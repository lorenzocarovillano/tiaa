/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:58:48 PM
**        * FROM NATURAL PROGRAM : Mihp0300
************************************************************
**        * FILE NAME            : Mihp0300.java
**        * CLASS NAME           : Mihp0300
**        * INSTANCE NAME        : Mihp0300
************************************************************
************************************************************************
* PROGRAM  : MIHP0300
* SYSTEM   :
* TITLE    : ARCHIVE MASTER INDEX
* GENERATED: JAN 18,96 AT 03:27 PM
* FUNCTION : THIS PROGRAM GENERATES WHO WORK REQUESTS AND WHO ACTIVITIES
*          : FROM MIT EVENTS.  ONLY THE EVENTS WHICH HAPPENED WITHIN A
*          : TIME WINDOW ON THE RUN CONTROL RECORD ARE SELECTED FOR
*          : ARCHIVING.
*
* CHANGES  : AT THE END OF PROCESS CONTROL RECORD WILL BE UPDATED WITH
*          : ENDING-DATE EQUAL TO LAST READ RECORD OR, IF THERE IS NO
*          : RECORDS READ,  EQUAL TO A STARTING-DATE.  09/05/96 L.E.
*          : *
*          : 02/22/98 CHECK OF THE #PROCESS-TIME-IND HAS BEEN ADDED
*          : TO THE LOGIC FOR SPECIFIC ACTIVITY RECORDS. ADDED BY L.E.
* 02/23/2017 - JHANWAR - PIN EXPANSION - AUG 2017 <STOW ONLY>
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Mihp0300 extends BLNatBase
{
    // Data Areas
    private GdaCwfg000 gdaCwfg000;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private PdaMiha0170 pdaMiha0170;
    private LdaMihl0170 ldaMihl0170;
    private LdaMihl0171 ldaMihl0171;
    private PdaMiha0030 pdaMiha0030;
    private PdaMiha0020 pdaMiha0020;
    private PdaMiha0010 pdaMiha0010;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Master_Index_View;
    private DbsField cwf_Master_Index_View_Last_Chnge_Dte_Tme;
    private DbsField cwf_Master_Index_View_Last_Chnge_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Rqst_Log_Dte_Tme;
    private DbsField cwf_Master_Index_View_Admin_Status_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_View_Admin_Unit_Cde;
    private DbsField cwf_Master_Index_View_Empl_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Step_Id;
    private DbsField cwf_Master_Index_View_Admin_Status_Cde;
    private DbsField cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Index_View_Final_Close_Out_Dte_Tme;
    private DbsField cwf_Master_Index_View_Pin_Nbr;
    private DbsField cwf_Master_Index_View_Rqst_Log_Invrt_Dte_Tme;
    private DbsField cwf_Master_Index_View_Rqst_Log_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Rqst_Orgn_Cde;
    private DbsField cwf_Master_Index_View_Sub_Rqst_Ind;
    private DbsField cwf_Master_Index_View_Case_Id_Cde;
    private DbsField cwf_Master_Index_View_Multi_Rqst_Ind;
    private DbsField cwf_Master_Index_View_Orgnl_Unit_Cde;
    private DbsField cwf_Master_Index_View_Work_Prcss_Id;
    private DbsField cwf_Master_Index_View_Assgn_Sprvsr_Oprtr_Cde;
    private DbsField cwf_Master_Index_View_Crprte_Status_Ind;
    private DbsField cwf_Master_Index_View_Actve_Ind;
    private DbsField cwf_Master_Index_View_Instn_Cde;
    private DbsField cwf_Master_Index_View_Rqst_Instn_Cde;
    private DbsField cwf_Master_Index_View_Check_Ind;
    private DbsField cwf_Master_Index_View_Tiaa_Rcvd_Dte;
    private DbsField cwf_Master_Index_View_Rqst_Invrt_Rcvd_Dte_Tme;
    private DbsField cwf_Master_Index_View_Effctve_Dte;
    private DbsField cwf_Master_Index_View_Trans_Dte;
    private DbsField cwf_Master_Index_View_Mj_Pull_Ind;
    private DbsField cwf_Master_Index_View_Rescan_Ind;
    private DbsField cwf_Master_Index_View_Cmplnt_Ind;
    private DbsGroup cwf_Master_Index_View_Cntrct_NbrMuGroup;
    private DbsField cwf_Master_Index_View_Cntrct_Nbr;
    private DbsField cwf_Master_Index_View_Rqst_Id;
    private DbsField cwf_Master_Index_View_Rlte_Rqst_Id;
    private DbsField cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte;
    private DbsField cwf_Master_Index_View_Due_Dte_Chg_Prty_Cde;

    private DbsGroup cwf_Master_Index_View__R_Field_1;
    private DbsField cwf_Master_Index_View_Pnd_Due_Dte_Chg_Prty_Cde_10;
    private DbsField cwf_Master_Index_View_Pnd_Process_Time_Ind;
    private DbsField cwf_Master_Index_View_Bsnss_Reply_Ind;
    private DbsField cwf_Master_Index_View_Status_Freeze_Ind;
    private DbsField cwf_Master_Index_View_Elctrnc_Fldr_Ind;
    private DbsField cwf_Master_Index_View_Log_Insttn_Srce_Cde;
    private DbsField cwf_Master_Index_View_Log_Rqstr_Cde;
    private DbsField cwf_Master_Index_View_Shphrd_Id;
    private DbsField cwf_Master_Index_View_Off_Rtng_Ind;
    private DbsField cwf_Master_Index_View_Crprte_On_Tme_Ind;
    private DbsField cwf_Master_Index_View_Owner_Unit_Cde;
    private DbsField cwf_Master_Index_View_Tiaa_Rcvd_Dte_Tme;
    private DbsField cwf_Master_Index_View_Trade_Dte_Tme;
    private DbsField cwf_Master_Index_View_Crprte_Due_Dte_Tme;
    private DbsField cwf_Master_Index_View_Rt_Sqnce_Nbr;
    private DbsField cwf_Master_Index_View_Last_Chnge_Invrt_Dte_Tme;
    private DbsField cwf_Master_Index_View_Unit_On_Tme_Ind;
    private DbsField cwf_Master_Index_View_Unit_Cde;
    private DbsField cwf_Master_Index_View_Status_Cde;
    private DbsField cwf_Master_Index_View_Rqst_Rgn_Cde;
    private DbsField cwf_Master_Index_View_Rqst_Spcl_Dsgntn_Cde;
    private DbsField cwf_Master_Index_View_Rqst_Brnch_Cde;
    private DbsField cwf_Master_Index_View_Trnsctn_Dte;

    private DataAccessProgramView vw_cwf_Who_Work_Request;
    private DbsField cwf_Who_Work_Request_Pin_Rqst_Key;
    private DbsField pnd_Chnge_Dte_Tme_Log_Dte_Tme_Key;

    private DbsGroup pnd_Chnge_Dte_Tme_Log_Dte_Tme_Key__R_Field_2;
    private DbsField pnd_Chnge_Dte_Tme_Log_Dte_Tme_Key_Pnd_Last_Chnge_Dte_Tme;
    private DbsField pnd_Chnge_Dte_Tme_Log_Dte_Tme_Key_Pnd_Rqst_Log_Dte_Tme;
    private DbsField pnd_Et_Threshold;
    private DbsField pnd_Records_Updated_Counter;
    private DbsField pnd_Work_Request_Counter;
    private DbsField pnd_Activity_Counter;
    private DbsField pnd_Run_Id;
    private DbsField pnd_Last_Chnge_Dte_Tme_Save;

    private DbsGroup pnd_Time_Window_Last_Change_Date_Time;
    private DbsField pnd_Time_Window_Last_Change_Date_Time_Pnd_Starting_Numeric;

    private DbsGroup pnd_Time_Window_Last_Change_Date_Time__R_Field_3;
    private DbsField pnd_Time_Window_Last_Change_Date_Time_Pnd_Starting_Alpha;
    private DbsField pnd_Time_Window_Last_Change_Date_Time_Pnd_Ending_Numeric;

    private DbsGroup pnd_Time_Window_Last_Change_Date_Time__R_Field_4;
    private DbsField pnd_Time_Window_Last_Change_Date_Time_Pnd_Ending_Alpha;
    private DbsField pnd_Transaction_Data;

    private DbsGroup pnd_Transaction_Data__R_Field_5;
    private DbsField pnd_Transaction_Data_Pnd_Last_Chnge_Dte_Tme;

    private DbsGroup pnd_Transaction_Data__R_Field_6;
    private DbsField pnd_Transaction_Data_Pnd_Last_Chnge_Dte_Tme_Alpha;
    private DbsField pnd_Transaction_Data_Filler;
    private DbsField pnd_Admin_Status_Cde_Split;

    private DbsGroup pnd_Admin_Status_Cde_Split__R_Field_7;
    private DbsField pnd_Admin_Status_Cde_Split_Pnd_Admin_Status_Cde_Split_1;
    private DbsField pnd_Due_Dte_Chg_Prty_Cde;

    private DbsGroup pnd_Due_Dte_Chg_Prty_Cde__R_Field_8;
    private DbsField pnd_Due_Dte_Chg_Prty_Cde_Pnd_Due_Dte_Filler;
    private DbsField pnd_Due_Dte_Chg_Prty_Cde_Pnd_Due_Dte_Chg_Rec_Ind;
    private DbsField pnd_Skip_C_Stat_Checking;
    private DbsField pnd_Mit_Read;
    private DbsField pnd_All_Et;
    private DbsField pnd_Frst_Rldt;
    private DbsField pnd_Frst_Lcdt;
    private DbsField pnd_Last_Rldt;
    private DbsField pnd_Last_Lcdt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaCwfg000 = GdaCwfg000.getInstance(getCallnatLevel());
        registerRecord(gdaCwfg000);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        pdaMiha0170 = new PdaMiha0170(localVariables);
        ldaMihl0170 = new LdaMihl0170();
        registerRecord(ldaMihl0170);
        registerRecord(ldaMihl0170.getVw_cwf_Who_Support_Tbl());
        ldaMihl0171 = new LdaMihl0171();
        registerRecord(ldaMihl0171);
        pdaMiha0030 = new PdaMiha0030(localVariables);
        pdaMiha0020 = new PdaMiha0020(localVariables);
        pdaMiha0010 = new PdaMiha0010(localVariables);

        // Local Variables

        vw_cwf_Master_Index_View = new DataAccessProgramView(new NameInfo("vw_cwf_Master_Index_View", "CWF-MASTER-INDEX-VIEW"), "CWF_MASTER_INDEX_VIEW", 
            "CWF_MASTER_INDEX");
        cwf_Master_Index_View_Last_Chnge_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "LAST_CHNGE_DTE_TME");
        cwf_Master_Index_View_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");
        cwf_Master_Index_View_Last_Chnge_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Chnge_Oprtr_Cde", 
            "LAST-CHNGE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_CHNGE_OPRTR_CDE");
        cwf_Master_Index_View_Last_Chnge_Oprtr_Cde.setDdmHeader("LOG CREATE/OPERATOR");
        cwf_Master_Index_View_Rqst_Log_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Master_Index_View_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        cwf_Master_Index_View_Admin_Status_Updte_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Admin_Status_Updte_Dte_Tme", 
            "ADMIN-STATUS-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_DTE_TME");
        cwf_Master_Index_View_Admin_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Admin_Unit_Cde", "ADMIN-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ADMIN_UNIT_CDE");
        cwf_Master_Index_View_Empl_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "EMPL_OPRTR_CDE");
        cwf_Master_Index_View_Empl_Oprtr_Cde.setDdmHeader("EMPLOYEE/ID");
        cwf_Master_Index_View_Step_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Step_Id", "STEP-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "STEP_ID");
        cwf_Master_Index_View_Step_Id.setDdmHeader("STEP/ID");
        cwf_Master_Index_View_Admin_Status_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Admin_Status_Cde", "ADMIN-STATUS-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "ADMIN_STATUS_CDE");
        cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme", 
            "CRPRTE-CLOCK-END-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "CRPRTE_CLOCK_END_DTE_TME");
        cwf_Master_Index_View_Crprte_Clock_End_Dte_Tme.setDdmHeader("CORPORATE CLOCK END DATE TIME");
        cwf_Master_Index_View_Final_Close_Out_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Final_Close_Out_Dte_Tme", 
            "FINAL-CLOSE-OUT-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "FINAL_CLOSE_OUT_DTE_TME");
        cwf_Master_Index_View_Final_Close_Out_Dte_Tme.setDdmHeader("FINAL/CLOSEOUT/DATE-TIME");
        cwf_Master_Index_View_Pin_Nbr = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "PIN_NBR");
        cwf_Master_Index_View_Pin_Nbr.setDdmHeader("PIN");
        cwf_Master_Index_View_Rqst_Log_Invrt_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Invrt_Dte_Tme", 
            "RQST-LOG-INVRT-DTE-TME", FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "RQST_LOG_INVRT_DTE_TME");
        cwf_Master_Index_View_Rqst_Log_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_LOG_OPRTR_CDE");
        cwf_Master_Index_View_Rqst_Log_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Master_Index_View_Rqst_Orgn_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Orgn_Cde", "RQST-ORGN-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_ORGN_CDE");
        cwf_Master_Index_View_Sub_Rqst_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Sub_Rqst_Ind", "SUB-RQST-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "SUB_RQST_IND");
        cwf_Master_Index_View_Sub_Rqst_Ind.setDdmHeader("SUB-REQUEST COUNT");
        cwf_Master_Index_View_Case_Id_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Case_Id_Cde", "CASE-ID-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CASE_ID_CDE");
        cwf_Master_Index_View_Case_Id_Cde.setDdmHeader("CASE/ID");
        cwf_Master_Index_View_Multi_Rqst_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Multi_Rqst_Ind", "MULTI-RQST-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "MULTI_RQST_IND");
        cwf_Master_Index_View_Multi_Rqst_Ind.setDdmHeader("MULTI/IND");
        cwf_Master_Index_View_Orgnl_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ORGNL_UNIT_CDE");
        cwf_Master_Index_View_Orgnl_Unit_Cde.setDdmHeader("LOG/UNIT");
        cwf_Master_Index_View_Work_Prcss_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Work_Prcss_Id", "WORK-PRCSS-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        cwf_Master_Index_View_Work_Prcss_Id.setDdmHeader("WORK/ID");
        cwf_Master_Index_View_Assgn_Sprvsr_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Assgn_Sprvsr_Oprtr_Cde", 
            "ASSGN-SPRVSR-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "ASSGN_SPRVSR_OPRTR_CDE");
        cwf_Master_Index_View_Assgn_Sprvsr_Oprtr_Cde.setDdmHeader("SUPER/VISOR");
        cwf_Master_Index_View_Crprte_Status_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Crprte_Status_Ind", "CRPRTE-STATUS-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CRPRTE_STATUS_IND");
        cwf_Master_Index_View_Crprte_Status_Ind.setDdmHeader("CORP/STAT");
        cwf_Master_Index_View_Actve_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Actve_Ind", "ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ACTVE_IND");
        cwf_Master_Index_View_Instn_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Instn_Cde", "INSTN-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "INSTN_CDE");
        cwf_Master_Index_View_Rqst_Instn_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Instn_Cde", "RQST-INSTN-CDE", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "RQST_INSTN_CDE");
        cwf_Master_Index_View_Check_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Check_Ind", "CHECK-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CHECK_IND");
        cwf_Master_Index_View_Check_Ind.setDdmHeader("CHK");
        cwf_Master_Index_View_Tiaa_Rcvd_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_RCVD_DTE");
        cwf_Master_Index_View_Rqst_Invrt_Rcvd_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Invrt_Rcvd_Dte_Tme", 
            "RQST-INVRT-RCVD-DTE-TME", FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "RQST_INVRT_RCVD_DTE_TME");
        cwf_Master_Index_View_Effctve_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Effctve_Dte", "EFFCTVE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "EFFCTVE_DTE");
        cwf_Master_Index_View_Trans_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Trans_Dte", "TRANS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TRANS_DTE");
        cwf_Master_Index_View_Trans_Dte.setDdmHeader("CHECK MAIL/DATE");
        cwf_Master_Index_View_Mj_Pull_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Mj_Pull_Ind", "MJ-PULL-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "MJ_PULL_IND");
        cwf_Master_Index_View_Mj_Pull_Ind.setDdmHeader("MJ PULL/IND");
        cwf_Master_Index_View_Rescan_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rescan_Ind", "RESCAN-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RESCAN_IND");
        cwf_Master_Index_View_Rescan_Ind.setDdmHeader("RE-SCAN/INDICATOR");
        cwf_Master_Index_View_Cmplnt_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Cmplnt_Ind", "CMPLNT-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CMPLNT_IND");
        cwf_Master_Index_View_Cntrct_NbrMuGroup = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("CWF_MASTER_INDEX_VIEW_CNTRCT_NBRMuGroup", "CNTRCT_NBRMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "CWF_MASTER_INDEX_CNTRCT_NBR");
        cwf_Master_Index_View_Cntrct_Nbr = cwf_Master_Index_View_Cntrct_NbrMuGroup.newFieldArrayInGroup("cwf_Master_Index_View_Cntrct_Nbr", "CNTRCT-NBR", 
            FieldType.STRING, 8, new DbsArrayController(1, 10), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_NBR");
        cwf_Master_Index_View_Rqst_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Id", "RQST-ID", FieldType.STRING, 
            28, RepeatingFieldStrategy.None, "RQST_ID");
        cwf_Master_Index_View_Rqst_Id.setDdmHeader("REQUEST ID");
        cwf_Master_Index_View_Rlte_Rqst_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rlte_Rqst_Id", "RLTE-RQST-ID", 
            FieldType.STRING, 28, RepeatingFieldStrategy.None, "RLTE_RQST_ID");
        cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte", 
            "EXTRNL-PEND-RCV-DTE", FieldType.TIME, RepeatingFieldStrategy.None, "EXTRNL_PEND_RCV_DTE");
        cwf_Master_Index_View_Extrnl_Pend_Rcv_Dte.setDdmHeader("LAST UNPEND DATE");
        cwf_Master_Index_View_Due_Dte_Chg_Prty_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Due_Dte_Chg_Prty_Cde", 
            "DUE-DTE-CHG-PRTY-CDE", FieldType.STRING, 16, RepeatingFieldStrategy.None, "DUE_DTE_CHG_PRTY_CDE");
        cwf_Master_Index_View_Due_Dte_Chg_Prty_Cde.setDdmHeader("DUE DATE CHANGE IND. PRIORITY");

        cwf_Master_Index_View__R_Field_1 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_1", "REDEFINE", cwf_Master_Index_View_Due_Dte_Chg_Prty_Cde);
        cwf_Master_Index_View_Pnd_Due_Dte_Chg_Prty_Cde_10 = cwf_Master_Index_View__R_Field_1.newFieldInGroup("cwf_Master_Index_View_Pnd_Due_Dte_Chg_Prty_Cde_10", 
            "#DUE-DTE-CHG-PRTY-CDE-10", FieldType.STRING, 10);
        cwf_Master_Index_View_Pnd_Process_Time_Ind = cwf_Master_Index_View__R_Field_1.newFieldInGroup("cwf_Master_Index_View_Pnd_Process_Time_Ind", "#PROCESS-TIME-IND", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Bsnss_Reply_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Bsnss_Reply_Ind", "BSNSS-REPLY-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "BSNSS_REPLY_IND");
        cwf_Master_Index_View_Bsnss_Reply_Ind.setDdmHeader("BUSINESS/REPLY");
        cwf_Master_Index_View_Status_Freeze_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Freeze_Ind", "STATUS-FREEZE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "STATUS_FREEZE_IND");
        cwf_Master_Index_View_Status_Freeze_Ind.setDdmHeader("FREEZE/STATUS");
        cwf_Master_Index_View_Elctrnc_Fldr_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Elctrnc_Fldr_Ind", "ELCTRNC-FLDR-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ELCTRNC_FLDR_IND");
        cwf_Master_Index_View_Elctrnc_Fldr_Ind.setDdmHeader("ELECTRONIC/FOLDER IND");
        cwf_Master_Index_View_Log_Insttn_Srce_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Log_Insttn_Srce_Cde", 
            "LOG-INSTTN-SRCE-CDE", FieldType.STRING, 5, RepeatingFieldStrategy.None, "LOG_INSTTN_SRCE_CDE");
        cwf_Master_Index_View_Log_Insttn_Srce_Cde.setDdmHeader("REQUESTOR/INSTITUTION");
        cwf_Master_Index_View_Log_Rqstr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Log_Rqstr_Cde", "LOG-RQSTR-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "LOG_RQSTR_CDE");
        cwf_Master_Index_View_Log_Rqstr_Cde.setDdmHeader("REQUESTOR/TYPE");
        cwf_Master_Index_View_Shphrd_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Shphrd_Id", "SHPHRD-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "SHPHRD_ID");
        cwf_Master_Index_View_Shphrd_Id.setDdmHeader("SHEPHERD");
        cwf_Master_Index_View_Off_Rtng_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Off_Rtng_Ind", "OFF-RTNG-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "OFF_RTNG_IND");
        cwf_Master_Index_View_Off_Rtng_Ind.setDdmHeader("OFF-ROUTING");
        cwf_Master_Index_View_Crprte_On_Tme_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Crprte_On_Tme_Ind", "CRPRTE-ON-TME-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CRPRTE_ON_TME_IND");
        cwf_Master_Index_View_Crprte_On_Tme_Ind.setDdmHeader("WORKRQST/DUE DATE");
        cwf_Master_Index_View_Owner_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Owner_Unit_Cde", "OWNER-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "OWNER_UNIT_CDE");
        cwf_Master_Index_View_Owner_Unit_Cde.setDdmHeader("OWNERUNIT");
        cwf_Master_Index_View_Tiaa_Rcvd_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Tiaa_Rcvd_Dte_Tme", "TIAA-RCVD-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "TIAA_RCVD_DTE_TME");
        cwf_Master_Index_View_Tiaa_Rcvd_Dte_Tme.setDdmHeader("TIAA-RCV/DTE-TME");
        cwf_Master_Index_View_Trade_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Trade_Dte_Tme", "TRADE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "TRADE_DTE_TME");
        cwf_Master_Index_View_Trade_Dte_Tme.setDdmHeader("TRADE/DTE-TME");
        cwf_Master_Index_View_Crprte_Due_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Crprte_Due_Dte_Tme", "CRPRTE-DUE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CRPRTE_DUE_DTE_TME");
        cwf_Master_Index_View_Crprte_Due_Dte_Tme.setDdmHeader("WORKRQST/DUE DATE");
        cwf_Master_Index_View_Rt_Sqnce_Nbr = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rt_Sqnce_Nbr", "RT-SQNCE-NBR", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "RT_SQNCE_NBR");
        cwf_Master_Index_View_Rt_Sqnce_Nbr.setDdmHeader("ROUTE/SEQ.");
        cwf_Master_Index_View_Last_Chnge_Invrt_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Chnge_Invrt_Dte_Tme", 
            "LAST-CHNGE-INVRT-DTE-TME", FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "LAST_CHNGE_INVRT_DTE_TME");
        cwf_Master_Index_View_Unit_On_Tme_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Unit_On_Tme_Ind", "UNIT-ON-TME-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "UNIT_ON_TME_IND");
        cwf_Master_Index_View_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Unit_Cde", "UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "UNIT_CDE");
        cwf_Master_Index_View_Unit_Cde.setDdmHeader("UNIT/CODE");
        cwf_Master_Index_View_Status_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Cde", "STATUS-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "STATUS_CDE");
        cwf_Master_Index_View_Rqst_Rgn_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Rgn_Cde", "RQST-RGN-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_RGN_CDE");
        cwf_Master_Index_View_Rqst_Spcl_Dsgntn_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Spcl_Dsgntn_Cde", 
            "RQST-SPCL-DSGNTN-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_SPCL_DSGNTN_CDE");
        cwf_Master_Index_View_Rqst_Brnch_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Brnch_Cde", "RQST-BRNCH-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_BRNCH_CDE");
        cwf_Master_Index_View_Trnsctn_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Trnsctn_Dte", "TRNSCTN-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TRNSCTN_DTE");
        cwf_Master_Index_View_Trnsctn_Dte.setDdmHeader("TRANSACTION/DATE");
        registerRecord(vw_cwf_Master_Index_View);

        vw_cwf_Who_Work_Request = new DataAccessProgramView(new NameInfo("vw_cwf_Who_Work_Request", "CWF-WHO-WORK-REQUEST"), "CWF_WHO_WORK_REQUEST", "CWF_WORK_REQUEST");
        cwf_Who_Work_Request_Pin_Rqst_Key = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Pin_Rqst_Key", "PIN-RQST-KEY", FieldType.STRING, 
            55, RepeatingFieldStrategy.None, "PIN_RQST_KEY");
        cwf_Who_Work_Request_Pin_Rqst_Key.setDdmHeader("PIN-RQST-KEY");
        cwf_Who_Work_Request_Pin_Rqst_Key.setSuperDescriptor(true);
        registerRecord(vw_cwf_Who_Work_Request);

        pnd_Chnge_Dte_Tme_Log_Dte_Tme_Key = localVariables.newFieldInRecord("pnd_Chnge_Dte_Tme_Log_Dte_Tme_Key", "#CHNGE-DTE-TME-LOG-DTE-TME-KEY", FieldType.STRING, 
            30);

        pnd_Chnge_Dte_Tme_Log_Dte_Tme_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Chnge_Dte_Tme_Log_Dte_Tme_Key__R_Field_2", "REDEFINE", pnd_Chnge_Dte_Tme_Log_Dte_Tme_Key);
        pnd_Chnge_Dte_Tme_Log_Dte_Tme_Key_Pnd_Last_Chnge_Dte_Tme = pnd_Chnge_Dte_Tme_Log_Dte_Tme_Key__R_Field_2.newFieldInGroup("pnd_Chnge_Dte_Tme_Log_Dte_Tme_Key_Pnd_Last_Chnge_Dte_Tme", 
            "#LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 15);
        pnd_Chnge_Dte_Tme_Log_Dte_Tme_Key_Pnd_Rqst_Log_Dte_Tme = pnd_Chnge_Dte_Tme_Log_Dte_Tme_Key__R_Field_2.newFieldInGroup("pnd_Chnge_Dte_Tme_Log_Dte_Tme_Key_Pnd_Rqst_Log_Dte_Tme", 
            "#RQST-LOG-DTE-TME", FieldType.NUMERIC, 15);
        pnd_Et_Threshold = localVariables.newFieldInRecord("pnd_Et_Threshold", "#ET-THRESHOLD", FieldType.PACKED_DECIMAL, 4);
        pnd_Records_Updated_Counter = localVariables.newFieldInRecord("pnd_Records_Updated_Counter", "#RECORDS-UPDATED-COUNTER", FieldType.PACKED_DECIMAL, 
            11);
        pnd_Work_Request_Counter = localVariables.newFieldInRecord("pnd_Work_Request_Counter", "#WORK-REQUEST-COUNTER", FieldType.PACKED_DECIMAL, 11);
        pnd_Activity_Counter = localVariables.newFieldInRecord("pnd_Activity_Counter", "#ACTIVITY-COUNTER", FieldType.PACKED_DECIMAL, 11);
        pnd_Run_Id = localVariables.newFieldInRecord("pnd_Run_Id", "#RUN-ID", FieldType.NUMERIC, 9);
        pnd_Last_Chnge_Dte_Tme_Save = localVariables.newFieldInRecord("pnd_Last_Chnge_Dte_Tme_Save", "#LAST-CHNGE-DTE-TME-SAVE", FieldType.STRING, 15);

        pnd_Time_Window_Last_Change_Date_Time = localVariables.newGroupInRecord("pnd_Time_Window_Last_Change_Date_Time", "#TIME-WINDOW-LAST-CHANGE-DATE-TIME");
        pnd_Time_Window_Last_Change_Date_Time_Pnd_Starting_Numeric = pnd_Time_Window_Last_Change_Date_Time.newFieldInGroup("pnd_Time_Window_Last_Change_Date_Time_Pnd_Starting_Numeric", 
            "#STARTING-NUMERIC", FieldType.NUMERIC, 15);

        pnd_Time_Window_Last_Change_Date_Time__R_Field_3 = pnd_Time_Window_Last_Change_Date_Time.newGroupInGroup("pnd_Time_Window_Last_Change_Date_Time__R_Field_3", 
            "REDEFINE", pnd_Time_Window_Last_Change_Date_Time_Pnd_Starting_Numeric);
        pnd_Time_Window_Last_Change_Date_Time_Pnd_Starting_Alpha = pnd_Time_Window_Last_Change_Date_Time__R_Field_3.newFieldInGroup("pnd_Time_Window_Last_Change_Date_Time_Pnd_Starting_Alpha", 
            "#STARTING-ALPHA", FieldType.STRING, 15);
        pnd_Time_Window_Last_Change_Date_Time_Pnd_Ending_Numeric = pnd_Time_Window_Last_Change_Date_Time.newFieldInGroup("pnd_Time_Window_Last_Change_Date_Time_Pnd_Ending_Numeric", 
            "#ENDING-NUMERIC", FieldType.NUMERIC, 15);

        pnd_Time_Window_Last_Change_Date_Time__R_Field_4 = pnd_Time_Window_Last_Change_Date_Time.newGroupInGroup("pnd_Time_Window_Last_Change_Date_Time__R_Field_4", 
            "REDEFINE", pnd_Time_Window_Last_Change_Date_Time_Pnd_Ending_Numeric);
        pnd_Time_Window_Last_Change_Date_Time_Pnd_Ending_Alpha = pnd_Time_Window_Last_Change_Date_Time__R_Field_4.newFieldInGroup("pnd_Time_Window_Last_Change_Date_Time_Pnd_Ending_Alpha", 
            "#ENDING-ALPHA", FieldType.STRING, 15);
        pnd_Transaction_Data = localVariables.newFieldInRecord("pnd_Transaction_Data", "#TRANSACTION-DATA", FieldType.STRING, 50);

        pnd_Transaction_Data__R_Field_5 = localVariables.newGroupInRecord("pnd_Transaction_Data__R_Field_5", "REDEFINE", pnd_Transaction_Data);
        pnd_Transaction_Data_Pnd_Last_Chnge_Dte_Tme = pnd_Transaction_Data__R_Field_5.newFieldInGroup("pnd_Transaction_Data_Pnd_Last_Chnge_Dte_Tme", "#LAST-CHNGE-DTE-TME", 
            FieldType.NUMERIC, 15);

        pnd_Transaction_Data__R_Field_6 = pnd_Transaction_Data__R_Field_5.newGroupInGroup("pnd_Transaction_Data__R_Field_6", "REDEFINE", pnd_Transaction_Data_Pnd_Last_Chnge_Dte_Tme);
        pnd_Transaction_Data_Pnd_Last_Chnge_Dte_Tme_Alpha = pnd_Transaction_Data__R_Field_6.newFieldInGroup("pnd_Transaction_Data_Pnd_Last_Chnge_Dte_Tme_Alpha", 
            "#LAST-CHNGE-DTE-TME-ALPHA", FieldType.STRING, 15);
        pnd_Transaction_Data_Filler = pnd_Transaction_Data__R_Field_5.newFieldInGroup("pnd_Transaction_Data_Filler", "FILLER", FieldType.STRING, 35);
        pnd_Admin_Status_Cde_Split = localVariables.newFieldInRecord("pnd_Admin_Status_Cde_Split", "#ADMIN-STATUS-CDE-SPLIT", FieldType.STRING, 4);

        pnd_Admin_Status_Cde_Split__R_Field_7 = localVariables.newGroupInRecord("pnd_Admin_Status_Cde_Split__R_Field_7", "REDEFINE", pnd_Admin_Status_Cde_Split);
        pnd_Admin_Status_Cde_Split_Pnd_Admin_Status_Cde_Split_1 = pnd_Admin_Status_Cde_Split__R_Field_7.newFieldInGroup("pnd_Admin_Status_Cde_Split_Pnd_Admin_Status_Cde_Split_1", 
            "#ADMIN-STATUS-CDE-SPLIT-1", FieldType.STRING, 1);
        pnd_Due_Dte_Chg_Prty_Cde = localVariables.newFieldInRecord("pnd_Due_Dte_Chg_Prty_Cde", "#DUE-DTE-CHG-PRTY-CDE", FieldType.STRING, 16);

        pnd_Due_Dte_Chg_Prty_Cde__R_Field_8 = localVariables.newGroupInRecord("pnd_Due_Dte_Chg_Prty_Cde__R_Field_8", "REDEFINE", pnd_Due_Dte_Chg_Prty_Cde);
        pnd_Due_Dte_Chg_Prty_Cde_Pnd_Due_Dte_Filler = pnd_Due_Dte_Chg_Prty_Cde__R_Field_8.newFieldInGroup("pnd_Due_Dte_Chg_Prty_Cde_Pnd_Due_Dte_Filler", 
            "#DUE-DTE-FILLER", FieldType.STRING, 11);
        pnd_Due_Dte_Chg_Prty_Cde_Pnd_Due_Dte_Chg_Rec_Ind = pnd_Due_Dte_Chg_Prty_Cde__R_Field_8.newFieldInGroup("pnd_Due_Dte_Chg_Prty_Cde_Pnd_Due_Dte_Chg_Rec_Ind", 
            "#DUE-DTE-CHG-REC-IND", FieldType.STRING, 1);
        pnd_Skip_C_Stat_Checking = localVariables.newFieldInRecord("pnd_Skip_C_Stat_Checking", "#SKIP-C-STAT-CHECKING", FieldType.STRING, 1);
        pnd_Mit_Read = localVariables.newFieldInRecord("pnd_Mit_Read", "#MIT-READ", FieldType.NUMERIC, 11);
        pnd_All_Et = localVariables.newFieldInRecord("pnd_All_Et", "#ALL-ET", FieldType.NUMERIC, 11);
        pnd_Frst_Rldt = localVariables.newFieldInRecord("pnd_Frst_Rldt", "#FRST-RLDT", FieldType.STRING, 15);
        pnd_Frst_Lcdt = localVariables.newFieldInRecord("pnd_Frst_Lcdt", "#FRST-LCDT", FieldType.STRING, 15);
        pnd_Last_Rldt = localVariables.newFieldInRecord("pnd_Last_Rldt", "#LAST-RLDT", FieldType.STRING, 15);
        pnd_Last_Lcdt = localVariables.newFieldInRecord("pnd_Last_Lcdt", "#LAST-LCDT", FieldType.STRING, 15);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Master_Index_View.reset();
        vw_cwf_Who_Work_Request.reset();

        ldaMihl0170.initializeValues();
        ldaMihl0171.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Mihp0300() throws Exception
    {
        super("Mihp0300");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("MIHP0300", onError);
        //*  WRITE /'MIHP0300.....STARTED...'
        //*  INITIALIZATION
        //*  --------------
        //* (TO PREVENT HOLD QUEUE OVERFLOW)
        //* (TO ALLOW EASY ESCAPE FROM THE ROUTINE)
        pnd_Et_Threshold.setValue(10);                                                                                                                                    //Natural: ASSIGN #ET-THRESHOLD := 10
        //*  MAIN PROCESSING
        //*  ---------------
        PROGRAM:                                                                                                                                                          //Natural: REPEAT
        while (condition(whileTrue))
        {
                                                                                                                                                                          //Natural: PERFORM SET-UP-THE-RUN-CONTROL-RECORD
            sub_Set_Up_The_Run_Control_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PROGRAM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PROGRAM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM PREVENT-NATURAL-ERROR-3021
            sub_Prevent_Natural_Error_3021();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PROGRAM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PROGRAM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM ARCHIVE-MASTER-INDEX
            sub_Archive_Master_Index();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PROGRAM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PROGRAM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_All_Et.nadd(pnd_Records_Updated_Counter);                                                                                                                 //Natural: ADD #RECORDS-UPDATED-COUNTER TO #ALL-ET
            //*  FORCING TO ET LAST BATCH
            pnd_Records_Updated_Counter.setValue(11);                                                                                                                     //Natural: MOVE 11 TO #RECORDS-UPDATED-COUNTER
                                                                                                                                                                          //Natural: PERFORM TRANSACTION-CONTROL
            sub_Transaction_Control();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PROGRAM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PROGRAM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  -------- CALLING SWEEP MODULE ----------------
            //*  CALLNAT 'MIHN0011' MIHA0170-INPUT MIHA0170-OUTPUT
            //*           CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
            //*  IF MSG-INFO-SUB.##MSG-NR = 999
            //*     OR MSG-INFO-SUB.##RETURN-CODE = 'E'
            //*   BACKOUT TRANSACTION
            //*   MOVE BY NAME MSG-INFO-SUB TO MSG-INFO
            //*   PERFORM ESCAPE-PROGRAM /*(TO RETURN TO THE CALLING PROGRAM)
            //*  END-IF /*(1800)
            //* ****************
                                                                                                                                                                          //Natural: PERFORM SET-THE-RUN-CONTROL-RECORD-TO-COMPLETE
            sub_Set_The_Run_Control_Record_To_Complete();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PROGRAM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PROGRAM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (true) break PROGRAM;                                                                                                                                      //Natural: ESCAPE BOTTOM ( PROGRAM. )
            //*  SUBROUTINE JUST FOR ENDING THIS PROGRAM IMMEDIATELY
            //*  ---------------------------------------------------
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ESCAPE-PROGRAM
            //* (PROGRAM.)
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  SUBROUTINES
        //*  ===========
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-UP-THE-RUN-CONTROL-RECORD
        //*  =============================================
        //*  USE MIHN0170 TO SET UP THE RUN CONTROL RECORD.  IT ALSO RETURNS THE
        //*  ISN OF THE RUN CONTROL RECORDS AND INDICATES WHETHER OR NOT THIS IS
        //*  A RESTART
        //*  -------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ARCHIVE-MASTER-INDEX
        //*  SELECT ALL MIT RECORDS IN THE TIME WINDOW
        //*  -----------------------------------------
        //*  ----------- CHANGED BY L.E. ON 10/22/98
        //*  CALL MIHN0020 TO COMPLETE THE LAST INCOMPLETE ACTIVITY, IF
        //*  THERE IS ONE, AND CREATE THE NEXT INCOMPLETE ACTIVITY IF NECESSARY
        //*  ------------------------------------------------------------------
        //*                                                      (ACTIVITY.
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TRANSACTION-CONTROL
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-FOR-SUBPROGRAM-ERRORS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-THE-RUN-CONTROL-RECORD-TO-COMPLETE
        //*  ======================================================
        //* ***** ADDED BY L.E.
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PREVENT-NATURAL-ERROR-3021
        //*  ==========================================
        //*  THIS OPENS THE DATABASE IN WHICH THE WHO FILES ARE KEPT.  IF THIS IS
        //*  NOT DONE IN THIS PROGRAM, DOING IT IN A SUBPROGRAM CALLED BY THIS
        //*  PROGRAM CAN CAUSE THE DATABASE ON WHICH MIT IS KEPT TO BE CLOSED AND
        //*  THE NATURAL ERROR 3021, INVALID COMMAND ID, OCCURS.
        //*  ON NATURAL RUN TIME ERROR, RETURN WITH AN APPROPRIATE ERROR MESSAGE
        //*  ================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STATISTIC-REPORT
        //*        'Number of All ET issued                          =' #ALL-ET   /
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Escape_Program() throws Exception                                                                                                                    //Natural: ESCAPE-PROGRAM
    {
        if (BLNatReinput.isReinput()) return;

        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( PROGRAM. )
        Global.setEscapeCode(EscapeType.Bottom, "PROGRAM");
        if (true) return;
        //* (1960)
    }
    private void sub_Set_Up_The_Run_Control_Record() throws Exception                                                                                                     //Natural: SET-UP-THE-RUN-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pdaMiha0170.getMiha0170_Input_Job_Name().setValue(Global.getINIT_USER());                                                                                         //Natural: ASSIGN MIHA0170-INPUT.JOB-NAME := *INIT-USER
        pdaMiha0170.getMiha0170_Input_Program_Name().setValue(Global.getPROGRAM());                                                                                       //Natural: ASSIGN MIHA0170-INPUT.PROGRAM-NAME := *PROGRAM
        //*  WRITE /'CALLING MIHN0170..CONTROL RECORD...'
        DbsUtil.callnat(Mihn0170.class , getCurrentProcessState(), pdaMiha0170.getMiha0170_Input(), pdaMiha0170.getMiha0170_Output(), pdaCdaobj.getCdaobj(),              //Natural: CALLNAT 'MIHN0170' MIHA0170-INPUT MIHA0170-OUTPUT CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
            pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
        if (condition(Global.isEscape())) return;
        //*  WRITE /'CAME BACK FROM MIHN0170.....'
        //*        / MSG-INFO-SUB.##MSG
        //*        / MSG-INFO-SUB.##MSG-NR
        //*        / MSG-INFO-SUB.##MSG-DATA(1)
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().equals(999) || pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("E")))                         //Natural: IF MSG-INFO-SUB.##MSG-NR = 999 OR MSG-INFO-SUB.##RETURN-CODE = 'E'
        {
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
            gdaCwfg000.getMsg_Info().setValuesByName(pdaCwfpda_M.getMsg_Info_Sub());                                                                                      //Natural: MOVE BY NAME MSG-INFO-SUB TO MSG-INFO
            //* (TO RETURN TO THE CALLING PROGRAM)
                                                                                                                                                                          //Natural: PERFORM ESCAPE-PROGRAM
            sub_Escape_Program();
            if (condition(Global.isEscape())) {return;}
            //* (1800)
        }                                                                                                                                                                 //Natural: END-IF
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //* (2050)
    }
    private void sub_Archive_Master_Index() throws Exception                                                                                                              //Natural: ARCHIVE-MASTER-INDEX
    {
        if (BLNatReinput.isReinput()) return;

        //*  ====================================
        //*  GET THE RUN CONTROL RECORD TO DETERMINE THE PROCESSING TIME WINDOW
        //*  ------------------------------------------------------------------
        INITIAL_RUN_CONTROL_GET:                                                                                                                                          //Natural: GET CWF-WHO-SUPPORT-TBL MIHA0170-OUTPUT.RUN-CONTROL-ISN
        ldaMihl0170.getVw_cwf_Who_Support_Tbl().readByID(pdaMiha0170.getMiha0170_Output_Run_Control_Isn().getLong(), "INITIAL_RUN_CONTROL_GET");
        pnd_Run_Id.setValue(ldaMihl0170.getCwf_Who_Support_Tbl_Run_Id());                                                                                                 //Natural: ASSIGN #RUN-ID := CWF-WHO-SUPPORT-TBL.RUN-ID
        pnd_Time_Window_Last_Change_Date_Time_Pnd_Starting_Alpha.setValueEdited(ldaMihl0170.getCwf_Who_Support_Tbl_Starting_Time(),new ReportEditMask("YYYYMMDDHHIISST")); //Natural: MOVE EDITED CWF-WHO-SUPPORT-TBL.STARTING-TIME ( EM = YYYYMMDDHHIISST ) TO #TIME-WINDOW-LAST-CHANGE-DATE-TIME.#STARTING-ALPHA
        pnd_Time_Window_Last_Change_Date_Time_Pnd_Ending_Alpha.setValueEdited(ldaMihl0170.getCwf_Who_Support_Tbl_Ending_Time(),new ReportEditMask("YYYYMMDDHHIISST"));    //Natural: MOVE EDITED CWF-WHO-SUPPORT-TBL.ENDING-TIME ( EM = YYYYMMDDHHIISST ) TO #TIME-WINDOW-LAST-CHANGE-DATE-TIME.#ENDING-ALPHA
        pdaMiha0170.getMiha0170_Input_Control_Start_Time().setValue(pnd_Time_Window_Last_Change_Date_Time_Pnd_Starting_Numeric);                                          //Natural: MOVE #STARTING-NUMERIC TO MIHA0170-INPUT.CONTROL-START-TIME
        pdaMiha0170.getMiha0170_Input_Control_End_Time().setValue(pnd_Time_Window_Last_Change_Date_Time_Pnd_Ending_Numeric);                                              //Natural: MOVE #ENDING-NUMERIC TO MIHA0170-INPUT.CONTROL-END-TIME
        //*  SET UP THE STARTING KEY DEPENDING ON WHETHER OR NOT THIS IS A RESTART
        //*  ---------------------------------------------------------------------
        if (condition(pdaMiha0170.getMiha0170_Output_Restarting_Flag().equals(true) && ldaMihl0170.getCwf_Who_Support_Tbl_Transaction_Data().notEquals(" ")))             //Natural: IF MIHA0170-OUTPUT.RESTARTING-FLAG = TRUE AND CWF-WHO-SUPPORT-TBL.TRANSACTION-DATA NE ' '
        {
            pnd_Transaction_Data.setValue(ldaMihl0170.getCwf_Who_Support_Tbl_Transaction_Data());                                                                         //Natural: ASSIGN #TRANSACTION-DATA := CWF-WHO-SUPPORT-TBL.TRANSACTION-DATA
            pnd_Chnge_Dte_Tme_Log_Dte_Tme_Key_Pnd_Last_Chnge_Dte_Tme.compute(new ComputeParameters(false, pnd_Chnge_Dte_Tme_Log_Dte_Tme_Key_Pnd_Last_Chnge_Dte_Tme),      //Natural: ASSIGN #CHNGE-DTE-TME-LOG-DTE-TME-KEY.#LAST-CHNGE-DTE-TME := #TRANSACTION-DATA.#LAST-CHNGE-DTE-TME + 1
                pnd_Transaction_Data_Pnd_Last_Chnge_Dte_Tme.add(1));
            //* (NOT RESTARTING)
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Chnge_Dte_Tme_Log_Dte_Tme_Key_Pnd_Last_Chnge_Dte_Tme.setValue(pnd_Time_Window_Last_Change_Date_Time_Pnd_Starting_Numeric);                                //Natural: ASSIGN #CHNGE-DTE-TME-LOG-DTE-TME-KEY.#LAST-CHNGE-DTE-TME := #TIME-WINDOW-LAST-CHANGE-DATE-TIME.#STARTING-NUMERIC
            //* (2580)
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Chnge_Dte_Tme_Log_Dte_Tme_Key_Pnd_Rqst_Log_Dte_Tme.reset();                                                                                                   //Natural: RESET #CHNGE-DTE-TME-LOG-DTE-TME-KEY.#RQST-LOG-DTE-TME
        pdaMiha0170.getMiha0170_Input_Control_Start_Time().setValue(pnd_Time_Window_Last_Change_Date_Time_Pnd_Starting_Numeric);                                          //Natural: ASSIGN MIHA0170-INPUT.CONTROL-START-TIME := #STARTING-NUMERIC
        pdaMiha0170.getMiha0170_Input_Control_End_Time().setValue(pnd_Time_Window_Last_Change_Date_Time_Pnd_Ending_Numeric);                                              //Natural: ASSIGN MIHA0170-INPUT.CONTROL-END-TIME := #ENDING-NUMERIC
        vw_cwf_Master_Index_View.startDatabaseRead                                                                                                                        //Natural: READ CWF-MASTER-INDEX-VIEW WITH CHNGE-DTE-TME-LOG-DTE-TME-KEY = #CHNGE-DTE-TME-LOG-DTE-TME-KEY
        (
        "MIT_READ",
        new Wc[] { new Wc("CHNGE_DTE_TME_LOG_DTE_TME_KEY", ">=", pnd_Chnge_Dte_Tme_Log_Dte_Tme_Key, WcType.BY) },
        new Oc[] { new Oc("CHNGE_DTE_TME_LOG_DTE_TME_KEY", "ASC") }
        );
        MIT_READ:
        while (condition(vw_cwf_Master_Index_View.readNextRow("MIT_READ")))
        {
            if (condition(cwf_Master_Index_View_Last_Chnge_Dte_Tme.greater(pnd_Time_Window_Last_Change_Date_Time_Pnd_Ending_Numeric)))                                    //Natural: IF CWF-MASTER-INDEX-VIEW.LAST-CHNGE-DTE-TME GT #TIME-WINDOW-LAST-CHANGE-DATE-TIME.#ENDING-NUMERIC
            {
                                                                                                                                                                          //Natural: PERFORM STATISTIC-REPORT
                sub_Statistic_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("MIT_READ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("MIT_READ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) break MIT_READ;                                                                                                                                 //Natural: ESCAPE BOTTOM ( MIT-READ. )
                //* (2790)
            }                                                                                                                                                             //Natural: END-IF
            pnd_Mit_Read.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #MIT-READ
            //*  ---------------------------- ADDED BY LEON 06/18/02
            if (condition(pnd_Mit_Read.equals(1)))                                                                                                                        //Natural: IF #MIT-READ = 1
            {
                pnd_Frst_Rldt.setValue(cwf_Master_Index_View_Rqst_Log_Dte_Tme);                                                                                           //Natural: MOVE CWF-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME TO #FRST-RLDT
                pnd_Frst_Lcdt.setValue(cwf_Master_Index_View_Last_Chnge_Dte_Tme);                                                                                         //Natural: MOVE CWF-MASTER-INDEX-VIEW.LAST-CHNGE-DTE-TME TO #FRST-LCDT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Last_Rldt.setValue(cwf_Master_Index_View_Rqst_Log_Dte_Tme);                                                                                           //Natural: MOVE CWF-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME TO #LAST-RLDT
                pnd_Last_Lcdt.setValue(cwf_Master_Index_View_Last_Chnge_Dte_Tme);                                                                                         //Natural: MOVE CWF-MASTER-INDEX-VIEW.LAST-CHNGE-DTE-TME TO #LAST-LCDT
            }                                                                                                                                                             //Natural: END-IF
            //*  WRITE /'MIHP0300....MIT.READ..RLDT LCDT ISN.'
            //*  CWF-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME
            //*  CWF-MASTER-INDEX-VIEW.LAST-CHNGE-DTE-TME *ISN
            //* *********************                     /* ADDED BY L.E. 07/14/98
            if (condition(cwf_Master_Index_View_Pin_Nbr.equals(getZero())))                                                                                               //Natural: IF CWF-MASTER-INDEX-VIEW.PIN-NBR EQ 0
            {
                //*  DO NOT PROCESS ZERO PINS
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cwf_Master_Index_View_Rescan_Ind.equals("Y")))                                                                                                  //Natural: IF CWF-MASTER-INDEX-VIEW.RESCAN-IND EQ 'Y'
            {
                //*  DO NOT PRCSS RE-SCAN RECS.
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //* *********************
            pnd_Last_Chnge_Dte_Tme_Save.setValue(cwf_Master_Index_View_Last_Chnge_Dte_Tme);                                                                               //Natural: MOVE CWF-MASTER-INDEX-VIEW.LAST-CHNGE-DTE-TME TO #LAST-CHNGE-DTE-TME-SAVE
            //*  IF THE LOG DATE TIME HAS CHANGED IT IS NOT POSSIBLE JUST TO ADD THE
            //*  NEW ACTIVITY.  INSTEAD ALL THE ACTIVITIES FOR THE WORK REQUEST HAVE TO
            //*  BE RECREATED USING MITN0030.
            //*  ----------------------------------------------------------------------
            if (condition(cwf_Master_Index_View_Admin_Status_Cde.equals("MERG")))                                                                                         //Natural: IF CWF-MASTER-INDEX-VIEW.ADMIN-STATUS-CDE = 'MERG'
            {
                pdaMiha0030.getMiha0030_Input_Rqst_Log_Dte_Tme().setValue(cwf_Master_Index_View_Rqst_Log_Dte_Tme);                                                        //Natural: ASSIGN MIHA0030-INPUT.RQST-LOG-DTE-TME := CWF-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME
                pdaMiha0030.getMiha0030_Input_Crtn_Run_Id().setValue(pnd_Run_Id);                                                                                         //Natural: ASSIGN MIHA0030-INPUT.CRTN-RUN-ID := #RUN-ID
                DbsUtil.callnat(Mihn0030.class , getCurrentProcessState(), pdaMiha0030.getMiha0030_Input(), pdaMiha0030.getMiha0030_Output(), pdaCdaobj.getCdaobj(),      //Natural: CALLNAT 'MIHN0030' MIHA0030-INPUT MIHA0030-OUTPUT CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
                    pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
                if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-SUBPROGRAM-ERRORS
                sub_Check_For_Subprogram_Errors();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("MIT_READ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("MIT_READ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Records_Updated_Counter.nadd(pdaMiha0030.getMiha0030_Output_Records_Updated_Counter());                                                               //Natural: ADD MIHA0030-OUTPUT.RECORDS-UPDATED-COUNTER TO #RECORDS-UPDATED-COUNTER
                                                                                                                                                                          //Natural: PERFORM TRANSACTION-CONTROL
                sub_Transaction_Control();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("MIT_READ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("MIT_READ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
                //* (3130)
            }                                                                                                                                                             //Natural: END-IF
            //*  --------------- CHECKING PROCES-TIME-IND  ----------------
            pnd_Skip_C_Stat_Checking.setValue("N");                                                                                                                       //Natural: MOVE 'N' TO #SKIP-C-STAT-CHECKING
            if (condition(cwf_Master_Index_View_Pnd_Process_Time_Ind.equals("Y")))                                                                                        //Natural: IF #PROCESS-TIME-IND = 'Y'
            {
                pnd_Skip_C_Stat_Checking.setValue("Y");                                                                                                                   //Natural: MOVE 'Y' TO #SKIP-C-STAT-CHECKING
                pdaMiha0030.getMiha0030_Input_Rqst_Log_Dte_Tme().setValue(cwf_Master_Index_View_Rqst_Log_Dte_Tme);                                                        //Natural: ASSIGN MIHA0030-INPUT.RQST-LOG-DTE-TME := CWF-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME
                pdaMiha0030.getMiha0030_Input_Crtn_Run_Id().setValue(pnd_Run_Id);                                                                                         //Natural: ASSIGN MIHA0030-INPUT.CRTN-RUN-ID := #RUN-ID
                DbsUtil.callnat(Mihn0030.class , getCurrentProcessState(), pdaMiha0030.getMiha0030_Input(), pdaMiha0030.getMiha0030_Output(), pdaCdaobj.getCdaobj(),      //Natural: CALLNAT 'MIHN0030' MIHA0030-INPUT MIHA0030-OUTPUT CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
                    pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
                if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                //*  -----------
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-SUBPROGRAM-ERRORS
                sub_Check_For_Subprogram_Errors();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("MIT_READ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("MIT_READ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Records_Updated_Counter.nadd(pdaMiha0030.getMiha0030_Output_Records_Updated_Counter());                                                               //Natural: ADD MIHA0030-OUTPUT.RECORDS-UPDATED-COUNTER TO #RECORDS-UPDATED-COUNTER
                pnd_Activity_Counter.nadd(pdaMiha0030.getMiha0030_Output_Records_Updated_Counter());                                                                      //Natural: ADD MIHA0030-OUTPUT.RECORDS-UPDATED-COUNTER TO #ACTIVITY-COUNTER
                                                                                                                                                                          //Natural: PERFORM TRANSACTION-CONTROL
                sub_Transaction_Control();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("MIT_READ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("MIT_READ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  -------- END PROCESS-TIME-IND (STATUS=3200) INQUIRY ---------- *
            //*  **********************************************************************
            //*  ADDED BY L.E. 05/18/98
            //*  IF RECORD CORPORATELY CLOSED AND ADMIN-STATUS-CDE = 'C600'-ADDED DOC.
            //*  IGNORE RECORD COMPLETELY.
            //*  ----------------------------------------------------------------------
            pnd_Admin_Status_Cde_Split.setValue(cwf_Master_Index_View_Admin_Status_Cde);                                                                                  //Natural: MOVE CWF-MASTER-INDEX-VIEW.ADMIN-STATUS-CDE TO #ADMIN-STATUS-CDE-SPLIT
            if (condition(pnd_Skip_C_Stat_Checking.equals("N")))                                                                                                          //Natural: IF #SKIP-C-STAT-CHECKING = 'N'
            {
                if (condition(pnd_Admin_Status_Cde_Split_Pnd_Admin_Status_Cde_Split_1.equals("C")))                                                                       //Natural: IF #ADMIN-STATUS-CDE-SPLIT-1 = 'C'
                {
                    //* (CORPORATELY CLOSED)
                    //* (JUST ADDED DOCUM.)
                    if (condition(cwf_Master_Index_View_Crprte_Status_Ind.equals("9") && cwf_Master_Index_View_Admin_Status_Cde.equals("C600")))                          //Natural: IF CWF-MASTER-INDEX-VIEW.CRPRTE-STATUS-IND = '9' AND CWF-MASTER-INDEX-VIEW.ADMIN-STATUS-CDE EQ 'C600'
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                        //* (3590)
                    }                                                                                                                                                     //Natural: END-IF
                    //*  --------- FOR THE OTHER "C" STATUSES UPDATE WR ONLY ----------
                    //* (CORPORATELY CLOSED)
                    if (condition(cwf_Master_Index_View_Crprte_Status_Ind.equals("9")))                                                                                   //Natural: IF CWF-MASTER-INDEX-VIEW.CRPRTE-STATUS-IND = '9'
                    {
                        pdaMiha0010.getMiha0010().setValuesByName(vw_cwf_Master_Index_View);                                                                              //Natural: MOVE BY NAME CWF-MASTER-INDEX-VIEW TO MIHA0010
                        pdaMiha0010.getMiha0010_Elapsed_Time_Overwrite_Flag().setValue(true);                                                                             //Natural: ASSIGN MIHA0010.ELAPSED-TIME-OVERWRITE-FLAG := TRUE
                        pdaMiha0010.getMiha0010_Strtng_Event_Dte_Tme().reset();                                                                                           //Natural: RESET MIHA0010.STRTNG-EVENT-DTE-TME
                        pdaMiha0010.getMiha0010_Crtn_Run_Id().setValue(pnd_Run_Id);                                                                                       //Natural: ASSIGN MIHA0010.CRTN-RUN-ID := #RUN-ID
                        //*  ---------
                        DbsUtil.callnat(Mihn0010.class , getCurrentProcessState(), pdaMiha0010.getMiha0010(), pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(),    //Natural: CALLNAT 'MIHN0010' MIHA0010 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
                            pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
                        if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-SUBPROGRAM-ERRORS
                        sub_Check_For_Subprogram_Errors();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("MIT_READ"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("MIT_READ"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Records_Updated_Counter.nadd(1);                                                                                                              //Natural: ADD 1 TO #RECORDS-UPDATED-COUNTER
                        pnd_Work_Request_Counter.nadd(1);                                                                                                                 //Natural: ADD 1 TO #WORK-REQUEST-COUNTER
                                                                                                                                                                          //Natural: PERFORM TRANSACTION-CONTROL
                        sub_Transaction_Control();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("MIT_READ"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("MIT_READ"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                        //* (3640)
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                    //* (3580)
                }                                                                                                                                                         //Natural: END-IF
                pdaMiha0020.getMiha0020_Input_General_Rqst_Log_Dte_Tme().setValue(cwf_Master_Index_View_Rqst_Log_Dte_Tme);                                                //Natural: ASSIGN MIHA0020-INPUT-GENERAL.RQST-LOG-DTE-TME := CWF-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME
                pdaMiha0020.getMiha0020_Input_General_Complete_Open_Activity_With_End().setValue(true);                                                                   //Natural: ASSIGN MIHA0020-INPUT-GENERAL.COMPLETE-OPEN-ACTIVITY-WITH-END := TRUE
                //* (IF CORPORATELY CLOSED AND THE
                //* (STATUS IS INSIGNIFICANT, DON't
                //* (CREATE AN OPEN
                if (condition(cwf_Master_Index_View_Crprte_Status_Ind.equals("9") && cwf_Master_Index_View_Actve_Ind.equals("A") && ! (DbsUtil.maskMatches(cwf_Master_Index_View_Admin_Status_Cde, //Natural: IF CWF-MASTER-INDEX-VIEW.CRPRTE-STATUS-IND = '9' AND CWF-MASTER-INDEX-VIEW.ACTVE-IND = 'A' AND CWF-MASTER-INDEX-VIEW.ADMIN-STATUS-CDE NE MASK ( NNNN )
                    "NNNN"))))
                {
                    pdaMiha0020.getMiha0020_Input_General_Create_Incomplete_With_Ending_Ev().setValue(false);                                                             //Natural: ASSIGN MIHA0020-INPUT-GENERAL.CREATE-INCOMPLETE-WITH-ENDING-EV := FALSE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaMiha0020.getMiha0020_Input_General_Create_Incomplete_With_Ending_Ev().setValue(true);                                                              //Natural: ASSIGN MIHA0020-INPUT-GENERAL.CREATE-INCOMPLETE-WITH-ENDING-EV := TRUE
                    //* (3590)
                }                                                                                                                                                         //Natural: END-IF
                pdaMiha0020.getMiha0020_Input_General_Create_Incomplete_With_Start_Ev().setValue(false);                                                                  //Natural: ASSIGN MIHA0020-INPUT-GENERAL.CREATE-INCOMPLETE-WITH-START-EV := FALSE
                pdaMiha0020.getMiha0020_Input_General_Create_Complete_With_Both_Ev().setValue(false);                                                                     //Natural: ASSIGN MIHA0020-INPUT-GENERAL.CREATE-COMPLETE-WITH-BOTH-EV := FALSE
                pdaMiha0020.getMiha0020_Input_General_Check_If_Exists_When_Creating().setValue(true);                                                                     //Natural: ASSIGN MIHA0020-INPUT-GENERAL.CHECK-IF-EXISTS-WHEN-CREATING := TRUE
                pdaMiha0020.getMiha0020_Input_General_Run_Id().setValue(pnd_Run_Id);                                                                                      //Natural: ASSIGN MIHA0020-INPUT-GENERAL.RUN-ID := #RUN-ID
                pdaMiha0020.getMiha0020_Input_Starting_Event().reset();                                                                                                   //Natural: RESET MIHA0020-INPUT-STARTING-EVENT
                pdaMiha0020.getMiha0020_Input_Ending_Event().setValuesByName(vw_cwf_Master_Index_View);                                                                   //Natural: MOVE BY NAME CWF-MASTER-INDEX-VIEW TO MIHA0020-INPUT-ENDING-EVENT
                //*  =====================
                DbsUtil.callnat(Mihn0020.class , getCurrentProcessState(), pdaMiha0020.getMiha0020_Input_General(), pdaMiha0020.getMiha0020_Input_Starting_Event(),       //Natural: CALLNAT 'MIHN0020' MIHA0020-INPUT-GENERAL MIHA0020-INPUT-STARTING-EVENT MIHA0020-INPUT-ENDING-EVENT MIHA0020-OUTPUT CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
                    pdaMiha0020.getMiha0020_Input_Ending_Event(), pdaMiha0020.getMiha0020_Output(), pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), 
                    pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
                if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-SUBPROGRAM-ERRORS
                sub_Check_For_Subprogram_Errors();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("MIT_READ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("MIT_READ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Records_Updated_Counter.nadd(pdaMiha0020.getMiha0020_Output_Records_Updated_Counter());                                                               //Natural: ADD MIHA0020-OUTPUT.RECORDS-UPDATED-COUNTER TO #RECORDS-UPDATED-COUNTER
                pnd_Activity_Counter.nadd(pdaMiha0020.getMiha0020_Output_Records_Updated_Counter());                                                                      //Natural: ADD MIHA0020-OUTPUT.RECORDS-UPDATED-COUNTER TO #ACTIVITY-COUNTER
                //* (3570)
            }                                                                                                                                                             //Natural: END-IF
            //*  CREATE OR UPDATE THE WORK REQUEST
            //*  ---------------------------------
            pdaMiha0010.getMiha0010().setValuesByName(vw_cwf_Master_Index_View);                                                                                          //Natural: MOVE BY NAME CWF-MASTER-INDEX-VIEW TO MIHA0010
            pdaMiha0010.getMiha0010_Elapsed_Time_Overwrite_Flag().setValue(false);                                                                                        //Natural: ASSIGN MIHA0010.ELAPSED-TIME-OVERWRITE-FLAG := FALSE
            pdaMiha0010.getMiha0010_Chnge_Prtcpnt_Elpsd_Clndr_Hours().setValue(pdaMiha0020.getMiha0020_Output_Total_Prtcpnt_Elpsd_Clndr_Hours());                         //Natural: ASSIGN MIHA0010.CHNGE-PRTCPNT-ELPSD-CLNDR-HOURS := MIHA0020-OUTPUT.TOTAL-PRTCPNT-ELPSD-CLNDR-HOURS
            pdaMiha0010.getMiha0010_Chnge_Prtcpnt_Elpsd_Bsnss_Hours().setValue(pdaMiha0020.getMiha0020_Output_Total_Prtcpnt_Elpsd_Bsnss_Hours());                         //Natural: ASSIGN MIHA0010.CHNGE-PRTCPNT-ELPSD-BSNSS-HOURS := MIHA0020-OUTPUT.TOTAL-PRTCPNT-ELPSD-BSNSS-HOURS
            pdaMiha0010.getMiha0010_Chnge_Crprte_Elpsd_Clndr_Hours().setValue(pdaMiha0020.getMiha0020_Output_Total_Crprte_Elpsd_Clndr_Hours());                           //Natural: ASSIGN MIHA0010.CHNGE-CRPRTE-ELPSD-CLNDR-HOURS := MIHA0020-OUTPUT.TOTAL-CRPRTE-ELPSD-CLNDR-HOURS
            pdaMiha0010.getMiha0010_Chnge_Crprte_Elpsd_Bsnss_Hours().setValue(pdaMiha0020.getMiha0020_Output_Total_Crprte_Elpsd_Bsnss_Hours());                           //Natural: ASSIGN MIHA0010.CHNGE-CRPRTE-ELPSD-BSNSS-HOURS := MIHA0020-OUTPUT.TOTAL-CRPRTE-ELPSD-BSNSS-HOURS
            //* (IF NECESSARY IT WILL BE FOUND
            pdaMiha0010.getMiha0010_Strtng_Event_Dte_Tme().reset();                                                                                                       //Natural: RESET MIHA0010.STRTNG-EVENT-DTE-TME
            pdaMiha0010.getMiha0010_Crtn_Run_Id().setValue(pnd_Run_Id);                                                                                                   //Natural: ASSIGN MIHA0010.CRTN-RUN-ID := #RUN-ID
            DbsUtil.callnat(Mihn0010.class , getCurrentProcessState(), pdaMiha0010.getMiha0010(), pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(),                //Natural: CALLNAT 'MIHN0010' MIHA0010 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
                pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
            if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-SUBPROGRAM-ERRORS
            sub_Check_For_Subprogram_Errors();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("MIT_READ"))) break;
                else if (condition(Global.isEscapeBottomImmediate("MIT_READ"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Records_Updated_Counter.nadd(1);                                                                                                                          //Natural: ADD 1 TO #RECORDS-UPDATED-COUNTER
            pnd_Work_Request_Counter.nadd(1);                                                                                                                             //Natural: ADD 1 TO #WORK-REQUEST-COUNTER
            //*  COMMIT THE TRANSACTION IF NECESSARY
            //*  -----------------------------------
                                                                                                                                                                          //Natural: PERFORM TRANSACTION-CONTROL
            sub_Transaction_Control();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("MIT_READ"))) break;
                else if (condition(Global.isEscapeBottomImmediate("MIT_READ"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* (2760)
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* (2380)
    }
    private void sub_Transaction_Control() throws Exception                                                                                                               //Natural: TRANSACTION-CONTROL
    {
        if (BLNatReinput.isReinput()) return;

        //*  ===================================
        if (condition(pnd_Records_Updated_Counter.greater(pnd_Et_Threshold)))                                                                                             //Natural: IF #RECORDS-UPDATED-COUNTER GT #ET-THRESHOLD
        {
            pnd_All_Et.nadd(pnd_Records_Updated_Counter);                                                                                                                 //Natural: ADD #RECORDS-UPDATED-COUNTER TO #ALL-ET
            pnd_Records_Updated_Counter.reset();                                                                                                                          //Natural: RESET #RECORDS-UPDATED-COUNTER
            TRANSACTION_CONTROL_GET:                                                                                                                                      //Natural: GET CWF-WHO-SUPPORT-TBL MIHA0170-OUTPUT.RUN-CONTROL-ISN
            ldaMihl0170.getVw_cwf_Who_Support_Tbl().readByID(pdaMiha0170.getMiha0170_Output_Run_Control_Isn().getLong(), "TRANSACTION_CONTROL_GET");
            pnd_Transaction_Data_Pnd_Last_Chnge_Dte_Tme.setValue(cwf_Master_Index_View_Last_Chnge_Dte_Tme);                                                               //Natural: ASSIGN #TRANSACTION-DATA.#LAST-CHNGE-DTE-TME := CWF-MASTER-INDEX-VIEW.LAST-CHNGE-DTE-TME
            ldaMihl0170.getCwf_Who_Support_Tbl_Transaction_Data().setValue(pnd_Transaction_Data);                                                                         //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.TRANSACTION-DATA := #TRANSACTION-DATA
            ldaMihl0170.getCwf_Who_Support_Tbl_Tbl_Updte_Dte_Tme().setValue(Global.getTIMX());                                                                            //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.TBL-UPDTE-DTE-TME := *TIMX
            ldaMihl0170.getVw_cwf_Who_Support_Tbl().updateDBRow("TRANSACTION_CONTROL_GET");                                                                               //Natural: UPDATE ( TRANSACTION-CONTROL-GET. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            //* (3670)
        }                                                                                                                                                                 //Natural: END-IF
        //* (4490)
    }
    private void sub_Check_For_Subprogram_Errors() throws Exception                                                                                                       //Natural: CHECK-FOR-SUBPROGRAM-ERRORS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ===========================================
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().equals(999)))                                                                                          //Natural: IF MSG-INFO-SUB.##MSG-NR = 999
        {
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
            gdaCwfg000.getMsg_Info().setValuesByName(pdaCwfpda_M.getMsg_Info_Sub());                                                                                      //Natural: MOVE BY NAME MSG-INFO-SUB TO MSG-INFO
            //* (TO RETURN TO THE CALLING PROGRAM)
                                                                                                                                                                          //Natural: PERFORM ESCAPE-PROGRAM
            sub_Escape_Program();
            if (condition(Global.isEscape())) {return;}
            //* (4720)
        }                                                                                                                                                                 //Natural: END-IF
        pdaCwfpda_M.getMsg_Info_Sub().reset();                                                                                                                            //Natural: RESET MSG-INFO-SUB
        //* (4700)
    }
    private void sub_Set_The_Run_Control_Record_To_Complete() throws Exception                                                                                            //Natural: SET-THE-RUN-CONTROL-RECORD-TO-COMPLETE
    {
        if (BLNatReinput.isReinput()) return;

        COMPLETE_RUN_CONTROL_GET:                                                                                                                                         //Natural: GET CWF-WHO-SUPPORT-TBL MIHA0170-OUTPUT.RUN-CONTROL-ISN
        ldaMihl0170.getVw_cwf_Who_Support_Tbl().readByID(pdaMiha0170.getMiha0170_Output_Run_Control_Isn().getLong(), "COMPLETE_RUN_CONTROL_GET");
        ldaMihl0170.getCwf_Who_Support_Tbl_Transaction_Data().reset();                                                                                                    //Natural: RESET CWF-WHO-SUPPORT-TBL.TRANSACTION-DATA
        ldaMihl0170.getCwf_Who_Support_Tbl_Run_Status().setValue(ldaMihl0171.getMihl0171_Completed_Status());                                                             //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.RUN-STATUS := MIHL0171.COMPLETED-STATUS
        ldaMihl0170.getCwf_Who_Support_Tbl_Active_Program().reset();                                                                                                      //Natural: RESET CWF-WHO-SUPPORT-TBL.ACTIVE-PROGRAM
        ldaMihl0170.getCwf_Who_Support_Tbl_Tbl_Updte_Dte_Tme().setValue(Global.getTIMX());                                                                                //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.TBL-UPDTE-DTE-TME := *TIMX
        ldaMihl0170.getCwf_Who_Support_Tbl_Phys_End_Time().setValue(Global.getTIMX());                                                                                    //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.PHYS-END-TIME := *TIMX
        ldaMihl0170.getCwf_Who_Support_Tbl_Tbl_Updte_Oprtr_Cde().setValue(Global.getPROGRAM());                                                                           //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.TBL-UPDTE-OPRTR-CDE := *PROGRAM
        ldaMihl0170.getCwf_Who_Support_Tbl_Asterisk().setValue("*");                                                                                                      //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.ASTERISK := '*'
        ldaMihl0170.getCwf_Who_Support_Tbl_Mit_Op_Cntr().setValue(pdaMiha0170.getMiha0170_Output_Mit_Op_Cntr());                                                          //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.MIT-OP-CNTR := MIHA0170-OUTPUT.MIT-OP-CNTR
        ldaMihl0170.getCwf_Who_Support_Tbl_Mit_Cl_Cntr().setValue(pdaMiha0170.getMiha0170_Output_Mit_Cl_Cntr());                                                          //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.MIT-CL-CNTR := MIHA0170-OUTPUT.MIT-CL-CNTR
        ldaMihl0170.getCwf_Who_Support_Tbl_Wr_Not_Archived().setValue(pdaMiha0170.getMiha0170_Output_Wr_Not_Archived());                                                  //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.WR-NOT-ARCHIVED := MIHA0170-OUTPUT.WR-NOT-ARCHIVED
        ldaMihl0170.getCwf_Who_Support_Tbl_Wr_Archived().setValue(pdaMiha0170.getMiha0170_Output_Wr_Archived());                                                          //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.WR-ARCHIVED := MIHA0170-OUTPUT.WR-ARCHIVED
        ldaMihl0170.getCwf_Who_Support_Tbl_Wr_Corrected().setValue(pdaMiha0170.getMiha0170_Output_Wr_Corrected());                                                        //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.WR-CORRECTED := MIHA0170-OUTPUT.WR-CORRECTED
        //* ***** ADDED BY L.E.
        if (condition(pnd_Last_Chnge_Dte_Tme_Save.greater(" ")))                                                                                                          //Natural: IF #LAST-CHNGE-DTE-TME-SAVE GT ' '
        {
            ldaMihl0170.getCwf_Who_Support_Tbl_Ending_Time().setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),pnd_Last_Chnge_Dte_Tme_Save);                           //Natural: MOVE EDITED #LAST-CHNGE-DTE-TME-SAVE TO ENDING-TIME ( EM = YYYYMMDDHHIISST )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaMihl0170.getCwf_Who_Support_Tbl_Ending_Time().setValue(ldaMihl0170.getCwf_Who_Support_Tbl_Starting_Time());                                                //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.ENDING-TIME := CWF-WHO-SUPPORT-TBL.STARTING-TIME
        }                                                                                                                                                                 //Natural: END-IF
        ldaMihl0170.getVw_cwf_Who_Support_Tbl().updateDBRow("COMPLETE_RUN_CONTROL_GET");                                                                                  //Natural: UPDATE ( COMPLETE-RUN-CONTROL-GET. )
        //*  END TRANSACTION
        //* (4840)
    }
    private void sub_Prevent_Natural_Error_3021() throws Exception                                                                                                        //Natural: PREVENT-NATURAL-ERROR-3021
    {
        if (BLNatReinput.isReinput()) return;

        vw_cwf_Who_Work_Request.createHistogram                                                                                                                           //Natural: HISTOGRAM ( 1 ) CWF-WHO-WORK-REQUEST FOR PIN-RQST-KEY
        (
        "OPEN_WHO_DATABASE",
        "PIN_RQST_KEY",
        1
        );
        OPEN_WHO_DATABASE:
        while (condition(vw_cwf_Who_Work_Request.readNextRow("OPEN_WHO_DATABASE")))
        {
            //* (OPEN-WHO-DATABASE.)
        }                                                                                                                                                                 //Natural: END-HISTOGRAM
        if (Global.isEscape()) return;
        //* (5220)
    }
    private void sub_Statistic_Report() throws Exception                                                                                                                  //Natural: STATISTIC-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //*  ================================
        getReports().write(0, "***** Statistic Report for Archiving Process *****",NEWLINE,"Archiving done for the period of StartingDateTime=",pnd_Time_Window_Last_Change_Date_Time_Pnd_Starting_Alpha, //Natural: WRITE '***** Statistic Report for Archiving Process *****' / 'Archiving done for the period of StartingDateTime=' #TIME-WINDOW-LAST-CHANGE-DATE-TIME.#STARTING-ALPHA / '                                   EndingDateTime=' #TIME-WINDOW-LAST-CHANGE-DATE-TIME.#ENDING-ALPHA / 'Number of MIT records read                       =' #MIT-READ / 'First RequestLogDateTime read = ' #FRST-RLDT / 'First LastChangeDateTime read = ' #FRST-LCDT / 'Last  RequestLogDateTime read = ' #LAST-RLDT / 'Last  LastChangeDateTime read = ' #LAST-LCDT / 'Archiving was finished on Day - Time =' *DATN '-' *TIME / '**************************************************'
            NEWLINE,"                                   EndingDateTime=",pnd_Time_Window_Last_Change_Date_Time_Pnd_Ending_Alpha,NEWLINE,"Number of MIT records read                       =",
            pnd_Mit_Read,NEWLINE,"First RequestLogDateTime read = ",pnd_Frst_Rldt,NEWLINE,"First LastChangeDateTime read = ",pnd_Frst_Lcdt,NEWLINE,"Last  RequestLogDateTime read = ",
            pnd_Last_Rldt,NEWLINE,"Last  LastChangeDateTime read = ",pnd_Last_Lcdt,NEWLINE,"Archiving was finished on Day - Time =",Global.getDATN(),"-",
            Global.getTIME(),NEWLINE,"**************************************************");
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        gdaCwfg000.getMsg_Info_Pnd_Pnd_Msg_Nr().setValue(999);                                                                                                            //Natural: ASSIGN MSG-INFO.##MSG-NR = 999
        gdaCwfg000.getMsg_Info_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Natural Error", Global.getERROR_NR(), "in", Global.getPROGRAM(), "....Line", Global.getERROR_LINE())); //Natural: COMPRESS 'Natural Error' *ERROR-NR 'in' *PROGRAM '....Line' *ERROR-LINE INTO MSG-INFO.##MSG
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        //*  WRITE / MSG-INFO.##MSG TERMINATE 13
        if (condition(true)) return;                                                                                                                                      //Natural: ESCAPE ROUTINE
    };                                                                                                                                                                    //Natural: END-ERROR
}
