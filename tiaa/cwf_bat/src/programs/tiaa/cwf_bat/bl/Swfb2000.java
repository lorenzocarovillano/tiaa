/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:23:47 PM
**        * FROM NATURAL PROGRAM : Swfb2000
************************************************************
**        * FILE NAME            : Swfb2000.java
**        * CLASS NAME           : Swfb2000
**        * INSTANCE NAME        : Swfb2000
************************************************************
************************************************************************
* PROGRAM  : SWFB2000
* SYSTEM   : SURVIVOR BENEFITS
* TITLE    : DATA EXTRACT FOR OFAC SCREENING
* CREATED  : MAR 28, 2003
* FUNCTION : DAILY DATA EXTRACT OF ALL SURVIVORS PERSONAL INFO FOR
*             SCREENING AGAINTS LIST FROM OFAC FOR POSSIBLE ID
*             OF SPECIALLY DESIGNATED NATIONALS, BLOCKED PERSONS, ETC.
* HISTORY
*
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Swfb2000 extends BLNatBase
{
    // Data Areas
    private PdaTwrapart pdaTwrapart;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Sb_Bene;
    private DbsField cwf_Sb_Bene_Rcrd_Type;
    private DbsField cwf_Sb_Bene_Srvvr_Mit_Idntfr;

    private DbsGroup cwf_Sb_Bene__R_Field_1;
    private DbsField cwf_Sb_Bene_Ph_Pin;
    private DbsField cwf_Sb_Bene_Sqn_No;
    private DbsField cwf_Sb_Bene_Srvvr_Type;

    private DbsGroup cwf_Sb_Bene_Srvvr_Nme;
    private DbsField cwf_Sb_Bene_Srvvr_Prfx_Nme;
    private DbsField cwf_Sb_Bene_Srvvr_Last_Nme;
    private DbsField cwf_Sb_Bene_Srvvr_Mddle_Nme;
    private DbsField cwf_Sb_Bene_Srvvr_Frst_Nme;
    private DbsField cwf_Sb_Bene_Srvvr_Sffx_Nme;
    private DbsField cwf_Sb_Bene_Srvvr_Nme_Fr_Frm;
    private DbsField cwf_Sb_Bene_Srvvr_Nme_Fr_Fr2;

    private DbsGroup cwf_Sb_Bene_Srvvr_Addr;
    private DbsField cwf_Sb_Bene_Srvvr_Addr_Line1;
    private DbsField cwf_Sb_Bene_Srvvr_Addr_Line2;
    private DbsField cwf_Sb_Bene_Srvvr_Addr_Line3;
    private DbsField cwf_Sb_Bene_Srvvr_Addr_Line4;
    private DbsField cwf_Sb_Bene_Srvvr_Addr_Line5;
    private DbsField cwf_Sb_Bene_Srvvr_Addr_Line6;
    private DbsField cwf_Sb_Bene_Srvvr_Zip_Cde;
    private DbsField cwf_Sb_Bene_State;
    private DbsField cwf_Sb_Bene_Srvvr_Dob_Dte;
    private DbsField cwf_Sb_Bene_Srvvr_Ssn_Nbr;
    private DbsField cwf_Sb_Bene_Srvvr_Rltnshp;
    private DbsField cwf_Sb_Bene_Last_Updte_Dte_Tme;
    private DbsField cwf_Sb_Bene_Last_Updte_Oprtr_Cde;
    private DbsField cwf_Sb_Bene_Us_Address;
    private DbsField cwf_Sb_Bene_Srvvr_Pin;
    private DbsField cwf_Sb_Bene_Rsdncy_Cde;
    private DbsField cwf_Sb_Bene_Ctzn_Cde;

    private DataAccessProgramView vw_cwf_Sb_Cont_Ent;
    private DbsField cwf_Sb_Cont_Ent_Srvvr_Mit_Idntfr;

    private DbsGroup cwf_Sb_Cont_Ent__R_Field_2;
    private DbsField cwf_Sb_Cont_Ent_Ph_Pin;
    private DbsField cwf_Sb_Cont_Ent_Sqn_No;
    private DbsField cwf_Sb_Cont_Ent_Lob;
    private DbsField cwf_Sb_Cont_Ent_Tiaa_Cntrct;
    private DbsField cwf_Sb_Cont_Ent_Rl_Estte_Cntrct;
    private DbsField cwf_Sb_Cont_Ent_Cref_Cert;
    private DbsField cwf_Sb_Cont_Ent_Last_Updte_Dte_Tme;
    private DbsField cwf_Sb_Cont_Ent_Last_Updte_Oprtr_Cde;

    private DataAccessProgramView vw_iaa_Cntrct_Prtcpnt_Role;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde;

    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte;
    private DbsField iaa_Cntrct_Cntrct_Type_Cde;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dod_Dte;

    private DbsGroup iaa_Cntrct__R_Field_3;
    private DbsField iaa_Cntrct_First_Annt_Dod_Yyyy;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte;

    private DbsGroup iaa_Cntrct__R_Field_4;
    private DbsField iaa_Cntrct_Scnd_Annt_Dod_Yyyy;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Ssn;
    private DbsField iaa_Cntrct_Cntrct_Div_Payee_Cde;
    private DbsField iaa_Cntrct_Cntrct_Div_Coll_Cde;
    private DbsField iaa_Cntrct_Cntrct_Inst_Iss_Cde;
    private DbsField iaa_Cntrct_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_Cntrct_Type;

    private DbsGroup pnd_Wfile;
    private DbsField pnd_Wfile_Srvvr_Pin;
    private DbsField pnd_Wfile_Srvvr_Ssn_Nbr;
    private DbsField pnd_Wfile_Srvvr_Dob_Dte;
    private DbsField pnd_Wfile_Lob;
    private DbsField pnd_Wfile_Tiaa_Cntrct;
    private DbsField pnd_Wfile_Cntrct_Stat_Cde;
    private DbsField pnd_Wfile_Cntrct_Stat_Dte;
    private DbsField pnd_Wfile_Srvvr_Nme_Fr_Frm;
    private DbsField pnd_Wfile_Srvvr_Addr_Line1;
    private DbsField pnd_Wfile_Srvvr_Addr_Line2;
    private DbsField pnd_Wfile_Srvvr_Addr_Line3;
    private DbsField pnd_Wfile_Srvvr_Addr_Line4_5;

    private DbsGroup pnd_Wfile__R_Field_5;
    private DbsField pnd_Wfile_Srvvr_Addr_Line4;
    private DbsField pnd_Wfile_Srvvr_Addr_Line5;
    private DbsField pnd_Wfile_Srvvr_Zip_Cde;
    private DbsField pnd_Wfile_Add_Ctry;
    private DbsField pnd_Wfile_Add_Stat_Cde;
    private DbsField pnd_Wfile_Filler;
    private DbsField pnd_Wfile_X_Field;
    private DbsField pnd_Srvvr_Key;

    private DbsGroup pnd_Srvvr_Key__R_Field_6;
    private DbsField pnd_Srvvr_Key_Rcrd_Type;
    private DbsField pnd_Srvvr_Key_Srvvr_Mit_Idntfr;

    private DbsGroup pnd_Srvvr_Cntrct_Key_Grp;
    private DbsField pnd_Srvvr_Cntrct_Key_Grp_Pnd_Srvvr_Cntrct_Key;

    private DbsGroup pnd_Srvvr_Cntrct_Key_Grp__R_Field_7;
    private DbsField pnd_Srvvr_Cntrct_Key_Grp_Srvvr_Mit_Idntfr;
    private DbsField pnd_Srvvr_Cntrct_Key_Grp_Tiaa_Cntrct;
    private DbsField pnd_Ctr_Read;
    private DbsField pnd_Ctr_Xtrctd;
    private DbsField pnd_Found;
    private DbsField pnd_Country_Name;
    private DbsField pnd_Crrnt_Dte;

    private DbsGroup pnd_Crrnt_Dte__R_Field_8;
    private DbsField pnd_Crrnt_Dte_Pnd_Year;
    private DbsField pnd_Crrnt_Dte_Pnd_Month;
    private DbsField pnd_Crrnt_Dte_Pnd_Day;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwrapart = new PdaTwrapart(localVariables);

        // Local Variables

        vw_cwf_Sb_Bene = new DataAccessProgramView(new NameInfo("vw_cwf_Sb_Bene", "CWF-SB-BENE"), "CWF_SB_BENE", "CWF_SB_BENE");
        cwf_Sb_Bene_Rcrd_Type = vw_cwf_Sb_Bene.getRecord().newFieldInGroup("cwf_Sb_Bene_Rcrd_Type", "RCRD-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RCRD_TYPE");
        cwf_Sb_Bene_Rcrd_Type.setDdmHeader("RECORD/TYPE");
        cwf_Sb_Bene_Srvvr_Mit_Idntfr = vw_cwf_Sb_Bene.getRecord().newFieldInGroup("cwf_Sb_Bene_Srvvr_Mit_Idntfr", "SRVVR-MIT-IDNTFR", FieldType.STRING, 
            14, RepeatingFieldStrategy.None, "SRVVR_MIT_IDNTFR");
        cwf_Sb_Bene_Srvvr_Mit_Idntfr.setDdmHeader("SURVIVOR MIT/IDENTIFIER");

        cwf_Sb_Bene__R_Field_1 = vw_cwf_Sb_Bene.getRecord().newGroupInGroup("cwf_Sb_Bene__R_Field_1", "REDEFINE", cwf_Sb_Bene_Srvvr_Mit_Idntfr);
        cwf_Sb_Bene_Ph_Pin = cwf_Sb_Bene__R_Field_1.newFieldInGroup("cwf_Sb_Bene_Ph_Pin", "PH-PIN", FieldType.STRING, 12);
        cwf_Sb_Bene_Sqn_No = cwf_Sb_Bene__R_Field_1.newFieldInGroup("cwf_Sb_Bene_Sqn_No", "SQN-NO", FieldType.NUMERIC, 2);
        cwf_Sb_Bene_Srvvr_Type = vw_cwf_Sb_Bene.getRecord().newFieldInGroup("cwf_Sb_Bene_Srvvr_Type", "SRVVR-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "SRVVR_TYPE");
        cwf_Sb_Bene_Srvvr_Type.setDdmHeader("SURVIVOR/TYPE");

        cwf_Sb_Bene_Srvvr_Nme = vw_cwf_Sb_Bene.getRecord().newGroupInGroup("CWF_SB_BENE_SRVVR_NME", "SRVVR-NME");
        cwf_Sb_Bene_Srvvr_Nme.setDdmHeader("SURVIVOR/NAME");
        cwf_Sb_Bene_Srvvr_Prfx_Nme = cwf_Sb_Bene_Srvvr_Nme.newFieldInGroup("cwf_Sb_Bene_Srvvr_Prfx_Nme", "SRVVR-PRFX-NME", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "SRVVR_PRFX_NME");
        cwf_Sb_Bene_Srvvr_Prfx_Nme.setDdmHeader("SURVIVOR/PREFIX");
        cwf_Sb_Bene_Srvvr_Last_Nme = cwf_Sb_Bene_Srvvr_Nme.newFieldInGroup("cwf_Sb_Bene_Srvvr_Last_Nme", "SRVVR-LAST-NME", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "SRVVR_LAST_NME");
        cwf_Sb_Bene_Srvvr_Last_Nme.setDdmHeader("SURVIVOR/LAST NME");
        cwf_Sb_Bene_Srvvr_Mddle_Nme = cwf_Sb_Bene_Srvvr_Nme.newFieldInGroup("cwf_Sb_Bene_Srvvr_Mddle_Nme", "SRVVR-MDDLE-NME", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "SRVVR_MDDLE_NME");
        cwf_Sb_Bene_Srvvr_Mddle_Nme.setDdmHeader("SURVIVOR/MIDDLE NME");
        cwf_Sb_Bene_Srvvr_Frst_Nme = cwf_Sb_Bene_Srvvr_Nme.newFieldInGroup("cwf_Sb_Bene_Srvvr_Frst_Nme", "SRVVR-FRST-NME", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "SRVVR_FRST_NME");
        cwf_Sb_Bene_Srvvr_Frst_Nme.setDdmHeader("SURVIVOR/FIRST NME");
        cwf_Sb_Bene_Srvvr_Sffx_Nme = cwf_Sb_Bene_Srvvr_Nme.newFieldInGroup("cwf_Sb_Bene_Srvvr_Sffx_Nme", "SRVVR-SFFX-NME", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "SRVVR_SFFX_NME");
        cwf_Sb_Bene_Srvvr_Sffx_Nme.setDdmHeader("SURVIVOR/SUFFIX");
        cwf_Sb_Bene_Srvvr_Nme_Fr_Frm = vw_cwf_Sb_Bene.getRecord().newFieldInGroup("cwf_Sb_Bene_Srvvr_Nme_Fr_Frm", "SRVVR-NME-FR-FRM", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "SRVVR_NME_FR_FRM");
        cwf_Sb_Bene_Srvvr_Nme_Fr_Frm.setDdmHeader("SURVIVOR NME/FREE FORM");
        cwf_Sb_Bene_Srvvr_Nme_Fr_Fr2 = vw_cwf_Sb_Bene.getRecord().newFieldInGroup("cwf_Sb_Bene_Srvvr_Nme_Fr_Fr2", "SRVVR-NME-FR-FR2", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "SRVVR_NME_FR_FR2");

        cwf_Sb_Bene_Srvvr_Addr = vw_cwf_Sb_Bene.getRecord().newGroupInGroup("CWF_SB_BENE_SRVVR_ADDR", "SRVVR-ADDR");
        cwf_Sb_Bene_Srvvr_Addr.setDdmHeader("SURVIVOR/ADDRESS");
        cwf_Sb_Bene_Srvvr_Addr_Line1 = cwf_Sb_Bene_Srvvr_Addr.newFieldInGroup("cwf_Sb_Bene_Srvvr_Addr_Line1", "SRVVR-ADDR-LINE1", FieldType.STRING, 35, 
            RepeatingFieldStrategy.None, "SRVVR_ADDR_LINE1");
        cwf_Sb_Bene_Srvvr_Addr_Line1.setDdmHeader("SURVIVOR/ADDR LINE1");
        cwf_Sb_Bene_Srvvr_Addr_Line2 = cwf_Sb_Bene_Srvvr_Addr.newFieldInGroup("cwf_Sb_Bene_Srvvr_Addr_Line2", "SRVVR-ADDR-LINE2", FieldType.STRING, 35, 
            RepeatingFieldStrategy.None, "SRVVR_ADDR_LINE2");
        cwf_Sb_Bene_Srvvr_Addr_Line2.setDdmHeader("SURVIVOR/ADDR LINE2");
        cwf_Sb_Bene_Srvvr_Addr_Line3 = cwf_Sb_Bene_Srvvr_Addr.newFieldInGroup("cwf_Sb_Bene_Srvvr_Addr_Line3", "SRVVR-ADDR-LINE3", FieldType.STRING, 35, 
            RepeatingFieldStrategy.None, "SRVVR_ADDR_LINE3");
        cwf_Sb_Bene_Srvvr_Addr_Line3.setDdmHeader("SURVIVOR/ADDR LINE3");
        cwf_Sb_Bene_Srvvr_Addr_Line4 = cwf_Sb_Bene_Srvvr_Addr.newFieldInGroup("cwf_Sb_Bene_Srvvr_Addr_Line4", "SRVVR-ADDR-LINE4", FieldType.STRING, 35, 
            RepeatingFieldStrategy.None, "SRVVR_ADDR_LINE4");
        cwf_Sb_Bene_Srvvr_Addr_Line4.setDdmHeader("SURVIVOR/ADDR LINE4");
        cwf_Sb_Bene_Srvvr_Addr_Line5 = cwf_Sb_Bene_Srvvr_Addr.newFieldInGroup("cwf_Sb_Bene_Srvvr_Addr_Line5", "SRVVR-ADDR-LINE5", FieldType.STRING, 35, 
            RepeatingFieldStrategy.None, "SRVVR_ADDR_LINE5");
        cwf_Sb_Bene_Srvvr_Addr_Line5.setDdmHeader("SURVIVOR/ADDR LINE5");
        cwf_Sb_Bene_Srvvr_Addr_Line6 = cwf_Sb_Bene_Srvvr_Addr.newFieldInGroup("cwf_Sb_Bene_Srvvr_Addr_Line6", "SRVVR-ADDR-LINE6", FieldType.STRING, 35, 
            RepeatingFieldStrategy.None, "SRVVR_ADDR_LINE6");
        cwf_Sb_Bene_Srvvr_Addr_Line6.setDdmHeader("SURVIVOR/ADDR LINE6");
        cwf_Sb_Bene_Srvvr_Zip_Cde = cwf_Sb_Bene_Srvvr_Addr.newFieldInGroup("cwf_Sb_Bene_Srvvr_Zip_Cde", "SRVVR-ZIP-CDE", FieldType.STRING, 9, RepeatingFieldStrategy.None, 
            "SRVVR_ZIP_CDE");
        cwf_Sb_Bene_Srvvr_Zip_Cde.setDdmHeader("SURVIVOR/ZIP CODE");
        cwf_Sb_Bene_State = cwf_Sb_Bene_Srvvr_Addr.newFieldInGroup("cwf_Sb_Bene_State", "STATE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "STATE");
        cwf_Sb_Bene_State.setDdmHeader("SURVIVOR/RES CODE");
        cwf_Sb_Bene_Srvvr_Dob_Dte = vw_cwf_Sb_Bene.getRecord().newFieldInGroup("cwf_Sb_Bene_Srvvr_Dob_Dte", "SRVVR-DOB-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "SRVVR_DOB_DTE");
        cwf_Sb_Bene_Srvvr_Dob_Dte.setDdmHeader("SURVIVOR/DOB");
        cwf_Sb_Bene_Srvvr_Ssn_Nbr = vw_cwf_Sb_Bene.getRecord().newFieldInGroup("cwf_Sb_Bene_Srvvr_Ssn_Nbr", "SRVVR-SSN-NBR", FieldType.STRING, 9, RepeatingFieldStrategy.None, 
            "SRVVR_SSN_NBR");
        cwf_Sb_Bene_Srvvr_Ssn_Nbr.setDdmHeader("SURVIVOR/SSN");
        cwf_Sb_Bene_Srvvr_Rltnshp = vw_cwf_Sb_Bene.getRecord().newFieldInGroup("cwf_Sb_Bene_Srvvr_Rltnshp", "SRVVR-RLTNSHP", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "SRVVR_RLTNSHP");
        cwf_Sb_Bene_Srvvr_Rltnshp.setDdmHeader("SURVIVOR/RELATIONSHIP");
        cwf_Sb_Bene_Last_Updte_Dte_Tme = vw_cwf_Sb_Bene.getRecord().newFieldInGroup("cwf_Sb_Bene_Last_Updte_Dte_Tme", "LAST-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LAST_UPDTE_DTE_TME");
        cwf_Sb_Bene_Last_Updte_Dte_Tme.setDdmHeader("LAST UPDATE/DATE/TIME");
        cwf_Sb_Bene_Last_Updte_Oprtr_Cde = vw_cwf_Sb_Bene.getRecord().newFieldInGroup("cwf_Sb_Bene_Last_Updte_Oprtr_Cde", "LAST-UPDTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "LAST_UPDTE_OPRTR_CDE");
        cwf_Sb_Bene_Last_Updte_Oprtr_Cde.setDdmHeader("LAST UPDATE/RACF-ID");
        cwf_Sb_Bene_Us_Address = vw_cwf_Sb_Bene.getRecord().newFieldInGroup("cwf_Sb_Bene_Us_Address", "US-ADDRESS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "US_ADDRESS");
        cwf_Sb_Bene_Srvvr_Pin = vw_cwf_Sb_Bene.getRecord().newFieldInGroup("cwf_Sb_Bene_Srvvr_Pin", "SRVVR-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "SRVVR_PIN");
        cwf_Sb_Bene_Rsdncy_Cde = vw_cwf_Sb_Bene.getRecord().newFieldInGroup("cwf_Sb_Bene_Rsdncy_Cde", "RSDNCY-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "RSDNCY_CDE");
        cwf_Sb_Bene_Ctzn_Cde = vw_cwf_Sb_Bene.getRecord().newFieldInGroup("cwf_Sb_Bene_Ctzn_Cde", "CTZN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CTZN_CDE");
        registerRecord(vw_cwf_Sb_Bene);

        vw_cwf_Sb_Cont_Ent = new DataAccessProgramView(new NameInfo("vw_cwf_Sb_Cont_Ent", "CWF-SB-CONT-ENT"), "CWF_SB_CONT_ENT", "CWF_SB_CONT_ENT");
        cwf_Sb_Cont_Ent_Srvvr_Mit_Idntfr = vw_cwf_Sb_Cont_Ent.getRecord().newFieldInGroup("cwf_Sb_Cont_Ent_Srvvr_Mit_Idntfr", "SRVVR-MIT-IDNTFR", FieldType.STRING, 
            14, RepeatingFieldStrategy.None, "SRVVR_MIT_IDNTFR");
        cwf_Sb_Cont_Ent_Srvvr_Mit_Idntfr.setDdmHeader("SURVIVOR MIT/IDENTIFIER");

        cwf_Sb_Cont_Ent__R_Field_2 = vw_cwf_Sb_Cont_Ent.getRecord().newGroupInGroup("cwf_Sb_Cont_Ent__R_Field_2", "REDEFINE", cwf_Sb_Cont_Ent_Srvvr_Mit_Idntfr);
        cwf_Sb_Cont_Ent_Ph_Pin = cwf_Sb_Cont_Ent__R_Field_2.newFieldInGroup("cwf_Sb_Cont_Ent_Ph_Pin", "PH-PIN", FieldType.STRING, 12);
        cwf_Sb_Cont_Ent_Sqn_No = cwf_Sb_Cont_Ent__R_Field_2.newFieldInGroup("cwf_Sb_Cont_Ent_Sqn_No", "SQN-NO", FieldType.NUMERIC, 2);
        cwf_Sb_Cont_Ent_Lob = vw_cwf_Sb_Cont_Ent.getRecord().newFieldInGroup("cwf_Sb_Cont_Ent_Lob", "LOB", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "LOB");
        cwf_Sb_Cont_Ent_Lob.setDdmHeader("LINE OF/BUSINESS");
        cwf_Sb_Cont_Ent_Tiaa_Cntrct = vw_cwf_Sb_Cont_Ent.getRecord().newFieldInGroup("cwf_Sb_Cont_Ent_Tiaa_Cntrct", "TIAA-CNTRCT", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "TIAA_CNTRCT");
        cwf_Sb_Cont_Ent_Tiaa_Cntrct.setDdmHeader("TIAA/CONTRACT");
        cwf_Sb_Cont_Ent_Rl_Estte_Cntrct = vw_cwf_Sb_Cont_Ent.getRecord().newFieldInGroup("cwf_Sb_Cont_Ent_Rl_Estte_Cntrct", "RL-ESTTE-CNTRCT", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "RL_ESTTE_CNTRCT");
        cwf_Sb_Cont_Ent_Rl_Estte_Cntrct.setDdmHeader("REAL ESTATE/CONTRACT");
        cwf_Sb_Cont_Ent_Cref_Cert = vw_cwf_Sb_Cont_Ent.getRecord().newFieldInGroup("cwf_Sb_Cont_Ent_Cref_Cert", "CREF-CERT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CREF_CERT");
        cwf_Sb_Cont_Ent_Cref_Cert.setDdmHeader("CREF/CERTIFICATE");
        cwf_Sb_Cont_Ent_Last_Updte_Dte_Tme = vw_cwf_Sb_Cont_Ent.getRecord().newFieldInGroup("cwf_Sb_Cont_Ent_Last_Updte_Dte_Tme", "LAST-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LAST_UPDTE_DTE_TME");
        cwf_Sb_Cont_Ent_Last_Updte_Dte_Tme.setDdmHeader("LAST UPDATE/DATE/TIME");
        cwf_Sb_Cont_Ent_Last_Updte_Oprtr_Cde = vw_cwf_Sb_Cont_Ent.getRecord().newFieldInGroup("cwf_Sb_Cont_Ent_Last_Updte_Oprtr_Cde", "LAST-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_UPDTE_OPRTR_CDE");
        cwf_Sb_Cont_Ent_Last_Updte_Oprtr_Cde.setDdmHeader("LAST UPDATE/OPERATOR CODE");
        registerRecord(vw_cwf_Sb_Cont_Ent);

        vw_iaa_Cntrct_Prtcpnt_Role = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Prtcpnt_Role", "IAA-CNTRCT-PRTCPNT-ROLE"), "IAA_CNTRCT_PRTCPNT_ROLE", 
            "IA_CONTRACT_PART");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr", 
            "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde", 
            "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr", "CPR-ID-NBR", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde", 
            "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        registerRecord(vw_iaa_Cntrct_Prtcpnt_Role);

        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_Optn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Cntrct_Orgn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_Cntrct_Issue_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        iaa_Cntrct_Cntrct_Type_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_TYPE_CDE");
        iaa_Cntrct_Cntrct_First_Annt_Dob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        iaa_Cntrct_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");

        iaa_Cntrct__R_Field_3 = vw_iaa_Cntrct.getRecord().newGroupInGroup("iaa_Cntrct__R_Field_3", "REDEFINE", iaa_Cntrct_Cntrct_First_Annt_Dod_Dte);
        iaa_Cntrct_First_Annt_Dod_Yyyy = iaa_Cntrct__R_Field_3.newFieldInGroup("iaa_Cntrct_First_Annt_Dod_Yyyy", "FIRST-ANNT-DOD-YYYY", FieldType.NUMERIC, 
            4);
        iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");

        iaa_Cntrct__R_Field_4 = vw_iaa_Cntrct.getRecord().newGroupInGroup("iaa_Cntrct__R_Field_4", "REDEFINE", iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte);
        iaa_Cntrct_Scnd_Annt_Dod_Yyyy = iaa_Cntrct__R_Field_4.newFieldInGroup("iaa_Cntrct_Scnd_Annt_Dod_Yyyy", "SCND-ANNT-DOD-YYYY", FieldType.NUMERIC, 
            4);
        iaa_Cntrct_Cntrct_Scnd_Annt_Ssn = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Ssn", "CNTRCT-SCND-ANNT-SSN", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SSN");
        iaa_Cntrct_Cntrct_Div_Payee_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Div_Payee_Cde", "CNTRCT-DIV-PAYEE-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_DIV_PAYEE_CDE");
        iaa_Cntrct_Cntrct_Div_Coll_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Div_Coll_Cde", "CNTRCT-DIV-COLL-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "CNTRCT_DIV_COLL_CDE");
        iaa_Cntrct_Cntrct_Inst_Iss_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Inst_Iss_Cde", "CNTRCT-INST-ISS-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "CNTRCT_INST_ISS_CDE");
        iaa_Cntrct_Lst_Trans_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        iaa_Cntrct_Cntrct_Type = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Type", "CNTRCT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_TYPE");
        registerRecord(vw_iaa_Cntrct);

        pnd_Wfile = localVariables.newGroupInRecord("pnd_Wfile", "#WFILE");
        pnd_Wfile_Srvvr_Pin = pnd_Wfile.newFieldInGroup("pnd_Wfile_Srvvr_Pin", "SRVVR-PIN", FieldType.STRING, 17);
        pnd_Wfile_Srvvr_Ssn_Nbr = pnd_Wfile.newFieldInGroup("pnd_Wfile_Srvvr_Ssn_Nbr", "SRVVR-SSN-NBR", FieldType.STRING, 9);
        pnd_Wfile_Srvvr_Dob_Dte = pnd_Wfile.newFieldInGroup("pnd_Wfile_Srvvr_Dob_Dte", "SRVVR-DOB-DTE", FieldType.NUMERIC, 8);
        pnd_Wfile_Lob = pnd_Wfile.newFieldInGroup("pnd_Wfile_Lob", "LOB", FieldType.STRING, 1);
        pnd_Wfile_Tiaa_Cntrct = pnd_Wfile.newFieldInGroup("pnd_Wfile_Tiaa_Cntrct", "TIAA-CNTRCT", FieldType.STRING, 10);
        pnd_Wfile_Cntrct_Stat_Cde = pnd_Wfile.newFieldInGroup("pnd_Wfile_Cntrct_Stat_Cde", "CNTRCT-STAT-CDE", FieldType.STRING, 1);
        pnd_Wfile_Cntrct_Stat_Dte = pnd_Wfile.newFieldInGroup("pnd_Wfile_Cntrct_Stat_Dte", "CNTRCT-STAT-DTE", FieldType.NUMERIC, 4);
        pnd_Wfile_Srvvr_Nme_Fr_Frm = pnd_Wfile.newFieldInGroup("pnd_Wfile_Srvvr_Nme_Fr_Frm", "SRVVR-NME-FR-FRM", FieldType.STRING, 35);
        pnd_Wfile_Srvvr_Addr_Line1 = pnd_Wfile.newFieldInGroup("pnd_Wfile_Srvvr_Addr_Line1", "SRVVR-ADDR-LINE1", FieldType.STRING, 35);
        pnd_Wfile_Srvvr_Addr_Line2 = pnd_Wfile.newFieldInGroup("pnd_Wfile_Srvvr_Addr_Line2", "SRVVR-ADDR-LINE2", FieldType.STRING, 35);
        pnd_Wfile_Srvvr_Addr_Line3 = pnd_Wfile.newFieldInGroup("pnd_Wfile_Srvvr_Addr_Line3", "SRVVR-ADDR-LINE3", FieldType.STRING, 35);
        pnd_Wfile_Srvvr_Addr_Line4_5 = pnd_Wfile.newFieldInGroup("pnd_Wfile_Srvvr_Addr_Line4_5", "SRVVR-ADDR-LINE4-5", FieldType.STRING, 70);

        pnd_Wfile__R_Field_5 = pnd_Wfile.newGroupInGroup("pnd_Wfile__R_Field_5", "REDEFINE", pnd_Wfile_Srvvr_Addr_Line4_5);
        pnd_Wfile_Srvvr_Addr_Line4 = pnd_Wfile__R_Field_5.newFieldInGroup("pnd_Wfile_Srvvr_Addr_Line4", "SRVVR-ADDR-LINE4", FieldType.STRING, 35);
        pnd_Wfile_Srvvr_Addr_Line5 = pnd_Wfile__R_Field_5.newFieldInGroup("pnd_Wfile_Srvvr_Addr_Line5", "SRVVR-ADDR-LINE5", FieldType.STRING, 35);
        pnd_Wfile_Srvvr_Zip_Cde = pnd_Wfile.newFieldInGroup("pnd_Wfile_Srvvr_Zip_Cde", "SRVVR-ZIP-CDE", FieldType.STRING, 9);
        pnd_Wfile_Add_Ctry = pnd_Wfile.newFieldInGroup("pnd_Wfile_Add_Ctry", "ADD-CTRY", FieldType.STRING, 35);
        pnd_Wfile_Add_Stat_Cde = pnd_Wfile.newFieldInGroup("pnd_Wfile_Add_Stat_Cde", "ADD-STAT-CDE", FieldType.STRING, 1);
        pnd_Wfile_Filler = pnd_Wfile.newFieldInGroup("pnd_Wfile_Filler", "FILLER", FieldType.STRING, 94);
        pnd_Wfile_X_Field = pnd_Wfile.newFieldInGroup("pnd_Wfile_X_Field", "X-FIELD", FieldType.STRING, 1);
        pnd_Srvvr_Key = localVariables.newFieldInRecord("pnd_Srvvr_Key", "#SRVVR-KEY", FieldType.STRING, 10);

        pnd_Srvvr_Key__R_Field_6 = localVariables.newGroupInRecord("pnd_Srvvr_Key__R_Field_6", "REDEFINE", pnd_Srvvr_Key);
        pnd_Srvvr_Key_Rcrd_Type = pnd_Srvvr_Key__R_Field_6.newFieldInGroup("pnd_Srvvr_Key_Rcrd_Type", "RCRD-TYPE", FieldType.STRING, 1);
        pnd_Srvvr_Key_Srvvr_Mit_Idntfr = pnd_Srvvr_Key__R_Field_6.newFieldInGroup("pnd_Srvvr_Key_Srvvr_Mit_Idntfr", "SRVVR-MIT-IDNTFR", FieldType.STRING, 
            9);

        pnd_Srvvr_Cntrct_Key_Grp = localVariables.newGroupInRecord("pnd_Srvvr_Cntrct_Key_Grp", "#SRVVR-CNTRCT-KEY-GRP");
        pnd_Srvvr_Cntrct_Key_Grp_Pnd_Srvvr_Cntrct_Key = pnd_Srvvr_Cntrct_Key_Grp.newFieldInGroup("pnd_Srvvr_Cntrct_Key_Grp_Pnd_Srvvr_Cntrct_Key", "#SRVVR-CNTRCT-KEY", 
            FieldType.STRING, 24);

        pnd_Srvvr_Cntrct_Key_Grp__R_Field_7 = pnd_Srvvr_Cntrct_Key_Grp.newGroupInGroup("pnd_Srvvr_Cntrct_Key_Grp__R_Field_7", "REDEFINE", pnd_Srvvr_Cntrct_Key_Grp_Pnd_Srvvr_Cntrct_Key);
        pnd_Srvvr_Cntrct_Key_Grp_Srvvr_Mit_Idntfr = pnd_Srvvr_Cntrct_Key_Grp__R_Field_7.newFieldInGroup("pnd_Srvvr_Cntrct_Key_Grp_Srvvr_Mit_Idntfr", "SRVVR-MIT-IDNTFR", 
            FieldType.STRING, 14);
        pnd_Srvvr_Cntrct_Key_Grp_Tiaa_Cntrct = pnd_Srvvr_Cntrct_Key_Grp__R_Field_7.newFieldInGroup("pnd_Srvvr_Cntrct_Key_Grp_Tiaa_Cntrct", "TIAA-CNTRCT", 
            FieldType.STRING, 10);
        pnd_Ctr_Read = localVariables.newFieldInRecord("pnd_Ctr_Read", "#CTR-READ", FieldType.PACKED_DECIMAL, 9);
        pnd_Ctr_Xtrctd = localVariables.newFieldInRecord("pnd_Ctr_Xtrctd", "#CTR-XTRCTD", FieldType.PACKED_DECIMAL, 9);
        pnd_Found = localVariables.newFieldInRecord("pnd_Found", "#FOUND", FieldType.BOOLEAN, 1);
        pnd_Country_Name = localVariables.newFieldInRecord("pnd_Country_Name", "#COUNTRY-NAME", FieldType.STRING, 5);
        pnd_Crrnt_Dte = localVariables.newFieldInRecord("pnd_Crrnt_Dte", "#CRRNT-DTE", FieldType.NUMERIC, 8);

        pnd_Crrnt_Dte__R_Field_8 = localVariables.newGroupInRecord("pnd_Crrnt_Dte__R_Field_8", "REDEFINE", pnd_Crrnt_Dte);
        pnd_Crrnt_Dte_Pnd_Year = pnd_Crrnt_Dte__R_Field_8.newFieldInGroup("pnd_Crrnt_Dte_Pnd_Year", "#YEAR", FieldType.NUMERIC, 4);
        pnd_Crrnt_Dte_Pnd_Month = pnd_Crrnt_Dte__R_Field_8.newFieldInGroup("pnd_Crrnt_Dte_Pnd_Month", "#MONTH", FieldType.NUMERIC, 2);
        pnd_Crrnt_Dte_Pnd_Day = pnd_Crrnt_Dte__R_Field_8.newFieldInGroup("pnd_Crrnt_Dte_Pnd_Day", "#DAY", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Sb_Bene.reset();
        vw_cwf_Sb_Cont_Ent.reset();
        vw_iaa_Cntrct_Prtcpnt_Role.reset();
        vw_iaa_Cntrct.reset();

        localVariables.reset();
        pnd_Found.setInitialValue(false);
        pnd_Country_Name.setInitialValue("USA");
        pnd_Crrnt_Dte.setInitialValue(Global.getDATN());
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Swfb2000() throws Exception
    {
        super("Swfb2000");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 40 LS = 55
        //*  MAIN LOOP
        vw_cwf_Sb_Bene.startDatabaseRead                                                                                                                                  //Natural: READ CWF-SB-BENE BY SRVVR-KEY
        (
        "RD1",
        new Oc[] { new Oc("SRVVR_KEY", "ASC") }
        );
        RD1:
        while (condition(vw_cwf_Sb_Bene.readNextRow("RD1")))
        {
            pnd_Wfile.reset();                                                                                                                                            //Natural: RESET #WFILE
            if (condition(!(cwf_Sb_Bene_Srvvr_Nme_Fr_Frm.greater(" ") || cwf_Sb_Bene_Srvvr_Ssn_Nbr.greater(" "))))                                                        //Natural: ACCEPT IF CWF-SB-BENE.SRVVR-NME-FR-FRM GT ' ' OR CWF-SB-BENE.SRVVR-SSN-NBR GT ' '
            {
                continue;
            }
            pnd_Srvvr_Cntrct_Key_Grp_Pnd_Srvvr_Cntrct_Key.reset();                                                                                                        //Natural: RESET #SRVVR-CNTRCT-KEY
            pnd_Srvvr_Cntrct_Key_Grp.setValuesByName(vw_cwf_Sb_Bene);                                                                                                     //Natural: MOVE BY NAME CWF-SB-BENE TO #SRVVR-CNTRCT-KEY-GRP
            pnd_Wfile.setValuesByName(vw_cwf_Sb_Bene);                                                                                                                    //Natural: MOVE BY NAME CWF-SB-BENE TO #WFILE
            pnd_Wfile_X_Field.setValue("X");                                                                                                                              //Natural: MOVE 'X' TO X-FIELD
            //*  PARSE THE ADDRESS LINES
                                                                                                                                                                          //Natural: PERFORM PARSE-ADDRSS
            sub_Parse_Addrss();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  GET CONTRACT INFO
            pnd_Found.setValue(false);                                                                                                                                    //Natural: MOVE FALSE TO #FOUND
            vw_cwf_Sb_Cont_Ent.startDatabaseRead                                                                                                                          //Natural: READ CWF-SB-CONT-ENT BY SRVVR-CNTRCT-KEY #SRVVR-CNTRCT-KEY
            (
            "RD2",
            new Wc[] { new Wc("SRVVR_CNTRCT_KEY", ">=", pnd_Srvvr_Cntrct_Key_Grp_Pnd_Srvvr_Cntrct_Key, WcType.BY) },
            new Oc[] { new Oc("SRVVR_CNTRCT_KEY", "ASC") }
            );
            RD2:
            while (condition(vw_cwf_Sb_Cont_Ent.readNextRow("RD2")))
            {
                if (condition(cwf_Sb_Cont_Ent_Srvvr_Mit_Idntfr.notEquals(pnd_Srvvr_Cntrct_Key_Grp_Srvvr_Mit_Idntfr)))                                                     //Natural: IF CWF-SB-CONT-ENT.SRVVR-MIT-IDNTFR NE #SRVVR-CNTRCT-KEY-GRP.SRVVR-MIT-IDNTFR
                {
                    if (true) break RD2;                                                                                                                                  //Natural: ESCAPE BOTTOM ( RD2. )
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ctr_Read.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #CTR-READ
                pnd_Wfile_Tiaa_Cntrct.reset();                                                                                                                            //Natural: RESET #WFILE.TIAA-CNTRCT #WFILE.CNTRCT-STAT-CDE #WFILE.CNTRCT-STAT-DTE
                pnd_Wfile_Cntrct_Stat_Cde.reset();
                pnd_Wfile_Cntrct_Stat_Dte.reset();
                pnd_Wfile_Tiaa_Cntrct.setValue(cwf_Sb_Cont_Ent_Tiaa_Cntrct);                                                                                              //Natural: MOVE CWF-SB-CONT-ENT.TIAA-CNTRCT TO #WFILE.TIAA-CNTRCT
                pnd_Wfile_Lob.setValue(cwf_Sb_Cont_Ent_Lob);                                                                                                              //Natural: MOVE CWF-SB-CONT-ENT.LOB TO #WFILE.LOB
                if (condition(pnd_Wfile_Tiaa_Cntrct.greater(" ")))                                                                                                        //Natural: IF #WFILE.TIAA-CNTRCT GT ' '
                {
                    //*  GET STATUS OF CONTRACT
                    vw_iaa_Cntrct_Prtcpnt_Role.startDatabaseRead                                                                                                          //Natural: READ IAA-CNTRCT-PRTCPNT-ROLE BY CNTRCT-PAYEE-KEY #WFILE.TIAA-CNTRCT
                    (
                    "READ01",
                    new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_Wfile_Tiaa_Cntrct, WcType.BY) },
                    new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") }
                    );
                    READ01:
                    while (condition(vw_iaa_Cntrct_Prtcpnt_Role.readNextRow("READ01")))
                    {
                        if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr.notEquals(pnd_Wfile_Tiaa_Cntrct)))                                                     //Natural: IF CNTRCT-PART-PPCN-NBR NE #WFILE.TIAA-CNTRCT
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Wfile_Cntrct_Stat_Cde.setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde);                                                                    //Natural: MOVE CNTRCT-ACTVTY-CDE TO #WFILE.CNTRCT-STAT-CDE
                    }                                                                                                                                                     //Natural: END-READ
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD2"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD2"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  GET CONTRACT STATUS DATE
                    if (condition(pnd_Wfile_Cntrct_Stat_Cde.greater(" ")))                                                                                                //Natural: IF #WFILE.CNTRCT-STAT-CDE GT ' '
                    {
                        vw_iaa_Cntrct.startDatabaseRead                                                                                                                   //Natural: READ IAA-CNTRCT BY CNTRCT-PPCN-NBR #WFILE.TIAA-CNTRCT
                        (
                        "READ02",
                        new Wc[] { new Wc("CNTRCT_PPCN_NBR", ">=", pnd_Wfile_Tiaa_Cntrct, WcType.BY) },
                        new Oc[] { new Oc("CNTRCT_PPCN_NBR", "ASC") }
                        );
                        READ02:
                        while (condition(vw_iaa_Cntrct.readNextRow("READ02")))
                        {
                            if (condition(iaa_Cntrct_Cntrct_Ppcn_Nbr.notEquals(pnd_Wfile_Tiaa_Cntrct)))                                                                   //Natural: IF CNTRCT-PPCN-NBR NE #WFILE.TIAA-CNTRCT
                            {
                                if (condition(true)) break;                                                                                                               //Natural: ESCAPE BOTTOM
                            }                                                                                                                                             //Natural: END-IF
                            short decideConditionsMet269 = 0;                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CNTRCT-SCND-ANNT-DOD-DTE GT 0
                            if (condition(iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte.greater(getZero())))
                            {
                                decideConditionsMet269++;
                                pnd_Wfile_Cntrct_Stat_Dte.setValue(iaa_Cntrct_Scnd_Annt_Dod_Yyyy);                                                                        //Natural: MOVE SCND-ANNT-DOD-YYYY TO #WFILE.CNTRCT-STAT-DTE
                            }                                                                                                                                             //Natural: WHEN CNTRCT-FIRST-ANNT-DOD-DTE GT 0
                            else if (condition(iaa_Cntrct_Cntrct_First_Annt_Dod_Dte.greater(getZero())))
                            {
                                decideConditionsMet269++;
                                pnd_Wfile_Cntrct_Stat_Dte.setValue(iaa_Cntrct_First_Annt_Dod_Yyyy);                                                                       //Natural: MOVE FIRST-ANNT-DOD-YYYY TO #WFILE.CNTRCT-STAT-DTE
                            }                                                                                                                                             //Natural: WHEN NONE
                            else if (condition())
                            {
                                ignore();
                            }                                                                                                                                             //Natural: END-DECIDE
                        }                                                                                                                                                 //Natural: END-READ
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD2"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD2"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  WRITE WORKFILE
                getWorkFiles().write(1, false, pnd_Wfile);                                                                                                                //Natural: WRITE WORK FILE 1 #WFILE
                pnd_Ctr_Xtrctd.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #CTR-XTRCTD
                pnd_Found.setValue(true);                                                                                                                                 //Natural: MOVE TRUE TO #FOUND
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Found.getBoolean()))                                                                                                                        //Natural: IF #FOUND
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ctr_Read.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CTR-READ
            getWorkFiles().write(1, false, pnd_Wfile);                                                                                                                    //Natural: WRITE WORK FILE 1 #WFILE
            pnd_Ctr_Xtrctd.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #CTR-XTRCTD
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(1, NEWLINE,NEWLINE,new TabSetting(22),"SBUM REPORT",NEWLINE,NEWLINE,new TabSetting(22),"TOTAL RECORDS READ     :",pnd_Ctr_Read,                //Natural: WRITE ( 1 ) // 22T 'SBUM REPORT' // 22T 'TOTAL RECORDS READ     :' #CTR-READ ( EM = ZZ,ZZZ,ZZ9 ) // 22T 'Total Records Extracted:' #CTR-XTRCTD ( EM = ZZ,ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,new TabSetting(22),"Total Records Extracted:",pnd_Ctr_Xtrctd, new ReportEditMask ("ZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PARSE-ADDRSS
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE TITLE LEFT / 01T *PROGRAM 22T 'Survivor Corporate Workflow Facility' 69T *DATX ( EM = LLL' 'DD','YYYY ) / 01T 'Page:' *PAGE-NUMBER ( EM = Z9 ) 27T 'SBUM-OFAC File Extraction' 72T *TIMX ( EM = HH':'II' 'AP )
    }
    private void sub_Parse_Addrss() throws Exception                                                                                                                      //Natural: PARSE-ADDRSS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  /*
        //*  RESET #TWRAPART
        //*  /*
        //*  #TWRAPART.#TAX-YEAR  := #YEAR
        //*  #TWRAPART.#ON-CNAD   := TRUE
        //*  #TWRAPART.ADDR-LNE-1 := CWF-SB-BENE.SRVVR-ADDR-LINE1
        //*  #TWRAPART.ADDR-LNE-2 := CWF-SB-BENE.SRVVR-ADDR-LINE2
        //*  #TWRAPART.ADDR-LNE-3 := CWF-SB-BENE.SRVVR-ADDR-LINE3
        //*  #TWRAPART.ADDR-LNE-4 := CWF-SB-BENE.SRVVR-ADDR-LINE4
        //*  #TWRAPART.ADDR-LNE-5 := CWF-SB-BENE.SRVVR-ADDR-LINE5
        //*  #TWRAPART.ADDR-POSTAL-DATA := CWF-SB-BENE.SRVVR-ZIP-CDE
        //*  /*
        //*  CALLNAT 'TWRN5050'
        //*       #TWRAPART
        //*  /*
        //*  /* CHECK FOR ERRORS
        //*  IF NOT (#TWRAPART.#SYS-ERR(23) OR #TWRAPART.#SYS-ERR(27) OR
        //*          #TWRAPART.#SYS-ERR(28) OR #TWRAPART.#SYS-ERR(29))
        //*    #WFILE.SRVVR-ADDR-LINE1 := #TWRAPART.STREET-ADDR
        //*    #WFILE.SRVVR-ADDR-LINE2 := #TWRAPART.STREET-ADDR-CONT-1
        //*    #WFILE.SRVVR-ADDR-LINE3 := #TWRAPART.STREET-ADDR-CONT-2
        //*    COMPRESS #TWRAPART.CITY CWF-SB-BENE.STATE #TWRAPART.COUNTRY-NAME
        //*                  INTO #WFILE.SRVVR-ADDR-LINE4-5
        //*  END-IF
        //*  /*
        if (condition(cwf_Sb_Bene_State.greater(" ")))                                                                                                                    //Natural: IF STATE GT ' '
        {
            short decideConditionsMet334 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CWF-SB-BENE.SRVVR-ADDR-LINE6 GT ' '
            if (condition(cwf_Sb_Bene_Srvvr_Addr_Line6.greater(" ")))
            {
                decideConditionsMet334++;
                pnd_Wfile_Srvvr_Addr_Line4_5.setValue(DbsUtil.compress(cwf_Sb_Bene_Srvvr_Addr_Line6, cwf_Sb_Bene_State, pnd_Country_Name));                               //Natural: COMPRESS CWF-SB-BENE.SRVVR-ADDR-LINE6 CWF-SB-BENE.STATE #COUNTRY-NAME INTO #WFILE.SRVVR-ADDR-LINE4-5
            }                                                                                                                                                             //Natural: WHEN #WFILE.SRVVR-ADDR-LINE5 GT ' '
            else if (condition(pnd_Wfile_Srvvr_Addr_Line5.greater(" ")))
            {
                decideConditionsMet334++;
                pnd_Wfile_Srvvr_Addr_Line4_5.setValue(DbsUtil.compress(pnd_Wfile_Srvvr_Addr_Line5, cwf_Sb_Bene_State, pnd_Country_Name));                                 //Natural: COMPRESS #WFILE.SRVVR-ADDR-LINE5 CWF-SB-BENE.STATE #COUNTRY-NAME INTO #WFILE.SRVVR-ADDR-LINE4-5
                pnd_Wfile_Srvvr_Addr_Line5.reset();                                                                                                                       //Natural: RESET #WFILE.SRVVR-ADDR-LINE5
            }                                                                                                                                                             //Natural: WHEN #WFILE.SRVVR-ADDR-LINE4 GT ' '
            else if (condition(pnd_Wfile_Srvvr_Addr_Line4.greater(" ")))
            {
                decideConditionsMet334++;
                pnd_Wfile_Srvvr_Addr_Line4_5.setValue(DbsUtil.compress(pnd_Wfile_Srvvr_Addr_Line4, cwf_Sb_Bene_State, pnd_Country_Name));                                 //Natural: COMPRESS #WFILE.SRVVR-ADDR-LINE4 CWF-SB-BENE.STATE #COUNTRY-NAME INTO #WFILE.SRVVR-ADDR-LINE4-5
                pnd_Wfile_Srvvr_Addr_Line4.reset();                                                                                                                       //Natural: RESET #WFILE.SRVVR-ADDR-LINE4
            }                                                                                                                                                             //Natural: WHEN #WFILE.SRVVR-ADDR-LINE3 GT ' '
            else if (condition(pnd_Wfile_Srvvr_Addr_Line3.greater(" ")))
            {
                decideConditionsMet334++;
                pnd_Wfile_Srvvr_Addr_Line4_5.setValue(DbsUtil.compress(pnd_Wfile_Srvvr_Addr_Line3, cwf_Sb_Bene_State, pnd_Country_Name));                                 //Natural: COMPRESS #WFILE.SRVVR-ADDR-LINE3 CWF-SB-BENE.STATE #COUNTRY-NAME INTO #WFILE.SRVVR-ADDR-LINE4-5
                pnd_Wfile_Srvvr_Addr_Line3.reset();                                                                                                                       //Natural: RESET #WFILE.SRVVR-ADDR-LINE3
            }                                                                                                                                                             //Natural: WHEN #WFILE.SRVVR-ADDR-LINE2 GT ' '
            else if (condition(pnd_Wfile_Srvvr_Addr_Line2.greater(" ")))
            {
                decideConditionsMet334++;
                pnd_Wfile_Srvvr_Addr_Line4_5.setValue(DbsUtil.compress(pnd_Wfile_Srvvr_Addr_Line2, cwf_Sb_Bene_State, pnd_Country_Name));                                 //Natural: COMPRESS #WFILE.SRVVR-ADDR-LINE2 CWF-SB-BENE.STATE #COUNTRY-NAME INTO #WFILE.SRVVR-ADDR-LINE4-5
                pnd_Wfile_Srvvr_Addr_Line2.reset();                                                                                                                       //Natural: RESET #WFILE.SRVVR-ADDR-LINE2
            }                                                                                                                                                             //Natural: WHEN #WFILE.SRVVR-ADDR-LINE1 GT ' '
            else if (condition(pnd_Wfile_Srvvr_Addr_Line1.greater(" ")))
            {
                decideConditionsMet334++;
                pnd_Wfile_Srvvr_Addr_Line4_5.setValue(DbsUtil.compress(pnd_Wfile_Srvvr_Addr_Line1, cwf_Sb_Bene_State, pnd_Country_Name));                                 //Natural: COMPRESS #WFILE.SRVVR-ADDR-LINE1 CWF-SB-BENE.STATE #COUNTRY-NAME INTO #WFILE.SRVVR-ADDR-LINE4-5
                pnd_Wfile_Srvvr_Addr_Line1.reset();                                                                                                                       //Natural: RESET #WFILE.SRVVR-ADDR-LINE1
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "PS=40 LS=55");

        getReports().write(0, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,NEWLINE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(22),"Survivor Corporate Workflow Facility",new 
            TabSetting(69),Global.getDATX(), new ReportEditMask ("LLL' 'DD','YYYY"),NEWLINE,new TabSetting(1),"Page:",getReports().getPageNumberDbs(0), 
            new ReportEditMask ("Z9"),new TabSetting(27),"SBUM-OFAC File Extraction",new TabSetting(72),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"));
    }
}
