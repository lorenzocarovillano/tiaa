/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:11:21 PM
**        * FROM NATURAL PROGRAM : Efsp0065
************************************************************
**        * FILE NAME            : Efsp0065.java
**        * CLASS NAME           : Efsp0065
**        * INSTANCE NAME        : Efsp0065
************************************************************
************************************************************************
* PROGRAM  : EFSP0065
* SYSTEM   : CRPCWF
* TITLE    : PRINT IMAGE UPLOAD - MINS WITH PAGE NO. CONFLICT AND/OR
*          : PIN CONFLICT.
* WRITTEN  : SEPT. 17, 1996 10:00 AM
* FUNCTION : THIS PROGRAM READS THE CWF-UPLOAD-AUDIT FILE FOR ALL
*            BATCHES DONE BASED ON THE DATE/TIME PARAMETER.  IT THEN
*            READS ALL THE MINS ON THE DOCUMENT FILE AND COMPARES THE
*            STARTING PAGE AND PIN AGAINST THE XREF FILE FOR POSSIBLE
*            CONFLICTS.  CONFLICTS WILL BE SHOWN ON THIS REPORT.
* HISTORY
* 11/25/96  BE  ADDED DOCS INDEXED W/O PAGE 1 ON THE CONFLICT REPORT.
* 04/30/97  BE  ADDED PROCESSING FOR ICWF CONFLICTS.
* 07/10/97  BE  REPORT ANY END OF PAGE CONFLICTS.
* 07/17/97  BE  DO NOT RPT PPG CONFLICT IF AT LEAST 1 DOC PPG = XREF PPG
* 02/23/2017 - SINGAK - PIN EXPANSION - AUG 2017
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsp0065 extends BLNatBase
{
    // Data Areas
    private GdaCwfg000 gdaCwfg000;
    private LdaEfsl0040 ldaEfsl0040;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Efm_Document;
    private DbsField cwf_Efm_Document_Cabinet_Id;

    private DbsGroup cwf_Efm_Document__R_Field_1;
    private DbsField cwf_Efm_Document_Cabinet_Pref;
    private DbsField cwf_Efm_Document_Cabinet_Pin;
    private DbsField cwf_Efm_Document_Entry_Empl_Racf_Id;
    private DbsField cwf_Efm_Document_Document_Retention_Ind;
    private DbsField cwf_Efm_Document_Image_Min;

    private DbsGroup cwf_Efm_Document__R_Field_2;
    private DbsField cwf_Efm_Document_Image_Batch;

    private DbsGroup cwf_Efm_Document__R_Field_3;
    private DbsField cwf_Efm_Document_Dcmt_Min;
    private DbsField cwf_Efm_Document_Dcmt_St_Page_No;
    private DbsField cwf_Efm_Document_Image_Source_Id;
    private DbsField cwf_Efm_Document_Dgtze_Doc_End_Page;

    private DataAccessProgramView vw_cwf_Efm_Document_2;
    private DbsField cwf_Efm_Document_2_Cabinet_Id;

    private DbsGroup cwf_Efm_Document_2__R_Field_4;
    private DbsField cwf_Efm_Document_2_Cabinet_Pref;
    private DbsField cwf_Efm_Document_2_Cabinet_Pin;
    private DbsField cwf_Efm_Document_2_Entry_Empl_Racf_Id;
    private DbsField cwf_Efm_Document_2_Document_Retention_Ind;
    private DbsField cwf_Efm_Document_2_Image_Min;

    private DbsGroup cwf_Efm_Document_2__R_Field_5;
    private DbsField cwf_Efm_Document_2_Image_Batch;

    private DbsGroup cwf_Efm_Document_2__R_Field_6;
    private DbsField cwf_Efm_Document_2_Dcmt_Min;
    private DbsField cwf_Efm_Document_2_Dcmt_St_Page_No;
    private DbsField cwf_Efm_Document_2_Image_Source_Id;
    private DbsField cwf_Efm_Document_2_Dgtze_Doc_End_Page;

    private DataAccessProgramView vw_cwf_Image_Xref;
    private DbsField cwf_Image_Xref_Mail_Item_Nbr;
    private DbsField cwf_Image_Xref_Image_Document_Address_Cde;
    private DbsField cwf_Image_Xref_Ph_Unique_Id_Nbr;
    private DbsField cwf_Image_Xref_Image_Quality_Ind;
    private DbsField cwf_Image_Xref_Upld_Date_Time;
    private DbsField cwf_Image_Xref_Number_Of_Pages;
    private DbsField cwf_Image_Xref_Source_Id;
    private DbsField cwf_Image_Xref_Capture_Id;
    private DbsField cwf_Image_Xref_Batch_Id;
    private DbsField cwf_Image_Xref_Image_Delete_Ind;
    private DbsField cwf_Image_Xref_Audit_System;
    private DbsField cwf_Image_Xref_Audit_Dte_Tme;
    private DbsField cwf_Image_Xref_Audit_Empl_Racf_Id;
    private DbsField cwf_Image_Xref_Cabinet_Id;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_7;
    private DbsField cwf_Support_Tbl_Pnd_Regular_Run_Parms;

    private DbsGroup cwf_Support_Tbl__R_Field_8;
    private DbsField cwf_Support_Tbl_Pnd_Reg_Start_End_Time;
    private DbsField cwf_Support_Tbl_Pnd_Reg_Run_Date;
    private DbsField cwf_Support_Tbl_Pnd_Override_Time_Parms;
    private DbsField cwf_Support_Tbl_Pnd_Override_Flag;
    private DbsField cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Dte;

    private DataAccessProgramView vw_icw_Efm_Document;
    private DbsField icw_Efm_Document_Cabinet_Id;

    private DbsGroup icw_Efm_Document__R_Field_9;
    private DbsField icw_Efm_Document_Iis_Hrchy_Level_Cde;
    private DbsField icw_Efm_Document_Ppg_Cde;
    private DbsField icw_Efm_Document_Rqst_Log_Dte_Tme;
    private DbsField icw_Efm_Document_Crte_Dte_Tme;
    private DbsField icw_Efm_Document_Crte_Sqnce_Nbr;
    private DbsField icw_Efm_Document_Crte_Empl_Racf_Id;
    private DbsField icw_Efm_Document_Crte_Unit;
    private DbsField icw_Efm_Document_Crte_System;
    private DbsField icw_Efm_Document_Crte_Jobname;

    private DbsGroup icw_Efm_Document_Dcmnt_Type;
    private DbsField icw_Efm_Document_Dcmnt_Ctgry;
    private DbsField icw_Efm_Document_Dcmnt_Sub_Ctgry;
    private DbsField icw_Efm_Document_Dcmnt_Dtl;
    private DbsField icw_Efm_Document_Dcmnt_Drctn;
    private DbsField icw_Efm_Document_Dcmnt_Scrty_Ind;
    private DbsField icw_Efm_Document_Dcmnt_Rtntn;
    private DbsField icw_Efm_Document_Dcmnt_Frmt;
    private DbsField icw_Efm_Document_Dcmnt_Anttn_Ind;
    private DbsField icw_Efm_Document_Dcmnt_Copy_Ind;
    private DbsField icw_Efm_Document_Invrt_Crte_Dte_Tme;

    private DbsGroup icw_Efm_Document_Audit_Data;
    private DbsField icw_Efm_Document_Audit_Action;
    private DbsField icw_Efm_Document_Audit_Empl_Racf_Id;
    private DbsField icw_Efm_Document_Audit_System;
    private DbsField icw_Efm_Document_Audit_Unit;
    private DbsField icw_Efm_Document_Audit_Jobname;
    private DbsField icw_Efm_Document_Audit_Dte_Tme;
    private DbsField icw_Efm_Document_Text_Dte_Tme;
    private DbsField icw_Efm_Document_Image_Source_Id;
    private DbsField icw_Efm_Document_Image_Min;

    private DbsGroup icw_Efm_Document__R_Field_10;
    private DbsField icw_Efm_Document_Image_Batch;

    private DbsGroup icw_Efm_Document__R_Field_11;
    private DbsField icw_Efm_Document_Dcmt_Min;
    private DbsField icw_Efm_Document_Dcmt_St_Page_No;
    private DbsField icw_Efm_Document_Image_End_Page;
    private DbsField icw_Efm_Document_Image_Ornttn;
    private DbsField icw_Efm_Document_Kdo_Crte_Dte_Tme;
    private DbsField icw_Efm_Document_Orgnl_Cabinet;

    private DataAccessProgramView vw_icw_Efm_Document_2;
    private DbsField icw_Efm_Document_2_Cabinet_Id;

    private DbsGroup icw_Efm_Document_2__R_Field_12;
    private DbsField icw_Efm_Document_2_Iis_Hrchy_Level_Cde;
    private DbsField icw_Efm_Document_2_Ppg_Cde;
    private DbsField icw_Efm_Document_2_Rqst_Log_Dte_Tme;
    private DbsField icw_Efm_Document_2_Crte_Dte_Tme;
    private DbsField icw_Efm_Document_2_Crte_Sqnce_Nbr;
    private DbsField icw_Efm_Document_2_Crte_Empl_Racf_Id;
    private DbsField icw_Efm_Document_2_Crte_Unit;
    private DbsField icw_Efm_Document_2_Crte_System;
    private DbsField icw_Efm_Document_2_Crte_Jobname;

    private DbsGroup icw_Efm_Document_2_Dcmnt_Type;
    private DbsField icw_Efm_Document_2_Dcmnt_Ctgry;
    private DbsField icw_Efm_Document_2_Dcmnt_Sub_Ctgry;
    private DbsField icw_Efm_Document_2_Dcmnt_Dtl;
    private DbsField icw_Efm_Document_2_Dcmnt_Drctn;
    private DbsField icw_Efm_Document_2_Dcmnt_Scrty_Ind;
    private DbsField icw_Efm_Document_2_Dcmnt_Rtntn;
    private DbsField icw_Efm_Document_2_Dcmnt_Frmt;
    private DbsField icw_Efm_Document_2_Dcmnt_Anttn_Ind;
    private DbsField icw_Efm_Document_2_Dcmnt_Copy_Ind;
    private DbsField icw_Efm_Document_2_Invrt_Crte_Dte_Tme;

    private DbsGroup icw_Efm_Document_2_Audit_Data;
    private DbsField icw_Efm_Document_2_Audit_Action;
    private DbsField icw_Efm_Document_2_Audit_Empl_Racf_Id;
    private DbsField icw_Efm_Document_2_Audit_System;
    private DbsField icw_Efm_Document_2_Audit_Unit;
    private DbsField icw_Efm_Document_2_Audit_Jobname;
    private DbsField icw_Efm_Document_2_Audit_Dte_Tme;
    private DbsField icw_Efm_Document_2_Text_Dte_Tme;
    private DbsField icw_Efm_Document_2_Image_Source_Id;
    private DbsField icw_Efm_Document_2_Image_Min;

    private DbsGroup icw_Efm_Document_2__R_Field_13;
    private DbsField icw_Efm_Document_2_Image_Batch;

    private DbsGroup icw_Efm_Document_2__R_Field_14;
    private DbsField icw_Efm_Document_2_Dcmt_Min;
    private DbsField icw_Efm_Document_2_Dcmt_St_Page_No;
    private DbsField icw_Efm_Document_2_Image_End_Page;
    private DbsField icw_Efm_Document_2_Image_Ornttn;
    private DbsField icw_Efm_Document_2_Kdo_Crte_Dte_Tme;
    private DbsField icw_Efm_Document_2_Orgnl_Cabinet;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_Lines_Reqd;
    private DbsField pnd_Program;
    private DbsField pnd_Tot_Pin_Conflict;
    private DbsField pnd_Tot_Pge_Conflict;
    private DbsField pnd_Tot_No_Pg_1;
    private DbsField pnd_Tot_End_Pg_Noeql;
    private DbsField pnd_Source_Id;
    private DbsField pnd_Unidentified_Line;

    private DbsGroup pnd_Unidentified_Line__R_Field_15;

    private DbsGroup pnd_Unidentified_Line_Pnd_Min_Array;
    private DbsField pnd_Unidentified_Line_Pnd_Print_Min;
    private DbsField pnd_Unidentified_Line_Pnd_Filler;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_16;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind;
    private DbsField pnd_Input_Source_Id;
    private DbsField pnd_Input_Data;

    private DbsGroup pnd_Input_Data__R_Field_17;
    private DbsField pnd_Input_Data_Pnd_Input_Time_Start;
    private DbsField pnd_Input_Data_Pnd_Input_Time_End;
    private DbsField pnd_Input_Data_Pnd_Input_Date;
    private DbsField pnd_Input_Date_Time;
    private DbsField pnd_Input_Date_D;
    private DbsField pnd_Start_Key;
    private DbsField pnd_End_Key;
    private DbsField pnd_Upld_Time;

    private DbsGroup pnd_Upld_Time__R_Field_18;
    private DbsField pnd_Upld_Time_Pnd_Upld_Timen;
    private DbsField pnd_Isn_Tbl;
    private DbsField pnd_Start_Date;
    private DbsField pnd_Next_Processing_Date;
    private DbsField pnd_Orig_Mm;
    private DbsField pnd_New_Mm;
    private DbsField pnd_Image_Pointer_Key;

    private DbsGroup pnd_Image_Pointer_Key__R_Field_19;
    private DbsField pnd_Image_Pointer_Key_Pnd_Image_Source;
    private DbsField pnd_Image_Pointer_Key_Pnd_Image_Batch;
    private DbsField pnd_Dcmt_Source_Id_Min_Key;

    private DbsGroup pnd_Dcmt_Source_Id_Min_Key__R_Field_20;
    private DbsField pnd_Dcmt_Source_Id_Min_Key_Pnd_Dcmt_Source_Id;
    private DbsField pnd_Dcmt_Source_Id_Min_Key_Pnd_Dcmt_Min;
    private DbsField pnd_Conflict_Desc;
    private DbsField pnd_Nothing_To_Rpt;
    private DbsField pnd_Prev_Min;
    private DbsField pnd_Page_One_Exists;
    private DbsField pnd_Old_St_Page;
    private DbsField pnd_Old_End_Page;
    private DbsField pnd_Exact_Image_Pointer_Key;

    private DbsGroup pnd_Exact_Image_Pointer_Key__R_Field_21;
    private DbsField pnd_Exact_Image_Pointer_Key_Pnd_Exact_Image_Source;
    private DbsField pnd_Exact_Image_Pointer_Key_Pnd_Exact_Image_Min;
    private DbsField pnd_Batch_Found_In_Cwf;
    private DbsField pnd_End_Pg_Cnflct_Processed;
    private DbsField pnd_Ppg_Code_Arr;
    private DbsField pnd_Ppg_Ctr;
    private DbsField pnd_Ppg_Conflict;
    private DbsField pnd_Record;

    private DbsRecord internalLoopRecord;
    private DbsField sort01Source_IdOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaCwfg000 = GdaCwfg000.getInstance(getCallnatLevel());
        registerRecord(gdaCwfg000);
        if (gdaOnly) return;

        ldaEfsl0040 = new LdaEfsl0040();
        registerRecord(ldaEfsl0040);
        registerRecord(ldaEfsl0040.getVw_cwf_Upload_Audit());

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Efm_Document = new DataAccessProgramView(new NameInfo("vw_cwf_Efm_Document", "CWF-EFM-DOCUMENT"), "CWF_EFM_DOCUMENT", "CWF_EFM_DOCUMENT");
        cwf_Efm_Document_Cabinet_Id = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Cabinet_Id", "CABINET-ID", FieldType.STRING, 14, 
            RepeatingFieldStrategy.None, "CABINET_ID");
        cwf_Efm_Document_Cabinet_Id.setDdmHeader("CABINET/ID");

        cwf_Efm_Document__R_Field_1 = vw_cwf_Efm_Document.getRecord().newGroupInGroup("cwf_Efm_Document__R_Field_1", "REDEFINE", cwf_Efm_Document_Cabinet_Id);
        cwf_Efm_Document_Cabinet_Pref = cwf_Efm_Document__R_Field_1.newFieldInGroup("cwf_Efm_Document_Cabinet_Pref", "CABINET-PREF", FieldType.STRING, 
            1);
        cwf_Efm_Document_Cabinet_Pin = cwf_Efm_Document__R_Field_1.newFieldInGroup("cwf_Efm_Document_Cabinet_Pin", "CABINET-PIN", FieldType.NUMERIC, 12);
        cwf_Efm_Document_Entry_Empl_Racf_Id = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Entry_Empl_Racf_Id", "ENTRY-EMPL-RACF-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ENTRY_EMPL_RACF_ID");
        cwf_Efm_Document_Entry_Empl_Racf_Id.setDdmHeader("CREATED BY/RACF ID");
        cwf_Efm_Document_Document_Retention_Ind = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Document_Retention_Ind", "DOCUMENT-RETENTION-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "DOCUMENT_RETENTION_IND");
        cwf_Efm_Document_Document_Retention_Ind.setDdmHeader("DOC/RETNTN");
        cwf_Efm_Document_Image_Min = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Image_Min", "IMAGE-MIN", FieldType.STRING, 14, 
            RepeatingFieldStrategy.None, "IMAGE_MIN");
        cwf_Efm_Document_Image_Min.setDdmHeader("IMAGE/POINTER");

        cwf_Efm_Document__R_Field_2 = vw_cwf_Efm_Document.getRecord().newGroupInGroup("cwf_Efm_Document__R_Field_2", "REDEFINE", cwf_Efm_Document_Image_Min);
        cwf_Efm_Document_Image_Batch = cwf_Efm_Document__R_Field_2.newFieldInGroup("cwf_Efm_Document_Image_Batch", "IMAGE-BATCH", FieldType.NUMERIC, 8);

        cwf_Efm_Document__R_Field_3 = vw_cwf_Efm_Document.getRecord().newGroupInGroup("cwf_Efm_Document__R_Field_3", "REDEFINE", cwf_Efm_Document_Image_Min);
        cwf_Efm_Document_Dcmt_Min = cwf_Efm_Document__R_Field_3.newFieldInGroup("cwf_Efm_Document_Dcmt_Min", "DCMT-MIN", FieldType.NUMERIC, 11);
        cwf_Efm_Document_Dcmt_St_Page_No = cwf_Efm_Document__R_Field_3.newFieldInGroup("cwf_Efm_Document_Dcmt_St_Page_No", "DCMT-ST-PAGE-NO", FieldType.NUMERIC, 
            3);
        cwf_Efm_Document_Image_Source_Id = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Image_Source_Id", "IMAGE-SOURCE-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "IMAGE_SOURCE_ID");
        cwf_Efm_Document_Image_Source_Id.setDdmHeader("IMAGE/SOURCE ID");
        cwf_Efm_Document_Dgtze_Doc_End_Page = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Dgtze_Doc_End_Page", "DGTZE-DOC-END-PAGE", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "DGTZE_DOC_END_PAGE");
        cwf_Efm_Document_Dgtze_Doc_End_Page.setDdmHeader("DOCUMENT/END PGE");
        registerRecord(vw_cwf_Efm_Document);

        vw_cwf_Efm_Document_2 = new DataAccessProgramView(new NameInfo("vw_cwf_Efm_Document_2", "CWF-EFM-DOCUMENT-2"), "CWF_EFM_DOCUMENT", "CWF_EFM_DOCUMENT");
        cwf_Efm_Document_2_Cabinet_Id = vw_cwf_Efm_Document_2.getRecord().newFieldInGroup("cwf_Efm_Document_2_Cabinet_Id", "CABINET-ID", FieldType.STRING, 
            14, RepeatingFieldStrategy.None, "CABINET_ID");
        cwf_Efm_Document_2_Cabinet_Id.setDdmHeader("CABINET/ID");

        cwf_Efm_Document_2__R_Field_4 = vw_cwf_Efm_Document_2.getRecord().newGroupInGroup("cwf_Efm_Document_2__R_Field_4", "REDEFINE", cwf_Efm_Document_2_Cabinet_Id);
        cwf_Efm_Document_2_Cabinet_Pref = cwf_Efm_Document_2__R_Field_4.newFieldInGroup("cwf_Efm_Document_2_Cabinet_Pref", "CABINET-PREF", FieldType.STRING, 
            1);
        cwf_Efm_Document_2_Cabinet_Pin = cwf_Efm_Document_2__R_Field_4.newFieldInGroup("cwf_Efm_Document_2_Cabinet_Pin", "CABINET-PIN", FieldType.NUMERIC, 
            12);
        cwf_Efm_Document_2_Entry_Empl_Racf_Id = vw_cwf_Efm_Document_2.getRecord().newFieldInGroup("cwf_Efm_Document_2_Entry_Empl_Racf_Id", "ENTRY-EMPL-RACF-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ENTRY_EMPL_RACF_ID");
        cwf_Efm_Document_2_Entry_Empl_Racf_Id.setDdmHeader("CREATED BY/RACF ID");
        cwf_Efm_Document_2_Document_Retention_Ind = vw_cwf_Efm_Document_2.getRecord().newFieldInGroup("cwf_Efm_Document_2_Document_Retention_Ind", "DOCUMENT-RETENTION-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "DOCUMENT_RETENTION_IND");
        cwf_Efm_Document_2_Document_Retention_Ind.setDdmHeader("DOC/RETNTN");
        cwf_Efm_Document_2_Image_Min = vw_cwf_Efm_Document_2.getRecord().newFieldInGroup("cwf_Efm_Document_2_Image_Min", "IMAGE-MIN", FieldType.STRING, 
            14, RepeatingFieldStrategy.None, "IMAGE_MIN");
        cwf_Efm_Document_2_Image_Min.setDdmHeader("IMAGE/POINTER");

        cwf_Efm_Document_2__R_Field_5 = vw_cwf_Efm_Document_2.getRecord().newGroupInGroup("cwf_Efm_Document_2__R_Field_5", "REDEFINE", cwf_Efm_Document_2_Image_Min);
        cwf_Efm_Document_2_Image_Batch = cwf_Efm_Document_2__R_Field_5.newFieldInGroup("cwf_Efm_Document_2_Image_Batch", "IMAGE-BATCH", FieldType.NUMERIC, 
            8);

        cwf_Efm_Document_2__R_Field_6 = vw_cwf_Efm_Document_2.getRecord().newGroupInGroup("cwf_Efm_Document_2__R_Field_6", "REDEFINE", cwf_Efm_Document_2_Image_Min);
        cwf_Efm_Document_2_Dcmt_Min = cwf_Efm_Document_2__R_Field_6.newFieldInGroup("cwf_Efm_Document_2_Dcmt_Min", "DCMT-MIN", FieldType.NUMERIC, 11);
        cwf_Efm_Document_2_Dcmt_St_Page_No = cwf_Efm_Document_2__R_Field_6.newFieldInGroup("cwf_Efm_Document_2_Dcmt_St_Page_No", "DCMT-ST-PAGE-NO", FieldType.NUMERIC, 
            3);
        cwf_Efm_Document_2_Image_Source_Id = vw_cwf_Efm_Document_2.getRecord().newFieldInGroup("cwf_Efm_Document_2_Image_Source_Id", "IMAGE-SOURCE-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "IMAGE_SOURCE_ID");
        cwf_Efm_Document_2_Image_Source_Id.setDdmHeader("IMAGE/SOURCE ID");
        cwf_Efm_Document_2_Dgtze_Doc_End_Page = vw_cwf_Efm_Document_2.getRecord().newFieldInGroup("cwf_Efm_Document_2_Dgtze_Doc_End_Page", "DGTZE-DOC-END-PAGE", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "DGTZE_DOC_END_PAGE");
        cwf_Efm_Document_2_Dgtze_Doc_End_Page.setDdmHeader("DOCUMENT/END PGE");
        registerRecord(vw_cwf_Efm_Document_2);

        vw_cwf_Image_Xref = new DataAccessProgramView(new NameInfo("vw_cwf_Image_Xref", "CWF-IMAGE-XREF"), "CWF_IMAGE_XREF", "CWF_IMAGE_XREF");
        cwf_Image_Xref_Mail_Item_Nbr = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Mail_Item_Nbr", "MAIL-ITEM-NBR", FieldType.NUMERIC, 
            11, RepeatingFieldStrategy.None, "MAIL_ITEM_NBR");
        cwf_Image_Xref_Mail_Item_Nbr.setDdmHeader("MAIL ITEM/NUMBER");
        cwf_Image_Xref_Image_Document_Address_Cde = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Image_Document_Address_Cde", "IMAGE-DOCUMENT-ADDRESS-CDE", 
            FieldType.STRING, 22, RepeatingFieldStrategy.None, "IMAGE_DOCUMENT_ADDRESS_CDE");
        cwf_Image_Xref_Image_Document_Address_Cde.setDdmHeader("IMNET DOC/ADDRESS");
        cwf_Image_Xref_Ph_Unique_Id_Nbr = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Ph_Unique_Id_Nbr", "PH-UNIQUE-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "PH_UNIQUE_ID_NBR");
        cwf_Image_Xref_Ph_Unique_Id_Nbr.setDdmHeader("PIN");
        cwf_Image_Xref_Image_Quality_Ind = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Image_Quality_Ind", "IMAGE-QUALITY-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "IMAGE_QUALITY_IND");
        cwf_Image_Xref_Image_Quality_Ind.setDdmHeader("POOR/QLTY");
        cwf_Image_Xref_Upld_Date_Time = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Upld_Date_Time", "UPLD-DATE-TIME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "UPLD_DATE_TIME");
        cwf_Image_Xref_Upld_Date_Time.setDdmHeader("UPLOAD/DATE TIME");
        cwf_Image_Xref_Number_Of_Pages = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Number_Of_Pages", "NUMBER-OF-PAGES", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "NUMBER_OF_PAGES");
        cwf_Image_Xref_Number_Of_Pages.setDdmHeader("NBR OF/PAGES");
        cwf_Image_Xref_Source_Id = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Source_Id", "SOURCE-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "SOURCE_ID");
        cwf_Image_Xref_Capture_Id = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Capture_Id", "CAPTURE-ID", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "CAPTURE_ID");
        cwf_Image_Xref_Batch_Id = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Batch_Id", "BATCH-ID", FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, 
            "BATCH_ID");
        cwf_Image_Xref_Image_Delete_Ind = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Image_Delete_Ind", "IMAGE-DELETE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "IMAGE_DELETE_IND");
        cwf_Image_Xref_Audit_System = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Audit_System", "AUDIT-SYSTEM", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "AUDIT_SYSTEM");
        cwf_Image_Xref_Audit_Dte_Tme = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Audit_Dte_Tme", "AUDIT-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "AUDIT_DTE_TME");
        cwf_Image_Xref_Audit_Empl_Racf_Id = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Audit_Empl_Racf_Id", "AUDIT-EMPL-RACF-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "AUDIT_EMPL_RACF_ID");
        cwf_Image_Xref_Cabinet_Id = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Cabinet_Id", "CABINET-ID", FieldType.STRING, 12, RepeatingFieldStrategy.None, 
            "CABINET_ID");
        registerRecord(vw_cwf_Image_Xref);

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Support_Tbl__R_Field_7 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_7", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Pnd_Regular_Run_Parms = cwf_Support_Tbl__R_Field_7.newFieldInGroup("cwf_Support_Tbl_Pnd_Regular_Run_Parms", "#REGULAR-RUN-PARMS", 
            FieldType.STRING, 20);

        cwf_Support_Tbl__R_Field_8 = cwf_Support_Tbl__R_Field_7.newGroupInGroup("cwf_Support_Tbl__R_Field_8", "REDEFINE", cwf_Support_Tbl_Pnd_Regular_Run_Parms);
        cwf_Support_Tbl_Pnd_Reg_Start_End_Time = cwf_Support_Tbl__R_Field_8.newFieldInGroup("cwf_Support_Tbl_Pnd_Reg_Start_End_Time", "#REG-START-END-TIME", 
            FieldType.STRING, 12);
        cwf_Support_Tbl_Pnd_Reg_Run_Date = cwf_Support_Tbl__R_Field_8.newFieldInGroup("cwf_Support_Tbl_Pnd_Reg_Run_Date", "#REG-RUN-DATE", FieldType.STRING, 
            8);
        cwf_Support_Tbl_Pnd_Override_Time_Parms = cwf_Support_Tbl__R_Field_7.newFieldInGroup("cwf_Support_Tbl_Pnd_Override_Time_Parms", "#OVERRIDE-TIME-PARMS", 
            FieldType.STRING, 12);
        cwf_Support_Tbl_Pnd_Override_Flag = cwf_Support_Tbl__R_Field_7.newFieldInGroup("cwf_Support_Tbl_Pnd_Override_Flag", "#OVERRIDE-FLAG", FieldType.STRING, 
            1);
        cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde", "TBL-DLTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_DLTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        cwf_Support_Tbl_Tbl_Updte_Dte = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Dte", "TBL-UPDTE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE");
        cwf_Support_Tbl_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        registerRecord(vw_cwf_Support_Tbl);

        vw_icw_Efm_Document = new DataAccessProgramView(new NameInfo("vw_icw_Efm_Document", "ICW-EFM-DOCUMENT"), "ICW_EFM_DOCUMENT", "ICW_EFM_DOCUMENT");
        icw_Efm_Document_Cabinet_Id = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Cabinet_Id", "CABINET-ID", FieldType.STRING, 12, 
            RepeatingFieldStrategy.None, "CABINET_ID");
        icw_Efm_Document_Cabinet_Id.setDdmHeader("CABINET/ID");

        icw_Efm_Document__R_Field_9 = vw_icw_Efm_Document.getRecord().newGroupInGroup("icw_Efm_Document__R_Field_9", "REDEFINE", icw_Efm_Document_Cabinet_Id);
        icw_Efm_Document_Iis_Hrchy_Level_Cde = icw_Efm_Document__R_Field_9.newFieldInGroup("icw_Efm_Document_Iis_Hrchy_Level_Cde", "IIS-HRCHY-LEVEL-CDE", 
            FieldType.NUMERIC, 6);
        icw_Efm_Document_Ppg_Cde = icw_Efm_Document__R_Field_9.newFieldInGroup("icw_Efm_Document_Ppg_Cde", "PPG-CDE", FieldType.STRING, 6);
        icw_Efm_Document_Rqst_Log_Dte_Tme = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        icw_Efm_Document_Rqst_Log_Dte_Tme.setDdmHeader("MIT RQST/DATE AND TIME");
        icw_Efm_Document_Crte_Dte_Tme = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Crte_Dte_Tme", "CRTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "CRTE_DTE_TME");
        icw_Efm_Document_Crte_Dte_Tme.setDdmHeader("CREATE/DATE AND TIME");
        icw_Efm_Document_Crte_Sqnce_Nbr = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Crte_Sqnce_Nbr", "CRTE-SQNCE-NBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "CRTE_SQNCE_NBR");
        icw_Efm_Document_Crte_Empl_Racf_Id = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Crte_Empl_Racf_Id", "CRTE-EMPL-RACF-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CRTE_EMPL_RACF_ID");
        icw_Efm_Document_Crte_Empl_Racf_Id.setDdmHeader("CREATED BY/RACF ID");
        icw_Efm_Document_Crte_Unit = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Crte_Unit", "CRTE-UNIT", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "CRTE_UNIT");
        icw_Efm_Document_Crte_Unit.setDdmHeader("CREATED BY/UNIT");
        icw_Efm_Document_Crte_System = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Crte_System", "CRTE-SYSTEM", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CRTE_SYSTEM");
        icw_Efm_Document_Crte_System.setDdmHeader("CREATED BY/SYSTEM");
        icw_Efm_Document_Crte_Jobname = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Crte_Jobname", "CRTE-JOBNAME", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CRTE_JOBNAME");
        icw_Efm_Document_Crte_Jobname.setDdmHeader("CREATED BY/JOBNAME");

        icw_Efm_Document_Dcmnt_Type = vw_icw_Efm_Document.getRecord().newGroupInGroup("ICW_EFM_DOCUMENT_DCMNT_TYPE", "DCMNT-TYPE");
        icw_Efm_Document_Dcmnt_Type.setDdmHeader("DOCUMENT/TYPE");
        icw_Efm_Document_Dcmnt_Ctgry = icw_Efm_Document_Dcmnt_Type.newFieldInGroup("icw_Efm_Document_Dcmnt_Ctgry", "DCMNT-CTGRY", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "DCMNT_CTGRY");
        icw_Efm_Document_Dcmnt_Ctgry.setDdmHeader("DOC/CAT");
        icw_Efm_Document_Dcmnt_Sub_Ctgry = icw_Efm_Document_Dcmnt_Type.newFieldInGroup("icw_Efm_Document_Dcmnt_Sub_Ctgry", "DCMNT-SUB-CTGRY", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "DCMNT_SUB_CTGRY");
        icw_Efm_Document_Dcmnt_Sub_Ctgry.setDdmHeader("DOC SUB-/CATEGORY");
        icw_Efm_Document_Dcmnt_Dtl = icw_Efm_Document_Dcmnt_Type.newFieldInGroup("icw_Efm_Document_Dcmnt_Dtl", "DCMNT-DTL", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "DCMNT_DTL");
        icw_Efm_Document_Dcmnt_Dtl.setDdmHeader("DOCUMENT/DETAIL");
        icw_Efm_Document_Dcmnt_Drctn = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Dcmnt_Drctn", "DCMNT-DRCTN", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DCMNT_DRCTN");
        icw_Efm_Document_Dcmnt_Drctn.setDdmHeader("DOC/DIR");
        icw_Efm_Document_Dcmnt_Scrty_Ind = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Dcmnt_Scrty_Ind", "DCMNT-SCRTY-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DCMNT_SCRTY_IND");
        icw_Efm_Document_Dcmnt_Scrty_Ind.setDdmHeader("SECRD/DOC");
        icw_Efm_Document_Dcmnt_Rtntn = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Dcmnt_Rtntn", "DCMNT-RTNTN", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DCMNT_RTNTN");
        icw_Efm_Document_Dcmnt_Rtntn.setDdmHeader("DOC/RETNTN");
        icw_Efm_Document_Dcmnt_Frmt = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Dcmnt_Frmt", "DCMNT-FRMT", FieldType.STRING, 3, 
            RepeatingFieldStrategy.None, "DCMNT_FRMT");
        icw_Efm_Document_Dcmnt_Anttn_Ind = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Dcmnt_Anttn_Ind", "DCMNT-ANTTN-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DCMNT_ANTTN_IND");
        icw_Efm_Document_Dcmnt_Anttn_Ind.setDdmHeader("ANOTATION/IND");
        icw_Efm_Document_Dcmnt_Copy_Ind = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Dcmnt_Copy_Ind", "DCMNT-COPY-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DCMNT_COPY_IND");
        icw_Efm_Document_Dcmnt_Copy_Ind.setDdmHeader("COPY/IND");
        icw_Efm_Document_Invrt_Crte_Dte_Tme = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Invrt_Crte_Dte_Tme", "INVRT-CRTE-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "INVRT_CRTE_DTE_TME");
        icw_Efm_Document_Invrt_Crte_Dte_Tme.setDdmHeader("INVERTED CREATE/DATE AND TIME");

        icw_Efm_Document_Audit_Data = vw_icw_Efm_Document.getRecord().newGroupInGroup("ICW_EFM_DOCUMENT_AUDIT_DATA", "AUDIT-DATA");
        icw_Efm_Document_Audit_Action = icw_Efm_Document_Audit_Data.newFieldInGroup("icw_Efm_Document_Audit_Action", "AUDIT-ACTION", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "AUDIT_ACTION");
        icw_Efm_Document_Audit_Action.setDdmHeader("ACTION");
        icw_Efm_Document_Audit_Empl_Racf_Id = icw_Efm_Document_Audit_Data.newFieldInGroup("icw_Efm_Document_Audit_Empl_Racf_Id", "AUDIT-EMPL-RACF-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "AUDIT_EMPL_RACF_ID");
        icw_Efm_Document_Audit_Empl_Racf_Id.setDdmHeader("EMPLOYEE/RACF ID");
        icw_Efm_Document_Audit_System = icw_Efm_Document_Audit_Data.newFieldInGroup("icw_Efm_Document_Audit_System", "AUDIT-SYSTEM", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "AUDIT_SYSTEM");
        icw_Efm_Document_Audit_System.setDdmHeader("AUDIT/SYSTEM");
        icw_Efm_Document_Audit_Unit = icw_Efm_Document_Audit_Data.newFieldInGroup("icw_Efm_Document_Audit_Unit", "AUDIT-UNIT", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "AUDIT_UNIT");
        icw_Efm_Document_Audit_Unit.setDdmHeader("AUDIT/UNIT");
        icw_Efm_Document_Audit_Jobname = icw_Efm_Document_Audit_Data.newFieldInGroup("icw_Efm_Document_Audit_Jobname", "AUDIT-JOBNAME", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "AUDIT_JOBNAME");
        icw_Efm_Document_Audit_Jobname.setDdmHeader("JOBNAME");
        icw_Efm_Document_Audit_Dte_Tme = icw_Efm_Document_Audit_Data.newFieldInGroup("icw_Efm_Document_Audit_Dte_Tme", "AUDIT-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "AUDIT_DTE_TME");
        icw_Efm_Document_Audit_Dte_Tme.setDdmHeader("DATE/& TIME");
        icw_Efm_Document_Text_Dte_Tme = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Text_Dte_Tme", "TEXT-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TEXT_DTE_TME");
        icw_Efm_Document_Text_Dte_Tme.setDdmHeader("TEXT/DATE AND TIME");
        icw_Efm_Document_Image_Source_Id = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Image_Source_Id", "IMAGE-SOURCE-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "IMAGE_SOURCE_ID");
        icw_Efm_Document_Image_Source_Id.setDdmHeader("IMAGE/SOURCE ID");
        icw_Efm_Document_Image_Min = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Image_Min", "IMAGE-MIN", FieldType.STRING, 14, 
            RepeatingFieldStrategy.None, "IMAGE_MIN");
        icw_Efm_Document_Image_Min.setDdmHeader("IMAGE/POINTER");

        icw_Efm_Document__R_Field_10 = vw_icw_Efm_Document.getRecord().newGroupInGroup("icw_Efm_Document__R_Field_10", "REDEFINE", icw_Efm_Document_Image_Min);
        icw_Efm_Document_Image_Batch = icw_Efm_Document__R_Field_10.newFieldInGroup("icw_Efm_Document_Image_Batch", "IMAGE-BATCH", FieldType.NUMERIC, 
            8);

        icw_Efm_Document__R_Field_11 = vw_icw_Efm_Document.getRecord().newGroupInGroup("icw_Efm_Document__R_Field_11", "REDEFINE", icw_Efm_Document_Image_Min);
        icw_Efm_Document_Dcmt_Min = icw_Efm_Document__R_Field_11.newFieldInGroup("icw_Efm_Document_Dcmt_Min", "DCMT-MIN", FieldType.NUMERIC, 11);
        icw_Efm_Document_Dcmt_St_Page_No = icw_Efm_Document__R_Field_11.newFieldInGroup("icw_Efm_Document_Dcmt_St_Page_No", "DCMT-ST-PAGE-NO", FieldType.NUMERIC, 
            3);
        icw_Efm_Document_Image_End_Page = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Image_End_Page", "IMAGE-END-PAGE", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "IMAGE_END_PAGE");
        icw_Efm_Document_Image_End_Page.setDdmHeader("IMAGE/END PAGE");
        icw_Efm_Document_Image_Ornttn = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Image_Ornttn", "IMAGE-ORNTTN", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "IMAGE_ORNTTN");
        icw_Efm_Document_Image_Ornttn.setDdmHeader("DOC/ORIENT");
        icw_Efm_Document_Kdo_Crte_Dte_Tme = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Kdo_Crte_Dte_Tme", "KDO-CRTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "KDO_CRTE_DTE_TME");
        icw_Efm_Document_Kdo_Crte_Dte_Tme.setDdmHeader("K D O/DATE AND TIME");
        icw_Efm_Document_Orgnl_Cabinet = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Orgnl_Cabinet", "ORGNL-CABINET", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "ORGNL_CABINET");
        registerRecord(vw_icw_Efm_Document);

        vw_icw_Efm_Document_2 = new DataAccessProgramView(new NameInfo("vw_icw_Efm_Document_2", "ICW-EFM-DOCUMENT-2"), "ICW_EFM_DOCUMENT", "ICW_EFM_DOCUMENT");
        icw_Efm_Document_2_Cabinet_Id = vw_icw_Efm_Document_2.getRecord().newFieldInGroup("icw_Efm_Document_2_Cabinet_Id", "CABINET-ID", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "CABINET_ID");
        icw_Efm_Document_2_Cabinet_Id.setDdmHeader("CABINET/ID");

        icw_Efm_Document_2__R_Field_12 = vw_icw_Efm_Document_2.getRecord().newGroupInGroup("icw_Efm_Document_2__R_Field_12", "REDEFINE", icw_Efm_Document_2_Cabinet_Id);
        icw_Efm_Document_2_Iis_Hrchy_Level_Cde = icw_Efm_Document_2__R_Field_12.newFieldInGroup("icw_Efm_Document_2_Iis_Hrchy_Level_Cde", "IIS-HRCHY-LEVEL-CDE", 
            FieldType.NUMERIC, 6);
        icw_Efm_Document_2_Ppg_Cde = icw_Efm_Document_2__R_Field_12.newFieldInGroup("icw_Efm_Document_2_Ppg_Cde", "PPG-CDE", FieldType.STRING, 6);
        icw_Efm_Document_2_Rqst_Log_Dte_Tme = vw_icw_Efm_Document_2.getRecord().newFieldInGroup("icw_Efm_Document_2_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        icw_Efm_Document_2_Rqst_Log_Dte_Tme.setDdmHeader("MIT RQST/DATE AND TIME");
        icw_Efm_Document_2_Crte_Dte_Tme = vw_icw_Efm_Document_2.getRecord().newFieldInGroup("icw_Efm_Document_2_Crte_Dte_Tme", "CRTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "CRTE_DTE_TME");
        icw_Efm_Document_2_Crte_Dte_Tme.setDdmHeader("CREATE/DATE AND TIME");
        icw_Efm_Document_2_Crte_Sqnce_Nbr = vw_icw_Efm_Document_2.getRecord().newFieldInGroup("icw_Efm_Document_2_Crte_Sqnce_Nbr", "CRTE-SQNCE-NBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "CRTE_SQNCE_NBR");
        icw_Efm_Document_2_Crte_Empl_Racf_Id = vw_icw_Efm_Document_2.getRecord().newFieldInGroup("icw_Efm_Document_2_Crte_Empl_Racf_Id", "CRTE-EMPL-RACF-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CRTE_EMPL_RACF_ID");
        icw_Efm_Document_2_Crte_Empl_Racf_Id.setDdmHeader("CREATED BY/RACF ID");
        icw_Efm_Document_2_Crte_Unit = vw_icw_Efm_Document_2.getRecord().newFieldInGroup("icw_Efm_Document_2_Crte_Unit", "CRTE-UNIT", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CRTE_UNIT");
        icw_Efm_Document_2_Crte_Unit.setDdmHeader("CREATED BY/UNIT");
        icw_Efm_Document_2_Crte_System = vw_icw_Efm_Document_2.getRecord().newFieldInGroup("icw_Efm_Document_2_Crte_System", "CRTE-SYSTEM", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CRTE_SYSTEM");
        icw_Efm_Document_2_Crte_System.setDdmHeader("CREATED BY/SYSTEM");
        icw_Efm_Document_2_Crte_Jobname = vw_icw_Efm_Document_2.getRecord().newFieldInGroup("icw_Efm_Document_2_Crte_Jobname", "CRTE-JOBNAME", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CRTE_JOBNAME");
        icw_Efm_Document_2_Crte_Jobname.setDdmHeader("CREATED BY/JOBNAME");

        icw_Efm_Document_2_Dcmnt_Type = vw_icw_Efm_Document_2.getRecord().newGroupInGroup("ICW_EFM_DOCUMENT_2_DCMNT_TYPE", "DCMNT-TYPE");
        icw_Efm_Document_2_Dcmnt_Type.setDdmHeader("DOCUMENT/TYPE");
        icw_Efm_Document_2_Dcmnt_Ctgry = icw_Efm_Document_2_Dcmnt_Type.newFieldInGroup("icw_Efm_Document_2_Dcmnt_Ctgry", "DCMNT-CTGRY", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DCMNT_CTGRY");
        icw_Efm_Document_2_Dcmnt_Ctgry.setDdmHeader("DOC/CAT");
        icw_Efm_Document_2_Dcmnt_Sub_Ctgry = icw_Efm_Document_2_Dcmnt_Type.newFieldInGroup("icw_Efm_Document_2_Dcmnt_Sub_Ctgry", "DCMNT-SUB-CTGRY", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "DCMNT_SUB_CTGRY");
        icw_Efm_Document_2_Dcmnt_Sub_Ctgry.setDdmHeader("DOC SUB-/CATEGORY");
        icw_Efm_Document_2_Dcmnt_Dtl = icw_Efm_Document_2_Dcmnt_Type.newFieldInGroup("icw_Efm_Document_2_Dcmnt_Dtl", "DCMNT-DTL", FieldType.STRING, 4, 
            RepeatingFieldStrategy.None, "DCMNT_DTL");
        icw_Efm_Document_2_Dcmnt_Dtl.setDdmHeader("DOCUMENT/DETAIL");
        icw_Efm_Document_2_Dcmnt_Drctn = vw_icw_Efm_Document_2.getRecord().newFieldInGroup("icw_Efm_Document_2_Dcmnt_Drctn", "DCMNT-DRCTN", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DCMNT_DRCTN");
        icw_Efm_Document_2_Dcmnt_Drctn.setDdmHeader("DOC/DIR");
        icw_Efm_Document_2_Dcmnt_Scrty_Ind = vw_icw_Efm_Document_2.getRecord().newFieldInGroup("icw_Efm_Document_2_Dcmnt_Scrty_Ind", "DCMNT-SCRTY-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "DCMNT_SCRTY_IND");
        icw_Efm_Document_2_Dcmnt_Scrty_Ind.setDdmHeader("SECRD/DOC");
        icw_Efm_Document_2_Dcmnt_Rtntn = vw_icw_Efm_Document_2.getRecord().newFieldInGroup("icw_Efm_Document_2_Dcmnt_Rtntn", "DCMNT-RTNTN", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DCMNT_RTNTN");
        icw_Efm_Document_2_Dcmnt_Rtntn.setDdmHeader("DOC/RETNTN");
        icw_Efm_Document_2_Dcmnt_Frmt = vw_icw_Efm_Document_2.getRecord().newFieldInGroup("icw_Efm_Document_2_Dcmnt_Frmt", "DCMNT-FRMT", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "DCMNT_FRMT");
        icw_Efm_Document_2_Dcmnt_Anttn_Ind = vw_icw_Efm_Document_2.getRecord().newFieldInGroup("icw_Efm_Document_2_Dcmnt_Anttn_Ind", "DCMNT-ANTTN-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "DCMNT_ANTTN_IND");
        icw_Efm_Document_2_Dcmnt_Anttn_Ind.setDdmHeader("ANOTATION/IND");
        icw_Efm_Document_2_Dcmnt_Copy_Ind = vw_icw_Efm_Document_2.getRecord().newFieldInGroup("icw_Efm_Document_2_Dcmnt_Copy_Ind", "DCMNT-COPY-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DCMNT_COPY_IND");
        icw_Efm_Document_2_Dcmnt_Copy_Ind.setDdmHeader("COPY/IND");
        icw_Efm_Document_2_Invrt_Crte_Dte_Tme = vw_icw_Efm_Document_2.getRecord().newFieldInGroup("icw_Efm_Document_2_Invrt_Crte_Dte_Tme", "INVRT-CRTE-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "INVRT_CRTE_DTE_TME");
        icw_Efm_Document_2_Invrt_Crte_Dte_Tme.setDdmHeader("INVERTED CREATE/DATE AND TIME");

        icw_Efm_Document_2_Audit_Data = vw_icw_Efm_Document_2.getRecord().newGroupInGroup("ICW_EFM_DOCUMENT_2_AUDIT_DATA", "AUDIT-DATA");
        icw_Efm_Document_2_Audit_Action = icw_Efm_Document_2_Audit_Data.newFieldInGroup("icw_Efm_Document_2_Audit_Action", "AUDIT-ACTION", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "AUDIT_ACTION");
        icw_Efm_Document_2_Audit_Action.setDdmHeader("ACTION");
        icw_Efm_Document_2_Audit_Empl_Racf_Id = icw_Efm_Document_2_Audit_Data.newFieldInGroup("icw_Efm_Document_2_Audit_Empl_Racf_Id", "AUDIT-EMPL-RACF-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "AUDIT_EMPL_RACF_ID");
        icw_Efm_Document_2_Audit_Empl_Racf_Id.setDdmHeader("EMPLOYEE/RACF ID");
        icw_Efm_Document_2_Audit_System = icw_Efm_Document_2_Audit_Data.newFieldInGroup("icw_Efm_Document_2_Audit_System", "AUDIT-SYSTEM", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "AUDIT_SYSTEM");
        icw_Efm_Document_2_Audit_System.setDdmHeader("AUDIT/SYSTEM");
        icw_Efm_Document_2_Audit_Unit = icw_Efm_Document_2_Audit_Data.newFieldInGroup("icw_Efm_Document_2_Audit_Unit", "AUDIT-UNIT", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "AUDIT_UNIT");
        icw_Efm_Document_2_Audit_Unit.setDdmHeader("AUDIT/UNIT");
        icw_Efm_Document_2_Audit_Jobname = icw_Efm_Document_2_Audit_Data.newFieldInGroup("icw_Efm_Document_2_Audit_Jobname", "AUDIT-JOBNAME", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "AUDIT_JOBNAME");
        icw_Efm_Document_2_Audit_Jobname.setDdmHeader("JOBNAME");
        icw_Efm_Document_2_Audit_Dte_Tme = icw_Efm_Document_2_Audit_Data.newFieldInGroup("icw_Efm_Document_2_Audit_Dte_Tme", "AUDIT-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "AUDIT_DTE_TME");
        icw_Efm_Document_2_Audit_Dte_Tme.setDdmHeader("DATE/& TIME");
        icw_Efm_Document_2_Text_Dte_Tme = vw_icw_Efm_Document_2.getRecord().newFieldInGroup("icw_Efm_Document_2_Text_Dte_Tme", "TEXT-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TEXT_DTE_TME");
        icw_Efm_Document_2_Text_Dte_Tme.setDdmHeader("TEXT/DATE AND TIME");
        icw_Efm_Document_2_Image_Source_Id = vw_icw_Efm_Document_2.getRecord().newFieldInGroup("icw_Efm_Document_2_Image_Source_Id", "IMAGE-SOURCE-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "IMAGE_SOURCE_ID");
        icw_Efm_Document_2_Image_Source_Id.setDdmHeader("IMAGE/SOURCE ID");
        icw_Efm_Document_2_Image_Min = vw_icw_Efm_Document_2.getRecord().newFieldInGroup("icw_Efm_Document_2_Image_Min", "IMAGE-MIN", FieldType.STRING, 
            14, RepeatingFieldStrategy.None, "IMAGE_MIN");
        icw_Efm_Document_2_Image_Min.setDdmHeader("IMAGE/POINTER");

        icw_Efm_Document_2__R_Field_13 = vw_icw_Efm_Document_2.getRecord().newGroupInGroup("icw_Efm_Document_2__R_Field_13", "REDEFINE", icw_Efm_Document_2_Image_Min);
        icw_Efm_Document_2_Image_Batch = icw_Efm_Document_2__R_Field_13.newFieldInGroup("icw_Efm_Document_2_Image_Batch", "IMAGE-BATCH", FieldType.NUMERIC, 
            8);

        icw_Efm_Document_2__R_Field_14 = vw_icw_Efm_Document_2.getRecord().newGroupInGroup("icw_Efm_Document_2__R_Field_14", "REDEFINE", icw_Efm_Document_2_Image_Min);
        icw_Efm_Document_2_Dcmt_Min = icw_Efm_Document_2__R_Field_14.newFieldInGroup("icw_Efm_Document_2_Dcmt_Min", "DCMT-MIN", FieldType.NUMERIC, 11);
        icw_Efm_Document_2_Dcmt_St_Page_No = icw_Efm_Document_2__R_Field_14.newFieldInGroup("icw_Efm_Document_2_Dcmt_St_Page_No", "DCMT-ST-PAGE-NO", FieldType.NUMERIC, 
            3);
        icw_Efm_Document_2_Image_End_Page = vw_icw_Efm_Document_2.getRecord().newFieldInGroup("icw_Efm_Document_2_Image_End_Page", "IMAGE-END-PAGE", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "IMAGE_END_PAGE");
        icw_Efm_Document_2_Image_End_Page.setDdmHeader("IMAGE/END PAGE");
        icw_Efm_Document_2_Image_Ornttn = vw_icw_Efm_Document_2.getRecord().newFieldInGroup("icw_Efm_Document_2_Image_Ornttn", "IMAGE-ORNTTN", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "IMAGE_ORNTTN");
        icw_Efm_Document_2_Image_Ornttn.setDdmHeader("DOC/ORIENT");
        icw_Efm_Document_2_Kdo_Crte_Dte_Tme = vw_icw_Efm_Document_2.getRecord().newFieldInGroup("icw_Efm_Document_2_Kdo_Crte_Dte_Tme", "KDO-CRTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "KDO_CRTE_DTE_TME");
        icw_Efm_Document_2_Kdo_Crte_Dte_Tme.setDdmHeader("K D O/DATE AND TIME");
        icw_Efm_Document_2_Orgnl_Cabinet = vw_icw_Efm_Document_2.getRecord().newFieldInGroup("icw_Efm_Document_2_Orgnl_Cabinet", "ORGNL-CABINET", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "ORGNL_CABINET");
        registerRecord(vw_icw_Efm_Document_2);

        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.PACKED_DECIMAL, 3);
        pnd_Lines_Reqd = localVariables.newFieldInRecord("pnd_Lines_Reqd", "#LINES-REQD", FieldType.PACKED_DECIMAL, 3);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Tot_Pin_Conflict = localVariables.newFieldInRecord("pnd_Tot_Pin_Conflict", "#TOT-PIN-CONFLICT", FieldType.NUMERIC, 6);
        pnd_Tot_Pge_Conflict = localVariables.newFieldInRecord("pnd_Tot_Pge_Conflict", "#TOT-PGE-CONFLICT", FieldType.NUMERIC, 6);
        pnd_Tot_No_Pg_1 = localVariables.newFieldInRecord("pnd_Tot_No_Pg_1", "#TOT-NO-PG-1", FieldType.NUMERIC, 6);
        pnd_Tot_End_Pg_Noeql = localVariables.newFieldInRecord("pnd_Tot_End_Pg_Noeql", "#TOT-END-PG-NOEQL", FieldType.NUMERIC, 6);
        pnd_Source_Id = localVariables.newFieldInRecord("pnd_Source_Id", "#SOURCE-ID", FieldType.STRING, 6);
        pnd_Unidentified_Line = localVariables.newFieldInRecord("pnd_Unidentified_Line", "#UNIDENTIFIED-LINE", FieldType.STRING, 66);

        pnd_Unidentified_Line__R_Field_15 = localVariables.newGroupInRecord("pnd_Unidentified_Line__R_Field_15", "REDEFINE", pnd_Unidentified_Line);

        pnd_Unidentified_Line_Pnd_Min_Array = pnd_Unidentified_Line__R_Field_15.newGroupArrayInGroup("pnd_Unidentified_Line_Pnd_Min_Array", "#MIN-ARRAY", 
            new DbsArrayController(1, 4));
        pnd_Unidentified_Line_Pnd_Print_Min = pnd_Unidentified_Line_Pnd_Min_Array.newFieldInGroup("pnd_Unidentified_Line_Pnd_Print_Min", "#PRINT-MIN", 
            FieldType.NUMERIC, 11);
        pnd_Unidentified_Line_Pnd_Filler = pnd_Unidentified_Line_Pnd_Min_Array.newFieldInGroup("pnd_Unidentified_Line_Pnd_Filler", "#FILLER", FieldType.STRING, 
            3);
        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_16 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_16", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_16.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_16.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_16.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind = pnd_Tbl_Prime_Key__R_Field_16.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", FieldType.STRING, 
            1);
        pnd_Input_Source_Id = localVariables.newFieldInRecord("pnd_Input_Source_Id", "#INPUT-SOURCE-ID", FieldType.STRING, 6);
        pnd_Input_Data = localVariables.newFieldInRecord("pnd_Input_Data", "#INPUT-DATA", FieldType.STRING, 60);

        pnd_Input_Data__R_Field_17 = localVariables.newGroupInRecord("pnd_Input_Data__R_Field_17", "REDEFINE", pnd_Input_Data);
        pnd_Input_Data_Pnd_Input_Time_Start = pnd_Input_Data__R_Field_17.newFieldInGroup("pnd_Input_Data_Pnd_Input_Time_Start", "#INPUT-TIME-START", FieldType.STRING, 
            6);
        pnd_Input_Data_Pnd_Input_Time_End = pnd_Input_Data__R_Field_17.newFieldInGroup("pnd_Input_Data_Pnd_Input_Time_End", "#INPUT-TIME-END", FieldType.STRING, 
            6);
        pnd_Input_Data_Pnd_Input_Date = pnd_Input_Data__R_Field_17.newFieldInGroup("pnd_Input_Data_Pnd_Input_Date", "#INPUT-DATE", FieldType.STRING, 8);
        pnd_Input_Date_Time = localVariables.newFieldInRecord("pnd_Input_Date_Time", "#INPUT-DATE-TIME", FieldType.STRING, 14);
        pnd_Input_Date_D = localVariables.newFieldInRecord("pnd_Input_Date_D", "#INPUT-DATE-D", FieldType.DATE);
        pnd_Start_Key = localVariables.newFieldInRecord("pnd_Start_Key", "#START-KEY", FieldType.TIME);
        pnd_End_Key = localVariables.newFieldInRecord("pnd_End_Key", "#END-KEY", FieldType.TIME);
        pnd_Upld_Time = localVariables.newFieldInRecord("pnd_Upld_Time", "#UPLD-TIME", FieldType.STRING, 4);

        pnd_Upld_Time__R_Field_18 = localVariables.newGroupInRecord("pnd_Upld_Time__R_Field_18", "REDEFINE", pnd_Upld_Time);
        pnd_Upld_Time_Pnd_Upld_Timen = pnd_Upld_Time__R_Field_18.newFieldInGroup("pnd_Upld_Time_Pnd_Upld_Timen", "#UPLD-TIMEN", FieldType.NUMERIC, 4);
        pnd_Isn_Tbl = localVariables.newFieldInRecord("pnd_Isn_Tbl", "#ISN-TBL", FieldType.PACKED_DECIMAL, 10);
        pnd_Start_Date = localVariables.newFieldInRecord("pnd_Start_Date", "#START-DATE", FieldType.DATE);
        pnd_Next_Processing_Date = localVariables.newFieldInRecord("pnd_Next_Processing_Date", "#NEXT-PROCESSING-DATE", FieldType.DATE);
        pnd_Orig_Mm = localVariables.newFieldInRecord("pnd_Orig_Mm", "#ORIG-MM", FieldType.STRING, 2);
        pnd_New_Mm = localVariables.newFieldInRecord("pnd_New_Mm", "#NEW-MM", FieldType.STRING, 2);
        pnd_Image_Pointer_Key = localVariables.newFieldInRecord("pnd_Image_Pointer_Key", "#IMAGE-POINTER-KEY", FieldType.STRING, 17);

        pnd_Image_Pointer_Key__R_Field_19 = localVariables.newGroupInRecord("pnd_Image_Pointer_Key__R_Field_19", "REDEFINE", pnd_Image_Pointer_Key);
        pnd_Image_Pointer_Key_Pnd_Image_Source = pnd_Image_Pointer_Key__R_Field_19.newFieldInGroup("pnd_Image_Pointer_Key_Pnd_Image_Source", "#IMAGE-SOURCE", 
            FieldType.STRING, 6);
        pnd_Image_Pointer_Key_Pnd_Image_Batch = pnd_Image_Pointer_Key__R_Field_19.newFieldInGroup("pnd_Image_Pointer_Key_Pnd_Image_Batch", "#IMAGE-BATCH", 
            FieldType.NUMERIC, 8);
        pnd_Dcmt_Source_Id_Min_Key = localVariables.newFieldInRecord("pnd_Dcmt_Source_Id_Min_Key", "#DCMT-SOURCE-ID-MIN-KEY", FieldType.STRING, 17);

        pnd_Dcmt_Source_Id_Min_Key__R_Field_20 = localVariables.newGroupInRecord("pnd_Dcmt_Source_Id_Min_Key__R_Field_20", "REDEFINE", pnd_Dcmt_Source_Id_Min_Key);
        pnd_Dcmt_Source_Id_Min_Key_Pnd_Dcmt_Source_Id = pnd_Dcmt_Source_Id_Min_Key__R_Field_20.newFieldInGroup("pnd_Dcmt_Source_Id_Min_Key_Pnd_Dcmt_Source_Id", 
            "#DCMT-SOURCE-ID", FieldType.STRING, 6);
        pnd_Dcmt_Source_Id_Min_Key_Pnd_Dcmt_Min = pnd_Dcmt_Source_Id_Min_Key__R_Field_20.newFieldInGroup("pnd_Dcmt_Source_Id_Min_Key_Pnd_Dcmt_Min", "#DCMT-MIN", 
            FieldType.NUMERIC, 11);
        pnd_Conflict_Desc = localVariables.newFieldInRecord("pnd_Conflict_Desc", "#CONFLICT-DESC", FieldType.STRING, 25);
        pnd_Nothing_To_Rpt = localVariables.newFieldInRecord("pnd_Nothing_To_Rpt", "#NOTHING-TO-RPT", FieldType.BOOLEAN, 1);
        pnd_Prev_Min = localVariables.newFieldInRecord("pnd_Prev_Min", "#PREV-MIN", FieldType.NUMERIC, 11);
        pnd_Page_One_Exists = localVariables.newFieldInRecord("pnd_Page_One_Exists", "#PAGE-ONE-EXISTS", FieldType.BOOLEAN, 1);
        pnd_Old_St_Page = localVariables.newFieldInRecord("pnd_Old_St_Page", "#OLD-ST-PAGE", FieldType.NUMERIC, 3);
        pnd_Old_End_Page = localVariables.newFieldInRecord("pnd_Old_End_Page", "#OLD-END-PAGE", FieldType.NUMERIC, 3);
        pnd_Exact_Image_Pointer_Key = localVariables.newFieldInRecord("pnd_Exact_Image_Pointer_Key", "#EXACT-IMAGE-POINTER-KEY", FieldType.STRING, 17);

        pnd_Exact_Image_Pointer_Key__R_Field_21 = localVariables.newGroupInRecord("pnd_Exact_Image_Pointer_Key__R_Field_21", "REDEFINE", pnd_Exact_Image_Pointer_Key);
        pnd_Exact_Image_Pointer_Key_Pnd_Exact_Image_Source = pnd_Exact_Image_Pointer_Key__R_Field_21.newFieldInGroup("pnd_Exact_Image_Pointer_Key_Pnd_Exact_Image_Source", 
            "#EXACT-IMAGE-SOURCE", FieldType.STRING, 6);
        pnd_Exact_Image_Pointer_Key_Pnd_Exact_Image_Min = pnd_Exact_Image_Pointer_Key__R_Field_21.newFieldInGroup("pnd_Exact_Image_Pointer_Key_Pnd_Exact_Image_Min", 
            "#EXACT-IMAGE-MIN", FieldType.NUMERIC, 11);
        pnd_Batch_Found_In_Cwf = localVariables.newFieldInRecord("pnd_Batch_Found_In_Cwf", "#BATCH-FOUND-IN-CWF", FieldType.BOOLEAN, 1);
        pnd_End_Pg_Cnflct_Processed = localVariables.newFieldInRecord("pnd_End_Pg_Cnflct_Processed", "#END-PG-CNFLCT-PROCESSED", FieldType.BOOLEAN, 1);
        pnd_Ppg_Code_Arr = localVariables.newFieldArrayInRecord("pnd_Ppg_Code_Arr", "#PPG-CODE-ARR", FieldType.STRING, 6, new DbsArrayController(1, 30));
        pnd_Ppg_Ctr = localVariables.newFieldInRecord("pnd_Ppg_Ctr", "#PPG-CTR", FieldType.INTEGER, 1);
        pnd_Ppg_Conflict = localVariables.newFieldInRecord("pnd_Ppg_Conflict", "#PPG-CONFLICT", FieldType.BOOLEAN, 1);
        pnd_Record = localVariables.newFieldInRecord("pnd_Record", "#RECORD", FieldType.STRING, 54);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        sort01Source_IdOld = internalLoopRecord.newFieldInRecord("Sort01_Source_Id_OLD", "Source_Id_OLD", FieldType.STRING, 6);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Efm_Document.reset();
        vw_cwf_Efm_Document_2.reset();
        vw_cwf_Image_Xref.reset();
        vw_cwf_Support_Tbl.reset();
        vw_icw_Efm_Document.reset();
        vw_icw_Efm_Document_2.reset();
        internalLoopRecord.reset();

        ldaEfsl0040.initializeValues();

        localVariables.reset();
        pnd_Nothing_To_Rpt.setInitialValue(true);
        pnd_Page_One_Exists.setInitialValue(false);
        pnd_Batch_Found_In_Cwf.setInitialValue(false);
        pnd_End_Pg_Cnflct_Processed.setInitialValue(false);
        pnd_Ppg_Conflict.setInitialValue(false);
        pnd_Record.setInitialValue("�E�&l1o2a5.6c72p3e66F�&a2L�(9905X�&l0L");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Efsp0065() throws Exception
    {
        super("Efsp0065");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Efsp0065|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        while(true)
        {
            try
            {
                getReports().definePrinter(2, "AUDIT");                                                                                                                   //Natural: DEFINE PRINTER ( AUDIT = 1 )
                pnd_Program.setValue(Global.getPROGRAM());                                                                                                                //Natural: FORMAT ( AUDIT ) LS = 133 PS = 60 ZP = OFF;//Natural: ASSIGN #PROGRAM := *PROGRAM
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Source_Id,pnd_Input_Data);                                                                   //Natural: INPUT #INPUT-SOURCE-ID #INPUT-DATA
                pnd_Input_Date_Time.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Input_Data_Pnd_Input_Date, pnd_Input_Data_Pnd_Input_Time_Start));        //Natural: COMPRESS #INPUT-DATE #INPUT-TIME-START INTO #INPUT-DATE-TIME LEAVING NO SPACE
                pnd_Start_Key.setValueEdited(new ReportEditMask("YYYYMMDDHHIISS"),pnd_Input_Date_Time);                                                                   //Natural: MOVE EDITED #INPUT-DATE-TIME TO #START-KEY ( EM = YYYYMMDDHHIISS )
                pnd_Input_Date_Time.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Input_Data_Pnd_Input_Date, pnd_Input_Data_Pnd_Input_Time_End));          //Natural: COMPRESS #INPUT-DATE #INPUT-TIME-END INTO #INPUT-DATE-TIME LEAVING NO SPACE
                pnd_End_Key.setValueEdited(new ReportEditMask("YYYYMMDDHHIISS"),pnd_Input_Date_Time);                                                                     //Natural: MOVE EDITED #INPUT-DATE-TIME TO #END-KEY ( EM = YYYYMMDDHHIISS )
                //*  ADD 1 TO THE DAY COUNT
                pnd_End_Key.nadd(864000);                                                                                                                                 //Natural: ADD 864000 TO #END-KEY
                pnd_Input_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Data_Pnd_Input_Date);                                                            //Natural: MOVE EDITED #INPUT-DATE TO #INPUT-DATE-D ( EM = YYYYMMDD )
                //*  READ THE AUDIT RECORDS FOR A SELECTED DAY
                ldaEfsl0040.getVw_cwf_Upload_Audit().startDatabaseRead                                                                                                    //Natural: READ CWF-UPLOAD-AUDIT BY UPLD-DATE-TIME STARTING FROM #START-KEY ENDING AT #END-KEY
                (
                "READ_AUDIT",
                new Wc[] { new Wc("UPLD_DATE_TIME", ">=", pnd_Start_Key, "And", WcType.BY) ,
                new Wc("UPLD_DATE_TIME", "<=", pnd_End_Key, WcType.BY) },
                new Oc[] { new Oc("UPLD_DATE_TIME", "ASC") }
                );
                READ_AUDIT:
                while (condition(ldaEfsl0040.getVw_cwf_Upload_Audit().readNextRow("READ_AUDIT")))
                {
                    if (condition(ldaEfsl0040.getCwf_Upload_Audit_Source_Id().notEquals(pnd_Input_Source_Id)))                                                            //Natural: REJECT IF CWF-UPLOAD-AUDIT.SOURCE-ID NE #INPUT-SOURCE-ID
                    {
                        continue;
                    }
                    getSort().writeSortInData(ldaEfsl0040.getCwf_Upload_Audit_Source_Id(), ldaEfsl0040.getCwf_Upload_Audit_Batch_Nbr(), ldaEfsl0040.getCwf_Upload_Audit_Batch_Ext_Nbr(),  //Natural: END-ALL
                        ldaEfsl0040.getCwf_Upload_Audit_Upld_Date_Time());
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                getSort().sortData(ldaEfsl0040.getCwf_Upload_Audit_Source_Id(), ldaEfsl0040.getCwf_Upload_Audit_Batch_Nbr(), ldaEfsl0040.getCwf_Upload_Audit_Batch_Ext_Nbr(),  //Natural: SORT BY CWF-UPLOAD-AUDIT.SOURCE-ID CWF-UPLOAD-AUDIT.BATCH-NBR CWF-UPLOAD-AUDIT.BATCH-EXT-NBR CWF-UPLOAD-AUDIT.UPLD-DATE-TIME USING KEYS
                    ldaEfsl0040.getCwf_Upload_Audit_Upld_Date_Time());
                boolean endOfDataSort01 = true;
                boolean firstSort01 = true;
                SORT01:
                while (condition(getSort().readSortOutData(ldaEfsl0040.getCwf_Upload_Audit_Source_Id(), ldaEfsl0040.getCwf_Upload_Audit_Batch_Nbr(), ldaEfsl0040.getCwf_Upload_Audit_Batch_Ext_Nbr(), 
                    ldaEfsl0040.getCwf_Upload_Audit_Upld_Date_Time())))
                {
                    if (condition(getSort().getAstCOUNTER().greater(0)))
                    {
                        atBreakEventSort01(false);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom()))
                                break;
                            else if (condition(Global.isEscapeBottomImmediate()))
                            {
                                endOfDataSort01 = false;
                                break;
                            }
                            else if (condition(Global.isEscapeTop()))
                            continue;
                            else if (condition())
                            return;
                        }
                    }
                    //*                                                                                                                                                   //Natural: AT BREAK OF CWF-UPLOAD-AUDIT.SOURCE-ID
                    if (condition(pnd_Image_Pointer_Key_Pnd_Image_Source.equals(ldaEfsl0040.getCwf_Upload_Audit_Source_Id()) && pnd_Image_Pointer_Key_Pnd_Image_Batch.equals(ldaEfsl0040.getCwf_Upload_Audit_Batch_Nbr()))) //Natural: IF #IMAGE-SOURCE = CWF-UPLOAD-AUDIT.SOURCE-ID AND #IMAGE-BATCH = CWF-UPLOAD-AUDIT.BATCH-NBR
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaEfsl0040.getCwf_Upload_Audit_Source_Id().equals("ICIRS")))                                                                           //Natural: IF CWF-UPLOAD-AUDIT.SOURCE-ID = 'ICIRS'
                    {
                        //*  ICW CONFLICTS
                                                                                                                                                                          //Natural: PERFORM PROCESS-ICW-CONFLICTS
                        sub_Process_Icw_Conflicts();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  CWF CONFLICTS
                                                                                                                                                                          //Natural: PERFORM PROCESS-CWF-CONFLICTS
                        sub_Process_Cwf_Conflicts();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        if (condition(ldaEfsl0040.getCwf_Upload_Audit_Source_Id().equals("DEN") && ! (pnd_Batch_Found_In_Cwf.getBoolean())))                              //Natural: IF CWF-UPLOAD-AUDIT.SOURCE-ID = 'DEN' AND NOT #BATCH-FOUND-IN-CWF
                        {
                            //*  CWF CONFLICTS
                                                                                                                                                                          //Natural: PERFORM PROCESS-ICW-CONFLICTS
                            sub_Process_Icw_Conflicts();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    sort01Source_IdOld.setValue(ldaEfsl0040.getCwf_Upload_Audit_Source_Id());                                                                             //Natural: END-SORT
                }
                if (condition(getSort().getAstCOUNTER().greater(0)))
                {
                    atBreakEventSort01(endOfDataSort01);
                }
                endSort();
                if (condition(pnd_Nothing_To_Rpt.getBoolean()))                                                                                                           //Natural: IF #NOTHING-TO-RPT
                {
                    getReports().write(2, ReportOption.NOTITLE,"Nothing to report.");                                                                                     //Natural: WRITE ( AUDIT ) NOTITLE 'Nothing to report.'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( AUDIT )
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-CWF-CONFLICTS
                //*          010T CWF-EFM-DOCUMENT-2.DCMT-MIN           (AD=ODL)
                //*          057T CWF-IMAGE-XREF.NUMBER-OF-PAGES        (AD=ODL)
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-ICW-CONFLICTS
                //* ***********************************************************************
                //*          010T ICW-EFM-DOCUMENT-2.DCMT-MIN          (AD=ODL)
                //*          057T CWF-IMAGE-XREF.NUMBER-OF-PAGES       (AD=ODL)
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CWF-CHECK-DOC-FOR-PG-CONFLICT
                //*    010T CWF-EFM-DOCUMENT-2.DCMT-MIN           (AD=ODL)
                //*    023T #OLD-ST-PAGE                          (AD=ODL)
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ICW-CHECK-DOC-FOR-PG-CONFLICT
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Process_Cwf_Conflicts() throws Exception                                                                                                             //Natural: PROCESS-CWF-CONFLICTS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Batch_Found_In_Cwf.resetInitial();                                                                                                                            //Natural: RESET INITIAL #BATCH-FOUND-IN-CWF
        pnd_Image_Pointer_Key_Pnd_Image_Source.setValue(ldaEfsl0040.getCwf_Upload_Audit_Source_Id());                                                                     //Natural: ASSIGN #IMAGE-SOURCE := CWF-UPLOAD-AUDIT.SOURCE-ID
        pnd_Image_Pointer_Key_Pnd_Image_Batch.setValue(ldaEfsl0040.getCwf_Upload_Audit_Batch_Nbr());                                                                      //Natural: ASSIGN #IMAGE-BATCH := CWF-UPLOAD-AUDIT.BATCH-NBR
        vw_cwf_Efm_Document.startDatabaseRead                                                                                                                             //Natural: READ CWF-EFM-DOCUMENT BY IMAGE-POINTER-KEY STARTING FROM #IMAGE-POINTER-KEY
        (
        "READ01",
        new Wc[] { new Wc("IMAGE_POINTER_KEY", ">=", pnd_Image_Pointer_Key, WcType.BY) },
        new Oc[] { new Oc("IMAGE_POINTER_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_cwf_Efm_Document.readNextRow("READ01")))
        {
            if (condition(cwf_Efm_Document_Image_Source_Id.notEquals(pnd_Image_Pointer_Key_Pnd_Image_Source) || cwf_Efm_Document_Image_Batch.notEquals(pnd_Image_Pointer_Key_Pnd_Image_Batch))) //Natural: IF CWF-EFM-DOCUMENT.IMAGE-SOURCE-ID NE #IMAGE-SOURCE OR CWF-EFM-DOCUMENT.IMAGE-BATCH NE #IMAGE-BATCH
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Image_Pointer_Key_Pnd_Image_Source.equals("DEN")))                                                                                          //Natural: IF #IMAGE-SOURCE = 'DEN'
            {
                pnd_Batch_Found_In_Cwf.setValue(true);                                                                                                                    //Natural: ASSIGN #BATCH-FOUND-IN-CWF := TRUE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cwf_Efm_Document_Document_Retention_Ind.equals("D")))                                                                                           //Natural: REJECT IF CWF-EFM-DOCUMENT.DOCUMENT-RETENTION-IND = 'D'
            {
                continue;
            }
            if (condition(pnd_Prev_Min.notEquals(cwf_Efm_Document_Dcmt_Min)))                                                                                             //Natural: IF #PREV-MIN NE DCMT-MIN
            {
                pnd_Exact_Image_Pointer_Key_Pnd_Exact_Image_Source.setValue(cwf_Efm_Document_Image_Source_Id);                                                            //Natural: ASSIGN #EXACT-IMAGE-SOURCE := CWF-EFM-DOCUMENT.IMAGE-SOURCE-ID
                pnd_Exact_Image_Pointer_Key_Pnd_Exact_Image_Min.setValue(cwf_Efm_Document_Dcmt_Min);                                                                      //Natural: ASSIGN #EXACT-IMAGE-MIN := CWF-EFM-DOCUMENT.DCMT-MIN
                                                                                                                                                                          //Natural: PERFORM CWF-CHECK-DOC-FOR-PG-CONFLICT
                sub_Cwf_Check_Doc_For_Pg_Conflict();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            pnd_Prev_Min.setValue(cwf_Efm_Document_Dcmt_Min);                                                                                                             //Natural: ASSIGN #PREV-MIN := DCMT-MIN
            pnd_Dcmt_Source_Id_Min_Key_Pnd_Dcmt_Source_Id.setValue(cwf_Efm_Document_Image_Source_Id);                                                                     //Natural: ASSIGN #DCMT-SOURCE-ID := CWF-EFM-DOCUMENT.IMAGE-SOURCE-ID
            pnd_Dcmt_Source_Id_Min_Key_Pnd_Dcmt_Min.setValue(cwf_Efm_Document_Dcmt_Min);                                                                                  //Natural: ASSIGN #DCMT-MIN := CWF-EFM-DOCUMENT.DCMT-MIN
            vw_cwf_Image_Xref.startDatabaseFind                                                                                                                           //Natural: FIND CWF-IMAGE-XREF WITH SOURCE-ID-MIN-KEY = #DCMT-SOURCE-ID-MIN-KEY
            (
            "FIND01",
            new Wc[] { new Wc("SOURCE_ID_MIN_KEY", "=", pnd_Dcmt_Source_Id_Min_Key, WcType.WITH) }
            );
            FIND01:
            while (condition(vw_cwf_Image_Xref.readNextRow("FIND01")))
            {
                vw_cwf_Image_Xref.setIfNotFoundControlFlag(false);
                if (condition(cwf_Efm_Document_Dcmt_St_Page_No.lessOrEqual(cwf_Image_Xref_Number_Of_Pages) && cwf_Efm_Document_Cabinet_Pin.equals(cwf_Image_Xref_Ph_Unique_Id_Nbr))) //Natural: IF CWF-EFM-DOCUMENT.DCMT-ST-PAGE-NO LE CWF-IMAGE-XREF.NUMBER-OF-PAGES AND CWF-EFM-DOCUMENT.CABINET-PIN = PH-UNIQUE-ID-NBR
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(! (pnd_End_Pg_Cnflct_Processed.getBoolean())))                                                                                              //Natural: IF NOT #END-PG-CNFLCT-PROCESSED
                {
                    if (condition(pnd_Old_End_Page.notEquals(cwf_Image_Xref_Number_Of_Pages)))                                                                            //Natural: IF #OLD-END-PAGE NE CWF-IMAGE-XREF.NUMBER-OF-PAGES
                    {
                        pnd_Tot_End_Pg_Noeql.nadd(1);                                                                                                                     //Natural: ADD 1 TO #TOT-END-PG-NOEQL
                        getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),cwf_Efm_Document_2_Cabinet_Pin, new FieldAttributes               //Natural: WRITE ( AUDIT ) NOTITLE NOHDR 001T CWF-EFM-DOCUMENT-2.CABINET-PIN ( AD = ODL ) 015T CWF-EFM-DOCUMENT-2.DCMT-MIN ( AD = ODL ) 029T #OLD-END-PAGE ( AD = ODL ) 034T CWF-EFM-DOCUMENT-2.ENTRY-EMPL-RACF-ID ( AD = ODL ) 045T CWF-IMAGE-XREF.PH-UNIQUE-ID-NBR ( AD = ODL ) 062T CWF-IMAGE-XREF.NUMBER-OF-PAGES ( AD = ODL ) 108T 'End Page Conflict'
                            ("AD=ODL"),new TabSetting(15),cwf_Efm_Document_2_Dcmt_Min, new FieldAttributes ("AD=ODL"),new TabSetting(29),pnd_Old_End_Page, 
                            new FieldAttributes ("AD=ODL"),new TabSetting(34),cwf_Efm_Document_2_Entry_Empl_Racf_Id, new FieldAttributes ("AD=ODL"),new 
                            TabSetting(45),cwf_Image_Xref_Ph_Unique_Id_Nbr, new FieldAttributes ("AD=ODL"),new TabSetting(62),cwf_Image_Xref_Number_Of_Pages, 
                            new FieldAttributes ("AD=ODL"),new TabSetting(108),"End Page Conflict");
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Nothing_To_Rpt.setValue(false);                                                                                                               //Natural: ASSIGN #NOTHING-TO-RPT := FALSE
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_End_Pg_Cnflct_Processed.setValue(true);                                                                                                           //Natural: ASSIGN #END-PG-CNFLCT-PROCESSED := TRUE
                }                                                                                                                                                         //Natural: END-IF
                short decideConditionsMet533 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CWF-EFM-DOCUMENT.DCMT-ST-PAGE-NO GT CWF-IMAGE-XREF.NUMBER-OF-PAGES AND ( CWF-EFM-DOCUMENT.CABINET-PIN NE PH-UNIQUE-ID-NBR AND PH-UNIQUE-ID-NBR NE 9898989 )
                if (condition(cwf_Efm_Document_Dcmt_St_Page_No.greater(cwf_Image_Xref_Number_Of_Pages) && cwf_Efm_Document_Cabinet_Pin.notEquals(cwf_Image_Xref_Ph_Unique_Id_Nbr) 
                    && cwf_Image_Xref_Ph_Unique_Id_Nbr.notEquals(9898989)))
                {
                    decideConditionsMet533++;
                    pnd_Tot_Pge_Conflict.nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOT-PGE-CONFLICT
                    pnd_Tot_Pin_Conflict.nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOT-PIN-CONFLICT
                    pnd_Conflict_Desc.setValue("PIN and Strt Pg Conflict");                                                                                               //Natural: ASSIGN #CONFLICT-DESC := 'PIN and Strt Pg Conflict'
                }                                                                                                                                                         //Natural: WHEN CWF-EFM-DOCUMENT.DCMT-ST-PAGE-NO GT CWF-IMAGE-XREF.NUMBER-OF-PAGES
                else if (condition(cwf_Efm_Document_Dcmt_St_Page_No.greater(cwf_Image_Xref_Number_Of_Pages)))
                {
                    decideConditionsMet533++;
                    pnd_Conflict_Desc.setValue("Start Pg Conflict");                                                                                                      //Natural: ASSIGN #CONFLICT-DESC := 'Start Pg Conflict'
                    pnd_Tot_Pge_Conflict.nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOT-PGE-CONFLICT
                }                                                                                                                                                         //Natural: WHEN CWF-EFM-DOCUMENT.CABINET-PIN NE PH-UNIQUE-ID-NBR AND PH-UNIQUE-ID-NBR NE 9898989
                else if (condition(cwf_Efm_Document_Cabinet_Pin.notEquals(cwf_Image_Xref_Ph_Unique_Id_Nbr) && cwf_Image_Xref_Ph_Unique_Id_Nbr.notEquals(9898989)))
                {
                    decideConditionsMet533++;
                    pnd_Conflict_Desc.setValue("PIN Conflict");                                                                                                           //Natural: ASSIGN #CONFLICT-DESC := 'PIN Conflict'
                    pnd_Tot_Pin_Conflict.nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOT-PIN-CONFLICT
                }                                                                                                                                                         //Natural: WHEN ANY
                if (condition(decideConditionsMet533 > 0))
                {
                    getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Efsf006c.class));                                                                  //Natural: WRITE ( AUDIT ) NOTITLE USING FORM 'EFSF006C'
                    pnd_Nothing_To_Rpt.setValue(false);                                                                                                                   //Natural: ASSIGN #NOTHING-TO-RPT := FALSE
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Process_Icw_Conflicts() throws Exception                                                                                                             //Natural: PROCESS-ICW-CONFLICTS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Image_Pointer_Key_Pnd_Image_Source.setValue(ldaEfsl0040.getCwf_Upload_Audit_Source_Id());                                                                     //Natural: ASSIGN #IMAGE-SOURCE := CWF-UPLOAD-AUDIT.SOURCE-ID
        pnd_Image_Pointer_Key_Pnd_Image_Batch.setValue(ldaEfsl0040.getCwf_Upload_Audit_Batch_Nbr());                                                                      //Natural: ASSIGN #IMAGE-BATCH := CWF-UPLOAD-AUDIT.BATCH-NBR
        vw_icw_Efm_Document.startDatabaseRead                                                                                                                             //Natural: READ ICW-EFM-DOCUMENT BY IMAGE-POINTER-KEY STARTING FROM #IMAGE-POINTER-KEY
        (
        "READ02",
        new Wc[] { new Wc("IMAGE_POINTER_KEY", ">=", pnd_Image_Pointer_Key, WcType.BY) },
        new Oc[] { new Oc("IMAGE_POINTER_KEY", "ASC") }
        );
        READ02:
        while (condition(vw_icw_Efm_Document.readNextRow("READ02")))
        {
            if (condition(icw_Efm_Document_Image_Source_Id.notEquals(pnd_Image_Pointer_Key_Pnd_Image_Source) || icw_Efm_Document_Image_Batch.notEquals(pnd_Image_Pointer_Key_Pnd_Image_Batch))) //Natural: IF ICW-EFM-DOCUMENT.IMAGE-SOURCE-ID NE #IMAGE-SOURCE OR ICW-EFM-DOCUMENT.IMAGE-BATCH NE #IMAGE-BATCH
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(icw_Efm_Document_Dcmnt_Rtntn.equals("D")))                                                                                                      //Natural: REJECT IF ICW-EFM-DOCUMENT.DCMNT-RTNTN = 'D'
            {
                continue;
            }
            if (condition(pnd_Prev_Min.notEquals(icw_Efm_Document_Dcmt_Min)))                                                                                             //Natural: IF #PREV-MIN NE DCMT-MIN
            {
                pnd_Exact_Image_Pointer_Key_Pnd_Exact_Image_Source.setValue(icw_Efm_Document_Image_Source_Id);                                                            //Natural: ASSIGN #EXACT-IMAGE-SOURCE := ICW-EFM-DOCUMENT.IMAGE-SOURCE-ID
                pnd_Exact_Image_Pointer_Key_Pnd_Exact_Image_Min.setValue(icw_Efm_Document_Dcmt_Min);                                                                      //Natural: ASSIGN #EXACT-IMAGE-MIN := ICW-EFM-DOCUMENT.DCMT-MIN
                                                                                                                                                                          //Natural: PERFORM ICW-CHECK-DOC-FOR-PG-CONFLICT
                sub_Icw_Check_Doc_For_Pg_Conflict();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            pnd_Prev_Min.setValue(icw_Efm_Document_Dcmt_Min);                                                                                                             //Natural: ASSIGN #PREV-MIN := DCMT-MIN
            pnd_Dcmt_Source_Id_Min_Key_Pnd_Dcmt_Source_Id.setValue(icw_Efm_Document_Image_Source_Id);                                                                     //Natural: ASSIGN #DCMT-SOURCE-ID := ICW-EFM-DOCUMENT.IMAGE-SOURCE-ID
            pnd_Dcmt_Source_Id_Min_Key_Pnd_Dcmt_Min.setValue(icw_Efm_Document_Dcmt_Min);                                                                                  //Natural: ASSIGN #DCMT-MIN := ICW-EFM-DOCUMENT.DCMT-MIN
            vw_cwf_Image_Xref.startDatabaseFind                                                                                                                           //Natural: FIND CWF-IMAGE-XREF WITH SOURCE-ID-MIN-KEY = #DCMT-SOURCE-ID-MIN-KEY
            (
            "FIND02",
            new Wc[] { new Wc("SOURCE_ID_MIN_KEY", "=", pnd_Dcmt_Source_Id_Min_Key, WcType.WITH) }
            );
            FIND02:
            while (condition(vw_cwf_Image_Xref.readNextRow("FIND02")))
            {
                vw_cwf_Image_Xref.setIfNotFoundControlFlag(false);
                if (condition(icw_Efm_Document_Dcmt_St_Page_No.lessOrEqual(cwf_Image_Xref_Number_Of_Pages) && icw_Efm_Document_Ppg_Cde.equals(cwf_Image_Xref_Cabinet_Id))) //Natural: IF ICW-EFM-DOCUMENT.DCMT-ST-PAGE-NO LE CWF-IMAGE-XREF.NUMBER-OF-PAGES AND ICW-EFM-DOCUMENT.PPG-CDE = CWF-IMAGE-XREF.CABINET-ID
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(! (pnd_End_Pg_Cnflct_Processed.getBoolean())))                                                                                              //Natural: IF NOT #END-PG-CNFLCT-PROCESSED
                {
                    if (condition(pnd_Old_End_Page.notEquals(cwf_Image_Xref_Number_Of_Pages)))                                                                            //Natural: IF #OLD-END-PAGE NE CWF-IMAGE-XREF.NUMBER-OF-PAGES
                    {
                        pnd_Tot_End_Pg_Noeql.nadd(1);                                                                                                                     //Natural: ADD 1 TO #TOT-END-PG-NOEQL
                        getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),icw_Efm_Document_2_Ppg_Cde, new FieldAttributes                   //Natural: WRITE ( AUDIT ) NOTITLE NOHDR 001T ICW-EFM-DOCUMENT-2.PPG-CDE ( AD = ODL ) 015T ICW-EFM-DOCUMENT-2.DCMT-MIN ( AD = ODL ) 029T #OLD-END-PAGE ( AD = ODL ) 034T ICW-EFM-DOCUMENT-2.CRTE-EMPL-RACF-ID ( AD = ODL ) 045T CWF-IMAGE-XREF.PH-UNIQUE-ID-NBR ( AD = ODL ) 062T CWF-IMAGE-XREF.NUMBER-OF-PAGES ( AD = ODL ) 108T 'End Page Conflict'
                            ("AD=ODL"),new TabSetting(15),icw_Efm_Document_2_Dcmt_Min, new FieldAttributes ("AD=ODL"),new TabSetting(29),pnd_Old_End_Page, 
                            new FieldAttributes ("AD=ODL"),new TabSetting(34),icw_Efm_Document_2_Crte_Empl_Racf_Id, new FieldAttributes ("AD=ODL"),new TabSetting(45),cwf_Image_Xref_Ph_Unique_Id_Nbr, 
                            new FieldAttributes ("AD=ODL"),new TabSetting(62),cwf_Image_Xref_Number_Of_Pages, new FieldAttributes ("AD=ODL"),new TabSetting(108),
                            "End Page Conflict");
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Nothing_To_Rpt.setValue(false);                                                                                                               //Natural: ASSIGN #NOTHING-TO-RPT := FALSE
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_End_Pg_Cnflct_Processed.setValue(true);                                                                                                           //Natural: ASSIGN #END-PG-CNFLCT-PROCESSED := TRUE
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ppg_Conflict.resetInitial();                                                                                                                          //Natural: RESET INITIAL #PPG-CONFLICT
                if (condition(! (icw_Efm_Document_Ppg_Cde.equals(pnd_Ppg_Code_Arr.getValue("*"))) && icw_Efm_Document_Ppg_Cde.notEquals(cwf_Image_Xref_Cabinet_Id)))      //Natural: IF NOT ( ICW-EFM-DOCUMENT.PPG-CDE = #PPG-CODE-ARR ( * ) ) AND ICW-EFM-DOCUMENT.PPG-CDE NE CWF-IMAGE-XREF.CABINET-ID
                {
                    pnd_Ppg_Conflict.setValue(true);                                                                                                                      //Natural: ASSIGN #PPG-CONFLICT := TRUE
                }                                                                                                                                                         //Natural: END-IF
                short decideConditionsMet595 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ICW-EFM-DOCUMENT.DCMT-ST-PAGE-NO GT CWF-IMAGE-XREF.NUMBER-OF-PAGES AND #PPG-CONFLICT
                if (condition(icw_Efm_Document_Dcmt_St_Page_No.greater(cwf_Image_Xref_Number_Of_Pages) && pnd_Ppg_Conflict.getBoolean()))
                {
                    decideConditionsMet595++;
                    pnd_Tot_Pge_Conflict.nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOT-PGE-CONFLICT
                    pnd_Tot_Pin_Conflict.nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOT-PIN-CONFLICT
                    pnd_Conflict_Desc.setValue("PPG and Strt Pg Conflict");                                                                                               //Natural: ASSIGN #CONFLICT-DESC := 'PPG and Strt Pg Conflict'
                }                                                                                                                                                         //Natural: WHEN ICW-EFM-DOCUMENT.DCMT-ST-PAGE-NO GT CWF-IMAGE-XREF.NUMBER-OF-PAGES
                else if (condition(icw_Efm_Document_Dcmt_St_Page_No.greater(cwf_Image_Xref_Number_Of_Pages)))
                {
                    decideConditionsMet595++;
                    pnd_Conflict_Desc.setValue("Start Pg Conflict");                                                                                                      //Natural: ASSIGN #CONFLICT-DESC := 'Start Pg Conflict'
                    pnd_Tot_Pge_Conflict.nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOT-PGE-CONFLICT
                }                                                                                                                                                         //Natural: WHEN #PPG-CONFLICT
                else if (condition(pnd_Ppg_Conflict.getBoolean()))
                {
                    decideConditionsMet595++;
                    pnd_Conflict_Desc.setValue("PPG Conflict");                                                                                                           //Natural: ASSIGN #CONFLICT-DESC := 'PPG Conflict'
                    pnd_Tot_Pin_Conflict.nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOT-PIN-CONFLICT
                }                                                                                                                                                         //Natural: WHEN ANY
                if (condition(decideConditionsMet595 > 0))
                {
                    getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Efsf006e.class));                                                                  //Natural: WRITE ( AUDIT ) NOTITLE USING FORM 'EFSF006E'
                    pnd_Nothing_To_Rpt.setValue(false);                                                                                                                   //Natural: ASSIGN #NOTHING-TO-RPT := FALSE
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Cwf_Check_Doc_For_Pg_Conflict() throws Exception                                                                                                     //Natural: CWF-CHECK-DOC-FOR-PG-CONFLICT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Page_One_Exists.resetInitial();                                                                                                                               //Natural: RESET INITIAL #PAGE-ONE-EXISTS #OLD-ST-PAGE #OLD-END-PAGE #END-PG-CNFLCT-PROCESSED
        pnd_Old_St_Page.resetInitial();
        pnd_Old_End_Page.resetInitial();
        pnd_End_Pg_Cnflct_Processed.resetInitial();
        vw_cwf_Efm_Document_2.startDatabaseFind                                                                                                                           //Natural: FIND CWF-EFM-DOCUMENT-2 WITH IMAGE-POINTER-KEY = #EXACT-IMAGE-POINTER-KEY
        (
        "FIND03",
        new Wc[] { new Wc("IMAGE_POINTER_KEY", "=", pnd_Exact_Image_Pointer_Key, WcType.WITH) }
        );
        FIND03:
        while (condition(vw_cwf_Efm_Document_2.readNextRow("FIND03")))
        {
            vw_cwf_Efm_Document_2.setIfNotFoundControlFlag(false);
            if (condition(pnd_Old_St_Page.equals(getZero()) || cwf_Efm_Document_2_Dcmt_St_Page_No.less(pnd_Old_St_Page)))                                                 //Natural: IF #OLD-ST-PAGE = 0 OR CWF-EFM-DOCUMENT-2.DCMT-ST-PAGE-NO LT #OLD-ST-PAGE
            {
                pnd_Old_St_Page.setValue(cwf_Efm_Document_2_Dcmt_St_Page_No);                                                                                             //Natural: ASSIGN #OLD-ST-PAGE := CWF-EFM-DOCUMENT-2.DCMT-ST-PAGE-NO
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Old_End_Page.equals(getZero()) || cwf_Efm_Document_2_Dgtze_Doc_End_Page.greater(pnd_Old_End_Page)))                                         //Natural: IF #OLD-END-PAGE = 0 OR CWF-EFM-DOCUMENT-2.DGTZE-DOC-END-PAGE GT #OLD-END-PAGE
            {
                pnd_Old_End_Page.setValue(cwf_Efm_Document_2_Dgtze_Doc_End_Page);                                                                                         //Natural: ASSIGN #OLD-END-PAGE := CWF-EFM-DOCUMENT-2.DGTZE-DOC-END-PAGE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cwf_Efm_Document_2_Dcmt_St_Page_No.equals(1)))                                                                                                  //Natural: IF CWF-EFM-DOCUMENT-2.DCMT-ST-PAGE-NO = 1
            {
                pnd_Page_One_Exists.setValue(true);                                                                                                                       //Natural: ASSIGN #PAGE-ONE-EXISTS := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        if (condition(! (pnd_Page_One_Exists.getBoolean())))                                                                                                              //Natural: IF NOT #PAGE-ONE-EXISTS
        {
            pnd_Tot_No_Pg_1.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #TOT-NO-PG-1
            //*  PIN-EXP
            //*  PIN-EXP
            getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),cwf_Efm_Document_2_Cabinet_Pin, new FieldAttributes ("AD=ODL"),new            //Natural: WRITE ( AUDIT ) NOTITLE NOHDR 001T CWF-EFM-DOCUMENT-2.CABINET-PIN ( AD = ODL ) 015T CWF-EFM-DOCUMENT-2.DCMT-MIN ( AD = ODL ) 028T #OLD-ST-PAGE ( AD = ODL ) 034T CWF-EFM-DOCUMENT-2.ENTRY-EMPL-RACF-ID ( AD = ODL ) 108T 'Doc Indxd without pg. 1 '
                TabSetting(15),cwf_Efm_Document_2_Dcmt_Min, new FieldAttributes ("AD=ODL"),new TabSetting(28),pnd_Old_St_Page, new FieldAttributes ("AD=ODL"),new 
                TabSetting(34),cwf_Efm_Document_2_Entry_Empl_Racf_Id, new FieldAttributes ("AD=ODL"),new TabSetting(108),"Doc Indxd without pg. 1 ");
            if (Global.isEscape()) return;
            pnd_Nothing_To_Rpt.setValue(false);                                                                                                                           //Natural: ASSIGN #NOTHING-TO-RPT := FALSE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Icw_Check_Doc_For_Pg_Conflict() throws Exception                                                                                                     //Natural: ICW-CHECK-DOC-FOR-PG-CONFLICT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Page_One_Exists.resetInitial();                                                                                                                               //Natural: RESET INITIAL #PAGE-ONE-EXISTS #OLD-ST-PAGE #OLD-END-PAGE #END-PG-CNFLCT-PROCESSED #PPG-CODE-ARR ( * ) #PPG-CTR
        pnd_Old_St_Page.resetInitial();
        pnd_Old_End_Page.resetInitial();
        pnd_End_Pg_Cnflct_Processed.resetInitial();
        pnd_Ppg_Code_Arr.getValue("*").resetInitial();
        pnd_Ppg_Ctr.resetInitial();
        vw_icw_Efm_Document_2.startDatabaseFind                                                                                                                           //Natural: FIND ICW-EFM-DOCUMENT-2 WITH IMAGE-POINTER-KEY = #EXACT-IMAGE-POINTER-KEY
        (
        "FIND04",
        new Wc[] { new Wc("IMAGE_POINTER_KEY", "=", pnd_Exact_Image_Pointer_Key, WcType.WITH) }
        );
        FIND04:
        while (condition(vw_icw_Efm_Document_2.readNextRow("FIND04")))
        {
            vw_icw_Efm_Document_2.setIfNotFoundControlFlag(false);
            if (condition(pnd_Old_St_Page.equals(getZero()) || icw_Efm_Document_2_Dcmt_St_Page_No.less(pnd_Old_St_Page)))                                                 //Natural: IF #OLD-ST-PAGE = 0 OR ICW-EFM-DOCUMENT-2.DCMT-ST-PAGE-NO LT #OLD-ST-PAGE
            {
                pnd_Old_St_Page.setValue(icw_Efm_Document_2_Dcmt_St_Page_No);                                                                                             //Natural: ASSIGN #OLD-ST-PAGE := ICW-EFM-DOCUMENT-2.DCMT-ST-PAGE-NO
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Old_End_Page.equals(getZero()) || icw_Efm_Document_2_Image_End_Page.greater(pnd_Old_End_Page)))                                             //Natural: IF #OLD-END-PAGE = 0 OR ICW-EFM-DOCUMENT-2.IMAGE-END-PAGE GT #OLD-END-PAGE
            {
                pnd_Old_End_Page.setValue(icw_Efm_Document_2_Image_End_Page);                                                                                             //Natural: ASSIGN #OLD-END-PAGE := ICW-EFM-DOCUMENT-2.IMAGE-END-PAGE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(icw_Efm_Document_2_Dcmt_St_Page_No.equals(1)))                                                                                                  //Natural: IF ICW-EFM-DOCUMENT-2.DCMT-ST-PAGE-NO = 1
            {
                pnd_Page_One_Exists.setValue(true);                                                                                                                       //Natural: ASSIGN #PAGE-ONE-EXISTS := TRUE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(! (icw_Efm_Document_2_Ppg_Cde.equals(pnd_Ppg_Code_Arr.getValue("*")))))                                                                         //Natural: IF NOT ( ICW-EFM-DOCUMENT-2.PPG-CDE = #PPG-CODE-ARR ( * ) )
            {
                pnd_Ppg_Ctr.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #PPG-CTR
                pnd_Ppg_Code_Arr.getValue(pnd_Ppg_Ctr).setValue(icw_Efm_Document_2_Ppg_Cde);                                                                              //Natural: ASSIGN #PPG-CODE-ARR ( #PPG-CTR ) := ICW-EFM-DOCUMENT-2.PPG-CDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        if (condition(! (pnd_Page_One_Exists.getBoolean())))                                                                                                              //Natural: IF NOT #PAGE-ONE-EXISTS
        {
            pnd_Tot_No_Pg_1.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #TOT-NO-PG-1
            getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),icw_Efm_Document_2_Ppg_Cde, new FieldAttributes ("AD=ODL"),new                //Natural: WRITE ( AUDIT ) NOTITLE NOHDR 001T ICW-EFM-DOCUMENT-2.PPG-CDE ( AD = ODL ) 010T ICW-EFM-DOCUMENT-2.DCMT-MIN ( AD = ODL ) 023T #OLD-ST-PAGE ( AD = ODL ) 034T ICW-EFM-DOCUMENT-2.CRTE-EMPL-RACF-ID ( AD = ODL ) 108T 'Doc Indxd without pg. 1 '
                TabSetting(10),icw_Efm_Document_2_Dcmt_Min, new FieldAttributes ("AD=ODL"),new TabSetting(23),pnd_Old_St_Page, new FieldAttributes ("AD=ODL"),new 
                TabSetting(34),icw_Efm_Document_2_Crte_Empl_Racf_Id, new FieldAttributes ("AD=ODL"),new TabSetting(108),"Doc Indxd without pg. 1 ");
            if (Global.isEscape()) return;
            pnd_Nothing_To_Rpt.setValue(false);                                                                                                                           //Natural: ASSIGN #NOTHING-TO-RPT := FALSE
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //*  BEING SUBMITTED VIA CTP8000
                    //*   SET LANDSCAPE
                    if (condition(! (DbsUtil.maskMatches(Global.getINIT_PROGRAM(),".....'CWD'"))))                                                                        //Natural: IF *INIT-PROGRAM NE MASK ( .....'CWD' )
                    {
                        getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,pnd_Record);                                                                        //Natural: WRITE ( AUDIT ) NOTITLE NOHDR #RECORD
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Efsf006b.class));                                                                  //Natural: WRITE ( AUDIT ) NOTITLE USING FORM 'EFSF006B'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean ldaEfsl0040_getCwf_Upload_Audit_Source_IdIsBreak = ldaEfsl0040.getCwf_Upload_Audit_Source_Id().isBreak(endOfData);
        if (condition(ldaEfsl0040_getCwf_Upload_Audit_Source_IdIsBreak))
        {
            pnd_Source_Id.setValue(sort01Source_IdOld);                                                                                                                   //Natural: ASSIGN #SOURCE-ID := OLD ( CWF-UPLOAD-AUDIT.SOURCE-ID )
            if (condition(pnd_Tot_Pge_Conflict.greater(getZero()) || pnd_Tot_Pin_Conflict.greater(getZero()) || pnd_Tot_No_Pg_1.greater(getZero()) ||                     //Natural: IF #TOT-PGE-CONFLICT GT 0 OR #TOT-PIN-CONFLICT GT 0 OR #TOT-NO-PG-1 GT 0 OR #TOT-END-PG-NOEQL GT 0
                pnd_Tot_End_Pg_Noeql.greater(getZero())))
            {
                getReports().skip(0, 3);                                                                                                                                  //Natural: SKIP ( AUDIT ) 3
                getReports().write(2, new ColumnSpacing(5),"Total No. of Starting Pge Conflicts:",pnd_Tot_Pge_Conflict,NEWLINE);                                          //Natural: WRITE ( AUDIT ) 5X 'Total No. of Starting Pge Conflicts:' #TOT-PGE-CONFLICT /
                if (condition(Global.isEscape())) return;
                getReports().write(2, new ColumnSpacing(5),"                  PIN/PPG Conflicts:",pnd_Tot_Pin_Conflict,NEWLINE);                                          //Natural: WRITE ( AUDIT ) 5X '                  PIN/PPG Conflicts:' #TOT-PIN-CONFLICT /
                if (condition(Global.isEscape())) return;
                getReports().write(2, new ColumnSpacing(5),"     Document Indexed without pg. 1:",pnd_Tot_No_Pg_1,NEWLINE);                                               //Natural: WRITE ( AUDIT ) 5X '     Document Indexed without pg. 1:' #TOT-NO-PG-1 /
                if (condition(Global.isEscape())) return;
                getReports().write(2, new ColumnSpacing(5),"                 End Page Conflicts:",pnd_Tot_End_Pg_Noeql,NEWLINE,NEWLINE);                                  //Natural: WRITE ( AUDIT ) 5X '                 End Page Conflicts:' #TOT-END-PG-NOEQL //
                if (condition(Global.isEscape())) return;
                pnd_Tot_Pin_Conflict.reset();                                                                                                                             //Natural: RESET #TOT-PIN-CONFLICT #TOT-PGE-CONFLICT #TOT-NO-PG-1 #TOT-END-PG-NOEQL
                pnd_Tot_Pge_Conflict.reset();
                pnd_Tot_No_Pg_1.reset();
                pnd_Tot_End_Pg_Noeql.reset();
                getReports().getPageNumberDbs(2).reset();                                                                                                                 //Natural: RESET *PAGE-NUMBER ( AUDIT )
                pnd_Source_Id.setValue(ldaEfsl0040.getCwf_Upload_Audit_Source_Id());                                                                                      //Natural: ASSIGN #SOURCE-ID := CWF-UPLOAD-AUDIT.SOURCE-ID
                getReports().newPage(new ReportSpecification(0));                                                                                                         //Natural: NEWPAGE ( AUDIT )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(2, "LS=133 PS=60 ZP=OFF");
    }
}
