/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:27:09 PM
**        * FROM NATURAL PROGRAM : Cwfb3020
************************************************************
**        * FILE NAME            : Cwfb3020.java
**        * CLASS NAME           : Cwfb3020
**        * INSTANCE NAME        : Cwfb3020
************************************************************
**********************************************************************
* PROGRAM  : LEMSTR12/CWFB3020
* SYSTEM   : CRPCWF
* TITLE    : COMPARISON REPORT MIT VS. WHO
* FUNCTION : THIS PROGRAM COMPARES MIT RECORDS LOGGED WITHIN PERIOD
*          : WITH WHO RECORDS. IF NOT ARCHIVED - PROGRAM WILL ARCHIVE
*          : IF RECORDS HAVE DISCREPANCIES WHO RECORD WILL BE UPDATED.
* 02/23/2017 - SINGAK - PIN EXPANSION - AUG 2017
**********************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3020 extends BLNatBase
{
    // Data Areas
    private PdaMiha0130 pdaMiha0130;
    private PdaMiha0180 pdaMiha0180;
    private PdaMiha0200 pdaMiha0200;
    private PdaMiha0030 pdaMiha0030;
    private PdaMiha0100 pdaMiha0100;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private LdaCwfl3019 ldaCwfl3019;
    private LdaMihl0210 ldaMihl0210;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Master_Histo_1;
    private DbsField cwf_Master_Histo_1_Actv_Unque_Key;

    private DbsGroup cwf_Master_Histo_1__R_Field_1;
    private DbsField cwf_Master_Histo_1_Pnd_Rqst_Log_Dte_Tme;

    private DbsGroup cwf_Master_Histo_1__R_Field_2;
    private DbsField cwf_Master_Histo_1_Pnd_Rqst_Log_Dte;
    private DbsField cwf_Master_Histo_1_Pnd_Rqst_Log_Tme;
    private DbsField cwf_Master_Histo_1_Pnd_Act_Ind;
    private DbsField pnd_Begin_Key;

    private DbsGroup pnd_Begin_Key__R_Field_3;
    private DbsField pnd_Begin_Key_Pnd_B_Crprte_Status_Ind;
    private DbsField pnd_Begin_Key_Pnd_B_Last_Chnge_Invrt_Dte_Tme;

    private DbsGroup pnd_Begin_Key__R_Field_4;
    private DbsField pnd_Begin_Key_Pnd_B_Last_Chnge_Invrt_Dte;
    private DbsField pnd_Begin_Key_Pnd_B_Last_Chnge_Invrt_Tme;
    private DbsField pnd_Begin_Key_Pnd_B_Admin_Unit_Cde;
    private DbsField pnd_Begin_Key_Pnd_B_Actve_Ind;
    private DbsField pnd_End_Key;

    private DbsGroup pnd_End_Key__R_Field_5;
    private DbsField pnd_End_Key_Pnd_E_Crprte_Status_Ind;
    private DbsField pnd_End_Key_Pnd_E_Last_Chnge_Invrt_Dte_Tme;

    private DbsGroup pnd_End_Key__R_Field_6;
    private DbsField pnd_End_Key_Pnd_E_Last_Chnge_Invrt_Dte;
    private DbsField pnd_End_Key_Pnd_E_Last_Chnge_Invrt_Tme;
    private DbsField pnd_End_Key_Pnd_E_Admin_Unit_Cde;
    private DbsField pnd_End_Key_Pnd_E_Actve_Ind;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_7;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind_2_2;
    private DbsField pnd_Tbl_Split;

    private DbsGroup pnd_Tbl_Split__R_Field_8;
    private DbsField pnd_Tbl_Split_Pnd_Tbl_Start_Dt;
    private DbsField pnd_Tbl_Split_Pnd_Tbl_Stop_Dt;
    private DbsField pnd_Conversion_Alpha;

    private DbsGroup pnd_Conversion_Alpha__R_Field_9;
    private DbsField pnd_Conversion_Alpha_Pnd_Conversion_Numeric;
    private DbsField pnd_Start_Dte_Tme;
    private DbsField pnd_End_Dte_Tme;

    private DbsGroup pnd_End_Dte_Tme__R_Field_10;
    private DbsField pnd_End_Dte_Tme_Pnd_End_Yyyymmdd;
    private DbsField pnd_End_Dte_Tme_Pnd_End_Hhiisst;

    private DbsGroup pnd_End_Dte_Tme__R_Field_11;
    private DbsField pnd_End_Dte_Tme_Pnd_End_Dte_Tme_N;
    private DbsField pnd_Curr_Dte_Tme;
    private DbsField pnd_Prev_Dte_Tme;

    private DbsGroup pnd_Prev_Dte_Tme__R_Field_12;
    private DbsField pnd_Prev_Dte_Tme_Pnd_Prev_Yyyymmdd;
    private DbsField pnd_Prev_Dte_Tme_Pnd_Prev_Hhiisst;
    private DbsField pnd_Date_T;
    private DbsField pnd_Date_D;
    private DbsField pnd_Display_Arch_Period;
    private DbsField pnd_Display_Strt_Period;
    private DbsField pnd_Display_Stop_Period;
    private DbsField pnd_Base_Dte_Tme;
    private DbsField pnd_End_Dte_Tme_D;
    private DbsField pnd_Bus_Days;

    private DataAccessProgramView vw_cwf_Who_Work_Request;
    private DbsField cwf_Who_Work_Request_Rqst_Log_Dte_Tme;
    private DbsField cwf_Who_Work_Request_Rqst_Log_Invrt_Dte_Tme;
    private DbsField cwf_Who_Work_Request_Pin_Nbr;
    private DbsField cwf_Who_Work_Request_Rqst_Id;
    private DbsField cwf_Who_Work_Request_Tiaa_Rcvd_Dte;
    private DbsField cwf_Who_Work_Request_Work_Prcss_Id;
    private DbsField cwf_Who_Work_Request_Rlte_Rqst_Id;
    private DbsField cwf_Who_Work_Request_Crprte_Clock_End_Dte_Tme;
    private DbsField cwf_Who_Work_Request_Final_Close_Out_Dte_Tme;
    private DbsField cwf_Who_Work_Request_Effctve_Dte;
    private DbsField cwf_Who_Work_Request_Status_Cde;
    private DbsField cwf_Who_Work_Request_Status_Freeze_Ind;
    private DbsField cwf_Who_Work_Request_Crprte_On_Tme_Ind;
    private DbsField cwf_Who_Work_Request_Trans_Dte;
    private DbsField cwf_Who_Work_Request_Crprte_Status_Ind;
    private DbsField cwf_Who_Work_Request_Sec_Turnaround_Tme;
    private DbsField cwf_Who_Work_Request_Sec_Updte_Dte;
    private DbsField cwf_Who_Work_Request_Rqst_Indicators;

    private DataAccessProgramView vw_cwf_Who_Activity_File;
    private DbsField cwf_Who_Activity_File_Rqst_Log_Dte_Tme;
    private DbsField cwf_Who_Activity_File_Strtng_Event_Dte_Tme;

    private DataAccessProgramView vw_cwf_Master;
    private DbsField cwf_Master_Rqst_Log_Dte_Tme;

    private DbsGroup cwf_Master__R_Field_13;
    private DbsField cwf_Master_Rqst_Log_Index_Dte;
    private DbsField cwf_Master_Rqst_Log_Index_Tmte;
    private DbsField cwf_Master_Pin_Nbr;
    private DbsField cwf_Master_Rqst_Orgn_Cde;
    private DbsField cwf_Master_Work_Prcss_Id;
    private DbsField cwf_Master_Unit_Cde;
    private DbsField cwf_Master_Unit_Updte_Dte_Tme;
    private DbsField cwf_Master_Empl_Oprtr_Cde;
    private DbsField cwf_Master_Last_Chnge_Dte_Tme;

    private DbsGroup cwf_Master__R_Field_14;
    private DbsField cwf_Master_Pnd_Date_C;

    private DbsGroup cwf_Master__R_Field_15;
    private DbsField cwf_Master_Pnd_Date_Ca;
    private DbsField cwf_Master_Last_Chnge_Oprtr_Cde;
    private DbsField cwf_Master_Last_Chnge_Invrt_Dte_Tme;
    private DbsField cwf_Master_Last_Chnge_Unit_Cde;
    private DbsField cwf_Master_Step_Id;
    private DbsField cwf_Master_Admin_Unit_Cde;

    private DbsGroup cwf_Master__R_Field_16;
    private DbsField cwf_Master_Unit_Id_Cde;
    private DbsField cwf_Master_Admin_Status_Cde;
    private DbsField cwf_Master_Status_Cde;
    private DbsField cwf_Master_Status_Updte_Dte_Tme;
    private DbsField cwf_Master_Last_Updte_Dte;
    private DbsField cwf_Master_Last_Updte_Dte_Tme;
    private DbsField cwf_Master_Last_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Actve_Ind;

    private DbsGroup cwf_Master__R_Field_17;
    private DbsField cwf_Master_Actve_Ind_1;
    private DbsField cwf_Master_Actve_Ind_2;
    private DbsField cwf_Master_Crprte_Status_Ind;
    private DbsField cwf_Master_Tiaa_Rcvd_Dte;
    private DbsField cwf_Master_Effctve_Dte;
    private DbsField cwf_Master_Trnsctn_Dte;
    private DbsField cwf_Master_Trans_Dte;
    private DbsField cwf_Master_Physcl_Fldr_Id_Nbr;
    private DbsField cwf_Master_Mj_Chrge_Dte_Tme;
    private DbsField cwf_Master_Mj_Pull_Ind;
    private DbsField cwf_Master_Status_Freeze_Ind;
    private DbsField cwf_Master_Work_List_Ind;
    private DbsField cwf_Master_Unit_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Unit_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Empl_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Empl_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Step_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Step_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Crprte_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Final_Close_Out_Dte_Tme;
    private DbsField cwf_Master_Rqst_Id;

    private DbsGroup cwf_Master__R_Field_18;
    private DbsField cwf_Master_Rqst_Work_Prcss_Id;
    private DbsField cwf_Master_Rqst_Tiaa_Dte_Tme;
    private DbsField cwf_Master_Rlte_Rqst_Id;
    private DbsField pnd_Mit_Counter_O;
    private DbsField pnd_Mit_Counter_C;
    private DbsField pnd_Nfd_Counter;
    private DbsField pnd_Now_Counter;
    private DbsField pnd_Act_Counter;
    private DbsField pnd_Cor_Counter;
    private DbsField pnd_Inter_Cnt;
    private DbsField pnd_Isn;
    private DbsField pnd_Found_Sw;
    private DbsField pnd_Disc_Msg;

    private DbsGroup pnd_Disc_Msg__R_Field_19;
    private DbsField pnd_Disc_Msg_Pnd_Disc_Msg_1;
    private DbsField pnd_Disc_Msg_Pnd_Disc_Msg_2;

    private DbsGroup pnd_Disc_Msg__R_Field_20;
    private DbsField pnd_Disc_Msg_Pnd_Disc_Msg_2_1;
    private DbsField pnd_Disc_Msg_Pnd_Disc_Msg_2_2;
    private DbsField pnd_Stop_Sweep_Time_A;

    private DbsGroup pnd_Stop_Sweep_Time_A__R_Field_21;
    private DbsField pnd_Stop_Sweep_Time_A_Pnd_Stop_Yyyymmdd;
    private DbsField pnd_Stop_Sweep_Time_A_Pnd_Stop_Hhiisst;

    private DbsGroup pnd_Stop_Sweep_Time_A__R_Field_22;
    private DbsField pnd_Stop_Sweep_Time_A_Pnd_Stop_Sweep_Time_N;
    private DbsField pnd_Stop_Sweep_Invrt_Time;
    private DbsField pnd_Time;
    private DbsField pnd_Rqst_Log_Dte_Tme_Save;
    private DbsField pnd_Pin_Nbr_Save;
    private DbsField pnd_Wpid_Save;
    private DbsField pnd_Tiaa_Rcvd_Dte_Save;
    private DbsField pnd_Rqst_Id_Save;
    private DbsField pnd_Rlte_Rqst_Id_Save;
    private DbsField pnd_Last_Chnge_Dte_Tme_Save;
    private DbsField pnd_Crprte_Stat_Ind_Save;
    private DbsField pnd_Final_Close_Out_Save;
    private DbsField pnd_Crprte_Clock_Dte_Save;
    private DbsField pnd_Mit_Status_Cde_Save;
    private DbsField pnd_Discrep_Sw;
    private DbsField pnd_Sec_Discrep;
    private DbsField pnd_Wpid_Chnge_Op;
    private DbsField pnd_Pin_Chnge_Op;
    private DbsField pnd_Tiaa_Chnge_Op;
    private DbsField pnd_Rqid_Chnge_Op;
    private DbsField pnd_Rlte_Chnge_Op;
    private DbsField pnd_Stat_Chnge_Op;
    private DbsField pnd_Fncl_Chnge_Op;
    private DbsField pnd_Clck_Chnge_Op;
    private DbsField pnd_Sec_Tatm_Chnge_Op;
    private DbsField pnd_Sec_Effd_Chnge_Op;
    private DbsField pnd_Sec_Trns_Chnge_Op;
    private DbsField pnd_Late_Chnge_Op;
    private DbsField pnd_Set_To_Zero_Op;
    private DbsField pnd_Set_To_Nine_Op;
    private DbsField pnd_Wpid_Chnge_Cl;
    private DbsField pnd_Pin_Chnge_Cl;
    private DbsField pnd_Tiaa_Chnge_Cl;
    private DbsField pnd_Rqid_Chnge_Cl;
    private DbsField pnd_Rlte_Chnge_Cl;
    private DbsField pnd_Stat_Chnge_Cl;
    private DbsField pnd_Fncl_Chnge_Cl;
    private DbsField pnd_Clck_Chnge_Cl;
    private DbsField pnd_Sec_Tatm_Chnge_Cl;
    private DbsField pnd_Sec_Effd_Chnge_Cl;
    private DbsField pnd_Sec_Trns_Chnge_Cl;
    private DbsField pnd_Late_Chnge_Cl;
    private DbsField pnd_Cor_Stat_Cntr;
    private DbsField pnd_Pin_Rqst_Key;

    private DbsGroup pnd_Pin_Rqst_Key__R_Field_23;
    private DbsField pnd_Pin_Rqst_Key_Pnd_Pin_Nbr;
    private DbsField pnd_Pin_Rqst_Key_Pnd_Rqst_Id;
    private DbsField pnd_Pin_Rqst_Key_Pnd_Rqst_Log_Dte_Tme;
    private DbsField pnd_Log_Dte_Tme_St_Event_Key;

    private DbsGroup pnd_Log_Dte_Tme_St_Event_Key__R_Field_24;
    private DbsField pnd_Log_Dte_Tme_St_Event_Key_Pnd_Rqst_Log_Dte_Tme;
    private DbsField pnd_Log_Dte_Tme_St_Event_Key_Pnd_Strtng_Event_Dte_Tme;
    private DbsField pnd_Crprte_Clock_End_Dte_Tme_T;
    private DbsField pnd_Late_Ind;
    private DbsField pnd_Who_Ind;
    private DbsField pnd_Check_Date;
    private DbsField pnd_This_Program_Has_Finished_Successfully;

    private DbsRecord setTimeRecord;
    private DbsField st;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaMiha0130 = new PdaMiha0130(localVariables);
        pdaMiha0180 = new PdaMiha0180(localVariables);
        pdaMiha0200 = new PdaMiha0200(localVariables);
        pdaMiha0030 = new PdaMiha0030(localVariables);
        pdaMiha0100 = new PdaMiha0100(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        ldaCwfl3019 = new LdaCwfl3019();
        registerRecord(ldaCwfl3019);
        registerRecord(ldaCwfl3019.getVw_cwf_Who_Work_Request_View());
        ldaMihl0210 = new LdaMihl0210();
        registerRecord(ldaMihl0210);
        registerRecord(ldaMihl0210.getVw_cwf_Who_Support_Tbl());

        // Local Variables

        vw_cwf_Master_Histo_1 = new DataAccessProgramView(new NameInfo("vw_cwf_Master_Histo_1", "CWF-MASTER-HISTO-1"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        cwf_Master_Histo_1_Actv_Unque_Key = vw_cwf_Master_Histo_1.getRecord().newFieldInGroup("cwf_Master_Histo_1_Actv_Unque_Key", "ACTV-UNQUE-KEY", FieldType.STRING, 
            16, RepeatingFieldStrategy.None, "ACTV_UNQUE_KEY");
        cwf_Master_Histo_1_Actv_Unque_Key.setDdmHeader("UNIQUE/KEY");
        cwf_Master_Histo_1_Actv_Unque_Key.setSuperDescriptor(true);

        cwf_Master_Histo_1__R_Field_1 = vw_cwf_Master_Histo_1.getRecord().newGroupInGroup("cwf_Master_Histo_1__R_Field_1", "REDEFINE", cwf_Master_Histo_1_Actv_Unque_Key);
        cwf_Master_Histo_1_Pnd_Rqst_Log_Dte_Tme = cwf_Master_Histo_1__R_Field_1.newFieldInGroup("cwf_Master_Histo_1_Pnd_Rqst_Log_Dte_Tme", "#RQST-LOG-DTE-TME", 
            FieldType.STRING, 15);

        cwf_Master_Histo_1__R_Field_2 = cwf_Master_Histo_1__R_Field_1.newGroupInGroup("cwf_Master_Histo_1__R_Field_2", "REDEFINE", cwf_Master_Histo_1_Pnd_Rqst_Log_Dte_Tme);
        cwf_Master_Histo_1_Pnd_Rqst_Log_Dte = cwf_Master_Histo_1__R_Field_2.newFieldInGroup("cwf_Master_Histo_1_Pnd_Rqst_Log_Dte", "#RQST-LOG-DTE", FieldType.STRING, 
            8);
        cwf_Master_Histo_1_Pnd_Rqst_Log_Tme = cwf_Master_Histo_1__R_Field_2.newFieldInGroup("cwf_Master_Histo_1_Pnd_Rqst_Log_Tme", "#RQST-LOG-TME", FieldType.STRING, 
            7);
        cwf_Master_Histo_1_Pnd_Act_Ind = cwf_Master_Histo_1__R_Field_1.newFieldInGroup("cwf_Master_Histo_1_Pnd_Act_Ind", "#ACT-IND", FieldType.STRING, 
            1);
        registerRecord(vw_cwf_Master_Histo_1);

        pnd_Begin_Key = localVariables.newFieldInRecord("pnd_Begin_Key", "#BEGIN-KEY", FieldType.STRING, 25);

        pnd_Begin_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Begin_Key__R_Field_3", "REDEFINE", pnd_Begin_Key);
        pnd_Begin_Key_Pnd_B_Crprte_Status_Ind = pnd_Begin_Key__R_Field_3.newFieldInGroup("pnd_Begin_Key_Pnd_B_Crprte_Status_Ind", "#B-CRPRTE-STATUS-IND", 
            FieldType.STRING, 1);
        pnd_Begin_Key_Pnd_B_Last_Chnge_Invrt_Dte_Tme = pnd_Begin_Key__R_Field_3.newFieldInGroup("pnd_Begin_Key_Pnd_B_Last_Chnge_Invrt_Dte_Tme", "#B-LAST-CHNGE-INVRT-DTE-TME", 
            FieldType.NUMERIC, 15);

        pnd_Begin_Key__R_Field_4 = pnd_Begin_Key__R_Field_3.newGroupInGroup("pnd_Begin_Key__R_Field_4", "REDEFINE", pnd_Begin_Key_Pnd_B_Last_Chnge_Invrt_Dte_Tme);
        pnd_Begin_Key_Pnd_B_Last_Chnge_Invrt_Dte = pnd_Begin_Key__R_Field_4.newFieldInGroup("pnd_Begin_Key_Pnd_B_Last_Chnge_Invrt_Dte", "#B-LAST-CHNGE-INVRT-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Begin_Key_Pnd_B_Last_Chnge_Invrt_Tme = pnd_Begin_Key__R_Field_4.newFieldInGroup("pnd_Begin_Key_Pnd_B_Last_Chnge_Invrt_Tme", "#B-LAST-CHNGE-INVRT-TME", 
            FieldType.NUMERIC, 7);
        pnd_Begin_Key_Pnd_B_Admin_Unit_Cde = pnd_Begin_Key__R_Field_3.newFieldInGroup("pnd_Begin_Key_Pnd_B_Admin_Unit_Cde", "#B-ADMIN-UNIT-CDE", FieldType.STRING, 
            8);
        pnd_Begin_Key_Pnd_B_Actve_Ind = pnd_Begin_Key__R_Field_3.newFieldInGroup("pnd_Begin_Key_Pnd_B_Actve_Ind", "#B-ACTVE-IND", FieldType.STRING, 1);
        pnd_End_Key = localVariables.newFieldInRecord("pnd_End_Key", "#END-KEY", FieldType.STRING, 25);

        pnd_End_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_End_Key__R_Field_5", "REDEFINE", pnd_End_Key);
        pnd_End_Key_Pnd_E_Crprte_Status_Ind = pnd_End_Key__R_Field_5.newFieldInGroup("pnd_End_Key_Pnd_E_Crprte_Status_Ind", "#E-CRPRTE-STATUS-IND", FieldType.STRING, 
            1);
        pnd_End_Key_Pnd_E_Last_Chnge_Invrt_Dte_Tme = pnd_End_Key__R_Field_5.newFieldInGroup("pnd_End_Key_Pnd_E_Last_Chnge_Invrt_Dte_Tme", "#E-LAST-CHNGE-INVRT-DTE-TME", 
            FieldType.NUMERIC, 15);

        pnd_End_Key__R_Field_6 = pnd_End_Key__R_Field_5.newGroupInGroup("pnd_End_Key__R_Field_6", "REDEFINE", pnd_End_Key_Pnd_E_Last_Chnge_Invrt_Dte_Tme);
        pnd_End_Key_Pnd_E_Last_Chnge_Invrt_Dte = pnd_End_Key__R_Field_6.newFieldInGroup("pnd_End_Key_Pnd_E_Last_Chnge_Invrt_Dte", "#E-LAST-CHNGE-INVRT-DTE", 
            FieldType.NUMERIC, 8);
        pnd_End_Key_Pnd_E_Last_Chnge_Invrt_Tme = pnd_End_Key__R_Field_6.newFieldInGroup("pnd_End_Key_Pnd_E_Last_Chnge_Invrt_Tme", "#E-LAST-CHNGE-INVRT-TME", 
            FieldType.NUMERIC, 7);
        pnd_End_Key_Pnd_E_Admin_Unit_Cde = pnd_End_Key__R_Field_5.newFieldInGroup("pnd_End_Key_Pnd_E_Admin_Unit_Cde", "#E-ADMIN-UNIT-CDE", FieldType.STRING, 
            8);
        pnd_End_Key_Pnd_E_Actve_Ind = pnd_End_Key__R_Field_5.newFieldInGroup("pnd_End_Key_Pnd_E_Actve_Ind", "#E-ACTVE-IND", FieldType.STRING, 1);
        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_7 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_7", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_7.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_7.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_7.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind_2_2 = pnd_Tbl_Prime_Key__R_Field_7.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind_2_2", "#TBL-ACTVE-IND-2-2", 
            FieldType.STRING, 1);
        pnd_Tbl_Split = localVariables.newFieldInRecord("pnd_Tbl_Split", "#TBL-SPLIT", FieldType.STRING, 30);

        pnd_Tbl_Split__R_Field_8 = localVariables.newGroupInRecord("pnd_Tbl_Split__R_Field_8", "REDEFINE", pnd_Tbl_Split);
        pnd_Tbl_Split_Pnd_Tbl_Start_Dt = pnd_Tbl_Split__R_Field_8.newFieldInGroup("pnd_Tbl_Split_Pnd_Tbl_Start_Dt", "#TBL-START-DT", FieldType.TIME);
        pnd_Tbl_Split_Pnd_Tbl_Stop_Dt = pnd_Tbl_Split__R_Field_8.newFieldInGroup("pnd_Tbl_Split_Pnd_Tbl_Stop_Dt", "#TBL-STOP-DT", FieldType.TIME);
        pnd_Conversion_Alpha = localVariables.newFieldInRecord("pnd_Conversion_Alpha", "#CONVERSION-ALPHA", FieldType.STRING, 15);

        pnd_Conversion_Alpha__R_Field_9 = localVariables.newGroupInRecord("pnd_Conversion_Alpha__R_Field_9", "REDEFINE", pnd_Conversion_Alpha);
        pnd_Conversion_Alpha_Pnd_Conversion_Numeric = pnd_Conversion_Alpha__R_Field_9.newFieldInGroup("pnd_Conversion_Alpha_Pnd_Conversion_Numeric", "#CONVERSION-NUMERIC", 
            FieldType.NUMERIC, 15);
        pnd_Start_Dte_Tme = localVariables.newFieldInRecord("pnd_Start_Dte_Tme", "#START-DTE-TME", FieldType.STRING, 15);
        pnd_End_Dte_Tme = localVariables.newFieldInRecord("pnd_End_Dte_Tme", "#END-DTE-TME", FieldType.STRING, 15);

        pnd_End_Dte_Tme__R_Field_10 = localVariables.newGroupInRecord("pnd_End_Dte_Tme__R_Field_10", "REDEFINE", pnd_End_Dte_Tme);
        pnd_End_Dte_Tme_Pnd_End_Yyyymmdd = pnd_End_Dte_Tme__R_Field_10.newFieldInGroup("pnd_End_Dte_Tme_Pnd_End_Yyyymmdd", "#END-YYYYMMDD", FieldType.STRING, 
            8);
        pnd_End_Dte_Tme_Pnd_End_Hhiisst = pnd_End_Dte_Tme__R_Field_10.newFieldInGroup("pnd_End_Dte_Tme_Pnd_End_Hhiisst", "#END-HHIISST", FieldType.STRING, 
            7);

        pnd_End_Dte_Tme__R_Field_11 = localVariables.newGroupInRecord("pnd_End_Dte_Tme__R_Field_11", "REDEFINE", pnd_End_Dte_Tme);
        pnd_End_Dte_Tme_Pnd_End_Dte_Tme_N = pnd_End_Dte_Tme__R_Field_11.newFieldInGroup("pnd_End_Dte_Tme_Pnd_End_Dte_Tme_N", "#END-DTE-TME-N", FieldType.NUMERIC, 
            15);
        pnd_Curr_Dte_Tme = localVariables.newFieldInRecord("pnd_Curr_Dte_Tme", "#CURR-DTE-TME", FieldType.STRING, 15);
        pnd_Prev_Dte_Tme = localVariables.newFieldInRecord("pnd_Prev_Dte_Tme", "#PREV-DTE-TME", FieldType.STRING, 15);

        pnd_Prev_Dte_Tme__R_Field_12 = localVariables.newGroupInRecord("pnd_Prev_Dte_Tme__R_Field_12", "REDEFINE", pnd_Prev_Dte_Tme);
        pnd_Prev_Dte_Tme_Pnd_Prev_Yyyymmdd = pnd_Prev_Dte_Tme__R_Field_12.newFieldInGroup("pnd_Prev_Dte_Tme_Pnd_Prev_Yyyymmdd", "#PREV-YYYYMMDD", FieldType.STRING, 
            8);
        pnd_Prev_Dte_Tme_Pnd_Prev_Hhiisst = pnd_Prev_Dte_Tme__R_Field_12.newFieldInGroup("pnd_Prev_Dte_Tme_Pnd_Prev_Hhiisst", "#PREV-HHIISST", FieldType.STRING, 
            7);
        pnd_Date_T = localVariables.newFieldInRecord("pnd_Date_T", "#DATE-T", FieldType.TIME);
        pnd_Date_D = localVariables.newFieldInRecord("pnd_Date_D", "#DATE-D", FieldType.DATE);
        pnd_Display_Arch_Period = localVariables.newFieldInRecord("pnd_Display_Arch_Period", "#DISPLAY-ARCH-PERIOD", FieldType.NUMERIC, 15);
        pnd_Display_Strt_Period = localVariables.newFieldInRecord("pnd_Display_Strt_Period", "#DISPLAY-STRT-PERIOD", FieldType.NUMERIC, 15);
        pnd_Display_Stop_Period = localVariables.newFieldInRecord("pnd_Display_Stop_Period", "#DISPLAY-STOP-PERIOD", FieldType.NUMERIC, 15);
        pnd_Base_Dte_Tme = localVariables.newFieldInRecord("pnd_Base_Dte_Tme", "#BASE-DTE-TME", FieldType.DATE);
        pnd_End_Dte_Tme_D = localVariables.newFieldInRecord("pnd_End_Dte_Tme_D", "#END-DTE-TME-D", FieldType.DATE);
        pnd_Bus_Days = localVariables.newFieldInRecord("pnd_Bus_Days", "#BUS-DAYS", FieldType.NUMERIC, 7);

        vw_cwf_Who_Work_Request = new DataAccessProgramView(new NameInfo("vw_cwf_Who_Work_Request", "CWF-WHO-WORK-REQUEST"), "CWF_WHO_WORK_REQUEST", "CWF_WORK_REQUEST");
        cwf_Who_Work_Request_Rqst_Log_Dte_Tme = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Who_Work_Request_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        cwf_Who_Work_Request_Rqst_Log_Invrt_Dte_Tme = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Rqst_Log_Invrt_Dte_Tme", 
            "RQST-LOG-INVRT-DTE-TME", FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "RQST_LOG_INVRT_DTE_TME");
        cwf_Who_Work_Request_Pin_Nbr = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "PIN_NBR");
        cwf_Who_Work_Request_Pin_Nbr.setDdmHeader("PIN");
        cwf_Who_Work_Request_Rqst_Id = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Rqst_Id", "RQST-ID", FieldType.STRING, 
            28, RepeatingFieldStrategy.None, "RQST_ID");
        cwf_Who_Work_Request_Rqst_Id.setDdmHeader("REQUEST ID");
        cwf_Who_Work_Request_Tiaa_Rcvd_Dte = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_RCVD_DTE");
        cwf_Who_Work_Request_Work_Prcss_Id = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Work_Prcss_Id", "WORK-PRCSS-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        cwf_Who_Work_Request_Work_Prcss_Id.setDdmHeader("WORK/ID");
        cwf_Who_Work_Request_Rlte_Rqst_Id = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Rlte_Rqst_Id", "RLTE-RQST-ID", FieldType.STRING, 
            28, RepeatingFieldStrategy.None, "RLTE_RQST_ID");
        cwf_Who_Work_Request_Crprte_Clock_End_Dte_Tme = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Crprte_Clock_End_Dte_Tme", 
            "CRPRTE-CLOCK-END-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "CRPRTE_CLOCK_END_DTE_TME");
        cwf_Who_Work_Request_Crprte_Clock_End_Dte_Tme.setDdmHeader("CORPORATE CLOCK END DATE TIME");
        cwf_Who_Work_Request_Final_Close_Out_Dte_Tme = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Final_Close_Out_Dte_Tme", 
            "FINAL-CLOSE-OUT-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "FINAL_CLOSE_OUT_DTE_TME");
        cwf_Who_Work_Request_Final_Close_Out_Dte_Tme.setDdmHeader("FINAL/CLOSEOUT/DATE-TIME");
        cwf_Who_Work_Request_Effctve_Dte = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Effctve_Dte", "EFFCTVE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "EFFCTVE_DTE");
        cwf_Who_Work_Request_Status_Cde = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Status_Cde", "STATUS-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "STATUS_CDE");
        cwf_Who_Work_Request_Status_Cde.setDdmHeader("STATUS/CODE");
        cwf_Who_Work_Request_Status_Freeze_Ind = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Status_Freeze_Ind", "STATUS-FREEZE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "STATUS_FREEZE_IND");
        cwf_Who_Work_Request_Status_Freeze_Ind.setDdmHeader("FREEZE/STATUS");
        cwf_Who_Work_Request_Crprte_On_Tme_Ind = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Crprte_On_Tme_Ind", "CRPRTE-ON-TME-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CRPRTE_ON_TME_IND");
        cwf_Who_Work_Request_Crprte_On_Tme_Ind.setDdmHeader("WORKRQST/DUE DATE");
        cwf_Who_Work_Request_Trans_Dte = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Trans_Dte", "TRANS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TRANS_DTE");
        cwf_Who_Work_Request_Trans_Dte.setDdmHeader("CHECK MAIL/DATE");
        cwf_Who_Work_Request_Crprte_Status_Ind = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Crprte_Status_Ind", "CRPRTE-STATUS-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CRPRTE_STATUS_IND");
        cwf_Who_Work_Request_Crprte_Status_Ind.setDdmHeader("CORP/STAT");
        cwf_Who_Work_Request_Sec_Turnaround_Tme = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Sec_Turnaround_Tme", "SEC-TURNAROUND-TME", 
            FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "SEC_TURNAROUND_TME");
        cwf_Who_Work_Request_Sec_Turnaround_Tme.setDdmHeader("SEC/TURNAROUND");
        cwf_Who_Work_Request_Sec_Updte_Dte = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Sec_Updte_Dte", "SEC-UPDTE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "SEC_UPDTE_DTE");
        cwf_Who_Work_Request_Sec_Updte_Dte.setDdmHeader("SEC/UPDATE");
        cwf_Who_Work_Request_Rqst_Indicators = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Rqst_Indicators", "RQST-INDICATORS", 
            FieldType.STRING, 25, RepeatingFieldStrategy.None, "RQST_INDICATORS");
        cwf_Who_Work_Request_Rqst_Indicators.setDdmHeader("INDICATORS");
        registerRecord(vw_cwf_Who_Work_Request);

        vw_cwf_Who_Activity_File = new DataAccessProgramView(new NameInfo("vw_cwf_Who_Activity_File", "CWF-WHO-ACTIVITY-FILE"), "CWF_WHO_ACTIVITY_FILE", 
            "CWF_WHO_ACTIVITY");
        cwf_Who_Activity_File_Rqst_Log_Dte_Tme = vw_cwf_Who_Activity_File.getRecord().newFieldInGroup("cwf_Who_Activity_File_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Who_Activity_File_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        cwf_Who_Activity_File_Strtng_Event_Dte_Tme = vw_cwf_Who_Activity_File.getRecord().newFieldInGroup("cwf_Who_Activity_File_Strtng_Event_Dte_Tme", 
            "STRTNG-EVENT-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "STRTNG_EVENT_DTE_TME");
        cwf_Who_Activity_File_Strtng_Event_Dte_Tme.setDdmHeader("STARTING/EVENT");
        registerRecord(vw_cwf_Who_Activity_File);

        vw_cwf_Master = new DataAccessProgramView(new NameInfo("vw_cwf_Master", "CWF-MASTER"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        cwf_Master_Rqst_Log_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15, 
            RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Master_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");

        cwf_Master__R_Field_13 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master__R_Field_13", "REDEFINE", cwf_Master_Rqst_Log_Dte_Tme);
        cwf_Master_Rqst_Log_Index_Dte = cwf_Master__R_Field_13.newFieldInGroup("cwf_Master_Rqst_Log_Index_Dte", "RQST-LOG-INDEX-DTE", FieldType.STRING, 
            8);
        cwf_Master_Rqst_Log_Index_Tmte = cwf_Master__R_Field_13.newFieldInGroup("cwf_Master_Rqst_Log_Index_Tmte", "RQST-LOG-INDEX-TMTE", FieldType.STRING, 
            7);
        cwf_Master_Pin_Nbr = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "PIN_NBR");
        cwf_Master_Pin_Nbr.setDdmHeader("PIN");
        cwf_Master_Rqst_Orgn_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Rqst_Orgn_Cde", "RQST-ORGN-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RQST_ORGN_CDE");
        cwf_Master_Work_Prcss_Id = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        cwf_Master_Work_Prcss_Id.setDdmHeader("WORK/ID");
        cwf_Master_Unit_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "UNIT_CDE");
        cwf_Master_Unit_Cde.setDdmHeader("UNIT/CODE");
        cwf_Master_Unit_Updte_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Unit_Updte_Dte_Tme", "UNIT-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "UNIT_UPDTE_DTE_TME");
        cwf_Master_Unit_Updte_Dte_Tme.setDdmHeader("UNIT UPDATE/DATE-TIME");
        cwf_Master_Empl_Oprtr_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "EMPL_OPRTR_CDE");
        cwf_Master_Empl_Oprtr_Cde.setDdmHeader("EMPLOYEE/ID");
        cwf_Master_Last_Chnge_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 
            15, RepeatingFieldStrategy.None, "LAST_CHNGE_DTE_TME");
        cwf_Master_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");

        cwf_Master__R_Field_14 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master__R_Field_14", "REDEFINE", cwf_Master_Last_Chnge_Dte_Tme);
        cwf_Master_Pnd_Date_C = cwf_Master__R_Field_14.newFieldInGroup("cwf_Master_Pnd_Date_C", "#DATE-C", FieldType.NUMERIC, 8);

        cwf_Master__R_Field_15 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master__R_Field_15", "REDEFINE", cwf_Master_Last_Chnge_Dte_Tme);
        cwf_Master_Pnd_Date_Ca = cwf_Master__R_Field_15.newFieldInGroup("cwf_Master_Pnd_Date_Ca", "#DATE-CA", FieldType.STRING, 8);
        cwf_Master_Last_Chnge_Oprtr_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Last_Chnge_Oprtr_Cde", "LAST-CHNGE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "LAST_CHNGE_OPRTR_CDE");
        cwf_Master_Last_Chnge_Oprtr_Cde.setDdmHeader("LOG CREATE/OPERATOR");
        cwf_Master_Last_Chnge_Invrt_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Last_Chnge_Invrt_Dte_Tme", "LAST-CHNGE-INVRT-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "LAST_CHNGE_INVRT_DTE_TME");
        cwf_Master_Last_Chnge_Unit_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Last_Chnge_Unit_Cde", "LAST-CHNGE-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "LAST_CHNGE_UNIT_CDE");
        cwf_Master_Last_Chnge_Unit_Cde.setDdmHeader("LAST/CHANGE/UNIT");
        cwf_Master_Step_Id = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Step_Id", "STEP-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "STEP_ID");
        cwf_Master_Step_Id.setDdmHeader("STEP/ID");
        cwf_Master_Admin_Unit_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ADMIN_UNIT_CDE");

        cwf_Master__R_Field_16 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master__R_Field_16", "REDEFINE", cwf_Master_Admin_Unit_Cde);
        cwf_Master_Unit_Id_Cde = cwf_Master__R_Field_16.newFieldInGroup("cwf_Master_Unit_Id_Cde", "UNIT-ID-CDE", FieldType.STRING, 5);
        cwf_Master_Admin_Status_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 4, 
            RepeatingFieldStrategy.None, "ADMIN_STATUS_CDE");
        cwf_Master_Status_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Status_Cde", "STATUS-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "STATUS_CDE");
        cwf_Master_Status_Updte_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Status_Updte_Dte_Tme", "STATUS-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "STATUS_UPDTE_DTE_TME");
        cwf_Master_Status_Updte_Dte_Tme.setDdmHeader("STATUS UPDATE/DATE-TIME");
        cwf_Master_Last_Updte_Dte = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Last_Updte_Dte", "LAST-UPDTE-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "LAST_UPDTE_DTE");
        cwf_Master_Last_Updte_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Last_Updte_Dte_Tme", "LAST-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LAST_UPDTE_DTE_TME");
        cwf_Master_Last_Updte_Oprtr_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Last_Updte_Oprtr_Cde", "LAST-UPDTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "LAST_UPDTE_OPRTR_CDE");
        cwf_Master_Actve_Ind = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTVE_IND");

        cwf_Master__R_Field_17 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master__R_Field_17", "REDEFINE", cwf_Master_Actve_Ind);
        cwf_Master_Actve_Ind_1 = cwf_Master__R_Field_17.newFieldInGroup("cwf_Master_Actve_Ind_1", "ACTVE-IND-1", FieldType.STRING, 1);
        cwf_Master_Actve_Ind_2 = cwf_Master__R_Field_17.newFieldInGroup("cwf_Master_Actve_Ind_2", "ACTVE-IND-2", FieldType.STRING, 1);
        cwf_Master_Crprte_Status_Ind = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Crprte_Status_Ind", "CRPRTE-STATUS-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CRPRTE_STATUS_IND");
        cwf_Master_Crprte_Status_Ind.setDdmHeader("CORP/STAT");
        cwf_Master_Tiaa_Rcvd_Dte = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIAA_RCVD_DTE");
        cwf_Master_Effctve_Dte = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Effctve_Dte", "EFFCTVE-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "EFFCTVE_DTE");
        cwf_Master_Trnsctn_Dte = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Trnsctn_Dte", "TRNSCTN-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TRNSCTN_DTE");
        cwf_Master_Trnsctn_Dte.setDdmHeader("TRANSACTION/DATE");
        cwf_Master_Trans_Dte = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Trans_Dte", "TRANS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        cwf_Master_Trans_Dte.setDdmHeader("CHECK MAIL/DATE");
        cwf_Master_Physcl_Fldr_Id_Nbr = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Physcl_Fldr_Id_Nbr", "PHYSCL-FLDR-ID-NBR", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "PHYSCL_FLDR_ID_NBR");
        cwf_Master_Physcl_Fldr_Id_Nbr.setDdmHeader("PHYSICAL FOLDER/ID NUMBER");
        cwf_Master_Mj_Chrge_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Mj_Chrge_Dte_Tme", "MJ-CHRGE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "MJ_CHRGE_DTE_TME");
        cwf_Master_Mj_Chrge_Dte_Tme.setDdmHeader("CHARGEOUT/DATE-TIME");
        cwf_Master_Mj_Pull_Ind = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Mj_Pull_Ind", "MJ-PULL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "MJ_PULL_IND");
        cwf_Master_Mj_Pull_Ind.setDdmHeader("MJ PULL/IND");
        cwf_Master_Status_Freeze_Ind = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Status_Freeze_Ind", "STATUS-FREEZE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "STATUS_FREEZE_IND");
        cwf_Master_Status_Freeze_Ind.setDdmHeader("FREEZE/STATUS");
        cwf_Master_Work_List_Ind = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Work_List_Ind", "WORK-LIST-IND", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "WORK_LIST_IND");
        cwf_Master_Work_List_Ind.setDdmHeader("WORK LIST INDICATOR");
        cwf_Master_Unit_Clock_Start_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Unit_Clock_Start_Dte_Tme", "UNIT-CLOCK-START-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "UNIT_CLOCK_START_DTE_TME");
        cwf_Master_Unit_Clock_Start_Dte_Tme.setDdmHeader("UNIT CLOCK START DATE TIME");
        cwf_Master_Unit_Clock_End_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Unit_Clock_End_Dte_Tme", "UNIT-CLOCK-END-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "UNIT_CLOCK_END_DTE_TME");
        cwf_Master_Unit_Clock_End_Dte_Tme.setDdmHeader("UNIT CLOCK END DATE TIME");
        cwf_Master_Empl_Clock_Start_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Empl_Clock_Start_Dte_Tme", "EMPL-CLOCK-START-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "EMPL_CLOCK_START_DTE_TME");
        cwf_Master_Empl_Clock_Start_Dte_Tme.setDdmHeader("EMPLOYEE CLOCK START DATE TIME");
        cwf_Master_Empl_Clock_End_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Empl_Clock_End_Dte_Tme", "EMPL-CLOCK-END-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "EMPL_CLOCK_END_DTE_TME");
        cwf_Master_Empl_Clock_End_Dte_Tme.setDdmHeader("EMPLOYEE CLOCK END DATE TIME");
        cwf_Master_Step_Clock_Start_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Step_Clock_Start_Dte_Tme", "STEP-CLOCK-START-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "STEP_CLOCK_START_DTE_TME");
        cwf_Master_Step_Clock_Start_Dte_Tme.setDdmHeader("STEP START DATE TIME");
        cwf_Master_Step_Clock_End_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Step_Clock_End_Dte_Tme", "STEP-CLOCK-END-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "STEP_CLOCK_END_DTE_TME");
        cwf_Master_Step_Clock_End_Dte_Tme.setDdmHeader("STEP CLOCK END DATE TIME");
        cwf_Master_Crprte_Clock_End_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Crprte_Clock_End_Dte_Tme", "CRPRTE-CLOCK-END-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "CRPRTE_CLOCK_END_DTE_TME");
        cwf_Master_Crprte_Clock_End_Dte_Tme.setDdmHeader("CORPORATE CLOCK END DATE TIME");
        cwf_Master_Final_Close_Out_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Final_Close_Out_Dte_Tme", "FINAL-CLOSE-OUT-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "FINAL_CLOSE_OUT_DTE_TME");
        cwf_Master_Final_Close_Out_Dte_Tme.setDdmHeader("FINAL/CLOSEOUT/DATE-TIME");
        cwf_Master_Rqst_Id = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Rqst_Id", "RQST-ID", FieldType.STRING, 28, RepeatingFieldStrategy.None, 
            "RQST_ID");
        cwf_Master_Rqst_Id.setDdmHeader("REQUEST ID");

        cwf_Master__R_Field_18 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master__R_Field_18", "REDEFINE", cwf_Master_Rqst_Id);
        cwf_Master_Rqst_Work_Prcss_Id = cwf_Master__R_Field_18.newFieldInGroup("cwf_Master_Rqst_Work_Prcss_Id", "RQST-WORK-PRCSS-ID", FieldType.STRING, 
            6);
        cwf_Master_Rqst_Tiaa_Dte_Tme = cwf_Master__R_Field_18.newFieldInGroup("cwf_Master_Rqst_Tiaa_Dte_Tme", "RQST-TIAA-DTE-TME", FieldType.STRING, 8);
        cwf_Master_Rlte_Rqst_Id = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Rlte_Rqst_Id", "RLTE-RQST-ID", FieldType.STRING, 28, RepeatingFieldStrategy.None, 
            "RLTE_RQST_ID");
        registerRecord(vw_cwf_Master);

        pnd_Mit_Counter_O = localVariables.newFieldInRecord("pnd_Mit_Counter_O", "#MIT-COUNTER-O", FieldType.PACKED_DECIMAL, 8);
        pnd_Mit_Counter_C = localVariables.newFieldInRecord("pnd_Mit_Counter_C", "#MIT-COUNTER-C", FieldType.PACKED_DECIMAL, 8);
        pnd_Nfd_Counter = localVariables.newFieldInRecord("pnd_Nfd_Counter", "#NFD-COUNTER", FieldType.PACKED_DECIMAL, 8);
        pnd_Now_Counter = localVariables.newFieldInRecord("pnd_Now_Counter", "#NOW-COUNTER", FieldType.PACKED_DECIMAL, 8);
        pnd_Act_Counter = localVariables.newFieldInRecord("pnd_Act_Counter", "#ACT-COUNTER", FieldType.PACKED_DECIMAL, 8);
        pnd_Cor_Counter = localVariables.newFieldInRecord("pnd_Cor_Counter", "#COR-COUNTER", FieldType.PACKED_DECIMAL, 8);
        pnd_Inter_Cnt = localVariables.newFieldInRecord("pnd_Inter_Cnt", "#INTER-CNT", FieldType.NUMERIC, 3);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 13);
        pnd_Found_Sw = localVariables.newFieldInRecord("pnd_Found_Sw", "#FOUND-SW", FieldType.STRING, 1);
        pnd_Disc_Msg = localVariables.newFieldInRecord("pnd_Disc_Msg", "#DISC-MSG", FieldType.STRING, 63);

        pnd_Disc_Msg__R_Field_19 = localVariables.newGroupInRecord("pnd_Disc_Msg__R_Field_19", "REDEFINE", pnd_Disc_Msg);
        pnd_Disc_Msg_Pnd_Disc_Msg_1 = pnd_Disc_Msg__R_Field_19.newFieldInGroup("pnd_Disc_Msg_Pnd_Disc_Msg_1", "#DISC-MSG-1", FieldType.STRING, 34);
        pnd_Disc_Msg_Pnd_Disc_Msg_2 = pnd_Disc_Msg__R_Field_19.newFieldInGroup("pnd_Disc_Msg_Pnd_Disc_Msg_2", "#DISC-MSG-2", FieldType.STRING, 29);

        pnd_Disc_Msg__R_Field_20 = pnd_Disc_Msg__R_Field_19.newGroupInGroup("pnd_Disc_Msg__R_Field_20", "REDEFINE", pnd_Disc_Msg_Pnd_Disc_Msg_2);
        pnd_Disc_Msg_Pnd_Disc_Msg_2_1 = pnd_Disc_Msg__R_Field_20.newFieldInGroup("pnd_Disc_Msg_Pnd_Disc_Msg_2_1", "#DISC-MSG-2-1", FieldType.STRING, 15);
        pnd_Disc_Msg_Pnd_Disc_Msg_2_2 = pnd_Disc_Msg__R_Field_20.newFieldInGroup("pnd_Disc_Msg_Pnd_Disc_Msg_2_2", "#DISC-MSG-2-2", FieldType.STRING, 14);
        pnd_Stop_Sweep_Time_A = localVariables.newFieldInRecord("pnd_Stop_Sweep_Time_A", "#STOP-SWEEP-TIME-A", FieldType.STRING, 15);

        pnd_Stop_Sweep_Time_A__R_Field_21 = localVariables.newGroupInRecord("pnd_Stop_Sweep_Time_A__R_Field_21", "REDEFINE", pnd_Stop_Sweep_Time_A);
        pnd_Stop_Sweep_Time_A_Pnd_Stop_Yyyymmdd = pnd_Stop_Sweep_Time_A__R_Field_21.newFieldInGroup("pnd_Stop_Sweep_Time_A_Pnd_Stop_Yyyymmdd", "#STOP-YYYYMMDD", 
            FieldType.STRING, 8);
        pnd_Stop_Sweep_Time_A_Pnd_Stop_Hhiisst = pnd_Stop_Sweep_Time_A__R_Field_21.newFieldInGroup("pnd_Stop_Sweep_Time_A_Pnd_Stop_Hhiisst", "#STOP-HHIISST", 
            FieldType.STRING, 7);

        pnd_Stop_Sweep_Time_A__R_Field_22 = localVariables.newGroupInRecord("pnd_Stop_Sweep_Time_A__R_Field_22", "REDEFINE", pnd_Stop_Sweep_Time_A);
        pnd_Stop_Sweep_Time_A_Pnd_Stop_Sweep_Time_N = pnd_Stop_Sweep_Time_A__R_Field_22.newFieldInGroup("pnd_Stop_Sweep_Time_A_Pnd_Stop_Sweep_Time_N", 
            "#STOP-SWEEP-TIME-N", FieldType.NUMERIC, 15);
        pnd_Stop_Sweep_Invrt_Time = localVariables.newFieldInRecord("pnd_Stop_Sweep_Invrt_Time", "#STOP-SWEEP-INVRT-TIME", FieldType.NUMERIC, 15);
        pnd_Time = localVariables.newFieldInRecord("pnd_Time", "#TIME", FieldType.TIME);
        pnd_Rqst_Log_Dte_Tme_Save = localVariables.newFieldInRecord("pnd_Rqst_Log_Dte_Tme_Save", "#RQST-LOG-DTE-TME-SAVE", FieldType.STRING, 15);
        pnd_Pin_Nbr_Save = localVariables.newFieldInRecord("pnd_Pin_Nbr_Save", "#PIN-NBR-SAVE", FieldType.NUMERIC, 12);
        pnd_Wpid_Save = localVariables.newFieldInRecord("pnd_Wpid_Save", "#WPID-SAVE", FieldType.STRING, 6);
        pnd_Tiaa_Rcvd_Dte_Save = localVariables.newFieldInRecord("pnd_Tiaa_Rcvd_Dte_Save", "#TIAA-RCVD-DTE-SAVE", FieldType.DATE);
        pnd_Rqst_Id_Save = localVariables.newFieldInRecord("pnd_Rqst_Id_Save", "#RQST-ID-SAVE", FieldType.STRING, 28);
        pnd_Rlte_Rqst_Id_Save = localVariables.newFieldInRecord("pnd_Rlte_Rqst_Id_Save", "#RLTE-RQST-ID-SAVE", FieldType.STRING, 28);
        pnd_Last_Chnge_Dte_Tme_Save = localVariables.newFieldInRecord("pnd_Last_Chnge_Dte_Tme_Save", "#LAST-CHNGE-DTE-TME-SAVE", FieldType.NUMERIC, 15);
        pnd_Crprte_Stat_Ind_Save = localVariables.newFieldInRecord("pnd_Crprte_Stat_Ind_Save", "#CRPRTE-STAT-IND-SAVE", FieldType.STRING, 1);
        pnd_Final_Close_Out_Save = localVariables.newFieldInRecord("pnd_Final_Close_Out_Save", "#FINAL-CLOSE-OUT-SAVE", FieldType.TIME);
        pnd_Crprte_Clock_Dte_Save = localVariables.newFieldInRecord("pnd_Crprte_Clock_Dte_Save", "#CRPRTE-CLOCK-DTE-SAVE", FieldType.STRING, 15);
        pnd_Mit_Status_Cde_Save = localVariables.newFieldInRecord("pnd_Mit_Status_Cde_Save", "#MIT-STATUS-CDE-SAVE", FieldType.STRING, 4);
        pnd_Discrep_Sw = localVariables.newFieldInRecord("pnd_Discrep_Sw", "#DISCREP-SW", FieldType.STRING, 1);
        pnd_Sec_Discrep = localVariables.newFieldInRecord("pnd_Sec_Discrep", "#SEC-DISCREP", FieldType.STRING, 1);
        pnd_Wpid_Chnge_Op = localVariables.newFieldInRecord("pnd_Wpid_Chnge_Op", "#WPID-CHNGE-OP", FieldType.NUMERIC, 7);
        pnd_Pin_Chnge_Op = localVariables.newFieldInRecord("pnd_Pin_Chnge_Op", "#PIN-CHNGE-OP", FieldType.NUMERIC, 12);
        pnd_Tiaa_Chnge_Op = localVariables.newFieldInRecord("pnd_Tiaa_Chnge_Op", "#TIAA-CHNGE-OP", FieldType.NUMERIC, 7);
        pnd_Rqid_Chnge_Op = localVariables.newFieldInRecord("pnd_Rqid_Chnge_Op", "#RQID-CHNGE-OP", FieldType.NUMERIC, 7);
        pnd_Rlte_Chnge_Op = localVariables.newFieldInRecord("pnd_Rlte_Chnge_Op", "#RLTE-CHNGE-OP", FieldType.NUMERIC, 7);
        pnd_Stat_Chnge_Op = localVariables.newFieldInRecord("pnd_Stat_Chnge_Op", "#STAT-CHNGE-OP", FieldType.NUMERIC, 7);
        pnd_Fncl_Chnge_Op = localVariables.newFieldInRecord("pnd_Fncl_Chnge_Op", "#FNCL-CHNGE-OP", FieldType.NUMERIC, 7);
        pnd_Clck_Chnge_Op = localVariables.newFieldInRecord("pnd_Clck_Chnge_Op", "#CLCK-CHNGE-OP", FieldType.NUMERIC, 7);
        pnd_Sec_Tatm_Chnge_Op = localVariables.newFieldInRecord("pnd_Sec_Tatm_Chnge_Op", "#SEC-TATM-CHNGE-OP", FieldType.NUMERIC, 7);
        pnd_Sec_Effd_Chnge_Op = localVariables.newFieldInRecord("pnd_Sec_Effd_Chnge_Op", "#SEC-EFFD-CHNGE-OP", FieldType.NUMERIC, 7);
        pnd_Sec_Trns_Chnge_Op = localVariables.newFieldInRecord("pnd_Sec_Trns_Chnge_Op", "#SEC-TRNS-CHNGE-OP", FieldType.NUMERIC, 7);
        pnd_Late_Chnge_Op = localVariables.newFieldInRecord("pnd_Late_Chnge_Op", "#LATE-CHNGE-OP", FieldType.NUMERIC, 7);
        pnd_Set_To_Zero_Op = localVariables.newFieldInRecord("pnd_Set_To_Zero_Op", "#SET-TO-ZERO-OP", FieldType.NUMERIC, 7);
        pnd_Set_To_Nine_Op = localVariables.newFieldInRecord("pnd_Set_To_Nine_Op", "#SET-TO-NINE-OP", FieldType.NUMERIC, 7);
        pnd_Wpid_Chnge_Cl = localVariables.newFieldInRecord("pnd_Wpid_Chnge_Cl", "#WPID-CHNGE-CL", FieldType.NUMERIC, 7);
        pnd_Pin_Chnge_Cl = localVariables.newFieldInRecord("pnd_Pin_Chnge_Cl", "#PIN-CHNGE-CL", FieldType.NUMERIC, 12);
        pnd_Tiaa_Chnge_Cl = localVariables.newFieldInRecord("pnd_Tiaa_Chnge_Cl", "#TIAA-CHNGE-CL", FieldType.NUMERIC, 7);
        pnd_Rqid_Chnge_Cl = localVariables.newFieldInRecord("pnd_Rqid_Chnge_Cl", "#RQID-CHNGE-CL", FieldType.NUMERIC, 7);
        pnd_Rlte_Chnge_Cl = localVariables.newFieldInRecord("pnd_Rlte_Chnge_Cl", "#RLTE-CHNGE-CL", FieldType.NUMERIC, 7);
        pnd_Stat_Chnge_Cl = localVariables.newFieldInRecord("pnd_Stat_Chnge_Cl", "#STAT-CHNGE-CL", FieldType.NUMERIC, 7);
        pnd_Fncl_Chnge_Cl = localVariables.newFieldInRecord("pnd_Fncl_Chnge_Cl", "#FNCL-CHNGE-CL", FieldType.NUMERIC, 7);
        pnd_Clck_Chnge_Cl = localVariables.newFieldInRecord("pnd_Clck_Chnge_Cl", "#CLCK-CHNGE-CL", FieldType.NUMERIC, 7);
        pnd_Sec_Tatm_Chnge_Cl = localVariables.newFieldInRecord("pnd_Sec_Tatm_Chnge_Cl", "#SEC-TATM-CHNGE-CL", FieldType.NUMERIC, 7);
        pnd_Sec_Effd_Chnge_Cl = localVariables.newFieldInRecord("pnd_Sec_Effd_Chnge_Cl", "#SEC-EFFD-CHNGE-CL", FieldType.NUMERIC, 7);
        pnd_Sec_Trns_Chnge_Cl = localVariables.newFieldInRecord("pnd_Sec_Trns_Chnge_Cl", "#SEC-TRNS-CHNGE-CL", FieldType.NUMERIC, 7);
        pnd_Late_Chnge_Cl = localVariables.newFieldInRecord("pnd_Late_Chnge_Cl", "#LATE-CHNGE-CL", FieldType.NUMERIC, 7);
        pnd_Cor_Stat_Cntr = localVariables.newFieldInRecord("pnd_Cor_Stat_Cntr", "#COR-STAT-CNTR", FieldType.NUMERIC, 7);
        pnd_Pin_Rqst_Key = localVariables.newFieldInRecord("pnd_Pin_Rqst_Key", "#PIN-RQST-KEY", FieldType.STRING, 55);

        pnd_Pin_Rqst_Key__R_Field_23 = localVariables.newGroupInRecord("pnd_Pin_Rqst_Key__R_Field_23", "REDEFINE", pnd_Pin_Rqst_Key);
        pnd_Pin_Rqst_Key_Pnd_Pin_Nbr = pnd_Pin_Rqst_Key__R_Field_23.newFieldInGroup("pnd_Pin_Rqst_Key_Pnd_Pin_Nbr", "#PIN-NBR", FieldType.NUMERIC, 12);
        pnd_Pin_Rqst_Key_Pnd_Rqst_Id = pnd_Pin_Rqst_Key__R_Field_23.newFieldInGroup("pnd_Pin_Rqst_Key_Pnd_Rqst_Id", "#RQST-ID", FieldType.STRING, 28);
        pnd_Pin_Rqst_Key_Pnd_Rqst_Log_Dte_Tme = pnd_Pin_Rqst_Key__R_Field_23.newFieldInGroup("pnd_Pin_Rqst_Key_Pnd_Rqst_Log_Dte_Tme", "#RQST-LOG-DTE-TME", 
            FieldType.STRING, 15);
        pnd_Log_Dte_Tme_St_Event_Key = localVariables.newFieldInRecord("pnd_Log_Dte_Tme_St_Event_Key", "#LOG-DTE-TME-ST-EVENT-KEY", FieldType.STRING, 
            22);

        pnd_Log_Dte_Tme_St_Event_Key__R_Field_24 = localVariables.newGroupInRecord("pnd_Log_Dte_Tme_St_Event_Key__R_Field_24", "REDEFINE", pnd_Log_Dte_Tme_St_Event_Key);
        pnd_Log_Dte_Tme_St_Event_Key_Pnd_Rqst_Log_Dte_Tme = pnd_Log_Dte_Tme_St_Event_Key__R_Field_24.newFieldInGroup("pnd_Log_Dte_Tme_St_Event_Key_Pnd_Rqst_Log_Dte_Tme", 
            "#RQST-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Log_Dte_Tme_St_Event_Key_Pnd_Strtng_Event_Dte_Tme = pnd_Log_Dte_Tme_St_Event_Key__R_Field_24.newFieldInGroup("pnd_Log_Dte_Tme_St_Event_Key_Pnd_Strtng_Event_Dte_Tme", 
            "#STRTNG-EVENT-DTE-TME", FieldType.TIME);
        pnd_Crprte_Clock_End_Dte_Tme_T = localVariables.newFieldInRecord("pnd_Crprte_Clock_End_Dte_Tme_T", "#CRPRTE-CLOCK-END-DTE-TME-T", FieldType.TIME);
        pnd_Late_Ind = localVariables.newFieldInRecord("pnd_Late_Ind", "#LATE-IND", FieldType.STRING, 1);
        pnd_Who_Ind = localVariables.newFieldInRecord("pnd_Who_Ind", "#WHO-IND", FieldType.STRING, 1);
        pnd_Check_Date = localVariables.newFieldInRecord("pnd_Check_Date", "#CHECK-DATE", FieldType.BOOLEAN, 1);
        pnd_This_Program_Has_Finished_Successfully = localVariables.newFieldInRecord("pnd_This_Program_Has_Finished_Successfully", "#THIS-PROGRAM-HAS-FINISHED-SUCCESSFULLY", 
            FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        setTimeRecord = new DbsRecord();
        st = setTimeRecord.newFieldInRecord("ST", "ST", FieldType.NUMERIC, 7);
        registerRecord(setTimeRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Master_Histo_1.reset();
        vw_cwf_Who_Work_Request.reset();
        vw_cwf_Who_Activity_File.reset();
        vw_cwf_Master.reset();

        ldaCwfl3019.initializeValues();
        ldaMihl0210.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb3020() throws Exception
    {
        super("Cwfb3020");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*    INITIALIZATION
        pnd_Begin_Key.reset();                                                                                                                                            //Natural: FORMAT ( 0 ) PS = 56 LS = 131;//Natural: RESET #BEGIN-KEY
        pnd_End_Key.reset();                                                                                                                                              //Natural: RESET #END-KEY
        pnd_Log_Dte_Tme_St_Event_Key_Pnd_Strtng_Event_Dte_Tme.reset();                                                                                                    //Natural: RESET #STRTNG-EVENT-DTE-TME
        cwf_Master_Histo_1_Actv_Unque_Key.reset();                                                                                                                        //Natural: RESET ACTV-UNQUE-KEY
        getReports().getPageNumberDbs(0).reset();                                                                                                                         //Natural: RESET *PAGE-NUMBER ( 0 )
        //*   TO ALLOW FOR EASY ESCAPE FROM PROGRAM.
        REP1:                                                                                                                                                             //Natural: REPEAT
        while (condition(whileTrue))
        {
            //*  PERFORM PREVENT-INVALID-CID
            st = Global.getTIMN();                                                                                                                                        //Natural: SET TIME
                                                                                                                                                                          //Natural: PERFORM READ-RUN-CONTROL
            sub_Read_Run_Control();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  MANUALY CREATED
            if (condition(pdaMiha0200.getMiha0200_Output_Control_Rec_Status().notEquals("P")))                                                                            //Natural: IF MIHA0200-OUTPUT.CONTROL-REC-STATUS NE 'P'
            {
                                                                                                                                                                          //Natural: PERFORM READ-ARCHIVE-CONTROL
                sub_Read_Archive_Control();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("REP1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //* *****
            pnd_Begin_Key_Pnd_B_Crprte_Status_Ind.setValue("0");                                                                                                          //Natural: ASSIGN #B-CRPRTE-STATUS-IND := '0'
            pnd_End_Key_Pnd_E_Crprte_Status_Ind.setValue("0");                                                                                                            //Natural: ASSIGN #E-CRPRTE-STATUS-IND := '0'
                                                                                                                                                                          //Natural: PERFORM SELECT-MASTER-RECORDS-OPEN
            sub_Select_Master_Records_Open();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Begin_Key_Pnd_B_Crprte_Status_Ind.setValue("9");                                                                                                          //Natural: ASSIGN #B-CRPRTE-STATUS-IND := '9'
            pnd_End_Key_Pnd_E_Crprte_Status_Ind.setValue("9");                                                                                                            //Natural: ASSIGN #E-CRPRTE-STATUS-IND := '9'
                                                                                                                                                                          //Natural: PERFORM SELECT-MASTER-RECORDS-CLOSE
            sub_Select_Master_Records_Close();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-TOTALS
            sub_Write_Totals();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM STATISTIC
            sub_Statistic();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_This_Program_Has_Finished_Successfully.setValue(true);                                                                                                    //Natural: ASSIGN #THIS-PROGRAM-HAS-FINISHED-SUCCESSFULLY := TRUE
                                                                                                                                                                          //Natural: PERFORM ESCAPE-PROGRAM
            sub_Escape_Program();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ESCAPE-PROGRAM
            //*   (REP1.)
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  CALL MIHP0415 TO COMPLETE THE RUN CONTROL RECORD IF SUCCESSFUL
        //*  --------------------------------------------------------------
        if (condition(pnd_This_Program_Has_Finished_Successfully.equals(true)))                                                                                           //Natural: IF #THIS-PROGRAM-HAS-FINISHED-SUCCESSFULLY = TRUE
        {
            DbsUtil.invokeMain(DbsUtil.getBlType("MIHP0415"), getCurrentProcessState());                                                                                  //Natural: FETCH RETURN 'MIHP0415'
            if (condition(Global.isEscape())) return;
            //* (0685)
        }                                                                                                                                                                 //Natural: END-IF
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-RUN-CONTROL
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-ARCHIVE-CONTROL
        //*  WRITE / 'End-Dte-Tme after compare: ' #END-DTE-TME
        //*        / 'End-Dte-Tme from MIHA0100: ' MIHA0100.ENDING-TIME
        //*        (EM=YYYYMMDDHHIISST)
        //* ******************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SELECT-MASTER-RECORDS-OPEN
        //* ******************************************************
        //*  FIND WHO WORK REQUEST RECORD
        //* ********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SELECT-MASTER-RECORDS-CLOSE
        //* ********************************************
        //*  FIND WHO WORK REQUEST RECORD
        //* *****************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TOTALS
        //* ****************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WORK-RQST-UPDATE
        //* ****************
        //* ***
        //* ***
        //*  CWF-WHO-WORK-REQUEST-VIEW.DWNLD-UPDTE-DTE-TME := *TIMX
        //* ****************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STATISTIC
    }
    private void sub_Escape_Program() throws Exception                                                                                                                    //Natural: ESCAPE-PROGRAM
    {
        if (BLNatReinput.isReinput()) return;

        //*  ================================
        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( REP1. )
        Global.setEscapeCode(EscapeType.Bottom, "REP1");
        if (true) return;
    }
    private void sub_Read_Run_Control() throws Exception                                                                                                                  //Natural: READ-RUN-CONTROL
    {
        if (BLNatReinput.isReinput()) return;

        //*  ================================
        //*  CALL THE SUB PROGRAM 'Set UP Run Control for MIT vs. WHO Comparison
        //*  PROCESS'. If a Critical ERROR is returned, Print Message & Terminate
        //*  THE JOB. COMMIT TRANSACTION FOR RUN-CONTROL SET UP.
        //*  ---------------------------------------------------------------------
        DbsUtil.callnat(Mihn0210.class , getCurrentProcessState(), pdaMiha0200.getMiha0200_Output(), pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(),             //Natural: CALLNAT 'MIHN0210' MIHA0200-OUTPUT CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
            pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("E") || pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("W")))                    //Natural: IF MSG-INFO-SUB.##RETURN-CODE = 'E' OR MSG-INFO-SUB.##RETURN-CODE = 'W'
        {
            getReports().write(0, ReportOption.NOTITLE,new TabSetting(10),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                     //Natural: WRITE 10T MSG-INFO-SUB.##MSG
            if (Global.isEscape()) return;
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("E")))                                                                                 //Natural: IF MSG-INFO-SUB.##RETURN-CODE = 'E'
            {
                DbsUtil.terminate(10);  if (true) return;                                                                                                                 //Natural: TERMINATE 10
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().equals(999)))                                                                                          //Natural: IF MSG-INFO-SUB.##MSG-NR = 999
        {
            getReports().write(0, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(10),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                  //Natural: WRITE ( 0 ) NOTITLE NOHDR 10T MSG-INFO-SUB.##MSG
            if (Global.isEscape()) return;
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
            DbsUtil.terminate(11);  if (true) return;                                                                                                                     //Natural: TERMINATE 11
        }                                                                                                                                                                 //Natural: END-IF
        //*  COMMIT THE TRANSACTION FOR THE RUN CONTROL SET UP
        //*  -------------------------------------------------
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //* ***********
        GET01:                                                                                                                                                            //Natural: GET CWF-WHO-SUPPORT-TBL MIHA0200-OUTPUT.RUN-CONTROL-ISN
        ldaMihl0210.getVw_cwf_Who_Support_Tbl().readByID(pdaMiha0200.getMiha0200_Output_Run_Control_Isn().getLong(), "GET01");
        //* **
        pnd_Tbl_Split.setValue(ldaMihl0210.getCwf_Who_Support_Tbl_Tbl_Data_Field());                                                                                      //Natural: MOVE CWF-WHO-SUPPORT-TBL.TBL-DATA-FIELD TO #TBL-SPLIT
        //*  MANUALY CREATED
        if (condition(pdaMiha0200.getMiha0200_Output_Control_Rec_Status().equals("P")))                                                                                   //Natural: IF MIHA0200-OUTPUT.CONTROL-REC-STATUS EQ 'P'
        {
            pnd_Conversion_Alpha.setValueEdited(pnd_Tbl_Split_Pnd_Tbl_Stop_Dt,new ReportEditMask("YYYYMMDDHHIISST"));                                                     //Natural: MOVE EDITED #TBL-STOP-DT ( EM = YYYYMMDDHHIISST ) TO #CONVERSION-ALPHA
            pnd_Begin_Key_Pnd_B_Last_Chnge_Invrt_Dte_Tme.compute(new ComputeParameters(false, pnd_Begin_Key_Pnd_B_Last_Chnge_Invrt_Dte_Tme), (new DbsDecimal("999999999999999").subtract(pnd_Conversion_Alpha_Pnd_Conversion_Numeric))); //Natural: COMPUTE #B-LAST-CHNGE-INVRT-DTE-TME = ( 999999999999999 - #CONVERSION-NUMERIC )
            pnd_Display_Stop_Period.setValue(pnd_Conversion_Alpha_Pnd_Conversion_Numeric);                                                                                //Natural: MOVE #CONVERSION-NUMERIC TO #DISPLAY-STOP-PERIOD
            pnd_Date_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),pnd_Conversion_Alpha);                                                                        //Natural: MOVE EDITED #CONVERSION-ALPHA TO #DATE-T ( EM = YYYYMMDDHHIISST )
            //*  WRITE 'END TIME FOR SWEEP IS(MANUAL CONTROL):' #CONVERSION-ALPHA
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Conversion_Alpha.setValueEdited(pnd_Tbl_Split_Pnd_Tbl_Start_Dt,new ReportEditMask("YYYYMMDDHHIISST"));                                                        //Natural: MOVE EDITED #TBL-START-DT ( EM = YYYYMMDDHHIISST ) TO #CONVERSION-ALPHA
        //*  WRITE 'START TIME FOR SWEEP IS:' #CONVERSION-ALPHA
        pnd_End_Key_Pnd_E_Last_Chnge_Invrt_Dte_Tme.compute(new ComputeParameters(false, pnd_End_Key_Pnd_E_Last_Chnge_Invrt_Dte_Tme), (new DbsDecimal("999999999999999").subtract(pnd_Conversion_Alpha_Pnd_Conversion_Numeric))); //Natural: COMPUTE #E-LAST-CHNGE-INVRT-DTE-TME = ( 999999999999999 - #CONVERSION-NUMERIC )
        pnd_Display_Strt_Period.setValue(pnd_Conversion_Alpha_Pnd_Conversion_Numeric);                                                                                    //Natural: MOVE #CONVERSION-NUMERIC TO #DISPLAY-STRT-PERIOD
        //* ************
    }
    private void sub_Read_Archive_Control() throws Exception                                                                                                              //Natural: READ-ARCHIVE-CONTROL
    {
        if (BLNatReinput.isReinput()) return;

        //*  ====================================
        //*  CALL THE SUB PROGRAM TO READ COMPLETED RUN CONTROL RECORD FOR MIT
        //*  ARCHIVING PROCESS. PICKUP LAST PROCESSED DATE & TIME FIELD FOR LATER
        //*  COMPARISON TO STOP THE SWEEP.
        //*  ---------------------------------------------------------------------
        pnd_Curr_Dte_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("YYYYMMDDHHIISST"));                                                                          //Natural: MOVE EDITED *TIMX ( EM = YYYYMMDDHHIISST ) TO #CURR-DTE-TME
        //*  WRITE 'CURRENT TIME IS:' #CURR-DTE-TME
        DbsUtil.callnat(Mihn0176.class , getCurrentProcessState(), pdaMiha0100.getMiha0100(), pdaMiha0100.getMiha0100_Id(), pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(),  //Natural: CALLNAT 'MIHN0176' MIHA0100 MIHA0100-ID CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
            pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("E") || pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("W")))                    //Natural: IF MSG-INFO-SUB.##RETURN-CODE = 'E' OR MSG-INFO-SUB.##RETURN-CODE = 'W'
        {
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(10),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                     //Natural: WRITE ( 1 ) 10T MSG-INFO-SUB.##MSG
            if (Global.isEscape()) return;
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("E")))                                                                                 //Natural: IF MSG-INFO-SUB.##RETURN-CODE = 'E'
            {
                DbsUtil.terminate(12);  if (true) return;                                                                                                                 //Natural: TERMINATE 12
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().equals(999)))                                                                                          //Natural: IF MSG-INFO-SUB.##MSG-NR = 999
        {
            getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(10),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                  //Natural: WRITE ( 1 ) NOTITLE NOHDR 10T MSG-INFO-SUB.##MSG
            if (Global.isEscape()) return;
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
            DbsUtil.terminate(13);  if (true) return;                                                                                                                     //Natural: TERMINATE 13
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Stop_Sweep_Time_A.setValueEdited(pdaMiha0100.getMiha0100_Ending_Time(),new ReportEditMask("YYYYMMDDHHIISST"));                                                //Natural: MOVE EDITED MIHA0100.ENDING-TIME ( EM = YYYYMMDDHHIISST ) TO #STOP-SWEEP-TIME-A
        //* *********** TEMPORARY       L.E.
        //*  #STOP-SWEEP-TIME-A := '199808260101010'
        //*  WRITE 'Ending Time from Arch:' #STOP-SWEEP-TIME-A
        //* ********************************************************************
        //*   CHECK CONTROL RECORD FROM ARCHIVING-IF THE END DATE IS THE GE TO
        //*   CURRENT DATE USE MIDNIGHT AS THE END DATE(LAST RUN OF ARCHIVING FI
        //*   ISHED AFTER MIDNIGHT) IF NOT ARCHIVING MAY BE CURRENTLY RUNNING SO
        //*   USE THE DATE FROM THE LAST COMPLETE RUN      - JVH 6/1/98
        //* ********************************************************************
        pnd_Date_D.setValue(Global.getDATX());                                                                                                                            //Natural: MOVE *DATX TO #DATE-D
        pnd_Date_D.nsubtract(1);                                                                                                                                          //Natural: ASSIGN #DATE-D := #DATE-D - 1
        pnd_Prev_Dte_Tme_Pnd_Prev_Yyyymmdd.setValueEdited(pnd_Date_D,new ReportEditMask("YYYYMMDD"));                                                                     //Natural: MOVE EDITED #DATE-D ( EM = YYYYMMDD ) TO #PREV-YYYYMMDD
        //*  MOVE *TIMX TO #TIME
        //*  MOVE EDITED #TIME(EM=YYYYMMDDHHIISST) TO #END-DTE-TME
        //*  WRITE / 'Stop-YYYYMMDD  : ' #STOP-YYYYMMDD
        //*        / 'Prev-YYYYMMDD  : ' #PREV-YYYYMMDD
        if (condition(pnd_Stop_Sweep_Time_A_Pnd_Stop_Yyyymmdd.greaterOrEqual(pnd_Prev_Dte_Tme_Pnd_Prev_Yyyymmdd)))                                                        //Natural: IF #STOP-YYYYMMDD GE #PREV-YYYYMMDD
        {
            pnd_End_Dte_Tme_Pnd_End_Yyyymmdd.setValueEdited(pnd_Date_D,new ReportEditMask("YYYYMMDD"));                                                                   //Natural: MOVE EDITED #DATE-D ( EM = YYYYMMDD ) TO #END-YYYYMMDD
            pnd_End_Dte_Tme_Pnd_End_Hhiisst.setValue("2359599");                                                                                                          //Natural: MOVE '2359599' TO #END-HHIISST
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_End_Dte_Tme.setValue(pnd_Stop_Sweep_Time_A);                                                                                                              //Natural: MOVE #STOP-SWEEP-TIME-A TO #END-DTE-TME
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Date_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),pnd_End_Dte_Tme);                                                                                 //Natural: MOVE EDITED #END-DTE-TME TO #DATE-T ( EM = YYYYMMDDHHIISST )
        //*  WRITE 'End-Dte-Tme before compare:' #END-DTE-TME
        if (condition(pnd_Date_T.greater(pdaMiha0100.getMiha0100_Ending_Time())))                                                                                         //Natural: IF #DATE-T GT MIHA0100.ENDING-TIME
        {
            pnd_End_Dte_Tme.setValue(pnd_Stop_Sweep_Time_A);                                                                                                              //Natural: MOVE #STOP-SWEEP-TIME-A TO #END-DTE-TME
            pnd_Date_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),pnd_End_Dte_Tme);                                                                             //Natural: MOVE EDITED #END-DTE-TME TO #DATE-T ( EM = YYYYMMDDHHIISST )
            //*  WRITE 'Final check of Archived date '
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Begin_Key_Pnd_B_Last_Chnge_Invrt_Dte_Tme.compute(new ComputeParameters(false, pnd_Begin_Key_Pnd_B_Last_Chnge_Invrt_Dte_Tme), (new DbsDecimal("999999999999999").subtract(pnd_End_Dte_Tme_Pnd_End_Dte_Tme_N))); //Natural: ASSIGN #B-LAST-CHNGE-INVRT-DTE-TME := ( 999999999999999 - #END-DTE-TME-N )
        pnd_Display_Stop_Period.setValue(pnd_Stop_Sweep_Time_A_Pnd_Stop_Sweep_Time_N);                                                                                    //Natural: MOVE #STOP-SWEEP-TIME-N TO #DISPLAY-STOP-PERIOD
        //*   TERMINATE 99
        //* ************
    }
    private void sub_Select_Master_Records_Open() throws Exception                                                                                                        //Natural: SELECT-MASTER-RECORDS-OPEN
    {
        if (BLNatReinput.isReinput()) return;

        vw_cwf_Master.startDatabaseRead                                                                                                                                   //Natural: READ CWF-MASTER BY CRPRTE-STATUS-CHNGE-DTE-KEY STARTING FROM #BEGIN-KEY
        (
        "R1",
        new Wc[] { new Wc("CRPRTE_STATUS_CHNGE_DTE_KEY", ">=", pnd_Begin_Key, WcType.BY) },
        new Oc[] { new Oc("CRPRTE_STATUS_CHNGE_DTE_KEY", "ASC") }
        );
        R1:
        while (condition(vw_cwf_Master.readNextRow("R1")))
        {
            if (condition(cwf_Master_Last_Chnge_Invrt_Dte_Tme.greater(pnd_End_Key_Pnd_E_Last_Chnge_Invrt_Dte_Tme)))                                                       //Natural: IF CWF-MASTER.LAST-CHNGE-INVRT-DTE-TME GT #E-LAST-CHNGE-INVRT-DTE-TME
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cwf_Master_Crprte_Status_Ind.equals("9")))                                                                                                      //Natural: IF CRPRTE-STATUS-IND = '9'
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cwf_Master_Pin_Nbr.equals(getZero())))                                                                                                          //Natural: REJECT IF PIN-NBR = 0
            {
                continue;
            }
            pnd_Mit_Counter_O.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #MIT-COUNTER-O
            pnd_Pin_Nbr_Save.setValue(cwf_Master_Pin_Nbr);                                                                                                                //Natural: MOVE CWF-MASTER.PIN-NBR TO #PIN-NBR-SAVE
            pnd_Wpid_Save.setValue(cwf_Master_Work_Prcss_Id);                                                                                                             //Natural: MOVE CWF-MASTER.WORK-PRCSS-ID TO #WPID-SAVE
            pnd_Tiaa_Rcvd_Dte_Save.setValue(cwf_Master_Tiaa_Rcvd_Dte);                                                                                                    //Natural: MOVE CWF-MASTER.TIAA-RCVD-DTE TO #TIAA-RCVD-DTE-SAVE
            pnd_Rqst_Id_Save.setValue(cwf_Master_Rqst_Id);                                                                                                                //Natural: MOVE CWF-MASTER.RQST-ID TO #RQST-ID-SAVE
            pnd_Rlte_Rqst_Id_Save.setValue(cwf_Master_Rlte_Rqst_Id);                                                                                                      //Natural: MOVE CWF-MASTER.RLTE-RQST-ID TO #RLTE-RQST-ID-SAVE
            pnd_Crprte_Stat_Ind_Save.setValue(cwf_Master_Crprte_Status_Ind);                                                                                              //Natural: MOVE CWF-MASTER.CRPRTE-STATUS-IND TO #CRPRTE-STAT-IND-SAVE
            pnd_Mit_Status_Cde_Save.setValue(cwf_Master_Status_Cde);                                                                                                      //Natural: MOVE CWF-MASTER.STATUS-CDE TO #MIT-STATUS-CDE-SAVE
            pnd_Final_Close_Out_Save.setValue(cwf_Master_Final_Close_Out_Dte_Tme);                                                                                        //Natural: MOVE CWF-MASTER.FINAL-CLOSE-OUT-DTE-TME TO #FINAL-CLOSE-OUT-SAVE
            pnd_Crprte_Clock_Dte_Save.setValue(cwf_Master_Crprte_Clock_End_Dte_Tme);                                                                                      //Natural: MOVE CWF-MASTER.CRPRTE-CLOCK-END-DTE-TME TO #CRPRTE-CLOCK-DTE-SAVE
            pnd_Rqst_Log_Dte_Tme_Save.setValue(cwf_Master_Rqst_Log_Dte_Tme);                                                                                              //Natural: MOVE CWF-MASTER.RQST-LOG-DTE-TME TO #RQST-LOG-DTE-TME-SAVE
            pnd_Last_Chnge_Dte_Tme_Save.setValue(cwf_Master_Last_Chnge_Dte_Tme);                                                                                          //Natural: MOVE CWF-MASTER.LAST-CHNGE-DTE-TME TO #LAST-CHNGE-DTE-TME-SAVE
            pnd_Found_Sw.setValue("F");                                                                                                                                   //Natural: MOVE 'F' TO #FOUND-SW
            vw_cwf_Who_Work_Request.startDatabaseFind                                                                                                                     //Natural: FIND CWF-WHO-WORK-REQUEST WITH RQST-LOG-DTE-TME = #RQST-LOG-DTE-TME-SAVE
            (
            "F1",
            new Wc[] { new Wc("RQST_LOG_DTE_TME", "=", pnd_Rqst_Log_Dte_Tme_Save, WcType.WITH) }
            );
            F1:
            while (condition(vw_cwf_Who_Work_Request.readNextRow("F1", true)))
            {
                vw_cwf_Who_Work_Request.setIfNotFoundControlFlag(false);
                if (condition(vw_cwf_Who_Work_Request.getAstCOUNTER().equals(0)))                                                                                         //Natural: IF NO RECORD FOUND
                {
                    pnd_Nfd_Counter.nadd(1);                                                                                                                              //Natural: ADD 1 TO #NFD-COUNTER
                    getReports().write(0, ReportOption.NOTITLE,NEWLINE,"This Open rec ntfnd on WHO RLDT,PIN,LCDT==>",pnd_Rqst_Log_Dte_Tme_Save,pnd_Pin_Nbr_Save,          //Natural: WRITE ( 0 ) / 'This Open rec ntfnd on WHO RLDT,PIN,LCDT==>' #RQST-LOG-DTE-TME-SAVE #PIN-NBR-SAVE #LAST-CHNGE-DTE-TME-SAVE
                        pnd_Last_Chnge_Dte_Tme_Save);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("F1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Found_Sw.setValue("C");                                                                                                                           //Natural: MOVE 'C' TO #FOUND-SW
                    if (true) break F1;                                                                                                                                   //Natural: ESCAPE BOTTOM ( F1. ) IMMEDIATE
                }                                                                                                                                                         //Natural: END-NOREC
                //*  COMPARE FIELDS - IF NOT EQUAL UPDATE WHO WORK REQUEST RECORD
                //*  ------------------------------------------------------------
                pnd_Discrep_Sw.reset();                                                                                                                                   //Natural: RESET #DISCREP-SW #DISC-MSG
                pnd_Disc_Msg.reset();
                if (condition(pnd_Found_Sw.equals("F")))                                                                                                                  //Natural: IF #FOUND-SW = 'F'
                {
                    pnd_Isn.setValue(vw_cwf_Who_Work_Request.getAstISN("F1"));                                                                                            //Natural: ASSIGN #ISN := *ISN ( F1. )
                    if (condition(cwf_Master_Work_Prcss_Id.notEquals(cwf_Who_Work_Request_Work_Prcss_Id)))                                                                //Natural: IF CWF-MASTER.WORK-PRCSS-ID NE CWF-WHO-WORK-REQUEST.WORK-PRCSS-ID
                    {
                        pnd_Disc_Msg_Pnd_Disc_Msg_2.reset();                                                                                                              //Natural: RESET #DISC-MSG-2
                        pnd_Discrep_Sw.setValue("Y");                                                                                                                     //Natural: MOVE 'Y' TO #DISCREP-SW
                        pnd_Wpid_Chnge_Op.nadd(1);                                                                                                                        //Natural: ADD 1 TO #WPID-CHNGE-OP
                        pnd_Disc_Msg_Pnd_Disc_Msg_2.setValue("Work Process ID difference");                                                                               //Natural: MOVE 'Work Process ID difference' TO #DISC-MSG-2
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(cwf_Master_Pin_Nbr.notEquals(cwf_Who_Work_Request_Pin_Nbr)))                                                                        //Natural: IF CWF-MASTER.PIN-NBR NE CWF-WHO-WORK-REQUEST.PIN-NBR
                        {
                            pnd_Disc_Msg_Pnd_Disc_Msg_2.reset();                                                                                                          //Natural: RESET #DISC-MSG-2
                            pnd_Discrep_Sw.setValue("Y");                                                                                                                 //Natural: MOVE 'Y' TO #DISCREP-SW
                            pnd_Pin_Chnge_Op.nadd(1);                                                                                                                     //Natural: ADD 1 TO #PIN-CHNGE-OP
                            pnd_Disc_Msg_Pnd_Disc_Msg_2.setValue("PIN Number difference");                                                                                //Natural: MOVE 'PIN Number difference' TO #DISC-MSG-2
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(cwf_Master_Tiaa_Rcvd_Dte.notEquals(cwf_Who_Work_Request_Tiaa_Rcvd_Dte)))                                                        //Natural: IF CWF-MASTER.TIAA-RCVD-DTE NE CWF-WHO-WORK-REQUEST.TIAA-RCVD-DTE
                            {
                                pnd_Disc_Msg_Pnd_Disc_Msg_2.reset();                                                                                                      //Natural: RESET #DISC-MSG-2
                                pnd_Discrep_Sw.setValue("Y");                                                                                                             //Natural: MOVE 'Y' TO #DISCREP-SW
                                pnd_Tiaa_Chnge_Op.nadd(1);                                                                                                                //Natural: ADD 1 TO #TIAA-CHNGE-OP
                                pnd_Disc_Msg_Pnd_Disc_Msg_2.setValue("TIAA Received Date difference");                                                                    //Natural: MOVE 'TIAA Received Date difference' TO #DISC-MSG-2
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                if (condition(cwf_Master_Rqst_Id.notEquals(cwf_Who_Work_Request_Rqst_Id)))                                                                //Natural: IF CWF-MASTER.RQST-ID NE CWF-WHO-WORK-REQUEST.RQST-ID
                                {
                                    pnd_Disc_Msg_Pnd_Disc_Msg_2.reset();                                                                                                  //Natural: RESET #DISC-MSG-2
                                    pnd_Discrep_Sw.setValue("Y");                                                                                                         //Natural: MOVE 'Y' TO #DISCREP-SW
                                    pnd_Rqid_Chnge_Op.nadd(1);                                                                                                            //Natural: ADD 1 TO #RQID-CHNGE-OP
                                    pnd_Disc_Msg_Pnd_Disc_Msg_2.setValue("Request ID difference");                                                                        //Natural: MOVE 'Request ID difference' TO #DISC-MSG-2
                                }                                                                                                                                         //Natural: ELSE
                                else if (condition())
                                {
                                    if (condition(cwf_Master_Rlte_Rqst_Id.notEquals(cwf_Who_Work_Request_Rlte_Rqst_Id)))                                                  //Natural: IF CWF-MASTER.RLTE-RQST-ID NE CWF-WHO-WORK-REQUEST.RLTE-RQST-ID
                                    {
                                        pnd_Disc_Msg_Pnd_Disc_Msg_2.reset();                                                                                              //Natural: RESET #DISC-MSG-2
                                        pnd_Discrep_Sw.setValue("Y");                                                                                                     //Natural: MOVE 'Y' TO #DISCREP-SW
                                        pnd_Rlte_Chnge_Op.nadd(1);                                                                                                        //Natural: ADD 1 TO #RLTE-CHNGE-OP
                                        pnd_Disc_Msg_Pnd_Disc_Msg_2.setValue("Relate Request ID difference");                                                             //Natural: MOVE 'Relate Request ID difference' TO #DISC-MSG-2
                                    }                                                                                                                                     //Natural: ELSE
                                    else if (condition())
                                    {
                                        if (condition(cwf_Master_Crprte_Status_Ind.notEquals(cwf_Who_Work_Request_Crprte_Status_Ind)))                                    //Natural: IF CWF-MASTER.CRPRTE-STATUS-IND NE CWF-WHO-WORK-REQUEST.CRPRTE-STATUS-IND
                                        {
                                            pnd_Disc_Msg_Pnd_Disc_Msg_2.reset();                                                                                          //Natural: RESET #DISC-MSG-2
                                            pnd_Discrep_Sw.setValue("Y");                                                                                                 //Natural: MOVE 'Y' TO #DISCREP-SW
                                            pnd_Stat_Chnge_Op.nadd(1);                                                                                                    //Natural: ADD 1 TO #STAT-CHNGE-OP
                                            pnd_Disc_Msg_Pnd_Disc_Msg_2.setValue("Corp-Stat-Ind ID difference");                                                          //Natural: MOVE 'Corp-Stat-Ind ID difference' TO #DISC-MSG-2
                                        }                                                                                                                                 //Natural: ELSE
                                        else if (condition())
                                        {
                                            if (condition(cwf_Master_Final_Close_Out_Dte_Tme.notEquals(cwf_Who_Work_Request_Final_Close_Out_Dte_Tme)))                    //Natural: IF CWF-MASTER.FINAL-CLOSE-OUT-DTE-TME NE CWF-WHO-WORK-REQUEST.FINAL-CLOSE-OUT-DTE-TME
                                            {
                                                pnd_Disc_Msg_Pnd_Disc_Msg_2.reset();                                                                                      //Natural: RESET #DISC-MSG-2
                                                pnd_Discrep_Sw.setValue("Y");                                                                                             //Natural: MOVE 'Y' TO #DISCREP-SW
                                                pnd_Fncl_Chnge_Op.nadd(1);                                                                                                //Natural: ADD 1 TO #FNCL-CHNGE-OP
                                                pnd_Disc_Msg_Pnd_Disc_Msg_2.setValue("Final-Clse-Out-dte-Tme difference");                                                //Natural: MOVE 'Final-Clse-Out-dte-Tme difference' TO #DISC-MSG-2
                                            }                                                                                                                             //Natural: ELSE
                                            else if (condition())
                                            {
                                                if (condition(cwf_Master_Crprte_Clock_End_Dte_Tme.notEquals(cwf_Who_Work_Request_Crprte_Clock_End_Dte_Tme)))              //Natural: IF CWF-MASTER.CRPRTE-CLOCK-END-DTE-TME NE CWF-WHO-WORK-REQUEST.CRPRTE-CLOCK-END-DTE-TME
                                                {
                                                    pnd_Disc_Msg_Pnd_Disc_Msg_2.reset();                                                                                  //Natural: RESET #DISC-MSG-2
                                                    pnd_Discrep_Sw.setValue("Y");                                                                                         //Natural: MOVE 'Y' TO #DISCREP-SW
                                                    pnd_Clck_Chnge_Op.nadd(1);                                                                                            //Natural: ADD 1 TO #CLCK-CHNGE-OP
                                                    pnd_Disc_Msg_Pnd_Disc_Msg_2.setValue("Corp-Clock-End-Dte-Tme difference");                                            //Natural: MOVE 'Corp-Clock-End-Dte-Tme difference' TO #DISC-MSG-2
                                                }                                                                                                                         //Natural: END-IF
                                            }                                                                                                                             //Natural: END-IF
                                        }                                                                                                                                 //Natural: END-IF
                                    }                                                                                                                                     //Natural: END-IF
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Sec_Discrep.setValue(" ");                                                                                                                        //Natural: MOVE ' ' TO #SEC-DISCREP
                    //*  CHECK FOR VALUE
                    if (condition(DbsUtil.maskMatches(cwf_Master_Crprte_Clock_End_Dte_Tme,"YYYYMMDD00-2400-6000-60N")))                                                   //Natural: IF CWF-MASTER.CRPRTE-CLOCK-END-DTE-TME = MASK ( YYYYMMDD00-2400-6000-60N )
                    {
                        //*  CHECK FOR VALUE
                        if (condition(cwf_Master_Effctve_Dte.greater(getZero())))                                                                                         //Natural: IF CWF-MASTER.EFFCTVE-DTE GT 0
                        {
                            //*  CHECK FOR VALUE
                            if (condition(cwf_Master_Trans_Dte.greater(getZero())))                                                                                       //Natural: IF CWF-MASTER.TRANS-DTE GT 0
                            {
                                pnd_Base_Dte_Tme.setValue(cwf_Master_Effctve_Dte);                                                                                        //Natural: MOVE CWF-MASTER.EFFCTVE-DTE TO #BASE-DTE-TME
                                pnd_End_Dte_Tme_D.setValue(cwf_Master_Trans_Dte);                                                                                         //Natural: MOVE CWF-MASTER.TRANS-DTE TO #END-DTE-TME-D
                                DbsUtil.callnat(Cwfn3905.class , getCurrentProcessState(), pnd_Base_Dte_Tme, pnd_End_Dte_Tme_D, pnd_Bus_Days);                            //Natural: CALLNAT 'CWFN3905' #BASE-DTE-TME #END-DTE-TME-D #BUS-DAYS
                                if (condition(Global.isEscape())) return;
                                if (condition(cwf_Who_Work_Request_Sec_Turnaround_Tme.notEquals(pnd_Bus_Days)))                                                           //Natural: IF CWF-WHO-WORK-REQUEST.SEC-TURNAROUND-TME NE #BUS-DAYS
                                {
                                    pnd_Disc_Msg_Pnd_Disc_Msg_2.reset();                                                                                                  //Natural: RESET #DISC-MSG-2
                                    pnd_Sec_Discrep.setValue("Y");                                                                                                        //Natural: MOVE 'Y' TO #SEC-DISCREP #DISCREP-SW
                                    pnd_Discrep_Sw.setValue("Y");
                                    pnd_Sec_Tatm_Chnge_Op.nadd(1);                                                                                                        //Natural: ADD 1 TO #SEC-TATM-CHNGE-OP
                                    pnd_Disc_Msg_Pnd_Disc_Msg_2.setValue("SEC Turnaround time difference");                                                               //Natural: MOVE 'SEC Turnaround time difference' TO #DISC-MSG-2
                                }                                                                                                                                         //Natural: END-IF
                                if (condition(cwf_Master_Effctve_Dte.notEquals(cwf_Who_Work_Request_Effctve_Dte)))                                                        //Natural: IF CWF-MASTER.EFFCTVE-DTE NE CWF-WHO-WORK-REQUEST.EFFCTVE-DTE
                                {
                                    pnd_Disc_Msg_Pnd_Disc_Msg_2.reset();                                                                                                  //Natural: RESET #DISC-MSG-2
                                    pnd_Sec_Discrep.setValue("Y");                                                                                                        //Natural: MOVE 'Y' TO #SEC-DISCREP #DISCREP-SW
                                    pnd_Discrep_Sw.setValue("Y");
                                    pnd_Sec_Effd_Chnge_Op.nadd(1);                                                                                                        //Natural: ADD 1 TO #SEC-EFFD-CHNGE-OP
                                    pnd_Disc_Msg_Pnd_Disc_Msg_2.setValue("SEC Turnaround time difference");                                                               //Natural: MOVE 'SEC Turnaround time difference' TO #DISC-MSG-2
                                }                                                                                                                                         //Natural: END-IF
                                if (condition(cwf_Master_Trans_Dte.notEquals(cwf_Who_Work_Request_Trans_Dte)))                                                            //Natural: IF CWF-MASTER.TRANS-DTE NE CWF-WHO-WORK-REQUEST.TRANS-DTE
                                {
                                    pnd_Disc_Msg_Pnd_Disc_Msg_2.reset();                                                                                                  //Natural: RESET #DISC-MSG-2
                                    pnd_Sec_Discrep.setValue("Y");                                                                                                        //Natural: MOVE 'Y' TO #SEC-DISCREP #DISCREP-SW
                                    pnd_Discrep_Sw.setValue("Y");
                                    pnd_Sec_Trns_Chnge_Op.nadd(1);                                                                                                        //Natural: ADD 1 TO #SEC-TRNS-CHNGE-OP
                                    pnd_Disc_Msg_Pnd_Disc_Msg_2.setValue("SEC Transaction Date difference");                                                              //Natural: MOVE 'SEC Transaction Date difference' TO #DISC-MSG-2
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(cwf_Who_Work_Request_Status_Cde.greaterOrEqual("4500") && cwf_Who_Work_Request_Status_Cde.lessOrEqual("4997")))                         //Natural: IF CWF-WHO-WORK-REQUEST.STATUS-CDE EQ '4500' THRU '4997'
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(cwf_Who_Work_Request_Status_Freeze_Ind.equals("Y")))                                                                                //Natural: IF CWF-WHO-WORK-REQUEST.STATUS-FREEZE-IND EQ 'Y'
                        {
                            if (condition(! (DbsUtil.maskMatches(cwf_Who_Work_Request_Crprte_Clock_End_Dte_Tme,"YYYYMMDD.......")) || ! (DbsUtil.maskMatches(cwf_Who_Work_Request_Crprte_Clock_End_Dte_Tme,"........00:23....."))  //Natural: IF CWF-WHO-WORK-REQUEST.CRPRTE-CLOCK-END-DTE-TME NE MASK ( YYYYMMDD....... ) OR CWF-WHO-WORK-REQUEST.CRPRTE-CLOCK-END-DTE-TME NE MASK ( ........00:23..... ) OR CWF-WHO-WORK-REQUEST.CRPRTE-CLOCK-END-DTE-TME NE MASK ( ..........00:59... ) OR CWF-WHO-WORK-REQUEST.CRPRTE-CLOCK-END-DTE-TME NE MASK ( ............000:599 )
                                || ! (DbsUtil.maskMatches(cwf_Who_Work_Request_Crprte_Clock_End_Dte_Tme,"..........00:59...")) || ! (DbsUtil.maskMatches(cwf_Who_Work_Request_Crprte_Clock_End_Dte_Tme,
                                "............000:599"))))
                            {
                                pnd_Check_Date.setValue(false);                                                                                                           //Natural: MOVE FALSE TO #CHECK-DATE
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Check_Date.setValue(true);                                                                                                            //Natural: MOVE TRUE TO #CHECK-DATE
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(pnd_Check_Date.getBoolean()))                                                                                                   //Natural: IF #CHECK-DATE
                            {
                                pnd_Crprte_Clock_End_Dte_Tme_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),cwf_Who_Work_Request_Crprte_Clock_End_Dte_Tme);       //Natural: MOVE EDITED CRPRTE-CLOCK-END-DTE-TME TO #CRPRTE-CLOCK-END-DTE-TME-T ( EM = YYYYMMDDHHIISST )
                                if (condition(pnd_Crprte_Clock_End_Dte_Tme_T.greater(ldaCwfl3019.getCwf_Who_Work_Request_View_Crprte_Due_Dte_Tme())))                     //Natural: IF #CRPRTE-CLOCK-END-DTE-TME-T GT CRPRTE-DUE-DTE-TME
                                {
                                    pnd_Late_Ind.setValue("L");                                                                                                           //Natural: ASSIGN #LATE-IND := 'L'
                                }                                                                                                                                         //Natural: ELSE
                                else if (condition())
                                {
                                    pnd_Late_Ind.setValue(" ");                                                                                                           //Natural: ASSIGN #LATE-IND := ' '
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Late_Ind.setValue("L");                                                                                                               //Natural: ASSIGN #LATE-IND := 'L'
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(Global.getTIMX().greater(ldaCwfl3019.getCwf_Who_Work_Request_View_Crprte_Due_Dte_Tme())))                                       //Natural: IF *TIMX GT CRPRTE-DUE-DTE-TME
                            {
                                pnd_Late_Ind.setValue("L");                                                                                                               //Natural: ASSIGN #LATE-IND := 'L'
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Late_Ind.setValue(" ");                                                                                                               //Natural: ASSIGN #LATE-IND := ' '
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                        //* MERGE
                        if (condition(cwf_Who_Work_Request_Crprte_On_Tme_Ind.notEquals("M")))                                                                             //Natural: IF CWF-WHO-WORK-REQUEST.CRPRTE-ON-TME-IND NE 'M'
                        {
                            if (condition(cwf_Who_Work_Request_Crprte_On_Tme_Ind.notEquals(pnd_Late_Ind)))                                                                //Natural: IF CWF-WHO-WORK-REQUEST.CRPRTE-ON-TME-IND NE #LATE-IND
                            {
                                pnd_Who_Ind.setValue(cwf_Who_Work_Request_Crprte_On_Tme_Ind);                                                                             //Natural: MOVE CWF-WHO-WORK-REQUEST.CRPRTE-ON-TME-IND TO #WHO-IND
                                cwf_Who_Work_Request_Crprte_On_Tme_Ind.setValue(pnd_Late_Ind);                                                                            //Natural: ASSIGN CWF-WHO-WORK-REQUEST.CRPRTE-ON-TME-IND := #LATE-IND
                                pnd_Disc_Msg_Pnd_Disc_Msg_2.reset();                                                                                                      //Natural: RESET #DISC-MSG-2
                                pnd_Discrep_Sw.setValue("Y");                                                                                                             //Natural: MOVE 'Y' TO #DISCREP-SW
                                pnd_Late_Chnge_Op.nadd(1);                                                                                                                //Natural: ADD 1 TO #LATE-CHNGE-OP
                                pnd_Disc_Msg_Pnd_Disc_Msg_2_2.setValue(" Late-Ind discrepancy");                                                                          //Natural: MOVE ' Late-Ind discrepancy' TO #DISC-MSG-2-2
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //* ******
                    if (condition(cwf_Who_Work_Request_Final_Close_Out_Dte_Tme.notEquals(getZero())))                                                                     //Natural: IF CWF-WHO-WORK-REQUEST.FINAL-CLOSE-OUT-DTE-TME NE 0
                    {
                        pnd_Final_Close_Out_Save.setValue(0);                                                                                                             //Natural: ASSIGN #FINAL-CLOSE-OUT-SAVE := 0
                        //*         CWF-WHO-WORK-REQUEST.FINAL-CLOSE-OUT-DTE-TME := 0
                        //*        MOVE 'Y' TO #TRIPLE-SW-FINL-CLOSE
                        pnd_Disc_Msg_Pnd_Disc_Msg_2.reset();                                                                                                              //Natural: RESET #DISC-MSG-2
                        pnd_Discrep_Sw.setValue("Y");                                                                                                                     //Natural: MOVE 'Y' TO #DISCREP-SW
                        pnd_Set_To_Zero_Op.nadd(1);                                                                                                                       //Natural: ADD 1 TO #SET-TO-ZERO-OP
                        pnd_Disc_Msg_Pnd_Disc_Msg_2_1.setValue("Final-Clse-Out-Dte-Tm");                                                                                  //Natural: MOVE 'Final-Clse-Out-Dte-Tm' TO #DISC-MSG-2-1
                        pnd_Disc_Msg_Pnd_Disc_Msg_2_2.setValue(" Set to a Zeros       ");                                                                                 //Natural: MOVE ' Set to a Zeros       ' TO #DISC-MSG-2-2
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(cwf_Who_Work_Request_Status_Cde.greaterOrEqual("4500") && cwf_Who_Work_Request_Status_Cde.lessOrEqual("4997")))                         //Natural: IF CWF-WHO-WORK-REQUEST.STATUS-CDE GE '4500' AND CWF-WHO-WORK-REQUEST.STATUS-CDE LE '4997'
                    {
                        pnd_Crprte_Clock_Dte_Save.setValue("999999999999999");                                                                                            //Natural: ASSIGN #CRPRTE-CLOCK-DTE-SAVE := '999999999999999'
                        //*         CRPRTE-CLOCK-END-DTE-TME := '999999999999999'
                        //*         MOVE 'Y' TO #TRIPLE-SW-CORP-CLOCK
                        pnd_Disc_Msg_Pnd_Disc_Msg_2.reset();                                                                                                              //Natural: RESET #DISC-MSG-2
                        pnd_Discrep_Sw.setValue("Y");                                                                                                                     //Natural: MOVE 'Y' TO #DISCREP-SW
                        pnd_Set_To_Nine_Op.nadd(1);                                                                                                                       //Natural: ADD 1 TO #SET-TO-NINE-OP
                        pnd_Disc_Msg_Pnd_Disc_Msg_2_1.setValue("External Pend Case - ");                                                                                  //Natural: MOVE 'External Pend Case - ' TO #DISC-MSG-2-1
                        pnd_Disc_Msg_Pnd_Disc_Msg_2_2.setValue("Crprte-Clock Set to 9s");                                                                                 //Natural: MOVE 'Crprte-Clock Set to 9s' TO #DISC-MSG-2-2
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  F1.
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Discrep_Sw.equals("Y")))                                                                                                                    //Natural: IF #DISCREP-SW = 'Y'
            {
                pnd_Disc_Msg_Pnd_Disc_Msg_1.setValue("Record will be Updated because of ");                                                                               //Natural: MOVE 'Record will be Updated because of ' TO #DISC-MSG-1
                getReports().write(0, ReportOption.NOTITLE,NEWLINE,"Not eq.LDT,PIN,WPID,TIAA-dt, ==>",pnd_Rqst_Log_Dte_Tme_Save,cwf_Master_Pin_Nbr,cwf_Master_Work_Prcss_Id, //Natural: WRITE ( 0 ) / 'Not eq.LDT,PIN,WPID,TIAA-dt, ==>' #RQST-LOG-DTE-TME-SAVE CWF-MASTER.PIN-NBR CWF-MASTER.WORK-PRCSS-ID CWF-MASTER.TIAA-RCVD-DTE / #DISC-MSG
                    cwf_Master_Tiaa_Rcvd_Dte,NEWLINE,pnd_Disc_Msg);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM WORK-RQST-UPDATE
                sub_Work_Rqst_Update();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  R1.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  (2195)
    }
    private void sub_Select_Master_Records_Close() throws Exception                                                                                                       //Natural: SELECT-MASTER-RECORDS-CLOSE
    {
        if (BLNatReinput.isReinput()) return;

        vw_cwf_Master.startDatabaseRead                                                                                                                                   //Natural: READ CWF-MASTER BY CRPRTE-STATUS-CHNGE-DTE-KEY STARTING FROM #BEGIN-KEY
        (
        "R2",
        new Wc[] { new Wc("CRPRTE_STATUS_CHNGE_DTE_KEY", ">=", pnd_Begin_Key, WcType.BY) },
        new Oc[] { new Oc("CRPRTE_STATUS_CHNGE_DTE_KEY", "ASC") }
        );
        R2:
        while (condition(vw_cwf_Master.readNextRow("R2")))
        {
            if (condition(cwf_Master_Last_Chnge_Invrt_Dte_Tme.greater(pnd_End_Key_Pnd_E_Last_Chnge_Invrt_Dte_Tme)))                                                       //Natural: IF CWF-MASTER.LAST-CHNGE-INVRT-DTE-TME GT #E-LAST-CHNGE-INVRT-DTE-TME
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cwf_Master_Pin_Nbr.equals(getZero())))                                                                                                          //Natural: REJECT IF PIN-NBR = 0
            {
                continue;
            }
            pnd_Mit_Counter_C.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #MIT-COUNTER-C
            pnd_Pin_Nbr_Save.setValue(cwf_Master_Pin_Nbr);                                                                                                                //Natural: MOVE CWF-MASTER.PIN-NBR TO #PIN-NBR-SAVE
            pnd_Wpid_Save.setValue(cwf_Master_Work_Prcss_Id);                                                                                                             //Natural: MOVE CWF-MASTER.WORK-PRCSS-ID TO #WPID-SAVE
            pnd_Tiaa_Rcvd_Dte_Save.setValue(cwf_Master_Tiaa_Rcvd_Dte);                                                                                                    //Natural: MOVE CWF-MASTER.TIAA-RCVD-DTE TO #TIAA-RCVD-DTE-SAVE
            pnd_Rqst_Id_Save.setValue(cwf_Master_Rqst_Id);                                                                                                                //Natural: MOVE CWF-MASTER.RQST-ID TO #RQST-ID-SAVE
            pnd_Rlte_Rqst_Id_Save.setValue(cwf_Master_Rlte_Rqst_Id);                                                                                                      //Natural: MOVE CWF-MASTER.RLTE-RQST-ID TO #RLTE-RQST-ID-SAVE
            pnd_Crprte_Stat_Ind_Save.setValue(cwf_Master_Crprte_Status_Ind);                                                                                              //Natural: MOVE CWF-MASTER.CRPRTE-STATUS-IND TO #CRPRTE-STAT-IND-SAVE
            pnd_Mit_Status_Cde_Save.setValue(cwf_Master_Status_Cde);                                                                                                      //Natural: MOVE CWF-MASTER.STATUS-CDE TO #MIT-STATUS-CDE-SAVE
            pnd_Final_Close_Out_Save.setValue(cwf_Master_Final_Close_Out_Dte_Tme);                                                                                        //Natural: MOVE CWF-MASTER.FINAL-CLOSE-OUT-DTE-TME TO #FINAL-CLOSE-OUT-SAVE
            pnd_Crprte_Clock_Dte_Save.setValue(cwf_Master_Crprte_Clock_End_Dte_Tme);                                                                                      //Natural: MOVE CWF-MASTER.CRPRTE-CLOCK-END-DTE-TME TO #CRPRTE-CLOCK-DTE-SAVE
            pnd_Mit_Status_Cde_Save.setValue(cwf_Master_Status_Cde);                                                                                                      //Natural: MOVE CWF-MASTER.STATUS-CDE TO #MIT-STATUS-CDE-SAVE
            pnd_Rqst_Log_Dte_Tme_Save.setValue(cwf_Master_Rqst_Log_Dte_Tme);                                                                                              //Natural: MOVE CWF-MASTER.RQST-LOG-DTE-TME TO #RQST-LOG-DTE-TME-SAVE
            pnd_Last_Chnge_Dte_Tme_Save.setValue(cwf_Master_Last_Chnge_Dte_Tme);                                                                                          //Natural: MOVE CWF-MASTER.LAST-CHNGE-DTE-TME TO #LAST-CHNGE-DTE-TME-SAVE
            pnd_Found_Sw.setValue("F");                                                                                                                                   //Natural: MOVE 'F' TO #FOUND-SW
            vw_cwf_Who_Work_Request.startDatabaseFind                                                                                                                     //Natural: FIND CWF-WHO-WORK-REQUEST WITH RQST-LOG-DTE-TME = #RQST-LOG-DTE-TME-SAVE
            (
            "F2",
            new Wc[] { new Wc("RQST_LOG_DTE_TME", "=", pnd_Rqst_Log_Dte_Tme_Save, WcType.WITH) }
            );
            F2:
            while (condition(vw_cwf_Who_Work_Request.readNextRow("F2", true)))
            {
                vw_cwf_Who_Work_Request.setIfNotFoundControlFlag(false);
                if (condition(vw_cwf_Who_Work_Request.getAstCOUNTER().equals(0)))                                                                                         //Natural: IF NO RECORD FOUND
                {
                    pnd_Nfd_Counter.nadd(1);                                                                                                                              //Natural: ADD 1 TO #NFD-COUNTER
                    getReports().write(0, ReportOption.NOTITLE,NEWLINE,"This Clse rec ntfnd on WHO RLDT,PIN,LCDT==>",pnd_Rqst_Log_Dte_Tme_Save,pnd_Pin_Nbr_Save,          //Natural: WRITE ( 0 ) / 'This Clse rec ntfnd on WHO RLDT,PIN,LCDT==>' #RQST-LOG-DTE-TME-SAVE #PIN-NBR-SAVE #LAST-CHNGE-DTE-TME-SAVE
                        pnd_Last_Chnge_Dte_Tme_Save);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("F2"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("F2"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Found_Sw.setValue("C");                                                                                                                           //Natural: MOVE 'C' TO #FOUND-SW
                    if (true) break F2;                                                                                                                                   //Natural: ESCAPE BOTTOM ( F2. ) IMMEDIATE
                }                                                                                                                                                         //Natural: END-NOREC
                //*  ---------------------------------------
                //*  IF MIT WORK REQUEST HAS A "9" AND STATUS OF THE WHO WORK REQUEST
                //*  RECORD IS OUT OF THE "Closure" RANGE THEN CALL CWFN3022 SUB-PROGRAM
                //*  TO CORRECT IT AND CREATE CLOSE ACTIVITY
                if (condition(cwf_Who_Work_Request_Status_Cde.less("7000")))                                                                                              //Natural: IF CWF-WHO-WORK-REQUEST.STATUS-CDE LT '7000'
                {
                    DbsUtil.callnat(Cwfn3022.class , getCurrentProcessState(), pnd_Rqst_Log_Dte_Tme_Save, pnd_Date_T);                                                    //Natural: CALLNAT 'CWFN3022' #RQST-LOG-DTE-TME-SAVE #DATE-T
                    if (condition(Global.isEscape())) return;
                    pnd_Cor_Stat_Cntr.nadd(1);                                                                                                                            //Natural: ADD 1 TO #COR-STAT-CNTR
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"CWFN3022 - RLDT of the record to be corrected = ",pnd_Rqst_Log_Dte_Tme_Save);                     //Natural: WRITE ( 1 ) /'CWFN3022 - RLDT of the record to be corrected = ' #RQST-LOG-DTE-TME-SAVE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("F2"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("F2"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  ----------------------------------------
                //*  COMPARE FIELDS - IF NOT EQUAL UPDATE WHO WORK REQUEST RECORD
                //*  ------------------------------------------------------------
                pnd_Discrep_Sw.reset();                                                                                                                                   //Natural: RESET #DISCREP-SW #DISC-MSG
                pnd_Disc_Msg.reset();
                if (condition(pnd_Found_Sw.equals("F")))                                                                                                                  //Natural: IF #FOUND-SW = 'F'
                {
                    pnd_Isn.setValue(vw_cwf_Who_Work_Request.getAstISN("F2"));                                                                                            //Natural: ASSIGN #ISN := *ISN ( F2. )
                    if (condition(cwf_Master_Work_Prcss_Id.notEquals(cwf_Who_Work_Request_Work_Prcss_Id)))                                                                //Natural: IF CWF-MASTER.WORK-PRCSS-ID NE CWF-WHO-WORK-REQUEST.WORK-PRCSS-ID
                    {
                        pnd_Disc_Msg_Pnd_Disc_Msg_2.reset();                                                                                                              //Natural: RESET #DISC-MSG-2
                        pnd_Discrep_Sw.setValue("Y");                                                                                                                     //Natural: MOVE 'Y' TO #DISCREP-SW
                        pnd_Wpid_Chnge_Cl.nadd(1);                                                                                                                        //Natural: ADD 1 TO #WPID-CHNGE-CL
                        pnd_Disc_Msg_Pnd_Disc_Msg_2.setValue("Work Process ID difference");                                                                               //Natural: MOVE 'Work Process ID difference' TO #DISC-MSG-2
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(cwf_Master_Pin_Nbr.notEquals(cwf_Who_Work_Request_Pin_Nbr)))                                                                        //Natural: IF CWF-MASTER.PIN-NBR NE CWF-WHO-WORK-REQUEST.PIN-NBR
                        {
                            pnd_Disc_Msg_Pnd_Disc_Msg_2.reset();                                                                                                          //Natural: RESET #DISC-MSG-2
                            pnd_Discrep_Sw.setValue("Y");                                                                                                                 //Natural: MOVE 'Y' TO #DISCREP-SW
                            pnd_Pin_Chnge_Cl.nadd(1);                                                                                                                     //Natural: ADD 1 TO #PIN-CHNGE-CL
                            pnd_Disc_Msg_Pnd_Disc_Msg_2.setValue("PIN Number difference");                                                                                //Natural: MOVE 'PIN Number difference' TO #DISC-MSG-2
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(cwf_Master_Tiaa_Rcvd_Dte.notEquals(cwf_Who_Work_Request_Tiaa_Rcvd_Dte)))                                                        //Natural: IF CWF-MASTER.TIAA-RCVD-DTE NE CWF-WHO-WORK-REQUEST.TIAA-RCVD-DTE
                            {
                                pnd_Disc_Msg_Pnd_Disc_Msg_2.reset();                                                                                                      //Natural: RESET #DISC-MSG-2
                                pnd_Discrep_Sw.setValue("Y");                                                                                                             //Natural: MOVE 'Y' TO #DISCREP-SW
                                pnd_Tiaa_Chnge_Cl.nadd(1);                                                                                                                //Natural: ADD 1 TO #TIAA-CHNGE-CL
                                pnd_Disc_Msg_Pnd_Disc_Msg_2.setValue("TIAA Received Date difference");                                                                    //Natural: MOVE 'TIAA Received Date difference' TO #DISC-MSG-2
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                if (condition(cwf_Master_Rqst_Id.notEquals(cwf_Who_Work_Request_Rqst_Id)))                                                                //Natural: IF CWF-MASTER.RQST-ID NE CWF-WHO-WORK-REQUEST.RQST-ID
                                {
                                    pnd_Disc_Msg_Pnd_Disc_Msg_2.reset();                                                                                                  //Natural: RESET #DISC-MSG-2
                                    pnd_Discrep_Sw.setValue("Y");                                                                                                         //Natural: MOVE 'Y' TO #DISCREP-SW
                                    pnd_Rqid_Chnge_Cl.nadd(1);                                                                                                            //Natural: ADD 1 TO #RQID-CHNGE-CL
                                    pnd_Disc_Msg_Pnd_Disc_Msg_2.setValue("Request ID difference");                                                                        //Natural: MOVE 'Request ID difference' TO #DISC-MSG-2
                                }                                                                                                                                         //Natural: ELSE
                                else if (condition())
                                {
                                    if (condition(cwf_Master_Rlte_Rqst_Id.notEquals(cwf_Who_Work_Request_Rlte_Rqst_Id)))                                                  //Natural: IF CWF-MASTER.RLTE-RQST-ID NE CWF-WHO-WORK-REQUEST.RLTE-RQST-ID
                                    {
                                        pnd_Disc_Msg_Pnd_Disc_Msg_2.reset();                                                                                              //Natural: RESET #DISC-MSG-2
                                        pnd_Discrep_Sw.setValue("Y");                                                                                                     //Natural: MOVE 'Y' TO #DISCREP-SW
                                        pnd_Rlte_Chnge_Cl.nadd(1);                                                                                                        //Natural: ADD 1 TO #RLTE-CHNGE-CL
                                        pnd_Disc_Msg_Pnd_Disc_Msg_2.setValue("Relate Request ID difference");                                                             //Natural: MOVE 'Relate Request ID difference' TO #DISC-MSG-2
                                    }                                                                                                                                     //Natural: ELSE
                                    else if (condition())
                                    {
                                        if (condition(cwf_Master_Crprte_Status_Ind.notEquals(cwf_Who_Work_Request_Crprte_Status_Ind)))                                    //Natural: IF CWF-MASTER.CRPRTE-STATUS-IND NE CWF-WHO-WORK-REQUEST.CRPRTE-STATUS-IND
                                        {
                                            pnd_Disc_Msg_Pnd_Disc_Msg_2.reset();                                                                                          //Natural: RESET #DISC-MSG-2
                                            pnd_Discrep_Sw.setValue("Y");                                                                                                 //Natural: MOVE 'Y' TO #DISCREP-SW
                                            pnd_Stat_Chnge_Cl.nadd(1);                                                                                                    //Natural: ADD 1 TO #STAT-CHNGE-CL
                                            pnd_Disc_Msg_Pnd_Disc_Msg_2.setValue("Corp-Stat-Ind ID difference");                                                          //Natural: MOVE 'Corp-Stat-Ind ID difference' TO #DISC-MSG-2
                                        }                                                                                                                                 //Natural: ELSE
                                        else if (condition())
                                        {
                                            if (condition(cwf_Master_Final_Close_Out_Dte_Tme.notEquals(cwf_Who_Work_Request_Final_Close_Out_Dte_Tme)))                    //Natural: IF CWF-MASTER.FINAL-CLOSE-OUT-DTE-TME NE CWF-WHO-WORK-REQUEST.FINAL-CLOSE-OUT-DTE-TME
                                            {
                                                pnd_Disc_Msg_Pnd_Disc_Msg_2.reset();                                                                                      //Natural: RESET #DISC-MSG-2
                                                pnd_Discrep_Sw.setValue("Y");                                                                                             //Natural: MOVE 'Y' TO #DISCREP-SW
                                                pnd_Fncl_Chnge_Cl.nadd(1);                                                                                                //Natural: ADD 1 TO #FNCL-CHNGE-CL
                                                pnd_Disc_Msg_Pnd_Disc_Msg_2.setValue("Final-Clse-Out-dte-Tme difference");                                                //Natural: MOVE 'Final-Clse-Out-dte-Tme difference' TO #DISC-MSG-2
                                            }                                                                                                                             //Natural: ELSE
                                            else if (condition())
                                            {
                                                if (condition(cwf_Master_Crprte_Clock_End_Dte_Tme.notEquals(cwf_Who_Work_Request_Crprte_Clock_End_Dte_Tme)))              //Natural: IF CWF-MASTER.CRPRTE-CLOCK-END-DTE-TME NE CWF-WHO-WORK-REQUEST.CRPRTE-CLOCK-END-DTE-TME
                                                {
                                                    pnd_Disc_Msg_Pnd_Disc_Msg_2.reset();                                                                                  //Natural: RESET #DISC-MSG-2
                                                    pnd_Discrep_Sw.setValue("Y");                                                                                         //Natural: MOVE 'Y' TO #DISCREP-SW
                                                    pnd_Clck_Chnge_Cl.nadd(1);                                                                                            //Natural: ADD 1 TO #CLCK-CHNGE-CL
                                                    pnd_Disc_Msg_Pnd_Disc_Msg_2.setValue("Corp-Clock-End-Dte-Tme difference");                                            //Natural: MOVE 'Corp-Clock-End-Dte-Tme difference' TO #DISC-MSG-2
                                                }                                                                                                                         //Natural: END-IF
                                            }                                                                                                                             //Natural: END-IF
                                        }                                                                                                                                 //Natural: END-IF
                                    }                                                                                                                                     //Natural: END-IF
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Sec_Discrep.setValue(" ");                                                                                                                        //Natural: MOVE ' ' TO #SEC-DISCREP
                    //*  CHECK FOR VALUE
                    if (condition(DbsUtil.maskMatches(cwf_Master_Crprte_Clock_End_Dte_Tme,"YYYYMMDD00-2400-6000-60N")))                                                   //Natural: IF CWF-MASTER.CRPRTE-CLOCK-END-DTE-TME = MASK ( YYYYMMDD00-2400-6000-60N )
                    {
                        //*  CHECK FOR VALUE
                        if (condition(cwf_Master_Effctve_Dte.greater(getZero())))                                                                                         //Natural: IF CWF-MASTER.EFFCTVE-DTE GT 0
                        {
                            //*  CHECK FOR VALUE
                            if (condition(cwf_Master_Trans_Dte.greater(getZero())))                                                                                       //Natural: IF CWF-MASTER.TRANS-DTE GT 0
                            {
                                pnd_Base_Dte_Tme.setValue(cwf_Master_Effctve_Dte);                                                                                        //Natural: MOVE CWF-MASTER.EFFCTVE-DTE TO #BASE-DTE-TME
                                pnd_End_Dte_Tme_D.setValue(cwf_Master_Trans_Dte);                                                                                         //Natural: MOVE CWF-MASTER.TRANS-DTE TO #END-DTE-TME-D
                                DbsUtil.callnat(Cwfn3905.class , getCurrentProcessState(), pnd_Base_Dte_Tme, pnd_End_Dte_Tme_D, pnd_Bus_Days);                            //Natural: CALLNAT 'CWFN3905' #BASE-DTE-TME #END-DTE-TME-D #BUS-DAYS
                                if (condition(Global.isEscape())) return;
                                if (condition(cwf_Who_Work_Request_Sec_Turnaround_Tme.notEquals(pnd_Bus_Days)))                                                           //Natural: IF CWF-WHO-WORK-REQUEST.SEC-TURNAROUND-TME NE #BUS-DAYS
                                {
                                    pnd_Disc_Msg_Pnd_Disc_Msg_2.reset();                                                                                                  //Natural: RESET #DISC-MSG-2
                                    pnd_Sec_Discrep.setValue("Y");                                                                                                        //Natural: MOVE 'Y' TO #SEC-DISCREP #DISCREP-SW
                                    pnd_Discrep_Sw.setValue("Y");
                                    pnd_Sec_Tatm_Chnge_Cl.nadd(1);                                                                                                        //Natural: ADD 1 TO #SEC-TATM-CHNGE-CL
                                    pnd_Disc_Msg_Pnd_Disc_Msg_2.setValue("SEC Turnaround time difference");                                                               //Natural: MOVE 'SEC Turnaround time difference' TO #DISC-MSG-2
                                }                                                                                                                                         //Natural: END-IF
                                if (condition(cwf_Master_Effctve_Dte.notEquals(cwf_Who_Work_Request_Effctve_Dte)))                                                        //Natural: IF CWF-MASTER.EFFCTVE-DTE NE CWF-WHO-WORK-REQUEST.EFFCTVE-DTE
                                {
                                    pnd_Disc_Msg_Pnd_Disc_Msg_2.reset();                                                                                                  //Natural: RESET #DISC-MSG-2
                                    pnd_Sec_Discrep.setValue("Y");                                                                                                        //Natural: MOVE 'Y' TO #SEC-DISCREP #DISCREP-SW
                                    pnd_Discrep_Sw.setValue("Y");
                                    pnd_Sec_Effd_Chnge_Cl.nadd(1);                                                                                                        //Natural: ADD 1 TO #SEC-EFFD-CHNGE-CL
                                    pnd_Disc_Msg_Pnd_Disc_Msg_2.setValue("SEC Turnaround time difference");                                                               //Natural: MOVE 'SEC Turnaround time difference' TO #DISC-MSG-2
                                }                                                                                                                                         //Natural: END-IF
                                if (condition(cwf_Master_Trans_Dte.notEquals(cwf_Who_Work_Request_Trans_Dte)))                                                            //Natural: IF CWF-MASTER.TRANS-DTE NE CWF-WHO-WORK-REQUEST.TRANS-DTE
                                {
                                    pnd_Disc_Msg_Pnd_Disc_Msg_2.reset();                                                                                                  //Natural: RESET #DISC-MSG-2
                                    pnd_Sec_Discrep.setValue("Y");                                                                                                        //Natural: MOVE 'Y' TO #SEC-DISCREP #DISCREP-SW
                                    pnd_Discrep_Sw.setValue("Y");
                                    pnd_Sec_Trns_Chnge_Cl.nadd(1);                                                                                                        //Natural: ADD 1 TO #SEC-TRNS-CHNGE-CL
                                    pnd_Disc_Msg_Pnd_Disc_Msg_2.setValue("SEC Transaction Date difference");                                                              //Natural: MOVE 'SEC Transaction Date difference' TO #DISC-MSG-2
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(cwf_Who_Work_Request_Status_Cde.greaterOrEqual("4500") && cwf_Who_Work_Request_Status_Cde.lessOrEqual("4997")))                         //Natural: IF CWF-WHO-WORK-REQUEST.STATUS-CDE EQ '4500' THRU '4997'
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(cwf_Who_Work_Request_Status_Freeze_Ind.equals("Y")))                                                                                //Natural: IF CWF-WHO-WORK-REQUEST.STATUS-FREEZE-IND EQ 'Y'
                        {
                            if (condition(! (DbsUtil.maskMatches(cwf_Who_Work_Request_Crprte_Clock_End_Dte_Tme,"YYYYMMDD.......")) || ! (DbsUtil.maskMatches(cwf_Who_Work_Request_Crprte_Clock_End_Dte_Tme,"........00:23....."))  //Natural: IF CWF-WHO-WORK-REQUEST.CRPRTE-CLOCK-END-DTE-TME NE MASK ( YYYYMMDD....... ) OR CWF-WHO-WORK-REQUEST.CRPRTE-CLOCK-END-DTE-TME NE MASK ( ........00:23..... ) OR CWF-WHO-WORK-REQUEST.CRPRTE-CLOCK-END-DTE-TME NE MASK ( ..........00:59... ) OR CWF-WHO-WORK-REQUEST.CRPRTE-CLOCK-END-DTE-TME NE MASK ( ............000:599 )
                                || ! (DbsUtil.maskMatches(cwf_Who_Work_Request_Crprte_Clock_End_Dte_Tme,"..........00:59...")) || ! (DbsUtil.maskMatches(cwf_Who_Work_Request_Crprte_Clock_End_Dte_Tme,
                                "............000:599"))))
                            {
                                pnd_Check_Date.setValue(false);                                                                                                           //Natural: MOVE FALSE TO #CHECK-DATE
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Check_Date.setValue(true);                                                                                                            //Natural: MOVE TRUE TO #CHECK-DATE
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(pnd_Check_Date.getBoolean()))                                                                                                   //Natural: IF #CHECK-DATE
                            {
                                pnd_Crprte_Clock_End_Dte_Tme_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),cwf_Who_Work_Request_Crprte_Clock_End_Dte_Tme);       //Natural: MOVE EDITED CRPRTE-CLOCK-END-DTE-TME TO #CRPRTE-CLOCK-END-DTE-TME-T ( EM = YYYYMMDDHHIISST )
                                if (condition(pnd_Crprte_Clock_End_Dte_Tme_T.greater(ldaCwfl3019.getCwf_Who_Work_Request_View_Crprte_Due_Dte_Tme())))                     //Natural: IF #CRPRTE-CLOCK-END-DTE-TME-T GT CRPRTE-DUE-DTE-TME
                                {
                                    pnd_Late_Ind.setValue("L");                                                                                                           //Natural: ASSIGN #LATE-IND := 'L'
                                }                                                                                                                                         //Natural: ELSE
                                else if (condition())
                                {
                                    pnd_Late_Ind.setValue(" ");                                                                                                           //Natural: ASSIGN #LATE-IND := ' '
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Late_Ind.setValue("L");                                                                                                               //Natural: ASSIGN #LATE-IND := 'L'
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(Global.getTIMX().greater(ldaCwfl3019.getCwf_Who_Work_Request_View_Crprte_Due_Dte_Tme())))                                       //Natural: IF *TIMX GT CRPRTE-DUE-DTE-TME
                            {
                                pnd_Late_Ind.setValue("L");                                                                                                               //Natural: ASSIGN #LATE-IND := 'L'
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Late_Ind.setValue(" ");                                                                                                               //Natural: ASSIGN #LATE-IND := ' '
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                        //* MERGE
                        if (condition(cwf_Who_Work_Request_Crprte_On_Tme_Ind.notEquals("M")))                                                                             //Natural: IF CWF-WHO-WORK-REQUEST.CRPRTE-ON-TME-IND NE 'M'
                        {
                            if (condition(cwf_Who_Work_Request_Crprte_On_Tme_Ind.notEquals(pnd_Late_Ind)))                                                                //Natural: IF CWF-WHO-WORK-REQUEST.CRPRTE-ON-TME-IND NE #LATE-IND
                            {
                                cwf_Who_Work_Request_Crprte_On_Tme_Ind.setValue(pnd_Late_Ind);                                                                            //Natural: ASSIGN CWF-WHO-WORK-REQUEST.CRPRTE-ON-TME-IND := #LATE-IND
                                pnd_Disc_Msg_Pnd_Disc_Msg_2.reset();                                                                                                      //Natural: RESET #DISC-MSG-2
                                pnd_Discrep_Sw.setValue("Y");                                                                                                             //Natural: MOVE 'Y' TO #DISCREP-SW
                                pnd_Late_Chnge_Cl.nadd(1);                                                                                                                //Natural: ADD 1 TO #LATE-CHNGE-CL
                                pnd_Disc_Msg_Pnd_Disc_Msg_2.setValue("Corp-On-Time-Ind difference");                                                                      //Natural: MOVE 'Corp-On-Time-Ind difference' TO #DISC-MSG-2
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  F2.
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R2"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Discrep_Sw.equals("Y")))                                                                                                                    //Natural: IF #DISCREP-SW = 'Y'
            {
                pnd_Disc_Msg_Pnd_Disc_Msg_1.setValue("Record will be Updated because of ");                                                                               //Natural: MOVE 'Record will be Updated because of ' TO #DISC-MSG-1
                getReports().write(0, ReportOption.NOTITLE,NEWLINE,"Not eq.LDT,PIN,WPID,TIAA-dt, ==>",pnd_Rqst_Log_Dte_Tme_Save,cwf_Master_Pin_Nbr,cwf_Master_Work_Prcss_Id, //Natural: WRITE ( 0 ) / 'Not eq.LDT,PIN,WPID,TIAA-dt, ==>' #RQST-LOG-DTE-TME-SAVE CWF-MASTER.PIN-NBR CWF-MASTER.WORK-PRCSS-ID CWF-MASTER.TIAA-RCVD-DTE / #DISC-MSG
                    cwf_Master_Tiaa_Rcvd_Dte,NEWLINE,pnd_Disc_Msg);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM WORK-RQST-UPDATE
                sub_Work_Rqst_Update();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  R2.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Write_Totals() throws Exception                                                                                                                      //Natural: WRITE-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,"******* Sweep is done for the following window *******",NEWLINE,"Begining period    = ",pnd_Display_Strt_Period,NEWLINE,"Ending   period    = ",pnd_Display_Stop_Period,NEWLINE,"***************************************************",NEWLINE,"Number of Open Records which was Logged on MIT   = ",pnd_Mit_Counter_O,NEWLINE,"Number of Clse Records which was Logged on MIT   = ",pnd_Mit_Counter_C,NEWLINE,"Number of Work Request Not Found on WHO file     = ",pnd_Nfd_Counter,NEWLINE,"Number of Work Request Records Archived by PGM   = ",pnd_Now_Counter,NEWLINE,"Number of Archived Records Corrected by PGM      = ",pnd_Cor_Counter,NEWLINE,"------- Update Statistics for Open cases --------- ",NEWLINE,"Number of WPID discrepancies                     = ",pnd_Wpid_Chnge_Op,NEWLINE,"Number of PIN  discrepancies                     = ",pnd_Pin_Chnge_Op,NEWLINE,"Number of TIAA Received Date discrepancies       = ",pnd_Tiaa_Chnge_Op,NEWLINE,"Number of Request ID discrepancies               = ",pnd_Rqid_Chnge_Op,NEWLINE,"Number of Relate Request ID discrepancies        = ",pnd_Rlte_Chnge_Op,NEWLINE,"Number of Corp-Status-Ind discrepancies          = ",pnd_Stat_Chnge_Op,NEWLINE,"Number of Final-Close-Out-Dte-Tme discrepancies  = ",pnd_Fncl_Chnge_Op,NEWLINE,"Number of Corp-Clock-End-Dte-Tme discrepancies   = ",pnd_Clck_Chnge_Op,NEWLINE,"Number of SEC Turnaround Time discrepancies      = ",pnd_Sec_Tatm_Chnge_Op,NEWLINE,"Number of SEC Effective Date  discrepancies      = ",pnd_Sec_Effd_Chnge_Op,NEWLINE,"Number of SEC Transaction Date discrepancies     = ",pnd_Sec_Trns_Chnge_Op,NEWLINE,"Number of Late Indicator discrepancies           = ",pnd_Late_Chnge_Op,NEWLINE,"Number of Final-Clse-Out-Dte-Tme Set to a Zeroes = ",pnd_Set_To_Zero_Op,NEWLINE,"Number of Crprte-Clock Set to 9s                 = ",pnd_Set_To_Nine_Op,NEWLINE,"------- Update Statistics for Close cases -------- ",NEWLINE,"Number of WPID discrepancies                     = ",pnd_Wpid_Chnge_Cl,NEWLINE,"Number of PIN  discrepancies                     = ",pnd_Pin_Chnge_Cl,NEWLINE,"Number of TIAA Received Date discrepancies       = ",pnd_Tiaa_Chnge_Cl,NEWLINE,"Number of Request ID discrepancies               = ",pnd_Rqid_Chnge_Cl,NEWLINE,"Number of Relate Request ID discrepancies        = ",pnd_Rlte_Chnge_Cl,NEWLINE,"Number of Corp-Status-Ind discrepancies          = ",pnd_Stat_Chnge_Cl,NEWLINE,"Number of Final-Close-Out-Dte-Tme discrepancies  = ",pnd_Fncl_Chnge_Cl,NEWLINE,"Number of Corp-Clock-End-Dte-Tme discrepancies   = ",pnd_Clck_Chnge_Cl,NEWLINE,"Number of SEC Turnaround Time discrepancies      = ",pnd_Sec_Tatm_Chnge_Cl,NEWLINE,"Number of SEC Effective Date  discrepancies      = ",pnd_Sec_Effd_Chnge_Cl,NEWLINE,"Number of SEC Transaction Date discrepancies     = ",pnd_Sec_Trns_Chnge_Cl,NEWLINE,"Number of Late Indicator discrepancies           = ",pnd_Late_Chnge_Cl,NEWLINE,"Number of Records to be Corrected (Status Range) = ",pnd_Cor_Stat_Cntr,NEWLINE,"***************************************************",NEWLINE,"ELAPSED TIME TO READ RECORDS","(HH:MM:SS.T) :",st,  //Natural: WRITE ( 0 ) / '******* Sweep is done for the following window *******' / 'Begining period    = ' #DISPLAY-STRT-PERIOD / 'Ending   period    = ' #DISPLAY-STOP-PERIOD / '***************************************************' / 'Number of Open Records which was Logged on MIT   = ' #MIT-COUNTER-O / 'Number of Clse Records which was Logged on MIT   = ' #MIT-COUNTER-C / 'Number of Work Request Not Found on WHO file     = ' #NFD-COUNTER / 'Number of Work Request Records Archived by PGM   = ' #NOW-COUNTER / 'Number of Archived Records Corrected by PGM      = ' #COR-COUNTER / '------- Update Statistics for Open cases --------- ' / 'Number of WPID discrepancies                     = ' #WPID-CHNGE-OP / 'Number of PIN  discrepancies                     = ' #PIN-CHNGE-OP / 'Number of TIAA Received Date discrepancies       = ' #TIAA-CHNGE-OP / 'Number of Request ID discrepancies               = ' #RQID-CHNGE-OP / 'Number of Relate Request ID discrepancies        = ' #RLTE-CHNGE-OP / 'Number of Corp-Status-Ind discrepancies          = ' #STAT-CHNGE-OP / 'Number of Final-Close-Out-Dte-Tme discrepancies  = ' #FNCL-CHNGE-OP / 'Number of Corp-Clock-End-Dte-Tme discrepancies   = ' #CLCK-CHNGE-OP / 'Number of SEC Turnaround Time discrepancies      = ' #SEC-TATM-CHNGE-OP / 'Number of SEC Effective Date  discrepancies      = ' #SEC-EFFD-CHNGE-OP / 'Number of SEC Transaction Date discrepancies     = ' #SEC-TRNS-CHNGE-OP / 'Number of Late Indicator discrepancies           = ' #LATE-CHNGE-OP / 'Number of Final-Clse-Out-Dte-Tme Set to a Zeroes = ' #SET-TO-ZERO-OP / 'Number of Crprte-Clock Set to 9s                 = ' #SET-TO-NINE-OP / '------- Update Statistics for Close cases -------- ' / 'Number of WPID discrepancies                     = ' #WPID-CHNGE-CL / 'Number of PIN  discrepancies                     = ' #PIN-CHNGE-CL / 'Number of TIAA Received Date discrepancies       = ' #TIAA-CHNGE-CL / 'Number of Request ID discrepancies               = ' #RQID-CHNGE-CL / 'Number of Relate Request ID discrepancies        = ' #RLTE-CHNGE-CL / 'Number of Corp-Status-Ind discrepancies          = ' #STAT-CHNGE-CL / 'Number of Final-Close-Out-Dte-Tme discrepancies  = ' #FNCL-CHNGE-CL / 'Number of Corp-Clock-End-Dte-Tme discrepancies   = ' #CLCK-CHNGE-CL / 'Number of SEC Turnaround Time discrepancies      = ' #SEC-TATM-CHNGE-CL / 'Number of SEC Effective Date  discrepancies      = ' #SEC-EFFD-CHNGE-CL / 'Number of SEC Transaction Date discrepancies     = ' #SEC-TRNS-CHNGE-CL / 'Number of Late Indicator discrepancies           = ' #LATE-CHNGE-CL / 'Number of Records to be Corrected (Status Range) = ' #COR-STAT-CNTR / '***************************************************' / 'ELAPSED TIME TO READ RECORDS' '(HH:MM:SS.T) :' *TIMD ( ST. ) ( EM = 99:99:99'.'9 )
            new ReportEditMask ("99:99:99'.'9"));
        if (Global.isEscape()) return;
    }
    private void sub_Work_Rqst_Update() throws Exception                                                                                                                  //Natural: WORK-RQST-UPDATE
    {
        if (BLNatReinput.isReinput()) return;

        G1:                                                                                                                                                               //Natural: GET CWF-WHO-WORK-REQUEST-VIEW #ISN
        ldaCwfl3019.getVw_cwf_Who_Work_Request_View().readByID(pnd_Isn.getLong(), "G1");
        ldaCwfl3019.getCwf_Who_Work_Request_View_Pin_Nbr().setValue(pnd_Pin_Nbr_Save);                                                                                    //Natural: ASSIGN CWF-WHO-WORK-REQUEST-VIEW.PIN-NBR := #PIN-NBR-SAVE
        ldaCwfl3019.getCwf_Who_Work_Request_View_Work_Prcss_Id().setValue(pnd_Wpid_Save);                                                                                 //Natural: ASSIGN CWF-WHO-WORK-REQUEST-VIEW.WORK-PRCSS-ID := #WPID-SAVE
        ldaCwfl3019.getCwf_Who_Work_Request_View_Tiaa_Rcvd_Dte().setValue(pnd_Tiaa_Rcvd_Dte_Save);                                                                        //Natural: ASSIGN CWF-WHO-WORK-REQUEST-VIEW.TIAA-RCVD-DTE := #TIAA-RCVD-DTE-SAVE
        ldaCwfl3019.getCwf_Who_Work_Request_View_Rqst_Id().setValue(pnd_Rqst_Id_Save);                                                                                    //Natural: ASSIGN CWF-WHO-WORK-REQUEST-VIEW.RQST-ID := #RQST-ID-SAVE
        ldaCwfl3019.getCwf_Who_Work_Request_View_Rlte_Rqst_Id().setValue(pnd_Rlte_Rqst_Id_Save);                                                                          //Natural: ASSIGN CWF-WHO-WORK-REQUEST-VIEW.RLTE-RQST-ID := #RLTE-RQST-ID-SAVE
        ldaCwfl3019.getCwf_Who_Work_Request_View_Crprte_Status_Ind().setValue(pnd_Crprte_Stat_Ind_Save);                                                                  //Natural: ASSIGN CWF-WHO-WORK-REQUEST-VIEW.CRPRTE-STATUS-IND := #CRPRTE-STAT-IND-SAVE
        ldaCwfl3019.getCwf_Who_Work_Request_View_Status_Cde().setValue(pnd_Mit_Status_Cde_Save);                                                                          //Natural: ASSIGN CWF-WHO-WORK-REQUEST-VIEW.STATUS-CDE := #MIT-STATUS-CDE-SAVE
        ldaCwfl3019.getCwf_Who_Work_Request_View_Final_Close_Out_Dte_Tme().setValue(pnd_Final_Close_Out_Save);                                                            //Natural: ASSIGN CWF-WHO-WORK-REQUEST-VIEW.FINAL-CLOSE-OUT-DTE-TME := #FINAL-CLOSE-OUT-SAVE
        ldaCwfl3019.getCwf_Who_Work_Request_View_Crprte_Clock_End_Dte_Tme().setValue(pnd_Crprte_Clock_Dte_Save);                                                          //Natural: ASSIGN CWF-WHO-WORK-REQUEST-VIEW.CRPRTE-CLOCK-END-DTE-TME := #CRPRTE-CLOCK-DTE-SAVE
        ldaCwfl3019.getCwf_Who_Work_Request_View_Dwnld_Updte_Dte_Tme().setValue(pnd_Date_T);                                                                              //Natural: ASSIGN CWF-WHO-WORK-REQUEST-VIEW.DWNLD-UPDTE-DTE-TME := #DATE-T
        if (condition(pnd_Sec_Discrep.equals("Y")))                                                                                                                       //Natural: IF #SEC-DISCREP = 'Y'
        {
            ldaCwfl3019.getCwf_Who_Work_Request_View_Sec_Turnaround_Tme().setValue(pnd_Bus_Days);                                                                         //Natural: ASSIGN CWF-WHO-WORK-REQUEST-VIEW.SEC-TURNAROUND-TME := #BUS-DAYS
            ldaCwfl3019.getCwf_Who_Work_Request_View_Effctve_Dte().setValue(cwf_Master_Effctve_Dte);                                                                      //Natural: ASSIGN CWF-WHO-WORK-REQUEST-VIEW.EFFCTVE-DTE := CWF-MASTER.EFFCTVE-DTE
            ldaCwfl3019.getCwf_Who_Work_Request_View_Trans_Dte().setValue(cwf_Master_Trans_Dte);                                                                          //Natural: ASSIGN CWF-WHO-WORK-REQUEST-VIEW.TRANS-DTE := CWF-MASTER.TRANS-DTE
            ldaCwfl3019.getCwf_Who_Work_Request_View_Sec_Updte_Dte().setValue(Global.getDATX());                                                                          //Natural: ASSIGN CWF-WHO-WORK-REQUEST-VIEW.SEC-UPDTE-DTE := *DATX
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Cor_Counter.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #COR-COUNTER
        ldaCwfl3019.getVw_cwf_Who_Work_Request_View().updateDBRow("G1");                                                                                                  //Natural: UPDATE
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_Statistic() throws Exception                                                                                                                         //Natural: STATISTIC
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************
        //*  PULL CONTROL RECORD TO APPLY STATISTIC COUNTERS
        //*  -----------------------------------------------
        GET_CONTROL_REC:                                                                                                                                                  //Natural: GET CWF-WHO-SUPPORT-TBL MIHA0200-OUTPUT.RUN-CONTROL-ISN
        ldaMihl0210.getVw_cwf_Who_Support_Tbl().readByID(pdaMiha0200.getMiha0200_Output_Run_Control_Isn().getLong(), "GET_CONTROL_REC");
        ldaMihl0210.getCwf_Who_Support_Tbl_Number_Of_Recs_Compared().compute(new ComputeParameters(false, ldaMihl0210.getCwf_Who_Support_Tbl_Number_Of_Recs_Compared()),  //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.NUMBER-OF-RECS-COMPARED = ( #MIT-COUNTER-O + #MIT-COUNTER-C )
            (pnd_Mit_Counter_O.add(pnd_Mit_Counter_C)));
        ldaMihl0210.getCwf_Who_Support_Tbl_Ending_Date().setValue(pnd_Date_T);                                                                                            //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.ENDING-DATE := #DATE-T
        ldaMihl0210.getCwf_Who_Support_Tbl_Number_Of_Recs_Nfnd_Who().setValue(pnd_Nfd_Counter);                                                                           //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.NUMBER-OF-RECS-NFND-WHO := #NFD-COUNTER
        ldaMihl0210.getCwf_Who_Support_Tbl_Number_Of_Recs_Archived().setValue(pnd_Now_Counter);                                                                           //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.NUMBER-OF-RECS-ARCHIVED := #NOW-COUNTER
        ldaMihl0210.getCwf_Who_Support_Tbl_Number_Of_Recs_Correctd().setValue(pnd_Cor_Counter);                                                                           //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.NUMBER-OF-RECS-CORRECTD := #COR-COUNTER
        ldaMihl0210.getCwf_Who_Support_Tbl_Run_Date().setValue(Global.getDATN());                                                                                         //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.RUN-DATE := *DATN
        ldaMihl0210.getVw_cwf_Who_Support_Tbl().updateDBRow("GET_CONTROL_REC");                                                                                           //Natural: UPDATE ( GET-CONTROL-REC. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  (4935)
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=56 LS=131");
    }
}
