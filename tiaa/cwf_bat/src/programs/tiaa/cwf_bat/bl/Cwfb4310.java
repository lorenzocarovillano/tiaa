/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:33:36 PM
**        * FROM NATURAL PROGRAM : Cwfb4310
************************************************************
**        * FILE NAME            : Cwfb4310.java
**        * CLASS NAME           : Cwfb4310
**        * INSTANCE NAME        : Cwfb4310
************************************************************
************************************************************************
* PROGRAM  : CWFB4300
* SYSTEM   : PROJCWF
* FUNCTION : RECORD SUPPORT SERVICES REPORT
*          : WORK REQUESTS REQUIRING HISTORICAL DOCUMENTS REVIEW
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
*
* 02/23/2017 - PIN EXPANSION - AUG 2017 - REFER FOR THE TAG PIN-EXP
*              FOR CHANGES
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb4310 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Vars;
    private DbsField pnd_Vars_Pnd_Mit_Tot_Cnt;
    private DbsField pnd_Vars_Pnd_Hist_Doc_Cnt;
    private DbsField pnd_Vars_Pnd_Act_Microjacket_Cnt_1;
    private DbsField pnd_Vars_Pnd_Frz_Microjacket_Cnt_1;
    private DbsField pnd_Vars_Pnd_Dig_Microjacket_Cnt_2;
    private DbsField pnd_Vars_Pnd_Need_To_Be_Dig_Cnt_3;
    private DbsField pnd_Vars_Pnd_Not_Microjacket_Cnt_2;
    private DbsField pnd_Vars_Pnd_Awt_Microjacket_Cnt_3;
    private DbsField pnd_Vars_Pnd_Category_1_Cnt;
    private DbsField pnd_Vars_Pnd_Category_2_Cnt;
    private DbsField pnd_Vars_Pnd_Category_3_Cnt;
    private DbsField pnd_Vars_Pnd_Not_Catgry_Cnt;
    private DbsField pnd_Vars_Pnd_Perc_Catg_1;
    private DbsField pnd_Vars_Pnd_Perc_Catg_2;
    private DbsField pnd_Vars_Pnd_Perc_Catg_3;
    private DbsField pnd_Vars_Pnd_Per_No_Catg;
    private DbsField pnd_Vars_Pnd_Perc_All_Rqst;
    private DbsField pnd_Vars_Pnd_Read_Routing_Cnt;
    private DbsField pnd_Vars_Pnd_Read_Cabinet_Cnt;
    private DbsField pnd_Vars_Pnd_Not_Category_Cnt;
    private DbsField pnd_Vars_Pnd_Routing_Nf_Cnt;
    private DbsField pnd_Vars_Pnd_Unit_Tbl_Nf_Cnt;
    private DbsField pnd_Vars_Pnd_Routing_Match;
    private DbsField pnd_Vars_Pnd_No_Routing_Match;
    private DbsField pnd_Vars_Pnd_Cab_Not_Match;
    private DbsField pnd_Vars_Pnd_Cab_Match;
    private DbsField pnd_Vars_Pnd_Tot_Match;
    private DbsField pnd_Vars_Pnd_Fld_A;
    private DbsField pnd_Vars_Pnd_Fld_1;
    private DbsField pnd_Vars_Pnd_Fld_2;
    private DbsField pnd_Vars_Pnd_Fld_3;
    private DbsField pnd_Vars_Pnd_Fld_4;
    private DbsField pnd_Vars_Pnd_End_Dte;
    private DbsField pnd_Vars_Pnd_Hdr_Dte;
    private DbsField pnd_Vars_Pnd_End_Dte_Proc;
    private DbsField pnd_Vars_Pnd_Rec_Processed;
    private DbsField pnd_Vars_Pnd_Hold_Wpid;
    private DbsField pnd_Vars_Pnd_Sort_Cnt;
    private DbsField pnd_Vars_Pnd_Sort_Tot;
    private DbsField pnd_Vars_Pnd_M_Pin_Nbr;
    private DbsField pnd_Vars_Pnd_M_Rqst_Log_Index_Dte;
    private DbsField pnd_Vars_Pnd_M_Work_Prcss_Id;
    private DbsField pnd_Vars_Pnd_M_Mj_Pull_Ind;
    private DbsField pnd_Vars_Pnd_M_Rqst_Rgn_Cde;
    private DbsField pnd_Vars_Pnd_M_Owner_Unit_Cde;
    private DbsField pnd_Vars_Pnd_C_Cabinet_Id;
    private DbsField pnd_Vars_Pnd_C_Media_Ind;
    private DbsField pnd_Vars_Pnd_C_Dgtze_Rqst_Log_Dte_Tme;
    private DbsField pnd_Vars_Pnd_R_Work_Prcss_Id;
    private DbsField pnd_Vars_Pnd_R_Dgtze_Ind;
    private DbsField pnd_Vars_Pnd_R_Mj_Pull_Ind;
    private DbsField pnd_Vars_Pnd_R_Dgtze_Region_Ind;
    private DbsField pnd_Vars_Pnd_R_Dgtze_Branch_Ind;
    private DbsField pnd_Vars_Pnd_R_Dgtze_Special_Needs_Ind;
    private DbsField pnd_Vars_Pnd_U_Unit_Cde;
    private DbsField pnd_Vars_Pnd_U_Unit_Bldg_Nbr_Brnch_Loc;
    private DbsField pnd_Vars_Pnd_Efm_Pin_Nbr_A;

    private DbsGroup pnd_Vars__R_Field_1;
    private DbsField pnd_Vars_Pnd_Efm_Pin_Pref;
    private DbsField pnd_Vars_Pnd_Efm_Pin_Nbr;
    private DbsField pnd_Vars_Pnd_Efm_Pin_Suff;
    private DbsField pnd_Vars_Pnd_Save_Isn;
    private DbsField pnd_Vars_Pnd_Start_Hdr_Dte;
    private DbsField pnd_Wpid_Uniq_Key;

    private DbsGroup pnd_Wpid_Uniq_Key__R_Field_2;
    private DbsField pnd_Wpid_Uniq_Key_Pnd_Work_Prcss_Id;
    private DbsField pnd_Wpid_Uniq_Key_Pnd_Act_Ind_A;
    private DbsField pnd_Actv_Unque_Key;

    private DbsGroup pnd_Actv_Unque_Key__R_Field_3;
    private DbsField pnd_Actv_Unque_Key_Pnd_Rqst_Log_Dte_Tme_Key;
    private DbsField pnd_Actv_Unque_Key_Pnd_Act_Ind;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_4;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Lvl;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Name;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Fld;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Actv_Ind;

    private DataAccessProgramView vw_cwf_Master;
    private DbsField cwf_Master_Pin_Nbr;
    private DbsField cwf_Master_Rqst_Rgn_Cde;
    private DbsField cwf_Master_Rqst_Log_Dte_Tme;

    private DbsGroup cwf_Master__R_Field_5;
    private DbsField cwf_Master_Rqst_Log_Index_Dte;
    private DbsField cwf_Master_Rqst_Log_Index_Tme;
    private DbsField cwf_Master_Work_Prcss_Id;
    private DbsField cwf_Master_Mj_Pull_Ind;
    private DbsField cwf_Master_Actve_Ind;
    private DbsField cwf_Master_Owner_Unit_Cde;

    private DataAccessProgramView vw_cwf_Efm_Cabinet;
    private DbsField cwf_Efm_Cabinet_Cabinet_Id;
    private DbsField cwf_Efm_Cabinet_Media_Ind;
    private DbsField cwf_Efm_Cabinet_Dgtze_Rqst_Log_Dte_Tme;

    private DataAccessProgramView vw_cwf_Routing_Tbl;
    private DbsField cwf_Routing_Tbl_Work_Prcss_Id;

    private DbsGroup cwf_Routing_Tbl__R_Field_6;
    private DbsField cwf_Routing_Tbl_Work_Actn_Rqstd_Cde;
    private DbsField cwf_Routing_Tbl_Work_Lob_Cmpny_Prdct_Cde;
    private DbsField cwf_Routing_Tbl_Work_Mjr_Bsnss_Prcss_Cde;
    private DbsField cwf_Routing_Tbl_Work_Spcfc_Bsnss_Prcss_Cde;
    private DbsField cwf_Routing_Tbl_Dgtze_Ind;
    private DbsField cwf_Routing_Tbl_Mj_Pull_Ind;
    private DbsField cwf_Routing_Tbl_Actve_Ind;
    private DbsField cwf_Routing_Tbl_Dgtze_Region_Ind;
    private DbsField cwf_Routing_Tbl_Dgtze_Branch_Ind;
    private DbsField cwf_Routing_Tbl_Dgtze_Special_Needs_Ind;

    private DataAccessProgramView vw_cwf_Org_Unit_Tbl;
    private DbsField cwf_Org_Unit_Tbl_Unit_Cde;

    private DbsGroup cwf_Org_Unit_Tbl__R_Field_7;
    private DbsField cwf_Org_Unit_Tbl_Unit_Id_Cde;
    private DbsField cwf_Org_Unit_Tbl_Unit_Rgn_Cde;
    private DbsField cwf_Org_Unit_Tbl_Unit_Spcl_Dsgntn_Cde;
    private DbsField cwf_Org_Unit_Tbl_Unit_Brnch_Group_Cde;
    private DbsField cwf_Org_Unit_Tbl_Unit_Bldg_Nbr_Brnch_Loc;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_8;
    private DbsField cwf_Support_Tbl_Tbl_Rpt_Date_Tme;

    private DbsGroup cwf_Support_Tbl__R_Field_9;
    private DbsField cwf_Support_Tbl_Pnd_Tbl_Dte;
    private DbsField cwf_Support_Tbl_Pnd_Tbl_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Dte;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Vars = localVariables.newGroupInRecord("pnd_Vars", "#VARS");
        pnd_Vars_Pnd_Mit_Tot_Cnt = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Mit_Tot_Cnt", "#MIT-TOT-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Vars_Pnd_Hist_Doc_Cnt = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Hist_Doc_Cnt", "#HIST-DOC-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Vars_Pnd_Act_Microjacket_Cnt_1 = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Act_Microjacket_Cnt_1", "#ACT-MICROJACKET-CNT-1", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Vars_Pnd_Frz_Microjacket_Cnt_1 = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Frz_Microjacket_Cnt_1", "#FRZ-MICROJACKET-CNT-1", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Vars_Pnd_Dig_Microjacket_Cnt_2 = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Dig_Microjacket_Cnt_2", "#DIG-MICROJACKET-CNT-2", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Vars_Pnd_Need_To_Be_Dig_Cnt_3 = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Need_To_Be_Dig_Cnt_3", "#NEED-TO-BE-DIG-CNT-3", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Vars_Pnd_Not_Microjacket_Cnt_2 = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Not_Microjacket_Cnt_2", "#NOT-MICROJACKET-CNT-2", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Vars_Pnd_Awt_Microjacket_Cnt_3 = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Awt_Microjacket_Cnt_3", "#AWT-MICROJACKET-CNT-3", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Vars_Pnd_Category_1_Cnt = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Category_1_Cnt", "#CATEGORY-1-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Vars_Pnd_Category_2_Cnt = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Category_2_Cnt", "#CATEGORY-2-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Vars_Pnd_Category_3_Cnt = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Category_3_Cnt", "#CATEGORY-3-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Vars_Pnd_Not_Catgry_Cnt = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Not_Catgry_Cnt", "#NOT-CATGRY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Vars_Pnd_Perc_Catg_1 = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Perc_Catg_1", "#PERC-CATG-1", FieldType.NUMERIC, 4, 1);
        pnd_Vars_Pnd_Perc_Catg_2 = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Perc_Catg_2", "#PERC-CATG-2", FieldType.NUMERIC, 4, 1);
        pnd_Vars_Pnd_Perc_Catg_3 = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Perc_Catg_3", "#PERC-CATG-3", FieldType.NUMERIC, 4, 1);
        pnd_Vars_Pnd_Per_No_Catg = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Per_No_Catg", "#PER-NO-CATG", FieldType.NUMERIC, 4, 1);
        pnd_Vars_Pnd_Perc_All_Rqst = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Perc_All_Rqst", "#PERC-ALL-RQST", FieldType.NUMERIC, 4, 1);
        pnd_Vars_Pnd_Read_Routing_Cnt = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Read_Routing_Cnt", "#READ-ROUTING-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Vars_Pnd_Read_Cabinet_Cnt = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Read_Cabinet_Cnt", "#READ-CABINET-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Vars_Pnd_Not_Category_Cnt = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Not_Category_Cnt", "#NOT-CATEGORY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Vars_Pnd_Routing_Nf_Cnt = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Routing_Nf_Cnt", "#ROUTING-NF-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Vars_Pnd_Unit_Tbl_Nf_Cnt = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Unit_Tbl_Nf_Cnt", "#UNIT-TBL-NF-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Vars_Pnd_Routing_Match = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Routing_Match", "#ROUTING-MATCH", FieldType.PACKED_DECIMAL, 7);
        pnd_Vars_Pnd_No_Routing_Match = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_No_Routing_Match", "#NO-ROUTING-MATCH", FieldType.PACKED_DECIMAL, 7);
        pnd_Vars_Pnd_Cab_Not_Match = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Cab_Not_Match", "#CAB-NOT-MATCH", FieldType.PACKED_DECIMAL, 7);
        pnd_Vars_Pnd_Cab_Match = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Cab_Match", "#CAB-MATCH", FieldType.PACKED_DECIMAL, 7);
        pnd_Vars_Pnd_Tot_Match = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Tot_Match", "#TOT-MATCH", FieldType.PACKED_DECIMAL, 7);
        pnd_Vars_Pnd_Fld_A = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Fld_A", "#FLD-A", FieldType.STRING, 25);
        pnd_Vars_Pnd_Fld_1 = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Fld_1", "#FLD-1", FieldType.STRING, 25);
        pnd_Vars_Pnd_Fld_2 = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Fld_2", "#FLD-2", FieldType.STRING, 25);
        pnd_Vars_Pnd_Fld_3 = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Fld_3", "#FLD-3", FieldType.STRING, 25);
        pnd_Vars_Pnd_Fld_4 = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Fld_4", "#FLD-4", FieldType.STRING, 25);
        pnd_Vars_Pnd_End_Dte = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_End_Dte", "#END-DTE", FieldType.DATE);
        pnd_Vars_Pnd_Hdr_Dte = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Hdr_Dte", "#HDR-DTE", FieldType.DATE);
        pnd_Vars_Pnd_End_Dte_Proc = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_End_Dte_Proc", "#END-DTE-PROC", FieldType.STRING, 8);
        pnd_Vars_Pnd_Rec_Processed = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Rec_Processed", "#REC-PROCESSED", FieldType.BOOLEAN, 1);
        pnd_Vars_Pnd_Hold_Wpid = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Hold_Wpid", "#HOLD-WPID", FieldType.STRING, 6);
        pnd_Vars_Pnd_Sort_Cnt = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Sort_Cnt", "#SORT-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Vars_Pnd_Sort_Tot = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Sort_Tot", "#SORT-TOT", FieldType.PACKED_DECIMAL, 7);
        pnd_Vars_Pnd_M_Pin_Nbr = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_M_Pin_Nbr", "#M-PIN-NBR", FieldType.STRING, 12);
        pnd_Vars_Pnd_M_Rqst_Log_Index_Dte = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_M_Rqst_Log_Index_Dte", "#M-RQST-LOG-INDEX-DTE", FieldType.STRING, 8);
        pnd_Vars_Pnd_M_Work_Prcss_Id = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_M_Work_Prcss_Id", "#M-WORK-PRCSS-ID", FieldType.STRING, 6);
        pnd_Vars_Pnd_M_Mj_Pull_Ind = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_M_Mj_Pull_Ind", "#M-MJ-PULL-IND", FieldType.STRING, 1);
        pnd_Vars_Pnd_M_Rqst_Rgn_Cde = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_M_Rqst_Rgn_Cde", "#M-RQST-RGN-CDE", FieldType.STRING, 1);
        pnd_Vars_Pnd_M_Owner_Unit_Cde = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_M_Owner_Unit_Cde", "#M-OWNER-UNIT-CDE", FieldType.STRING, 8);
        pnd_Vars_Pnd_C_Cabinet_Id = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_C_Cabinet_Id", "#C-CABINET-ID", FieldType.STRING, 14);
        pnd_Vars_Pnd_C_Media_Ind = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_C_Media_Ind", "#C-MEDIA-IND", FieldType.STRING, 1);
        pnd_Vars_Pnd_C_Dgtze_Rqst_Log_Dte_Tme = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_C_Dgtze_Rqst_Log_Dte_Tme", "#C-DGTZE-RQST-LOG-DTE-TME", FieldType.STRING, 
            15);
        pnd_Vars_Pnd_R_Work_Prcss_Id = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_R_Work_Prcss_Id", "#R-WORK-PRCSS-ID", FieldType.STRING, 6);
        pnd_Vars_Pnd_R_Dgtze_Ind = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_R_Dgtze_Ind", "#R-DGTZE-IND", FieldType.STRING, 1);
        pnd_Vars_Pnd_R_Mj_Pull_Ind = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_R_Mj_Pull_Ind", "#R-MJ-PULL-IND", FieldType.STRING, 1);
        pnd_Vars_Pnd_R_Dgtze_Region_Ind = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_R_Dgtze_Region_Ind", "#R-DGTZE-REGION-IND", FieldType.STRING, 1);
        pnd_Vars_Pnd_R_Dgtze_Branch_Ind = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_R_Dgtze_Branch_Ind", "#R-DGTZE-BRANCH-IND", FieldType.STRING, 1);
        pnd_Vars_Pnd_R_Dgtze_Special_Needs_Ind = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_R_Dgtze_Special_Needs_Ind", "#R-DGTZE-SPECIAL-NEEDS-IND", FieldType.STRING, 
            1);
        pnd_Vars_Pnd_U_Unit_Cde = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_U_Unit_Cde", "#U-UNIT-CDE", FieldType.STRING, 8);
        pnd_Vars_Pnd_U_Unit_Bldg_Nbr_Brnch_Loc = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_U_Unit_Bldg_Nbr_Brnch_Loc", "#U-UNIT-BLDG-NBR-BRNCH-LOC", FieldType.STRING, 
            3);
        pnd_Vars_Pnd_Efm_Pin_Nbr_A = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Efm_Pin_Nbr_A", "#EFM-PIN-NBR-A", FieldType.STRING, 14);

        pnd_Vars__R_Field_1 = pnd_Vars.newGroupInGroup("pnd_Vars__R_Field_1", "REDEFINE", pnd_Vars_Pnd_Efm_Pin_Nbr_A);
        pnd_Vars_Pnd_Efm_Pin_Pref = pnd_Vars__R_Field_1.newFieldInGroup("pnd_Vars_Pnd_Efm_Pin_Pref", "#EFM-PIN-PREF", FieldType.STRING, 1);
        pnd_Vars_Pnd_Efm_Pin_Nbr = pnd_Vars__R_Field_1.newFieldInGroup("pnd_Vars_Pnd_Efm_Pin_Nbr", "#EFM-PIN-NBR", FieldType.STRING, 12);
        pnd_Vars_Pnd_Efm_Pin_Suff = pnd_Vars__R_Field_1.newFieldInGroup("pnd_Vars_Pnd_Efm_Pin_Suff", "#EFM-PIN-SUFF", FieldType.STRING, 1);
        pnd_Vars_Pnd_Save_Isn = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Save_Isn", "#SAVE-ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Vars_Pnd_Start_Hdr_Dte = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Start_Hdr_Dte", "#START-HDR-DTE", FieldType.DATE);
        pnd_Wpid_Uniq_Key = localVariables.newFieldInRecord("pnd_Wpid_Uniq_Key", "#WPID-UNIQ-KEY", FieldType.STRING, 7);

        pnd_Wpid_Uniq_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Wpid_Uniq_Key__R_Field_2", "REDEFINE", pnd_Wpid_Uniq_Key);
        pnd_Wpid_Uniq_Key_Pnd_Work_Prcss_Id = pnd_Wpid_Uniq_Key__R_Field_2.newFieldInGroup("pnd_Wpid_Uniq_Key_Pnd_Work_Prcss_Id", "#WORK-PRCSS-ID", FieldType.STRING, 
            6);
        pnd_Wpid_Uniq_Key_Pnd_Act_Ind_A = pnd_Wpid_Uniq_Key__R_Field_2.newFieldInGroup("pnd_Wpid_Uniq_Key_Pnd_Act_Ind_A", "#ACT-IND-A", FieldType.STRING, 
            1);
        pnd_Actv_Unque_Key = localVariables.newFieldInRecord("pnd_Actv_Unque_Key", "#ACTV-UNQUE-KEY", FieldType.STRING, 16);

        pnd_Actv_Unque_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Actv_Unque_Key__R_Field_3", "REDEFINE", pnd_Actv_Unque_Key);
        pnd_Actv_Unque_Key_Pnd_Rqst_Log_Dte_Tme_Key = pnd_Actv_Unque_Key__R_Field_3.newFieldInGroup("pnd_Actv_Unque_Key_Pnd_Rqst_Log_Dte_Tme_Key", "#RQST-LOG-DTE-TME-KEY", 
            FieldType.STRING, 15);
        pnd_Actv_Unque_Key_Pnd_Act_Ind = pnd_Actv_Unque_Key__R_Field_3.newFieldInGroup("pnd_Actv_Unque_Key_Pnd_Act_Ind", "#ACT-IND", FieldType.STRING, 
            1);
        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_4", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Lvl = pnd_Tbl_Prime_Key__R_Field_4.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Lvl", "#TBL-SCRTY-LVL", FieldType.STRING, 
            2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Name = pnd_Tbl_Prime_Key__R_Field_4.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Name", "#TBL-NAME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Fld = pnd_Tbl_Prime_Key__R_Field_4.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Fld", "#TBL-KEY-FLD", FieldType.STRING, 
            30);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actv_Ind = pnd_Tbl_Prime_Key__R_Field_4.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Actv_Ind", "#TBL-ACTV-IND", FieldType.STRING, 
            1);

        vw_cwf_Master = new DataAccessProgramView(new NameInfo("vw_cwf_Master", "CWF-MASTER"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        cwf_Master_Pin_Nbr = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "PIN_NBR");
        cwf_Master_Pin_Nbr.setDdmHeader("PIN");
        cwf_Master_Rqst_Rgn_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Rqst_Rgn_Cde", "RQST-RGN-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RQST_RGN_CDE");
        cwf_Master_Rqst_Log_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15, 
            RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Master_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");

        cwf_Master__R_Field_5 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master__R_Field_5", "REDEFINE", cwf_Master_Rqst_Log_Dte_Tme);
        cwf_Master_Rqst_Log_Index_Dte = cwf_Master__R_Field_5.newFieldInGroup("cwf_Master_Rqst_Log_Index_Dte", "RQST-LOG-INDEX-DTE", FieldType.STRING, 
            8);
        cwf_Master_Rqst_Log_Index_Tme = cwf_Master__R_Field_5.newFieldInGroup("cwf_Master_Rqst_Log_Index_Tme", "RQST-LOG-INDEX-TME", FieldType.STRING, 
            7);
        cwf_Master_Work_Prcss_Id = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        cwf_Master_Work_Prcss_Id.setDdmHeader("WORK/ID");
        cwf_Master_Mj_Pull_Ind = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Mj_Pull_Ind", "MJ-PULL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "MJ_PULL_IND");
        cwf_Master_Mj_Pull_Ind.setDdmHeader("MJ PULL/IND");
        cwf_Master_Actve_Ind = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTVE_IND");
        cwf_Master_Owner_Unit_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Owner_Unit_Cde", "OWNER-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "OWNER_UNIT_CDE");
        cwf_Master_Owner_Unit_Cde.setDdmHeader("OWNERUNIT");
        registerRecord(vw_cwf_Master);

        vw_cwf_Efm_Cabinet = new DataAccessProgramView(new NameInfo("vw_cwf_Efm_Cabinet", "CWF-EFM-CABINET"), "CWF_EFM_CABINET", "CWF_EFM_CABINET");
        cwf_Efm_Cabinet_Cabinet_Id = vw_cwf_Efm_Cabinet.getRecord().newFieldInGroup("cwf_Efm_Cabinet_Cabinet_Id", "CABINET-ID", FieldType.STRING, 14, 
            RepeatingFieldStrategy.None, "CABINET_ID");
        cwf_Efm_Cabinet_Cabinet_Id.setDdmHeader("CABINET/ID");
        cwf_Efm_Cabinet_Media_Ind = vw_cwf_Efm_Cabinet.getRecord().newFieldInGroup("cwf_Efm_Cabinet_Media_Ind", "MEDIA-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "MEDIA_IND");
        cwf_Efm_Cabinet_Media_Ind.setDdmHeader("MEDIA IND");
        cwf_Efm_Cabinet_Dgtze_Rqst_Log_Dte_Tme = vw_cwf_Efm_Cabinet.getRecord().newFieldInGroup("cwf_Efm_Cabinet_Dgtze_Rqst_Log_Dte_Tme", "DGTZE-RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "DGTZE_RQST_LOG_DTE_TME");
        cwf_Efm_Cabinet_Dgtze_Rqst_Log_Dte_Tme.setDdmHeader("DGTZE RQST LOG/DTE TME");
        registerRecord(vw_cwf_Efm_Cabinet);

        vw_cwf_Routing_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Routing_Tbl", "CWF-ROUTING-TBL"), "CWF_WP_WORK_PRCSS_ID", "CWF_PROFILE");
        cwf_Routing_Tbl_Work_Prcss_Id = vw_cwf_Routing_Tbl.getRecord().newFieldInGroup("cwf_Routing_Tbl_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        cwf_Routing_Tbl_Work_Prcss_Id.setDdmHeader("WORK PRCSS ID");

        cwf_Routing_Tbl__R_Field_6 = vw_cwf_Routing_Tbl.getRecord().newGroupInGroup("cwf_Routing_Tbl__R_Field_6", "REDEFINE", cwf_Routing_Tbl_Work_Prcss_Id);
        cwf_Routing_Tbl_Work_Actn_Rqstd_Cde = cwf_Routing_Tbl__R_Field_6.newFieldInGroup("cwf_Routing_Tbl_Work_Actn_Rqstd_Cde", "WORK-ACTN-RQSTD-CDE", 
            FieldType.STRING, 1);
        cwf_Routing_Tbl_Work_Lob_Cmpny_Prdct_Cde = cwf_Routing_Tbl__R_Field_6.newFieldInGroup("cwf_Routing_Tbl_Work_Lob_Cmpny_Prdct_Cde", "WORK-LOB-CMPNY-PRDCT-CDE", 
            FieldType.STRING, 2);
        cwf_Routing_Tbl_Work_Mjr_Bsnss_Prcss_Cde = cwf_Routing_Tbl__R_Field_6.newFieldInGroup("cwf_Routing_Tbl_Work_Mjr_Bsnss_Prcss_Cde", "WORK-MJR-BSNSS-PRCSS-CDE", 
            FieldType.STRING, 1);
        cwf_Routing_Tbl_Work_Spcfc_Bsnss_Prcss_Cde = cwf_Routing_Tbl__R_Field_6.newFieldInGroup("cwf_Routing_Tbl_Work_Spcfc_Bsnss_Prcss_Cde", "WORK-SPCFC-BSNSS-PRCSS-CDE", 
            FieldType.STRING, 2);
        cwf_Routing_Tbl_Dgtze_Ind = vw_cwf_Routing_Tbl.getRecord().newFieldInGroup("cwf_Routing_Tbl_Dgtze_Ind", "DGTZE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "DGTZE_IND");
        cwf_Routing_Tbl_Dgtze_Ind.setDdmHeader("DIGITIZE/INDICATOR");
        cwf_Routing_Tbl_Mj_Pull_Ind = vw_cwf_Routing_Tbl.getRecord().newFieldInGroup("cwf_Routing_Tbl_Mj_Pull_Ind", "MJ-PULL-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "MJ_PULL_IND");
        cwf_Routing_Tbl_Mj_Pull_Ind.setDdmHeader("MJ/IND");
        cwf_Routing_Tbl_Actve_Ind = vw_cwf_Routing_Tbl.getRecord().newFieldInGroup("cwf_Routing_Tbl_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTVE_IND");
        cwf_Routing_Tbl_Dgtze_Region_Ind = vw_cwf_Routing_Tbl.getRecord().newFieldInGroup("cwf_Routing_Tbl_Dgtze_Region_Ind", "DGTZE-REGION-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DGTZE_REGION_IND");
        cwf_Routing_Tbl_Dgtze_Region_Ind.setDdmHeader("DIGITIZE REGION/INDICATOR");
        cwf_Routing_Tbl_Dgtze_Branch_Ind = vw_cwf_Routing_Tbl.getRecord().newFieldInGroup("cwf_Routing_Tbl_Dgtze_Branch_Ind", "DGTZE-BRANCH-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DGTZE_BRANCH_IND");
        cwf_Routing_Tbl_Dgtze_Branch_Ind.setDdmHeader("DIGITIZE BRANCH/INDICATOR");
        cwf_Routing_Tbl_Dgtze_Special_Needs_Ind = vw_cwf_Routing_Tbl.getRecord().newFieldInGroup("cwf_Routing_Tbl_Dgtze_Special_Needs_Ind", "DGTZE-SPECIAL-NEEDS-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "DGTZE_SPECIAL_NEEDS_IND");
        cwf_Routing_Tbl_Dgtze_Special_Needs_Ind.setDdmHeader("DIGITIZE SPECIAL/NEEDS INDICATOR");
        registerRecord(vw_cwf_Routing_Tbl);

        vw_cwf_Org_Unit_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Org_Unit_Tbl", "CWF-ORG-UNIT-TBL"), "CWF_ORG_UNIT_TBL", "CWF_ASSIGN_RULE");
        cwf_Org_Unit_Tbl_Unit_Cde = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "UNIT_CDE");
        cwf_Org_Unit_Tbl_Unit_Cde.setDdmHeader("UNIT/CODE");

        cwf_Org_Unit_Tbl__R_Field_7 = vw_cwf_Org_Unit_Tbl.getRecord().newGroupInGroup("cwf_Org_Unit_Tbl__R_Field_7", "REDEFINE", cwf_Org_Unit_Tbl_Unit_Cde);
        cwf_Org_Unit_Tbl_Unit_Id_Cde = cwf_Org_Unit_Tbl__R_Field_7.newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Id_Cde", "UNIT-ID-CDE", FieldType.STRING, 5);
        cwf_Org_Unit_Tbl_Unit_Rgn_Cde = cwf_Org_Unit_Tbl__R_Field_7.newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Rgn_Cde", "UNIT-RGN-CDE", FieldType.STRING, 
            1);
        cwf_Org_Unit_Tbl_Unit_Spcl_Dsgntn_Cde = cwf_Org_Unit_Tbl__R_Field_7.newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Spcl_Dsgntn_Cde", "UNIT-SPCL-DSGNTN-CDE", 
            FieldType.STRING, 1);
        cwf_Org_Unit_Tbl_Unit_Brnch_Group_Cde = cwf_Org_Unit_Tbl__R_Field_7.newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Brnch_Group_Cde", "UNIT-BRNCH-GROUP-CDE", 
            FieldType.STRING, 1);
        cwf_Org_Unit_Tbl_Unit_Bldg_Nbr_Brnch_Loc = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Bldg_Nbr_Brnch_Loc", "UNIT-BLDG-NBR-BRNCH-LOC", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "UNIT_BLDG_NBR_BRNCH_LOC");
        cwf_Org_Unit_Tbl_Unit_Bldg_Nbr_Brnch_Loc.setDdmHeader("BLDG NO //BR. CODE");
        registerRecord(vw_cwf_Org_Unit_Tbl);

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Support_Tbl__R_Field_8 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_8", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Tbl_Rpt_Date_Tme = cwf_Support_Tbl__R_Field_8.newFieldInGroup("cwf_Support_Tbl_Tbl_Rpt_Date_Tme", "TBL-RPT-DATE-TME", FieldType.STRING, 
            15);

        cwf_Support_Tbl__R_Field_9 = cwf_Support_Tbl__R_Field_8.newGroupInGroup("cwf_Support_Tbl__R_Field_9", "REDEFINE", cwf_Support_Tbl_Tbl_Rpt_Date_Tme);
        cwf_Support_Tbl_Pnd_Tbl_Dte = cwf_Support_Tbl__R_Field_9.newFieldInGroup("cwf_Support_Tbl_Pnd_Tbl_Dte", "#TBL-DTE", FieldType.STRING, 8);
        cwf_Support_Tbl_Pnd_Tbl_Tme = cwf_Support_Tbl__R_Field_9.newFieldInGroup("cwf_Support_Tbl_Pnd_Tbl_Tme", "#TBL-TME", FieldType.STRING, 7);
        cwf_Support_Tbl_Tbl_Updte_Dte_Tme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Dte_Tme", "TBL-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE_TME");
        cwf_Support_Tbl_Tbl_Updte_Dte_Tme.setDdmHeader("UPDATE DATE/TIME");
        cwf_Support_Tbl_Tbl_Updte_Dte = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Dte", "TBL-UPDTE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE");
        cwf_Support_Tbl_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Master.reset();
        vw_cwf_Efm_Cabinet.reset();
        vw_cwf_Routing_Tbl.reset();
        vw_cwf_Org_Unit_Tbl.reset();
        vw_cwf_Support_Tbl.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Cwfb4310() throws Exception
    {
        super("Cwfb4310");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("CWFB4310", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 132 PS = 60 ES = OFF;//Natural: FORMAT ( 2 ) LS = 132 PS = 60 ES = OFF;//Natural: AT TOP OF PAGE ( 1 );//Natural: AT TOP OF PAGE ( 2 )
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Lvl.setValue("A");                                                                                                                //Natural: MOVE 'A' TO #TBL-SCRTY-LVL
        pnd_Tbl_Prime_Key_Pnd_Tbl_Name.setValue("RSS-WORK-REQUEST");                                                                                                      //Natural: MOVE 'RSS-WORK-REQUEST' TO #TBL-NAME
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Fld.setValue("CWFB4310");                                                                                                           //Natural: MOVE 'CWFB4310' TO #TBL-KEY-FLD
        vw_cwf_Support_Tbl.startDatabaseFind                                                                                                                              //Natural: FIND ( 1 ) CWF-SUPPORT-TBL WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
        (
        "FIND01",
        new Wc[] { new Wc("TBL_PRIME_KEY", "=", pnd_Tbl_Prime_Key, WcType.WITH) },
        1
        );
        FIND01:
        while (condition(vw_cwf_Support_Tbl.readNextRow("FIND01", true)))
        {
            vw_cwf_Support_Tbl.setIfNotFoundControlFlag(false);
            if (condition(vw_cwf_Support_Tbl.getAstCOUNTER().equals(0)))                                                                                                  //Natural: IF NO RECORDS FOUND
            {
                getReports().write(0, "CWFB4310 Cancelled - table record no found");                                                                                      //Natural: WRITE 'CWFB4310 Cancelled - table record no found'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(5);  if (true) return;                                                                                                                  //Natural: TERMINATE 05
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Vars_Pnd_Save_Isn.setValue(vw_cwf_Support_Tbl.getAstISN("Find01"));                                                                                       //Natural: MOVE *ISN TO #SAVE-ISN
            pnd_Actv_Unque_Key_Pnd_Rqst_Log_Dte_Tme_Key.setValue(cwf_Support_Tbl_Tbl_Rpt_Date_Tme);                                                                       //Natural: MOVE TBL-RPT-DATE-TME TO #RQST-LOG-DTE-TME-KEY
            pnd_Vars_Pnd_Start_Hdr_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),cwf_Support_Tbl_Pnd_Tbl_Dte);                                                        //Natural: MOVE EDITED #TBL-DTE TO #START-HDR-DTE ( EM = YYYYMMDD )
            pnd_Vars_Pnd_End_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),cwf_Support_Tbl_Pnd_Tbl_Dte);                                                              //Natural: MOVE EDITED #TBL-DTE TO #END-DTE ( EM = YYYYMMDD )
            pnd_Vars_Pnd_End_Dte.nadd(7);                                                                                                                                 //Natural: ADD 7 TO #END-DTE
            pnd_Vars_Pnd_End_Dte_Proc.setValueEdited(pnd_Vars_Pnd_End_Dte,new ReportEditMask("YYYYMMDD"));                                                                //Natural: MOVE EDITED #END-DTE ( EM = YYYYMMDD ) TO #END-DTE-PROC
            pnd_Vars_Pnd_Hdr_Dte.setValue(pnd_Vars_Pnd_End_Dte);                                                                                                          //Natural: MOVE #END-DTE TO #HDR-DTE
            pnd_Vars_Pnd_Hdr_Dte.nsubtract(1);                                                                                                                            //Natural: SUBTRACT 1 FROM #HDR-DTE
            pnd_Vars_Pnd_Efm_Pin_Pref.setValue("P");                                                                                                                      //Natural: MOVE 'P' TO #EFM-PIN-PREF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        vw_cwf_Master.startDatabaseRead                                                                                                                                   //Natural: READ CWF-MASTER BY ACTV-UNQUE-KEY FROM #ACTV-UNQUE-KEY
        (
        "READ01",
        new Wc[] { new Wc("ACTV_UNQUE_KEY", ">=", pnd_Actv_Unque_Key, WcType.BY) },
        new Oc[] { new Oc("ACTV_UNQUE_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_cwf_Master.readNextRow("READ01")))
        {
            if (condition(cwf_Master_Rqst_Log_Index_Dte.greaterOrEqual(pnd_Vars_Pnd_End_Dte_Proc)))                                                                       //Natural: IF CWF-MASTER.RQST-LOG-INDEX-DTE GE #END-DTE-PROC
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  MOVE PIN-NBR                   TO #M-PIN-NBR /* PIN-EXP
            //*  PIN-EXP
            pnd_Vars_Pnd_M_Pin_Nbr.moveAll(cwf_Master_Pin_Nbr);                                                                                                           //Natural: MOVE ALL PIN-NBR TO #M-PIN-NBR
            pnd_Vars_Pnd_M_Rqst_Log_Index_Dte.setValue(cwf_Master_Rqst_Log_Index_Dte);                                                                                    //Natural: MOVE RQST-LOG-INDEX-DTE TO #M-RQST-LOG-INDEX-DTE
            pnd_Vars_Pnd_M_Work_Prcss_Id.setValue(cwf_Master_Work_Prcss_Id);                                                                                              //Natural: MOVE CWF-MASTER.WORK-PRCSS-ID TO #M-WORK-PRCSS-ID
            pnd_Vars_Pnd_M_Mj_Pull_Ind.setValue(cwf_Master_Mj_Pull_Ind);                                                                                                  //Natural: MOVE CWF-MASTER.MJ-PULL-IND TO #M-MJ-PULL-IND
            pnd_Vars_Pnd_M_Rqst_Rgn_Cde.setValue(cwf_Master_Rqst_Rgn_Cde);                                                                                                //Natural: MOVE RQST-RGN-CDE TO #M-RQST-RGN-CDE
            pnd_Vars_Pnd_M_Owner_Unit_Cde.setValue(cwf_Master_Owner_Unit_Cde);                                                                                            //Natural: MOVE OWNER-UNIT-CDE TO #M-OWNER-UNIT-CDE
            pnd_Vars_Pnd_Rec_Processed.setValue(false);                                                                                                                   //Natural: MOVE FALSE TO #REC-PROCESSED
            pnd_Vars_Pnd_Mit_Tot_Cnt.nadd(1);                                                                                                                             //Natural: ADD 1 TO #MIT-TOT-CNT
            if (condition(cwf_Master_Mj_Pull_Ind.equals("Y") || cwf_Master_Mj_Pull_Ind.equals("J") || cwf_Master_Mj_Pull_Ind.equals("R")))                                //Natural: IF CWF-MASTER.MJ-PULL-IND = 'Y' OR = 'J' OR = 'R'
            {
                pnd_Vars_Pnd_Read_Cabinet_Cnt.nadd(1);                                                                                                                    //Natural: ADD 1 TO #READ-CABINET-CNT
                //*   MOVE CWF-MASTER.PIN-NBR TO #EFM-PIN-NBR      /* PIN-EXP
                //*  PIN-EXP
                pnd_Vars_Pnd_Efm_Pin_Nbr.moveAll(cwf_Master_Pin_Nbr);                                                                                                     //Natural: MOVE ALL CWF-MASTER.PIN-NBR TO #EFM-PIN-NBR
                                                                                                                                                                          //Natural: PERFORM READ-EFM-CABINET-RECORD
                sub_Read_Efm_Cabinet_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Vars_Pnd_Read_Routing_Cnt.nadd(1);                                                                                                                    //Natural: ADD 1 TO #READ-ROUTING-CNT
                pnd_Wpid_Uniq_Key_Pnd_Work_Prcss_Id.setValue(cwf_Master_Work_Prcss_Id);                                                                                   //Natural: MOVE CWF-MASTER.WORK-PRCSS-ID TO #WORK-PRCSS-ID
                                                                                                                                                                          //Natural: PERFORM READ-WPID-ROUTING-TBL
                sub_Read_Wpid_Routing_Tbl();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pnd_Vars_Pnd_M_Pin_Nbr.reset();                                                                                                                               //Natural: RESET #M-PIN-NBR #M-RQST-LOG-INDEX-DTE #M-WORK-PRCSS-ID #M-MJ-PULL-IND #M-RQST-RGN-CDE #M-OWNER-UNIT-CDE #C-CABINET-ID #C-MEDIA-IND #C-DGTZE-RQST-LOG-DTE-TME #R-WORK-PRCSS-ID #R-DGTZE-IND #R-MJ-PULL-IND #R-DGTZE-REGION-IND #R-DGTZE-BRANCH-IND #R-DGTZE-SPECIAL-NEEDS-IND #U-UNIT-CDE #U-UNIT-BLDG-NBR-BRNCH-LOC
            pnd_Vars_Pnd_M_Rqst_Log_Index_Dte.reset();
            pnd_Vars_Pnd_M_Work_Prcss_Id.reset();
            pnd_Vars_Pnd_M_Mj_Pull_Ind.reset();
            pnd_Vars_Pnd_M_Rqst_Rgn_Cde.reset();
            pnd_Vars_Pnd_M_Owner_Unit_Cde.reset();
            pnd_Vars_Pnd_C_Cabinet_Id.reset();
            pnd_Vars_Pnd_C_Media_Ind.reset();
            pnd_Vars_Pnd_C_Dgtze_Rqst_Log_Dte_Tme.reset();
            pnd_Vars_Pnd_R_Work_Prcss_Id.reset();
            pnd_Vars_Pnd_R_Dgtze_Ind.reset();
            pnd_Vars_Pnd_R_Mj_Pull_Ind.reset();
            pnd_Vars_Pnd_R_Dgtze_Region_Ind.reset();
            pnd_Vars_Pnd_R_Dgtze_Branch_Ind.reset();
            pnd_Vars_Pnd_R_Dgtze_Special_Needs_Ind.reset();
            pnd_Vars_Pnd_U_Unit_Cde.reset();
            pnd_Vars_Pnd_U_Unit_Bldg_Nbr_Brnch_Loc.reset();
            if (condition(! (pnd_Vars_Pnd_Rec_Processed.getBoolean())))                                                                                                   //Natural: IF NOT #REC-PROCESSED
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-EFM-CABINET-RECORD
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-WPID-ROUTING-TBL
            getSort().writeSortInData(cwf_Master_Work_Prcss_Id, cwf_Master_Pin_Nbr);                                                                                      //Natural: END-ALL
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getSort().sortData(cwf_Master_Work_Prcss_Id);                                                                                                                     //Natural: SORT BY CWF-MASTER.WORK-PRCSS-ID USING PIN-NBR
        SORT01:
        while (condition(getSort().readSortOutData(cwf_Master_Work_Prcss_Id, cwf_Master_Pin_Nbr)))
        {
            pnd_Vars_Pnd_Sort_Tot.nadd(1);                                                                                                                                //Natural: ADD 1 TO #SORT-TOT
            if (condition(pnd_Vars_Pnd_Sort_Tot.equals(1)))                                                                                                               //Natural: IF #SORT-TOT = 1
            {
                pnd_Vars_Pnd_Hold_Wpid.setValue(cwf_Master_Work_Prcss_Id);                                                                                                //Natural: MOVE CWF-MASTER.WORK-PRCSS-ID TO #HOLD-WPID
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Vars_Pnd_Hold_Wpid.notEquals(cwf_Master_Work_Prcss_Id)))                                                                                    //Natural: IF #HOLD-WPID NE CWF-MASTER.WORK-PRCSS-ID
            {
                getReports().write(2, ReportOption.NOTITLE,"Total count for WPID ",pnd_Vars_Pnd_Hold_Wpid," = ",pnd_Vars_Pnd_Sort_Cnt,NEWLINE);                           //Natural: WRITE ( 2 ) 'Total count for WPID ' #HOLD-WPID ' = ' #SORT-CNT /
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Vars_Pnd_Hold_Wpid.setValue(cwf_Master_Work_Prcss_Id);                                                                                                //Natural: MOVE CWF-MASTER.WORK-PRCSS-ID TO #HOLD-WPID
                pnd_Vars_Pnd_Sort_Cnt.reset();                                                                                                                            //Natural: RESET #SORT-CNT
            }                                                                                                                                                             //Natural: END-IF
            pnd_Vars_Pnd_Sort_Cnt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #SORT-CNT
        }                                                                                                                                                                 //Natural: END-SORT
        endSort();
        getReports().write(2, ReportOption.NOTITLE,"Total count for WPID ",pnd_Vars_Pnd_Hold_Wpid," = ",pnd_Vars_Pnd_Sort_Cnt,NEWLINE,NEWLINE,NEWLINE);                   //Natural: WRITE ( 2 ) 'Total count for WPID ' #HOLD-WPID ' = ' #SORT-CNT ///
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"Total MIT processed             ",pnd_Vars_Pnd_Sort_Tot);                                                             //Natural: WRITE ( 2 ) 'Total MIT processed             ' #SORT-TOT
        if (Global.isEscape()) return;
        F1:                                                                                                                                                               //Natural: GET CWF-SUPPORT-TBL #SAVE-ISN
        vw_cwf_Support_Tbl.readByID(pnd_Vars_Pnd_Save_Isn.getLong(), "F1");
        cwf_Support_Tbl_Pnd_Tbl_Dte.setValue(pnd_Vars_Pnd_End_Dte_Proc);                                                                                                  //Natural: MOVE #END-DTE-PROC TO #TBL-DTE
        cwf_Support_Tbl_Pnd_Tbl_Tme.setValue("0000000");                                                                                                                  //Natural: MOVE '0000000' TO #TBL-TME
        vw_cwf_Support_Tbl.updateDBRow("F1");                                                                                                                             //Natural: UPDATE ( F1. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        pnd_Vars_Pnd_Perc_All_Rqst.compute(new ComputeParameters(true, pnd_Vars_Pnd_Perc_All_Rqst), (pnd_Vars_Pnd_Hist_Doc_Cnt.multiply(100)).divide(pnd_Vars_Pnd_Mit_Tot_Cnt)); //Natural: COMPUTE ROUNDED #PERC-ALL-RQST = ( #HIST-DOC-CNT * 100 ) / #MIT-TOT-CNT
        pnd_Vars_Pnd_Perc_Catg_1.compute(new ComputeParameters(true, pnd_Vars_Pnd_Perc_Catg_1), (pnd_Vars_Pnd_Category_1_Cnt.multiply(100)).divide(pnd_Vars_Pnd_Hist_Doc_Cnt)); //Natural: COMPUTE ROUNDED #PERC-CATG-1 = ( #CATEGORY-1-CNT * 100 ) / #HIST-DOC-CNT
        pnd_Vars_Pnd_Perc_Catg_2.compute(new ComputeParameters(true, pnd_Vars_Pnd_Perc_Catg_2), (pnd_Vars_Pnd_Category_2_Cnt.multiply(100)).divide(pnd_Vars_Pnd_Hist_Doc_Cnt)); //Natural: COMPUTE ROUNDED #PERC-CATG-2 = ( #CATEGORY-2-CNT * 100 ) / #HIST-DOC-CNT
        pnd_Vars_Pnd_Perc_Catg_3.compute(new ComputeParameters(true, pnd_Vars_Pnd_Perc_Catg_3), (pnd_Vars_Pnd_Category_3_Cnt.multiply(100)).divide(pnd_Vars_Pnd_Hist_Doc_Cnt)); //Natural: COMPUTE ROUNDED #PERC-CATG-3 = ( #CATEGORY-3-CNT * 100 ) / #HIST-DOC-CNT
        pnd_Vars_Pnd_Per_No_Catg.compute(new ComputeParameters(true, pnd_Vars_Pnd_Per_No_Catg), (pnd_Vars_Pnd_Not_Catgry_Cnt.multiply(100)).divide(pnd_Vars_Pnd_Hist_Doc_Cnt)); //Natural: COMPUTE ROUNDED #PER-NO-CATG = ( #NOT-CATGRY-CNT * 100 ) / #HIST-DOC-CNT
        pnd_Vars_Pnd_Tot_Match.compute(new ComputeParameters(false, pnd_Vars_Pnd_Tot_Match), pnd_Vars_Pnd_Cab_Match.subtract(pnd_Vars_Pnd_Routing_Match));                //Natural: COMPUTE #TOT-MATCH = #CAB-MATCH - #ROUTING-MATCH
        pnd_Vars_Pnd_Fld_A.setValue(DbsUtil.compress(pnd_Vars_Pnd_Hist_Doc_Cnt, "* 100 /", pnd_Vars_Pnd_Mit_Tot_Cnt, ")"));                                               //Natural: COMPRESS #HIST-DOC-CNT '* 100 /' #MIT-TOT-CNT ')' INTO #FLD-A LEAVING
        pnd_Vars_Pnd_Fld_1.setValue(DbsUtil.compress(pnd_Vars_Pnd_Category_1_Cnt, "* 100 /", pnd_Vars_Pnd_Hist_Doc_Cnt, ")"));                                            //Natural: COMPRESS #CATEGORY-1-CNT'* 100 /' #HIST-DOC-CNT')' INTO #FLD-1 LEAVING
        pnd_Vars_Pnd_Fld_2.setValue(DbsUtil.compress(pnd_Vars_Pnd_Category_2_Cnt, "* 100 /", pnd_Vars_Pnd_Hist_Doc_Cnt, ")"));                                            //Natural: COMPRESS #CATEGORY-2-CNT'* 100 /' #HIST-DOC-CNT')' INTO #FLD-2 LEAVING
        pnd_Vars_Pnd_Fld_3.setValue(DbsUtil.compress(pnd_Vars_Pnd_Category_3_Cnt, "* 100 /", pnd_Vars_Pnd_Hist_Doc_Cnt, ")"));                                            //Natural: COMPRESS #CATEGORY-3-CNT'* 100 /' #HIST-DOC-CNT')' INTO #FLD-3 LEAVING
        pnd_Vars_Pnd_Fld_4.setValue(DbsUtil.compress(pnd_Vars_Pnd_Not_Catgry_Cnt, "* 100 /", pnd_Vars_Pnd_Hist_Doc_Cnt, ")"));                                            //Natural: COMPRESS #NOT-CATGRY-CNT'* 100 /' #HIST-DOC-CNT')' INTO #FLD-4 LEAVING
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,"Total Work Rqst LOGGED WITHIN PERIOD          ",pnd_Vars_Pnd_Mit_Tot_Cnt,NEWLINE,NEWLINE,             //Natural: WRITE ( 2 ) // 'Total Work Rqst LOGGED WITHIN PERIOD          ' #MIT-TOT-CNT // 'Total Work Rqst REQUIRING HISTORICAL DOCUMENTS' #HIST-DOC-CNT '   '#PERC-ALL-RQST'percent of Rqst logged            = ('#FLD-A // 'Total Work Rqst REQUIRING MICRJCKT TO BE PULLED ' #CATEGORY-1-CNT ' '#PERC-CATG-1'percent of historical Dcmnts Rqst = ('#FLD-1 / 'Total Work Rqst WITH ELECTRONIC HISTORICAL DCMNT' #CATEGORY-2-CNT ' '#PERC-CATG-2'percent of historical Dcmnts Rqst = ('#FLD-2 / 'Total Work Rqst REQUIRING MICJKT TO BE DIGITIZED' #CATEGORY-3-CNT ' '#PERC-CATG-3'percent of historical Dcmnts Rqst = ('#FLD-3 / 'Total Work Rqst NOT CATEGORIZED                 ' #NOT-CATGRY-CNT ' '#PER-NO-CATG'percent of historical Dcmnts Rqst = ('#FLD-4 /
            "Total Work Rqst REQUIRING HISTORICAL DOCUMENTS",pnd_Vars_Pnd_Hist_Doc_Cnt,"   ",pnd_Vars_Pnd_Perc_All_Rqst,"percent of Rqst logged            = (",
            pnd_Vars_Pnd_Fld_A,NEWLINE,NEWLINE,"Total Work Rqst REQUIRING MICRJCKT TO BE PULLED ",pnd_Vars_Pnd_Category_1_Cnt," ",pnd_Vars_Pnd_Perc_Catg_1,
            "percent of historical Dcmnts Rqst = (",pnd_Vars_Pnd_Fld_1,NEWLINE,"Total Work Rqst WITH ELECTRONIC HISTORICAL DCMNT",pnd_Vars_Pnd_Category_2_Cnt,
            " ",pnd_Vars_Pnd_Perc_Catg_2,"percent of historical Dcmnts Rqst = (",pnd_Vars_Pnd_Fld_2,NEWLINE,"Total Work Rqst REQUIRING MICJKT TO BE DIGITIZED",
            pnd_Vars_Pnd_Category_3_Cnt," ",pnd_Vars_Pnd_Perc_Catg_3,"percent of historical Dcmnts Rqst = (",pnd_Vars_Pnd_Fld_3,NEWLINE,"Total Work Rqst NOT CATEGORIZED                 ",
            pnd_Vars_Pnd_Not_Catgry_Cnt," ",pnd_Vars_Pnd_Per_No_Catg,"percent of historical Dcmnts Rqst = (",pnd_Vars_Pnd_Fld_4,NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,NEWLINE,"Total CABINET read      ",pnd_Vars_Pnd_Read_Cabinet_Cnt,NEWLINE,"Total matching              ",pnd_Vars_Pnd_Tot_Match,     //Natural: WRITE // 'Total CABINET read      ' #READ-CABINET-CNT / 'Total matching              ' #TOT-MATCH / 'Total not matching          ' #CAB-NOT-MATCH // 'Total ROUTING read      ' #READ-ROUTING-CNT / 'Total matching              ' #ROUTING-MATCH / 'Total not matching          ' #NO-ROUTING-MATCH //// 'Total not matching UNIT TBL FILE ' #UNIT-TBL-NF-CNT
            NEWLINE,"Total not matching          ",pnd_Vars_Pnd_Cab_Not_Match,NEWLINE,NEWLINE,"Total ROUTING read      ",pnd_Vars_Pnd_Read_Routing_Cnt,NEWLINE,
            "Total matching              ",pnd_Vars_Pnd_Routing_Match,NEWLINE,"Total not matching          ",pnd_Vars_Pnd_No_Routing_Match,NEWLINE,NEWLINE,
            NEWLINE,NEWLINE,"Total not matching UNIT TBL FILE ",pnd_Vars_Pnd_Unit_Tbl_Nf_Cnt);
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"Total MIT written ",pnd_Vars_Pnd_Sort_Tot);                                                           //Natural: WRITE ( 1 ) // 'Total MIT written ' #SORT-TOT
        if (Global.isEscape()) return;
    }                                                                                                                                                                     //Natural: ON ERROR
    private void sub_Read_Efm_Cabinet_Record() throws Exception                                                                                                           //Natural: READ-EFM-CABINET-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        vw_cwf_Efm_Cabinet.startDatabaseFind                                                                                                                              //Natural: FIND ( 1 ) CWF-EFM-CABINET WITH CABINET-ID = #EFM-PIN-NBR-A
        (
        "FIND02",
        new Wc[] { new Wc("CABINET_ID", "=", pnd_Vars_Pnd_Efm_Pin_Nbr_A, WcType.WITH) },
        1
        );
        FIND02:
        while (condition(vw_cwf_Efm_Cabinet.readNextRow("FIND02", true)))
        {
            vw_cwf_Efm_Cabinet.setIfNotFoundControlFlag(false);
            if (condition(vw_cwf_Efm_Cabinet.getAstCOUNTER().equals(0)))                                                                                                  //Natural: IF NO RECORDS FOUND
            {
                pnd_Vars_Pnd_Cab_Not_Match.nadd(1);                                                                                                                       //Natural: ADD 1 TO #CAB-NOT-MATCH
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Vars_Pnd_Rec_Processed.setValue(true);                                                                                                                    //Natural: MOVE TRUE TO #REC-PROCESSED
            pnd_Vars_Pnd_C_Cabinet_Id.setValue(cwf_Efm_Cabinet_Cabinet_Id);                                                                                               //Natural: MOVE CABINET-ID TO #C-CABINET-ID
            pnd_Vars_Pnd_C_Media_Ind.setValue(cwf_Efm_Cabinet_Media_Ind);                                                                                                 //Natural: MOVE MEDIA-IND TO #C-MEDIA-IND
            pnd_Vars_Pnd_C_Dgtze_Rqst_Log_Dte_Tme.setValue(cwf_Efm_Cabinet_Dgtze_Rqst_Log_Dte_Tme);                                                                       //Natural: MOVE DGTZE-RQST-LOG-DTE-TME TO #C-DGTZE-RQST-LOG-DTE-TME
            //*  PIN-EXP
            //*  PIN-EXP
            getReports().display(1, "PIN #",                                                                                                                              //Natural: DISPLAY ( 1 ) 'PIN #' #M-PIN-NBR 'RQST-DTE' #M-RQST-LOG-INDEX-DTE 'WPID' #M-WORK-PRCSS-ID 'PI' #M-MJ-PULL-IND 'RRC' #M-RQST-RGN-CDE 'OUC' #M-OWNER-UNIT-CDE '*CF' 'CAB-ID' #C-CABINET-ID 'MI' #C-MEDIA-IND 'DRDT' #C-DGTZE-RQST-LOG-DTE-TME '*RF' 'WPID' #R-WORK-PRCSS-ID 'DI' #R-DGTZE-IND 'PI' #R-MJ-PULL-IND 'DRI' #R-DGTZE-REGION-IND 'DBI' #R-DGTZE-BRANCH-IND 'DSNI' #R-DGTZE-SPECIAL-NEEDS-IND '*UF' 'UC' #U-UNIT-CDE 'UBN' #U-UNIT-BLDG-NBR-BRNCH-LOC
            		pnd_Vars_Pnd_M_Pin_Nbr,"RQST-DTE",
            		pnd_Vars_Pnd_M_Rqst_Log_Index_Dte,"WPID",
            		pnd_Vars_Pnd_M_Work_Prcss_Id,"PI",
            		pnd_Vars_Pnd_M_Mj_Pull_Ind,"RRC",
            		pnd_Vars_Pnd_M_Rqst_Rgn_Cde,"OUC",
            		pnd_Vars_Pnd_M_Owner_Unit_Cde,"*CF",
            		"CAB-ID",
            		pnd_Vars_Pnd_C_Cabinet_Id,"MI",
            		pnd_Vars_Pnd_C_Media_Ind,"DRDT",
            		pnd_Vars_Pnd_C_Dgtze_Rqst_Log_Dte_Tme,"*RF",
            		"WPID",
            		pnd_Vars_Pnd_R_Work_Prcss_Id,"DI",
            		pnd_Vars_Pnd_R_Dgtze_Ind,"PI",
            		pnd_Vars_Pnd_R_Mj_Pull_Ind,"DRI",
            		pnd_Vars_Pnd_R_Dgtze_Region_Ind,"DBI",
            		pnd_Vars_Pnd_R_Dgtze_Branch_Ind,"DSNI",
            		pnd_Vars_Pnd_R_Dgtze_Special_Needs_Ind,"*UF",
            		"UC",
            		pnd_Vars_Pnd_U_Unit_Cde,"UBN",
            		pnd_Vars_Pnd_U_Unit_Bldg_Nbr_Brnch_Loc);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Vars_Pnd_Cab_Match.nadd(1);                                                                                                                               //Natural: ADD 1 TO #CAB-MATCH
            pnd_Vars_Pnd_Hist_Doc_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #HIST-DOC-CNT
            if (condition(cwf_Efm_Cabinet_Media_Ind.equals(" ")))                                                                                                         //Natural: IF MEDIA-IND = ' '
            {
                pnd_Vars_Pnd_Act_Microjacket_Cnt_1.nadd(1);                                                                                                               //Natural: ADD 1 TO #ACT-MICROJACKET-CNT-1
                pnd_Vars_Pnd_Category_1_Cnt.nadd(1);                                                                                                                      //Natural: ADD 1 TO #CATEGORY-1-CNT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(cwf_Efm_Cabinet_Media_Ind.equals("2")))                                                                                                     //Natural: IF MEDIA-IND = '2'
                {
                    pnd_Vars_Pnd_Frz_Microjacket_Cnt_1.nadd(1);                                                                                                           //Natural: ADD 1 TO #FRZ-MICROJACKET-CNT-1
                    pnd_Vars_Pnd_Category_1_Cnt.nadd(1);                                                                                                                  //Natural: ADD 1 TO #CATEGORY-1-CNT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(cwf_Efm_Cabinet_Media_Ind.equals("3")))                                                                                                 //Natural: IF MEDIA-IND = '3'
                    {
                        if (condition(cwf_Efm_Cabinet_Dgtze_Rqst_Log_Dte_Tme.less(cwf_Support_Tbl_Tbl_Rpt_Date_Tme)))                                                     //Natural: IF DGTZE-RQST-LOG-DTE-TME < TBL-RPT-DATE-TME
                        {
                            pnd_Vars_Pnd_Dig_Microjacket_Cnt_2.nadd(1);                                                                                                   //Natural: ADD 1 TO #DIG-MICROJACKET-CNT-2
                            pnd_Vars_Pnd_Category_2_Cnt.nadd(1);                                                                                                          //Natural: ADD 1 TO #CATEGORY-2-CNT
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Vars_Pnd_Need_To_Be_Dig_Cnt_3.nadd(1);                                                                                                    //Natural: ADD 1 TO #NEED-TO-BE-DIG-CNT-3
                            pnd_Vars_Pnd_Category_3_Cnt.nadd(1);                                                                                                          //Natural: ADD 1 TO #CATEGORY-3-CNT
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(cwf_Efm_Cabinet_Media_Ind.equals("4")))                                                                                             //Natural: IF MEDIA-IND = '4'
                        {
                            pnd_Vars_Pnd_Not_Microjacket_Cnt_2.nadd(1);                                                                                                   //Natural: ADD 1 TO #NOT-MICROJACKET-CNT-2
                            pnd_Vars_Pnd_Category_2_Cnt.nadd(1);                                                                                                          //Natural: ADD 1 TO #CATEGORY-2-CNT
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(cwf_Efm_Cabinet_Media_Ind.equals("5")))                                                                                         //Natural: IF MEDIA-IND = '5'
                            {
                                pnd_Vars_Pnd_Awt_Microjacket_Cnt_3.nadd(1);                                                                                               //Natural: ADD 1 TO #AWT-MICROJACKET-CNT-3
                                pnd_Vars_Pnd_Category_3_Cnt.nadd(1);                                                                                                      //Natural: ADD 1 TO #CATEGORY-3-CNT
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Vars_Pnd_Not_Catgry_Cnt.nadd(1);                                                                                                      //Natural: ADD 1 TO #NOT-CATGRY-CNT
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Read_Wpid_Routing_Tbl() throws Exception                                                                                                             //Natural: READ-WPID-ROUTING-TBL
    {
        if (BLNatReinput.isReinput()) return;

        vw_cwf_Routing_Tbl.startDatabaseFind                                                                                                                              //Natural: FIND ( 1 ) CWF-ROUTING-TBL WITH WPID-UNIQ-KEY = #WPID-UNIQ-KEY
        (
        "FIND03",
        new Wc[] { new Wc("WPID_UNIQ_KEY", "=", pnd_Wpid_Uniq_Key, WcType.WITH) },
        1
        );
        FIND03:
        while (condition(vw_cwf_Routing_Tbl.readNextRow("FIND03", true)))
        {
            vw_cwf_Routing_Tbl.setIfNotFoundControlFlag(false);
            if (condition(vw_cwf_Routing_Tbl.getAstCOUNTER().equals(0)))                                                                                                  //Natural: IF NO RECORDS FOUND
            {
                pnd_Vars_Pnd_Routing_Nf_Cnt.nadd(1);                                                                                                                      //Natural: ADD 1 TO #ROUTING-NF-CNT
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Vars_Pnd_R_Work_Prcss_Id.setValue(cwf_Routing_Tbl_Work_Prcss_Id);                                                                                         //Natural: MOVE CWF-ROUTING-TBL.WORK-PRCSS-ID TO #R-WORK-PRCSS-ID
            pnd_Vars_Pnd_R_Dgtze_Ind.setValue(cwf_Routing_Tbl_Dgtze_Ind);                                                                                                 //Natural: MOVE DGTZE-IND TO #R-DGTZE-IND
            pnd_Vars_Pnd_R_Mj_Pull_Ind.setValue(cwf_Routing_Tbl_Mj_Pull_Ind);                                                                                             //Natural: MOVE CWF-ROUTING-TBL.MJ-PULL-IND TO #R-MJ-PULL-IND
            pnd_Vars_Pnd_R_Dgtze_Region_Ind.setValue(cwf_Routing_Tbl_Dgtze_Region_Ind);                                                                                   //Natural: MOVE DGTZE-REGION-IND TO #R-DGTZE-REGION-IND
            pnd_Vars_Pnd_R_Dgtze_Branch_Ind.setValue(cwf_Routing_Tbl_Dgtze_Branch_Ind);                                                                                   //Natural: MOVE DGTZE-BRANCH-IND TO #R-DGTZE-BRANCH-IND
            pnd_Vars_Pnd_R_Dgtze_Special_Needs_Ind.setValue(cwf_Routing_Tbl_Dgtze_Special_Needs_Ind);                                                                     //Natural: MOVE DGTZE-SPECIAL-NEEDS-IND TO #R-DGTZE-SPECIAL-NEEDS-IND
            vw_cwf_Org_Unit_Tbl.startDatabaseRead                                                                                                                         //Natural: READ ( 1 ) CWF-ORG-UNIT-TBL BY UNIQ-UNIT-CDE FROM CWF-MASTER.OWNER-UNIT-CDE
            (
            "READ_UNIT",
            new Wc[] { new Wc("UNIQ_UNIT_CDE", ">=", cwf_Master_Owner_Unit_Cde, WcType.BY) },
            new Oc[] { new Oc("UNIQ_UNIT_CDE", "ASC") },
            1
            );
            READ_UNIT:
            while (condition(vw_cwf_Org_Unit_Tbl.readNextRow("READ_UNIT")))
            {
                if (condition(cwf_Org_Unit_Tbl_Unit_Cde.notEquals(cwf_Master_Owner_Unit_Cde)))                                                                            //Natural: IF CWF-ORG-UNIT-TBL.UNIT-CDE NE CWF-MASTER.OWNER-UNIT-CDE
                {
                    pnd_Vars_Pnd_Unit_Tbl_Nf_Cnt.nadd(1);                                                                                                                 //Natural: ADD 1 TO #UNIT-TBL-NF-CNT
                    if (true) break READ_UNIT;                                                                                                                            //Natural: ESCAPE BOTTOM ( READ-UNIT. )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Vars_Pnd_U_Unit_Cde.setValue(cwf_Org_Unit_Tbl_Unit_Cde);                                                                                          //Natural: MOVE UNIT-CDE TO #U-UNIT-CDE
                    pnd_Vars_Pnd_U_Unit_Bldg_Nbr_Brnch_Loc.setValue(cwf_Org_Unit_Tbl_Unit_Bldg_Nbr_Brnch_Loc);                                                            //Natural: MOVE UNIT-BLDG-NBR-BRNCH-LOC TO #U-UNIT-BLDG-NBR-BRNCH-LOC
                    if (condition((((cwf_Routing_Tbl_Mj_Pull_Ind.equals("Y") || cwf_Routing_Tbl_Mj_Pull_Ind.equals("J")) || cwf_Routing_Tbl_Mj_Pull_Ind.equals("R"))      //Natural: IF ( CWF-ROUTING-TBL.MJ-PULL-IND = 'Y' OR = 'J' OR = 'R' ) AND ( UNIT-BLDG-NBR-BRNCH-LOC NE '730' AND UNIT-BLDG-NBR-BRNCH-LOC NE '750' AND UNIT-BLDG-NBR-BRNCH-LOC NE '485' )
                        && ((cwf_Org_Unit_Tbl_Unit_Bldg_Nbr_Brnch_Loc.notEquals("730") && cwf_Org_Unit_Tbl_Unit_Bldg_Nbr_Brnch_Loc.notEquals("750")) && 
                        cwf_Org_Unit_Tbl_Unit_Bldg_Nbr_Brnch_Loc.notEquals("485")))))
                    {
                        pnd_Vars_Pnd_Routing_Match.nadd(1);                                                                                                               //Natural: ADD 1 TO #ROUTING-MATCH
                        //*                MOVE CWF-MASTER.PIN-NBR TO #EFM-PIN-NBR     /* PIN-EXP
                        //*  PIN-EXP
                        pnd_Vars_Pnd_Efm_Pin_Nbr.moveAll(cwf_Master_Pin_Nbr);                                                                                             //Natural: MOVE ALL CWF-MASTER.PIN-NBR TO #EFM-PIN-NBR
                                                                                                                                                                          //Natural: PERFORM READ-EFM-CABINET-RECORD
                        sub_Read_Efm_Cabinet_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_UNIT"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_UNIT"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition((cwf_Routing_Tbl_Dgtze_Ind.equals("Y") && cwf_Routing_Tbl_Dgtze_Region_Ind.equals(cwf_Master_Rqst_Rgn_Cde)) || (cwf_Routing_Tbl_Dgtze_Ind.equals("Y")  //Natural: IF ( DGTZE-IND = 'Y' AND DGTZE-REGION-IND = CWF-MASTER.RQST-RGN-CDE ) OR ( DGTZE-IND = 'Y' AND DGTZE-REGION-IND = ' ' AND DGTZE-BRANCH-IND = ' ' AND DGTZE-SPECIAL-NEEDS-IND = ' ' )
                && cwf_Routing_Tbl_Dgtze_Region_Ind.equals(" ") && cwf_Routing_Tbl_Dgtze_Branch_Ind.equals(" ") && cwf_Routing_Tbl_Dgtze_Special_Needs_Ind.equals(" "))))
            {
                pnd_Vars_Pnd_Routing_Match.nadd(1);                                                                                                                       //Natural: ADD 1 TO #ROUTING-MATCH
                //*    MOVE CWF-MASTER.PIN-NBR TO #EFM-PIN-NBR      /* PIN-EXP
                //*  PIN-EXP
                pnd_Vars_Pnd_Efm_Pin_Nbr.moveAll(cwf_Master_Pin_Nbr);                                                                                                     //Natural: MOVE ALL CWF-MASTER.PIN-NBR TO #EFM-PIN-NBR
                                                                                                                                                                          //Natural: PERFORM READ-EFM-CABINET-RECORD
                sub_Read_Efm_Cabinet_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Vars_Pnd_No_Routing_Match.nadd(1);                                                                                                                    //Natural: ADD 1 TO #NO-ROUTING-MATCH
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //*  PIN-EXP
                    //*  PIN-EXP
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,"PROGRAM",Global.getPROGRAM(),new TabSetting(20),"Record Support Services - Work Requests Requiring Historical",new  //Natural: WRITE ( 1 ) NOTITLE NOHDR 'PROGRAM' *PROGRAM 020T 'Record Support Services - Work Requests Requiring Historical' 081T 'Documents Review' 115T 'DATE:' *DATX / 035T 'For the period starting from ' #START-HDR-DTE ( EM = MM/DD/YY ) 'TO' #HDR-DTE ( EM = MM/DD/YY ) 115T 'TIME:' *TIME / 030T 'MJ' 098T 'MJ' 115T 'PAGE:' *PAGE-NUMBER ( 1 ) /
                        TabSetting(81),"Documents Review",new TabSetting(115),"DATE:",Global.getDATX(),NEWLINE,new TabSetting(35),"For the period starting from ",pnd_Vars_Pnd_Start_Hdr_Dte, 
                        new ReportEditMask ("MM/DD/YY"),"TO",pnd_Vars_Pnd_Hdr_Dte, new ReportEditMask ("MM/DD/YY"),new TabSetting(115),"TIME:",Global.getTIME(),NEWLINE,new 
                        TabSetting(30),"MJ",new TabSetting(98),"MJ",new TabSetting(115),"PAGE:",getReports().getPageNumberDbs(1),NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,"PROGRAM",Global.getPROGRAM(),new TabSetting(20),"Record Support Services - Work Requests Requiring Historical",new  //Natural: WRITE ( 2 ) NOTITLE NOHDR 'PROGRAM' *PROGRAM 020T 'Record Support Services - Work Requests Requiring Historical' 081T 'Documents Review' 115T 'DATE:' *DATX / 030T 'Sorted by WPID - For the period starting from ' #START-HDR-DTE ( EM = MM/DD/YY ) 'TO' #HDR-DTE ( EM = MM/DD/YY ) 115T 'TIME:' *TIME / 115T 'PAGE:' *PAGE-NUMBER ( 2 ) /
                        TabSetting(81),"Documents Review",new TabSetting(115),"DATE:",Global.getDATX(),NEWLINE,new TabSetting(30),"Sorted by WPID - For the period starting from ",pnd_Vars_Pnd_Start_Hdr_Dte, 
                        new ReportEditMask ("MM/DD/YY"),"TO",pnd_Vars_Pnd_Hdr_Dte, new ReportEditMask ("MM/DD/YY"),new TabSetting(115),"TIME:",Global.getTIME(),NEWLINE,new 
                        TabSetting(115),"PAGE:",getReports().getPageNumberDbs(2),NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, "NATURAL ERROR",Global.getERROR_NR(),"ON LINE",Global.getERROR_LINE(),"OF",Global.getPROGRAM());                                            //Natural: WRITE 'NATURAL ERROR' *ERROR-NR 'ON LINE' *ERROR-LINE 'OF' *PROGRAM
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=60 ES=OFF");
        Global.format(2, "LS=132 PS=60 ES=OFF");

        getReports().setDisplayColumns(1, "PIN #",
        		pnd_Vars_Pnd_M_Pin_Nbr,"RQST-DTE",
        		pnd_Vars_Pnd_M_Rqst_Log_Index_Dte,"WPID",
        		pnd_Vars_Pnd_M_Work_Prcss_Id,"PI",
        		pnd_Vars_Pnd_M_Mj_Pull_Ind,"RRC",
        		pnd_Vars_Pnd_M_Rqst_Rgn_Cde,"OUC",
        		pnd_Vars_Pnd_M_Owner_Unit_Cde,"*CF",
        		"CAB-ID",
        		pnd_Vars_Pnd_C_Cabinet_Id,"MI",
        		pnd_Vars_Pnd_C_Media_Ind,"DRDT",
        		pnd_Vars_Pnd_C_Dgtze_Rqst_Log_Dte_Tme,"*RF",
        		"WPID",
        		pnd_Vars_Pnd_R_Work_Prcss_Id,"DI",
        		pnd_Vars_Pnd_R_Dgtze_Ind,"PI",
        		pnd_Vars_Pnd_R_Mj_Pull_Ind,"DRI",
        		pnd_Vars_Pnd_R_Dgtze_Region_Ind,"DBI",
        		pnd_Vars_Pnd_R_Dgtze_Branch_Ind,"DSNI",
        		pnd_Vars_Pnd_R_Dgtze_Special_Needs_Ind,"*UF",
        		"UC",
        		pnd_Vars_Pnd_U_Unit_Cde,"UBN",
        		pnd_Vars_Pnd_U_Unit_Bldg_Nbr_Brnch_Loc);
    }
}
