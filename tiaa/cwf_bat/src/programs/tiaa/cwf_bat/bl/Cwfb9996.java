/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:47:50 PM
**        * FROM NATURAL PROGRAM : Cwfb9996
************************************************************
**        * FILE NAME            : Cwfb9996.java
**        * CLASS NAME           : Cwfb9996
**        * INSTANCE NAME        : Cwfb9996
************************************************************
************************************************************************
* SYSTEM   : CORPORATE WORKFLOW
* PROGRAM  : CWFB9996
* TITLE    : CWF ADHOC PROGRAM
* FUNCTION : CWF ADHOC PROGRAM TO EXECUTE A NATURAL PROGRAM STORED IN
*          : THE FILE 'CWF-SUPPORT-TBL'; TABLE - 'CWF-ADHOC-PARM'
*          : IT CAN PASS UPTO 3 PARMS TO THE TRIGGERED PROGRAM EACH OF
*          : SIZE 63 BYTES
* ADABAS   : FILE 'CWF-SUPPORT-TBL'; TABLE - 'CWF-ADHOC-PARM'
* JOB      : PCW9996R
* MOBIUS   : CW9996AR
* HISTORY  : CREATED ON AUG 12 2014 - VINODH KUMAR RAMACHANDRAN
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb9996 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_1;
    private DbsField cwf_Support_Tbl_Pnd_Tbl_Pgm;

    private DbsGroup cwf_Support_Tbl__R_Field_2;
    private DbsField cwf_Support_Tbl_Pnd_Tbl_Pgm_A8;
    private DbsField cwf_Support_Tbl_Pnd_Tbl_Parm1;
    private DbsField cwf_Support_Tbl_Pnd_Tbl_Parm2;
    private DbsField cwf_Support_Tbl_Pnd_Tbl_Parm3;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_3;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Support_Tbl__R_Field_1 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_1", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Pnd_Tbl_Pgm = cwf_Support_Tbl__R_Field_1.newFieldInGroup("cwf_Support_Tbl_Pnd_Tbl_Pgm", "#TBL-PGM", FieldType.STRING, 63);

        cwf_Support_Tbl__R_Field_2 = cwf_Support_Tbl__R_Field_1.newGroupInGroup("cwf_Support_Tbl__R_Field_2", "REDEFINE", cwf_Support_Tbl_Pnd_Tbl_Pgm);
        cwf_Support_Tbl_Pnd_Tbl_Pgm_A8 = cwf_Support_Tbl__R_Field_2.newFieldInGroup("cwf_Support_Tbl_Pnd_Tbl_Pgm_A8", "#TBL-PGM-A8", FieldType.STRING, 
            8);
        cwf_Support_Tbl_Pnd_Tbl_Parm1 = cwf_Support_Tbl__R_Field_1.newFieldInGroup("cwf_Support_Tbl_Pnd_Tbl_Parm1", "#TBL-PARM1", FieldType.STRING, 63);
        cwf_Support_Tbl_Pnd_Tbl_Parm2 = cwf_Support_Tbl__R_Field_1.newFieldInGroup("cwf_Support_Tbl_Pnd_Tbl_Parm2", "#TBL-PARM2", FieldType.STRING, 63);
        cwf_Support_Tbl_Pnd_Tbl_Parm3 = cwf_Support_Tbl__R_Field_1.newFieldInGroup("cwf_Support_Tbl_Pnd_Tbl_Parm3", "#TBL-PARM3", FieldType.STRING, 63);
        registerRecord(vw_cwf_Support_Tbl);

        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_3", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Support_Tbl.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb9996() throws Exception
    {
        super("Cwfb9996");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        //* *----------------------------------------------------------------------
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind.setValue("A");                                                                                                          //Natural: ASSIGN #TBL-SCRTY-LEVEL-IND := 'A'
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme.setValue("CWF-ADHOC-PARM");                                                                                                   //Natural: ASSIGN #TBL-TABLE-NME := 'CWF-ADHOC-PARM'
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field.setValue(" ");                                                                                                                //Natural: ASSIGN #TBL-KEY-FIELD := ' '
        vw_cwf_Support_Tbl.startDatabaseRead                                                                                                                              //Natural: READ ( 1 ) CWF-SUPPORT-TBL BY TBL-PRIME-KEY STARTING FROM #TBL-PRIME-KEY
        (
        "READ01",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(vw_cwf_Support_Tbl.readNextRow("READ01")))
        {
            if (condition(cwf_Support_Tbl_Tbl_Scrty_Level_Ind.notEquals("A ") || cwf_Support_Tbl_Tbl_Table_Nme.notEquals("CWF-ADHOC-PARM")))                              //Natural: IF TBL-SCRTY-LEVEL-IND NE 'A ' OR TBL-TABLE-NME NE 'CWF-ADHOC-PARM'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            getReports().print(0, "** CWF ADHOC PROGRAM **",Global.getDATX(),Global.getTIMX(),NEWLINE,"PROGRAM ID:",cwf_Support_Tbl_Pnd_Tbl_Pgm_A8,NEWLINE,               //Natural: PRINT '** CWF ADHOC PROGRAM **' *DATX *TIMX / 'PROGRAM ID:' #TBL-PGM-A8 / 'PARM 1    :' #TBL-PARM1 / 'PARM 2    :' #TBL-PARM2 / 'PARM 3    :' #TBL-PARM3
                "PARM 1    :",cwf_Support_Tbl_Pnd_Tbl_Parm1,NEWLINE,"PARM 2    :",cwf_Support_Tbl_Pnd_Tbl_Parm2,NEWLINE,"PARM 3    :",cwf_Support_Tbl_Pnd_Tbl_Parm3);
            if (condition(cwf_Support_Tbl_Pnd_Tbl_Pgm_A8.notEquals(" ")))                                                                                                 //Natural: IF #TBL-PGM-A8 NE ' '
            {
                Global.getSTACK().pushData(StackOption.TOP, cwf_Support_Tbl_Pnd_Tbl_Parm1, cwf_Support_Tbl_Pnd_Tbl_Parm2, cwf_Support_Tbl_Pnd_Tbl_Parm3);                 //Natural: FETCH #TBL-PGM-A8 #TBL-PARM1 #TBL-PARM2 #TBL-PARM3
                Global.setFetchProgram(DbsUtil.getBlType(cwf_Support_Tbl_Pnd_Tbl_Pgm_A8));
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //
}
