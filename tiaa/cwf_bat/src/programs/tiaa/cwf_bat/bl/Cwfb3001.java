/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:26:12 PM
**        * FROM NATURAL PROGRAM : Cwfb3001
************************************************************
**        * FILE NAME            : Cwfb3001.java
**        * CLASS NAME           : Cwfb3001
**        * INSTANCE NAME        : Cwfb3001
************************************************************
**SAG GENERATOR: SHELL-TIAA                       VERSION: 3.2.2
**SAG TITLE: REPORT 1
**SAG SYSTEM: CRPCWF
************************************************************************
* PROGRAM  : CWFB3001
* SYSTEM   : CRPCWF
* TITLE    : REPORT 1
* GENERATED: AUG 11,93 AT 10:46 AM
* FUNCTION : REPORT OF WORK REQUEST LOGGED AND INDEXED
*          | BATCH REGULAR
*          |
*          |
*          |
*          |
*          |
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* MMM DD YY  ________ ______________________________________________
* 08/30/94   JEAN BUCKLE - ADDED PIN-NBR + PRIORITY TO EXTRACT RECORD.
* ______________________________________________
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3001 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Rep_Parm;
    private DbsField pnd_Rep_Parm_Pnd_Rep_Racf_Id;
    private DbsField pnd_Rep_Parm_Pnd_Rep_Start_Dd;

    private DbsGroup pnd_Rep_Parm__R_Field_1;
    private DbsField pnd_Rep_Parm_Pnd_Rep_Start_Dd_A;
    private DbsField pnd_Rep_Parm_Pnd_Rep_Start_Mm;

    private DbsGroup pnd_Rep_Parm__R_Field_2;
    private DbsField pnd_Rep_Parm_Pnd_Rep_Start_Mm_A;
    private DbsField pnd_Rep_Parm_Pnd_Rep_Start_Yy;

    private DbsGroup pnd_Rep_Parm__R_Field_3;
    private DbsField pnd_Rep_Parm_Pnd_Rep_Start_Yy_A;
    private DbsField pnd_Rep_Parm_Pnd_Rep_End_Dd;

    private DbsGroup pnd_Rep_Parm__R_Field_4;
    private DbsField pnd_Rep_Parm_Pnd_Rep_End_Dd_A;
    private DbsField pnd_Rep_Parm_Pnd_Rep_End_Mm;

    private DbsGroup pnd_Rep_Parm__R_Field_5;
    private DbsField pnd_Rep_Parm_Pnd_Rep_End_Mm_A;
    private DbsField pnd_Rep_Parm_Pnd_Rep_End_Yy;

    private DbsGroup pnd_Rep_Parm__R_Field_6;
    private DbsField pnd_Rep_Parm_Pnd_Rep_End_Yy_A;
    private DbsField pnd_Rep_Parm_Pnd_Rep_Unit_Cde;
    private DbsField pnd_Rep_Parm_Pnd_Start_Date;

    private DbsGroup pnd_Rep_Parm__R_Field_7;
    private DbsField pnd_Rep_Parm_Pnd_Start_Date_N;
    private DbsField pnd_Rep_Parm_Pnd_End_Date;

    private DbsGroup pnd_Rep_Parm__R_Field_8;
    private DbsField pnd_Rep_Parm_Pnd_End_Date_N;
    private DbsField pnd_Rep_Parm_Pnd_Work_Start_Date_A;

    private DbsGroup pnd_Rep_Parm__R_Field_9;
    private DbsField pnd_Rep_Parm_Pnd_Work_Mm;
    private DbsField pnd_Rep_Parm_Pnd_Filler1;
    private DbsField pnd_Rep_Parm_Pnd_Work_Dd;
    private DbsField pnd_Rep_Parm_Pnd_Filler2;
    private DbsField pnd_Rep_Parm_Pnd_Work_Yy;
    private DbsField pnd_Rep_Parm_Pnd_Work_End_Date_A;

    private DbsGroup pnd_Rep_Parm__R_Field_10;
    private DbsField pnd_Rep_Parm_Pnd_Work_Mmm;
    private DbsField pnd_Rep_Parm_Pnd_Fillera;
    private DbsField pnd_Rep_Parm_Pnd_Work_Ddd;
    private DbsField pnd_Rep_Parm_Pnd_Fillerb;
    private DbsField pnd_Rep_Parm_Pnd_Work_Yyy;

    private DbsGroup pnd_Local_Data;

    private DbsGroup pnd_Local_Data_Pnd_Work_Record;
    private DbsField pnd_Local_Data_Tbl_Wpid;

    private DbsGroup pnd_Local_Data__R_Field_11;
    private DbsField pnd_Local_Data_Tbl_Wpid_Action;
    private DbsField pnd_Local_Data_Tbl_Wpid_Lob;
    private DbsField pnd_Local_Data_Tbl_Wpid_Mbp;
    private DbsField pnd_Local_Data_Tbl_Wpid_Sbp;
    private DbsField pnd_Local_Data_Tbl_Wpid_Act;
    private DbsField pnd_Local_Data_Tbl_Unit;
    private DbsField pnd_Local_Data_Tbl_Racf_Id;
    private DbsField pnd_Local_Data_Tbl_Prty;
    private DbsField pnd_Local_Data_Tbl_Pin_Nbr;
    private DbsField pnd_Local_Data_Rush_Req_Ind;
    private DbsField pnd_Local_Data_Pnd_Work_Date_D;
    private DbsField pnd_Local_Data_Pnd_Rep_Wpid_Name;
    private DbsField pnd_Local_Data_Pnd_Rep_Empl_Name;
    private DbsField pnd_Local_Data_Pnd_Pad_Space;
    private DbsField pnd_Local_Data_Pnd_Wrk_Unit_Cde;

    private DbsGroup pnd_Local_Data__R_Field_12;
    private DbsField pnd_Local_Data_Pnd_Wrk_Unit_Id_Cde;
    private DbsField pnd_Local_Data_Pnd_Wrk_Unit_Suffix;
    private DbsField pnd_Local_Data_Pnd_Work_Prcss_Id;
    private DbsField pnd_Local_Data_Pnd_Wpid_Code;
    private DbsField pnd_Local_Data_Pnd_Wpid_Desc;
    private DbsField pnd_Local_Data_Pnd_Rep_Ctr;
    private DbsField pnd_Local_Data_Pnd_Max;
    private DbsField pnd_Local_Data_Pnd_Max_Less_1;
    private DbsField pnd_Local_Data_Pnd_I;
    private DbsField pnd_Local_Data_Pnd_J;
    private DbsField pnd_Local_Data_Pnd_K;
    private DbsField pnd_Local_Data_Pnd_Total_Logged;
    private DbsField pnd_Local_Data_Pnd_Wpid_Count;
    private DbsField pnd_Local_Data_Pnd_Unclear_Ctr;
    private DbsField pnd_Local_Data_Pnd_Total_Unclear;
    private DbsField pnd_Local_Data_Pnd_Booklet_Ctr;
    private DbsField pnd_Local_Data_Pnd_Forms_Ctr;
    private DbsField pnd_Local_Data_Pnd_Inquire_Ctr;
    private DbsField pnd_Local_Data_Pnd_Research_Ctr;
    private DbsField pnd_Local_Data_Pnd_Trans_Ctr;
    private DbsField pnd_Local_Data_Pnd_Complaint_Ctr;
    private DbsField pnd_Local_Data_Pnd_Other_Ctr;
    private DbsField pnd_Local_Data_Pnd_End_Of_Data;
    private DbsField pnd_Local_Data_Pnd_Confirmed;

    private DataAccessProgramView vw_cwf_Master_Index;
    private DbsField cwf_Master_Index_Pin_Nbr;
    private DbsField cwf_Master_Index_Rqst_Log_Dte_Tme;

    private DbsGroup cwf_Master_Index__R_Field_13;
    private DbsField cwf_Master_Index_Rqst_Log_Index_Dte;
    private DbsField cwf_Master_Index_Work_Prcss_Id;
    private DbsField cwf_Master_Index_Rqst_Log_Oprtr_Cde;
    private DbsField cwf_Master_Index_Status_Cde;
    private DbsField cwf_Master_Index_Admin_Status_Cde;
    private DbsField cwf_Master_Index_Last_Chnge_Unit_Cde;
    private DbsField cwf_Master_Index_Last_Chnge_Dte_Tme;
    private DbsField cwf_Master_Index_Mj_Pull_Ind;
    private DbsField cwf_Master_Index_Mj_Emrgncy_Rqst_Dte_Tme;
    private DbsField cwf_Master_Index_Mj_Emrgncy_Rqst_Oprtr_Cde;
    private DbsField cwf_Master_Index_Actve_Ind;

    private DataAccessProgramView vw_hst_Master_Index;
    private DbsField hst_Master_Index_Pin_Nbr;
    private DbsField hst_Master_Index_Rqst_Log_Dte_Tme;
    private DbsField hst_Master_Index_Rqst_Log_Oprtr_Cde;
    private DbsField hst_Master_Index_Work_Prcss_Id;

    private DbsGroup hst_Master_Index__R_Field_14;
    private DbsField hst_Master_Index_Work_Actn_Rqstd_Cde;
    private DbsField hst_Master_Index_Work_Lob_Cmpny_Prdct_Cde;
    private DbsField hst_Master_Index_Work_Mjr_Bsnss_Prcss_Cde;
    private DbsField hst_Master_Index_Work_Spcfc_Bsnss_Prcss_Cde;
    private DbsField hst_Master_Index_Work_Rqst_Prty_Cde;
    private DbsField hst_Master_Index_Mj_Pull_Ind;
    private DbsField hst_Master_Index_Last_Chnge_Unit_Cde;
    private DbsField pnd_Parm_Type;
    private DbsField pnd_Env;
    private DbsField pnd_Page;
    private DbsField pnd_Sort_Unit;
    private DbsField pnd_New_Racf;
    private DbsField pnd_New_Unit;
    private DbsField pnd_Yyyymmdd;

    private DbsGroup pnd_Yyyymmdd__R_Field_15;
    private DbsField pnd_Yyyymmdd_Pnd_Century;
    private DbsField pnd_Yyyymmdd_Pnd_Yy;
    private DbsField pnd_Yyyymmdd_Pnd_Mm;
    private DbsField pnd_Yyyymmdd_Pnd_Dd;
    private DbsField pnd_Report_No;
    private DbsField pnd_Racf_Id;
    private DbsField pnd_Floor;
    private DbsField pnd_Bldg;
    private DbsField pnd_Drop_Off;
    private DbsField pnd_Unit_Name;
    private DbsField pnd_Report_Parm;
    private DbsField pnd_Tbl_Run_Flag;
    private DbsField pnd_Parm_Unit;
    private DbsField pnd_Comp_Date;
    private DbsField pnd_Reccount;
    private DbsField pnd_Work_Start_Date;
    private DbsField pnd_Work_Comp_Date;
    private DbsField pnd_Date_Diff;
    private DbsField pnd_Oprtr_Cde;
    private DbsField pnd_Rush_Req;
    private DbsField pnd_Work_Unit;
    private DbsField pnd_Start_Dte_Tme;
    private DbsField pnd_End_Dte_Tme;
    private DbsField pnd_End_Dte_Tme_D;

    private DbsGroup pnd_End_Dte_Tme_D__R_Field_16;
    private DbsField pnd_End_Dte_Tme_D_Pnd_End_Dte_Tme_N;

    private DbsRecord internalLoopRecord;
    private DbsField sort01Tbl_WpidOld;
    private DbsField sort01Tbl_WpidCount397;
    private DbsField sort01Tbl_WpidCount;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Rep_Parm = localVariables.newGroupInRecord("pnd_Rep_Parm", "#REP-PARM");
        pnd_Rep_Parm_Pnd_Rep_Racf_Id = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_Racf_Id", "#REP-RACF-ID", FieldType.STRING, 8);
        pnd_Rep_Parm_Pnd_Rep_Start_Dd = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_Start_Dd", "#REP-START-DD", FieldType.NUMERIC, 2);

        pnd_Rep_Parm__R_Field_1 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_1", "REDEFINE", pnd_Rep_Parm_Pnd_Rep_Start_Dd);
        pnd_Rep_Parm_Pnd_Rep_Start_Dd_A = pnd_Rep_Parm__R_Field_1.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_Start_Dd_A", "#REP-START-DD-A", FieldType.STRING, 
            2);
        pnd_Rep_Parm_Pnd_Rep_Start_Mm = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_Start_Mm", "#REP-START-MM", FieldType.NUMERIC, 2);

        pnd_Rep_Parm__R_Field_2 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_2", "REDEFINE", pnd_Rep_Parm_Pnd_Rep_Start_Mm);
        pnd_Rep_Parm_Pnd_Rep_Start_Mm_A = pnd_Rep_Parm__R_Field_2.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_Start_Mm_A", "#REP-START-MM-A", FieldType.STRING, 
            2);
        pnd_Rep_Parm_Pnd_Rep_Start_Yy = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_Start_Yy", "#REP-START-YY", FieldType.NUMERIC, 2);

        pnd_Rep_Parm__R_Field_3 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_3", "REDEFINE", pnd_Rep_Parm_Pnd_Rep_Start_Yy);
        pnd_Rep_Parm_Pnd_Rep_Start_Yy_A = pnd_Rep_Parm__R_Field_3.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_Start_Yy_A", "#REP-START-YY-A", FieldType.STRING, 
            2);
        pnd_Rep_Parm_Pnd_Rep_End_Dd = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_End_Dd", "#REP-END-DD", FieldType.NUMERIC, 2);

        pnd_Rep_Parm__R_Field_4 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_4", "REDEFINE", pnd_Rep_Parm_Pnd_Rep_End_Dd);
        pnd_Rep_Parm_Pnd_Rep_End_Dd_A = pnd_Rep_Parm__R_Field_4.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_End_Dd_A", "#REP-END-DD-A", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Rep_End_Mm = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_End_Mm", "#REP-END-MM", FieldType.NUMERIC, 2);

        pnd_Rep_Parm__R_Field_5 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_5", "REDEFINE", pnd_Rep_Parm_Pnd_Rep_End_Mm);
        pnd_Rep_Parm_Pnd_Rep_End_Mm_A = pnd_Rep_Parm__R_Field_5.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_End_Mm_A", "#REP-END-MM-A", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Rep_End_Yy = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_End_Yy", "#REP-END-YY", FieldType.NUMERIC, 2);

        pnd_Rep_Parm__R_Field_6 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_6", "REDEFINE", pnd_Rep_Parm_Pnd_Rep_End_Yy);
        pnd_Rep_Parm_Pnd_Rep_End_Yy_A = pnd_Rep_Parm__R_Field_6.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_End_Yy_A", "#REP-END-YY-A", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Rep_Unit_Cde = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_Unit_Cde", "#REP-UNIT-CDE", FieldType.STRING, 8);
        pnd_Rep_Parm_Pnd_Start_Date = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Start_Date", "#START-DATE", FieldType.STRING, 8);

        pnd_Rep_Parm__R_Field_7 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_7", "REDEFINE", pnd_Rep_Parm_Pnd_Start_Date);
        pnd_Rep_Parm_Pnd_Start_Date_N = pnd_Rep_Parm__R_Field_7.newFieldInGroup("pnd_Rep_Parm_Pnd_Start_Date_N", "#START-DATE-N", FieldType.NUMERIC, 8);
        pnd_Rep_Parm_Pnd_End_Date = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_End_Date", "#END-DATE", FieldType.STRING, 8);

        pnd_Rep_Parm__R_Field_8 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_8", "REDEFINE", pnd_Rep_Parm_Pnd_End_Date);
        pnd_Rep_Parm_Pnd_End_Date_N = pnd_Rep_Parm__R_Field_8.newFieldInGroup("pnd_Rep_Parm_Pnd_End_Date_N", "#END-DATE-N", FieldType.NUMERIC, 8);
        pnd_Rep_Parm_Pnd_Work_Start_Date_A = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Start_Date_A", "#WORK-START-DATE-A", FieldType.STRING, 
            8);

        pnd_Rep_Parm__R_Field_9 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_9", "REDEFINE", pnd_Rep_Parm_Pnd_Work_Start_Date_A);
        pnd_Rep_Parm_Pnd_Work_Mm = pnd_Rep_Parm__R_Field_9.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Mm", "#WORK-MM", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Filler1 = pnd_Rep_Parm__R_Field_9.newFieldInGroup("pnd_Rep_Parm_Pnd_Filler1", "#FILLER1", FieldType.STRING, 1);
        pnd_Rep_Parm_Pnd_Work_Dd = pnd_Rep_Parm__R_Field_9.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Dd", "#WORK-DD", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Filler2 = pnd_Rep_Parm__R_Field_9.newFieldInGroup("pnd_Rep_Parm_Pnd_Filler2", "#FILLER2", FieldType.STRING, 1);
        pnd_Rep_Parm_Pnd_Work_Yy = pnd_Rep_Parm__R_Field_9.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Yy", "#WORK-YY", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Work_End_Date_A = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_End_Date_A", "#WORK-END-DATE-A", FieldType.STRING, 8);

        pnd_Rep_Parm__R_Field_10 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_10", "REDEFINE", pnd_Rep_Parm_Pnd_Work_End_Date_A);
        pnd_Rep_Parm_Pnd_Work_Mmm = pnd_Rep_Parm__R_Field_10.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Mmm", "#WORK-MMM", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Fillera = pnd_Rep_Parm__R_Field_10.newFieldInGroup("pnd_Rep_Parm_Pnd_Fillera", "#FILLERA", FieldType.STRING, 1);
        pnd_Rep_Parm_Pnd_Work_Ddd = pnd_Rep_Parm__R_Field_10.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Ddd", "#WORK-DDD", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Fillerb = pnd_Rep_Parm__R_Field_10.newFieldInGroup("pnd_Rep_Parm_Pnd_Fillerb", "#FILLERB", FieldType.STRING, 1);
        pnd_Rep_Parm_Pnd_Work_Yyy = pnd_Rep_Parm__R_Field_10.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Yyy", "#WORK-YYY", FieldType.STRING, 2);

        pnd_Local_Data = localVariables.newGroupInRecord("pnd_Local_Data", "#LOCAL-DATA");

        pnd_Local_Data_Pnd_Work_Record = pnd_Local_Data.newGroupInGroup("pnd_Local_Data_Pnd_Work_Record", "#WORK-RECORD");
        pnd_Local_Data_Tbl_Wpid = pnd_Local_Data_Pnd_Work_Record.newFieldInGroup("pnd_Local_Data_Tbl_Wpid", "TBL-WPID", FieldType.STRING, 6);

        pnd_Local_Data__R_Field_11 = pnd_Local_Data_Pnd_Work_Record.newGroupInGroup("pnd_Local_Data__R_Field_11", "REDEFINE", pnd_Local_Data_Tbl_Wpid);
        pnd_Local_Data_Tbl_Wpid_Action = pnd_Local_Data__R_Field_11.newFieldInGroup("pnd_Local_Data_Tbl_Wpid_Action", "TBL-WPID-ACTION", FieldType.STRING, 
            1);
        pnd_Local_Data_Tbl_Wpid_Lob = pnd_Local_Data__R_Field_11.newFieldInGroup("pnd_Local_Data_Tbl_Wpid_Lob", "TBL-WPID-LOB", FieldType.STRING, 2);
        pnd_Local_Data_Tbl_Wpid_Mbp = pnd_Local_Data__R_Field_11.newFieldInGroup("pnd_Local_Data_Tbl_Wpid_Mbp", "TBL-WPID-MBP", FieldType.STRING, 1);
        pnd_Local_Data_Tbl_Wpid_Sbp = pnd_Local_Data__R_Field_11.newFieldInGroup("pnd_Local_Data_Tbl_Wpid_Sbp", "TBL-WPID-SBP", FieldType.STRING, 2);
        pnd_Local_Data_Tbl_Wpid_Act = pnd_Local_Data_Pnd_Work_Record.newFieldInGroup("pnd_Local_Data_Tbl_Wpid_Act", "TBL-WPID-ACT", FieldType.STRING, 
            1);
        pnd_Local_Data_Tbl_Unit = pnd_Local_Data_Pnd_Work_Record.newFieldInGroup("pnd_Local_Data_Tbl_Unit", "TBL-UNIT", FieldType.STRING, 8);
        pnd_Local_Data_Tbl_Racf_Id = pnd_Local_Data_Pnd_Work_Record.newFieldInGroup("pnd_Local_Data_Tbl_Racf_Id", "TBL-RACF-ID", FieldType.STRING, 8);
        pnd_Local_Data_Tbl_Prty = pnd_Local_Data_Pnd_Work_Record.newFieldInGroup("pnd_Local_Data_Tbl_Prty", "TBL-PRTY", FieldType.STRING, 1);
        pnd_Local_Data_Tbl_Pin_Nbr = pnd_Local_Data_Pnd_Work_Record.newFieldInGroup("pnd_Local_Data_Tbl_Pin_Nbr", "TBL-PIN-NBR", FieldType.NUMERIC, 12);
        pnd_Local_Data_Rush_Req_Ind = pnd_Local_Data_Pnd_Work_Record.newFieldInGroup("pnd_Local_Data_Rush_Req_Ind", "RUSH-REQ-IND", FieldType.STRING, 
            1);
        pnd_Local_Data_Pnd_Work_Date_D = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Work_Date_D", "#WORK-DATE-D", FieldType.DATE);
        pnd_Local_Data_Pnd_Rep_Wpid_Name = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Rep_Wpid_Name", "#REP-WPID-NAME", FieldType.STRING, 45);
        pnd_Local_Data_Pnd_Rep_Empl_Name = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Rep_Empl_Name", "#REP-EMPL-NAME", FieldType.STRING, 30);
        pnd_Local_Data_Pnd_Pad_Space = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Pad_Space", "#PAD-SPACE", FieldType.STRING, 8);
        pnd_Local_Data_Pnd_Wrk_Unit_Cde = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Wrk_Unit_Cde", "#WRK-UNIT-CDE", FieldType.STRING, 8);

        pnd_Local_Data__R_Field_12 = pnd_Local_Data.newGroupInGroup("pnd_Local_Data__R_Field_12", "REDEFINE", pnd_Local_Data_Pnd_Wrk_Unit_Cde);
        pnd_Local_Data_Pnd_Wrk_Unit_Id_Cde = pnd_Local_Data__R_Field_12.newFieldInGroup("pnd_Local_Data_Pnd_Wrk_Unit_Id_Cde", "#WRK-UNIT-ID-CDE", FieldType.STRING, 
            5);
        pnd_Local_Data_Pnd_Wrk_Unit_Suffix = pnd_Local_Data__R_Field_12.newFieldInGroup("pnd_Local_Data_Pnd_Wrk_Unit_Suffix", "#WRK-UNIT-SUFFIX", FieldType.STRING, 
            3);
        pnd_Local_Data_Pnd_Work_Prcss_Id = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Work_Prcss_Id", "#WORK-PRCSS-ID", FieldType.STRING, 6);
        pnd_Local_Data_Pnd_Wpid_Code = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Wpid_Code", "#WPID-CODE", FieldType.STRING, 6);
        pnd_Local_Data_Pnd_Wpid_Desc = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Wpid_Desc", "#WPID-DESC", FieldType.STRING, 45);
        pnd_Local_Data_Pnd_Rep_Ctr = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Rep_Ctr", "#REP-CTR", FieldType.PACKED_DECIMAL, 2);
        pnd_Local_Data_Pnd_Max = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Max", "#MAX", FieldType.PACKED_DECIMAL, 2);
        pnd_Local_Data_Pnd_Max_Less_1 = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Max_Less_1", "#MAX-LESS-1", FieldType.PACKED_DECIMAL, 2);
        pnd_Local_Data_Pnd_I = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 2);
        pnd_Local_Data_Pnd_J = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_J", "#J", FieldType.PACKED_DECIMAL, 2);
        pnd_Local_Data_Pnd_K = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_K", "#K", FieldType.PACKED_DECIMAL, 2);
        pnd_Local_Data_Pnd_Total_Logged = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Total_Logged", "#TOTAL-LOGGED", FieldType.NUMERIC, 5);
        pnd_Local_Data_Pnd_Wpid_Count = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Wpid_Count", "#WPID-COUNT", FieldType.NUMERIC, 5);
        pnd_Local_Data_Pnd_Unclear_Ctr = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Unclear_Ctr", "#UNCLEAR-CTR", FieldType.NUMERIC, 5);
        pnd_Local_Data_Pnd_Total_Unclear = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Total_Unclear", "#TOTAL-UNCLEAR", FieldType.NUMERIC, 5);
        pnd_Local_Data_Pnd_Booklet_Ctr = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Booklet_Ctr", "#BOOKLET-CTR", FieldType.NUMERIC, 5);
        pnd_Local_Data_Pnd_Forms_Ctr = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Forms_Ctr", "#FORMS-CTR", FieldType.NUMERIC, 5);
        pnd_Local_Data_Pnd_Inquire_Ctr = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Inquire_Ctr", "#INQUIRE-CTR", FieldType.NUMERIC, 5);
        pnd_Local_Data_Pnd_Research_Ctr = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Research_Ctr", "#RESEARCH-CTR", FieldType.NUMERIC, 5);
        pnd_Local_Data_Pnd_Trans_Ctr = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Trans_Ctr", "#TRANS-CTR", FieldType.NUMERIC, 5);
        pnd_Local_Data_Pnd_Complaint_Ctr = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Complaint_Ctr", "#COMPLAINT-CTR", FieldType.NUMERIC, 5);
        pnd_Local_Data_Pnd_Other_Ctr = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Other_Ctr", "#OTHER-CTR", FieldType.NUMERIC, 5);
        pnd_Local_Data_Pnd_End_Of_Data = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_End_Of_Data", "#END-OF-DATA", FieldType.BOOLEAN, 1);
        pnd_Local_Data_Pnd_Confirmed = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Confirmed", "#CONFIRMED", FieldType.BOOLEAN, 1);

        vw_cwf_Master_Index = new DataAccessProgramView(new NameInfo("vw_cwf_Master_Index", "CWF-MASTER-INDEX"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        cwf_Master_Index_Pin_Nbr = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "PIN_NBR");
        cwf_Master_Index_Pin_Nbr.setDdmHeader("PIN");
        cwf_Master_Index_Rqst_Log_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Master_Index_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");

        cwf_Master_Index__R_Field_13 = vw_cwf_Master_Index.getRecord().newGroupInGroup("cwf_Master_Index__R_Field_13", "REDEFINE", cwf_Master_Index_Rqst_Log_Dte_Tme);
        cwf_Master_Index_Rqst_Log_Index_Dte = cwf_Master_Index__R_Field_13.newFieldInGroup("cwf_Master_Index_Rqst_Log_Index_Dte", "RQST-LOG-INDEX-DTE", 
            FieldType.STRING, 8);
        cwf_Master_Index_Work_Prcss_Id = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        cwf_Master_Index_Work_Prcss_Id.setDdmHeader("WORK/ID");
        cwf_Master_Index_Rqst_Log_Oprtr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_LOG_OPRTR_CDE");
        cwf_Master_Index_Rqst_Log_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Master_Index_Status_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Status_Cde", "STATUS-CDE", FieldType.STRING, 4, 
            RepeatingFieldStrategy.None, "STATUS_CDE");
        cwf_Master_Index_Admin_Status_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "ADMIN_STATUS_CDE");
        cwf_Master_Index_Last_Chnge_Unit_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Last_Chnge_Unit_Cde", "LAST-CHNGE-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_CHNGE_UNIT_CDE");
        cwf_Master_Index_Last_Chnge_Unit_Cde.setDdmHeader("LAST/CHANGE/UNIT");
        cwf_Master_Index_Last_Chnge_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "LAST_CHNGE_DTE_TME");
        cwf_Master_Index_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");
        cwf_Master_Index_Mj_Pull_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Mj_Pull_Ind", "MJ-PULL-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "MJ_PULL_IND");
        cwf_Master_Index_Mj_Pull_Ind.setDdmHeader("MJ PULL/IND");
        cwf_Master_Index_Mj_Emrgncy_Rqst_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Mj_Emrgncy_Rqst_Dte_Tme", "MJ-EMRGNCY-RQST-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "MJ_EMRGNCY_RQST_DTE_TME");
        cwf_Master_Index_Mj_Emrgncy_Rqst_Dte_Tme.setDdmHeader("EMERGENCY REQ/DATE-TIME");
        cwf_Master_Index_Mj_Emrgncy_Rqst_Oprtr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Mj_Emrgncy_Rqst_Oprtr_Cde", "MJ-EMRGNCY-RQST-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "MJ_EMRGNCY_RQST_OPRTR_CDE");
        cwf_Master_Index_Mj_Emrgncy_Rqst_Oprtr_Cde.setDdmHeader("EMERGENCY/REQUESTOR");
        cwf_Master_Index_Actve_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTVE_IND");
        registerRecord(vw_cwf_Master_Index);

        vw_hst_Master_Index = new DataAccessProgramView(new NameInfo("vw_hst_Master_Index", "HST-MASTER-INDEX"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        hst_Master_Index_Pin_Nbr = vw_hst_Master_Index.getRecord().newFieldInGroup("hst_Master_Index_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "PIN_NBR");
        hst_Master_Index_Pin_Nbr.setDdmHeader("PIN");
        hst_Master_Index_Rqst_Log_Dte_Tme = vw_hst_Master_Index.getRecord().newFieldInGroup("hst_Master_Index_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        hst_Master_Index_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        hst_Master_Index_Rqst_Log_Oprtr_Cde = vw_hst_Master_Index.getRecord().newFieldInGroup("hst_Master_Index_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_LOG_OPRTR_CDE");
        hst_Master_Index_Rqst_Log_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        hst_Master_Index_Work_Prcss_Id = vw_hst_Master_Index.getRecord().newFieldInGroup("hst_Master_Index_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        hst_Master_Index_Work_Prcss_Id.setDdmHeader("WORK/ID");

        hst_Master_Index__R_Field_14 = vw_hst_Master_Index.getRecord().newGroupInGroup("hst_Master_Index__R_Field_14", "REDEFINE", hst_Master_Index_Work_Prcss_Id);
        hst_Master_Index_Work_Actn_Rqstd_Cde = hst_Master_Index__R_Field_14.newFieldInGroup("hst_Master_Index_Work_Actn_Rqstd_Cde", "WORK-ACTN-RQSTD-CDE", 
            FieldType.STRING, 1);
        hst_Master_Index_Work_Lob_Cmpny_Prdct_Cde = hst_Master_Index__R_Field_14.newFieldInGroup("hst_Master_Index_Work_Lob_Cmpny_Prdct_Cde", "WORK-LOB-CMPNY-PRDCT-CDE", 
            FieldType.STRING, 2);
        hst_Master_Index_Work_Mjr_Bsnss_Prcss_Cde = hst_Master_Index__R_Field_14.newFieldInGroup("hst_Master_Index_Work_Mjr_Bsnss_Prcss_Cde", "WORK-MJR-BSNSS-PRCSS-CDE", 
            FieldType.STRING, 1);
        hst_Master_Index_Work_Spcfc_Bsnss_Prcss_Cde = hst_Master_Index__R_Field_14.newFieldInGroup("hst_Master_Index_Work_Spcfc_Bsnss_Prcss_Cde", "WORK-SPCFC-BSNSS-PRCSS-CDE", 
            FieldType.STRING, 2);
        hst_Master_Index_Work_Rqst_Prty_Cde = vw_hst_Master_Index.getRecord().newFieldInGroup("hst_Master_Index_Work_Rqst_Prty_Cde", "WORK-RQST-PRTY-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "WORK_RQST_PRTY_CDE");
        hst_Master_Index_Work_Rqst_Prty_Cde.setDdmHeader("PRIO");
        hst_Master_Index_Mj_Pull_Ind = vw_hst_Master_Index.getRecord().newFieldInGroup("hst_Master_Index_Mj_Pull_Ind", "MJ-PULL-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "MJ_PULL_IND");
        hst_Master_Index_Mj_Pull_Ind.setDdmHeader("MJ PULL/IND");
        hst_Master_Index_Last_Chnge_Unit_Cde = vw_hst_Master_Index.getRecord().newFieldInGroup("hst_Master_Index_Last_Chnge_Unit_Cde", "LAST-CHNGE-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_CHNGE_UNIT_CDE");
        hst_Master_Index_Last_Chnge_Unit_Cde.setDdmHeader("LAST/CHANGE/UNIT");
        registerRecord(vw_hst_Master_Index);

        pnd_Parm_Type = localVariables.newFieldInRecord("pnd_Parm_Type", "#PARM-TYPE", FieldType.STRING, 1);
        pnd_Env = localVariables.newFieldInRecord("pnd_Env", "#ENV", FieldType.STRING, 10);
        pnd_Page = localVariables.newFieldInRecord("pnd_Page", "#PAGE", FieldType.NUMERIC, 5);
        pnd_Sort_Unit = localVariables.newFieldInRecord("pnd_Sort_Unit", "#SORT-UNIT", FieldType.STRING, 1);
        pnd_New_Racf = localVariables.newFieldInRecord("pnd_New_Racf", "#NEW-RACF", FieldType.BOOLEAN, 1);
        pnd_New_Unit = localVariables.newFieldInRecord("pnd_New_Unit", "#NEW-UNIT", FieldType.BOOLEAN, 1);
        pnd_Yyyymmdd = localVariables.newFieldInRecord("pnd_Yyyymmdd", "#YYYYMMDD", FieldType.STRING, 8);

        pnd_Yyyymmdd__R_Field_15 = localVariables.newGroupInRecord("pnd_Yyyymmdd__R_Field_15", "REDEFINE", pnd_Yyyymmdd);
        pnd_Yyyymmdd_Pnd_Century = pnd_Yyyymmdd__R_Field_15.newFieldInGroup("pnd_Yyyymmdd_Pnd_Century", "#CENTURY", FieldType.STRING, 2);
        pnd_Yyyymmdd_Pnd_Yy = pnd_Yyyymmdd__R_Field_15.newFieldInGroup("pnd_Yyyymmdd_Pnd_Yy", "#YY", FieldType.STRING, 2);
        pnd_Yyyymmdd_Pnd_Mm = pnd_Yyyymmdd__R_Field_15.newFieldInGroup("pnd_Yyyymmdd_Pnd_Mm", "#MM", FieldType.STRING, 2);
        pnd_Yyyymmdd_Pnd_Dd = pnd_Yyyymmdd__R_Field_15.newFieldInGroup("pnd_Yyyymmdd_Pnd_Dd", "#DD", FieldType.STRING, 2);
        pnd_Report_No = localVariables.newFieldInRecord("pnd_Report_No", "#REPORT-NO", FieldType.NUMERIC, 2);
        pnd_Racf_Id = localVariables.newFieldInRecord("pnd_Racf_Id", "#RACF-ID", FieldType.STRING, 8);
        pnd_Floor = localVariables.newFieldInRecord("pnd_Floor", "#FLOOR", FieldType.NUMERIC, 2);
        pnd_Bldg = localVariables.newFieldInRecord("pnd_Bldg", "#BLDG", FieldType.STRING, 3);
        pnd_Drop_Off = localVariables.newFieldInRecord("pnd_Drop_Off", "#DROP-OFF", FieldType.STRING, 2);
        pnd_Unit_Name = localVariables.newFieldInRecord("pnd_Unit_Name", "#UNIT-NAME", FieldType.STRING, 45);
        pnd_Report_Parm = localVariables.newFieldInRecord("pnd_Report_Parm", "#REPORT-PARM", FieldType.STRING, 17);
        pnd_Tbl_Run_Flag = localVariables.newFieldInRecord("pnd_Tbl_Run_Flag", "#TBL-RUN-FLAG", FieldType.STRING, 1);
        pnd_Parm_Unit = localVariables.newFieldInRecord("pnd_Parm_Unit", "#PARM-UNIT", FieldType.STRING, 7);
        pnd_Comp_Date = localVariables.newFieldInRecord("pnd_Comp_Date", "#COMP-DATE", FieldType.STRING, 8);
        pnd_Reccount = localVariables.newFieldInRecord("pnd_Reccount", "#RECCOUNT", FieldType.NUMERIC, 5);
        pnd_Work_Start_Date = localVariables.newFieldInRecord("pnd_Work_Start_Date", "#WORK-START-DATE", FieldType.DATE);
        pnd_Work_Comp_Date = localVariables.newFieldInRecord("pnd_Work_Comp_Date", "#WORK-COMP-DATE", FieldType.DATE);
        pnd_Date_Diff = localVariables.newFieldInRecord("pnd_Date_Diff", "#DATE-DIFF", FieldType.NUMERIC, 3);
        pnd_Oprtr_Cde = localVariables.newFieldInRecord("pnd_Oprtr_Cde", "#OPRTR-CDE", FieldType.STRING, 8);
        pnd_Rush_Req = localVariables.newFieldInRecord("pnd_Rush_Req", "#RUSH-REQ", FieldType.STRING, 1);
        pnd_Work_Unit = localVariables.newFieldInRecord("pnd_Work_Unit", "#WORK-UNIT", FieldType.STRING, 8);
        pnd_Start_Dte_Tme = localVariables.newFieldInRecord("pnd_Start_Dte_Tme", "#START-DTE-TME", FieldType.TIME);
        pnd_End_Dte_Tme = localVariables.newFieldInRecord("pnd_End_Dte_Tme", "#END-DTE-TME", FieldType.TIME);
        pnd_End_Dte_Tme_D = localVariables.newFieldInRecord("pnd_End_Dte_Tme_D", "#END-DTE-TME-D", FieldType.STRING, 15);

        pnd_End_Dte_Tme_D__R_Field_16 = localVariables.newGroupInRecord("pnd_End_Dte_Tme_D__R_Field_16", "REDEFINE", pnd_End_Dte_Tme_D);
        pnd_End_Dte_Tme_D_Pnd_End_Dte_Tme_N = pnd_End_Dte_Tme_D__R_Field_16.newFieldInGroup("pnd_End_Dte_Tme_D_Pnd_End_Dte_Tme_N", "#END-DTE-TME-N", FieldType.NUMERIC, 
            15);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        sort01Tbl_WpidOld = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Wpid_OLD", "Tbl_Wpid_OLD", FieldType.STRING, 6);
        sort01Tbl_WpidCount397 = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Wpid_COUNT_397", "Tbl_Wpid_COUNT_397", FieldType.NUMERIC, 9);
        sort01Tbl_WpidCount = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Wpid_COUNT", "Tbl_Wpid_COUNT", FieldType.NUMERIC, 9);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Master_Index.reset();
        vw_hst_Master_Index.reset();
        internalLoopRecord.reset();

        localVariables.reset();
        pnd_Parm_Type.setInitialValue("D");
        pnd_Report_No.setInitialValue(1);
        pnd_Report_Parm.setInitialValue("CWFB3001D*");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Cwfb3001() throws Exception
    {
        super("Cwfb3001");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("CWFB3001", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //* *********************************
        //*  COPYCODE : CWFC3000
        //*  FUNCTION : SETUP ENVONMENT MESSAGE
        //*  AUTHOR   : PATINGO, JOSEPH S.
        //* *********************************
        pnd_Env.setValue(Global.getLIBRARY_ID());                                                                                                                         //Natural: MOVE *LIBRARY-ID TO #ENV
        if (condition(pnd_Env.equals("PROJCWF") || pnd_Env.equals("PROJCWF")))                                                                                            //Natural: IF #ENV = 'PROJCWF' OR #ENV = 'PROJCWF'
        {
            pnd_Env.setValue("DEV'T ");                                                                                                                                   //Natural: MOVE 'DEV"T ' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("PRACANN")))                                                                                                                         //Natural: IF #ENV = 'PRACANN'
        {
            pnd_Env.setValue("PRACTICE");                                                                                                                                 //Natural: MOVE 'PRACTICE' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("PRODANN")))                                                                                                                         //Natural: IF #ENV = 'PRODANN'
        {
            pnd_Env.setValue(" ");                                                                                                                                        //Natural: MOVE ' ' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT034")))                                                                                                                         //Natural: IF #ENV = 'ACPT034'
        {
            pnd_Env.setValue("RGN:AT07");                                                                                                                                 //Natural: MOVE 'RGN:AT07' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT023")))                                                                                                                         //Natural: IF #ENV = 'ACPT023'
        {
            pnd_Env.setValue("RGN:AT06");                                                                                                                                 //Natural: MOVE 'RGN:AT06' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT028")))                                                                                                                         //Natural: IF #ENV = 'ACPT028'
        {
            pnd_Env.setValue("RGN:AT05");                                                                                                                                 //Natural: MOVE 'RGN:AT05' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        //* ****** END COPYCODE *************
        if (condition(Global.getLIBRARY_ID().notEquals("PRODANN")))                                                                                                       //Natural: IF *LIBRARY-ID NE 'PRODANN'
        {
            pnd_Env.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "(", pnd_Env, ")"));                                                                         //Natural: COMPRESS '(' #ENV ')' INTO #ENV LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-IF
        //* ********************
        //*                    *
        //*  REPORT SECTION    *
        //* ********************
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 80 PS = 58
        pnd_Rep_Parm_Pnd_Filler1.setValue("/");                                                                                                                           //Natural: MOVE '/' TO #FILLER1 #FILLER2
        pnd_Rep_Parm_Pnd_Filler2.setValue("/");
        pnd_Rep_Parm_Pnd_Fillera.setValue("/");                                                                                                                           //Natural: MOVE '/' TO #FILLERA #FILLERB
        pnd_Rep_Parm_Pnd_Fillerb.setValue("/");
        //*  GETS SYSTEM RUN DATE ( NEW ONE )
        //*  (INIT = D)
        DbsUtil.callnat(Cwfn3912.class , getCurrentProcessState(), pnd_Comp_Date, pnd_Parm_Type);                                                                         //Natural: CALLNAT 'CWFN3912' #COMP-DATE #PARM-TYPE
        if (condition(Global.isEscape())) return;
        //*  RESETS THE Y/N FLAG BUT DOES NOT BUMP UP THE DATE
        //*  YET
        DbsUtil.callnat(Cwfn3913.class , getCurrentProcessState(), pnd_Report_Parm, pnd_Racf_Id, pnd_Parm_Unit, pnd_Floor, pnd_Bldg, pnd_Drop_Off, pnd_Rep_Parm_Pnd_Start_Date,  //Natural: CALLNAT 'CWFN3913' #REPORT-PARM #RACF-ID #PARM-UNIT #FLOOR #BLDG #DROP-OFF #START-DATE #TBL-RUN-FLAG
            pnd_Tbl_Run_Flag);
        if (condition(Global.isEscape())) return;
        //*  RUN FOR LAST BUSINESS DAY
        //*  1. BUMP UP PREVIOUS RUN DATE AND
        //*  2. GET PREVIOUS DAY IF TIME IS = OR LT 5:00 PM
        //*     ELSE GET DATE TODAY
        //*   |-----------AM----------|----------PM-----------|
        //*   0-0-0-0-0-0-0-0-0-0-1-1-1-0-0-0-0-0-0-0-0-0-1-1-1
        //*   0-1-2-3-4-5-6-7-8-9-0-1-2-1-2-3-4-5-6-7-8-9-0-1-1
        //*                                                   5
        //*                                                   9
        //*   PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPNNNNNNNNNNNNNN
        //*  3. COMPARE 1 & 2
        //*  4. USE WHICHEVER IS MORE RECENT
        if (condition(pnd_Comp_Date.greater(pnd_Rep_Parm_Pnd_Start_Date)))                                                                                                //Natural: IF #COMP-DATE GT #START-DATE
        {
                                                                                                                                                                          //Natural: PERFORM GET-START-DATE
            sub_Get_Start_Date();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            DbsUtil.terminate();  if (true) return;                                                                                                                       //Natural: TERMINATE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Yyyymmdd.setValue(pnd_Rep_Parm_Pnd_Start_Date);                                                                                                               //Natural: MOVE #START-DATE TO #YYYYMMDD
        pnd_Rep_Parm_Pnd_Work_Mm.setValue(pnd_Yyyymmdd_Pnd_Mm);                                                                                                           //Natural: MOVE #MM TO #WORK-MM
        pnd_Rep_Parm_Pnd_Work_Dd.setValue(pnd_Yyyymmdd_Pnd_Dd);                                                                                                           //Natural: MOVE #DD TO #WORK-DD
        pnd_Rep_Parm_Pnd_Work_Yy.setValue(pnd_Yyyymmdd_Pnd_Yy);                                                                                                           //Natural: MOVE #YY TO #WORK-YY
        pnd_Rep_Parm_Pnd_End_Date.setValue(pnd_Comp_Date);                                                                                                                //Natural: MOVE #COMP-DATE TO #END-DATE #YYYYMMDD
        pnd_Yyyymmdd.setValue(pnd_Comp_Date);
        pnd_Rep_Parm_Pnd_Work_Mmm.setValue(pnd_Yyyymmdd_Pnd_Mm);                                                                                                          //Natural: MOVE #MM TO #WORK-MMM
        pnd_Rep_Parm_Pnd_Work_Ddd.setValue(pnd_Yyyymmdd_Pnd_Dd);                                                                                                          //Natural: MOVE #DD TO #WORK-DDD
        pnd_Rep_Parm_Pnd_Work_Yyy.setValue(pnd_Yyyymmdd_Pnd_Yy);                                                                                                          //Natural: MOVE #YY TO #WORK-YYY
        vw_cwf_Master_Index.startDatabaseRead                                                                                                                             //Natural: READ CWF-MASTER-INDEX BY ACTV-UNQUE-KEY FROM #START-DATE
        (
        "READ_MASTER",
        new Wc[] { new Wc("ACTV_UNQUE_KEY", ">=", pnd_Rep_Parm_Pnd_Start_Date, WcType.BY) },
        new Oc[] { new Oc("ACTV_UNQUE_KEY", "ASC") }
        );
        READ_MASTER:
        while (condition(vw_cwf_Master_Index.readNextRow("READ_MASTER")))
        {
            if (condition(cwf_Master_Index_Rqst_Log_Index_Dte.greater(pnd_Rep_Parm_Pnd_End_Date)))                                                                        //Natural: IF CWF-MASTER-INDEX.RQST-LOG-INDEX-DTE GT #END-DATE
            {
                if (true) break READ_MASTER;                                                                                                                              //Natural: ESCAPE BOTTOM ( READ-MASTER. ) IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            vw_hst_Master_Index.startDatabaseRead                                                                                                                         //Natural: READ ( 1 ) HST-MASTER-INDEX BY RQST-ROUTING-KEY FROM CWF-MASTER-INDEX.RQST-LOG-DTE-TME
            (
            "GET_WPID",
            new Wc[] { new Wc("RQST_ROUTING_KEY", ">=", cwf_Master_Index_Rqst_Log_Dte_Tme, WcType.BY) },
            new Oc[] { new Oc("RQST_ROUTING_KEY", "ASC") },
            1
            );
            GET_WPID:
            while (condition(vw_hst_Master_Index.readNextRow("GET_WPID")))
            {
                pnd_Local_Data_Tbl_Pin_Nbr.setValue(hst_Master_Index_Pin_Nbr);                                                                                            //Natural: MOVE HST-MASTER-INDEX.PIN-NBR TO TBL-PIN-NBR
                pnd_Local_Data_Pnd_Work_Prcss_Id.setValue(hst_Master_Index_Work_Prcss_Id);                                                                                //Natural: MOVE HST-MASTER-INDEX.WORK-PRCSS-ID TO #WORK-PRCSS-ID
                if (condition(hst_Master_Index_Mj_Pull_Ind.equals("R")))                                                                                                  //Natural: IF HST-MASTER-INDEX.MJ-PULL-IND = 'R'
                {
                    pnd_Local_Data_Tbl_Prty.setValue(hst_Master_Index_Mj_Pull_Ind);                                                                                       //Natural: MOVE HST-MASTER-INDEX.MJ-PULL-IND TO TBL-PRTY
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Local_Data_Tbl_Prty.setValue(hst_Master_Index_Work_Rqst_Prty_Cde);                                                                                //Natural: MOVE HST-MASTER-INDEX.WORK-RQST-PRTY-CDE TO TBL-PRTY
                }                                                                                                                                                         //Natural: END-IF
                pnd_Oprtr_Cde.setValue(hst_Master_Index_Rqst_Log_Oprtr_Cde);                                                                                              //Natural: MOVE HST-MASTER-INDEX.RQST-LOG-OPRTR-CDE TO #OPRTR-CDE
                pnd_Work_Unit.setValue(hst_Master_Index_Last_Chnge_Unit_Cde);                                                                                             //Natural: MOVE HST-MASTER-INDEX.LAST-CHNGE-UNIT-CDE TO #WORK-UNIT
                pnd_Rush_Req.setValue(" ");                                                                                                                               //Natural: MOVE ' ' TO #RUSH-REQ
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_MASTER"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_MASTER"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* EMPL+UNIT
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-FILE
            sub_Write_Work_File();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_MASTER"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_MASTER"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  READ-MASTER.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Start_Dte_Tme.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Rep_Parm_Pnd_Start_Date);                                                                     //Natural: MOVE EDITED #START-DATE TO #START-DTE-TME ( EM = YYYYMMDD )
        pnd_End_Dte_Tme.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Rep_Parm_Pnd_End_Date);                                                                         //Natural: MOVE EDITED #END-DATE TO #END-DTE-TME ( EM = YYYYMMDD )
        pnd_End_Dte_Tme.nadd(863999);                                                                                                                                     //Natural: COMPUTE #END-DTE-TME = #END-DTE-TME + 863999
        pnd_End_Dte_Tme_D.setValueEdited(pnd_End_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                                                          //Natural: MOVE EDITED #END-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #END-DTE-TME-D
        vw_cwf_Master_Index.startDatabaseRead                                                                                                                             //Natural: READ CWF-MASTER-INDEX BY LAST-CHNGE-DTE-KEY FROM #START-DATE-N
        (
        "READ_MASTER2",
        new Wc[] { new Wc("LAST_CHNGE_DTE_KEY", ">=", pnd_Rep_Parm_Pnd_Start_Date_N, WcType.BY) },
        new Oc[] { new Oc("LAST_CHNGE_DTE_KEY", "ASC") }
        );
        READ_MASTER2:
        while (condition(vw_cwf_Master_Index.readNextRow("READ_MASTER2")))
        {
            if (condition(cwf_Master_Index_Last_Chnge_Dte_Tme.greater(pnd_End_Dte_Tme_D_Pnd_End_Dte_Tme_N)))                                                              //Natural: IF CWF-MASTER-INDEX.LAST-CHNGE-DTE-TME GT #END-DTE-TME-N
            {
                if (true) break READ_MASTER2;                                                                                                                             //Natural: ESCAPE BOTTOM ( READ-MASTER2. )
            }                                                                                                                                                             //Natural: END-IF
            //*  MJ REQUEST
            if (condition(!(cwf_Master_Index_Mj_Pull_Ind.equals("R") && cwf_Master_Index_Mj_Emrgncy_Rqst_Dte_Tme.greaterOrEqual(pnd_Start_Dte_Tme) &&                     //Natural: ACCEPT IF CWF-MASTER-INDEX.MJ-PULL-IND = 'R' AND CWF-MASTER-INDEX.MJ-EMRGNCY-RQST-DTE-TME = #START-DTE-TME THRU #END-DTE-TME AND CWF-MASTER-INDEX.ADMIN-STATUS-CDE = 'C030'
                cwf_Master_Index_Mj_Emrgncy_Rqst_Dte_Tme.lessOrEqual(pnd_End_Dte_Tme) && cwf_Master_Index_Admin_Status_Cde.equals("C030"))))
            {
                continue;
            }
            pnd_Local_Data_Tbl_Pin_Nbr.setValue(cwf_Master_Index_Pin_Nbr);                                                                                                //Natural: MOVE CWF-MASTER-INDEX.PIN-NBR TO TBL-PIN-NBR
            pnd_Local_Data_Pnd_Work_Prcss_Id.setValue(cwf_Master_Index_Work_Prcss_Id);                                                                                    //Natural: MOVE CWF-MASTER-INDEX.WORK-PRCSS-ID TO #WORK-PRCSS-ID
            pnd_Local_Data_Tbl_Prty.setValue("R");                                                                                                                        //Natural: MOVE 'R' TO TBL-PRTY
            pnd_Rush_Req.setValue("R");                                                                                                                                   //Natural: MOVE 'R' TO #RUSH-REQ
            pnd_Oprtr_Cde.setValue(cwf_Master_Index_Mj_Emrgncy_Rqst_Oprtr_Cde);                                                                                           //Natural: MOVE CWF-MASTER-INDEX.MJ-EMRGNCY-RQST-OPRTR-CDE TO #OPRTR-CDE
            pnd_Work_Unit.setValue(cwf_Master_Index_Last_Chnge_Unit_Cde);                                                                                                 //Natural: MOVE CWF-MASTER-INDEX.LAST-CHNGE-UNIT-CDE TO #WORK-UNIT
            //* EMPL+UNIT
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-FILE
            sub_Write_Work_File();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_MASTER2"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_MASTER2"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  READ-MASTER.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Reccount.equals(getZero())))                                                                                                                    //Natural: IF #RECCOUNT = 0
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            DbsUtil.callnat(Cwfn3910.class , getCurrentProcessState(), pnd_Report_No, pnd_Racf_Id, pnd_Local_Data_Tbl_Unit, pnd_Floor, pnd_Bldg, pnd_Drop_Off,            //Natural: CALLNAT 'CWFN3910' #REPORT-NO #RACF-ID TBL-UNIT #FLOOR #BLDG #DROP-OFF #COMP-DATE
                pnd_Comp_Date);
            if (condition(Global.isEscape())) return;
            DbsUtil.callnat(Cwfn3915.class , getCurrentProcessState(), pnd_Report_No);                                                                                    //Natural: CALLNAT 'CWFN3915' #REPORT-NO
            if (condition(Global.isEscape())) return;
            DbsUtil.callnat(Cwfn3911.class , getCurrentProcessState());                                                                                                   //Natural: CALLNAT 'CWFN3911'
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_New_Unit.setValue(true);                                                                                                                                      //Natural: MOVE TRUE TO #NEW-UNIT #NEW-RACF
        pnd_New_Racf.setValue(true);
        setControl("WB");                                                                                                                                                 //Natural: SET CONTROL 'WB'
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #WORK-RECORD
        while (condition(getWorkFiles().read(1, pnd_Local_Data_Pnd_Work_Record)))
        {
            if (condition(pnd_Local_Data_Rush_Req_Ind.greater(" ")))                                                                                                      //Natural: REJECT IF RUSH-REQ-IND GT ' '
            {
                continue;
            }
            getSort().writeSortInData(pnd_Local_Data_Tbl_Unit, pnd_Local_Data_Tbl_Racf_Id, pnd_Local_Data_Tbl_Wpid_Act, pnd_Local_Data_Tbl_Wpid, pnd_Sort_Unit);          //Natural: END-ALL
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getSort().sortData(pnd_Local_Data_Tbl_Unit, pnd_Local_Data_Tbl_Racf_Id, pnd_Local_Data_Tbl_Wpid_Act, pnd_Local_Data_Tbl_Wpid);                                    //Natural: SORT BY TBL-UNIT TBL-RACF-ID TBL-WPID-ACT TBL-WPID USING #SORT-UNIT
        sort01Tbl_WpidCount397.setDec(new DbsDecimal(0));
        sort01Tbl_WpidCount.setDec(new DbsDecimal(0));
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(pnd_Local_Data_Tbl_Unit, pnd_Local_Data_Tbl_Racf_Id, pnd_Local_Data_Tbl_Wpid_Act, pnd_Local_Data_Tbl_Wpid, 
            pnd_Sort_Unit)))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            sort01Tbl_WpidCount397.setInt(sort01Tbl_WpidCount397.getInt() + 1);
            sort01Tbl_WpidCount.setInt(sort01Tbl_WpidCount.getInt() + 1);
            pnd_Rep_Parm_Pnd_Rep_Unit_Cde.setValue(pnd_Local_Data_Tbl_Unit);                                                                                              //Natural: MOVE TBL-UNIT TO #REP-UNIT-CDE #REP-EMPL-NAME
            pnd_Local_Data_Pnd_Rep_Empl_Name.setValue(pnd_Local_Data_Tbl_Unit);
            pnd_Rep_Parm_Pnd_Rep_Racf_Id.setValue(pnd_Local_Data_Tbl_Racf_Id);                                                                                            //Natural: MOVE TBL-RACF-ID TO #REP-RACF-ID
            DbsUtil.callnat(Cwfn1107.class , getCurrentProcessState(), pnd_Rep_Parm_Pnd_Rep_Racf_Id, pnd_Local_Data_Pnd_Rep_Empl_Name);                                   //Natural: CALLNAT 'CWFN1107' #REP-RACF-ID #REP-EMPL-NAME
            if (condition(Global.isEscape())) return;
            if (condition(pnd_New_Unit.getBoolean()))                                                                                                                     //Natural: IF #NEW-UNIT
            {
                DbsUtil.callnat(Cwfn3910.class , getCurrentProcessState(), pnd_Report_No, pnd_Racf_Id, pnd_Local_Data_Tbl_Unit, pnd_Floor, pnd_Bldg, pnd_Drop_Off,        //Natural: CALLNAT 'CWFN3910' #REPORT-NO #RACF-ID TBL-UNIT #FLOOR #BLDG #DROP-OFF #COMP-DATE
                    pnd_Comp_Date);
                if (condition(Global.isEscape())) return;
                pnd_New_Unit.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #NEW-UNIT
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet359 = 0;                                                                                                                             //Natural: AT TOP OF PAGE ( 1 );//Natural: DECIDE ON FIRST VALUE OF TBL-WPID-ACT;//Natural: VALUE 'B'
            if (condition((pnd_Local_Data_Tbl_Wpid_Act.equals("B"))))
            {
                decideConditionsMet359++;
                pnd_Local_Data_Pnd_Booklet_Ctr.nadd(1);                                                                                                                   //Natural: ADD 1 TO #BOOKLET-CTR
            }                                                                                                                                                             //Natural: VALUE 'F'
            else if (condition((pnd_Local_Data_Tbl_Wpid_Act.equals("F"))))
            {
                decideConditionsMet359++;
                pnd_Local_Data_Pnd_Forms_Ctr.nadd(1);                                                                                                                     //Natural: ADD 1 TO #FORMS-CTR
            }                                                                                                                                                             //Natural: VALUE 'I'
            else if (condition((pnd_Local_Data_Tbl_Wpid_Act.equals("I"))))
            {
                decideConditionsMet359++;
                pnd_Local_Data_Pnd_Inquire_Ctr.nadd(1);                                                                                                                   //Natural: ADD 1 TO #INQUIRE-CTR
            }                                                                                                                                                             //Natural: VALUE 'R'
            else if (condition((pnd_Local_Data_Tbl_Wpid_Act.equals("R"))))
            {
                decideConditionsMet359++;
                pnd_Local_Data_Pnd_Research_Ctr.nadd(1);                                                                                                                  //Natural: ADD 1 TO #RESEARCH-CTR
            }                                                                                                                                                             //Natural: VALUE 'T'
            else if (condition((pnd_Local_Data_Tbl_Wpid_Act.equals("T"))))
            {
                decideConditionsMet359++;
                pnd_Local_Data_Pnd_Trans_Ctr.nadd(1);                                                                                                                     //Natural: ADD 1 TO #TRANS-CTR
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pnd_Local_Data_Tbl_Wpid_Act.equals("X"))))
            {
                decideConditionsMet359++;
                pnd_Local_Data_Pnd_Complaint_Ctr.nadd(1);                                                                                                                 //Natural: ADD 1 TO #COMPLAINT-CTR
            }                                                                                                                                                             //Natural: VALUE 'Z'
            else if (condition((pnd_Local_Data_Tbl_Wpid_Act.equals("Z"))))
            {
                decideConditionsMet359++;
                pnd_Local_Data_Pnd_Other_Ctr.nadd(1);                                                                                                                     //Natural: ADD 1 TO #OTHER-CTR
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Local_Data_Pnd_Unclear_Ctr.reset();                                                                                                                       //Natural: RESET #UNCLEAR-CTR
            if (condition(pnd_Local_Data_Tbl_Wpid_Action.equals("U")))                                                                                                    //Natural: IF TBL-WPID-ACTION = 'U'
            {
                pnd_Local_Data_Pnd_Unclear_Ctr.nadd(1);                                                                                                                   //Natural: ADD 1 TO #UNCLEAR-CTR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Local_Data_Tbl_Wpid_Lob.equals("U")))                                                                                                       //Natural: IF TBL-WPID-LOB = 'U'
            {
                pnd_Local_Data_Pnd_Unclear_Ctr.nadd(1);                                                                                                                   //Natural: ADD 1 TO #UNCLEAR-CTR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Local_Data_Tbl_Wpid_Mbp.equals("U")))                                                                                                       //Natural: IF TBL-WPID-MBP = 'U'
            {
                pnd_Local_Data_Pnd_Unclear_Ctr.nadd(1);                                                                                                                   //Natural: ADD 1 TO #UNCLEAR-CTR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Local_Data_Tbl_Wpid_Sbp.equals("U")))                                                                                                       //Natural: IF TBL-WPID-SBP = 'U'
            {
                pnd_Local_Data_Pnd_Unclear_Ctr.nadd(1);                                                                                                                   //Natural: ADD 1 TO #UNCLEAR-CTR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Local_Data_Pnd_Unclear_Ctr.greater(getZero())))                                                                                             //Natural: IF #UNCLEAR-CTR GT 0
            {
                pnd_Local_Data_Pnd_Unclear_Ctr.reset();                                                                                                                   //Natural: RESET #UNCLEAR-CTR
                pnd_Local_Data_Pnd_Total_Unclear.nadd(1);                                                                                                                 //Natural: COMPUTE #TOTAL-UNCLEAR = #TOTAL-UNCLEAR + 1
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.callnat(Cwfn1105.class , getCurrentProcessState(), pnd_Local_Data_Tbl_Wpid, pnd_Local_Data_Pnd_Wpid_Desc);                                            //Natural: CALLNAT 'CWFN1105' TBL-WPID #WPID-DESC
            if (condition(Global.isEscape())) return;
            //*                                                                                                                                                           //Natural: AT BREAK OF TBL-WPID
            //*                                                                                                                                                           //Natural: AT BREAK OF TBL-RACF-ID;//Natural: AT BREAK OF TBL-UNIT
            //*  READ-2.
            sort01Tbl_WpidOld.setValue(pnd_Local_Data_Tbl_Wpid);                                                                                                          //Natural: END-SORT
        }
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            sort01Tbl_WpidCount397.resetBreak();
            sort01Tbl_WpidCount.resetBreak();
            atBreakEventSort01(endOfDataSort01);
        }
        endSort();
        pnd_Tbl_Run_Flag.setValue("Y");                                                                                                                                   //Natural: ON ERROR;//Natural: MOVE 'Y' TO #TBL-RUN-FLAG
        DbsUtil.callnat(Cwfn3914.class , getCurrentProcessState(), pnd_Report_Parm, pnd_Comp_Date, pnd_Tbl_Run_Flag);                                                     //Natural: CALLNAT 'CWFN3914' #REPORT-PARM #COMP-DATE #TBL-RUN-FLAG
        if (condition(Global.isEscape())) return;
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WORK-FILE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-START-DATE
    }
    private void sub_Write_Work_File() throws Exception                                                                                                                   //Natural: WRITE-WORK-FILE
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(DbsUtil.maskMatches(pnd_Local_Data_Pnd_Work_Prcss_Id,"N.....")))                                                                                    //Natural: IF #WORK-PRCSS-ID = MASK ( N..... )
        {
            pnd_Local_Data_Tbl_Wpid_Act.setValue("Z");                                                                                                                    //Natural: MOVE 'Z' TO TBL-WPID-ACT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Local_Data_Tbl_Wpid_Act.setValue(pnd_Local_Data_Pnd_Work_Prcss_Id);                                                                                       //Natural: MOVE #WORK-PRCSS-ID TO TBL-WPID-ACT
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Local_Data_Tbl_Wpid.setValue(pnd_Local_Data_Pnd_Work_Prcss_Id);                                                                                               //Natural: MOVE #WORK-PRCSS-ID TO TBL-WPID
        pnd_Local_Data_Tbl_Unit.setValue(pnd_Work_Unit);                                                                                                                  //Natural: MOVE #WORK-UNIT TO TBL-UNIT
        pnd_Local_Data_Tbl_Racf_Id.setValue(pnd_Oprtr_Cde);                                                                                                               //Natural: MOVE #OPRTR-CDE TO TBL-RACF-ID
        pnd_Local_Data_Rush_Req_Ind.setValue(pnd_Rush_Req);                                                                                                               //Natural: MOVE #RUSH-REQ TO RUSH-REQ-IND
        getWorkFiles().write(1, false, pnd_Local_Data_Pnd_Work_Record);                                                                                                   //Natural: WRITE WORK FILE 1 #WORK-RECORD
        pnd_Reccount.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #RECCOUNT
    }
    private void sub_Get_Start_Date() throws Exception                                                                                                                    //Natural: GET-START-DATE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Work_Comp_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Comp_Date);                                                                                  //Natural: MOVE EDITED #COMP-DATE TO #WORK-COMP-DATE ( EM = YYYYMMDD )
        pnd_Work_Start_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Rep_Parm_Pnd_Start_Date);                                                                   //Natural: MOVE EDITED #START-DATE TO #WORK-START-DATE ( EM = YYYYMMDD )
        pnd_Date_Diff.compute(new ComputeParameters(false, pnd_Date_Diff), pnd_Work_Comp_Date.subtract(pnd_Work_Start_Date));                                             //Natural: COMPUTE #DATE-DIFF = #WORK-COMP-DATE - #WORK-START-DATE
        if (condition(pnd_Date_Diff.greater(1)))                                                                                                                          //Natural: IF #DATE-DIFF GT 1
        {
            pnd_Work_Start_Date.nadd(1);                                                                                                                                  //Natural: COMPUTE #WORK-START-DATE = #WORK-START-DATE + 1
            pnd_Rep_Parm_Pnd_Start_Date.setValueEdited(pnd_Work_Start_Date,new ReportEditMask("YYYYMMDD"));                                                               //Natural: MOVE EDITED #WORK-START-DATE ( EM = YYYYMMDD ) TO #START-DATE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Rep_Parm_Pnd_Start_Date.setValue(pnd_Comp_Date);                                                                                                          //Natural: MOVE #COMP-DATE TO #START-DATE
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Page.nadd(1);                                                                                                                                     //Natural: COMPUTE #PAGE = #PAGE + 1
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),pnd_Env,new TabSetting(26),"CORPORATE WORKFLOW FACILITIES",new TabSetting(70),"PAGE",pnd_Page,  //Natural: WRITE ( 1 ) NOTITLE *PROGRAM #ENV 26T 'CORPORATE WORKFLOW FACILITIES' 70T 'PAGE' #PAGE ( NL = 5 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 25T 'WORK REQUESTS LOGGED AND INDEXED' 72T *TIMX ( EM = HH':'II' 'AP )
                        new NumericLength (5), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),new 
                        TabSetting(25),"WORK REQUESTS LOGGED AND INDEXED",new TabSetting(72),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"));
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                    if (condition(pnd_New_Racf.getBoolean()))                                                                                                             //Natural: IF #NEW-RACF
                    {
                        DbsUtil.callnat(Cwfn1103.class , getCurrentProcessState(), pnd_Rep_Parm_Pnd_Rep_Unit_Cde, pnd_Unit_Name);                                         //Natural: CALLNAT 'CWFN1103' #REP-UNIT-CDE #UNIT-NAME
                        if (condition(Global.isEscape())) return;
                        getReports().write(1, ReportOption.NOTITLE,"UNIT      :",pnd_Rep_Parm_Pnd_Rep_Unit_Cde,pnd_Unit_Name);                                            //Natural: WRITE ( 1 ) 'UNIT      :' #REP-UNIT-CDE #UNIT-NAME
                        getReports().write(1, ReportOption.NOTITLE,"START-DATE:",pnd_Rep_Parm_Pnd_Work_Start_Date_A);                                                     //Natural: WRITE ( 1 ) 'START-DATE:' #WORK-START-DATE-A
                        getReports().write(1, ReportOption.NOTITLE,"END-DATE  :",pnd_Rep_Parm_Pnd_Work_End_Date_A);                                                       //Natural: WRITE ( 1 ) 'END-DATE  :' #WORK-END-DATE-A
                        if (condition(pnd_Local_Data_Pnd_Rep_Empl_Name.equals("NOT FOUND")))                                                                              //Natural: IF #REP-EMPL-NAME = 'NOT FOUND'
                        {
                            pnd_Local_Data_Pnd_Rep_Empl_Name.setValue(pnd_Rep_Parm_Pnd_Rep_Racf_Id);                                                                      //Natural: MOVE #REP-RACF-ID TO #REP-EMPL-NAME
                        }                                                                                                                                                 //Natural: END-IF
                        getReports().write(1, ReportOption.NOTITLE,"EMPLOYEE  :",pnd_Local_Data_Pnd_Rep_Empl_Name);                                                       //Natural: WRITE ( 1 ) 'EMPLOYEE  :' #REP-EMPL-NAME
                        pnd_New_Racf.setValue(false);                                                                                                                     //Natural: MOVE FALSE TO #NEW-RACF
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE,"=",new RepeatItem(79));                                                                                   //Natural: WRITE ( 1 ) '=' ( 79 )
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                    if (condition(! (pnd_Local_Data_Pnd_End_Of_Data.getBoolean())))                                                                                       //Natural: IF NOT #END-OF-DATA
                    {
                        getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(62),"NUMBER OF",NEWLINE,new ColumnSpacing(10),"WPID",new ColumnSpacing(19),"DESCRIPTION",new  //Natural: WRITE ( 1 ) 62X 'NUMBER OF' / 10X 'WPID' 19X 'DESCRIPTION' 18X 'REQUESTS' / 9X '-' ( 6 ) '-' ( 45 ) '-' ( 9 )
                            ColumnSpacing(18),"REQUESTS",NEWLINE,new ColumnSpacing(9),"-",new RepeatItem(6),"-",new RepeatItem(45),"-",new RepeatItem(9));
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        pnd_Tbl_Run_Flag.setValue(" ");                                                                                                                                   //Natural: MOVE ' ' TO #TBL-RUN-FLAG
        DbsUtil.callnat(Cwfn3914.class , getCurrentProcessState(), pnd_Report_Parm, pnd_Comp_Date, pnd_Tbl_Run_Flag);                                                     //Natural: CALLNAT 'CWFN3914' #REPORT-PARM #COMP-DATE #TBL-RUN-FLAG
        if (condition(Global.isEscape())) return;
    };                                                                                                                                                                    //Natural: END-ERROR

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean pnd_Local_Data_Tbl_WpidIsBreak = pnd_Local_Data_Tbl_Wpid.isBreak(endOfData);
        boolean pnd_Local_Data_Tbl_Racf_IdIsBreak = pnd_Local_Data_Tbl_Racf_Id.isBreak(endOfData);
        boolean pnd_Local_Data_Tbl_UnitIsBreak = pnd_Local_Data_Tbl_Unit.isBreak(endOfData);
        if (condition(pnd_Local_Data_Tbl_WpidIsBreak || pnd_Local_Data_Tbl_Racf_IdIsBreak || pnd_Local_Data_Tbl_UnitIsBreak))
        {
            pnd_Local_Data_Pnd_Wpid_Code.setValue(sort01Tbl_WpidOld);                                                                                                     //Natural: MOVE OLD ( TBL-WPID ) TO #WPID-CODE
            pnd_Local_Data_Pnd_Wpid_Count.setValue(sort01Tbl_WpidCount397);                                                                                               //Natural: MOVE COUNT ( TBL-WPID ) TO #WPID-COUNT
            pnd_Local_Data_Pnd_Total_Logged.nadd(pnd_Local_Data_Pnd_Wpid_Count);                                                                                          //Natural: COMPUTE #TOTAL-LOGGED = #TOTAL-LOGGED + #WPID-COUNT
            getReports().write(1, ReportOption.NOTITLE,pnd_Local_Data_Pnd_Pad_Space,pnd_Local_Data_Pnd_Wpid_Code,pnd_Local_Data_Pnd_Wpid_Desc,pnd_Local_Data_Pnd_Wpid_Count,  //Natural: WRITE ( 1 ) #PAD-SPACE #WPID-CODE #WPID-DESC #WPID-COUNT ( EM = ZZ,ZZ9 )
                new ReportEditMask ("ZZ,ZZ9"));
            if (condition(Global.isEscape())) return;
            sort01Tbl_WpidCount397.setDec(new DbsDecimal(0));                                                                                                             //Natural: END-BREAK
        }
        if (condition(pnd_Local_Data_Tbl_Racf_IdIsBreak || pnd_Local_Data_Tbl_UnitIsBreak))
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(35),"TOTAL ",new TabSetting(63),pnd_Local_Data_Pnd_Total_Logged,NEWLINE,NEWLINE,"NUMBER OF WORK REQUESTS LOGGED AND INDEXED WITH 'U' IN ANY ELEMENT:",pnd_Local_Data_Pnd_Total_Unclear,  //Natural: WRITE ( 1 ) NOTITLE / 35T 'TOTAL ' 63T #TOTAL-LOGGED / / 'NUMBER OF WORK REQUESTS LOGGED AND INDEXED WITH "U" IN ANY ELEMENT:' #TOTAL-UNCLEAR ( EM = ZZ,ZZ9 )
                new ReportEditMask ("ZZ,ZZ9"));
            if (condition(Global.isEscape())) return;
            pnd_Rep_Parm_Pnd_Rep_Racf_Id.setValue(pnd_Local_Data_Tbl_Racf_Id);                                                                                            //Natural: MOVE TBL-RACF-ID TO #REP-RACF-ID
            if (condition(getReports().getAstLineCount(1).greater(53)))                                                                                                   //Natural: IF *LINE-COUNT ( 1 ) GT 53
            {
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,NEWLINE,"BOOKLETS  FORMS",NEWLINE,"REQUESTED REQUESTED INQUIRIES RESEARCH TRANSACTIONS COMPLAINTS  OTHERS   ",NEWLINE,"--------- --------- --------- -------- ------------ ----------  ---------",NEWLINE,new  //Natural: WRITE ( 1 ) NOTITLE / / '*' ( 79 ) // 'BOOKLETS  FORMS' / 'REQUESTED REQUESTED INQUIRIES RESEARCH TRANSACTIONS COMPLAINTS  OTHERS   ' / '--------- --------- --------- -------- ------------ ----------  ---------' / 4T #BOOKLET-CTR 14T #FORMS-CTR 24T #INQUIRE-CTR 33T #RESEARCH-CTR 46T #TRANS-CTR 57T #COMPLAINT-CTR 68T #OTHER-CTR
                TabSetting(4),pnd_Local_Data_Pnd_Booklet_Ctr,new TabSetting(14),pnd_Local_Data_Pnd_Forms_Ctr,new TabSetting(24),pnd_Local_Data_Pnd_Inquire_Ctr,new 
                TabSetting(33),pnd_Local_Data_Pnd_Research_Ctr,new TabSetting(46),pnd_Local_Data_Pnd_Trans_Ctr,new TabSetting(57),pnd_Local_Data_Pnd_Complaint_Ctr,new 
                TabSetting(68),pnd_Local_Data_Pnd_Other_Ctr);
            if (condition(Global.isEscape())) return;
            pnd_Local_Data_Pnd_Booklet_Ctr.reset();                                                                                                                       //Natural: RESET #BOOKLET-CTR #FORMS-CTR #INQUIRE-CTR #RESEARCH-CTR #TRANS-CTR #COMPLAINT-CTR #OTHER-CTR #TOTAL-LOGGED #TOTAL-UNCLEAR
            pnd_Local_Data_Pnd_Forms_Ctr.reset();
            pnd_Local_Data_Pnd_Inquire_Ctr.reset();
            pnd_Local_Data_Pnd_Research_Ctr.reset();
            pnd_Local_Data_Pnd_Trans_Ctr.reset();
            pnd_Local_Data_Pnd_Complaint_Ctr.reset();
            pnd_Local_Data_Pnd_Other_Ctr.reset();
            pnd_Local_Data_Pnd_Total_Logged.reset();
            pnd_Local_Data_Pnd_Total_Unclear.reset();
            pnd_New_Racf.setValue(true);                                                                                                                                  //Natural: MOVE TRUE TO #NEW-RACF
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pnd_Local_Data_Tbl_UnitIsBreak))
        {
            pnd_Local_Data_Pnd_Booklet_Ctr.reset();                                                                                                                       //Natural: RESET #BOOKLET-CTR #FORMS-CTR #INQUIRE-CTR #RESEARCH-CTR #TRANS-CTR #COMPLAINT-CTR #TOTAL-LOGGED #TOTAL-UNCLEAR #PAGE
            pnd_Local_Data_Pnd_Forms_Ctr.reset();
            pnd_Local_Data_Pnd_Inquire_Ctr.reset();
            pnd_Local_Data_Pnd_Research_Ctr.reset();
            pnd_Local_Data_Pnd_Trans_Ctr.reset();
            pnd_Local_Data_Pnd_Complaint_Ctr.reset();
            pnd_Local_Data_Pnd_Total_Logged.reset();
            pnd_Local_Data_Pnd_Total_Unclear.reset();
            pnd_Page.reset();
            DbsUtil.callnat(Cwfn3911.class , getCurrentProcessState());                                                                                                   //Natural: CALLNAT 'CWFN3911'
            if (condition(Global.isEscape())) return;
            pnd_New_Unit.setValue(true);                                                                                                                                  //Natural: MOVE TRUE TO #NEW-UNIT #NEW-RACF
            pnd_New_Racf.setValue(true);
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=80 PS=58");
    }
}
