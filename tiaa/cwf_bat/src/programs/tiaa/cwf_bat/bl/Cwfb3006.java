/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:26:25 PM
**        * FROM NATURAL PROGRAM : Cwfb3006
************************************************************
**        * FILE NAME            : Cwfb3006.java
**        * CLASS NAME           : Cwfb3006
**        * INSTANCE NAME        : Cwfb3006
************************************************************
**SAG GENERATOR: SHELL-TIAA                       VERSION: 3.2.2
**SAG TITLE: REPORT 6
**SAG SYSTEM: CRPCWF
************************************************************************
* PROGRAM  : CWFB3006
* SYSTEM   : CRPCWF
* TITLE    : REPORT 6
* GENERATED: AUG 11,93 AT 10:46 AM
* FUNCTION : REPORT OF OPEN CASES
* MOD DATE   MOD BY      DESCRIPTION OF CHANGES
* NOV. 13 95 J. HARGRAVE ADDED EN ROUTE TO STATUS's to acceptance
*                        CRITERIA
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3006 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Work_Record;
    private DbsField pnd_Work_Record_Tbl_Racf;
    private DbsField pnd_Work_Record_Tbl_Calendar_Days;
    private DbsField pnd_Work_Record_Tbl_Tiaa_Bsnss_Days;
    private DbsField pnd_Work_Record_Tbl_Business_Days;
    private DbsField pnd_Work_Record_Tbl_Wpid_Act;
    private DbsField pnd_Work_Record_Tbl_Wpid;

    private DbsGroup pnd_Work_Record__R_Field_1;
    private DbsField pnd_Work_Record_Tbl_Wpid_Action;
    private DbsField pnd_Work_Record_Tbl_Status_Key;

    private DbsGroup pnd_Work_Record__R_Field_2;
    private DbsField pnd_Work_Record_Tbl_Last_Chnge_Unit_Cde;
    private DbsField pnd_Work_Record_Tbl_Status_Cde;
    private DbsField pnd_Work_Record_Tbl_Log_Unit_Cde;
    private DbsField pnd_Work_Record_Tbl_Pin;
    private DbsField pnd_Work_Record_Tbl_Step_Id;
    private DbsField pnd_Work_Record_Tbl_Sort_Step_Id;
    private DbsField pnd_Work_Record_Tbl_Contracts;
    private DbsField pnd_Work_Record_Tbl_Log_Dte_Tme;
    private DbsField pnd_Work_Record_Tbl_Tiaa_Dte;
    private DbsField pnd_Work_Record_Tbl_Status_Dte;
    private DbsField pnd_Work_Record_Return_Doc_Rec_Dte_Tme;
    private DbsField pnd_Work_Record_Return_Rcvd_Dte_Tme;
    private DbsField pnd_Work_Record_Tbl_Last_Chge_Dte;
    private DbsField pnd_Work_Record_Partic_Sname;

    private DbsGroup pnd_Report_Data;
    private DbsField pnd_Report_Data_Pnd_Rep_Unit_Name;
    private DbsField pnd_Report_Data_Pnd_Rep_Unit_Cde;

    private DbsGroup pnd_Report_Data__R_Field_3;
    private DbsField pnd_Report_Data_Pnd_Rep_Unit_Id_Cde;
    private DbsField pnd_Report_Data_Pnd_Rep_Wpid;

    private DbsGroup pnd_Report_Data__R_Field_4;
    private DbsField pnd_Report_Data_Pnd_Rep_Wpid_Actn_Rqstd_Cde;
    private DbsField pnd_Report_Data_Pnd_Rep_Wpid_Lob;
    private DbsField pnd_Report_Data_Pnd_Rep_Wpid_Mbp;
    private DbsField pnd_Report_Data_Pnd_Rep_Wpid_Sbp;
    private DbsField pnd_Report_Data_Pnd_Rep_Racf_Id;
    private DbsField pnd_Report_Data_Pnd_Rep_Wpid_Name;
    private DbsField pnd_Report_Data_Pnd_Rep_Empl_Name;
    private DbsField pnd_Report_Data_Pnd_Rep_Subtotal_Ind;

    private DbsGroup pnd_Misc_Parm;
    private DbsField pnd_Misc_Parm_Pnd_Wrk_Unit_Cde;

    private DbsGroup pnd_Misc_Parm__R_Field_5;
    private DbsField pnd_Misc_Parm_Pnd_Wrk_Unit_Id_Cde;
    private DbsField pnd_Misc_Parm_Pnd_Wrk_Unit_Suffix;
    private DbsField pnd_Misc_Parm_Pnd_Save_Action;
    private DbsField pnd_Misc_Parm_Pnd_Wpid;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Action;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Code;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Count;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Desc;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Sname;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Text;
    private DbsField pnd_Misc_Parm_Pnd_Rep_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Total_Count;
    private DbsField pnd_Misc_Parm_Pnd_Sub_Count;
    private DbsField pnd_Misc_Parm_Pnd_Booklet_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Forms_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Inquire_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Research_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Trans_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Complaint_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Other_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Read_Count;
    private DbsField pnd_Misc_Parm_Pnd_Actve_Unque_Key;
    private DbsField pnd_Misc_Parm_Pnd_Todays_Time;
    private DbsField pnd_Misc_Parm_Pnd_Return_Code;
    private DbsField pnd_Misc_Parm_Pnd_Return_Msg;
    private DbsField pnd_Misc_Parm_Pnd_Status_Sname;
    private DbsField pnd_Misc_Parm_Pnd_Return_Doc_Txt;
    private DbsField pnd_Misc_Parm_Pnd_Return_Rcvd_Txt;
    private DbsField pnd_Misc_Parm_Pnd_Confirmed;
    private DbsField pnd_Page;
    private DbsField pnd_New_Unit;
    private DbsField pnd_Env;

    private DbsGroup cwf_Master_Index;
    private DbsField cwf_Master_Index_Pin_Nbr;
    private DbsField cwf_Master_Index_Rqst_Log_Dte_Tme;

    private DbsGroup cwf_Master_Index__R_Field_6;
    private DbsField cwf_Master_Index_Rqst_Log_Index_Dte;
    private DbsField cwf_Master_Index_Rqst_Log_Index_Tme;
    private DbsField cwf_Master_Index_Rqst_Log_Oprtr_Cde;
    private DbsField cwf_Master_Index_Orgnl_Log_Dte_Tme;
    private DbsField cwf_Master_Index_Orgnl_Unit_Cde;
    private DbsField cwf_Master_Index_Work_Prcss_Id;
    private DbsField cwf_Master_Index_Unit_Cde;
    private DbsField cwf_Master_Index_Unit_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_Empl_Oprtr_Cde;

    private DbsGroup cwf_Master_Index__R_Field_7;
    private DbsField cwf_Master_Index_Empl_Racf_Id;
    private DbsField cwf_Master_Index_Last_Chnge_Dte_Tme;
    private DbsField cwf_Master_Index_Last_Chnge_Oprtr_Cde;
    private DbsField cwf_Master_Index_Last_Chnge_Unit_Cde;
    private DbsField cwf_Master_Index_Step_Id;
    private DbsField cwf_Master_Index_Admin_Unit_Cde;
    private DbsField cwf_Master_Index_Admin_Status_Cde;
    private DbsField cwf_Master_Index_Admin_Status_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_Admin_Status_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Index_Status_Cde;
    private DbsField cwf_Master_Index_Status_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_Status_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Index_Last_Updte_Dte;
    private DbsField cwf_Master_Index_Last_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_Last_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Index_Crprte_Status_Ind;
    private DbsField cwf_Master_Index_Tiaa_Rcvd_Dte;
    private DbsField cwf_Master_Index_Effctve_Dte;
    private DbsField cwf_Master_Index_Trans_Dte;
    private DbsField cwf_Master_Index_Cntrct_Nbr;
    private DbsField cwf_Master_Index_Tbl_Calendar_Days;
    private DbsField cwf_Master_Index_Tbl_Tiaa_Bsnss_Days;
    private DbsField cwf_Master_Index_Return_Doc_Rec_Dte_Tme;
    private DbsField cwf_Master_Index_Return_Rcvd_Dte_Tme;
    private DbsField cwf_Master_Index_Tbl_Business_Days;
    private DbsField cwf_Master_Index_Tbl_Pend_Days;
    private DbsField cwf_Master_Index_Tbl_Ext_Pend_Days;
    private DbsField cwf_Master_Index_Tbl_Ext_Pend_Bsnss_Days;
    private DbsField cwf_Master_Index_Pnd_Partic_Sname;
    private DbsField cwf_Master_Index_Work_List_Ind;
    private DbsField pnd_Report_No;
    private DbsField pnd_Racf_Id;
    private DbsField pnd_Floor;
    private DbsField pnd_Bldg;
    private DbsField pnd_Drop_Off;
    private DbsField pnd_Unit_Name;
    private DbsField pnd_Run_Date;
    private DbsField pnd_Comp_Date;
    private DbsField pnd_Report_Parm;
    private DbsField pnd_Parm_Unit;
    private DbsField pnd_Tbl_Run_Flag;
    private DbsField pnd_Parm_Type;

    private DbsRecord internalLoopRecord;
    private DbsField sort01Tbl_WpidOld;
    private DbsField sort01Tbl_Wpid_ActOld;
    private DbsField sort01Tbl_WpidCount343;
    private DbsField sort01Tbl_WpidCount;
    private DbsField sort01Tbl_Log_Unit_CdeOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Work_Record = localVariables.newGroupInRecord("pnd_Work_Record", "#WORK-RECORD");
        pnd_Work_Record_Tbl_Racf = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Racf", "TBL-RACF", FieldType.STRING, 8);
        pnd_Work_Record_Tbl_Calendar_Days = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Calendar_Days", "TBL-CALENDAR-DAYS", FieldType.NUMERIC, 
            5);
        pnd_Work_Record_Tbl_Tiaa_Bsnss_Days = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Tiaa_Bsnss_Days", "TBL-TIAA-BSNSS-DAYS", FieldType.NUMERIC, 
            5);
        pnd_Work_Record_Tbl_Business_Days = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Business_Days", "TBL-BUSINESS-DAYS", FieldType.NUMERIC, 
            5, 1);
        pnd_Work_Record_Tbl_Wpid_Act = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Wpid_Act", "TBL-WPID-ACT", FieldType.STRING, 1);
        pnd_Work_Record_Tbl_Wpid = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Wpid", "TBL-WPID", FieldType.STRING, 6);

        pnd_Work_Record__R_Field_1 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record__R_Field_1", "REDEFINE", pnd_Work_Record_Tbl_Wpid);
        pnd_Work_Record_Tbl_Wpid_Action = pnd_Work_Record__R_Field_1.newFieldInGroup("pnd_Work_Record_Tbl_Wpid_Action", "TBL-WPID-ACTION", FieldType.STRING, 
            1);
        pnd_Work_Record_Tbl_Status_Key = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Status_Key", "TBL-STATUS-KEY", FieldType.STRING, 13);

        pnd_Work_Record__R_Field_2 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record__R_Field_2", "REDEFINE", pnd_Work_Record_Tbl_Status_Key);
        pnd_Work_Record_Tbl_Last_Chnge_Unit_Cde = pnd_Work_Record__R_Field_2.newFieldInGroup("pnd_Work_Record_Tbl_Last_Chnge_Unit_Cde", "TBL-LAST-CHNGE-UNIT-CDE", 
            FieldType.STRING, 8);
        pnd_Work_Record_Tbl_Status_Cde = pnd_Work_Record__R_Field_2.newFieldInGroup("pnd_Work_Record_Tbl_Status_Cde", "TBL-STATUS-CDE", FieldType.STRING, 
            4);
        pnd_Work_Record_Tbl_Log_Unit_Cde = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Log_Unit_Cde", "TBL-LOG-UNIT-CDE", FieldType.STRING, 8);
        pnd_Work_Record_Tbl_Pin = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Pin", "TBL-PIN", FieldType.NUMERIC, 12);
        pnd_Work_Record_Tbl_Step_Id = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Step_Id", "TBL-STEP-ID", FieldType.STRING, 6);
        pnd_Work_Record_Tbl_Sort_Step_Id = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Sort_Step_Id", "TBL-SORT-STEP-ID", FieldType.STRING, 6);
        pnd_Work_Record_Tbl_Contracts = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Contracts", "TBL-CONTRACTS", FieldType.STRING, 8);
        pnd_Work_Record_Tbl_Log_Dte_Tme = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Log_Dte_Tme", "TBL-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Work_Record_Tbl_Tiaa_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Tiaa_Dte", "TBL-TIAA-DTE", FieldType.DATE);
        pnd_Work_Record_Tbl_Status_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Status_Dte", "TBL-STATUS-DTE", FieldType.TIME);
        pnd_Work_Record_Return_Doc_Rec_Dte_Tme = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Return_Doc_Rec_Dte_Tme", "RETURN-DOC-REC-DTE-TME", FieldType.TIME);
        pnd_Work_Record_Return_Rcvd_Dte_Tme = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Return_Rcvd_Dte_Tme", "RETURN-RCVD-DTE-TME", FieldType.TIME);
        pnd_Work_Record_Tbl_Last_Chge_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Last_Chge_Dte", "TBL-LAST-CHGE-DTE", FieldType.NUMERIC, 
            15);
        pnd_Work_Record_Partic_Sname = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Partic_Sname", "PARTIC-SNAME", FieldType.STRING, 7);

        pnd_Report_Data = localVariables.newGroupInRecord("pnd_Report_Data", "#REPORT-DATA");
        pnd_Report_Data_Pnd_Rep_Unit_Name = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Unit_Name", "#REP-UNIT-NAME", FieldType.STRING, 45);
        pnd_Report_Data_Pnd_Rep_Unit_Cde = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Unit_Cde", "#REP-UNIT-CDE", FieldType.STRING, 8);

        pnd_Report_Data__R_Field_3 = pnd_Report_Data.newGroupInGroup("pnd_Report_Data__R_Field_3", "REDEFINE", pnd_Report_Data_Pnd_Rep_Unit_Cde);
        pnd_Report_Data_Pnd_Rep_Unit_Id_Cde = pnd_Report_Data__R_Field_3.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Unit_Id_Cde", "#REP-UNIT-ID-CDE", FieldType.STRING, 
            5);
        pnd_Report_Data_Pnd_Rep_Wpid = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Wpid", "#REP-WPID", FieldType.STRING, 6);

        pnd_Report_Data__R_Field_4 = pnd_Report_Data.newGroupInGroup("pnd_Report_Data__R_Field_4", "REDEFINE", pnd_Report_Data_Pnd_Rep_Wpid);
        pnd_Report_Data_Pnd_Rep_Wpid_Actn_Rqstd_Cde = pnd_Report_Data__R_Field_4.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Wpid_Actn_Rqstd_Cde", "#REP-WPID-ACTN-RQSTD-CDE", 
            FieldType.STRING, 1);
        pnd_Report_Data_Pnd_Rep_Wpid_Lob = pnd_Report_Data__R_Field_4.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Wpid_Lob", "#REP-WPID-LOB", FieldType.STRING, 
            2);
        pnd_Report_Data_Pnd_Rep_Wpid_Mbp = pnd_Report_Data__R_Field_4.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Wpid_Mbp", "#REP-WPID-MBP", FieldType.STRING, 
            1);
        pnd_Report_Data_Pnd_Rep_Wpid_Sbp = pnd_Report_Data__R_Field_4.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Wpid_Sbp", "#REP-WPID-SBP", FieldType.STRING, 
            2);
        pnd_Report_Data_Pnd_Rep_Racf_Id = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Racf_Id", "#REP-RACF-ID", FieldType.STRING, 8);
        pnd_Report_Data_Pnd_Rep_Wpid_Name = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Wpid_Name", "#REP-WPID-NAME", FieldType.STRING, 45);
        pnd_Report_Data_Pnd_Rep_Empl_Name = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Empl_Name", "#REP-EMPL-NAME", FieldType.STRING, 30);
        pnd_Report_Data_Pnd_Rep_Subtotal_Ind = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Subtotal_Ind", "#REP-SUBTOTAL-IND", FieldType.STRING, 
            1);

        pnd_Misc_Parm = localVariables.newGroupInRecord("pnd_Misc_Parm", "#MISC-PARM");
        pnd_Misc_Parm_Pnd_Wrk_Unit_Cde = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wrk_Unit_Cde", "#WRK-UNIT-CDE", FieldType.STRING, 8);

        pnd_Misc_Parm__R_Field_5 = pnd_Misc_Parm.newGroupInGroup("pnd_Misc_Parm__R_Field_5", "REDEFINE", pnd_Misc_Parm_Pnd_Wrk_Unit_Cde);
        pnd_Misc_Parm_Pnd_Wrk_Unit_Id_Cde = pnd_Misc_Parm__R_Field_5.newFieldInGroup("pnd_Misc_Parm_Pnd_Wrk_Unit_Id_Cde", "#WRK-UNIT-ID-CDE", FieldType.STRING, 
            5);
        pnd_Misc_Parm_Pnd_Wrk_Unit_Suffix = pnd_Misc_Parm__R_Field_5.newFieldInGroup("pnd_Misc_Parm_Pnd_Wrk_Unit_Suffix", "#WRK-UNIT-SUFFIX", FieldType.STRING, 
            3);
        pnd_Misc_Parm_Pnd_Save_Action = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Save_Action", "#SAVE-ACTION", FieldType.STRING, 1);
        pnd_Misc_Parm_Pnd_Wpid = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid", "#WPID", FieldType.STRING, 6);
        pnd_Misc_Parm_Pnd_Wpid_Action = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Action", "#WPID-ACTION", FieldType.STRING, 1);
        pnd_Misc_Parm_Pnd_Wpid_Code = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Code", "#WPID-CODE", FieldType.STRING, 6);
        pnd_Misc_Parm_Pnd_Wpid_Count = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Count", "#WPID-COUNT", FieldType.NUMERIC, 5);
        pnd_Misc_Parm_Pnd_Wpid_Desc = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Desc", "#WPID-DESC", FieldType.STRING, 45);
        pnd_Misc_Parm_Pnd_Wpid_Sname = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Sname", "#WPID-SNAME", FieldType.STRING, 15);
        pnd_Misc_Parm_Pnd_Wpid_Text = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Text", "#WPID-TEXT", FieldType.STRING, 56);
        pnd_Misc_Parm_Pnd_Rep_Ctr = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Rep_Ctr", "#REP-CTR", FieldType.PACKED_DECIMAL, 2);
        pnd_Misc_Parm_Pnd_Total_Count = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Total_Count", "#TOTAL-COUNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Misc_Parm_Pnd_Sub_Count = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Sub_Count", "#SUB-COUNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Misc_Parm_Pnd_Booklet_Ctr = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Booklet_Ctr", "#BOOKLET-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Misc_Parm_Pnd_Forms_Ctr = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Forms_Ctr", "#FORMS-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Misc_Parm_Pnd_Inquire_Ctr = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Inquire_Ctr", "#INQUIRE-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Misc_Parm_Pnd_Research_Ctr = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Research_Ctr", "#RESEARCH-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Misc_Parm_Pnd_Trans_Ctr = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Trans_Ctr", "#TRANS-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Misc_Parm_Pnd_Complaint_Ctr = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Complaint_Ctr", "#COMPLAINT-CTR", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Misc_Parm_Pnd_Other_Ctr = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Other_Ctr", "#OTHER-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Misc_Parm_Pnd_Read_Count = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Read_Count", "#READ-COUNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Misc_Parm_Pnd_Actve_Unque_Key = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Actve_Unque_Key", "#ACTVE-UNQUE-KEY", FieldType.STRING, 15);
        pnd_Misc_Parm_Pnd_Todays_Time = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Todays_Time", "#TODAYS-TIME", FieldType.TIME);
        pnd_Misc_Parm_Pnd_Return_Code = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Return_Code", "#RETURN-CODE", FieldType.NUMERIC, 1);
        pnd_Misc_Parm_Pnd_Return_Msg = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Return_Msg", "#RETURN-MSG", FieldType.STRING, 20);
        pnd_Misc_Parm_Pnd_Status_Sname = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Status_Sname", "#STATUS-SNAME", FieldType.STRING, 25);
        pnd_Misc_Parm_Pnd_Return_Doc_Txt = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Return_Doc_Txt", "#RETURN-DOC-TXT", FieldType.STRING, 8);
        pnd_Misc_Parm_Pnd_Return_Rcvd_Txt = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Return_Rcvd_Txt", "#RETURN-RCVD-TXT", FieldType.STRING, 8);
        pnd_Misc_Parm_Pnd_Confirmed = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Confirmed", "#CONFIRMED", FieldType.BOOLEAN, 1);
        pnd_Page = localVariables.newFieldInRecord("pnd_Page", "#PAGE", FieldType.NUMERIC, 5);
        pnd_New_Unit = localVariables.newFieldInRecord("pnd_New_Unit", "#NEW-UNIT", FieldType.BOOLEAN, 1);
        pnd_Env = localVariables.newFieldInRecord("pnd_Env", "#ENV", FieldType.STRING, 10);

        cwf_Master_Index = localVariables.newGroupInRecord("cwf_Master_Index", "CWF-MASTER-INDEX");
        cwf_Master_Index_Pin_Nbr = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12);
        cwf_Master_Index_Rqst_Log_Dte_Tme = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15);

        cwf_Master_Index__R_Field_6 = cwf_Master_Index.newGroupInGroup("cwf_Master_Index__R_Field_6", "REDEFINE", cwf_Master_Index_Rqst_Log_Dte_Tme);
        cwf_Master_Index_Rqst_Log_Index_Dte = cwf_Master_Index__R_Field_6.newFieldInGroup("cwf_Master_Index_Rqst_Log_Index_Dte", "RQST-LOG-INDEX-DTE", 
            FieldType.STRING, 8);
        cwf_Master_Index_Rqst_Log_Index_Tme = cwf_Master_Index__R_Field_6.newFieldInGroup("cwf_Master_Index_Rqst_Log_Index_Tme", "RQST-LOG-INDEX-TME", 
            FieldType.STRING, 7);
        cwf_Master_Index_Rqst_Log_Oprtr_Cde = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", FieldType.STRING, 
            8);
        cwf_Master_Index_Orgnl_Log_Dte_Tme = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Orgnl_Log_Dte_Tme", "ORGNL-LOG-DTE-TME", FieldType.STRING, 
            15);
        cwf_Master_Index_Orgnl_Unit_Cde = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", FieldType.STRING, 8);
        cwf_Master_Index_Work_Prcss_Id = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6);
        cwf_Master_Index_Unit_Cde = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8);
        cwf_Master_Index_Unit_Updte_Dte_Tme = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Unit_Updte_Dte_Tme", "UNIT-UPDTE-DTE-TME", FieldType.TIME);
        cwf_Master_Index_Empl_Oprtr_Cde = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", FieldType.STRING, 10);

        cwf_Master_Index__R_Field_7 = cwf_Master_Index.newGroupInGroup("cwf_Master_Index__R_Field_7", "REDEFINE", cwf_Master_Index_Empl_Oprtr_Cde);
        cwf_Master_Index_Empl_Racf_Id = cwf_Master_Index__R_Field_7.newFieldInGroup("cwf_Master_Index_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 
            8);
        cwf_Master_Index_Last_Chnge_Dte_Tme = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 
            15);
        cwf_Master_Index_Last_Chnge_Oprtr_Cde = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Last_Chnge_Oprtr_Cde", "LAST-CHNGE-OPRTR-CDE", FieldType.STRING, 
            8);
        cwf_Master_Index_Last_Chnge_Unit_Cde = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Last_Chnge_Unit_Cde", "LAST-CHNGE-UNIT-CDE", FieldType.STRING, 
            8);
        cwf_Master_Index_Step_Id = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Step_Id", "STEP-ID", FieldType.STRING, 6);
        cwf_Master_Index_Admin_Unit_Cde = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 8);
        cwf_Master_Index_Admin_Status_Cde = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 
            4);
        cwf_Master_Index_Admin_Status_Updte_Dte_Tme = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Admin_Status_Updte_Dte_Tme", "ADMIN-STATUS-UPDTE-DTE-TME", 
            FieldType.TIME);
        cwf_Master_Index_Admin_Status_Updte_Oprtr_Cde = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Admin_Status_Updte_Oprtr_Cde", "ADMIN-STATUS-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8);
        cwf_Master_Index_Status_Cde = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Status_Cde", "STATUS-CDE", FieldType.STRING, 4);
        cwf_Master_Index_Status_Updte_Dte_Tme = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Status_Updte_Dte_Tme", "STATUS-UPDTE-DTE-TME", FieldType.TIME);
        cwf_Master_Index_Status_Updte_Oprtr_Cde = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Status_Updte_Oprtr_Cde", "STATUS-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8);
        cwf_Master_Index_Last_Updte_Dte = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Last_Updte_Dte", "LAST-UPDTE-DTE", FieldType.NUMERIC, 8);
        cwf_Master_Index_Last_Updte_Dte_Tme = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Last_Updte_Dte_Tme", "LAST-UPDTE-DTE-TME", FieldType.TIME);
        cwf_Master_Index_Last_Updte_Oprtr_Cde = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Last_Updte_Oprtr_Cde", "LAST-UPDTE-OPRTR-CDE", FieldType.STRING, 
            8);
        cwf_Master_Index_Crprte_Status_Ind = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Crprte_Status_Ind", "CRPRTE-STATUS-IND", FieldType.STRING, 
            1);
        cwf_Master_Index_Tiaa_Rcvd_Dte = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE);
        cwf_Master_Index_Effctve_Dte = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Effctve_Dte", "EFFCTVE-DTE", FieldType.DATE);
        cwf_Master_Index_Trans_Dte = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Trans_Dte", "TRANS-DTE", FieldType.DATE);
        cwf_Master_Index_Cntrct_Nbr = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Cntrct_Nbr", "CNTRCT-NBR", FieldType.STRING, 8);
        cwf_Master_Index_Tbl_Calendar_Days = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Tbl_Calendar_Days", "TBL-CALENDAR-DAYS", FieldType.NUMERIC, 
            5);
        cwf_Master_Index_Tbl_Tiaa_Bsnss_Days = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Tbl_Tiaa_Bsnss_Days", "TBL-TIAA-BSNSS-DAYS", FieldType.NUMERIC, 
            5);
        cwf_Master_Index_Return_Doc_Rec_Dte_Tme = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Return_Doc_Rec_Dte_Tme", "RETURN-DOC-REC-DTE-TME", 
            FieldType.TIME);
        cwf_Master_Index_Return_Rcvd_Dte_Tme = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Return_Rcvd_Dte_Tme", "RETURN-RCVD-DTE-TME", FieldType.TIME);
        cwf_Master_Index_Tbl_Business_Days = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Tbl_Business_Days", "TBL-BUSINESS-DAYS", FieldType.NUMERIC, 
            5, 1);
        cwf_Master_Index_Tbl_Pend_Days = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Tbl_Pend_Days", "TBL-PEND-DAYS", FieldType.NUMERIC, 5, 1);
        cwf_Master_Index_Tbl_Ext_Pend_Days = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Tbl_Ext_Pend_Days", "TBL-EXT-PEND-DAYS", FieldType.NUMERIC, 
            5, 1);
        cwf_Master_Index_Tbl_Ext_Pend_Bsnss_Days = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Tbl_Ext_Pend_Bsnss_Days", "TBL-EXT-PEND-BSNSS-DAYS", 
            FieldType.NUMERIC, 5, 1);
        cwf_Master_Index_Pnd_Partic_Sname = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Pnd_Partic_Sname", "#PARTIC-SNAME", FieldType.STRING, 7);
        cwf_Master_Index_Work_List_Ind = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Work_List_Ind", "WORK-LIST-IND", FieldType.STRING, 3);
        pnd_Report_No = localVariables.newFieldInRecord("pnd_Report_No", "#REPORT-NO", FieldType.NUMERIC, 2);
        pnd_Racf_Id = localVariables.newFieldInRecord("pnd_Racf_Id", "#RACF-ID", FieldType.STRING, 8);
        pnd_Floor = localVariables.newFieldInRecord("pnd_Floor", "#FLOOR", FieldType.NUMERIC, 2);
        pnd_Bldg = localVariables.newFieldInRecord("pnd_Bldg", "#BLDG", FieldType.STRING, 3);
        pnd_Drop_Off = localVariables.newFieldInRecord("pnd_Drop_Off", "#DROP-OFF", FieldType.STRING, 2);
        pnd_Unit_Name = localVariables.newFieldInRecord("pnd_Unit_Name", "#UNIT-NAME", FieldType.STRING, 45);
        pnd_Run_Date = localVariables.newFieldInRecord("pnd_Run_Date", "#RUN-DATE", FieldType.STRING, 8);
        pnd_Comp_Date = localVariables.newFieldInRecord("pnd_Comp_Date", "#COMP-DATE", FieldType.STRING, 8);
        pnd_Report_Parm = localVariables.newFieldInRecord("pnd_Report_Parm", "#REPORT-PARM", FieldType.STRING, 17);
        pnd_Parm_Unit = localVariables.newFieldInRecord("pnd_Parm_Unit", "#PARM-UNIT", FieldType.STRING, 7);
        pnd_Tbl_Run_Flag = localVariables.newFieldInRecord("pnd_Tbl_Run_Flag", "#TBL-RUN-FLAG", FieldType.STRING, 1);
        pnd_Parm_Type = localVariables.newFieldInRecord("pnd_Parm_Type", "#PARM-TYPE", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        sort01Tbl_WpidOld = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Wpid_OLD", "Tbl_Wpid_OLD", FieldType.STRING, 6);
        sort01Tbl_Wpid_ActOld = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Wpid_Act_OLD", "Tbl_Wpid_Act_OLD", FieldType.STRING, 1);
        sort01Tbl_WpidCount343 = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Wpid_COUNT_343", "Tbl_Wpid_COUNT_343", FieldType.NUMERIC, 9);
        sort01Tbl_WpidCount = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Wpid_COUNT", "Tbl_Wpid_COUNT", FieldType.NUMERIC, 9);
        sort01Tbl_Log_Unit_CdeOld = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Log_Unit_Cde_OLD", "Tbl_Log_Unit_Cde_OLD", FieldType.STRING, 8);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        localVariables.reset();
        pnd_Report_No.setInitialValue(6);
        pnd_Report_Parm.setInitialValue("CWFB3006D*");
        pnd_Parm_Type.setInitialValue("D");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Cwfb3006() throws Exception
    {
        super("Cwfb3006");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("CWFB3006", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        pnd_Report_Data_Pnd_Rep_Subtotal_Ind.setValue("N");                                                                                                               //Natural: FORMAT ( 1 ) LS = 142 PS = 58;//Natural: MOVE 'N' TO #REP-SUBTOTAL-IND
        //* *********************************
        //*  COPYCODE : CWFC3000
        //*  FUNCTION : SETUP ENVONMENT MESSAGE
        //*  AUTHOR   : PATINGO, JOSEPH S.
        //* *********************************
        pnd_Env.setValue(Global.getLIBRARY_ID());                                                                                                                         //Natural: MOVE *LIBRARY-ID TO #ENV
        if (condition(pnd_Env.equals("PROJCWF") || pnd_Env.equals("PROJCWF")))                                                                                            //Natural: IF #ENV = 'PROJCWF' OR #ENV = 'PROJCWF'
        {
            pnd_Env.setValue("DEV'T ");                                                                                                                                   //Natural: MOVE 'DEV"T ' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("PRACANN")))                                                                                                                         //Natural: IF #ENV = 'PRACANN'
        {
            pnd_Env.setValue("PRACTICE");                                                                                                                                 //Natural: MOVE 'PRACTICE' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("PRODANN")))                                                                                                                         //Natural: IF #ENV = 'PRODANN'
        {
            pnd_Env.setValue(" ");                                                                                                                                        //Natural: MOVE ' ' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT034")))                                                                                                                         //Natural: IF #ENV = 'ACPT034'
        {
            pnd_Env.setValue("RGN:AT07");                                                                                                                                 //Natural: MOVE 'RGN:AT07' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT023")))                                                                                                                         //Natural: IF #ENV = 'ACPT023'
        {
            pnd_Env.setValue("RGN:AT06");                                                                                                                                 //Natural: MOVE 'RGN:AT06' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT028")))                                                                                                                         //Natural: IF #ENV = 'ACPT028'
        {
            pnd_Env.setValue("RGN:AT05");                                                                                                                                 //Natural: MOVE 'RGN:AT05' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        //* ****** END COPYCODE *************
        if (condition(Global.getLIBRARY_ID().notEquals("PRODANN")))                                                                                                       //Natural: IF *LIBRARY-ID NE 'PRODANN'
        {
            pnd_Env.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "(", pnd_Env, ")"));                                                                         //Natural: COMPRESS '(' #ENV ')' INTO #ENV LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Cwfn3912.class , getCurrentProcessState(), pnd_Comp_Date, pnd_Parm_Type);                                                                         //Natural: CALLNAT 'CWFN3912' #COMP-DATE #PARM-TYPE
        if (condition(Global.isEscape())) return;
        DbsUtil.callnat(Cwfn3913.class , getCurrentProcessState(), pnd_Report_Parm, pnd_Racf_Id, pnd_Parm_Unit, pnd_Floor, pnd_Bldg, pnd_Drop_Off, pnd_Run_Date,          //Natural: CALLNAT 'CWFN3913' #REPORT-PARM #RACF-ID #PARM-UNIT #FLOOR #BLDG #DROP-OFF #RUN-DATE #TBL-RUN-FLAG
            pnd_Tbl_Run_Flag);
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Comp_Date.lessOrEqual(pnd_Run_Date)))                                                                                                           //Natural: IF #COMP-DATE LE #RUN-DATE
        {
            DbsUtil.terminate();  if (true) return;                                                                                                                       //Natural: TERMINATE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Run_Date.setValue(pnd_Comp_Date);                                                                                                                             //Natural: MOVE #COMP-DATE TO #RUN-DATE
        //*  READ CWF-MASTER-INDEX
        READ_MASTER:                                                                                                                                                      //Natural: READ WORK 5 CWF-MASTER-INDEX
        while (condition(getWorkFiles().read(5, cwf_Master_Index)))
        {
            //*  JVH
            //*  JVH
            if (condition(!((cwf_Master_Index_Pin_Nbr.notEquals(getZero()) && ((((((cwf_Master_Index_Status_Cde.greaterOrEqual("0000") && cwf_Master_Index_Status_Cde.lessOrEqual("0899"))  //Natural: ACCEPT IF CWF-MASTER-INDEX.PIN-NBR NE 0 AND ( CWF-MASTER-INDEX.STATUS-CDE = '0000' THRU '0899' OR CWF-MASTER-INDEX.STATUS-CDE = '1000' THRU '3999' OR CWF-MASTER-INDEX.STATUS-CDE = '4998' THRU '6999' OR CWF-MASTER-INDEX.STATUS-CDE = '7000' THRU '7999' OR CWF-MASTER-INDEX.STATUS-CDE = '8000' THRU '8799' OR CWF-MASTER-INDEX.STATUS-CDE = 'LINK' )
                || (cwf_Master_Index_Status_Cde.greaterOrEqual("1000") && cwf_Master_Index_Status_Cde.lessOrEqual("3999"))) || (cwf_Master_Index_Status_Cde.greaterOrEqual("4998") 
                && cwf_Master_Index_Status_Cde.lessOrEqual("6999"))) || (cwf_Master_Index_Status_Cde.greaterOrEqual("7000") && cwf_Master_Index_Status_Cde.lessOrEqual("7999"))) 
                || (cwf_Master_Index_Status_Cde.greaterOrEqual("8000") && cwf_Master_Index_Status_Cde.lessOrEqual("8799"))) || cwf_Master_Index_Status_Cde.equals("LINK"))))))
            {
                continue;
            }
            //*    CWF-MASTER-INDEX.STATUS-CDE = '7500' THRU '7999' OR
            pnd_Work_Record.reset();                                                                                                                                      //Natural: RESET #WORK-RECORD
            pnd_Work_Record_Tbl_Wpid.setValue(cwf_Master_Index_Work_Prcss_Id);                                                                                            //Natural: MOVE CWF-MASTER-INDEX.WORK-PRCSS-ID TO #WORK-RECORD.TBL-WPID
            if (condition(DbsUtil.maskMatches(cwf_Master_Index_Work_Prcss_Id,"NN....")))                                                                                  //Natural: IF CWF-MASTER-INDEX.WORK-PRCSS-ID = MASK ( NN.... )
            {
                pnd_Work_Record_Tbl_Wpid_Act.setValue("Z");                                                                                                               //Natural: MOVE 'Z' TO #WORK-RECORD.TBL-WPID-ACT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Work_Record_Tbl_Wpid_Act.setValue(cwf_Master_Index_Work_Prcss_Id);                                                                                    //Natural: MOVE CWF-MASTER-INDEX.WORK-PRCSS-ID TO #WORK-RECORD.TBL-WPID-ACT
            }                                                                                                                                                             //Natural: END-IF
            pnd_Work_Record_Tbl_Pin.setValue(cwf_Master_Index_Pin_Nbr);                                                                                                   //Natural: MOVE CWF-MASTER-INDEX.PIN-NBR TO #WORK-RECORD.TBL-PIN
            pnd_Work_Record_Tbl_Last_Chnge_Unit_Cde.setValue(cwf_Master_Index_Last_Chnge_Unit_Cde);                                                                       //Natural: MOVE CWF-MASTER-INDEX.LAST-CHNGE-UNIT-CDE TO #WORK-RECORD.TBL-LAST-CHNGE-UNIT-CDE
            pnd_Work_Record_Tbl_Log_Unit_Cde.setValue(cwf_Master_Index_Admin_Unit_Cde);                                                                                   //Natural: MOVE CWF-MASTER-INDEX.ADMIN-UNIT-CDE TO #WORK-RECORD.TBL-LOG-UNIT-CDE
            pnd_Work_Record_Tbl_Racf.setValue(cwf_Master_Index_Empl_Racf_Id);                                                                                             //Natural: MOVE CWF-MASTER-INDEX.EMPL-RACF-ID TO #WORK-RECORD.TBL-RACF
            pnd_Work_Record_Tbl_Last_Chge_Dte.setValue(cwf_Master_Index_Last_Chnge_Dte_Tme);                                                                              //Natural: MOVE CWF-MASTER-INDEX.LAST-CHNGE-DTE-TME TO #WORK-RECORD.TBL-LAST-CHGE-DTE
            pnd_Work_Record_Tbl_Log_Dte_Tme.setValue(cwf_Master_Index_Rqst_Log_Dte_Tme);                                                                                  //Natural: MOVE CWF-MASTER-INDEX.RQST-LOG-DTE-TME TO #WORK-RECORD.TBL-LOG-DTE-TME
            pnd_Work_Record_Tbl_Tiaa_Dte.setValue(cwf_Master_Index_Tiaa_Rcvd_Dte);                                                                                        //Natural: MOVE CWF-MASTER-INDEX.TIAA-RCVD-DTE TO #WORK-RECORD.TBL-TIAA-DTE
            pnd_Work_Record_Tbl_Step_Id.setValue(cwf_Master_Index_Step_Id);                                                                                               //Natural: MOVE CWF-MASTER-INDEX.STEP-ID TO #WORK-RECORD.TBL-STEP-ID
            if (condition(pnd_Report_Data_Pnd_Rep_Subtotal_Ind.greater(" ")))                                                                                             //Natural: IF #REP-SUBTOTAL-IND GT ' '
            {
                pnd_Work_Record_Tbl_Sort_Step_Id.setValue(cwf_Master_Index_Step_Id);                                                                                      //Natural: MOVE CWF-MASTER-INDEX.STEP-ID TO #WORK-RECORD.TBL-SORT-STEP-ID
            }                                                                                                                                                             //Natural: END-IF
            pnd_Work_Record_Tbl_Status_Cde.setValue(cwf_Master_Index_Admin_Status_Cde);                                                                                   //Natural: MOVE CWF-MASTER-INDEX.ADMIN-STATUS-CDE TO #WORK-RECORD.TBL-STATUS-CDE
            pnd_Work_Record_Tbl_Status_Dte.setValue(cwf_Master_Index_Admin_Status_Updte_Dte_Tme);                                                                         //Natural: MOVE CWF-MASTER-INDEX.ADMIN-STATUS-UPDTE-DTE-TME TO #WORK-RECORD.TBL-STATUS-DTE
            pnd_Work_Record_Tbl_Contracts.setValue(cwf_Master_Index_Cntrct_Nbr);                                                                                          //Natural: MOVE CWF-MASTER-INDEX.CNTRCT-NBR TO #WORK-RECORD.TBL-CONTRACTS
            pnd_Work_Record_Tbl_Calendar_Days.setValue(cwf_Master_Index_Tbl_Calendar_Days);                                                                               //Natural: MOVE CWF-MASTER-INDEX.TBL-CALENDAR-DAYS TO #WORK-RECORD.TBL-CALENDAR-DAYS
            pnd_Work_Record_Tbl_Tiaa_Bsnss_Days.setValue(cwf_Master_Index_Tbl_Tiaa_Bsnss_Days);                                                                           //Natural: MOVE CWF-MASTER-INDEX.TBL-TIAA-BSNSS-DAYS TO #WORK-RECORD.TBL-TIAA-BSNSS-DAYS
            pnd_Work_Record_Return_Doc_Rec_Dte_Tme.setValue(cwf_Master_Index_Return_Doc_Rec_Dte_Tme);                                                                     //Natural: MOVE CWF-MASTER-INDEX.RETURN-DOC-REC-DTE-TME TO #WORK-RECORD.RETURN-DOC-REC-DTE-TME
            pnd_Work_Record_Return_Rcvd_Dte_Tme.setValue(cwf_Master_Index_Return_Rcvd_Dte_Tme);                                                                           //Natural: MOVE CWF-MASTER-INDEX.RETURN-RCVD-DTE-TME TO #WORK-RECORD.RETURN-RCVD-DTE-TME
            pnd_Work_Record_Tbl_Business_Days.setValue(cwf_Master_Index_Tbl_Business_Days);                                                                               //Natural: MOVE CWF-MASTER-INDEX.TBL-BUSINESS-DAYS TO #WORK-RECORD.TBL-BUSINESS-DAYS
            pnd_Work_Record_Partic_Sname.setValue(cwf_Master_Index_Pnd_Partic_Sname);                                                                                     //Natural: MOVE CWF-MASTER-INDEX.#PARTIC-SNAME TO #WORK-RECORD.PARTIC-SNAME
            getWorkFiles().write(1, false, pnd_Work_Record);                                                                                                              //Natural: WRITE WORK FILE 1 #WORK-RECORD
            pnd_Misc_Parm_Pnd_Read_Count.nadd(1);                                                                                                                         //Natural: ADD 1 TO #READ-COUNT
            pnd_Work_Record.reset();                                                                                                                                      //Natural: RESET #WORK-RECORD
            //*  READ-MASTER.
        }                                                                                                                                                                 //Natural: END-WORK
        READ_MASTER_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_Misc_Parm_Pnd_Read_Count.equals(getZero())))                                                                                                    //Natural: IF #READ-COUNT = 0
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            DbsUtil.callnat(Cwfn3910.class , getCurrentProcessState(), pnd_Report_No, pnd_Racf_Id, pnd_Work_Record_Tbl_Log_Unit_Cde, pnd_Floor, pnd_Bldg,                 //Natural: CALLNAT 'CWFN3910' #REPORT-NO #RACF-ID TBL-LOG-UNIT-CDE #FLOOR #BLDG #DROP-OFF #RUN-DATE
                pnd_Drop_Off, pnd_Run_Date);
            if (condition(Global.isEscape())) return;
            DbsUtil.callnat(Cwfn3915.class , getCurrentProcessState(), pnd_Report_No);                                                                                    //Natural: CALLNAT 'CWFN3915' #REPORT-NO
            if (condition(Global.isEscape())) return;
            DbsUtil.callnat(Cwfn3911.class , getCurrentProcessState());                                                                                                   //Natural: CALLNAT 'CWFN3911'
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        setControl("WB");                                                                                                                                                 //Natural: SET CONTROL 'WB'
        //* ***********************************************************************
        pnd_New_Unit.setValue(true);                                                                                                                                      //Natural: FORMAT ( 1 ) LS = 142 PS = 60;//Natural: MOVE TRUE TO #NEW-UNIT
        pnd_Page.setValue(0);                                                                                                                                             //Natural: MOVE 0 TO #PAGE
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #WORK-RECORD
        while (condition(getWorkFiles().read(1, pnd_Work_Record)))
        {
            getSort().writeSortInData(pnd_Work_Record_Tbl_Log_Unit_Cde, pnd_Work_Record_Tbl_Wpid_Act, pnd_Work_Record_Tbl_Wpid, pnd_Work_Record_Return_Doc_Rec_Dte_Tme,   //Natural: END-ALL
                pnd_Work_Record_Tbl_Tiaa_Dte, pnd_Work_Record_Tbl_Status_Cde, pnd_Work_Record_Tbl_Racf, pnd_Work_Record_Tbl_Calendar_Days, pnd_Work_Record_Tbl_Tiaa_Bsnss_Days, 
                pnd_Work_Record_Tbl_Business_Days, pnd_Work_Record_Tbl_Pin, pnd_Work_Record_Tbl_Step_Id, pnd_Work_Record_Tbl_Status_Key, pnd_Work_Record_Tbl_Contracts, 
                pnd_Work_Record_Tbl_Status_Dte, pnd_Work_Record_Return_Rcvd_Dte_Tme, pnd_Work_Record_Partic_Sname);
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getSort().sortData(pnd_Work_Record_Tbl_Log_Unit_Cde, pnd_Work_Record_Tbl_Wpid_Act, pnd_Work_Record_Tbl_Wpid, pnd_Work_Record_Return_Doc_Rec_Dte_Tme,              //Natural: SORT BY #WORK-RECORD.TBL-LOG-UNIT-CDE #WORK-RECORD.TBL-WPID-ACT #WORK-RECORD.TBL-WPID #WORK-RECORD.RETURN-DOC-REC-DTE-TME #WORK-RECORD.TBL-TIAA-DTE #WORK-RECORD.TBL-STATUS-CDE #WORK-RECORD.TBL-RACF USING #WORK-RECORD.TBL-CALENDAR-DAYS #WORK-RECORD.TBL-TIAA-BSNSS-DAYS #WORK-RECORD.TBL-BUSINESS-DAYS #WORK-RECORD.TBL-PIN #WORK-RECORD.TBL-STEP-ID #WORK-RECORD.TBL-STATUS-KEY #WORK-RECORD.TBL-CONTRACTS #WORK-RECORD.TBL-STATUS-DTE #WORK-RECORD.RETURN-RCVD-DTE-TME #WORK-RECORD.PARTIC-SNAME
            pnd_Work_Record_Tbl_Tiaa_Dte, pnd_Work_Record_Tbl_Status_Cde, pnd_Work_Record_Tbl_Racf);
        sort01Tbl_WpidCount343.setDec(new DbsDecimal(0));
        sort01Tbl_WpidCount.setDec(new DbsDecimal(0));
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(pnd_Work_Record_Tbl_Log_Unit_Cde, pnd_Work_Record_Tbl_Wpid_Act, pnd_Work_Record_Tbl_Wpid, pnd_Work_Record_Return_Doc_Rec_Dte_Tme, 
            pnd_Work_Record_Tbl_Tiaa_Dte, pnd_Work_Record_Tbl_Status_Cde, pnd_Work_Record_Tbl_Racf, pnd_Work_Record_Tbl_Calendar_Days, pnd_Work_Record_Tbl_Tiaa_Bsnss_Days, 
            pnd_Work_Record_Tbl_Business_Days, pnd_Work_Record_Tbl_Pin, pnd_Work_Record_Tbl_Step_Id, pnd_Work_Record_Tbl_Status_Key, pnd_Work_Record_Tbl_Contracts, 
            pnd_Work_Record_Tbl_Status_Dte, pnd_Work_Record_Return_Rcvd_Dte_Tme, pnd_Work_Record_Partic_Sname)))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            sort01Tbl_WpidCount343.setInt(sort01Tbl_WpidCount343.getInt() + 1);
            sort01Tbl_WpidCount.setInt(sort01Tbl_WpidCount.getInt() + 1);
            pnd_Report_Data_Pnd_Rep_Unit_Cde.setValue(pnd_Work_Record_Tbl_Log_Unit_Cde);                                                                                  //Natural: MOVE TBL-LOG-UNIT-CDE TO #REP-UNIT-CDE
            if (condition(pnd_New_Unit.getBoolean()))                                                                                                                     //Natural: IF #NEW-UNIT
            {
                DbsUtil.callnat(Cwfn3910.class , getCurrentProcessState(), pnd_Report_No, pnd_Racf_Id, pnd_Work_Record_Tbl_Log_Unit_Cde, pnd_Floor, pnd_Bldg,             //Natural: CALLNAT 'CWFN3910' #REPORT-NO #RACF-ID TBL-LOG-UNIT-CDE #FLOOR #BLDG #DROP-OFF #RUN-DATE
                    pnd_Drop_Off, pnd_Run_Date);
                if (condition(Global.isEscape())) return;
                DbsUtil.callnat(Cwfn1103.class , getCurrentProcessState(), pnd_Work_Record_Tbl_Log_Unit_Cde, pnd_Report_Data_Pnd_Rep_Unit_Name);                          //Natural: CALLNAT 'CWFN1103' TBL-LOG-UNIT-CDE #REP-UNIT-NAME
                if (condition(Global.isEscape())) return;
                pnd_New_Unit.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #NEW-UNIT
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet284 = 0;                                                                                                                             //Natural: AT TOP OF PAGE ( 1 );//Natural: DECIDE ON FIRST VALUE OF #WORK-RECORD.TBL-WPID-ACTION;//Natural: VALUE 'B'
            if (condition((pnd_Work_Record_Tbl_Wpid_Action.equals("B"))))
            {
                decideConditionsMet284++;
                pnd_Misc_Parm_Pnd_Booklet_Ctr.nadd(1);                                                                                                                    //Natural: ADD 1 TO #BOOKLET-CTR
            }                                                                                                                                                             //Natural: VALUE 'F'
            else if (condition((pnd_Work_Record_Tbl_Wpid_Action.equals("F"))))
            {
                decideConditionsMet284++;
                pnd_Misc_Parm_Pnd_Forms_Ctr.nadd(1);                                                                                                                      //Natural: ADD 1 TO #FORMS-CTR
            }                                                                                                                                                             //Natural: VALUE 'I'
            else if (condition((pnd_Work_Record_Tbl_Wpid_Action.equals("I"))))
            {
                decideConditionsMet284++;
                pnd_Misc_Parm_Pnd_Inquire_Ctr.nadd(1);                                                                                                                    //Natural: ADD 1 TO #INQUIRE-CTR
            }                                                                                                                                                             //Natural: VALUE 'R'
            else if (condition((pnd_Work_Record_Tbl_Wpid_Action.equals("R"))))
            {
                decideConditionsMet284++;
                pnd_Misc_Parm_Pnd_Research_Ctr.nadd(1);                                                                                                                   //Natural: ADD 1 TO #RESEARCH-CTR
            }                                                                                                                                                             //Natural: VALUE 'T'
            else if (condition((pnd_Work_Record_Tbl_Wpid_Action.equals("T"))))
            {
                decideConditionsMet284++;
                pnd_Misc_Parm_Pnd_Trans_Ctr.nadd(1);                                                                                                                      //Natural: ADD 1 TO #TRANS-CTR
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pnd_Work_Record_Tbl_Wpid_Action.equals("X"))))
            {
                decideConditionsMet284++;
                pnd_Misc_Parm_Pnd_Complaint_Ctr.nadd(1);                                                                                                                  //Natural: ADD 1 TO #COMPLAINT-CTR
            }                                                                                                                                                             //Natural: VALUES '0':'9'
            else if (condition((pnd_Work_Record_Tbl_Wpid_Action.equals("0' : '9"))))
            {
                decideConditionsMet284++;
                pnd_Misc_Parm_Pnd_Other_Ctr.nadd(1);                                                                                                                      //Natural: ADD 1 TO #OTHER-CTR
            }                                                                                                                                                             //Natural: ANY
            if (condition(decideConditionsMet284 > 0))
            {
                pnd_Misc_Parm_Pnd_Sub_Count.nadd(1);                                                                                                                      //Natural: ADD 1 TO #SUB-COUNT
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            DbsUtil.callnat(Cwfn5390.class , getCurrentProcessState(), pnd_Work_Record_Tbl_Wpid, pnd_Misc_Parm_Pnd_Wpid_Sname);                                           //Natural: CALLNAT 'CWFN5390' #WORK-RECORD.TBL-WPID #WPID-SNAME
            if (condition(Global.isEscape())) return;
            DbsUtil.callnat(Cwfn1102.class , getCurrentProcessState(), pnd_Work_Record_Tbl_Status_Key, pnd_Misc_Parm_Pnd_Status_Sname);                                   //Natural: CALLNAT 'CWFN1102' #WORK-RECORD.TBL-STATUS-KEY #STATUS-SNAME
            if (condition(Global.isEscape())) return;
            if (condition(pnd_Work_Record_Return_Doc_Rec_Dte_Tme.greater(getZero())))                                                                                     //Natural: IF #WORK-RECORD.RETURN-DOC-REC-DTE-TME GT 0
            {
                pnd_Misc_Parm_Pnd_Return_Doc_Txt.setValueEdited(pnd_Work_Record_Return_Doc_Rec_Dte_Tme,new ReportEditMask("MM/DD/YY"));                                   //Natural: MOVE EDITED #WORK-RECORD.RETURN-DOC-REC-DTE-TME ( EM = MM/DD/YY ) TO #RETURN-DOC-TXT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Misc_Parm_Pnd_Return_Doc_Txt.setValue(" ");                                                                                                           //Natural: MOVE ' ' TO #RETURN-DOC-TXT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Record_Return_Rcvd_Dte_Tme.greater(getZero())))                                                                                        //Natural: IF #WORK-RECORD.RETURN-RCVD-DTE-TME GT 0
            {
                pnd_Misc_Parm_Pnd_Return_Rcvd_Txt.setValueEdited(pnd_Work_Record_Return_Rcvd_Dte_Tme,new ReportEditMask("MM/DD/YY"));                                     //Natural: MOVE EDITED #WORK-RECORD.RETURN-RCVD-DTE-TME ( EM = MM/DD/YY ) TO #RETURN-RCVD-TXT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Misc_Parm_Pnd_Return_Rcvd_Txt.setValue(" ");                                                                                                          //Natural: MOVE ' ' TO #RETURN-RCVD-TXT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Misc_Parm_Pnd_Save_Action.notEquals(pnd_Work_Record_Tbl_Wpid_Act)))                                                                         //Natural: IF #SAVE-ACTION NE #WORK-RECORD.TBL-WPID-ACT
            {
                pnd_Misc_Parm_Pnd_Save_Action.setValue(pnd_Work_Record_Tbl_Wpid_Act);                                                                                     //Natural: MOVE #WORK-RECORD.TBL-WPID-ACT TO #SAVE-ACTION
                short decideConditionsMet320 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #SAVE-ACTION;//Natural: VALUE 'B'
                if (condition((pnd_Misc_Parm_Pnd_Save_Action.equals("B"))))
                {
                    decideConditionsMet320++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"BOOKLETS    :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'BOOKLETS    :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'F'
                else if (condition((pnd_Misc_Parm_Pnd_Save_Action.equals("F"))))
                {
                    decideConditionsMet320++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"FORMS       :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'FORMS       :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'I'
                else if (condition((pnd_Misc_Parm_Pnd_Save_Action.equals("I"))))
                {
                    decideConditionsMet320++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"INQUIRY     :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'INQUIRY     :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'R'
                else if (condition((pnd_Misc_Parm_Pnd_Save_Action.equals("R"))))
                {
                    decideConditionsMet320++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"RESEARCH    :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'RESEARCH    :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'T'
                else if (condition((pnd_Misc_Parm_Pnd_Save_Action.equals("T"))))
                {
                    decideConditionsMet320++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TRANSACTIONS:",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'TRANSACTIONS:' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'X'
                else if (condition((pnd_Misc_Parm_Pnd_Save_Action.equals("X"))))
                {
                    decideConditionsMet320++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"COMPLAINTS  :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'COMPLAINTS  :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'Z'
                else if (condition((pnd_Misc_Parm_Pnd_Save_Action.equals("Z"))))
                {
                    decideConditionsMet320++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"OTHERS      :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'OTHERS      :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                //*  PIN-EXP
            }                                                                                                                                                             //Natural: END-IF
            getReports().display(1, "//RECEIVED/AT TIAA",                                                                                                                 //Natural: DISPLAY ( 1 ) '//RECEIVED/AT TIAA' #WORK-RECORD.TBL-TIAA-DTE ( EM = MM/DD/YY ) '//DOC/REC"D' #RETURN-DOC-TXT '//REC"D IN/UNIT' #RETURN-RCVD-TXT 'BSNSS/DAYS/IN/UNIT' #WORK-RECORD.TBL-BUSINESS-DAYS ( EM = ZZZ9.9 ) '//WORK/PROCESS' #WPID-SNAME ( IS = ON AL = 12 ) '//WORK/STEP' #WORK-RECORD.TBL-STEP-ID '///STATUS' #STATUS-SNAME ( AL = 20 ) '//STATUS/DATE' #WORK-RECORD.TBL-STATUS-DTE ( EM = MM/DD/YY ) 'CLNDR/DAYS/IN/TIAA' #WORK-RECORD.TBL-CALENDAR-DAYS ( EM = ZZZ9 ) 'BSNSS/DAYS/IN/TIAA' #WORK-RECORD.TBL-TIAA-BSNSS-DAYS ( EM = ZZZ9 ) '//PARTIC/NAME' PARTIC-SNAME '///PIN' #WORK-RECORD.TBL-PIN ( EM = 999999999999 ) '//CNTRCT/NUMBER' #WORK-RECORD.TBL-CONTRACTS '///EMPLOYEE' #WORK-RECORD.TBL-RACF
            		pnd_Work_Record_Tbl_Tiaa_Dte, new ReportEditMask ("MM/DD/YY"),"//DOC/REC'D",
            		pnd_Misc_Parm_Pnd_Return_Doc_Txt,"//REC'D IN/UNIT",
            		pnd_Misc_Parm_Pnd_Return_Rcvd_Txt,"BSNSS/DAYS/IN/UNIT",
            		pnd_Work_Record_Tbl_Business_Days, new ReportEditMask ("ZZZ9.9"),"//WORK/PROCESS",
            		pnd_Misc_Parm_Pnd_Wpid_Sname, new IdenticalSuppress(true), new AlphanumericLength (12),"//WORK/STEP",
            		pnd_Work_Record_Tbl_Step_Id,"///STATUS",
            		pnd_Misc_Parm_Pnd_Status_Sname, new AlphanumericLength (20),"//STATUS/DATE",
            		pnd_Work_Record_Tbl_Status_Dte, new ReportEditMask ("MM/DD/YY"),"CLNDR/DAYS/IN/TIAA",
            		pnd_Work_Record_Tbl_Calendar_Days, new ReportEditMask ("ZZZ9"),"BSNSS/DAYS/IN/TIAA",
            		pnd_Work_Record_Tbl_Tiaa_Bsnss_Days, new ReportEditMask ("ZZZ9"),"//PARTIC/NAME",
            		pnd_Work_Record_Partic_Sname,"///PIN",
            		pnd_Work_Record_Tbl_Pin, new ReportEditMask ("999999999999"),"//CNTRCT/NUMBER",
            		pnd_Work_Record_Tbl_Contracts,"///EMPLOYEE",
            		pnd_Work_Record_Tbl_Racf);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*                                                                                                                                                           //Natural: AT BREAK OF #WORK-RECORD.TBL-WPID;//Natural: AT BREAK OF #WORK-RECORD.TBL-WPID-ACT;//Natural: AT BREAK OF TBL-LOG-UNIT-CDE
            //*  READ-2.
            sort01Tbl_WpidOld.setValue(pnd_Work_Record_Tbl_Wpid);                                                                                                         //Natural: END-SORT
            sort01Tbl_Wpid_ActOld.setValue(pnd_Work_Record_Tbl_Wpid_Act);
            sort01Tbl_Log_Unit_CdeOld.setValue(pnd_Work_Record_Tbl_Log_Unit_Cde);
        }
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            sort01Tbl_WpidCount343.resetBreak();
            sort01Tbl_WpidCount.resetBreak();
            atBreakEventSort01(endOfDataSort01);
        }
        endSort();
        //* ***********************************************************************
        //* ***********************************************************************                                                                                       //Natural: ON ERROR
        pnd_Tbl_Run_Flag.setValue("Y");                                                                                                                                   //Natural: MOVE 'Y' TO #TBL-RUN-FLAG
        DbsUtil.callnat(Cwfn3914.class , getCurrentProcessState(), pnd_Report_Parm, pnd_Run_Date, pnd_Tbl_Run_Flag);                                                      //Natural: CALLNAT 'CWFN3914' #REPORT-PARM #RUN-DATE #TBL-RUN-FLAG
        if (condition(Global.isEscape())) return;
        //* ***********************************************************************
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Page.nadd(1);                                                                                                                                     //Natural: COMPUTE #PAGE = #PAGE + 1
                    getReports().write(1, ReportOption.NOTITLE,"CWFB3006",pnd_Env,new TabSetting(52),"CORPORATE WORKFLOW FACILITIES",new TabSetting(122),"PAGE",pnd_Page, //Natural: WRITE ( 1 ) NOTITLE 'CWFB3006' #ENV 52T 'CORPORATE WORKFLOW FACILITIES' 122T 'PAGE' #PAGE ( NL = 5 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 57T 'REPORT OF OPEN CASES' 124T *TIMX ( EM = HH':'II' 'AP ) / 61T '(NOT PENDED)'
                        new NumericLength (5), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),new 
                        TabSetting(57),"REPORT OF OPEN CASES",new TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"),NEWLINE,new TabSetting(61),
                        "(NOT PENDED)");
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                    getReports().write(1, ReportOption.NOTITLE,"UNIT      :",pnd_Report_Data_Pnd_Rep_Unit_Cde,pnd_Report_Data_Pnd_Rep_Unit_Name);                         //Natural: WRITE ( 1 ) 'UNIT      :' #REP-UNIT-CDE #REP-UNIT-NAME
                    if (condition(pnd_Report_Data_Pnd_Rep_Racf_Id.greater(" ")))                                                                                          //Natural: IF #REP-RACF-ID GT ' '
                    {
                        DbsUtil.callnat(Cwfn1107.class , getCurrentProcessState(), pnd_Report_Data_Pnd_Rep_Racf_Id, pnd_Report_Data_Pnd_Rep_Empl_Name);                   //Natural: CALLNAT 'CWFN1107' #REP-RACF-ID #REP-EMPL-NAME
                        if (condition(Global.isEscape())) return;
                        getReports().write(1, ReportOption.NOTITLE,"EMPLOYEE  :",pnd_Report_Data_Pnd_Rep_Empl_Name,"(OPTIONAL)");                                         //Natural: WRITE ( 1 ) 'EMPLOYEE  :' #REP-EMPL-NAME '(OPTIONAL)'
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Report_Data_Pnd_Rep_Wpid_Actn_Rqstd_Cde.greater(" ")))                                                                              //Natural: IF #REP-WPID-ACTN-RQSTD-CDE GT ' '
                    {
                        DbsUtil.callnat(Cwfn1105.class , getCurrentProcessState(), pnd_Report_Data_Pnd_Rep_Wpid, pnd_Report_Data_Pnd_Rep_Wpid_Name);                      //Natural: CALLNAT 'CWFN1105' #REP-WPID #REP-WPID-NAME
                        if (condition(Global.isEscape())) return;
                        getReports().write(1, ReportOption.NOTITLE,"FOR WPID  :",pnd_Report_Data_Pnd_Rep_Wpid_Actn_Rqstd_Cde,pnd_Report_Data_Pnd_Rep_Wpid_Lob,            //Natural: WRITE ( 1 ) 'FOR WPID  :' #REP-WPID-ACTN-RQSTD-CDE #REP-WPID-LOB #REP-WPID-MBP #REP-WPID-SBP #REP-WPID-NAME '(OPTIONAL)'
                            pnd_Report_Data_Pnd_Rep_Wpid_Mbp,pnd_Report_Data_Pnd_Rep_Wpid_Sbp,pnd_Report_Data_Pnd_Rep_Wpid_Name,"(OPTIONAL)");
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Report_Data_Pnd_Rep_Subtotal_Ind.greater(" ")))                                                                                     //Natural: IF #REP-SUBTOTAL-IND GT ' '
                    {
                        getReports().write(1, ReportOption.NOTITLE,"WORK STEP SUBTOTAL? :",pnd_Report_Data_Pnd_Rep_Subtotal_Ind,"(OPTIONAL)");                            //Natural: WRITE ( 1 ) 'WORK STEP SUBTOTAL? :' #REP-SUBTOTAL-IND '(OPTIONAL)'
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        pnd_Tbl_Run_Flag.setValue(" ");                                                                                                                                   //Natural: MOVE ' ' TO #TBL-RUN-FLAG
        DbsUtil.callnat(Cwfn3914.class , getCurrentProcessState(), pnd_Report_Parm, pnd_Run_Date, pnd_Tbl_Run_Flag);                                                      //Natural: CALLNAT 'CWFN3914' #REPORT-PARM #RUN-DATE #TBL-RUN-FLAG
        if (condition(Global.isEscape())) return;
    };                                                                                                                                                                    //Natural: END-ERROR

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean pnd_Work_Record_Tbl_WpidIsBreak = pnd_Work_Record_Tbl_Wpid.isBreak(endOfData);
        boolean pnd_Work_Record_Tbl_Wpid_ActIsBreak = pnd_Work_Record_Tbl_Wpid_Act.isBreak(endOfData);
        boolean pnd_Work_Record_Tbl_Log_Unit_CdeIsBreak = pnd_Work_Record_Tbl_Log_Unit_Cde.isBreak(endOfData);
        if (condition(pnd_Work_Record_Tbl_WpidIsBreak || pnd_Work_Record_Tbl_Wpid_ActIsBreak || pnd_Work_Record_Tbl_Log_Unit_CdeIsBreak))
        {
            pnd_Misc_Parm_Pnd_Wpid.setValue(sort01Tbl_WpidOld);                                                                                                           //Natural: MOVE OLD ( #WORK-RECORD.TBL-WPID ) TO #WPID
            pnd_Misc_Parm_Pnd_Wpid_Action.setValue(sort01Tbl_Wpid_ActOld);                                                                                                //Natural: MOVE OLD ( #WORK-RECORD.TBL-WPID-ACT ) TO #WPID-ACTION
            pnd_Misc_Parm_Pnd_Wpid_Count.setValue(sort01Tbl_WpidCount343);                                                                                                //Natural: MOVE COUNT ( #WORK-RECORD.TBL-WPID ) TO #WPID-COUNT
            short decideConditionsMet347 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #WPID-ACTION;//Natural: VALUE 'B'
            if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("B"))))
            {
                decideConditionsMet347++;
                pnd_Misc_Parm_Pnd_Wpid_Text.setValue(DbsUtil.compress("TOTAL OPEN REQUESTS FOR BOOKLETS FOR WPID....:", pnd_Misc_Parm_Pnd_Wpid));                         //Natural: COMPRESS 'TOTAL OPEN REQUESTS FOR BOOKLETS FOR WPID....:' #WPID INTO #WPID-TEXT
            }                                                                                                                                                             //Natural: VALUE 'F'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("F"))))
            {
                decideConditionsMet347++;
                pnd_Misc_Parm_Pnd_Wpid_Text.setValue(DbsUtil.compress("TOTAL OPEN REQUESTS FOR FORMS FOR WPID.......:", pnd_Misc_Parm_Pnd_Wpid));                         //Natural: COMPRESS 'TOTAL OPEN REQUESTS FOR FORMS FOR WPID.......:' #WPID INTO #WPID-TEXT
            }                                                                                                                                                             //Natural: VALUE 'I'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("I"))))
            {
                decideConditionsMet347++;
                pnd_Misc_Parm_Pnd_Wpid_Text.setValue(DbsUtil.compress("TOTAL OPEN REQUESTS FOR INQUIRY FOR WPID.....:", pnd_Misc_Parm_Pnd_Wpid));                         //Natural: COMPRESS 'TOTAL OPEN REQUESTS FOR INQUIRY FOR WPID.....:' #WPID INTO #WPID-TEXT
            }                                                                                                                                                             //Natural: VALUE 'R'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("R"))))
            {
                decideConditionsMet347++;
                pnd_Misc_Parm_Pnd_Wpid_Text.setValue(DbsUtil.compress("TOTAL OPEN REQUESTS FOR RESEARCH FOR WPID....:", pnd_Misc_Parm_Pnd_Wpid));                         //Natural: COMPRESS 'TOTAL OPEN REQUESTS FOR RESEARCH FOR WPID....:' #WPID INTO #WPID-TEXT
            }                                                                                                                                                             //Natural: VALUE 'T'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("T"))))
            {
                decideConditionsMet347++;
                pnd_Misc_Parm_Pnd_Wpid_Text.setValue(DbsUtil.compress("TOTAL OPEN REQUESTS FOR TRANSACTIONS FOR WPID:", pnd_Misc_Parm_Pnd_Wpid));                         //Natural: COMPRESS 'TOTAL OPEN REQUESTS FOR TRANSACTIONS FOR WPID:' #WPID INTO #WPID-TEXT
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("X"))))
            {
                decideConditionsMet347++;
                pnd_Misc_Parm_Pnd_Wpid_Text.setValue(DbsUtil.compress("TOTAL OPEN REQUESTS FOR COMPLAINTS FOR WPID..:", pnd_Misc_Parm_Pnd_Wpid));                         //Natural: COMPRESS 'TOTAL OPEN REQUESTS FOR COMPLAINTS FOR WPID..:' #WPID INTO #WPID-TEXT
            }                                                                                                                                                             //Natural: VALUES 'Z'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("Z"))))
            {
                decideConditionsMet347++;
                pnd_Misc_Parm_Pnd_Wpid_Text.setValue(DbsUtil.compress("TOTAL OTHER OPEN REQUESTS FOR WPID...........:", pnd_Misc_Parm_Pnd_Wpid));                         //Natural: COMPRESS 'TOTAL OTHER OPEN REQUESTS FOR WPID...........:' #WPID INTO #WPID-TEXT
            }                                                                                                                                                             //Natural: ANY
            if (condition(decideConditionsMet347 > 0))
            {
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),pnd_Misc_Parm_Pnd_Wpid_Text," ",pnd_Misc_Parm_Pnd_Wpid_Count, new                    //Natural: WRITE ( 1 ) / 3T #WPID-TEXT ' ' #WPID-COUNT ( AD = OI ) /
                    FieldAttributes ("AD=OI"),NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            sort01Tbl_WpidCount343.setDec(new DbsDecimal(0));                                                                                                             //Natural: END-BREAK
        }
        if (condition(pnd_Work_Record_Tbl_Wpid_ActIsBreak || pnd_Work_Record_Tbl_Log_Unit_CdeIsBreak))
        {
            pnd_Misc_Parm_Pnd_Total_Count.nadd(pnd_Misc_Parm_Pnd_Sub_Count);                                                                                              //Natural: COMPUTE #TOTAL-COUNT = #TOTAL-COUNT + #SUB-COUNT
            pnd_Misc_Parm_Pnd_Wpid_Action.setValue(sort01Tbl_Wpid_ActOld);                                                                                                //Natural: MOVE OLD ( #WORK-RECORD.TBL-WPID-ACT ) TO #WPID-ACTION
            short decideConditionsMet371 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #WPID-ACTION;//Natural: VALUE 'B'
            if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("B"))))
            {
                decideConditionsMet371++;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"  TOTAL OPEN REQUESTS FOR BOOKLETS (ALL WPIDS).....:  ",pnd_Misc_Parm_Pnd_Booklet_Ctr,  //Natural: WRITE ( 1 ) / 3T '  TOTAL OPEN REQUESTS FOR BOOKLETS (ALL WPIDS).....:  ' #BOOKLET-CTR ( AD = OU ) /
                    new FieldAttributes ("AD=OU"),NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'F'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("F"))))
            {
                decideConditionsMet371++;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"  TOTAL OPEN REQUESTS FOR FORMS (ALL WPIDS)........:  ",pnd_Misc_Parm_Pnd_Forms_Ctr,  //Natural: WRITE ( 1 ) / 3T '  TOTAL OPEN REQUESTS FOR FORMS (ALL WPIDS)........:  ' #FORMS-CTR ( AD = OU ) /
                    new FieldAttributes ("AD=OU"),NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'I'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("I"))))
            {
                decideConditionsMet371++;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"  TOTAL OPEN REQUESTS FOR INQUIRY (ALL WPIDS)......:  ",pnd_Misc_Parm_Pnd_Inquire_Ctr,  //Natural: WRITE ( 1 ) / 3T '  TOTAL OPEN REQUESTS FOR INQUIRY (ALL WPIDS)......:  ' #INQUIRE-CTR ( AD = OU ) /
                    new FieldAttributes ("AD=OU"),NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'R'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("R"))))
            {
                decideConditionsMet371++;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"  TOTAL OPEN REQUESTS FOR RESEARCH (ALL WPIDS).....:  ",pnd_Misc_Parm_Pnd_Research_Ctr,  //Natural: WRITE ( 1 ) / 3T '  TOTAL OPEN REQUESTS FOR RESEARCH (ALL WPIDS).....:  ' #RESEARCH-CTR ( AD = OU ) /
                    new FieldAttributes ("AD=OU"),NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'T'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("T"))))
            {
                decideConditionsMet371++;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"  TOTAL OPEN REQUESTS FOR TRANSACTIONS (ALL WPIDS).:  ",pnd_Misc_Parm_Pnd_Trans_Ctr,  //Natural: WRITE ( 1 ) / 3T '  TOTAL OPEN REQUESTS FOR TRANSACTIONS (ALL WPIDS).:  ' #TRANS-CTR ( AD = OU ) /
                    new FieldAttributes ("AD=OU"),NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("X"))))
            {
                decideConditionsMet371++;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"  TOTAL OPEN REQUESTS FOR COMPLAINTS (ALL WPIDS)...:  ",pnd_Misc_Parm_Pnd_Complaint_Ctr,  //Natural: WRITE ( 1 ) / 3T '  TOTAL OPEN REQUESTS FOR COMPLAINTS (ALL WPIDS)...:  ' #COMPLAINT-CTR ( AD = OU ) /
                    new FieldAttributes ("AD=OU"),NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'Z'
            else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("Z"))))
            {
                decideConditionsMet371++;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"  TOTAL OTHER OPEN REQUESTS (ALL WPIDS)............:  ",pnd_Misc_Parm_Pnd_Other_Ctr,  //Natural: WRITE ( 1 ) / 3T '  TOTAL OTHER OPEN REQUESTS (ALL WPIDS)............:  ' #OTHER-CTR ( AD = OU ) /
                    new FieldAttributes ("AD=OU"),NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Misc_Parm_Pnd_Booklet_Ctr.reset();                                                                                                                        //Natural: RESET #BOOKLET-CTR #FORMS-CTR #INQUIRE-CTR #RESEARCH-CTR #TRANS-CTR #COMPLAINT-CTR #OTHER-CTR #SUB-COUNT
            pnd_Misc_Parm_Pnd_Forms_Ctr.reset();
            pnd_Misc_Parm_Pnd_Inquire_Ctr.reset();
            pnd_Misc_Parm_Pnd_Research_Ctr.reset();
            pnd_Misc_Parm_Pnd_Trans_Ctr.reset();
            pnd_Misc_Parm_Pnd_Complaint_Ctr.reset();
            pnd_Misc_Parm_Pnd_Other_Ctr.reset();
            pnd_Misc_Parm_Pnd_Sub_Count.reset();
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pnd_Work_Record_Tbl_Log_Unit_CdeIsBreak))
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(3),"    GRAND TOTAL OF ALL OPEN REQUESTS               :  ",pnd_Misc_Parm_Pnd_Total_Count,  //Natural: WRITE ( 1 ) // 3T '    GRAND TOTAL OF ALL OPEN REQUESTS               :  ' #TOTAL-COUNT ( AD = OU ) / 3T '    -------------------------------------------------'
                new FieldAttributes ("AD=OU"),NEWLINE,new TabSetting(3),"    -------------------------------------------------");
            if (condition(Global.isEscape())) return;
            pnd_Misc_Parm_Pnd_Booklet_Ctr.reset();                                                                                                                        //Natural: RESET #BOOKLET-CTR #FORMS-CTR #INQUIRE-CTR #RESEARCH-CTR #TRANS-CTR #COMPLAINT-CTR #OTHER-CTR #SUB-COUNT #TOTAL-COUNT
            pnd_Misc_Parm_Pnd_Forms_Ctr.reset();
            pnd_Misc_Parm_Pnd_Inquire_Ctr.reset();
            pnd_Misc_Parm_Pnd_Research_Ctr.reset();
            pnd_Misc_Parm_Pnd_Trans_Ctr.reset();
            pnd_Misc_Parm_Pnd_Complaint_Ctr.reset();
            pnd_Misc_Parm_Pnd_Other_Ctr.reset();
            pnd_Misc_Parm_Pnd_Sub_Count.reset();
            pnd_Misc_Parm_Pnd_Total_Count.reset();
            pnd_Report_Data_Pnd_Rep_Unit_Cde.setValue(sort01Tbl_Log_Unit_CdeOld);                                                                                         //Natural: MOVE OLD ( TBL-LOG-UNIT-CDE ) TO #REP-UNIT-CDE
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            DbsUtil.callnat(Cwfn3911.class , getCurrentProcessState());                                                                                                   //Natural: CALLNAT 'CWFN3911'
            if (condition(Global.isEscape())) return;
            pnd_New_Unit.setValue(true);                                                                                                                                  //Natural: MOVE TRUE TO #NEW-UNIT
            pnd_Page.reset();                                                                                                                                             //Natural: RESET #PAGE
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=142 PS=58");
        Global.format(1, "LS=142 PS=60");

        getReports().setDisplayColumns(1, "//RECEIVED/AT TIAA",
        		pnd_Work_Record_Tbl_Tiaa_Dte, new ReportEditMask ("MM/DD/YY"),"//DOC/REC'D",
        		pnd_Misc_Parm_Pnd_Return_Doc_Txt,"//REC'D IN/UNIT",
        		pnd_Misc_Parm_Pnd_Return_Rcvd_Txt,"BSNSS/DAYS/IN/UNIT",
        		pnd_Work_Record_Tbl_Business_Days, new ReportEditMask ("ZZZ9.9"),"//WORK/PROCESS",
        		pnd_Misc_Parm_Pnd_Wpid_Sname, new IdenticalSuppress(true), new AlphanumericLength (12),"//WORK/STEP",
        		pnd_Work_Record_Tbl_Step_Id,"///STATUS",
        		pnd_Misc_Parm_Pnd_Status_Sname, new AlphanumericLength (20),"//STATUS/DATE",
        		pnd_Work_Record_Tbl_Status_Dte, new ReportEditMask ("MM/DD/YY"),"CLNDR/DAYS/IN/TIAA",
        		pnd_Work_Record_Tbl_Calendar_Days, new ReportEditMask ("ZZZ9"),"BSNSS/DAYS/IN/TIAA",
        		pnd_Work_Record_Tbl_Tiaa_Bsnss_Days, new ReportEditMask ("ZZZ9"),"//PARTIC/NAME",
        		pnd_Work_Record_Partic_Sname,"///PIN",
        		pnd_Work_Record_Tbl_Pin, new ReportEditMask ("999999999999"),"//CNTRCT/NUMBER",
        		pnd_Work_Record_Tbl_Contracts,"///EMPLOYEE",
        		pnd_Work_Record_Tbl_Racf);
    }
}
