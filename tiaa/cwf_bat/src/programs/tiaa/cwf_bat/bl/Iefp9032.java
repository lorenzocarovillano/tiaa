/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:48:41 PM
**        * FROM NATURAL PROGRAM : Iefp9032
************************************************************
**        * FILE NAME            : Iefp9032.java
**        * CLASS NAME           : Iefp9032
**        * INSTANCE NAME        : Iefp9032
************************************************************
************************************************************************
* PROGRAM  : IEFP9032
* SYSTEM   : CRPCWF
* TITLE    : CONTROL MODULE STATISTICS SUMMARIZATION
* GENERATED: MARCH 10, 97 AT  10:00 AM
* FUNCTION : THIS PROGRAM IS RUN AT THE END OF JOB STREAM TO
*          : DELETE THE CONTROL RECORD
* HISTORY
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iefp9032 extends BLNatBase
{
    // Data Areas
    private GdaCwfg000 gdaCwfg000;
    private LdaIefl9031 ldaIefl9031;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Dte_System_Key;

    private DbsGroup pnd_Dte_System_Key__R_Field_1;
    private DbsField pnd_Dte_System_Key_Pnd_Log_Dte;
    private DbsField pnd_Dte_System_Key_Pnd_System;
    private DbsField pnd_Program;
    private DbsField pnd_Abort_Message;
    private DbsField pnd_Rept_Message;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaCwfg000 = GdaCwfg000.getInstance(getCallnatLevel());
        registerRecord(gdaCwfg000);
        if (gdaOnly) return;

        ldaIefl9031 = new LdaIefl9031();
        registerRecord(ldaIefl9031);
        registerRecord(ldaIefl9031.getVw_icw_Efm_Stats());

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Dte_System_Key = localVariables.newFieldInRecord("pnd_Dte_System_Key", "#DTE-SYSTEM-KEY", FieldType.STRING, 22);

        pnd_Dte_System_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Dte_System_Key__R_Field_1", "REDEFINE", pnd_Dte_System_Key);
        pnd_Dte_System_Key_Pnd_Log_Dte = pnd_Dte_System_Key__R_Field_1.newFieldInGroup("pnd_Dte_System_Key_Pnd_Log_Dte", "#LOG-DTE", FieldType.TIME);
        pnd_Dte_System_Key_Pnd_System = pnd_Dte_System_Key__R_Field_1.newFieldInGroup("pnd_Dte_System_Key_Pnd_System", "#SYSTEM", FieldType.STRING, 15);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Abort_Message = localVariables.newFieldArrayInRecord("pnd_Abort_Message", "#ABORT-MESSAGE", FieldType.STRING, 80, new DbsArrayController(1, 
            2));
        pnd_Rept_Message = localVariables.newFieldArrayInRecord("pnd_Rept_Message", "#REPT-MESSAGE", FieldType.STRING, 80, new DbsArrayController(1, 2));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIefl9031.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iefp9032() throws Exception
    {
        super("Iefp9032");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IEFP9032", onError);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        getReports().definePrinter(2, "ABORT");                                                                                                                           //Natural: DEFINE PRINTER ( ABORT = 1 )
        getReports().definePrinter(3, "REPT");                                                                                                                            //Natural: DEFINE PRINTER ( REPT = 2 )
        //*                                                                                                                                                               //Natural: FORMAT ( ABORT ) LS = 132 PS = 60 ZP = OFF SG = OFF;//Natural: FORMAT ( REPT ) LS = 132 PS = 60 ZP = OFF SG = OFF
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: ASSIGN #PROGRAM = *PROGRAM
        //*  DELETE THE CONTROL RECORD
        pnd_Dte_System_Key_Pnd_Log_Dte.setValue(1);                                                                                                                       //Natural: ASSIGN #LOG-DTE = 1
        pnd_Dte_System_Key_Pnd_System.setValue("CONTROL");                                                                                                                //Natural: ASSIGN #SYSTEM = 'CONTROL'
        ldaIefl9031.getVw_icw_Efm_Stats().startDatabaseFind                                                                                                               //Natural: FIND ( 1 ) ICW-EFM-STATS WITH DTE-SYSTEM-KEY = #DTE-SYSTEM-KEY
        (
        "FIND_CNTL",
        new Wc[] { new Wc("DTE_SYSTEM_KEY", "=", pnd_Dte_System_Key.getBinary(), WcType.WITH) },
        1
        );
        FIND_CNTL:
        while (condition(ldaIefl9031.getVw_icw_Efm_Stats().readNextRow("FIND_CNTL", true)))
        {
            ldaIefl9031.getVw_icw_Efm_Stats().setIfNotFoundControlFlag(false);
            if (condition(ldaIefl9031.getVw_icw_Efm_Stats().getAstCOUNTER().equals(0)))                                                                                   //Natural: IF NO RECORD FOUND
            {
                pnd_Abort_Message.getValue(1).setValue("CAN NOT DELETE THE CONTROL RECORD");                                                                              //Natural: ASSIGN #ABORT-MESSAGE ( 1 ) = 'CAN NOT DELETE THE CONTROL RECORD'
                pnd_Abort_Message.getValue(2).setValue("BECAUSE IT IS NOT ON THE FILE");                                                                                  //Natural: ASSIGN #ABORT-MESSAGE ( 2 ) = 'BECAUSE IT IS NOT ON THE FILE'
                getReports().write(2, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Efsf9030.class));                                                  //Natural: WRITE ( ABORT ) NOTITLE NOHDR USING FORM 'EFSF9030'
                DbsUtil.terminate(24);  if (true) return;                                                                                                                 //Natural: TERMINATE 24
            }                                                                                                                                                             //Natural: END-NOREC
            ldaIefl9031.getVw_icw_Efm_Stats().deleteDBRow("FIND_CNTL");                                                                                                   //Natural: DELETE ( FIND-CNTL. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  WRITE SUCCESSFUL COMPLETION REPORT
        pnd_Rept_Message.getValue(1).setValue(DbsUtil.compress("PHASE 3 - STATISTICS SUMMARIZATION FINISHED SUCCESSFULLY AT", Global.getDATU(), Global.getTIME()));       //Natural: COMPRESS 'PHASE 3 - STATISTICS SUMMARIZATION FINISHED SUCCESSFULLY AT' *DATU *TIME INTO #REPT-MESSAGE ( 1 )
        getReports().write(3, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Efsf9033.class));                                                          //Natural: WRITE ( REPT ) NOTITLE NOHDR USING FORM 'EFSF9033'
        getReports().skip(0, 2);                                                                                                                                          //Natural: SKIP ( REPT ) 2 LINES
        DbsUtil.stop();  if (true) return;                                                                                                                                //Natural: STOP
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( REPT )
        //*                                                                                                                                                               //Natural: ON ERROR
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(3, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Ieff9034.class));                                              //Natural: WRITE ( REPT ) NOTITLE NOHDR USING FORM 'IEFF9034'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(2, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Efsf9030.class));                                                          //Natural: WRITE ( ABORT ) NOTITLE NOHDR USING FORM 'EFSF9030'
        DbsUtil.terminate(24);  if (true) return;                                                                                                                         //Natural: TERMINATE 24
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(2, "LS=132 PS=60 ZP=OFF SG=OFF");
        Global.format(3, "LS=132 PS=60 ZP=OFF SG=OFF");
    }
}
