/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:59:09 PM
**        * FROM NATURAL PROGRAM : Mihp0600
************************************************************
**        * FILE NAME            : Mihp0600.java
**        * CLASS NAME           : Mihp0600
**        * INSTANCE NAME        : Mihp0600
************************************************************
************************************************************************
* PROGRAM    : MIHP0600
* SYSTEM     :
* TITLE      : PURGE MASTER INDEX BY ISN BASED ON ISN-LIST
* GENERATED  : MAY 9, 96
* FUNCTION   : THIS JOB IS USED FOR THE PURGING OF MIT RECORDS FOLLOWING
*            : THE CONVERSION FROM MIT TO WORK REQUEST HISTORY OBJECT.
*            : THE FIRST STEP CREATES A LIST OF ISN's of MIT records
*            : ON A FLAT FILE; THIS PROGRAM READS IT AND DELETE APPRO-
*            : PRIATE RECORD(S); CONTROL MAINTAINED.
*
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Mihp0600 extends BLNatBase
{
    // Data Areas
    private PdaMiha0200 pdaMiha0200;
    private LdaMihl0180 ldaMihl0180;
    private LdaMihl0181 ldaMihl0181;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Read_Work_Record;

    private DbsGroup pnd_Read_Work_Record__R_Field_1;
    private DbsField pnd_Read_Work_Record_Pnd_Isn;
    private DbsField pnd_Read_Work_Record_Pnd_Log_Dte_Tme;

    private DataAccessProgramView vw_cwf_Who_Work_Request;
    private DbsField cwf_Who_Work_Request_Rqst_Log_Dte_Tme;

    private DataAccessProgramView vw_cwf_Master_Index_View_Select;
    private DbsField cwf_Master_Index_View_Select_Crprte_Status_Chnge_Dte_Key;
    private DbsField cwf_Master_Index_View_Select_Crprte_Status_Ind;
    private DbsField cwf_Master_Index_View_Select_Last_Chnge_Invrt_Dte_Tme;
    private DbsField cwf_Master_Index_View_Select_Last_Chnge_Dte_Tme;
    private DbsField cwf_Master_Index_View_Select_Rqst_Log_Dte_Tme;

    private DataAccessProgramView vw_cwf_Master_Index_View;
    private DbsField cwf_Master_Index_View_Rqst_Log_Dte_Tme;
    private DbsField cwf_Master_Index_View_Last_Chnge_Dte_Tme;
    private DbsField cwf_Master_Index_View_Rqst_Routing_Key;

    private DataAccessProgramView vw_cwf_Who_Support_Tbl_Prevent;
    private DbsField cwf_Who_Support_Tbl_Prevent_Tbl_Prime_Key;

    private DataAccessProgramView vw_cwf_Who_Work_Request_Prevent;
    private DbsField cwf_Who_Work_Request_Prevent_Rqst_Log_Dte_Tme;
    private DbsField pnd_End_Of_Window;
    private DbsField pnd_No_Of_Records_Limit;
    private DbsField pnd_Crprte_Status_Chnge_Dte_Key_From;

    private DbsGroup pnd_Crprte_Status_Chnge_Dte_Key_From__R_Field_2;
    private DbsField pnd_Crprte_Status_Chnge_Dte_Key_From_Pnd_Crprte_Status_Ind;
    private DbsField pnd_Crprte_Status_Chnge_Dte_Key_From_Pnd_Last_Chnge_Invrtd_Dte_Tme;
    private DbsField pnd_Crprte_Status_Chnge_Dte_Key_From_Pnd_Admin_Unit_Cde;
    private DbsField pnd_Crprte_Status_Chnge_Dte_Key_From_Pnd_Actv_Ind;
    private DbsField pnd_Crprte_Status_Chnge_Dte_Key_To;

    private DbsGroup pnd_Crprte_Status_Chnge_Dte_Key_To__R_Field_3;
    private DbsField pnd_Crprte_Status_Chnge_Dte_Key_To_Pnd_Crprte_Status_Ind;
    private DbsField pnd_Crprte_Status_Chnge_Dte_Key_To_Pnd_Last_Chnge_Invrtd_Dte_Tme;
    private DbsField pnd_Crprte_Status_Chnge_Dte_Key_To_Pnd_Admin_Unit_Cde;
    private DbsField pnd_Crprte_Status_Chnge_Dte_Key_To_Pnd_Actv_Ind;
    private DbsField pnd_Start_Master_Key;

    private DbsGroup pnd_Start_Master_Key__R_Field_4;
    private DbsField pnd_Start_Master_Key_Pnd_Start_Rqst_Log_Dte_Tme;
    private DbsField pnd_Start_Master_Key_Pnd_Start_Last_Chnge_Dte_Tme;
    private DbsField pnd_Nbr_Of_Events_Purged;
    private DbsField pnd_Nbr_Of_Rqsts_Purged;
    private DbsField pnd_Inverted_Last_Chnge_Dte_Tme;
    private DbsField pnd_Convert_Date_Time_Field;

    private DbsGroup pnd_Convert_Date_Time_Field__R_Field_5;
    private DbsField pnd_Convert_Date_Time_Field_Pnd_Convert_Date_Field;
    private DbsField pnd_Convert_Date_Time_Field_Pnd_Convert_Time_Field;
    private DbsField pnd_Work_File_Record;
    private DbsField pnd_This_Program_Has_Finished_Successfully;
    private DbsField pnd_Tbl_Prime_Key_From;

    private DbsGroup pnd_Tbl_Prime_Key_From__R_Field_6;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Tbl_Key_Field;

    private DbsGroup pnd_Tbl_Prime_Key_From__R_Field_7;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Run_Status;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Starting_Date_9s_Complement;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Rest_Of_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_From_Pnd_Tbl_Actve_Ind;
    private DbsField pnd_First_Isn_Sw;
    private DbsField pnd_Restart_Sw;
    private DbsField pnd_Frst_Isn_Of_Block;
    private DbsField pnd_Isn_Save;
    private DbsField pnd_Restart_Isn_Save;
    private DbsField pnd_Log_Dte_Tme_Save;
    private DbsField pnd_No_Of_Records_Get;
    private DbsField pnd_Sorted_Isn_Cntr;
    private DbsField pnd_Run_Id_Prt;
    private DbsField pnd_Isn_Deleted;
    private DbsField pnd_Isn_Listed;
    private DbsField pnd_Duplicate_Isn_Cntr;
    private DbsField pnd_Mismatch_Cntr;
    private DbsField pnd_Mismatch_Sw;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaMiha0200 = new PdaMiha0200(localVariables);
        ldaMihl0180 = new LdaMihl0180();
        registerRecord(ldaMihl0180);
        registerRecord(ldaMihl0180.getVw_cwf_Who_Support_Tbl());
        ldaMihl0181 = new LdaMihl0181();
        registerRecord(ldaMihl0181);
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);

        // Local Variables
        pnd_Read_Work_Record = localVariables.newFieldInRecord("pnd_Read_Work_Record", "#READ-WORK-RECORD", FieldType.STRING, 19);

        pnd_Read_Work_Record__R_Field_1 = localVariables.newGroupInRecord("pnd_Read_Work_Record__R_Field_1", "REDEFINE", pnd_Read_Work_Record);
        pnd_Read_Work_Record_Pnd_Isn = pnd_Read_Work_Record__R_Field_1.newFieldInGroup("pnd_Read_Work_Record_Pnd_Isn", "#ISN", FieldType.BINARY, 4);
        pnd_Read_Work_Record_Pnd_Log_Dte_Tme = pnd_Read_Work_Record__R_Field_1.newFieldInGroup("pnd_Read_Work_Record_Pnd_Log_Dte_Tme", "#LOG-DTE-TME", 
            FieldType.STRING, 15);

        vw_cwf_Who_Work_Request = new DataAccessProgramView(new NameInfo("vw_cwf_Who_Work_Request", "CWF-WHO-WORK-REQUEST"), "CWF_WHO_WORK_REQUEST", "CWF_WORK_REQUEST");
        cwf_Who_Work_Request_Rqst_Log_Dte_Tme = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Who_Work_Request_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        registerRecord(vw_cwf_Who_Work_Request);

        vw_cwf_Master_Index_View_Select = new DataAccessProgramView(new NameInfo("vw_cwf_Master_Index_View_Select", "CWF-MASTER-INDEX-VIEW-SELECT"), "CWF_MASTER_INDEX_VIEW", 
            "CWF_MASTER_INDEX");
        cwf_Master_Index_View_Select_Crprte_Status_Chnge_Dte_Key = vw_cwf_Master_Index_View_Select.getRecord().newFieldInGroup("cwf_Master_Index_View_Select_Crprte_Status_Chnge_Dte_Key", 
            "CRPRTE-STATUS-CHNGE-DTE-KEY", FieldType.STRING, 25, RepeatingFieldStrategy.None, "CRPRTE_STATUS_CHNGE_DTE_KEY");
        cwf_Master_Index_View_Select_Crprte_Status_Chnge_Dte_Key.setDdmHeader("STATUS-CHANGE-DATE-KEY");
        cwf_Master_Index_View_Select_Crprte_Status_Chnge_Dte_Key.setSuperDescriptor(true);
        cwf_Master_Index_View_Select_Crprte_Status_Ind = vw_cwf_Master_Index_View_Select.getRecord().newFieldInGroup("cwf_Master_Index_View_Select_Crprte_Status_Ind", 
            "CRPRTE-STATUS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CRPRTE_STATUS_IND");
        cwf_Master_Index_View_Select_Crprte_Status_Ind.setDdmHeader("CORP/STAT");
        cwf_Master_Index_View_Select_Last_Chnge_Invrt_Dte_Tme = vw_cwf_Master_Index_View_Select.getRecord().newFieldInGroup("cwf_Master_Index_View_Select_Last_Chnge_Invrt_Dte_Tme", 
            "LAST-CHNGE-INVRT-DTE-TME", FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "LAST_CHNGE_INVRT_DTE_TME");
        cwf_Master_Index_View_Select_Last_Chnge_Dte_Tme = vw_cwf_Master_Index_View_Select.getRecord().newFieldInGroup("cwf_Master_Index_View_Select_Last_Chnge_Dte_Tme", 
            "LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "LAST_CHNGE_DTE_TME");
        cwf_Master_Index_View_Select_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");
        cwf_Master_Index_View_Select_Rqst_Log_Dte_Tme = vw_cwf_Master_Index_View_Select.getRecord().newFieldInGroup("cwf_Master_Index_View_Select_Rqst_Log_Dte_Tme", 
            "RQST-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Master_Index_View_Select_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        registerRecord(vw_cwf_Master_Index_View_Select);

        vw_cwf_Master_Index_View = new DataAccessProgramView(new NameInfo("vw_cwf_Master_Index_View", "CWF-MASTER-INDEX-VIEW"), "CWF_MASTER_INDEX_VIEW", 
            "CWF_MASTER_INDEX");
        cwf_Master_Index_View_Rqst_Log_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Master_Index_View_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        cwf_Master_Index_View_Last_Chnge_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "LAST_CHNGE_DTE_TME");
        cwf_Master_Index_View_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");
        cwf_Master_Index_View_Rqst_Routing_Key = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Routing_Key", "RQST-ROUTING-KEY", 
            FieldType.BINARY, 30, RepeatingFieldStrategy.None, "RQST_ROUTING_KEY");
        cwf_Master_Index_View_Rqst_Routing_Key.setSuperDescriptor(true);
        registerRecord(vw_cwf_Master_Index_View);

        vw_cwf_Who_Support_Tbl_Prevent = new DataAccessProgramView(new NameInfo("vw_cwf_Who_Support_Tbl_Prevent", "CWF-WHO-SUPPORT-TBL-PREVENT"), "CWF_WHO_SUPPORT_TBL", 
            "CWF_WHO_SUPPORT");
        cwf_Who_Support_Tbl_Prevent_Tbl_Prime_Key = vw_cwf_Who_Support_Tbl_Prevent.getRecord().newFieldInGroup("cwf_Who_Support_Tbl_Prevent_Tbl_Prime_Key", 
            "TBL-PRIME-KEY", FieldType.STRING, 53, RepeatingFieldStrategy.None, "TBL_PRIME_KEY");
        cwf_Who_Support_Tbl_Prevent_Tbl_Prime_Key.setDdmHeader("RECORD KEY");
        cwf_Who_Support_Tbl_Prevent_Tbl_Prime_Key.setSuperDescriptor(true);
        registerRecord(vw_cwf_Who_Support_Tbl_Prevent);

        vw_cwf_Who_Work_Request_Prevent = new DataAccessProgramView(new NameInfo("vw_cwf_Who_Work_Request_Prevent", "CWF-WHO-WORK-REQUEST-PREVENT"), "CWF_WHO_WORK_REQUEST", 
            "CWF_WORK_REQUEST");
        cwf_Who_Work_Request_Prevent_Rqst_Log_Dte_Tme = vw_cwf_Who_Work_Request_Prevent.getRecord().newFieldInGroup("cwf_Who_Work_Request_Prevent_Rqst_Log_Dte_Tme", 
            "RQST-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Who_Work_Request_Prevent_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        registerRecord(vw_cwf_Who_Work_Request_Prevent);

        pnd_End_Of_Window = localVariables.newFieldInRecord("pnd_End_Of_Window", "#END-OF-WINDOW", FieldType.STRING, 1);
        pnd_No_Of_Records_Limit = localVariables.newFieldInRecord("pnd_No_Of_Records_Limit", "#NO-OF-RECORDS-LIMIT", FieldType.NUMERIC, 9);
        pnd_Crprte_Status_Chnge_Dte_Key_From = localVariables.newFieldInRecord("pnd_Crprte_Status_Chnge_Dte_Key_From", "#CRPRTE-STATUS-CHNGE-DTE-KEY-FROM", 
            FieldType.STRING, 25);

        pnd_Crprte_Status_Chnge_Dte_Key_From__R_Field_2 = localVariables.newGroupInRecord("pnd_Crprte_Status_Chnge_Dte_Key_From__R_Field_2", "REDEFINE", 
            pnd_Crprte_Status_Chnge_Dte_Key_From);
        pnd_Crprte_Status_Chnge_Dte_Key_From_Pnd_Crprte_Status_Ind = pnd_Crprte_Status_Chnge_Dte_Key_From__R_Field_2.newFieldInGroup("pnd_Crprte_Status_Chnge_Dte_Key_From_Pnd_Crprte_Status_Ind", 
            "#CRPRTE-STATUS-IND", FieldType.STRING, 1);
        pnd_Crprte_Status_Chnge_Dte_Key_From_Pnd_Last_Chnge_Invrtd_Dte_Tme = pnd_Crprte_Status_Chnge_Dte_Key_From__R_Field_2.newFieldInGroup("pnd_Crprte_Status_Chnge_Dte_Key_From_Pnd_Last_Chnge_Invrtd_Dte_Tme", 
            "#LAST-CHNGE-INVRTD-DTE-TME", FieldType.NUMERIC, 15);
        pnd_Crprte_Status_Chnge_Dte_Key_From_Pnd_Admin_Unit_Cde = pnd_Crprte_Status_Chnge_Dte_Key_From__R_Field_2.newFieldInGroup("pnd_Crprte_Status_Chnge_Dte_Key_From_Pnd_Admin_Unit_Cde", 
            "#ADMIN-UNIT-CDE", FieldType.STRING, 8);
        pnd_Crprte_Status_Chnge_Dte_Key_From_Pnd_Actv_Ind = pnd_Crprte_Status_Chnge_Dte_Key_From__R_Field_2.newFieldInGroup("pnd_Crprte_Status_Chnge_Dte_Key_From_Pnd_Actv_Ind", 
            "#ACTV-IND", FieldType.STRING, 1);
        pnd_Crprte_Status_Chnge_Dte_Key_To = localVariables.newFieldInRecord("pnd_Crprte_Status_Chnge_Dte_Key_To", "#CRPRTE-STATUS-CHNGE-DTE-KEY-TO", 
            FieldType.STRING, 25);

        pnd_Crprte_Status_Chnge_Dte_Key_To__R_Field_3 = localVariables.newGroupInRecord("pnd_Crprte_Status_Chnge_Dte_Key_To__R_Field_3", "REDEFINE", pnd_Crprte_Status_Chnge_Dte_Key_To);
        pnd_Crprte_Status_Chnge_Dte_Key_To_Pnd_Crprte_Status_Ind = pnd_Crprte_Status_Chnge_Dte_Key_To__R_Field_3.newFieldInGroup("pnd_Crprte_Status_Chnge_Dte_Key_To_Pnd_Crprte_Status_Ind", 
            "#CRPRTE-STATUS-IND", FieldType.STRING, 1);
        pnd_Crprte_Status_Chnge_Dte_Key_To_Pnd_Last_Chnge_Invrtd_Dte_Tme = pnd_Crprte_Status_Chnge_Dte_Key_To__R_Field_3.newFieldInGroup("pnd_Crprte_Status_Chnge_Dte_Key_To_Pnd_Last_Chnge_Invrtd_Dte_Tme", 
            "#LAST-CHNGE-INVRTD-DTE-TME", FieldType.NUMERIC, 15);
        pnd_Crprte_Status_Chnge_Dte_Key_To_Pnd_Admin_Unit_Cde = pnd_Crprte_Status_Chnge_Dte_Key_To__R_Field_3.newFieldInGroup("pnd_Crprte_Status_Chnge_Dte_Key_To_Pnd_Admin_Unit_Cde", 
            "#ADMIN-UNIT-CDE", FieldType.STRING, 8);
        pnd_Crprte_Status_Chnge_Dte_Key_To_Pnd_Actv_Ind = pnd_Crprte_Status_Chnge_Dte_Key_To__R_Field_3.newFieldInGroup("pnd_Crprte_Status_Chnge_Dte_Key_To_Pnd_Actv_Ind", 
            "#ACTV-IND", FieldType.STRING, 1);
        pnd_Start_Master_Key = localVariables.newFieldInRecord("pnd_Start_Master_Key", "#START-MASTER-KEY", FieldType.STRING, 30);

        pnd_Start_Master_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Start_Master_Key__R_Field_4", "REDEFINE", pnd_Start_Master_Key);
        pnd_Start_Master_Key_Pnd_Start_Rqst_Log_Dte_Tme = pnd_Start_Master_Key__R_Field_4.newFieldInGroup("pnd_Start_Master_Key_Pnd_Start_Rqst_Log_Dte_Tme", 
            "#START-RQST-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Start_Master_Key_Pnd_Start_Last_Chnge_Dte_Tme = pnd_Start_Master_Key__R_Field_4.newFieldInGroup("pnd_Start_Master_Key_Pnd_Start_Last_Chnge_Dte_Tme", 
            "#START-LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 15);
        pnd_Nbr_Of_Events_Purged = localVariables.newFieldInRecord("pnd_Nbr_Of_Events_Purged", "#NBR-OF-EVENTS-PURGED", FieldType.PACKED_DECIMAL, 9);
        pnd_Nbr_Of_Rqsts_Purged = localVariables.newFieldInRecord("pnd_Nbr_Of_Rqsts_Purged", "#NBR-OF-RQSTS-PURGED", FieldType.PACKED_DECIMAL, 9);
        pnd_Inverted_Last_Chnge_Dte_Tme = localVariables.newFieldInRecord("pnd_Inverted_Last_Chnge_Dte_Tme", "#INVERTED-LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 
            15);
        pnd_Convert_Date_Time_Field = localVariables.newFieldInRecord("pnd_Convert_Date_Time_Field", "#CONVERT-DATE-TIME-FIELD", FieldType.NUMERIC, 15);

        pnd_Convert_Date_Time_Field__R_Field_5 = localVariables.newGroupInRecord("pnd_Convert_Date_Time_Field__R_Field_5", "REDEFINE", pnd_Convert_Date_Time_Field);
        pnd_Convert_Date_Time_Field_Pnd_Convert_Date_Field = pnd_Convert_Date_Time_Field__R_Field_5.newFieldInGroup("pnd_Convert_Date_Time_Field_Pnd_Convert_Date_Field", 
            "#CONVERT-DATE-FIELD", FieldType.NUMERIC, 8);
        pnd_Convert_Date_Time_Field_Pnd_Convert_Time_Field = pnd_Convert_Date_Time_Field__R_Field_5.newFieldInGroup("pnd_Convert_Date_Time_Field_Pnd_Convert_Time_Field", 
            "#CONVERT-TIME-FIELD", FieldType.NUMERIC, 7);
        pnd_Work_File_Record = localVariables.newFieldInRecord("pnd_Work_File_Record", "#WORK-FILE-RECORD", FieldType.BINARY, 4);
        pnd_This_Program_Has_Finished_Successfully = localVariables.newFieldInRecord("pnd_This_Program_Has_Finished_Successfully", "#THIS-PROGRAM-HAS-FINISHED-SUCCESSFULLY", 
            FieldType.BOOLEAN, 1);
        pnd_Tbl_Prime_Key_From = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key_From", "#TBL-PRIME-KEY-FROM", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key_From__R_Field_6 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key_From__R_Field_6", "REDEFINE", pnd_Tbl_Prime_Key_From);
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key_From__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Tbl_Scrty_Level_Ind", 
            "#TBL-SCRTY-LEVEL-IND", FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key_From__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", 
            FieldType.STRING, 20);
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key_From__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", 
            FieldType.STRING, 30);

        pnd_Tbl_Prime_Key_From__R_Field_7 = pnd_Tbl_Prime_Key_From__R_Field_6.newGroupInGroup("pnd_Tbl_Prime_Key_From__R_Field_7", "REDEFINE", pnd_Tbl_Prime_Key_From_Pnd_Tbl_Key_Field);
        pnd_Tbl_Prime_Key_From_Pnd_Run_Status = pnd_Tbl_Prime_Key_From__R_Field_7.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Run_Status", "#RUN-STATUS", 
            FieldType.STRING, 1);
        pnd_Tbl_Prime_Key_From_Pnd_Starting_Date_9s_Complement = pnd_Tbl_Prime_Key_From__R_Field_7.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Starting_Date_9s_Complement", 
            "#STARTING-DATE-9S-COMPLEMENT", FieldType.NUMERIC, 8);
        pnd_Tbl_Prime_Key_From_Pnd_Rest_Of_Tbl_Key_Field = pnd_Tbl_Prime_Key_From__R_Field_7.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Rest_Of_Tbl_Key_Field", 
            "#REST-OF-TBL-KEY-FIELD", FieldType.STRING, 21);
        pnd_Tbl_Prime_Key_From_Pnd_Tbl_Actve_Ind = pnd_Tbl_Prime_Key_From__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_From_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", 
            FieldType.STRING, 1);
        pnd_First_Isn_Sw = localVariables.newFieldInRecord("pnd_First_Isn_Sw", "#FIRST-ISN-SW", FieldType.STRING, 1);
        pnd_Restart_Sw = localVariables.newFieldInRecord("pnd_Restart_Sw", "#RESTART-SW", FieldType.STRING, 1);
        pnd_Frst_Isn_Of_Block = localVariables.newFieldInRecord("pnd_Frst_Isn_Of_Block", "#FRST-ISN-OF-BLOCK", FieldType.NUMERIC, 9);
        pnd_Isn_Save = localVariables.newFieldInRecord("pnd_Isn_Save", "#ISN-SAVE", FieldType.NUMERIC, 9);
        pnd_Restart_Isn_Save = localVariables.newFieldInRecord("pnd_Restart_Isn_Save", "#RESTART-ISN-SAVE", FieldType.NUMERIC, 9);
        pnd_Log_Dte_Tme_Save = localVariables.newFieldInRecord("pnd_Log_Dte_Tme_Save", "#LOG-DTE-TME-SAVE", FieldType.STRING, 15);
        pnd_No_Of_Records_Get = localVariables.newFieldInRecord("pnd_No_Of_Records_Get", "#NO-OF-RECORDS-GET", FieldType.NUMERIC, 9);
        pnd_Sorted_Isn_Cntr = localVariables.newFieldInRecord("pnd_Sorted_Isn_Cntr", "#SORTED-ISN-CNTR", FieldType.NUMERIC, 9);
        pnd_Run_Id_Prt = localVariables.newFieldInRecord("pnd_Run_Id_Prt", "#RUN-ID-PRT", FieldType.NUMERIC, 9);
        pnd_Isn_Deleted = localVariables.newFieldInRecord("pnd_Isn_Deleted", "#ISN-DELETED", FieldType.NUMERIC, 9);
        pnd_Isn_Listed = localVariables.newFieldInRecord("pnd_Isn_Listed", "#ISN-LISTED", FieldType.NUMERIC, 9);
        pnd_Duplicate_Isn_Cntr = localVariables.newFieldInRecord("pnd_Duplicate_Isn_Cntr", "#DUPLICATE-ISN-CNTR", FieldType.NUMERIC, 9);
        pnd_Mismatch_Cntr = localVariables.newFieldInRecord("pnd_Mismatch_Cntr", "#MISMATCH-CNTR", FieldType.NUMERIC, 9);
        pnd_Mismatch_Sw = localVariables.newFieldInRecord("pnd_Mismatch_Sw", "#MISMATCH-SW", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Who_Work_Request.reset();
        vw_cwf_Master_Index_View_Select.reset();
        vw_cwf_Master_Index_View.reset();
        vw_cwf_Who_Support_Tbl_Prevent.reset();
        vw_cwf_Who_Work_Request_Prevent.reset();

        ldaMihl0180.initializeValues();
        ldaMihl0181.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Mihp0600() throws Exception
    {
        super("Mihp0600");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("MIHP0600", onError);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 55 LS = 132
        //*  ASSIGN THE LIMIT OF THE NUMBER OF RECORDS TO BE PROCESSED BETWEEN
        //*  COMMITTING AN END TRANSACTION.
        //*  ----------------------------------------------------------------------
        pnd_No_Of_Records_Limit.setValue(100);                                                                                                                            //Natural: ASSIGN #NO-OF-RECORDS-LIMIT := 100
        //*  INITIALIZE
        //*  ----------
        pnd_This_Program_Has_Finished_Successfully.setValue(false);                                                                                                       //Natural: ASSIGN #THIS-PROGRAM-HAS-FINISHED-SUCCESSFULLY := FALSE
        pnd_Isn_Save.reset();                                                                                                                                             //Natural: RESET #ISN-SAVE
        pnd_Restart_Isn_Save.reset();                                                                                                                                     //Natural: RESET #RESTART-ISN-SAVE #RESTART-SW
        pnd_Restart_Sw.reset();
        //*   TO ALLOW FOR EASY ESCAPE FROM PROGRAM.
        REP1:                                                                                                                                                             //Natural: REPEAT
        while (condition(whileTrue))
        {
                                                                                                                                                                          //Natural: PERFORM PREVENT-INVALID-CID
            sub_Prevent_Invalid_Cid();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM READ-RUN-CONTROL
            sub_Read_Run_Control();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            GET01:                                                                                                                                                        //Natural: GET CWF-WHO-SUPPORT-TBL MIHA0200-OUTPUT.RUN-CONTROL-ISN
            ldaMihl0180.getVw_cwf_Who_Support_Tbl().readByID(pdaMiha0200.getMiha0200_Output_Run_Control_Isn().getLong(), "GET01");
            if (condition(ldaMihl0180.getCwf_Who_Support_Tbl_Frst_Isn_Of_Blck().greater(getZero())))                                                                      //Natural: IF CWF-WHO-SUPPORT-TBL.FRST-ISN-OF-BLCK GT 0
            {
                pnd_Restart_Isn_Save.setValue(ldaMihl0180.getCwf_Who_Support_Tbl_Frst_Isn_Of_Blck());                                                                     //Natural: MOVE CWF-WHO-SUPPORT-TBL.FRST-ISN-OF-BLCK TO #RESTART-ISN-SAVE
                pnd_Restart_Sw.setValue("Y");                                                                                                                             //Natural: MOVE 'Y' TO #RESTART-SW
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM SELECT-MIT-RECORDS
            sub_Select_Mit_Records();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
                                                                                                                                                                          //Natural: PERFORM STATISTIC-RTN
            sub_Statistic_Rtn();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM COMPARE-RTN
            sub_Compare_Rtn();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, NEWLINE,"***************************************************************",NEWLINE,"***               This Run is Successful                    ***", //Natural: WRITE / '***************************************************************' / '***               This Run is Successful                    ***' / '*** Number of ISN Supplied and (Deleted + Dups) are equal:  ***' / '************ Run ID Control Record' #RUN-ID-PRT ' *************' / '============================================================' / 'Number of recs supplied for Deletion MIHP0400 = ' #ISN-LISTED / 'Number of records actually Deleted   MIHP0600 = ' #ISN-DELETED / 'Number of records which have duplicate  ISNs  = ' #DUPLICATE-ISN-CNTR / 'Number of records with Mismatched Log-Dte-Tme = ' #MISMATCH-CNTR / '************************************************************'
                NEWLINE,"*** Number of ISN Supplied and (Deleted + Dups) are equal:  ***",NEWLINE,"************ Run ID Control Record",pnd_Run_Id_Prt," *************",
                NEWLINE,"============================================================",NEWLINE,"Number of recs supplied for Deletion MIHP0400 = ",pnd_Isn_Listed,
                NEWLINE,"Number of records actually Deleted   MIHP0600 = ",pnd_Isn_Deleted,NEWLINE,"Number of records which have duplicate  ISNs  = ",pnd_Duplicate_Isn_Cntr,
                NEWLINE,"Number of records with Mismatched Log-Dte-Tme = ",pnd_Mismatch_Cntr,NEWLINE,"************************************************************");
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Restart_Sw.equals("Y")))                                                                                                                    //Natural: IF #RESTART-SW = 'Y'
            {
                getReports().write(0, NEWLINE,NEWLINE,"&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&",NEWLINE,"***  This run has been Restarted  ***",NEWLINE,                    //Natural: WRITE // '&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&' / '***  This run has been Restarted  ***' / '&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&'
                    "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("REP1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pnd_This_Program_Has_Finished_Successfully.setValue(true);                                                                                                    //Natural: ASSIGN #THIS-PROGRAM-HAS-FINISHED-SUCCESSFULLY := TRUE
                                                                                                                                                                          //Natural: PERFORM ESCAPE-PROGRAM
            sub_Escape_Program();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ESCAPE-PROGRAM
            //*   (REP1.)
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PREVENT-INVALID-CID
        //*  ===================================
        //*  ISSUE A DUMMY ACCESS TO THE CWF-WHO-SUPPORT-TBL TO OPEN THE
        //*  DATABASE FILE AND PREVENT A CID ERROR OCCURING.
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-RUN-CONTROL
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SELECT-MIT-RECORDS
        //*  =====================================
        //*  READ SORTED ISN FILE (LIST) THEN READ MIT BY ISN AND DELETE RECORD.
        //*  ---------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STATISTIC-RTN
        //* ****************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COMPARE-RTN
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Escape_Program() throws Exception                                                                                                                    //Natural: ESCAPE-PROGRAM
    {
        if (BLNatReinput.isReinput()) return;

        //*  ================================
        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( REP1. )
        Global.setEscapeCode(EscapeType.Bottom, "REP1");
        if (true) return;
    }
    private void sub_Prevent_Invalid_Cid() throws Exception                                                                                                               //Natural: PREVENT-INVALID-CID
    {
        if (BLNatReinput.isReinput()) return;

        vw_cwf_Who_Support_Tbl_Prevent.createHistogram                                                                                                                    //Natural: HISTOGRAM ( 1 ) CWF-WHO-SUPPORT-TBL-PREVENT TBL-PRIME-KEY
        (
        "HIST01",
        "TBL_PRIME_KEY",
        1
        );
        HIST01:
        while (condition(vw_cwf_Who_Support_Tbl_Prevent.readNextRow("HIST01")))
        {
        }                                                                                                                                                                 //Natural: END-HISTOGRAM
        if (Global.isEscape()) return;
        vw_cwf_Who_Work_Request_Prevent.createHistogram                                                                                                                   //Natural: HISTOGRAM ( 1 ) CWF-WHO-WORK-REQUEST-PREVENT RQST-LOG-DTE-TME
        (
        "HIST02",
        "RQST_LOG_DTE_TME",
        1
        );
        HIST02:
        while (condition(vw_cwf_Who_Work_Request_Prevent.readNextRow("HIST02")))
        {
        }                                                                                                                                                                 //Natural: END-HISTOGRAM
        if (Global.isEscape()) return;
        //*  (1650)
    }
    private void sub_Read_Run_Control() throws Exception                                                                                                                  //Natural: READ-RUN-CONTROL
    {
        if (BLNatReinput.isReinput()) return;

        //*  ================================
        //*  CALL THE SUB PROGRAM 'Set up Run Control for the MIT ISN Delete
        //*  PROCESS'. If a critical error is returned, print message & terminate
        //*  THE JOB. COMMIT TRANSACTION FOR RUN-CONTROL SET UP.
        //*  ----------------------------------------------------------------------
        DbsUtil.callnat(Mihn0200.class , getCurrentProcessState(), pdaMiha0200.getMiha0200_Output(), pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(),             //Natural: CALLNAT 'MIHN0200' MIHA0200-OUTPUT CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
            pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("E") || pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("W")))                    //Natural: IF MSG-INFO-SUB.##RETURN-CODE = 'E' OR MSG-INFO-SUB.##RETURN-CODE = 'W'
        {
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(10),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                     //Natural: WRITE ( 1 ) 10T MSG-INFO-SUB.##MSG
            if (Global.isEscape()) return;
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("E")))                                                                                 //Natural: IF MSG-INFO-SUB.##RETURN-CODE = 'E'
            {
                DbsUtil.terminate(4);  if (true) return;                                                                                                                  //Natural: TERMINATE 4
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().equals(999)))                                                                                          //Natural: IF MSG-INFO-SUB.##MSG-NR = 999
        {
            getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(10),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                  //Natural: WRITE ( 1 ) NOTITLE NOHDR 10T MSG-INFO-SUB.##MSG
            if (Global.isEscape()) return;
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
            DbsUtil.terminate(4);  if (true) return;                                                                                                                      //Natural: TERMINATE 4
        }                                                                                                                                                                 //Natural: END-IF
        //*  (1880)
    }
    private void sub_Select_Mit_Records() throws Exception                                                                                                                //Natural: SELECT-MIT-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        R_W1:                                                                                                                                                             //Natural: READ WORK FILE 1 #READ-WORK-RECORD
        while (condition(getWorkFiles().read(1, pnd_Read_Work_Record)))
        {
            pnd_Sorted_Isn_Cntr.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #SORTED-ISN-CNTR
            //*  TO ELIMINATE DUPLICATES
            if (condition(pnd_Read_Work_Record_Pnd_Isn.equals(pnd_Isn_Save)))                                                                                             //Natural: IF #READ-WORK-RECORD.#ISN EQ #ISN-SAVE
            {
                pnd_Duplicate_Isn_Cntr.nadd(1);                                                                                                                           //Natural: ADD 1 TO #DUPLICATE-ISN-CNTR
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Restart_Isn_Save.greater(getZero())))                                                                                                       //Natural: IF #RESTART-ISN-SAVE GT 0
            {
                //*  TO SKIP DEL RECS
                if (condition(pnd_Read_Work_Record_Pnd_Isn.less(pnd_Restart_Isn_Save)))                                                                                   //Natural: IF #READ-WORK-RECORD.#ISN LT #RESTART-ISN-SAVE
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pnd_Restart_Isn_Save.reset();                                                                                                                             //Natural: RESET #RESTART-ISN-SAVE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Isn_Save.setValue(pnd_Read_Work_Record_Pnd_Isn);                                                                                                          //Natural: MOVE #READ-WORK-RECORD.#ISN TO #ISN-SAVE
            pnd_Log_Dte_Tme_Save.setValue(pnd_Read_Work_Record_Pnd_Log_Dte_Tme);                                                                                          //Natural: MOVE #READ-WORK-RECORD.#LOG-DTE-TME TO #LOG-DTE-TME-SAVE
            if (condition(pnd_First_Isn_Sw.equals(" ")))                                                                                                                  //Natural: IF #FIRST-ISN-SW = ' '
            {
                GET02:                                                                                                                                                    //Natural: GET CWF-WHO-SUPPORT-TBL MIHA0200-OUTPUT.RUN-CONTROL-ISN
                ldaMihl0180.getVw_cwf_Who_Support_Tbl().readByID(pdaMiha0200.getMiha0200_Output_Run_Control_Isn().getLong(), "GET02");
                ldaMihl0180.getCwf_Who_Support_Tbl_Frst_Isn_Of_Blck().setValue(pnd_Isn_Save);                                                                             //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.FRST-ISN-OF-BLCK := #ISN-SAVE
                pnd_First_Isn_Sw.setValue("Y");                                                                                                                           //Natural: MOVE 'Y' TO #FIRST-ISN-SW
                ldaMihl0180.getVw_cwf_Who_Support_Tbl().updateDBRow("GET02");                                                                                             //Natural: UPDATE
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
            GET_MIT:                                                                                                                                                      //Natural: GET CWF-MASTER-INDEX-VIEW #ISN-SAVE
            vw_cwf_Master_Index_View.readByID(pnd_Isn_Save.getLong(), "GET_MIT");
            pnd_Mismatch_Sw.reset();                                                                                                                                      //Natural: RESET #MISMATCH-SW
            if (condition(cwf_Master_Index_View_Rqst_Log_Dte_Tme.notEquals(pnd_Log_Dte_Tme_Save)))                                                                        //Natural: IF CWF-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME NE #LOG-DTE-TME-SAVE
            {
                getReports().write(0, NEWLINE,"*** THE FOLLOWING RECORD WAS NOT DELETED ***",NEWLINE,"WORK ISN     = ",pnd_Read_Work_Record_Pnd_Isn,NEWLINE,              //Natural: WRITE / '*** THE FOLLOWING RECORD WAS NOT DELETED ***' / 'WORK ISN     = ' #READ-WORK-RECORD.#ISN / 'WORK LOG-DTE = ' #READ-WORK-RECORD.#LOG-DTE-TME / 'MIT  ISN     = ' *ISN / 'MIT  LOG-DTE = ' RQST-LOG-DTE-TME
                    "WORK LOG-DTE = ",pnd_Read_Work_Record_Pnd_Log_Dte_Tme,NEWLINE,"MIT  ISN     = ",Global.getAstISN(),NEWLINE,"MIT  LOG-DTE = ",cwf_Who_Work_Request_Rqst_Log_Dte_Tme);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R_W1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R_W1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Mismatch_Cntr.nadd(1);                                                                                                                                //Natural: ADD 1 TO #MISMATCH-CNTR
                pnd_Mismatch_Sw.setValue("Y");                                                                                                                            //Natural: MOVE 'Y' TO #MISMATCH-SW
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Mismatch_Sw.equals(" ")))                                                                                                                   //Natural: IF #MISMATCH-SW = ' '
            {
                vw_cwf_Master_Index_View.deleteDBRow("GET_MIT");                                                                                                          //Natural: DELETE ( GET-MIT. )
                pnd_No_Of_Records_Get.nadd(1);                                                                                                                            //Natural: ADD 1 TO #NO-OF-RECORDS-GET
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_No_Of_Records_Get.greaterOrEqual(pnd_No_Of_Records_Limit)))                                                                                 //Natural: IF #NO-OF-RECORDS-GET GE #NO-OF-RECORDS-LIMIT
            {
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                                                                                                                                                                          //Natural: PERFORM STATISTIC-RTN
                sub_Statistic_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R_W1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R_W1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  R-W1.
        }                                                                                                                                                                 //Natural: END-WORK
        R_W1_Exit:
        if (Global.isEscape()) return;
        //*  (2130)
    }
    private void sub_Statistic_Rtn() throws Exception                                                                                                                     //Natural: STATISTIC-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //*  ===================================
        //*  ACCUMULATE THE NUMBER OF RECORDS WHICH ARE PROCESSED AND COMMIT A
        //*  TRANSACTION WHEN THE STIPULATED LIMIT IS REACHED. UPDATE THE DELETE
        //*  STATISTICS ON THE RUN CONTROL RECORD.
        //*  ----------------------------------------------------------------------
        GET_TRANSACTION_UPDATE:                                                                                                                                           //Natural: GET CWF-WHO-SUPPORT-TBL MIHA0200-OUTPUT.RUN-CONTROL-ISN
        ldaMihl0180.getVw_cwf_Who_Support_Tbl().readByID(pdaMiha0200.getMiha0200_Output_Run_Control_Isn().getLong(), "GET_TRANSACTION_UPDATE");
        ldaMihl0180.getCwf_Who_Support_Tbl_Number_Of_Isn_Deleted().nadd(pnd_No_Of_Records_Get);                                                                           //Natural: COMPUTE CWF-WHO-SUPPORT-TBL.NUMBER-OF-ISN-DELETED = CWF-WHO-SUPPORT-TBL.NUMBER-OF-ISN-DELETED + #NO-OF-RECORDS-GET
        ldaMihl0180.getCwf_Who_Support_Tbl_Last_Isn_Deleted().setValue(pnd_Isn_Save);                                                                                     //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.LAST-ISN-DELETED := #ISN-SAVE
        ldaMihl0180.getCwf_Who_Support_Tbl_Frst_Isn_Of_Blck().setValue(0);                                                                                                //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.FRST-ISN-OF-BLCK := 0
        //*  ------------------------------------------------------
        //*  THE FOLLOWING INFORMATION FOR PRINTING REPORT   ------
        //*  ------------------------------------------------------
        pnd_Isn_Listed.setValue(ldaMihl0180.getCwf_Who_Support_Tbl_Number_Of_Isn_Listed());                                                                               //Natural: MOVE CWF-WHO-SUPPORT-TBL.NUMBER-OF-ISN-LISTED TO #ISN-LISTED
        pnd_Isn_Deleted.setValue(ldaMihl0180.getCwf_Who_Support_Tbl_Number_Of_Isn_Deleted());                                                                             //Natural: MOVE CWF-WHO-SUPPORT-TBL.NUMBER-OF-ISN-DELETED TO #ISN-DELETED
        pnd_Run_Id_Prt.setValue(ldaMihl0180.getCwf_Who_Support_Tbl_Run_Id());                                                                                             //Natural: MOVE CWF-WHO-SUPPORT-TBL.RUN-ID TO #RUN-ID-PRT
        ldaMihl0180.getVw_cwf_Who_Support_Tbl().updateDBRow("GET_TRANSACTION_UPDATE");                                                                                    //Natural: UPDATE ( GET-TRANSACTION-UPDATE. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        pnd_No_Of_Records_Get.reset();                                                                                                                                    //Natural: RESET #NO-OF-RECORDS-GET
        pnd_First_Isn_Sw.reset();                                                                                                                                         //Natural: RESET #FIRST-ISN-SW
        //*  (2730)
    }
    private void sub_Compare_Rtn() throws Exception                                                                                                                       //Natural: COMPARE-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************
        //*  PULL CONTROL RECORD FOR COMPARISON AND UPDATE DATE FIELD
        //*  --------------------------------------------------------
        GET_CONTROL_REC:                                                                                                                                                  //Natural: GET CWF-WHO-SUPPORT-TBL MIHA0200-OUTPUT.RUN-CONTROL-ISN
        ldaMihl0180.getVw_cwf_Who_Support_Tbl().readByID(pdaMiha0200.getMiha0200_Output_Run_Control_Isn().getLong(), "GET_CONTROL_REC");
        pnd_Isn_Listed.setValue(ldaMihl0180.getCwf_Who_Support_Tbl_Number_Of_Isn_Listed());                                                                               //Natural: MOVE CWF-WHO-SUPPORT-TBL.NUMBER-OF-ISN-LISTED TO #ISN-LISTED
        if (condition(ldaMihl0180.getCwf_Who_Support_Tbl_Number_Of_Isn_Listed().equals((ldaMihl0180.getCwf_Who_Support_Tbl_Number_Of_Isn_Deleted().add(pnd_Duplicate_Isn_Cntr))))) //Natural: IF CWF-WHO-SUPPORT-TBL.NUMBER-OF-ISN-LISTED EQ ( CWF-WHO-SUPPORT-TBL.NUMBER-OF-ISN-DELETED + #DUPLICATE-ISN-CNTR )
        {
            ldaMihl0180.getCwf_Who_Support_Tbl_Last_Isn_Deleted().setValue(0);                                                                                            //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.LAST-ISN-DELETED := 0
            ldaMihl0180.getCwf_Who_Support_Tbl_Frst_Isn_Of_Blck().setValue(0);                                                                                            //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.FRST-ISN-OF-BLCK := 0
            ldaMihl0180.getCwf_Who_Support_Tbl_Run_Date().setValue(Global.getDATN());                                                                                     //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.RUN-DATE := *DATN
            ldaMihl0180.getCwf_Who_Support_Tbl_Run_Status().setValue(ldaMihl0181.getMihl0181_Completed_Status());                                                         //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.RUN-STATUS := MIHL0181.COMPLETED-STATUS
            ldaMihl0180.getCwf_Who_Support_Tbl_Tbl_Updte_Dte_Tme().setValue(Global.getTIMX());                                                                            //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.TBL-UPDTE-DTE-TME := *TIMX
            ldaMihl0180.getCwf_Who_Support_Tbl_Tbl_Updte_Oprtr_Cde().setValue(Global.getPROGRAM());                                                                       //Natural: ASSIGN CWF-WHO-SUPPORT-TBL.TBL-UPDTE-OPRTR-CDE := *PROGRAM
            ldaMihl0180.getVw_cwf_Who_Support_Tbl().updateDBRow("GET_CONTROL_REC");                                                                                       //Natural: UPDATE ( GET-CONTROL-REC. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE IMMEDIATE
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, NEWLINE,"***********************************************************",NEWLINE,"*********         Attention Operator              *********", //Natural: WRITE / '***********************************************************' / '*********         Attention Operator              *********' / '**  There is a serious problem with MIT Deletion process **' / '************       Program MIHP0600         ***************' / '************ Run ID Control Record' #RUN-ID-PRT ' ************' / '** Print out this page and Call CWF group x4669 or x4994 **' / 'Number of recs supplied for Deletion MIHP0400 = ' #ISN-LISTED / 'Number of records actually Deleted   MIHP0600 = ' #ISN-DELETED / 'Number of records which have duplicate  ISNs  = ' #DUPLICATE-ISN-CNTR / 'Number of records with Mismatched Log-Dte-Tme = ' #MISMATCH-CNTR / '***********************************************************'
            NEWLINE,"**  There is a serious problem with MIT Deletion process **",NEWLINE,"************       Program MIHP0600         ***************",
            NEWLINE,"************ Run ID Control Record",pnd_Run_Id_Prt," ************",NEWLINE,"** Print out this page and Call CWF group x4669 or x4994 **",
            NEWLINE,"Number of recs supplied for Deletion MIHP0400 = ",pnd_Isn_Listed,NEWLINE,"Number of records actually Deleted   MIHP0600 = ",pnd_Isn_Deleted,
            NEWLINE,"Number of records which have duplicate  ISNs  = ",pnd_Duplicate_Isn_Cntr,NEWLINE,"Number of records with Mismatched Log-Dte-Tme = ",
            pnd_Mismatch_Cntr,NEWLINE,"***********************************************************");
        if (Global.isEscape()) return;
        DbsUtil.terminate(8);  if (true) return;                                                                                                                          //Natural: TERMINATE 8
        //*  (3020)
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, NEWLINE,"NATURAL ERROR",Global.getERROR_NR(),"IN",Global.getPROGRAM(),"....LINE",Global.getERROR_LINE(),NEWLINE," ",NEWLINE,                //Natural: WRITE / 'NATURAL ERROR' *ERROR-NR 'IN' *PROGRAM '....LINE' *ERROR-LINE / ' ' / '***********************************************************' / '*********         Attention Operator              *********' / '***********************************************************' / '** If this is a Program Error - Do Not Restart it Again ***' / '****************  See Message above ***********************' / '******************** Otherwise ****************************' / '************** This Job is Restartable  *******************' / '************ Resubmitt JCL - Job P9983 again **************' / '* Please notify Corporate Work Flow group x4669 or x4994 **' / '****************  about this case. Thank You.  ************' / '***********************************************************'
            "***********************************************************",NEWLINE,"*********         Attention Operator              *********",NEWLINE,
            "***********************************************************",NEWLINE,"** If this is a Program Error - Do Not Restart it Again ***",NEWLINE,
            "****************  See Message above ***********************",NEWLINE,"******************** Otherwise ****************************",NEWLINE,
            "************** This Job is Restartable  *******************",NEWLINE,"************ Resubmitt JCL - Job P9983 again **************",NEWLINE,
            "* Please notify Corporate Work Flow group x4669 or x4994 **",NEWLINE,"****************  about this case. Thank You.  ************",NEWLINE,
            "***********************************************************");
        DbsUtil.terminate(10);  if (true) return;                                                                                                                         //Natural: TERMINATE 10
        //*   END-IF
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=55 LS=132");
    }
}
