/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:53:45 PM
**        * FROM NATURAL PROGRAM : Mcsp1600
************************************************************
**        * FILE NAME            : Mcsp1600.java
**        * CLASS NAME           : Mcsp1600
**        * INSTANCE NAME        : Mcsp1600
************************************************************
************************************************************************
* PROGRAM  : MCSP1600
* SYSTEM   : PROJCWF
* TITLE    : PRINT UPLOAD AUDIT RECORDS FOR A SELECTED SYSTEM
* GENERATED: MARCH 10, 1994
* FUNCTION : THIS PROGRAM READS THE AUDIT STATS RECORDS AND DELETES
*            ALL RECORDS PREVIOUS TO THE ONE WEEK AGO.
* HISTORY
* 09/23/98 : OB  RETAIN ALL TYPES OF RECORD FOR AT LEAST 30 DAYS
* 12/08/98 : MP  DELETE ANY TYPE OF 'successful' RECORDS AFTER 30 DAYS.
*          :     SOME OF THEM WITH CONTAINER-ID, OTHERS WITHOUT IT.
* 08/03/99 : EPM CHECKED WHETHER ALL RECORDS FOR THE CONTAINER HAS BEEN
*          :     SUCCESSFULLY PROCESSED (MEANING ERROR-MSG-TXT IS BLANK)
*          :     BEFORE THE RECORD IS DELETED.
* 04/24/00 : EPM DELETE RECORDS WHEN CLIENT-APP-CODE  IS BATCHTDA
* 01/08/01 : EPM 1. DELETE RECORDS WHEN CLIENT-APP-CODE IS IN SUPPORT
*          :        TABLE WFO-CLIENTS-CLEANUP.
*          :     2. ALSO DELETES EXPRESS TRACKING RECORDS.
* 06/06/02 : EPM 1. CHANGED SO THAT CONTAINERS WHICH WERE SENT MO
*          :        ARE DELETED
************************************************************************
* NOTES    : ALL RECORDS WE DELETE ARE COMPARED TO 30 DAYS PRIOR TO
*          : SYSTEMS DATE. IF UPLOAD DATE TIME IS LESS THAN THIS DATE,
*          : 1. WE DELETE THOSE SUCCESSFUL CONTAINERS WHOSE
*          :    UPLOAD DATE TIME IS 30 DAYS PRIOR TO SYSTEMS DATE, I.E.,
*          :    IF ANY RECORD IN THE CONTAINER HAS ERROR MSG TXT NE ' '
*          :    WE DO NOT DELETE THOSE RECORDS.
*          : 2. WE ALSO DELETE RECORDS BELONGING TO CLIENT APP CODE IN
*          :    WFO-CLIENTS-CLEANUP TABLE
*          : 3. WE ALSO DELETE RECORDS BELONGING TO EXPRESS OR MODIFY
*          :    CONTAINERS.
* 02/23/2017 - SINGAK - PIN EXPANSION - AUG 2017
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Mcsp1600 extends BLNatBase
{
    // Data Areas
    private GdaMcsg000 gdaMcsg000;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Mcss_Calls;
    private DbsField cwf_Mcss_Calls_Rcrd_Stamp_Tme;
    private DbsField cwf_Mcss_Calls_Rcrd_Upld_Tme;
    private DbsField cwf_Mcss_Calls_Rcrd_Type_Cde;
    private DbsField cwf_Mcss_Calls_Rcrd_Contn_Ind;
    private DbsField cwf_Mcss_Calls_Systm_Ind;
    private DbsField cwf_Mcss_Calls_Pin_Nbr;
    private DbsField cwf_Mcss_Calls_Contact_Id_Cde;
    private DbsField cwf_Mcss_Calls_Call_Stamp_Tme;
    private DbsField cwf_Mcss_Calls_Call_Rcvd_Dte;
    private DbsField cwf_Mcss_Calls_Btch_Nbr;
    private DbsField cwf_Mcss_Calls_Lines_Nbr;
    private DbsField cwf_Mcss_Calls_Wpids_Nbr;
    private DbsField cwf_Mcss_Calls_Lines_Total_Nbr;
    private DbsField cwf_Mcss_Calls_Wpids_Total_Nbr;
    private DbsField cwf_Mcss_Calls_Cntct_Total_Nbr;
    private DbsField cwf_Mcss_Calls_Cntct_Error_Nbr;
    private DbsField cwf_Mcss_Calls_Doc_Scrty_Cde;
    private DbsField cwf_Mcss_Calls_Doc_Ctgry_Cde;
    private DbsField cwf_Mcss_Calls_Doc_Clss_Cde;
    private DbsField cwf_Mcss_Calls_Rqst_Entry_Op_Cde;
    private DbsField cwf_Mcss_Calls_Rqst_Origin_Unit_Cde;
    private DbsField cwf_Mcss_Calls_Spcl_Hndlng_Txt;
    private DbsField cwf_Mcss_Calls_Attn_Txt;
    private DbsField cwf_Mcss_Calls_Error_Msg_Txt;
    private DbsField cwf_Mcss_Calls_Ph_Ssn_Nbr;
    private DbsField cwf_Mcss_Calls_Ph_Home_Phone_Area_Cde;
    private DbsField cwf_Mcss_Calls_Ph_Home_Phone_Actual_Nbr;
    private DbsField cwf_Mcss_Calls_Ph_Wrk_Phone_Area_Cde;
    private DbsField cwf_Mcss_Calls_Ph_Wrk_Phone_Actual_Nbr;
    private DbsField cwf_Mcss_Calls_Ph_Wrk_Phone_Ext_Nbr;
    private DbsField cwf_Mcss_Calls_Spouse_Birth_Dte;

    private DbsGroup cwf_Mcss_Calls_Request_Array_Grp;
    private DbsField cwf_Mcss_Calls_Action_Cde;
    private DbsField cwf_Mcss_Calls_Wpid_Cde;
    private DbsGroup cwf_Mcss_Calls_Cntrct_NbrMuGroup;
    private DbsField cwf_Mcss_Calls_Cntrct_Nbr;
    private DbsField cwf_Mcss_Calls_Rel_Tiaa_Rcvd_Dte;

    private DbsGroup cwf_Mcss_Calls__R_Field_1;

    private DbsGroup cwf_Mcss_Calls_Struct_3600;
    private DbsField cwf_Mcss_Calls_Rel_Tiaa_Rcvd_Cc;
    private DbsField cwf_Mcss_Calls_Rel_Tiaa_Rcvd_Yymmdd;
    private DbsField cwf_Mcss_Calls_Rel_Wpid_Cde;
    private DbsField cwf_Mcss_Calls_Rel_Case_Id_Nbr;
    private DbsField cwf_Mcss_Calls_Rel_Sub_Rqst_Cde;
    private DbsField cwf_Mcss_Calls_Rel_Log_Dte_Tme;
    private DbsField cwf_Mcss_Calls_Wpid_Validate_Ind;
    private DbsField cwf_Mcss_Calls_Dstntn_Unt_Cde;
    private DbsField cwf_Mcss_Calls_Mail_Rply_Cde;
    private DbsField cwf_Mcss_Calls_Mail_Id_Nbr;
    private DbsField cwf_Mcss_Calls_Corp_Status_Cde;
    private DbsField cwf_Mcss_Calls_Unit_Status_Cde;
    private DbsField cwf_Mcss_Calls_Work_Req_Prty_Cde;
    private DbsField cwf_Mcss_Calls_Container_Id;
    private DbsField cwf_Mcss_Calls_Doc_Type_Spcfc_Cde;

    private DbsGroup cwf_Mcss_Calls_Doc_Text_Area_Grp;
    private DbsField cwf_Mcss_Calls_Doc_Txt;

    private DbsGroup cwf_Mcss_Calls_Wfo_All_Othr_Flds;
    private DbsField cwf_Mcss_Calls_Wfo_Othr_Flds;

    private DbsGroup cwf_Mcss_Calls__R_Field_2;
    private DbsField cwf_Mcss_Calls_Wf_Ind;
    private DbsField cwf_Mcss_Calls_Ind_Tracking_Id;
    private DbsField cwf_Mcss_Calls_Inst_Tracking_Id;
    private DbsField cwf_Mcss_Calls_Cabinet_Id;
    private DbsField cwf_Mcss_Calls_Cabinet_Level;
    private DbsField cwf_Mcss_Calls_Empl_Racf_Id;
    private DbsField cwf_Mcss_Calls_Step;
    private DbsField cwf_Mcss_Calls_Due_Dte;
    private DbsField cwf_Mcss_Calls_Client_App_Cde;
    private DbsField cwf_Mcss_Calls_Elctrnc_Fldr_Ind;
    private DbsField cwf_Mcss_Calls_Rqst_Orgn_Cde;
    private DbsField cwf_Mcss_Calls_Topic;

    private DbsGroup cwf_Mcss_Calls_Processed_Work_Rqsts_Grp;
    private DbsField cwf_Mcss_Calls_Processed_Work_Rqsts_Elmt;

    private DbsGroup cwf_Mcss_Calls__R_Field_3;
    private DbsField cwf_Mcss_Calls_P_Rqst_Id;

    private DbsGroup cwf_Mcss_Calls__R_Field_4;
    private DbsField cwf_Mcss_Calls_P_Tiaa_Rcvd_Date;
    private DbsField cwf_Mcss_Calls_P_Case_Id;
    private DbsField cwf_Mcss_Calls_P_Wpid;
    private DbsField cwf_Mcss_Calls_P_Sub_Rqst_Cde;
    private DbsField cwf_Mcss_Calls_P_Multi_Rqst_Ind;
    private DbsField cwf_Mcss_Calls_P_Rldt;
    private DbsField cwf_Mcss_Calls_P_Doc_Group;

    private DbsGroup cwf_Mcss_Calls__R_Field_5;
    private DbsField cwf_Mcss_Calls_P_Dstntn_Unt_Cde;
    private DbsField cwf_Mcss_Calls_P_Fil1;
    private DbsField cwf_Mcss_Calls_P_Topic;
    private DbsField cwf_Mcss_Calls_P_Fil2;
    private DbsField cwf_Mcss_Calls_P_Wpid_Cde;
    private DbsField cwf_Mcss_Calls_P_Fil3;
    private DbsField cwf_Mcss_Calls_P_Corp_Status;
    private DbsField cwf_Mcss_Calls_P_Fil4;
    private DbsField cwf_Mcss_Calls_P_Wf_Ind;
    private DbsField cwf_Mcss_Calls_P_Fil5;
    private DbsField cwf_Mcss_Calls_P_Action_Cde;
    private DbsField cwf_Mcss_Calls_P_Cabinet;

    private DbsGroup cwf_Mcss_Calls__R_Field_6;
    private DbsField cwf_Mcss_Calls_P_Prefix;
    private DbsField cwf_Mcss_Calls_P_Pin;
    private DbsField cwf_Mcss_Calls_P_Cabinet_Level;
    private DbsField cwf_Mcss_Calls_Processed_Text_Ind;
    private DbsField cwf_Mcss_Calls_Part_Orig_Crte_Dte_Tme;
    private DbsField cwf_Mcss_Calls_Inst_Orig_Crte_Dte_Tme;
    private DbsField cwf_Mcss_Calls_Nonp_Orig_Crte_Dte_Tme;
    private DbsField cwf_Mcss_Calls_Part_Orig_Document_Name;

    private DbsGroup cwf_Mcss_Calls__R_Field_7;
    private DbsField cwf_Mcss_Calls_Part_Orig_Category;
    private DbsField cwf_Mcss_Calls_Part_Orig_Class;
    private DbsField cwf_Mcss_Calls_Part_Orig_Work_Prcss_Id;
    private DbsField cwf_Mcss_Calls_Part_Orig_Specific;
    private DbsField cwf_Mcss_Calls_Part_Orig_Creation_Cc;
    private DbsField cwf_Mcss_Calls_Part_Orig_Creation_Yymmdd;
    private DbsField cwf_Mcss_Calls_Part_Orig_Direction;
    private DbsField cwf_Mcss_Calls_Part_Orig_Secured_Ind;
    private DbsField cwf_Mcss_Calls_Part_Orig_Retention_Ind;
    private DbsField cwf_Mcss_Calls_Part_Orig_Format;
    private DbsField cwf_Mcss_Calls_Part_Orig_Tie_Breaker;
    private DbsField cwf_Mcss_Calls_Part_Orig_Copy_Ind;
    private DbsField cwf_Mcss_Calls_Part_Orig_Important_Ind;
    private DbsField cwf_Mcss_Calls_Part_Orig_Package_Nbr;
    private DbsField cwf_Mcss_Calls_Inst_Orig_Document_Name;

    private DbsGroup cwf_Mcss_Calls__R_Field_8;
    private DbsField cwf_Mcss_Calls_Inst_Orig_Category;
    private DbsField cwf_Mcss_Calls_Inst_Orig_Class;
    private DbsField cwf_Mcss_Calls_Inst_Orig_Specific;
    private DbsField cwf_Mcss_Calls_Inst_Orig_Direction;
    private DbsField cwf_Mcss_Calls_Inst_Orig_Format;
    private DbsField cwf_Mcss_Calls_Nonp_Orig_Document_Name;

    private DbsGroup cwf_Mcss_Calls__R_Field_9;
    private DbsField cwf_Mcss_Calls_Nonp_Orig_Category;
    private DbsField cwf_Mcss_Calls_Nonp_Orig_Class;
    private DbsField cwf_Mcss_Calls_Nonp_Orig_Specific;
    private DbsField cwf_Mcss_Calls_Nonp_Orig_Direction;
    private DbsField cwf_Mcss_Calls_Nonp_Orig_Format;
    private DbsField cwf_Mcss_Calls_Cntrct;

    private DbsGroup cwf_Mcss_Calls_Processed2_Work_Rqsts_Grp;
    private DbsField cwf_Mcss_Calls_P_Tracking_Id;
    private DbsField cwf_Mcss_Calls_P_Ati_Ind;
    private DbsField cwf_Mcss_Calls_Lead_Class;
    private DbsField cwf_Mcss_Calls_Express_Ind;
    private DbsField cwf_Mcss_Calls_Mail_Cde;

    private DbsGroup cwf_Mcss_Calls__R_Field_10;
    private DbsField cwf_Mcss_Calls_Dcmnt_Obj_Id;
    private DbsField cwf_Mcss_Calls_Mail_Item_No;
    private DbsField cwf_Mcss_Calls_T_Client_App_Cde;
    private DbsGroup cwf_Mcss_Calls_Tracking_Rcrd_NbrMuGroup;
    private DbsField cwf_Mcss_Calls_Tracking_Rcrd_Nbr;
    private DbsField cwf_Mcss_Calls_Wfo_Ind;

    private DataAccessProgramView vw_cwf_Mcss_Calls2;
    private DbsField cwf_Mcss_Calls2_Container_Id;
    private DbsField cwf_Mcss_Calls2_Error_Msg_Txt;
    private DbsField pnd_I;
    private DbsField pnd_Program;
    private DbsField pnd_Input_Time;
    private DbsField pnd_First_Rec;
    private DbsField pnd_Date_Time;

    private DbsGroup pnd_Date_Time__R_Field_11;
    private DbsField pnd_Date_Time_Pnd_Date;
    private DbsField pnd_Date_Time_Pnd_Time;
    private DbsField pnd_Chk_Dtetme;

    private DbsGroup pnd_Chk_Dtetme__R_Field_12;
    private DbsField pnd_Chk_Dtetme_Pnd_Chk_Date;

    private DbsGroup pnd_Chk_Dtetme__R_Field_13;
    private DbsField pnd_Chk_Dtetme_Pnd_Chk_Yyyy;
    private DbsField pnd_Chk_Dtetme_Pnd_Chk_Mm;
    private DbsField pnd_Chk_Dtetme_Pnd_Chk_Time;
    private DbsField pnd_Date_A;

    private DbsGroup pnd_Date_A__R_Field_14;
    private DbsField pnd_Date_A_Pnd_Date_Num;
    private DbsField pnd_Tot_Delete;
    private DbsField pnd_Rec_Cnt;
    private DbsField pnd_Upld_Date;
    private DbsField pnd_Upld_Time;

    private DbsGroup pnd_Upld_Time__R_Field_15;
    private DbsField pnd_Upld_Time_Pnd_Upld_Timen;
    private DbsField pnd_Input_Sys;
    private DbsField pnd_Input_From;

    private DbsGroup pnd_Input_From__R_Field_16;
    private DbsField pnd_Input_From_Yyyy;
    private DbsField pnd_Input_From_Mm;
    private DbsField pnd_Input_From_Dd;
    private DbsField pnd_Input_From_D;
    private DbsField pnd_Input_To;

    private DbsGroup pnd_Input_To__R_Field_17;
    private DbsField pnd_Input_To_Yyyy;
    private DbsField pnd_Input_To_Mm;
    private DbsField pnd_Input_To_Dd;
    private DbsField pnd_Input_To_D;
    private DbsField pnd_Input_To_T;
    private DbsField pnd_Input;

    private DbsGroup pnd_Input__R_Field_18;
    private DbsField pnd_Input_Yyyy;
    private DbsField pnd_Input_Mm;
    private DbsField pnd_Input_Dd;
    private DbsField pnd_Cwf_Mcss_Key;

    private DbsGroup pnd_Cwf_Mcss_Key__R_Field_19;
    private DbsField pnd_Cwf_Mcss_Key_Systm_Ind;
    private DbsField pnd_Cwf_Mcss_Key_Rcrd_Upld_Tme;
    private DbsField pnd_Cwf_Mcss_Key_Rcrd_Type_Cde;
    private DbsField pnd_Cwf_Mcss_Key_Btch_Nbr;
    private DbsField pnd_Cwf_Mcss_Key_Contact_Id_Cde;
    private DbsField pnd_Whole_Container_Processed;
    private DbsField pnd_Container_Rcrd_Type_Key;

    private DbsGroup pnd_Container_Rcrd_Type_Key__R_Field_20;
    private DbsField pnd_Container_Rcrd_Type_Key_Pnd_Container;
    private DbsField pnd_Container_Rcrd_Type_Key_Pnd_Rcrd_Type;
    private DbsField pnd_Container_Rcrd_Type_Key2;

    private DbsGroup pnd_Container_Rcrd_Type_Key2__R_Field_21;
    private DbsField pnd_Container_Rcrd_Type_Key2_Pnd_Container2;
    private DbsField pnd_Container_Rcrd_Type_Key2_Pnd_Rcrd_Type2;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_22;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind_2_2;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_23;
    private DbsField cwf_Support_Tbl_Pnd_Client;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;
    private DbsField cwf_Support_Tbl_Tbl_Actve_Ind;

    private DbsGroup cwf_Support_Tbl__R_Field_24;
    private DbsField cwf_Support_Tbl_Fill_1;
    private DbsField cwf_Support_Tbl_Tbl_Actve_Ind_2_2;
    private DbsField pnd_Client_App_Cde;
    private DbsField pnd_Cleanup;
    private DbsField pnd_Cleanup_Clients;
    private DbsField pnd_Client_Ctr;
    private DbsField pnd_Idx;
    private DbsField pnd_Save_Container;
    private DbsField pnd_Save_Client_App_Cde;
    private DbsField pnd_Tracking_Type_Key;

    private DbsGroup pnd_Tracking_Type_Key__R_Field_25;
    private DbsField pnd_Tracking_Type_Key_Pnd_T_Container_Id;
    private DbsField pnd_Tracking_Type_Key_Pnd_T_Rcrd_Type_Cde;
    private DbsField pnd_Tracking_Type_Key_Pnd_T_Wfo_Ind;
    private DbsField pnd_Tracking_Type_Key_Pnd_T_Tracking_Rcrd_Nbr;

    private DataAccessProgramView vw_cwf_Mcss_Calls3;

    private DbsGroup cwf_Mcss_Calls3_Wfo_All_Othr_Flds;
    private DbsField cwf_Mcss_Calls3_Wfo_Othr_Flds;

    private DbsGroup cwf_Mcss_Calls3__R_Field_26;
    private DbsField cwf_Mcss_Calls3_Wf_Ind;
    private DbsField cwf_Mcss_Calls3_Ind_Tracking_Id;
    private DbsField cwf_Mcss_Calls3_Inst_Tracking_Id;
    private DbsField cwf_Mcss_Calls3_Cabinet_Id;
    private DbsField cwf_Mcss_Calls3_Cabinet_Level;
    private DbsField cwf_Mcss_Calls3_Empl_Racf_Id;
    private DbsField cwf_Mcss_Calls3_Step;
    private DbsField cwf_Mcss_Calls3_Due_Dte;
    private DbsField cwf_Mcss_Calls3_Client_App_Cde;
    private DbsField pnd_Mo_Cont;
    private DbsField pnd_Express_Cont;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMcsg000 = GdaMcsg000.getInstance(getCallnatLevel());
        registerRecord(gdaMcsg000);
        if (gdaOnly) return;

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Mcss_Calls = new DataAccessProgramView(new NameInfo("vw_cwf_Mcss_Calls", "CWF-MCSS-CALLS"), "CWF_MCSS_CALLS", "CWF_MCSS_CALLS", DdmPeriodicGroups.getInstance().getGroups("CWF_MCSS_CALLS"));
        cwf_Mcss_Calls_Rcrd_Stamp_Tme = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Rcrd_Stamp_Tme", "RCRD-STAMP-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "RCRD_STAMP_TME");
        cwf_Mcss_Calls_Rcrd_Stamp_Tme.setDdmHeader("RECORD/TIME/STAMP");
        cwf_Mcss_Calls_Rcrd_Upld_Tme = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Rcrd_Upld_Tme", "RCRD-UPLD-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "RCRD_UPLD_TME");
        cwf_Mcss_Calls_Rcrd_Upld_Tme.setDdmHeader("RECORD/UPLOAD/TIME");
        cwf_Mcss_Calls_Rcrd_Type_Cde = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RCRD_TYPE_CDE");
        cwf_Mcss_Calls_Rcrd_Type_Cde.setDdmHeader("RECORD/TYPE");
        cwf_Mcss_Calls_Rcrd_Contn_Ind = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Rcrd_Contn_Ind", "RCRD-CONTN-IND", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "RCRD_CONTN_IND");
        cwf_Mcss_Calls_Rcrd_Contn_Ind.setDdmHeader("RECORD/CONT./NUMBER");
        cwf_Mcss_Calls_Systm_Ind = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Systm_Ind", "SYSTM-IND", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "SYSTM_IND");
        cwf_Mcss_Calls_Systm_Ind.setDdmHeader("SYSTEM/OF/ORIGIN");
        cwf_Mcss_Calls_Pin_Nbr = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "PIN_NBR");
        cwf_Mcss_Calls_Pin_Nbr.setDdmHeader("PIN");
        cwf_Mcss_Calls_Contact_Id_Cde = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Contact_Id_Cde", "CONTACT-ID-CDE", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CONTACT_ID_CDE");
        cwf_Mcss_Calls_Contact_Id_Cde.setDdmHeader("CONTACT/ID");
        cwf_Mcss_Calls_Call_Stamp_Tme = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Call_Stamp_Tme", "CALL-STAMP-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "CALL_STAMP_TME");
        cwf_Mcss_Calls_Call_Stamp_Tme.setDdmHeader("CALL/DATE &/TIME");
        cwf_Mcss_Calls_Call_Rcvd_Dte = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Call_Rcvd_Dte", "CALL-RCVD-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CALL_RCVD_DTE");
        cwf_Mcss_Calls_Call_Rcvd_Dte.setDdmHeader("CALL/DATE");
        cwf_Mcss_Calls_Btch_Nbr = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Btch_Nbr", "BTCH-NBR", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "BTCH_NBR");
        cwf_Mcss_Calls_Btch_Nbr.setDdmHeader("BATCH/NUM.");
        cwf_Mcss_Calls_Lines_Nbr = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Lines_Nbr", "LINES-NBR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "LINES_NBR");
        cwf_Mcss_Calls_Lines_Nbr.setDdmHeader("TEXT/LINES/SENT");
        cwf_Mcss_Calls_Wpids_Nbr = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Wpids_Nbr", "WPIDS-NBR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "WPIDS_NBR");
        cwf_Mcss_Calls_Wpids_Nbr.setDdmHeader("WPIDS/SENT");
        cwf_Mcss_Calls_Lines_Total_Nbr = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Lines_Total_Nbr", "LINES-TOTAL-NBR", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "LINES_TOTAL_NBR");
        cwf_Mcss_Calls_Lines_Total_Nbr.setDdmHeader("TOTAL/LINES");
        cwf_Mcss_Calls_Wpids_Total_Nbr = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Wpids_Total_Nbr", "WPIDS-TOTAL-NBR", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "WPIDS_TOTAL_NBR");
        cwf_Mcss_Calls_Wpids_Total_Nbr.setDdmHeader("TOTAL/WPIDS");
        cwf_Mcss_Calls_Cntct_Total_Nbr = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Cntct_Total_Nbr", "CNTCT-TOTAL-NBR", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTCT_TOTAL_NBR");
        cwf_Mcss_Calls_Cntct_Total_Nbr.setDdmHeader("TOTAL/CONTACT/SHEETS");
        cwf_Mcss_Calls_Cntct_Error_Nbr = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Cntct_Error_Nbr", "CNTCT-ERROR-NBR", FieldType.NUMERIC, 
            4, RepeatingFieldStrategy.None, "CNTCT_ERROR_NBR");
        cwf_Mcss_Calls_Cntct_Error_Nbr.setDdmHeader("TOTAL/ERRORS");
        cwf_Mcss_Calls_Doc_Scrty_Cde = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Doc_Scrty_Cde", "DOC-SCRTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DOC_SCRTY_CDE");
        cwf_Mcss_Calls_Doc_Scrty_Cde.setDdmHeader("DOCUMENT/SECURITY");
        cwf_Mcss_Calls_Doc_Ctgry_Cde = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Doc_Ctgry_Cde", "DOC-CTGRY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DOC_CTGRY_CDE");
        cwf_Mcss_Calls_Doc_Clss_Cde = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Doc_Clss_Cde", "DOC-CLSS-CDE", FieldType.STRING, 3, 
            RepeatingFieldStrategy.None, "DOC_CLSS_CDE");
        cwf_Mcss_Calls_Rqst_Entry_Op_Cde = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Rqst_Entry_Op_Cde", "RQST-ENTRY-OP-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "RQST_ENTRY_OP_CDE");
        cwf_Mcss_Calls_Rqst_Entry_Op_Cde.setDdmHeader("ORIGIN/RACF/ID");
        cwf_Mcss_Calls_Rqst_Origin_Unit_Cde = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Rqst_Origin_Unit_Cde", "RQST-ORIGIN-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_ORIGIN_UNIT_CDE");
        cwf_Mcss_Calls_Rqst_Origin_Unit_Cde.setDdmHeader("ORIGIN/UNIT");
        cwf_Mcss_Calls_Spcl_Hndlng_Txt = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Spcl_Hndlng_Txt", "SPCL-HNDLNG-TXT", FieldType.STRING, 
            45, RepeatingFieldStrategy.None, "SPCL_HNDLNG_TXT");
        cwf_Mcss_Calls_Spcl_Hndlng_Txt.setDdmHeader("SPECIAL/HANDLING");
        cwf_Mcss_Calls_Attn_Txt = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Attn_Txt", "ATTN-TXT", FieldType.STRING, 25, RepeatingFieldStrategy.None, 
            "ATTN_TXT");
        cwf_Mcss_Calls_Attn_Txt.setDdmHeader("ATTN./TEXT");
        cwf_Mcss_Calls_Error_Msg_Txt = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Error_Msg_Txt", "ERROR-MSG-TXT", FieldType.STRING, 
            60, RepeatingFieldStrategy.None, "ERROR_MSG_TXT");
        cwf_Mcss_Calls_Error_Msg_Txt.setDdmHeader("ERROR/MSG.");
        cwf_Mcss_Calls_Ph_Ssn_Nbr = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Ph_Ssn_Nbr", "PH-SSN-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "PH_SSN_NBR");
        cwf_Mcss_Calls_Ph_Home_Phone_Area_Cde = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Ph_Home_Phone_Area_Cde", "PH-HOME-PHONE-AREA-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "PH_HOME_PHONE_AREA_CDE");
        cwf_Mcss_Calls_Ph_Home_Phone_Actual_Nbr = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Ph_Home_Phone_Actual_Nbr", "PH-HOME-PHONE-ACTUAL-NBR", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "PH_HOME_PHONE_ACTUAL_NBR");
        cwf_Mcss_Calls_Ph_Wrk_Phone_Area_Cde = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Ph_Wrk_Phone_Area_Cde", "PH-WRK-PHONE-AREA-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "PH_WRK_PHONE_AREA_CDE");
        cwf_Mcss_Calls_Ph_Wrk_Phone_Actual_Nbr = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Ph_Wrk_Phone_Actual_Nbr", "PH-WRK-PHONE-ACTUAL-NBR", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "PH_WRK_PHONE_ACTUAL_NBR");
        cwf_Mcss_Calls_Ph_Wrk_Phone_Ext_Nbr = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Ph_Wrk_Phone_Ext_Nbr", "PH-WRK-PHONE-EXT-NBR", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "PH_WRK_PHONE_EXT_NBR");
        cwf_Mcss_Calls_Spouse_Birth_Dte = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Spouse_Birth_Dte", "SPOUSE-BIRTH-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "SPOUSE_BIRTH_DTE");

        cwf_Mcss_Calls_Request_Array_Grp = vw_cwf_Mcss_Calls.getRecord().newGroupInGroup("cwf_Mcss_Calls_Request_Array_Grp", "REQUEST-ARRAY-GRP", null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "CWF_MCSS_CALLS_REQUEST_ARRAY_GRP");
        cwf_Mcss_Calls_Action_Cde = cwf_Mcss_Calls_Request_Array_Grp.newFieldArrayInGroup("cwf_Mcss_Calls_Action_Cde", "ACTION-CDE", FieldType.STRING, 
            2, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACTION_CDE", "CWF_MCSS_CALLS_REQUEST_ARRAY_GRP");
        cwf_Mcss_Calls_Wpid_Cde = cwf_Mcss_Calls_Request_Array_Grp.newFieldArrayInGroup("cwf_Mcss_Calls_Wpid_Cde", "WPID-CDE", FieldType.STRING, 6, new 
            DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "WPID_CDE", "CWF_MCSS_CALLS_REQUEST_ARRAY_GRP");
        cwf_Mcss_Calls_Cntrct_NbrMuGroup = cwf_Mcss_Calls_Request_Array_Grp.newGroupInGroup("CWF_MCSS_CALLS_CNTRCT_NBRMuGroup", "CNTRCT_NBRMuGroup", RepeatingFieldStrategy.PeriodicGroupSubTableFieldArray, 
            "CWF_MCSS_CALLS_CNTRCT_NBR", "CWF_MCSS_CALLS_REQUEST_ARRAY_GRP");
        cwf_Mcss_Calls_Cntrct_Nbr = cwf_Mcss_Calls_Cntrct_NbrMuGroup.newFieldArrayInGroup("cwf_Mcss_Calls_Cntrct_Nbr", "CNTRCT-NBR", FieldType.STRING, 
            8, new DbsArrayController(1, 10, 1, 10) , RepeatingFieldStrategy.SubTableFieldArray, "CNTRCT_NBR", "CWF_MCSS_CALLS_REQUEST_ARRAY_GRP");
        cwf_Mcss_Calls_Rel_Tiaa_Rcvd_Dte = cwf_Mcss_Calls_Request_Array_Grp.newFieldArrayInGroup("cwf_Mcss_Calls_Rel_Tiaa_Rcvd_Dte", "REL-TIAA-RCVD-DTE", 
            FieldType.NUMERIC, 8, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "REL_TIAA_RCVD_DTE", "CWF_MCSS_CALLS_REQUEST_ARRAY_GRP");

        cwf_Mcss_Calls__R_Field_1 = cwf_Mcss_Calls_Request_Array_Grp.newGroupInGroup("cwf_Mcss_Calls__R_Field_1", "REDEFINE", cwf_Mcss_Calls_Rel_Tiaa_Rcvd_Dte);

        cwf_Mcss_Calls_Struct_3600 = cwf_Mcss_Calls_Request_Array_Grp.newGroupArrayInGroup("cwf_Mcss_Calls_Struct_3600", "STRUCT-3600", new DbsArrayController(1, 
            10));
        cwf_Mcss_Calls_Rel_Tiaa_Rcvd_Cc = cwf_Mcss_Calls_Struct_3600.newFieldInGroup("cwf_Mcss_Calls_Rel_Tiaa_Rcvd_Cc", "REL-TIAA-RCVD-CC", FieldType.NUMERIC, 
            2);
        cwf_Mcss_Calls_Rel_Tiaa_Rcvd_Yymmdd = cwf_Mcss_Calls_Struct_3600.newFieldInGroup("cwf_Mcss_Calls_Rel_Tiaa_Rcvd_Yymmdd", "REL-TIAA-RCVD-YYMMDD", 
            FieldType.NUMERIC, 6);
        cwf_Mcss_Calls_Rel_Wpid_Cde = vw_cwf_Mcss_Calls.getRecord().newFieldArrayInGroup("cwf_Mcss_Calls_Rel_Wpid_Cde", "REL-WPID-CDE", FieldType.STRING, 
            6, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "REL_WPID_CDE", "CWF_MCSS_CALLS_REQUEST_ARRAY_GRP");
        cwf_Mcss_Calls_Rel_Case_Id_Nbr = vw_cwf_Mcss_Calls.getRecord().newFieldArrayInGroup("cwf_Mcss_Calls_Rel_Case_Id_Nbr", "REL-CASE-ID-NBR", FieldType.STRING, 
            1, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "REL_CASE_ID_NBR", "CWF_MCSS_CALLS_REQUEST_ARRAY_GRP");
        cwf_Mcss_Calls_Rel_Sub_Rqst_Cde = vw_cwf_Mcss_Calls.getRecord().newFieldArrayInGroup("cwf_Mcss_Calls_Rel_Sub_Rqst_Cde", "REL-SUB-RQST-CDE", FieldType.STRING, 
            1, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "REL_SUB_RQST_CDE", "CWF_MCSS_CALLS_REQUEST_ARRAY_GRP");
        cwf_Mcss_Calls_Rel_Log_Dte_Tme = vw_cwf_Mcss_Calls.getRecord().newFieldArrayInGroup("cwf_Mcss_Calls_Rel_Log_Dte_Tme", "REL-LOG-DTE-TME", FieldType.TIME, 
            new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "REL_LOG_DTE_TME", "CWF_MCSS_CALLS_REQUEST_ARRAY_GRP");
        cwf_Mcss_Calls_Wpid_Validate_Ind = vw_cwf_Mcss_Calls.getRecord().newFieldArrayInGroup("cwf_Mcss_Calls_Wpid_Validate_Ind", "WPID-VALIDATE-IND", 
            FieldType.STRING, 1, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "WPID_VALIDATE_IND", "CWF_MCSS_CALLS_REQUEST_ARRAY_GRP");
        cwf_Mcss_Calls_Dstntn_Unt_Cde = vw_cwf_Mcss_Calls.getRecord().newFieldArrayInGroup("cwf_Mcss_Calls_Dstntn_Unt_Cde", "DSTNTN-UNT-CDE", FieldType.STRING, 
            8, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "DSTNTN_UNT_CDE", "CWF_MCSS_CALLS_REQUEST_ARRAY_GRP");
        cwf_Mcss_Calls_Mail_Rply_Cde = vw_cwf_Mcss_Calls.getRecord().newFieldArrayInGroup("cwf_Mcss_Calls_Mail_Rply_Cde", "MAIL-RPLY-CDE", FieldType.STRING, 
            1, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "MAIL_RPLY_CDE", "CWF_MCSS_CALLS_REQUEST_ARRAY_GRP");
        cwf_Mcss_Calls_Mail_Id_Nbr = vw_cwf_Mcss_Calls.getRecord().newFieldArrayInGroup("cwf_Mcss_Calls_Mail_Id_Nbr", "MAIL-ID-NBR", FieldType.STRING, 
            7, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "MAIL_ID_NBR", "CWF_MCSS_CALLS_REQUEST_ARRAY_GRP");
        cwf_Mcss_Calls_Corp_Status_Cde = vw_cwf_Mcss_Calls.getRecord().newFieldArrayInGroup("cwf_Mcss_Calls_Corp_Status_Cde", "CORP-STATUS-CDE", FieldType.STRING, 
            1, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CORP_STATUS_CDE", "CWF_MCSS_CALLS_REQUEST_ARRAY_GRP");
        cwf_Mcss_Calls_Unit_Status_Cde = vw_cwf_Mcss_Calls.getRecord().newFieldArrayInGroup("cwf_Mcss_Calls_Unit_Status_Cde", "UNIT-STATUS-CDE", FieldType.STRING, 
            4, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "UNIT_STATUS_CDE", "CWF_MCSS_CALLS_REQUEST_ARRAY_GRP");
        cwf_Mcss_Calls_Work_Req_Prty_Cde = vw_cwf_Mcss_Calls.getRecord().newFieldArrayInGroup("cwf_Mcss_Calls_Work_Req_Prty_Cde", "WORK-REQ-PRTY-CDE", 
            FieldType.STRING, 1, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "WORK_REQ_PRTY_CDE", "CWF_MCSS_CALLS_REQUEST_ARRAY_GRP");
        cwf_Mcss_Calls_Container_Id = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Container_Id", "CONTAINER-ID", FieldType.STRING, 20, 
            RepeatingFieldStrategy.None, "CONTAINER_ID");
        cwf_Mcss_Calls_Doc_Type_Spcfc_Cde = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Doc_Type_Spcfc_Cde", "DOC-TYPE-SPCFC-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "DOC_TYPE_SPCFC_CDE");

        cwf_Mcss_Calls_Doc_Text_Area_Grp = vw_cwf_Mcss_Calls.getRecord().newGroupArrayInGroup("cwf_Mcss_Calls_Doc_Text_Area_Grp", "DOC-TEXT-AREA-GRP", 
            new DbsArrayController(1, 60) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CWF_MCSS_CALLS_DOC_TEXT_AREA_GRP");
        cwf_Mcss_Calls_Doc_Txt = cwf_Mcss_Calls_Doc_Text_Area_Grp.newFieldInGroup("cwf_Mcss_Calls_Doc_Txt", "DOC-TXT", FieldType.STRING, 80, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "DOC_TXT", "CWF_MCSS_CALLS_DOC_TEXT_AREA_GRP");

        cwf_Mcss_Calls_Wfo_All_Othr_Flds = vw_cwf_Mcss_Calls.getRecord().newGroupInGroup("cwf_Mcss_Calls_Wfo_All_Othr_Flds", "WFO-ALL-OTHR-FLDS", null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "CWF_MCSS_CALLS_WFO_ALL_OTHR_FLDS");
        cwf_Mcss_Calls_Wfo_Othr_Flds = cwf_Mcss_Calls_Wfo_All_Othr_Flds.newFieldArrayInGroup("cwf_Mcss_Calls_Wfo_Othr_Flds", "WFO-OTHR-FLDS", FieldType.STRING, 
            100, new DbsArrayController(1, 99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "WFO_OTHR_FLDS", "CWF_MCSS_CALLS_WFO_ALL_OTHR_FLDS");

        cwf_Mcss_Calls__R_Field_2 = cwf_Mcss_Calls_Wfo_All_Othr_Flds.newGroupInGroup("cwf_Mcss_Calls__R_Field_2", "REDEFINE", cwf_Mcss_Calls_Wfo_Othr_Flds);
        cwf_Mcss_Calls_Wf_Ind = cwf_Mcss_Calls__R_Field_2.newFieldArrayInGroup("cwf_Mcss_Calls_Wf_Ind", "WF-IND", FieldType.STRING, 1, new DbsArrayController(1, 
            10));
        cwf_Mcss_Calls_Ind_Tracking_Id = cwf_Mcss_Calls__R_Field_2.newFieldArrayInGroup("cwf_Mcss_Calls_Ind_Tracking_Id", "IND-TRACKING-ID", FieldType.STRING, 
            20, new DbsArrayController(1, 10));
        cwf_Mcss_Calls_Inst_Tracking_Id = cwf_Mcss_Calls__R_Field_2.newFieldArrayInGroup("cwf_Mcss_Calls_Inst_Tracking_Id", "INST-TRACKING-ID", FieldType.STRING, 
            20, new DbsArrayController(1, 10));
        cwf_Mcss_Calls_Cabinet_Id = cwf_Mcss_Calls__R_Field_2.newFieldArrayInGroup("cwf_Mcss_Calls_Cabinet_Id", "CABINET-ID", FieldType.STRING, 17, new 
            DbsArrayController(1, 10));
        cwf_Mcss_Calls_Cabinet_Level = cwf_Mcss_Calls__R_Field_2.newFieldArrayInGroup("cwf_Mcss_Calls_Cabinet_Level", "CABINET-LEVEL", FieldType.STRING, 
            1, new DbsArrayController(1, 10));
        cwf_Mcss_Calls_Empl_Racf_Id = cwf_Mcss_Calls__R_Field_2.newFieldArrayInGroup("cwf_Mcss_Calls_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 
            8, new DbsArrayController(1, 10));
        cwf_Mcss_Calls_Step = cwf_Mcss_Calls__R_Field_2.newFieldArrayInGroup("cwf_Mcss_Calls_Step", "STEP", FieldType.STRING, 6, new DbsArrayController(1, 
            10));
        cwf_Mcss_Calls_Due_Dte = cwf_Mcss_Calls__R_Field_2.newFieldArrayInGroup("cwf_Mcss_Calls_Due_Dte", "DUE-DTE", FieldType.NUMERIC, 8, new DbsArrayController(1, 
            10));
        cwf_Mcss_Calls_Client_App_Cde = cwf_Mcss_Calls__R_Field_2.newFieldArrayInGroup("cwf_Mcss_Calls_Client_App_Cde", "CLIENT-APP-CDE", FieldType.STRING, 
            8, new DbsArrayController(1, 10));
        cwf_Mcss_Calls_Elctrnc_Fldr_Ind = cwf_Mcss_Calls__R_Field_2.newFieldArrayInGroup("cwf_Mcss_Calls_Elctrnc_Fldr_Ind", "ELCTRNC-FLDR-IND", FieldType.STRING, 
            1, new DbsArrayController(1, 10));
        cwf_Mcss_Calls_Rqst_Orgn_Cde = cwf_Mcss_Calls__R_Field_2.newFieldArrayInGroup("cwf_Mcss_Calls_Rqst_Orgn_Cde", "RQST-ORGN-CDE", FieldType.STRING, 
            1, new DbsArrayController(1, 10));
        cwf_Mcss_Calls_Topic = cwf_Mcss_Calls__R_Field_2.newFieldArrayInGroup("cwf_Mcss_Calls_Topic", "TOPIC", FieldType.STRING, 20, new DbsArrayController(1, 
            10));

        cwf_Mcss_Calls_Processed_Work_Rqsts_Grp = cwf_Mcss_Calls__R_Field_2.newGroupArrayInGroup("cwf_Mcss_Calls_Processed_Work_Rqsts_Grp", "PROCESSED-WORK-RQSTS-GRP", 
            new DbsArrayController(1, 11));
        cwf_Mcss_Calls_Processed_Work_Rqsts_Elmt = cwf_Mcss_Calls_Processed_Work_Rqsts_Grp.newFieldInGroup("cwf_Mcss_Calls_Processed_Work_Rqsts_Elmt", 
            "PROCESSED-WORK-RQSTS-ELMT", FieldType.STRING, 110);

        cwf_Mcss_Calls__R_Field_3 = cwf_Mcss_Calls_Processed_Work_Rqsts_Grp.newGroupInGroup("cwf_Mcss_Calls__R_Field_3", "REDEFINE", cwf_Mcss_Calls_Processed_Work_Rqsts_Elmt);
        cwf_Mcss_Calls_P_Rqst_Id = cwf_Mcss_Calls__R_Field_3.newFieldInGroup("cwf_Mcss_Calls_P_Rqst_Id", "P-RQST-ID", FieldType.STRING, 17);

        cwf_Mcss_Calls__R_Field_4 = cwf_Mcss_Calls__R_Field_3.newGroupInGroup("cwf_Mcss_Calls__R_Field_4", "REDEFINE", cwf_Mcss_Calls_P_Rqst_Id);
        cwf_Mcss_Calls_P_Tiaa_Rcvd_Date = cwf_Mcss_Calls__R_Field_4.newFieldInGroup("cwf_Mcss_Calls_P_Tiaa_Rcvd_Date", "P-TIAA-RCVD-DATE", FieldType.NUMERIC, 
            8);
        cwf_Mcss_Calls_P_Case_Id = cwf_Mcss_Calls__R_Field_4.newFieldInGroup("cwf_Mcss_Calls_P_Case_Id", "P-CASE-ID", FieldType.STRING, 1);
        cwf_Mcss_Calls_P_Wpid = cwf_Mcss_Calls__R_Field_4.newFieldInGroup("cwf_Mcss_Calls_P_Wpid", "P-WPID", FieldType.STRING, 6);
        cwf_Mcss_Calls_P_Sub_Rqst_Cde = cwf_Mcss_Calls__R_Field_4.newFieldInGroup("cwf_Mcss_Calls_P_Sub_Rqst_Cde", "P-SUB-RQST-CDE", FieldType.STRING, 
            1);
        cwf_Mcss_Calls_P_Multi_Rqst_Ind = cwf_Mcss_Calls__R_Field_4.newFieldInGroup("cwf_Mcss_Calls_P_Multi_Rqst_Ind", "P-MULTI-RQST-IND", FieldType.STRING, 
            1);
        cwf_Mcss_Calls_P_Rldt = cwf_Mcss_Calls__R_Field_3.newFieldInGroup("cwf_Mcss_Calls_P_Rldt", "P-RLDT", FieldType.STRING, 15);
        cwf_Mcss_Calls_P_Doc_Group = cwf_Mcss_Calls__R_Field_3.newFieldInGroup("cwf_Mcss_Calls_P_Doc_Group", "P-DOC-GROUP", FieldType.STRING, 54);

        cwf_Mcss_Calls__R_Field_5 = cwf_Mcss_Calls__R_Field_3.newGroupInGroup("cwf_Mcss_Calls__R_Field_5", "REDEFINE", cwf_Mcss_Calls_P_Doc_Group);
        cwf_Mcss_Calls_P_Dstntn_Unt_Cde = cwf_Mcss_Calls__R_Field_5.newFieldInGroup("cwf_Mcss_Calls_P_Dstntn_Unt_Cde", "P-DSTNTN-UNT-CDE", FieldType.STRING, 
            8);
        cwf_Mcss_Calls_P_Fil1 = cwf_Mcss_Calls__R_Field_5.newFieldInGroup("cwf_Mcss_Calls_P_Fil1", "P-FIL1", FieldType.STRING, 2);
        cwf_Mcss_Calls_P_Topic = cwf_Mcss_Calls__R_Field_5.newFieldInGroup("cwf_Mcss_Calls_P_Topic", "P-TOPIC", FieldType.STRING, 20);
        cwf_Mcss_Calls_P_Fil2 = cwf_Mcss_Calls__R_Field_5.newFieldInGroup("cwf_Mcss_Calls_P_Fil2", "P-FIL2", FieldType.STRING, 2);
        cwf_Mcss_Calls_P_Wpid_Cde = cwf_Mcss_Calls__R_Field_5.newFieldInGroup("cwf_Mcss_Calls_P_Wpid_Cde", "P-WPID-CDE", FieldType.STRING, 6);
        cwf_Mcss_Calls_P_Fil3 = cwf_Mcss_Calls__R_Field_5.newFieldInGroup("cwf_Mcss_Calls_P_Fil3", "P-FIL3", FieldType.STRING, 2);
        cwf_Mcss_Calls_P_Corp_Status = cwf_Mcss_Calls__R_Field_5.newFieldInGroup("cwf_Mcss_Calls_P_Corp_Status", "P-CORP-STATUS", FieldType.STRING, 5);
        cwf_Mcss_Calls_P_Fil4 = cwf_Mcss_Calls__R_Field_5.newFieldInGroup("cwf_Mcss_Calls_P_Fil4", "P-FIL4", FieldType.STRING, 2);
        cwf_Mcss_Calls_P_Wf_Ind = cwf_Mcss_Calls__R_Field_5.newFieldInGroup("cwf_Mcss_Calls_P_Wf_Ind", "P-WF-IND", FieldType.STRING, 1);
        cwf_Mcss_Calls_P_Fil5 = cwf_Mcss_Calls__R_Field_5.newFieldInGroup("cwf_Mcss_Calls_P_Fil5", "P-FIL5", FieldType.STRING, 3);
        cwf_Mcss_Calls_P_Action_Cde = cwf_Mcss_Calls__R_Field_5.newFieldInGroup("cwf_Mcss_Calls_P_Action_Cde", "P-ACTION-CDE", FieldType.STRING, 3);
        cwf_Mcss_Calls_P_Cabinet = cwf_Mcss_Calls__R_Field_3.newFieldInGroup("cwf_Mcss_Calls_P_Cabinet", "P-CABINET", FieldType.STRING, 17);

        cwf_Mcss_Calls__R_Field_6 = cwf_Mcss_Calls__R_Field_3.newGroupInGroup("cwf_Mcss_Calls__R_Field_6", "REDEFINE", cwf_Mcss_Calls_P_Cabinet);
        cwf_Mcss_Calls_P_Prefix = cwf_Mcss_Calls__R_Field_6.newFieldInGroup("cwf_Mcss_Calls_P_Prefix", "P-PREFIX", FieldType.STRING, 1);
        cwf_Mcss_Calls_P_Pin = cwf_Mcss_Calls__R_Field_6.newFieldInGroup("cwf_Mcss_Calls_P_Pin", "P-PIN", FieldType.NUMERIC, 12);
        cwf_Mcss_Calls_P_Cabinet_Level = cwf_Mcss_Calls__R_Field_3.newFieldInGroup("cwf_Mcss_Calls_P_Cabinet_Level", "P-CABINET-LEVEL", FieldType.STRING, 
            1);
        cwf_Mcss_Calls_Processed_Text_Ind = cwf_Mcss_Calls__R_Field_2.newFieldArrayInGroup("cwf_Mcss_Calls_Processed_Text_Ind", "PROCESSED-TEXT-IND", 
            FieldType.STRING, 1, new DbsArrayController(1, 10));
        cwf_Mcss_Calls_Part_Orig_Crte_Dte_Tme = cwf_Mcss_Calls__R_Field_2.newFieldInGroup("cwf_Mcss_Calls_Part_Orig_Crte_Dte_Tme", "PART-ORIG-CRTE-DTE-TME", 
            FieldType.STRING, 15);
        cwf_Mcss_Calls_Inst_Orig_Crte_Dte_Tme = cwf_Mcss_Calls__R_Field_2.newFieldInGroup("cwf_Mcss_Calls_Inst_Orig_Crte_Dte_Tme", "INST-ORIG-CRTE-DTE-TME", 
            FieldType.STRING, 15);
        cwf_Mcss_Calls_Nonp_Orig_Crte_Dte_Tme = cwf_Mcss_Calls__R_Field_2.newFieldInGroup("cwf_Mcss_Calls_Nonp_Orig_Crte_Dte_Tme", "NONP-ORIG-CRTE-DTE-TME", 
            FieldType.STRING, 15);
        cwf_Mcss_Calls_Part_Orig_Document_Name = cwf_Mcss_Calls__R_Field_2.newFieldInGroup("cwf_Mcss_Calls_Part_Orig_Document_Name", "PART-ORIG-DOCUMENT-NAME", 
            FieldType.STRING, 32);

        cwf_Mcss_Calls__R_Field_7 = cwf_Mcss_Calls__R_Field_2.newGroupInGroup("cwf_Mcss_Calls__R_Field_7", "REDEFINE", cwf_Mcss_Calls_Part_Orig_Document_Name);
        cwf_Mcss_Calls_Part_Orig_Category = cwf_Mcss_Calls__R_Field_7.newFieldInGroup("cwf_Mcss_Calls_Part_Orig_Category", "PART-ORIG-CATEGORY", FieldType.STRING, 
            1);
        cwf_Mcss_Calls_Part_Orig_Class = cwf_Mcss_Calls__R_Field_7.newFieldInGroup("cwf_Mcss_Calls_Part_Orig_Class", "PART-ORIG-CLASS", FieldType.STRING, 
            3);
        cwf_Mcss_Calls_Part_Orig_Work_Prcss_Id = cwf_Mcss_Calls__R_Field_7.newFieldInGroup("cwf_Mcss_Calls_Part_Orig_Work_Prcss_Id", "PART-ORIG-WORK-PRCSS-ID", 
            FieldType.STRING, 6);
        cwf_Mcss_Calls_Part_Orig_Specific = cwf_Mcss_Calls__R_Field_7.newFieldInGroup("cwf_Mcss_Calls_Part_Orig_Specific", "PART-ORIG-SPECIFIC", FieldType.STRING, 
            4);
        cwf_Mcss_Calls_Part_Orig_Creation_Cc = cwf_Mcss_Calls__R_Field_7.newFieldInGroup("cwf_Mcss_Calls_Part_Orig_Creation_Cc", "PART-ORIG-CREATION-CC", 
            FieldType.STRING, 1);
        cwf_Mcss_Calls_Part_Orig_Creation_Yymmdd = cwf_Mcss_Calls__R_Field_7.newFieldInGroup("cwf_Mcss_Calls_Part_Orig_Creation_Yymmdd", "PART-ORIG-CREATION-YYMMDD", 
            FieldType.STRING, 6);
        cwf_Mcss_Calls_Part_Orig_Direction = cwf_Mcss_Calls__R_Field_7.newFieldInGroup("cwf_Mcss_Calls_Part_Orig_Direction", "PART-ORIG-DIRECTION", FieldType.STRING, 
            1);
        cwf_Mcss_Calls_Part_Orig_Secured_Ind = cwf_Mcss_Calls__R_Field_7.newFieldInGroup("cwf_Mcss_Calls_Part_Orig_Secured_Ind", "PART-ORIG-SECURED-IND", 
            FieldType.STRING, 1);
        cwf_Mcss_Calls_Part_Orig_Retention_Ind = cwf_Mcss_Calls__R_Field_7.newFieldInGroup("cwf_Mcss_Calls_Part_Orig_Retention_Ind", "PART-ORIG-RETENTION-IND", 
            FieldType.STRING, 1);
        cwf_Mcss_Calls_Part_Orig_Format = cwf_Mcss_Calls__R_Field_7.newFieldInGroup("cwf_Mcss_Calls_Part_Orig_Format", "PART-ORIG-FORMAT", FieldType.STRING, 
            1);
        cwf_Mcss_Calls_Part_Orig_Tie_Breaker = cwf_Mcss_Calls__R_Field_7.newFieldInGroup("cwf_Mcss_Calls_Part_Orig_Tie_Breaker", "PART-ORIG-TIE-BREAKER", 
            FieldType.STRING, 1);
        cwf_Mcss_Calls_Part_Orig_Copy_Ind = cwf_Mcss_Calls__R_Field_7.newFieldInGroup("cwf_Mcss_Calls_Part_Orig_Copy_Ind", "PART-ORIG-COPY-IND", FieldType.STRING, 
            1);
        cwf_Mcss_Calls_Part_Orig_Important_Ind = cwf_Mcss_Calls__R_Field_7.newFieldInGroup("cwf_Mcss_Calls_Part_Orig_Important_Ind", "PART-ORIG-IMPORTANT-IND", 
            FieldType.STRING, 1);
        cwf_Mcss_Calls_Part_Orig_Package_Nbr = cwf_Mcss_Calls__R_Field_7.newFieldInGroup("cwf_Mcss_Calls_Part_Orig_Package_Nbr", "PART-ORIG-PACKAGE-NBR", 
            FieldType.NUMERIC, 4);
        cwf_Mcss_Calls_Inst_Orig_Document_Name = cwf_Mcss_Calls__R_Field_2.newFieldInGroup("cwf_Mcss_Calls_Inst_Orig_Document_Name", "INST-ORIG-DOCUMENT-NAME", 
            FieldType.STRING, 10);

        cwf_Mcss_Calls__R_Field_8 = cwf_Mcss_Calls__R_Field_2.newGroupInGroup("cwf_Mcss_Calls__R_Field_8", "REDEFINE", cwf_Mcss_Calls_Inst_Orig_Document_Name);
        cwf_Mcss_Calls_Inst_Orig_Category = cwf_Mcss_Calls__R_Field_8.newFieldInGroup("cwf_Mcss_Calls_Inst_Orig_Category", "INST-ORIG-CATEGORY", FieldType.STRING, 
            1);
        cwf_Mcss_Calls_Inst_Orig_Class = cwf_Mcss_Calls__R_Field_8.newFieldInGroup("cwf_Mcss_Calls_Inst_Orig_Class", "INST-ORIG-CLASS", FieldType.STRING, 
            3);
        cwf_Mcss_Calls_Inst_Orig_Specific = cwf_Mcss_Calls__R_Field_8.newFieldInGroup("cwf_Mcss_Calls_Inst_Orig_Specific", "INST-ORIG-SPECIFIC", FieldType.STRING, 
            4);
        cwf_Mcss_Calls_Inst_Orig_Direction = cwf_Mcss_Calls__R_Field_8.newFieldInGroup("cwf_Mcss_Calls_Inst_Orig_Direction", "INST-ORIG-DIRECTION", FieldType.STRING, 
            1);
        cwf_Mcss_Calls_Inst_Orig_Format = cwf_Mcss_Calls__R_Field_8.newFieldInGroup("cwf_Mcss_Calls_Inst_Orig_Format", "INST-ORIG-FORMAT", FieldType.STRING, 
            1);
        cwf_Mcss_Calls_Nonp_Orig_Document_Name = cwf_Mcss_Calls__R_Field_2.newFieldInGroup("cwf_Mcss_Calls_Nonp_Orig_Document_Name", "NONP-ORIG-DOCUMENT-NAME", 
            FieldType.STRING, 10);

        cwf_Mcss_Calls__R_Field_9 = cwf_Mcss_Calls__R_Field_2.newGroupInGroup("cwf_Mcss_Calls__R_Field_9", "REDEFINE", cwf_Mcss_Calls_Nonp_Orig_Document_Name);
        cwf_Mcss_Calls_Nonp_Orig_Category = cwf_Mcss_Calls__R_Field_9.newFieldInGroup("cwf_Mcss_Calls_Nonp_Orig_Category", "NONP-ORIG-CATEGORY", FieldType.STRING, 
            1);
        cwf_Mcss_Calls_Nonp_Orig_Class = cwf_Mcss_Calls__R_Field_9.newFieldInGroup("cwf_Mcss_Calls_Nonp_Orig_Class", "NONP-ORIG-CLASS", FieldType.STRING, 
            3);
        cwf_Mcss_Calls_Nonp_Orig_Specific = cwf_Mcss_Calls__R_Field_9.newFieldInGroup("cwf_Mcss_Calls_Nonp_Orig_Specific", "NONP-ORIG-SPECIFIC", FieldType.STRING, 
            4);
        cwf_Mcss_Calls_Nonp_Orig_Direction = cwf_Mcss_Calls__R_Field_9.newFieldInGroup("cwf_Mcss_Calls_Nonp_Orig_Direction", "NONP-ORIG-DIRECTION", FieldType.STRING, 
            1);
        cwf_Mcss_Calls_Nonp_Orig_Format = cwf_Mcss_Calls__R_Field_9.newFieldInGroup("cwf_Mcss_Calls_Nonp_Orig_Format", "NONP-ORIG-FORMAT", FieldType.STRING, 
            1);
        cwf_Mcss_Calls_Cntrct = cwf_Mcss_Calls__R_Field_2.newFieldArrayInGroup("cwf_Mcss_Calls_Cntrct", "CNTRCT", FieldType.STRING, 10, new DbsArrayController(1, 
            10, 1, 10));

        cwf_Mcss_Calls_Processed2_Work_Rqsts_Grp = cwf_Mcss_Calls__R_Field_2.newGroupArrayInGroup("cwf_Mcss_Calls_Processed2_Work_Rqsts_Grp", "PROCESSED2-WORK-RQSTS-GRP", 
            new DbsArrayController(1, 10));
        cwf_Mcss_Calls_P_Tracking_Id = cwf_Mcss_Calls_Processed2_Work_Rqsts_Grp.newFieldInGroup("cwf_Mcss_Calls_P_Tracking_Id", "P-TRACKING-ID", FieldType.STRING, 
            20);
        cwf_Mcss_Calls_P_Ati_Ind = cwf_Mcss_Calls__R_Field_2.newFieldInGroup("cwf_Mcss_Calls_P_Ati_Ind", "P-ATI-IND", FieldType.STRING, 1);
        cwf_Mcss_Calls_Lead_Class = cwf_Mcss_Calls__R_Field_2.newFieldArrayInGroup("cwf_Mcss_Calls_Lead_Class", "LEAD-CLASS", FieldType.STRING, 4, new 
            DbsArrayController(1, 10));
        cwf_Mcss_Calls_Express_Ind = cwf_Mcss_Calls__R_Field_2.newFieldInGroup("cwf_Mcss_Calls_Express_Ind", "EXPRESS-IND", FieldType.STRING, 1);
        cwf_Mcss_Calls_Mail_Cde = cwf_Mcss_Calls__R_Field_2.newFieldArrayInGroup("cwf_Mcss_Calls_Mail_Cde", "MAIL-CDE", FieldType.STRING, 2, new DbsArrayController(1, 
            10));

        cwf_Mcss_Calls__R_Field_10 = cwf_Mcss_Calls_Wfo_All_Othr_Flds.newGroupInGroup("cwf_Mcss_Calls__R_Field_10", "REDEFINE", cwf_Mcss_Calls_Wfo_Othr_Flds);
        cwf_Mcss_Calls_Dcmnt_Obj_Id = cwf_Mcss_Calls__R_Field_10.newFieldInGroup("cwf_Mcss_Calls_Dcmnt_Obj_Id", "DCMNT-OBJ-ID", FieldType.STRING, 20);
        cwf_Mcss_Calls_Mail_Item_No = cwf_Mcss_Calls__R_Field_10.newFieldInGroup("cwf_Mcss_Calls_Mail_Item_No", "MAIL-ITEM-NO", FieldType.STRING, 11);
        cwf_Mcss_Calls_T_Client_App_Cde = cwf_Mcss_Calls__R_Field_10.newFieldInGroup("cwf_Mcss_Calls_T_Client_App_Cde", "T-CLIENT-APP-CDE", FieldType.STRING, 
            8);
        cwf_Mcss_Calls_Tracking_Rcrd_NbrMuGroup = vw_cwf_Mcss_Calls.getRecord().newGroupInGroup("CWF_MCSS_CALLS_TRACKING_RCRD_NBRMuGroup", "TRACKING_RCRD_NBRMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "CWF_MCSS_CALLS_TRACKING_RCRD_NBR");
        cwf_Mcss_Calls_Tracking_Rcrd_Nbr = cwf_Mcss_Calls_Tracking_Rcrd_NbrMuGroup.newFieldArrayInGroup("cwf_Mcss_Calls_Tracking_Rcrd_Nbr", "TRACKING-RCRD-NBR", 
            FieldType.STRING, 20, new DbsArrayController(1, 10), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "TRACKING_RCRD_NBR");
        cwf_Mcss_Calls_Wfo_Ind = vw_cwf_Mcss_Calls.getRecord().newFieldInGroup("cwf_Mcss_Calls_Wfo_Ind", "WFO-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "WFO_IND");
        registerRecord(vw_cwf_Mcss_Calls);

        vw_cwf_Mcss_Calls2 = new DataAccessProgramView(new NameInfo("vw_cwf_Mcss_Calls2", "CWF-MCSS-CALLS2"), "CWF_MCSS_CALLS", "CWF_MCSS_CALLS");
        cwf_Mcss_Calls2_Container_Id = vw_cwf_Mcss_Calls2.getRecord().newFieldInGroup("cwf_Mcss_Calls2_Container_Id", "CONTAINER-ID", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "CONTAINER_ID");
        cwf_Mcss_Calls2_Error_Msg_Txt = vw_cwf_Mcss_Calls2.getRecord().newFieldInGroup("cwf_Mcss_Calls2_Error_Msg_Txt", "ERROR-MSG-TXT", FieldType.STRING, 
            60, RepeatingFieldStrategy.None, "ERROR_MSG_TXT");
        cwf_Mcss_Calls2_Error_Msg_Txt.setDdmHeader("ERROR/MSG.");
        registerRecord(vw_cwf_Mcss_Calls2);

        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Input_Time = localVariables.newFieldInRecord("pnd_Input_Time", "#INPUT-TIME", FieldType.PACKED_DECIMAL, 6);
        pnd_First_Rec = localVariables.newFieldInRecord("pnd_First_Rec", "#FIRST-REC", FieldType.STRING, 1);
        pnd_Date_Time = localVariables.newFieldInRecord("pnd_Date_Time", "#DATE-TIME", FieldType.STRING, 14);

        pnd_Date_Time__R_Field_11 = localVariables.newGroupInRecord("pnd_Date_Time__R_Field_11", "REDEFINE", pnd_Date_Time);
        pnd_Date_Time_Pnd_Date = pnd_Date_Time__R_Field_11.newFieldInGroup("pnd_Date_Time_Pnd_Date", "#DATE", FieldType.STRING, 8);
        pnd_Date_Time_Pnd_Time = pnd_Date_Time__R_Field_11.newFieldInGroup("pnd_Date_Time_Pnd_Time", "#TIME", FieldType.STRING, 6);
        pnd_Chk_Dtetme = localVariables.newFieldInRecord("pnd_Chk_Dtetme", "#CHK-DTETME", FieldType.STRING, 14);

        pnd_Chk_Dtetme__R_Field_12 = localVariables.newGroupInRecord("pnd_Chk_Dtetme__R_Field_12", "REDEFINE", pnd_Chk_Dtetme);
        pnd_Chk_Dtetme_Pnd_Chk_Date = pnd_Chk_Dtetme__R_Field_12.newFieldInGroup("pnd_Chk_Dtetme_Pnd_Chk_Date", "#CHK-DATE", FieldType.STRING, 8);

        pnd_Chk_Dtetme__R_Field_13 = pnd_Chk_Dtetme__R_Field_12.newGroupInGroup("pnd_Chk_Dtetme__R_Field_13", "REDEFINE", pnd_Chk_Dtetme_Pnd_Chk_Date);
        pnd_Chk_Dtetme_Pnd_Chk_Yyyy = pnd_Chk_Dtetme__R_Field_13.newFieldInGroup("pnd_Chk_Dtetme_Pnd_Chk_Yyyy", "#CHK-YYYY", FieldType.NUMERIC, 4);
        pnd_Chk_Dtetme_Pnd_Chk_Mm = pnd_Chk_Dtetme__R_Field_13.newFieldInGroup("pnd_Chk_Dtetme_Pnd_Chk_Mm", "#CHK-MM", FieldType.NUMERIC, 2);
        pnd_Chk_Dtetme_Pnd_Chk_Time = pnd_Chk_Dtetme__R_Field_12.newFieldInGroup("pnd_Chk_Dtetme_Pnd_Chk_Time", "#CHK-TIME", FieldType.STRING, 6);
        pnd_Date_A = localVariables.newFieldInRecord("pnd_Date_A", "#DATE-A", FieldType.STRING, 14);

        pnd_Date_A__R_Field_14 = localVariables.newGroupInRecord("pnd_Date_A__R_Field_14", "REDEFINE", pnd_Date_A);
        pnd_Date_A_Pnd_Date_Num = pnd_Date_A__R_Field_14.newFieldInGroup("pnd_Date_A_Pnd_Date_Num", "#DATE-NUM", FieldType.NUMERIC, 14);
        pnd_Tot_Delete = localVariables.newFieldInRecord("pnd_Tot_Delete", "#TOT-DELETE", FieldType.NUMERIC, 8);
        pnd_Rec_Cnt = localVariables.newFieldInRecord("pnd_Rec_Cnt", "#REC-CNT", FieldType.NUMERIC, 4);
        pnd_Upld_Date = localVariables.newFieldInRecord("pnd_Upld_Date", "#UPLD-DATE", FieldType.DATE);
        pnd_Upld_Time = localVariables.newFieldInRecord("pnd_Upld_Time", "#UPLD-TIME", FieldType.STRING, 4);

        pnd_Upld_Time__R_Field_15 = localVariables.newGroupInRecord("pnd_Upld_Time__R_Field_15", "REDEFINE", pnd_Upld_Time);
        pnd_Upld_Time_Pnd_Upld_Timen = pnd_Upld_Time__R_Field_15.newFieldInGroup("pnd_Upld_Time_Pnd_Upld_Timen", "#UPLD-TIMEN", FieldType.NUMERIC, 4);
        pnd_Input_Sys = localVariables.newFieldInRecord("pnd_Input_Sys", "#INPUT-SYS", FieldType.STRING, 15);
        pnd_Input_From = localVariables.newFieldInRecord("pnd_Input_From", "#INPUT-FROM", FieldType.STRING, 8);

        pnd_Input_From__R_Field_16 = localVariables.newGroupInRecord("pnd_Input_From__R_Field_16", "REDEFINE", pnd_Input_From);
        pnd_Input_From_Yyyy = pnd_Input_From__R_Field_16.newFieldInGroup("pnd_Input_From_Yyyy", "YYYY", FieldType.NUMERIC, 4);
        pnd_Input_From_Mm = pnd_Input_From__R_Field_16.newFieldInGroup("pnd_Input_From_Mm", "MM", FieldType.NUMERIC, 2);
        pnd_Input_From_Dd = pnd_Input_From__R_Field_16.newFieldInGroup("pnd_Input_From_Dd", "DD", FieldType.NUMERIC, 2);
        pnd_Input_From_D = localVariables.newFieldInRecord("pnd_Input_From_D", "#INPUT-FROM-D", FieldType.DATE);
        pnd_Input_To = localVariables.newFieldInRecord("pnd_Input_To", "#INPUT-TO", FieldType.STRING, 8);

        pnd_Input_To__R_Field_17 = localVariables.newGroupInRecord("pnd_Input_To__R_Field_17", "REDEFINE", pnd_Input_To);
        pnd_Input_To_Yyyy = pnd_Input_To__R_Field_17.newFieldInGroup("pnd_Input_To_Yyyy", "YYYY", FieldType.NUMERIC, 4);
        pnd_Input_To_Mm = pnd_Input_To__R_Field_17.newFieldInGroup("pnd_Input_To_Mm", "MM", FieldType.NUMERIC, 2);
        pnd_Input_To_Dd = pnd_Input_To__R_Field_17.newFieldInGroup("pnd_Input_To_Dd", "DD", FieldType.NUMERIC, 2);
        pnd_Input_To_D = localVariables.newFieldInRecord("pnd_Input_To_D", "#INPUT-TO-D", FieldType.DATE);
        pnd_Input_To_T = localVariables.newFieldInRecord("pnd_Input_To_T", "#INPUT-TO-T", FieldType.TIME);
        pnd_Input = localVariables.newFieldInRecord("pnd_Input", "#INPUT", FieldType.STRING, 8);

        pnd_Input__R_Field_18 = localVariables.newGroupInRecord("pnd_Input__R_Field_18", "REDEFINE", pnd_Input);
        pnd_Input_Yyyy = pnd_Input__R_Field_18.newFieldInGroup("pnd_Input_Yyyy", "YYYY", FieldType.NUMERIC, 4);
        pnd_Input_Mm = pnd_Input__R_Field_18.newFieldInGroup("pnd_Input_Mm", "MM", FieldType.NUMERIC, 2);
        pnd_Input_Dd = pnd_Input__R_Field_18.newFieldInGroup("pnd_Input_Dd", "DD", FieldType.NUMERIC, 2);
        pnd_Cwf_Mcss_Key = localVariables.newFieldInRecord("pnd_Cwf_Mcss_Key", "#CWF-MCSS-KEY", FieldType.STRING, 41);

        pnd_Cwf_Mcss_Key__R_Field_19 = localVariables.newGroupInRecord("pnd_Cwf_Mcss_Key__R_Field_19", "REDEFINE", pnd_Cwf_Mcss_Key);
        pnd_Cwf_Mcss_Key_Systm_Ind = pnd_Cwf_Mcss_Key__R_Field_19.newFieldInGroup("pnd_Cwf_Mcss_Key_Systm_Ind", "SYSTM-IND", FieldType.STRING, 15);
        pnd_Cwf_Mcss_Key_Rcrd_Upld_Tme = pnd_Cwf_Mcss_Key__R_Field_19.newFieldInGroup("pnd_Cwf_Mcss_Key_Rcrd_Upld_Tme", "RCRD-UPLD-TME", FieldType.TIME);
        pnd_Cwf_Mcss_Key_Rcrd_Type_Cde = pnd_Cwf_Mcss_Key__R_Field_19.newFieldInGroup("pnd_Cwf_Mcss_Key_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 
            1);
        pnd_Cwf_Mcss_Key_Btch_Nbr = pnd_Cwf_Mcss_Key__R_Field_19.newFieldInGroup("pnd_Cwf_Mcss_Key_Btch_Nbr", "BTCH-NBR", FieldType.NUMERIC, 8);
        pnd_Cwf_Mcss_Key_Contact_Id_Cde = pnd_Cwf_Mcss_Key__R_Field_19.newFieldInGroup("pnd_Cwf_Mcss_Key_Contact_Id_Cde", "CONTACT-ID-CDE", FieldType.STRING, 
            10);
        pnd_Whole_Container_Processed = localVariables.newFieldInRecord("pnd_Whole_Container_Processed", "#WHOLE-CONTAINER-PROCESSED", FieldType.BOOLEAN, 
            1);
        pnd_Container_Rcrd_Type_Key = localVariables.newFieldInRecord("pnd_Container_Rcrd_Type_Key", "#CONTAINER-RCRD-TYPE-KEY", FieldType.STRING, 21);

        pnd_Container_Rcrd_Type_Key__R_Field_20 = localVariables.newGroupInRecord("pnd_Container_Rcrd_Type_Key__R_Field_20", "REDEFINE", pnd_Container_Rcrd_Type_Key);
        pnd_Container_Rcrd_Type_Key_Pnd_Container = pnd_Container_Rcrd_Type_Key__R_Field_20.newFieldInGroup("pnd_Container_Rcrd_Type_Key_Pnd_Container", 
            "#CONTAINER", FieldType.STRING, 20);
        pnd_Container_Rcrd_Type_Key_Pnd_Rcrd_Type = pnd_Container_Rcrd_Type_Key__R_Field_20.newFieldInGroup("pnd_Container_Rcrd_Type_Key_Pnd_Rcrd_Type", 
            "#RCRD-TYPE", FieldType.STRING, 1);
        pnd_Container_Rcrd_Type_Key2 = localVariables.newFieldInRecord("pnd_Container_Rcrd_Type_Key2", "#CONTAINER-RCRD-TYPE-KEY2", FieldType.STRING, 
            21);

        pnd_Container_Rcrd_Type_Key2__R_Field_21 = localVariables.newGroupInRecord("pnd_Container_Rcrd_Type_Key2__R_Field_21", "REDEFINE", pnd_Container_Rcrd_Type_Key2);
        pnd_Container_Rcrd_Type_Key2_Pnd_Container2 = pnd_Container_Rcrd_Type_Key2__R_Field_21.newFieldInGroup("pnd_Container_Rcrd_Type_Key2_Pnd_Container2", 
            "#CONTAINER2", FieldType.STRING, 20);
        pnd_Container_Rcrd_Type_Key2_Pnd_Rcrd_Type2 = pnd_Container_Rcrd_Type_Key2__R_Field_21.newFieldInGroup("pnd_Container_Rcrd_Type_Key2_Pnd_Rcrd_Type2", 
            "#RCRD-TYPE2", FieldType.STRING, 1);
        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_22 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_22", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_22.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_22.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_22.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind_2_2 = pnd_Tbl_Prime_Key__R_Field_22.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind_2_2", "#TBL-ACTVE-IND-2-2", 
            FieldType.STRING, 1);

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");

        cwf_Support_Tbl__R_Field_23 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_23", "REDEFINE", cwf_Support_Tbl_Tbl_Key_Field);
        cwf_Support_Tbl_Pnd_Client = cwf_Support_Tbl__R_Field_23.newFieldInGroup("cwf_Support_Tbl_Pnd_Client", "#CLIENT", FieldType.STRING, 8);
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");
        cwf_Support_Tbl_Tbl_Actve_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Actve_Ind", "TBL-ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TBL_ACTVE_IND");

        cwf_Support_Tbl__R_Field_24 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_24", "REDEFINE", cwf_Support_Tbl_Tbl_Actve_Ind);
        cwf_Support_Tbl_Fill_1 = cwf_Support_Tbl__R_Field_24.newFieldInGroup("cwf_Support_Tbl_Fill_1", "FILL-1", FieldType.STRING, 1);
        cwf_Support_Tbl_Tbl_Actve_Ind_2_2 = cwf_Support_Tbl__R_Field_24.newFieldInGroup("cwf_Support_Tbl_Tbl_Actve_Ind_2_2", "TBL-ACTVE-IND-2-2", FieldType.STRING, 
            1);
        registerRecord(vw_cwf_Support_Tbl);

        pnd_Client_App_Cde = localVariables.newFieldInRecord("pnd_Client_App_Cde", "#CLIENT-APP-CDE", FieldType.STRING, 8);
        pnd_Cleanup = localVariables.newFieldInRecord("pnd_Cleanup", "#CLEANUP", FieldType.BOOLEAN, 1);
        pnd_Cleanup_Clients = localVariables.newFieldArrayInRecord("pnd_Cleanup_Clients", "#CLEANUP-CLIENTS", FieldType.STRING, 8, new DbsArrayController(1, 
            100));
        pnd_Client_Ctr = localVariables.newFieldInRecord("pnd_Client_Ctr", "#CLIENT-CTR", FieldType.INTEGER, 2);
        pnd_Idx = localVariables.newFieldInRecord("pnd_Idx", "#IDX", FieldType.INTEGER, 2);
        pnd_Save_Container = localVariables.newFieldInRecord("pnd_Save_Container", "#SAVE-CONTAINER", FieldType.STRING, 20);
        pnd_Save_Client_App_Cde = localVariables.newFieldInRecord("pnd_Save_Client_App_Cde", "#SAVE-CLIENT-APP-CDE", FieldType.STRING, 8);
        pnd_Tracking_Type_Key = localVariables.newFieldInRecord("pnd_Tracking_Type_Key", "#TRACKING-TYPE-KEY", FieldType.STRING, 42);

        pnd_Tracking_Type_Key__R_Field_25 = localVariables.newGroupInRecord("pnd_Tracking_Type_Key__R_Field_25", "REDEFINE", pnd_Tracking_Type_Key);
        pnd_Tracking_Type_Key_Pnd_T_Container_Id = pnd_Tracking_Type_Key__R_Field_25.newFieldInGroup("pnd_Tracking_Type_Key_Pnd_T_Container_Id", "#T-CONTAINER-ID", 
            FieldType.STRING, 20);
        pnd_Tracking_Type_Key_Pnd_T_Rcrd_Type_Cde = pnd_Tracking_Type_Key__R_Field_25.newFieldInGroup("pnd_Tracking_Type_Key_Pnd_T_Rcrd_Type_Cde", "#T-RCRD-TYPE-CDE", 
            FieldType.STRING, 1);
        pnd_Tracking_Type_Key_Pnd_T_Wfo_Ind = pnd_Tracking_Type_Key__R_Field_25.newFieldInGroup("pnd_Tracking_Type_Key_Pnd_T_Wfo_Ind", "#T-WFO-IND", FieldType.STRING, 
            1);
        pnd_Tracking_Type_Key_Pnd_T_Tracking_Rcrd_Nbr = pnd_Tracking_Type_Key__R_Field_25.newFieldInGroup("pnd_Tracking_Type_Key_Pnd_T_Tracking_Rcrd_Nbr", 
            "#T-TRACKING-RCRD-NBR", FieldType.STRING, 20);

        vw_cwf_Mcss_Calls3 = new DataAccessProgramView(new NameInfo("vw_cwf_Mcss_Calls3", "CWF-MCSS-CALLS3"), "CWF_MCSS_CALLS", "CWF_MCSS_CALLS", DdmPeriodicGroups.getInstance().getGroups("CWF_MCSS_CALLS"));

        cwf_Mcss_Calls3_Wfo_All_Othr_Flds = vw_cwf_Mcss_Calls3.getRecord().newGroupInGroup("cwf_Mcss_Calls3_Wfo_All_Othr_Flds", "WFO-ALL-OTHR-FLDS", null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "CWF_MCSS_CALLS_WFO_ALL_OTHR_FLDS");
        cwf_Mcss_Calls3_Wfo_Othr_Flds = cwf_Mcss_Calls3_Wfo_All_Othr_Flds.newFieldArrayInGroup("cwf_Mcss_Calls3_Wfo_Othr_Flds", "WFO-OTHR-FLDS", FieldType.STRING, 
            100, new DbsArrayController(1, 99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "WFO_OTHR_FLDS", "CWF_MCSS_CALLS_WFO_ALL_OTHR_FLDS");

        cwf_Mcss_Calls3__R_Field_26 = cwf_Mcss_Calls3_Wfo_All_Othr_Flds.newGroupInGroup("cwf_Mcss_Calls3__R_Field_26", "REDEFINE", cwf_Mcss_Calls3_Wfo_Othr_Flds);
        cwf_Mcss_Calls3_Wf_Ind = cwf_Mcss_Calls3__R_Field_26.newFieldArrayInGroup("cwf_Mcss_Calls3_Wf_Ind", "WF-IND", FieldType.STRING, 1, new DbsArrayController(1, 
            10));
        cwf_Mcss_Calls3_Ind_Tracking_Id = cwf_Mcss_Calls3__R_Field_26.newFieldArrayInGroup("cwf_Mcss_Calls3_Ind_Tracking_Id", "IND-TRACKING-ID", FieldType.STRING, 
            20, new DbsArrayController(1, 10));
        cwf_Mcss_Calls3_Inst_Tracking_Id = cwf_Mcss_Calls3__R_Field_26.newFieldArrayInGroup("cwf_Mcss_Calls3_Inst_Tracking_Id", "INST-TRACKING-ID", FieldType.STRING, 
            20, new DbsArrayController(1, 10));
        cwf_Mcss_Calls3_Cabinet_Id = cwf_Mcss_Calls3__R_Field_26.newFieldArrayInGroup("cwf_Mcss_Calls3_Cabinet_Id", "CABINET-ID", FieldType.STRING, 17, 
            new DbsArrayController(1, 10));
        cwf_Mcss_Calls3_Cabinet_Level = cwf_Mcss_Calls3__R_Field_26.newFieldArrayInGroup("cwf_Mcss_Calls3_Cabinet_Level", "CABINET-LEVEL", FieldType.STRING, 
            1, new DbsArrayController(1, 10));
        cwf_Mcss_Calls3_Empl_Racf_Id = cwf_Mcss_Calls3__R_Field_26.newFieldArrayInGroup("cwf_Mcss_Calls3_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 
            8, new DbsArrayController(1, 10));
        cwf_Mcss_Calls3_Step = cwf_Mcss_Calls3__R_Field_26.newFieldArrayInGroup("cwf_Mcss_Calls3_Step", "STEP", FieldType.STRING, 6, new DbsArrayController(1, 
            10));
        cwf_Mcss_Calls3_Due_Dte = cwf_Mcss_Calls3__R_Field_26.newFieldArrayInGroup("cwf_Mcss_Calls3_Due_Dte", "DUE-DTE", FieldType.NUMERIC, 8, new DbsArrayController(1, 
            10));
        cwf_Mcss_Calls3_Client_App_Cde = cwf_Mcss_Calls3__R_Field_26.newFieldArrayInGroup("cwf_Mcss_Calls3_Client_App_Cde", "CLIENT-APP-CDE", FieldType.STRING, 
            8, new DbsArrayController(1, 10));
        registerRecord(vw_cwf_Mcss_Calls3);

        pnd_Mo_Cont = localVariables.newFieldInRecord("pnd_Mo_Cont", "#MO-CONT", FieldType.BOOLEAN, 1);
        pnd_Express_Cont = localVariables.newFieldInRecord("pnd_Express_Cont", "#EXPRESS-CONT", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Mcss_Calls.reset();
        vw_cwf_Mcss_Calls2.reset();
        vw_cwf_Support_Tbl.reset();
        vw_cwf_Mcss_Calls3.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Mcsp1600() throws Exception
    {
        super("Mcsp1600");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: FORMAT ( 1 ) LS = 133 PS = 60 ZP = OFF;//Natural: ASSIGN #PROGRAM = *PROGRAM
        pnd_Input.setValue(Global.getDATN());                                                                                                                             //Natural: ASSIGN #INPUT = *DATN
        pnd_Input_From.setValue(19900101);                                                                                                                                //Natural: ASSIGN #INPUT-FROM = 19900101
        pnd_Input_To.setValue(pnd_Input);                                                                                                                                 //Natural: ASSIGN #INPUT-TO = #INPUT
        pnd_Input_Sys.setValue("aaa");                                                                                                                                    //Natural: ASSIGN #INPUT-SYS = 'aaa'
        pnd_Cwf_Mcss_Key_Systm_Ind.setValue(pnd_Input_Sys);                                                                                                               //Natural: ASSIGN #CWF-MCSS-KEY.SYSTM-IND = #INPUT-SYS
        pnd_Cwf_Mcss_Key_Rcrd_Type_Cde.setValue("A");                                                                                                                     //Natural: ASSIGN #CWF-MCSS-KEY.RCRD-TYPE-CDE = 'A'
        pnd_Date_Time_Pnd_Date.setValue(pnd_Input_From);                                                                                                                  //Natural: ASSIGN #DATE = #INPUT-FROM
        pnd_Date_Time_Pnd_Time.setValue("000001");                                                                                                                        //Natural: ASSIGN #TIME = '000001'
        pnd_Cwf_Mcss_Key_Rcrd_Upld_Tme.setValueEdited(new ReportEditMask("YYYYMMDDHHIISS"),pnd_Date_Time);                                                                //Natural: MOVE EDITED #DATE-TIME TO #CWF-MCSS-KEY.RCRD-UPLD-TME ( EM = YYYYMMDDHHIISS )
        pnd_Input_From_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_From);                                                                                   //Natural: MOVE EDITED #INPUT-FROM TO #INPUT-FROM-D ( EM = YYYYMMDD )
        pnd_Input_To_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_To);                                                                                       //Natural: MOVE EDITED #INPUT-TO TO #INPUT-TO-D ( EM = YYYYMMDD )
        pnd_Input_To_D.nsubtract(30);                                                                                                                                     //Natural: COMPUTE #INPUT-TO-D = #INPUT-TO-D - 30
        pnd_Input_To_T.setValue(pnd_Input_To_D);                                                                                                                          //Natural: MOVE #INPUT-TO-D TO #INPUT-TO-T
        //*  START EPM 01/08/01
        //*  READ THE CLIENTS WHICH NEED TO BE DELETED
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind.setValue("A ");                                                                                                         //Natural: MOVE 'A ' TO #TBL-SCRTY-LEVEL-IND
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme.setValue("WFO-CLIENTS-CLEANUP");                                                                                              //Natural: MOVE 'WFO-CLIENTS-CLEANUP' TO #TBL-TABLE-NME
        pnd_Client_Ctr.reset();                                                                                                                                           //Natural: RESET #CLIENT-CTR
        pnd_Cleanup_Clients.getValue("*").reset();                                                                                                                        //Natural: RESET #CLEANUP-CLIENTS ( * )
        vw_cwf_Support_Tbl.startDatabaseRead                                                                                                                              //Natural: READ CWF-SUPPORT-TBL WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
        (
        "READ01",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_cwf_Support_Tbl.readNextRow("READ01")))
        {
            if (condition(cwf_Support_Tbl_Tbl_Table_Nme.notEquals(pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme)))                                                                  //Natural: IF CWF-SUPPORT-TBL.TBL-TABLE-NME NE #TBL-TABLE-NME
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Client_Ctr.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #CLIENT-CTR
            pnd_Cleanup_Clients.getValue(pnd_Client_Ctr).setValue(cwf_Support_Tbl_Pnd_Client);                                                                            //Natural: MOVE CWF-SUPPORT-TBL.#CLIENT TO #CLEANUP-CLIENTS ( #CLIENT-CTR )
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  END EPM 01/08/01
        //*  READ THE AUDIT RECORDS BY UPLOAD DATE TIME
        vw_cwf_Mcss_Calls.startDatabaseRead                                                                                                                               //Natural: READ CWF-MCSS-CALLS WITH BATCH-KEY = #CWF-MCSS-KEY
        (
        "READ_AUDIT",
        new Wc[] { new Wc("BATCH_KEY", ">=", pnd_Cwf_Mcss_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("BATCH_KEY", "ASC") }
        );
        READ_AUDIT:
        while (condition(vw_cwf_Mcss_Calls.readNextRow("READ_AUDIT")))
        {
            //*  COUNT READS - NOT DELETES BECAUSE NATURAL READS WITH HOLD
            pnd_Rec_Cnt.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #REC-CNT
            //*  EPM 01/08/01
                                                                                                                                                                          //Natural: PERFORM CHECK-CLIENT-CODE
            sub_Check_Client_Code();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_AUDIT"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_AUDIT"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  IF CWF-MCSS-CALLS.RCRD-TYPE-CDE = 'A'     /* OB 09/23/98
            //*      OR CWF-MCSS-CALLS.RCRD-TYPE-CDE = 'S' /*
            //*  --- DELETE SUCCESSFUL RECORDS
            //*  EPM 04/24/2000
            //* EPM04/24/2000
            //*  EPM04/24/2000
            //*  EPM 01/08/01
            //*  EPM 01/08/01
            //*  EPM01/08/01
            //*  EPM01/08/01
            //*  EPM 06/06/02
            if (condition((cwf_Mcss_Calls_Rcrd_Upld_Tme.lessOrEqual(pnd_Input_To_T) && cwf_Mcss_Calls_Container_Id.greater(" ") && cwf_Mcss_Calls_Error_Msg_Txt.equals(" "))  //Natural: IF ( CWF-MCSS-CALLS.RCRD-UPLD-TME LE #INPUT-TO-T AND CWF-MCSS-CALLS.CONTAINER-ID GT ' ' AND CWF-MCSS-CALLS.ERROR-MSG-TXT = ' ' ) OR ( CWF-MCSS-CALLS.RCRD-UPLD-TME LE #INPUT-TO-T AND CWF-MCSS-CALLS.CONTAINER-ID = ' ' ) OR ( CWF-MCSS-CALLS.RCRD-UPLD-TME LE #INPUT-TO-T AND CWF-MCSS-CALLS.CONTAINER-ID GT ' ' AND #CLEANUP ) OR ( CWF-MCSS-CALLS.RCRD-UPLD-TME LE #INPUT-TO-T AND CWF-MCSS-CALLS.CONTAINER-ID GT ' ' AND ( #EXPRESS-CONT EQ 'Y' OR #MO-CONT ) )
                || (cwf_Mcss_Calls_Rcrd_Upld_Tme.lessOrEqual(pnd_Input_To_T) && cwf_Mcss_Calls_Container_Id.equals(" ")) || (cwf_Mcss_Calls_Rcrd_Upld_Tme.lessOrEqual(pnd_Input_To_T) 
                && cwf_Mcss_Calls_Container_Id.greater(" ") && pnd_Cleanup.getBoolean()) || (cwf_Mcss_Calls_Rcrd_Upld_Tme.lessOrEqual(pnd_Input_To_T) && 
                cwf_Mcss_Calls_Container_Id.greater(" ") && (pnd_Express_Cont.equals("Y") || pnd_Mo_Cont.getBoolean()))))
            {
                //*  --- DELETE RECORDS WITH CONTAINER ID = ' '
                //*  --- DELETE RECORDS BELONGING TO THE WFO-CLIENTS-CLEANUP TABLE
                //*     (CWF-MCSS-CALLS.CLIENT-APP-CDE(*) = 'BATCHTDA' OR
                //*    CWF-MCSS-CALLS.T-CLIENT-APP-CDE = 'BATCHTDA'))
                //*  --- DELETE EXPRESS TRACKING RECORDS OR MODIFY CONTAINERS
                //* ***  CWF-MCSS-CALLS.EXPRESS-IND NE ' ')
                //*  EPM 06/06/02 COMMENTED ABOVE AND  CHANGED TO BELOW CONDITION
                //*  BECAUSE EXPRESS-IND CAN BE FOUND ONLY IN W RECORD
                //*  #EXPRESS-CONT GETS THE VALUE EXPRESS-IND OF THE LAST WRECORD PROCESSED
                //*  EPM 08/03/99
                pnd_Whole_Container_Processed.setValue(true);                                                                                                             //Natural: MOVE TRUE TO #WHOLE-CONTAINER-PROCESSED
                //*  EPM 08/03/99
                //*  EPM 04/24/2000
                //*  EPM 01/08/01
                //*  EPM 06/06/02 CHANGED TO THIS COND
                if (condition(cwf_Mcss_Calls_Container_Id.greater(" ") && cwf_Mcss_Calls_Error_Msg_Txt.equals(" ") && ! (pnd_Cleanup.getBoolean()) &&                     //Natural: IF ( CWF-MCSS-CALLS.CONTAINER-ID GT ' ' AND CWF-MCSS-CALLS.ERROR-MSG-TXT = ' ' AND NOT #CLEANUP AND #EXPRESS-CONT = ' ' )
                    pnd_Express_Cont.equals(" ")))
                {
                    //* ****   AND CWF-MCSS-CALLS.EXPRESS-IND = ' ')
                    //*  EPM
                    pnd_Container_Rcrd_Type_Key_Pnd_Container.setValue(cwf_Mcss_Calls_Container_Id);                                                                      //Natural: MOVE CWF-MCSS-CALLS.CONTAINER-ID TO #CONTAINER
                    //*  EPM
                    pnd_Container_Rcrd_Type_Key_Pnd_Rcrd_Type.reset();                                                                                                    //Natural: RESET #RCRD-TYPE
                    //*  EPM 08/03/99
                                                                                                                                                                          //Natural: PERFORM CHECK-WHOLE-CONTAINER
                    sub_Check_Whole_Container();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_AUDIT"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_AUDIT"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  EPM 08/03/99
                }                                                                                                                                                         //Natural: END-IF
                //*  EPM 08/03/99
                if (condition(pnd_Whole_Container_Processed.getBoolean()))                                                                                                //Natural: IF #WHOLE-CONTAINER-PROCESSED
                {
                    //*  EPM 01/01/01
                    //*  EPM 01/08/01
                    //*  EPM 08/03/99
                    pnd_Tot_Delete.nadd(1);                                                                                                                               //Natural: ADD 1 TO #TOT-DELETE
                    getReports().write(1, ReportOption.NOTITLE,cwf_Mcss_Calls_Rcrd_Type_Cde,cwf_Mcss_Calls_Pin_Nbr,cwf_Mcss_Calls_Contact_Id_Cde,cwf_Mcss_Calls_Container_Id,cwf_Mcss_Calls_Rcrd_Upld_Tme,  //Natural: WRITE ( 1 ) CWF-MCSS-CALLS.RCRD-TYPE-CDE CWF-MCSS-CALLS.PIN-NBR CWF-MCSS-CALLS.CONTACT-ID-CDE CWF-MCSS-CALLS.CONTAINER-ID CWF-MCSS-CALLS.RCRD-UPLD-TME ( EM = YYYYMMDDHHIISST ) CWF-MCSS-CALLS.ERROR-MSG-TXT CWF-MCSS-CALLS.EXPRESS-IND #CLIENT-APP-CDE /
                        new ReportEditMask ("YYYYMMDDHHIISST"),cwf_Mcss_Calls_Error_Msg_Txt,cwf_Mcss_Calls_Express_Ind,pnd_Client_App_Cde,NEWLINE);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_AUDIT"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_AUDIT"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    GET1:                                                                                                                                                 //Natural: GET CWF-MCSS-CALLS *ISN ( READ-AUDIT. )
                    vw_cwf_Mcss_Calls.readByID(vw_cwf_Mcss_Calls.getAstISN("READ_AUDIT"), "GET1");
                    //*           DELETE (READ-AUDIT.)   /* EPM 08/03/99
                    //*  EPM 08/03/99
                    vw_cwf_Mcss_Calls.deleteDBRow("GET1");                                                                                                                //Natural: DELETE ( GET1. )
                    //*  EPM 08/03/99
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  ELSE                             /*
            //*    ADD 1 TO #TOT-DELETE           /* OB 09/23/98
            //*    DELETE (READ-AUDIT.)           /*
            //*  END-IF                           /*
            if (condition(pnd_Rec_Cnt.greater(100)))                                                                                                                      //Natural: IF #REC-CNT GT 100
            {
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                pnd_Rec_Cnt.setValue(0);                                                                                                                                  //Natural: ASSIGN #REC-CNT = 0
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"    NUMBER OF RECORDS DELETED : ",pnd_Tot_Delete);                                                    //Natural: WRITE ( 1 ) // '    NUMBER OF RECORDS DELETED : ' #TOT-DELETE
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE," ** UPLOAD CONTROL RECORD DELETE SUCCESSFUL **");                                                     //Natural: WRITE ( 1 ) // ' ** UPLOAD CONTROL RECORD DELETE SUCCESSFUL **'
        if (Global.isEscape()) return;
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-WHOLE-CONTAINER
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-CLIENT-CODE
    }
    private void sub_Check_Whole_Container() throws Exception                                                                                                             //Natural: CHECK-WHOLE-CONTAINER
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        vw_cwf_Mcss_Calls2.startDatabaseRead                                                                                                                              //Natural: READ CWF-MCSS-CALLS2 WITH CONTAINER-RCRD-TYPE-KEY = #CONTAINER-RCRD-TYPE-KEY
        (
        "READ02",
        new Wc[] { new Wc("CONTAINER_RCRD_TYPE_KEY", ">=", pnd_Container_Rcrd_Type_Key, WcType.BY) },
        new Oc[] { new Oc("CONTAINER_RCRD_TYPE_KEY", "ASC") }
        );
        READ02:
        while (condition(vw_cwf_Mcss_Calls2.readNextRow("READ02")))
        {
            if (condition(cwf_Mcss_Calls2_Container_Id.notEquals(pnd_Container_Rcrd_Type_Key_Pnd_Container)))                                                             //Natural: IF CWF-MCSS-CALLS2.CONTAINER-ID NE #CONTAINER
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cwf_Mcss_Calls2_Error_Msg_Txt.notEquals(" ")))                                                                                                  //Natural: IF CWF-MCSS-CALLS2.ERROR-MSG-TXT NE ' '
            {
                pnd_Whole_Container_Processed.setValue(false);                                                                                                            //Natural: MOVE FALSE TO #WHOLE-CONTAINER-PROCESSED
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Check_Client_Code() throws Exception                                                                                                                 //Natural: CHECK-CLIENT-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        pnd_Cleanup.setValue(false);                                                                                                                                      //Natural: MOVE FALSE TO #CLEANUP
        //*  VALUE SHOULD BE NON-BLANK
        pnd_Client_App_Cde.setValue("********");                                                                                                                          //Natural: MOVE '********' TO #CLIENT-APP-CDE
        //*  SINCE #CLEANUP-CLIENTS(*) MAY
        //*  CONTAIN BLANK
        if (condition(cwf_Mcss_Calls_Rcrd_Type_Cde.equals("W")))                                                                                                          //Natural: IF CWF-MCSS-CALLS.RCRD-TYPE-CDE = 'W'
        {
            pnd_Save_Container.setValue(cwf_Mcss_Calls_Container_Id);                                                                                                     //Natural: ASSIGN #SAVE-CONTAINER := CWF-MCSS-CALLS.CONTAINER-ID
            pnd_Save_Client_App_Cde.setValue(cwf_Mcss_Calls_Client_App_Cde.getValue(1));                                                                                  //Natural: ASSIGN #SAVE-CLIENT-APP-CDE := CWF-MCSS-CALLS.CLIENT-APP-CDE ( 1 )
            pnd_Client_App_Cde.setValue(cwf_Mcss_Calls_Client_App_Cde.getValue(1));                                                                                       //Natural: MOVE CWF-MCSS-CALLS.CLIENT-APP-CDE ( 1 ) TO #CLIENT-APP-CDE
            pnd_Express_Cont.setValue(cwf_Mcss_Calls_Express_Ind);                                                                                                        //Natural: ASSIGN #EXPRESS-CONT := CWF-MCSS-CALLS.EXPRESS-IND
            pnd_Mo_Cont.setValue(false);                                                                                                                                  //Natural: ASSIGN #MO-CONT := FALSE
            if (condition(cwf_Mcss_Calls_Action_Cde.getValue("*").equals("AR")))                                                                                          //Natural: IF CWF-MCSS-CALLS.ACTION-CDE ( * ) = 'AR'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Mo_Cont.setValue(true);                                                                                                                               //Natural: ASSIGN #MO-CONT := TRUE
            }                                                                                                                                                             //Natural: END-IF
            //*  END   EPM 06/06/02
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(cwf_Mcss_Calls_Rcrd_Type_Cde.equals("T")))                                                                                                      //Natural: IF CWF-MCSS-CALLS.RCRD-TYPE-CDE = 'T'
            {
                pnd_Client_App_Cde.setValue(cwf_Mcss_Calls_T_Client_App_Cde);                                                                                             //Natural: MOVE CWF-MCSS-CALLS.T-CLIENT-APP-CDE TO #CLIENT-APP-CDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(cwf_Mcss_Calls_Rcrd_Type_Cde.equals("K") || cwf_Mcss_Calls_Rcrd_Type_Cde.equals("O")))                                                      //Natural: IF CWF-MCSS-CALLS.RCRD-TYPE-CDE = 'K' OR = 'O'
                {
                    if (condition(cwf_Mcss_Calls_Container_Id.equals(pnd_Save_Container)))                                                                                //Natural: IF CWF-MCSS-CALLS.CONTAINER-ID = #SAVE-CONTAINER
                    {
                        pnd_Client_App_Cde.setValue(pnd_Save_Client_App_Cde);                                                                                             //Natural: ASSIGN #CLIENT-APP-CDE := #SAVE-CLIENT-APP-CDE
                        //* *********************************************
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Container_Rcrd_Type_Key2_Pnd_Container2.setValue(cwf_Mcss_Calls_Container_Id);                                                                //Natural: MOVE CWF-MCSS-CALLS.CONTAINER-ID TO #CONTAINER-RCRD-TYPE-KEY2.#CONTAINER2
                        pnd_Container_Rcrd_Type_Key2_Pnd_Rcrd_Type2.setValue("W");                                                                                        //Natural: MOVE 'W' TO #CONTAINER-RCRD-TYPE-KEY2.#RCRD-TYPE2
                        vw_cwf_Mcss_Calls3.startDatabaseFind                                                                                                              //Natural: FIND ( 1 ) CWF-MCSS-CALLS3 WITH CONTAINER-RCRD-TYPE-KEY = #CONTAINER-RCRD-TYPE-KEY2
                        (
                        "FIND01",
                        new Wc[] { new Wc("CONTAINER_RCRD_TYPE_KEY", "=", pnd_Container_Rcrd_Type_Key2, WcType.WITH) },
                        1
                        );
                        FIND01:
                        while (condition(vw_cwf_Mcss_Calls3.readNextRow("FIND01", true)))
                        {
                            vw_cwf_Mcss_Calls3.setIfNotFoundControlFlag(false);
                            //*  MEANING THE 'W' RECORD HAS BEEN DELETED,
                            if (condition(vw_cwf_Mcss_Calls3.getAstCOUNTER().equals(0)))                                                                                  //Natural: IF NO RECORDS FOUND
                            {
                                pnd_Cleanup.setValue(true);                                                                                                               //Natural: ASSIGN #CLEANUP := TRUE
                                //*  AND THEREFORE, THE 'K' OR 'O' SHLD ALSO BE DELETED
                                if (condition(true)) break;                                                                                                               //Natural: ESCAPE BOTTOM
                            }                                                                                                                                             //Natural: END-NOREC
                            pnd_Client_App_Cde.setValue(cwf_Mcss_Calls3_Client_App_Cde.getValue(1));                                                                      //Natural: MOVE CWF-MCSS-CALLS3.CLIENT-APP-CDE ( 1 ) TO #CLIENT-APP-CDE
                        }                                                                                                                                                 //Natural: END-FIND
                        if (Global.isEscape()) return;
                    }                                                                                                                                                     //Natural: END-IF
                    //* *********************************************
                    //*  OF CWF-MCSS-CALLS.RCRD-TYPE-CDE = 'K' OR = 'O'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  CHECK IF CLIENT NEEDS TO BE DELETED
        if (condition(! (pnd_Cleanup.getBoolean())))                                                                                                                      //Natural: IF NOT #CLEANUP
        {
            pnd_Idx.reset();                                                                                                                                              //Natural: RESET #IDX
            if (condition(DbsUtil.maskMatches(pnd_Client_App_Cde,"'BATCH'*")))                                                                                            //Natural: IF #CLIENT-APP-CDE = MASK ( 'BATCH'* )
            {
                pnd_Client_App_Cde.setValue(pnd_Client_App_Cde.getSubstring(6));                                                                                          //Natural: ASSIGN #CLIENT-APP-CDE := SUBSTR ( #CLIENT-APP-CDE,6 )
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.examine(new ExamineSource(pnd_Cleanup_Clients.getValue("*"),true), new ExamineSearch(pnd_Client_App_Cde, true), new ExamineGivingIndex(pnd_Idx));     //Natural: EXAMINE FULL #CLEANUP-CLIENTS ( * ) FOR FULL #CLIENT-APP-CDE GIVING INDEX #IDX
            if (condition(pnd_Idx.greater(getZero())))                                                                                                                    //Natural: IF #IDX GT 0
            {
                pnd_Cleanup.setValue(true);                                                                                                                               //Natural: ASSIGN #CLEANUP := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Mcsf1600.class));                                              //Natural: WRITE ( 1 ) NOTITLE NOHDR USING FORM 'MCSF1600'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=60 ZP=OFF");
    }
}
