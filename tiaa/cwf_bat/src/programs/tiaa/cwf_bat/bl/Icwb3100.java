/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:39:20 PM
**        * FROM NATURAL PROGRAM : Icwb3100
************************************************************
**        * FILE NAME            : Icwb3100.java
**        * CLASS NAME           : Icwb3100
**        * INSTANCE NAME        : Icwb3100
************************************************************
************************************************************************
* PROGRAM  : ICWB3100
* SYSTEM   : CWF
* TITLE    : WORK PROCESS ID EXTRACT
* WRITTEN  : AUG 20, 2002
* AUTHOR   : LEON EPSTEIN
* FUNCTION : THIS PROGRAM READS CONTROL RECORD AND NUMBER OF FILES AND
*          : EXTRACTS WPID AND DOCUMENT INFORMATION.
*          : CREATES A FLAT WORK FILE.
*          : AT THE END OF PROCESS CONTROL RECORD WILL BE UPDATED.
* -------- :
* CHANGES  : 02/28/03 DATE & TIME CHANGED TO *TIMX
*          : 02/28/03 NUMBER-OF-PAGES FIELD CHANGED TO NUMERIC
*          : 04/02/03 IF NUMBER OF PAGES NEGATIVE - CHANGE TO ZERO.
*          : 04/02/03 MIT-NO-ZZ-READ ROUTINE HAS BEEN CHANGED TO READ
*          :    FOLDER FILE IF WR IS A SUB-REQUEST.
* 02/23/2017 - SINGAK - PIN EXPANSION - AUG 2017
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Icwb3100 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Image_Xref;
    private DbsField cwf_Image_Xref_Mail_Item_Nbr;

    private DbsGroup cwf_Image_Xref__R_Field_1;
    private DbsField cwf_Image_Xref_Mail_Item_Nbr_A;
    private DbsField cwf_Image_Xref_Image_Document_Address_Cde;

    private DbsGroup cwf_Image_Xref__R_Field_2;
    private DbsField cwf_Image_Xref_Platter;
    private DbsField cwf_Image_Xref_Filenum;
    private DbsField cwf_Image_Xref_Images;

    private DbsGroup cwf_Image_Xref__R_Field_3;
    private DbsField cwf_Image_Xref_Filler_1;
    private DbsField cwf_Image_Xref_Numpages;
    private DbsField cwf_Image_Xref_Filler_2;
    private DbsField cwf_Image_Xref_Ph_Unique_Id_Nbr;
    private DbsField cwf_Image_Xref_Upld_Date_Time;
    private DbsField cwf_Image_Xref_Source_Id;
    private DbsField cwf_Image_Xref_Number_Of_Pages;

    private DataAccessProgramView vw_cwf_Master_Index_View;
    private DbsField cwf_Master_Index_View_Pin_Nbr;
    private DbsField cwf_Master_Index_View_Rqst_Log_Dte_Tme;
    private DbsField cwf_Master_Index_View_Tiaa_Rcvd_Dte;
    private DbsField cwf_Master_Index_View_Case_Id_Cde;
    private DbsGroup cwf_Master_Index_View_Mail_Item_NbrMuGroup;
    private DbsField cwf_Master_Index_View_Mail_Item_Nbr;
    private DbsField cwf_Master_Index_View_Multi_Rqst_Ind;
    private DbsField cwf_Master_Index_View_Work_Prcss_Id;
    private DbsField cwf_Master_Index_View_Actve_Ind;

    private DataAccessProgramView vw_ncw_Master_Index;
    private DbsField ncw_Master_Index_Np_Pin;
    private DbsField ncw_Master_Index_Rqst_Log_Dte_Tme;
    private DbsField ncw_Master_Index_Work_Prcss_Id;
    private DbsField ncw_Master_Index_Actve_Ind;

    private DataAccessProgramView vw_icw_Master_Index;
    private DbsField icw_Master_Index_Rqst_Log_Dte_Tme;
    private DbsField icw_Master_Index_Work_Prcss_Id;
    private DbsField icw_Master_Index_Actve_Ind;

    private DataAccessProgramView vw_cwf_Efm_Folder;
    private DbsField cwf_Efm_Folder_Cabinet_Id;

    private DbsGroup cwf_Efm_Folder__R_Field_4;
    private DbsField cwf_Efm_Folder_Cabinet_Id_Letter;
    private DbsField cwf_Efm_Folder_Cabinet_Id_Pin;

    private DbsGroup cwf_Efm_Folder_Folder_Id;
    private DbsField cwf_Efm_Folder_Tiaa_Rcvd_Dte;
    private DbsField cwf_Efm_Folder_Case_Workprcss_Multi_Subrqst;

    private DbsGroup cwf_Efm_Folder__R_Field_5;
    private DbsField cwf_Efm_Folder_Case_1;
    private DbsField cwf_Efm_Folder_Wpid;
    private DbsField cwf_Efm_Folder_Case_8;
    private DbsField cwf_Efm_Folder_Case_9;
    private DbsField cwf_Efm_Folder_Active_Ind;
    private DbsField cwf_Efm_Folder_Sub_Rqst_Work_Prcss_Id;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_6;
    private DbsField cwf_Support_Tbl_Run_Indicator;
    private DbsField cwf_Support_Tbl_Filler_01;
    private DbsField cwf_Support_Tbl_Normal_Start_Dte_Tme;
    private DbsField cwf_Support_Tbl_Filler_02;
    private DbsField cwf_Support_Tbl_Normal_End_Dte_Tme;
    private DbsField cwf_Support_Tbl_Filler_03;
    private DbsField cwf_Support_Tbl_Normal_Record_Count;
    private DbsField cwf_Support_Tbl_Filler_04;
    private DbsField cwf_Support_Tbl_Special_Start_Dte_Tme;
    private DbsField cwf_Support_Tbl_Filler_05;
    private DbsField cwf_Support_Tbl_Special_End_Dte_Tme;
    private DbsField cwf_Support_Tbl_Filler_06;
    private DbsField cwf_Support_Tbl_Special_Record_Count;
    private DbsField cwf_Support_Tbl_Tbl_Actve_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Entry_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Dte;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Dlte_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Table_Rectype;
    private DbsField cwf_Support_Tbl_Tbl_Table_Access_Level;

    private DataAccessProgramView vw_cwf_Efm_Document;
    private DbsField cwf_Efm_Document_Cabinet_Id;

    private DbsGroup cwf_Efm_Document__R_Field_7;
    private DbsField cwf_Efm_Document_Cabinet_Id_Letter;
    private DbsField cwf_Efm_Document_Pin_Nbr;
    private DbsField cwf_Efm_Document_Tiaa_Rcvd_Dte;
    private DbsField cwf_Efm_Document_Case_Workprcss_Multi_Subrqst;

    private DbsGroup cwf_Efm_Document__R_Field_8;
    private DbsField cwf_Efm_Document_Case_Id_Cde;
    private DbsField cwf_Efm_Document_Work_Prcss_Id;
    private DbsField cwf_Efm_Document_Multi_Subrqst;
    private DbsField cwf_Efm_Document_Sub_Rqst_Ind;
    private DbsField cwf_Efm_Document_Entry_Dte_Tme;
    private DbsField cwf_Efm_Document_Entry_Empl_Racf_Id;
    private DbsField cwf_Efm_Document_Entry_System_Or_Unit;

    private DbsGroup cwf_Efm_Document_Document_Type;
    private DbsField cwf_Efm_Document_Document_Category;
    private DbsField cwf_Efm_Document_Document_Sub_Category;
    private DbsField cwf_Efm_Document_Document_Detail;
    private DbsField cwf_Efm_Document_Copy_Ind;
    private DbsField cwf_Efm_Document_Sub_Rqst_Work_Prcss_Id;
    private DbsField cwf_Efm_Document_Image_Min;
    private DbsField cwf_Efm_Document_Image_Source_Id;
    private DbsField cwf_Efm_Document_Dgtze_Doc_End_Page;

    private DataAccessProgramView vw_icw_Efm_Document;
    private DbsField icw_Efm_Document_Cabinet_Id;
    private DbsField icw_Efm_Document_Rqst_Log_Dte_Tme;
    private DbsField icw_Efm_Document_Crte_Dte_Tme;
    private DbsField icw_Efm_Document_Crte_Empl_Racf_Id;
    private DbsField icw_Efm_Document_Crte_Unit;

    private DbsGroup icw_Efm_Document_Dcmnt_Type;
    private DbsField icw_Efm_Document_Dcmnt_Ctgry;
    private DbsField icw_Efm_Document_Dcmnt_Sub_Ctgry;
    private DbsField icw_Efm_Document_Dcmnt_Dtl;
    private DbsField icw_Efm_Document_Image_Source_Id;
    private DbsField icw_Efm_Document_Image_Min;
    private DbsField icw_Efm_Document_Dcmnt_Copy_Ind;
    private DbsField icw_Efm_Document_Image_End_Page;

    private DataAccessProgramView vw_ncw_Efm_Document;
    private DbsField ncw_Efm_Document_Np_Pin;
    private DbsField ncw_Efm_Document_Rqst_Log_Dte_Tme;
    private DbsField ncw_Efm_Document_Crte_Dte_Tme;
    private DbsField ncw_Efm_Document_Crte_Empl_Racf_Id;
    private DbsField ncw_Efm_Document_Crte_Unit;

    private DbsGroup ncw_Efm_Document_Dcmnt_Type;
    private DbsField ncw_Efm_Document_Dcmnt_Ctgry;
    private DbsField ncw_Efm_Document_Dcmnt_Sub_Ctgry;
    private DbsField ncw_Efm_Document_Dcmnt_Dtl;
    private DbsField ncw_Efm_Document_Image_Source_Id;
    private DbsField ncw_Efm_Document_Dcmnt_Copy_Ind;
    private DbsField ncw_Efm_Document_Image_Min;
    private DbsField ncw_Efm_Document_Image_End_Page;

    private DataAccessProgramView vw_cwf_Org_Empl_Tbl;
    private DbsField cwf_Org_Empl_Tbl_Empl_Racf_Id;
    private DbsField cwf_Org_Empl_Tbl_Empl_Info;
    private DbsField cwf_Org_Empl_Tbl_Unit_Empl_Racf_Id_Key;

    private DataAccessProgramView vw_cwf_Efm_Cabinet;
    private DbsField cwf_Efm_Cabinet_Cabinet_Id;

    private DbsGroup cwf_Efm_Cabinet__R_Field_9;
    private DbsField cwf_Efm_Cabinet_Cabinet_Id_Letter;
    private DbsField cwf_Efm_Cabinet_Cabinet_Id_Pin;
    private DbsField cwf_Efm_Cabinet_Mit_Rqst_Log_Dte_Tme;
    private DbsField cwf_Efm_Cabinet_Dgtze_Rqst_Log_Dte_Tme;
    private DbsField cwf_Efm_Cabinet_Media_Ind;
    private DbsField cwf_Efm_Cabinet_Media_Updte_Dte_Tme;

    private DbsGroup work_File_1;
    private DbsField work_File_1_Wf_Cabinet_Id;
    private DbsField work_File_1_Wf_Crte_Dte_Tme;
    private DbsField work_File_1_Wf_Crte_Empl_Racf_Id;
    private DbsField work_File_1_Wf_Dcmnt_Copy_Ind;
    private DbsField work_File_1_Wf_Dcmnt_Type;

    private DbsGroup work_File_1__R_Field_10;
    private DbsField work_File_1_Wf_Dcmnt_Category;
    private DbsField work_File_1_Wf_Dcmnt_Sub_Category;
    private DbsField work_File_1_Wf_Dcmnt_Detail;
    private DbsField work_File_1_Wf_Dcmnt_File_Flag;
    private DbsField work_File_1_Wf_Mail_Item_Nbr;
    private DbsField work_File_1_Wf_Mj_Rqst_Log_Dte_Tme;
    private DbsField work_File_1_Wf_Mj_Work_Prcss_Id;
    private DbsField work_File_1_Wf_Nbr_Of_Pages;
    private DbsField work_File_1_Wf_Rqst_Work_Prcss_Id;
    private DbsField work_File_1_Wf_Site;
    private DbsField work_File_1_Wf_Source_Id;
    private DbsField work_File_1_Wf_Upld_Date_Time;

    private DbsGroup report_Line;
    private DbsField report_Line_Wr_System;
    private DbsField report_Line_Wr_Cabinet_Id;
    private DbsField report_Line_Wr_Crte_Dte_Tme;
    private DbsField report_Line_Wr_Crte_Empl_Racf_Id;
    private DbsField report_Line_Wr_Dcmnt_Copy_Ind;
    private DbsField report_Line_Wr_Dcmnt_Type;
    private DbsField report_Line_Wr_Dcmnt_File_Flag;
    private DbsField report_Line_Wr_Mail_Item_Nbr;
    private DbsField report_Line_Wr_Mj_Rqst_Log_Dte_Tme;
    private DbsField report_Line_Wr_Mj_Work_Prcss_Id;
    private DbsField report_Line_Wr_Nbr_Of_Pages;
    private DbsField report_Line_Wr_Rqst_Work_Prcss_Id;
    private DbsField report_Line_Wr_Site;
    private DbsField report_Line_Wr_Source_Id;
    private DbsField report_Line_Wr_Upld_Date_Time;
    private DbsField report_Line_Wr_Rec_End;
    private DbsField pnd_Reported_Case_Rldt;
    private DbsField pnd_Compare_Value;

    private DbsGroup pnd_Compare_Value__R_Field_11;
    private DbsField pnd_Compare_Value_Pnd_Compare_Value_Min;
    private DbsField pnd_Compare_Value_Pnd_Compare_Value_Wpid;
    private DbsField pnd_Compare_Value_Pnd_Compare_Value_Multi;
    private DbsField pnd_Compare_Value_Pnd_Compare_Value_Rldt;
    private DbsField pnd_Compare_Value_Pnd_Compare_Doc_Cat;
    private DbsField pnd_Compare_Value_Pnd_Compare_Doc_Sub_Cat;
    private DbsField pnd_Compare_Value_Pnd_Compare_Doc_Detail;
    private DbsField pnd_Rep_Post_Case_Rldt;
    private DbsField pnd_Com_Post_Value;

    private DbsGroup pnd_Com_Post_Value__R_Field_12;
    private DbsField pnd_Com_Post_Value_Pnd_Post_Value_Wpid;
    private DbsField pnd_Com_Post_Value_Pnd_Post_Value_Multi;
    private DbsField pnd_Com_Post_Value_Pnd_Post_Doc_Cat;
    private DbsField pnd_Com_Post_Value_Pnd_Post_Doc_Sub_Cat;
    private DbsField pnd_Com_Post_Value_Pnd_Post_Doc_Detail;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_13;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind;
    private DbsField pnd_Common_Key;

    private DbsGroup pnd_Common_Key__R_Field_14;
    private DbsField pnd_Common_Key_Pnd_Common_Image_Source_Id;
    private DbsField pnd_Common_Key_Pnd_Common_Image_Min;
    private DbsField pnd_Unit_Empl_Racf_Id_Key;

    private DbsGroup pnd_Unit_Empl_Racf_Id_Key__R_Field_15;
    private DbsField pnd_Unit_Empl_Racf_Id_Key_Pnd_Empl_Unit_Cde;
    private DbsField pnd_Unit_Empl_Racf_Id_Key_Pnd_Empl_Racf_Id;
    private DbsField pnd_Unit_Empl_Racf_Id_Key_Pnd_Actve_Ind;
    private DbsField pnd_Active_Cabinet_Key;

    private DbsGroup pnd_Active_Cabinet_Key__R_Field_16;
    private DbsField pnd_Active_Cabinet_Key_Pnd_Cabinet_Id;
    private DbsField pnd_Active_Cabinet_Key_Pnd_Actve_Fldrs_Ind;
    private DbsField pnd_Folder_Key;

    private DbsGroup pnd_Folder_Key__R_Field_17;
    private DbsField pnd_Folder_Key_Pnd_Cabinet_Id;
    private DbsField pnd_Folder_Key_Pnd_Tiaa_Rcvd_Dte;
    private DbsField pnd_Folder_Key_Pnd_Case_Workprcss_Multi_Subrqst;
    private DbsField pnd_Folder_Name_Key;

    private DbsGroup pnd_Folder_Name_Key__R_Field_18;
    private DbsField pnd_Folder_Name_Key_Pnd_Rqst_Id;

    private DbsGroup pnd_Folder_Name_Key__R_Field_19;
    private DbsField pnd_Folder_Name_Key_Pnd_Wpid;
    private DbsField pnd_Folder_Name_Key_Pnd_Tiaa_Rcvd_Dte;
    private DbsField pnd_Folder_Name_Key_Pnd_Case_Id_1;
    private DbsField pnd_Folder_Name_Key_Pnd_Case_Id_2;
    private DbsField pnd_Folder_Name_Key_Pnd_Pin_Nbr;
    private DbsField pnd_Case_Id_Key;

    private DbsGroup pnd_Case_Id_Key__R_Field_20;
    private DbsField pnd_Case_Id_Key_Pnd_Pin_Nbr;
    private DbsField pnd_Case_Id_Key_Pnd_Tiaa_Rcvd_Dte;
    private DbsField pnd_Case_Id_Key_Pnd_Case_Id_Cde;
    private DbsField pnd_Case_Id_Key_Pnd_Actve_Ind;
    private DbsField pnd_Actv_Unque_Key;

    private DbsGroup pnd_Actv_Unque_Key__R_Field_21;
    private DbsField pnd_Actv_Unque_Key_Pnd_Rqst_Log_Dte_Tme;
    private DbsField pnd_Actv_Unque_Key_Pnd_Actve_Ind;
    private DbsField pnd_Rqst_Id_Key;

    private DbsGroup pnd_Rqst_Id_Key__R_Field_22;
    private DbsField pnd_Rqst_Id_Key_Pnd_Rqst_Id;

    private DbsGroup pnd_Rqst_Id_Key__R_Field_23;
    private DbsField pnd_Rqst_Id_Key_Pnd_Rqst_Wpid;
    private DbsField pnd_Rqst_Id_Key_Pnd_Rqst_Rcvd_Dte;
    private DbsField pnd_Rqst_Id_Key_Pnd_Rqst_Group_9;

    private DbsGroup pnd_Rqst_Id_Key__R_Field_24;
    private DbsField pnd_Rqst_Id_Key_Pnd_Rqst_Case_1;
    private DbsField pnd_Rqst_Id_Key_Pnd_Rqst_Case_2;
    private DbsField pnd_Rqst_Id_Key_Pnd_Rqst_Pin_Nbr;
    private DbsField pnd_Rqst_Id_Key_Pnd_Multi_Rqst_Ind;
    private DbsField pnd_Rqst_Id_Key_Pnd_Actve_Ind;
    private DbsField pnd_Pin_History_Key;

    private DbsGroup pnd_Pin_History_Key__R_Field_25;
    private DbsField pnd_Pin_History_Key_Pnd_Pin_Nbr;
    private DbsField pnd_Pin_History_Key_Pnd_Crprte_Status_Ind;
    private DbsField pnd_Pin_History_Key_Pnd_Rqst_Invrt_Rcvd_Dte_Tme;
    private DbsField pnd_Pin_History_Key_Pnd_Work_Prcss_Id;
    private DbsField pnd_Pin_History_Key_Pnd_Actve_Ind;
    private DbsField pnd_Min_Key;

    private DbsGroup pnd_Min_Key__R_Field_26;
    private DbsField pnd_Min_Key_Pnd_Min_Value;
    private DbsField pnd_Min_Key_Pnd_Actve_Ind;
    private DbsField pnd_Running_Dte_Tme_Str;

    private DbsGroup pnd_Running_Dte_Tme_Str__R_Field_27;
    private DbsField pnd_Running_Dte_Tme_Str_Pnd_Running_Date;
    private DbsField pnd_Running_Dte_Tme_Str_Pnd_Running_Time;

    private DbsGroup pnd_Running_Dte_Tme_Str__R_Field_28;
    private DbsField pnd_Running_Dte_Tme_Str_Pnd_Running_Dte_Tme_Sn;
    private DbsField pnd_Running_Dte_Tme_End;

    private DbsGroup pnd_Running_Dte_Tme_End__R_Field_29;
    private DbsField pnd_Running_Dte_Tme_End_Pnd_Running_Date_End;
    private DbsField pnd_Running_Dte_Tme_End_Pnd_Running_Time_End;

    private DbsGroup pnd_Running_Dte_Tme_End__R_Field_30;
    private DbsField pnd_Running_Dte_Tme_End_Pnd_Running_Dte_Tme_En;
    private DbsField pnd_Isn_Control;
    private DbsField pnd_Pin_Nbr_Save;
    private DbsField pnd_Wf_Rec_Cntr;
    private DbsField pnd_Wpid_Split;

    private DbsGroup pnd_Wpid_Split__R_Field_31;
    private DbsField pnd_Wpid_Split_Pnd_Wpid_Split_2;
    private DbsField pnd_Mit_Read;
    private DbsField pnd_Number_Of_Pages;
    private DbsField pnd_Min_Save;
    private DbsField pnd_Isn_Save;
    private DbsField pnd_Numpages_Save;
    private DbsField pnd_Date_Time_A;

    private DbsGroup pnd_Date_Time_A__R_Field_32;
    private DbsField pnd_Date_Time_A_Pnd_Date_Time_N;
    private DbsField pnd_Date_T;
    private DbsField pnd_Date_T_Result;
    private DbsField pnd_Date_D;
    private DbsField pnd_Curr_Date_N;

    private DbsGroup pnd_Curr_Date_N__R_Field_33;
    private DbsField pnd_Curr_Date_N_Pnd_Curr_Date_A;
    private DbsField pnd_Mail_Item_Nbr_Save;

    private DbsGroup pnd_Mail_Item_Nbr_Save__R_Field_34;
    private DbsField pnd_Mail_Item_Nbr_Save_Pnd_Mail_Item_Nbr_Save_A;
    private DbsField pnd_Upld_Date_Time_Save;
    private DbsField pnd_Source_Id_Save;
    private DbsField pnd_Message;
    private DbsField pnd_Control_Flag;
    private DbsField pnd_Key_Compare;

    private DbsGroup pnd_Key_Compare__R_Field_35;
    private DbsField pnd_Key_Compare_Pnd_Key_Compare_Source_Id;
    private DbsField pnd_Key_Compare_Pnd_Key_Compare_Min;
    private DbsField pnd_Min_Split;

    private DbsGroup pnd_Min_Split__R_Field_36;
    private DbsField pnd_Min_Split_Pnd_Filler_Min_11;
    private DbsField pnd_Min_Split_Pnd_Min_Split_3;
    private DbsField pnd_Start_T;
    private DbsField pnd_End_T;
    private DbsField pnd_Image_Cntr;
    private DbsField pnd_Site_Split;

    private DbsGroup pnd_Site_Split__R_Field_37;
    private DbsField pnd_Site_Split_Pnd_Site_Filler;
    private DbsField pnd_Site_Split_Pnd_Site_Split_38;
    private DbsField pnd_Doc_Fnd;
    private DbsField pnd_Doc_Nfnd_Cntr;
    private DbsField pnd_Multi_Subrqst;
    private DbsField pnd_Doc_Case;

    private DbsGroup pnd_Doc_Case__R_Field_38;
    private DbsField pnd_Doc_Case_Pnd_Doc_Case_1;
    private DbsField pnd_Doc_Case_Pnd_Doc_Case_2;
    private DbsField pnd_I;
    private DbsField pnd_C;
    private DbsField pnd_Mit_Write_Flag;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Image_Xref = new DataAccessProgramView(new NameInfo("vw_cwf_Image_Xref", "CWF-IMAGE-XREF"), "CWF_IMAGE_XREF", "CWF_IMAGE_XREF");
        cwf_Image_Xref_Mail_Item_Nbr = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Mail_Item_Nbr", "MAIL-ITEM-NBR", FieldType.NUMERIC, 
            11, RepeatingFieldStrategy.None, "MAIL_ITEM_NBR");
        cwf_Image_Xref_Mail_Item_Nbr.setDdmHeader("MAIL ITEM/NUMBER");

        cwf_Image_Xref__R_Field_1 = vw_cwf_Image_Xref.getRecord().newGroupInGroup("cwf_Image_Xref__R_Field_1", "REDEFINE", cwf_Image_Xref_Mail_Item_Nbr);
        cwf_Image_Xref_Mail_Item_Nbr_A = cwf_Image_Xref__R_Field_1.newFieldInGroup("cwf_Image_Xref_Mail_Item_Nbr_A", "MAIL-ITEM-NBR-A", FieldType.STRING, 
            11);
        cwf_Image_Xref_Image_Document_Address_Cde = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Image_Document_Address_Cde", "IMAGE-DOCUMENT-ADDRESS-CDE", 
            FieldType.STRING, 22, RepeatingFieldStrategy.None, "IMAGE_DOCUMENT_ADDRESS_CDE");
        cwf_Image_Xref_Image_Document_Address_Cde.setDdmHeader("IMNET DOC/ADDRESS");

        cwf_Image_Xref__R_Field_2 = vw_cwf_Image_Xref.getRecord().newGroupInGroup("cwf_Image_Xref__R_Field_2", "REDEFINE", cwf_Image_Xref_Image_Document_Address_Cde);
        cwf_Image_Xref_Platter = cwf_Image_Xref__R_Field_2.newFieldInGroup("cwf_Image_Xref_Platter", "PLATTER", FieldType.STRING, 10);
        cwf_Image_Xref_Filenum = cwf_Image_Xref__R_Field_2.newFieldInGroup("cwf_Image_Xref_Filenum", "FILENUM", FieldType.STRING, 6);
        cwf_Image_Xref_Images = cwf_Image_Xref__R_Field_2.newFieldInGroup("cwf_Image_Xref_Images", "IMAGES", FieldType.STRING, 5);

        cwf_Image_Xref__R_Field_3 = cwf_Image_Xref__R_Field_2.newGroupInGroup("cwf_Image_Xref__R_Field_3", "REDEFINE", cwf_Image_Xref_Images);
        cwf_Image_Xref_Filler_1 = cwf_Image_Xref__R_Field_3.newFieldInGroup("cwf_Image_Xref_Filler_1", "FILLER-1", FieldType.STRING, 2);
        cwf_Image_Xref_Numpages = cwf_Image_Xref__R_Field_3.newFieldInGroup("cwf_Image_Xref_Numpages", "NUMPAGES", FieldType.NUMERIC, 3);
        cwf_Image_Xref_Filler_2 = cwf_Image_Xref__R_Field_2.newFieldInGroup("cwf_Image_Xref_Filler_2", "FILLER-2", FieldType.STRING, 1);
        cwf_Image_Xref_Ph_Unique_Id_Nbr = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Ph_Unique_Id_Nbr", "PH-UNIQUE-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "PH_UNIQUE_ID_NBR");
        cwf_Image_Xref_Ph_Unique_Id_Nbr.setDdmHeader("PIN");
        cwf_Image_Xref_Upld_Date_Time = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Upld_Date_Time", "UPLD-DATE-TIME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "UPLD_DATE_TIME");
        cwf_Image_Xref_Upld_Date_Time.setDdmHeader("UPLOAD/DATE TIME");
        cwf_Image_Xref_Source_Id = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Source_Id", "SOURCE-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "SOURCE_ID");
        cwf_Image_Xref_Number_Of_Pages = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Number_Of_Pages", "NUMBER-OF-PAGES", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "NUMBER_OF_PAGES");
        cwf_Image_Xref_Number_Of_Pages.setDdmHeader("NBR OF/PAGES");
        registerRecord(vw_cwf_Image_Xref);

        vw_cwf_Master_Index_View = new DataAccessProgramView(new NameInfo("vw_cwf_Master_Index_View", "CWF-MASTER-INDEX-VIEW"), "CWF_MASTER_INDEX_VIEW", 
            "CWF_MASTER_INDEX");
        cwf_Master_Index_View_Pin_Nbr = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "PIN_NBR");
        cwf_Master_Index_View_Pin_Nbr.setDdmHeader("PIN");
        cwf_Master_Index_View_Rqst_Log_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Master_Index_View_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        cwf_Master_Index_View_Tiaa_Rcvd_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_RCVD_DTE");
        cwf_Master_Index_View_Case_Id_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Case_Id_Cde", "CASE-ID-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CASE_ID_CDE");
        cwf_Master_Index_View_Case_Id_Cde.setDdmHeader("CASE/ID");
        cwf_Master_Index_View_Mail_Item_NbrMuGroup = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("CWF_MASTER_INDEX_VIEW_MAIL_ITEM_NBRMuGroup", 
            "MAIL_ITEM_NBRMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "CWF_MASTER_INDEX_MAIL_ITEM_NBR");
        cwf_Master_Index_View_Mail_Item_Nbr = cwf_Master_Index_View_Mail_Item_NbrMuGroup.newFieldArrayInGroup("cwf_Master_Index_View_Mail_Item_Nbr", "MAIL-ITEM-NBR", 
            FieldType.STRING, 11, new DbsArrayController(1, 10), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "MAIL_ITEM_NBR");
        cwf_Master_Index_View_Mail_Item_Nbr.setDdmHeader("MIN");
        cwf_Master_Index_View_Multi_Rqst_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Multi_Rqst_Ind", "MULTI-RQST-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "MULTI_RQST_IND");
        cwf_Master_Index_View_Multi_Rqst_Ind.setDdmHeader("MULTI/IND");
        cwf_Master_Index_View_Work_Prcss_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Work_Prcss_Id", "WORK-PRCSS-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        cwf_Master_Index_View_Work_Prcss_Id.setDdmHeader("WORK/ID");
        cwf_Master_Index_View_Actve_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Actve_Ind", "ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ACTVE_IND");
        registerRecord(vw_cwf_Master_Index_View);

        vw_ncw_Master_Index = new DataAccessProgramView(new NameInfo("vw_ncw_Master_Index", "NCW-MASTER-INDEX"), "NCW_MASTER_INDEX", "NCW_MASTER_INDEX");
        ncw_Master_Index_Np_Pin = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Np_Pin", "NP-PIN", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "NP_PIN");
        ncw_Master_Index_Rqst_Log_Dte_Tme = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        ncw_Master_Index_Work_Prcss_Id = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        ncw_Master_Index_Work_Prcss_Id.setDdmHeader("WORK/ID");
        ncw_Master_Index_Actve_Ind = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTVE_IND");
        registerRecord(vw_ncw_Master_Index);

        vw_icw_Master_Index = new DataAccessProgramView(new NameInfo("vw_icw_Master_Index", "ICW-MASTER-INDEX"), "ICW_MASTER_INDEX", "ICW_MASTER_INDEX");
        icw_Master_Index_Rqst_Log_Dte_Tme = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        icw_Master_Index_Work_Prcss_Id = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        icw_Master_Index_Work_Prcss_Id.setDdmHeader("WORK/ID");
        icw_Master_Index_Actve_Ind = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTVE_IND");
        registerRecord(vw_icw_Master_Index);

        vw_cwf_Efm_Folder = new DataAccessProgramView(new NameInfo("vw_cwf_Efm_Folder", "CWF-EFM-FOLDER"), "CWF_EFM_FOLDER", "CWF_EFM_FOLDER");
        cwf_Efm_Folder_Cabinet_Id = vw_cwf_Efm_Folder.getRecord().newFieldInGroup("cwf_Efm_Folder_Cabinet_Id", "CABINET-ID", FieldType.STRING, 14, RepeatingFieldStrategy.None, 
            "CABINET_ID");
        cwf_Efm_Folder_Cabinet_Id.setDdmHeader("CABINET/ID");

        cwf_Efm_Folder__R_Field_4 = vw_cwf_Efm_Folder.getRecord().newGroupInGroup("cwf_Efm_Folder__R_Field_4", "REDEFINE", cwf_Efm_Folder_Cabinet_Id);
        cwf_Efm_Folder_Cabinet_Id_Letter = cwf_Efm_Folder__R_Field_4.newFieldInGroup("cwf_Efm_Folder_Cabinet_Id_Letter", "CABINET-ID-LETTER", FieldType.STRING, 
            1);
        cwf_Efm_Folder_Cabinet_Id_Pin = cwf_Efm_Folder__R_Field_4.newFieldInGroup("cwf_Efm_Folder_Cabinet_Id_Pin", "CABINET-ID-PIN", FieldType.NUMERIC, 
            12);

        cwf_Efm_Folder_Folder_Id = vw_cwf_Efm_Folder.getRecord().newGroupInGroup("CWF_EFM_FOLDER_FOLDER_ID", "FOLDER-ID");
        cwf_Efm_Folder_Tiaa_Rcvd_Dte = cwf_Efm_Folder_Folder_Id.newFieldInGroup("cwf_Efm_Folder_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIAA_RCVD_DTE");
        cwf_Efm_Folder_Tiaa_Rcvd_Dte.setDdmHeader("TIAA RECVD/DATE");
        cwf_Efm_Folder_Case_Workprcss_Multi_Subrqst = cwf_Efm_Folder_Folder_Id.newFieldInGroup("cwf_Efm_Folder_Case_Workprcss_Multi_Subrqst", "CASE-WORKPRCSS-MULTI-SUBRQST", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CASE_WORKPRCSS_MULTI_SUBRQST");
        cwf_Efm_Folder_Case_Workprcss_Multi_Subrqst.setDdmHeader("CASE/WPID/MULTI/SUB-REQ");

        cwf_Efm_Folder__R_Field_5 = cwf_Efm_Folder_Folder_Id.newGroupInGroup("cwf_Efm_Folder__R_Field_5", "REDEFINE", cwf_Efm_Folder_Case_Workprcss_Multi_Subrqst);
        cwf_Efm_Folder_Case_1 = cwf_Efm_Folder__R_Field_5.newFieldInGroup("cwf_Efm_Folder_Case_1", "CASE-1", FieldType.STRING, 1);
        cwf_Efm_Folder_Wpid = cwf_Efm_Folder__R_Field_5.newFieldInGroup("cwf_Efm_Folder_Wpid", "WPID", FieldType.STRING, 6);
        cwf_Efm_Folder_Case_8 = cwf_Efm_Folder__R_Field_5.newFieldInGroup("cwf_Efm_Folder_Case_8", "CASE-8", FieldType.STRING, 1);
        cwf_Efm_Folder_Case_9 = cwf_Efm_Folder__R_Field_5.newFieldInGroup("cwf_Efm_Folder_Case_9", "CASE-9", FieldType.STRING, 1);
        cwf_Efm_Folder_Active_Ind = vw_cwf_Efm_Folder.getRecord().newFieldInGroup("cwf_Efm_Folder_Active_Ind", "ACTIVE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ACTIVE_IND");
        cwf_Efm_Folder_Active_Ind.setDdmHeader("ACTV/IND");
        cwf_Efm_Folder_Sub_Rqst_Work_Prcss_Id = vw_cwf_Efm_Folder.getRecord().newFieldInGroup("cwf_Efm_Folder_Sub_Rqst_Work_Prcss_Id", "SUB-RQST-WORK-PRCSS-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "SUB_RQST_WORK_PRCSS_ID");
        cwf_Efm_Folder_Sub_Rqst_Work_Prcss_Id.setDdmHeader("SUB-RQST WORK/PROCESS ID");
        registerRecord(vw_cwf_Efm_Folder);

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Support_Tbl__R_Field_6 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_6", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Run_Indicator = cwf_Support_Tbl__R_Field_6.newFieldInGroup("cwf_Support_Tbl_Run_Indicator", "RUN-INDICATOR", FieldType.STRING, 
            1);
        cwf_Support_Tbl_Filler_01 = cwf_Support_Tbl__R_Field_6.newFieldInGroup("cwf_Support_Tbl_Filler_01", "FILLER-01", FieldType.STRING, 8);
        cwf_Support_Tbl_Normal_Start_Dte_Tme = cwf_Support_Tbl__R_Field_6.newFieldInGroup("cwf_Support_Tbl_Normal_Start_Dte_Tme", "NORMAL-START-DTE-TME", 
            FieldType.NUMERIC, 15);
        cwf_Support_Tbl_Filler_02 = cwf_Support_Tbl__R_Field_6.newFieldInGroup("cwf_Support_Tbl_Filler_02", "FILLER-02", FieldType.STRING, 5);
        cwf_Support_Tbl_Normal_End_Dte_Tme = cwf_Support_Tbl__R_Field_6.newFieldInGroup("cwf_Support_Tbl_Normal_End_Dte_Tme", "NORMAL-END-DTE-TME", FieldType.NUMERIC, 
            15);
        cwf_Support_Tbl_Filler_03 = cwf_Support_Tbl__R_Field_6.newFieldInGroup("cwf_Support_Tbl_Filler_03", "FILLER-03", FieldType.STRING, 5);
        cwf_Support_Tbl_Normal_Record_Count = cwf_Support_Tbl__R_Field_6.newFieldInGroup("cwf_Support_Tbl_Normal_Record_Count", "NORMAL-RECORD-COUNT", 
            FieldType.NUMERIC, 11);
        cwf_Support_Tbl_Filler_04 = cwf_Support_Tbl__R_Field_6.newFieldInGroup("cwf_Support_Tbl_Filler_04", "FILLER-04", FieldType.STRING, 12);
        cwf_Support_Tbl_Special_Start_Dte_Tme = cwf_Support_Tbl__R_Field_6.newFieldInGroup("cwf_Support_Tbl_Special_Start_Dte_Tme", "SPECIAL-START-DTE-TME", 
            FieldType.NUMERIC, 15);
        cwf_Support_Tbl_Filler_05 = cwf_Support_Tbl__R_Field_6.newFieldInGroup("cwf_Support_Tbl_Filler_05", "FILLER-05", FieldType.STRING, 5);
        cwf_Support_Tbl_Special_End_Dte_Tme = cwf_Support_Tbl__R_Field_6.newFieldInGroup("cwf_Support_Tbl_Special_End_Dte_Tme", "SPECIAL-END-DTE-TME", 
            FieldType.NUMERIC, 15);
        cwf_Support_Tbl_Filler_06 = cwf_Support_Tbl__R_Field_6.newFieldInGroup("cwf_Support_Tbl_Filler_06", "FILLER-06", FieldType.STRING, 5);
        cwf_Support_Tbl_Special_Record_Count = cwf_Support_Tbl__R_Field_6.newFieldInGroup("cwf_Support_Tbl_Special_Record_Count", "SPECIAL-RECORD-COUNT", 
            FieldType.NUMERIC, 11);
        cwf_Support_Tbl_Tbl_Actve_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Actve_Ind", "TBL-ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TBL_ACTVE_IND");
        cwf_Support_Tbl_Tbl_Entry_Dte_Tme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Entry_Dte_Tme", "TBL-ENTRY-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_ENTRY_DTE_TME");
        cwf_Support_Tbl_Tbl_Entry_Dte_Tme.setDdmHeader("ENTRY DATE/TIME");
        cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde", "TBL-ENTRY-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_ENTRY_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Support_Tbl_Tbl_Updte_Dte_Tme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Dte_Tme", "TBL-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE_TME");
        cwf_Support_Tbl_Tbl_Updte_Dte_Tme.setDdmHeader("UPDATE DATE/TIME");
        cwf_Support_Tbl_Tbl_Updte_Dte = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Dte", "TBL-UPDTE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE");
        cwf_Support_Tbl_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        cwf_Support_Tbl_Tbl_Dlte_Dte_Tme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Dlte_Dte_Tme", "TBL-DLTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_DLTE_DTE_TME");
        cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde", "TBL-DLTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_DLTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Table_Rectype = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Rectype", "TBL-TABLE-RECTYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TBL_TABLE_RECTYPE");
        cwf_Support_Tbl_Tbl_Table_Access_Level = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Access_Level", "TBL-TABLE-ACCESS-LEVEL", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TBL_TABLE_ACCESS_LEVEL");
        cwf_Support_Tbl_Tbl_Table_Access_Level.setDdmHeader("ACCESS/LEVEL");
        registerRecord(vw_cwf_Support_Tbl);

        vw_cwf_Efm_Document = new DataAccessProgramView(new NameInfo("vw_cwf_Efm_Document", "CWF-EFM-DOCUMENT"), "CWF_EFM_DOCUMENT", "CWF_EFM_DOCUMENT");
        cwf_Efm_Document_Cabinet_Id = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Cabinet_Id", "CABINET-ID", FieldType.STRING, 14, 
            RepeatingFieldStrategy.None, "CABINET_ID");
        cwf_Efm_Document_Cabinet_Id.setDdmHeader("CABINET/ID");

        cwf_Efm_Document__R_Field_7 = vw_cwf_Efm_Document.getRecord().newGroupInGroup("cwf_Efm_Document__R_Field_7", "REDEFINE", cwf_Efm_Document_Cabinet_Id);
        cwf_Efm_Document_Cabinet_Id_Letter = cwf_Efm_Document__R_Field_7.newFieldInGroup("cwf_Efm_Document_Cabinet_Id_Letter", "CABINET-ID-LETTER", FieldType.STRING, 
            1);
        cwf_Efm_Document_Pin_Nbr = cwf_Efm_Document__R_Field_7.newFieldInGroup("cwf_Efm_Document_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12);
        cwf_Efm_Document_Tiaa_Rcvd_Dte = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TIAA_RCVD_DTE");
        cwf_Efm_Document_Tiaa_Rcvd_Dte.setDdmHeader("TIAA RECVD/DATE");
        cwf_Efm_Document_Case_Workprcss_Multi_Subrqst = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Case_Workprcss_Multi_Subrqst", 
            "CASE-WORKPRCSS-MULTI-SUBRQST", FieldType.STRING, 9, RepeatingFieldStrategy.None, "CASE_WORKPRCSS_MULTI_SUBRQST");
        cwf_Efm_Document_Case_Workprcss_Multi_Subrqst.setDdmHeader("CASE/WPID/MULTI/SUB-REQ");

        cwf_Efm_Document__R_Field_8 = vw_cwf_Efm_Document.getRecord().newGroupInGroup("cwf_Efm_Document__R_Field_8", "REDEFINE", cwf_Efm_Document_Case_Workprcss_Multi_Subrqst);
        cwf_Efm_Document_Case_Id_Cde = cwf_Efm_Document__R_Field_8.newFieldInGroup("cwf_Efm_Document_Case_Id_Cde", "CASE-ID-CDE", FieldType.STRING, 1);
        cwf_Efm_Document_Work_Prcss_Id = cwf_Efm_Document__R_Field_8.newFieldInGroup("cwf_Efm_Document_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 
            6);
        cwf_Efm_Document_Multi_Subrqst = cwf_Efm_Document__R_Field_8.newFieldInGroup("cwf_Efm_Document_Multi_Subrqst", "MULTI-SUBRQST", FieldType.STRING, 
            1);
        cwf_Efm_Document_Sub_Rqst_Ind = cwf_Efm_Document__R_Field_8.newFieldInGroup("cwf_Efm_Document_Sub_Rqst_Ind", "SUB-RQST-IND", FieldType.STRING, 
            1);
        cwf_Efm_Document_Entry_Dte_Tme = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Entry_Dte_Tme", "ENTRY-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ENTRY_DTE_TME");
        cwf_Efm_Document_Entry_Dte_Tme.setDdmHeader("ENTRY/DATE AND TIME");
        cwf_Efm_Document_Entry_Empl_Racf_Id = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Entry_Empl_Racf_Id", "ENTRY-EMPL-RACF-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ENTRY_EMPL_RACF_ID");
        cwf_Efm_Document_Entry_Empl_Racf_Id.setDdmHeader("CREATED BY/RACF ID");
        cwf_Efm_Document_Entry_System_Or_Unit = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Entry_System_Or_Unit", "ENTRY-SYSTEM-OR-UNIT", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ENTRY_SYSTEM_OR_UNIT");
        cwf_Efm_Document_Entry_System_Or_Unit.setDdmHeader("CREATED BY/SYSTEM OR UNIT");

        cwf_Efm_Document_Document_Type = vw_cwf_Efm_Document.getRecord().newGroupInGroup("CWF_EFM_DOCUMENT_DOCUMENT_TYPE", "DOCUMENT-TYPE");
        cwf_Efm_Document_Document_Type.setDdmHeader("DOCUMENT/TYPE");
        cwf_Efm_Document_Document_Category = cwf_Efm_Document_Document_Type.newFieldInGroup("cwf_Efm_Document_Document_Category", "DOCUMENT-CATEGORY", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "DOCUMENT_CATEGORY");
        cwf_Efm_Document_Document_Category.setDdmHeader("DOC/CAT");
        cwf_Efm_Document_Document_Sub_Category = cwf_Efm_Document_Document_Type.newFieldInGroup("cwf_Efm_Document_Document_Sub_Category", "DOCUMENT-SUB-CATEGORY", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "DOCUMENT_SUB_CATEGORY");
        cwf_Efm_Document_Document_Sub_Category.setDdmHeader("DOC SUB-/CATEGORY");
        cwf_Efm_Document_Document_Detail = cwf_Efm_Document_Document_Type.newFieldInGroup("cwf_Efm_Document_Document_Detail", "DOCUMENT-DETAIL", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "DOCUMENT_DETAIL");
        cwf_Efm_Document_Document_Detail.setDdmHeader("DOCUMENT/DETAIL");
        cwf_Efm_Document_Copy_Ind = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Copy_Ind", "COPY-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "COPY_IND");
        cwf_Efm_Document_Copy_Ind.setDdmHeader("COPY/IND");
        cwf_Efm_Document_Sub_Rqst_Work_Prcss_Id = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Sub_Rqst_Work_Prcss_Id", "SUB-RQST-WORK-PRCSS-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "SUB_RQST_WORK_PRCSS_ID");
        cwf_Efm_Document_Sub_Rqst_Work_Prcss_Id.setDdmHeader("SUB-REQUEST/WORK PROCESS ID");
        cwf_Efm_Document_Image_Min = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Image_Min", "IMAGE-MIN", FieldType.STRING, 14, 
            RepeatingFieldStrategy.None, "IMAGE_MIN");
        cwf_Efm_Document_Image_Min.setDdmHeader("IMAGE/POINTER");
        cwf_Efm_Document_Image_Source_Id = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Image_Source_Id", "IMAGE-SOURCE-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "IMAGE_SOURCE_ID");
        cwf_Efm_Document_Image_Source_Id.setDdmHeader("IMAGE/SOURCE ID");
        cwf_Efm_Document_Dgtze_Doc_End_Page = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Dgtze_Doc_End_Page", "DGTZE-DOC-END-PAGE", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "DGTZE_DOC_END_PAGE");
        cwf_Efm_Document_Dgtze_Doc_End_Page.setDdmHeader("DOCUMENT/END PGE");
        registerRecord(vw_cwf_Efm_Document);

        vw_icw_Efm_Document = new DataAccessProgramView(new NameInfo("vw_icw_Efm_Document", "ICW-EFM-DOCUMENT"), "ICW_EFM_DOCUMENT", "ICW_EFM_DOCUMENT");
        icw_Efm_Document_Cabinet_Id = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Cabinet_Id", "CABINET-ID", FieldType.STRING, 12, 
            RepeatingFieldStrategy.None, "CABINET_ID");
        icw_Efm_Document_Cabinet_Id.setDdmHeader("CABINET/ID");
        icw_Efm_Document_Rqst_Log_Dte_Tme = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        icw_Efm_Document_Rqst_Log_Dte_Tme.setDdmHeader("MIT RQST/DATE AND TIME");
        icw_Efm_Document_Crte_Dte_Tme = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Crte_Dte_Tme", "CRTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "CRTE_DTE_TME");
        icw_Efm_Document_Crte_Dte_Tme.setDdmHeader("CREATE/DATE AND TIME");
        icw_Efm_Document_Crte_Empl_Racf_Id = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Crte_Empl_Racf_Id", "CRTE-EMPL-RACF-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CRTE_EMPL_RACF_ID");
        icw_Efm_Document_Crte_Empl_Racf_Id.setDdmHeader("CREATED BY/RACF ID");
        icw_Efm_Document_Crte_Unit = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Crte_Unit", "CRTE-UNIT", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "CRTE_UNIT");
        icw_Efm_Document_Crte_Unit.setDdmHeader("CREATED BY/UNIT");

        icw_Efm_Document_Dcmnt_Type = vw_icw_Efm_Document.getRecord().newGroupInGroup("ICW_EFM_DOCUMENT_DCMNT_TYPE", "DCMNT-TYPE");
        icw_Efm_Document_Dcmnt_Type.setDdmHeader("DOCUMENT/TYPE");
        icw_Efm_Document_Dcmnt_Ctgry = icw_Efm_Document_Dcmnt_Type.newFieldInGroup("icw_Efm_Document_Dcmnt_Ctgry", "DCMNT-CTGRY", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "DCMNT_CTGRY");
        icw_Efm_Document_Dcmnt_Ctgry.setDdmHeader("DOC/CAT");
        icw_Efm_Document_Dcmnt_Sub_Ctgry = icw_Efm_Document_Dcmnt_Type.newFieldInGroup("icw_Efm_Document_Dcmnt_Sub_Ctgry", "DCMNT-SUB-CTGRY", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "DCMNT_SUB_CTGRY");
        icw_Efm_Document_Dcmnt_Sub_Ctgry.setDdmHeader("DOC SUB-/CATEGORY");
        icw_Efm_Document_Dcmnt_Dtl = icw_Efm_Document_Dcmnt_Type.newFieldInGroup("icw_Efm_Document_Dcmnt_Dtl", "DCMNT-DTL", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "DCMNT_DTL");
        icw_Efm_Document_Dcmnt_Dtl.setDdmHeader("DOCUMENT/DETAIL");
        icw_Efm_Document_Image_Source_Id = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Image_Source_Id", "IMAGE-SOURCE-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "IMAGE_SOURCE_ID");
        icw_Efm_Document_Image_Source_Id.setDdmHeader("IMAGE/SOURCE ID");
        icw_Efm_Document_Image_Min = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Image_Min", "IMAGE-MIN", FieldType.STRING, 14, 
            RepeatingFieldStrategy.None, "IMAGE_MIN");
        icw_Efm_Document_Image_Min.setDdmHeader("IMAGE/POINTER");
        icw_Efm_Document_Dcmnt_Copy_Ind = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Dcmnt_Copy_Ind", "DCMNT-COPY-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DCMNT_COPY_IND");
        icw_Efm_Document_Dcmnt_Copy_Ind.setDdmHeader("COPY/IND");
        icw_Efm_Document_Image_End_Page = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Image_End_Page", "IMAGE-END-PAGE", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "IMAGE_END_PAGE");
        icw_Efm_Document_Image_End_Page.setDdmHeader("IMAGE/END PAGE");
        registerRecord(vw_icw_Efm_Document);

        vw_ncw_Efm_Document = new DataAccessProgramView(new NameInfo("vw_ncw_Efm_Document", "NCW-EFM-DOCUMENT"), "NCW_EFM_DOCUMENT", "NCW_EFM_DOCUMENT");
        ncw_Efm_Document_Np_Pin = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Np_Pin", "NP-PIN", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "NP_PIN");
        ncw_Efm_Document_Rqst_Log_Dte_Tme = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        ncw_Efm_Document_Rqst_Log_Dte_Tme.setDdmHeader("MIT RQST/DATE AND TIME");
        ncw_Efm_Document_Crte_Dte_Tme = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Crte_Dte_Tme", "CRTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "CRTE_DTE_TME");
        ncw_Efm_Document_Crte_Dte_Tme.setDdmHeader("CREATE/DATE AND TIME");
        ncw_Efm_Document_Crte_Empl_Racf_Id = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Crte_Empl_Racf_Id", "CRTE-EMPL-RACF-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CRTE_EMPL_RACF_ID");
        ncw_Efm_Document_Crte_Empl_Racf_Id.setDdmHeader("CREATED BY/RACF ID");
        ncw_Efm_Document_Crte_Unit = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Crte_Unit", "CRTE-UNIT", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "CRTE_UNIT");
        ncw_Efm_Document_Crte_Unit.setDdmHeader("CREATED BY/UNIT");

        ncw_Efm_Document_Dcmnt_Type = vw_ncw_Efm_Document.getRecord().newGroupInGroup("NCW_EFM_DOCUMENT_DCMNT_TYPE", "DCMNT-TYPE");
        ncw_Efm_Document_Dcmnt_Type.setDdmHeader("DOCUMENT/TYPE");
        ncw_Efm_Document_Dcmnt_Ctgry = ncw_Efm_Document_Dcmnt_Type.newFieldInGroup("ncw_Efm_Document_Dcmnt_Ctgry", "DCMNT-CTGRY", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "DCMNT_CTGRY");
        ncw_Efm_Document_Dcmnt_Ctgry.setDdmHeader("DOC/CAT");
        ncw_Efm_Document_Dcmnt_Sub_Ctgry = ncw_Efm_Document_Dcmnt_Type.newFieldInGroup("ncw_Efm_Document_Dcmnt_Sub_Ctgry", "DCMNT-SUB-CTGRY", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "DCMNT_SUB_CTGRY");
        ncw_Efm_Document_Dcmnt_Sub_Ctgry.setDdmHeader("DOC SUB-/CATEGORY");
        ncw_Efm_Document_Dcmnt_Dtl = ncw_Efm_Document_Dcmnt_Type.newFieldInGroup("ncw_Efm_Document_Dcmnt_Dtl", "DCMNT-DTL", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "DCMNT_DTL");
        ncw_Efm_Document_Dcmnt_Dtl.setDdmHeader("DOCUMENT/DETAIL");
        ncw_Efm_Document_Image_Source_Id = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Image_Source_Id", "IMAGE-SOURCE-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "IMAGE_SOURCE_ID");
        ncw_Efm_Document_Image_Source_Id.setDdmHeader("IMAGE/SOURCE ID");
        ncw_Efm_Document_Dcmnt_Copy_Ind = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Dcmnt_Copy_Ind", "DCMNT-COPY-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DCMNT_COPY_IND");
        ncw_Efm_Document_Dcmnt_Copy_Ind.setDdmHeader("COPY/IND");
        ncw_Efm_Document_Image_Min = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Image_Min", "IMAGE-MIN", FieldType.STRING, 14, 
            RepeatingFieldStrategy.None, "IMAGE_MIN");
        ncw_Efm_Document_Image_Min.setDdmHeader("IMAGE/POINTER");
        ncw_Efm_Document_Image_End_Page = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Image_End_Page", "IMAGE-END-PAGE", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "IMAGE_END_PAGE");
        ncw_Efm_Document_Image_End_Page.setDdmHeader("IMAGE/END PAGE");
        registerRecord(vw_ncw_Efm_Document);

        vw_cwf_Org_Empl_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Org_Empl_Tbl", "CWF-ORG-EMPL-TBL"), "CWF_ORG_EMPL_TBL", "CWF_ASSIGN_RULE");
        cwf_Org_Empl_Tbl_Empl_Racf_Id = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "EMPL_RACF_ID");
        cwf_Org_Empl_Tbl_Empl_Racf_Id.setDdmHeader("EMPLOYEE/RACF  ID");
        cwf_Org_Empl_Tbl_Empl_Info = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Empl_Info", "EMPL-INFO", FieldType.STRING, 100, 
            RepeatingFieldStrategy.None, "EMPL_INFO");
        cwf_Org_Empl_Tbl_Unit_Empl_Racf_Id_Key = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Unit_Empl_Racf_Id_Key", "UNIT-EMPL-RACF-ID-KEY", 
            FieldType.STRING, 17, RepeatingFieldStrategy.None, "UNIT_EMPL_RACF_ID_KEY");
        cwf_Org_Empl_Tbl_Unit_Empl_Racf_Id_Key.setDdmHeader("EMPLOYEE RACF ID");
        cwf_Org_Empl_Tbl_Unit_Empl_Racf_Id_Key.setSuperDescriptor(true);
        registerRecord(vw_cwf_Org_Empl_Tbl);

        vw_cwf_Efm_Cabinet = new DataAccessProgramView(new NameInfo("vw_cwf_Efm_Cabinet", "CWF-EFM-CABINET"), "CWF_EFM_CABINET", "CWF_EFM_CABINET");
        cwf_Efm_Cabinet_Cabinet_Id = vw_cwf_Efm_Cabinet.getRecord().newFieldInGroup("cwf_Efm_Cabinet_Cabinet_Id", "CABINET-ID", FieldType.STRING, 14, 
            RepeatingFieldStrategy.None, "CABINET_ID");
        cwf_Efm_Cabinet_Cabinet_Id.setDdmHeader("CABINET/ID");

        cwf_Efm_Cabinet__R_Field_9 = vw_cwf_Efm_Cabinet.getRecord().newGroupInGroup("cwf_Efm_Cabinet__R_Field_9", "REDEFINE", cwf_Efm_Cabinet_Cabinet_Id);
        cwf_Efm_Cabinet_Cabinet_Id_Letter = cwf_Efm_Cabinet__R_Field_9.newFieldInGroup("cwf_Efm_Cabinet_Cabinet_Id_Letter", "CABINET-ID-LETTER", FieldType.STRING, 
            1);
        cwf_Efm_Cabinet_Cabinet_Id_Pin = cwf_Efm_Cabinet__R_Field_9.newFieldInGroup("cwf_Efm_Cabinet_Cabinet_Id_Pin", "CABINET-ID-PIN", FieldType.NUMERIC, 
            12);
        cwf_Efm_Cabinet_Mit_Rqst_Log_Dte_Tme = vw_cwf_Efm_Cabinet.getRecord().newFieldInGroup("cwf_Efm_Cabinet_Mit_Rqst_Log_Dte_Tme", "MIT-RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "MIT_RQST_LOG_DTE_TME");
        cwf_Efm_Cabinet_Mit_Rqst_Log_Dte_Tme.setDdmHeader("MIT-RLDT");
        cwf_Efm_Cabinet_Dgtze_Rqst_Log_Dte_Tme = vw_cwf_Efm_Cabinet.getRecord().newFieldInGroup("cwf_Efm_Cabinet_Dgtze_Rqst_Log_Dte_Tme", "DGTZE-RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "DGTZE_RQST_LOG_DTE_TME");
        cwf_Efm_Cabinet_Dgtze_Rqst_Log_Dte_Tme.setDdmHeader("DGTZE RQST LOG/DTE TME");
        cwf_Efm_Cabinet_Media_Ind = vw_cwf_Efm_Cabinet.getRecord().newFieldInGroup("cwf_Efm_Cabinet_Media_Ind", "MEDIA-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "MEDIA_IND");
        cwf_Efm_Cabinet_Media_Ind.setDdmHeader("MEDIA IND");
        cwf_Efm_Cabinet_Media_Updte_Dte_Tme = vw_cwf_Efm_Cabinet.getRecord().newFieldInGroup("cwf_Efm_Cabinet_Media_Updte_Dte_Tme", "MEDIA-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "MEDIA_UPDTE_DTE_TME");
        cwf_Efm_Cabinet_Media_Updte_Dte_Tme.setDdmHeader("MEDIA UPDATE");
        registerRecord(vw_cwf_Efm_Cabinet);

        work_File_1 = localVariables.newGroupInRecord("work_File_1", "WORK-FILE-1");
        work_File_1_Wf_Cabinet_Id = work_File_1.newFieldInGroup("work_File_1_Wf_Cabinet_Id", "WF-CABINET-ID", FieldType.STRING, 17);
        work_File_1_Wf_Crte_Dte_Tme = work_File_1.newFieldInGroup("work_File_1_Wf_Crte_Dte_Tme", "WF-CRTE-DTE-TME", FieldType.STRING, 15);
        work_File_1_Wf_Crte_Empl_Racf_Id = work_File_1.newFieldInGroup("work_File_1_Wf_Crte_Empl_Racf_Id", "WF-CRTE-EMPL-RACF-ID", FieldType.STRING, 8);
        work_File_1_Wf_Dcmnt_Copy_Ind = work_File_1.newFieldInGroup("work_File_1_Wf_Dcmnt_Copy_Ind", "WF-DCMNT-COPY-IND", FieldType.STRING, 1);
        work_File_1_Wf_Dcmnt_Type = work_File_1.newFieldInGroup("work_File_1_Wf_Dcmnt_Type", "WF-DCMNT-TYPE", FieldType.STRING, 8);

        work_File_1__R_Field_10 = work_File_1.newGroupInGroup("work_File_1__R_Field_10", "REDEFINE", work_File_1_Wf_Dcmnt_Type);
        work_File_1_Wf_Dcmnt_Category = work_File_1__R_Field_10.newFieldInGroup("work_File_1_Wf_Dcmnt_Category", "WF-DCMNT-CATEGORY", FieldType.STRING, 
            1);
        work_File_1_Wf_Dcmnt_Sub_Category = work_File_1__R_Field_10.newFieldInGroup("work_File_1_Wf_Dcmnt_Sub_Category", "WF-DCMNT-SUB-CATEGORY", FieldType.STRING, 
            3);
        work_File_1_Wf_Dcmnt_Detail = work_File_1__R_Field_10.newFieldInGroup("work_File_1_Wf_Dcmnt_Detail", "WF-DCMNT-DETAIL", FieldType.STRING, 4);
        work_File_1_Wf_Dcmnt_File_Flag = work_File_1.newFieldInGroup("work_File_1_Wf_Dcmnt_File_Flag", "WF-DCMNT-FILE-FLAG", FieldType.STRING, 1);
        work_File_1_Wf_Mail_Item_Nbr = work_File_1.newFieldInGroup("work_File_1_Wf_Mail_Item_Nbr", "WF-MAIL-ITEM-NBR", FieldType.NUMERIC, 11);
        work_File_1_Wf_Mj_Rqst_Log_Dte_Tme = work_File_1.newFieldInGroup("work_File_1_Wf_Mj_Rqst_Log_Dte_Tme", "WF-MJ-RQST-LOG-DTE-TME", FieldType.STRING, 
            15);
        work_File_1_Wf_Mj_Work_Prcss_Id = work_File_1.newFieldInGroup("work_File_1_Wf_Mj_Work_Prcss_Id", "WF-MJ-WORK-PRCSS-ID", FieldType.STRING, 6);
        work_File_1_Wf_Nbr_Of_Pages = work_File_1.newFieldInGroup("work_File_1_Wf_Nbr_Of_Pages", "WF-NBR-OF-PAGES", FieldType.NUMERIC, 3);
        work_File_1_Wf_Rqst_Work_Prcss_Id = work_File_1.newFieldInGroup("work_File_1_Wf_Rqst_Work_Prcss_Id", "WF-RQST-WORK-PRCSS-ID", FieldType.STRING, 
            6);
        work_File_1_Wf_Site = work_File_1.newFieldInGroup("work_File_1_Wf_Site", "WF-SITE", FieldType.STRING, 2);
        work_File_1_Wf_Source_Id = work_File_1.newFieldInGroup("work_File_1_Wf_Source_Id", "WF-SOURCE-ID", FieldType.STRING, 6);
        work_File_1_Wf_Upld_Date_Time = work_File_1.newFieldInGroup("work_File_1_Wf_Upld_Date_Time", "WF-UPLD-DATE-TIME", FieldType.STRING, 15);

        report_Line = localVariables.newGroupInRecord("report_Line", "REPORT-LINE");
        report_Line_Wr_System = report_Line.newFieldInGroup("report_Line_Wr_System", "WR-SYSTEM", FieldType.STRING, 3);
        report_Line_Wr_Cabinet_Id = report_Line.newFieldInGroup("report_Line_Wr_Cabinet_Id", "WR-CABINET-ID", FieldType.STRING, 17);
        report_Line_Wr_Crte_Dte_Tme = report_Line.newFieldInGroup("report_Line_Wr_Crte_Dte_Tme", "WR-CRTE-DTE-TME", FieldType.STRING, 15);
        report_Line_Wr_Crte_Empl_Racf_Id = report_Line.newFieldInGroup("report_Line_Wr_Crte_Empl_Racf_Id", "WR-CRTE-EMPL-RACF-ID", FieldType.STRING, 8);
        report_Line_Wr_Dcmnt_Copy_Ind = report_Line.newFieldInGroup("report_Line_Wr_Dcmnt_Copy_Ind", "WR-DCMNT-COPY-IND", FieldType.STRING, 1);
        report_Line_Wr_Dcmnt_Type = report_Line.newFieldInGroup("report_Line_Wr_Dcmnt_Type", "WR-DCMNT-TYPE", FieldType.STRING, 8);
        report_Line_Wr_Dcmnt_File_Flag = report_Line.newFieldInGroup("report_Line_Wr_Dcmnt_File_Flag", "WR-DCMNT-FILE-FLAG", FieldType.STRING, 1);
        report_Line_Wr_Mail_Item_Nbr = report_Line.newFieldInGroup("report_Line_Wr_Mail_Item_Nbr", "WR-MAIL-ITEM-NBR", FieldType.NUMERIC, 11);
        report_Line_Wr_Mj_Rqst_Log_Dte_Tme = report_Line.newFieldInGroup("report_Line_Wr_Mj_Rqst_Log_Dte_Tme", "WR-MJ-RQST-LOG-DTE-TME", FieldType.STRING, 
            15);
        report_Line_Wr_Mj_Work_Prcss_Id = report_Line.newFieldInGroup("report_Line_Wr_Mj_Work_Prcss_Id", "WR-MJ-WORK-PRCSS-ID", FieldType.STRING, 6);
        report_Line_Wr_Nbr_Of_Pages = report_Line.newFieldInGroup("report_Line_Wr_Nbr_Of_Pages", "WR-NBR-OF-PAGES", FieldType.STRING, 3);
        report_Line_Wr_Rqst_Work_Prcss_Id = report_Line.newFieldInGroup("report_Line_Wr_Rqst_Work_Prcss_Id", "WR-RQST-WORK-PRCSS-ID", FieldType.STRING, 
            6);
        report_Line_Wr_Site = report_Line.newFieldInGroup("report_Line_Wr_Site", "WR-SITE", FieldType.STRING, 2);
        report_Line_Wr_Source_Id = report_Line.newFieldInGroup("report_Line_Wr_Source_Id", "WR-SOURCE-ID", FieldType.STRING, 6);
        report_Line_Wr_Upld_Date_Time = report_Line.newFieldInGroup("report_Line_Wr_Upld_Date_Time", "WR-UPLD-DATE-TIME", FieldType.STRING, 15);
        report_Line_Wr_Rec_End = report_Line.newFieldInGroup("report_Line_Wr_Rec_End", "WR-REC-END", FieldType.STRING, 1);
        pnd_Reported_Case_Rldt = localVariables.newFieldArrayInRecord("pnd_Reported_Case_Rldt", "#REPORTED-CASE-RLDT", FieldType.STRING, 44, new DbsArrayController(1, 
            100));
        pnd_Compare_Value = localVariables.newFieldInRecord("pnd_Compare_Value", "#COMPARE-VALUE", FieldType.STRING, 44);

        pnd_Compare_Value__R_Field_11 = localVariables.newGroupInRecord("pnd_Compare_Value__R_Field_11", "REDEFINE", pnd_Compare_Value);
        pnd_Compare_Value_Pnd_Compare_Value_Min = pnd_Compare_Value__R_Field_11.newFieldInGroup("pnd_Compare_Value_Pnd_Compare_Value_Min", "#COMPARE-VALUE-MIN", 
            FieldType.STRING, 14);
        pnd_Compare_Value_Pnd_Compare_Value_Wpid = pnd_Compare_Value__R_Field_11.newFieldInGroup("pnd_Compare_Value_Pnd_Compare_Value_Wpid", "#COMPARE-VALUE-WPID", 
            FieldType.STRING, 6);
        pnd_Compare_Value_Pnd_Compare_Value_Multi = pnd_Compare_Value__R_Field_11.newFieldInGroup("pnd_Compare_Value_Pnd_Compare_Value_Multi", "#COMPARE-VALUE-MULTI", 
            FieldType.STRING, 1);
        pnd_Compare_Value_Pnd_Compare_Value_Rldt = pnd_Compare_Value__R_Field_11.newFieldInGroup("pnd_Compare_Value_Pnd_Compare_Value_Rldt", "#COMPARE-VALUE-RLDT", 
            FieldType.STRING, 15);
        pnd_Compare_Value_Pnd_Compare_Doc_Cat = pnd_Compare_Value__R_Field_11.newFieldInGroup("pnd_Compare_Value_Pnd_Compare_Doc_Cat", "#COMPARE-DOC-CAT", 
            FieldType.STRING, 1);
        pnd_Compare_Value_Pnd_Compare_Doc_Sub_Cat = pnd_Compare_Value__R_Field_11.newFieldInGroup("pnd_Compare_Value_Pnd_Compare_Doc_Sub_Cat", "#COMPARE-DOC-SUB-CAT", 
            FieldType.STRING, 3);
        pnd_Compare_Value_Pnd_Compare_Doc_Detail = pnd_Compare_Value__R_Field_11.newFieldInGroup("pnd_Compare_Value_Pnd_Compare_Doc_Detail", "#COMPARE-DOC-DETAIL", 
            FieldType.STRING, 4);
        pnd_Rep_Post_Case_Rldt = localVariables.newFieldArrayInRecord("pnd_Rep_Post_Case_Rldt", "#REP-POST-CASE-RLDT", FieldType.STRING, 15, new DbsArrayController(1, 
            100));
        pnd_Com_Post_Value = localVariables.newFieldInRecord("pnd_Com_Post_Value", "#COM-POST-VALUE", FieldType.STRING, 15);

        pnd_Com_Post_Value__R_Field_12 = localVariables.newGroupInRecord("pnd_Com_Post_Value__R_Field_12", "REDEFINE", pnd_Com_Post_Value);
        pnd_Com_Post_Value_Pnd_Post_Value_Wpid = pnd_Com_Post_Value__R_Field_12.newFieldInGroup("pnd_Com_Post_Value_Pnd_Post_Value_Wpid", "#POST-VALUE-WPID", 
            FieldType.STRING, 6);
        pnd_Com_Post_Value_Pnd_Post_Value_Multi = pnd_Com_Post_Value__R_Field_12.newFieldInGroup("pnd_Com_Post_Value_Pnd_Post_Value_Multi", "#POST-VALUE-MULTI", 
            FieldType.STRING, 1);
        pnd_Com_Post_Value_Pnd_Post_Doc_Cat = pnd_Com_Post_Value__R_Field_12.newFieldInGroup("pnd_Com_Post_Value_Pnd_Post_Doc_Cat", "#POST-DOC-CAT", FieldType.STRING, 
            1);
        pnd_Com_Post_Value_Pnd_Post_Doc_Sub_Cat = pnd_Com_Post_Value__R_Field_12.newFieldInGroup("pnd_Com_Post_Value_Pnd_Post_Doc_Sub_Cat", "#POST-DOC-SUB-CAT", 
            FieldType.STRING, 3);
        pnd_Com_Post_Value_Pnd_Post_Doc_Detail = pnd_Com_Post_Value__R_Field_12.newFieldInGroup("pnd_Com_Post_Value_Pnd_Post_Doc_Detail", "#POST-DOC-DETAIL", 
            FieldType.STRING, 4);
        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_13 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_13", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_13.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_13.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_13.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind = pnd_Tbl_Prime_Key__R_Field_13.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", FieldType.STRING, 
            1);
        pnd_Common_Key = localVariables.newFieldInRecord("pnd_Common_Key", "#COMMON-KEY", FieldType.STRING, 17);

        pnd_Common_Key__R_Field_14 = localVariables.newGroupInRecord("pnd_Common_Key__R_Field_14", "REDEFINE", pnd_Common_Key);
        pnd_Common_Key_Pnd_Common_Image_Source_Id = pnd_Common_Key__R_Field_14.newFieldInGroup("pnd_Common_Key_Pnd_Common_Image_Source_Id", "#COMMON-IMAGE-SOURCE-ID", 
            FieldType.STRING, 6);
        pnd_Common_Key_Pnd_Common_Image_Min = pnd_Common_Key__R_Field_14.newFieldInGroup("pnd_Common_Key_Pnd_Common_Image_Min", "#COMMON-IMAGE-MIN", FieldType.NUMERIC, 
            11);
        pnd_Unit_Empl_Racf_Id_Key = localVariables.newFieldInRecord("pnd_Unit_Empl_Racf_Id_Key", "#UNIT-EMPL-RACF-ID-KEY", FieldType.STRING, 17);

        pnd_Unit_Empl_Racf_Id_Key__R_Field_15 = localVariables.newGroupInRecord("pnd_Unit_Empl_Racf_Id_Key__R_Field_15", "REDEFINE", pnd_Unit_Empl_Racf_Id_Key);
        pnd_Unit_Empl_Racf_Id_Key_Pnd_Empl_Unit_Cde = pnd_Unit_Empl_Racf_Id_Key__R_Field_15.newFieldInGroup("pnd_Unit_Empl_Racf_Id_Key_Pnd_Empl_Unit_Cde", 
            "#EMPL-UNIT-CDE", FieldType.STRING, 8);
        pnd_Unit_Empl_Racf_Id_Key_Pnd_Empl_Racf_Id = pnd_Unit_Empl_Racf_Id_Key__R_Field_15.newFieldInGroup("pnd_Unit_Empl_Racf_Id_Key_Pnd_Empl_Racf_Id", 
            "#EMPL-RACF-ID", FieldType.STRING, 8);
        pnd_Unit_Empl_Racf_Id_Key_Pnd_Actve_Ind = pnd_Unit_Empl_Racf_Id_Key__R_Field_15.newFieldInGroup("pnd_Unit_Empl_Racf_Id_Key_Pnd_Actve_Ind", "#ACTVE-IND", 
            FieldType.STRING, 1);
        pnd_Active_Cabinet_Key = localVariables.newFieldInRecord("pnd_Active_Cabinet_Key", "#ACTIVE-CABINET-KEY", FieldType.STRING, 15);

        pnd_Active_Cabinet_Key__R_Field_16 = localVariables.newGroupInRecord("pnd_Active_Cabinet_Key__R_Field_16", "REDEFINE", pnd_Active_Cabinet_Key);
        pnd_Active_Cabinet_Key_Pnd_Cabinet_Id = pnd_Active_Cabinet_Key__R_Field_16.newFieldInGroup("pnd_Active_Cabinet_Key_Pnd_Cabinet_Id", "#CABINET-ID", 
            FieldType.STRING, 14);
        pnd_Active_Cabinet_Key_Pnd_Actve_Fldrs_Ind = pnd_Active_Cabinet_Key__R_Field_16.newFieldInGroup("pnd_Active_Cabinet_Key_Pnd_Actve_Fldrs_Ind", 
            "#ACTVE-FLDRS-IND", FieldType.STRING, 1);
        pnd_Folder_Key = localVariables.newFieldInRecord("pnd_Folder_Key", "#FOLDER-KEY", FieldType.STRING, 27);

        pnd_Folder_Key__R_Field_17 = localVariables.newGroupInRecord("pnd_Folder_Key__R_Field_17", "REDEFINE", pnd_Folder_Key);
        pnd_Folder_Key_Pnd_Cabinet_Id = pnd_Folder_Key__R_Field_17.newFieldInGroup("pnd_Folder_Key_Pnd_Cabinet_Id", "#CABINET-ID", FieldType.STRING, 14);
        pnd_Folder_Key_Pnd_Tiaa_Rcvd_Dte = pnd_Folder_Key__R_Field_17.newFieldInGroup("pnd_Folder_Key_Pnd_Tiaa_Rcvd_Dte", "#TIAA-RCVD-DTE", FieldType.DATE);
        pnd_Folder_Key_Pnd_Case_Workprcss_Multi_Subrqst = pnd_Folder_Key__R_Field_17.newFieldInGroup("pnd_Folder_Key_Pnd_Case_Workprcss_Multi_Subrqst", 
            "#CASE-WORKPRCSS-MULTI-SUBRQST", FieldType.STRING, 9);
        pnd_Folder_Name_Key = localVariables.newFieldInRecord("pnd_Folder_Name_Key", "#FOLDER-NAME-KEY", FieldType.STRING, 29);

        pnd_Folder_Name_Key__R_Field_18 = localVariables.newGroupInRecord("pnd_Folder_Name_Key__R_Field_18", "REDEFINE", pnd_Folder_Name_Key);
        pnd_Folder_Name_Key_Pnd_Rqst_Id = pnd_Folder_Name_Key__R_Field_18.newFieldInGroup("pnd_Folder_Name_Key_Pnd_Rqst_Id", "#RQST-ID", FieldType.STRING, 
            28);

        pnd_Folder_Name_Key__R_Field_19 = pnd_Folder_Name_Key__R_Field_18.newGroupInGroup("pnd_Folder_Name_Key__R_Field_19", "REDEFINE", pnd_Folder_Name_Key_Pnd_Rqst_Id);
        pnd_Folder_Name_Key_Pnd_Wpid = pnd_Folder_Name_Key__R_Field_19.newFieldInGroup("pnd_Folder_Name_Key_Pnd_Wpid", "#WPID", FieldType.STRING, 6);
        pnd_Folder_Name_Key_Pnd_Tiaa_Rcvd_Dte = pnd_Folder_Name_Key__R_Field_19.newFieldInGroup("pnd_Folder_Name_Key_Pnd_Tiaa_Rcvd_Dte", "#TIAA-RCVD-DTE", 
            FieldType.STRING, 8);
        pnd_Folder_Name_Key_Pnd_Case_Id_1 = pnd_Folder_Name_Key__R_Field_19.newFieldInGroup("pnd_Folder_Name_Key_Pnd_Case_Id_1", "#CASE-ID-1", FieldType.STRING, 
            1);
        pnd_Folder_Name_Key_Pnd_Case_Id_2 = pnd_Folder_Name_Key__R_Field_19.newFieldInGroup("pnd_Folder_Name_Key_Pnd_Case_Id_2", "#CASE-ID-2", FieldType.STRING, 
            1);
        pnd_Folder_Name_Key_Pnd_Pin_Nbr = pnd_Folder_Name_Key__R_Field_19.newFieldInGroup("pnd_Folder_Name_Key_Pnd_Pin_Nbr", "#PIN-NBR", FieldType.NUMERIC, 
            12);
        pnd_Case_Id_Key = localVariables.newFieldInRecord("pnd_Case_Id_Key", "#CASE-ID-KEY", FieldType.STRING, 13);

        pnd_Case_Id_Key__R_Field_20 = localVariables.newGroupInRecord("pnd_Case_Id_Key__R_Field_20", "REDEFINE", pnd_Case_Id_Key);
        pnd_Case_Id_Key_Pnd_Pin_Nbr = pnd_Case_Id_Key__R_Field_20.newFieldInGroup("pnd_Case_Id_Key_Pnd_Pin_Nbr", "#PIN-NBR", FieldType.NUMERIC, 7);
        pnd_Case_Id_Key_Pnd_Tiaa_Rcvd_Dte = pnd_Case_Id_Key__R_Field_20.newFieldInGroup("pnd_Case_Id_Key_Pnd_Tiaa_Rcvd_Dte", "#TIAA-RCVD-DTE", FieldType.DATE);
        pnd_Case_Id_Key_Pnd_Case_Id_Cde = pnd_Case_Id_Key__R_Field_20.newFieldInGroup("pnd_Case_Id_Key_Pnd_Case_Id_Cde", "#CASE-ID-CDE", FieldType.STRING, 
            1);
        pnd_Case_Id_Key_Pnd_Actve_Ind = pnd_Case_Id_Key__R_Field_20.newFieldInGroup("pnd_Case_Id_Key_Pnd_Actve_Ind", "#ACTVE-IND", FieldType.STRING, 1);
        pnd_Actv_Unque_Key = localVariables.newFieldInRecord("pnd_Actv_Unque_Key", "#ACTV-UNQUE-KEY", FieldType.STRING, 16);

        pnd_Actv_Unque_Key__R_Field_21 = localVariables.newGroupInRecord("pnd_Actv_Unque_Key__R_Field_21", "REDEFINE", pnd_Actv_Unque_Key);
        pnd_Actv_Unque_Key_Pnd_Rqst_Log_Dte_Tme = pnd_Actv_Unque_Key__R_Field_21.newFieldInGroup("pnd_Actv_Unque_Key_Pnd_Rqst_Log_Dte_Tme", "#RQST-LOG-DTE-TME", 
            FieldType.STRING, 15);
        pnd_Actv_Unque_Key_Pnd_Actve_Ind = pnd_Actv_Unque_Key__R_Field_21.newFieldInGroup("pnd_Actv_Unque_Key_Pnd_Actve_Ind", "#ACTVE-IND", FieldType.STRING, 
            1);
        pnd_Rqst_Id_Key = localVariables.newFieldInRecord("pnd_Rqst_Id_Key", "#RQST-ID-KEY", FieldType.STRING, 30);

        pnd_Rqst_Id_Key__R_Field_22 = localVariables.newGroupInRecord("pnd_Rqst_Id_Key__R_Field_22", "REDEFINE", pnd_Rqst_Id_Key);
        pnd_Rqst_Id_Key_Pnd_Rqst_Id = pnd_Rqst_Id_Key__R_Field_22.newFieldInGroup("pnd_Rqst_Id_Key_Pnd_Rqst_Id", "#RQST-ID", FieldType.STRING, 28);

        pnd_Rqst_Id_Key__R_Field_23 = pnd_Rqst_Id_Key__R_Field_22.newGroupInGroup("pnd_Rqst_Id_Key__R_Field_23", "REDEFINE", pnd_Rqst_Id_Key_Pnd_Rqst_Id);
        pnd_Rqst_Id_Key_Pnd_Rqst_Wpid = pnd_Rqst_Id_Key__R_Field_23.newFieldInGroup("pnd_Rqst_Id_Key_Pnd_Rqst_Wpid", "#RQST-WPID", FieldType.STRING, 6);
        pnd_Rqst_Id_Key_Pnd_Rqst_Rcvd_Dte = pnd_Rqst_Id_Key__R_Field_23.newFieldInGroup("pnd_Rqst_Id_Key_Pnd_Rqst_Rcvd_Dte", "#RQST-RCVD-DTE", FieldType.STRING, 
            8);
        pnd_Rqst_Id_Key_Pnd_Rqst_Group_9 = pnd_Rqst_Id_Key__R_Field_23.newFieldInGroup("pnd_Rqst_Id_Key_Pnd_Rqst_Group_9", "#RQST-GROUP-9", FieldType.STRING, 
            14);

        pnd_Rqst_Id_Key__R_Field_24 = pnd_Rqst_Id_Key__R_Field_23.newGroupInGroup("pnd_Rqst_Id_Key__R_Field_24", "REDEFINE", pnd_Rqst_Id_Key_Pnd_Rqst_Group_9);
        pnd_Rqst_Id_Key_Pnd_Rqst_Case_1 = pnd_Rqst_Id_Key__R_Field_24.newFieldInGroup("pnd_Rqst_Id_Key_Pnd_Rqst_Case_1", "#RQST-CASE-1", FieldType.STRING, 
            1);
        pnd_Rqst_Id_Key_Pnd_Rqst_Case_2 = pnd_Rqst_Id_Key__R_Field_24.newFieldInGroup("pnd_Rqst_Id_Key_Pnd_Rqst_Case_2", "#RQST-CASE-2", FieldType.STRING, 
            1);
        pnd_Rqst_Id_Key_Pnd_Rqst_Pin_Nbr = pnd_Rqst_Id_Key__R_Field_24.newFieldInGroup("pnd_Rqst_Id_Key_Pnd_Rqst_Pin_Nbr", "#RQST-PIN-NBR", FieldType.NUMERIC, 
            12);
        pnd_Rqst_Id_Key_Pnd_Multi_Rqst_Ind = pnd_Rqst_Id_Key__R_Field_22.newFieldInGroup("pnd_Rqst_Id_Key_Pnd_Multi_Rqst_Ind", "#MULTI-RQST-IND", FieldType.STRING, 
            1);
        pnd_Rqst_Id_Key_Pnd_Actve_Ind = pnd_Rqst_Id_Key__R_Field_22.newFieldInGroup("pnd_Rqst_Id_Key_Pnd_Actve_Ind", "#ACTVE-IND", FieldType.STRING, 1);
        pnd_Pin_History_Key = localVariables.newFieldInRecord("pnd_Pin_History_Key", "#PIN-HISTORY-KEY", FieldType.STRING, 35);

        pnd_Pin_History_Key__R_Field_25 = localVariables.newGroupInRecord("pnd_Pin_History_Key__R_Field_25", "REDEFINE", pnd_Pin_History_Key);
        pnd_Pin_History_Key_Pnd_Pin_Nbr = pnd_Pin_History_Key__R_Field_25.newFieldInGroup("pnd_Pin_History_Key_Pnd_Pin_Nbr", "#PIN-NBR", FieldType.NUMERIC, 
            12);
        pnd_Pin_History_Key_Pnd_Crprte_Status_Ind = pnd_Pin_History_Key__R_Field_25.newFieldInGroup("pnd_Pin_History_Key_Pnd_Crprte_Status_Ind", "#CRPRTE-STATUS-IND", 
            FieldType.STRING, 1);
        pnd_Pin_History_Key_Pnd_Rqst_Invrt_Rcvd_Dte_Tme = pnd_Pin_History_Key__R_Field_25.newFieldInGroup("pnd_Pin_History_Key_Pnd_Rqst_Invrt_Rcvd_Dte_Tme", 
            "#RQST-INVRT-RCVD-DTE-TME", FieldType.NUMERIC, 15);
        pnd_Pin_History_Key_Pnd_Work_Prcss_Id = pnd_Pin_History_Key__R_Field_25.newFieldInGroup("pnd_Pin_History_Key_Pnd_Work_Prcss_Id", "#WORK-PRCSS-ID", 
            FieldType.STRING, 6);
        pnd_Pin_History_Key_Pnd_Actve_Ind = pnd_Pin_History_Key__R_Field_25.newFieldInGroup("pnd_Pin_History_Key_Pnd_Actve_Ind", "#ACTVE-IND", FieldType.STRING, 
            1);
        pnd_Min_Key = localVariables.newFieldInRecord("pnd_Min_Key", "#MIN-KEY", FieldType.STRING, 12);

        pnd_Min_Key__R_Field_26 = localVariables.newGroupInRecord("pnd_Min_Key__R_Field_26", "REDEFINE", pnd_Min_Key);
        pnd_Min_Key_Pnd_Min_Value = pnd_Min_Key__R_Field_26.newFieldInGroup("pnd_Min_Key_Pnd_Min_Value", "#MIN-VALUE", FieldType.STRING, 11);
        pnd_Min_Key_Pnd_Actve_Ind = pnd_Min_Key__R_Field_26.newFieldInGroup("pnd_Min_Key_Pnd_Actve_Ind", "#ACTVE-IND", FieldType.STRING, 1);
        pnd_Running_Dte_Tme_Str = localVariables.newFieldInRecord("pnd_Running_Dte_Tme_Str", "#RUNNING-DTE-TME-STR", FieldType.STRING, 15);

        pnd_Running_Dte_Tme_Str__R_Field_27 = localVariables.newGroupInRecord("pnd_Running_Dte_Tme_Str__R_Field_27", "REDEFINE", pnd_Running_Dte_Tme_Str);
        pnd_Running_Dte_Tme_Str_Pnd_Running_Date = pnd_Running_Dte_Tme_Str__R_Field_27.newFieldInGroup("pnd_Running_Dte_Tme_Str_Pnd_Running_Date", "#RUNNING-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Running_Dte_Tme_Str_Pnd_Running_Time = pnd_Running_Dte_Tme_Str__R_Field_27.newFieldInGroup("pnd_Running_Dte_Tme_Str_Pnd_Running_Time", "#RUNNING-TIME", 
            FieldType.NUMERIC, 7);

        pnd_Running_Dte_Tme_Str__R_Field_28 = localVariables.newGroupInRecord("pnd_Running_Dte_Tme_Str__R_Field_28", "REDEFINE", pnd_Running_Dte_Tme_Str);
        pnd_Running_Dte_Tme_Str_Pnd_Running_Dte_Tme_Sn = pnd_Running_Dte_Tme_Str__R_Field_28.newFieldInGroup("pnd_Running_Dte_Tme_Str_Pnd_Running_Dte_Tme_Sn", 
            "#RUNNING-DTE-TME-SN", FieldType.NUMERIC, 15);
        pnd_Running_Dte_Tme_End = localVariables.newFieldInRecord("pnd_Running_Dte_Tme_End", "#RUNNING-DTE-TME-END", FieldType.STRING, 15);

        pnd_Running_Dte_Tme_End__R_Field_29 = localVariables.newGroupInRecord("pnd_Running_Dte_Tme_End__R_Field_29", "REDEFINE", pnd_Running_Dte_Tme_End);
        pnd_Running_Dte_Tme_End_Pnd_Running_Date_End = pnd_Running_Dte_Tme_End__R_Field_29.newFieldInGroup("pnd_Running_Dte_Tme_End_Pnd_Running_Date_End", 
            "#RUNNING-DATE-END", FieldType.STRING, 8);
        pnd_Running_Dte_Tme_End_Pnd_Running_Time_End = pnd_Running_Dte_Tme_End__R_Field_29.newFieldInGroup("pnd_Running_Dte_Tme_End_Pnd_Running_Time_End", 
            "#RUNNING-TIME-END", FieldType.NUMERIC, 7);

        pnd_Running_Dte_Tme_End__R_Field_30 = localVariables.newGroupInRecord("pnd_Running_Dte_Tme_End__R_Field_30", "REDEFINE", pnd_Running_Dte_Tme_End);
        pnd_Running_Dte_Tme_End_Pnd_Running_Dte_Tme_En = pnd_Running_Dte_Tme_End__R_Field_30.newFieldInGroup("pnd_Running_Dte_Tme_End_Pnd_Running_Dte_Tme_En", 
            "#RUNNING-DTE-TME-EN", FieldType.NUMERIC, 15);
        pnd_Isn_Control = localVariables.newFieldInRecord("pnd_Isn_Control", "#ISN-CONTROL", FieldType.PACKED_DECIMAL, 11);
        pnd_Pin_Nbr_Save = localVariables.newFieldInRecord("pnd_Pin_Nbr_Save", "#PIN-NBR-SAVE", FieldType.NUMERIC, 7);
        pnd_Wf_Rec_Cntr = localVariables.newFieldInRecord("pnd_Wf_Rec_Cntr", "#WF-REC-CNTR", FieldType.PACKED_DECIMAL, 11);
        pnd_Wpid_Split = localVariables.newFieldInRecord("pnd_Wpid_Split", "#WPID-SPLIT", FieldType.STRING, 6);

        pnd_Wpid_Split__R_Field_31 = localVariables.newGroupInRecord("pnd_Wpid_Split__R_Field_31", "REDEFINE", pnd_Wpid_Split);
        pnd_Wpid_Split_Pnd_Wpid_Split_2 = pnd_Wpid_Split__R_Field_31.newFieldInGroup("pnd_Wpid_Split_Pnd_Wpid_Split_2", "#WPID-SPLIT-2", FieldType.STRING, 
            2);
        pnd_Mit_Read = localVariables.newFieldInRecord("pnd_Mit_Read", "#MIT-READ", FieldType.NUMERIC, 11);
        pnd_Number_Of_Pages = localVariables.newFieldInRecord("pnd_Number_Of_Pages", "#NUMBER-OF-PAGES", FieldType.NUMERIC, 3);
        pnd_Min_Save = localVariables.newFieldInRecord("pnd_Min_Save", "#MIN-SAVE", FieldType.NUMERIC, 11);
        pnd_Isn_Save = localVariables.newFieldInRecord("pnd_Isn_Save", "#ISN-SAVE", FieldType.NUMERIC, 11);
        pnd_Numpages_Save = localVariables.newFieldInRecord("pnd_Numpages_Save", "#NUMPAGES-SAVE", FieldType.NUMERIC, 3);
        pnd_Date_Time_A = localVariables.newFieldInRecord("pnd_Date_Time_A", "#DATE-TIME-A", FieldType.STRING, 15);

        pnd_Date_Time_A__R_Field_32 = localVariables.newGroupInRecord("pnd_Date_Time_A__R_Field_32", "REDEFINE", pnd_Date_Time_A);
        pnd_Date_Time_A_Pnd_Date_Time_N = pnd_Date_Time_A__R_Field_32.newFieldInGroup("pnd_Date_Time_A_Pnd_Date_Time_N", "#DATE-TIME-N", FieldType.NUMERIC, 
            15);
        pnd_Date_T = localVariables.newFieldInRecord("pnd_Date_T", "#DATE-T", FieldType.TIME);
        pnd_Date_T_Result = localVariables.newFieldInRecord("pnd_Date_T_Result", "#DATE-T-RESULT", FieldType.TIME);
        pnd_Date_D = localVariables.newFieldInRecord("pnd_Date_D", "#DATE-D", FieldType.DATE);
        pnd_Curr_Date_N = localVariables.newFieldInRecord("pnd_Curr_Date_N", "#CURR-DATE-N", FieldType.NUMERIC, 8);

        pnd_Curr_Date_N__R_Field_33 = localVariables.newGroupInRecord("pnd_Curr_Date_N__R_Field_33", "REDEFINE", pnd_Curr_Date_N);
        pnd_Curr_Date_N_Pnd_Curr_Date_A = pnd_Curr_Date_N__R_Field_33.newFieldInGroup("pnd_Curr_Date_N_Pnd_Curr_Date_A", "#CURR-DATE-A", FieldType.STRING, 
            8);
        pnd_Mail_Item_Nbr_Save = localVariables.newFieldInRecord("pnd_Mail_Item_Nbr_Save", "#MAIL-ITEM-NBR-SAVE", FieldType.NUMERIC, 11);

        pnd_Mail_Item_Nbr_Save__R_Field_34 = localVariables.newGroupInRecord("pnd_Mail_Item_Nbr_Save__R_Field_34", "REDEFINE", pnd_Mail_Item_Nbr_Save);
        pnd_Mail_Item_Nbr_Save_Pnd_Mail_Item_Nbr_Save_A = pnd_Mail_Item_Nbr_Save__R_Field_34.newFieldInGroup("pnd_Mail_Item_Nbr_Save_Pnd_Mail_Item_Nbr_Save_A", 
            "#MAIL-ITEM-NBR-SAVE-A", FieldType.STRING, 11);
        pnd_Upld_Date_Time_Save = localVariables.newFieldInRecord("pnd_Upld_Date_Time_Save", "#UPLD-DATE-TIME-SAVE", FieldType.STRING, 15);
        pnd_Source_Id_Save = localVariables.newFieldInRecord("pnd_Source_Id_Save", "#SOURCE-ID-SAVE", FieldType.STRING, 6);
        pnd_Message = localVariables.newFieldInRecord("pnd_Message", "#MESSAGE", FieldType.STRING, 79);
        pnd_Control_Flag = localVariables.newFieldInRecord("pnd_Control_Flag", "#CONTROL-FLAG", FieldType.STRING, 1);
        pnd_Key_Compare = localVariables.newFieldInRecord("pnd_Key_Compare", "#KEY-COMPARE", FieldType.STRING, 17);

        pnd_Key_Compare__R_Field_35 = localVariables.newGroupInRecord("pnd_Key_Compare__R_Field_35", "REDEFINE", pnd_Key_Compare);
        pnd_Key_Compare_Pnd_Key_Compare_Source_Id = pnd_Key_Compare__R_Field_35.newFieldInGroup("pnd_Key_Compare_Pnd_Key_Compare_Source_Id", "#KEY-COMPARE-SOURCE-ID", 
            FieldType.STRING, 6);
        pnd_Key_Compare_Pnd_Key_Compare_Min = pnd_Key_Compare__R_Field_35.newFieldInGroup("pnd_Key_Compare_Pnd_Key_Compare_Min", "#KEY-COMPARE-MIN", FieldType.STRING, 
            11);
        pnd_Min_Split = localVariables.newFieldInRecord("pnd_Min_Split", "#MIN-SPLIT", FieldType.STRING, 14);

        pnd_Min_Split__R_Field_36 = localVariables.newGroupInRecord("pnd_Min_Split__R_Field_36", "REDEFINE", pnd_Min_Split);
        pnd_Min_Split_Pnd_Filler_Min_11 = pnd_Min_Split__R_Field_36.newFieldInGroup("pnd_Min_Split_Pnd_Filler_Min_11", "#FILLER-MIN-11", FieldType.NUMERIC, 
            11);
        pnd_Min_Split_Pnd_Min_Split_3 = pnd_Min_Split__R_Field_36.newFieldInGroup("pnd_Min_Split_Pnd_Min_Split_3", "#MIN-SPLIT-3", FieldType.NUMERIC, 
            3);
        pnd_Start_T = localVariables.newFieldInRecord("pnd_Start_T", "#START-T", FieldType.TIME);
        pnd_End_T = localVariables.newFieldInRecord("pnd_End_T", "#END-T", FieldType.TIME);
        pnd_Image_Cntr = localVariables.newFieldInRecord("pnd_Image_Cntr", "#IMAGE-CNTR", FieldType.NUMERIC, 11);
        pnd_Site_Split = localVariables.newFieldInRecord("pnd_Site_Split", "#SITE-SPLIT", FieldType.STRING, 39);

        pnd_Site_Split__R_Field_37 = localVariables.newGroupInRecord("pnd_Site_Split__R_Field_37", "REDEFINE", pnd_Site_Split);
        pnd_Site_Split_Pnd_Site_Filler = pnd_Site_Split__R_Field_37.newFieldInGroup("pnd_Site_Split_Pnd_Site_Filler", "#SITE-FILLER", FieldType.STRING, 
            37);
        pnd_Site_Split_Pnd_Site_Split_38 = pnd_Site_Split__R_Field_37.newFieldInGroup("pnd_Site_Split_Pnd_Site_Split_38", "#SITE-SPLIT-38", FieldType.STRING, 
            2);
        pnd_Doc_Fnd = localVariables.newFieldInRecord("pnd_Doc_Fnd", "#DOC-FND", FieldType.STRING, 1);
        pnd_Doc_Nfnd_Cntr = localVariables.newFieldInRecord("pnd_Doc_Nfnd_Cntr", "#DOC-NFND-CNTR", FieldType.NUMERIC, 11);
        pnd_Multi_Subrqst = localVariables.newFieldInRecord("pnd_Multi_Subrqst", "#MULTI-SUBRQST", FieldType.STRING, 1);
        pnd_Doc_Case = localVariables.newFieldInRecord("pnd_Doc_Case", "#DOC-CASE", FieldType.STRING, 2);

        pnd_Doc_Case__R_Field_38 = localVariables.newGroupInRecord("pnd_Doc_Case__R_Field_38", "REDEFINE", pnd_Doc_Case);
        pnd_Doc_Case_Pnd_Doc_Case_1 = pnd_Doc_Case__R_Field_38.newFieldInGroup("pnd_Doc_Case_Pnd_Doc_Case_1", "#DOC-CASE-1", FieldType.STRING, 1);
        pnd_Doc_Case_Pnd_Doc_Case_2 = pnd_Doc_Case__R_Field_38.newFieldInGroup("pnd_Doc_Case_Pnd_Doc_Case_2", "#DOC-CASE-2", FieldType.STRING, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_C = localVariables.newFieldInRecord("pnd_C", "#C", FieldType.NUMERIC, 3);
        pnd_Mit_Write_Flag = localVariables.newFieldInRecord("pnd_Mit_Write_Flag", "#MIT-WRITE-FLAG", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Image_Xref.reset();
        vw_cwf_Master_Index_View.reset();
        vw_ncw_Master_Index.reset();
        vw_icw_Master_Index.reset();
        vw_cwf_Efm_Folder.reset();
        vw_cwf_Support_Tbl.reset();
        vw_cwf_Efm_Document.reset();
        vw_icw_Efm_Document.reset();
        vw_ncw_Efm_Document.reset();
        vw_cwf_Org_Empl_Tbl.reset();
        vw_cwf_Efm_Cabinet.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Icwb3100() throws Exception
    {
        super("Icwb3100");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("ICWB3100", onError);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 132 PS = 60
        //*  MAIN PROCESSING
        //*  ---------------
        PROGRAM:                                                                                                                                                          //Natural: REPEAT
        while (condition(whileTrue))
        {
                                                                                                                                                                          //Natural: PERFORM READ-RUN-CONTROL-RECORD
            sub_Read_Run_Control_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PROGRAM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PROGRAM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM READ-CWF-IMAGE-XREF
            sub_Read_Cwf_Image_Xref();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PROGRAM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PROGRAM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM CREATE-SUMMARY-RECORD
            sub_Create_Summary_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PROGRAM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PROGRAM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTROL-RECORD
            sub_Update_Control_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PROGRAM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PROGRAM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM ESCAPE-PROGRAM
            sub_Escape_Program();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PROGRAM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PROGRAM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  SUBROUTINE JUST FOR ENDING THIS PROGRAM IMMEDIATELY
            //*  ---------------------------------------------------
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ESCAPE-PROGRAM
            //* (PROGRAM.)
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  SUBROUTINES
        //*  ===========
        //*  =======================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-RUN-CONTROL-RECORD
        //*  ===================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-CWF-IMAGE-XREF
        //*  ===================================
        //*  =========================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EXTRACT-DOC-INFO-FROM-CWF
        //*  ------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MIT-NO-ZZ-READ
        //*  ------------------------------------
        //*  ------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MIT-WITH-ZZ-READ
        //*  ------------------------------------------------------------------
        //*  =============================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCESS-FOLDER
        //*  =============================
        //*  ----------------------------------------------------------------
        //*  =========================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EXTRACT-DOC-INFO-FROM-ICW
        //*  =========================================
        //*  ----------------------------------------------------------------
        //*  =========================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EXTRACT-DOC-INFO-FROM-NCW
        //*  =========================================
        //*  ----------------------------------------------------------------
        //*  =================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-EMPLOYEE-INFO
        //*  ===============================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WF-RECORD
        //*  ============================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT
        //*  =====================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-SUMMARY-RECORD
        //*  =====================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-CONTROL-RECORD
        //*  =====================================
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Escape_Program() throws Exception                                                                                                                    //Natural: ESCAPE-PROGRAM
    {
        if (BLNatReinput.isReinput()) return;

        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( PROGRAM. )
        Global.setEscapeCode(EscapeType.Bottom, "PROGRAM");
        if (true) return;
    }
    private void sub_Read_Run_Control_Record() throws Exception                                                                                                           //Natural: READ-RUN-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  =======================================
        //*  --------- SET THE KEY -----------------
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind.setValue("A");                                                                                                          //Natural: MOVE 'A' TO #TBL-SCRTY-LEVEL-IND
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme.setValue("MIN-EXTRACT-CONTROL");                                                                                              //Natural: MOVE 'MIN-EXTRACT-CONTROL' TO #TBL-TABLE-NME
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field.setValue("UPLOAD DATE AND RECORD COUNT");                                                                                     //Natural: MOVE 'UPLOAD DATE AND RECORD COUNT' TO #TBL-KEY-FIELD
        //*  ------------------------------------------
        vw_cwf_Support_Tbl.startDatabaseRead                                                                                                                              //Natural: READ ( 1 ) CWF-SUPPORT-TBL BY TBL-PRIME-KEY STARTING FROM #TBL-PRIME-KEY
        (
        "READ01",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(vw_cwf_Support_Tbl.readNextRow("READ01")))
        {
            //*  --------------- CHECK FOR CONTROL RECORD ERRORS -----------------
            if (condition(cwf_Support_Tbl_Run_Indicator.equals("S") || cwf_Support_Tbl_Run_Indicator.equals("N")))                                                        //Natural: IF CWF-SUPPORT-TBL.RUN-INDICATOR = 'S' OR = 'N'
            {
                pnd_Control_Flag.setValue("Y");                                                                                                                           //Natural: MOVE 'Y' TO #CONTROL-FLAG
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Control_Flag.setValue("N");                                                                                                                           //Natural: MOVE 'N' TO #CONTROL-FLAG
                getReports().write(0, "Run-Indicator on Control record is Wrong - can be 'S'or'N'");                                                                      //Natural: WRITE 'Run-Indicator on Control record is Wrong - can be "S"or"N"'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(11);  if (true) return;                                                                                                                 //Natural: TERMINATE 11
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cwf_Support_Tbl_Run_Indicator.equals("S")))                                                                                                     //Natural: IF CWF-SUPPORT-TBL.RUN-INDICATOR = 'S'
            {
                if (condition(cwf_Support_Tbl_Special_Start_Dte_Tme.equals(getZero()) || cwf_Support_Tbl_Special_End_Dte_Tme.equals(getZero())))                          //Natural: IF CWF-SUPPORT-TBL.SPECIAL-START-DTE-TME = 0 OR CWF-SUPPORT-TBL.SPECIAL-END-DTE-TME = 0
                {
                    getReports().write(0, "Supply Start and End dates for Special Run");                                                                                  //Natural: WRITE 'Supply Start and End dates for Special Run'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    DbsUtil.terminate(12);  if (true) return;                                                                                                             //Natural: TERMINATE 12
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cwf_Support_Tbl_Run_Indicator.equals("S")))                                                                                                     //Natural: IF CWF-SUPPORT-TBL.RUN-INDICATOR = 'S'
            {
                if (condition(cwf_Support_Tbl_Special_End_Dte_Tme.lessOrEqual(cwf_Support_Tbl_Special_Start_Dte_Tme)))                                                    //Natural: IF CWF-SUPPORT-TBL.SPECIAL-END-DTE-TME LE CWF-SUPPORT-TBL.SPECIAL-START-DTE-TME
                {
                    getReports().write(0, "End date cannot be Equal or Less than Starting date");                                                                         //Natural: WRITE 'End date cannot be Equal or Less than Starting date'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    DbsUtil.terminate(13);  if (true) return;                                                                                                             //Natural: TERMINATE 13
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cwf_Support_Tbl_Run_Indicator.equals("N")))                                                                                                     //Natural: IF CWF-SUPPORT-TBL.RUN-INDICATOR = 'N'
            {
                if (condition(cwf_Support_Tbl_Normal_End_Dte_Tme.equals(getZero())))                                                                                      //Natural: IF CWF-SUPPORT-TBL.NORMAL-END-DTE-TME EQ 0
                {
                    getReports().write(0, "End date of 'Normal Run' cannot be Equal to Zero");                                                                            //Natural: WRITE 'End date of "Normal Run" cannot be Equal to Zero'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    DbsUtil.terminate(14);  if (true) return;                                                                                                             //Natural: TERMINATE 14
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  --------------- CONTROL RECORD HAS NO ERRORS --------------------
            if (condition(cwf_Support_Tbl_Run_Indicator.equals("S")))                                                                                                     //Natural: IF CWF-SUPPORT-TBL.RUN-INDICATOR = 'S'
            {
                pnd_Running_Dte_Tme_Str.setValue(cwf_Support_Tbl_Special_Start_Dte_Tme);                                                                                  //Natural: MOVE CWF-SUPPORT-TBL.SPECIAL-START-DTE-TME TO #RUNNING-DTE-TME-STR
                pnd_Running_Dte_Tme_End.setValue(cwf_Support_Tbl_Special_End_Dte_Tme);                                                                                    //Natural: MOVE CWF-SUPPORT-TBL.SPECIAL-END-DTE-TME TO #RUNNING-DTE-TME-END
            }                                                                                                                                                             //Natural: END-IF
            //*  WRITE /'******** Normal Running Dates before adustment *******'
            //*        /'Start Date.............= ' NORMAL-START-DTE-TME
            //*        /'End   Date.............= ' NORMAL-END-DTE-TME
            if (condition(cwf_Support_Tbl_Run_Indicator.equals("N")))                                                                                                     //Natural: IF CWF-SUPPORT-TBL.RUN-INDICATOR = 'N'
            {
                pnd_Date_Time_A.setValue(cwf_Support_Tbl_Normal_End_Dte_Tme);                                                                                             //Natural: MOVE CWF-SUPPORT-TBL.NORMAL-END-DTE-TME TO #DATE-TIME-A
                pnd_Date_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),pnd_Date_Time_A);                                                                         //Natural: MOVE EDITED #DATE-TIME-A TO #DATE-T ( EM = YYYYMMDDHHIISST )
                pnd_Date_T_Result.compute(new ComputeParameters(false, pnd_Date_T_Result), pnd_Date_T.add(1));                                                            //Natural: ASSIGN #DATE-T-RESULT := #DATE-T + 1
                pnd_Date_Time_A.setValueEdited(pnd_Date_T_Result,new ReportEditMask("YYYYMMDDHHIISST"));                                                                  //Natural: MOVE EDITED #DATE-T-RESULT ( EM = YYYYMMDDHHIISST ) TO #DATE-TIME-A
                pnd_Running_Dte_Tme_Str.setValue(pnd_Date_Time_A_Pnd_Date_Time_N);                                                                                        //Natural: MOVE #DATE-TIME-N TO #RUNNING-DTE-TME-STR
                pnd_Curr_Date_N.setValue(Global.getDATN());                                                                                                               //Natural: MOVE *DATN TO #CURR-DATE-N
                pnd_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Curr_Date_N_Pnd_Curr_Date_A);                                                                //Natural: MOVE EDITED #CURR-DATE-A TO #DATE-D ( EM = YYYYMMDD )
                pnd_Date_D.compute(new ComputeParameters(false, pnd_Date_D), (pnd_Date_D.subtract(1)));                                                                   //Natural: COMPUTE #DATE-D = ( #DATE-D - 1 )
                pnd_Running_Dte_Tme_End_Pnd_Running_Date_End.setValueEdited(pnd_Date_D,new ReportEditMask("YYYYMMDD"));                                                   //Natural: MOVE EDITED #DATE-D ( EM = YYYYMMDD ) TO #RUNNING-DATE-END
                pnd_Running_Dte_Tme_End_Pnd_Running_Time_End.setValue(2359599);                                                                                           //Natural: MOVE 2359599 TO #RUNNING-TIME-END
            }                                                                                                                                                             //Natural: END-IF
            pnd_Isn_Control.setValue(vw_cwf_Support_Tbl.getAstISN("Read01"));                                                                                             //Natural: MOVE *ISN TO #ISN-CONTROL
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Start_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),pnd_Running_Dte_Tme_Str);                                                                        //Natural: MOVE EDITED #RUNNING-DTE-TME-STR TO #START-T ( EM = YYYYMMDDHHIISST )
        pnd_End_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),pnd_Running_Dte_Tme_End);                                                                          //Natural: MOVE EDITED #RUNNING-DTE-TME-END TO #END-T ( EM = YYYYMMDDHHIISST )
        if (condition(cwf_Support_Tbl_Run_Indicator.equals("N")))                                                                                                         //Natural: IF CWF-SUPPORT-TBL.RUN-INDICATOR = 'N'
        {
            if (condition(pnd_End_T.lessOrEqual(pnd_Start_T)))                                                                                                            //Natural: IF #END-T LE #START-T
            {
                getReports().write(0, NEWLINE,"********** Error *******",NEWLINE,"This is likely Second Run for Today..Use Special Run",NEWLINE,"End date can't be Equal or Less than Starting date"); //Natural: WRITE /'********** Error *******' /'This is likely Second Run for Today..Use Special Run' /'End date can"t be Equal or Less than Starting date'
                if (Global.isEscape()) return;
                DbsUtil.terminate(15);  if (true) return;                                                                                                                 //Natural: TERMINATE 15
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE /'******** Normal Running Dates after adustment *******'
        //*        /'Start Date.............= ' #START-T(EM=YYYYMMDDHHIISST)
        //*        /'End   Date.............= ' #END-T(EM=YYYYMMDDHHIISST)
        //*      STOP
        //*  READ-RUN-CONTROL-RECORD
    }
    private void sub_Read_Cwf_Image_Xref() throws Exception                                                                                                               //Natural: READ-CWF-IMAGE-XREF
    {
        if (BLNatReinput.isReinput()) return;

        vw_cwf_Image_Xref.startDatabaseRead                                                                                                                               //Natural: READ CWF-IMAGE-XREF BY UPLD-DATE-TIME STARTING FROM #START-T
        (
        "R1",
        new Wc[] { new Wc("UPLD_DATE_TIME", ">=", pnd_Start_T, WcType.BY) },
        new Oc[] { new Oc("UPLD_DATE_TIME", "ASC") }
        );
        R1:
        while (condition(vw_cwf_Image_Xref.readNextRow("R1")))
        {
            getReports().write(0, NEWLINE,"Image record........= ",cwf_Image_Xref_Upld_Date_Time, new ReportEditMask ("YYYYMMDDHHIISST"),cwf_Image_Xref_Source_Id,        //Natural: WRITE /'Image record........= ' UPLD-DATE-TIME ( EM = YYYYMMDDHHIISST ) SOURCE-ID MAIL-ITEM-NBR
                cwf_Image_Xref_Mail_Item_Nbr);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Image_Cntr.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #IMAGE-CNTR
            work_File_1.reset();                                                                                                                                          //Natural: RESET WORK-FILE-1 REPORT-LINE #DOC-FND
            report_Line.reset();
            pnd_Doc_Fnd.reset();
            if (condition(cwf_Image_Xref_Upld_Date_Time.equals(getZero())))                                                                                               //Natural: IF CWF-IMAGE-XREF.UPLD-DATE-TIME = 0
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  END OF THE WINDOW
            if (condition(cwf_Image_Xref_Upld_Date_Time.greater(pnd_End_T)))                                                                                              //Natural: IF CWF-IMAGE-XREF.UPLD-DATE-TIME GT #END-T
            {
                getReports().write(0, NEWLINE,"====================================================",NEWLINE,"Program:",Global.getPROGRAM(),"*** Image-Document Extract ***  Date:", //Natural: WRITE /'====================================================' /'Program:' *PROGRAM '*** Image-Document Extract ***  Date:' *DATU /'Extract was done for the following Dates: ' /'START...................=' #RUNNING-DTE-TME-STR /'END.....................=' #RUNNING-DTE-TME-END /'Number of Images read            = ' #IMAGE-CNTR /'Number of Records with Documents = ' #WF-REC-CNTR /'Number of Records w-o  Documents = ' #DOC-NFND-CNTR /'=================================================='
                    Global.getDATU(),NEWLINE,"Extract was done for the following Dates: ",NEWLINE,"START...................=",pnd_Running_Dte_Tme_Str,NEWLINE,
                    "END.....................=",pnd_Running_Dte_Tme_End,NEWLINE,"Number of Images read            = ",pnd_Image_Cntr,NEWLINE,"Number of Records with Documents = ",
                    pnd_Wf_Rec_Cntr,NEWLINE,"Number of Records w-o  Documents = ",pnd_Doc_Nfnd_Cntr,NEWLINE,"==================================================");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Numpages_Save.setValue(cwf_Image_Xref_Numpages);                                                                                                          //Natural: MOVE NUMPAGES TO #NUMPAGES-SAVE
            pnd_Mail_Item_Nbr_Save.setValue(cwf_Image_Xref_Mail_Item_Nbr);                                                                                                //Natural: MOVE CWF-IMAGE-XREF.MAIL-ITEM-NBR TO #MAIL-ITEM-NBR-SAVE
            pnd_Upld_Date_Time_Save.setValueEdited(cwf_Image_Xref_Upld_Date_Time,new ReportEditMask("YYYYMMDDHHIISST"));                                                  //Natural: MOVE EDITED CWF-IMAGE-XREF.UPLD-DATE-TIME ( EM = YYYYMMDDHHIISST ) TO #UPLD-DATE-TIME-SAVE
            pnd_Source_Id_Save.setValue(cwf_Image_Xref_Source_Id);                                                                                                        //Natural: MOVE CWF-IMAGE-XREF.SOURCE-ID TO #SOURCE-ID-SAVE
            pnd_Isn_Save.setValue(vw_cwf_Image_Xref.getAstISN("R1"));                                                                                                     //Natural: MOVE *ISN ( R1. ) TO #ISN-SAVE
            //*  ------------------- BUILD COMMON KEY -------------------------
            pnd_Common_Key_Pnd_Common_Image_Min.setValue(cwf_Image_Xref_Mail_Item_Nbr);                                                                                   //Natural: MOVE CWF-IMAGE-XREF.MAIL-ITEM-NBR TO #COMMON-IMAGE-MIN
            pnd_Common_Key_Pnd_Common_Image_Source_Id.setValue(cwf_Image_Xref_Source_Id);                                                                                 //Natural: MOVE CWF-IMAGE-XREF.SOURCE-ID TO #COMMON-IMAGE-SOURCE-ID
            //*   WRITE /'Common Key...Post01.= ' #COMMON-KEY #DOC-FND
            //*  ---------------------------------------------------------------
            if (condition(pnd_Doc_Fnd.equals(" ")))                                                                                                                       //Natural: IF #DOC-FND = ' '
            {
                                                                                                                                                                          //Natural: PERFORM EXTRACT-DOC-INFO-FROM-CWF
                sub_Extract_Doc_Info_From_Cwf();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Doc_Fnd.equals(" ")))                                                                                                                       //Natural: IF #DOC-FND = ' '
            {
                                                                                                                                                                          //Natural: PERFORM EXTRACT-DOC-INFO-FROM-ICW
                sub_Extract_Doc_Info_From_Icw();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Doc_Fnd.equals(" ")))                                                                                                                       //Natural: IF #DOC-FND = ' '
            {
                                                                                                                                                                          //Natural: PERFORM EXTRACT-DOC-INFO-FROM-NCW
                sub_Extract_Doc_Info_From_Ncw();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  ---------- WRITE WORK FILE IF DOCUMENT NOT FOUND --------
            if (condition(pnd_Doc_Fnd.equals(" ")))                                                                                                                       //Natural: IF #DOC-FND = ' '
            {
                work_File_1.reset();                                                                                                                                      //Natural: RESET WORK-FILE-1 REPORT-LINE
                report_Line.reset();
                work_File_1_Wf_Dcmnt_File_Flag.setValue("X");                                                                                                             //Natural: MOVE 'X' TO WF-DCMNT-FILE-FLAG
                work_File_1_Wf_Source_Id.setValue(cwf_Image_Xref_Source_Id);                                                                                              //Natural: MOVE CWF-IMAGE-XREF.SOURCE-ID TO WF-SOURCE-ID
                work_File_1_Wf_Mail_Item_Nbr.setValue(cwf_Image_Xref_Mail_Item_Nbr);                                                                                      //Natural: MOVE CWF-IMAGE-XREF.MAIL-ITEM-NBR TO WF-MAIL-ITEM-NBR
                work_File_1_Wf_Upld_Date_Time.setValueEdited(cwf_Image_Xref_Upld_Date_Time,new ReportEditMask("YYYYMMDDHHIISST"));                                        //Natural: MOVE EDITED CWF-IMAGE-XREF.UPLD-DATE-TIME ( EM = YYYYMMDDHHIISST ) TO WF-UPLD-DATE-TIME
                                                                                                                                                                          //Natural: PERFORM WRITE-WF-RECORD
                sub_Write_Wf_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Doc_Nfnd_Cntr.nadd(1);                                                                                                                                //Natural: ADD 1 TO #DOC-NFND-CNTR
                //*   WRITE /'Doc Nfnd  ...Post02.= ' #COMMON-KEY
                //*       PERFORM WRITE-REPORT
            }                                                                                                                                                             //Natural: END-IF
            //*  READ CWF-IMAGE-XREF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Extract_Doc_Info_From_Cwf() throws Exception                                                                                                         //Natural: EXTRACT-DOC-INFO-FROM-CWF
    {
        if (BLNatReinput.isReinput()) return;

        //*  =========================================
        pnd_Wpid_Split.reset();                                                                                                                                           //Natural: RESET #WPID-SPLIT #REPORTED-CASE-RLDT ( * ) #I #DOC-FND
        pnd_Reported_Case_Rldt.getValue("*").reset();
        pnd_I.reset();
        pnd_Doc_Fnd.reset();
        vw_cwf_Efm_Document.startDatabaseRead                                                                                                                             //Natural: READ ( 1 ) CWF-EFM-DOCUMENT BY IMAGE-POINTER-KEY STARTING FROM #COMMON-KEY
        (
        "DOCCWF",
        new Wc[] { new Wc("IMAGE_POINTER_KEY", ">=", pnd_Common_Key, WcType.BY) },
        new Oc[] { new Oc("IMAGE_POINTER_KEY", "ASC") },
        1
        );
        DOCCWF:
        while (condition(vw_cwf_Efm_Document.readNextRow("DOCCWF")))
        {
            //*  ----------------- RECORD EVALUATION ------------------------
            pnd_Key_Compare_Pnd_Key_Compare_Source_Id.setValue(cwf_Efm_Document_Image_Source_Id);                                                                         //Natural: MOVE CWF-EFM-DOCUMENT.IMAGE-SOURCE-ID TO #KEY-COMPARE-SOURCE-ID
            pnd_Key_Compare_Pnd_Key_Compare_Min.setValue(cwf_Efm_Document_Image_Min);                                                                                     //Natural: MOVE CWF-EFM-DOCUMENT.IMAGE-MIN TO #KEY-COMPARE-MIN #MIN-SPLIT
            pnd_Min_Split.setValue(cwf_Efm_Document_Image_Min);
            if (condition(pnd_Key_Compare.notEquals(pnd_Common_Key)))                                                                                                     //Natural: IF #KEY-COMPARE NE #COMMON-KEY
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*  ------------------------------------------------------------
            pnd_Doc_Fnd.setValue("Y");                                                                                                                                    //Natural: MOVE 'Y' TO #DOC-FND
            work_File_1_Wf_Dcmnt_File_Flag.setValue("P");                                                                                                                 //Natural: MOVE 'P' TO WF-DCMNT-FILE-FLAG
            report_Line_Wr_System.setValue("CWF");                                                                                                                        //Natural: MOVE 'CWF' TO WR-SYSTEM
            work_File_1_Wf_Cabinet_Id.setValue(cwf_Efm_Document_Cabinet_Id);                                                                                              //Natural: MOVE CWF-EFM-DOCUMENT.CABINET-ID TO WF-CABINET-ID
            work_File_1_Wf_Crte_Dte_Tme.setValueEdited(cwf_Efm_Document_Entry_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                             //Natural: MOVE EDITED CWF-EFM-DOCUMENT.ENTRY-DTE-TME ( EM = YYYYMMDDHHIISST ) TO WF-CRTE-DTE-TME
            work_File_1_Wf_Crte_Empl_Racf_Id.setValue(cwf_Efm_Document_Entry_Empl_Racf_Id);                                                                               //Natural: MOVE CWF-EFM-DOCUMENT.ENTRY-EMPL-RACF-ID TO WF-CRTE-EMPL-RACF-ID
            work_File_1_Wf_Dcmnt_Category.setValue(cwf_Efm_Document_Document_Category);                                                                                   //Natural: MOVE CWF-EFM-DOCUMENT.DOCUMENT-CATEGORY TO WF-DCMNT-CATEGORY
            work_File_1_Wf_Dcmnt_Sub_Category.setValue(cwf_Efm_Document_Document_Sub_Category);                                                                           //Natural: MOVE CWF-EFM-DOCUMENT.DOCUMENT-SUB-CATEGORY TO WF-DCMNT-SUB-CATEGORY
            work_File_1_Wf_Dcmnt_Detail.setValue(cwf_Efm_Document_Document_Detail);                                                                                       //Natural: MOVE CWF-EFM-DOCUMENT.DOCUMENT-DETAIL TO WF-DCMNT-DETAIL
            work_File_1_Wf_Dcmnt_Copy_Ind.setValue(cwf_Efm_Document_Copy_Ind);                                                                                            //Natural: MOVE CWF-EFM-DOCUMENT.COPY-IND TO WF-DCMNT-COPY-IND
            work_File_1_Wf_Rqst_Work_Prcss_Id.setValue(cwf_Efm_Document_Sub_Rqst_Work_Prcss_Id);                                                                          //Natural: MOVE CWF-EFM-DOCUMENT.SUB-RQST-WORK-PRCSS-ID TO WF-RQST-WORK-PRCSS-ID
            if (condition(cwf_Efm_Document_Image_Source_Id.equals("POST") || cwf_Efm_Document_Image_Source_Id.equals("PCPOST")))                                          //Natural: IF CWF-EFM-DOCUMENT.IMAGE-SOURCE-ID = 'POST' OR = 'PCPOST'
            {
                work_File_1_Wf_Nbr_Of_Pages.setValue(pnd_Numpages_Save);                                                                                                  //Natural: MOVE #NUMPAGES-SAVE TO WF-NBR-OF-PAGES
                getReports().write(0, NEWLINE,"POST01........",work_File_1_Wf_Nbr_Of_Pages);                                                                              //Natural: WRITE / 'POST01........' WF-NBR-OF-PAGES
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("DOCCWF"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("DOCCWF"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Number_Of_Pages.compute(new ComputeParameters(false, pnd_Number_Of_Pages), (cwf_Efm_Document_Dgtze_Doc_End_Page.subtract(pnd_Min_Split_Pnd_Min_Split_3).add(1))); //Natural: COMPUTE #NUMBER-OF-PAGES = ( CWF-EFM-DOCUMENT.DGTZE-DOC-END-PAGE - #MIN-SPLIT-3 + 1 )
                //*  03/28/03 L.E.
                if (condition(pnd_Number_Of_Pages.less(getZero())))                                                                                                       //Natural: IF #NUMBER-OF-PAGES LT 0
                {
                    pnd_Number_Of_Pages.setValue(0);                                                                                                                      //Natural: ASSIGN #NUMBER-OF-PAGES := 0
                }                                                                                                                                                         //Natural: END-IF
                work_File_1_Wf_Nbr_Of_Pages.setValue(pnd_Number_Of_Pages);                                                                                                //Natural: MOVE #NUMBER-OF-PAGES TO WF-NBR-OF-PAGES
            }                                                                                                                                                             //Natural: END-IF
            //*  ----------------- BUILD KEY FOR EMPL-ORG-TABLE ---------------------
            pnd_Unit_Empl_Racf_Id_Key_Pnd_Empl_Unit_Cde.setValue(cwf_Efm_Document_Entry_System_Or_Unit);                                                                  //Natural: MOVE CWF-EFM-DOCUMENT.ENTRY-SYSTEM-OR-UNIT TO #UNIT-EMPL-RACF-ID-KEY.#EMPL-UNIT-CDE
            pnd_Unit_Empl_Racf_Id_Key_Pnd_Empl_Racf_Id.setValue(cwf_Efm_Document_Entry_Empl_Racf_Id);                                                                     //Natural: MOVE CWF-EFM-DOCUMENT.ENTRY-EMPL-RACF-ID TO #UNIT-EMPL-RACF-ID-KEY.#EMPL-RACF-ID
                                                                                                                                                                          //Natural: PERFORM GET-EMPLOYEE-INFO
            sub_Get_Employee_Info();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("DOCCWF"))) break;
                else if (condition(Global.isEscapeBottomImmediate("DOCCWF"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ------------------------------------------------------------------
            pnd_Wpid_Split.setValue(cwf_Efm_Document_Sub_Rqst_Work_Prcss_Id);                                                                                             //Natural: MOVE CWF-EFM-DOCUMENT.SUB-RQST-WORK-PRCSS-ID TO #WPID-SPLIT
            //*  ------------- BUILD KEY FOR CABINET ---------------------
            pnd_Active_Cabinet_Key_Pnd_Cabinet_Id.setValue(cwf_Efm_Document_Cabinet_Id);                                                                                  //Natural: MOVE CWF-EFM-DOCUMENT.CABINET-ID TO #ACTIVE-CABINET-KEY.#CABINET-ID
            //*  ---------------------------------------------------------
            if (condition(pnd_Wpid_Split_Pnd_Wpid_Split_2.equals("ZZ")))                                                                                                  //Natural: IF #WPID-SPLIT-2 = 'ZZ'
            {
                vw_cwf_Efm_Cabinet.startDatabaseRead                                                                                                                      //Natural: READ ( 1 ) CWF-EFM-CABINET WITH ACTIVE-CABINET-KEY = #ACTIVE-CABINET-KEY
                (
                "RCAB",
                new Wc[] { new Wc("ACTIVE_CABINET_KEY", ">=", pnd_Active_Cabinet_Key, WcType.BY) },
                new Oc[] { new Oc("ACTIVE_CABINET_KEY", "ASC") },
                1
                );
                RCAB:
                while (condition(vw_cwf_Efm_Cabinet.readNextRow("RCAB")))
                {
                    work_File_1_Wf_Mj_Rqst_Log_Dte_Tme.setValue(cwf_Efm_Cabinet_Dgtze_Rqst_Log_Dte_Tme);                                                                  //Natural: MOVE CWF-EFM-CABINET.DGTZE-RQST-LOG-DTE-TME TO WF-MJ-RQST-LOG-DTE-TME
                    //*  CWF-EFM-CABINET
                }                                                                                                                                                         //Natural: END-READ
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("DOCCWF"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("DOCCWF"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  -------------- BUILD UNIQUE ACTV. MIT ------------------
            pnd_Actv_Unque_Key.reset();                                                                                                                                   //Natural: RESET #ACTV-UNQUE-KEY
            pnd_Actv_Unque_Key_Pnd_Rqst_Log_Dte_Tme.setValue(cwf_Efm_Cabinet_Dgtze_Rqst_Log_Dte_Tme);                                                                     //Natural: MOVE CWF-EFM-CABINET.DGTZE-RQST-LOG-DTE-TME TO #ACTV-UNQUE-KEY.#RQST-LOG-DTE-TME
            //*  -------------- BUILD RQST-ID-KEY FOR MIT ------------------
            pnd_Rqst_Id_Key.reset();                                                                                                                                      //Natural: RESET #RQST-ID-KEY
            pnd_Rqst_Id_Key_Pnd_Rqst_Wpid.setValue(cwf_Efm_Document_Work_Prcss_Id);                                                                                       //Natural: MOVE CWF-EFM-DOCUMENT.WORK-PRCSS-ID TO #RQST-ID-KEY.#RQST-WPID
            pnd_Rqst_Id_Key_Pnd_Rqst_Rcvd_Dte.setValueEdited(cwf_Efm_Document_Tiaa_Rcvd_Dte,new ReportEditMask("YYYYMMDD"));                                              //Natural: MOVE EDITED CWF-EFM-DOCUMENT.TIAA-RCVD-DTE ( EM = YYYYMMDD ) TO #RQST-RCVD-DTE
            pnd_Rqst_Id_Key_Pnd_Rqst_Case_1.setValue(cwf_Efm_Document_Case_Id_Cde);                                                                                       //Natural: MOVE CWF-EFM-DOCUMENT.CASE-ID-CDE TO #RQST-CASE-1
            pnd_Rqst_Id_Key_Pnd_Rqst_Case_2.setValue(cwf_Efm_Document_Sub_Rqst_Ind);                                                                                      //Natural: MOVE CWF-EFM-DOCUMENT.SUB-RQST-IND TO #RQST-CASE-2
            pnd_Rqst_Id_Key_Pnd_Rqst_Pin_Nbr.setValue(cwf_Efm_Document_Pin_Nbr);                                                                                          //Natural: MOVE CWF-EFM-DOCUMENT.PIN-NBR TO #RQST-PIN-NBR
            pnd_Rqst_Id_Key_Pnd_Multi_Rqst_Ind.setValue(cwf_Efm_Document_Multi_Subrqst);                                                                                  //Natural: MOVE CWF-EFM-DOCUMENT.MULTI-SUBRQST TO #MULTI-RQST-IND
            //*  ----------------------------------------------------------------
            if (condition(pnd_Wpid_Split_Pnd_Wpid_Split_2.notEquals("ZZ")))                                                                                               //Natural: IF #WPID-SPLIT-2 NE 'ZZ'
            {
                                                                                                                                                                          //Natural: PERFORM MIT-NO-ZZ-READ
                sub_Mit_No_Zz_Read();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("DOCCWF"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("DOCCWF"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM MIT-WITH-ZZ-READ
                sub_Mit_With_Zz_Read();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("DOCCWF"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("DOCCWF"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pnd_Doc_Fnd.setValue("Y");                                                                                                                                    //Natural: ASSIGN #DOC-FND := 'Y'
            //*  CWF-EFM-DOCUMENT
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* (5960)
    }
    private void sub_Mit_No_Zz_Read() throws Exception                                                                                                                    //Natural: MIT-NO-ZZ-READ
    {
        if (BLNatReinput.isReinput()) return;

        vw_cwf_Master_Index_View.startDatabaseFind                                                                                                                        //Natural: FIND ( 1 ) CWF-MASTER-INDEX-VIEW WITH RQST-ID-KEY = #RQST-ID-KEY
        (
        "R_CWF",
        new Wc[] { new Wc("RQST_ID_KEY", "=", pnd_Rqst_Id_Key, WcType.WITH) },
        1
        );
        R_CWF:
        while (condition(vw_cwf_Master_Index_View.readNextRow("R_CWF", true)))
        {
            vw_cwf_Master_Index_View.setIfNotFoundControlFlag(false);
            if (condition(vw_cwf_Master_Index_View.getAstCOUNTER().equals(0)))                                                                                            //Natural: IF NO RECORD FOUND
            {
                pnd_Folder_Key_Pnd_Cabinet_Id.setValue(cwf_Efm_Document_Cabinet_Id);                                                                                      //Natural: MOVE CWF-EFM-DOCUMENT.CABINET-ID TO #FOLDER-KEY.#CABINET-ID
                pnd_Folder_Key_Pnd_Tiaa_Rcvd_Dte.setValue(cwf_Efm_Document_Tiaa_Rcvd_Dte);                                                                                //Natural: MOVE CWF-EFM-DOCUMENT.TIAA-RCVD-DTE TO #FOLDER-KEY.#TIAA-RCVD-DTE
                                                                                                                                                                          //Natural: PERFORM ACCESS-FOLDER
                sub_Access_Folder();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R_CWF"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R_CWF"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  ---------------
        getReports().write(0, NEWLINE,"MIT-1 was Found...RQST-KEY = ",pnd_Rqst_Id_Key);                                                                                   //Natural: WRITE /'MIT-1 was Found...RQST-KEY = ' #RQST-ID-KEY
        if (Global.isEscape()) return;
        work_File_1_Wf_Crte_Dte_Tme.setValue(cwf_Master_Index_View_Rqst_Log_Dte_Tme);                                                                                     //Natural: MOVE CWF-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME TO WF-CRTE-DTE-TME
                                                                                                                                                                          //Natural: PERFORM WRITE-WF-RECORD
        sub_Write_Wf_Record();
        if (condition(Global.isEscape())) {return;}
        //*    PERFORM WRITE-REPORT
        pnd_Mit_Write_Flag.setValue("Y");                                                                                                                                 //Natural: MOVE 'Y' TO #MIT-WRITE-FLAG
    }
    private void sub_Mit_With_Zz_Read() throws Exception                                                                                                                  //Natural: MIT-WITH-ZZ-READ
    {
        if (BLNatReinput.isReinput()) return;

        vw_cwf_Master_Index_View.startDatabaseRead                                                                                                                        //Natural: READ ( 1 ) CWF-MASTER-INDEX-VIEW BY ACTV-UNQUE-KEY STARTING FROM #ACTV-UNQUE-KEY
        (
        "RCWF",
        new Wc[] { new Wc("ACTV_UNQUE_KEY", ">=", pnd_Actv_Unque_Key, WcType.BY) },
        new Oc[] { new Oc("ACTV_UNQUE_KEY", "ASC") },
        1
        );
        RCWF:
        while (condition(vw_cwf_Master_Index_View.readNextRow("RCWF")))
        {
            if (condition(cwf_Master_Index_View_Rqst_Log_Dte_Tme.notEquals(pnd_Actv_Unque_Key_Pnd_Rqst_Log_Dte_Tme)))                                                     //Natural: IF CWF-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME NE #ACTV-UNQUE-KEY.#RQST-LOG-DTE-TME
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            work_File_1_Wf_Crte_Dte_Tme.setValueEdited(cwf_Efm_Document_Entry_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                             //Natural: MOVE EDITED CWF-EFM-DOCUMENT.ENTRY-DTE-TME ( EM = YYYYMMDDHHIISST ) TO WF-CRTE-DTE-TME
            work_File_1_Wf_Mj_Work_Prcss_Id.setValue(cwf_Master_Index_View_Work_Prcss_Id);                                                                                //Natural: MOVE CWF-MASTER-INDEX-VIEW.WORK-PRCSS-ID TO WF-MJ-WORK-PRCSS-ID
                                                                                                                                                                          //Natural: PERFORM WRITE-WF-RECORD
            sub_Write_Wf_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RCWF"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RCWF"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*        PERFORM WRITE-REPORT
            pnd_Mit_Write_Flag.setValue("Y");                                                                                                                             //Natural: MOVE 'Y' TO #MIT-WRITE-FLAG
            //*  MIT
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Access_Folder() throws Exception                                                                                                                     //Natural: ACCESS-FOLDER
    {
        if (BLNatReinput.isReinput()) return;

        vw_cwf_Efm_Folder.startDatabaseRead                                                                                                                               //Natural: READ ( 1 ) CWF-EFM-FOLDER BY FOLDER-KEY STARTING FROM #FOLDER-KEY
        (
        "R_FLD",
        new Wc[] { new Wc("FOLDER_KEY", ">=", pnd_Folder_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("FOLDER_KEY", "ASC") },
        1
        );
        R_FLD:
        while (condition(vw_cwf_Efm_Folder.readNextRow("R_FLD")))
        {
            getReports().write(0, NEWLINE,"Reading Folder......f-key=",pnd_Folder_Key_Pnd_Cabinet_Id,pnd_Folder_Key_Pnd_Tiaa_Rcvd_Dte, new ReportEditMask                 //Natural: WRITE / 'Reading Folder......f-key=' #FOLDER-KEY.#CABINET-ID #FOLDER-KEY.#TIAA-RCVD-DTE ( EM = YYYYMMDD )
                ("YYYYMMDD"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R_FLD"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R_FLD"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(cwf_Efm_Folder_Cabinet_Id.equals(pnd_Folder_Key_Pnd_Cabinet_Id) && cwf_Efm_Folder_Tiaa_Rcvd_Dte.equals(pnd_Folder_Key_Pnd_Tiaa_Rcvd_Dte)))      //Natural: IF CWF-EFM-FOLDER.CABINET-ID EQ #FOLDER-KEY.#CABINET-ID AND CWF-EFM-FOLDER.TIAA-RCVD-DTE EQ #FOLDER-KEY.#TIAA-RCVD-DTE
            {
                getReports().write(0, NEWLINE,"Folder found  ...........=",cwf_Efm_Folder_Cabinet_Id,cwf_Efm_Folder_Tiaa_Rcvd_Dte, new ReportEditMask                     //Natural: WRITE / 'Folder found  ...........=' CABINET-ID TIAA-RCVD-DTE ( EM = YYYYMMDD )
                    ("YYYYMMDD"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R_FLD"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R_FLD"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*   ---------- POPULATE FOLDER-NAME-KEY (SUB-REQUEST) ---------------
                pnd_Folder_Name_Key.reset();                                                                                                                              //Natural: RESET #FOLDER-NAME-KEY
                pnd_Folder_Name_Key_Pnd_Wpid.setValue(cwf_Efm_Folder_Wpid);                                                                                               //Natural: MOVE CWF-EFM-FOLDER.WPID TO #FOLDER-NAME-KEY.#WPID
                pnd_Folder_Name_Key_Pnd_Tiaa_Rcvd_Dte.setValueEdited(cwf_Efm_Folder_Tiaa_Rcvd_Dte,new ReportEditMask("YYYYMMDD"));                                        //Natural: MOVE EDITED CWF-EFM-FOLDER.TIAA-RCVD-DTE ( EM = YYYYMMDD ) TO #FOLDER-NAME-KEY.#TIAA-RCVD-DTE
                pnd_Folder_Name_Key_Pnd_Case_Id_1.setValue(cwf_Efm_Folder_Case_1);                                                                                        //Natural: MOVE CWF-EFM-FOLDER.CASE-1 TO #FOLDER-NAME-KEY.#CASE-ID-1
                pnd_Folder_Name_Key_Pnd_Case_Id_2.setValue(cwf_Efm_Folder_Case_9);                                                                                        //Natural: MOVE CWF-EFM-FOLDER.CASE-9 TO #FOLDER-NAME-KEY.#CASE-ID-2
                pnd_Folder_Name_Key_Pnd_Pin_Nbr.setValue(cwf_Efm_Folder_Cabinet_Id_Pin);                                                                                  //Natural: MOVE CWF-EFM-FOLDER.CABINET-ID-PIN TO #FOLDER-NAME-KEY.#PIN-NBR
                vw_cwf_Master_Index_View.startDatabaseFind                                                                                                                //Natural: FIND ( 1 ) CWF-MASTER-INDEX-VIEW WITH FOLDER-NAME-KEY = #FOLDER-NAME-KEY
                (
                "R_MIT",
                new Wc[] { new Wc("FOLDER_NAME_KEY", "=", pnd_Folder_Name_Key, WcType.WITH) },
                1
                );
                R_MIT:
                while (condition(vw_cwf_Master_Index_View.readNextRow("R_MIT", true)))
                {
                    vw_cwf_Master_Index_View.setIfNotFoundControlFlag(false);
                    if (condition(vw_cwf_Master_Index_View.getAstCOUNTER().equals(0)))                                                                                    //Natural: IF NO RECORD FOUND
                    {
                        getReports().write(0, NEWLINE,"MIT-2 rec Not found.....",pnd_Folder_Name_Key);                                                                    //Natural: WRITE /'MIT-2 rec Not found.....' #FOLDER-NAME-KEY
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R_MIT"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R_MIT"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-NOREC
                    getReports().write(0, NEWLINE,"Reading MIT-2......key = ",pnd_Folder_Name_Key,NEWLINE,"CWF-MIT RLDT.= ",cwf_Master_Index_View_Rqst_Log_Dte_Tme);      //Natural: WRITE /'Reading MIT-2......key = ' #FOLDER-NAME-KEY /'CWF-MIT RLDT.= ' CWF-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R_MIT"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R_MIT"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    work_File_1_Wf_Crte_Dte_Tme.setValue(cwf_Master_Index_View_Rqst_Log_Dte_Tme);                                                                         //Natural: MOVE CWF-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME TO WF-CRTE-DTE-TME
                                                                                                                                                                          //Natural: PERFORM WRITE-WF-RECORD
                    sub_Write_Wf_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R_MIT"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R_MIT"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*    PERFORM WRITE-REPORT
                    pnd_Mit_Write_Flag.setValue("Y");                                                                                                                     //Natural: MOVE 'Y' TO #MIT-WRITE-FLAG
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R_FLD"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R_FLD"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Extract_Doc_Info_From_Icw() throws Exception                                                                                                         //Natural: EXTRACT-DOC-INFO-FROM-ICW
    {
        if (BLNatReinput.isReinput()) return;

        vw_icw_Efm_Document.startDatabaseRead                                                                                                                             //Natural: READ ( 1 ) ICW-EFM-DOCUMENT BY IMAGE-POINTER-KEY STARTING FROM #COMMON-KEY
        (
        "DOCICW",
        new Wc[] { new Wc("IMAGE_POINTER_KEY", ">=", pnd_Common_Key, WcType.BY) },
        new Oc[] { new Oc("IMAGE_POINTER_KEY", "ASC") },
        1
        );
        DOCICW:
        while (condition(vw_icw_Efm_Document.readNextRow("DOCICW")))
        {
            //*  ----------------- RECORD EVALUATION ------------------------
            pnd_Key_Compare_Pnd_Key_Compare_Source_Id.setValue(icw_Efm_Document_Image_Source_Id);                                                                         //Natural: MOVE ICW-EFM-DOCUMENT.IMAGE-SOURCE-ID TO #KEY-COMPARE-SOURCE-ID
            pnd_Key_Compare_Pnd_Key_Compare_Min.setValue(icw_Efm_Document_Image_Min);                                                                                     //Natural: MOVE ICW-EFM-DOCUMENT.IMAGE-MIN TO #KEY-COMPARE-MIN #MIN-SPLIT
            pnd_Min_Split.setValue(icw_Efm_Document_Image_Min);
            if (condition(pnd_Key_Compare.notEquals(pnd_Common_Key)))                                                                                                     //Natural: IF #KEY-COMPARE NE #COMMON-KEY
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*  ------------------------------------------------------------
            pnd_Doc_Fnd.setValue("Y");                                                                                                                                    //Natural: MOVE 'Y' TO #DOC-FND
            work_File_1_Wf_Dcmnt_File_Flag.setValue("I");                                                                                                                 //Natural: MOVE 'I' TO WF-DCMNT-FILE-FLAG
            report_Line_Wr_System.setValue("ICW");                                                                                                                        //Natural: MOVE 'ICW' TO WR-SYSTEM
            work_File_1_Wf_Cabinet_Id.setValue(icw_Efm_Document_Cabinet_Id);                                                                                              //Natural: MOVE ICW-EFM-DOCUMENT.CABINET-ID TO WF-CABINET-ID
            //*  MOVE EDITED ICW-EFM-DOCUMENT.CRTE-DTE-TME(EM=YYYYMMDDHHIISST)
            //*                                           TO WF-CRTE-DTE-TME
            work_File_1_Wf_Crte_Empl_Racf_Id.setValue(icw_Efm_Document_Crte_Empl_Racf_Id);                                                                                //Natural: MOVE ICW-EFM-DOCUMENT.CRTE-EMPL-RACF-ID TO WF-CRTE-EMPL-RACF-ID
            work_File_1_Wf_Dcmnt_Category.setValue(icw_Efm_Document_Dcmnt_Ctgry);                                                                                         //Natural: MOVE ICW-EFM-DOCUMENT.DCMNT-CTGRY TO WF-DCMNT-CATEGORY
            work_File_1_Wf_Dcmnt_Sub_Category.setValue(icw_Efm_Document_Dcmnt_Sub_Ctgry);                                                                                 //Natural: MOVE ICW-EFM-DOCUMENT.DCMNT-SUB-CTGRY TO WF-DCMNT-SUB-CATEGORY
            work_File_1_Wf_Dcmnt_Detail.setValue(icw_Efm_Document_Dcmnt_Dtl);                                                                                             //Natural: MOVE ICW-EFM-DOCUMENT.DCMNT-DTL TO WF-DCMNT-DETAIL
            work_File_1_Wf_Dcmnt_Copy_Ind.setValue(icw_Efm_Document_Dcmnt_Copy_Ind);                                                                                      //Natural: MOVE ICW-EFM-DOCUMENT.DCMNT-COPY-IND TO WF-DCMNT-COPY-IND
            pnd_Number_Of_Pages.compute(new ComputeParameters(false, pnd_Number_Of_Pages), (icw_Efm_Document_Image_End_Page.subtract(pnd_Min_Split_Pnd_Min_Split_3).add(1))); //Natural: COMPUTE #NUMBER-OF-PAGES = ( ICW-EFM-DOCUMENT.IMAGE-END-PAGE - #MIN-SPLIT-3 + 1 )
            work_File_1_Wf_Nbr_Of_Pages.setValue(pnd_Number_Of_Pages);                                                                                                    //Natural: MOVE #NUMBER-OF-PAGES TO WF-NBR-OF-PAGES
            //*  ----------------- BUILD KEY FOR EMPL-ORG-TABLE ---------------------
            pnd_Unit_Empl_Racf_Id_Key_Pnd_Empl_Unit_Cde.setValue(icw_Efm_Document_Crte_Unit);                                                                             //Natural: MOVE ICW-EFM-DOCUMENT.CRTE-UNIT TO #UNIT-EMPL-RACF-ID-KEY.#EMPL-UNIT-CDE
            pnd_Unit_Empl_Racf_Id_Key_Pnd_Empl_Racf_Id.setValue(icw_Efm_Document_Crte_Empl_Racf_Id);                                                                      //Natural: MOVE ICW-EFM-DOCUMENT.CRTE-EMPL-RACF-ID TO #UNIT-EMPL-RACF-ID-KEY.#EMPL-RACF-ID
                                                                                                                                                                          //Natural: PERFORM GET-EMPLOYEE-INFO
            sub_Get_Employee_Info();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("DOCICW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("DOCICW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ----------------- BUILD KEY FOR ICW-MASTER-INDEX -------------------
            pnd_Actv_Unque_Key_Pnd_Rqst_Log_Dte_Tme.setValue(icw_Efm_Document_Rqst_Log_Dte_Tme);                                                                          //Natural: MOVE ICW-EFM-DOCUMENT.RQST-LOG-DTE-TME TO #ACTV-UNQUE-KEY.#RQST-LOG-DTE-TME
            vw_icw_Master_Index.startDatabaseRead                                                                                                                         //Natural: READ ICW-MASTER-INDEX WITH ACTV-UNQUE-KEY = #ACTV-UNQUE-KEY
            (
            "R_ICW",
            new Wc[] { new Wc("ACTV_UNQUE_KEY", ">=", pnd_Actv_Unque_Key, WcType.BY) },
            new Oc[] { new Oc("ACTV_UNQUE_KEY", "ASC") }
            );
            R_ICW:
            while (condition(vw_icw_Master_Index.readNextRow("R_ICW")))
            {
                if (condition(icw_Master_Index_Rqst_Log_Dte_Tme.notEquals(pnd_Actv_Unque_Key_Pnd_Rqst_Log_Dte_Tme)))                                                      //Natural: IF ICW-MASTER-INDEX.RQST-LOG-DTE-TME NE #ACTV-UNQUE-KEY.#RQST-LOG-DTE-TME
                {
                    if (true) break R_ICW;                                                                                                                                //Natural: ESCAPE BOTTOM ( R-ICW. ) IMMEDIATE
                }                                                                                                                                                         //Natural: END-IF
                work_File_1_Wf_Rqst_Work_Prcss_Id.setValue(icw_Master_Index_Work_Prcss_Id);                                                                               //Natural: MOVE ICW-MASTER-INDEX.WORK-PRCSS-ID TO WF-RQST-WORK-PRCSS-ID
                work_File_1_Wf_Crte_Dte_Tme.setValue(icw_Master_Index_Rqst_Log_Dte_Tme);                                                                                  //Natural: MOVE ICW-MASTER-INDEX.RQST-LOG-DTE-TME TO WF-CRTE-DTE-TME
                //*  ICW
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("DOCICW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("DOCICW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-WF-RECORD
            sub_Write_Wf_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("DOCICW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("DOCICW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*        PERFORM WRITE-REPORT
            //*  ICW-EFM-DOCUMENT
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Doc_Fnd.setValue("Y");                                                                                                                                        //Natural: ASSIGN #DOC-FND := 'Y'
        //* (5960)
    }
    private void sub_Extract_Doc_Info_From_Ncw() throws Exception                                                                                                         //Natural: EXTRACT-DOC-INFO-FROM-NCW
    {
        if (BLNatReinput.isReinput()) return;

        vw_ncw_Efm_Document.startDatabaseRead                                                                                                                             //Natural: READ ( 1 ) NCW-EFM-DOCUMENT BY IMAGE-POINTER-KEY STARTING FROM #COMMON-KEY
        (
        "DOCNCW",
        new Wc[] { new Wc("IMAGE_POINTER_KEY", ">=", pnd_Common_Key, WcType.BY) },
        new Oc[] { new Oc("IMAGE_POINTER_KEY", "ASC") },
        1
        );
        DOCNCW:
        while (condition(vw_ncw_Efm_Document.readNextRow("DOCNCW")))
        {
            //*  ----------------- RECORD EVALUATION ------------------------
            pnd_Key_Compare_Pnd_Key_Compare_Source_Id.setValue(ncw_Efm_Document_Image_Source_Id);                                                                         //Natural: MOVE NCW-EFM-DOCUMENT.IMAGE-SOURCE-ID TO #KEY-COMPARE-SOURCE-ID
            pnd_Key_Compare_Pnd_Key_Compare_Min.setValue(ncw_Efm_Document_Image_Min);                                                                                     //Natural: MOVE NCW-EFM-DOCUMENT.IMAGE-MIN TO #KEY-COMPARE-MIN #MIN-SPLIT
            pnd_Min_Split.setValue(ncw_Efm_Document_Image_Min);
            if (condition(pnd_Key_Compare.notEquals(pnd_Common_Key)))                                                                                                     //Natural: IF #KEY-COMPARE NE #COMMON-KEY
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*  ------------------------------------------------------------
            pnd_Doc_Fnd.setValue("Y");                                                                                                                                    //Natural: MOVE 'Y' TO #DOC-FND
            work_File_1_Wf_Dcmnt_File_Flag.setValue("N");                                                                                                                 //Natural: MOVE 'N' TO WF-DCMNT-FILE-FLAG
            report_Line_Wr_System.setValue("NCW");                                                                                                                        //Natural: MOVE 'NCW' TO WR-SYSTEM
            work_File_1_Wf_Cabinet_Id.setValue(ncw_Efm_Document_Np_Pin);                                                                                                  //Natural: MOVE NCW-EFM-DOCUMENT.NP-PIN TO WF-CABINET-ID
            //*  MOVE EDITED NCW-EFM-DOCUMENT.CRTE-DTE-TME(EM=YYYYMMDDHHIISST)
            //*                                           TO WF-CRTE-DTE-TME
            work_File_1_Wf_Crte_Empl_Racf_Id.setValue(ncw_Efm_Document_Crte_Empl_Racf_Id);                                                                                //Natural: MOVE NCW-EFM-DOCUMENT.CRTE-EMPL-RACF-ID TO WF-CRTE-EMPL-RACF-ID
            work_File_1_Wf_Dcmnt_Category.setValue(ncw_Efm_Document_Dcmnt_Ctgry);                                                                                         //Natural: MOVE NCW-EFM-DOCUMENT.DCMNT-CTGRY TO WF-DCMNT-CATEGORY
            work_File_1_Wf_Dcmnt_Sub_Category.setValue(ncw_Efm_Document_Dcmnt_Sub_Ctgry);                                                                                 //Natural: MOVE NCW-EFM-DOCUMENT.DCMNT-SUB-CTGRY TO WF-DCMNT-SUB-CATEGORY
            work_File_1_Wf_Dcmnt_Detail.setValue(ncw_Efm_Document_Dcmnt_Dtl);                                                                                             //Natural: MOVE NCW-EFM-DOCUMENT.DCMNT-DTL TO WF-DCMNT-DETAIL
            work_File_1_Wf_Dcmnt_Copy_Ind.setValue(ncw_Efm_Document_Dcmnt_Copy_Ind);                                                                                      //Natural: MOVE NCW-EFM-DOCUMENT.DCMNT-COPY-IND TO WF-DCMNT-COPY-IND
            pnd_Number_Of_Pages.compute(new ComputeParameters(false, pnd_Number_Of_Pages), (ncw_Efm_Document_Image_End_Page.subtract(pnd_Min_Split_Pnd_Min_Split_3).add(1))); //Natural: COMPUTE #NUMBER-OF-PAGES = ( NCW-EFM-DOCUMENT.IMAGE-END-PAGE - #MIN-SPLIT-3 + 1 )
            work_File_1_Wf_Nbr_Of_Pages.setValue(pnd_Number_Of_Pages);                                                                                                    //Natural: MOVE #NUMBER-OF-PAGES TO WF-NBR-OF-PAGES
            //*  ----------------- BUILD KEY FOR EMPL-ORG-TABLE ---------------------
            pnd_Unit_Empl_Racf_Id_Key_Pnd_Empl_Unit_Cde.setValue(ncw_Efm_Document_Crte_Unit);                                                                             //Natural: MOVE NCW-EFM-DOCUMENT.CRTE-UNIT TO #UNIT-EMPL-RACF-ID-KEY.#EMPL-UNIT-CDE
            pnd_Unit_Empl_Racf_Id_Key_Pnd_Empl_Racf_Id.setValue(ncw_Efm_Document_Crte_Empl_Racf_Id);                                                                      //Natural: MOVE NCW-EFM-DOCUMENT.CRTE-EMPL-RACF-ID TO #UNIT-EMPL-RACF-ID-KEY.#EMPL-RACF-ID
                                                                                                                                                                          //Natural: PERFORM GET-EMPLOYEE-INFO
            sub_Get_Employee_Info();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("DOCNCW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("DOCNCW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ----------------- BUILD KEY FOR NCW-MASTER-INDEX -------------------
            pnd_Actv_Unque_Key_Pnd_Rqst_Log_Dte_Tme.setValue(ncw_Efm_Document_Rqst_Log_Dte_Tme);                                                                          //Natural: MOVE NCW-EFM-DOCUMENT.RQST-LOG-DTE-TME TO #ACTV-UNQUE-KEY.#RQST-LOG-DTE-TME
            vw_ncw_Master_Index.startDatabaseRead                                                                                                                         //Natural: READ NCW-MASTER-INDEX WITH ACTV-UNQUE-KEY = #ACTV-UNQUE-KEY
            (
            "R_NCW",
            new Wc[] { new Wc("ACTV_UNQUE_KEY", ">=", pnd_Actv_Unque_Key, WcType.BY) },
            new Oc[] { new Oc("ACTV_UNQUE_KEY", "ASC") }
            );
            R_NCW:
            while (condition(vw_ncw_Master_Index.readNextRow("R_NCW")))
            {
                if (condition(ncw_Master_Index_Rqst_Log_Dte_Tme.notEquals(pnd_Actv_Unque_Key_Pnd_Rqst_Log_Dte_Tme)))                                                      //Natural: IF NCW-MASTER-INDEX.RQST-LOG-DTE-TME NE #ACTV-UNQUE-KEY.#RQST-LOG-DTE-TME
                {
                    if (true) break R_NCW;                                                                                                                                //Natural: ESCAPE BOTTOM ( R-NCW. ) IMMEDIATE
                }                                                                                                                                                         //Natural: END-IF
                work_File_1_Wf_Rqst_Work_Prcss_Id.setValue(ncw_Master_Index_Work_Prcss_Id);                                                                               //Natural: MOVE NCW-MASTER-INDEX.WORK-PRCSS-ID TO WF-RQST-WORK-PRCSS-ID
                work_File_1_Wf_Crte_Dte_Tme.setValue(ncw_Master_Index_Rqst_Log_Dte_Tme);                                                                                  //Natural: MOVE NCW-MASTER-INDEX.RQST-LOG-DTE-TME TO WF-CRTE-DTE-TME
                //*  NCW
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("DOCNCW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("DOCNCW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-WF-RECORD
            sub_Write_Wf_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("DOCNCW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("DOCNCW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*        PERFORM WRITE-REPORT
            //*  NCW-EFM-DOCUMENT
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Doc_Fnd.setValue("Y");                                                                                                                                        //Natural: ASSIGN #DOC-FND := 'Y'
        //* (5960)
    }
    private void sub_Get_Employee_Info() throws Exception                                                                                                                 //Natural: GET-EMPLOYEE-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //*  =================================
        vw_cwf_Org_Empl_Tbl.startDatabaseFind                                                                                                                             //Natural: FIND ( 1 ) CWF-ORG-EMPL-TBL WITH UNIT-EMPL-RACF-ID-KEY = #UNIT-EMPL-RACF-ID-KEY
        (
        "FIND01",
        new Wc[] { new Wc("UNIT_EMPL_RACF_ID_KEY", "=", pnd_Unit_Empl_Racf_Id_Key, WcType.WITH) },
        1
        );
        FIND01:
        while (condition(vw_cwf_Org_Empl_Tbl.readNextRow("FIND01", true)))
        {
            vw_cwf_Org_Empl_Tbl.setIfNotFoundControlFlag(false);
            if (condition(vw_cwf_Org_Empl_Tbl.getAstCOUNTER().equals(0)))                                                                                                 //Natural: IF NO RECORD FOUND
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Site_Split.setValue(cwf_Org_Empl_Tbl_Empl_Info);                                                                                                          //Natural: MOVE CWF-ORG-EMPL-TBL.EMPL-INFO TO #SITE-SPLIT
            work_File_1_Wf_Site.setValue(pnd_Site_Split_Pnd_Site_Split_38);                                                                                               //Natural: MOVE #SITE-SPLIT-38 TO WF-SITE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*   (8640)
    }
    private void sub_Write_Wf_Record() throws Exception                                                                                                                   //Natural: WRITE-WF-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ===============================
        pnd_Wf_Rec_Cntr.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #WF-REC-CNTR
        work_File_1_Wf_Mail_Item_Nbr.setValue(pnd_Mail_Item_Nbr_Save);                                                                                                    //Natural: MOVE #MAIL-ITEM-NBR-SAVE TO WF-MAIL-ITEM-NBR
        work_File_1_Wf_Upld_Date_Time.setValue(pnd_Upld_Date_Time_Save);                                                                                                  //Natural: MOVE #UPLD-DATE-TIME-SAVE TO WF-UPLD-DATE-TIME
        work_File_1_Wf_Source_Id.setValue(pnd_Source_Id_Save);                                                                                                            //Natural: MOVE #SOURCE-ID-SAVE TO WF-SOURCE-ID
        getWorkFiles().write(1, false, work_File_1);                                                                                                                      //Natural: WRITE WORK FILE 1 WORK-FILE-1
    }
    private void sub_Write_Report() throws Exception                                                                                                                      //Natural: WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //*  ============================
        report_Line_Wr_Cabinet_Id.setValue(work_File_1_Wf_Cabinet_Id);                                                                                                    //Natural: MOVE WF-CABINET-ID TO WR-CABINET-ID
        report_Line_Wr_Crte_Dte_Tme.setValue(work_File_1_Wf_Crte_Dte_Tme);                                                                                                //Natural: MOVE WF-CRTE-DTE-TME TO WR-CRTE-DTE-TME
        report_Line_Wr_Crte_Empl_Racf_Id.setValue(work_File_1_Wf_Crte_Empl_Racf_Id);                                                                                      //Natural: MOVE WF-CRTE-EMPL-RACF-ID TO WR-CRTE-EMPL-RACF-ID
        report_Line_Wr_Dcmnt_Copy_Ind.setValue(work_File_1_Wf_Dcmnt_Copy_Ind);                                                                                            //Natural: MOVE WF-DCMNT-COPY-IND TO WR-DCMNT-COPY-IND
        report_Line_Wr_Dcmnt_Type.setValue(work_File_1_Wf_Dcmnt_Type);                                                                                                    //Natural: MOVE WF-DCMNT-TYPE TO WR-DCMNT-TYPE
        report_Line_Wr_Dcmnt_File_Flag.setValue(work_File_1_Wf_Dcmnt_File_Flag);                                                                                          //Natural: MOVE WF-DCMNT-FILE-FLAG TO WR-DCMNT-FILE-FLAG
        report_Line_Wr_Mail_Item_Nbr.setValue(work_File_1_Wf_Mail_Item_Nbr);                                                                                              //Natural: MOVE WF-MAIL-ITEM-NBR TO WR-MAIL-ITEM-NBR
        report_Line_Wr_Mj_Rqst_Log_Dte_Tme.setValue(work_File_1_Wf_Mj_Rqst_Log_Dte_Tme);                                                                                  //Natural: MOVE WF-MJ-RQST-LOG-DTE-TME TO WR-MJ-RQST-LOG-DTE-TME
        report_Line_Wr_Mj_Work_Prcss_Id.setValue(work_File_1_Wf_Mj_Work_Prcss_Id);                                                                                        //Natural: MOVE WF-MJ-WORK-PRCSS-ID TO WR-MJ-WORK-PRCSS-ID
        report_Line_Wr_Nbr_Of_Pages.setValue(work_File_1_Wf_Nbr_Of_Pages);                                                                                                //Natural: MOVE WF-NBR-OF-PAGES TO WR-NBR-OF-PAGES
        report_Line_Wr_Rqst_Work_Prcss_Id.setValue(work_File_1_Wf_Rqst_Work_Prcss_Id);                                                                                    //Natural: MOVE WF-RQST-WORK-PRCSS-ID TO WR-RQST-WORK-PRCSS-ID
        report_Line_Wr_Site.setValue(work_File_1_Wf_Site);                                                                                                                //Natural: MOVE WF-SITE TO WR-SITE
        report_Line_Wr_Source_Id.setValue(work_File_1_Wf_Source_Id);                                                                                                      //Natural: MOVE WF-SOURCE-ID TO WR-SOURCE-ID
        report_Line_Wr_Upld_Date_Time.setValue(work_File_1_Wf_Upld_Date_Time);                                                                                            //Natural: MOVE WF-UPLD-DATE-TIME TO WR-UPLD-DATE-TIME
        getReports().write(1, NEWLINE,report_Line_Wr_System,report_Line_Wr_Cabinet_Id,report_Line_Wr_Crte_Dte_Tme,report_Line_Wr_Crte_Empl_Racf_Id,report_Line_Wr_Dcmnt_Copy_Ind, //Natural: WRITE ( 1 ) / REPORT-LINE
            report_Line_Wr_Dcmnt_Type,report_Line_Wr_Dcmnt_File_Flag,report_Line_Wr_Mail_Item_Nbr,report_Line_Wr_Mj_Rqst_Log_Dte_Tme,report_Line_Wr_Mj_Work_Prcss_Id,
            report_Line_Wr_Nbr_Of_Pages,report_Line_Wr_Rqst_Work_Prcss_Id,report_Line_Wr_Site,report_Line_Wr_Source_Id,report_Line_Wr_Upld_Date_Time,report_Line_Wr_Rec_End);
        if (Global.isEscape()) return;
    }
    private void sub_Create_Summary_Record() throws Exception                                                                                                             //Natural: CREATE-SUMMARY-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  =====================================
        work_File_1.reset();                                                                                                                                              //Natural: RESET WORK-FILE-1
        pnd_Wf_Rec_Cntr.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #WF-REC-CNTR
        //*  MOVE 'ZZZZZZZZZZZZ' TO WF-CABINET-ID                /* PIN-EXP
        //*  PIN-EXP
        work_File_1_Wf_Cabinet_Id.setValue("ZZZZZZZZZZZZZZZZZ");                                                                                                          //Natural: MOVE 'ZZZZZZZZZZZZZZZZZ' TO WF-CABINET-ID
        work_File_1_Wf_Mail_Item_Nbr.setValue(pnd_Wf_Rec_Cntr);                                                                                                           //Natural: MOVE #WF-REC-CNTR TO WF-MAIL-ITEM-NBR
        //*  MOVE *DATN          TO WF-UPLD-DATE              /* 02/28/03
        //*  MOVE *TIMN          TO WF-UPLD-TIME
        work_File_1_Wf_Upld_Date_Time.setValueEdited(Global.getTIMX(),new ReportEditMask("YYYYMMDDHHIISST"));                                                             //Natural: MOVE EDITED *TIMX ( EM = YYYYMMDDHHIISST ) TO WF-UPLD-DATE-TIME
        getWorkFiles().write(1, false, work_File_1);                                                                                                                      //Natural: WRITE WORK FILE 1 WORK-FILE-1
    }
    private void sub_Update_Control_Record() throws Exception                                                                                                             //Natural: UPDATE-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        G1:                                                                                                                                                               //Natural: GET CWF-SUPPORT-TBL #ISN-CONTROL
        vw_cwf_Support_Tbl.readByID(pnd_Isn_Control.getLong(), "G1");
        if (condition(cwf_Support_Tbl_Run_Indicator.equals("N")))                                                                                                         //Natural: IF RUN-INDICATOR = 'N'
        {
            cwf_Support_Tbl_Normal_Record_Count.setValue(pnd_Wf_Rec_Cntr);                                                                                                //Natural: MOVE #WF-REC-CNTR TO CWF-SUPPORT-TBL.NORMAL-RECORD-COUNT
            cwf_Support_Tbl_Normal_Start_Dte_Tme.setValue(pnd_Running_Dte_Tme_Str_Pnd_Running_Dte_Tme_Sn);                                                                //Natural: MOVE #RUNNING-DTE-TME-SN TO CWF-SUPPORT-TBL.NORMAL-START-DTE-TME
            cwf_Support_Tbl_Normal_End_Dte_Tme.setValue(pnd_Running_Dte_Tme_End_Pnd_Running_Dte_Tme_En);                                                                  //Natural: MOVE #RUNNING-DTE-TME-EN TO CWF-SUPPORT-TBL.NORMAL-END-DTE-TME
            cwf_Support_Tbl_Special_Start_Dte_Tme.setValue(0);                                                                                                            //Natural: MOVE 0 TO CWF-SUPPORT-TBL.SPECIAL-START-DTE-TME
            cwf_Support_Tbl_Special_End_Dte_Tme.setValue(0);                                                                                                              //Natural: MOVE 0 TO CWF-SUPPORT-TBL.SPECIAL-END-DTE-TME
            cwf_Support_Tbl_Special_Record_Count.setValue(0);                                                                                                             //Natural: MOVE 0 TO CWF-SUPPORT-TBL.SPECIAL-RECORD-COUNT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cwf_Support_Tbl_Run_Indicator.equals("S")))                                                                                                         //Natural: IF RUN-INDICATOR = 'S'
        {
            cwf_Support_Tbl_Special_Record_Count.setValue(0);                                                                                                             //Natural: MOVE 0 TO CWF-SUPPORT-TBL.SPECIAL-RECORD-COUNT
            cwf_Support_Tbl_Special_Record_Count.setValue(pnd_Wf_Rec_Cntr);                                                                                               //Natural: MOVE #WF-REC-CNTR TO CWF-SUPPORT-TBL.SPECIAL-RECORD-COUNT
            cwf_Support_Tbl_Run_Indicator.setValue("N");                                                                                                                  //Natural: MOVE 'N' TO CWF-SUPPORT-TBL.RUN-INDICATOR
        }                                                                                                                                                                 //Natural: END-IF
        vw_cwf_Support_Tbl.updateDBRow("G1");                                                                                                                             //Natural: UPDATE ( G1. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        pnd_Message.setValue(DbsUtil.compress("Natural Error", Global.getERROR_NR(), "in", Global.getPROGRAM(), "....Line", Global.getERROR_LINE()));                     //Natural: COMPRESS 'Natural Error' *ERROR-NR 'in' *PROGRAM '....Line' *ERROR-LINE INTO #MESSAGE
        getReports().write(0, NEWLINE,pnd_Message,NEWLINE,"Mail Item Number = ",pnd_Min_Save,NEWLINE,"ISN of cwf-image = ",pnd_Isn_Save);                                 //Natural: WRITE / #MESSAGE / 'Mail Item Number = ' #MIN-SAVE / 'ISN of cwf-image = ' #ISN-SAVE
        DbsUtil.terminate(99);  if (true) return;                                                                                                                         //Natural: TERMINATE 99
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=60");
    }
}
