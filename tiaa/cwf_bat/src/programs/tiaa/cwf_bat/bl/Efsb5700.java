/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:07:07 PM
**        * FROM NATURAL PROGRAM : Efsb5700
************************************************************
**        * FILE NAME            : Efsb5700.java
**        * CLASS NAME           : Efsb5700
**        * INSTANCE NAME        : Efsb5700
************************************************************
************************************************************************
* PROGRAM  : EFSB5700
* SYSTEM   : CWF
* TITLE    : EFM ARCHIVING - MOVE ACTIVE DATA (DB145) TO ARCHIVE (DB054)
*            - FOLDERS   CWF-EFM-FOLDER      TO CWF-ARCH-FOLDER
*            - DOCUMENTS CWF-EFM-DOCUMENT    TO CWF-ARCH-DOCUMENT
*            - TEXT      CWF-EFM-TEXT-OBJECT TO CWF-ARCH-TEXT-OBJECT
* WRITTEN  : FEBRUARY 1997
* FUNCTION : 1. READ CWF-ARCH-SUPPORT-TBL ON DB054 FOR STATS
*  (OLD)     2. READ CWF-EFM-CABINET BY CABINET-ID
*            3. READ CWF-MASTER-INDEX-VIEW BY CASE-ID-KEY
*            4. IF MIT RECORD FOR THIS PIN EXISTS, GO TO 2.
*            5. READ DB145 AND WRITE FOLDERS, DOCUMENTS, AND TEXT
*               OBJECTS FOR THIS PIN TO DB054
*            6. WRITE CABINET, FOLDER, DOCUMENT, TEXT OBJECT ISNS AND
*               KEYS TO A SEQUENTIAL FILE
*            7. REPORT TOTAL RECORDS PROCESSED BY CABINET
*
* AS OF 2/18/98
* NEW
* FUNCTION : 1. READ CWF-ARCH-SUPPORT-TBL ON DB054 FOR STATS
*            2. READ CWF-EFM-FOLDER BY FOLDER-KEY
*            3. READ CWF-MASTER-INDEX-VIEW BY CASE-ID-KEY
*            4. IF MIT RECORD FOR THIS FOLDER EXISTS, GO TO 2.
*            5. READ DB145 AND WRITE FOLDERS, DOCUMENTS, AND TEXT
*               OBJECTS FOR THIS PIN TO DB054
*            6. WRITE CABINET, FOLDER, DOCUMENT, TEXT OBJECT ISNS AND
*               KEYS TO A SEQUENTIAL FILE
*            7. REPORT TOTAL RECORDS PROCESSED BY CABINET
*
* ----------------------------------------------------------------------
* NOTE: THERE IS A CLONED VERSION OF THIS PROGRAM "EFSB570X".
*       IT WAS CLONED FROM THIS VERSION BUT MODIFIED TO ARCHIVE SINGLE
*       PIN's that are too large to be processed with this version.
*       THE TECHNIQUE IS TO:
*         1. TEMPORARILY REPLACE THE EXISTING "EFSB5700" WITH "EFSB570X"
*            CODE AND MOVE IT TO "DCOPYLIB".
*         2. MODIFY THE "CWF-ARCH-SUPPORT-TBL" (SPECIAL TABLE LAYOUT)
*            BY PLUGGING IN THE DESIRED PIN NUMBER IN TWO PLACES. (SEE
*            "EFSB570X" FOR MORE DETAILS).
*            (NOTE: MAKE A HARDCOPY OF THE VALUES BEFORE MAKING CHANGES)
*         3. AFTER SUCCESSFUL ARCHIVE, RESTORE THE ORIGINAL "EFSB5700"
*            CODE TO "DCOPYLIB" AND ORIGINAL VALUES TO THE ARCH SUPPORT
*            TABLE.
* ----------------------------------------------------------------------
*
* HISTORY  :
* --------
* 04/28/97 JHH - CHANGED #ET-TOT TO P9
* 05/06/97 JHH - ADD TO #BT-CNT AFTER EVERY READ
*              - CHECK FOR ET AT CABINET BREAK AND FOLDER BREAK
* 05/19/97 JHH - WRITE(3) DUPLICATE FOLDERS AND DOCUMENTS
* 08/12/97 JHH - DON't select Cabinets with recent AUDIT-DTE-TME
* 02/18/98 BE  - ARCHIVE FOLDERS, DOCUMENTS AND TEXTS IF MIT IS NOT
*                PRESENT.  (MIT NOT PRESENT = MIT ARCHIVED)
* 10/09/98 AS  - TEMPORARY FIX FOR NAT3047, BYPASS PIN-NBR 1096219 FOR
*                TONIGHTS RUN.
* 10/12/98 AS  - TOOK OUT TEMPORARY CODE
* 10/26/98 AS  - TEMPORARY FIX FOR NAT3047, BYPASS PIN-NBR 1336815 FOR
*                TONIGHTS RUN.
* 12/14/98 AS  - BYPASS PIN's 1096219 & 1336815
* 07/28/99 JG  - COMMENTED OUT PREVIOUS "Bypass PIN's 1096219 & 1336815"
* 03/03/00 AS  - ISSUE ET BEFORE ARCHIVING MISC FOLDER
* 07/14/05 RC  - ISSUE ET IN ALL LEVELS TO AVOID NISNHQ
* 02/23/2017 - SINGAK - PIN EXPANSION - AUG 2017 /*PIN-EXP
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsb5700 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Tbl;
    private DbsField cwf_Tbl_Tbl_Data_Field;
    private DbsField cwf_Tbl_Tbl_Updte_Dte_Tme;
    private DbsField cwf_Tbl_Tbl_Updte_Dte;
    private DbsField cwf_Tbl_Tbl_Updte_Oprtr_Cde;
    private DbsField pnd_Tbl_Prime_Key;
    private DbsField pnd_Tbl_Data_Field;

    private DbsGroup pnd_Tbl_Data_Field__R_Field_1;
    private DbsField pnd_Tbl_Data_Field_Pnd_Tbl_1st_Cab;
    private DbsField pnd_Tbl_Data_Field_Pnd_Fill1;
    private DbsField pnd_Tbl_Data_Field_Pnd_Tbl_Run_Dte;
    private DbsField pnd_Tbl_Data_Field_Pnd_Fill2;
    private DbsField pnd_Tbl_Data_Field_Pnd_Run_Limit_Cnt;
    private DbsField pnd_Tbl_Data_Field_Pnd_Fill2a;
    private DbsField pnd_Tbl_Data_Field_Pnd_Run_Limit_Tme;
    private DbsField pnd_Tbl_Data_Field_Pnd_Fill1a;
    private DbsField pnd_Tbl_Data_Field_Pnd_Et_Limit;
    private DbsField pnd_Tbl_Data_Field_Pnd_Fill2b;
    private DbsField pnd_Tbl_Data_Field_Pnd_080_Run;
    private DbsField pnd_Tbl_Data_Field_Pnd_Fill2c;
    private DbsField pnd_Tbl_Data_Field_Pnd_080_Tot;
    private DbsField pnd_Tbl_Data_Field_Pnd_Fill2d;
    private DbsField pnd_Tbl_Data_Field_Pnd_Mit_Run;
    private DbsField pnd_Tbl_Data_Field_Pnd_Fill2e;
    private DbsField pnd_Tbl_Data_Field_Pnd_Mit_Tot;
    private DbsField pnd_Tbl_Data_Field_Pnd_Fill3;
    private DbsField pnd_Tbl_Data_Field_Pnd_Fld_Not_Run;
    private DbsField pnd_Tbl_Data_Field_Pnd_Fill4;
    private DbsField pnd_Tbl_Data_Field_Pnd_Fld_Not_Tot;
    private DbsField pnd_Tbl_Data_Field_Pnd_Fill5;
    private DbsField pnd_Tbl_Data_Field_Pnd_081_Run;
    private DbsField pnd_Tbl_Data_Field_Pnd_Fill6;
    private DbsField pnd_Tbl_Data_Field_Pnd_081_Tot;
    private DbsField pnd_Tbl_Data_Field_Pnd_Fill7;
    private DbsField pnd_Tbl_Data_Field_Pnd_082_Run;
    private DbsField pnd_Tbl_Data_Field_Pnd_Fill8;
    private DbsField pnd_Tbl_Data_Field_Pnd_082_Tot;
    private DbsField pnd_Tbl_Data_Field_Pnd_Fill9;
    private DbsField pnd_Tbl_Data_Field_Pnd_186_Run;
    private DbsField pnd_Tbl_Data_Field_Pnd_Fill10;
    private DbsField pnd_Tbl_Data_Field_Pnd_186_Tot;
    private DbsField pnd_Tbl_Data_Field_Pnd_Fill11;
    private DbsField pnd_Tbl_Data_Field_Pnd_Kdo_Not_Run;
    private DbsField pnd_Tbl_Data_Field_Pnd_Fill12;
    private DbsField pnd_Tbl_Data_Field_Pnd_Kdo_Not_Tot;
    private DbsField pnd_Tbl_Data_Field_Pnd_Fill13;
    private DbsField pnd_Tbl_Data_Field_Pnd_083_Run;
    private DbsField pnd_Tbl_Data_Field_Pnd_Fill14;
    private DbsField pnd_Tbl_Data_Field_Pnd_083_Tot;
    private DbsField pnd_Tbl_Data_Field_Pnd_Fill15;
    private DbsField pnd_Tbl_Data_Field_Pnd_Tbl_Last_Cab;

    private DbsGroup pnd_Tbl_Data_Field__R_Field_2;
    private DbsField pnd_Tbl_Data_Field_Pnd_Tbl1;
    private DbsField pnd_Starting_Cab;

    private DbsGroup pnd_Starting_Cab__R_Field_3;
    private DbsField pnd_Starting_Cab_Pnd_Cab_Pfx;
    private DbsField pnd_Starting_Cab_Pnd_Pin_Nbr;

    private DataAccessProgramView vw_cwf_Mit;
    private DbsField cwf_Mit_Case_Id_Key;

    private DbsGroup cwf_Mit__R_Field_4;
    private DbsField cwf_Mit_Pnd_Pin_Nbr;
    private DbsField pnd_Case_Id_Key;

    private DbsGroup pnd_Case_Id_Key__R_Field_5;
    private DbsField pnd_Case_Id_Key_Pnd_Pin_Nbr;
    private DbsField pnd_Case_Id_Key_Pnd_Tiaa_Rcvd_Dte;
    private DbsField pnd_Case_Id_Key_Pnd_Case_Id_Cde;

    private DataAccessProgramView vw_cwf_Fldr;
    private DbsField cwf_Fldr_Cabinet_Id;

    private DbsGroup cwf_Fldr__R_Field_6;
    private DbsField cwf_Fldr_Cabinet_Pfx;
    private DbsField cwf_Fldr_Cabinet_Pin;

    private DbsGroup cwf_Fldr_Folder_Id;
    private DbsField cwf_Fldr_Tiaa_Rcvd_Dte;
    private DbsField cwf_Fldr_Case_Workprcss_Multi_Subrqst;

    private DbsGroup cwf_Fldr__R_Field_7;
    private DbsField cwf_Fldr_Case_Id;
    private DbsField cwf_Fldr_Wpid;

    private DbsGroup cwf_Fldr__R_Field_8;
    private DbsField cwf_Fldr_Wpid_A2;
    private DbsField cwf_Fldr_Multi_Ind;
    private DbsField cwf_Fldr_Sub_Ind;
    private DbsField cwf_Fldr_Active_Ind;
    private DbsField cwf_Fldr_Sub_Rqst_Work_Prcss_Id;
    private DbsField cwf_Fldr_Sub_Rqst_Work_Prcss_Long_Nme;
    private DbsField cwf_Fldr_Sub_Rqst_Work_Prcss_Short_Nme;
    private DbsField cwf_Fldr_Complaint_Ind;

    private DbsGroup cwf_Fldr_Complaint_Originating_Folder_Id;
    private DbsField cwf_Fldr_Complaint_Orig_Tiaa_Rcvd_Dte;
    private DbsField cwf_Fldr_Cmplnt_Case_Wrkprcss_Mlti_Sbrqst;
    private DbsField cwf_Fldr_Original_Document_Entry_Dte_Tme;
    private DbsField cwf_Fldr_Original_Document_Subtype;
    private DbsField cwf_Fldr_Invrt_Tiaa_Rcvd_Dte;
    private DbsField cwf_Fldr_Documents_Location;

    private DbsGroup cwf_Fldr_Audit_Data;
    private DbsField cwf_Fldr_Audit_Action;
    private DbsField cwf_Fldr_Audit_Empl_Racf_Id;
    private DbsField cwf_Fldr_Audit_System_Or_Unit;
    private DbsField cwf_Fldr_Audit_Jobname;
    private DbsField cwf_Fldr_Audit_Dte_Tme;
    private DbsField cwf_Fldr_Secured_Min_Ind;
    private DbsGroup cwf_Fldr_Contrct_NbrMuGroup;
    private DbsField cwf_Fldr_Contrct_Nbr;
    private DbsField cwf_Fldr_Fldr_In_Arch_Ind;
    private DbsField cwf_Fldr_Folder_Key;

    private DataAccessProgramView vw_cwf_Arch_Fldr;
    private DbsField cwf_Arch_Fldr_Cabinet_Id;

    private DbsGroup cwf_Arch_Fldr_Folder_Id;
    private DbsField cwf_Arch_Fldr_Tiaa_Rcvd_Dte;
    private DbsField cwf_Arch_Fldr_Case_Workprcss_Multi_Subrqst;
    private DbsField cwf_Arch_Fldr_Active_Ind;
    private DbsField cwf_Arch_Fldr_Sub_Rqst_Work_Prcss_Id;
    private DbsField cwf_Arch_Fldr_Sub_Rqst_Work_Prcss_Long_Nme;
    private DbsField cwf_Arch_Fldr_Sub_Rqst_Work_Prcss_Short_Nme;
    private DbsField cwf_Arch_Fldr_Complaint_Ind;

    private DbsGroup cwf_Arch_Fldr_Complaint_Originating_Folder_Id;
    private DbsField cwf_Arch_Fldr_Complaint_Orig_Tiaa_Rcvd_Dte;
    private DbsField cwf_Arch_Fldr_Cmplnt_Case_Wrkprcss_Mlti_Sbrqst;
    private DbsField cwf_Arch_Fldr_Original_Document_Entry_Dte_Tme;
    private DbsField cwf_Arch_Fldr_Original_Document_Subtype;
    private DbsField cwf_Arch_Fldr_Invrt_Tiaa_Rcvd_Dte;
    private DbsField cwf_Arch_Fldr_Documents_Location;

    private DbsGroup cwf_Arch_Fldr_Audit_Data;
    private DbsField cwf_Arch_Fldr_Audit_Action;
    private DbsField cwf_Arch_Fldr_Audit_Empl_Racf_Id;
    private DbsField cwf_Arch_Fldr_Audit_System_Or_Unit;
    private DbsField cwf_Arch_Fldr_Audit_Jobname;
    private DbsField cwf_Arch_Fldr_Audit_Dte_Tme;
    private DbsField cwf_Arch_Fldr_Secured_Min_Ind;
    private DbsGroup cwf_Arch_Fldr_Contrct_NbrMuGroup;
    private DbsField cwf_Arch_Fldr_Contrct_Nbr;
    private DbsField cwf_Arch_Fldr_Fldr_In_Arch_Ind;
    private DbsField pnd_Dcmt_Key;

    private DbsGroup pnd_Dcmt_Key__R_Field_9;
    private DbsField pnd_Dcmt_Key_Pnd_Fldr_Key;

    private DbsGroup pnd_Dcmt_Key__R_Field_10;
    private DbsField pnd_Dcmt_Key_Pnd_Cab_Id;
    private DbsField pnd_Dcmt_Key_Pnd_Tiaa_Rcvd_Dte;
    private DbsField pnd_Dcmt_Key_Pnd_Case_Wpid_Etc;
    private DbsField pnd_Dcmt_Key_Pnd_Entry_Dte_Tme;

    private DataAccessProgramView vw_cwf_Dcmt;
    private DbsField cwf_Dcmt_Cabinet_Id;

    private DbsGroup cwf_Dcmt_Folder_Id;
    private DbsField cwf_Dcmt_Tiaa_Rcvd_Dte;
    private DbsField cwf_Dcmt_Case_Workprcss_Multi_Subrqst;
    private DbsField cwf_Dcmt_Entry_Dte_Tme;
    private DbsField cwf_Dcmt_Entry_Empl_Racf_Id;
    private DbsField cwf_Dcmt_Entry_System_Or_Unit;
    private DbsField cwf_Dcmt_Entry_Jobname;

    private DbsGroup cwf_Dcmt_Document_Type;
    private DbsField cwf_Dcmt_Document_Category;
    private DbsField cwf_Dcmt_Document_Sub_Category;
    private DbsField cwf_Dcmt_Document_Detail;

    private DbsGroup cwf_Dcmt__R_Field_11;
    private DbsField cwf_Dcmt_Pnd_Dcmt_Type;
    private DbsField cwf_Dcmt_Rcvd_Sent_Dte;
    private DbsField cwf_Dcmt_Document_Direction_Ind;
    private DbsField cwf_Dcmt_Secured_Document_Ind;
    private DbsField cwf_Dcmt_Document_Retention_Ind;
    private DbsField cwf_Dcmt_Important_Ind;
    private DbsField cwf_Dcmt_Copy_Ind;
    private DbsField cwf_Dcmt_Documents_Location;
    private DbsField cwf_Dcmt_Sub_Rqst_Work_Prcss_Id;
    private DbsField cwf_Dcmt_Sub_Rqst_Work_Prcss_Short_Nme;
    private DbsField cwf_Dcmt_Business_Document_Description;
    private DbsField cwf_Dcmt_Invrt_Entry_Dte_Tme;

    private DbsGroup cwf_Dcmt_Audit_Data;
    private DbsField cwf_Dcmt_Audit_Action;
    private DbsField cwf_Dcmt_Audit_Empl_Racf_Id;
    private DbsField cwf_Dcmt_Audit_System_Or_Unit;
    private DbsField cwf_Dcmt_Audit_Jobname;
    private DbsField cwf_Dcmt_Audit_Dte_Tme;
    private DbsField cwf_Dcmt_Document_Subtype;

    private DbsGroup cwf_Dcmt__R_Field_12;
    private DbsField cwf_Dcmt_Pnd_Doc_Text;
    private DbsField cwf_Dcmt_Pnd_Doc_Img;
    private DbsField cwf_Dcmt_Pnd_Doc_Kdo;
    private DbsField cwf_Dcmt_Text_Pointer;
    private DbsField cwf_Dcmt_Image_Min;
    private DbsField cwf_Dcmt_Kdo_Application_Id;
    private DbsField cwf_Dcmt_Kdo_Pointer;
    private DbsField cwf_Dcmt_Image_Source_Id;
    private DbsField cwf_Dcmt_Entry_System;
    private DbsField cwf_Dcmt_Dgtze_Corres_From_Dte;
    private DbsField cwf_Dcmt_Dgtze_Corres_To_Dte;
    private DbsField cwf_Dcmt_Dgtze_Corres_Seq_No;
    private DbsField cwf_Dcmt_Dgtze_Doc_End_Page;
    private DbsField cwf_Dcmt_Security_Ind;
    private DbsField cwf_Dcmt_Dgtze_Ind;
    private DbsField cwf_Dcmt_Document_Key;

    private DataAccessProgramView vw_cwf_Arch_Dcmt;
    private DbsField cwf_Arch_Dcmt_Cabinet_Id;

    private DbsGroup cwf_Arch_Dcmt_Folder_Id;
    private DbsField cwf_Arch_Dcmt_Tiaa_Rcvd_Dte;
    private DbsField cwf_Arch_Dcmt_Case_Workprcss_Multi_Subrqst;
    private DbsField cwf_Arch_Dcmt_Entry_Dte_Tme;
    private DbsField cwf_Arch_Dcmt_Entry_Empl_Racf_Id;
    private DbsField cwf_Arch_Dcmt_Entry_System_Or_Unit;
    private DbsField cwf_Arch_Dcmt_Entry_Jobname;

    private DbsGroup cwf_Arch_Dcmt_Document_Type;
    private DbsField cwf_Arch_Dcmt_Document_Category;
    private DbsField cwf_Arch_Dcmt_Document_Sub_Category;
    private DbsField cwf_Arch_Dcmt_Document_Detail;

    private DbsGroup cwf_Arch_Dcmt__R_Field_13;
    private DbsField cwf_Arch_Dcmt_Pnd_Dcmt_Type;
    private DbsField cwf_Arch_Dcmt_Rcvd_Sent_Dte;
    private DbsField cwf_Arch_Dcmt_Document_Direction_Ind;
    private DbsField cwf_Arch_Dcmt_Secured_Document_Ind;
    private DbsField cwf_Arch_Dcmt_Document_Retention_Ind;
    private DbsField cwf_Arch_Dcmt_Important_Ind;
    private DbsField cwf_Arch_Dcmt_Copy_Ind;
    private DbsField cwf_Arch_Dcmt_Documents_Location;
    private DbsField cwf_Arch_Dcmt_Sub_Rqst_Work_Prcss_Id;
    private DbsField cwf_Arch_Dcmt_Sub_Rqst_Work_Prcss_Short_Nme;
    private DbsField cwf_Arch_Dcmt_Business_Document_Description;
    private DbsField cwf_Arch_Dcmt_Invrt_Entry_Dte_Tme;

    private DbsGroup cwf_Arch_Dcmt_Audit_Data;
    private DbsField cwf_Arch_Dcmt_Audit_Action;
    private DbsField cwf_Arch_Dcmt_Audit_Empl_Racf_Id;
    private DbsField cwf_Arch_Dcmt_Audit_System_Or_Unit;
    private DbsField cwf_Arch_Dcmt_Audit_Jobname;
    private DbsField cwf_Arch_Dcmt_Audit_Dte_Tme;
    private DbsField cwf_Arch_Dcmt_Document_Subtype;
    private DbsField cwf_Arch_Dcmt_Text_Pointer;
    private DbsField cwf_Arch_Dcmt_Image_Min;
    private DbsField cwf_Arch_Dcmt_Kdo_Application_Id;
    private DbsField cwf_Arch_Dcmt_Kdo_Pointer;
    private DbsField cwf_Arch_Dcmt_Image_Source_Id;
    private DbsField cwf_Arch_Dcmt_Entry_System;
    private DbsField cwf_Arch_Dcmt_Dgtze_Corres_From_Dte;
    private DbsField cwf_Arch_Dcmt_Dgtze_Corres_To_Dte;
    private DbsField cwf_Arch_Dcmt_Dgtze_Corres_Seq_No;
    private DbsField cwf_Arch_Dcmt_Dgtze_Doc_End_Page;
    private DbsField cwf_Arch_Dcmt_Security_Ind;
    private DbsField cwf_Arch_Dcmt_Dgtze_Ind;

    private DataAccessProgramView vw_cwf_Arch_Dcmt_Histo;
    private DbsField cwf_Arch_Dcmt_Histo_Document_Key;

    private DataAccessProgramView vw_cwf_Txt_Obj;
    private DbsField cwf_Txt_Obj_Cabinet_Id;
    private DbsField cwf_Txt_Obj_Text_Pointer;
    private DbsField cwf_Txt_Obj_Text_Sqnce_Nbr;
    private DbsField cwf_Txt_Obj_Last_Text_Lines_Group_Ind;

    private DbsGroup cwf_Txt_Obj_Text_Document_Line_Group;
    private DbsField cwf_Txt_Obj_Text_Document_Line;
    private DbsField cwf_Txt_Obj_Text_Object_Key;

    private DataAccessProgramView vw_cwf_Arch_Txt_Obj;
    private DbsField cwf_Arch_Txt_Obj_Cabinet_Id;
    private DbsField cwf_Arch_Txt_Obj_Text_Pointer;
    private DbsField cwf_Arch_Txt_Obj_Text_Sqnce_Nbr;
    private DbsField cwf_Arch_Txt_Obj_Last_Text_Lines_Group_Ind;

    private DbsGroup cwf_Arch_Txt_Obj_Text_Document_Line_Group;
    private DbsField cwf_Arch_Txt_Obj_Text_Document_Line;
    private DbsField pnd_Text_Key;

    private DbsGroup pnd_Text_Key__R_Field_14;
    private DbsField pnd_Text_Key_Pnd_Cabinet;
    private DbsField pnd_Text_Key_Pnd_Text_Ptr;
    private DbsField pnd_Text_Key_Pnd_Seq_Nbr;
    private DbsField pnd_Source_Id_Min_Key;

    private DbsGroup pnd_Source_Id_Min_Key__R_Field_15;
    private DbsField pnd_Source_Id_Min_Key_Pnd_Source_Id;
    private DbsField pnd_Source_Id_Min_Key_Pnd_Mail_Item_Nbr;
    private DbsField pnd_081_Cab;
    private DbsField pnd_Fld_Not_Cab;
    private DbsField pnd_082_Cab;
    private DbsField pnd_083_Cab;
    private DbsField pnd_186_Cab;
    private DbsField pnd_Kdo_Not_Cab;
    private DbsField pnd_Tme_Begin;
    private DbsField pnd_Tme_End;
    private DbsField pnd_Isn_Cab;
    private DbsField pnd_Isn_Tbl;
    private DbsField pnd_Isn_Fldr;
    private DbsField pnd_Isn_Dcmt;
    private DbsField pnd_Bt_Cnt;
    private DbsField pnd_Et_Cnt;
    private DbsField pnd_Et_Tot;
    private DbsField pnd_Pge_Cnt;
    private DbsField pnd_X;
    private DbsField pnd_Prt3_Ex;
    private DbsField pnd_Prt3_Desc;
    private DbsField pnd_Audit_Dte;

    private DbsGroup pnd_Work;
    private DbsField pnd_Work_Pnd_Work_File;
    private DbsField pnd_Work_Pnd_Work_Isn;
    private DbsField pnd_Work_Pnd_Work_Key;
    private DbsField pnd_Work_Pnd_Work_Upd;

    private DbsGroup pnd_Head1;
    private DbsField pnd_Head1_Pnd_H1a;
    private DbsField pnd_Head1_Pnd_H1b;

    private DbsGroup pnd_Head2;
    private DbsField pnd_Head2_Pnd_H2a;
    private DbsField pnd_Head2_Pnd_H2b;

    private DbsGroup pnd_Prt1_Lne;
    private DbsField pnd_Prt1_Lne_P_Cab;
    private DbsField pnd_Prt1_Lne_P_081;
    private DbsField pnd_Prt1_Lne_P_081_Not;
    private DbsField pnd_Prt1_Lne_P_082;
    private DbsField pnd_Prt1_Lne_P_186;
    private DbsField pnd_Prt1_Lne_P_Kdo_Not;
    private DbsField pnd_Prt1_Lne_P_083;
    private DbsField pnd_Prt1_Lne_P_Mit;
    private DbsField pnd_Rqst_Id;

    private DbsGroup pnd_Rqst_Id__R_Field_16;
    private DbsField pnd_Rqst_Id_Pnd_Rqst_Work_Prcss_Id;
    private DbsField pnd_Rqst_Id_Pnd_Rqst_Tiaa_Rcvd_Dte;
    private DbsField pnd_Rqst_Id_Pnd_Rqst_Case_Id_Cde;

    private DbsGroup pnd_Rqst_Id__R_Field_17;
    private DbsField pnd_Rqst_Id_Pnd_Rqst_Case;
    private DbsField pnd_Rqst_Id_Pnd_Rqst_Sub;
    private DbsField pnd_Rqst_Id_Pnd_Rqst_Pin_Nbr;

    private DbsGroup pnd_Tbl_Data;
    private DbsField pnd_Tbl_Data_Pnd_Tab1;
    private DbsField pnd_Tbl_Data_Pnd_Tab2;

    private DbsGroup pnd_Tbl_Und;
    private DbsField pnd_Tbl_Und_Pnd_Und1;
    private DbsField pnd_Tbl_Und_Pnd_Und2;
    private DbsField pnd_Prev_Cab;
    private DbsField pnd_Et_Count;

    private DbsRecord setTimeRecord;
    private DbsField st;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Tbl", "CWF-TBL"), "CWF_ARCH_SUPPORT_TBL", "EFM_ARCH_CONTRL");
        cwf_Tbl_Tbl_Data_Field = vw_cwf_Tbl.getRecord().newFieldInGroup("cwf_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 253, RepeatingFieldStrategy.None, 
            "TBL_DATA_FIELD");
        cwf_Tbl_Tbl_Updte_Dte_Tme = vw_cwf_Tbl.getRecord().newFieldInGroup("cwf_Tbl_Tbl_Updte_Dte_Tme", "TBL-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TBL_UPDTE_DTE_TME");
        cwf_Tbl_Tbl_Updte_Dte_Tme.setDdmHeader("UPDATE DATE/TIME");
        cwf_Tbl_Tbl_Updte_Dte = vw_cwf_Tbl.getRecord().newFieldInGroup("cwf_Tbl_Tbl_Updte_Dte", "TBL-UPDTE-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TBL_UPDTE_DTE");
        cwf_Tbl_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        cwf_Tbl_Tbl_Updte_Oprtr_Cde = vw_cwf_Tbl.getRecord().newFieldInGroup("cwf_Tbl_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Tbl_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        registerRecord(vw_cwf_Tbl);

        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);
        pnd_Tbl_Data_Field = localVariables.newFieldInRecord("pnd_Tbl_Data_Field", "#TBL-DATA-FIELD", FieldType.STRING, 253);

        pnd_Tbl_Data_Field__R_Field_1 = localVariables.newGroupInRecord("pnd_Tbl_Data_Field__R_Field_1", "REDEFINE", pnd_Tbl_Data_Field);
        pnd_Tbl_Data_Field_Pnd_Tbl_1st_Cab = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Tbl_1st_Cab", "#TBL-1ST-CAB", FieldType.STRING, 
            14);
        pnd_Tbl_Data_Field_Pnd_Fill1 = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Fill1", "#FILL1", FieldType.STRING, 1);
        pnd_Tbl_Data_Field_Pnd_Tbl_Run_Dte = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Tbl_Run_Dte", "#TBL-RUN-DTE", FieldType.STRING, 
            10);
        pnd_Tbl_Data_Field_Pnd_Fill2 = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Fill2", "#FILL2", FieldType.STRING, 1);
        pnd_Tbl_Data_Field_Pnd_Run_Limit_Cnt = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Run_Limit_Cnt", "#RUN-LIMIT-CNT", 
            FieldType.NUMERIC, 7);
        pnd_Tbl_Data_Field_Pnd_Fill2a = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Fill2a", "#FILL2A", FieldType.STRING, 1);
        pnd_Tbl_Data_Field_Pnd_Run_Limit_Tme = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Run_Limit_Tme", "#RUN-LIMIT-TME", 
            FieldType.NUMERIC, 7);
        pnd_Tbl_Data_Field_Pnd_Fill1a = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Fill1a", "#FILL1A", FieldType.STRING, 1);
        pnd_Tbl_Data_Field_Pnd_Et_Limit = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Et_Limit", "#ET-LIMIT", FieldType.NUMERIC, 
            4);
        pnd_Tbl_Data_Field_Pnd_Fill2b = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Fill2b", "#FILL2B", FieldType.STRING, 1);
        pnd_Tbl_Data_Field_Pnd_080_Run = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_080_Run", "#080-RUN", FieldType.NUMERIC, 
            7);
        pnd_Tbl_Data_Field_Pnd_Fill2c = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Fill2c", "#FILL2C", FieldType.STRING, 1);
        pnd_Tbl_Data_Field_Pnd_080_Tot = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_080_Tot", "#080-TOT", FieldType.NUMERIC, 
            7);
        pnd_Tbl_Data_Field_Pnd_Fill2d = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Fill2d", "#FILL2D", FieldType.STRING, 1);
        pnd_Tbl_Data_Field_Pnd_Mit_Run = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Mit_Run", "#MIT-RUN", FieldType.NUMERIC, 
            7);
        pnd_Tbl_Data_Field_Pnd_Fill2e = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Fill2e", "#FILL2E", FieldType.STRING, 1);
        pnd_Tbl_Data_Field_Pnd_Mit_Tot = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Mit_Tot", "#MIT-TOT", FieldType.NUMERIC, 
            7);
        pnd_Tbl_Data_Field_Pnd_Fill3 = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Fill3", "#FILL3", FieldType.STRING, 1);
        pnd_Tbl_Data_Field_Pnd_Fld_Not_Run = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Fld_Not_Run", "#FLD-NOT-RUN", FieldType.NUMERIC, 
            7);
        pnd_Tbl_Data_Field_Pnd_Fill4 = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Fill4", "#FILL4", FieldType.STRING, 1);
        pnd_Tbl_Data_Field_Pnd_Fld_Not_Tot = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Fld_Not_Tot", "#FLD-NOT-TOT", FieldType.NUMERIC, 
            7);
        pnd_Tbl_Data_Field_Pnd_Fill5 = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Fill5", "#FILL5", FieldType.STRING, 1);
        pnd_Tbl_Data_Field_Pnd_081_Run = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_081_Run", "#081-RUN", FieldType.NUMERIC, 
            7);
        pnd_Tbl_Data_Field_Pnd_Fill6 = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Fill6", "#FILL6", FieldType.STRING, 1);
        pnd_Tbl_Data_Field_Pnd_081_Tot = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_081_Tot", "#081-TOT", FieldType.NUMERIC, 
            7);
        pnd_Tbl_Data_Field_Pnd_Fill7 = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Fill7", "#FILL7", FieldType.STRING, 1);
        pnd_Tbl_Data_Field_Pnd_082_Run = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_082_Run", "#082-RUN", FieldType.NUMERIC, 
            7);
        pnd_Tbl_Data_Field_Pnd_Fill8 = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Fill8", "#FILL8", FieldType.STRING, 1);
        pnd_Tbl_Data_Field_Pnd_082_Tot = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_082_Tot", "#082-TOT", FieldType.NUMERIC, 
            7);
        pnd_Tbl_Data_Field_Pnd_Fill9 = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Fill9", "#FILL9", FieldType.STRING, 1);
        pnd_Tbl_Data_Field_Pnd_186_Run = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_186_Run", "#186-RUN", FieldType.NUMERIC, 
            7);
        pnd_Tbl_Data_Field_Pnd_Fill10 = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Fill10", "#FILL10", FieldType.STRING, 1);
        pnd_Tbl_Data_Field_Pnd_186_Tot = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_186_Tot", "#186-TOT", FieldType.NUMERIC, 
            7);
        pnd_Tbl_Data_Field_Pnd_Fill11 = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Fill11", "#FILL11", FieldType.STRING, 1);
        pnd_Tbl_Data_Field_Pnd_Kdo_Not_Run = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Kdo_Not_Run", "#KDO-NOT-RUN", FieldType.NUMERIC, 
            7);
        pnd_Tbl_Data_Field_Pnd_Fill12 = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Fill12", "#FILL12", FieldType.STRING, 1);
        pnd_Tbl_Data_Field_Pnd_Kdo_Not_Tot = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Kdo_Not_Tot", "#KDO-NOT-TOT", FieldType.NUMERIC, 
            7);
        pnd_Tbl_Data_Field_Pnd_Fill13 = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Fill13", "#FILL13", FieldType.STRING, 1);
        pnd_Tbl_Data_Field_Pnd_083_Run = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_083_Run", "#083-RUN", FieldType.NUMERIC, 
            9);
        pnd_Tbl_Data_Field_Pnd_Fill14 = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Fill14", "#FILL14", FieldType.STRING, 1);
        pnd_Tbl_Data_Field_Pnd_083_Tot = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_083_Tot", "#083-TOT", FieldType.NUMERIC, 
            9);
        pnd_Tbl_Data_Field_Pnd_Fill15 = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Fill15", "#FILL15", FieldType.STRING, 1);
        pnd_Tbl_Data_Field_Pnd_Tbl_Last_Cab = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Tbl_Last_Cab", "#TBL-LAST-CAB", FieldType.STRING, 
            14);

        pnd_Tbl_Data_Field__R_Field_2 = localVariables.newGroupInRecord("pnd_Tbl_Data_Field__R_Field_2", "REDEFINE", pnd_Tbl_Data_Field);
        pnd_Tbl_Data_Field_Pnd_Tbl1 = pnd_Tbl_Data_Field__R_Field_2.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Tbl1", "#TBL1", FieldType.STRING, 73);
        pnd_Starting_Cab = localVariables.newFieldInRecord("pnd_Starting_Cab", "#STARTING-CAB", FieldType.STRING, 14);

        pnd_Starting_Cab__R_Field_3 = localVariables.newGroupInRecord("pnd_Starting_Cab__R_Field_3", "REDEFINE", pnd_Starting_Cab);
        pnd_Starting_Cab_Pnd_Cab_Pfx = pnd_Starting_Cab__R_Field_3.newFieldInGroup("pnd_Starting_Cab_Pnd_Cab_Pfx", "#CAB-PFX", FieldType.STRING, 1);
        pnd_Starting_Cab_Pnd_Pin_Nbr = pnd_Starting_Cab__R_Field_3.newFieldInGroup("pnd_Starting_Cab_Pnd_Pin_Nbr", "#PIN-NBR", FieldType.NUMERIC, 12);

        vw_cwf_Mit = new DataAccessProgramView(new NameInfo("vw_cwf_Mit", "CWF-MIT"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        cwf_Mit_Case_Id_Key = vw_cwf_Mit.getRecord().newFieldInGroup("cwf_Mit_Case_Id_Key", "CASE-ID-KEY", FieldType.BINARY, 18, RepeatingFieldStrategy.None, 
            "CASE_ID_KEY");
        cwf_Mit_Case_Id_Key.setSuperDescriptor(true);

        cwf_Mit__R_Field_4 = vw_cwf_Mit.getRecord().newGroupInGroup("cwf_Mit__R_Field_4", "REDEFINE", cwf_Mit_Case_Id_Key);
        cwf_Mit_Pnd_Pin_Nbr = cwf_Mit__R_Field_4.newFieldInGroup("cwf_Mit_Pnd_Pin_Nbr", "#PIN-NBR", FieldType.NUMERIC, 12);
        registerRecord(vw_cwf_Mit);

        pnd_Case_Id_Key = localVariables.newFieldInRecord("pnd_Case_Id_Key", "#CASE-ID-KEY", FieldType.STRING, 18);

        pnd_Case_Id_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Case_Id_Key__R_Field_5", "REDEFINE", pnd_Case_Id_Key);
        pnd_Case_Id_Key_Pnd_Pin_Nbr = pnd_Case_Id_Key__R_Field_5.newFieldInGroup("pnd_Case_Id_Key_Pnd_Pin_Nbr", "#PIN-NBR", FieldType.NUMERIC, 12);
        pnd_Case_Id_Key_Pnd_Tiaa_Rcvd_Dte = pnd_Case_Id_Key__R_Field_5.newFieldInGroup("pnd_Case_Id_Key_Pnd_Tiaa_Rcvd_Dte", "#TIAA-RCVD-DTE", FieldType.DATE);
        pnd_Case_Id_Key_Pnd_Case_Id_Cde = pnd_Case_Id_Key__R_Field_5.newFieldInGroup("pnd_Case_Id_Key_Pnd_Case_Id_Cde", "#CASE-ID-CDE", FieldType.STRING, 
            1);

        vw_cwf_Fldr = new DataAccessProgramView(new NameInfo("vw_cwf_Fldr", "CWF-FLDR"), "CWF_EFM_FOLDER", "CWF_EFM_FOLDER");
        cwf_Fldr_Cabinet_Id = vw_cwf_Fldr.getRecord().newFieldInGroup("cwf_Fldr_Cabinet_Id", "CABINET-ID", FieldType.STRING, 14, RepeatingFieldStrategy.None, 
            "CABINET_ID");
        cwf_Fldr_Cabinet_Id.setDdmHeader("CABINET/ID");

        cwf_Fldr__R_Field_6 = vw_cwf_Fldr.getRecord().newGroupInGroup("cwf_Fldr__R_Field_6", "REDEFINE", cwf_Fldr_Cabinet_Id);
        cwf_Fldr_Cabinet_Pfx = cwf_Fldr__R_Field_6.newFieldInGroup("cwf_Fldr_Cabinet_Pfx", "CABINET-PFX", FieldType.STRING, 1);
        cwf_Fldr_Cabinet_Pin = cwf_Fldr__R_Field_6.newFieldInGroup("cwf_Fldr_Cabinet_Pin", "CABINET-PIN", FieldType.NUMERIC, 12);

        cwf_Fldr_Folder_Id = vw_cwf_Fldr.getRecord().newGroupInGroup("CWF_FLDR_FOLDER_ID", "FOLDER-ID");
        cwf_Fldr_Tiaa_Rcvd_Dte = cwf_Fldr_Folder_Id.newFieldInGroup("cwf_Fldr_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIAA_RCVD_DTE");
        cwf_Fldr_Tiaa_Rcvd_Dte.setDdmHeader("TIAA RECVD/DATE");
        cwf_Fldr_Case_Workprcss_Multi_Subrqst = cwf_Fldr_Folder_Id.newFieldInGroup("cwf_Fldr_Case_Workprcss_Multi_Subrqst", "CASE-WORKPRCSS-MULTI-SUBRQST", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CASE_WORKPRCSS_MULTI_SUBRQST");
        cwf_Fldr_Case_Workprcss_Multi_Subrqst.setDdmHeader("CASE/WPID/MULTI/SUB-REQ");

        cwf_Fldr__R_Field_7 = cwf_Fldr_Folder_Id.newGroupInGroup("cwf_Fldr__R_Field_7", "REDEFINE", cwf_Fldr_Case_Workprcss_Multi_Subrqst);
        cwf_Fldr_Case_Id = cwf_Fldr__R_Field_7.newFieldInGroup("cwf_Fldr_Case_Id", "CASE-ID", FieldType.STRING, 1);
        cwf_Fldr_Wpid = cwf_Fldr__R_Field_7.newFieldInGroup("cwf_Fldr_Wpid", "WPID", FieldType.STRING, 6);

        cwf_Fldr__R_Field_8 = cwf_Fldr__R_Field_7.newGroupInGroup("cwf_Fldr__R_Field_8", "REDEFINE", cwf_Fldr_Wpid);
        cwf_Fldr_Wpid_A2 = cwf_Fldr__R_Field_8.newFieldInGroup("cwf_Fldr_Wpid_A2", "WPID-A2", FieldType.STRING, 2);
        cwf_Fldr_Multi_Ind = cwf_Fldr__R_Field_7.newFieldInGroup("cwf_Fldr_Multi_Ind", "MULTI-IND", FieldType.STRING, 1);
        cwf_Fldr_Sub_Ind = cwf_Fldr__R_Field_7.newFieldInGroup("cwf_Fldr_Sub_Ind", "SUB-IND", FieldType.STRING, 1);
        cwf_Fldr_Active_Ind = vw_cwf_Fldr.getRecord().newFieldInGroup("cwf_Fldr_Active_Ind", "ACTIVE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ACTIVE_IND");
        cwf_Fldr_Active_Ind.setDdmHeader("ACTV/IND");
        cwf_Fldr_Sub_Rqst_Work_Prcss_Id = vw_cwf_Fldr.getRecord().newFieldInGroup("cwf_Fldr_Sub_Rqst_Work_Prcss_Id", "SUB-RQST-WORK-PRCSS-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "SUB_RQST_WORK_PRCSS_ID");
        cwf_Fldr_Sub_Rqst_Work_Prcss_Id.setDdmHeader("SUB-RQST WORK/PROCESS ID");
        cwf_Fldr_Sub_Rqst_Work_Prcss_Long_Nme = vw_cwf_Fldr.getRecord().newFieldInGroup("cwf_Fldr_Sub_Rqst_Work_Prcss_Long_Nme", "SUB-RQST-WORK-PRCSS-LONG-NME", 
            FieldType.STRING, 45, RepeatingFieldStrategy.None, "SUB_RQST_WORK_PRCSS_LONG_NME");
        cwf_Fldr_Sub_Rqst_Work_Prcss_Long_Nme.setDdmHeader("SUB-RQST WORK/PROCESS NAME");
        cwf_Fldr_Sub_Rqst_Work_Prcss_Short_Nme = vw_cwf_Fldr.getRecord().newFieldInGroup("cwf_Fldr_Sub_Rqst_Work_Prcss_Short_Nme", "SUB-RQST-WORK-PRCSS-SHORT-NME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "SUB_RQST_WORK_PRCSS_SHORT_NME");
        cwf_Fldr_Sub_Rqst_Work_Prcss_Short_Nme.setDdmHeader("SUB-RQST WORK/PROCESS NAME");
        cwf_Fldr_Complaint_Ind = vw_cwf_Fldr.getRecord().newFieldInGroup("cwf_Fldr_Complaint_Ind", "COMPLAINT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "COMPLAINT_IND");
        cwf_Fldr_Complaint_Ind.setDdmHeader("COMPLAINT/IND");

        cwf_Fldr_Complaint_Originating_Folder_Id = vw_cwf_Fldr.getRecord().newGroupInGroup("CWF_FLDR_COMPLAINT_ORIGINATING_FOLDER_ID", "COMPLAINT-ORIGINATING-FOLDER-ID");
        cwf_Fldr_Complaint_Originating_Folder_Id.setDdmHeader("COMPLAINT/FOLDER ID");
        cwf_Fldr_Complaint_Orig_Tiaa_Rcvd_Dte = cwf_Fldr_Complaint_Originating_Folder_Id.newFieldInGroup("cwf_Fldr_Complaint_Orig_Tiaa_Rcvd_Dte", "COMPLAINT-ORIG-TIAA-RCVD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "COMPLAINT_ORIG_TIAA_RCVD_DTE");
        cwf_Fldr_Complaint_Orig_Tiaa_Rcvd_Dte.setDdmHeader("COMPLAINT ORIG./TIAA RCVD DATE");
        cwf_Fldr_Cmplnt_Case_Wrkprcss_Mlti_Sbrqst = cwf_Fldr_Complaint_Originating_Folder_Id.newFieldInGroup("cwf_Fldr_Cmplnt_Case_Wrkprcss_Mlti_Sbrqst", 
            "CMPLNT-CASE-WRKPRCSS-MLTI-SBRQST", FieldType.STRING, 9, RepeatingFieldStrategy.None, "CMPLNT_CASE_WRKPRCSS_MLTI_SBRQST");
        cwf_Fldr_Cmplnt_Case_Wrkprcss_Mlti_Sbrqst.setDdmHeader("CMPLNT/CASE/WPID/MULTI/SUB-REQ");
        cwf_Fldr_Original_Document_Entry_Dte_Tme = vw_cwf_Fldr.getRecord().newFieldInGroup("cwf_Fldr_Original_Document_Entry_Dte_Tme", "ORIGINAL-DOCUMENT-ENTRY-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "ORIGINAL_DOCUMENT_ENTRY_DTE_TME");
        cwf_Fldr_Original_Document_Entry_Dte_Tme.setDdmHeader("ORIGINAL/DOCUMENT PTR");
        cwf_Fldr_Original_Document_Subtype = vw_cwf_Fldr.getRecord().newFieldInGroup("cwf_Fldr_Original_Document_Subtype", "ORIGINAL-DOCUMENT-SUBTYPE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "ORIGINAL_DOCUMENT_SUBTYPE");
        cwf_Fldr_Original_Document_Subtype.setDdmHeader("ORIG DOC/SUBTYPE");
        cwf_Fldr_Invrt_Tiaa_Rcvd_Dte = vw_cwf_Fldr.getRecord().newFieldInGroup("cwf_Fldr_Invrt_Tiaa_Rcvd_Dte", "INVRT-TIAA-RCVD-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "INVRT_TIAA_RCVD_DTE");
        cwf_Fldr_Invrt_Tiaa_Rcvd_Dte.setDdmHeader("INVERTED/TIAA RECVD DATE");
        cwf_Fldr_Documents_Location = vw_cwf_Fldr.getRecord().newFieldInGroup("cwf_Fldr_Documents_Location", "DOCUMENTS-LOCATION", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "DOCUMENTS_LOCATION");
        cwf_Fldr_Documents_Location.setDdmHeader("LOCATION OF/DOCUMENTS");

        cwf_Fldr_Audit_Data = vw_cwf_Fldr.getRecord().newGroupInGroup("CWF_FLDR_AUDIT_DATA", "AUDIT-DATA");
        cwf_Fldr_Audit_Action = cwf_Fldr_Audit_Data.newFieldInGroup("cwf_Fldr_Audit_Action", "AUDIT-ACTION", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "AUDIT_ACTION");
        cwf_Fldr_Audit_Action.setDdmHeader("ACTION");
        cwf_Fldr_Audit_Empl_Racf_Id = cwf_Fldr_Audit_Data.newFieldInGroup("cwf_Fldr_Audit_Empl_Racf_Id", "AUDIT-EMPL-RACF-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "AUDIT_EMPL_RACF_ID");
        cwf_Fldr_Audit_Empl_Racf_Id.setDdmHeader("EMPLOYEE/RACF ID");
        cwf_Fldr_Audit_System_Or_Unit = cwf_Fldr_Audit_Data.newFieldInGroup("cwf_Fldr_Audit_System_Or_Unit", "AUDIT-SYSTEM-OR-UNIT", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "AUDIT_SYSTEM_OR_UNIT");
        cwf_Fldr_Audit_System_Or_Unit.setDdmHeader("SYSTEM/OR UNIT");
        cwf_Fldr_Audit_Jobname = cwf_Fldr_Audit_Data.newFieldInGroup("cwf_Fldr_Audit_Jobname", "AUDIT-JOBNAME", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "AUDIT_JOBNAME");
        cwf_Fldr_Audit_Jobname.setDdmHeader("JOBNAME");
        cwf_Fldr_Audit_Dte_Tme = cwf_Fldr_Audit_Data.newFieldInGroup("cwf_Fldr_Audit_Dte_Tme", "AUDIT-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "AUDIT_DTE_TME");
        cwf_Fldr_Audit_Dte_Tme.setDdmHeader("DATE/& TIME");
        cwf_Fldr_Secured_Min_Ind = vw_cwf_Fldr.getRecord().newFieldInGroup("cwf_Fldr_Secured_Min_Ind", "SECURED-MIN-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "SECURED_MIN_IND");
        cwf_Fldr_Contrct_NbrMuGroup = vw_cwf_Fldr.getRecord().newGroupInGroup("CWF_FLDR_CONTRCT_NBRMuGroup", "CONTRCT_NBRMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "CWF_EFM_FOLDER_CONTRCT_NBR");
        cwf_Fldr_Contrct_Nbr = cwf_Fldr_Contrct_NbrMuGroup.newFieldArrayInGroup("cwf_Fldr_Contrct_Nbr", "CONTRCT-NBR", FieldType.STRING, 8, new DbsArrayController(1, 
            99), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CONTRCT_NBR");
        cwf_Fldr_Contrct_Nbr.setDdmHeader("CONTRACT/NUMBER");
        cwf_Fldr_Fldr_In_Arch_Ind = vw_cwf_Fldr.getRecord().newFieldInGroup("cwf_Fldr_Fldr_In_Arch_Ind", "FLDR-IN-ARCH-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "FLDR_IN_ARCH_IND");
        cwf_Fldr_Folder_Key = vw_cwf_Fldr.getRecord().newFieldInGroup("cwf_Fldr_Folder_Key", "FOLDER-KEY", FieldType.BINARY, 27, RepeatingFieldStrategy.None, 
            "FOLDER_KEY");
        cwf_Fldr_Folder_Key.setDdmHeader("FOLDER/KEY");
        cwf_Fldr_Folder_Key.setSuperDescriptor(true);
        registerRecord(vw_cwf_Fldr);

        vw_cwf_Arch_Fldr = new DataAccessProgramView(new NameInfo("vw_cwf_Arch_Fldr", "CWF-ARCH-FLDR"), "CWF_ARCH_FOLDER", "EFM_ARCH_FOLDER");
        cwf_Arch_Fldr_Cabinet_Id = vw_cwf_Arch_Fldr.getRecord().newFieldInGroup("cwf_Arch_Fldr_Cabinet_Id", "CABINET-ID", FieldType.STRING, 14, RepeatingFieldStrategy.None, 
            "CABINET_ID");
        cwf_Arch_Fldr_Cabinet_Id.setDdmHeader("CABINET/ID");

        cwf_Arch_Fldr_Folder_Id = vw_cwf_Arch_Fldr.getRecord().newGroupInGroup("CWF_ARCH_FLDR_FOLDER_ID", "FOLDER-ID");
        cwf_Arch_Fldr_Tiaa_Rcvd_Dte = cwf_Arch_Fldr_Folder_Id.newFieldInGroup("cwf_Arch_Fldr_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIAA_RCVD_DTE");
        cwf_Arch_Fldr_Tiaa_Rcvd_Dte.setDdmHeader("TIAA RECVD/DATE");
        cwf_Arch_Fldr_Case_Workprcss_Multi_Subrqst = cwf_Arch_Fldr_Folder_Id.newFieldInGroup("cwf_Arch_Fldr_Case_Workprcss_Multi_Subrqst", "CASE-WORKPRCSS-MULTI-SUBRQST", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CASE_WORKPRCSS_MULTI_SUBRQST");
        cwf_Arch_Fldr_Case_Workprcss_Multi_Subrqst.setDdmHeader("CASE/WPID/MULTI/SUB-REQ");
        cwf_Arch_Fldr_Active_Ind = vw_cwf_Arch_Fldr.getRecord().newFieldInGroup("cwf_Arch_Fldr_Active_Ind", "ACTIVE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ACTIVE_IND");
        cwf_Arch_Fldr_Active_Ind.setDdmHeader("ACTV/IND");
        cwf_Arch_Fldr_Sub_Rqst_Work_Prcss_Id = vw_cwf_Arch_Fldr.getRecord().newFieldInGroup("cwf_Arch_Fldr_Sub_Rqst_Work_Prcss_Id", "SUB-RQST-WORK-PRCSS-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "SUB_RQST_WORK_PRCSS_ID");
        cwf_Arch_Fldr_Sub_Rqst_Work_Prcss_Id.setDdmHeader("SUB-RQST WORK/PROCESS ID");
        cwf_Arch_Fldr_Sub_Rqst_Work_Prcss_Long_Nme = vw_cwf_Arch_Fldr.getRecord().newFieldInGroup("cwf_Arch_Fldr_Sub_Rqst_Work_Prcss_Long_Nme", "SUB-RQST-WORK-PRCSS-LONG-NME", 
            FieldType.STRING, 45, RepeatingFieldStrategy.None, "SUB_RQST_WORK_PRCSS_LONG_NME");
        cwf_Arch_Fldr_Sub_Rqst_Work_Prcss_Long_Nme.setDdmHeader("SUB-RQST WORK/PROCESS NAME");
        cwf_Arch_Fldr_Sub_Rqst_Work_Prcss_Short_Nme = vw_cwf_Arch_Fldr.getRecord().newFieldInGroup("cwf_Arch_Fldr_Sub_Rqst_Work_Prcss_Short_Nme", "SUB-RQST-WORK-PRCSS-SHORT-NME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "SUB_RQST_WORK_PRCSS_SHORT_NME");
        cwf_Arch_Fldr_Sub_Rqst_Work_Prcss_Short_Nme.setDdmHeader("SUB-RQST WORK/PROCESS NAME");
        cwf_Arch_Fldr_Complaint_Ind = vw_cwf_Arch_Fldr.getRecord().newFieldInGroup("cwf_Arch_Fldr_Complaint_Ind", "COMPLAINT-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "COMPLAINT_IND");
        cwf_Arch_Fldr_Complaint_Ind.setDdmHeader("COMPLAINT/IND");

        cwf_Arch_Fldr_Complaint_Originating_Folder_Id = vw_cwf_Arch_Fldr.getRecord().newGroupInGroup("CWF_ARCH_FLDR_COMPLAINT_ORIGINATING_FOLDER_ID", 
            "COMPLAINT-ORIGINATING-FOLDER-ID");
        cwf_Arch_Fldr_Complaint_Originating_Folder_Id.setDdmHeader("COMPLAINT/FOLDER ID");
        cwf_Arch_Fldr_Complaint_Orig_Tiaa_Rcvd_Dte = cwf_Arch_Fldr_Complaint_Originating_Folder_Id.newFieldInGroup("cwf_Arch_Fldr_Complaint_Orig_Tiaa_Rcvd_Dte", 
            "COMPLAINT-ORIG-TIAA-RCVD-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "COMPLAINT_ORIG_TIAA_RCVD_DTE");
        cwf_Arch_Fldr_Complaint_Orig_Tiaa_Rcvd_Dte.setDdmHeader("COMPLAINT ORIG./TIAA RCVD DATE");
        cwf_Arch_Fldr_Cmplnt_Case_Wrkprcss_Mlti_Sbrqst = cwf_Arch_Fldr_Complaint_Originating_Folder_Id.newFieldInGroup("cwf_Arch_Fldr_Cmplnt_Case_Wrkprcss_Mlti_Sbrqst", 
            "CMPLNT-CASE-WRKPRCSS-MLTI-SBRQST", FieldType.STRING, 9, RepeatingFieldStrategy.None, "CMPLNT_CASE_WRKPRCSS_MLTI_SBRQST");
        cwf_Arch_Fldr_Cmplnt_Case_Wrkprcss_Mlti_Sbrqst.setDdmHeader("CMPLNT/CASE/WPID/MULTI/SUB-REQ");
        cwf_Arch_Fldr_Original_Document_Entry_Dte_Tme = vw_cwf_Arch_Fldr.getRecord().newFieldInGroup("cwf_Arch_Fldr_Original_Document_Entry_Dte_Tme", 
            "ORIGINAL-DOCUMENT-ENTRY-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "ORIGINAL_DOCUMENT_ENTRY_DTE_TME");
        cwf_Arch_Fldr_Original_Document_Entry_Dte_Tme.setDdmHeader("ORIGINAL/DOCUMENT PTR");
        cwf_Arch_Fldr_Original_Document_Subtype = vw_cwf_Arch_Fldr.getRecord().newFieldInGroup("cwf_Arch_Fldr_Original_Document_Subtype", "ORIGINAL-DOCUMENT-SUBTYPE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "ORIGINAL_DOCUMENT_SUBTYPE");
        cwf_Arch_Fldr_Original_Document_Subtype.setDdmHeader("ORIG DOC/SUBTYPE");
        cwf_Arch_Fldr_Invrt_Tiaa_Rcvd_Dte = vw_cwf_Arch_Fldr.getRecord().newFieldInGroup("cwf_Arch_Fldr_Invrt_Tiaa_Rcvd_Dte", "INVRT-TIAA-RCVD-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "INVRT_TIAA_RCVD_DTE");
        cwf_Arch_Fldr_Invrt_Tiaa_Rcvd_Dte.setDdmHeader("INVERTED/TIAA RECVD DATE");
        cwf_Arch_Fldr_Documents_Location = vw_cwf_Arch_Fldr.getRecord().newFieldInGroup("cwf_Arch_Fldr_Documents_Location", "DOCUMENTS-LOCATION", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "DOCUMENTS_LOCATION");
        cwf_Arch_Fldr_Documents_Location.setDdmHeader("LOCATION OF/DOCUMENTS");

        cwf_Arch_Fldr_Audit_Data = vw_cwf_Arch_Fldr.getRecord().newGroupInGroup("CWF_ARCH_FLDR_AUDIT_DATA", "AUDIT-DATA");
        cwf_Arch_Fldr_Audit_Action = cwf_Arch_Fldr_Audit_Data.newFieldInGroup("cwf_Arch_Fldr_Audit_Action", "AUDIT-ACTION", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "AUDIT_ACTION");
        cwf_Arch_Fldr_Audit_Action.setDdmHeader("ACTION");
        cwf_Arch_Fldr_Audit_Empl_Racf_Id = cwf_Arch_Fldr_Audit_Data.newFieldInGroup("cwf_Arch_Fldr_Audit_Empl_Racf_Id", "AUDIT-EMPL-RACF-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "AUDIT_EMPL_RACF_ID");
        cwf_Arch_Fldr_Audit_Empl_Racf_Id.setDdmHeader("EMPLOYEE/RACF ID");
        cwf_Arch_Fldr_Audit_System_Or_Unit = cwf_Arch_Fldr_Audit_Data.newFieldInGroup("cwf_Arch_Fldr_Audit_System_Or_Unit", "AUDIT-SYSTEM-OR-UNIT", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "AUDIT_SYSTEM_OR_UNIT");
        cwf_Arch_Fldr_Audit_System_Or_Unit.setDdmHeader("SYSTEM/OR UNIT");
        cwf_Arch_Fldr_Audit_Jobname = cwf_Arch_Fldr_Audit_Data.newFieldInGroup("cwf_Arch_Fldr_Audit_Jobname", "AUDIT-JOBNAME", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "AUDIT_JOBNAME");
        cwf_Arch_Fldr_Audit_Jobname.setDdmHeader("JOBNAME");
        cwf_Arch_Fldr_Audit_Dte_Tme = cwf_Arch_Fldr_Audit_Data.newFieldInGroup("cwf_Arch_Fldr_Audit_Dte_Tme", "AUDIT-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "AUDIT_DTE_TME");
        cwf_Arch_Fldr_Audit_Dte_Tme.setDdmHeader("DATE/& TIME");
        cwf_Arch_Fldr_Secured_Min_Ind = vw_cwf_Arch_Fldr.getRecord().newFieldInGroup("cwf_Arch_Fldr_Secured_Min_Ind", "SECURED-MIN-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "SECURED_MIN_IND");
        cwf_Arch_Fldr_Contrct_NbrMuGroup = vw_cwf_Arch_Fldr.getRecord().newGroupInGroup("CWF_ARCH_FLDR_CONTRCT_NBRMuGroup", "CONTRCT_NBRMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "EFM_ARCH_FOLDER_CONTRCT_NBR");
        cwf_Arch_Fldr_Contrct_Nbr = cwf_Arch_Fldr_Contrct_NbrMuGroup.newFieldArrayInGroup("cwf_Arch_Fldr_Contrct_Nbr", "CONTRCT-NBR", FieldType.STRING, 
            8, new DbsArrayController(1, 99), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CONTRCT_NBR");
        cwf_Arch_Fldr_Contrct_Nbr.setDdmHeader("CONTRACT/NUMBER");
        cwf_Arch_Fldr_Fldr_In_Arch_Ind = vw_cwf_Arch_Fldr.getRecord().newFieldInGroup("cwf_Arch_Fldr_Fldr_In_Arch_Ind", "FLDR-IN-ARCH-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "FLDR_IN_ARCH_IND");
        registerRecord(vw_cwf_Arch_Fldr);

        pnd_Dcmt_Key = localVariables.newFieldInRecord("pnd_Dcmt_Key", "#DCMT-KEY", FieldType.STRING, 34);

        pnd_Dcmt_Key__R_Field_9 = localVariables.newGroupInRecord("pnd_Dcmt_Key__R_Field_9", "REDEFINE", pnd_Dcmt_Key);
        pnd_Dcmt_Key_Pnd_Fldr_Key = pnd_Dcmt_Key__R_Field_9.newFieldInGroup("pnd_Dcmt_Key_Pnd_Fldr_Key", "#FLDR-KEY", FieldType.STRING, 27);

        pnd_Dcmt_Key__R_Field_10 = pnd_Dcmt_Key__R_Field_9.newGroupInGroup("pnd_Dcmt_Key__R_Field_10", "REDEFINE", pnd_Dcmt_Key_Pnd_Fldr_Key);
        pnd_Dcmt_Key_Pnd_Cab_Id = pnd_Dcmt_Key__R_Field_10.newFieldInGroup("pnd_Dcmt_Key_Pnd_Cab_Id", "#CAB-ID", FieldType.STRING, 14);
        pnd_Dcmt_Key_Pnd_Tiaa_Rcvd_Dte = pnd_Dcmt_Key__R_Field_10.newFieldInGroup("pnd_Dcmt_Key_Pnd_Tiaa_Rcvd_Dte", "#TIAA-RCVD-DTE", FieldType.DATE);
        pnd_Dcmt_Key_Pnd_Case_Wpid_Etc = pnd_Dcmt_Key__R_Field_10.newFieldInGroup("pnd_Dcmt_Key_Pnd_Case_Wpid_Etc", "#CASE-WPID-ETC", FieldType.STRING, 
            9);
        pnd_Dcmt_Key_Pnd_Entry_Dte_Tme = pnd_Dcmt_Key__R_Field_9.newFieldInGroup("pnd_Dcmt_Key_Pnd_Entry_Dte_Tme", "#ENTRY-DTE-TME", FieldType.TIME);

        vw_cwf_Dcmt = new DataAccessProgramView(new NameInfo("vw_cwf_Dcmt", "CWF-DCMT"), "CWF_EFM_DOCUMENT", "CWF_EFM_DOCUMENT");
        cwf_Dcmt_Cabinet_Id = vw_cwf_Dcmt.getRecord().newFieldInGroup("cwf_Dcmt_Cabinet_Id", "CABINET-ID", FieldType.STRING, 14, RepeatingFieldStrategy.None, 
            "CABINET_ID");
        cwf_Dcmt_Cabinet_Id.setDdmHeader("CABINET/ID");

        cwf_Dcmt_Folder_Id = vw_cwf_Dcmt.getRecord().newGroupInGroup("CWF_DCMT_FOLDER_ID", "FOLDER-ID");
        cwf_Dcmt_Folder_Id.setDdmHeader("FOLDER/ID");
        cwf_Dcmt_Tiaa_Rcvd_Dte = cwf_Dcmt_Folder_Id.newFieldInGroup("cwf_Dcmt_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIAA_RCVD_DTE");
        cwf_Dcmt_Tiaa_Rcvd_Dte.setDdmHeader("TIAA RECVD/DATE");
        cwf_Dcmt_Case_Workprcss_Multi_Subrqst = cwf_Dcmt_Folder_Id.newFieldInGroup("cwf_Dcmt_Case_Workprcss_Multi_Subrqst", "CASE-WORKPRCSS-MULTI-SUBRQST", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CASE_WORKPRCSS_MULTI_SUBRQST");
        cwf_Dcmt_Case_Workprcss_Multi_Subrqst.setDdmHeader("CASE/WPID/MULTI/SUB-REQ");
        cwf_Dcmt_Entry_Dte_Tme = vw_cwf_Dcmt.getRecord().newFieldInGroup("cwf_Dcmt_Entry_Dte_Tme", "ENTRY-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "ENTRY_DTE_TME");
        cwf_Dcmt_Entry_Dte_Tme.setDdmHeader("ENTRY/DATE AND TIME");
        cwf_Dcmt_Entry_Empl_Racf_Id = vw_cwf_Dcmt.getRecord().newFieldInGroup("cwf_Dcmt_Entry_Empl_Racf_Id", "ENTRY-EMPL-RACF-ID", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "ENTRY_EMPL_RACF_ID");
        cwf_Dcmt_Entry_Empl_Racf_Id.setDdmHeader("CREATED BY/RACF ID");
        cwf_Dcmt_Entry_System_Or_Unit = vw_cwf_Dcmt.getRecord().newFieldInGroup("cwf_Dcmt_Entry_System_Or_Unit", "ENTRY-SYSTEM-OR-UNIT", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ENTRY_SYSTEM_OR_UNIT");
        cwf_Dcmt_Entry_System_Or_Unit.setDdmHeader("CREATED BY/SYSTEM OR UNIT");
        cwf_Dcmt_Entry_Jobname = vw_cwf_Dcmt.getRecord().newFieldInGroup("cwf_Dcmt_Entry_Jobname", "ENTRY-JOBNAME", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ENTRY_JOBNAME");
        cwf_Dcmt_Entry_Jobname.setDdmHeader("CREATED BY/JOBNAME");

        cwf_Dcmt_Document_Type = vw_cwf_Dcmt.getRecord().newGroupInGroup("CWF_DCMT_DOCUMENT_TYPE", "DOCUMENT-TYPE");
        cwf_Dcmt_Document_Type.setDdmHeader("DOCUMENT/TYPE");
        cwf_Dcmt_Document_Category = cwf_Dcmt_Document_Type.newFieldInGroup("cwf_Dcmt_Document_Category", "DOCUMENT-CATEGORY", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "DOCUMENT_CATEGORY");
        cwf_Dcmt_Document_Category.setDdmHeader("DOC/CAT");
        cwf_Dcmt_Document_Sub_Category = cwf_Dcmt_Document_Type.newFieldInGroup("cwf_Dcmt_Document_Sub_Category", "DOCUMENT-SUB-CATEGORY", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "DOCUMENT_SUB_CATEGORY");
        cwf_Dcmt_Document_Sub_Category.setDdmHeader("DOC SUB-/CATEGORY");
        cwf_Dcmt_Document_Detail = cwf_Dcmt_Document_Type.newFieldInGroup("cwf_Dcmt_Document_Detail", "DOCUMENT-DETAIL", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "DOCUMENT_DETAIL");
        cwf_Dcmt_Document_Detail.setDdmHeader("DOCUMENT/DETAIL");

        cwf_Dcmt__R_Field_11 = vw_cwf_Dcmt.getRecord().newGroupInGroup("cwf_Dcmt__R_Field_11", "REDEFINE", cwf_Dcmt_Document_Type);
        cwf_Dcmt_Pnd_Dcmt_Type = cwf_Dcmt__R_Field_11.newFieldInGroup("cwf_Dcmt_Pnd_Dcmt_Type", "#DCMT-TYPE", FieldType.STRING, 8);
        cwf_Dcmt_Rcvd_Sent_Dte = vw_cwf_Dcmt.getRecord().newFieldInGroup("cwf_Dcmt_Rcvd_Sent_Dte", "RCVD-SENT-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "RCVD_SENT_DTE");
        cwf_Dcmt_Rcvd_Sent_Dte.setDdmHeader("RECEIVED/SENT/DATE");
        cwf_Dcmt_Document_Direction_Ind = vw_cwf_Dcmt.getRecord().newFieldInGroup("cwf_Dcmt_Document_Direction_Ind", "DOCUMENT-DIRECTION-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DOCUMENT_DIRECTION_IND");
        cwf_Dcmt_Document_Direction_Ind.setDdmHeader("DOC/DIR");
        cwf_Dcmt_Secured_Document_Ind = vw_cwf_Dcmt.getRecord().newFieldInGroup("cwf_Dcmt_Secured_Document_Ind", "SECURED-DOCUMENT-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "SECURED_DOCUMENT_IND");
        cwf_Dcmt_Secured_Document_Ind.setDdmHeader("SECRD/DOC");
        cwf_Dcmt_Document_Retention_Ind = vw_cwf_Dcmt.getRecord().newFieldInGroup("cwf_Dcmt_Document_Retention_Ind", "DOCUMENT-RETENTION-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DOCUMENT_RETENTION_IND");
        cwf_Dcmt_Document_Retention_Ind.setDdmHeader("DOC/RETNTN");
        cwf_Dcmt_Important_Ind = vw_cwf_Dcmt.getRecord().newFieldInGroup("cwf_Dcmt_Important_Ind", "IMPORTANT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "IMPORTANT_IND");
        cwf_Dcmt_Important_Ind.setDdmHeader("IMP/IND");
        cwf_Dcmt_Copy_Ind = vw_cwf_Dcmt.getRecord().newFieldInGroup("cwf_Dcmt_Copy_Ind", "COPY-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "COPY_IND");
        cwf_Dcmt_Copy_Ind.setDdmHeader("COPY/IND");
        cwf_Dcmt_Documents_Location = vw_cwf_Dcmt.getRecord().newFieldInGroup("cwf_Dcmt_Documents_Location", "DOCUMENTS-LOCATION", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "DOCUMENTS_LOCATION");
        cwf_Dcmt_Documents_Location.setDdmHeader("LOCATION OF/DOCUMENTS");
        cwf_Dcmt_Sub_Rqst_Work_Prcss_Id = vw_cwf_Dcmt.getRecord().newFieldInGroup("cwf_Dcmt_Sub_Rqst_Work_Prcss_Id", "SUB-RQST-WORK-PRCSS-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "SUB_RQST_WORK_PRCSS_ID");
        cwf_Dcmt_Sub_Rqst_Work_Prcss_Id.setDdmHeader("SUB-REQUEST/WORK PROCESS ID");
        cwf_Dcmt_Sub_Rqst_Work_Prcss_Short_Nme = vw_cwf_Dcmt.getRecord().newFieldInGroup("cwf_Dcmt_Sub_Rqst_Work_Prcss_Short_Nme", "SUB-RQST-WORK-PRCSS-SHORT-NME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "SUB_RQST_WORK_PRCSS_SHORT_NME");
        cwf_Dcmt_Sub_Rqst_Work_Prcss_Short_Nme.setDdmHeader("SUB-REQUEST WORK/PROCESS NAME");
        cwf_Dcmt_Business_Document_Description = vw_cwf_Dcmt.getRecord().newFieldInGroup("cwf_Dcmt_Business_Document_Description", "BUSINESS-DOCUMENT-DESCRIPTION", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "BUSINESS_DOCUMENT_DESCRIPTION");
        cwf_Dcmt_Business_Document_Description.setDdmHeader("BUSINESS DOCUMENT/DESCRIPTION");
        cwf_Dcmt_Invrt_Entry_Dte_Tme = vw_cwf_Dcmt.getRecord().newFieldInGroup("cwf_Dcmt_Invrt_Entry_Dte_Tme", "INVRT-ENTRY-DTE-TME", FieldType.NUMERIC, 
            15, RepeatingFieldStrategy.None, "INVRT_ENTRY_DTE_TME");
        cwf_Dcmt_Invrt_Entry_Dte_Tme.setDdmHeader("INVERTED ENTRY/DATE AND TIME");

        cwf_Dcmt_Audit_Data = vw_cwf_Dcmt.getRecord().newGroupInGroup("CWF_DCMT_AUDIT_DATA", "AUDIT-DATA");
        cwf_Dcmt_Audit_Action = cwf_Dcmt_Audit_Data.newFieldInGroup("cwf_Dcmt_Audit_Action", "AUDIT-ACTION", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "AUDIT_ACTION");
        cwf_Dcmt_Audit_Action.setDdmHeader("ACTION");
        cwf_Dcmt_Audit_Empl_Racf_Id = cwf_Dcmt_Audit_Data.newFieldInGroup("cwf_Dcmt_Audit_Empl_Racf_Id", "AUDIT-EMPL-RACF-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "AUDIT_EMPL_RACF_ID");
        cwf_Dcmt_Audit_Empl_Racf_Id.setDdmHeader("EMPLOYEE/RACF ID");
        cwf_Dcmt_Audit_System_Or_Unit = cwf_Dcmt_Audit_Data.newFieldInGroup("cwf_Dcmt_Audit_System_Or_Unit", "AUDIT-SYSTEM-OR-UNIT", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "AUDIT_SYSTEM_OR_UNIT");
        cwf_Dcmt_Audit_System_Or_Unit.setDdmHeader("SYSTEM/OR UNIT");
        cwf_Dcmt_Audit_Jobname = cwf_Dcmt_Audit_Data.newFieldInGroup("cwf_Dcmt_Audit_Jobname", "AUDIT-JOBNAME", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "AUDIT_JOBNAME");
        cwf_Dcmt_Audit_Jobname.setDdmHeader("JOBNAME");
        cwf_Dcmt_Audit_Dte_Tme = cwf_Dcmt_Audit_Data.newFieldInGroup("cwf_Dcmt_Audit_Dte_Tme", "AUDIT-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "AUDIT_DTE_TME");
        cwf_Dcmt_Audit_Dte_Tme.setDdmHeader("DATE/& TIME");
        cwf_Dcmt_Document_Subtype = vw_cwf_Dcmt.getRecord().newFieldInGroup("cwf_Dcmt_Document_Subtype", "DOCUMENT-SUBTYPE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "DOCUMENT_SUBTYPE");
        cwf_Dcmt_Document_Subtype.setDdmHeader("DOC/SUBTP");

        cwf_Dcmt__R_Field_12 = vw_cwf_Dcmt.getRecord().newGroupInGroup("cwf_Dcmt__R_Field_12", "REDEFINE", cwf_Dcmt_Document_Subtype);
        cwf_Dcmt_Pnd_Doc_Text = cwf_Dcmt__R_Field_12.newFieldInGroup("cwf_Dcmt_Pnd_Doc_Text", "#DOC-TEXT", FieldType.STRING, 1);
        cwf_Dcmt_Pnd_Doc_Img = cwf_Dcmt__R_Field_12.newFieldInGroup("cwf_Dcmt_Pnd_Doc_Img", "#DOC-IMG", FieldType.STRING, 1);
        cwf_Dcmt_Pnd_Doc_Kdo = cwf_Dcmt__R_Field_12.newFieldInGroup("cwf_Dcmt_Pnd_Doc_Kdo", "#DOC-KDO", FieldType.STRING, 1);
        cwf_Dcmt_Text_Pointer = vw_cwf_Dcmt.getRecord().newFieldInGroup("cwf_Dcmt_Text_Pointer", "TEXT-POINTER", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, 
            "TEXT_POINTER");
        cwf_Dcmt_Text_Pointer.setDdmHeader("TEXT/POINTER");
        cwf_Dcmt_Image_Min = vw_cwf_Dcmt.getRecord().newFieldInGroup("cwf_Dcmt_Image_Min", "IMAGE-MIN", FieldType.STRING, 14, RepeatingFieldStrategy.None, 
            "IMAGE_MIN");
        cwf_Dcmt_Image_Min.setDdmHeader("IMAGE/POINTER");
        cwf_Dcmt_Kdo_Application_Id = vw_cwf_Dcmt.getRecord().newFieldInGroup("cwf_Dcmt_Kdo_Application_Id", "KDO-APPLICATION-ID", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "KDO_APPLICATION_ID");
        cwf_Dcmt_Kdo_Application_Id.setDdmHeader("KDO/APPLIC & ID");
        cwf_Dcmt_Kdo_Pointer = vw_cwf_Dcmt.getRecord().newFieldInGroup("cwf_Dcmt_Kdo_Pointer", "KDO-POINTER", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, 
            "KDO_POINTER");
        cwf_Dcmt_Kdo_Pointer.setDdmHeader("K D O/POINTER");
        cwf_Dcmt_Image_Source_Id = vw_cwf_Dcmt.getRecord().newFieldInGroup("cwf_Dcmt_Image_Source_Id", "IMAGE-SOURCE-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "IMAGE_SOURCE_ID");
        cwf_Dcmt_Image_Source_Id.setDdmHeader("IMAGE/SOURCE ID");
        cwf_Dcmt_Entry_System = vw_cwf_Dcmt.getRecord().newFieldInGroup("cwf_Dcmt_Entry_System", "ENTRY-SYSTEM", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ENTRY_SYSTEM");
        cwf_Dcmt_Entry_System.setDdmHeader("ENTRY/SYSTEM");
        cwf_Dcmt_Dgtze_Corres_From_Dte = vw_cwf_Dcmt.getRecord().newFieldInGroup("cwf_Dcmt_Dgtze_Corres_From_Dte", "DGTZE-CORRES-FROM-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "DGTZE_CORRES_FROM_DTE");
        cwf_Dcmt_Dgtze_Corres_From_Dte.setDdmHeader("CORRESPONCE FROM/DATE");
        cwf_Dcmt_Dgtze_Corres_To_Dte = vw_cwf_Dcmt.getRecord().newFieldInGroup("cwf_Dcmt_Dgtze_Corres_To_Dte", "DGTZE-CORRES-TO-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "DGTZE_CORRES_TO_DTE");
        cwf_Dcmt_Dgtze_Corres_To_Dte.setDdmHeader("CORRESPONDENCE TO/DATE");
        cwf_Dcmt_Dgtze_Corres_Seq_No = vw_cwf_Dcmt.getRecord().newFieldInGroup("cwf_Dcmt_Dgtze_Corres_Seq_No", "DGTZE-CORRES-SEQ-NO", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "DGTZE_CORRES_SEQ_NO");
        cwf_Dcmt_Dgtze_Corres_Seq_No.setDdmHeader("CORRESPONDENCE/SEQUENCE NO");
        cwf_Dcmt_Dgtze_Doc_End_Page = vw_cwf_Dcmt.getRecord().newFieldInGroup("cwf_Dcmt_Dgtze_Doc_End_Page", "DGTZE-DOC-END-PAGE", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "DGTZE_DOC_END_PAGE");
        cwf_Dcmt_Dgtze_Doc_End_Page.setDdmHeader("DOCUMENT/END PGE");
        cwf_Dcmt_Security_Ind = vw_cwf_Dcmt.getRecord().newFieldInGroup("cwf_Dcmt_Security_Ind", "SECURITY-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "SECURITY_IND");
        cwf_Dcmt_Security_Ind.setDdmHeader("SECURED MIN/IND");
        cwf_Dcmt_Dgtze_Ind = vw_cwf_Dcmt.getRecord().newFieldInGroup("cwf_Dcmt_Dgtze_Ind", "DGTZE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "DGTZE_IND");
        cwf_Dcmt_Dgtze_Ind.setDdmHeader("DIGITIZE/IND");
        cwf_Dcmt_Document_Key = vw_cwf_Dcmt.getRecord().newFieldInGroup("cwf_Dcmt_Document_Key", "DOCUMENT-KEY", FieldType.BINARY, 34, RepeatingFieldStrategy.None, 
            "DOCUMENT_KEY");
        cwf_Dcmt_Document_Key.setDdmHeader("DOCUMENT/KEY");
        cwf_Dcmt_Document_Key.setSuperDescriptor(true);
        registerRecord(vw_cwf_Dcmt);

        vw_cwf_Arch_Dcmt = new DataAccessProgramView(new NameInfo("vw_cwf_Arch_Dcmt", "CWF-ARCH-DCMT"), "CWF_ARCH_DOCUMENT", "EFM_ARCH_DOCMNT");
        cwf_Arch_Dcmt_Cabinet_Id = vw_cwf_Arch_Dcmt.getRecord().newFieldInGroup("cwf_Arch_Dcmt_Cabinet_Id", "CABINET-ID", FieldType.STRING, 14, RepeatingFieldStrategy.None, 
            "CABINET_ID");
        cwf_Arch_Dcmt_Cabinet_Id.setDdmHeader("CABINET/ID");

        cwf_Arch_Dcmt_Folder_Id = vw_cwf_Arch_Dcmt.getRecord().newGroupInGroup("CWF_ARCH_DCMT_FOLDER_ID", "FOLDER-ID");
        cwf_Arch_Dcmt_Folder_Id.setDdmHeader("FOLDER/ID");
        cwf_Arch_Dcmt_Tiaa_Rcvd_Dte = cwf_Arch_Dcmt_Folder_Id.newFieldInGroup("cwf_Arch_Dcmt_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIAA_RCVD_DTE");
        cwf_Arch_Dcmt_Tiaa_Rcvd_Dte.setDdmHeader("TIAA RECVD/DATE");
        cwf_Arch_Dcmt_Case_Workprcss_Multi_Subrqst = cwf_Arch_Dcmt_Folder_Id.newFieldInGroup("cwf_Arch_Dcmt_Case_Workprcss_Multi_Subrqst", "CASE-WORKPRCSS-MULTI-SUBRQST", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CASE_WORKPRCSS_MULTI_SUBRQST");
        cwf_Arch_Dcmt_Case_Workprcss_Multi_Subrqst.setDdmHeader("CASE/WPID/MULTI/SUB-REQ");
        cwf_Arch_Dcmt_Entry_Dte_Tme = vw_cwf_Arch_Dcmt.getRecord().newFieldInGroup("cwf_Arch_Dcmt_Entry_Dte_Tme", "ENTRY-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "ENTRY_DTE_TME");
        cwf_Arch_Dcmt_Entry_Dte_Tme.setDdmHeader("ENTRY/DATE AND TIME");
        cwf_Arch_Dcmt_Entry_Empl_Racf_Id = vw_cwf_Arch_Dcmt.getRecord().newFieldInGroup("cwf_Arch_Dcmt_Entry_Empl_Racf_Id", "ENTRY-EMPL-RACF-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ENTRY_EMPL_RACF_ID");
        cwf_Arch_Dcmt_Entry_Empl_Racf_Id.setDdmHeader("CREATED BY/RACF ID");
        cwf_Arch_Dcmt_Entry_System_Or_Unit = vw_cwf_Arch_Dcmt.getRecord().newFieldInGroup("cwf_Arch_Dcmt_Entry_System_Or_Unit", "ENTRY-SYSTEM-OR-UNIT", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ENTRY_SYSTEM_OR_UNIT");
        cwf_Arch_Dcmt_Entry_System_Or_Unit.setDdmHeader("CREATED BY/SYSTEM OR UNIT");
        cwf_Arch_Dcmt_Entry_Jobname = vw_cwf_Arch_Dcmt.getRecord().newFieldInGroup("cwf_Arch_Dcmt_Entry_Jobname", "ENTRY-JOBNAME", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "ENTRY_JOBNAME");
        cwf_Arch_Dcmt_Entry_Jobname.setDdmHeader("CREATED BY/JOBNAME");

        cwf_Arch_Dcmt_Document_Type = vw_cwf_Arch_Dcmt.getRecord().newGroupInGroup("CWF_ARCH_DCMT_DOCUMENT_TYPE", "DOCUMENT-TYPE");
        cwf_Arch_Dcmt_Document_Type.setDdmHeader("DOCUMENT/TYPE");
        cwf_Arch_Dcmt_Document_Category = cwf_Arch_Dcmt_Document_Type.newFieldInGroup("cwf_Arch_Dcmt_Document_Category", "DOCUMENT-CATEGORY", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DOCUMENT_CATEGORY");
        cwf_Arch_Dcmt_Document_Category.setDdmHeader("DOC/CAT");
        cwf_Arch_Dcmt_Document_Sub_Category = cwf_Arch_Dcmt_Document_Type.newFieldInGroup("cwf_Arch_Dcmt_Document_Sub_Category", "DOCUMENT-SUB-CATEGORY", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "DOCUMENT_SUB_CATEGORY");
        cwf_Arch_Dcmt_Document_Sub_Category.setDdmHeader("DOC SUB-/CATEGORY");
        cwf_Arch_Dcmt_Document_Detail = cwf_Arch_Dcmt_Document_Type.newFieldInGroup("cwf_Arch_Dcmt_Document_Detail", "DOCUMENT-DETAIL", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "DOCUMENT_DETAIL");
        cwf_Arch_Dcmt_Document_Detail.setDdmHeader("DOCUMENT/DETAIL");

        cwf_Arch_Dcmt__R_Field_13 = vw_cwf_Arch_Dcmt.getRecord().newGroupInGroup("cwf_Arch_Dcmt__R_Field_13", "REDEFINE", cwf_Arch_Dcmt_Document_Type);
        cwf_Arch_Dcmt_Pnd_Dcmt_Type = cwf_Arch_Dcmt__R_Field_13.newFieldInGroup("cwf_Arch_Dcmt_Pnd_Dcmt_Type", "#DCMT-TYPE", FieldType.STRING, 8);
        cwf_Arch_Dcmt_Rcvd_Sent_Dte = vw_cwf_Arch_Dcmt.getRecord().newFieldInGroup("cwf_Arch_Dcmt_Rcvd_Sent_Dte", "RCVD-SENT-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "RCVD_SENT_DTE");
        cwf_Arch_Dcmt_Rcvd_Sent_Dte.setDdmHeader("RECEIVED/SENT/DATE");
        cwf_Arch_Dcmt_Document_Direction_Ind = vw_cwf_Arch_Dcmt.getRecord().newFieldInGroup("cwf_Arch_Dcmt_Document_Direction_Ind", "DOCUMENT-DIRECTION-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "DOCUMENT_DIRECTION_IND");
        cwf_Arch_Dcmt_Document_Direction_Ind.setDdmHeader("DOC/DIR");
        cwf_Arch_Dcmt_Secured_Document_Ind = vw_cwf_Arch_Dcmt.getRecord().newFieldInGroup("cwf_Arch_Dcmt_Secured_Document_Ind", "SECURED-DOCUMENT-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "SECURED_DOCUMENT_IND");
        cwf_Arch_Dcmt_Secured_Document_Ind.setDdmHeader("SECRD/DOC");
        cwf_Arch_Dcmt_Document_Retention_Ind = vw_cwf_Arch_Dcmt.getRecord().newFieldInGroup("cwf_Arch_Dcmt_Document_Retention_Ind", "DOCUMENT-RETENTION-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "DOCUMENT_RETENTION_IND");
        cwf_Arch_Dcmt_Document_Retention_Ind.setDdmHeader("DOC/RETNTN");
        cwf_Arch_Dcmt_Important_Ind = vw_cwf_Arch_Dcmt.getRecord().newFieldInGroup("cwf_Arch_Dcmt_Important_Ind", "IMPORTANT-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "IMPORTANT_IND");
        cwf_Arch_Dcmt_Important_Ind.setDdmHeader("IMP/IND");
        cwf_Arch_Dcmt_Copy_Ind = vw_cwf_Arch_Dcmt.getRecord().newFieldInGroup("cwf_Arch_Dcmt_Copy_Ind", "COPY-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "COPY_IND");
        cwf_Arch_Dcmt_Copy_Ind.setDdmHeader("COPY/IND");
        cwf_Arch_Dcmt_Documents_Location = vw_cwf_Arch_Dcmt.getRecord().newFieldInGroup("cwf_Arch_Dcmt_Documents_Location", "DOCUMENTS-LOCATION", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "DOCUMENTS_LOCATION");
        cwf_Arch_Dcmt_Documents_Location.setDdmHeader("LOCATION OF/DOCUMENTS");
        cwf_Arch_Dcmt_Sub_Rqst_Work_Prcss_Id = vw_cwf_Arch_Dcmt.getRecord().newFieldInGroup("cwf_Arch_Dcmt_Sub_Rqst_Work_Prcss_Id", "SUB-RQST-WORK-PRCSS-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "SUB_RQST_WORK_PRCSS_ID");
        cwf_Arch_Dcmt_Sub_Rqst_Work_Prcss_Id.setDdmHeader("SUB-REQUEST/WORK PROCESS ID");
        cwf_Arch_Dcmt_Sub_Rqst_Work_Prcss_Short_Nme = vw_cwf_Arch_Dcmt.getRecord().newFieldInGroup("cwf_Arch_Dcmt_Sub_Rqst_Work_Prcss_Short_Nme", "SUB-RQST-WORK-PRCSS-SHORT-NME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "SUB_RQST_WORK_PRCSS_SHORT_NME");
        cwf_Arch_Dcmt_Sub_Rqst_Work_Prcss_Short_Nme.setDdmHeader("SUB-REQUEST WORK/PROCESS NAME");
        cwf_Arch_Dcmt_Business_Document_Description = vw_cwf_Arch_Dcmt.getRecord().newFieldInGroup("cwf_Arch_Dcmt_Business_Document_Description", "BUSINESS-DOCUMENT-DESCRIPTION", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "BUSINESS_DOCUMENT_DESCRIPTION");
        cwf_Arch_Dcmt_Business_Document_Description.setDdmHeader("BUSINESS DOCUMENT/DESCRIPTION");
        cwf_Arch_Dcmt_Invrt_Entry_Dte_Tme = vw_cwf_Arch_Dcmt.getRecord().newFieldInGroup("cwf_Arch_Dcmt_Invrt_Entry_Dte_Tme", "INVRT-ENTRY-DTE-TME", FieldType.NUMERIC, 
            15, RepeatingFieldStrategy.None, "INVRT_ENTRY_DTE_TME");
        cwf_Arch_Dcmt_Invrt_Entry_Dte_Tme.setDdmHeader("INVERTED ENTRY/DATE AND TIME");

        cwf_Arch_Dcmt_Audit_Data = vw_cwf_Arch_Dcmt.getRecord().newGroupInGroup("CWF_ARCH_DCMT_AUDIT_DATA", "AUDIT-DATA");
        cwf_Arch_Dcmt_Audit_Action = cwf_Arch_Dcmt_Audit_Data.newFieldInGroup("cwf_Arch_Dcmt_Audit_Action", "AUDIT-ACTION", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "AUDIT_ACTION");
        cwf_Arch_Dcmt_Audit_Action.setDdmHeader("ACTION");
        cwf_Arch_Dcmt_Audit_Empl_Racf_Id = cwf_Arch_Dcmt_Audit_Data.newFieldInGroup("cwf_Arch_Dcmt_Audit_Empl_Racf_Id", "AUDIT-EMPL-RACF-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "AUDIT_EMPL_RACF_ID");
        cwf_Arch_Dcmt_Audit_Empl_Racf_Id.setDdmHeader("EMPLOYEE/RACF ID");
        cwf_Arch_Dcmt_Audit_System_Or_Unit = cwf_Arch_Dcmt_Audit_Data.newFieldInGroup("cwf_Arch_Dcmt_Audit_System_Or_Unit", "AUDIT-SYSTEM-OR-UNIT", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "AUDIT_SYSTEM_OR_UNIT");
        cwf_Arch_Dcmt_Audit_System_Or_Unit.setDdmHeader("SYSTEM/OR UNIT");
        cwf_Arch_Dcmt_Audit_Jobname = cwf_Arch_Dcmt_Audit_Data.newFieldInGroup("cwf_Arch_Dcmt_Audit_Jobname", "AUDIT-JOBNAME", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "AUDIT_JOBNAME");
        cwf_Arch_Dcmt_Audit_Jobname.setDdmHeader("JOBNAME");
        cwf_Arch_Dcmt_Audit_Dte_Tme = cwf_Arch_Dcmt_Audit_Data.newFieldInGroup("cwf_Arch_Dcmt_Audit_Dte_Tme", "AUDIT-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "AUDIT_DTE_TME");
        cwf_Arch_Dcmt_Audit_Dte_Tme.setDdmHeader("DATE/& TIME");
        cwf_Arch_Dcmt_Document_Subtype = vw_cwf_Arch_Dcmt.getRecord().newFieldInGroup("cwf_Arch_Dcmt_Document_Subtype", "DOCUMENT-SUBTYPE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "DOCUMENT_SUBTYPE");
        cwf_Arch_Dcmt_Document_Subtype.setDdmHeader("DOC/SUBTP");
        cwf_Arch_Dcmt_Text_Pointer = vw_cwf_Arch_Dcmt.getRecord().newFieldInGroup("cwf_Arch_Dcmt_Text_Pointer", "TEXT-POINTER", FieldType.NUMERIC, 7, 
            RepeatingFieldStrategy.None, "TEXT_POINTER");
        cwf_Arch_Dcmt_Text_Pointer.setDdmHeader("TEXT/POINTER");
        cwf_Arch_Dcmt_Image_Min = vw_cwf_Arch_Dcmt.getRecord().newFieldInGroup("cwf_Arch_Dcmt_Image_Min", "IMAGE-MIN", FieldType.STRING, 14, RepeatingFieldStrategy.None, 
            "IMAGE_MIN");
        cwf_Arch_Dcmt_Image_Min.setDdmHeader("IMAGE/POINTER");
        cwf_Arch_Dcmt_Kdo_Application_Id = vw_cwf_Arch_Dcmt.getRecord().newFieldInGroup("cwf_Arch_Dcmt_Kdo_Application_Id", "KDO-APPLICATION-ID", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "KDO_APPLICATION_ID");
        cwf_Arch_Dcmt_Kdo_Application_Id.setDdmHeader("KDO/APPLIC & ID");
        cwf_Arch_Dcmt_Kdo_Pointer = vw_cwf_Arch_Dcmt.getRecord().newFieldInGroup("cwf_Arch_Dcmt_Kdo_Pointer", "KDO-POINTER", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, 
            "KDO_POINTER");
        cwf_Arch_Dcmt_Kdo_Pointer.setDdmHeader("K D O/POINTER");
        cwf_Arch_Dcmt_Image_Source_Id = vw_cwf_Arch_Dcmt.getRecord().newFieldInGroup("cwf_Arch_Dcmt_Image_Source_Id", "IMAGE-SOURCE-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "IMAGE_SOURCE_ID");
        cwf_Arch_Dcmt_Image_Source_Id.setDdmHeader("IMAGE/SOURCE ID");
        cwf_Arch_Dcmt_Entry_System = vw_cwf_Arch_Dcmt.getRecord().newFieldInGroup("cwf_Arch_Dcmt_Entry_System", "ENTRY-SYSTEM", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ENTRY_SYSTEM");
        cwf_Arch_Dcmt_Entry_System.setDdmHeader("ENTRY/SYSTEM");
        cwf_Arch_Dcmt_Dgtze_Corres_From_Dte = vw_cwf_Arch_Dcmt.getRecord().newFieldInGroup("cwf_Arch_Dcmt_Dgtze_Corres_From_Dte", "DGTZE-CORRES-FROM-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "DGTZE_CORRES_FROM_DTE");
        cwf_Arch_Dcmt_Dgtze_Corres_From_Dte.setDdmHeader("CORRESPONCE FROM/DATE");
        cwf_Arch_Dcmt_Dgtze_Corres_To_Dte = vw_cwf_Arch_Dcmt.getRecord().newFieldInGroup("cwf_Arch_Dcmt_Dgtze_Corres_To_Dte", "DGTZE-CORRES-TO-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "DGTZE_CORRES_TO_DTE");
        cwf_Arch_Dcmt_Dgtze_Corres_To_Dte.setDdmHeader("CORRESPONDENCE TO/DATE");
        cwf_Arch_Dcmt_Dgtze_Corres_Seq_No = vw_cwf_Arch_Dcmt.getRecord().newFieldInGroup("cwf_Arch_Dcmt_Dgtze_Corres_Seq_No", "DGTZE-CORRES-SEQ-NO", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "DGTZE_CORRES_SEQ_NO");
        cwf_Arch_Dcmt_Dgtze_Corres_Seq_No.setDdmHeader("CORRESPONDENCE/SEQUENCE NO");
        cwf_Arch_Dcmt_Dgtze_Doc_End_Page = vw_cwf_Arch_Dcmt.getRecord().newFieldInGroup("cwf_Arch_Dcmt_Dgtze_Doc_End_Page", "DGTZE-DOC-END-PAGE", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "DGTZE_DOC_END_PAGE");
        cwf_Arch_Dcmt_Dgtze_Doc_End_Page.setDdmHeader("DOCUMENT/END PGE");
        cwf_Arch_Dcmt_Security_Ind = vw_cwf_Arch_Dcmt.getRecord().newFieldInGroup("cwf_Arch_Dcmt_Security_Ind", "SECURITY-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "SECURITY_IND");
        cwf_Arch_Dcmt_Dgtze_Ind = vw_cwf_Arch_Dcmt.getRecord().newFieldInGroup("cwf_Arch_Dcmt_Dgtze_Ind", "DGTZE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "DGTZE_IND");
        registerRecord(vw_cwf_Arch_Dcmt);

        vw_cwf_Arch_Dcmt_Histo = new DataAccessProgramView(new NameInfo("vw_cwf_Arch_Dcmt_Histo", "CWF-ARCH-DCMT-HISTO"), "CWF_ARCH_DOCUMENT", "EFM_ARCH_DOCMNT");
        cwf_Arch_Dcmt_Histo_Document_Key = vw_cwf_Arch_Dcmt_Histo.getRecord().newFieldInGroup("cwf_Arch_Dcmt_Histo_Document_Key", "DOCUMENT-KEY", FieldType.BINARY, 
            34, RepeatingFieldStrategy.None, "DOCUMENT_KEY");
        cwf_Arch_Dcmt_Histo_Document_Key.setDdmHeader("DOCUMENT/KEY");
        cwf_Arch_Dcmt_Histo_Document_Key.setSuperDescriptor(true);
        registerRecord(vw_cwf_Arch_Dcmt_Histo);

        vw_cwf_Txt_Obj = new DataAccessProgramView(new NameInfo("vw_cwf_Txt_Obj", "CWF-TXT-OBJ"), "CWF_EFM_TEXT_OBJECT", "CWF_EMF_TEXT_OBJ", DdmPeriodicGroups.getInstance().getGroups("CWF_EFM_TEXT_OBJECT"));
        cwf_Txt_Obj_Cabinet_Id = vw_cwf_Txt_Obj.getRecord().newFieldInGroup("cwf_Txt_Obj_Cabinet_Id", "CABINET-ID", FieldType.STRING, 14, RepeatingFieldStrategy.None, 
            "CABINET_ID");
        cwf_Txt_Obj_Cabinet_Id.setDdmHeader("CABINET/ID");
        cwf_Txt_Obj_Text_Pointer = vw_cwf_Txt_Obj.getRecord().newFieldInGroup("cwf_Txt_Obj_Text_Pointer", "TEXT-POINTER", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, 
            "TEXT_POINTER");
        cwf_Txt_Obj_Text_Pointer.setDdmHeader("TEXT/POINTER");
        cwf_Txt_Obj_Text_Sqnce_Nbr = vw_cwf_Txt_Obj.getRecord().newFieldInGroup("cwf_Txt_Obj_Text_Sqnce_Nbr", "TEXT-SQNCE-NBR", FieldType.NUMERIC, 3, 
            RepeatingFieldStrategy.None, "TEXT_SQNCE_NBR");
        cwf_Txt_Obj_Text_Sqnce_Nbr.setDdmHeader("SQNCE/NBR");
        cwf_Txt_Obj_Last_Text_Lines_Group_Ind = vw_cwf_Txt_Obj.getRecord().newFieldInGroup("cwf_Txt_Obj_Last_Text_Lines_Group_Ind", "LAST-TEXT-LINES-GROUP-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "LAST_TEXT_LINES_GROUP_IND");
        cwf_Txt_Obj_Last_Text_Lines_Group_Ind.setDdmHeader("LAST/TEXT IND");

        cwf_Txt_Obj_Text_Document_Line_Group = vw_cwf_Txt_Obj.getRecord().newGroupArrayInGroup("cwf_Txt_Obj_Text_Document_Line_Group", "TEXT-DOCUMENT-LINE-GROUP", 
            new DbsArrayController(1, 30) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CWF_EMF_TEXT_OBJ_TEXT_DOCUMENT_LINE_GROUP");
        cwf_Txt_Obj_Text_Document_Line_Group.setDdmHeader("TEXT/LINES");
        cwf_Txt_Obj_Text_Document_Line = cwf_Txt_Obj_Text_Document_Line_Group.newFieldInGroup("cwf_Txt_Obj_Text_Document_Line", "TEXT-DOCUMENT-LINE", 
            FieldType.STRING, 80, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "TEXT_DOCUMENT_LINE", "CWF_EMF_TEXT_OBJ_TEXT_DOCUMENT_LINE_GROUP");
        cwf_Txt_Obj_Text_Document_Line.setDdmHeader("TEXT DOCUMENT/LINE");
        cwf_Txt_Obj_Text_Object_Key = vw_cwf_Txt_Obj.getRecord().newFieldInGroup("cwf_Txt_Obj_Text_Object_Key", "TEXT-OBJECT-KEY", FieldType.BINARY, 24, 
            RepeatingFieldStrategy.None, "TEXT_OBJECT_KEY");
        cwf_Txt_Obj_Text_Object_Key.setDdmHeader("TEXT OBJECT/KEY");
        cwf_Txt_Obj_Text_Object_Key.setSuperDescriptor(true);
        registerRecord(vw_cwf_Txt_Obj);

        vw_cwf_Arch_Txt_Obj = new DataAccessProgramView(new NameInfo("vw_cwf_Arch_Txt_Obj", "CWF-ARCH-TXT-OBJ"), "CWF_ARCH_TEXT_OBJECT", "EFM_ARCH_TEXT", 
            DdmPeriodicGroups.getInstance().getGroups("CWF_ARCH_TEXT_OBJECT"));
        cwf_Arch_Txt_Obj_Cabinet_Id = vw_cwf_Arch_Txt_Obj.getRecord().newFieldInGroup("cwf_Arch_Txt_Obj_Cabinet_Id", "CABINET-ID", FieldType.STRING, 14, 
            RepeatingFieldStrategy.None, "CABINET_ID");
        cwf_Arch_Txt_Obj_Cabinet_Id.setDdmHeader("CABINET/ID");
        cwf_Arch_Txt_Obj_Text_Pointer = vw_cwf_Arch_Txt_Obj.getRecord().newFieldInGroup("cwf_Arch_Txt_Obj_Text_Pointer", "TEXT-POINTER", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "TEXT_POINTER");
        cwf_Arch_Txt_Obj_Text_Pointer.setDdmHeader("TEXT/POINTER");
        cwf_Arch_Txt_Obj_Text_Sqnce_Nbr = vw_cwf_Arch_Txt_Obj.getRecord().newFieldInGroup("cwf_Arch_Txt_Obj_Text_Sqnce_Nbr", "TEXT-SQNCE-NBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "TEXT_SQNCE_NBR");
        cwf_Arch_Txt_Obj_Text_Sqnce_Nbr.setDdmHeader("SQNCE/NBR");
        cwf_Arch_Txt_Obj_Last_Text_Lines_Group_Ind = vw_cwf_Arch_Txt_Obj.getRecord().newFieldInGroup("cwf_Arch_Txt_Obj_Last_Text_Lines_Group_Ind", "LAST-TEXT-LINES-GROUP-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "LAST_TEXT_LINES_GROUP_IND");
        cwf_Arch_Txt_Obj_Last_Text_Lines_Group_Ind.setDdmHeader("LAST/TEXT IND");

        cwf_Arch_Txt_Obj_Text_Document_Line_Group = vw_cwf_Arch_Txt_Obj.getRecord().newGroupArrayInGroup("cwf_Arch_Txt_Obj_Text_Document_Line_Group", 
            "TEXT-DOCUMENT-LINE-GROUP", new DbsArrayController(1, 30) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "EFM_ARCH_TEXT_TEXT_DOCUMENT_LINE_GROUP");
        cwf_Arch_Txt_Obj_Text_Document_Line_Group.setDdmHeader("TEXT/LINES");
        cwf_Arch_Txt_Obj_Text_Document_Line = cwf_Arch_Txt_Obj_Text_Document_Line_Group.newFieldInGroup("cwf_Arch_Txt_Obj_Text_Document_Line", "TEXT-DOCUMENT-LINE", 
            FieldType.STRING, 80, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "TEXT_DOCUMENT_LINE", "EFM_ARCH_TEXT_TEXT_DOCUMENT_LINE_GROUP");
        cwf_Arch_Txt_Obj_Text_Document_Line.setDdmHeader("TEXT DOCUMENT/LINE");
        registerRecord(vw_cwf_Arch_Txt_Obj);

        pnd_Text_Key = localVariables.newFieldInRecord("pnd_Text_Key", "#TEXT-KEY", FieldType.STRING, 24);

        pnd_Text_Key__R_Field_14 = localVariables.newGroupInRecord("pnd_Text_Key__R_Field_14", "REDEFINE", pnd_Text_Key);
        pnd_Text_Key_Pnd_Cabinet = pnd_Text_Key__R_Field_14.newFieldInGroup("pnd_Text_Key_Pnd_Cabinet", "#CABINET", FieldType.STRING, 14);
        pnd_Text_Key_Pnd_Text_Ptr = pnd_Text_Key__R_Field_14.newFieldInGroup("pnd_Text_Key_Pnd_Text_Ptr", "#TEXT-PTR", FieldType.NUMERIC, 7);
        pnd_Text_Key_Pnd_Seq_Nbr = pnd_Text_Key__R_Field_14.newFieldInGroup("pnd_Text_Key_Pnd_Seq_Nbr", "#SEQ-NBR", FieldType.NUMERIC, 3);
        pnd_Source_Id_Min_Key = localVariables.newFieldInRecord("pnd_Source_Id_Min_Key", "#SOURCE-ID-MIN-KEY", FieldType.STRING, 17);

        pnd_Source_Id_Min_Key__R_Field_15 = localVariables.newGroupInRecord("pnd_Source_Id_Min_Key__R_Field_15", "REDEFINE", pnd_Source_Id_Min_Key);
        pnd_Source_Id_Min_Key_Pnd_Source_Id = pnd_Source_Id_Min_Key__R_Field_15.newFieldInGroup("pnd_Source_Id_Min_Key_Pnd_Source_Id", "#SOURCE-ID", FieldType.STRING, 
            6);
        pnd_Source_Id_Min_Key_Pnd_Mail_Item_Nbr = pnd_Source_Id_Min_Key__R_Field_15.newFieldInGroup("pnd_Source_Id_Min_Key_Pnd_Mail_Item_Nbr", "#MAIL-ITEM-NBR", 
            FieldType.STRING, 11);
        pnd_081_Cab = localVariables.newFieldInRecord("pnd_081_Cab", "#081-CAB", FieldType.PACKED_DECIMAL, 7);
        pnd_Fld_Not_Cab = localVariables.newFieldInRecord("pnd_Fld_Not_Cab", "#FLD-NOT-CAB", FieldType.PACKED_DECIMAL, 7);
        pnd_082_Cab = localVariables.newFieldInRecord("pnd_082_Cab", "#082-CAB", FieldType.PACKED_DECIMAL, 7);
        pnd_083_Cab = localVariables.newFieldInRecord("pnd_083_Cab", "#083-CAB", FieldType.PACKED_DECIMAL, 9);
        pnd_186_Cab = localVariables.newFieldInRecord("pnd_186_Cab", "#186-CAB", FieldType.PACKED_DECIMAL, 7);
        pnd_Kdo_Not_Cab = localVariables.newFieldInRecord("pnd_Kdo_Not_Cab", "#KDO-NOT-CAB", FieldType.PACKED_DECIMAL, 7);
        pnd_Tme_Begin = localVariables.newFieldInRecord("pnd_Tme_Begin", "#TME-BEGIN", FieldType.TIME);
        pnd_Tme_End = localVariables.newFieldInRecord("pnd_Tme_End", "#TME-END", FieldType.TIME);
        pnd_Isn_Cab = localVariables.newFieldInRecord("pnd_Isn_Cab", "#ISN-CAB", FieldType.PACKED_DECIMAL, 10);
        pnd_Isn_Tbl = localVariables.newFieldInRecord("pnd_Isn_Tbl", "#ISN-TBL", FieldType.PACKED_DECIMAL, 10);
        pnd_Isn_Fldr = localVariables.newFieldInRecord("pnd_Isn_Fldr", "#ISN-FLDR", FieldType.PACKED_DECIMAL, 10);
        pnd_Isn_Dcmt = localVariables.newFieldInRecord("pnd_Isn_Dcmt", "#ISN-DCMT", FieldType.PACKED_DECIMAL, 10);
        pnd_Bt_Cnt = localVariables.newFieldInRecord("pnd_Bt_Cnt", "#BT-CNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Et_Cnt = localVariables.newFieldInRecord("pnd_Et_Cnt", "#ET-CNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Et_Tot = localVariables.newFieldInRecord("pnd_Et_Tot", "#ET-TOT", FieldType.PACKED_DECIMAL, 9);
        pnd_Pge_Cnt = localVariables.newFieldInRecord("pnd_Pge_Cnt", "#PGE-CNT", FieldType.PACKED_DECIMAL, 5);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.PACKED_DECIMAL, 3);
        pnd_Prt3_Ex = localVariables.newFieldInRecord("pnd_Prt3_Ex", "#PRT3-EX", FieldType.BOOLEAN, 1);
        pnd_Prt3_Desc = localVariables.newFieldInRecord("pnd_Prt3_Desc", "#PRT3-DESC", FieldType.STRING, 9);
        pnd_Audit_Dte = localVariables.newFieldInRecord("pnd_Audit_Dte", "#AUDIT-DTE", FieldType.DATE);

        pnd_Work = localVariables.newGroupInRecord("pnd_Work", "#WORK");
        pnd_Work_Pnd_Work_File = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Work_File", "#WORK-FILE", FieldType.STRING, 3);
        pnd_Work_Pnd_Work_Isn = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Work_Isn", "#WORK-ISN", FieldType.NUMERIC, 10);
        pnd_Work_Pnd_Work_Key = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Work_Key", "#WORK-KEY", FieldType.STRING, 34);
        pnd_Work_Pnd_Work_Upd = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Work_Upd", "#WORK-UPD", FieldType.NUMERIC, 2);

        pnd_Head1 = localVariables.newGroupInRecord("pnd_Head1", "#HEAD1");
        pnd_Head1_Pnd_H1a = pnd_Head1.newFieldInGroup("pnd_Head1_Pnd_H1a", "#H1A", FieldType.STRING, 61);
        pnd_Head1_Pnd_H1b = pnd_Head1.newFieldInGroup("pnd_Head1_Pnd_H1b", "#H1B", FieldType.STRING, 18);

        pnd_Head2 = localVariables.newGroupInRecord("pnd_Head2", "#HEAD2");
        pnd_Head2_Pnd_H2a = pnd_Head2.newFieldInGroup("pnd_Head2_Pnd_H2a", "#H2A", FieldType.STRING, 61);
        pnd_Head2_Pnd_H2b = pnd_Head2.newFieldInGroup("pnd_Head2_Pnd_H2b", "#H2B", FieldType.STRING, 18);

        pnd_Prt1_Lne = localVariables.newGroupInRecord("pnd_Prt1_Lne", "#PRT1-LNE");
        pnd_Prt1_Lne_P_Cab = pnd_Prt1_Lne.newFieldInGroup("pnd_Prt1_Lne_P_Cab", "P-CAB", FieldType.STRING, 14);
        pnd_Prt1_Lne_P_081 = pnd_Prt1_Lne.newFieldInGroup("pnd_Prt1_Lne_P_081", "P-081", FieldType.NUMERIC, 7);
        pnd_Prt1_Lne_P_081_Not = pnd_Prt1_Lne.newFieldInGroup("pnd_Prt1_Lne_P_081_Not", "P-081-NOT", FieldType.NUMERIC, 7);
        pnd_Prt1_Lne_P_082 = pnd_Prt1_Lne.newFieldInGroup("pnd_Prt1_Lne_P_082", "P-082", FieldType.NUMERIC, 7);
        pnd_Prt1_Lne_P_186 = pnd_Prt1_Lne.newFieldInGroup("pnd_Prt1_Lne_P_186", "P-186", FieldType.NUMERIC, 7);
        pnd_Prt1_Lne_P_Kdo_Not = pnd_Prt1_Lne.newFieldInGroup("pnd_Prt1_Lne_P_Kdo_Not", "P-KDO-NOT", FieldType.NUMERIC, 7);
        pnd_Prt1_Lne_P_083 = pnd_Prt1_Lne.newFieldInGroup("pnd_Prt1_Lne_P_083", "P-083", FieldType.NUMERIC, 9);
        pnd_Prt1_Lne_P_Mit = pnd_Prt1_Lne.newFieldInGroup("pnd_Prt1_Lne_P_Mit", "P-MIT", FieldType.NUMERIC, 7);
        pnd_Rqst_Id = localVariables.newFieldInRecord("pnd_Rqst_Id", "#RQST-ID", FieldType.STRING, 28);

        pnd_Rqst_Id__R_Field_16 = localVariables.newGroupInRecord("pnd_Rqst_Id__R_Field_16", "REDEFINE", pnd_Rqst_Id);
        pnd_Rqst_Id_Pnd_Rqst_Work_Prcss_Id = pnd_Rqst_Id__R_Field_16.newFieldInGroup("pnd_Rqst_Id_Pnd_Rqst_Work_Prcss_Id", "#RQST-WORK-PRCSS-ID", FieldType.STRING, 
            6);
        pnd_Rqst_Id_Pnd_Rqst_Tiaa_Rcvd_Dte = pnd_Rqst_Id__R_Field_16.newFieldInGroup("pnd_Rqst_Id_Pnd_Rqst_Tiaa_Rcvd_Dte", "#RQST-TIAA-RCVD-DTE", FieldType.STRING, 
            8);
        pnd_Rqst_Id_Pnd_Rqst_Case_Id_Cde = pnd_Rqst_Id__R_Field_16.newFieldInGroup("pnd_Rqst_Id_Pnd_Rqst_Case_Id_Cde", "#RQST-CASE-ID-CDE", FieldType.STRING, 
            2);

        pnd_Rqst_Id__R_Field_17 = pnd_Rqst_Id__R_Field_16.newGroupInGroup("pnd_Rqst_Id__R_Field_17", "REDEFINE", pnd_Rqst_Id_Pnd_Rqst_Case_Id_Cde);
        pnd_Rqst_Id_Pnd_Rqst_Case = pnd_Rqst_Id__R_Field_17.newFieldInGroup("pnd_Rqst_Id_Pnd_Rqst_Case", "#RQST-CASE", FieldType.STRING, 1);
        pnd_Rqst_Id_Pnd_Rqst_Sub = pnd_Rqst_Id__R_Field_17.newFieldInGroup("pnd_Rqst_Id_Pnd_Rqst_Sub", "#RQST-SUB", FieldType.STRING, 1);
        pnd_Rqst_Id_Pnd_Rqst_Pin_Nbr = pnd_Rqst_Id__R_Field_16.newFieldInGroup("pnd_Rqst_Id_Pnd_Rqst_Pin_Nbr", "#RQST-PIN-NBR", FieldType.NUMERIC, 12);

        pnd_Tbl_Data = localVariables.newGroupInRecord("pnd_Tbl_Data", "#TBL-DATA");
        pnd_Tbl_Data_Pnd_Tab1 = pnd_Tbl_Data.newFieldInGroup("pnd_Tbl_Data_Pnd_Tab1", "#TAB1", FieldType.STRING, 61);
        pnd_Tbl_Data_Pnd_Tab2 = pnd_Tbl_Data.newFieldInGroup("pnd_Tbl_Data_Pnd_Tab2", "#TAB2", FieldType.STRING, 31);

        pnd_Tbl_Und = localVariables.newGroupInRecord("pnd_Tbl_Und", "#TBL-UND");
        pnd_Tbl_Und_Pnd_Und1 = pnd_Tbl_Und.newFieldInGroup("pnd_Tbl_Und_Pnd_Und1", "#UND1", FieldType.STRING, 61);
        pnd_Tbl_Und_Pnd_Und2 = pnd_Tbl_Und.newFieldInGroup("pnd_Tbl_Und_Pnd_Und2", "#UND2", FieldType.STRING, 31);
        pnd_Prev_Cab = localVariables.newFieldInRecord("pnd_Prev_Cab", "#PREV-CAB", FieldType.STRING, 14);
        pnd_Et_Count = localVariables.newFieldInRecord("pnd_Et_Count", "#ET-COUNT", FieldType.NUMERIC, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        setTimeRecord = new DbsRecord();
        st = setTimeRecord.newFieldInRecord("ST", "ST", FieldType.NUMERIC, 7);
        registerRecord(setTimeRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Tbl.reset();
        vw_cwf_Mit.reset();
        vw_cwf_Fldr.reset();
        vw_cwf_Arch_Fldr.reset();
        vw_cwf_Dcmt.reset();
        vw_cwf_Arch_Dcmt.reset();
        vw_cwf_Arch_Dcmt_Histo.reset();
        vw_cwf_Txt_Obj.reset();
        vw_cwf_Arch_Txt_Obj.reset();

        localVariables.reset();
        pnd_Tbl_Prime_Key.setInitialValue("A CWF-REPORT12-INPUT  EFSB5700");
        pnd_Prt3_Ex.setInitialValue(true);
        pnd_Head1_Pnd_H1a.setInitialValue("Cabinet         Folders  A/Fldrs  Docmnts   Images     KDO  ");
        pnd_Head1_Pnd_H1b.setInitialValue(" Txt Obj   No MIT ");
        pnd_Head2_Pnd_H2a.setInitialValue("--------------  -------- -------- -------- -------- --------");
        pnd_Head2_Pnd_H2b.setInitialValue(" -------- --------");
        pnd_Tbl_Data_Pnd_Tab1.setInitialValue("Last Cab        1st Cab        Run Dte   Cnt/Tme Limits  ETs ");
        pnd_Tbl_Data_Pnd_Tab2.setInitialValue("--- Cabinets -- ----- MIT -----");
        pnd_Tbl_Und_Pnd_Und1.setInitialValue("-------------- -------------- ---------- ------- ------- ----");
        pnd_Tbl_Und_Pnd_Und2.setInitialValue("------- ------- ------- -------");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Efsb5700() throws Exception
    {
        super("Efsb5700");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  ==================================================
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 132 PS = 58
        st = Global.getTIMN();                                                                                                                                            //Natural: SET TIME
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*  --------------------------------------------------------------
        //*  TABLE PROCESSING
        //*  --------------------------------------------------------------
        //*  FILE 067 ON DB 054
        vw_cwf_Tbl.startDatabaseFind                                                                                                                                      //Natural: FIND ( 1 ) CWF-TBL WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
        (
        "FIND_TBL",
        new Wc[] { new Wc("TBL_PRIME_KEY", "=", pnd_Tbl_Prime_Key, WcType.WITH) },
        1
        );
        FIND_TBL:
        while (condition(vw_cwf_Tbl.readNextRow("FIND_TBL")))
        {
            vw_cwf_Tbl.setIfNotFoundControlFlag(false);
            pnd_Isn_Tbl.setValue(vw_cwf_Tbl.getAstISN("FIND_TBL"));                                                                                                       //Natural: MOVE *ISN ( FIND-TBL. ) TO #ISN-TBL
            pnd_Tbl_Data_Field.setValue(cwf_Tbl_Tbl_Data_Field);                                                                                                          //Natural: MOVE CWF-TBL.TBL-DATA-FIELD TO #TBL-DATA-FIELD
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        if (condition(pnd_Isn_Tbl.equals(getZero())))                                                                                                                     //Natural: IF #ISN-TBL = 0
        {
            getReports().write(1, ReportOption.NOTITLE,"CWF-SUPPORT-TBL =",pnd_Tbl_Prime_Key,"not found - Terminated");                                                   //Natural: WRITE ( 1 ) 'CWF-SUPPORT-TBL =' #TBL-PRIME-KEY 'not found - Terminated'
            if (Global.isEscape()) return;
            DbsUtil.terminate(5);  if (true) return;                                                                                                                      //Natural: TERMINATE 05
            //*  TRIGGER AT END OF PAGE
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(1, ReportOption.NOTITLE," ");                                                                                                                  //Natural: WRITE ( 1 ) ' '
        if (Global.isEscape()) return;
        //*  LAST JOB OK
        if (condition(pnd_Tbl_Data_Field_Pnd_Tbl_1st_Cab.notEquals(pnd_Tbl_Data_Field_Pnd_Tbl_Last_Cab)))                                                                 //Natural: IF #TBL-1ST-CAB NOT = #TBL-LAST-CAB
        {
            pnd_Tbl_Data_Field_Pnd_Tbl_1st_Cab.setValue(pnd_Tbl_Data_Field_Pnd_Tbl_Last_Cab);                                                                             //Natural: ASSIGN #TBL-1ST-CAB := #TBL-LAST-CAB
        }                                                                                                                                                                 //Natural: END-IF
        //*  LAST CAB PROCESSED
        pnd_Starting_Cab.setValue(pnd_Tbl_Data_Field_Pnd_Tbl_1st_Cab);                                                                                                    //Natural: MOVE #TBL-1ST-CAB TO #STARTING-CAB
        pnd_Starting_Cab_Pnd_Cab_Pfx.setValue("P");                                                                                                                       //Natural: MOVE 'P' TO #STARTING-CAB.#CAB-PFX
        //*  START AT NEXT CAB
        pnd_Starting_Cab_Pnd_Pin_Nbr.nadd(1);                                                                                                                             //Natural: ADD 1 TO #STARTING-CAB.#PIN-NBR
        pnd_Tme_Begin.setValue(Global.getTIMX());                                                                                                                         //Natural: MOVE *TIMX TO #TME-BEGIN #TME-END
        pnd_Tme_End.setValue(Global.getTIMX());
        pnd_Tbl_Data_Field_Pnd_080_Run.reset();                                                                                                                           //Natural: RESET #080-RUN #MIT-RUN #FLD-NOT-RUN #081-RUN #082-RUN #186-RUN #KDO-NOT-RUN #083-RUN
        pnd_Tbl_Data_Field_Pnd_Mit_Run.reset();
        pnd_Tbl_Data_Field_Pnd_Fld_Not_Run.reset();
        pnd_Tbl_Data_Field_Pnd_081_Run.reset();
        pnd_Tbl_Data_Field_Pnd_082_Run.reset();
        pnd_Tbl_Data_Field_Pnd_186_Run.reset();
        pnd_Tbl_Data_Field_Pnd_Kdo_Not_Run.reset();
        pnd_Tbl_Data_Field_Pnd_083_Run.reset();
        if (condition(pnd_Tbl_Data_Field_Pnd_Tbl_1st_Cab.equals("P0000000 ")))                                                                                            //Natural: IF #TBL-1ST-CAB = 'P0000000 '
        {
            pnd_Tbl_Data_Field_Pnd_080_Tot.reset();                                                                                                                       //Natural: RESET #080-TOT #MIT-TOT #FLD-NOT-TOT #081-TOT #082-TOT #186-TOT #KDO-NOT-TOT #083-TOT
            pnd_Tbl_Data_Field_Pnd_Mit_Tot.reset();
            pnd_Tbl_Data_Field_Pnd_Fld_Not_Tot.reset();
            pnd_Tbl_Data_Field_Pnd_081_Tot.reset();
            pnd_Tbl_Data_Field_Pnd_082_Tot.reset();
            pnd_Tbl_Data_Field_Pnd_186_Tot.reset();
            pnd_Tbl_Data_Field_Pnd_Kdo_Not_Tot.reset();
            pnd_Tbl_Data_Field_Pnd_083_Tot.reset();
        }                                                                                                                                                                 //Natural: END-IF
        //*  ==================================================
        PROG:                                                                                                                                                             //Natural: REPEAT
        while (condition(whileTrue))
        {
            //*  --------------------------------------------------------------
            //*  BEGIN FOLDER PROCESSING
            //*  --------------------------------------------------------------
            pnd_Dcmt_Key_Pnd_Cab_Id.setValue(pnd_Starting_Cab);                                                                                                           //Natural: MOVE #STARTING-CAB TO #DCMT-KEY.#CAB-ID
            pnd_Dcmt_Key_Pnd_Tiaa_Rcvd_Dte.reset();                                                                                                                       //Natural: RESET #DCMT-KEY.#TIAA-RCVD-DTE #DCMT-KEY.#CASE-WPID-ETC
            pnd_Dcmt_Key_Pnd_Case_Wpid_Etc.reset();
            vw_cwf_Fldr.startDatabaseRead                                                                                                                                 //Natural: READ CWF-FLDR BY FOLDER-KEY STARTING FROM #FLDR-KEY
            (
            "READ_FLDR",
            new Wc[] { new Wc("FOLDER_KEY", ">=", pnd_Dcmt_Key_Pnd_Fldr_Key.getBinary(), WcType.BY) },
            new Oc[] { new Oc("FOLDER_KEY", "ASC") }
            );
            READ_FLDR:
            while (condition(vw_cwf_Fldr.readNextRow("READ_FLDR")))
            {
                pnd_Bt_Cnt.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #BT-CNT
                if (condition((pnd_Bt_Cnt.greater(2000)) || (pnd_Et_Cnt.greater(pnd_Tbl_Data_Field_Pnd_Et_Limit))))                                                       //Natural: IF ( #BT-CNT > 2000 ) OR ( #ET-CNT > #ET-LIMIT )
                {
                    //*  FOR ARCHIVE STORES
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION
                    pnd_Et_Tot.nadd(pnd_Et_Cnt);                                                                                                                          //Natural: ADD #ET-CNT TO #ET-TOT
                    pnd_Et_Cnt.reset();                                                                                                                                   //Natural: RESET #ET-CNT #BT-CNT
                    pnd_Bt_Cnt.reset();
                }                                                                                                                                                         //Natural: END-IF
                //* ** IF (CABINET-ID = 'P1096219' OR= 'P1336815') /* BYPASS PIN's
                //* **   ESCAPE TOP                                /* 1096219 & 1336815
                //* ** END-IF
                //*  FOLDER USED IN DIGITIZING
                if (condition(cwf_Fldr_Wpid_A2.equals("ZZ")))                                                                                                             //Natural: IF CWF-FLDR.WPID-A2 = 'ZZ'
                {
                    pnd_Audit_Dte.setValue(cwf_Fldr_Audit_Dte_Tme);                                                                                                       //Natural: ASSIGN #AUDIT-DTE := CWF-FLDR.AUDIT-DTE-TME
                    //*  LT 90 DAYS, DO NOT ARCHIVE
                    if (condition(Global.getDATX().subtract(pnd_Audit_Dte).less(90)))                                                                                     //Natural: IF *DATX - #AUDIT-DTE < 90
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    //*  03/03 AS ISSUE ET AT THE START OF EVERY
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION
                    //*  MISC FOLDER.
                    pnd_Et_Tot.nadd(pnd_Et_Cnt);                                                                                                                          //Natural: ADD #ET-CNT TO #ET-TOT
                    pnd_Et_Cnt.reset();                                                                                                                                   //Natural: RESET #ET-CNT #BT-CNT
                    pnd_Bt_Cnt.reset();
                }                                                                                                                                                         //Natural: END-IF
                //*    --------------------------------------------------------------
                //*    MIT PROCESSING
                //*    --------------------------------------------------------------
                pnd_Rqst_Id_Pnd_Rqst_Work_Prcss_Id.setValue(cwf_Fldr_Wpid);                                                                                               //Natural: ASSIGN #RQST-WORK-PRCSS-ID := CWF-FLDR.WPID
                pnd_Rqst_Id_Pnd_Rqst_Tiaa_Rcvd_Dte.setValueEdited(cwf_Fldr_Tiaa_Rcvd_Dte,new ReportEditMask("YYYYMMDD"));                                                 //Natural: MOVE EDITED CWF-FLDR.TIAA-RCVD-DTE ( EM = YYYYMMDD ) TO #RQST-TIAA-RCVD-DTE
                pnd_Rqst_Id_Pnd_Rqst_Case.setValue(cwf_Fldr_Case_Id);                                                                                                     //Natural: ASSIGN #RQST-CASE := CWF-FLDR.CASE-ID
                pnd_Rqst_Id_Pnd_Rqst_Sub.setValue(cwf_Fldr_Sub_Ind);                                                                                                      //Natural: ASSIGN #RQST-SUB := CWF-FLDR.SUB-IND
                pnd_Rqst_Id_Pnd_Rqst_Pin_Nbr.setValue(cwf_Fldr_Cabinet_Pin);                                                                                              //Natural: ASSIGN #RQST-PIN-NBR := CWF-FLDR.CABINET-PIN
                vw_cwf_Mit.getTotalRowCount                                                                                                                               //Natural: FIND NUMBER CWF-MIT WITH FOLDER-NAME-KEY = #RQST-ID
                (
                new Wc[] { new Wc("FOLDER_NAME_KEY", "=", pnd_Rqst_Id, WcType.WITH) }
                );
                if (condition(vw_cwf_Mit.getAstNUMBER().greater(getZero())))                                                                                              //Natural: IF *NUMBER ( L-F-NO. ) GT 0
                {
                    //*  MIT EXISTS, DO NOT ARCHIVE
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Prev_Cab.notEquals(cwf_Fldr_Cabinet_Id)))                                                                                               //Natural: IF #PREV-CAB NE CWF-FLDR.CABINET-ID
                {
                    pnd_Tbl_Data_Field_Pnd_080_Run.nadd(1);                                                                                                               //Natural: ADD 1 TO #080-RUN
                    pnd_Tbl_Data_Field_Pnd_080_Tot.nadd(1);                                                                                                               //Natural: ADD 1 TO #080-TOT
                    if (condition(pnd_Prev_Cab.greater(" ")))                                                                                                             //Natural: IF #PREV-CAB GT ' '
                    {
                                                                                                                                                                          //Natural: PERFORM REPORT-ARCH-STAT-PER-PIN
                        sub_Report_Arch_Stat_Per_Pin();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_FLDR"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_FLDR"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*        --------------------------------------------------------------
                        //*        TEST RUN LIMITS
                        //*        --------------------------------------------------------------
                        pnd_Tme_End.setValue(Global.getTIMX());                                                                                                           //Natural: MOVE *TIMX TO #TME-END
                        if (condition((pnd_Tme_End.subtract(pnd_Tme_Begin)).greater(pnd_Tbl_Data_Field_Pnd_Run_Limit_Tme)))                                               //Natural: IF ( #TME-END - #TME-BEGIN ) > #RUN-LIMIT-TME
                        {
                            getReports().write(1, ReportOption.NOTITLE,"Archive Time's up",pnd_Tme_Begin, new ReportEditMask ("YYYYMMDDHHIISST"),pnd_Tme_End,             //Natural: WRITE ( 1 ) 'Archive Time"s up' #TME-BEGIN ( EM = YYYYMMDDHHIISST ) #TME-END ( EM = YYYYMMDDHHIISST ) #RUN-LIMIT-TME
                                new ReportEditMask ("YYYYMMDDHHIISST"),pnd_Tbl_Data_Field_Pnd_Run_Limit_Tme);
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("READ_FLDR"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("READ_FLDR"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            pnd_Prev_Cab.reset();                                                                                                                         //Natural: RESET #PREV-CAB
                            if (true) break PROG;                                                                                                                         //Natural: ESCAPE BOTTOM ( PROG. )
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition((pnd_Et_Cnt.add(pnd_Et_Tot)).greater(pnd_Tbl_Data_Field_Pnd_Run_Limit_Cnt)))                                                        //Natural: IF ( #ET-CNT + #ET-TOT ) > #RUN-LIMIT-CNT
                        {
                            getReports().write(1, ReportOption.NOTITLE,"Archive ET run limit",pnd_Tbl_Data_Field_Pnd_Run_Limit_Cnt,"exceeded");                           //Natural: WRITE ( 1 ) 'Archive ET run limit' #RUN-LIMIT-CNT 'exceeded'
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("READ_FLDR"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("READ_FLDR"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            pnd_Prev_Cab.reset();                                                                                                                         //Natural: RESET #PREV-CAB
                            if (true) break PROG;                                                                                                                         //Natural: ESCAPE BOTTOM ( PROG. )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Prt1_Lne_P_Cab.setValue(cwf_Fldr_Cabinet_Id);                                                                                                         //Natural: MOVE CWF-FLDR.CABINET-ID TO P-CAB #PREV-CAB
                pnd_Prev_Cab.setValue(cwf_Fldr_Cabinet_Id);
                //*  NO MIT FOR THIS FOLDER
                pnd_Tbl_Data_Field_Pnd_Mit_Run.nadd(1);                                                                                                                   //Natural: ADD 1 TO #MIT-RUN
                pnd_Tbl_Data_Field_Pnd_Mit_Tot.nadd(1);                                                                                                                   //Natural: ADD 1 TO #MIT-TOT
                pnd_Isn_Fldr.setValue(vw_cwf_Fldr.getAstISN("READ_FLDR"));                                                                                                //Natural: MOVE *ISN ( READ-FLDR. ) TO #ISN-FLDR
                //*    --------------------------------------------------------------
                //*    HOLD FOLDER; BEGIN DOCUMENT PROCESSING
                //*    --------------------------------------------------------------
                pnd_Dcmt_Key_Pnd_Fldr_Key.setValue(cwf_Fldr_Folder_Key);                                                                                                  //Natural: MOVE CWF-FLDR.FOLDER-KEY TO #FLDR-KEY
                pnd_Dcmt_Key_Pnd_Entry_Dte_Tme.reset();                                                                                                                   //Natural: RESET #DCMT-KEY.#ENTRY-DTE-TME
                vw_cwf_Dcmt.startDatabaseRead                                                                                                                             //Natural: READ CWF-DCMT BY DOCUMENT-KEY STARTING FROM #DCMT-KEY
                (
                "READ_DCMT",
                new Wc[] { new Wc("DOCUMENT_KEY", ">=", pnd_Dcmt_Key.getBinary(), WcType.BY) },
                new Oc[] { new Oc("DOCUMENT_KEY", "ASC") }
                );
                READ_DCMT:
                while (condition(vw_cwf_Dcmt.readNextRow("READ_DCMT")))
                {
                    pnd_Bt_Cnt.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #BT-CNT
                    if (condition(cwf_Dcmt_Cabinet_Id.notEquals(cwf_Fldr_Cabinet_Id) || cwf_Dcmt_Tiaa_Rcvd_Dte.notEquals(pnd_Dcmt_Key_Pnd_Tiaa_Rcvd_Dte)                  //Natural: IF CWF-DCMT.CABINET-ID NE CWF-FLDR.CABINET-ID OR CWF-DCMT.TIAA-RCVD-DTE NE #DCMT-KEY.#TIAA-RCVD-DTE OR CWF-DCMT.CASE-WORKPRCSS-MULTI-SUBRQST NE #DCMT-KEY.#CASE-WPID-ETC
                        || cwf_Dcmt_Case_Workprcss_Multi_Subrqst.notEquals(pnd_Dcmt_Key_Pnd_Case_Wpid_Etc)))
                    {
                        if (true) break READ_DCMT;                                                                                                                        //Natural: ESCAPE BOTTOM ( READ-DCMT. )
                    }                                                                                                                                                     //Natural: END-IF
                    //*      WRITE(1) 'Read dcmt' CWF-DCMT.DOCUMENT-KEY
                    pnd_Isn_Dcmt.setValue(vw_cwf_Dcmt.getAstISN("READ_DCMT"));                                                                                            //Natural: MOVE *ISN ( READ-DCMT. ) TO #ISN-DCMT
                    //*      --------------------------------------------------------------
                    //*      HOLD DOCUMENT; BEGIN TEXT PROCESSING
                    //*      --------------------------------------------------------------
                    pnd_Text_Key_Pnd_Cabinet.setValue(cwf_Fldr_Cabinet_Id);                                                                                               //Natural: MOVE CWF-FLDR.CABINET-ID TO #TEXT-KEY.#CABINET
                    pnd_Text_Key_Pnd_Text_Ptr.setValue(cwf_Dcmt_Text_Pointer);                                                                                            //Natural: MOVE CWF-DCMT.TEXT-POINTER TO #TEXT-KEY.#TEXT-PTR
                    pnd_Text_Key_Pnd_Seq_Nbr.reset();                                                                                                                     //Natural: RESET #TEXT-KEY.#SEQ-NBR
                    vw_cwf_Txt_Obj.startDatabaseRead                                                                                                                      //Natural: READ CWF-TXT-OBJ BY TEXT-OBJECT-KEY STARTING FROM #TEXT-KEY
                    (
                    "READ_TEXT",
                    new Wc[] { new Wc("TEXT_OBJECT_KEY", ">=", pnd_Text_Key, WcType.BY) },
                    new Oc[] { new Oc("TEXT_OBJECT_KEY", "ASC") }
                    );
                    READ_TEXT:
                    while (condition(vw_cwf_Txt_Obj.readNextRow("READ_TEXT")))
                    {
                        pnd_Bt_Cnt.nadd(1);                                                                                                                               //Natural: ADD 1 TO #BT-CNT
                        if (condition(cwf_Txt_Obj_Cabinet_Id.notEquals(pnd_Text_Key_Pnd_Cabinet) || cwf_Txt_Obj_Text_Pointer.notEquals(pnd_Text_Key_Pnd_Text_Ptr)))       //Natural: IF CWF-TXT-OBJ.CABINET-ID NE #TEXT-KEY.#CABINET OR CWF-TXT-OBJ.TEXT-POINTER NE #TEXT-KEY.#TEXT-PTR
                        {
                            if (true) break READ_TEXT;                                                                                                                    //Natural: ESCAPE BOTTOM ( READ-TEXT. )
                        }                                                                                                                                                 //Natural: END-IF
                        //*        WRITE(1) 'Read text' CWF-TXT-OBJ.TEXT-OBJECT-KEY
                        pnd_Work_Pnd_Work_Isn.setValue(vw_cwf_Txt_Obj.getAstISN("READ_TEXT"));                                                                            //Natural: MOVE *ISN ( READ-TEXT. ) TO #WORK-ISN
                        pnd_Work_Pnd_Work_Key.setValue(cwf_Txt_Obj_Text_Object_Key);                                                                                      //Natural: MOVE CWF-TXT-OBJ.TEXT-OBJECT-KEY TO #WORK-KEY
                        vw_cwf_Arch_Txt_Obj.startDatabaseFind                                                                                                             //Natural: FIND ( 1 ) CWF-ARCH-TXT-OBJ WITH TEXT-OBJECT-KEY = #TEXT-KEY
                        (
                        "FIND_TEXT",
                        new Wc[] { new Wc("TEXT_OBJECT_KEY", "=", pnd_Text_Key, WcType.WITH) },
                        1
                        );
                        FIND_TEXT:
                        while (condition(vw_cwf_Arch_Txt_Obj.readNextRow("FIND_TEXT", true)))
                        {
                            vw_cwf_Arch_Txt_Obj.setIfNotFoundControlFlag(false);
                            if (condition(vw_cwf_Arch_Txt_Obj.getAstCOUNTER().equals(0)))                                                                                 //Natural: IF NO RECORD FOUND
                            {
                                vw_cwf_Arch_Txt_Obj.setValuesByName(vw_cwf_Txt_Obj);                                                                                      //Natural: MOVE BY NAME CWF-TXT-OBJ TO CWF-ARCH-TXT-OBJ
                                vw_cwf_Arch_Txt_Obj.insertDBRow();                                                                                                        //Natural: STORE CWF-ARCH-TXT-OBJ
                                if (condition(true)) break;                                                                                                               //Natural: ESCAPE BOTTOM
                            }                                                                                                                                             //Natural: END-NOREC
                            pnd_Work_Pnd_Work_Upd.nadd(1);                                                                                                                //Natural: ADD 1 TO #WORK-UPD
                        }                                                                                                                                                 //Natural: END-FIND
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_TEXT"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_TEXT"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  ROXANNE
                        pnd_Et_Count.nadd(1);                                                                                                                             //Natural: ADD 1 TO #ET-COUNT
                        if (condition(pnd_Et_Count.greater(100)))                                                                                                         //Natural: IF #ET-COUNT > 100
                        {
                            pnd_Et_Count.reset();                                                                                                                         //Natural: RESET #ET-COUNT
                            getCurrentProcessState().getDbConv().dbCommit();                                                                                              //Natural: END TRANSACTION
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_083_Cab.nadd(1);                                                                                                                              //Natural: ADD 1 TO #083-CAB
                        pnd_Tbl_Data_Field_Pnd_083_Run.nadd(1);                                                                                                           //Natural: ADD 1 TO #083-RUN
                        pnd_Tbl_Data_Field_Pnd_083_Tot.nadd(1);                                                                                                           //Natural: ADD 1 TO #083-TOT
                        pnd_Work_Pnd_Work_File.setValue("083");                                                                                                           //Natural: MOVE '083' TO #WORK-FILE
                        pnd_Et_Cnt.nadd(1);                                                                                                                               //Natural: ADD 1 TO #ET-CNT
                        //*  TEXT OBJECT (083) RECORDS
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK
                        sub_Write_Work();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_TEXT"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_TEXT"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  READ-TEXT.
                    }                                                                                                                                                     //Natural: END-READ
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_DCMT"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_DCMT"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*      --------------------------------------------------------------
                    //*      END OF ALL TEXT OBJECTS FOR THIS DOCUMENT; FINISH DOCUMENT
                    //*      --------------------------------------------------------------
                    pnd_Work_Pnd_Work_Isn.setValue(pnd_Isn_Dcmt);                                                                                                         //Natural: MOVE #ISN-DCMT TO #WORK-ISN
                    pnd_Work_Pnd_Work_Key.setValue(cwf_Dcmt_Document_Key);                                                                                                //Natural: MOVE CWF-DCMT.DOCUMENT-KEY TO #WORK-KEY #DCMT-KEY
                    pnd_Dcmt_Key.setValue(cwf_Dcmt_Document_Key);
                    //*  LIST AND DELETE; DO NOT ARCHIVE!!
                    if (condition(cwf_Dcmt_Pnd_Doc_Kdo.equals("K")))                                                                                                      //Natural: IF CWF-DCMT.#DOC-KDO = 'K'
                    {
                        pnd_Prt3_Desc.setValue("KDO  Dcmt");                                                                                                              //Natural: MOVE 'KDO  Dcmt' TO #PRT3-DESC
                                                                                                                                                                          //Natural: PERFORM PRINT-EXCEPTION
                        sub_Print_Exception();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_DCMT"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_DCMT"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Kdo_Not_Cab.nadd(1);                                                                                                                          //Natural: ADD 1 TO #KDO-NOT-CAB
                        pnd_Tbl_Data_Field_Pnd_Kdo_Not_Run.nadd(1);                                                                                                       //Natural: ADD 1 TO #KDO-NOT-RUN
                        pnd_Tbl_Data_Field_Pnd_Kdo_Not_Tot.nadd(1);                                                                                                       //Natural: ADD 1 TO #KDO-NOT-TOT
                        //*  NOT KDO - OK TO ARCHIVE!!
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        vw_cwf_Arch_Dcmt.startDatabaseFind                                                                                                                //Natural: FIND ( 1 ) CWF-ARCH-DCMT WITH DOCUMENT-KEY = #DCMT-KEY
                        (
                        "FIND_DCMT",
                        new Wc[] { new Wc("DOCUMENT_KEY", "=", pnd_Dcmt_Key.getBinary(), WcType.WITH) },
                        1
                        );
                        FIND_DCMT:
                        while (condition(vw_cwf_Arch_Dcmt.readNextRow("FIND_DCMT", true)))
                        {
                            vw_cwf_Arch_Dcmt.setIfNotFoundControlFlag(false);
                            if (condition(vw_cwf_Arch_Dcmt.getAstCOUNTER().equals(0)))                                                                                    //Natural: IF NO RECORD FOUND
                            {
                                vw_cwf_Arch_Dcmt.setValuesByName(vw_cwf_Dcmt);                                                                                            //Natural: MOVE BY NAME CWF-DCMT TO CWF-ARCH-DCMT
                                vw_cwf_Arch_Dcmt.insertDBRow();                                                                                                           //Natural: STORE CWF-ARCH-DCMT
                                if (condition(true)) break;                                                                                                               //Natural: ESCAPE BOTTOM
                            }                                                                                                                                             //Natural: END-NOREC
                            //*  --------------------------------------    /* DUPLICATE DOCUMENTS?
                            if (condition((cwf_Dcmt_Image_Min.notEquals(cwf_Arch_Dcmt_Image_Min)) || (cwf_Dcmt_Pnd_Dcmt_Type.notEquals(cwf_Arch_Dcmt_Pnd_Dcmt_Type))))    //Natural: IF ( CWF-DCMT.IMAGE-MIN NE CWF-ARCH-DCMT.IMAGE-MIN ) OR ( CWF-DCMT.#DCMT-TYPE NE CWF-ARCH-DCMT.#DCMT-TYPE )
                            {
                                FOR01:                                                                                                                                    //Natural: FOR #X = 1 TO 20
                                for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(20)); pnd_X.nadd(1))
                                {
                                    pnd_Dcmt_Key_Pnd_Entry_Dte_Tme.nadd(10);                                                                                              //Natural: ADD 10 TO #DCMT-KEY.#ENTRY-DTE-TME
                                    vw_cwf_Arch_Dcmt_Histo.createHistogram                                                                                                //Natural: HISTOGRAM ( 1 ) CWF-ARCH-DCMT-HISTO DOCUMENT-KEY FROM #DCMT-KEY THRU #DCMT-KEY
                                    (
                                    "PND_PND_L6950",
                                    "DOCUMENT_KEY",
                                    new Wc[] { new Wc("DOCUMENT_KEY", ">=", pnd_Dcmt_Key.getBinary(), "And", WcType.WITH) ,
                                    new Wc("DOCUMENT_KEY", "<=", pnd_Dcmt_Key.getBinary(), WcType.WITH) },
                                    1
                                    );
                                    PND_PND_L6950:
                                    while (condition(vw_cwf_Arch_Dcmt_Histo.readNextRow("PND_PND_L6950")))
                                    {
                                    }                                                                                                                                     //Natural: END-HISTOGRAM
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom())) break;
                                        else if (condition(Global.isEscapeBottomImmediate())) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                    //*   PIN-EXP
                                    if (condition(vw_cwf_Arch_Dcmt_Histo.getAstNUMBER().equals(getZero())))                                                               //Natural: IF *NUMBER ( ##L6950. ) = 0
                                    {
                                        if (condition(true)) break;                                                                                                       //Natural: ESCAPE BOTTOM
                                    }                                                                                                                                     //Natural: END-IF
                                }                                                                                                                                         //Natural: END-FOR
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom("FIND_DCMT"))) break;
                                    else if (condition(Global.isEscapeBottomImmediate("FIND_DCMT"))) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                vw_cwf_Arch_Dcmt.setValuesByName(vw_cwf_Dcmt);                                                                                            //Natural: MOVE BY NAME CWF-DCMT TO CWF-ARCH-DCMT
                                cwf_Arch_Dcmt_Entry_Dte_Tme.setValue(pnd_Dcmt_Key_Pnd_Entry_Dte_Tme);                                                                     //Natural: MOVE #DCMT-KEY.#ENTRY-DTE-TME TO CWF-ARCH-DCMT.ENTRY-DTE-TME
                                vw_cwf_Arch_Dcmt.insertDBRow();                                                                                                           //Natural: STORE CWF-ARCH-DCMT
                                pnd_Prt3_Desc.setValue("Dupl Dcmt");                                                                                                      //Natural: MOVE 'Dupl Dcmt' TO #PRT3-DESC
                                                                                                                                                                          //Natural: PERFORM PRINT-EXCEPTION
                                sub_Print_Exception();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom("FIND_DCMT"))) break;
                                    else if (condition(Global.isEscapeBottomImmediate("FIND_DCMT"))) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                //*  --------------------------------------    /* TRUE DUPLICATE
                                vw_cwf_Arch_Dcmt.setValuesByName(vw_cwf_Dcmt);                                                                                            //Natural: MOVE BY NAME CWF-DCMT TO CWF-ARCH-DCMT
                                vw_cwf_Arch_Dcmt.updateDBRow("FIND_DCMT");                                                                                                //Natural: UPDATE ( FIND-DCMT. )
                                pnd_Prt3_Desc.setValue("Arch Dcmt");                                                                                                      //Natural: MOVE 'Arch Dcmt' TO #PRT3-DESC
                                                                                                                                                                          //Natural: PERFORM PRINT-EXCEPTION
                                sub_Print_Exception();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom("FIND_DCMT"))) break;
                                    else if (condition(Global.isEscapeBottomImmediate("FIND_DCMT"))) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                pnd_Work_Pnd_Work_Upd.nadd(1);                                                                                                            //Natural: ADD 1 TO #WORK-UPD
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-FIND
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_DCMT"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_DCMT"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  ROXANNE
                        pnd_Et_Count.nadd(1);                                                                                                                             //Natural: ADD 1 TO #ET-COUNT
                        if (condition(pnd_Et_Count.greater(100)))                                                                                                         //Natural: IF #ET-COUNT > 100
                        {
                            pnd_Et_Count.reset();                                                                                                                         //Natural: RESET #ET-COUNT
                            getCurrentProcessState().getDbConv().dbCommit();                                                                                              //Natural: END TRANSACTION
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*  ---------
                    pnd_082_Cab.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #082-CAB
                    pnd_Tbl_Data_Field_Pnd_082_Run.nadd(1);                                                                                                               //Natural: ADD 1 TO #082-RUN
                    pnd_Tbl_Data_Field_Pnd_082_Tot.nadd(1);                                                                                                               //Natural: ADD 1 TO #082-TOT
                    pnd_Work_Pnd_Work_File.setValue("082");                                                                                                               //Natural: MOVE '082' TO #WORK-FILE
                    pnd_Et_Cnt.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #ET-CNT
                    //*  DOCUMENT (082) RECORDS
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK
                    sub_Write_Work();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_DCMT"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_DCMT"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  -----------------------
                    //*  IMAGE RECORDS
                    if (condition(cwf_Dcmt_Pnd_Doc_Img.equals("I")))                                                                                                      //Natural: IF CWF-DCMT.#DOC-IMG = 'I'
                    {
                        pnd_186_Cab.nadd(1);                                                                                                                              //Natural: ADD 1 TO #186-CAB
                        pnd_Tbl_Data_Field_Pnd_186_Run.nadd(1);                                                                                                           //Natural: ADD 1 TO #186-RUN
                        pnd_Tbl_Data_Field_Pnd_186_Tot.nadd(1);                                                                                                           //Natural: ADD 1 TO #186-TOT
                        pnd_Source_Id_Min_Key_Pnd_Source_Id.setValue(cwf_Dcmt_Image_Source_Id);                                                                           //Natural: MOVE CWF-DCMT.IMAGE-SOURCE-ID TO #SOURCE-ID
                        pnd_Source_Id_Min_Key_Pnd_Mail_Item_Nbr.setValue(cwf_Dcmt_Image_Min);                                                                             //Natural: MOVE CWF-DCMT.IMAGE-MIN TO #MAIL-ITEM-NBR
                        pnd_Work_Pnd_Work_Key.setValue(pnd_Source_Id_Min_Key);                                                                                            //Natural: MOVE #SOURCE-ID-MIN-KEY TO #WORK-KEY
                        pnd_Work_Pnd_Work_Isn.reset();                                                                                                                    //Natural: RESET #WORK-ISN
                        pnd_Work_Pnd_Work_File.setValue("186");                                                                                                           //Natural: MOVE '186' TO #WORK-FILE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK
                        sub_Write_Work();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_DCMT"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_DCMT"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    //*  READ-DCMT.
                }                                                                                                                                                         //Natural: END-READ
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_FLDR"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_FLDR"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*    --------------------------------------------------------------
                //*    END OF ALL DOCUMENTS FOR THIS FOLDER; FINISH FOLDER
                //*    --------------------------------------------------------------
                pnd_Work_Pnd_Work_Isn.setValue(pnd_Isn_Fldr);                                                                                                             //Natural: MOVE #ISN-FLDR TO #WORK-ISN
                pnd_Work_Pnd_Work_Key.setValue(cwf_Fldr_Folder_Key);                                                                                                      //Natural: MOVE CWF-FLDR.FOLDER-KEY TO #WORK-KEY #FLDR-KEY
                pnd_Dcmt_Key_Pnd_Fldr_Key.setValue(cwf_Fldr_Folder_Key);
                vw_cwf_Arch_Fldr.startDatabaseFind                                                                                                                        //Natural: FIND ( 1 ) CWF-ARCH-FLDR WITH FOLDER-KEY = #FLDR-KEY
                (
                "FIND_FLDR",
                new Wc[] { new Wc("FOLDER_KEY", "=", pnd_Dcmt_Key_Pnd_Fldr_Key.getBinary(), WcType.WITH) },
                1
                );
                FIND_FLDR:
                while (condition(vw_cwf_Arch_Fldr.readNextRow("FIND_FLDR", true)))
                {
                    vw_cwf_Arch_Fldr.setIfNotFoundControlFlag(false);
                    if (condition(vw_cwf_Arch_Fldr.getAstCOUNTER().equals(0)))                                                                                            //Natural: IF NO RECORD FOUND
                    {
                        vw_cwf_Arch_Fldr.setValuesByName(vw_cwf_Fldr);                                                                                                    //Natural: MOVE BY NAME CWF-FLDR TO CWF-ARCH-FLDR
                        cwf_Arch_Fldr_Fldr_In_Arch_Ind.setValue("Y");                                                                                                     //Natural: MOVE 'Y' TO CWF-ARCH-FLDR.FLDR-IN-ARCH-IND
                        vw_cwf_Arch_Fldr.insertDBRow();                                                                                                                   //Natural: STORE CWF-ARCH-FLDR
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-NOREC
                    //*  ---------
                    //*  PREVIOUSLY ARCHIVED
                    if (condition(cwf_Fldr_Fldr_In_Arch_Ind.equals("Y")))                                                                                                 //Natural: IF CWF-FLDR.FLDR-IN-ARCH-IND = 'Y'
                    {
                        pnd_Dcmt_Key_Pnd_Entry_Dte_Tme.reset();                                                                                                           //Natural: RESET #DCMT-KEY.#ENTRY-DTE-TME
                        pnd_Prt3_Desc.setValue("Yrch Fldr");                                                                                                              //Natural: MOVE 'Yrch Fldr' TO #PRT3-DESC
                                                                                                                                                                          //Natural: PERFORM PRINT-EXCEPTION
                        sub_Print_Exception();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FIND_FLDR"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FIND_FLDR"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Fld_Not_Cab.nadd(1);                                                                                                                          //Natural: ADD 1 TO #FLD-NOT-CAB
                        pnd_Tbl_Data_Field_Pnd_Fld_Not_Run.nadd(1);                                                                                                       //Natural: ADD 1 TO #FLD-NOT-RUN
                        pnd_Tbl_Data_Field_Pnd_Fld_Not_Tot.nadd(1);                                                                                                       //Natural: ADD 1 TO #FLD-NOT-TOT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        vw_cwf_Arch_Fldr.setValuesByName(vw_cwf_Fldr);                                                                                                    //Natural: MOVE BY NAME CWF-FLDR TO CWF-ARCH-FLDR
                        cwf_Arch_Fldr_Fldr_In_Arch_Ind.setValue("Y");                                                                                                     //Natural: MOVE 'Y' TO CWF-ARCH-FLDR.FLDR-IN-ARCH-IND
                        vw_cwf_Arch_Fldr.updateDBRow("FIND_FLDR");                                                                                                        //Natural: UPDATE ( FIND-FLDR. )
                        pnd_Prt3_Desc.setValue("Nrch Fldr");                                                                                                              //Natural: MOVE 'Nrch Fldr' TO #PRT3-DESC
                                                                                                                                                                          //Natural: PERFORM PRINT-EXCEPTION
                        sub_Print_Exception();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FIND_FLDR"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FIND_FLDR"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Work_Pnd_Work_Upd.nadd(1);                                                                                                                    //Natural: ADD 1 TO #WORK-UPD
                    }                                                                                                                                                     //Natural: END-IF
                    //*  ---------
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_FLDR"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_FLDR"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_081_Cab.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #081-CAB
                pnd_Tbl_Data_Field_Pnd_081_Run.nadd(1);                                                                                                                   //Natural: ADD 1 TO #081-RUN
                pnd_Tbl_Data_Field_Pnd_081_Tot.nadd(1);                                                                                                                   //Natural: ADD 1 TO #081-TOT
                pnd_Work_Pnd_Work_File.setValue("081");                                                                                                                   //Natural: MOVE '081' TO #WORK-FILE
                pnd_Et_Cnt.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #ET-CNT
                //*  FOLDER (081) RECORDS
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK
                sub_Write_Work();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_FLDR"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_FLDR"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  CHECK FOR ET - 5/6/97
                if (condition((pnd_Bt_Cnt.greater(2000)) || (pnd_Et_Cnt.greater(pnd_Tbl_Data_Field_Pnd_Et_Limit))))                                                       //Natural: IF ( #BT-CNT > 2000 ) OR ( #ET-CNT > #ET-LIMIT )
                {
                    //*  FOR ARCHIVE STORES
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION
                    pnd_Et_Tot.nadd(pnd_Et_Cnt);                                                                                                                          //Natural: ADD #ET-CNT TO #ET-TOT
                    pnd_Et_Cnt.reset();                                                                                                                                   //Natural: RESET #ET-CNT #BT-CNT
                    pnd_Bt_Cnt.reset();
                }                                                                                                                                                         //Natural: END-IF
                //*  ROXANNE
                pnd_Et_Count.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #ET-COUNT
                if (condition(pnd_Et_Count.greater(100)))                                                                                                                 //Natural: IF #ET-COUNT > 100
                {
                    pnd_Et_Count.reset();                                                                                                                                 //Natural: RESET #ET-COUNT
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION
                }                                                                                                                                                         //Natural: END-IF
                //*  READ-FLDR.
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PROG"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ========================================== END OF ALL FOLDER PINS
            //*  MOVE 'P0000000 ' TO CWF-FLDR.CABINET-ID     /* START OVER NEXT TIME
            //*  PIN-EXP
            //*  START OVER NEXT TIME
            cwf_Fldr_Cabinet_Id.setValue("P000000000000 ");                                                                                                               //Natural: MOVE 'P000000000000 ' TO CWF-FLDR.CABINET-ID
            //*  PIN-EXP
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
            //*  PROG.
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  ========================================== END OF JOB
        //*  WRITE LAST CABINET ON THE REPORT IF NOT YET REPORTED.
        if (condition(pnd_Prev_Cab.greater(" ")))                                                                                                                         //Natural: IF #PREV-CAB GT ' '
        {
                                                                                                                                                                          //Natural: PERFORM REPORT-ARCH-STAT-PER-PIN
            sub_Report_Arch_Stat_Per_Pin();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        GET_TBL:                                                                                                                                                          //Natural: GET CWF-TBL #ISN-TBL
        vw_cwf_Tbl.readByID(pnd_Isn_Tbl.getLong(), "GET_TBL");
        pnd_Tbl_Data_Field_Pnd_Tbl_Last_Cab.setValue(cwf_Fldr_Cabinet_Id);                                                                                                //Natural: MOVE CWF-FLDR.CABINET-ID TO #TBL-LAST-CAB
        pnd_Tbl_Data_Field_Pnd_Tbl_Run_Dte.setValue(Global.getDATU());                                                                                                    //Natural: MOVE *DATU TO #TBL-RUN-DTE
        cwf_Tbl_Tbl_Data_Field.setValue(pnd_Tbl_Data_Field);                                                                                                              //Natural: MOVE #TBL-DATA-FIELD TO CWF-TBL.TBL-DATA-FIELD
        cwf_Tbl_Tbl_Updte_Dte_Tme.setValue(Global.getTIMX());                                                                                                             //Natural: MOVE *TIMX TO CWF-TBL.TBL-UPDTE-DTE-TME
        cwf_Tbl_Tbl_Updte_Dte.setValue(Global.getDATX());                                                                                                                 //Natural: MOVE *DATX TO CWF-TBL.TBL-UPDTE-DTE
        cwf_Tbl_Tbl_Updte_Oprtr_Cde.setValue(Global.getINIT_USER());                                                                                                      //Natural: MOVE *INIT-USER TO CWF-TBL.TBL-UPDTE-OPRTR-CDE
        vw_cwf_Tbl.updateDBRow("GET_TBL");                                                                                                                                //Natural: UPDATE ( GET-TBL. )
        //* * WRITE(1) 'Updt tbl ' CWF-TBL.TBL-DATA-FIELD (AL=65)
        //*  FOR TBL UPDATE
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        getReports().write(1, ReportOption.NOTITLE,pnd_Head2_Pnd_H2a,pnd_Head2_Pnd_H2b);                                                                                  //Natural: WRITE ( 1 ) #HEAD2
        if (Global.isEscape()) return;
        pnd_Prt1_Lne_P_Cab.setValue("Run total");                                                                                                                         //Natural: MOVE 'Run total' TO P-CAB
        pnd_Prt1_Lne_P_081.setValue(pnd_Tbl_Data_Field_Pnd_081_Run);                                                                                                      //Natural: MOVE #081-RUN TO P-081
        pnd_Prt1_Lne_P_082.setValue(pnd_Tbl_Data_Field_Pnd_082_Run);                                                                                                      //Natural: MOVE #082-RUN TO P-082
        pnd_Prt1_Lne_P_081_Not.setValue(pnd_Tbl_Data_Field_Pnd_Fld_Not_Run);                                                                                              //Natural: MOVE #FLD-NOT-RUN TO P-081-NOT
        pnd_Prt1_Lne_P_083.setValue(pnd_Tbl_Data_Field_Pnd_083_Run);                                                                                                      //Natural: MOVE #083-RUN TO P-083
        pnd_Prt1_Lne_P_186.setValue(pnd_Tbl_Data_Field_Pnd_186_Run);                                                                                                      //Natural: MOVE #186-RUN TO P-186
        pnd_Prt1_Lne_P_Kdo_Not.setValue(pnd_Tbl_Data_Field_Pnd_Kdo_Not_Run);                                                                                              //Natural: MOVE #KDO-NOT-RUN TO P-KDO-NOT
        pnd_Prt1_Lne_P_Mit.setValue(pnd_Tbl_Data_Field_Pnd_Mit_Run);                                                                                                      //Natural: MOVE #MIT-RUN TO P-MIT
        getReports().write(1, ReportOption.NOTITLE,pnd_Prt1_Lne_P_Cab,pnd_Prt1_Lne_P_081,pnd_Prt1_Lne_P_081_Not,pnd_Prt1_Lne_P_082,pnd_Prt1_Lne_P_186,                    //Natural: WRITE ( 1 ) #PRT1-LNE
            pnd_Prt1_Lne_P_Kdo_Not,pnd_Prt1_Lne_P_083,pnd_Prt1_Lne_P_Mit);
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,pnd_Head2_Pnd_H2a,pnd_Head2_Pnd_H2b);                                                                                  //Natural: WRITE ( 1 ) #HEAD2
        if (Global.isEscape()) return;
        pnd_Prt1_Lne_P_Cab.setValue("Job total");                                                                                                                         //Natural: MOVE 'Job total' TO P-CAB
        pnd_Prt1_Lne_P_081.setValue(pnd_Tbl_Data_Field_Pnd_081_Tot);                                                                                                      //Natural: MOVE #081-TOT TO P-081
        pnd_Prt1_Lne_P_082.setValue(pnd_Tbl_Data_Field_Pnd_082_Tot);                                                                                                      //Natural: MOVE #082-TOT TO P-082
        pnd_Prt1_Lne_P_081_Not.setValue(pnd_Tbl_Data_Field_Pnd_Fld_Not_Tot);                                                                                              //Natural: MOVE #FLD-NOT-TOT TO P-081-NOT
        pnd_Prt1_Lne_P_083.setValue(pnd_Tbl_Data_Field_Pnd_083_Tot);                                                                                                      //Natural: MOVE #083-TOT TO P-083
        pnd_Prt1_Lne_P_186.setValue(pnd_Tbl_Data_Field_Pnd_186_Tot);                                                                                                      //Natural: MOVE #186-TOT TO P-186
        pnd_Prt1_Lne_P_Kdo_Not.setValue(pnd_Tbl_Data_Field_Pnd_Kdo_Not_Tot);                                                                                              //Natural: MOVE #KDO-NOT-TOT TO P-KDO-NOT
        pnd_Prt1_Lne_P_Mit.setValue(pnd_Tbl_Data_Field_Pnd_Mit_Tot);                                                                                                      //Natural: MOVE #MIT-TOT TO P-MIT
        getReports().write(1, ReportOption.NOTITLE,pnd_Prt1_Lne_P_Cab,pnd_Prt1_Lne_P_081,pnd_Prt1_Lne_P_081_Not,pnd_Prt1_Lne_P_082,pnd_Prt1_Lne_P_186,                    //Natural: WRITE ( 1 ) #PRT1-LNE
            pnd_Prt1_Lne_P_Kdo_Not,pnd_Prt1_Lne_P_083,pnd_Prt1_Lne_P_Mit);
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,pnd_Tbl_Data_Pnd_Tab1,pnd_Tbl_Data_Pnd_Tab2);                                                          //Natural: WRITE ( 1 ) // #TBL-DATA
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,pnd_Tbl_Und_Pnd_Und1,pnd_Tbl_Und_Pnd_Und2);                                                                            //Natural: WRITE ( 1 ) #TBL-UND
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,pnd_Tbl_Data_Field_Pnd_Tbl_Last_Cab,pnd_Tbl_Data_Field_Pnd_Tbl1);                                                      //Natural: WRITE ( 1 ) #TBL-LAST-CAB #TBL1
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"Run Time:",st, new ReportEditMask ("99:99:99:9"));                                                            //Natural: WRITE ( 1 ) / 'Run Time:' *TIMD ( ST. ) ( EM = 99:99:99:9 )
        if (Global.isEscape()) return;
        //*  ==================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WORK
        //*  ==================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-EXCEPTION
        //*  ==================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-ARCH-STAT-PER-PIN
    }
    private void sub_Write_Work() throws Exception                                                                                                                        //Natural: WRITE-WORK
    {
        if (BLNatReinput.isReinput()) return;

        getWorkFiles().write(1, false, pnd_Work);                                                                                                                         //Natural: WRITE WORK FILE 1 #WORK
        //* * WRITE(1) 'Work     ' #WORK #ET-CNT #ET-TOT
        pnd_Work.reset();                                                                                                                                                 //Natural: RESET #WORK
        //*  WRITE-WORK
    }
    private void sub_Print_Exception() throws Exception                                                                                                                   //Natural: PRINT-EXCEPTION
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pnd_Prt3_Ex.equals(true)))                                                                                                                          //Natural: IF #PRT3-EX = TRUE
        {
            pnd_Prt3_Ex.setValue(false);                                                                                                                                  //Natural: ASSIGN #PRT3-EX = FALSE
            getReports().write(3, ReportOption.NOTITLE,"      KDO Documents not Archived, and others");                                                                   //Natural: WRITE ( 3 ) NOTITLE '      KDO Documents not Archived, and others'
            if (Global.isEscape()) return;
            getReports().write(3, ReportOption.NOTITLE,"      --------------------------------------");                                                                   //Natural: WRITE ( 3 ) '      --------------------------------------'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(3, ReportOption.NOTITLE,pnd_Prt3_Desc,pnd_Work_Pnd_Work_Isn,pnd_Dcmt_Key_Pnd_Cab_Id,pnd_Dcmt_Key_Pnd_Tiaa_Rcvd_Dte, new ReportEditMask         //Natural: WRITE ( 3 ) #PRT3-DESC #WORK-ISN #DCMT-KEY.#CAB-ID #DCMT-KEY.#TIAA-RCVD-DTE ( EM = MM/DD/YYYY ) #DCMT-KEY.#CASE-WPID-ETC #DCMT-KEY.#ENTRY-DTE-TME ( EM = MM/DD/YYYY' 'HH:II:SS:T )
            ("MM/DD/YYYY"),pnd_Dcmt_Key_Pnd_Case_Wpid_Etc,pnd_Dcmt_Key_Pnd_Entry_Dte_Tme, new ReportEditMask ("MM/DD/YYYY' 'HH:II:SS:T"));
        if (Global.isEscape()) return;
        //*  PRINT-EXCEPTION
    }
    private void sub_Report_Arch_Stat_Per_Pin() throws Exception                                                                                                          //Natural: REPORT-ARCH-STAT-PER-PIN
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------------------------------------
        //*  END OF ALL FOLDERS FOR THIS CABINET; FINISH CABINET
        //*  --------------------------------------------------------------
        pnd_Prt1_Lne_P_081.setValue(pnd_081_Cab);                                                                                                                         //Natural: MOVE #081-CAB TO P-081
        pnd_Prt1_Lne_P_082.setValue(pnd_082_Cab);                                                                                                                         //Natural: MOVE #082-CAB TO P-082
        pnd_Prt1_Lne_P_081_Not.setValue(pnd_Fld_Not_Cab);                                                                                                                 //Natural: MOVE #FLD-NOT-CAB TO P-081-NOT
        pnd_Prt1_Lne_P_083.setValue(pnd_083_Cab);                                                                                                                         //Natural: MOVE #083-CAB TO P-083
        pnd_Prt1_Lne_P_186.setValue(pnd_186_Cab);                                                                                                                         //Natural: MOVE #186-CAB TO P-186
        //*  CABINET TOTALS
        pnd_Prt1_Lne_P_Kdo_Not.setValue(pnd_Kdo_Not_Cab);                                                                                                                 //Natural: MOVE #KDO-NOT-CAB TO P-KDO-NOT
        getReports().write(1, ReportOption.NOTITLE,pnd_Prt1_Lne_P_Cab,pnd_Prt1_Lne_P_081,pnd_Prt1_Lne_P_081_Not,pnd_Prt1_Lne_P_082,pnd_Prt1_Lne_P_186,                    //Natural: WRITE ( 1 ) #PRT1-LNE
            pnd_Prt1_Lne_P_Kdo_Not,pnd_Prt1_Lne_P_083,pnd_Prt1_Lne_P_Mit);
        if (Global.isEscape()) return;
        pnd_081_Cab.reset();                                                                                                                                              //Natural: RESET #081-CAB #FLD-NOT-CAB #082-CAB #083-CAB #KDO-NOT-CAB #186-CAB
        pnd_Fld_Not_Cab.reset();
        pnd_082_Cab.reset();
        pnd_083_Cab.reset();
        pnd_Kdo_Not_Cab.reset();
        pnd_186_Cab.reset();
        //*  PRINT-EXCEPTION
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    if (condition(pnd_Pge_Cnt.equals(getZero())))                                                                                                         //Natural: IF #PGE-CNT = 0
                    {
                        getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_Tbl_Data_Pnd_Tab1,pnd_Tbl_Data_Pnd_Tab2);                                                  //Natural: WRITE ( 1 ) NOTITLE / #TBL-DATA
                        getReports().write(1, ReportOption.NOTITLE,pnd_Tbl_Und_Pnd_Und1,pnd_Tbl_Und_Pnd_Und2);                                                            //Natural: WRITE ( 1 ) #TBL-UND
                        getReports().write(1, ReportOption.NOTITLE,pnd_Tbl_Data_Field_Pnd_Tbl_Last_Cab,pnd_Tbl_Data_Field_Pnd_Tbl1);                                      //Natural: WRITE ( 1 ) #TBL-LAST-CAB #TBL1
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Pge_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #PGE-CNT
                    getReports().write(1, ReportOption.NOTITLE,Global.getDATU(),"           EFM Archive - Inactive Cabinets              ",pnd_Pge_Cnt,                   //Natural: WRITE ( 1 ) NOTITLE *DATU '           EFM Archive - Inactive Cabinets              ' #PGE-CNT
                        new ReportEditMask ("ZZZZ9"));
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),"           -------------------------------");                                         //Natural: WRITE ( 1 ) *PROGRAM '           -------------------------------'
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_Head1_Pnd_H1a,pnd_Head1_Pnd_H1b);                                                              //Natural: WRITE ( 1 ) / #HEAD1
                    getReports().write(1, ReportOption.NOTITLE,pnd_Head2_Pnd_H2a,pnd_Head2_Pnd_H2b);                                                                      //Natural: WRITE ( 1 ) #HEAD2
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=58");
    }
}
