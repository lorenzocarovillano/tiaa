/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:26:29 PM
**        * FROM NATURAL PROGRAM : Cwfb3009
************************************************************
**        * FILE NAME            : Cwfb3009.java
**        * CLASS NAME           : Cwfb3009
**        * INSTANCE NAME        : Cwfb3009
************************************************************
**SAG GENERATOR: SHELL-TIAA                       VERSION: 3.2.2
**SAG TITLE: REPORT 9
**SAG SYSTEM: CRPCWF
************************************************************************
* PROGRAM  : CWFB3009
* SYSTEM   : CRPCWF
* TITLE    : REPORT 9
* GENERATED: AUG 11,93 AT 10:46 AM
* FUNCTION : REPORT OF PENDED CASES PART II - EXTERNAL
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
*
*
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3009 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Work_Record;
    private DbsField pnd_Work_Record_Tbl_Racf;
    private DbsField pnd_Work_Record_Tbl_Calendar_Days;
    private DbsField pnd_Work_Record_Tbl_Tiaa_Bsnss_Days;
    private DbsField pnd_Work_Record_Tbl_Business_Days;
    private DbsField pnd_Work_Record_Tbl_Pend_Days;
    private DbsField pnd_Work_Record_Tbl_Ext_Pend_Bsnss_Days;
    private DbsField pnd_Work_Record_Tbl_Wpid_Act;
    private DbsField pnd_Work_Record_Tbl_Wpid;

    private DbsGroup pnd_Work_Record__R_Field_1;
    private DbsField pnd_Work_Record_Tbl_Wpid_Action;
    private DbsField pnd_Work_Record_Tbl_Status_Key;

    private DbsGroup pnd_Work_Record__R_Field_2;
    private DbsField pnd_Work_Record_Tbl_Last_Unit_Cde;
    private DbsField pnd_Work_Record_Tbl_Status_Cde;
    private DbsField pnd_Work_Record_Tbl_Log_Unit_Cde;
    private DbsField pnd_Work_Record_Tbl_Pin;
    private DbsField pnd_Work_Record_Tbl_Step_Id;
    private DbsField pnd_Work_Record_Tbl_Contracts;
    private DbsField pnd_Work_Record_Tbl_Log_Dte_Tme;
    private DbsField pnd_Work_Record_Tbl_Tiaa_Dte;
    private DbsField pnd_Work_Record_Tbl_Status_Dte;
    private DbsField pnd_Work_Record_Return_Doc_Rec_Dte_Tme;
    private DbsField pnd_Work_Record_Return_Rcvd_Dte_Tme;
    private DbsField pnd_Work_Record_Tbl_Last_Chge_Dte;
    private DbsField pnd_Work_Record_Partic_Sname;

    private DbsGroup pnd_Report_Data;
    private DbsField pnd_Report_Data_Pnd_Rep_Unit;

    private DbsGroup pnd_Report_Data__R_Field_3;
    private DbsField pnd_Report_Data_Pnd_Rep_Unit_1;
    private DbsField pnd_Report_Data_Pnd_Rep_Unit_2;
    private DbsField pnd_Report_Data_Pnd_Rep_Unit_3;
    private DbsField pnd_Report_Data_Pnd_Rep_Unit_4;
    private DbsField pnd_Report_Data_Pnd_Rep_Unit_5;
    private DbsField pnd_Report_Data_Pnd_Rep_Unit_6;
    private DbsField pnd_Report_Data_Pnd_Rep_Unit_7;
    private DbsField pnd_Report_Data_Pnd_Rep_Unit_8;
    private DbsField pnd_Report_Data_Pnd_Rep_Unit_9;
    private DbsField pnd_Report_Data_Pnd_Rep_Unit_10;
    private DbsField pnd_Report_Data_Pnd_Rep_Unit_11;

    private DbsGroup pnd_Report_Data__R_Field_4;
    private DbsField pnd_Report_Data_Pnd_Rep_Unit_Cde;
    private DbsField pnd_Report_Data_Pnd_Rep_Wpid;

    private DbsGroup pnd_Report_Data__R_Field_5;
    private DbsField pnd_Report_Data_Pnd_Rep_Wpid_Actn_Rqstd_Cde;
    private DbsField pnd_Report_Data_Pnd_Rep_Wpid_Lob;
    private DbsField pnd_Report_Data_Pnd_Rep_Wpid_Mbp;
    private DbsField pnd_Report_Data_Pnd_Rep_Wpid_Sbp;
    private DbsField pnd_Report_Data_Pnd_Rep_Racf_Id;
    private DbsField pnd_Report_Data_Pnd_Rep_Wpid_Name;
    private DbsField pnd_Report_Data_Pnd_Rep_Empl_Name;
    private DbsField pnd_Report_Data_Pnd_Rep_Subtotal_Ind;

    private DbsGroup pnd_Misc_Parm;
    private DbsField pnd_Misc_Parm_Pnd_Wrk_Unit_Cde;

    private DbsGroup pnd_Misc_Parm__R_Field_6;
    private DbsField pnd_Misc_Parm_Pnd_Wrk_Unit_Id_Cde;
    private DbsField pnd_Misc_Parm_Pnd_Wrk_Unit_Suffix;
    private DbsField pnd_Misc_Parm_Pnd_Save_Action;
    private DbsField pnd_Misc_Parm_Pnd_Wpid;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Action;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Code;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Count;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Desc;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Sname;
    private DbsField pnd_Misc_Parm_Pnd_Wpid_Text;
    private DbsField pnd_Misc_Parm_Pnd_Rep_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Total_Count;
    private DbsField pnd_Misc_Parm_Pnd_Sub_Count;

    private DbsGroup pnd_Misc_Parm_Pnd_Array_Ctrs;
    private DbsField pnd_Misc_Parm_Pnd_Booklet_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Forms_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Inquire_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Research_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Trans_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Complaint_Ctr;
    private DbsField pnd_Misc_Parm_Pnd_Other_Ctr;

    private DbsGroup pnd_Misc_Parm_Pnd_Tiaa_Bsnss_Ctrs;
    private DbsField pnd_Misc_Parm_Pnd_Booklet_Bctr;
    private DbsField pnd_Misc_Parm_Pnd_Forms_Bctr;
    private DbsField pnd_Misc_Parm_Pnd_Inquire_Bctr;
    private DbsField pnd_Misc_Parm_Pnd_Research_Bctr;
    private DbsField pnd_Misc_Parm_Pnd_Trans_Bctr;
    private DbsField pnd_Misc_Parm_Pnd_Complaint_Bctr;
    private DbsField pnd_Misc_Parm_Pnd_Other_Bctr;
    private DbsField pnd_Misc_Parm_Pnd_Read_Count;
    private DbsField pnd_Misc_Parm_Pnd_Actve_Unque_Key;
    private DbsField pnd_Misc_Parm_Pnd_Todays_Time;
    private DbsField pnd_Misc_Parm_Pnd_Return_Code;
    private DbsField pnd_Misc_Parm_Pnd_Return_Msg;
    private DbsField pnd_Misc_Parm_Pnd_Status_Sname;
    private DbsField pnd_Misc_Parm_Pnd_Return_Doc_Txt;
    private DbsField pnd_Misc_Parm_Pnd_Return_Rcvd_Txt;
    private DbsField pnd_Misc_Parm_Pnd_Confirmed;

    private DbsGroup cwf_Master_Index;
    private DbsField cwf_Master_Index_Pin_Nbr;
    private DbsField cwf_Master_Index_Rqst_Log_Dte_Tme;

    private DbsGroup cwf_Master_Index__R_Field_7;
    private DbsField cwf_Master_Index_Rqst_Log_Index_Dte;
    private DbsField cwf_Master_Index_Rqst_Log_Index_Tme;
    private DbsField cwf_Master_Index_Rqst_Log_Oprtr_Cde;
    private DbsField cwf_Master_Index_Orgnl_Log_Dte_Tme;
    private DbsField cwf_Master_Index_Orgnl_Unit_Cde;
    private DbsField cwf_Master_Index_Work_Prcss_Id;

    private DbsGroup cwf_Master_Index__R_Field_8;
    private DbsField cwf_Master_Index_Work_Actn_Rqstd_Cde;
    private DbsField cwf_Master_Index_Work_Lob_Cmpny_Prdct_Cde;
    private DbsField cwf_Master_Index_Work_Mjr_Bsnss_Prcss_Cde;
    private DbsField cwf_Master_Index_Work_Spcfc_Bsnss_Prcss_Cde;
    private DbsField cwf_Master_Index_Unit_Cde;

    private DbsGroup cwf_Master_Index__R_Field_9;
    private DbsField cwf_Master_Index_Unit_Id_Cde;
    private DbsField cwf_Master_Index_Unit_Rgn_Cde;
    private DbsField cwf_Master_Index_Unit_Spcl_Dsgntn_Cde;
    private DbsField cwf_Master_Index_Unit_Brnch_Group_Cde;
    private DbsField cwf_Master_Index_Unit_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_Empl_Oprtr_Cde;

    private DbsGroup cwf_Master_Index__R_Field_10;
    private DbsField cwf_Master_Index_Empl_Racf_Id;
    private DbsField cwf_Master_Index_Empl_Sffx_Cde;
    private DbsField cwf_Master_Index_Last_Chnge_Dte_Tme;
    private DbsField cwf_Master_Index_Last_Chnge_Oprtr_Cde;
    private DbsField cwf_Master_Index_Last_Chnge_Unit_Cde;
    private DbsField cwf_Master_Index_Step_Id;
    private DbsField cwf_Master_Index_Admin_Unit_Cde;
    private DbsField cwf_Master_Index_Admin_Status_Cde;
    private DbsField cwf_Master_Index_Admin_Status_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_Admin_Status_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Index_Status_Cde;
    private DbsField cwf_Master_Index_Status_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_Status_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Index_Last_Updte_Dte;
    private DbsField cwf_Master_Index_Last_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_Last_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Index_Crprte_Status_Ind;
    private DbsField cwf_Master_Index_Tiaa_Rcvd_Dte;
    private DbsField cwf_Master_Index_Effctve_Dte;
    private DbsField cwf_Master_Index_Trans_Dte;
    private DbsField cwf_Master_Index_Cntrct_Nbr;
    private DbsField cwf_Master_Index_Tbl_Calendar_Days;
    private DbsField cwf_Master_Index_Tbl_Tiaa_Bsnss_Days;
    private DbsField cwf_Master_Index_Return_Doc_Rec_Dte_Tme;
    private DbsField cwf_Master_Index_Return_Rcvd_Dte_Tme;
    private DbsField cwf_Master_Index_Tbl_Business_Days;
    private DbsField cwf_Master_Index_Tbl_Pend_Days;
    private DbsField cwf_Master_Index_Tbl_Ext_Pend_Days;
    private DbsField cwf_Master_Index_Tbl_Ext_Pend_Bsnss_Days;
    private DbsField cwf_Master_Index_Pnd_Partic_Sname;
    private DbsField cwf_Master_Index_Work_List_Ind;
    private DbsField pnd_Env;
    private DbsField pnd_Rep_Unit_Name;
    private DbsField pnd_Report_No;
    private DbsField pnd_Racf_Id;
    private DbsField pnd_Floor;
    private DbsField pnd_Bldg;
    private DbsField pnd_Drop_Off;
    private DbsField pnd_Unit_Name;
    private DbsField pnd_New_Unit;
    private DbsField pnd_Page;
    private DbsField pnd_Report_Parm;
    private DbsField pnd_Parm_Unit;
    private DbsField pnd_Comp_Date;
    private DbsField pnd_Run_Date;
    private DbsField pnd_Tbl_Run_Flag;
    private DbsField pnd_Parm_Type;
    private DbsField pnd_Save_Racf;
    private DbsField pnd_Pers_Name;
    private DbsField pnd_Test_Wpid;
    private DbsField pnd_Test_Wpid_Act;
    private DbsField pnd_Test_Racf;
    private DbsField pnd_Test_Log_Unit;
    private DbsField pnd_Racf_Save;
    private DbsField pnd_Unit_Save;

    private DbsRecord internalLoopRecord;
    private DbsField sort01Tbl_WpidOld;
    private DbsField sort01Tbl_WpidCount565;
    private DbsField sort01Tbl_WpidCount;
    private DbsField sort01Tbl_Wpid_ActOld;
    private DbsField sort01Tbl_Log_Unit_CdeOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Work_Record = localVariables.newGroupInRecord("pnd_Work_Record", "#WORK-RECORD");
        pnd_Work_Record_Tbl_Racf = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Racf", "TBL-RACF", FieldType.STRING, 8);
        pnd_Work_Record_Tbl_Calendar_Days = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Calendar_Days", "TBL-CALENDAR-DAYS", FieldType.NUMERIC, 
            5);
        pnd_Work_Record_Tbl_Tiaa_Bsnss_Days = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Tiaa_Bsnss_Days", "TBL-TIAA-BSNSS-DAYS", FieldType.NUMERIC, 
            5);
        pnd_Work_Record_Tbl_Business_Days = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Business_Days", "TBL-BUSINESS-DAYS", FieldType.NUMERIC, 
            5, 1);
        pnd_Work_Record_Tbl_Pend_Days = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Pend_Days", "TBL-PEND-DAYS", FieldType.NUMERIC, 5, 1);
        pnd_Work_Record_Tbl_Ext_Pend_Bsnss_Days = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Ext_Pend_Bsnss_Days", "TBL-EXT-PEND-BSNSS-DAYS", 
            FieldType.NUMERIC, 5, 1);
        pnd_Work_Record_Tbl_Wpid_Act = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Wpid_Act", "TBL-WPID-ACT", FieldType.STRING, 1);
        pnd_Work_Record_Tbl_Wpid = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Wpid", "TBL-WPID", FieldType.STRING, 6);

        pnd_Work_Record__R_Field_1 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record__R_Field_1", "REDEFINE", pnd_Work_Record_Tbl_Wpid);
        pnd_Work_Record_Tbl_Wpid_Action = pnd_Work_Record__R_Field_1.newFieldInGroup("pnd_Work_Record_Tbl_Wpid_Action", "TBL-WPID-ACTION", FieldType.STRING, 
            1);
        pnd_Work_Record_Tbl_Status_Key = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Status_Key", "TBL-STATUS-KEY", FieldType.STRING, 13);

        pnd_Work_Record__R_Field_2 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record__R_Field_2", "REDEFINE", pnd_Work_Record_Tbl_Status_Key);
        pnd_Work_Record_Tbl_Last_Unit_Cde = pnd_Work_Record__R_Field_2.newFieldInGroup("pnd_Work_Record_Tbl_Last_Unit_Cde", "TBL-LAST-UNIT-CDE", FieldType.STRING, 
            8);
        pnd_Work_Record_Tbl_Status_Cde = pnd_Work_Record__R_Field_2.newFieldInGroup("pnd_Work_Record_Tbl_Status_Cde", "TBL-STATUS-CDE", FieldType.STRING, 
            4);
        pnd_Work_Record_Tbl_Log_Unit_Cde = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Log_Unit_Cde", "TBL-LOG-UNIT-CDE", FieldType.STRING, 8);
        pnd_Work_Record_Tbl_Pin = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Pin", "TBL-PIN", FieldType.NUMERIC, 12);
        pnd_Work_Record_Tbl_Step_Id = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Step_Id", "TBL-STEP-ID", FieldType.STRING, 6);
        pnd_Work_Record_Tbl_Contracts = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Contracts", "TBL-CONTRACTS", FieldType.STRING, 8);
        pnd_Work_Record_Tbl_Log_Dte_Tme = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Log_Dte_Tme", "TBL-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Work_Record_Tbl_Tiaa_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Tiaa_Dte", "TBL-TIAA-DTE", FieldType.DATE);
        pnd_Work_Record_Tbl_Status_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Status_Dte", "TBL-STATUS-DTE", FieldType.TIME);
        pnd_Work_Record_Return_Doc_Rec_Dte_Tme = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Return_Doc_Rec_Dte_Tme", "RETURN-DOC-REC-DTE-TME", FieldType.TIME);
        pnd_Work_Record_Return_Rcvd_Dte_Tme = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Return_Rcvd_Dte_Tme", "RETURN-RCVD-DTE-TME", FieldType.TIME);
        pnd_Work_Record_Tbl_Last_Chge_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Last_Chge_Dte", "TBL-LAST-CHGE-DTE", FieldType.NUMERIC, 
            15);
        pnd_Work_Record_Partic_Sname = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Partic_Sname", "PARTIC-SNAME", FieldType.STRING, 7);

        pnd_Report_Data = localVariables.newGroupInRecord("pnd_Report_Data", "#REPORT-DATA");
        pnd_Report_Data_Pnd_Rep_Unit = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Unit", "#REP-UNIT", FieldType.STRING, 88);

        pnd_Report_Data__R_Field_3 = pnd_Report_Data.newGroupInGroup("pnd_Report_Data__R_Field_3", "REDEFINE", pnd_Report_Data_Pnd_Rep_Unit);
        pnd_Report_Data_Pnd_Rep_Unit_1 = pnd_Report_Data__R_Field_3.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Unit_1", "#REP-UNIT-1", FieldType.STRING, 
            8);
        pnd_Report_Data_Pnd_Rep_Unit_2 = pnd_Report_Data__R_Field_3.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Unit_2", "#REP-UNIT-2", FieldType.STRING, 
            8);
        pnd_Report_Data_Pnd_Rep_Unit_3 = pnd_Report_Data__R_Field_3.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Unit_3", "#REP-UNIT-3", FieldType.STRING, 
            8);
        pnd_Report_Data_Pnd_Rep_Unit_4 = pnd_Report_Data__R_Field_3.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Unit_4", "#REP-UNIT-4", FieldType.STRING, 
            8);
        pnd_Report_Data_Pnd_Rep_Unit_5 = pnd_Report_Data__R_Field_3.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Unit_5", "#REP-UNIT-5", FieldType.STRING, 
            8);
        pnd_Report_Data_Pnd_Rep_Unit_6 = pnd_Report_Data__R_Field_3.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Unit_6", "#REP-UNIT-6", FieldType.STRING, 
            8);
        pnd_Report_Data_Pnd_Rep_Unit_7 = pnd_Report_Data__R_Field_3.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Unit_7", "#REP-UNIT-7", FieldType.STRING, 
            8);
        pnd_Report_Data_Pnd_Rep_Unit_8 = pnd_Report_Data__R_Field_3.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Unit_8", "#REP-UNIT-8", FieldType.STRING, 
            8);
        pnd_Report_Data_Pnd_Rep_Unit_9 = pnd_Report_Data__R_Field_3.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Unit_9", "#REP-UNIT-9", FieldType.STRING, 
            8);
        pnd_Report_Data_Pnd_Rep_Unit_10 = pnd_Report_Data__R_Field_3.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Unit_10", "#REP-UNIT-10", FieldType.STRING, 
            8);
        pnd_Report_Data_Pnd_Rep_Unit_11 = pnd_Report_Data__R_Field_3.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Unit_11", "#REP-UNIT-11", FieldType.STRING, 
            8);

        pnd_Report_Data__R_Field_4 = pnd_Report_Data.newGroupInGroup("pnd_Report_Data__R_Field_4", "REDEFINE", pnd_Report_Data_Pnd_Rep_Unit);
        pnd_Report_Data_Pnd_Rep_Unit_Cde = pnd_Report_Data__R_Field_4.newFieldArrayInGroup("pnd_Report_Data_Pnd_Rep_Unit_Cde", "#REP-UNIT-CDE", FieldType.STRING, 
            8, new DbsArrayController(1, 11));
        pnd_Report_Data_Pnd_Rep_Wpid = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Wpid", "#REP-WPID", FieldType.STRING, 6);

        pnd_Report_Data__R_Field_5 = pnd_Report_Data.newGroupInGroup("pnd_Report_Data__R_Field_5", "REDEFINE", pnd_Report_Data_Pnd_Rep_Wpid);
        pnd_Report_Data_Pnd_Rep_Wpid_Actn_Rqstd_Cde = pnd_Report_Data__R_Field_5.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Wpid_Actn_Rqstd_Cde", "#REP-WPID-ACTN-RQSTD-CDE", 
            FieldType.STRING, 1);
        pnd_Report_Data_Pnd_Rep_Wpid_Lob = pnd_Report_Data__R_Field_5.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Wpid_Lob", "#REP-WPID-LOB", FieldType.STRING, 
            2);
        pnd_Report_Data_Pnd_Rep_Wpid_Mbp = pnd_Report_Data__R_Field_5.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Wpid_Mbp", "#REP-WPID-MBP", FieldType.STRING, 
            1);
        pnd_Report_Data_Pnd_Rep_Wpid_Sbp = pnd_Report_Data__R_Field_5.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Wpid_Sbp", "#REP-WPID-SBP", FieldType.STRING, 
            2);
        pnd_Report_Data_Pnd_Rep_Racf_Id = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Racf_Id", "#REP-RACF-ID", FieldType.STRING, 8);
        pnd_Report_Data_Pnd_Rep_Wpid_Name = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Wpid_Name", "#REP-WPID-NAME", FieldType.STRING, 45);
        pnd_Report_Data_Pnd_Rep_Empl_Name = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Empl_Name", "#REP-EMPL-NAME", FieldType.STRING, 30);
        pnd_Report_Data_Pnd_Rep_Subtotal_Ind = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Subtotal_Ind", "#REP-SUBTOTAL-IND", FieldType.STRING, 
            1);

        pnd_Misc_Parm = localVariables.newGroupInRecord("pnd_Misc_Parm", "#MISC-PARM");
        pnd_Misc_Parm_Pnd_Wrk_Unit_Cde = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wrk_Unit_Cde", "#WRK-UNIT-CDE", FieldType.STRING, 8);

        pnd_Misc_Parm__R_Field_6 = pnd_Misc_Parm.newGroupInGroup("pnd_Misc_Parm__R_Field_6", "REDEFINE", pnd_Misc_Parm_Pnd_Wrk_Unit_Cde);
        pnd_Misc_Parm_Pnd_Wrk_Unit_Id_Cde = pnd_Misc_Parm__R_Field_6.newFieldInGroup("pnd_Misc_Parm_Pnd_Wrk_Unit_Id_Cde", "#WRK-UNIT-ID-CDE", FieldType.STRING, 
            5);
        pnd_Misc_Parm_Pnd_Wrk_Unit_Suffix = pnd_Misc_Parm__R_Field_6.newFieldInGroup("pnd_Misc_Parm_Pnd_Wrk_Unit_Suffix", "#WRK-UNIT-SUFFIX", FieldType.STRING, 
            3);
        pnd_Misc_Parm_Pnd_Save_Action = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Save_Action", "#SAVE-ACTION", FieldType.STRING, 1);
        pnd_Misc_Parm_Pnd_Wpid = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid", "#WPID", FieldType.STRING, 6);
        pnd_Misc_Parm_Pnd_Wpid_Action = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Action", "#WPID-ACTION", FieldType.STRING, 1);
        pnd_Misc_Parm_Pnd_Wpid_Code = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Code", "#WPID-CODE", FieldType.STRING, 6);
        pnd_Misc_Parm_Pnd_Wpid_Count = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Count", "#WPID-COUNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Misc_Parm_Pnd_Wpid_Desc = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Desc", "#WPID-DESC", FieldType.STRING, 45);
        pnd_Misc_Parm_Pnd_Wpid_Sname = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Sname", "#WPID-SNAME", FieldType.STRING, 15);
        pnd_Misc_Parm_Pnd_Wpid_Text = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Wpid_Text", "#WPID-TEXT", FieldType.STRING, 56);
        pnd_Misc_Parm_Pnd_Rep_Ctr = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Rep_Ctr", "#REP-CTR", FieldType.PACKED_DECIMAL, 2);
        pnd_Misc_Parm_Pnd_Total_Count = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Total_Count", "#TOTAL-COUNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Misc_Parm_Pnd_Sub_Count = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Sub_Count", "#SUB-COUNT", FieldType.PACKED_DECIMAL, 9);

        pnd_Misc_Parm_Pnd_Array_Ctrs = pnd_Misc_Parm.newGroupArrayInGroup("pnd_Misc_Parm_Pnd_Array_Ctrs", "#ARRAY-CTRS", new DbsArrayController(1, 7));
        pnd_Misc_Parm_Pnd_Booklet_Ctr = pnd_Misc_Parm_Pnd_Array_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Booklet_Ctr", "#BOOKLET-CTR", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Misc_Parm_Pnd_Forms_Ctr = pnd_Misc_Parm_Pnd_Array_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Forms_Ctr", "#FORMS-CTR", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Misc_Parm_Pnd_Inquire_Ctr = pnd_Misc_Parm_Pnd_Array_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Inquire_Ctr", "#INQUIRE-CTR", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Misc_Parm_Pnd_Research_Ctr = pnd_Misc_Parm_Pnd_Array_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Research_Ctr", "#RESEARCH-CTR", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Misc_Parm_Pnd_Trans_Ctr = pnd_Misc_Parm_Pnd_Array_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Trans_Ctr", "#TRANS-CTR", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Misc_Parm_Pnd_Complaint_Ctr = pnd_Misc_Parm_Pnd_Array_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Complaint_Ctr", "#COMPLAINT-CTR", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Misc_Parm_Pnd_Other_Ctr = pnd_Misc_Parm_Pnd_Array_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Other_Ctr", "#OTHER-CTR", FieldType.PACKED_DECIMAL, 
            7);

        pnd_Misc_Parm_Pnd_Tiaa_Bsnss_Ctrs = pnd_Misc_Parm.newGroupArrayInGroup("pnd_Misc_Parm_Pnd_Tiaa_Bsnss_Ctrs", "#TIAA-BSNSS-CTRS", new DbsArrayController(1, 
            2));
        pnd_Misc_Parm_Pnd_Booklet_Bctr = pnd_Misc_Parm_Pnd_Tiaa_Bsnss_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Booklet_Bctr", "#BOOKLET-BCTR", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Misc_Parm_Pnd_Forms_Bctr = pnd_Misc_Parm_Pnd_Tiaa_Bsnss_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Forms_Bctr", "#FORMS-BCTR", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Misc_Parm_Pnd_Inquire_Bctr = pnd_Misc_Parm_Pnd_Tiaa_Bsnss_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Inquire_Bctr", "#INQUIRE-BCTR", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Misc_Parm_Pnd_Research_Bctr = pnd_Misc_Parm_Pnd_Tiaa_Bsnss_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Research_Bctr", "#RESEARCH-BCTR", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Misc_Parm_Pnd_Trans_Bctr = pnd_Misc_Parm_Pnd_Tiaa_Bsnss_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Trans_Bctr", "#TRANS-BCTR", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Misc_Parm_Pnd_Complaint_Bctr = pnd_Misc_Parm_Pnd_Tiaa_Bsnss_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Complaint_Bctr", "#COMPLAINT-BCTR", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Misc_Parm_Pnd_Other_Bctr = pnd_Misc_Parm_Pnd_Tiaa_Bsnss_Ctrs.newFieldInGroup("pnd_Misc_Parm_Pnd_Other_Bctr", "#OTHER-BCTR", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Misc_Parm_Pnd_Read_Count = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Read_Count", "#READ-COUNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Misc_Parm_Pnd_Actve_Unque_Key = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Actve_Unque_Key", "#ACTVE-UNQUE-KEY", FieldType.STRING, 15);
        pnd_Misc_Parm_Pnd_Todays_Time = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Todays_Time", "#TODAYS-TIME", FieldType.TIME);
        pnd_Misc_Parm_Pnd_Return_Code = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Return_Code", "#RETURN-CODE", FieldType.NUMERIC, 1);
        pnd_Misc_Parm_Pnd_Return_Msg = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Return_Msg", "#RETURN-MSG", FieldType.STRING, 20);
        pnd_Misc_Parm_Pnd_Status_Sname = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Status_Sname", "#STATUS-SNAME", FieldType.STRING, 25);
        pnd_Misc_Parm_Pnd_Return_Doc_Txt = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Return_Doc_Txt", "#RETURN-DOC-TXT", FieldType.STRING, 8);
        pnd_Misc_Parm_Pnd_Return_Rcvd_Txt = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Return_Rcvd_Txt", "#RETURN-RCVD-TXT", FieldType.STRING, 8);
        pnd_Misc_Parm_Pnd_Confirmed = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Confirmed", "#CONFIRMED", FieldType.BOOLEAN, 1);

        cwf_Master_Index = localVariables.newGroupInRecord("cwf_Master_Index", "CWF-MASTER-INDEX");
        cwf_Master_Index_Pin_Nbr = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12);
        cwf_Master_Index_Rqst_Log_Dte_Tme = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15);

        cwf_Master_Index__R_Field_7 = cwf_Master_Index.newGroupInGroup("cwf_Master_Index__R_Field_7", "REDEFINE", cwf_Master_Index_Rqst_Log_Dte_Tme);
        cwf_Master_Index_Rqst_Log_Index_Dte = cwf_Master_Index__R_Field_7.newFieldInGroup("cwf_Master_Index_Rqst_Log_Index_Dte", "RQST-LOG-INDEX-DTE", 
            FieldType.STRING, 8);
        cwf_Master_Index_Rqst_Log_Index_Tme = cwf_Master_Index__R_Field_7.newFieldInGroup("cwf_Master_Index_Rqst_Log_Index_Tme", "RQST-LOG-INDEX-TME", 
            FieldType.STRING, 7);
        cwf_Master_Index_Rqst_Log_Oprtr_Cde = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", FieldType.STRING, 
            8);
        cwf_Master_Index_Orgnl_Log_Dte_Tme = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Orgnl_Log_Dte_Tme", "ORGNL-LOG-DTE-TME", FieldType.STRING, 
            15);
        cwf_Master_Index_Orgnl_Unit_Cde = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", FieldType.STRING, 8);
        cwf_Master_Index_Work_Prcss_Id = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6);

        cwf_Master_Index__R_Field_8 = cwf_Master_Index.newGroupInGroup("cwf_Master_Index__R_Field_8", "REDEFINE", cwf_Master_Index_Work_Prcss_Id);
        cwf_Master_Index_Work_Actn_Rqstd_Cde = cwf_Master_Index__R_Field_8.newFieldInGroup("cwf_Master_Index_Work_Actn_Rqstd_Cde", "WORK-ACTN-RQSTD-CDE", 
            FieldType.STRING, 1);
        cwf_Master_Index_Work_Lob_Cmpny_Prdct_Cde = cwf_Master_Index__R_Field_8.newFieldInGroup("cwf_Master_Index_Work_Lob_Cmpny_Prdct_Cde", "WORK-LOB-CMPNY-PRDCT-CDE", 
            FieldType.STRING, 2);
        cwf_Master_Index_Work_Mjr_Bsnss_Prcss_Cde = cwf_Master_Index__R_Field_8.newFieldInGroup("cwf_Master_Index_Work_Mjr_Bsnss_Prcss_Cde", "WORK-MJR-BSNSS-PRCSS-CDE", 
            FieldType.STRING, 1);
        cwf_Master_Index_Work_Spcfc_Bsnss_Prcss_Cde = cwf_Master_Index__R_Field_8.newFieldInGroup("cwf_Master_Index_Work_Spcfc_Bsnss_Prcss_Cde", "WORK-SPCFC-BSNSS-PRCSS-CDE", 
            FieldType.STRING, 2);
        cwf_Master_Index_Unit_Cde = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8);

        cwf_Master_Index__R_Field_9 = cwf_Master_Index.newGroupInGroup("cwf_Master_Index__R_Field_9", "REDEFINE", cwf_Master_Index_Unit_Cde);
        cwf_Master_Index_Unit_Id_Cde = cwf_Master_Index__R_Field_9.newFieldInGroup("cwf_Master_Index_Unit_Id_Cde", "UNIT-ID-CDE", FieldType.STRING, 5);
        cwf_Master_Index_Unit_Rgn_Cde = cwf_Master_Index__R_Field_9.newFieldInGroup("cwf_Master_Index_Unit_Rgn_Cde", "UNIT-RGN-CDE", FieldType.STRING, 
            1);
        cwf_Master_Index_Unit_Spcl_Dsgntn_Cde = cwf_Master_Index__R_Field_9.newFieldInGroup("cwf_Master_Index_Unit_Spcl_Dsgntn_Cde", "UNIT-SPCL-DSGNTN-CDE", 
            FieldType.STRING, 1);
        cwf_Master_Index_Unit_Brnch_Group_Cde = cwf_Master_Index__R_Field_9.newFieldInGroup("cwf_Master_Index_Unit_Brnch_Group_Cde", "UNIT-BRNCH-GROUP-CDE", 
            FieldType.STRING, 1);
        cwf_Master_Index_Unit_Updte_Dte_Tme = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Unit_Updte_Dte_Tme", "UNIT-UPDTE-DTE-TME", FieldType.TIME);
        cwf_Master_Index_Empl_Oprtr_Cde = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", FieldType.STRING, 10);

        cwf_Master_Index__R_Field_10 = cwf_Master_Index.newGroupInGroup("cwf_Master_Index__R_Field_10", "REDEFINE", cwf_Master_Index_Empl_Oprtr_Cde);
        cwf_Master_Index_Empl_Racf_Id = cwf_Master_Index__R_Field_10.newFieldInGroup("cwf_Master_Index_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 
            8);
        cwf_Master_Index_Empl_Sffx_Cde = cwf_Master_Index__R_Field_10.newFieldInGroup("cwf_Master_Index_Empl_Sffx_Cde", "EMPL-SFFX-CDE", FieldType.STRING, 
            2);
        cwf_Master_Index_Last_Chnge_Dte_Tme = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 
            15);
        cwf_Master_Index_Last_Chnge_Oprtr_Cde = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Last_Chnge_Oprtr_Cde", "LAST-CHNGE-OPRTR-CDE", FieldType.STRING, 
            8);
        cwf_Master_Index_Last_Chnge_Unit_Cde = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Last_Chnge_Unit_Cde", "LAST-CHNGE-UNIT-CDE", FieldType.STRING, 
            8);
        cwf_Master_Index_Step_Id = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Step_Id", "STEP-ID", FieldType.STRING, 6);
        cwf_Master_Index_Admin_Unit_Cde = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 8);
        cwf_Master_Index_Admin_Status_Cde = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 
            4);
        cwf_Master_Index_Admin_Status_Updte_Dte_Tme = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Admin_Status_Updte_Dte_Tme", "ADMIN-STATUS-UPDTE-DTE-TME", 
            FieldType.TIME);
        cwf_Master_Index_Admin_Status_Updte_Oprtr_Cde = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Admin_Status_Updte_Oprtr_Cde", "ADMIN-STATUS-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8);
        cwf_Master_Index_Status_Cde = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Status_Cde", "STATUS-CDE", FieldType.STRING, 4);
        cwf_Master_Index_Status_Updte_Dte_Tme = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Status_Updte_Dte_Tme", "STATUS-UPDTE-DTE-TME", FieldType.TIME);
        cwf_Master_Index_Status_Updte_Oprtr_Cde = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Status_Updte_Oprtr_Cde", "STATUS-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8);
        cwf_Master_Index_Last_Updte_Dte = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Last_Updte_Dte", "LAST-UPDTE-DTE", FieldType.NUMERIC, 8);
        cwf_Master_Index_Last_Updte_Dte_Tme = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Last_Updte_Dte_Tme", "LAST-UPDTE-DTE-TME", FieldType.TIME);
        cwf_Master_Index_Last_Updte_Oprtr_Cde = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Last_Updte_Oprtr_Cde", "LAST-UPDTE-OPRTR-CDE", FieldType.STRING, 
            8);
        cwf_Master_Index_Crprte_Status_Ind = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Crprte_Status_Ind", "CRPRTE-STATUS-IND", FieldType.STRING, 
            1);
        cwf_Master_Index_Tiaa_Rcvd_Dte = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE);
        cwf_Master_Index_Effctve_Dte = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Effctve_Dte", "EFFCTVE-DTE", FieldType.DATE);
        cwf_Master_Index_Trans_Dte = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Trans_Dte", "TRANS-DTE", FieldType.DATE);
        cwf_Master_Index_Cntrct_Nbr = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Cntrct_Nbr", "CNTRCT-NBR", FieldType.STRING, 8);
        cwf_Master_Index_Tbl_Calendar_Days = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Tbl_Calendar_Days", "TBL-CALENDAR-DAYS", FieldType.NUMERIC, 
            5);
        cwf_Master_Index_Tbl_Tiaa_Bsnss_Days = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Tbl_Tiaa_Bsnss_Days", "TBL-TIAA-BSNSS-DAYS", FieldType.NUMERIC, 
            5);
        cwf_Master_Index_Return_Doc_Rec_Dte_Tme = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Return_Doc_Rec_Dte_Tme", "RETURN-DOC-REC-DTE-TME", 
            FieldType.TIME);
        cwf_Master_Index_Return_Rcvd_Dte_Tme = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Return_Rcvd_Dte_Tme", "RETURN-RCVD-DTE-TME", FieldType.TIME);
        cwf_Master_Index_Tbl_Business_Days = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Tbl_Business_Days", "TBL-BUSINESS-DAYS", FieldType.NUMERIC, 
            5, 1);
        cwf_Master_Index_Tbl_Pend_Days = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Tbl_Pend_Days", "TBL-PEND-DAYS", FieldType.NUMERIC, 5, 1);
        cwf_Master_Index_Tbl_Ext_Pend_Days = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Tbl_Ext_Pend_Days", "TBL-EXT-PEND-DAYS", FieldType.NUMERIC, 
            5, 1);
        cwf_Master_Index_Tbl_Ext_Pend_Bsnss_Days = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Tbl_Ext_Pend_Bsnss_Days", "TBL-EXT-PEND-BSNSS-DAYS", 
            FieldType.NUMERIC, 5, 1);
        cwf_Master_Index_Pnd_Partic_Sname = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Pnd_Partic_Sname", "#PARTIC-SNAME", FieldType.STRING, 7);
        cwf_Master_Index_Work_List_Ind = cwf_Master_Index.newFieldInGroup("cwf_Master_Index_Work_List_Ind", "WORK-LIST-IND", FieldType.STRING, 3);
        pnd_Env = localVariables.newFieldInRecord("pnd_Env", "#ENV", FieldType.STRING, 10);
        pnd_Rep_Unit_Name = localVariables.newFieldInRecord("pnd_Rep_Unit_Name", "#REP-UNIT-NAME", FieldType.STRING, 45);
        pnd_Report_No = localVariables.newFieldInRecord("pnd_Report_No", "#REPORT-NO", FieldType.NUMERIC, 2);
        pnd_Racf_Id = localVariables.newFieldInRecord("pnd_Racf_Id", "#RACF-ID", FieldType.STRING, 8);
        pnd_Floor = localVariables.newFieldInRecord("pnd_Floor", "#FLOOR", FieldType.NUMERIC, 2);
        pnd_Bldg = localVariables.newFieldInRecord("pnd_Bldg", "#BLDG", FieldType.STRING, 3);
        pnd_Drop_Off = localVariables.newFieldInRecord("pnd_Drop_Off", "#DROP-OFF", FieldType.STRING, 2);
        pnd_Unit_Name = localVariables.newFieldInRecord("pnd_Unit_Name", "#UNIT-NAME", FieldType.STRING, 45);
        pnd_New_Unit = localVariables.newFieldInRecord("pnd_New_Unit", "#NEW-UNIT", FieldType.BOOLEAN, 1);
        pnd_Page = localVariables.newFieldInRecord("pnd_Page", "#PAGE", FieldType.NUMERIC, 5);
        pnd_Report_Parm = localVariables.newFieldInRecord("pnd_Report_Parm", "#REPORT-PARM", FieldType.STRING, 17);
        pnd_Parm_Unit = localVariables.newFieldInRecord("pnd_Parm_Unit", "#PARM-UNIT", FieldType.STRING, 7);
        pnd_Comp_Date = localVariables.newFieldInRecord("pnd_Comp_Date", "#COMP-DATE", FieldType.STRING, 8);
        pnd_Run_Date = localVariables.newFieldInRecord("pnd_Run_Date", "#RUN-DATE", FieldType.STRING, 8);
        pnd_Tbl_Run_Flag = localVariables.newFieldInRecord("pnd_Tbl_Run_Flag", "#TBL-RUN-FLAG", FieldType.STRING, 1);
        pnd_Parm_Type = localVariables.newFieldInRecord("pnd_Parm_Type", "#PARM-TYPE", FieldType.STRING, 1);
        pnd_Save_Racf = localVariables.newFieldInRecord("pnd_Save_Racf", "#SAVE-RACF", FieldType.STRING, 8);
        pnd_Pers_Name = localVariables.newFieldInRecord("pnd_Pers_Name", "#PERS-NAME", FieldType.STRING, 30);
        pnd_Test_Wpid = localVariables.newFieldInRecord("pnd_Test_Wpid", "#TEST-WPID", FieldType.STRING, 6);
        pnd_Test_Wpid_Act = localVariables.newFieldInRecord("pnd_Test_Wpid_Act", "#TEST-WPID-ACT", FieldType.STRING, 1);
        pnd_Test_Racf = localVariables.newFieldInRecord("pnd_Test_Racf", "#TEST-RACF", FieldType.STRING, 8);
        pnd_Test_Log_Unit = localVariables.newFieldInRecord("pnd_Test_Log_Unit", "#TEST-LOG-UNIT", FieldType.STRING, 8);
        pnd_Racf_Save = localVariables.newFieldInRecord("pnd_Racf_Save", "#RACF-SAVE", FieldType.STRING, 8);
        pnd_Unit_Save = localVariables.newFieldInRecord("pnd_Unit_Save", "#UNIT-SAVE", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        sort01Tbl_WpidOld = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Wpid_OLD", "Tbl_Wpid_OLD", FieldType.STRING, 6);
        sort01Tbl_WpidCount565 = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Wpid_COUNT_565", "Tbl_Wpid_COUNT_565", FieldType.NUMERIC, 9);
        sort01Tbl_WpidCount = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Wpid_COUNT", "Tbl_Wpid_COUNT", FieldType.NUMERIC, 9);
        sort01Tbl_Wpid_ActOld = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Wpid_Act_OLD", "Tbl_Wpid_Act_OLD", FieldType.STRING, 1);
        sort01Tbl_Log_Unit_CdeOld = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Log_Unit_Cde_OLD", "Tbl_Log_Unit_Cde_OLD", FieldType.STRING, 8);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        localVariables.reset();
        pnd_Report_No.setInitialValue(9);
        pnd_Report_Parm.setInitialValue("CWFB3009D*");
        pnd_Parm_Type.setInitialValue("D");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Cwfb3009() throws Exception
    {
        super("Cwfb3009");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("CWFB3009", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        if (condition(Global.getLIBRARY_ID().equals("PROJIWF")))                                                                                                          //Natural: IF *LIBRARY-ID = 'PROJIWF'
        {
            DbsUtil.invokeMain(DbsUtil.getBlType("JSPLSCPE"), getCurrentProcessState());                                                                                  //Natural: FETCH RETURN 'JSPLSCPE'
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Cwfn3912.class , getCurrentProcessState(), pnd_Comp_Date, pnd_Parm_Type);                                                                         //Natural: CALLNAT 'CWFN3912' #COMP-DATE #PARM-TYPE
        if (condition(Global.isEscape())) return;
        DbsUtil.callnat(Cwfn3913.class , getCurrentProcessState(), pnd_Report_Parm, pnd_Racf_Id, pnd_Parm_Unit, pnd_Floor, pnd_Bldg, pnd_Drop_Off, pnd_Run_Date,          //Natural: CALLNAT 'CWFN3913' #REPORT-PARM #RACF-ID #PARM-UNIT #FLOOR #BLDG #DROP-OFF #RUN-DATE #TBL-RUN-FLAG
            pnd_Tbl_Run_Flag);
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Comp_Date.lessOrEqual(pnd_Run_Date)))                                                                                                           //Natural: IF #COMP-DATE LE #RUN-DATE
        {
            DbsUtil.terminate();  if (true) return;                                                                                                                       //Natural: TERMINATE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Run_Date.setValue(pnd_Comp_Date);                                                                                                                             //Natural: MOVE #COMP-DATE TO #RUN-DATE
        //* *********************************
        //*  COPYCODE : CWFC3000
        //*  FUNCTION : SETUP ENVONMENT MESSAGE
        //*  AUTHOR   : PATINGO, JOSEPH S.
        //* *********************************
        pnd_Env.setValue(Global.getLIBRARY_ID());                                                                                                                         //Natural: MOVE *LIBRARY-ID TO #ENV
        if (condition(pnd_Env.equals("PROJCWF") || pnd_Env.equals("PROJCWF")))                                                                                            //Natural: IF #ENV = 'PROJCWF' OR #ENV = 'PROJCWF'
        {
            pnd_Env.setValue("DEV'T ");                                                                                                                                   //Natural: MOVE 'DEV"T ' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("PRACANN")))                                                                                                                         //Natural: IF #ENV = 'PRACANN'
        {
            pnd_Env.setValue("PRACTICE");                                                                                                                                 //Natural: MOVE 'PRACTICE' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("PRODANN")))                                                                                                                         //Natural: IF #ENV = 'PRODANN'
        {
            pnd_Env.setValue(" ");                                                                                                                                        //Natural: MOVE ' ' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT034")))                                                                                                                         //Natural: IF #ENV = 'ACPT034'
        {
            pnd_Env.setValue("RGN:AT07");                                                                                                                                 //Natural: MOVE 'RGN:AT07' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT023")))                                                                                                                         //Natural: IF #ENV = 'ACPT023'
        {
            pnd_Env.setValue("RGN:AT06");                                                                                                                                 //Natural: MOVE 'RGN:AT06' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT028")))                                                                                                                         //Natural: IF #ENV = 'ACPT028'
        {
            pnd_Env.setValue("RGN:AT05");                                                                                                                                 //Natural: MOVE 'RGN:AT05' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        //* ****** END COPYCODE *************
        if (condition(Global.getLIBRARY_ID().notEquals("PRODANN")))                                                                                                       //Natural: IF *LIBRARY-ID NE 'PRODANN'
        {
            pnd_Env.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "(", pnd_Env, ")"));                                                                         //Natural: COMPRESS '(' #ENV ')' INTO #ENV LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Misc_Parm_Pnd_Todays_Time.setValue(Global.getTIMX());                                                                                                         //Natural: FORMAT ( 1 ) PS = 58 LS = 140;//Natural: MOVE *TIMX TO #TODAYS-TIME
        pnd_Save_Racf.setValue("H'FF'");                                                                                                                                  //Natural: MOVE H'FF' TO #SAVE-RACF
        READ_MASTER:                                                                                                                                                      //Natural: READ WORK 5 CWF-MASTER-INDEX
        while (condition(getWorkFiles().read(5, cwf_Master_Index)))
        {
            if (condition(!(((cwf_Master_Index_Admin_Status_Cde.greaterOrEqual("4500") && cwf_Master_Index_Admin_Status_Cde.lessOrEqual("4997")) || (cwf_Master_Index_Status_Cde.greaterOrEqual("4500")  //Natural: ACCEPT IF CWF-MASTER-INDEX.ADMIN-STATUS-CDE = '4500' THRU '4997' OR CWF-MASTER-INDEX.STATUS-CDE = '4500' THRU '4997'
                && cwf_Master_Index_Status_Cde.lessOrEqual("4997"))))))
            {
                continue;
            }
            //* EMPL+UNIT
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-FILE
            sub_Write_Work_File();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_MASTER"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_MASTER"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  READ-MASTER.
        }                                                                                                                                                                 //Natural: END-WORK
        READ_MASTER_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_Misc_Parm_Pnd_Read_Count.equals(getZero())))                                                                                                    //Natural: IF #READ-COUNT = 0
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            DbsUtil.callnat(Cwfn3910.class , getCurrentProcessState(), pnd_Report_No, pnd_Racf_Id, pnd_Work_Record_Tbl_Log_Unit_Cde, pnd_Floor, pnd_Bldg,                 //Natural: CALLNAT 'CWFN3910' #REPORT-NO #RACF-ID TBL-LOG-UNIT-CDE #FLOOR #BLDG #DROP-OFF #RUN-DATE
                pnd_Drop_Off, pnd_Run_Date);
            if (condition(Global.isEscape())) return;
            DbsUtil.callnat(Cwfn3915.class , getCurrentProcessState(), pnd_Report_No);                                                                                    //Natural: CALLNAT 'CWFN3915' #REPORT-NO
            if (condition(Global.isEscape())) return;
            DbsUtil.callnat(Cwfn3911.class , getCurrentProcessState());                                                                                                   //Natural: CALLNAT 'CWFN3911'
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_New_Unit.setValue(true);                                                                                                                                      //Natural: MOVE TRUE TO #NEW-UNIT
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #WORK-RECORD
        while (condition(getWorkFiles().read(1, pnd_Work_Record)))
        {
            getSort().writeSortInData(pnd_Work_Record_Tbl_Log_Unit_Cde, pnd_Work_Record_Tbl_Wpid_Act, pnd_Work_Record_Tbl_Wpid, pnd_Work_Record_Tbl_Racf,                 //Natural: END-ALL
                pnd_Work_Record_Return_Doc_Rec_Dte_Tme, pnd_Work_Record_Tbl_Tiaa_Dte, pnd_Work_Record_Tbl_Step_Id, pnd_Work_Record_Tbl_Status_Cde, pnd_Work_Record_Tbl_Last_Unit_Cde, 
                pnd_Work_Record_Tbl_Calendar_Days, pnd_Work_Record_Tbl_Tiaa_Bsnss_Days, pnd_Work_Record_Tbl_Business_Days, pnd_Work_Record_Tbl_Pend_Days, 
                pnd_Work_Record_Tbl_Pin, pnd_Work_Record_Tbl_Contracts, pnd_Work_Record_Tbl_Status_Dte, pnd_Work_Record_Return_Rcvd_Dte_Tme, pnd_Work_Record_Partic_Sname);
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getSort().sortData(pnd_Work_Record_Tbl_Log_Unit_Cde, pnd_Work_Record_Tbl_Wpid_Act, pnd_Work_Record_Tbl_Wpid, pnd_Work_Record_Tbl_Racf, pnd_Work_Record_Return_Doc_Rec_Dte_Tme,  //Natural: SORT BY #WORK-RECORD.TBL-LOG-UNIT-CDE #WORK-RECORD.TBL-WPID-ACT #WORK-RECORD.TBL-WPID #WORK-RECORD.TBL-RACF #WORK-RECORD.RETURN-DOC-REC-DTE-TME #WORK-RECORD.TBL-TIAA-DTE #WORK-RECORD.TBL-STEP-ID #WORK-RECORD.TBL-STATUS-CDE USING #WORK-RECORD.TBL-LAST-UNIT-CDE #WORK-RECORD.TBL-CALENDAR-DAYS #WORK-RECORD.TBL-TIAA-BSNSS-DAYS #WORK-RECORD.TBL-BUSINESS-DAYS #WORK-RECORD.TBL-PEND-DAYS #WORK-RECORD.TBL-PIN #WORK-RECORD.TBL-CONTRACTS #WORK-RECORD.TBL-STATUS-DTE #WORK-RECORD.RETURN-RCVD-DTE-TME #WORK-RECORD.PARTIC-SNAME
            pnd_Work_Record_Tbl_Tiaa_Dte, pnd_Work_Record_Tbl_Step_Id, pnd_Work_Record_Tbl_Status_Cde);
        sort01Tbl_WpidCount565.setDec(new DbsDecimal(0));
        sort01Tbl_WpidCount.setDec(new DbsDecimal(0));
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(pnd_Work_Record_Tbl_Log_Unit_Cde, pnd_Work_Record_Tbl_Wpid_Act, pnd_Work_Record_Tbl_Wpid, pnd_Work_Record_Tbl_Racf, 
            pnd_Work_Record_Return_Doc_Rec_Dte_Tme, pnd_Work_Record_Tbl_Tiaa_Dte, pnd_Work_Record_Tbl_Step_Id, pnd_Work_Record_Tbl_Status_Cde, pnd_Work_Record_Tbl_Last_Unit_Cde, 
            pnd_Work_Record_Tbl_Calendar_Days, pnd_Work_Record_Tbl_Tiaa_Bsnss_Days, pnd_Work_Record_Tbl_Business_Days, pnd_Work_Record_Tbl_Pend_Days, pnd_Work_Record_Tbl_Pin, 
            pnd_Work_Record_Tbl_Contracts, pnd_Work_Record_Tbl_Status_Dte, pnd_Work_Record_Return_Rcvd_Dte_Tme, pnd_Work_Record_Partic_Sname)))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            sort01Tbl_WpidCount565.setInt(sort01Tbl_WpidCount565.getInt() + 1);
            sort01Tbl_WpidCount.setInt(sort01Tbl_WpidCount.getInt() + 1);
            if (condition(pnd_New_Unit.getBoolean()))                                                                                                                     //Natural: IF #NEW-UNIT
            {
                DbsUtil.callnat(Cwfn3910.class , getCurrentProcessState(), pnd_Report_No, pnd_Racf_Id, pnd_Work_Record_Tbl_Log_Unit_Cde, pnd_Floor, pnd_Bldg,             //Natural: CALLNAT 'CWFN3910' #REPORT-NO #RACF-ID TBL-LOG-UNIT-CDE #FLOOR #BLDG #DROP-OFF #RUN-DATE
                    pnd_Drop_Off, pnd_Run_Date);
                if (condition(Global.isEscape())) return;
                pnd_New_Unit.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #NEW-UNIT
            }                                                                                                                                                             //Natural: END-IF
            pnd_Report_Data_Pnd_Rep_Unit_Cde.getValue(1).setValue(pnd_Work_Record_Tbl_Log_Unit_Cde);                                                                      //Natural: MOVE TBL-LOG-UNIT-CDE TO #REP-UNIT-CDE ( 1 )
            short decideConditionsMet268 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #WORK-RECORD.TBL-EXT-PEND-BSNSS-DAYS;//Natural: VALUE 0.0:3.9
            if (condition(((pnd_Work_Record_Tbl_Ext_Pend_Bsnss_Days.greaterOrEqual(0) && pnd_Work_Record_Tbl_Ext_Pend_Bsnss_Days.lessOrEqual(3)))))
            {
                decideConditionsMet268++;
                //*  ADD DAYS PENDED LOGIC
                short decideConditionsMet271 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #WORK-RECORD.TBL-WPID-ACT;//Natural: VALUE 'B'
                if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("B"))))
                {
                    decideConditionsMet271++;
                    pnd_Misc_Parm_Pnd_Booklet_Bctr.getValue(1).nadd(1);                                                                                                   //Natural: ADD 1 TO #BOOKLET-BCTR ( 1 )
                }                                                                                                                                                         //Natural: VALUE 'F'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("F"))))
                {
                    decideConditionsMet271++;
                    pnd_Misc_Parm_Pnd_Forms_Bctr.getValue(1).nadd(1);                                                                                                     //Natural: ADD 1 TO #FORMS-BCTR ( 1 )
                }                                                                                                                                                         //Natural: VALUE 'I'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("I"))))
                {
                    decideConditionsMet271++;
                    pnd_Misc_Parm_Pnd_Inquire_Bctr.getValue(1).nadd(1);                                                                                                   //Natural: ADD 1 TO #INQUIRE-BCTR ( 1 )
                }                                                                                                                                                         //Natural: VALUE 'R'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("R"))))
                {
                    decideConditionsMet271++;
                    pnd_Misc_Parm_Pnd_Research_Bctr.getValue(1).nadd(1);                                                                                                  //Natural: ADD 1 TO #RESEARCH-BCTR ( 1 )
                }                                                                                                                                                         //Natural: VALUE 'T'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("T"))))
                {
                    decideConditionsMet271++;
                    pnd_Misc_Parm_Pnd_Trans_Bctr.getValue(1).nadd(1);                                                                                                     //Natural: ADD 1 TO #TRANS-BCTR ( 1 )
                }                                                                                                                                                         //Natural: VALUE 'X'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("X"))))
                {
                    decideConditionsMet271++;
                    pnd_Misc_Parm_Pnd_Complaint_Bctr.getValue(1).nadd(1);                                                                                                 //Natural: ADD 1 TO #COMPLAINT-BCTR ( 1 )
                }                                                                                                                                                         //Natural: VALUE 'Z'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("Z"))))
                {
                    decideConditionsMet271++;
                    pnd_Misc_Parm_Pnd_Other_Bctr.getValue(1).nadd(1);                                                                                                     //Natural: ADD 1 TO #OTHER-BCTR ( 1 )
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: VALUE 4.0:9999.9
            else if (condition(((pnd_Work_Record_Tbl_Ext_Pend_Bsnss_Days.greaterOrEqual(4) && pnd_Work_Record_Tbl_Ext_Pend_Bsnss_Days.lessOrEqual(9999)))))
            {
                decideConditionsMet268++;
                //*  ADD DAYS PENDED LOGIC
                short decideConditionsMet291 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #WORK-RECORD.TBL-WPID-ACT;//Natural: VALUE 'B'
                if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("B"))))
                {
                    decideConditionsMet291++;
                    pnd_Misc_Parm_Pnd_Booklet_Bctr.getValue(2).nadd(1);                                                                                                   //Natural: ADD 1 TO #BOOKLET-BCTR ( 2 )
                }                                                                                                                                                         //Natural: VALUE 'F'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("F"))))
                {
                    decideConditionsMet291++;
                    pnd_Misc_Parm_Pnd_Forms_Bctr.getValue(2).nadd(1);                                                                                                     //Natural: ADD 1 TO #FORMS-BCTR ( 2 )
                }                                                                                                                                                         //Natural: VALUE 'I'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("I"))))
                {
                    decideConditionsMet291++;
                    pnd_Misc_Parm_Pnd_Inquire_Bctr.getValue(2).nadd(1);                                                                                                   //Natural: ADD 1 TO #INQUIRE-BCTR ( 2 )
                }                                                                                                                                                         //Natural: VALUE 'R'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("R"))))
                {
                    decideConditionsMet291++;
                    pnd_Misc_Parm_Pnd_Research_Bctr.getValue(2).nadd(1);                                                                                                  //Natural: ADD 1 TO #RESEARCH-BCTR ( 2 )
                }                                                                                                                                                         //Natural: VALUE 'T'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("T"))))
                {
                    decideConditionsMet291++;
                    pnd_Misc_Parm_Pnd_Trans_Bctr.getValue(2).nadd(1);                                                                                                     //Natural: ADD 1 TO #TRANS-BCTR ( 2 )
                }                                                                                                                                                         //Natural: VALUE 'X'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("X"))))
                {
                    decideConditionsMet291++;
                    pnd_Misc_Parm_Pnd_Complaint_Bctr.getValue(2).nadd(1);                                                                                                 //Natural: ADD 1 TO #COMPLAINT-BCTR ( 2 )
                }                                                                                                                                                         //Natural: VALUE 'Z'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("Z"))))
                {
                    decideConditionsMet291++;
                    pnd_Misc_Parm_Pnd_Other_Bctr.getValue(2).nadd(1);                                                                                                     //Natural: ADD 1 TO #OTHER-BCTR ( 2 )
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            short decideConditionsMet312 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #WORK-RECORD.TBL-PEND-DAYS;//Natural: VALUE 0.0:7.9
            if (condition(((pnd_Work_Record_Tbl_Pend_Days.greaterOrEqual(0) && pnd_Work_Record_Tbl_Pend_Days.lessOrEqual(7)))))
            {
                decideConditionsMet312++;
                //*  ADD DAYS PENDED LOGIC
                short decideConditionsMet315 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #WORK-RECORD.TBL-WPID-ACT;//Natural: VALUE 'B'
                if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("B"))))
                {
                    decideConditionsMet315++;
                    pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue(1).nadd(1);                                                                                                    //Natural: ADD 1 TO #BOOKLET-CTR ( 1 )
                }                                                                                                                                                         //Natural: VALUE 'F'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("F"))))
                {
                    decideConditionsMet315++;
                    pnd_Misc_Parm_Pnd_Forms_Ctr.getValue(1).nadd(1);                                                                                                      //Natural: ADD 1 TO #FORMS-CTR ( 1 )
                }                                                                                                                                                         //Natural: VALUE 'I'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("I"))))
                {
                    decideConditionsMet315++;
                    pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue(1).nadd(1);                                                                                                    //Natural: ADD 1 TO #INQUIRE-CTR ( 1 )
                }                                                                                                                                                         //Natural: VALUE 'R'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("R"))))
                {
                    decideConditionsMet315++;
                    pnd_Misc_Parm_Pnd_Research_Ctr.getValue(1).nadd(1);                                                                                                   //Natural: ADD 1 TO #RESEARCH-CTR ( 1 )
                }                                                                                                                                                         //Natural: VALUE 'T'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("T"))))
                {
                    decideConditionsMet315++;
                    pnd_Misc_Parm_Pnd_Trans_Ctr.getValue(1).nadd(1);                                                                                                      //Natural: ADD 1 TO #TRANS-CTR ( 1 )
                }                                                                                                                                                         //Natural: VALUE 'X'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("X"))))
                {
                    decideConditionsMet315++;
                    pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue(1).nadd(1);                                                                                                  //Natural: ADD 1 TO #COMPLAINT-CTR ( 1 )
                }                                                                                                                                                         //Natural: VALUE 'Z'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("Z"))))
                {
                    decideConditionsMet315++;
                    pnd_Misc_Parm_Pnd_Other_Ctr.getValue(1).nadd(1);                                                                                                      //Natural: ADD 1 TO #OTHER-CTR ( 1 )
                }                                                                                                                                                         //Natural: ANY
                if (condition(decideConditionsMet315 > 0))
                {
                    pnd_Misc_Parm_Pnd_Sub_Count.nadd(1);                                                                                                                  //Natural: ADD 1 TO #SUB-COUNT
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: VALUE 8.0:14.9
            else if (condition(((pnd_Work_Record_Tbl_Pend_Days.greaterOrEqual(8) && pnd_Work_Record_Tbl_Pend_Days.lessOrEqual(14)))))
            {
                decideConditionsMet312++;
                //*  ADD DAYS PENDED LOGIC
                short decideConditionsMet337 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #WORK-RECORD.TBL-WPID-ACT;//Natural: VALUE 'B'
                if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("B"))))
                {
                    decideConditionsMet337++;
                    pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue(2).nadd(1);                                                                                                    //Natural: ADD 1 TO #BOOKLET-CTR ( 2 )
                }                                                                                                                                                         //Natural: VALUE 'F'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("F"))))
                {
                    decideConditionsMet337++;
                    pnd_Misc_Parm_Pnd_Forms_Ctr.getValue(2).nadd(1);                                                                                                      //Natural: ADD 1 TO #FORMS-CTR ( 2 )
                }                                                                                                                                                         //Natural: VALUE 'I'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("I"))))
                {
                    decideConditionsMet337++;
                    pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue(2).nadd(1);                                                                                                    //Natural: ADD 1 TO #INQUIRE-CTR ( 2 )
                }                                                                                                                                                         //Natural: VALUE 'R'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("R"))))
                {
                    decideConditionsMet337++;
                    pnd_Misc_Parm_Pnd_Research_Ctr.getValue(2).nadd(1);                                                                                                   //Natural: ADD 1 TO #RESEARCH-CTR ( 2 )
                }                                                                                                                                                         //Natural: VALUE 'T'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("T"))))
                {
                    decideConditionsMet337++;
                    pnd_Misc_Parm_Pnd_Trans_Ctr.getValue(2).nadd(1);                                                                                                      //Natural: ADD 1 TO #TRANS-CTR ( 2 )
                }                                                                                                                                                         //Natural: VALUE 'X'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("X"))))
                {
                    decideConditionsMet337++;
                    pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue(2).nadd(1);                                                                                                  //Natural: ADD 1 TO #COMPLAINT-CTR ( 2 )
                }                                                                                                                                                         //Natural: VALUE 'Z'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("Z"))))
                {
                    decideConditionsMet337++;
                    pnd_Misc_Parm_Pnd_Other_Ctr.getValue(2).nadd(1);                                                                                                      //Natural: ADD 1 TO #OTHER-CTR ( 2 )
                }                                                                                                                                                         //Natural: ANY
                if (condition(decideConditionsMet337 > 0))
                {
                    pnd_Misc_Parm_Pnd_Sub_Count.nadd(1);                                                                                                                  //Natural: ADD 1 TO #SUB-COUNT
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: VALUE 15.0:9999.9
            else if (condition(((pnd_Work_Record_Tbl_Pend_Days.greaterOrEqual(15) && pnd_Work_Record_Tbl_Pend_Days.lessOrEqual(9999)))))
            {
                decideConditionsMet312++;
                //*  ADD DAYS PENDED LOGIC
                short decideConditionsMet359 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #WORK-RECORD.TBL-WPID-ACT;//Natural: VALUE 'B'
                if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("B"))))
                {
                    decideConditionsMet359++;
                    pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue(3).nadd(1);                                                                                                    //Natural: ADD 1 TO #BOOKLET-CTR ( 3 )
                }                                                                                                                                                         //Natural: VALUE 'F'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("F"))))
                {
                    decideConditionsMet359++;
                    pnd_Misc_Parm_Pnd_Forms_Ctr.getValue(3).nadd(1);                                                                                                      //Natural: ADD 1 TO #FORMS-CTR ( 3 )
                }                                                                                                                                                         //Natural: VALUE 'I'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("I"))))
                {
                    decideConditionsMet359++;
                    pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue(3).nadd(1);                                                                                                    //Natural: ADD 1 TO #INQUIRE-CTR ( 3 )
                }                                                                                                                                                         //Natural: VALUE 'R'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("R"))))
                {
                    decideConditionsMet359++;
                    pnd_Misc_Parm_Pnd_Research_Ctr.getValue(3).nadd(1);                                                                                                   //Natural: ADD 1 TO #RESEARCH-CTR ( 3 )
                }                                                                                                                                                         //Natural: VALUE 'T'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("T"))))
                {
                    decideConditionsMet359++;
                    pnd_Misc_Parm_Pnd_Trans_Ctr.getValue(3).nadd(1);                                                                                                      //Natural: ADD 1 TO #TRANS-CTR ( 3 )
                }                                                                                                                                                         //Natural: VALUE 'X'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("X"))))
                {
                    decideConditionsMet359++;
                    pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue(3).nadd(1);                                                                                                  //Natural: ADD 1 TO #COMPLAINT-CTR ( 3 )
                }                                                                                                                                                         //Natural: VALUE 'Z'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("Z"))))
                {
                    decideConditionsMet359++;
                    pnd_Misc_Parm_Pnd_Other_Ctr.getValue(3).nadd(1);                                                                                                      //Natural: ADD 1 TO #OTHER-CTR ( 3 )
                }                                                                                                                                                         //Natural: ANY
                if (condition(decideConditionsMet359 > 0))
                {
                    pnd_Misc_Parm_Pnd_Sub_Count.nadd(1);                                                                                                                  //Natural: ADD 1 TO #SUB-COUNT
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            short decideConditionsMet382 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #WORK-RECORD.TBL-PEND-DAYS;//Natural: VALUE 0.0:29.9
            if (condition(((pnd_Work_Record_Tbl_Pend_Days.greaterOrEqual(0) && pnd_Work_Record_Tbl_Pend_Days.lessOrEqual(29)))))
            {
                decideConditionsMet382++;
                //*  ADD DAYS PENDED LOGIC
                short decideConditionsMet385 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #WORK-RECORD.TBL-WPID-ACT;//Natural: VALUE 'B'
                if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("B"))))
                {
                    decideConditionsMet385++;
                    pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue(4).nadd(1);                                                                                                    //Natural: ADD 1 TO #BOOKLET-CTR ( 4 )
                }                                                                                                                                                         //Natural: VALUE 'F'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("F"))))
                {
                    decideConditionsMet385++;
                    pnd_Misc_Parm_Pnd_Forms_Ctr.getValue(4).nadd(1);                                                                                                      //Natural: ADD 1 TO #FORMS-CTR ( 4 )
                }                                                                                                                                                         //Natural: VALUE 'I'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("I"))))
                {
                    decideConditionsMet385++;
                    pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue(4).nadd(1);                                                                                                    //Natural: ADD 1 TO #INQUIRE-CTR ( 4 )
                }                                                                                                                                                         //Natural: VALUE 'R'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("R"))))
                {
                    decideConditionsMet385++;
                    pnd_Misc_Parm_Pnd_Research_Ctr.getValue(4).nadd(1);                                                                                                   //Natural: ADD 1 TO #RESEARCH-CTR ( 4 )
                }                                                                                                                                                         //Natural: VALUE 'T'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("T"))))
                {
                    decideConditionsMet385++;
                    pnd_Misc_Parm_Pnd_Trans_Ctr.getValue(4).nadd(1);                                                                                                      //Natural: ADD 1 TO #TRANS-CTR ( 4 )
                }                                                                                                                                                         //Natural: VALUE 'X'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("X"))))
                {
                    decideConditionsMet385++;
                    pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue(4).nadd(1);                                                                                                  //Natural: ADD 1 TO #COMPLAINT-CTR ( 4 )
                }                                                                                                                                                         //Natural: VALUE 'Z'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("Z"))))
                {
                    decideConditionsMet385++;
                    pnd_Misc_Parm_Pnd_Other_Ctr.getValue(4).nadd(1);                                                                                                      //Natural: ADD 1 TO #OTHER-CTR ( 4 )
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: VALUE 30.0:59.9
            else if (condition(((pnd_Work_Record_Tbl_Pend_Days.greaterOrEqual(30) && pnd_Work_Record_Tbl_Pend_Days.lessOrEqual(59)))))
            {
                decideConditionsMet382++;
                //*  ADD DAYS PENDED LOGIC
                short decideConditionsMet405 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #WORK-RECORD.TBL-WPID-ACT;//Natural: VALUE 'B'
                if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("B"))))
                {
                    decideConditionsMet405++;
                    pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue(5).nadd(1);                                                                                                    //Natural: ADD 1 TO #BOOKLET-CTR ( 5 )
                }                                                                                                                                                         //Natural: VALUE 'F'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("F"))))
                {
                    decideConditionsMet405++;
                    pnd_Misc_Parm_Pnd_Forms_Ctr.getValue(5).nadd(1);                                                                                                      //Natural: ADD 1 TO #FORMS-CTR ( 5 )
                }                                                                                                                                                         //Natural: VALUE 'I'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("I"))))
                {
                    decideConditionsMet405++;
                    pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue(5).nadd(1);                                                                                                    //Natural: ADD 1 TO #INQUIRE-CTR ( 5 )
                }                                                                                                                                                         //Natural: VALUE 'R'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("R"))))
                {
                    decideConditionsMet405++;
                    pnd_Misc_Parm_Pnd_Research_Ctr.getValue(5).nadd(1);                                                                                                   //Natural: ADD 1 TO #RESEARCH-CTR ( 5 )
                }                                                                                                                                                         //Natural: VALUE 'T'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("T"))))
                {
                    decideConditionsMet405++;
                    pnd_Misc_Parm_Pnd_Trans_Ctr.getValue(5).nadd(1);                                                                                                      //Natural: ADD 1 TO #TRANS-CTR ( 5 )
                }                                                                                                                                                         //Natural: VALUE 'X'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("X"))))
                {
                    decideConditionsMet405++;
                    pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue(5).nadd(1);                                                                                                  //Natural: ADD 1 TO #COMPLAINT-CTR ( 5 )
                }                                                                                                                                                         //Natural: VALUE 'Z'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("Z"))))
                {
                    decideConditionsMet405++;
                    pnd_Misc_Parm_Pnd_Other_Ctr.getValue(5).nadd(1);                                                                                                      //Natural: ADD 1 TO #OTHER-CTR ( 5 )
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: VALUE 60.0:89.9
            else if (condition(((pnd_Work_Record_Tbl_Pend_Days.greaterOrEqual(60) && pnd_Work_Record_Tbl_Pend_Days.lessOrEqual(89)))))
            {
                decideConditionsMet382++;
                //*  ADD DAYS PENDED LOGIC
                short decideConditionsMet425 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #WORK-RECORD.TBL-WPID-ACT;//Natural: VALUE 'B'
                if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("B"))))
                {
                    decideConditionsMet425++;
                    pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue(6).nadd(1);                                                                                                    //Natural: ADD 1 TO #BOOKLET-CTR ( 6 )
                }                                                                                                                                                         //Natural: VALUE 'F'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("F"))))
                {
                    decideConditionsMet425++;
                    pnd_Misc_Parm_Pnd_Forms_Ctr.getValue(6).nadd(1);                                                                                                      //Natural: ADD 1 TO #FORMS-CTR ( 6 )
                }                                                                                                                                                         //Natural: VALUE 'I'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("I"))))
                {
                    decideConditionsMet425++;
                    pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue(6).nadd(1);                                                                                                    //Natural: ADD 1 TO #INQUIRE-CTR ( 6 )
                }                                                                                                                                                         //Natural: VALUE 'R'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("R"))))
                {
                    decideConditionsMet425++;
                    pnd_Misc_Parm_Pnd_Research_Ctr.getValue(6).nadd(1);                                                                                                   //Natural: ADD 1 TO #RESEARCH-CTR ( 6 )
                }                                                                                                                                                         //Natural: VALUE 'T'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("T"))))
                {
                    decideConditionsMet425++;
                    pnd_Misc_Parm_Pnd_Trans_Ctr.getValue(6).nadd(1);                                                                                                      //Natural: ADD 1 TO #TRANS-CTR ( 6 )
                }                                                                                                                                                         //Natural: VALUE 'X'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("X"))))
                {
                    decideConditionsMet425++;
                    pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue(6).nadd(1);                                                                                                  //Natural: ADD 1 TO #COMPLAINT-CTR ( 6 )
                }                                                                                                                                                         //Natural: VALUE 'Z'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("Z"))))
                {
                    decideConditionsMet425++;
                    pnd_Misc_Parm_Pnd_Other_Ctr.getValue(6).nadd(1);                                                                                                      //Natural: ADD 1 TO #OTHER-CTR ( 6 )
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: VALUE 90.0:9999.9
            else if (condition(((pnd_Work_Record_Tbl_Pend_Days.greaterOrEqual(90) && pnd_Work_Record_Tbl_Pend_Days.lessOrEqual(9999)))))
            {
                decideConditionsMet382++;
                //*  ADD DAYS PENDED LOGIC
                short decideConditionsMet445 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #WORK-RECORD.TBL-WPID-ACT;//Natural: VALUE 'B'
                if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("B"))))
                {
                    decideConditionsMet445++;
                    pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue(7).nadd(1);                                                                                                    //Natural: ADD 1 TO #BOOKLET-CTR ( 7 )
                }                                                                                                                                                         //Natural: VALUE 'F'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("F"))))
                {
                    decideConditionsMet445++;
                    pnd_Misc_Parm_Pnd_Forms_Ctr.getValue(7).nadd(1);                                                                                                      //Natural: ADD 1 TO #FORMS-CTR ( 7 )
                }                                                                                                                                                         //Natural: VALUE 'I'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("I"))))
                {
                    decideConditionsMet445++;
                    pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue(7).nadd(1);                                                                                                    //Natural: ADD 1 TO #INQUIRE-CTR ( 7 )
                }                                                                                                                                                         //Natural: VALUE 'R'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("R"))))
                {
                    decideConditionsMet445++;
                    pnd_Misc_Parm_Pnd_Research_Ctr.getValue(7).nadd(1);                                                                                                   //Natural: ADD 1 TO #RESEARCH-CTR ( 7 )
                }                                                                                                                                                         //Natural: VALUE 'T'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("T"))))
                {
                    decideConditionsMet445++;
                    pnd_Misc_Parm_Pnd_Trans_Ctr.getValue(7).nadd(1);                                                                                                      //Natural: ADD 1 TO #TRANS-CTR ( 7 )
                }                                                                                                                                                         //Natural: VALUE 'X'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("X"))))
                {
                    decideConditionsMet445++;
                    pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue(7).nadd(1);                                                                                                  //Natural: ADD 1 TO #COMPLAINT-CTR ( 7 )
                }                                                                                                                                                         //Natural: VALUE 'Z'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("Z"))))
                {
                    decideConditionsMet445++;
                    pnd_Misc_Parm_Pnd_Other_Ctr.getValue(7).nadd(1);                                                                                                      //Natural: ADD 1 TO #OTHER-CTR ( 7 )
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                //*  ADD DAYS PENDED LOGIC
                short decideConditionsMet465 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #WORK-RECORD.TBL-WPID-ACT;//Natural: VALUE 'B'
                if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("B"))))
                {
                    decideConditionsMet465++;
                    pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue(1).nadd(1);                                                                                                    //Natural: ADD 1 TO #BOOKLET-CTR ( 1 )
                }                                                                                                                                                         //Natural: VALUE 'F'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("F"))))
                {
                    decideConditionsMet465++;
                    pnd_Misc_Parm_Pnd_Forms_Ctr.getValue(1).nadd(1);                                                                                                      //Natural: ADD 1 TO #FORMS-CTR ( 1 )
                }                                                                                                                                                         //Natural: VALUE 'I'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("I"))))
                {
                    decideConditionsMet465++;
                    pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue(1).nadd(1);                                                                                                    //Natural: ADD 1 TO #INQUIRE-CTR ( 1 )
                }                                                                                                                                                         //Natural: VALUE 'R'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("R"))))
                {
                    decideConditionsMet465++;
                    pnd_Misc_Parm_Pnd_Research_Ctr.getValue(1).nadd(1);                                                                                                   //Natural: ADD 1 TO #RESEARCH-CTR ( 1 )
                }                                                                                                                                                         //Natural: VALUE 'T'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("T"))))
                {
                    decideConditionsMet465++;
                    pnd_Misc_Parm_Pnd_Trans_Ctr.getValue(1).nadd(1);                                                                                                      //Natural: ADD 1 TO #TRANS-CTR ( 1 )
                }                                                                                                                                                         //Natural: VALUE 'X'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("X"))))
                {
                    decideConditionsMet465++;
                    pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue(1).nadd(1);                                                                                                  //Natural: ADD 1 TO #COMPLAINT-CTR ( 1 )
                }                                                                                                                                                         //Natural: VALUE 'Z'
                else if (condition((pnd_Work_Record_Tbl_Wpid_Act.equals("Z"))))
                {
                    decideConditionsMet465++;
                    pnd_Misc_Parm_Pnd_Other_Ctr.getValue(1).nadd(1);                                                                                                      //Natural: ADD 1 TO #OTHER-CTR ( 1 )
                }                                                                                                                                                         //Natural: ANY
                if (condition(decideConditionsMet465 > 0))
                {
                    pnd_Misc_Parm_Pnd_Sub_Count.nadd(1);                                                                                                                  //Natural: ADD 1 TO #SUB-COUNT
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-DECIDE
            DbsUtil.callnat(Cwfn5390.class , getCurrentProcessState(), pnd_Work_Record_Tbl_Wpid, pnd_Misc_Parm_Pnd_Wpid_Sname);                                           //Natural: AT TOP OF PAGE ( 1 );//Natural: CALLNAT 'CWFN5390' #WORK-RECORD.TBL-WPID #WPID-SNAME
            if (condition(Global.isEscape())) return;
            DbsUtil.callnat(Cwfn1102.class , getCurrentProcessState(), pnd_Work_Record_Tbl_Status_Key, pnd_Misc_Parm_Pnd_Status_Sname);                                   //Natural: CALLNAT 'CWFN1102' #WORK-RECORD.TBL-STATUS-KEY #STATUS-SNAME
            if (condition(Global.isEscape())) return;
            if (condition(pnd_Work_Record_Return_Doc_Rec_Dte_Tme.greater(getZero())))                                                                                     //Natural: IF #WORK-RECORD.RETURN-DOC-REC-DTE-TME GT 0
            {
                pnd_Misc_Parm_Pnd_Return_Doc_Txt.setValueEdited(pnd_Work_Record_Return_Doc_Rec_Dte_Tme,new ReportEditMask("MM/DD/YY"));                                   //Natural: MOVE EDITED #WORK-RECORD.RETURN-DOC-REC-DTE-TME ( EM = MM/DD/YY ) TO #RETURN-DOC-TXT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Misc_Parm_Pnd_Return_Doc_Txt.setValue(" ");                                                                                                           //Natural: MOVE ' ' TO #RETURN-DOC-TXT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Record_Return_Rcvd_Dte_Tme.greater(getZero())))                                                                                        //Natural: IF #WORK-RECORD.RETURN-RCVD-DTE-TME GT 0
            {
                pnd_Misc_Parm_Pnd_Return_Rcvd_Txt.setValueEdited(pnd_Work_Record_Return_Rcvd_Dte_Tme,new ReportEditMask("MM/DD/YY"));                                     //Natural: MOVE EDITED #WORK-RECORD.RETURN-RCVD-DTE-TME ( EM = MM/DD/YY ) TO #RETURN-RCVD-TXT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Misc_Parm_Pnd_Return_Rcvd_Txt.setValue(" ");                                                                                                          //Natural: MOVE ' ' TO #RETURN-RCVD-TXT
            }                                                                                                                                                             //Natural: END-IF
            //*  IF #SAVE-RACF NE #WORK-RECORD.TBL-RACF
            //*    MOVE #WORK-RECORD.TBL-LOG-UNIT-CDE TO #PERS-NAME
            //*    MOVE #WORK-RECORD.TBL-RACF TO #SAVE-RACF
            //*    CALLNAT 'CWFN1107' #WORK-RECORD.TBL-RACF #PERS-NAME
            //*    WRITE(1) / 'EMPLOYEE    :' #PERS-NAME (AD=I) /
            //*      '=============' /
            //*    IF #SAVE-ACTION = #WORK-RECORD.TBL-WPID-ACT
            //*      DECIDE ON FIRST VALUE OF #SAVE-ACTION
            //*        VALUE 'B'
            //*          WRITE(1) / 'BOOKLETS    :' /
            //*            '-------------'
            //*        VALUE 'F'
            //*          WRITE(1) / 'FORMS       :' /
            //*            '-------------'
            //*        VALUE 'I'
            //*          WRITE(1) / 'INQUIRY     :' /
            //*            '-------------'
            //*        VALUE 'R'
            //*          WRITE(1) / 'RESEARCH    :' /
            //*            '-------------'
            //*        VALUE 'T'
            //*          WRITE(1) / 'TRANSACTIONS:' /
            //*            '-------------'
            //*        VALUE 'X'
            //*          WRITE(1) / 'COMPLAINTS  :' /
            //*            '-------------'
            //*        VALUE 'Z'
            //*          WRITE(1) / 'OTHERS      :' /
            //*            '-------------'
            //*        NONE
            //*          IGNORE
            //*      END-DECIDE
            //*    END-IF
            //*  END-IF
            if (condition(pnd_Misc_Parm_Pnd_Save_Action.notEquals(pnd_Work_Record_Tbl_Wpid_Act)))                                                                         //Natural: IF #SAVE-ACTION NE #WORK-RECORD.TBL-WPID-ACT
            {
                pnd_Misc_Parm_Pnd_Save_Action.setValue(pnd_Work_Record_Tbl_Wpid_Act);                                                                                     //Natural: MOVE #WORK-RECORD.TBL-WPID-ACT TO #SAVE-ACTION
                short decideConditionsMet543 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #SAVE-ACTION;//Natural: VALUE 'B'
                if (condition((pnd_Misc_Parm_Pnd_Save_Action.equals("B"))))
                {
                    decideConditionsMet543++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"BOOKLETS    :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'BOOKLETS    :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'F'
                else if (condition((pnd_Misc_Parm_Pnd_Save_Action.equals("F"))))
                {
                    decideConditionsMet543++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"FORMS       :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'FORMS       :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'I'
                else if (condition((pnd_Misc_Parm_Pnd_Save_Action.equals("I"))))
                {
                    decideConditionsMet543++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"INQUIRY     :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'INQUIRY     :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'R'
                else if (condition((pnd_Misc_Parm_Pnd_Save_Action.equals("R"))))
                {
                    decideConditionsMet543++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"RESEARCH    :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'RESEARCH    :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'T'
                else if (condition((pnd_Misc_Parm_Pnd_Save_Action.equals("T"))))
                {
                    decideConditionsMet543++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TRANSACTIONS:",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'TRANSACTIONS:' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'X'
                else if (condition((pnd_Misc_Parm_Pnd_Save_Action.equals("X"))))
                {
                    decideConditionsMet543++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"COMPLAINTS  :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'COMPLAINTS  :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'Z'
                else if (condition((pnd_Misc_Parm_Pnd_Save_Action.equals("Z"))))
                {
                    decideConditionsMet543++;
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"OTHERS      :",NEWLINE,"-------------");                                                          //Natural: WRITE ( 1 ) / 'OTHERS      :' / '-------------'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                //*  PIN-EXP
            }                                                                                                                                                             //Natural: END-IF
            getReports().display(1, "//RECEIVED/AT TIAA",                                                                                                                 //Natural: DISPLAY ( 1 ) '//RECEIVED/AT TIAA' #WORK-RECORD.TBL-TIAA-DTE ( EM = MM/DD/YY ) '//DOC/REC"D' #RETURN-DOC-TXT '//REC"D IN/UNIT' #RETURN-RCVD-TXT '//WORK/PROCESS' #WPID-SNAME ( AL = 12 ) '//WORK/STEP' #WORK-RECORD.TBL-STEP-ID ( IS = ON ) '///STATUS' #STATUS-SNAME ( AL = 15 ) '//STATUS/DATE' #WORK-RECORD.TBL-STATUS-DTE ( EM = MM/DD/YY ) '/CLNDR/DAYS/PENDED' #WORK-RECORD.TBL-PEND-DAYS ( EM = Z,ZZ9.9 ) 'CLNDR/DAYS/IN/TIAA' #WORK-RECORD.TBL-CALENDAR-DAYS ( EM = ZZZ9 ) 'BSNSS/DAYS/IN/TIAA' #WORK-RECORD.TBL-TIAA-BSNSS-DAYS ( EM = ZZZ9 ) '//PARTIC/NAME' PARTIC-SNAME '///PIN' #WORK-RECORD.TBL-PIN ( EM = 999999999999 ) '//CONTRACT/NUMBER' #WORK-RECORD.TBL-CONTRACTS '///EMPLOYEE' #WORK-RECORD.TBL-RACF
            		pnd_Work_Record_Tbl_Tiaa_Dte, new ReportEditMask ("MM/DD/YY"),"//DOC/REC'D",
            		pnd_Misc_Parm_Pnd_Return_Doc_Txt,"//REC'D IN/UNIT",
            		pnd_Misc_Parm_Pnd_Return_Rcvd_Txt,"//WORK/PROCESS",
            		pnd_Misc_Parm_Pnd_Wpid_Sname, new AlphanumericLength (12),"//WORK/STEP",
            		pnd_Work_Record_Tbl_Step_Id, new IdenticalSuppress(true),"///STATUS",
            		pnd_Misc_Parm_Pnd_Status_Sname, new AlphanumericLength (15),"//STATUS/DATE",
            		pnd_Work_Record_Tbl_Status_Dte, new ReportEditMask ("MM/DD/YY"),"/CLNDR/DAYS/PENDED",
            		pnd_Work_Record_Tbl_Pend_Days, new ReportEditMask ("Z,ZZ9.9"),"CLNDR/DAYS/IN/TIAA",
            		pnd_Work_Record_Tbl_Calendar_Days, new ReportEditMask ("ZZZ9"),"BSNSS/DAYS/IN/TIAA",
            		pnd_Work_Record_Tbl_Tiaa_Bsnss_Days, new ReportEditMask ("ZZZ9"),"//PARTIC/NAME",
            		pnd_Work_Record_Partic_Sname,"///PIN",
            		pnd_Work_Record_Tbl_Pin, new ReportEditMask ("999999999999"),"//CONTRACT/NUMBER",
            		pnd_Work_Record_Tbl_Contracts,"///EMPLOYEE",
            		pnd_Work_Record_Tbl_Racf);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*    '///PIN' #WORK-RECORD.TBL-PIN (EM=9999999)
            //*  READ-2.                                                                                                                                                  //Natural: AT BREAK OF #WORK-RECORD.TBL-WPID;//Natural: AT BREAK OF #WORK-RECORD.TBL-WPID-ACT;//Natural: AT BREAK OF TBL-LOG-UNIT-CDE
            sort01Tbl_WpidOld.setValue(pnd_Work_Record_Tbl_Wpid);                                                                                                         //Natural: END-SORT
            sort01Tbl_Wpid_ActOld.setValue(pnd_Work_Record_Tbl_Wpid_Act);
            sort01Tbl_Log_Unit_CdeOld.setValue(pnd_Work_Record_Tbl_Log_Unit_Cde);
        }
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            sort01Tbl_WpidCount565.resetBreak();
            sort01Tbl_WpidCount.resetBreak();
            atBreakEventSort01(endOfDataSort01);
        }
        endSort();
        pnd_Tbl_Run_Flag.setValue("Y");                                                                                                                                   //Natural: ON ERROR;//Natural: MOVE 'Y' TO #TBL-RUN-FLAG
        DbsUtil.callnat(Cwfn3914.class , getCurrentProcessState(), pnd_Report_Parm, pnd_Run_Date, pnd_Tbl_Run_Flag);                                                      //Natural: CALLNAT 'CWFN3914' #REPORT-PARM #RUN-DATE #TBL-RUN-FLAG
        if (condition(Global.isEscape())) return;
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WORK-FILE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WPID-BREAK
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WPID-ACT-BREAK
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UNIT-BREAK
    }
    private void sub_Write_Work_File() throws Exception                                                                                                                   //Natural: WRITE-WORK-FILE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Work_Record.reset();                                                                                                                                          //Natural: RESET #WORK-RECORD
        pnd_Work_Record_Tbl_Calendar_Days.setValue(cwf_Master_Index_Tbl_Calendar_Days);                                                                                   //Natural: MOVE CWF-MASTER-INDEX.TBL-CALENDAR-DAYS TO #WORK-RECORD.TBL-CALENDAR-DAYS
        pnd_Work_Record_Tbl_Tiaa_Bsnss_Days.setValue(cwf_Master_Index_Tbl_Tiaa_Bsnss_Days);                                                                               //Natural: MOVE CWF-MASTER-INDEX.TBL-TIAA-BSNSS-DAYS TO #WORK-RECORD.TBL-TIAA-BSNSS-DAYS
        pnd_Work_Record_Return_Doc_Rec_Dte_Tme.setValue(cwf_Master_Index_Return_Doc_Rec_Dte_Tme);                                                                         //Natural: MOVE CWF-MASTER-INDEX.RETURN-DOC-REC-DTE-TME TO #WORK-RECORD.RETURN-DOC-REC-DTE-TME
        pnd_Work_Record_Return_Rcvd_Dte_Tme.setValue(cwf_Master_Index_Return_Rcvd_Dte_Tme);                                                                               //Natural: MOVE CWF-MASTER-INDEX.RETURN-RCVD-DTE-TME TO #WORK-RECORD.RETURN-RCVD-DTE-TME
        pnd_Work_Record_Tbl_Business_Days.setValue(cwf_Master_Index_Tbl_Business_Days);                                                                                   //Natural: MOVE CWF-MASTER-INDEX.TBL-BUSINESS-DAYS TO #WORK-RECORD.TBL-BUSINESS-DAYS
        pnd_Work_Record_Tbl_Pend_Days.setValue(cwf_Master_Index_Tbl_Ext_Pend_Days);                                                                                       //Natural: MOVE CWF-MASTER-INDEX.TBL-EXT-PEND-DAYS TO #WORK-RECORD.TBL-PEND-DAYS
        pnd_Work_Record_Tbl_Ext_Pend_Bsnss_Days.setValue(cwf_Master_Index_Tbl_Ext_Pend_Bsnss_Days);                                                                       //Natural: MOVE CWF-MASTER-INDEX.TBL-EXT-PEND-BSNSS-DAYS TO #WORK-RECORD.TBL-EXT-PEND-BSNSS-DAYS
        pnd_Work_Record_Tbl_Wpid.setValue(cwf_Master_Index_Work_Prcss_Id);                                                                                                //Natural: MOVE CWF-MASTER-INDEX.WORK-PRCSS-ID TO #WORK-RECORD.TBL-WPID
        if (condition(DbsUtil.maskMatches(cwf_Master_Index_Work_Prcss_Id,"N.....")))                                                                                      //Natural: IF CWF-MASTER-INDEX.WORK-PRCSS-ID = MASK ( N..... )
        {
            pnd_Work_Record_Tbl_Wpid_Act.setValue("Z");                                                                                                                   //Natural: MOVE 'Z' TO #WORK-RECORD.TBL-WPID-ACT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Work_Record_Tbl_Wpid_Act.setValue(cwf_Master_Index_Work_Prcss_Id);                                                                                        //Natural: MOVE CWF-MASTER-INDEX.WORK-PRCSS-ID TO #WORK-RECORD.TBL-WPID-ACT
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Work_Record_Tbl_Pin.setValue(cwf_Master_Index_Pin_Nbr);                                                                                                       //Natural: MOVE CWF-MASTER-INDEX.PIN-NBR TO #WORK-RECORD.TBL-PIN
        pnd_Work_Record_Tbl_Last_Unit_Cde.setValue(cwf_Master_Index_Last_Chnge_Unit_Cde);                                                                                 //Natural: MOVE CWF-MASTER-INDEX.LAST-CHNGE-UNIT-CDE TO #WORK-RECORD.TBL-LAST-UNIT-CDE
        pnd_Work_Record_Tbl_Log_Unit_Cde.setValue(cwf_Master_Index_Admin_Unit_Cde);                                                                                       //Natural: MOVE CWF-MASTER-INDEX.ADMIN-UNIT-CDE TO #WORK-RECORD.TBL-LOG-UNIT-CDE
        pnd_Work_Record_Tbl_Racf.setValue(cwf_Master_Index_Empl_Racf_Id);                                                                                                 //Natural: MOVE CWF-MASTER-INDEX.EMPL-RACF-ID TO #WORK-RECORD.TBL-RACF
        pnd_Work_Record_Tbl_Last_Chge_Dte.setValue(cwf_Master_Index_Last_Chnge_Dte_Tme);                                                                                  //Natural: MOVE CWF-MASTER-INDEX.LAST-CHNGE-DTE-TME TO #WORK-RECORD.TBL-LAST-CHGE-DTE
        pnd_Work_Record_Tbl_Log_Dte_Tme.setValue(cwf_Master_Index_Rqst_Log_Dte_Tme);                                                                                      //Natural: MOVE CWF-MASTER-INDEX.RQST-LOG-DTE-TME TO #WORK-RECORD.TBL-LOG-DTE-TME
        pnd_Work_Record_Tbl_Tiaa_Dte.setValue(cwf_Master_Index_Tiaa_Rcvd_Dte);                                                                                            //Natural: MOVE CWF-MASTER-INDEX.TIAA-RCVD-DTE TO #WORK-RECORD.TBL-TIAA-DTE
        pnd_Work_Record_Tbl_Step_Id.setValue(cwf_Master_Index_Step_Id);                                                                                                   //Natural: MOVE CWF-MASTER-INDEX.STEP-ID TO #WORK-RECORD.TBL-STEP-ID
        pnd_Work_Record_Tbl_Status_Cde.setValue(cwf_Master_Index_Admin_Status_Cde);                                                                                       //Natural: MOVE CWF-MASTER-INDEX.ADMIN-STATUS-CDE TO #WORK-RECORD.TBL-STATUS-CDE
        pnd_Work_Record_Tbl_Status_Dte.setValue(cwf_Master_Index_Admin_Status_Updte_Dte_Tme);                                                                             //Natural: MOVE CWF-MASTER-INDEX.ADMIN-STATUS-UPDTE-DTE-TME TO #WORK-RECORD.TBL-STATUS-DTE
        pnd_Work_Record_Tbl_Contracts.setValue(cwf_Master_Index_Cntrct_Nbr);                                                                                              //Natural: MOVE CWF-MASTER-INDEX.CNTRCT-NBR TO #WORK-RECORD.TBL-CONTRACTS
        pnd_Work_Record_Partic_Sname.setValue(cwf_Master_Index_Pnd_Partic_Sname);                                                                                         //Natural: MOVE CWF-MASTER-INDEX.#PARTIC-SNAME TO #WORK-RECORD.PARTIC-SNAME
        getWorkFiles().write(1, false, pnd_Work_Record);                                                                                                                  //Natural: WRITE WORK FILE 1 #WORK-RECORD
        pnd_Misc_Parm_Pnd_Read_Count.nadd(1);                                                                                                                             //Natural: ADD 1 TO #READ-COUNT
        pnd_Work_Record.reset();                                                                                                                                          //Natural: RESET #WORK-RECORD
    }
    private void sub_Wpid_Break() throws Exception                                                                                                                        //Natural: WPID-BREAK
    {
        if (BLNatReinput.isReinput()) return;

        short decideConditionsMet625 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #WPID-ACTION;//Natural: VALUE 'B'
        if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("B"))))
        {
            decideConditionsMet625++;
            pnd_Misc_Parm_Pnd_Wpid_Text.setValue(DbsUtil.compress("TOTAL BOOKLETS PENDED FOR WPID", pnd_Misc_Parm_Pnd_Wpid));                                             //Natural: COMPRESS 'TOTAL BOOKLETS PENDED FOR WPID' #WPID INTO #WPID-TEXT
        }                                                                                                                                                                 //Natural: VALUE 'F'
        else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("F"))))
        {
            decideConditionsMet625++;
            pnd_Misc_Parm_Pnd_Wpid_Text.setValue(DbsUtil.compress("TOTAL FORMS PENDED FOR WPID", pnd_Misc_Parm_Pnd_Wpid));                                                //Natural: COMPRESS 'TOTAL FORMS PENDED FOR WPID' #WPID INTO #WPID-TEXT
        }                                                                                                                                                                 //Natural: VALUE 'I'
        else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("I"))))
        {
            decideConditionsMet625++;
            pnd_Misc_Parm_Pnd_Wpid_Text.setValue(DbsUtil.compress("TOTAL INQUIRY PENDED FOR WPID", pnd_Misc_Parm_Pnd_Wpid));                                              //Natural: COMPRESS 'TOTAL INQUIRY PENDED FOR WPID' #WPID INTO #WPID-TEXT
        }                                                                                                                                                                 //Natural: VALUE 'R'
        else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("R"))))
        {
            decideConditionsMet625++;
            pnd_Misc_Parm_Pnd_Wpid_Text.setValue(DbsUtil.compress("TOTAL RESEARCH PENDED FOR WPID", pnd_Misc_Parm_Pnd_Wpid));                                             //Natural: COMPRESS 'TOTAL RESEARCH PENDED FOR WPID' #WPID INTO #WPID-TEXT
        }                                                                                                                                                                 //Natural: VALUE 'T'
        else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("T"))))
        {
            decideConditionsMet625++;
            pnd_Misc_Parm_Pnd_Wpid_Text.setValue(DbsUtil.compress("TOTAL TRANSACTIONS PENDED FOR WPID", pnd_Misc_Parm_Pnd_Wpid));                                         //Natural: COMPRESS 'TOTAL TRANSACTIONS PENDED FOR WPID' #WPID INTO #WPID-TEXT
        }                                                                                                                                                                 //Natural: VALUE 'X'
        else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("X"))))
        {
            decideConditionsMet625++;
            pnd_Misc_Parm_Pnd_Wpid_Text.setValue(DbsUtil.compress("TOTAL COMPLAINTS PENDED FOR WPID", pnd_Misc_Parm_Pnd_Wpid));                                           //Natural: COMPRESS 'TOTAL COMPLAINTS PENDED FOR WPID' #WPID INTO #WPID-TEXT
        }                                                                                                                                                                 //Natural: VALUE 'Z'
        else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("Z"))))
        {
            decideConditionsMet625++;
            pnd_Misc_Parm_Pnd_Wpid_Text.setValue(DbsUtil.compress("TOTAL OTHERS PENDED FOR WPID", pnd_Misc_Parm_Pnd_Wpid));                                               //Natural: COMPRESS 'TOTAL OTHERS PENDED FOR WPID' #WPID INTO #WPID-TEXT
        }                                                                                                                                                                 //Natural: ANY
        if (condition(decideConditionsMet625 > 0))
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),pnd_Misc_Parm_Pnd_Wpid_Text,pnd_Misc_Parm_Pnd_Wpid_Count, new FieldAttributes           //Natural: WRITE ( 1 ) / 10T #WPID-TEXT #WPID-COUNT ( AD = OI ) /
                ("AD=OI"),NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Misc_Parm_Pnd_Wpid_Count.reset();                                                                                                                             //Natural: RESET #WPID-COUNT
    }
    private void sub_Wpid_Act_Break() throws Exception                                                                                                                    //Natural: WPID-ACT-BREAK
    {
        if (BLNatReinput.isReinput()) return;

        short decideConditionsMet648 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #WPID-ACTION;//Natural: VALUE 'B'
        if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("B"))))
        {
            decideConditionsMet648++;
            getReports().write(1, ReportOption.NOTITLE,"TOTAL BOOKLETS PENDED IN 7 CALENDAR DAYS OR LESS     :",pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue(1),                //Natural: WRITE ( 1 ) 'TOTAL BOOKLETS PENDED IN 7 CALENDAR DAYS OR LESS     :' #BOOKLET-CTR ( 1 ) ( AD = OU ) 03X 'TOTAL BOOKLETS PENDED FOR 0 - 29 CALENDAR DAYS       :' #BOOKLET-CTR ( 4 ) ( AD = OU ) / 'TOTAL BOOKLETS PENDED IN 8 - 14 CALENDAR DAYS        :' #BOOKLET-CTR ( 2 ) ( AD = OU ) 03X 'TOTAL BOOKLETS PENDED FOR 30 - 59 CALENDAR DAYS      :' #BOOKLET-CTR ( 5 ) ( AD = OU ) / 'TOTAL BOOKLETS PENDED IN OVER 14 CALENDAR DAYS       :' #BOOKLET-CTR ( 3 ) ( AD = OU ) 03X 'TOTAL BOOKLETS PENDED FOR 60 - 89 CALENDAR DAYS      :' #BOOKLET-CTR ( 6 ) ( AD = OU ) / 65T 'TOTAL BOOKLETS PENDED FOR OVER 90 CALENDAR DAYS      :' #BOOKLET-CTR ( 7 ) ( AD = OU ) // 'TOTAL BOOKLETS PENDED IN 3 BUSINESS DAYS OR LESS     :' #BOOKLET-BCTR ( 1 ) ( AD = OU ) / 'TOTAL BOOKLETS PENDED IN 4 BUSINESS DAYS OR MORE     :' #BOOKLET-BCTR ( 2 ) ( AD = OU ) /
                new FieldAttributes ("AD=OU"),new ColumnSpacing(3),"TOTAL BOOKLETS PENDED FOR 0 - 29 CALENDAR DAYS       :",pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue(4), 
                new FieldAttributes ("AD=OU"),NEWLINE,"TOTAL BOOKLETS PENDED IN 8 - 14 CALENDAR DAYS        :",pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue(2), 
                new FieldAttributes ("AD=OU"),new ColumnSpacing(3),"TOTAL BOOKLETS PENDED FOR 30 - 59 CALENDAR DAYS      :",pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue(5), 
                new FieldAttributes ("AD=OU"),NEWLINE,"TOTAL BOOKLETS PENDED IN OVER 14 CALENDAR DAYS       :",pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue(3), 
                new FieldAttributes ("AD=OU"),new ColumnSpacing(3),"TOTAL BOOKLETS PENDED FOR 60 - 89 CALENDAR DAYS      :",pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue(6), 
                new FieldAttributes ("AD=OU"),NEWLINE,new TabSetting(65),"TOTAL BOOKLETS PENDED FOR OVER 90 CALENDAR DAYS      :",pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue(7), 
                new FieldAttributes ("AD=OU"),NEWLINE,NEWLINE,"TOTAL BOOKLETS PENDED IN 3 BUSINESS DAYS OR LESS     :",pnd_Misc_Parm_Pnd_Booklet_Bctr.getValue(1), 
                new FieldAttributes ("AD=OU"),NEWLINE,"TOTAL BOOKLETS PENDED IN 4 BUSINESS DAYS OR MORE     :",pnd_Misc_Parm_Pnd_Booklet_Bctr.getValue(2), 
                new FieldAttributes ("AD=OU"),NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: VALUE 'F'
        else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("F"))))
        {
            decideConditionsMet648++;
            getReports().write(1, ReportOption.NOTITLE,"TOTAL FORMS PENDED IN 7 CALENDAR DAYS OR LESS        :",pnd_Misc_Parm_Pnd_Forms_Ctr.getValue(1),                  //Natural: WRITE ( 1 ) 'TOTAL FORMS PENDED IN 7 CALENDAR DAYS OR LESS        :' #FORMS-CTR ( 1 ) ( AD = OU ) 03X 'TOTAL FORMS PENDED FOR 0 - 29 CALENDAR DAYS          :' #FORMS-CTR ( 4 ) ( AD = OU ) / 'TOTAL FORMS PENDED IN 8 - 14 CALENDAR DAYS           :' #FORMS-CTR ( 2 ) ( AD = OU ) 03X 'TOTAL FORMS PENDED FOR 30 - 59 CALENDAR DAYS         :' #FORMS-CTR ( 5 ) ( AD = OU ) / 'TOTAL FORMS PENDED IN OVER 14 CALENDAR DAYS          :' #FORMS-CTR ( 3 ) ( AD = OU ) 03X 'TOTAL FORMS PENDED FOR 60 - 89 CALENDAR DAYS         :' #FORMS-CTR ( 6 ) ( AD = OU ) / 65T 'TOTAL FORMS PENDED FOR OVER 90 CALENDAR DAYS         :' #FORMS-CTR ( 7 ) ( AD = OU ) // 'TOTAL FORMS PENDED IN 3 BUSINESS DAYS OR LESS        :' #FORMS-BCTR ( 1 ) ( AD = OU ) / 'TOTAL FORMS PENDED IN 4 BUSINESS DAYS OR MORE        :' #FORMS-BCTR ( 2 ) ( AD = OU ) /
                new FieldAttributes ("AD=OU"),new ColumnSpacing(3),"TOTAL FORMS PENDED FOR 0 - 29 CALENDAR DAYS          :",pnd_Misc_Parm_Pnd_Forms_Ctr.getValue(4), 
                new FieldAttributes ("AD=OU"),NEWLINE,"TOTAL FORMS PENDED IN 8 - 14 CALENDAR DAYS           :",pnd_Misc_Parm_Pnd_Forms_Ctr.getValue(2), 
                new FieldAttributes ("AD=OU"),new ColumnSpacing(3),"TOTAL FORMS PENDED FOR 30 - 59 CALENDAR DAYS         :",pnd_Misc_Parm_Pnd_Forms_Ctr.getValue(5), 
                new FieldAttributes ("AD=OU"),NEWLINE,"TOTAL FORMS PENDED IN OVER 14 CALENDAR DAYS          :",pnd_Misc_Parm_Pnd_Forms_Ctr.getValue(3), 
                new FieldAttributes ("AD=OU"),new ColumnSpacing(3),"TOTAL FORMS PENDED FOR 60 - 89 CALENDAR DAYS         :",pnd_Misc_Parm_Pnd_Forms_Ctr.getValue(6), 
                new FieldAttributes ("AD=OU"),NEWLINE,new TabSetting(65),"TOTAL FORMS PENDED FOR OVER 90 CALENDAR DAYS         :",pnd_Misc_Parm_Pnd_Forms_Ctr.getValue(7), 
                new FieldAttributes ("AD=OU"),NEWLINE,NEWLINE,"TOTAL FORMS PENDED IN 3 BUSINESS DAYS OR LESS        :",pnd_Misc_Parm_Pnd_Forms_Bctr.getValue(1), 
                new FieldAttributes ("AD=OU"),NEWLINE,"TOTAL FORMS PENDED IN 4 BUSINESS DAYS OR MORE        :",pnd_Misc_Parm_Pnd_Forms_Bctr.getValue(2), 
                new FieldAttributes ("AD=OU"),NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: VALUE 'I'
        else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("I"))))
        {
            decideConditionsMet648++;
            getReports().write(1, ReportOption.NOTITLE,"TOTAL INQUIRIES PENDED IN 7 CALENDAR DAYS OR LESS    :",pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue(1),                //Natural: WRITE ( 1 ) 'TOTAL INQUIRIES PENDED IN 7 CALENDAR DAYS OR LESS    :' #INQUIRE-CTR ( 1 ) ( AD = OU ) 03X 'TOTAL INQUIRIES PENDED FOR 0 - 29 CALENDAR DAYS      :' #INQUIRE-CTR ( 4 ) ( AD = OU ) / 'TOTAL INQUIRIES PENDED IN 8 - 14 CALENDAR DAYS       :' #INQUIRE-CTR ( 2 ) ( AD = OU ) 03X 'TOTAL INQUIRIES PENDED FOR 30 - 59 CALENDAR DAYS     :' #INQUIRE-CTR ( 5 ) ( AD = OU ) / 'TOTAL INQUIRIES PENDED IN OVER 14 CALENDAR DAYS      :' #INQUIRE-CTR ( 3 ) ( AD = OU ) 03X 'TOTAL INQUIRIES PENDED FOR 60 - 89 CALENDAR DAYS     :' #INQUIRE-CTR ( 6 ) ( AD = OU ) / 65T 'TOTAL INQUIRIES PENDED FOR OVER 90 CALENDAR DAYS     :' #INQUIRE-CTR ( 7 ) ( AD = OU ) // 'TOTAL INQUIRIES PENDED IN 3 BUSINESS DAYS OR LESS    :' #INQUIRE-BCTR ( 1 ) ( AD = OU ) / 'TOTAL INQUIRIES PENDED IN 4 BUSINESS DAYS OR MORE    :' #INQUIRE-BCTR ( 2 ) ( AD = OU ) /
                new FieldAttributes ("AD=OU"),new ColumnSpacing(3),"TOTAL INQUIRIES PENDED FOR 0 - 29 CALENDAR DAYS      :",pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue(4), 
                new FieldAttributes ("AD=OU"),NEWLINE,"TOTAL INQUIRIES PENDED IN 8 - 14 CALENDAR DAYS       :",pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue(2), 
                new FieldAttributes ("AD=OU"),new ColumnSpacing(3),"TOTAL INQUIRIES PENDED FOR 30 - 59 CALENDAR DAYS     :",pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue(5), 
                new FieldAttributes ("AD=OU"),NEWLINE,"TOTAL INQUIRIES PENDED IN OVER 14 CALENDAR DAYS      :",pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue(3), 
                new FieldAttributes ("AD=OU"),new ColumnSpacing(3),"TOTAL INQUIRIES PENDED FOR 60 - 89 CALENDAR DAYS     :",pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue(6), 
                new FieldAttributes ("AD=OU"),NEWLINE,new TabSetting(65),"TOTAL INQUIRIES PENDED FOR OVER 90 CALENDAR DAYS     :",pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue(7), 
                new FieldAttributes ("AD=OU"),NEWLINE,NEWLINE,"TOTAL INQUIRIES PENDED IN 3 BUSINESS DAYS OR LESS    :",pnd_Misc_Parm_Pnd_Inquire_Bctr.getValue(1), 
                new FieldAttributes ("AD=OU"),NEWLINE,"TOTAL INQUIRIES PENDED IN 4 BUSINESS DAYS OR MORE    :",pnd_Misc_Parm_Pnd_Inquire_Bctr.getValue(2), 
                new FieldAttributes ("AD=OU"),NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: VALUE 'R'
        else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("R"))))
        {
            decideConditionsMet648++;
            getReports().write(1, ReportOption.NOTITLE,"TOTAL RESEARCH PENDED IN 7 CALENDAR DAYS OR LESS     :",pnd_Misc_Parm_Pnd_Research_Ctr.getValue(1),               //Natural: WRITE ( 1 ) 'TOTAL RESEARCH PENDED IN 7 CALENDAR DAYS OR LESS     :' #RESEARCH-CTR ( 1 ) ( AD = OU ) 03X 'TOTAL RESEARCH PENDED FOR 0 - 29 CALENDAR DAYS       :' #RESEARCH-CTR ( 4 ) ( AD = OU ) / 'TOTAL RESEARCH PENDED IN 8 - 14 CALENDAR DAYS        :' #RESEARCH-CTR ( 2 ) ( AD = OU ) 03X 'TOTAL RESEARCH PENDED FOR 30 - 59 CALENDAR DAYS      :' #RESEARCH-CTR ( 5 ) ( AD = OU ) / 'TOTAL RESEARCH PENDED IN OVER 14 CALENDAR DAYS       :' #RESEARCH-CTR ( 3 ) ( AD = OU ) 03X 'TOTAL RESEARCH PENDED FOR 60 - 89 CALENDAR DAYS      :' #RESEARCH-CTR ( 6 ) ( AD = OU ) / 65T 'TOTAL RESEARCH PENDED FOR OVER 90 CALENDAR DAYS      :' #RESEARCH-CTR ( 7 ) ( AD = OU ) // 'TOTAL RESEARCH PENDED IN 3 BUSINESS DAYS OR LESS     :' #RESEARCH-BCTR ( 1 ) ( AD = OU ) / 'TOTAL RESEARCH PENDED IN 4 BUSINESS DAYS OR MORE     :' #RESEARCH-BCTR ( 2 ) ( AD = OU ) /
                new FieldAttributes ("AD=OU"),new ColumnSpacing(3),"TOTAL RESEARCH PENDED FOR 0 - 29 CALENDAR DAYS       :",pnd_Misc_Parm_Pnd_Research_Ctr.getValue(4), 
                new FieldAttributes ("AD=OU"),NEWLINE,"TOTAL RESEARCH PENDED IN 8 - 14 CALENDAR DAYS        :",pnd_Misc_Parm_Pnd_Research_Ctr.getValue(2), 
                new FieldAttributes ("AD=OU"),new ColumnSpacing(3),"TOTAL RESEARCH PENDED FOR 30 - 59 CALENDAR DAYS      :",pnd_Misc_Parm_Pnd_Research_Ctr.getValue(5), 
                new FieldAttributes ("AD=OU"),NEWLINE,"TOTAL RESEARCH PENDED IN OVER 14 CALENDAR DAYS       :",pnd_Misc_Parm_Pnd_Research_Ctr.getValue(3), 
                new FieldAttributes ("AD=OU"),new ColumnSpacing(3),"TOTAL RESEARCH PENDED FOR 60 - 89 CALENDAR DAYS      :",pnd_Misc_Parm_Pnd_Research_Ctr.getValue(6), 
                new FieldAttributes ("AD=OU"),NEWLINE,new TabSetting(65),"TOTAL RESEARCH PENDED FOR OVER 90 CALENDAR DAYS      :",pnd_Misc_Parm_Pnd_Research_Ctr.getValue(7), 
                new FieldAttributes ("AD=OU"),NEWLINE,NEWLINE,"TOTAL RESEARCH PENDED IN 3 BUSINESS DAYS OR LESS     :",pnd_Misc_Parm_Pnd_Research_Bctr.getValue(1), 
                new FieldAttributes ("AD=OU"),NEWLINE,"TOTAL RESEARCH PENDED IN 4 BUSINESS DAYS OR MORE     :",pnd_Misc_Parm_Pnd_Research_Bctr.getValue(2), 
                new FieldAttributes ("AD=OU"),NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: VALUE 'T'
        else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("T"))))
        {
            decideConditionsMet648++;
            getReports().write(1, ReportOption.NOTITLE,"TOTAL TRANSACTIONS PENDED IN 7 CALENDAR DAYS OR LESS :",pnd_Misc_Parm_Pnd_Trans_Ctr.getValue(1),                  //Natural: WRITE ( 1 ) 'TOTAL TRANSACTIONS PENDED IN 7 CALENDAR DAYS OR LESS :' #TRANS-CTR ( 1 ) ( AD = OU ) 03X 'TOTAL TRANSACTIONS PENDED FOR 0 - 29 CALENDAR DAYS   :' #TRANS-CTR ( 4 ) ( AD = OU ) / 'TOTAL TRANSACTIONS PENDED IN 8 - 14 CALENDAR DAYS    :' #TRANS-CTR ( 2 ) ( AD = OU ) 03X 'TOTAL TRANSACTIONS PENDED FOR 30 - 59 CALENDAR DAYS  :' #TRANS-CTR ( 5 ) ( AD = OU ) / 'TOTAL TRANSACTIONS PENDED IN OVER 14 CALENDAR DAYS   :' #TRANS-CTR ( 3 ) ( AD = OU ) 03X 'TOTAL TRANSACTIONS PENDED FOR 60 - 89 CALENDAR DAYS  :' #TRANS-CTR ( 6 ) ( AD = OU ) / 65T 'TOTAL TRANSACTIONS PENDED FOR OVER 90 CALENDAR DAYS  :' #TRANS-CTR ( 7 ) ( AD = OU ) // 'TOTAL TRANSACTIONS PENDED IN 3 BUSINESS DAYS OR LESS :' #TRANS-BCTR ( 1 ) ( AD = OU ) / 'TOTAL TRANSACTIONS PENDED IN 4 BUSINESS DAYS OR MORE :' #TRANS-BCTR ( 2 ) ( AD = OU ) /
                new FieldAttributes ("AD=OU"),new ColumnSpacing(3),"TOTAL TRANSACTIONS PENDED FOR 0 - 29 CALENDAR DAYS   :",pnd_Misc_Parm_Pnd_Trans_Ctr.getValue(4), 
                new FieldAttributes ("AD=OU"),NEWLINE,"TOTAL TRANSACTIONS PENDED IN 8 - 14 CALENDAR DAYS    :",pnd_Misc_Parm_Pnd_Trans_Ctr.getValue(2), 
                new FieldAttributes ("AD=OU"),new ColumnSpacing(3),"TOTAL TRANSACTIONS PENDED FOR 30 - 59 CALENDAR DAYS  :",pnd_Misc_Parm_Pnd_Trans_Ctr.getValue(5), 
                new FieldAttributes ("AD=OU"),NEWLINE,"TOTAL TRANSACTIONS PENDED IN OVER 14 CALENDAR DAYS   :",pnd_Misc_Parm_Pnd_Trans_Ctr.getValue(3), 
                new FieldAttributes ("AD=OU"),new ColumnSpacing(3),"TOTAL TRANSACTIONS PENDED FOR 60 - 89 CALENDAR DAYS  :",pnd_Misc_Parm_Pnd_Trans_Ctr.getValue(6), 
                new FieldAttributes ("AD=OU"),NEWLINE,new TabSetting(65),"TOTAL TRANSACTIONS PENDED FOR OVER 90 CALENDAR DAYS  :",pnd_Misc_Parm_Pnd_Trans_Ctr.getValue(7), 
                new FieldAttributes ("AD=OU"),NEWLINE,NEWLINE,"TOTAL TRANSACTIONS PENDED IN 3 BUSINESS DAYS OR LESS :",pnd_Misc_Parm_Pnd_Trans_Bctr.getValue(1), 
                new FieldAttributes ("AD=OU"),NEWLINE,"TOTAL TRANSACTIONS PENDED IN 4 BUSINESS DAYS OR MORE :",pnd_Misc_Parm_Pnd_Trans_Bctr.getValue(2), 
                new FieldAttributes ("AD=OU"),NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: VALUE 'X'
        else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("X"))))
        {
            decideConditionsMet648++;
            getReports().write(1, ReportOption.NOTITLE,"TOTAL COMPLAINTS PENDED IN 7 CALENDAR DAYS OR LESS   :",pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue(1),              //Natural: WRITE ( 1 ) 'TOTAL COMPLAINTS PENDED IN 7 CALENDAR DAYS OR LESS   :' #COMPLAINT-CTR ( 1 ) ( AD = OU ) 03X 'TOTAL COMPLAINTS PENDED FOR 0 - 29 CALENDAR DAYS     :' #COMPLAINT-CTR ( 4 ) ( AD = OU ) / 'TOTAL COMPLAINTS PENDED IN 8 - 14 CALENDAR DAYS      :' #COMPLAINT-CTR ( 2 ) ( AD = OU ) 03X 'TOTAL COMPLAINTS PENDED FOR 30 - 59 CALENDAR DAYS    :' #COMPLAINT-CTR ( 5 ) ( AD = OU ) / 'TOTAL COMPLAINTS PENDED IN OVER 14 CALENDAR DAYS     :' #COMPLAINT-CTR ( 3 ) ( AD = OU ) 03X 'TOTAL COMPLAINTS PENDED FOR 60 - 89 CALENDAR DAYS    :' #COMPLAINT-CTR ( 6 ) ( AD = OU ) / 65T 'TOTAL COMPLAINTS PENDED FOR OVER 90 CALENDAR DAYS    :' #COMPLAINT-CTR ( 7 ) ( AD = OU ) // 'TOTAL COMPLAINTS PENDED IN 3 BUSINESS DAYS OR LESS   :' #COMPLAINT-BCTR ( 1 ) ( AD = OU ) / 'TOTAL COMPLAINTS PENDED IN 4 BUSINESS DAYS OR MORE   :' #COMPLAINT-BCTR ( 2 ) ( AD = OU ) /
                new FieldAttributes ("AD=OU"),new ColumnSpacing(3),"TOTAL COMPLAINTS PENDED FOR 0 - 29 CALENDAR DAYS     :",pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue(4), 
                new FieldAttributes ("AD=OU"),NEWLINE,"TOTAL COMPLAINTS PENDED IN 8 - 14 CALENDAR DAYS      :",pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue(2), 
                new FieldAttributes ("AD=OU"),new ColumnSpacing(3),"TOTAL COMPLAINTS PENDED FOR 30 - 59 CALENDAR DAYS    :",pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue(5), 
                new FieldAttributes ("AD=OU"),NEWLINE,"TOTAL COMPLAINTS PENDED IN OVER 14 CALENDAR DAYS     :",pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue(3), 
                new FieldAttributes ("AD=OU"),new ColumnSpacing(3),"TOTAL COMPLAINTS PENDED FOR 60 - 89 CALENDAR DAYS    :",pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue(6), 
                new FieldAttributes ("AD=OU"),NEWLINE,new TabSetting(65),"TOTAL COMPLAINTS PENDED FOR OVER 90 CALENDAR DAYS    :",pnd_Misc_Parm_Pnd_Complaint_Ctr.getValue(7), 
                new FieldAttributes ("AD=OU"),NEWLINE,NEWLINE,"TOTAL COMPLAINTS PENDED IN 3 BUSINESS DAYS OR LESS   :",pnd_Misc_Parm_Pnd_Complaint_Bctr.getValue(1), 
                new FieldAttributes ("AD=OU"),NEWLINE,"TOTAL COMPLAINTS PENDED IN 4 BUSINESS DAYS OR MORE   :",pnd_Misc_Parm_Pnd_Complaint_Bctr.getValue(2), 
                new FieldAttributes ("AD=OU"),NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: VALUE 'Z'
        else if (condition((pnd_Misc_Parm_Pnd_Wpid_Action.equals("Z"))))
        {
            decideConditionsMet648++;
            getReports().write(1, ReportOption.NOTITLE,"TOTAL OTHERS PENDED IN 7 CALENDAR DAYS OR LESS       :",pnd_Misc_Parm_Pnd_Other_Ctr.getValue(1),                  //Natural: WRITE ( 1 ) 'TOTAL OTHERS PENDED IN 7 CALENDAR DAYS OR LESS       :' #OTHER-CTR ( 1 ) ( AD = OU ) 03X 'TOTAL OTHERS PENDED FOR 0 - 29 CALENDAR DAYS         :' #OTHER-CTR ( 4 ) ( AD = OU ) / 'TOTAL OTHERS PENDED IN 8 - 14 CALENDAR DAYS          :' #OTHER-CTR ( 2 ) ( AD = OU ) 03X 'TOTAL OTHERS PENDED FOR 30 - 59 CALENDAR DAYS        :' #OTHER-CTR ( 5 ) ( AD = OU ) / 'TOTAL OTHERS PENDED IN OVER 14 CALENDAR DAYS         :' #OTHER-CTR ( 3 ) ( AD = OU ) 03X 'TOTAL OTHERS PENDED FOR 60 - 89 CALENDAR DAYS        :' #OTHER-CTR ( 6 ) ( AD = OU ) / 65T 'TOTAL OTHERS PENDED FOR OVER 90 CALENDAR DAYS        :' #OTHER-CTR ( 7 ) ( AD = OU ) // 'TOTAL OTHERS PENDED IN 3 BUSINESS DAYS OR LESS       :' #OTHER-BCTR ( 1 ) ( AD = OU ) / 'TOTAL OTHERS PENDED IN 4 BUSINESS DAYS OR MORE       :' #OTHER-BCTR ( 2 ) ( AD = OU ) /
                new FieldAttributes ("AD=OU"),new ColumnSpacing(3),"TOTAL OTHERS PENDED FOR 0 - 29 CALENDAR DAYS         :",pnd_Misc_Parm_Pnd_Other_Ctr.getValue(4), 
                new FieldAttributes ("AD=OU"),NEWLINE,"TOTAL OTHERS PENDED IN 8 - 14 CALENDAR DAYS          :",pnd_Misc_Parm_Pnd_Other_Ctr.getValue(2), 
                new FieldAttributes ("AD=OU"),new ColumnSpacing(3),"TOTAL OTHERS PENDED FOR 30 - 59 CALENDAR DAYS        :",pnd_Misc_Parm_Pnd_Other_Ctr.getValue(5), 
                new FieldAttributes ("AD=OU"),NEWLINE,"TOTAL OTHERS PENDED IN OVER 14 CALENDAR DAYS         :",pnd_Misc_Parm_Pnd_Other_Ctr.getValue(3), 
                new FieldAttributes ("AD=OU"),new ColumnSpacing(3),"TOTAL OTHERS PENDED FOR 60 - 89 CALENDAR DAYS        :",pnd_Misc_Parm_Pnd_Other_Ctr.getValue(6), 
                new FieldAttributes ("AD=OU"),NEWLINE,new TabSetting(65),"TOTAL OTHERS PENDED FOR OVER 90 CALENDAR DAYS        :",pnd_Misc_Parm_Pnd_Other_Ctr.getValue(7), 
                new FieldAttributes ("AD=OU"),NEWLINE,NEWLINE,"TOTAL OTHERS PENDED IN 3 BUSINESS DAYS OR LESS       :",pnd_Misc_Parm_Pnd_Other_Bctr.getValue(1), 
                new FieldAttributes ("AD=OU"),NEWLINE,"TOTAL OTHERS PENDED IN 4 BUSINESS DAYS OR MORE       :",pnd_Misc_Parm_Pnd_Other_Bctr.getValue(2), 
                new FieldAttributes ("AD=OU"),NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue("*").reset();                                                                                                              //Natural: RESET #BOOKLET-CTR ( * ) #FORMS-CTR ( * ) #INQUIRE-CTR ( * ) #RESEARCH-CTR ( * ) #OTHER-CTR ( * ) #BOOKLET-BCTR ( * ) #FORMS-BCTR ( * ) #INQUIRE-BCTR ( * ) #RESEARCH-BCTR ( * ) #OTHER-BCTR ( * ) #SUB-COUNT
        pnd_Misc_Parm_Pnd_Forms_Ctr.getValue("*").reset();
        pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue("*").reset();
        pnd_Misc_Parm_Pnd_Research_Ctr.getValue("*").reset();
        pnd_Misc_Parm_Pnd_Other_Ctr.getValue("*").reset();
        pnd_Misc_Parm_Pnd_Booklet_Bctr.getValue("*").reset();
        pnd_Misc_Parm_Pnd_Forms_Bctr.getValue("*").reset();
        pnd_Misc_Parm_Pnd_Inquire_Bctr.getValue("*").reset();
        pnd_Misc_Parm_Pnd_Research_Bctr.getValue("*").reset();
        pnd_Misc_Parm_Pnd_Other_Bctr.getValue("*").reset();
        pnd_Misc_Parm_Pnd_Sub_Count.reset();
    }
    private void sub_Unit_Break() throws Exception                                                                                                                        //Natural: UNIT-BREAK
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(26),"GRAND TOTAL OF ALL EXTERNALLY PENDED REQUESTS    :", new FieldAttributes           //Natural: WRITE ( 1 ) // 26T 'GRAND TOTAL OF ALL EXTERNALLY PENDED REQUESTS    :' ( AD = OU ) #TOTAL-COUNT ( AD = OU ) / 26T '=================================================='
            ("AD=OU"),pnd_Misc_Parm_Pnd_Total_Count, new FieldAttributes ("AD=OU"),NEWLINE,new TabSetting(26),"==================================================");
        if (Global.isEscape()) return;
        pnd_Misc_Parm_Pnd_Booklet_Ctr.getValue("*").reset();                                                                                                              //Natural: RESET #BOOKLET-CTR ( * ) #FORMS-CTR ( * ) #INQUIRE-CTR ( * ) #TRANS-CTR ( * ) #RESEARCH-CTR ( * ) #OTHER-CTR ( * ) #SUB-COUNT #TOTAL-COUNT #BOOKLET-BCTR ( * ) #FORMS-BCTR ( * ) #INQUIRE-BCTR ( * ) #RESEARCH-BCTR ( * ) #OTHER-BCTR ( * )
        pnd_Misc_Parm_Pnd_Forms_Ctr.getValue("*").reset();
        pnd_Misc_Parm_Pnd_Inquire_Ctr.getValue("*").reset();
        pnd_Misc_Parm_Pnd_Trans_Ctr.getValue("*").reset();
        pnd_Misc_Parm_Pnd_Research_Ctr.getValue("*").reset();
        pnd_Misc_Parm_Pnd_Other_Ctr.getValue("*").reset();
        pnd_Misc_Parm_Pnd_Sub_Count.reset();
        pnd_Misc_Parm_Pnd_Total_Count.reset();
        pnd_Misc_Parm_Pnd_Booklet_Bctr.getValue("*").reset();
        pnd_Misc_Parm_Pnd_Forms_Bctr.getValue("*").reset();
        pnd_Misc_Parm_Pnd_Inquire_Bctr.getValue("*").reset();
        pnd_Misc_Parm_Pnd_Research_Bctr.getValue("*").reset();
        pnd_Misc_Parm_Pnd_Other_Bctr.getValue("*").reset();
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        pnd_New_Unit.setValue(true);                                                                                                                                      //Natural: MOVE TRUE TO #NEW-UNIT
        pnd_Page.setValue(0);                                                                                                                                             //Natural: MOVE 0 TO #PAGE
        DbsUtil.callnat(Cwfn3911.class , getCurrentProcessState());                                                                                                       //Natural: CALLNAT 'CWFN3911'
        if (condition(Global.isEscape())) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Page.nadd(1);                                                                                                                                     //Natural: COMPUTE #PAGE = #PAGE + 1
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),pnd_Env,new TabSetting(52),"CORPORATE WORKFLOW FACILITIES",new TabSetting(122),"PAGE",pnd_Page,  //Natural: WRITE ( 1 ) NOTITLE *PROGRAM #ENV 52T 'CORPORATE WORKFLOW FACILITIES' 122T 'PAGE' #PAGE ( NL = 5 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 50T 'REPORT OF EXTERNALLY PENDED CASES' 124T *TIMX ( EM = HH':'II' 'AP )
                        new NumericLength (5), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),new 
                        TabSetting(50),"REPORT OF EXTERNALLY PENDED CASES",new TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"));
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                    DbsUtil.callnat(Cwfn1103.class , getCurrentProcessState(), pnd_Report_Data_Pnd_Rep_Unit_Cde.getValue(1), pnd_Rep_Unit_Name);                          //Natural: CALLNAT 'CWFN1103' #REP-UNIT-CDE ( 1 ) #REP-UNIT-NAME
                    if (condition(Global.isEscape())) return;
                    getReports().write(1, ReportOption.NOTITLE,"UNIT      :",pnd_Report_Data_Pnd_Rep_Unit_Cde.getValue(1),pnd_Rep_Unit_Name);                             //Natural: WRITE ( 1 ) 'UNIT      :' #REP-UNIT-CDE ( 1 ) #REP-UNIT-NAME
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        pnd_Tbl_Run_Flag.setValue(" ");                                                                                                                                   //Natural: MOVE ' ' TO #TBL-RUN-FLAG
        DbsUtil.callnat(Cwfn3914.class , getCurrentProcessState(), pnd_Report_Parm, pnd_Run_Date, pnd_Tbl_Run_Flag);                                                      //Natural: CALLNAT 'CWFN3914' #REPORT-PARM #RUN-DATE #TBL-RUN-FLAG
        if (condition(Global.isEscape())) return;
    };                                                                                                                                                                    //Natural: END-ERROR

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean pnd_Work_Record_Tbl_WpidIsBreak = pnd_Work_Record_Tbl_Wpid.isBreak(endOfData);
        boolean pnd_Work_Record_Tbl_Wpid_ActIsBreak = pnd_Work_Record_Tbl_Wpid_Act.isBreak(endOfData);
        boolean pnd_Work_Record_Tbl_Log_Unit_CdeIsBreak = pnd_Work_Record_Tbl_Log_Unit_Cde.isBreak(endOfData);
        if (condition(pnd_Work_Record_Tbl_WpidIsBreak || pnd_Work_Record_Tbl_Wpid_ActIsBreak || pnd_Work_Record_Tbl_Log_Unit_CdeIsBreak))
        {
            pnd_Misc_Parm_Pnd_Wpid.setValue(sort01Tbl_WpidOld);                                                                                                           //Natural: MOVE OLD ( #WORK-RECORD.TBL-WPID ) TO #WPID
            pnd_Misc_Parm_Pnd_Wpid_Count.setValue(sort01Tbl_WpidCount565);                                                                                                //Natural: MOVE COUNT ( #WORK-RECORD.TBL-WPID ) TO #WPID-COUNT
                                                                                                                                                                          //Natural: PERFORM WPID-BREAK
            sub_Wpid_Break();
            if (condition(Global.isEscape())) {return;}
            sort01Tbl_WpidCount565.setDec(new DbsDecimal(0));                                                                                                             //Natural: END-BREAK
        }
        if (condition(pnd_Work_Record_Tbl_Wpid_ActIsBreak || pnd_Work_Record_Tbl_Log_Unit_CdeIsBreak))
        {
            pnd_Misc_Parm_Pnd_Total_Count.nadd(pnd_Misc_Parm_Pnd_Sub_Count);                                                                                              //Natural: COMPUTE #TOTAL-COUNT = #TOTAL-COUNT + #SUB-COUNT
            pnd_Misc_Parm_Pnd_Wpid_Action.setValue(sort01Tbl_Wpid_ActOld);                                                                                                //Natural: MOVE OLD ( #WORK-RECORD.TBL-WPID-ACT ) TO #WPID-ACTION
                                                                                                                                                                          //Natural: PERFORM WPID-ACT-BREAK
            sub_Wpid_Act_Break();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pnd_Work_Record_Tbl_Log_Unit_CdeIsBreak))
        {
            pnd_Report_Data_Pnd_Rep_Unit_Cde.getValue(1).setValue(sort01Tbl_Log_Unit_CdeOld);                                                                             //Natural: MOVE OLD ( TBL-LOG-UNIT-CDE ) TO #REP-UNIT-CDE ( 1 )
                                                                                                                                                                          //Natural: PERFORM UNIT-BREAK
            sub_Unit_Break();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=58 LS=140");

        getReports().setDisplayColumns(1, "//RECEIVED/AT TIAA",
        		pnd_Work_Record_Tbl_Tiaa_Dte, new ReportEditMask ("MM/DD/YY"),"//DOC/REC'D",
        		pnd_Misc_Parm_Pnd_Return_Doc_Txt,"//REC'D IN/UNIT",
        		pnd_Misc_Parm_Pnd_Return_Rcvd_Txt,"//WORK/PROCESS",
        		pnd_Misc_Parm_Pnd_Wpid_Sname, new AlphanumericLength (12),"//WORK/STEP",
        		pnd_Work_Record_Tbl_Step_Id, new IdenticalSuppress(true),"///STATUS",
        		pnd_Misc_Parm_Pnd_Status_Sname, new AlphanumericLength (15),"//STATUS/DATE",
        		pnd_Work_Record_Tbl_Status_Dte, new ReportEditMask ("MM/DD/YY"),"/CLNDR/DAYS/PENDED",
        		pnd_Work_Record_Tbl_Pend_Days, new ReportEditMask ("Z,ZZ9.9"),"CLNDR/DAYS/IN/TIAA",
        		pnd_Work_Record_Tbl_Calendar_Days, new ReportEditMask ("ZZZ9"),"BSNSS/DAYS/IN/TIAA",
        		pnd_Work_Record_Tbl_Tiaa_Bsnss_Days, new ReportEditMask ("ZZZ9"),"//PARTIC/NAME",
        		pnd_Work_Record_Partic_Sname,"///PIN",
        		pnd_Work_Record_Tbl_Pin, new ReportEditMask ("999999999999"),"//CONTRACT/NUMBER",
        		pnd_Work_Record_Tbl_Contracts,"///EMPLOYEE",
        		pnd_Work_Record_Tbl_Racf);
    }
}
