/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:56:49 PM
**        * FROM NATURAL PROGRAM : Mcsp3850
************************************************************
**        * FILE NAME            : Mcsp3850.java
**        * CLASS NAME           : Mcsp3850
**        * INSTANCE NAME        : Mcsp3850
************************************************************
************************************************************************
* PROGRAM  : MCSP3850
* SYSTEM   : PROJCWF
* TITLE    : DELETE ATI RECORDS (WEEKLY CLEANUP)
* GENERATED: 12/01/95
* FUNCTION : THIS PROGRAM READS THE ATI RECORDS AND DELETES ANY RECORD
*            GT THAN "Certain" DAYS OLD.
*
* MOD DATE   MOD BY   DESCRIPTION OF CHANGES
* --------  -------- ---------------------------------------------------
* 11/21/01  GREENFI  CHANGED RETENTION PERIOD FROM 30 DAYS TO 60 DAYS.
* 01/29/02  GREENFI  CHANGED RETENTION PERIOD FROM 60 DAYS TO 30 DAYS.
* 06/04/02  GREENFI  CHANGED RETENTION PERIOD FROM 30 DAYS TO 20 DAYS.
* 02/06/03  GH       CHANGED RETENTION PERIOD FROM 20 DAYS TO 10 DAYS.
*
* 02/23/2017 - SINGAK - PIN EXPANSION - AUG 2017
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Mcsp3850 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Ati_Fulfill;
    private DbsField cwf_Ati_Fulfill_Ati_Entry_Dte_Tme;
    private DbsField pnd_Program;
    private DbsField pnd_Tot_Delete;
    private DbsField pnd_Rec_Cnt;
    private DbsField pnd_Input_To_D;
    private DbsField pnd_Input_To_T;
    private DbsField pnd_Ati_Completed_Key;

    private DbsGroup pnd_Ati_Completed_Key__R_Field_1;
    private DbsField pnd_Ati_Completed_Key_Ati_Rcrd_Type;
    private DbsField pnd_Ati_Completed_Key_Ati_Pin;
    private DbsField pnd_Ati_Completed_Key_Ati_Mit_Log_Dt_Tme;
    private DbsField pnd_Ati_Completed_Key_Ati_Prge_Ind;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Ati_Fulfill = new DataAccessProgramView(new NameInfo("vw_cwf_Ati_Fulfill", "CWF-ATI-FULFILL"), "CWF_ATI_FULFILL", "CWF_KDO_FULFILL");
        cwf_Ati_Fulfill_Ati_Entry_Dte_Tme = vw_cwf_Ati_Fulfill.getRecord().newFieldInGroup("cwf_Ati_Fulfill_Ati_Entry_Dte_Tme", "ATI-ENTRY-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ATI_ENTRY_DTE_TME");
        registerRecord(vw_cwf_Ati_Fulfill);

        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Tot_Delete = localVariables.newFieldInRecord("pnd_Tot_Delete", "#TOT-DELETE", FieldType.NUMERIC, 8);
        pnd_Rec_Cnt = localVariables.newFieldInRecord("pnd_Rec_Cnt", "#REC-CNT", FieldType.NUMERIC, 4);
        pnd_Input_To_D = localVariables.newFieldInRecord("pnd_Input_To_D", "#INPUT-TO-D", FieldType.DATE);
        pnd_Input_To_T = localVariables.newFieldInRecord("pnd_Input_To_T", "#INPUT-TO-T", FieldType.TIME);
        pnd_Ati_Completed_Key = localVariables.newFieldInRecord("pnd_Ati_Completed_Key", "#ATI-COMPLETED-KEY", FieldType.STRING, 21);

        pnd_Ati_Completed_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Ati_Completed_Key__R_Field_1", "REDEFINE", pnd_Ati_Completed_Key);
        pnd_Ati_Completed_Key_Ati_Rcrd_Type = pnd_Ati_Completed_Key__R_Field_1.newFieldInGroup("pnd_Ati_Completed_Key_Ati_Rcrd_Type", "ATI-RCRD-TYPE", 
            FieldType.STRING, 1);
        pnd_Ati_Completed_Key_Ati_Pin = pnd_Ati_Completed_Key__R_Field_1.newFieldInGroup("pnd_Ati_Completed_Key_Ati_Pin", "ATI-PIN", FieldType.NUMERIC, 
            12);
        pnd_Ati_Completed_Key_Ati_Mit_Log_Dt_Tme = pnd_Ati_Completed_Key__R_Field_1.newFieldInGroup("pnd_Ati_Completed_Key_Ati_Mit_Log_Dt_Tme", "ATI-MIT-LOG-DT-TME", 
            FieldType.TIME);
        pnd_Ati_Completed_Key_Ati_Prge_Ind = pnd_Ati_Completed_Key__R_Field_1.newFieldInGroup("pnd_Ati_Completed_Key_Ati_Prge_Ind", "ATI-PRGE-IND", FieldType.STRING, 
            1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Ati_Fulfill.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Mcsp3850() throws Exception
    {
        super("Mcsp3850");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  ----------------------------------------------------------------------
        //*   MAIN PROGRAM
        //*  ----------------------------------------------------------------------
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: FORMAT ( 1 ) LS = 133 PS = 60 ZP = OFF;//Natural: ASSIGN #PROGRAM = *PROGRAM
        pnd_Input_To_D.setValue(Global.getDATX());                                                                                                                        //Natural: ASSIGN #INPUT-TO-D := *DATX
        //*  <--- RETENTION PERIOD
        pnd_Input_To_D.nsubtract(10);                                                                                                                                     //Natural: COMPUTE #INPUT-TO-D = #INPUT-TO-D - 10
        pnd_Input_To_T.setValue(pnd_Input_To_D);                                                                                                                          //Natural: MOVE #INPUT-TO-D TO #INPUT-TO-T
        vw_cwf_Ati_Fulfill.startDatabaseRead                                                                                                                              //Natural: READ CWF-ATI-FULFILL WITH ATI-COMPLETED-KEY = #ATI-COMPLETED-KEY
        (
        "READ_ATI",
        new Wc[] { new Wc("ATI_COMPLETED_KEY", ">=", pnd_Ati_Completed_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("ATI_COMPLETED_KEY", "ASC") }
        );
        READ_ATI:
        while (condition(vw_cwf_Ati_Fulfill.readNextRow("READ_ATI")))
        {
            //*  COUNT READS - NOT DELETES BECAUSE NATURAL READS WITH HOLD
            pnd_Rec_Cnt.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #REC-CNT
            if (condition(cwf_Ati_Fulfill_Ati_Entry_Dte_Tme.lessOrEqual(pnd_Input_To_T)))                                                                                 //Natural: IF CWF-ATI-FULFILL.ATI-ENTRY-DTE-TME LE #INPUT-TO-T
            {
                pnd_Tot_Delete.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #TOT-DELETE
                vw_cwf_Ati_Fulfill.deleteDBRow("READ_ATI");                                                                                                               //Natural: DELETE ( READ-ATI. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Rec_Cnt.greater(100)))                                                                                                                      //Natural: IF #REC-CNT GT 100
            {
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                pnd_Rec_Cnt.setValue(0);                                                                                                                                  //Natural: ASSIGN #REC-CNT = 0
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"    NUMBER OF RECORDS DELETED : ",pnd_Tot_Delete);                                                    //Natural: WRITE ( 1 ) // '    NUMBER OF RECORDS DELETED : ' #TOT-DELETE
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE," ** MCSP3850 RECORD DELETE SUCCESSFUL **");                                                           //Natural: WRITE ( 1 ) // ' ** MCSP3850 RECORD DELETE SUCCESSFUL **'
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Mcsf3850.class));                                              //Natural: WRITE ( 1 ) NOTITLE NOHDR USING FORM 'MCSF3850'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=60 ZP=OFF");
    }
}
