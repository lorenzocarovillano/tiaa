/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:13:07 PM
**        * FROM NATURAL PROGRAM : Efsp7063
************************************************************
**        * FILE NAME            : Efsp7063.java
**        * CLASS NAME           : Efsp7063
**        * INSTANCE NAME        : Efsp7063
************************************************************
************************************************************************
* PROGRAM  : EFSP7063
* SYSTEM   : CRPCWF
* TITLE    : PRINT IMAGE UPLOAD SUMMARY REPORT (MONTHLY)
* GENERATED: JUL 12,93 AT 10:00 AM
* FUNCTION : THIS PROGRAM READS THE CWF-UPLOAD-AUDIT FILE AND PRINTS
*            THE IMAGE UPLOAD SUMMARY REPORT
* HISTORY
* 04/28/95  BE  ADDED TOTAL NO. OF MINS WITHOUT A PIN IN THE REPORT.
* 05/13/96  BE  WRITE 'Nothing to Report' IF THERE ARE NO RECS TO RPT.
* 03/31/97  CS  FORCE LANDSCAPE PRINT MODE IF SUBMITTED ONLINE
* 12/24/97  CS  INCLUDED 'FORMS' WHEN REPORTING 'CIRS'
* 02/23/2017 - SINGAK - PIN EXPANSION - AUG 2017 <STOW ONLY>
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsp7063 extends BLNatBase
{
    // Data Areas
    private GdaCwfg000 gdaCwfg000;
    private LdaEfsl0040 ldaEfsl0040;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_I;
    private DbsField pnd_Last_Dt;
    private DbsField pnd_End_Dt;
    private DbsField pnd_End_Dt_S;
    private DbsField pnd_Program;

    private DbsGroup pnd_Daily;
    private DbsField pnd_Daily_Pnd_Batch_Count;
    private DbsField pnd_Daily_Pnd_Mins_Count;
    private DbsField pnd_Daily_Pnd_Images_Count;

    private DbsGroup pnd_Summary;
    private DbsField pnd_Summary_Pnd_Batch_Count;
    private DbsField pnd_Summary_Pnd_Mins_Count;
    private DbsField pnd_Summary_Pnd_Images_Count;

    private DbsGroup pnd_Total;
    private DbsField pnd_Total_Pnd_Batch_Count;
    private DbsField pnd_Total_Pnd_Mins_Count;
    private DbsField pnd_Total_Pnd_Mins_No_Pin_Count;
    private DbsField pnd_Total_Pnd_Images_Count;
    private DbsField pnd_Start_Key;
    private DbsField pnd_Record_Date;
    private DbsField pnd_Record_Yymm;
    private DbsField pnd_Record_Time;

    private DbsGroup pnd_Record_Time__R_Field_1;
    private DbsField pnd_Record_Time_Pnd_Record_Hhmmss;
    private DbsField pnd_Input_Date_Time;
    private DbsField pnd_Input_Source_Id;
    private DbsField pnd_Input_Data;

    private DbsGroup pnd_Input_Data__R_Field_2;
    private DbsField pnd_Input_Data_Pnd_Input_Time_Start;

    private DbsGroup pnd_Input_Data__R_Field_3;
    private DbsField pnd_Input_Data_Pnd_Input_Time_N;
    private DbsField pnd_Input_Data_Pnd_Input_Time_End;
    private DbsField pnd_Input_Data_Pnd_Input_Date;

    private DbsGroup pnd_Input_Data__R_Field_4;
    private DbsField pnd_Input_Data_Pnd_Input_Yymm;
    private DbsField pnd_Input_Data_Pnd_Input_Dd;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_5;
    private DbsField cwf_Support_Tbl_Pnd_Regular_Run_Parms;
    private DbsField cwf_Support_Tbl_Pnd_Override_Time_Parms;
    private DbsField cwf_Support_Tbl_Pnd_Override_Flag;
    private DbsField cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_6;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind;
    private DbsField pnd_Nothing_To_Rpt;
    private DbsField pnd_Record;
    private DbsField pnd_Image_Pointer_Key;

    private DbsGroup pnd_Image_Pointer_Key__R_Field_7;
    private DbsField pnd_Image_Pointer_Key_Pnd_Image_Source;
    private DbsField pnd_Image_Pointer_Key_Pnd_Image_Batch;
    private DbsField pnd_Image_Pointer_Key_Pnd_Image_Batch_Nbr;
    private DbsField pnd_Image_Combine;
    private DbsField pnd_Image_Platter;
    private DbsField pnd_Image_Platter_Old;

    private DataAccessProgramView vw_cwf_Image_Xref;
    private DbsField cwf_Image_Xref_Mail_Item_Nbr;
    private DbsField cwf_Image_Xref_Image_Document_Address_Cde;

    private DbsGroup cwf_Image_Xref__R_Field_8;
    private DbsField cwf_Image_Xref_Image_Document_Address_Cde_Platter;
    private DbsField cwf_Image_Xref_Ph_Unique_Id_Nbr;
    private DbsField cwf_Image_Xref_Image_Quality_Ind;
    private DbsField cwf_Image_Xref_Upld_Date_Time;
    private DbsField cwf_Image_Xref_Number_Of_Pages;
    private DbsField cwf_Image_Xref_Source_Id;
    private DbsField cwf_Image_Xref_Capture_Id;
    private DbsField cwf_Image_Xref_Batch_Id;
    private DbsField cwf_Image_Xref_Image_Delete_Ind;
    private DbsField cwf_Image_Xref_Audit_System;
    private DbsField cwf_Image_Xref_Audit_Dte_Tme;
    private DbsField cwf_Image_Xref_Audit_Empl_Racf_Id;
    private DbsField cwf_Image_Xref_Cabinet_Id;

    private DbsRecord internalLoopRecord;
    private DbsField sort01Pnd_Record_DateOld;
    private DbsField sort01Source_IdOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaCwfg000 = GdaCwfg000.getInstance(getCallnatLevel());
        registerRecord(gdaCwfg000);
        if (gdaOnly) return;

        ldaEfsl0040 = new LdaEfsl0040();
        registerRecord(ldaEfsl0040);
        registerRecord(ldaEfsl0040.getVw_cwf_Upload_Audit());

        // Local Variables
        localVariables = new DbsRecord();
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Last_Dt = localVariables.newFieldInRecord("pnd_Last_Dt", "#LAST-DT", FieldType.STRING, 8);
        pnd_End_Dt = localVariables.newFieldInRecord("pnd_End_Dt", "#END-DT", FieldType.STRING, 8);
        pnd_End_Dt_S = localVariables.newFieldInRecord("pnd_End_Dt_S", "#END-DT-S", FieldType.STRING, 8);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);

        pnd_Daily = localVariables.newGroupInRecord("pnd_Daily", "#DAILY");
        pnd_Daily_Pnd_Batch_Count = pnd_Daily.newFieldInGroup("pnd_Daily_Pnd_Batch_Count", "#BATCH-COUNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Daily_Pnd_Mins_Count = pnd_Daily.newFieldInGroup("pnd_Daily_Pnd_Mins_Count", "#MINS-COUNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Daily_Pnd_Images_Count = pnd_Daily.newFieldInGroup("pnd_Daily_Pnd_Images_Count", "#IMAGES-COUNT", FieldType.PACKED_DECIMAL, 7);

        pnd_Summary = localVariables.newGroupInRecord("pnd_Summary", "#SUMMARY");
        pnd_Summary_Pnd_Batch_Count = pnd_Summary.newFieldInGroup("pnd_Summary_Pnd_Batch_Count", "#BATCH-COUNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Summary_Pnd_Mins_Count = pnd_Summary.newFieldInGroup("pnd_Summary_Pnd_Mins_Count", "#MINS-COUNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Summary_Pnd_Images_Count = pnd_Summary.newFieldInGroup("pnd_Summary_Pnd_Images_Count", "#IMAGES-COUNT", FieldType.PACKED_DECIMAL, 7);

        pnd_Total = localVariables.newGroupInRecord("pnd_Total", "#TOTAL");
        pnd_Total_Pnd_Batch_Count = pnd_Total.newFieldInGroup("pnd_Total_Pnd_Batch_Count", "#BATCH-COUNT", FieldType.NUMERIC, 5);
        pnd_Total_Pnd_Mins_Count = pnd_Total.newFieldInGroup("pnd_Total_Pnd_Mins_Count", "#MINS-COUNT", FieldType.NUMERIC, 7);
        pnd_Total_Pnd_Mins_No_Pin_Count = pnd_Total.newFieldInGroup("pnd_Total_Pnd_Mins_No_Pin_Count", "#MINS-NO-PIN-COUNT", FieldType.NUMERIC, 7);
        pnd_Total_Pnd_Images_Count = pnd_Total.newFieldInGroup("pnd_Total_Pnd_Images_Count", "#IMAGES-COUNT", FieldType.NUMERIC, 7);
        pnd_Start_Key = localVariables.newFieldInRecord("pnd_Start_Key", "#START-KEY", FieldType.TIME);
        pnd_Record_Date = localVariables.newFieldInRecord("pnd_Record_Date", "#RECORD-DATE", FieldType.DATE);
        pnd_Record_Yymm = localVariables.newFieldInRecord("pnd_Record_Yymm", "#RECORD-YYMM", FieldType.STRING, 6);
        pnd_Record_Time = localVariables.newFieldInRecord("pnd_Record_Time", "#RECORD-TIME", FieldType.STRING, 6);

        pnd_Record_Time__R_Field_1 = localVariables.newGroupInRecord("pnd_Record_Time__R_Field_1", "REDEFINE", pnd_Record_Time);
        pnd_Record_Time_Pnd_Record_Hhmmss = pnd_Record_Time__R_Field_1.newFieldInGroup("pnd_Record_Time_Pnd_Record_Hhmmss", "#RECORD-HHMMSS", FieldType.NUMERIC, 
            6);
        pnd_Input_Date_Time = localVariables.newFieldInRecord("pnd_Input_Date_Time", "#INPUT-DATE-TIME", FieldType.STRING, 14);
        pnd_Input_Source_Id = localVariables.newFieldInRecord("pnd_Input_Source_Id", "#INPUT-SOURCE-ID", FieldType.STRING, 6);
        pnd_Input_Data = localVariables.newFieldInRecord("pnd_Input_Data", "#INPUT-DATA", FieldType.STRING, 60);

        pnd_Input_Data__R_Field_2 = localVariables.newGroupInRecord("pnd_Input_Data__R_Field_2", "REDEFINE", pnd_Input_Data);
        pnd_Input_Data_Pnd_Input_Time_Start = pnd_Input_Data__R_Field_2.newFieldInGroup("pnd_Input_Data_Pnd_Input_Time_Start", "#INPUT-TIME-START", FieldType.STRING, 
            6);

        pnd_Input_Data__R_Field_3 = pnd_Input_Data__R_Field_2.newGroupInGroup("pnd_Input_Data__R_Field_3", "REDEFINE", pnd_Input_Data_Pnd_Input_Time_Start);
        pnd_Input_Data_Pnd_Input_Time_N = pnd_Input_Data__R_Field_3.newFieldInGroup("pnd_Input_Data_Pnd_Input_Time_N", "#INPUT-TIME-N", FieldType.NUMERIC, 
            6);
        pnd_Input_Data_Pnd_Input_Time_End = pnd_Input_Data__R_Field_2.newFieldInGroup("pnd_Input_Data_Pnd_Input_Time_End", "#INPUT-TIME-END", FieldType.STRING, 
            6);
        pnd_Input_Data_Pnd_Input_Date = pnd_Input_Data__R_Field_2.newFieldInGroup("pnd_Input_Data_Pnd_Input_Date", "#INPUT-DATE", FieldType.STRING, 8);

        pnd_Input_Data__R_Field_4 = pnd_Input_Data__R_Field_2.newGroupInGroup("pnd_Input_Data__R_Field_4", "REDEFINE", pnd_Input_Data_Pnd_Input_Date);
        pnd_Input_Data_Pnd_Input_Yymm = pnd_Input_Data__R_Field_4.newFieldInGroup("pnd_Input_Data_Pnd_Input_Yymm", "#INPUT-YYMM", FieldType.STRING, 6);
        pnd_Input_Data_Pnd_Input_Dd = pnd_Input_Data__R_Field_4.newFieldInGroup("pnd_Input_Data_Pnd_Input_Dd", "#INPUT-DD", FieldType.STRING, 2);

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Support_Tbl__R_Field_5 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_5", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Pnd_Regular_Run_Parms = cwf_Support_Tbl__R_Field_5.newFieldInGroup("cwf_Support_Tbl_Pnd_Regular_Run_Parms", "#REGULAR-RUN-PARMS", 
            FieldType.STRING, 20);
        cwf_Support_Tbl_Pnd_Override_Time_Parms = cwf_Support_Tbl__R_Field_5.newFieldInGroup("cwf_Support_Tbl_Pnd_Override_Time_Parms", "#OVERRIDE-TIME-PARMS", 
            FieldType.STRING, 12);
        cwf_Support_Tbl_Pnd_Override_Flag = cwf_Support_Tbl__R_Field_5.newFieldInGroup("cwf_Support_Tbl_Pnd_Override_Flag", "#OVERRIDE-FLAG", FieldType.STRING, 
            1);
        cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde", "TBL-DLTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_DLTE_OPRTR_CDE");
        registerRecord(vw_cwf_Support_Tbl);

        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_6 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_6", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind = pnd_Tbl_Prime_Key__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", FieldType.STRING, 
            1);
        pnd_Nothing_To_Rpt = localVariables.newFieldInRecord("pnd_Nothing_To_Rpt", "#NOTHING-TO-RPT", FieldType.BOOLEAN, 1);
        pnd_Record = localVariables.newFieldInRecord("pnd_Record", "#RECORD", FieldType.STRING, 54);
        pnd_Image_Pointer_Key = localVariables.newFieldInRecord("pnd_Image_Pointer_Key", "#IMAGE-POINTER-KEY", FieldType.STRING, 17);

        pnd_Image_Pointer_Key__R_Field_7 = localVariables.newGroupInRecord("pnd_Image_Pointer_Key__R_Field_7", "REDEFINE", pnd_Image_Pointer_Key);
        pnd_Image_Pointer_Key_Pnd_Image_Source = pnd_Image_Pointer_Key__R_Field_7.newFieldInGroup("pnd_Image_Pointer_Key_Pnd_Image_Source", "#IMAGE-SOURCE", 
            FieldType.STRING, 6);
        pnd_Image_Pointer_Key_Pnd_Image_Batch = pnd_Image_Pointer_Key__R_Field_7.newFieldInGroup("pnd_Image_Pointer_Key_Pnd_Image_Batch", "#IMAGE-BATCH", 
            FieldType.NUMERIC, 8);
        pnd_Image_Pointer_Key_Pnd_Image_Batch_Nbr = pnd_Image_Pointer_Key__R_Field_7.newFieldInGroup("pnd_Image_Pointer_Key_Pnd_Image_Batch_Nbr", "#IMAGE-BATCH-NBR", 
            FieldType.NUMERIC, 2);
        pnd_Image_Combine = localVariables.newFieldInRecord("pnd_Image_Combine", "#IMAGE-COMBINE", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Image_Platter = localVariables.newFieldInRecord("pnd_Image_Platter", "#IMAGE-PLATTER", FieldType.STRING, 10);
        pnd_Image_Platter_Old = localVariables.newFieldInRecord("pnd_Image_Platter_Old", "#IMAGE-PLATTER-OLD", FieldType.STRING, 10);

        vw_cwf_Image_Xref = new DataAccessProgramView(new NameInfo("vw_cwf_Image_Xref", "CWF-IMAGE-XREF"), "CWF_IMAGE_XREF", "CWF_IMAGE_XREF");
        cwf_Image_Xref_Mail_Item_Nbr = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Mail_Item_Nbr", "MAIL-ITEM-NBR", FieldType.NUMERIC, 
            11, RepeatingFieldStrategy.None, "MAIL_ITEM_NBR");
        cwf_Image_Xref_Mail_Item_Nbr.setDdmHeader("MAIL ITEM/NUMBER");
        cwf_Image_Xref_Image_Document_Address_Cde = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Image_Document_Address_Cde", "IMAGE-DOCUMENT-ADDRESS-CDE", 
            FieldType.STRING, 22, RepeatingFieldStrategy.None, "IMAGE_DOCUMENT_ADDRESS_CDE");
        cwf_Image_Xref_Image_Document_Address_Cde.setDdmHeader("IMNET DOC/ADDRESS");

        cwf_Image_Xref__R_Field_8 = vw_cwf_Image_Xref.getRecord().newGroupInGroup("cwf_Image_Xref__R_Field_8", "REDEFINE", cwf_Image_Xref_Image_Document_Address_Cde);
        cwf_Image_Xref_Image_Document_Address_Cde_Platter = cwf_Image_Xref__R_Field_8.newFieldInGroup("cwf_Image_Xref_Image_Document_Address_Cde_Platter", 
            "IMAGE-DOCUMENT-ADDRESS-CDE-PLATTER", FieldType.STRING, 10);
        cwf_Image_Xref_Ph_Unique_Id_Nbr = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Ph_Unique_Id_Nbr", "PH-UNIQUE-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "PH_UNIQUE_ID_NBR");
        cwf_Image_Xref_Ph_Unique_Id_Nbr.setDdmHeader("PIN");
        cwf_Image_Xref_Image_Quality_Ind = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Image_Quality_Ind", "IMAGE-QUALITY-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "IMAGE_QUALITY_IND");
        cwf_Image_Xref_Image_Quality_Ind.setDdmHeader("POOR/QLTY");
        cwf_Image_Xref_Upld_Date_Time = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Upld_Date_Time", "UPLD-DATE-TIME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "UPLD_DATE_TIME");
        cwf_Image_Xref_Upld_Date_Time.setDdmHeader("UPLOAD/DATE TIME");
        cwf_Image_Xref_Number_Of_Pages = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Number_Of_Pages", "NUMBER-OF-PAGES", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "NUMBER_OF_PAGES");
        cwf_Image_Xref_Number_Of_Pages.setDdmHeader("NBR OF/PAGES");
        cwf_Image_Xref_Source_Id = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Source_Id", "SOURCE-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "SOURCE_ID");
        cwf_Image_Xref_Capture_Id = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Capture_Id", "CAPTURE-ID", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "CAPTURE_ID");
        cwf_Image_Xref_Batch_Id = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Batch_Id", "BATCH-ID", FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, 
            "BATCH_ID");
        cwf_Image_Xref_Image_Delete_Ind = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Image_Delete_Ind", "IMAGE-DELETE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "IMAGE_DELETE_IND");
        cwf_Image_Xref_Audit_System = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Audit_System", "AUDIT-SYSTEM", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "AUDIT_SYSTEM");
        cwf_Image_Xref_Audit_Dte_Tme = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Audit_Dte_Tme", "AUDIT-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "AUDIT_DTE_TME");
        cwf_Image_Xref_Audit_Empl_Racf_Id = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Audit_Empl_Racf_Id", "AUDIT-EMPL-RACF-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "AUDIT_EMPL_RACF_ID");
        cwf_Image_Xref_Cabinet_Id = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Cabinet_Id", "CABINET-ID", FieldType.STRING, 12, RepeatingFieldStrategy.None, 
            "CABINET_ID");
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        sort01Pnd_Record_DateOld = internalLoopRecord.newFieldInRecord("Sort01_Pnd_Record_Date_OLD", "Pnd_Record_Date_OLD", FieldType.DATE);
        sort01Source_IdOld = internalLoopRecord.newFieldInRecord("Sort01_Source_Id_OLD", "Source_Id_OLD", FieldType.STRING, 6);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Support_Tbl.reset();
        vw_cwf_Image_Xref.reset();
        internalLoopRecord.reset();

        ldaEfsl0040.initializeValues();

        localVariables.reset();
        pnd_Nothing_To_Rpt.setInitialValue(true);
        pnd_Record.setInitialValue("�E�&l1o2a5.6c72p3e66F�&a2L�(9905X�&l0L");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Efsp7063() throws Exception
    {
        super("Efsp7063");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Efsp7063|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        while(true)
        {
            try
            {
                getReports().definePrinter(2, "AUDIT");                                                                                                                   //Natural: DEFINE PRINTER ( AUDIT = 1 )
                //*  NEEDS TO START AT THE FIRST OF THE MONTH.                                                                                                            //Natural: FORMAT ( AUDIT ) LS = 133 PS = 60 ZP = OFF
                pnd_Program.setValue(Global.getPROGRAM());                                                                                                                //Natural: ASSIGN #PROGRAM = *PROGRAM
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Source_Id,pnd_Input_Data);                                                                   //Natural: INPUT #INPUT-SOURCE-ID #INPUT-DATA
                pnd_Input_Data_Pnd_Input_Dd.setValue("01");                                                                                                               //Natural: ASSIGN #INPUT-DD := '01'
                pnd_Input_Date_Time.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Input_Data_Pnd_Input_Yymm, pnd_Input_Data_Pnd_Input_Dd,                  //Natural: COMPRESS #INPUT-YYMM #INPUT-DD #INPUT-TIME-START INTO #INPUT-DATE-TIME LEAVING NO SPACE
                    pnd_Input_Data_Pnd_Input_Time_Start));
                pnd_Start_Key.setValueEdited(new ReportEditMask("YYYYMMDDHHII"),pnd_Input_Date_Time);                                                                     //Natural: MOVE EDITED #INPUT-DATE-TIME TO #START-KEY ( EM = YYYYMMDDHHII )
                //*  READ THE AUDIT RECORDS FOR A SELECTED DAY
                ldaEfsl0040.getVw_cwf_Upload_Audit().startDatabaseRead                                                                                                    //Natural: READ CWF-UPLOAD-AUDIT BY UPLD-DATE-TIME STARTING FROM #START-KEY
                (
                "READ_AUDIT",
                new Wc[] { new Wc("UPLD_DATE_TIME", ">=", pnd_Start_Key, WcType.BY) },
                new Oc[] { new Oc("UPLD_DATE_TIME", "ASC") }
                );
                READ_AUDIT:
                while (condition(ldaEfsl0040.getVw_cwf_Upload_Audit().readNextRow("READ_AUDIT")))
                {
                    //*   REJECT IF CWF-UPLOAD-AUDIT.EXCEPTION-MSG NE ' ' /* IGNORE ERROR LOGS
                    //* ---  COS 12.24.1997
                    //* ---  INCLUDE 'FORMS' IF 'CIRS'
                    if (condition(ldaEfsl0040.getCwf_Upload_Audit_Source_Id().notEquals(pnd_Input_Source_Id) && ! (ldaEfsl0040.getCwf_Upload_Audit_Source_Id().equals("FORMS")  //Natural: REJECT IF CWF-UPLOAD-AUDIT.SOURCE-ID NE #INPUT-SOURCE-ID AND NOT ( CWF-UPLOAD-AUDIT.SOURCE-ID EQ 'FORMS' AND #INPUT-SOURCE-ID EQ 'CIRS' )
                        && pnd_Input_Source_Id.equals("CIRS"))))
                    {
                        continue;
                    }
                    //*  IGNORE
                    if (condition(ldaEfsl0040.getCwf_Upload_Audit_Mins_Uploaded_Cnt().equals(getZero()) && ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(1).notEquals(" "))) //Natural: REJECT IF CWF-UPLOAD-AUDIT.MINS-UPLOADED-CNT = 0 AND CWF-UPLOAD-AUDIT.ERROR-MSG ( 1 ) NE ' '
                    {
                        continue;
                    }
                    //*  ONLY SELECT RECORDS FOR THE CURRENT MONTH
                    pnd_Record_Date.setValue(ldaEfsl0040.getCwf_Upload_Audit_Upld_Date_Time());                                                                           //Natural: ASSIGN #RECORD-DATE = CWF-UPLOAD-AUDIT.UPLD-DATE-TIME
                    pnd_Record_Time.setValueEdited(ldaEfsl0040.getCwf_Upload_Audit_Upld_Date_Time(),new ReportEditMask("HHIISS"));                                        //Natural: MOVE EDITED CWF-UPLOAD-AUDIT.UPLD-DATE-TIME ( EM = HHIISS ) TO #RECORD-TIME
                    //*  THIS IS PREVIOUS DAY's work
                    if (condition(pnd_Record_Time_Pnd_Record_Hhmmss.less(pnd_Input_Data_Pnd_Input_Time_N)))                                                               //Natural: IF #RECORD-HHMMSS < #INPUT-TIME-N
                    {
                        pnd_Record_Date.nsubtract(1);                                                                                                                     //Natural: SUBTRACT 1 FROM #RECORD-DATE
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Record_Yymm.setValueEdited(pnd_Record_Date,new ReportEditMask("YYYYMM"));                                                                         //Natural: MOVE EDITED #RECORD-DATE ( EM = YYYYMM ) TO #RECORD-YYMM
                    if (condition(pnd_Record_Yymm.notEquals(pnd_Input_Data_Pnd_Input_Yymm)))                                                                              //Natural: IF #RECORD-YYMM NE #INPUT-YYMM
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM IMMEDIATE
                    }                                                                                                                                                     //Natural: END-IF
                    //*  MKAZI *
                    pnd_Image_Pointer_Key_Pnd_Image_Source.setValue(ldaEfsl0040.getCwf_Upload_Audit_Source_Id());                                                         //Natural: ASSIGN #IMAGE-SOURCE := CWF-UPLOAD-AUDIT.SOURCE-ID
                    pnd_Image_Pointer_Key_Pnd_Image_Batch.setValue(ldaEfsl0040.getCwf_Upload_Audit_Batch_Nbr());                                                          //Natural: ASSIGN #IMAGE-BATCH := CWF-UPLOAD-AUDIT.BATCH-NBR
                    pnd_Image_Pointer_Key_Pnd_Image_Batch_Nbr.setValue(ldaEfsl0040.getCwf_Upload_Audit_Batch_Ext_Nbr());                                                  //Natural: ASSIGN #IMAGE-BATCH-NBR := CWF-UPLOAD-AUDIT.BATCH-EXT-NBR
                    pnd_Image_Combine.compute(new ComputeParameters(false, pnd_Image_Combine), (pnd_Image_Pointer_Key_Pnd_Image_Batch_Nbr.divide(100)).add(pnd_Image_Pointer_Key_Pnd_Image_Batch)); //Natural: COMPUTE #IMAGE-COMBINE = ( #IMAGE-BATCH-NBR / 100 ) + #IMAGE-BATCH
                    pnd_Image_Platter.reset();                                                                                                                            //Natural: RESET #IMAGE-PLATTER
                    vw_cwf_Image_Xref.startDatabaseRead                                                                                                                   //Natural: READ ( 1 ) CWF-IMAGE-XREF WITH SOURCE-ID-BATCH-ID-MIN-KEY = #IMAGE-POINTER-KEY
                    (
                    "READ01",
                    new Wc[] { new Wc("SOURCE_ID_BATCH_ID_MIN_KEY", ">=", pnd_Image_Pointer_Key, WcType.BY) },
                    new Oc[] { new Oc("SOURCE_ID_BATCH_ID_MIN_KEY", "ASC") },
                    1
                    );
                    READ01:
                    while (condition(vw_cwf_Image_Xref.readNextRow("READ01")))
                    {
                        if (condition(cwf_Image_Xref_Batch_Id.notEquals(pnd_Image_Combine) || cwf_Image_Xref_Source_Id.notEquals(pnd_Image_Pointer_Key_Pnd_Image_Source))) //Natural: IF CWF-IMAGE-XREF.BATCH-ID NE #IMAGE-COMBINE OR CWF-IMAGE-XREF.SOURCE-ID NE #IMAGE-SOURCE
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Image_Platter.setValue(cwf_Image_Xref_Image_Document_Address_Cde);                                                                            //Natural: ASSIGN #IMAGE-PLATTER := IMAGE-DOCUMENT-ADDRESS-CDE
                    }                                                                                                                                                     //Natural: END-READ
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_AUDIT"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_AUDIT"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getSort().writeSortInData(ldaEfsl0040.getCwf_Upload_Audit_Source_Id(), pnd_Record_Date, pnd_Image_Platter, ldaEfsl0040.getCwf_Upload_Audit_Upld_Date_Time(),  //Natural: END-ALL
                        ldaEfsl0040.getCwf_Upload_Audit_Images_Uploaded_Cnt(), ldaEfsl0040.getCwf_Upload_Audit_Mins_Uploaded_Cnt(), ldaEfsl0040.getCwf_Upload_Audit_Count_Castunidentified_Min_Nbr());
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                getSort().sortData(ldaEfsl0040.getCwf_Upload_Audit_Source_Id(), pnd_Record_Date, pnd_Image_Platter);                                                      //Natural: SORT BY CWF-UPLOAD-AUDIT.SOURCE-ID #RECORD-DATE #IMAGE-PLATTER USING CWF-UPLOAD-AUDIT.UPLD-DATE-TIME CWF-UPLOAD-AUDIT.IMAGES-UPLOADED-CNT CWF-UPLOAD-AUDIT.MINS-UPLOADED-CNT CWF-UPLOAD-AUDIT.C*UNIDENTIFIED-MIN-NBR
                boolean endOfDataSort01 = true;
                boolean firstSort01 = true;
                SORT01:
                while (condition(getSort().readSortOutData(ldaEfsl0040.getCwf_Upload_Audit_Source_Id(), pnd_Record_Date, pnd_Image_Platter, ldaEfsl0040.getCwf_Upload_Audit_Upld_Date_Time(), 
                    ldaEfsl0040.getCwf_Upload_Audit_Images_Uploaded_Cnt(), ldaEfsl0040.getCwf_Upload_Audit_Mins_Uploaded_Cnt(), ldaEfsl0040.getCwf_Upload_Audit_Count_Castunidentified_Min_Nbr())))
                {
                    if (condition(getSort().getAstCOUNTER().greater(0)))
                    {
                        atBreakEventSort01(false);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom()))
                                break;
                            else if (condition(Global.isEscapeBottomImmediate()))
                            {
                                endOfDataSort01 = false;
                                break;
                            }
                            else if (condition(Global.isEscapeTop()))
                            continue;
                            else if (condition())
                            return;
                        }
                    }
                    //*    #RECORD-DATE          /*---  COS 12.24.1997
                    pnd_Last_Dt.setValue(pnd_Record_Date);                                                                                                                //Natural: ASSIGN #LAST-DT = #RECORD-DATE
                    //*                                                                                                                                                   //Natural: AT BREAK OF #IMAGE-PLATTER
                    //*                                                                                                                                                   //Natural: AT BREAK #RECORD-DATE
                    //*                                                                                                                                                   //Natural: AT BREAK CWF-UPLOAD-AUDIT.SOURCE-ID
                    //* * ADD THE TOTALS
                    pnd_Summary_Pnd_Batch_Count.nadd(1);                                                                                                                  //Natural: ADD 1 TO #SUMMARY.#BATCH-COUNT
                    pnd_Daily_Pnd_Batch_Count.nadd(1);                                                                                                                    //Natural: ADD 1 TO #DAILY.#BATCH-COUNT
                    pnd_Total_Pnd_Batch_Count.nadd(1);                                                                                                                    //Natural: ADD 1 TO #TOTAL.#BATCH-COUNT
                    pnd_Daily_Pnd_Mins_Count.nadd(ldaEfsl0040.getCwf_Upload_Audit_Mins_Uploaded_Cnt());                                                                   //Natural: ADD CWF-UPLOAD-AUDIT.MINS-UPLOADED-CNT TO #DAILY.#MINS-COUNT
                    pnd_Summary_Pnd_Mins_Count.nadd(ldaEfsl0040.getCwf_Upload_Audit_Mins_Uploaded_Cnt());                                                                 //Natural: ADD CWF-UPLOAD-AUDIT.MINS-UPLOADED-CNT TO #SUMMARY.#MINS-COUNT
                    pnd_Total_Pnd_Mins_Count.nadd(ldaEfsl0040.getCwf_Upload_Audit_Mins_Uploaded_Cnt());                                                                   //Natural: ADD CWF-UPLOAD-AUDIT.MINS-UPLOADED-CNT TO #TOTAL.#MINS-COUNT
                    pnd_Daily_Pnd_Images_Count.nadd(ldaEfsl0040.getCwf_Upload_Audit_Images_Uploaded_Cnt());                                                               //Natural: ADD CWF-UPLOAD-AUDIT.IMAGES-UPLOADED-CNT TO #DAILY.#IMAGES-COUNT
                    pnd_Summary_Pnd_Images_Count.nadd(ldaEfsl0040.getCwf_Upload_Audit_Images_Uploaded_Cnt());                                                             //Natural: ADD CWF-UPLOAD-AUDIT.IMAGES-UPLOADED-CNT TO #SUMMARY.#IMAGES-COUNT
                    pnd_Total_Pnd_Images_Count.nadd(ldaEfsl0040.getCwf_Upload_Audit_Images_Uploaded_Cnt());                                                               //Natural: ADD CWF-UPLOAD-AUDIT.IMAGES-UPLOADED-CNT TO #TOTAL.#IMAGES-COUNT
                    pnd_Total_Pnd_Mins_No_Pin_Count.nadd(ldaEfsl0040.getCwf_Upload_Audit_Count_Castunidentified_Min_Nbr());                                               //Natural: COMPUTE #MINS-NO-PIN-COUNT = #MINS-NO-PIN-COUNT + CWF-UPLOAD-AUDIT.C*UNIDENTIFIED-MIN-NBR
                    pnd_Nothing_To_Rpt.setValue(false);                                                                                                                   //Natural: ASSIGN #NOTHING-TO-RPT := FALSE
                    pnd_Image_Platter_Old.setValue(pnd_Image_Platter);                                                                                                    //Natural: ASSIGN #IMAGE-PLATTER-OLD := #IMAGE-PLATTER
                    sort01Pnd_Record_DateOld.setValue(pnd_Record_Date);                                                                                                   //Natural: END-SORT
                    sort01Source_IdOld.setValue(ldaEfsl0040.getCwf_Upload_Audit_Source_Id());
                }
                if (condition(getSort().getAstCOUNTER().greater(0)))
                {
                    atBreakEventSort01(endOfDataSort01);
                }
                endSort();
                if (condition(pnd_Nothing_To_Rpt.getBoolean()))                                                                                                           //Natural: IF #NOTHING-TO-RPT
                {
                    getReports().write(2, ReportOption.NOTITLE,"Nothing to report.");                                                                                     //Natural: WRITE ( AUDIT ) NOTITLE 'Nothing to report.'
                    if (Global.isEscape()) return;
                    //* ---  WRITE TOTALS     COS 12.24.1997
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(getReports().getAstLinesLeft(0).less(16)))                                                                                              //Natural: NEWPAGE ( AUDIT ) IF LESS THAN 16 LINES LEFT
                    {
                        getReports().newPage(0);
                        if (condition(Global.isEscape())){return;}
                    }
                    getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Efsf0065.class));                                                                  //Natural: WRITE ( AUDIT ) NOTITLE USING FORM 'EFSF0065'
                }                                                                                                                                                         //Natural: END-IF
                //*  BEING SUBMITTED VIA CTP8000
                //*   SET LANDSCAPE
                getReports().newPage(new ReportSpecification(0));                                                                                                         //Natural: NEWPAGE ( AUDIT )
                if (condition(Global.isEscape())){return;}
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( AUDIT )
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    if (condition(! (DbsUtil.maskMatches(Global.getINIT_PROGRAM(),".....'CWD'"))))                                                                        //Natural: IF *INIT-PROGRAM NE MASK ( .....'CWD' )
                    {
                        getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,pnd_Record);                                                                        //Natural: WRITE ( AUDIT ) NOTITLE NOHDR #RECORD
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(2, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Efsf7063.class));                                              //Natural: WRITE ( AUDIT ) NOTITLE NOHDR USING FORM 'EFSF7063'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean pnd_Image_PlatterIsBreak = pnd_Image_Platter.isBreak(endOfData);
        boolean pnd_Record_DateIsBreak = pnd_Record_Date.isBreak(endOfData);
        boolean ldaEfsl0040_getCwf_Upload_Audit_Source_IdIsBreak = ldaEfsl0040.getCwf_Upload_Audit_Source_Id().isBreak(endOfData);
        if (condition(pnd_Image_PlatterIsBreak || pnd_Record_DateIsBreak || ldaEfsl0040_getCwf_Upload_Audit_Source_IdIsBreak))
        {
            pnd_End_Dt.compute(new ComputeParameters(false, pnd_End_Dt), sort01Pnd_Record_DateOld.add(1));                                                                //Natural: ASSIGN #END-DT = OLD ( #RECORD-DATE ) + 1
            getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Efsf7064.class));                                                                          //Natural: WRITE ( AUDIT ) NOTITLE USING FORM 'EFSF7064'
            //*  RESET DAILY TOTALS
            pnd_Summary.reset();                                                                                                                                          //Natural: RESET #SUMMARY
            //* ---  COS 12.24.1997
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pnd_Record_DateIsBreak || ldaEfsl0040_getCwf_Upload_Audit_Source_IdIsBreak))
        {
            //*  DOUBLE SPACE EACH PRINT LINE
            pnd_End_Dt.compute(new ComputeParameters(false, pnd_End_Dt), sort01Pnd_Record_DateOld.add(1));                                                                //Natural: ASSIGN #END-DT = OLD ( #RECORD-DATE ) + 1
            getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Efsf7164.class));                                                                          //Natural: WRITE ( AUDIT ) NOTITLE USING FORM 'EFSF7164'
            getReports().write(2, ReportOption.NOTITLE," ");                                                                                                              //Natural: WRITE ( AUDIT ) NOTITLE ' '
            if (condition(Global.isEscape())) return;
            //*  RESET DAILY TOTALS
            pnd_Daily.reset();                                                                                                                                            //Natural: RESET #DAILY
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaEfsl0040_getCwf_Upload_Audit_Source_IdIsBreak))
        {
            if (condition(ldaEfsl0040.getCwf_Upload_Audit_Source_Id().notEquals(sort01Source_IdOld)))                                                                     //Natural: IF CWF-UPLOAD-AUDIT.SOURCE-ID NE OLD ( CWF-UPLOAD-AUDIT.SOURCE-ID )
            {
                if (condition(getReports().getAstLinesLeft(0).less(4)))                                                                                                   //Natural: NEWPAGE ( AUDIT ) IF LESS THAN 4 LINES LEFT
                {
                    getReports().newPage(0);
                    if (condition(Global.isEscape())){return;}
                }
                getReports().write(2, NEWLINE,"*",new RepeatItem(28),NEWLINE,new ColumnSpacing(3),ldaEfsl0040.getCwf_Upload_Audit_Source_Id(),"START HERE...",NEWLINE,"*",new  //Natural: WRITE ( AUDIT ) /'*' ( 28 ) / 3X CWF-UPLOAD-AUDIT.SOURCE-ID 'START HERE...' /'*' ( 28 ) /
                    RepeatItem(28),NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(2, "LS=133 PS=60 ZP=OFF");
    }
}
