/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:46:00 PM
**        * FROM NATURAL PROGRAM : Cwfb8626
************************************************************
**        * FILE NAME            : Cwfb8626.java
**        * CLASS NAME           : Cwfb8626
**        * INSTANCE NAME        : Cwfb8626
************************************************************
************************************************************************
*
* SYSTEM   : CORPORATE WORKFLOW
* PROGRAM  : CWFB8626
* TITLE    : MONTHLY EXTRACT OF SECOND SIGHTED REQUESTS
* FUNCTION : READ CWF-MASTER-INDEX-VIEW (045/181);
*          : SELECT ALL REQUESTS SECOND SIGHTED FOR THE MONTH;
*          : (ONE OR MORE SECOND SIGHT TRANSACTIONS PER REQUEST)
*          : EXTRACT REQUEST INFORMATION; CREATE A WORKFILE.
* CREATED  : 01/2009 RSALGADO
* HISTORY  : 07/24/2012 VINODHKUMAR RAMACHANDRAN
*          :            INCLUDED THE UNIT 'CDMSC' TO GET THE WPID
*          :            'TIS MCC' IN THE REPORT
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb8626 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_mit;
    private DbsField mit_Pin_Nbr;
    private DbsField mit_Rqst_Log_Dte_Tme;

    private DbsGroup mit__R_Field_1;
    private DbsField mit_Rqst_Log_Dte;
    private DbsField mit_Rqst_Log_Tme;

    private DbsGroup mit__R_Field_2;
    private DbsField mit_Rqst_Log_Dte_Yyyymmdd;
    private DbsField mit_Work_Prcss_Id;
    private DbsField mit_Status_Cde;
    private DbsField mit_Last_Chnge_Dte_Tme;
    private DbsField mit_Tiaa_Rcvd_Dte;
    private DbsField mit_Crprte_Due_Dte_Tme;
    private DbsField mit_Crprte_Status_Ind;
    private DbsField mit_Status_Updte_Dte_Tme;

    private DataAccessProgramView vw_mit_Hist;
    private DbsField mit_Hist_Pin_Nbr;
    private DbsField mit_Hist_Empl_Oprtr_Cde;
    private DbsField mit_Hist_Rqst_Log_Dte_Tme;

    private DbsGroup mit_Hist__R_Field_3;
    private DbsField mit_Hist_Rqst_Log_Dte;
    private DbsField mit_Hist_Rqst_Log_Tme;
    private DbsField mit_Hist_Work_Prcss_Id;
    private DbsField mit_Hist_Orgnl_Unit_Cde;
    private DbsField mit_Hist_Unit_Cde;
    private DbsField mit_Hist_Admin_Status_Cde;
    private DbsField mit_Hist_Last_Chnge_Dte_Tme;
    private DbsField mit_Hist_Last_Chnge_Oprtr_Cde;
    private DbsField pnd_History_Key;

    private DbsGroup pnd_History_Key__R_Field_4;
    private DbsField pnd_History_Key_Pnd_K_Log_Dte_Tme;
    private DbsField pnd_History_Key_Pnd_K_Last_Chnge_Dte_Tme;
    private DbsField pnd_Work_File;

    private DbsGroup pnd_Work_File__R_Field_5;
    private DbsField pnd_Work_File_Pnd_Wf_Wpid;
    private DbsField pnd_Work_File_Pnd_Wf_Pin;
    private DbsField pnd_Work_File_Pnd_Wf_Closed_Stts;
    private DbsField pnd_Work_File_Pnd_Wf_Closed_Dte_Tme_A;

    private DbsGroup pnd_Work_File__R_Field_6;
    private DbsField pnd_Work_File_Pnd_Wf_Closed_Dte;
    private DbsField pnd_Work_File__Filler1;
    private DbsField pnd_Work_File_Pnd_Wf_Rcvd_Dte_A;

    private DbsGroup pnd_Work_File__R_Field_7;
    private DbsField pnd_Work_File_Pnd_Wf_Rcvd_Dte;
    private DbsField pnd_Work_File_Pnd_Wf_Oprtr;
    private DbsField pnd_Work_File_Pnd_Wf_Auth_Queue_Dte_Tme;

    private DbsGroup pnd_Work_File__R_Field_8;
    private DbsField pnd_Work_File_Pnd_Wf_Auth_Queue_Dte;
    private DbsField pnd_Work_File_Pnd_Wf_Auth_Queue_Tme;
    private DbsField pnd_Work_File__Filler2;
    private DbsField pnd_Work_File_Pnd_Wf_2sight_Oprtr;
    private DbsField pnd_Work_File_Pnd_Wf_2sight_Dte_Tme;

    private DbsGroup pnd_Work_File__R_Field_9;
    private DbsField pnd_Work_File_Pnd_Wf_2sight_Dte;
    private DbsField pnd_Work_File_Pnd_Wf_2sight_Tme;
    private DbsField pnd_Work_File__Filler3;
    private DbsField pnd_Work_File_Pnd_Wf_Return_Oprtr;
    private DbsField pnd_Work_File_Pnd_Wf_Return_Unit;
    private DbsField pnd_Work_File_Pnd_Wf_Return_Stts;
    private DbsField pnd_Work_File_Pnd_Wf_Return_Dte_Tme;

    private DbsGroup pnd_Work_File__R_Field_10;
    private DbsField pnd_Work_File_Pnd_Wf_Return_Dte;
    private DbsField pnd_Work_File_Pnd_Wf_Return_Tme;
    private DbsField pnd_Work_File__Filler4;

    private DbsGroup pnd_Work_File__R_Field_11;
    private DbsField pnd_Work_File__Filler5;
    private DbsField pnd_Work_File_Pnd_Wf_Run;
    private DbsField pnd_Work_File__Filler6;
    private DbsField pnd_Work_File_Pnd_Wf_Start_Dte;
    private DbsField pnd_Work_File__Filler7;
    private DbsField pnd_Work_File_Pnd_Wf_End_Dte;
    private DbsField pnd_In_Auth_Queue;
    private DbsField pnd_Authorized;
    private DbsField pnd_Assigned;
    private DbsField pnd_Sv_Oprtr;
    private DbsField pnd_Sv_Auth_Queue_Dte_Tme;

    private DbsGroup pnd_Units;
    private DbsField pnd_Units_Pnd_Unit_01;
    private DbsField pnd_Units_Pnd_Unit_02;
    private DbsField pnd_Units_Pnd_Unit_03;

    private DbsGroup pnd_Units__R_Field_12;
    private DbsField pnd_Units_Pnd_T_Unit;
    private DbsField pnd_Start_Datd;
    private DbsField pnd_Start_Yyyymmdd;

    private DbsGroup pnd_Start_Yyyymmdd__R_Field_13;
    private DbsField pnd_Start_Yyyymmdd_Pnd_Start_Date;

    private DbsGroup pnd_Start_Yyyymmdd__R_Field_14;
    private DbsField pnd_Start_Yyyymmdd_Pnd_Start_Yyyymm;
    private DbsField pnd_Start_Yyyymmdd_Pnd_Start_Dd;
    private DbsField pnd_End_Datd;
    private DbsField pnd_End_Yyyymmdd;

    private DbsGroup pnd_End_Yyyymmdd__R_Field_15;
    private DbsField pnd_End_Yyyymmdd_Pnd_End_Date;

    private DbsGroup pnd_End_Yyyymmdd__R_Field_16;
    private DbsField pnd_End_Yyyymmdd_Pnd_End_Yyyymm;
    private DbsField pnd_End_Yyyymmdd_Pnd_End_Dd;
    private DbsField pnd_Wk_Latest_Stts;
    private DbsField pnd_Log_Dte_Tme;
    private DbsField pnd_Write_Count;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_mit = new DataAccessProgramView(new NameInfo("vw_mit", "MIT"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        mit_Pin_Nbr = vw_mit.getRecord().newFieldInGroup("mit_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "PIN_NBR");
        mit_Pin_Nbr.setDdmHeader("PIN");
        mit_Rqst_Log_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "RQST_LOG_DTE_TME");
        mit_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");

        mit__R_Field_1 = vw_mit.getRecord().newGroupInGroup("mit__R_Field_1", "REDEFINE", mit_Rqst_Log_Dte_Tme);
        mit_Rqst_Log_Dte = mit__R_Field_1.newFieldInGroup("mit_Rqst_Log_Dte", "RQST-LOG-DTE", FieldType.STRING, 8);
        mit_Rqst_Log_Tme = mit__R_Field_1.newFieldInGroup("mit_Rqst_Log_Tme", "RQST-LOG-TME", FieldType.STRING, 7);

        mit__R_Field_2 = vw_mit.getRecord().newGroupInGroup("mit__R_Field_2", "REDEFINE", mit_Rqst_Log_Dte_Tme);
        mit_Rqst_Log_Dte_Yyyymmdd = mit__R_Field_2.newFieldInGroup("mit_Rqst_Log_Dte_Yyyymmdd", "RQST-LOG-DTE-YYYYMMDD", FieldType.NUMERIC, 8);
        mit_Work_Prcss_Id = vw_mit.getRecord().newFieldInGroup("mit_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        mit_Work_Prcss_Id.setDdmHeader("WORK/ID");
        mit_Status_Cde = vw_mit.getRecord().newFieldInGroup("mit_Status_Cde", "STATUS-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, "STATUS_CDE");
        mit_Last_Chnge_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, 
            "LAST_CHNGE_DTE_TME");
        mit_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");
        mit_Tiaa_Rcvd_Dte = vw_mit.getRecord().newFieldInGroup("mit_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_RCVD_DTE");
        mit_Crprte_Due_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Crprte_Due_Dte_Tme", "CRPRTE-DUE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "CRPRTE_DUE_DTE_TME");
        mit_Crprte_Due_Dte_Tme.setDdmHeader("WORKRQST/DUE DATE");
        mit_Crprte_Status_Ind = vw_mit.getRecord().newFieldInGroup("mit_Crprte_Status_Ind", "CRPRTE-STATUS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CRPRTE_STATUS_IND");
        mit_Crprte_Status_Ind.setDdmHeader("CORP/STAT");
        mit_Status_Updte_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Status_Updte_Dte_Tme", "STATUS-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "STATUS_UPDTE_DTE_TME");
        mit_Status_Updte_Dte_Tme.setDdmHeader("STATUS UPDATE/DATE-TIME");
        registerRecord(vw_mit);

        vw_mit_Hist = new DataAccessProgramView(new NameInfo("vw_mit_Hist", "MIT-HIST"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        mit_Hist_Pin_Nbr = vw_mit_Hist.getRecord().newFieldInGroup("mit_Hist_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "PIN_NBR");
        mit_Hist_Pin_Nbr.setDdmHeader("PIN");
        mit_Hist_Empl_Oprtr_Cde = vw_mit_Hist.getRecord().newFieldInGroup("mit_Hist_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "EMPL_OPRTR_CDE");
        mit_Hist_Empl_Oprtr_Cde.setDdmHeader("EMPLOYEE/ID");
        mit_Hist_Rqst_Log_Dte_Tme = vw_mit_Hist.getRecord().newFieldInGroup("mit_Hist_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "RQST_LOG_DTE_TME");
        mit_Hist_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");

        mit_Hist__R_Field_3 = vw_mit_Hist.getRecord().newGroupInGroup("mit_Hist__R_Field_3", "REDEFINE", mit_Hist_Rqst_Log_Dte_Tme);
        mit_Hist_Rqst_Log_Dte = mit_Hist__R_Field_3.newFieldInGroup("mit_Hist_Rqst_Log_Dte", "RQST-LOG-DTE", FieldType.STRING, 8);
        mit_Hist_Rqst_Log_Tme = mit_Hist__R_Field_3.newFieldInGroup("mit_Hist_Rqst_Log_Tme", "RQST-LOG-TME", FieldType.STRING, 7);
        mit_Hist_Work_Prcss_Id = vw_mit_Hist.getRecord().newFieldInGroup("mit_Hist_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        mit_Hist_Work_Prcss_Id.setDdmHeader("WORK/ID");
        mit_Hist_Orgnl_Unit_Cde = vw_mit_Hist.getRecord().newFieldInGroup("mit_Hist_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ORGNL_UNIT_CDE");
        mit_Hist_Orgnl_Unit_Cde.setDdmHeader("LOG/UNIT");
        mit_Hist_Unit_Cde = vw_mit_Hist.getRecord().newFieldInGroup("mit_Hist_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "UNIT_CDE");
        mit_Hist_Unit_Cde.setDdmHeader("UNIT/CODE");
        mit_Hist_Admin_Status_Cde = vw_mit_Hist.getRecord().newFieldInGroup("mit_Hist_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "ADMIN_STATUS_CDE");
        mit_Hist_Last_Chnge_Dte_Tme = vw_mit_Hist.getRecord().newFieldInGroup("mit_Hist_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 
            15, RepeatingFieldStrategy.None, "LAST_CHNGE_DTE_TME");
        mit_Hist_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");
        mit_Hist_Last_Chnge_Oprtr_Cde = vw_mit_Hist.getRecord().newFieldInGroup("mit_Hist_Last_Chnge_Oprtr_Cde", "LAST-CHNGE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "LAST_CHNGE_OPRTR_CDE");
        mit_Hist_Last_Chnge_Oprtr_Cde.setDdmHeader("LOG CREATE/OPERATOR");
        registerRecord(vw_mit_Hist);

        pnd_History_Key = localVariables.newFieldInRecord("pnd_History_Key", "#HISTORY-KEY", FieldType.STRING, 30);

        pnd_History_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_History_Key__R_Field_4", "REDEFINE", pnd_History_Key);
        pnd_History_Key_Pnd_K_Log_Dte_Tme = pnd_History_Key__R_Field_4.newFieldInGroup("pnd_History_Key_Pnd_K_Log_Dte_Tme", "#K-LOG-DTE-TME", FieldType.STRING, 
            15);
        pnd_History_Key_Pnd_K_Last_Chnge_Dte_Tme = pnd_History_Key__R_Field_4.newFieldInGroup("pnd_History_Key_Pnd_K_Last_Chnge_Dte_Tme", "#K-LAST-CHNGE-DTE-TME", 
            FieldType.NUMERIC, 15);
        pnd_Work_File = localVariables.newFieldInRecord("pnd_Work_File", "#WORK-FILE", FieldType.STRING, 250);

        pnd_Work_File__R_Field_5 = localVariables.newGroupInRecord("pnd_Work_File__R_Field_5", "REDEFINE", pnd_Work_File);
        pnd_Work_File_Pnd_Wf_Wpid = pnd_Work_File__R_Field_5.newFieldInGroup("pnd_Work_File_Pnd_Wf_Wpid", "#WF-WPID", FieldType.STRING, 6);
        pnd_Work_File_Pnd_Wf_Pin = pnd_Work_File__R_Field_5.newFieldInGroup("pnd_Work_File_Pnd_Wf_Pin", "#WF-PIN", FieldType.NUMERIC, 12);
        pnd_Work_File_Pnd_Wf_Closed_Stts = pnd_Work_File__R_Field_5.newFieldInGroup("pnd_Work_File_Pnd_Wf_Closed_Stts", "#WF-CLOSED-STTS", FieldType.STRING, 
            4);
        pnd_Work_File_Pnd_Wf_Closed_Dte_Tme_A = pnd_Work_File__R_Field_5.newFieldInGroup("pnd_Work_File_Pnd_Wf_Closed_Dte_Tme_A", "#WF-CLOSED-DTE-TME-A", 
            FieldType.STRING, 15);

        pnd_Work_File__R_Field_6 = pnd_Work_File__R_Field_5.newGroupInGroup("pnd_Work_File__R_Field_6", "REDEFINE", pnd_Work_File_Pnd_Wf_Closed_Dte_Tme_A);
        pnd_Work_File_Pnd_Wf_Closed_Dte = pnd_Work_File__R_Field_6.newFieldInGroup("pnd_Work_File_Pnd_Wf_Closed_Dte", "#WF-CLOSED-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_File__Filler1 = pnd_Work_File__R_Field_6.newFieldInGroup("pnd_Work_File__Filler1", "_FILLER1", FieldType.STRING, 7);
        pnd_Work_File_Pnd_Wf_Rcvd_Dte_A = pnd_Work_File__R_Field_5.newFieldInGroup("pnd_Work_File_Pnd_Wf_Rcvd_Dte_A", "#WF-RCVD-DTE-A", FieldType.STRING, 
            8);

        pnd_Work_File__R_Field_7 = pnd_Work_File__R_Field_5.newGroupInGroup("pnd_Work_File__R_Field_7", "REDEFINE", pnd_Work_File_Pnd_Wf_Rcvd_Dte_A);
        pnd_Work_File_Pnd_Wf_Rcvd_Dte = pnd_Work_File__R_Field_7.newFieldInGroup("pnd_Work_File_Pnd_Wf_Rcvd_Dte", "#WF-RCVD-DTE", FieldType.NUMERIC, 8);
        pnd_Work_File_Pnd_Wf_Oprtr = pnd_Work_File__R_Field_5.newFieldInGroup("pnd_Work_File_Pnd_Wf_Oprtr", "#WF-OPRTR", FieldType.STRING, 8);
        pnd_Work_File_Pnd_Wf_Auth_Queue_Dte_Tme = pnd_Work_File__R_Field_5.newFieldInGroup("pnd_Work_File_Pnd_Wf_Auth_Queue_Dte_Tme", "#WF-AUTH-QUEUE-DTE-TME", 
            FieldType.NUMERIC, 15);

        pnd_Work_File__R_Field_8 = pnd_Work_File__R_Field_5.newGroupInGroup("pnd_Work_File__R_Field_8", "REDEFINE", pnd_Work_File_Pnd_Wf_Auth_Queue_Dte_Tme);
        pnd_Work_File_Pnd_Wf_Auth_Queue_Dte = pnd_Work_File__R_Field_8.newFieldInGroup("pnd_Work_File_Pnd_Wf_Auth_Queue_Dte", "#WF-AUTH-QUEUE-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_File_Pnd_Wf_Auth_Queue_Tme = pnd_Work_File__R_Field_8.newFieldInGroup("pnd_Work_File_Pnd_Wf_Auth_Queue_Tme", "#WF-AUTH-QUEUE-TME", FieldType.NUMERIC, 
            4);
        pnd_Work_File__Filler2 = pnd_Work_File__R_Field_8.newFieldInGroup("pnd_Work_File__Filler2", "_FILLER2", FieldType.STRING, 3);
        pnd_Work_File_Pnd_Wf_2sight_Oprtr = pnd_Work_File__R_Field_5.newFieldInGroup("pnd_Work_File_Pnd_Wf_2sight_Oprtr", "#WF-2SIGHT-OPRTR", FieldType.STRING, 
            8);
        pnd_Work_File_Pnd_Wf_2sight_Dte_Tme = pnd_Work_File__R_Field_5.newFieldInGroup("pnd_Work_File_Pnd_Wf_2sight_Dte_Tme", "#WF-2SIGHT-DTE-TME", FieldType.STRING, 
            15);

        pnd_Work_File__R_Field_9 = pnd_Work_File__R_Field_5.newGroupInGroup("pnd_Work_File__R_Field_9", "REDEFINE", pnd_Work_File_Pnd_Wf_2sight_Dte_Tme);
        pnd_Work_File_Pnd_Wf_2sight_Dte = pnd_Work_File__R_Field_9.newFieldInGroup("pnd_Work_File_Pnd_Wf_2sight_Dte", "#WF-2SIGHT-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_File_Pnd_Wf_2sight_Tme = pnd_Work_File__R_Field_9.newFieldInGroup("pnd_Work_File_Pnd_Wf_2sight_Tme", "#WF-2SIGHT-TME", FieldType.NUMERIC, 
            4);
        pnd_Work_File__Filler3 = pnd_Work_File__R_Field_9.newFieldInGroup("pnd_Work_File__Filler3", "_FILLER3", FieldType.STRING, 3);
        pnd_Work_File_Pnd_Wf_Return_Oprtr = pnd_Work_File__R_Field_5.newFieldInGroup("pnd_Work_File_Pnd_Wf_Return_Oprtr", "#WF-RETURN-OPRTR", FieldType.STRING, 
            8);
        pnd_Work_File_Pnd_Wf_Return_Unit = pnd_Work_File__R_Field_5.newFieldInGroup("pnd_Work_File_Pnd_Wf_Return_Unit", "#WF-RETURN-UNIT", FieldType.STRING, 
            8);
        pnd_Work_File_Pnd_Wf_Return_Stts = pnd_Work_File__R_Field_5.newFieldInGroup("pnd_Work_File_Pnd_Wf_Return_Stts", "#WF-RETURN-STTS", FieldType.STRING, 
            4);
        pnd_Work_File_Pnd_Wf_Return_Dte_Tme = pnd_Work_File__R_Field_5.newFieldInGroup("pnd_Work_File_Pnd_Wf_Return_Dte_Tme", "#WF-RETURN-DTE-TME", FieldType.NUMERIC, 
            15);

        pnd_Work_File__R_Field_10 = pnd_Work_File__R_Field_5.newGroupInGroup("pnd_Work_File__R_Field_10", "REDEFINE", pnd_Work_File_Pnd_Wf_Return_Dte_Tme);
        pnd_Work_File_Pnd_Wf_Return_Dte = pnd_Work_File__R_Field_10.newFieldInGroup("pnd_Work_File_Pnd_Wf_Return_Dte", "#WF-RETURN-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_File_Pnd_Wf_Return_Tme = pnd_Work_File__R_Field_10.newFieldInGroup("pnd_Work_File_Pnd_Wf_Return_Tme", "#WF-RETURN-TME", FieldType.NUMERIC, 
            4);
        pnd_Work_File__Filler4 = pnd_Work_File__R_Field_10.newFieldInGroup("pnd_Work_File__Filler4", "_FILLER4", FieldType.STRING, 3);

        pnd_Work_File__R_Field_11 = localVariables.newGroupInRecord("pnd_Work_File__R_Field_11", "REDEFINE", pnd_Work_File);
        pnd_Work_File__Filler5 = pnd_Work_File__R_Field_11.newFieldInGroup("pnd_Work_File__Filler5", "_FILLER5", FieldType.STRING, 8);
        pnd_Work_File_Pnd_Wf_Run = pnd_Work_File__R_Field_11.newFieldInGroup("pnd_Work_File_Pnd_Wf_Run", "#WF-RUN", FieldType.STRING, 8);
        pnd_Work_File__Filler6 = pnd_Work_File__R_Field_11.newFieldInGroup("pnd_Work_File__Filler6", "_FILLER6", FieldType.STRING, 1);
        pnd_Work_File_Pnd_Wf_Start_Dte = pnd_Work_File__R_Field_11.newFieldInGroup("pnd_Work_File_Pnd_Wf_Start_Dte", "#WF-START-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_File__Filler7 = pnd_Work_File__R_Field_11.newFieldInGroup("pnd_Work_File__Filler7", "_FILLER7", FieldType.STRING, 1);
        pnd_Work_File_Pnd_Wf_End_Dte = pnd_Work_File__R_Field_11.newFieldInGroup("pnd_Work_File_Pnd_Wf_End_Dte", "#WF-END-DTE", FieldType.NUMERIC, 8);
        pnd_In_Auth_Queue = localVariables.newFieldInRecord("pnd_In_Auth_Queue", "#IN-AUTH-QUEUE", FieldType.BOOLEAN, 1);
        pnd_Authorized = localVariables.newFieldInRecord("pnd_Authorized", "#AUTHORIZED", FieldType.BOOLEAN, 1);
        pnd_Assigned = localVariables.newFieldInRecord("pnd_Assigned", "#ASSIGNED", FieldType.BOOLEAN, 1);
        pnd_Sv_Oprtr = localVariables.newFieldInRecord("pnd_Sv_Oprtr", "#SV-OPRTR", FieldType.STRING, 8);
        pnd_Sv_Auth_Queue_Dte_Tme = localVariables.newFieldInRecord("pnd_Sv_Auth_Queue_Dte_Tme", "#SV-AUTH-QUEUE-DTE-TME", FieldType.NUMERIC, 15);

        pnd_Units = localVariables.newGroupInRecord("pnd_Units", "#UNITS");
        pnd_Units_Pnd_Unit_01 = pnd_Units.newFieldInGroup("pnd_Units_Pnd_Unit_01", "#UNIT-01", FieldType.STRING, 8);
        pnd_Units_Pnd_Unit_02 = pnd_Units.newFieldInGroup("pnd_Units_Pnd_Unit_02", "#UNIT-02", FieldType.STRING, 8);
        pnd_Units_Pnd_Unit_03 = pnd_Units.newFieldInGroup("pnd_Units_Pnd_Unit_03", "#UNIT-03", FieldType.STRING, 8);

        pnd_Units__R_Field_12 = localVariables.newGroupInRecord("pnd_Units__R_Field_12", "REDEFINE", pnd_Units);
        pnd_Units_Pnd_T_Unit = pnd_Units__R_Field_12.newFieldArrayInGroup("pnd_Units_Pnd_T_Unit", "#T-UNIT", FieldType.STRING, 8, new DbsArrayController(1, 
            3));
        pnd_Start_Datd = localVariables.newFieldInRecord("pnd_Start_Datd", "#START-DATD", FieldType.DATE);
        pnd_Start_Yyyymmdd = localVariables.newFieldInRecord("pnd_Start_Yyyymmdd", "#START-YYYYMMDD", FieldType.NUMERIC, 8);

        pnd_Start_Yyyymmdd__R_Field_13 = localVariables.newGroupInRecord("pnd_Start_Yyyymmdd__R_Field_13", "REDEFINE", pnd_Start_Yyyymmdd);
        pnd_Start_Yyyymmdd_Pnd_Start_Date = pnd_Start_Yyyymmdd__R_Field_13.newFieldInGroup("pnd_Start_Yyyymmdd_Pnd_Start_Date", "#START-DATE", FieldType.STRING, 
            8);

        pnd_Start_Yyyymmdd__R_Field_14 = localVariables.newGroupInRecord("pnd_Start_Yyyymmdd__R_Field_14", "REDEFINE", pnd_Start_Yyyymmdd);
        pnd_Start_Yyyymmdd_Pnd_Start_Yyyymm = pnd_Start_Yyyymmdd__R_Field_14.newFieldInGroup("pnd_Start_Yyyymmdd_Pnd_Start_Yyyymm", "#START-YYYYMM", FieldType.NUMERIC, 
            6);
        pnd_Start_Yyyymmdd_Pnd_Start_Dd = pnd_Start_Yyyymmdd__R_Field_14.newFieldInGroup("pnd_Start_Yyyymmdd_Pnd_Start_Dd", "#START-DD", FieldType.NUMERIC, 
            2);
        pnd_End_Datd = localVariables.newFieldInRecord("pnd_End_Datd", "#END-DATD", FieldType.DATE);
        pnd_End_Yyyymmdd = localVariables.newFieldInRecord("pnd_End_Yyyymmdd", "#END-YYYYMMDD", FieldType.NUMERIC, 8);

        pnd_End_Yyyymmdd__R_Field_15 = localVariables.newGroupInRecord("pnd_End_Yyyymmdd__R_Field_15", "REDEFINE", pnd_End_Yyyymmdd);
        pnd_End_Yyyymmdd_Pnd_End_Date = pnd_End_Yyyymmdd__R_Field_15.newFieldInGroup("pnd_End_Yyyymmdd_Pnd_End_Date", "#END-DATE", FieldType.STRING, 8);

        pnd_End_Yyyymmdd__R_Field_16 = localVariables.newGroupInRecord("pnd_End_Yyyymmdd__R_Field_16", "REDEFINE", pnd_End_Yyyymmdd);
        pnd_End_Yyyymmdd_Pnd_End_Yyyymm = pnd_End_Yyyymmdd__R_Field_16.newFieldInGroup("pnd_End_Yyyymmdd_Pnd_End_Yyyymm", "#END-YYYYMM", FieldType.NUMERIC, 
            6);
        pnd_End_Yyyymmdd_Pnd_End_Dd = pnd_End_Yyyymmdd__R_Field_16.newFieldInGroup("pnd_End_Yyyymmdd_Pnd_End_Dd", "#END-DD", FieldType.NUMERIC, 2);
        pnd_Wk_Latest_Stts = localVariables.newFieldInRecord("pnd_Wk_Latest_Stts", "#WK-LATEST-STTS", FieldType.STRING, 4);
        pnd_Log_Dte_Tme = localVariables.newFieldInRecord("pnd_Log_Dte_Tme", "#LOG-DTE-TME", FieldType.NUMERIC, 15);
        pnd_Write_Count = localVariables.newFieldInRecord("pnd_Write_Count", "#WRITE-COUNT", FieldType.NUMERIC, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_mit.reset();
        vw_mit_Hist.reset();

        localVariables.reset();
        pnd_Units_Pnd_Unit_01.setInitialValue("IIPAP   ");
        pnd_Units_Pnd_Unit_02.setInitialValue("IIPAW   ");
        pnd_Units_Pnd_Unit_03.setInitialValue("CDMSC   ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb8626() throws Exception
    {
        super("Cwfb8626");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*  GET PRIOR MONTH's date
        //*                                                                                                                                                               //Natural: FORMAT PS = 55 LS = 132
        pnd_Start_Datd.compute(new ComputeParameters(false, pnd_Start_Datd), Global.getDATX().subtract(28));                                                              //Natural: ASSIGN #START-DATD := *DATX - 28
        //*  FIRST DAY OF
        pnd_Start_Yyyymmdd_Pnd_Start_Date.setValueEdited(pnd_Start_Datd,new ReportEditMask("YYYYMMDD"));                                                                  //Natural: MOVE EDITED #START-DATD ( EM = YYYYMMDD ) TO #START-DATE
        //*    PRIOR MONTH
        pnd_Start_Yyyymmdd_Pnd_Start_Dd.setValue(1);                                                                                                                      //Natural: MOVE 01 TO #START-DD
        //*  LAST DATE OF
        pnd_End_Yyyymmdd_Pnd_End_Date.setValue(pnd_Start_Yyyymmdd_Pnd_Start_Date);                                                                                        //Natural: MOVE #START-DATE TO #END-DATE
        //*    PRIOR MONTH
        pnd_End_Yyyymmdd_Pnd_End_Dd.setValue(31);                                                                                                                         //Natural: MOVE 31 TO #END-DD
        //* **************** TEST ** TEST ** TEST ** TEST ** TEST ** TEST ** TEST
        //* **
        //* **     #START-DATE  := 20070701
        //* **     #END-DATE    := 20070712
        //* **************** TEST ** TEST ** TEST ** TEST ** TEST ** TEST ** TEST
        //* *
        pnd_Work_File_Pnd_Wf_Run.setValue("CWFB8626");                                                                                                                    //Natural: ASSIGN #WF-RUN := 'CWFB8626'
        pnd_Work_File_Pnd_Wf_Start_Dte.setValue(pnd_Start_Yyyymmdd);                                                                                                      //Natural: ASSIGN #WF-START-DTE := #START-YYYYMMDD
        pnd_Work_File_Pnd_Wf_End_Dte.setValue(pnd_End_Yyyymmdd);                                                                                                          //Natural: ASSIGN #WF-END-DTE := #END-YYYYMMDD
        getWorkFiles().write(1, false, pnd_Work_File);                                                                                                                    //Natural: WRITE WORK FILE 1 #WORK-FILE
        pnd_Work_File.reset();                                                                                                                                            //Natural: RESET #WORK-FILE
        //*  LOG-DTE-TME/ACTIVE IND
        vw_mit.startDatabaseRead                                                                                                                                          //Natural: READ MIT BY ACTV-UNQUE-KEY STARTING FROM #START-DATE
        (
        "READ01",
        new Wc[] { new Wc("ACTV_UNQUE_KEY", ">=", pnd_Start_Yyyymmdd_Pnd_Start_Date, WcType.BY) },
        new Oc[] { new Oc("ACTV_UNQUE_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_mit.readNextRow("READ01")))
        {
            //* ************************* TEST **********************/*
            //*  IF #WRITE-COUNT > 100
            //*    ESCAPE BOTTOM
            //*  END-IF
            //*    STARTING FROM '20081016083242 '
            //*  IF MIT.RQST-LOG-DTE-TME > '200810169999999'
            //*  IF MIT.PIN-NBR NE  2300203
            //*    ESCAPE TOP
            //*  END-IF
            //*                                                      /*
            //* ************************* TEST **********************/8
                                                                                                                                                                          //Natural: PERFORM DETERMINE-REQUEST-HISTORY
            sub_Determine_Request_History();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-REQUEST-HISTORY
        //*    REQUEST PUT ON QUEUE FOR SECOND SIGHTED BY IIPAP OR IIPAW
        //*    REQUEST PICKED UP BY SECOND VERIFIER
        //*    REQUEST RETURNED
        //*    REQUEST RE-ASSIGNED BY SECOND VERIFIER
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-2SIGHT-RECORD
        //* ***********************************************************************
        if (condition(pnd_In_Auth_Queue.getBoolean()))                                                                                                                    //Natural: IF #IN-AUTH-QUEUE
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-2SIGHT-RECORD
            sub_Write_2sight_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, "=",pnd_Write_Count);                                                                                                                       //Natural: WRITE '=' #WRITE-COUNT
        if (Global.isEscape()) return;
    }
    private void sub_Determine_Request_History() throws Exception                                                                                                         //Natural: DETERMINE-REQUEST-HISTORY
    {
        if (BLNatReinput.isReinput()) return;

        pnd_In_Auth_Queue.setValue(false);                                                                                                                                //Natural: ASSIGN #IN-AUTH-QUEUE := FALSE
        pnd_Authorized.setValue(false);                                                                                                                                   //Natural: ASSIGN #AUTHORIZED := FALSE
        pnd_Assigned.setValue(false);                                                                                                                                     //Natural: ASSIGN #ASSIGNED := FALSE
        pnd_History_Key.reset();                                                                                                                                          //Natural: RESET #HISTORY-KEY
        pnd_History_Key_Pnd_K_Log_Dte_Tme.setValue(mit_Rqst_Log_Dte_Tme);                                                                                                 //Natural: ASSIGN #K-LOG-DTE-TME := MIT.RQST-LOG-DTE-TME
        pnd_History_Key_Pnd_K_Last_Chnge_Dte_Tme.setValue(0);                                                                                                             //Natural: ASSIGN #K-LAST-CHNGE-DTE-TME := 0
        pnd_Work_File.reset();                                                                                                                                            //Natural: RESET #WORK-FILE
        pnd_Work_File_Pnd_Wf_Pin.setValue(mit_Pin_Nbr);                                                                                                                   //Natural: ASSIGN #WF-PIN := MIT.PIN-NBR
        pnd_Work_File_Pnd_Wf_Closed_Stts.setValue(mit_Status_Cde);                                                                                                        //Natural: ASSIGN #WF-CLOSED-STTS := MIT.STATUS-CDE
        pnd_Work_File_Pnd_Wf_Rcvd_Dte_A.setValueEdited(mit_Tiaa_Rcvd_Dte,new ReportEditMask("YYYYMMDD"));                                                                 //Natural: MOVE EDITED MIT.TIAA-RCVD-DTE ( EM = YYYYMMDD ) TO #WF-RCVD-DTE-A
        pnd_Work_File_Pnd_Wf_Closed_Dte_Tme_A.setValueEdited(mit_Status_Updte_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                             //Natural: MOVE EDITED MIT.STATUS-UPDTE-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #WF-CLOSED-DTE-TME-A
        if (condition(mit_Crprte_Status_Ind.equals("9")))                                                                                                                 //Natural: IF MIT.CRPRTE-STATUS-IND = '9'
        {
            pnd_Work_File_Pnd_Wf_Closed_Stts.setValue(mit_Status_Cde);                                                                                                    //Natural: ASSIGN #WF-CLOSED-STTS := MIT.STATUS-CDE
            pnd_Work_File_Pnd_Wf_Closed_Dte_Tme_A.setValue(mit_Last_Chnge_Dte_Tme);                                                                                       //Natural: ASSIGN #WF-CLOSED-DTE-TME-A := MIT.LAST-CHNGE-DTE-TME
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Work_File_Pnd_Wf_Closed_Stts.setValue(" ");                                                                                                               //Natural: ASSIGN #WF-CLOSED-STTS := ' '
            pnd_Work_File_Pnd_Wf_Closed_Dte_Tme_A.setValue(" ");                                                                                                          //Natural: ASSIGN #WF-CLOSED-DTE-TME-A := ' '
        }                                                                                                                                                                 //Natural: END-IF
        //*  RLDT, LAST CHG DTE TME
        vw_mit_Hist.startDatabaseRead                                                                                                                                     //Natural: READ MIT-HIST BY RQST-ROUTING-KEY STARTING FROM #HISTORY-KEY
        (
        "READ02",
        new Wc[] { new Wc("RQST_ROUTING_KEY", ">=", pnd_History_Key, WcType.BY) },
        new Oc[] { new Oc("RQST_ROUTING_KEY", "ASC") }
        );
        READ02:
        while (condition(vw_mit_Hist.readNextRow("READ02")))
        {
            if (condition(mit_Hist_Rqst_Log_Dte_Tme.greater(mit_Rqst_Log_Dte_Tme) || mit_Hist_Pin_Nbr.notEquals(mit_Pin_Nbr)))                                            //Natural: IF MIT-HIST.RQST-LOG-DTE-TME > MIT.RQST-LOG-DTE-TME OR MIT-HIST.PIN-NBR NE MIT.PIN-NBR
            {
                if (condition(pnd_In_Auth_Queue.getBoolean() || pnd_Authorized.getBoolean()))                                                                             //Natural: IF #IN-AUTH-QUEUE OR #AUTHORIZED
                {
                    //*  SAVE PRIOR 2SIGHTV INFO
                                                                                                                                                                          //Natural: PERFORM WRITE-2SIGHT-RECORD
                    sub_Write_2sight_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_In_Auth_Queue.setValue(false);                                                                                                                    //Natural: ASSIGN #IN-AUTH-QUEUE := FALSE
                    pnd_Authorized.setValue(false);                                                                                                                       //Natural: ASSIGN #AUTHORIZED := FALSE
                    pnd_Assigned.setValue(false);                                                                                                                         //Natural: ASSIGN #ASSIGNED := FALSE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet230 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE MIT-HIST.ADMIN-STATUS-CDE;//Natural: VALUE '4325'
            if (condition((mit_Hist_Admin_Status_Cde.equals("4325"))))
            {
                decideConditionsMet230++;
                if (condition(mit_Hist_Unit_Cde.equals(pnd_Units_Pnd_T_Unit.getValue("*"))))                                                                              //Natural: IF MIT-HIST.UNIT-CDE = #T-UNIT ( * )
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_In_Auth_Queue.getBoolean()))                                                                                                            //Natural: IF #IN-AUTH-QUEUE
                {
                    //*  SAVE PRIOR 2SIGHTV INFO
                                                                                                                                                                          //Natural: PERFORM WRITE-2SIGHT-RECORD
                    sub_Write_2sight_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_In_Auth_Queue.setValue(false);                                                                                                                    //Natural: ASSIGN #IN-AUTH-QUEUE := FALSE
                    pnd_Authorized.setValue(false);                                                                                                                       //Natural: ASSIGN #AUTHORIZED := FALSE
                    pnd_Assigned.setValue(false);                                                                                                                         //Natural: ASSIGN #ASSIGNED := FALSE
                }                                                                                                                                                         //Natural: END-IF
                pnd_Work_File_Pnd_Wf_Oprtr.setValue(mit_Hist_Last_Chnge_Oprtr_Cde);                                                                                       //Natural: ASSIGN #WF-OPRTR := MIT-HIST.LAST-CHNGE-OPRTR-CDE
                pnd_Sv_Oprtr.setValue(mit_Hist_Last_Chnge_Oprtr_Cde);                                                                                                     //Natural: ASSIGN #SV-OPRTR := MIT-HIST.LAST-CHNGE-OPRTR-CDE
                pnd_Work_File_Pnd_Wf_Auth_Queue_Dte_Tme.setValue(mit_Hist_Last_Chnge_Dte_Tme);                                                                            //Natural: ASSIGN #WF-AUTH-QUEUE-DTE-TME := MIT-HIST.LAST-CHNGE-DTE-TME
                pnd_Sv_Auth_Queue_Dte_Tme.setValue(mit_Hist_Last_Chnge_Dte_Tme);                                                                                          //Natural: ASSIGN #SV-AUTH-QUEUE-DTE-TME := MIT-HIST.LAST-CHNGE-DTE-TME
                pnd_Work_File_Pnd_Wf_Wpid.setValue(mit_Hist_Work_Prcss_Id);                                                                                               //Natural: ASSIGN #WF-WPID := MIT-HIST.WORK-PRCSS-ID
                pnd_In_Auth_Queue.setValue(true);                                                                                                                         //Natural: ASSIGN #IN-AUTH-QUEUE := TRUE
            }                                                                                                                                                             //Natural: VALUE '2112'
            else if (condition((mit_Hist_Admin_Status_Cde.equals("2112"))))
            {
                decideConditionsMet230++;
                if (condition(pnd_Authorized.getBoolean()))                                                                                                               //Natural: IF #AUTHORIZED
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-2SIGHT-RECORD
                    sub_Write_2sight_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_In_Auth_Queue.setValue(false);                                                                                                                    //Natural: ASSIGN #IN-AUTH-QUEUE := FALSE
                    pnd_Authorized.setValue(false);                                                                                                                       //Natural: ASSIGN #AUTHORIZED := FALSE
                    pnd_Assigned.setValue(false);                                                                                                                         //Natural: ASSIGN #ASSIGNED := FALSE
                    pnd_Work_File_Pnd_Wf_Oprtr.setValue(pnd_Sv_Oprtr);                                                                                                    //Natural: ASSIGN #WF-OPRTR := #SV-OPRTR
                    pnd_Work_File_Pnd_Wf_Auth_Queue_Dte_Tme.setValue(pnd_Sv_Auth_Queue_Dte_Tme);                                                                          //Natural: ASSIGN #WF-AUTH-QUEUE-DTE-TME := #SV-AUTH-QUEUE-DTE-TME
                }                                                                                                                                                         //Natural: END-IF
                pnd_Work_File_Pnd_Wf_2sight_Oprtr.setValue(mit_Hist_Empl_Oprtr_Cde);                                                                                      //Natural: ASSIGN #WF-2SIGHT-OPRTR := MIT-HIST.EMPL-OPRTR-CDE
                pnd_Work_File_Pnd_Wf_2sight_Dte_Tme.setValue(mit_Hist_Last_Chnge_Dte_Tme);                                                                                //Natural: ASSIGN #WF-2SIGHT-DTE-TME := MIT-HIST.LAST-CHNGE-DTE-TME
                pnd_Authorized.setValue(true);                                                                                                                            //Natural: ASSIGN #AUTHORIZED := TRUE
            }                                                                                                                                                             //Natural: VALUE '2500'
            else if (condition((mit_Hist_Admin_Status_Cde.equals("2500"))))
            {
                decideConditionsMet230++;
                pnd_Work_File_Pnd_Wf_Return_Oprtr.setValue(mit_Hist_Empl_Oprtr_Cde);                                                                                      //Natural: ASSIGN #WF-RETURN-OPRTR := MIT-HIST.EMPL-OPRTR-CDE
                pnd_Work_File_Pnd_Wf_Return_Dte_Tme.setValue(mit_Hist_Last_Chnge_Dte_Tme);                                                                                //Natural: ASSIGN #WF-RETURN-DTE-TME := MIT-HIST.LAST-CHNGE-DTE-TME
            }                                                                                                                                                             //Natural: VALUE '2000':'2999'
            else if (condition((mit_Hist_Admin_Status_Cde.equals("2000' : '2999"))))
            {
                decideConditionsMet230++;
                if (condition(pnd_Authorized.getBoolean()))                                                                                                               //Natural: IF #AUTHORIZED
                {
                    if (condition(! (pnd_Assigned.getBoolean())))                                                                                                         //Natural: IF NOT #ASSIGNED
                    {
                        pnd_Sv_Oprtr.setValue(mit_Hist_Empl_Oprtr_Cde);                                                                                                   //Natural: ASSIGN #SV-OPRTR := MIT-HIST.EMPL-OPRTR-CDE
                        pnd_Assigned.setValue(true);                                                                                                                      //Natural: ASSIGN #ASSIGNED := TRUE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Sv_Oprtr.setValue(mit_Hist_Last_Chnge_Oprtr_Cde);                                                                                             //Natural: ASSIGN #SV-OPRTR := MIT-HIST.LAST-CHNGE-OPRTR-CDE
                        pnd_Sv_Auth_Queue_Dte_Tme.setValue(mit_Hist_Last_Chnge_Dte_Tme);                                                                                  //Natural: ASSIGN #SV-AUTH-QUEUE-DTE-TME := MIT-HIST.LAST-CHNGE-DTE-TME
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  DETERMINE-REQUEST-HISTORY
    }
    private void sub_Write_2sight_Record() throws Exception                                                                                                               //Natural: WRITE-2SIGHT-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  IGNORE REQUESTS PUT IN AUTHORIZE QUEUE BUT STILL IN OPEN STATUS
        if (condition(pnd_Work_File_Pnd_Wf_2sight_Oprtr.equals(" ") && pnd_Work_File_Pnd_Wf_Closed_Dte_Tme_A.equals(" ")))                                                //Natural: IF #WF-2SIGHT-OPRTR = ' ' AND #WF-CLOSED-DTE-TME-A = ' '
        {
            pnd_Work_File.reset();                                                                                                                                        //Natural: RESET #WORK-FILE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Work_File_Pnd_Wf_Pin.equals(getZero()) || pnd_Work_File_Pnd_Wf_Auth_Queue_Dte.equals(getZero()) || pnd_Work_File_Pnd_Wf_Wpid.equals(" ")))      //Natural: IF #WF-PIN = 0 OR #WF-AUTH-QUEUE-DTE = 0 OR #WF-WPID = ' '
        {
            pnd_Work_File.reset();                                                                                                                                        //Natural: RESET #WORK-FILE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        getReports().display(0, "pin",                                                                                                                                    //Natural: DISPLAY 'pin' #WF-PIN 'RCVD' #WF-RCVD-DTE 'oprtr' #WF-OPRTR 'queue' #WF-AUTH-QUEUE-DTE 'authzr' #WF-2SIGHT-OPRTR 'autdte' #WF-2SIGHT-DTE 'return' #WF-RETURN-OPRTR
        		pnd_Work_File_Pnd_Wf_Pin,"RCVD",
        		pnd_Work_File_Pnd_Wf_Rcvd_Dte,"oprtr",
        		pnd_Work_File_Pnd_Wf_Oprtr,"queue",
        		pnd_Work_File_Pnd_Wf_Auth_Queue_Dte,"authzr",
        		pnd_Work_File_Pnd_Wf_2sight_Oprtr,"autdte",
        		pnd_Work_File_Pnd_Wf_2sight_Dte,"return",
        		pnd_Work_File_Pnd_Wf_Return_Oprtr);
        if (Global.isEscape()) return;
        //*  TO SORT TO END OF REPORT
        if (condition(pnd_Work_File_Pnd_Wf_2sight_Oprtr.equals(" ")))                                                                                                     //Natural: IF #WF-2SIGHT-OPRTR = ' '
        {
            pnd_Work_File_Pnd_Wf_2sight_Oprtr.setValue("99999999");                                                                                                       //Natural: ASSIGN #WF-2SIGHT-OPRTR := '99999999'
        }                                                                                                                                                                 //Natural: END-IF
        getWorkFiles().write(1, false, pnd_Work_File);                                                                                                                    //Natural: WRITE WORK FILE 1 #WORK-FILE
        pnd_Work_File_Pnd_Wf_Return_Oprtr.reset();                                                                                                                        //Natural: RESET #WF-RETURN-OPRTR #WF-RETURN-UNIT #WF-RETURN-STTS #WF-2SIGHT-DTE
        pnd_Work_File_Pnd_Wf_Return_Unit.reset();
        pnd_Work_File_Pnd_Wf_Return_Stts.reset();
        pnd_Work_File_Pnd_Wf_2sight_Dte.reset();
        pnd_Write_Count.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #WRITE-COUNT
        //*  WRITE-2SIGHT-RECORD
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=55 LS=132");

        getReports().setDisplayColumns(0, "pin",
        		pnd_Work_File_Pnd_Wf_Pin,"RCVD",
        		pnd_Work_File_Pnd_Wf_Rcvd_Dte,"oprtr",
        		pnd_Work_File_Pnd_Wf_Oprtr,"queue",
        		pnd_Work_File_Pnd_Wf_Auth_Queue_Dte,"authzr",
        		pnd_Work_File_Pnd_Wf_2sight_Oprtr,"autdte",
        		pnd_Work_File_Pnd_Wf_2sight_Dte,"return",
        		pnd_Work_File_Pnd_Wf_Return_Oprtr);
    }
}
