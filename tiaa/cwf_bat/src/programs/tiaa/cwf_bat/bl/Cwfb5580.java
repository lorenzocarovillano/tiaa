/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:39:26 PM
**        * FROM NATURAL PROGRAM : Cwfb5580
************************************************************
**        * FILE NAME            : Cwfb5580.java
**        * CLASS NAME           : Cwfb5580
**        * INSTANCE NAME        : Cwfb5580
************************************************************
************************************************************************
* PROGRAM  : CWFB5580
* SYSTEM   : CRPCWF
* FUNCTION : PRINT COUNTS PER WPID PER MONTH LOG BY CRC IN THE ORDER OF
*          :   COUNTS (HIGHEST TO LOWEST).
*          |
*          |    INPUT: YYYYMMNN = YYYYMM - STARTING MONTH
*          |                      NN     - NUMBER OF MONTHS TO INCLUDE
*          |
*          |    ADDED MTH TOTALS
* 07/26/96 | OB  IF NOT INITIATED FROM P12..... ASSUME MONTH
* 04/16/02 | IE  DGTZE INDICATOR ADDED TO THE REPORT - SCAN FOR EPPEL
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb5580 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_master;
    private DbsField master_Orgnl_Unit_Cde;
    private DbsField master_Work_Prcss_Id;
    private DbsField master_Rqst_Id;

    private DbsGroup master__R_Field_1;
    private DbsField master_Rqst_Work_Prcss_Id;
    private DbsField master_Rqst_Tiaa_Rcvd_Dte;

    private DbsGroup master__R_Field_2;
    private DbsField master_Tiaa_Yyyymm;

    private DataAccessProgramView vw_wpid_Tbl;
    private DbsField wpid_Tbl_Work_Prcss_Id;
    private DbsField wpid_Tbl_Work_Prcss_Long_Nme;
    private DbsField wpid_Tbl_Dgtze_Ind;

    private DbsGroup pnd_Vars;
    private DbsField pnd_Vars_Pnd_Ctr;
    private DbsField pnd_Vars_Pnd_Ctr_A1;
    private DbsField pnd_Vars_Pnd_Crc_Cnt;
    private DbsField pnd_Vars_Pnd_Crc_Cnt_1;
    private DbsField pnd_Vars_Pnd_Crc_Mth_Tot;
    private DbsField pnd_Vars_Pnd_Dte;
    private DbsField pnd_Vars_Pnd_Escape_Read;
    private DbsField pnd_Vars_Pnd_Non_Crc_Cnt;
    private DbsField pnd_Vars_Pnd_Non_Crc_Cnt_1;
    private DbsField pnd_Vars_Pnd_Non_Crc_Mth_Tot;
    private DbsField pnd_Vars_Pnd_Prev_Yyyymm;
    private DbsField pnd_Vars_Pnd_Rept_Yyyymm;
    private DbsField pnd_Vars_Pnd_Wpid_Ctr;
    private DbsField pnd_Vars_Pnd_Yyyymm;
    private DbsField pnd_Vars_Pnd_Dgtze_Ind;
    private DbsField pnd_Ri_Key;

    private DbsGroup pnd_Ri_Key__R_Field_3;
    private DbsField pnd_Ri_Key_Pnd_Rik_Wpid;
    private DbsField pnd_Ri_Key_Pnd_Rik_Yyyymm;
    private DbsField pnd_Wpid_Uniq_Key;

    private DbsGroup pnd_Wpid_Uniq_Key__R_Field_4;
    private DbsField pnd_Wpid_Uniq_Key_Pnd_Wpid;
    private DbsField pnd_Wpid_Uniq_Key_Pnd_Actve_Ind;
    private DbsField pnd_Input_Parm;

    private DbsGroup pnd_Input_Parm__R_Field_5;
    private DbsField pnd_Input_Parm_Pnd_Input_Yyyymm;
    private DbsField pnd_Input_Parm_Pnd_Input_Nn;

    private DbsGroup pnd_Input_Parm__R_Field_6;
    private DbsField pnd_Input_Parm_Pnd_Input_Nn_A;
    private DbsField pnd_Ending_Mth;
    private DbsField pnd_End_Yyyymm;

    private DbsGroup pnd_End_Yyyymm__R_Field_7;
    private DbsField pnd_End_Yyyymm_Pnd_End_Yyyymm_N;

    private DbsRecord setTimeRecord;
    private DbsField st;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_master = new DataAccessProgramView(new NameInfo("vw_master", "MASTER"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        master_Orgnl_Unit_Cde = vw_master.getRecord().newFieldInGroup("master_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ORGNL_UNIT_CDE");
        master_Orgnl_Unit_Cde.setDdmHeader("LOG/UNIT");
        master_Work_Prcss_Id = vw_master.getRecord().newFieldInGroup("master_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        master_Work_Prcss_Id.setDdmHeader("WORK/ID");
        master_Rqst_Id = vw_master.getRecord().newFieldInGroup("master_Rqst_Id", "RQST-ID", FieldType.STRING, 28, RepeatingFieldStrategy.None, "RQST_ID");
        master_Rqst_Id.setDdmHeader("REQUEST ID");

        master__R_Field_1 = vw_master.getRecord().newGroupInGroup("master__R_Field_1", "REDEFINE", master_Rqst_Id);
        master_Rqst_Work_Prcss_Id = master__R_Field_1.newFieldInGroup("master_Rqst_Work_Prcss_Id", "RQST-WORK-PRCSS-ID", FieldType.STRING, 6);
        master_Rqst_Tiaa_Rcvd_Dte = master__R_Field_1.newFieldInGroup("master_Rqst_Tiaa_Rcvd_Dte", "RQST-TIAA-RCVD-DTE", FieldType.STRING, 8);

        master__R_Field_2 = master__R_Field_1.newGroupInGroup("master__R_Field_2", "REDEFINE", master_Rqst_Tiaa_Rcvd_Dte);
        master_Tiaa_Yyyymm = master__R_Field_2.newFieldInGroup("master_Tiaa_Yyyymm", "TIAA-YYYYMM", FieldType.STRING, 6);
        registerRecord(vw_master);

        vw_wpid_Tbl = new DataAccessProgramView(new NameInfo("vw_wpid_Tbl", "WPID-TBL"), "CWF_WP_WORK_PRCSS_ID", "CWF_PROFILE");
        wpid_Tbl_Work_Prcss_Id = vw_wpid_Tbl.getRecord().newFieldInGroup("wpid_Tbl_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        wpid_Tbl_Work_Prcss_Id.setDdmHeader("WORK PRCSS ID");
        wpid_Tbl_Work_Prcss_Long_Nme = vw_wpid_Tbl.getRecord().newFieldInGroup("wpid_Tbl_Work_Prcss_Long_Nme", "WORK-PRCSS-LONG-NME", FieldType.STRING, 
            45, RepeatingFieldStrategy.None, "WORK_PRCSS_LONG_NME");
        wpid_Tbl_Work_Prcss_Long_Nme.setDdmHeader("NAME");
        wpid_Tbl_Dgtze_Ind = vw_wpid_Tbl.getRecord().newFieldInGroup("wpid_Tbl_Dgtze_Ind", "DGTZE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "DGTZE_IND");
        wpid_Tbl_Dgtze_Ind.setDdmHeader("DIGITIZE/INDICATOR");
        registerRecord(vw_wpid_Tbl);

        pnd_Vars = localVariables.newGroupInRecord("pnd_Vars", "#VARS");
        pnd_Vars_Pnd_Ctr = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Ctr", "#CTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Vars_Pnd_Ctr_A1 = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Ctr_A1", "#CTR-A1", FieldType.PACKED_DECIMAL, 8);
        pnd_Vars_Pnd_Crc_Cnt = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Crc_Cnt", "#CRC-CNT", FieldType.PACKED_DECIMAL, 8);
        pnd_Vars_Pnd_Crc_Cnt_1 = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Crc_Cnt_1", "#CRC-CNT-1", FieldType.PACKED_DECIMAL, 8);
        pnd_Vars_Pnd_Crc_Mth_Tot = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Crc_Mth_Tot", "#CRC-MTH-TOT", FieldType.PACKED_DECIMAL, 8);
        pnd_Vars_Pnd_Dte = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Dte", "#DTE", FieldType.DATE);
        pnd_Vars_Pnd_Escape_Read = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Escape_Read", "#ESCAPE-READ", FieldType.BOOLEAN, 1);
        pnd_Vars_Pnd_Non_Crc_Cnt = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Non_Crc_Cnt", "#NON-CRC-CNT", FieldType.PACKED_DECIMAL, 8);
        pnd_Vars_Pnd_Non_Crc_Cnt_1 = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Non_Crc_Cnt_1", "#NON-CRC-CNT-1", FieldType.PACKED_DECIMAL, 8);
        pnd_Vars_Pnd_Non_Crc_Mth_Tot = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Non_Crc_Mth_Tot", "#NON-CRC-MTH-TOT", FieldType.PACKED_DECIMAL, 8);
        pnd_Vars_Pnd_Prev_Yyyymm = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Prev_Yyyymm", "#PREV-YYYYMM", FieldType.STRING, 6);
        pnd_Vars_Pnd_Rept_Yyyymm = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Rept_Yyyymm", "#REPT-YYYYMM", FieldType.STRING, 6);
        pnd_Vars_Pnd_Wpid_Ctr = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Wpid_Ctr", "#WPID-CTR", FieldType.NUMERIC, 4);
        pnd_Vars_Pnd_Yyyymm = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Yyyymm", "#YYYYMM", FieldType.STRING, 6);
        pnd_Vars_Pnd_Dgtze_Ind = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Dgtze_Ind", "#DGTZE-IND", FieldType.STRING, 1);
        pnd_Ri_Key = localVariables.newFieldInRecord("pnd_Ri_Key", "#RI-KEY", FieldType.STRING, 25);

        pnd_Ri_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Ri_Key__R_Field_3", "REDEFINE", pnd_Ri_Key);
        pnd_Ri_Key_Pnd_Rik_Wpid = pnd_Ri_Key__R_Field_3.newFieldInGroup("pnd_Ri_Key_Pnd_Rik_Wpid", "#RIK-WPID", FieldType.STRING, 6);
        pnd_Ri_Key_Pnd_Rik_Yyyymm = pnd_Ri_Key__R_Field_3.newFieldInGroup("pnd_Ri_Key_Pnd_Rik_Yyyymm", "#RIK-YYYYMM", FieldType.STRING, 6);
        pnd_Wpid_Uniq_Key = localVariables.newFieldInRecord("pnd_Wpid_Uniq_Key", "#WPID-UNIQ-KEY", FieldType.STRING, 7);

        pnd_Wpid_Uniq_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Wpid_Uniq_Key__R_Field_4", "REDEFINE", pnd_Wpid_Uniq_Key);
        pnd_Wpid_Uniq_Key_Pnd_Wpid = pnd_Wpid_Uniq_Key__R_Field_4.newFieldInGroup("pnd_Wpid_Uniq_Key_Pnd_Wpid", "#WPID", FieldType.STRING, 6);
        pnd_Wpid_Uniq_Key_Pnd_Actve_Ind = pnd_Wpid_Uniq_Key__R_Field_4.newFieldInGroup("pnd_Wpid_Uniq_Key_Pnd_Actve_Ind", "#ACTVE-IND", FieldType.STRING, 
            1);
        pnd_Input_Parm = localVariables.newFieldInRecord("pnd_Input_Parm", "#INPUT-PARM", FieldType.STRING, 8);

        pnd_Input_Parm__R_Field_5 = localVariables.newGroupInRecord("pnd_Input_Parm__R_Field_5", "REDEFINE", pnd_Input_Parm);
        pnd_Input_Parm_Pnd_Input_Yyyymm = pnd_Input_Parm__R_Field_5.newFieldInGroup("pnd_Input_Parm_Pnd_Input_Yyyymm", "#INPUT-YYYYMM", FieldType.STRING, 
            6);
        pnd_Input_Parm_Pnd_Input_Nn = pnd_Input_Parm__R_Field_5.newFieldInGroup("pnd_Input_Parm_Pnd_Input_Nn", "#INPUT-NN", FieldType.NUMERIC, 2);

        pnd_Input_Parm__R_Field_6 = pnd_Input_Parm__R_Field_5.newGroupInGroup("pnd_Input_Parm__R_Field_6", "REDEFINE", pnd_Input_Parm_Pnd_Input_Nn);
        pnd_Input_Parm_Pnd_Input_Nn_A = pnd_Input_Parm__R_Field_6.newFieldInGroup("pnd_Input_Parm_Pnd_Input_Nn_A", "#INPUT-NN-A", FieldType.STRING, 2);
        pnd_Ending_Mth = localVariables.newFieldInRecord("pnd_Ending_Mth", "#ENDING-MTH", FieldType.STRING, 6);
        pnd_End_Yyyymm = localVariables.newFieldInRecord("pnd_End_Yyyymm", "#END-YYYYMM", FieldType.STRING, 6);

        pnd_End_Yyyymm__R_Field_7 = localVariables.newGroupInRecord("pnd_End_Yyyymm__R_Field_7", "REDEFINE", pnd_End_Yyyymm);
        pnd_End_Yyyymm_Pnd_End_Yyyymm_N = pnd_End_Yyyymm__R_Field_7.newFieldInGroup("pnd_End_Yyyymm_Pnd_End_Yyyymm_N", "#END-YYYYMM-N", FieldType.NUMERIC, 
            6);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        setTimeRecord = new DbsRecord();
        st = setTimeRecord.newFieldInRecord("ST", "ST", FieldType.NUMERIC, 7);
        registerRecord(setTimeRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_master.reset();
        vw_wpid_Tbl.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Cwfb5580() throws Exception
    {
        super("Cwfb5580");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Cwfb5580|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        while(true)
        {
            try
            {
                st = Global.getTIMN();                                                                                                                                    //Natural: SET TIME
                //*    INITIALIZATION
                getReports().getPageNumberDbs(1).reset();                                                                                                                 //Natural: FORMAT ( 1 ) PS = 58 LS = 132;//Natural: RESET *PAGE-NUMBER ( 1 )
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape())){return;}
                //*  FROM MONHTLY JOB (P2000CWM)
                if (condition(DbsUtil.maskMatches(Global.getINIT_PROGRAM(),"'P'......'M'")))                                                                              //Natural: IF *INIT-PROGRAM EQ MASK ( 'P'......'M' )
                {
                    pnd_Input_Parm.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                       //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO #INPUT-PARM
                    if (condition(pnd_Input_Parm_Pnd_Input_Nn.greaterOrEqual(23)))                                                                                        //Natural: IF #INPUT-NN GE 23
                    {
                        //*  PROCESS CURRENT MONTH
                        pnd_Input_Parm_Pnd_Input_Nn_A.setValue("01");                                                                                                     //Natural: MOVE '01' TO #INPUT-NN-A
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Vars_Pnd_Dte.setValue(Global.getDATX());                                                                                                      //Natural: MOVE *DATX TO #DTE
                        //*  GET PREVIOUS MONTH
                        pnd_Vars_Pnd_Dte.nsubtract(23);                                                                                                                   //Natural: SUBTRACT 23 FROM #DTE
                        pnd_Input_Parm.setValueEdited(pnd_Vars_Pnd_Dte,new ReportEditMask("YYYYMMDD"));                                                                   //Natural: MOVE EDITED #DTE ( EM = YYYYMMDD ) TO #INPUT-PARM
                        //*  PROCESS PREVIOUS MONTH
                        pnd_Input_Parm_Pnd_Input_Nn_A.setValue("01");                                                                                                     //Natural: MOVE '01' TO #INPUT-NN-A
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition(INPUT_1))
                {
                    DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Parm);                                                                                   //Natural: INPUT #INPUT-PARM
                }                                                                                                                                                         //Natural: END-IF
                //*    VALIDATE PARAMETER
                if (condition(! (DbsUtil.maskMatches(pnd_Input_Parm,"NNNNNN..")) || ! (DbsUtil.maskMatches(pnd_Input_Parm,"1970:2100....")) || ! (DbsUtil.maskMatches(pnd_Input_Parm, //Natural: IF #INPUT-PARM NE MASK ( NNNNNN.. ) OR #INPUT-PARM NE MASK ( 1970:2100.... ) OR #INPUT-PARM NE MASK ( ....01:12.. )
                    "....01:12.."))))
                {
                    getReports().write(1, "***********************");                                                                                                     //Natural: WRITE ( 1 ) '***********************'
                    if (Global.isEscape()) return;
                    getReports().write(1, "  Invalid starting month",pnd_Input_Parm_Pnd_Input_Yyyymm);                                                                    //Natural: WRITE ( 1 ) '  Invalid starting month' #INPUT-YYYYMM
                    if (Global.isEscape()) return;
                    getReports().write(1, "***********************");                                                                                                     //Natural: WRITE ( 1 ) '***********************'
                    if (Global.isEscape()) return;
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(! (DbsUtil.maskMatches(pnd_Input_Parm,"......NN")) || ! (DbsUtil.maskMatches(pnd_Input_Parm,"......01:12"))))                               //Natural: IF #INPUT-PARM NE MASK ( ......NN ) OR #INPUT-PARM NE MASK ( ......01:12 )
                {
                    getReports().write(1, "***********************");                                                                                                     //Natural: WRITE ( 1 ) '***********************'
                    if (Global.isEscape()) return;
                    getReports().write(1, "  Invalid number of months",pnd_Input_Parm_Pnd_Input_Nn_A);                                                                    //Natural: WRITE ( 1 ) '  Invalid number of months' #INPUT-NN-A
                    if (Global.isEscape()) return;
                    getReports().write(1, "***********************");                                                                                                     //Natural: WRITE ( 1 ) '***********************'
                    if (Global.isEscape()) return;
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                pnd_End_Yyyymm.setValue(pnd_Input_Parm_Pnd_Input_Yyyymm);                                                                                                 //Natural: MOVE #INPUT-YYYYMM TO #END-YYYYMM
                pnd_End_Yyyymm_Pnd_End_Yyyymm_N.nadd(pnd_Input_Parm_Pnd_Input_Nn);                                                                                        //Natural: ADD #INPUT-NN TO #END-YYYYMM-N
                pnd_End_Yyyymm_Pnd_End_Yyyymm_N.nsubtract(1);                                                                                                             //Natural: SUBTRACT 1 FROM #END-YYYYMM-N
                pnd_Ending_Mth.setValue(pnd_End_Yyyymm);                                                                                                                  //Natural: MOVE #END-YYYYMM TO #ENDING-MTH
                pnd_End_Yyyymm_Pnd_End_Yyyymm_N.nadd(1);                                                                                                                  //Natural: ADD 1 TO #END-YYYYMM-N
                vw_wpid_Tbl.startDatabaseRead                                                                                                                             //Natural: READ WPID-TBL BY WPID-UNIQ-KEY
                (
                "RW",
                new Oc[] { new Oc("WPID_UNIQ_KEY", "ASC") }
                );
                RW:
                while (condition(vw_wpid_Tbl.readNextRow("RW")))
                {
                    pnd_Ri_Key_Pnd_Rik_Wpid.setValue(wpid_Tbl_Work_Prcss_Id);                                                                                             //Natural: MOVE WPID-TBL.WORK-PRCSS-ID TO #RIK-WPID
                    //*  EPPEL
                    pnd_Vars_Pnd_Dgtze_Ind.setValue(wpid_Tbl_Dgtze_Ind);                                                                                                  //Natural: MOVE WPID-TBL.DGTZE-IND TO #DGTZE-IND
                    pnd_Ri_Key_Pnd_Rik_Yyyymm.setValue(pnd_Input_Parm_Pnd_Input_Yyyymm);                                                                                  //Natural: MOVE #INPUT-YYYYMM TO #RIK-YYYYMM
                    pnd_Vars_Pnd_Escape_Read.setValue(false);                                                                                                             //Natural: MOVE FALSE TO #ESCAPE-READ
                    pnd_Vars_Pnd_Crc_Cnt_1.reset();                                                                                                                       //Natural: RESET #CRC-CNT-1 #NON-CRC-CNT-1 #PREV-YYYYMM
                    pnd_Vars_Pnd_Non_Crc_Cnt_1.reset();
                    pnd_Vars_Pnd_Prev_Yyyymm.reset();
                    vw_master.startDatabaseRead                                                                                                                           //Natural: READ MASTER BY RQST-ID-KEY FROM #RI-KEY
                    (
                    "RM",
                    new Wc[] { new Wc("RQST_ID_KEY", ">=", pnd_Ri_Key, WcType.BY) },
                    new Oc[] { new Oc("RQST_ID_KEY", "ASC") }
                    );
                    RM:
                    while (condition(vw_master.readNextRow("RM")))
                    {
                        pnd_Vars_Pnd_Ctr.nadd(1);                                                                                                                         //Natural: ADD 1 TO #CTR
                        if (condition(master_Rqst_Work_Prcss_Id.notEquals(pnd_Ri_Key_Pnd_Rik_Wpid) || master_Rqst_Tiaa_Rcvd_Dte.greater(pnd_End_Yyyymm)))                 //Natural: IF MASTER.RQST-WORK-PRCSS-ID NE #RIK-WPID OR MASTER.RQST-TIAA-RCVD-DTE GT #END-YYYYMM
                        {
                            if (condition(pnd_Vars_Pnd_Escape_Read.getBoolean() || vw_master.getAstCOUNTER().equals(1)))                                                  //Natural: IF #ESCAPE-READ OR *COUNTER ( RM. ) EQ 1
                            {
                                if (true) break RM;                                                                                                                       //Natural: ESCAPE BOTTOM ( RM. ) IMMEDIATE
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Vars_Pnd_Escape_Read.setValue(true);                                                                                                  //Natural: MOVE TRUE TO #ESCAPE-READ
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(master_Tiaa_Yyyymm.equals(pnd_Vars_Pnd_Prev_Yyyymm)))                                                                               //Natural: IF MASTER.TIAA-YYYYMM EQ #PREV-YYYYMM
                        {
                            if (condition(master_Orgnl_Unit_Cde.equals("CRC")))                                                                                           //Natural: IF MASTER.ORGNL-UNIT-CDE EQ 'CRC'
                            {
                                pnd_Vars_Pnd_Crc_Cnt_1.nadd(1);                                                                                                           //Natural: ADD 1 TO #CRC-CNT-1
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Vars_Pnd_Non_Crc_Cnt_1.nadd(1);                                                                                                       //Natural: ADD 1 TO #NON-CRC-CNT-1
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Vars_Pnd_Crc_Cnt.setValue(pnd_Vars_Pnd_Crc_Cnt_1);                                                                                        //Natural: MOVE #CRC-CNT-1 TO #CRC-CNT
                            pnd_Vars_Pnd_Non_Crc_Cnt.setValue(pnd_Vars_Pnd_Non_Crc_Cnt_1);                                                                                //Natural: MOVE #NON-CRC-CNT-1 TO #NON-CRC-CNT
                            pnd_Vars_Pnd_Yyyymm.setValue(pnd_Vars_Pnd_Prev_Yyyymm);                                                                                       //Natural: MOVE #PREV-YYYYMM TO #YYYYMM
                            pnd_Vars_Pnd_Prev_Yyyymm.setValue(master_Tiaa_Yyyymm);                                                                                        //Natural: MOVE MASTER.TIAA-YYYYMM TO #PREV-YYYYMM
                            if (condition(master_Orgnl_Unit_Cde.equals("CRC")))                                                                                           //Natural: IF MASTER.ORGNL-UNIT-CDE EQ 'CRC'
                            {
                                pnd_Vars_Pnd_Crc_Cnt_1.setValue(1);                                                                                                       //Natural: MOVE 1 TO #CRC-CNT-1
                                pnd_Vars_Pnd_Non_Crc_Cnt_1.setValue(0);                                                                                                   //Natural: MOVE 0 TO #NON-CRC-CNT-1
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Vars_Pnd_Non_Crc_Cnt_1.setValue(1);                                                                                                   //Natural: MOVE 1 TO #NON-CRC-CNT-1
                                pnd_Vars_Pnd_Crc_Cnt_1.setValue(0);                                                                                                       //Natural: MOVE 0 TO #CRC-CNT-1
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(pnd_Vars_Pnd_Crc_Cnt.equals(getZero()) && pnd_Vars_Pnd_Non_Crc_Cnt.equals(getZero())))                                          //Natural: IF #CRC-CNT EQ 0 AND #NON-CRC-CNT EQ 0
                            {
                                if (condition(true)) continue;                                                                                                            //Natural: ESCAPE TOP
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Vars_Pnd_Ctr_A1.nadd(1);                                                                                                                      //Natural: ADD 1 TO #CTR-A1
                        getSort().writeSortInData(pnd_Vars_Pnd_Yyyymm, pnd_Vars_Pnd_Crc_Cnt, wpid_Tbl_Work_Prcss_Id, pnd_Vars_Pnd_Non_Crc_Cnt, wpid_Tbl_Work_Prcss_Long_Nme,  //Natural: END-ALL
                            wpid_Tbl_Dgtze_Ind);
                    }                                                                                                                                                     //Natural: END-READ
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RW"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                //*  EPPEL
                getSort().sortData(pnd_Vars_Pnd_Yyyymm, pnd_Vars_Pnd_Crc_Cnt, "DESCENDING", wpid_Tbl_Work_Prcss_Id);                                                      //Natural: SORT BY #YYYYMM #CRC-CNT DESCENDING WPID-TBL.WORK-PRCSS-ID USING #NON-CRC-CNT WPID-TBL.WORK-PRCSS-LONG-NME WPID-TBL.DGTZE-IND
                SORT01:
                while (condition(getSort().readSortOutData(pnd_Vars_Pnd_Yyyymm, pnd_Vars_Pnd_Crc_Cnt, wpid_Tbl_Work_Prcss_Id, pnd_Vars_Pnd_Non_Crc_Cnt, 
                    wpid_Tbl_Work_Prcss_Long_Nme, wpid_Tbl_Dgtze_Ind)))
                {
                    if (condition(pnd_Vars_Pnd_Yyyymm.notEquals(pnd_Vars_Pnd_Rept_Yyyymm)))                                                                               //Natural: IF #YYYYMM NE #REPT-YYYYMM
                    {
                        if (condition(pnd_Vars_Pnd_Rept_Yyyymm.greater(" ")))                                                                                             //Natural: IF #REPT-YYYYMM GT ' '
                        {
                                                                                                                                                                          //Natural: PERFORM WRITE-MTH-TOT
                            sub_Write_Mth_Tot();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                        }                                                                                                                                                 //Natural: END-IF
                        getReports().write(1, NEWLINE,pnd_Vars_Pnd_Yyyymm, new ReportEditMask ("XXXX'-'XX"));                                                             //Natural: WRITE ( 1 ) / #YYYYMM ( EM = XXXX'-'XX )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Vars_Pnd_Rept_Yyyymm.setValue(pnd_Vars_Pnd_Yyyymm);                                                                                           //Natural: MOVE #YYYYMM TO #REPT-YYYYMM
                        pnd_Vars_Pnd_Wpid_Ctr.reset();                                                                                                                    //Natural: RESET #WPID-CTR #CRC-MTH-TOT #NON-CRC-MTH-TOT
                        pnd_Vars_Pnd_Crc_Mth_Tot.reset();
                        pnd_Vars_Pnd_Non_Crc_Mth_Tot.reset();
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Vars_Pnd_Wpid_Ctr.nadd(1);                                                                                                                        //Natural: ADD 1 TO #WPID-CTR
                    //* EPPEL
                    getReports().write(1, new TabSetting(5),pnd_Vars_Pnd_Wpid_Ctr,wpid_Tbl_Work_Prcss_Id,wpid_Tbl_Work_Prcss_Long_Nme,pnd_Vars_Pnd_Crc_Cnt,pnd_Vars_Pnd_Non_Crc_Cnt,new  //Natural: WRITE ( 1 ) 5T #WPID-CTR WPID-TBL.WORK-PRCSS-ID WPID-TBL.WORK-PRCSS-LONG-NME #CRC-CNT #NON-CRC-CNT 95T WPID-TBL.DGTZE-IND
                        TabSetting(95),wpid_Tbl_Dgtze_Ind);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Vars_Pnd_Crc_Mth_Tot.nadd(pnd_Vars_Pnd_Crc_Cnt);                                                                                                  //Natural: ADD #CRC-CNT TO #CRC-MTH-TOT
                    pnd_Vars_Pnd_Non_Crc_Mth_Tot.nadd(pnd_Vars_Pnd_Non_Crc_Cnt);                                                                                          //Natural: ADD #NON-CRC-CNT TO #NON-CRC-MTH-TOT
                }                                                                                                                                                         //Natural: END-SORT
                endSort();
                                                                                                                                                                          //Natural: PERFORM WRITE-MTH-TOT
                sub_Write_Mth_Tot();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( 1 )
                getReports().write(1, NEWLINE,NEWLINE,"Time Elapsed    :",st, new ReportEditMask ("99':'99':'99'.'9"),NEWLINE,"Records Sorted:",pnd_Vars_Pnd_Ctr_A1,      //Natural: WRITE ( 1 ) // 'Time Elapsed    :' *TIMD ( ST. ) ( EM = 99':'99':'99'.'9 ) / 'Records Sorted:' #CTR-A1 / 'Records Read  :' #CTR
                    NEWLINE,"Records Read  :",pnd_Vars_Pnd_Ctr);
                if (Global.isEscape()) return;
                getReports().getPageNumberDbs(1).reset();                                                                                                                 //Natural: RESET *PAGE-NUMBER ( 1 )
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape())){return;}
                //* ***************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-MTH-TOT
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Write_Mth_Tot() throws Exception                                                                                                                     //Natural: WRITE-MTH-TOT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************
        getReports().write(1, new TabSetting(63),"--------- ---------",NEWLINE,new TabSetting(35),"Total for",pnd_Vars_Pnd_Rept_Yyyymm, new ReportEditMask                //Natural: WRITE ( 1 ) 63T '--------- ---------' / 35T 'Total for' #REPT-YYYYMM ( EM = XXXX'-'XX ) 63T #CRC-MTH-TOT #NON-CRC-MTH-TOT
            ("XXXX'-'XX"),new TabSetting(63),pnd_Vars_Pnd_Crc_Mth_Tot,pnd_Vars_Pnd_Non_Crc_Mth_Tot);
        if (Global.isEscape()) return;
        //*  WRITE-MTH-TOT
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* EPPEL
                    getReports().write(1, Global.getPROGRAM(),new TabSetting(13),"WPID Counts per Month Logged from",pnd_Input_Parm_Pnd_Input_Yyyymm,"thru",pnd_Ending_Mth,new  //Natural: WRITE ( 1 ) *PROGRAM 13T 'WPID Counts per Month Logged from' #INPUT-YYYYMM 'thru' #ENDING-MTH 10X 'Page:' *PAGE-NUMBER ( 1 ) // '  Date' / 11T 'WPID     WPID Description' 69T 'By CRC    By Non-CRC' 91T 'DGTZE Ind'
                        ColumnSpacing(10),"Page:",getReports().getPageNumberDbs(1),NEWLINE,NEWLINE,"  Date",NEWLINE,new TabSetting(11),"WPID     WPID Description",new 
                        TabSetting(69),"By CRC    By Non-CRC",new TabSetting(91),"DGTZE Ind");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=58 LS=132");
    }
}
