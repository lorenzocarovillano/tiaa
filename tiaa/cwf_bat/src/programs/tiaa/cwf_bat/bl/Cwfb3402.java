/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:29:17 PM
**        * FROM NATURAL PROGRAM : Cwfb3402
************************************************************
**        * FILE NAME            : Cwfb3402.java
**        * CLASS NAME           : Cwfb3402
**        * INSTANCE NAME        : Cwfb3402
************************************************************
**SAG GENERATOR: SHELL-TIAA                       VERSION: 3.2.2
**SAG TITLE: REPORT 1
**SAG SYSTEM: CRPCWF
************************************************************************
* PROGRAM  : CWFB3402
* SYSTEM   : CRPCWF
* TITLE    : REPORT 1
* GENERATED: AUG 11,93 AT 10:46 AM
* FUNCTION : REPORT OF WORK REQUEST LOGGED AND INDEXED
*          | BATCH SPECIAL - CRC WEEKLY REPORT OF LOGGED + RETURNING DOC
*          |
*          |
*          |
*          |
*          |
* MOD DATE | MOD
* MM DD YY | BY  DESCRIPTION OF CHANGES
* -------- + --  -------------------------------------------------------
* 01/02/02 | GY  MODIFIED REPORT TO PRODUCE THREE REPORTS, ONE FOR
*          |     EACH SITE CODE AND SHOWING TOTALS FOR ALL THREE SITES.
*          |     THREE WORK FILES ARE USED IN ORDER TO SHOW TOTALS AND
*          |     PRINTING ONLY OCCURS WHEN ALL THREE ARE PROCESSED FOR
*          |     A GIVEN WPID. SITES ARE CHARLOTTE, DENVER AND NEW YORK.
* 03/01/02 | JRF INCLUDE NON-PARTICIPANT PROCESSING  REF. JF030102
* 04/10/02 | JRF INCREASED LENGTH OF COUNTERS TO N6 REF. JF041002
* 05/20/02 | JRF WRITE REPORT TO WORK FILE 2 FOR FURTHER SORTING (BY
*          |     SITE, LOG COUNT, WPID).  TRANSFER PRINTING OF REPORT
*          |     TO CWFN3302 MODULE.
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3402 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Local_Data;

    private DbsGroup pnd_Local_Data_Pnd_Work_Record;
    private DbsField pnd_Local_Data_Tbl_Wpid;

    private DbsGroup pnd_Local_Data__R_Field_1;
    private DbsField pnd_Local_Data_Tbl_Wpid_Action;
    private DbsField pnd_Local_Data_Tbl_Wpid_Lob;
    private DbsField pnd_Local_Data_Tbl_Wpid_Mbp;
    private DbsField pnd_Local_Data_Tbl_Wpid_Sbp;
    private DbsField pnd_Local_Data_Tbl_Wpid_Act;
    private DbsField pnd_Local_Data_Tbl_Status;
    private DbsField pnd_Local_Data_Tbl_Racf_Id;
    private DbsField pnd_Local_Data_Tbl_Empl_Name;
    private DbsField pnd_Local_Data_Tbl_Site_Id;
    private DbsField pnd_Local_Data_Tbl_Rqst_Type;
    private DbsField pnd_Local_Data_Pnd_Wpid_Code;
    private DbsField pnd_Local_Data_Pnd_Total_New;
    private DbsField pnd_Local_Data_Pnd_Total_Logged;
    private DbsField pnd_Local_Data_Pnd_Total_Retn_Doc;
    private DbsField pnd_Local_Data_Pnd_Wpid_Count;
    private DbsField pnd_Local_Data_Pnd_Wpid_New;
    private DbsField pnd_Local_Data_Pnd_Returned_Doc;
    private DbsField pnd_Local_Data_Pnd_Total_Unclear;
    private DbsField pnd_Local_Data_Pnd_Wpid_Values;
    private DbsField pnd_Local_Data_Pnd_Counters;
    private DbsField pnd_Local_Data_Pnd_Wpid_Cnt;
    private DbsField pnd_Local_Data_Pnd_Totl_Cnt;
    private DbsField pnd_Local_Data_Pnd_Site;
    private DbsField pnd_Local_Data_Pnd_Log_Ret;
    private DbsField pnd_Local_Data_Pnd_Action;
    private DbsField pnd_Local_Data_Pnd_Run_Type;

    private DbsGroup pnd_Work_Record_Sort;
    private DbsField pnd_Work_Record_Sort_Site_Id;
    private DbsField pnd_Work_Record_Sort_Wpid;
    private DbsField pnd_Work_Record_Sort_New_Rqst_Site;
    private DbsField pnd_Work_Record_Sort_New_Rqst_All;
    private DbsField pnd_Work_Record_Sort_Ret_Doc_Site;
    private DbsField pnd_Work_Record_Sort_Ret_Doc_All;
    private DbsField pnd_Work_Record_Sort_Tot_Log_Site;
    private DbsField pnd_Work_Record_Sort_Tot_Log_All;

    private DbsRecord internalLoopRecord;
    private DbsField sort01Tbl_WpidOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Local_Data = localVariables.newGroupInRecord("pnd_Local_Data", "#LOCAL-DATA");

        pnd_Local_Data_Pnd_Work_Record = pnd_Local_Data.newGroupInGroup("pnd_Local_Data_Pnd_Work_Record", "#WORK-RECORD");
        pnd_Local_Data_Tbl_Wpid = pnd_Local_Data_Pnd_Work_Record.newFieldInGroup("pnd_Local_Data_Tbl_Wpid", "TBL-WPID", FieldType.STRING, 6);

        pnd_Local_Data__R_Field_1 = pnd_Local_Data_Pnd_Work_Record.newGroupInGroup("pnd_Local_Data__R_Field_1", "REDEFINE", pnd_Local_Data_Tbl_Wpid);
        pnd_Local_Data_Tbl_Wpid_Action = pnd_Local_Data__R_Field_1.newFieldInGroup("pnd_Local_Data_Tbl_Wpid_Action", "TBL-WPID-ACTION", FieldType.STRING, 
            1);
        pnd_Local_Data_Tbl_Wpid_Lob = pnd_Local_Data__R_Field_1.newFieldInGroup("pnd_Local_Data_Tbl_Wpid_Lob", "TBL-WPID-LOB", FieldType.STRING, 2);
        pnd_Local_Data_Tbl_Wpid_Mbp = pnd_Local_Data__R_Field_1.newFieldInGroup("pnd_Local_Data_Tbl_Wpid_Mbp", "TBL-WPID-MBP", FieldType.STRING, 1);
        pnd_Local_Data_Tbl_Wpid_Sbp = pnd_Local_Data__R_Field_1.newFieldInGroup("pnd_Local_Data_Tbl_Wpid_Sbp", "TBL-WPID-SBP", FieldType.STRING, 2);
        pnd_Local_Data_Tbl_Wpid_Act = pnd_Local_Data_Pnd_Work_Record.newFieldInGroup("pnd_Local_Data_Tbl_Wpid_Act", "TBL-WPID-ACT", FieldType.STRING, 
            1);
        pnd_Local_Data_Tbl_Status = pnd_Local_Data_Pnd_Work_Record.newFieldInGroup("pnd_Local_Data_Tbl_Status", "TBL-STATUS", FieldType.STRING, 4);
        pnd_Local_Data_Tbl_Racf_Id = pnd_Local_Data_Pnd_Work_Record.newFieldInGroup("pnd_Local_Data_Tbl_Racf_Id", "TBL-RACF-ID", FieldType.STRING, 8);
        pnd_Local_Data_Tbl_Empl_Name = pnd_Local_Data_Pnd_Work_Record.newFieldInGroup("pnd_Local_Data_Tbl_Empl_Name", "TBL-EMPL-NAME", FieldType.STRING, 
            30);
        pnd_Local_Data_Tbl_Site_Id = pnd_Local_Data_Pnd_Work_Record.newFieldInGroup("pnd_Local_Data_Tbl_Site_Id", "TBL-SITE-ID", FieldType.STRING, 2);
        pnd_Local_Data_Tbl_Rqst_Type = pnd_Local_Data_Pnd_Work_Record.newFieldInGroup("pnd_Local_Data_Tbl_Rqst_Type", "TBL-RQST-TYPE", FieldType.STRING, 
            1);
        pnd_Local_Data_Pnd_Wpid_Code = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Wpid_Code", "#WPID-CODE", FieldType.STRING, 6);
        pnd_Local_Data_Pnd_Total_New = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Total_New", "#TOTAL-NEW", FieldType.NUMERIC, 6);
        pnd_Local_Data_Pnd_Total_Logged = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Total_Logged", "#TOTAL-LOGGED", FieldType.NUMERIC, 6);
        pnd_Local_Data_Pnd_Total_Retn_Doc = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Total_Retn_Doc", "#TOTAL-RETN-DOC", FieldType.NUMERIC, 
            6);
        pnd_Local_Data_Pnd_Wpid_Count = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Wpid_Count", "#WPID-COUNT", FieldType.NUMERIC, 6);
        pnd_Local_Data_Pnd_Wpid_New = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Wpid_New", "#WPID-NEW", FieldType.NUMERIC, 6);
        pnd_Local_Data_Pnd_Returned_Doc = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Returned_Doc", "#RETURNED-DOC", FieldType.NUMERIC, 6);
        pnd_Local_Data_Pnd_Total_Unclear = pnd_Local_Data.newFieldArrayInGroup("pnd_Local_Data_Pnd_Total_Unclear", "#TOTAL-UNCLEAR", FieldType.PACKED_DECIMAL, 
            6, new DbsArrayController(1, 3));
        pnd_Local_Data_Pnd_Wpid_Values = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Wpid_Values", "#WPID-VALUES", FieldType.STRING, 7);
        pnd_Local_Data_Pnd_Counters = pnd_Local_Data.newFieldArrayInGroup("pnd_Local_Data_Pnd_Counters", "#COUNTERS", FieldType.PACKED_DECIMAL, 6, new 
            DbsArrayController(1, 3, 1, 2, 1, 8));
        pnd_Local_Data_Pnd_Wpid_Cnt = pnd_Local_Data.newFieldArrayInGroup("pnd_Local_Data_Pnd_Wpid_Cnt", "#WPID-CNT", FieldType.PACKED_DECIMAL, 6, new 
            DbsArrayController(1, 3, 1, 2));
        pnd_Local_Data_Pnd_Totl_Cnt = pnd_Local_Data.newFieldArrayInGroup("pnd_Local_Data_Pnd_Totl_Cnt", "#TOTL-CNT", FieldType.PACKED_DECIMAL, 6, new 
            DbsArrayController(1, 3, 1, 2));
        pnd_Local_Data_Pnd_Site = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Site", "#SITE", FieldType.PACKED_DECIMAL, 1);
        pnd_Local_Data_Pnd_Log_Ret = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Log_Ret", "#LOG-RET", FieldType.PACKED_DECIMAL, 1);
        pnd_Local_Data_Pnd_Action = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Action", "#ACTION", FieldType.PACKED_DECIMAL, 1);
        pnd_Local_Data_Pnd_Run_Type = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Run_Type", "#RUN-TYPE", FieldType.STRING, 7);

        pnd_Work_Record_Sort = localVariables.newGroupInRecord("pnd_Work_Record_Sort", "#WORK-RECORD-SORT");
        pnd_Work_Record_Sort_Site_Id = pnd_Work_Record_Sort.newFieldInGroup("pnd_Work_Record_Sort_Site_Id", "SITE-ID", FieldType.STRING, 2);
        pnd_Work_Record_Sort_Wpid = pnd_Work_Record_Sort.newFieldInGroup("pnd_Work_Record_Sort_Wpid", "WPID", FieldType.STRING, 6);
        pnd_Work_Record_Sort_New_Rqst_Site = pnd_Work_Record_Sort.newFieldInGroup("pnd_Work_Record_Sort_New_Rqst_Site", "NEW-RQST-SITE", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Work_Record_Sort_New_Rqst_All = pnd_Work_Record_Sort.newFieldInGroup("pnd_Work_Record_Sort_New_Rqst_All", "NEW-RQST-ALL", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Work_Record_Sort_Ret_Doc_Site = pnd_Work_Record_Sort.newFieldInGroup("pnd_Work_Record_Sort_Ret_Doc_Site", "RET-DOC-SITE", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Work_Record_Sort_Ret_Doc_All = pnd_Work_Record_Sort.newFieldInGroup("pnd_Work_Record_Sort_Ret_Doc_All", "RET-DOC-ALL", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Work_Record_Sort_Tot_Log_Site = pnd_Work_Record_Sort.newFieldInGroup("pnd_Work_Record_Sort_Tot_Log_Site", "TOT-LOG-SITE", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Work_Record_Sort_Tot_Log_All = pnd_Work_Record_Sort.newFieldInGroup("pnd_Work_Record_Sort_Tot_Log_All", "TOT-LOG-ALL", FieldType.PACKED_DECIMAL, 
            6);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        sort01Tbl_WpidOld = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Wpid_OLD", "Tbl_Wpid_OLD", FieldType.STRING, 6);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        localVariables.reset();
        pnd_Local_Data_Pnd_Wpid_Values.setInitialValue("BFIRTXZ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Cwfb3402() throws Exception
    {
        super("Cwfb3402");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Cwfb3402|Main");
        while(true)
        {
            try
            {
                if (condition(Global.getSTACK().getDatacount().equals(1), INPUT_1))                                                                                       //Natural: IF *DATA = 1
                {
                    DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Local_Data_Pnd_Run_Type, new FieldAttributes ("AD=A"));                                        //Natural: INPUT #RUN-TYPE ( AD = A )
                }                                                                                                                                                         //Natural: END-IF
                READWORK01:                                                                                                                                               //Natural: READ WORK 1 #WORK-RECORD
                while (condition(getWorkFiles().read(1, pnd_Local_Data_Pnd_Work_Record)))
                {
                    if (condition(! (pnd_Local_Data_Tbl_Site_Id.equals("C") || pnd_Local_Data_Tbl_Site_Id.equals("D") || pnd_Local_Data_Tbl_Site_Id.equals("N"))))        //Natural: REJECT IF NOT ( TBL-SITE-ID = 'C' OR = 'D' OR = 'N' )
                    {
                        continue;
                    }
                    getSort().writeSortInData(pnd_Local_Data_Tbl_Wpid, pnd_Local_Data_Tbl_Site_Id, pnd_Local_Data_Tbl_Racf_Id, pnd_Local_Data_Tbl_Wpid_Act,               //Natural: END-ALL
                        pnd_Local_Data_Tbl_Status, pnd_Local_Data_Tbl_Rqst_Type);
                }                                                                                                                                                         //Natural: END-WORK
                READWORK01_Exit:
                if (Global.isEscape()) return;
                getSort().sortData(pnd_Local_Data_Tbl_Wpid);                                                                                                              //Natural: SORT BY TBL-WPID USING TBL-SITE-ID TBL-RACF-ID TBL-WPID-ACT TBL-STATUS TBL-RQST-TYPE
                boolean endOfDataSort01 = true;
                boolean firstSort01 = true;
                SORT01:
                while (condition(getSort().readSortOutData(pnd_Local_Data_Tbl_Wpid, pnd_Local_Data_Tbl_Site_Id, pnd_Local_Data_Tbl_Racf_Id, pnd_Local_Data_Tbl_Wpid_Act, 
                    pnd_Local_Data_Tbl_Status, pnd_Local_Data_Tbl_Rqst_Type)))
                {
                    if (condition(getSort().getAstCOUNTER().greater(0)))
                    {
                        atBreakEventSort01(false);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom()))
                                break;
                            else if (condition(Global.isEscapeBottomImmediate()))
                            {
                                endOfDataSort01 = false;
                                break;
                            }
                            else if (condition(Global.isEscapeTop()))
                            continue;
                            else if (condition())
                            return;
                        }
                    }
                    //*  ----------------------------------------------------------------
                    //*  WPID VALUES
                    //*  B=BOOKLET  F=FORMS  I=INQUIRIES  R=RESEARCH  T=TRANSACTIONS
                    //*  X=COMPLAINTS  Z=MISC NEW     ALL OTHERS=OTHER
                    //*  ----------------------------------------------------------------
                    DbsUtil.examine(new ExamineSource(pnd_Local_Data_Pnd_Wpid_Values), new ExamineSearch(pnd_Local_Data_Tbl_Wpid_Act), new ExamineGivingPosition(pnd_Local_Data_Pnd_Action)); //Natural: EXAMINE #WPID-VALUES FOR TBL-WPID-ACT GIVING POSITION #ACTION
                    //*  "other"
                    if (condition(pnd_Local_Data_Pnd_Action.equals(getZero())))                                                                                           //Natural: IF #ACTION = 0
                    {
                        pnd_Local_Data_Pnd_Action.setValue(8);                                                                                                            //Natural: MOVE 8 TO #ACTION
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Local_Data_Tbl_Status.notEquals("0200")))                                                                                           //Natural: IF TBL-STATUS NE '0200'
                    {
                        //*  LOGGED
                        pnd_Local_Data_Pnd_Log_Ret.setValue(1);                                                                                                           //Natural: MOVE 1 TO #LOG-RET
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  RETURNING DOCUMENTS
                        pnd_Local_Data_Pnd_Log_Ret.setValue(2);                                                                                                           //Natural: MOVE 2 TO #LOG-RET
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Local_Data_Pnd_Site.reset();                                                                                                                      //Natural: RESET #SITE
                    short decideConditionsMet119 = 0;                                                                                                                     //Natural: DECIDE ON FIRST VALUE OF TBL-SITE-ID;//Natural: VALUE 'C'
                    if (condition((pnd_Local_Data_Tbl_Site_Id.equals("C"))))
                    {
                        decideConditionsMet119++;
                        pnd_Local_Data_Pnd_Site.setValue(1);                                                                                                              //Natural: MOVE 1 TO #SITE
                    }                                                                                                                                                     //Natural: VALUE 'D'
                    else if (condition((pnd_Local_Data_Tbl_Site_Id.equals("D"))))
                    {
                        decideConditionsMet119++;
                        pnd_Local_Data_Pnd_Site.setValue(2);                                                                                                              //Natural: MOVE 2 TO #SITE
                    }                                                                                                                                                     //Natural: VALUE 'N'
                    else if (condition((pnd_Local_Data_Tbl_Site_Id.equals("N"))))
                    {
                        decideConditionsMet119++;
                        pnd_Local_Data_Pnd_Site.setValue(3);                                                                                                              //Natural: MOVE 3 TO #SITE
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        getReports().write(0, pnd_Local_Data_Tbl_Racf_Id,"INVALID SITEID =",pnd_Local_Data_Tbl_Site_Id);                                                  //Natural: WRITE TBL-RACF-ID 'INVALID SITEID =' TBL-SITE-ID
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-DECIDE
                    //*  FOR TOTALS
                    pnd_Local_Data_Pnd_Counters.getValue(pnd_Local_Data_Pnd_Site,pnd_Local_Data_Pnd_Log_Ret,pnd_Local_Data_Pnd_Action).nadd(1);                           //Natural: ADD 1 TO #COUNTERS ( #SITE, #LOG-RET, #ACTION )
                    //*  FOR DETAIL LINE
                    pnd_Local_Data_Pnd_Wpid_Cnt.getValue(pnd_Local_Data_Pnd_Site,pnd_Local_Data_Pnd_Log_Ret).nadd(1);                                                     //Natural: ADD 1 TO #WPID-CNT ( #SITE, #LOG-RET )
                    if (condition(pnd_Local_Data_Tbl_Status.equals("0200")))                                                                                              //Natural: IF TBL-STATUS = '0200'
                    {
                        pnd_Local_Data_Pnd_Returned_Doc.nadd(1);                                                                                                          //Natural: ADD 1 TO #RETURNED-DOC
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Local_Data_Tbl_Wpid_Action.equals("U") || pnd_Local_Data_Tbl_Wpid_Lob.equals("U") || pnd_Local_Data_Tbl_Wpid_Mbp.equals("U")        //Natural: IF TBL-WPID-ACTION = 'U' OR TBL-WPID-LOB = 'U' OR TBL-WPID-MBP = 'U' OR TBL-WPID-SBP = 'U'
                        || pnd_Local_Data_Tbl_Wpid_Sbp.equals("U")))
                    {
                        pnd_Local_Data_Pnd_Total_Unclear.getValue(pnd_Local_Data_Pnd_Site).nadd(1);                                                                       //Natural: COMPUTE #TOTAL-UNCLEAR ( #SITE ) = #TOTAL-UNCLEAR ( #SITE ) + 1
                    }                                                                                                                                                     //Natural: END-IF
                    //*                                                                                                                                                   //Natural: AT BREAK OF TBL-WPID
                    //*                                                                                                                                                   //Natural: AT END OF DATA
                    //*  READ-2.
                    sort01Tbl_WpidOld.setValue(pnd_Local_Data_Tbl_Wpid);                                                                                                  //Natural: END-SORT
                }
                if (condition(getSort().getAstCOUNTER().greater(0)))
                {
                    atBreakEventSort01(endOfDataSort01);
                }
                if (condition(getSort().getAtEndOfData()))
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-ENDDATA
                endSort();
                //*  -----------------------------------------------------------
                //*  PRINT REPORT - SORTED BY TOTAL-RQST-LOGGED COUNT DESCENDING
                //*  -----------------------------------------------------------
                DbsUtil.callnat(Cwfn3302.class , getCurrentProcessState(), pnd_Local_Data_Pnd_Run_Type, pnd_Local_Data_Pnd_Totl_Cnt.getValue("*","*"),                    //Natural: CALLNAT 'CWFN3302' #RUN-TYPE #TOTL-CNT ( *,* ) #TOTAL-UNCLEAR ( * ) #COUNTERS ( *,*,* )
                    pnd_Local_Data_Pnd_Total_Unclear.getValue("*"), pnd_Local_Data_Pnd_Counters.getValue("*","*","*"));
                if (condition(Global.isEscape())) return;
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }

    //

    // Support Methods

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean pnd_Local_Data_Tbl_WpidIsBreak = pnd_Local_Data_Tbl_Wpid.isBreak(endOfData);
        if (condition(pnd_Local_Data_Tbl_WpidIsBreak))
        {
            pnd_Local_Data_Pnd_Wpid_Code.setValue(sort01Tbl_WpidOld);                                                                                                     //Natural: MOVE OLD ( TBL-WPID ) TO #WPID-CODE
            pnd_Local_Data_Pnd_Total_New.nadd(pnd_Local_Data_Pnd_Wpid_Cnt.getValue("*",1));                                                                               //Natural: ADD #WPID-CNT ( *,1 ) TO #TOTAL-NEW
            pnd_Local_Data_Pnd_Total_Retn_Doc.nadd(pnd_Local_Data_Pnd_Wpid_Cnt.getValue("*",2));                                                                          //Natural: ADD #WPID-CNT ( *,2 ) TO #TOTAL-RETN-DOC
            pnd_Local_Data_Pnd_Total_Logged.nadd(pnd_Local_Data_Pnd_Wpid_Cnt.getValue("*","*"));                                                                          //Natural: ADD #WPID-CNT ( *,* ) TO #TOTAL-LOGGED
            pnd_Work_Record_Sort.reset();                                                                                                                                 //Natural: RESET #WORK-RECORD-SORT #WPID-COUNT
            pnd_Local_Data_Pnd_Wpid_Count.reset();
            pnd_Local_Data_Pnd_Wpid_Count.nadd(pnd_Local_Data_Pnd_Wpid_Cnt.getValue(1,1,":",2));                                                                          //Natural: ADD #WPID-CNT ( 1,1:2 ) TO #WPID-COUNT
            pnd_Work_Record_Sort_Site_Id.setValue("C");                                                                                                                   //Natural: ASSIGN #WORK-RECORD-SORT.SITE-ID := 'C'
            pnd_Work_Record_Sort_Wpid.setValue(pnd_Local_Data_Pnd_Wpid_Code);                                                                                             //Natural: ASSIGN #WORK-RECORD-SORT.WPID := #WPID-CODE
            pnd_Work_Record_Sort_New_Rqst_Site.setValue(pnd_Local_Data_Pnd_Wpid_Cnt.getValue(1,1));                                                                       //Natural: ASSIGN #WORK-RECORD-SORT.NEW-RQST-SITE := #WPID-CNT ( 1,1 )
            pnd_Work_Record_Sort_New_Rqst_All.setValue(pnd_Local_Data_Pnd_Total_New);                                                                                     //Natural: ASSIGN #WORK-RECORD-SORT.NEW-RQST-ALL := #TOTAL-NEW
            pnd_Work_Record_Sort_Ret_Doc_Site.setValue(pnd_Local_Data_Pnd_Wpid_Cnt.getValue(1,2));                                                                        //Natural: ASSIGN #WORK-RECORD-SORT.RET-DOC-SITE := #WPID-CNT ( 1,2 )
            pnd_Work_Record_Sort_Ret_Doc_All.setValue(pnd_Local_Data_Pnd_Total_Retn_Doc);                                                                                 //Natural: ASSIGN #WORK-RECORD-SORT.RET-DOC-ALL := #TOTAL-RETN-DOC
            pnd_Work_Record_Sort_Tot_Log_Site.setValue(pnd_Local_Data_Pnd_Wpid_Count);                                                                                    //Natural: ASSIGN #WORK-RECORD-SORT.TOT-LOG-SITE := #WPID-COUNT
            pnd_Work_Record_Sort_Tot_Log_All.setValue(pnd_Local_Data_Pnd_Total_Logged);                                                                                   //Natural: ASSIGN #WORK-RECORD-SORT.TOT-LOG-ALL := #TOTAL-LOGGED
            getWorkFiles().write(2, false, pnd_Work_Record_Sort);                                                                                                         //Natural: WRITE WORK FILE 2 #WORK-RECORD-SORT
            pnd_Work_Record_Sort.reset();                                                                                                                                 //Natural: RESET #WORK-RECORD-SORT #WPID-COUNT
            pnd_Local_Data_Pnd_Wpid_Count.reset();
            pnd_Local_Data_Pnd_Wpid_Count.nadd(pnd_Local_Data_Pnd_Wpid_Cnt.getValue(2,1,":",2));                                                                          //Natural: ADD #WPID-CNT ( 2,1:2 ) TO #WPID-COUNT
            pnd_Work_Record_Sort_Site_Id.setValue("D");                                                                                                                   //Natural: ASSIGN #WORK-RECORD-SORT.SITE-ID := 'D'
            pnd_Work_Record_Sort_Wpid.setValue(pnd_Local_Data_Pnd_Wpid_Code);                                                                                             //Natural: ASSIGN #WORK-RECORD-SORT.WPID := #WPID-CODE
            pnd_Work_Record_Sort_New_Rqst_Site.setValue(pnd_Local_Data_Pnd_Wpid_Cnt.getValue(2,1));                                                                       //Natural: ASSIGN #WORK-RECORD-SORT.NEW-RQST-SITE := #WPID-CNT ( 2,1 )
            pnd_Work_Record_Sort_New_Rqst_All.setValue(pnd_Local_Data_Pnd_Total_New);                                                                                     //Natural: ASSIGN #WORK-RECORD-SORT.NEW-RQST-ALL := #TOTAL-NEW
            pnd_Work_Record_Sort_Ret_Doc_Site.setValue(pnd_Local_Data_Pnd_Wpid_Cnt.getValue(2,2));                                                                        //Natural: ASSIGN #WORK-RECORD-SORT.RET-DOC-SITE := #WPID-CNT ( 2,2 )
            pnd_Work_Record_Sort_Ret_Doc_All.setValue(pnd_Local_Data_Pnd_Total_Retn_Doc);                                                                                 //Natural: ASSIGN #WORK-RECORD-SORT.RET-DOC-ALL := #TOTAL-RETN-DOC
            pnd_Work_Record_Sort_Tot_Log_Site.setValue(pnd_Local_Data_Pnd_Wpid_Count);                                                                                    //Natural: ASSIGN #WORK-RECORD-SORT.TOT-LOG-SITE := #WPID-COUNT
            pnd_Work_Record_Sort_Tot_Log_All.setValue(pnd_Local_Data_Pnd_Total_Logged);                                                                                   //Natural: ASSIGN #WORK-RECORD-SORT.TOT-LOG-ALL := #TOTAL-LOGGED
            getWorkFiles().write(2, false, pnd_Work_Record_Sort);                                                                                                         //Natural: WRITE WORK FILE 2 #WORK-RECORD-SORT
            pnd_Work_Record_Sort.reset();                                                                                                                                 //Natural: RESET #WORK-RECORD-SORT #WPID-COUNT
            pnd_Local_Data_Pnd_Wpid_Count.reset();
            pnd_Local_Data_Pnd_Wpid_Count.nadd(pnd_Local_Data_Pnd_Wpid_Cnt.getValue(3,1,":",2));                                                                          //Natural: ADD #WPID-CNT ( 3,1:2 ) TO #WPID-COUNT
            pnd_Work_Record_Sort_Site_Id.setValue("N");                                                                                                                   //Natural: ASSIGN #WORK-RECORD-SORT.SITE-ID := 'N'
            pnd_Work_Record_Sort_Wpid.setValue(pnd_Local_Data_Pnd_Wpid_Code);                                                                                             //Natural: ASSIGN #WORK-RECORD-SORT.WPID := #WPID-CODE
            pnd_Work_Record_Sort_New_Rqst_Site.setValue(pnd_Local_Data_Pnd_Wpid_Cnt.getValue(3,1));                                                                       //Natural: ASSIGN #WORK-RECORD-SORT.NEW-RQST-SITE := #WPID-CNT ( 3,1 )
            pnd_Work_Record_Sort_New_Rqst_All.setValue(pnd_Local_Data_Pnd_Total_New);                                                                                     //Natural: ASSIGN #WORK-RECORD-SORT.NEW-RQST-ALL := #TOTAL-NEW
            pnd_Work_Record_Sort_Ret_Doc_Site.setValue(pnd_Local_Data_Pnd_Wpid_Cnt.getValue(3,2));                                                                        //Natural: ASSIGN #WORK-RECORD-SORT.RET-DOC-SITE := #WPID-CNT ( 3,2 )
            pnd_Work_Record_Sort_Ret_Doc_All.setValue(pnd_Local_Data_Pnd_Total_Retn_Doc);                                                                                 //Natural: ASSIGN #WORK-RECORD-SORT.RET-DOC-ALL := #TOTAL-RETN-DOC
            pnd_Work_Record_Sort_Tot_Log_Site.setValue(pnd_Local_Data_Pnd_Wpid_Count);                                                                                    //Natural: ASSIGN #WORK-RECORD-SORT.TOT-LOG-SITE := #WPID-COUNT
            pnd_Work_Record_Sort_Tot_Log_All.setValue(pnd_Local_Data_Pnd_Total_Logged);                                                                                   //Natural: ASSIGN #WORK-RECORD-SORT.TOT-LOG-ALL := #TOTAL-LOGGED
            getWorkFiles().write(2, false, pnd_Work_Record_Sort);                                                                                                         //Natural: WRITE WORK FILE 2 #WORK-RECORD-SORT
            pnd_Local_Data_Pnd_Totl_Cnt.getValue("*","*").nadd(pnd_Local_Data_Pnd_Wpid_Cnt.getValue("*","*"));                                                            //Natural: ADD #WPID-CNT ( *,* ) TO #TOTL-CNT ( *,* )
            pnd_Local_Data_Pnd_Wpid_Cnt.getValue("*","*").reset();                                                                                                        //Natural: RESET #WPID-CNT ( *,* ) #WPID-COUNT #TOTAL-NEW #TOTAL-RETN-DOC #TOTAL-LOGGED
            pnd_Local_Data_Pnd_Wpid_Count.reset();
            pnd_Local_Data_Pnd_Total_New.reset();
            pnd_Local_Data_Pnd_Total_Retn_Doc.reset();
            pnd_Local_Data_Pnd_Total_Logged.reset();
        }                                                                                                                                                                 //Natural: END-BREAK
    }
}
