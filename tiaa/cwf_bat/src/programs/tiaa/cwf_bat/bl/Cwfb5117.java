/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:37:10 PM
**        * FROM NATURAL PROGRAM : Cwfb5117
************************************************************
**        * FILE NAME            : Cwfb5117.java
**        * CLASS NAME           : Cwfb5117
**        * INSTANCE NAME        : Cwfb5117
************************************************************
************************************************************************
* PROGRAM  : CWFB5117
* AUTHOR   : CSARMIENTO 02/24/2004
* TITLE    : PI TABLES - TRANSLATION PROGRAM
* FUNCTION : THIS PROGRAM EXTRACTS THE WPIDS AND PI TASK TYPES TO LOAD
*            INTO A CONTACT ORACLE TABLE
*
*
* HISTORY
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb5117 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Rec;
    private DbsField pnd_Rec_Pnd_Seq;
    private DbsField pnd_Rec_Pnd_Wpid;
    private DbsField pnd_Rec_Pnd_Pi_Task;

    private DataAccessProgramView vw_cwf_Wpid;
    private DbsField cwf_Wpid_Work_Prcss_Id;
    private DbsField cwf_Wpid_Work_Prcss_Long_Nme;
    private DbsField cwf_Wpid_Work_Prcss_Short_Nme;
    private DbsField cwf_Wpid_Dlte_Oprtr_Cde;
    private DbsField cwf_Wpid_Pi_Task_Type;
    private DbsField cwf_Wpid_Pi_Ind;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Rec = localVariables.newGroupInRecord("pnd_Rec", "#REC");
        pnd_Rec_Pnd_Seq = pnd_Rec.newFieldInGroup("pnd_Rec_Pnd_Seq", "#SEQ", FieldType.NUMERIC, 5);
        pnd_Rec_Pnd_Wpid = pnd_Rec.newFieldInGroup("pnd_Rec_Pnd_Wpid", "#WPID", FieldType.STRING, 6);
        pnd_Rec_Pnd_Pi_Task = pnd_Rec.newFieldInGroup("pnd_Rec_Pnd_Pi_Task", "#PI-TASK", FieldType.STRING, 11);

        vw_cwf_Wpid = new DataAccessProgramView(new NameInfo("vw_cwf_Wpid", "CWF-WPID"), "CWF_WP_WORK_PRCSS_ID", "CWF_PROFILE");
        cwf_Wpid_Work_Prcss_Id = vw_cwf_Wpid.getRecord().newFieldInGroup("cwf_Wpid_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        cwf_Wpid_Work_Prcss_Id.setDdmHeader("WORK PRCSS ID");
        cwf_Wpid_Work_Prcss_Long_Nme = vw_cwf_Wpid.getRecord().newFieldInGroup("cwf_Wpid_Work_Prcss_Long_Nme", "WORK-PRCSS-LONG-NME", FieldType.STRING, 
            45, RepeatingFieldStrategy.None, "WORK_PRCSS_LONG_NME");
        cwf_Wpid_Work_Prcss_Long_Nme.setDdmHeader("NAME");
        cwf_Wpid_Work_Prcss_Short_Nme = vw_cwf_Wpid.getRecord().newFieldInGroup("cwf_Wpid_Work_Prcss_Short_Nme", "WORK-PRCSS-SHORT-NME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "WORK_PRCSS_SHORT_NME");
        cwf_Wpid_Work_Prcss_Short_Nme.setDdmHeader("SHORT NAME");
        cwf_Wpid_Dlte_Oprtr_Cde = vw_cwf_Wpid.getRecord().newFieldInGroup("cwf_Wpid_Dlte_Oprtr_Cde", "DLTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "DLTE_OPRTR_CDE");
        cwf_Wpid_Dlte_Oprtr_Cde.setDdmHeader("DELETE/OPER");
        cwf_Wpid_Pi_Task_Type = vw_cwf_Wpid.getRecord().newFieldInGroup("cwf_Wpid_Pi_Task_Type", "PI-TASK-TYPE", FieldType.STRING, 12, RepeatingFieldStrategy.None, 
            "PI_TASK_TYPE");
        cwf_Wpid_Pi_Ind = vw_cwf_Wpid.getRecord().newFieldInGroup("cwf_Wpid_Pi_Ind", "PI-IND", FieldType.STRING, 10, RepeatingFieldStrategy.None, "PI_IND");
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Wpid.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb5117() throws Exception
    {
        super("Cwfb5117");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        vw_cwf_Wpid.startDatabaseRead                                                                                                                                     //Natural: READ CWF-WPID BY WPID-UNIQ-KEY
        (
        "READ01",
        new Oc[] { new Oc("WPID_UNIQ_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_cwf_Wpid.readNextRow("READ01")))
        {
            if (condition(cwf_Wpid_Pi_Task_Type.equals(" ") || cwf_Wpid_Pi_Ind.notEquals("Y")))                                                                           //Natural: IF PI-TASK-TYPE EQ ' ' OR PI-IND NE 'Y'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Rec_Pnd_Seq.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #SEQ
            pnd_Rec_Pnd_Wpid.setValue(cwf_Wpid_Work_Prcss_Id);                                                                                                            //Natural: ASSIGN #WPID := WORK-PRCSS-ID
            pnd_Rec_Pnd_Pi_Task.setValue(cwf_Wpid_Pi_Task_Type);                                                                                                          //Natural: ASSIGN #PI-TASK := PI-TASK-TYPE
            getWorkFiles().write(1, false, pnd_Rec);                                                                                                                      //Natural: WRITE WORK FILE 1 #REC
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //
}
