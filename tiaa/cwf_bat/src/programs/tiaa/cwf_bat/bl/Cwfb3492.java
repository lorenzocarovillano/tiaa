/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:29:55 PM
**        * FROM NATURAL PROGRAM : Cwfb3492
************************************************************
**        * FILE NAME            : Cwfb3492.java
**        * CLASS NAME           : Cwfb3492
**        * INSTANCE NAME        : Cwfb3492
************************************************************
************************************************************************
* PROGRAM  : CWFB3492
* SYSTEM   : CWF
* TITLE    :
* GENERATED: OCT 05,2000
* FUNCTION : EXTRACTS ALL ACTIVE UNIT STATUS CODES / DESCRIPTIONS
*          :   FOR S SPECIFIC UNIT FROM THE SUPPORT TABLE.
*          :   FOR LOADING TO ORACLE (FOR SMART)
*          :
*          :
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* --------   -------- --------------------------------------------------
*
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3492 extends BLNatBase
{
    // Data Areas
    private PdaCwfl5312 pdaCwfl5312;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Org_Status_Tbl;
    private DbsField cwf_Org_Status_Tbl_Status_Cde;
    private DbsField cwf_Org_Status_Tbl_Status_Unit_Cde;
    private DbsField cwf_Org_Status_Tbl_Status_Dscrptn_Txt;
    private DbsField cwf_Org_Status_Tbl_Crprte_Srvce_Time_Stndrd_Grp;
    private DbsField cwf_Org_Status_Tbl_Actve_Ind;
    private DbsField cwf_Org_Status_Tbl_Dlte_Dte_Tme;
    private DbsField cwf_Org_Status_Tbl_Dlte_Oprtr_Cde;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_1;
    private DbsField cwf_Support_Tbl_Tbl_Status_Cde;

    private DbsGroup cwf_Support_Tbl__R_Field_2;
    private DbsField cwf_Support_Tbl_Tbl_C_Status_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_3;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field1;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field2;
    private DbsField cwf_Support_Tbl_Tbl_Actve_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Entry_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Dte;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Dlte_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Table_Rectype;
    private DbsField cwf_Support_Tbl_Tbl_Table_Access_Level;

    private DbsGroup pnd_Work_File;
    private DbsField pnd_Work_File_Group_Name;
    private DbsField pnd_Work_File_Key_Name;

    private DbsGroup pnd_Work_File__R_Field_4;
    private DbsField pnd_Work_File_Status_Unit_Cde;
    private DbsField pnd_Work_File_Status_Cde;
    private DbsField pnd_Work_File_Status_Dscrptn_Txt;
    private DbsField pnd_Status_Key;

    private DbsGroup pnd_Status_Key__R_Field_5;
    private DbsField pnd_Status_Key_Pnd_Status_Unit_Cde;
    private DbsField pnd_Status_Key_Pnd_Status_Cde;
    private DbsField pnd_Status_Key_Pnd_Actve_Ind;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_6;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCwfl5312 = new PdaCwfl5312(localVariables);

        // Local Variables

        vw_cwf_Org_Status_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Org_Status_Tbl", "CWF-ORG-STATUS-TBL"), "CWF_ORG_STATUS_TBL", "CWF_ASSIGN_RULE");
        cwf_Org_Status_Tbl_Status_Cde = vw_cwf_Org_Status_Tbl.getRecord().newFieldInGroup("cwf_Org_Status_Tbl_Status_Cde", "STATUS-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "STATUS_CDE");
        cwf_Org_Status_Tbl_Status_Cde.setDdmHeader("STATUS/CODE");
        cwf_Org_Status_Tbl_Status_Unit_Cde = vw_cwf_Org_Status_Tbl.getRecord().newFieldInGroup("cwf_Org_Status_Tbl_Status_Unit_Cde", "STATUS-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "STATUS_UNIT_CDE");
        cwf_Org_Status_Tbl_Status_Unit_Cde.setDdmHeader("UNIT/CODE");
        cwf_Org_Status_Tbl_Status_Dscrptn_Txt = vw_cwf_Org_Status_Tbl.getRecord().newFieldInGroup("cwf_Org_Status_Tbl_Status_Dscrptn_Txt", "STATUS-DSCRPTN-TXT", 
            FieldType.STRING, 25, RepeatingFieldStrategy.None, "STATUS_DSCRPTN_TXT");
        cwf_Org_Status_Tbl_Status_Dscrptn_Txt.setDdmHeader("STEP/DESCRIPTION");
        cwf_Org_Status_Tbl_Crprte_Srvce_Time_Stndrd_Grp = vw_cwf_Org_Status_Tbl.getRecord().newFieldInGroup("cwf_Org_Status_Tbl_Crprte_Srvce_Time_Stndrd_Grp", 
            "CRPRTE-SRVCE-TIME-STNDRD-GRP", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, "CRPRTE_SRVCE_TIME_STNDRD_GRP");
        cwf_Org_Status_Tbl_Actve_Ind = vw_cwf_Org_Status_Tbl.getRecord().newFieldInGroup("cwf_Org_Status_Tbl_Actve_Ind", "ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ACTVE_IND");
        cwf_Org_Status_Tbl_Dlte_Dte_Tme = vw_cwf_Org_Status_Tbl.getRecord().newFieldInGroup("cwf_Org_Status_Tbl_Dlte_Dte_Tme", "DLTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "DLTE_DTE_TME");
        cwf_Org_Status_Tbl_Dlte_Dte_Tme.setDdmHeader("DELETE/DATE-TIME");
        cwf_Org_Status_Tbl_Dlte_Oprtr_Cde = vw_cwf_Org_Status_Tbl.getRecord().newFieldInGroup("cwf_Org_Status_Tbl_Dlte_Oprtr_Cde", "DLTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "DLTE_OPRTR_CDE");
        cwf_Org_Status_Tbl_Dlte_Oprtr_Cde.setDdmHeader("DELETE/OPERATOR");
        registerRecord(vw_cwf_Org_Status_Tbl);

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");

        cwf_Support_Tbl__R_Field_1 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_1", "REDEFINE", cwf_Support_Tbl_Tbl_Key_Field);
        cwf_Support_Tbl_Tbl_Status_Cde = cwf_Support_Tbl__R_Field_1.newFieldInGroup("cwf_Support_Tbl_Tbl_Status_Cde", "TBL-STATUS-CDE", FieldType.STRING, 
            10);

        cwf_Support_Tbl__R_Field_2 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_2", "REDEFINE", cwf_Support_Tbl_Tbl_Key_Field);
        cwf_Support_Tbl_Tbl_C_Status_Cde = cwf_Support_Tbl__R_Field_2.newFieldInGroup("cwf_Support_Tbl_Tbl_C_Status_Cde", "TBL-C-STATUS-CDE", FieldType.STRING, 
            4);
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Support_Tbl__R_Field_3 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_3", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Tbl_Data_Field1 = cwf_Support_Tbl__R_Field_3.newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field1", "TBL-DATA-FIELD1", FieldType.STRING, 
            33);
        cwf_Support_Tbl_Tbl_Data_Field2 = cwf_Support_Tbl__R_Field_3.newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field2", "TBL-DATA-FIELD2", FieldType.STRING, 
            25);
        cwf_Support_Tbl_Tbl_Actve_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Actve_Ind", "TBL-ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TBL_ACTVE_IND");
        cwf_Support_Tbl_Tbl_Entry_Dte_Tme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Entry_Dte_Tme", "TBL-ENTRY-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_ENTRY_DTE_TME");
        cwf_Support_Tbl_Tbl_Entry_Dte_Tme.setDdmHeader("ENTRY DATE/TIME");
        cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde", "TBL-ENTRY-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_ENTRY_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Support_Tbl_Tbl_Updte_Dte_Tme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Dte_Tme", "TBL-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE_TME");
        cwf_Support_Tbl_Tbl_Updte_Dte_Tme.setDdmHeader("UPDATE DATE/TIME");
        cwf_Support_Tbl_Tbl_Updte_Dte = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Dte", "TBL-UPDTE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE");
        cwf_Support_Tbl_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        cwf_Support_Tbl_Tbl_Dlte_Dte_Tme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Dlte_Dte_Tme", "TBL-DLTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_DLTE_DTE_TME");
        cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde", "TBL-DLTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_DLTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Table_Rectype = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Rectype", "TBL-TABLE-RECTYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TBL_TABLE_RECTYPE");
        cwf_Support_Tbl_Tbl_Table_Access_Level = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Access_Level", "TBL-TABLE-ACCESS-LEVEL", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TBL_TABLE_ACCESS_LEVEL");
        cwf_Support_Tbl_Tbl_Table_Access_Level.setDdmHeader("ACCESS/LEVEL");
        registerRecord(vw_cwf_Support_Tbl);

        pnd_Work_File = localVariables.newGroupInRecord("pnd_Work_File", "#WORK-FILE");
        pnd_Work_File_Group_Name = pnd_Work_File.newFieldInGroup("pnd_Work_File_Group_Name", "GROUP-NAME", FieldType.STRING, 30);
        pnd_Work_File_Key_Name = pnd_Work_File.newFieldInGroup("pnd_Work_File_Key_Name", "KEY-NAME", FieldType.STRING, 12);

        pnd_Work_File__R_Field_4 = pnd_Work_File.newGroupInGroup("pnd_Work_File__R_Field_4", "REDEFINE", pnd_Work_File_Key_Name);
        pnd_Work_File_Status_Unit_Cde = pnd_Work_File__R_Field_4.newFieldInGroup("pnd_Work_File_Status_Unit_Cde", "STATUS-UNIT-CDE", FieldType.STRING, 
            8);
        pnd_Work_File_Status_Cde = pnd_Work_File__R_Field_4.newFieldInGroup("pnd_Work_File_Status_Cde", "STATUS-CDE", FieldType.STRING, 4);
        pnd_Work_File_Status_Dscrptn_Txt = pnd_Work_File.newFieldInGroup("pnd_Work_File_Status_Dscrptn_Txt", "STATUS-DSCRPTN-TXT", FieldType.STRING, 25);
        pnd_Status_Key = localVariables.newFieldInRecord("pnd_Status_Key", "#STATUS-KEY", FieldType.STRING, 13);

        pnd_Status_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Status_Key__R_Field_5", "REDEFINE", pnd_Status_Key);
        pnd_Status_Key_Pnd_Status_Unit_Cde = pnd_Status_Key__R_Field_5.newFieldInGroup("pnd_Status_Key_Pnd_Status_Unit_Cde", "#STATUS-UNIT-CDE", FieldType.STRING, 
            8);
        pnd_Status_Key_Pnd_Status_Cde = pnd_Status_Key__R_Field_5.newFieldInGroup("pnd_Status_Key_Pnd_Status_Cde", "#STATUS-CDE", FieldType.STRING, 4);
        pnd_Status_Key_Pnd_Actve_Ind = pnd_Status_Key__R_Field_5.newFieldInGroup("pnd_Status_Key_Pnd_Actve_Ind", "#ACTVE-IND", FieldType.STRING, 1);
        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_6 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_6", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind = pnd_Tbl_Prime_Key__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", FieldType.STRING, 
            1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Org_Status_Tbl.reset();
        vw_cwf_Support_Tbl.reset();

        localVariables.reset();
        pnd_Work_File_Group_Name.setInitialValue("UNIT-STATUS-TBL");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb3492() throws Exception
    {
        super("Cwfb3492");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        pdaCwfl5312.getPnd_Gen_Var_Pnd_Gen_Tbl_Authrty().setValue("A");                                                                                                   //Natural: MOVE 'A' TO #GEN-TBL-AUTHRTY
        pdaCwfl5312.getPnd_Gen_Var_Pnd_Gen_Tbl_Name().setValue("SMART-EXTRACT");                                                                                          //Natural: MOVE 'SMART-EXTRACT' TO #GEN-TBL-NAME
        pdaCwfl5312.getPnd_Gen_Var_Pnd_Gen_Tbl_Code().setValue("STATUS-CDE");                                                                                             //Natural: MOVE 'STATUS-CDE' TO #GEN-TBL-CODE
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind.setValue("S");                                                                                                          //Natural: MOVE 'S' TO #TBL-SCRTY-LEVEL-IND
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme.setValue("ROUTE-CODE-TABLE");                                                                                                 //Natural: MOVE 'ROUTE-CODE-TABLE' TO #TBL-TABLE-NME
        vw_cwf_Support_Tbl.startDatabaseRead                                                                                                                              //Natural: READ CWF-SUPPORT-TBL BY TBL-PRIME-KEY FROM #GEN-TBL-KEY
        (
        "RD2",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pdaCwfl5312.getPnd_Gen_Var_Pnd_Gen_Tbl_Key(), WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
        );
        RD2:
        while (condition(vw_cwf_Support_Tbl.readNextRow("RD2")))
        {
            if (condition(cwf_Support_Tbl_Tbl_Scrty_Level_Ind.notEquals(pdaCwfl5312.getPnd_Gen_Var_Pnd_Gen_Tbl_Authrty()) || cwf_Support_Tbl_Tbl_Table_Nme.notEquals(pdaCwfl5312.getPnd_Gen_Var_Pnd_Gen_Tbl_Name())  //Natural: IF CWF-SUPPORT-TBL.TBL-SCRTY-LEVEL-IND NE #GEN-TBL-AUTHRTY OR CWF-SUPPORT-TBL.TBL-TABLE-NME NE #GEN-TBL-NAME OR CWF-SUPPORT-TBL.TBL-STATUS-CDE NE #GEN-TBL-CODE
                || cwf_Support_Tbl_Tbl_Status_Cde.notEquals(pdaCwfl5312.getPnd_Gen_Var_Pnd_Gen_Tbl_Code())))
            {
                if (true) break RD2;                                                                                                                                      //Natural: ESCAPE BOTTOM ( RD2. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Status_Key_Pnd_Status_Unit_Cde.setValue(cwf_Support_Tbl_Tbl_Data_Field);                                                                                  //Natural: MOVE TBL-DATA-FIELD TO #STATUS-UNIT-CDE
            //*  NUMERIC STATUS-CDE
            vw_cwf_Org_Status_Tbl.startDatabaseRead                                                                                                                       //Natural: READ CWF-ORG-STATUS-TBL BY STATUS-KEY #STATUS-KEY
            (
            "RD3",
            new Wc[] { new Wc("STATUS_KEY", ">=", pnd_Status_Key, WcType.BY) },
            new Oc[] { new Oc("STATUS_KEY", "ASC") }
            );
            RD3:
            while (condition(vw_cwf_Org_Status_Tbl.readNextRow("RD3")))
            {
                if (condition(cwf_Org_Status_Tbl_Status_Unit_Cde.notEquals(pnd_Status_Key_Pnd_Status_Unit_Cde)))                                                          //Natural: IF CWF-ORG-STATUS-TBL.STATUS-UNIT-CDE NE #STATUS-UNIT-CDE
                {
                    if (true) break RD3;                                                                                                                                  //Natural: ESCAPE BOTTOM ( RD3. )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(cwf_Org_Status_Tbl_Dlte_Dte_Tme.greater(getZero()) || cwf_Org_Status_Tbl_Dlte_Oprtr_Cde.greater(" ")))                                      //Natural: REJECT IF DLTE-DTE-TME GT 0 OR DLTE-OPRTR-CDE GT ' '
                {
                    continue;
                }
                pnd_Work_File.setValuesByName(vw_cwf_Org_Status_Tbl);                                                                                                     //Natural: MOVE BY NAME CWF-ORG-STATUS-TBL TO #WORK-FILE
                getWorkFiles().write(1, false, pnd_Work_File);                                                                                                            //Natural: WRITE WORK FILE 1 #WORK-FILE
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD2"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD2"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  C-STATUS-CDE
            vw_cwf_Support_Tbl.startDatabaseRead                                                                                                                          //Natural: READ CWF-SUPPORT-TBL BY TBL-PRIME-KEY = #TBL-PRIME-KEY
            (
            "RD1",
            new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key, WcType.BY) },
            new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
            );
            RD1:
            while (condition(vw_cwf_Support_Tbl.readNextRow("RD1")))
            {
                if (condition(cwf_Support_Tbl_Tbl_Scrty_Level_Ind.notEquals(pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind) || cwf_Support_Tbl_Tbl_Table_Nme.notEquals(pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme)  //Natural: IF CWF-SUPPORT-TBL.TBL-SCRTY-LEVEL-IND NE #TBL-SCRTY-LEVEL-IND OR CWF-SUPPORT-TBL.TBL-TABLE-NME NE #TBL-TABLE-NME OR CWF-SUPPORT-TBL.TBL-C-STATUS-CDE EQ MASK ( N... )
                    || DbsUtil.maskMatches(cwf_Support_Tbl_Tbl_C_Status_Cde,"N...")))
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(cwf_Org_Status_Tbl_Dlte_Dte_Tme.greater(getZero()) || cwf_Org_Status_Tbl_Dlte_Oprtr_Cde.greater(" ")))                                      //Natural: REJECT IF DLTE-DTE-TME GT 0 OR DLTE-OPRTR-CDE GT ' '
                {
                    continue;
                }
                pnd_Work_File_Group_Name.setValue("UNIT-STATUS-TBL");                                                                                                     //Natural: MOVE 'UNIT-STATUS-TBL' TO #WORK-FILE.GROUP-NAME
                pnd_Work_File_Status_Unit_Cde.setValue(pnd_Status_Key_Pnd_Status_Unit_Cde);                                                                               //Natural: MOVE #STATUS-UNIT-CDE TO #WORK-FILE.STATUS-UNIT-CDE
                pnd_Work_File_Status_Cde.setValue(cwf_Support_Tbl_Tbl_C_Status_Cde);                                                                                      //Natural: MOVE CWF-SUPPORT-TBL.TBL-C-STATUS-CDE TO #WORK-FILE.STATUS-CDE
                pnd_Work_File_Status_Dscrptn_Txt.setValue(cwf_Support_Tbl_Tbl_Data_Field2);                                                                               //Natural: MOVE CWF-SUPPORT-TBL.TBL-DATA-FIELD2 TO #WORK-FILE.STATUS-DSCRPTN-TXT
                getWorkFiles().write(1, false, pnd_Work_File);                                                                                                            //Natural: WRITE WORK FILE 1 #WORK-FILE
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD2"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD2"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //
}
