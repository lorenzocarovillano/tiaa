/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:03:41 PM
**        * FROM NATURAL PROGRAM : Ncwp9022
************************************************************
**        * FILE NAME            : Ncwp9022.java
**        * CLASS NAME           : Ncwp9022
**        * INSTANCE NAME        : Ncwp9022
************************************************************
************************************************************************
* PROGRAM  : NCWP9022
* SYSTEM   : CRPCWF
* TITLE    : CONTROL MODULE TRANSACTION AUDIT LIST
* GENERATED: MARCH 10, 1997 AT  10:00 AM
* FUNCTION : THIS PROGRAM PRINTS TRANSACTIONS LOGGED BY THE NCW CONTROL
*          : MODULE. THE TRANSACTIONS WERE SELECTED IN EFSP9020 AND
*          : SORTED BY LOG-DTE-TME.  THE REPORT IS PRODUCED DAILY.
* HISTORY
* ---------------------------------------
* 10/11/95 JHH - READ SORTED WORK FILE RATHER THAN NCW-EFM-AUDIT
************************************************************************
* GLOBAL USING CWFG000

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Ncwp9022 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_ncw_Efm_Audit;
    private DbsField ncw_Efm_Audit_Log_Dte_Tme;
    private DbsField ncw_Efm_Audit_System_Cde;
    private DbsField ncw_Efm_Audit_Action_Cde;
    private DbsField ncw_Efm_Audit_Empl_Oprtr_Cde;
    private DbsField ncw_Efm_Audit_Rqst_Prcss_Tme;
    private DbsField ncw_Efm_Audit_Recursive_Ind;
    private DbsField ncw_Efm_Audit_Error_Cde;
    private DbsField ncw_Efm_Audit_Mit_Added_Cnt;
    private DbsField ncw_Efm_Audit_Mit_Updated_Cnt;
    private DbsField ncw_Efm_Audit_Dcmnt_Added_Cnt;
    private DbsField ncw_Efm_Audit_Dcmnt_Renamed_Cnt;
    private DbsField ncw_Efm_Audit_Np_Pin;
    private DbsGroup ncw_Efm_Audit_Count_Castrqst_IdMuGroup;
    private DbsField ncw_Efm_Audit_Count_Castrqst_Id;
    private DbsGroup ncw_Efm_Audit_Rqst_IdMuGroup;
    private DbsField ncw_Efm_Audit_Rqst_Id;
    private DbsField pnd_Program;
    private DbsField pnd_I;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_ncw_Efm_Audit = new DataAccessProgramView(new NameInfo("vw_ncw_Efm_Audit", "NCW-EFM-AUDIT"), "NCW_EFM_AUDIT", "NCW_EFM_AUDIT");
        ncw_Efm_Audit_Log_Dte_Tme = vw_ncw_Efm_Audit.getRecord().newFieldInGroup("ncw_Efm_Audit_Log_Dte_Tme", "LOG-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LOG_DTE_TME");
        ncw_Efm_Audit_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        ncw_Efm_Audit_System_Cde = vw_ncw_Efm_Audit.getRecord().newFieldInGroup("ncw_Efm_Audit_System_Cde", "SYSTEM-CDE", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "SYSTEM_CDE");
        ncw_Efm_Audit_System_Cde.setDdmHeader("SYSTEM");
        ncw_Efm_Audit_Action_Cde = vw_ncw_Efm_Audit.getRecord().newFieldInGroup("ncw_Efm_Audit_Action_Cde", "ACTION-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTION_CDE");
        ncw_Efm_Audit_Action_Cde.setDdmHeader("ACTION");
        ncw_Efm_Audit_Empl_Oprtr_Cde = vw_ncw_Efm_Audit.getRecord().newFieldInGroup("ncw_Efm_Audit_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "EMPL_OPRTR_CDE");
        ncw_Efm_Audit_Empl_Oprtr_Cde.setDdmHeader("OPERATOR/CODE");
        ncw_Efm_Audit_Rqst_Prcss_Tme = vw_ncw_Efm_Audit.getRecord().newFieldInGroup("ncw_Efm_Audit_Rqst_Prcss_Tme", "RQST-PRCSS-TME", FieldType.NUMERIC, 
            5, RepeatingFieldStrategy.None, "RQST_PRCSS_TME");
        ncw_Efm_Audit_Rqst_Prcss_Tme.setDdmHeader("PROCESS/TIME");
        ncw_Efm_Audit_Recursive_Ind = vw_ncw_Efm_Audit.getRecord().newFieldInGroup("ncw_Efm_Audit_Recursive_Ind", "RECURSIVE-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "RECURSIVE_IND");
        ncw_Efm_Audit_Recursive_Ind.setDdmHeader("RECUR/IND");
        ncw_Efm_Audit_Error_Cde = vw_ncw_Efm_Audit.getRecord().newFieldInGroup("ncw_Efm_Audit_Error_Cde", "ERROR-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "ERROR_CDE");
        ncw_Efm_Audit_Error_Cde.setDdmHeader("ERR/CDE");
        ncw_Efm_Audit_Mit_Added_Cnt = vw_ncw_Efm_Audit.getRecord().newFieldInGroup("ncw_Efm_Audit_Mit_Added_Cnt", "MIT-ADDED-CNT", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "MIT_ADDED_CNT");
        ncw_Efm_Audit_Mit_Added_Cnt.setDdmHeader("NBR OF MIT/RECS ADDED");
        ncw_Efm_Audit_Mit_Updated_Cnt = vw_ncw_Efm_Audit.getRecord().newFieldInGroup("ncw_Efm_Audit_Mit_Updated_Cnt", "MIT-UPDATED-CNT", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "MIT_UPDATED_CNT");
        ncw_Efm_Audit_Mit_Updated_Cnt.setDdmHeader("NBR OF MIT/RECS UPDATED");
        ncw_Efm_Audit_Dcmnt_Added_Cnt = vw_ncw_Efm_Audit.getRecord().newFieldInGroup("ncw_Efm_Audit_Dcmnt_Added_Cnt", "DCMNT-ADDED-CNT", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "DCMNT_ADDED_CNT");
        ncw_Efm_Audit_Dcmnt_Added_Cnt.setDdmHeader("NBR OF DCMT/ADDED");
        ncw_Efm_Audit_Dcmnt_Renamed_Cnt = vw_ncw_Efm_Audit.getRecord().newFieldInGroup("ncw_Efm_Audit_Dcmnt_Renamed_Cnt", "DCMNT-RENAMED-CNT", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "DCMNT_RENAMED_CNT");
        ncw_Efm_Audit_Dcmnt_Renamed_Cnt.setDdmHeader("NBR OF DCMT/RENAMED");
        ncw_Efm_Audit_Np_Pin = vw_ncw_Efm_Audit.getRecord().newFieldInGroup("ncw_Efm_Audit_Np_Pin", "NP-PIN", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "NP_PIN");
        ncw_Efm_Audit_Np_Pin.setDdmHeader("PIN");
        ncw_Efm_Audit_Count_Castrqst_Id = vw_ncw_Efm_Audit.getRecord().newFieldInGroup("ncw_Efm_Audit_Count_Castrqst_Id", "C*RQST-ID", RepeatingFieldStrategy.CAsteriskVariable, 
            "NCW_EFM_AUDIT_RQST_ID");
        ncw_Efm_Audit_Rqst_IdMuGroup = vw_ncw_Efm_Audit.getRecord().newGroupInGroup("NCW_EFM_AUDIT_RQST_IDMuGroup", "RQST_IDMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "NCW_EFM_AUDIT_RQST_ID");
        ncw_Efm_Audit_Rqst_Id = ncw_Efm_Audit_Rqst_IdMuGroup.newFieldArrayInGroup("ncw_Efm_Audit_Rqst_Id", "RQST-ID", FieldType.STRING, 23, new DbsArrayController(1, 
            10), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "RQST_ID");
        ncw_Efm_Audit_Rqst_Id.setDdmHeader("RQST ID");
        registerRecord(vw_ncw_Efm_Audit);

        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ncw_Efm_Audit.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Ncwp9022() throws Exception
    {
        super("Ncwp9022");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("NCWP9022", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        getReports().definePrinter(2, "REPT");                                                                                                                            //Natural: DEFINE PRINTER ( REPT = 1 )
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: FORMAT ( REPT ) LS = 132 PS = 60 ZP = OFF SG = OFF;//Natural: ASSIGN #PROGRAM = *PROGRAM
        //*  READ THE FOLDER AUDIT WORK FILE AND PRINT THE REPORT
        //*  NEW 10/11/95 - JHH
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 NCW-EFM-AUDIT
        while (condition(getWorkFiles().read(1, vw_ncw_Efm_Audit)))
        {
            if (condition(ncw_Efm_Audit_Log_Dte_Tme.equals(getZero())))                                                                                                   //Natural: REJECT IF NCW-EFM-AUDIT.LOG-DTE-TME = 0
            {
                continue;
            }
            getReports().write(2, ReportOption.NOHDR, writeMapToStringOutput(Ncwf9041.class));                                                                            //Natural: WRITE ( REPT ) NOHDR USING FORM 'NCWF9041'
            if (condition(ncw_Efm_Audit_Count_Castrqst_Id.greater(1)))                                                                                                    //Natural: IF NCW-EFM-AUDIT.C*RQST-ID > 1
            {
                FOR01:                                                                                                                                                    //Natural: FOR #I = 2 TO NCW-EFM-AUDIT.C*RQST-ID
                for (pnd_I.setValue(2); condition(pnd_I.lessOrEqual(ncw_Efm_Audit_Count_Castrqst_Id)); pnd_I.nadd(1))
                {
                    getReports().write(2, ReportOption.NOHDR,new TabSetting(95),ncw_Efm_Audit_Rqst_Id.getValue(pnd_I));                                                   //Natural: WRITE ( REPT ) NOHDR 095T NCW-EFM-AUDIT.RQST-ID ( #I )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( REPT )
        //*                                                                                                                                                               //Natural: ON ERROR
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Ncwf9040.class));                                              //Natural: WRITE ( REPT ) NOTITLE NOHDR USING FORM 'NCWF9040'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, ReportOption.NOTITLE,"NATURAL error",Global.getERROR_NR(),"in",Global.getPROGRAM(),"at line",Global.getERROR_LINE());                       //Natural: WRITE 'NATURAL error' *ERROR-NR 'in' *PROGRAM 'at line' *ERROR-LINE
        DbsUtil.terminate(5);  if (true) return;                                                                                                                          //Natural: TERMINATE 05
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(2, "LS=132 PS=60 ZP=OFF SG=OFF");
    }
}
