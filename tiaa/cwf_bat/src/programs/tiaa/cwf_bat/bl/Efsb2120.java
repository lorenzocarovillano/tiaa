/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:06:13 PM
**        * FROM NATURAL PROGRAM : Efsb2120
************************************************************
**        * FILE NAME            : Efsb2120.java
**        * CLASS NAME           : Efsb2120
**        * INSTANCE NAME        : Efsb2120
************************************************************
**********************************************************************
* PROGRAM NAME: EFSB2120
* WRITTEN BY  : J HOFSTETTER
* DESCRIPTION : REPORT 'Document Statistics for Closed Work Requests'
*               . READS WORK FILE FROM EFSB2100 SORTED BY UNIT AND DOC
*               . PRINTS REPORT BY UNIT, DOCUMENT TYPE
* DATE WRITTEN: JUL 27, 1995
* MODIFIED    :
* MM/DD/YY JHH -
* 04/24/2017 - PIN-EXP - PIN EXPANSION 2017, AUG REL
**********************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsb2120 extends BLNatBase
{
    // Data Areas
    private PdaCwfl5312 pdaCwfl5312;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Work_Record;

    private DbsGroup pnd_Work_Record__R_Field_1;
    private DbsField pnd_Work_Record_Pnd_Work_Type;
    private DbsField pnd_Work_Record_Pnd_Work_Folder;
    private DbsField pnd_Work_Record_Pnd_Work_Unit_1st;
    private DbsField pnd_Work_Record_Pnd_Work_Doc_Type;

    private DbsGroup pnd_Work_Record__R_Field_2;
    private DbsField pnd_Work_Record_Pnd_Work_Cat;
    private DbsField pnd_Work_Record_Pnd_Work_Sub_Cat;
    private DbsField pnd_Work_Record_Pnd_Work_Detail;
    private DbsField pnd_Work_Record_Pnd_Work_Doc_Sub_Type;
    private DbsField pnd_Work_Record_Pnd_Work_System;
    private DbsField pnd_Work_Record_Pnd_Work_Direction;
    private DbsField pnd_Work_Record_Pnd_Work_Unit_Entry;

    private DbsGroup pnd_Work_Record__R_Field_3;
    private DbsField pnd_Work_Record_Pnd_Work_Typ10;

    private DbsGroup pnd_Work_Record_Pnd_Input_Dates;
    private DbsField pnd_Work_Record_Pnd_F_Yy;
    private DbsField pnd_Work_Record_Pnd_F_Mm;
    private DbsField pnd_Work_Record_Pnd_F_Dd;
    private DbsField pnd_Work_Record_Pnd_T_Yy;
    private DbsField pnd_Work_Record_Pnd_T_Mm;
    private DbsField pnd_Work_Record_Pnd_T_Dd;

    private DataAccessProgramView vw_cwf_Doc_Type;
    private DbsField cwf_Doc_Type_Dcmnt_Nme;

    private DbsGroup h10;
    private DbsField h10_H10a;
    private DbsField h10_H10b;
    private DbsField h10_H10c;

    private DbsGroup h10__R_Field_4;

    private DbsGroup h10_H10_Dates;
    private DbsField h10_Pnd_F_Mm;
    private DbsField h10_Sl1;
    private DbsField h10_Pnd_F_Dd;
    private DbsField h10_Sl2;
    private DbsField h10_Pnd_F_Yy;
    private DbsField h10_Dash;
    private DbsField h10_Pnd_T_Mm;
    private DbsField h10_Sl3;
    private DbsField h10_Pnd_T_Dd;
    private DbsField h10_Sl4;
    private DbsField h10_Pnd_T_Yy;
    private DbsField h10_H10d;
    private DbsField h10_H10pge;

    private DbsGroup h20;
    private DbsField h20_H20a;
    private DbsField h20_H20b;

    private DbsGroup h30;
    private DbsField h30_H30a;
    private DbsField h30_H30b;

    private DbsGroup h35;
    private DbsField h35_H35a;
    private DbsField h35_H35b;

    private DbsGroup h40;
    private DbsField h40_H40a;
    private DbsField h40_H40b;

    private DbsGroup d50;
    private DbsField d50_D50_Unit;
    private DbsField d50_D50_Cat;
    private DbsField d50_D50_Type;
    private DbsField d50_D50_Name;
    private DbsField d50_D50_Text;
    private DbsField d50_D50_Image;
    private DbsField d50_D50_Other;
    private DbsField d50_D50_Total;
    private DbsField pnd_Text_Tot;
    private DbsField pnd_Image_Tot;
    private DbsField pnd_Other_Tot;
    private DbsField pnd_Tot_Tot;
    private DbsField pnd_Pge1_Cnt;
    private DbsField pnd_Pge2_Cnt;
    private DbsField pnd_Line_Cnt;
    private DbsField pnd_Doc_Cnt;
    private DbsField pnd_Hold_Unit;
    private DbsField pnd_Hold_Doc_Type;

    private DbsGroup pnd_Hold_Doc_Type__R_Field_5;
    private DbsField pnd_Hold_Doc_Type_Pnd_Hold_Cat;
    private DbsField pnd_Hold_Doc_Type_Pnd_Hold_Sub_Cat;
    private DbsField pnd_Hold_Doc_Type_Pnd_Hold_Detail;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCwfl5312 = new PdaCwfl5312(localVariables);

        // Local Variables
        pnd_Work_Record = localVariables.newFieldInRecord("pnd_Work_Record", "#WORK-RECORD", FieldType.STRING, 65);

        pnd_Work_Record__R_Field_1 = localVariables.newGroupInRecord("pnd_Work_Record__R_Field_1", "REDEFINE", pnd_Work_Record);
        pnd_Work_Record_Pnd_Work_Type = pnd_Work_Record__R_Field_1.newFieldInGroup("pnd_Work_Record_Pnd_Work_Type", "#WORK-TYPE", FieldType.STRING, 2);
        pnd_Work_Record_Pnd_Work_Folder = pnd_Work_Record__R_Field_1.newFieldInGroup("pnd_Work_Record_Pnd_Work_Folder", "#WORK-FOLDER", FieldType.STRING, 
            27);
        pnd_Work_Record_Pnd_Work_Unit_1st = pnd_Work_Record__R_Field_1.newFieldInGroup("pnd_Work_Record_Pnd_Work_Unit_1st", "#WORK-UNIT-1ST", FieldType.STRING, 
            8);
        pnd_Work_Record_Pnd_Work_Doc_Type = pnd_Work_Record__R_Field_1.newFieldInGroup("pnd_Work_Record_Pnd_Work_Doc_Type", "#WORK-DOC-TYPE", FieldType.STRING, 
            8);

        pnd_Work_Record__R_Field_2 = pnd_Work_Record__R_Field_1.newGroupInGroup("pnd_Work_Record__R_Field_2", "REDEFINE", pnd_Work_Record_Pnd_Work_Doc_Type);
        pnd_Work_Record_Pnd_Work_Cat = pnd_Work_Record__R_Field_2.newFieldInGroup("pnd_Work_Record_Pnd_Work_Cat", "#WORK-CAT", FieldType.STRING, 1);
        pnd_Work_Record_Pnd_Work_Sub_Cat = pnd_Work_Record__R_Field_2.newFieldInGroup("pnd_Work_Record_Pnd_Work_Sub_Cat", "#WORK-SUB-CAT", FieldType.STRING, 
            3);
        pnd_Work_Record_Pnd_Work_Detail = pnd_Work_Record__R_Field_2.newFieldInGroup("pnd_Work_Record_Pnd_Work_Detail", "#WORK-DETAIL", FieldType.STRING, 
            4);
        pnd_Work_Record_Pnd_Work_Doc_Sub_Type = pnd_Work_Record__R_Field_1.newFieldInGroup("pnd_Work_Record_Pnd_Work_Doc_Sub_Type", "#WORK-DOC-SUB-TYPE", 
            FieldType.STRING, 1);
        pnd_Work_Record_Pnd_Work_System = pnd_Work_Record__R_Field_1.newFieldInGroup("pnd_Work_Record_Pnd_Work_System", "#WORK-SYSTEM", FieldType.STRING, 
            8);
        pnd_Work_Record_Pnd_Work_Direction = pnd_Work_Record__R_Field_1.newFieldInGroup("pnd_Work_Record_Pnd_Work_Direction", "#WORK-DIRECTION", FieldType.STRING, 
            1);
        pnd_Work_Record_Pnd_Work_Unit_Entry = pnd_Work_Record__R_Field_1.newFieldInGroup("pnd_Work_Record_Pnd_Work_Unit_Entry", "#WORK-UNIT-ENTRY", FieldType.STRING, 
            8);

        pnd_Work_Record__R_Field_3 = localVariables.newGroupInRecord("pnd_Work_Record__R_Field_3", "REDEFINE", pnd_Work_Record);
        pnd_Work_Record_Pnd_Work_Typ10 = pnd_Work_Record__R_Field_3.newFieldInGroup("pnd_Work_Record_Pnd_Work_Typ10", "#WORK-TYP10", FieldType.STRING, 
            2);

        pnd_Work_Record_Pnd_Input_Dates = pnd_Work_Record__R_Field_3.newGroupInGroup("pnd_Work_Record_Pnd_Input_Dates", "#INPUT-DATES");
        pnd_Work_Record_Pnd_F_Yy = pnd_Work_Record_Pnd_Input_Dates.newFieldInGroup("pnd_Work_Record_Pnd_F_Yy", "#F-YY", FieldType.STRING, 4);
        pnd_Work_Record_Pnd_F_Mm = pnd_Work_Record_Pnd_Input_Dates.newFieldInGroup("pnd_Work_Record_Pnd_F_Mm", "#F-MM", FieldType.STRING, 2);
        pnd_Work_Record_Pnd_F_Dd = pnd_Work_Record_Pnd_Input_Dates.newFieldInGroup("pnd_Work_Record_Pnd_F_Dd", "#F-DD", FieldType.STRING, 2);
        pnd_Work_Record_Pnd_T_Yy = pnd_Work_Record_Pnd_Input_Dates.newFieldInGroup("pnd_Work_Record_Pnd_T_Yy", "#T-YY", FieldType.STRING, 4);
        pnd_Work_Record_Pnd_T_Mm = pnd_Work_Record_Pnd_Input_Dates.newFieldInGroup("pnd_Work_Record_Pnd_T_Mm", "#T-MM", FieldType.STRING, 2);
        pnd_Work_Record_Pnd_T_Dd = pnd_Work_Record_Pnd_Input_Dates.newFieldInGroup("pnd_Work_Record_Pnd_T_Dd", "#T-DD", FieldType.STRING, 2);

        vw_cwf_Doc_Type = new DataAccessProgramView(new NameInfo("vw_cwf_Doc_Type", "CWF-DOC-TYPE"), "CWF_WP_DCMNT_TYPE_TBL", "CWF_PROFILE");
        cwf_Doc_Type_Dcmnt_Nme = vw_cwf_Doc_Type.getRecord().newFieldInGroup("cwf_Doc_Type_Dcmnt_Nme", "DCMNT-NME", FieldType.STRING, 45, RepeatingFieldStrategy.None, 
            "DCMNT_NME");
        cwf_Doc_Type_Dcmnt_Nme.setDdmHeader("DOCNAME");
        registerRecord(vw_cwf_Doc_Type);

        h10 = localVariables.newGroupInRecord("h10", "H10");
        h10_H10a = h10.newFieldInGroup("h10_H10a", "H10A", FieldType.STRING, 10);
        h10_H10b = h10.newFieldInGroup("h10_H10b", "H10B", FieldType.STRING, 60);
        h10_H10c = h10.newFieldInGroup("h10_H10c", "H10C", FieldType.STRING, 23);

        h10__R_Field_4 = h10.newGroupInGroup("h10__R_Field_4", "REDEFINE", h10_H10c);

        h10_H10_Dates = h10__R_Field_4.newGroupInGroup("h10_H10_Dates", "H10-DATES");
        h10_Pnd_F_Mm = h10_H10_Dates.newFieldInGroup("h10_Pnd_F_Mm", "#F-MM", FieldType.STRING, 2);
        h10_Sl1 = h10_H10_Dates.newFieldInGroup("h10_Sl1", "SL1", FieldType.STRING, 1);
        h10_Pnd_F_Dd = h10_H10_Dates.newFieldInGroup("h10_Pnd_F_Dd", "#F-DD", FieldType.STRING, 2);
        h10_Sl2 = h10_H10_Dates.newFieldInGroup("h10_Sl2", "SL2", FieldType.STRING, 1);
        h10_Pnd_F_Yy = h10_H10_Dates.newFieldInGroup("h10_Pnd_F_Yy", "#F-YY", FieldType.STRING, 4);
        h10_Dash = h10_H10_Dates.newFieldInGroup("h10_Dash", "DASH", FieldType.STRING, 3);
        h10_Pnd_T_Mm = h10_H10_Dates.newFieldInGroup("h10_Pnd_T_Mm", "#T-MM", FieldType.STRING, 2);
        h10_Sl3 = h10_H10_Dates.newFieldInGroup("h10_Sl3", "SL3", FieldType.STRING, 1);
        h10_Pnd_T_Dd = h10_H10_Dates.newFieldInGroup("h10_Pnd_T_Dd", "#T-DD", FieldType.STRING, 2);
        h10_Sl4 = h10_H10_Dates.newFieldInGroup("h10_Sl4", "SL4", FieldType.STRING, 1);
        h10_Pnd_T_Yy = h10_H10_Dates.newFieldInGroup("h10_Pnd_T_Yy", "#T-YY", FieldType.STRING, 4);
        h10_H10d = h10.newFieldInGroup("h10_H10d", "H10D", FieldType.STRING, 15);
        h10_H10pge = h10.newFieldInGroup("h10_H10pge", "H10PGE", FieldType.NUMERIC, 3);

        h20 = localVariables.newGroupInRecord("h20", "H20");
        h20_H20a = h20.newFieldInGroup("h20_H20a", "H20A", FieldType.STRING, 36);
        h20_H20b = h20.newFieldInGroup("h20_H20b", "H20B", FieldType.STRING, 69);

        h30 = localVariables.newGroupInRecord("h30", "H30");
        h30_H30a = h30.newFieldInGroup("h30_H30a", "H30A", FieldType.STRING, 37);
        h30_H30b = h30.newFieldInGroup("h30_H30b", "H30B", FieldType.STRING, 62);

        h35 = localVariables.newGroupInRecord("h35", "H35");
        h35_H35a = h35.newFieldInGroup("h35_H35a", "H35A", FieldType.STRING, 37);
        h35_H35b = h35.newFieldInGroup("h35_H35b", "H35B", FieldType.STRING, 62);

        h40 = localVariables.newGroupInRecord("h40", "H40");
        h40_H40a = h40.newFieldInGroup("h40_H40a", "H40A", FieldType.STRING, 37);
        h40_H40b = h40.newFieldInGroup("h40_H40b", "H40B", FieldType.STRING, 62);

        d50 = localVariables.newGroupInRecord("d50", "D50");
        d50_D50_Unit = d50.newFieldInGroup("d50_D50_Unit", "D50-UNIT", FieldType.STRING, 9);
        d50_D50_Cat = d50.newFieldInGroup("d50_D50_Cat", "D50-CAT", FieldType.STRING, 16);
        d50_D50_Type = d50.newFieldInGroup("d50_D50_Type", "D50-TYPE", FieldType.STRING, 11);
        d50_D50_Name = d50.newFieldInGroup("d50_D50_Name", "D50-NAME", FieldType.STRING, 33);
        d50_D50_Text = d50.newFieldInGroup("d50_D50_Text", "D50-TEXT", FieldType.NUMERIC, 6);
        d50_D50_Image = d50.newFieldInGroup("d50_D50_Image", "D50-IMAGE", FieldType.NUMERIC, 6);
        d50_D50_Other = d50.newFieldInGroup("d50_D50_Other", "D50-OTHER", FieldType.NUMERIC, 6);
        d50_D50_Total = d50.newFieldInGroup("d50_D50_Total", "D50-TOTAL", FieldType.NUMERIC, 6);
        pnd_Text_Tot = localVariables.newFieldArrayInRecord("pnd_Text_Tot", "#TEXT-TOT", FieldType.NUMERIC, 7, new DbsArrayController(1, 3));
        pnd_Image_Tot = localVariables.newFieldArrayInRecord("pnd_Image_Tot", "#IMAGE-TOT", FieldType.NUMERIC, 7, new DbsArrayController(1, 3));
        pnd_Other_Tot = localVariables.newFieldArrayInRecord("pnd_Other_Tot", "#OTHER-TOT", FieldType.NUMERIC, 7, new DbsArrayController(1, 3));
        pnd_Tot_Tot = localVariables.newFieldArrayInRecord("pnd_Tot_Tot", "#TOT-TOT", FieldType.NUMERIC, 7, new DbsArrayController(1, 3));
        pnd_Pge1_Cnt = localVariables.newFieldInRecord("pnd_Pge1_Cnt", "#PGE1-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_Pge2_Cnt = localVariables.newFieldInRecord("pnd_Pge2_Cnt", "#PGE2-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_Line_Cnt = localVariables.newFieldInRecord("pnd_Line_Cnt", "#LINE-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_Doc_Cnt = localVariables.newFieldInRecord("pnd_Doc_Cnt", "#DOC-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Hold_Unit = localVariables.newFieldInRecord("pnd_Hold_Unit", "#HOLD-UNIT", FieldType.STRING, 8);
        pnd_Hold_Doc_Type = localVariables.newFieldInRecord("pnd_Hold_Doc_Type", "#HOLD-DOC-TYPE", FieldType.STRING, 8);

        pnd_Hold_Doc_Type__R_Field_5 = localVariables.newGroupInRecord("pnd_Hold_Doc_Type__R_Field_5", "REDEFINE", pnd_Hold_Doc_Type);
        pnd_Hold_Doc_Type_Pnd_Hold_Cat = pnd_Hold_Doc_Type__R_Field_5.newFieldInGroup("pnd_Hold_Doc_Type_Pnd_Hold_Cat", "#HOLD-CAT", FieldType.STRING, 
            1);
        pnd_Hold_Doc_Type_Pnd_Hold_Sub_Cat = pnd_Hold_Doc_Type__R_Field_5.newFieldInGroup("pnd_Hold_Doc_Type_Pnd_Hold_Sub_Cat", "#HOLD-SUB-CAT", FieldType.STRING, 
            3);
        pnd_Hold_Doc_Type_Pnd_Hold_Detail = pnd_Hold_Doc_Type__R_Field_5.newFieldInGroup("pnd_Hold_Doc_Type_Pnd_Hold_Detail", "#HOLD-DETAIL", FieldType.STRING, 
            4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Doc_Type.reset();

        localVariables.reset();
        h10_H10b.setInitialValue(" EFM Document Statistics for Closed Work Requests for Period");
        h10_H10c.setInitialValue("00/00/0000 - 00/00/0000");
        h10_H10d.setInitialValue("           Page");
        h20_H20b.setInitialValue(" Document Sources by Unit ");
        h30_H30a.setInitialValue("Entry     Document         Document  ");
        h30_H30b.setInitialValue("  Document                            ------- Totals -------  ");
        h35_H35a.setInitialValue("Unit      Category           Type    ");
        h35_H35b.setInitialValue(" Description                         Text  Image   KDO   Total");
        h40_H40a.setInitialValue("--------  ---------------  --------  ");
        h40_H40b.setInitialValue(" ----------------------------       -----  -----  -----  -----");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Efsb2120() throws Exception
    {
        super("Efsb2120");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*  ===================================================================
        pdaCwfl5312.getPnd_Gen_Var_Pnd_Gen_Tbl_Key().setValue("A DOCUMENT-CATEGORY");                                                                                     //Natural: FORMAT ( 1 ) LS = 132 PS = 58;//Natural: FORMAT ( 2 ) LS = 132 PS = 58;//Natural: ASSIGN #GEN-TBL-KEY = 'A DOCUMENT-CATEGORY'
        //*  -----------------------------
        //*  -----------------------------                                                                                                                                //Natural: AT TOP OF PAGE ( 1 )
        //*  -----------------------------                                                                                                                                //Natural: AT TOP OF PAGE ( 2 )
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #WORK-RECORD
        while (condition(getWorkFiles().read(1, pnd_Work_Record)))
        {
            if (condition(pnd_Work_Record_Pnd_Work_Type.equals("10")))                                                                                                    //Natural: IF #WORK-TYPE = '10'
            {
                h10_H10_Dates.setValuesByName(pnd_Work_Record_Pnd_Input_Dates);                                                                                           //Natural: MOVE BY NAME #INPUT-DATES TO H10-DATES
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Doc_Cnt.equals(getZero())))                                                                                                                 //Natural: IF #DOC-CNT = 0
            {
                pnd_Hold_Unit.setValue(pnd_Work_Record_Pnd_Work_Unit_Entry);                                                                                              //Natural: MOVE #WORK-UNIT-ENTRY TO #HOLD-UNIT D50-UNIT
                d50_D50_Unit.setValue(pnd_Work_Record_Pnd_Work_Unit_Entry);
                pnd_Hold_Doc_Type.setValue(pnd_Work_Record_Pnd_Work_Doc_Type);                                                                                            //Natural: MOVE #WORK-DOC-TYPE TO #HOLD-DOC-TYPE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Doc_Cnt.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #DOC-CNT
            if (condition(pnd_Work_Record_Pnd_Work_Unit_Entry.notEquals(pnd_Hold_Unit)))                                                                                  //Natural: IF #WORK-UNIT-ENTRY NOT = #HOLD-UNIT
            {
                                                                                                                                                                          //Natural: PERFORM LINE-BREAK
                sub_Line_Break();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM CATEGORY-BREAK
                sub_Category_Break();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM UNIT-BREAK
                sub_Unit_Break();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Line_Cnt.greater(54)))                                                                                                                  //Natural: IF #LINE-CNT > 54
                {
                    getReports().newPage(new ReportSpecification(1));                                                                                                     //Natural: NEWPAGE ( 1 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Record_Pnd_Work_Cat.notEquals(pnd_Hold_Doc_Type_Pnd_Hold_Cat)))                                                                        //Natural: IF #WORK-CAT NOT = #HOLD-CAT
            {
                                                                                                                                                                          //Natural: PERFORM LINE-BREAK
                sub_Line_Break();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM CATEGORY-BREAK
                sub_Category_Break();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(! (pnd_Work_Record_Pnd_Work_Sub_Cat.equals(pnd_Hold_Doc_Type_Pnd_Hold_Sub_Cat) && pnd_Work_Record_Pnd_Work_Detail.equals(pnd_Hold_Doc_Type_Pnd_Hold_Detail)))) //Natural: IF NOT ( #WORK-SUB-CAT = #HOLD-SUB-CAT AND #WORK-DETAIL = #HOLD-DETAIL )
            {
                                                                                                                                                                          //Natural: PERFORM LINE-BREAK
                sub_Line_Break();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            d50_D50_Total.nadd(1);                                                                                                                                        //Natural: ADD 1 TO D50-TOTAL
            if (condition(pnd_Work_Record_Pnd_Work_Doc_Sub_Type.equals("T")))                                                                                             //Natural: IF #WORK-DOC-SUB-TYPE = 'T'
            {
                d50_D50_Text.nadd(1);                                                                                                                                     //Natural: ADD 1 TO D50-TEXT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Work_Record_Pnd_Work_Doc_Sub_Type.equals("I")))                                                                                         //Natural: IF #WORK-DOC-SUB-TYPE = 'I'
                {
                    d50_D50_Image.nadd(1);                                                                                                                                //Natural: ADD 1 TO D50-IMAGE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    d50_D50_Other.nadd(1);                                                                                                                                //Natural: ADD 1 TO D50-OTHER
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  -----------------------------
        //*  ==============================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LINE-BREAK
        //*  ==============================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CATEGORY-BREAK
        //*  ==============================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UNIT-BREAK
        //*  ==============================================
                                                                                                                                                                          //Natural: PERFORM LINE-BREAK
        sub_Line_Break();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CATEGORY-BREAK
        sub_Category_Break();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM UNIT-BREAK
        sub_Unit_Break();
        if (condition(Global.isEscape())) {return;}
        d50_D50_Unit.setValue("Totals");                                                                                                                                  //Natural: ASSIGN D50-UNIT = 'Totals'
        d50_D50_Text.setValue(pnd_Text_Tot.getValue(3));                                                                                                                  //Natural: ASSIGN D50-TEXT = #TEXT-TOT ( 3 )
        d50_D50_Image.setValue(pnd_Image_Tot.getValue(3));                                                                                                                //Natural: ASSIGN D50-IMAGE = #IMAGE-TOT ( 3 )
        d50_D50_Other.setValue(pnd_Other_Tot.getValue(3));                                                                                                                //Natural: ASSIGN D50-OTHER = #OTHER-TOT ( 3 )
        d50_D50_Total.setValue(pnd_Tot_Tot.getValue(3));                                                                                                                  //Natural: ASSIGN D50-TOTAL = #TOT-TOT ( 3 )
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,d50_D50_Unit,d50_D50_Cat,d50_D50_Type,d50_D50_Name,d50_D50_Text,d50_D50_Image,d50_D50_Other,                   //Natural: WRITE ( 1 ) / D50
            d50_D50_Total);
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,d50_D50_Unit,d50_D50_Cat,d50_D50_Type,d50_D50_Name,d50_D50_Text,d50_D50_Image,d50_D50_Other,                   //Natural: WRITE ( 2 ) / D50
            d50_D50_Total);
        if (Global.isEscape()) return;
        //*  ----------------------------------------
        getReports().write(0, NEWLINE,pnd_Doc_Cnt,"Documents");                                                                                                           //Natural: WRITE / #DOC-CNT 'Documents'
        if (Global.isEscape()) return;
    }
    private void sub_Line_Break() throws Exception                                                                                                                        //Natural: LINE-BREAK
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------
        d50_D50_Type.setValue(pnd_Hold_Doc_Type);                                                                                                                         //Natural: ASSIGN D50-TYPE = #HOLD-DOC-TYPE
        vw_cwf_Doc_Type.startDatabaseFind                                                                                                                                 //Natural: FIND ( 1 ) CWF-DOC-TYPE WITH DCMNT-TYPE-CDE-KEY = #HOLD-DOC-TYPE
        (
        "FIND01",
        new Wc[] { new Wc("DCMNT_TYPE_CDE_KEY", "=", pnd_Hold_Doc_Type, WcType.WITH) },
        1
        );
        FIND01:
        while (condition(vw_cwf_Doc_Type.readNextRow("FIND01")))
        {
            vw_cwf_Doc_Type.setIfNotFoundControlFlag(false);
            d50_D50_Name.setValue(cwf_Doc_Type_Dcmnt_Nme);                                                                                                                //Natural: ASSIGN D50-NAME = CWF-DOC-TYPE.DCMNT-NME
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        pnd_Text_Tot.getValue(1).nadd(d50_D50_Text);                                                                                                                      //Natural: ADD D50-TEXT TO #TEXT-TOT ( 1 )
        pnd_Image_Tot.getValue(1).nadd(d50_D50_Image);                                                                                                                    //Natural: ADD D50-IMAGE TO #IMAGE-TOT ( 1 )
        pnd_Other_Tot.getValue(1).nadd(d50_D50_Other);                                                                                                                    //Natural: ADD D50-OTHER TO #OTHER-TOT ( 1 )
        pnd_Tot_Tot.getValue(1).nadd(d50_D50_Total);                                                                                                                      //Natural: ADD D50-TOTAL TO #TOT-TOT ( 1 )
        getReports().write(1, ReportOption.NOTITLE,d50_D50_Unit,d50_D50_Cat,d50_D50_Type,d50_D50_Name,d50_D50_Text,d50_D50_Image,d50_D50_Other,d50_D50_Total);            //Natural: WRITE ( 1 ) D50
        if (Global.isEscape()) return;
        pnd_Line_Cnt.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #LINE-CNT
        d50_D50_Text.reset();                                                                                                                                             //Natural: RESET D50-TEXT D50-IMAGE D50-OTHER D50-TOTAL D50-UNIT
        d50_D50_Image.reset();
        d50_D50_Other.reset();
        d50_D50_Total.reset();
        d50_D50_Unit.reset();
        pnd_Hold_Doc_Type_Pnd_Hold_Sub_Cat.setValue(pnd_Work_Record_Pnd_Work_Sub_Cat);                                                                                    //Natural: ASSIGN #HOLD-SUB-CAT = #WORK-SUB-CAT
        pnd_Hold_Doc_Type_Pnd_Hold_Detail.setValue(pnd_Work_Record_Pnd_Work_Detail);                                                                                      //Natural: ASSIGN #HOLD-DETAIL = #WORK-DETAIL
        //*  LINE-BREAK
    }
    private void sub_Category_Break() throws Exception                                                                                                                    //Natural: CATEGORY-BREAK
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------
        pdaCwfl5312.getPnd_Gen_Var_Pnd_Gen_Tbl_Code().setValue(pnd_Hold_Doc_Type_Pnd_Hold_Cat);                                                                           //Natural: ASSIGN #GEN-TBL-CODE = #HOLD-CAT
        DbsUtil.callnat(Cwfn5312.class , getCurrentProcessState(), pdaCwfl5312.getPnd_Gen_Var_Pnd_Gen_Tbl_Key(), pdaCwfl5312.getPnd_Gen_Var_Pnd_Gen_Tbl_Desc());          //Natural: CALLNAT 'CWFN5312' #GEN-TBL-KEY #GEN-TBL-DESC
        if (condition(Global.isEscape())) return;
        d50_D50_Cat.setValue(pdaCwfl5312.getPnd_Gen_Var_Pnd_Gen_Tbl_Desc());                                                                                              //Natural: MOVE #GEN-TBL-DESC TO D50-CAT
        d50_D50_Text.setValue(pnd_Text_Tot.getValue(1));                                                                                                                  //Natural: ASSIGN D50-TEXT = #TEXT-TOT ( 1 )
        d50_D50_Image.setValue(pnd_Image_Tot.getValue(1));                                                                                                                //Natural: ASSIGN D50-IMAGE = #IMAGE-TOT ( 1 )
        d50_D50_Other.setValue(pnd_Other_Tot.getValue(1));                                                                                                                //Natural: ASSIGN D50-OTHER = #OTHER-TOT ( 1 )
        d50_D50_Total.setValue(pnd_Tot_Tot.getValue(1));                                                                                                                  //Natural: ASSIGN D50-TOTAL = #TOT-TOT ( 1 )
        pnd_Text_Tot.getValue(2).nadd(pnd_Text_Tot.getValue(1));                                                                                                          //Natural: ADD #TEXT-TOT ( 1 ) TO #TEXT-TOT ( 2 )
        pnd_Image_Tot.getValue(2).nadd(pnd_Image_Tot.getValue(1));                                                                                                        //Natural: ADD #IMAGE-TOT ( 1 ) TO #IMAGE-TOT ( 2 )
        pnd_Other_Tot.getValue(2).nadd(pnd_Other_Tot.getValue(1));                                                                                                        //Natural: ADD #OTHER-TOT ( 1 ) TO #OTHER-TOT ( 2 )
        pnd_Tot_Tot.getValue(2).nadd(pnd_Tot_Tot.getValue(1));                                                                                                            //Natural: ADD #TOT-TOT ( 1 ) TO #TOT-TOT ( 2 )
        d50_D50_Type.reset();                                                                                                                                             //Natural: RESET D50-TYPE D50-NAME
        d50_D50_Name.reset();
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,d50_D50_Unit,d50_D50_Cat,d50_D50_Type,d50_D50_Name,d50_D50_Text,d50_D50_Image,d50_D50_Other,                   //Natural: WRITE ( 1 ) / D50
            d50_D50_Total);
        if (Global.isEscape()) return;
        pnd_Line_Cnt.nadd(3);                                                                                                                                             //Natural: ADD 3 TO #LINE-CNT
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(107));                                                                                              //Natural: WRITE ( 1 ) '-' ( 107 )
        if (Global.isEscape()) return;
        d50_D50_Text.reset();                                                                                                                                             //Natural: RESET D50-TEXT D50-IMAGE D50-OTHER D50-TOTAL #TEXT-TOT ( 1 ) #IMAGE-TOT ( 1 ) #OTHER-TOT ( 1 ) #TOT-TOT ( 1 ) D50-CAT
        d50_D50_Image.reset();
        d50_D50_Other.reset();
        d50_D50_Total.reset();
        pnd_Text_Tot.getValue(1).reset();
        pnd_Image_Tot.getValue(1).reset();
        pnd_Other_Tot.getValue(1).reset();
        pnd_Tot_Tot.getValue(1).reset();
        d50_D50_Cat.reset();
        pnd_Hold_Doc_Type_Pnd_Hold_Cat.setValue(pnd_Work_Record_Pnd_Work_Cat);                                                                                            //Natural: ASSIGN #HOLD-CAT = #WORK-CAT
        //*  CATEGORY-BRAEK
    }
    private void sub_Unit_Break() throws Exception                                                                                                                        //Natural: UNIT-BREAK
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------
        d50_D50_Unit.setValue(pnd_Hold_Unit);                                                                                                                             //Natural: ASSIGN D50-UNIT = #HOLD-UNIT
        d50_D50_Text.setValue(pnd_Text_Tot.getValue(2));                                                                                                                  //Natural: ASSIGN D50-TEXT = #TEXT-TOT ( 2 )
        d50_D50_Image.setValue(pnd_Image_Tot.getValue(2));                                                                                                                //Natural: ASSIGN D50-IMAGE = #IMAGE-TOT ( 2 )
        d50_D50_Other.setValue(pnd_Other_Tot.getValue(2));                                                                                                                //Natural: ASSIGN D50-OTHER = #OTHER-TOT ( 2 )
        d50_D50_Total.setValue(pnd_Tot_Tot.getValue(2));                                                                                                                  //Natural: ASSIGN D50-TOTAL = #TOT-TOT ( 2 )
        pnd_Text_Tot.getValue(3).nadd(pnd_Text_Tot.getValue(2));                                                                                                          //Natural: ADD #TEXT-TOT ( 2 ) TO #TEXT-TOT ( 3 )
        pnd_Image_Tot.getValue(3).nadd(pnd_Image_Tot.getValue(2));                                                                                                        //Natural: ADD #IMAGE-TOT ( 2 ) TO #IMAGE-TOT ( 3 )
        pnd_Other_Tot.getValue(3).nadd(pnd_Other_Tot.getValue(2));                                                                                                        //Natural: ADD #OTHER-TOT ( 2 ) TO #OTHER-TOT ( 3 )
        pnd_Tot_Tot.getValue(3).nadd(pnd_Tot_Tot.getValue(2));                                                                                                            //Natural: ADD #TOT-TOT ( 2 ) TO #TOT-TOT ( 3 )
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,d50_D50_Unit,d50_D50_Cat,d50_D50_Type,d50_D50_Name,d50_D50_Text,d50_D50_Image,d50_D50_Other,                   //Natural: WRITE ( 1 ) / D50
            d50_D50_Total);
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"=",new RepeatItem(107));                                                                                              //Natural: WRITE ( 1 ) '=' ( 107 )
        if (Global.isEscape()) return;
        pnd_Line_Cnt.nadd(3);                                                                                                                                             //Natural: ADD 3 TO #LINE-CNT
        getReports().write(2, ReportOption.NOTITLE,d50_D50_Unit,d50_D50_Cat,d50_D50_Type,d50_D50_Name,d50_D50_Text,d50_D50_Image,d50_D50_Other,d50_D50_Total);            //Natural: WRITE ( 2 ) D50
        if (Global.isEscape()) return;
        d50_D50_Text.reset();                                                                                                                                             //Natural: RESET D50-TEXT D50-IMAGE D50-OTHER D50-TOTAL #TEXT-TOT ( 2 ) #IMAGE-TOT ( 2 ) #OTHER-TOT ( 2 ) #TOT-TOT ( 2 )
        d50_D50_Image.reset();
        d50_D50_Other.reset();
        d50_D50_Total.reset();
        pnd_Text_Tot.getValue(2).reset();
        pnd_Image_Tot.getValue(2).reset();
        pnd_Other_Tot.getValue(2).reset();
        pnd_Tot_Tot.getValue(2).reset();
        pnd_Hold_Unit.setValue(pnd_Work_Record_Pnd_Work_Unit_Entry);                                                                                                      //Natural: MOVE #WORK-UNIT-ENTRY TO #HOLD-UNIT D50-UNIT
        d50_D50_Unit.setValue(pnd_Work_Record_Pnd_Work_Unit_Entry);
        //*  UNIT-BREAK
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Pge1_Cnt.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #PGE1-CNT
                    h10_H10pge.setValue(pnd_Pge1_Cnt);                                                                                                                    //Natural: MOVE #PGE1-CNT TO H10PGE
                    getReports().write(1, ReportOption.NOTITLE,Global.getDATU(),h10_H10a,h10_H10b,h10_H10c,h10_H10d,h10_H10pge);                                          //Natural: WRITE ( 1 ) NOTITLE *DATU H10
                    getReports().write(1, ReportOption.NOTITLE,Global.getTIMX(),h20_H20a,h20_H20b,Global.getPROGRAM());                                                   //Natural: WRITE ( 1 ) *TIMX H20 *PROGRAM
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,h30_H30a,h30_H30b);                                                                                //Natural: WRITE ( 1 ) / H30
                    getReports().write(1, ReportOption.NOTITLE,h35_H35a,h35_H35b);                                                                                        //Natural: WRITE ( 1 ) H35
                    getReports().write(1, ReportOption.NOTITLE,h40_H40a,h40_H40b);                                                                                        //Natural: WRITE ( 1 ) H40
                    pnd_Line_Cnt.setValue(6);                                                                                                                             //Natural: MOVE 6 TO #LINE-CNT
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Pge2_Cnt.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #PGE2-CNT
                    h10_H10pge.setValue(pnd_Pge2_Cnt);                                                                                                                    //Natural: MOVE #PGE2-CNT TO H10PGE
                    getReports().write(2, ReportOption.NOTITLE,Global.getDATU(),h10_H10a,h10_H10b,h10_H10c,h10_H10d,h10_H10pge);                                          //Natural: WRITE ( 2 ) NOTITLE *DATU H10
                    getReports().write(2, ReportOption.NOTITLE,Global.getTIMX(),h20_H20a,h20_H20b,Global.getPROGRAM());                                                   //Natural: WRITE ( 2 ) *TIMX H20 *PROGRAM
                    getReports().write(2, ReportOption.NOTITLE,NEWLINE,h30_H30a,h30_H30b);                                                                                //Natural: WRITE ( 2 ) / H30
                    getReports().write(2, ReportOption.NOTITLE,h35_H35a,h35_H35b);                                                                                        //Natural: WRITE ( 2 ) H35
                    getReports().write(2, ReportOption.NOTITLE,h40_H40a,h40_H40b);                                                                                        //Natural: WRITE ( 2 ) H40
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=58");
        Global.format(2, "LS=132 PS=58");
    }
}
