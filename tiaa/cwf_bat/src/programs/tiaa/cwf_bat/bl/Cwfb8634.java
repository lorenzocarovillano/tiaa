/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:46:24 PM
**        * FROM NATURAL PROGRAM : Cwfb8634
************************************************************
**        * FILE NAME            : Cwfb8634.java
**        * CLASS NAME           : Cwfb8634
**        * INSTANCE NAME        : Cwfb8634
************************************************************
************************************************************************
* SYSTEM   : CORPORATE WORKFLOW
* PROGRAM  : CWFB8634
* TITLE    : WPID / TASK TYPE TRANSLATION EXTRACT
* FUNCTION : READ CWF-WP-WORK-PRCSS-ID (045/182) TO CREATE
*          : WEEKLY WPID/TASK TYPE EXTRACT
* HISTORY  : CREATED ON JULY 26 2012 - VINODHKUMAR RAMACHANDRAN
************************************************************************
* EXTRACTS ALL THE ACTIVE WPIDS AND WRITE INTO A WORK FILE WITH THE
* FOLLOWING FIELDS DELIMITED BY COMMA (,)
*                  - WPID
*                  - WPID DESCRIPTION
*                  - PI TASK TYPE
*                  - PI INDICATOR
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb8634 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Wpid;
    private DbsField cwf_Wpid_Work_Prcss_Id;
    private DbsField cwf_Wpid_Actve_Ind;
    private DbsField cwf_Wpid_Dlte_Oprtr_Cde;
    private DbsField cwf_Wpid_Work_Prcss_Long_Nme;
    private DbsField cwf_Wpid_Dlte_Dte_Tme;
    private DbsField cwf_Wpid_Pi_Task_Type;
    private DbsField cwf_Wpid_Pi_Ind;
    private DbsField pnd_X;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Wpid = new DataAccessProgramView(new NameInfo("vw_cwf_Wpid", "CWF-WPID"), "CWF_WP_WORK_PRCSS_ID", "CWF_PROFILE");
        cwf_Wpid_Work_Prcss_Id = vw_cwf_Wpid.getRecord().newFieldInGroup("cwf_Wpid_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        cwf_Wpid_Work_Prcss_Id.setDdmHeader("WORK PRCSS ID");
        cwf_Wpid_Actve_Ind = vw_cwf_Wpid.getRecord().newFieldInGroup("cwf_Wpid_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTVE_IND");
        cwf_Wpid_Dlte_Oprtr_Cde = vw_cwf_Wpid.getRecord().newFieldInGroup("cwf_Wpid_Dlte_Oprtr_Cde", "DLTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "DLTE_OPRTR_CDE");
        cwf_Wpid_Dlte_Oprtr_Cde.setDdmHeader("DELETE/OPER");
        cwf_Wpid_Work_Prcss_Long_Nme = vw_cwf_Wpid.getRecord().newFieldInGroup("cwf_Wpid_Work_Prcss_Long_Nme", "WORK-PRCSS-LONG-NME", FieldType.STRING, 
            45, RepeatingFieldStrategy.None, "WORK_PRCSS_LONG_NME");
        cwf_Wpid_Work_Prcss_Long_Nme.setDdmHeader("NAME");
        cwf_Wpid_Dlte_Dte_Tme = vw_cwf_Wpid.getRecord().newFieldInGroup("cwf_Wpid_Dlte_Dte_Tme", "DLTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "DLTE_DTE_TME");
        cwf_Wpid_Dlte_Dte_Tme.setDdmHeader("DELETE/DT-TM");
        cwf_Wpid_Pi_Task_Type = vw_cwf_Wpid.getRecord().newFieldInGroup("cwf_Wpid_Pi_Task_Type", "PI-TASK-TYPE", FieldType.STRING, 12, RepeatingFieldStrategy.None, 
            "PI_TASK_TYPE");
        cwf_Wpid_Pi_Ind = vw_cwf_Wpid.getRecord().newFieldInGroup("cwf_Wpid_Pi_Ind", "PI-IND", FieldType.STRING, 10, RepeatingFieldStrategy.None, "PI_IND");
        registerRecord(vw_cwf_Wpid);

        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Wpid.reset();

        localVariables.reset();
        pnd_X.setInitialValue(",");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb8634() throws Exception
    {
        super("Cwfb8634");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        //* ***********************************************************************
        vw_cwf_Wpid.startDatabaseRead                                                                                                                                     //Natural: READ CWF-WPID BY WPID-UNIQ-KEY
        (
        "READ01",
        new Oc[] { new Oc("WPID_UNIQ_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_cwf_Wpid.readNextRow("READ01")))
        {
            if (condition(cwf_Wpid_Dlte_Oprtr_Cde.notEquals(" ") || cwf_Wpid_Dlte_Dte_Tme.notEquals(getZero())))                                                          //Natural: IF DLTE-OPRTR-CDE NE ' ' OR DLTE-DTE-TME NE 0
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(1, false, cwf_Wpid_Work_Prcss_Id, pnd_X, cwf_Wpid_Work_Prcss_Long_Nme, pnd_X, cwf_Wpid_Pi_Task_Type, pnd_X, cwf_Wpid_Pi_Ind);            //Natural: WRITE WORK FILE 1 WORK-PRCSS-ID #X WORK-PRCSS-LONG-NME #X PI-TASK-TYPE #X PI-IND
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //
}
