/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:30:54 PM
**        * FROM NATURAL PROGRAM : Cwfb3906
************************************************************
**        * FILE NAME            : Cwfb3906.java
**        * CLASS NAME           : Cwfb3906
**        * INSTANCE NAME        : Cwfb3906
************************************************************
************************************************************************
* PROGRAM  : CWFB3906
* SYSTEM   : CRPCWF
* TITLE    : NEW EXTRACT (ONE LOOP METHOD)
* GENERATED: OCT 05,93 AT 08:41 AM
* FUNCTION : EXTRACT ALL OPEN MASTER INDEX RECORDS
*          | (ACTIVE AND CRPRTE STATUS IND NE 9)
*
* HISTORY
* --------
* 11/22/06     : GH REVISED THE RECORD SELECTION LOGIC.
* APR 23, 2014 : VR CHANGED THE KEY IN THE READ STATEMENT TO FETCH
*                WORK REQUESTS BASED ON A DATE RANGE.
*                INSTEAD OF READING THE WHOLE MASTER INDEX FILE
*                NOW READING ONLY 3 YEAR's worth of data only to
*                MINIMIZE THE CPU USAGE BY THIS JOB & PROGRAM
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3906 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Master_Index;
    private DbsField cwf_Master_Index_Pin_Nbr;
    private DbsField cwf_Master_Index_Rqst_Log_Dte_Tme;

    private DbsGroup cwf_Master_Index__R_Field_1;
    private DbsField cwf_Master_Index_Rqst_Log_Index_Dte;

    private DbsGroup cwf_Master_Index__R_Field_2;
    private DbsField cwf_Master_Index_Rqst_Log_Index_Dte_N;
    private DbsField cwf_Master_Index_Rqst_Log_Index_Tme;
    private DbsField cwf_Master_Index_Rqst_Log_Oprtr_Cde;
    private DbsField cwf_Master_Index_Rqst_Orgn_Cde;
    private DbsField cwf_Master_Index_Orgnl_Log_Dte_Tme;
    private DbsField cwf_Master_Index_Orgnl_Unit_Cde;
    private DbsField cwf_Master_Index_Work_Prcss_Id;
    private DbsField cwf_Master_Index_Unit_Cde;
    private DbsField cwf_Master_Index_Unit_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_Empl_Oprtr_Cde;
    private DbsField cwf_Master_Index_Last_Chnge_Dte_Tme;
    private DbsField cwf_Master_Index_Last_Chnge_Oprtr_Cde;
    private DbsField cwf_Master_Index_Last_Chnge_Unit_Cde;
    private DbsField cwf_Master_Index_Step_Id;
    private DbsField cwf_Master_Index_Admin_Unit_Cde;
    private DbsField cwf_Master_Index_Admin_Status_Cde;
    private DbsField cwf_Master_Index_Admin_Status_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_Admin_Status_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Index_Status_Cde;
    private DbsField cwf_Master_Index_Status_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_Status_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Index_Last_Updte_Dte;
    private DbsField cwf_Master_Index_Last_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_Last_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Index_Actve_Ind;
    private DbsField cwf_Master_Index_Crprte_Status_Ind;
    private DbsField cwf_Master_Index_Tiaa_Rcvd_Dte;
    private DbsField cwf_Master_Index_Effctve_Dte;
    private DbsField cwf_Master_Index_Trans_Dte;
    private DbsField cwf_Master_Index_Mj_Pull_Ind;
    private DbsField cwf_Master_Index_Physcl_Fldr_Id_Nbr;
    private DbsGroup cwf_Master_Index_Cntrct_NbrMuGroup;
    private DbsField cwf_Master_Index_Cntrct_Nbr;
    private DbsField cwf_Master_Index_Extrnl_Pend_Rcv_Dte;
    private DbsField cwf_Master_Index_Work_List_Ind;
    private DbsField cwf_Master_Index_Due_Dte_Chg_Prty_Cde;

    private DbsGroup cwf_Master_Index__R_Field_3;
    private DbsField cwf_Master_Index_Due_Dte;
    private DbsField cwf_Master_Index_Due_Dte_Chg_Ind;
    private DbsField cwf_Master_Index_Due_Dte_Prty_Cde;
    private DbsField cwf_Master_Index_Due_Dte_Filler;
    private DbsField cwf_Master_Index_Bsnss_Reply_Ind;
    private DbsField cwf_Master_Index_Status_Freeze_Ind;
    private DbsField cwf_Master_Index_Elctrnc_Fldr_Ind;
    private DbsField cwf_Master_Index_Unit_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Index_Unit_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Index_Empl_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Index_Empl_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Index_Intrnl_Pnd_Start_Dte_Tme;
    private DbsField cwf_Master_Index_Intrnl_Pnd_End_Dte_Tme;
    private DbsField cwf_Master_Index_Intrnl_Pnd_Days;
    private DbsField cwf_Master_Index_Step_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Index_Step_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Index_Crprte_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Index_Unit_En_Rte_To_Dte_Tme;

    private DbsGroup extract_Record;
    private DbsField extract_Record_Pin_Nbr;
    private DbsField extract_Record_Rqst_Log_Dte_Tme;

    private DbsGroup extract_Record__R_Field_4;
    private DbsField extract_Record_Rqst_Log_Index_Dte;
    private DbsField extract_Record_Rqst_Log_Index_Tme;
    private DbsField extract_Record_Rqst_Log_Oprtr_Cde;
    private DbsField extract_Record_Orgnl_Log_Dte_Tme;
    private DbsField extract_Record_Orgnl_Unit_Cde;
    private DbsField extract_Record_Work_Prcss_Id;

    private DbsGroup extract_Record__R_Field_5;
    private DbsField extract_Record_Work_Actn_Rqstd_Cde;
    private DbsField extract_Record_Work_Lob_Cmpny_Prdct_Cde;
    private DbsField extract_Record_Work_Mjr_Bsnss_Prcss_Cde;
    private DbsField extract_Record_Work_Spcfc_Bsnss_Prcss_Cde;
    private DbsField extract_Record_Unit_Cde;

    private DbsGroup extract_Record__R_Field_6;
    private DbsField extract_Record_Unit_Id_Cde;
    private DbsField extract_Record_Unit_Rgn_Cde;
    private DbsField extract_Record_Unit_Spcl_Dsgntn_Cde;
    private DbsField extract_Record_Unit_Brnch_Group_Cde;
    private DbsField extract_Record_Unit_Updte_Dte_Tme;
    private DbsField extract_Record_Empl_Oprtr_Cde;

    private DbsGroup extract_Record__R_Field_7;
    private DbsField extract_Record_Empl_Racf_Id;
    private DbsField extract_Record_Empl_Sffx_Cde;
    private DbsField extract_Record_Last_Chnge_Dte_Tme;
    private DbsField extract_Record_Last_Chnge_Oprtr_Cde;
    private DbsField extract_Record_Last_Chnge_Unit_Cde;
    private DbsField extract_Record_Step_Id;
    private DbsField extract_Record_Admin_Unit_Cde;
    private DbsField extract_Record_Admin_Status_Cde;
    private DbsField extract_Record_Admin_Status_Updte_Dte_Tme;
    private DbsField extract_Record_Admin_Status_Updte_Oprtr_Cde;
    private DbsField extract_Record_Status_Cde;
    private DbsField extract_Record_Status_Updte_Dte_Tme;
    private DbsField extract_Record_Status_Updte_Oprtr_Cde;
    private DbsField extract_Record_Last_Updte_Dte;
    private DbsField extract_Record_Last_Updte_Dte_Tme;
    private DbsField extract_Record_Last_Updte_Oprtr_Cde;
    private DbsField extract_Record_Crprte_Status_Ind;
    private DbsField extract_Record_Tiaa_Rcvd_Dte;
    private DbsField extract_Record_Effctve_Dte;
    private DbsField extract_Record_Trans_Dte;
    private DbsField extract_Record_Cntrct_Nbr;
    private DbsField extract_Record_Pnd_Calendar_Days_In_Tiaa;
    private DbsField extract_Record_Pnd_Business_Days_In_Tiaa;
    private DbsField extract_Record_Extrnl_Pend_Rcv_Dte;
    private DbsField extract_Record_Pnd_Received_In_Unit;
    private DbsField extract_Record_Pnd_Bsnss_Days_In_Unit;
    private DbsField extract_Record_Pnd_Intrnl_Pend_Days;
    private DbsField extract_Record_Pnd_Extrnl_Pend_Days;
    private DbsField extract_Record_Pnd_Extrnl_Pend_Bsnss_Days;
    private DbsField extract_Record_Pnd_Partic_Sname;

    private DbsGroup extract_Record__R_Field_8;
    private DbsField extract_Record_Pnd_Folder_Prefix;
    private DbsField extract_Record_Pnd_Folder_Nbr;
    private DbsField extract_Record_Work_List_Ind;
    private DbsField pnd_Env;
    private DbsField pnd_Read_Ctr;
    private DbsField pnd_Conv_Ctr;
    private DbsField pnd_Nconv_Ctr;
    private DbsField pnd_Rject_Ctr;
    private DbsField pnd_Total_Ctr;
    private DbsField pnd_No_Extract;
    private DbsField pnd_Report_Parm;
    private DbsField pnd_Racf_Id;
    private DbsField pnd_Parm_Unit;
    private DbsField pnd_Floor;
    private DbsField pnd_Bldg;
    private DbsField pnd_Drop_Off;
    private DbsField pnd_Tbl_Run_Date;
    private DbsField pnd_Tbl_Run_Flag;
    private DbsField pnd_Comp_Date;
    private DbsField pnd_Start_Date_A;
    private DbsField pnd_Base_Date;
    private DbsField pnd_Tbl_Key;

    private DbsGroup pnd_Tbl_Key__R_Field_9;

    private DbsGroup pnd_Tbl_Key_Data_Nme;
    private DbsField pnd_Tbl_Key_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Key_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Key_Tbl_Key_Field;

    private DbsGroup pnd_Tbl_Prime_Key;
    private DbsField pnd_Tbl_Prime_Key_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Tbl_Key_Field;
    private DbsField pnd_Parm_Report_No;
    private DbsField pnd_Parm_Empl_Racf_Id;
    private DbsField pnd_Parm_Unit_Cde;
    private DbsField pnd_Parm_Floor;
    private DbsField pnd_Parm_Bldg;
    private DbsField pnd_Parm_Drop_Off;
    private DbsField pnd_Parm_Run_Date;

    private DbsGroup pnd_End_Var;
    private DbsField pnd_End_Var_Pnd_End_Tbl_Key;

    private DbsGroup pnd_End_Var__R_Field_10;
    private DbsField pnd_End_Var_Pnd_End_Tbl_Authrty;
    private DbsField pnd_End_Var_Pnd_End_Tbl_Name;
    private DbsField pnd_End_Var_Pnd_End_Tbl_Code;

    private DbsGroup pnd_Gen_Var;
    private DbsField pnd_Gen_Var_Pnd_Gen_Tbl_Key;

    private DbsGroup pnd_Gen_Var__R_Field_11;
    private DbsField pnd_Gen_Var_Pnd_Gen_Tbl_Authrty;
    private DbsField pnd_Gen_Var_Pnd_Gen_Tbl_Name;
    private DbsField pnd_Gen_Var_Pnd_Gen_Tbl_Code;
    private DbsField pnd_Gen_Var_Pnd_Gen_Tbl_Desc;
    private DbsField pnd_Parm_Type;
    private DbsField pnd_Sub;
    private DbsField pnd_Extrnl_Pend_Daysb;
    private DbsField pnd_Timx;
    private DbsField pnd_Receive_Date;
    private DbsField pnd_Report_Date;
    private DbsField pnd_Start_Date;
    private DbsField pnd_Report_Date_Time;
    private DbsField pnd_Intrnl_Pend_Date;
    private DbsField pnd_Unit_Start_Date_Time;
    private DbsField pnd_Intrnl_Pend_Daysx;
    private DbsField pnd_Bsnss_Days_In_Unitx;
    private DbsField pnd_Extrnl_Pend_Daysx;
    private DbsField pnd_No_Work_Days;
    private DbsField pnd_Pnd_Start_Date;

    private DbsGroup pnd_Pnd_Start_Date__R_Field_12;
    private DbsField pnd_Pnd_Start_Date_Pnd_Pnd_Start_Date_N;
    private DbsField pnd_Pnd_End_Date;

    private DbsGroup pnd_Pnd_End_Date__R_Field_13;
    private DbsField pnd_Pnd_End_Date_Pnd_Pnd_End_Date_N;
    private DbsField pnd_Work_Date;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Master_Index = new DataAccessProgramView(new NameInfo("vw_cwf_Master_Index", "CWF-MASTER-INDEX"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        cwf_Master_Index_Pin_Nbr = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "PIN_NBR");
        cwf_Master_Index_Pin_Nbr.setDdmHeader("PIN");
        cwf_Master_Index_Rqst_Log_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Master_Index_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");

        cwf_Master_Index__R_Field_1 = vw_cwf_Master_Index.getRecord().newGroupInGroup("cwf_Master_Index__R_Field_1", "REDEFINE", cwf_Master_Index_Rqst_Log_Dte_Tme);
        cwf_Master_Index_Rqst_Log_Index_Dte = cwf_Master_Index__R_Field_1.newFieldInGroup("cwf_Master_Index_Rqst_Log_Index_Dte", "RQST-LOG-INDEX-DTE", 
            FieldType.STRING, 8);

        cwf_Master_Index__R_Field_2 = cwf_Master_Index__R_Field_1.newGroupInGroup("cwf_Master_Index__R_Field_2", "REDEFINE", cwf_Master_Index_Rqst_Log_Index_Dte);
        cwf_Master_Index_Rqst_Log_Index_Dte_N = cwf_Master_Index__R_Field_2.newFieldInGroup("cwf_Master_Index_Rqst_Log_Index_Dte_N", "RQST-LOG-INDEX-DTE-N", 
            FieldType.NUMERIC, 8);
        cwf_Master_Index_Rqst_Log_Index_Tme = cwf_Master_Index__R_Field_1.newFieldInGroup("cwf_Master_Index_Rqst_Log_Index_Tme", "RQST-LOG-INDEX-TME", 
            FieldType.STRING, 7);
        cwf_Master_Index_Rqst_Log_Oprtr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_LOG_OPRTR_CDE");
        cwf_Master_Index_Rqst_Log_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Master_Index_Rqst_Orgn_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Rqst_Orgn_Cde", "RQST-ORGN-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RQST_ORGN_CDE");
        cwf_Master_Index_Orgnl_Log_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Orgnl_Log_Dte_Tme", "ORGNL-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "ORGNL_LOG_DTE_TME");
        cwf_Master_Index_Orgnl_Log_Dte_Tme.setDdmHeader("ORIGINAL/LOG DTE/TME");
        cwf_Master_Index_Orgnl_Unit_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ORGNL_UNIT_CDE");
        cwf_Master_Index_Orgnl_Unit_Cde.setDdmHeader("LOG/UNIT");
        cwf_Master_Index_Work_Prcss_Id = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        cwf_Master_Index_Work_Prcss_Id.setDdmHeader("WORK/ID");
        cwf_Master_Index_Unit_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "UNIT_CDE");
        cwf_Master_Index_Unit_Cde.setDdmHeader("UNIT/CODE");
        cwf_Master_Index_Unit_Updte_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Unit_Updte_Dte_Tme", "UNIT-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "UNIT_UPDTE_DTE_TME");
        cwf_Master_Index_Unit_Updte_Dte_Tme.setDdmHeader("UNIT UPDATE/DATE-TIME");
        cwf_Master_Index_Empl_Oprtr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "EMPL_OPRTR_CDE");
        cwf_Master_Index_Empl_Oprtr_Cde.setDdmHeader("EMPLOYEE/ID");
        cwf_Master_Index_Last_Chnge_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "LAST_CHNGE_DTE_TME");
        cwf_Master_Index_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");
        cwf_Master_Index_Last_Chnge_Oprtr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Last_Chnge_Oprtr_Cde", "LAST-CHNGE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_CHNGE_OPRTR_CDE");
        cwf_Master_Index_Last_Chnge_Oprtr_Cde.setDdmHeader("LOG CREATE/OPERATOR");
        cwf_Master_Index_Last_Chnge_Unit_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Last_Chnge_Unit_Cde", "LAST-CHNGE-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_CHNGE_UNIT_CDE");
        cwf_Master_Index_Last_Chnge_Unit_Cde.setDdmHeader("LAST/CHANGE/UNIT");
        cwf_Master_Index_Step_Id = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Step_Id", "STEP-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "STEP_ID");
        cwf_Master_Index_Step_Id.setDdmHeader("STEP/ID");
        cwf_Master_Index_Admin_Unit_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ADMIN_UNIT_CDE");
        cwf_Master_Index_Admin_Status_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "ADMIN_STATUS_CDE");
        cwf_Master_Index_Admin_Status_Updte_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Admin_Status_Updte_Dte_Tme", "ADMIN-STATUS-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_DTE_TME");
        cwf_Master_Index_Admin_Status_Updte_Oprtr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Admin_Status_Updte_Oprtr_Cde", 
            "ADMIN-STATUS-UPDTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_OPRTR_CDE");
        cwf_Master_Index_Status_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Status_Cde", "STATUS-CDE", FieldType.STRING, 4, 
            RepeatingFieldStrategy.None, "STATUS_CDE");
        cwf_Master_Index_Status_Updte_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Status_Updte_Dte_Tme", "STATUS-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "STATUS_UPDTE_DTE_TME");
        cwf_Master_Index_Status_Updte_Dte_Tme.setDdmHeader("STATUS UPDATE/DATE-TIME");
        cwf_Master_Index_Status_Updte_Oprtr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Status_Updte_Oprtr_Cde", "STATUS-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "STATUS_UPDTE_OPRTR_CDE");
        cwf_Master_Index_Last_Updte_Dte = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Last_Updte_Dte", "LAST-UPDTE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "LAST_UPDTE_DTE");
        cwf_Master_Index_Last_Updte_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Last_Updte_Dte_Tme", "LAST-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LAST_UPDTE_DTE_TME");
        cwf_Master_Index_Last_Updte_Oprtr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Last_Updte_Oprtr_Cde", "LAST-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_UPDTE_OPRTR_CDE");
        cwf_Master_Index_Actve_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTVE_IND");
        cwf_Master_Index_Crprte_Status_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Crprte_Status_Ind", "CRPRTE-STATUS-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CRPRTE_STATUS_IND");
        cwf_Master_Index_Crprte_Status_Ind.setDdmHeader("CORP/STAT");
        cwf_Master_Index_Tiaa_Rcvd_Dte = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TIAA_RCVD_DTE");
        cwf_Master_Index_Effctve_Dte = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Effctve_Dte", "EFFCTVE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "EFFCTVE_DTE");
        cwf_Master_Index_Trans_Dte = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Trans_Dte", "TRANS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        cwf_Master_Index_Trans_Dte.setDdmHeader("CHECK MAIL/DATE");
        cwf_Master_Index_Mj_Pull_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Mj_Pull_Ind", "MJ-PULL-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "MJ_PULL_IND");
        cwf_Master_Index_Mj_Pull_Ind.setDdmHeader("MJ PULL/IND");
        cwf_Master_Index_Physcl_Fldr_Id_Nbr = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Physcl_Fldr_Id_Nbr", "PHYSCL-FLDR-ID-NBR", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "PHYSCL_FLDR_ID_NBR");
        cwf_Master_Index_Physcl_Fldr_Id_Nbr.setDdmHeader("PHYSICAL FOLDER/ID NUMBER");
        cwf_Master_Index_Cntrct_NbrMuGroup = vw_cwf_Master_Index.getRecord().newGroupInGroup("CWF_MASTER_INDEX_CNTRCT_NBRMuGroup", "CNTRCT_NBRMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "CWF_MASTER_INDEX_CNTRCT_NBR");
        cwf_Master_Index_Cntrct_Nbr = cwf_Master_Index_Cntrct_NbrMuGroup.newFieldArrayInGroup("cwf_Master_Index_Cntrct_Nbr", "CNTRCT-NBR", FieldType.STRING, 
            8, new DbsArrayController(1, 1), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_NBR");
        cwf_Master_Index_Extrnl_Pend_Rcv_Dte = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Extrnl_Pend_Rcv_Dte", "EXTRNL-PEND-RCV-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "EXTRNL_PEND_RCV_DTE");
        cwf_Master_Index_Extrnl_Pend_Rcv_Dte.setDdmHeader("LAST UNPEND DATE");
        cwf_Master_Index_Work_List_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Work_List_Ind", "WORK-LIST-IND", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "WORK_LIST_IND");
        cwf_Master_Index_Work_List_Ind.setDdmHeader("WORK LIST INDICATOR");
        cwf_Master_Index_Due_Dte_Chg_Prty_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Due_Dte_Chg_Prty_Cde", "DUE-DTE-CHG-PRTY-CDE", 
            FieldType.STRING, 16, RepeatingFieldStrategy.None, "DUE_DTE_CHG_PRTY_CDE");
        cwf_Master_Index_Due_Dte_Chg_Prty_Cde.setDdmHeader("DUE DATE CHANGE IND. PRIORITY");

        cwf_Master_Index__R_Field_3 = vw_cwf_Master_Index.getRecord().newGroupInGroup("cwf_Master_Index__R_Field_3", "REDEFINE", cwf_Master_Index_Due_Dte_Chg_Prty_Cde);
        cwf_Master_Index_Due_Dte = cwf_Master_Index__R_Field_3.newFieldInGroup("cwf_Master_Index_Due_Dte", "DUE-DTE", FieldType.STRING, 8);
        cwf_Master_Index_Due_Dte_Chg_Ind = cwf_Master_Index__R_Field_3.newFieldInGroup("cwf_Master_Index_Due_Dte_Chg_Ind", "DUE-DTE-CHG-IND", FieldType.STRING, 
            1);
        cwf_Master_Index_Due_Dte_Prty_Cde = cwf_Master_Index__R_Field_3.newFieldInGroup("cwf_Master_Index_Due_Dte_Prty_Cde", "DUE-DTE-PRTY-CDE", FieldType.STRING, 
            1);
        cwf_Master_Index_Due_Dte_Filler = cwf_Master_Index__R_Field_3.newFieldInGroup("cwf_Master_Index_Due_Dte_Filler", "DUE-DTE-FILLER", FieldType.STRING, 
            6);
        cwf_Master_Index_Bsnss_Reply_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Bsnss_Reply_Ind", "BSNSS-REPLY-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BSNSS_REPLY_IND");
        cwf_Master_Index_Bsnss_Reply_Ind.setDdmHeader("BUSINESS/REPLY");
        cwf_Master_Index_Status_Freeze_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Status_Freeze_Ind", "STATUS-FREEZE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "STATUS_FREEZE_IND");
        cwf_Master_Index_Status_Freeze_Ind.setDdmHeader("FREEZE/STATUS");
        cwf_Master_Index_Elctrnc_Fldr_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Elctrnc_Fldr_Ind", "ELCTRNC-FLDR-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ELCTRNC_FLDR_IND");
        cwf_Master_Index_Elctrnc_Fldr_Ind.setDdmHeader("ELECTRONIC/FOLDER IND");
        cwf_Master_Index_Unit_Clock_Start_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Unit_Clock_Start_Dte_Tme", "UNIT-CLOCK-START-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "UNIT_CLOCK_START_DTE_TME");
        cwf_Master_Index_Unit_Clock_Start_Dte_Tme.setDdmHeader("UNIT CLOCK START DATE TIME");
        cwf_Master_Index_Unit_Clock_End_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Unit_Clock_End_Dte_Tme", "UNIT-CLOCK-END-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "UNIT_CLOCK_END_DTE_TME");
        cwf_Master_Index_Unit_Clock_End_Dte_Tme.setDdmHeader("UNIT CLOCK END DATE TIME");
        cwf_Master_Index_Empl_Clock_Start_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Empl_Clock_Start_Dte_Tme", "EMPL-CLOCK-START-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "EMPL_CLOCK_START_DTE_TME");
        cwf_Master_Index_Empl_Clock_Start_Dte_Tme.setDdmHeader("EMPLOYEE CLOCK START DATE TIME");
        cwf_Master_Index_Empl_Clock_End_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Empl_Clock_End_Dte_Tme", "EMPL-CLOCK-END-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "EMPL_CLOCK_END_DTE_TME");
        cwf_Master_Index_Empl_Clock_End_Dte_Tme.setDdmHeader("EMPLOYEE CLOCK END DATE TIME");
        cwf_Master_Index_Intrnl_Pnd_Start_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Intrnl_Pnd_Start_Dte_Tme", "INTRNL-PND-START-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "INTRNL_PND_START_DTE_TME");
        cwf_Master_Index_Intrnl_Pnd_Start_Dte_Tme.setDdmHeader("ACKNOWLEDGEMENT DATE TIME");
        cwf_Master_Index_Intrnl_Pnd_End_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Intrnl_Pnd_End_Dte_Tme", "INTRNL-PND-END-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "INTRNL_PND_END_DTE_TME");
        cwf_Master_Index_Intrnl_Pnd_End_Dte_Tme.setDdmHeader("INTERNAL PEND END DATE TIME");
        cwf_Master_Index_Intrnl_Pnd_Days = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Intrnl_Pnd_Days", "INTRNL-PND-DAYS", FieldType.PACKED_DECIMAL, 
            5, RepeatingFieldStrategy.None, "INTRNL_PND_DAYS");
        cwf_Master_Index_Intrnl_Pnd_Days.setDdmHeader("INTERNAL PEND DAYS");
        cwf_Master_Index_Step_Clock_Start_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Step_Clock_Start_Dte_Tme", "STEP-CLOCK-START-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "STEP_CLOCK_START_DTE_TME");
        cwf_Master_Index_Step_Clock_Start_Dte_Tme.setDdmHeader("STEP START DATE TIME");
        cwf_Master_Index_Step_Clock_End_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Step_Clock_End_Dte_Tme", "STEP-CLOCK-END-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "STEP_CLOCK_END_DTE_TME");
        cwf_Master_Index_Step_Clock_End_Dte_Tme.setDdmHeader("STEP CLOCK END DATE TIME");
        cwf_Master_Index_Crprte_Clock_End_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Crprte_Clock_End_Dte_Tme", "CRPRTE-CLOCK-END-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "CRPRTE_CLOCK_END_DTE_TME");
        cwf_Master_Index_Crprte_Clock_End_Dte_Tme.setDdmHeader("CORPORATE CLOCK END DATE TIME");
        cwf_Master_Index_Unit_En_Rte_To_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Unit_En_Rte_To_Dte_Tme", "UNIT-EN-RTE-TO-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "UNIT_EN_RTE_TO_DTE_TME");
        cwf_Master_Index_Unit_En_Rte_To_Dte_Tme.setDdmHeader("UNIT EN ROUTE TO DATE TIME");
        registerRecord(vw_cwf_Master_Index);

        extract_Record = localVariables.newGroupInRecord("extract_Record", "EXTRACT-RECORD");
        extract_Record_Pin_Nbr = extract_Record.newFieldInGroup("extract_Record_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12);
        extract_Record_Rqst_Log_Dte_Tme = extract_Record.newFieldInGroup("extract_Record_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15);

        extract_Record__R_Field_4 = extract_Record.newGroupInGroup("extract_Record__R_Field_4", "REDEFINE", extract_Record_Rqst_Log_Dte_Tme);
        extract_Record_Rqst_Log_Index_Dte = extract_Record__R_Field_4.newFieldInGroup("extract_Record_Rqst_Log_Index_Dte", "RQST-LOG-INDEX-DTE", FieldType.STRING, 
            8);
        extract_Record_Rqst_Log_Index_Tme = extract_Record__R_Field_4.newFieldInGroup("extract_Record_Rqst_Log_Index_Tme", "RQST-LOG-INDEX-TME", FieldType.STRING, 
            7);
        extract_Record_Rqst_Log_Oprtr_Cde = extract_Record.newFieldInGroup("extract_Record_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", FieldType.STRING, 
            8);
        extract_Record_Orgnl_Log_Dte_Tme = extract_Record.newFieldInGroup("extract_Record_Orgnl_Log_Dte_Tme", "ORGNL-LOG-DTE-TME", FieldType.STRING, 15);
        extract_Record_Orgnl_Unit_Cde = extract_Record.newFieldInGroup("extract_Record_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", FieldType.STRING, 8);
        extract_Record_Work_Prcss_Id = extract_Record.newFieldInGroup("extract_Record_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6);

        extract_Record__R_Field_5 = extract_Record.newGroupInGroup("extract_Record__R_Field_5", "REDEFINE", extract_Record_Work_Prcss_Id);
        extract_Record_Work_Actn_Rqstd_Cde = extract_Record__R_Field_5.newFieldInGroup("extract_Record_Work_Actn_Rqstd_Cde", "WORK-ACTN-RQSTD-CDE", FieldType.STRING, 
            1);
        extract_Record_Work_Lob_Cmpny_Prdct_Cde = extract_Record__R_Field_5.newFieldInGroup("extract_Record_Work_Lob_Cmpny_Prdct_Cde", "WORK-LOB-CMPNY-PRDCT-CDE", 
            FieldType.STRING, 2);
        extract_Record_Work_Mjr_Bsnss_Prcss_Cde = extract_Record__R_Field_5.newFieldInGroup("extract_Record_Work_Mjr_Bsnss_Prcss_Cde", "WORK-MJR-BSNSS-PRCSS-CDE", 
            FieldType.STRING, 1);
        extract_Record_Work_Spcfc_Bsnss_Prcss_Cde = extract_Record__R_Field_5.newFieldInGroup("extract_Record_Work_Spcfc_Bsnss_Prcss_Cde", "WORK-SPCFC-BSNSS-PRCSS-CDE", 
            FieldType.STRING, 2);
        extract_Record_Unit_Cde = extract_Record.newFieldInGroup("extract_Record_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8);

        extract_Record__R_Field_6 = extract_Record.newGroupInGroup("extract_Record__R_Field_6", "REDEFINE", extract_Record_Unit_Cde);
        extract_Record_Unit_Id_Cde = extract_Record__R_Field_6.newFieldInGroup("extract_Record_Unit_Id_Cde", "UNIT-ID-CDE", FieldType.STRING, 5);
        extract_Record_Unit_Rgn_Cde = extract_Record__R_Field_6.newFieldInGroup("extract_Record_Unit_Rgn_Cde", "UNIT-RGN-CDE", FieldType.STRING, 1);
        extract_Record_Unit_Spcl_Dsgntn_Cde = extract_Record__R_Field_6.newFieldInGroup("extract_Record_Unit_Spcl_Dsgntn_Cde", "UNIT-SPCL-DSGNTN-CDE", 
            FieldType.STRING, 1);
        extract_Record_Unit_Brnch_Group_Cde = extract_Record__R_Field_6.newFieldInGroup("extract_Record_Unit_Brnch_Group_Cde", "UNIT-BRNCH-GROUP-CDE", 
            FieldType.STRING, 1);
        extract_Record_Unit_Updte_Dte_Tme = extract_Record.newFieldInGroup("extract_Record_Unit_Updte_Dte_Tme", "UNIT-UPDTE-DTE-TME", FieldType.TIME);
        extract_Record_Empl_Oprtr_Cde = extract_Record.newFieldInGroup("extract_Record_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", FieldType.STRING, 10);

        extract_Record__R_Field_7 = extract_Record.newGroupInGroup("extract_Record__R_Field_7", "REDEFINE", extract_Record_Empl_Oprtr_Cde);
        extract_Record_Empl_Racf_Id = extract_Record__R_Field_7.newFieldInGroup("extract_Record_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 8);
        extract_Record_Empl_Sffx_Cde = extract_Record__R_Field_7.newFieldInGroup("extract_Record_Empl_Sffx_Cde", "EMPL-SFFX-CDE", FieldType.STRING, 2);
        extract_Record_Last_Chnge_Dte_Tme = extract_Record.newFieldInGroup("extract_Record_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 
            15);
        extract_Record_Last_Chnge_Oprtr_Cde = extract_Record.newFieldInGroup("extract_Record_Last_Chnge_Oprtr_Cde", "LAST-CHNGE-OPRTR-CDE", FieldType.STRING, 
            8);
        extract_Record_Last_Chnge_Unit_Cde = extract_Record.newFieldInGroup("extract_Record_Last_Chnge_Unit_Cde", "LAST-CHNGE-UNIT-CDE", FieldType.STRING, 
            8);
        extract_Record_Step_Id = extract_Record.newFieldInGroup("extract_Record_Step_Id", "STEP-ID", FieldType.STRING, 6);
        extract_Record_Admin_Unit_Cde = extract_Record.newFieldInGroup("extract_Record_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 8);
        extract_Record_Admin_Status_Cde = extract_Record.newFieldInGroup("extract_Record_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 4);
        extract_Record_Admin_Status_Updte_Dte_Tme = extract_Record.newFieldInGroup("extract_Record_Admin_Status_Updte_Dte_Tme", "ADMIN-STATUS-UPDTE-DTE-TME", 
            FieldType.TIME);
        extract_Record_Admin_Status_Updte_Oprtr_Cde = extract_Record.newFieldInGroup("extract_Record_Admin_Status_Updte_Oprtr_Cde", "ADMIN-STATUS-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8);
        extract_Record_Status_Cde = extract_Record.newFieldInGroup("extract_Record_Status_Cde", "STATUS-CDE", FieldType.STRING, 4);
        extract_Record_Status_Updte_Dte_Tme = extract_Record.newFieldInGroup("extract_Record_Status_Updte_Dte_Tme", "STATUS-UPDTE-DTE-TME", FieldType.TIME);
        extract_Record_Status_Updte_Oprtr_Cde = extract_Record.newFieldInGroup("extract_Record_Status_Updte_Oprtr_Cde", "STATUS-UPDTE-OPRTR-CDE", FieldType.STRING, 
            8);
        extract_Record_Last_Updte_Dte = extract_Record.newFieldInGroup("extract_Record_Last_Updte_Dte", "LAST-UPDTE-DTE", FieldType.NUMERIC, 8);
        extract_Record_Last_Updte_Dte_Tme = extract_Record.newFieldInGroup("extract_Record_Last_Updte_Dte_Tme", "LAST-UPDTE-DTE-TME", FieldType.TIME);
        extract_Record_Last_Updte_Oprtr_Cde = extract_Record.newFieldInGroup("extract_Record_Last_Updte_Oprtr_Cde", "LAST-UPDTE-OPRTR-CDE", FieldType.STRING, 
            8);
        extract_Record_Crprte_Status_Ind = extract_Record.newFieldInGroup("extract_Record_Crprte_Status_Ind", "CRPRTE-STATUS-IND", FieldType.STRING, 1);
        extract_Record_Tiaa_Rcvd_Dte = extract_Record.newFieldInGroup("extract_Record_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE);
        extract_Record_Effctve_Dte = extract_Record.newFieldInGroup("extract_Record_Effctve_Dte", "EFFCTVE-DTE", FieldType.DATE);
        extract_Record_Trans_Dte = extract_Record.newFieldInGroup("extract_Record_Trans_Dte", "TRANS-DTE", FieldType.DATE);
        extract_Record_Cntrct_Nbr = extract_Record.newFieldInGroup("extract_Record_Cntrct_Nbr", "CNTRCT-NBR", FieldType.STRING, 8);
        extract_Record_Pnd_Calendar_Days_In_Tiaa = extract_Record.newFieldInGroup("extract_Record_Pnd_Calendar_Days_In_Tiaa", "#CALENDAR-DAYS-IN-TIAA", 
            FieldType.NUMERIC, 5);
        extract_Record_Pnd_Business_Days_In_Tiaa = extract_Record.newFieldInGroup("extract_Record_Pnd_Business_Days_In_Tiaa", "#BUSINESS-DAYS-IN-TIAA", 
            FieldType.NUMERIC, 5);
        extract_Record_Extrnl_Pend_Rcv_Dte = extract_Record.newFieldInGroup("extract_Record_Extrnl_Pend_Rcv_Dte", "EXTRNL-PEND-RCV-DTE", FieldType.TIME);
        extract_Record_Pnd_Received_In_Unit = extract_Record.newFieldInGroup("extract_Record_Pnd_Received_In_Unit", "#RECEIVED-IN-UNIT", FieldType.TIME);
        extract_Record_Pnd_Bsnss_Days_In_Unit = extract_Record.newFieldInGroup("extract_Record_Pnd_Bsnss_Days_In_Unit", "#BSNSS-DAYS-IN-UNIT", FieldType.NUMERIC, 
            5, 1);
        extract_Record_Pnd_Intrnl_Pend_Days = extract_Record.newFieldInGroup("extract_Record_Pnd_Intrnl_Pend_Days", "#INTRNL-PEND-DAYS", FieldType.NUMERIC, 
            5, 1);
        extract_Record_Pnd_Extrnl_Pend_Days = extract_Record.newFieldInGroup("extract_Record_Pnd_Extrnl_Pend_Days", "#EXTRNL-PEND-DAYS", FieldType.NUMERIC, 
            5, 1);
        extract_Record_Pnd_Extrnl_Pend_Bsnss_Days = extract_Record.newFieldInGroup("extract_Record_Pnd_Extrnl_Pend_Bsnss_Days", "#EXTRNL-PEND-BSNSS-DAYS", 
            FieldType.NUMERIC, 5, 1);
        extract_Record_Pnd_Partic_Sname = extract_Record.newFieldInGroup("extract_Record_Pnd_Partic_Sname", "#PARTIC-SNAME", FieldType.STRING, 7);

        extract_Record__R_Field_8 = extract_Record.newGroupInGroup("extract_Record__R_Field_8", "REDEFINE", extract_Record_Pnd_Partic_Sname);
        extract_Record_Pnd_Folder_Prefix = extract_Record__R_Field_8.newFieldInGroup("extract_Record_Pnd_Folder_Prefix", "#FOLDER-PREFIX", FieldType.STRING, 
            1);
        extract_Record_Pnd_Folder_Nbr = extract_Record__R_Field_8.newFieldInGroup("extract_Record_Pnd_Folder_Nbr", "#FOLDER-NBR", FieldType.NUMERIC, 6);
        extract_Record_Work_List_Ind = extract_Record.newFieldInGroup("extract_Record_Work_List_Ind", "WORK-LIST-IND", FieldType.STRING, 3);
        pnd_Env = localVariables.newFieldInRecord("pnd_Env", "#ENV", FieldType.STRING, 10);
        pnd_Read_Ctr = localVariables.newFieldInRecord("pnd_Read_Ctr", "#READ-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Conv_Ctr = localVariables.newFieldInRecord("pnd_Conv_Ctr", "#CONV-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Nconv_Ctr = localVariables.newFieldInRecord("pnd_Nconv_Ctr", "#NCONV-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Rject_Ctr = localVariables.newFieldInRecord("pnd_Rject_Ctr", "#RJECT-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Total_Ctr = localVariables.newFieldInRecord("pnd_Total_Ctr", "#TOTAL-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_No_Extract = localVariables.newFieldInRecord("pnd_No_Extract", "#NO-EXTRACT", FieldType.NUMERIC, 2);
        pnd_Report_Parm = localVariables.newFieldInRecord("pnd_Report_Parm", "#REPORT-PARM", FieldType.STRING, 17);
        pnd_Racf_Id = localVariables.newFieldInRecord("pnd_Racf_Id", "#RACF-ID", FieldType.STRING, 8);
        pnd_Parm_Unit = localVariables.newFieldInRecord("pnd_Parm_Unit", "#PARM-UNIT", FieldType.STRING, 7);
        pnd_Floor = localVariables.newFieldInRecord("pnd_Floor", "#FLOOR", FieldType.NUMERIC, 2);
        pnd_Bldg = localVariables.newFieldInRecord("pnd_Bldg", "#BLDG", FieldType.STRING, 3);
        pnd_Drop_Off = localVariables.newFieldInRecord("pnd_Drop_Off", "#DROP-OFF", FieldType.STRING, 2);
        pnd_Tbl_Run_Date = localVariables.newFieldInRecord("pnd_Tbl_Run_Date", "#TBL-RUN-DATE", FieldType.STRING, 8);
        pnd_Tbl_Run_Flag = localVariables.newFieldInRecord("pnd_Tbl_Run_Flag", "#TBL-RUN-FLAG", FieldType.STRING, 1);
        pnd_Comp_Date = localVariables.newFieldInRecord("pnd_Comp_Date", "#COMP-DATE", FieldType.STRING, 8);
        pnd_Start_Date_A = localVariables.newFieldInRecord("pnd_Start_Date_A", "#START-DATE-A", FieldType.STRING, 8);
        pnd_Base_Date = localVariables.newFieldInRecord("pnd_Base_Date", "#BASE-DATE", FieldType.DATE);
        pnd_Tbl_Key = localVariables.newFieldInRecord("pnd_Tbl_Key", "#TBL-KEY", FieldType.STRING, 53);

        pnd_Tbl_Key__R_Field_9 = localVariables.newGroupInRecord("pnd_Tbl_Key__R_Field_9", "REDEFINE", pnd_Tbl_Key);

        pnd_Tbl_Key_Data_Nme = pnd_Tbl_Key__R_Field_9.newGroupInGroup("pnd_Tbl_Key_Data_Nme", "DATA-NME");
        pnd_Tbl_Key_Tbl_Scrty_Level_Ind = pnd_Tbl_Key_Data_Nme.newFieldInGroup("pnd_Tbl_Key_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", FieldType.STRING, 
            2);
        pnd_Tbl_Key_Tbl_Table_Nme = pnd_Tbl_Key_Data_Nme.newFieldInGroup("pnd_Tbl_Key_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 20);
        pnd_Tbl_Key_Tbl_Key_Field = pnd_Tbl_Key_Data_Nme.newFieldInGroup("pnd_Tbl_Key_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 30);

        pnd_Tbl_Prime_Key = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY");
        pnd_Tbl_Prime_Key_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key.newFieldInGroup("pnd_Tbl_Prime_Key_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", FieldType.STRING, 
            2);
        pnd_Tbl_Prime_Key_Tbl_Table_Nme = pnd_Tbl_Prime_Key.newFieldInGroup("pnd_Tbl_Prime_Key_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 20);
        pnd_Tbl_Prime_Key_Tbl_Key_Field = pnd_Tbl_Prime_Key.newFieldInGroup("pnd_Tbl_Prime_Key_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 30);
        pnd_Parm_Report_No = localVariables.newFieldInRecord("pnd_Parm_Report_No", "#PARM-REPORT-NO", FieldType.NUMERIC, 2);
        pnd_Parm_Empl_Racf_Id = localVariables.newFieldInRecord("pnd_Parm_Empl_Racf_Id", "#PARM-EMPL-RACF-ID", FieldType.STRING, 8);
        pnd_Parm_Unit_Cde = localVariables.newFieldInRecord("pnd_Parm_Unit_Cde", "#PARM-UNIT-CDE", FieldType.STRING, 8);
        pnd_Parm_Floor = localVariables.newFieldInRecord("pnd_Parm_Floor", "#PARM-FLOOR", FieldType.NUMERIC, 2);
        pnd_Parm_Bldg = localVariables.newFieldInRecord("pnd_Parm_Bldg", "#PARM-BLDG", FieldType.STRING, 3);
        pnd_Parm_Drop_Off = localVariables.newFieldInRecord("pnd_Parm_Drop_Off", "#PARM-DROP-OFF", FieldType.STRING, 2);
        pnd_Parm_Run_Date = localVariables.newFieldInRecord("pnd_Parm_Run_Date", "#PARM-RUN-DATE", FieldType.STRING, 8);

        pnd_End_Var = localVariables.newGroupInRecord("pnd_End_Var", "#END-VAR");
        pnd_End_Var_Pnd_End_Tbl_Key = pnd_End_Var.newFieldInGroup("pnd_End_Var_Pnd_End_Tbl_Key", "#END-TBL-KEY", FieldType.STRING, 53);

        pnd_End_Var__R_Field_10 = pnd_End_Var.newGroupInGroup("pnd_End_Var__R_Field_10", "REDEFINE", pnd_End_Var_Pnd_End_Tbl_Key);
        pnd_End_Var_Pnd_End_Tbl_Authrty = pnd_End_Var__R_Field_10.newFieldInGroup("pnd_End_Var_Pnd_End_Tbl_Authrty", "#END-TBL-AUTHRTY", FieldType.STRING, 
            2);
        pnd_End_Var_Pnd_End_Tbl_Name = pnd_End_Var__R_Field_10.newFieldInGroup("pnd_End_Var_Pnd_End_Tbl_Name", "#END-TBL-NAME", FieldType.STRING, 20);
        pnd_End_Var_Pnd_End_Tbl_Code = pnd_End_Var__R_Field_10.newFieldInGroup("pnd_End_Var_Pnd_End_Tbl_Code", "#END-TBL-CODE", FieldType.STRING, 30);

        pnd_Gen_Var = localVariables.newGroupInRecord("pnd_Gen_Var", "#GEN-VAR");
        pnd_Gen_Var_Pnd_Gen_Tbl_Key = pnd_Gen_Var.newFieldInGroup("pnd_Gen_Var_Pnd_Gen_Tbl_Key", "#GEN-TBL-KEY", FieldType.STRING, 53);

        pnd_Gen_Var__R_Field_11 = pnd_Gen_Var.newGroupInGroup("pnd_Gen_Var__R_Field_11", "REDEFINE", pnd_Gen_Var_Pnd_Gen_Tbl_Key);
        pnd_Gen_Var_Pnd_Gen_Tbl_Authrty = pnd_Gen_Var__R_Field_11.newFieldInGroup("pnd_Gen_Var_Pnd_Gen_Tbl_Authrty", "#GEN-TBL-AUTHRTY", FieldType.STRING, 
            2);
        pnd_Gen_Var_Pnd_Gen_Tbl_Name = pnd_Gen_Var__R_Field_11.newFieldInGroup("pnd_Gen_Var_Pnd_Gen_Tbl_Name", "#GEN-TBL-NAME", FieldType.STRING, 20);
        pnd_Gen_Var_Pnd_Gen_Tbl_Code = pnd_Gen_Var__R_Field_11.newFieldInGroup("pnd_Gen_Var_Pnd_Gen_Tbl_Code", "#GEN-TBL-CODE", FieldType.STRING, 30);
        pnd_Gen_Var_Pnd_Gen_Tbl_Desc = pnd_Gen_Var.newFieldInGroup("pnd_Gen_Var_Pnd_Gen_Tbl_Desc", "#GEN-TBL-DESC", FieldType.STRING, 30);
        pnd_Parm_Type = localVariables.newFieldInRecord("pnd_Parm_Type", "#PARM-TYPE", FieldType.STRING, 1);
        pnd_Sub = localVariables.newFieldInRecord("pnd_Sub", "#SUB", FieldType.PACKED_DECIMAL, 4);
        pnd_Extrnl_Pend_Daysb = localVariables.newFieldInRecord("pnd_Extrnl_Pend_Daysb", "#EXTRNL-PEND-DAYSB", FieldType.NUMERIC, 20, 2);
        pnd_Timx = localVariables.newFieldInRecord("pnd_Timx", "#TIMX", FieldType.TIME);
        pnd_Receive_Date = localVariables.newFieldInRecord("pnd_Receive_Date", "#RECEIVE-DATE", FieldType.DATE);
        pnd_Report_Date = localVariables.newFieldInRecord("pnd_Report_Date", "#REPORT-DATE", FieldType.DATE);
        pnd_Start_Date = localVariables.newFieldInRecord("pnd_Start_Date", "#START-DATE", FieldType.DATE);
        pnd_Report_Date_Time = localVariables.newFieldInRecord("pnd_Report_Date_Time", "#REPORT-DATE-TIME", FieldType.TIME);
        pnd_Intrnl_Pend_Date = localVariables.newFieldInRecord("pnd_Intrnl_Pend_Date", "#INTRNL-PEND-DATE", FieldType.TIME);
        pnd_Unit_Start_Date_Time = localVariables.newFieldInRecord("pnd_Unit_Start_Date_Time", "#UNIT-START-DATE-TIME", FieldType.TIME);
        pnd_Intrnl_Pend_Daysx = localVariables.newFieldInRecord("pnd_Intrnl_Pend_Daysx", "#INTRNL-PEND-DAYSX", FieldType.NUMERIC, 20, 2);
        pnd_Bsnss_Days_In_Unitx = localVariables.newFieldInRecord("pnd_Bsnss_Days_In_Unitx", "#BSNSS-DAYS-IN-UNITX", FieldType.NUMERIC, 20, 2);
        pnd_Extrnl_Pend_Daysx = localVariables.newFieldInRecord("pnd_Extrnl_Pend_Daysx", "#EXTRNL-PEND-DAYSX", FieldType.PACKED_DECIMAL, 19, 1);
        pnd_No_Work_Days = localVariables.newFieldInRecord("pnd_No_Work_Days", "#NO-WORK-DAYS", FieldType.NUMERIC, 18);
        pnd_Pnd_Start_Date = localVariables.newFieldInRecord("pnd_Pnd_Start_Date", "##START-DATE", FieldType.STRING, 8);

        pnd_Pnd_Start_Date__R_Field_12 = localVariables.newGroupInRecord("pnd_Pnd_Start_Date__R_Field_12", "REDEFINE", pnd_Pnd_Start_Date);
        pnd_Pnd_Start_Date_Pnd_Pnd_Start_Date_N = pnd_Pnd_Start_Date__R_Field_12.newFieldInGroup("pnd_Pnd_Start_Date_Pnd_Pnd_Start_Date_N", "##START-DATE-N", 
            FieldType.NUMERIC, 8);
        pnd_Pnd_End_Date = localVariables.newFieldInRecord("pnd_Pnd_End_Date", "##END-DATE", FieldType.STRING, 8);

        pnd_Pnd_End_Date__R_Field_13 = localVariables.newGroupInRecord("pnd_Pnd_End_Date__R_Field_13", "REDEFINE", pnd_Pnd_End_Date);
        pnd_Pnd_End_Date_Pnd_Pnd_End_Date_N = pnd_Pnd_End_Date__R_Field_13.newFieldInGroup("pnd_Pnd_End_Date_Pnd_Pnd_End_Date_N", "##END-DATE-N", FieldType.NUMERIC, 
            8);
        pnd_Work_Date = localVariables.newFieldInRecord("pnd_Work_Date", "#WORK-DATE", FieldType.DATE);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Master_Index.reset();

        localVariables.reset();
        pnd_Tbl_Prime_Key_Tbl_Scrty_Level_Ind.setInitialValue("A");
        pnd_Tbl_Prime_Key_Tbl_Table_Nme.setInitialValue("CWF-RPRT-RUN-TBL");
        pnd_Tbl_Prime_Key_Tbl_Key_Field.setInitialValue("D");
        pnd_Parm_Report_No.setInitialValue(21);
        pnd_Parm_Unit_Cde.setInitialValue("CWF");
        pnd_Parm_Type.setInitialValue("D");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb3906() throws Exception
    {
        super("Cwfb3906");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("CWFB3906", onError);
        pnd_Timx.setValue(Global.getTIMX());                                                                                                                              //Natural: MOVE *TIMX TO #TIMX
        pnd_Report_Date.setValue(pnd_Timx);                                                                                                                               //Natural: MOVE #TIMX TO #REPORT-DATE
        pnd_Report_Date_Time.setValue(pnd_Timx);                                                                                                                          //Natural: MOVE #TIMX TO #REPORT-DATE-TIME
        //* ***********************************************************************
        DbsUtil.callnat(Cwfn3912.class , getCurrentProcessState(), pnd_Start_Date_A, pnd_Parm_Type);                                                                      //Natural: CALLNAT 'CWFN3912' #START-DATE-A #PARM-TYPE
        if (condition(Global.isEscape())) return;
        //*  DEFINE FORMATS
        pnd_Comp_Date.setValue(pnd_Start_Date_A);                                                                                                                         //Natural: MOVE #START-DATE-A TO #COMP-DATE #PARM-RUN-DATE
        pnd_Parm_Run_Date.setValue(pnd_Start_Date_A);
                                                                                                                                                                          //Natural: PERFORM DECIDE-EXTRACT
        sub_Decide_Extract();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_No_Extract.equals(3)))                                                                                                                          //Natural: IF #NO-EXTRACT = 3
        {
            DbsUtil.terminate();  if (true) return;                                                                                                                       //Natural: TERMINATE
        }                                                                                                                                                                 //Natural: END-IF
        //* ******************** START OF MAIN PROGRAM LOGIC **********************
        pnd_Base_Date.compute(new ComputeParameters(false, pnd_Base_Date), Global.getDATX().subtract(9999));                                                              //Natural: COMPUTE #BASE-DATE = *DATX - 9999
        //* *
        //* *********************************
        //*  COPYCODE : CWFC3000
        //*  FUNCTION : SETUP ENVONMENT MESSAGE
        //*  AUTHOR   : PATINGO, JOSEPH S.
        //* *********************************
        pnd_Env.setValue(Global.getLIBRARY_ID());                                                                                                                         //Natural: MOVE *LIBRARY-ID TO #ENV
        if (condition(pnd_Env.equals("PROJCWF") || pnd_Env.equals("PROJCWF")))                                                                                            //Natural: IF #ENV = 'PROJCWF' OR #ENV = 'PROJCWF'
        {
            pnd_Env.setValue("DEV'T ");                                                                                                                                   //Natural: MOVE 'DEV"T ' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("PRACANN")))                                                                                                                         //Natural: IF #ENV = 'PRACANN'
        {
            pnd_Env.setValue("PRACTICE");                                                                                                                                 //Natural: MOVE 'PRACTICE' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("PRODANN")))                                                                                                                         //Natural: IF #ENV = 'PRODANN'
        {
            pnd_Env.setValue(" ");                                                                                                                                        //Natural: MOVE ' ' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT034")))                                                                                                                         //Natural: IF #ENV = 'ACPT034'
        {
            pnd_Env.setValue("RGN:AT07");                                                                                                                                 //Natural: MOVE 'RGN:AT07' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT023")))                                                                                                                         //Natural: IF #ENV = 'ACPT023'
        {
            pnd_Env.setValue("RGN:AT06");                                                                                                                                 //Natural: MOVE 'RGN:AT06' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT028")))                                                                                                                         //Natural: IF #ENV = 'ACPT028'
        {
            pnd_Env.setValue("RGN:AT05");                                                                                                                                 //Natural: MOVE 'RGN:AT05' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        //* ****** END COPYCODE *************
        if (condition(Global.getLIBRARY_ID().notEquals("PRODANN")))                                                                                                       //Natural: IF *LIBRARY-ID NE 'PRODANN'
        {
            pnd_Env.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "(", pnd_Env, ")"));                                                                         //Natural: COMPRESS '(' #ENV ')' INTO #ENV LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Cwfn3910.class , getCurrentProcessState(), pnd_Parm_Report_No, pnd_Parm_Empl_Racf_Id, pnd_Parm_Unit_Cde, pnd_Parm_Floor, pnd_Parm_Bldg,           //Natural: CALLNAT 'CWFN3910' #PARM-REPORT-NO #PARM-EMPL-RACF-ID #PARM-UNIT-CDE #PARM-FLOOR #PARM-BLDG #PARM-DROP-OFF #PARM-RUN-DATE
            pnd_Parm_Drop_Off, pnd_Parm_Run_Date);
        if (condition(Global.isEscape())) return;
        //*  MAIN PROGRAM LOOP
        //*  VR 4/23/2014 STARTS
        //*  PROG.
        //*  READ CWF-MASTER-INDEX BY
        //*     CRPRTE-STATUS-CHNGE-DTE-KEY FROM '0'
        //*   IF CWF-MASTER-INDEX.CRPRTE-STATUS-IND GT '0'
        //*     ESCAPE BOTTOM(PROG.)
        //*   END-IF
        pnd_Pnd_End_Date.setValue(Global.getDATN());                                                                                                                      //Natural: MOVE *DATN TO ##END-DATE
        pnd_Work_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Pnd_End_Date);                                                                                    //Natural: MOVE EDITED ##END-DATE TO #WORK-DATE ( EM = YYYYMMDD )
        //*  FETCH LAST 3 YEARS
        pnd_Work_Date.nsubtract(1095);                                                                                                                                    //Natural: COMPUTE #WORK-DATE = #WORK-DATE - 1095
        pnd_Pnd_Start_Date.setValueEdited(pnd_Work_Date,new ReportEditMask("YYYYMMDD"));                                                                                  //Natural: MOVE EDITED #WORK-DATE ( EM = YYYYMMDD ) TO ##START-DATE
        getReports().print(0, "=",pnd_Pnd_Start_Date,"=",pnd_Pnd_End_Date);                                                                                               //Natural: PRINT '=' ##START-DATE '=' ##END-DATE
        vw_cwf_Master_Index.startDatabaseRead                                                                                                                             //Natural: READ CWF-MASTER-INDEX BY RQST-HISTORY-KEY FROM ##START-DATE-N
        (
        "PROG",
        new Wc[] { new Wc("RQST_HISTORY_KEY", ">=", pnd_Pnd_Start_Date_Pnd_Pnd_Start_Date_N, WcType.BY) },
        new Oc[] { new Oc("RQST_HISTORY_KEY", "ASC") }
        );
        PROG:
        while (condition(vw_cwf_Master_Index.readNextRow("PROG")))
        {
            if (condition(cwf_Master_Index_Rqst_Log_Index_Dte_N.greater(pnd_Pnd_End_Date_Pnd_Pnd_End_Date_N)))                                                            //Natural: IF CWF-MASTER-INDEX.RQST-LOG-INDEX-DTE-N GT ##END-DATE-N
            {
                if (true) break PROG;                                                                                                                                     //Natural: ESCAPE BOTTOM ( PROG. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cwf_Master_Index_Crprte_Status_Ind.equals("0") && cwf_Master_Index_Actve_Ind.equals("A")))                                                      //Natural: IF CWF-MASTER-INDEX.CRPRTE-STATUS-IND = '0' AND ACTVE-IND = 'A'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  VR 4/23/2014 ENDS
            //*  ACCEPT IF CWF-MASTER-INDEX.ADMIN-STATUS-CDE = 'LINK' OR
            //*  CWF-MASTER-INDEX.ADMIN-STATUS-CDE = '0000' THRU '7999' OR
            //*  CWF-MASTER-INDEX.STATUS-CDE = '0000' THRU '7999'
            if (condition(! (((cwf_Master_Index_Admin_Status_Cde.equals("LINK") || (cwf_Master_Index_Admin_Status_Cde.greaterOrEqual("0000") && cwf_Master_Index_Admin_Status_Cde.lessOrEqual("7999")))  //Natural: REJECT IF NOT ( CWF-MASTER-INDEX.ADMIN-STATUS-CDE = 'LINK' OR CWF-MASTER-INDEX.ADMIN-STATUS-CDE = '0000' THRU '7999' OR CWF-MASTER-INDEX.STATUS-CDE = '0000' THRU '7999' )
                || (cwf_Master_Index_Status_Cde.greaterOrEqual("0000") && cwf_Master_Index_Status_Cde.lessOrEqual("7999"))))))
            {
                continue;
            }
            //*  PIN-EXP
            if (condition(cwf_Master_Index_Tiaa_Rcvd_Dte.lessOrEqual(pnd_Base_Date)))                                                                                     //Natural: IF CWF-MASTER-INDEX.TIAA-RCVD-DTE LE #BASE-DATE
            {
                getReports().write(1, ReportOption.NOTITLE,"TIAA DATE",cwf_Master_Index_Tiaa_Rcvd_Dte,"MAY BE IN ERROR FOR PIN",cwf_Master_Index_Pin_Nbr,                 //Natural: WRITE ( 1 ) 'TIAA DATE' CWF-MASTER-INDEX.TIAA-RCVD-DTE 'MAY BE IN ERROR FOR PIN' CWF-MASTER-INDEX.PIN-NBR ( EM = 999999999999 ) '(' CWF-MASTER-INDEX.RQST-LOG-DTE-TME ') WPID = ' CWF-MASTER-INDEX.WORK-PRCSS-ID
                    new ReportEditMask ("999999999999"),"(",cwf_Master_Index_Rqst_Log_Dte_Tme,") WPID = ",cwf_Master_Index_Work_Prcss_Id);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cwf_Master_Index_Rqst_Orgn_Cde.notEquals("J")))                                                                                                 //Natural: IF CWF-MASTER-INDEX.RQST-ORGN-CDE NE 'J'
            {
                //*  ---------------------
                //*  CALENDAR DAYS AND BUSINESS DAYS IN TIAA
                //*  ---------------------------------------
                if (condition(cwf_Master_Index_Extrnl_Pend_Rcv_Dte.greater(getZero())))                                                                                   //Natural: IF CWF-MASTER-INDEX.EXTRNL-PEND-RCV-DTE GT 0
                {
                    pnd_Receive_Date.setValue(cwf_Master_Index_Extrnl_Pend_Rcv_Dte);                                                                                      //Natural: MOVE CWF-MASTER-INDEX.EXTRNL-PEND-RCV-DTE TO #RECEIVE-DATE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Receive_Date.setValue(cwf_Master_Index_Tiaa_Rcvd_Dte);                                                                                            //Natural: MOVE CWF-MASTER-INDEX.TIAA-RCVD-DTE TO #RECEIVE-DATE
                }                                                                                                                                                         //Natural: END-IF
                extract_Record_Pnd_Calendar_Days_In_Tiaa.compute(new ComputeParameters(false, extract_Record_Pnd_Calendar_Days_In_Tiaa), pnd_Report_Date.subtract(pnd_Receive_Date)); //Natural: COMPUTE #CALENDAR-DAYS-IN-TIAA = #REPORT-DATE - #RECEIVE-DATE
                DbsUtil.callnat(Cwfn3901.class , getCurrentProcessState(), pnd_Receive_Date, pnd_Report_Date, pnd_No_Work_Days);                                          //Natural: CALLNAT 'CWFN3901' #RECEIVE-DATE #REPORT-DATE #NO-WORK-DAYS
                if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                extract_Record_Pnd_Business_Days_In_Tiaa.compute(new ComputeParameters(false, extract_Record_Pnd_Business_Days_In_Tiaa), extract_Record_Pnd_Calendar_Days_In_Tiaa.subtract(pnd_No_Work_Days)); //Natural: COMPUTE #BUSINESS-DAYS-IN-TIAA = #CALENDAR-DAYS-IN-TIAA - #NO-WORK-DAYS
                //*  ---------------------
                //*  RECEIVED IN UNIT DATE
                //*  ---------------------
                if (condition(DbsUtil.maskMatches(cwf_Master_Index_Unit_Clock_Start_Dte_Tme,"YYYYMMDD")))                                                                 //Natural: IF CWF-MASTER-INDEX.UNIT-CLOCK-START-DTE-TME = MASK ( YYYYMMDD )
                {
                    extract_Record_Pnd_Received_In_Unit.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),cwf_Master_Index_Unit_Clock_Start_Dte_Tme);                  //Natural: MOVE EDITED CWF-MASTER-INDEX.UNIT-CLOCK-START-DTE-TME TO #RECEIVED-IN-UNIT ( EM = YYYYMMDDHHIISST )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    extract_Record_Pnd_Received_In_Unit.reset();                                                                                                          //Natural: RESET #RECEIVED-IN-UNIT
                }                                                                                                                                                         //Natural: END-IF
                //*  ------------------
                //*  EXTERNAL-PEND-DAYS
                //*  ------------------
                if (condition(((cwf_Master_Index_Admin_Status_Cde.greaterOrEqual("4500") && cwf_Master_Index_Admin_Status_Cde.lessOrEqual("4997")) ||                     //Natural: IF CWF-MASTER-INDEX.ADMIN-STATUS-CDE = '4500' THRU '4997' OR CWF-MASTER-INDEX.STATUS-CDE = '4500' THRU '4997'
                    (cwf_Master_Index_Status_Cde.greaterOrEqual("4500") && cwf_Master_Index_Status_Cde.lessOrEqual("4997")))))
                {
                    extract_Record_Pnd_Extrnl_Pend_Bsnss_Days.reset();                                                                                                    //Natural: RESET #EXTRNL-PEND-BSNSS-DAYS
                    pnd_Extrnl_Pend_Daysx.compute(new ComputeParameters(false, pnd_Extrnl_Pend_Daysx), pnd_Report_Date_Time.subtract(cwf_Master_Index_Admin_Status_Updte_Dte_Tme)); //Natural: COMPUTE #EXTRNL-PEND-DAYSX = #REPORT-DATE-TIME - CWF-MASTER-INDEX.ADMIN-STATUS-UPDTE-DTE-TME
                    extract_Record_Pnd_Extrnl_Pend_Days.compute(new ComputeParameters(false, extract_Record_Pnd_Extrnl_Pend_Days), pnd_Extrnl_Pend_Daysx.divide(864000)); //Natural: COMPUTE #EXTRNL-PEND-DAYS = #EXTRNL-PEND-DAYSX / 864000
                    DbsUtil.callnat(Cwfn3900.class , getCurrentProcessState(), cwf_Master_Index_Admin_Status_Updte_Dte_Tme, pnd_Report_Date_Time, pnd_Extrnl_Pend_Daysb); //Natural: CALLNAT 'CWFN3900' CWF-MASTER-INDEX.ADMIN-STATUS-UPDTE-DTE-TME #REPORT-DATE-TIME #EXTRNL-PEND-DAYSB
                    if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                    extract_Record_Pnd_Extrnl_Pend_Bsnss_Days.nadd(pnd_Extrnl_Pend_Daysb);                                                                                //Natural: COMPUTE #EXTRNL-PEND-BSNSS-DAYS = #EXTRNL-PEND-BSNSS-DAYS + #EXTRNL-PEND-DAYSB
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    extract_Record_Pnd_Extrnl_Pend_Days.reset();                                                                                                          //Natural: RESET #EXTRNL-PEND-DAYS
                }                                                                                                                                                         //Natural: END-IF
                //*  ------------------
                //*  INTERNAL-PEND-DAYS
                //*  ------------------
                if (condition(cwf_Master_Index_Work_List_Ind.equals("2IP") || cwf_Master_Index_Work_List_Ind.equals("1RA")))                                              //Natural: IF CWF-MASTER-INDEX.WORK-LIST-IND = '2IP' OR = '1RA'
                {
                    if (condition(cwf_Master_Index_Intrnl_Pnd_Start_Dte_Tme.greater(" ")))                                                                                //Natural: IF CWF-MASTER-INDEX.INTRNL-PND-START-DTE-TME GT ' '
                    {
                        pnd_Intrnl_Pend_Date.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),cwf_Master_Index_Intrnl_Pnd_Start_Dte_Tme);                             //Natural: MOVE EDITED CWF-MASTER-INDEX.INTRNL-PND-START-DTE-TME TO #INTRNL-PEND-DATE ( EM = YYYYMMDDHHIISST )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Intrnl_Pend_Date.setValue(cwf_Master_Index_Admin_Status_Updte_Dte_Tme);                                                                       //Natural: MOVE CWF-MASTER-INDEX.ADMIN-STATUS-UPDTE-DTE-TME TO #INTRNL-PEND-DATE
                    }                                                                                                                                                     //Natural: END-IF
                    DbsUtil.callnat(Cwfn3900.class , getCurrentProcessState(), pnd_Intrnl_Pend_Date, pnd_Report_Date_Time, pnd_Intrnl_Pend_Daysx);                        //Natural: CALLNAT 'CWFN3900' #INTRNL-PEND-DATE #REPORT-DATE-TIME #INTRNL-PEND-DAYSX
                    if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                    extract_Record_Pnd_Intrnl_Pend_Days.compute(new ComputeParameters(false, extract_Record_Pnd_Intrnl_Pend_Days), pnd_Intrnl_Pend_Daysx.add((cwf_Master_Index_Intrnl_Pnd_Days.divide(10)))); //Natural: COMPUTE #INTRNL-PEND-DAYS = #INTRNL-PEND-DAYSX + ( CWF-MASTER-INDEX.INTRNL-PND-DAYS / 10 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    extract_Record_Pnd_Intrnl_Pend_Days.reset();                                                                                                          //Natural: RESET #INTRNL-PEND-DAYS
                }                                                                                                                                                         //Natural: END-IF
                //*  -------------------------------------------------------------
                //*  BUSINESS DAYS IN UNIT (LESS HOLIDAYS/WEEKENDS/INTERNAL PENDS)
                //*  -------------------------------------------------------------
                //*        COMPUTE HOLIDAYS/WEEKENDS BETWEEN DAYS
                extract_Record_Pnd_Bsnss_Days_In_Unit.setValue(0);                                                                                                        //Natural: ASSIGN #BSNSS-DAYS-IN-UNIT = 0
                if (condition(cwf_Master_Index_Unit_Clock_Start_Dte_Tme.greater(" ")))                                                                                    //Natural: IF CWF-MASTER-INDEX.UNIT-CLOCK-START-DTE-TME GT ' '
                {
                    pnd_Unit_Start_Date_Time.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),cwf_Master_Index_Unit_Clock_Start_Dte_Tme);                             //Natural: MOVE EDITED CWF-MASTER-INDEX.UNIT-CLOCK-START-DTE-TME TO #UNIT-START-DATE-TIME ( EM = YYYYMMDDHHIISST )
                    DbsUtil.callnat(Cwfn3900.class , getCurrentProcessState(), pnd_Unit_Start_Date_Time, pnd_Report_Date_Time, pnd_Bsnss_Days_In_Unitx);                  //Natural: CALLNAT 'CWFN3900' #UNIT-START-DATE-TIME #REPORT-DATE-TIME #BSNSS-DAYS-IN-UNITX
                    if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                    extract_Record_Pnd_Bsnss_Days_In_Unit.nadd(pnd_Bsnss_Days_In_Unitx);                                                                                  //Natural: ASSIGN #BSNSS-DAYS-IN-UNIT := #BSNSS-DAYS-IN-UNIT + #BSNSS-DAYS-IN-UNITX
                }                                                                                                                                                         //Natural: END-IF
                //* *****************************************************************
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cwf_Master_Index_Admin_Status_Cde.lessOrEqual("0899")))                                                                                         //Natural: IF CWF-MASTER-INDEX.ADMIN-STATUS-CDE LE '0899'
            {
                extract_Record_Pnd_Bsnss_Days_In_Unit.reset();                                                                                                            //Natural: RESET #BSNSS-DAYS-IN-UNIT #INTRNL-PEND-DAYS #RECEIVED-IN-UNIT
                extract_Record_Pnd_Intrnl_Pend_Days.reset();
                extract_Record_Pnd_Received_In_Unit.reset();
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cwf_Master_Index_Mj_Pull_Ind.equals("Y") || cwf_Master_Index_Mj_Pull_Ind.equals("R")))                                                          //Natural: IF CWF-MASTER-INDEX.MJ-PULL-IND = 'Y' OR = 'R'
            {
                extract_Record_Pnd_Folder_Prefix.setValue("M");                                                                                                           //Natural: MOVE 'M' TO #FOLDER-PREFIX
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                extract_Record_Pnd_Folder_Prefix.setValue("F");                                                                                                           //Natural: MOVE 'F' TO #FOLDER-PREFIX
            }                                                                                                                                                             //Natural: END-IF
            extract_Record_Pnd_Folder_Nbr.setValue(cwf_Master_Index_Physcl_Fldr_Id_Nbr);                                                                                  //Natural: MOVE CWF-MASTER-INDEX.PHYSCL-FLDR-ID-NBR TO #FOLDER-NBR
            //* *
            extract_Record.setValuesByName(vw_cwf_Master_Index);                                                                                                          //Natural: MOVE BY NAME CWF-MASTER-INDEX TO EXTRACT-RECORD
            getWorkFiles().write(5, false, extract_Record);                                                                                                               //Natural: WRITE WORK FILE 5 EXTRACT-RECORD
            //* *
            if (condition(cwf_Master_Index_Rqst_Orgn_Cde.equals("J")))                                                                                                    //Natural: IF CWF-MASTER-INDEX.RQST-ORGN-CDE = 'J'
            {
                pnd_Conv_Ctr.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #CONV-CTR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Nconv_Ctr.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #NCONV-CTR
            }                                                                                                                                                             //Natural: END-IF
            //* *
            extract_Record.reset();                                                                                                                                       //Natural: RESET EXTRACT-RECORD
            //* *
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* *
        pnd_Read_Ctr.setValue(vw_cwf_Master_Index.getAstCOUNTER());                                                                                                       //Natural: MOVE *COUNTER ( PROG. ) TO #READ-CTR
        //* *
        pnd_Total_Ctr.compute(new ComputeParameters(false, pnd_Total_Ctr), pnd_Conv_Ctr.add(pnd_Nconv_Ctr));                                                              //Natural: COMPUTE #TOTAL-CTR = #CONV-CTR + #NCONV-CTR
        pnd_Rject_Ctr.compute(new ComputeParameters(false, pnd_Rject_Ctr), pnd_Read_Ctr.subtract(pnd_Total_Ctr));                                                         //Natural: COMPUTE #RJECT-CTR = #READ-CTR - #TOTAL-CTR
        getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),pnd_Env,new TabSetting(26),"CORPORATE WORKFLOW FACILITIES",new TabSetting(72),"PAGE",getReports().getPageNumberDbs(1),  //Natural: WRITE ( 1 ) NOTITLE *PROGRAM #ENV 26T 'CORPORATE WORKFLOW FACILITIES' 72T 'PAGE' *PAGE-NUMBER ( 1 ) ( NL = 3 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 33T 'DAILY EXTRACT' 72T *TIMX ( EM = HH':'II' 'AP )
            new NumericLength (3), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),new 
            TabSetting(33),"DAILY EXTRACT",new TabSetting(72),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"));
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,"=",new RepeatItem(79));                                                                                               //Natural: WRITE ( 1 ) '=' ( 79 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"TOTAL NUMBER OF RECORDS READ                  ",pnd_Read_Ctr,NEWLINE,"                REJECTED RECORDS              ",pnd_Rject_Ctr,NEWLINE,"                EXTRACTED RECORDS             ",pnd_Total_Ctr,NEWLINE,NEWLINE,"BREAKDOWN OF EXTRACTED RECORDS                ",NEWLINE,"                CONVERTED MIT RECORDS WRITTEN ",pnd_Conv_Ctr,NEWLINE,"                REGULAR MIT RECORDS WRITTEN   ",pnd_Nconv_Ctr,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"REV-2********************** END OF REPORT *******************11/01/94",NEWLINE,"=",new  //Natural: WRITE ( 1 ) / / 'TOTAL NUMBER OF RECORDS READ                  ' #READ-CTR / '                REJECTED RECORDS              ' #RJECT-CTR / '                EXTRACTED RECORDS             ' #TOTAL-CTR / / 'BREAKDOWN OF EXTRACTED RECORDS                ' / '                CONVERTED MIT RECORDS WRITTEN ' #CONV-CTR / '                REGULAR MIT RECORDS WRITTEN   ' #NCONV-CTR //// 'REV-2********************** END OF REPORT *******************11/01/94' / '=' ( 79 )
            RepeatItem(79));
        if (Global.isEscape()) return;
        getReports().eject(1, true);                                                                                                                                      //Natural: EJECT ( 1 )
        //* *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DECIDE-EXTRACT
        //* *
        DbsUtil.callnat(Cwfn3911.class , getCurrentProcessState());                                                                                                       //Natural: CALLNAT 'CWFN3911'
        if (condition(Global.isEscape())) return;
        //* *
        //*  PIN-EXP
    }                                                                                                                                                                     //Natural: ON ERROR
    private void sub_Decide_Extract() throws Exception                                                                                                                    //Natural: DECIDE-EXTRACT
    {
        if (BLNatReinput.isReinput()) return;

        //* *
        pnd_No_Extract.reset();                                                                                                                                           //Natural: RESET #NO-EXTRACT
        pnd_Report_Parm.setValue("CWFB3006D*");                                                                                                                           //Natural: MOVE 'CWFB3006D*' TO #REPORT-PARM
        DbsUtil.callnat(Cwfn3916.class , getCurrentProcessState(), pnd_Report_Parm, pnd_Racf_Id, pnd_Parm_Unit, pnd_Floor, pnd_Bldg, pnd_Drop_Off, pnd_Tbl_Run_Date,      //Natural: CALLNAT 'CWFN3916' #REPORT-PARM #RACF-ID #PARM-UNIT #FLOOR #BLDG #DROP-OFF #TBL-RUN-DATE #TBL-RUN-FLAG
            pnd_Tbl_Run_Flag);
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Comp_Date.lessOrEqual(pnd_Tbl_Run_Date)))                                                                                                       //Natural: IF #COMP-DATE LE #TBL-RUN-DATE
        {
            pnd_No_Extract.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #NO-EXTRACT
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Report_Parm.setValue("CWFB3008D*");                                                                                                                           //Natural: MOVE 'CWFB3008D*' TO #REPORT-PARM
        DbsUtil.callnat(Cwfn3916.class , getCurrentProcessState(), pnd_Report_Parm, pnd_Racf_Id, pnd_Parm_Unit, pnd_Floor, pnd_Bldg, pnd_Drop_Off, pnd_Tbl_Run_Date,      //Natural: CALLNAT 'CWFN3916' #REPORT-PARM #RACF-ID #PARM-UNIT #FLOOR #BLDG #DROP-OFF #TBL-RUN-DATE #TBL-RUN-FLAG
            pnd_Tbl_Run_Flag);
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Comp_Date.lessOrEqual(pnd_Tbl_Run_Date)))                                                                                                       //Natural: IF #COMP-DATE LE #TBL-RUN-DATE
        {
            pnd_No_Extract.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #NO-EXTRACT
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Report_Parm.setValue("CWFB3009D*");                                                                                                                           //Natural: MOVE 'CWFB3009D*' TO #REPORT-PARM
        DbsUtil.callnat(Cwfn3916.class , getCurrentProcessState(), pnd_Report_Parm, pnd_Racf_Id, pnd_Parm_Unit, pnd_Floor, pnd_Bldg, pnd_Drop_Off, pnd_Tbl_Run_Date,      //Natural: CALLNAT 'CWFN3916' #REPORT-PARM #RACF-ID #PARM-UNIT #FLOOR #BLDG #DROP-OFF #TBL-RUN-DATE #TBL-RUN-FLAG
            pnd_Tbl_Run_Flag);
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Comp_Date.lessOrEqual(pnd_Tbl_Run_Date)))                                                                                                       //Natural: IF #COMP-DATE LE #TBL-RUN-DATE
        {
            pnd_No_Extract.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #NO-EXTRACT
        }                                                                                                                                                                 //Natural: END-IF
        //* *
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"*************** START OF ERROR LOG *****************",NEWLINE,"ERROR NO----->",Global.getERROR_NR(),NEWLINE,"LINE--------->",Global.getERROR_LINE(),NEWLINE,"PROGRAM------>",Global.getPROGRAM(),NEWLINE,"PARM PIN----->",cwf_Master_Index_Pin_Nbr,  //Natural: WRITE ( 1 ) / '*************** START OF ERROR LOG *****************' / 'ERROR NO----->' *ERROR-NR / 'LINE--------->' *ERROR-LINE / 'PROGRAM------>' *PROGRAM / 'PARM PIN----->' CWF-MASTER-INDEX.PIN-NBR ( EM = 999999999999 ) / 'PARM WPID---->' CWF-MASTER-INDEX.WORK-PRCSS-ID / 'PARM RQST NO->' CWF-MASTER-INDEX.RQST-LOG-DTE-TME / '**************** END OF ERROR LOG ******************'
            new ReportEditMask ("999999999999"),NEWLINE,"PARM WPID---->",cwf_Master_Index_Work_Prcss_Id,NEWLINE,"PARM RQST NO->",cwf_Master_Index_Rqst_Log_Dte_Tme,
            NEWLINE,"**************** END OF ERROR LOG ******************");
    };                                                                                                                                                                    //Natural: END-ERROR
}
