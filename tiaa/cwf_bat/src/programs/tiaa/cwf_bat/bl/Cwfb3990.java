/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:31:04 PM
**        * FROM NATURAL PROGRAM : Cwfb3990
************************************************************
**        * FILE NAME            : Cwfb3990.java
**        * CLASS NAME           : Cwfb3990
**        * INSTANCE NAME        : Cwfb3990
************************************************************
************************************************************************
* PROGRAM  : CWFB3990
* SYSTEM   : CRPCWF
* TITLE    : EXTRACT WORK PROCESS ID RECORDS
* GENERATED: OCT 05,93 AT 08:41 AM
* FUNCTION : EXTRACTS WPID RECORDS
*          |
*          |
*          |
*          |
*          |
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* AUG 20 97 EPM    1) PASS INSRNCE-PRCSS-IND AT THE END OF WORK FILE.
*                  2) AFTER WRITING ALL WPIDS, WRITE 'END OF FILE' INTO
*                     THE WORK FILE.
*                  3) READ CWF-WP-DCMNT-TYPE-TBL AND PASS DOCUMENT TYPE
*                     AND DOCUMENT DESCRIPTION TO THE SAME WORK FILE.
************************************************************************
* THIS PROGRAM DUMPS THE WPID TBL TO A WORK FILE
*
************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3990 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_wpid_Tbl;
    private DbsField wpid_Tbl_Work_Prcss_Id;
    private DbsField wpid_Tbl_Work_Prcss_Long_Nme;
    private DbsField wpid_Tbl_Work_Prcss_Short_Nme;
    private DbsField wpid_Tbl_Mj_Pull_Ind;
    private DbsField wpid_Tbl_Dlte_Oprtr_Cde;
    private DbsField wpid_Tbl_Insrnce_Prcss_Ind;

    private DataAccessProgramView vw_routing;
    private DbsField routing_Rt_Sqnce_Prcss_Id;
    private DbsField routing_Rt_Sqnce_Nbr;
    private DbsField routing_Unit_Cde;

    private DataAccessProgramView vw_unit_Tbl;
    private DbsField unit_Tbl_Unit_Cde;
    private DbsField unit_Tbl_Unit_Long_Nme;
    private DbsField unit_Tbl_Unit_Short_Nme;

    private DataAccessProgramView vw_doc_Tbl;
    private DbsField doc_Tbl_Dcmnt_Type_Cde;
    private DbsField doc_Tbl_Dcmnt_Nme;
    private DbsField doc_Tbl_Dlte_Oprtr_Cde;
    private DbsField pnd_Workf1;

    private DbsGroup pnd_Workf1__R_Field_1;
    private DbsField pnd_Workf1_Pnd_Wpid;
    private DbsField pnd_Workf1_Pnd_Wpid_Lnme;
    private DbsField pnd_Workf1_Pnd_Wpid_Snme;
    private DbsField pnd_Workf1_Pnd_1st_Unit;
    private DbsField pnd_Workf1_Pnd_Unit_Lnme;
    private DbsField pnd_Workf1_Pnd_Mj_Pull;
    private DbsField pnd_Workf1_Pnd_Insrnce_Prcss_Ind;

    private DbsGroup pnd_Workf1__R_Field_2;
    private DbsField pnd_Workf1_Pnd_Dcmnt_Type_Cde;
    private DbsField pnd_Workf1_Pnd_Dcmnt_Nme;
    private DbsField pnd_Ctr;
    private DbsField pnd_Ctr2;

    private DbsRecord setTimeRecord;
    private DbsField st;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_wpid_Tbl = new DataAccessProgramView(new NameInfo("vw_wpid_Tbl", "WPID-TBL"), "CWF_WP_WORK_PRCSS_ID", "CWF_PROFILE");
        wpid_Tbl_Work_Prcss_Id = vw_wpid_Tbl.getRecord().newFieldInGroup("wpid_Tbl_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        wpid_Tbl_Work_Prcss_Id.setDdmHeader("WORK PRCSS ID");
        wpid_Tbl_Work_Prcss_Long_Nme = vw_wpid_Tbl.getRecord().newFieldInGroup("wpid_Tbl_Work_Prcss_Long_Nme", "WORK-PRCSS-LONG-NME", FieldType.STRING, 
            45, RepeatingFieldStrategy.None, "WORK_PRCSS_LONG_NME");
        wpid_Tbl_Work_Prcss_Long_Nme.setDdmHeader("NAME");
        wpid_Tbl_Work_Prcss_Short_Nme = vw_wpid_Tbl.getRecord().newFieldInGroup("wpid_Tbl_Work_Prcss_Short_Nme", "WORK-PRCSS-SHORT-NME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "WORK_PRCSS_SHORT_NME");
        wpid_Tbl_Work_Prcss_Short_Nme.setDdmHeader("SHORT NAME");
        wpid_Tbl_Mj_Pull_Ind = vw_wpid_Tbl.getRecord().newFieldInGroup("wpid_Tbl_Mj_Pull_Ind", "MJ-PULL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "MJ_PULL_IND");
        wpid_Tbl_Mj_Pull_Ind.setDdmHeader("MJ/IND");
        wpid_Tbl_Dlte_Oprtr_Cde = vw_wpid_Tbl.getRecord().newFieldInGroup("wpid_Tbl_Dlte_Oprtr_Cde", "DLTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "DLTE_OPRTR_CDE");
        wpid_Tbl_Dlte_Oprtr_Cde.setDdmHeader("DELETE/OPER");
        wpid_Tbl_Insrnce_Prcss_Ind = vw_wpid_Tbl.getRecord().newFieldInGroup("wpid_Tbl_Insrnce_Prcss_Ind", "INSRNCE-PRCSS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "INSRNCE_PRCSS_IND");
        wpid_Tbl_Insrnce_Prcss_Ind.setDdmHeader("INSURANCE/PROCESS");
        registerRecord(vw_wpid_Tbl);

        vw_routing = new DataAccessProgramView(new NameInfo("vw_routing", "ROUTING"), "CWF_WP_ROUTING", "CWF_PROFILE");
        routing_Rt_Sqnce_Prcss_Id = vw_routing.getRecord().newFieldInGroup("routing_Rt_Sqnce_Prcss_Id", "RT-SQNCE-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "RT_SQNCE_PRCSS_ID");
        routing_Rt_Sqnce_Nbr = vw_routing.getRecord().newFieldInGroup("routing_Rt_Sqnce_Nbr", "RT-SQNCE-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "RT_SQNCE_NBR");
        routing_Rt_Sqnce_Nbr.setDdmHeader("ROUTING/SEQUENCE");
        routing_Unit_Cde = vw_routing.getRecord().newFieldInGroup("routing_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "UNIT_CDE");
        routing_Unit_Cde.setDdmHeader("UNIT/CODE");
        registerRecord(vw_routing);

        vw_unit_Tbl = new DataAccessProgramView(new NameInfo("vw_unit_Tbl", "UNIT-TBL"), "CWF_ORG_UNIT_TBL", "CWF_ASSIGN_RULE");
        unit_Tbl_Unit_Cde = vw_unit_Tbl.getRecord().newFieldInGroup("unit_Tbl_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "UNIT_CDE");
        unit_Tbl_Unit_Cde.setDdmHeader("UNIT/CODE");
        unit_Tbl_Unit_Long_Nme = vw_unit_Tbl.getRecord().newFieldInGroup("unit_Tbl_Unit_Long_Nme", "UNIT-LONG-NME", FieldType.STRING, 45, RepeatingFieldStrategy.None, 
            "UNIT_LONG_NME");
        unit_Tbl_Unit_Long_Nme.setDdmHeader("UNIT LONG NAME");
        unit_Tbl_Unit_Short_Nme = vw_unit_Tbl.getRecord().newFieldInGroup("unit_Tbl_Unit_Short_Nme", "UNIT-SHORT-NME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "UNIT_SHORT_NME");
        unit_Tbl_Unit_Short_Nme.setDdmHeader("UNIT SHORT NAME");
        registerRecord(vw_unit_Tbl);

        vw_doc_Tbl = new DataAccessProgramView(new NameInfo("vw_doc_Tbl", "DOC-TBL"), "CWF_WP_DCMNT_TYPE_TBL", "CWF_PROFILE");
        doc_Tbl_Dcmnt_Type_Cde = vw_doc_Tbl.getRecord().newFieldInGroup("doc_Tbl_Dcmnt_Type_Cde", "DCMNT-TYPE-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "DCMNT_TYPE_CDE");
        doc_Tbl_Dcmnt_Type_Cde.setDdmHeader("DOCTYPE");
        doc_Tbl_Dcmnt_Nme = vw_doc_Tbl.getRecord().newFieldInGroup("doc_Tbl_Dcmnt_Nme", "DCMNT-NME", FieldType.STRING, 45, RepeatingFieldStrategy.None, 
            "DCMNT_NME");
        doc_Tbl_Dcmnt_Nme.setDdmHeader("DOCNAME");
        doc_Tbl_Dlte_Oprtr_Cde = vw_doc_Tbl.getRecord().newFieldInGroup("doc_Tbl_Dlte_Oprtr_Cde", "DLTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "DLTE_OPRTR_CDE");
        doc_Tbl_Dlte_Oprtr_Cde.setDdmHeader("DELETE/OPER");
        registerRecord(vw_doc_Tbl);

        pnd_Workf1 = localVariables.newFieldInRecord("pnd_Workf1", "#WORKF1", FieldType.STRING, 121);

        pnd_Workf1__R_Field_1 = localVariables.newGroupInRecord("pnd_Workf1__R_Field_1", "REDEFINE", pnd_Workf1);
        pnd_Workf1_Pnd_Wpid = pnd_Workf1__R_Field_1.newFieldInGroup("pnd_Workf1_Pnd_Wpid", "#WPID", FieldType.STRING, 6);
        pnd_Workf1_Pnd_Wpid_Lnme = pnd_Workf1__R_Field_1.newFieldInGroup("pnd_Workf1_Pnd_Wpid_Lnme", "#WPID-LNME", FieldType.STRING, 45);
        pnd_Workf1_Pnd_Wpid_Snme = pnd_Workf1__R_Field_1.newFieldInGroup("pnd_Workf1_Pnd_Wpid_Snme", "#WPID-SNME", FieldType.STRING, 15);
        pnd_Workf1_Pnd_1st_Unit = pnd_Workf1__R_Field_1.newFieldInGroup("pnd_Workf1_Pnd_1st_Unit", "#1ST-UNIT", FieldType.STRING, 8);
        pnd_Workf1_Pnd_Unit_Lnme = pnd_Workf1__R_Field_1.newFieldInGroup("pnd_Workf1_Pnd_Unit_Lnme", "#UNIT-LNME", FieldType.STRING, 45);
        pnd_Workf1_Pnd_Mj_Pull = pnd_Workf1__R_Field_1.newFieldInGroup("pnd_Workf1_Pnd_Mj_Pull", "#MJ-PULL", FieldType.STRING, 1);
        pnd_Workf1_Pnd_Insrnce_Prcss_Ind = pnd_Workf1__R_Field_1.newFieldInGroup("pnd_Workf1_Pnd_Insrnce_Prcss_Ind", "#INSRNCE-PRCSS-IND", FieldType.STRING, 
            1);

        pnd_Workf1__R_Field_2 = localVariables.newGroupInRecord("pnd_Workf1__R_Field_2", "REDEFINE", pnd_Workf1);
        pnd_Workf1_Pnd_Dcmnt_Type_Cde = pnd_Workf1__R_Field_2.newFieldInGroup("pnd_Workf1_Pnd_Dcmnt_Type_Cde", "#DCMNT-TYPE-CDE", FieldType.STRING, 8);
        pnd_Workf1_Pnd_Dcmnt_Nme = pnd_Workf1__R_Field_2.newFieldInGroup("pnd_Workf1_Pnd_Dcmnt_Nme", "#DCMNT-NME", FieldType.STRING, 45);
        pnd_Ctr = localVariables.newFieldInRecord("pnd_Ctr", "#CTR", FieldType.PACKED_DECIMAL, 5);
        pnd_Ctr2 = localVariables.newFieldInRecord("pnd_Ctr2", "#CTR2", FieldType.PACKED_DECIMAL, 5);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        setTimeRecord = new DbsRecord();
        st = setTimeRecord.newFieldInRecord("ST", "ST", FieldType.NUMERIC, 7);
        registerRecord(setTimeRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_wpid_Tbl.reset();
        vw_routing.reset();
        vw_unit_Tbl.reset();
        vw_doc_Tbl.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb3990() throws Exception
    {
        super("Cwfb3990");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        st = Global.getTIMN();                                                                                                                                            //Natural: SET TIME
        //*                                                                                                                                                               //Natural: FORMAT PS = 58 LS = 132
        vw_wpid_Tbl.startDatabaseRead                                                                                                                                     //Natural: READ WPID-TBL BY WPID-UNIQ-KEY
        (
        "WPID",
        new Oc[] { new Oc("WPID_UNIQ_KEY", "ASC") }
        );
        WPID:
        while (condition(vw_wpid_Tbl.readNextRow("WPID")))
        {
            if (condition(wpid_Tbl_Dlte_Oprtr_Cde.greater(" ")))                                                                                                          //Natural: REJECT IF WPID-TBL.DLTE-OPRTR-CDE GT ' '
            {
                continue;
            }
            unit_Tbl_Unit_Long_Nme.setValue(" ");                                                                                                                         //Natural: MOVE ' ' TO UNIT-TBL.UNIT-LONG-NME ROUTING.UNIT-CDE
            routing_Unit_Cde.setValue(" ");
            vw_routing.startDatabaseRead                                                                                                                                  //Natural: READ ( 1 ) ROUTING BY ROUTING-SQNCE-KEY FROM WORK-PRCSS-ID
            (
            "READ01",
            new Wc[] { new Wc("ROUTING_SQNCE_KEY", ">=", wpid_Tbl_Work_Prcss_Id, WcType.BY) },
            new Oc[] { new Oc("ROUTING_SQNCE_KEY", "ASC") },
            1
            );
            READ01:
            while (condition(vw_routing.readNextRow("READ01")))
            {
                if (condition(routing_Rt_Sqnce_Prcss_Id.notEquals(wpid_Tbl_Work_Prcss_Id)))                                                                               //Natural: IF RT-SQNCE-PRCSS-ID NE WORK-PRCSS-ID
                {
                    routing_Unit_Cde.setValue(" ");                                                                                                                       //Natural: MOVE ' ' TO ROUTING.UNIT-CDE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    vw_unit_Tbl.startDatabaseRead                                                                                                                         //Natural: READ ( 1 ) UNIT-TBL BY UNIQ-UNIT-CDE FROM ROUTING.UNIT-CDE
                    (
                    "READ02",
                    new Wc[] { new Wc("UNIQ_UNIT_CDE", ">=", routing_Unit_Cde, WcType.BY) },
                    new Oc[] { new Oc("UNIQ_UNIT_CDE", "ASC") },
                    1
                    );
                    READ02:
                    while (condition(vw_unit_Tbl.readNextRow("READ02")))
                    {
                        if (condition(unit_Tbl_Unit_Cde.notEquals(routing_Unit_Cde)))                                                                                     //Natural: IF UNIT-TBL.UNIT-CDE NE ROUTING.UNIT-CDE
                        {
                            unit_Tbl_Unit_Long_Nme.setValue(" ");                                                                                                         //Natural: MOVE ' ' TO UNIT-TBL.UNIT-LONG-NME
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-READ
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("WPID"))) break;
                else if (condition(Global.isEscapeBottomImmediate("WPID"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  D-READ
            pnd_Ctr.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #CTR
            pnd_Workf1_Pnd_Wpid.setValue(wpid_Tbl_Work_Prcss_Id);                                                                                                         //Natural: MOVE WORK-PRCSS-ID TO #WPID
            pnd_Workf1_Pnd_Wpid_Lnme.setValue(wpid_Tbl_Work_Prcss_Long_Nme);                                                                                              //Natural: MOVE WORK-PRCSS-LONG-NME TO #WPID-LNME
            pnd_Workf1_Pnd_Wpid_Snme.setValue(wpid_Tbl_Work_Prcss_Short_Nme);                                                                                             //Natural: MOVE WORK-PRCSS-SHORT-NME TO #WPID-SNME
            pnd_Workf1_Pnd_1st_Unit.setValue(routing_Unit_Cde);                                                                                                           //Natural: MOVE ROUTING.UNIT-CDE TO #1ST-UNIT
            pnd_Workf1_Pnd_Unit_Lnme.setValue(unit_Tbl_Unit_Long_Nme);                                                                                                    //Natural: MOVE UNIT-TBL.UNIT-LONG-NME TO #UNIT-LNME
            pnd_Workf1_Pnd_Mj_Pull.setValue(wpid_Tbl_Mj_Pull_Ind);                                                                                                        //Natural: MOVE MJ-PULL-IND TO #MJ-PULL
            pnd_Workf1_Pnd_Insrnce_Prcss_Ind.setValue(wpid_Tbl_Insrnce_Prcss_Ind);                                                                                        //Natural: MOVE INSRNCE-PRCSS-IND TO #INSRNCE-PRCSS-IND
            getWorkFiles().write(1, false, pnd_Workf1);                                                                                                                   //Natural: WRITE WORK FILE 1 #WORKF1
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Workf1.setValue("**********END-OF-WPID-TABLE**********");                                                                                                     //Natural: MOVE '**********END-OF-WPID-TABLE**********' TO #WORKF1
        pnd_Ctr2.reset();                                                                                                                                                 //Natural: RESET #CTR2
        getWorkFiles().write(1, false, pnd_Workf1);                                                                                                                       //Natural: WRITE WORK FILE 1 #WORKF1
        vw_doc_Tbl.startDatabaseRead                                                                                                                                      //Natural: READ DOC-TBL BY DCMNT-TYPE-CDE-KEY
        (
        "DOC",
        new Oc[] { new Oc("DCMNT_TYPE_CDE_KEY", "ASC") }
        );
        DOC:
        while (condition(vw_doc_Tbl.readNextRow("DOC")))
        {
            if (condition(doc_Tbl_Dlte_Oprtr_Cde.greater(" ")))                                                                                                           //Natural: REJECT IF DOC-TBL.DLTE-OPRTR-CDE GT ' '
            {
                continue;
            }
            pnd_Workf1_Pnd_Dcmnt_Type_Cde.setValue(doc_Tbl_Dcmnt_Type_Cde);                                                                                               //Natural: MOVE DCMNT-TYPE-CDE TO #DCMNT-TYPE-CDE
            pnd_Workf1_Pnd_Dcmnt_Nme.setValue(doc_Tbl_Dcmnt_Nme);                                                                                                         //Natural: MOVE DCMNT-NME TO #DCMNT-NME
            getWorkFiles().write(1, false, pnd_Workf1);                                                                                                                   //Natural: WRITE WORK FILE 1 #WORKF1
            pnd_Ctr2.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CTR2
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Workf1.setValue("**********END-OF-DOC-TABLE**********");                                                                                                      //Natural: MOVE '**********END-OF-DOC-TABLE**********' TO #WORKF1
        getWorkFiles().write(1, false, pnd_Workf1);                                                                                                                       //Natural: WRITE WORK FILE 1 #WORKF1
        getReports().write(0, NEWLINE,NEWLINE,"TIME ELAPSED :",st, new ReportEditMask ("99':'99':'99'.'9"),NEWLINE,"TOTAL WPID RECORDS:",pnd_Ctr,NEWLINE,                 //Natural: WRITE // 'TIME ELAPSED :' *TIMD ( ST. ) ( EM = 99':'99':'99'.'9 ) /'TOTAL WPID RECORDS:' #CTR /'TOTAL DOC  RECORDS:' #CTR2
            "TOTAL DOC  RECORDS:",pnd_Ctr2);
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=58 LS=132");
    }
}
