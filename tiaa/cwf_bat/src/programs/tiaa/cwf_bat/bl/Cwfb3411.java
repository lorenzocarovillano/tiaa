/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:29:21 PM
**        * FROM NATURAL PROGRAM : Cwfb3411
************************************************************
**        * FILE NAME            : Cwfb3411.java
**        * CLASS NAME           : Cwfb3411
**        * INSTANCE NAME        : Cwfb3411
************************************************************
**SAG GENERATOR: BATCH-TIAA                       VERSION: 3.2.2
**SAG TITLE: TAX REPORT
**SAG SYSTEM: CRPCWF
**SAG GDA: CWFG000
**SAG REPORT-HEADING(1): CORPORATE WORKFLOW FACILITIES
**SAG PRINT-FILE(1): 1
**SAG REPORT-HEADING(2): WEEKLY TAX REPORT OF MODIFIED PROCESSING CASES
**SAG PRINT-FILE(2): 1
**SAG HEADING-LINE: 12000000
**SAG DESCS(1): THIS PROGRAM ...
**SAG PRIMARY-FILE: CWF-MASTER-INDEX-VIEW
************************************************************************
* PROGRAM  : CWFB3411
* SYSTEM   : CRPCWF
* TITLE    : TAX REPORT
* GENERATED: OCT 19,94 AT 12:21 PM
* FUNCTION : THIS PROGRAM ...
*
*
*
* HISTORY
* 02/23/2017 - PIN EXPANSION - AUG 2017 <STOW ONLY>
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3411 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_master_Index;
    private DbsField master_Index_Rqst_Log_Dte_Tme;
    private DbsField master_Index_Last_Chnge_Unit_Cde;
    private DbsField master_Index_Admin_Unit_Cde;
    private DbsField master_Index_Work_Prcss_Id;
    private DbsField master_Index_Status_Updte_Dte_Tme;
    private DbsField master_Index_Pin_Nbr;
    private DbsField master_Index_Physcl_Fldr_Id_Nbr;
    private DbsField master_Index_Mj_Pull_Ind;
    private DbsField master_Index_Tiaa_Rcvd_Dte;
    private DbsField master_Index_Effctve_Dte;
    private DbsField master_Index_Trans_Dte;
    private DbsField master_Index_Admin_Status_Cde;
    private DbsField master_Index_Admin_Status_Updte_Dte_Tme;
    private DbsField master_Index_Last_Chnge_Dte_Tme;
    private DbsField master_Index_Actve_Ind;
    private DbsField master_Index_Status_Freeze_Ind;

    private DataAccessProgramView vw_hist_View;
    private DbsField hist_View_Rqst_Log_Dte_Tme;
    private DbsField hist_View_Last_Chnge_Unit_Cde;
    private DbsField hist_View_Admin_Unit_Cde;
    private DbsField hist_View_Work_Prcss_Id;
    private DbsField hist_View_Status_Updte_Dte_Tme;
    private DbsField hist_View_Pin_Nbr;
    private DbsField hist_View_Physcl_Fldr_Id_Nbr;
    private DbsField hist_View_Mj_Pull_Ind;
    private DbsField hist_View_Tiaa_Rcvd_Dte;
    private DbsField hist_View_Effctve_Dte;
    private DbsField hist_View_Trans_Dte;
    private DbsField hist_View_Status_Cde;
    private DbsField hist_View_Admin_Status_Cde;
    private DbsField hist_View_Admin_Status_Updte_Dte_Tme;
    private DbsField pnd_Start_Dte_Tme;

    private DbsGroup pnd_Start_Dte_Tme__R_Field_1;
    private DbsField pnd_Start_Dte_Tme_Pnd_Start_Dte_Tme_N;

    private DbsGroup pnd_Start_Dte_Tme__R_Field_2;
    private DbsField pnd_Start_Dte_Tme_Pnd_Start_Date;

    private DbsGroup pnd_Start_Dte_Tme__R_Field_3;
    private DbsField pnd_Start_Dte_Tme_Pnd_Start_Dte;
    private DbsField pnd_Start_Dte_Tme_Pnd_Start_Time;
    private DbsField pnd_End_Dte;

    private DbsGroup pnd_End_Dte__R_Field_4;
    private DbsField pnd_End_Dte_Pnd_End_Date;

    private DbsGroup pnd_End_Dte__R_Field_5;
    private DbsField pnd_End_Dte_Pnd_End_Date_N;
    private DbsField pnd_End_Dte_Pnd_End_Time;

    private DbsGroup pnd_End_Dte__R_Field_6;
    private DbsField pnd_End_Dte_Pnd_End_Dte_N;
    private DbsField pnd_Hist_Ctr;
    private DbsField pnd_Physcl_Fldr_Id_Nbr;

    private DbsGroup pnd_Physcl_Fldr_Id_Nbr__R_Field_7;
    private DbsField pnd_Physcl_Fldr_Id_Nbr_Pnd_Mj_Prefix;
    private DbsField pnd_Physcl_Fldr_Id_Nbr_Pnd_Mj_Id;
    private DbsField pnd_Unit_Cde;
    private DbsField pnd_Unit_Name;
    private DbsField pnd_Parm_Status_Key;

    private DbsGroup pnd_Parm_Status_Key__R_Field_8;
    private DbsField pnd_Parm_Status_Key_Pnd_Parm_Status_Unit;
    private DbsField pnd_Parm_Status_Key_Pnd_Parm_Status_Code;
    private DbsField pnd_Parm_Status_Name;
    private DbsField pnd_Parm_Status_Cde_Name;
    private DbsField pnd_Old_Rt_Cde;
    private DbsField pnd_Wpid_Sname;
    private DbsField pnd_Wpid_Lname;
    private DbsField pnd_Wpid_Cde_Name;
    private DbsField pnd_Parm_Status_Wpid;
    private DbsField pnd_Source;
    private DbsField pnd_Page_Number;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_master_Index = new DataAccessProgramView(new NameInfo("vw_master_Index", "MASTER-INDEX"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        master_Index_Rqst_Log_Dte_Tme = vw_master_Index.getRecord().newFieldInGroup("master_Index_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        master_Index_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        master_Index_Last_Chnge_Unit_Cde = vw_master_Index.getRecord().newFieldInGroup("master_Index_Last_Chnge_Unit_Cde", "LAST-CHNGE-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "LAST_CHNGE_UNIT_CDE");
        master_Index_Last_Chnge_Unit_Cde.setDdmHeader("LAST/CHANGE/UNIT");
        master_Index_Admin_Unit_Cde = vw_master_Index.getRecord().newFieldInGroup("master_Index_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "ADMIN_UNIT_CDE");
        master_Index_Work_Prcss_Id = vw_master_Index.getRecord().newFieldInGroup("master_Index_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        master_Index_Work_Prcss_Id.setDdmHeader("WORK/ID");
        master_Index_Status_Updte_Dte_Tme = vw_master_Index.getRecord().newFieldInGroup("master_Index_Status_Updte_Dte_Tme", "STATUS-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "STATUS_UPDTE_DTE_TME");
        master_Index_Status_Updte_Dte_Tme.setDdmHeader("STATUS UPDATE/DATE-TIME");
        master_Index_Pin_Nbr = vw_master_Index.getRecord().newFieldInGroup("master_Index_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "PIN_NBR");
        master_Index_Pin_Nbr.setDdmHeader("PIN");
        master_Index_Physcl_Fldr_Id_Nbr = vw_master_Index.getRecord().newFieldInGroup("master_Index_Physcl_Fldr_Id_Nbr", "PHYSCL-FLDR-ID-NBR", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "PHYSCL_FLDR_ID_NBR");
        master_Index_Physcl_Fldr_Id_Nbr.setDdmHeader("PHYSICAL FOLDER/ID NUMBER");
        master_Index_Mj_Pull_Ind = vw_master_Index.getRecord().newFieldInGroup("master_Index_Mj_Pull_Ind", "MJ-PULL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "MJ_PULL_IND");
        master_Index_Mj_Pull_Ind.setDdmHeader("MJ PULL/IND");
        master_Index_Tiaa_Rcvd_Dte = vw_master_Index.getRecord().newFieldInGroup("master_Index_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIAA_RCVD_DTE");
        master_Index_Effctve_Dte = vw_master_Index.getRecord().newFieldInGroup("master_Index_Effctve_Dte", "EFFCTVE-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "EFFCTVE_DTE");
        master_Index_Trans_Dte = vw_master_Index.getRecord().newFieldInGroup("master_Index_Trans_Dte", "TRANS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        master_Index_Trans_Dte.setDdmHeader("CHECK MAIL/DATE");
        master_Index_Admin_Status_Cde = vw_master_Index.getRecord().newFieldInGroup("master_Index_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "ADMIN_STATUS_CDE");
        master_Index_Admin_Status_Updte_Dte_Tme = vw_master_Index.getRecord().newFieldInGroup("master_Index_Admin_Status_Updte_Dte_Tme", "ADMIN-STATUS-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_DTE_TME");
        master_Index_Last_Chnge_Dte_Tme = vw_master_Index.getRecord().newFieldInGroup("master_Index_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 
            15, RepeatingFieldStrategy.None, "LAST_CHNGE_DTE_TME");
        master_Index_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");
        master_Index_Actve_Ind = vw_master_Index.getRecord().newFieldInGroup("master_Index_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTVE_IND");
        master_Index_Status_Freeze_Ind = vw_master_Index.getRecord().newFieldInGroup("master_Index_Status_Freeze_Ind", "STATUS-FREEZE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "STATUS_FREEZE_IND");
        master_Index_Status_Freeze_Ind.setDdmHeader("FREEZE/STATUS");
        registerRecord(vw_master_Index);

        vw_hist_View = new DataAccessProgramView(new NameInfo("vw_hist_View", "HIST-VIEW"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        hist_View_Rqst_Log_Dte_Tme = vw_hist_View.getRecord().newFieldInGroup("hist_View_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15, 
            RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        hist_View_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        hist_View_Last_Chnge_Unit_Cde = vw_hist_View.getRecord().newFieldInGroup("hist_View_Last_Chnge_Unit_Cde", "LAST-CHNGE-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "LAST_CHNGE_UNIT_CDE");
        hist_View_Last_Chnge_Unit_Cde.setDdmHeader("LAST/CHANGE/UNIT");
        hist_View_Admin_Unit_Cde = vw_hist_View.getRecord().newFieldInGroup("hist_View_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ADMIN_UNIT_CDE");
        hist_View_Work_Prcss_Id = vw_hist_View.getRecord().newFieldInGroup("hist_View_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        hist_View_Work_Prcss_Id.setDdmHeader("WORK/ID");
        hist_View_Status_Updte_Dte_Tme = vw_hist_View.getRecord().newFieldInGroup("hist_View_Status_Updte_Dte_Tme", "STATUS-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "STATUS_UPDTE_DTE_TME");
        hist_View_Status_Updte_Dte_Tme.setDdmHeader("STATUS UPDATE/DATE-TIME");
        hist_View_Pin_Nbr = vw_hist_View.getRecord().newFieldInGroup("hist_View_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "PIN_NBR");
        hist_View_Pin_Nbr.setDdmHeader("PIN");
        hist_View_Physcl_Fldr_Id_Nbr = vw_hist_View.getRecord().newFieldInGroup("hist_View_Physcl_Fldr_Id_Nbr", "PHYSCL-FLDR-ID-NBR", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "PHYSCL_FLDR_ID_NBR");
        hist_View_Physcl_Fldr_Id_Nbr.setDdmHeader("PHYSICAL FOLDER/ID NUMBER");
        hist_View_Mj_Pull_Ind = vw_hist_View.getRecord().newFieldInGroup("hist_View_Mj_Pull_Ind", "MJ-PULL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "MJ_PULL_IND");
        hist_View_Mj_Pull_Ind.setDdmHeader("MJ PULL/IND");
        hist_View_Tiaa_Rcvd_Dte = vw_hist_View.getRecord().newFieldInGroup("hist_View_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIAA_RCVD_DTE");
        hist_View_Effctve_Dte = vw_hist_View.getRecord().newFieldInGroup("hist_View_Effctve_Dte", "EFFCTVE-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "EFFCTVE_DTE");
        hist_View_Trans_Dte = vw_hist_View.getRecord().newFieldInGroup("hist_View_Trans_Dte", "TRANS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        hist_View_Trans_Dte.setDdmHeader("CHECK MAIL/DATE");
        hist_View_Status_Cde = vw_hist_View.getRecord().newFieldInGroup("hist_View_Status_Cde", "STATUS-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "STATUS_CDE");
        hist_View_Admin_Status_Cde = vw_hist_View.getRecord().newFieldInGroup("hist_View_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "ADMIN_STATUS_CDE");
        hist_View_Admin_Status_Updte_Dte_Tme = vw_hist_View.getRecord().newFieldInGroup("hist_View_Admin_Status_Updte_Dte_Tme", "ADMIN-STATUS-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_DTE_TME");
        registerRecord(vw_hist_View);

        pnd_Start_Dte_Tme = localVariables.newFieldInRecord("pnd_Start_Dte_Tme", "#START-DTE-TME", FieldType.STRING, 15);

        pnd_Start_Dte_Tme__R_Field_1 = localVariables.newGroupInRecord("pnd_Start_Dte_Tme__R_Field_1", "REDEFINE", pnd_Start_Dte_Tme);
        pnd_Start_Dte_Tme_Pnd_Start_Dte_Tme_N = pnd_Start_Dte_Tme__R_Field_1.newFieldInGroup("pnd_Start_Dte_Tme_Pnd_Start_Dte_Tme_N", "#START-DTE-TME-N", 
            FieldType.NUMERIC, 15);

        pnd_Start_Dte_Tme__R_Field_2 = localVariables.newGroupInRecord("pnd_Start_Dte_Tme__R_Field_2", "REDEFINE", pnd_Start_Dte_Tme);
        pnd_Start_Dte_Tme_Pnd_Start_Date = pnd_Start_Dte_Tme__R_Field_2.newFieldInGroup("pnd_Start_Dte_Tme_Pnd_Start_Date", "#START-DATE", FieldType.STRING, 
            8);

        pnd_Start_Dte_Tme__R_Field_3 = pnd_Start_Dte_Tme__R_Field_2.newGroupInGroup("pnd_Start_Dte_Tme__R_Field_3", "REDEFINE", pnd_Start_Dte_Tme_Pnd_Start_Date);
        pnd_Start_Dte_Tme_Pnd_Start_Dte = pnd_Start_Dte_Tme__R_Field_3.newFieldInGroup("pnd_Start_Dte_Tme_Pnd_Start_Dte", "#START-DTE", FieldType.NUMERIC, 
            8);
        pnd_Start_Dte_Tme_Pnd_Start_Time = pnd_Start_Dte_Tme__R_Field_2.newFieldInGroup("pnd_Start_Dte_Tme_Pnd_Start_Time", "#START-TIME", FieldType.STRING, 
            7);
        pnd_End_Dte = localVariables.newFieldInRecord("pnd_End_Dte", "#END-DTE", FieldType.STRING, 15);

        pnd_End_Dte__R_Field_4 = localVariables.newGroupInRecord("pnd_End_Dte__R_Field_4", "REDEFINE", pnd_End_Dte);
        pnd_End_Dte_Pnd_End_Date = pnd_End_Dte__R_Field_4.newFieldInGroup("pnd_End_Dte_Pnd_End_Date", "#END-DATE", FieldType.STRING, 8);

        pnd_End_Dte__R_Field_5 = pnd_End_Dte__R_Field_4.newGroupInGroup("pnd_End_Dte__R_Field_5", "REDEFINE", pnd_End_Dte_Pnd_End_Date);
        pnd_End_Dte_Pnd_End_Date_N = pnd_End_Dte__R_Field_5.newFieldInGroup("pnd_End_Dte_Pnd_End_Date_N", "#END-DATE-N", FieldType.NUMERIC, 8);
        pnd_End_Dte_Pnd_End_Time = pnd_End_Dte__R_Field_4.newFieldInGroup("pnd_End_Dte_Pnd_End_Time", "#END-TIME", FieldType.STRING, 7);

        pnd_End_Dte__R_Field_6 = localVariables.newGroupInRecord("pnd_End_Dte__R_Field_6", "REDEFINE", pnd_End_Dte);
        pnd_End_Dte_Pnd_End_Dte_N = pnd_End_Dte__R_Field_6.newFieldInGroup("pnd_End_Dte_Pnd_End_Dte_N", "#END-DTE-N", FieldType.NUMERIC, 15);
        pnd_Hist_Ctr = localVariables.newFieldInRecord("pnd_Hist_Ctr", "#HIST-CTR", FieldType.PACKED_DECIMAL, 6);
        pnd_Physcl_Fldr_Id_Nbr = localVariables.newFieldInRecord("pnd_Physcl_Fldr_Id_Nbr", "#PHYSCL-FLDR-ID-NBR", FieldType.STRING, 7);

        pnd_Physcl_Fldr_Id_Nbr__R_Field_7 = localVariables.newGroupInRecord("pnd_Physcl_Fldr_Id_Nbr__R_Field_7", "REDEFINE", pnd_Physcl_Fldr_Id_Nbr);
        pnd_Physcl_Fldr_Id_Nbr_Pnd_Mj_Prefix = pnd_Physcl_Fldr_Id_Nbr__R_Field_7.newFieldInGroup("pnd_Physcl_Fldr_Id_Nbr_Pnd_Mj_Prefix", "#MJ-PREFIX", 
            FieldType.STRING, 1);
        pnd_Physcl_Fldr_Id_Nbr_Pnd_Mj_Id = pnd_Physcl_Fldr_Id_Nbr__R_Field_7.newFieldInGroup("pnd_Physcl_Fldr_Id_Nbr_Pnd_Mj_Id", "#MJ-ID", FieldType.NUMERIC, 
            6);
        pnd_Unit_Cde = localVariables.newFieldInRecord("pnd_Unit_Cde", "#UNIT-CDE", FieldType.STRING, 8);
        pnd_Unit_Name = localVariables.newFieldInRecord("pnd_Unit_Name", "#UNIT-NAME", FieldType.STRING, 45);
        pnd_Parm_Status_Key = localVariables.newFieldInRecord("pnd_Parm_Status_Key", "#PARM-STATUS-KEY", FieldType.STRING, 13);

        pnd_Parm_Status_Key__R_Field_8 = localVariables.newGroupInRecord("pnd_Parm_Status_Key__R_Field_8", "REDEFINE", pnd_Parm_Status_Key);
        pnd_Parm_Status_Key_Pnd_Parm_Status_Unit = pnd_Parm_Status_Key__R_Field_8.newFieldInGroup("pnd_Parm_Status_Key_Pnd_Parm_Status_Unit", "#PARM-STATUS-UNIT", 
            FieldType.STRING, 8);
        pnd_Parm_Status_Key_Pnd_Parm_Status_Code = pnd_Parm_Status_Key__R_Field_8.newFieldInGroup("pnd_Parm_Status_Key_Pnd_Parm_Status_Code", "#PARM-STATUS-CODE", 
            FieldType.STRING, 4);
        pnd_Parm_Status_Name = localVariables.newFieldInRecord("pnd_Parm_Status_Name", "#PARM-STATUS-NAME", FieldType.STRING, 25);
        pnd_Parm_Status_Cde_Name = localVariables.newFieldInRecord("pnd_Parm_Status_Cde_Name", "#PARM-STATUS-CDE-NAME", FieldType.STRING, 32);
        pnd_Old_Rt_Cde = localVariables.newFieldInRecord("pnd_Old_Rt_Cde", "#OLD-RT-CDE", FieldType.STRING, 4);
        pnd_Wpid_Sname = localVariables.newFieldInRecord("pnd_Wpid_Sname", "#WPID-SNAME", FieldType.STRING, 15);
        pnd_Wpid_Lname = localVariables.newFieldInRecord("pnd_Wpid_Lname", "#WPID-LNAME", FieldType.STRING, 45);
        pnd_Wpid_Cde_Name = localVariables.newFieldInRecord("pnd_Wpid_Cde_Name", "#WPID-CDE-NAME", FieldType.STRING, 24);
        pnd_Parm_Status_Wpid = localVariables.newFieldInRecord("pnd_Parm_Status_Wpid", "#PARM-STATUS-WPID", FieldType.STRING, 6);
        pnd_Source = localVariables.newFieldInRecord("pnd_Source", "#SOURCE", FieldType.STRING, 7);
        pnd_Page_Number = localVariables.newFieldInRecord("pnd_Page_Number", "#PAGE-NUMBER", FieldType.NUMERIC, 6);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_master_Index.reset();
        vw_hist_View.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Cwfb3411() throws Exception
    {
        super("Cwfb3411");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Cwfb3411|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        while(true)
        {
            try
            {
                pnd_Page_Number.reset();                                                                                                                                  //Natural: FORMAT ( 1 ) LS = 140 PS = 60;//Natural: RESET #PAGE-NUMBER
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Start_Dte_Tme_Pnd_Start_Date,pnd_End_Dte_Pnd_End_Date);                                            //Natural: INPUT #START-DATE #END-DATE
                pnd_Start_Dte_Tme_Pnd_Start_Time.setValue("0000000");                                                                                                     //Natural: MOVE '0000000' TO #START-TIME
                pnd_End_Dte_Pnd_End_Time.setValue("2359599");                                                                                                             //Natural: MOVE '2359599' TO #END-TIME
                pnd_Unit_Cde.setValue("TAXRC");                                                                                                                           //Natural: MOVE 'TAXRC' TO #UNIT-CDE
                DbsUtil.callnat(Cwfn1103.class , getCurrentProcessState(), pnd_Unit_Cde, pnd_Unit_Name);                                                                  //Natural: CALLNAT 'CWFN1103' #UNIT-CDE #UNIT-NAME
                if (condition(Global.isEscape())) return;
                //*  ---------------------------------------------------------
                //*  ---------------------------------------------------------                                                                                            //Natural: AT TOP OF PAGE ( 1 )
                //*  R1. READ MASTER-INDEX BY ACTV-UNQUE-KEY
                vw_master_Index.startDatabaseFind                                                                                                                         //Natural: FIND MASTER-INDEX WITH LAST-CHNGE-DTE-KEY = #START-DTE THRU #END-DATE-N
                (
                "R1",
                new Wc[] { new Wc("LAST_CHNGE_DTE_KEY", "<=", pnd_End_Dte_Pnd_End_Date_N, WcType.WITH) }
                );
                R1:
                while (condition(vw_master_Index.readNextRow("R1")))
                {
                    vw_master_Index.setIfNotFoundControlFlag(false);
                    if (condition(!(((master_Index_Actve_Ind.greater(" ") && master_Index_Status_Freeze_Ind.greater(" ")) && (((DbsUtil.maskMatches(master_Index_Work_Prcss_Id,"'TIAD'..")  //Natural: ACCEPT MASTER-INDEX.ACTVE-IND GT ' ' AND MASTER-INDEX.STATUS-FREEZE-IND GT ' ' AND ( MASTER-INDEX.WORK-PRCSS-ID = MASK ( 'TIAD'.. ) OR = MASK ( 'T'....'M' ) OR = MASK ( 'TA'..'CH' ) OR = MASK ( 'TA'..'SP' ) )
                        || DbsUtil.maskMatches(master_Index_Work_Prcss_Id,"'T'....'M'")) || DbsUtil.maskMatches(master_Index_Work_Prcss_Id,"'TA'..'CH'")) 
                        || DbsUtil.maskMatches(master_Index_Work_Prcss_Id,"'TA'..'SP'"))))))
                    {
                        continue;
                    }
                    //*  ---------------------------------------------------------
                    pnd_Hist_Ctr.reset();                                                                                                                                 //Natural: RESET #HIST-CTR
                    vw_hist_View.startDatabaseRead                                                                                                                        //Natural: READ HIST-VIEW BY RQST-HISTORY-KEY FROM MASTER-INDEX.RQST-LOG-DTE-TME
                    (
                    "R2",
                    new Wc[] { new Wc("RQST_HISTORY_KEY", ">=", master_Index_Rqst_Log_Dte_Tme, WcType.BY) },
                    new Oc[] { new Oc("RQST_HISTORY_KEY", "ASC") }
                    );
                    R2:
                    while (condition(vw_hist_View.readNextRow("R2")))
                    {
                        if (condition(hist_View_Rqst_Log_Dte_Tme.greater(master_Index_Rqst_Log_Dte_Tme)))                                                                 //Natural: IF HIST-VIEW.RQST-LOG-DTE-TME GT MASTER-INDEX.RQST-LOG-DTE-TME
                        {
                            if (true) break R2;                                                                                                                           //Natural: ESCAPE BOTTOM ( R2. )
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition((((hist_View_Last_Chnge_Unit_Cde.equals("TAXRC") || hist_View_Last_Chnge_Unit_Cde.equals("TAXCP")) && (hist_View_Admin_Unit_Cde.equals("TAXRC")  //Natural: IF ( HIST-VIEW.LAST-CHNGE-UNIT-CDE = 'TAXRC' OR = 'TAXCP' ) AND ( HIST-VIEW.ADMIN-UNIT-CDE = 'TAXRC' OR = 'TAXCP' ) AND ( HIST-VIEW.STATUS-CDE GT '0999' OR HIST-VIEW.ADMIN-STATUS-CDE GT '0999' )
                            || hist_View_Admin_Unit_Cde.equals("TAXCP"))) && (hist_View_Status_Cde.greater("0999") || hist_View_Admin_Status_Cde.greater("0999")))))
                        {
                            pnd_Hist_Ctr.reset();                                                                                                                         //Natural: RESET #HIST-CTR
                            if (true) break R2;                                                                                                                           //Natural: ESCAPE BOTTOM ( R2. )
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Hist_Ctr.nadd(1);                                                                                                                         //Natural: ADD 1 TO #HIST-CTR
                        }                                                                                                                                                 //Natural: END-IF
                        //*  R2.
                    }                                                                                                                                                     //Natural: END-READ
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  WORK NEVER RECEIVED IN TAXRC OR TAXCP
                    if (condition(pnd_Hist_Ctr.greater(getZero())))                                                                                                       //Natural: IF #HIST-CTR GT 0
                    {
                        pnd_Hist_Ctr.reset();                                                                                                                             //Natural: RESET #HIST-CTR
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        vw_master_Index.reset();                                                                                                                          //Natural: RESET MASTER-INDEX HIST-VIEW
                        vw_hist_View.reset();
                        //*  R1
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    //*  ---------------------------------------------------------
                    //*  R1.
                    getSort().writeSortInData(master_Index_Work_Prcss_Id, master_Index_Status_Updte_Dte_Tme, master_Index_Pin_Nbr, master_Index_Mj_Pull_Ind,              //Natural: END-ALL
                        master_Index_Physcl_Fldr_Id_Nbr, master_Index_Tiaa_Rcvd_Dte, master_Index_Effctve_Dte, master_Index_Trans_Dte, master_Index_Admin_Unit_Cde, 
                        master_Index_Admin_Status_Cde, master_Index_Last_Chnge_Unit_Cde, master_Index_Admin_Status_Updte_Dte_Tme);
                }                                                                                                                                                         //Natural: END-FIND
                if (Global.isEscape()) return;
                //*  ---------------------------------------------------------
                getSort().sortData(master_Index_Work_Prcss_Id, master_Index_Status_Updte_Dte_Tme);                                                                        //Natural: SORT MASTER-INDEX.WORK-PRCSS-ID ASCENDING MASTER-INDEX.STATUS-UPDTE-DTE-TME ASCENDING USING MASTER-INDEX.PIN-NBR MASTER-INDEX.MJ-PULL-IND MASTER-INDEX.PHYSCL-FLDR-ID-NBR MASTER-INDEX.TIAA-RCVD-DTE MASTER-INDEX.EFFCTVE-DTE MASTER-INDEX.TRANS-DTE MASTER-INDEX.ADMIN-UNIT-CDE MASTER-INDEX.ADMIN-STATUS-CDE MASTER-INDEX.LAST-CHNGE-UNIT-CDE MASTER-INDEX.ADMIN-STATUS-UPDTE-DTE-TME
                SORT01:
                while (condition(getSort().readSortOutData(master_Index_Work_Prcss_Id, master_Index_Status_Updte_Dte_Tme, master_Index_Pin_Nbr, master_Index_Mj_Pull_Ind, 
                    master_Index_Physcl_Fldr_Id_Nbr, master_Index_Tiaa_Rcvd_Dte, master_Index_Effctve_Dte, master_Index_Trans_Dte, master_Index_Admin_Unit_Cde, 
                    master_Index_Admin_Status_Cde, master_Index_Last_Chnge_Unit_Cde, master_Index_Admin_Status_Updte_Dte_Tme)))
                {
                    //*  ---------------------------------------------------------
                    if (condition(master_Index_Mj_Pull_Ind.equals("Y") || master_Index_Mj_Pull_Ind.equals("R")))                                                          //Natural: IF MASTER-INDEX.MJ-PULL-IND = 'Y' OR = 'R'
                    {
                        pnd_Physcl_Fldr_Id_Nbr_Pnd_Mj_Prefix.setValue("M");                                                                                               //Natural: MOVE 'M' TO #MJ-PREFIX
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Physcl_Fldr_Id_Nbr_Pnd_Mj_Prefix.setValue("F");                                                                                               //Natural: MOVE 'F' TO #MJ-PREFIX
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Physcl_Fldr_Id_Nbr_Pnd_Mj_Id.setValue(master_Index_Physcl_Fldr_Id_Nbr);                                                                           //Natural: MOVE MASTER-INDEX.PHYSCL-FLDR-ID-NBR TO #MJ-ID
                    if (condition(DbsUtil.maskMatches(pnd_Physcl_Fldr_Id_Nbr,".'000000'")))                                                                               //Natural: IF #PHYSCL-FLDR-ID-NBR = MASK ( .'000000' )
                    {
                        pnd_Physcl_Fldr_Id_Nbr.setValue("NONE");                                                                                                          //Natural: MOVE 'NONE' TO #PHYSCL-FLDR-ID-NBR
                    }                                                                                                                                                     //Natural: END-IF
                    //*  ---------------------------------------------------------
                    pnd_Parm_Status_Key_Pnd_Parm_Status_Code.setValue(master_Index_Admin_Status_Cde);                                                                     //Natural: MOVE MASTER-INDEX.ADMIN-STATUS-CDE TO #PARM-STATUS-CODE
                    pnd_Parm_Status_Key_Pnd_Parm_Status_Unit.setValue(master_Index_Last_Chnge_Unit_Cde);                                                                  //Natural: MOVE MASTER-INDEX.LAST-CHNGE-UNIT-CDE TO #PARM-STATUS-UNIT
                    DbsUtil.callnat(Cwfn1118.class , getCurrentProcessState(), pnd_Parm_Status_Key, pnd_Parm_Status_Name, pnd_Parm_Status_Wpid, pnd_Source);              //Natural: CALLNAT 'CWFN1118' #PARM-STATUS-KEY #PARM-STATUS-NAME #PARM-STATUS-WPID #SOURCE
                    if (condition(Global.isEscape())) return;
                    pnd_Parm_Status_Cde_Name.setValue(DbsUtil.compress(master_Index_Admin_Status_Cde, "-", pnd_Parm_Status_Name));                                        //Natural: COMPRESS MASTER-INDEX.ADMIN-STATUS-CDE '-' #PARM-STATUS-NAME INTO #PARM-STATUS-CDE-NAME
                    //*  ---------------------------------------------------------
                    DbsUtil.callnat(Cwfn0013.class , getCurrentProcessState(), master_Index_Work_Prcss_Id, pnd_Old_Rt_Cde, pnd_Wpid_Lname, pnd_Wpid_Sname);               //Natural: CALLNAT 'CWFN0013' MASTER-INDEX.WORK-PRCSS-ID #OLD-RT-CDE #WPID-LNAME #WPID-SNAME
                    if (condition(Global.isEscape())) return;
                    pnd_Wpid_Cde_Name.setValue(DbsUtil.compress(master_Index_Work_Prcss_Id, "-", pnd_Wpid_Sname));                                                        //Natural: COMPRESS MASTER-INDEX.WORK-PRCSS-ID '-' #WPID-SNAME INTO #WPID-CDE-NAME
                    //*  ---------------------------------------------------------
                    getReports().display(1, "/Work Process ID",                                                                                                           //Natural: DISPLAY ( 1 ) '/Work Process ID' #WPID-CDE-NAME '/Participant/Closed Date' MASTER-INDEX.STATUS-UPDTE-DTE-TME ( EM = MM'/'DD'/'YY ) '//PIN' MASTER-INDEX.PIN-NBR 'Physical/Folder/ID' #PHYSCL-FLDR-ID-NBR 'TIAA/Received/Date' MASTER-INDEX.TIAA-RCVD-DTE ( EM = MM'/'DD'/'YY ) '/Effective/Date' MASTER-INDEX.EFFCTVE-DTE ( EM = MM'/'DD'/'YY ) 'Check/Mailed/Date' MASTER-INDEX.TRANS-DTE ( EM = MM'/'DD'/'YY ) 'Current/Status/Date' MASTER-INDEX.ADMIN-STATUS-UPDTE-DTE-TME ( EM = MM'/'DD'/'YY ) '/Current/Status' #PARM-STATUS-CDE-NAME '/Current/Unit' MASTER-INDEX.ADMIN-UNIT-CDE
                    		pnd_Wpid_Cde_Name,"/Participant/Closed Date",
                    		master_Index_Status_Updte_Dte_Tme, new ReportEditMask ("MM'/'DD'/'YY"),"//PIN",
                    		master_Index_Pin_Nbr,"Physical/Folder/ID",
                    		pnd_Physcl_Fldr_Id_Nbr,"TIAA/Received/Date",
                    		master_Index_Tiaa_Rcvd_Dte, new ReportEditMask ("MM'/'DD'/'YY"),"/Effective/Date",
                    		master_Index_Effctve_Dte, new ReportEditMask ("MM'/'DD'/'YY"),"Check/Mailed/Date",
                    		master_Index_Trans_Dte, new ReportEditMask ("MM'/'DD'/'YY"),"Current/Status/Date",
                    		master_Index_Admin_Status_Updte_Dte_Tme, new ReportEditMask ("MM'/'DD'/'YY"),"/Current/Status",
                    		pnd_Parm_Status_Cde_Name,"/Current/Unit",
                    		master_Index_Admin_Unit_Cde);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  ---------------------------------------------------------
                }                                                                                                                                                         //Natural: END-SORT
                endSort();
                getReports().eject(1, true);                                                                                                                              //Natural: EJECT ( 1 )
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Page_Number.nadd(1);                                                                                                                              //Natural: ADD 1 TO #PAGE-NUMBER
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(56),"Corporate Workflow Facilities",new TabSetting(128),"Page",pnd_Page_Number,  //Natural: WRITE ( 1 ) NOTITLE *PROGRAM 56T 'Corporate Workflow Facilities' 128T 'Page' #PAGE-NUMBER ( NL = 3 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 48T 'Weekly Tax Report of Modified Processing Cases' 128T *TIMX ( EM = HH':'II' 'AP ) / '-' ( 139 ) / ' ' / 'UNIT :' #UNIT-CDE '-' #UNIT-NAME / '-' ( 139 ) / ' ' / ' '
                        new NumericLength (3), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),new 
                        TabSetting(48),"Weekly Tax Report of Modified Processing Cases",new TabSetting(128),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"),NEWLINE,"-",new 
                        RepeatItem(139),NEWLINE," ",NEWLINE,"UNIT :",pnd_Unit_Cde,"-",pnd_Unit_Name,NEWLINE,"-",new RepeatItem(139),NEWLINE," ",NEWLINE,
                        " ");
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=140 PS=60");

        getReports().setDisplayColumns(1, "/Work Process ID",
        		pnd_Wpid_Cde_Name,"/Participant/Closed Date",
        		master_Index_Status_Updte_Dte_Tme, new ReportEditMask ("MM'/'DD'/'YY"),"//PIN",
        		master_Index_Pin_Nbr,"Physical/Folder/ID",
        		pnd_Physcl_Fldr_Id_Nbr,"TIAA/Received/Date",
        		master_Index_Tiaa_Rcvd_Dte, new ReportEditMask ("MM'/'DD'/'YY"),"/Effective/Date",
        		master_Index_Effctve_Dte, new ReportEditMask ("MM'/'DD'/'YY"),"Check/Mailed/Date",
        		master_Index_Trans_Dte, new ReportEditMask ("MM'/'DD'/'YY"),"Current/Status/Date",
        		master_Index_Admin_Status_Updte_Dte_Tme, new ReportEditMask ("MM'/'DD'/'YY"),"/Current/Status",
        		pnd_Parm_Status_Cde_Name,"/Current/Unit",
        		master_Index_Admin_Unit_Cde);
    }
}
