/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:29:20 PM
**        * FROM NATURAL PROGRAM : Cwfb3410
************************************************************
**        * FILE NAME            : Cwfb3410.java
**        * CLASS NAME           : Cwfb3410
**        * INSTANCE NAME        : Cwfb3410
************************************************************
**SAG GENERATOR: SHELL-TIAA                       VERSION: 3.2.2
**SAG TITLE: WEEKLY REPORT
**SAG SYSTEM: CRPCWF
************************************************************************
* PROGRAM  : CWFB3410
* SYSTEM   : CRPCWF
* TITLE    : REPORT 1
* GENERATED: OCT 19,93 AT 10:46 AM
* FUNCTION : WEEKLY TAX REPORT OF MODIFIED PROCESSING CASES
*          |
*          | REPORT DRIVER
*          |
*          |
*          |
*          |
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* MMM DD YY  ________ ______________________________________________
* ______________________________________________
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3410 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Rep_Parm;
    private DbsField pnd_Rep_Parm_Pnd_Opt;
    private DbsField pnd_Rep_Parm_Pnd_Opt_Var;
    private DbsField pnd_Rep_Parm_Pnd_Rep_Racf_Id;
    private DbsField pnd_Rep_Parm_Pnd_Rep_Start_Dd;

    private DbsGroup pnd_Rep_Parm__R_Field_1;
    private DbsField pnd_Rep_Parm_Pnd_Rep_Start_Dd_A;
    private DbsField pnd_Rep_Parm_Pnd_Rep_Start_Mm;

    private DbsGroup pnd_Rep_Parm__R_Field_2;
    private DbsField pnd_Rep_Parm_Pnd_Rep_Start_Mm_A;
    private DbsField pnd_Rep_Parm_Pnd_Rep_Start_Yy;

    private DbsGroup pnd_Rep_Parm__R_Field_3;
    private DbsField pnd_Rep_Parm_Pnd_Rep_Start_Yy_A;
    private DbsField pnd_Rep_Parm_Pnd_Rep_End_Dd;

    private DbsGroup pnd_Rep_Parm__R_Field_4;
    private DbsField pnd_Rep_Parm_Pnd_Rep_End_Dd_A;
    private DbsField pnd_Rep_Parm_Pnd_Rep_End_Mm;

    private DbsGroup pnd_Rep_Parm__R_Field_5;
    private DbsField pnd_Rep_Parm_Pnd_Rep_End_Mm_A;
    private DbsField pnd_Rep_Parm_Pnd_Rep_End_Yy;

    private DbsGroup pnd_Rep_Parm__R_Field_6;
    private DbsField pnd_Rep_Parm_Pnd_Rep_End_Yy_A;
    private DbsField pnd_Rep_Parm_Pnd_Rep_Unit_Cde;
    private DbsField pnd_Rep_Parm_Pnd_Rep_Wpid;

    private DbsGroup pnd_Rep_Parm__R_Field_7;
    private DbsField pnd_Rep_Parm_Pnd_Rep_Wpid_Actn_Rqstd_Cde;
    private DbsField pnd_Rep_Parm_Pnd_Rep_Wpid_Lob;
    private DbsField pnd_Rep_Parm_Pnd_Rep_Wpid_Mbp;
    private DbsField pnd_Rep_Parm_Pnd_Rep_Wpid_Sbp;
    private DbsField pnd_Rep_Parm_Pnd_Start_Date;

    private DbsGroup pnd_Rep_Parm__R_Field_8;
    private DbsField pnd_Rep_Parm_Pnd_Start_Date_N;
    private DbsField pnd_Rep_Parm_Pnd_End_Date;

    private DbsGroup pnd_Rep_Parm__R_Field_9;
    private DbsField pnd_Rep_Parm_Pnd_End_Date_N;
    private DbsField pnd_Rep_Parm_Pnd_Work_Start_Date_A;

    private DbsGroup pnd_Rep_Parm__R_Field_10;
    private DbsField pnd_Rep_Parm_Pnd_Work_Mm;
    private DbsField pnd_Rep_Parm_Pnd_Filler1;
    private DbsField pnd_Rep_Parm_Pnd_Work_Dd;
    private DbsField pnd_Rep_Parm_Pnd_Filler2;
    private DbsField pnd_Rep_Parm_Pnd_Work_Yy;
    private DbsField pnd_Rep_Parm_Pnd_Work_End_Date_A;

    private DbsGroup pnd_Rep_Parm__R_Field_11;
    private DbsField pnd_Rep_Parm_Pnd_Work_Mmm;
    private DbsField pnd_Rep_Parm_Pnd_Fillera;
    private DbsField pnd_Rep_Parm_Pnd_Work_Ddd;
    private DbsField pnd_Rep_Parm_Pnd_Fillerb;
    private DbsField pnd_Rep_Parm_Pnd_Work_Yyy;
    private DbsField pnd_Env;
    private DbsField pnd_Page;
    private DbsField pnd_Sort_Unit;
    private DbsField pnd_Work_Date;
    private DbsField pnd_To;
    private DbsField pnd_From;
    private DbsField pnd_Day;
    private DbsField pnd_Sub;
    private DbsField pnd_New_Racf;
    private DbsField pnd_New_Unit;
    private DbsField pnd_Yyyymmdd;

    private DbsGroup pnd_Yyyymmdd__R_Field_12;
    private DbsField pnd_Yyyymmdd_Pnd_Century;
    private DbsField pnd_Yyyymmdd_Pnd_Yy;
    private DbsField pnd_Yyyymmdd_Pnd_Mm;
    private DbsField pnd_Yyyymmdd_Pnd_Dd;
    private DbsField pnd_Report_No;
    private DbsField pnd_Racf_Id;
    private DbsField pnd_Floor;
    private DbsField pnd_Bldg;
    private DbsField pnd_Drop_Off;
    private DbsField pnd_Unit_Name;
    private DbsField pnd_Comp_Date;
    private DbsField pnd_Reccount;
    private DbsField pnd_Work_Start_Date;
    private DbsField pnd_Work_Comp_Date;
    private DbsField pnd_Date_Diff;
    private DbsField pnd_Oprtr_Cde;
    private DbsField pnd_Day_Of_Week;
    private DbsField pnd_Days_To_Subtract;
    private DbsField pnd_Proc_Unit;
    private DbsField pnd_Pend_Dte_Tme;

    private DbsGroup pnd_Pend_Dte_Tme__R_Field_13;
    private DbsField pnd_Pend_Dte_Tme_Pnd_Pend_Dte;
    private DbsField pnd_Run_Date;
    private DbsField pnd_Work_Run_Date;
    private DbsField pnd_Work_Run_Date_A;

    private DbsGroup pnd_Work_Run_Date_A__R_Field_14;
    private DbsField pnd_Work_Run_Date_A_Pnd_Work_Run_Yyyy;
    private DbsField pnd_Work_Run_Date_A_Pnd_Work_Run_Mm;
    private DbsField pnd_Work_Run_Date_A_Pnd_Work_Run_Dd;
    private DbsField pnd_Data_Stacked;
    private DbsField pnd_Record;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Rep_Parm = localVariables.newGroupInRecord("pnd_Rep_Parm", "#REP-PARM");
        pnd_Rep_Parm_Pnd_Opt = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Opt", "#OPT", FieldType.STRING, 1);
        pnd_Rep_Parm_Pnd_Opt_Var = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Opt_Var", "#OPT-VAR", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Rep_Parm_Pnd_Rep_Racf_Id = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_Racf_Id", "#REP-RACF-ID", FieldType.STRING, 8);
        pnd_Rep_Parm_Pnd_Rep_Start_Dd = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_Start_Dd", "#REP-START-DD", FieldType.NUMERIC, 2);

        pnd_Rep_Parm__R_Field_1 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_1", "REDEFINE", pnd_Rep_Parm_Pnd_Rep_Start_Dd);
        pnd_Rep_Parm_Pnd_Rep_Start_Dd_A = pnd_Rep_Parm__R_Field_1.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_Start_Dd_A", "#REP-START-DD-A", FieldType.STRING, 
            2);
        pnd_Rep_Parm_Pnd_Rep_Start_Mm = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_Start_Mm", "#REP-START-MM", FieldType.NUMERIC, 2);

        pnd_Rep_Parm__R_Field_2 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_2", "REDEFINE", pnd_Rep_Parm_Pnd_Rep_Start_Mm);
        pnd_Rep_Parm_Pnd_Rep_Start_Mm_A = pnd_Rep_Parm__R_Field_2.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_Start_Mm_A", "#REP-START-MM-A", FieldType.STRING, 
            2);
        pnd_Rep_Parm_Pnd_Rep_Start_Yy = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_Start_Yy", "#REP-START-YY", FieldType.NUMERIC, 2);

        pnd_Rep_Parm__R_Field_3 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_3", "REDEFINE", pnd_Rep_Parm_Pnd_Rep_Start_Yy);
        pnd_Rep_Parm_Pnd_Rep_Start_Yy_A = pnd_Rep_Parm__R_Field_3.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_Start_Yy_A", "#REP-START-YY-A", FieldType.STRING, 
            2);
        pnd_Rep_Parm_Pnd_Rep_End_Dd = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_End_Dd", "#REP-END-DD", FieldType.NUMERIC, 2);

        pnd_Rep_Parm__R_Field_4 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_4", "REDEFINE", pnd_Rep_Parm_Pnd_Rep_End_Dd);
        pnd_Rep_Parm_Pnd_Rep_End_Dd_A = pnd_Rep_Parm__R_Field_4.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_End_Dd_A", "#REP-END-DD-A", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Rep_End_Mm = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_End_Mm", "#REP-END-MM", FieldType.NUMERIC, 2);

        pnd_Rep_Parm__R_Field_5 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_5", "REDEFINE", pnd_Rep_Parm_Pnd_Rep_End_Mm);
        pnd_Rep_Parm_Pnd_Rep_End_Mm_A = pnd_Rep_Parm__R_Field_5.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_End_Mm_A", "#REP-END-MM-A", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Rep_End_Yy = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_End_Yy", "#REP-END-YY", FieldType.NUMERIC, 2);

        pnd_Rep_Parm__R_Field_6 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_6", "REDEFINE", pnd_Rep_Parm_Pnd_Rep_End_Yy);
        pnd_Rep_Parm_Pnd_Rep_End_Yy_A = pnd_Rep_Parm__R_Field_6.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_End_Yy_A", "#REP-END-YY-A", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Rep_Unit_Cde = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_Unit_Cde", "#REP-UNIT-CDE", FieldType.STRING, 8);
        pnd_Rep_Parm_Pnd_Rep_Wpid = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_Wpid", "#REP-WPID", FieldType.STRING, 6);

        pnd_Rep_Parm__R_Field_7 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_7", "REDEFINE", pnd_Rep_Parm_Pnd_Rep_Wpid);
        pnd_Rep_Parm_Pnd_Rep_Wpid_Actn_Rqstd_Cde = pnd_Rep_Parm__R_Field_7.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_Wpid_Actn_Rqstd_Cde", "#REP-WPID-ACTN-RQSTD-CDE", 
            FieldType.STRING, 1);
        pnd_Rep_Parm_Pnd_Rep_Wpid_Lob = pnd_Rep_Parm__R_Field_7.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_Wpid_Lob", "#REP-WPID-LOB", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Rep_Wpid_Mbp = pnd_Rep_Parm__R_Field_7.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_Wpid_Mbp", "#REP-WPID-MBP", FieldType.STRING, 1);
        pnd_Rep_Parm_Pnd_Rep_Wpid_Sbp = pnd_Rep_Parm__R_Field_7.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_Wpid_Sbp", "#REP-WPID-SBP", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Start_Date = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Start_Date", "#START-DATE", FieldType.STRING, 8);

        pnd_Rep_Parm__R_Field_8 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_8", "REDEFINE", pnd_Rep_Parm_Pnd_Start_Date);
        pnd_Rep_Parm_Pnd_Start_Date_N = pnd_Rep_Parm__R_Field_8.newFieldInGroup("pnd_Rep_Parm_Pnd_Start_Date_N", "#START-DATE-N", FieldType.NUMERIC, 8);
        pnd_Rep_Parm_Pnd_End_Date = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_End_Date", "#END-DATE", FieldType.STRING, 8);

        pnd_Rep_Parm__R_Field_9 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_9", "REDEFINE", pnd_Rep_Parm_Pnd_End_Date);
        pnd_Rep_Parm_Pnd_End_Date_N = pnd_Rep_Parm__R_Field_9.newFieldInGroup("pnd_Rep_Parm_Pnd_End_Date_N", "#END-DATE-N", FieldType.NUMERIC, 8);
        pnd_Rep_Parm_Pnd_Work_Start_Date_A = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Start_Date_A", "#WORK-START-DATE-A", FieldType.STRING, 
            8);

        pnd_Rep_Parm__R_Field_10 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_10", "REDEFINE", pnd_Rep_Parm_Pnd_Work_Start_Date_A);
        pnd_Rep_Parm_Pnd_Work_Mm = pnd_Rep_Parm__R_Field_10.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Mm", "#WORK-MM", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Filler1 = pnd_Rep_Parm__R_Field_10.newFieldInGroup("pnd_Rep_Parm_Pnd_Filler1", "#FILLER1", FieldType.STRING, 1);
        pnd_Rep_Parm_Pnd_Work_Dd = pnd_Rep_Parm__R_Field_10.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Dd", "#WORK-DD", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Filler2 = pnd_Rep_Parm__R_Field_10.newFieldInGroup("pnd_Rep_Parm_Pnd_Filler2", "#FILLER2", FieldType.STRING, 1);
        pnd_Rep_Parm_Pnd_Work_Yy = pnd_Rep_Parm__R_Field_10.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Yy", "#WORK-YY", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Work_End_Date_A = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_End_Date_A", "#WORK-END-DATE-A", FieldType.STRING, 8);

        pnd_Rep_Parm__R_Field_11 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_11", "REDEFINE", pnd_Rep_Parm_Pnd_Work_End_Date_A);
        pnd_Rep_Parm_Pnd_Work_Mmm = pnd_Rep_Parm__R_Field_11.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Mmm", "#WORK-MMM", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Fillera = pnd_Rep_Parm__R_Field_11.newFieldInGroup("pnd_Rep_Parm_Pnd_Fillera", "#FILLERA", FieldType.STRING, 1);
        pnd_Rep_Parm_Pnd_Work_Ddd = pnd_Rep_Parm__R_Field_11.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Ddd", "#WORK-DDD", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Fillerb = pnd_Rep_Parm__R_Field_11.newFieldInGroup("pnd_Rep_Parm_Pnd_Fillerb", "#FILLERB", FieldType.STRING, 1);
        pnd_Rep_Parm_Pnd_Work_Yyy = pnd_Rep_Parm__R_Field_11.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Yyy", "#WORK-YYY", FieldType.STRING, 2);
        pnd_Env = localVariables.newFieldInRecord("pnd_Env", "#ENV", FieldType.STRING, 10);
        pnd_Page = localVariables.newFieldInRecord("pnd_Page", "#PAGE", FieldType.NUMERIC, 5);
        pnd_Sort_Unit = localVariables.newFieldInRecord("pnd_Sort_Unit", "#SORT-UNIT", FieldType.STRING, 1);
        pnd_Work_Date = localVariables.newFieldInRecord("pnd_Work_Date", "#WORK-DATE", FieldType.DATE);
        pnd_To = localVariables.newFieldInRecord("pnd_To", "#TO", FieldType.DATE);
        pnd_From = localVariables.newFieldInRecord("pnd_From", "#FROM", FieldType.DATE);
        pnd_Day = localVariables.newFieldInRecord("pnd_Day", "#DAY", FieldType.STRING, 3);
        pnd_Sub = localVariables.newFieldInRecord("pnd_Sub", "#SUB", FieldType.NUMERIC, 1);
        pnd_New_Racf = localVariables.newFieldInRecord("pnd_New_Racf", "#NEW-RACF", FieldType.BOOLEAN, 1);
        pnd_New_Unit = localVariables.newFieldInRecord("pnd_New_Unit", "#NEW-UNIT", FieldType.BOOLEAN, 1);
        pnd_Yyyymmdd = localVariables.newFieldInRecord("pnd_Yyyymmdd", "#YYYYMMDD", FieldType.STRING, 8);

        pnd_Yyyymmdd__R_Field_12 = localVariables.newGroupInRecord("pnd_Yyyymmdd__R_Field_12", "REDEFINE", pnd_Yyyymmdd);
        pnd_Yyyymmdd_Pnd_Century = pnd_Yyyymmdd__R_Field_12.newFieldInGroup("pnd_Yyyymmdd_Pnd_Century", "#CENTURY", FieldType.STRING, 2);
        pnd_Yyyymmdd_Pnd_Yy = pnd_Yyyymmdd__R_Field_12.newFieldInGroup("pnd_Yyyymmdd_Pnd_Yy", "#YY", FieldType.STRING, 2);
        pnd_Yyyymmdd_Pnd_Mm = pnd_Yyyymmdd__R_Field_12.newFieldInGroup("pnd_Yyyymmdd_Pnd_Mm", "#MM", FieldType.STRING, 2);
        pnd_Yyyymmdd_Pnd_Dd = pnd_Yyyymmdd__R_Field_12.newFieldInGroup("pnd_Yyyymmdd_Pnd_Dd", "#DD", FieldType.STRING, 2);
        pnd_Report_No = localVariables.newFieldInRecord("pnd_Report_No", "#REPORT-NO", FieldType.NUMERIC, 2);
        pnd_Racf_Id = localVariables.newFieldInRecord("pnd_Racf_Id", "#RACF-ID", FieldType.STRING, 8);
        pnd_Floor = localVariables.newFieldInRecord("pnd_Floor", "#FLOOR", FieldType.NUMERIC, 2);
        pnd_Bldg = localVariables.newFieldInRecord("pnd_Bldg", "#BLDG", FieldType.STRING, 3);
        pnd_Drop_Off = localVariables.newFieldInRecord("pnd_Drop_Off", "#DROP-OFF", FieldType.STRING, 2);
        pnd_Unit_Name = localVariables.newFieldInRecord("pnd_Unit_Name", "#UNIT-NAME", FieldType.STRING, 45);
        pnd_Comp_Date = localVariables.newFieldInRecord("pnd_Comp_Date", "#COMP-DATE", FieldType.STRING, 8);
        pnd_Reccount = localVariables.newFieldInRecord("pnd_Reccount", "#RECCOUNT", FieldType.NUMERIC, 5);
        pnd_Work_Start_Date = localVariables.newFieldInRecord("pnd_Work_Start_Date", "#WORK-START-DATE", FieldType.DATE);
        pnd_Work_Comp_Date = localVariables.newFieldInRecord("pnd_Work_Comp_Date", "#WORK-COMP-DATE", FieldType.DATE);
        pnd_Date_Diff = localVariables.newFieldInRecord("pnd_Date_Diff", "#DATE-DIFF", FieldType.NUMERIC, 3);
        pnd_Oprtr_Cde = localVariables.newFieldInRecord("pnd_Oprtr_Cde", "#OPRTR-CDE", FieldType.STRING, 8);
        pnd_Day_Of_Week = localVariables.newFieldInRecord("pnd_Day_Of_Week", "#DAY-OF-WEEK", FieldType.STRING, 3);
        pnd_Days_To_Subtract = localVariables.newFieldInRecord("pnd_Days_To_Subtract", "#DAYS-TO-SUBTRACT", FieldType.NUMERIC, 2);
        pnd_Proc_Unit = localVariables.newFieldInRecord("pnd_Proc_Unit", "#PROC-UNIT", FieldType.STRING, 8);
        pnd_Pend_Dte_Tme = localVariables.newFieldInRecord("pnd_Pend_Dte_Tme", "#PEND-DTE-TME", FieldType.STRING, 15);

        pnd_Pend_Dte_Tme__R_Field_13 = localVariables.newGroupInRecord("pnd_Pend_Dte_Tme__R_Field_13", "REDEFINE", pnd_Pend_Dte_Tme);
        pnd_Pend_Dte_Tme_Pnd_Pend_Dte = pnd_Pend_Dte_Tme__R_Field_13.newFieldInGroup("pnd_Pend_Dte_Tme_Pnd_Pend_Dte", "#PEND-DTE", FieldType.STRING, 8);
        pnd_Run_Date = localVariables.newFieldInRecord("pnd_Run_Date", "#RUN-DATE", FieldType.STRING, 8);
        pnd_Work_Run_Date = localVariables.newFieldInRecord("pnd_Work_Run_Date", "#WORK-RUN-DATE", FieldType.DATE);
        pnd_Work_Run_Date_A = localVariables.newFieldInRecord("pnd_Work_Run_Date_A", "#WORK-RUN-DATE-A", FieldType.STRING, 8);

        pnd_Work_Run_Date_A__R_Field_14 = localVariables.newGroupInRecord("pnd_Work_Run_Date_A__R_Field_14", "REDEFINE", pnd_Work_Run_Date_A);
        pnd_Work_Run_Date_A_Pnd_Work_Run_Yyyy = pnd_Work_Run_Date_A__R_Field_14.newFieldInGroup("pnd_Work_Run_Date_A_Pnd_Work_Run_Yyyy", "#WORK-RUN-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Work_Run_Date_A_Pnd_Work_Run_Mm = pnd_Work_Run_Date_A__R_Field_14.newFieldInGroup("pnd_Work_Run_Date_A_Pnd_Work_Run_Mm", "#WORK-RUN-MM", FieldType.NUMERIC, 
            2);
        pnd_Work_Run_Date_A_Pnd_Work_Run_Dd = pnd_Work_Run_Date_A__R_Field_14.newFieldInGroup("pnd_Work_Run_Date_A_Pnd_Work_Run_Dd", "#WORK-RUN-DD", FieldType.STRING, 
            2);
        pnd_Data_Stacked = localVariables.newFieldInRecord("pnd_Data_Stacked", "#DATA-STACKED", FieldType.BOOLEAN, 1);
        pnd_Record = localVariables.newFieldInRecord("pnd_Record", "#RECORD", FieldType.STRING, 54);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Report_No.setInitialValue(14);
        pnd_Proc_Unit.setInitialValue("TAXRC");
        pnd_Record.setInitialValue("�E�&l1o2a5.6c72p3e66F�&a2L�(9905X�&l0L");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb3410() throws Exception
    {
        super("Cwfb3410");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Cwfb3410|Main");
        setupReports();
        while(true)
        {
            try
            {
                pnd_Env.setValue(Global.getLIBRARY_ID());                                                                                                                 //Natural: MOVE *LIBRARY-ID TO #ENV
                if (condition(pnd_Env.equals("PROJCWF")))                                                                                                                 //Natural: IF #ENV = 'PROJCWF'
                {
                    pnd_Env.setValue("TEST ");                                                                                                                            //Natural: MOVE 'TEST ' TO #ENV
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Env.equals("PRACANN")))                                                                                                                 //Natural: IF #ENV = 'PRACANN'
                {
                    pnd_Env.setValue("PRACTICE");                                                                                                                         //Natural: MOVE 'PRACTICE' TO #ENV
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Env.equals("PRODANN")))                                                                                                                 //Natural: IF #ENV = 'PRODANN'
                {
                    pnd_Env.setValue(" ");                                                                                                                                //Natural: MOVE ' ' TO #ENV
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Env.equals("ACPT034")))                                                                                                                 //Natural: IF #ENV = 'ACPT034'
                {
                    pnd_Env.setValue("RGN:AT07");                                                                                                                         //Natural: MOVE 'RGN:AT07' TO #ENV
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Env.equals("ACPT023")))                                                                                                                 //Natural: IF #ENV = 'ACPT023'
                {
                    pnd_Env.setValue("RGN:AT06");                                                                                                                         //Natural: MOVE 'RGN:AT06' TO #ENV
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Env.equals("ACPT028")))                                                                                                                 //Natural: IF #ENV = 'ACPT028'
                {
                    pnd_Env.setValue("RGN:AT05");                                                                                                                         //Natural: MOVE 'RGN:AT05' TO #ENV
                }                                                                                                                                                         //Natural: END-IF
                if (condition(Global.getLIBRARY_ID().notEquals("PRODANN")))                                                                                               //Natural: IF *LIBRARY-ID NE 'PRODANN'
                {
                    pnd_Env.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "(", pnd_Env, ")"));                                                                 //Natural: COMPRESS '(' #ENV ')' INTO #ENV LEAVING NO SPACE
                }                                                                                                                                                         //Natural: END-IF
                //* ********************
                //*                    *
                //*  REPORT SECTION    *
                //* ********************
                //*                                                                                                                                                       //Natural: FORMAT ( 1 ) LS = 132 PS = 58
                //*  DATA STACKED
                if (condition(Global.getSTACK().getDatacount().greater(getZero()), INPUT_1))                                                                              //Natural: IF *DATA GT 0
                {
                    DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Run_Date);                                                                                     //Natural: INPUT #RUN-DATE
                    pnd_Data_Stacked.setValue(true);                                                                                                                      //Natural: MOVE TRUE TO #DATA-STACKED
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Run_Date.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                         //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO #RUN-DATE
                    pnd_Data_Stacked.setValue(false);                                                                                                                     //Natural: MOVE FALSE TO #DATA-STACKED
                }                                                                                                                                                         //Natural: END-IF
                pnd_Work_Comp_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Run_Date);                                                                           //Natural: MOVE EDITED #RUN-DATE TO #WORK-COMP-DATE ( EM = YYYYMMDD )
                pnd_Rep_Parm_Pnd_Filler1.setValue("/");                                                                                                                   //Natural: MOVE '/' TO #FILLER1 #FILLER2
                pnd_Rep_Parm_Pnd_Filler2.setValue("/");
                pnd_Rep_Parm_Pnd_Fillera.setValue("/");                                                                                                                   //Natural: MOVE '/' TO #FILLERA #FILLERB
                pnd_Rep_Parm_Pnd_Fillerb.setValue("/");
                pnd_Comp_Date.setValue(pnd_Run_Date);                                                                                                                     //Natural: MOVE #RUN-DATE TO #COMP-DATE
                pnd_Day_Of_Week.setValueEdited(pnd_Work_Comp_Date,new ReportEditMask("NNN"));                                                                             //Natural: MOVE EDITED #WORK-COMP-DATE ( EM = NNN ) TO #DAY-OF-WEEK
                if (condition(pnd_Data_Stacked.getBoolean()))                                                                                                             //Natural: IF #DATA-STACKED
                {
                    //*  GET END DATE FOR THE MONTH RUN FOR 1 MONTH ONLY
                    pnd_Rep_Parm_Pnd_Start_Date.setValueEdited(pnd_Work_Comp_Date,new ReportEditMask("YYYYMMDD"));                                                        //Natural: MOVE EDITED #WORK-COMP-DATE ( EM = YYYYMMDD ) TO #START-DATE
                    pnd_Work_Run_Date_A.setValueEdited(pnd_Work_Comp_Date,new ReportEditMask("YYYYMMDD"));                                                                //Natural: MOVE EDITED #WORK-COMP-DATE ( EM = YYYYMMDD ) TO #WORK-RUN-DATE-A
                    if (condition(pnd_Work_Run_Date_A_Pnd_Work_Run_Mm.greaterOrEqual(1) && pnd_Work_Run_Date_A_Pnd_Work_Run_Mm.lessOrEqual(11)))                          //Natural: IF #WORK-RUN-MM = 01 THRU 11
                    {
                        pnd_Work_Run_Date_A_Pnd_Work_Run_Mm.nadd(1);                                                                                                      //Natural: ADD 1 TO #WORK-RUN-MM
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Work_Run_Date_A_Pnd_Work_Run_Mm.setValue(1);                                                                                                  //Natural: MOVE 01 TO #WORK-RUN-MM
                        pnd_Work_Run_Date_A_Pnd_Work_Run_Yyyy.nadd(1);                                                                                                    //Natural: ADD 1 TO #WORK-RUN-YYYY
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Work_Run_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Work_Run_Date_A);                                                                 //Natural: MOVE EDITED #WORK-RUN-DATE-A TO #WORK-RUN-DATE ( EM = YYYYMMDD )
                    pnd_Work_Run_Date.nsubtract(1);                                                                                                                       //Natural: SUBTRACT 1 FROM #WORK-RUN-DATE
                    pnd_Rep_Parm_Pnd_End_Date.setValueEdited(pnd_Work_Run_Date,new ReportEditMask("YYYYMMDD"));                                                           //Natural: MOVE EDITED #WORK-RUN-DATE ( EM = YYYYMMDD ) TO #END-DATE
                    pnd_Comp_Date.setValue(pnd_Rep_Parm_Pnd_End_Date);                                                                                                    //Natural: MOVE #END-DATE TO #COMP-DATE
                    pnd_Yyyymmdd.setValue(pnd_Rep_Parm_Pnd_Start_Date);                                                                                                   //Natural: MOVE #START-DATE TO #YYYYMMDD
                    pnd_Rep_Parm_Pnd_Work_Mm.setValue(pnd_Yyyymmdd_Pnd_Mm);                                                                                               //Natural: MOVE #MM TO #WORK-MM
                    pnd_Rep_Parm_Pnd_Work_Dd.setValue(pnd_Yyyymmdd_Pnd_Dd);                                                                                               //Natural: MOVE #DD TO #WORK-DD
                    pnd_Rep_Parm_Pnd_Work_Yy.setValue(pnd_Yyyymmdd_Pnd_Yy);                                                                                               //Natural: MOVE #YY TO #WORK-YY
                    pnd_Yyyymmdd.setValue(pnd_Rep_Parm_Pnd_End_Date);                                                                                                     //Natural: MOVE #END-DATE TO #YYYYMMDD
                    pnd_Rep_Parm_Pnd_Work_Mmm.setValue(pnd_Yyyymmdd_Pnd_Mm);                                                                                              //Natural: MOVE #MM TO #WORK-MMM
                    pnd_Rep_Parm_Pnd_Work_Ddd.setValue(pnd_Yyyymmdd_Pnd_Dd);                                                                                              //Natural: MOVE #DD TO #WORK-DDD
                    pnd_Rep_Parm_Pnd_Work_Yyy.setValue(pnd_Yyyymmdd_Pnd_Yy);                                                                                              //Natural: MOVE #YY TO #WORK-YYY
                    //*  START DATE AND END DATE ESTABLISHED
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  REPORT GENERATED FROM LAST SUNDAY TO LATEST SATURDAY (7 DAYS)
                    short decideConditionsMet196 = 0;                                                                                                                     //Natural: DECIDE ON FIRST VALUE OF #DAY-OF-WEEK;//Natural: VALUE 'Sat'
                    if (condition((pnd_Day_Of_Week.equals("Sat"))))
                    {
                        decideConditionsMet196++;
                        pnd_Days_To_Subtract.setValue(6);                                                                                                                 //Natural: MOVE 6 TO #DAYS-TO-SUBTRACT
                    }                                                                                                                                                     //Natural: VALUE 'Sun'
                    else if (condition((pnd_Day_Of_Week.equals("Sun"))))
                    {
                        decideConditionsMet196++;
                        pnd_Days_To_Subtract.setValue(7);                                                                                                                 //Natural: MOVE 7 TO #DAYS-TO-SUBTRACT
                    }                                                                                                                                                     //Natural: VALUE 'Mon'
                    else if (condition((pnd_Day_Of_Week.equals("Mon"))))
                    {
                        decideConditionsMet196++;
                        pnd_Days_To_Subtract.setValue(8);                                                                                                                 //Natural: MOVE 8 TO #DAYS-TO-SUBTRACT
                    }                                                                                                                                                     //Natural: VALUE 'Tue'
                    else if (condition((pnd_Day_Of_Week.equals("Tue"))))
                    {
                        decideConditionsMet196++;
                        pnd_Days_To_Subtract.setValue(9);                                                                                                                 //Natural: MOVE 9 TO #DAYS-TO-SUBTRACT
                    }                                                                                                                                                     //Natural: VALUE 'Wed'
                    else if (condition((pnd_Day_Of_Week.equals("Wed"))))
                    {
                        decideConditionsMet196++;
                        pnd_Days_To_Subtract.setValue(10);                                                                                                                //Natural: MOVE 10 TO #DAYS-TO-SUBTRACT
                    }                                                                                                                                                     //Natural: VALUE 'Thu'
                    else if (condition((pnd_Day_Of_Week.equals("Thu"))))
                    {
                        decideConditionsMet196++;
                        pnd_Days_To_Subtract.setValue(11);                                                                                                                //Natural: MOVE 11 TO #DAYS-TO-SUBTRACT
                    }                                                                                                                                                     //Natural: VALUE 'Fri'
                    else if (condition((pnd_Day_Of_Week.equals("Fri"))))
                    {
                        decideConditionsMet196++;
                        pnd_Days_To_Subtract.setValue(12);                                                                                                                //Natural: MOVE 12 TO #DAYS-TO-SUBTRACT
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                    pnd_Work_Comp_Date.nsubtract(pnd_Days_To_Subtract);                                                                                                   //Natural: SUBTRACT #DAYS-TO-SUBTRACT FROM #WORK-COMP-DATE
                    pnd_Rep_Parm_Pnd_Start_Date.setValueEdited(pnd_Work_Comp_Date,new ReportEditMask("YYYYMMDD"));                                                        //Natural: MOVE EDITED #WORK-COMP-DATE ( EM = YYYYMMDD ) TO #START-DATE
                    pnd_Work_Comp_Date.nadd(6);                                                                                                                           //Natural: ADD 6 TO #WORK-COMP-DATE
                    pnd_Rep_Parm_Pnd_End_Date.setValueEdited(pnd_Work_Comp_Date,new ReportEditMask("YYYYMMDD"));                                                          //Natural: MOVE EDITED #WORK-COMP-DATE ( EM = YYYYMMDD ) TO #END-DATE
                    pnd_Comp_Date.setValue(pnd_Rep_Parm_Pnd_End_Date);                                                                                                    //Natural: MOVE #END-DATE TO #COMP-DATE
                    pnd_Yyyymmdd.setValue(pnd_Rep_Parm_Pnd_Start_Date);                                                                                                   //Natural: MOVE #START-DATE TO #YYYYMMDD
                    pnd_Rep_Parm_Pnd_Work_Mm.setValue(pnd_Yyyymmdd_Pnd_Mm);                                                                                               //Natural: MOVE #MM TO #WORK-MM
                    pnd_Rep_Parm_Pnd_Work_Dd.setValue(pnd_Yyyymmdd_Pnd_Dd);                                                                                               //Natural: MOVE #DD TO #WORK-DD
                    pnd_Rep_Parm_Pnd_Work_Yy.setValue(pnd_Yyyymmdd_Pnd_Yy);                                                                                               //Natural: MOVE #YY TO #WORK-YY
                    pnd_Yyyymmdd.setValue(pnd_Rep_Parm_Pnd_End_Date);                                                                                                     //Natural: MOVE #END-DATE TO #YYYYMMDD
                    pnd_Rep_Parm_Pnd_Work_Mmm.setValue(pnd_Yyyymmdd_Pnd_Mm);                                                                                              //Natural: MOVE #MM TO #WORK-MMM
                    pnd_Rep_Parm_Pnd_Work_Ddd.setValue(pnd_Yyyymmdd_Pnd_Dd);                                                                                              //Natural: MOVE #DD TO #WORK-DDD
                    pnd_Rep_Parm_Pnd_Work_Yyy.setValue(pnd_Yyyymmdd_Pnd_Yy);                                                                                              //Natural: MOVE #YY TO #WORK-YYY
                    //*  START DATE AND END DATE ESTABLISHED
                }                                                                                                                                                         //Natural: END-IF
                pnd_Bldg.setValue("W");                                                                                                                                   //Natural: MOVE 'W' TO #BLDG
                //*  START OF REPORT BANNER
                DbsUtil.callnat(Cwfn3910.class , getCurrentProcessState(), pnd_Report_No, pnd_Racf_Id, pnd_Proc_Unit, pnd_Floor, pnd_Bldg, pnd_Drop_Off,                  //Natural: CALLNAT 'CWFN3910' #REPORT-NO #RACF-ID #PROC-UNIT #FLOOR #BLDG #DROP-OFF #COMP-DATE
                    pnd_Comp_Date);
                if (condition(Global.isEscape())) return;
                Global.getSTACK().pushData(StackOption.TOP, pnd_Rep_Parm_Pnd_Start_Date, pnd_Rep_Parm_Pnd_End_Date);                                                      //Natural: FETCH RETURN 'CWFB3411' #START-DATE #END-DATE
                DbsUtil.invokeMain(DbsUtil.getBlType("CWFB3411"), getCurrentProcessState());
                if (condition(Global.isEscape())) return;
                //*  END OF REPORT BANNER
                DbsUtil.callnat(Cwfn3911.class , getCurrentProcessState());                                                                                               //Natural: CALLNAT 'CWFN3911'
                if (condition(Global.isEscape())) return;
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=58");
    }
}
