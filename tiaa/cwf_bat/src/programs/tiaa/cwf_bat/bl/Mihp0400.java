/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:58:54 PM
**        * FROM NATURAL PROGRAM : Mihp0400
************************************************************
**        * FILE NAME            : Mihp0400.java
**        * CLASS NAME           : Mihp0400
**        * INSTANCE NAME        : Mihp0400
************************************************************
************************************************************************
* PROGRAM    : MIHP0400
* SYSTEM     :
* TITLE      : PURGE MASTER INDEX (AFTER ARCHIVE TO WHO)
* GENERATED  : JAN 31, 95 AT 03:50 PM
* FUNCTION   : THIS JOB IS USED FOR THE PURGING OF MIT RECORDS FOLLOWING
*            : THE CONVERSION FROM MIT TO WORK REQUEST HISTORY OBJECT.
*            : THE FIRST STEP CREATES A LIST OF ISN's of MIT records
*            : IN AN ADABAS FILE, CONSISTING OF MIT RECORDS IN CLOSED
*            : STATUS WHICH PRECEED THE CUT-OFF DATE.
*            :
* CHANGES    : OUTPUT FILE HAS BEEN EXPANDED WITH LOG-DTE-TME
*            :
*            : L.E. 07/24/97 AN ADDITIONAL NUMERIC WPID INCLUDED INTO
*            : THE LOGIC TO AVOID DELETING SURVIVOR's records plus
*            : EVERY WPID CHECKED FOR 3-RD ELEMENT(4-BYTE) TO PREVENT
*            : DELETION.
*            : L.E. 10/31/01
*            : EXCLUDE MDO WORK REQUEST (TA SM;TA SMS;TA DM;TA DMS)
*            : L.E. 04/02/02 EXCLUDE WPID (TA TS)
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Mihp0400 extends BLNatBase
{
    // Data Areas
    private PdaMiha0180 pdaMiha0180;
    private LdaMihl0180 ldaMihl0180;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Who_Work_Request;
    private DbsField cwf_Who_Work_Request_Rqst_Log_Dte_Tme;

    private DataAccessProgramView vw_cwf_Master_Index_View_Select;
    private DbsField cwf_Master_Index_View_Select_Crprte_Status_Chnge_Dte_Key;
    private DbsField cwf_Master_Index_View_Select_Crprte_Status_Ind;
    private DbsField cwf_Master_Index_View_Select_Last_Chnge_Invrt_Dte_Tme;
    private DbsField cwf_Master_Index_View_Select_Last_Chnge_Dte_Tme;
    private DbsField cwf_Master_Index_View_Select_Rqst_Log_Dte_Tme;
    private DbsField cwf_Master_Index_View_Select_Work_Prcss_Id;
    private DbsField cwf_Master_Index_View_Select_Rltnshp_Type;

    private DataAccessProgramView vw_cwf_Master_Index_View;
    private DbsField cwf_Master_Index_View_Rqst_Log_Dte_Tme;
    private DbsField cwf_Master_Index_View_Last_Chnge_Dte_Tme;
    private DbsField cwf_Master_Index_View_Rqst_Routing_Key;

    private DataAccessProgramView vw_cwf_Who_Support_Tbl_Prevent;
    private DbsField cwf_Who_Support_Tbl_Prevent_Tbl_Prime_Key;

    private DataAccessProgramView vw_cwf_Who_Work_Request_Prevent;
    private DbsField cwf_Who_Work_Request_Prevent_Rqst_Log_Dte_Tme;
    private DbsField pnd_End_Of_Window;
    private DbsField pnd_No_Of_Records_Limit;
    private DbsField pnd_Crprte_Status_Chnge_Dte_Key_From;

    private DbsGroup pnd_Crprte_Status_Chnge_Dte_Key_From__R_Field_1;
    private DbsField pnd_Crprte_Status_Chnge_Dte_Key_From_Pnd_Crprte_Status_Ind;
    private DbsField pnd_Crprte_Status_Chnge_Dte_Key_From_Pnd_Last_Chnge_Invrtd_Dte_Tme;
    private DbsField pnd_Crprte_Status_Chnge_Dte_Key_From_Pnd_Admin_Unit_Cde;
    private DbsField pnd_Crprte_Status_Chnge_Dte_Key_From_Pnd_Actv_Ind;
    private DbsField pnd_Crprte_Status_Chnge_Dte_Key_To;

    private DbsGroup pnd_Crprte_Status_Chnge_Dte_Key_To__R_Field_2;
    private DbsField pnd_Crprte_Status_Chnge_Dte_Key_To_Pnd_Crprte_Status_Ind;
    private DbsField pnd_Crprte_Status_Chnge_Dte_Key_To_Pnd_Last_Chnge_Invrtd_Dte_Tme;
    private DbsField pnd_Crprte_Status_Chnge_Dte_Key_To_Pnd_Admin_Unit_Cde;
    private DbsField pnd_Crprte_Status_Chnge_Dte_Key_To_Pnd_Actv_Ind;
    private DbsField pnd_Start_Master_Key;

    private DbsGroup pnd_Start_Master_Key__R_Field_3;
    private DbsField pnd_Start_Master_Key_Pnd_Start_Rqst_Log_Dte_Tme;
    private DbsField pnd_Start_Master_Key_Pnd_Start_Last_Chnge_Dte_Tme;
    private DbsField pnd_Nbr_Of_Events_Purged;
    private DbsField pnd_Nbr_Of_Rqsts_Purged;
    private DbsField pnd_Nbr_Of_Isn_Listed;
    private DbsField pnd_Inverted_Last_Chnge_Dte_Tme;
    private DbsField pnd_Convert_Date_Time_Field;

    private DbsGroup pnd_Convert_Date_Time_Field__R_Field_4;
    private DbsField pnd_Convert_Date_Time_Field_Pnd_Convert_Date_Field;
    private DbsField pnd_Convert_Date_Time_Field_Pnd_Convert_Time_Field;
    private DbsField pnd_Work_File_Isn_N;

    private DbsGroup pnd_Work_File_Record;
    private DbsField pnd_Work_File_Record_Pnd_Work_File_Isn;
    private DbsField pnd_Work_File_Record_Pnd_Work_Log_Dte_Tme;
    private DbsField pnd_This_Program_Has_Finished_Successfully;
    private DbsField pnd_Work_Prcss_Id_Split;

    private DbsGroup pnd_Work_Prcss_Id_Split__R_Field_5;
    private DbsField pnd_Work_Prcss_Id_Split_Pnd_Work_Prcss_Id_Split_1_3;
    private DbsField pnd_Work_Prcss_Id_Split_Pnd_Work_Prcss_Id_Split_4;
    private DbsField pnd_Work_Prcss_Id_Split_Pnd_Work_Prcss_Id_Split_5_6;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaMiha0180 = new PdaMiha0180(localVariables);
        ldaMihl0180 = new LdaMihl0180();
        registerRecord(ldaMihl0180);
        registerRecord(ldaMihl0180.getVw_cwf_Who_Support_Tbl());
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);

        // Local Variables

        vw_cwf_Who_Work_Request = new DataAccessProgramView(new NameInfo("vw_cwf_Who_Work_Request", "CWF-WHO-WORK-REQUEST"), "CWF_WHO_WORK_REQUEST", "CWF_WORK_REQUEST");
        cwf_Who_Work_Request_Rqst_Log_Dte_Tme = vw_cwf_Who_Work_Request.getRecord().newFieldInGroup("cwf_Who_Work_Request_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Who_Work_Request_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        registerRecord(vw_cwf_Who_Work_Request);

        vw_cwf_Master_Index_View_Select = new DataAccessProgramView(new NameInfo("vw_cwf_Master_Index_View_Select", "CWF-MASTER-INDEX-VIEW-SELECT"), "CWF_MASTER_INDEX_VIEW", 
            "CWF_MASTER_INDEX");
        cwf_Master_Index_View_Select_Crprte_Status_Chnge_Dte_Key = vw_cwf_Master_Index_View_Select.getRecord().newFieldInGroup("cwf_Master_Index_View_Select_Crprte_Status_Chnge_Dte_Key", 
            "CRPRTE-STATUS-CHNGE-DTE-KEY", FieldType.STRING, 25, RepeatingFieldStrategy.None, "CRPRTE_STATUS_CHNGE_DTE_KEY");
        cwf_Master_Index_View_Select_Crprte_Status_Chnge_Dte_Key.setDdmHeader("STATUS-CHANGE-DATE-KEY");
        cwf_Master_Index_View_Select_Crprte_Status_Chnge_Dte_Key.setSuperDescriptor(true);
        cwf_Master_Index_View_Select_Crprte_Status_Ind = vw_cwf_Master_Index_View_Select.getRecord().newFieldInGroup("cwf_Master_Index_View_Select_Crprte_Status_Ind", 
            "CRPRTE-STATUS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CRPRTE_STATUS_IND");
        cwf_Master_Index_View_Select_Crprte_Status_Ind.setDdmHeader("CORP/STAT");
        cwf_Master_Index_View_Select_Last_Chnge_Invrt_Dte_Tme = vw_cwf_Master_Index_View_Select.getRecord().newFieldInGroup("cwf_Master_Index_View_Select_Last_Chnge_Invrt_Dte_Tme", 
            "LAST-CHNGE-INVRT-DTE-TME", FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "LAST_CHNGE_INVRT_DTE_TME");
        cwf_Master_Index_View_Select_Last_Chnge_Dte_Tme = vw_cwf_Master_Index_View_Select.getRecord().newFieldInGroup("cwf_Master_Index_View_Select_Last_Chnge_Dte_Tme", 
            "LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "LAST_CHNGE_DTE_TME");
        cwf_Master_Index_View_Select_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");
        cwf_Master_Index_View_Select_Rqst_Log_Dte_Tme = vw_cwf_Master_Index_View_Select.getRecord().newFieldInGroup("cwf_Master_Index_View_Select_Rqst_Log_Dte_Tme", 
            "RQST-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Master_Index_View_Select_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        cwf_Master_Index_View_Select_Work_Prcss_Id = vw_cwf_Master_Index_View_Select.getRecord().newFieldInGroup("cwf_Master_Index_View_Select_Work_Prcss_Id", 
            "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        cwf_Master_Index_View_Select_Work_Prcss_Id.setDdmHeader("WORK/ID");
        cwf_Master_Index_View_Select_Rltnshp_Type = vw_cwf_Master_Index_View_Select.getRecord().newFieldInGroup("cwf_Master_Index_View_Select_Rltnshp_Type", 
            "RLTNSHP-TYPE", FieldType.STRING, 5, RepeatingFieldStrategy.None, "RLTNSHP_TYPE");
        registerRecord(vw_cwf_Master_Index_View_Select);

        vw_cwf_Master_Index_View = new DataAccessProgramView(new NameInfo("vw_cwf_Master_Index_View", "CWF-MASTER-INDEX-VIEW"), "CWF_MASTER_INDEX_VIEW", 
            "CWF_MASTER_INDEX");
        cwf_Master_Index_View_Rqst_Log_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Master_Index_View_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        cwf_Master_Index_View_Last_Chnge_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "LAST_CHNGE_DTE_TME");
        cwf_Master_Index_View_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");
        cwf_Master_Index_View_Rqst_Routing_Key = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Routing_Key", "RQST-ROUTING-KEY", 
            FieldType.BINARY, 30, RepeatingFieldStrategy.None, "RQST_ROUTING_KEY");
        cwf_Master_Index_View_Rqst_Routing_Key.setSuperDescriptor(true);
        registerRecord(vw_cwf_Master_Index_View);

        vw_cwf_Who_Support_Tbl_Prevent = new DataAccessProgramView(new NameInfo("vw_cwf_Who_Support_Tbl_Prevent", "CWF-WHO-SUPPORT-TBL-PREVENT"), "CWF_WHO_SUPPORT_TBL", 
            "CWF_WHO_SUPPORT");
        cwf_Who_Support_Tbl_Prevent_Tbl_Prime_Key = vw_cwf_Who_Support_Tbl_Prevent.getRecord().newFieldInGroup("cwf_Who_Support_Tbl_Prevent_Tbl_Prime_Key", 
            "TBL-PRIME-KEY", FieldType.STRING, 53, RepeatingFieldStrategy.None, "TBL_PRIME_KEY");
        cwf_Who_Support_Tbl_Prevent_Tbl_Prime_Key.setDdmHeader("RECORD KEY");
        cwf_Who_Support_Tbl_Prevent_Tbl_Prime_Key.setSuperDescriptor(true);
        registerRecord(vw_cwf_Who_Support_Tbl_Prevent);

        vw_cwf_Who_Work_Request_Prevent = new DataAccessProgramView(new NameInfo("vw_cwf_Who_Work_Request_Prevent", "CWF-WHO-WORK-REQUEST-PREVENT"), "CWF_WHO_WORK_REQUEST", 
            "CWF_WORK_REQUEST");
        cwf_Who_Work_Request_Prevent_Rqst_Log_Dte_Tme = vw_cwf_Who_Work_Request_Prevent.getRecord().newFieldInGroup("cwf_Who_Work_Request_Prevent_Rqst_Log_Dte_Tme", 
            "RQST-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Who_Work_Request_Prevent_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        registerRecord(vw_cwf_Who_Work_Request_Prevent);

        pnd_End_Of_Window = localVariables.newFieldInRecord("pnd_End_Of_Window", "#END-OF-WINDOW", FieldType.STRING, 1);
        pnd_No_Of_Records_Limit = localVariables.newFieldInRecord("pnd_No_Of_Records_Limit", "#NO-OF-RECORDS-LIMIT", FieldType.NUMERIC, 9);
        pnd_Crprte_Status_Chnge_Dte_Key_From = localVariables.newFieldInRecord("pnd_Crprte_Status_Chnge_Dte_Key_From", "#CRPRTE-STATUS-CHNGE-DTE-KEY-FROM", 
            FieldType.STRING, 25);

        pnd_Crprte_Status_Chnge_Dte_Key_From__R_Field_1 = localVariables.newGroupInRecord("pnd_Crprte_Status_Chnge_Dte_Key_From__R_Field_1", "REDEFINE", 
            pnd_Crprte_Status_Chnge_Dte_Key_From);
        pnd_Crprte_Status_Chnge_Dte_Key_From_Pnd_Crprte_Status_Ind = pnd_Crprte_Status_Chnge_Dte_Key_From__R_Field_1.newFieldInGroup("pnd_Crprte_Status_Chnge_Dte_Key_From_Pnd_Crprte_Status_Ind", 
            "#CRPRTE-STATUS-IND", FieldType.STRING, 1);
        pnd_Crprte_Status_Chnge_Dte_Key_From_Pnd_Last_Chnge_Invrtd_Dte_Tme = pnd_Crprte_Status_Chnge_Dte_Key_From__R_Field_1.newFieldInGroup("pnd_Crprte_Status_Chnge_Dte_Key_From_Pnd_Last_Chnge_Invrtd_Dte_Tme", 
            "#LAST-CHNGE-INVRTD-DTE-TME", FieldType.NUMERIC, 15);
        pnd_Crprte_Status_Chnge_Dte_Key_From_Pnd_Admin_Unit_Cde = pnd_Crprte_Status_Chnge_Dte_Key_From__R_Field_1.newFieldInGroup("pnd_Crprte_Status_Chnge_Dte_Key_From_Pnd_Admin_Unit_Cde", 
            "#ADMIN-UNIT-CDE", FieldType.STRING, 8);
        pnd_Crprte_Status_Chnge_Dte_Key_From_Pnd_Actv_Ind = pnd_Crprte_Status_Chnge_Dte_Key_From__R_Field_1.newFieldInGroup("pnd_Crprte_Status_Chnge_Dte_Key_From_Pnd_Actv_Ind", 
            "#ACTV-IND", FieldType.STRING, 1);
        pnd_Crprte_Status_Chnge_Dte_Key_To = localVariables.newFieldInRecord("pnd_Crprte_Status_Chnge_Dte_Key_To", "#CRPRTE-STATUS-CHNGE-DTE-KEY-TO", 
            FieldType.STRING, 25);

        pnd_Crprte_Status_Chnge_Dte_Key_To__R_Field_2 = localVariables.newGroupInRecord("pnd_Crprte_Status_Chnge_Dte_Key_To__R_Field_2", "REDEFINE", pnd_Crprte_Status_Chnge_Dte_Key_To);
        pnd_Crprte_Status_Chnge_Dte_Key_To_Pnd_Crprte_Status_Ind = pnd_Crprte_Status_Chnge_Dte_Key_To__R_Field_2.newFieldInGroup("pnd_Crprte_Status_Chnge_Dte_Key_To_Pnd_Crprte_Status_Ind", 
            "#CRPRTE-STATUS-IND", FieldType.STRING, 1);
        pnd_Crprte_Status_Chnge_Dte_Key_To_Pnd_Last_Chnge_Invrtd_Dte_Tme = pnd_Crprte_Status_Chnge_Dte_Key_To__R_Field_2.newFieldInGroup("pnd_Crprte_Status_Chnge_Dte_Key_To_Pnd_Last_Chnge_Invrtd_Dte_Tme", 
            "#LAST-CHNGE-INVRTD-DTE-TME", FieldType.NUMERIC, 15);
        pnd_Crprte_Status_Chnge_Dte_Key_To_Pnd_Admin_Unit_Cde = pnd_Crprte_Status_Chnge_Dte_Key_To__R_Field_2.newFieldInGroup("pnd_Crprte_Status_Chnge_Dte_Key_To_Pnd_Admin_Unit_Cde", 
            "#ADMIN-UNIT-CDE", FieldType.STRING, 8);
        pnd_Crprte_Status_Chnge_Dte_Key_To_Pnd_Actv_Ind = pnd_Crprte_Status_Chnge_Dte_Key_To__R_Field_2.newFieldInGroup("pnd_Crprte_Status_Chnge_Dte_Key_To_Pnd_Actv_Ind", 
            "#ACTV-IND", FieldType.STRING, 1);
        pnd_Start_Master_Key = localVariables.newFieldInRecord("pnd_Start_Master_Key", "#START-MASTER-KEY", FieldType.STRING, 30);

        pnd_Start_Master_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Start_Master_Key__R_Field_3", "REDEFINE", pnd_Start_Master_Key);
        pnd_Start_Master_Key_Pnd_Start_Rqst_Log_Dte_Tme = pnd_Start_Master_Key__R_Field_3.newFieldInGroup("pnd_Start_Master_Key_Pnd_Start_Rqst_Log_Dte_Tme", 
            "#START-RQST-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Start_Master_Key_Pnd_Start_Last_Chnge_Dte_Tme = pnd_Start_Master_Key__R_Field_3.newFieldInGroup("pnd_Start_Master_Key_Pnd_Start_Last_Chnge_Dte_Tme", 
            "#START-LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 15);
        pnd_Nbr_Of_Events_Purged = localVariables.newFieldInRecord("pnd_Nbr_Of_Events_Purged", "#NBR-OF-EVENTS-PURGED", FieldType.PACKED_DECIMAL, 9);
        pnd_Nbr_Of_Rqsts_Purged = localVariables.newFieldInRecord("pnd_Nbr_Of_Rqsts_Purged", "#NBR-OF-RQSTS-PURGED", FieldType.PACKED_DECIMAL, 9);
        pnd_Nbr_Of_Isn_Listed = localVariables.newFieldInRecord("pnd_Nbr_Of_Isn_Listed", "#NBR-OF-ISN-LISTED", FieldType.PACKED_DECIMAL, 9);
        pnd_Inverted_Last_Chnge_Dte_Tme = localVariables.newFieldInRecord("pnd_Inverted_Last_Chnge_Dte_Tme", "#INVERTED-LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 
            15);
        pnd_Convert_Date_Time_Field = localVariables.newFieldInRecord("pnd_Convert_Date_Time_Field", "#CONVERT-DATE-TIME-FIELD", FieldType.NUMERIC, 15);

        pnd_Convert_Date_Time_Field__R_Field_4 = localVariables.newGroupInRecord("pnd_Convert_Date_Time_Field__R_Field_4", "REDEFINE", pnd_Convert_Date_Time_Field);
        pnd_Convert_Date_Time_Field_Pnd_Convert_Date_Field = pnd_Convert_Date_Time_Field__R_Field_4.newFieldInGroup("pnd_Convert_Date_Time_Field_Pnd_Convert_Date_Field", 
            "#CONVERT-DATE-FIELD", FieldType.NUMERIC, 8);
        pnd_Convert_Date_Time_Field_Pnd_Convert_Time_Field = pnd_Convert_Date_Time_Field__R_Field_4.newFieldInGroup("pnd_Convert_Date_Time_Field_Pnd_Convert_Time_Field", 
            "#CONVERT-TIME-FIELD", FieldType.NUMERIC, 7);
        pnd_Work_File_Isn_N = localVariables.newFieldInRecord("pnd_Work_File_Isn_N", "#WORK-FILE-ISN-N", FieldType.NUMERIC, 9);

        pnd_Work_File_Record = localVariables.newGroupInRecord("pnd_Work_File_Record", "#WORK-FILE-RECORD");
        pnd_Work_File_Record_Pnd_Work_File_Isn = pnd_Work_File_Record.newFieldInGroup("pnd_Work_File_Record_Pnd_Work_File_Isn", "#WORK-FILE-ISN", FieldType.BINARY, 
            4);
        pnd_Work_File_Record_Pnd_Work_Log_Dte_Tme = pnd_Work_File_Record.newFieldInGroup("pnd_Work_File_Record_Pnd_Work_Log_Dte_Tme", "#WORK-LOG-DTE-TME", 
            FieldType.STRING, 15);
        pnd_This_Program_Has_Finished_Successfully = localVariables.newFieldInRecord("pnd_This_Program_Has_Finished_Successfully", "#THIS-PROGRAM-HAS-FINISHED-SUCCESSFULLY", 
            FieldType.BOOLEAN, 1);
        pnd_Work_Prcss_Id_Split = localVariables.newFieldInRecord("pnd_Work_Prcss_Id_Split", "#WORK-PRCSS-ID-SPLIT", FieldType.STRING, 6);

        pnd_Work_Prcss_Id_Split__R_Field_5 = localVariables.newGroupInRecord("pnd_Work_Prcss_Id_Split__R_Field_5", "REDEFINE", pnd_Work_Prcss_Id_Split);
        pnd_Work_Prcss_Id_Split_Pnd_Work_Prcss_Id_Split_1_3 = pnd_Work_Prcss_Id_Split__R_Field_5.newFieldInGroup("pnd_Work_Prcss_Id_Split_Pnd_Work_Prcss_Id_Split_1_3", 
            "#WORK-PRCSS-ID-SPLIT-1-3", FieldType.STRING, 3);
        pnd_Work_Prcss_Id_Split_Pnd_Work_Prcss_Id_Split_4 = pnd_Work_Prcss_Id_Split__R_Field_5.newFieldInGroup("pnd_Work_Prcss_Id_Split_Pnd_Work_Prcss_Id_Split_4", 
            "#WORK-PRCSS-ID-SPLIT-4", FieldType.STRING, 1);
        pnd_Work_Prcss_Id_Split_Pnd_Work_Prcss_Id_Split_5_6 = pnd_Work_Prcss_Id_Split__R_Field_5.newFieldInGroup("pnd_Work_Prcss_Id_Split_Pnd_Work_Prcss_Id_Split_5_6", 
            "#WORK-PRCSS-ID-SPLIT-5-6", FieldType.STRING, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Who_Work_Request.reset();
        vw_cwf_Master_Index_View_Select.reset();
        vw_cwf_Master_Index_View.reset();
        vw_cwf_Who_Support_Tbl_Prevent.reset();
        vw_cwf_Who_Work_Request_Prevent.reset();

        ldaMihl0180.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Mihp0400() throws Exception
    {
        super("Mihp0400");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 55 LS = 132;//Natural: FORMAT ( 1 ) PS = 55 LS = 132
        //*  ASSIGN THE LIMIT OF THE NUMBER OF RECORDS TO BE PROCESSED BETWEEN
        //*  COMMITTING AN END TRANSACTION.
        //*  ----------------------------------------------------------------------
        pnd_No_Of_Records_Limit.setValue(100000);                                                                                                                         //Natural: ASSIGN #NO-OF-RECORDS-LIMIT := 100000
        //*  INITIALIZE
        //*  ----------
        pnd_This_Program_Has_Finished_Successfully.setValue(false);                                                                                                       //Natural: ASSIGN #THIS-PROGRAM-HAS-FINISHED-SUCCESSFULLY := FALSE
        //*   TO ALLOW FOR EASY ESCAPE FROM PROGRAM.
        REP1:                                                                                                                                                             //Natural: REPEAT
        while (condition(whileTrue))
        {
                                                                                                                                                                          //Natural: PERFORM PREVENT-INVALID-CID
            sub_Prevent_Invalid_Cid();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM READ-RUN-CONTROL
            sub_Read_Run_Control();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM SELECT-MASTER-RECORDS
            sub_Select_Master_Records();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM TRANSACTION-ROUTINE
            sub_Transaction_Routine();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_This_Program_Has_Finished_Successfully.setValue(true);                                                                                                    //Natural: ASSIGN #THIS-PROGRAM-HAS-FINISHED-SUCCESSFULLY := TRUE
                                                                                                                                                                          //Natural: PERFORM ESCAPE-PROGRAM
            sub_Escape_Program();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ESCAPE-PROGRAM
            //*   (REP1.)
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  CALL MIHP0410 TO COMPLETE THE RUN CONTROL RECORD IF SUCCESSFUL
        //*  --------------------------------------------------------------
        //*  IF #THIS-PROGRAM-HAS-FINISHED-SUCCESSFULLY = TRUE
        //*   FETCH 'MIHP0410'
        //*  END-IF /*(0870)
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PREVENT-INVALID-CID
        //*  ===================================
        //*  ISSUE A DUMMY ACCESS TO THE CWF-WHO-SUPPORT-TBL TO OPEN THE
        //*  DATABASE FILE AND PREVENT A CID ERROR OCCURING.
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-RUN-CONTROL
        //*  USE THE RUN CONTROL RECORD TO POPULATE THE STARTING KEY FIELD
        //*  FOR SELECTING MIT RECORDS. BECAUSE THE READ USES AN INVERTED LAST
        //*  CHNGE DTE TME, THE ENDING DATE IS USED TO POPULATE THE STARTING KEY.
        //*  ---------------------------------------------------------------------
        //*  BECAUSE THE READ USES AN INVERTED LAST-CHNGE-DTE-TME, THE STARTING
        //*  DATE IS USED TO POPULATE THE ENDING KEY.
        //*  ----------------------------------------------------------------------
        //* ******* CHANGED BY L.E. 06/07/96
        //*  #CRPRTE-STATUS-CHNGE-DTE-KEY-TO.#LAST-CHNGE-INVRTD-DTE-TME :=
        //*                                   999999999999999
        //* ******* CHANGED BY L.E. 06/07/96
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SELECT-MASTER-RECORDS
        //*  WRITE THE ISN's & RQST-LOG-DTE-TME to a  Work file to be later used
        //*  BY THE DELETION PROGRAM MIHP0600
        //*  ----------------------------------------------------------------------
        //*        #WORK-FILE-RECORD := *ISN(ACCESS-MASTER-EVENTS.)
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TRANSACTION-ROUTINE
    }
    private void sub_Escape_Program() throws Exception                                                                                                                    //Natural: ESCAPE-PROGRAM
    {
        if (BLNatReinput.isReinput()) return;

        //*  ================================
        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( REP1. )
        Global.setEscapeCode(EscapeType.Bottom, "REP1");
        if (true) return;
    }
    private void sub_Prevent_Invalid_Cid() throws Exception                                                                                                               //Natural: PREVENT-INVALID-CID
    {
        if (BLNatReinput.isReinput()) return;

        vw_cwf_Who_Support_Tbl_Prevent.createHistogram                                                                                                                    //Natural: HISTOGRAM ( 1 ) CWF-WHO-SUPPORT-TBL-PREVENT TBL-PRIME-KEY
        (
        "HIST01",
        "TBL_PRIME_KEY",
        1
        );
        HIST01:
        while (condition(vw_cwf_Who_Support_Tbl_Prevent.readNextRow("HIST01")))
        {
        }                                                                                                                                                                 //Natural: END-HISTOGRAM
        if (Global.isEscape()) return;
        vw_cwf_Who_Work_Request_Prevent.createHistogram                                                                                                                   //Natural: HISTOGRAM ( 1 ) CWF-WHO-WORK-REQUEST-PREVENT RQST-LOG-DTE-TME
        (
        "HIST02",
        "RQST_LOG_DTE_TME",
        1
        );
        HIST02:
        while (condition(vw_cwf_Who_Work_Request_Prevent.readNextRow("HIST02")))
        {
        }                                                                                                                                                                 //Natural: END-HISTOGRAM
        if (Global.isEscape()) return;
        //*  (1330)
    }
    private void sub_Read_Run_Control() throws Exception                                                                                                                  //Natural: READ-RUN-CONTROL
    {
        if (BLNatReinput.isReinput()) return;

        //*  ================================
        //*  CALL THE SUB PROGRAM 'Set up Run Control for the MIT Purge
        //*  PROCESS'. If a critical error is returned, print message & terminate
        //*  THE JOB. COMMIT TRANSACTION FOR RUN-CONTROL SET UP.
        //*  ----------------------------------------------------------------------
        DbsUtil.callnat(Mihn0180.class , getCurrentProcessState(), pdaMiha0180.getMiha0180_Output(), pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(),             //Natural: CALLNAT 'MIHN0180' MIHA0180-OUTPUT CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
            pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("E") || pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("W")))                    //Natural: IF MSG-INFO-SUB.##RETURN-CODE = 'E' OR MSG-INFO-SUB.##RETURN-CODE = 'W'
        {
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(10),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                     //Natural: WRITE ( 1 ) 10T MSG-INFO-SUB.##MSG
            if (Global.isEscape()) return;
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("E")))                                                                                 //Natural: IF MSG-INFO-SUB.##RETURN-CODE = 'E'
            {
                DbsUtil.terminate(4);  if (true) return;                                                                                                                  //Natural: TERMINATE 4
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().equals(999)))                                                                                          //Natural: IF MSG-INFO-SUB.##MSG-NR = 999
        {
            getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(10),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                  //Natural: WRITE ( 1 ) NOTITLE NOHDR 10T MSG-INFO-SUB.##MSG
            if (Global.isEscape()) return;
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
            DbsUtil.terminate(4);  if (true) return;                                                                                                                      //Natural: TERMINATE 4
        }                                                                                                                                                                 //Natural: END-IF
        //*  COMMIT THE TRANSACTION FOR THE RUN CONTROL SET UP
        //*  -------------------------------------------------
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  ACCESS THE RUN CONTROL RECORD USING OUTPUT.ISN
        //*  ----------------------------------------------------------------------
        GET_TRANSACTION:                                                                                                                                                  //Natural: GET CWF-WHO-SUPPORT-TBL MIHA0180-OUTPUT.RUN-CONTROL-ISN
        ldaMihl0180.getVw_cwf_Who_Support_Tbl().readByID(pdaMiha0180.getMiha0180_Output_Run_Control_Isn().getLong(), "GET_TRANSACTION");
        pnd_Convert_Date_Time_Field_Pnd_Convert_Date_Field.setValue(ldaMihl0180.getCwf_Who_Support_Tbl_Ending_Date());                                                    //Natural: ASSIGN #CONVERT-DATE-FIELD := CWF-WHO-SUPPORT-TBL.ENDING-DATE
        pnd_Convert_Date_Time_Field_Pnd_Convert_Time_Field.setValue(9999999);                                                                                             //Natural: ASSIGN #CONVERT-TIME-FIELD := 9999999
        pnd_Inverted_Last_Chnge_Dte_Tme.compute(new ComputeParameters(false, pnd_Inverted_Last_Chnge_Dte_Tme), new DbsDecimal("999999999999999").subtract(pnd_Convert_Date_Time_Field)); //Natural: COMPUTE #INVERTED-LAST-CHNGE-DTE-TME = 999999999999999 - #CONVERT-DATE-TIME-FIELD
        pnd_Crprte_Status_Chnge_Dte_Key_From_Pnd_Crprte_Status_Ind.setValue("9");                                                                                         //Natural: ASSIGN #CRPRTE-STATUS-CHNGE-DTE-KEY-FROM.#CRPRTE-STATUS-IND := '9'
        pnd_Crprte_Status_Chnge_Dte_Key_From_Pnd_Last_Chnge_Invrtd_Dte_Tme.setValue(pnd_Inverted_Last_Chnge_Dte_Tme);                                                     //Natural: ASSIGN #CRPRTE-STATUS-CHNGE-DTE-KEY-FROM.#LAST-CHNGE-INVRTD-DTE-TME := #INVERTED-LAST-CHNGE-DTE-TME
        pnd_Crprte_Status_Chnge_Dte_Key_From_Pnd_Admin_Unit_Cde.setValue(" ");                                                                                            //Natural: ASSIGN #CRPRTE-STATUS-CHNGE-DTE-KEY-FROM.#ADMIN-UNIT-CDE := ' '
        pnd_Crprte_Status_Chnge_Dte_Key_From_Pnd_Actv_Ind.setValue(" ");                                                                                                  //Natural: ASSIGN #CRPRTE-STATUS-CHNGE-DTE-KEY-FROM.#ACTV-IND := ' '
        pnd_Convert_Date_Time_Field_Pnd_Convert_Date_Field.setValue(ldaMihl0180.getCwf_Who_Support_Tbl_Starting_Date());                                                  //Natural: ASSIGN #CONVERT-DATE-FIELD := CWF-WHO-SUPPORT-TBL.STARTING-DATE
        pnd_Convert_Date_Time_Field_Pnd_Convert_Time_Field.setValue(0);                                                                                                   //Natural: ASSIGN #CONVERT-TIME-FIELD := 0
        pnd_Inverted_Last_Chnge_Dte_Tme.compute(new ComputeParameters(false, pnd_Inverted_Last_Chnge_Dte_Tme), new DbsDecimal("999999999999999").subtract(pnd_Convert_Date_Time_Field)); //Natural: COMPUTE #INVERTED-LAST-CHNGE-DTE-TME = 999999999999999 - #CONVERT-DATE-TIME-FIELD
        pnd_Crprte_Status_Chnge_Dte_Key_To_Pnd_Crprte_Status_Ind.setValue("9");                                                                                           //Natural: ASSIGN #CRPRTE-STATUS-CHNGE-DTE-KEY-TO.#CRPRTE-STATUS-IND := '9'
        pnd_Crprte_Status_Chnge_Dte_Key_To_Pnd_Last_Chnge_Invrtd_Dte_Tme.setValue(pnd_Inverted_Last_Chnge_Dte_Tme);                                                       //Natural: ASSIGN #CRPRTE-STATUS-CHNGE-DTE-KEY-TO.#LAST-CHNGE-INVRTD-DTE-TME := #INVERTED-LAST-CHNGE-DTE-TME
        pnd_Crprte_Status_Chnge_Dte_Key_To_Pnd_Admin_Unit_Cde.setValue("99999999");                                                                                       //Natural: ASSIGN #CRPRTE-STATUS-CHNGE-DTE-KEY-TO.#ADMIN-UNIT-CDE := '99999999'
        pnd_Crprte_Status_Chnge_Dte_Key_To_Pnd_Actv_Ind.setValue("9");                                                                                                    //Natural: ASSIGN #CRPRTE-STATUS-CHNGE-DTE-KEY-TO.#ACTV-IND := '9'
        //*  (1460)
    }
    private void sub_Select_Master_Records() throws Exception                                                                                                             //Natural: SELECT-MASTER-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        //*  =====================================
        //*  SELECT CLOSED MIT RECORDS WHICH HAVE A LAST CHANGE DATE TIME EARLIER
        //*  THAN THE RUN CONTROL.PURGE CUT-OFF DATE. NOTE: THE SUPERDESCRIPTER
        //*  USES AN INVERTED LAST-CHNGE-DTE-TME - I.E. READING BACKWARDS,
        //*  FROM THE PURGE CUT-OFF DATE TO THE BEGINNING OF THE RUN WINDOW.
        //*  ---------------------------------------------------------------------
        vw_cwf_Master_Index_View_Select.startDatabaseRead                                                                                                                 //Natural: READ CWF-MASTER-INDEX-VIEW-SELECT BY CRPRTE-STATUS-CHNGE-DTE-KEY STARTING FROM #CRPRTE-STATUS-CHNGE-DTE-KEY-FROM ENDING AT #CRPRTE-STATUS-CHNGE-DTE-KEY-TO
        (
        "SELECT_CLOSED_MIT",
        new Wc[] { new Wc("CRPRTE_STATUS_CHNGE_DTE_KEY", ">=", pnd_Crprte_Status_Chnge_Dte_Key_From, "And", WcType.BY) ,
        new Wc("CRPRTE_STATUS_CHNGE_DTE_KEY", "<=", pnd_Crprte_Status_Chnge_Dte_Key_To, WcType.BY) },
        new Oc[] { new Oc("CRPRTE_STATUS_CHNGE_DTE_KEY", "ASC") }
        );
        SELECT_CLOSED_MIT:
        while (condition(vw_cwf_Master_Index_View_Select.readNextRow("SELECT_CLOSED_MIT")))
        {
            //*  TO REJECT IF WPID
            pnd_Work_Prcss_Id_Split.setValue(cwf_Master_Index_View_Select_Work_Prcss_Id);                                                                                 //Natural: MOVE WORK-PRCSS-ID TO #WORK-PRCSS-ID-SPLIT
            //*  BELONG TO SURVIVOR
            if (condition(pnd_Work_Prcss_Id_Split_Pnd_Work_Prcss_Id_Split_4.equals("D")))                                                                                 //Natural: REJECT IF #WORK-PRCSS-ID-SPLIT-4 = 'D'
            {
                continue;
            }
            //*  --------- EXCLUDE MDO WORK REQUEST  10/31/01
            if (condition(cwf_Master_Index_View_Select_Work_Prcss_Id.equals("TA SM ") || cwf_Master_Index_View_Select_Work_Prcss_Id.equals("TA SMS") ||                   //Natural: REJECT IF WORK-PRCSS-ID = 'TA SM ' OR = 'TA SMS' OR = 'TA DM ' OR = 'TA DMS' OR = 'TA TS'
                cwf_Master_Index_View_Select_Work_Prcss_Id.equals("TA DM ") || cwf_Master_Index_View_Select_Work_Prcss_Id.equals("TA DMS") || cwf_Master_Index_View_Select_Work_Prcss_Id.equals("TA TS")))
            {
                continue;
            }
            //*  WPID FOR SURVIVOR's
            if (condition(cwf_Master_Index_View_Select_Work_Prcss_Id.equals("TACCK ") || cwf_Master_Index_View_Select_Work_Prcss_Id.equals("TAGCK ") ||                   //Natural: REJECT IF WORK-PRCSS-ID = 'TACCK ' OR = 'TAGCK ' OR = 'TABCK ' OR = 'TASCK ' OR = 'TAKCK ' OR = 'TA TJ ' OR = 'TA TJM' OR = '2130  ' OR = '2221  ' OR = '2223  ' OR = '2227  ' OR = '2599  ' OR = '6106  ' OR = '6122  ' OR = '6131  ' OR = '6133  ' OR = '6267  ' OR = '6268  ' OR = '6282  ' OR = '6284  ' OR = '6287  ' OR = '6290  ' OR = '6367  ' OR = '6767  '
                cwf_Master_Index_View_Select_Work_Prcss_Id.equals("TABCK ") || cwf_Master_Index_View_Select_Work_Prcss_Id.equals("TASCK ") || cwf_Master_Index_View_Select_Work_Prcss_Id.equals("TAKCK ") 
                || cwf_Master_Index_View_Select_Work_Prcss_Id.equals("TA TJ ") || cwf_Master_Index_View_Select_Work_Prcss_Id.equals("TA TJM") || cwf_Master_Index_View_Select_Work_Prcss_Id.equals("2130  ") 
                || cwf_Master_Index_View_Select_Work_Prcss_Id.equals("2221  ") || cwf_Master_Index_View_Select_Work_Prcss_Id.equals("2223  ") || cwf_Master_Index_View_Select_Work_Prcss_Id.equals("2227  ") 
                || cwf_Master_Index_View_Select_Work_Prcss_Id.equals("2599  ") || cwf_Master_Index_View_Select_Work_Prcss_Id.equals("6106  ") || cwf_Master_Index_View_Select_Work_Prcss_Id.equals("6122  ") 
                || cwf_Master_Index_View_Select_Work_Prcss_Id.equals("6131  ") || cwf_Master_Index_View_Select_Work_Prcss_Id.equals("6133  ") || cwf_Master_Index_View_Select_Work_Prcss_Id.equals("6267  ") 
                || cwf_Master_Index_View_Select_Work_Prcss_Id.equals("6268  ") || cwf_Master_Index_View_Select_Work_Prcss_Id.equals("6282  ") || cwf_Master_Index_View_Select_Work_Prcss_Id.equals("6284  ") 
                || cwf_Master_Index_View_Select_Work_Prcss_Id.equals("6287  ") || cwf_Master_Index_View_Select_Work_Prcss_Id.equals("6290  ") || cwf_Master_Index_View_Select_Work_Prcss_Id.equals("6367  ") 
                || cwf_Master_Index_View_Select_Work_Prcss_Id.equals("6767  ")))
            {
                continue;
            }
            //*  ------- REJECT WHEN RELATIONSHIP EXIST -- 06/25/01
            if (condition(cwf_Master_Index_View_Select_Rltnshp_Type.greater(" ")))                                                                                        //Natural: REJECT IF RLTNSHP-TYPE GT ' '
            {
                continue;
            }
            //*  CHECK FOR A RECORD ON THE WHO-WORK-REQUEST WITH THE SAME LOG-DTE-TME
            //*  IF IT DOES NOT EXIST OR THE MIT IS NOT IN CLOSED STATUS, DO NOT ADD
            //*  THE MIT RECORD TO THE LIST OF ISN's to be purged.
            //*  ----------------------------------------------------------------------
            vw_cwf_Who_Work_Request.createHistogram                                                                                                                       //Natural: HISTOGRAM CWF-WHO-WORK-REQUEST RQST-LOG-DTE-TME STARTING FROM CWF-MASTER-INDEX-VIEW-SELECT.RQST-LOG-DTE-TME ENDING AT CWF-MASTER-INDEX-VIEW-SELECT.RQST-LOG-DTE-TME
            (
            "HISTOGRAM_WHO_RQST",
            "RQST_LOG_DTE_TME",
            new Wc[] { new Wc("RQST_LOG_DTE_TME", ">=", cwf_Master_Index_View_Select_Rqst_Log_Dte_Tme, "And", WcType.WITH) ,
            new Wc("RQST_LOG_DTE_TME", "<=", cwf_Master_Index_View_Select_Rqst_Log_Dte_Tme, WcType.WITH) }
            );
            HISTOGRAM_WHO_RQST:
            while (condition(vw_cwf_Who_Work_Request.readNextRow("HISTOGRAM_WHO_RQST")))
            {
                //*  IF A WORK REQUEST RECORD EXISTS, AND THE MIT RECORD IS IN CLOSED
                //*  STATUS, THEN MARK THE RECORDS FOR DELETION BY WRITING THEM TO A FILE.
                //*  ---------------------------------------------------------------------
                if (condition(cwf_Master_Index_View_Select_Crprte_Status_Ind.equals("9")))                                                                                //Natural: IF CWF-MASTER-INDEX-VIEW-SELECT.CRPRTE-STATUS-IND = '9'
                {
                    pnd_Start_Master_Key_Pnd_Start_Rqst_Log_Dte_Tme.setValue(cwf_Master_Index_View_Select_Rqst_Log_Dte_Tme);                                              //Natural: ASSIGN #START-MASTER-KEY.#START-RQST-LOG-DTE-TME := CWF-MASTER-INDEX-VIEW-SELECT.RQST-LOG-DTE-TME
                    pnd_Start_Master_Key_Pnd_Start_Last_Chnge_Dte_Tme.reset();                                                                                            //Natural: RESET #START-MASTER-KEY.#START-LAST-CHNGE-DTE-TME
                    vw_cwf_Master_Index_View.startDatabaseRead                                                                                                            //Natural: READ CWF-MASTER-INDEX-VIEW BY RQST-ROUTING-KEY FROM #START-MASTER-KEY
                    (
                    "ACCESS_MASTER_EVENTS",
                    new Wc[] { new Wc("RQST_ROUTING_KEY", ">=", pnd_Start_Master_Key, WcType.BY) },
                    new Oc[] { new Oc("RQST_ROUTING_KEY", "ASC") }
                    );
                    ACCESS_MASTER_EVENTS:
                    while (condition(vw_cwf_Master_Index_View.readNextRow("ACCESS_MASTER_EVENTS")))
                    {
                        if (condition(cwf_Master_Index_View_Rqst_Log_Dte_Tme.notEquals(pnd_Start_Master_Key_Pnd_Start_Rqst_Log_Dte_Tme)))                                 //Natural: IF CWF-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME NE #START-MASTER-KEY.#START-RQST-LOG-DTE-TME
                        {
                            vw_cwf_Master_Index_View.getAstCOUNTER().nsubtract(1);                                                                                        //Natural: ASSIGN *COUNTER := *COUNTER - 1
                            if (true) break ACCESS_MASTER_EVENTS;                                                                                                         //Natural: ESCAPE BOTTOM ( ACCESS-MASTER-EVENTS. )
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Work_File_Record_Pnd_Work_File_Isn.setValue(vw_cwf_Master_Index_View.getAstISN("ACCESS_MASTER_EVENTS"));                                      //Natural: ASSIGN #WORK-FILE-ISN := *ISN ( ACCESS-MASTER-EVENTS. )
                        pnd_Work_File_Record_Pnd_Work_Log_Dte_Tme.setValue(cwf_Master_Index_View_Rqst_Log_Dte_Tme);                                                       //Natural: ASSIGN #WORK-LOG-DTE-TME := CWF-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME
                        getWorkFiles().write(1, false, pnd_Work_File_Record);                                                                                             //Natural: WRITE WORK FILE 1 #WORK-FILE-RECORD
                        pnd_Nbr_Of_Isn_Listed.nadd(1);                                                                                                                    //Natural: ADD 1 TO #NBR-OF-ISN-LISTED
                        //*  (ACCESS-MASTER-EVENTS.)
                    }                                                                                                                                                     //Natural: END-READ
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("HISTOGRAM_WHO_RQST"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("HISTOGRAM_WHO_RQST"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  INCREMENT THE NUMBER OF EVENTS TO BE PURGED.
                    //*  ----------------------------------------------------------------------
                    pnd_Nbr_Of_Events_Purged.nadd(vw_cwf_Master_Index_View.getAstCOUNTER());                                                                              //Natural: COMPUTE #NBR-OF-EVENTS-PURGED = #NBR-OF-EVENTS-PURGED + *COUNTER ( ACCESS-MASTER-EVENTS. )
                    pnd_Nbr_Of_Rqsts_Purged.nadd(1);                                                                                                                      //Natural: COMPUTE #NBR-OF-RQSTS-PURGED = #NBR-OF-RQSTS-PURGED + 1
                    //*  (2540)
                }                                                                                                                                                         //Natural: END-IF
                //*  (HISTOGRAM-WHO-RQST.)
            }                                                                                                                                                             //Natural: END-HISTOGRAM
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("SELECT_CLOSED_MIT"))) break;
                else if (condition(Global.isEscapeBottomImmediate("SELECT_CLOSED_MIT"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM TRANSACTION-ROUTINE
            sub_Transaction_Routine();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("SELECT_CLOSED_MIT"))) break;
                else if (condition(Global.isEscapeBottomImmediate("SELECT_CLOSED_MIT"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  (SELECT-CLOSED-MIT.)
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_End_Of_Window.setValue("Y");                                                                                                                                  //Natural: ASSIGN #END-OF-WINDOW := 'Y'
        //*  (2100)
    }
    private void sub_Transaction_Routine() throws Exception                                                                                                               //Natural: TRANSACTION-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ===================================
        //*  ACCUMULATE THE NUMBER OF RECORDS WHICH ARE PROCESSED AND COMMIT A
        //*  TRANSACTION WHEN THE STIPULATED LIMIT IS REACHED. UPDATE THE PURGE
        //*  STATISTICS ON THE RUN CONTROL RECORD.
        //*  ----------------------------------------------------------------------
        if (condition(pnd_Nbr_Of_Events_Purged.greaterOrEqual(pnd_No_Of_Records_Limit) || pnd_End_Of_Window.equals("Y")))                                                 //Natural: IF #NBR-OF-EVENTS-PURGED GE #NO-OF-RECORDS-LIMIT OR #END-OF-WINDOW = 'Y'
        {
            //*   UPDATE THE PURGE STATISTICS.
            //*  --------------------------------------------
            GET_TRANSACTION_UPDATE:                                                                                                                                       //Natural: GET CWF-WHO-SUPPORT-TBL MIHA0180-OUTPUT.RUN-CONTROL-ISN
            ldaMihl0180.getVw_cwf_Who_Support_Tbl().readByID(pdaMiha0180.getMiha0180_Output_Run_Control_Isn().getLong(), "GET_TRANSACTION_UPDATE");
            ldaMihl0180.getCwf_Who_Support_Tbl_Number_Of_Events_Purged().nadd(pnd_Nbr_Of_Events_Purged);                                                                  //Natural: COMPUTE CWF-WHO-SUPPORT-TBL.NUMBER-OF-EVENTS-PURGED = CWF-WHO-SUPPORT-TBL.NUMBER-OF-EVENTS-PURGED + #NBR-OF-EVENTS-PURGED
            ldaMihl0180.getCwf_Who_Support_Tbl_Number_Of_Work_Requests_Purged().nadd(pnd_Nbr_Of_Rqsts_Purged);                                                            //Natural: COMPUTE CWF-WHO-SUPPORT-TBL.NUMBER-OF-WORK-REQUESTS-PURGED = CWF-WHO-SUPPORT-TBL.NUMBER-OF-WORK-REQUESTS-PURGED + #NBR-OF-RQSTS-PURGED
            ldaMihl0180.getCwf_Who_Support_Tbl_Number_Of_Isn_Listed().nadd(pnd_Nbr_Of_Isn_Listed);                                                                        //Natural: COMPUTE CWF-WHO-SUPPORT-TBL.NUMBER-OF-ISN-LISTED = CWF-WHO-SUPPORT-TBL.NUMBER-OF-ISN-LISTED + #NBR-OF-ISN-LISTED
            ldaMihl0180.getVw_cwf_Who_Support_Tbl().updateDBRow("GET_TRANSACTION_UPDATE");                                                                                //Natural: UPDATE ( GET-TRANSACTION-UPDATE. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            pnd_Nbr_Of_Events_Purged.reset();                                                                                                                             //Natural: RESET #NBR-OF-EVENTS-PURGED #NBR-OF-RQSTS-PURGED #NBR-OF-ISN-LISTED
            pnd_Nbr_Of_Rqsts_Purged.reset();
            pnd_Nbr_Of_Isn_Listed.reset();
        }                                                                                                                                                                 //Natural: END-IF
        //*  (2970)
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=55 LS=132");
        Global.format(1, "PS=55 LS=132");
    }
}
