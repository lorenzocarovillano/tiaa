/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:32:21 PM
**        * FROM NATURAL PROGRAM : Cwfb4024
************************************************************
**        * FILE NAME            : Cwfb4024.java
**        * CLASS NAME           : Cwfb4024
**        * INSTANCE NAME        : Cwfb4024
************************************************************
************************************************************************
* PROGRAM  : CWFB4024
* SYSTEM   : CRPCWF
* TITLE    : SPECIAL BUSINESS PROCESS
* GENERATED: SEP 15,95 AT 11:34 AM
* FUNCTION : THIS PROGRAM READS THE CWF-WP-SBP-TBL AND WRITES
*            A WORK FILE TO BE DOWNLOADED TO THE SQL SERVER.
*
*
* HISTORY
**SAG DEFINE EXIT CHANGE-HISTORY
* CHANGED ON SEP 12,95 BY HARGRAV FOR RELEASE ____
* >
* >
* >
**SAG END-EXIT
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb4024 extends BLNatBase
{
    // Data Areas
    private LdaCdbatxa ldaCdbatxa;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Cur_Lang;
    private DbsField pnd_Program;

    private DataAccessProgramView vw_cwf_Wp_Sbp_Tbl;
    private DbsField cwf_Wp_Sbp_Tbl_Work_Prcss_Cde;

    private DbsGroup cwf_Wp_Sbp_Tbl__R_Field_1;
    private DbsField cwf_Wp_Sbp_Tbl_Mjr_Bsnss_Prcss;
    private DbsField cwf_Wp_Sbp_Tbl_Spcfc_Bsnss_Prcss_Cde;
    private DbsField cwf_Wp_Sbp_Tbl_Work_Prcss_Nme;
    private DbsField cwf_Wp_Sbp_Tbl_Actve_Ind;

    private DbsGroup cwf_Wp_Sbp_Tbl__R_Field_2;
    private DbsField cwf_Wp_Sbp_Tbl_Fill_1;
    private DbsField cwf_Wp_Sbp_Tbl_Actve_Ind_2_2;
    private DbsField cwf_Wp_Sbp_Tbl_Entry_Dte_Tme;
    private DbsField cwf_Wp_Sbp_Tbl_Entry_Oprtr_Cde;
    private DbsField cwf_Wp_Sbp_Tbl_Updte_Dte;
    private DbsField cwf_Wp_Sbp_Tbl_Updte_Dte_Tme;
    private DbsField cwf_Wp_Sbp_Tbl_Updte_Oprtr_Cde;
    private DbsField cwf_Wp_Sbp_Tbl_Dlte_Dte_Tme;
    private DbsField cwf_Wp_Sbp_Tbl_Dlte_Oprtr_Cde;
    private DbsField pnd_Header1_1;
    private DbsField pnd_Header1_2;

    private DbsGroup work_Record;
    private DbsField work_Record_Mjr_Bsnss_Prcss;
    private DbsField work_Record_Pnd_C1;
    private DbsField work_Record_Spcfc_Bsnss_Prcss_Cde;
    private DbsField work_Record_Pnd_C2;
    private DbsField work_Record_Work_Prcss_Nme;

    private DbsGroup pnd_Counters;
    private DbsField pnd_Counters_Pnd_Rex_Read;
    private DbsField pnd_Counters_Pnd_Rex_Written;

    private DbsGroup pnd_Report_Fields;
    private DbsField pnd_Report_Fields_Pnd_Error_Msg;

    private DbsGroup pnd_Contstants;
    private DbsField pnd_Contstants_Pnd_Delimiter;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCdbatxa = new LdaCdbatxa();
        registerRecord(ldaCdbatxa);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Cur_Lang = localVariables.newFieldInRecord("pnd_Cur_Lang", "#CUR-LANG", FieldType.PACKED_DECIMAL, 1);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);

        vw_cwf_Wp_Sbp_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Wp_Sbp_Tbl", "CWF-WP-SBP-TBL"), "CWF_WP_SBP_TBL", "CWF_PROFILE");
        cwf_Wp_Sbp_Tbl_Work_Prcss_Cde = vw_cwf_Wp_Sbp_Tbl.getRecord().newFieldInGroup("cwf_Wp_Sbp_Tbl_Work_Prcss_Cde", "WORK-PRCSS-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "WORK_PRCSS_CDE");
        cwf_Wp_Sbp_Tbl_Work_Prcss_Cde.setDdmHeader("WORK PROCESS/ID");

        cwf_Wp_Sbp_Tbl__R_Field_1 = vw_cwf_Wp_Sbp_Tbl.getRecord().newGroupInGroup("cwf_Wp_Sbp_Tbl__R_Field_1", "REDEFINE", cwf_Wp_Sbp_Tbl_Work_Prcss_Cde);
        cwf_Wp_Sbp_Tbl_Mjr_Bsnss_Prcss = cwf_Wp_Sbp_Tbl__R_Field_1.newFieldInGroup("cwf_Wp_Sbp_Tbl_Mjr_Bsnss_Prcss", "MJR-BSNSS-PRCSS", FieldType.STRING, 
            1);
        cwf_Wp_Sbp_Tbl_Spcfc_Bsnss_Prcss_Cde = cwf_Wp_Sbp_Tbl__R_Field_1.newFieldInGroup("cwf_Wp_Sbp_Tbl_Spcfc_Bsnss_Prcss_Cde", "SPCFC-BSNSS-PRCSS-CDE", 
            FieldType.STRING, 2);
        cwf_Wp_Sbp_Tbl_Work_Prcss_Nme = vw_cwf_Wp_Sbp_Tbl.getRecord().newFieldInGroup("cwf_Wp_Sbp_Tbl_Work_Prcss_Nme", "WORK-PRCSS-NME", FieldType.STRING, 
            45, RepeatingFieldStrategy.None, "WORK_PRCSS_NME");
        cwf_Wp_Sbp_Tbl_Work_Prcss_Nme.setDdmHeader("WORK TYPE LONG NAME");
        cwf_Wp_Sbp_Tbl_Actve_Ind = vw_cwf_Wp_Sbp_Tbl.getRecord().newFieldInGroup("cwf_Wp_Sbp_Tbl_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTVE_IND");

        cwf_Wp_Sbp_Tbl__R_Field_2 = vw_cwf_Wp_Sbp_Tbl.getRecord().newGroupInGroup("cwf_Wp_Sbp_Tbl__R_Field_2", "REDEFINE", cwf_Wp_Sbp_Tbl_Actve_Ind);
        cwf_Wp_Sbp_Tbl_Fill_1 = cwf_Wp_Sbp_Tbl__R_Field_2.newFieldInGroup("cwf_Wp_Sbp_Tbl_Fill_1", "FILL-1", FieldType.STRING, 1);
        cwf_Wp_Sbp_Tbl_Actve_Ind_2_2 = cwf_Wp_Sbp_Tbl__R_Field_2.newFieldInGroup("cwf_Wp_Sbp_Tbl_Actve_Ind_2_2", "ACTVE-IND-2-2", FieldType.STRING, 1);
        cwf_Wp_Sbp_Tbl_Entry_Dte_Tme = vw_cwf_Wp_Sbp_Tbl.getRecord().newFieldInGroup("cwf_Wp_Sbp_Tbl_Entry_Dte_Tme", "ENTRY-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ENTRY_DTE_TME");
        cwf_Wp_Sbp_Tbl_Entry_Dte_Tme.setDdmHeader("ENTRY/DT-TM");
        cwf_Wp_Sbp_Tbl_Entry_Oprtr_Cde = vw_cwf_Wp_Sbp_Tbl.getRecord().newFieldInGroup("cwf_Wp_Sbp_Tbl_Entry_Oprtr_Cde", "ENTRY-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ENTRY_OPRTR_CDE");
        cwf_Wp_Sbp_Tbl_Entry_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Wp_Sbp_Tbl_Updte_Dte = vw_cwf_Wp_Sbp_Tbl.getRecord().newFieldInGroup("cwf_Wp_Sbp_Tbl_Updte_Dte", "UPDTE-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "UPDTE_DTE");
        cwf_Wp_Sbp_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        cwf_Wp_Sbp_Tbl_Updte_Dte_Tme = vw_cwf_Wp_Sbp_Tbl.getRecord().newFieldInGroup("cwf_Wp_Sbp_Tbl_Updte_Dte_Tme", "UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "UPDTE_DTE_TME");
        cwf_Wp_Sbp_Tbl_Updte_Oprtr_Cde = vw_cwf_Wp_Sbp_Tbl.getRecord().newFieldInGroup("cwf_Wp_Sbp_Tbl_Updte_Oprtr_Cde", "UPDTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "UPDTE_OPRTR_CDE");
        cwf_Wp_Sbp_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPER");
        cwf_Wp_Sbp_Tbl_Dlte_Dte_Tme = vw_cwf_Wp_Sbp_Tbl.getRecord().newFieldInGroup("cwf_Wp_Sbp_Tbl_Dlte_Dte_Tme", "DLTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "DLTE_DTE_TME");
        cwf_Wp_Sbp_Tbl_Dlte_Dte_Tme.setDdmHeader("DELETE/DT-TM");
        cwf_Wp_Sbp_Tbl_Dlte_Oprtr_Cde = vw_cwf_Wp_Sbp_Tbl.getRecord().newFieldInGroup("cwf_Wp_Sbp_Tbl_Dlte_Oprtr_Cde", "DLTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "DLTE_OPRTR_CDE");
        cwf_Wp_Sbp_Tbl_Dlte_Oprtr_Cde.setDdmHeader("DELETE/OPER");
        registerRecord(vw_cwf_Wp_Sbp_Tbl);

        pnd_Header1_1 = localVariables.newFieldInRecord("pnd_Header1_1", "#HEADER1-1", FieldType.STRING, 50);
        pnd_Header1_2 = localVariables.newFieldInRecord("pnd_Header1_2", "#HEADER1-2", FieldType.STRING, 50);

        work_Record = localVariables.newGroupInRecord("work_Record", "WORK-RECORD");
        work_Record_Mjr_Bsnss_Prcss = work_Record.newFieldInGroup("work_Record_Mjr_Bsnss_Prcss", "MJR-BSNSS-PRCSS", FieldType.STRING, 1);
        work_Record_Pnd_C1 = work_Record.newFieldInGroup("work_Record_Pnd_C1", "#C1", FieldType.STRING, 1);
        work_Record_Spcfc_Bsnss_Prcss_Cde = work_Record.newFieldInGroup("work_Record_Spcfc_Bsnss_Prcss_Cde", "SPCFC-BSNSS-PRCSS-CDE", FieldType.STRING, 
            2);
        work_Record_Pnd_C2 = work_Record.newFieldInGroup("work_Record_Pnd_C2", "#C2", FieldType.STRING, 1);
        work_Record_Work_Prcss_Nme = work_Record.newFieldInGroup("work_Record_Work_Prcss_Nme", "WORK-PRCSS-NME", FieldType.STRING, 45);

        pnd_Counters = localVariables.newGroupInRecord("pnd_Counters", "#COUNTERS");
        pnd_Counters_Pnd_Rex_Read = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Rex_Read", "#REX-READ", FieldType.PACKED_DECIMAL, 5);
        pnd_Counters_Pnd_Rex_Written = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Rex_Written", "#REX-WRITTEN", FieldType.PACKED_DECIMAL, 5);

        pnd_Report_Fields = localVariables.newGroupInRecord("pnd_Report_Fields", "#REPORT-FIELDS");
        pnd_Report_Fields_Pnd_Error_Msg = pnd_Report_Fields.newFieldInGroup("pnd_Report_Fields_Pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 60);

        pnd_Contstants = localVariables.newGroupInRecord("pnd_Contstants", "#CONTSTANTS");
        pnd_Contstants_Pnd_Delimiter = pnd_Contstants.newFieldInGroup("pnd_Contstants_Pnd_Delimiter", "#DELIMITER", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Wp_Sbp_Tbl.reset();

        ldaCdbatxa.initializeValues();

        localVariables.reset();
        pnd_Header1_1.setInitialValue(" CWF Special Business Proc. Codes Extract Report");
        pnd_Header1_2.setInitialValue("                        �");
        pnd_Contstants_Pnd_Delimiter.setInitialValue("@");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb4024() throws Exception
    {
        super("Cwfb4024");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  DEFINE PRINTERS AND FORMATS
        getReports().definePrinter(2, "NOT DEFINED");                                                                                                                     //Natural: DEFINE PRINTER ( 1 )
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: ASSIGN #PROGRAM = *PROGRAM
        //*  MAP THE CURRENT LANGUAGE CODE TO THE CODE IN THE BATCH MODEL TEXT LDA.
        pnd_Cur_Lang.setValue(Global.getLANGUAGE());                                                                                                                      //Natural: ASSIGN #CUR-LANG = *LANGUAGE
        pnd_Cur_Lang.setValue(ldaCdbatxa.getCdbatxa_Pnd_Lang_Map().getValue(pnd_Cur_Lang));                                                                               //Natural: ASSIGN #CUR-LANG = CDBATXA.#LANG-MAP ( #CUR-LANG )
        //* *SAG DEFINE EXIT START-OF-PROGRAM
        //* *SAG END-EXIT
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH' THEN
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA = 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        //* *SAG DEFINE EXIT BEFORE-READ
        //* *SAG END-EXIT
        //*  PRIMARY FILE
        vw_cwf_Wp_Sbp_Tbl.startDatabaseRead                                                                                                                               //Natural: READ CWF-WP-SBP-TBL BY WORK-PRCSS-KEY
        (
        "READ_PRIME",
        new Oc[] { new Oc("WORK_PRCSS_KEY", "ASC") }
        );
        READ_PRIME:
        while (condition(vw_cwf_Wp_Sbp_Tbl.readNextRow("READ_PRIME")))
        {
            //* *SAG DEFINE EXIT PRIME-WRITE-FIELDS
            if (condition(cwf_Wp_Sbp_Tbl_Dlte_Oprtr_Cde.greater(" ")))                                                                                                    //Natural: REJECT IF DLTE-OPRTR-CDE GT ' '
            {
                continue;
            }
            pnd_Counters_Pnd_Rex_Read.nadd(1);                                                                                                                            //Natural: ADD 1 TO #REX-READ
                                                                                                                                                                          //Natural: PERFORM FORMAT-RECORD
            sub_Format_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-FILE
            sub_Write_Work_File();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* *SAG END-EXIT
            //*  PRIMARY FILE.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* *SAG DEFINE EXIT END-OF-PROGRAM
        getReports().write(1, ReportOption.NOTITLE,"Total Number of Special Business Proc. Table Records Read:",pnd_Counters_Pnd_Rex_Read,NEWLINE,"Total Number of Work File Records Written                :",pnd_Counters_Pnd_Rex_Written,NEWLINE,NEWLINE,new  //Natural: WRITE ( 1 ) 'Total Number of Special Business Proc. Table Records Read:' #REX-READ / 'Total Number of Work File Records Written                :' #REX-WRITTEN //56T 'End of Report'
            TabSetting(56),"End of Report");
        if (Global.isEscape()) return;
        //* *SAG END-EXIT
        //* *SAG DEFINE EXIT MISCELLANEOUS-SUBROUTINES
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-RECORD
        //* *************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WORK-FILE
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT
        //* *****************************
        //* *************
        //* *SAG END-EXIT
    }
    private void sub_Format_Record() throws Exception                                                                                                                     //Natural: FORMAT-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        work_Record.reset();                                                                                                                                              //Natural: RESET WORK-RECORD
        work_Record.setValuesByName(vw_cwf_Wp_Sbp_Tbl);                                                                                                                   //Natural: MOVE BY NAME CWF-WP-SBP-TBL TO WORK-RECORD
        work_Record_Pnd_C1.setValue(pnd_Contstants_Pnd_Delimiter);                                                                                                        //Natural: MOVE #DELIMITER TO #C1 #C2
        work_Record_Pnd_C2.setValue(pnd_Contstants_Pnd_Delimiter);
        //* *************
        //*  FORMAT-RECORD
    }
    private void sub_Write_Work_File() throws Exception                                                                                                                   //Natural: WRITE-WORK-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getWorkFiles().write(1, false, work_Record);                                                                                                                      //Natural: WRITE WORK FILE 1 WORK-RECORD
        pnd_Counters_Pnd_Rex_Written.nadd(1);                                                                                                                             //Natural: ADD 1 TO #REX-WRITTEN
        //*  PERFORM WRITE-REPORT
        //* ********************************
        //*  WRITE-WORK-FILE
    }
    private void sub_Write_Report() throws Exception                                                                                                                      //Natural: WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(1, "Major/Bsnss Cde",                                                                                                                        //Natural: DISPLAY ( 1 ) 'Major/Bsnss Cde' CWF-WP-SBP-TBL.MJR-BSNSS-PRCSS 'Specific/Bsnss Cde' CWF-WP-SBP-TBL.SPCFC-BSNSS-PRCSS-CDE '/Name' CWF-WP-SBP-TBL.WORK-PRCSS-NME
        		cwf_Wp_Sbp_Tbl_Mjr_Bsnss_Prcss,"Specific/Bsnss Cde",
        		cwf_Wp_Sbp_Tbl_Spcfc_Bsnss_Prcss_Cde,"/Name",
        		cwf_Wp_Sbp_Tbl_Work_Prcss_Nme);
        if (Global.isEscape()) return;
        //* *************
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(41),pnd_Header1_1,new TabSetting(124),ldaCdbatxa.getCdbatxa_Pnd_Page_Txt().getValue(pnd_Cur_Lang),getReports().getPageNumberDbs(1),  //Natural: WRITE ( 1 ) NOTITLE *PROGRAM 41T #HEADER1-1 124T CDBATXA.#PAGE-TXT ( #CUR-LANG ) *PAGE-NUMBER ( 1 ) ( NL = 4 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) #HEADER1-2 124T *TIMX ( EM = HH':'II' 'AP )
                        new NumericLength (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),pnd_Header1_2,new 
                        TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"));
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");

        getReports().setDisplayColumns(1, "Major/Bsnss Cde",
        		cwf_Wp_Sbp_Tbl_Mjr_Bsnss_Prcss,"Specific/Bsnss Cde",
        		cwf_Wp_Sbp_Tbl_Spcfc_Bsnss_Prcss_Cde,"/Name",
        		cwf_Wp_Sbp_Tbl_Work_Prcss_Nme);
    }
}
