/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:47:01 PM
**        * FROM NATURAL PROGRAM : Cwfb9125
************************************************************
**        * FILE NAME            : Cwfb9125.java
**        * CLASS NAME           : Cwfb9125
**        * INSTANCE NAME        : Cwfb9125
************************************************************
************************************************************************
* SYSTEM   : CORPORATE WORKFLOW
* PROGRAM  : CWFB9125
* TITLE    : IA INTERFACE FOR BERWYN REPORT AUTOMATION
* FUNCTION : READ CWF-MASTER-INDEX-VIEW (045/181); SELECT REQUESTS FOR
*          : NO-IA-PAYMENT-WPID's with request date GE the LAST-RUN-DATE
*          : ON BERWYN-MIT-LOG-DATE TABLE; GET DOD FROM ELECTRONIC
*          : FOLDER DOCUMENT, SSN FROM COR; CALL IAAN9125 TO STOP
*          : PAYMENTS TO PARTICIPANTS FOLLOWING DEATH OF BENEFICIARY;
*          : WRITE TWO REPORTS (SUCCESSFUL AND UNSUCCESSFUL STOP
*          : PAYMENTS); UPDATE STATUS AND CLOSE REQUESTS FOR SUCCESSFUL
*          : STOP PAYMENTS.
* HISTORY  :
*          : 05/2011 DON MEADE
*          : REVISIONS:
*          : 07/06/11 DM: PASS ALPHANUMERIC SSN TO IAAN9125
*                         READ MIT BY UNIT-WPID KEY
*          : 07/06/11 GH: RETRIEVE THE DECEASED's SSN from Note
*          : 07/20/11 DM: CHANGE REPORT HEADINGS TO DELETE "Contract"
*                         AS CONTRACT NUMBER IS NOT ON MIT RECORD
*          : 07/28/11 DM: START READ OF CWF WITH 'RAID' NOT 'RAIDA'
*          : 08/15/11 DM: ADDITIONAL BERWYN WPID's added. Expanded
*                         #NO-IA-PAYMENT-WPID AREA AND STARTED
*                         WPID READ FROM 'A     '
*          : 08/16/11 DM: ADDITIONAL UNIT ACTPS ADDED.
*                         CHECK IF DOD FITS FORMAT - CATCH BAD EFM DOCS
*                         MAKE SSN PARM PASSED TO IAAN9125 NUMERIC.
*          : 10/05/11 GH  REMOVE CLOSED WORK REQUESTS OUT FROM WORKLIST.
*                         CLOSE WORK REQUEST IN RSSMP.
*          : 08/30/13 AB  ADDED LOGIC TO PASS MATCH-INDICATOR TO
*                         PGM IAAN9125 FOR AUTOMATIC PROCESS FOR IA
*          : 08/10/15 JB  COR/NAAD RETIREMENT PROJECT
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb9125 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_mit;
    private DbsField mit_Pin_Nbr;
    private DbsField mit_Actve_Ind;
    private DbsField mit_Rqst_Log_Dte_Tme;
    private DbsField mit_Rqst_Log_Invrt_Dte_Tme;
    private DbsField mit_Work_Prcss_Id;
    private DbsField mit_Admin_Unit_Cde;
    private DbsGroup mit_Cntrct_NbrMuGroup;
    private DbsField mit_Cntrct_Nbr;
    private DbsField mit_Last_Chnge_Dte_Tme;
    private DbsField mit_Tiaa_Rcvd_Dte_Tme;
    private DbsField mit_Crprte_Due_Dte_Tme;
    private DbsField mit_Status_Cde;
    private DbsField mit_Status_Updte_Dte_Tme;
    private DbsField mit_Status_Updte_Oprtr_Cde;
    private DbsField mit_Admin_Status_Cde;
    private DbsField mit_Admin_Status_Updte_Dte_Tme;
    private DbsField mit_Admin_Status_Updte_Oprtr_Cde;
    private DbsField mit_Crprte_Status_Ind;
    private DbsField mit_Tiaa_Rcvd_Dte;
    private DbsField mit_Case_Id_Cde;

    private DbsGroup mit__R_Field_1;
    private DbsField mit_Case_Ind;
    private DbsField mit_Sub_Rqst_Sqnce_Ind;
    private DbsField mit_Last_Updte_Dte_Tme;
    private DbsField mit_Last_Updte_Oprtr_Cde;
    private DbsField mit_Work_List_Ind;
    private DbsField pnd_Unit_Wpid_Key;

    private DbsGroup pnd_Unit_Wpid_Key__R_Field_2;
    private DbsField pnd_Unit_Wpid_Key_Pnd_Key_Open;
    private DbsField pnd_Unit_Wpid_Key_Pnd_Key_Unit;
    private DbsField pnd_Unit_Wpid_Key_Pnd_Key_Wpid;
    private DbsField pnd_Mit_Rqst_Log_Dte_Tme;

    private DbsGroup pnd_Mit_Rqst_Log_Dte_Tme__R_Field_3;
    private DbsField pnd_Mit_Rqst_Log_Dte_Tme_Pnd_Run_Yyyymmdd;
    private DbsField pnd_Mit_Rqst_Log_Dte_Tme_Pnd_Run_0;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_4;
    private DbsField cwf_Support_Tbl_Tbl_No_Ia_Payment_Wps;

    private DbsGroup cwf_Support_Tbl__R_Field_5;
    private DbsField cwf_Support_Tbl_Tbl_Last_Run_Date;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_6;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field;

    private DataAccessProgramView vw_cwf_Doc;
    private DbsField cwf_Doc_Cabinet_Id;
    private DbsField cwf_Doc_Tiaa_Rcvd_Dte;
    private DbsField cwf_Doc_Case_Workprcss_Multi_Subrqst;

    private DbsGroup cwf_Doc_Document_Type;
    private DbsField cwf_Doc_Document_Category;
    private DbsField cwf_Doc_Document_Sub_Category;
    private DbsField cwf_Doc_Document_Detail;
    private DbsField cwf_Doc_Document_Direction_Ind;
    private DbsField cwf_Doc_Secured_Document_Ind;
    private DbsField cwf_Doc_Document_Subtype;
    private DbsField cwf_Doc_Document_Retention_Ind;
    private DbsField cwf_Doc_Entry_System_Or_Unit;
    private DbsField cwf_Doc_Entry_Dte_Tme;
    private DbsField cwf_Doc_Text_Pointer;
    private DbsField pnd_Document_Key;

    private DbsGroup pnd_Document_Key__R_Field_7;
    private DbsField pnd_Document_Key_Pnd_Doc_Cabinet_Id;

    private DbsGroup pnd_Document_Key__R_Field_8;
    private DbsField pnd_Document_Key_Pnd_Doc_Cabinet_Type;
    private DbsField pnd_Document_Key_Pnd_Doc_Cabinet_Name;

    private DbsGroup pnd_Document_Key__R_Field_9;
    private DbsField pnd_Document_Key_Pnd_Doc_Cabinet_Name_A12;
    private DbsField pnd_Document_Key_Pnd_Doc_Tiaa_Rcvd_Dte;
    private DbsField pnd_Document_Key_Pnd_Case_Workprcss_Multi_Subrqst;

    private DbsGroup pnd_Document_Key__R_Field_10;
    private DbsField pnd_Document_Key_Pnd_Doc_Case_Ind;
    private DbsField pnd_Document_Key_Pnd_Doc_Originating_Work_Prcss_Id;
    private DbsField pnd_Document_Key_Pnd_Doc_Multi_Rqst_Ind;
    private DbsField pnd_Document_Key_Pnd_Doc_Sub_Rqst_Ind;
    private DbsField pnd_Document_Key_Pnd_Doc_Entry_Dte_Tme;

    private DataAccessProgramView vw_cwf_Text;
    private DbsField cwf_Text_Cabinet_Id;
    private DbsField cwf_Text_Text_Pointer;
    private DbsField cwf_Text_Text_Sqnce_Nbr;
    private DbsField cwf_Text_Last_Text_Lines_Group_Ind;
    private DbsField cwf_Text_Count_Casttext_Document_Line_Group;

    private DbsGroup cwf_Text_Text_Document_Line_Group;
    private DbsField cwf_Text_Text_Document_Line;
    private DbsField pnd_Text_Object_Key;

    private DbsGroup pnd_Text_Object_Key__R_Field_11;
    private DbsField pnd_Text_Object_Key_Pnd_Txt_Cabinet_Id;
    private DbsField pnd_Text_Object_Key_Pnd_Txt_Text_Pointer;
    private DbsField pnd_Text_Object_Key_Pnd_Txt_Text_Sqnce_Nbr;
    private DbsField pnd_Last_Run_Date;
    private DbsField pnd_No_Ia_Payment_Wpids;

    private DbsGroup pnd_No_Ia_Payment_Wpids__R_Field_12;
    private DbsField pnd_No_Ia_Payment_Wpids_Pnd_No_Ia_Payment_Wpid;
    private DbsField pnd_X;
    private DbsField pnd_Isn;
    private DbsField pnd_Wpid_Hit;
    private DbsField pnd_Successful_Cnt;
    private DbsField pnd_Error_Rpt_Cnt;
    private DbsField pnd_Et_Cntr;
    private DbsField pnd_Text_Dod;

    private DbsGroup pnd_Text_Dod__R_Field_13;
    private DbsField pnd_Text_Dod_Pnd_Dod_Mm;
    private DbsField pnd_Text_Dod_Pnd_Dod_Fil1;
    private DbsField pnd_Text_Dod_Pnd_Dod_Dd;
    private DbsField pnd_Text_Dod_Pnd_Dod_Fil2;
    private DbsField pnd_Text_Dod_Pnd_Dod_Yyyy;
    private DbsField pnd_Header1_1;
    private DbsField pnd_Header1_2;
    private DbsField pnd_Header1_3;
    private DbsField pnd_Header2_1;
    private DbsField pnd_Header2_2;
    private DbsField pnd_Header2_3;
    private DbsField pnd_Successful_Report_Line;

    private DbsGroup pnd_Successful_Report_Line__R_Field_14;
    private DbsField pnd_Successful_Report_Line_Pnd_Stop_Pay_Pin;
    private DbsField pnd_Error_Report_Line;

    private DbsGroup pnd_Error_Report_Line__R_Field_15;
    private DbsField pnd_Error_Report_Line_Pnd_Error_Pin;
    private DbsField pnd_Error_Report_Line_Pnd_Filler;
    private DbsField pnd_Error_Report_Line_Pnd_Error_Rc;
    private DbsField pnd_Error_Report_Line_Pnd_Filler2;
    private DbsField pnd_Error_Report_Line_Pnd_Error_Message;
    private DbsField pnd_Nat_Err_Msg;

    private DbsGroup pnd_Iaan9125_Interface;
    private DbsField pnd_Iaan9125_Interface_Pnd_Iaan9125_Pin;
    private DbsField pnd_Iaan9125_Interface_Pnd_Iaan9125_Ssn;

    private DbsGroup pnd_Iaan9125_Interface__R_Field_16;
    private DbsField pnd_Iaan9125_Interface_Pnd_Iaan9125_Ssn_Num;
    private DbsField pnd_Iaan9125_Interface_Pnd_Iaan9125_Dod;

    private DbsGroup pnd_Iaan9125_Interface__R_Field_17;
    private DbsField pnd_Iaan9125_Interface_Pnd_Iaan9125_Dod_Yyyy;
    private DbsField pnd_Iaan9125_Interface_Pnd_Iaan9125_Dod_Mm;
    private DbsField pnd_Iaan9125_Interface_Pnd_Iaan9125_Dod_Dd;
    private DbsField pnd_Iaan9125_Interface_Pnd_Iaan9125_Rc;
    private DbsField pnd_Iaan9125_Interface_Pnd_Iaan9125_Msg;
    private DbsField pnd_Iaan9125_Interface_Pnd_Iaan9125_Match_Ind;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        // Local Variables
        localVariables = new DbsRecord();

        vw_mit = new DataAccessProgramView(new NameInfo("vw_mit", "MIT"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        mit_Pin_Nbr = vw_mit.getRecord().newFieldInGroup("mit_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "PIN_NBR");
        mit_Pin_Nbr.setDdmHeader("PIN");
        mit_Actve_Ind = vw_mit.getRecord().newFieldInGroup("mit_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, "ACTVE_IND");
        mit_Rqst_Log_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "RQST_LOG_DTE_TME");
        mit_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        mit_Rqst_Log_Invrt_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Rqst_Log_Invrt_Dte_Tme", "RQST-LOG-INVRT-DTE-TME", FieldType.NUMERIC, 15, 
            RepeatingFieldStrategy.None, "RQST_LOG_INVRT_DTE_TME");
        mit_Work_Prcss_Id = vw_mit.getRecord().newFieldInGroup("mit_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        mit_Work_Prcss_Id.setDdmHeader("WORK/ID");
        mit_Admin_Unit_Cde = vw_mit.getRecord().newFieldInGroup("mit_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ADMIN_UNIT_CDE");
        mit_Cntrct_NbrMuGroup = vw_mit.getRecord().newGroupInGroup("MIT_CNTRCT_NBRMuGroup", "CNTRCT_NBRMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "CWF_MASTER_INDEX_CNTRCT_NBR");
        mit_Cntrct_Nbr = mit_Cntrct_NbrMuGroup.newFieldArrayInGroup("mit_Cntrct_Nbr", "CNTRCT-NBR", FieldType.STRING, 8, new DbsArrayController(1, 1), 
            RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_NBR");
        mit_Last_Chnge_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, 
            "LAST_CHNGE_DTE_TME");
        mit_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");
        mit_Tiaa_Rcvd_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Tiaa_Rcvd_Dte_Tme", "TIAA-RCVD-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TIAA_RCVD_DTE_TME");
        mit_Tiaa_Rcvd_Dte_Tme.setDdmHeader("TIAA-RCV/DTE-TME");
        mit_Crprte_Due_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Crprte_Due_Dte_Tme", "CRPRTE-DUE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "CRPRTE_DUE_DTE_TME");
        mit_Crprte_Due_Dte_Tme.setDdmHeader("WORKRQST/DUE DATE");
        mit_Status_Cde = vw_mit.getRecord().newFieldInGroup("mit_Status_Cde", "STATUS-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, "STATUS_CDE");
        mit_Status_Updte_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Status_Updte_Dte_Tme", "STATUS-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "STATUS_UPDTE_DTE_TME");
        mit_Status_Updte_Dte_Tme.setDdmHeader("STATUS UPDATE/DATE-TIME");
        mit_Status_Updte_Oprtr_Cde = vw_mit.getRecord().newFieldInGroup("mit_Status_Updte_Oprtr_Cde", "STATUS-UPDTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "STATUS_UPDTE_OPRTR_CDE");
        mit_Admin_Status_Cde = vw_mit.getRecord().newFieldInGroup("mit_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "ADMIN_STATUS_CDE");
        mit_Admin_Status_Updte_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Admin_Status_Updte_Dte_Tme", "ADMIN-STATUS-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_DTE_TME");
        mit_Admin_Status_Updte_Oprtr_Cde = vw_mit.getRecord().newFieldInGroup("mit_Admin_Status_Updte_Oprtr_Cde", "ADMIN-STATUS-UPDTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_OPRTR_CDE");
        mit_Crprte_Status_Ind = vw_mit.getRecord().newFieldInGroup("mit_Crprte_Status_Ind", "CRPRTE-STATUS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CRPRTE_STATUS_IND");
        mit_Crprte_Status_Ind.setDdmHeader("CORP/STAT");
        mit_Tiaa_Rcvd_Dte = vw_mit.getRecord().newFieldInGroup("mit_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_RCVD_DTE");
        mit_Case_Id_Cde = vw_mit.getRecord().newFieldInGroup("mit_Case_Id_Cde", "CASE-ID-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "CASE_ID_CDE");
        mit_Case_Id_Cde.setDdmHeader("CASE/ID");

        mit__R_Field_1 = vw_mit.getRecord().newGroupInGroup("mit__R_Field_1", "REDEFINE", mit_Case_Id_Cde);
        mit_Case_Ind = mit__R_Field_1.newFieldInGroup("mit_Case_Ind", "CASE-IND", FieldType.STRING, 1);
        mit_Sub_Rqst_Sqnce_Ind = mit__R_Field_1.newFieldInGroup("mit_Sub_Rqst_Sqnce_Ind", "SUB-RQST-SQNCE-IND", FieldType.STRING, 1);
        mit_Last_Updte_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Last_Updte_Dte_Tme", "LAST-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LAST_UPDTE_DTE_TME");
        mit_Last_Updte_Oprtr_Cde = vw_mit.getRecord().newFieldInGroup("mit_Last_Updte_Oprtr_Cde", "LAST-UPDTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "LAST_UPDTE_OPRTR_CDE");
        mit_Work_List_Ind = vw_mit.getRecord().newFieldInGroup("mit_Work_List_Ind", "WORK-LIST-IND", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "WORK_LIST_IND");
        mit_Work_List_Ind.setDdmHeader("WORK LIST INDICATOR");
        registerRecord(vw_mit);

        pnd_Unit_Wpid_Key = localVariables.newFieldInRecord("pnd_Unit_Wpid_Key", "#UNIT-WPID-KEY", FieldType.STRING, 15);

        pnd_Unit_Wpid_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Unit_Wpid_Key__R_Field_2", "REDEFINE", pnd_Unit_Wpid_Key);
        pnd_Unit_Wpid_Key_Pnd_Key_Open = pnd_Unit_Wpid_Key__R_Field_2.newFieldInGroup("pnd_Unit_Wpid_Key_Pnd_Key_Open", "#KEY-OPEN", FieldType.STRING, 
            1);
        pnd_Unit_Wpid_Key_Pnd_Key_Unit = pnd_Unit_Wpid_Key__R_Field_2.newFieldInGroup("pnd_Unit_Wpid_Key_Pnd_Key_Unit", "#KEY-UNIT", FieldType.STRING, 
            8);
        pnd_Unit_Wpid_Key_Pnd_Key_Wpid = pnd_Unit_Wpid_Key__R_Field_2.newFieldInGroup("pnd_Unit_Wpid_Key_Pnd_Key_Wpid", "#KEY-WPID", FieldType.STRING, 
            6);
        pnd_Mit_Rqst_Log_Dte_Tme = localVariables.newFieldInRecord("pnd_Mit_Rqst_Log_Dte_Tme", "#MIT-RQST-LOG-DTE-TME", FieldType.STRING, 15);

        pnd_Mit_Rqst_Log_Dte_Tme__R_Field_3 = localVariables.newGroupInRecord("pnd_Mit_Rqst_Log_Dte_Tme__R_Field_3", "REDEFINE", pnd_Mit_Rqst_Log_Dte_Tme);
        pnd_Mit_Rqst_Log_Dte_Tme_Pnd_Run_Yyyymmdd = pnd_Mit_Rqst_Log_Dte_Tme__R_Field_3.newFieldInGroup("pnd_Mit_Rqst_Log_Dte_Tme_Pnd_Run_Yyyymmdd", "#RUN-YYYYMMDD", 
            FieldType.STRING, 8);
        pnd_Mit_Rqst_Log_Dte_Tme_Pnd_Run_0 = pnd_Mit_Rqst_Log_Dte_Tme__R_Field_3.newFieldInGroup("pnd_Mit_Rqst_Log_Dte_Tme_Pnd_Run_0", "#RUN-0", FieldType.STRING, 
            7);

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Support_Tbl__R_Field_4 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_4", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Tbl_No_Ia_Payment_Wps = cwf_Support_Tbl__R_Field_4.newFieldInGroup("cwf_Support_Tbl_Tbl_No_Ia_Payment_Wps", "TBL-NO-IA-PAYMENT-WPS", 
            FieldType.STRING, 120);

        cwf_Support_Tbl__R_Field_5 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_5", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Tbl_Last_Run_Date = cwf_Support_Tbl__R_Field_5.newFieldInGroup("cwf_Support_Tbl_Tbl_Last_Run_Date", "TBL-LAST-RUN-DATE", FieldType.NUMERIC, 
            8);
        registerRecord(vw_cwf_Support_Tbl);

        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_6 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_6", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);

        vw_cwf_Doc = new DataAccessProgramView(new NameInfo("vw_cwf_Doc", "CWF-DOC"), "CWF_EFM_DOCUMENT", "CWF_EFM_DOCUMENT");
        cwf_Doc_Cabinet_Id = vw_cwf_Doc.getRecord().newFieldInGroup("cwf_Doc_Cabinet_Id", "CABINET-ID", FieldType.STRING, 14, RepeatingFieldStrategy.None, 
            "CABINET_ID");
        cwf_Doc_Cabinet_Id.setDdmHeader("CABINET/ID");
        cwf_Doc_Tiaa_Rcvd_Dte = vw_cwf_Doc.getRecord().newFieldInGroup("cwf_Doc_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIAA_RCVD_DTE");
        cwf_Doc_Tiaa_Rcvd_Dte.setDdmHeader("TIAA RECVD/DATE");
        cwf_Doc_Case_Workprcss_Multi_Subrqst = vw_cwf_Doc.getRecord().newFieldInGroup("cwf_Doc_Case_Workprcss_Multi_Subrqst", "CASE-WORKPRCSS-MULTI-SUBRQST", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CASE_WORKPRCSS_MULTI_SUBRQST");
        cwf_Doc_Case_Workprcss_Multi_Subrqst.setDdmHeader("CASE/WPID/MULTI/SUB-REQ");

        cwf_Doc_Document_Type = vw_cwf_Doc.getRecord().newGroupInGroup("CWF_DOC_DOCUMENT_TYPE", "DOCUMENT-TYPE");
        cwf_Doc_Document_Type.setDdmHeader("DOCUMENT/TYPE");
        cwf_Doc_Document_Category = cwf_Doc_Document_Type.newFieldInGroup("cwf_Doc_Document_Category", "DOCUMENT-CATEGORY", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "DOCUMENT_CATEGORY");
        cwf_Doc_Document_Category.setDdmHeader("DOC/CAT");
        cwf_Doc_Document_Sub_Category = cwf_Doc_Document_Type.newFieldInGroup("cwf_Doc_Document_Sub_Category", "DOCUMENT-SUB-CATEGORY", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "DOCUMENT_SUB_CATEGORY");
        cwf_Doc_Document_Sub_Category.setDdmHeader("DOC SUB-/CATEGORY");
        cwf_Doc_Document_Detail = cwf_Doc_Document_Type.newFieldInGroup("cwf_Doc_Document_Detail", "DOCUMENT-DETAIL", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "DOCUMENT_DETAIL");
        cwf_Doc_Document_Detail.setDdmHeader("DOCUMENT/DETAIL");
        cwf_Doc_Document_Direction_Ind = vw_cwf_Doc.getRecord().newFieldInGroup("cwf_Doc_Document_Direction_Ind", "DOCUMENT-DIRECTION-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DOCUMENT_DIRECTION_IND");
        cwf_Doc_Document_Direction_Ind.setDdmHeader("DOC/DIR");
        cwf_Doc_Secured_Document_Ind = vw_cwf_Doc.getRecord().newFieldInGroup("cwf_Doc_Secured_Document_Ind", "SECURED-DOCUMENT-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "SECURED_DOCUMENT_IND");
        cwf_Doc_Secured_Document_Ind.setDdmHeader("SECRD/DOC");
        cwf_Doc_Document_Subtype = vw_cwf_Doc.getRecord().newFieldInGroup("cwf_Doc_Document_Subtype", "DOCUMENT-SUBTYPE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "DOCUMENT_SUBTYPE");
        cwf_Doc_Document_Subtype.setDdmHeader("DOC/SUBTP");
        cwf_Doc_Document_Retention_Ind = vw_cwf_Doc.getRecord().newFieldInGroup("cwf_Doc_Document_Retention_Ind", "DOCUMENT-RETENTION-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DOCUMENT_RETENTION_IND");
        cwf_Doc_Document_Retention_Ind.setDdmHeader("DOC/RETNTN");
        cwf_Doc_Entry_System_Or_Unit = vw_cwf_Doc.getRecord().newFieldInGroup("cwf_Doc_Entry_System_Or_Unit", "ENTRY-SYSTEM-OR-UNIT", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ENTRY_SYSTEM_OR_UNIT");
        cwf_Doc_Entry_System_Or_Unit.setDdmHeader("CREATED BY/SYSTEM OR UNIT");
        cwf_Doc_Entry_Dte_Tme = vw_cwf_Doc.getRecord().newFieldInGroup("cwf_Doc_Entry_Dte_Tme", "ENTRY-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "ENTRY_DTE_TME");
        cwf_Doc_Entry_Dte_Tme.setDdmHeader("ENTRY/DATE AND TIME");
        cwf_Doc_Text_Pointer = vw_cwf_Doc.getRecord().newFieldInGroup("cwf_Doc_Text_Pointer", "TEXT-POINTER", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, 
            "TEXT_POINTER");
        cwf_Doc_Text_Pointer.setDdmHeader("TEXT/POINTER");
        registerRecord(vw_cwf_Doc);

        pnd_Document_Key = localVariables.newFieldInRecord("pnd_Document_Key", "#DOCUMENT-KEY", FieldType.STRING, 34);

        pnd_Document_Key__R_Field_7 = localVariables.newGroupInRecord("pnd_Document_Key__R_Field_7", "REDEFINE", pnd_Document_Key);
        pnd_Document_Key_Pnd_Doc_Cabinet_Id = pnd_Document_Key__R_Field_7.newFieldInGroup("pnd_Document_Key_Pnd_Doc_Cabinet_Id", "#DOC-CABINET-ID", FieldType.STRING, 
            14);

        pnd_Document_Key__R_Field_8 = pnd_Document_Key__R_Field_7.newGroupInGroup("pnd_Document_Key__R_Field_8", "REDEFINE", pnd_Document_Key_Pnd_Doc_Cabinet_Id);
        pnd_Document_Key_Pnd_Doc_Cabinet_Type = pnd_Document_Key__R_Field_8.newFieldInGroup("pnd_Document_Key_Pnd_Doc_Cabinet_Type", "#DOC-CABINET-TYPE", 
            FieldType.STRING, 1);
        pnd_Document_Key_Pnd_Doc_Cabinet_Name = pnd_Document_Key__R_Field_8.newFieldInGroup("pnd_Document_Key_Pnd_Doc_Cabinet_Name", "#DOC-CABINET-NAME", 
            FieldType.STRING, 13);

        pnd_Document_Key__R_Field_9 = pnd_Document_Key__R_Field_8.newGroupInGroup("pnd_Document_Key__R_Field_9", "REDEFINE", pnd_Document_Key_Pnd_Doc_Cabinet_Name);
        pnd_Document_Key_Pnd_Doc_Cabinet_Name_A12 = pnd_Document_Key__R_Field_9.newFieldInGroup("pnd_Document_Key_Pnd_Doc_Cabinet_Name_A12", "#DOC-CABINET-NAME-A12", 
            FieldType.STRING, 12);
        pnd_Document_Key_Pnd_Doc_Tiaa_Rcvd_Dte = pnd_Document_Key__R_Field_7.newFieldInGroup("pnd_Document_Key_Pnd_Doc_Tiaa_Rcvd_Dte", "#DOC-TIAA-RCVD-DTE", 
            FieldType.DATE);
        pnd_Document_Key_Pnd_Case_Workprcss_Multi_Subrqst = pnd_Document_Key__R_Field_7.newFieldInGroup("pnd_Document_Key_Pnd_Case_Workprcss_Multi_Subrqst", 
            "#CASE-WORKPRCSS-MULTI-SUBRQST", FieldType.STRING, 9);

        pnd_Document_Key__R_Field_10 = pnd_Document_Key__R_Field_7.newGroupInGroup("pnd_Document_Key__R_Field_10", "REDEFINE", pnd_Document_Key_Pnd_Case_Workprcss_Multi_Subrqst);
        pnd_Document_Key_Pnd_Doc_Case_Ind = pnd_Document_Key__R_Field_10.newFieldInGroup("pnd_Document_Key_Pnd_Doc_Case_Ind", "#DOC-CASE-IND", FieldType.STRING, 
            1);
        pnd_Document_Key_Pnd_Doc_Originating_Work_Prcss_Id = pnd_Document_Key__R_Field_10.newFieldInGroup("pnd_Document_Key_Pnd_Doc_Originating_Work_Prcss_Id", 
            "#DOC-ORIGINATING-WORK-PRCSS-ID", FieldType.STRING, 6);
        pnd_Document_Key_Pnd_Doc_Multi_Rqst_Ind = pnd_Document_Key__R_Field_10.newFieldInGroup("pnd_Document_Key_Pnd_Doc_Multi_Rqst_Ind", "#DOC-MULTI-RQST-IND", 
            FieldType.STRING, 1);
        pnd_Document_Key_Pnd_Doc_Sub_Rqst_Ind = pnd_Document_Key__R_Field_10.newFieldInGroup("pnd_Document_Key_Pnd_Doc_Sub_Rqst_Ind", "#DOC-SUB-RQST-IND", 
            FieldType.STRING, 1);
        pnd_Document_Key_Pnd_Doc_Entry_Dte_Tme = pnd_Document_Key__R_Field_7.newFieldInGroup("pnd_Document_Key_Pnd_Doc_Entry_Dte_Tme", "#DOC-ENTRY-DTE-TME", 
            FieldType.TIME);

        vw_cwf_Text = new DataAccessProgramView(new NameInfo("vw_cwf_Text", "CWF-TEXT"), "CWF_EFM_TEXT_OBJECT", "CWF_EMF_TEXT_OBJ", DdmPeriodicGroups.getInstance().getGroups("CWF_EFM_TEXT_OBJECT"));
        cwf_Text_Cabinet_Id = vw_cwf_Text.getRecord().newFieldInGroup("cwf_Text_Cabinet_Id", "CABINET-ID", FieldType.STRING, 14, RepeatingFieldStrategy.None, 
            "CABINET_ID");
        cwf_Text_Cabinet_Id.setDdmHeader("CABINET/ID");
        cwf_Text_Text_Pointer = vw_cwf_Text.getRecord().newFieldInGroup("cwf_Text_Text_Pointer", "TEXT-POINTER", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, 
            "TEXT_POINTER");
        cwf_Text_Text_Pointer.setDdmHeader("TEXT/POINTER");
        cwf_Text_Text_Sqnce_Nbr = vw_cwf_Text.getRecord().newFieldInGroup("cwf_Text_Text_Sqnce_Nbr", "TEXT-SQNCE-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TEXT_SQNCE_NBR");
        cwf_Text_Text_Sqnce_Nbr.setDdmHeader("SQNCE/NBR");
        cwf_Text_Last_Text_Lines_Group_Ind = vw_cwf_Text.getRecord().newFieldInGroup("cwf_Text_Last_Text_Lines_Group_Ind", "LAST-TEXT-LINES-GROUP-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "LAST_TEXT_LINES_GROUP_IND");
        cwf_Text_Last_Text_Lines_Group_Ind.setDdmHeader("LAST/TEXT IND");
        cwf_Text_Count_Casttext_Document_Line_Group = vw_cwf_Text.getRecord().newFieldInGroup("cwf_Text_Count_Casttext_Document_Line_Group", "C*TEXT-DOCUMENT-LINE-GROUP", 
            RepeatingFieldStrategy.CAsteriskVariable, "CWF_EMF_TEXT_OBJ_TEXT_DOCUMENT_LINE_GROUP");

        cwf_Text_Text_Document_Line_Group = vw_cwf_Text.getRecord().newGroupArrayInGroup("cwf_Text_Text_Document_Line_Group", "TEXT-DOCUMENT-LINE-GROUP", 
            new DbsArrayController(1, 30) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CWF_EMF_TEXT_OBJ_TEXT_DOCUMENT_LINE_GROUP");
        cwf_Text_Text_Document_Line_Group.setDdmHeader("TEXT/LINES");
        cwf_Text_Text_Document_Line = cwf_Text_Text_Document_Line_Group.newFieldInGroup("cwf_Text_Text_Document_Line", "TEXT-DOCUMENT-LINE", FieldType.STRING, 
            80, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "TEXT_DOCUMENT_LINE", "CWF_EMF_TEXT_OBJ_TEXT_DOCUMENT_LINE_GROUP");
        cwf_Text_Text_Document_Line.setDdmHeader("TEXT DOCUMENT/LINE");
        registerRecord(vw_cwf_Text);

        pnd_Text_Object_Key = localVariables.newFieldInRecord("pnd_Text_Object_Key", "#TEXT-OBJECT-KEY", FieldType.STRING, 24);

        pnd_Text_Object_Key__R_Field_11 = localVariables.newGroupInRecord("pnd_Text_Object_Key__R_Field_11", "REDEFINE", pnd_Text_Object_Key);
        pnd_Text_Object_Key_Pnd_Txt_Cabinet_Id = pnd_Text_Object_Key__R_Field_11.newFieldInGroup("pnd_Text_Object_Key_Pnd_Txt_Cabinet_Id", "#TXT-CABINET-ID", 
            FieldType.STRING, 14);
        pnd_Text_Object_Key_Pnd_Txt_Text_Pointer = pnd_Text_Object_Key__R_Field_11.newFieldInGroup("pnd_Text_Object_Key_Pnd_Txt_Text_Pointer", "#TXT-TEXT-POINTER", 
            FieldType.NUMERIC, 7);
        pnd_Text_Object_Key_Pnd_Txt_Text_Sqnce_Nbr = pnd_Text_Object_Key__R_Field_11.newFieldInGroup("pnd_Text_Object_Key_Pnd_Txt_Text_Sqnce_Nbr", "#TXT-TEXT-SQNCE-NBR", 
            FieldType.NUMERIC, 3);
        pnd_Last_Run_Date = localVariables.newFieldInRecord("pnd_Last_Run_Date", "#LAST-RUN-DATE", FieldType.STRING, 8);
        pnd_No_Ia_Payment_Wpids = localVariables.newFieldInRecord("pnd_No_Ia_Payment_Wpids", "#NO-IA-PAYMENT-WPIDS", FieldType.STRING, 120);

        pnd_No_Ia_Payment_Wpids__R_Field_12 = localVariables.newGroupInRecord("pnd_No_Ia_Payment_Wpids__R_Field_12", "REDEFINE", pnd_No_Ia_Payment_Wpids);
        pnd_No_Ia_Payment_Wpids_Pnd_No_Ia_Payment_Wpid = pnd_No_Ia_Payment_Wpids__R_Field_12.newFieldArrayInGroup("pnd_No_Ia_Payment_Wpids_Pnd_No_Ia_Payment_Wpid", 
            "#NO-IA-PAYMENT-WPID", FieldType.STRING, 6, new DbsArrayController(1, 20));
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.INTEGER, 4);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.BINARY, 4);
        pnd_Wpid_Hit = localVariables.newFieldInRecord("pnd_Wpid_Hit", "#WPID-HIT", FieldType.NUMERIC, 1);
        pnd_Successful_Cnt = localVariables.newFieldInRecord("pnd_Successful_Cnt", "#SUCCESSFUL-CNT", FieldType.NUMERIC, 5);
        pnd_Error_Rpt_Cnt = localVariables.newFieldInRecord("pnd_Error_Rpt_Cnt", "#ERROR-RPT-CNT", FieldType.NUMERIC, 5);
        pnd_Et_Cntr = localVariables.newFieldInRecord("pnd_Et_Cntr", "#ET-CNTR", FieldType.NUMERIC, 2);
        pnd_Text_Dod = localVariables.newFieldInRecord("pnd_Text_Dod", "#TEXT-DOD", FieldType.STRING, 10);

        pnd_Text_Dod__R_Field_13 = localVariables.newGroupInRecord("pnd_Text_Dod__R_Field_13", "REDEFINE", pnd_Text_Dod);
        pnd_Text_Dod_Pnd_Dod_Mm = pnd_Text_Dod__R_Field_13.newFieldInGroup("pnd_Text_Dod_Pnd_Dod_Mm", "#DOD-MM", FieldType.NUMERIC, 2);
        pnd_Text_Dod_Pnd_Dod_Fil1 = pnd_Text_Dod__R_Field_13.newFieldInGroup("pnd_Text_Dod_Pnd_Dod_Fil1", "#DOD-FIL1", FieldType.STRING, 1);
        pnd_Text_Dod_Pnd_Dod_Dd = pnd_Text_Dod__R_Field_13.newFieldInGroup("pnd_Text_Dod_Pnd_Dod_Dd", "#DOD-DD", FieldType.NUMERIC, 2);
        pnd_Text_Dod_Pnd_Dod_Fil2 = pnd_Text_Dod__R_Field_13.newFieldInGroup("pnd_Text_Dod_Pnd_Dod_Fil2", "#DOD-FIL2", FieldType.STRING, 1);
        pnd_Text_Dod_Pnd_Dod_Yyyy = pnd_Text_Dod__R_Field_13.newFieldInGroup("pnd_Text_Dod_Pnd_Dod_Yyyy", "#DOD-YYYY", FieldType.NUMERIC, 4);
        pnd_Header1_1 = localVariables.newFieldInRecord("pnd_Header1_1", "#HEADER1-1", FieldType.STRING, 60);
        pnd_Header1_2 = localVariables.newFieldInRecord("pnd_Header1_2", "#HEADER1-2", FieldType.STRING, 58);
        pnd_Header1_3 = localVariables.newFieldInRecord("pnd_Header1_3", "#HEADER1-3", FieldType.STRING, 59);
        pnd_Header2_1 = localVariables.newFieldInRecord("pnd_Header2_1", "#HEADER2-1", FieldType.STRING, 60);
        pnd_Header2_2 = localVariables.newFieldInRecord("pnd_Header2_2", "#HEADER2-2", FieldType.STRING, 58);
        pnd_Header2_3 = localVariables.newFieldInRecord("pnd_Header2_3", "#HEADER2-3", FieldType.STRING, 59);
        pnd_Successful_Report_Line = localVariables.newFieldInRecord("pnd_Successful_Report_Line", "#SUCCESSFUL-REPORT-LINE", FieldType.STRING, 79);

        pnd_Successful_Report_Line__R_Field_14 = localVariables.newGroupInRecord("pnd_Successful_Report_Line__R_Field_14", "REDEFINE", pnd_Successful_Report_Line);
        pnd_Successful_Report_Line_Pnd_Stop_Pay_Pin = pnd_Successful_Report_Line__R_Field_14.newFieldInGroup("pnd_Successful_Report_Line_Pnd_Stop_Pay_Pin", 
            "#STOP-PAY-PIN", FieldType.NUMERIC, 12);
        pnd_Error_Report_Line = localVariables.newFieldInRecord("pnd_Error_Report_Line", "#ERROR-REPORT-LINE", FieldType.STRING, 79);

        pnd_Error_Report_Line__R_Field_15 = localVariables.newGroupInRecord("pnd_Error_Report_Line__R_Field_15", "REDEFINE", pnd_Error_Report_Line);
        pnd_Error_Report_Line_Pnd_Error_Pin = pnd_Error_Report_Line__R_Field_15.newFieldInGroup("pnd_Error_Report_Line_Pnd_Error_Pin", "#ERROR-PIN", FieldType.NUMERIC, 
            12);
        pnd_Error_Report_Line_Pnd_Filler = pnd_Error_Report_Line__R_Field_15.newFieldInGroup("pnd_Error_Report_Line_Pnd_Filler", "#FILLER", FieldType.STRING, 
            2);
        pnd_Error_Report_Line_Pnd_Error_Rc = pnd_Error_Report_Line__R_Field_15.newFieldInGroup("pnd_Error_Report_Line_Pnd_Error_Rc", "#ERROR-RC", FieldType.NUMERIC, 
            4);
        pnd_Error_Report_Line_Pnd_Filler2 = pnd_Error_Report_Line__R_Field_15.newFieldInGroup("pnd_Error_Report_Line_Pnd_Filler2", "#FILLER2", FieldType.STRING, 
            2);
        pnd_Error_Report_Line_Pnd_Error_Message = pnd_Error_Report_Line__R_Field_15.newFieldInGroup("pnd_Error_Report_Line_Pnd_Error_Message", "#ERROR-MESSAGE", 
            FieldType.STRING, 50);
        pnd_Nat_Err_Msg = localVariables.newFieldInRecord("pnd_Nat_Err_Msg", "#NAT-ERR-MSG", FieldType.STRING, 50);

        pnd_Iaan9125_Interface = localVariables.newGroupInRecord("pnd_Iaan9125_Interface", "#IAAN9125-INTERFACE");
        pnd_Iaan9125_Interface_Pnd_Iaan9125_Pin = pnd_Iaan9125_Interface.newFieldInGroup("pnd_Iaan9125_Interface_Pnd_Iaan9125_Pin", "#IAAN9125-PIN", FieldType.NUMERIC, 
            12);
        pnd_Iaan9125_Interface_Pnd_Iaan9125_Ssn = pnd_Iaan9125_Interface.newFieldInGroup("pnd_Iaan9125_Interface_Pnd_Iaan9125_Ssn", "#IAAN9125-SSN", FieldType.STRING, 
            9);

        pnd_Iaan9125_Interface__R_Field_16 = pnd_Iaan9125_Interface.newGroupInGroup("pnd_Iaan9125_Interface__R_Field_16", "REDEFINE", pnd_Iaan9125_Interface_Pnd_Iaan9125_Ssn);
        pnd_Iaan9125_Interface_Pnd_Iaan9125_Ssn_Num = pnd_Iaan9125_Interface__R_Field_16.newFieldInGroup("pnd_Iaan9125_Interface_Pnd_Iaan9125_Ssn_Num", 
            "#IAAN9125-SSN-NUM", FieldType.NUMERIC, 9);
        pnd_Iaan9125_Interface_Pnd_Iaan9125_Dod = pnd_Iaan9125_Interface.newFieldInGroup("pnd_Iaan9125_Interface_Pnd_Iaan9125_Dod", "#IAAN9125-DOD", FieldType.NUMERIC, 
            8);

        pnd_Iaan9125_Interface__R_Field_17 = pnd_Iaan9125_Interface.newGroupInGroup("pnd_Iaan9125_Interface__R_Field_17", "REDEFINE", pnd_Iaan9125_Interface_Pnd_Iaan9125_Dod);
        pnd_Iaan9125_Interface_Pnd_Iaan9125_Dod_Yyyy = pnd_Iaan9125_Interface__R_Field_17.newFieldInGroup("pnd_Iaan9125_Interface_Pnd_Iaan9125_Dod_Yyyy", 
            "#IAAN9125-DOD-YYYY", FieldType.NUMERIC, 4);
        pnd_Iaan9125_Interface_Pnd_Iaan9125_Dod_Mm = pnd_Iaan9125_Interface__R_Field_17.newFieldInGroup("pnd_Iaan9125_Interface_Pnd_Iaan9125_Dod_Mm", 
            "#IAAN9125-DOD-MM", FieldType.NUMERIC, 2);
        pnd_Iaan9125_Interface_Pnd_Iaan9125_Dod_Dd = pnd_Iaan9125_Interface__R_Field_17.newFieldInGroup("pnd_Iaan9125_Interface_Pnd_Iaan9125_Dod_Dd", 
            "#IAAN9125-DOD-DD", FieldType.NUMERIC, 2);
        pnd_Iaan9125_Interface_Pnd_Iaan9125_Rc = pnd_Iaan9125_Interface.newFieldInGroup("pnd_Iaan9125_Interface_Pnd_Iaan9125_Rc", "#IAAN9125-RC", FieldType.NUMERIC, 
            4);
        pnd_Iaan9125_Interface_Pnd_Iaan9125_Msg = pnd_Iaan9125_Interface.newFieldInGroup("pnd_Iaan9125_Interface_Pnd_Iaan9125_Msg", "#IAAN9125-MSG", FieldType.STRING, 
            50);
        pnd_Iaan9125_Interface_Pnd_Iaan9125_Match_Ind = pnd_Iaan9125_Interface.newFieldInGroup("pnd_Iaan9125_Interface_Pnd_Iaan9125_Match_Ind", "#IAAN9125-MATCH-IND", 
            FieldType.STRING, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_mit.reset();
        vw_cwf_Support_Tbl.reset();
        vw_cwf_Doc.reset();
        vw_cwf_Text.reset();

        localVariables.reset();
        pnd_Successful_Cnt.setInitialValue(0);
        pnd_Error_Rpt_Cnt.setInitialValue(0);
        pnd_Et_Cntr.setInitialValue(0);
        pnd_Header1_1.setInitialValue("            AUTOMATED STOP IA PAYMENT PROCESS");
        pnd_Header1_2.setInitialValue("              PAYMENTS STOPPED REPORT");
        pnd_Header1_3.setInitialValue("PIN             ");
        pnd_Header2_1.setInitialValue("            AUTOMATED STOP IA PAYMENT PROCESS");
        pnd_Header2_2.setInitialValue("                   ERROR REPORT");
        pnd_Header2_3.setInitialValue("PIN             ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb9125() throws Exception
    {
        super("Cwfb9125");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("CWFB9125", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //* ***************************************************                                                                                                           //Natural: FORMAT PS = 37 LS = 80;//Natural: FORMAT ( 1 ) PS = 37 LS = 80;//Natural: FORMAT ( 2 ) PS = 37 LS = 80
        //*  OPEN MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
                                                                                                                                                                          //Natural: PERFORM GET-LAST-RUN-DATE
        sub_Get_Last_Run_Date();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM LOAD-NO-IA-PAYMENT-WPID
        sub_Load_No_Ia_Payment_Wpid();
        if (condition(Global.isEscape())) {return;}
        pnd_Mit_Rqst_Log_Dte_Tme_Pnd_Run_Yyyymmdd.setValue(pnd_Last_Run_Date);                                                                                            //Natural: ASSIGN #RUN-YYYYMMDD := #LAST-RUN-DATE
        pnd_Mit_Rqst_Log_Dte_Tme_Pnd_Run_0.setValue("0000000");                                                                                                           //Natural: ASSIGN #RUN-0 := '0000000'
        pnd_Unit_Wpid_Key_Pnd_Key_Open.setValue("0");                                                                                                                     //Natural: ASSIGN #KEY-OPEN := '0'
        pnd_Unit_Wpid_Key_Pnd_Key_Unit.setValue("ACTPS   ");                                                                                                              //Natural: ASSIGN #KEY-UNIT := 'ACTPS   '
        pnd_Unit_Wpid_Key_Pnd_Key_Wpid.setValue("A     ");                                                                                                                //Natural: ASSIGN #KEY-WPID := 'A     '
        vw_mit.startDatabaseRead                                                                                                                                          //Natural: READ MIT BY UNIT-WPID-KEY STARTING FROM #UNIT-WPID-KEY
        (
        "READ01",
        new Wc[] { new Wc("UNIT_WPID_KEY", ">=", pnd_Unit_Wpid_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("UNIT_WPID_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_mit.readNextRow("READ01")))
        {
            if (condition(mit_Crprte_Status_Ind.notEquals("0")))                                                                                                          //Natural: IF MIT.CRPRTE-STATUS-IND NE '0'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(mit_Admin_Unit_Cde.greater("BPSSFWC ")))                                                                                                        //Natural: IF MIT.ADMIN-UNIT-CDE GT 'BPSSFWC '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!((((mit_Admin_Unit_Cde.equals("ACTPS   ") || mit_Admin_Unit_Cde.equals("BPSSFWC ")) && mit_Rqst_Log_Dte_Tme.greaterOrEqual(pnd_Mit_Rqst_Log_Dte_Tme))  //Natural: ACCEPT IF ( MIT.ADMIN-UNIT-CDE = 'ACTPS   ' OR = 'BPSSFWC ' ) AND RQST-LOG-DTE-TME GE #MIT-RQST-LOG-DTE-TME AND WORK-PRCSS-ID = #NO-IA-PAYMENT-WPID ( * )
                && mit_Work_Prcss_Id.equals(pnd_No_Ia_Payment_Wpids_Pnd_No_Ia_Payment_Wpid.getValue("*"))))))
            {
                continue;
            }
                                                                                                                                                                          //Natural: PERFORM RETRIEVE-DOD-SSN
            sub_Retrieve_Dod_Ssn();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Text_Dod.equals(" ")))                                                                                                                      //Natural: IF #TEXT-DOD = ' '
            {
                pnd_Error_Report_Line_Pnd_Error_Rc.setValue(0);                                                                                                           //Natural: ASSIGN #ERROR-RC := 0
                pnd_Error_Report_Line_Pnd_Error_Message.setValue("DOD NOT FOUND");                                                                                        //Natural: ASSIGN #ERROR-MESSAGE := 'DOD NOT FOUND'
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-RPT-RCRD
                sub_Write_Error_Rpt_Rcrd();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(DbsUtil.maskMatches(pnd_Iaan9125_Interface_Pnd_Iaan9125_Dod,"NNNNNNNN")))                                                                       //Natural: IF #IAAN9125-DOD = MASK ( NNNNNNNN )
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Error_Report_Line_Pnd_Error_Rc.setValue(0);                                                                                                           //Natural: ASSIGN #ERROR-RC := 0
                pnd_Error_Report_Line_Pnd_Error_Message.setValue("DOD INVALID ON EFM");                                                                                   //Natural: ASSIGN #ERROR-MESSAGE := 'DOD INVALID ON EFM'
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-RPT-RCRD
                sub_Write_Error_Rpt_Rcrd();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(DbsUtil.maskMatches(pnd_Iaan9125_Interface_Pnd_Iaan9125_Ssn_Num,"NNNNNNNNN")))                                                                  //Natural: IF #IAAN9125-SSN-NUM = MASK ( NNNNNNNNN )
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Error_Report_Line_Pnd_Error_Rc.setValue(0);                                                                                                           //Natural: ASSIGN #ERROR-RC := 0
                pnd_Error_Report_Line_Pnd_Error_Message.setValue("SSN INVALID ON EFM");                                                                                   //Natural: ASSIGN #ERROR-MESSAGE := 'SSN INVALID ON EFM'
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-RPT-RCRD
                sub_Write_Error_Rpt_Rcrd();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Iaan9125_Interface_Pnd_Iaan9125_Pin.setValue(mit_Pin_Nbr);                                                                                                //Natural: ASSIGN #IAAN9125-PIN := PIN-NBR
            pnd_Iaan9125_Interface_Pnd_Iaan9125_Rc.reset();                                                                                                               //Natural: RESET #IAAN9125-RC #IAAN9125-MSG
            pnd_Iaan9125_Interface_Pnd_Iaan9125_Msg.reset();
            DbsUtil.callnat(Iaan9125.class , getCurrentProcessState(), pnd_Iaan9125_Interface);                                                                           //Natural: CALLNAT 'IAAN9125' #IAAN9125-INTERFACE
            if (condition(Global.isEscape())) return;
            getReports().print(0, "=",pnd_Iaan9125_Interface_Pnd_Iaan9125_Pin,"=",pnd_Iaan9125_Interface_Pnd_Iaan9125_Rc);                                                //Natural: PRINT '=' #IAAN9125-PIN '=' #IAAN9125-RC
            if (condition(pnd_Iaan9125_Interface_Pnd_Iaan9125_Rc.equals(getZero())))                                                                                      //Natural: IF #IAAN9125-RC = 0
            {
                pnd_Isn.setValue(vw_mit.getAstISN("Read01"));                                                                                                             //Natural: ASSIGN #ISN := *ISN
                                                                                                                                                                          //Natural: PERFORM GET-AND-UPDATE
                sub_Get_And_Update();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM WRITE-SUCCESSFUL-RPT-RCRD
                sub_Write_Successful_Rpt_Rcrd();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Error_Report_Line_Pnd_Error_Rc.setValue(pnd_Iaan9125_Interface_Pnd_Iaan9125_Rc);                                                                      //Natural: ASSIGN #ERROR-RC := #IAAN9125-RC
                pnd_Error_Report_Line_Pnd_Error_Message.setValue(pnd_Iaan9125_Interface_Pnd_Iaan9125_Msg);                                                                //Natural: ASSIGN #ERROR-MESSAGE := #IAAN9125-MSG
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-RPT-RCRD
                sub_Write_Error_Rpt_Rcrd();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        getReports().write(1, ReportOption.NOTITLE,"TOTAL STOP PAYMENTS PROCESSED",pnd_Successful_Cnt, new ReportEditMask ("ZZ,ZZ9"));                                    //Natural: WRITE ( 1 ) 'TOTAL STOP PAYMENTS PROCESSED' #SUCCESSFUL-CNT ( EM = ZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"TOTAL ERROR CASES",pnd_Error_Rpt_Cnt, new ReportEditMask ("ZZ,ZZ9"));                                                 //Natural: WRITE ( 2 ) 'TOTAL ERROR CASES' #ERROR-RPT-CNT ( EM = ZZ,ZZ9 )
        if (Global.isEscape()) return;
        //*  CLOSE MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        //* ***********************************************************************
        //* **********************************************************************
        //* **********************************************************************
        //* **********************************************************************
        //* **********************************************************************
        //* **********************************************************************
        //* **********************************************************************
        //* **********************************************************************
        //* **********************************************************************
        //* **********************************************************************
        //* **********************************************************************
        //* **********************************************************************
        //* *********************************************************************
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 );//Natural: AT TOP OF PAGE ( 2 )
    }                                                                                                                                                                     //Natural: ON ERROR
    private void sub_Get_Last_Run_Date() throws Exception                                                                                                                 //Natural: GET-LAST-RUN-DATE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind.setValue("S");                                                                                                          //Natural: ASSIGN #TBL-SCRTY-LEVEL-IND := 'S'
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme.setValue("BERWYN-MIT-LOG-DATE");                                                                                              //Natural: ASSIGN #TBL-TABLE-NME := 'BERWYN-MIT-LOG-DATE'
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field.setValue("LAST-RUN-DATE");                                                                                                    //Natural: ASSIGN #TBL-KEY-FIELD := 'LAST-RUN-DATE'
        vw_cwf_Support_Tbl.startDatabaseRead                                                                                                                              //Natural: READ CWF-SUPPORT-TBL BY TBL-PRIME-KEY STARTING FROM #TBL-PRIME-KEY
        (
        "READ02",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
        );
        READ02:
        while (condition(vw_cwf_Support_Tbl.readNextRow("READ02")))
        {
            if (condition(cwf_Support_Tbl_Tbl_Table_Nme.notEquals("BERWYN-MIT-LOG-DATE")))                                                                                //Natural: IF TBL-TABLE-NME NE 'BERWYN-MIT-LOG-DATE'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(cwf_Support_Tbl_Tbl_Key_Field.getSubstring(1,13).equals("LAST-RUN-DATE")))                                                                  //Natural: IF SUBSTRING ( CWF-SUPPORT-TBL.TBL-KEY-FIELD,1,13 ) = 'LAST-RUN-DATE'
                {
                    pnd_Last_Run_Date.setValue(cwf_Support_Tbl_Tbl_Last_Run_Date);                                                                                        //Natural: ASSIGN #LAST-RUN-DATE := CWF-SUPPORT-TBL.TBL-LAST-RUN-DATE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Load_No_Ia_Payment_Wpid() throws Exception                                                                                                           //Natural: LOAD-NO-IA-PAYMENT-WPID
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind.setValue("S");                                                                                                          //Natural: ASSIGN #TBL-SCRTY-LEVEL-IND := 'S'
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme.setValue("BERWYN-MIT-LOG-DATE");                                                                                              //Natural: ASSIGN #TBL-TABLE-NME := 'BERWYN-MIT-LOG-DATE'
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field.setValue("NO-IA-PAYMENT-WPID");                                                                                               //Natural: ASSIGN #TBL-KEY-FIELD := 'NO-IA-PAYMENT-WPID'
        pnd_X.setValue(1);                                                                                                                                                //Natural: ASSIGN #X := 1
        vw_cwf_Support_Tbl.startDatabaseRead                                                                                                                              //Natural: READ CWF-SUPPORT-TBL BY TBL-PRIME-KEY STARTING FROM #TBL-PRIME-KEY
        (
        "READ03",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
        );
        READ03:
        while (condition(vw_cwf_Support_Tbl.readNextRow("READ03")))
        {
            if (condition(cwf_Support_Tbl_Tbl_Table_Nme.notEquals("BERWYN-MIT-LOG-DATE")))                                                                                //Natural: IF TBL-TABLE-NME NE 'BERWYN-MIT-LOG-DATE'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(cwf_Support_Tbl_Tbl_Key_Field.getSubstring(1,18).equals("NO-IA-PAYMENT-WPID")))                                                             //Natural: IF SUBSTRING ( CWF-SUPPORT-TBL.TBL-KEY-FIELD,1,18 ) = 'NO-IA-PAYMENT-WPID'
                {
                    pnd_No_Ia_Payment_Wpids.setValue(cwf_Support_Tbl_Tbl_No_Ia_Payment_Wps);                                                                              //Natural: ASSIGN #NO-IA-PAYMENT-WPIDS := TBL-NO-IA-PAYMENT-WPS
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Retrieve_Dod_Ssn() throws Exception                                                                                                                  //Natural: RETRIEVE-DOD-SSN
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Document_Key_Pnd_Doc_Cabinet_Type.setValue("P");                                                                                                              //Natural: ASSIGN #DOC-CABINET-TYPE := 'P'
        //*  #DOC-CABINET-NAME              := MIT.PIN-NBR      /*PIN-EXP
        //*  PIN-EXP
        pnd_Document_Key_Pnd_Doc_Cabinet_Name_A12.moveAll(mit_Pin_Nbr);                                                                                                   //Natural: MOVE ALL MIT.PIN-NBR TO #DOC-CABINET-NAME-A12
        pnd_Document_Key_Pnd_Doc_Tiaa_Rcvd_Dte.setValue(mit_Tiaa_Rcvd_Dte);                                                                                               //Natural: ASSIGN #DOC-TIAA-RCVD-DTE := MIT.TIAA-RCVD-DTE
        pnd_Document_Key_Pnd_Doc_Case_Ind.setValue(mit_Case_Ind);                                                                                                         //Natural: ASSIGN #DOC-CASE-IND := MIT.CASE-IND
        pnd_Document_Key_Pnd_Doc_Originating_Work_Prcss_Id.setValue(mit_Work_Prcss_Id);                                                                                   //Natural: ASSIGN #DOC-ORIGINATING-WORK-PRCSS-ID := MIT.WORK-PRCSS-ID
        vw_cwf_Doc.startDatabaseRead                                                                                                                                      //Natural: READ CWF-DOC WITH DOCUMENT-KEY = #DOCUMENT-KEY
        (
        "READ04",
        new Wc[] { new Wc("DOCUMENT_KEY", ">=", pnd_Document_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("DOCUMENT_KEY", "ASC") }
        );
        READ04:
        while (condition(vw_cwf_Doc.readNextRow("READ04")))
        {
            if (condition(cwf_Doc_Cabinet_Id.notEquals(pnd_Document_Key_Pnd_Doc_Cabinet_Id) || cwf_Doc_Tiaa_Rcvd_Dte.notEquals(pnd_Document_Key_Pnd_Doc_Tiaa_Rcvd_Dte)))  //Natural: IF CWF-DOC.CABINET-ID NE #DOC-CABINET-ID OR CWF-DOC.TIAA-RCVD-DTE NE #DOC-TIAA-RCVD-DTE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Text_Object_Key_Pnd_Txt_Cabinet_Id.setValue(cwf_Doc_Cabinet_Id);                                                                                          //Natural: ASSIGN #TXT-CABINET-ID := CWF-DOC.CABINET-ID
            pnd_Text_Object_Key_Pnd_Txt_Text_Pointer.setValue(cwf_Doc_Text_Pointer);                                                                                      //Natural: ASSIGN #TXT-TEXT-POINTER := CWF-DOC.TEXT-POINTER
            pnd_Text_Object_Key_Pnd_Txt_Text_Sqnce_Nbr.setValue(1);                                                                                                       //Natural: ASSIGN #TXT-TEXT-SQNCE-NBR := 1
            vw_cwf_Text.startDatabaseRead                                                                                                                                 //Natural: READ ( 1 ) CWF-TEXT WITH TEXT-OBJECT-KEY = #TEXT-OBJECT-KEY
            (
            "READ05",
            new Wc[] { new Wc("TEXT_OBJECT_KEY", ">=", pnd_Text_Object_Key, WcType.BY) },
            new Oc[] { new Oc("TEXT_OBJECT_KEY", "ASC") },
            1
            );
            READ05:
            while (condition(vw_cwf_Text.readNextRow("READ05")))
            {
                pnd_Text_Dod.setValue(cwf_Text_Text_Document_Line.getValue(1));                                                                                           //Natural: MOVE TEXT-DOCUMENT-LINE ( 1 ) TO #TEXT-DOD
                pnd_Iaan9125_Interface_Pnd_Iaan9125_Dod_Yyyy.setValue(pnd_Text_Dod_Pnd_Dod_Yyyy);                                                                         //Natural: ASSIGN #IAAN9125-DOD-YYYY := #DOD-YYYY
                pnd_Iaan9125_Interface_Pnd_Iaan9125_Dod_Mm.setValue(pnd_Text_Dod_Pnd_Dod_Mm);                                                                             //Natural: ASSIGN #IAAN9125-DOD-MM := #DOD-MM
                pnd_Iaan9125_Interface_Pnd_Iaan9125_Dod_Dd.setValue(pnd_Text_Dod_Pnd_Dod_Dd);                                                                             //Natural: ASSIGN #IAAN9125-DOD-DD := #DOD-DD
                pnd_Iaan9125_Interface_Pnd_Iaan9125_Ssn.setValue(cwf_Text_Text_Document_Line.getValue(2));                                                                //Natural: MOVE TEXT-DOCUMENT-LINE ( 2 ) TO #IAAN9125-SSN
                //* ************ ADD ON 8/30/13 *************************
                pnd_Iaan9125_Interface_Pnd_Iaan9125_Match_Ind.setValue(cwf_Text_Text_Document_Line.getValue(4));                                                          //Natural: MOVE TEXT-DOCUMENT-LINE ( 4 ) TO #IAAN9125-MATCH-IND
                //* *****************************************************
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Get_And_Update() throws Exception                                                                                                                    //Natural: GET-AND-UPDATE
    {
        if (BLNatReinput.isReinput()) return;

        GET:                                                                                                                                                              //Natural: GET MIT #ISN
        vw_mit.readByID(pnd_Isn.getLong(), "GET");
        mit_Status_Cde.setValue("8020");                                                                                                                                  //Natural: MOVE '8020' TO STATUS-CDE ADMIN-STATUS-CDE
        mit_Admin_Status_Cde.setValue("8020");
        mit_Status_Updte_Dte_Tme.setValue(Global.getTIMX());                                                                                                              //Natural: MOVE *TIMX TO STATUS-UPDTE-DTE-TME ADMIN-STATUS-UPDTE-DTE-TME LAST-UPDTE-DTE-TME
        mit_Admin_Status_Updte_Dte_Tme.setValue(Global.getTIMX());
        mit_Last_Updte_Dte_Tme.setValue(Global.getTIMX());
        mit_Status_Updte_Oprtr_Cde.setValue("BATCHCWF");                                                                                                                  //Natural: MOVE 'BATCHCWF' TO STATUS-UPDTE-OPRTR-CDE ADMIN-STATUS-UPDTE-OPRTR-CDE LAST-UPDTE-OPRTR-CDE
        mit_Admin_Status_Updte_Oprtr_Cde.setValue("BATCHCWF");
        mit_Last_Updte_Oprtr_Cde.setValue("BATCHCWF");
        mit_Crprte_Status_Ind.setValue("9");                                                                                                                              //Natural: MOVE '9' TO CRPRTE-STATUS-IND
        mit_Work_List_Ind.setValue("   ");                                                                                                                                //Natural: MOVE '   ' TO WORK-LIST-IND
        mit_Admin_Unit_Cde.setValue("RSSMP");                                                                                                                             //Natural: MOVE 'RSSMP' TO ADMIN-UNIT-CDE
        vw_mit.updateDBRow("GET");                                                                                                                                        //Natural: UPDATE ( GET. )
        pnd_Et_Cntr.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #ET-CNTR
        if (condition(pnd_Et_Cntr.greater(50)))                                                                                                                           //Natural: IF #ET-CNTR GT 50
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            pnd_Et_Cntr.setValue(0);                                                                                                                                      //Natural: ASSIGN #ET-CNTR := 0
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Successful_Rpt_Rcrd() throws Exception                                                                                                         //Natural: WRITE-SUCCESSFUL-RPT-RCRD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Successful_Report_Line_Pnd_Stop_Pay_Pin.setValue(mit_Pin_Nbr);                                                                                                //Natural: ASSIGN #STOP-PAY-PIN := MIT.PIN-NBR
        pnd_Successful_Cnt.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #SUCCESSFUL-CNT
        getReports().write(1, ReportOption.NOTITLE,pnd_Successful_Report_Line);                                                                                           //Natural: WRITE ( 1 ) #SUCCESSFUL-REPORT-LINE
        if (Global.isEscape()) return;
    }
    private void sub_Write_Error_Rpt_Rcrd() throws Exception                                                                                                              //Natural: WRITE-ERROR-RPT-RCRD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Error_Report_Line_Pnd_Error_Pin.setValue(mit_Pin_Nbr);                                                                                                        //Natural: ASSIGN #ERROR-PIN := MIT.PIN-NBR
        pnd_Error_Rpt_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #ERROR-RPT-CNT
        getReports().write(2, ReportOption.NOTITLE,pnd_Error_Report_Line);                                                                                                //Natural: WRITE ( 2 ) #ERROR-REPORT-LINE
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),pnd_Header1_1,"Page",getReports().getPageNumberDbs(1), new NumericLength               //Natural: WRITE ( 1 ) NOTITLE *PROGRAM #HEADER1-1 'Page' *PAGE-NUMBER ( 1 ) ( NL = 4 AD = L SG = OFF ) / *DATX ( EM = MM/DD/YYYY ) #HEADER1-2 *TIMX ( EM = HH':'II' 'AP ) / #HEADER1-3
                        (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),pnd_Header1_2,Global.getTIMX(), 
                        new ReportEditMask ("HH':'II' 'AP"),NEWLINE,pnd_Header1_3);
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,Global.getPROGRAM(),pnd_Header2_1,"Page",getReports().getPageNumberDbs(1), new NumericLength               //Natural: WRITE ( 2 ) NOTITLE *PROGRAM #HEADER2-1 'Page' *PAGE-NUMBER ( 1 ) ( NL = 4 AD = L SG = OFF ) / *DATX ( EM = MM/DD/YYYY ) #HEADER2-2 *TIMX ( EM = HH':'II' 'AP ) / #HEADER2-3
                        (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),pnd_Header2_2,Global.getTIMX(), 
                        new ReportEditMask ("HH':'II' 'AP"),NEWLINE,pnd_Header2_3);
                    getReports().skip(2, 1);                                                                                                                              //Natural: SKIP ( 2 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        pnd_Nat_Err_Msg.setValue(DbsUtil.compress("NATURAL ERROR", Global.getERROR_NR(), "ON LINE", Global.getERROR_LINE(), "OF", Global.getPROGRAM()));                  //Natural: COMPRESS 'NATURAL ERROR' *ERROR-NR 'ON LINE' *ERROR-LINE 'OF' *PROGRAM INTO #NAT-ERR-MSG
        getReports().write(0, pnd_Nat_Err_Msg);                                                                                                                           //Natural: WRITE #NAT-ERR-MSG
        DbsUtil.terminate(33);  if (true) return;                                                                                                                         //Natural: TERMINATE 33
        if (condition(true)) return;                                                                                                                                      //Natural: ESCAPE ROUTINE
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=37 LS=80");
        Global.format(1, "PS=37 LS=80");
        Global.format(2, "PS=37 LS=80");
    }
}
