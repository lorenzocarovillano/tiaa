/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:48:39 PM
**        * FROM NATURAL PROGRAM : Iefp9031
************************************************************
**        * FILE NAME            : Iefp9031.java
**        * CLASS NAME           : Iefp9031
**        * INSTANCE NAME        : Iefp9031
************************************************************
************************************************************************
* PROGRAM  : IEFP9031
* SYSTEM   : CRPCWF
* TITLE    : CONTROL MODULE STATISTICS SUMMARIZATION
* GENERATED: MARCH 10, 97 AT  10:00 AM
* FUNCTION : THIS PROGRAM READS SORTED ICW EFM AUDIT WORK FILE
*          : AND CREATES RECORDS IN THE STATISTICS FILE
* HISTORY
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iefp9031 extends BLNatBase
{
    // Data Areas
    private GdaCwfg000 gdaCwfg000;
    private LdaIefl9020 ldaIefl9020;
    private LdaIefl9031 ldaIefl9031;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Dte_System_Key;

    private DbsGroup pnd_Dte_System_Key__R_Field_1;
    private DbsField pnd_Dte_System_Key_Pnd_Log_Dte;
    private DbsField pnd_Dte_System_Key_Pnd_System;
    private DbsField pnd_I;
    private DbsField pnd_Unconditionally;
    private DbsField pnd_Abort_Message;
    private DbsField pnd_Rept_Message;
    private DbsField pnd_Program;
    private DbsField pnd_Restarted;
    private DbsField pnd_Restart_Count;
    private DbsField pnd_Cntl_Isn;
    private DbsField pnd_Records_Read;
    private DbsField pnd_Records_Processed;
    private DbsField pnd_Actn_Cnt;

    private DbsGroup pnd_Old;
    private DbsField pnd_Old_Log_Dte;
    private DbsField pnd_Old_System_Cde;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaCwfg000 = GdaCwfg000.getInstance(getCallnatLevel());
        registerRecord(gdaCwfg000);
        if (gdaOnly) return;

        ldaIefl9020 = new LdaIefl9020();
        registerRecord(ldaIefl9020);
        ldaIefl9031 = new LdaIefl9031();
        registerRecord(ldaIefl9031);
        registerRecord(ldaIefl9031.getVw_icw_Efm_Stats());

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Dte_System_Key = localVariables.newFieldInRecord("pnd_Dte_System_Key", "#DTE-SYSTEM-KEY", FieldType.STRING, 22);

        pnd_Dte_System_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Dte_System_Key__R_Field_1", "REDEFINE", pnd_Dte_System_Key);
        pnd_Dte_System_Key_Pnd_Log_Dte = pnd_Dte_System_Key__R_Field_1.newFieldInGroup("pnd_Dte_System_Key_Pnd_Log_Dte", "#LOG-DTE", FieldType.TIME);
        pnd_Dte_System_Key_Pnd_System = pnd_Dte_System_Key__R_Field_1.newFieldInGroup("pnd_Dte_System_Key_Pnd_System", "#SYSTEM", FieldType.STRING, 15);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 5);
        pnd_Unconditionally = localVariables.newFieldInRecord("pnd_Unconditionally", "#UNCONDITIONALLY", FieldType.BOOLEAN, 1);
        pnd_Abort_Message = localVariables.newFieldArrayInRecord("pnd_Abort_Message", "#ABORT-MESSAGE", FieldType.STRING, 80, new DbsArrayController(1, 
            2));
        pnd_Rept_Message = localVariables.newFieldArrayInRecord("pnd_Rept_Message", "#REPT-MESSAGE", FieldType.STRING, 80, new DbsArrayController(1, 2));
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Restarted = localVariables.newFieldInRecord("pnd_Restarted", "#RESTARTED", FieldType.BOOLEAN, 1);
        pnd_Restart_Count = localVariables.newFieldInRecord("pnd_Restart_Count", "#RESTART-COUNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cntl_Isn = localVariables.newFieldInRecord("pnd_Cntl_Isn", "#CNTL-ISN", FieldType.PACKED_DECIMAL, 8);
        pnd_Records_Read = localVariables.newFieldInRecord("pnd_Records_Read", "#RECORDS-READ", FieldType.PACKED_DECIMAL, 7);
        pnd_Records_Processed = localVariables.newFieldInRecord("pnd_Records_Processed", "#RECORDS-PROCESSED", FieldType.PACKED_DECIMAL, 7);
        pnd_Actn_Cnt = localVariables.newFieldInRecord("pnd_Actn_Cnt", "#ACTN-CNT", FieldType.PACKED_DECIMAL, 3);

        pnd_Old = localVariables.newGroupInRecord("pnd_Old", "#OLD");
        pnd_Old_Log_Dte = pnd_Old.newFieldInGroup("pnd_Old_Log_Dte", "LOG-DTE", FieldType.STRING, 8);
        pnd_Old_System_Cde = pnd_Old.newFieldInGroup("pnd_Old_System_Cde", "SYSTEM-CDE", FieldType.STRING, 15);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIefl9020.initializeValues();
        ldaIefl9031.initializeValues();

        localVariables.reset();
        pnd_Unconditionally.setInitialValue(true);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iefp9031() throws Exception
    {
        super("Iefp9031");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IEFP9031", onError);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        getReports().definePrinter(2, "ABORT");                                                                                                                           //Natural: DEFINE PRINTER ( ABORT = 1 )
        getReports().definePrinter(3, "REPT");                                                                                                                            //Natural: DEFINE PRINTER ( REPT = 2 )
        //*                                                                                                                                                               //Natural: FORMAT ( ABORT ) LS = 132 PS = 60 ZP = OFF SG = OFF;//Natural: FORMAT ( REPT ) LS = 132 PS = 60 ZP = OFF SG = OFF
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: ASSIGN #PROGRAM = *PROGRAM
        //*  FIND OUT IF THE LAST RUN FINISHED SUCCESSFULLY. IF NOT, A CONTROL
        //*  RECORD WILL BE PRESENT ON THE FOLDER STATISTICS FILE
        pnd_Dte_System_Key_Pnd_Log_Dte.setValue(1);                                                                                                                       //Natural: ASSIGN #LOG-DTE = 1
        pnd_Dte_System_Key_Pnd_System.setValue("CONTROL");                                                                                                                //Natural: ASSIGN #SYSTEM = 'CONTROL'
        ldaIefl9031.getVw_icw_Efm_Stats().startDatabaseFind                                                                                                               //Natural: FIND ( 1 ) ICW-EFM-STATS WITH DTE-SYSTEM-KEY = #DTE-SYSTEM-KEY
        (
        "FIND_CNTL",
        new Wc[] { new Wc("DTE_SYSTEM_KEY", "=", pnd_Dte_System_Key.getBinary(), WcType.WITH) },
        1
        );
        FIND_CNTL:
        while (condition(ldaIefl9031.getVw_icw_Efm_Stats().readNextRow("FIND_CNTL", true)))
        {
            ldaIefl9031.getVw_icw_Efm_Stats().setIfNotFoundControlFlag(false);
            if (condition(ldaIefl9031.getVw_icw_Efm_Stats().getAstCOUNTER().equals(0)))                                                                                   //Natural: IF NO RECORD FOUND
            {
                if (true) break FIND_CNTL;                                                                                                                                //Natural: ESCAPE BOTTOM ( FIND-CNTL. )
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Restarted.setValue(true);                                                                                                                                 //Natural: ASSIGN #RESTARTED = TRUE
            pnd_Restart_Count.setValue(ldaIefl9031.getIcw_Efm_Stats_Xtn_Cnt().getValue(1));                                                                               //Natural: ASSIGN #RESTART-COUNT = ICW-EFM-STATS.XTN-CNT ( 1 )
            pnd_Records_Processed.setValue(pnd_Restart_Count);                                                                                                            //Natural: ASSIGN #RECORDS-PROCESSED = #RESTART-COUNT
            pnd_Cntl_Isn.setValue(ldaIefl9031.getVw_icw_Efm_Stats().getAstISN("FIND_CNTL"));                                                                              //Natural: ASSIGN #CNTL-ISN = *ISN ( FIND-CNTL. )
            pnd_Rept_Message.getValue(1).setValue(DbsUtil.compress("CONTROL MODULE STATISTICS SUMMARIZATION RE-STARTED", "AT", Global.getDATU(), Global.getTIME()));      //Natural: COMPRESS 'CONTROL MODULE STATISTICS SUMMARIZATION RE-STARTED' 'AT' *DATU *TIME INTO #REPT-MESSAGE ( 1 )
            pnd_Rept_Message.getValue(2).setValue("PHASE 2 - CREATE STATISTICS RECORDS");                                                                                 //Natural: ASSIGN #REPT-MESSAGE ( 2 ) = 'PHASE 2 - CREATE STATISTICS RECORDS'
            getReports().write(3, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Efsf9033.class));                                                      //Natural: WRITE ( REPT ) NOTITLE NOHDR USING FORM 'EFSF9033'
            getReports().skip(0, 2);                                                                                                                                      //Natural: SKIP ( REPT ) 2 LINES
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  IF RESTART RECORD NOT FOUND, CREATE ONE
        if (condition(! (pnd_Restarted.getBoolean())))                                                                                                                    //Natural: IF NOT #RESTARTED
        {
            ldaIefl9031.getVw_icw_Efm_Stats().reset();                                                                                                                    //Natural: RESET ICW-EFM-STATS
            ldaIefl9031.getIcw_Efm_Stats_Log_Dte().setValue(1);                                                                                                           //Natural: ASSIGN ICW-EFM-STATS.LOG-DTE = 1
            ldaIefl9031.getIcw_Efm_Stats_System_Cde().setValue("CONTROL");                                                                                                //Natural: ASSIGN ICW-EFM-STATS.SYSTEM-CDE = 'CONTROL'
            STORE_CNTL:                                                                                                                                                   //Natural: STORE ICW-EFM-STATS
            ldaIefl9031.getVw_icw_Efm_Stats().insertDBRow("STORE_CNTL");
            pnd_Cntl_Isn.setValue(ldaIefl9031.getVw_icw_Efm_Stats().getAstISN("STORE_CNTL"));                                                                             //Natural: ASSIGN #CNTL-ISN = *ISN ( STORE-CNTL. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            pnd_Rept_Message.getValue(1).setValue(DbsUtil.compress("PHASE 2 - CREATE STATISTICS RECORD STARTED NORMALLY AT", Global.getDATU(), Global.getTIME()));        //Natural: COMPRESS 'PHASE 2 - CREATE STATISTICS RECORD STARTED NORMALLY AT' *DATU *TIME INTO #REPT-MESSAGE ( 1 )
            getReports().write(3, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Efsf9033.class));                                                      //Natural: WRITE ( REPT ) NOTITLE NOHDR USING FORM 'EFSF9033'
            getReports().skip(0, 2);                                                                                                                                      //Natural: SKIP ( REPT ) 2 LINES
        }                                                                                                                                                                 //Natural: END-IF
        //*  READ THE SORTED WORK FILE AND CREATE STATISTICS RECORDS
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #WORK-RECORD
        while (condition(getWorkFiles().read(1, ldaIefl9020.getPnd_Work_Record())))
        {
            pnd_Records_Read.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #RECORDS-READ
            if (condition(pnd_Records_Read.lessOrEqual(pnd_Restart_Count)))                                                                                               //Natural: REJECT IF #RECORDS-READ LE #RESTART-COUNT
            {
                continue;
            }
            if (condition(ldaIefl9020.getPnd_Work_Record_Log_Dte().equals("00000000")))                                                                                   //Natural: REJECT IF #WORK-RECORD.LOG-DTE = '00000000'
            {
                continue;
            }
            //*  FIRST TIME ONLY
            if (condition((pnd_Old_Log_Dte.equals(" ")) && (pnd_Old_System_Cde.equals(" "))))                                                                             //Natural: IF ( #OLD.LOG-DTE = ' ' ) AND ( #OLD.SYSTEM-CDE = ' ' )
            {
                pnd_Old_Log_Dte.setValue(ldaIefl9020.getPnd_Work_Record_Log_Dte());                                                                                       //Natural: ASSIGN #OLD.LOG-DTE = #WORK-RECORD.LOG-DTE
                pnd_Old_System_Cde.setValue(ldaIefl9020.getPnd_Work_Record_System_Cde());                                                                                 //Natural: ASSIGN #OLD.SYSTEM-CDE = #WORK-RECORD.SYSTEM-CDE
                ldaIefl9031.getVw_icw_Efm_Stats().reset();                                                                                                                //Natural: RESET ICW-EFM-STATS
                ldaIefl9031.getIcw_Efm_Stats_Log_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),ldaIefl9020.getPnd_Work_Record_Log_Dte());                           //Natural: MOVE EDITED #WORK-RECORD.LOG-DTE TO ICW-EFM-STATS.LOG-DTE ( EM = YYYYMMDD )
                ldaIefl9031.getIcw_Efm_Stats_System_Cde().setValue(ldaIefl9020.getPnd_Work_Record_System_Cde());                                                          //Natural: ASSIGN ICW-EFM-STATS.SYSTEM-CDE = #WORK-RECORD.SYSTEM-CDE
                pnd_Actn_Cnt.reset();                                                                                                                                     //Natural: RESET #ACTN-CNT
            }                                                                                                                                                             //Natural: END-IF
            if (condition((ldaIefl9020.getPnd_Work_Record_Log_Dte().notEquals(pnd_Old_Log_Dte)) || (ldaIefl9020.getPnd_Work_Record_System_Cde().notEquals(pnd_Old_System_Cde)))) //Natural: IF ( #WORK-RECORD.LOG-DTE NE #OLD.LOG-DTE ) OR ( #WORK-RECORD.SYSTEM-CDE NE #OLD.SYSTEM-CDE )
            {
                ldaIefl9031.getVw_icw_Efm_Stats().insertDBRow();                                                                                                          //Natural: STORE ICW-EFM-STATS
                GET_CNTL:                                                                                                                                                 //Natural: GET ICW-EFM-STATS #CNTL-ISN
                ldaIefl9031.getVw_icw_Efm_Stats().readByID(pnd_Cntl_Isn.getLong(), "GET_CNTL");
                ldaIefl9031.getIcw_Efm_Stats_Xtn_Cnt().getValue(1).setValue(pnd_Records_Processed);                                                                       //Natural: ASSIGN ICW-EFM-STATS.XTN-CNT ( 1 ) = #RECORDS-PROCESSED
                ldaIefl9031.getVw_icw_Efm_Stats().updateDBRow("GET_CNTL");                                                                                                //Natural: UPDATE ( GET-CNTL. )
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                ldaIefl9031.getVw_icw_Efm_Stats().reset();                                                                                                                //Natural: RESET ICW-EFM-STATS
                ldaIefl9031.getIcw_Efm_Stats_Log_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),ldaIefl9020.getPnd_Work_Record_Log_Dte());                           //Natural: MOVE EDITED #WORK-RECORD.LOG-DTE TO ICW-EFM-STATS.LOG-DTE ( EM = YYYYMMDD )
                ldaIefl9031.getIcw_Efm_Stats_System_Cde().setValue(ldaIefl9020.getPnd_Work_Record_System_Cde());                                                          //Natural: ASSIGN ICW-EFM-STATS.SYSTEM-CDE = #WORK-RECORD.SYSTEM-CDE
                pnd_Old_Log_Dte.setValue(ldaIefl9020.getPnd_Work_Record_Log_Dte());                                                                                       //Natural: ASSIGN #OLD.LOG-DTE = #WORK-RECORD.LOG-DTE
                pnd_Old_System_Cde.setValue(ldaIefl9020.getPnd_Work_Record_System_Cde());                                                                                 //Natural: ASSIGN #OLD.SYSTEM-CDE = #WORK-RECORD.SYSTEM-CDE
                pnd_Actn_Cnt.reset();                                                                                                                                     //Natural: RESET #ACTN-CNT
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.examine(new ExamineSource(ldaIefl9031.getIcw_Efm_Stats_Action_Cde().getValue("*"),true), new ExamineSearch(ldaIefl9020.getPnd_Work_Record_Action_Cde(),  //Natural: EXAMINE FULL ICW-EFM-STATS.ACTION-CDE ( * ) FOR FULL #WORK-RECORD.ACTION-CDE GIVING INDEX #I
                true), new ExamineGivingIndex(pnd_I));
            if (condition(pnd_I.equals(getZero())))                                                                                                                       //Natural: IF #I = 0
            {
                pnd_Actn_Cnt.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #ACTN-CNT
                pnd_I.setValue(pnd_Actn_Cnt);                                                                                                                             //Natural: ASSIGN #I = #ACTN-CNT
            }                                                                                                                                                             //Natural: END-IF
            ldaIefl9031.getIcw_Efm_Stats_Action_Cde().getValue(pnd_I).setValue(ldaIefl9020.getPnd_Work_Record_Action_Cde());                                              //Natural: ASSIGN ICW-EFM-STATS.ACTION-CDE ( #I ) = #WORK-RECORD.ACTION-CDE
            if (condition(ldaIefl9020.getPnd_Work_Record_Recursive_Ind().equals(" ")))                                                                                    //Natural: IF #WORK-RECORD.RECURSIVE-IND = ' '
            {
                ldaIefl9031.getIcw_Efm_Stats_Xtn_Cnt().getValue(pnd_I).nadd(1);                                                                                           //Natural: ADD 1 TO ICW-EFM-STATS.XTN-CNT ( #I )
            }                                                                                                                                                             //Natural: END-IF
            ldaIefl9031.getIcw_Efm_Stats_Mit_Added_Cnt().getValue(pnd_I).nadd(ldaIefl9020.getPnd_Work_Record_Mit_Added_Cnt());                                            //Natural: ADD #WORK-RECORD.MIT-ADDED-CNT TO ICW-EFM-STATS.MIT-ADDED-CNT ( #I )
            ldaIefl9031.getIcw_Efm_Stats_Mit_Updated_Cnt().getValue(pnd_I).nadd(ldaIefl9020.getPnd_Work_Record_Mit_Updated_Cnt());                                        //Natural: ADD #WORK-RECORD.MIT-UPDATED-CNT TO ICW-EFM-STATS.MIT-UPDATED-CNT ( #I )
            ldaIefl9031.getIcw_Efm_Stats_Dcmnt_Added_Cnt().getValue(pnd_I).nadd(ldaIefl9020.getPnd_Work_Record_Dcmnt_Added_Cnt());                                        //Natural: ADD #WORK-RECORD.DCMNT-ADDED-CNT TO ICW-EFM-STATS.DCMNT-ADDED-CNT ( #I )
            ldaIefl9031.getIcw_Efm_Stats_Dcmnt_Renamed_Cnt().getValue(pnd_I).nadd(ldaIefl9020.getPnd_Work_Record_Dcmnt_Renamed_Cnt());                                    //Natural: ADD #WORK-RECORD.DCMNT-RENAMED-CNT TO ICW-EFM-STATS.DCMNT-RENAMED-CNT ( #I )
            pnd_Records_Processed.nadd(1);                                                                                                                                //Natural: ADD 1 TO #RECORDS-PROCESSED
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  VALID RECORD PRESENT
        if (condition((ldaIefl9031.getIcw_Efm_Stats_System_Cde().notEquals(" ")) && (ldaIefl9031.getIcw_Efm_Stats_Log_Dte().notEquals(getZero()))))                       //Natural: IF ( ICW-EFM-STATS.SYSTEM-CDE NE ' ' ) AND ( ICW-EFM-STATS.LOG-DTE NE 0 )
        {
            ldaIefl9031.getVw_icw_Efm_Stats().insertDBRow();                                                                                                              //Natural: STORE ICW-EFM-STATS
            GET_CNTL2:                                                                                                                                                    //Natural: GET ICW-EFM-STATS #CNTL-ISN
            ldaIefl9031.getVw_icw_Efm_Stats().readByID(pnd_Cntl_Isn.getLong(), "GET_CNTL2");
            ldaIefl9031.getIcw_Efm_Stats_Xtn_Cnt().getValue(1).setValue(pnd_Records_Processed);                                                                           //Natural: ASSIGN ICW-EFM-STATS.XTN-CNT ( 1 ) = #RECORDS-PROCESSED
            ldaIefl9031.getVw_icw_Efm_Stats().updateDBRow("GET_CNTL2");                                                                                                   //Natural: UPDATE ( GET-CNTL2. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( REPT )
        //*                                                                                                                                                               //Natural: ON ERROR
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(3, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Ieff9034.class));                                              //Natural: WRITE ( REPT ) NOTITLE NOHDR USING FORM 'IEFF9034'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(2, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Efsf9030.class));                                                          //Natural: WRITE ( ABORT ) NOTITLE NOHDR USING FORM 'EFSF9030'
        DbsUtil.terminate(24);  if (true) return;                                                                                                                         //Natural: TERMINATE 24
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(2, "LS=132 PS=60 ZP=OFF SG=OFF");
        Global.format(3, "LS=132 PS=60 ZP=OFF SG=OFF");
    }
}
