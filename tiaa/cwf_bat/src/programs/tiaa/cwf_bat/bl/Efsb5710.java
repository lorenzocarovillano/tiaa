/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:07:17 PM
**        * FROM NATURAL PROGRAM : Efsb5710
************************************************************
**        * FILE NAME            : Efsb5710.java
**        * CLASS NAME           : Efsb5710
**        * INSTANCE NAME        : Efsb5710
************************************************************
************************************************************************
* PROGRAM  : EFSB5710
* SYSTEM   : CWF
* TITLE    : EFM ARCHIVING - DELETE ACTIVE RECORDS FROM DB045
*            - FOLDERS   - FILE 081 - CWF-EFM-FOLDER
*            - DOCUMENTS - FILE 082 - CWF-EFM-DOCUMENT
*            - TEXT      - FILE 083 - CWF-EFM-TEXT-OBJECT
* WRITTEN  : FEBRUARY 1997
* FUNCTION : 1. READ CWF-SUPPORT-TBL ON DB045 FOR STATS
*            2. READ 'Work' FILE CREATED IN EFSB5700
*            3. UPDATE DB045
*               . DELETE RECORDS BY FILE AND ISN
*               . UPDATE CABINETS (FILE 080) & CWF-IMAGE-XREF (FILE 186)
*            4. UPDATE CWF-SUPPORT-TBL ON DB045 WITH LAST PROCESS
*            5. REPORT TOTALS PROCESSED; COMPARE TO ARCHIVE CTL RECORD
* HISTORY  :
* --------
* 04/28/97 JHH - CHANGED #ET-TOT TO P9
* 04/29/97 JHH - DON't reconcile Text and Image totals at EOJ
* 04/30/97 JHH - PRINT 'Inp' TOTALS AT EOJ
* 05/19/97 JHH - TEST FOR DUPLICATE ISN IN FILE 082 (DOCUMENTS)
* 05/27/97 JHH - DON't check Document 'JOB' totals in EOJ reconciliation
* 06/06/97 JHH - TEST FOR DUPLICATE ISN IN FILE 081 (FOLDERS)
* 06/06/97 JHH - DON't check Folder 'JOB' totals in EOJ reconciliation
* 07/07/97 JHH - REMOVE ALL CHECKS FOR 'Job' TOTALS AT EOJ
* 08/13/97 JHH - ELIMINATE PRE-FETCH IN //DDCARD OF JOB P9920CWD
*              - CHANGE READ TO FIND IN IMAGE (FILE 186) ROUTINE
* 02/18/98 BE  - ARCHIVE FOLDERS, DOCUMENTS AND TEXTS IF MIT IS NOT
*                PRESENT.  (MIT NOT PRESENT = MIT ARCHIVED)
* 06/10/03 LE  - ACCOUNT FOR DUPLICATES AFTER READ WORK FILE ADDED
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsb5710 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Tbl;
    private DbsField cwf_Tbl_Tbl_Data_Field;
    private DbsField cwf_Tbl_Tbl_Updte_Dte_Tme;
    private DbsField cwf_Tbl_Tbl_Updte_Dte;
    private DbsField cwf_Tbl_Tbl_Updte_Oprtr_Cde;
    private DbsField pnd_Tbl_Prime_Key;
    private DbsField pnd_Tbl_Data_Field;

    private DbsGroup pnd_Tbl_Data_Field__R_Field_1;
    private DbsField pnd_Tbl_Data_Field_Pnd_Tbl_Run_Dte;
    private DbsField pnd_Tbl_Data_Field_Pnd_Fill1;
    private DbsField pnd_Tbl_Data_Field_Pnd_Run_Limit_Cnt;
    private DbsField pnd_Tbl_Data_Field_Pnd_Fill1a;
    private DbsField pnd_Tbl_Data_Field_Pnd_Run_Limit_Tme;
    private DbsField pnd_Tbl_Data_Field_Pnd_Fill2;
    private DbsField pnd_Tbl_Data_Field_Pnd_Et_Limit;
    private DbsField pnd_Tbl_Data_Field_Pnd_Fill3;
    private DbsField pnd_Tbl_Data_Field_Pnd_080_Run;
    private DbsField pnd_Tbl_Data_Field_Pnd_Fill4;
    private DbsField pnd_Tbl_Data_Field_Pnd_080_Tot;
    private DbsField pnd_Tbl_Data_Field_Pnd_Fill5;
    private DbsField pnd_Tbl_Data_Field_Pnd_081_Run;
    private DbsField pnd_Tbl_Data_Field_Pnd_Fill6;
    private DbsField pnd_Tbl_Data_Field_Pnd_081_Tot;
    private DbsField pnd_Tbl_Data_Field_Pnd_Fill7;
    private DbsField pnd_Tbl_Data_Field_Pnd_082_Run;
    private DbsField pnd_Tbl_Data_Field_Pnd_Fill8;
    private DbsField pnd_Tbl_Data_Field_Pnd_082_Tot;
    private DbsField pnd_Tbl_Data_Field_Pnd_Fill9;
    private DbsField pnd_Tbl_Data_Field_Pnd_186_Run;
    private DbsField pnd_Tbl_Data_Field_Pnd_Fill10;
    private DbsField pnd_Tbl_Data_Field_Pnd_186_Tot;
    private DbsField pnd_Tbl_Data_Field_Pnd_Fill11;
    private DbsField pnd_Tbl_Data_Field_Pnd_083_Run;
    private DbsField pnd_Tbl_Data_Field_Pnd_Fill12;
    private DbsField pnd_Tbl_Data_Field_Pnd_083_Tot;
    private DbsField pnd_Tbl_Data_Field_Pnd_Fill13;
    private DbsField pnd_Tbl_Data_Field_Pnd_Tbl_Work;

    private DbsGroup pnd_Tbl_Data_Field__R_Field_2;
    private DbsField pnd_Tbl_Data_Field_Pnd_Last_Rec_Key;

    private DbsGroup pnd_Tbl_Data_Field__R_Field_3;
    private DbsField pnd_Tbl_Data_Field_Pnd_Last_File;
    private DbsField pnd_Tbl_Data_Field_Pnd_Last_Isn;
    private DbsField pnd_Tbl_Data_Field_Pnd_Last_Key;
    private DbsField pnd_Tbl_Data_Field_Pnd_Last_Upd;

    private DbsGroup pnd_Tbl_Data_Field__R_Field_4;
    private DbsField pnd_Tbl_Data_Field_Pnd_Tbl1;
    private DbsField pnd_Tbl_Data_Field_Pnd_Tbl2;

    private DataAccessProgramView vw_cwf_Arch_Tbl;
    private DbsField cwf_Arch_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Arch_Tbl__R_Field_5;
    private DbsField cwf_Arch_Tbl_Pnd_Tbl_Cab;
    private DbsField cwf_Arch_Tbl_Pnd_Fill1;
    private DbsField cwf_Arch_Tbl_Pnd_Tbl_Run_Dte_Arch;
    private DbsField cwf_Arch_Tbl_Pnd_Fill2;
    private DbsField cwf_Arch_Tbl_Pnd_Run_Limit_Cnt_Arch;
    private DbsField cwf_Arch_Tbl_Pnd_Fill2a;
    private DbsField cwf_Arch_Tbl_Pnd_Run_Limit_Tme_Arch;
    private DbsField cwf_Arch_Tbl_Pnd_Fill1a;
    private DbsField cwf_Arch_Tbl_Pnd_Et_Limit_Arch;
    private DbsField cwf_Arch_Tbl_Pnd_Fill2b;
    private DbsField cwf_Arch_Tbl_Pnd_080_Run_Arch;
    private DbsField cwf_Arch_Tbl_Pnd_Fill2c;
    private DbsField cwf_Arch_Tbl_Pnd_080_Tot_Arch;
    private DbsField cwf_Arch_Tbl_Pnd_Fill2d;
    private DbsField cwf_Arch_Tbl_Pnd_Mit_Run_Arch;
    private DbsField cwf_Arch_Tbl_Pnd_Fill2e;
    private DbsField cwf_Arch_Tbl_Pnd_Mit_Tot_Arch;
    private DbsField cwf_Arch_Tbl_Pnd_Fill3;
    private DbsField cwf_Arch_Tbl_Pnd_Fld_Not_Run;
    private DbsField cwf_Arch_Tbl_Pnd_Fill4;
    private DbsField cwf_Arch_Tbl_Pnd_Fld_Not_Tot;
    private DbsField cwf_Arch_Tbl_Pnd_Fill5;
    private DbsField cwf_Arch_Tbl_Pnd_081_Run_Arch;
    private DbsField cwf_Arch_Tbl_Pnd_Fill6;
    private DbsField cwf_Arch_Tbl_Pnd_081_Tot_Arch;
    private DbsField cwf_Arch_Tbl_Pnd_Fill7;
    private DbsField cwf_Arch_Tbl_Pnd_082_Run_Arch;
    private DbsField cwf_Arch_Tbl_Pnd_Fill8;
    private DbsField cwf_Arch_Tbl_Pnd_082_Tot_Arch;
    private DbsField cwf_Arch_Tbl_Pnd_Fill9;
    private DbsField cwf_Arch_Tbl_Pnd_186_Run_Arch;
    private DbsField cwf_Arch_Tbl_Pnd_Fill10;
    private DbsField cwf_Arch_Tbl_Pnd_186_Tot_Arch;
    private DbsField cwf_Arch_Tbl_Pnd_Fill11;
    private DbsField cwf_Arch_Tbl_Pnd_Kdo_Not_Run;
    private DbsField cwf_Arch_Tbl_Pnd_Fill12;
    private DbsField cwf_Arch_Tbl_Pnd_Kdo_Not_Tot;
    private DbsField cwf_Arch_Tbl_Pnd_Fill13;
    private DbsField cwf_Arch_Tbl_Pnd_083_Run_Arch;
    private DbsField cwf_Arch_Tbl_Pnd_Fill14;
    private DbsField cwf_Arch_Tbl_Pnd_083_Tot_Arch;
    private DbsField cwf_Arch_Tbl_Tbl_Updte_Dte_Tme;
    private DbsField cwf_Arch_Tbl_Tbl_Updte_Dte;
    private DbsField cwf_Arch_Tbl_Tbl_Updte_Oprtr_Cde;
    private DbsField pnd_Tbl_Arch_Prime_Key;

    private DataAccessProgramView vw_cwf_Cab;
    private DbsField cwf_Cab_Cabinet_Id;

    private DbsGroup cwf_Cab__R_Field_6;
    private DbsField cwf_Cab_Pnd_Cab_Pfx;
    private DbsField cwf_Cab_Pnd_Pin_Nbr;
    private DbsField cwf_Cab_Active_Fldrs_Ind;

    private DataAccessProgramView vw_cwf_Fldr;
    private DbsField cwf_Fldr_Folder_Key;
    private DbsField pnd_Dcmt_Key;

    private DbsGroup pnd_Dcmt_Key__R_Field_7;
    private DbsField pnd_Dcmt_Key_Pnd_Fldr_Key;

    private DbsGroup pnd_Dcmt_Key__R_Field_8;
    private DbsField pnd_Dcmt_Key_Pnd_Cab_Id;
    private DbsField pnd_Dcmt_Key_Pnd_Tiaa_Rcvd_Dte;
    private DbsField pnd_Dcmt_Key_Pnd_Case_Wpid_Etc;
    private DbsField pnd_Dcmt_Key_Pnd_Entry_Dte_Tme;

    private DataAccessProgramView vw_cwf_Dcmt;
    private DbsField cwf_Dcmt_Document_Key;

    private DbsGroup cwf_Dcmt__R_Field_9;
    private DbsField cwf_Dcmt_Pnd_Cab_Id;

    private DataAccessProgramView vw_cwf_Txt_Obj;
    private DbsField cwf_Txt_Obj_Text_Object_Key;
    private DbsField pnd_Text_Key;

    private DbsGroup pnd_Text_Key__R_Field_10;
    private DbsField pnd_Text_Key_Pnd_Cabinet;
    private DbsField pnd_Text_Key_Pnd_Text_Ptr;
    private DbsField pnd_Text_Key_Pnd_Seq_Nbr;

    private DataAccessProgramView vw_cwf_Image_Xref;
    private DbsField cwf_Image_Xref_Mail_Item_Nbr;
    private DbsField cwf_Image_Xref_Source_Id;
    private DbsField cwf_Image_Xref_Image_Delete_Ind;
    private DbsField cwf_Image_Xref_Archive_Dte_Tme;
    private DbsField pnd_Source_Id_Min_Key;

    private DbsGroup pnd_Source_Id_Min_Key__R_Field_11;
    private DbsField pnd_Source_Id_Min_Key_Pnd_Source_Id;
    private DbsField pnd_Source_Id_Min_Key_Pnd_Mail_Item_Nbr;
    private DbsField pnd_Work_Cnt;
    private DbsField pnd_Work_Spin;
    private DbsField pnd_Tme_Begin;
    private DbsField pnd_Tme_End;
    private DbsField pnd_Isn_Tbl;
    private DbsField pnd_Et_Cnt;
    private DbsField pnd_Et_Tot;
    private DbsField pnd_Eoj;
    private DbsField pnd_Pge_Cnt;
    private DbsField pnd_081_Isn;
    private DbsField pnd_082_Isn;
    private DbsField pnd_083_Isn;
    private DbsField pnd_080_Inp;
    private DbsField pnd_186_Inp;
    private DbsField pnd_081_Inp;
    private DbsField pnd_082_Inp;
    private DbsField pnd_083_Inp;
    private DbsField pnd_186_Dup;
    private DbsField pnd_081_Dup;
    private DbsField pnd_082_Dup;
    private DbsField pnd_083_Dup;
    private DbsField pnd_Work;

    private DbsGroup pnd_Work__R_Field_12;
    private DbsField pnd_Work_Pnd_Work_Rec_Key;

    private DbsGroup pnd_Work__R_Field_13;
    private DbsField pnd_Work_Pnd_Work_File;
    private DbsField pnd_Work_Pnd_Work_Isn;
    private DbsField pnd_Work_Pnd_Work_Key;

    private DbsGroup pnd_Work__R_Field_14;
    private DbsField pnd_Work_Pnd_Work_Key_186;
    private DbsField pnd_Work_Pnd_Work_Upd;
    private DbsField pnd_Tbl_Data;
    private DbsField pnd_Tbl_Und;
    private DbsField pnd_Head1;
    private DbsField pnd_Head2;

    private DbsGroup pnd_Prt1_Lne;
    private DbsField pnd_Prt1_Lne_P_Cab;
    private DbsField pnd_Prt1_Lne_P_080;
    private DbsField pnd_Prt1_Lne_P_081;
    private DbsField pnd_Prt1_Lne_P_082;
    private DbsField pnd_Prt1_Lne_P_186;
    private DbsField pnd_Prt1_Lne_P_083;

    private DbsRecord setTimeRecord;
    private DbsField st;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Tbl", "CWF-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Tbl_Tbl_Data_Field = vw_cwf_Tbl.getRecord().newFieldInGroup("cwf_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 253, RepeatingFieldStrategy.None, 
            "TBL_DATA_FIELD");
        cwf_Tbl_Tbl_Updte_Dte_Tme = vw_cwf_Tbl.getRecord().newFieldInGroup("cwf_Tbl_Tbl_Updte_Dte_Tme", "TBL-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TBL_UPDTE_DTE_TME");
        cwf_Tbl_Tbl_Updte_Dte_Tme.setDdmHeader("UPDATE DATE/TIME");
        cwf_Tbl_Tbl_Updte_Dte = vw_cwf_Tbl.getRecord().newFieldInGroup("cwf_Tbl_Tbl_Updte_Dte", "TBL-UPDTE-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TBL_UPDTE_DTE");
        cwf_Tbl_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        cwf_Tbl_Tbl_Updte_Oprtr_Cde = vw_cwf_Tbl.getRecord().newFieldInGroup("cwf_Tbl_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Tbl_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        registerRecord(vw_cwf_Tbl);

        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);
        pnd_Tbl_Data_Field = localVariables.newFieldInRecord("pnd_Tbl_Data_Field", "#TBL-DATA-FIELD", FieldType.STRING, 253);

        pnd_Tbl_Data_Field__R_Field_1 = localVariables.newGroupInRecord("pnd_Tbl_Data_Field__R_Field_1", "REDEFINE", pnd_Tbl_Data_Field);
        pnd_Tbl_Data_Field_Pnd_Tbl_Run_Dte = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Tbl_Run_Dte", "#TBL-RUN-DTE", FieldType.STRING, 
            10);
        pnd_Tbl_Data_Field_Pnd_Fill1 = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Fill1", "#FILL1", FieldType.STRING, 1);
        pnd_Tbl_Data_Field_Pnd_Run_Limit_Cnt = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Run_Limit_Cnt", "#RUN-LIMIT-CNT", 
            FieldType.NUMERIC, 7);
        pnd_Tbl_Data_Field_Pnd_Fill1a = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Fill1a", "#FILL1A", FieldType.STRING, 1);
        pnd_Tbl_Data_Field_Pnd_Run_Limit_Tme = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Run_Limit_Tme", "#RUN-LIMIT-TME", 
            FieldType.NUMERIC, 7);
        pnd_Tbl_Data_Field_Pnd_Fill2 = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Fill2", "#FILL2", FieldType.STRING, 1);
        pnd_Tbl_Data_Field_Pnd_Et_Limit = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Et_Limit", "#ET-LIMIT", FieldType.NUMERIC, 
            4);
        pnd_Tbl_Data_Field_Pnd_Fill3 = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Fill3", "#FILL3", FieldType.STRING, 1);
        pnd_Tbl_Data_Field_Pnd_080_Run = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_080_Run", "#080-RUN", FieldType.NUMERIC, 
            7);
        pnd_Tbl_Data_Field_Pnd_Fill4 = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Fill4", "#FILL4", FieldType.STRING, 1);
        pnd_Tbl_Data_Field_Pnd_080_Tot = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_080_Tot", "#080-TOT", FieldType.NUMERIC, 
            7);
        pnd_Tbl_Data_Field_Pnd_Fill5 = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Fill5", "#FILL5", FieldType.STRING, 1);
        pnd_Tbl_Data_Field_Pnd_081_Run = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_081_Run", "#081-RUN", FieldType.NUMERIC, 
            7);
        pnd_Tbl_Data_Field_Pnd_Fill6 = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Fill6", "#FILL6", FieldType.STRING, 1);
        pnd_Tbl_Data_Field_Pnd_081_Tot = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_081_Tot", "#081-TOT", FieldType.NUMERIC, 
            7);
        pnd_Tbl_Data_Field_Pnd_Fill7 = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Fill7", "#FILL7", FieldType.STRING, 1);
        pnd_Tbl_Data_Field_Pnd_082_Run = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_082_Run", "#082-RUN", FieldType.NUMERIC, 
            7);
        pnd_Tbl_Data_Field_Pnd_Fill8 = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Fill8", "#FILL8", FieldType.STRING, 1);
        pnd_Tbl_Data_Field_Pnd_082_Tot = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_082_Tot", "#082-TOT", FieldType.NUMERIC, 
            7);
        pnd_Tbl_Data_Field_Pnd_Fill9 = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Fill9", "#FILL9", FieldType.STRING, 1);
        pnd_Tbl_Data_Field_Pnd_186_Run = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_186_Run", "#186-RUN", FieldType.NUMERIC, 
            7);
        pnd_Tbl_Data_Field_Pnd_Fill10 = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Fill10", "#FILL10", FieldType.STRING, 1);
        pnd_Tbl_Data_Field_Pnd_186_Tot = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_186_Tot", "#186-TOT", FieldType.NUMERIC, 
            7);
        pnd_Tbl_Data_Field_Pnd_Fill11 = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Fill11", "#FILL11", FieldType.STRING, 1);
        pnd_Tbl_Data_Field_Pnd_083_Run = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_083_Run", "#083-RUN", FieldType.NUMERIC, 
            7);
        pnd_Tbl_Data_Field_Pnd_Fill12 = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Fill12", "#FILL12", FieldType.STRING, 1);
        pnd_Tbl_Data_Field_Pnd_083_Tot = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_083_Tot", "#083-TOT", FieldType.NUMERIC, 
            7);
        pnd_Tbl_Data_Field_Pnd_Fill13 = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Fill13", "#FILL13", FieldType.STRING, 1);
        pnd_Tbl_Data_Field_Pnd_Tbl_Work = pnd_Tbl_Data_Field__R_Field_1.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Tbl_Work", "#TBL-WORK", FieldType.STRING, 
            49);

        pnd_Tbl_Data_Field__R_Field_2 = pnd_Tbl_Data_Field__R_Field_1.newGroupInGroup("pnd_Tbl_Data_Field__R_Field_2", "REDEFINE", pnd_Tbl_Data_Field_Pnd_Tbl_Work);
        pnd_Tbl_Data_Field_Pnd_Last_Rec_Key = pnd_Tbl_Data_Field__R_Field_2.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Last_Rec_Key", "#LAST-REC-KEY", FieldType.STRING, 
            47);

        pnd_Tbl_Data_Field__R_Field_3 = pnd_Tbl_Data_Field__R_Field_2.newGroupInGroup("pnd_Tbl_Data_Field__R_Field_3", "REDEFINE", pnd_Tbl_Data_Field_Pnd_Last_Rec_Key);
        pnd_Tbl_Data_Field_Pnd_Last_File = pnd_Tbl_Data_Field__R_Field_3.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Last_File", "#LAST-FILE", FieldType.STRING, 
            3);
        pnd_Tbl_Data_Field_Pnd_Last_Isn = pnd_Tbl_Data_Field__R_Field_3.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Last_Isn", "#LAST-ISN", FieldType.NUMERIC, 
            10);
        pnd_Tbl_Data_Field_Pnd_Last_Key = pnd_Tbl_Data_Field__R_Field_3.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Last_Key", "#LAST-KEY", FieldType.STRING, 
            34);
        pnd_Tbl_Data_Field_Pnd_Last_Upd = pnd_Tbl_Data_Field__R_Field_2.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Last_Upd", "#LAST-UPD", FieldType.NUMERIC, 
            2);

        pnd_Tbl_Data_Field__R_Field_4 = localVariables.newGroupInRecord("pnd_Tbl_Data_Field__R_Field_4", "REDEFINE", pnd_Tbl_Data_Field);
        pnd_Tbl_Data_Field_Pnd_Tbl1 = pnd_Tbl_Data_Field__R_Field_4.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Tbl1", "#TBL1", FieldType.STRING, 11);
        pnd_Tbl_Data_Field_Pnd_Tbl2 = pnd_Tbl_Data_Field__R_Field_4.newFieldInGroup("pnd_Tbl_Data_Field_Pnd_Tbl2", "#TBL2", FieldType.STRING, 20);

        vw_cwf_Arch_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Arch_Tbl", "CWF-ARCH-TBL"), "CWF_ARCH_SUPPORT_TBL", "EFM_ARCH_CONTRL");
        cwf_Arch_Tbl_Tbl_Data_Field = vw_cwf_Arch_Tbl.getRecord().newFieldInGroup("cwf_Arch_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 253, 
            RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Arch_Tbl__R_Field_5 = vw_cwf_Arch_Tbl.getRecord().newGroupInGroup("cwf_Arch_Tbl__R_Field_5", "REDEFINE", cwf_Arch_Tbl_Tbl_Data_Field);
        cwf_Arch_Tbl_Pnd_Tbl_Cab = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_Tbl_Cab", "#TBL-CAB", FieldType.STRING, 14);
        cwf_Arch_Tbl_Pnd_Fill1 = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_Fill1", "#FILL1", FieldType.STRING, 1);
        cwf_Arch_Tbl_Pnd_Tbl_Run_Dte_Arch = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_Tbl_Run_Dte_Arch", "#TBL-RUN-DTE-ARCH", FieldType.STRING, 
            10);
        cwf_Arch_Tbl_Pnd_Fill2 = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_Fill2", "#FILL2", FieldType.STRING, 1);
        cwf_Arch_Tbl_Pnd_Run_Limit_Cnt_Arch = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_Run_Limit_Cnt_Arch", "#RUN-LIMIT-CNT-ARCH", FieldType.NUMERIC, 
            7);
        cwf_Arch_Tbl_Pnd_Fill2a = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_Fill2a", "#FILL2A", FieldType.STRING, 1);
        cwf_Arch_Tbl_Pnd_Run_Limit_Tme_Arch = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_Run_Limit_Tme_Arch", "#RUN-LIMIT-TME-ARCH", FieldType.NUMERIC, 
            7);
        cwf_Arch_Tbl_Pnd_Fill1a = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_Fill1a", "#FILL1A", FieldType.STRING, 1);
        cwf_Arch_Tbl_Pnd_Et_Limit_Arch = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_Et_Limit_Arch", "#ET-LIMIT-ARCH", FieldType.NUMERIC, 
            4);
        cwf_Arch_Tbl_Pnd_Fill2b = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_Fill2b", "#FILL2B", FieldType.STRING, 1);
        cwf_Arch_Tbl_Pnd_080_Run_Arch = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_080_Run_Arch", "#080-RUN-ARCH", FieldType.NUMERIC, 7);
        cwf_Arch_Tbl_Pnd_Fill2c = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_Fill2c", "#FILL2C", FieldType.STRING, 1);
        cwf_Arch_Tbl_Pnd_080_Tot_Arch = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_080_Tot_Arch", "#080-TOT-ARCH", FieldType.NUMERIC, 7);
        cwf_Arch_Tbl_Pnd_Fill2d = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_Fill2d", "#FILL2D", FieldType.STRING, 1);
        cwf_Arch_Tbl_Pnd_Mit_Run_Arch = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_Mit_Run_Arch", "#MIT-RUN-ARCH", FieldType.NUMERIC, 7);
        cwf_Arch_Tbl_Pnd_Fill2e = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_Fill2e", "#FILL2E", FieldType.STRING, 1);
        cwf_Arch_Tbl_Pnd_Mit_Tot_Arch = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_Mit_Tot_Arch", "#MIT-TOT-ARCH", FieldType.NUMERIC, 7);
        cwf_Arch_Tbl_Pnd_Fill3 = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_Fill3", "#FILL3", FieldType.STRING, 1);
        cwf_Arch_Tbl_Pnd_Fld_Not_Run = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_Fld_Not_Run", "#FLD-NOT-RUN", FieldType.NUMERIC, 7);
        cwf_Arch_Tbl_Pnd_Fill4 = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_Fill4", "#FILL4", FieldType.STRING, 1);
        cwf_Arch_Tbl_Pnd_Fld_Not_Tot = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_Fld_Not_Tot", "#FLD-NOT-TOT", FieldType.NUMERIC, 7);
        cwf_Arch_Tbl_Pnd_Fill5 = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_Fill5", "#FILL5", FieldType.STRING, 1);
        cwf_Arch_Tbl_Pnd_081_Run_Arch = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_081_Run_Arch", "#081-RUN-ARCH", FieldType.NUMERIC, 7);
        cwf_Arch_Tbl_Pnd_Fill6 = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_Fill6", "#FILL6", FieldType.STRING, 1);
        cwf_Arch_Tbl_Pnd_081_Tot_Arch = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_081_Tot_Arch", "#081-TOT-ARCH", FieldType.NUMERIC, 7);
        cwf_Arch_Tbl_Pnd_Fill7 = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_Fill7", "#FILL7", FieldType.STRING, 1);
        cwf_Arch_Tbl_Pnd_082_Run_Arch = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_082_Run_Arch", "#082-RUN-ARCH", FieldType.NUMERIC, 7);
        cwf_Arch_Tbl_Pnd_Fill8 = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_Fill8", "#FILL8", FieldType.STRING, 1);
        cwf_Arch_Tbl_Pnd_082_Tot_Arch = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_082_Tot_Arch", "#082-TOT-ARCH", FieldType.NUMERIC, 7);
        cwf_Arch_Tbl_Pnd_Fill9 = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_Fill9", "#FILL9", FieldType.STRING, 1);
        cwf_Arch_Tbl_Pnd_186_Run_Arch = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_186_Run_Arch", "#186-RUN-ARCH", FieldType.NUMERIC, 7);
        cwf_Arch_Tbl_Pnd_Fill10 = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_Fill10", "#FILL10", FieldType.STRING, 1);
        cwf_Arch_Tbl_Pnd_186_Tot_Arch = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_186_Tot_Arch", "#186-TOT-ARCH", FieldType.NUMERIC, 7);
        cwf_Arch_Tbl_Pnd_Fill11 = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_Fill11", "#FILL11", FieldType.STRING, 1);
        cwf_Arch_Tbl_Pnd_Kdo_Not_Run = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_Kdo_Not_Run", "#KDO-NOT-RUN", FieldType.NUMERIC, 7);
        cwf_Arch_Tbl_Pnd_Fill12 = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_Fill12", "#FILL12", FieldType.STRING, 1);
        cwf_Arch_Tbl_Pnd_Kdo_Not_Tot = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_Kdo_Not_Tot", "#KDO-NOT-TOT", FieldType.NUMERIC, 7);
        cwf_Arch_Tbl_Pnd_Fill13 = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_Fill13", "#FILL13", FieldType.STRING, 1);
        cwf_Arch_Tbl_Pnd_083_Run_Arch = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_083_Run_Arch", "#083-RUN-ARCH", FieldType.NUMERIC, 7);
        cwf_Arch_Tbl_Pnd_Fill14 = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_Fill14", "#FILL14", FieldType.STRING, 1);
        cwf_Arch_Tbl_Pnd_083_Tot_Arch = cwf_Arch_Tbl__R_Field_5.newFieldInGroup("cwf_Arch_Tbl_Pnd_083_Tot_Arch", "#083-TOT-ARCH", FieldType.NUMERIC, 7);
        cwf_Arch_Tbl_Tbl_Updte_Dte_Tme = vw_cwf_Arch_Tbl.getRecord().newFieldInGroup("cwf_Arch_Tbl_Tbl_Updte_Dte_Tme", "TBL-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE_TME");
        cwf_Arch_Tbl_Tbl_Updte_Dte_Tme.setDdmHeader("UPDATE DATE/TIME");
        cwf_Arch_Tbl_Tbl_Updte_Dte = vw_cwf_Arch_Tbl.getRecord().newFieldInGroup("cwf_Arch_Tbl_Tbl_Updte_Dte", "TBL-UPDTE-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TBL_UPDTE_DTE");
        cwf_Arch_Tbl_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        cwf_Arch_Tbl_Tbl_Updte_Oprtr_Cde = vw_cwf_Arch_Tbl.getRecord().newFieldInGroup("cwf_Arch_Tbl_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Arch_Tbl_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        registerRecord(vw_cwf_Arch_Tbl);

        pnd_Tbl_Arch_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Arch_Prime_Key", "#TBL-ARCH-PRIME-KEY", FieldType.STRING, 53);

        vw_cwf_Cab = new DataAccessProgramView(new NameInfo("vw_cwf_Cab", "CWF-CAB"), "CWF_EFM_CABINET", "CWF_EFM_CABINET");
        cwf_Cab_Cabinet_Id = vw_cwf_Cab.getRecord().newFieldInGroup("cwf_Cab_Cabinet_Id", "CABINET-ID", FieldType.STRING, 14, RepeatingFieldStrategy.None, 
            "CABINET_ID");
        cwf_Cab_Cabinet_Id.setDdmHeader("CABINET/ID");

        cwf_Cab__R_Field_6 = vw_cwf_Cab.getRecord().newGroupInGroup("cwf_Cab__R_Field_6", "REDEFINE", cwf_Cab_Cabinet_Id);
        cwf_Cab_Pnd_Cab_Pfx = cwf_Cab__R_Field_6.newFieldInGroup("cwf_Cab_Pnd_Cab_Pfx", "#CAB-PFX", FieldType.STRING, 1);
        cwf_Cab_Pnd_Pin_Nbr = cwf_Cab__R_Field_6.newFieldInGroup("cwf_Cab_Pnd_Pin_Nbr", "#PIN-NBR", FieldType.NUMERIC, 12);
        cwf_Cab_Active_Fldrs_Ind = vw_cwf_Cab.getRecord().newFieldInGroup("cwf_Cab_Active_Fldrs_Ind", "ACTIVE-FLDRS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ACTIVE_FLDRS_IND");
        registerRecord(vw_cwf_Cab);

        vw_cwf_Fldr = new DataAccessProgramView(new NameInfo("vw_cwf_Fldr", "CWF-FLDR"), "CWF_EFM_FOLDER", "CWF_EFM_FOLDER");
        cwf_Fldr_Folder_Key = vw_cwf_Fldr.getRecord().newFieldInGroup("cwf_Fldr_Folder_Key", "FOLDER-KEY", FieldType.BINARY, 27, RepeatingFieldStrategy.None, 
            "FOLDER_KEY");
        cwf_Fldr_Folder_Key.setDdmHeader("FOLDER/KEY");
        cwf_Fldr_Folder_Key.setSuperDescriptor(true);
        registerRecord(vw_cwf_Fldr);

        pnd_Dcmt_Key = localVariables.newFieldInRecord("pnd_Dcmt_Key", "#DCMT-KEY", FieldType.STRING, 34);

        pnd_Dcmt_Key__R_Field_7 = localVariables.newGroupInRecord("pnd_Dcmt_Key__R_Field_7", "REDEFINE", pnd_Dcmt_Key);
        pnd_Dcmt_Key_Pnd_Fldr_Key = pnd_Dcmt_Key__R_Field_7.newFieldInGroup("pnd_Dcmt_Key_Pnd_Fldr_Key", "#FLDR-KEY", FieldType.STRING, 27);

        pnd_Dcmt_Key__R_Field_8 = pnd_Dcmt_Key__R_Field_7.newGroupInGroup("pnd_Dcmt_Key__R_Field_8", "REDEFINE", pnd_Dcmt_Key_Pnd_Fldr_Key);
        pnd_Dcmt_Key_Pnd_Cab_Id = pnd_Dcmt_Key__R_Field_8.newFieldInGroup("pnd_Dcmt_Key_Pnd_Cab_Id", "#CAB-ID", FieldType.STRING, 14);
        pnd_Dcmt_Key_Pnd_Tiaa_Rcvd_Dte = pnd_Dcmt_Key__R_Field_8.newFieldInGroup("pnd_Dcmt_Key_Pnd_Tiaa_Rcvd_Dte", "#TIAA-RCVD-DTE", FieldType.DATE);
        pnd_Dcmt_Key_Pnd_Case_Wpid_Etc = pnd_Dcmt_Key__R_Field_8.newFieldInGroup("pnd_Dcmt_Key_Pnd_Case_Wpid_Etc", "#CASE-WPID-ETC", FieldType.STRING, 
            9);
        pnd_Dcmt_Key_Pnd_Entry_Dte_Tme = pnd_Dcmt_Key__R_Field_7.newFieldInGroup("pnd_Dcmt_Key_Pnd_Entry_Dte_Tme", "#ENTRY-DTE-TME", FieldType.TIME);

        vw_cwf_Dcmt = new DataAccessProgramView(new NameInfo("vw_cwf_Dcmt", "CWF-DCMT"), "CWF_EFM_DOCUMENT", "CWF_EFM_DOCUMENT");
        cwf_Dcmt_Document_Key = vw_cwf_Dcmt.getRecord().newFieldInGroup("cwf_Dcmt_Document_Key", "DOCUMENT-KEY", FieldType.BINARY, 34, RepeatingFieldStrategy.None, 
            "DOCUMENT_KEY");
        cwf_Dcmt_Document_Key.setDdmHeader("DOCUMENT/KEY");
        cwf_Dcmt_Document_Key.setSuperDescriptor(true);

        cwf_Dcmt__R_Field_9 = vw_cwf_Dcmt.getRecord().newGroupInGroup("cwf_Dcmt__R_Field_9", "REDEFINE", cwf_Dcmt_Document_Key);
        cwf_Dcmt_Pnd_Cab_Id = cwf_Dcmt__R_Field_9.newFieldInGroup("cwf_Dcmt_Pnd_Cab_Id", "#CAB-ID", FieldType.STRING, 14);
        registerRecord(vw_cwf_Dcmt);

        vw_cwf_Txt_Obj = new DataAccessProgramView(new NameInfo("vw_cwf_Txt_Obj", "CWF-TXT-OBJ"), "CWF_EFM_TEXT_OBJECT", "CWF_EMF_TEXT_OBJ");
        cwf_Txt_Obj_Text_Object_Key = vw_cwf_Txt_Obj.getRecord().newFieldInGroup("cwf_Txt_Obj_Text_Object_Key", "TEXT-OBJECT-KEY", FieldType.BINARY, 24, 
            RepeatingFieldStrategy.None, "TEXT_OBJECT_KEY");
        cwf_Txt_Obj_Text_Object_Key.setDdmHeader("TEXT OBJECT/KEY");
        cwf_Txt_Obj_Text_Object_Key.setSuperDescriptor(true);
        registerRecord(vw_cwf_Txt_Obj);

        pnd_Text_Key = localVariables.newFieldInRecord("pnd_Text_Key", "#TEXT-KEY", FieldType.STRING, 24);

        pnd_Text_Key__R_Field_10 = localVariables.newGroupInRecord("pnd_Text_Key__R_Field_10", "REDEFINE", pnd_Text_Key);
        pnd_Text_Key_Pnd_Cabinet = pnd_Text_Key__R_Field_10.newFieldInGroup("pnd_Text_Key_Pnd_Cabinet", "#CABINET", FieldType.STRING, 14);
        pnd_Text_Key_Pnd_Text_Ptr = pnd_Text_Key__R_Field_10.newFieldInGroup("pnd_Text_Key_Pnd_Text_Ptr", "#TEXT-PTR", FieldType.NUMERIC, 7);
        pnd_Text_Key_Pnd_Seq_Nbr = pnd_Text_Key__R_Field_10.newFieldInGroup("pnd_Text_Key_Pnd_Seq_Nbr", "#SEQ-NBR", FieldType.NUMERIC, 3);

        vw_cwf_Image_Xref = new DataAccessProgramView(new NameInfo("vw_cwf_Image_Xref", "CWF-IMAGE-XREF"), "CWF_IMAGE_XREF", "CWF_IMAGE_XREF");
        cwf_Image_Xref_Mail_Item_Nbr = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Mail_Item_Nbr", "MAIL-ITEM-NBR", FieldType.NUMERIC, 
            11, RepeatingFieldStrategy.None, "MAIL_ITEM_NBR");
        cwf_Image_Xref_Mail_Item_Nbr.setDdmHeader("MAIL ITEM/NUMBER");
        cwf_Image_Xref_Source_Id = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Source_Id", "SOURCE-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "SOURCE_ID");
        cwf_Image_Xref_Image_Delete_Ind = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Image_Delete_Ind", "IMAGE-DELETE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "IMAGE_DELETE_IND");
        cwf_Image_Xref_Archive_Dte_Tme = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Archive_Dte_Tme", "ARCHIVE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ARCHIVE_DTE_TME");
        registerRecord(vw_cwf_Image_Xref);

        pnd_Source_Id_Min_Key = localVariables.newFieldInRecord("pnd_Source_Id_Min_Key", "#SOURCE-ID-MIN-KEY", FieldType.STRING, 17);

        pnd_Source_Id_Min_Key__R_Field_11 = localVariables.newGroupInRecord("pnd_Source_Id_Min_Key__R_Field_11", "REDEFINE", pnd_Source_Id_Min_Key);
        pnd_Source_Id_Min_Key_Pnd_Source_Id = pnd_Source_Id_Min_Key__R_Field_11.newFieldInGroup("pnd_Source_Id_Min_Key_Pnd_Source_Id", "#SOURCE-ID", FieldType.STRING, 
            6);
        pnd_Source_Id_Min_Key_Pnd_Mail_Item_Nbr = pnd_Source_Id_Min_Key__R_Field_11.newFieldInGroup("pnd_Source_Id_Min_Key_Pnd_Mail_Item_Nbr", "#MAIL-ITEM-NBR", 
            FieldType.NUMERIC, 11);
        pnd_Work_Cnt = localVariables.newFieldInRecord("pnd_Work_Cnt", "#WORK-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Work_Spin = localVariables.newFieldInRecord("pnd_Work_Spin", "#WORK-SPIN", FieldType.PACKED_DECIMAL, 7);
        pnd_Tme_Begin = localVariables.newFieldInRecord("pnd_Tme_Begin", "#TME-BEGIN", FieldType.TIME);
        pnd_Tme_End = localVariables.newFieldInRecord("pnd_Tme_End", "#TME-END", FieldType.TIME);
        pnd_Isn_Tbl = localVariables.newFieldInRecord("pnd_Isn_Tbl", "#ISN-TBL", FieldType.PACKED_DECIMAL, 10);
        pnd_Et_Cnt = localVariables.newFieldInRecord("pnd_Et_Cnt", "#ET-CNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Et_Tot = localVariables.newFieldInRecord("pnd_Et_Tot", "#ET-TOT", FieldType.PACKED_DECIMAL, 9);
        pnd_Eoj = localVariables.newFieldInRecord("pnd_Eoj", "#EOJ", FieldType.BOOLEAN, 1);
        pnd_Pge_Cnt = localVariables.newFieldInRecord("pnd_Pge_Cnt", "#PGE-CNT", FieldType.PACKED_DECIMAL, 5);
        pnd_081_Isn = localVariables.newFieldInRecord("pnd_081_Isn", "#081-ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_082_Isn = localVariables.newFieldInRecord("pnd_082_Isn", "#082-ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_083_Isn = localVariables.newFieldInRecord("pnd_083_Isn", "#083-ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_080_Inp = localVariables.newFieldInRecord("pnd_080_Inp", "#080-INP", FieldType.PACKED_DECIMAL, 7);
        pnd_186_Inp = localVariables.newFieldInRecord("pnd_186_Inp", "#186-INP", FieldType.PACKED_DECIMAL, 7);
        pnd_081_Inp = localVariables.newFieldInRecord("pnd_081_Inp", "#081-INP", FieldType.PACKED_DECIMAL, 7);
        pnd_082_Inp = localVariables.newFieldInRecord("pnd_082_Inp", "#082-INP", FieldType.PACKED_DECIMAL, 7);
        pnd_083_Inp = localVariables.newFieldInRecord("pnd_083_Inp", "#083-INP", FieldType.PACKED_DECIMAL, 7);
        pnd_186_Dup = localVariables.newFieldInRecord("pnd_186_Dup", "#186-DUP", FieldType.PACKED_DECIMAL, 7);
        pnd_081_Dup = localVariables.newFieldInRecord("pnd_081_Dup", "#081-DUP", FieldType.PACKED_DECIMAL, 7);
        pnd_082_Dup = localVariables.newFieldInRecord("pnd_082_Dup", "#082-DUP", FieldType.PACKED_DECIMAL, 7);
        pnd_083_Dup = localVariables.newFieldInRecord("pnd_083_Dup", "#083-DUP", FieldType.PACKED_DECIMAL, 7);
        pnd_Work = localVariables.newFieldInRecord("pnd_Work", "#WORK", FieldType.STRING, 49);

        pnd_Work__R_Field_12 = localVariables.newGroupInRecord("pnd_Work__R_Field_12", "REDEFINE", pnd_Work);
        pnd_Work_Pnd_Work_Rec_Key = pnd_Work__R_Field_12.newFieldInGroup("pnd_Work_Pnd_Work_Rec_Key", "#WORK-REC-KEY", FieldType.STRING, 47);

        pnd_Work__R_Field_13 = pnd_Work__R_Field_12.newGroupInGroup("pnd_Work__R_Field_13", "REDEFINE", pnd_Work_Pnd_Work_Rec_Key);
        pnd_Work_Pnd_Work_File = pnd_Work__R_Field_13.newFieldInGroup("pnd_Work_Pnd_Work_File", "#WORK-FILE", FieldType.STRING, 3);
        pnd_Work_Pnd_Work_Isn = pnd_Work__R_Field_13.newFieldInGroup("pnd_Work_Pnd_Work_Isn", "#WORK-ISN", FieldType.NUMERIC, 10);
        pnd_Work_Pnd_Work_Key = pnd_Work__R_Field_13.newFieldInGroup("pnd_Work_Pnd_Work_Key", "#WORK-KEY", FieldType.STRING, 34);

        pnd_Work__R_Field_14 = pnd_Work__R_Field_13.newGroupInGroup("pnd_Work__R_Field_14", "REDEFINE", pnd_Work_Pnd_Work_Key);
        pnd_Work_Pnd_Work_Key_186 = pnd_Work__R_Field_14.newFieldInGroup("pnd_Work_Pnd_Work_Key_186", "#WORK-KEY-186", FieldType.STRING, 17);
        pnd_Work_Pnd_Work_Upd = pnd_Work__R_Field_12.newFieldInGroup("pnd_Work_Pnd_Work_Upd", "#WORK-UPD", FieldType.NUMERIC, 2);
        pnd_Tbl_Data = localVariables.newFieldInRecord("pnd_Tbl_Data", "#TBL-DATA", FieldType.STRING, 55);
        pnd_Tbl_Und = localVariables.newFieldInRecord("pnd_Tbl_Und", "#TBL-UND", FieldType.STRING, 65);
        pnd_Head1 = localVariables.newFieldInRecord("pnd_Head1", "#HEAD1", FieldType.STRING, 63);
        pnd_Head2 = localVariables.newFieldInRecord("pnd_Head2", "#HEAD2", FieldType.STRING, 63);

        pnd_Prt1_Lne = localVariables.newGroupInRecord("pnd_Prt1_Lne", "#PRT1-LNE");
        pnd_Prt1_Lne_P_Cab = pnd_Prt1_Lne.newFieldInGroup("pnd_Prt1_Lne_P_Cab", "P-CAB", FieldType.STRING, 9);
        pnd_Prt1_Lne_P_080 = pnd_Prt1_Lne.newFieldInGroup("pnd_Prt1_Lne_P_080", "P-080", FieldType.NUMERIC, 7);
        pnd_Prt1_Lne_P_081 = pnd_Prt1_Lne.newFieldInGroup("pnd_Prt1_Lne_P_081", "P-081", FieldType.NUMERIC, 7);
        pnd_Prt1_Lne_P_082 = pnd_Prt1_Lne.newFieldInGroup("pnd_Prt1_Lne_P_082", "P-082", FieldType.NUMERIC, 7);
        pnd_Prt1_Lne_P_186 = pnd_Prt1_Lne.newFieldInGroup("pnd_Prt1_Lne_P_186", "P-186", FieldType.NUMERIC, 7);
        pnd_Prt1_Lne_P_083 = pnd_Prt1_Lne.newFieldInGroup("pnd_Prt1_Lne_P_083", "P-083", FieldType.NUMERIC, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        setTimeRecord = new DbsRecord();
        st = setTimeRecord.newFieldInRecord("ST", "ST", FieldType.NUMERIC, 7);
        registerRecord(setTimeRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Tbl.reset();
        vw_cwf_Arch_Tbl.reset();
        vw_cwf_Cab.reset();
        vw_cwf_Fldr.reset();
        vw_cwf_Dcmt.reset();
        vw_cwf_Txt_Obj.reset();
        vw_cwf_Image_Xref.reset();

        localVariables.reset();
        pnd_Tbl_Prime_Key.setInitialValue("A CWF-REPORT12-INPUT  EFSB5710");
        pnd_Tbl_Arch_Prime_Key.setInitialValue("A CWF-REPORT12-INPUT  EFSB5700");
        pnd_Eoj.setInitialValue(false);
        pnd_Tbl_Data.setInitialValue("Cnt/Tme Limits  ETs  Last File(3), ISN(10), Key(29) ");
        pnd_Tbl_Und.setInitialValue("------- ------- ---- --------------------------------------------");
        pnd_Head1.setInitialValue("           Cabnets  Folders  Docmnts  Images    Texts ");
        pnd_Head2.setInitialValue("          -------- -------- -------- -------- --------");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Efsb5710() throws Exception
    {
        super("Efsb5710");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  ==================================================
        st = Global.getTIMN();                                                                                                                                            //Natural: FORMAT ( 1 ) LS = 132 PS = 58;//Natural: SET TIME
        //*  ------------------------                                                                                                                                     //Natural: AT TOP OF PAGE ( 1 )
        //*  FILE 183 ON DB045
        vw_cwf_Tbl.startDatabaseFind                                                                                                                                      //Natural: FIND ( 1 ) CWF-TBL WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
        (
        "FIND_TBL",
        new Wc[] { new Wc("TBL_PRIME_KEY", "=", pnd_Tbl_Prime_Key, WcType.WITH) },
        1
        );
        FIND_TBL:
        while (condition(vw_cwf_Tbl.readNextRow("FIND_TBL")))
        {
            vw_cwf_Tbl.setIfNotFoundControlFlag(false);
            pnd_Isn_Tbl.setValue(vw_cwf_Tbl.getAstISN("FIND_TBL"));                                                                                                       //Natural: MOVE *ISN TO #ISN-TBL
            pnd_Tbl_Data_Field.setValue(cwf_Tbl_Tbl_Data_Field);                                                                                                          //Natural: MOVE CWF-TBL.TBL-DATA-FIELD TO #TBL-DATA-FIELD
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        if (condition(pnd_Isn_Tbl.equals(getZero())))                                                                                                                     //Natural: IF #ISN-TBL = 0
        {
            getReports().write(1, ReportOption.NOTITLE,"CWF-SUPPORT-TBL =",pnd_Tbl_Prime_Key,"not found - terminated");                                                   //Natural: WRITE ( 1 ) 'CWF-SUPPORT-TBL =' #TBL-PRIME-KEY 'not found - terminated'
            if (Global.isEscape()) return;
            DbsUtil.terminate(5);  if (true) return;                                                                                                                      //Natural: TERMINATE 05
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Tme_Begin.setValue(Global.getTIMX());                                                                                                                         //Natural: MOVE *TIMX TO #TME-BEGIN #TME-END
        pnd_Tme_End.setValue(Global.getTIMX());
        pnd_Tbl_Data_Field_Pnd_080_Run.reset();                                                                                                                           //Natural: RESET #080-RUN #082-RUN #186-RUN #081-RUN #083-RUN
        pnd_Tbl_Data_Field_Pnd_082_Run.reset();
        pnd_Tbl_Data_Field_Pnd_186_Run.reset();
        pnd_Tbl_Data_Field_Pnd_081_Run.reset();
        pnd_Tbl_Data_Field_Pnd_083_Run.reset();
        if (condition(pnd_Tbl_Data_Field_Pnd_Last_File.equals("XXX")))                                                                                                    //Natural: IF #LAST-FILE = 'XXX'
        {
            pnd_Tbl_Data_Field_Pnd_080_Tot.reset();                                                                                                                       //Natural: RESET #080-TOT #082-TOT #186-TOT #081-TOT #083-TOT
            pnd_Tbl_Data_Field_Pnd_082_Tot.reset();
            pnd_Tbl_Data_Field_Pnd_186_Tot.reset();
            pnd_Tbl_Data_Field_Pnd_081_Tot.reset();
            pnd_Tbl_Data_Field_Pnd_083_Tot.reset();
        }                                                                                                                                                                 //Natural: END-IF
        //*  RESET RUNNING TOTALS IF ANY REACHED 9000000
        if (condition(pnd_Tbl_Data_Field_Pnd_080_Tot.greaterOrEqual(9000000) || pnd_Tbl_Data_Field_Pnd_082_Tot.greaterOrEqual(9000000) || pnd_Tbl_Data_Field_Pnd_186_Tot.greaterOrEqual(9000000)  //Natural: IF #080-TOT GE 9000000 OR #082-TOT GE 9000000 OR #186-TOT GE 9000000 OR #081-TOT GE 9000000 OR #083-TOT GE 9000000
            || pnd_Tbl_Data_Field_Pnd_081_Tot.greaterOrEqual(9000000) || pnd_Tbl_Data_Field_Pnd_083_Tot.greaterOrEqual(9000000)))
        {
            pnd_Tbl_Data_Field_Pnd_080_Tot.reset();                                                                                                                       //Natural: RESET #080-TOT #082-TOT #186-TOT #081-TOT #083-TOT
            pnd_Tbl_Data_Field_Pnd_082_Tot.reset();
            pnd_Tbl_Data_Field_Pnd_186_Tot.reset();
            pnd_Tbl_Data_Field_Pnd_081_Tot.reset();
            pnd_Tbl_Data_Field_Pnd_083_Tot.reset();
            //*  TRIGGER 'AT TOP OF PAGE'
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(1, ReportOption.NOTITLE," ");                                                                                                                  //Natural: WRITE ( 1 ) ' '
        if (Global.isEscape()) return;
        //*  -------------------------
        PROG:                                                                                                                                                             //Natural: REPEAT
        while (condition(whileTrue))
        {
            READ_CAB:                                                                                                                                                     //Natural: READ WORK 2 #WORK
            while (condition(getWorkFiles().read(2, pnd_Work)))
            {
                //*  RESUMING PREVIOUS JOB
                if (condition(pnd_Work_Pnd_Work_Rec_Key.lessOrEqual(pnd_Tbl_Data_Field_Pnd_Last_Rec_Key)))                                                                //Natural: IF #WORK-REC-KEY NOT > #LAST-REC-KEY
                {
                    pnd_Work_Spin.nadd(1);                                                                                                                                //Natural: ADD 1 TO #WORK-SPIN
                    //*  DUPLICATE RECORD L.E.
                    if (condition(pnd_Work_Pnd_Work_Rec_Key.equals(pnd_Tbl_Data_Field_Pnd_Last_Rec_Key)))                                                                 //Natural: IF #WORK-REC-KEY = #LAST-REC-KEY
                    {
                        //*  FOLDERS
                        if (condition(pnd_Work_Pnd_Work_File.equals("081")))                                                                                              //Natural: IF #WORK-FILE = '081'
                        {
                            pnd_081_Inp.nadd(1);                                                                                                                          //Natural: ADD 1 TO #081-INP
                            pnd_081_Dup.nadd(1);                                                                                                                          //Natural: ADD 1 TO #081-DUP
                        }                                                                                                                                                 //Natural: END-IF
                        //*  DOCUMENTS
                        if (condition(pnd_Work_Pnd_Work_File.equals("082")))                                                                                              //Natural: IF #WORK-FILE = '082'
                        {
                            pnd_082_Inp.nadd(1);                                                                                                                          //Natural: ADD 1 TO #082-INP
                            pnd_082_Dup.nadd(1);                                                                                                                          //Natural: ADD 1 TO #082-DUP
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                if (condition((pnd_Tme_End.subtract(pnd_Tme_Begin)).greater(pnd_Tbl_Data_Field_Pnd_Run_Limit_Tme)))                                                       //Natural: IF ( #TME-END - #TME-BEGIN ) > #RUN-LIMIT-TME
                {
                    getReports().write(1, ReportOption.NOTITLE,"Delete Time's up",pnd_Tme_Begin, new ReportEditMask ("YYYYMMDDHHIISST"),pnd_Tme_End, new                  //Natural: WRITE ( 1 ) 'Delete Time"s up' #TME-BEGIN ( EM = YYYYMMDDHHIISST ) #TME-END ( EM = YYYYMMDDHHIISST ) #RUN-LIMIT-TME
                        ReportEditMask ("YYYYMMDDHHIISST"),pnd_Tbl_Data_Field_Pnd_Run_Limit_Tme);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_CAB"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_CAB"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Eoj.setValue(true);                                                                                                                               //Natural: ASSIGN #EOJ = TRUE
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-ET
                    sub_Check_For_Et();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_CAB"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_CAB"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    DbsUtil.terminate(4);  if (true) return;                                                                                                              //Natural: TERMINATE 04
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Et_Tot.greater(pnd_Tbl_Data_Field_Pnd_Run_Limit_Cnt)))                                                                                  //Natural: IF #ET-TOT > #RUN-LIMIT-CNT
                {
                    getReports().write(1, ReportOption.NOTITLE,"Delete Cnt limit",pnd_Tbl_Data_Field_Pnd_Run_Limit_Cnt,"exceeded");                                       //Natural: WRITE ( 1 ) 'Delete Cnt limit' #RUN-LIMIT-CNT 'exceeded'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_CAB"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_CAB"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Eoj.setValue(true);                                                                                                                               //Natural: ASSIGN #EOJ = TRUE
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-ET
                    sub_Check_For_Et();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_CAB"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_CAB"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    DbsUtil.terminate(4);  if (true) return;                                                                                                              //Natural: TERMINATE 04
                }                                                                                                                                                         //Natural: END-IF
                pnd_Work_Cnt.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #WORK-CNT
                pnd_Dcmt_Key.setValue(pnd_Work_Pnd_Work_Key);                                                                                                             //Natural: MOVE #WORK-KEY TO #DCMT-KEY
                //* *  WRITE(1) 'Read work' #WORK
                //*  -------------------------------------------------------
                //*  FOLDER
                if (condition(pnd_Work_Pnd_Work_File.equals("081")))                                                                                                      //Natural: IF #WORK-FILE = '081'
                {
                    pnd_081_Inp.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #081-INP
                    //*  DUPLICATE FLDR ISN
                    if (condition(pnd_Work_Pnd_Work_Isn.equals(pnd_081_Isn)))                                                                                             //Natural: IF #WORK-ISN = #081-ISN
                    {
                        pnd_081_Dup.nadd(1);                                                                                                                              //Natural: ADD 1 TO #081-DUP
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_081_Isn.setValue(pnd_Work_Pnd_Work_Isn);                                                                                                          //Natural: MOVE #WORK-ISN TO #081-ISN
                    GET_FLDR:                                                                                                                                             //Natural: GET CWF-FLDR #WORK-ISN
                    vw_cwf_Fldr.readByID(pnd_Work_Pnd_Work_Isn.getLong(), "GET_FLDR");
                    if (condition(cwf_Fldr_Folder_Key.notEquals(pnd_Dcmt_Key_Pnd_Fldr_Key)))                                                                              //Natural: IF CWF-FLDR.FOLDER-KEY NOT = #DCMT-KEY.#FLDR-KEY
                    {
                        getReports().write(1, ReportOption.NOTITLE,"Bad Fldr",pnd_Work,cwf_Fldr_Folder_Key);                                                              //Natural: WRITE ( 1 ) 'Bad Fldr' #WORK CWF-FLDR.FOLDER-KEY
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_CAB"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_CAB"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        DbsUtil.terminate(8);  if (true) return;                                                                                                          //Natural: TERMINATE 08
                    }                                                                                                                                                     //Natural: END-IF
                    vw_cwf_Fldr.deleteDBRow("GET_FLDR");                                                                                                                  //Natural: DELETE ( GET-FLDR. )
                    pnd_Tbl_Data_Field_Pnd_081_Run.nadd(1);                                                                                                               //Natural: ADD 1 TO #081-RUN
                    pnd_Tbl_Data_Field_Pnd_081_Tot.nadd(1);                                                                                                               //Natural: ADD 1 TO #081-TOT
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-ET
                    sub_Check_For_Et();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_CAB"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_CAB"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*  -------------------------------------------------------
                //*  DOCUMENT
                if (condition(pnd_Work_Pnd_Work_File.equals("082")))                                                                                                      //Natural: IF #WORK-FILE = '082'
                {
                    pnd_082_Inp.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #082-INP
                    //*  DUPLICATE DCMT ISN
                    if (condition(pnd_Work_Pnd_Work_Isn.equals(pnd_082_Isn)))                                                                                             //Natural: IF #WORK-ISN = #082-ISN
                    {
                        pnd_082_Dup.nadd(1);                                                                                                                              //Natural: ADD 1 TO #082-DUP
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_082_Isn.setValue(pnd_Work_Pnd_Work_Isn);                                                                                                          //Natural: MOVE #WORK-ISN TO #082-ISN
                    GET_DCMT:                                                                                                                                             //Natural: GET CWF-DCMT #WORK-ISN
                    vw_cwf_Dcmt.readByID(pnd_Work_Pnd_Work_Isn.getLong(), "GET_DCMT");
                    if (condition(cwf_Dcmt_Document_Key.notEquals(pnd_Dcmt_Key)))                                                                                         //Natural: IF CWF-DCMT.DOCUMENT-KEY NOT = #DCMT-KEY
                    {
                        getReports().write(1, ReportOption.NOTITLE,"Bad Dcmt",pnd_Work,cwf_Dcmt_Document_Key);                                                            //Natural: WRITE ( 1 ) 'Bad Dcmt' #WORK CWF-DCMT.DOCUMENT-KEY
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_CAB"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_CAB"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        DbsUtil.terminate(8);  if (true) return;                                                                                                          //Natural: TERMINATE 08
                    }                                                                                                                                                     //Natural: END-IF
                    vw_cwf_Dcmt.deleteDBRow("GET_DCMT");                                                                                                                  //Natural: DELETE ( GET-DCMT. )
                    pnd_Tbl_Data_Field_Pnd_082_Run.nadd(1);                                                                                                               //Natural: ADD 1 TO #082-RUN
                    pnd_Tbl_Data_Field_Pnd_082_Tot.nadd(1);                                                                                                               //Natural: ADD 1 TO #082-TOT
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-ET
                    sub_Check_For_Et();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_CAB"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_CAB"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*  -------------------------------------------------------
                //*  TEXT OBJECT
                if (condition(pnd_Work_Pnd_Work_File.equals("083")))                                                                                                      //Natural: IF #WORK-FILE = '083'
                {
                    pnd_083_Inp.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #083-INP
                    //*  DUPLICATE TEXT ISN
                    if (condition(pnd_Work_Pnd_Work_Isn.equals(pnd_083_Isn)))                                                                                             //Natural: IF #WORK-ISN = #083-ISN
                    {
                        pnd_083_Dup.nadd(1);                                                                                                                              //Natural: ADD 1 TO #083-DUP
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_083_Isn.setValue(pnd_Work_Pnd_Work_Isn);                                                                                                          //Natural: MOVE #WORK-ISN TO #083-ISN
                    GET_TXT:                                                                                                                                              //Natural: GET CWF-TXT-OBJ #WORK-ISN
                    vw_cwf_Txt_Obj.readByID(pnd_Work_Pnd_Work_Isn.getLong(), "GET_TXT");
                    if (condition(cwf_Txt_Obj_Text_Object_Key.notEquals(pnd_Dcmt_Key)))                                                                                   //Natural: IF CWF-TXT-OBJ.TEXT-OBJECT-KEY NOT = #DCMT-KEY
                    {
                        getReports().write(1, ReportOption.NOTITLE,"Bad Text",pnd_Work,cwf_Txt_Obj_Text_Object_Key);                                                      //Natural: WRITE ( 1 ) 'Bad Text' #WORK CWF-TXT-OBJ.TEXT-OBJECT-KEY
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_CAB"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_CAB"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        DbsUtil.terminate(8);  if (true) return;                                                                                                          //Natural: TERMINATE 08
                    }                                                                                                                                                     //Natural: END-IF
                    vw_cwf_Txt_Obj.deleteDBRow("GET_TXT");                                                                                                                //Natural: DELETE ( GET-TXT. )
                    pnd_Tbl_Data_Field_Pnd_083_Run.nadd(1);                                                                                                               //Natural: ADD 1 TO #083-RUN
                    pnd_Tbl_Data_Field_Pnd_083_Tot.nadd(1);                                                                                                               //Natural: ADD 1 TO #083-TOT
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-ET
                    sub_Check_For_Et();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_CAB"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_CAB"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*  -------------------------------------------------------
                //*  IMAGES
                if (condition(pnd_Work_Pnd_Work_File.equals("186")))                                                                                                      //Natural: IF #WORK-FILE = '186'
                {
                    pnd_186_Inp.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #186-INP
                    //*  DUPLICATE IMAGE POINTER
                    if (condition(pnd_Work_Pnd_Work_Key_186.equals(pnd_Source_Id_Min_Key)))                                                                               //Natural: IF #WORK-KEY-186 = #SOURCE-ID-MIN-KEY
                    {
                        pnd_186_Dup.nadd(1);                                                                                                                              //Natural: ADD 1 TO #186-DUP
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Source_Id_Min_Key.setValue(pnd_Work_Pnd_Work_Key_186);                                                                                            //Natural: MOVE #WORK-KEY-186 TO #SOURCE-ID-MIN-KEY
                    //*  08/13/97 JHH
                    vw_cwf_Image_Xref.startDatabaseFind                                                                                                                   //Natural: FIND CWF-IMAGE-XREF WITH SOURCE-ID-MIN-KEY = #SOURCE-ID-MIN-KEY
                    (
                    "READ_IMG",
                    new Wc[] { new Wc("SOURCE_ID_MIN_KEY", "=", pnd_Source_Id_Min_Key, WcType.WITH) }
                    );
                    READ_IMG:
                    while (condition(vw_cwf_Image_Xref.readNextRow("READ_IMG")))
                    {
                        vw_cwf_Image_Xref.setIfNotFoundControlFlag(false);
                        cwf_Image_Xref_Archive_Dte_Tme.setValue(Global.getTIMX());                                                                                        //Natural: ASSIGN CWF-IMAGE-XREF.ARCHIVE-DTE-TME = *TIMX
                        vw_cwf_Image_Xref.updateDBRow("READ_IMG");                                                                                                        //Natural: UPDATE ( READ-IMG. )
                        pnd_Tbl_Data_Field_Pnd_186_Run.nadd(1);                                                                                                           //Natural: ADD 1 TO #186-RUN
                        pnd_Tbl_Data_Field_Pnd_186_Tot.nadd(1);                                                                                                           //Natural: ADD 1 TO #186-TOT
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-ET
                        sub_Check_For_Et();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_IMG"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_IMG"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-FIND
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_CAB"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_CAB"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*  =======================================================
                getReports().write(1, ReportOption.NOTITLE,"Bad Work",pnd_Work,"File Nbr?");                                                                              //Natural: WRITE ( 1 ) 'Bad Work' #WORK 'File Nbr?'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_CAB"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_CAB"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(8);  if (true) return;                                                                                                                  //Natural: TERMINATE 08
            }                                                                                                                                                             //Natural: END-WORK
            READ_CAB_Exit:
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PROG"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Work.reset();                                                                                                                                             //Natural: RESET #WORK
            pnd_Work_Pnd_Work_File.setValue("EOJ");                                                                                                                       //Natural: MOVE 'EOJ' TO #WORK-FILE
            pnd_Eoj.setValue(true);                                                                                                                                       //Natural: ASSIGN #EOJ = TRUE
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-ET
            sub_Check_For_Et();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PROG"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
            //*  FILE 067 ON DB054
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  =======================================================
        //*  ==================================================
        vw_cwf_Arch_Tbl.startDatabaseFind                                                                                                                                 //Natural: FIND ( 1 ) CWF-ARCH-TBL WITH TBL-PRIME-KEY = #TBL-ARCH-PRIME-KEY
        (
        "FIND01",
        new Wc[] { new Wc("TBL_PRIME_KEY", "=", pnd_Tbl_Arch_Prime_Key, WcType.WITH) },
        1
        );
        FIND01:
        while (condition(vw_cwf_Arch_Tbl.readNextRow("FIND01")))
        {
            vw_cwf_Arch_Tbl.setIfNotFoundControlFlag(false);
            pnd_Prt1_Lne_P_080.setValue(cwf_Arch_Tbl_Pnd_Mit_Run_Arch);                                                                                                   //Natural: MOVE #MIT-RUN-ARCH TO P-080
            pnd_Prt1_Lne_P_081.setValue(cwf_Arch_Tbl_Pnd_081_Run_Arch);                                                                                                   //Natural: MOVE #081-RUN-ARCH TO P-081
            pnd_Prt1_Lne_P_082.setValue(cwf_Arch_Tbl_Pnd_082_Run_Arch);                                                                                                   //Natural: MOVE #082-RUN-ARCH TO P-082
            pnd_Prt1_Lne_P_083.setValue(cwf_Arch_Tbl_Pnd_083_Run_Arch);                                                                                                   //Natural: MOVE #083-RUN-ARCH TO P-083
            pnd_Prt1_Lne_P_186.setValue(cwf_Arch_Tbl_Pnd_186_Run_Arch);                                                                                                   //Natural: MOVE #186-RUN-ARCH TO P-186
            pnd_Prt1_Lne_P_Cab.setValue("Run Archd");                                                                                                                     //Natural: MOVE 'Run Archd' TO P-CAB
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_Prt1_Lne_P_Cab,pnd_Prt1_Lne_P_080,pnd_Prt1_Lne_P_081,pnd_Prt1_Lne_P_082,pnd_Prt1_Lne_P_186,            //Natural: WRITE ( 1 ) / #PRT1-LNE
                pnd_Prt1_Lne_P_083);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Prt1_Lne_P_080.setValue(cwf_Arch_Tbl_Pnd_Mit_Tot_Arch);                                                                                                   //Natural: MOVE #MIT-TOT-ARCH TO P-080
            pnd_Prt1_Lne_P_081.setValue(cwf_Arch_Tbl_Pnd_081_Tot_Arch);                                                                                                   //Natural: MOVE #081-TOT-ARCH TO P-081
            pnd_Prt1_Lne_P_082.setValue(cwf_Arch_Tbl_Pnd_082_Tot_Arch);                                                                                                   //Natural: MOVE #082-TOT-ARCH TO P-082
            pnd_Prt1_Lne_P_083.setValue(cwf_Arch_Tbl_Pnd_083_Tot_Arch);                                                                                                   //Natural: MOVE #083-TOT-ARCH TO P-083
            pnd_Prt1_Lne_P_186.setValue(cwf_Arch_Tbl_Pnd_186_Tot_Arch);                                                                                                   //Natural: MOVE #186-TOT-ARCH TO P-186
            pnd_Prt1_Lne_P_Cab.setValue("Job Archd");                                                                                                                     //Natural: MOVE 'Job Archd' TO P-CAB
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_Prt1_Lne_P_Cab,pnd_Prt1_Lne_P_080,pnd_Prt1_Lne_P_081,pnd_Prt1_Lne_P_082,pnd_Prt1_Lne_P_186,            //Natural: WRITE ( 1 ) / #PRT1-LNE
                pnd_Prt1_Lne_P_083);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* *  IF (#080-INP = #MIT-RUN-ARCH) /* 2/18/98 BE
            //* *       07/07/97 JHH              AND (#080-TOT = #MIT-TOT-ARCH)
            if (condition((pnd_081_Inp.equals(cwf_Arch_Tbl_Pnd_081_Run_Arch)) && (pnd_082_Inp.equals(cwf_Arch_Tbl_Pnd_082_Run_Arch))))                                    //Natural: IF ( #081-INP = #081-RUN-ARCH ) AND ( #082-INP = #082-RUN-ARCH )
            {
                //* *       06/06/97 JHH              AND (#081-TOT = #081-TOT-ARCH)
                //* *       05/27/97 JHH              AND (#082-TOT = #082-TOT-ARCH)
                //* *  AND (#083-INP = #083-RUN-ARCH)
                //* *  AND (#186-INP = #186-RUN-ARCH)
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"Archive totals = Delete totals - O.K. to continue");                                                  //Natural: WRITE ( 1 ) / 'Archive totals = Delete totals - O.K. to continue'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"Archive 'Run' totals not = Delete 'Run' totals -","reconcile before resuming ..");                    //Natural: WRITE ( 1 ) / 'Archive "Run" totals not = Delete "Run" totals -' 'reconcile before resuming ..'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(8);  if (true) return;                                                                                                                  //Natural: TERMINATE 08
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  ----------------------------------
    }
    private void sub_Check_For_Et() throws Exception                                                                                                                      //Natural: CHECK-FOR-ET
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Et_Cnt.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #ET-CNT
        if (condition(pnd_Et_Cnt.greater(pnd_Tbl_Data_Field_Pnd_Et_Limit) || pnd_Eoj.equals(true)))                                                                       //Natural: IF #ET-CNT > #ET-LIMIT OR #EOJ = TRUE
        {
            GET_TBL:                                                                                                                                                      //Natural: GET CWF-TBL #ISN-TBL
            vw_cwf_Tbl.readByID(pnd_Isn_Tbl.getLong(), "GET_TBL");
            pnd_Tbl_Data_Field_Pnd_Tbl_Run_Dte.setValue(Global.getDATU());                                                                                                //Natural: MOVE *DATU TO #TBL-RUN-DTE
            pnd_Tme_End.setValue(Global.getTIMX());                                                                                                                       //Natural: MOVE *TIMX TO #TME-END
            pnd_Tbl_Data_Field_Pnd_Tbl_Work.setValue(pnd_Work);                                                                                                           //Natural: MOVE #WORK TO #TBL-WORK
            cwf_Tbl_Tbl_Data_Field.setValue(pnd_Tbl_Data_Field);                                                                                                          //Natural: MOVE #TBL-DATA-FIELD TO CWF-TBL.TBL-DATA-FIELD
            cwf_Tbl_Tbl_Updte_Dte_Tme.setValue(Global.getTIMX());                                                                                                         //Natural: MOVE *TIMX TO CWF-TBL.TBL-UPDTE-DTE-TME
            cwf_Tbl_Tbl_Updte_Dte.setValue(Global.getDATX());                                                                                                             //Natural: MOVE *DATX TO CWF-TBL.TBL-UPDTE-DTE
            cwf_Tbl_Tbl_Updte_Oprtr_Cde.setValue(Global.getINIT_USER());                                                                                                  //Natural: MOVE *INIT-USER TO CWF-TBL.TBL-UPDTE-OPRTR-CDE
            vw_cwf_Tbl.updateDBRow("GET_TBL");                                                                                                                            //Natural: UPDATE ( GET-TBL. )
            //*  FOR DB 045
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            pnd_Et_Tot.nadd(pnd_Et_Cnt);                                                                                                                                  //Natural: ADD #ET-CNT TO #ET-TOT
            pnd_Et_Cnt.reset();                                                                                                                                           //Natural: RESET #ET-CNT
            pnd_Prt1_Lne_P_080.setValue(pnd_Tbl_Data_Field_Pnd_080_Run);                                                                                                  //Natural: MOVE #080-RUN TO P-080
            pnd_Prt1_Lne_P_081.setValue(pnd_Tbl_Data_Field_Pnd_081_Run);                                                                                                  //Natural: MOVE #081-RUN TO P-081
            pnd_Prt1_Lne_P_082.setValue(pnd_Tbl_Data_Field_Pnd_082_Run);                                                                                                  //Natural: MOVE #082-RUN TO P-082
            pnd_Prt1_Lne_P_083.setValue(pnd_Tbl_Data_Field_Pnd_083_Run);                                                                                                  //Natural: MOVE #083-RUN TO P-083
            pnd_Prt1_Lne_P_186.setValue(pnd_Tbl_Data_Field_Pnd_186_Run);                                                                                                  //Natural: MOVE #186-RUN TO P-186
            pnd_Prt1_Lne_P_Cab.setValue("Run Delet");                                                                                                                     //Natural: MOVE 'Run Delet' TO P-CAB
            getReports().write(1, ReportOption.NOTITLE,pnd_Prt1_Lne_P_Cab,pnd_Prt1_Lne_P_080,pnd_Prt1_Lne_P_081,pnd_Prt1_Lne_P_082,pnd_Prt1_Lne_P_186,                    //Natural: WRITE ( 1 ) #PRT1-LNE
                pnd_Prt1_Lne_P_083);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Eoj.equals(true)))                                                                                                                              //Natural: IF #EOJ = TRUE
        {
            pnd_Prt1_Lne.reset();                                                                                                                                         //Natural: RESET #PRT1-LNE
            pnd_Prt1_Lne_P_081.setValue(pnd_081_Dup);                                                                                                                     //Natural: MOVE #081-DUP TO P-081
            pnd_Prt1_Lne_P_082.setValue(pnd_082_Dup);                                                                                                                     //Natural: MOVE #082-DUP TO P-082
            pnd_Prt1_Lne_P_083.setValue(pnd_083_Dup);                                                                                                                     //Natural: MOVE #083-DUP TO P-083
            pnd_Prt1_Lne_P_186.setValue(pnd_186_Dup);                                                                                                                     //Natural: MOVE #186-DUP TO P-186
            pnd_Prt1_Lne_P_Cab.setValue("Dup Delet");                                                                                                                     //Natural: MOVE 'Dup Delet' TO P-CAB
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_Prt1_Lne_P_Cab,pnd_Prt1_Lne_P_080,pnd_Prt1_Lne_P_081,pnd_Prt1_Lne_P_082,pnd_Prt1_Lne_P_186,            //Natural: WRITE ( 1 ) / #PRT1-LNE
                pnd_Prt1_Lne_P_083);
            if (Global.isEscape()) return;
            pnd_Prt1_Lne_P_080.setValue(pnd_080_Inp);                                                                                                                     //Natural: MOVE #080-INP TO P-080
            pnd_Prt1_Lne_P_081.setValue(pnd_081_Inp);                                                                                                                     //Natural: MOVE #081-INP TO P-081
            pnd_Prt1_Lne_P_082.setValue(pnd_082_Inp);                                                                                                                     //Natural: MOVE #082-INP TO P-082
            pnd_Prt1_Lne_P_083.setValue(pnd_083_Inp);                                                                                                                     //Natural: MOVE #083-INP TO P-083
            pnd_Prt1_Lne_P_186.setValue(pnd_186_Inp);                                                                                                                     //Natural: MOVE #186-INP TO P-186
            pnd_Prt1_Lne_P_Cab.setValue("Inp Delet");                                                                                                                     //Natural: MOVE 'Inp Delet' TO P-CAB
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_Prt1_Lne_P_Cab,pnd_Prt1_Lne_P_080,pnd_Prt1_Lne_P_081,pnd_Prt1_Lne_P_082,pnd_Prt1_Lne_P_186,            //Natural: WRITE ( 1 ) / #PRT1-LNE
                pnd_Prt1_Lne_P_083);
            if (Global.isEscape()) return;
            pnd_Prt1_Lne_P_080.setValue(pnd_Tbl_Data_Field_Pnd_080_Tot);                                                                                                  //Natural: MOVE #080-TOT TO P-080
            pnd_Prt1_Lne_P_081.setValue(pnd_Tbl_Data_Field_Pnd_081_Tot);                                                                                                  //Natural: MOVE #081-TOT TO P-081
            pnd_Prt1_Lne_P_082.setValue(pnd_Tbl_Data_Field_Pnd_082_Tot);                                                                                                  //Natural: MOVE #082-TOT TO P-082
            pnd_Prt1_Lne_P_083.setValue(pnd_Tbl_Data_Field_Pnd_083_Tot);                                                                                                  //Natural: MOVE #083-TOT TO P-083
            pnd_Prt1_Lne_P_186.setValue(pnd_Tbl_Data_Field_Pnd_186_Tot);                                                                                                  //Natural: MOVE #186-TOT TO P-186
            pnd_Prt1_Lne_P_Cab.setValue("Job Delet");                                                                                                                     //Natural: MOVE 'Job Delet' TO P-CAB
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_Prt1_Lne_P_Cab,pnd_Prt1_Lne_P_080,pnd_Prt1_Lne_P_081,pnd_Prt1_Lne_P_082,pnd_Prt1_Lne_P_186,            //Natural: WRITE ( 1 ) / #PRT1-LNE
                pnd_Prt1_Lne_P_083);
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,pnd_Tbl_Data,pnd_Tbl_Data_Field_Pnd_Tbl_Run_Dte);                                                  //Natural: WRITE ( 1 ) // #TBL-DATA #TBL-RUN-DTE
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,pnd_Tbl_Und);                                                                                                      //Natural: WRITE ( 1 ) #TBL-UND
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,pnd_Tbl_Data_Field_Pnd_Tbl2,pnd_Tbl_Data_Field_Pnd_Tbl_Work);                                                      //Natural: WRITE ( 1 ) #TBL2 #TBL-WORK
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,"Run time:",st, new ReportEditMask ("99:99:99:9"));                                                        //Natural: WRITE ( 1 ) / 'Run time:' *TIMD ( ST. ) ( EM = 99:99:99:9 )
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  CHECK-FOR-ET
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    if (condition(pnd_Pge_Cnt.equals(getZero())))                                                                                                         //Natural: IF #PGE-CNT = 0
                    {
                        getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_Tbl_Data,pnd_Tbl_Data_Field_Pnd_Tbl_Run_Dte);                                              //Natural: WRITE ( 1 ) NOTITLE / #TBL-DATA #TBL-RUN-DTE
                        getReports().write(1, ReportOption.NOTITLE,pnd_Tbl_Und);                                                                                          //Natural: WRITE ( 1 ) #TBL-UND
                        getReports().write(1, ReportOption.NOTITLE,pnd_Tbl_Data_Field_Pnd_Tbl2,pnd_Tbl_Data_Field_Pnd_Tbl_Work,NEWLINE,NEWLINE);                          //Natural: WRITE ( 1 ) #TBL2 #TBL-WORK / /
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Pge_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #PGE-CNT
                    getReports().write(1, ReportOption.NOTITLE,Global.getDATU(),"           EFM Archive - Deleted Records                 ",pnd_Pge_Cnt);                 //Natural: WRITE ( 1 ) NOTITLE *DATU '           EFM Archive - Deleted Records                 ' #PGE-CNT
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),"           ----------------------------- ");                                          //Natural: WRITE ( 1 ) *PROGRAM '           ----------------------------- '
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_Head1);                                                                                        //Natural: WRITE ( 1 ) / #HEAD1
                    getReports().write(1, ReportOption.NOTITLE,pnd_Head2);                                                                                                //Natural: WRITE ( 1 ) #HEAD2
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=58");
    }
}
