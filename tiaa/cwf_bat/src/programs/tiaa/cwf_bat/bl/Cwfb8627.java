/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:46:03 PM
**        * FROM NATURAL PROGRAM : Cwfb8627
************************************************************
**        * FILE NAME            : Cwfb8627.java
**        * CLASS NAME           : Cwfb8627
**        * INSTANCE NAME        : Cwfb8627
************************************************************
************************************************************************
*
* SYSTEM   : CORPORATE WORKFLOW
* PROGRAM  : CWFB8626
* TITLE    : SECOND SIGHTED REPORT (REPORT PRINT)
* FUNCTION : READ SORTED WORKFILE CREATED FROM CWF-MASTER-INDEX-VIEW
*          : (045/181); CREATE REPORT
* CREATED  : 10/2008 RSALGADO
* HISTORY  :
*          :
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb8627 extends BLNatBase
{
    // Data Areas
    private PdaNeca4000 pdaNeca4000;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Work_File;

    private DbsGroup pnd_Work_File__R_Field_1;
    private DbsField pnd_Work_File_Pnd_Wf_Wpid;
    private DbsField pnd_Work_File_Pnd_Wf_Pin;
    private DbsField pnd_Work_File_Pnd_Wf_Closed_Stts;
    private DbsField pnd_Work_File_Pnd_Wf_Closed_Dte_Tme_A;

    private DbsGroup pnd_Work_File__R_Field_2;
    private DbsField pnd_Work_File_Pnd_Wf_Closed_Dte;
    private DbsField pnd_Work_File__Filler1;
    private DbsField pnd_Work_File_Pnd_Wf_Rcvd_Dte_A;

    private DbsGroup pnd_Work_File__R_Field_3;
    private DbsField pnd_Work_File_Pnd_Wf_Rcvd_Dte;
    private DbsField pnd_Work_File_Pnd_Wf_Oprtr;
    private DbsField pnd_Work_File_Pnd_Wf_Auth_Queue_Dte_Tme;

    private DbsGroup pnd_Work_File__R_Field_4;
    private DbsField pnd_Work_File_Pnd_Wf_Auth_Queue_Dte;

    private DbsGroup pnd_Work_File__R_Field_5;
    private DbsField pnd_Work_File_Pnd_Wf_Auth_Queue_Dte_A;
    private DbsField pnd_Work_File_Pnd_Wf_Auth_Queue_Tme;
    private DbsField pnd_Work_File__Filler2;
    private DbsField pnd_Work_File_Pnd_Wf_2sight_Oprtr;
    private DbsField pnd_Work_File_Pnd_Wf_2sight_Dte_Tme;

    private DbsGroup pnd_Work_File__R_Field_6;
    private DbsField pnd_Work_File_Pnd_Wf_2sight_Dte;

    private DbsGroup pnd_Work_File__R_Field_7;
    private DbsField pnd_Work_File_Pnd_Wf_2sight_Dte_A;
    private DbsField pnd_Work_File_Pnd_Wf_2sight_Tme;
    private DbsField pnd_Work_File__Filler3;
    private DbsField pnd_Work_File_Pnd_Wf_Return_Oprtr;
    private DbsField pnd_Work_File_Pnd_Wf_Return_Unit;
    private DbsField pnd_Work_File_Pnd_Wf_Return_Stts;
    private DbsField pnd_Work_File_Pnd_Wf_Return_Dte_Tme;

    private DbsGroup pnd_Work_File__R_Field_8;
    private DbsField pnd_Work_File_Pnd_Wf_Return_Dte;
    private DbsField pnd_Work_File_Pnd_Wf_Return_Tme;
    private DbsField pnd_Work_File__Filler4;

    private DbsGroup pnd_Work_File__R_Field_9;
    private DbsField pnd_Work_File__Filler5;
    private DbsField pnd_Work_File_Pnd_Wf_Run;
    private DbsField pnd_Work_File__Filler6;
    private DbsField pnd_Work_File_Pnd_Wf_Start_Dte;

    private DbsGroup pnd_Work_File__R_Field_10;
    private DbsField pnd_Work_File_Pnd_Wf_Start_Yyyy;
    private DbsField pnd_Work_File_Pnd_Wf_Start_Mm;
    private DbsField pnd_Work_File_Pnd_Wf_Start_Dd;
    private DbsField pnd_Work_File__Filler7;
    private DbsField pnd_Work_File_Pnd_Wf_End_Dte;

    private DbsGroup pnd_Work_File__R_Field_11;
    private DbsField pnd_Work_File_Pnd_Wf_End_Yyyy;
    private DbsField pnd_Work_File_Pnd_Wf_End_Mm;
    private DbsField pnd_Work_File_Pnd_Wf_End_Dd;
    private DbsField pnd_H_Start_Dte;
    private DbsField pnd_H_End_Dte;
    private DbsField pnd_H_Report_Month;
    private DbsField pnd_H_Line_2;

    private DbsGroup pnd_Month_Table;
    private DbsField pnd_Month_Table_Pnd_Months1;
    private DbsField pnd_Month_Table_Pnd_Months2;
    private DbsField pnd_Month_Table_Pnd_Months3;
    private DbsField pnd_Month_Table_Pnd_Months4;

    private DbsGroup pnd_Month_Table__R_Field_12;
    private DbsField pnd_Month_Table_Pnd_T_Month;
    private DbsField pnd_Wk_Queue_Date_D;
    private DbsField pnd_Wk_Queue_Dte_A;

    private DbsGroup pnd_Wk_Queue_Dte_A__R_Field_13;
    private DbsField pnd_Wk_Queue_Dte_A_Pnd_Wk_Queue_Dte_N;
    private DbsField pnd_Wk_Auth_Date_D;
    private DbsField pnd_Wk_Bsnss_Days;
    private DbsField pnd_P_Days_Auth;
    private DbsField pnd_Return_Count;
    private DbsField pnd_Unauth_Count;
    private DbsField pnd_Auth_T0;
    private DbsField pnd_Auth_T1;
    private DbsField pnd_Auth_T2;
    private DbsField pnd_Auth_T3;
    private DbsField pnd_Auth_T3plus;
    private DbsField pnd_Auth_Count;
    private DbsField pnd_Auth_Days;
    private DbsField pnd_Tot_Return_Count;
    private DbsField pnd_Tot_Unauth_Count;
    private DbsField pnd_Tot_Auth_T0;
    private DbsField pnd_Tot_Auth_T1;
    private DbsField pnd_Tot_Auth_T2;
    private DbsField pnd_Tot_Auth_T3;
    private DbsField pnd_Tot_Auth_T3plus;
    private DbsField pnd_Tot_Auth_Count;
    private DbsField pnd_Tot_Auth_Days;
    private DbsField pnd_Avg_Auth_Days;
    private DbsField pnd_Write_Count;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Pnd_Wf_2sight_OprtrOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaNeca4000 = new PdaNeca4000(localVariables);

        // Local Variables
        pnd_Work_File = localVariables.newFieldInRecord("pnd_Work_File", "#WORK-FILE", FieldType.STRING, 250);

        pnd_Work_File__R_Field_1 = localVariables.newGroupInRecord("pnd_Work_File__R_Field_1", "REDEFINE", pnd_Work_File);
        pnd_Work_File_Pnd_Wf_Wpid = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wf_Wpid", "#WF-WPID", FieldType.STRING, 6);
        pnd_Work_File_Pnd_Wf_Pin = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wf_Pin", "#WF-PIN", FieldType.NUMERIC, 12);
        pnd_Work_File_Pnd_Wf_Closed_Stts = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wf_Closed_Stts", "#WF-CLOSED-STTS", FieldType.STRING, 
            4);
        pnd_Work_File_Pnd_Wf_Closed_Dte_Tme_A = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wf_Closed_Dte_Tme_A", "#WF-CLOSED-DTE-TME-A", 
            FieldType.STRING, 15);

        pnd_Work_File__R_Field_2 = pnd_Work_File__R_Field_1.newGroupInGroup("pnd_Work_File__R_Field_2", "REDEFINE", pnd_Work_File_Pnd_Wf_Closed_Dte_Tme_A);
        pnd_Work_File_Pnd_Wf_Closed_Dte = pnd_Work_File__R_Field_2.newFieldInGroup("pnd_Work_File_Pnd_Wf_Closed_Dte", "#WF-CLOSED-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_File__Filler1 = pnd_Work_File__R_Field_2.newFieldInGroup("pnd_Work_File__Filler1", "_FILLER1", FieldType.STRING, 7);
        pnd_Work_File_Pnd_Wf_Rcvd_Dte_A = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wf_Rcvd_Dte_A", "#WF-RCVD-DTE-A", FieldType.STRING, 
            8);

        pnd_Work_File__R_Field_3 = pnd_Work_File__R_Field_1.newGroupInGroup("pnd_Work_File__R_Field_3", "REDEFINE", pnd_Work_File_Pnd_Wf_Rcvd_Dte_A);
        pnd_Work_File_Pnd_Wf_Rcvd_Dte = pnd_Work_File__R_Field_3.newFieldInGroup("pnd_Work_File_Pnd_Wf_Rcvd_Dte", "#WF-RCVD-DTE", FieldType.NUMERIC, 8);
        pnd_Work_File_Pnd_Wf_Oprtr = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wf_Oprtr", "#WF-OPRTR", FieldType.STRING, 8);
        pnd_Work_File_Pnd_Wf_Auth_Queue_Dte_Tme = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wf_Auth_Queue_Dte_Tme", "#WF-AUTH-QUEUE-DTE-TME", 
            FieldType.NUMERIC, 15);

        pnd_Work_File__R_Field_4 = pnd_Work_File__R_Field_1.newGroupInGroup("pnd_Work_File__R_Field_4", "REDEFINE", pnd_Work_File_Pnd_Wf_Auth_Queue_Dte_Tme);
        pnd_Work_File_Pnd_Wf_Auth_Queue_Dte = pnd_Work_File__R_Field_4.newFieldInGroup("pnd_Work_File_Pnd_Wf_Auth_Queue_Dte", "#WF-AUTH-QUEUE-DTE", FieldType.NUMERIC, 
            8);

        pnd_Work_File__R_Field_5 = pnd_Work_File__R_Field_4.newGroupInGroup("pnd_Work_File__R_Field_5", "REDEFINE", pnd_Work_File_Pnd_Wf_Auth_Queue_Dte);
        pnd_Work_File_Pnd_Wf_Auth_Queue_Dte_A = pnd_Work_File__R_Field_5.newFieldInGroup("pnd_Work_File_Pnd_Wf_Auth_Queue_Dte_A", "#WF-AUTH-QUEUE-DTE-A", 
            FieldType.STRING, 8);
        pnd_Work_File_Pnd_Wf_Auth_Queue_Tme = pnd_Work_File__R_Field_4.newFieldInGroup("pnd_Work_File_Pnd_Wf_Auth_Queue_Tme", "#WF-AUTH-QUEUE-TME", FieldType.NUMERIC, 
            4);
        pnd_Work_File__Filler2 = pnd_Work_File__R_Field_4.newFieldInGroup("pnd_Work_File__Filler2", "_FILLER2", FieldType.STRING, 3);
        pnd_Work_File_Pnd_Wf_2sight_Oprtr = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wf_2sight_Oprtr", "#WF-2SIGHT-OPRTR", FieldType.STRING, 
            8);
        pnd_Work_File_Pnd_Wf_2sight_Dte_Tme = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wf_2sight_Dte_Tme", "#WF-2SIGHT-DTE-TME", FieldType.STRING, 
            15);

        pnd_Work_File__R_Field_6 = pnd_Work_File__R_Field_1.newGroupInGroup("pnd_Work_File__R_Field_6", "REDEFINE", pnd_Work_File_Pnd_Wf_2sight_Dte_Tme);
        pnd_Work_File_Pnd_Wf_2sight_Dte = pnd_Work_File__R_Field_6.newFieldInGroup("pnd_Work_File_Pnd_Wf_2sight_Dte", "#WF-2SIGHT-DTE", FieldType.NUMERIC, 
            8);

        pnd_Work_File__R_Field_7 = pnd_Work_File__R_Field_6.newGroupInGroup("pnd_Work_File__R_Field_7", "REDEFINE", pnd_Work_File_Pnd_Wf_2sight_Dte);
        pnd_Work_File_Pnd_Wf_2sight_Dte_A = pnd_Work_File__R_Field_7.newFieldInGroup("pnd_Work_File_Pnd_Wf_2sight_Dte_A", "#WF-2SIGHT-DTE-A", FieldType.STRING, 
            8);
        pnd_Work_File_Pnd_Wf_2sight_Tme = pnd_Work_File__R_Field_6.newFieldInGroup("pnd_Work_File_Pnd_Wf_2sight_Tme", "#WF-2SIGHT-TME", FieldType.NUMERIC, 
            4);
        pnd_Work_File__Filler3 = pnd_Work_File__R_Field_6.newFieldInGroup("pnd_Work_File__Filler3", "_FILLER3", FieldType.STRING, 3);
        pnd_Work_File_Pnd_Wf_Return_Oprtr = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wf_Return_Oprtr", "#WF-RETURN-OPRTR", FieldType.STRING, 
            8);
        pnd_Work_File_Pnd_Wf_Return_Unit = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wf_Return_Unit", "#WF-RETURN-UNIT", FieldType.STRING, 
            8);
        pnd_Work_File_Pnd_Wf_Return_Stts = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wf_Return_Stts", "#WF-RETURN-STTS", FieldType.STRING, 
            4);
        pnd_Work_File_Pnd_Wf_Return_Dte_Tme = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wf_Return_Dte_Tme", "#WF-RETURN-DTE-TME", FieldType.NUMERIC, 
            15);

        pnd_Work_File__R_Field_8 = pnd_Work_File__R_Field_1.newGroupInGroup("pnd_Work_File__R_Field_8", "REDEFINE", pnd_Work_File_Pnd_Wf_Return_Dte_Tme);
        pnd_Work_File_Pnd_Wf_Return_Dte = pnd_Work_File__R_Field_8.newFieldInGroup("pnd_Work_File_Pnd_Wf_Return_Dte", "#WF-RETURN-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_File_Pnd_Wf_Return_Tme = pnd_Work_File__R_Field_8.newFieldInGroup("pnd_Work_File_Pnd_Wf_Return_Tme", "#WF-RETURN-TME", FieldType.NUMERIC, 
            4);
        pnd_Work_File__Filler4 = pnd_Work_File__R_Field_8.newFieldInGroup("pnd_Work_File__Filler4", "_FILLER4", FieldType.STRING, 3);

        pnd_Work_File__R_Field_9 = localVariables.newGroupInRecord("pnd_Work_File__R_Field_9", "REDEFINE", pnd_Work_File);
        pnd_Work_File__Filler5 = pnd_Work_File__R_Field_9.newFieldInGroup("pnd_Work_File__Filler5", "_FILLER5", FieldType.STRING, 8);
        pnd_Work_File_Pnd_Wf_Run = pnd_Work_File__R_Field_9.newFieldInGroup("pnd_Work_File_Pnd_Wf_Run", "#WF-RUN", FieldType.STRING, 8);
        pnd_Work_File__Filler6 = pnd_Work_File__R_Field_9.newFieldInGroup("pnd_Work_File__Filler6", "_FILLER6", FieldType.STRING, 1);
        pnd_Work_File_Pnd_Wf_Start_Dte = pnd_Work_File__R_Field_9.newFieldInGroup("pnd_Work_File_Pnd_Wf_Start_Dte", "#WF-START-DTE", FieldType.STRING, 
            8);

        pnd_Work_File__R_Field_10 = pnd_Work_File__R_Field_9.newGroupInGroup("pnd_Work_File__R_Field_10", "REDEFINE", pnd_Work_File_Pnd_Wf_Start_Dte);
        pnd_Work_File_Pnd_Wf_Start_Yyyy = pnd_Work_File__R_Field_10.newFieldInGroup("pnd_Work_File_Pnd_Wf_Start_Yyyy", "#WF-START-YYYY", FieldType.STRING, 
            4);
        pnd_Work_File_Pnd_Wf_Start_Mm = pnd_Work_File__R_Field_10.newFieldInGroup("pnd_Work_File_Pnd_Wf_Start_Mm", "#WF-START-MM", FieldType.NUMERIC, 
            2);
        pnd_Work_File_Pnd_Wf_Start_Dd = pnd_Work_File__R_Field_10.newFieldInGroup("pnd_Work_File_Pnd_Wf_Start_Dd", "#WF-START-DD", FieldType.STRING, 2);
        pnd_Work_File__Filler7 = pnd_Work_File__R_Field_9.newFieldInGroup("pnd_Work_File__Filler7", "_FILLER7", FieldType.STRING, 1);
        pnd_Work_File_Pnd_Wf_End_Dte = pnd_Work_File__R_Field_9.newFieldInGroup("pnd_Work_File_Pnd_Wf_End_Dte", "#WF-END-DTE", FieldType.STRING, 8);

        pnd_Work_File__R_Field_11 = pnd_Work_File__R_Field_9.newGroupInGroup("pnd_Work_File__R_Field_11", "REDEFINE", pnd_Work_File_Pnd_Wf_End_Dte);
        pnd_Work_File_Pnd_Wf_End_Yyyy = pnd_Work_File__R_Field_11.newFieldInGroup("pnd_Work_File_Pnd_Wf_End_Yyyy", "#WF-END-YYYY", FieldType.STRING, 4);
        pnd_Work_File_Pnd_Wf_End_Mm = pnd_Work_File__R_Field_11.newFieldInGroup("pnd_Work_File_Pnd_Wf_End_Mm", "#WF-END-MM", FieldType.NUMERIC, 2);
        pnd_Work_File_Pnd_Wf_End_Dd = pnd_Work_File__R_Field_11.newFieldInGroup("pnd_Work_File_Pnd_Wf_End_Dd", "#WF-END-DD", FieldType.STRING, 2);
        pnd_H_Start_Dte = localVariables.newFieldInRecord("pnd_H_Start_Dte", "#H-START-DTE", FieldType.STRING, 8);
        pnd_H_End_Dte = localVariables.newFieldInRecord("pnd_H_End_Dte", "#H-END-DTE", FieldType.STRING, 8);
        pnd_H_Report_Month = localVariables.newFieldInRecord("pnd_H_Report_Month", "#H-REPORT-MONTH", FieldType.STRING, 10);
        pnd_H_Line_2 = localVariables.newFieldInRecord("pnd_H_Line_2", "#H-LINE-2", FieldType.STRING, 32);

        pnd_Month_Table = localVariables.newGroupInRecord("pnd_Month_Table", "#MONTH-TABLE");
        pnd_Month_Table_Pnd_Months1 = pnd_Month_Table.newFieldInGroup("pnd_Month_Table_Pnd_Months1", "#MONTHS1", FieldType.STRING, 30);
        pnd_Month_Table_Pnd_Months2 = pnd_Month_Table.newFieldInGroup("pnd_Month_Table_Pnd_Months2", "#MONTHS2", FieldType.STRING, 30);
        pnd_Month_Table_Pnd_Months3 = pnd_Month_Table.newFieldInGroup("pnd_Month_Table_Pnd_Months3", "#MONTHS3", FieldType.STRING, 30);
        pnd_Month_Table_Pnd_Months4 = pnd_Month_Table.newFieldInGroup("pnd_Month_Table_Pnd_Months4", "#MONTHS4", FieldType.STRING, 30);

        pnd_Month_Table__R_Field_12 = localVariables.newGroupInRecord("pnd_Month_Table__R_Field_12", "REDEFINE", pnd_Month_Table);
        pnd_Month_Table_Pnd_T_Month = pnd_Month_Table__R_Field_12.newFieldArrayInGroup("pnd_Month_Table_Pnd_T_Month", "#T-MONTH", FieldType.STRING, 10, 
            new DbsArrayController(1, 12));
        pnd_Wk_Queue_Date_D = localVariables.newFieldInRecord("pnd_Wk_Queue_Date_D", "#WK-QUEUE-DATE-D", FieldType.DATE);
        pnd_Wk_Queue_Dte_A = localVariables.newFieldInRecord("pnd_Wk_Queue_Dte_A", "#WK-QUEUE-DTE-A", FieldType.STRING, 8);

        pnd_Wk_Queue_Dte_A__R_Field_13 = localVariables.newGroupInRecord("pnd_Wk_Queue_Dte_A__R_Field_13", "REDEFINE", pnd_Wk_Queue_Dte_A);
        pnd_Wk_Queue_Dte_A_Pnd_Wk_Queue_Dte_N = pnd_Wk_Queue_Dte_A__R_Field_13.newFieldInGroup("pnd_Wk_Queue_Dte_A_Pnd_Wk_Queue_Dte_N", "#WK-QUEUE-DTE-N", 
            FieldType.NUMERIC, 8);
        pnd_Wk_Auth_Date_D = localVariables.newFieldInRecord("pnd_Wk_Auth_Date_D", "#WK-AUTH-DATE-D", FieldType.DATE);
        pnd_Wk_Bsnss_Days = localVariables.newFieldInRecord("pnd_Wk_Bsnss_Days", "#WK-BSNSS-DAYS", FieldType.NUMERIC, 4);
        pnd_P_Days_Auth = localVariables.newFieldInRecord("pnd_P_Days_Auth", "#P-DAYS-AUTH", FieldType.NUMERIC, 4);
        pnd_Return_Count = localVariables.newFieldInRecord("pnd_Return_Count", "#RETURN-COUNT", FieldType.NUMERIC, 7);
        pnd_Unauth_Count = localVariables.newFieldInRecord("pnd_Unauth_Count", "#UNAUTH-COUNT", FieldType.NUMERIC, 7);
        pnd_Auth_T0 = localVariables.newFieldInRecord("pnd_Auth_T0", "#AUTH-T0", FieldType.NUMERIC, 7);
        pnd_Auth_T1 = localVariables.newFieldInRecord("pnd_Auth_T1", "#AUTH-T1", FieldType.NUMERIC, 7);
        pnd_Auth_T2 = localVariables.newFieldInRecord("pnd_Auth_T2", "#AUTH-T2", FieldType.NUMERIC, 7);
        pnd_Auth_T3 = localVariables.newFieldInRecord("pnd_Auth_T3", "#AUTH-T3", FieldType.NUMERIC, 7);
        pnd_Auth_T3plus = localVariables.newFieldInRecord("pnd_Auth_T3plus", "#AUTH-T3PLUS", FieldType.NUMERIC, 7);
        pnd_Auth_Count = localVariables.newFieldInRecord("pnd_Auth_Count", "#AUTH-COUNT", FieldType.NUMERIC, 7);
        pnd_Auth_Days = localVariables.newFieldInRecord("pnd_Auth_Days", "#AUTH-DAYS", FieldType.NUMERIC, 7);
        pnd_Tot_Return_Count = localVariables.newFieldInRecord("pnd_Tot_Return_Count", "#TOT-RETURN-COUNT", FieldType.NUMERIC, 7);
        pnd_Tot_Unauth_Count = localVariables.newFieldInRecord("pnd_Tot_Unauth_Count", "#TOT-UNAUTH-COUNT", FieldType.NUMERIC, 7);
        pnd_Tot_Auth_T0 = localVariables.newFieldInRecord("pnd_Tot_Auth_T0", "#TOT-AUTH-T0", FieldType.NUMERIC, 7);
        pnd_Tot_Auth_T1 = localVariables.newFieldInRecord("pnd_Tot_Auth_T1", "#TOT-AUTH-T1", FieldType.NUMERIC, 7);
        pnd_Tot_Auth_T2 = localVariables.newFieldInRecord("pnd_Tot_Auth_T2", "#TOT-AUTH-T2", FieldType.NUMERIC, 7);
        pnd_Tot_Auth_T3 = localVariables.newFieldInRecord("pnd_Tot_Auth_T3", "#TOT-AUTH-T3", FieldType.NUMERIC, 7);
        pnd_Tot_Auth_T3plus = localVariables.newFieldInRecord("pnd_Tot_Auth_T3plus", "#TOT-AUTH-T3PLUS", FieldType.NUMERIC, 7);
        pnd_Tot_Auth_Count = localVariables.newFieldInRecord("pnd_Tot_Auth_Count", "#TOT-AUTH-COUNT", FieldType.NUMERIC, 7);
        pnd_Tot_Auth_Days = localVariables.newFieldInRecord("pnd_Tot_Auth_Days", "#TOT-AUTH-DAYS", FieldType.NUMERIC, 7);
        pnd_Avg_Auth_Days = localVariables.newFieldInRecord("pnd_Avg_Auth_Days", "#AVG-AUTH-DAYS", FieldType.NUMERIC, 7, 2);
        pnd_Write_Count = localVariables.newFieldInRecord("pnd_Write_Count", "#WRITE-COUNT", FieldType.NUMERIC, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Pnd_Wf_2sight_OprtrOld = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Wf_2sight_Oprtr_OLD", "Pnd_Wf_2sight_Oprtr_OLD", FieldType.STRING, 
            8);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();

        localVariables.reset();
        pnd_Month_Table_Pnd_Months1.setInitialValue("January,  February, March,    ");
        pnd_Month_Table_Pnd_Months2.setInitialValue("April,    May,      June,     ");
        pnd_Month_Table_Pnd_Months3.setInitialValue("July,     August,   September,");
        pnd_Month_Table_Pnd_Months4.setInitialValue("October,  November, December, ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb8627() throws Exception
    {
        super("Cwfb8627");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 00 ) PS = 55 LS = 133;//Natural: FORMAT ( 01 ) PS = 55 LS = 133 ZP = OFF
        getWorkFiles().read(1, pnd_Work_File);                                                                                                                            //Natural: READ WORK 1 ONCE #WORK-FILE
        pnd_H_Start_Dte.setValue(pnd_Work_File_Pnd_Wf_Start_Dte);                                                                                                         //Natural: ASSIGN #H-START-DTE := #WF-START-DTE
        pnd_H_End_Dte.setValue(pnd_Work_File_Pnd_Wf_End_Dte);                                                                                                             //Natural: ASSIGN #H-END-DTE := #WF-END-DTE
        pnd_H_Report_Month.setValue(pnd_Month_Table_Pnd_T_Month.getValue(pnd_Work_File_Pnd_Wf_Start_Mm));                                                                 //Natural: ASSIGN #H-REPORT-MONTH := #T-MONTH ( #WF-START-MM )
        if (condition(pnd_Work_File_Pnd_Wf_Run.equals("CWFB8625")))                                                                                                       //Natural: IF #WF-RUN = 'CWFB8625'
        {
            pnd_H_Line_2.setValue(DbsUtil.compress("From", pnd_Work_File_Pnd_Wf_Start_Mm, "/", pnd_Work_File_Pnd_Wf_Start_Dd, "/", pnd_Work_File_Pnd_Wf_Start_Yyyy,       //Natural: COMPRESS 'From' #WF-START-MM '/' #WF-START-DD '/' #WF-START-YYYY 'thru' #WF-END-MM '/' #WF-END-DD '/' #WF-END-YYYY INTO #H-LINE-2
                "thru", pnd_Work_File_Pnd_Wf_End_Mm, "/", pnd_Work_File_Pnd_Wf_End_Dd, "/", pnd_Work_File_Pnd_Wf_End_Yyyy));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_H_Line_2.setValue(DbsUtil.compress("For the Month of", pnd_H_Report_Month, pnd_Work_File_Pnd_Wf_End_Yyyy));                                               //Natural: COMPRESS 'For the Month of' #H-REPORT-MONTH #WF-END-YYYY INTO #H-LINE-2
        }                                                                                                                                                                 //Natural: END-IF
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT *PROGRAM 33X 'ATA Work Management - Second Sighted MIT Requests' / *DATX ( EM = LLL' 'DD', 'YYYY ) 38X #H-LINE-2 / 'Page:' *PAGE-NUMBER ( 1 ) ( EM = ZZZ9 ) 14X
        //* *  'From' #H-START-DTE (EM=XXXX/XX/XX)
        //* *  'thru' #H-END-DTE   (EM=XXXX/XX/XX)
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK 1 #WORK-FILE
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, pnd_Work_File)))
        {
            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT BREAK #WF-2SIGHT-OPRTR
            //*  CALCULATE NUMBER DAYS BETWEEN QUEUE AND SECOND VERIFICATION
            //*  REQUEST PROCESSED AFTER 4PM IS COUNTED AS NEXT DAY's work
            pnd_P_Days_Auth.setValue(0);                                                                                                                                  //Natural: ASSIGN #P-DAYS-AUTH := 0
            if (condition(pnd_Work_File_Pnd_Wf_Auth_Queue_Dte.greater(getZero()) && pnd_Work_File_Pnd_Wf_2sight_Dte.greater(getZero())))                                  //Natural: IF #WF-AUTH-QUEUE-DTE > 0 AND #WF-2SIGHT-DTE > 0
            {
                pnd_Wk_Queue_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Work_File_Pnd_Wf_Auth_Queue_Dte_A);                                                 //Natural: MOVE EDITED #WF-AUTH-QUEUE-DTE-A TO #WK-QUEUE-DATE-D ( EM = YYYYMMDD )
                pnd_Wk_Auth_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Work_File_Pnd_Wf_2sight_Dte_A);                                                      //Natural: MOVE EDITED #WF-2SIGHT-DTE-A TO #WK-AUTH-DATE-D ( EM = YYYYMMDD )
                if (condition(pnd_Work_File_Pnd_Wf_Auth_Queue_Tme.greater(1600)))                                                                                         //Natural: IF #WF-AUTH-QUEUE-TME > 1600
                {
                    pnd_Wk_Queue_Date_D.nadd(1);                                                                                                                          //Natural: ADD 1 TO #WK-QUEUE-DATE-D
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Work_File_Pnd_Wf_2sight_Tme.greater(1600)))                                                                                             //Natural: IF #WF-2SIGHT-TME > 1600
                {
                    pnd_Wk_Auth_Date_D.nadd(1);                                                                                                                           //Natural: ADD 1 TO #WK-AUTH-DATE-D
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CALCULATE-BUSINESS-DAYS
                sub_Calculate_Business_Days();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_P_Days_Auth.setValue(pnd_Wk_Bsnss_Days);                                                                                                              //Natural: ASSIGN #P-DAYS-AUTH := #WK-BSNSS-DAYS
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_File_Pnd_Wf_2sight_Oprtr.equals("99999999")))                                                                                          //Natural: IF #WF-2SIGHT-OPRTR = '99999999'
            {
                pnd_Work_File_Pnd_Wf_2sight_Oprtr.setValue(" ");                                                                                                          //Natural: ASSIGN #WF-2SIGHT-OPRTR := ' '
            }                                                                                                                                                             //Natural: END-IF
            getReports().display(1, "//Authorizer",                                                                                                                       //Natural: DISPLAY ( 1 ) '//Authorizer' #WF-2SIGHT-OPRTR ( IS = ON ) 'Auth/Queue/Date' #WF-AUTH-QUEUE-DTE ( EM = 9999/99/99 ) 'Auth/Queue/Time' #WF-AUTH-QUEUE-TME ( EM = 99:99 ) '//WPID' #WF-WPID '//Pin' #WF-PIN '/Auth/Date' #WF-2SIGHT-DTE ( EM = 9999/99/99 ) '/Auth/Time' #WF-2SIGHT-TME ( EM = 99:99 ) 'Avg/Auth/Days' #P-DAYS-AUTH ( EM = Z,ZZ9 ) '//Operator' #WF-OPRTR '/Return/Oprtr' #WF-RETURN-OPRTR '/Return/Date' #WF-RETURN-DTE ( EM = 9999/99/99 ) 'TIAA/Recvd/Date' #WF-RCVD-DTE ( EM = 9999/99/99 ) '/Closed/Date' #WF-CLOSED-DTE ( EM = 9999/99/99 )
            		pnd_Work_File_Pnd_Wf_2sight_Oprtr, new IdenticalSuppress(true),"Auth/Queue/Date",
            		pnd_Work_File_Pnd_Wf_Auth_Queue_Dte, new ReportEditMask ("9999/99/99"),"Auth/Queue/Time",
            		pnd_Work_File_Pnd_Wf_Auth_Queue_Tme, new ReportEditMask ("99:99"),"//WPID",
            		pnd_Work_File_Pnd_Wf_Wpid,"//Pin",
            		pnd_Work_File_Pnd_Wf_Pin,"/Auth/Date",
            		pnd_Work_File_Pnd_Wf_2sight_Dte, new ReportEditMask ("9999/99/99"),"/Auth/Time",
            		pnd_Work_File_Pnd_Wf_2sight_Tme, new ReportEditMask ("99:99"),"Avg/Auth/Days",
            		pnd_P_Days_Auth, new ReportEditMask ("Z,ZZ9"),"//Operator",
            		pnd_Work_File_Pnd_Wf_Oprtr,"/Return/Oprtr",
            		pnd_Work_File_Pnd_Wf_Return_Oprtr,"/Return/Date",
            		pnd_Work_File_Pnd_Wf_Return_Dte, new ReportEditMask ("9999/99/99"),"TIAA/Recvd/Date",
            		pnd_Work_File_Pnd_Wf_Rcvd_Dte, new ReportEditMask ("9999/99/99"),"/Closed/Date",
            		pnd_Work_File_Pnd_Wf_Closed_Dte, new ReportEditMask ("9999/99/99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Work_File_Pnd_Wf_Return_Oprtr.greater(" ")))                                                                                                //Natural: IF #WF-RETURN-OPRTR > ' '
            {
                pnd_Return_Count.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #RETURN-COUNT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_File_Pnd_Wf_2sight_Oprtr.equals(" ")))                                                                                                 //Natural: IF #WF-2SIGHT-OPRTR = ' '
            {
                pnd_Unauth_Count.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #UNAUTH-COUNT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                short decideConditionsMet1162 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE #P-DAYS-AUTH;//Natural: VALUE 0
                if (condition((pnd_P_Days_Auth.equals(0))))
                {
                    decideConditionsMet1162++;
                    pnd_Auth_T0.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #AUTH-T0
                }                                                                                                                                                         //Natural: VALUE 1
                else if (condition((pnd_P_Days_Auth.equals(1))))
                {
                    decideConditionsMet1162++;
                    pnd_Auth_T1.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #AUTH-T1
                }                                                                                                                                                         //Natural: VALUE 2
                else if (condition((pnd_P_Days_Auth.equals(2))))
                {
                    decideConditionsMet1162++;
                    pnd_Auth_T2.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #AUTH-T2
                }                                                                                                                                                         //Natural: VALUE 3
                else if (condition((pnd_P_Days_Auth.equals(3))))
                {
                    decideConditionsMet1162++;
                    pnd_Auth_T3.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #AUTH-T3
                }                                                                                                                                                         //Natural: NONE VALUE
                else if (condition())
                {
                    pnd_Auth_T3plus.nadd(1);                                                                                                                              //Natural: ADD 1 TO #AUTH-T3PLUS
                }                                                                                                                                                         //Natural: END-DECIDE
                pnd_Auth_Count.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #AUTH-COUNT
                pnd_Auth_Days.nadd(pnd_P_Days_Auth);                                                                                                                      //Natural: ADD #P-DAYS-AUTH TO #AUTH-DAYS
            }                                                                                                                                                             //Natural: END-IF
            pnd_Write_Count.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #WRITE-COUNT
            readWork01Pnd_Wf_2sight_OprtrOld.setValue(pnd_Work_File_Pnd_Wf_2sight_Oprtr);                                                                                 //Natural: END-WORK
        }
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-BUSINESS-DAYS
        //* ***********************************************************************
        //*  GRAND TOTAL
        pnd_Avg_Auth_Days.compute(new ComputeParameters(true, pnd_Avg_Auth_Days), pnd_Tot_Auth_Days.divide(pnd_Tot_Auth_Count));                                          //Natural: COMPUTE ROUNDED #AVG-AUTH-DAYS = #TOT-AUTH-DAYS / #TOT-AUTH-COUNT
        getReports().write(1, new ColumnSpacing(39),"Total Unauthorized/Undefined",pnd_Tot_Unauth_Count, new ReportEditMask ("ZZZZ,ZZ9"),NEWLINE,new ColumnSpacing(47),"Total Returned Rqst ",pnd_Tot_Return_Count,  //Natural: WRITE ( 01 ) 39X 'Total Unauthorized/Undefined' #TOT-UNAUTH-COUNT ( EM = ZZZZZ,ZZ9 ) / 47X 'Total Returned Rqst ' #TOT-RETURN-COUNT ( EM = ZZZZZ,ZZ9 ) / 47X 'Total T+0  Requests ' #TOT-AUTH-T0 ( EM = ZZZZZ,ZZ9 ) / 47X 'Total T+1  Requests ' #TOT-AUTH-T1 ( EM = ZZZZZ,ZZ9 ) / 47X 'Total T+2  Requests ' #TOT-AUTH-T2 ( EM = ZZZZZ,ZZ9 ) / 47X 'Total T+3  Requests ' #TOT-AUTH-T3 ( EM = ZZZZZ,ZZ9 ) / 47X 'Total T>3  Requests ' #TOT-AUTH-T3PLUS ( EM = ZZZZZ,ZZ9 ) / 47X 'Total Auth Requests ' #TOT-AUTH-COUNT ( EM = ZZZZZ,ZZ9 ) / 40X 'Avg. Authorized Rqst. Days' #AVG-AUTH-DAYS ( EM = ZZ,ZZ9.99 ) / '_' ( 131 )
            new ReportEditMask ("ZZZZ,ZZ9"),NEWLINE,new ColumnSpacing(47),"Total T+0  Requests ",pnd_Tot_Auth_T0, new ReportEditMask ("ZZZZ,ZZ9"),NEWLINE,new 
            ColumnSpacing(47),"Total T+1  Requests ",pnd_Tot_Auth_T1, new ReportEditMask ("ZZZZ,ZZ9"),NEWLINE,new ColumnSpacing(47),"Total T+2  Requests ",pnd_Tot_Auth_T2, 
            new ReportEditMask ("ZZZZ,ZZ9"),NEWLINE,new ColumnSpacing(47),"Total T+3  Requests ",pnd_Tot_Auth_T3, new ReportEditMask ("ZZZZ,ZZ9"),NEWLINE,new 
            ColumnSpacing(47),"Total T>3  Requests ",pnd_Tot_Auth_T3plus, new ReportEditMask ("ZZZZ,ZZ9"),NEWLINE,new ColumnSpacing(47),"Total Auth Requests ",pnd_Tot_Auth_Count, 
            new ReportEditMask ("ZZZZ,ZZ9"),NEWLINE,new ColumnSpacing(40),"Avg. Authorized Rqst. Days",pnd_Avg_Auth_Days, new ReportEditMask ("ZZ,ZZ9.99"),NEWLINE,"_",new 
            RepeatItem(131));
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,NEWLINE,NEWLINE,pnd_Write_Count, new ReportEditMask ("ZZZZ,ZZ9"),"Total Count");                                                    //Natural: WRITE /// #WRITE-COUNT ( EM = ZZZZ,ZZ9 ) 'Total Count'
        if (Global.isEscape()) return;
    }
    private void sub_Calculate_Business_Days() throws Exception                                                                                                           //Natural: CALCULATE-BUSINESS-DAYS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Wk_Bsnss_Days.setValue(0);                                                                                                                                    //Natural: ASSIGN #WK-BSNSS-DAYS := 0
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Wk_Queue_Date_D.greaterOrEqual(pnd_Wk_Auth_Date_D))) {break;}                                                                               //Natural: UNTIL #WK-QUEUE-DATE-D GE #WK-AUTH-DATE-D
            pnd_Wk_Queue_Dte_A.setValueEdited(pnd_Wk_Queue_Date_D,new ReportEditMask("YYYYMMDD"));                                                                        //Natural: MOVE EDITED #WK-QUEUE-DATE-D ( EM = YYYYMMDD ) TO #WK-QUEUE-DTE-A
            pdaNeca4000.getNeca4000_Request_Ind().setValue(" ");                                                                                                          //Natural: ASSIGN NECA4000.REQUEST-IND := ' '
            pdaNeca4000.getNeca4000_Function_Cde().setValue("CAL");                                                                                                       //Natural: ASSIGN NECA4000.FUNCTION-CDE := 'CAL'
            pdaNeca4000.getNeca4000_Inpt_Key_Option_Cde().setValue("  ");                                                                                                 //Natural: ASSIGN NECA4000.INPT-KEY-OPTION-CDE := '  '
            pdaNeca4000.getNeca4000_Cal_Key_For_Dte().setValue(pnd_Wk_Queue_Dte_A_Pnd_Wk_Queue_Dte_N);                                                                    //Natural: ASSIGN NECA4000.CAL-KEY-FOR-DTE := #WK-QUEUE-DTE-N
            DbsUtil.callnat(Necn4000.class , getCurrentProcessState(), pdaNeca4000.getNeca4000());                                                                        //Natural: CALLNAT 'NECN4000' NECA4000
            if (condition(Global.isEscape())) return;
            if (condition(pdaNeca4000.getNeca4000_Return_Cde().notEquals("00")))                                                                                          //Natural: IF NECA4000.RETURN-CDE NE '00'
            {
                getReports().write(0, " BAD RETURN FROM CALENDAR CALL",pdaNeca4000.getNeca4000_Return_Cde());                                                             //Natural: WRITE ' BAD RETURN FROM CALENDAR CALL'NECA4000.RETURN-CDE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(8);  if (true) return;                                                                                                                  //Natural: TERMINATE 8
            }                                                                                                                                                             //Natural: END-IF
            pnd_Wk_Queue_Date_D.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #WK-QUEUE-DATE-D
            if (condition(pdaNeca4000.getNeca4000_Cal_Business_Day_Ind().getValue(1).equals("Y")))                                                                        //Natural: IF NECA4000.CAL-BUSINESS-DAY-IND ( 1 ) = 'Y'
            {
                pnd_Wk_Bsnss_Days.nadd(1);                                                                                                                                //Natural: ADD 1 TO #WK-BSNSS-DAYS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        if (condition(pnd_Wk_Bsnss_Days.less(getZero())))                                                                                                                 //Natural: IF #WK-BSNSS-DAYS LT 0
        {
            pnd_Wk_Bsnss_Days.setValue(0);                                                                                                                                //Natural: ASSIGN #WK-BSNSS-DAYS := 0
        }                                                                                                                                                                 //Natural: END-IF
        //*  CALCULATE-BUSINESS-DAYS
    }

    //

    // Support Methods

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pnd_Work_File_Pnd_Wf_2sight_OprtrIsBreak = pnd_Work_File_Pnd_Wf_2sight_Oprtr.isBreak(endOfData);
        if (condition(pnd_Work_File_Pnd_Wf_2sight_OprtrIsBreak))
        {
            if (condition(readWork01Pnd_Wf_2sight_OprtrOld.equals(" ")))                                                                                                  //Natural: IF OLD ( #WF-2SIGHT-OPRTR ) = ' '
            {
                getReports().write(1, new ColumnSpacing(45),"Unauthorized/Undefined",pnd_Unauth_Count, new ReportEditMask ("ZZZZ,ZZ9"),NEWLINE,"_",new                    //Natural: WRITE ( 01 ) 45X 'Unauthorized/Undefined' #UNAUTH-COUNT ( EM = ZZZZZ,ZZ9 ) / '_' ( 131 )
                    RepeatItem(131));
                if (condition(Global.isEscape())) return;
                pnd_Tot_Unauth_Count.nadd(pnd_Unauth_Count);                                                                                                              //Natural: ADD #UNAUTH-COUNT TO #TOT-UNAUTH-COUNT
                pnd_Unauth_Count.reset();                                                                                                                                 //Natural: RESET #UNAUTH-COUNT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Avg_Auth_Days.compute(new ComputeParameters(true, pnd_Avg_Auth_Days), pnd_Auth_Days.divide(pnd_Auth_Count));                                          //Natural: COMPUTE ROUNDED #AVG-AUTH-DAYS = #AUTH-DAYS / #AUTH-COUNT
                getReports().write(1, new ColumnSpacing(47),"  Returned Requests ",pnd_Return_Count, new ReportEditMask ("ZZZZ,ZZ9"),NEWLINE,new ColumnSpacing(47),"     Authorized T+0 ",pnd_Auth_T0,  //Natural: WRITE ( 01 ) 47X '  Returned Requests ' #RETURN-COUNT ( EM = ZZZZZ,ZZ9 ) / 47X '     Authorized T+0 ' #AUTH-T0 ( EM = ZZZZZ,ZZ9 ) / 47X '     Authorized T+1 ' #AUTH-T1 ( EM = ZZZZZ,ZZ9 ) / 47X '     Authorized T+2 ' #AUTH-T2 ( EM = ZZZZZ,ZZ9 ) / 47X '     Authorized T+3 ' #AUTH-T3 ( EM = ZZZZZ,ZZ9 ) / 47X '     Authorized T>3 ' #AUTH-T3PLUS ( EM = ZZZZZ,ZZ9 ) / 47X 'Authorized Requests ' #AUTH-COUNT ( EM = ZZZZZ,ZZ9 ) / 40X 'Avg. Authorized Rqst. Days' #AVG-AUTH-DAYS ( EM = ZZ,ZZ9.99 ) / '_' ( 131 )
                    new ReportEditMask ("ZZZZ,ZZ9"),NEWLINE,new ColumnSpacing(47),"     Authorized T+1 ",pnd_Auth_T1, new ReportEditMask ("ZZZZ,ZZ9"),NEWLINE,new 
                    ColumnSpacing(47),"     Authorized T+2 ",pnd_Auth_T2, new ReportEditMask ("ZZZZ,ZZ9"),NEWLINE,new ColumnSpacing(47),"     Authorized T+3 ",pnd_Auth_T3, 
                    new ReportEditMask ("ZZZZ,ZZ9"),NEWLINE,new ColumnSpacing(47),"     Authorized T>3 ",pnd_Auth_T3plus, new ReportEditMask ("ZZZZ,ZZ9"),NEWLINE,new 
                    ColumnSpacing(47),"Authorized Requests ",pnd_Auth_Count, new ReportEditMask ("ZZZZ,ZZ9"),NEWLINE,new ColumnSpacing(40),"Avg. Authorized Rqst. Days",pnd_Avg_Auth_Days, 
                    new ReportEditMask ("ZZ,ZZ9.99"),NEWLINE,"_",new RepeatItem(131));
                if (condition(Global.isEscape())) return;
                pnd_Tot_Return_Count.nadd(pnd_Return_Count);                                                                                                              //Natural: ADD #RETURN-COUNT TO #TOT-RETURN-COUNT
                pnd_Tot_Auth_Count.nadd(pnd_Auth_Count);                                                                                                                  //Natural: ADD #AUTH-COUNT TO #TOT-AUTH-COUNT
                pnd_Tot_Auth_Days.nadd(pnd_Auth_Days);                                                                                                                    //Natural: ADD #AUTH-DAYS TO #TOT-AUTH-DAYS
                pnd_Tot_Auth_T0.nadd(pnd_Auth_T0);                                                                                                                        //Natural: ADD #AUTH-T0 TO #TOT-AUTH-T0
                pnd_Tot_Auth_T1.nadd(pnd_Auth_T1);                                                                                                                        //Natural: ADD #AUTH-T1 TO #TOT-AUTH-T1
                pnd_Tot_Auth_T2.nadd(pnd_Auth_T2);                                                                                                                        //Natural: ADD #AUTH-T2 TO #TOT-AUTH-T2
                pnd_Tot_Auth_T3.nadd(pnd_Auth_T3);                                                                                                                        //Natural: ADD #AUTH-T3 TO #TOT-AUTH-T3
                pnd_Tot_Auth_T3plus.nadd(pnd_Auth_T3plus);                                                                                                                //Natural: ADD #AUTH-T3PLUS TO #TOT-AUTH-T3PLUS
                pnd_Auth_Days.reset();                                                                                                                                    //Natural: RESET #AUTH-DAYS #RETURN-COUNT #AUTH-COUNT #AUTH-T0 #AUTH-T1 #AUTH-T2 #AUTH-T3 #AUTH-T3PLUS
                pnd_Return_Count.reset();
                pnd_Auth_Count.reset();
                pnd_Auth_T0.reset();
                pnd_Auth_T1.reset();
                pnd_Auth_T2.reset();
                pnd_Auth_T3.reset();
                pnd_Auth_T3plus.reset();
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=55 LS=133");
        Global.format(1, "PS=55 LS=133 ZP=OFF");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getPROGRAM(),new ColumnSpacing(33),"ATA Work Management - Second Sighted MIT Requests",NEWLINE,Global.getDATX(), 
            new ReportEditMask ("LLL' 'DD', 'YYYY"),new ColumnSpacing(38),pnd_H_Line_2,NEWLINE,"Page:",getReports().getPageNumberDbs(1), new ReportEditMask 
            ("ZZZ9"),new ColumnSpacing(14));

        getReports().setDisplayColumns(1, "//Authorizer",
        		pnd_Work_File_Pnd_Wf_2sight_Oprtr, new IdenticalSuppress(true),"Auth/Queue/Date",
        		pnd_Work_File_Pnd_Wf_Auth_Queue_Dte, new ReportEditMask ("9999/99/99"),"Auth/Queue/Time",
        		pnd_Work_File_Pnd_Wf_Auth_Queue_Tme, new ReportEditMask ("99:99"),"//WPID",
        		pnd_Work_File_Pnd_Wf_Wpid,"//Pin",
        		pnd_Work_File_Pnd_Wf_Pin,"/Auth/Date",
        		pnd_Work_File_Pnd_Wf_2sight_Dte, new ReportEditMask ("9999/99/99"),"/Auth/Time",
        		pnd_Work_File_Pnd_Wf_2sight_Tme, new ReportEditMask ("99:99"),"Avg/Auth/Days",
        		pnd_P_Days_Auth, new ReportEditMask ("Z,ZZ9"),"//Operator",
        		pnd_Work_File_Pnd_Wf_Oprtr,"/Return/Oprtr",
        		pnd_Work_File_Pnd_Wf_Return_Oprtr,"/Return/Date",
        		pnd_Work_File_Pnd_Wf_Return_Dte, new ReportEditMask ("9999/99/99"),"TIAA/Recvd/Date",
        		pnd_Work_File_Pnd_Wf_Rcvd_Dte, new ReportEditMask ("9999/99/99"),"/Closed/Date",
        		pnd_Work_File_Pnd_Wf_Closed_Dte, new ReportEditMask ("9999/99/99"));
    }
}
