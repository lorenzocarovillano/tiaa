/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:45:46 PM
**        * FROM NATURAL PROGRAM : Cwfb8621
************************************************************
**        * FILE NAME            : Cwfb8621.java
**        * CLASS NAME           : Cwfb8621
**        * INSTANCE NAME        : Cwfb8621
************************************************************
************************************************************************
*
* SYSTEM   : CORPORATE WORKFLOW
* PROGRAM  : CWFB8621
* TITLE    : STAFF ACTIVITY REPORT (MONTHLY EXTRACT)
* FUNCTION : READ CWF-MASTER-INDEX-VIEW (045/181); EXTRACT OPERATOR
*          : INFORMATION FOR A MONTH; CREATE A WORKFILE.
* HISTORY  :
*          : 07/2007 RSALGADO
* 01/2008  : 1- ADD USERS TO LIST; INCREASE TABLE   /* RS0108
* RSALGADO : 2- INCLUDE "received time" IN REPORT
* 02/2008  : REMOVE 'HAYHURS' FROM LIST OF OPERATOR /* RS0208
* 05/2008  : REMOVE 'BARRETT' AND 'WILKIJO'         /* RS0508
* 10/2010  : REPLACE HARD-CODED ID'S WITH TABLE LOOKUP DM
* 03/2015  : INCREASED THE SIZE OF USER ARRAY TO ACCOMMODATE THE
*            NEW USERS ADDED TO THE ATA-USER TABLE - VR
* 02/2016  : CHANGED THE PROCESS DAYS CALCULATION LOGIC PER
*            BUSINESS (GWEN ARCHER) - IB
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb8621 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_mit;
    private DbsField mit_Pin_Nbr;
    private DbsField mit_Rqst_Log_Dte_Tme;

    private DbsGroup mit__R_Field_1;
    private DbsField mit_Rqst_Log_Dte;
    private DbsField mit_Rqst_Log_Tme;
    private DbsField mit_Rqst_Log_Invrt_Dte_Tme;
    private DbsField mit_Work_Prcss_Id;
    private DbsField mit_Last_Chnge_Dte_Tme;
    private DbsField mit_Tiaa_Rcvd_Dte_Tme;
    private DbsField mit_Crprte_Due_Dte_Tme;

    private DataAccessProgramView vw_mit_Hist;
    private DbsField mit_Hist_Pin_Nbr;
    private DbsField mit_Hist_Empl_Oprtr_Cde;
    private DbsField mit_Hist_Rqst_Log_Oprtr_Cde;
    private DbsField mit_Hist_Rqst_Log_Dte_Tme;

    private DbsGroup mit_Hist__R_Field_2;
    private DbsField mit_Hist_Rqst_Log_Dte;
    private DbsField mit_Hist_Rqst_Log_Tme;
    private DbsField mit_Hist_Rqst_Log_Invrt_Dte_Tme;
    private DbsField mit_Hist_Orgnl_Unit_Cde;
    private DbsField mit_Hist_Unit_Cde;
    private DbsField mit_Hist_Status_Cde;
    private DbsField mit_Hist_Last_Chnge_Dte_Tme;
    private DbsField mit_Hist_Last_Chnge_Oprtr_Cde;
    private DbsField pnd_Status_Date_Key;

    private DbsGroup pnd_Status_Date_Key__R_Field_3;
    private DbsField pnd_Status_Date_Key_Pnd_K_Status;
    private DbsField pnd_Status_Date_Key_Pnd_K_Invert_Upd_Dte;

    private DbsGroup pnd_Status_Date_Key__R_Field_4;
    private DbsField pnd_Status_Date_Key_Pnd_K_Invert_Yymmdd;
    private DbsField pnd_Status_Date_Key_Pnd_K_Invert_Hhiisst;
    private DbsField pnd_Status_Date_Key__Filler1;
    private DbsField pnd_History_Key;

    private DbsGroup pnd_History_Key__R_Field_5;
    private DbsField pnd_History_Key_Pnd_K_Log_Dte_Tme;
    private DbsField pnd_History_Key_Pnd_K_Last_Chnge_Invrt_D_T;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_6;
    private DbsField cwf_Support_Tbl_Pnd_Tbl_User_Key;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_7;
    private DbsField cwf_Support_Tbl_Tbl_User;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_8;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field;

    private DbsGroup pnd_Operators;
    private DbsField pnd_Operators_Pnd_T_Oprtr;
    private DbsField pnd_I_User;
    private DbsField pnd_Work_File;

    private DbsGroup pnd_Work_File__R_Field_9;
    private DbsField pnd_Work_File_Pnd_Wf_Oprtr;
    private DbsField pnd_Work_File_Pnd_Wf_Wpid;
    private DbsField pnd_Work_File_Pnd_Wf_Pin;
    private DbsField pnd_Work_File_Pnd_Wf_Recvd_Dte_Tme;

    private DbsGroup pnd_Work_File__R_Field_10;
    private DbsField pnd_Work_File_Pnd_Wf_Recvd_Dte;
    private DbsField pnd_Work_File_Pnd_Wf_Recvd_Tme;
    private DbsField pnd_Work_File__Filler2;
    private DbsField pnd_Work_File_Pnd_Wf_Due_Dte_Tme;

    private DbsGroup pnd_Work_File__R_Field_11;
    private DbsField pnd_Work_File_Pnd_Wf_Due_Dte;
    private DbsField pnd_Work_File_Pnd_Wf_Due_Tme;
    private DbsField pnd_Work_File_Pnd_Wf_Compltd_Dte_Tme;

    private DbsGroup pnd_Work_File__R_Field_12;
    private DbsField pnd_Work_File_Pnd_Wf_Compltd_Dte;
    private DbsField pnd_Work_File_Pnd_Wf_Compltd_Tme;
    private DbsField pnd_Work_File_Pnd_Wf_Last_Chnge_Oprtr;
    private DbsField pnd_Work_File_Pnd_Wf_Unit_Open_Stts;
    private DbsField pnd_Work_File_Pnd_Wf_Unit_Open_Dte_Tme;

    private DbsGroup pnd_Work_File__R_Field_13;
    private DbsField pnd_Work_File_Pnd_Wf_Unit_Open_Dte;
    private DbsField pnd_Work_File_Pnd_Wf_Unit_Open_Tme;
    private DbsField pnd_Work_File_Pnd_Wf_Unit_Clsd_Stts;
    private DbsField pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte_Tme;

    private DbsGroup pnd_Work_File__R_Field_14;
    private DbsField pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte;
    private DbsField pnd_Work_File_Pnd_Wf_Unit_Clsd_Tme;

    private DbsGroup pnd_Work_File__R_Field_15;
    private DbsField pnd_Work_File__Filler3;
    private DbsField pnd_Work_File_Pnd_Wf_Run;
    private DbsField pnd_Work_File_Pnd_Wf_Start_Dte;
    private DbsField pnd_Work_File_Pnd_Wf_End_Dte;

    private DbsGroup pnd_Units;
    private DbsField pnd_Units_Pnd_Unit_01;
    private DbsField pnd_Units_Pnd_Unit_02;

    private DbsGroup pnd_Units__R_Field_16;
    private DbsField pnd_Units_Pnd_T_Unit;
    private DbsField pnd_Start_Datd;
    private DbsField pnd_Start_Yyyymmdd;

    private DbsGroup pnd_Start_Yyyymmdd__R_Field_17;
    private DbsField pnd_Start_Yyyymmdd_Pnd_Start_Date;

    private DbsGroup pnd_Start_Yyyymmdd__R_Field_18;
    private DbsField pnd_Start_Yyyymmdd_Pnd_Start_Yyyymm;
    private DbsField pnd_Start_Yyyymmdd_Pnd_Start_Dd;
    private DbsField pnd_End_Datd;
    private DbsField pnd_End_Yyyymmdd;

    private DbsGroup pnd_End_Yyyymmdd__R_Field_19;
    private DbsField pnd_End_Yyyymmdd_Pnd_End_Date;

    private DbsGroup pnd_End_Yyyymmdd__R_Field_20;
    private DbsField pnd_End_Yyyymmdd_Pnd_End_Yyyymm;
    private DbsField pnd_End_Yyyymmdd_Pnd_End_Dd;
    private DbsField pnd_Last_Chnge_Dte_Tme;

    private DbsGroup pnd_Last_Chnge_Dte_Tme__R_Field_21;
    private DbsField pnd_Last_Chnge_Dte_Tme_Pnd_Last_Chnge_Yyyymmdd;
    private DbsField pnd_Log_Dte_Tme;
    private DbsField pnd_Write_Count;

    private DbsGroup pnd_Array;
    private DbsField pnd_Array_Pnd_Arr_Wf_Unit_Clsd_Stts;
    private DbsField pnd_Array_Pnd_Arr_Wf_Unit_Clsd_Dte_Tme;
    private DbsField pnd_Array_Pnd_Arr_Wf_Last_Chnge_Oprtr;
    private DbsField pnd_Cnt;
    private DbsField pnd_I;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_mit = new DataAccessProgramView(new NameInfo("vw_mit", "MIT"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        mit_Pin_Nbr = vw_mit.getRecord().newFieldInGroup("mit_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "PIN_NBR");
        mit_Pin_Nbr.setDdmHeader("PIN");
        mit_Rqst_Log_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "RQST_LOG_DTE_TME");
        mit_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");

        mit__R_Field_1 = vw_mit.getRecord().newGroupInGroup("mit__R_Field_1", "REDEFINE", mit_Rqst_Log_Dte_Tme);
        mit_Rqst_Log_Dte = mit__R_Field_1.newFieldInGroup("mit_Rqst_Log_Dte", "RQST-LOG-DTE", FieldType.STRING, 8);
        mit_Rqst_Log_Tme = mit__R_Field_1.newFieldInGroup("mit_Rqst_Log_Tme", "RQST-LOG-TME", FieldType.STRING, 7);
        mit_Rqst_Log_Invrt_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Rqst_Log_Invrt_Dte_Tme", "RQST-LOG-INVRT-DTE-TME", FieldType.NUMERIC, 15, 
            RepeatingFieldStrategy.None, "RQST_LOG_INVRT_DTE_TME");
        mit_Work_Prcss_Id = vw_mit.getRecord().newFieldInGroup("mit_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        mit_Work_Prcss_Id.setDdmHeader("WORK/ID");
        mit_Last_Chnge_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, 
            "LAST_CHNGE_DTE_TME");
        mit_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");
        mit_Tiaa_Rcvd_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Tiaa_Rcvd_Dte_Tme", "TIAA-RCVD-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TIAA_RCVD_DTE_TME");
        mit_Tiaa_Rcvd_Dte_Tme.setDdmHeader("TIAA-RCV/DTE-TME");
        mit_Crprte_Due_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Crprte_Due_Dte_Tme", "CRPRTE-DUE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "CRPRTE_DUE_DTE_TME");
        mit_Crprte_Due_Dte_Tme.setDdmHeader("WORKRQST/DUE DATE");
        registerRecord(vw_mit);

        vw_mit_Hist = new DataAccessProgramView(new NameInfo("vw_mit_Hist", "MIT-HIST"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        mit_Hist_Pin_Nbr = vw_mit_Hist.getRecord().newFieldInGroup("mit_Hist_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "PIN_NBR");
        mit_Hist_Pin_Nbr.setDdmHeader("PIN");
        mit_Hist_Empl_Oprtr_Cde = vw_mit_Hist.getRecord().newFieldInGroup("mit_Hist_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "EMPL_OPRTR_CDE");
        mit_Hist_Empl_Oprtr_Cde.setDdmHeader("EMPLOYEE/ID");
        mit_Hist_Rqst_Log_Oprtr_Cde = vw_mit_Hist.getRecord().newFieldInGroup("mit_Hist_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "RQST_LOG_OPRTR_CDE");
        mit_Hist_Rqst_Log_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        mit_Hist_Rqst_Log_Dte_Tme = vw_mit_Hist.getRecord().newFieldInGroup("mit_Hist_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "RQST_LOG_DTE_TME");
        mit_Hist_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");

        mit_Hist__R_Field_2 = vw_mit_Hist.getRecord().newGroupInGroup("mit_Hist__R_Field_2", "REDEFINE", mit_Hist_Rqst_Log_Dte_Tme);
        mit_Hist_Rqst_Log_Dte = mit_Hist__R_Field_2.newFieldInGroup("mit_Hist_Rqst_Log_Dte", "RQST-LOG-DTE", FieldType.STRING, 8);
        mit_Hist_Rqst_Log_Tme = mit_Hist__R_Field_2.newFieldInGroup("mit_Hist_Rqst_Log_Tme", "RQST-LOG-TME", FieldType.STRING, 7);
        mit_Hist_Rqst_Log_Invrt_Dte_Tme = vw_mit_Hist.getRecord().newFieldInGroup("mit_Hist_Rqst_Log_Invrt_Dte_Tme", "RQST-LOG-INVRT-DTE-TME", FieldType.NUMERIC, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_INVRT_DTE_TME");
        mit_Hist_Orgnl_Unit_Cde = vw_mit_Hist.getRecord().newFieldInGroup("mit_Hist_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ORGNL_UNIT_CDE");
        mit_Hist_Orgnl_Unit_Cde.setDdmHeader("LOG/UNIT");
        mit_Hist_Unit_Cde = vw_mit_Hist.getRecord().newFieldInGroup("mit_Hist_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "UNIT_CDE");
        mit_Hist_Unit_Cde.setDdmHeader("UNIT/CODE");
        mit_Hist_Status_Cde = vw_mit_Hist.getRecord().newFieldInGroup("mit_Hist_Status_Cde", "STATUS-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "STATUS_CDE");
        mit_Hist_Last_Chnge_Dte_Tme = vw_mit_Hist.getRecord().newFieldInGroup("mit_Hist_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 
            15, RepeatingFieldStrategy.None, "LAST_CHNGE_DTE_TME");
        mit_Hist_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");
        mit_Hist_Last_Chnge_Oprtr_Cde = vw_mit_Hist.getRecord().newFieldInGroup("mit_Hist_Last_Chnge_Oprtr_Cde", "LAST-CHNGE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "LAST_CHNGE_OPRTR_CDE");
        mit_Hist_Last_Chnge_Oprtr_Cde.setDdmHeader("LOG CREATE/OPERATOR");
        registerRecord(vw_mit_Hist);

        pnd_Status_Date_Key = localVariables.newFieldInRecord("pnd_Status_Date_Key", "#STATUS-DATE-KEY", FieldType.STRING, 25);

        pnd_Status_Date_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Status_Date_Key__R_Field_3", "REDEFINE", pnd_Status_Date_Key);
        pnd_Status_Date_Key_Pnd_K_Status = pnd_Status_Date_Key__R_Field_3.newFieldInGroup("pnd_Status_Date_Key_Pnd_K_Status", "#K-STATUS", FieldType.STRING, 
            1);
        pnd_Status_Date_Key_Pnd_K_Invert_Upd_Dte = pnd_Status_Date_Key__R_Field_3.newFieldInGroup("pnd_Status_Date_Key_Pnd_K_Invert_Upd_Dte", "#K-INVERT-UPD-DTE", 
            FieldType.NUMERIC, 15);

        pnd_Status_Date_Key__R_Field_4 = pnd_Status_Date_Key__R_Field_3.newGroupInGroup("pnd_Status_Date_Key__R_Field_4", "REDEFINE", pnd_Status_Date_Key_Pnd_K_Invert_Upd_Dte);
        pnd_Status_Date_Key_Pnd_K_Invert_Yymmdd = pnd_Status_Date_Key__R_Field_4.newFieldInGroup("pnd_Status_Date_Key_Pnd_K_Invert_Yymmdd", "#K-INVERT-YYMMDD", 
            FieldType.NUMERIC, 8);
        pnd_Status_Date_Key_Pnd_K_Invert_Hhiisst = pnd_Status_Date_Key__R_Field_4.newFieldInGroup("pnd_Status_Date_Key_Pnd_K_Invert_Hhiisst", "#K-INVERT-HHIISST", 
            FieldType.NUMERIC, 7);
        pnd_Status_Date_Key__Filler1 = pnd_Status_Date_Key__R_Field_3.newFieldInGroup("pnd_Status_Date_Key__Filler1", "_FILLER1", FieldType.STRING, 9);
        pnd_History_Key = localVariables.newFieldInRecord("pnd_History_Key", "#HISTORY-KEY", FieldType.STRING, 30);

        pnd_History_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_History_Key__R_Field_5", "REDEFINE", pnd_History_Key);
        pnd_History_Key_Pnd_K_Log_Dte_Tme = pnd_History_Key__R_Field_5.newFieldInGroup("pnd_History_Key_Pnd_K_Log_Dte_Tme", "#K-LOG-DTE-TME", FieldType.STRING, 
            15);
        pnd_History_Key_Pnd_K_Last_Chnge_Invrt_D_T = pnd_History_Key__R_Field_5.newFieldInGroup("pnd_History_Key_Pnd_K_Last_Chnge_Invrt_D_T", "#K-LAST-CHNGE-INVRT-D-T", 
            FieldType.NUMERIC, 15);

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");

        cwf_Support_Tbl__R_Field_6 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_6", "REDEFINE", cwf_Support_Tbl_Tbl_Key_Field);
        cwf_Support_Tbl_Pnd_Tbl_User_Key = cwf_Support_Tbl__R_Field_6.newFieldInGroup("cwf_Support_Tbl_Pnd_Tbl_User_Key", "#TBL-USER-KEY", FieldType.STRING, 
            20);
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Support_Tbl__R_Field_7 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_7", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Tbl_User = cwf_Support_Tbl__R_Field_7.newFieldInGroup("cwf_Support_Tbl_Tbl_User", "TBL-USER", FieldType.STRING, 8);
        registerRecord(vw_cwf_Support_Tbl);

        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_8 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_8", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_8.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_8.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_8.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);

        pnd_Operators = localVariables.newGroupInRecord("pnd_Operators", "#OPERATORS");
        pnd_Operators_Pnd_T_Oprtr = pnd_Operators.newFieldArrayInGroup("pnd_Operators_Pnd_T_Oprtr", "#T-OPRTR", FieldType.STRING, 8, new DbsArrayController(1, 
            99));
        pnd_I_User = localVariables.newFieldInRecord("pnd_I_User", "#I-USER", FieldType.NUMERIC, 2);
        pnd_Work_File = localVariables.newFieldInRecord("pnd_Work_File", "#WORK-FILE", FieldType.STRING, 250);

        pnd_Work_File__R_Field_9 = localVariables.newGroupInRecord("pnd_Work_File__R_Field_9", "REDEFINE", pnd_Work_File);
        pnd_Work_File_Pnd_Wf_Oprtr = pnd_Work_File__R_Field_9.newFieldInGroup("pnd_Work_File_Pnd_Wf_Oprtr", "#WF-OPRTR", FieldType.STRING, 8);
        pnd_Work_File_Pnd_Wf_Wpid = pnd_Work_File__R_Field_9.newFieldInGroup("pnd_Work_File_Pnd_Wf_Wpid", "#WF-WPID", FieldType.STRING, 6);
        pnd_Work_File_Pnd_Wf_Pin = pnd_Work_File__R_Field_9.newFieldInGroup("pnd_Work_File_Pnd_Wf_Pin", "#WF-PIN", FieldType.NUMERIC, 12);
        pnd_Work_File_Pnd_Wf_Recvd_Dte_Tme = pnd_Work_File__R_Field_9.newFieldInGroup("pnd_Work_File_Pnd_Wf_Recvd_Dte_Tme", "#WF-RECVD-DTE-TME", FieldType.STRING, 
            15);

        pnd_Work_File__R_Field_10 = pnd_Work_File__R_Field_9.newGroupInGroup("pnd_Work_File__R_Field_10", "REDEFINE", pnd_Work_File_Pnd_Wf_Recvd_Dte_Tme);
        pnd_Work_File_Pnd_Wf_Recvd_Dte = pnd_Work_File__R_Field_10.newFieldInGroup("pnd_Work_File_Pnd_Wf_Recvd_Dte", "#WF-RECVD-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_File_Pnd_Wf_Recvd_Tme = pnd_Work_File__R_Field_10.newFieldInGroup("pnd_Work_File_Pnd_Wf_Recvd_Tme", "#WF-RECVD-TME", FieldType.NUMERIC, 
            4);
        pnd_Work_File__Filler2 = pnd_Work_File__R_Field_10.newFieldInGroup("pnd_Work_File__Filler2", "_FILLER2", FieldType.STRING, 3);
        pnd_Work_File_Pnd_Wf_Due_Dte_Tme = pnd_Work_File__R_Field_9.newFieldInGroup("pnd_Work_File_Pnd_Wf_Due_Dte_Tme", "#WF-DUE-DTE-TME", FieldType.STRING, 
            15);

        pnd_Work_File__R_Field_11 = pnd_Work_File__R_Field_9.newGroupInGroup("pnd_Work_File__R_Field_11", "REDEFINE", pnd_Work_File_Pnd_Wf_Due_Dte_Tme);
        pnd_Work_File_Pnd_Wf_Due_Dte = pnd_Work_File__R_Field_11.newFieldInGroup("pnd_Work_File_Pnd_Wf_Due_Dte", "#WF-DUE-DTE", FieldType.NUMERIC, 8);
        pnd_Work_File_Pnd_Wf_Due_Tme = pnd_Work_File__R_Field_11.newFieldInGroup("pnd_Work_File_Pnd_Wf_Due_Tme", "#WF-DUE-TME", FieldType.NUMERIC, 7);
        pnd_Work_File_Pnd_Wf_Compltd_Dte_Tme = pnd_Work_File__R_Field_9.newFieldInGroup("pnd_Work_File_Pnd_Wf_Compltd_Dte_Tme", "#WF-COMPLTD-DTE-TME", 
            FieldType.STRING, 15);

        pnd_Work_File__R_Field_12 = pnd_Work_File__R_Field_9.newGroupInGroup("pnd_Work_File__R_Field_12", "REDEFINE", pnd_Work_File_Pnd_Wf_Compltd_Dte_Tme);
        pnd_Work_File_Pnd_Wf_Compltd_Dte = pnd_Work_File__R_Field_12.newFieldInGroup("pnd_Work_File_Pnd_Wf_Compltd_Dte", "#WF-COMPLTD-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_File_Pnd_Wf_Compltd_Tme = pnd_Work_File__R_Field_12.newFieldInGroup("pnd_Work_File_Pnd_Wf_Compltd_Tme", "#WF-COMPLTD-TME", FieldType.NUMERIC, 
            7);
        pnd_Work_File_Pnd_Wf_Last_Chnge_Oprtr = pnd_Work_File__R_Field_9.newFieldInGroup("pnd_Work_File_Pnd_Wf_Last_Chnge_Oprtr", "#WF-LAST-CHNGE-OPRTR", 
            FieldType.STRING, 8);
        pnd_Work_File_Pnd_Wf_Unit_Open_Stts = pnd_Work_File__R_Field_9.newFieldInGroup("pnd_Work_File_Pnd_Wf_Unit_Open_Stts", "#WF-UNIT-OPEN-STTS", FieldType.STRING, 
            4);
        pnd_Work_File_Pnd_Wf_Unit_Open_Dte_Tme = pnd_Work_File__R_Field_9.newFieldInGroup("pnd_Work_File_Pnd_Wf_Unit_Open_Dte_Tme", "#WF-UNIT-OPEN-DTE-TME", 
            FieldType.STRING, 15);

        pnd_Work_File__R_Field_13 = pnd_Work_File__R_Field_9.newGroupInGroup("pnd_Work_File__R_Field_13", "REDEFINE", pnd_Work_File_Pnd_Wf_Unit_Open_Dte_Tme);
        pnd_Work_File_Pnd_Wf_Unit_Open_Dte = pnd_Work_File__R_Field_13.newFieldInGroup("pnd_Work_File_Pnd_Wf_Unit_Open_Dte", "#WF-UNIT-OPEN-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_File_Pnd_Wf_Unit_Open_Tme = pnd_Work_File__R_Field_13.newFieldInGroup("pnd_Work_File_Pnd_Wf_Unit_Open_Tme", "#WF-UNIT-OPEN-TME", FieldType.NUMERIC, 
            7);
        pnd_Work_File_Pnd_Wf_Unit_Clsd_Stts = pnd_Work_File__R_Field_9.newFieldInGroup("pnd_Work_File_Pnd_Wf_Unit_Clsd_Stts", "#WF-UNIT-CLSD-STTS", FieldType.STRING, 
            4);
        pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte_Tme = pnd_Work_File__R_Field_9.newFieldInGroup("pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte_Tme", "#WF-UNIT-CLSD-DTE-TME", 
            FieldType.STRING, 15);

        pnd_Work_File__R_Field_14 = pnd_Work_File__R_Field_9.newGroupInGroup("pnd_Work_File__R_Field_14", "REDEFINE", pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte_Tme);
        pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte = pnd_Work_File__R_Field_14.newFieldInGroup("pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte", "#WF-UNIT-CLSD-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_File_Pnd_Wf_Unit_Clsd_Tme = pnd_Work_File__R_Field_14.newFieldInGroup("pnd_Work_File_Pnd_Wf_Unit_Clsd_Tme", "#WF-UNIT-CLSD-TME", FieldType.NUMERIC, 
            7);

        pnd_Work_File__R_Field_15 = localVariables.newGroupInRecord("pnd_Work_File__R_Field_15", "REDEFINE", pnd_Work_File);
        pnd_Work_File__Filler3 = pnd_Work_File__R_Field_15.newFieldInGroup("pnd_Work_File__Filler3", "_FILLER3", FieldType.STRING, 8);
        pnd_Work_File_Pnd_Wf_Run = pnd_Work_File__R_Field_15.newFieldInGroup("pnd_Work_File_Pnd_Wf_Run", "#WF-RUN", FieldType.STRING, 8);
        pnd_Work_File_Pnd_Wf_Start_Dte = pnd_Work_File__R_Field_15.newFieldInGroup("pnd_Work_File_Pnd_Wf_Start_Dte", "#WF-START-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_File_Pnd_Wf_End_Dte = pnd_Work_File__R_Field_15.newFieldInGroup("pnd_Work_File_Pnd_Wf_End_Dte", "#WF-END-DTE", FieldType.NUMERIC, 8);

        pnd_Units = localVariables.newGroupInRecord("pnd_Units", "#UNITS");
        pnd_Units_Pnd_Unit_01 = pnd_Units.newFieldInGroup("pnd_Units_Pnd_Unit_01", "#UNIT-01", FieldType.STRING, 8);
        pnd_Units_Pnd_Unit_02 = pnd_Units.newFieldInGroup("pnd_Units_Pnd_Unit_02", "#UNIT-02", FieldType.STRING, 8);

        pnd_Units__R_Field_16 = localVariables.newGroupInRecord("pnd_Units__R_Field_16", "REDEFINE", pnd_Units);
        pnd_Units_Pnd_T_Unit = pnd_Units__R_Field_16.newFieldArrayInGroup("pnd_Units_Pnd_T_Unit", "#T-UNIT", FieldType.STRING, 8, new DbsArrayController(1, 
            2));
        pnd_Start_Datd = localVariables.newFieldInRecord("pnd_Start_Datd", "#START-DATD", FieldType.DATE);
        pnd_Start_Yyyymmdd = localVariables.newFieldInRecord("pnd_Start_Yyyymmdd", "#START-YYYYMMDD", FieldType.NUMERIC, 8);

        pnd_Start_Yyyymmdd__R_Field_17 = localVariables.newGroupInRecord("pnd_Start_Yyyymmdd__R_Field_17", "REDEFINE", pnd_Start_Yyyymmdd);
        pnd_Start_Yyyymmdd_Pnd_Start_Date = pnd_Start_Yyyymmdd__R_Field_17.newFieldInGroup("pnd_Start_Yyyymmdd_Pnd_Start_Date", "#START-DATE", FieldType.STRING, 
            8);

        pnd_Start_Yyyymmdd__R_Field_18 = localVariables.newGroupInRecord("pnd_Start_Yyyymmdd__R_Field_18", "REDEFINE", pnd_Start_Yyyymmdd);
        pnd_Start_Yyyymmdd_Pnd_Start_Yyyymm = pnd_Start_Yyyymmdd__R_Field_18.newFieldInGroup("pnd_Start_Yyyymmdd_Pnd_Start_Yyyymm", "#START-YYYYMM", FieldType.NUMERIC, 
            6);
        pnd_Start_Yyyymmdd_Pnd_Start_Dd = pnd_Start_Yyyymmdd__R_Field_18.newFieldInGroup("pnd_Start_Yyyymmdd_Pnd_Start_Dd", "#START-DD", FieldType.NUMERIC, 
            2);
        pnd_End_Datd = localVariables.newFieldInRecord("pnd_End_Datd", "#END-DATD", FieldType.DATE);
        pnd_End_Yyyymmdd = localVariables.newFieldInRecord("pnd_End_Yyyymmdd", "#END-YYYYMMDD", FieldType.NUMERIC, 8);

        pnd_End_Yyyymmdd__R_Field_19 = localVariables.newGroupInRecord("pnd_End_Yyyymmdd__R_Field_19", "REDEFINE", pnd_End_Yyyymmdd);
        pnd_End_Yyyymmdd_Pnd_End_Date = pnd_End_Yyyymmdd__R_Field_19.newFieldInGroup("pnd_End_Yyyymmdd_Pnd_End_Date", "#END-DATE", FieldType.STRING, 8);

        pnd_End_Yyyymmdd__R_Field_20 = localVariables.newGroupInRecord("pnd_End_Yyyymmdd__R_Field_20", "REDEFINE", pnd_End_Yyyymmdd);
        pnd_End_Yyyymmdd_Pnd_End_Yyyymm = pnd_End_Yyyymmdd__R_Field_20.newFieldInGroup("pnd_End_Yyyymmdd_Pnd_End_Yyyymm", "#END-YYYYMM", FieldType.NUMERIC, 
            6);
        pnd_End_Yyyymmdd_Pnd_End_Dd = pnd_End_Yyyymmdd__R_Field_20.newFieldInGroup("pnd_End_Yyyymmdd_Pnd_End_Dd", "#END-DD", FieldType.NUMERIC, 2);
        pnd_Last_Chnge_Dte_Tme = localVariables.newFieldInRecord("pnd_Last_Chnge_Dte_Tme", "#LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 15);

        pnd_Last_Chnge_Dte_Tme__R_Field_21 = localVariables.newGroupInRecord("pnd_Last_Chnge_Dte_Tme__R_Field_21", "REDEFINE", pnd_Last_Chnge_Dte_Tme);
        pnd_Last_Chnge_Dte_Tme_Pnd_Last_Chnge_Yyyymmdd = pnd_Last_Chnge_Dte_Tme__R_Field_21.newFieldInGroup("pnd_Last_Chnge_Dte_Tme_Pnd_Last_Chnge_Yyyymmdd", 
            "#LAST-CHNGE-YYYYMMDD", FieldType.NUMERIC, 8);
        pnd_Log_Dte_Tme = localVariables.newFieldInRecord("pnd_Log_Dte_Tme", "#LOG-DTE-TME", FieldType.NUMERIC, 15);
        pnd_Write_Count = localVariables.newFieldInRecord("pnd_Write_Count", "#WRITE-COUNT", FieldType.NUMERIC, 9);

        pnd_Array = localVariables.newGroupArrayInRecord("pnd_Array", "#ARRAY", new DbsArrayController(1, 1030));
        pnd_Array_Pnd_Arr_Wf_Unit_Clsd_Stts = pnd_Array.newFieldInGroup("pnd_Array_Pnd_Arr_Wf_Unit_Clsd_Stts", "#ARR-WF-UNIT-CLSD-STTS", FieldType.STRING, 
            4);
        pnd_Array_Pnd_Arr_Wf_Unit_Clsd_Dte_Tme = pnd_Array.newFieldInGroup("pnd_Array_Pnd_Arr_Wf_Unit_Clsd_Dte_Tme", "#ARR-WF-UNIT-CLSD-DTE-TME", FieldType.STRING, 
            15);
        pnd_Array_Pnd_Arr_Wf_Last_Chnge_Oprtr = pnd_Array.newFieldInGroup("pnd_Array_Pnd_Arr_Wf_Last_Chnge_Oprtr", "#ARR-WF-LAST-CHNGE-OPRTR", FieldType.STRING, 
            8);
        pnd_Cnt = localVariables.newFieldInRecord("pnd_Cnt", "#CNT", FieldType.NUMERIC, 4);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_mit.reset();
        vw_mit_Hist.reset();
        vw_cwf_Support_Tbl.reset();

        localVariables.reset();
        pnd_Units_Pnd_Unit_01.setInitialValue("IIPAP   ");
        pnd_Units_Pnd_Unit_02.setInitialValue("IIPAW   ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb8621() throws Exception
    {
        super("Cwfb8621");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*  GET PRIOR MONTH's date
        //*                                                                                                                                                               //Natural: FORMAT PS = 22 LS = 132
        pnd_Start_Datd.compute(new ComputeParameters(false, pnd_Start_Datd), Global.getDATX().subtract(28));                                                              //Natural: ASSIGN #START-DATD := *DATX - 28
        //*  FIRST DAY OF
        pnd_Start_Yyyymmdd_Pnd_Start_Date.setValueEdited(pnd_Start_Datd,new ReportEditMask("YYYYMMDD"));                                                                  //Natural: MOVE EDITED #START-DATD ( EM = YYYYMMDD ) TO #START-DATE
        //*    PRIOR MONTH
        pnd_Start_Yyyymmdd_Pnd_Start_Dd.setValue(1);                                                                                                                      //Natural: MOVE 01 TO #START-DD
        //*  LAST DATE OF
        pnd_End_Yyyymmdd_Pnd_End_Date.setValue(pnd_Start_Yyyymmdd_Pnd_Start_Date);                                                                                        //Natural: MOVE #START-DATE TO #END-DATE
        //*    PRIOR MONTH
        //*  CLOSED REQUEST
        pnd_End_Yyyymmdd_Pnd_End_Dd.setValue(31);                                                                                                                         //Natural: MOVE 31 TO #END-DD
        //* **************** TEST ** TEST ** TEST ** TEST ** TEST ** TEST ** TEST
        //* **
        //*        #START-DATE  := 20160403
        //*        #END-DATE    := 20160409
        //* **************** TEST ** TEST ** TEST ** TEST ** TEST ** TEST ** TEST
        //*  READ BY INVERT DATE
        //* *
        pnd_Status_Date_Key.setValue(" ");                                                                                                                                //Natural: ASSIGN #STATUS-DATE-KEY := ' '
        pnd_Status_Date_Key_Pnd_K_Status.setValue("9");                                                                                                                   //Natural: ASSIGN #K-STATUS := '9'
        pnd_Status_Date_Key_Pnd_K_Invert_Yymmdd.compute(new ComputeParameters(false, pnd_Status_Date_Key_Pnd_K_Invert_Yymmdd), DbsField.subtract(99999999,                //Natural: COMPUTE #K-INVERT-YYMMDD = 99999999 - #END-YYYYMMDD
            pnd_End_Yyyymmdd));
        pnd_Status_Date_Key_Pnd_K_Invert_Hhiisst.setValue(0);                                                                                                             //Natural: ASSIGN #K-INVERT-HHIISST := 0000000
        getReports().write(0, "=",pnd_Start_Yyyymmdd,"=",pnd_End_Yyyymmdd);                                                                                               //Natural: WRITE '=' #START-YYYYMMDD '=' #END-YYYYMMDD
        if (Global.isEscape()) return;
        //*  HEADER RECORD
        pnd_Work_File_Pnd_Wf_Run.setValue("MONTHLY ");                                                                                                                    //Natural: ASSIGN #WF-RUN := 'MONTHLY '
        pnd_Work_File_Pnd_Wf_Start_Dte.setValue(pnd_Start_Yyyymmdd);                                                                                                      //Natural: ASSIGN #WF-START-DTE := #START-YYYYMMDD
        pnd_Work_File_Pnd_Wf_End_Dte.setValue(pnd_End_Yyyymmdd);                                                                                                          //Natural: ASSIGN #WF-END-DTE := #END-YYYYMMDD
        getWorkFiles().write(1, false, pnd_Work_File);                                                                                                                    //Natural: WRITE WORK FILE 1 #WORK-FILE
        pnd_Work_File.reset();                                                                                                                                            //Natural: RESET #WORK-FILE
        //*  DM 10/10
                                                                                                                                                                          //Natural: PERFORM LOAD-USER-TABLE
        sub_Load_User_Table();
        if (condition(Global.isEscape())) {return;}
        vw_mit.startDatabaseRead                                                                                                                                          //Natural: READ MIT BY CRPRTE-STATUS-CHNGE-DTE-KEY STARTING FROM #STATUS-DATE-KEY
        (
        "READ01",
        new Wc[] { new Wc("CRPRTE_STATUS_CHNGE_DTE_KEY", ">=", pnd_Status_Date_Key, WcType.BY) },
        new Oc[] { new Oc("CRPRTE_STATUS_CHNGE_DTE_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_mit.readNextRow("READ01")))
        {
            //* **IF NOT (WORK-PRCSS-ID = 'TIPCPW' OR = 'TIPCS ' OR = 'TIPSA ')
            //* **  ESCAPE TOP
            //* **END-IF
            pnd_Last_Chnge_Dte_Tme.setValue(mit_Last_Chnge_Dte_Tme);                                                                                                      //Natural: ASSIGN #LAST-CHNGE-DTE-TME := MIT.LAST-CHNGE-DTE-TME
            //*  REQUESTS ARE READ IN REVERSE DATE ORDER
            if (condition(pnd_Last_Chnge_Dte_Tme_Pnd_Last_Chnge_Yyyymmdd.less(pnd_Start_Yyyymmdd)))                                                                       //Natural: IF #LAST-CHNGE-YYYYMMDD LT #START-YYYYMMDD
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //* *IF MIT.RQST-LOG-OPRTR-CDE = #T-OPRTR (*)
            //* *  IGNORE
            //* *ELSE
            //* *  ESCAPE TOP
            //* *END-IF
            pnd_History_Key.reset();                                                                                                                                      //Natural: RESET #HISTORY-KEY
            pnd_History_Key_Pnd_K_Log_Dte_Tme.setValue(mit_Rqst_Log_Dte_Tme);                                                                                             //Natural: ASSIGN #K-LOG-DTE-TME := MIT.RQST-LOG-DTE-TME
            pnd_History_Key_Pnd_K_Last_Chnge_Invrt_D_T.setValue(0);                                                                                                       //Natural: ASSIGN #K-LAST-CHNGE-INVRT-D-T := 0
                                                                                                                                                                          //Natural: PERFORM DETERMINE-REQUEST-HISTORY
            sub_Determine_Request_History();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  02/29/2016 IB START
            if (condition(pnd_Cnt.greater(getZero())))                                                                                                                    //Natural: IF #CNT > 0
            {
                FOR01:                                                                                                                                                    //Natural: FOR #I = #CNT TO 1 STEP -1
                for (pnd_I.setValue(pnd_Cnt); condition(pnd_I.greaterOrEqual(1)); pnd_I.nsubtract(1))
                {
                    if (condition(DbsUtil.maskMatches(pnd_Array_Pnd_Arr_Wf_Unit_Clsd_Stts.getValue(pnd_I),"'8'...") || DbsUtil.maskMatches(pnd_Array_Pnd_Arr_Wf_Unit_Clsd_Stts.getValue(pnd_I), //Natural: IF #ARR-WF-UNIT-CLSD-STTS ( #I ) = MASK ( '8'... ) OR #ARR-WF-UNIT-CLSD-STTS ( #I ) = MASK ( '7'... )
                        "'7'...")))
                    {
                        pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte_Tme.setValue(pnd_Array_Pnd_Arr_Wf_Unit_Clsd_Dte_Tme.getValue(pnd_I));                                          //Natural: ASSIGN #WF-UNIT-CLSD-DTE-TME := #ARR-WF-UNIT-CLSD-DTE-TME ( #I )
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte_Tme.equals(" ")))                                                                                        //Natural: IF #WF-UNIT-CLSD-DTE-TME = ' '
                {
                    FOR02:                                                                                                                                                //Natural: FOR #I = #CNT TO 1 STEP -1
                    for (pnd_I.setValue(pnd_Cnt); condition(pnd_I.greaterOrEqual(1)); pnd_I.nsubtract(1))
                    {
                        if (condition(pnd_Array_Pnd_Arr_Wf_Unit_Clsd_Stts.getValue(pnd_I).equals("4325")))                                                                //Natural: IF #ARR-WF-UNIT-CLSD-STTS ( #I ) = '4325'
                        {
                            pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte_Tme.setValue(pnd_Array_Pnd_Arr_Wf_Unit_Clsd_Dte_Tme.getValue(pnd_I));                                      //Natural: ASSIGN #WF-UNIT-CLSD-DTE-TME := #ARR-WF-UNIT-CLSD-DTE-TME ( #I )
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  02/29/2016 IB END
            //*  4/6/2016 VR START
            if (condition(pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte.less(pnd_Start_Yyyymmdd) || pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte.greater(pnd_End_Yyyymmdd)))                   //Natural: IF #WF-UNIT-CLSD-DTE LT #START-YYYYMMDD OR #WF-UNIT-CLSD-DTE GT #END-YYYYMMDD
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  4/6/2016 VR END
            //*  RQST NOT OPENED BY UNIT
            //*  RQST NOT CLOSED BY UNIT
            if (condition(pnd_Work_File_Pnd_Wf_Unit_Open_Stts.equals(" ") || pnd_Work_File_Pnd_Wf_Unit_Clsd_Stts.equals(" ")))                                            //Natural: IF #WF-UNIT-OPEN-STTS = ' ' OR #WF-UNIT-CLSD-STTS = ' '
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_File_Pnd_Wf_Oprtr.equals(pnd_Operators_Pnd_T_Oprtr.getValue("*"))))                                                                    //Natural: IF #WF-OPRTR = #T-OPRTR ( * )
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Work_File_Pnd_Wf_Wpid.setValue(mit_Work_Prcss_Id);                                                                                                        //Natural: ASSIGN #WF-WPID := MIT.WORK-PRCSS-ID
            pnd_Work_File_Pnd_Wf_Pin.setValue(mit_Pin_Nbr);                                                                                                               //Natural: ASSIGN #WF-PIN := MIT.PIN-NBR
            pnd_Work_File_Pnd_Wf_Recvd_Dte_Tme.setValueEdited(mit_Tiaa_Rcvd_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                               //Natural: MOVE EDITED MIT.TIAA-RCVD-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #WF-RECVD-DTE-TME
            pnd_Work_File_Pnd_Wf_Due_Dte_Tme.setValueEdited(mit_Crprte_Due_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                                //Natural: MOVE EDITED MIT.CRPRTE-DUE-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #WF-DUE-DTE-TME
            //*  02/29/2016 IB START
            //*  #WF-COMPLTD-DTE-TME  := MIT.LAST-CHNGE-DTE-TME
            pnd_Work_File_Pnd_Wf_Compltd_Dte_Tme.setValue(pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte_Tme);                                                                        //Natural: ASSIGN #WF-COMPLTD-DTE-TME := #WF-UNIT-CLSD-DTE-TME
            if (condition(pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte_Tme.notEquals(" ")))                                                                                         //Natural: IF #WF-UNIT-CLSD-DTE-TME NE ' '
            {
                //*  02/29/2016 IB END
                getWorkFiles().write(1, false, pnd_Work_File);                                                                                                            //Natural: WRITE WORK FILE 1 #WORK-FILE
                //*    RESET #WORK-FILE
                pnd_Write_Count.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #WRITE-COUNT
                //*  02/29/2016 IB START
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, "COMPLETED DATE IS BLANK: ",pnd_Work_File_Pnd_Wf_Pin,pnd_Work_File_Pnd_Wf_Recvd_Dte);                                               //Natural: WRITE 'COMPLETED DATE IS BLANK: ' #WF-PIN #WF-RECVD-DTE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  02/29/2016 IB END
            //*  RS0108
            getReports().display(0, "oprtr",                                                                                                                              //Natural: DISPLAY 'oprtr' #WF-OPRTR 'wpid' #WF-WPID 'pin' #WF-PIN 'rcvd' #WF-RECVD-DTE ' ' #WF-RECVD-TME 'due' #WF-DUE-DTE 'cmpltd' #WF-COMPLTD-DTE 'open' #WF-UNIT-OPEN-STTS ' ' #WF-UNIT-OPEN-DTE 'clsd' #WF-UNIT-CLSD-STTS ' ' #WF-UNIT-CLSD-DTE
            		pnd_Work_File_Pnd_Wf_Oprtr,"wpid",
            		pnd_Work_File_Pnd_Wf_Wpid,"pin",
            		pnd_Work_File_Pnd_Wf_Pin,"rcvd",
            		pnd_Work_File_Pnd_Wf_Recvd_Dte," ",
            		pnd_Work_File_Pnd_Wf_Recvd_Tme,"due",
            		pnd_Work_File_Pnd_Wf_Due_Dte,"cmpltd",
            		pnd_Work_File_Pnd_Wf_Compltd_Dte,"open",
            		pnd_Work_File_Pnd_Wf_Unit_Open_Stts," ",
            		pnd_Work_File_Pnd_Wf_Unit_Open_Dte,"clsd",
            		pnd_Work_File_Pnd_Wf_Unit_Clsd_Stts," ",
            		pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Work_File.reset();                                                                                                                                        //Natural: RESET #WORK-FILE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Write_Count);                                                                                                                       //Natural: WRITE '=' #WRITE-COUNT
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-USER-TABLE
        //* **********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-REQUEST-HISTORY
        //*  02/29/2016 IB START
        //*    #WF-UNIT-CLSD-DTE-TME  := MIT-HIST.LAST-CHNGE-DTE-TME
        //*  02/29/2016 IB START
        //* ***********************************************************************
    }
    //*  ADDED DM 10/10
    private void sub_Load_User_Table() throws Exception                                                                                                                   //Natural: LOAD-USER-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind.setValue("A");                                                                                                          //Natural: ASSIGN #TBL-SCRTY-LEVEL-IND := 'A'
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme.setValue("ATA-REPORT-PARMS");                                                                                                 //Natural: ASSIGN #TBL-TABLE-NME := 'ATA-REPORT-PARMS'
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field.setValue("ATA-USER");                                                                                                         //Natural: ASSIGN #TBL-KEY-FIELD := 'ATA-USER'
        vw_cwf_Support_Tbl.startDatabaseRead                                                                                                                              //Natural: READ CWF-SUPPORT-TBL BY TBL-PRIME-KEY STARTING FROM #TBL-PRIME-KEY
        (
        "READ02",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
        );
        READ02:
        while (condition(vw_cwf_Support_Tbl.readNextRow("READ02")))
        {
            if (condition(pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme.notEquals("ATA-REPORT-PARMS")))                                                                             //Natural: IF #TBL-TABLE-NME NE 'ATA-REPORT-PARMS'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(cwf_Support_Tbl_Tbl_Key_Field.getSubstring(1,8).equals("ATA-USER")))                                                                        //Natural: IF SUBSTRING ( CWF-SUPPORT-TBL.TBL-KEY-FIELD,1,8 ) = 'ATA-USER'
                {
                    pnd_I_User.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #I-USER
                    pnd_Operators_Pnd_T_Oprtr.getValue(pnd_I_User).setValue(cwf_Support_Tbl_Tbl_User);                                                                    //Natural: ASSIGN #T-OPRTR ( #I-USER ) := CWF-SUPPORT-TBL.TBL-USER
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Determine_Request_History() throws Exception                                                                                                         //Natural: DETERMINE-REQUEST-HISTORY
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Work_File_Pnd_Wf_Unit_Open_Stts.reset();                                                                                                                      //Natural: RESET #WF-UNIT-OPEN-STTS #WF-UNIT-CLSD-STTS #WF-UNIT-OPEN-DTE #WF-UNIT-CLSD-DTE #WF-UNIT-OPEN-DTE-TME #WF-UNIT-CLSD-DTE-TME #CNT #ARRAY ( * )
        pnd_Work_File_Pnd_Wf_Unit_Clsd_Stts.reset();
        pnd_Work_File_Pnd_Wf_Unit_Open_Dte.reset();
        pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte.reset();
        pnd_Work_File_Pnd_Wf_Unit_Open_Dte_Tme.reset();
        pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte_Tme.reset();
        pnd_Cnt.reset();
        pnd_Array.getValue("*").reset();
        //*  02/29/2016 IB END
        vw_mit_Hist.startDatabaseRead                                                                                                                                     //Natural: READ MIT-HIST BY RQST-HISTORY-KEY STARTING FROM #HISTORY-KEY
        (
        "READ03",
        new Wc[] { new Wc("RQST_HISTORY_KEY", ">=", pnd_History_Key, WcType.BY) },
        new Oc[] { new Oc("RQST_HISTORY_KEY", "ASC") }
        );
        READ03:
        while (condition(vw_mit_Hist.readNextRow("READ03")))
        {
            if (condition(mit_Hist_Rqst_Log_Dte_Tme.greater(mit_Rqst_Log_Dte_Tme)))                                                                                       //Natural: IF MIT-HIST.RQST-LOG-DTE-TME > MIT.RQST-LOG-DTE-TME
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(mit_Hist_Pin_Nbr.notEquals(mit_Pin_Nbr)))                                                                                                       //Natural: IF MIT-HIST.PIN-NBR NE MIT.PIN-NBR
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(mit_Hist_Orgnl_Unit_Cde.equals(pnd_Units_Pnd_T_Unit.getValue("*")) || mit_Hist_Unit_Cde.equals(pnd_Units_Pnd_T_Unit.getValue("*"))))            //Natural: IF MIT-HIST.ORGNL-UNIT-CDE = #T-UNIT ( * ) OR MIT-HIST.UNIT-CDE = #T-UNIT ( * )
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  TAKE THE FIRST "assign to" OPERATOR AND THE FIRST "closed" DATE
            //*  ASSIGNED
            if (condition(DbsUtil.maskMatches(mit_Hist_Status_Cde,"'2'...")))                                                                                             //Natural: IF MIT-HIST.STATUS-CDE = MASK ( '2'... )
            {
                pnd_Work_File_Pnd_Wf_Oprtr.setValue(mit_Hist_Empl_Oprtr_Cde);                                                                                             //Natural: ASSIGN #WF-OPRTR := MIT-HIST.EMPL-OPRTR-CDE
                pnd_Work_File_Pnd_Wf_Unit_Open_Stts.setValue(mit_Hist_Status_Cde);                                                                                        //Natural: ASSIGN #WF-UNIT-OPEN-STTS := MIT-HIST.STATUS-CDE
                pnd_Work_File_Pnd_Wf_Unit_Open_Dte_Tme.setValue(mit_Hist_Last_Chnge_Dte_Tme);                                                                             //Natural: ASSIGN #WF-UNIT-OPEN-DTE-TME := MIT-HIST.LAST-CHNGE-DTE-TME
            }                                                                                                                                                             //Natural: END-IF
            //*  COMPLETED
            //*  IB
            if (condition(mit_Hist_Status_Cde.equals("4325") || DbsUtil.maskMatches(mit_Hist_Status_Cde,"'7'...") || DbsUtil.maskMatches(mit_Hist_Status_Cde,             //Natural: IF MIT-HIST.STATUS-CDE = '4325' OR = MASK ( '7'... ) OR = MASK ( '8'... )
                "'8'...")))
            {
                pnd_Work_File_Pnd_Wf_Unit_Clsd_Stts.setValue(mit_Hist_Status_Cde);                                                                                        //Natural: ASSIGN #WF-UNIT-CLSD-STTS := MIT-HIST.STATUS-CDE
                pnd_Work_File_Pnd_Wf_Last_Chnge_Oprtr.setValue(mit_Hist_Last_Chnge_Oprtr_Cde);                                                                            //Natural: ASSIGN #WF-LAST-CHNGE-OPRTR := MIT-HIST.LAST-CHNGE-OPRTR-CDE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Cnt.nadd(1);                                                                                                                                              //Natural: ASSIGN #CNT := #CNT + 1
            pnd_Array_Pnd_Arr_Wf_Unit_Clsd_Stts.getValue(pnd_Cnt).setValue(mit_Hist_Status_Cde);                                                                          //Natural: MOVE MIT-HIST.STATUS-CDE TO #ARR-WF-UNIT-CLSD-STTS ( #CNT )
            pnd_Array_Pnd_Arr_Wf_Unit_Clsd_Dte_Tme.getValue(pnd_Cnt).setValue(mit_Hist_Last_Chnge_Dte_Tme);                                                               //Natural: MOVE MIT-HIST.LAST-CHNGE-DTE-TME TO #ARR-WF-UNIT-CLSD-DTE-TME ( #CNT )
            pnd_Array_Pnd_Arr_Wf_Last_Chnge_Oprtr.getValue(pnd_Cnt).setValue(mit_Hist_Last_Chnge_Oprtr_Cde);                                                              //Natural: MOVE MIT-HIST.LAST-CHNGE-OPRTR-CDE TO #ARR-WF-LAST-CHNGE-OPRTR ( #CNT )
            //*  02/29/2016 IB END
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  DETERMINE-REQUEST-HISTORY
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=22 LS=132");

        getReports().setDisplayColumns(0, "oprtr",
        		pnd_Work_File_Pnd_Wf_Oprtr,"wpid",
        		pnd_Work_File_Pnd_Wf_Wpid,"pin",
        		pnd_Work_File_Pnd_Wf_Pin,"rcvd",
        		pnd_Work_File_Pnd_Wf_Recvd_Dte," ",
        		pnd_Work_File_Pnd_Wf_Recvd_Tme,"due",
        		pnd_Work_File_Pnd_Wf_Due_Dte,"cmpltd",
        		pnd_Work_File_Pnd_Wf_Compltd_Dte,"open",
        		pnd_Work_File_Pnd_Wf_Unit_Open_Stts," ",
        		pnd_Work_File_Pnd_Wf_Unit_Open_Dte,"clsd",
        		pnd_Work_File_Pnd_Wf_Unit_Clsd_Stts," ",
        		pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte);
    }
}
