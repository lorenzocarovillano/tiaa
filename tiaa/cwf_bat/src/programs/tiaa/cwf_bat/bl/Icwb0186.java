/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:38:58 PM
**        * FROM NATURAL PROGRAM : Icwb0186
************************************************************
**        * FILE NAME            : Icwb0186.java
**        * CLASS NAME           : Icwb0186
**        * INSTANCE NAME        : Icwb0186
************************************************************
************************************************************************
* PROGRAM  : ICWB0186
* SYSTEM   : CWF
* TITLE    : IMAGE CONTROL BATCH ID EXTRACT
* WRITTEN  : SEPTEMBER 21, 2010
* AUTHOR   : JAMIE J. CRUZ
*
* FUNCTION : THIS PROGRAM READS THE CWF-IMAGE-XREF VIEW OF FILE 186, DB
*            45 BY PIMN (IMAGE-DOCUMENT-ADDRESS-CDE) AND WRITES OUT ALL
*            PIMNS AND BATCH-IDS FOR ALL RECORDS PROCESSED.
*
****
* HISTORY
*
* WHEN       WHO     WHAT
*
**
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Icwb0186 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Cur_Lang;
    private DbsField pnd_Program;
    private DbsField pnd_Output_Rec;
    private DbsField pnd_Input_Cnt;
    private DbsField pnd_Output_Cnt;
    private DbsField pnd_Zero_Batch;
    private DbsField pnd_I;
    private DbsField pnd_P;
    private DbsField pnd_Load_Tstamp;
    private DbsField pnd_Start_Datx;
    private DbsField pnd_Start_Date;
    private DbsField pnd_Start_Date_Time;
    private DbsField pnd_Start_Tstamp;
    private DbsField pnd_Wk_Batch_Id;

    private DbsGroup pnd_Wk_Batch_Id__R_Field_1;
    private DbsField pnd_Wk_Batch_Id_Pnd_Wk_Batch_First_8;
    private DbsField pnd_Wk_Batch_Id_Pnd_Wk_Batch_Last_2;
    private DbsField pnd_Batch_Id_A11;
    private DbsField pnd_Error_Message;
    private DbsField pnd_Warning_Message_Line;
    private DbsField pnd_Warning_Message_Line_2;
    private DbsField pnd_Warning_Message_Line_3;

    private DataAccessProgramView vw_cwf_Image_Xref;
    private DbsField cwf_Image_Xref_Image_Document_Address_Cde;
    private DbsField cwf_Image_Xref_Source_Id;
    private DbsField cwf_Image_Xref_Batch_Id;
    private DbsField cwf_Image_Xref_Upld_Date_Time;
    private DbsField pnd_Cwf_Xref_Key;

    private DbsGroup pnd_Cwf_Xref_Key__R_Field_2;
    private DbsField pnd_Cwf_Xref_Key_Pnd_Xref_Srce;
    private DbsField pnd_Cwf_Xref_Key_Pnd_Xref_Batch;
    private DbsField pnd_Cwf_Xref_Key_Pnd_Xref_Imageid;
    private DbsField pnd_Run_Parms;

    private DbsGroup pnd_Run_Parms__R_Field_3;
    private DbsField pnd_Run_Parms_Pnd_Run_Mode;
    private DbsField pnd_Run_Parms_Pnd_Dst_Prt1;
    private DbsField pnd_Run_Parms_Pnd_Dst_Prt2;
    private DbsField pnd_Run_Parms_Pnd_Dst_Prt3;
    private DbsField pnd_Run_Parms_Pnd_Passed_Cutoff_Dte;
    private DbsField pnd_Dest_Print1;
    private DbsField pnd_Dest_Print2;
    private DbsField pnd_Dest_Print3;
    private DbsField pnd_End_Of_Report;
    private DbsField pls_Trace;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Cur_Lang = localVariables.newFieldInRecord("pnd_Cur_Lang", "#CUR-LANG", FieldType.PACKED_DECIMAL, 1);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Output_Rec = localVariables.newFieldInRecord("pnd_Output_Rec", "#OUTPUT-REC", FieldType.STRING, 31);
        pnd_Input_Cnt = localVariables.newFieldInRecord("pnd_Input_Cnt", "#INPUT-CNT", FieldType.NUMERIC, 7);
        pnd_Output_Cnt = localVariables.newFieldInRecord("pnd_Output_Cnt", "#OUTPUT-CNT", FieldType.NUMERIC, 11);
        pnd_Zero_Batch = localVariables.newFieldInRecord("pnd_Zero_Batch", "#ZERO-BATCH", FieldType.NUMERIC, 11);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_P = localVariables.newFieldInRecord("pnd_P", "#P", FieldType.INTEGER, 2);
        pnd_Load_Tstamp = localVariables.newFieldInRecord("pnd_Load_Tstamp", "#LOAD-TSTAMP", FieldType.STRING, 15);
        pnd_Start_Datx = localVariables.newFieldInRecord("pnd_Start_Datx", "#START-DATX", FieldType.DATE);
        pnd_Start_Date = localVariables.newFieldInRecord("pnd_Start_Date", "#START-DATE", FieldType.STRING, 8);
        pnd_Start_Date_Time = localVariables.newFieldInRecord("pnd_Start_Date_Time", "#START-DATE-TIME", FieldType.STRING, 14);
        pnd_Start_Tstamp = localVariables.newFieldInRecord("pnd_Start_Tstamp", "#START-TSTAMP", FieldType.TIME);
        pnd_Wk_Batch_Id = localVariables.newFieldInRecord("pnd_Wk_Batch_Id", "#WK-BATCH-ID", FieldType.NUMERIC, 10, 2);

        pnd_Wk_Batch_Id__R_Field_1 = localVariables.newGroupInRecord("pnd_Wk_Batch_Id__R_Field_1", "REDEFINE", pnd_Wk_Batch_Id);
        pnd_Wk_Batch_Id_Pnd_Wk_Batch_First_8 = pnd_Wk_Batch_Id__R_Field_1.newFieldInGroup("pnd_Wk_Batch_Id_Pnd_Wk_Batch_First_8", "#WK-BATCH-FIRST-8", 
            FieldType.STRING, 8);
        pnd_Wk_Batch_Id_Pnd_Wk_Batch_Last_2 = pnd_Wk_Batch_Id__R_Field_1.newFieldInGroup("pnd_Wk_Batch_Id_Pnd_Wk_Batch_Last_2", "#WK-BATCH-LAST-2", FieldType.STRING, 
            2);
        pnd_Batch_Id_A11 = localVariables.newFieldInRecord("pnd_Batch_Id_A11", "#BATCH-ID-A11", FieldType.STRING, 11);
        pnd_Error_Message = localVariables.newFieldInRecord("pnd_Error_Message", "#ERROR-MESSAGE", FieldType.STRING, 22);
        pnd_Warning_Message_Line = localVariables.newFieldInRecord("pnd_Warning_Message_Line", "#WARNING-MESSAGE-LINE", FieldType.STRING, 70);
        pnd_Warning_Message_Line_2 = localVariables.newFieldInRecord("pnd_Warning_Message_Line_2", "#WARNING-MESSAGE-LINE-2", FieldType.STRING, 70);
        pnd_Warning_Message_Line_3 = localVariables.newFieldInRecord("pnd_Warning_Message_Line_3", "#WARNING-MESSAGE-LINE-3", FieldType.STRING, 70);

        vw_cwf_Image_Xref = new DataAccessProgramView(new NameInfo("vw_cwf_Image_Xref", "CWF-IMAGE-XREF"), "CWF_IMAGE_XREF", "CWF_IMAGE_XREF");
        cwf_Image_Xref_Image_Document_Address_Cde = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Image_Document_Address_Cde", "IMAGE-DOCUMENT-ADDRESS-CDE", 
            FieldType.STRING, 22, RepeatingFieldStrategy.None, "IMAGE_DOCUMENT_ADDRESS_CDE");
        cwf_Image_Xref_Image_Document_Address_Cde.setDdmHeader("IMNET DOC/ADDRESS");
        cwf_Image_Xref_Source_Id = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Source_Id", "SOURCE-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "SOURCE_ID");
        cwf_Image_Xref_Batch_Id = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Batch_Id", "BATCH-ID", FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, 
            "BATCH_ID");
        cwf_Image_Xref_Upld_Date_Time = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Upld_Date_Time", "UPLD-DATE-TIME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "UPLD_DATE_TIME");
        cwf_Image_Xref_Upld_Date_Time.setDdmHeader("UPLOAD/DATE TIME");
        registerRecord(vw_cwf_Image_Xref);

        pnd_Cwf_Xref_Key = localVariables.newFieldInRecord("pnd_Cwf_Xref_Key", "#CWF-XREF-KEY", FieldType.STRING, 27);

        pnd_Cwf_Xref_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Cwf_Xref_Key__R_Field_2", "REDEFINE", pnd_Cwf_Xref_Key);
        pnd_Cwf_Xref_Key_Pnd_Xref_Srce = pnd_Cwf_Xref_Key__R_Field_2.newFieldInGroup("pnd_Cwf_Xref_Key_Pnd_Xref_Srce", "#XREF-SRCE", FieldType.STRING, 
            6);
        pnd_Cwf_Xref_Key_Pnd_Xref_Batch = pnd_Cwf_Xref_Key__R_Field_2.newFieldInGroup("pnd_Cwf_Xref_Key_Pnd_Xref_Batch", "#XREF-BATCH", FieldType.STRING, 
            10);
        pnd_Cwf_Xref_Key_Pnd_Xref_Imageid = pnd_Cwf_Xref_Key__R_Field_2.newFieldInGroup("pnd_Cwf_Xref_Key_Pnd_Xref_Imageid", "#XREF-IMAGEID", FieldType.STRING, 
            11);
        pnd_Run_Parms = localVariables.newFieldInRecord("pnd_Run_Parms", "#RUN-PARMS", FieldType.STRING, 19);

        pnd_Run_Parms__R_Field_3 = localVariables.newGroupInRecord("pnd_Run_Parms__R_Field_3", "REDEFINE", pnd_Run_Parms);
        pnd_Run_Parms_Pnd_Run_Mode = pnd_Run_Parms__R_Field_3.newFieldInGroup("pnd_Run_Parms_Pnd_Run_Mode", "#RUN-MODE", FieldType.STRING, 8);
        pnd_Run_Parms_Pnd_Dst_Prt1 = pnd_Run_Parms__R_Field_3.newFieldInGroup("pnd_Run_Parms_Pnd_Dst_Prt1", "#DST-PRT1", FieldType.STRING, 1);
        pnd_Run_Parms_Pnd_Dst_Prt2 = pnd_Run_Parms__R_Field_3.newFieldInGroup("pnd_Run_Parms_Pnd_Dst_Prt2", "#DST-PRT2", FieldType.STRING, 1);
        pnd_Run_Parms_Pnd_Dst_Prt3 = pnd_Run_Parms__R_Field_3.newFieldInGroup("pnd_Run_Parms_Pnd_Dst_Prt3", "#DST-PRT3", FieldType.STRING, 1);
        pnd_Run_Parms_Pnd_Passed_Cutoff_Dte = pnd_Run_Parms__R_Field_3.newFieldInGroup("pnd_Run_Parms_Pnd_Passed_Cutoff_Dte", "#PASSED-CUTOFF-DTE", FieldType.STRING, 
            8);
        pnd_Dest_Print1 = localVariables.newFieldInRecord("pnd_Dest_Print1", "#DEST-PRINT1", FieldType.STRING, 8);
        pnd_Dest_Print2 = localVariables.newFieldInRecord("pnd_Dest_Print2", "#DEST-PRINT2", FieldType.STRING, 8);
        pnd_Dest_Print3 = localVariables.newFieldInRecord("pnd_Dest_Print3", "#DEST-PRINT3", FieldType.STRING, 8);
        pnd_End_Of_Report = localVariables.newFieldInRecord("pnd_End_Of_Report", "#END-OF-REPORT", FieldType.STRING, 24);
        pls_Trace = WsIndependent.getInstance().newFieldInRecord("pls_Trace", "+TRACE", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Image_Xref.reset();

        localVariables.reset();
        pnd_Dest_Print1.setInitialValue("CMPRT01");
        pnd_Dest_Print2.setInitialValue("CMPRT02");
        pnd_Dest_Print3.setInitialValue("CMPRT03");
        pnd_End_Of_Report.setInitialValue("  *** END OF REPORT *** ");
        pls_Trace.setInitialValue(true);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Icwb0186() throws Exception
    {
        super("Icwb0186");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*  INPUT #RUN-PARMS  /* TO SEE IF WE UPDATE THE CONTROL FILES.
        //*   THE USER CAN SELECT WHETHER THE REPORTS ARE PRINTED (CMPRT01/2/3)
        //*   OR ONLY SPOOLED TO ARM (CMPRT11/12/13)
        short decideConditionsMet102 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #DST-PRT1 = 'A'
        if (condition(pnd_Run_Parms_Pnd_Dst_Prt1.equals("A")))
        {
            decideConditionsMet102++;
            pnd_Dest_Print1.setValue("CMPRT11");                                                                                                                          //Natural: MOVE 'CMPRT11' TO #DEST-PRINT1
        }                                                                                                                                                                 //Natural: WHEN #DST-PRT2 = 'A'
        if (condition(pnd_Run_Parms_Pnd_Dst_Prt2.equals("A")))
        {
            decideConditionsMet102++;
            pnd_Dest_Print2.setValue("CMPRT12");                                                                                                                          //Natural: MOVE 'CMPRT12' TO #DEST-PRINT2
        }                                                                                                                                                                 //Natural: WHEN #DST-PRT3 = 'A'
        if (condition(pnd_Run_Parms_Pnd_Dst_Prt3.equals("A")))
        {
            decideConditionsMet102++;
            pnd_Dest_Print3.setValue("CMPRT13");                                                                                                                          //Natural: MOVE 'CMPRT13' TO #DEST-PRINT3
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet102 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  DEFINE PRINTERS AND FORMATS
        //*  TOTALS
        getReports().definePrinter(2, "SUMMARY");                                                                                                                         //Natural: DEFINE PRINTER ( SUMMARY = 1 ) OUTPUT #DEST-PRINT1
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) LS = 132 PS = 60;//Natural: FORMAT ( SUMMARY ) LS = 80 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF HW = OFF
        //*  TOTALS REPORT
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( SUMMARY ) TITLE LEFT 001T *PROGRAM 019T 'BATCH ID EXTRACT FOR WORKFLOW STABILITY FILE' 065T 'PAGE:' *PAGE-NUMBER ( SUMMARY ) ( NL = 4 AD = L SG = OFF ) / 001T 'DATE:' *DATX ( EM = LLL�DD,�YYYY ) 033T 'DAILY RUN SUMMARY' 065T 'TIME:' *TIMX ( EM = HH':'II'  'AP ) //
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: ASSIGN #PROGRAM = *PROGRAM
        //*  MAP THE CURRENT LANGUAGE CODE TO THE CODE IN THE BATCH MODEL TEXT LDA.
        pnd_Cur_Lang.setValue(Global.getLANGUAGE());                                                                                                                      //Natural: ASSIGN #CUR-LANG = *LANGUAGE
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH' THEN
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        //* *
        pnd_Cwf_Xref_Key_Pnd_Xref_Srce.setValue("POST");                                                                                                                  //Natural: ASSIGN #XREF-SRCE := 'POST'
        pnd_Start_Datx.compute(new ComputeParameters(false, pnd_Start_Datx), Global.getDATX().subtract(14));                                                              //Natural: ASSIGN #START-DATX := *DATX - 14
        pnd_Start_Date.setValueEdited(pnd_Start_Datx,new ReportEditMask("YYYYMMDD"));                                                                                     //Natural: MOVE EDITED #START-DATX ( EM = YYYYMMDD ) TO #START-DATE
        pnd_Start_Date_Time.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Start_Date, "000000"));                                                          //Natural: COMPRESS #START-DATE '000000' INTO #START-DATE-TIME LEAVING NO SPACE
        getReports().write(0, NEWLINE,NEWLINE,"=",pnd_Start_Date_Time,NEWLINE,NEWLINE);                                                                                   //Natural: WRITE ( 0 ) // '=' #START-DATE-TIME //
        if (Global.isEscape()) return;
        pnd_Start_Tstamp.setValueEdited(new ReportEditMask("YYYYMMDDHHIISS"),pnd_Start_Date_Time);                                                                        //Natural: MOVE EDITED #START-DATE-TIME TO #START-TSTAMP ( EM = YYYYMMDDHHIISS )
        vw_cwf_Image_Xref.startDatabaseRead                                                                                                                               //Natural: READ CWF-IMAGE-XREF WITH UPLD-DATE-TIME = #START-TSTAMP
        (
        "READ01",
        new Wc[] { new Wc("UPLD_DATE_TIME", ">=", pnd_Start_Tstamp, WcType.BY) },
        new Oc[] { new Oc("UPLD_DATE_TIME", "ASC") }
        );
        READ01:
        while (condition(vw_cwf_Image_Xref.readNextRow("READ01")))
        {
            if (condition(cwf_Image_Xref_Source_Id.notEquals("POST")))                                                                                                    //Natural: IF SOURCE-ID NE 'POST'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Load_Tstamp.setValueEdited(cwf_Image_Xref_Upld_Date_Time,new ReportEditMask("YYYYMMDDHHIISST"));                                                          //Natural: MOVE EDITED UPLD-DATE-TIME ( EM = YYYYMMDDHHIISST ) TO #LOAD-TSTAMP
            if (condition(cwf_Image_Xref_Batch_Id.notEquals(getZero())))                                                                                                  //Natural: IF BATCH-ID NE 0
            {
                pnd_Wk_Batch_Id.compute(new ComputeParameters(false, pnd_Wk_Batch_Id), NMath.abs (cwf_Image_Xref_Batch_Id));                                              //Natural: ASSIGN #WK-BATCH-ID := ABS ( BATCH-ID )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Zero_Batch.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #ZERO-BATCH
                pnd_Wk_Batch_Id_Pnd_Wk_Batch_First_8.reset();                                                                                                             //Natural: RESET #WK-BATCH-FIRST-8
            }                                                                                                                                                             //Natural: END-IF
            pnd_Output_Rec.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, cwf_Image_Xref_Image_Document_Address_Cde, ",", pnd_Wk_Batch_Id_Pnd_Wk_Batch_First_8)); //Natural: COMPRESS IMAGE-DOCUMENT-ADDRESS-CDE ',' #WK-BATCH-FIRST-8 INTO #OUTPUT-REC LEAVING NO SPACE
            getWorkFiles().write(1, false, pnd_Output_Rec);                                                                                                               //Natural: WRITE WORK FILE 01 #OUTPUT-REC
            pnd_Output_Cnt.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #OUTPUT-CNT
            if (condition(pnd_Output_Cnt.lessOrEqual(44)))                                                                                                                //Natural: IF #OUTPUT-CNT LE 44
            {
                getReports().display(0, "COUNT",                                                                                                                          //Natural: DISPLAY ( 0 ) 'COUNT' #OUTPUT-CNT 'IMNET DOC ADDRESS/PIMN' IMAGE-DOCUMENT-ADDRESS-CDE 'SOURCE' SOURCE-ID 'BATCH NUMBER' #WK-BATCH-FIRST-8 'UPLD TIMESTAMP' UPLD-DATE-TIME ( EM = YYYY'/'MM'/'DD' 'HH':'II':'SS )
                		pnd_Output_Cnt,"IMNET DOC ADDRESS/PIMN",
                		cwf_Image_Xref_Image_Document_Address_Cde,"SOURCE",
                		cwf_Image_Xref_Source_Id,"BATCH NUMBER",
                		pnd_Wk_Batch_Id_Pnd_Wk_Batch_First_8,"UPLD TIMESTAMP",
                		cwf_Image_Xref_Upld_Date_Time, new ReportEditMask ("YYYY'/'MM'/'DD' 'HH':'II':'SS"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(2, NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(22),"***  JOB COMPLETED SUCCESSFULLY  ***",NEWLINE,NEWLINE,new TabSetting(20),pnd_Output_Cnt,  //Natural: WRITE ( SUMMARY ) //// 22T '***  JOB COMPLETED SUCCESSFULLY  ***' // 20T #OUTPUT-CNT ( EM = ZZ,ZZZ,ZZZ,ZZ9 ) 'OUTPUT RECORDS WRITTEN' // 20T #ZERO-BATCH ( EM = ZZ,ZZZ,ZZZ,ZZ9 ) 'HAD NO BATCH NUMBER' //// 22T #END-OF-REPORT
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9"),"OUTPUT RECORDS WRITTEN",NEWLINE,NEWLINE,new TabSetting(20),pnd_Zero_Batch, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9"),"HAD NO BATCH NUMBER",NEWLINE,NEWLINE,NEWLINE,NEWLINE,new 
            TabSetting(22),pnd_End_Of_Report);
        if (Global.isEscape()) return;
        //* ***********************************************************************
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=60");
        Global.format(2, "LS=80 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF HW=OFF");

        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,new TabSetting(1),Global.getPROGRAM(),new TabSetting(19),"BATCH ID EXTRACT FOR WORKFLOW STABILITY FILE",new 
            TabSetting(65),"PAGE:",getReports().getPageNumberDbs(2), new NumericLength (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,new 
            TabSetting(1),"DATE:",Global.getDATX(), new ReportEditMask ("LLL DD, YYYY"),new TabSetting(33),"DAILY RUN SUMMARY",new TabSetting(65),"TIME:",Global.getTIMX(), 
            new ReportEditMask ("HH':'II'  'AP"),NEWLINE,NEWLINE);

        getReports().setDisplayColumns(0, "COUNT",
        		pnd_Output_Cnt,"IMNET DOC ADDRESS/PIMN",
        		cwf_Image_Xref_Image_Document_Address_Cde,"SOURCE",
        		cwf_Image_Xref_Source_Id,"BATCH NUMBER",
        		pnd_Wk_Batch_Id_Pnd_Wk_Batch_First_8,"UPLD TIMESTAMP",
        		cwf_Image_Xref_Upld_Date_Time, new ReportEditMask ("YYYY'/'MM'/'DD' 'HH':'II':'SS"));
    }
}
