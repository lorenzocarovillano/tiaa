/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:35:07 PM
**        * FROM NATURAL PROGRAM : Cwfb4810
************************************************************
**        * FILE NAME            : Cwfb4810.java
**        * CLASS NAME           : Cwfb4810
**        * INSTANCE NAME        : Cwfb4810
************************************************************
**********************************************************************
* PROGRAM  : CWFB4810
* SYSTEM   : CRPCWF
* TITLE    : COLLECTION OF MIT RECORDS.
* FUNCTION : THIS PROGRAM COLLECTS MIT RECORDS WHICH WAS TOUCHED DURING
*          : THE PERIOD DEFINED BY THE CONTROL RECORD "Window".
*          : THESE RECORDS RQST-LOG-DTE-TME WILL BE WRITTEN TO A FILE.
* CHANGE   : ADDITIONAL UNSORTED FILE WHICH CONTAINS RQST-LOG-DTE-TME
* L.E.     : FOR SMART INFORMATION WILL BE WRITTEN TO A FILE. 10/16/00
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
**********************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb4810 extends BLNatBase
{
    // Data Areas
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private PdaCwfa4815 pdaCwfa4815;
    private LdaCwfl4815 ldaCwfl4815;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Master;
    private DbsField cwf_Master_Pin_Nbr;
    private DbsField cwf_Master_Rqst_Log_Dte_Tme;
    private DbsField cwf_Master_Work_Prcss_Id;
    private DbsField cwf_Master_Last_Chnge_Dte_Tme;

    private DbsGroup pnd_Work_File_1;
    private DbsField pnd_Work_File_1_Pnd_Rqst_Log_Dte_Tme;

    private DbsGroup pnd_Work_File_2;
    private DbsField pnd_Work_File_2_Pnd_Rqst_Log_Dte_Tme;
    private DbsField pnd_Chnge_Dte_Tme_Log_Dte_Tme_Key;

    private DbsGroup pnd_Chnge_Dte_Tme_Log_Dte_Tme_Key__R_Field_1;
    private DbsField pnd_Chnge_Dte_Tme_Log_Dte_Tme_Key_Pnd_Last_Chnge_Dte_Tme;
    private DbsField pnd_Chnge_Dte_Tme_Log_Dte_Tme_Key_Pnd_Rqst_Log_Dte_Tme;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_2;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind_2_2;
    private DbsField pnd_Start_Dte_Tme_A;

    private DbsGroup pnd_Start_Dte_Tme_A__R_Field_3;
    private DbsField pnd_Start_Dte_Tme_A_Pnd_Start_Dte_Tme_N;
    private DbsField pnd_End_Dte_Tme_A;

    private DbsGroup pnd_End_Dte_Tme_A__R_Field_4;
    private DbsField pnd_End_Dte_Tme_A_Pnd_End_Dte_Tme_N;

    private DataAccessProgramView vw_cwf_Support_Tbl_Smart;
    private DbsField cwf_Support_Tbl_Smart_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Smart_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Smart_Tbl_Key_Field;

    private DbsGroup cwf_Support_Tbl_Smart__R_Field_5;
    private DbsField cwf_Support_Tbl_Smart_Run_Status;
    private DbsField cwf_Support_Tbl_Smart_Tbl_Data_Field;

    private DbsGroup cwf_Support_Tbl_Smart__R_Field_6;
    private DbsField cwf_Support_Tbl_Smart_Tbl_Wpid_Values;
    private DbsField pnd_Mit_Cntr_Selctd;
    private DbsField pnd_Smart_Rec_Selctd;
    private DbsField pnd_Zero_Pin;
    private DbsField pnd_Smart_Tbl;

    private DbsGroup pnd_Smart_Tbl__R_Field_7;
    private DbsField pnd_Smart_Tbl_Pnd_Smart_Wpid;

    private DbsRecord setTimeRecord;
    private DbsField st;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        pdaCwfa4815 = new PdaCwfa4815(localVariables);
        ldaCwfl4815 = new LdaCwfl4815();
        registerRecord(ldaCwfl4815);
        registerRecord(ldaCwfl4815.getVw_cwf_Support_Tbl());

        // Local Variables

        vw_cwf_Master = new DataAccessProgramView(new NameInfo("vw_cwf_Master", "CWF-MASTER"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        cwf_Master_Pin_Nbr = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "PIN_NBR");
        cwf_Master_Pin_Nbr.setDdmHeader("PIN");
        cwf_Master_Rqst_Log_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15, 
            RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Master_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        cwf_Master_Work_Prcss_Id = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        cwf_Master_Work_Prcss_Id.setDdmHeader("WORK/ID");
        cwf_Master_Last_Chnge_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 
            15, RepeatingFieldStrategy.None, "LAST_CHNGE_DTE_TME");
        cwf_Master_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");
        registerRecord(vw_cwf_Master);

        pnd_Work_File_1 = localVariables.newGroupInRecord("pnd_Work_File_1", "#WORK-FILE-1");
        pnd_Work_File_1_Pnd_Rqst_Log_Dte_Tme = pnd_Work_File_1.newFieldInGroup("pnd_Work_File_1_Pnd_Rqst_Log_Dte_Tme", "#RQST-LOG-DTE-TME", FieldType.STRING, 
            15);

        pnd_Work_File_2 = localVariables.newGroupInRecord("pnd_Work_File_2", "#WORK-FILE-2");
        pnd_Work_File_2_Pnd_Rqst_Log_Dte_Tme = pnd_Work_File_2.newFieldInGroup("pnd_Work_File_2_Pnd_Rqst_Log_Dte_Tme", "#RQST-LOG-DTE-TME", FieldType.STRING, 
            15);
        pnd_Chnge_Dte_Tme_Log_Dte_Tme_Key = localVariables.newFieldInRecord("pnd_Chnge_Dte_Tme_Log_Dte_Tme_Key", "#CHNGE-DTE-TME-LOG-DTE-TME-KEY", FieldType.STRING, 
            30);

        pnd_Chnge_Dte_Tme_Log_Dte_Tme_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Chnge_Dte_Tme_Log_Dte_Tme_Key__R_Field_1", "REDEFINE", pnd_Chnge_Dte_Tme_Log_Dte_Tme_Key);
        pnd_Chnge_Dte_Tme_Log_Dte_Tme_Key_Pnd_Last_Chnge_Dte_Tme = pnd_Chnge_Dte_Tme_Log_Dte_Tme_Key__R_Field_1.newFieldInGroup("pnd_Chnge_Dte_Tme_Log_Dte_Tme_Key_Pnd_Last_Chnge_Dte_Tme", 
            "#LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 15);
        pnd_Chnge_Dte_Tme_Log_Dte_Tme_Key_Pnd_Rqst_Log_Dte_Tme = pnd_Chnge_Dte_Tme_Log_Dte_Tme_Key__R_Field_1.newFieldInGroup("pnd_Chnge_Dte_Tme_Log_Dte_Tme_Key_Pnd_Rqst_Log_Dte_Tme", 
            "#RQST-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_2", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_2.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_2.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_2.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind_2_2 = pnd_Tbl_Prime_Key__R_Field_2.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind_2_2", "#TBL-ACTVE-IND-2-2", 
            FieldType.STRING, 1);
        pnd_Start_Dte_Tme_A = localVariables.newFieldInRecord("pnd_Start_Dte_Tme_A", "#START-DTE-TME-A", FieldType.STRING, 15);

        pnd_Start_Dte_Tme_A__R_Field_3 = localVariables.newGroupInRecord("pnd_Start_Dte_Tme_A__R_Field_3", "REDEFINE", pnd_Start_Dte_Tme_A);
        pnd_Start_Dte_Tme_A_Pnd_Start_Dte_Tme_N = pnd_Start_Dte_Tme_A__R_Field_3.newFieldInGroup("pnd_Start_Dte_Tme_A_Pnd_Start_Dte_Tme_N", "#START-DTE-TME-N", 
            FieldType.NUMERIC, 15);
        pnd_End_Dte_Tme_A = localVariables.newFieldInRecord("pnd_End_Dte_Tme_A", "#END-DTE-TME-A", FieldType.STRING, 15);

        pnd_End_Dte_Tme_A__R_Field_4 = localVariables.newGroupInRecord("pnd_End_Dte_Tme_A__R_Field_4", "REDEFINE", pnd_End_Dte_Tme_A);
        pnd_End_Dte_Tme_A_Pnd_End_Dte_Tme_N = pnd_End_Dte_Tme_A__R_Field_4.newFieldInGroup("pnd_End_Dte_Tme_A_Pnd_End_Dte_Tme_N", "#END-DTE-TME-N", FieldType.NUMERIC, 
            15);

        vw_cwf_Support_Tbl_Smart = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl_Smart", "CWF-SUPPORT-TBL-SMART"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Smart_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl_Smart.getRecord().newFieldInGroup("cwf_Support_Tbl_Smart_Tbl_Scrty_Level_Ind", 
            "TBL-SCRTY-LEVEL-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Smart_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Smart_Tbl_Table_Nme = vw_cwf_Support_Tbl_Smart.getRecord().newFieldInGroup("cwf_Support_Tbl_Smart_Tbl_Table_Nme", "TBL-TABLE-NME", 
            FieldType.STRING, 20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Smart_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Smart_Tbl_Key_Field = vw_cwf_Support_Tbl_Smart.getRecord().newFieldInGroup("cwf_Support_Tbl_Smart_Tbl_Key_Field", "TBL-KEY-FIELD", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");

        cwf_Support_Tbl_Smart__R_Field_5 = vw_cwf_Support_Tbl_Smart.getRecord().newGroupInGroup("cwf_Support_Tbl_Smart__R_Field_5", "REDEFINE", cwf_Support_Tbl_Smart_Tbl_Key_Field);
        cwf_Support_Tbl_Smart_Run_Status = cwf_Support_Tbl_Smart__R_Field_5.newFieldInGroup("cwf_Support_Tbl_Smart_Run_Status", "RUN-STATUS", FieldType.STRING, 
            1);
        cwf_Support_Tbl_Smart_Tbl_Data_Field = vw_cwf_Support_Tbl_Smart.getRecord().newFieldInGroup("cwf_Support_Tbl_Smart_Tbl_Data_Field", "TBL-DATA-FIELD", 
            FieldType.STRING, 253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Support_Tbl_Smart__R_Field_6 = vw_cwf_Support_Tbl_Smart.getRecord().newGroupInGroup("cwf_Support_Tbl_Smart__R_Field_6", "REDEFINE", cwf_Support_Tbl_Smart_Tbl_Data_Field);
        cwf_Support_Tbl_Smart_Tbl_Wpid_Values = cwf_Support_Tbl_Smart__R_Field_6.newFieldInGroup("cwf_Support_Tbl_Smart_Tbl_Wpid_Values", "TBL-WPID-VALUES", 
            FieldType.STRING, 60);
        registerRecord(vw_cwf_Support_Tbl_Smart);

        pnd_Mit_Cntr_Selctd = localVariables.newFieldInRecord("pnd_Mit_Cntr_Selctd", "#MIT-CNTR-SELCTD", FieldType.PACKED_DECIMAL, 8);
        pnd_Smart_Rec_Selctd = localVariables.newFieldInRecord("pnd_Smart_Rec_Selctd", "#SMART-REC-SELCTD", FieldType.PACKED_DECIMAL, 8);
        pnd_Zero_Pin = localVariables.newFieldInRecord("pnd_Zero_Pin", "#ZERO-PIN", FieldType.PACKED_DECIMAL, 8);
        pnd_Smart_Tbl = localVariables.newFieldInRecord("pnd_Smart_Tbl", "#SMART-TBL", FieldType.STRING, 60);

        pnd_Smart_Tbl__R_Field_7 = localVariables.newGroupInRecord("pnd_Smart_Tbl__R_Field_7", "REDEFINE", pnd_Smart_Tbl);
        pnd_Smart_Tbl_Pnd_Smart_Wpid = pnd_Smart_Tbl__R_Field_7.newFieldArrayInGroup("pnd_Smart_Tbl_Pnd_Smart_Wpid", "#SMART-WPID", FieldType.STRING, 
            6, new DbsArrayController(1, 10));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        setTimeRecord = new DbsRecord();
        st = setTimeRecord.newFieldInRecord("ST", "ST", FieldType.NUMERIC, 7);
        registerRecord(setTimeRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Master.reset();
        vw_cwf_Support_Tbl_Smart.reset();

        ldaCwfl4815.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb4810() throws Exception
    {
        super("Cwfb4810");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*    INITIALIZATION
        pnd_Chnge_Dte_Tme_Log_Dte_Tme_Key.reset();                                                                                                                        //Natural: FORMAT ( 0 ) PS = 56 LS = 131;//Natural: RESET #CHNGE-DTE-TME-LOG-DTE-TME-KEY
        //*   TO ALLOW FOR EASY ESCAPE FROM PROGRAM.
        REP1:                                                                                                                                                             //Natural: REPEAT
        while (condition(whileTrue))
        {
                                                                                                                                                                          //Natural: PERFORM LOAD-SMART-TABLE
            sub_Load_Smart_Table();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM READ-RUN-CONTROL
            sub_Read_Run_Control();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM SELECT-MIT-RECORDS
            sub_Select_Mit_Records();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-TOTALS-REPORT
            sub_Write_Totals_Report();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM UPDATE-RUN-CONTROL
            sub_Update_Run_Control();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM ESCAPE-PROGRAM
            sub_Escape_Program();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ESCAPE-PROGRAM
            //*   (REP1.)
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  ================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-SMART-TABLE
        //*  ================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-RUN-CONTROL
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SELECT-MIT-RECORDS
        //* ***********************************
        //* *****************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TOTALS-REPORT
        //* ****************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-RUN-CONTROL
    }
    private void sub_Escape_Program() throws Exception                                                                                                                    //Natural: ESCAPE-PROGRAM
    {
        if (BLNatReinput.isReinput()) return;

        //*  ================================
        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( REP1. )
        Global.setEscapeCode(EscapeType.Bottom, "REP1");
        if (true) return;
    }
    private void sub_Load_Smart_Table() throws Exception                                                                                                                  //Natural: LOAD-SMART-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ================================
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind.setValue("A ");                                                                                                         //Natural: MOVE 'A ' TO #TBL-SCRTY-LEVEL-IND
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme.setValue("CWF-SMART-TABLE");                                                                                                  //Natural: MOVE 'CWF-SMART-TABLE' TO #TBL-TABLE-NME
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field.setValue("SMART-WPIDS");                                                                                                      //Natural: MOVE 'SMART-WPIDS' TO #TBL-KEY-FIELD
        vw_cwf_Support_Tbl_Smart.startDatabaseRead                                                                                                                        //Natural: READ ( 1 ) CWF-SUPPORT-TBL-SMART BY TBL-PRIME-KEY STARTING FROM #TBL-PRIME-KEY
        (
        "READ01",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(vw_cwf_Support_Tbl_Smart.readNextRow("READ01")))
        {
            if (condition(cwf_Support_Tbl_Smart_Tbl_Table_Nme.notEquals(pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme)))                                                            //Natural: IF CWF-SUPPORT-TBL-SMART.TBL-TABLE-NME NE #TBL-PRIME-KEY.#TBL-TABLE-NME
            {
                getReports().write(0, ReportOption.NOTITLE,"Smart Support Table was not found....");                                                                      //Natural: WRITE 'Smart Support Table was not found....'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(55);  if (true) return;                                                                                                                 //Natural: TERMINATE 55
            }                                                                                                                                                             //Natural: END-IF
            pnd_Smart_Tbl.setValue(cwf_Support_Tbl_Smart_Tbl_Wpid_Values);                                                                                                //Natural: MOVE CWF-SUPPORT-TBL-SMART.TBL-WPID-VALUES TO #SMART-TBL
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Read_Run_Control() throws Exception                                                                                                                  //Natural: READ-RUN-CONTROL
    {
        if (BLNatReinput.isReinput()) return;

        //*  ================================
        //*  CALL THE SUB PROGRAM 'Set UP Run Control for MIT vs. WHO Comparison
        //*  PROCESS'. If a Critical ERROR is returned, Print Message & Terminate
        //*  THE JOB. COMMIT TRANSACTION FOR RUN-CONTROL SET UP.
        //*  ---------------------------------------------------------------------
        DbsUtil.callnat(Cwfn4815.class , getCurrentProcessState(), pdaCwfa4815.getCwfa4815_Output(), pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(),             //Natural: CALLNAT 'CWFN4815' CWFA4815-OUTPUT CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
            pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("E") || pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("W")))                    //Natural: IF MSG-INFO-SUB.##RETURN-CODE = 'E' OR MSG-INFO-SUB.##RETURN-CODE = 'W'
        {
            getReports().write(0, ReportOption.NOTITLE,new TabSetting(10),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                     //Natural: WRITE 10T MSG-INFO-SUB.##MSG
            if (Global.isEscape()) return;
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("E")))                                                                                 //Natural: IF MSG-INFO-SUB.##RETURN-CODE = 'E'
            {
                DbsUtil.terminate(10);  if (true) return;                                                                                                                 //Natural: TERMINATE 10
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().equals(999)))                                                                                          //Natural: IF MSG-INFO-SUB.##MSG-NR = 999
        {
            getReports().write(0, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(10),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                  //Natural: WRITE ( 0 ) NOTITLE NOHDR 10T MSG-INFO-SUB.##MSG
            if (Global.isEscape()) return;
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
            DbsUtil.terminate(11);  if (true) return;                                                                                                                     //Natural: TERMINATE 11
        }                                                                                                                                                                 //Natural: END-IF
        //*  ------------------------------------------
        GET1:                                                                                                                                                             //Natural: GET CWF-SUPPORT-TBL CWFA4815-OUTPUT.RUN-CONTROL-ISN
        ldaCwfl4815.getVw_cwf_Support_Tbl().readByID(pdaCwfa4815.getCwfa4815_Output_Run_Control_Isn().getLong(), "GET1");
        pnd_Start_Dte_Tme_A.setValueEdited(ldaCwfl4815.getCwf_Support_Tbl_Starting_Date(),new ReportEditMask("YYYYMMDDHHIISST"));                                         //Natural: MOVE EDITED CWF-SUPPORT-TBL.STARTING-DATE ( EM = YYYYMMDDHHIISST ) TO #START-DTE-TME-A
        pnd_End_Dte_Tme_A.setValueEdited(ldaCwfl4815.getCwf_Support_Tbl_Ending_Date(),new ReportEditMask("YYYYMMDDHHIISST"));                                             //Natural: MOVE EDITED CWF-SUPPORT-TBL.ENDING-DATE ( EM = YYYYMMDDHHIISST ) TO #END-DTE-TME-A
        pnd_Chnge_Dte_Tme_Log_Dte_Tme_Key_Pnd_Last_Chnge_Dte_Tme.setValue(pnd_Start_Dte_Tme_A_Pnd_Start_Dte_Tme_N);                                                       //Natural: ASSIGN #CHNGE-DTE-TME-LOG-DTE-TME-KEY.#LAST-CHNGE-DTE-TME := #START-DTE-TME-N
        //*  -------------------------------------------------
        //*  COMMIT THE TRANSACTION FOR THE RUN CONTROL SET UP
        //*  -------------------------------------------------
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,"CWFB4810 - Cont ISN ",pdaCwfa4815.getCwfa4815_Output_Run_Control_Isn());                                      //Natural: WRITE /'CWFB4810 - Cont ISN ' CWFA4815-OUTPUT.RUN-CONTROL-ISN
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  ------------------------------------------
    }
    private void sub_Select_Mit_Records() throws Exception                                                                                                                //Natural: SELECT-MIT-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        st = Global.getTIMN();                                                                                                                                            //Natural: SET TIME
        vw_cwf_Master.startDatabaseRead                                                                                                                                   //Natural: READ CWF-MASTER BY CHNGE-DTE-TME-LOG-DTE-TME-KEY STARTING FROM #CHNGE-DTE-TME-LOG-DTE-TME-KEY
        (
        "R1",
        new Wc[] { new Wc("CHNGE_DTE_TME_LOG_DTE_TME_KEY", ">=", pnd_Chnge_Dte_Tme_Log_Dte_Tme_Key, WcType.BY) },
        new Oc[] { new Oc("CHNGE_DTE_TME_LOG_DTE_TME_KEY", "ASC") }
        );
        R1:
        while (condition(vw_cwf_Master.readNextRow("R1")))
        {
            if (condition(cwf_Master_Last_Chnge_Dte_Tme.greater(pnd_End_Dte_Tme_A_Pnd_End_Dte_Tme_N)))                                                                    //Natural: IF CWF-MASTER.LAST-CHNGE-DTE-TME GT #END-DTE-TME-N
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*  TO SKIP ZERO PIN WRS 04/04/2000
            if (condition(cwf_Master_Pin_Nbr.lessOrEqual(getZero())))                                                                                                     //Natural: IF CWF-MASTER.PIN-NBR NOT GT 0
            {
                pnd_Zero_Pin.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #ZERO-PIN
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cwf_Master_Work_Prcss_Id.equals(pnd_Smart_Tbl_Pnd_Smart_Wpid.getValue("*"))))                                                                   //Natural: IF CWF-MASTER.WORK-PRCSS-ID = #SMART-WPID ( * )
            {
                pnd_Work_File_2_Pnd_Rqst_Log_Dte_Tme.setValue(cwf_Master_Rqst_Log_Dte_Tme);                                                                               //Natural: MOVE CWF-MASTER.RQST-LOG-DTE-TME TO #WORK-FILE-2.#RQST-LOG-DTE-TME
                getWorkFiles().write(2, false, pnd_Work_File_2);                                                                                                          //Natural: WRITE WORK FILE 2 #WORK-FILE-2
                pnd_Smart_Rec_Selctd.nadd(1);                                                                                                                             //Natural: ADD 1 TO #SMART-REC-SELCTD
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Work_File_1_Pnd_Rqst_Log_Dte_Tme.setValue(cwf_Master_Rqst_Log_Dte_Tme);                                                                                   //Natural: MOVE CWF-MASTER.RQST-LOG-DTE-TME TO #WORK-FILE-1.#RQST-LOG-DTE-TME
            getWorkFiles().write(1, false, pnd_Work_File_1);                                                                                                              //Natural: WRITE WORK FILE 1 #WORK-FILE-1
            pnd_Mit_Cntr_Selctd.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #MIT-CNTR-SELCTD
            //*  R1.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  (1600)
    }
    private void sub_Write_Totals_Report() throws Exception                                                                                                               //Natural: WRITE-TOTALS-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,"Program:",Global.getPROGRAM(),new TabSetting(30),"Date:",Global.getDATX(),NEWLINE,"*** Selection of RLDT for the following Window ***",NEWLINE,"Begining period    = ",pnd_Start_Dte_Tme_A,NEWLINE,"Ending   period    = ",pnd_End_Dte_Tme_A,NEWLINE,"**********************************************",NEWLINE,"Number of MIT Records selected  = ",pnd_Mit_Cntr_Selctd,NEWLINE,"Number of MIT Records w-Zero-PIN= ",pnd_Zero_Pin,NEWLINE,"Number of SMART Recs. selected  = ",pnd_Smart_Rec_Selctd,NEWLINE,"**********************************************",NEWLINE,"Elapsed Time to Read records","(HH:MM:SS.T) :",st,  //Natural: WRITE ( 0 ) / 'Program:' *PROGRAM 30T 'Date:' *DATX / '*** Selection of RLDT for the following Window ***' / 'Begining period    = ' #START-DTE-TME-A / 'Ending   period    = ' #END-DTE-TME-A / '**********************************************' / 'Number of MIT Records selected  = ' #MIT-CNTR-SELCTD / 'Number of MIT Records w-Zero-PIN= ' #ZERO-PIN / 'Number of SMART Recs. selected  = ' #SMART-REC-SELCTD / '**********************************************' / 'Elapsed Time to Read records' '(HH:MM:SS.T) :' *TIMD ( ST. ) ( EM = 99:99:99'.'9 )
            new ReportEditMask ("99:99:99'.'9"));
        if (Global.isEscape()) return;
    }
    private void sub_Update_Run_Control() throws Exception                                                                                                                //Natural: UPDATE-RUN-CONTROL
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************
        //*  PULL CONTROL RECORD TO APPLY STATISTIC COUNTERS
        //*  -----------------------------------------------
        GET_2:                                                                                                                                                            //Natural: GET CWF-SUPPORT-TBL CWFA4815-OUTPUT.RUN-CONTROL-ISN
        ldaCwfl4815.getVw_cwf_Support_Tbl().readByID(pdaCwfa4815.getCwfa4815_Output_Run_Control_Isn().getLong(), "GET_2");
        ldaCwfl4815.getCwf_Support_Tbl_Program_Name().setValue(Global.getPROGRAM());                                                                                      //Natural: ASSIGN CWF-SUPPORT-TBL.PROGRAM-NAME := *PROGRAM
        ldaCwfl4815.getCwf_Support_Tbl_Number_Of_Mit_Wr_Selected().setValue(pnd_Mit_Cntr_Selctd);                                                                         //Natural: ASSIGN CWF-SUPPORT-TBL.NUMBER-OF-MIT-WR-SELECTED := #MIT-CNTR-SELCTD
        ldaCwfl4815.getCwf_Support_Tbl_Run_Date().setValue(Global.getTIMX());                                                                                             //Natural: ASSIGN CWF-SUPPORT-TBL.RUN-DATE := *TIMX
        ldaCwfl4815.getVw_cwf_Support_Tbl().updateDBRow("GET_2");                                                                                                         //Natural: UPDATE ( GET-2. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  (2210)
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=56 LS=131");
    }
}
