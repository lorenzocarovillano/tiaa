/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:45:42 PM
**        * FROM NATURAL PROGRAM : Cwfb8613
************************************************************
**        * FILE NAME            : Cwfb8613.java
**        * CLASS NAME           : Cwfb8613
**        * INSTANCE NAME        : Cwfb8613
************************************************************
************************************************************************
*
* SECURITY CERTIFICATION REMEDIATION
*
* READ SORTED WORKFILE CREATED FROM CWF-ORG-EMPL-TBL4 (045/184);
* CREATE CWF AND EWS REPORTS.
*
* 2008, 07 - DB USING CWF TABLE FOR RACF CRITERIA.
*
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb8613 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Work_File;
    private DbsField pnd_Work_File_Pnd_Wf_A;
    private DbsField pnd_Work_File_Pnd_Wf_B;

    private DbsGroup pnd_Work_File__R_Field_1;
    private DbsField pnd_Work_File_Pnd_Wf_System;
    private DbsField pnd_Work_File_Pnd_Wf_Action_Dte;
    private DbsField pnd_Work_File_Pnd_Wf_Empl_Racf;
    private DbsField pnd_Work_File_Pnd_Wf_Unit_Cde;
    private DbsField pnd_Work_File_Pnd_Wf_Action;
    private DbsField pnd_Work_File_Pnd_Wf_Admin_Racf;
    private DbsField pnd_Work_File_Pnd_Wf_Empl_System;
    private DbsField pnd_Work_File_Pnd_Wf_Empl_Scrty;

    private DbsGroup pnd_Work_File__R_Field_2;
    private DbsField pnd_Work_File__Filler1;
    private DbsField pnd_Work_File_Pnd_Wf_Start_Dte;
    private DbsField pnd_Work_File_Pnd_Wf_End_Dte;
    private DbsField pnd_I;
    private DbsField pnd_Cwf_Id1;
    private DbsField pnd_Cwf_Id2;
    private DbsField pnd_Cwf_Id3;
    private DbsField pnd_Cwf_Id4;
    private DbsField pnd_Cwf_Id5;
    private DbsField pnd_Cwf_Ct;
    private DbsField pnd_Ews_Id1;
    private DbsField pnd_Ews_Id2;
    private DbsField pnd_Ews_Id3;
    private DbsField pnd_Ews_Id4;
    private DbsField pnd_Ews_Id5;
    private DbsField pnd_Ews_Ct;
    private DbsField pnd_T_Start_Dte;
    private DbsField pnd_T_End_Dte;

    private DbsGroup pnd_Table_Var;
    private DbsField pnd_Table_Var_Pnd_Cwf_Sec_Id;

    private DbsGroup pnd_Table_Var__R_Field_3;
    private DbsField pnd_Table_Var_Pnd_Cwf_Sec_Id_Array;
    private DbsField pnd_Table_Var_Pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Table_Var__R_Field_4;
    private DbsField pnd_Table_Var_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Table_Var_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Table_Var_Pnd_Tbl_Key_Field;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Work_File = localVariables.newGroupInRecord("pnd_Work_File", "#WORK-FILE");
        pnd_Work_File_Pnd_Wf_A = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Wf_A", "#WF-A", FieldType.STRING, 240);
        pnd_Work_File_Pnd_Wf_B = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Wf_B", "#WF-B", FieldType.STRING, 60);

        pnd_Work_File__R_Field_1 = localVariables.newGroupInRecord("pnd_Work_File__R_Field_1", "REDEFINE", pnd_Work_File);
        pnd_Work_File_Pnd_Wf_System = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wf_System", "#WF-SYSTEM", FieldType.STRING, 3);
        pnd_Work_File_Pnd_Wf_Action_Dte = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wf_Action_Dte", "#WF-ACTION-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_File_Pnd_Wf_Empl_Racf = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wf_Empl_Racf", "#WF-EMPL-RACF", FieldType.STRING, 
            8);
        pnd_Work_File_Pnd_Wf_Unit_Cde = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wf_Unit_Cde", "#WF-UNIT-CDE", FieldType.STRING, 8);
        pnd_Work_File_Pnd_Wf_Action = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wf_Action", "#WF-ACTION", FieldType.STRING, 5);
        pnd_Work_File_Pnd_Wf_Admin_Racf = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wf_Admin_Racf", "#WF-ADMIN-RACF", FieldType.STRING, 
            8);
        pnd_Work_File_Pnd_Wf_Empl_System = pnd_Work_File__R_Field_1.newFieldArrayInGroup("pnd_Work_File_Pnd_Wf_Empl_System", "#WF-EMPL-SYSTEM", FieldType.STRING, 
            8, new DbsArrayController(1, 20));
        pnd_Work_File_Pnd_Wf_Empl_Scrty = pnd_Work_File__R_Field_1.newFieldArrayInGroup("pnd_Work_File_Pnd_Wf_Empl_Scrty", "#WF-EMPL-SCRTY", FieldType.STRING, 
            2, new DbsArrayController(1, 20));

        pnd_Work_File__R_Field_2 = localVariables.newGroupInRecord("pnd_Work_File__R_Field_2", "REDEFINE", pnd_Work_File);
        pnd_Work_File__Filler1 = pnd_Work_File__R_Field_2.newFieldInGroup("pnd_Work_File__Filler1", "_FILLER1", FieldType.STRING, 3);
        pnd_Work_File_Pnd_Wf_Start_Dte = pnd_Work_File__R_Field_2.newFieldInGroup("pnd_Work_File_Pnd_Wf_Start_Dte", "#WF-START-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_File_Pnd_Wf_End_Dte = pnd_Work_File__R_Field_2.newFieldInGroup("pnd_Work_File_Pnd_Wf_End_Dte", "#WF-END-DTE", FieldType.NUMERIC, 8);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_Cwf_Id1 = localVariables.newFieldInRecord("pnd_Cwf_Id1", "#CWF-ID1", FieldType.NUMERIC, 7);
        pnd_Cwf_Id2 = localVariables.newFieldInRecord("pnd_Cwf_Id2", "#CWF-ID2", FieldType.NUMERIC, 7);
        pnd_Cwf_Id3 = localVariables.newFieldInRecord("pnd_Cwf_Id3", "#CWF-ID3", FieldType.NUMERIC, 7);
        pnd_Cwf_Id4 = localVariables.newFieldInRecord("pnd_Cwf_Id4", "#CWF-ID4", FieldType.NUMERIC, 7);
        pnd_Cwf_Id5 = localVariables.newFieldInRecord("pnd_Cwf_Id5", "#CWF-ID5", FieldType.NUMERIC, 7);
        pnd_Cwf_Ct = localVariables.newFieldInRecord("pnd_Cwf_Ct", "#CWF-CT", FieldType.NUMERIC, 7);
        pnd_Ews_Id1 = localVariables.newFieldInRecord("pnd_Ews_Id1", "#EWS-ID1", FieldType.NUMERIC, 7);
        pnd_Ews_Id2 = localVariables.newFieldInRecord("pnd_Ews_Id2", "#EWS-ID2", FieldType.NUMERIC, 7);
        pnd_Ews_Id3 = localVariables.newFieldInRecord("pnd_Ews_Id3", "#EWS-ID3", FieldType.NUMERIC, 7);
        pnd_Ews_Id4 = localVariables.newFieldInRecord("pnd_Ews_Id4", "#EWS-ID4", FieldType.NUMERIC, 7);
        pnd_Ews_Id5 = localVariables.newFieldInRecord("pnd_Ews_Id5", "#EWS-ID5", FieldType.NUMERIC, 7);
        pnd_Ews_Ct = localVariables.newFieldInRecord("pnd_Ews_Ct", "#EWS-CT", FieldType.NUMERIC, 7);
        pnd_T_Start_Dte = localVariables.newFieldInRecord("pnd_T_Start_Dte", "#T-START-DTE", FieldType.NUMERIC, 8);
        pnd_T_End_Dte = localVariables.newFieldInRecord("pnd_T_End_Dte", "#T-END-DTE", FieldType.NUMERIC, 8);

        pnd_Table_Var = localVariables.newGroupInRecord("pnd_Table_Var", "#TABLE-VAR");
        pnd_Table_Var_Pnd_Cwf_Sec_Id = pnd_Table_Var.newFieldInGroup("pnd_Table_Var_Pnd_Cwf_Sec_Id", "#CWF-SEC-ID", FieldType.STRING, 40);

        pnd_Table_Var__R_Field_3 = pnd_Table_Var.newGroupInGroup("pnd_Table_Var__R_Field_3", "REDEFINE", pnd_Table_Var_Pnd_Cwf_Sec_Id);
        pnd_Table_Var_Pnd_Cwf_Sec_Id_Array = pnd_Table_Var__R_Field_3.newFieldArrayInGroup("pnd_Table_Var_Pnd_Cwf_Sec_Id_Array", "#CWF-SEC-ID-ARRAY", 
            FieldType.STRING, 8, new DbsArrayController(1, 5));
        pnd_Table_Var_Pnd_Tbl_Prime_Key = pnd_Table_Var.newFieldInGroup("pnd_Table_Var_Pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Table_Var__R_Field_4 = pnd_Table_Var.newGroupInGroup("pnd_Table_Var__R_Field_4", "REDEFINE", pnd_Table_Var_Pnd_Tbl_Prime_Key);
        pnd_Table_Var_Pnd_Tbl_Scrty_Level_Ind = pnd_Table_Var__R_Field_4.newFieldInGroup("pnd_Table_Var_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Table_Var_Pnd_Tbl_Table_Nme = pnd_Table_Var__R_Field_4.newFieldInGroup("pnd_Table_Var_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Table_Var_Pnd_Tbl_Key_Field = pnd_Table_Var__R_Field_4.newFieldInGroup("pnd_Table_Var_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb8613() throws Exception
    {
        super("Cwfb8613");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 55;//Natural: FORMAT ( 2 ) PS = 55
        //*  RETRIEVE THE CWF SEC IDS    /* GH 2008, JUNE
        //*  MOVE 'S '                        TO #TBL-SCRTY-LEVEL-IND
        //*  MOVE 'CWF-EWS-SEC-CERTIFY'       TO #TBL-TABLE-NME
        //*  MOVE 'CWF PRODUCTION SUPPORT ID' TO #TBL-KEY-FIELD
        //*  READ(1) CWF-SUPTB WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
        //*    IF TBL-SCRTY-LEVEL-IND NE #TBL-SCRTY-LEVEL-IND
        //*        OR TBL-TABLE-NME   NE #TBL-TABLE-NME
        //*      OR TBL-KEY-FIELD   NE #TBL-KEY-FIELD
        //*     ESCAPE ROUTINE
        //*   END-IF
        //*   MOVE TBL-DATA-FIELD TO #CWF-SEC-ID
        //*  END-READ
        getWorkFiles().read(1, pnd_Work_File);                                                                                                                            //Natural: READ WORK 1 ONCE #WORK-FILE
        pnd_T_Start_Dte.setValue(pnd_Work_File_Pnd_Wf_Start_Dte);                                                                                                         //Natural: ASSIGN #T-START-DTE := #WF-START-DTE
        pnd_T_End_Dte.setValue(pnd_Work_File_Pnd_Wf_End_Dte);                                                                                                             //Natural: ASSIGN #T-END-DTE := #WF-END-DTE
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT *PROGRAM 20X 'Corporate Workflow' / *DATX ( EM = LLL' 'DD', 'YYYY ) 10X 'Security Compliance Remediation' / 'Page:' *PAGE-NUMBER ( 1 ) ( EM = ZZZ9 ) 14X 'From' #T-START-DTE 'To' #T-END-DTE /
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 2 ) TITLE LEFT *PROGRAM 22X 'Electronic Warrant System' / *DATX ( EM = LLL' 'DD', 'YYYY ) 15X 'Security Compliance Remediation' / 'Page:' *PAGE-NUMBER ( 2 ) ( EM = ZZZ9 ) 19X 'From' #T-START-DTE 'To' #T-END-DTE /
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #WORK-FILE
        while (condition(getWorkFiles().read(1, pnd_Work_File)))
        {
            short decideConditionsMet100 = 0;                                                                                                                             //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #WF-SYSTEM = 'CWF'
            if (condition(pnd_Work_File_Pnd_Wf_System.equals("CWF")))
            {
                decideConditionsMet100++;
                getReports().display(1, new ColumnSpacing(15),"Process/Date",                                                                                             //Natural: DISPLAY ( 1 ) 15X 'Process/Date' #WF-ACTION-DTE 'Racf/Modified' #WF-EMPL-RACF '/Unit' #WF-UNIT-CDE '/Action' #WF-ACTION 'Admin/Racf id' #WF-ADMIN-RACF
                		pnd_Work_File_Pnd_Wf_Action_Dte,"Racf/Modified",
                		pnd_Work_File_Pnd_Wf_Empl_Racf,"/Unit",
                		pnd_Work_File_Pnd_Wf_Unit_Cde,"/Action",
                		pnd_Work_File_Pnd_Wf_Action,"Admin/Racf id",
                		pnd_Work_File_Pnd_Wf_Admin_Racf);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Cwf_Ct.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #CWF-CT
                //*      DECIDE ON FIRST VALUE OF #WF-ADMIN-RACF
                //*         VALUE  #CWF-SEC-ID-ARRAY(1)
                //*           ADD 1 TO #CWF-ID1
                //*         VALUE  #CWF-SEC-ID-ARRAY(2)
                //*           ADD 1 TO #CWF-ID2
                //*         VALUE  #CWF-SEC-ID-ARRAY(3)
                //*           ADD 1 TO #CWF-ID3
                //*         VALUE  #CWF-SEC-ID-ARRAY(4)
                //*           ADD 1 TO #CWF-ID4
                //*         VALUE  #CWF-SEC-ID-ARRAY(5)
                //*           ADD 1 TO #CWF-ID5
                //*         ANY VALUES
                //*           ADD 1 TO #CWF-CT
                //*         NONE
                //*           IGNORE
                //*       END-DECIDE
            }                                                                                                                                                             //Natural: WHEN #WF-SYSTEM = 'EWS'
            if (condition(pnd_Work_File_Pnd_Wf_System.equals("EWS")))
            {
                decideConditionsMet100++;
                getReports().display(2, new ColumnSpacing(10),"Process/Date",                                                                                             //Natural: DISPLAY ( 2 ) 10X 'Process/Date' #WF-ACTION-DTE 'Racf/Modified' #WF-EMPL-RACF '/Unit' #WF-UNIT-CDE '/Action' #WF-ACTION 'Admin/Racf id' #WF-ADMIN-RACF '/System(s)' #WF-EMPL-SYSTEM ( 1 ) '/Security' #WF-EMPL-SCRTY ( 1 )
                		pnd_Work_File_Pnd_Wf_Action_Dte,"Racf/Modified",
                		pnd_Work_File_Pnd_Wf_Empl_Racf,"/Unit",
                		pnd_Work_File_Pnd_Wf_Unit_Cde,"/Action",
                		pnd_Work_File_Pnd_Wf_Action,"Admin/Racf id",
                		pnd_Work_File_Pnd_Wf_Admin_Racf,"/System(s)",
                		pnd_Work_File_Pnd_Wf_Empl_System.getValue(1),"/Security",
                		pnd_Work_File_Pnd_Wf_Empl_Scrty.getValue(1));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Ews_Ct.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #EWS-CT
                FOR01:                                                                                                                                                    //Natural: FOR #I 2 8
                for (pnd_I.setValue(2); condition(pnd_I.lessOrEqual(8)); pnd_I.nadd(1))
                {
                    if (condition(pnd_Work_File_Pnd_Wf_Empl_System.getValue(pnd_I).notEquals(" ")))                                                                       //Natural: IF #WF-EMPL-SYSTEM ( #I ) NE ' '
                    {
                        getReports().write(2, new ReportTAsterisk(pnd_Work_File_Pnd_Wf_Empl_System.getValue(1)),pnd_Work_File_Pnd_Wf_Empl_System.getValue(pnd_I),new      //Natural: WRITE ( 2 ) T*#WF-EMPL-SYSTEM ( 1 ) #WF-EMPL-SYSTEM ( #I ) T*#WF-EMPL-SCRTY ( 1 ) #WF-EMPL-SCRTY ( #I )
                            ReportTAsterisk(pnd_Work_File_Pnd_Wf_Empl_Scrty.getValue(1)),pnd_Work_File_Pnd_Wf_Empl_Scrty.getValue(pnd_I));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*       DECIDE ON FIRST VALUE OF #WF-ADMIN-RACF
                //*         VALUE  #CWF-SEC-ID-ARRAY(1)
                //*           ADD 1 TO #EWS-ID1
                //*         VALUE  #CWF-SEC-ID-ARRAY(2)
                //*           ADD 1 TO #EWS-ID2
                //*         VALUE  #CWF-SEC-ID-ARRAY(3)
                //*           ADD 1 TO #EWS-ID3
                //*         VALUE  #CWF-SEC-ID-ARRAY(4)
                //*           ADD 1 TO #EWS-ID4
                //*         VALUE  #CWF-SEC-ID-ARRAY(5)
                //*           ADD 1 TO #EWS-ID5
                //*         ANY VALUES
                //*           ADD 1 TO #EWS-CT
                //*         NONE
                //*           IGNORE
                //*       END-DECIDE
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet100 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_Cwf_Ct.equals(getZero())))                                                                                                                      //Natural: IF #CWF-CT = 0
        {
            getReports().write(1, NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"******************************************************************",                   //Natural: WRITE ( 1 ) ///// / '******************************************************************' / '***                                                            ***' / '***           No Activities for Corporate Workflow             ***' / '***                                                            ***' / '******************************************************************'
                NEWLINE,"***                                                            ***",NEWLINE,"***           No Activities for Corporate Workflow             ***",
                NEWLINE,"***                                                            ***",NEWLINE,"******************************************************************");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ews_Ct.equals(getZero())))                                                                                                                      //Natural: IF #EWS-CT = 0
        {
            getReports().write(2, NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"******************************************************************",                   //Natural: WRITE ( 2 ) ///// / '******************************************************************' / '***                                                            ***' / '***           No Activities for Electronic Warrants            ***' / '***                                                            ***' / '******************************************************************'
                NEWLINE,"***                                                            ***",NEWLINE,"***           No Activities for Electronic Warrants            ***",
                NEWLINE,"***                                                            ***",NEWLINE,"******************************************************************");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(1, NEWLINE,NEWLINE,NEWLINE,NEWLINE,pnd_Cwf_Ct, new ReportEditMask ("ZZZZ,ZZ9"),"TOTAL CWF");                                                   //Natural: WRITE ( 1 ) /// / #CWF-CT ( EM = ZZZZ,ZZ9 ) 'TOTAL CWF'
        if (Global.isEscape()) return;
        //*   / #CWF-ID1             (EM=ZZZZ,ZZ9) #CWF-SEC-ID-ARRAY(1)
        //*   / #CWF-ID2             (EM=ZZZZ,ZZ9) #CWF-SEC-ID-ARRAY(2)
        //*   / #CWF-ID3             (EM=ZZZZ,ZZ9) #CWF-SEC-ID-ARRAY(3)
        //*   / #CWF-ID4             (EM=ZZZZ,ZZ9) #CWF-SEC-ID-ARRAY(4)
        //*   / #CWF-ID5             (EM=ZZZZ,ZZ9) #CWF-SEC-ID-ARRAY(5)
        getReports().write(2, NEWLINE,NEWLINE,NEWLINE,NEWLINE,pnd_Ews_Ct, new ReportEditMask ("ZZZZ,ZZ9"),"TOTAL EWS");                                                   //Natural: WRITE ( 2 ) /// / #EWS-CT ( EM = ZZZZ,ZZ9 ) 'TOTAL EWS'
        if (Global.isEscape()) return;
        //*   / #EWS-ID1             (EM=ZZZZ,ZZ9) #CWF-SEC-ID-ARRAY(1)
        //*   / #EWS-ID2             (EM=ZZZZ,ZZ9) #CWF-SEC-ID-ARRAY(2)
        //*   / #EWS-ID3             (EM=ZZZZ,ZZ9) #CWF-SEC-ID-ARRAY(3)
        //*   / #EWS-ID4             (EM=ZZZZ,ZZ9) #CWF-SEC-ID-ARRAY(4)
        //*   / #EWS-ID5             (EM=ZZZZ,ZZ9) #CWF-SEC-ID-ARRAY(5)
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "PS=55");
        Global.format(2, "PS=55");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getPROGRAM(),new ColumnSpacing(20),"Corporate Workflow",NEWLINE,Global.getDATX(), 
            new ReportEditMask ("LLL' 'DD', 'YYYY"),new ColumnSpacing(10),"Security Compliance Remediation",NEWLINE,"Page:",getReports().getPageNumberDbs(1), 
            new ReportEditMask ("ZZZ9"),new ColumnSpacing(14),"From",pnd_T_Start_Dte,"To",pnd_T_End_Dte,NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getPROGRAM(),new ColumnSpacing(22),"Electronic Warrant System",NEWLINE,Global.getDATX(), 
            new ReportEditMask ("LLL' 'DD', 'YYYY"),new ColumnSpacing(15),"Security Compliance Remediation",NEWLINE,"Page:",getReports().getPageNumberDbs(2), 
            new ReportEditMask ("ZZZ9"),new ColumnSpacing(19),"From",pnd_T_Start_Dte,"To",pnd_T_End_Dte,NEWLINE);

        getReports().setDisplayColumns(1, new ColumnSpacing(15),"Process/Date",
        		pnd_Work_File_Pnd_Wf_Action_Dte,"Racf/Modified",
        		pnd_Work_File_Pnd_Wf_Empl_Racf,"/Unit",
        		pnd_Work_File_Pnd_Wf_Unit_Cde,"/Action",
        		pnd_Work_File_Pnd_Wf_Action,"Admin/Racf id",
        		pnd_Work_File_Pnd_Wf_Admin_Racf);
        getReports().setDisplayColumns(2, new ColumnSpacing(10),"Process/Date",
        		pnd_Work_File_Pnd_Wf_Action_Dte,"Racf/Modified",
        		pnd_Work_File_Pnd_Wf_Empl_Racf,"/Unit",
        		pnd_Work_File_Pnd_Wf_Unit_Cde,"/Action",
        		pnd_Work_File_Pnd_Wf_Action,"Admin/Racf id",
        		pnd_Work_File_Pnd_Wf_Admin_Racf,"/System(s)",
        		pnd_Work_File_Pnd_Wf_Empl_System,"/Security",
        		pnd_Work_File_Pnd_Wf_Empl_Scrty);
    }
}
