/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:35:20 PM
**        * FROM NATURAL PROGRAM : Cwfb4818
************************************************************
**        * FILE NAME            : Cwfb4818.java
**        * CLASS NAME           : Cwfb4818
**        * INSTANCE NAME        : Cwfb4818
************************************************************
**********************************************************************
* PROGRAM  : CWFB4818
* SYSTEM   : CRPCWF
* TITLE    : CORPORATE REPORTING EXTRACT FROM EFM-CABINET FILE
* FUNCTION : THIS PROGRAM EXTRACT ALL EFM-CABINET RECORDS AND CREATES
*          : A "Flat" FILE TO BE LOADED EVERY NIGHT TO ORACLE TABLE.
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
**********************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb4818 extends BLNatBase
{
    // Data Areas
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private LdaCwftotal ldaCwftotal;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup work_File_1;
    private DbsField work_File_1_Pin_Nbr;
    private DbsField work_File_1_A1;
    private DbsField work_File_1_Sc_Rqst_Log_Dte_Tme_A;
    private DbsField work_File_1_A2;
    private DbsField work_File_1_Sc_Rldt_Date_D;

    private DataAccessProgramView vw_cwf_Efm_Cabinet;
    private DbsField cwf_Efm_Cabinet_Cabinet_Id;
    private DbsField cwf_Efm_Cabinet_Audit_Dte_Tme;
    private DbsField cwf_Efm_Cabinet_Srvce_Cncrn_Rqst_Log_Dte_Tme;

    private DbsGroup total_Counters;
    private DbsField total_Counters_Work_Read_Cntr;
    private DbsField total_Counters_Wrqst_Add_Cntr;
    private DbsField total_Counters_Wrqst_Upd_Cntr;
    private DbsField total_Counters_Unita_Add_Cntr;
    private DbsField total_Counters_Unita_Upd_Cntr;
    private DbsField total_Counters_Empla_Add_Cntr;
    private DbsField total_Counters_Empla_Upd_Cntr;
    private DbsField total_Counters_Stepa_Add_Cntr;
    private DbsField total_Counters_Stepa_Upd_Cntr;
    private DbsField total_Counters_Extra_Add_Cntr;
    private DbsField total_Counters_Extra_Upd_Cntr;
    private DbsField total_Counters_Intra_Add_Cntr;
    private DbsField total_Counters_Intra_Upd_Cntr;
    private DbsField total_Counters_Enrta_Add_Cntr;
    private DbsField total_Counters_Enrta_Upd_Cntr;
    private DbsField total_Counters_Contr_Add_Cntr;
    private DbsField total_Counters_Contr_Upd_Cntr;
    private DbsField total_Counters_Cstat_Add_Cntr;
    private DbsField total_Counters_Merg_Delt_Cntr;
    private DbsField total_Counters_Adtnl_Upd_Cntr;
    private DbsField total_Counters_Adtnl_Add_Cntr;
    private DbsField total_Counters_Relat_Add_Cntr;
    private DbsField total_Counters_Efmcab_Add_Cntr;
    private DbsField total_Counters_Wpidind_Add_Cntr;
    private DbsField pnd_Input_Cntr;
    private DbsField pnd_Outpt_Cntr;
    private DbsField pnd_Skip_Cntr;
    private DbsField pnd_Isn;
    private DbsField pnd_Time_T;
    private DbsField pnd_Date_D;
    private DbsField pnd_Atsign;
    private DbsField pnd_Cabinet_Split;

    private DbsGroup pnd_Cabinet_Split__R_Field_1;
    private DbsField pnd_Cabinet_Split_Pnd_Cabinet_Ident;
    private DbsField pnd_Cabinet_Split_Pnd_Cabinet_Pin;

    private DbsGroup pnd_Cabinet_Split__R_Field_2;
    private DbsField pnd_Cabinet_Split_Pnd_Cabinet_Pin_5;
    private DbsField pnd_Cabinet_Split_Pnd_Cabinet_Pin_7;
    private DbsField pnd_Rldt_Split;

    private DbsGroup pnd_Rldt_Split__R_Field_3;
    private DbsField pnd_Rldt_Split_Pnd_Rldt_Yyyymmdd;

    private DbsRecord setTimeRecord;
    private DbsField st;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        ldaCwftotal = new LdaCwftotal();
        registerRecord(ldaCwftotal);

        // Local Variables

        work_File_1 = localVariables.newGroupInRecord("work_File_1", "WORK-FILE-1");
        work_File_1_Pin_Nbr = work_File_1.newFieldInGroup("work_File_1_Pin_Nbr", "PIN-NBR", FieldType.STRING, 7);
        work_File_1_A1 = work_File_1.newFieldInGroup("work_File_1_A1", "A1", FieldType.STRING, 1);
        work_File_1_Sc_Rqst_Log_Dte_Tme_A = work_File_1.newFieldInGroup("work_File_1_Sc_Rqst_Log_Dte_Tme_A", "SC-RQST-LOG-DTE-TME-A", FieldType.STRING, 
            26);
        work_File_1_A2 = work_File_1.newFieldInGroup("work_File_1_A2", "A2", FieldType.STRING, 1);
        work_File_1_Sc_Rldt_Date_D = work_File_1.newFieldInGroup("work_File_1_Sc_Rldt_Date_D", "SC-RLDT-DATE-D", FieldType.STRING, 24);

        vw_cwf_Efm_Cabinet = new DataAccessProgramView(new NameInfo("vw_cwf_Efm_Cabinet", "CWF-EFM-CABINET"), "CWF_EFM_CABINET", "CWF_EFM_CABINET");
        cwf_Efm_Cabinet_Cabinet_Id = vw_cwf_Efm_Cabinet.getRecord().newFieldInGroup("cwf_Efm_Cabinet_Cabinet_Id", "CABINET-ID", FieldType.STRING, 14, 
            RepeatingFieldStrategy.None, "CABINET_ID");
        cwf_Efm_Cabinet_Cabinet_Id.setDdmHeader("CABINET/ID");
        cwf_Efm_Cabinet_Audit_Dte_Tme = vw_cwf_Efm_Cabinet.getRecord().newFieldInGroup("cwf_Efm_Cabinet_Audit_Dte_Tme", "AUDIT-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "AUDIT_DTE_TME");
        cwf_Efm_Cabinet_Audit_Dte_Tme.setDdmHeader("DATE/& TIME");
        cwf_Efm_Cabinet_Srvce_Cncrn_Rqst_Log_Dte_Tme = vw_cwf_Efm_Cabinet.getRecord().newFieldInGroup("cwf_Efm_Cabinet_Srvce_Cncrn_Rqst_Log_Dte_Tme", 
            "SRVCE-CNCRN-RQST-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "SRVCE_CNCRN_RQST_LOG_DTE_TME");
        registerRecord(vw_cwf_Efm_Cabinet);

        total_Counters = localVariables.newGroupInRecord("total_Counters", "TOTAL-COUNTERS");
        total_Counters_Work_Read_Cntr = total_Counters.newFieldInGroup("total_Counters_Work_Read_Cntr", "WORK-READ-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Wrqst_Add_Cntr = total_Counters.newFieldInGroup("total_Counters_Wrqst_Add_Cntr", "WRQST-ADD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Wrqst_Upd_Cntr = total_Counters.newFieldInGroup("total_Counters_Wrqst_Upd_Cntr", "WRQST-UPD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Unita_Add_Cntr = total_Counters.newFieldInGroup("total_Counters_Unita_Add_Cntr", "UNITA-ADD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Unita_Upd_Cntr = total_Counters.newFieldInGroup("total_Counters_Unita_Upd_Cntr", "UNITA-UPD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Empla_Add_Cntr = total_Counters.newFieldInGroup("total_Counters_Empla_Add_Cntr", "EMPLA-ADD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Empla_Upd_Cntr = total_Counters.newFieldInGroup("total_Counters_Empla_Upd_Cntr", "EMPLA-UPD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Stepa_Add_Cntr = total_Counters.newFieldInGroup("total_Counters_Stepa_Add_Cntr", "STEPA-ADD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Stepa_Upd_Cntr = total_Counters.newFieldInGroup("total_Counters_Stepa_Upd_Cntr", "STEPA-UPD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Extra_Add_Cntr = total_Counters.newFieldInGroup("total_Counters_Extra_Add_Cntr", "EXTRA-ADD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Extra_Upd_Cntr = total_Counters.newFieldInGroup("total_Counters_Extra_Upd_Cntr", "EXTRA-UPD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Intra_Add_Cntr = total_Counters.newFieldInGroup("total_Counters_Intra_Add_Cntr", "INTRA-ADD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Intra_Upd_Cntr = total_Counters.newFieldInGroup("total_Counters_Intra_Upd_Cntr", "INTRA-UPD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Enrta_Add_Cntr = total_Counters.newFieldInGroup("total_Counters_Enrta_Add_Cntr", "ENRTA-ADD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Enrta_Upd_Cntr = total_Counters.newFieldInGroup("total_Counters_Enrta_Upd_Cntr", "ENRTA-UPD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Contr_Add_Cntr = total_Counters.newFieldInGroup("total_Counters_Contr_Add_Cntr", "CONTR-ADD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Contr_Upd_Cntr = total_Counters.newFieldInGroup("total_Counters_Contr_Upd_Cntr", "CONTR-UPD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Cstat_Add_Cntr = total_Counters.newFieldInGroup("total_Counters_Cstat_Add_Cntr", "CSTAT-ADD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Merg_Delt_Cntr = total_Counters.newFieldInGroup("total_Counters_Merg_Delt_Cntr", "MERG-DELT-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Adtnl_Upd_Cntr = total_Counters.newFieldInGroup("total_Counters_Adtnl_Upd_Cntr", "ADTNL-UPD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Adtnl_Add_Cntr = total_Counters.newFieldInGroup("total_Counters_Adtnl_Add_Cntr", "ADTNL-ADD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Relat_Add_Cntr = total_Counters.newFieldInGroup("total_Counters_Relat_Add_Cntr", "RELAT-ADD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Efmcab_Add_Cntr = total_Counters.newFieldInGroup("total_Counters_Efmcab_Add_Cntr", "EFMCAB-ADD-CNTR", FieldType.NUMERIC, 9);
        total_Counters_Wpidind_Add_Cntr = total_Counters.newFieldInGroup("total_Counters_Wpidind_Add_Cntr", "WPIDIND-ADD-CNTR", FieldType.NUMERIC, 9);
        pnd_Input_Cntr = localVariables.newFieldInRecord("pnd_Input_Cntr", "#INPUT-CNTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Outpt_Cntr = localVariables.newFieldInRecord("pnd_Outpt_Cntr", "#OUTPT-CNTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Skip_Cntr = localVariables.newFieldInRecord("pnd_Skip_Cntr", "#SKIP-CNTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Time_T = localVariables.newFieldInRecord("pnd_Time_T", "#TIME-T", FieldType.TIME);
        pnd_Date_D = localVariables.newFieldInRecord("pnd_Date_D", "#DATE-D", FieldType.DATE);
        pnd_Atsign = localVariables.newFieldInRecord("pnd_Atsign", "#ATSIGN", FieldType.STRING, 1);
        pnd_Cabinet_Split = localVariables.newFieldInRecord("pnd_Cabinet_Split", "#CABINET-SPLIT", FieldType.STRING, 14);

        pnd_Cabinet_Split__R_Field_1 = localVariables.newGroupInRecord("pnd_Cabinet_Split__R_Field_1", "REDEFINE", pnd_Cabinet_Split);
        pnd_Cabinet_Split_Pnd_Cabinet_Ident = pnd_Cabinet_Split__R_Field_1.newFieldInGroup("pnd_Cabinet_Split_Pnd_Cabinet_Ident", "#CABINET-IDENT", FieldType.STRING, 
            1);
        pnd_Cabinet_Split_Pnd_Cabinet_Pin = pnd_Cabinet_Split__R_Field_1.newFieldInGroup("pnd_Cabinet_Split_Pnd_Cabinet_Pin", "#CABINET-PIN", FieldType.STRING, 
            12);

        pnd_Cabinet_Split__R_Field_2 = pnd_Cabinet_Split__R_Field_1.newGroupInGroup("pnd_Cabinet_Split__R_Field_2", "REDEFINE", pnd_Cabinet_Split_Pnd_Cabinet_Pin);
        pnd_Cabinet_Split_Pnd_Cabinet_Pin_5 = pnd_Cabinet_Split__R_Field_2.newFieldInGroup("pnd_Cabinet_Split_Pnd_Cabinet_Pin_5", "#CABINET-PIN-5", FieldType.STRING, 
            5);
        pnd_Cabinet_Split_Pnd_Cabinet_Pin_7 = pnd_Cabinet_Split__R_Field_2.newFieldInGroup("pnd_Cabinet_Split_Pnd_Cabinet_Pin_7", "#CABINET-PIN-7", FieldType.STRING, 
            7);
        pnd_Rldt_Split = localVariables.newFieldInRecord("pnd_Rldt_Split", "#RLDT-SPLIT", FieldType.STRING, 15);

        pnd_Rldt_Split__R_Field_3 = localVariables.newGroupInRecord("pnd_Rldt_Split__R_Field_3", "REDEFINE", pnd_Rldt_Split);
        pnd_Rldt_Split_Pnd_Rldt_Yyyymmdd = pnd_Rldt_Split__R_Field_3.newFieldInGroup("pnd_Rldt_Split_Pnd_Rldt_Yyyymmdd", "#RLDT-YYYYMMDD", FieldType.STRING, 
            8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        setTimeRecord = new DbsRecord();
        st = setTimeRecord.newFieldInRecord("ST", "ST", FieldType.NUMERIC, 7);
        registerRecord(setTimeRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Efm_Cabinet.reset();

        ldaCwftotal.initializeValues();

        localVariables.reset();
        pnd_Atsign.setInitialValue("@");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb4818() throws Exception
    {
        super("Cwfb4818");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("CWFB4818", onError);
        setupReports();
        //*  ----------- INITIALIZATION ---------------------
        //*  ----------- MAIN LOGIC -------------------------                                                                                                             //Natural: FORMAT PS = 56 LS = 131
        //*   TO ALLOW FOR EASY ESCAPE FROM PROGRAM.
        REP1:                                                                                                                                                             //Natural: REPEAT
        while (condition(whileTrue))
        {
            st = Global.getTIMN();                                                                                                                                        //Natural: SET TIME
                                                                                                                                                                          //Natural: PERFORM READ-CABINET-RECORDS
            sub_Read_Cabinet_Records();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM UPD-TOTAL-REC
            sub_Upd_Total_Rec();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-TOTALS
            sub_Write_Totals();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM ESCAPE-PROGRAM
            sub_Escape_Program();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ESCAPE-PROGRAM
            //*   (REP1.)
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-CABINET-RECORDS
        //* *************************************
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPD-TOTAL-REC
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TOTALS
        //* *******************************                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Escape_Program() throws Exception                                                                                                                    //Natural: ESCAPE-PROGRAM
    {
        if (BLNatReinput.isReinput()) return;

        //*  ================================
        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( REP1. )
        Global.setEscapeCode(EscapeType.Bottom, "REP1");
        if (true) return;
    }
    private void sub_Read_Cabinet_Records() throws Exception                                                                                                              //Natural: READ-CABINET-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        vw_cwf_Efm_Cabinet.startDatabaseRead                                                                                                                              //Natural: READ CWF-EFM-CABINET PHYSICAL SEQUENCE
        (
        "R1",
        new Oc[] { new Oc("ISN", "ASC") }
        );
        R1:
        while (condition(vw_cwf_Efm_Cabinet.readNextRow("R1")))
        {
            pnd_Input_Cntr.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #INPUT-CNTR
            if (condition(cwf_Efm_Cabinet_Srvce_Cncrn_Rqst_Log_Dte_Tme.equals(" ")))                                                                                      //Natural: IF CWF-EFM-CABINET.SRVCE-CNCRN-RQST-LOG-DTE-TME EQ ' '
            {
                pnd_Skip_Cntr.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #SKIP-CNTR
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Cabinet_Split.setValue(cwf_Efm_Cabinet_Cabinet_Id);                                                                                                       //Natural: MOVE CWF-EFM-CABINET.CABINET-ID TO #CABINET-SPLIT
            pnd_Isn.setValue(vw_cwf_Efm_Cabinet.getAstISN("R1"));                                                                                                         //Natural: ASSIGN #ISN := *ISN
            work_File_1.reset();                                                                                                                                          //Natural: RESET WORK-FILE-1
            work_File_1_A1.setValue(pnd_Atsign);                                                                                                                          //Natural: MOVE #ATSIGN TO A1 A2
            work_File_1_A2.setValue(pnd_Atsign);
            //*       MOVE #CABINET-PIN TO WORK-FILE-1.PIN-NBR
            work_File_1_Pin_Nbr.setValue(pnd_Cabinet_Split_Pnd_Cabinet_Pin_7);                                                                                            //Natural: MOVE #CABINET-PIN-7 TO WORK-FILE-1.PIN-NBR
            pnd_Time_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),cwf_Efm_Cabinet_Srvce_Cncrn_Rqst_Log_Dte_Tme);                                                //Natural: MOVE EDITED CWF-EFM-CABINET.SRVCE-CNCRN-RQST-LOG-DTE-TME TO #TIME-T ( EM = YYYYMMDDHHIISST )
            work_File_1_Sc_Rqst_Log_Dte_Tme_A.setValueEdited(pnd_Time_T,new ReportEditMask("LLL�DD�YYYY�HH':'II':'SS:0AP"));                                              //Natural: MOVE EDITED #TIME-T ( EM = LLL�DD�YYYY�HH':'II':'SS:T'00'AP ) TO WORK-FILE-1.SC-RQST-LOG-DTE-TME-A
            work_File_1_Sc_Rldt_Date_D.setValueEdited(pnd_Time_T,new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''"));                                                 //Natural: MOVE EDITED #TIME-T ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO WORK-FILE-1.SC-RLDT-DATE-D
            getWorkFiles().write(1, false, work_File_1);                                                                                                                  //Natural: WRITE WORK FILE 1 WORK-FILE-1
            pnd_Outpt_Cntr.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #OUTPT-CNTR
            //*  R1.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  READ-CABINET-RECORDS
    }
    private void sub_Upd_Total_Rec() throws Exception                                                                                                                     //Natural: UPD-TOTAL-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        total_Counters.reset();                                                                                                                                           //Natural: RESET TOTAL-COUNTERS
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 2 TOTAL
        while (condition(getWorkFiles().read(2, ldaCwftotal.getTotal())))
        {
            total_Counters.setValuesByName(ldaCwftotal.getTotal());                                                                                                       //Natural: MOVE BY NAME TOTAL TO TOTAL-COUNTERS
            ldaCwftotal.getTotal_Program_Name().setValue("CWFB4818");                                                                                                     //Natural: ASSIGN TOTAL.PROGRAM-NAME := 'CWFB4818'
            total_Counters_Efmcab_Add_Cntr.setValue(pnd_Outpt_Cntr);                                                                                                      //Natural: ASSIGN TOTAL-COUNTERS.EFMCAB-ADD-CNTR := #OUTPT-CNTR
            ldaCwftotal.getTotal().setValuesByName(total_Counters);                                                                                                       //Natural: MOVE BY NAME TOTAL-COUNTERS TO TOTAL
            ldaCwftotal.getTotal_T01().setValue(pnd_Atsign);                                                                                                              //Natural: MOVE #ATSIGN TO T01 T02 T03 T04 T05 T06 T07 T08 T09 T10 T11 T12 T13 T14 T15 T16 T17 T18 T19 T20 T21 T22 T23 T24 T25 T26 T27
            ldaCwftotal.getTotal_T02().setValue(pnd_Atsign);
            ldaCwftotal.getTotal_T03().setValue(pnd_Atsign);
            ldaCwftotal.getTotal_T04().setValue(pnd_Atsign);
            ldaCwftotal.getTotal_T05().setValue(pnd_Atsign);
            ldaCwftotal.getTotal_T06().setValue(pnd_Atsign);
            ldaCwftotal.getTotal_T07().setValue(pnd_Atsign);
            ldaCwftotal.getTotal_T08().setValue(pnd_Atsign);
            ldaCwftotal.getTotal_T09().setValue(pnd_Atsign);
            ldaCwftotal.getTotal_T10().setValue(pnd_Atsign);
            ldaCwftotal.getTotal_T11().setValue(pnd_Atsign);
            ldaCwftotal.getTotal_T12().setValue(pnd_Atsign);
            ldaCwftotal.getTotal_T13().setValue(pnd_Atsign);
            ldaCwftotal.getTotal_T14().setValue(pnd_Atsign);
            ldaCwftotal.getTotal_T15().setValue(pnd_Atsign);
            ldaCwftotal.getTotal_T16().setValue(pnd_Atsign);
            ldaCwftotal.getTotal_T17().setValue(pnd_Atsign);
            ldaCwftotal.getTotal_T18().setValue(pnd_Atsign);
            ldaCwftotal.getTotal_T19().setValue(pnd_Atsign);
            ldaCwftotal.getTotal_T20().setValue(pnd_Atsign);
            ldaCwftotal.getTotal_T21().setValue(pnd_Atsign);
            ldaCwftotal.getTotal_T22().setValue(pnd_Atsign);
            ldaCwftotal.getTotal_T23().setValue(pnd_Atsign);
            ldaCwftotal.getTotal_T24().setValue(pnd_Atsign);
            ldaCwftotal.getTotal_T25().setValue(pnd_Atsign);
            ldaCwftotal.getTotal_T26().setValue(pnd_Atsign);
            ldaCwftotal.getTotal_T27().setValue(pnd_Atsign);
            getWorkFiles().write(3, false, ldaCwftotal.getTotal());                                                                                                       //Natural: WRITE WORK FILE 3 TOTAL
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
    }
    private void sub_Write_Totals() throws Exception                                                                                                                      //Natural: WRITE-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        getReports().write(0, NEWLINE,"Program:",Global.getPROGRAM(),new TabSetting(45),"Run Date:",Global.getDATX(),NEWLINE,"*** EFM-Cabinet Extract Statistics for Corporate Reporting  ***",NEWLINE,"***************************************************************",NEWLINE,"Number of EFM-Cabinet  records has been Read    = ",pnd_Input_Cntr,NEWLINE,"Number of EFM-Cabinet  records has been Written = ",pnd_Outpt_Cntr,NEWLINE,"Number of EFM-Cabinet  records logicaly Skipped = ",pnd_Skip_Cntr,NEWLINE,"***************************************************",NEWLINE,"Elapsed Time to Process Records ","(HH:MM:SS.T) :",st,  //Natural: WRITE / 'Program:' *PROGRAM 45T 'Run Date:' *DATX / '*** EFM-Cabinet Extract Statistics for Corporate Reporting  ***' / '***************************************************************' / 'Number of EFM-Cabinet  records has been Read    = ' #INPUT-CNTR / 'Number of EFM-Cabinet  records has been Written = ' #OUTPT-CNTR / 'Number of EFM-Cabinet  records logicaly Skipped = ' #SKIP-CNTR / '***************************************************' / 'Elapsed Time to Process Records ' '(HH:MM:SS.T) :' *TIMD ( ST. ) ( EM = 99:99:99'.'9 )
            new ReportEditMask ("99:99:99'.'9"));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, NEWLINE,"EFM-PIN         ",cwf_Efm_Cabinet_Cabinet_Id,NEWLINE,"ISN Number      ",pnd_Isn,NEWLINE,"NATURAL ERROR",Global.getERROR_NR(),      //Natural: WRITE / 'EFM-PIN         ' CWF-EFM-CABINET.CABINET-ID / 'ISN Number      ' #ISN / 'NATURAL ERROR' *ERROR-NR 'IN' *PROGRAM '....LINE' *ERROR-LINE / ' '
            "IN",Global.getPROGRAM(),"....LINE",Global.getERROR_LINE(),NEWLINE," ");
        DbsUtil.terminate(10);  if (true) return;                                                                                                                         //Natural: TERMINATE 10
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=56 LS=131");
    }
}
