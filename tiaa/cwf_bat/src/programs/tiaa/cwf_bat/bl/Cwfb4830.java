/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:35:32 PM
**        * FROM NATURAL PROGRAM : Cwfb4830
************************************************************
**        * FILE NAME            : Cwfb4830.java
**        * CLASS NAME           : Cwfb4830
**        * INSTANCE NAME        : Cwfb4830
************************************************************
**********************************************************************
* PROGRAM  : CWFB4830
* SYSTEM   : CRPCWF
* TITLE    : CORPORATE REPORTING EXTRACT FROM MIT (PARTICIPANTS)
*          : SMART (SALES MANAGEMENT & ASSET RETENTION TRACKING)
* FUNCTION : THIS PROGRAM EXTRACT MIT RECORDS WITHIN "Window" PERIOD
*          : AND CREATE TWO FILES WHICH WILL CONTAIN WORK REQUESTS LEAD
*          : & CUSTOMER FILE FOR ORACLE REPORTING PURPOSES.
*
* MODIFICATIONS:
* NAME            DATE DESCRIPTION
* --------------  ---- -----------
* JAMIE HARGRAVE  9/01 MODIFY TO CORRECT CABINET RECORD PROBLEM AS PER
*                      HSIU-MEI LIN.
* GEORGE YEARWOOD 9/23 MODIFY TO CORRECT MINOR BUG
* JULIUS BREMER  4/15 NAD RETIREMENT - GET RID OF NAS LOOKUP = REDUNDANT
*  SEE JB1                   ALPHA STATE CODE DEFAULTS TO 'XX'
* JULIUS BREMER  8/17 REMOVE COR DDM
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
**********************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb4830 extends BLNatBase
{
    // Data Areas
    private PdaCwfa4815 pdaCwfa4815;
    private PdaNsta1245 pdaNsta1245;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private LdaCwfl4817 ldaCwfl4817;
    private LdaCwfl4818 ldaCwfl4818;
    private LdaCwfl4815 ldaCwfl4815;
    private LdaCwfl4832 ldaCwfl4832;
    private LdaCwfl4898 ldaCwfl4898;
    private LdaCwfl4833 ldaCwfl4833;
    private LdaCwfl4897 ldaCwfl4897;
    private LdaCwfl4824 ldaCwfl4824;
    private LdaCwfl4829 ldaCwfl4829;
    private LdaCwfl4830 ldaCwfl4830;
    private PdaCwfa1400 pdaCwfa1400;
    private PdaCwfa1401 pdaCwfa1401;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Work_File_1;
    private DbsField pnd_Work_File_1_Pnd_Rqst_Log_Dte_Tme;

    private DataAccessProgramView vw_cwf_Master_View_Select;
    private DbsField cwf_Master_View_Select_Rqst_Log_Dte_Tme;
    private DbsField cwf_Master_View_Select_Rqst_Id;

    private DbsGroup cwf_Master_View_Select__R_Field_1;
    private DbsField cwf_Master_View_Select_Rqst_Id_Wpid;

    private DataAccessProgramView vw_cwf_Sb_Ph;
    private DbsField cwf_Sb_Ph_Rcrd_Type;
    private DbsField cwf_Sb_Ph_Srvvr_Mit_Idntfr;
    private DbsField cwf_Sb_Ph_Dod_Not_Dte;

    private DbsGroup cwf_Sb_Ph__R_Field_2;
    private DbsField cwf_Sb_Ph_Dod_Not_Dte_A;
    private DbsField pnd_Owner_Prjct_Key;

    private DbsGroup pnd_Owner_Prjct_Key__R_Field_3;
    private DbsField pnd_Owner_Prjct_Key_Pnd_Owner_Unit;
    private DbsField pnd_Owner_Prjct_Key_Pnd_Prjct_Id;
    private DbsField pnd_Owner_Prjct_Key_Pnd_Actve_Ind;

    private DataAccessProgramView vw_cwf_Efm_Document;
    private DbsField cwf_Efm_Document_Cabinet_Id;

    private DbsGroup cwf_Efm_Document__R_Field_4;
    private DbsField cwf_Efm_Document_Pnd_P_Value;
    private DbsField cwf_Efm_Document_Pnd_Pin_Value;
    private DbsField cwf_Efm_Document_Tiaa_Rcvd_Dte;
    private DbsField cwf_Efm_Document_Case_Workprcss_Multi_Subrqst;

    private DbsGroup cwf_Efm_Document__R_Field_5;
    private DbsField cwf_Efm_Document_Case_Ind;
    private DbsField cwf_Efm_Document_Originating_Work_Prcss_Id;
    private DbsField cwf_Efm_Document_Multi_Rqst_Ind;
    private DbsField cwf_Efm_Document_Sub_Rqst_Ind;
    private DbsField cwf_Efm_Document_Entry_Dte_Tme;

    private DbsGroup cwf_Efm_Document_Document_Type;
    private DbsField cwf_Efm_Document_Document_Category;
    private DbsField cwf_Efm_Document_Document_Sub_Category;
    private DbsField cwf_Efm_Document_Document_Detail;
    private DbsField cwf_Efm_Document_Document_Direction_Ind;
    private DbsField pnd_Document_Key;

    private DbsGroup pnd_Document_Key__R_Field_6;
    private DbsField pnd_Document_Key_Pnd_Cabinet_Id;

    private DbsGroup pnd_Document_Key__R_Field_7;
    private DbsField pnd_Document_Key_Pnd_P_Value;
    private DbsField pnd_Document_Key_Pnd_Pin_Value;
    private DbsField pnd_Document_Key_Pnd_Tiaa_Rcvd_Dte;
    private DbsField pnd_Document_Key_Pnd_Case_Workprcss_Multi_Subrqst;

    private DbsGroup pnd_Document_Key__R_Field_8;
    private DbsField pnd_Document_Key_Pnd_Case_Ind;
    private DbsField pnd_Document_Key_Pnd_Originating_Work_Prcss_Id;
    private DbsField pnd_Document_Key_Pnd_Multi_Rqst_Ind;
    private DbsField pnd_Document_Key_Pnd_Sub_Rqst_Ind;
    private DbsField pnd_Document_Key_Pnd_Entry_Dte_Tme;
    private DbsField pnd_Doc_Type_Combo;

    private DbsGroup pnd_Doc_Type_Combo__R_Field_9;
    private DbsField pnd_Doc_Type_Combo_Pnd_Doc_Category;
    private DbsField pnd_Doc_Type_Combo_Pnd_Doc_Sub_Category;
    private DbsField pnd_Doc_Type_Combo_Pnd_Doc_Detail;
    private DbsField pnd_Table_Id_Entry_Cde;

    private DbsGroup pnd_Table_Id_Entry_Cde__R_Field_10;
    private DbsField pnd_Table_Id_Entry_Cde_Entry_Table_Id_Nbr;
    private DbsField pnd_Table_Id_Entry_Cde_Entry_Cde;
    private DbsField pnd_Tbl_Start_Dte_Tme_T;
    private DbsField pnd_Tbl_End_Dte_Tme_T;
    private DbsField pnd_Fix_Time;
    private DbsField pnd_Fix_Dte_Tme;

    private DbsGroup pnd_Fix_Dte_Tme__R_Field_11;
    private DbsField pnd_Fix_Dte_Tme_Pnd_Dte;

    private DbsGroup pnd_Fix_Dte_Tme__R_Field_12;
    private DbsField pnd_Fix_Dte_Tme_Pnd_Dte_Yyyy;
    private DbsField pnd_Fix_Dte_Tme_Pnd_Mm;
    private DbsField pnd_Fix_Dte_Tme_Pnd_Dd;
    private DbsField pnd_Fix_Dte_Tme_Pnd_Tme;
    private DbsField pnd_Fix_Dte_10;

    private DbsGroup pnd_Fix_Dte_10__R_Field_13;
    private DbsField pnd_Fix_Dte_10_Pnd_Fix_Dte_6;
    private DbsField pnd_Fix_Dte_10_Pnd_Fix_Dte_Yyyy;
    private DbsField pnd_Fix_Dte;
    private DbsField pnd_Effctve_Dte;
    private DbsField pnd_Trans_Dte;
    private DbsField pnd_Trnsctn_Dte;
    private DbsField pnd_Sec_Updte_Dte;
    private DbsField pnd_Date_Split;

    private DbsGroup pnd_Date_Split__R_Field_14;
    private DbsField pnd_Date_Split_Pnd_Date_Split_Dte;
    private DbsField pnd_Date_Split_Pnd_Date_Split_Tme;
    private DbsField pnd_Dob;
    private DbsField pnd_Dod_D;
    private DbsField pnd_Time;
    private DbsField pnd_Rqst_Log_Dte_Tme_Save;
    private DbsField pnd_Pin_Number_Save;
    private DbsField pnd_End_Dte_Tme;

    private DbsGroup pnd_End_Dte_Tme__R_Field_15;
    private DbsField pnd_End_Dte_Tme_Pnd_End_Yyyymmdd;
    private DbsField pnd_End_Dte_Tme_Pnd_End_Hhiisst;

    private DbsGroup pnd_End_Dte_Tme__R_Field_16;
    private DbsField pnd_End_Dte_Tme_Pnd_End_Dte_Tme_N;
    private DbsField pnd_Conversion_Due_Dte_Chg_Prty_Cde;

    private DbsGroup pnd_Conversion_Due_Dte_Chg_Prty_Cde__R_Field_17;
    private DbsField pnd_Conversion_Due_Dte_Chg_Prty_Cde_Pnd_Conversion_Fill_1;
    private DbsField pnd_Conversion_Due_Dte_Chg_Prty_Cde_Pnd_Recreate_Ind;
    private DbsField pnd_Conversion_Due_Dte_Chg_Prty_Cde_Pnd_Conversion_Fill_2;
    private DbsField pnd_Conversion_Due_Dte_Chg_Prty_Cde_Pnd_Prcssng_Type_Cde;
    private DbsField pnd_Start_Date_T;
    private DbsField pnd_End_Date_T;
    private DbsField pnd_Start_Date_D;
    private DbsField pnd_End_Date_D;
    private DbsField pnd_Error_Msg;
    private DbsField pnd_Pass_Tlc;
    private DbsField pnd_Pass_Dob;

    private DbsGroup pnd_Pass_Dob__R_Field_18;
    private DbsField pnd_Pass_Dob_Pnd_Pass_Dob_A;
    private DbsField pnd_Pass_Pin;
    private DbsField pnd_Pass_Name;
    private DbsField pnd_Pass_Ssn;
    private DbsField pnd_Pass_Found;
    private DbsField pnd_Found_One;
    private DbsField pnd_Pass_Unit;
    private DbsField pnd_Pass_Division;
    private DbsField pnd_Isn;
    private DbsField pnd_Atsign;
    private DbsField pnd_Base_Dte_Tme_D;
    private DbsField pnd_End_Dte_Tme_D;
    private DbsField pnd_Pin_Split;

    private DbsGroup pnd_Pin_Split__R_Field_19;
    private DbsField pnd_Pin_Split_Pnd_Pin_Split_1;
    private DbsField pnd_I;
    private DbsField pnd_Time_T;
    private DbsField pnd_Last_Chnge_Dte_Tme_T;
    private DbsField pnd_Number_W_Decimals;
    private DbsField pnd_Number_W_Decimals_13;
    private DbsField pnd_Pin_Base_Key;

    private DbsGroup pnd_Pin_Base_Key__R_Field_20;
    private DbsField pnd_Pin_Base_Key_Pin_Nbr;
    private DbsField pnd_Actv_Unque_Key;

    private DbsGroup pnd_Actv_Unque_Key__R_Field_21;
    private DbsField pnd_Actv_Unque_Key_Rqst_Log_Dte_Tme;
    private DbsField pnd_Actv_Unque_Key_Actve_Ind;

    private DbsGroup corr_Cntrct_Payee_Key;
    private DbsField corr_Cntrct_Payee_Key_Pnd_Corr_Cntrct_Payee_Key;

    private DbsGroup corr_Cntrct_Payee_Key__R_Field_22;
    private DbsField corr_Cntrct_Payee_Key_Correspondence_Addrss_Ind;
    private DbsField corr_Cntrct_Payee_Key_Cntrct_Nmbr;
    private DbsField corr_Cntrct_Payee_Key_Cntrct_Payee_Cde;
    private DbsField pnd_Cab_Key;

    private DbsGroup pnd_Cab_Key__R_Field_23;
    private DbsField pnd_Cab_Key_Pnd_Cabinet_Type;
    private DbsField pnd_Cab_Key_Pnd_Pin_Nbr;
    private DbsField pnd_Tbl_Key;

    private DbsGroup pnd_Tbl_Key__R_Field_24;
    private DbsField pnd_Tbl_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Key_Pnd_Tbl_Key_Field;
    private DbsField pnd_Tbl_Key_Pnd_Tbl_Actve_Ind;
    private DbsField pnd_Geo_Grp;

    private DbsGroup pnd_Geo_Grp__R_Field_25;
    private DbsField pnd_Geo_Grp_Pnd_State;
    private DbsField pnd_Geo_Grp_Pnd_Geo_Code;
    private DbsField pnd_Ph_Key;

    private DbsGroup pnd_Ph_Key__R_Field_26;
    private DbsField pnd_Ph_Key_Rcrd_Type;
    private DbsField pnd_Ph_Key_Srvvr_Mit_Idntfr;
    private DbsField pnd_Mit_Read_Cntr;
    private DbsField pnd_Work_Read_Cntr;
    private DbsField pnd_Wr_Lead_Cntr;
    private DbsField pnd_Cust_Ad_Cntr;
    private DbsField pnd_Cust_Up_Cntr;
    private DbsField pnd_Customer_Cntr;
    private DbsField pnd_Tlc_Cntr;
    private DbsField pnd_No_Base_Rec;
    private DbsField pnd_Vac_Address;
    private DbsField pnd_No_Base_Rec_V;
    private DbsField pnd_No_State_In_Table;
    private DbsField pnd_Pass_Nfnd_Cntr;
    private DbsField pnd_Add;
    private DbsField pnd_Update;
    private DbsField pnd_Act_Strt_Time_T;
    private DbsField pnd_Foreign_Address;
    private DbsField pnd_Activity_Type;
    private DbsField pnd_Pin_Nbr_Save;

    private DbsGroup pnd_Pin_Nbr_Save__R_Field_27;
    private DbsField pnd_Pin_Nbr_Save_Pnd_Pin_Nbr_Save_N;

    private DbsGroup pnd_Frgn_Geo_Pass;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_1;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_2;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_3;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_4;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_5;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_6;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_Return_Cde;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_Mail_Cde;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_Geo_Cde;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_St_Cde1;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_St_Cde2;

    private DbsRecord setTimeRecord;
    private DbsField st;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCwfa4815 = new PdaCwfa4815(localVariables);
        pdaNsta1245 = new PdaNsta1245(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        ldaCwfl4817 = new LdaCwfl4817();
        registerRecord(ldaCwfl4817);
        registerRecord(ldaCwfl4817.getVw_cwf_Master());
        ldaCwfl4818 = new LdaCwfl4818();
        registerRecord(ldaCwfl4818);
        registerRecord(ldaCwfl4818.getVw_cwf_Route_Support_Tbl());
        ldaCwfl4815 = new LdaCwfl4815();
        registerRecord(ldaCwfl4815);
        registerRecord(ldaCwfl4815.getVw_cwf_Support_Tbl());
        ldaCwfl4832 = new LdaCwfl4832();
        registerRecord(ldaCwfl4832);
        ldaCwfl4898 = new LdaCwfl4898();
        registerRecord(ldaCwfl4898);
        ldaCwfl4833 = new LdaCwfl4833();
        registerRecord(ldaCwfl4833);
        ldaCwfl4897 = new LdaCwfl4897();
        registerRecord(ldaCwfl4897);
        ldaCwfl4824 = new LdaCwfl4824();
        registerRecord(ldaCwfl4824);
        ldaCwfl4829 = new LdaCwfl4829();
        registerRecord(ldaCwfl4829);
        registerRecord(ldaCwfl4829.getVw_cwf_Bsnss_Data_File());
        ldaCwfl4830 = new LdaCwfl4830();
        registerRecord(ldaCwfl4830);
        pdaCwfa1400 = new PdaCwfa1400(localVariables);
        pdaCwfa1401 = new PdaCwfa1401(localVariables);

        // Local Variables

        pnd_Work_File_1 = localVariables.newGroupInRecord("pnd_Work_File_1", "#WORK-FILE-1");
        pnd_Work_File_1_Pnd_Rqst_Log_Dte_Tme = pnd_Work_File_1.newFieldInGroup("pnd_Work_File_1_Pnd_Rqst_Log_Dte_Tme", "#RQST-LOG-DTE-TME", FieldType.STRING, 
            15);

        vw_cwf_Master_View_Select = new DataAccessProgramView(new NameInfo("vw_cwf_Master_View_Select", "CWF-MASTER-VIEW-SELECT"), "CWF_MASTER_INDEX_VIEW", 
            "CWF_MASTER_INDEX");
        cwf_Master_View_Select_Rqst_Log_Dte_Tme = vw_cwf_Master_View_Select.getRecord().newFieldInGroup("cwf_Master_View_Select_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Master_View_Select_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        cwf_Master_View_Select_Rqst_Id = vw_cwf_Master_View_Select.getRecord().newFieldInGroup("cwf_Master_View_Select_Rqst_Id", "RQST-ID", FieldType.STRING, 
            28, RepeatingFieldStrategy.None, "RQST_ID");
        cwf_Master_View_Select_Rqst_Id.setDdmHeader("REQUEST ID");

        cwf_Master_View_Select__R_Field_1 = vw_cwf_Master_View_Select.getRecord().newGroupInGroup("cwf_Master_View_Select__R_Field_1", "REDEFINE", cwf_Master_View_Select_Rqst_Id);
        cwf_Master_View_Select_Rqst_Id_Wpid = cwf_Master_View_Select__R_Field_1.newFieldInGroup("cwf_Master_View_Select_Rqst_Id_Wpid", "RQST-ID-WPID", 
            FieldType.STRING, 6);
        registerRecord(vw_cwf_Master_View_Select);

        vw_cwf_Sb_Ph = new DataAccessProgramView(new NameInfo("vw_cwf_Sb_Ph", "CWF-SB-PH"), "CWF_SB_PH", "CWF_SB_BENE");
        cwf_Sb_Ph_Rcrd_Type = vw_cwf_Sb_Ph.getRecord().newFieldInGroup("cwf_Sb_Ph_Rcrd_Type", "RCRD-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RCRD_TYPE");
        cwf_Sb_Ph_Rcrd_Type.setDdmHeader("RECORD/TYPE");
        cwf_Sb_Ph_Srvvr_Mit_Idntfr = vw_cwf_Sb_Ph.getRecord().newFieldInGroup("cwf_Sb_Ph_Srvvr_Mit_Idntfr", "SRVVR-MIT-IDNTFR", FieldType.STRING, 14, 
            RepeatingFieldStrategy.None, "SRVVR_MIT_IDNTFR");
        cwf_Sb_Ph_Srvvr_Mit_Idntfr.setDdmHeader("SURVIVOR MIT/IDENTIFIER");
        cwf_Sb_Ph_Dod_Not_Dte = vw_cwf_Sb_Ph.getRecord().newFieldInGroup("cwf_Sb_Ph_Dod_Not_Dte", "DOD-NOT-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "DOD_NOT_DTE");
        cwf_Sb_Ph_Dod_Not_Dte.setDdmHeader("DOD NOTIF/DATE");

        cwf_Sb_Ph__R_Field_2 = vw_cwf_Sb_Ph.getRecord().newGroupInGroup("cwf_Sb_Ph__R_Field_2", "REDEFINE", cwf_Sb_Ph_Dod_Not_Dte);
        cwf_Sb_Ph_Dod_Not_Dte_A = cwf_Sb_Ph__R_Field_2.newFieldInGroup("cwf_Sb_Ph_Dod_Not_Dte_A", "DOD-NOT-DTE-A", FieldType.STRING, 8);
        registerRecord(vw_cwf_Sb_Ph);

        pnd_Owner_Prjct_Key = localVariables.newFieldInRecord("pnd_Owner_Prjct_Key", "#OWNER-PRJCT-KEY", FieldType.STRING, 17);

        pnd_Owner_Prjct_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Owner_Prjct_Key__R_Field_3", "REDEFINE", pnd_Owner_Prjct_Key);
        pnd_Owner_Prjct_Key_Pnd_Owner_Unit = pnd_Owner_Prjct_Key__R_Field_3.newFieldInGroup("pnd_Owner_Prjct_Key_Pnd_Owner_Unit", "#OWNER-UNIT", FieldType.STRING, 
            8);
        pnd_Owner_Prjct_Key_Pnd_Prjct_Id = pnd_Owner_Prjct_Key__R_Field_3.newFieldInGroup("pnd_Owner_Prjct_Key_Pnd_Prjct_Id", "#PRJCT-ID", FieldType.STRING, 
            8);
        pnd_Owner_Prjct_Key_Pnd_Actve_Ind = pnd_Owner_Prjct_Key__R_Field_3.newFieldInGroup("pnd_Owner_Prjct_Key_Pnd_Actve_Ind", "#ACTVE-IND", FieldType.STRING, 
            1);

        vw_cwf_Efm_Document = new DataAccessProgramView(new NameInfo("vw_cwf_Efm_Document", "CWF-EFM-DOCUMENT"), "CWF_EFM_DOCUMENT", "CWF_EFM_DOCUMENT");
        cwf_Efm_Document_Cabinet_Id = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Cabinet_Id", "CABINET-ID", FieldType.STRING, 14, 
            RepeatingFieldStrategy.None, "CABINET_ID");
        cwf_Efm_Document_Cabinet_Id.setDdmHeader("CABINET/ID");

        cwf_Efm_Document__R_Field_4 = vw_cwf_Efm_Document.getRecord().newGroupInGroup("cwf_Efm_Document__R_Field_4", "REDEFINE", cwf_Efm_Document_Cabinet_Id);
        cwf_Efm_Document_Pnd_P_Value = cwf_Efm_Document__R_Field_4.newFieldInGroup("cwf_Efm_Document_Pnd_P_Value", "#P-VALUE", FieldType.STRING, 1);
        cwf_Efm_Document_Pnd_Pin_Value = cwf_Efm_Document__R_Field_4.newFieldInGroup("cwf_Efm_Document_Pnd_Pin_Value", "#PIN-VALUE", FieldType.STRING, 
            12);
        cwf_Efm_Document_Tiaa_Rcvd_Dte = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TIAA_RCVD_DTE");
        cwf_Efm_Document_Tiaa_Rcvd_Dte.setDdmHeader("TIAA RECVD/DATE");
        cwf_Efm_Document_Case_Workprcss_Multi_Subrqst = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Case_Workprcss_Multi_Subrqst", 
            "CASE-WORKPRCSS-MULTI-SUBRQST", FieldType.STRING, 9, RepeatingFieldStrategy.None, "CASE_WORKPRCSS_MULTI_SUBRQST");
        cwf_Efm_Document_Case_Workprcss_Multi_Subrqst.setDdmHeader("CASE/WPID/MULTI/SUB-REQ");

        cwf_Efm_Document__R_Field_5 = vw_cwf_Efm_Document.getRecord().newGroupInGroup("cwf_Efm_Document__R_Field_5", "REDEFINE", cwf_Efm_Document_Case_Workprcss_Multi_Subrqst);
        cwf_Efm_Document_Case_Ind = cwf_Efm_Document__R_Field_5.newFieldInGroup("cwf_Efm_Document_Case_Ind", "CASE-IND", FieldType.STRING, 1);
        cwf_Efm_Document_Originating_Work_Prcss_Id = cwf_Efm_Document__R_Field_5.newFieldInGroup("cwf_Efm_Document_Originating_Work_Prcss_Id", "ORIGINATING-WORK-PRCSS-ID", 
            FieldType.STRING, 6);
        cwf_Efm_Document_Multi_Rqst_Ind = cwf_Efm_Document__R_Field_5.newFieldInGroup("cwf_Efm_Document_Multi_Rqst_Ind", "MULTI-RQST-IND", FieldType.STRING, 
            1);
        cwf_Efm_Document_Sub_Rqst_Ind = cwf_Efm_Document__R_Field_5.newFieldInGroup("cwf_Efm_Document_Sub_Rqst_Ind", "SUB-RQST-IND", FieldType.STRING, 
            1);
        cwf_Efm_Document_Entry_Dte_Tme = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Entry_Dte_Tme", "ENTRY-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ENTRY_DTE_TME");
        cwf_Efm_Document_Entry_Dte_Tme.setDdmHeader("ENTRY/DATE AND TIME");

        cwf_Efm_Document_Document_Type = vw_cwf_Efm_Document.getRecord().newGroupInGroup("CWF_EFM_DOCUMENT_DOCUMENT_TYPE", "DOCUMENT-TYPE");
        cwf_Efm_Document_Document_Type.setDdmHeader("DOCUMENT/TYPE");
        cwf_Efm_Document_Document_Category = cwf_Efm_Document_Document_Type.newFieldInGroup("cwf_Efm_Document_Document_Category", "DOCUMENT-CATEGORY", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "DOCUMENT_CATEGORY");
        cwf_Efm_Document_Document_Category.setDdmHeader("DOC/CAT");
        cwf_Efm_Document_Document_Sub_Category = cwf_Efm_Document_Document_Type.newFieldInGroup("cwf_Efm_Document_Document_Sub_Category", "DOCUMENT-SUB-CATEGORY", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "DOCUMENT_SUB_CATEGORY");
        cwf_Efm_Document_Document_Sub_Category.setDdmHeader("DOC SUB-/CATEGORY");
        cwf_Efm_Document_Document_Detail = cwf_Efm_Document_Document_Type.newFieldInGroup("cwf_Efm_Document_Document_Detail", "DOCUMENT-DETAIL", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "DOCUMENT_DETAIL");
        cwf_Efm_Document_Document_Detail.setDdmHeader("DOCUMENT/DETAIL");
        cwf_Efm_Document_Document_Direction_Ind = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Document_Direction_Ind", "DOCUMENT-DIRECTION-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "DOCUMENT_DIRECTION_IND");
        cwf_Efm_Document_Document_Direction_Ind.setDdmHeader("DOC/DIR");
        registerRecord(vw_cwf_Efm_Document);

        pnd_Document_Key = localVariables.newFieldInRecord("pnd_Document_Key", "#DOCUMENT-KEY", FieldType.STRING, 34);

        pnd_Document_Key__R_Field_6 = localVariables.newGroupInRecord("pnd_Document_Key__R_Field_6", "REDEFINE", pnd_Document_Key);
        pnd_Document_Key_Pnd_Cabinet_Id = pnd_Document_Key__R_Field_6.newFieldInGroup("pnd_Document_Key_Pnd_Cabinet_Id", "#CABINET-ID", FieldType.STRING, 
            14);

        pnd_Document_Key__R_Field_7 = pnd_Document_Key__R_Field_6.newGroupInGroup("pnd_Document_Key__R_Field_7", "REDEFINE", pnd_Document_Key_Pnd_Cabinet_Id);
        pnd_Document_Key_Pnd_P_Value = pnd_Document_Key__R_Field_7.newFieldInGroup("pnd_Document_Key_Pnd_P_Value", "#P-VALUE", FieldType.STRING, 1);
        pnd_Document_Key_Pnd_Pin_Value = pnd_Document_Key__R_Field_7.newFieldInGroup("pnd_Document_Key_Pnd_Pin_Value", "#PIN-VALUE", FieldType.STRING, 
            12);
        pnd_Document_Key_Pnd_Tiaa_Rcvd_Dte = pnd_Document_Key__R_Field_6.newFieldInGroup("pnd_Document_Key_Pnd_Tiaa_Rcvd_Dte", "#TIAA-RCVD-DTE", FieldType.DATE);
        pnd_Document_Key_Pnd_Case_Workprcss_Multi_Subrqst = pnd_Document_Key__R_Field_6.newFieldInGroup("pnd_Document_Key_Pnd_Case_Workprcss_Multi_Subrqst", 
            "#CASE-WORKPRCSS-MULTI-SUBRQST", FieldType.STRING, 9);

        pnd_Document_Key__R_Field_8 = pnd_Document_Key__R_Field_6.newGroupInGroup("pnd_Document_Key__R_Field_8", "REDEFINE", pnd_Document_Key_Pnd_Case_Workprcss_Multi_Subrqst);
        pnd_Document_Key_Pnd_Case_Ind = pnd_Document_Key__R_Field_8.newFieldInGroup("pnd_Document_Key_Pnd_Case_Ind", "#CASE-IND", FieldType.STRING, 1);
        pnd_Document_Key_Pnd_Originating_Work_Prcss_Id = pnd_Document_Key__R_Field_8.newFieldInGroup("pnd_Document_Key_Pnd_Originating_Work_Prcss_Id", 
            "#ORIGINATING-WORK-PRCSS-ID", FieldType.STRING, 6);
        pnd_Document_Key_Pnd_Multi_Rqst_Ind = pnd_Document_Key__R_Field_8.newFieldInGroup("pnd_Document_Key_Pnd_Multi_Rqst_Ind", "#MULTI-RQST-IND", FieldType.STRING, 
            1);
        pnd_Document_Key_Pnd_Sub_Rqst_Ind = pnd_Document_Key__R_Field_8.newFieldInGroup("pnd_Document_Key_Pnd_Sub_Rqst_Ind", "#SUB-RQST-IND", FieldType.STRING, 
            1);
        pnd_Document_Key_Pnd_Entry_Dte_Tme = pnd_Document_Key__R_Field_6.newFieldInGroup("pnd_Document_Key_Pnd_Entry_Dte_Tme", "#ENTRY-DTE-TME", FieldType.TIME);
        pnd_Doc_Type_Combo = localVariables.newFieldInRecord("pnd_Doc_Type_Combo", "#DOC-TYPE-COMBO", FieldType.STRING, 8);

        pnd_Doc_Type_Combo__R_Field_9 = localVariables.newGroupInRecord("pnd_Doc_Type_Combo__R_Field_9", "REDEFINE", pnd_Doc_Type_Combo);
        pnd_Doc_Type_Combo_Pnd_Doc_Category = pnd_Doc_Type_Combo__R_Field_9.newFieldInGroup("pnd_Doc_Type_Combo_Pnd_Doc_Category", "#DOC-CATEGORY", FieldType.STRING, 
            1);
        pnd_Doc_Type_Combo_Pnd_Doc_Sub_Category = pnd_Doc_Type_Combo__R_Field_9.newFieldInGroup("pnd_Doc_Type_Combo_Pnd_Doc_Sub_Category", "#DOC-SUB-CATEGORY", 
            FieldType.STRING, 3);
        pnd_Doc_Type_Combo_Pnd_Doc_Detail = pnd_Doc_Type_Combo__R_Field_9.newFieldInGroup("pnd_Doc_Type_Combo_Pnd_Doc_Detail", "#DOC-DETAIL", FieldType.STRING, 
            4);
        pnd_Table_Id_Entry_Cde = localVariables.newFieldInRecord("pnd_Table_Id_Entry_Cde", "#TABLE-ID-ENTRY-CDE", FieldType.STRING, 26);

        pnd_Table_Id_Entry_Cde__R_Field_10 = localVariables.newGroupInRecord("pnd_Table_Id_Entry_Cde__R_Field_10", "REDEFINE", pnd_Table_Id_Entry_Cde);
        pnd_Table_Id_Entry_Cde_Entry_Table_Id_Nbr = pnd_Table_Id_Entry_Cde__R_Field_10.newFieldInGroup("pnd_Table_Id_Entry_Cde_Entry_Table_Id_Nbr", "ENTRY-TABLE-ID-NBR", 
            FieldType.STRING, 6);
        pnd_Table_Id_Entry_Cde_Entry_Cde = pnd_Table_Id_Entry_Cde__R_Field_10.newFieldInGroup("pnd_Table_Id_Entry_Cde_Entry_Cde", "ENTRY-CDE", FieldType.STRING, 
            20);
        pnd_Tbl_Start_Dte_Tme_T = localVariables.newFieldInRecord("pnd_Tbl_Start_Dte_Tme_T", "#TBL-START-DTE-TME-T", FieldType.TIME);
        pnd_Tbl_End_Dte_Tme_T = localVariables.newFieldInRecord("pnd_Tbl_End_Dte_Tme_T", "#TBL-END-DTE-TME-T", FieldType.TIME);
        pnd_Fix_Time = localVariables.newFieldInRecord("pnd_Fix_Time", "#FIX-TIME", FieldType.STRING, 15);
        pnd_Fix_Dte_Tme = localVariables.newFieldInRecord("pnd_Fix_Dte_Tme", "#FIX-DTE-TME", FieldType.STRING, 15);

        pnd_Fix_Dte_Tme__R_Field_11 = localVariables.newGroupInRecord("pnd_Fix_Dte_Tme__R_Field_11", "REDEFINE", pnd_Fix_Dte_Tme);
        pnd_Fix_Dte_Tme_Pnd_Dte = pnd_Fix_Dte_Tme__R_Field_11.newFieldInGroup("pnd_Fix_Dte_Tme_Pnd_Dte", "#DTE", FieldType.STRING, 8);

        pnd_Fix_Dte_Tme__R_Field_12 = pnd_Fix_Dte_Tme__R_Field_11.newGroupInGroup("pnd_Fix_Dte_Tme__R_Field_12", "REDEFINE", pnd_Fix_Dte_Tme_Pnd_Dte);
        pnd_Fix_Dte_Tme_Pnd_Dte_Yyyy = pnd_Fix_Dte_Tme__R_Field_12.newFieldInGroup("pnd_Fix_Dte_Tme_Pnd_Dte_Yyyy", "#DTE-YYYY", FieldType.NUMERIC, 4);
        pnd_Fix_Dte_Tme_Pnd_Mm = pnd_Fix_Dte_Tme__R_Field_12.newFieldInGroup("pnd_Fix_Dte_Tme_Pnd_Mm", "#MM", FieldType.NUMERIC, 2);
        pnd_Fix_Dte_Tme_Pnd_Dd = pnd_Fix_Dte_Tme__R_Field_12.newFieldInGroup("pnd_Fix_Dte_Tme_Pnd_Dd", "#DD", FieldType.NUMERIC, 2);
        pnd_Fix_Dte_Tme_Pnd_Tme = pnd_Fix_Dte_Tme__R_Field_11.newFieldInGroup("pnd_Fix_Dte_Tme_Pnd_Tme", "#TME", FieldType.STRING, 7);
        pnd_Fix_Dte_10 = localVariables.newFieldInRecord("pnd_Fix_Dte_10", "#FIX-DTE-10", FieldType.STRING, 10);

        pnd_Fix_Dte_10__R_Field_13 = localVariables.newGroupInRecord("pnd_Fix_Dte_10__R_Field_13", "REDEFINE", pnd_Fix_Dte_10);
        pnd_Fix_Dte_10_Pnd_Fix_Dte_6 = pnd_Fix_Dte_10__R_Field_13.newFieldInGroup("pnd_Fix_Dte_10_Pnd_Fix_Dte_6", "#FIX-DTE-6", FieldType.STRING, 6);
        pnd_Fix_Dte_10_Pnd_Fix_Dte_Yyyy = pnd_Fix_Dte_10__R_Field_13.newFieldInGroup("pnd_Fix_Dte_10_Pnd_Fix_Dte_Yyyy", "#FIX-DTE-YYYY", FieldType.NUMERIC, 
            4);
        pnd_Fix_Dte = localVariables.newFieldInRecord("pnd_Fix_Dte", "#FIX-DTE", FieldType.STRING, 8);
        pnd_Effctve_Dte = localVariables.newFieldInRecord("pnd_Effctve_Dte", "#EFFCTVE-DTE", FieldType.STRING, 8);
        pnd_Trans_Dte = localVariables.newFieldInRecord("pnd_Trans_Dte", "#TRANS-DTE", FieldType.STRING, 8);
        pnd_Trnsctn_Dte = localVariables.newFieldInRecord("pnd_Trnsctn_Dte", "#TRNSCTN-DTE", FieldType.STRING, 8);
        pnd_Sec_Updte_Dte = localVariables.newFieldInRecord("pnd_Sec_Updte_Dte", "#SEC-UPDTE-DTE", FieldType.STRING, 8);
        pnd_Date_Split = localVariables.newFieldInRecord("pnd_Date_Split", "#DATE-SPLIT", FieldType.STRING, 15);

        pnd_Date_Split__R_Field_14 = localVariables.newGroupInRecord("pnd_Date_Split__R_Field_14", "REDEFINE", pnd_Date_Split);
        pnd_Date_Split_Pnd_Date_Split_Dte = pnd_Date_Split__R_Field_14.newFieldInGroup("pnd_Date_Split_Pnd_Date_Split_Dte", "#DATE-SPLIT-DTE", FieldType.STRING, 
            8);
        pnd_Date_Split_Pnd_Date_Split_Tme = pnd_Date_Split__R_Field_14.newFieldInGroup("pnd_Date_Split_Pnd_Date_Split_Tme", "#DATE-SPLIT-TME", FieldType.STRING, 
            7);
        pnd_Dob = localVariables.newFieldInRecord("pnd_Dob", "#DOB", FieldType.DATE);
        pnd_Dod_D = localVariables.newFieldInRecord("pnd_Dod_D", "#DOD-D", FieldType.DATE);
        pnd_Time = localVariables.newFieldInRecord("pnd_Time", "#TIME", FieldType.TIME);
        pnd_Rqst_Log_Dte_Tme_Save = localVariables.newFieldInRecord("pnd_Rqst_Log_Dte_Tme_Save", "#RQST-LOG-DTE-TME-SAVE", FieldType.STRING, 15);
        pnd_Pin_Number_Save = localVariables.newFieldInRecord("pnd_Pin_Number_Save", "#PIN-NUMBER-SAVE", FieldType.NUMERIC, 7);
        pnd_End_Dte_Tme = localVariables.newFieldInRecord("pnd_End_Dte_Tme", "#END-DTE-TME", FieldType.STRING, 15);

        pnd_End_Dte_Tme__R_Field_15 = localVariables.newGroupInRecord("pnd_End_Dte_Tme__R_Field_15", "REDEFINE", pnd_End_Dte_Tme);
        pnd_End_Dte_Tme_Pnd_End_Yyyymmdd = pnd_End_Dte_Tme__R_Field_15.newFieldInGroup("pnd_End_Dte_Tme_Pnd_End_Yyyymmdd", "#END-YYYYMMDD", FieldType.STRING, 
            8);
        pnd_End_Dte_Tme_Pnd_End_Hhiisst = pnd_End_Dte_Tme__R_Field_15.newFieldInGroup("pnd_End_Dte_Tme_Pnd_End_Hhiisst", "#END-HHIISST", FieldType.STRING, 
            7);

        pnd_End_Dte_Tme__R_Field_16 = localVariables.newGroupInRecord("pnd_End_Dte_Tme__R_Field_16", "REDEFINE", pnd_End_Dte_Tme);
        pnd_End_Dte_Tme_Pnd_End_Dte_Tme_N = pnd_End_Dte_Tme__R_Field_16.newFieldInGroup("pnd_End_Dte_Tme_Pnd_End_Dte_Tme_N", "#END-DTE-TME-N", FieldType.NUMERIC, 
            15);
        pnd_Conversion_Due_Dte_Chg_Prty_Cde = localVariables.newFieldInRecord("pnd_Conversion_Due_Dte_Chg_Prty_Cde", "#CONVERSION-DUE-DTE-CHG-PRTY-CDE", 
            FieldType.STRING, 16);

        pnd_Conversion_Due_Dte_Chg_Prty_Cde__R_Field_17 = localVariables.newGroupInRecord("pnd_Conversion_Due_Dte_Chg_Prty_Cde__R_Field_17", "REDEFINE", 
            pnd_Conversion_Due_Dte_Chg_Prty_Cde);
        pnd_Conversion_Due_Dte_Chg_Prty_Cde_Pnd_Conversion_Fill_1 = pnd_Conversion_Due_Dte_Chg_Prty_Cde__R_Field_17.newFieldInGroup("pnd_Conversion_Due_Dte_Chg_Prty_Cde_Pnd_Conversion_Fill_1", 
            "#CONVERSION-FILL-1", FieldType.STRING, 11);
        pnd_Conversion_Due_Dte_Chg_Prty_Cde_Pnd_Recreate_Ind = pnd_Conversion_Due_Dte_Chg_Prty_Cde__R_Field_17.newFieldInGroup("pnd_Conversion_Due_Dte_Chg_Prty_Cde_Pnd_Recreate_Ind", 
            "#RECREATE-IND", FieldType.STRING, 1);
        pnd_Conversion_Due_Dte_Chg_Prty_Cde_Pnd_Conversion_Fill_2 = pnd_Conversion_Due_Dte_Chg_Prty_Cde__R_Field_17.newFieldInGroup("pnd_Conversion_Due_Dte_Chg_Prty_Cde_Pnd_Conversion_Fill_2", 
            "#CONVERSION-FILL-2", FieldType.STRING, 3);
        pnd_Conversion_Due_Dte_Chg_Prty_Cde_Pnd_Prcssng_Type_Cde = pnd_Conversion_Due_Dte_Chg_Prty_Cde__R_Field_17.newFieldInGroup("pnd_Conversion_Due_Dte_Chg_Prty_Cde_Pnd_Prcssng_Type_Cde", 
            "#PRCSSNG-TYPE-CDE", FieldType.STRING, 1);
        pnd_Start_Date_T = localVariables.newFieldInRecord("pnd_Start_Date_T", "#START-DATE-T", FieldType.TIME);
        pnd_End_Date_T = localVariables.newFieldInRecord("pnd_End_Date_T", "#END-DATE-T", FieldType.TIME);
        pnd_Start_Date_D = localVariables.newFieldInRecord("pnd_Start_Date_D", "#START-DATE-D", FieldType.DATE);
        pnd_End_Date_D = localVariables.newFieldInRecord("pnd_End_Date_D", "#END-DATE-D", FieldType.DATE);
        pnd_Error_Msg = localVariables.newFieldInRecord("pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 132);
        pnd_Pass_Tlc = localVariables.newFieldInRecord("pnd_Pass_Tlc", "#PASS-TLC", FieldType.STRING, 7);
        pnd_Pass_Dob = localVariables.newFieldInRecord("pnd_Pass_Dob", "#PASS-DOB", FieldType.NUMERIC, 8);

        pnd_Pass_Dob__R_Field_18 = localVariables.newGroupInRecord("pnd_Pass_Dob__R_Field_18", "REDEFINE", pnd_Pass_Dob);
        pnd_Pass_Dob_Pnd_Pass_Dob_A = pnd_Pass_Dob__R_Field_18.newFieldInGroup("pnd_Pass_Dob_Pnd_Pass_Dob_A", "#PASS-DOB-A", FieldType.STRING, 8);
        pnd_Pass_Pin = localVariables.newFieldInRecord("pnd_Pass_Pin", "#PASS-PIN", FieldType.NUMERIC, 12);
        pnd_Pass_Name = localVariables.newFieldInRecord("pnd_Pass_Name", "#PASS-NAME", FieldType.STRING, 40);
        pnd_Pass_Ssn = localVariables.newFieldInRecord("pnd_Pass_Ssn", "#PASS-SSN", FieldType.NUMERIC, 9);
        pnd_Pass_Found = localVariables.newFieldInRecord("pnd_Pass_Found", "#PASS-FOUND", FieldType.BOOLEAN, 1);
        pnd_Found_One = localVariables.newFieldInRecord("pnd_Found_One", "#FOUND-ONE", FieldType.BOOLEAN, 1);
        pnd_Pass_Unit = localVariables.newFieldInRecord("pnd_Pass_Unit", "#PASS-UNIT", FieldType.STRING, 8);
        pnd_Pass_Division = localVariables.newFieldInRecord("pnd_Pass_Division", "#PASS-DIVISION", FieldType.STRING, 6);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Atsign = localVariables.newFieldInRecord("pnd_Atsign", "#ATSIGN", FieldType.STRING, 1);
        pnd_Base_Dte_Tme_D = localVariables.newFieldInRecord("pnd_Base_Dte_Tme_D", "#BASE-DTE-TME-D", FieldType.DATE);
        pnd_End_Dte_Tme_D = localVariables.newFieldInRecord("pnd_End_Dte_Tme_D", "#END-DTE-TME-D", FieldType.DATE);
        pnd_Pin_Split = localVariables.newFieldInRecord("pnd_Pin_Split", "#PIN-SPLIT", FieldType.STRING, 12);

        pnd_Pin_Split__R_Field_19 = localVariables.newGroupInRecord("pnd_Pin_Split__R_Field_19", "REDEFINE", pnd_Pin_Split);
        pnd_Pin_Split_Pnd_Pin_Split_1 = pnd_Pin_Split__R_Field_19.newFieldInGroup("pnd_Pin_Split_Pnd_Pin_Split_1", "#PIN-SPLIT-1", FieldType.STRING, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_Time_T = localVariables.newFieldInRecord("pnd_Time_T", "#TIME-T", FieldType.TIME);
        pnd_Last_Chnge_Dte_Tme_T = localVariables.newFieldInRecord("pnd_Last_Chnge_Dte_Tme_T", "#LAST-CHNGE-DTE-TME-T", FieldType.TIME);
        pnd_Number_W_Decimals = localVariables.newFieldInRecord("pnd_Number_W_Decimals", "#NUMBER-W-DECIMALS", FieldType.NUMERIC, 9, 2);
        pnd_Number_W_Decimals_13 = localVariables.newFieldInRecord("pnd_Number_W_Decimals_13", "#NUMBER-W-DECIMALS-13", FieldType.NUMERIC, 13, 2);
        pnd_Pin_Base_Key = localVariables.newFieldInRecord("pnd_Pin_Base_Key", "#PIN-BASE-KEY", FieldType.STRING, 13);

        pnd_Pin_Base_Key__R_Field_20 = localVariables.newGroupInRecord("pnd_Pin_Base_Key__R_Field_20", "REDEFINE", pnd_Pin_Base_Key);
        pnd_Pin_Base_Key_Pin_Nbr = pnd_Pin_Base_Key__R_Field_20.newFieldInGroup("pnd_Pin_Base_Key_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12);
        pnd_Actv_Unque_Key = localVariables.newFieldInRecord("pnd_Actv_Unque_Key", "#ACTV-UNQUE-KEY", FieldType.STRING, 16);

        pnd_Actv_Unque_Key__R_Field_21 = localVariables.newGroupInRecord("pnd_Actv_Unque_Key__R_Field_21", "REDEFINE", pnd_Actv_Unque_Key);
        pnd_Actv_Unque_Key_Rqst_Log_Dte_Tme = pnd_Actv_Unque_Key__R_Field_21.newFieldInGroup("pnd_Actv_Unque_Key_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", 
            FieldType.STRING, 15);
        pnd_Actv_Unque_Key_Actve_Ind = pnd_Actv_Unque_Key__R_Field_21.newFieldInGroup("pnd_Actv_Unque_Key_Actve_Ind", "ACTVE-IND", FieldType.STRING, 1);

        corr_Cntrct_Payee_Key = localVariables.newGroupInRecord("corr_Cntrct_Payee_Key", "CORR-CNTRCT-PAYEE-KEY");
        corr_Cntrct_Payee_Key_Pnd_Corr_Cntrct_Payee_Key = corr_Cntrct_Payee_Key.newFieldInGroup("corr_Cntrct_Payee_Key_Pnd_Corr_Cntrct_Payee_Key", "#CORR-CNTRCT-PAYEE-KEY", 
            FieldType.STRING, 13);

        corr_Cntrct_Payee_Key__R_Field_22 = corr_Cntrct_Payee_Key.newGroupInGroup("corr_Cntrct_Payee_Key__R_Field_22", "REDEFINE", corr_Cntrct_Payee_Key_Pnd_Corr_Cntrct_Payee_Key);
        corr_Cntrct_Payee_Key_Correspondence_Addrss_Ind = corr_Cntrct_Payee_Key__R_Field_22.newFieldInGroup("corr_Cntrct_Payee_Key_Correspondence_Addrss_Ind", 
            "CORRESPONDENCE-ADDRSS-IND", FieldType.STRING, 1);
        corr_Cntrct_Payee_Key_Cntrct_Nmbr = corr_Cntrct_Payee_Key__R_Field_22.newFieldInGroup("corr_Cntrct_Payee_Key_Cntrct_Nmbr", "CNTRCT-NMBR", FieldType.STRING, 
            10);
        corr_Cntrct_Payee_Key_Cntrct_Payee_Cde = corr_Cntrct_Payee_Key__R_Field_22.newFieldInGroup("corr_Cntrct_Payee_Key_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Cab_Key = localVariables.newFieldInRecord("pnd_Cab_Key", "#CAB-KEY", FieldType.STRING, 14);

        pnd_Cab_Key__R_Field_23 = localVariables.newGroupInRecord("pnd_Cab_Key__R_Field_23", "REDEFINE", pnd_Cab_Key);
        pnd_Cab_Key_Pnd_Cabinet_Type = pnd_Cab_Key__R_Field_23.newFieldInGroup("pnd_Cab_Key_Pnd_Cabinet_Type", "#CABINET-TYPE", FieldType.STRING, 1);
        pnd_Cab_Key_Pnd_Pin_Nbr = pnd_Cab_Key__R_Field_23.newFieldInGroup("pnd_Cab_Key_Pnd_Pin_Nbr", "#PIN-NBR", FieldType.NUMERIC, 12);
        pnd_Tbl_Key = localVariables.newFieldInRecord("pnd_Tbl_Key", "#TBL-KEY", FieldType.STRING, 53);

        pnd_Tbl_Key__R_Field_24 = localVariables.newGroupInRecord("pnd_Tbl_Key__R_Field_24", "REDEFINE", pnd_Tbl_Key);
        pnd_Tbl_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Key__R_Field_24.newFieldInGroup("pnd_Tbl_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", FieldType.STRING, 
            2);
        pnd_Tbl_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Key__R_Field_24.newFieldInGroup("pnd_Tbl_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 20);
        pnd_Tbl_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Key__R_Field_24.newFieldInGroup("pnd_Tbl_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 30);
        pnd_Tbl_Key_Pnd_Tbl_Actve_Ind = pnd_Tbl_Key__R_Field_24.newFieldInGroup("pnd_Tbl_Key_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", FieldType.STRING, 1);
        pnd_Geo_Grp = localVariables.newFieldInRecord("pnd_Geo_Grp", "#GEO-GRP", FieldType.STRING, 4);

        pnd_Geo_Grp__R_Field_25 = localVariables.newGroupInRecord("pnd_Geo_Grp__R_Field_25", "REDEFINE", pnd_Geo_Grp);
        pnd_Geo_Grp_Pnd_State = pnd_Geo_Grp__R_Field_25.newFieldInGroup("pnd_Geo_Grp_Pnd_State", "#STATE", FieldType.STRING, 2);
        pnd_Geo_Grp_Pnd_Geo_Code = pnd_Geo_Grp__R_Field_25.newFieldInGroup("pnd_Geo_Grp_Pnd_Geo_Code", "#GEO-CODE", FieldType.STRING, 2);
        pnd_Ph_Key = localVariables.newFieldInRecord("pnd_Ph_Key", "#PH-KEY", FieldType.STRING, 8);

        pnd_Ph_Key__R_Field_26 = localVariables.newGroupInRecord("pnd_Ph_Key__R_Field_26", "REDEFINE", pnd_Ph_Key);
        pnd_Ph_Key_Rcrd_Type = pnd_Ph_Key__R_Field_26.newFieldInGroup("pnd_Ph_Key_Rcrd_Type", "RCRD-TYPE", FieldType.STRING, 1);
        pnd_Ph_Key_Srvvr_Mit_Idntfr = pnd_Ph_Key__R_Field_26.newFieldInGroup("pnd_Ph_Key_Srvvr_Mit_Idntfr", "SRVVR-MIT-IDNTFR", FieldType.STRING, 7);
        pnd_Mit_Read_Cntr = localVariables.newFieldInRecord("pnd_Mit_Read_Cntr", "#MIT-READ-CNTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Work_Read_Cntr = localVariables.newFieldInRecord("pnd_Work_Read_Cntr", "#WORK-READ-CNTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Wr_Lead_Cntr = localVariables.newFieldInRecord("pnd_Wr_Lead_Cntr", "#WR-LEAD-CNTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Cust_Ad_Cntr = localVariables.newFieldInRecord("pnd_Cust_Ad_Cntr", "#CUST-AD-CNTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Cust_Up_Cntr = localVariables.newFieldInRecord("pnd_Cust_Up_Cntr", "#CUST-UP-CNTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Customer_Cntr = localVariables.newFieldInRecord("pnd_Customer_Cntr", "#CUSTOMER-CNTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Tlc_Cntr = localVariables.newFieldInRecord("pnd_Tlc_Cntr", "#TLC-CNTR", FieldType.PACKED_DECIMAL, 8);
        pnd_No_Base_Rec = localVariables.newFieldInRecord("pnd_No_Base_Rec", "#NO-BASE-REC", FieldType.PACKED_DECIMAL, 8);
        pnd_Vac_Address = localVariables.newFieldInRecord("pnd_Vac_Address", "#VAC-ADDRESS", FieldType.PACKED_DECIMAL, 8);
        pnd_No_Base_Rec_V = localVariables.newFieldInRecord("pnd_No_Base_Rec_V", "#NO-BASE-REC-V", FieldType.PACKED_DECIMAL, 8);
        pnd_No_State_In_Table = localVariables.newFieldInRecord("pnd_No_State_In_Table", "#NO-STATE-IN-TABLE", FieldType.PACKED_DECIMAL, 8);
        pnd_Pass_Nfnd_Cntr = localVariables.newFieldInRecord("pnd_Pass_Nfnd_Cntr", "#PASS-NFND-CNTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Add = localVariables.newFieldInRecord("pnd_Add", "#ADD", FieldType.BOOLEAN, 1);
        pnd_Update = localVariables.newFieldInRecord("pnd_Update", "#UPDATE", FieldType.BOOLEAN, 1);
        pnd_Act_Strt_Time_T = localVariables.newFieldInRecord("pnd_Act_Strt_Time_T", "#ACT-STRT-TIME-T", FieldType.TIME);
        pnd_Foreign_Address = localVariables.newFieldInRecord("pnd_Foreign_Address", "#FOREIGN-ADDRESS", FieldType.BOOLEAN, 1);
        pnd_Activity_Type = localVariables.newFieldInRecord("pnd_Activity_Type", "#ACTIVITY-TYPE", FieldType.STRING, 4);
        pnd_Pin_Nbr_Save = localVariables.newFieldInRecord("pnd_Pin_Nbr_Save", "#PIN-NBR-SAVE", FieldType.STRING, 12);

        pnd_Pin_Nbr_Save__R_Field_27 = localVariables.newGroupInRecord("pnd_Pin_Nbr_Save__R_Field_27", "REDEFINE", pnd_Pin_Nbr_Save);
        pnd_Pin_Nbr_Save_Pnd_Pin_Nbr_Save_N = pnd_Pin_Nbr_Save__R_Field_27.newFieldInGroup("pnd_Pin_Nbr_Save_Pnd_Pin_Nbr_Save_N", "#PIN-NBR-SAVE-N", FieldType.NUMERIC, 
            12);

        pnd_Frgn_Geo_Pass = localVariables.newGroupInRecord("pnd_Frgn_Geo_Pass", "#FRGN-GEO-PASS");
        pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_1 = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_1", "#GEO-ADDR-LINE-1", FieldType.STRING, 
            35);
        pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_2 = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_2", "#GEO-ADDR-LINE-2", FieldType.STRING, 
            35);
        pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_3 = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_3", "#GEO-ADDR-LINE-3", FieldType.STRING, 
            35);
        pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_4 = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_4", "#GEO-ADDR-LINE-4", FieldType.STRING, 
            35);
        pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_5 = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_5", "#GEO-ADDR-LINE-5", FieldType.STRING, 
            35);
        pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_6 = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_6", "#GEO-ADDR-LINE-6", FieldType.STRING, 
            35);
        pnd_Frgn_Geo_Pass_Pnd_Return_Cde = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_Return_Cde", "#RETURN-CDE", FieldType.NUMERIC, 1);
        pnd_Frgn_Geo_Pass_Pnd_Mail_Cde = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_Mail_Cde", "#MAIL-CDE", FieldType.STRING, 1);
        pnd_Frgn_Geo_Pass_Pnd_Geo_Cde = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_Geo_Cde", "#GEO-CDE", FieldType.STRING, 2);
        pnd_Frgn_Geo_Pass_Pnd_St_Cde1 = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_St_Cde1", "#ST-CDE1", FieldType.STRING, 2);
        pnd_Frgn_Geo_Pass_Pnd_St_Cde2 = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_St_Cde2", "#ST-CDE2", FieldType.STRING, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        setTimeRecord = new DbsRecord();
        st = setTimeRecord.newFieldInRecord("ST", "ST", FieldType.NUMERIC, 7);
        registerRecord(setTimeRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Master_View_Select.reset();
        vw_cwf_Sb_Ph.reset();
        vw_cwf_Efm_Document.reset();

        ldaCwfl4817.initializeValues();
        ldaCwfl4818.initializeValues();
        ldaCwfl4815.initializeValues();
        ldaCwfl4832.initializeValues();
        ldaCwfl4898.initializeValues();
        ldaCwfl4833.initializeValues();
        ldaCwfl4897.initializeValues();
        ldaCwfl4824.initializeValues();
        ldaCwfl4829.initializeValues();
        ldaCwfl4830.initializeValues();

        localVariables.reset();
        pnd_Atsign.setInitialValue("@");
        pnd_Pin_Base_Key.setInitialValue("            B");
        pnd_Cab_Key.setInitialValue("P");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb4830() throws Exception
    {
        super("Cwfb4830");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("CWFB4830", onError);
        setupReports();
        //*  ----------- INITIALIZATION ---------------------
        //*  ----------- MAIN LOGIC -------------------------                                                                                                             //Natural: FORMAT ( 0 ) PS = 56 LS = 131;//Natural: FORMAT ( 1 ) PS = 56 LS = 131
        //*   TO ALLOW FOR EASY ESCAPE FROM PROGRAM.
        REP1:                                                                                                                                                             //Natural: REPEAT
        while (condition(whileTrue))
        {
            st = Global.getTIMN();                                                                                                                                        //Natural: SET TIME
                                                                                                                                                                          //Natural: PERFORM READ-RUN-CONTROL
            sub_Read_Run_Control();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM SELECT-MASTER-RECORDS
            sub_Select_Master_Records();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-TOTALS
            sub_Write_Totals();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTROL
            sub_Update_Control();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM ESCAPE-PROGRAM
            sub_Escape_Program();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ESCAPE-PROGRAM
            //*   (REP1.)
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  ================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-RUN-CONTROL
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SELECT-MASTER-RECORDS
        //* **************************************
        //* ***************
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WORK-LEAD
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FILL-WORK-LEAD
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-WR-LEAD-DATES
        //* *************
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DERIVE-INDICATORS
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-DOCUMENT-TYPE
        //* ***************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CUSTOMER-RECORD
        //* *************
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-NAME-SSN
        //* *****************************
        //* *************
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-PART-AGE
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TOTALS
        //*  'Number of Customer Add Records Created          = ' #CUST-AD-CNTR
        //*  'Number of Customer Upd Records Created          = ' #CUST-UP-CNTR
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-CONTROL
        //* *******************************                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Escape_Program() throws Exception                                                                                                                    //Natural: ESCAPE-PROGRAM
    {
        if (BLNatReinput.isReinput()) return;

        //*  ================================
        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( REP1. )
        Global.setEscapeCode(EscapeType.Bottom, "REP1");
        if (true) return;
    }
    private void sub_Read_Run_Control() throws Exception                                                                                                                  //Natural: READ-RUN-CONTROL
    {
        if (BLNatReinput.isReinput()) return;

        //*  ================================
        //*  CALL THE SUB PROGRAM TO FIND "Acitive" RUN CONTROL RECORD WHICH HAS
        //*  BEEN ESTABLISH IN A PREVIOUS STEP.
        //*  ---------------------------------------------------------------------
        DbsUtil.callnat(Cwfn4816.class , getCurrentProcessState(), pdaCwfa4815.getCwfa4815_Output(), pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(),             //Natural: CALLNAT 'CWFN4816' CWFA4815-OUTPUT CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
            pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("E") || pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("W")))                    //Natural: IF MSG-INFO-SUB.##RETURN-CODE = 'E' OR MSG-INFO-SUB.##RETURN-CODE = 'W'
        {
            getReports().write(0, ReportOption.NOTITLE,new TabSetting(10),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                     //Natural: WRITE 10T MSG-INFO-SUB.##MSG
            if (Global.isEscape()) return;
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("E")))                                                                                 //Natural: IF MSG-INFO-SUB.##RETURN-CODE = 'E'
            {
                DbsUtil.terminate(10);  if (true) return;                                                                                                                 //Natural: TERMINATE 10
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().equals(999)))                                                                                          //Natural: IF MSG-INFO-SUB.##MSG-NR = 999
        {
            getReports().write(0, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(10),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                  //Natural: WRITE ( 0 ) NOTITLE NOHDR 10T MSG-INFO-SUB.##MSG
            if (Global.isEscape()) return;
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
            DbsUtil.terminate(11);  if (true) return;                                                                                                                     //Natural: TERMINATE 11
        }                                                                                                                                                                 //Natural: END-IF
        //*  ------------------------------------------
        GET1:                                                                                                                                                             //Natural: GET CWF-SUPPORT-TBL CWFA4815-OUTPUT.RUN-CONTROL-ISN
        ldaCwfl4815.getVw_cwf_Support_Tbl().readByID(pdaCwfa4815.getCwfa4815_Output_Run_Control_Isn().getLong(), "GET1");
        pnd_Tbl_Start_Dte_Tme_T.setValue(ldaCwfl4815.getCwf_Support_Tbl_Starting_Date());                                                                                 //Natural: MOVE CWF-SUPPORT-TBL.STARTING-DATE TO #TBL-START-DTE-TME-T
        pnd_Tbl_End_Dte_Tme_T.setValue(ldaCwfl4815.getCwf_Support_Tbl_Ending_Date());                                                                                     //Natural: MOVE CWF-SUPPORT-TBL.ENDING-DATE TO #TBL-END-DTE-TME-T
    }
    private void sub_Select_Master_Records() throws Exception                                                                                                             //Natural: SELECT-MASTER-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        R1:                                                                                                                                                               //Natural: READ WORK FILE 1 #WORK-FILE-1
        while (condition(getWorkFiles().read(1, pnd_Work_File_1)))
        {
            //*  --------------------- CHECK RQST-LOG-DTE-TME -----------------
            if (condition(! (DbsUtil.maskMatches(pnd_Work_File_1_Pnd_Rqst_Log_Dte_Tme,"YYYYMMDD00-2400-6000-60N"))))                                                      //Natural: IF #WORK-FILE-1.#RQST-LOG-DTE-TME NE MASK ( YYYYMMDD00-2400-6000-60N )
            {
                getReports().write(0, ReportOption.NOTITLE,NEWLINE,"Work-file.Rqst-Log-Dte-Tme has wrong value = ",pnd_Work_File_1_Pnd_Rqst_Log_Dte_Tme);                 //Natural: WRITE /'Work-file.Rqst-Log-Dte-Tme has wrong value = ' #WORK-FILE-1.#RQST-LOG-DTE-TME
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Work_Read_Cntr.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #WORK-READ-CNTR
            ldaCwfl4817.getVw_cwf_Master().startDatabaseRead                                                                                                              //Natural: READ CWF-MASTER BY RQST-HISTORY-KEY STARTING FROM #WORK-FILE-1.#RQST-LOG-DTE-TME
            (
            "R2",
            new Wc[] { new Wc("RQST_HISTORY_KEY", ">=", pnd_Work_File_1_Pnd_Rqst_Log_Dte_Tme, WcType.BY) },
            new Oc[] { new Oc("RQST_HISTORY_KEY", "ASC") }
            );
            R2:
            while (condition(ldaCwfl4817.getVw_cwf_Master().readNextRow("R2")))
            {
                //*  --- AVOID PROCESSING RECORD FOR THE FOLLOWING REASONS -----
                if (condition(ldaCwfl4817.getCwf_Master_Rqst_Log_Dte_Tme().notEquals(pnd_Work_File_1_Pnd_Rqst_Log_Dte_Tme)))                                              //Natural: IF CWF-MASTER.RQST-LOG-DTE-TME NE #WORK-FILE-1.#RQST-LOG-DTE-TME
                {
                    if (true) break R2;                                                                                                                                   //Natural: ESCAPE BOTTOM ( R2. ) IMMEDIATE
                }                                                                                                                                                         //Natural: END-IF
                pnd_Last_Chnge_Dte_Tme_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaCwfl4817.getCwf_Master_Last_Chnge_Dte_Tme_A());                          //Natural: MOVE EDITED CWF-MASTER.LAST-CHNGE-DTE-TME-A TO #LAST-CHNGE-DTE-TME-T ( EM = YYYYMMDDHHIISST )
                if (condition(pnd_Last_Chnge_Dte_Tme_T.less(ldaCwfl4815.getCwf_Support_Tbl_Starting_Date())))                                                             //Natural: IF #LAST-CHNGE-DTE-TME-T LT CWF-SUPPORT-TBL.STARTING-DATE
                {
                    //*  AND CWF-MASTER.LAST-UPDTE-DTE-TME LT CWF-SUPPORT-TBL.STARTING-DATE
                    if (true) break R2;                                                                                                                                   //Natural: ESCAPE BOTTOM ( R2. ) IMMEDIATE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Last_Chnge_Dte_Tme_T.greater(ldaCwfl4815.getCwf_Support_Tbl_Ending_Date())))                                                            //Natural: IF #LAST-CHNGE-DTE-TME-T GT CWF-SUPPORT-TBL.ENDING-DATE
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                    //*  JVH 6/01
                }                                                                                                                                                         //Natural: END-IF
                pnd_Act_Strt_Time_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaCwfl4817.getCwf_Master_Rqst_Log_Dte_Tme());                                   //Natural: MOVE EDITED CWF-MASTER.RQST-LOG-DTE-TME TO #ACT-STRT-TIME-T ( EM = YYYYMMDDHHIISST )
                pnd_Add.setValue(false);                                                                                                                                  //Natural: MOVE FALSE TO #ADD #UPDATE
                pnd_Update.setValue(false);
                //*  ----------------------------------------------------------------
                pnd_Rqst_Log_Dte_Tme_Save.setValue(ldaCwfl4817.getCwf_Master_Rqst_Log_Dte_Tme());                                                                         //Natural: MOVE CWF-MASTER.RQST-LOG-DTE-TME TO #RQST-LOG-DTE-TME-SAVE
                pnd_Isn.setValue(ldaCwfl4817.getVw_cwf_Master().getAstISN("R2"));                                                                                         //Natural: ASSIGN #ISN := *ISN
                if (condition(pnd_Act_Strt_Time_T.less(ldaCwfl4815.getCwf_Support_Tbl_Starting_Date())))                                                                  //Natural: IF #ACT-STRT-TIME-T LT CWF-SUPPORT-TBL.STARTING-DATE
                {
                    pnd_Update.setValue(true);                                                                                                                            //Natural: MOVE TRUE TO #UPDATE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Add.setValue(true);                                                                                                                               //Natural: MOVE TRUE TO #ADD
                }                                                                                                                                                         //Natural: END-IF
                pnd_Mit_Read_Cntr.nadd(1);                                                                                                                                //Natural: ADD 1 TO #MIT-READ-CNTR
                //* *****
                //*  ------------- CREATE WORK REQUEST LEAD RECORD  -----------------------
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-LEAD
                sub_Write_Work_Lead();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  ------------- CREATE CUSTOMER RECORD ALWAYS    -----------------------
                if (condition(ldaCwfl4817.getCwf_Master_Pin_Nbr().notEquals(pnd_Pin_Nbr_Save_Pnd_Pin_Nbr_Save_N)))                                                        //Natural: IF CWF-MASTER.PIN-NBR NE #PIN-NBR-SAVE-N
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-CUSTOMER-RECORD
                    sub_Write_Customer_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R2"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Pin_Nbr_Save.setValue(ldaCwfl4817.getCwf_Master_Pin_Nbr());                                                                                       //Natural: MOVE CWF-MASTER.PIN-NBR TO #PIN-NBR-SAVE
                }                                                                                                                                                         //Natural: END-IF
                //*  ----------------------------------------------------------------------
                //*  R2.
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  R1.
        }                                                                                                                                                                 //Natural: END-WORK
        R1_Exit:
        if (Global.isEscape()) return;
    }
    private void sub_Write_Work_Lead() throws Exception                                                                                                                   //Natural: WRITE-WORK-LEAD
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        ldaCwfl4832.getWork_Lead().reset();                                                                                                                               //Natural: RESET WORK-LEAD CUSTOMER
        ldaCwfl4833.getCustomer().reset();
                                                                                                                                                                          //Natural: PERFORM FILL-WORK-LEAD
        sub_Fill_Work_Lead();
        if (condition(Global.isEscape())) {return;}
        //*  WRITE WORK FILE 2 WORK-LEAD
        ldaCwfl4898.getWork_Lead_New().reset();                                                                                                                           //Natural: RESET WORK-LEAD-NEW
        if (condition(DbsUtil.maskMatches(ldaCwfl4832.getWork_Lead_Pin_Npin(),"N...........")))                                                                           //Natural: IF WORK-LEAD.PIN-NPIN EQ MASK ( N........... )
        {
            ldaCwfl4898.getWork_Lead_New_Pin_Npin().setValue(ldaCwfl4832.getWork_Lead_Pin_Npin().getSubstring(6,7));                                                      //Natural: MOVE SUBSTR ( WORK-LEAD.PIN-NPIN,6,7 ) TO WORK-LEAD-NEW.PIN-NPIN
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCwfl4898.getWork_Lead_New_Pin_Npin().setValue(ldaCwfl4832.getWork_Lead_Pin_Npin().getSubstring(1,7));                                                      //Natural: MOVE SUBSTR ( WORK-LEAD.PIN-NPIN,1,7 ) TO WORK-LEAD-NEW.PIN-NPIN
        }                                                                                                                                                                 //Natural: END-IF
        ldaCwfl4898.getWork_Lead_New_C1().setValue(ldaCwfl4832.getWork_Lead_A1());                                                                                        //Natural: MOVE WORK-LEAD.A1 TO WORK-LEAD-NEW.C1
        ldaCwfl4898.getWork_Lead_New_Rqst_Log_Dte_Tme_A_15().setValue(ldaCwfl4832.getWork_Lead_Rqst_Log_Dte_Tme_A_15());                                                  //Natural: MOVE WORK-LEAD.RQST-LOG-DTE-TME-A-15 TO WORK-LEAD-NEW.RQST-LOG-DTE-TME-A-15
        ldaCwfl4898.getWork_Lead_New_C2().setValue(ldaCwfl4832.getWork_Lead_A2());                                                                                        //Natural: MOVE WORK-LEAD.A2 TO WORK-LEAD-NEW.C2
        ldaCwfl4898.getWork_Lead_New_Last_Chnge_Dte_Tme_A().setValue(ldaCwfl4832.getWork_Lead_Last_Chnge_Dte_Tme_A());                                                    //Natural: MOVE WORK-LEAD.LAST-CHNGE-DTE-TME-A TO WORK-LEAD-NEW.LAST-CHNGE-DTE-TME-A
        ldaCwfl4898.getWork_Lead_New_C3().setValue(ldaCwfl4832.getWork_Lead_A3());                                                                                        //Natural: MOVE WORK-LEAD.A3 TO WORK-LEAD-NEW.C3
        ldaCwfl4898.getWork_Lead_New_Work_Prcss_Id().setValue(ldaCwfl4832.getWork_Lead_Work_Prcss_Id());                                                                  //Natural: MOVE WORK-LEAD.WORK-PRCSS-ID TO WORK-LEAD-NEW.WORK-PRCSS-ID
        ldaCwfl4898.getWork_Lead_New_C4().setValue(ldaCwfl4832.getWork_Lead_A4());                                                                                        //Natural: MOVE WORK-LEAD.A4 TO WORK-LEAD-NEW.C4
        ldaCwfl4898.getWork_Lead_New_Tiaa_Rcvd_Dte_Tme_A().setValue(ldaCwfl4832.getWork_Lead_Tiaa_Rcvd_Dte_Tme_A());                                                      //Natural: MOVE WORK-LEAD.TIAA-RCVD-DTE-TME-A TO WORK-LEAD-NEW.TIAA-RCVD-DTE-TME-A
        ldaCwfl4898.getWork_Lead_New_C5().setValue(ldaCwfl4832.getWork_Lead_A5());                                                                                        //Natural: MOVE WORK-LEAD.A5 TO WORK-LEAD-NEW.C5
        ldaCwfl4898.getWork_Lead_New_Rqst_Log_Oprtr_Cde().setValue(ldaCwfl4832.getWork_Lead_Rqst_Log_Oprtr_Cde());                                                        //Natural: MOVE WORK-LEAD.RQST-LOG-OPRTR-CDE TO WORK-LEAD-NEW.RQST-LOG-OPRTR-CDE
        ldaCwfl4898.getWork_Lead_New_C6().setValue(ldaCwfl4832.getWork_Lead_A6());                                                                                        //Natural: MOVE WORK-LEAD.A6 TO WORK-LEAD-NEW.C6
        ldaCwfl4898.getWork_Lead_New_Orgnl_Unit_Cde().setValue(ldaCwfl4832.getWork_Lead_Orgnl_Unit_Cde());                                                                //Natural: MOVE WORK-LEAD.ORGNL-UNIT-CDE TO WORK-LEAD-NEW.ORGNL-UNIT-CDE
        ldaCwfl4898.getWork_Lead_New_C7().setValue(ldaCwfl4832.getWork_Lead_A7());                                                                                        //Natural: MOVE WORK-LEAD.A7 TO WORK-LEAD-NEW.C7
        ldaCwfl4898.getWork_Lead_New_Admin_Unit_Cde().setValue(ldaCwfl4832.getWork_Lead_Admin_Unit_Cde());                                                                //Natural: MOVE WORK-LEAD.ADMIN-UNIT-CDE TO WORK-LEAD-NEW.ADMIN-UNIT-CDE
        ldaCwfl4898.getWork_Lead_New_C8().setValue(ldaCwfl4832.getWork_Lead_A8());                                                                                        //Natural: MOVE WORK-LEAD.A8 TO WORK-LEAD-NEW.C8
        ldaCwfl4898.getWork_Lead_New_Admin_Status_Cde().setValue(ldaCwfl4832.getWork_Lead_Admin_Status_Cde());                                                            //Natural: MOVE WORK-LEAD.ADMIN-STATUS-CDE TO WORK-LEAD-NEW.ADMIN-STATUS-CDE
        ldaCwfl4898.getWork_Lead_New_C9().setValue(ldaCwfl4832.getWork_Lead_A9());                                                                                        //Natural: MOVE WORK-LEAD.A9 TO WORK-LEAD-NEW.C9
        ldaCwfl4898.getWork_Lead_New_Document_Type().setValue(ldaCwfl4832.getWork_Lead_Document_Type());                                                                  //Natural: MOVE WORK-LEAD.DOCUMENT-TYPE TO WORK-LEAD-NEW.DOCUMENT-TYPE
        ldaCwfl4898.getWork_Lead_New_C10().setValue(ldaCwfl4832.getWork_Lead_A10());                                                                                      //Natural: MOVE WORK-LEAD.A10 TO WORK-LEAD-NEW.C10
        ldaCwfl4898.getWork_Lead_New_Empl_Oprtr_Cde().setValue(ldaCwfl4832.getWork_Lead_Empl_Oprtr_Cde());                                                                //Natural: MOVE WORK-LEAD.EMPL-OPRTR-CDE TO WORK-LEAD-NEW.EMPL-OPRTR-CDE
        ldaCwfl4898.getWork_Lead_New_C11().setValue(ldaCwfl4832.getWork_Lead_A11());                                                                                      //Natural: MOVE WORK-LEAD.A11 TO WORK-LEAD-NEW.C11
        ldaCwfl4898.getWork_Lead_New_Crprte_Due_Dte_Tme_A().setValue(ldaCwfl4832.getWork_Lead_Crprte_Due_Dte_Tme_A());                                                    //Natural: MOVE WORK-LEAD.CRPRTE-DUE-DTE-TME-A TO WORK-LEAD-NEW.CRPRTE-DUE-DTE-TME-A
        ldaCwfl4898.getWork_Lead_New_C12().setValue(ldaCwfl4832.getWork_Lead_A12());                                                                                      //Natural: MOVE WORK-LEAD.A12 TO WORK-LEAD-NEW.C12
        ldaCwfl4898.getWork_Lead_New_Crprte_Status_Ind().setValue(ldaCwfl4832.getWork_Lead_Crprte_Status_Ind());                                                          //Natural: MOVE WORK-LEAD.CRPRTE-STATUS-IND TO WORK-LEAD-NEW.CRPRTE-STATUS-IND
        ldaCwfl4898.getWork_Lead_New_C13().setValue(ldaCwfl4832.getWork_Lead_A13());                                                                                      //Natural: MOVE WORK-LEAD.A13 TO WORK-LEAD-NEW.C13
        ldaCwfl4898.getWork_Lead_New_Part_Close_Status_A().setValue(ldaCwfl4832.getWork_Lead_Part_Close_Status_A());                                                      //Natural: MOVE WORK-LEAD.PART-CLOSE-STATUS-A TO WORK-LEAD-NEW.PART-CLOSE-STATUS-A
        ldaCwfl4898.getWork_Lead_New_C14().setValue(ldaCwfl4832.getWork_Lead_A14());                                                                                      //Natural: MOVE WORK-LEAD.A14 TO WORK-LEAD-NEW.C14
        ldaCwfl4898.getWork_Lead_New_Crprte_Clock_End_Dte_Tme_A().setValue(ldaCwfl4832.getWork_Lead_Crprte_Clock_End_Dte_Tme_A());                                        //Natural: MOVE WORK-LEAD.CRPRTE-CLOCK-END-DTE-TME-A TO WORK-LEAD-NEW.CRPRTE-CLOCK-END-DTE-TME-A
        ldaCwfl4898.getWork_Lead_New_C15().setValue(ldaCwfl4832.getWork_Lead_A15());                                                                                      //Natural: MOVE WORK-LEAD.A15 TO WORK-LEAD-NEW.C15
        ldaCwfl4898.getWork_Lead_New_Moc().setValue(ldaCwfl4832.getWork_Lead_Moc());                                                                                      //Natural: MOVE WORK-LEAD.MOC TO WORK-LEAD-NEW.MOC
        ldaCwfl4898.getWork_Lead_New_C16().setValue(ldaCwfl4832.getWork_Lead_A16());                                                                                      //Natural: MOVE WORK-LEAD.A16 TO WORK-LEAD-NEW.C16
        ldaCwfl4898.getWork_Lead_New_Part_Age().setValue(ldaCwfl4832.getWork_Lead_Part_Age());                                                                            //Natural: MOVE WORK-LEAD.PART-AGE TO WORK-LEAD-NEW.PART-AGE
        ldaCwfl4898.getWork_Lead_New_C17().setValue(ldaCwfl4832.getWork_Lead_A17());                                                                                      //Natural: MOVE WORK-LEAD.A17 TO WORK-LEAD-NEW.C17
        ldaCwfl4898.getWork_Lead_New_Last_Chnge_Oprtr_Cde().setValue(ldaCwfl4832.getWork_Lead_Last_Chnge_Oprtr_Cde());                                                    //Natural: MOVE WORK-LEAD.LAST-CHNGE-OPRTR-CDE TO WORK-LEAD-NEW.LAST-CHNGE-OPRTR-CDE
        ldaCwfl4898.getWork_Lead_New_C18().setValue(ldaCwfl4832.getWork_Lead_A18());                                                                                      //Natural: MOVE WORK-LEAD.A18 TO WORK-LEAD-NEW.C18
        ldaCwfl4898.getWork_Lead_New_Crprte_Rqst_Log_Dte_Tme_A().setValue(ldaCwfl4832.getWork_Lead_Crprte_Rqst_Log_Dte_Tme_A());                                          //Natural: MOVE WORK-LEAD.CRPRTE-RQST-LOG-DTE-TME-A TO WORK-LEAD-NEW.CRPRTE-RQST-LOG-DTE-TME-A
        ldaCwfl4898.getWork_Lead_New_C19().setValue(ldaCwfl4832.getWork_Lead_A19());                                                                                      //Natural: MOVE WORK-LEAD.A19 TO WORK-LEAD-NEW.C19
        ldaCwfl4898.getWork_Lead_New_Actve_Ind().setValue(ldaCwfl4832.getWork_Lead_Actve_Ind());                                                                          //Natural: MOVE WORK-LEAD.ACTVE-IND TO WORK-LEAD-NEW.ACTVE-IND
        ldaCwfl4898.getWork_Lead_New_C20().setValue(ldaCwfl4832.getWork_Lead_A20());                                                                                      //Natural: MOVE WORK-LEAD.A20 TO WORK-LEAD-NEW.C20
        ldaCwfl4898.getWork_Lead_New_Status_Cde().setValue(ldaCwfl4832.getWork_Lead_Status_Cde());                                                                        //Natural: MOVE WORK-LEAD.STATUS-CDE TO WORK-LEAD-NEW.STATUS-CDE
        ldaCwfl4898.getWork_Lead_New_C21().setValue(ldaCwfl4832.getWork_Lead_A21());                                                                                      //Natural: MOVE WORK-LEAD.A21 TO WORK-LEAD-NEW.C21
        ldaCwfl4898.getWork_Lead_New_Admin_Status_Updte_Dte_Tme_A().setValue(ldaCwfl4832.getWork_Lead_Admin_Status_Updte_Dte_Tme_A());                                    //Natural: MOVE WORK-LEAD.ADMIN-STATUS-UPDTE-DTE-TME-A TO WORK-LEAD-NEW.ADMIN-STATUS-UPDTE-DTE-TME-A
        getWorkFiles().write(2, false, ldaCwfl4898.getWork_Lead_New());                                                                                                   //Natural: WRITE WORK FILE 2 WORK-LEAD-NEW
        pnd_Wr_Lead_Cntr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #WR-LEAD-CNTR
    }
    private void sub_Fill_Work_Lead() throws Exception                                                                                                                    //Natural: FILL-WORK-LEAD
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        ldaCwfl4832.getWork_Lead().setValuesByName(ldaCwfl4817.getVw_cwf_Master());                                                                                       //Natural: MOVE BY NAME CWF-MASTER TO WORK-LEAD
        //*  MOVE CWF-MASTER.PIN-NBR TO WORK-LEAD.PIN-NPIN           /* PIN-EXP
        //*  PIN-EXP
        ldaCwfl4832.getWork_Lead_Pin_Npin().moveAll(ldaCwfl4817.getCwf_Master_Pin_Nbr());                                                                                 //Natural: MOVE ALL CWF-MASTER.PIN-NBR TO WORK-LEAD.PIN-NPIN
        ldaCwfl4832.getWork_Lead_A1().setValue(pnd_Atsign);                                                                                                               //Natural: MOVE #ATSIGN TO A1 A2 A3 A4 A5 A6 A7 A8 A9 A10 A11 A12 A13 A14 A15 A16 A17 A18 A19 A20 A21
        ldaCwfl4832.getWork_Lead_A2().setValue(pnd_Atsign);
        ldaCwfl4832.getWork_Lead_A3().setValue(pnd_Atsign);
        ldaCwfl4832.getWork_Lead_A4().setValue(pnd_Atsign);
        ldaCwfl4832.getWork_Lead_A5().setValue(pnd_Atsign);
        ldaCwfl4832.getWork_Lead_A6().setValue(pnd_Atsign);
        ldaCwfl4832.getWork_Lead_A7().setValue(pnd_Atsign);
        ldaCwfl4832.getWork_Lead_A8().setValue(pnd_Atsign);
        ldaCwfl4832.getWork_Lead_A9().setValue(pnd_Atsign);
        ldaCwfl4832.getWork_Lead_A10().setValue(pnd_Atsign);
        ldaCwfl4832.getWork_Lead_A11().setValue(pnd_Atsign);
        ldaCwfl4832.getWork_Lead_A12().setValue(pnd_Atsign);
        ldaCwfl4832.getWork_Lead_A13().setValue(pnd_Atsign);
        ldaCwfl4832.getWork_Lead_A14().setValue(pnd_Atsign);
        ldaCwfl4832.getWork_Lead_A15().setValue(pnd_Atsign);
        ldaCwfl4832.getWork_Lead_A16().setValue(pnd_Atsign);
        ldaCwfl4832.getWork_Lead_A17().setValue(pnd_Atsign);
        ldaCwfl4832.getWork_Lead_A18().setValue(pnd_Atsign);
        ldaCwfl4832.getWork_Lead_A19().setValue(pnd_Atsign);
        ldaCwfl4832.getWork_Lead_A20().setValue(pnd_Atsign);
        ldaCwfl4832.getWork_Lead_A21().setValue(pnd_Atsign);
        if (condition(ldaCwfl4817.getCwf_Master_Status_Cde().less("8000")))                                                                                               //Natural: IF CWF-MASTER.STATUS-CDE LT '8000'
        {
            ldaCwfl4832.getWork_Lead_Part_Close_Status_A().setValue(" ");                                                                                                 //Natural: MOVE ' ' TO WORK-LEAD.PART-CLOSE-STATUS-A
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCwfl4832.getWork_Lead_Part_Close_Status_A().setValue(ldaCwfl4817.getCwf_Master_Status_Cde());                                                              //Natural: MOVE CWF-MASTER.STATUS-CDE TO WORK-LEAD.PART-CLOSE-STATUS-A
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF ADMIN-STATUS-CDE-1 = 'C'
        //*    MOVE  CWF-MASTER.STATUS-CDE TO WORK-LEAD.ADMIN-STATUS-CDE
        //*  END-IF
        //*  FOR CUSTOMER FILE
                                                                                                                                                                          //Natural: PERFORM GET-NAME-SSN
        sub_Get_Name_Ssn();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM FORMAT-WR-LEAD-DATES
        sub_Format_Wr_Lead_Dates();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM DERIVE-INDICATORS
        sub_Derive_Indicators();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-PART-AGE
        sub_Get_Part_Age();
        if (condition(Global.isEscape())) {return;}
        //*  ----------- SET UP DOCUMENT KEY -------------------
        pnd_Document_Key_Pnd_P_Value.setValue("P");                                                                                                                       //Natural: MOVE 'P' TO #DOCUMENT-KEY.#P-VALUE
        //*  MOVE CWF-MASTER.PIN-NBR        TO #DOCUMENT-KEY.#PIN-VALUE  /* PIN-EXP
        //*  PIN-EXP
        pnd_Document_Key_Pnd_Pin_Value.moveAll(ldaCwfl4817.getCwf_Master_Pin_Nbr());                                                                                      //Natural: MOVE ALL CWF-MASTER.PIN-NBR TO #DOCUMENT-KEY.#PIN-VALUE
        pnd_Document_Key_Pnd_Tiaa_Rcvd_Dte.setValue(ldaCwfl4817.getCwf_Master_Tiaa_Rcvd_Dte());                                                                           //Natural: MOVE CWF-MASTER.TIAA-RCVD-DTE TO #DOCUMENT-KEY.#TIAA-RCVD-DTE
        pnd_Document_Key_Pnd_Case_Ind.setValue(ldaCwfl4817.getCwf_Master_Case_Id_Cde());                                                                                  //Natural: MOVE CWF-MASTER.CASE-ID-CDE TO #DOCUMENT-KEY.#CASE-IND
        pnd_Document_Key_Pnd_Originating_Work_Prcss_Id.setValue(ldaCwfl4817.getCwf_Master_Work_Prcss_Id());                                                               //Natural: MOVE CWF-MASTER.WORK-PRCSS-ID TO #DOCUMENT-KEY.#ORIGINATING-WORK-PRCSS-ID
        pnd_Document_Key_Pnd_Multi_Rqst_Ind.setValue(ldaCwfl4817.getCwf_Master_Multi_Rqst_Ind());                                                                         //Natural: MOVE CWF-MASTER.MULTI-RQST-IND TO #DOCUMENT-KEY.#MULTI-RQST-IND
        //*  JVH 09/01
        pnd_Document_Key_Pnd_Entry_Dte_Tme.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaCwfl4817.getCwf_Master_Last_Chnge_Dte_Tme_A());                        //Natural: MOVE EDITED CWF-MASTER.LAST-CHNGE-DTE-TME-A TO #DOCUMENT-KEY.#ENTRY-DTE-TME ( EM = YYYYMMDDHHIISST )
        //*  JVH 09/01 - SUB RQST IND IS BLANK ON CWF-MASTER-INDEX BUT HAS
        //*              A '0' VALUE IN THE EFM-DOC FILE - GO FIGURE
        if (condition(ldaCwfl4817.getCwf_Master_Sub_Rqst_Ind().equals(" ")))                                                                                              //Natural: IF CWF-MASTER.SUB-RQST-IND = ' '
        {
            pnd_Document_Key_Pnd_Sub_Rqst_Ind.setValue("0");                                                                                                              //Natural: MOVE '0' TO #DOCUMENT-KEY.#SUB-RQST-IND
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Document_Key_Pnd_Sub_Rqst_Ind.setValue(ldaCwfl4817.getCwf_Master_Sub_Rqst_Ind());                                                                         //Natural: MOVE CWF-MASTER.SUB-RQST-IND TO #DOCUMENT-KEY.#SUB-RQST-IND
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-DOCUMENT-TYPE
        sub_Get_Document_Type();
        if (condition(Global.isEscape())) {return;}
        //*  FILL-WORK-REQUEST
    }
    private void sub_Format_Wr_Lead_Dates() throws Exception                                                                                                              //Natural: FORMAT-WR-LEAD-DATES
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        ldaCwfl4832.getWork_Lead_Rqst_Log_Dte_Tme_A_15().setValue(ldaCwfl4817.getCwf_Master_Rqst_Log_Dte_Tme());                                                          //Natural: MOVE CWF-MASTER.RQST-LOG-DTE-TME TO WORK-LEAD.RQST-LOG-DTE-TME-A-15
        pnd_Time_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaCwfl4817.getCwf_Master_Rqst_Log_Dte_Tme());                                                    //Natural: MOVE EDITED CWF-MASTER.RQST-LOG-DTE-TME TO #TIME-T ( EM = YYYYMMDDHHIISST )
        ldaCwfl4832.getWork_Lead_Crprte_Rqst_Log_Dte_Tme_A().setValueEdited(pnd_Time_T,new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SS:0AP'''"));                         //Natural: MOVE EDITED #TIME-T ( EM = '"'LLL�DD�YYYY�HH':'II':'SS:T'00'AP'"' ) TO WORK-LEAD.CRPRTE-RQST-LOG-DTE-TME-A
        if (condition(DbsUtil.maskMatches(ldaCwfl4817.getCwf_Master_Crprte_Clock_End_Dte_Tme(),"YYYYMMDD00-2400-6000-60N")))                                              //Natural: IF CWF-MASTER.CRPRTE-CLOCK-END-DTE-TME = MASK ( YYYYMMDD00-2400-6000-60N )
        {
            pnd_Time_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaCwfl4817.getCwf_Master_Crprte_Clock_End_Dte_Tme());                                        //Natural: MOVE EDITED CWF-MASTER.CRPRTE-CLOCK-END-DTE-TME TO #TIME-T ( EM = YYYYMMDDHHIISST )
            ldaCwfl4832.getWork_Lead_Crprte_Clock_End_Dte_Tme_A().setValueEdited(pnd_Time_T,new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''"));                      //Natural: MOVE EDITED #TIME-T ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO WORK-LEAD.CRPRTE-CLOCK-END-DTE-TME-A
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCwfl4832.getWork_Lead_Crprte_Clock_End_Dte_Tme_A().setValue(" ");                                                                                          //Natural: MOVE ' ' TO WORK-LEAD.CRPRTE-CLOCK-END-DTE-TME-A
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaCwfl4817.getCwf_Master_Crprte_Due_Dte_Tme().greater(getZero())))                                                                                 //Natural: IF CWF-MASTER.CRPRTE-DUE-DTE-TME GT 0
        {
            ldaCwfl4832.getWork_Lead_Crprte_Due_Dte_Tme_A().setValueEdited(ldaCwfl4817.getCwf_Master_Crprte_Due_Dte_Tme(),new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''")); //Natural: MOVE EDITED CWF-MASTER.CRPRTE-DUE-DTE-TME ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO WORK-LEAD.CRPRTE-DUE-DTE-TME-A
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaCwfl4817.getCwf_Master_Tiaa_Rcvd_Dte_Tme().greater(getZero())))                                                                                  //Natural: IF CWF-MASTER.TIAA-RCVD-DTE-TME GT 0
        {
            ldaCwfl4832.getWork_Lead_Tiaa_Rcvd_Dte_Tme_A().setValueEdited(ldaCwfl4817.getCwf_Master_Tiaa_Rcvd_Dte_Tme(),new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''")); //Natural: MOVE EDITED CWF-MASTER.TIAA-RCVD-DTE-TME ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO WORK-LEAD.TIAA-RCVD-DTE-TME-A
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaCwfl4817.getCwf_Master_Actve_Ind().equals("A")))                                                                                                 //Natural: IF CWF-MASTER.ACTVE-IND = 'A'
        {
            ldaCwfl4832.getWork_Lead_Last_Chnge_Dte_Tme_A().setValue(ldaCwfl4817.getCwf_Master_Last_Chnge_Dte_Tme());                                                     //Natural: MOVE CWF-MASTER.LAST-CHNGE-DTE-TME TO WORK-LEAD.LAST-CHNGE-DTE-TME-A
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Time_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaCwfl4817.getCwf_Master_Last_Chnge_Dte_Tme_A());                                            //Natural: MOVE EDITED CWF-MASTER.LAST-CHNGE-DTE-TME-A TO #TIME-T ( EM = YYYYMMDDHHIISST )
            pnd_Time_T.compute(new ComputeParameters(false, pnd_Time_T), (pnd_Time_T.subtract(1)));                                                                       //Natural: COMPUTE #TIME-T = ( #TIME-T - 1 )
            ldaCwfl4832.getWork_Lead_Last_Chnge_Dte_Tme_A().setValueEdited(pnd_Time_T,new ReportEditMask("YYYYMMDDHHIISST"));                                             //Natural: MOVE EDITED #TIME-T ( EM = YYYYMMDDHHIISST ) TO WORK-LEAD.LAST-CHNGE-DTE-TME-A
        }                                                                                                                                                                 //Natural: END-IF
        ldaCwfl4832.getWork_Lead_Admin_Status_Updte_Dte_Tme_A().setValueEdited(ldaCwfl4817.getCwf_Master_Admin_Status_Updte_Dte_Tme(),new ReportEditMask("YYYYMMDDHHIISST")); //Natural: MOVE EDITED CWF-MASTER.ADMIN-STATUS-UPDTE-DTE-TME ( EM = YYYYMMDDHHIISST ) TO WORK-LEAD.ADMIN-STATUS-UPDTE-DTE-TME-A
        pdaNsta1245.getNsta1245_Pnd_Eff_Dte().reset();                                                                                                                    //Natural: RESET NSTA1245.#EFF-DTE
        if (condition(ldaCwfl4817.getCwf_Master_Tiaa_Rcvd_Dte_Tme().notEquals(getZero())))                                                                                //Natural: IF CWF-MASTER.TIAA-RCVD-DTE-TME NE 0
        {
            pnd_Date_Split.setValueEdited(ldaCwfl4817.getCwf_Master_Tiaa_Rcvd_Dte_Tme(),new ReportEditMask("YYYYMMDDHHIISST"));                                           //Natural: MOVE EDITED CWF-MASTER.TIAA-RCVD-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #DATE-SPLIT
            //*  AGE
            pdaNsta1245.getNsta1245_Pnd_Eff_Dte().setValue(pnd_Date_Split_Pnd_Date_Split_Dte);                                                                            //Natural: MOVE #DATE-SPLIT-DTE TO NSTA1245.#EFF-DTE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Fix_Dte.setValue(ldaCwfl4817.getCwf_Master_Tiaa_Rcvd_Dte());                                                                                              //Natural: ASSIGN #FIX-DTE := CWF-MASTER.TIAA-RCVD-DTE
            if (condition(DbsUtil.maskMatches(pnd_Fix_Dte,"MM'/'DD'/'YY")))                                                                                               //Natural: IF #FIX-DTE EQ MASK ( MM'/'DD'/'YY )
            {
                pdaNsta1245.getNsta1245_Pnd_Eff_Dte().setValueEdited(ldaCwfl4817.getCwf_Master_Tiaa_Rcvd_Dte(),new ReportEditMask("YYYYMMDD"));                           //Natural: MOVE EDITED CWF-MASTER.TIAA-RCVD-DTE ( EM = YYYYMMDD ) TO NSTA1245.#EFF-DTE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* *************
        //*  FORMAT-WR-DATES
    }
    private void sub_Derive_Indicators() throws Exception                                                                                                                 //Natural: DERIVE-INDICATORS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        ldaCwfl4832.getWork_Lead_Crprte_Status_Ind().setValue(ldaCwfl4817.getCwf_Master_Crprte_Status_Ind());                                                             //Natural: MOVE CWF-MASTER.CRPRTE-STATUS-IND TO WORK-LEAD.CRPRTE-STATUS-IND
        //*  METHOD OF COMMUNIC.
        ldaCwfl4832.getWork_Lead_Moc().setValue(ldaCwfl4817.getCwf_Master_Rqst_Orgn_Cde());                                                                               //Natural: MOVE CWF-MASTER.RQST-ORGN-CDE TO WORK-LEAD.MOC
        if (condition(ldaCwfl4817.getCwf_Master_Actve_Ind().equals(" ")))                                                                                                 //Natural: IF CWF-MASTER.ACTVE-IND = ' '
        {
            ldaCwfl4832.getWork_Lead_Actve_Ind().setValue("N ");                                                                                                          //Natural: MOVE 'N ' TO WORK-LEAD.ACTVE-IND
        }                                                                                                                                                                 //Natural: END-IF
        //*  ------------
        //*  DERIVE-INDICATORS
    }
    private void sub_Get_Document_Type() throws Exception                                                                                                                 //Natural: GET-DOCUMENT-TYPE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        vw_cwf_Efm_Document.startDatabaseRead                                                                                                                             //Natural: READ CWF-EFM-DOCUMENT BY DOCUMENT-KEY STARTING FROM #DOCUMENT-KEY
        (
        "READ_EFM",
        new Wc[] { new Wc("DOCUMENT_KEY", ">=", pnd_Document_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("DOCUMENT_KEY", "ASC") }
        );
        READ_EFM:
        while (condition(vw_cwf_Efm_Document.readNextRow("READ_EFM")))
        {
            if (condition(cwf_Efm_Document_Cabinet_Id.notEquals(pnd_Document_Key_Pnd_Cabinet_Id) || cwf_Efm_Document_Tiaa_Rcvd_Dte.notEquals(pnd_Document_Key_Pnd_Tiaa_Rcvd_Dte)  //Natural: IF CWF-EFM-DOCUMENT.CABINET-ID NE #DOCUMENT-KEY.#CABINET-ID OR CWF-EFM-DOCUMENT.TIAA-RCVD-DTE NE #DOCUMENT-KEY.#TIAA-RCVD-DTE OR CWF-EFM-DOCUMENT.CASE-WORKPRCSS-MULTI-SUBRQST NE #DOCUMENT-KEY.#CASE-WORKPRCSS-MULTI-SUBRQST
                || cwf_Efm_Document_Case_Workprcss_Multi_Subrqst.notEquals(pnd_Document_Key_Pnd_Case_Workprcss_Multi_Subrqst)))
            {
                //*     WRITE /'Document Type Not found for PIN = ' CWF-MASTER.PIN-NBR
                //*           /'Document-Key = ' #DOCUMENT-KEY
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*  GRY 9/25/01
            if (condition(cwf_Efm_Document_Document_Direction_Ind.equals("O") && cwf_Efm_Document_Document_Category.equals("L")))                                         //Natural: IF CWF-EFM-DOCUMENT.DOCUMENT-DIRECTION-IND = 'O' AND CWF-EFM-DOCUMENT.DOCUMENT-CATEGORY = 'L'
            {
                pnd_Doc_Type_Combo_Pnd_Doc_Category.setValue(cwf_Efm_Document_Document_Category);                                                                         //Natural: MOVE DOCUMENT-CATEGORY TO #DOC-TYPE-COMBO.#DOC-CATEGORY
                pnd_Doc_Type_Combo_Pnd_Doc_Sub_Category.setValue(cwf_Efm_Document_Document_Sub_Category);                                                                 //Natural: MOVE DOCUMENT-SUB-CATEGORY TO #DOC-TYPE-COMBO.#DOC-SUB-CATEGORY
                pnd_Doc_Type_Combo_Pnd_Doc_Detail.setValue(cwf_Efm_Document_Document_Detail);                                                                             //Natural: MOVE DOCUMENT-DETAIL TO #DOC-TYPE-COMBO.#DOC-DETAIL
                ldaCwfl4832.getWork_Lead_Document_Type().setValue(pnd_Doc_Type_Combo);                                                                                    //Natural: MOVE #DOC-TYPE-COMBO TO WORK-LEAD.DOCUMENT-TYPE
                if (true) break READ_EFM;                                                                                                                                 //Natural: ESCAPE BOTTOM ( READ-EFM. )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-IF
            //*  READ EFM-DOCUMENT
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  GET-DOCUMENT-TYPE
    }
    private void sub_Write_Customer_Record() throws Exception                                                                                                             //Natural: WRITE-CUSTOMER-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************
        ldaCwfl4833.getCustomer_B1().setValue(pnd_Atsign);                                                                                                                //Natural: MOVE #ATSIGN TO B1 B2 B3 B4 B5 B6
        ldaCwfl4833.getCustomer_B2().setValue(pnd_Atsign);
        ldaCwfl4833.getCustomer_B3().setValue(pnd_Atsign);
        ldaCwfl4833.getCustomer_B4().setValue(pnd_Atsign);
        ldaCwfl4833.getCustomer_B5().setValue(pnd_Atsign);
        ldaCwfl4833.getCustomer_B6().setValue(pnd_Atsign);
        //*  MOVE CWF-MASTER.PIN-NBR TO CUSTOMER.PIN-NPIN #PIN-SPLIT   /* PIN-EXP
        //*  PIN-EXP
        ldaCwfl4833.getCustomer_Pin_Npin().moveAll(ldaCwfl4817.getCwf_Master_Pin_Nbr());                                                                                  //Natural: MOVE ALL CWF-MASTER.PIN-NBR TO CUSTOMER.PIN-NPIN
        //*  PIN-EXP
        pnd_Pin_Split.moveAll(ldaCwfl4817.getCwf_Master_Pin_Nbr());                                                                                                       //Natural: MOVE ALL CWF-MASTER.PIN-NBR TO #PIN-SPLIT
        if (condition(DbsUtil.maskMatches(pnd_Pin_Split_Pnd_Pin_Split_1,"N")))                                                                                            //Natural: IF #PIN-SPLIT-1 = MASK ( N )
        {
            ldaCwfl4833.getCustomer_Pin_Type().setValue("P");                                                                                                             //Natural: MOVE 'P' TO CUSTOMER.PIN-TYPE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCwfl4833.getCustomer_Pin_Type().setValue("N");                                                                                                             //Natural: MOVE 'N' TO CUSTOMER.PIN-TYPE
        }                                                                                                                                                                 //Natural: END-IF
        ldaCwfl4833.getCustomer_Last_Chnge_Dte_Tme_A().setValue(ldaCwfl4817.getCwf_Master_Last_Chnge_Dte_Tme());                                                          //Natural: MOVE CWF-MASTER.LAST-CHNGE-DTE-TME TO CUSTOMER.LAST-CHNGE-DTE-TME-A
        //*  ------------
        //*  DECIDE FOR FIRST CONDITION
        //*   WHEN #ADD
        //*     WRITE WORK FILE 3 CUSTOMER
        pnd_Cust_Ad_Cntr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CUST-AD-CNTR
        //*   WHEN #UPDATE
        //*     WRITE WORK FILE 4 CUSTOMER
        //*     ADD 1 TO #CUST-UP-CNTR
        //*   WHEN NONE IGNORE
        //*  END-DECIDE
        //*  ------------
        //*   JB1
        ldaCwfl4833.getCustomer_State_Of_Res().setValue("XX");                                                                                                            //Natural: MOVE 'XX' TO CUSTOMER.STATE-OF-RES
        //*  WRITE WORK FILE 3 CUSTOMER
        ldaCwfl4897.getCustomer_New().reset();                                                                                                                            //Natural: RESET CUSTOMER-NEW
        if (condition(DbsUtil.maskMatches(ldaCwfl4833.getCustomer_Pin_Npin(),"N...........")))                                                                            //Natural: IF CUSTOMER.PIN-NPIN EQ MASK ( N........... )
        {
            ldaCwfl4897.getCustomer_New_Pin_Npin().setValue(ldaCwfl4833.getCustomer_Pin_Npin().getSubstring(6,7));                                                        //Natural: MOVE SUBSTR ( CUSTOMER.PIN-NPIN,6,7 ) TO CUSTOMER-NEW.PIN-NPIN
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCwfl4897.getCustomer_New_Pin_Npin().setValue(ldaCwfl4833.getCustomer_Pin_Npin().getSubstring(1,7));                                                        //Natural: MOVE SUBSTR ( CUSTOMER.PIN-NPIN,1,7 ) TO CUSTOMER-NEW.PIN-NPIN
        }                                                                                                                                                                 //Natural: END-IF
        ldaCwfl4897.getCustomer_New_D1().setValue(ldaCwfl4833.getCustomer_B1());                                                                                          //Natural: MOVE CUSTOMER.B1 TO CUSTOMER-NEW.D1
        ldaCwfl4897.getCustomer_New_Ssn().setValue(ldaCwfl4833.getCustomer_Ssn());                                                                                        //Natural: MOVE CUSTOMER.SSN TO CUSTOMER-NEW.SSN
        ldaCwfl4897.getCustomer_New_D2().setValue(ldaCwfl4833.getCustomer_B2());                                                                                          //Natural: MOVE CUSTOMER.B2 TO CUSTOMER-NEW.D2
        ldaCwfl4897.getCustomer_New_Part_Name().setValue(ldaCwfl4833.getCustomer_Part_Name());                                                                            //Natural: MOVE CUSTOMER.PART-NAME TO CUSTOMER-NEW.PART-NAME
        ldaCwfl4897.getCustomer_New_D3().setValue(ldaCwfl4833.getCustomer_B3());                                                                                          //Natural: MOVE CUSTOMER.B3 TO CUSTOMER-NEW.D3
        ldaCwfl4897.getCustomer_New_State_Of_Res().setValue(ldaCwfl4833.getCustomer_State_Of_Res());                                                                      //Natural: MOVE CUSTOMER.STATE-OF-RES TO CUSTOMER-NEW.STATE-OF-RES
        ldaCwfl4897.getCustomer_New_D4().setValue(ldaCwfl4833.getCustomer_B4());                                                                                          //Natural: MOVE CUSTOMER.B4 TO CUSTOMER-NEW.D4
        ldaCwfl4897.getCustomer_New_Dte_Of_Birth().setValue(ldaCwfl4833.getCustomer_Dte_Of_Birth());                                                                      //Natural: MOVE CUSTOMER.DTE-OF-BIRTH TO CUSTOMER-NEW.DTE-OF-BIRTH
        ldaCwfl4897.getCustomer_New_D5().setValue(ldaCwfl4833.getCustomer_B5());                                                                                          //Natural: MOVE CUSTOMER.B5 TO CUSTOMER-NEW.D5
        ldaCwfl4897.getCustomer_New_Pin_Type().setValue(ldaCwfl4833.getCustomer_Pin_Type());                                                                              //Natural: MOVE CUSTOMER.PIN-TYPE TO CUSTOMER-NEW.PIN-TYPE
        ldaCwfl4897.getCustomer_New_D6().setValue(ldaCwfl4833.getCustomer_B6());                                                                                          //Natural: MOVE CUSTOMER.B6 TO CUSTOMER-NEW.D6
        ldaCwfl4897.getCustomer_New_Last_Chnge_Dte_Tme_A().setValue(ldaCwfl4833.getCustomer_Last_Chnge_Dte_Tme_A());                                                      //Natural: MOVE CUSTOMER.LAST-CHNGE-DTE-TME-A TO CUSTOMER-NEW.LAST-CHNGE-DTE-TME-A
        getWorkFiles().write(3, false, ldaCwfl4897.getCustomer_New());                                                                                                    //Natural: WRITE WORK FILE 3 CUSTOMER-NEW
        pnd_Customer_Cntr.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #CUSTOMER-CNTR
        //*  WRITE-CUSTOMER-RECORD
    }
    private void sub_Get_Name_Ssn() throws Exception                                                                                                                      //Natural: GET-NAME-SSN
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Pass_Pin.setValue(ldaCwfl4817.getCwf_Master_Pin_Nbr());                                                                                                       //Natural: ASSIGN #PASS-PIN := CWF-MASTER.PIN-NBR
        pnd_Pass_Found.setValue(false);                                                                                                                                   //Natural: MOVE FALSE TO #PASS-FOUND
        //*  TO CALCULATE PARTICIPANT's Age
        pdaNsta1245.getNsta1245_Pnd_Dob_Dte().reset();                                                                                                                    //Natural: RESET NSTA1245.#DOB-DTE
        DbsUtil.callnat(Cwfn5372.class , getCurrentProcessState(), pnd_Pass_Pin, pnd_Pass_Name, pnd_Pass_Ssn, pnd_Pass_Tlc, pnd_Pass_Dob, pnd_Pass_Found);                //Natural: CALLNAT 'CWFN5372' #PASS-PIN #PASS-NAME #PASS-SSN #PASS-TLC #PASS-DOB #PASS-FOUND
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Pass_Found.getBoolean()))                                                                                                                       //Natural: IF #PASS-FOUND
        {
            ldaCwfl4833.getCustomer_Part_Name().setValue(pnd_Pass_Name);                                                                                                  //Natural: ASSIGN CUSTOMER.PART-NAME := #PASS-NAME
            ldaCwfl4833.getCustomer_Ssn().setValue(pnd_Pass_Ssn);                                                                                                         //Natural: ASSIGN CUSTOMER.SSN := #PASS-SSN
            short decideConditionsMet2005 = 0;                                                                                                                            //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #PASS-DOB NE 0
            if (condition(pnd_Pass_Dob.notEquals(getZero())))
            {
                decideConditionsMet2005++;
                //*  AGE
                pdaNsta1245.getNsta1245_Pnd_Dob_Dte().setValue(pnd_Pass_Dob_Pnd_Pass_Dob_A);                                                                              //Natural: MOVE #PASS-DOB-A TO NSTA1245.#DOB-DTE
                pnd_Dob.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Pass_Dob_Pnd_Pass_Dob_A);                                                                       //Natural: MOVE EDITED #PASS-DOB-A TO #DOB ( EM = YYYYMMDD )
                pnd_Fix_Dte_10.setValueEdited(pnd_Dob,new ReportEditMask("MM/DD/YYYY"));                                                                                  //Natural: MOVE EDITED #DOB ( EM = MM/DD/YYYY ) TO #FIX-DTE-10
                if (condition(pnd_Fix_Dte_10_Pnd_Fix_Dte_Yyyy.less(1800)))                                                                                                //Natural: IF #FIX-DTE-YYYY < 1800
                {
                    getReports().write(0, ReportOption.NOTITLE,NEWLINE,"Bad Date of Birth for PIN:",ldaCwfl4817.getCwf_Master_Pin_Nbr(),"RQST Log",ldaCwfl4817.getCwf_Master_Rqst_Log_Dte_Tme()); //Natural: WRITE /'Bad Date of Birth for PIN:' CWF-MASTER.PIN-NBR 'RQST Log' CWF-MASTER.RQST-LOG-DTE-TME
                    if (Global.isEscape()) return;
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaCwfl4833.getCustomer_Dte_Of_Birth().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "'", pnd_Fix_Dte_10, "'"));                           //Natural: COMPRESS '"' #FIX-DTE-10 '"' INTO CUSTOMER.DTE-OF-BIRTH LEAVING NO SPACE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet2005 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, ReportOption.NOTITLE,NEWLINE,"No Record On COR file for PIN:",ldaCwfl4817.getCwf_Master_Pin_Nbr(),"RQST Log",ldaCwfl4817.getCwf_Master_Rqst_Log_Dte_Tme()); //Natural: WRITE / 'No Record On COR file for PIN:' CWF-MASTER.PIN-NBR 'RQST Log' CWF-MASTER.RQST-LOG-DTE-TME
            if (Global.isEscape()) return;
            pnd_Pass_Nfnd_Cntr.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #PASS-NFND-CNTR
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //* *************
        //*  GET-NAME
    }
    private void sub_Get_Part_Age() throws Exception                                                                                                                      //Natural: GET-PART-AGE
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        pdaNsta1245.getNsta1245_Pnd_Years().reset();                                                                                                                      //Natural: RESET NSTA1245.#YEARS
        if (condition(pdaNsta1245.getNsta1245_Pnd_Eff_Dte().greater(" ") && pdaNsta1245.getNsta1245_Pnd_Dob_Dte().greater(" ")))                                          //Natural: IF NSTA1245.#EFF-DTE GT ' ' AND NSTA1245.#DOB-DTE GT ' '
        {
            DbsUtil.callnat(Nstn1245.class , getCurrentProcessState(), pdaNsta1245.getNsta1245(), pdaCwfpda_M.getMsg_Info_Sub());                                         //Natural: CALLNAT 'NSTN1245' NSTA1245 MSG-INFO-SUB
            if (condition(Global.isEscape())) return;
            ldaCwfl4832.getWork_Lead_Part_Age().setValue(pdaNsta1245.getNsta1245_Pnd_Years());                                                                            //Natural: ASSIGN WORK-LEAD.PART-AGE := NSTA1245.#YEARS
        }                                                                                                                                                                 //Natural: END-IF
        //* GET-PART-AGE
    }
    private void sub_Write_Totals() throws Exception                                                                                                                      //Natural: WRITE-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        getReports().write(1, NEWLINE,"Program:",Global.getPROGRAM(),new TabSetting(45),"Run Date:",Global.getDATX(),NEWLINE,"******* MIT Extract Statistics for Corporate Reporting ********",NEWLINE,"Begining period    = ",pnd_Tbl_Start_Dte_Tme_T,  //Natural: WRITE ( 1 ) / 'Program:' *PROGRAM 45T 'Run Date:' *DATX / '******* MIT Extract Statistics for Corporate Reporting ********' / 'Begining period    = ' #TBL-START-DTE-TME-T ( EM = YYYYMMDDHHIISST ) / 'Ending   period    = ' #TBL-END-DTE-TME-T ( EM = YYYYMMDDHHIISST ) / '***************************************************************' / 'Number of Work-File Records which has been read = ' #WORK-READ-CNTR / 'Number of MIT Records which has been read       = ' #MIT-READ-CNTR / '---------------- Work Request Lead Statistic -------------' / 'Number of Work Request Lead Records             = ' #WR-LEAD-CNTR / '---------------- Customer Records Statistic  -------------' / 'Number of Customer Records Created              = ' #CUSTOMER-CNTR / '***************************************************' / 'Elapsed Time to Process Records ' '(HH:MM:SS.T) :' *TIMD ( ST. ) ( EM = 99:99:99'.'9 )
            new ReportEditMask ("YYYYMMDDHHIISST"),NEWLINE,"Ending   period    = ",pnd_Tbl_End_Dte_Tme_T, new ReportEditMask ("YYYYMMDDHHIISST"),NEWLINE,"***************************************************************",NEWLINE,"Number of Work-File Records which has been read = ",pnd_Work_Read_Cntr,NEWLINE,"Number of MIT Records which has been read       = ",pnd_Mit_Read_Cntr,NEWLINE,"---------------- Work Request Lead Statistic -------------",NEWLINE,"Number of Work Request Lead Records             = ",pnd_Wr_Lead_Cntr,NEWLINE,"---------------- Customer Records Statistic  -------------",NEWLINE,"Number of Customer Records Created              = ",pnd_Customer_Cntr,NEWLINE,"***************************************************",NEWLINE,"Elapsed Time to Process Records ","(HH:MM:SS.T) :",st, 
            new ReportEditMask ("99:99:99'.'9"));
        if (Global.isEscape()) return;
    }
    private void sub_Update_Control() throws Exception                                                                                                                    //Natural: UPDATE-CONTROL
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        //*  PULL CONTROL RECORD TO APPLY STATISTIC COUNTERS
        //*  -----------------------------------------------
        GET_CONTROL_REC:                                                                                                                                                  //Natural: GET CWF-SUPPORT-TBL CWFA4815-OUTPUT.RUN-CONTROL-ISN
        ldaCwfl4815.getVw_cwf_Support_Tbl().readByID(pdaCwfa4815.getCwfa4815_Output_Run_Control_Isn().getLong(), "GET_CONTROL_REC");
        ldaCwfl4815.getCwf_Support_Tbl_Program_Name().setValue("CWFB4830");                                                                                               //Natural: ASSIGN CWF-SUPPORT-TBL.PROGRAM-NAME := 'CWFB4830'
        ldaCwfl4815.getCwf_Support_Tbl_Run_Status().setValue("C");                                                                                                        //Natural: ASSIGN CWF-SUPPORT-TBL.RUN-STATUS := 'C'
        ldaCwfl4815.getCwf_Support_Tbl_Run_Date().setValue(Global.getTIMX());                                                                                             //Natural: ASSIGN CWF-SUPPORT-TBL.RUN-DATE := *TIMX
        ldaCwfl4815.getVw_cwf_Support_Tbl().updateDBRow("GET_CONTROL_REC");                                                                                               //Natural: UPDATE ( GET-CONTROL-REC. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  (7870)
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,"RQST-LOG-DTE-TME",ldaCwfl4817.getCwf_Master_Rqst_Log_Dte_Tme(),NEWLINE,"PIN Number      ",                    //Natural: WRITE / 'RQST-LOG-DTE-TME' CWF-MASTER.RQST-LOG-DTE-TME / 'PIN Number      ' CWF-MASTER.PIN-NBR / 'ISN Number      ' #ISN / 'NATURAL ERROR' *ERROR-NR 'IN' *PROGRAM '....LINE' *ERROR-LINE / ' '
            ldaCwfl4817.getCwf_Master_Pin_Nbr(),NEWLINE,"ISN Number      ",pnd_Isn,NEWLINE,"NATURAL ERROR",Global.getERROR_NR(),"IN",Global.getPROGRAM(),
            "....LINE",Global.getERROR_LINE(),NEWLINE," ");
        DbsUtil.terminate(10);  if (true) return;                                                                                                                         //Natural: TERMINATE 10
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=56 LS=131");
        Global.format(1, "PS=56 LS=131");
    }
}
