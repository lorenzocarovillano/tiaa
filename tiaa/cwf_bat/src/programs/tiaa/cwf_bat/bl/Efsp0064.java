/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:11:19 PM
**        * FROM NATURAL PROGRAM : Efsp0064
************************************************************
**        * FILE NAME            : Efsp0064.java
**        * CLASS NAME           : Efsp0064
**        * INSTANCE NAME        : Efsp0064
************************************************************
************************************************************************
* PROGRAM  : EFSP0064
* SYSTEM   : CRPCWF
* TITLE    : PRINT IMAGE UPLOAD - UNIDENTIFIED MINS REPORT
* GENERATED: OCT 07,93 AT 10:00 AM
* FUNCTION : THIS PROGRAM READS THE CWF-UPLOAD-AUDIT FILE AND PRINTS
*            THE AUDIT REPORT SORTED BY BATCH NUMBER, UPLOAD DATE/TIME
* HISTORY
* 04/28/95  BE  ADDED TOTAL NO. OF MINS WITHOUT A PIN IN THE REPORT.
* 05/13/96  BE  WRITE 'Nothing to Report' IF THERE ARE NO RECS TO RPT.
* 03/31/97  CS  FORCE LANDSCAPE PRINT MODE IF SUBMITTED ONLINE
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsp0064 extends BLNatBase
{
    // Data Areas
    private GdaCwfg000 gdaCwfg000;
    private LdaEfsl0040 ldaEfsl0040;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_Lines_Reqd;
    private DbsField pnd_Program;
    private DbsField pnd_Tot_Mins;
    private DbsField pnd_Source_Id;
    private DbsField pnd_Unidentified_Line;

    private DbsGroup pnd_Unidentified_Line__R_Field_1;

    private DbsGroup pnd_Unidentified_Line_Pnd_Min_Array;
    private DbsField pnd_Unidentified_Line_Pnd_Print_Min;
    private DbsField pnd_Unidentified_Line_Pnd_Filler;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_2;
    private DbsField cwf_Support_Tbl_Pnd_Regular_Run_Parms;

    private DbsGroup cwf_Support_Tbl__R_Field_3;
    private DbsField cwf_Support_Tbl_Pnd_Reg_Start_End_Time;
    private DbsField cwf_Support_Tbl_Pnd_Reg_Run_Date;
    private DbsField cwf_Support_Tbl_Pnd_Override_Time_Parms;
    private DbsField cwf_Support_Tbl_Pnd_Override_Flag;
    private DbsField cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Dte;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_4;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind;
    private DbsField pnd_Input_Source_Id;
    private DbsField pnd_Input_Data;

    private DbsGroup pnd_Input_Data__R_Field_5;
    private DbsField pnd_Input_Data_Pnd_Input_Time_Start;
    private DbsField pnd_Input_Data_Pnd_Input_Time_End;
    private DbsField pnd_Input_Data_Pnd_Input_Date;
    private DbsField pnd_Input_Date_Time;
    private DbsField pnd_Input_Date_D;
    private DbsField pnd_Start_Key;
    private DbsField pnd_End_Key;
    private DbsField pnd_Upld_Time;

    private DbsGroup pnd_Upld_Time__R_Field_6;
    private DbsField pnd_Upld_Time_Pnd_Upld_Timen;
    private DbsField pnd_Isn_Tbl;
    private DbsField pnd_Start_Date;
    private DbsField pnd_Next_Processing_Date;
    private DbsField pnd_Orig_Mm;
    private DbsField pnd_New_Mm;
    private DbsField pnd_Nothing_To_Rpt;
    private DbsField pnd_Record;

    private DbsRecord internalLoopRecord;
    private DbsField sort01Source_IdOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaCwfg000 = GdaCwfg000.getInstance(getCallnatLevel());
        registerRecord(gdaCwfg000);
        if (gdaOnly) return;

        ldaEfsl0040 = new LdaEfsl0040();
        registerRecord(ldaEfsl0040);
        registerRecord(ldaEfsl0040.getVw_cwf_Upload_Audit());

        // Local Variables
        localVariables = new DbsRecord();
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.PACKED_DECIMAL, 3);
        pnd_Lines_Reqd = localVariables.newFieldInRecord("pnd_Lines_Reqd", "#LINES-REQD", FieldType.PACKED_DECIMAL, 3);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Tot_Mins = localVariables.newFieldInRecord("pnd_Tot_Mins", "#TOT-MINS", FieldType.NUMERIC, 6);
        pnd_Source_Id = localVariables.newFieldInRecord("pnd_Source_Id", "#SOURCE-ID", FieldType.STRING, 6);
        pnd_Unidentified_Line = localVariables.newFieldInRecord("pnd_Unidentified_Line", "#UNIDENTIFIED-LINE", FieldType.STRING, 66);

        pnd_Unidentified_Line__R_Field_1 = localVariables.newGroupInRecord("pnd_Unidentified_Line__R_Field_1", "REDEFINE", pnd_Unidentified_Line);

        pnd_Unidentified_Line_Pnd_Min_Array = pnd_Unidentified_Line__R_Field_1.newGroupArrayInGroup("pnd_Unidentified_Line_Pnd_Min_Array", "#MIN-ARRAY", 
            new DbsArrayController(1, 4));
        pnd_Unidentified_Line_Pnd_Print_Min = pnd_Unidentified_Line_Pnd_Min_Array.newFieldInGroup("pnd_Unidentified_Line_Pnd_Print_Min", "#PRINT-MIN", 
            FieldType.NUMERIC, 11);
        pnd_Unidentified_Line_Pnd_Filler = pnd_Unidentified_Line_Pnd_Min_Array.newFieldInGroup("pnd_Unidentified_Line_Pnd_Filler", "#FILLER", FieldType.STRING, 
            3);

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Support_Tbl__R_Field_2 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_2", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Pnd_Regular_Run_Parms = cwf_Support_Tbl__R_Field_2.newFieldInGroup("cwf_Support_Tbl_Pnd_Regular_Run_Parms", "#REGULAR-RUN-PARMS", 
            FieldType.STRING, 20);

        cwf_Support_Tbl__R_Field_3 = cwf_Support_Tbl__R_Field_2.newGroupInGroup("cwf_Support_Tbl__R_Field_3", "REDEFINE", cwf_Support_Tbl_Pnd_Regular_Run_Parms);
        cwf_Support_Tbl_Pnd_Reg_Start_End_Time = cwf_Support_Tbl__R_Field_3.newFieldInGroup("cwf_Support_Tbl_Pnd_Reg_Start_End_Time", "#REG-START-END-TIME", 
            FieldType.STRING, 12);
        cwf_Support_Tbl_Pnd_Reg_Run_Date = cwf_Support_Tbl__R_Field_3.newFieldInGroup("cwf_Support_Tbl_Pnd_Reg_Run_Date", "#REG-RUN-DATE", FieldType.STRING, 
            8);
        cwf_Support_Tbl_Pnd_Override_Time_Parms = cwf_Support_Tbl__R_Field_2.newFieldInGroup("cwf_Support_Tbl_Pnd_Override_Time_Parms", "#OVERRIDE-TIME-PARMS", 
            FieldType.STRING, 12);
        cwf_Support_Tbl_Pnd_Override_Flag = cwf_Support_Tbl__R_Field_2.newFieldInGroup("cwf_Support_Tbl_Pnd_Override_Flag", "#OVERRIDE-FLAG", FieldType.STRING, 
            1);
        cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde", "TBL-DLTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_DLTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        cwf_Support_Tbl_Tbl_Updte_Dte = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Dte", "TBL-UPDTE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE");
        cwf_Support_Tbl_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        registerRecord(vw_cwf_Support_Tbl);

        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_4", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_4.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_4.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_4.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind = pnd_Tbl_Prime_Key__R_Field_4.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", FieldType.STRING, 
            1);
        pnd_Input_Source_Id = localVariables.newFieldInRecord("pnd_Input_Source_Id", "#INPUT-SOURCE-ID", FieldType.STRING, 6);
        pnd_Input_Data = localVariables.newFieldInRecord("pnd_Input_Data", "#INPUT-DATA", FieldType.STRING, 60);

        pnd_Input_Data__R_Field_5 = localVariables.newGroupInRecord("pnd_Input_Data__R_Field_5", "REDEFINE", pnd_Input_Data);
        pnd_Input_Data_Pnd_Input_Time_Start = pnd_Input_Data__R_Field_5.newFieldInGroup("pnd_Input_Data_Pnd_Input_Time_Start", "#INPUT-TIME-START", FieldType.STRING, 
            6);
        pnd_Input_Data_Pnd_Input_Time_End = pnd_Input_Data__R_Field_5.newFieldInGroup("pnd_Input_Data_Pnd_Input_Time_End", "#INPUT-TIME-END", FieldType.STRING, 
            6);
        pnd_Input_Data_Pnd_Input_Date = pnd_Input_Data__R_Field_5.newFieldInGroup("pnd_Input_Data_Pnd_Input_Date", "#INPUT-DATE", FieldType.STRING, 8);
        pnd_Input_Date_Time = localVariables.newFieldInRecord("pnd_Input_Date_Time", "#INPUT-DATE-TIME", FieldType.STRING, 14);
        pnd_Input_Date_D = localVariables.newFieldInRecord("pnd_Input_Date_D", "#INPUT-DATE-D", FieldType.DATE);
        pnd_Start_Key = localVariables.newFieldInRecord("pnd_Start_Key", "#START-KEY", FieldType.TIME);
        pnd_End_Key = localVariables.newFieldInRecord("pnd_End_Key", "#END-KEY", FieldType.TIME);
        pnd_Upld_Time = localVariables.newFieldInRecord("pnd_Upld_Time", "#UPLD-TIME", FieldType.STRING, 4);

        pnd_Upld_Time__R_Field_6 = localVariables.newGroupInRecord("pnd_Upld_Time__R_Field_6", "REDEFINE", pnd_Upld_Time);
        pnd_Upld_Time_Pnd_Upld_Timen = pnd_Upld_Time__R_Field_6.newFieldInGroup("pnd_Upld_Time_Pnd_Upld_Timen", "#UPLD-TIMEN", FieldType.NUMERIC, 4);
        pnd_Isn_Tbl = localVariables.newFieldInRecord("pnd_Isn_Tbl", "#ISN-TBL", FieldType.PACKED_DECIMAL, 10);
        pnd_Start_Date = localVariables.newFieldInRecord("pnd_Start_Date", "#START-DATE", FieldType.DATE);
        pnd_Next_Processing_Date = localVariables.newFieldInRecord("pnd_Next_Processing_Date", "#NEXT-PROCESSING-DATE", FieldType.DATE);
        pnd_Orig_Mm = localVariables.newFieldInRecord("pnd_Orig_Mm", "#ORIG-MM", FieldType.STRING, 2);
        pnd_New_Mm = localVariables.newFieldInRecord("pnd_New_Mm", "#NEW-MM", FieldType.STRING, 2);
        pnd_Nothing_To_Rpt = localVariables.newFieldInRecord("pnd_Nothing_To_Rpt", "#NOTHING-TO-RPT", FieldType.BOOLEAN, 1);
        pnd_Record = localVariables.newFieldInRecord("pnd_Record", "#RECORD", FieldType.STRING, 54);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        sort01Source_IdOld = internalLoopRecord.newFieldInRecord("Sort01_Source_Id_OLD", "Source_Id_OLD", FieldType.STRING, 6);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Support_Tbl.reset();
        internalLoopRecord.reset();

        ldaEfsl0040.initializeValues();

        localVariables.reset();
        pnd_Nothing_To_Rpt.setInitialValue(true);
        pnd_Record.setInitialValue("�E�&l1o2a5.6c72p3e66F�&a2L�(9905X�&l0L");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Efsp0064() throws Exception
    {
        super("Efsp0064");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Efsp0064|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        while(true)
        {
            try
            {
                getReports().definePrinter(2, "AUDIT");                                                                                                                   //Natural: DEFINE PRINTER ( AUDIT = 1 )
                pnd_Program.setValue(Global.getPROGRAM());                                                                                                                //Natural: FORMAT ( AUDIT ) LS = 133 PS = 60 ZP = OFF;//Natural: ASSIGN #PROGRAM = *PROGRAM
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Source_Id,pnd_Input_Data);                                                                   //Natural: INPUT #INPUT-SOURCE-ID #INPUT-DATA
                pnd_Input_Date_Time.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Input_Data_Pnd_Input_Date, pnd_Input_Data_Pnd_Input_Time_Start));        //Natural: COMPRESS #INPUT-DATE #INPUT-TIME-START INTO #INPUT-DATE-TIME LEAVING NO SPACE
                pnd_Start_Key.setValueEdited(new ReportEditMask("YYYYMMDDHHIISS"),pnd_Input_Date_Time);                                                                   //Natural: MOVE EDITED #INPUT-DATE-TIME TO #START-KEY ( EM = YYYYMMDDHHIISS )
                pnd_Input_Date_Time.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Input_Data_Pnd_Input_Date, pnd_Input_Data_Pnd_Input_Time_End));          //Natural: COMPRESS #INPUT-DATE #INPUT-TIME-END INTO #INPUT-DATE-TIME LEAVING NO SPACE
                pnd_End_Key.setValueEdited(new ReportEditMask("YYYYMMDDHHIISS"),pnd_Input_Date_Time);                                                                     //Natural: MOVE EDITED #INPUT-DATE-TIME TO #END-KEY ( EM = YYYYMMDDHHIISS )
                //*  ADD 1 TO THE DAY COUNT
                pnd_End_Key.nadd(864000);                                                                                                                                 //Natural: ADD 864000 TO #END-KEY
                pnd_Input_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Data_Pnd_Input_Date);                                                            //Natural: MOVE EDITED #INPUT-DATE TO #INPUT-DATE-D ( EM = YYYYMMDD )
                //*  READ THE AUDIT RECORDS FOR A SELECTED DAY
                ldaEfsl0040.getVw_cwf_Upload_Audit().startDatabaseRead                                                                                                    //Natural: READ CWF-UPLOAD-AUDIT BY UPLD-DATE-TIME STARTING FROM #START-KEY ENDING AT #END-KEY
                (
                "READ_AUDIT",
                new Wc[] { new Wc("UPLD_DATE_TIME", ">=", pnd_Start_Key, "And", WcType.BY) ,
                new Wc("UPLD_DATE_TIME", "<=", pnd_End_Key, WcType.BY) },
                new Oc[] { new Oc("UPLD_DATE_TIME", "ASC") }
                );
                READ_AUDIT:
                while (condition(ldaEfsl0040.getVw_cwf_Upload_Audit().readNextRow("READ_AUDIT")))
                {
                    if (condition(ldaEfsl0040.getCwf_Upload_Audit_Source_Id().notEquals(pnd_Input_Source_Id)))                                                            //Natural: REJECT IF CWF-UPLOAD-AUDIT.SOURCE-ID NE #INPUT-SOURCE-ID
                    {
                        continue;
                    }
                    if (condition(ldaEfsl0040.getCwf_Upload_Audit_Count_Castunidentified_Min_Nbr().equals(getZero()) && ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(1).equals(" "))) //Natural: REJECT IF ( CWF-UPLOAD-AUDIT.C*UNIDENTIFIED-MIN-NBR = 0 ) AND ( CWF-UPLOAD-AUDIT.ERROR-MSG ( 1 ) = ' ' )
                    {
                        continue;
                    }
                    getSort().writeSortInData(ldaEfsl0040.getCwf_Upload_Audit_Source_Id(), ldaEfsl0040.getCwf_Upload_Audit_Batch_Nbr(), ldaEfsl0040.getCwf_Upload_Audit_Batch_Ext_Nbr(),  //Natural: END-ALL
                        ldaEfsl0040.getCwf_Upload_Audit_Upld_Date_Time(), ldaEfsl0040.getCwf_Upload_Audit_Batch_Label_Txt(), ldaEfsl0040.getCwf_Upload_Audit_Mins_Bch_Hdr_Cnt(), 
                        ldaEfsl0040.getCwf_Upload_Audit_Count_Castunidentified_Min_Nbr(), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(1), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(2), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(3), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(4), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(5), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(6), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(7), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(8), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(9), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(10), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(11), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(12), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(13), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(14), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(15), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(16), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(17), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(18), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(19), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(20), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(21), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(22), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(23), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(24), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(25), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(26), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(27), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(28), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(29), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(30), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(31), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(32), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(33), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(34), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(35), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(36), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(37), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(38), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(39), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(40), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(41), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(42), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(43), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(44), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(45), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(46), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(47), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(48), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(49), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(50), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(51), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(52), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(53), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(54), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(55), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(56), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(57), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(58), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(59), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(60), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(61), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(62), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(63), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(64), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(65), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(66), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(67), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(68), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(69), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(70), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(71), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(72), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(73), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(74), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(75), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(76), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(77), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(78), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(79), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(80), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(81), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(82), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(83), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(84), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(85), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(86), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(87), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(88), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(89), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(90), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(91), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(92), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(93), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(94), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(95), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(96), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(97), 
                        ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(98), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(99), 
                        ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(1), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(2), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(3), 
                        ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(4), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(5), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(6), 
                        ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(7), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(8), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(9), 
                        ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(10), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(11), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(12), 
                        ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(13), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(14), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(15), 
                        ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(16), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(17), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(18), 
                        ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(19), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(20), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(21), 
                        ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(22), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(23), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(24), 
                        ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(25), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(26), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(27), 
                        ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(28), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(29), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(30), 
                        ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(31), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(32), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(33), 
                        ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(34), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(35), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(36), 
                        ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(37), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(38), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(39), 
                        ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(40), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(41), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(42), 
                        ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(43), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(44), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(45), 
                        ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(46), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(47), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(48), 
                        ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(49), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(50), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(51), 
                        ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(52), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(53), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(54), 
                        ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(55), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(56), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(57), 
                        ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(58), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(59), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(60), 
                        ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(61), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(62), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(63), 
                        ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(64), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(65), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(66), 
                        ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(67), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(68), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(69), 
                        ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(70), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(71), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(72), 
                        ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(73), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(74), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(75), 
                        ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(76), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(77), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(78), 
                        ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(79), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(80), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(81), 
                        ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(82), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(83), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(84), 
                        ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(85), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(86), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(87), 
                        ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(88), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(89), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(90), 
                        ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(91), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(92), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(93), 
                        ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(94), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(95), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(96), 
                        ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(97), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(98), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(99), 
                        ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(100), ldaEfsl0040.getCwf_Upload_Audit_Count_Casterror_Msg());
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                getSort().sortData(ldaEfsl0040.getCwf_Upload_Audit_Source_Id(), ldaEfsl0040.getCwf_Upload_Audit_Batch_Nbr(), ldaEfsl0040.getCwf_Upload_Audit_Batch_Ext_Nbr(),  //Natural: SORT BY CWF-UPLOAD-AUDIT.SOURCE-ID CWF-UPLOAD-AUDIT.BATCH-NBR CWF-UPLOAD-AUDIT.BATCH-EXT-NBR CWF-UPLOAD-AUDIT.UPLD-DATE-TIME USING CWF-UPLOAD-AUDIT.BATCH-LABEL-TXT CWF-UPLOAD-AUDIT.MINS-BCH-HDR-CNT CWF-UPLOAD-AUDIT.C*UNIDENTIFIED-MIN-NBR CWF-UPLOAD-AUDIT.UNIDENTIFIED-MIN-NBR ( * ) CWF-UPLOAD-AUDIT.ERROR-MSG ( * ) CWF-UPLOAD-AUDIT.C*ERROR-MSG
                    ldaEfsl0040.getCwf_Upload_Audit_Upld_Date_Time());
                boolean endOfDataSort01 = true;
                boolean firstSort01 = true;
                SORT01:
                while (condition(getSort().readSortOutData(ldaEfsl0040.getCwf_Upload_Audit_Source_Id(), ldaEfsl0040.getCwf_Upload_Audit_Batch_Nbr(), ldaEfsl0040.getCwf_Upload_Audit_Batch_Ext_Nbr(), 
                    ldaEfsl0040.getCwf_Upload_Audit_Upld_Date_Time(), ldaEfsl0040.getCwf_Upload_Audit_Batch_Label_Txt(), ldaEfsl0040.getCwf_Upload_Audit_Mins_Bch_Hdr_Cnt(), 
                    ldaEfsl0040.getCwf_Upload_Audit_Count_Castunidentified_Min_Nbr(), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(1), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(2), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(3), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(4), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(5), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(6), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(7), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(8), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(9), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(10), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(11), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(12), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(13), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(14), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(15), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(16), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(17), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(18), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(19), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(20), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(21), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(22), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(23), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(24), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(25), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(26), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(27), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(28), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(29), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(30), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(31), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(32), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(33), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(34), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(35), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(36), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(37), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(38), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(39), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(40), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(41), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(42), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(43), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(44), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(45), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(46), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(47), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(48), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(49), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(50), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(51), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(52), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(53), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(54), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(55), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(56), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(57), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(58), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(59), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(60), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(61), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(62), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(63), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(64), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(65), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(66), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(67), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(68), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(69), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(70), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(71), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(72), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(73), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(74), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(75), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(76), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(77), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(78), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(79), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(80), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(81), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(82), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(83), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(84), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(85), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(86), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(87), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(88), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(89), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(90), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(91), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(92), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(93), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(94), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(95), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(96), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(97), 
                    ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(98), ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(99), 
                    ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(1), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(2), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(3), 
                    ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(4), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(5), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(6), 
                    ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(7), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(8), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(9), 
                    ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(10), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(11), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(12), 
                    ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(13), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(14), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(15), 
                    ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(16), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(17), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(18), 
                    ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(19), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(20), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(21), 
                    ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(22), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(23), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(24), 
                    ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(25), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(26), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(27), 
                    ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(28), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(29), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(30), 
                    ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(31), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(32), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(33), 
                    ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(34), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(35), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(36), 
                    ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(37), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(38), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(39), 
                    ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(40), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(41), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(42), 
                    ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(43), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(44), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(45), 
                    ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(46), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(47), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(48), 
                    ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(49), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(50), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(51), 
                    ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(52), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(53), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(54), 
                    ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(55), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(56), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(57), 
                    ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(58), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(59), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(60), 
                    ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(61), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(62), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(63), 
                    ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(64), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(65), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(66), 
                    ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(67), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(68), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(69), 
                    ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(70), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(71), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(72), 
                    ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(73), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(74), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(75), 
                    ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(76), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(77), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(78), 
                    ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(79), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(80), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(81), 
                    ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(82), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(83), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(84), 
                    ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(85), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(86), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(87), 
                    ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(88), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(89), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(90), 
                    ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(91), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(92), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(93), 
                    ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(94), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(95), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(96), 
                    ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(97), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(98), ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(99), 
                    ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(100), ldaEfsl0040.getCwf_Upload_Audit_Count_Casterror_Msg())))
                {
                    if (condition(getSort().getAstCOUNTER().greater(0)))
                    {
                        atBreakEventSort01(false);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom()))
                                break;
                            else if (condition(Global.isEscapeBottomImmediate()))
                            {
                                endOfDataSort01 = false;
                                break;
                            }
                            else if (condition(Global.isEscapeTop()))
                            continue;
                            else if (condition())
                            return;
                        }
                    }
                    //*     CWF-UPLOAD-AUDIT.EXCEPTION-MSG
                    //*                                                                                                                                                   //Natural: AT BREAK OF CWF-UPLOAD-AUDIT.SOURCE-ID
                    pnd_Source_Id.setValue(ldaEfsl0040.getCwf_Upload_Audit_Source_Id());                                                                                  //Natural: ASSIGN #SOURCE-ID = CWF-UPLOAD-AUDIT.SOURCE-ID
                    pnd_Tot_Mins.nadd(ldaEfsl0040.getCwf_Upload_Audit_Count_Castunidentified_Min_Nbr());                                                                  //Natural: COMPUTE #TOT-MINS = #TOT-MINS + CWF-UPLOAD-AUDIT.C*UNIDENTIFIED-MIN-NBR
                    pnd_Lines_Reqd.compute(new ComputeParameters(true, pnd_Lines_Reqd), ldaEfsl0040.getCwf_Upload_Audit_Count_Castunidentified_Min_Nbr().divide(4));      //Natural: COMPUTE ROUNDED #LINES-REQD = CWF-UPLOAD-AUDIT.C*UNIDENTIFIED-MIN-NBR / 4
                    pnd_Lines_Reqd.nadd(1);                                                                                                                               //Natural: ADD 1 TO #LINES-REQD
                    if (condition(getReports().getAstLinesLeft(0).less(pnd_Lines_Reqd)))                                                                                  //Natural: NEWPAGE ( AUDIT ) IF LESS THAN #LINES-REQD LINES LEFT
                    {
                        getReports().newPage(0);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }
                    pnd_Unidentified_Line.reset();                                                                                                                        //Natural: RESET #UNIDENTIFIED-LINE
                    FOR01:                                                                                                                                                //Natural: FOR #I = 1 TO 4
                    for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(4)); pnd_I.nadd(1))
                    {
                        if (condition(pnd_I.lessOrEqual(ldaEfsl0040.getCwf_Upload_Audit_Count_Castunidentified_Min_Nbr())))                                               //Natural: IF #I LE CWF-UPLOAD-AUDIT.C*UNIDENTIFIED-MIN-NBR
                        {
                            pnd_Unidentified_Line_Pnd_Print_Min.getValue(pnd_I).setValue(ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(pnd_I));         //Natural: ASSIGN #PRINT-MIN ( #I ) = CWF-UPLOAD-AUDIT.UNIDENTIFIED-MIN-NBR ( #I )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  ERROR RECORD
                    if (condition(ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue("*").notEquals(" ")))                                                              //Natural: IF CWF-UPLOAD-AUDIT.ERROR-MSG ( * ) NE ' '
                    {
                        FOR02:                                                                                                                                            //Natural: FOR #I = 1 TO C*CWF-UPLOAD-AUDIT.ERROR-MSG
                        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(ldaEfsl0040.getCwf_Upload_Audit_Count_Casterror_Msg())); pnd_I.nadd(1))
                        {
                            if (condition(getReports().getAstLinesLeft(0).less(3)))                                                                                       //Natural: NEWPAGE ( AUDIT ) IF LESS THAN 3 LINES LEFT
                            {
                                getReports().newPage(0);
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                            }
                            if (condition(pnd_I.equals(1)))                                                                                                               //Natural: IF #I = 1
                            {
                                getReports().write(2, ReportOption.NOTITLE,"*",new RepeatItem(131));                                                                      //Natural: WRITE ( AUDIT ) NOTITLE '*' ( 131 )
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                            }                                                                                                                                             //Natural: END-IF
                            pnd_Unidentified_Line.setValue(ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(pnd_I));                                                  //Natural: ASSIGN #UNIDENTIFIED-LINE = CWF-UPLOAD-AUDIT.ERROR-MSG ( #I )
                            getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Efsf006a.class));                                                          //Natural: WRITE ( AUDIT ) NOTITLE USING FORM 'EFSF006A'
                        }                                                                                                                                                 //Natural: END-FOR
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(2, ReportOption.NOTITLE,"*",new RepeatItem(131));                                                                              //Natural: WRITE ( AUDIT ) NOTITLE '*' ( 131 )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Efsf006a.class));                                                              //Natural: WRITE ( AUDIT ) NOTITLE USING FORM 'EFSF006A'
                    }                                                                                                                                                     //Natural: END-IF
                    //*  WRITE 1 LINE FOR 4 UNIDENTIFIED MINS
                    if (condition(ldaEfsl0040.getCwf_Upload_Audit_Count_Castunidentified_Min_Nbr().greater(4)))                                                           //Natural: IF CWF-UPLOAD-AUDIT.C*UNIDENTIFIED-MIN-NBR > 4
                    {
                        FOR03:                                                                                                                                            //Natural: FOR #I = 5 TO CWF-UPLOAD-AUDIT.C*UNIDENTIFIED-MIN-NBR STEP 4
                        for (pnd_I.setValue(5); condition(pnd_I.lessOrEqual(ldaEfsl0040.getCwf_Upload_Audit_Count_Castunidentified_Min_Nbr())); pnd_I.nadd(1))
                        {
                            pnd_Unidentified_Line.reset();                                                                                                                //Natural: RESET #UNIDENTIFIED-LINE
                            FOR04:                                                                                                                                        //Natural: FOR #J = 1 TO 4
                            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(4)); pnd_J.nadd(1))
                            {
                                pnd_K.compute(new ComputeParameters(false, pnd_K), pnd_I.add(pnd_J).subtract(1));                                                         //Natural: COMPUTE #K = #I + #J - 1
                                if (condition(pnd_K.lessOrEqual(ldaEfsl0040.getCwf_Upload_Audit_Count_Castunidentified_Min_Nbr())))                                       //Natural: IF #K LE CWF-UPLOAD-AUDIT.C*UNIDENTIFIED-MIN-NBR
                                {
                                    pnd_Unidentified_Line_Pnd_Print_Min.getValue(pnd_J).setValue(ldaEfsl0040.getCwf_Upload_Audit_Unidentified_Min_Nbr().getValue(pnd_K)); //Natural: ASSIGN #PRINT-MIN ( #J ) = CWF-UPLOAD-AUDIT.UNIDENTIFIED-MIN-NBR ( #K )
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-FOR
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(2, ReportOption.NOTITLE,new TabSetting(66),pnd_Unidentified_Line);                                                         //Natural: WRITE ( AUDIT ) NOTITLE 66T #UNIDENTIFIED-LINE
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-FOR
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().skip(0, 1);                                                                                                                              //Natural: SKIP ( AUDIT ) 1 LINES
                    pnd_Nothing_To_Rpt.setValue(false);                                                                                                                   //Natural: ASSIGN #NOTHING-TO-RPT := FALSE
                    sort01Source_IdOld.setValue(ldaEfsl0040.getCwf_Upload_Audit_Source_Id());                                                                             //Natural: END-SORT
                }
                if (condition(getSort().getAstCOUNTER().greater(0)))
                {
                    atBreakEventSort01(endOfDataSort01);
                }
                endSort();
                if (condition(pnd_Nothing_To_Rpt.getBoolean()))                                                                                                           //Natural: IF #NOTHING-TO-RPT
                {
                    getReports().write(2, ReportOption.NOTITLE,"Nothing to report.");                                                                                     //Natural: WRITE ( AUDIT ) NOTITLE 'Nothing to report.'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( AUDIT )
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //*  BEING SUBMITTED VIA CTP8000
                    //*   SET LANDSCAPE
                    if (condition(! (DbsUtil.maskMatches(Global.getINIT_PROGRAM(),".....'CWD'"))))                                                                        //Natural: IF *INIT-PROGRAM NE MASK ( .....'CWD' )
                    {
                        getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,pnd_Record);                                                                        //Natural: WRITE ( AUDIT ) NOTITLE NOHDR #RECORD
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(2, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Efsf0069.class));                                              //Natural: WRITE ( AUDIT ) NOTITLE NOHDR USING FORM 'EFSF0069'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean ldaEfsl0040_getCwf_Upload_Audit_Source_IdIsBreak = ldaEfsl0040.getCwf_Upload_Audit_Source_Id().isBreak(endOfData);
        if (condition(ldaEfsl0040_getCwf_Upload_Audit_Source_IdIsBreak))
        {
            pnd_Source_Id.setValue(sort01Source_IdOld);                                                                                                                   //Natural: ASSIGN #SOURCE-ID = OLD ( CWF-UPLOAD-AUDIT.SOURCE-ID )
            if (condition(pnd_Tot_Mins.greater(getZero())))                                                                                                               //Natural: IF #TOT-MINS GT 0
            {
                if (condition(getReports().getAstLinesLeft(0).less(3)))                                                                                                   //Natural: NEWPAGE ( AUDIT ) IF LESS THAN 3 LINES LEFT
                {
                    getReports().newPage(0);
                    if (condition(Global.isEscape())){return;}
                }
                getReports().skip(0, 2);                                                                                                                                  //Natural: SKIP ( AUDIT ) 2 LINES
                getReports().write(2, new TabSetting(5),"TOTAL MIN'S UPLOADED WITH NO PIN :",new TabSetting(40),pnd_Tot_Mins);                                            //Natural: WRITE ( AUDIT ) 05T 'TOTAL MIN"S UPLOADED WITH NO PIN :' 40T #TOT-MINS
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            pnd_Tot_Mins.reset();                                                                                                                                         //Natural: RESET #TOT-MINS
            getReports().getPageNumberDbs(2).reset();                                                                                                                     //Natural: RESET *PAGE-NUMBER ( AUDIT )
            pnd_Source_Id.setValue(ldaEfsl0040.getCwf_Upload_Audit_Source_Id());                                                                                          //Natural: ASSIGN #SOURCE-ID = CWF-UPLOAD-AUDIT.SOURCE-ID
            getReports().newPage(new ReportSpecification(0));                                                                                                             //Natural: NEWPAGE ( AUDIT )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(2, "LS=133 PS=60 ZP=OFF");
    }
}
