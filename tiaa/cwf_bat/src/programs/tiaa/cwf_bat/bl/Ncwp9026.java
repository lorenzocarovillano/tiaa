/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:03:43 PM
**        * FROM NATURAL PROGRAM : Ncwp9026
************************************************************
**        * FILE NAME            : Ncwp9026.java
**        * CLASS NAME           : Ncwp9026
**        * INSTANCE NAME        : Ncwp9026
************************************************************
************************************************************************
* PROGRAM  : NCWP9026 (FORMERLY EFSP0050)
* SYSTEM   : PROJCWF
* TITLE    : DELETED MIN's report from NCW-EFM-AUDIT work file.
* GENERATED: 03/07/97
* FUNCTION : THIS PROGRAM READS A SORTED NCW-EFM-AUDIT WORK FILE
*            DAILY AND CREATES A REPORT FOR DELETED MIN's.
* HISTORY
* ----------
* 10/17/95 JHH - READ WORK FILE, NOT CWF-FOLDER-AUDIT
************************************************************************
* GLOBAL USING CWFG000

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Ncwp9026 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Program;
    private DbsField pnd_Source_Id;
    private DbsField pnd_Batch_Count;

    private DbsGroup ncw_Efm_Audit;
    private DbsField ncw_Efm_Audit_Image_Source_Id;
    private DbsField ncw_Efm_Audit_Image_Min;
    private DbsField ncw_Efm_Audit_Np_Pin;
    private DbsField ncw_Efm_Audit_Rqst_Id;
    private DbsField ncw_Efm_Audit_Empl_Oprtr_Cde;
    private DbsField ncw_Efm_Audit_Error_Msg;
    private DbsField ncw_Efm_Audit_Log_Dte_Tme;
    private DbsField ncw_Efm_Audit_Filler;
    private DbsField pnd_Rqst_Id;

    private DbsGroup pnd_Rqst_Id__R_Field_1;
    private DbsField pnd_Rqst_Id_Pnd_Wpid;
    private DbsField pnd_Rqst_Id_Pnd_Tiaa_Rcvd_Dte;
    private DbsField pnd_Rqst_Id_Pnd_Case_Id;
    private DbsField pnd_Rqst_Id_Pnd_Subrqst_Id;
    private DbsField pnd_Rqst_Id_Pnd_Np_Pin;

    private DbsRecord internalLoopRecord;
    private DbsField rEAD_AUDITImage_Source_IdOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Source_Id = localVariables.newFieldInRecord("pnd_Source_Id", "#SOURCE-ID", FieldType.STRING, 6);
        pnd_Batch_Count = localVariables.newFieldInRecord("pnd_Batch_Count", "#BATCH-COUNT", FieldType.NUMERIC, 5);

        ncw_Efm_Audit = localVariables.newGroupInRecord("ncw_Efm_Audit", "NCW-EFM-AUDIT");
        ncw_Efm_Audit_Image_Source_Id = ncw_Efm_Audit.newFieldInGroup("ncw_Efm_Audit_Image_Source_Id", "IMAGE-SOURCE-ID", FieldType.STRING, 6);
        ncw_Efm_Audit_Image_Min = ncw_Efm_Audit.newFieldInGroup("ncw_Efm_Audit_Image_Min", "IMAGE-MIN", FieldType.STRING, 11);
        ncw_Efm_Audit_Np_Pin = ncw_Efm_Audit.newFieldInGroup("ncw_Efm_Audit_Np_Pin", "NP-PIN", FieldType.STRING, 7);
        ncw_Efm_Audit_Rqst_Id = ncw_Efm_Audit.newFieldArrayInGroup("ncw_Efm_Audit_Rqst_Id", "RQST-ID", FieldType.STRING, 23, new DbsArrayController(1, 
            10));
        ncw_Efm_Audit_Empl_Oprtr_Cde = ncw_Efm_Audit.newFieldInGroup("ncw_Efm_Audit_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", FieldType.STRING, 8);
        ncw_Efm_Audit_Error_Msg = ncw_Efm_Audit.newFieldInGroup("ncw_Efm_Audit_Error_Msg", "ERROR-MSG", FieldType.STRING, 30);
        ncw_Efm_Audit_Log_Dte_Tme = ncw_Efm_Audit.newFieldInGroup("ncw_Efm_Audit_Log_Dte_Tme", "LOG-DTE-TME", FieldType.TIME);
        ncw_Efm_Audit_Filler = ncw_Efm_Audit.newFieldInGroup("ncw_Efm_Audit_Filler", "FILLER", FieldType.STRING, 1);
        pnd_Rqst_Id = localVariables.newFieldInRecord("pnd_Rqst_Id", "#RQST-ID", FieldType.STRING, 23);

        pnd_Rqst_Id__R_Field_1 = localVariables.newGroupInRecord("pnd_Rqst_Id__R_Field_1", "REDEFINE", pnd_Rqst_Id);
        pnd_Rqst_Id_Pnd_Wpid = pnd_Rqst_Id__R_Field_1.newFieldInGroup("pnd_Rqst_Id_Pnd_Wpid", "#WPID", FieldType.STRING, 6);
        pnd_Rqst_Id_Pnd_Tiaa_Rcvd_Dte = pnd_Rqst_Id__R_Field_1.newFieldInGroup("pnd_Rqst_Id_Pnd_Tiaa_Rcvd_Dte", "#TIAA-RCVD-DTE", FieldType.STRING, 8);
        pnd_Rqst_Id_Pnd_Case_Id = pnd_Rqst_Id__R_Field_1.newFieldInGroup("pnd_Rqst_Id_Pnd_Case_Id", "#CASE-ID", FieldType.STRING, 1);
        pnd_Rqst_Id_Pnd_Subrqst_Id = pnd_Rqst_Id__R_Field_1.newFieldInGroup("pnd_Rqst_Id_Pnd_Subrqst_Id", "#SUBRQST-ID", FieldType.STRING, 1);
        pnd_Rqst_Id_Pnd_Np_Pin = pnd_Rqst_Id__R_Field_1.newFieldInGroup("pnd_Rqst_Id_Pnd_Np_Pin", "#NP-PIN", FieldType.STRING, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        rEAD_AUDITImage_Source_IdOld = internalLoopRecord.newFieldInRecord("READ_AUDIT_Image_Source_Id_OLD", "Image_Source_Id_OLD", FieldType.STRING, 
            6);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Ncwp9026() throws Exception
    {
        super("Ncwp9026");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt4, 4);
        setupReports();
        //*  ------------------------------------------------------
        getReports().definePrinter(5, "AUDIT");                                                                                                                           //Natural: DEFINE PRINTER ( AUDIT = 4 )
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: FORMAT ( AUDIT ) LS = 133 PS = 60 ZP = OFF;//Natural: ASSIGN #PROGRAM = *PROGRAM
        //*  -------------------------------------
        boolean endOfDataReadAudit = true;                                                                                                                                //Natural: READ WORK 3 NCW-EFM-AUDIT
        boolean firstReadAudit = true;
        READ_AUDIT:
        while (condition(getWorkFiles().read(3, ncw_Efm_Audit)))
        {
            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventRead_Audit();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadAudit = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            if (condition(ncw_Efm_Audit_Image_Source_Id.equals("000000")))                                                                                                //Natural: REJECT IF NCW-EFM-AUDIT.IMAGE-SOURCE-ID = '000000'
            {
                continue;
            }
            //*                                                                                                                                                           //Natural: AT BREAK OF NCW-EFM-AUDIT.IMAGE-SOURCE-ID
            pnd_Source_Id.setValue(ncw_Efm_Audit_Image_Source_Id);                                                                                                        //Natural: ASSIGN #SOURCE-ID = NCW-EFM-AUDIT.IMAGE-SOURCE-ID
            pnd_Rqst_Id.setValue(ncw_Efm_Audit_Rqst_Id.getValue(1));                                                                                                      //Natural: ASSIGN #RQST-ID = NCW-EFM-AUDIT.RQST-ID ( 1 )
            getReports().write(5, ReportOption.NOTITLE, writeMapToStringOutput(Ncwf0051.class));                                                                          //Natural: WRITE ( AUDIT ) NOTITLE USING FORM 'NCWF0051'
            pnd_Batch_Count.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #BATCH-COUNT
            rEAD_AUDITImage_Source_IdOld.setValue(ncw_Efm_Audit_Image_Source_Id);                                                                                         //Natural: END-WORK
        }
        READ_AUDIT_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventRead_Audit(endOfDataReadAudit);
        }
        if (Global.isEscape()) return;
        //*  -------------------------------------
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( AUDIT )
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt4 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(5, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Ncwf0050.class));                                              //Natural: WRITE ( AUDIT ) NOTITLE NOHDR USING FORM 'NCWF0050'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventRead_Audit() throws Exception {atBreakEventRead_Audit(false);}
    private void atBreakEventRead_Audit(boolean endOfData) throws Exception
    {
        boolean ncw_Efm_Audit_Image_Source_IdIsBreak = ncw_Efm_Audit_Image_Source_Id.isBreak(endOfData);
        if (condition(ncw_Efm_Audit_Image_Source_IdIsBreak))
        {
            pnd_Source_Id.setValue(rEAD_AUDITImage_Source_IdOld);                                                                                                         //Natural: ASSIGN #SOURCE-ID = OLD ( NCW-EFM-AUDIT.IMAGE-SOURCE-ID )
            if (condition(getReports().getAstLinesLeft(0).less(5)))                                                                                                       //Natural: NEWPAGE ( AUDIT ) IF LESS THAN 5 LINES LEFT
            {
                getReports().newPage(0);
                if (condition(Global.isEscape())){return;}
            }
            getReports().write(5, NEWLINE,NEWLINE,new TabSetting(1),"TOTAL FOR IMAGE SOURCE ID",pnd_Source_Id," : ",pnd_Batch_Count);                                     //Natural: WRITE ( AUDIT ) // 1T 'TOTAL FOR IMAGE SOURCE ID' #SOURCE-ID ' : ' #BATCH-COUNT
            if (condition(Global.isEscape())) return;
            pnd_Batch_Count.reset();                                                                                                                                      //Natural: RESET #BATCH-COUNT
            getReports().getPageNumberDbs(5).reset();                                                                                                                     //Natural: RESET *PAGE-NUMBER ( AUDIT )
            pnd_Source_Id.setValue(ncw_Efm_Audit_Image_Source_Id);                                                                                                        //Natural: ASSIGN #SOURCE-ID = NCW-EFM-AUDIT.IMAGE-SOURCE-ID
            getReports().newPage(new ReportSpecification(0));                                                                                                             //Natural: NEWPAGE ( AUDIT )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(5, "LS=133 PS=60 ZP=OFF");
    }
}
