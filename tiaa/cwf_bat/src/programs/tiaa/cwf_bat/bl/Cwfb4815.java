/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:35:11 PM
**        * FROM NATURAL PROGRAM : Cwfb4815
************************************************************
**        * FILE NAME            : Cwfb4815.java
**        * CLASS NAME           : Cwfb4815
**        * INSTANCE NAME        : Cwfb4815
************************************************************
**********************************************************************
* PROGRAM  : CWFB4815
* SYSTEM   : CRPCWF
* TITLE    : CORPORATE REPORTING EXTRACT FROM MIT
* FUNCTION : THIS PROGRAM EXTRACT MIT RECORDS WITHIN "Window" PERIOD
*          : AND CREATE NUMEROUS FILES WHICH WILL CONTAIN WORK REQUESTS
*          : & ACTIVITIES FOR SQL SERVER FOR REPORTING PURPOSES.
*
* JVH 09/99 - CHANGE EDIT MASKS FOR DATE FIELDS WITH TENTH OF A SECOND
*            FROM  (EM='"'LLL' 'DD' 'YYYY' 'HH':'II':'SS'.'T'00'AP'"')
*             TO    (EM='"'LLL�DD�YYYY�HH':'II':'SSAP'"') FOR ORACLE
* JVH 09/99 - CHANGE EDIT MASKS FOR RQST-LOG-DTE-TME FIELDS
*            FROM  (EM='"'LLL' 'DD' 'YYYY' 'HH':'II':'SS'.'T'00'AP'"')
*            TO    (EM='"'LLL' 'DD' 'YYYY' 'HH':'II':'SS':'T'00'AP'"')
* L.E. 06/2000 IES FIELDS HAVE BEEN ADDED TO WORK REQUEST FILE.
* L.E. 06/2001 CREATION OF TLC TABLE HAVE BEEN ADDED TO PROGRAM BODY.
* J.B. 04/2015 NAD RETIREMENT - REMOVE NAS CODE TO GET STATE CODE
*                  BECAUSE IT POPULATES IT WITHSPACES ANYWAY
* V.R. 04/2017 COR/NAAD SUNSET
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
**********************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb4815 extends BLNatBase
{
    // Data Areas
    private PdaCwfa4815 pdaCwfa4815;
    private PdaNsta1245 pdaNsta1245;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private LdaCwfl4817 ldaCwfl4817;
    private LdaCwfl4818 ldaCwfl4818;
    private LdaCwfl4815 ldaCwfl4815;
    private LdaCwfl4819 ldaCwfl4819;
    private LdaCwfl4899 ldaCwfl4899;
    private LdaCwfl4820 ldaCwfl4820;
    private LdaCwfl4821 ldaCwfl4821;
    private LdaCwfl4822 ldaCwfl4822;
    private LdaCwfl4823 ldaCwfl4823;
    private LdaCwfl4824 ldaCwfl4824;
    private LdaCwfl4828 ldaCwfl4828;
    private LdaCwfl4829 ldaCwfl4829;
    private LdaCwfl4830 ldaCwfl4830;
    private PdaCwfa1400 pdaCwfa1400;
    private PdaCwfa1401 pdaCwfa1401;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Work_File_1;
    private DbsField pnd_Work_File_1_Pnd_Rqst_Log_Dte_Tme;

    private DataAccessProgramView vw_cwf_Master_View_Select;
    private DbsField cwf_Master_View_Select_Rqst_Log_Dte_Tme;
    private DbsField cwf_Master_View_Select_Rqst_Id;

    private DbsGroup cwf_Master_View_Select__R_Field_1;
    private DbsField cwf_Master_View_Select_Rqst_Id_Wpid;

    private DataAccessProgramView vw_cwf_Sb_Ph;
    private DbsField cwf_Sb_Ph_Rcrd_Type;
    private DbsField cwf_Sb_Ph_Srvvr_Mit_Idntfr;
    private DbsField cwf_Sb_Ph_Dod_Not_Dte;

    private DbsGroup cwf_Sb_Ph__R_Field_2;
    private DbsField cwf_Sb_Ph_Dod_Not_Dte_A;

    private DbsGroup pnd_Work_File_23;
    private DbsField pnd_Work_File_23_Pnd_Cor_Pin_Field;
    private DbsField pnd_Tbl_Start_Dte_Tme_T;
    private DbsField pnd_Tbl_End_Dte_Tme_T;
    private DbsField pnd_Fix_Time;
    private DbsField pnd_Fix_Dte_Tme;

    private DbsGroup pnd_Fix_Dte_Tme__R_Field_3;
    private DbsField pnd_Fix_Dte_Tme_Pnd_Dte;

    private DbsGroup pnd_Fix_Dte_Tme__R_Field_4;
    private DbsField pnd_Fix_Dte_Tme_Pnd_Dte_Yyyy;
    private DbsField pnd_Fix_Dte_Tme_Pnd_Mm;
    private DbsField pnd_Fix_Dte_Tme_Pnd_Dd;
    private DbsField pnd_Fix_Dte_Tme_Pnd_Tme;
    private DbsField pnd_Fix_Dte_10;

    private DbsGroup pnd_Fix_Dte_10__R_Field_5;
    private DbsField pnd_Fix_Dte_10_Pnd_Fix_Dte_6;
    private DbsField pnd_Fix_Dte_10_Pnd_Fix_Dte_Yyyy;
    private DbsField pnd_Fix_Dte;
    private DbsField pnd_Effctve_Dte;
    private DbsField pnd_Trans_Dte;
    private DbsField pnd_Trnsctn_Dte;
    private DbsField pnd_Sec_Updte_Dte;
    private DbsField pnd_Date_Split;

    private DbsGroup pnd_Date_Split__R_Field_6;
    private DbsField pnd_Date_Split_Pnd_Date_Split_Dte;
    private DbsField pnd_Date_Split_Pnd_Date_Split_Tme;
    private DbsField pnd_Dob;
    private DbsField pnd_Dod_D;
    private DbsField pnd_Time;
    private DbsField pnd_Rqst_Log_Dte_Tme_Save;
    private DbsField pnd_Pin_Number_Save;
    private DbsField pnd_End_Dte_Tme;

    private DbsGroup pnd_End_Dte_Tme__R_Field_7;
    private DbsField pnd_End_Dte_Tme_Pnd_End_Yyyymmdd;
    private DbsField pnd_End_Dte_Tme_Pnd_End_Hhiisst;

    private DbsGroup pnd_End_Dte_Tme__R_Field_8;
    private DbsField pnd_End_Dte_Tme_Pnd_End_Dte_Tme_N;
    private DbsField pnd_Conversion_Due_Dte_Chg_Prty_Cde;

    private DbsGroup pnd_Conversion_Due_Dte_Chg_Prty_Cde__R_Field_9;
    private DbsField pnd_Conversion_Due_Dte_Chg_Prty_Cde_Pnd_Conversion_Fill_1;
    private DbsField pnd_Conversion_Due_Dte_Chg_Prty_Cde_Pnd_Recreate_Ind;
    private DbsField pnd_Conversion_Due_Dte_Chg_Prty_Cde_Pnd_Conversion_Fill_2;
    private DbsField pnd_Conversion_Due_Dte_Chg_Prty_Cde_Pnd_Prcssng_Type_Cde;
    private DbsField pnd_Curr_Dte_Tme;
    private DbsField pnd_Prev_Dte_Tme;

    private DbsGroup pnd_Prev_Dte_Tme__R_Field_10;
    private DbsField pnd_Prev_Dte_Tme_Pnd_Prev_Yyyymmdd;
    private DbsField pnd_Prev_Dte_Tme_Pnd_Prev_Hhiisst;
    private DbsField pnd_Service_Days;
    private DbsField pnd_Service_Hrs;
    private DbsField pnd_Service_Mins;
    private DbsField pnd_Elpsd_Time_Compose;

    private DbsGroup pnd_Elpsd_Time_Compose__R_Field_11;
    private DbsField pnd_Elpsd_Time_Compose_Pnd_Elpsd_Date_Portion;
    private DbsField pnd_Elpsd_Time_Compose_Pnd_Elpsd_Time_Portion;
    private DbsField pnd_Elpsd_Days;
    private DbsField pnd_Date_T;
    private DbsField pnd_Date_D;
    private DbsField pnd_Hold_Tme_A;
    private DbsField pnd_Start_Date_T;
    private DbsField pnd_End_Date_T;
    private DbsField pnd_Start_Date_D;
    private DbsField pnd_End_Date_D;
    private DbsField pnd_Chck_Crprte_Due_Dte_Tme;
    private DbsField pnd_Check_End_Dte_Tme;
    private DbsField pnd_Hold_Close_Dte;

    private DbsGroup pnd_Hold_Close_Dte__R_Field_12;
    private DbsField pnd_Hold_Close_Dte_Pnd_Hold_Close_C;
    private DbsField pnd_Header_Date;
    private DbsField pnd_Format_Dte_Tme;

    private DbsGroup pnd_Format_Dte_Tme__R_Field_13;
    private DbsField pnd_Format_Dte_Tme_Pnd_Format_Dte;
    private DbsField pnd_Format_Dte_Tme_Pnd_Format_Tme;
    private DbsField pnd_Format_Dte_Tme_Upd;

    private DbsGroup pnd_Format_Dte_Tme_Upd__R_Field_14;
    private DbsField pnd_Format_Dte_Tme_Upd_Pnd_Format_Dte_U;
    private DbsField pnd_Format_Dte_Tme_Upd_Pnd_Format_Tme_U;
    private DbsField pnd_Header_Line_4;
    private DbsField pnd_Error_Msg;
    private DbsField pnd_Pass_Tlc;
    private DbsField pnd_Pass_Dob;

    private DbsGroup pnd_Pass_Dob__R_Field_15;
    private DbsField pnd_Pass_Dob_Pnd_Pass_Dob_A;
    private DbsField pnd_Pass_Pin;
    private DbsField pnd_Pass_Name;
    private DbsField pnd_Pass_Ssn;
    private DbsField pnd_Pass_Found;
    private DbsField pnd_Found_One;
    private DbsField pnd_Pass_Unit;
    private DbsField pnd_Pass_Division;
    private DbsField pnd_I;
    private DbsField pnd_Isn;
    private DbsField pnd_Sec_Turnaround_Tme;
    private DbsField pnd_Atsign;
    private DbsField pnd_Base_Dte_Tme_D;
    private DbsField pnd_End_Dte_Tme_D;
    private DbsField pnd_Bus_Days;
    private DbsField pnd_Time_T;
    private DbsField pnd_Unit_Time_T;
    private DbsField pnd_Part_Time_T;
    private DbsField pnd_Act_Strt_Time_T;
    private DbsField pnd_Act_Stop_Time_T;
    private DbsField pnd_Last_Chnge_Dte_Tme_T;
    private DbsField pnd_Static_Dte_Tme_A_15;
    private DbsField pnd_Static_Dte_Tme_T;
    private DbsField pnd_Act_Prcss_Tme;

    private DbsGroup pnd_Act_Prcss_Tme__R_Field_16;
    private DbsField pnd_Act_Prcss_Tme_Pnd_Act_Prcss_Tme_N;
    private DbsField pnd_Act_Wait_Tme;

    private DbsGroup pnd_Act_Wait_Tme__R_Field_17;
    private DbsField pnd_Act_Wait_Tme_Pnd_Act_Wait_Tme_N;
    private DbsField pnd_Number_W_Decimals;
    private DbsField pnd_Number_W_Decimals_13;
    private DbsField pnd_Pin_Base_Key;

    private DbsGroup pnd_Pin_Base_Key__R_Field_18;
    private DbsField pnd_Pin_Base_Key_Pin_Nbr;
    private DbsField pnd_Actv_Unque_Key;

    private DbsGroup pnd_Actv_Unque_Key__R_Field_19;
    private DbsField pnd_Actv_Unque_Key_Rqst_Log_Dte_Tme;
    private DbsField pnd_Actv_Unque_Key_Actve_Ind;

    private DbsGroup corr_Cntrct_Payee_Key;
    private DbsField corr_Cntrct_Payee_Key_Pnd_Corr_Cntrct_Payee_Key;

    private DbsGroup corr_Cntrct_Payee_Key__R_Field_20;
    private DbsField corr_Cntrct_Payee_Key_Correspondence_Addrss_Ind;
    private DbsField corr_Cntrct_Payee_Key_Cntrct_Nmbr;
    private DbsField corr_Cntrct_Payee_Key_Cntrct_Payee_Cde;
    private DbsField pnd_Cab_Key;

    private DbsGroup pnd_Cab_Key__R_Field_21;
    private DbsField pnd_Cab_Key_Pnd_Cabinet_Type;
    private DbsField pnd_Cab_Key_Pnd_Pin_Nbr;
    private DbsField pnd_Table_Id_Entry_Cde;

    private DbsGroup pnd_Table_Id_Entry_Cde__R_Field_22;
    private DbsField pnd_Table_Id_Entry_Cde_Entry_Table_Id_Nbr;
    private DbsField pnd_Table_Id_Entry_Cde_Entry_Cde;
    private DbsField pnd_Tbl_Key;

    private DbsGroup pnd_Tbl_Key__R_Field_23;
    private DbsField pnd_Tbl_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Key_Pnd_Tbl_Key_Field;
    private DbsField pnd_Tbl_Key_Pnd_Tbl_Actve_Ind;
    private DbsField pnd_Geo_Grp;

    private DbsGroup pnd_Geo_Grp__R_Field_24;
    private DbsField pnd_Geo_Grp_Pnd_State;
    private DbsField pnd_Geo_Grp_Pnd_Geo_Code;
    private DbsField pnd_Ph_Key;

    private DbsGroup pnd_Ph_Key__R_Field_25;
    private DbsField pnd_Ph_Key_Rcrd_Type;
    private DbsField pnd_Ph_Key_Srvvr_Mit_Idntfr;
    private DbsField pnd_Mit_Read_Cntr;
    private DbsField pnd_Work_Read_Cntr;
    private DbsField pnd_Wrqst_Add_Cntr;
    private DbsField pnd_Wrqst_Upd_Cntr;
    private DbsField pnd_Cntrt_Add_Cntr;
    private DbsField pnd_Cntrt_Upd_Cntr;
    private DbsField pnd_Unita_Add_Cntr;
    private DbsField pnd_Unita_Upd_Cntr;
    private DbsField pnd_Empla_Add_Cntr;
    private DbsField pnd_Empla_Upd_Cntr;
    private DbsField pnd_Stepa_Add_Cntr;
    private DbsField pnd_Stepa_Upd_Cntr;
    private DbsField pnd_Extra_Add_Cntr;
    private DbsField pnd_Extra_Upd_Cntr;
    private DbsField pnd_Intra_Add_Cntr;
    private DbsField pnd_Intra_Upd_Cntr;
    private DbsField pnd_Enrta_Add_Cntr;
    private DbsField pnd_Enrta_Upd_Cntr;
    private DbsField pnd_Del_Wrqst_Cntr;
    private DbsField pnd_Tlc_Cntr;
    private DbsField pnd_Total_Rec_Cntr;
    private DbsField pnd_Cstat_Add_Cntr;
    private DbsField pnd_Adtnl_Wrk_Add;
    private DbsField pnd_Adtnl_Wrk_Upd;
    private DbsField pnd_Late_Upd;
    private DbsField pnd_Late_Add;
    private DbsField pnd_No_Base_Rec;
    private DbsField pnd_Vac_Address;
    private DbsField pnd_No_Base_Rec_V;
    private DbsField pnd_No_State_In_Table;
    private DbsField pnd_Rerun_Wr_Skip;
    private DbsField pnd_Pass_Nfnd_Cntr;
    private DbsField pnd_Nbr_Act_Recs;
    private DbsField pnd_Act_Wr_Accept;
    private DbsField pnd_Cor_Pin_Cntr;
    private DbsField pnd_Add;
    private DbsField pnd_Update;
    private DbsField pnd_Foreign_Address;
    private DbsField pnd_Status_Of_Interest;
    private DbsField pnd_C_Status_Match;
    private DbsField pnd_End_Date_Switch;
    private DbsField pnd_Wipe_Out_End_Act;
    private DbsField pnd_Static_Sw;
    private DbsField pnd_Activity_Type;

    private DbsGroup pnd_Frgn_Geo_Pass;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_1;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_2;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_3;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_4;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_5;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_6;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_Return_Cde;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_Mail_Cde;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_Geo_Cde;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_St_Cde1;
    private DbsField pnd_Frgn_Geo_Pass_Pnd_St_Cde2;

    private DataAccessProgramView vw_cwf_Efm_Cabinet;
    private DbsField cwf_Efm_Cabinet_Cabinet_Id;

    private DbsGroup cwf_Efm_Cabinet__R_Field_26;
    private DbsField cwf_Efm_Cabinet_Pnd_Policy_Holder;
    private DbsField cwf_Efm_Cabinet_Pnd_Pin_Nbr;
    private DbsField cwf_Efm_Cabinet_Active_Ind;
    private DbsField cwf_Efm_Cabinet_Media_Ind;
    private DbsField pnd_Pin_Nbr_A12;

    private DbsGroup pnd_Pin_Nbr_A12__R_Field_27;
    private DbsField pnd_Pin_Nbr_A12_Pnd_Pin_Nbr_1_5;
    private DbsField pnd_Pin_Nbr_A12_Pnd_Pin_Nbr_6_12;

    private DbsRecord setTimeRecord;
    private DbsField st;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCwfa4815 = new PdaCwfa4815(localVariables);
        pdaNsta1245 = new PdaNsta1245(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        ldaCwfl4817 = new LdaCwfl4817();
        registerRecord(ldaCwfl4817);
        registerRecord(ldaCwfl4817.getVw_cwf_Master());
        ldaCwfl4818 = new LdaCwfl4818();
        registerRecord(ldaCwfl4818);
        registerRecord(ldaCwfl4818.getVw_cwf_Route_Support_Tbl());
        ldaCwfl4815 = new LdaCwfl4815();
        registerRecord(ldaCwfl4815);
        registerRecord(ldaCwfl4815.getVw_cwf_Support_Tbl());
        ldaCwfl4819 = new LdaCwfl4819();
        registerRecord(ldaCwfl4819);
        ldaCwfl4899 = new LdaCwfl4899();
        registerRecord(ldaCwfl4899);
        ldaCwfl4820 = new LdaCwfl4820();
        registerRecord(ldaCwfl4820);
        ldaCwfl4821 = new LdaCwfl4821();
        registerRecord(ldaCwfl4821);
        ldaCwfl4822 = new LdaCwfl4822();
        registerRecord(ldaCwfl4822);
        ldaCwfl4823 = new LdaCwfl4823();
        registerRecord(ldaCwfl4823);
        ldaCwfl4824 = new LdaCwfl4824();
        registerRecord(ldaCwfl4824);
        ldaCwfl4828 = new LdaCwfl4828();
        registerRecord(ldaCwfl4828);
        ldaCwfl4829 = new LdaCwfl4829();
        registerRecord(ldaCwfl4829);
        registerRecord(ldaCwfl4829.getVw_cwf_Bsnss_Data_File());
        ldaCwfl4830 = new LdaCwfl4830();
        registerRecord(ldaCwfl4830);
        pdaCwfa1400 = new PdaCwfa1400(localVariables);
        pdaCwfa1401 = new PdaCwfa1401(localVariables);

        // Local Variables

        pnd_Work_File_1 = localVariables.newGroupInRecord("pnd_Work_File_1", "#WORK-FILE-1");
        pnd_Work_File_1_Pnd_Rqst_Log_Dte_Tme = pnd_Work_File_1.newFieldInGroup("pnd_Work_File_1_Pnd_Rqst_Log_Dte_Tme", "#RQST-LOG-DTE-TME", FieldType.STRING, 
            15);

        vw_cwf_Master_View_Select = new DataAccessProgramView(new NameInfo("vw_cwf_Master_View_Select", "CWF-MASTER-VIEW-SELECT"), "CWF_MASTER_INDEX_VIEW", 
            "CWF_MASTER_INDEX");
        cwf_Master_View_Select_Rqst_Log_Dte_Tme = vw_cwf_Master_View_Select.getRecord().newFieldInGroup("cwf_Master_View_Select_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Master_View_Select_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        cwf_Master_View_Select_Rqst_Id = vw_cwf_Master_View_Select.getRecord().newFieldInGroup("cwf_Master_View_Select_Rqst_Id", "RQST-ID", FieldType.STRING, 
            28, RepeatingFieldStrategy.None, "RQST_ID");
        cwf_Master_View_Select_Rqst_Id.setDdmHeader("REQUEST ID");

        cwf_Master_View_Select__R_Field_1 = vw_cwf_Master_View_Select.getRecord().newGroupInGroup("cwf_Master_View_Select__R_Field_1", "REDEFINE", cwf_Master_View_Select_Rqst_Id);
        cwf_Master_View_Select_Rqst_Id_Wpid = cwf_Master_View_Select__R_Field_1.newFieldInGroup("cwf_Master_View_Select_Rqst_Id_Wpid", "RQST-ID-WPID", 
            FieldType.STRING, 6);
        registerRecord(vw_cwf_Master_View_Select);

        vw_cwf_Sb_Ph = new DataAccessProgramView(new NameInfo("vw_cwf_Sb_Ph", "CWF-SB-PH"), "CWF_SB_PH", "CWF_SB_BENE");
        cwf_Sb_Ph_Rcrd_Type = vw_cwf_Sb_Ph.getRecord().newFieldInGroup("cwf_Sb_Ph_Rcrd_Type", "RCRD-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RCRD_TYPE");
        cwf_Sb_Ph_Rcrd_Type.setDdmHeader("RECORD/TYPE");
        cwf_Sb_Ph_Srvvr_Mit_Idntfr = vw_cwf_Sb_Ph.getRecord().newFieldInGroup("cwf_Sb_Ph_Srvvr_Mit_Idntfr", "SRVVR-MIT-IDNTFR", FieldType.STRING, 14, 
            RepeatingFieldStrategy.None, "SRVVR_MIT_IDNTFR");
        cwf_Sb_Ph_Srvvr_Mit_Idntfr.setDdmHeader("SURVIVOR MIT/IDENTIFIER");
        cwf_Sb_Ph_Dod_Not_Dte = vw_cwf_Sb_Ph.getRecord().newFieldInGroup("cwf_Sb_Ph_Dod_Not_Dte", "DOD-NOT-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "DOD_NOT_DTE");
        cwf_Sb_Ph_Dod_Not_Dte.setDdmHeader("DOD NOTIF/DATE");

        cwf_Sb_Ph__R_Field_2 = vw_cwf_Sb_Ph.getRecord().newGroupInGroup("cwf_Sb_Ph__R_Field_2", "REDEFINE", cwf_Sb_Ph_Dod_Not_Dte);
        cwf_Sb_Ph_Dod_Not_Dte_A = cwf_Sb_Ph__R_Field_2.newFieldInGroup("cwf_Sb_Ph_Dod_Not_Dte_A", "DOD-NOT-DTE-A", FieldType.STRING, 8);
        registerRecord(vw_cwf_Sb_Ph);

        pnd_Work_File_23 = localVariables.newGroupInRecord("pnd_Work_File_23", "#WORK-FILE-23");
        pnd_Work_File_23_Pnd_Cor_Pin_Field = pnd_Work_File_23.newFieldInGroup("pnd_Work_File_23_Pnd_Cor_Pin_Field", "#COR-PIN-FIELD", FieldType.STRING, 
            7);
        pnd_Tbl_Start_Dte_Tme_T = localVariables.newFieldInRecord("pnd_Tbl_Start_Dte_Tme_T", "#TBL-START-DTE-TME-T", FieldType.TIME);
        pnd_Tbl_End_Dte_Tme_T = localVariables.newFieldInRecord("pnd_Tbl_End_Dte_Tme_T", "#TBL-END-DTE-TME-T", FieldType.TIME);
        pnd_Fix_Time = localVariables.newFieldInRecord("pnd_Fix_Time", "#FIX-TIME", FieldType.STRING, 15);
        pnd_Fix_Dte_Tme = localVariables.newFieldInRecord("pnd_Fix_Dte_Tme", "#FIX-DTE-TME", FieldType.STRING, 15);

        pnd_Fix_Dte_Tme__R_Field_3 = localVariables.newGroupInRecord("pnd_Fix_Dte_Tme__R_Field_3", "REDEFINE", pnd_Fix_Dte_Tme);
        pnd_Fix_Dte_Tme_Pnd_Dte = pnd_Fix_Dte_Tme__R_Field_3.newFieldInGroup("pnd_Fix_Dte_Tme_Pnd_Dte", "#DTE", FieldType.STRING, 8);

        pnd_Fix_Dte_Tme__R_Field_4 = pnd_Fix_Dte_Tme__R_Field_3.newGroupInGroup("pnd_Fix_Dte_Tme__R_Field_4", "REDEFINE", pnd_Fix_Dte_Tme_Pnd_Dte);
        pnd_Fix_Dte_Tme_Pnd_Dte_Yyyy = pnd_Fix_Dte_Tme__R_Field_4.newFieldInGroup("pnd_Fix_Dte_Tme_Pnd_Dte_Yyyy", "#DTE-YYYY", FieldType.NUMERIC, 4);
        pnd_Fix_Dte_Tme_Pnd_Mm = pnd_Fix_Dte_Tme__R_Field_4.newFieldInGroup("pnd_Fix_Dte_Tme_Pnd_Mm", "#MM", FieldType.NUMERIC, 2);
        pnd_Fix_Dte_Tme_Pnd_Dd = pnd_Fix_Dte_Tme__R_Field_4.newFieldInGroup("pnd_Fix_Dte_Tme_Pnd_Dd", "#DD", FieldType.NUMERIC, 2);
        pnd_Fix_Dte_Tme_Pnd_Tme = pnd_Fix_Dte_Tme__R_Field_3.newFieldInGroup("pnd_Fix_Dte_Tme_Pnd_Tme", "#TME", FieldType.STRING, 7);
        pnd_Fix_Dte_10 = localVariables.newFieldInRecord("pnd_Fix_Dte_10", "#FIX-DTE-10", FieldType.STRING, 10);

        pnd_Fix_Dte_10__R_Field_5 = localVariables.newGroupInRecord("pnd_Fix_Dte_10__R_Field_5", "REDEFINE", pnd_Fix_Dte_10);
        pnd_Fix_Dte_10_Pnd_Fix_Dte_6 = pnd_Fix_Dte_10__R_Field_5.newFieldInGroup("pnd_Fix_Dte_10_Pnd_Fix_Dte_6", "#FIX-DTE-6", FieldType.STRING, 6);
        pnd_Fix_Dte_10_Pnd_Fix_Dte_Yyyy = pnd_Fix_Dte_10__R_Field_5.newFieldInGroup("pnd_Fix_Dte_10_Pnd_Fix_Dte_Yyyy", "#FIX-DTE-YYYY", FieldType.NUMERIC, 
            4);
        pnd_Fix_Dte = localVariables.newFieldInRecord("pnd_Fix_Dte", "#FIX-DTE", FieldType.STRING, 8);
        pnd_Effctve_Dte = localVariables.newFieldInRecord("pnd_Effctve_Dte", "#EFFCTVE-DTE", FieldType.STRING, 8);
        pnd_Trans_Dte = localVariables.newFieldInRecord("pnd_Trans_Dte", "#TRANS-DTE", FieldType.STRING, 8);
        pnd_Trnsctn_Dte = localVariables.newFieldInRecord("pnd_Trnsctn_Dte", "#TRNSCTN-DTE", FieldType.STRING, 8);
        pnd_Sec_Updte_Dte = localVariables.newFieldInRecord("pnd_Sec_Updte_Dte", "#SEC-UPDTE-DTE", FieldType.STRING, 8);
        pnd_Date_Split = localVariables.newFieldInRecord("pnd_Date_Split", "#DATE-SPLIT", FieldType.STRING, 15);

        pnd_Date_Split__R_Field_6 = localVariables.newGroupInRecord("pnd_Date_Split__R_Field_6", "REDEFINE", pnd_Date_Split);
        pnd_Date_Split_Pnd_Date_Split_Dte = pnd_Date_Split__R_Field_6.newFieldInGroup("pnd_Date_Split_Pnd_Date_Split_Dte", "#DATE-SPLIT-DTE", FieldType.STRING, 
            8);
        pnd_Date_Split_Pnd_Date_Split_Tme = pnd_Date_Split__R_Field_6.newFieldInGroup("pnd_Date_Split_Pnd_Date_Split_Tme", "#DATE-SPLIT-TME", FieldType.STRING, 
            7);
        pnd_Dob = localVariables.newFieldInRecord("pnd_Dob", "#DOB", FieldType.DATE);
        pnd_Dod_D = localVariables.newFieldInRecord("pnd_Dod_D", "#DOD-D", FieldType.DATE);
        pnd_Time = localVariables.newFieldInRecord("pnd_Time", "#TIME", FieldType.TIME);
        pnd_Rqst_Log_Dte_Tme_Save = localVariables.newFieldInRecord("pnd_Rqst_Log_Dte_Tme_Save", "#RQST-LOG-DTE-TME-SAVE", FieldType.STRING, 15);
        pnd_Pin_Number_Save = localVariables.newFieldInRecord("pnd_Pin_Number_Save", "#PIN-NUMBER-SAVE", FieldType.NUMERIC, 12);
        pnd_End_Dte_Tme = localVariables.newFieldInRecord("pnd_End_Dte_Tme", "#END-DTE-TME", FieldType.STRING, 15);

        pnd_End_Dte_Tme__R_Field_7 = localVariables.newGroupInRecord("pnd_End_Dte_Tme__R_Field_7", "REDEFINE", pnd_End_Dte_Tme);
        pnd_End_Dte_Tme_Pnd_End_Yyyymmdd = pnd_End_Dte_Tme__R_Field_7.newFieldInGroup("pnd_End_Dte_Tme_Pnd_End_Yyyymmdd", "#END-YYYYMMDD", FieldType.STRING, 
            8);
        pnd_End_Dte_Tme_Pnd_End_Hhiisst = pnd_End_Dte_Tme__R_Field_7.newFieldInGroup("pnd_End_Dte_Tme_Pnd_End_Hhiisst", "#END-HHIISST", FieldType.STRING, 
            7);

        pnd_End_Dte_Tme__R_Field_8 = localVariables.newGroupInRecord("pnd_End_Dte_Tme__R_Field_8", "REDEFINE", pnd_End_Dte_Tme);
        pnd_End_Dte_Tme_Pnd_End_Dte_Tme_N = pnd_End_Dte_Tme__R_Field_8.newFieldInGroup("pnd_End_Dte_Tme_Pnd_End_Dte_Tme_N", "#END-DTE-TME-N", FieldType.NUMERIC, 
            15);
        pnd_Conversion_Due_Dte_Chg_Prty_Cde = localVariables.newFieldInRecord("pnd_Conversion_Due_Dte_Chg_Prty_Cde", "#CONVERSION-DUE-DTE-CHG-PRTY-CDE", 
            FieldType.STRING, 16);

        pnd_Conversion_Due_Dte_Chg_Prty_Cde__R_Field_9 = localVariables.newGroupInRecord("pnd_Conversion_Due_Dte_Chg_Prty_Cde__R_Field_9", "REDEFINE", 
            pnd_Conversion_Due_Dte_Chg_Prty_Cde);
        pnd_Conversion_Due_Dte_Chg_Prty_Cde_Pnd_Conversion_Fill_1 = pnd_Conversion_Due_Dte_Chg_Prty_Cde__R_Field_9.newFieldInGroup("pnd_Conversion_Due_Dte_Chg_Prty_Cde_Pnd_Conversion_Fill_1", 
            "#CONVERSION-FILL-1", FieldType.STRING, 11);
        pnd_Conversion_Due_Dte_Chg_Prty_Cde_Pnd_Recreate_Ind = pnd_Conversion_Due_Dte_Chg_Prty_Cde__R_Field_9.newFieldInGroup("pnd_Conversion_Due_Dte_Chg_Prty_Cde_Pnd_Recreate_Ind", 
            "#RECREATE-IND", FieldType.STRING, 1);
        pnd_Conversion_Due_Dte_Chg_Prty_Cde_Pnd_Conversion_Fill_2 = pnd_Conversion_Due_Dte_Chg_Prty_Cde__R_Field_9.newFieldInGroup("pnd_Conversion_Due_Dte_Chg_Prty_Cde_Pnd_Conversion_Fill_2", 
            "#CONVERSION-FILL-2", FieldType.STRING, 3);
        pnd_Conversion_Due_Dte_Chg_Prty_Cde_Pnd_Prcssng_Type_Cde = pnd_Conversion_Due_Dte_Chg_Prty_Cde__R_Field_9.newFieldInGroup("pnd_Conversion_Due_Dte_Chg_Prty_Cde_Pnd_Prcssng_Type_Cde", 
            "#PRCSSNG-TYPE-CDE", FieldType.STRING, 1);
        pnd_Curr_Dte_Tme = localVariables.newFieldInRecord("pnd_Curr_Dte_Tme", "#CURR-DTE-TME", FieldType.STRING, 15);
        pnd_Prev_Dte_Tme = localVariables.newFieldInRecord("pnd_Prev_Dte_Tme", "#PREV-DTE-TME", FieldType.STRING, 15);

        pnd_Prev_Dte_Tme__R_Field_10 = localVariables.newGroupInRecord("pnd_Prev_Dte_Tme__R_Field_10", "REDEFINE", pnd_Prev_Dte_Tme);
        pnd_Prev_Dte_Tme_Pnd_Prev_Yyyymmdd = pnd_Prev_Dte_Tme__R_Field_10.newFieldInGroup("pnd_Prev_Dte_Tme_Pnd_Prev_Yyyymmdd", "#PREV-YYYYMMDD", FieldType.STRING, 
            8);
        pnd_Prev_Dte_Tme_Pnd_Prev_Hhiisst = pnd_Prev_Dte_Tme__R_Field_10.newFieldInGroup("pnd_Prev_Dte_Tme_Pnd_Prev_Hhiisst", "#PREV-HHIISST", FieldType.STRING, 
            7);
        pnd_Service_Days = localVariables.newFieldInRecord("pnd_Service_Days", "#SERVICE-DAYS", FieldType.NUMERIC, 3);
        pnd_Service_Hrs = localVariables.newFieldInRecord("pnd_Service_Hrs", "#SERVICE-HRS", FieldType.NUMERIC, 2);
        pnd_Service_Mins = localVariables.newFieldInRecord("pnd_Service_Mins", "#SERVICE-MINS", FieldType.NUMERIC, 2);
        pnd_Elpsd_Time_Compose = localVariables.newFieldInRecord("pnd_Elpsd_Time_Compose", "#ELPSD-TIME-COMPOSE", FieldType.STRING, 15);

        pnd_Elpsd_Time_Compose__R_Field_11 = localVariables.newGroupInRecord("pnd_Elpsd_Time_Compose__R_Field_11", "REDEFINE", pnd_Elpsd_Time_Compose);
        pnd_Elpsd_Time_Compose_Pnd_Elpsd_Date_Portion = pnd_Elpsd_Time_Compose__R_Field_11.newFieldInGroup("pnd_Elpsd_Time_Compose_Pnd_Elpsd_Date_Portion", 
            "#ELPSD-DATE-PORTION", FieldType.STRING, 8);
        pnd_Elpsd_Time_Compose_Pnd_Elpsd_Time_Portion = pnd_Elpsd_Time_Compose__R_Field_11.newFieldInGroup("pnd_Elpsd_Time_Compose_Pnd_Elpsd_Time_Portion", 
            "#ELPSD-TIME-PORTION", FieldType.STRING, 7);
        pnd_Elpsd_Days = localVariables.newFieldInRecord("pnd_Elpsd_Days", "#ELPSD-DAYS", FieldType.NUMERIC, 9, 2);
        pnd_Date_T = localVariables.newFieldInRecord("pnd_Date_T", "#DATE-T", FieldType.TIME);
        pnd_Date_D = localVariables.newFieldInRecord("pnd_Date_D", "#DATE-D", FieldType.DATE);
        pnd_Hold_Tme_A = localVariables.newFieldInRecord("pnd_Hold_Tme_A", "#HOLD-TME-A", FieldType.STRING, 7);
        pnd_Start_Date_T = localVariables.newFieldInRecord("pnd_Start_Date_T", "#START-DATE-T", FieldType.TIME);
        pnd_End_Date_T = localVariables.newFieldInRecord("pnd_End_Date_T", "#END-DATE-T", FieldType.TIME);
        pnd_Start_Date_D = localVariables.newFieldInRecord("pnd_Start_Date_D", "#START-DATE-D", FieldType.DATE);
        pnd_End_Date_D = localVariables.newFieldInRecord("pnd_End_Date_D", "#END-DATE-D", FieldType.DATE);
        pnd_Chck_Crprte_Due_Dte_Tme = localVariables.newFieldInRecord("pnd_Chck_Crprte_Due_Dte_Tme", "#CHCK-CRPRTE-DUE-DTE-TME", FieldType.STRING, 15);
        pnd_Check_End_Dte_Tme = localVariables.newFieldInRecord("pnd_Check_End_Dte_Tme", "#CHECK-END-DTE-TME", FieldType.TIME);
        pnd_Hold_Close_Dte = localVariables.newFieldInRecord("pnd_Hold_Close_Dte", "#HOLD-CLOSE-DTE", FieldType.STRING, 15);

        pnd_Hold_Close_Dte__R_Field_12 = localVariables.newGroupInRecord("pnd_Hold_Close_Dte__R_Field_12", "REDEFINE", pnd_Hold_Close_Dte);
        pnd_Hold_Close_Dte_Pnd_Hold_Close_C = pnd_Hold_Close_Dte__R_Field_12.newFieldInGroup("pnd_Hold_Close_Dte_Pnd_Hold_Close_C", "#HOLD-CLOSE-C", FieldType.STRING, 
            1);
        pnd_Header_Date = localVariables.newFieldInRecord("pnd_Header_Date", "#HEADER-DATE", FieldType.STRING, 26);
        pnd_Format_Dte_Tme = localVariables.newFieldInRecord("pnd_Format_Dte_Tme", "#FORMAT-DTE-TME", FieldType.STRING, 26);

        pnd_Format_Dte_Tme__R_Field_13 = localVariables.newGroupInRecord("pnd_Format_Dte_Tme__R_Field_13", "REDEFINE", pnd_Format_Dte_Tme);
        pnd_Format_Dte_Tme_Pnd_Format_Dte = pnd_Format_Dte_Tme__R_Field_13.newFieldInGroup("pnd_Format_Dte_Tme_Pnd_Format_Dte", "#FORMAT-DTE", FieldType.STRING, 
            13);
        pnd_Format_Dte_Tme_Pnd_Format_Tme = pnd_Format_Dte_Tme__R_Field_13.newFieldInGroup("pnd_Format_Dte_Tme_Pnd_Format_Tme", "#FORMAT-TME", FieldType.STRING, 
            13);
        pnd_Format_Dte_Tme_Upd = localVariables.newFieldInRecord("pnd_Format_Dte_Tme_Upd", "#FORMAT-DTE-TME-UPD", FieldType.STRING, 24);

        pnd_Format_Dte_Tme_Upd__R_Field_14 = localVariables.newGroupInRecord("pnd_Format_Dte_Tme_Upd__R_Field_14", "REDEFINE", pnd_Format_Dte_Tme_Upd);
        pnd_Format_Dte_Tme_Upd_Pnd_Format_Dte_U = pnd_Format_Dte_Tme_Upd__R_Field_14.newFieldInGroup("pnd_Format_Dte_Tme_Upd_Pnd_Format_Dte_U", "#FORMAT-DTE-U", 
            FieldType.STRING, 12);
        pnd_Format_Dte_Tme_Upd_Pnd_Format_Tme_U = pnd_Format_Dte_Tme_Upd__R_Field_14.newFieldInGroup("pnd_Format_Dte_Tme_Upd_Pnd_Format_Tme_U", "#FORMAT-TME-U", 
            FieldType.STRING, 12);
        pnd_Header_Line_4 = localVariables.newFieldInRecord("pnd_Header_Line_4", "#HEADER-LINE-4", FieldType.STRING, 63);
        pnd_Error_Msg = localVariables.newFieldInRecord("pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 132);
        pnd_Pass_Tlc = localVariables.newFieldInRecord("pnd_Pass_Tlc", "#PASS-TLC", FieldType.STRING, 7);
        pnd_Pass_Dob = localVariables.newFieldInRecord("pnd_Pass_Dob", "#PASS-DOB", FieldType.NUMERIC, 8);

        pnd_Pass_Dob__R_Field_15 = localVariables.newGroupInRecord("pnd_Pass_Dob__R_Field_15", "REDEFINE", pnd_Pass_Dob);
        pnd_Pass_Dob_Pnd_Pass_Dob_A = pnd_Pass_Dob__R_Field_15.newFieldInGroup("pnd_Pass_Dob_Pnd_Pass_Dob_A", "#PASS-DOB-A", FieldType.STRING, 8);
        pnd_Pass_Pin = localVariables.newFieldInRecord("pnd_Pass_Pin", "#PASS-PIN", FieldType.NUMERIC, 12);
        pnd_Pass_Name = localVariables.newFieldInRecord("pnd_Pass_Name", "#PASS-NAME", FieldType.STRING, 40);
        pnd_Pass_Ssn = localVariables.newFieldInRecord("pnd_Pass_Ssn", "#PASS-SSN", FieldType.NUMERIC, 9);
        pnd_Pass_Found = localVariables.newFieldInRecord("pnd_Pass_Found", "#PASS-FOUND", FieldType.BOOLEAN, 1);
        pnd_Found_One = localVariables.newFieldInRecord("pnd_Found_One", "#FOUND-ONE", FieldType.BOOLEAN, 1);
        pnd_Pass_Unit = localVariables.newFieldInRecord("pnd_Pass_Unit", "#PASS-UNIT", FieldType.STRING, 8);
        pnd_Pass_Division = localVariables.newFieldInRecord("pnd_Pass_Division", "#PASS-DIVISION", FieldType.STRING, 6);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Sec_Turnaround_Tme = localVariables.newFieldInRecord("pnd_Sec_Turnaround_Tme", "#SEC-TURNAROUND-TME", FieldType.NUMERIC, 9, 2);
        pnd_Atsign = localVariables.newFieldInRecord("pnd_Atsign", "#ATSIGN", FieldType.STRING, 1);
        pnd_Base_Dte_Tme_D = localVariables.newFieldInRecord("pnd_Base_Dte_Tme_D", "#BASE-DTE-TME-D", FieldType.DATE);
        pnd_End_Dte_Tme_D = localVariables.newFieldInRecord("pnd_End_Dte_Tme_D", "#END-DTE-TME-D", FieldType.DATE);
        pnd_Bus_Days = localVariables.newFieldInRecord("pnd_Bus_Days", "#BUS-DAYS", FieldType.NUMERIC, 7);
        pnd_Time_T = localVariables.newFieldInRecord("pnd_Time_T", "#TIME-T", FieldType.TIME);
        pnd_Unit_Time_T = localVariables.newFieldInRecord("pnd_Unit_Time_T", "#UNIT-TIME-T", FieldType.TIME);
        pnd_Part_Time_T = localVariables.newFieldInRecord("pnd_Part_Time_T", "#PART-TIME-T", FieldType.TIME);
        pnd_Act_Strt_Time_T = localVariables.newFieldInRecord("pnd_Act_Strt_Time_T", "#ACT-STRT-TIME-T", FieldType.TIME);
        pnd_Act_Stop_Time_T = localVariables.newFieldInRecord("pnd_Act_Stop_Time_T", "#ACT-STOP-TIME-T", FieldType.TIME);
        pnd_Last_Chnge_Dte_Tme_T = localVariables.newFieldInRecord("pnd_Last_Chnge_Dte_Tme_T", "#LAST-CHNGE-DTE-TME-T", FieldType.TIME);
        pnd_Static_Dte_Tme_A_15 = localVariables.newFieldInRecord("pnd_Static_Dte_Tme_A_15", "#STATIC-DTE-TME-A-15", FieldType.STRING, 15);
        pnd_Static_Dte_Tme_T = localVariables.newFieldInRecord("pnd_Static_Dte_Tme_T", "#STATIC-DTE-TME-T", FieldType.TIME);
        pnd_Act_Prcss_Tme = localVariables.newFieldInRecord("pnd_Act_Prcss_Tme", "#ACT-PRCSS-TME", FieldType.STRING, 7);

        pnd_Act_Prcss_Tme__R_Field_16 = localVariables.newGroupInRecord("pnd_Act_Prcss_Tme__R_Field_16", "REDEFINE", pnd_Act_Prcss_Tme);
        pnd_Act_Prcss_Tme_Pnd_Act_Prcss_Tme_N = pnd_Act_Prcss_Tme__R_Field_16.newFieldInGroup("pnd_Act_Prcss_Tme_Pnd_Act_Prcss_Tme_N", "#ACT-PRCSS-TME-N", 
            FieldType.NUMERIC, 6, 2);
        pnd_Act_Wait_Tme = localVariables.newFieldInRecord("pnd_Act_Wait_Tme", "#ACT-WAIT-TME", FieldType.STRING, 7);

        pnd_Act_Wait_Tme__R_Field_17 = localVariables.newGroupInRecord("pnd_Act_Wait_Tme__R_Field_17", "REDEFINE", pnd_Act_Wait_Tme);
        pnd_Act_Wait_Tme_Pnd_Act_Wait_Tme_N = pnd_Act_Wait_Tme__R_Field_17.newFieldInGroup("pnd_Act_Wait_Tme_Pnd_Act_Wait_Tme_N", "#ACT-WAIT-TME-N", FieldType.NUMERIC, 
            6, 2);
        pnd_Number_W_Decimals = localVariables.newFieldInRecord("pnd_Number_W_Decimals", "#NUMBER-W-DECIMALS", FieldType.NUMERIC, 9, 2);
        pnd_Number_W_Decimals_13 = localVariables.newFieldInRecord("pnd_Number_W_Decimals_13", "#NUMBER-W-DECIMALS-13", FieldType.NUMERIC, 13, 2);
        pnd_Pin_Base_Key = localVariables.newFieldInRecord("pnd_Pin_Base_Key", "#PIN-BASE-KEY", FieldType.STRING, 13);

        pnd_Pin_Base_Key__R_Field_18 = localVariables.newGroupInRecord("pnd_Pin_Base_Key__R_Field_18", "REDEFINE", pnd_Pin_Base_Key);
        pnd_Pin_Base_Key_Pin_Nbr = pnd_Pin_Base_Key__R_Field_18.newFieldInGroup("pnd_Pin_Base_Key_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12);
        pnd_Actv_Unque_Key = localVariables.newFieldInRecord("pnd_Actv_Unque_Key", "#ACTV-UNQUE-KEY", FieldType.STRING, 16);

        pnd_Actv_Unque_Key__R_Field_19 = localVariables.newGroupInRecord("pnd_Actv_Unque_Key__R_Field_19", "REDEFINE", pnd_Actv_Unque_Key);
        pnd_Actv_Unque_Key_Rqst_Log_Dte_Tme = pnd_Actv_Unque_Key__R_Field_19.newFieldInGroup("pnd_Actv_Unque_Key_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", 
            FieldType.STRING, 15);
        pnd_Actv_Unque_Key_Actve_Ind = pnd_Actv_Unque_Key__R_Field_19.newFieldInGroup("pnd_Actv_Unque_Key_Actve_Ind", "ACTVE-IND", FieldType.STRING, 1);

        corr_Cntrct_Payee_Key = localVariables.newGroupInRecord("corr_Cntrct_Payee_Key", "CORR-CNTRCT-PAYEE-KEY");
        corr_Cntrct_Payee_Key_Pnd_Corr_Cntrct_Payee_Key = corr_Cntrct_Payee_Key.newFieldInGroup("corr_Cntrct_Payee_Key_Pnd_Corr_Cntrct_Payee_Key", "#CORR-CNTRCT-PAYEE-KEY", 
            FieldType.STRING, 13);

        corr_Cntrct_Payee_Key__R_Field_20 = corr_Cntrct_Payee_Key.newGroupInGroup("corr_Cntrct_Payee_Key__R_Field_20", "REDEFINE", corr_Cntrct_Payee_Key_Pnd_Corr_Cntrct_Payee_Key);
        corr_Cntrct_Payee_Key_Correspondence_Addrss_Ind = corr_Cntrct_Payee_Key__R_Field_20.newFieldInGroup("corr_Cntrct_Payee_Key_Correspondence_Addrss_Ind", 
            "CORRESPONDENCE-ADDRSS-IND", FieldType.STRING, 1);
        corr_Cntrct_Payee_Key_Cntrct_Nmbr = corr_Cntrct_Payee_Key__R_Field_20.newFieldInGroup("corr_Cntrct_Payee_Key_Cntrct_Nmbr", "CNTRCT-NMBR", FieldType.STRING, 
            10);
        corr_Cntrct_Payee_Key_Cntrct_Payee_Cde = corr_Cntrct_Payee_Key__R_Field_20.newFieldInGroup("corr_Cntrct_Payee_Key_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Cab_Key = localVariables.newFieldInRecord("pnd_Cab_Key", "#CAB-KEY", FieldType.STRING, 14);

        pnd_Cab_Key__R_Field_21 = localVariables.newGroupInRecord("pnd_Cab_Key__R_Field_21", "REDEFINE", pnd_Cab_Key);
        pnd_Cab_Key_Pnd_Cabinet_Type = pnd_Cab_Key__R_Field_21.newFieldInGroup("pnd_Cab_Key_Pnd_Cabinet_Type", "#CABINET-TYPE", FieldType.STRING, 1);
        pnd_Cab_Key_Pnd_Pin_Nbr = pnd_Cab_Key__R_Field_21.newFieldInGroup("pnd_Cab_Key_Pnd_Pin_Nbr", "#PIN-NBR", FieldType.NUMERIC, 12);
        pnd_Table_Id_Entry_Cde = localVariables.newFieldInRecord("pnd_Table_Id_Entry_Cde", "#TABLE-ID-ENTRY-CDE", FieldType.STRING, 26);

        pnd_Table_Id_Entry_Cde__R_Field_22 = localVariables.newGroupInRecord("pnd_Table_Id_Entry_Cde__R_Field_22", "REDEFINE", pnd_Table_Id_Entry_Cde);
        pnd_Table_Id_Entry_Cde_Entry_Table_Id_Nbr = pnd_Table_Id_Entry_Cde__R_Field_22.newFieldInGroup("pnd_Table_Id_Entry_Cde_Entry_Table_Id_Nbr", "ENTRY-TABLE-ID-NBR", 
            FieldType.STRING, 6);
        pnd_Table_Id_Entry_Cde_Entry_Cde = pnd_Table_Id_Entry_Cde__R_Field_22.newFieldInGroup("pnd_Table_Id_Entry_Cde_Entry_Cde", "ENTRY-CDE", FieldType.STRING, 
            20);
        pnd_Tbl_Key = localVariables.newFieldInRecord("pnd_Tbl_Key", "#TBL-KEY", FieldType.STRING, 53);

        pnd_Tbl_Key__R_Field_23 = localVariables.newGroupInRecord("pnd_Tbl_Key__R_Field_23", "REDEFINE", pnd_Tbl_Key);
        pnd_Tbl_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Key__R_Field_23.newFieldInGroup("pnd_Tbl_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", FieldType.STRING, 
            2);
        pnd_Tbl_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Key__R_Field_23.newFieldInGroup("pnd_Tbl_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 20);
        pnd_Tbl_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Key__R_Field_23.newFieldInGroup("pnd_Tbl_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 30);
        pnd_Tbl_Key_Pnd_Tbl_Actve_Ind = pnd_Tbl_Key__R_Field_23.newFieldInGroup("pnd_Tbl_Key_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", FieldType.STRING, 1);
        pnd_Geo_Grp = localVariables.newFieldInRecord("pnd_Geo_Grp", "#GEO-GRP", FieldType.STRING, 4);

        pnd_Geo_Grp__R_Field_24 = localVariables.newGroupInRecord("pnd_Geo_Grp__R_Field_24", "REDEFINE", pnd_Geo_Grp);
        pnd_Geo_Grp_Pnd_State = pnd_Geo_Grp__R_Field_24.newFieldInGroup("pnd_Geo_Grp_Pnd_State", "#STATE", FieldType.STRING, 2);
        pnd_Geo_Grp_Pnd_Geo_Code = pnd_Geo_Grp__R_Field_24.newFieldInGroup("pnd_Geo_Grp_Pnd_Geo_Code", "#GEO-CODE", FieldType.STRING, 2);
        pnd_Ph_Key = localVariables.newFieldInRecord("pnd_Ph_Key", "#PH-KEY", FieldType.STRING, 13);

        pnd_Ph_Key__R_Field_25 = localVariables.newGroupInRecord("pnd_Ph_Key__R_Field_25", "REDEFINE", pnd_Ph_Key);
        pnd_Ph_Key_Rcrd_Type = pnd_Ph_Key__R_Field_25.newFieldInGroup("pnd_Ph_Key_Rcrd_Type", "RCRD-TYPE", FieldType.STRING, 1);
        pnd_Ph_Key_Srvvr_Mit_Idntfr = pnd_Ph_Key__R_Field_25.newFieldInGroup("pnd_Ph_Key_Srvvr_Mit_Idntfr", "SRVVR-MIT-IDNTFR", FieldType.STRING, 12);
        pnd_Mit_Read_Cntr = localVariables.newFieldInRecord("pnd_Mit_Read_Cntr", "#MIT-READ-CNTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Work_Read_Cntr = localVariables.newFieldInRecord("pnd_Work_Read_Cntr", "#WORK-READ-CNTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Wrqst_Add_Cntr = localVariables.newFieldInRecord("pnd_Wrqst_Add_Cntr", "#WRQST-ADD-CNTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Wrqst_Upd_Cntr = localVariables.newFieldInRecord("pnd_Wrqst_Upd_Cntr", "#WRQST-UPD-CNTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Cntrt_Add_Cntr = localVariables.newFieldInRecord("pnd_Cntrt_Add_Cntr", "#CNTRT-ADD-CNTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Cntrt_Upd_Cntr = localVariables.newFieldInRecord("pnd_Cntrt_Upd_Cntr", "#CNTRT-UPD-CNTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Unita_Add_Cntr = localVariables.newFieldInRecord("pnd_Unita_Add_Cntr", "#UNITA-ADD-CNTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Unita_Upd_Cntr = localVariables.newFieldInRecord("pnd_Unita_Upd_Cntr", "#UNITA-UPD-CNTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Empla_Add_Cntr = localVariables.newFieldInRecord("pnd_Empla_Add_Cntr", "#EMPLA-ADD-CNTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Empla_Upd_Cntr = localVariables.newFieldInRecord("pnd_Empla_Upd_Cntr", "#EMPLA-UPD-CNTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Stepa_Add_Cntr = localVariables.newFieldInRecord("pnd_Stepa_Add_Cntr", "#STEPA-ADD-CNTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Stepa_Upd_Cntr = localVariables.newFieldInRecord("pnd_Stepa_Upd_Cntr", "#STEPA-UPD-CNTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Extra_Add_Cntr = localVariables.newFieldInRecord("pnd_Extra_Add_Cntr", "#EXTRA-ADD-CNTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Extra_Upd_Cntr = localVariables.newFieldInRecord("pnd_Extra_Upd_Cntr", "#EXTRA-UPD-CNTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Intra_Add_Cntr = localVariables.newFieldInRecord("pnd_Intra_Add_Cntr", "#INTRA-ADD-CNTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Intra_Upd_Cntr = localVariables.newFieldInRecord("pnd_Intra_Upd_Cntr", "#INTRA-UPD-CNTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Enrta_Add_Cntr = localVariables.newFieldInRecord("pnd_Enrta_Add_Cntr", "#ENRTA-ADD-CNTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Enrta_Upd_Cntr = localVariables.newFieldInRecord("pnd_Enrta_Upd_Cntr", "#ENRTA-UPD-CNTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Del_Wrqst_Cntr = localVariables.newFieldInRecord("pnd_Del_Wrqst_Cntr", "#DEL-WRQST-CNTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Tlc_Cntr = localVariables.newFieldInRecord("pnd_Tlc_Cntr", "#TLC-CNTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Total_Rec_Cntr = localVariables.newFieldInRecord("pnd_Total_Rec_Cntr", "#TOTAL-REC-CNTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Cstat_Add_Cntr = localVariables.newFieldInRecord("pnd_Cstat_Add_Cntr", "#CSTAT-ADD-CNTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Adtnl_Wrk_Add = localVariables.newFieldInRecord("pnd_Adtnl_Wrk_Add", "#ADTNL-WRK-ADD", FieldType.PACKED_DECIMAL, 8);
        pnd_Adtnl_Wrk_Upd = localVariables.newFieldInRecord("pnd_Adtnl_Wrk_Upd", "#ADTNL-WRK-UPD", FieldType.PACKED_DECIMAL, 8);
        pnd_Late_Upd = localVariables.newFieldInRecord("pnd_Late_Upd", "#LATE-UPD", FieldType.PACKED_DECIMAL, 8);
        pnd_Late_Add = localVariables.newFieldInRecord("pnd_Late_Add", "#LATE-ADD", FieldType.PACKED_DECIMAL, 8);
        pnd_No_Base_Rec = localVariables.newFieldInRecord("pnd_No_Base_Rec", "#NO-BASE-REC", FieldType.PACKED_DECIMAL, 8);
        pnd_Vac_Address = localVariables.newFieldInRecord("pnd_Vac_Address", "#VAC-ADDRESS", FieldType.PACKED_DECIMAL, 8);
        pnd_No_Base_Rec_V = localVariables.newFieldInRecord("pnd_No_Base_Rec_V", "#NO-BASE-REC-V", FieldType.PACKED_DECIMAL, 8);
        pnd_No_State_In_Table = localVariables.newFieldInRecord("pnd_No_State_In_Table", "#NO-STATE-IN-TABLE", FieldType.PACKED_DECIMAL, 8);
        pnd_Rerun_Wr_Skip = localVariables.newFieldInRecord("pnd_Rerun_Wr_Skip", "#RERUN-WR-SKIP", FieldType.PACKED_DECIMAL, 8);
        pnd_Pass_Nfnd_Cntr = localVariables.newFieldInRecord("pnd_Pass_Nfnd_Cntr", "#PASS-NFND-CNTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Nbr_Act_Recs = localVariables.newFieldInRecord("pnd_Nbr_Act_Recs", "#NBR-ACT-RECS", FieldType.PACKED_DECIMAL, 8);
        pnd_Act_Wr_Accept = localVariables.newFieldInRecord("pnd_Act_Wr_Accept", "#ACT-WR-ACCEPT", FieldType.PACKED_DECIMAL, 8);
        pnd_Cor_Pin_Cntr = localVariables.newFieldInRecord("pnd_Cor_Pin_Cntr", "#COR-PIN-CNTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Add = localVariables.newFieldInRecord("pnd_Add", "#ADD", FieldType.BOOLEAN, 1);
        pnd_Update = localVariables.newFieldInRecord("pnd_Update", "#UPDATE", FieldType.BOOLEAN, 1);
        pnd_Foreign_Address = localVariables.newFieldInRecord("pnd_Foreign_Address", "#FOREIGN-ADDRESS", FieldType.BOOLEAN, 1);
        pnd_Status_Of_Interest = localVariables.newFieldInRecord("pnd_Status_Of_Interest", "#STATUS-OF-INTEREST", FieldType.BOOLEAN, 1);
        pnd_C_Status_Match = localVariables.newFieldInRecord("pnd_C_Status_Match", "#C-STATUS-MATCH", FieldType.BOOLEAN, 1);
        pnd_End_Date_Switch = localVariables.newFieldInRecord("pnd_End_Date_Switch", "#END-DATE-SWITCH", FieldType.BOOLEAN, 1);
        pnd_Wipe_Out_End_Act = localVariables.newFieldInRecord("pnd_Wipe_Out_End_Act", "#WIPE-OUT-END-ACT", FieldType.BOOLEAN, 1);
        pnd_Static_Sw = localVariables.newFieldInRecord("pnd_Static_Sw", "#STATIC-SW", FieldType.BOOLEAN, 1);
        pnd_Activity_Type = localVariables.newFieldInRecord("pnd_Activity_Type", "#ACTIVITY-TYPE", FieldType.STRING, 4);

        pnd_Frgn_Geo_Pass = localVariables.newGroupInRecord("pnd_Frgn_Geo_Pass", "#FRGN-GEO-PASS");
        pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_1 = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_1", "#GEO-ADDR-LINE-1", FieldType.STRING, 
            35);
        pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_2 = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_2", "#GEO-ADDR-LINE-2", FieldType.STRING, 
            35);
        pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_3 = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_3", "#GEO-ADDR-LINE-3", FieldType.STRING, 
            35);
        pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_4 = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_4", "#GEO-ADDR-LINE-4", FieldType.STRING, 
            35);
        pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_5 = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_5", "#GEO-ADDR-LINE-5", FieldType.STRING, 
            35);
        pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_6 = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_Geo_Addr_Line_6", "#GEO-ADDR-LINE-6", FieldType.STRING, 
            35);
        pnd_Frgn_Geo_Pass_Pnd_Return_Cde = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_Return_Cde", "#RETURN-CDE", FieldType.NUMERIC, 1);
        pnd_Frgn_Geo_Pass_Pnd_Mail_Cde = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_Mail_Cde", "#MAIL-CDE", FieldType.STRING, 1);
        pnd_Frgn_Geo_Pass_Pnd_Geo_Cde = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_Geo_Cde", "#GEO-CDE", FieldType.STRING, 2);
        pnd_Frgn_Geo_Pass_Pnd_St_Cde1 = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_St_Cde1", "#ST-CDE1", FieldType.STRING, 2);
        pnd_Frgn_Geo_Pass_Pnd_St_Cde2 = pnd_Frgn_Geo_Pass.newFieldInGroup("pnd_Frgn_Geo_Pass_Pnd_St_Cde2", "#ST-CDE2", FieldType.STRING, 2);

        vw_cwf_Efm_Cabinet = new DataAccessProgramView(new NameInfo("vw_cwf_Efm_Cabinet", "CWF-EFM-CABINET"), "CWF_EFM_CABINET", "CWF_EFM_CABINET");
        cwf_Efm_Cabinet_Cabinet_Id = vw_cwf_Efm_Cabinet.getRecord().newFieldInGroup("cwf_Efm_Cabinet_Cabinet_Id", "CABINET-ID", FieldType.STRING, 14, 
            RepeatingFieldStrategy.None, "CABINET_ID");
        cwf_Efm_Cabinet_Cabinet_Id.setDdmHeader("CABINET/ID");

        cwf_Efm_Cabinet__R_Field_26 = vw_cwf_Efm_Cabinet.getRecord().newGroupInGroup("cwf_Efm_Cabinet__R_Field_26", "REDEFINE", cwf_Efm_Cabinet_Cabinet_Id);
        cwf_Efm_Cabinet_Pnd_Policy_Holder = cwf_Efm_Cabinet__R_Field_26.newFieldInGroup("cwf_Efm_Cabinet_Pnd_Policy_Holder", "#POLICY-HOLDER", FieldType.STRING, 
            1);
        cwf_Efm_Cabinet_Pnd_Pin_Nbr = cwf_Efm_Cabinet__R_Field_26.newFieldInGroup("cwf_Efm_Cabinet_Pnd_Pin_Nbr", "#PIN-NBR", FieldType.NUMERIC, 12);
        cwf_Efm_Cabinet_Active_Ind = vw_cwf_Efm_Cabinet.getRecord().newFieldInGroup("cwf_Efm_Cabinet_Active_Ind", "ACTIVE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ACTIVE_IND");
        cwf_Efm_Cabinet_Active_Ind.setDdmHeader("ACTV/IND");
        cwf_Efm_Cabinet_Media_Ind = vw_cwf_Efm_Cabinet.getRecord().newFieldInGroup("cwf_Efm_Cabinet_Media_Ind", "MEDIA-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "MEDIA_IND");
        cwf_Efm_Cabinet_Media_Ind.setDdmHeader("MEDIA IND");
        registerRecord(vw_cwf_Efm_Cabinet);

        pnd_Pin_Nbr_A12 = localVariables.newFieldInRecord("pnd_Pin_Nbr_A12", "#PIN-NBR-A12", FieldType.STRING, 12);

        pnd_Pin_Nbr_A12__R_Field_27 = localVariables.newGroupInRecord("pnd_Pin_Nbr_A12__R_Field_27", "REDEFINE", pnd_Pin_Nbr_A12);
        pnd_Pin_Nbr_A12_Pnd_Pin_Nbr_1_5 = pnd_Pin_Nbr_A12__R_Field_27.newFieldInGroup("pnd_Pin_Nbr_A12_Pnd_Pin_Nbr_1_5", "#PIN-NBR-1-5", FieldType.NUMERIC, 
            5);
        pnd_Pin_Nbr_A12_Pnd_Pin_Nbr_6_12 = pnd_Pin_Nbr_A12__R_Field_27.newFieldInGroup("pnd_Pin_Nbr_A12_Pnd_Pin_Nbr_6_12", "#PIN-NBR-6-12", FieldType.NUMERIC, 
            7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        setTimeRecord = new DbsRecord();
        st = setTimeRecord.newFieldInRecord("ST", "ST", FieldType.NUMERIC, 7);
        registerRecord(setTimeRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Master_View_Select.reset();
        vw_cwf_Sb_Ph.reset();
        vw_cwf_Efm_Cabinet.reset();

        ldaCwfl4817.initializeValues();
        ldaCwfl4818.initializeValues();
        ldaCwfl4815.initializeValues();
        ldaCwfl4819.initializeValues();
        ldaCwfl4899.initializeValues();
        ldaCwfl4820.initializeValues();
        ldaCwfl4821.initializeValues();
        ldaCwfl4822.initializeValues();
        ldaCwfl4823.initializeValues();
        ldaCwfl4824.initializeValues();
        ldaCwfl4828.initializeValues();
        ldaCwfl4829.initializeValues();
        ldaCwfl4830.initializeValues();

        localVariables.reset();
        pnd_Atsign.setInitialValue("@");
        pnd_Static_Dte_Tme_A_15.setInitialValue("193001010000000");
        pnd_Pin_Base_Key.setInitialValue("            B");
        pnd_Cab_Key.setInitialValue("P");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb4815() throws Exception
    {
        super("Cwfb4815");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("CWFB4815", onError);
        setupReports();
        //*  ----------- INITIALIZATION ---------------------
        //*  ----------- MAIN LOGIC -------------------------                                                                                                             //Natural: FORMAT ( 0 ) PS = 56 LS = 131;//Natural: FORMAT ( 1 ) PS = 56 LS = 131
        //*   TO ALLOW FOR EASY ESCAPE FROM PROGRAM.
        REP1:                                                                                                                                                             //Natural: REPEAT
        while (condition(whileTrue))
        {
            st = Global.getTIMN();                                                                                                                                        //Natural: SET TIME
                                                                                                                                                                          //Natural: PERFORM READ-RUN-CONTROL
            sub_Read_Run_Control();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM SELECT-MASTER-RECORDS
            sub_Select_Master_Records();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-TOTALS
            sub_Write_Totals();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTROL
            sub_Update_Control();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM ESCAPE-PROGRAM
            sub_Escape_Program();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("REP1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ESCAPE-PROGRAM
            //*   (REP1.)
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  ================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-RUN-CONTROL
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SELECT-MASTER-RECORDS
        //* **************************************
        //*  --------------
        //*  --------------
        //*  --------------
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-IF-ADD-OR-UPDATE
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-STATUS-RANGES
        //* ************************************
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ROUTE-TBL-LOOK-UP
        //* **********************************
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-STATUS-RANGES-C
        //* **************************************
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WORK-REQUEST
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FILL-WORK-REQUEST
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-WR-DATES
        //*     (EM='"'LLL�DD�YYYY�HH':'II':'SSAP'"')
        //*  ------------- DEFINE ADJUSTED NEXT BUSINESS DAY --------
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-BUSINESS-DATA
        //* **********************************
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-PART-AGE
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-ACTIVITY
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FILL-ACTIVITY
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DERIVE-INDICATORS
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-DIVISION
        //* *************
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-MEDIA-IND
        //* ******************************
        //* *************
        //* **************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-WPID
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TOTALS
        //*  'Number of Status Activities Added               = ' #STATA-ADD-CNTR
        //*  'Number of Status Activities Updated             = ' #STATA-UPD-CNTR
        //*  'COR PIN file created - Number of records        = ' #COR-PIN-CNTR
        //*  COR/NAAD SUNSET START
        //*  ********************************
        //*  DEFINE SUBROUTINE CHECK-TLC-CODE
        //*  ********************************
        //*  WRITE / '************ LOOKING FOR TLC CODE PIN = ' CWF-MASTER.PIN-NBR
        //*  READ COR-XREF-PH   BY COR-SUPER-PIN-RCDTYPE
        //*     STARTING FROM  #COR-SUPER-PIN-RCDTYPE
        //*   IF COR-XREF-PH.PH-UNIQUE-ID-NBR NE CWF-MASTER.PIN-NBR
        //*     ESCAPE ROUTINE
        //*   END-IF
        //*   FOR #I 1 TO 8
        //*     IF PH-ACTVE-VIP-CDE(#I) GT ' '
        //*       PERFORM GET-TLC-DESCRIPTION
        //*     END-IF
        //*   END-FOR
        //*  END-READ
        //*  END-SUBROUTINE     /* CHECK TLC CODE
        //*  *************************************
        //*  DEFINE SUBROUTINE GET-TLC-DESCRIPTION
        //*  *************************************
        //*  WRITE / '************ GET TLC DESCRIOTION ***********' #I
        //*  RESET #TABLE-ID-ENTRY-CDE
        //*  #TABLE-ID-ENTRY-CDE.ENTRY-TABLE-ID-NBR := '000018'
        //*  READ TABLE-ENTRY BY TABLE-ID-ENTRY-CDE
        //*     STARTING FROM #TABLE-ID-ENTRY-CDE
        //*   IF TABLE-ENTRY.ENTRY-TABLE-ID-NBR NE 18
        //*     ESCAPE ROUTINE
        //*   END-IF
        //*   IF #ENTRY-CDE-2 = PH-ACTVE-VIP-CDE(#I)
        //*     MOVE  CWF-MASTER.PIN-NBR   TO TLCTABLE.CUST-CODE
        //*     MOVE  PH-ACTVE-VIP-CDE(#I) TO TLCTABLE.TLC-CODE
        //*     MOVE  ENTRY-DSCRPTN-TXT    TO TLCTABLE.TLC-DESCRIPTION
        //*     MOVE  #ATSIGN TO D1 D2
        //*     WRITE WORK FILE 23 TLCTABLE
        //*     ADD 1 TO #TLC-CNTR
        //*     ESCAPE ROUTINE
        //*   END-IF
        //*  END-READ
        //*  END-SUBROUTINE         /* GET TLC DESCRIPTION
        //*  COR/NAAD SUNSET END
        //* *************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-NAME
        //* *************************
        //* *************
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FILL-N-WRITE-CONTRACT
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DELETE-CWF-CORP-RPT-WR
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-PARENT-REC
        //* *******************************
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-DOD-NOT-INFO
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-CONTROL
        //*  CWF-SUPPORT-TBL.RUN-STATUS                  := 'C'
        //*  F-SUPPORT-TBL.NUMBER-OF-STAT-ACT-ADDED    := #STATA-ADD-CNTR
        //*  F-SUPPORT-TBL.NUMBER-OF-STAT-ACT-UPDTD    := #STATA-UPD-CNTR
        //* *******************************                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Escape_Program() throws Exception                                                                                                                    //Natural: ESCAPE-PROGRAM
    {
        if (BLNatReinput.isReinput()) return;

        //*  ================================
        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( REP1. )
        Global.setEscapeCode(EscapeType.Bottom, "REP1");
        if (true) return;
    }
    private void sub_Read_Run_Control() throws Exception                                                                                                                  //Natural: READ-RUN-CONTROL
    {
        if (BLNatReinput.isReinput()) return;

        //*  ================================
        //*  CALL THE SUB PROGRAM TO FIND "Acitive" RUN CONTROL RECORD WHICH HAS
        //*  BEEN ESTABLISH IN A PREVIOUS STEP.
        //*  ---------------------------------------------------------------------
        DbsUtil.callnat(Cwfn4816.class , getCurrentProcessState(), pdaCwfa4815.getCwfa4815_Output(), pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(),             //Natural: CALLNAT 'CWFN4816' CWFA4815-OUTPUT CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
            pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("E") || pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("W")))                    //Natural: IF MSG-INFO-SUB.##RETURN-CODE = 'E' OR MSG-INFO-SUB.##RETURN-CODE = 'W'
        {
            getReports().write(0, ReportOption.NOTITLE,new TabSetting(10),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                     //Natural: WRITE 10T MSG-INFO-SUB.##MSG
            if (Global.isEscape()) return;
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("E")))                                                                                 //Natural: IF MSG-INFO-SUB.##RETURN-CODE = 'E'
            {
                DbsUtil.terminate(10);  if (true) return;                                                                                                                 //Natural: TERMINATE 10
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().equals(999)))                                                                                          //Natural: IF MSG-INFO-SUB.##MSG-NR = 999
        {
            getReports().write(0, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(10),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                  //Natural: WRITE ( 0 ) NOTITLE NOHDR 10T MSG-INFO-SUB.##MSG
            if (Global.isEscape()) return;
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
            DbsUtil.terminate(11);  if (true) return;                                                                                                                     //Natural: TERMINATE 11
        }                                                                                                                                                                 //Natural: END-IF
        //*  ------------------------------------------
        GET1:                                                                                                                                                             //Natural: GET CWF-SUPPORT-TBL CWFA4815-OUTPUT.RUN-CONTROL-ISN
        ldaCwfl4815.getVw_cwf_Support_Tbl().readByID(pdaCwfa4815.getCwfa4815_Output_Run_Control_Isn().getLong(), "GET1");
        pnd_Tbl_Start_Dte_Tme_T.setValue(ldaCwfl4815.getCwf_Support_Tbl_Starting_Date());                                                                                 //Natural: MOVE CWF-SUPPORT-TBL.STARTING-DATE TO #TBL-START-DTE-TME-T
        pnd_Tbl_End_Dte_Tme_T.setValue(ldaCwfl4815.getCwf_Support_Tbl_Ending_Date());                                                                                     //Natural: MOVE CWF-SUPPORT-TBL.ENDING-DATE TO #TBL-END-DTE-TME-T
        //*  WRITE /'CWFB4815 - CONT ISN 1-TIME' CWFA4815-OUTPUT.RUN-CONTROL-ISN
    }
    private void sub_Select_Master_Records() throws Exception                                                                                                             //Natural: SELECT-MASTER-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        R1:                                                                                                                                                               //Natural: READ WORK FILE 1 #WORK-FILE-1
        while (condition(getWorkFiles().read(1, pnd_Work_File_1)))
        {
            //*  --------------------- CHECK RQST-LOG-DTE-TME -----------------
            if (condition(! (DbsUtil.maskMatches(pnd_Work_File_1_Pnd_Rqst_Log_Dte_Tme,"YYYYMMDD00-2400-6000-60N"))))                                                      //Natural: IF #WORK-FILE-1.#RQST-LOG-DTE-TME NE MASK ( YYYYMMDD00-2400-6000-60N )
            {
                //*       WRITE /'WORK-FILE.RQST-LOG-DTE-TME HAS WRONG VALUE = '
                //*                         #WORK-FILE-1.#RQST-LOG-DTE-TME
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Work_Read_Cntr.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #WORK-READ-CNTR
            ldaCwfl4817.getVw_cwf_Master().startDatabaseRead                                                                                                              //Natural: READ CWF-MASTER BY RQST-HISTORY-KEY STARTING FROM #WORK-FILE-1.#RQST-LOG-DTE-TME
            (
            "R2",
            new Wc[] { new Wc("RQST_HISTORY_KEY", ">=", pnd_Work_File_1_Pnd_Rqst_Log_Dte_Tme, WcType.BY) },
            new Oc[] { new Oc("RQST_HISTORY_KEY", "ASC") }
            );
            R2:
            while (condition(ldaCwfl4817.getVw_cwf_Master().readNextRow("R2")))
            {
                //*  --- AVOID PROCESSING RECORD FOR THE FOLLOWING REASONS -----
                if (condition(ldaCwfl4817.getCwf_Master_Rqst_Log_Dte_Tme().notEquals(pnd_Work_File_1_Pnd_Rqst_Log_Dte_Tme)))                                              //Natural: IF CWF-MASTER.RQST-LOG-DTE-TME NE #WORK-FILE-1.#RQST-LOG-DTE-TME
                {
                    if (true) break R2;                                                                                                                                   //Natural: ESCAPE BOTTOM ( R2. ) IMMEDIATE
                }                                                                                                                                                         //Natural: END-IF
                pnd_Last_Chnge_Dte_Tme_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaCwfl4817.getCwf_Master_Last_Chnge_Dte_Tme_A());                          //Natural: MOVE EDITED CWF-MASTER.LAST-CHNGE-DTE-TME-A TO #LAST-CHNGE-DTE-TME-T ( EM = YYYYMMDDHHIISST )
                if (condition(pnd_Last_Chnge_Dte_Tme_T.less(ldaCwfl4815.getCwf_Support_Tbl_Starting_Date()) && ldaCwfl4817.getCwf_Master_Last_Updte_Dte_Tme().less(ldaCwfl4815.getCwf_Support_Tbl_Starting_Date()))) //Natural: IF #LAST-CHNGE-DTE-TME-T LT CWF-SUPPORT-TBL.STARTING-DATE AND CWF-MASTER.LAST-UPDTE-DTE-TME LT CWF-SUPPORT-TBL.STARTING-DATE
                {
                    if (true) break R2;                                                                                                                                   //Natural: ESCAPE BOTTOM ( R2. ) IMMEDIATE
                }                                                                                                                                                         //Natural: END-IF
                //*  --- AVOID PROCESSING RECORD IF RE-SCAN-IND = 'Y' ---------- 11/07/00
                if (condition(ldaCwfl4817.getCwf_Master_Rescan_Ind().equals("Y")))                                                                                        //Natural: IF RESCAN-IND = 'Y'
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*  ----------------------------------------------------------------
                pnd_Rqst_Log_Dte_Tme_Save.setValue(ldaCwfl4817.getCwf_Master_Rqst_Log_Dte_Tme());                                                                         //Natural: MOVE CWF-MASTER.RQST-LOG-DTE-TME TO #RQST-LOG-DTE-TME-SAVE
                pnd_Pin_Number_Save.setValue(ldaCwfl4817.getCwf_Master_Pin_Nbr());                                                                                        //Natural: MOVE CWF-MASTER.PIN-NBR TO #PIN-NUMBER-SAVE
                pnd_Isn.setValue(ldaCwfl4817.getVw_cwf_Master().getAstISN("R2"));                                                                                         //Natural: ASSIGN #ISN := *ISN
                pnd_Mit_Read_Cntr.nadd(1);                                                                                                                                //Natural: ADD 1 TO #MIT-READ-CNTR
                if (condition(ldaCwfl4817.getCwf_Master_Admin_Status_Cde().equals("MERG")))                                                                               //Natural: IF CWF-MASTER.ADMIN-STATUS-CDE = 'MERG'
                {
                                                                                                                                                                          //Natural: PERFORM DELETE-CWF-CORP-RPT-WR
                    sub_Delete_Cwf_Corp_Rpt_Wr();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R2"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*    ESCAPE TOP                             /* COMMENTED ON 07/20/99
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaCwfl4817.getCwf_Master_Actve_Ind().equals("A")))                                                                                         //Natural: IF CWF-MASTER.ACTVE-IND = 'A'
                {
                    pnd_Nbr_Act_Recs.nadd(1);                                                                                                                             //Natural: ADD 1 TO #NBR-ACT-RECS
                    pnd_Act_Strt_Time_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaCwfl4817.getCwf_Master_Rqst_Log_Dte_Tme());                               //Natural: MOVE EDITED CWF-MASTER.RQST-LOG-DTE-TME TO #ACT-STRT-TIME-T ( EM = YYYYMMDDHHIISST )
                    pnd_Add.setValue(false);                                                                                                                              //Natural: MOVE FALSE TO #ADD #UPDATE
                    pnd_Update.setValue(false);
                    //* *****
                    short decideConditionsMet2242 = 0;                                                                                                                    //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CWF-SUPPORT-TBL.RERUN-FLAG = 'Y'
                    if (condition(ldaCwfl4815.getCwf_Support_Tbl_Rerun_Flag().equals("Y")))
                    {
                        decideConditionsMet2242++;
                        pnd_Last_Chnge_Dte_Tme_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaCwfl4817.getCwf_Master_Last_Chnge_Dte_Tme_A());                  //Natural: MOVE EDITED CWF-MASTER.LAST-CHNGE-DTE-TME-A TO #LAST-CHNGE-DTE-TME-T ( EM = YYYYMMDDHHIISST )
                        if (condition(pnd_Last_Chnge_Dte_Tme_T.lessOrEqual(pnd_Tbl_End_Dte_Tme_T)))                                                                       //Natural: IF #LAST-CHNGE-DTE-TME-T LE #TBL-END-DTE-TME-T
                        {
                            pnd_Act_Wr_Accept.nadd(1);                                                                                                                    //Natural: ADD 1 TO #ACT-WR-ACCEPT
                            pnd_Update.setValue(true);                                                                                                                    //Natural: MOVE TRUE TO #UPDATE
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Rerun_Wr_Skip.nadd(1);                                                                                                                    //Natural: ADD 1 TO #RERUN-WR-SKIP
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: WHEN #ACT-STRT-TIME-T LT #TBL-START-DTE-TME-T
                    else if (condition(pnd_Act_Strt_Time_T.less(pnd_Tbl_Start_Dte_Tme_T)))
                    {
                        decideConditionsMet2242++;
                        pnd_Update.setValue(true);                                                                                                                        //Natural: MOVE TRUE TO #UPDATE
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        pnd_Add.setValue(true);                                                                                                                           //Natural: MOVE TRUE TO #ADD
                    }                                                                                                                                                     //Natural: END-DECIDE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-REQUEST
                    sub_Write_Work_Request();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R2"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  ------------- CHECKING CLOCKS ------------
                //*  CHECK FOR VALUE
                short decideConditionsMet2260 = 0;                                                                                                                        //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN CWF-MASTER.UNIT-CLOCK-START-DTE-TME = MASK ( YYYYMMDD00-2400-6000-60N )
                if (condition(DbsUtil.maskMatches(ldaCwfl4817.getCwf_Master_Unit_Clock_Start_Dte_Tme(),"YYYYMMDD00-2400-6000-60N")))
                {
                    decideConditionsMet2260++;
                    ldaCwfl4820.getActivity().reset();                                                                                                                    //Natural: RESET ACTIVITY
                    pnd_Act_Strt_Time_T.reset();                                                                                                                          //Natural: RESET #ACT-STRT-TIME-T #ACT-STOP-TIME-T
                    pnd_Act_Stop_Time_T.reset();
                    pnd_Act_Prcss_Tme.reset();                                                                                                                            //Natural: RESET #ACT-PRCSS-TME #ACT-WAIT-TME
                    pnd_Act_Wait_Tme.reset();
                    pnd_Activity_Type.setValue("UNIT");                                                                                                                   //Natural: MOVE 'UNIT' TO #ACTIVITY-TYPE
                    if (condition(ldaCwfl4817.getCwf_Master_Unit_Clock_Start_Dte_Tme().equals("999999999999999") || ldaCwfl4817.getCwf_Master_Unit_Clock_Start_Dte_Tme().equals(" "))) //Natural: IF CWF-MASTER.UNIT-CLOCK-START-DTE-TME = '999999999999999' OR CWF-MASTER.UNIT-CLOCK-START-DTE-TME = ' '
                    {
                        pnd_Act_Strt_Time_T.setValue(0);                                                                                                                  //Natural: MOVE 0 TO #ACT-STRT-TIME-T
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Act_Strt_Time_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaCwfl4817.getCwf_Master_Unit_Clock_Start_Dte_Tme());                   //Natural: MOVE EDITED CWF-MASTER.UNIT-CLOCK-START-DTE-TME TO #ACT-STRT-TIME-T ( EM = YYYYMMDDHHIISST )
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaCwfl4817.getCwf_Master_Unit_Clock_End_Dte_Tme().equals("999999999999999") || ldaCwfl4817.getCwf_Master_Unit_Clock_End_Dte_Tme().equals(" "))) //Natural: IF CWF-MASTER.UNIT-CLOCK-END-DTE-TME = '999999999999999' OR CWF-MASTER.UNIT-CLOCK-END-DTE-TME = ' '
                    {
                        pnd_Act_Stop_Time_T.setValue(0);                                                                                                                  //Natural: MOVE 0 TO #ACT-STOP-TIME-T
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(DbsUtil.maskMatches(ldaCwfl4817.getCwf_Master_Unit_Clock_End_Dte_Tme(),"YYYYMMDD00-2400-6000-60N")))                                //Natural: IF CWF-MASTER.UNIT-CLOCK-END-DTE-TME = MASK ( YYYYMMDD00-2400-6000-60N )
                        {
                            pnd_Act_Stop_Time_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaCwfl4817.getCwf_Master_Unit_Clock_End_Dte_Tme());                 //Natural: MOVE EDITED CWF-MASTER.UNIT-CLOCK-END-DTE-TME TO #ACT-STOP-TIME-T ( EM = YYYYMMDDHHIISST )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaCwfl4817.getCwf_Master_Unit_Elpsd_Clndr_Days_Tme().greater(" ")))                                                                    //Natural: IF CWF-MASTER.UNIT-ELPSD-CLNDR-DAYS-TME GT ' '
                    {
                        ldaCwfl4820.getActivity_Wait_Time().setValue(ldaCwfl4817.getCwf_Master_Unit_Elpsd_Clndr_Days_Tme());                                              //Natural: MOVE CWF-MASTER.UNIT-ELPSD-CLNDR-DAYS-TME TO ACTIVITY.WAIT-TIME
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaCwfl4817.getCwf_Master_Unit_Elpsd_Bsnss_Days_Tme().greater(" ")))                                                                    //Natural: IF CWF-MASTER.UNIT-ELPSD-BSNSS-DAYS-TME GT ' '
                    {
                        ldaCwfl4820.getActivity_Prcss_Time().setValue(ldaCwfl4817.getCwf_Master_Unit_Elpsd_Bsnss_Days_Tme());                                             //Natural: MOVE CWF-MASTER.UNIT-ELPSD-BSNSS-DAYS-TME TO ACTIVITY.PRCSS-TIME
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-ADD-OR-UPDATE
                    sub_Check_If_Add_Or_Update();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R2"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_Add.getBoolean() || pnd_Update.getBoolean()))                                                                                       //Natural: IF #ADD OR #UPDATE
                    {
                                                                                                                                                                          //Natural: PERFORM WRITE-ACTIVITY
                        sub_Write_Activity();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R2"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  CHECK FOR VALUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN CWF-MASTER.EMPL-CLOCK-START-DTE-TME = MASK ( YYYYMMDD00-2400-6000-60N )
                if (condition(DbsUtil.maskMatches(ldaCwfl4817.getCwf_Master_Empl_Clock_Start_Dte_Tme(),"YYYYMMDD00-2400-6000-60N")))
                {
                    decideConditionsMet2260++;
                    ldaCwfl4820.getActivity().reset();                                                                                                                    //Natural: RESET ACTIVITY
                    pnd_Act_Strt_Time_T.reset();                                                                                                                          //Natural: RESET #ACT-STRT-TIME-T #ACT-STOP-TIME-T
                    pnd_Act_Stop_Time_T.reset();
                    pnd_Act_Prcss_Tme.reset();                                                                                                                            //Natural: RESET #ACT-PRCSS-TME #ACT-WAIT-TME
                    pnd_Act_Wait_Tme.reset();
                    pnd_Activity_Type.setValue("EMPL");                                                                                                                   //Natural: MOVE 'EMPL' TO #ACTIVITY-TYPE
                    if (condition(ldaCwfl4817.getCwf_Master_Empl_Clock_Start_Dte_Tme().equals("999999999999999") || ldaCwfl4817.getCwf_Master_Empl_Clock_Start_Dte_Tme().equals(" "))) //Natural: IF CWF-MASTER.EMPL-CLOCK-START-DTE-TME = '999999999999999' OR CWF-MASTER.EMPL-CLOCK-START-DTE-TME = ' '
                    {
                        pnd_Act_Strt_Time_T.setValue(0);                                                                                                                  //Natural: MOVE 0 TO #ACT-STRT-TIME-T
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Act_Strt_Time_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaCwfl4817.getCwf_Master_Empl_Clock_Start_Dte_Tme());                   //Natural: MOVE EDITED CWF-MASTER.EMPL-CLOCK-START-DTE-TME TO #ACT-STRT-TIME-T ( EM = YYYYMMDDHHIISST )
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaCwfl4817.getCwf_Master_Empl_Clock_End_Dte_Tme().equals("999999999999999") || ldaCwfl4817.getCwf_Master_Empl_Clock_End_Dte_Tme().equals(" "))) //Natural: IF CWF-MASTER.EMPL-CLOCK-END-DTE-TME = '999999999999999' OR CWF-MASTER.EMPL-CLOCK-END-DTE-TME = ' '
                    {
                        pnd_Act_Stop_Time_T.setValue(0);                                                                                                                  //Natural: MOVE 0 TO #ACT-STOP-TIME-T
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(DbsUtil.maskMatches(ldaCwfl4817.getCwf_Master_Empl_Clock_End_Dte_Tme(),"YYYYMMDD00-2400-6000-60N")))                                //Natural: IF CWF-MASTER.EMPL-CLOCK-END-DTE-TME = MASK ( YYYYMMDD00-2400-6000-60N )
                        {
                            pnd_Act_Stop_Time_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaCwfl4817.getCwf_Master_Empl_Clock_End_Dte_Tme());                 //Natural: MOVE EDITED CWF-MASTER.EMPL-CLOCK-END-DTE-TME TO #ACT-STOP-TIME-T ( EM = YYYYMMDDHHIISST )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaCwfl4817.getCwf_Master_Empl_Elpsd_Clndr_Days_Tme().greater(" ")))                                                                    //Natural: IF CWF-MASTER.EMPL-ELPSD-CLNDR-DAYS-TME GT ' '
                    {
                        ldaCwfl4820.getActivity_Wait_Time().setValue(ldaCwfl4817.getCwf_Master_Empl_Elpsd_Clndr_Days_Tme());                                              //Natural: MOVE CWF-MASTER.EMPL-ELPSD-CLNDR-DAYS-TME TO ACTIVITY.WAIT-TIME
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaCwfl4817.getCwf_Master_Empl_Elpsd_Bsnss_Days_Tme().greater(" ")))                                                                    //Natural: IF CWF-MASTER.EMPL-ELPSD-BSNSS-DAYS-TME GT ' '
                    {
                        ldaCwfl4820.getActivity_Prcss_Time().setValue(ldaCwfl4817.getCwf_Master_Empl_Elpsd_Bsnss_Days_Tme());                                             //Natural: MOVE CWF-MASTER.EMPL-ELPSD-BSNSS-DAYS-TME TO ACTIVITY.PRCSS-TIME
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-ADD-OR-UPDATE
                    sub_Check_If_Add_Or_Update();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R2"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_Add.getBoolean() || pnd_Update.getBoolean()))                                                                                       //Natural: IF #ADD OR #UPDATE
                    {
                                                                                                                                                                          //Natural: PERFORM WRITE-ACTIVITY
                        sub_Write_Activity();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R2"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  CHECK FOR VALUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN CWF-MASTER.STEP-CLOCK-START-DTE-TME = MASK ( YYYYMMDD00-2400-6000-60N )
                if (condition(DbsUtil.maskMatches(ldaCwfl4817.getCwf_Master_Step_Clock_Start_Dte_Tme(),"YYYYMMDD00-2400-6000-60N")))
                {
                    decideConditionsMet2260++;
                    ldaCwfl4820.getActivity().reset();                                                                                                                    //Natural: RESET ACTIVITY
                    pnd_Act_Strt_Time_T.reset();                                                                                                                          //Natural: RESET #ACT-STRT-TIME-T #ACT-STOP-TIME-T
                    pnd_Act_Stop_Time_T.reset();
                    pnd_Act_Prcss_Tme.reset();                                                                                                                            //Natural: RESET #ACT-PRCSS-TME #ACT-WAIT-TME
                    pnd_Act_Wait_Tme.reset();
                    pnd_Activity_Type.setValue("STEP");                                                                                                                   //Natural: MOVE 'STEP' TO #ACTIVITY-TYPE
                    if (condition(ldaCwfl4817.getCwf_Master_Step_Clock_Start_Dte_Tme().equals("999999999999999") || ldaCwfl4817.getCwf_Master_Step_Clock_Start_Dte_Tme().equals(" "))) //Natural: IF CWF-MASTER.STEP-CLOCK-START-DTE-TME = '999999999999999' OR CWF-MASTER.STEP-CLOCK-START-DTE-TME = ' '
                    {
                        pnd_Act_Strt_Time_T.setValue(0);                                                                                                                  //Natural: MOVE 0 TO #ACT-STRT-TIME-T
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Act_Strt_Time_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaCwfl4817.getCwf_Master_Step_Clock_Start_Dte_Tme());                   //Natural: MOVE EDITED CWF-MASTER.STEP-CLOCK-START-DTE-TME TO #ACT-STRT-TIME-T ( EM = YYYYMMDDHHIISST )
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaCwfl4817.getCwf_Master_Step_Clock_End_Dte_Tme().equals("999999999999999") || ldaCwfl4817.getCwf_Master_Step_Clock_End_Dte_Tme().equals(" "))) //Natural: IF CWF-MASTER.STEP-CLOCK-END-DTE-TME = '999999999999999' OR CWF-MASTER.STEP-CLOCK-END-DTE-TME = ' '
                    {
                        pnd_Act_Stop_Time_T.setValue(0);                                                                                                                  //Natural: MOVE 0 TO #ACT-STOP-TIME-T
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(DbsUtil.maskMatches(ldaCwfl4817.getCwf_Master_Step_Clock_End_Dte_Tme(),"YYYYMMDD00-2400-6000-60N")))                                //Natural: IF CWF-MASTER.STEP-CLOCK-END-DTE-TME = MASK ( YYYYMMDD00-2400-6000-60N )
                        {
                            pnd_Act_Stop_Time_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaCwfl4817.getCwf_Master_Step_Clock_End_Dte_Tme());                 //Natural: MOVE EDITED CWF-MASTER.STEP-CLOCK-END-DTE-TME TO #ACT-STOP-TIME-T ( EM = YYYYMMDDHHIISST )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaCwfl4817.getCwf_Master_Step_Elpsd_Clndr_Days_Tme().greater(" ")))                                                                    //Natural: IF CWF-MASTER.STEP-ELPSD-CLNDR-DAYS-TME GT ' '
                    {
                        ldaCwfl4820.getActivity_Wait_Time().setValue(ldaCwfl4817.getCwf_Master_Step_Elpsd_Clndr_Days_Tme());                                              //Natural: MOVE CWF-MASTER.STEP-ELPSD-CLNDR-DAYS-TME TO ACTIVITY.WAIT-TIME
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaCwfl4817.getCwf_Master_Step_Elpsd_Bsnss_Days_Tme().greater(" ")))                                                                    //Natural: IF CWF-MASTER.STEP-ELPSD-BSNSS-DAYS-TME GT ' '
                    {
                        ldaCwfl4820.getActivity_Prcss_Time().setValue(ldaCwfl4817.getCwf_Master_Step_Elpsd_Bsnss_Days_Tme());                                             //Natural: MOVE CWF-MASTER.STEP-ELPSD-BSNSS-DAYS-TME TO ACTIVITY.PRCSS-TIME
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-ADD-OR-UPDATE
                    sub_Check_If_Add_Or_Update();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R2"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_Add.getBoolean() || pnd_Update.getBoolean()))                                                                                       //Natural: IF #ADD OR #UPDATE
                    {
                                                                                                                                                                          //Natural: PERFORM WRITE-ACTIVITY
                        sub_Write_Activity();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R2"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  CHECK FOR VALUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN CWF-MASTER.STATUS-CLOCK-START-DTE-TME = MASK ( YYYYMMDD00-2400-6000-60N )
                if (condition(DbsUtil.maskMatches(ldaCwfl4817.getCwf_Master_Status_Clock_Start_Dte_Tme(),"YYYYMMDD00-2400-6000-60N")))
                {
                    decideConditionsMet2260++;
                    ldaCwfl4820.getActivity().reset();                                                                                                                    //Natural: RESET ACTIVITY
                    pnd_Activity_Type.reset();                                                                                                                            //Natural: RESET #ACTIVITY-TYPE
                                                                                                                                                                          //Natural: PERFORM CHECK-STATUS-RANGES
                    sub_Check_Status_Ranges();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R2"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_Status_Of_Interest.getBoolean()))                                                                                                   //Natural: IF #STATUS-OF-INTEREST
                    {
                        pnd_Act_Strt_Time_T.reset();                                                                                                                      //Natural: RESET #ACT-STRT-TIME-T #ACT-STOP-TIME-T
                        pnd_Act_Stop_Time_T.reset();
                        pnd_Act_Prcss_Tme.reset();                                                                                                                        //Natural: RESET #ACT-PRCSS-TME #ACT-WAIT-TME
                        pnd_Act_Wait_Tme.reset();
                        if (condition(ldaCwfl4817.getCwf_Master_Status_Clock_Start_Dte_Tme().equals("999999999999999") || ldaCwfl4817.getCwf_Master_Status_Clock_Start_Dte_Tme().equals(" "))) //Natural: IF CWF-MASTER.STATUS-CLOCK-START-DTE-TME = '999999999999999' OR CWF-MASTER.STATUS-CLOCK-START-DTE-TME = ' '
                        {
                            pnd_Act_Strt_Time_T.setValue(0);                                                                                                              //Natural: MOVE 0 TO #ACT-STRT-TIME-T
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Act_Strt_Time_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaCwfl4817.getCwf_Master_Status_Clock_Start_Dte_Tme());             //Natural: MOVE EDITED CWF-MASTER.STATUS-CLOCK-START-DTE-TME TO #ACT-STRT-TIME-T ( EM = YYYYMMDDHHIISST )
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaCwfl4817.getCwf_Master_Status_Clock_End_Dte_Tme().equals("999999999999999") || ldaCwfl4817.getCwf_Master_Status_Clock_End_Dte_Tme().equals(" "))) //Natural: IF CWF-MASTER.STATUS-CLOCK-END-DTE-TME = '999999999999999' OR CWF-MASTER.STATUS-CLOCK-END-DTE-TME = ' '
                        {
                            pnd_Act_Stop_Time_T.setValue(0);                                                                                                              //Natural: MOVE 0 TO #ACT-STOP-TIME-T
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(DbsUtil.maskMatches(ldaCwfl4817.getCwf_Master_Status_Clock_End_Dte_Tme(),"YYYYMMDD00-2400-6000-60N")))                          //Natural: IF CWF-MASTER.STATUS-CLOCK-END-DTE-TME = MASK ( YYYYMMDD00-2400-6000-60N )
                            {
                                pnd_Act_Stop_Time_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaCwfl4817.getCwf_Master_Status_Clock_End_Dte_Tme());           //Natural: MOVE EDITED CWF-MASTER.STATUS-CLOCK-END-DTE-TME TO #ACT-STOP-TIME-T ( EM = YYYYMMDDHHIISST )
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaCwfl4817.getCwf_Master_Status_Elpsd_Clndr_Days_Tme().greater(" ")))                                                              //Natural: IF CWF-MASTER.STATUS-ELPSD-CLNDR-DAYS-TME GT ' '
                        {
                            ldaCwfl4820.getActivity_Wait_Time().setValue(ldaCwfl4817.getCwf_Master_Status_Elpsd_Clndr_Days_Tme());                                        //Natural: MOVE CWF-MASTER.STATUS-ELPSD-CLNDR-DAYS-TME TO ACTIVITY.WAIT-TIME
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaCwfl4817.getCwf_Master_Status_Elpsd_Bsnss_Days_Tme().greater(" ")))                                                              //Natural: IF CWF-MASTER.STATUS-ELPSD-BSNSS-DAYS-TME GT ' '
                        {
                            ldaCwfl4820.getActivity_Prcss_Time().setValue(ldaCwfl4817.getCwf_Master_Status_Elpsd_Bsnss_Days_Tme());                                       //Natural: MOVE CWF-MASTER.STATUS-ELPSD-BSNSS-DAYS-TME TO ACTIVITY.PRCSS-TIME
                        }                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-ADD-OR-UPDATE
                        sub_Check_If_Add_Or_Update();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R2"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(pnd_Add.getBoolean() || pnd_Update.getBoolean()))                                                                                   //Natural: IF #ADD OR #UPDATE
                        {
                            //*  INCLUDING C-STATUSES
                                                                                                                                                                          //Natural: PERFORM WRITE-ACTIVITY
                            sub_Write_Activity();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("R2"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN NONE
                if (condition(decideConditionsMet2260 == 0))
                {
                    ignore();
                    //*  --------------
                }                                                                                                                                                         //Natural: END-DECIDE
                //*  ----------- SET UP TLC KEY -------------------
                //*  COR/NAAD SUNSET START
                //*    RESET #COR-SUPER-PIN-RCDTYPE
                //*    #COR-SUPER-PIN-RCDTYPE.#PH-UNIQUE-ID-NBR := CWF-MASTER.PIN-NBR
                //*    #COR-SUPER-PIN-RCDTYPE.#PH-RCD-TYPE-CDE  := 1
                //*    PERFORM CHECK-TLC-CODE
                //*  COR/NAAD SUNSET END
                //*  R2.
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  R1.
        }                                                                                                                                                                 //Natural: END-WORK
        R1_Exit:
        if (Global.isEscape()) return;
    }
    private void sub_Check_If_Add_Or_Update() throws Exception                                                                                                            //Natural: CHECK-IF-ADD-OR-UPDATE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        pnd_Add.setValue(false);                                                                                                                                          //Natural: MOVE FALSE TO #ADD #UPDATE
        pnd_Update.setValue(false);
        pnd_Wipe_Out_End_Act.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #WIPE-OUT-END-ACT
        if (condition(pnd_Act_Strt_Time_T.greaterOrEqual(pnd_Tbl_Start_Dte_Tme_T) && pnd_Act_Strt_Time_T.lessOrEqual(pnd_Tbl_End_Dte_Tme_T)))                             //Natural: IF #ACT-STRT-TIME-T GE #TBL-START-DTE-TME-T AND #ACT-STRT-TIME-T LE #TBL-END-DTE-TME-T
        {
            if (condition(pnd_Act_Stop_Time_T.greater(pnd_Tbl_End_Dte_Tme_T)))                                                                                            //Natural: IF #ACT-STOP-TIME-T GT #TBL-END-DTE-TME-T
            {
                pnd_Update.setValue(true);                                                                                                                                //Natural: MOVE TRUE TO #UPDATE
                if (condition(ldaCwfl4815.getCwf_Support_Tbl_Rerun_Flag().equals("Y")))                                                                                   //Natural: IF CWF-SUPPORT-TBL.RERUN-FLAG = 'Y'
                {
                    pnd_Wipe_Out_End_Act.setValue(false);                                                                                                                 //Natural: MOVE FALSE TO #WIPE-OUT-END-ACT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Wipe_Out_End_Act.setValue(true);                                                                                                                  //Natural: MOVE TRUE TO #WIPE-OUT-END-ACT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Add.setValue(true);                                                                                                                                   //Natural: MOVE TRUE TO #ADD
                if (condition(ldaCwfl4815.getCwf_Support_Tbl_Rerun_Flag().equals("Y")))                                                                                   //Natural: IF CWF-SUPPORT-TBL.RERUN-FLAG = 'Y'
                {
                    pnd_Update.setValue(true);                                                                                                                            //Natural: MOVE TRUE TO #UPDATE
                    pnd_Add.setValue(false);                                                                                                                              //Natural: MOVE FALSE TO #ADD
                    pnd_Wipe_Out_End_Act.setValue(false);                                                                                                                 //Natural: MOVE FALSE TO #WIPE-OUT-END-ACT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Act_Stop_Time_T.greaterOrEqual(pnd_Tbl_Start_Dte_Tme_T) && pnd_Act_Stop_Time_T.lessOrEqual(pnd_Tbl_End_Dte_Tme_T)))                         //Natural: IF #ACT-STOP-TIME-T GE #TBL-START-DTE-TME-T AND #ACT-STOP-TIME-T LE #TBL-END-DTE-TME-T
            {
                pnd_Update.setValue(true);                                                                                                                                //Natural: MOVE TRUE TO #UPDATE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* ******
    }
    private void sub_Check_Status_Ranges() throws Exception                                                                                                               //Natural: CHECK-STATUS-RANGES
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Status_Of_Interest.setValue(false);                                                                                                                           //Natural: ASSIGN #STATUS-OF-INTEREST := FALSE
        short decideConditionsMet2423 = 0;                                                                                                                                //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN CWF-MASTER.ADMIN-STATUS-CDE GE '4500' AND CWF-MASTER.ADMIN-STATUS-CDE LE '4997'
        if (condition(ldaCwfl4817.getCwf_Master_Admin_Status_Cde().greaterOrEqual("4500") && ldaCwfl4817.getCwf_Master_Admin_Status_Cde().lessOrEqual("4997")))
        {
            decideConditionsMet2423++;
            pnd_Activity_Type.setValue("EXTR");                                                                                                                           //Natural: MOVE 'EXTR' TO #ACTIVITY-TYPE
            pnd_Status_Of_Interest.setValue(true);                                                                                                                        //Natural: ASSIGN #STATUS-OF-INTEREST := TRUE
        }                                                                                                                                                                 //Natural: WHEN CWF-MASTER.ADMIN-STATUS-CDE GE '0900' AND CWF-MASTER.ADMIN-STATUS-CDE LE '0999' OR CWF-MASTER.ADMIN-STATUS-CDE GE '4000' AND CWF-MASTER.ADMIN-STATUS-CDE LE '4499'
        if (condition(((ldaCwfl4817.getCwf_Master_Admin_Status_Cde().greaterOrEqual("0900") && ldaCwfl4817.getCwf_Master_Admin_Status_Cde().lessOrEqual("0999")) 
            || (ldaCwfl4817.getCwf_Master_Admin_Status_Cde().greaterOrEqual("4000") && ldaCwfl4817.getCwf_Master_Admin_Status_Cde().lessOrEqual("4499")))))
        {
            decideConditionsMet2423++;
            pnd_Activity_Type.setValue("INTR");                                                                                                                           //Natural: MOVE 'INTR' TO #ACTIVITY-TYPE
            pnd_Status_Of_Interest.setValue(true);                                                                                                                        //Natural: ASSIGN #STATUS-OF-INTEREST := TRUE
        }                                                                                                                                                                 //Natural: WHEN CWF-MASTER.ADMIN-STATUS-CDE GE '0000' AND CWF-MASTER.ADMIN-STATUS-CDE LE '0899'
        if (condition(ldaCwfl4817.getCwf_Master_Admin_Status_Cde().greaterOrEqual("0000") && ldaCwfl4817.getCwf_Master_Admin_Status_Cde().lessOrEqual("0899")))
        {
            decideConditionsMet2423++;
            pnd_Activity_Type.setValue("ENRT");                                                                                                                           //Natural: MOVE 'ENRT' TO #ACTIVITY-TYPE
            pnd_Status_Of_Interest.setValue(true);                                                                                                                        //Natural: ASSIGN #STATUS-OF-INTEREST := TRUE
        }                                                                                                                                                                 //Natural: WHEN CWF-MASTER.ADMIN-STATUS-CDE GE '7000' AND CWF-MASTER.ADMIN-STATUS-CDE LE '7499'
        if (condition(ldaCwfl4817.getCwf_Master_Admin_Status_Cde().greaterOrEqual("7000") && ldaCwfl4817.getCwf_Master_Admin_Status_Cde().lessOrEqual("7499")))
        {
            decideConditionsMet2423++;
            if (condition(ldaCwfl4817.getCwf_Master_Admin_Unit_Cde().notEquals(ldaCwfl4817.getCwf_Master_Last_Chnge_Unit_Cde())))                                         //Natural: IF CWF-MASTER.ADMIN-UNIT-CDE NE CWF-MASTER.LAST-CHNGE-UNIT-CDE
            {
                pnd_Activity_Type.setValue("ENRT");                                                                                                                       //Natural: MOVE 'ENRT' TO #ACTIVITY-TYPE
                pnd_Status_Of_Interest.setValue(true);                                                                                                                    //Natural: ASSIGN #STATUS-OF-INTEREST := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN CWF-MASTER.ADMIN-STATUS-CDE GE '8000' AND CWF-MASTER.ADMIN-STATUS-CDE LE '8799'
        if (condition(ldaCwfl4817.getCwf_Master_Admin_Status_Cde().greaterOrEqual("8000") && ldaCwfl4817.getCwf_Master_Admin_Status_Cde().lessOrEqual("8799")))
        {
            decideConditionsMet2423++;
            if (condition(ldaCwfl4817.getCwf_Master_Admin_Unit_Cde().notEquals(ldaCwfl4817.getCwf_Master_Last_Chnge_Unit_Cde())))                                         //Natural: IF CWF-MASTER.ADMIN-UNIT-CDE NE CWF-MASTER.LAST-CHNGE-UNIT-CDE
            {
                pnd_Activity_Type.setValue("ENRT");                                                                                                                       //Natural: MOVE 'ENRT' TO #ACTIVITY-TYPE
                pnd_Status_Of_Interest.setValue(true);                                                                                                                    //Natural: ASSIGN #STATUS-OF-INTEREST := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN CWF-MASTER.ADMIN-STATUS-CDE-1 EQ 'C'
        if (condition(ldaCwfl4817.getCwf_Master_Admin_Status_Cde_1().equals("C")))
        {
            decideConditionsMet2423++;
            //*   IF WORK-LIST-IND GT ' '                  /* ANY ACTIVITY
                                                                                                                                                                          //Natural: PERFORM CHECK-STATUS-RANGES-C
            sub_Check_Status_Ranges_C();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Status_Of_Interest.getBoolean()))                                                                                                           //Natural: IF #STATUS-OF-INTEREST
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM ROUTE-TBL-LOOK-UP
            sub_Route_Tbl_Look_Up();
            if (condition(Global.isEscape())) {return;}
            //*  TRUE - C-STATUS MATCH FOUND W/FLAG
            if (condition(pnd_C_Status_Match.getBoolean()))                                                                                                               //Natural: IF #C-STATUS-MATCH
            {
                pnd_Activity_Type.setValue("C-ST");                                                                                                                       //Natural: MOVE 'C-ST' TO #ACTIVITY-TYPE
                pnd_Status_Of_Interest.setValue(true);                                                                                                                    //Natural: ASSIGN #STATUS-OF-INTEREST := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet2423 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  CHECK-STATUS-RANGES
    }
    private void sub_Route_Tbl_Look_Up() throws Exception                                                                                                                 //Natural: ROUTE-TBL-LOOK-UP
    {
        if (BLNatReinput.isReinput()) return;

        pnd_C_Status_Match.setValue(false);                                                                                                                               //Natural: ASSIGN #C-STATUS-MATCH := FALSE
        pnd_Tbl_Key.reset();                                                                                                                                              //Natural: RESET #TBL-KEY
        pnd_Tbl_Key_Pnd_Tbl_Scrty_Level_Ind.setValue("S");                                                                                                                //Natural: ASSIGN #TBL-KEY.#TBL-SCRTY-LEVEL-IND := 'S'
        pnd_Tbl_Key_Pnd_Tbl_Table_Nme.setValue("ROUTE-CODE-TABLE");                                                                                                       //Natural: ASSIGN #TBL-KEY.#TBL-TABLE-NME := 'ROUTE-CODE-TABLE'
        pnd_Tbl_Key_Pnd_Tbl_Key_Field.setValue(ldaCwfl4817.getCwf_Master_Admin_Status_Cde());                                                                             //Natural: ASSIGN #TBL-KEY.#TBL-KEY-FIELD := CWF-MASTER.ADMIN-STATUS-CDE
        ldaCwfl4818.getVw_cwf_Route_Support_Tbl().startDatabaseFind                                                                                                       //Natural: FIND ( 1 ) CWF-ROUTE-SUPPORT-TBL WITH TBL-PRIME-KEY = #TBL-KEY
        (
        "F1",
        new Wc[] { new Wc("TBL_PRIME_KEY", "=", pnd_Tbl_Key, WcType.WITH) },
        1
        );
        F1:
        while (condition(ldaCwfl4818.getVw_cwf_Route_Support_Tbl().readNextRow("F1", true)))
        {
            ldaCwfl4818.getVw_cwf_Route_Support_Tbl().setIfNotFoundControlFlag(false);
            if (condition(ldaCwfl4818.getVw_cwf_Route_Support_Tbl().getAstCOUNTER().equals(0)))                                                                           //Natural: IF NO RECORD FOUND
            {
                //*      WRITE / 'ROUTE TABLE CODE WAS NOT FOUND '
                //*                                   CWF-MASTER.ADMIN-STATUS-CDE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            if (condition(ldaCwfl4818.getCwf_Route_Support_Tbl_Status_Code().equals("Y   ")))                                                                             //Natural: IF CWF-ROUTE-SUPPORT-TBL.STATUS-CODE = 'Y   '
            {
                pnd_C_Status_Match.setValue(true);                                                                                                                        //Natural: ASSIGN #C-STATUS-MATCH := TRUE
            }                                                                                                                                                             //Natural: END-IF
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  ROUTE-TBL-LOOK-UP
    }
    private void sub_Check_Status_Ranges_C() throws Exception                                                                                                             //Natural: CHECK-STATUS-RANGES-C
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Status_Of_Interest.setValue(false);                                                                                                                           //Natural: ASSIGN #STATUS-OF-INTEREST := FALSE
        short decideConditionsMet2486 = 0;                                                                                                                                //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN CWF-MASTER.STATUS-CDE GE '4500' AND CWF-MASTER.STATUS-CDE LE '4997'
        if (condition(ldaCwfl4817.getCwf_Master_Status_Cde().greaterOrEqual("4500") && ldaCwfl4817.getCwf_Master_Status_Cde().lessOrEqual("4997")))
        {
            decideConditionsMet2486++;
            pnd_Activity_Type.setValue("EXTR");                                                                                                                           //Natural: MOVE 'EXTR' TO #ACTIVITY-TYPE
            pnd_Status_Of_Interest.setValue(true);                                                                                                                        //Natural: ASSIGN #STATUS-OF-INTEREST := TRUE
        }                                                                                                                                                                 //Natural: WHEN CWF-MASTER.STATUS-CDE GE '0900' AND CWF-MASTER.STATUS-CDE LE '0999' OR CWF-MASTER.STATUS-CDE GE '4000' AND CWF-MASTER.STATUS-CDE LE '4499'
        if (condition(((ldaCwfl4817.getCwf_Master_Status_Cde().greaterOrEqual("0900") && ldaCwfl4817.getCwf_Master_Status_Cde().lessOrEqual("0999")) || 
            (ldaCwfl4817.getCwf_Master_Status_Cde().greaterOrEqual("4000") && ldaCwfl4817.getCwf_Master_Status_Cde().lessOrEqual("4499")))))
        {
            decideConditionsMet2486++;
            pnd_Activity_Type.setValue("INTR");                                                                                                                           //Natural: MOVE 'INTR' TO #ACTIVITY-TYPE
            pnd_Status_Of_Interest.setValue(true);                                                                                                                        //Natural: ASSIGN #STATUS-OF-INTEREST := TRUE
        }                                                                                                                                                                 //Natural: WHEN CWF-MASTER.STATUS-CDE GE '0000' AND CWF-MASTER.STATUS-CDE LE '0899'
        if (condition(ldaCwfl4817.getCwf_Master_Status_Cde().greaterOrEqual("0000") && ldaCwfl4817.getCwf_Master_Status_Cde().lessOrEqual("0899")))
        {
            decideConditionsMet2486++;
            pnd_Activity_Type.setValue("ENRT");                                                                                                                           //Natural: MOVE 'ENRT' TO #ACTIVITY-TYPE
            pnd_Status_Of_Interest.setValue(true);                                                                                                                        //Natural: ASSIGN #STATUS-OF-INTEREST := TRUE
        }                                                                                                                                                                 //Natural: WHEN CWF-MASTER.STATUS-CDE GE '7000' AND CWF-MASTER.STATUS-CDE LE '7499'
        if (condition(ldaCwfl4817.getCwf_Master_Status_Cde().greaterOrEqual("7000") && ldaCwfl4817.getCwf_Master_Status_Cde().lessOrEqual("7499")))
        {
            decideConditionsMet2486++;
            if (condition(ldaCwfl4817.getCwf_Master_Admin_Unit_Cde().notEquals(ldaCwfl4817.getCwf_Master_Last_Chnge_Unit_Cde())))                                         //Natural: IF CWF-MASTER.ADMIN-UNIT-CDE NE CWF-MASTER.LAST-CHNGE-UNIT-CDE
            {
                pnd_Activity_Type.setValue("ENRT");                                                                                                                       //Natural: MOVE 'ENRT' TO #ACTIVITY-TYPE
                pnd_Status_Of_Interest.setValue(true);                                                                                                                    //Natural: ASSIGN #STATUS-OF-INTEREST := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN CWF-MASTER.STATUS-CDE GE '8000' AND CWF-MASTER.STATUS-CDE LE '8799'
        if (condition(ldaCwfl4817.getCwf_Master_Status_Cde().greaterOrEqual("8000") && ldaCwfl4817.getCwf_Master_Status_Cde().lessOrEqual("8799")))
        {
            decideConditionsMet2486++;
            if (condition(ldaCwfl4817.getCwf_Master_Admin_Unit_Cde().notEquals(ldaCwfl4817.getCwf_Master_Last_Chnge_Unit_Cde())))                                         //Natural: IF CWF-MASTER.ADMIN-UNIT-CDE NE CWF-MASTER.LAST-CHNGE-UNIT-CDE
            {
                pnd_Activity_Type.setValue("ENRT");                                                                                                                       //Natural: MOVE 'ENRT' TO #ACTIVITY-TYPE
                pnd_Status_Of_Interest.setValue(true);                                                                                                                    //Natural: ASSIGN #STATUS-OF-INTEREST := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet2486 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  CHECK-STATUS-RANGES
    }
    private void sub_Write_Work_Request() throws Exception                                                                                                                //Natural: WRITE-WORK-REQUEST
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        ldaCwfl4819.getWork_Request().reset();                                                                                                                            //Natural: RESET WORK-REQUEST EXTRA-TABLE
        ldaCwfl4828.getExtra_Table().reset();
                                                                                                                                                                          //Natural: PERFORM FILL-WORK-REQUEST
        sub_Fill_Work_Request();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-NAME
        sub_Get_Name();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-PART-AGE
        sub_Get_Part_Age();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-DOD-NOT-INFO
        sub_Get_Dod_Not_Info();
        if (condition(Global.isEscape())) {return;}
        //*  IF #PASS-FOUND                   /* COMMENTED ON 04/05/00
        short decideConditionsMet2520 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #UPDATE
        if (condition(pnd_Update.getBoolean()))
        {
            decideConditionsMet2520++;
            ldaCwfl4899.getWork_Request_New().reset();                                                                                                                    //Natural: RESET WORK-REQUEST-NEW
            ldaCwfl4899.getWork_Request_New_Rqst_Log_Dte_Tme_A().setValue(ldaCwfl4819.getWork_Request_Rqst_Log_Dte_Tme_A());                                              //Natural: MOVE WORK-REQUEST.RQST-LOG-DTE-TME-A TO WORK-REQUEST-NEW.RQST-LOG-DTE-TME-A
            ldaCwfl4899.getWork_Request_New_D1().setValue(ldaCwfl4819.getWork_Request_C1());                                                                              //Natural: MOVE WORK-REQUEST.C1 TO WORK-REQUEST-NEW.D1
            ldaCwfl4899.getWork_Request_New_Work_Prcss_Id().setValue(ldaCwfl4819.getWork_Request_Work_Prcss_Id());                                                        //Natural: MOVE WORK-REQUEST.WORK-PRCSS-ID TO WORK-REQUEST-NEW.WORK-PRCSS-ID
            ldaCwfl4899.getWork_Request_New_D2().setValue(ldaCwfl4819.getWork_Request_C2());                                                                              //Natural: MOVE WORK-REQUEST.C2 TO WORK-REQUEST-NEW.D2
            //*    MOVE WORK-REQUEST.PIN-NBR TO WORK-REQUEST-NEW.PIN-NBR
            pnd_Pin_Nbr_A12.reset();                                                                                                                                      //Natural: RESET #PIN-NBR-A12
            pnd_Pin_Nbr_A12.moveAll(ldaCwfl4819.getWork_Request_Pin_Nbr());                                                                                               //Natural: MOVE ALL WORK-REQUEST.PIN-NBR TO #PIN-NBR-A12
            ldaCwfl4899.getWork_Request_New_Pin_Nbr().setValue(pnd_Pin_Nbr_A12_Pnd_Pin_Nbr_6_12);                                                                         //Natural: MOVE #PIN-NBR-6-12 TO WORK-REQUEST-NEW.PIN-NBR
            ldaCwfl4899.getWork_Request_New_D3().setValue(ldaCwfl4819.getWork_Request_C3());                                                                              //Natural: MOVE WORK-REQUEST.C3 TO WORK-REQUEST-NEW.D3
            ldaCwfl4899.getWork_Request_New_Part_Name().setValue(ldaCwfl4819.getWork_Request_Part_Name());                                                                //Natural: MOVE WORK-REQUEST.PART-NAME TO WORK-REQUEST-NEW.PART-NAME
            ldaCwfl4899.getWork_Request_New_D4().setValue(ldaCwfl4819.getWork_Request_C4());                                                                              //Natural: MOVE WORK-REQUEST.C4 TO WORK-REQUEST-NEW.D4
            ldaCwfl4899.getWork_Request_New_Ssn().setValue(ldaCwfl4819.getWork_Request_Ssn());                                                                            //Natural: MOVE WORK-REQUEST.SSN TO WORK-REQUEST-NEW.SSN
            ldaCwfl4899.getWork_Request_New_D5().setValue(ldaCwfl4819.getWork_Request_C5());                                                                              //Natural: MOVE WORK-REQUEST.C5 TO WORK-REQUEST-NEW.D5
            ldaCwfl4899.getWork_Request_New_Tiaa_Rcvd_Dte_Tme_A().setValue(ldaCwfl4819.getWork_Request_Tiaa_Rcvd_Dte_Tme_A());                                            //Natural: MOVE WORK-REQUEST.TIAA-RCVD-DTE-TME-A TO WORK-REQUEST-NEW.TIAA-RCVD-DTE-TME-A
            ldaCwfl4899.getWork_Request_New_D6().setValue(ldaCwfl4819.getWork_Request_C6());                                                                              //Natural: MOVE WORK-REQUEST.C6 TO WORK-REQUEST-NEW.D6
            ldaCwfl4899.getWork_Request_New_Tiaa_Rcvd_Dte_A().setValue(ldaCwfl4819.getWork_Request_Tiaa_Rcvd_Dte_A());                                                    //Natural: MOVE WORK-REQUEST.TIAA-RCVD-DTE-A TO WORK-REQUEST-NEW.TIAA-RCVD-DTE-A
            ldaCwfl4899.getWork_Request_New_D7().setValue(ldaCwfl4819.getWork_Request_C7());                                                                              //Natural: MOVE WORK-REQUEST.C7 TO WORK-REQUEST-NEW.D7
            ldaCwfl4899.getWork_Request_New_Orgnl_Unit_Cde().setValue(ldaCwfl4819.getWork_Request_Orgnl_Unit_Cde());                                                      //Natural: MOVE WORK-REQUEST.ORGNL-UNIT-CDE TO WORK-REQUEST-NEW.ORGNL-UNIT-CDE
            ldaCwfl4899.getWork_Request_New_D8().setValue(ldaCwfl4819.getWork_Request_C8());                                                                              //Natural: MOVE WORK-REQUEST.C8 TO WORK-REQUEST-NEW.D8
            ldaCwfl4899.getWork_Request_New_Rqst_Log_Oprtr_Cde().setValue(ldaCwfl4819.getWork_Request_Rqst_Log_Oprtr_Cde());                                              //Natural: MOVE WORK-REQUEST.RQST-LOG-OPRTR-CDE TO WORK-REQUEST-NEW.RQST-LOG-OPRTR-CDE
            ldaCwfl4899.getWork_Request_New_D9().setValue(ldaCwfl4819.getWork_Request_C9());                                                                              //Natural: MOVE WORK-REQUEST.C9 TO WORK-REQUEST-NEW.D9
            ldaCwfl4899.getWork_Request_New_Rqst_Orgn_Cde().setValue(ldaCwfl4819.getWork_Request_Rqst_Orgn_Cde());                                                        //Natural: MOVE WORK-REQUEST.RQST-ORGN-CDE TO WORK-REQUEST-NEW.RQST-ORGN-CDE
            ldaCwfl4899.getWork_Request_New_D10().setValue(ldaCwfl4819.getWork_Request_C10());                                                                            //Natural: MOVE WORK-REQUEST.C10 TO WORK-REQUEST-NEW.D10
            ldaCwfl4899.getWork_Request_New_Crprte_Status_Ind().setValue(ldaCwfl4819.getWork_Request_Crprte_Status_Ind());                                                //Natural: MOVE WORK-REQUEST.CRPRTE-STATUS-IND TO WORK-REQUEST-NEW.CRPRTE-STATUS-IND
            ldaCwfl4899.getWork_Request_New_D11().setValue(ldaCwfl4819.getWork_Request_C11());                                                                            //Natural: MOVE WORK-REQUEST.C11 TO WORK-REQUEST-NEW.D11
            ldaCwfl4899.getWork_Request_New_Crprte_Clock_End_Dte_Tme_A().setValue(ldaCwfl4819.getWork_Request_Crprte_Clock_End_Dte_Tme_A());                              //Natural: MOVE WORK-REQUEST.CRPRTE-CLOCK-END-DTE-TME-A TO WORK-REQUEST-NEW.CRPRTE-CLOCK-END-DTE-TME-A
            ldaCwfl4899.getWork_Request_New_D12().setValue(ldaCwfl4819.getWork_Request_C12());                                                                            //Natural: MOVE WORK-REQUEST.C12 TO WORK-REQUEST-NEW.D12
            ldaCwfl4899.getWork_Request_New_Final_Close_Out_Dte_Tme_A().setValue(ldaCwfl4819.getWork_Request_Final_Close_Out_Dte_Tme_A());                                //Natural: MOVE WORK-REQUEST.FINAL-CLOSE-OUT-DTE-TME-A TO WORK-REQUEST-NEW.FINAL-CLOSE-OUT-DTE-TME-A
            ldaCwfl4899.getWork_Request_New_D13().setValue(ldaCwfl4819.getWork_Request_C13());                                                                            //Natural: MOVE WORK-REQUEST.C13 TO WORK-REQUEST-NEW.D13
            ldaCwfl4899.getWork_Request_New_Effctve_Dte_A().setValue(ldaCwfl4819.getWork_Request_Effctve_Dte_A());                                                        //Natural: MOVE WORK-REQUEST.EFFCTVE-DTE-A TO WORK-REQUEST-NEW.EFFCTVE-DTE-A
            ldaCwfl4899.getWork_Request_New_D14().setValue(ldaCwfl4819.getWork_Request_C14());                                                                            //Natural: MOVE WORK-REQUEST.C14 TO WORK-REQUEST-NEW.D14
            ldaCwfl4899.getWork_Request_New_Trans_Dte_A().setValue(ldaCwfl4819.getWork_Request_Trans_Dte_A());                                                            //Natural: MOVE WORK-REQUEST.TRANS-DTE-A TO WORK-REQUEST-NEW.TRANS-DTE-A
            ldaCwfl4899.getWork_Request_New_D15().setValue(ldaCwfl4819.getWork_Request_C15());                                                                            //Natural: MOVE WORK-REQUEST.C15 TO WORK-REQUEST-NEW.D15
            ldaCwfl4899.getWork_Request_New_Trnsctn_Dte_A().setValue(ldaCwfl4819.getWork_Request_Trnsctn_Dte_A());                                                        //Natural: MOVE WORK-REQUEST.TRNSCTN-DTE-A TO WORK-REQUEST-NEW.TRNSCTN-DTE-A
            ldaCwfl4899.getWork_Request_New_D16().setValue(ldaCwfl4819.getWork_Request_C16());                                                                            //Natural: MOVE WORK-REQUEST.C16 TO WORK-REQUEST-NEW.D16
            ldaCwfl4899.getWork_Request_New_Owner_Unit_Cde().setValue(ldaCwfl4819.getWork_Request_Owner_Unit_Cde());                                                      //Natural: MOVE WORK-REQUEST.OWNER-UNIT-CDE TO WORK-REQUEST-NEW.OWNER-UNIT-CDE
            ldaCwfl4899.getWork_Request_New_D17().setValue(ldaCwfl4819.getWork_Request_C17());                                                                            //Natural: MOVE WORK-REQUEST.C17 TO WORK-REQUEST-NEW.D17
            ldaCwfl4899.getWork_Request_New_Owner_Division().setValue(ldaCwfl4819.getWork_Request_Owner_Division());                                                      //Natural: MOVE WORK-REQUEST.OWNER-DIVISION TO WORK-REQUEST-NEW.OWNER-DIVISION
            ldaCwfl4899.getWork_Request_New_D18().setValue(ldaCwfl4819.getWork_Request_C18());                                                                            //Natural: MOVE WORK-REQUEST.C18 TO WORK-REQUEST-NEW.D18
            ldaCwfl4899.getWork_Request_New_Shphrd_Id().setValue(ldaCwfl4819.getWork_Request_Shphrd_Id());                                                                //Natural: MOVE WORK-REQUEST.SHPHRD-ID TO WORK-REQUEST-NEW.SHPHRD-ID
            ldaCwfl4899.getWork_Request_New_D19().setValue(ldaCwfl4819.getWork_Request_C19());                                                                            //Natural: MOVE WORK-REQUEST.C19 TO WORK-REQUEST-NEW.D19
            ldaCwfl4899.getWork_Request_New_Sec_Ind().setValue(ldaCwfl4819.getWork_Request_Sec_Ind());                                                                    //Natural: MOVE WORK-REQUEST.SEC-IND TO WORK-REQUEST-NEW.SEC-IND
            ldaCwfl4899.getWork_Request_New_D20().setValue(ldaCwfl4819.getWork_Request_C20());                                                                            //Natural: MOVE WORK-REQUEST.C20 TO WORK-REQUEST-NEW.D20
            ldaCwfl4899.getWork_Request_New_Admin_Work_Ind().setValue(ldaCwfl4819.getWork_Request_Admin_Work_Ind());                                                      //Natural: MOVE WORK-REQUEST.ADMIN-WORK-IND TO WORK-REQUEST-NEW.ADMIN-WORK-IND
            ldaCwfl4899.getWork_Request_New_D21().setValue(ldaCwfl4819.getWork_Request_C21());                                                                            //Natural: MOVE WORK-REQUEST.C21 TO WORK-REQUEST-NEW.D21
            ldaCwfl4899.getWork_Request_New_Sub_Rqst_Ind().setValue(ldaCwfl4819.getWork_Request_Sub_Rqst_Ind());                                                          //Natural: MOVE WORK-REQUEST.SUB-RQST-IND TO WORK-REQUEST-NEW.SUB-RQST-IND
            ldaCwfl4899.getWork_Request_New_D22().setValue(ldaCwfl4819.getWork_Request_C22());                                                                            //Natural: MOVE WORK-REQUEST.C22 TO WORK-REQUEST-NEW.D22
            ldaCwfl4899.getWork_Request_New_Prnt_Rqst_Log_Dte_Tme_A().setValue(ldaCwfl4819.getWork_Request_Prnt_Rqst_Log_Dte_Tme_A());                                    //Natural: MOVE WORK-REQUEST.PRNT-RQST-LOG-DTE-TME-A TO WORK-REQUEST-NEW.PRNT-RQST-LOG-DTE-TME-A
            ldaCwfl4899.getWork_Request_New_D23().setValue(ldaCwfl4819.getWork_Request_C23());                                                                            //Natural: MOVE WORK-REQUEST.C23 TO WORK-REQUEST-NEW.D23
            ldaCwfl4899.getWork_Request_New_Prnt_Work_Prcss_Id().setValue(ldaCwfl4819.getWork_Request_Prnt_Work_Prcss_Id());                                              //Natural: MOVE WORK-REQUEST.PRNT-WORK-PRCSS-ID TO WORK-REQUEST-NEW.PRNT-WORK-PRCSS-ID
            ldaCwfl4899.getWork_Request_New_D24().setValue(ldaCwfl4819.getWork_Request_C24());                                                                            //Natural: MOVE WORK-REQUEST.C24 TO WORK-REQUEST-NEW.D24
            ldaCwfl4899.getWork_Request_New_Multi_Rqst_Ind().setValue(ldaCwfl4819.getWork_Request_Multi_Rqst_Ind());                                                      //Natural: MOVE WORK-REQUEST.MULTI-RQST-IND TO WORK-REQUEST-NEW.MULTI-RQST-IND
            ldaCwfl4899.getWork_Request_New_D25().setValue(ldaCwfl4819.getWork_Request_C25());                                                                            //Natural: MOVE WORK-REQUEST.C25 TO WORK-REQUEST-NEW.D25
            ldaCwfl4899.getWork_Request_New_Cmplnt_Ind().setValue(ldaCwfl4819.getWork_Request_Cmplnt_Ind());                                                              //Natural: MOVE WORK-REQUEST.CMPLNT-IND TO WORK-REQUEST-NEW.CMPLNT-IND
            ldaCwfl4899.getWork_Request_New_D26().setValue(ldaCwfl4819.getWork_Request_C26());                                                                            //Natural: MOVE WORK-REQUEST.C26 TO WORK-REQUEST-NEW.D26
            ldaCwfl4899.getWork_Request_New_Elctrnc_Fldr_Ind().setValue(ldaCwfl4819.getWork_Request_Elctrnc_Fldr_Ind());                                                  //Natural: MOVE WORK-REQUEST.ELCTRNC-FLDR-IND TO WORK-REQUEST-NEW.ELCTRNC-FLDR-IND
            ldaCwfl4899.getWork_Request_New_D27().setValue(ldaCwfl4819.getWork_Request_C27());                                                                            //Natural: MOVE WORK-REQUEST.C27 TO WORK-REQUEST-NEW.D27
            ldaCwfl4899.getWork_Request_New_Mj_Pull_Ind().setValue(ldaCwfl4819.getWork_Request_Mj_Pull_Ind());                                                            //Natural: MOVE WORK-REQUEST.MJ-PULL-IND TO WORK-REQUEST-NEW.MJ-PULL-IND
            ldaCwfl4899.getWork_Request_New_D28().setValue(ldaCwfl4819.getWork_Request_C28());                                                                            //Natural: MOVE WORK-REQUEST.C28 TO WORK-REQUEST-NEW.D28
            ldaCwfl4899.getWork_Request_New_Check_Ind().setValue(ldaCwfl4819.getWork_Request_Check_Ind());                                                                //Natural: MOVE WORK-REQUEST.CHECK-IND TO WORK-REQUEST-NEW.CHECK-IND
            ldaCwfl4899.getWork_Request_New_D29().setValue(ldaCwfl4819.getWork_Request_C29());                                                                            //Natural: MOVE WORK-REQUEST.C29 TO WORK-REQUEST-NEW.D29
            ldaCwfl4899.getWork_Request_New_Bsnss_Reply_Ind().setValue(ldaCwfl4819.getWork_Request_Bsnss_Reply_Ind());                                                    //Natural: MOVE WORK-REQUEST.BSNSS-REPLY-IND TO WORK-REQUEST-NEW.BSNSS-REPLY-IND
            ldaCwfl4899.getWork_Request_New_D30().setValue(ldaCwfl4819.getWork_Request_C30());                                                                            //Natural: MOVE WORK-REQUEST.C30 TO WORK-REQUEST-NEW.D30
            ldaCwfl4899.getWork_Request_New_Tlc_Ind().setValue(ldaCwfl4819.getWork_Request_Tlc_Ind());                                                                    //Natural: MOVE WORK-REQUEST.TLC-IND TO WORK-REQUEST-NEW.TLC-IND
            ldaCwfl4899.getWork_Request_New_D31().setValue(ldaCwfl4819.getWork_Request_C31());                                                                            //Natural: MOVE WORK-REQUEST.C31 TO WORK-REQUEST-NEW.D31
            ldaCwfl4899.getWork_Request_New_Redo_Ind().setValue(ldaCwfl4819.getWork_Request_Redo_Ind());                                                                  //Natural: MOVE WORK-REQUEST.REDO-IND TO WORK-REQUEST-NEW.REDO-IND
            ldaCwfl4899.getWork_Request_New_D32().setValue(ldaCwfl4819.getWork_Request_C32());                                                                            //Natural: MOVE WORK-REQUEST.C32 TO WORK-REQUEST-NEW.D32
            ldaCwfl4899.getWork_Request_New_Crprte_On_Tme_Ind().setValue(ldaCwfl4819.getWork_Request_Crprte_On_Tme_Ind());                                                //Natural: MOVE WORK-REQUEST.CRPRTE-ON-TME-IND TO WORK-REQUEST-NEW.CRPRTE-ON-TME-IND
            ldaCwfl4899.getWork_Request_New_D33().setValue(ldaCwfl4819.getWork_Request_C33());                                                                            //Natural: MOVE WORK-REQUEST.C33 TO WORK-REQUEST-NEW.D33
            ldaCwfl4899.getWork_Request_New_Off_Rtng_Ind().setValue(ldaCwfl4819.getWork_Request_Off_Rtng_Ind());                                                          //Natural: MOVE WORK-REQUEST.OFF-RTNG-IND TO WORK-REQUEST-NEW.OFF-RTNG-IND
            ldaCwfl4899.getWork_Request_New_D34().setValue(ldaCwfl4819.getWork_Request_C34());                                                                            //Natural: MOVE WORK-REQUEST.C34 TO WORK-REQUEST-NEW.D34
            ldaCwfl4899.getWork_Request_New_Prcssng_Type_Cde().setValue(ldaCwfl4819.getWork_Request_Prcssng_Type_Cde());                                                  //Natural: MOVE WORK-REQUEST.PRCSSNG-TYPE-CDE TO WORK-REQUEST-NEW.PRCSSNG-TYPE-CDE
            ldaCwfl4899.getWork_Request_New_D35().setValue(ldaCwfl4819.getWork_Request_C35());                                                                            //Natural: MOVE WORK-REQUEST.C35 TO WORK-REQUEST-NEW.D35
            ldaCwfl4899.getWork_Request_New_Log_Rqstr_Cde().setValue(ldaCwfl4819.getWork_Request_Log_Rqstr_Cde());                                                        //Natural: MOVE WORK-REQUEST.LOG-RQSTR-CDE TO WORK-REQUEST-NEW.LOG-RQSTR-CDE
            ldaCwfl4899.getWork_Request_New_D36().setValue(ldaCwfl4819.getWork_Request_C36());                                                                            //Natural: MOVE WORK-REQUEST.C36 TO WORK-REQUEST-NEW.D36
            ldaCwfl4899.getWork_Request_New_Log_Insttn_Srce_Cde().setValue(ldaCwfl4819.getWork_Request_Log_Insttn_Srce_Cde());                                            //Natural: MOVE WORK-REQUEST.LOG-INSTTN-SRCE-CDE TO WORK-REQUEST-NEW.LOG-INSTTN-SRCE-CDE
            ldaCwfl4899.getWork_Request_New_D37().setValue(ldaCwfl4819.getWork_Request_C37());                                                                            //Natural: MOVE WORK-REQUEST.C37 TO WORK-REQUEST-NEW.D37
            ldaCwfl4899.getWork_Request_New_Instn_Cde().setValue(ldaCwfl4819.getWork_Request_Instn_Cde());                                                                //Natural: MOVE WORK-REQUEST.INSTN-CDE TO WORK-REQUEST-NEW.INSTN-CDE
            ldaCwfl4899.getWork_Request_New_D38().setValue(ldaCwfl4819.getWork_Request_C38());                                                                            //Natural: MOVE WORK-REQUEST.C38 TO WORK-REQUEST-NEW.D38
            ldaCwfl4899.getWork_Request_New_Rqst_Instn_Cde().setValue(ldaCwfl4819.getWork_Request_Rqst_Instn_Cde());                                                      //Natural: MOVE WORK-REQUEST.RQST-INSTN-CDE TO WORK-REQUEST-NEW.RQST-INSTN-CDE
            ldaCwfl4899.getWork_Request_New_D39().setValue(ldaCwfl4819.getWork_Request_C39());                                                                            //Natural: MOVE WORK-REQUEST.C39 TO WORK-REQUEST-NEW.D39
            ldaCwfl4899.getWork_Request_New_Rqst_Rgn_Cde().setValue(ldaCwfl4819.getWork_Request_Rqst_Rgn_Cde());                                                          //Natural: MOVE WORK-REQUEST.RQST-RGN-CDE TO WORK-REQUEST-NEW.RQST-RGN-CDE
            ldaCwfl4899.getWork_Request_New_D40().setValue(ldaCwfl4819.getWork_Request_C40());                                                                            //Natural: MOVE WORK-REQUEST.C40 TO WORK-REQUEST-NEW.D40
            ldaCwfl4899.getWork_Request_New_Rqst_Spcl_Dsgntn_Cde().setValue(ldaCwfl4819.getWork_Request_Rqst_Spcl_Dsgntn_Cde());                                          //Natural: MOVE WORK-REQUEST.RQST-SPCL-DSGNTN-CDE TO WORK-REQUEST-NEW.RQST-SPCL-DSGNTN-CDE
            ldaCwfl4899.getWork_Request_New_D41().setValue(ldaCwfl4819.getWork_Request_C41());                                                                            //Natural: MOVE WORK-REQUEST.C41 TO WORK-REQUEST-NEW.D41
            ldaCwfl4899.getWork_Request_New_Rqst_Brnch_Cde().setValue(ldaCwfl4819.getWork_Request_Rqst_Brnch_Cde());                                                      //Natural: MOVE WORK-REQUEST.RQST-BRNCH-CDE TO WORK-REQUEST-NEW.RQST-BRNCH-CDE
            ldaCwfl4899.getWork_Request_New_D42().setValue(ldaCwfl4819.getWork_Request_C42());                                                                            //Natural: MOVE WORK-REQUEST.C42 TO WORK-REQUEST-NEW.D42
            ldaCwfl4899.getWork_Request_New_Extrnl_Pend_Ind().setValue(ldaCwfl4819.getWork_Request_Extrnl_Pend_Ind());                                                    //Natural: MOVE WORK-REQUEST.EXTRNL-PEND-IND TO WORK-REQUEST-NEW.EXTRNL-PEND-IND
            ldaCwfl4899.getWork_Request_New_D43().setValue(ldaCwfl4819.getWork_Request_C43());                                                                            //Natural: MOVE WORK-REQUEST.C43 TO WORK-REQUEST-NEW.D43
            ldaCwfl4899.getWork_Request_New_Crprte_Due_Dte_Tme_A().setValue(ldaCwfl4819.getWork_Request_Crprte_Due_Dte_Tme_A());                                          //Natural: MOVE WORK-REQUEST.CRPRTE-DUE-DTE-TME-A TO WORK-REQUEST-NEW.CRPRTE-DUE-DTE-TME-A
            ldaCwfl4899.getWork_Request_New_D44().setValue(ldaCwfl4819.getWork_Request_C44());                                                                            //Natural: MOVE WORK-REQUEST.C44 TO WORK-REQUEST-NEW.D44
            ldaCwfl4899.getWork_Request_New_Mis_Routed_Ind().setValue(ldaCwfl4819.getWork_Request_Mis_Routed_Ind());                                                      //Natural: MOVE WORK-REQUEST.MIS-ROUTED-IND TO WORK-REQUEST-NEW.MIS-ROUTED-IND
            ldaCwfl4899.getWork_Request_New_D45().setValue(ldaCwfl4819.getWork_Request_C45());                                                                            //Natural: MOVE WORK-REQUEST.C45 TO WORK-REQUEST-NEW.D45
            ldaCwfl4899.getWork_Request_New_Dte_Of_Birth().setValue(ldaCwfl4819.getWork_Request_Dte_Of_Birth());                                                          //Natural: MOVE WORK-REQUEST.DTE-OF-BIRTH TO WORK-REQUEST-NEW.DTE-OF-BIRTH
            ldaCwfl4899.getWork_Request_New_D46().setValue(ldaCwfl4819.getWork_Request_C46());                                                                            //Natural: MOVE WORK-REQUEST.C46 TO WORK-REQUEST-NEW.D46
            ldaCwfl4899.getWork_Request_New_Trade_Dte_Tme_A().setValue(ldaCwfl4819.getWork_Request_Trade_Dte_Tme_A());                                                    //Natural: MOVE WORK-REQUEST.TRADE-DTE-TME-A TO WORK-REQUEST-NEW.TRADE-DTE-TME-A
            ldaCwfl4899.getWork_Request_New_D47().setValue(ldaCwfl4819.getWork_Request_C47());                                                                            //Natural: MOVE WORK-REQUEST.C47 TO WORK-REQUEST-NEW.D47
            ldaCwfl4899.getWork_Request_New_Mj_Media_Ind().setValue(ldaCwfl4819.getWork_Request_Mj_Media_Ind());                                                          //Natural: MOVE WORK-REQUEST.MJ-MEDIA-IND TO WORK-REQUEST-NEW.MJ-MEDIA-IND
            ldaCwfl4899.getWork_Request_New_D48().setValue(ldaCwfl4819.getWork_Request_C48());                                                                            //Natural: MOVE WORK-REQUEST.C48 TO WORK-REQUEST-NEW.D48
            ldaCwfl4899.getWork_Request_New_State_Of_Res().setValue(ldaCwfl4819.getWork_Request_State_Of_Res());                                                          //Natural: MOVE WORK-REQUEST.STATE-OF-RES TO WORK-REQUEST-NEW.STATE-OF-RES
            ldaCwfl4899.getWork_Request_New_D49().setValue(ldaCwfl4819.getWork_Request_C49());                                                                            //Natural: MOVE WORK-REQUEST.C49 TO WORK-REQUEST-NEW.D49
            ldaCwfl4899.getWork_Request_New_Sec_Turnaround_Tme_A().setValue(ldaCwfl4819.getWork_Request_Sec_Turnaround_Tme_A());                                          //Natural: MOVE WORK-REQUEST.SEC-TURNAROUND-TME-A TO WORK-REQUEST-NEW.SEC-TURNAROUND-TME-A
            ldaCwfl4899.getWork_Request_New_D50().setValue(ldaCwfl4819.getWork_Request_C50());                                                                            //Natural: MOVE WORK-REQUEST.C50 TO WORK-REQUEST-NEW.D50
            ldaCwfl4899.getWork_Request_New_Sec_Updte_Dte_A().setValue(ldaCwfl4819.getWork_Request_Sec_Updte_Dte_A());                                                    //Natural: MOVE WORK-REQUEST.SEC-UPDTE-DTE-A TO WORK-REQUEST-NEW.SEC-UPDTE-DTE-A
            ldaCwfl4899.getWork_Request_New_D51().setValue(ldaCwfl4819.getWork_Request_C51());                                                                            //Natural: MOVE WORK-REQUEST.C51 TO WORK-REQUEST-NEW.D51
            ldaCwfl4899.getWork_Request_New_Rqst_Indicators().setValue(ldaCwfl4819.getWork_Request_Rqst_Indicators());                                                    //Natural: MOVE WORK-REQUEST.RQST-INDICATORS TO WORK-REQUEST-NEW.RQST-INDICATORS
            ldaCwfl4899.getWork_Request_New_D52().setValue(ldaCwfl4819.getWork_Request_C52());                                                                            //Natural: MOVE WORK-REQUEST.C52 TO WORK-REQUEST-NEW.D52
            ldaCwfl4899.getWork_Request_New_Part_Bus_Days().setValue(ldaCwfl4819.getWork_Request_Part_Bus_Days());                                                        //Natural: MOVE WORK-REQUEST.PART-BUS-DAYS TO WORK-REQUEST-NEW.PART-BUS-DAYS
            ldaCwfl4899.getWork_Request_New_D53().setValue(ldaCwfl4819.getWork_Request_C53());                                                                            //Natural: MOVE WORK-REQUEST.C53 TO WORK-REQUEST-NEW.D53
            ldaCwfl4899.getWork_Request_New_Part_Cal_Days().setValue(ldaCwfl4819.getWork_Request_Part_Cal_Days());                                                        //Natural: MOVE WORK-REQUEST.PART-CAL-DAYS TO WORK-REQUEST-NEW.PART-CAL-DAYS
            ldaCwfl4899.getWork_Request_New_D54().setValue(ldaCwfl4819.getWork_Request_C54());                                                                            //Natural: MOVE WORK-REQUEST.C54 TO WORK-REQUEST-NEW.D54
            ldaCwfl4899.getWork_Request_New_Start_Dte_Type().setValue(ldaCwfl4819.getWork_Request_Start_Dte_Type());                                                      //Natural: MOVE WORK-REQUEST.START-DTE-TYPE TO WORK-REQUEST-NEW.START-DTE-TYPE
            ldaCwfl4899.getWork_Request_New_D55().setValue(ldaCwfl4819.getWork_Request_C55());                                                                            //Natural: MOVE WORK-REQUEST.C55 TO WORK-REQUEST-NEW.D55
            ldaCwfl4899.getWork_Request_New_Acknldg_Dte_Tme_A().setValue(ldaCwfl4819.getWork_Request_Acknldg_Dte_Tme_A());                                                //Natural: MOVE WORK-REQUEST.ACKNLDG-DTE-TME-A TO WORK-REQUEST-NEW.ACKNLDG-DTE-TME-A
            ldaCwfl4899.getWork_Request_New_D56().setValue(ldaCwfl4819.getWork_Request_C56());                                                                            //Natural: MOVE WORK-REQUEST.C56 TO WORK-REQUEST-NEW.D56
            ldaCwfl4899.getWork_Request_New_Acknldg_Tat_Tme().setValue(ldaCwfl4819.getWork_Request_Acknldg_Tat_Tme());                                                    //Natural: MOVE WORK-REQUEST.ACKNLDG-TAT-TME TO WORK-REQUEST-NEW.ACKNLDG-TAT-TME
            ldaCwfl4899.getWork_Request_New_D57().setValue(ldaCwfl4819.getWork_Request_C57());                                                                            //Natural: MOVE WORK-REQUEST.C57 TO WORK-REQUEST-NEW.D57
            ldaCwfl4899.getWork_Request_New_Acknldg_Reqd_Ind().setValue(ldaCwfl4819.getWork_Request_Acknldg_Reqd_Ind());                                                  //Natural: MOVE WORK-REQUEST.ACKNLDG-REQD-IND TO WORK-REQUEST-NEW.ACKNLDG-REQD-IND
            ldaCwfl4899.getWork_Request_New_D58().setValue(ldaCwfl4819.getWork_Request_C58());                                                                            //Natural: MOVE WORK-REQUEST.C58 TO WORK-REQUEST-NEW.D58
            ldaCwfl4899.getWork_Request_New_Acknldg_On_Time_Ind().setValue(ldaCwfl4819.getWork_Request_Acknldg_On_Time_Ind());                                            //Natural: MOVE WORK-REQUEST.ACKNLDG-ON-TIME-IND TO WORK-REQUEST-NEW.ACKNLDG-ON-TIME-IND
            ldaCwfl4899.getWork_Request_New_D59().setValue(ldaCwfl4819.getWork_Request_C59());                                                                            //Natural: MOVE WORK-REQUEST.C59 TO WORK-REQUEST-NEW.D59
            ldaCwfl4899.getWork_Request_New_Last_Chnge_Dte_Tme_A().setValue(ldaCwfl4819.getWork_Request_Last_Chnge_Dte_Tme_A());                                          //Natural: MOVE WORK-REQUEST.LAST-CHNGE-DTE-TME-A TO WORK-REQUEST-NEW.LAST-CHNGE-DTE-TME-A
            ldaCwfl4899.getWork_Request_New_D60().setValue(ldaCwfl4819.getWork_Request_C60());                                                                            //Natural: MOVE WORK-REQUEST.C60 TO WORK-REQUEST-NEW.D60
            ldaCwfl4899.getWork_Request_New_Part_Close_Status_A().setValue(ldaCwfl4819.getWork_Request_Part_Close_Status_A());                                            //Natural: MOVE WORK-REQUEST.PART-CLOSE-STATUS-A TO WORK-REQUEST-NEW.PART-CLOSE-STATUS-A
            ldaCwfl4899.getWork_Request_New_D61().setValue(ldaCwfl4819.getWork_Request_C61());                                                                            //Natural: MOVE WORK-REQUEST.C61 TO WORK-REQUEST-NEW.D61
            ldaCwfl4899.getWork_Request_New_Cnnctd_Rqst_Ind().setValue(ldaCwfl4819.getWork_Request_Cnnctd_Rqst_Ind());                                                    //Natural: MOVE WORK-REQUEST.CNNCTD-RQST-IND TO WORK-REQUEST-NEW.CNNCTD-RQST-IND
            ldaCwfl4899.getWork_Request_New_D62().setValue(ldaCwfl4819.getWork_Request_C62());                                                                            //Natural: MOVE WORK-REQUEST.C62 TO WORK-REQUEST-NEW.D62
            ldaCwfl4899.getWork_Request_New_Prvte_Wrk_Rqst_Ind().setValue(ldaCwfl4819.getWork_Request_Prvte_Wrk_Rqst_Ind());                                              //Natural: MOVE WORK-REQUEST.PRVTE-WRK-RQST-IND TO WORK-REQUEST-NEW.PRVTE-WRK-RQST-IND
            ldaCwfl4899.getWork_Request_New_D63().setValue(ldaCwfl4819.getWork_Request_C63());                                                                            //Natural: MOVE WORK-REQUEST.C63 TO WORK-REQUEST-NEW.D63
            ldaCwfl4899.getWork_Request_New_Instn_Rqst_Log_Dte_Tme_A().setValue(ldaCwfl4819.getWork_Request_Instn_Rqst_Log_Dte_Tme_A());                                  //Natural: MOVE WORK-REQUEST.INSTN-RQST-LOG-DTE-TME-A TO WORK-REQUEST-NEW.INSTN-RQST-LOG-DTE-TME-A
            ldaCwfl4899.getWork_Request_New_D64().setValue(ldaCwfl4819.getWork_Request_C64());                                                                            //Natural: MOVE WORK-REQUEST.C64 TO WORK-REQUEST-NEW.D64
            ldaCwfl4899.getWork_Request_New_Log_Sqnce_Nbr().setValue(ldaCwfl4819.getWork_Request_Log_Sqnce_Nbr());                                                        //Natural: MOVE WORK-REQUEST.LOG-SQNCE-NBR TO WORK-REQUEST-NEW.LOG-SQNCE-NBR
            ldaCwfl4899.getWork_Request_New_D65().setValue(ldaCwfl4819.getWork_Request_C65());                                                                            //Natural: MOVE WORK-REQUEST.C65 TO WORK-REQUEST-NEW.D65
            ldaCwfl4899.getWork_Request_New_Start_Dte_Tme_A().setValue(ldaCwfl4819.getWork_Request_Start_Dte_Tme_A());                                                    //Natural: MOVE WORK-REQUEST.START-DTE-TME-A TO WORK-REQUEST-NEW.START-DTE-TME-A
            ldaCwfl4899.getWork_Request_New_D66().setValue(ldaCwfl4819.getWork_Request_C66());                                                                            //Natural: MOVE WORK-REQUEST.C66 TO WORK-REQUEST-NEW.D66
            ldaCwfl4899.getWork_Request_New_Admin_Status_Cde().setValue(ldaCwfl4819.getWork_Request_Admin_Status_Cde());                                                  //Natural: MOVE WORK-REQUEST.ADMIN-STATUS-CDE TO WORK-REQUEST-NEW.ADMIN-STATUS-CDE
            ldaCwfl4899.getWork_Request_New_D67().setValue(ldaCwfl4819.getWork_Request_C67());                                                                            //Natural: MOVE WORK-REQUEST.C67 TO WORK-REQUEST-NEW.D67
            ldaCwfl4899.getWork_Request_New_Crrnt_Cmt_Ind().setValue(ldaCwfl4819.getWork_Request_Crrnt_Cmt_Ind());                                                        //Natural: MOVE WORK-REQUEST.CRRNT-CMT-IND TO WORK-REQUEST-NEW.CRRNT-CMT-IND
            ldaCwfl4899.getWork_Request_New_D68().setValue(ldaCwfl4819.getWork_Request_C68());                                                                            //Natural: MOVE WORK-REQUEST.C68 TO WORK-REQUEST-NEW.D68
            ldaCwfl4899.getWork_Request_New_Np_Pin().setValue(ldaCwfl4819.getWork_Request_Np_Pin());                                                                      //Natural: MOVE WORK-REQUEST.NP-PIN TO WORK-REQUEST-NEW.NP-PIN
            ldaCwfl4899.getWork_Request_New_D69().setValue(ldaCwfl4819.getWork_Request_C69());                                                                            //Natural: MOVE WORK-REQUEST.C69 TO WORK-REQUEST-NEW.D69
            ldaCwfl4899.getWork_Request_New_Dod_Not_Dte().setValue(ldaCwfl4819.getWork_Request_Dod_Not_Dte());                                                            //Natural: MOVE WORK-REQUEST.DOD-NOT-DTE TO WORK-REQUEST-NEW.DOD-NOT-DTE
            ldaCwfl4899.getWork_Request_New_D70().setValue(ldaCwfl4819.getWork_Request_C70());                                                                            //Natural: MOVE WORK-REQUEST.C70 TO WORK-REQUEST-NEW.D70
            ldaCwfl4899.getWork_Request_New_Da_Accum_Cntr().setValue(ldaCwfl4819.getWork_Request_Da_Accum_Cntr());                                                        //Natural: MOVE WORK-REQUEST.DA-ACCUM-CNTR TO WORK-REQUEST-NEW.DA-ACCUM-CNTR
            ldaCwfl4899.getWork_Request_New_D71().setValue(ldaCwfl4819.getWork_Request_C71());                                                                            //Natural: MOVE WORK-REQUEST.C71 TO WORK-REQUEST-NEW.D71
            ldaCwfl4899.getWork_Request_New_Tpa_Accum_Cntr().setValue(ldaCwfl4819.getWork_Request_Tpa_Accum_Cntr());                                                      //Natural: MOVE WORK-REQUEST.TPA-ACCUM-CNTR TO WORK-REQUEST-NEW.TPA-ACCUM-CNTR
            ldaCwfl4899.getWork_Request_New_D72().setValue(ldaCwfl4819.getWork_Request_C72());                                                                            //Natural: MOVE WORK-REQUEST.C72 TO WORK-REQUEST-NEW.D72
            ldaCwfl4899.getWork_Request_New_P_I_Accum_Cntr().setValue(ldaCwfl4819.getWork_Request_P_I_Accum_Cntr());                                                      //Natural: MOVE WORK-REQUEST.P-I-ACCUM-CNTR TO WORK-REQUEST-NEW.P-I-ACCUM-CNTR
            ldaCwfl4899.getWork_Request_New_D73().setValue(ldaCwfl4819.getWork_Request_C73());                                                                            //Natural: MOVE WORK-REQUEST.C73 TO WORK-REQUEST-NEW.D73
            ldaCwfl4899.getWork_Request_New_Ipro_Accum_Cntr().setValue(ldaCwfl4819.getWork_Request_Ipro_Accum_Cntr());                                                    //Natural: MOVE WORK-REQUEST.IPRO-ACCUM-CNTR TO WORK-REQUEST-NEW.IPRO-ACCUM-CNTR
            ldaCwfl4899.getWork_Request_New_D74().setValue(ldaCwfl4819.getWork_Request_C74());                                                                            //Natural: MOVE WORK-REQUEST.C74 TO WORK-REQUEST-NEW.D74
            ldaCwfl4899.getWork_Request_New_Tot_Accum_Cntr().setValue(ldaCwfl4819.getWork_Request_Tot_Accum_Cntr());                                                      //Natural: MOVE WORK-REQUEST.TOT-ACCUM-CNTR TO WORK-REQUEST-NEW.TOT-ACCUM-CNTR
            ldaCwfl4899.getWork_Request_New_D75().setValue(ldaCwfl4819.getWork_Request_C75());                                                                            //Natural: MOVE WORK-REQUEST.C75 TO WORK-REQUEST-NEW.D75
            ldaCwfl4899.getWork_Request_New_Multi_Prod_Cde().setValue(ldaCwfl4819.getWork_Request_Multi_Prod_Cde());                                                      //Natural: MOVE WORK-REQUEST.MULTI-PROD-CDE TO WORK-REQUEST-NEW.MULTI-PROD-CDE
            ldaCwfl4899.getWork_Request_New_D76().setValue(ldaCwfl4819.getWork_Request_C76());                                                                            //Natural: MOVE WORK-REQUEST.C76 TO WORK-REQUEST-NEW.D76
            ldaCwfl4899.getWork_Request_New_Complex_Txn_Cde().setValue(ldaCwfl4819.getWork_Request_Complex_Txn_Cde());                                                    //Natural: MOVE WORK-REQUEST.COMPLEX-TXN-CDE TO WORK-REQUEST-NEW.COMPLEX-TXN-CDE
            ldaCwfl4899.getWork_Request_New_D77().setValue(ldaCwfl4819.getWork_Request_C77());                                                                            //Natural: MOVE WORK-REQUEST.C77 TO WORK-REQUEST-NEW.D77
            ldaCwfl4899.getWork_Request_New_Part_Age().setValue(ldaCwfl4819.getWork_Request_Part_Age());                                                                  //Natural: MOVE WORK-REQUEST.PART-AGE TO WORK-REQUEST-NEW.PART-AGE
            ldaCwfl4899.getWork_Request_New_D78().setValue(ldaCwfl4819.getWork_Request_C78());                                                                            //Natural: MOVE WORK-REQUEST.C78 TO WORK-REQUEST-NEW.D78
            if (condition(DbsUtil.maskMatches(ldaCwfl4819.getWork_Request_Pin_Npin(),"N...........")))                                                                    //Natural: IF WORK-REQUEST.PIN-NPIN EQ MASK ( N........... )
            {
                ldaCwfl4899.getWork_Request_New_Pin_Npin().setValue(ldaCwfl4819.getWork_Request_Pin_Npin().getSubstring(6,7));                                            //Natural: MOVE SUBSTR ( WORK-REQUEST.PIN-NPIN,6,7 ) TO WORK-REQUEST-NEW.PIN-NPIN
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaCwfl4899.getWork_Request_New_Pin_Npin().setValue(ldaCwfl4819.getWork_Request_Pin_Npin().getSubstring(1,7));                                            //Natural: MOVE SUBSTR ( WORK-REQUEST.PIN-NPIN,1,7 ) TO WORK-REQUEST-NEW.PIN-NPIN
            }                                                                                                                                                             //Natural: END-IF
            ldaCwfl4899.getWork_Request_New_D79().setValue(ldaCwfl4819.getWork_Request_C79());                                                                            //Natural: MOVE WORK-REQUEST.C79 TO WORK-REQUEST-NEW.D79
            ldaCwfl4899.getWork_Request_New_Pin_Type_Ind().setValue(ldaCwfl4819.getWork_Request_Pin_Type_Ind());                                                          //Natural: MOVE WORK-REQUEST.PIN-TYPE-IND TO WORK-REQUEST-NEW.PIN-TYPE-IND
            ldaCwfl4899.getWork_Request_New_D80().setValue(ldaCwfl4819.getWork_Request_C80());                                                                            //Natural: MOVE WORK-REQUEST.C80 TO WORK-REQUEST-NEW.D80
            ldaCwfl4899.getWork_Request_New_Rqst_Log_Dte_Tme_D().setValue(ldaCwfl4819.getWork_Request_Rqst_Log_Dte_Tme_D());                                              //Natural: MOVE WORK-REQUEST.RQST-LOG-DTE-TME-D TO WORK-REQUEST-NEW.RQST-LOG-DTE-TME-D
            ldaCwfl4899.getWork_Request_New_D81().setValue(ldaCwfl4819.getWork_Request_C81());                                                                            //Natural: MOVE WORK-REQUEST.C81 TO WORK-REQUEST-NEW.D81
            ldaCwfl4899.getWork_Request_New_Admin_Unit_Cde().setValue(ldaCwfl4819.getWork_Request_Admin_Unit_Cde());                                                      //Natural: MOVE WORK-REQUEST.ADMIN-UNIT-CDE TO WORK-REQUEST-NEW.ADMIN-UNIT-CDE
            ldaCwfl4899.getWork_Request_New_D82().setValue(ldaCwfl4819.getWork_Request_C82());                                                                            //Natural: MOVE WORK-REQUEST.C82 TO WORK-REQUEST-NEW.D82
            ldaCwfl4899.getWork_Request_New_Last_Updte_Oprtr_Cde().setValue(ldaCwfl4819.getWork_Request_Last_Updte_Oprtr_Cde());                                          //Natural: MOVE WORK-REQUEST.LAST-UPDTE-OPRTR-CDE TO WORK-REQUEST-NEW.LAST-UPDTE-OPRTR-CDE
            ldaCwfl4899.getWork_Request_New_D83().setValue(ldaCwfl4819.getWork_Request_C83());                                                                            //Natural: MOVE WORK-REQUEST.C83 TO WORK-REQUEST-NEW.D83
            ldaCwfl4899.getWork_Request_New_Due_Dte_Prty().setValue(ldaCwfl4819.getWork_Request_Due_Dte_Prty());                                                          //Natural: MOVE WORK-REQUEST.DUE-DTE-PRTY TO WORK-REQUEST-NEW.DUE-DTE-PRTY
            //*    WRITE WORK FILE 3 WORK-REQUEST
            getWorkFiles().write(3, false, ldaCwfl4899.getWork_Request_New());                                                                                            //Natural: WRITE WORK FILE 3 WORK-REQUEST-NEW
            pnd_Wrqst_Upd_Cntr.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #WRQST-UPD-CNTR
                                                                                                                                                                          //Natural: PERFORM FILL-N-WRITE-CONTRACT
            sub_Fill_N_Write_Contract();
            if (condition(Global.isEscape())) {return;}
            getWorkFiles().write(22, false, ldaCwfl4828.getExtra_Table());                                                                                                //Natural: WRITE WORK FILE 22 EXTRA-TABLE
            pnd_Adtnl_Wrk_Upd.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #ADTNL-WRK-UPD
        }                                                                                                                                                                 //Natural: WHEN #ADD
        else if (condition(pnd_Add.getBoolean()))
        {
            decideConditionsMet2520++;
            //*    WRITE WORK FILE 2 WORK-REQUEST
            ldaCwfl4899.getWork_Request_New().reset();                                                                                                                    //Natural: RESET WORK-REQUEST-NEW
            ldaCwfl4899.getWork_Request_New_Rqst_Log_Dte_Tme_A().setValue(ldaCwfl4819.getWork_Request_Rqst_Log_Dte_Tme_A());                                              //Natural: MOVE WORK-REQUEST.RQST-LOG-DTE-TME-A TO WORK-REQUEST-NEW.RQST-LOG-DTE-TME-A
            ldaCwfl4899.getWork_Request_New_D1().setValue(ldaCwfl4819.getWork_Request_C1());                                                                              //Natural: MOVE WORK-REQUEST.C1 TO WORK-REQUEST-NEW.D1
            ldaCwfl4899.getWork_Request_New_Work_Prcss_Id().setValue(ldaCwfl4819.getWork_Request_Work_Prcss_Id());                                                        //Natural: MOVE WORK-REQUEST.WORK-PRCSS-ID TO WORK-REQUEST-NEW.WORK-PRCSS-ID
            ldaCwfl4899.getWork_Request_New_D2().setValue(ldaCwfl4819.getWork_Request_C2());                                                                              //Natural: MOVE WORK-REQUEST.C2 TO WORK-REQUEST-NEW.D2
            //*    MOVE WORK-REQUEST.PIN-NBR TO WORK-REQUEST-NEW.PIN-NBR
            pnd_Pin_Nbr_A12.reset();                                                                                                                                      //Natural: RESET #PIN-NBR-A12
            pnd_Pin_Nbr_A12.moveAll(ldaCwfl4819.getWork_Request_Pin_Nbr());                                                                                               //Natural: MOVE ALL WORK-REQUEST.PIN-NBR TO #PIN-NBR-A12
            ldaCwfl4899.getWork_Request_New_Pin_Nbr().setValue(pnd_Pin_Nbr_A12_Pnd_Pin_Nbr_6_12);                                                                         //Natural: MOVE #PIN-NBR-6-12 TO WORK-REQUEST-NEW.PIN-NBR
            ldaCwfl4899.getWork_Request_New_D3().setValue(ldaCwfl4819.getWork_Request_C3());                                                                              //Natural: MOVE WORK-REQUEST.C3 TO WORK-REQUEST-NEW.D3
            ldaCwfl4899.getWork_Request_New_Part_Name().setValue(ldaCwfl4819.getWork_Request_Part_Name());                                                                //Natural: MOVE WORK-REQUEST.PART-NAME TO WORK-REQUEST-NEW.PART-NAME
            ldaCwfl4899.getWork_Request_New_D4().setValue(ldaCwfl4819.getWork_Request_C4());                                                                              //Natural: MOVE WORK-REQUEST.C4 TO WORK-REQUEST-NEW.D4
            ldaCwfl4899.getWork_Request_New_Ssn().setValue(ldaCwfl4819.getWork_Request_Ssn());                                                                            //Natural: MOVE WORK-REQUEST.SSN TO WORK-REQUEST-NEW.SSN
            ldaCwfl4899.getWork_Request_New_D5().setValue(ldaCwfl4819.getWork_Request_C5());                                                                              //Natural: MOVE WORK-REQUEST.C5 TO WORK-REQUEST-NEW.D5
            ldaCwfl4899.getWork_Request_New_Tiaa_Rcvd_Dte_Tme_A().setValue(ldaCwfl4819.getWork_Request_Tiaa_Rcvd_Dte_Tme_A());                                            //Natural: MOVE WORK-REQUEST.TIAA-RCVD-DTE-TME-A TO WORK-REQUEST-NEW.TIAA-RCVD-DTE-TME-A
            ldaCwfl4899.getWork_Request_New_D6().setValue(ldaCwfl4819.getWork_Request_C6());                                                                              //Natural: MOVE WORK-REQUEST.C6 TO WORK-REQUEST-NEW.D6
            ldaCwfl4899.getWork_Request_New_Tiaa_Rcvd_Dte_A().setValue(ldaCwfl4819.getWork_Request_Tiaa_Rcvd_Dte_A());                                                    //Natural: MOVE WORK-REQUEST.TIAA-RCVD-DTE-A TO WORK-REQUEST-NEW.TIAA-RCVD-DTE-A
            ldaCwfl4899.getWork_Request_New_D7().setValue(ldaCwfl4819.getWork_Request_C7());                                                                              //Natural: MOVE WORK-REQUEST.C7 TO WORK-REQUEST-NEW.D7
            ldaCwfl4899.getWork_Request_New_Orgnl_Unit_Cde().setValue(ldaCwfl4819.getWork_Request_Orgnl_Unit_Cde());                                                      //Natural: MOVE WORK-REQUEST.ORGNL-UNIT-CDE TO WORK-REQUEST-NEW.ORGNL-UNIT-CDE
            ldaCwfl4899.getWork_Request_New_D8().setValue(ldaCwfl4819.getWork_Request_C8());                                                                              //Natural: MOVE WORK-REQUEST.C8 TO WORK-REQUEST-NEW.D8
            ldaCwfl4899.getWork_Request_New_Rqst_Log_Oprtr_Cde().setValue(ldaCwfl4819.getWork_Request_Rqst_Log_Oprtr_Cde());                                              //Natural: MOVE WORK-REQUEST.RQST-LOG-OPRTR-CDE TO WORK-REQUEST-NEW.RQST-LOG-OPRTR-CDE
            ldaCwfl4899.getWork_Request_New_D9().setValue(ldaCwfl4819.getWork_Request_C9());                                                                              //Natural: MOVE WORK-REQUEST.C9 TO WORK-REQUEST-NEW.D9
            ldaCwfl4899.getWork_Request_New_Rqst_Orgn_Cde().setValue(ldaCwfl4819.getWork_Request_Rqst_Orgn_Cde());                                                        //Natural: MOVE WORK-REQUEST.RQST-ORGN-CDE TO WORK-REQUEST-NEW.RQST-ORGN-CDE
            ldaCwfl4899.getWork_Request_New_D10().setValue(ldaCwfl4819.getWork_Request_C10());                                                                            //Natural: MOVE WORK-REQUEST.C10 TO WORK-REQUEST-NEW.D10
            ldaCwfl4899.getWork_Request_New_Crprte_Status_Ind().setValue(ldaCwfl4819.getWork_Request_Crprte_Status_Ind());                                                //Natural: MOVE WORK-REQUEST.CRPRTE-STATUS-IND TO WORK-REQUEST-NEW.CRPRTE-STATUS-IND
            ldaCwfl4899.getWork_Request_New_D11().setValue(ldaCwfl4819.getWork_Request_C11());                                                                            //Natural: MOVE WORK-REQUEST.C11 TO WORK-REQUEST-NEW.D11
            ldaCwfl4899.getWork_Request_New_Crprte_Clock_End_Dte_Tme_A().setValue(ldaCwfl4819.getWork_Request_Crprte_Clock_End_Dte_Tme_A());                              //Natural: MOVE WORK-REQUEST.CRPRTE-CLOCK-END-DTE-TME-A TO WORK-REQUEST-NEW.CRPRTE-CLOCK-END-DTE-TME-A
            ldaCwfl4899.getWork_Request_New_D12().setValue(ldaCwfl4819.getWork_Request_C12());                                                                            //Natural: MOVE WORK-REQUEST.C12 TO WORK-REQUEST-NEW.D12
            ldaCwfl4899.getWork_Request_New_Final_Close_Out_Dte_Tme_A().setValue(ldaCwfl4819.getWork_Request_Final_Close_Out_Dte_Tme_A());                                //Natural: MOVE WORK-REQUEST.FINAL-CLOSE-OUT-DTE-TME-A TO WORK-REQUEST-NEW.FINAL-CLOSE-OUT-DTE-TME-A
            ldaCwfl4899.getWork_Request_New_D13().setValue(ldaCwfl4819.getWork_Request_C13());                                                                            //Natural: MOVE WORK-REQUEST.C13 TO WORK-REQUEST-NEW.D13
            ldaCwfl4899.getWork_Request_New_Effctve_Dte_A().setValue(ldaCwfl4819.getWork_Request_Effctve_Dte_A());                                                        //Natural: MOVE WORK-REQUEST.EFFCTVE-DTE-A TO WORK-REQUEST-NEW.EFFCTVE-DTE-A
            ldaCwfl4899.getWork_Request_New_D14().setValue(ldaCwfl4819.getWork_Request_C14());                                                                            //Natural: MOVE WORK-REQUEST.C14 TO WORK-REQUEST-NEW.D14
            ldaCwfl4899.getWork_Request_New_Trans_Dte_A().setValue(ldaCwfl4819.getWork_Request_Trans_Dte_A());                                                            //Natural: MOVE WORK-REQUEST.TRANS-DTE-A TO WORK-REQUEST-NEW.TRANS-DTE-A
            ldaCwfl4899.getWork_Request_New_D15().setValue(ldaCwfl4819.getWork_Request_C15());                                                                            //Natural: MOVE WORK-REQUEST.C15 TO WORK-REQUEST-NEW.D15
            ldaCwfl4899.getWork_Request_New_Trnsctn_Dte_A().setValue(ldaCwfl4819.getWork_Request_Trnsctn_Dte_A());                                                        //Natural: MOVE WORK-REQUEST.TRNSCTN-DTE-A TO WORK-REQUEST-NEW.TRNSCTN-DTE-A
            ldaCwfl4899.getWork_Request_New_D16().setValue(ldaCwfl4819.getWork_Request_C16());                                                                            //Natural: MOVE WORK-REQUEST.C16 TO WORK-REQUEST-NEW.D16
            ldaCwfl4899.getWork_Request_New_Owner_Unit_Cde().setValue(ldaCwfl4819.getWork_Request_Owner_Unit_Cde());                                                      //Natural: MOVE WORK-REQUEST.OWNER-UNIT-CDE TO WORK-REQUEST-NEW.OWNER-UNIT-CDE
            ldaCwfl4899.getWork_Request_New_D17().setValue(ldaCwfl4819.getWork_Request_C17());                                                                            //Natural: MOVE WORK-REQUEST.C17 TO WORK-REQUEST-NEW.D17
            ldaCwfl4899.getWork_Request_New_Owner_Division().setValue(ldaCwfl4819.getWork_Request_Owner_Division());                                                      //Natural: MOVE WORK-REQUEST.OWNER-DIVISION TO WORK-REQUEST-NEW.OWNER-DIVISION
            ldaCwfl4899.getWork_Request_New_D18().setValue(ldaCwfl4819.getWork_Request_C18());                                                                            //Natural: MOVE WORK-REQUEST.C18 TO WORK-REQUEST-NEW.D18
            ldaCwfl4899.getWork_Request_New_Shphrd_Id().setValue(ldaCwfl4819.getWork_Request_Shphrd_Id());                                                                //Natural: MOVE WORK-REQUEST.SHPHRD-ID TO WORK-REQUEST-NEW.SHPHRD-ID
            ldaCwfl4899.getWork_Request_New_D19().setValue(ldaCwfl4819.getWork_Request_C19());                                                                            //Natural: MOVE WORK-REQUEST.C19 TO WORK-REQUEST-NEW.D19
            ldaCwfl4899.getWork_Request_New_Sec_Ind().setValue(ldaCwfl4819.getWork_Request_Sec_Ind());                                                                    //Natural: MOVE WORK-REQUEST.SEC-IND TO WORK-REQUEST-NEW.SEC-IND
            ldaCwfl4899.getWork_Request_New_D20().setValue(ldaCwfl4819.getWork_Request_C20());                                                                            //Natural: MOVE WORK-REQUEST.C20 TO WORK-REQUEST-NEW.D20
            ldaCwfl4899.getWork_Request_New_Admin_Work_Ind().setValue(ldaCwfl4819.getWork_Request_Admin_Work_Ind());                                                      //Natural: MOVE WORK-REQUEST.ADMIN-WORK-IND TO WORK-REQUEST-NEW.ADMIN-WORK-IND
            ldaCwfl4899.getWork_Request_New_D21().setValue(ldaCwfl4819.getWork_Request_C21());                                                                            //Natural: MOVE WORK-REQUEST.C21 TO WORK-REQUEST-NEW.D21
            ldaCwfl4899.getWork_Request_New_Sub_Rqst_Ind().setValue(ldaCwfl4819.getWork_Request_Sub_Rqst_Ind());                                                          //Natural: MOVE WORK-REQUEST.SUB-RQST-IND TO WORK-REQUEST-NEW.SUB-RQST-IND
            ldaCwfl4899.getWork_Request_New_D22().setValue(ldaCwfl4819.getWork_Request_C22());                                                                            //Natural: MOVE WORK-REQUEST.C22 TO WORK-REQUEST-NEW.D22
            ldaCwfl4899.getWork_Request_New_Prnt_Rqst_Log_Dte_Tme_A().setValue(ldaCwfl4819.getWork_Request_Prnt_Rqst_Log_Dte_Tme_A());                                    //Natural: MOVE WORK-REQUEST.PRNT-RQST-LOG-DTE-TME-A TO WORK-REQUEST-NEW.PRNT-RQST-LOG-DTE-TME-A
            ldaCwfl4899.getWork_Request_New_D23().setValue(ldaCwfl4819.getWork_Request_C23());                                                                            //Natural: MOVE WORK-REQUEST.C23 TO WORK-REQUEST-NEW.D23
            ldaCwfl4899.getWork_Request_New_Prnt_Work_Prcss_Id().setValue(ldaCwfl4819.getWork_Request_Prnt_Work_Prcss_Id());                                              //Natural: MOVE WORK-REQUEST.PRNT-WORK-PRCSS-ID TO WORK-REQUEST-NEW.PRNT-WORK-PRCSS-ID
            ldaCwfl4899.getWork_Request_New_D24().setValue(ldaCwfl4819.getWork_Request_C24());                                                                            //Natural: MOVE WORK-REQUEST.C24 TO WORK-REQUEST-NEW.D24
            ldaCwfl4899.getWork_Request_New_Multi_Rqst_Ind().setValue(ldaCwfl4819.getWork_Request_Multi_Rqst_Ind());                                                      //Natural: MOVE WORK-REQUEST.MULTI-RQST-IND TO WORK-REQUEST-NEW.MULTI-RQST-IND
            ldaCwfl4899.getWork_Request_New_D25().setValue(ldaCwfl4819.getWork_Request_C25());                                                                            //Natural: MOVE WORK-REQUEST.C25 TO WORK-REQUEST-NEW.D25
            ldaCwfl4899.getWork_Request_New_Cmplnt_Ind().setValue(ldaCwfl4819.getWork_Request_Cmplnt_Ind());                                                              //Natural: MOVE WORK-REQUEST.CMPLNT-IND TO WORK-REQUEST-NEW.CMPLNT-IND
            ldaCwfl4899.getWork_Request_New_D26().setValue(ldaCwfl4819.getWork_Request_C26());                                                                            //Natural: MOVE WORK-REQUEST.C26 TO WORK-REQUEST-NEW.D26
            ldaCwfl4899.getWork_Request_New_Elctrnc_Fldr_Ind().setValue(ldaCwfl4819.getWork_Request_Elctrnc_Fldr_Ind());                                                  //Natural: MOVE WORK-REQUEST.ELCTRNC-FLDR-IND TO WORK-REQUEST-NEW.ELCTRNC-FLDR-IND
            ldaCwfl4899.getWork_Request_New_D27().setValue(ldaCwfl4819.getWork_Request_C27());                                                                            //Natural: MOVE WORK-REQUEST.C27 TO WORK-REQUEST-NEW.D27
            ldaCwfl4899.getWork_Request_New_Mj_Pull_Ind().setValue(ldaCwfl4819.getWork_Request_Mj_Pull_Ind());                                                            //Natural: MOVE WORK-REQUEST.MJ-PULL-IND TO WORK-REQUEST-NEW.MJ-PULL-IND
            ldaCwfl4899.getWork_Request_New_D28().setValue(ldaCwfl4819.getWork_Request_C28());                                                                            //Natural: MOVE WORK-REQUEST.C28 TO WORK-REQUEST-NEW.D28
            ldaCwfl4899.getWork_Request_New_Check_Ind().setValue(ldaCwfl4819.getWork_Request_Check_Ind());                                                                //Natural: MOVE WORK-REQUEST.CHECK-IND TO WORK-REQUEST-NEW.CHECK-IND
            ldaCwfl4899.getWork_Request_New_D29().setValue(ldaCwfl4819.getWork_Request_C29());                                                                            //Natural: MOVE WORK-REQUEST.C29 TO WORK-REQUEST-NEW.D29
            ldaCwfl4899.getWork_Request_New_Bsnss_Reply_Ind().setValue(ldaCwfl4819.getWork_Request_Bsnss_Reply_Ind());                                                    //Natural: MOVE WORK-REQUEST.BSNSS-REPLY-IND TO WORK-REQUEST-NEW.BSNSS-REPLY-IND
            ldaCwfl4899.getWork_Request_New_D30().setValue(ldaCwfl4819.getWork_Request_C30());                                                                            //Natural: MOVE WORK-REQUEST.C30 TO WORK-REQUEST-NEW.D30
            ldaCwfl4899.getWork_Request_New_Tlc_Ind().setValue(ldaCwfl4819.getWork_Request_Tlc_Ind());                                                                    //Natural: MOVE WORK-REQUEST.TLC-IND TO WORK-REQUEST-NEW.TLC-IND
            ldaCwfl4899.getWork_Request_New_D31().setValue(ldaCwfl4819.getWork_Request_C31());                                                                            //Natural: MOVE WORK-REQUEST.C31 TO WORK-REQUEST-NEW.D31
            ldaCwfl4899.getWork_Request_New_Redo_Ind().setValue(ldaCwfl4819.getWork_Request_Redo_Ind());                                                                  //Natural: MOVE WORK-REQUEST.REDO-IND TO WORK-REQUEST-NEW.REDO-IND
            ldaCwfl4899.getWork_Request_New_D32().setValue(ldaCwfl4819.getWork_Request_C32());                                                                            //Natural: MOVE WORK-REQUEST.C32 TO WORK-REQUEST-NEW.D32
            ldaCwfl4899.getWork_Request_New_Crprte_On_Tme_Ind().setValue(ldaCwfl4819.getWork_Request_Crprte_On_Tme_Ind());                                                //Natural: MOVE WORK-REQUEST.CRPRTE-ON-TME-IND TO WORK-REQUEST-NEW.CRPRTE-ON-TME-IND
            ldaCwfl4899.getWork_Request_New_D33().setValue(ldaCwfl4819.getWork_Request_C33());                                                                            //Natural: MOVE WORK-REQUEST.C33 TO WORK-REQUEST-NEW.D33
            ldaCwfl4899.getWork_Request_New_Off_Rtng_Ind().setValue(ldaCwfl4819.getWork_Request_Off_Rtng_Ind());                                                          //Natural: MOVE WORK-REQUEST.OFF-RTNG-IND TO WORK-REQUEST-NEW.OFF-RTNG-IND
            ldaCwfl4899.getWork_Request_New_D34().setValue(ldaCwfl4819.getWork_Request_C34());                                                                            //Natural: MOVE WORK-REQUEST.C34 TO WORK-REQUEST-NEW.D34
            ldaCwfl4899.getWork_Request_New_Prcssng_Type_Cde().setValue(ldaCwfl4819.getWork_Request_Prcssng_Type_Cde());                                                  //Natural: MOVE WORK-REQUEST.PRCSSNG-TYPE-CDE TO WORK-REQUEST-NEW.PRCSSNG-TYPE-CDE
            ldaCwfl4899.getWork_Request_New_D35().setValue(ldaCwfl4819.getWork_Request_C35());                                                                            //Natural: MOVE WORK-REQUEST.C35 TO WORK-REQUEST-NEW.D35
            ldaCwfl4899.getWork_Request_New_Log_Rqstr_Cde().setValue(ldaCwfl4819.getWork_Request_Log_Rqstr_Cde());                                                        //Natural: MOVE WORK-REQUEST.LOG-RQSTR-CDE TO WORK-REQUEST-NEW.LOG-RQSTR-CDE
            ldaCwfl4899.getWork_Request_New_D36().setValue(ldaCwfl4819.getWork_Request_C36());                                                                            //Natural: MOVE WORK-REQUEST.C36 TO WORK-REQUEST-NEW.D36
            ldaCwfl4899.getWork_Request_New_Log_Insttn_Srce_Cde().setValue(ldaCwfl4819.getWork_Request_Log_Insttn_Srce_Cde());                                            //Natural: MOVE WORK-REQUEST.LOG-INSTTN-SRCE-CDE TO WORK-REQUEST-NEW.LOG-INSTTN-SRCE-CDE
            ldaCwfl4899.getWork_Request_New_D37().setValue(ldaCwfl4819.getWork_Request_C37());                                                                            //Natural: MOVE WORK-REQUEST.C37 TO WORK-REQUEST-NEW.D37
            ldaCwfl4899.getWork_Request_New_Instn_Cde().setValue(ldaCwfl4819.getWork_Request_Instn_Cde());                                                                //Natural: MOVE WORK-REQUEST.INSTN-CDE TO WORK-REQUEST-NEW.INSTN-CDE
            ldaCwfl4899.getWork_Request_New_D38().setValue(ldaCwfl4819.getWork_Request_C38());                                                                            //Natural: MOVE WORK-REQUEST.C38 TO WORK-REQUEST-NEW.D38
            ldaCwfl4899.getWork_Request_New_Rqst_Instn_Cde().setValue(ldaCwfl4819.getWork_Request_Rqst_Instn_Cde());                                                      //Natural: MOVE WORK-REQUEST.RQST-INSTN-CDE TO WORK-REQUEST-NEW.RQST-INSTN-CDE
            ldaCwfl4899.getWork_Request_New_D39().setValue(ldaCwfl4819.getWork_Request_C39());                                                                            //Natural: MOVE WORK-REQUEST.C39 TO WORK-REQUEST-NEW.D39
            ldaCwfl4899.getWork_Request_New_Rqst_Rgn_Cde().setValue(ldaCwfl4819.getWork_Request_Rqst_Rgn_Cde());                                                          //Natural: MOVE WORK-REQUEST.RQST-RGN-CDE TO WORK-REQUEST-NEW.RQST-RGN-CDE
            ldaCwfl4899.getWork_Request_New_D40().setValue(ldaCwfl4819.getWork_Request_C40());                                                                            //Natural: MOVE WORK-REQUEST.C40 TO WORK-REQUEST-NEW.D40
            ldaCwfl4899.getWork_Request_New_Rqst_Spcl_Dsgntn_Cde().setValue(ldaCwfl4819.getWork_Request_Rqst_Spcl_Dsgntn_Cde());                                          //Natural: MOVE WORK-REQUEST.RQST-SPCL-DSGNTN-CDE TO WORK-REQUEST-NEW.RQST-SPCL-DSGNTN-CDE
            ldaCwfl4899.getWork_Request_New_D41().setValue(ldaCwfl4819.getWork_Request_C41());                                                                            //Natural: MOVE WORK-REQUEST.C41 TO WORK-REQUEST-NEW.D41
            ldaCwfl4899.getWork_Request_New_Rqst_Brnch_Cde().setValue(ldaCwfl4819.getWork_Request_Rqst_Brnch_Cde());                                                      //Natural: MOVE WORK-REQUEST.RQST-BRNCH-CDE TO WORK-REQUEST-NEW.RQST-BRNCH-CDE
            ldaCwfl4899.getWork_Request_New_D42().setValue(ldaCwfl4819.getWork_Request_C42());                                                                            //Natural: MOVE WORK-REQUEST.C42 TO WORK-REQUEST-NEW.D42
            ldaCwfl4899.getWork_Request_New_Extrnl_Pend_Ind().setValue(ldaCwfl4819.getWork_Request_Extrnl_Pend_Ind());                                                    //Natural: MOVE WORK-REQUEST.EXTRNL-PEND-IND TO WORK-REQUEST-NEW.EXTRNL-PEND-IND
            ldaCwfl4899.getWork_Request_New_D43().setValue(ldaCwfl4819.getWork_Request_C43());                                                                            //Natural: MOVE WORK-REQUEST.C43 TO WORK-REQUEST-NEW.D43
            ldaCwfl4899.getWork_Request_New_Crprte_Due_Dte_Tme_A().setValue(ldaCwfl4819.getWork_Request_Crprte_Due_Dte_Tme_A());                                          //Natural: MOVE WORK-REQUEST.CRPRTE-DUE-DTE-TME-A TO WORK-REQUEST-NEW.CRPRTE-DUE-DTE-TME-A
            ldaCwfl4899.getWork_Request_New_D44().setValue(ldaCwfl4819.getWork_Request_C44());                                                                            //Natural: MOVE WORK-REQUEST.C44 TO WORK-REQUEST-NEW.D44
            ldaCwfl4899.getWork_Request_New_Mis_Routed_Ind().setValue(ldaCwfl4819.getWork_Request_Mis_Routed_Ind());                                                      //Natural: MOVE WORK-REQUEST.MIS-ROUTED-IND TO WORK-REQUEST-NEW.MIS-ROUTED-IND
            ldaCwfl4899.getWork_Request_New_D45().setValue(ldaCwfl4819.getWork_Request_C45());                                                                            //Natural: MOVE WORK-REQUEST.C45 TO WORK-REQUEST-NEW.D45
            ldaCwfl4899.getWork_Request_New_Dte_Of_Birth().setValue(ldaCwfl4819.getWork_Request_Dte_Of_Birth());                                                          //Natural: MOVE WORK-REQUEST.DTE-OF-BIRTH TO WORK-REQUEST-NEW.DTE-OF-BIRTH
            ldaCwfl4899.getWork_Request_New_D46().setValue(ldaCwfl4819.getWork_Request_C46());                                                                            //Natural: MOVE WORK-REQUEST.C46 TO WORK-REQUEST-NEW.D46
            ldaCwfl4899.getWork_Request_New_Trade_Dte_Tme_A().setValue(ldaCwfl4819.getWork_Request_Trade_Dte_Tme_A());                                                    //Natural: MOVE WORK-REQUEST.TRADE-DTE-TME-A TO WORK-REQUEST-NEW.TRADE-DTE-TME-A
            ldaCwfl4899.getWork_Request_New_D47().setValue(ldaCwfl4819.getWork_Request_C47());                                                                            //Natural: MOVE WORK-REQUEST.C47 TO WORK-REQUEST-NEW.D47
            ldaCwfl4899.getWork_Request_New_Mj_Media_Ind().setValue(ldaCwfl4819.getWork_Request_Mj_Media_Ind());                                                          //Natural: MOVE WORK-REQUEST.MJ-MEDIA-IND TO WORK-REQUEST-NEW.MJ-MEDIA-IND
            ldaCwfl4899.getWork_Request_New_D48().setValue(ldaCwfl4819.getWork_Request_C48());                                                                            //Natural: MOVE WORK-REQUEST.C48 TO WORK-REQUEST-NEW.D48
            ldaCwfl4899.getWork_Request_New_State_Of_Res().setValue(ldaCwfl4819.getWork_Request_State_Of_Res());                                                          //Natural: MOVE WORK-REQUEST.STATE-OF-RES TO WORK-REQUEST-NEW.STATE-OF-RES
            ldaCwfl4899.getWork_Request_New_D49().setValue(ldaCwfl4819.getWork_Request_C49());                                                                            //Natural: MOVE WORK-REQUEST.C49 TO WORK-REQUEST-NEW.D49
            ldaCwfl4899.getWork_Request_New_Sec_Turnaround_Tme_A().setValue(ldaCwfl4819.getWork_Request_Sec_Turnaround_Tme_A());                                          //Natural: MOVE WORK-REQUEST.SEC-TURNAROUND-TME-A TO WORK-REQUEST-NEW.SEC-TURNAROUND-TME-A
            ldaCwfl4899.getWork_Request_New_D50().setValue(ldaCwfl4819.getWork_Request_C50());                                                                            //Natural: MOVE WORK-REQUEST.C50 TO WORK-REQUEST-NEW.D50
            ldaCwfl4899.getWork_Request_New_Sec_Updte_Dte_A().setValue(ldaCwfl4819.getWork_Request_Sec_Updte_Dte_A());                                                    //Natural: MOVE WORK-REQUEST.SEC-UPDTE-DTE-A TO WORK-REQUEST-NEW.SEC-UPDTE-DTE-A
            ldaCwfl4899.getWork_Request_New_D51().setValue(ldaCwfl4819.getWork_Request_C51());                                                                            //Natural: MOVE WORK-REQUEST.C51 TO WORK-REQUEST-NEW.D51
            ldaCwfl4899.getWork_Request_New_Rqst_Indicators().setValue(ldaCwfl4819.getWork_Request_Rqst_Indicators());                                                    //Natural: MOVE WORK-REQUEST.RQST-INDICATORS TO WORK-REQUEST-NEW.RQST-INDICATORS
            ldaCwfl4899.getWork_Request_New_D52().setValue(ldaCwfl4819.getWork_Request_C52());                                                                            //Natural: MOVE WORK-REQUEST.C52 TO WORK-REQUEST-NEW.D52
            ldaCwfl4899.getWork_Request_New_Part_Bus_Days().setValue(ldaCwfl4819.getWork_Request_Part_Bus_Days());                                                        //Natural: MOVE WORK-REQUEST.PART-BUS-DAYS TO WORK-REQUEST-NEW.PART-BUS-DAYS
            ldaCwfl4899.getWork_Request_New_D53().setValue(ldaCwfl4819.getWork_Request_C53());                                                                            //Natural: MOVE WORK-REQUEST.C53 TO WORK-REQUEST-NEW.D53
            ldaCwfl4899.getWork_Request_New_Part_Cal_Days().setValue(ldaCwfl4819.getWork_Request_Part_Cal_Days());                                                        //Natural: MOVE WORK-REQUEST.PART-CAL-DAYS TO WORK-REQUEST-NEW.PART-CAL-DAYS
            ldaCwfl4899.getWork_Request_New_D54().setValue(ldaCwfl4819.getWork_Request_C54());                                                                            //Natural: MOVE WORK-REQUEST.C54 TO WORK-REQUEST-NEW.D54
            ldaCwfl4899.getWork_Request_New_Start_Dte_Type().setValue(ldaCwfl4819.getWork_Request_Start_Dte_Type());                                                      //Natural: MOVE WORK-REQUEST.START-DTE-TYPE TO WORK-REQUEST-NEW.START-DTE-TYPE
            ldaCwfl4899.getWork_Request_New_D55().setValue(ldaCwfl4819.getWork_Request_C55());                                                                            //Natural: MOVE WORK-REQUEST.C55 TO WORK-REQUEST-NEW.D55
            ldaCwfl4899.getWork_Request_New_Acknldg_Dte_Tme_A().setValue(ldaCwfl4819.getWork_Request_Acknldg_Dte_Tme_A());                                                //Natural: MOVE WORK-REQUEST.ACKNLDG-DTE-TME-A TO WORK-REQUEST-NEW.ACKNLDG-DTE-TME-A
            ldaCwfl4899.getWork_Request_New_D56().setValue(ldaCwfl4819.getWork_Request_C56());                                                                            //Natural: MOVE WORK-REQUEST.C56 TO WORK-REQUEST-NEW.D56
            ldaCwfl4899.getWork_Request_New_Acknldg_Tat_Tme().setValue(ldaCwfl4819.getWork_Request_Acknldg_Tat_Tme());                                                    //Natural: MOVE WORK-REQUEST.ACKNLDG-TAT-TME TO WORK-REQUEST-NEW.ACKNLDG-TAT-TME
            ldaCwfl4899.getWork_Request_New_D57().setValue(ldaCwfl4819.getWork_Request_C57());                                                                            //Natural: MOVE WORK-REQUEST.C57 TO WORK-REQUEST-NEW.D57
            ldaCwfl4899.getWork_Request_New_Acknldg_Reqd_Ind().setValue(ldaCwfl4819.getWork_Request_Acknldg_Reqd_Ind());                                                  //Natural: MOVE WORK-REQUEST.ACKNLDG-REQD-IND TO WORK-REQUEST-NEW.ACKNLDG-REQD-IND
            ldaCwfl4899.getWork_Request_New_D58().setValue(ldaCwfl4819.getWork_Request_C58());                                                                            //Natural: MOVE WORK-REQUEST.C58 TO WORK-REQUEST-NEW.D58
            ldaCwfl4899.getWork_Request_New_Acknldg_On_Time_Ind().setValue(ldaCwfl4819.getWork_Request_Acknldg_On_Time_Ind());                                            //Natural: MOVE WORK-REQUEST.ACKNLDG-ON-TIME-IND TO WORK-REQUEST-NEW.ACKNLDG-ON-TIME-IND
            ldaCwfl4899.getWork_Request_New_D59().setValue(ldaCwfl4819.getWork_Request_C59());                                                                            //Natural: MOVE WORK-REQUEST.C59 TO WORK-REQUEST-NEW.D59
            ldaCwfl4899.getWork_Request_New_Last_Chnge_Dte_Tme_A().setValue(ldaCwfl4819.getWork_Request_Last_Chnge_Dte_Tme_A());                                          //Natural: MOVE WORK-REQUEST.LAST-CHNGE-DTE-TME-A TO WORK-REQUEST-NEW.LAST-CHNGE-DTE-TME-A
            ldaCwfl4899.getWork_Request_New_D60().setValue(ldaCwfl4819.getWork_Request_C60());                                                                            //Natural: MOVE WORK-REQUEST.C60 TO WORK-REQUEST-NEW.D60
            ldaCwfl4899.getWork_Request_New_Part_Close_Status_A().setValue(ldaCwfl4819.getWork_Request_Part_Close_Status_A());                                            //Natural: MOVE WORK-REQUEST.PART-CLOSE-STATUS-A TO WORK-REQUEST-NEW.PART-CLOSE-STATUS-A
            ldaCwfl4899.getWork_Request_New_D61().setValue(ldaCwfl4819.getWork_Request_C61());                                                                            //Natural: MOVE WORK-REQUEST.C61 TO WORK-REQUEST-NEW.D61
            ldaCwfl4899.getWork_Request_New_Cnnctd_Rqst_Ind().setValue(ldaCwfl4819.getWork_Request_Cnnctd_Rqst_Ind());                                                    //Natural: MOVE WORK-REQUEST.CNNCTD-RQST-IND TO WORK-REQUEST-NEW.CNNCTD-RQST-IND
            ldaCwfl4899.getWork_Request_New_D62().setValue(ldaCwfl4819.getWork_Request_C62());                                                                            //Natural: MOVE WORK-REQUEST.C62 TO WORK-REQUEST-NEW.D62
            ldaCwfl4899.getWork_Request_New_Prvte_Wrk_Rqst_Ind().setValue(ldaCwfl4819.getWork_Request_Prvte_Wrk_Rqst_Ind());                                              //Natural: MOVE WORK-REQUEST.PRVTE-WRK-RQST-IND TO WORK-REQUEST-NEW.PRVTE-WRK-RQST-IND
            ldaCwfl4899.getWork_Request_New_D63().setValue(ldaCwfl4819.getWork_Request_C63());                                                                            //Natural: MOVE WORK-REQUEST.C63 TO WORK-REQUEST-NEW.D63
            ldaCwfl4899.getWork_Request_New_Instn_Rqst_Log_Dte_Tme_A().setValue(ldaCwfl4819.getWork_Request_Instn_Rqst_Log_Dte_Tme_A());                                  //Natural: MOVE WORK-REQUEST.INSTN-RQST-LOG-DTE-TME-A TO WORK-REQUEST-NEW.INSTN-RQST-LOG-DTE-TME-A
            ldaCwfl4899.getWork_Request_New_D64().setValue(ldaCwfl4819.getWork_Request_C64());                                                                            //Natural: MOVE WORK-REQUEST.C64 TO WORK-REQUEST-NEW.D64
            ldaCwfl4899.getWork_Request_New_Log_Sqnce_Nbr().setValue(ldaCwfl4819.getWork_Request_Log_Sqnce_Nbr());                                                        //Natural: MOVE WORK-REQUEST.LOG-SQNCE-NBR TO WORK-REQUEST-NEW.LOG-SQNCE-NBR
            ldaCwfl4899.getWork_Request_New_D65().setValue(ldaCwfl4819.getWork_Request_C65());                                                                            //Natural: MOVE WORK-REQUEST.C65 TO WORK-REQUEST-NEW.D65
            ldaCwfl4899.getWork_Request_New_Start_Dte_Tme_A().setValue(ldaCwfl4819.getWork_Request_Start_Dte_Tme_A());                                                    //Natural: MOVE WORK-REQUEST.START-DTE-TME-A TO WORK-REQUEST-NEW.START-DTE-TME-A
            ldaCwfl4899.getWork_Request_New_D66().setValue(ldaCwfl4819.getWork_Request_C66());                                                                            //Natural: MOVE WORK-REQUEST.C66 TO WORK-REQUEST-NEW.D66
            ldaCwfl4899.getWork_Request_New_Admin_Status_Cde().setValue(ldaCwfl4819.getWork_Request_Admin_Status_Cde());                                                  //Natural: MOVE WORK-REQUEST.ADMIN-STATUS-CDE TO WORK-REQUEST-NEW.ADMIN-STATUS-CDE
            ldaCwfl4899.getWork_Request_New_D67().setValue(ldaCwfl4819.getWork_Request_C67());                                                                            //Natural: MOVE WORK-REQUEST.C67 TO WORK-REQUEST-NEW.D67
            ldaCwfl4899.getWork_Request_New_Crrnt_Cmt_Ind().setValue(ldaCwfl4819.getWork_Request_Crrnt_Cmt_Ind());                                                        //Natural: MOVE WORK-REQUEST.CRRNT-CMT-IND TO WORK-REQUEST-NEW.CRRNT-CMT-IND
            ldaCwfl4899.getWork_Request_New_D68().setValue(ldaCwfl4819.getWork_Request_C68());                                                                            //Natural: MOVE WORK-REQUEST.C68 TO WORK-REQUEST-NEW.D68
            ldaCwfl4899.getWork_Request_New_Np_Pin().setValue(ldaCwfl4819.getWork_Request_Np_Pin());                                                                      //Natural: MOVE WORK-REQUEST.NP-PIN TO WORK-REQUEST-NEW.NP-PIN
            ldaCwfl4899.getWork_Request_New_D69().setValue(ldaCwfl4819.getWork_Request_C69());                                                                            //Natural: MOVE WORK-REQUEST.C69 TO WORK-REQUEST-NEW.D69
            ldaCwfl4899.getWork_Request_New_Dod_Not_Dte().setValue(ldaCwfl4819.getWork_Request_Dod_Not_Dte());                                                            //Natural: MOVE WORK-REQUEST.DOD-NOT-DTE TO WORK-REQUEST-NEW.DOD-NOT-DTE
            ldaCwfl4899.getWork_Request_New_D70().setValue(ldaCwfl4819.getWork_Request_C70());                                                                            //Natural: MOVE WORK-REQUEST.C70 TO WORK-REQUEST-NEW.D70
            ldaCwfl4899.getWork_Request_New_Da_Accum_Cntr().setValue(ldaCwfl4819.getWork_Request_Da_Accum_Cntr());                                                        //Natural: MOVE WORK-REQUEST.DA-ACCUM-CNTR TO WORK-REQUEST-NEW.DA-ACCUM-CNTR
            ldaCwfl4899.getWork_Request_New_D71().setValue(ldaCwfl4819.getWork_Request_C71());                                                                            //Natural: MOVE WORK-REQUEST.C71 TO WORK-REQUEST-NEW.D71
            ldaCwfl4899.getWork_Request_New_Tpa_Accum_Cntr().setValue(ldaCwfl4819.getWork_Request_Tpa_Accum_Cntr());                                                      //Natural: MOVE WORK-REQUEST.TPA-ACCUM-CNTR TO WORK-REQUEST-NEW.TPA-ACCUM-CNTR
            ldaCwfl4899.getWork_Request_New_D72().setValue(ldaCwfl4819.getWork_Request_C72());                                                                            //Natural: MOVE WORK-REQUEST.C72 TO WORK-REQUEST-NEW.D72
            ldaCwfl4899.getWork_Request_New_P_I_Accum_Cntr().setValue(ldaCwfl4819.getWork_Request_P_I_Accum_Cntr());                                                      //Natural: MOVE WORK-REQUEST.P-I-ACCUM-CNTR TO WORK-REQUEST-NEW.P-I-ACCUM-CNTR
            ldaCwfl4899.getWork_Request_New_D73().setValue(ldaCwfl4819.getWork_Request_C73());                                                                            //Natural: MOVE WORK-REQUEST.C73 TO WORK-REQUEST-NEW.D73
            ldaCwfl4899.getWork_Request_New_Ipro_Accum_Cntr().setValue(ldaCwfl4819.getWork_Request_Ipro_Accum_Cntr());                                                    //Natural: MOVE WORK-REQUEST.IPRO-ACCUM-CNTR TO WORK-REQUEST-NEW.IPRO-ACCUM-CNTR
            ldaCwfl4899.getWork_Request_New_D74().setValue(ldaCwfl4819.getWork_Request_C74());                                                                            //Natural: MOVE WORK-REQUEST.C74 TO WORK-REQUEST-NEW.D74
            ldaCwfl4899.getWork_Request_New_Tot_Accum_Cntr().setValue(ldaCwfl4819.getWork_Request_Tot_Accum_Cntr());                                                      //Natural: MOVE WORK-REQUEST.TOT-ACCUM-CNTR TO WORK-REQUEST-NEW.TOT-ACCUM-CNTR
            ldaCwfl4899.getWork_Request_New_D75().setValue(ldaCwfl4819.getWork_Request_C75());                                                                            //Natural: MOVE WORK-REQUEST.C75 TO WORK-REQUEST-NEW.D75
            ldaCwfl4899.getWork_Request_New_Multi_Prod_Cde().setValue(ldaCwfl4819.getWork_Request_Multi_Prod_Cde());                                                      //Natural: MOVE WORK-REQUEST.MULTI-PROD-CDE TO WORK-REQUEST-NEW.MULTI-PROD-CDE
            ldaCwfl4899.getWork_Request_New_D76().setValue(ldaCwfl4819.getWork_Request_C76());                                                                            //Natural: MOVE WORK-REQUEST.C76 TO WORK-REQUEST-NEW.D76
            ldaCwfl4899.getWork_Request_New_Complex_Txn_Cde().setValue(ldaCwfl4819.getWork_Request_Complex_Txn_Cde());                                                    //Natural: MOVE WORK-REQUEST.COMPLEX-TXN-CDE TO WORK-REQUEST-NEW.COMPLEX-TXN-CDE
            ldaCwfl4899.getWork_Request_New_D77().setValue(ldaCwfl4819.getWork_Request_C77());                                                                            //Natural: MOVE WORK-REQUEST.C77 TO WORK-REQUEST-NEW.D77
            ldaCwfl4899.getWork_Request_New_Part_Age().setValue(ldaCwfl4819.getWork_Request_Part_Age());                                                                  //Natural: MOVE WORK-REQUEST.PART-AGE TO WORK-REQUEST-NEW.PART-AGE
            ldaCwfl4899.getWork_Request_New_D78().setValue(ldaCwfl4819.getWork_Request_C78());                                                                            //Natural: MOVE WORK-REQUEST.C78 TO WORK-REQUEST-NEW.D78
            if (condition(DbsUtil.maskMatches(ldaCwfl4819.getWork_Request_Pin_Npin(),"N...........")))                                                                    //Natural: IF WORK-REQUEST.PIN-NPIN EQ MASK ( N........... )
            {
                ldaCwfl4899.getWork_Request_New_Pin_Npin().setValue(ldaCwfl4819.getWork_Request_Pin_Npin().getSubstring(6,7));                                            //Natural: MOVE SUBSTR ( WORK-REQUEST.PIN-NPIN,6,7 ) TO WORK-REQUEST-NEW.PIN-NPIN
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaCwfl4899.getWork_Request_New_Pin_Npin().setValue(ldaCwfl4819.getWork_Request_Pin_Npin().getSubstring(1,7));                                            //Natural: MOVE SUBSTR ( WORK-REQUEST.PIN-NPIN,1,7 ) TO WORK-REQUEST-NEW.PIN-NPIN
            }                                                                                                                                                             //Natural: END-IF
            ldaCwfl4899.getWork_Request_New_D79().setValue(ldaCwfl4819.getWork_Request_C79());                                                                            //Natural: MOVE WORK-REQUEST.C79 TO WORK-REQUEST-NEW.D79
            ldaCwfl4899.getWork_Request_New_Pin_Type_Ind().setValue(ldaCwfl4819.getWork_Request_Pin_Type_Ind());                                                          //Natural: MOVE WORK-REQUEST.PIN-TYPE-IND TO WORK-REQUEST-NEW.PIN-TYPE-IND
            ldaCwfl4899.getWork_Request_New_D80().setValue(ldaCwfl4819.getWork_Request_C80());                                                                            //Natural: MOVE WORK-REQUEST.C80 TO WORK-REQUEST-NEW.D80
            ldaCwfl4899.getWork_Request_New_Rqst_Log_Dte_Tme_D().setValue(ldaCwfl4819.getWork_Request_Rqst_Log_Dte_Tme_D());                                              //Natural: MOVE WORK-REQUEST.RQST-LOG-DTE-TME-D TO WORK-REQUEST-NEW.RQST-LOG-DTE-TME-D
            ldaCwfl4899.getWork_Request_New_D81().setValue(ldaCwfl4819.getWork_Request_C81());                                                                            //Natural: MOVE WORK-REQUEST.C81 TO WORK-REQUEST-NEW.D81
            ldaCwfl4899.getWork_Request_New_Admin_Unit_Cde().setValue(ldaCwfl4819.getWork_Request_Admin_Unit_Cde());                                                      //Natural: MOVE WORK-REQUEST.ADMIN-UNIT-CDE TO WORK-REQUEST-NEW.ADMIN-UNIT-CDE
            ldaCwfl4899.getWork_Request_New_D82().setValue(ldaCwfl4819.getWork_Request_C82());                                                                            //Natural: MOVE WORK-REQUEST.C82 TO WORK-REQUEST-NEW.D82
            ldaCwfl4899.getWork_Request_New_Last_Updte_Oprtr_Cde().setValue(ldaCwfl4819.getWork_Request_Last_Updte_Oprtr_Cde());                                          //Natural: MOVE WORK-REQUEST.LAST-UPDTE-OPRTR-CDE TO WORK-REQUEST-NEW.LAST-UPDTE-OPRTR-CDE
            ldaCwfl4899.getWork_Request_New_D83().setValue(ldaCwfl4819.getWork_Request_C83());                                                                            //Natural: MOVE WORK-REQUEST.C83 TO WORK-REQUEST-NEW.D83
            ldaCwfl4899.getWork_Request_New_Due_Dte_Prty().setValue(ldaCwfl4819.getWork_Request_Due_Dte_Prty());                                                          //Natural: MOVE WORK-REQUEST.DUE-DTE-PRTY TO WORK-REQUEST-NEW.DUE-DTE-PRTY
            getWorkFiles().write(2, false, ldaCwfl4899.getWork_Request_New());                                                                                            //Natural: WRITE WORK FILE 2 WORK-REQUEST-NEW
            pnd_Wrqst_Add_Cntr.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #WRQST-ADD-CNTR
                                                                                                                                                                          //Natural: PERFORM FILL-N-WRITE-CONTRACT
            sub_Fill_N_Write_Contract();
            if (condition(Global.isEscape())) {return;}
            getWorkFiles().write(21, false, ldaCwfl4828.getExtra_Table());                                                                                                //Natural: WRITE WORK FILE 21 EXTRA-TABLE
            pnd_Adtnl_Wrk_Add.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #ADTNL-WRK-ADD
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  END-IF
    }
    private void sub_Fill_Work_Request() throws Exception                                                                                                                 //Natural: FILL-WORK-REQUEST
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        ldaCwfl4819.getWork_Request().setValuesByName(ldaCwfl4817.getVw_cwf_Master());                                                                                    //Natural: MOVE BY NAME CWF-MASTER TO WORK-REQUEST
        ldaCwfl4828.getExtra_Table().setValuesByName(ldaCwfl4817.getVw_cwf_Master());                                                                                     //Natural: MOVE BY NAME CWF-MASTER TO EXTRA-TABLE
        ldaCwfl4828.getExtra_Table_Q1().setValue(pnd_Atsign);                                                                                                             //Natural: MOVE #ATSIGN TO Q1
        if (condition(ldaCwfl4817.getCwf_Master_Admin_Status_Cde_1().equals("C")))                                                                                        //Natural: IF ADMIN-STATUS-CDE-1 = 'C'
        {
            ldaCwfl4819.getWork_Request_Admin_Status_Cde().setValue(ldaCwfl4817.getCwf_Master_Status_Cde());                                                              //Natural: MOVE CWF-MASTER.STATUS-CDE TO WORK-REQUEST.ADMIN-STATUS-CDE
            ldaCwfl4828.getExtra_Table_Admin_Status_Cde().setValue(ldaCwfl4817.getCwf_Master_Status_Cde());                                                               //Natural: MOVE CWF-MASTER.STATUS-CDE TO EXTRA-TABLE.ADMIN-STATUS-CDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(DbsUtil.maskMatches(ldaCwfl4817.getCwf_Master_Crprte_Clock_End_Dte_Tme(),"YYYYMMDD00-2400-6000-60N")))                                              //Natural: IF CWF-MASTER.CRPRTE-CLOCK-END-DTE-TME EQ MASK ( YYYYMMDD00-2400-6000-60N )
        {
            ldaCwfl4819.getWork_Request_Part_Close_Status_A().setValue(ldaCwfl4817.getCwf_Master_Status_Cde());                                                           //Natural: MOVE CWF-MASTER.STATUS-CDE TO WORK-REQUEST.PART-CLOSE-STATUS-A
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(DbsUtil.maskMatches(ldaCwfl4817.getCwf_Master_Crprte_Clock_End_Dte_Tme(),"YYYYMMDD00-2400-6000-60N")))                                              //Natural: IF CWF-MASTER.CRPRTE-CLOCK-END-DTE-TME = MASK ( YYYYMMDD00-2400-6000-60N )
        {
            if (condition(DbsUtil.maskMatches(ldaCwfl4817.getCwf_Master_Unit_Clock_Start_Dte_Tme(),"YYYYMMDD00-2400-6000-60N")))                                          //Natural: IF CWF-MASTER.UNIT-CLOCK-START-DTE-TME = MASK ( YYYYMMDD00-2400-6000-60N )
            {
                pnd_Unit_Time_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaCwfl4817.getCwf_Master_Unit_Clock_Start_Dte_Tme());                               //Natural: MOVE EDITED CWF-MASTER.UNIT-CLOCK-START-DTE-TME TO #UNIT-TIME-T ( EM = YYYYMMDDHHIISST )
                pnd_Part_Time_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaCwfl4817.getCwf_Master_Crprte_Clock_End_Dte_Tme());                               //Natural: MOVE EDITED CWF-MASTER.CRPRTE-CLOCK-END-DTE-TME TO #PART-TIME-T ( EM = YYYYMMDDHHIISST )
                if (condition(pnd_Unit_Time_T.greaterOrEqual(pnd_Part_Time_T)))                                                                                           //Natural: IF #UNIT-TIME-T GE #PART-TIME-T
                {
                    ldaCwfl4819.getWork_Request_Admin_Work_Ind().setValue("Y");                                                                                           //Natural: MOVE 'Y' TO WORK-REQUEST.ADMIN-WORK-IND
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM FORMAT-WR-DATES
        sub_Format_Wr_Dates();
        if (condition(Global.isEscape())) {return;}
        pnd_Conversion_Due_Dte_Chg_Prty_Cde.setValue(ldaCwfl4817.getCwf_Master_Due_Dte_Chg_Prty_Cde());                                                                   //Natural: ASSIGN #CONVERSION-DUE-DTE-CHG-PRTY-CDE := CWF-MASTER.DUE-DTE-CHG-PRTY-CDE
                                                                                                                                                                          //Natural: PERFORM DERIVE-INDICATORS
        sub_Derive_Indicators();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-BUSINESS-DATA
        sub_Get_Business_Data();
        if (condition(Global.isEscape())) {return;}
        //*  FILL-WORK-REQUEST
    }
    private void sub_Format_Wr_Dates() throws Exception                                                                                                                   //Natural: FORMAT-WR-DATES
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        pnd_Number_W_Decimals.reset();                                                                                                                                    //Natural: RESET #NUMBER-W-DECIMALS
        pnd_Time_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaCwfl4817.getCwf_Master_Rqst_Log_Dte_Tme());                                                    //Natural: MOVE EDITED CWF-MASTER.RQST-LOG-DTE-TME TO #TIME-T ( EM = YYYYMMDDHHIISST )
        ldaCwfl4819.getWork_Request_Rqst_Log_Dte_Tme_A().setValueEdited(pnd_Time_T,new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SS:0AP'''"));                             //Natural: MOVE EDITED #TIME-T ( EM = '"'LLL�DD�YYYY�HH':'II':'SS:T'00'AP'"' ) TO WORK-REQUEST.RQST-LOG-DTE-TME-A
        ldaCwfl4821.getContract_Rqst_Log_Dte_Tme().setValueEdited(pnd_Time_T,new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SS:0AP'''"));                                   //Natural: MOVE EDITED #TIME-T ( EM = '"'LLL�DD�YYYY�HH':'II':'SS:T'00'AP'"' ) TO CONTRACT.RQST-LOG-DTE-TME
        ldaCwfl4828.getExtra_Table_Rqst_Log_Dte_Tme_A().setValueEdited(pnd_Time_T,new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SS:0AP'''"));                              //Natural: MOVE EDITED #TIME-T ( EM = '"'LLL�DD�YYYY�HH':'II':'SS:T'00'AP'"' ) TO EXTRA-TABLE.RQST-LOG-DTE-TME-A
        ldaCwfl4819.getWork_Request_Rqst_Log_Dte_Tme_D().setValueEdited(pnd_Time_T,new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''"));                               //Natural: MOVE EDITED #TIME-T ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO WORK-REQUEST.RQST-LOG-DTE-TME-D
        //*  -------------- DEFINE THE START-DATE -----------------------
        //*  CONTAINS RQST-LOG-DTE
        if (condition(ldaCwfl4817.getCwf_Master_Tiaa_Rcvd_Dte_Tme().less(pnd_Time_T)))                                                                                    //Natural: IF CWF-MASTER.TIAA-RCVD-DTE-TME LT #TIME-T
        {
            ldaCwfl4819.getWork_Request_Start_Dte_Tme_A().setValueEdited(ldaCwfl4817.getCwf_Master_Tiaa_Rcvd_Dte_Tme(),new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''")); //Natural: MOVE EDITED CWF-MASTER.TIAA-RCVD-DTE-TME ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO WORK-REQUEST.START-DTE-TME-A
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  CONTAINS RQST-LOG-DTE
            ldaCwfl4819.getWork_Request_Start_Dte_Tme_A().setValueEdited(pnd_Time_T,new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''"));                              //Natural: MOVE EDITED #TIME-T ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO WORK-REQUEST.START-DTE-TME-A
        }                                                                                                                                                                 //Natural: END-IF
        pdaNsta1245.getNsta1245_Pnd_Eff_Dte().reset();                                                                                                                    //Natural: RESET NSTA1245.#EFF-DTE
        if (condition(ldaCwfl4817.getCwf_Master_Tiaa_Rcvd_Dte_Tme().notEquals(getZero())))                                                                                //Natural: IF CWF-MASTER.TIAA-RCVD-DTE-TME NE 0
        {
            ldaCwfl4819.getWork_Request_Tiaa_Rcvd_Dte_Tme_A().setValueEdited(ldaCwfl4817.getCwf_Master_Tiaa_Rcvd_Dte_Tme(),new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''")); //Natural: MOVE EDITED CWF-MASTER.TIAA-RCVD-DTE-TME ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO WORK-REQUEST.TIAA-RCVD-DTE-TME-A
            pnd_Date_Split.setValueEdited(ldaCwfl4817.getCwf_Master_Tiaa_Rcvd_Dte_Tme(),new ReportEditMask("YYYYMMDDHHIISST"));                                           //Natural: MOVE EDITED CWF-MASTER.TIAA-RCVD-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #DATE-SPLIT
            //*  AGE
            pdaNsta1245.getNsta1245_Pnd_Eff_Dte().setValue(pnd_Date_Split_Pnd_Date_Split_Dte);                                                                            //Natural: MOVE #DATE-SPLIT-DTE TO NSTA1245.#EFF-DTE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Fix_Dte.setValue(ldaCwfl4817.getCwf_Master_Tiaa_Rcvd_Dte());                                                                                              //Natural: ASSIGN #FIX-DTE := CWF-MASTER.TIAA-RCVD-DTE
            if (condition(DbsUtil.maskMatches(pnd_Fix_Dte,"MM'/'DD'/'YY")))                                                                                               //Natural: IF #FIX-DTE EQ MASK ( MM'/'DD'/'YY )
            {
                pdaNsta1245.getNsta1245_Pnd_Eff_Dte().setValueEdited(ldaCwfl4817.getCwf_Master_Tiaa_Rcvd_Dte(),new ReportEditMask("YYYYMMDD"));                           //Natural: MOVE EDITED CWF-MASTER.TIAA-RCVD-DTE ( EM = YYYYMMDD ) TO NSTA1245.#EFF-DTE
                pnd_Format_Dte_Tme_Pnd_Format_Dte.setValueEdited(ldaCwfl4817.getCwf_Master_Tiaa_Rcvd_Dte(),new ReportEditMask("'''LLL�DD�YYYY�"));                        //Natural: MOVE EDITED CWF-MASTER.TIAA-RCVD-DTE ( EM = '"'LLL�DD�YYYY� ) TO #FORMAT-DTE
                pnd_Format_Dte_Tme_Pnd_Format_Tme.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "08:45:00AM'"));                                               //Natural: COMPRESS '08:45:00AM"' INTO #FORMAT-TME LEAVING NO SPACE
                ldaCwfl4819.getWork_Request_Tiaa_Rcvd_Dte_Tme_A().setValue(pnd_Format_Dte_Tme);                                                                           //Natural: MOVE #FORMAT-DTE-TME TO WORK-REQUEST.TIAA-RCVD-DTE-TME-A
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaCwfl4819.getWork_Request_Tiaa_Rcvd_Dte_Tme_A().reset();                                                                                                //Natural: RESET WORK-REQUEST.TIAA-RCVD-DTE-TME-A
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*   FORMAT THE LAST CHNGE DATE TIME ON THE WORK REQUEST
        pnd_Time_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaCwfl4817.getCwf_Master_Last_Chnge_Dte_Tme_A());                                                //Natural: MOVE EDITED CWF-MASTER.LAST-CHNGE-DTE-TME-A TO #TIME-T ( EM = YYYYMMDDHHIISST )
        ldaCwfl4819.getWork_Request_Last_Chnge_Dte_Tme_A().setValueEdited(pnd_Time_T,new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''"));                             //Natural: MOVE EDITED #TIME-T ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO WORK-REQUEST.LAST-CHNGE-DTE-TME-A
        pnd_Fix_Dte.setValue(ldaCwfl4817.getCwf_Master_Tiaa_Rcvd_Dte());                                                                                                  //Natural: ASSIGN #FIX-DTE := CWF-MASTER.TIAA-RCVD-DTE
        if (condition(DbsUtil.maskMatches(pnd_Fix_Dte,"MM'/'DD'/'YY")))                                                                                                   //Natural: IF #FIX-DTE EQ MASK ( MM'/'DD'/'YY )
        {
            pnd_Fix_Dte_10.setValueEdited(ldaCwfl4817.getCwf_Master_Tiaa_Rcvd_Dte(),new ReportEditMask("MM/DD/YYYY"));                                                    //Natural: MOVE EDITED CWF-MASTER.TIAA-RCVD-DTE ( EM = MM/DD/YYYY ) TO #FIX-DTE-10
            ldaCwfl4819.getWork_Request_Tiaa_Rcvd_Dte_A().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "'", pnd_Fix_Dte_10, "'"));                            //Natural: COMPRESS '"' #FIX-DTE-10 '"' INTO WORK-REQUEST.TIAA-RCVD-DTE-A LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCwfl4819.getWork_Request_Tiaa_Rcvd_Dte_A().reset();                                                                                                        //Natural: RESET WORK-REQUEST.TIAA-RCVD-DTE-A
        }                                                                                                                                                                 //Natural: END-IF
        //* ******
        ldaCwfl4819.getWork_Request_Pin_Npin().moveAll(ldaCwfl4817.getCwf_Master_Pin_Nbr());                                                                              //Natural: MOVE ALL CWF-MASTER.PIN-NBR TO WORK-REQUEST.PIN-NPIN
        ldaCwfl4819.getWork_Request_Pin_Type_Ind().setValue("P");                                                                                                         //Natural: MOVE 'P' TO WORK-REQUEST.PIN-TYPE-IND
        //* ******
        //*  -------------- DEFINE THE TRADE-DATE -----------------------
        if (condition(ldaCwfl4817.getCwf_Master_Extrnl_Pend_Rcv_Dte().notEquals(getZero())))                                                                              //Natural: IF CWF-MASTER.EXTRNL-PEND-RCV-DTE NE 0
        {
            ldaCwfl4817.getCwf_Master_Trade_Dte_Tme().setValue(ldaCwfl4817.getCwf_Master_Extrnl_Pend_Rcv_Dte());                                                          //Natural: MOVE CWF-MASTER.EXTRNL-PEND-RCV-DTE TO CWF-MASTER.TRADE-DTE-TME
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  #TIME-T = RLDT
            if (condition(pnd_Time_T.less(ldaCwfl4817.getCwf_Master_Tiaa_Rcvd_Dte())))                                                                                    //Natural: IF #TIME-T LT CWF-MASTER.TIAA-RCVD-DTE
            {
                ldaCwfl4817.getCwf_Master_Trade_Dte_Tme().setValue(pnd_Time_T);                                                                                           //Natural: MOVE #TIME-T TO CWF-MASTER.TRADE-DTE-TME
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaCwfl4817.getCwf_Master_Trade_Dte_Tme().setValue(ldaCwfl4817.getCwf_Master_Tiaa_Rcvd_Dte_Tme());                                                        //Natural: MOVE CWF-MASTER.TIAA-RCVD-DTE-TME TO CWF-MASTER.TRADE-DTE-TME
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  -------------------------------------------------------------
        if (condition(ldaCwfl4817.getCwf_Master_Trade_Dte_Tme().notEquals(getZero())))                                                                                    //Natural: IF CWF-MASTER.TRADE-DTE-TME NE 0
        {
            ldaCwfl4819.getWork_Request_Trade_Dte_Tme_A().setValueEdited(ldaCwfl4817.getCwf_Master_Trade_Dte_Tme(),new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''")); //Natural: MOVE EDITED CWF-MASTER.TRADE-DTE-TME ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO WORK-REQUEST.TRADE-DTE-TME-A
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaCwfl4819.getWork_Request_Tiaa_Rcvd_Dte_Tme_A().notEquals(" ")))                                                                              //Natural: IF WORK-REQUEST.TIAA-RCVD-DTE-TME-A NE ' '
            {
                ldaCwfl4819.getWork_Request_Trade_Dte_Tme_A().setValueEdited(ldaCwfl4817.getCwf_Master_Tiaa_Rcvd_Dte_Tme(),new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''")); //Natural: MOVE EDITED CWF-MASTER.TIAA-RCVD-DTE-TME ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO WORK-REQUEST.TRADE-DTE-TME-A
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaCwfl4819.getWork_Request_Trade_Dte_Tme_A().reset();                                                                                                    //Natural: RESET WORK-REQUEST.TRADE-DTE-TME-A
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Effctve_Dte.setValue(ldaCwfl4817.getCwf_Master_Effctve_Dte());                                                                                                //Natural: ASSIGN #EFFCTVE-DTE := CWF-MASTER.EFFCTVE-DTE
        pnd_Trans_Dte.setValue(ldaCwfl4817.getCwf_Master_Trans_Dte());                                                                                                    //Natural: ASSIGN #TRANS-DTE := CWF-MASTER.TRANS-DTE
        pnd_Trnsctn_Dte.setValue(ldaCwfl4817.getCwf_Master_Trnsctn_Dte());                                                                                                //Natural: ASSIGN #TRNSCTN-DTE := CWF-MASTER.TRNSCTN-DTE
        short decideConditionsMet3001 = 0;                                                                                                                                //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #EFFCTVE-DTE EQ MASK ( MM'/'DD'/'YY )
        if (condition(DbsUtil.maskMatches(pnd_Effctve_Dte,"MM'/'DD'/'YY")))
        {
            decideConditionsMet3001++;
            pnd_Fix_Dte_10.setValueEdited(ldaCwfl4817.getCwf_Master_Effctve_Dte(),new ReportEditMask("MM/DD/YYYY"));                                                      //Natural: MOVE EDITED CWF-MASTER.EFFCTVE-DTE ( EM = MM/DD/YYYY ) TO #FIX-DTE-10
            ldaCwfl4819.getWork_Request_Effctve_Dte_A().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "'", pnd_Fix_Dte_10, "'"));                              //Natural: COMPRESS '"' #FIX-DTE-10 '"' INTO WORK-REQUEST.EFFCTVE-DTE-A LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: WHEN #TRANS-DTE EQ MASK ( MM'/'DD'/'YY )
        if (condition(DbsUtil.maskMatches(pnd_Trans_Dte,"MM'/'DD'/'YY")))
        {
            decideConditionsMet3001++;
            pnd_Fix_Dte_10.setValueEdited(ldaCwfl4817.getCwf_Master_Trans_Dte(),new ReportEditMask("MM/DD/YYYY"));                                                        //Natural: MOVE EDITED CWF-MASTER.TRANS-DTE ( EM = MM/DD/YYYY ) TO #FIX-DTE-10
            ldaCwfl4819.getWork_Request_Trans_Dte_A().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "'", pnd_Fix_Dte_10, "'"));                                //Natural: COMPRESS '"' #FIX-DTE-10 '"' INTO WORK-REQUEST.TRANS-DTE-A LEAVING NO
        }                                                                                                                                                                 //Natural: WHEN #TRNSCTN-DTE EQ MASK ( MM'/'DD'/'YY )
        if (condition(DbsUtil.maskMatches(pnd_Trnsctn_Dte,"MM'/'DD'/'YY")))
        {
            decideConditionsMet3001++;
            pnd_Fix_Dte_10.setValueEdited(ldaCwfl4817.getCwf_Master_Trnsctn_Dte(),new ReportEditMask("MM/DD/YYYY"));                                                      //Natural: MOVE EDITED CWF-MASTER.TRNSCTN-DTE ( EM = MM/DD/YYYY ) TO #FIX-DTE-10
            ldaCwfl4819.getWork_Request_Trnsctn_Dte_A().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "'", pnd_Fix_Dte_10, "'"));                              //Natural: COMPRESS '"' #FIX-DTE-10 '"' INTO WORK-REQUEST.TRNSCTN-DTE-A LEAVING NO
        }                                                                                                                                                                 //Natural: WHEN #SEC-UPDTE-DTE EQ MASK ( MM'/'DD'/'YY )
        if (condition(DbsUtil.maskMatches(pnd_Sec_Updte_Dte,"MM'/'DD'/'YY")))
        {
            decideConditionsMet3001++;
            pnd_Fix_Dte_10.setValueEdited(Global.getDATX(),new ReportEditMask("MM/DD/YYYY"));                                                                             //Natural: MOVE EDITED *DATX ( EM = MM/DD/YYYY ) TO #FIX-DTE-10
            ldaCwfl4819.getWork_Request_Sec_Updte_Dte_A().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "'", pnd_Fix_Dte_10, "'"));                            //Natural: COMPRESS '"' #FIX-DTE-10 '"' INTO WORK-REQUEST.SEC-UPDTE-DTE-A LEAVING NO
        }                                                                                                                                                                 //Natural: WHEN CWF-MASTER.FINAL-CLOSE-OUT-DTE-TME NE 0
        if (condition(ldaCwfl4817.getCwf_Master_Final_Close_Out_Dte_Tme().notEquals(getZero())))
        {
            decideConditionsMet3001++;
            ldaCwfl4819.getWork_Request_Final_Close_Out_Dte_Tme_A().setValueEdited(ldaCwfl4817.getCwf_Master_Final_Close_Out_Dte_Tme(),new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''")); //Natural: MOVE EDITED CWF-MASTER.FINAL-CLOSE-OUT-DTE-TME ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO WORK-REQUEST.FINAL-CLOSE-OUT-DTE-TME-A
        }                                                                                                                                                                 //Natural: WHEN CWF-MASTER.CRPRTE-DUE-DTE-TME NE 0
        if (condition(ldaCwfl4817.getCwf_Master_Crprte_Due_Dte_Tme().notEquals(getZero())))
        {
            decideConditionsMet3001++;
            ldaCwfl4819.getWork_Request_Crprte_Due_Dte_Tme_A().setValueEdited(ldaCwfl4817.getCwf_Master_Crprte_Due_Dte_Tme(),new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''")); //Natural: MOVE EDITED CWF-MASTER.CRPRTE-DUE-DTE-TME ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO WORK-REQUEST.CRPRTE-DUE-DTE-TME-A
            pnd_Chck_Crprte_Due_Dte_Tme.setValueEdited(ldaCwfl4817.getCwf_Master_Crprte_Due_Dte_Tme(),new ReportEditMask("YYYYMMDDHHIISST"));                             //Natural: MOVE EDITED CWF-MASTER.CRPRTE-DUE-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #CHCK-CRPRTE-DUE-DTE-TME
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet3001 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(ldaCwfl4817.getCwf_Master_Orgnl_Log_Dte_Tme().greater(" ")))                                                                                        //Natural: IF CWF-MASTER.ORGNL-LOG-DTE-TME GT ' '
        {
            if (condition(DbsUtil.maskMatches(ldaCwfl4817.getCwf_Master_Orgnl_Log_Dte_Tme(),"YYYYMMDD00-2400-6000-60N")))                                                 //Natural: IF CWF-MASTER.ORGNL-LOG-DTE-TME = MASK ( YYYYMMDD00-2400-6000-60N )
            {
                pnd_Time_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaCwfl4817.getCwf_Master_Orgnl_Log_Dte_Tme());                                           //Natural: MOVE EDITED CWF-MASTER.ORGNL-LOG-DTE-TME TO #TIME-T ( EM = YYYYMMDDHHIISST )
                //*  CHNGE ON 01/19/00
                ldaCwfl4819.getWork_Request_Prnt_Rqst_Log_Dte_Tme_A().setValueEdited(pnd_Time_T,new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SS:0AP'''"));                //Natural: MOVE EDITED #TIME-T ( EM = '"'LLL�DD�YYYY�HH':'II':'SS:T'00'AP'"' ) TO WORK-REQUEST.PRNT-RQST-LOG-DTE-TME-A
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaCwfl4819.getWork_Request_Prnt_Rqst_Log_Dte_Tme_A().reset();                                                                                            //Natural: RESET WORK-REQUEST.PRNT-RQST-LOG-DTE-TME-A
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaCwfl4817.getCwf_Master_Cmplnt_Ind().greater(" ")))                                                                                           //Natural: IF CWF-MASTER.CMPLNT-IND GT ' '
            {
                pnd_Actv_Unque_Key_Rqst_Log_Dte_Tme.setValue(ldaCwfl4817.getCwf_Master_Orgnl_Log_Dte_Tme());                                                              //Natural: MOVE CWF-MASTER.ORGNL-LOG-DTE-TME TO #ACTV-UNQUE-KEY.RQST-LOG-DTE-TME
                                                                                                                                                                          //Natural: PERFORM GET-PARENT-REC
                sub_Get_Parent_Rec();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaCwfl4819.getWork_Request_Prnt_Work_Prcss_Id().setValue(ldaCwfl4817.getCwf_Master_Rqst_Work_Prcss_Id());                                                //Natural: MOVE CWF-MASTER.RQST-WORK-PRCSS-ID TO WORK-REQUEST.PRNT-WORK-PRCSS-ID
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (DbsUtil.maskMatches(ldaCwfl4817.getCwf_Master_Crprte_Clock_End_Dte_Tme(),"YYYYMMDD00-2400-6000-60N"))))                                          //Natural: IF CWF-MASTER.CRPRTE-CLOCK-END-DTE-TME NE MASK ( YYYYMMDD00-2400-6000-60N )
        {
            if (condition(ldaCwfl4817.getCwf_Master_Crprte_Clock_End_Dte_Tme().equals(" ") || ldaCwfl4817.getCwf_Master_Crprte_Clock_End_Dte_Tme().equals("999999999999999"))) //Natural: IF CWF-MASTER.CRPRTE-CLOCK-END-DTE-TME EQ ' ' OR CWF-MASTER.CRPRTE-CLOCK-END-DTE-TME EQ '999999999999999'
            {
                pnd_Static_Sw.setValue(true);                                                                                                                             //Natural: ASSIGN #STATIC-SW := TRUE
                if (condition(ldaCwfl4817.getCwf_Master_Final_Close_Out_Dte_Tme().greater(getZero())))                                                                    //Natural: IF CWF-MASTER.FINAL-CLOSE-OUT-DTE-TME GT 0
                {
                    pnd_Hold_Close_Dte.setValueEdited(ldaCwfl4817.getCwf_Master_Final_Close_Out_Dte_Tme(),new ReportEditMask("YYYYMMDDHHIISST"));                         //Natural: MOVE EDITED CWF-MASTER.FINAL-CLOSE-OUT-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #HOLD-CLOSE-DTE
                    if (condition(DbsUtil.maskMatches(pnd_Hold_Close_Dte_Pnd_Hold_Close_C,"1:2")))                                                                        //Natural: IF #HOLD-CLOSE-C = MASK ( 1:2 )
                    {
                        ldaCwfl4819.getWork_Request_Crprte_Clock_End_Dte_Tme_A().setValueEdited(ldaCwfl4817.getCwf_Master_Final_Close_Out_Dte_Tme(),new                   //Natural: MOVE EDITED CWF-MASTER.FINAL-CLOSE-OUT-DTE-TME ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO WORK-REQUEST.CRPRTE-CLOCK-END-DTE-TME-A
                            ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''"));
                        pnd_Static_Sw.setValue(false);                                                                                                                    //Natural: ASSIGN #STATIC-SW := FALSE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Static_Sw.setValue(false);                                                                                                                                //Natural: ASSIGN #STATIC-SW := FALSE
            pnd_Hold_Close_Dte.setValue(ldaCwfl4817.getCwf_Master_Crprte_Clock_End_Dte_Tme());                                                                            //Natural: MOVE CWF-MASTER.CRPRTE-CLOCK-END-DTE-TME TO #HOLD-CLOSE-DTE
            pnd_Time_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaCwfl4817.getCwf_Master_Crprte_Clock_End_Dte_Tme());                                        //Natural: MOVE EDITED CWF-MASTER.CRPRTE-CLOCK-END-DTE-TME TO #TIME-T ( EM = YYYYMMDDHHIISST )
            ldaCwfl4819.getWork_Request_Crprte_Clock_End_Dte_Tme_A().setValueEdited(pnd_Time_T,new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''"));                   //Natural: MOVE EDITED #TIME-T ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO WORK-REQUEST.CRPRTE-CLOCK-END-DTE-TME-A
            //*  ------------ DEFINE LATE INDICATOR --------------------------------
            if (condition(pnd_Chck_Crprte_Due_Dte_Tme.notEquals(" ")))                                                                                                    //Natural: IF #CHCK-CRPRTE-DUE-DTE-TME NE ' '
            {
                if (condition(ldaCwfl4817.getCwf_Master_Crprte_Clock_End_Dte_Tme().greater(pnd_Chck_Crprte_Due_Dte_Tme)))                                                 //Natural: IF CWF-MASTER.CRPRTE-CLOCK-END-DTE-TME GT #CHCK-CRPRTE-DUE-DTE-TME
                {
                    ldaCwfl4819.getWork_Request_Crprte_On_Tme_Ind().setValue("L");                                                                                        //Natural: MOVE 'L' TO WORK-REQUEST.CRPRTE-ON-TME-IND
                    pnd_Late_Add.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #LATE-ADD
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaCwfl4819.getWork_Request_Crprte_On_Tme_Ind().reset();                                                                                              //Natural: RESET WORK-REQUEST.CRPRTE-ON-TME-IND
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  -- IF PART.CLOSE IS EMPTY - SET STATIC DATE=JAN 01 1930 12:00:00PM --
        if (condition(pnd_Static_Sw.getBoolean()))                                                                                                                        //Natural: IF #STATIC-SW
        {
            pnd_Static_Dte_Tme_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),pnd_Static_Dte_Tme_A_15);                                                           //Natural: MOVE EDITED #STATIC-DTE-TME-A-15 TO #STATIC-DTE-TME-T ( EM = YYYYMMDDHHIISST )
            ldaCwfl4819.getWork_Request_Crprte_Clock_End_Dte_Tme_A().setValueEdited(pnd_Static_Dte_Tme_T,new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''"));         //Natural: MOVE EDITED #STATIC-DTE-TME-T ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO WORK-REQUEST.CRPRTE-CLOCK-END-DTE-TME-A
        }                                                                                                                                                                 //Natural: END-IF
        //*  ------------- NEW INTEGRATED FIELD ---------------------
        if (condition(DbsUtil.maskMatches(ldaCwfl4817.getCwf_Master_Instn_Rqst_Log_Dte_Tme(),"YYYYMMDD00-2400-6000-60N")))                                                //Natural: IF CWF-MASTER.INSTN-RQST-LOG-DTE-TME = MASK ( YYYYMMDD00-2400-6000-60N )
        {
            pnd_Time_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaCwfl4817.getCwf_Master_Instn_Rqst_Log_Dte_Tme());                                          //Natural: MOVE EDITED CWF-MASTER.INSTN-RQST-LOG-DTE-TME TO #TIME-T ( EM = YYYYMMDDHHIISST )
            ldaCwfl4819.getWork_Request_Instn_Rqst_Log_Dte_Tme_A().setValueEdited(pnd_Time_T,new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SS:0AP'''"));                   //Natural: MOVE EDITED #TIME-T ( EM = '"'LLL�DD�YYYY�HH':'II':'SS:T'00'AP'"' ) TO WORK-REQUEST.INSTN-RQST-LOG-DTE-TME-A
        }                                                                                                                                                                 //Natural: END-IF
        pnd_End_Date_Switch.setValue(false);                                                                                                                              //Natural: ASSIGN #END-DATE-SWITCH := FALSE
        short decideConditionsMet3076 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CWF-MASTER.INTRNL-PND-START-DTE-TME = MASK ( YYYYMMDD00-2400-6000-60N )
        if (condition(DbsUtil.maskMatches(ldaCwfl4817.getCwf_Master_Intrnl_Pnd_Start_Dte_Tme(),"YYYYMMDD00-2400-6000-60N")))
        {
            decideConditionsMet3076++;
            pnd_End_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),ldaCwfl4817.getCwf_Master_Intrnl_Pnd_Start_Dte());                                               //Natural: MOVE EDITED CWF-MASTER.INTRNL-PND-START-DTE TO #END-DATE-D ( EM = YYYYMMDD )
            pnd_Hold_Tme_A.setValue(ldaCwfl4817.getCwf_Master_Intrnl_Pnd_Start_Tme());                                                                                    //Natural: MOVE CWF-MASTER.INTRNL-PND-START-TME TO #HOLD-TME-A
            pnd_End_Date_Switch.setValue(true);                                                                                                                           //Natural: ASSIGN #END-DATE-SWITCH := TRUE
        }                                                                                                                                                                 //Natural: WHEN CWF-MASTER.CRPRTE-CLOCK-END-DTE-TME = MASK ( YYYYMMDD00-2400-6000-60N )
        else if (condition(DbsUtil.maskMatches(ldaCwfl4817.getCwf_Master_Crprte_Clock_End_Dte_Tme(),"YYYYMMDD00-2400-6000-60N")))
        {
            decideConditionsMet3076++;
            if (condition(ldaCwfl4817.getCwf_Master_Crprte_Clock_End_Dte_Tme().notEquals("999999999999999")))                                                             //Natural: IF CWF-MASTER.CRPRTE-CLOCK-END-DTE-TME NE '999999999999999'
            {
                pnd_End_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),ldaCwfl4817.getCwf_Master_Crprte_Clock_End_Dte());                                           //Natural: MOVE EDITED CWF-MASTER.CRPRTE-CLOCK-END-DTE TO #END-DATE-D ( EM = YYYYMMDD )
                pnd_Hold_Tme_A.setValue(ldaCwfl4817.getCwf_Master_Crprte_Clock_End_Tme());                                                                                //Natural: MOVE CWF-MASTER.CRPRTE-CLOCK-END-TME TO #HOLD-TME-A
                pnd_End_Date_Switch.setValue(true);                                                                                                                       //Natural: ASSIGN #END-DATE-SWITCH := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #END-DATE-SWITCH
        else if (condition(pnd_End_Date_Switch.getBoolean()))
        {
            decideConditionsMet3076++;
            DbsUtil.callnat(Cwfn3999.class , getCurrentProcessState(), pnd_End_Date_D);                                                                                   //Natural: CALLNAT 'CWFN3999' #END-DATE-D
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  ------------- DEFINE ELAPSED BUSINESS DAYS ------------------
        if (condition(pnd_End_Date_Switch.getBoolean()))                                                                                                                  //Natural: IF #END-DATE-SWITCH
        {
            pnd_Start_Date_T.setValue(ldaCwfl4817.getCwf_Master_Trade_Dte_Tme());                                                                                         //Natural: MOVE CWF-MASTER.TRADE-DTE-TME TO #START-DATE-T
            pnd_Elpsd_Time_Compose_Pnd_Elpsd_Date_Portion.setValueEdited(pnd_End_Date_D,new ReportEditMask("YYYYMMDD"));                                                  //Natural: MOVE EDITED #END-DATE-D ( EM = YYYYMMDD ) TO #ELPSD-DATE-PORTION
            pnd_Elpsd_Time_Compose_Pnd_Elpsd_Time_Portion.setValue(pnd_Hold_Tme_A);                                                                                       //Natural: MOVE #HOLD-TME-A TO #ELPSD-TIME-PORTION
            pnd_End_Date_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),pnd_Elpsd_Time_Compose);                                                                  //Natural: MOVE EDITED #ELPSD-TIME-COMPOSE TO #END-DATE-T ( EM = YYYYMMDDHHIISST )
            DbsUtil.callnat(Icwn9371.class , getCurrentProcessState(), pnd_Start_Date_T, pnd_End_Date_T, pnd_Elpsd_Days);                                                 //Natural: CALLNAT 'ICWN9371' #START-DATE-T #END-DATE-T #ELPSD-DAYS
            if (condition(Global.isEscape())) return;
            ldaCwfl4819.getWork_Request_Acknldg_Tat_Tme().setValueEdited(pnd_Elpsd_Days,new ReportEditMask("9999999.99"));                                                //Natural: MOVE EDITED #ELPSD-DAYS ( EM = 9999999.99 ) TO WORK-REQUEST.ACKNLDG-TAT-TME
            ldaCwfl4819.getWork_Request_Acknldg_Dte_Tme_A().setValueEdited(pnd_End_Date_T,new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''"));                        //Natural: MOVE EDITED #END-DATE-T ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO WORK-REQUEST.ACKNLDG-DTE-TME-A
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(DbsUtil.maskMatches(ldaCwfl4817.getCwf_Master_Crprte_Clock_End_Dte_Tme(),"YYYYMMDD00-2400-6000-60N")))                                              //Natural: IF CWF-MASTER.CRPRTE-CLOCK-END-DTE-TME = MASK ( YYYYMMDD00-2400-6000-60N )
        {
            if (condition(ldaCwfl4817.getCwf_Master_Effctve_Dte().greater(getZero())))                                                                                    //Natural: IF CWF-MASTER.EFFCTVE-DTE GT 0
            {
                if (condition(ldaCwfl4817.getCwf_Master_Trans_Dte().greater(getZero())))                                                                                  //Natural: IF CWF-MASTER.TRANS-DTE GT 0
                {
                    pnd_Base_Dte_Tme_D.setValue(ldaCwfl4817.getCwf_Master_Effctve_Dte());                                                                                 //Natural: MOVE CWF-MASTER.EFFCTVE-DTE TO #BASE-DTE-TME-D
                    pnd_End_Dte_Tme_D.setValue(ldaCwfl4817.getCwf_Master_Trans_Dte());                                                                                    //Natural: MOVE CWF-MASTER.TRANS-DTE TO #END-DTE-TME-D
                    DbsUtil.callnat(Cwfn3905.class , getCurrentProcessState(), pnd_Base_Dte_Tme_D, pnd_End_Dte_Tme_D, pnd_Bus_Days);                                      //Natural: CALLNAT 'CWFN3905' #BASE-DTE-TME-D #END-DTE-TME-D #BUS-DAYS
                    if (condition(Global.isEscape())) return;
                    //*  NUMERIC 7 POSITION
                    pnd_Number_W_Decimals.setValue(pnd_Bus_Days);                                                                                                         //Natural: MOVE #BUS-DAYS TO #NUMBER-W-DECIMALS
                    ldaCwfl4819.getWork_Request_Sec_Turnaround_Tme_A().setValueEdited(pnd_Number_W_Decimals,new ReportEditMask("9999999.99"));                            //Natural: MOVE EDITED #NUMBER-W-DECIMALS ( EM = 9999999.99 ) TO WORK-REQUEST.SEC-TURNAROUND-TME-A
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        ldaCwfl4819.getWork_Request_C1().setValue(pnd_Atsign);                                                                                                            //Natural: MOVE #ATSIGN TO C1 C2 C3 C4 C5 C6 C7 C8 C9 C10 C11 C12 C13 C14 C15 C16 C17 C18 C19 C20 C21 C22 C23 C24 C25 C26 C27 C28 C29 C30 C31 C32 C33 C34 C35 C36 C37 C38 C39 C40 C41 C42 C43 C44 C45 C46 C47 C48 C49 C50 C51 C52 C53 C54 C55 C56 C57 C58 C59 C60 C61 C62 C63 C64 C65 C66 C67 C68 C69 C70 C71 C72 C73 C74 C75 C76 C77 C78 C79 C80 C81 C82 C83
        ldaCwfl4819.getWork_Request_C2().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C3().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C4().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C5().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C6().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C7().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C8().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C9().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C10().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C11().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C12().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C13().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C14().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C15().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C16().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C17().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C18().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C19().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C20().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C21().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C22().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C23().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C24().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C25().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C26().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C27().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C28().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C29().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C30().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C31().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C32().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C33().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C34().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C35().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C36().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C37().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C38().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C39().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C40().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C41().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C42().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C43().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C44().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C45().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C46().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C47().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C48().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C49().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C50().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C51().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C52().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C53().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C54().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C55().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C56().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C57().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C58().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C59().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C60().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C61().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C62().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C63().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C64().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C65().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C66().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C67().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C68().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C69().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C70().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C71().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C72().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C73().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C74().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C75().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C76().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C77().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C78().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C79().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C80().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C81().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C82().setValue(pnd_Atsign);
        ldaCwfl4819.getWork_Request_C83().setValue(pnd_Atsign);
        //*  FORMAT-WR-DATES
    }
    private void sub_Get_Business_Data() throws Exception                                                                                                                 //Natural: GET-BUSINESS-DATA
    {
        if (BLNatReinput.isReinput()) return;

        ldaCwfl4829.getVw_cwf_Bsnss_Data_File().startDatabaseRead                                                                                                         //Natural: READ CWF-BSNSS-DATA-FILE BY RQST-LOG-DTE-TME STARTING FROM CWF-MASTER.RQST-LOG-DTE-TME
        (
        "RB",
        new Wc[] { new Wc("RQST_LOG_DTE_TME", ">=", ldaCwfl4817.getCwf_Master_Rqst_Log_Dte_Tme(), WcType.BY) },
        new Oc[] { new Oc("RQST_LOG_DTE_TME", "ASC") }
        );
        RB:
        while (condition(ldaCwfl4829.getVw_cwf_Bsnss_Data_File().readNextRow("RB")))
        {
            if (condition(ldaCwfl4829.getCwf_Bsnss_Data_File_Rqst_Log_Dte_Tme().notEquals(ldaCwfl4817.getCwf_Master_Rqst_Log_Dte_Tme())))                                 //Natural: IF CWF-BSNSS-DATA-FILE.RQST-LOG-DTE-TME NE CWF-MASTER.RQST-LOG-DTE-TME
            {
                if (true) break RB;                                                                                                                                       //Natural: ESCAPE BOTTOM ( RB. ) IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaCwfl4829.getCwf_Bsnss_Data_File_Da_Amt().greater(getZero())))                                                                                //Natural: IF CWF-BSNSS-DATA-FILE.DA-AMT GT 0
            {
                pnd_Number_W_Decimals_13.setValue(ldaCwfl4829.getCwf_Bsnss_Data_File_Da_Amt());                                                                           //Natural: ASSIGN #NUMBER-W-DECIMALS-13 := CWF-BSNSS-DATA-FILE.DA-AMT
                ldaCwfl4819.getWork_Request_Da_Accum_Cntr().setValueEdited(pnd_Number_W_Decimals_13,new ReportEditMask("99999999999.99"));                                //Natural: MOVE EDITED #NUMBER-W-DECIMALS-13 ( EM = 99999999999.99 ) TO WORK-REQUEST.DA-ACCUM-CNTR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaCwfl4829.getCwf_Bsnss_Data_File_Tpa_Amt().greater(getZero())))                                                                               //Natural: IF CWF-BSNSS-DATA-FILE.TPA-AMT GT 0
            {
                pnd_Number_W_Decimals_13.setValue(ldaCwfl4829.getCwf_Bsnss_Data_File_Tpa_Amt());                                                                          //Natural: ASSIGN #NUMBER-W-DECIMALS-13 := CWF-BSNSS-DATA-FILE.TPA-AMT
                ldaCwfl4819.getWork_Request_Tpa_Accum_Cntr().setValueEdited(pnd_Number_W_Decimals_13,new ReportEditMask("99999999999.99"));                               //Natural: MOVE EDITED #NUMBER-W-DECIMALS-13 ( EM = 99999999999.99 ) TO WORK-REQUEST.TPA-ACCUM-CNTR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaCwfl4829.getCwf_Bsnss_Data_File_P_And_I_Amt().greater(getZero())))                                                                           //Natural: IF CWF-BSNSS-DATA-FILE.P-AND-I-AMT GT 0
            {
                pnd_Number_W_Decimals_13.setValue(ldaCwfl4829.getCwf_Bsnss_Data_File_P_And_I_Amt());                                                                      //Natural: ASSIGN #NUMBER-W-DECIMALS-13 := CWF-BSNSS-DATA-FILE.P-AND-I-AMT
                ldaCwfl4819.getWork_Request_P_I_Accum_Cntr().setValueEdited(pnd_Number_W_Decimals_13,new ReportEditMask("99999999999.99"));                               //Natural: MOVE EDITED #NUMBER-W-DECIMALS-13 ( EM = 99999999999.99 ) TO WORK-REQUEST.P-I-ACCUM-CNTR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaCwfl4829.getCwf_Bsnss_Data_File_Ipro_Amt().greater(getZero())))                                                                              //Natural: IF CWF-BSNSS-DATA-FILE.IPRO-AMT GT 0
            {
                pnd_Number_W_Decimals_13.setValue(ldaCwfl4829.getCwf_Bsnss_Data_File_Ipro_Amt());                                                                         //Natural: ASSIGN #NUMBER-W-DECIMALS-13 := CWF-BSNSS-DATA-FILE.IPRO-AMT
                ldaCwfl4819.getWork_Request_Ipro_Accum_Cntr().setValueEdited(pnd_Number_W_Decimals_13,new ReportEditMask("99999999999.99"));                              //Natural: MOVE EDITED #NUMBER-W-DECIMALS-13 ( EM = 99999999999.99 ) TO WORK-REQUEST.IPRO-ACCUM-CNTR
            }                                                                                                                                                             //Natural: END-IF
            pnd_Number_W_Decimals_13.compute(new ComputeParameters(false, pnd_Number_W_Decimals_13), (ldaCwfl4829.getCwf_Bsnss_Data_File_Da_Amt().add(ldaCwfl4829.getCwf_Bsnss_Data_File_Tpa_Amt()).add(ldaCwfl4829.getCwf_Bsnss_Data_File_P_And_I_Amt()).add(ldaCwfl4829.getCwf_Bsnss_Data_File_Ipro_Amt()))); //Natural: COMPUTE #NUMBER-W-DECIMALS-13 = ( CWF-BSNSS-DATA-FILE.DA-AMT + CWF-BSNSS-DATA-FILE.TPA-AMT + CWF-BSNSS-DATA-FILE.P-AND-I-AMT + CWF-BSNSS-DATA-FILE.IPRO-AMT )
            ldaCwfl4819.getWork_Request_Tot_Accum_Cntr().setValueEdited(pnd_Number_W_Decimals_13,new ReportEditMask("99999999999.99"));                                   //Natural: MOVE EDITED #NUMBER-W-DECIMALS-13 ( EM = 99999999999.99 ) TO WORK-REQUEST.TOT-ACCUM-CNTR
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
            //*  RB.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  GET-BUSINESS-DATA
    }
    private void sub_Get_Part_Age() throws Exception                                                                                                                      //Natural: GET-PART-AGE
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        pdaNsta1245.getNsta1245_Pnd_Years().reset();                                                                                                                      //Natural: RESET NSTA1245.#YEARS
        if (condition(pdaNsta1245.getNsta1245_Pnd_Eff_Dte().greater(" ") && pdaNsta1245.getNsta1245_Pnd_Dob_Dte().greater(" ")))                                          //Natural: IF NSTA1245.#EFF-DTE GT ' ' AND NSTA1245.#DOB-DTE GT ' '
        {
            DbsUtil.callnat(Nstn1245.class , getCurrentProcessState(), pdaNsta1245.getNsta1245(), pdaCwfpda_M.getMsg_Info_Sub());                                         //Natural: CALLNAT 'NSTN1245' NSTA1245 MSG-INFO-SUB
            if (condition(Global.isEscape())) return;
            ldaCwfl4819.getWork_Request_Part_Age().setValue(pdaNsta1245.getNsta1245_Pnd_Years());                                                                         //Natural: ASSIGN WORK-REQUEST.PART-AGE := NSTA1245.#YEARS
        }                                                                                                                                                                 //Natural: END-IF
        //* GET-PART-AGE
    }
    private void sub_Write_Activity() throws Exception                                                                                                                    //Natural: WRITE-ACTIVITY
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        //*  POPULATE AN ACTIVITY
                                                                                                                                                                          //Natural: PERFORM FILL-ACTIVITY
        sub_Fill_Activity();
        if (condition(Global.isEscape())) {return;}
        //*  --------
        //*  CHECK FOR UNIT .....?
        if (condition(ldaCwfl4820.getActivity_Admin_Unit_Cde().equals(" ")))                                                                                              //Natural: IF ACTIVITY.ADMIN-UNIT-CDE = ' '
        {
            getReports().write(0, ReportOption.NOTITLE,"AdminUnit is Empty - No Act.will be Created ",pnd_Rqst_Log_Dte_Tme_Save,pnd_Pin_Number_Save,pnd_Isn);             //Natural: WRITE 'AdminUnit is Empty - No Act.will be Created ' #RQST-LOG-DTE-TME-SAVE #PIN-NUMBER-SAVE #ISN
            if (Global.isEscape()) return;
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  --------
        if (condition(pnd_Wipe_Out_End_Act.getBoolean()))                                                                                                                 //Natural: IF #WIPE-OUT-END-ACT
        {
            ldaCwfl4820.getActivity_Stop_Dte_Tme_A().setValue(" ");                                                                                                       //Natural: MOVE ' ' TO ACTIVITY.STOP-DTE-TME-A
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet3177 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #ACTIVITY-TYPE = 'UNIT'
        if (condition(pnd_Activity_Type.equals("UNIT")))
        {
            decideConditionsMet3177++;
            if (condition(pnd_Update.getBoolean()))                                                                                                                       //Natural: IF #UPDATE
            {
                getWorkFiles().write(6, false, ldaCwfl4820.getActivity());                                                                                                //Natural: WRITE WORK FILE 6 ACTIVITY
                pnd_Unita_Upd_Cntr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #UNITA-UPD-CNTR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getWorkFiles().write(5, false, ldaCwfl4820.getActivity());                                                                                                //Natural: WRITE WORK FILE 5 ACTIVITY
                pnd_Unita_Add_Cntr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #UNITA-ADD-CNTR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #ACTIVITY-TYPE = 'EMPL'
        else if (condition(pnd_Activity_Type.equals("EMPL")))
        {
            decideConditionsMet3177++;
            //*  --------
            //*  CHECK FOR EMPL-OPRTR.?
            if (condition(ldaCwfl4820.getActivity_Empl_Oprtr_Cde().equals(" ")))                                                                                          //Natural: IF ACTIVITY.EMPL-OPRTR-CDE = ' '
            {
                //*  WRITE 'EMPL-OPRTR   IS EMPTY - ACTIVITY NOT CREATED'
                //*        #RQST-LOG-DTE-TME-SAVE #PIN-NUMBER-SAVE #ISN
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*  --------
            if (condition(pnd_Update.getBoolean()))                                                                                                                       //Natural: IF #UPDATE
            {
                getWorkFiles().write(8, false, ldaCwfl4820.getActivity());                                                                                                //Natural: WRITE WORK FILE 8 ACTIVITY
                pnd_Empla_Upd_Cntr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #EMPLA-UPD-CNTR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getWorkFiles().write(7, false, ldaCwfl4820.getActivity());                                                                                                //Natural: WRITE WORK FILE 7 ACTIVITY
                pnd_Empla_Add_Cntr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #EMPLA-ADD-CNTR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #ACTIVITY-TYPE = 'STEP'
        else if (condition(pnd_Activity_Type.equals("STEP")))
        {
            decideConditionsMet3177++;
            //*  --------
            //*  CHECK FOR STEP-ID...?
            if (condition(ldaCwfl4820.getActivity_Step_Id().equals(" ")))                                                                                                 //Natural: IF ACTIVITY.STEP-ID = ' '
            {
                //*  WRITE 'STEP-ID      IS EMPTY - ACTIVITY NOT CREATED'
                //*        #RQST-LOG-DTE-TME-SAVE #PIN-NUMBER-SAVE #ISN
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*  --------
            if (condition(pnd_Update.getBoolean()))                                                                                                                       //Natural: IF #UPDATE
            {
                getWorkFiles().write(10, false, ldaCwfl4820.getActivity());                                                                                               //Natural: WRITE WORK FILE 10 ACTIVITY
                pnd_Stepa_Upd_Cntr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #STEPA-UPD-CNTR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getWorkFiles().write(9, false, ldaCwfl4820.getActivity());                                                                                                //Natural: WRITE WORK FILE 9 ACTIVITY
                pnd_Stepa_Add_Cntr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #STEPA-ADD-CNTR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #ACTIVITY-TYPE = 'INTR'
        else if (condition(pnd_Activity_Type.equals("INTR")))
        {
            decideConditionsMet3177++;
            if (condition(pnd_Update.getBoolean()))                                                                                                                       //Natural: IF #UPDATE
            {
                getWorkFiles().write(12, false, ldaCwfl4820.getActivity());                                                                                               //Natural: WRITE WORK FILE 12 ACTIVITY
                pnd_Intra_Upd_Cntr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #INTRA-UPD-CNTR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getWorkFiles().write(11, false, ldaCwfl4820.getActivity());                                                                                               //Natural: WRITE WORK FILE 11 ACTIVITY
                pnd_Intra_Add_Cntr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #INTRA-ADD-CNTR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #ACTIVITY-TYPE = 'EXTR'
        else if (condition(pnd_Activity_Type.equals("EXTR")))
        {
            decideConditionsMet3177++;
            if (condition(pnd_Update.getBoolean()))                                                                                                                       //Natural: IF #UPDATE
            {
                getWorkFiles().write(14, false, ldaCwfl4820.getActivity());                                                                                               //Natural: WRITE WORK FILE 14 ACTIVITY
                pnd_Extra_Upd_Cntr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #EXTRA-UPD-CNTR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getWorkFiles().write(13, false, ldaCwfl4820.getActivity());                                                                                               //Natural: WRITE WORK FILE 13 ACTIVITY
                pnd_Extra_Add_Cntr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #EXTRA-ADD-CNTR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #ACTIVITY-TYPE = 'ENRT'
        else if (condition(pnd_Activity_Type.equals("ENRT")))
        {
            decideConditionsMet3177++;
            if (condition(pnd_Update.getBoolean()))                                                                                                                       //Natural: IF #UPDATE
            {
                getWorkFiles().write(16, false, ldaCwfl4820.getActivity());                                                                                               //Natural: WRITE WORK FILE 16 ACTIVITY
                pnd_Enrta_Upd_Cntr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #ENRTA-UPD-CNTR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getWorkFiles().write(15, false, ldaCwfl4820.getActivity());                                                                                               //Natural: WRITE WORK FILE 15 ACTIVITY
                pnd_Enrta_Add_Cntr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #ENRTA-ADD-CNTR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #ACTIVITY-TYPE = 'C-ST'
        else if (condition(pnd_Activity_Type.equals("C-ST")))
        {
            decideConditionsMet3177++;
            ldaCwfl4822.getCstatus().reset();                                                                                                                             //Natural: RESET CSTATUS
            ldaCwfl4822.getCstatus().setValuesByName(ldaCwfl4817.getVw_cwf_Master());                                                                                     //Natural: MOVE BY NAME CWF-MASTER TO CSTATUS
            pnd_Time_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaCwfl4817.getCwf_Master_Rqst_Log_Dte_Tme());                                                //Natural: MOVE EDITED CWF-MASTER.RQST-LOG-DTE-TME TO #TIME-T ( EM = YYYYMMDDHHIISST )
            ldaCwfl4822.getCstatus_Rqst_Log_Dte_Tme_A().setValueEdited(pnd_Time_T,new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SS':'0AP'''"));                            //Natural: MOVE EDITED #TIME-T ( EM = '"'LLL�DD�YYYY�HH':'II':'SS':'T'00'AP'"' ) TO CSTATUS.RQST-LOG-DTE-TME-A
            pnd_Time_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaCwfl4817.getCwf_Master_Last_Chnge_Dte_Tme_A());                                            //Natural: MOVE EDITED CWF-MASTER.LAST-CHNGE-DTE-TME-A TO #TIME-T ( EM = YYYYMMDDHHIISST )
            ldaCwfl4822.getCstatus_Last_Chnge_Dte_Tme_A().setValueEdited(pnd_Time_T,new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''"));                              //Natural: MOVE EDITED #TIME-T ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO CSTATUS.LAST-CHNGE-DTE-TME-A
            ldaCwfl4822.getCstatus_Status_Description().setValue(ldaCwfl4818.getCwf_Route_Support_Tbl_Status_Desc());                                                     //Natural: MOVE CWF-ROUTE-SUPPORT-TBL.STATUS-DESC TO CSTATUS.STATUS-DESCRIPTION
            ldaCwfl4822.getCstatus_F1().setValue(pnd_Atsign);                                                                                                             //Natural: MOVE #ATSIGN TO F1 F2 F3 F4 F5
            ldaCwfl4822.getCstatus_F2().setValue(pnd_Atsign);
            ldaCwfl4822.getCstatus_F3().setValue(pnd_Atsign);
            ldaCwfl4822.getCstatus_F4().setValue(pnd_Atsign);
            ldaCwfl4822.getCstatus_F5().setValue(pnd_Atsign);
            if (condition(pnd_Add.getBoolean()))                                                                                                                          //Natural: IF #ADD
            {
                getWorkFiles().write(19, false, ldaCwfl4822.getCstatus());                                                                                                //Natural: WRITE WORK FILE 19 CSTATUS
                pnd_Cstat_Add_Cntr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #CSTAT-ADD-CNTR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Fill_Activity() throws Exception                                                                                                                     //Natural: FILL-ACTIVITY
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        ldaCwfl4820.getActivity().setValuesByName(ldaCwfl4817.getVw_cwf_Master());                                                                                        //Natural: MOVE BY NAME CWF-MASTER TO ACTIVITY
        if (condition(pnd_Activity_Type.equals("UNIT")))                                                                                                                  //Natural: IF #ACTIVITY-TYPE = 'UNIT'
        {
            ldaCwfl4820.getActivity_Status_Cde().setValue(ldaCwfl4817.getCwf_Master_Unit_Begin_Status());                                                                 //Natural: MOVE UNIT-BEGIN-STATUS TO ACTIVITY.STATUS-CDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Activity_Type.equals("EMPL")))                                                                                                                  //Natural: IF #ACTIVITY-TYPE = 'EMPL'
        {
            ldaCwfl4820.getActivity_Status_Cde().setValue(ldaCwfl4817.getCwf_Master_Empl_Begin_Status());                                                                 //Natural: MOVE EMPL-BEGIN-STATUS TO ACTIVITY.STATUS-CDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Activity_Type.equals("STEP")))                                                                                                                  //Natural: IF #ACTIVITY-TYPE = 'STEP'
        {
            ldaCwfl4820.getActivity_Status_Cde().setValue(ldaCwfl4817.getCwf_Master_Step_Begin_Status());                                                                 //Natural: MOVE STEP-BEGIN-STATUS TO ACTIVITY.STATUS-CDE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Time_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaCwfl4817.getCwf_Master_Rqst_Log_Dte_Tme());                                                    //Natural: MOVE EDITED CWF-MASTER.RQST-LOG-DTE-TME TO #TIME-T ( EM = YYYYMMDDHHIISST )
        ldaCwfl4820.getActivity_Rqst_Log_Dte_Tme_A().setValueEdited(pnd_Time_T,new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SS':'0AP'''"));                               //Natural: MOVE EDITED #TIME-T ( EM = '"'LLL�DD�YYYY�HH':'II':'SS':'T'00'AP'"' ) TO ACTIVITY.RQST-LOG-DTE-TME-A
        if (condition(pnd_Act_Strt_Time_T.greater(getZero())))                                                                                                            //Natural: IF #ACT-STRT-TIME-T GT 0
        {
            ldaCwfl4820.getActivity_Strt_Dte_Tme_A().setValueEdited(pnd_Act_Strt_Time_T,new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''"));                          //Natural: MOVE EDITED #ACT-STRT-TIME-T ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO ACTIVITY.STRT-DTE-TME-A
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCwfl4820.getActivity_Strt_Dte_Tme_A().setValue(" ");                                                                                                       //Natural: MOVE ' ' TO ACTIVITY.STRT-DTE-TME-A
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Act_Stop_Time_T.greater(getZero())))                                                                                                            //Natural: IF #ACT-STOP-TIME-T GT 0
        {
            ldaCwfl4820.getActivity_Stop_Dte_Tme_A().setValueEdited(pnd_Act_Stop_Time_T,new ReportEditMask("'''LLL�DD�YYYY�HH':'II':'SSAP'''"));                          //Natural: MOVE EDITED #ACT-STOP-TIME-T ( EM = '"'LLL�DD�YYYY�HH':'II':'SSAP'"' ) TO ACTIVITY.STOP-DTE-TME-A
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCwfl4820.getActivity_Stop_Dte_Tme_A().setValue(" ");                                                                                                       //Natural: MOVE ' ' TO ACTIVITY.STOP-DTE-TME-A
        }                                                                                                                                                                 //Natural: END-IF
        ldaCwfl4820.getActivity_A1().setValue(pnd_Atsign);                                                                                                                //Natural: MOVE #ATSIGN TO A1 A2 A3 A4 A5 A6 A7 A8 A9 A10
        ldaCwfl4820.getActivity_A2().setValue(pnd_Atsign);
        ldaCwfl4820.getActivity_A3().setValue(pnd_Atsign);
        ldaCwfl4820.getActivity_A4().setValue(pnd_Atsign);
        ldaCwfl4820.getActivity_A5().setValue(pnd_Atsign);
        ldaCwfl4820.getActivity_A6().setValue(pnd_Atsign);
        ldaCwfl4820.getActivity_A7().setValue(pnd_Atsign);
        ldaCwfl4820.getActivity_A8().setValue(pnd_Atsign);
        ldaCwfl4820.getActivity_A9().setValue(pnd_Atsign);
        ldaCwfl4820.getActivity_A10().setValue(pnd_Atsign);
    }
    private void sub_Derive_Indicators() throws Exception                                                                                                                 //Natural: DERIVE-INDICATORS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        short decideConditionsMet3290 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CWF-MASTER.CRPRTE-STATUS-IND = '9'
        if (condition(ldaCwfl4817.getCwf_Master_Crprte_Status_Ind().equals("9")))
        {
            decideConditionsMet3290++;
            ldaCwfl4819.getWork_Request_Crprte_Status_Ind().setValue("C");                                                                                                //Natural: MOVE 'C' TO WORK-REQUEST.CRPRTE-STATUS-IND
        }                                                                                                                                                                 //Natural: WHEN CWF-MASTER.CRPRTE-STATUS-IND = '0'
        else if (condition(ldaCwfl4817.getCwf_Master_Crprte_Status_Ind().equals("0")))
        {
            decideConditionsMet3290++;
            if (condition(ldaCwfl4817.getCwf_Master_Status_Freeze_Ind().equals("Y")))                                                                                     //Natural: IF CWF-MASTER.STATUS-FREEZE-IND = 'Y'
            {
                ldaCwfl4819.getWork_Request_Crprte_Status_Ind().setValue("P");                                                                                            //Natural: MOVE 'P' TO WORK-REQUEST.CRPRTE-STATUS-IND
                ldaCwfl4819.getWork_Request_Final_Close_Out_Dte_Tme_A().setValue(" ");                                                                                    //Natural: MOVE ' ' TO WORK-REQUEST.FINAL-CLOSE-OUT-DTE-TME-A
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaCwfl4819.getWork_Request_Crprte_Status_Ind().setValue("O");                                                                                            //Natural: MOVE 'O' TO WORK-REQUEST.CRPRTE-STATUS-IND
                ldaCwfl4819.getWork_Request_Final_Close_Out_Dte_Tme_A().setValue(" ");                                                                                    //Natural: MOVE ' ' TO WORK-REQUEST.FINAL-CLOSE-OUT-DTE-TME-A
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(ldaCwfl4817.getCwf_Master_Elctrnc_Fldr_Ind().equals("Y")))                                                                                          //Natural: IF CWF-MASTER.ELCTRNC-FLDR-IND = 'Y'
        {
            ldaCwfl4819.getWork_Request_Elctrnc_Fldr_Ind().setValue("Y");                                                                                                 //Natural: ASSIGN WORK-REQUEST.ELCTRNC-FLDR-IND := 'Y'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCwfl4819.getWork_Request_Elctrnc_Fldr_Ind().setValue("N");                                                                                                 //Natural: ASSIGN WORK-REQUEST.ELCTRNC-FLDR-IND := 'N'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Conversion_Due_Dte_Chg_Prty_Cde_Pnd_Prcssng_Type_Cde.equals("A")))                                                                              //Natural: IF #CONVERSION-DUE-DTE-CHG-PRTY-CDE.#PRCSSNG-TYPE-CDE = 'A'
        {
            ldaCwfl4819.getWork_Request_Prcssng_Type_Cde().setValue("A");                                                                                                 //Natural: MOVE 'A' TO WORK-REQUEST.PRCSSNG-TYPE-CDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCwfl4819.getWork_Request_Prcssng_Type_Cde().setValue("E");                                                                                                 //Natural: MOVE 'E' TO WORK-REQUEST.PRCSSNG-TYPE-CDE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM READ-WPID
        sub_Read_Wpid();
        if (condition(Global.isEscape())) {return;}
        pnd_Pass_Unit.reset();                                                                                                                                            //Natural: RESET #PASS-UNIT
        if (condition(ldaCwfl4819.getWork_Request_Owner_Unit_Cde().notEquals(" ")))                                                                                       //Natural: IF WORK-REQUEST.OWNER-UNIT-CDE NE ' '
        {
            pnd_Pass_Unit.setValue(ldaCwfl4819.getWork_Request_Owner_Unit_Cde());                                                                                         //Natural: ASSIGN #PASS-UNIT := WORK-REQUEST.OWNER-UNIT-CDE
                                                                                                                                                                          //Natural: PERFORM GET-DIVISION
            sub_Get_Division();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Pass_Unit.equals(" ")))                                                                                                                         //Natural: IF #PASS-UNIT = ' '
        {
            short decideConditionsMet3325 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CWFA1400.OWNER-UNIT-CDE NE ' '
            if (condition(pdaCwfa1400.getCwfa1400_Owner_Unit_Cde().notEquals(" ")))
            {
                decideConditionsMet3325++;
                pnd_Pass_Unit.setValue(pdaCwfa1400.getCwfa1400_Owner_Unit_Cde());                                                                                         //Natural: ASSIGN #PASS-UNIT := CWFA1400.OWNER-UNIT-CDE
            }                                                                                                                                                             //Natural: WHEN CWFA1400.UNIT-CDE ( 1 ) NE ' '
            else if (condition(pdaCwfa1400.getCwfa1400_Unit_Cde().getValue(1).notEquals(" ")))
            {
                decideConditionsMet3325++;
                pnd_Pass_Unit.setValue(pdaCwfa1400.getCwfa1400_Unit_Cde().getValue(1));                                                                                   //Natural: ASSIGN #PASS-UNIT := CWFA1400.UNIT-CDE ( 1 )
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet3325 > 0))
            {
                ldaCwfl4819.getWork_Request_Owner_Unit_Cde().setValue(pnd_Pass_Unit);                                                                                     //Natural: ASSIGN WORK-REQUEST.OWNER-UNIT-CDE := #PASS-UNIT
                                                                                                                                                                          //Natural: PERFORM GET-DIVISION
                sub_Get_Division();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ldaCwfl4819.getWork_Request_Owner_Division().reset();                                                                                                     //Natural: RESET WORK-REQUEST.OWNER-DIVISION
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Cab_Key_Pnd_Pin_Nbr.setValue(ldaCwfl4819.getWork_Request_Pin_Nbr());                                                                                          //Natural: ASSIGN #CAB-KEY.#PIN-NBR := WORK-REQUEST.PIN-NBR
                                                                                                                                                                          //Natural: PERFORM GET-MEDIA-IND
        sub_Get_Media_Ind();
        if (condition(Global.isEscape())) {return;}
        ldaCwfl4819.getWork_Request_Mj_Media_Ind().setValue(cwf_Efm_Cabinet_Media_Ind);                                                                                   //Natural: ASSIGN WORK-REQUEST.MJ-MEDIA-IND := CWF-EFM-CABINET.MEDIA-IND
        //*  ------------
        //*  DERIVE-INDICATORS
    }
    private void sub_Get_Division() throws Exception                                                                                                                      //Natural: GET-DIVISION
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        DbsUtil.callnat(Cwfn4033.class , getCurrentProcessState(), pnd_Pass_Unit, pnd_Pass_Division);                                                                     //Natural: CALLNAT 'CWFN4033' #PASS-UNIT #PASS-DIVISION
        if (condition(Global.isEscape())) return;
        ldaCwfl4819.getWork_Request_Owner_Division().setValue(pnd_Pass_Division);                                                                                         //Natural: ASSIGN WORK-REQUEST.OWNER-DIVISION := #PASS-DIVISION
        //*  ------------
        //*  GET-DIVISION
    }
    private void sub_Get_Media_Ind() throws Exception                                                                                                                     //Natural: GET-MEDIA-IND
    {
        if (BLNatReinput.isReinput()) return;

        vw_cwf_Efm_Cabinet.startDatabaseRead                                                                                                                              //Natural: READ ( 1 ) CWF-EFM-CABINET BY CABINET-ID STARTING FROM #CAB-KEY
        (
        "EF1",
        new Wc[] { new Wc("CABINET_ID", ">=", pnd_Cab_Key, WcType.BY) },
        new Oc[] { new Oc("CABINET_ID", "ASC") },
        1
        );
        EF1:
        while (condition(vw_cwf_Efm_Cabinet.readNextRow("EF1")))
        {
            //*  EF1.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(cwf_Efm_Cabinet_Pnd_Pin_Nbr.notEquals(pnd_Cab_Key_Pnd_Pin_Nbr)))                                                                                    //Natural: IF CWF-EFM-CABINET.#PIN-NBR NE #CAB-KEY.#PIN-NBR
        {
            ldaCwfl4819.getWork_Request_Mj_Media_Ind().reset();                                                                                                           //Natural: RESET WORK-REQUEST.MJ-MEDIA-IND
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  ------------
        //*  GET-MEDIA-IND
    }
    private void sub_Read_Wpid() throws Exception                                                                                                                         //Natural: READ-WPID
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************
        pnd_Found_One.setValue(false);                                                                                                                                    //Natural: MOVE FALSE TO #FOUND-ONE
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("GET");                                                                                                               //Natural: MOVE 'GET' TO CDAOBJ.#FUNCTION
        pdaCwfa1400.getCwfa1400_Work_Prcss_Id().setValue(ldaCwfl4817.getCwf_Master_Work_Prcss_Id());                                                                      //Natural: MOVE CWF-MASTER.WORK-PRCSS-ID TO CWFA1400.WORK-PRCSS-ID
        DbsUtil.callnat(Cwfx1400.class , getCurrentProcessState(), pdaCwfa1400.getCwfa1400(), pdaCwfa1400.getCwfa1400_Id(), pdaCwfa1401.getCwfa1401(),                    //Natural: CALLNAT 'CWFX1400' CWFA1400 CWFA1400-ID CWFA1401 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##ERROR-FIELD NE ' '
        {
            pnd_Error_Msg.setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                                            //Natural: MOVE MSG-INFO-SUB.##MSG TO #ERROR-MSG
            pnd_Error_Msg.setValue(DbsUtil.compress("Sec-Ind not found for WPID:", ldaCwfl4817.getCwf_Master_Work_Prcss_Id()));                                           //Natural: COMPRESS 'Sec-Ind not found for WPID:' CWF-MASTER.WORK-PRCSS-ID INTO #ERROR-MSG
            //*   PERFORM WRITE-ERROR
            pdaCwfa1400.getCwfa1400().reset();                                                                                                                            //Natural: RESET CWFA1400
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCwfl4819.getWork_Request_Sec_Ind().setValue(pdaCwfa1400.getCwfa1400_Sec_Ind());                                                                            //Natural: MOVE CWFA1400.SEC-IND TO WORK-REQUEST.SEC-IND
            pnd_Service_Hrs.setValue(pdaCwfa1400.getCwfa1400_Crprte_Srvce_Stndrd_Hours_Nbr());                                                                            //Natural: MOVE CWFA1400.CRPRTE-SRVCE-STNDRD-HOURS-NBR TO #SERVICE-HRS
            pnd_Service_Mins.setValue(pdaCwfa1400.getCwfa1400_Crprte_Srvce_Stndrd_Mins_Nbr());                                                                            //Natural: MOVE CWFA1400.CRPRTE-SRVCE-STNDRD-MINS-NBR TO #SERVICE-MINS
            pnd_Service_Days.setValue(pdaCwfa1400.getCwfa1400_Crprte_Srvce_Stndrd_Days_Nbr());                                                                            //Natural: MOVE CWFA1400.CRPRTE-SRVCE-STNDRD-DAYS-NBR TO #SERVICE-DAYS
        }                                                                                                                                                                 //Natural: END-IF
        //* *************
        //*  GET-SEC-IND
    }
    private void sub_Write_Totals() throws Exception                                                                                                                      //Natural: WRITE-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        getReports().write(1, NEWLINE,"Program:",Global.getPROGRAM(),new TabSetting(45),"Run Date:",Global.getDATX(),NEWLINE,"******* MIT Extract Statistics for Corporate Reporting ********",NEWLINE,"Begining period    = ",pnd_Tbl_Start_Dte_Tme_T,  //Natural: WRITE ( 1 ) / 'Program:' *PROGRAM 45T 'Run Date:' *DATX / '******* MIT Extract Statistics for Corporate Reporting ********' / 'Begining period    = ' #TBL-START-DTE-TME-T ( EM = YYYYMMDDHHIISST ) / 'Ending   period    = ' #TBL-END-DTE-TME-T ( EM = YYYYMMDDHHIISST ) / '***************************************************************' / 'Number of Work-File Records which has been read = ' #WORK-READ-CNTR / 'Number of MIT Records which has been read       = ' #MIT-READ-CNTR / '---------------- Work Request Statistic -------------' / 'Number of Work Request Records Added            = ' #WRQST-ADD-CNTR / 'Number of Work Request Records Updated          = ' #WRQST-UPD-CNTR / '---------------- Activities Statistic ---------------' / 'Number of Unit Activities Added                 = ' #UNITA-ADD-CNTR / 'Number of Unit Activities Updated               = ' #UNITA-UPD-CNTR / 'Number of Employee Activities Added             = ' #EMPLA-ADD-CNTR / 'Number of Employee Activities Updated           = ' #EMPLA-UPD-CNTR / 'Number of Step Activities Added                 = ' #STEPA-ADD-CNTR / 'Number of Step Activities Updated               = ' #STEPA-UPD-CNTR / 'Number of Internal Pend Activities Added        = ' #INTRA-ADD-CNTR / 'Number of Internal Pend Activities Updated      = ' #INTRA-UPD-CNTR / 'Number of External Pend Activities Added        = ' #EXTRA-ADD-CNTR / 'Number of External Pend Activities Updated      = ' #EXTRA-UPD-CNTR / 'Number of En-Route-To Activities Added          = ' #ENRTA-ADD-CNTR / 'Number of En-Route-To Activities Updated        = ' #ENRTA-UPD-CNTR / 'Number of Work Requests Deleted                 = ' #DEL-WRQST-CNTR / 'Number of C-Status records Added                = ' #CSTAT-ADD-CNTR / 'Number of records on Total file                 = ' #TOTAL-REC-CNTR / 'Number of Additional Work Request Added         = ' #ADTNL-WRK-ADD / 'Number of Additional Work Request Updated       = ' #ADTNL-WRK-UPD / 'Number of Late Indicator Added                  = ' #LATE-ADD / 'Number of Late Indicator Updated                = ' #LATE-UPD / 'Number of TLC records created                   = ' #TLC-CNTR / '----------------- Contract Statistic --------------- ' / 'Number of Contract Added                        = ' #CNTRT-ADD-CNTR / 'Number of Contract Updated                      = ' #CNTRT-UPD-CNTR / '----------------- Problems --------------------------' / 'No Address Found                                = ' #NO-BASE-REC / 'Vacation Address Found                          = ' #VAC-ADDRESS / 'No Vacation Address Found                       = ' #NO-BASE-REC-V / 'No State in the Table                           = ' #NO-STATE-IN-TABLE / 'Name and SS number Not found (WR Not created)   = ' #PASS-NFND-CNTR / 'Number of Active Work Requests found            = ' #NBR-ACT-RECS / 'Number of ReRun Active records Accepted         = ' #ACT-WR-ACCEPT / 'Re-Run Work Request skipped                     = ' #RERUN-WR-SKIP / '***************************************************' / 'Elapsed Time to Process Records ' '(HH:MM:SS.T) :' *TIMD ( ST. ) ( EM = 99:99:99'.'9 )
            new ReportEditMask ("YYYYMMDDHHIISST"),NEWLINE,"Ending   period    = ",pnd_Tbl_End_Dte_Tme_T, new ReportEditMask ("YYYYMMDDHHIISST"),NEWLINE,"***************************************************************",NEWLINE,"Number of Work-File Records which has been read = ",pnd_Work_Read_Cntr,NEWLINE,"Number of MIT Records which has been read       = ",pnd_Mit_Read_Cntr,NEWLINE,"---------------- Work Request Statistic -------------",NEWLINE,"Number of Work Request Records Added            = ",pnd_Wrqst_Add_Cntr,NEWLINE,"Number of Work Request Records Updated          = ",pnd_Wrqst_Upd_Cntr,NEWLINE,"---------------- Activities Statistic ---------------",NEWLINE,"Number of Unit Activities Added                 = ",pnd_Unita_Add_Cntr,NEWLINE,"Number of Unit Activities Updated               = ",pnd_Unita_Upd_Cntr,NEWLINE,"Number of Employee Activities Added             = ",pnd_Empla_Add_Cntr,NEWLINE,"Number of Employee Activities Updated           = ",pnd_Empla_Upd_Cntr,NEWLINE,"Number of Step Activities Added                 = ",pnd_Stepa_Add_Cntr,NEWLINE,"Number of Step Activities Updated               = ",pnd_Stepa_Upd_Cntr,NEWLINE,"Number of Internal Pend Activities Added        = ",pnd_Intra_Add_Cntr,NEWLINE,"Number of Internal Pend Activities Updated      = ",pnd_Intra_Upd_Cntr,NEWLINE,"Number of External Pend Activities Added        = ",pnd_Extra_Add_Cntr,NEWLINE,"Number of External Pend Activities Updated      = ",pnd_Extra_Upd_Cntr,NEWLINE,"Number of En-Route-To Activities Added          = ",pnd_Enrta_Add_Cntr,NEWLINE,"Number of En-Route-To Activities Updated        = ",pnd_Enrta_Upd_Cntr,NEWLINE,"Number of Work Requests Deleted                 = ",pnd_Del_Wrqst_Cntr,NEWLINE,"Number of C-Status records Added                = ",pnd_Cstat_Add_Cntr,NEWLINE,"Number of records on Total file                 = ",pnd_Total_Rec_Cntr,NEWLINE,"Number of Additional Work Request Added         = ",pnd_Adtnl_Wrk_Add,NEWLINE,"Number of Additional Work Request Updated       = ",pnd_Adtnl_Wrk_Upd,NEWLINE,"Number of Late Indicator Added                  = ",pnd_Late_Add,NEWLINE,"Number of Late Indicator Updated                = ",pnd_Late_Upd,NEWLINE,"Number of TLC records created                   = ",pnd_Tlc_Cntr,NEWLINE,"----------------- Contract Statistic --------------- ",NEWLINE,"Number of Contract Added                        = ",pnd_Cntrt_Add_Cntr,NEWLINE,"Number of Contract Updated                      = ",pnd_Cntrt_Upd_Cntr,NEWLINE,"----------------- Problems --------------------------",NEWLINE,"No Address Found                                = ",pnd_No_Base_Rec,NEWLINE,"Vacation Address Found                          = ",pnd_Vac_Address,NEWLINE,"No Vacation Address Found                       = ",pnd_No_Base_Rec_V,NEWLINE,"No State in the Table                           = ",pnd_No_State_In_Table,NEWLINE,"Name and SS number Not found (WR Not created)   = ",pnd_Pass_Nfnd_Cntr,NEWLINE,"Number of Active Work Requests found            = ",pnd_Nbr_Act_Recs,NEWLINE,"Number of ReRun Active records Accepted         = ",pnd_Act_Wr_Accept,NEWLINE,"Re-Run Work Request skipped                     = ",pnd_Rerun_Wr_Skip,NEWLINE,"***************************************************",NEWLINE,"Elapsed Time to Process Records ","(HH:MM:SS.T) :",st, 
            new ReportEditMask ("99:99:99'.'9"));
        if (Global.isEscape()) return;
    }
    private void sub_Get_Name() throws Exception                                                                                                                          //Natural: GET-NAME
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Pass_Pin.setValue(ldaCwfl4817.getCwf_Master_Pin_Nbr());                                                                                                       //Natural: ASSIGN #PASS-PIN := CWF-MASTER.PIN-NBR
        pnd_Pass_Found.setValue(false);                                                                                                                                   //Natural: MOVE FALSE TO #PASS-FOUND
        //*  TO CALCULATE PARTICIPANT's Age
        pdaNsta1245.getNsta1245_Pnd_Dob_Dte().reset();                                                                                                                    //Natural: RESET NSTA1245.#DOB-DTE
        //*  COR/NAAD SUNSET START
        //*  CALLNAT 'CWFX5372'
        //*   #PASS-PIN
        //*   #PASS-NAME
        //*   #PASS-SSN
        //*   #PASS-TLC
        //*   #PASS-DOB
        //*   #PASS-FOUND
        //*  MQ OPEN
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        DbsUtil.callnat(Cwfn5372.class , getCurrentProcessState(), pnd_Pass_Pin, pnd_Pass_Name, pnd_Pass_Ssn, pnd_Pass_Tlc, pnd_Pass_Dob, pnd_Pass_Found);                //Natural: CALLNAT 'CWFN5372' #PASS-PIN #PASS-NAME #PASS-SSN #PASS-TLC #PASS-DOB #PASS-FOUND
        if (condition(Global.isEscape())) return;
        //*  MQ CLOSE
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        //*  COR/NAAD SUNSET END
        if (condition(pnd_Pass_Found.getBoolean()))                                                                                                                       //Natural: IF #PASS-FOUND
        {
            ldaCwfl4819.getWork_Request_Part_Name().setValue(pnd_Pass_Name);                                                                                              //Natural: ASSIGN WORK-REQUEST.PART-NAME := #PASS-NAME
            ldaCwfl4819.getWork_Request_Ssn().setValue(pnd_Pass_Ssn);                                                                                                     //Natural: ASSIGN WORK-REQUEST.SSN := #PASS-SSN
            short decideConditionsMet3411 = 0;                                                                                                                            //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #PASS-TLC GT ' '
            if (condition(pnd_Pass_Tlc.greater(" ")))
            {
                decideConditionsMet3411++;
                ldaCwfl4819.getWork_Request_Tlc_Ind().setValue("Y");                                                                                                      //Natural: ASSIGN WORK-REQUEST.TLC-IND := 'Y'
            }                                                                                                                                                             //Natural: WHEN #PASS-DOB NE 0
            if (condition(pnd_Pass_Dob.notEquals(getZero())))
            {
                decideConditionsMet3411++;
                //*  AGE
                pdaNsta1245.getNsta1245_Pnd_Dob_Dte().setValue(pnd_Pass_Dob_Pnd_Pass_Dob_A);                                                                              //Natural: MOVE #PASS-DOB-A TO NSTA1245.#DOB-DTE
                pnd_Dob.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Pass_Dob_Pnd_Pass_Dob_A);                                                                       //Natural: MOVE EDITED #PASS-DOB-A TO #DOB ( EM = YYYYMMDD )
                pnd_Fix_Dte_10.setValueEdited(pnd_Dob,new ReportEditMask("MM/DD/YYYY"));                                                                                  //Natural: MOVE EDITED #DOB ( EM = MM/DD/YYYY ) TO #FIX-DTE-10
                if (condition(pnd_Fix_Dte_10_Pnd_Fix_Dte_Yyyy.less(1800)))                                                                                                //Natural: IF #FIX-DTE-YYYY < 1800
                {
                    getReports().write(0, ReportOption.NOTITLE,NEWLINE,"Bad Date of Birth for PIN:",ldaCwfl4817.getCwf_Master_Pin_Nbr(),"RQST Log",ldaCwfl4817.getCwf_Master_Rqst_Log_Dte_Tme()); //Natural: WRITE /'Bad Date of Birth for PIN:' CWF-MASTER.PIN-NBR 'RQST Log' CWF-MASTER.RQST-LOG-DTE-TME
                    if (Global.isEscape()) return;
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaCwfl4819.getWork_Request_Dte_Of_Birth().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "'", pnd_Fix_Dte_10, "'"));                       //Natural: COMPRESS '"' #FIX-DTE-10 '"' INTO WORK-REQUEST.DTE-OF-BIRTH LEAVING NO SPACE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet3411 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*      WRITE / 'NO RECORD ON COR FILE FOR PIN:' CWF-MASTER.PIN-NBR
            //*              'RQST LOG' CWF-MASTER.RQST-LOG-DTE-TME
            pnd_Pass_Nfnd_Cntr.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #PASS-NFND-CNTR
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //* *************
        //*  GET-NAME
    }
    private void sub_Fill_N_Write_Contract() throws Exception                                                                                                             //Natural: FILL-N-WRITE-CONTRACT
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        ldaCwfl4821.getContract().reset();                                                                                                                                //Natural: RESET CONTRACT
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO 10
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(10)); pnd_I.nadd(1))
        {
            if (condition(ldaCwfl4817.getCwf_Master_Cntrct_Nbr().getValue(pnd_I).notEquals(" ")))                                                                         //Natural: IF CWF-MASTER.CNTRCT-NBR ( #I ) NE ' '
            {
                ldaCwfl4821.getContract_Cntrct_Num().setValue(ldaCwfl4817.getCwf_Master_Cntrct_Nbr().getValue(pnd_I));                                                    //Natural: MOVE CWF-MASTER.CNTRCT-NBR ( #I ) TO CONTRACT.CNTRCT-NUM
                ldaCwfl4821.getContract_Cntrct_Seq_Num().setValue(pnd_I);                                                                                                 //Natural: MOVE #I TO CONTRACT.CNTRCT-SEQ-NUM
                pnd_Time_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaCwfl4817.getCwf_Master_Rqst_Log_Dte_Tme());                                            //Natural: MOVE EDITED CWF-MASTER.RQST-LOG-DTE-TME TO #TIME-T ( EM = YYYYMMDDHHIISST )
                ldaCwfl4821.getContract_Rqst_Log_Dte_Tme().setValueEdited(pnd_Time_T,new ReportEditMask("'''LLL' 'DD' 'YYYY' 'HH':'II':'SS':'0AP'''"));                   //Natural: MOVE EDITED #TIME-T ( EM = '"'LLL' 'DD' 'YYYY' 'HH':'II':'SS':'T'00'AP'"' ) TO CONTRACT.RQST-LOG-DTE-TME
                ldaCwfl4821.getContract_Pnd_A().setValue(pnd_Atsign);                                                                                                     //Natural: MOVE #ATSIGN TO #A #B
                ldaCwfl4821.getContract_Pnd_B().setValue(pnd_Atsign);
                if (condition(pnd_Update.getBoolean()))                                                                                                                   //Natural: IF #UPDATE
                {
                    getWorkFiles().write(18, false, ldaCwfl4821.getContract());                                                                                           //Natural: WRITE WORK FILE 18 CONTRACT
                    pnd_Cntrt_Upd_Cntr.nadd(1);                                                                                                                           //Natural: ADD 1 TO #CNTRT-UPD-CNTR
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getWorkFiles().write(17, false, ldaCwfl4821.getContract());                                                                                           //Natural: WRITE WORK FILE 17 CONTRACT
                    pnd_Cntrt_Add_Cntr.nadd(1);                                                                                                                           //Natural: ADD 1 TO #CNTRT-ADD-CNTR
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  FILL-CONTRACT
    }
    private void sub_Delete_Cwf_Corp_Rpt_Wr() throws Exception                                                                                                            //Natural: DELETE-CWF-CORP-RPT-WR
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        if (condition(ldaCwfl4817.getCwf_Master_Merged_Rqst_Log_Dte_Tme().greater(" ")))                                                                                  //Natural: IF CWF-MASTER.MERGED-RQST-LOG-DTE-TME GT ' '
        {
            pnd_Time_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaCwfl4817.getCwf_Master_Merged_Rqst_Log_Dte_Tme());                                         //Natural: MOVE EDITED CWF-MASTER.MERGED-RQST-LOG-DTE-TME TO #TIME-T ( EM = YYYYMMDDHHIISST )
            ldaCwfl4823.getWrdel_Rqst_Log_Dte_Tme().setValueEdited(pnd_Time_T,new ReportEditMask("'''LLL' 'DD' 'YYYY' 'HH':'II':'SS':'0AP'''"));                          //Natural: MOVE EDITED #TIME-T ( EM = '"'LLL' 'DD' 'YYYY' 'HH':'II':'SS':'T'00'AP'"' ) TO WRDEL.RQST-LOG-DTE-TME
            getWorkFiles().write(4, false, ldaCwfl4823.getWrdel());                                                                                                       //Natural: WRITE WORK FILE 4 WRDEL
            pnd_Del_Wrqst_Cntr.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #DEL-WRQST-CNTR
        }                                                                                                                                                                 //Natural: END-IF
        //* *************
        //*  DELETE-WORK-REQUEST
    }
    private void sub_Get_Parent_Rec() throws Exception                                                                                                                    //Natural: GET-PARENT-REC
    {
        if (BLNatReinput.isReinput()) return;

        vw_cwf_Master_View_Select.startDatabaseRead                                                                                                                       //Natural: READ ( 1 ) CWF-MASTER-VIEW-SELECT BY ACTV-UNQUE-KEY = #ACTV-UNQUE-KEY
        (
        "R5",
        new Wc[] { new Wc("ACTV_UNQUE_KEY", ">=", pnd_Actv_Unque_Key, WcType.BY) },
        new Oc[] { new Oc("ACTV_UNQUE_KEY", "ASC") },
        1
        );
        R5:
        while (condition(vw_cwf_Master_View_Select.readNextRow("R5")))
        {
            ldaCwfl4819.getWork_Request_Prnt_Work_Prcss_Id().setValue(cwf_Master_View_Select_Rqst_Id_Wpid);                                                               //Natural: MOVE CWF-MASTER-VIEW-SELECT.RQST-ID-WPID TO WORK-REQUEST.PRNT-WORK-PRCSS-ID
            //*  R5.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* *************
        //*  GET-PARENT-REC
    }
    private void sub_Get_Dod_Not_Info() throws Exception                                                                                                                  //Natural: GET-DOD-NOT-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        pnd_Ph_Key.reset();                                                                                                                                               //Natural: RESET #PH-KEY
        pnd_Ph_Key_Rcrd_Type.setValue("P");                                                                                                                               //Natural: ASSIGN #PH-KEY.RCRD-TYPE := 'P'
        //*  #PH-KEY.SRVVR-MIT-IDNTFR := CWF-MASTER.PIN-NBR           /* PIN-EXP
        //*  PIN-EXP
        pnd_Ph_Key_Srvvr_Mit_Idntfr.moveAll(ldaCwfl4817.getCwf_Master_Pin_Nbr());                                                                                         //Natural: MOVE ALL CWF-MASTER.PIN-NBR TO #PH-KEY.SRVVR-MIT-IDNTFR
        vw_cwf_Sb_Ph.startDatabaseFind                                                                                                                                    //Natural: FIND CWF-SB-PH WITH PH-KEY = #PH-KEY
        (
        "FIND01",
        new Wc[] { new Wc("PH_KEY", "=", pnd_Ph_Key, WcType.WITH) }
        );
        FIND01:
        while (condition(vw_cwf_Sb_Ph.readNextRow("FIND01", true)))
        {
            vw_cwf_Sb_Ph.setIfNotFoundControlFlag(false);
            if (condition(vw_cwf_Sb_Ph.getAstCOUNTER().equals(0)))                                                                                                        //Natural: IF NO RECORD FOUND
            {
                //*      WRITE /'SB-Record Not found..PIN= ' CWF-MASTER.PIN-NBR
                ldaCwfl4819.getWork_Request_Dod_Not_Dte().reset();                                                                                                        //Natural: RESET WORK-REQUEST.DOD-NOT-DTE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            //*      WRITE /'SB-Record Found..DOD= ' CWF-SB-PH.DOD-NOT-DTE
            pnd_Dod_D.setValueEdited(new ReportEditMask("YYYYMMDD"),cwf_Sb_Ph_Dod_Not_Dte_A);                                                                             //Natural: MOVE EDITED CWF-SB-PH.DOD-NOT-DTE-A TO #DOD-D ( EM = YYYYMMDD )
            ldaCwfl4819.getWork_Request_Dod_Not_Dte().setValueEdited(pnd_Dod_D,new ReportEditMask("MM/DD/YYYY"));                                                         //Natural: MOVE EDITED #DOD-D ( EM = MM/DD/YYYY ) TO WORK-REQUEST.DOD-NOT-DTE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //* *************
        //*  GET-DOD-NOT-INFO
    }
    private void sub_Update_Control() throws Exception                                                                                                                    //Natural: UPDATE-CONTROL
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        //*  ---------------------- POPULATE TOTAL RECORD ----------------
        ldaCwfl4824.getTotal().reset();                                                                                                                                   //Natural: RESET TOTAL
        ldaCwfl4824.getTotal_Program_Name().setValue("CWFB4815");                                                                                                         //Natural: ASSIGN TOTAL.PROGRAM-NAME := 'CWFB4815'
        ldaCwfl4824.getTotal_Start_Of_Window().setValueEdited(ldaCwfl4815.getCwf_Support_Tbl_Starting_Date(),new ReportEditMask("'''LLL' 'DD' 'YYYY' 'HH':'II':'SSAP'''")); //Natural: MOVE EDITED CWF-SUPPORT-TBL.STARTING-DATE ( EM = '"'LLL' 'DD' 'YYYY' 'HH':'II':'SSAP'"' ) TO TOTAL.START-OF-WINDOW
        ldaCwfl4824.getTotal_End_Of_Window().setValueEdited(ldaCwfl4815.getCwf_Support_Tbl_Ending_Date(),new ReportEditMask("'''LLL' 'DD' 'YYYY' 'HH':'II':'SSAP'''"));   //Natural: MOVE EDITED CWF-SUPPORT-TBL.ENDING-DATE ( EM = '"'LLL' 'DD' 'YYYY' 'HH':'II':'SSAP'"' ) TO TOTAL.END-OF-WINDOW
        ldaCwfl4824.getTotal_Run_Date().setValueEdited(Global.getDATX(),new ReportEditMask("MM/DD/YYYY"));                                                                //Natural: MOVE EDITED *DATX ( EM = MM/DD/YYYY ) TO TOTAL.RUN-DATE
        ldaCwfl4824.getTotal_Work_Read_Cntr().setValue(pnd_Work_Read_Cntr);                                                                                               //Natural: ASSIGN TOTAL.WORK-READ-CNTR := #WORK-READ-CNTR
        ldaCwfl4824.getTotal_Wrqst_Add_Cntr().setValue(pnd_Wrqst_Add_Cntr);                                                                                               //Natural: ASSIGN TOTAL.WRQST-ADD-CNTR := #WRQST-ADD-CNTR
        ldaCwfl4824.getTotal_Wrqst_Upd_Cntr().setValue(pnd_Wrqst_Upd_Cntr);                                                                                               //Natural: ASSIGN TOTAL.WRQST-UPD-CNTR := #WRQST-UPD-CNTR
        ldaCwfl4824.getTotal_Unita_Add_Cntr().setValue(pnd_Unita_Add_Cntr);                                                                                               //Natural: ASSIGN TOTAL.UNITA-ADD-CNTR := #UNITA-ADD-CNTR
        ldaCwfl4824.getTotal_Unita_Upd_Cntr().setValue(pnd_Unita_Upd_Cntr);                                                                                               //Natural: ASSIGN TOTAL.UNITA-UPD-CNTR := #UNITA-UPD-CNTR
        ldaCwfl4824.getTotal_Empla_Add_Cntr().setValue(pnd_Empla_Add_Cntr);                                                                                               //Natural: ASSIGN TOTAL.EMPLA-ADD-CNTR := #EMPLA-ADD-CNTR
        ldaCwfl4824.getTotal_Empla_Upd_Cntr().setValue(pnd_Empla_Upd_Cntr);                                                                                               //Natural: ASSIGN TOTAL.EMPLA-UPD-CNTR := #EMPLA-UPD-CNTR
        ldaCwfl4824.getTotal_Stepa_Add_Cntr().setValue(pnd_Stepa_Add_Cntr);                                                                                               //Natural: ASSIGN TOTAL.STEPA-ADD-CNTR := #STEPA-ADD-CNTR
        ldaCwfl4824.getTotal_Stepa_Upd_Cntr().setValue(pnd_Stepa_Upd_Cntr);                                                                                               //Natural: ASSIGN TOTAL.STEPA-UPD-CNTR := #STEPA-UPD-CNTR
        ldaCwfl4824.getTotal_Extra_Add_Cntr().setValue(pnd_Extra_Add_Cntr);                                                                                               //Natural: ASSIGN TOTAL.EXTRA-ADD-CNTR := #EXTRA-ADD-CNTR
        ldaCwfl4824.getTotal_Extra_Upd_Cntr().setValue(pnd_Extra_Upd_Cntr);                                                                                               //Natural: ASSIGN TOTAL.EXTRA-UPD-CNTR := #EXTRA-UPD-CNTR
        ldaCwfl4824.getTotal_Intra_Add_Cntr().setValue(pnd_Intra_Add_Cntr);                                                                                               //Natural: ASSIGN TOTAL.INTRA-ADD-CNTR := #INTRA-ADD-CNTR
        ldaCwfl4824.getTotal_Intra_Upd_Cntr().setValue(pnd_Intra_Upd_Cntr);                                                                                               //Natural: ASSIGN TOTAL.INTRA-UPD-CNTR := #INTRA-UPD-CNTR
        ldaCwfl4824.getTotal_Enrta_Add_Cntr().setValue(pnd_Enrta_Add_Cntr);                                                                                               //Natural: ASSIGN TOTAL.ENRTA-ADD-CNTR := #ENRTA-ADD-CNTR
        ldaCwfl4824.getTotal_Enrta_Upd_Cntr().setValue(pnd_Enrta_Upd_Cntr);                                                                                               //Natural: ASSIGN TOTAL.ENRTA-UPD-CNTR := #ENRTA-UPD-CNTR
        ldaCwfl4824.getTotal_Contr_Add_Cntr().setValue(pnd_Cntrt_Add_Cntr);                                                                                               //Natural: ASSIGN TOTAL.CONTR-ADD-CNTR := #CNTRT-ADD-CNTR
        ldaCwfl4824.getTotal_Contr_Upd_Cntr().setValue(pnd_Cntrt_Upd_Cntr);                                                                                               //Natural: ASSIGN TOTAL.CONTR-UPD-CNTR := #CNTRT-UPD-CNTR
        ldaCwfl4824.getTotal_Cstat_Add_Cntr().setValue(pnd_Cstat_Add_Cntr);                                                                                               //Natural: ASSIGN TOTAL.CSTAT-ADD-CNTR := #CSTAT-ADD-CNTR
        ldaCwfl4824.getTotal_Merg_Delt_Cntr().setValue(pnd_Del_Wrqst_Cntr);                                                                                               //Natural: ASSIGN TOTAL.MERG-DELT-CNTR := #DEL-WRQST-CNTR
        ldaCwfl4824.getTotal_Adtnl_Upd_Cntr().setValue(pnd_Adtnl_Wrk_Upd);                                                                                                //Natural: ASSIGN TOTAL.ADTNL-UPD-CNTR := #ADTNL-WRK-UPD
        ldaCwfl4824.getTotal_Adtnl_Add_Cntr().setValue(pnd_Adtnl_Wrk_Add);                                                                                                //Natural: ASSIGN TOTAL.ADTNL-ADD-CNTR := #ADTNL-WRK-ADD
        ldaCwfl4824.getTotal_T01().setValue(pnd_Atsign);                                                                                                                  //Natural: MOVE #ATSIGN TO T01 T02 T03 T04 T05 T06 T07 T08 T09 T10 T11 T12 T13 T14 T15 T16 T17 T18 T19 T20 T21 T22 T23 T24 T25 T26 T27
        ldaCwfl4824.getTotal_T02().setValue(pnd_Atsign);
        ldaCwfl4824.getTotal_T03().setValue(pnd_Atsign);
        ldaCwfl4824.getTotal_T04().setValue(pnd_Atsign);
        ldaCwfl4824.getTotal_T05().setValue(pnd_Atsign);
        ldaCwfl4824.getTotal_T06().setValue(pnd_Atsign);
        ldaCwfl4824.getTotal_T07().setValue(pnd_Atsign);
        ldaCwfl4824.getTotal_T08().setValue(pnd_Atsign);
        ldaCwfl4824.getTotal_T09().setValue(pnd_Atsign);
        ldaCwfl4824.getTotal_T10().setValue(pnd_Atsign);
        ldaCwfl4824.getTotal_T11().setValue(pnd_Atsign);
        ldaCwfl4824.getTotal_T12().setValue(pnd_Atsign);
        ldaCwfl4824.getTotal_T13().setValue(pnd_Atsign);
        ldaCwfl4824.getTotal_T14().setValue(pnd_Atsign);
        ldaCwfl4824.getTotal_T15().setValue(pnd_Atsign);
        ldaCwfl4824.getTotal_T16().setValue(pnd_Atsign);
        ldaCwfl4824.getTotal_T17().setValue(pnd_Atsign);
        ldaCwfl4824.getTotal_T18().setValue(pnd_Atsign);
        ldaCwfl4824.getTotal_T19().setValue(pnd_Atsign);
        ldaCwfl4824.getTotal_T20().setValue(pnd_Atsign);
        ldaCwfl4824.getTotal_T21().setValue(pnd_Atsign);
        ldaCwfl4824.getTotal_T22().setValue(pnd_Atsign);
        ldaCwfl4824.getTotal_T23().setValue(pnd_Atsign);
        ldaCwfl4824.getTotal_T24().setValue(pnd_Atsign);
        ldaCwfl4824.getTotal_T25().setValue(pnd_Atsign);
        ldaCwfl4824.getTotal_T26().setValue(pnd_Atsign);
        ldaCwfl4824.getTotal_T27().setValue(pnd_Atsign);
        getWorkFiles().write(20, false, ldaCwfl4824.getTotal());                                                                                                          //Natural: WRITE WORK FILE 20 TOTAL
        pnd_Total_Rec_Cntr.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #TOTAL-REC-CNTR
        //*  PULL CONTROL RECORD TO APPLY STATISTIC COUNTERS
        //*  -----------------------------------------------
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,"CWFB4815 - CONT ISN 2-TIME",pdaCwfa4815.getCwfa4815_Output_Run_Control_Isn());                                //Natural: WRITE /'CWFB4815 - CONT ISN 2-TIME' CWFA4815-OUTPUT.RUN-CONTROL-ISN
        if (Global.isEscape()) return;
        GET_CONTROL_REC:                                                                                                                                                  //Natural: GET CWF-SUPPORT-TBL CWFA4815-OUTPUT.RUN-CONTROL-ISN
        ldaCwfl4815.getVw_cwf_Support_Tbl().readByID(pdaCwfa4815.getCwfa4815_Output_Run_Control_Isn().getLong(), "GET_CONTROL_REC");
        ldaCwfl4815.getCwf_Support_Tbl_Program_Name().setValue("CWFB4815");                                                                                               //Natural: ASSIGN CWF-SUPPORT-TBL.PROGRAM-NAME := 'CWFB4815'
        ldaCwfl4815.getCwf_Support_Tbl_Run_Status().setValue("A");                                                                                                        //Natural: ASSIGN CWF-SUPPORT-TBL.RUN-STATUS := 'A'
        ldaCwfl4815.getCwf_Support_Tbl_Number_Of_Recs_Processed().setValue(pnd_Work_Read_Cntr);                                                                           //Natural: ASSIGN CWF-SUPPORT-TBL.NUMBER-OF-RECS-PROCESSED := #WORK-READ-CNTR
        ldaCwfl4815.getCwf_Support_Tbl_Number_Of_Wr_Added().setValue(pnd_Wrqst_Add_Cntr);                                                                                 //Natural: ASSIGN CWF-SUPPORT-TBL.NUMBER-OF-WR-ADDED := #WRQST-ADD-CNTR
        ldaCwfl4815.getCwf_Support_Tbl_Number_Of_Wr_Updtd().setValue(pnd_Wrqst_Upd_Cntr);                                                                                 //Natural: ASSIGN CWF-SUPPORT-TBL.NUMBER-OF-WR-UPDTD := #WRQST-UPD-CNTR
        ldaCwfl4815.getCwf_Support_Tbl_Number_Of_Unit_Act_Added().setValue(pnd_Unita_Add_Cntr);                                                                           //Natural: ASSIGN CWF-SUPPORT-TBL.NUMBER-OF-UNIT-ACT-ADDED := #UNITA-ADD-CNTR
        ldaCwfl4815.getCwf_Support_Tbl_Number_Of_Unit_Act_Updtd().setValue(pnd_Unita_Upd_Cntr);                                                                           //Natural: ASSIGN CWF-SUPPORT-TBL.NUMBER-OF-UNIT-ACT-UPDTD := #UNITA-UPD-CNTR
        ldaCwfl4815.getCwf_Support_Tbl_Number_Of_Empl_Act_Added().setValue(pnd_Empla_Add_Cntr);                                                                           //Natural: ASSIGN CWF-SUPPORT-TBL.NUMBER-OF-EMPL-ACT-ADDED := #EMPLA-ADD-CNTR
        ldaCwfl4815.getCwf_Support_Tbl_Number_Of_Empl_Act_Updtd().setValue(pnd_Empla_Upd_Cntr);                                                                           //Natural: ASSIGN CWF-SUPPORT-TBL.NUMBER-OF-EMPL-ACT-UPDTD := #EMPLA-UPD-CNTR
        ldaCwfl4815.getCwf_Support_Tbl_Number_Of_Step_Act_Added().setValue(pnd_Stepa_Add_Cntr);                                                                           //Natural: ASSIGN CWF-SUPPORT-TBL.NUMBER-OF-STEP-ACT-ADDED := #STEPA-ADD-CNTR
        ldaCwfl4815.getCwf_Support_Tbl_Number_Of_Step_Act_Updtd().setValue(pnd_Stepa_Upd_Cntr);                                                                           //Natural: ASSIGN CWF-SUPPORT-TBL.NUMBER-OF-STEP-ACT-UPDTD := #STEPA-UPD-CNTR
        ldaCwfl4815.getCwf_Support_Tbl_Number_Of_Extr_Act_Added().setValue(pnd_Extra_Add_Cntr);                                                                           //Natural: ASSIGN CWF-SUPPORT-TBL.NUMBER-OF-EXTR-ACT-ADDED := #EXTRA-ADD-CNTR
        ldaCwfl4815.getCwf_Support_Tbl_Number_Of_Extr_Act_Updtd().setValue(pnd_Extra_Upd_Cntr);                                                                           //Natural: ASSIGN CWF-SUPPORT-TBL.NUMBER-OF-EXTR-ACT-UPDTD := #EXTRA-UPD-CNTR
        ldaCwfl4815.getCwf_Support_Tbl_Number_Of_Intr_Act_Added().setValue(pnd_Intra_Add_Cntr);                                                                           //Natural: ASSIGN CWF-SUPPORT-TBL.NUMBER-OF-INTR-ACT-ADDED := #INTRA-ADD-CNTR
        ldaCwfl4815.getCwf_Support_Tbl_Number_Of_Intr_Act_Updtd().setValue(pnd_Intra_Upd_Cntr);                                                                           //Natural: ASSIGN CWF-SUPPORT-TBL.NUMBER-OF-INTR-ACT-UPDTD := #INTRA-UPD-CNTR
        ldaCwfl4815.getCwf_Support_Tbl_Number_Of_Enrt_Act_Added().setValue(pnd_Enrta_Add_Cntr);                                                                           //Natural: ASSIGN CWF-SUPPORT-TBL.NUMBER-OF-ENRT-ACT-ADDED := #ENRTA-ADD-CNTR
        ldaCwfl4815.getCwf_Support_Tbl_Number_Of_Enrt_Act_Updtd().setValue(pnd_Enrta_Upd_Cntr);                                                                           //Natural: ASSIGN CWF-SUPPORT-TBL.NUMBER-OF-ENRT-ACT-UPDTD := #ENRTA-UPD-CNTR
        ldaCwfl4815.getCwf_Support_Tbl_Number_Of_Wr_Deleted().setValue(pnd_Del_Wrqst_Cntr);                                                                               //Natural: ASSIGN CWF-SUPPORT-TBL.NUMBER-OF-WR-DELETED := #DEL-WRQST-CNTR
        ldaCwfl4815.getCwf_Support_Tbl_Run_Date().setValue(Global.getTIMX());                                                                                             //Natural: ASSIGN CWF-SUPPORT-TBL.RUN-DATE := *TIMX
        ldaCwfl4815.getCwf_Support_Tbl_Rerun_Flag().setValue(" ");                                                                                                        //Natural: ASSIGN CWF-SUPPORT-TBL.RERUN-FLAG := ' '
        ldaCwfl4815.getVw_cwf_Support_Tbl().updateDBRow("GET_CONTROL_REC");                                                                                               //Natural: UPDATE ( GET-CONTROL-REC. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  (4080)
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,"RQST-LOG-DTE-TME",ldaCwfl4817.getCwf_Master_Rqst_Log_Dte_Tme(),NEWLINE,"PIN Number      ",                    //Natural: WRITE / 'RQST-LOG-DTE-TME' CWF-MASTER.RQST-LOG-DTE-TME / 'PIN Number      ' CWF-MASTER.PIN-NBR / 'ISN Number      ' #ISN / 'NATURAL ERROR' *ERROR-NR 'IN' *PROGRAM '....LINE' *ERROR-LINE / ' ' / 'SUP ISN ' CWFA4815-OUTPUT.RUN-CONTROL-ISN
            ldaCwfl4817.getCwf_Master_Pin_Nbr(),NEWLINE,"ISN Number      ",pnd_Isn,NEWLINE,"NATURAL ERROR",Global.getERROR_NR(),"IN",Global.getPROGRAM(),
            "....LINE",Global.getERROR_LINE(),NEWLINE," ",NEWLINE,"SUP ISN ",pdaCwfa4815.getCwfa4815_Output_Run_Control_Isn());
        DbsUtil.terminate(10);  if (true) return;                                                                                                                         //Natural: TERMINATE 10
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=56 LS=131");
        Global.format(1, "PS=56 LS=131");
    }
}
