/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:29:37 PM
**        * FROM NATURAL PROGRAM : Cwfb3416
************************************************************
**        * FILE NAME            : Cwfb3416.java
**        * CLASS NAME           : Cwfb3416
**        * INSTANCE NAME        : Cwfb3416
************************************************************
**SAG GENERATOR: BATCH-TIAA                       VERSION: 3.2.2
**SAG TITLE: EXTERNAL AFFAIRS REPORT
**SAG SYSTEM: CRPCWF
**SAG GDA: CWFG000
**SAG REPORT-HEADING(1): CORPORATE WORKFLOW FACILITIES
**SAG PRINT-FILE(1): 1
**SAG REPORT-HEADING(2): ACTUARIAL SUMMARY OF DUE DATES IN UNIT
**SAG PRINT-FILE(2): 1
**SAG REPORT-HEADING(3):
**SAG PRINT-FILE(3): 1
**SAG REPORT-HEADING(4):
**SAG PRINT-FILE(4): 1
**SAG HEADING-LINE: 12340000
**SAG DESCS(1): THIS PROGRAM PRINTS THE ACTUARIAL SUMMARY OF
**SAG DESCS(2): DUE DATES IN THE UNIT
**SAG DESCS(3): THIS REPORT IS ONLY FOR ACTUARIAL
**SAG DESCS(4): (ALL UNIT CODE W/ ACT PREFIX)
**SAG PRIMARY-FILE: CWF-MASTER-INDEX-VIEW
**SAG PRIMARY-KEY: UNIT-WPID-KEY
************************************************************************
* PROGRAM  : CWFB3415
* SYSTEM   : CRPCWF
* TITLE    : ACTUARIAL REPORT
* GENERATED: DEC 27,94 AT 08:13 AM
* FUNCTION : THIS PROGRAM PRINTS ACTUARIAL REPORT
*
* HISTORY
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3416 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Program;

    private DataAccessProgramView vw_cwf_Master_Index_View;
    private DbsField cwf_Master_Index_View_Rqst_Log_Dte_Tme;
    private DbsField cwf_Master_Index_View_Orgnl_Log_Dte_Tme;
    private DbsField cwf_Master_Index_View_Work_Prcss_Id;
    private DbsField cwf_Master_Index_View_Empl_Oprtr_Cde;

    private DbsGroup cwf_Master_Index_View__R_Field_1;
    private DbsField cwf_Master_Index_View_Empl_Racf_Id;
    private DbsField cwf_Master_Index_View_Admin_Unit_Cde;
    private DbsField cwf_Master_Index_View_Tiaa_Rcvd_Dte;
    private DbsField cwf_Master_Index_View_Due_Dte_Chg_Prty_Cde;

    private DbsGroup cwf_Master_Index_View__R_Field_2;
    private DbsField cwf_Master_Index_View_Due_Date;
    private DbsField cwf_Master_Index_View_Due_Dte_Chg_Ind;
    private DbsField cwf_Master_Index_View_Due_Dte_Prty_Cde;

    private DataAccessProgramView vw_hist_View;
    private DbsField hist_View_Rqst_Log_Dte_Tme;
    private DbsField hist_View_Work_Prcss_Id;
    private DbsField hist_View_Due_Dte_Chg_Prty_Cde;

    private DbsGroup hist_View__R_Field_3;
    private DbsField hist_View_Due_Date;
    private DbsField hist_View_Due_Dte_Chg_Ind;
    private DbsField hist_View_Due_Dte_Prty_Cde;
    private DbsField pnd_Header1_1;
    private DbsField pnd_Header1_2;
    private DbsField pnd_Header1_3;

    private DbsGroup pnd_Parm;
    private DbsField pnd_Parm_Old_Rt_Cde;
    private DbsField pnd_Parm_Work_Prcss_Long_Nme;
    private DbsField pnd_Parm_Work_Prcss_Short_Nme;
    private DbsField pnd_Unit_Cde;
    private DbsField pnd_Unit_Name;
    private DbsField pnd_Wpid;
    private DbsField pnd_Date_Now;
    private DbsField pnd_Work_Due_Date;
    private DbsField pnd_Actuarial_Due_Date;
    private DbsField pnd_Bad_Count;
    private DbsField pnd_Due_Today;
    private DbsField pnd_Due_Tommorow;
    private DbsField pnd_Due_In_2_Days;
    private DbsField pnd_Due_More_Than_2_Days;
    private DbsField pnd_Al;
    private DbsField pnd_Dt;
    private DbsField pnd_Dm;
    private DbsField pnd_D2;
    private DbsField pnd_D9;
    private DbsField pnd_Due_Days;

    private DbsRecord internalLoopRecord;
    private DbsField rEAD_PRIMEAdmin_Unit_CdeOld;
    private DbsField rEAD_PRIMEWork_Prcss_IdOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);

        vw_cwf_Master_Index_View = new DataAccessProgramView(new NameInfo("vw_cwf_Master_Index_View", "CWF-MASTER-INDEX-VIEW"), "CWF_MASTER_INDEX_VIEW", 
            "CWF_MASTER_INDEX");
        cwf_Master_Index_View_Rqst_Log_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Master_Index_View_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        cwf_Master_Index_View_Orgnl_Log_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Orgnl_Log_Dte_Tme", "ORGNL-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "ORGNL_LOG_DTE_TME");
        cwf_Master_Index_View_Orgnl_Log_Dte_Tme.setDdmHeader("ORIGINAL/LOG DTE/TME");
        cwf_Master_Index_View_Work_Prcss_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Work_Prcss_Id", "WORK-PRCSS-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        cwf_Master_Index_View_Work_Prcss_Id.setDdmHeader("WORK/ID");
        cwf_Master_Index_View_Empl_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "EMPL_OPRTR_CDE");
        cwf_Master_Index_View_Empl_Oprtr_Cde.setDdmHeader("EMPLOYEE/ID");

        cwf_Master_Index_View__R_Field_1 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_1", "REDEFINE", cwf_Master_Index_View_Empl_Oprtr_Cde);
        cwf_Master_Index_View_Empl_Racf_Id = cwf_Master_Index_View__R_Field_1.newFieldInGroup("cwf_Master_Index_View_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 
            8);
        cwf_Master_Index_View_Admin_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Admin_Unit_Cde", "ADMIN-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ADMIN_UNIT_CDE");
        cwf_Master_Index_View_Tiaa_Rcvd_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_RCVD_DTE");
        cwf_Master_Index_View_Due_Dte_Chg_Prty_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Due_Dte_Chg_Prty_Cde", 
            "DUE-DTE-CHG-PRTY-CDE", FieldType.STRING, 16, RepeatingFieldStrategy.None, "DUE_DTE_CHG_PRTY_CDE");
        cwf_Master_Index_View_Due_Dte_Chg_Prty_Cde.setDdmHeader("DUE DATE CHANGE IND. PRIORITY");

        cwf_Master_Index_View__R_Field_2 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_2", "REDEFINE", cwf_Master_Index_View_Due_Dte_Chg_Prty_Cde);
        cwf_Master_Index_View_Due_Date = cwf_Master_Index_View__R_Field_2.newFieldInGroup("cwf_Master_Index_View_Due_Date", "DUE-DATE", FieldType.STRING, 
            8);
        cwf_Master_Index_View_Due_Dte_Chg_Ind = cwf_Master_Index_View__R_Field_2.newFieldInGroup("cwf_Master_Index_View_Due_Dte_Chg_Ind", "DUE-DTE-CHG-IND", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Due_Dte_Prty_Cde = cwf_Master_Index_View__R_Field_2.newFieldInGroup("cwf_Master_Index_View_Due_Dte_Prty_Cde", "DUE-DTE-PRTY-CDE", 
            FieldType.STRING, 1);
        registerRecord(vw_cwf_Master_Index_View);

        vw_hist_View = new DataAccessProgramView(new NameInfo("vw_hist_View", "HIST-VIEW"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        hist_View_Rqst_Log_Dte_Tme = vw_hist_View.getRecord().newFieldInGroup("hist_View_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15, 
            RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        hist_View_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        hist_View_Work_Prcss_Id = vw_hist_View.getRecord().newFieldInGroup("hist_View_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        hist_View_Work_Prcss_Id.setDdmHeader("WORK/ID");
        hist_View_Due_Dte_Chg_Prty_Cde = vw_hist_View.getRecord().newFieldInGroup("hist_View_Due_Dte_Chg_Prty_Cde", "DUE-DTE-CHG-PRTY-CDE", FieldType.STRING, 
            16, RepeatingFieldStrategy.None, "DUE_DTE_CHG_PRTY_CDE");
        hist_View_Due_Dte_Chg_Prty_Cde.setDdmHeader("DUE DATE CHANGE IND. PRIORITY");

        hist_View__R_Field_3 = vw_hist_View.getRecord().newGroupInGroup("hist_View__R_Field_3", "REDEFINE", hist_View_Due_Dte_Chg_Prty_Cde);
        hist_View_Due_Date = hist_View__R_Field_3.newFieldInGroup("hist_View_Due_Date", "DUE-DATE", FieldType.STRING, 8);
        hist_View_Due_Dte_Chg_Ind = hist_View__R_Field_3.newFieldInGroup("hist_View_Due_Dte_Chg_Ind", "DUE-DTE-CHG-IND", FieldType.STRING, 1);
        hist_View_Due_Dte_Prty_Cde = hist_View__R_Field_3.newFieldInGroup("hist_View_Due_Dte_Prty_Cde", "DUE-DTE-PRTY-CDE", FieldType.STRING, 1);
        registerRecord(vw_hist_View);

        pnd_Header1_1 = localVariables.newFieldInRecord("pnd_Header1_1", "#HEADER1-1", FieldType.STRING, 50);
        pnd_Header1_2 = localVariables.newFieldInRecord("pnd_Header1_2", "#HEADER1-2", FieldType.STRING, 50);
        pnd_Header1_3 = localVariables.newFieldInRecord("pnd_Header1_3", "#HEADER1-3", FieldType.STRING, 50);

        pnd_Parm = localVariables.newGroupInRecord("pnd_Parm", "#PARM");
        pnd_Parm_Old_Rt_Cde = pnd_Parm.newFieldInGroup("pnd_Parm_Old_Rt_Cde", "OLD-RT-CDE", FieldType.STRING, 4);
        pnd_Parm_Work_Prcss_Long_Nme = pnd_Parm.newFieldInGroup("pnd_Parm_Work_Prcss_Long_Nme", "WORK-PRCSS-LONG-NME", FieldType.STRING, 45);
        pnd_Parm_Work_Prcss_Short_Nme = pnd_Parm.newFieldInGroup("pnd_Parm_Work_Prcss_Short_Nme", "WORK-PRCSS-SHORT-NME", FieldType.STRING, 15);
        pnd_Unit_Cde = localVariables.newFieldInRecord("pnd_Unit_Cde", "#UNIT-CDE", FieldType.STRING, 8);
        pnd_Unit_Name = localVariables.newFieldInRecord("pnd_Unit_Name", "#UNIT-NAME", FieldType.STRING, 45);
        pnd_Wpid = localVariables.newFieldInRecord("pnd_Wpid", "#WPID", FieldType.STRING, 6);
        pnd_Date_Now = localVariables.newFieldInRecord("pnd_Date_Now", "#DATE-NOW", FieldType.DATE);
        pnd_Work_Due_Date = localVariables.newFieldInRecord("pnd_Work_Due_Date", "#WORK-DUE-DATE", FieldType.DATE);
        pnd_Actuarial_Due_Date = localVariables.newFieldInRecord("pnd_Actuarial_Due_Date", "#ACTUARIAL-DUE-DATE", FieldType.DATE);
        pnd_Bad_Count = localVariables.newFieldInRecord("pnd_Bad_Count", "#BAD-COUNT", FieldType.NUMERIC, 4);
        pnd_Due_Today = localVariables.newFieldInRecord("pnd_Due_Today", "#DUE-TODAY", FieldType.NUMERIC, 4);
        pnd_Due_Tommorow = localVariables.newFieldInRecord("pnd_Due_Tommorow", "#DUE-TOMMOROW", FieldType.NUMERIC, 4);
        pnd_Due_In_2_Days = localVariables.newFieldInRecord("pnd_Due_In_2_Days", "#DUE-IN-2-DAYS", FieldType.NUMERIC, 4);
        pnd_Due_More_Than_2_Days = localVariables.newFieldInRecord("pnd_Due_More_Than_2_Days", "#DUE-MORE-THAN-2-DAYS", FieldType.NUMERIC, 4);
        pnd_Al = localVariables.newFieldInRecord("pnd_Al", "#AL", FieldType.NUMERIC, 4);
        pnd_Dt = localVariables.newFieldInRecord("pnd_Dt", "#DT", FieldType.NUMERIC, 4);
        pnd_Dm = localVariables.newFieldInRecord("pnd_Dm", "#DM", FieldType.NUMERIC, 4);
        pnd_D2 = localVariables.newFieldInRecord("pnd_D2", "#D2", FieldType.NUMERIC, 4);
        pnd_D9 = localVariables.newFieldInRecord("pnd_D9", "#D9", FieldType.NUMERIC, 4);
        pnd_Due_Days = localVariables.newFieldInRecord("pnd_Due_Days", "#DUE-DAYS", FieldType.NUMERIC, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        rEAD_PRIMEAdmin_Unit_CdeOld = internalLoopRecord.newFieldInRecord("READ_PRIME_Admin_Unit_Cde_OLD", "Admin_Unit_Cde_OLD", FieldType.STRING, 8);
        rEAD_PRIMEWork_Prcss_IdOld = internalLoopRecord.newFieldInRecord("READ_PRIME_Work_Prcss_Id_OLD", "Work_Prcss_Id_OLD", FieldType.STRING, 6);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Master_Index_View.reset();
        vw_hist_View.reset();
        internalLoopRecord.reset();

        localVariables.reset();
        pnd_Header1_1.setInitialValue("          Corporate Workflow Facilities");
        pnd_Header1_2.setInitialValue("     Actuarial Summary of Due Dates in Unit");
        pnd_Header1_3.setInitialValue("         Subrequests with 'Bad' Due Dates");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb3416() throws Exception
    {
        super("Cwfb3416");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        getReports().getPageNumberDbs(1).reset();                                                                                                                         //Natural: RESET *PAGE-NUMBER ( 1 )
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        //*  DEFINE PRINTERS AND FORMATS
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: ASSIGN #PROGRAM = *PROGRAM
        //*  MAP THE CURRENT LANGUAGE CODE TO THE CODE IN THE BATCH MODEL TEXT LDA.
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH' THEN
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        //*  PRIMARY FILE
        boolean endOfDataReadPrime = true;                                                                                                                                //Natural: READ WORK 1 CWF-MASTER-INDEX-VIEW
        boolean firstReadPrime = true;
        READ_PRIME:
        while (condition(getWorkFiles().read(1, vw_cwf_Master_Index_View)))
        {
            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventRead_Prime();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadPrime = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //* *SAG DEFINE EXIT AFTER-PRIME-READ
            if (condition(pnd_Unit_Cde.equals(" ")))                                                                                                                      //Natural: IF #UNIT-CDE = ' '
            {
                pnd_Unit_Cde.setValue(cwf_Master_Index_View_Admin_Unit_Cde);                                                                                              //Natural: MOVE CWF-MASTER-INDEX-VIEW.ADMIN-UNIT-CDE TO #UNIT-CDE
                DbsUtil.callnat(Cwfn1103.class , getCurrentProcessState(), pnd_Unit_Cde, pnd_Unit_Name);                                                                  //Natural: CALLNAT 'CWFN1103' #UNIT-CDE #UNIT-NAME
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Unit_Cde.notEquals(cwf_Master_Index_View_Admin_Unit_Cde)))                                                                              //Natural: IF #UNIT-CDE NE CWF-MASTER-INDEX-VIEW.ADMIN-UNIT-CDE
                {
                    pnd_Unit_Cde.setValue(cwf_Master_Index_View_Admin_Unit_Cde);                                                                                          //Natural: MOVE CWF-MASTER-INDEX-VIEW.ADMIN-UNIT-CDE TO #UNIT-CDE
                    DbsUtil.callnat(Cwfn1103.class , getCurrentProcessState(), pnd_Unit_Cde, pnd_Unit_Name);                                                              //Natural: CALLNAT 'CWFN1103' #UNIT-CDE #UNIT-NAME
                    if (condition(Global.isEscape())) return;
                    pnd_Bad_Count.reset();                                                                                                                                //Natural: RESET #BAD-COUNT
                    getReports().newPage(new ReportSpecification(1));                                                                                                     //Natural: NEWPAGE ( 1 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //* *SAG END-EXIT
            //* *SAG DEFINE EXIT PRIME-WRITE-FIELDS
            vw_hist_View.startDatabaseRead                                                                                                                                //Natural: READ ( 1 ) HIST-VIEW BY ACTV-UNQUE-KEY FROM CWF-MASTER-INDEX-VIEW.ORGNL-LOG-DTE-TME
            (
            "READ01",
            new Wc[] { new Wc("ACTV_UNQUE_KEY", ">=", cwf_Master_Index_View_Orgnl_Log_Dte_Tme, WcType.BY) },
            new Oc[] { new Oc("ACTV_UNQUE_KEY", "ASC") },
            1
            );
            READ01:
            while (condition(vw_hist_View.readNextRow("READ01")))
            {
                if (condition(! (DbsUtil.maskMatches(hist_View_Work_Prcss_Id,"'X'....."))))                                                                               //Natural: IF HIST-VIEW.WORK-PRCSS-ID NE MASK ( 'X'..... )
                {
                    if (condition(hist_View_Due_Date.equals(cwf_Master_Index_View_Due_Date)))                                                                             //Natural: IF HIST-VIEW.DUE-DATE = CWF-MASTER-INDEX-VIEW.DUE-DATE
                    {
                        pnd_Bad_Count.nadd(1);                                                                                                                            //Natural: ADD 1 TO #BAD-COUNT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* *SAG END-EXIT                                                                                                                                             //Natural: AT TOP OF PAGE ( 1 );//Natural: AT BREAK OF CWF-MASTER-INDEX-VIEW.WORK-PRCSS-ID;//Natural: AT BREAK OF CWF-MASTER-INDEX-VIEW.ADMIN-UNIT-CDE
            //*  PRIMARY FILE.
            rEAD_PRIMEAdmin_Unit_CdeOld.setValue(cwf_Master_Index_View_Admin_Unit_Cde);                                                                                   //Natural: END-WORK
            rEAD_PRIMEWork_Prcss_IdOld.setValue(cwf_Master_Index_View_Work_Prcss_Id);
        }
        READ_PRIME_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventRead_Prime(endOfDataReadPrime);
        }
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(41),pnd_Header1_1,new TabSetting(124),"Page",getReports().getPageNumberDbs(1),  //Natural: WRITE ( 1 ) NOTITLE *PROGRAM 41T #HEADER1-1 124T 'Page' *PAGE-NUMBER ( 1 ) ( NL = 4 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 41T #HEADER1-2 124T *TIMX ( EM = HH':'II' 'AP ) / 41T #HEADER1-3 // 'Unit :' #UNIT-CDE '-' #UNIT-NAME /
                        new NumericLength (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),new 
                        TabSetting(41),pnd_Header1_2,new TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"),NEWLINE,new TabSetting(41),
                        pnd_Header1_3,NEWLINE,NEWLINE,"Unit :",pnd_Unit_Cde,"-",pnd_Unit_Name,NEWLINE);
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventRead_Prime() throws Exception {atBreakEventRead_Prime(false);}
    private void atBreakEventRead_Prime(boolean endOfData) throws Exception
    {
        boolean cwf_Master_Index_View_Work_Prcss_IdIsBreak = cwf_Master_Index_View_Work_Prcss_Id.isBreak(endOfData);
        boolean cwf_Master_Index_View_Admin_Unit_CdeIsBreak = cwf_Master_Index_View_Admin_Unit_Cde.isBreak(endOfData);
        if (condition(cwf_Master_Index_View_Work_Prcss_IdIsBreak || cwf_Master_Index_View_Admin_Unit_CdeIsBreak))
        {
            if (condition(DbsUtil.maskMatches(rEAD_PRIMEAdmin_Unit_CdeOld,"'ACT'.....")))                                                                                 //Natural: IF OLD ( CWF-MASTER-INDEX-VIEW.ADMIN-UNIT-CDE ) = MASK ( 'ACT'..... )
            {
                pnd_Wpid.setValue(rEAD_PRIMEWork_Prcss_IdOld);                                                                                                            //Natural: MOVE OLD ( CWF-MASTER-INDEX-VIEW.WORK-PRCSS-ID ) TO #WPID
                DbsUtil.callnat(Cwfn0013.class , getCurrentProcessState(), pnd_Wpid, pnd_Parm);                                                                           //Natural: CALLNAT 'CWFN0013' #WPID #PARM
                if (condition(Global.isEscape())) return;
                if (condition(pnd_Bad_Count.greater(getZero())))                                                                                                          //Natural: IF #BAD-COUNT GT 0
                {
                    getReports().display(1, "Work/Process ID",                                                                                                            //Natural: DISPLAY ( 1 ) 'Work/Process ID' #WPID ( AL = 11 LC = ����� ) '/' #PARM.WORK-PRCSS-LONG-NME '/' #BAD-COUNT
                    		pnd_Wpid, new AlphanumericLength (11), new FieldAttributes("LC=�����"),"/",
                    		pnd_Parm_Work_Prcss_Long_Nme,"/",
                    		pnd_Bad_Count);
                    if (condition(Global.isEscape())) return;
                    pnd_Al.nadd(pnd_Bad_Count);                                                                                                                           //Natural: COMPUTE #AL = #AL + #BAD-COUNT
                    pnd_Bad_Count.reset();                                                                                                                                //Natural: RESET #BAD-COUNT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(cwf_Master_Index_View_Admin_Unit_CdeIsBreak))
        {
            if (condition(DbsUtil.maskMatches(rEAD_PRIMEAdmin_Unit_CdeOld,"'ACT'.....")))                                                                                 //Natural: IF OLD ( CWF-MASTER-INDEX-VIEW.ADMIN-UNIT-CDE ) = MASK ( 'ACT'..... )
            {
                if (condition(pnd_Al.greater(getZero())))                                                                                                                 //Natural: IF #AL GT 0
                {
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(48),"     Unit Total",new ReportTAsterisk(pnd_Bad_Count),pnd_Al);                   //Natural: WRITE ( 1 ) / 48T '     Unit Total' T*#BAD-COUNT #AL
                    if (condition(Global.isEscape())) return;
                    pnd_Al.reset();                                                                                                                                       //Natural: RESET #AL
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(1, ReportOption.NOTITLE,new ReportTAsterisk(pnd_Wpid),"NO SUBREQUEST WITH BAD DUE DATES");                                         //Natural: WRITE ( 1 ) T*#WPID 'NO SUBREQUEST WITH BAD DUE DATES'
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");

        getReports().setDisplayColumns(1, "Work/Process ID",
        		pnd_Wpid, new AlphanumericLength (11), new FieldAttributes("LC=�����"),"/",
        		pnd_Parm_Work_Prcss_Long_Nme,"/",
        		pnd_Bad_Count);
    }
}
