/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:08:00 PM
**        * FROM NATURAL PROGRAM : Efsb6110
************************************************************
**        * FILE NAME            : Efsb6110.java
**        * CLASS NAME           : Efsb6110
**        * INSTANCE NAME        : Efsb6110
************************************************************
************************************************************************
* PROGRAM  : EFSB6110
* SYSTEM   : CRPCWF
* TITLE    : IMAGE-XREF AND DOCUMENT FILES RECONCILATION (COLLECTION)
* WRITTEN  : JANUARY, 03
* FUNCTION : THIS PROGRAM READ FILES USING DATE CONTROL RECORD
*          : COMPARES DOCUMENT & THE IMAGE CROSS REFERENCE FILES
*          : AND PRODUCES A WORK FILE TO BE USED BY
*          : EFSB6120 TO PRODUCE THE FOLLOWING REPORTS
*          : EFSN6130 TO CREATE MISSING DOCUMENTS
*          :  1. MAIL ITEMS ON XREF BUT NOT ON DOCUMENT
*          :  2. MAIL ITEMS ON DOCUMENTS BUT NOT ON XREF
*          : 06/10/03 THIS PROGRAM WILL PRODUCE STATISTIC REPORT (L.E.)
* HISTORY  :
* 02/23/2017 - SARKAB - PIN EXPANSION - AUG 2017
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsb6110 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Image_Xref;
    private DbsField cwf_Image_Xref_Mail_Item_Nbr;

    private DbsGroup cwf_Image_Xref__R_Field_1;
    private DbsField cwf_Image_Xref_Mail_Item_Nbr_A;
    private DbsField cwf_Image_Xref_Ph_Unique_Id_Nbr;
    private DbsField cwf_Image_Xref_Upld_Date_Time;
    private DbsField cwf_Image_Xref_Source_Id;
    private DbsField cwf_Image_Xref_Batch_Id;
    private DbsField cwf_Image_Xref_Cabinet_Id;

    private DataAccessProgramView vw_cwf_Efm_Document;
    private DbsField cwf_Efm_Document_Entry_Dte_Tme;
    private DbsField cwf_Efm_Document_Image_Source_Id;
    private DbsField cwf_Efm_Document_Image_Min;

    private DbsGroup cwf_Efm_Document__R_Field_2;
    private DbsField cwf_Efm_Document_Image_Min_A;

    private DbsGroup cwf_Efm_Document__R_Field_3;
    private DbsField cwf_Efm_Document_Image_Min_N;

    private DataAccessProgramView vw_icw_Efm_Document;
    private DbsField icw_Efm_Document_Crte_Dte_Tme;
    private DbsField icw_Efm_Document_Image_Source_Id;
    private DbsField icw_Efm_Document_Image_Min;

    private DbsGroup icw_Efm_Document__R_Field_4;
    private DbsField icw_Efm_Document_Image_Min_A;

    private DbsGroup icw_Efm_Document__R_Field_5;
    private DbsField icw_Efm_Document_Image_Min_N;

    private DataAccessProgramView vw_ncw_Efm_Document;
    private DbsField ncw_Efm_Document_Crte_Dte_Tme;
    private DbsField ncw_Efm_Document_Image_Source_Id;
    private DbsField ncw_Efm_Document_Image_Min;

    private DbsGroup ncw_Efm_Document__R_Field_6;
    private DbsField ncw_Efm_Document_Image_Min_A;

    private DbsGroup ncw_Efm_Document__R_Field_7;
    private DbsField ncw_Efm_Document_Image_Min_N;
    private DbsField pnd_Work_Record;

    private DbsGroup pnd_Work_Record__R_Field_8;
    private DbsField pnd_Work_Record_Pnd_Wf_Source_Id;
    private DbsField pnd_Work_Record_Pnd_Wf_Mail_Item_No;
    private DbsField pnd_Work_Record_Pnd_Wf_Record_Type;
    private DbsField pnd_Report_Line;

    private DbsGroup pnd_Report_Line__R_Field_9;
    private DbsField pnd_Report_Line_Pnd_Rl_Source_Id;
    private DbsField pnd_Report_Line_Filler_01;
    private DbsField pnd_Report_Line_Pnd_Rl_Mail_Item_No;
    private DbsField pnd_Report_Line_Filler_02;
    private DbsField pnd_Report_Line_Pnd_Rl_Record_Type;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_10;
    private DbsField cwf_Support_Tbl_Run_Indicator;
    private DbsField cwf_Support_Tbl_Filler_01;
    private DbsField cwf_Support_Tbl_Normal_Start_Dte_Tme;
    private DbsField cwf_Support_Tbl_Filler_02;
    private DbsField cwf_Support_Tbl_Normal_End_Dte_Tme;
    private DbsField cwf_Support_Tbl_Filler_03;
    private DbsField cwf_Support_Tbl_Normal_Record_Count;
    private DbsField cwf_Support_Tbl_Filler_04;
    private DbsField cwf_Support_Tbl_Et_On_Off_Flag;
    private DbsField cwf_Support_Tbl_Filler_05;
    private DbsField cwf_Support_Tbl_Special_Start_Dte_Tme;
    private DbsField cwf_Support_Tbl_Filler_06;
    private DbsField cwf_Support_Tbl_Special_End_Dte_Tme;
    private DbsField cwf_Support_Tbl_Filler_07;
    private DbsField cwf_Support_Tbl_Special_Record_Count;
    private DbsField cwf_Support_Tbl_Filler_08;
    private DbsField cwf_Support_Tbl_Calc_Start_Dte_Tme;
    private DbsField cwf_Support_Tbl_Filler_09;
    private DbsField cwf_Support_Tbl_Calc_End_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Actve_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Entry_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Dte;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Dlte_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Table_Rectype;
    private DbsField cwf_Support_Tbl_Tbl_Table_Access_Level;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_11;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind;
    private DbsField pnd_Common_Image_Pointer_Key;

    private DbsGroup pnd_Common_Image_Pointer_Key__R_Field_12;
    private DbsField pnd_Common_Image_Pointer_Key_Pnd_Common_Image_Source_Id;
    private DbsField pnd_Common_Image_Pointer_Key_Pnd_Common_Image_Min;
    private DbsField pnd_Source_Id_Min_Key;

    private DbsGroup pnd_Source_Id_Min_Key__R_Field_13;
    private DbsField pnd_Source_Id_Min_Key_Pnd_Source_Id;
    private DbsField pnd_Source_Id_Min_Key_Pnd_Min_Nbr;
    private DbsField pnd_Wf_Rec_Cntr;
    private DbsField pnd_Xref_Cntr;
    private DbsField pnd_Xref_Cntr_Accpt;
    private DbsField pnd_Xref_Orphan;
    private DbsField pnd_Cdoc_Cntr;
    private DbsField pnd_Cdoc_Cntr_Accpt;
    private DbsField pnd_Cdoc_Orphan;
    private DbsField pnd_Idoc_Cntr;
    private DbsField pnd_Idoc_Cntr_Accpt;
    private DbsField pnd_Idoc_Orphan;
    private DbsField pnd_Ndoc_Cntr;
    private DbsField pnd_Ndoc_Cntr_Accpt;
    private DbsField pnd_Ndoc_Orphan;
    private DbsField pnd_Doc_Fnd;
    private DbsField pnd_Control_Flag;
    private DbsField pnd_Start_T;
    private DbsField pnd_End_T;
    private DbsField pnd_Isn_Control;
    private DbsField pnd_Running_Dte_Tme_Str;

    private DbsGroup pnd_Running_Dte_Tme_Str__R_Field_14;
    private DbsField pnd_Running_Dte_Tme_Str_Pnd_Running_Dte_Tme_Str_A;
    private DbsField pnd_Running_Dte_Tme_End;

    private DbsGroup pnd_Running_Dte_Tme_End__R_Field_15;
    private DbsField pnd_Running_Dte_Tme_End_Pnd_Running_Dte_Tme_End_A;

    private DbsGroup pnd_Running_Dte_Tme_End__R_Field_16;
    private DbsField pnd_Running_Dte_Tme_End_Pnd_Running_Date_End;
    private DbsField pnd_Running_Dte_Tme_End_Pnd_Running_Time_End;
    private DbsField pnd_Date_Time_A;

    private DbsGroup pnd_Date_Time_A__R_Field_17;
    private DbsField pnd_Date_Time_A_Pnd_Date_Time_N;
    private DbsField pnd_Date_T;
    private DbsField pnd_Date_T_Result;
    private DbsField pnd_Curr_Date_N;

    private DbsGroup pnd_Curr_Date_N__R_Field_18;
    private DbsField pnd_Curr_Date_N_Pnd_Curr_Date_A;
    private DbsField pnd_Date_D;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Image_Xref = new DataAccessProgramView(new NameInfo("vw_cwf_Image_Xref", "CWF-IMAGE-XREF"), "CWF_IMAGE_XREF", "CWF_IMAGE_XREF");
        cwf_Image_Xref_Mail_Item_Nbr = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Mail_Item_Nbr", "MAIL-ITEM-NBR", FieldType.NUMERIC, 
            11, RepeatingFieldStrategy.None, "MAIL_ITEM_NBR");
        cwf_Image_Xref_Mail_Item_Nbr.setDdmHeader("MAIL ITEM/NUMBER");

        cwf_Image_Xref__R_Field_1 = vw_cwf_Image_Xref.getRecord().newGroupInGroup("cwf_Image_Xref__R_Field_1", "REDEFINE", cwf_Image_Xref_Mail_Item_Nbr);
        cwf_Image_Xref_Mail_Item_Nbr_A = cwf_Image_Xref__R_Field_1.newFieldInGroup("cwf_Image_Xref_Mail_Item_Nbr_A", "MAIL-ITEM-NBR-A", FieldType.STRING, 
            11);
        cwf_Image_Xref_Ph_Unique_Id_Nbr = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Ph_Unique_Id_Nbr", "PH-UNIQUE-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "PH_UNIQUE_ID_NBR");
        cwf_Image_Xref_Ph_Unique_Id_Nbr.setDdmHeader("PIN");
        cwf_Image_Xref_Upld_Date_Time = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Upld_Date_Time", "UPLD-DATE-TIME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "UPLD_DATE_TIME");
        cwf_Image_Xref_Upld_Date_Time.setDdmHeader("UPLOAD/DATE TIME");
        cwf_Image_Xref_Source_Id = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Source_Id", "SOURCE-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "SOURCE_ID");
        cwf_Image_Xref_Batch_Id = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Batch_Id", "BATCH-ID", FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, 
            "BATCH_ID");
        cwf_Image_Xref_Cabinet_Id = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Cabinet_Id", "CABINET-ID", FieldType.STRING, 12, RepeatingFieldStrategy.None, 
            "CABINET_ID");
        registerRecord(vw_cwf_Image_Xref);

        vw_cwf_Efm_Document = new DataAccessProgramView(new NameInfo("vw_cwf_Efm_Document", "CWF-EFM-DOCUMENT"), "CWF_EFM_DOCUMENT", "CWF_EFM_DOCUMENT");
        cwf_Efm_Document_Entry_Dte_Tme = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Entry_Dte_Tme", "ENTRY-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ENTRY_DTE_TME");
        cwf_Efm_Document_Entry_Dte_Tme.setDdmHeader("ENTRY/DATE AND TIME");
        cwf_Efm_Document_Image_Source_Id = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Image_Source_Id", "IMAGE-SOURCE-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "IMAGE_SOURCE_ID");
        cwf_Efm_Document_Image_Source_Id.setDdmHeader("IMAGE/SOURCE ID");
        cwf_Efm_Document_Image_Min = vw_cwf_Efm_Document.getRecord().newFieldInGroup("cwf_Efm_Document_Image_Min", "IMAGE-MIN", FieldType.STRING, 14, 
            RepeatingFieldStrategy.None, "IMAGE_MIN");
        cwf_Efm_Document_Image_Min.setDdmHeader("IMAGE/POINTER");

        cwf_Efm_Document__R_Field_2 = vw_cwf_Efm_Document.getRecord().newGroupInGroup("cwf_Efm_Document__R_Field_2", "REDEFINE", cwf_Efm_Document_Image_Min);
        cwf_Efm_Document_Image_Min_A = cwf_Efm_Document__R_Field_2.newFieldInGroup("cwf_Efm_Document_Image_Min_A", "IMAGE-MIN-A", FieldType.STRING, 11);

        cwf_Efm_Document__R_Field_3 = cwf_Efm_Document__R_Field_2.newGroupInGroup("cwf_Efm_Document__R_Field_3", "REDEFINE", cwf_Efm_Document_Image_Min_A);
        cwf_Efm_Document_Image_Min_N = cwf_Efm_Document__R_Field_3.newFieldInGroup("cwf_Efm_Document_Image_Min_N", "IMAGE-MIN-N", FieldType.NUMERIC, 11);
        registerRecord(vw_cwf_Efm_Document);

        vw_icw_Efm_Document = new DataAccessProgramView(new NameInfo("vw_icw_Efm_Document", "ICW-EFM-DOCUMENT"), "ICW_EFM_DOCUMENT", "ICW_EFM_DOCUMENT");
        icw_Efm_Document_Crte_Dte_Tme = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Crte_Dte_Tme", "CRTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "CRTE_DTE_TME");
        icw_Efm_Document_Crte_Dte_Tme.setDdmHeader("CREATE/DATE AND TIME");
        icw_Efm_Document_Image_Source_Id = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Image_Source_Id", "IMAGE-SOURCE-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "IMAGE_SOURCE_ID");
        icw_Efm_Document_Image_Source_Id.setDdmHeader("IMAGE/SOURCE ID");
        icw_Efm_Document_Image_Min = vw_icw_Efm_Document.getRecord().newFieldInGroup("icw_Efm_Document_Image_Min", "IMAGE-MIN", FieldType.STRING, 14, 
            RepeatingFieldStrategy.None, "IMAGE_MIN");
        icw_Efm_Document_Image_Min.setDdmHeader("IMAGE/POINTER");

        icw_Efm_Document__R_Field_4 = vw_icw_Efm_Document.getRecord().newGroupInGroup("icw_Efm_Document__R_Field_4", "REDEFINE", icw_Efm_Document_Image_Min);
        icw_Efm_Document_Image_Min_A = icw_Efm_Document__R_Field_4.newFieldInGroup("icw_Efm_Document_Image_Min_A", "IMAGE-MIN-A", FieldType.STRING, 11);

        icw_Efm_Document__R_Field_5 = icw_Efm_Document__R_Field_4.newGroupInGroup("icw_Efm_Document__R_Field_5", "REDEFINE", icw_Efm_Document_Image_Min_A);
        icw_Efm_Document_Image_Min_N = icw_Efm_Document__R_Field_5.newFieldInGroup("icw_Efm_Document_Image_Min_N", "IMAGE-MIN-N", FieldType.NUMERIC, 11);
        registerRecord(vw_icw_Efm_Document);

        vw_ncw_Efm_Document = new DataAccessProgramView(new NameInfo("vw_ncw_Efm_Document", "NCW-EFM-DOCUMENT"), "NCW_EFM_DOCUMENT", "NCW_EFM_DOCUMENT");
        ncw_Efm_Document_Crte_Dte_Tme = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Crte_Dte_Tme", "CRTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "CRTE_DTE_TME");
        ncw_Efm_Document_Crte_Dte_Tme.setDdmHeader("CREATE/DATE AND TIME");
        ncw_Efm_Document_Image_Source_Id = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Image_Source_Id", "IMAGE-SOURCE-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "IMAGE_SOURCE_ID");
        ncw_Efm_Document_Image_Source_Id.setDdmHeader("IMAGE/SOURCE ID");
        ncw_Efm_Document_Image_Min = vw_ncw_Efm_Document.getRecord().newFieldInGroup("ncw_Efm_Document_Image_Min", "IMAGE-MIN", FieldType.STRING, 14, 
            RepeatingFieldStrategy.None, "IMAGE_MIN");
        ncw_Efm_Document_Image_Min.setDdmHeader("IMAGE/POINTER");

        ncw_Efm_Document__R_Field_6 = vw_ncw_Efm_Document.getRecord().newGroupInGroup("ncw_Efm_Document__R_Field_6", "REDEFINE", ncw_Efm_Document_Image_Min);
        ncw_Efm_Document_Image_Min_A = ncw_Efm_Document__R_Field_6.newFieldInGroup("ncw_Efm_Document_Image_Min_A", "IMAGE-MIN-A", FieldType.STRING, 11);

        ncw_Efm_Document__R_Field_7 = ncw_Efm_Document__R_Field_6.newGroupInGroup("ncw_Efm_Document__R_Field_7", "REDEFINE", ncw_Efm_Document_Image_Min_A);
        ncw_Efm_Document_Image_Min_N = ncw_Efm_Document__R_Field_7.newFieldInGroup("ncw_Efm_Document_Image_Min_N", "IMAGE-MIN-N", FieldType.NUMERIC, 11);
        registerRecord(vw_ncw_Efm_Document);

        pnd_Work_Record = localVariables.newFieldInRecord("pnd_Work_Record", "#WORK-RECORD", FieldType.STRING, 18);

        pnd_Work_Record__R_Field_8 = localVariables.newGroupInRecord("pnd_Work_Record__R_Field_8", "REDEFINE", pnd_Work_Record);
        pnd_Work_Record_Pnd_Wf_Source_Id = pnd_Work_Record__R_Field_8.newFieldInGroup("pnd_Work_Record_Pnd_Wf_Source_Id", "#WF-SOURCE-ID", FieldType.STRING, 
            6);
        pnd_Work_Record_Pnd_Wf_Mail_Item_No = pnd_Work_Record__R_Field_8.newFieldInGroup("pnd_Work_Record_Pnd_Wf_Mail_Item_No", "#WF-MAIL-ITEM-NO", FieldType.NUMERIC, 
            11);
        pnd_Work_Record_Pnd_Wf_Record_Type = pnd_Work_Record__R_Field_8.newFieldInGroup("pnd_Work_Record_Pnd_Wf_Record_Type", "#WF-RECORD-TYPE", FieldType.STRING, 
            1);
        pnd_Report_Line = localVariables.newFieldInRecord("pnd_Report_Line", "#REPORT-LINE", FieldType.STRING, 22);

        pnd_Report_Line__R_Field_9 = localVariables.newGroupInRecord("pnd_Report_Line__R_Field_9", "REDEFINE", pnd_Report_Line);
        pnd_Report_Line_Pnd_Rl_Source_Id = pnd_Report_Line__R_Field_9.newFieldInGroup("pnd_Report_Line_Pnd_Rl_Source_Id", "#RL-SOURCE-ID", FieldType.STRING, 
            6);
        pnd_Report_Line_Filler_01 = pnd_Report_Line__R_Field_9.newFieldInGroup("pnd_Report_Line_Filler_01", "FILLER-01", FieldType.STRING, 2);
        pnd_Report_Line_Pnd_Rl_Mail_Item_No = pnd_Report_Line__R_Field_9.newFieldInGroup("pnd_Report_Line_Pnd_Rl_Mail_Item_No", "#RL-MAIL-ITEM-NO", FieldType.NUMERIC, 
            11);
        pnd_Report_Line_Filler_02 = pnd_Report_Line__R_Field_9.newFieldInGroup("pnd_Report_Line_Filler_02", "FILLER-02", FieldType.STRING, 2);
        pnd_Report_Line_Pnd_Rl_Record_Type = pnd_Report_Line__R_Field_9.newFieldInGroup("pnd_Report_Line_Pnd_Rl_Record_Type", "#RL-RECORD-TYPE", FieldType.STRING, 
            1);

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Support_Tbl__R_Field_10 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_10", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Run_Indicator = cwf_Support_Tbl__R_Field_10.newFieldInGroup("cwf_Support_Tbl_Run_Indicator", "RUN-INDICATOR", FieldType.STRING, 
            1);
        cwf_Support_Tbl_Filler_01 = cwf_Support_Tbl__R_Field_10.newFieldInGroup("cwf_Support_Tbl_Filler_01", "FILLER-01", FieldType.STRING, 8);
        cwf_Support_Tbl_Normal_Start_Dte_Tme = cwf_Support_Tbl__R_Field_10.newFieldInGroup("cwf_Support_Tbl_Normal_Start_Dte_Tme", "NORMAL-START-DTE-TME", 
            FieldType.NUMERIC, 15);
        cwf_Support_Tbl_Filler_02 = cwf_Support_Tbl__R_Field_10.newFieldInGroup("cwf_Support_Tbl_Filler_02", "FILLER-02", FieldType.STRING, 3);
        cwf_Support_Tbl_Normal_End_Dte_Tme = cwf_Support_Tbl__R_Field_10.newFieldInGroup("cwf_Support_Tbl_Normal_End_Dte_Tme", "NORMAL-END-DTE-TME", FieldType.NUMERIC, 
            15);
        cwf_Support_Tbl_Filler_03 = cwf_Support_Tbl__R_Field_10.newFieldInGroup("cwf_Support_Tbl_Filler_03", "FILLER-03", FieldType.STRING, 2);
        cwf_Support_Tbl_Normal_Record_Count = cwf_Support_Tbl__R_Field_10.newFieldInGroup("cwf_Support_Tbl_Normal_Record_Count", "NORMAL-RECORD-COUNT", 
            FieldType.NUMERIC, 11);
        cwf_Support_Tbl_Filler_04 = cwf_Support_Tbl__R_Field_10.newFieldInGroup("cwf_Support_Tbl_Filler_04", "FILLER-04", FieldType.STRING, 4);
        cwf_Support_Tbl_Et_On_Off_Flag = cwf_Support_Tbl__R_Field_10.newFieldInGroup("cwf_Support_Tbl_Et_On_Off_Flag", "ET-ON-OFF-FLAG", FieldType.STRING, 
            1);
        cwf_Support_Tbl_Filler_05 = cwf_Support_Tbl__R_Field_10.newFieldInGroup("cwf_Support_Tbl_Filler_05", "FILLER-05", FieldType.STRING, 12);
        cwf_Support_Tbl_Special_Start_Dte_Tme = cwf_Support_Tbl__R_Field_10.newFieldInGroup("cwf_Support_Tbl_Special_Start_Dte_Tme", "SPECIAL-START-DTE-TME", 
            FieldType.NUMERIC, 15);
        cwf_Support_Tbl_Filler_06 = cwf_Support_Tbl__R_Field_10.newFieldInGroup("cwf_Support_Tbl_Filler_06", "FILLER-06", FieldType.STRING, 3);
        cwf_Support_Tbl_Special_End_Dte_Tme = cwf_Support_Tbl__R_Field_10.newFieldInGroup("cwf_Support_Tbl_Special_End_Dte_Tme", "SPECIAL-END-DTE-TME", 
            FieldType.NUMERIC, 15);
        cwf_Support_Tbl_Filler_07 = cwf_Support_Tbl__R_Field_10.newFieldInGroup("cwf_Support_Tbl_Filler_07", "FILLER-07", FieldType.STRING, 2);
        cwf_Support_Tbl_Special_Record_Count = cwf_Support_Tbl__R_Field_10.newFieldInGroup("cwf_Support_Tbl_Special_Record_Count", "SPECIAL-RECORD-COUNT", 
            FieldType.NUMERIC, 11);
        cwf_Support_Tbl_Filler_08 = cwf_Support_Tbl__R_Field_10.newFieldInGroup("cwf_Support_Tbl_Filler_08", "FILLER-08", FieldType.STRING, 17);
        cwf_Support_Tbl_Calc_Start_Dte_Tme = cwf_Support_Tbl__R_Field_10.newFieldInGroup("cwf_Support_Tbl_Calc_Start_Dte_Tme", "CALC-START-DTE-TME", FieldType.STRING, 
            15);
        cwf_Support_Tbl_Filler_09 = cwf_Support_Tbl__R_Field_10.newFieldInGroup("cwf_Support_Tbl_Filler_09", "FILLER-09", FieldType.STRING, 3);
        cwf_Support_Tbl_Calc_End_Dte_Tme = cwf_Support_Tbl__R_Field_10.newFieldInGroup("cwf_Support_Tbl_Calc_End_Dte_Tme", "CALC-END-DTE-TME", FieldType.STRING, 
            15);
        cwf_Support_Tbl_Tbl_Actve_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Actve_Ind", "TBL-ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TBL_ACTVE_IND");
        cwf_Support_Tbl_Tbl_Entry_Dte_Tme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Entry_Dte_Tme", "TBL-ENTRY-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_ENTRY_DTE_TME");
        cwf_Support_Tbl_Tbl_Entry_Dte_Tme.setDdmHeader("ENTRY DATE/TIME");
        cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde", "TBL-ENTRY-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_ENTRY_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Support_Tbl_Tbl_Updte_Dte_Tme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Dte_Tme", "TBL-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE_TME");
        cwf_Support_Tbl_Tbl_Updte_Dte_Tme.setDdmHeader("UPDATE DATE/TIME");
        cwf_Support_Tbl_Tbl_Updte_Dte = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Dte", "TBL-UPDTE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE");
        cwf_Support_Tbl_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        cwf_Support_Tbl_Tbl_Dlte_Dte_Tme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Dlte_Dte_Tme", "TBL-DLTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_DLTE_DTE_TME");
        cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde", "TBL-DLTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_DLTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Table_Rectype = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Rectype", "TBL-TABLE-RECTYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TBL_TABLE_RECTYPE");
        cwf_Support_Tbl_Tbl_Table_Access_Level = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Access_Level", "TBL-TABLE-ACCESS-LEVEL", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TBL_TABLE_ACCESS_LEVEL");
        cwf_Support_Tbl_Tbl_Table_Access_Level.setDdmHeader("ACCESS/LEVEL");
        registerRecord(vw_cwf_Support_Tbl);

        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_11 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_11", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_11.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_11.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_11.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind = pnd_Tbl_Prime_Key__R_Field_11.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", FieldType.STRING, 
            1);
        pnd_Common_Image_Pointer_Key = localVariables.newFieldInRecord("pnd_Common_Image_Pointer_Key", "#COMMON-IMAGE-POINTER-KEY", FieldType.STRING, 
            17);

        pnd_Common_Image_Pointer_Key__R_Field_12 = localVariables.newGroupInRecord("pnd_Common_Image_Pointer_Key__R_Field_12", "REDEFINE", pnd_Common_Image_Pointer_Key);
        pnd_Common_Image_Pointer_Key_Pnd_Common_Image_Source_Id = pnd_Common_Image_Pointer_Key__R_Field_12.newFieldInGroup("pnd_Common_Image_Pointer_Key_Pnd_Common_Image_Source_Id", 
            "#COMMON-IMAGE-SOURCE-ID", FieldType.STRING, 6);
        pnd_Common_Image_Pointer_Key_Pnd_Common_Image_Min = pnd_Common_Image_Pointer_Key__R_Field_12.newFieldInGroup("pnd_Common_Image_Pointer_Key_Pnd_Common_Image_Min", 
            "#COMMON-IMAGE-MIN", FieldType.NUMERIC, 11);
        pnd_Source_Id_Min_Key = localVariables.newFieldInRecord("pnd_Source_Id_Min_Key", "#SOURCE-ID-MIN-KEY", FieldType.STRING, 17);

        pnd_Source_Id_Min_Key__R_Field_13 = localVariables.newGroupInRecord("pnd_Source_Id_Min_Key__R_Field_13", "REDEFINE", pnd_Source_Id_Min_Key);
        pnd_Source_Id_Min_Key_Pnd_Source_Id = pnd_Source_Id_Min_Key__R_Field_13.newFieldInGroup("pnd_Source_Id_Min_Key_Pnd_Source_Id", "#SOURCE-ID", FieldType.STRING, 
            6);
        pnd_Source_Id_Min_Key_Pnd_Min_Nbr = pnd_Source_Id_Min_Key__R_Field_13.newFieldInGroup("pnd_Source_Id_Min_Key_Pnd_Min_Nbr", "#MIN-NBR", FieldType.STRING, 
            11);
        pnd_Wf_Rec_Cntr = localVariables.newFieldInRecord("pnd_Wf_Rec_Cntr", "#WF-REC-CNTR", FieldType.NUMERIC, 7);
        pnd_Xref_Cntr = localVariables.newFieldInRecord("pnd_Xref_Cntr", "#XREF-CNTR", FieldType.NUMERIC, 7);
        pnd_Xref_Cntr_Accpt = localVariables.newFieldInRecord("pnd_Xref_Cntr_Accpt", "#XREF-CNTR-ACCPT", FieldType.NUMERIC, 7);
        pnd_Xref_Orphan = localVariables.newFieldInRecord("pnd_Xref_Orphan", "#XREF-ORPHAN", FieldType.NUMERIC, 7);
        pnd_Cdoc_Cntr = localVariables.newFieldInRecord("pnd_Cdoc_Cntr", "#CDOC-CNTR", FieldType.NUMERIC, 7);
        pnd_Cdoc_Cntr_Accpt = localVariables.newFieldInRecord("pnd_Cdoc_Cntr_Accpt", "#CDOC-CNTR-ACCPT", FieldType.NUMERIC, 7);
        pnd_Cdoc_Orphan = localVariables.newFieldInRecord("pnd_Cdoc_Orphan", "#CDOC-ORPHAN", FieldType.NUMERIC, 7);
        pnd_Idoc_Cntr = localVariables.newFieldInRecord("pnd_Idoc_Cntr", "#IDOC-CNTR", FieldType.NUMERIC, 7);
        pnd_Idoc_Cntr_Accpt = localVariables.newFieldInRecord("pnd_Idoc_Cntr_Accpt", "#IDOC-CNTR-ACCPT", FieldType.NUMERIC, 7);
        pnd_Idoc_Orphan = localVariables.newFieldInRecord("pnd_Idoc_Orphan", "#IDOC-ORPHAN", FieldType.NUMERIC, 7);
        pnd_Ndoc_Cntr = localVariables.newFieldInRecord("pnd_Ndoc_Cntr", "#NDOC-CNTR", FieldType.NUMERIC, 7);
        pnd_Ndoc_Cntr_Accpt = localVariables.newFieldInRecord("pnd_Ndoc_Cntr_Accpt", "#NDOC-CNTR-ACCPT", FieldType.NUMERIC, 7);
        pnd_Ndoc_Orphan = localVariables.newFieldInRecord("pnd_Ndoc_Orphan", "#NDOC-ORPHAN", FieldType.NUMERIC, 7);
        pnd_Doc_Fnd = localVariables.newFieldInRecord("pnd_Doc_Fnd", "#DOC-FND", FieldType.STRING, 1);
        pnd_Control_Flag = localVariables.newFieldInRecord("pnd_Control_Flag", "#CONTROL-FLAG", FieldType.STRING, 1);
        pnd_Start_T = localVariables.newFieldInRecord("pnd_Start_T", "#START-T", FieldType.TIME);
        pnd_End_T = localVariables.newFieldInRecord("pnd_End_T", "#END-T", FieldType.TIME);
        pnd_Isn_Control = localVariables.newFieldInRecord("pnd_Isn_Control", "#ISN-CONTROL", FieldType.PACKED_DECIMAL, 11);
        pnd_Running_Dte_Tme_Str = localVariables.newFieldInRecord("pnd_Running_Dte_Tme_Str", "#RUNNING-DTE-TME-STR", FieldType.NUMERIC, 15);

        pnd_Running_Dte_Tme_Str__R_Field_14 = localVariables.newGroupInRecord("pnd_Running_Dte_Tme_Str__R_Field_14", "REDEFINE", pnd_Running_Dte_Tme_Str);
        pnd_Running_Dte_Tme_Str_Pnd_Running_Dte_Tme_Str_A = pnd_Running_Dte_Tme_Str__R_Field_14.newFieldInGroup("pnd_Running_Dte_Tme_Str_Pnd_Running_Dte_Tme_Str_A", 
            "#RUNNING-DTE-TME-STR-A", FieldType.STRING, 15);
        pnd_Running_Dte_Tme_End = localVariables.newFieldInRecord("pnd_Running_Dte_Tme_End", "#RUNNING-DTE-TME-END", FieldType.NUMERIC, 15);

        pnd_Running_Dte_Tme_End__R_Field_15 = localVariables.newGroupInRecord("pnd_Running_Dte_Tme_End__R_Field_15", "REDEFINE", pnd_Running_Dte_Tme_End);
        pnd_Running_Dte_Tme_End_Pnd_Running_Dte_Tme_End_A = pnd_Running_Dte_Tme_End__R_Field_15.newFieldInGroup("pnd_Running_Dte_Tme_End_Pnd_Running_Dte_Tme_End_A", 
            "#RUNNING-DTE-TME-END-A", FieldType.STRING, 15);

        pnd_Running_Dte_Tme_End__R_Field_16 = pnd_Running_Dte_Tme_End__R_Field_15.newGroupInGroup("pnd_Running_Dte_Tme_End__R_Field_16", "REDEFINE", pnd_Running_Dte_Tme_End_Pnd_Running_Dte_Tme_End_A);
        pnd_Running_Dte_Tme_End_Pnd_Running_Date_End = pnd_Running_Dte_Tme_End__R_Field_16.newFieldInGroup("pnd_Running_Dte_Tme_End_Pnd_Running_Date_End", 
            "#RUNNING-DATE-END", FieldType.STRING, 8);
        pnd_Running_Dte_Tme_End_Pnd_Running_Time_End = pnd_Running_Dte_Tme_End__R_Field_16.newFieldInGroup("pnd_Running_Dte_Tme_End_Pnd_Running_Time_End", 
            "#RUNNING-TIME-END", FieldType.NUMERIC, 7);
        pnd_Date_Time_A = localVariables.newFieldInRecord("pnd_Date_Time_A", "#DATE-TIME-A", FieldType.STRING, 15);

        pnd_Date_Time_A__R_Field_17 = localVariables.newGroupInRecord("pnd_Date_Time_A__R_Field_17", "REDEFINE", pnd_Date_Time_A);
        pnd_Date_Time_A_Pnd_Date_Time_N = pnd_Date_Time_A__R_Field_17.newFieldInGroup("pnd_Date_Time_A_Pnd_Date_Time_N", "#DATE-TIME-N", FieldType.NUMERIC, 
            15);
        pnd_Date_T = localVariables.newFieldInRecord("pnd_Date_T", "#DATE-T", FieldType.TIME);
        pnd_Date_T_Result = localVariables.newFieldInRecord("pnd_Date_T_Result", "#DATE-T-RESULT", FieldType.TIME);
        pnd_Curr_Date_N = localVariables.newFieldInRecord("pnd_Curr_Date_N", "#CURR-DATE-N", FieldType.NUMERIC, 8);

        pnd_Curr_Date_N__R_Field_18 = localVariables.newGroupInRecord("pnd_Curr_Date_N__R_Field_18", "REDEFINE", pnd_Curr_Date_N);
        pnd_Curr_Date_N_Pnd_Curr_Date_A = pnd_Curr_Date_N__R_Field_18.newFieldInGroup("pnd_Curr_Date_N_Pnd_Curr_Date_A", "#CURR-DATE-A", FieldType.STRING, 
            8);
        pnd_Date_D = localVariables.newFieldInRecord("pnd_Date_D", "#DATE-D", FieldType.DATE);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Image_Xref.reset();
        vw_cwf_Efm_Document.reset();
        vw_icw_Efm_Document.reset();
        vw_ncw_Efm_Document.reset();
        vw_cwf_Support_Tbl.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Efsb6110() throws Exception
    {
        super("Efsb6110");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 132 PS = 60
        //*  --------------- MAIN PROCESS -----------------------
                                                                                                                                                                          //Natural: PERFORM READ-RUN-CONTROL-RECORD
        sub_Read_Run_Control_Record();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM COLLECT-XREF-RECORDS
        sub_Collect_Xref_Records();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM COLLECT-CDOC-RECORDS
        sub_Collect_Cdoc_Records();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM COLLECT-IDOC-RECORDS
        sub_Collect_Idoc_Records();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM COLLECT-NDOC-RECORDS
        sub_Collect_Ndoc_Records();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CONTROL-RECORD-UPDATE
        sub_Control_Record_Update();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-STATISTIC-REPORT
        sub_Write_Statistic_Report();
        if (condition(Global.isEscape())) {return;}
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COLLECT-XREF-RECORDS
        //* *************************************
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-DOC-INFO-FROM-CWF
        //* ***************************************
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-DOC-INFO-FROM-ICW
        //* ***************************************
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-DOC-INFO-FROM-NCW
        //* ***************************************
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COLLECT-CDOC-RECORDS
        //* *************************************
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COLLECT-IDOC-RECORDS
        //* *************************************
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COLLECT-NDOC-RECORDS
        //* *************************************
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-IMAGE-XREF-RECORD
        //*  =======================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-RUN-CONTROL-RECORD
        //*  --------------- CONTROL RECORD HAS NO ERRORS --------------------
        //*  ===============================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WF-RECORD
        //*  =====================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CONTROL-RECORD-UPDATE
        //*  =====================================
        //*  ======================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-STATISTIC-REPORT
        //*  ======================================
    }
    private void sub_Collect_Xref_Records() throws Exception                                                                                                              //Natural: COLLECT-XREF-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        vw_cwf_Image_Xref.startDatabaseRead                                                                                                                               //Natural: READ CWF-IMAGE-XREF BY UPLD-DATE-TIME STARTING FROM #START-T
        (
        "R1",
        new Wc[] { new Wc("UPLD_DATE_TIME", ">=", pnd_Start_T, WcType.BY) },
        new Oc[] { new Oc("UPLD_DATE_TIME", "ASC") }
        );
        R1:
        while (condition(vw_cwf_Image_Xref.readNextRow("R1")))
        {
            pnd_Xref_Cntr.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #XREF-CNTR
            pnd_Work_Record.reset();                                                                                                                                      //Natural: RESET #WORK-RECORD #REPORT-LINE #DOC-FND
            pnd_Report_Line.reset();
            pnd_Doc_Fnd.reset();
            if (condition(cwf_Image_Xref_Source_Id.equals("FORMS")))                                                                                                      //Natural: IF CWF-IMAGE-XREF.SOURCE-ID EQ 'FORMS'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cwf_Image_Xref_Upld_Date_Time.equals(getZero()) || cwf_Image_Xref_Source_Id.equals(" ") || cwf_Image_Xref_Mail_Item_Nbr.equals(getZero())       //Natural: IF CWF-IMAGE-XREF.UPLD-DATE-TIME = 0 OR CWF-IMAGE-XREF.SOURCE-ID EQ ' ' OR CWF-IMAGE-XREF.MAIL-ITEM-NBR = 0 OR CWF-IMAGE-XREF.MAIL-ITEM-NBR NOT = MASK ( NNNNNNNNNNN )
                || ! (DbsUtil.maskMatches(cwf_Image_Xref_Mail_Item_Nbr,"NNNNNNNNNNN"))))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  END OF THE WINDOW
            if (condition(cwf_Image_Xref_Upld_Date_Time.greater(pnd_End_T)))                                                                                              //Natural: IF CWF-IMAGE-XREF.UPLD-DATE-TIME GT #END-T
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Xref_Cntr_Accpt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #XREF-CNTR-ACCPT
            //*  ------------------- BUILD COMMON KEY -------------------------
            pnd_Common_Image_Pointer_Key_Pnd_Common_Image_Source_Id.setValue(cwf_Image_Xref_Source_Id);                                                                   //Natural: MOVE CWF-IMAGE-XREF.SOURCE-ID TO #COMMON-IMAGE-SOURCE-ID
            pnd_Common_Image_Pointer_Key_Pnd_Common_Image_Min.setValue(cwf_Image_Xref_Mail_Item_Nbr);                                                                     //Natural: MOVE CWF-IMAGE-XREF.MAIL-ITEM-NBR TO #COMMON-IMAGE-MIN
            //*  ---------------------------------------------------------------
                                                                                                                                                                          //Natural: PERFORM FIND-DOC-INFO-FROM-CWF
            sub_Find_Doc_Info_From_Cwf();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Doc_Fnd.greater(" ")))                                                                                                                      //Natural: IF #DOC-FND GT ' '
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM FIND-DOC-INFO-FROM-ICW
            sub_Find_Doc_Info_From_Icw();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Doc_Fnd.greater(" ")))                                                                                                                      //Natural: IF #DOC-FND GT ' '
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM FIND-DOC-INFO-FROM-NCW
            sub_Find_Doc_Info_From_Ncw();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Doc_Fnd.greater(" ")))                                                                                                                      //Natural: IF #DOC-FND GT ' '
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  ---------- WRITE WORK FILE IF DOCUMENT NOT FOUND --------
            pnd_Xref_Orphan.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #XREF-ORPHAN
            pnd_Work_Record_Pnd_Wf_Source_Id.setValue(cwf_Image_Xref_Source_Id);                                                                                          //Natural: MOVE CWF-IMAGE-XREF.SOURCE-ID TO #WORK-RECORD.#WF-SOURCE-ID
            pnd_Work_Record_Pnd_Wf_Mail_Item_No.setValue(cwf_Image_Xref_Mail_Item_Nbr);                                                                                   //Natural: MOVE CWF-IMAGE-XREF.MAIL-ITEM-NBR TO #WORK-RECORD.#WF-MAIL-ITEM-NO
            pnd_Work_Record_Pnd_Wf_Record_Type.setValue("X");                                                                                                             //Natural: MOVE 'X' TO #WORK-RECORD.#WF-RECORD-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-WF-RECORD
            sub_Write_Wf_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  R1.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Find_Doc_Info_From_Cwf() throws Exception                                                                                                            //Natural: FIND-DOC-INFO-FROM-CWF
    {
        if (BLNatReinput.isReinput()) return;

        vw_cwf_Efm_Document.startDatabaseFind                                                                                                                             //Natural: FIND ( 1 ) CWF-EFM-DOCUMENT WITH IMAGE-POINTER-KEY = #COMMON-IMAGE-POINTER-KEY
        (
        "F1",
        new Wc[] { new Wc("IMAGE_POINTER_KEY", "=", pnd_Common_Image_Pointer_Key, WcType.WITH) },
        1
        );
        F1:
        while (condition(vw_cwf_Efm_Document.readNextRow("F1", true)))
        {
            vw_cwf_Efm_Document.setIfNotFoundControlFlag(false);
            if (condition(vw_cwf_Efm_Document.getAstCOUNTER().equals(0)))                                                                                                 //Natural: IF NO RECORD FOUND
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Doc_Fnd.setValue("Y");                                                                                                                                    //Natural: MOVE 'Y' TO #DOC-FND
            //*  F1.
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Find_Doc_Info_From_Icw() throws Exception                                                                                                            //Natural: FIND-DOC-INFO-FROM-ICW
    {
        if (BLNatReinput.isReinput()) return;

        vw_icw_Efm_Document.startDatabaseFind                                                                                                                             //Natural: FIND ( 1 ) ICW-EFM-DOCUMENT WITH IMAGE-POINTER-KEY = #COMMON-IMAGE-POINTER-KEY
        (
        "F2",
        new Wc[] { new Wc("IMAGE_POINTER_KEY", "=", pnd_Common_Image_Pointer_Key, WcType.WITH) },
        1
        );
        F2:
        while (condition(vw_icw_Efm_Document.readNextRow("F2", true)))
        {
            vw_icw_Efm_Document.setIfNotFoundControlFlag(false);
            if (condition(vw_icw_Efm_Document.getAstCOUNTER().equals(0)))                                                                                                 //Natural: IF NO RECORD FOUND
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Doc_Fnd.setValue("Y");                                                                                                                                    //Natural: MOVE 'Y' TO #DOC-FND
            //*  F2.
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Find_Doc_Info_From_Ncw() throws Exception                                                                                                            //Natural: FIND-DOC-INFO-FROM-NCW
    {
        if (BLNatReinput.isReinput()) return;

        vw_ncw_Efm_Document.startDatabaseFind                                                                                                                             //Natural: FIND ( 1 ) NCW-EFM-DOCUMENT WITH IMAGE-POINTER-KEY = #COMMON-IMAGE-POINTER-KEY
        (
        "F3",
        new Wc[] { new Wc("IMAGE_POINTER_KEY", "=", pnd_Common_Image_Pointer_Key, WcType.WITH) },
        1
        );
        F3:
        while (condition(vw_ncw_Efm_Document.readNextRow("F3", true)))
        {
            vw_ncw_Efm_Document.setIfNotFoundControlFlag(false);
            if (condition(vw_ncw_Efm_Document.getAstCOUNTER().equals(0)))                                                                                                 //Natural: IF NO RECORD FOUND
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Doc_Fnd.setValue("Y");                                                                                                                                    //Natural: MOVE 'Y' TO #DOC-FND
            //*  F3.
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Collect_Cdoc_Records() throws Exception                                                                                                              //Natural: COLLECT-CDOC-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        vw_cwf_Efm_Document.startDatabaseRead                                                                                                                             //Natural: READ CWF-EFM-DOCUMENT BY ENTRY-DTE-TME STARTING FROM #START-T
        (
        "R2",
        new Wc[] { new Wc("ENTRY_DTE_TME", ">=", pnd_Start_T, WcType.BY) },
        new Oc[] { new Oc("ENTRY_DTE_TME", "ASC") }
        );
        R2:
        while (condition(vw_cwf_Efm_Document.readNextRow("R2")))
        {
            pnd_Cdoc_Cntr.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #CDOC-CNTR
            pnd_Work_Record.reset();                                                                                                                                      //Natural: RESET #WORK-RECORD #REPORT-LINE #DOC-FND
            pnd_Report_Line.reset();
            pnd_Doc_Fnd.reset();
            if (condition(cwf_Efm_Document_Entry_Dte_Tme.equals(getZero()) || cwf_Efm_Document_Image_Source_Id.equals(" ") || cwf_Efm_Document_Image_Min.equals(" ")      //Natural: IF CWF-EFM-DOCUMENT.ENTRY-DTE-TME = 0 OR CWF-EFM-DOCUMENT.IMAGE-SOURCE-ID = ' ' OR CWF-EFM-DOCUMENT.IMAGE-MIN = ' ' OR CWF-EFM-DOCUMENT.IMAGE-MIN NOT = MASK ( NNNNNNNNNNN )
                || ! (DbsUtil.maskMatches(cwf_Efm_Document_Image_Min,"NNNNNNNNNNN"))))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  END OF THE WINDOW
            if (condition(cwf_Efm_Document_Entry_Dte_Tme.greater(pnd_End_T)))                                                                                             //Natural: IF CWF-EFM-DOCUMENT.ENTRY-DTE-TME GT #END-T
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Cdoc_Cntr_Accpt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #CDOC-CNTR-ACCPT
            //*  ------------------- BUILD SOURCE-ID-MIN-KEY -------------------------
            pnd_Source_Id_Min_Key_Pnd_Source_Id.setValue(cwf_Efm_Document_Image_Source_Id);                                                                               //Natural: MOVE CWF-EFM-DOCUMENT.IMAGE-SOURCE-ID TO #SOURCE-ID-MIN-KEY.#SOURCE-ID
            pnd_Source_Id_Min_Key_Pnd_Min_Nbr.setValue(cwf_Efm_Document_Image_Min);                                                                                       //Natural: MOVE CWF-EFM-DOCUMENT.IMAGE-MIN TO #SOURCE-ID-MIN-KEY.#MIN-NBR
            //*  ----------------------------------------------------------------------
                                                                                                                                                                          //Natural: PERFORM FIND-IMAGE-XREF-RECORD
            sub_Find_Image_Xref_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R2"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Doc_Fnd.greater(" ")))                                                                                                                      //Natural: IF #DOC-FND GT ' '
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  ---------- WRITE WORK FILE IF IMAGE-XREF NOT FOUND --------
            pnd_Cdoc_Orphan.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #CDOC-ORPHAN
            pnd_Work_Record_Pnd_Wf_Source_Id.setValue(cwf_Efm_Document_Image_Source_Id);                                                                                  //Natural: MOVE CWF-EFM-DOCUMENT.IMAGE-SOURCE-ID TO #WORK-RECORD.#WF-SOURCE-ID
            pnd_Work_Record_Pnd_Wf_Mail_Item_No.setValue(cwf_Efm_Document_Image_Min_N);                                                                                   //Natural: MOVE CWF-EFM-DOCUMENT.IMAGE-MIN-N TO #WORK-RECORD.#WF-MAIL-ITEM-NO
            pnd_Work_Record_Pnd_Wf_Record_Type.setValue("D");                                                                                                             //Natural: MOVE 'D' TO #WORK-RECORD.#WF-RECORD-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-WF-RECORD
            sub_Write_Wf_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R2"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  R2.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Collect_Idoc_Records() throws Exception                                                                                                              //Natural: COLLECT-IDOC-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        vw_icw_Efm_Document.startDatabaseRead                                                                                                                             //Natural: READ ICW-EFM-DOCUMENT BY CRTE-DTE-TME STARTING FROM #START-T
        (
        "R3",
        new Wc[] { new Wc("CRTE_DTE_TME", ">=", pnd_Start_T, WcType.BY) },
        new Oc[] { new Oc("CRTE_DTE_TME", "ASC") }
        );
        R3:
        while (condition(vw_icw_Efm_Document.readNextRow("R3")))
        {
            pnd_Idoc_Cntr.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #IDOC-CNTR
            pnd_Work_Record.reset();                                                                                                                                      //Natural: RESET #WORK-RECORD #REPORT-LINE #DOC-FND
            pnd_Report_Line.reset();
            pnd_Doc_Fnd.reset();
            if (condition(icw_Efm_Document_Crte_Dte_Tme.equals(getZero()) || icw_Efm_Document_Image_Source_Id.equals(" ") || icw_Efm_Document_Image_Min.equals(" ")       //Natural: IF ICW-EFM-DOCUMENT.CRTE-DTE-TME = 0 OR ICW-EFM-DOCUMENT.IMAGE-SOURCE-ID = ' ' OR ICW-EFM-DOCUMENT.IMAGE-MIN = ' ' OR ICW-EFM-DOCUMENT.IMAGE-MIN NOT = MASK ( NNNNNNNNNNN )
                || ! (DbsUtil.maskMatches(icw_Efm_Document_Image_Min,"NNNNNNNNNNN"))))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  END OF THE WINDOW
            if (condition(icw_Efm_Document_Crte_Dte_Tme.greater(pnd_End_T)))                                                                                              //Natural: IF ICW-EFM-DOCUMENT.CRTE-DTE-TME GT #END-T
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Idoc_Cntr_Accpt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #IDOC-CNTR-ACCPT
            //*  ------------------- BUILD SOURCE-ID-MIN-KEY -------------------------
            pnd_Source_Id_Min_Key_Pnd_Source_Id.setValue(icw_Efm_Document_Image_Source_Id);                                                                               //Natural: MOVE ICW-EFM-DOCUMENT.IMAGE-SOURCE-ID TO #SOURCE-ID-MIN-KEY.#SOURCE-ID
            pnd_Source_Id_Min_Key_Pnd_Min_Nbr.setValue(icw_Efm_Document_Image_Min);                                                                                       //Natural: MOVE ICW-EFM-DOCUMENT.IMAGE-MIN TO #SOURCE-ID-MIN-KEY.#MIN-NBR
            //*  ----------------------------------------------------------------------
                                                                                                                                                                          //Natural: PERFORM FIND-IMAGE-XREF-RECORD
            sub_Find_Image_Xref_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R3"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R3"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Doc_Fnd.greater(" ")))                                                                                                                      //Natural: IF #DOC-FND GT ' '
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  ---------- WRITE WORK FILE IF IMAGE-XREF NOT FOUND --------
            pnd_Idoc_Orphan.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #IDOC-ORPHAN
            pnd_Work_Record_Pnd_Wf_Source_Id.setValue(icw_Efm_Document_Image_Source_Id);                                                                                  //Natural: MOVE ICW-EFM-DOCUMENT.IMAGE-SOURCE-ID TO #WORK-RECORD.#WF-SOURCE-ID
            pnd_Work_Record_Pnd_Wf_Mail_Item_No.setValue(icw_Efm_Document_Image_Min_N);                                                                                   //Natural: MOVE ICW-EFM-DOCUMENT.IMAGE-MIN-N TO #WORK-RECORD.#WF-MAIL-ITEM-NO
            pnd_Work_Record_Pnd_Wf_Record_Type.setValue("I");                                                                                                             //Natural: MOVE 'I' TO #WORK-RECORD.#WF-RECORD-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-WF-RECORD
            sub_Write_Wf_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R3"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R3"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  R3.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Collect_Ndoc_Records() throws Exception                                                                                                              //Natural: COLLECT-NDOC-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        vw_ncw_Efm_Document.startDatabaseRead                                                                                                                             //Natural: READ NCW-EFM-DOCUMENT BY CRTE-DTE-TME STARTING FROM #START-T
        (
        "R4",
        new Wc[] { new Wc("CRTE_DTE_TME", ">=", pnd_Start_T, WcType.BY) },
        new Oc[] { new Oc("CRTE_DTE_TME", "ASC") }
        );
        R4:
        while (condition(vw_ncw_Efm_Document.readNextRow("R4")))
        {
            pnd_Ndoc_Cntr.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #NDOC-CNTR
            pnd_Work_Record.reset();                                                                                                                                      //Natural: RESET #WORK-RECORD #REPORT-LINE #DOC-FND
            pnd_Report_Line.reset();
            pnd_Doc_Fnd.reset();
            if (condition(ncw_Efm_Document_Crte_Dte_Tme.equals(getZero()) || ncw_Efm_Document_Image_Source_Id.equals(" ") || ncw_Efm_Document_Image_Min.equals(" ")       //Natural: IF NCW-EFM-DOCUMENT.CRTE-DTE-TME = 0 OR NCW-EFM-DOCUMENT.IMAGE-SOURCE-ID = ' ' OR NCW-EFM-DOCUMENT.IMAGE-MIN = ' ' OR NCW-EFM-DOCUMENT.IMAGE-MIN NOT = MASK ( NNNNNNNNNNN )
                || ! (DbsUtil.maskMatches(ncw_Efm_Document_Image_Min,"NNNNNNNNNNN"))))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  END OF THE WINDOW
            if (condition(ncw_Efm_Document_Crte_Dte_Tme.greater(pnd_End_T)))                                                                                              //Natural: IF NCW-EFM-DOCUMENT.CRTE-DTE-TME GT #END-T
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ndoc_Cntr_Accpt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #NDOC-CNTR-ACCPT
            //*  ------------------- BUILD SOURCE-ID-MIN-KEY -------------------------
            pnd_Source_Id_Min_Key_Pnd_Source_Id.setValue(ncw_Efm_Document_Image_Source_Id);                                                                               //Natural: MOVE NCW-EFM-DOCUMENT.IMAGE-SOURCE-ID TO #SOURCE-ID-MIN-KEY.#SOURCE-ID
            pnd_Source_Id_Min_Key_Pnd_Min_Nbr.setValue(ncw_Efm_Document_Image_Min);                                                                                       //Natural: MOVE NCW-EFM-DOCUMENT.IMAGE-MIN TO #SOURCE-ID-MIN-KEY.#MIN-NBR
            //*  ----------------------------------------------------------------------
                                                                                                                                                                          //Natural: PERFORM FIND-IMAGE-XREF-RECORD
            sub_Find_Image_Xref_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R4"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R4"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Doc_Fnd.greater(" ")))                                                                                                                      //Natural: IF #DOC-FND GT ' '
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  ---------- WRITE WORK FILE IF IMAGE-XREF NOT FOUND --------
            pnd_Ndoc_Orphan.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #NDOC-ORPHAN
            pnd_Work_Record_Pnd_Wf_Source_Id.setValue(ncw_Efm_Document_Image_Source_Id);                                                                                  //Natural: MOVE NCW-EFM-DOCUMENT.IMAGE-SOURCE-ID TO #WORK-RECORD.#WF-SOURCE-ID
            pnd_Work_Record_Pnd_Wf_Mail_Item_No.setValue(ncw_Efm_Document_Image_Min_N);                                                                                   //Natural: MOVE NCW-EFM-DOCUMENT.IMAGE-MIN-N TO #WORK-RECORD.#WF-MAIL-ITEM-NO
            pnd_Work_Record_Pnd_Wf_Record_Type.setValue("N");                                                                                                             //Natural: MOVE 'N' TO #WORK-RECORD.#WF-RECORD-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-WF-RECORD
            sub_Write_Wf_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R4"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R4"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  R4.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Find_Image_Xref_Record() throws Exception                                                                                                            //Natural: FIND-IMAGE-XREF-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        pnd_Doc_Fnd.reset();                                                                                                                                              //Natural: RESET #DOC-FND
        vw_cwf_Image_Xref.startDatabaseFind                                                                                                                               //Natural: FIND ( 1 ) CWF-IMAGE-XREF WITH SOURCE-ID-MIN-KEY = #SOURCE-ID-MIN-KEY
        (
        "F4",
        new Wc[] { new Wc("SOURCE_ID_MIN_KEY", "=", pnd_Source_Id_Min_Key, WcType.WITH) },
        1
        );
        F4:
        while (condition(vw_cwf_Image_Xref.readNextRow("F4", true)))
        {
            vw_cwf_Image_Xref.setIfNotFoundControlFlag(false);
            if (condition(vw_cwf_Image_Xref.getAstCOUNTER().equals(0)))                                                                                                   //Natural: IF NO RECORD FOUND
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Doc_Fnd.setValue("Y");                                                                                                                                    //Natural: MOVE 'Y' TO #DOC-FND
            //*  F1.
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Read_Run_Control_Record() throws Exception                                                                                                           //Natural: READ-RUN-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  =======================================
        //*  --------- SET THE KEY -----------------
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind.setValue("A");                                                                                                          //Natural: MOVE 'A' TO #TBL-SCRTY-LEVEL-IND
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme.setValue("XREF-DOC-RECONCIL");                                                                                                //Natural: MOVE 'XREF-DOC-RECONCIL' TO #TBL-TABLE-NME
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field.setValue("RECONCILIATION EXTRACT");                                                                                           //Natural: MOVE 'RECONCILIATION EXTRACT' TO #TBL-KEY-FIELD
        //*  ------------------------------------------
        vw_cwf_Support_Tbl.startDatabaseRead                                                                                                                              //Natural: READ ( 1 ) CWF-SUPPORT-TBL BY TBL-PRIME-KEY STARTING FROM #TBL-PRIME-KEY
        (
        "READ01",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(vw_cwf_Support_Tbl.readNextRow("READ01")))
        {
            //*  --------------- CHECK FOR CONTROL RECORD ERRORS -----------------
            if (condition(cwf_Support_Tbl_Run_Indicator.equals("S") || cwf_Support_Tbl_Run_Indicator.equals("N")))                                                        //Natural: IF CWF-SUPPORT-TBL.RUN-INDICATOR = 'S' OR = 'N'
            {
                pnd_Control_Flag.setValue("Y");                                                                                                                           //Natural: MOVE 'Y' TO #CONTROL-FLAG
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Control_Flag.setValue("N");                                                                                                                           //Natural: MOVE 'N' TO #CONTROL-FLAG
                getReports().write(0, "Run-Indicator on Control record is Wrong - can be 'S'or'N'");                                                                      //Natural: WRITE 'Run-Indicator on Control record is Wrong - can be "S"or"N"'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(11);  if (true) return;                                                                                                                 //Natural: TERMINATE 11
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cwf_Support_Tbl_Run_Indicator.equals("S")))                                                                                                     //Natural: IF CWF-SUPPORT-TBL.RUN-INDICATOR = 'S'
            {
                if (condition(cwf_Support_Tbl_Special_Start_Dte_Tme.equals(getZero()) || cwf_Support_Tbl_Special_End_Dte_Tme.equals(getZero())))                          //Natural: IF CWF-SUPPORT-TBL.SPECIAL-START-DTE-TME = 0 OR CWF-SUPPORT-TBL.SPECIAL-END-DTE-TME = 0
                {
                    getReports().write(0, "Supply Start and End dates for Special Run");                                                                                  //Natural: WRITE 'Supply Start and End dates for Special Run'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    DbsUtil.terminate(12);  if (true) return;                                                                                                             //Natural: TERMINATE 12
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cwf_Support_Tbl_Run_Indicator.equals("S")))                                                                                                     //Natural: IF CWF-SUPPORT-TBL.RUN-INDICATOR = 'S'
            {
                if (condition(cwf_Support_Tbl_Special_End_Dte_Tme.lessOrEqual(cwf_Support_Tbl_Special_Start_Dte_Tme)))                                                    //Natural: IF CWF-SUPPORT-TBL.SPECIAL-END-DTE-TME LE CWF-SUPPORT-TBL.SPECIAL-START-DTE-TME
                {
                    getReports().write(0, "End date cannot be Equal or Less than Starting date");                                                                         //Natural: WRITE 'End date cannot be Equal or Less than Starting date'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    DbsUtil.terminate(13);  if (true) return;                                                                                                             //Natural: TERMINATE 13
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cwf_Support_Tbl_Run_Indicator.equals("N")))                                                                                                     //Natural: IF CWF-SUPPORT-TBL.RUN-INDICATOR = 'N'
            {
                if (condition(cwf_Support_Tbl_Normal_End_Dte_Tme.equals(getZero())))                                                                                      //Natural: IF CWF-SUPPORT-TBL.NORMAL-END-DTE-TME EQ 0
                {
                    getReports().write(0, "End date of 'Normal Run' cannot be Equal to Zero");                                                                            //Natural: WRITE 'End date of "Normal Run" cannot be Equal to Zero'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    DbsUtil.terminate(14);  if (true) return;                                                                                                             //Natural: TERMINATE 14
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Isn_Control.setValue(vw_cwf_Support_Tbl.getAstISN("Read01"));                                                                                             //Natural: ASSIGN #ISN-CONTROL := *ISN
            if (condition(cwf_Support_Tbl_Run_Indicator.equals("S")))                                                                                                     //Natural: IF CWF-SUPPORT-TBL.RUN-INDICATOR = 'S'
            {
                pnd_Running_Dte_Tme_Str.setValue(cwf_Support_Tbl_Special_Start_Dte_Tme);                                                                                  //Natural: MOVE CWF-SUPPORT-TBL.SPECIAL-START-DTE-TME TO #RUNNING-DTE-TME-STR
                pnd_Running_Dte_Tme_End.setValue(cwf_Support_Tbl_Special_End_Dte_Tme);                                                                                    //Natural: MOVE CWF-SUPPORT-TBL.SPECIAL-END-DTE-TME TO #RUNNING-DTE-TME-END
            }                                                                                                                                                             //Natural: END-IF
            //*  WRITE /'******** Normal Running Dates before adjustment *******'
            //*        /'Start Date.............= ' NORMAL-START-DTE-TME
            //*        /'End   Date.............= ' NORMAL-END-DTE-TME
            //*        /'Running Start..........= ' #RUNNING-DTE-TME-STR
            //*        /'Running End............= ' #RUNNING-DTE-TME-END
            //*  STOP
            //*  -------------- INDICATOR EQUAL TO "N" -----------------------
            if (condition(cwf_Support_Tbl_Run_Indicator.equals("N")))                                                                                                     //Natural: IF CWF-SUPPORT-TBL.RUN-INDICATOR = 'N'
            {
                pnd_Date_Time_A.setValue(cwf_Support_Tbl_Normal_Start_Dte_Tme);                                                                                           //Natural: MOVE CWF-SUPPORT-TBL.NORMAL-START-DTE-TME TO #DATE-TIME-A
                //*  ONE DAY ADVANCE
                pnd_Date_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),pnd_Date_Time_A);                                                                         //Natural: MOVE EDITED #DATE-TIME-A TO #DATE-T ( EM = YYYYMMDDHHIISST )
                pnd_Date_T_Result.compute(new ComputeParameters(false, pnd_Date_T_Result), (pnd_Date_T.add(864000)));                                                     //Natural: ASSIGN #DATE-T-RESULT := ( #DATE-T + 864000 )
                pnd_Date_Time_A.setValueEdited(pnd_Date_T_Result,new ReportEditMask("YYYYMMDDHHIISST"));                                                                  //Natural: MOVE EDITED #DATE-T-RESULT ( EM = YYYYMMDDHHIISST ) TO #DATE-TIME-A
                pnd_Running_Dte_Tme_Str.setValue(pnd_Date_Time_A_Pnd_Date_Time_N);                                                                                        //Natural: MOVE #DATE-TIME-N TO #RUNNING-DTE-TME-STR
                pnd_Date_Time_A.setValue(cwf_Support_Tbl_Normal_End_Dte_Tme);                                                                                             //Natural: MOVE CWF-SUPPORT-TBL.NORMAL-END-DTE-TME TO #DATE-TIME-A
                //*  ONE DAY ADVANCE
                pnd_Date_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),pnd_Date_Time_A);                                                                         //Natural: MOVE EDITED #DATE-TIME-A TO #DATE-T ( EM = YYYYMMDDHHIISST )
                pnd_Date_T_Result.compute(new ComputeParameters(false, pnd_Date_T_Result), (pnd_Date_T.add(864000)));                                                     //Natural: ASSIGN #DATE-T-RESULT := ( #DATE-T + 864000 )
                pnd_Date_Time_A.setValueEdited(pnd_Date_T_Result,new ReportEditMask("YYYYMMDDHHIISST"));                                                                  //Natural: MOVE EDITED #DATE-T-RESULT ( EM = YYYYMMDDHHIISST ) TO #DATE-TIME-A
                pnd_Running_Dte_Tme_End.setValue(pnd_Date_Time_A_Pnd_Date_Time_N);                                                                                        //Natural: MOVE #DATE-TIME-N TO #RUNNING-DTE-TME-END
                //*  WRITE /'******** Normal Running Dates before adjustment *******'
                //*        /'Start Date.............= ' NORMAL-START-DTE-TME
                //*        /'End   Date.............= ' NORMAL-END-DTE-TME
                //*        /'Running Start..........= ' #RUNNING-DTE-TME-STR
                //*        /'Running End............= ' #RUNNING-DTE-TME-END
                //*  STOP
                //*      MOVE *DATN TO #CURR-DATE-N
                //*      MOVE EDITED   #CURR-DATE-A        TO #DATE-D (EM=YYYYMMDD)
                //*      COMPUTE #DATE-D  =  (#DATE-D - 1)
                //*      MOVE EDITED #DATE-D (EM=YYYYMMDD) TO #RUNNING-DATE-END
                //*      MOVE 2359599                      TO #RUNNING-TIME-END
                //*      MOVE *ISN                         TO #ISN-CONTROL
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Start_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),pnd_Running_Dte_Tme_Str_Pnd_Running_Dte_Tme_Str_A);                                              //Natural: MOVE EDITED #RUNNING-DTE-TME-STR-A TO #START-T ( EM = YYYYMMDDHHIISST )
        pnd_End_T.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),pnd_Running_Dte_Tme_End_Pnd_Running_Dte_Tme_End_A);                                                //Natural: MOVE EDITED #RUNNING-DTE-TME-END-A TO #END-T ( EM = YYYYMMDDHHIISST )
        if (condition(pnd_End_T.lessOrEqual(pnd_Start_T)))                                                                                                                //Natural: IF #END-T LE #START-T
        {
            //*     IF  #RUNNING-DTE-TME-END LE #RUNNING-DTE-TME-STR
            getReports().write(0, NEWLINE,"********** Error *******",NEWLINE,"This is likely Second Run for Today..Use Special Run",NEWLINE,"End date can't be Equal or Less than Starting date"); //Natural: WRITE /'********** Error *******' /'This is likely Second Run for Today..Use Special Run' /'End date can"t be Equal or Less than Starting date'
            if (Global.isEscape()) return;
            DbsUtil.terminate(15);  if (true) return;                                                                                                                     //Natural: TERMINATE 15
        }                                                                                                                                                                 //Natural: END-IF
        PND_PND_L4850:                                                                                                                                                    //Natural: GET CWF-SUPPORT-TBL #ISN-CONTROL
        vw_cwf_Support_Tbl.readByID(pnd_Isn_Control.getLong(), "PND_PND_L4850");
        cwf_Support_Tbl_Calc_Start_Dte_Tme.setValue(pnd_Running_Dte_Tme_Str);                                                                                             //Natural: MOVE #RUNNING-DTE-TME-STR TO CWF-SUPPORT-TBL.CALC-START-DTE-TME
        cwf_Support_Tbl_Calc_End_Dte_Tme.setValue(pnd_Running_Dte_Tme_End);                                                                                               //Natural: MOVE #RUNNING-DTE-TME-END TO CWF-SUPPORT-TBL.CALC-END-DTE-TME
        vw_cwf_Support_Tbl.updateDBRow("PND_PND_L4850");                                                                                                                  //Natural: UPDATE ( ##L4850. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  WRITE /'******** Normal Running Dates after adjustment *******'
        //*        /'Start Date.............= ' #START-T(EM=YYYYMMDDHHIISST)
        //*        /'End   Date.............= ' #END-T(EM=YYYYMMDDHHIISST)
        //*      STOP
        //*  READ-RUN-CONTROL-RECORD
    }
    private void sub_Write_Wf_Record() throws Exception                                                                                                                   //Natural: WRITE-WF-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ===============================
        pnd_Wf_Rec_Cntr.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #WF-REC-CNTR
        getWorkFiles().write(1, false, pnd_Work_Record);                                                                                                                  //Natural: WRITE WORK FILE 1 #WORK-RECORD
        //*  DISPLAY  #WORK-RECORD
    }
    private void sub_Control_Record_Update() throws Exception                                                                                                             //Natural: CONTROL-RECORD-UPDATE
    {
        if (BLNatReinput.isReinput()) return;

        PND_PND_L5090:                                                                                                                                                    //Natural: GET CWF-SUPPORT-TBL #ISN-CONTROL
        vw_cwf_Support_Tbl.readByID(pnd_Isn_Control.getLong(), "PND_PND_L5090");
        if (condition(cwf_Support_Tbl_Run_Indicator.equals("S")))                                                                                                         //Natural: IF CWF-SUPPORT-TBL.RUN-INDICATOR = 'S'
        {
            cwf_Support_Tbl_Special_Record_Count.setValue(pnd_Wf_Rec_Cntr);                                                                                               //Natural: MOVE #WF-REC-CNTR TO SPECIAL-RECORD-COUNT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            cwf_Support_Tbl_Normal_Record_Count.setValue(pnd_Wf_Rec_Cntr);                                                                                                //Natural: MOVE #WF-REC-CNTR TO NORMAL-RECORD-COUNT
        }                                                                                                                                                                 //Natural: END-IF
        vw_cwf_Support_Tbl.updateDBRow("PND_PND_L5090");                                                                                                                  //Natural: UPDATE ( ##L5090. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_Write_Statistic_Report() throws Exception                                                                                                            //Natural: WRITE-STATISTIC-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(1, NEWLINE,"Program: ",Global.getPROGRAM(),"Reconciliation Process ","Date: ",Global.getDATN(),NEWLINE,"                      Extraction period      ", //Natural: WRITE ( 1 ) /'Program: ' *PROGRAM 'Reconciliation Process ' 'Date: ' *DATN /'                      Extraction period      ' /'            From :   ' #RUNNING-DTE-TME-STR /'              To :   ' #RUNNING-DTE-TME-END /'==========================================================' /'Number of XREF Read                    = ' #XREF-CNTR /'Number of XREF Accepted                = ' #XREF-CNTR-ACCPT /'Number of XREF Orphan                         = ' #XREF-ORPHAN /'Number of CWF-Document Read            = ' #CDOC-CNTR /'Number of CWF-Document Accepted        = ' #CDOC-CNTR-ACCPT /'Number of CWF-Document Orphan                 = ' #CDOC-ORPHAN /'Number of ICW-Document Read            = ' #IDOC-CNTR /'Number of ICW-Document Accepted        = ' #IDOC-CNTR-ACCPT /'Number of ICW-Document Orphan                 = ' #IDOC-ORPHAN /'Number of NCW-Document Read            = ' #NDOC-CNTR /'Number of NCW-Document Accepted        = ' #NDOC-CNTR-ACCPT /'Number of NCW-Document Orphan                 = ' #NDOC-ORPHAN /'Number of records written on Work File        = ' #WF-REC-CNTR
            NEWLINE,"            From :   ",pnd_Running_Dte_Tme_Str,NEWLINE,"              To :   ",pnd_Running_Dte_Tme_End,NEWLINE,"==========================================================",
            NEWLINE,"Number of XREF Read                    = ",pnd_Xref_Cntr,NEWLINE,"Number of XREF Accepted                = ",pnd_Xref_Cntr_Accpt,NEWLINE,
            "Number of XREF Orphan                         = ",pnd_Xref_Orphan,NEWLINE,"Number of CWF-Document Read            = ",pnd_Cdoc_Cntr,NEWLINE,
            "Number of CWF-Document Accepted        = ",pnd_Cdoc_Cntr_Accpt,NEWLINE,"Number of CWF-Document Orphan                 = ",pnd_Cdoc_Orphan,NEWLINE,
            "Number of ICW-Document Read            = ",pnd_Idoc_Cntr,NEWLINE,"Number of ICW-Document Accepted        = ",pnd_Idoc_Cntr_Accpt,NEWLINE,"Number of ICW-Document Orphan                 = ",
            pnd_Idoc_Orphan,NEWLINE,"Number of NCW-Document Read            = ",pnd_Ndoc_Cntr,NEWLINE,"Number of NCW-Document Accepted        = ",pnd_Ndoc_Cntr_Accpt,
            NEWLINE,"Number of NCW-Document Orphan                 = ",pnd_Ndoc_Orphan,NEWLINE,"Number of records written on Work File        = ",pnd_Wf_Rec_Cntr);
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=60");
    }
}
