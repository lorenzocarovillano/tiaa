/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:46:58 PM
**        * FROM NATURAL PROGRAM : Cwfb9121
************************************************************
**        * FILE NAME            : Cwfb9121.java
**        * CLASS NAME           : Cwfb9121
**        * INSTANCE NAME        : Cwfb9121
************************************************************
************************************************************************
* PROGRAM  : CWFB9121
* SYSTEM   : PROJCWF
* AUTHOR   : G. HONG
* FUNCTION : THIS PROGRAM CREATES/UPDATES THE ELECTRONIC FOLDER AND
*          : DOCUMENTS OF A CABINET FOR BERWYN REPORT FILES
*
* 06/24/11 : GH  CREATED
* 07/05/11 : GH  IF #DSSN HAS VALUE, THEN IT IS DISEASED SSN, OTHERWISE
*          :     #SSN IS DISEASED SSN.
* 07/18/11 : GH  REVERSED THE RAIDD/RAIDE AND RAIDDD/RAIDEE LOGIC
* 08/10/11 : GH  ADD LOGIC TO LOG MIT MULTI TIMES FOR BOTH 1ST & 2ND
*          :     ANNUITANTS DISEASED AT THE SAME PERIOD OF TIME.
* 08/30/13 : AB  ADD LOGIC TO MOVE MATCH INDICATOR TO NOTE-4 SO THAT
*          :     IN PGM CWFB9125 WILL PASS THE INDICATOR TO IAAN9125
*          :     FOR THE AUTOMATIC TERMINATION PROCESS IN IA
* 12/23/15 | JB  COR/NAAD RETIREMENT - ADD MQ OPEN/CLOSE  IA
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
***********************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb9121 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private PdaCdaobj pdaCdaobj;
    private PdaEfsa9120 pdaEfsa9120;
    private PdaEfsa902r pdaEfsa902r;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Suptb;
    private DbsField cwf_Suptb_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Suptb_Tbl_Table_Nme;
    private DbsField cwf_Suptb_Tbl_Key_Field;
    private DbsField cwf_Suptb_Tbl_Data_Field;
    private DbsField cwf_Suptb_Tbl_Actve_Ind;
    private DbsField cwf_Suptb_Tbl_Updte_Dte_Tme;
    private DbsField cwf_Suptb_Tbl_Updte_Dte;
    private DbsField cwf_Suptb_Tbl_Updte_Oprtr_Cde;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_1;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field;

    private DbsGroup pnd_Var;

    private DbsGroup pnd_Var_Pnd_Record;
    private DbsField pnd_Var_Pnd_Obs;
    private DbsField pnd_Var_Pnd_Ssn;
    private DbsField pnd_Var_Pnd_Last_Name;
    private DbsField pnd_Var_Pnd_First_Name;
    private DbsField pnd_Var_Pnd_User_Fiedl1;
    private DbsField pnd_Var_Pnd_User_Fiedl2;

    private DbsGroup pnd_Var__R_Field_2;
    private DbsField pnd_Var_Pnd_Contract_Nbr;
    private DbsField pnd_Var_Pnd_Option_Code;
    private DbsField pnd_Var_Pnd_X2;
    private DbsField pnd_Var_Pnd_Dssn;
    private DbsField pnd_Var_Pnd_D_Last_Name;
    private DbsField pnd_Var_Pnd_D_First_Name;
    private DbsField pnd_Var_Pnd_D_Dob;
    private DbsField pnd_Var_Pnd_D_Dod;
    private DbsField pnd_Var_Pnd_Match_Indicator;
    private DbsField pnd_Var_Pnd_Months_Since_Death;
    private DbsField pnd_Var_Pnd_Death_Source;
    private DbsField pnd_Var_Pnd_C_P_Ind;
    private DbsField pnd_Var_Pnd_Add_9;

    private DbsGroup pnd_Var__R_Field_3;
    private DbsField pnd_Var_Pnd_Pin;

    private DbsGroup pnd_Var__R_Field_4;
    private DbsField pnd_Var_Pnd_Tiaa_Pin;

    private DbsGroup pnd_Var__R_Field_5;
    private DbsField pnd_Var_Pnd_Tiaa_Pin_N7;
    private DbsField pnd_Var_Pnd_Tiaa_Pin_N5;
    private DbsField pnd_Var_Pnd_Origin_Code;
    private DbsField pnd_Var_Pnd_Temp_Pin;
    private DbsField pnd_Var_Pnd_Wpid;
    private DbsField pnd_Var_Pnd_Unit;
    private DbsField pnd_Var_Pnd_Msg;
    private DbsField pnd_Var_Pnd_Last_Run_Date;
    private DbsField pnd_Var_Pnd_Datn;

    private DbsGroup pnd_Var__R_Field_6;
    private DbsField pnd_Var_Pnd_Date;
    private DbsField pnd_Var_Pnd_Return_Date_Time;
    private DbsField pnd_Var_Pnd_Save_Pin;
    private DbsField pnd_Var_Pnd_Note_Line_1;
    private DbsField pnd_Var_Pnd_Note_Line_2;
    private DbsField pnd_Var_Pnd_Note_Line_3;
    private DbsField pnd_Var_Pnd_Note_Line_4;
    private DbsField pnd_Var_Pnd_Total_Record_Count;
    private DbsField pnd_Var_Pnd_Diseased_Ssn;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaEfsa9120 = new PdaEfsa9120(localVariables);
        pdaEfsa902r = new PdaEfsa902r(localVariables);

        // Local Variables

        vw_cwf_Suptb = new DataAccessProgramView(new NameInfo("vw_cwf_Suptb", "CWF-SUPTB"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Suptb_Tbl_Scrty_Level_Ind = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Suptb_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Suptb_Tbl_Table_Nme = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "TBL_TABLE_NME");
        cwf_Suptb_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Suptb_Tbl_Key_Field = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "TBL_KEY_FIELD");
        cwf_Suptb_Tbl_Data_Field = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 253, RepeatingFieldStrategy.None, 
            "TBL_DATA_FIELD");
        cwf_Suptb_Tbl_Actve_Ind = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Actve_Ind", "TBL-ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TBL_ACTVE_IND");
        cwf_Suptb_Tbl_Updte_Dte_Tme = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Updte_Dte_Tme", "TBL-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TBL_UPDTE_DTE_TME");
        cwf_Suptb_Tbl_Updte_Dte_Tme.setDdmHeader("UPDATE DATE/TIME");
        cwf_Suptb_Tbl_Updte_Dte = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Updte_Dte", "TBL-UPDTE-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TBL_UPDTE_DTE");
        cwf_Suptb_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        cwf_Suptb_Tbl_Updte_Oprtr_Cde = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Suptb_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        registerRecord(vw_cwf_Suptb);

        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_1", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_1.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_1.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_1.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);

        pnd_Var = localVariables.newGroupInRecord("pnd_Var", "#VAR");

        pnd_Var_Pnd_Record = pnd_Var.newGroupInGroup("pnd_Var_Pnd_Record", "#RECORD");
        pnd_Var_Pnd_Obs = pnd_Var_Pnd_Record.newFieldInGroup("pnd_Var_Pnd_Obs", "#OBS", FieldType.STRING, 5);
        pnd_Var_Pnd_Ssn = pnd_Var_Pnd_Record.newFieldInGroup("pnd_Var_Pnd_Ssn", "#SSN", FieldType.STRING, 9);
        pnd_Var_Pnd_Last_Name = pnd_Var_Pnd_Record.newFieldInGroup("pnd_Var_Pnd_Last_Name", "#LAST-NAME", FieldType.STRING, 12);
        pnd_Var_Pnd_First_Name = pnd_Var_Pnd_Record.newFieldInGroup("pnd_Var_Pnd_First_Name", "#FIRST-NAME", FieldType.STRING, 10);
        pnd_Var_Pnd_User_Fiedl1 = pnd_Var_Pnd_Record.newFieldInGroup("pnd_Var_Pnd_User_Fiedl1", "#USER-FIEDL1", FieldType.STRING, 10);
        pnd_Var_Pnd_User_Fiedl2 = pnd_Var_Pnd_Record.newFieldInGroup("pnd_Var_Pnd_User_Fiedl2", "#USER-FIEDL2", FieldType.STRING, 12);

        pnd_Var__R_Field_2 = pnd_Var_Pnd_Record.newGroupInGroup("pnd_Var__R_Field_2", "REDEFINE", pnd_Var_Pnd_User_Fiedl2);
        pnd_Var_Pnd_Contract_Nbr = pnd_Var__R_Field_2.newFieldInGroup("pnd_Var_Pnd_Contract_Nbr", "#CONTRACT-NBR", FieldType.STRING, 8);
        pnd_Var_Pnd_Option_Code = pnd_Var__R_Field_2.newFieldInGroup("pnd_Var_Pnd_Option_Code", "#OPTION-CODE", FieldType.STRING, 2);
        pnd_Var_Pnd_X2 = pnd_Var__R_Field_2.newFieldInGroup("pnd_Var_Pnd_X2", "#X2", FieldType.STRING, 2);
        pnd_Var_Pnd_Dssn = pnd_Var_Pnd_Record.newFieldInGroup("pnd_Var_Pnd_Dssn", "#DSSN", FieldType.STRING, 9);
        pnd_Var_Pnd_D_Last_Name = pnd_Var_Pnd_Record.newFieldInGroup("pnd_Var_Pnd_D_Last_Name", "#D-LAST-NAME", FieldType.STRING, 12);
        pnd_Var_Pnd_D_First_Name = pnd_Var_Pnd_Record.newFieldInGroup("pnd_Var_Pnd_D_First_Name", "#D-FIRST-NAME", FieldType.STRING, 10);
        pnd_Var_Pnd_D_Dob = pnd_Var_Pnd_Record.newFieldInGroup("pnd_Var_Pnd_D_Dob", "#D-DOB", FieldType.STRING, 10);
        pnd_Var_Pnd_D_Dod = pnd_Var_Pnd_Record.newFieldInGroup("pnd_Var_Pnd_D_Dod", "#D-DOD", FieldType.STRING, 10);
        pnd_Var_Pnd_Match_Indicator = pnd_Var_Pnd_Record.newFieldInGroup("pnd_Var_Pnd_Match_Indicator", "#MATCH-INDICATOR", FieldType.STRING, 3);
        pnd_Var_Pnd_Months_Since_Death = pnd_Var_Pnd_Record.newFieldInGroup("pnd_Var_Pnd_Months_Since_Death", "#MONTHS-SINCE-DEATH", FieldType.STRING, 
            4);
        pnd_Var_Pnd_Death_Source = pnd_Var_Pnd_Record.newFieldInGroup("pnd_Var_Pnd_Death_Source", "#DEATH-SOURCE", FieldType.STRING, 10);
        pnd_Var_Pnd_C_P_Ind = pnd_Var_Pnd_Record.newFieldInGroup("pnd_Var_Pnd_C_P_Ind", "#C-P-IND", FieldType.STRING, 1);
        pnd_Var_Pnd_Add_9 = pnd_Var_Pnd_Record.newFieldInGroup("pnd_Var_Pnd_Add_9", "#ADD-9", FieldType.STRING, 14);

        pnd_Var__R_Field_3 = pnd_Var_Pnd_Record.newGroupInGroup("pnd_Var__R_Field_3", "REDEFINE", pnd_Var_Pnd_Add_9);
        pnd_Var_Pnd_Pin = pnd_Var__R_Field_3.newFieldInGroup("pnd_Var_Pnd_Pin", "#PIN", FieldType.STRING, 12);

        pnd_Var__R_Field_4 = pnd_Var__R_Field_3.newGroupInGroup("pnd_Var__R_Field_4", "REDEFINE", pnd_Var_Pnd_Pin);
        pnd_Var_Pnd_Tiaa_Pin = pnd_Var__R_Field_4.newFieldInGroup("pnd_Var_Pnd_Tiaa_Pin", "#TIAA-PIN", FieldType.NUMERIC, 12);

        pnd_Var__R_Field_5 = pnd_Var__R_Field_3.newGroupInGroup("pnd_Var__R_Field_5", "REDEFINE", pnd_Var_Pnd_Pin);
        pnd_Var_Pnd_Tiaa_Pin_N7 = pnd_Var__R_Field_5.newFieldInGroup("pnd_Var_Pnd_Tiaa_Pin_N7", "#TIAA-PIN-N7", FieldType.NUMERIC, 7);
        pnd_Var_Pnd_Tiaa_Pin_N5 = pnd_Var__R_Field_5.newFieldInGroup("pnd_Var_Pnd_Tiaa_Pin_N5", "#TIAA-PIN-N5", FieldType.NUMERIC, 5);
        pnd_Var_Pnd_Origin_Code = pnd_Var__R_Field_3.newFieldInGroup("pnd_Var_Pnd_Origin_Code", "#ORIGIN-CODE", FieldType.STRING, 2);
        pnd_Var_Pnd_Temp_Pin = pnd_Var.newFieldInGroup("pnd_Var_Pnd_Temp_Pin", "#TEMP-PIN", FieldType.NUMERIC, 12);
        pnd_Var_Pnd_Wpid = pnd_Var.newFieldInGroup("pnd_Var_Pnd_Wpid", "#WPID", FieldType.STRING, 6);
        pnd_Var_Pnd_Unit = pnd_Var.newFieldInGroup("pnd_Var_Pnd_Unit", "#UNIT", FieldType.STRING, 8);
        pnd_Var_Pnd_Msg = pnd_Var.newFieldInGroup("pnd_Var_Pnd_Msg", "#MSG", FieldType.STRING, 60);
        pnd_Var_Pnd_Last_Run_Date = pnd_Var.newFieldInGroup("pnd_Var_Pnd_Last_Run_Date", "#LAST-RUN-DATE", FieldType.STRING, 8);
        pnd_Var_Pnd_Datn = pnd_Var.newFieldInGroup("pnd_Var_Pnd_Datn", "#DATN", FieldType.NUMERIC, 8);

        pnd_Var__R_Field_6 = pnd_Var.newGroupInGroup("pnd_Var__R_Field_6", "REDEFINE", pnd_Var_Pnd_Datn);
        pnd_Var_Pnd_Date = pnd_Var__R_Field_6.newFieldInGroup("pnd_Var_Pnd_Date", "#DATE", FieldType.STRING, 8);
        pnd_Var_Pnd_Return_Date_Time = pnd_Var.newFieldInGroup("pnd_Var_Pnd_Return_Date_Time", "#RETURN-DATE-TIME", FieldType.STRING, 15);
        pnd_Var_Pnd_Save_Pin = pnd_Var.newFieldInGroup("pnd_Var_Pnd_Save_Pin", "#SAVE-PIN", FieldType.NUMERIC, 12);
        pnd_Var_Pnd_Note_Line_1 = pnd_Var.newFieldInGroup("pnd_Var_Pnd_Note_Line_1", "#NOTE-LINE-1", FieldType.STRING, 70);
        pnd_Var_Pnd_Note_Line_2 = pnd_Var.newFieldInGroup("pnd_Var_Pnd_Note_Line_2", "#NOTE-LINE-2", FieldType.STRING, 70);
        pnd_Var_Pnd_Note_Line_3 = pnd_Var.newFieldInGroup("pnd_Var_Pnd_Note_Line_3", "#NOTE-LINE-3", FieldType.STRING, 70);
        pnd_Var_Pnd_Note_Line_4 = pnd_Var.newFieldInGroup("pnd_Var_Pnd_Note_Line_4", "#NOTE-LINE-4", FieldType.STRING, 70);
        pnd_Var_Pnd_Total_Record_Count = pnd_Var.newFieldInGroup("pnd_Var_Pnd_Total_Record_Count", "#TOTAL-RECORD-COUNT", FieldType.NUMERIC, 12);
        pnd_Var_Pnd_Diseased_Ssn = pnd_Var.newFieldInGroup("pnd_Var_Pnd_Diseased_Ssn", "#DISEASED-SSN", FieldType.STRING, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Suptb.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb9121() throws Exception
    {
        super("Cwfb9121");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("CWFB9121", onError);
        setupReports();
        //*  OPEN MQ TO FACILITATE MDM CALL
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        //*                                                                                                                                                               //Natural: FORMAT LS = 132 PS = 60 SG = OFF;//Natural: FORMAT ( 1 ) LS = 132 PS = 60 SG = OFF
        pnd_Var_Pnd_Datn.setValue(Global.getDATN());                                                                                                                      //Natural: ASSIGN #DATN := *DATN
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 RECORD #RECORD
        while (condition(getWorkFiles().read(1, pnd_Var_Pnd_Record)))
        {
            pnd_Var_Pnd_Total_Record_Count.nadd(1);                                                                                                                       //Natural: ADD 1 TO #TOTAL-RECORD-COUNT
            pnd_Var_Pnd_Unit.setValue("BPSSFWC");                                                                                                                         //Natural: ASSIGN #UNIT := 'BPSSFWC'
            if (condition(pnd_Var_Pnd_C_P_Ind.equals("C") || pnd_Var_Pnd_C_P_Ind.equals("c")))                                                                            //Natural: IF #C-P-IND = 'C' OR = 'c'
            {
                //*  GH 07/18/11
                //*  GH 07/18/11
                short decideConditionsMet547 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #OPTION-CODE;//Natural: VALUE '01'
                if (condition((pnd_Var_Pnd_Option_Code.equals("01"))))
                {
                    decideConditionsMet547++;
                    pnd_Var_Pnd_Wpid.setValue("RAIDA");                                                                                                                   //Natural: ASSIGN #WPID := 'RAIDA'
                }                                                                                                                                                         //Natural: VALUE '05', '06', '09'
                else if (condition((pnd_Var_Pnd_Option_Code.equals("05") || pnd_Var_Pnd_Option_Code.equals("06") || pnd_Var_Pnd_Option_Code.equals("09"))))
                {
                    decideConditionsMet547++;
                    pnd_Var_Pnd_Wpid.setValue("RAIDB");                                                                                                                   //Natural: ASSIGN #WPID := 'RAIDB'
                }                                                                                                                                                         //Natural: VALUE '03', '12', '15', '17'
                else if (condition((pnd_Var_Pnd_Option_Code.equals("03") || pnd_Var_Pnd_Option_Code.equals("12") || pnd_Var_Pnd_Option_Code.equals("15") 
                    || pnd_Var_Pnd_Option_Code.equals("17"))))
                {
                    decideConditionsMet547++;
                    pnd_Var_Pnd_Wpid.setValue("RAIDC");                                                                                                                   //Natural: ASSIGN #WPID := 'RAIDC'
                }                                                                                                                                                         //Natural: VALUE '07', '08', '10', '13'
                else if (condition((pnd_Var_Pnd_Option_Code.equals("07") || pnd_Var_Pnd_Option_Code.equals("08") || pnd_Var_Pnd_Option_Code.equals("10") 
                    || pnd_Var_Pnd_Option_Code.equals("13"))))
                {
                    decideConditionsMet547++;
                    pnd_Var_Pnd_Wpid.setValue("RAIDE");                                                                                                                   //Natural: ASSIGN #WPID := 'RAIDE'
                }                                                                                                                                                         //Natural: VALUE '04', '11', '14', '16'
                else if (condition((pnd_Var_Pnd_Option_Code.equals("04") || pnd_Var_Pnd_Option_Code.equals("11") || pnd_Var_Pnd_Option_Code.equals("14") 
                    || pnd_Var_Pnd_Option_Code.equals("16"))))
                {
                    decideConditionsMet547++;
                    pnd_Var_Pnd_Wpid.setValue("RAIDD");                                                                                                                   //Natural: ASSIGN #WPID := 'RAIDD'
                }                                                                                                                                                         //Natural: NONE VALUE
                else if (condition())
                {
                    pnd_Var_Pnd_Wpid.setValue("RAIDF");                                                                                                                   //Natural: ASSIGN #WPID := 'RAIDF'
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Var_Pnd_C_P_Ind.equals("P") || pnd_Var_Pnd_C_P_Ind.equals("p")))                                                                            //Natural: IF #C-P-IND = 'P' OR = 'p'
            {
                //*  GH 07/18/11
                //*  GH 07/18/11
                short decideConditionsMet566 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #OPTION-CODE;//Natural: VALUE '01'
                if (condition((pnd_Var_Pnd_Option_Code.equals("01"))))
                {
                    decideConditionsMet566++;
                    pnd_Var_Pnd_Wpid.setValue("RAIDAA");                                                                                                                  //Natural: ASSIGN #WPID := 'RAIDAA'
                }                                                                                                                                                         //Natural: VALUE '05', '06', '09'
                else if (condition((pnd_Var_Pnd_Option_Code.equals("05") || pnd_Var_Pnd_Option_Code.equals("06") || pnd_Var_Pnd_Option_Code.equals("09"))))
                {
                    decideConditionsMet566++;
                    pnd_Var_Pnd_Wpid.setValue("RAIDBB");                                                                                                                  //Natural: ASSIGN #WPID := 'RAIDBB'
                }                                                                                                                                                         //Natural: VALUE '03', '12', '15', '17'
                else if (condition((pnd_Var_Pnd_Option_Code.equals("03") || pnd_Var_Pnd_Option_Code.equals("12") || pnd_Var_Pnd_Option_Code.equals("15") 
                    || pnd_Var_Pnd_Option_Code.equals("17"))))
                {
                    decideConditionsMet566++;
                    pnd_Var_Pnd_Wpid.setValue("RAIDCC");                                                                                                                  //Natural: ASSIGN #WPID := 'RAIDCC'
                }                                                                                                                                                         //Natural: VALUE '07', '08', '10', '13'
                else if (condition((pnd_Var_Pnd_Option_Code.equals("07") || pnd_Var_Pnd_Option_Code.equals("08") || pnd_Var_Pnd_Option_Code.equals("10") 
                    || pnd_Var_Pnd_Option_Code.equals("13"))))
                {
                    decideConditionsMet566++;
                    pnd_Var_Pnd_Wpid.setValue("RAIDEE");                                                                                                                  //Natural: ASSIGN #WPID := 'RAIDEE'
                }                                                                                                                                                         //Natural: VALUE '04', '11', '14', '16'
                else if (condition((pnd_Var_Pnd_Option_Code.equals("04") || pnd_Var_Pnd_Option_Code.equals("11") || pnd_Var_Pnd_Option_Code.equals("14") 
                    || pnd_Var_Pnd_Option_Code.equals("16"))))
                {
                    decideConditionsMet566++;
                    pnd_Var_Pnd_Wpid.setValue("RAIDDD");                                                                                                                  //Natural: ASSIGN #WPID := 'RAIDDD'
                }                                                                                                                                                         //Natural: NONE VALUE
                else if (condition())
                {
                    pnd_Var_Pnd_Wpid.setValue("RAIDFF");                                                                                                                  //Natural: ASSIGN #WPID := 'RAIDFF'
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            //*  ONLY LOG THE SAME PIN ONCE FOR ALL CASES, BUT WILL LOG MULTI TIMES
            //*  FOR THE 1ST & 2ND ANNUITANT BOTH DISEASED AT THE SAME PERIOD
            //*   --- 08/10/2011 ---
            //*  PIN-EXP STARTS
            if (condition(pnd_Var_Pnd_Tiaa_Pin_N5.equals(getZero())))                                                                                                     //Natural: IF #TIAA-PIN-N5 = 0
            {
                pnd_Var_Pnd_Temp_Pin.reset();                                                                                                                             //Natural: RESET #TEMP-PIN
                pnd_Var_Pnd_Temp_Pin.setValue(pnd_Var_Pnd_Tiaa_Pin_N7);                                                                                                   //Natural: MOVE #TIAA-PIN-N7 TO #TEMP-PIN
                pnd_Var_Pnd_Pin.reset();                                                                                                                                  //Natural: RESET #PIN
                pnd_Var_Pnd_Pin.moveAll(pnd_Var_Pnd_Temp_Pin);                                                                                                            //Natural: MOVE ALL #TEMP-PIN TO #PIN
            }                                                                                                                                                             //Natural: END-IF
            //*  PIN-EXP ENDS
            if (condition(((pnd_Var_Pnd_Tiaa_Pin.equals(pnd_Var_Pnd_Save_Pin)) && (pnd_Var_Pnd_Match_Indicator.equals("BFL"))) || pnd_Var_Pnd_Tiaa_Pin.notEquals(pnd_Var_Pnd_Save_Pin))) //Natural: IF ( ( #TIAA-PIN = #SAVE-PIN ) AND ( #MATCH-INDICATOR = 'BFL' ) ) OR #TIAA-PIN NE #SAVE-PIN
            {
                //* **** IF #TIAA-PIN NE #SAVE-PIN  /* OLD CODE FOR THE REFERENCE.
                pnd_Var_Pnd_Save_Pin.setValue(pnd_Var_Pnd_Tiaa_Pin);                                                                                                      //Natural: MOVE #TIAA-PIN TO #SAVE-PIN
                //*  IF GROUP CONTRACT, THEN USING IT's WPID and Unit
                                                                                                                                                                          //Natural: PERFORM GROUP-CONTRACT-WPID-UNIT
                sub_Group_Contract_Wpid_Unit();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  LOG MIT REQUEST
                                                                                                                                                                          //Natural: PERFORM LOG-REQUEST
                sub_Log_Request();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  WRITE DATE INTO CWF TABLE, ONLY DO ONCE FOR REPORT 1
                                                                                                                                                                          //Natural: PERFORM UPDATE-TABLE-DATE
                sub_Update_Table_Date();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pnd_Var_Pnd_Record.reset();                                                                                                                                   //Natural: RESET #RECORD
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        pnd_Var_Pnd_Save_Pin.reset();                                                                                                                                     //Natural: RESET #SAVE-PIN #RECORD
        pnd_Var_Pnd_Record.reset();
        getReports().write(0, "TOTAL-RECORD-COUNT =",pnd_Var_Pnd_Total_Record_Count);                                                                                     //Natural: WRITE 'TOTAL-RECORD-COUNT =' #TOTAL-RECORD-COUNT
        if (Global.isEscape()) return;
        getReports().write(1, "TOTAL-RECORD-COUNT =",pnd_Var_Pnd_Total_Record_Count);                                                                                     //Natural: WRITE ( 1 ) 'TOTAL-RECORD-COUNT =' #TOTAL-RECORD-COUNT
        if (Global.isEscape()) return;
        //*  OPEN MQ TO FACILITATE MDM CALL
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GROUP-CONTRACT-WPID-UNIT
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOG-REQUEST
        //* *********************************************************************
        //*  POPULATE DATA TO LOG MIT FOR DIFFERENT WPID
        //* *****************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-TABLE-DATE
        //*  ******************************************************************
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Group_Contract_Wpid_Unit() throws Exception                                                                                                          //Natural: GROUP-CONTRACT-WPID-UNIT
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        //*  GROUP CONTRACT
        //*   (GROUP ANNUITY-DEATH CLAIM)
        if (condition(pnd_Var_Pnd_Origin_Code.equals("04") || pnd_Var_Pnd_Origin_Code.equals("06")))                                                                      //Natural: IF #ORIGIN-CODE = '04' OR = '06'
        {
            pnd_Var_Pnd_Wpid.setValue("TH DP");                                                                                                                           //Natural: ASSIGN #WPID := 'TH DP'
            pnd_Var_Pnd_Unit.setValue("ACTPS");                                                                                                                           //Natural: ASSIGN #UNIT := 'ACTPS'
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Log_Request() throws Exception                                                                                                                       //Natural: LOG-REQUEST
    {
        if (BLNatReinput.isReinput()) return;

        pdaEfsa9120.getEfsa9120_Action().setValue("AR");                                                                                                                  //Natural: ASSIGN EFSA9120.ACTION := 'AR'
        pdaEfsa9120.getEfsa9120_Cabinet_Prefix().setValue("P");                                                                                                           //Natural: ASSIGN EFSA9120.CABINET-PREFIX := 'P'
        pdaEfsa9120.getEfsa9120_Pin_Nbr().setValue(pnd_Var_Pnd_Tiaa_Pin);                                                                                                 //Natural: ASSIGN EFSA9120.PIN-NBR := #TIAA-PIN
        pdaEfsa9120.getEfsa9120_Tiaa_Rcvd_Dte().getValue(1).setValue(Global.getDATN());                                                                                   //Natural: ASSIGN EFSA9120.TIAA-RCVD-DTE ( 1 ) := *DATN
        pdaEfsa9120.getEfsa9120_Empl_Oprtr_Cde().getValue(1).setValue(" ");                                                                                               //Natural: ASSIGN EFSA9120.EMPL-OPRTR-CDE ( 1 ) := ' '
        pdaEfsa9120.getEfsa9120_Wpid().getValue(1).setValue(pnd_Var_Pnd_Wpid);                                                                                            //Natural: ASSIGN EFSA9120.WPID ( 1 ) := #WPID
        pdaEfsa9120.getEfsa9120_Wpid_Validate_Ind().getValue(1).setValue("Y");                                                                                            //Natural: ASSIGN EFSA9120.WPID-VALIDATE-IND ( 1 ) := 'Y'
        pdaEfsa9120.getEfsa9120_Unit_Cde().getValue(1).setValue(pnd_Var_Pnd_Unit);                                                                                        //Natural: ASSIGN EFSA9120.UNIT-CDE ( 1 ) := #UNIT
        pdaEfsa9120.getEfsa9120_Status_Cde().getValue(1).setValue("1000");                                                                                                //Natural: ASSIGN EFSA9120.STATUS-CDE ( 1 ) := '1000'
        pdaEfsa9120.getEfsa9120_Corp_Status().getValue(1).setValue(0);                                                                                                    //Natural: ASSIGN EFSA9120.CORP-STATUS ( 1 ) := 0
        pdaEfsa9120.getEfsa9120_Rqst_Entry_Op_Cde().setValue("BATCHCWF");                                                                                                 //Natural: ASSIGN EFSA9120.RQST-ENTRY-OP-CDE := 'BATCHCWF'
        pdaEfsa9120.getEfsa9120_Rqst_Origin_Cde().setValue("I");                                                                                                          //Natural: ASSIGN EFSA9120.RQST-ORIGIN-CDE := 'I'
        pdaEfsa9120.getEfsa9120_Rqst_Origin_Unit_Cde().setValue(pnd_Var_Pnd_Unit);                                                                                        //Natural: ASSIGN EFSA9120.RQST-ORIGIN-UNIT-CDE := #UNIT
        pdaEfsa9120.getEfsa9120_System().setValue("MIT");                                                                                                                 //Natural: ASSIGN EFSA9120.SYSTEM := 'MIT'
        DbsUtil.callnat(Efsn9120.class , getCurrentProcessState(), pdaEfsa9120.getEfsa9120(), pdaEfsa9120.getEfsa9120_Id(), pdaEfsa902r.getEfsa902r(),                    //Natural: CALLNAT 'EFSN9120' EFSA9120 EFSA9120-ID EFSA902R CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().notEquals(" ") || pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().notEquals(" ") ||                     //Natural: IF MSG-INFO-SUB.##MSG NE ' ' OR ##ERROR-FIELD NE ' ' OR ##MSG-NR NE 0
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().notEquals(getZero())))
        {
            pnd_Var_Pnd_Msg.setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                                          //Natural: MOVE ##MSG TO #MSG
            getReports().write(0, "Error Message Logging:",pdaEfsa9120.getEfsa9120_Pin_Nbr(),pnd_Var_Pnd_Msg);                                                            //Natural: WRITE 'Error Message Logging:' EFSA9120.PIN-NBR #MSG
            if (Global.isEscape()) return;
            getReports().write(1, "Error Message Logging:",pdaEfsa9120.getEfsa9120_Pin_Nbr(),pnd_Var_Pnd_Msg);                                                            //Natural: WRITE ( 1 ) 'Error Message Logging:' EFSA9120.PIN-NBR #MSG
            if (Global.isEscape()) return;
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            pnd_Var_Pnd_Return_Date_Time.setValue(pdaEfsa9120.getEfsa9120_Return_Date_Time().getValue(1));                                                                //Natural: MOVE EFSA9120.RETURN-DATE-TIME ( 1 ) TO #RETURN-DATE-TIME
            pnd_Var_Pnd_Note_Line_1.setValue(pnd_Var_Pnd_D_Dod);                                                                                                          //Natural: MOVE #D-DOD TO #NOTE-LINE-1
            //*  IF #DSSN HAS VALUE, THEN IT IS DISEASED SSN, OTHERWISE
            //*  THE VALUE OF THE #SSN IS DISEASED SSN.
            if (condition(pnd_Var_Pnd_Dssn.notEquals(" ")))                                                                                                               //Natural: IF #DSSN NE ' ' THEN
            {
                pnd_Var_Pnd_Diseased_Ssn.setValue(pnd_Var_Pnd_Dssn);                                                                                                      //Natural: ASSIGN #DISEASED-SSN := #DSSN
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Var_Pnd_Diseased_Ssn.setValue(pnd_Var_Pnd_Ssn);                                                                                                       //Natural: ASSIGN #DISEASED-SSN := #SSN
            }                                                                                                                                                             //Natural: END-IF
            pnd_Var_Pnd_Note_Line_2.setValue(DbsUtil.compress(pnd_Var_Pnd_Diseased_Ssn, "/", pnd_Var_Pnd_D_Last_Name, ",", pnd_Var_Pnd_D_First_Name, "/",                 //Natural: COMPRESS #DISEASED-SSN '/' #D-LAST-NAME ',' #D-FIRST-NAME '/' #D-DOB INTO #NOTE-LINE-2
                pnd_Var_Pnd_D_Dob));
            pnd_Var_Pnd_Note_Line_3.setValue(DbsUtil.compress(pnd_Var_Pnd_Contract_Nbr, "/", pnd_Var_Pnd_Death_Source));                                                  //Natural: COMPRESS #CONTRACT-NBR '/' #DEATH-SOURCE INTO #NOTE-LINE-3
            pdaEfsa9120.getEfsa9120_Action().setValue("AD");                                                                                                              //Natural: ASSIGN EFSA9120.ACTION := 'AD'
            pdaEfsa9120.getEfsa9120_Rqst_Id().getValue(1).setValue(pnd_Var_Pnd_Return_Date_Time);                                                                         //Natural: ASSIGN EFSA9120.RQST-ID ( 1 ) := #RETURN-DATE-TIME
            pdaEfsa9120.getEfsa9120_Rqst_Log_Dte_Tme().getValue(1).setValue(pnd_Var_Pnd_Return_Date_Time);                                                                //Natural: ASSIGN EFSA9120.RQST-LOG-DTE-TME ( 1 ) := #RETURN-DATE-TIME
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(1).setValue(pnd_Var_Pnd_Note_Line_1);                                                                             //Natural: ASSIGN EFSA9120.DOC-TEXT ( 1 ) := #NOTE-LINE-1
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(2).setValue(pnd_Var_Pnd_Note_Line_2);                                                                             //Natural: ASSIGN EFSA9120.DOC-TEXT ( 2 ) := #NOTE-LINE-2
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(3).setValue(pnd_Var_Pnd_Note_Line_3);                                                                             //Natural: ASSIGN EFSA9120.DOC-TEXT ( 3 ) := #NOTE-LINE-3
            //* ************** ADDED 8/30/13 ***********************************
            //*  ADDED LOGIC FOR AUTOMATIC TERMINATION
            //*  MANDATORY
            //*  MANDATORY
            //*  MANDATORY
            //*  MANDATORY
            pnd_Var_Pnd_Note_Line_4.setValue(pnd_Var_Pnd_Match_Indicator);                                                                                                //Natural: MOVE #MATCH-INDICATOR TO #NOTE-LINE-4
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(4).setValue(pnd_Var_Pnd_Note_Line_4);                                                                             //Natural: ASSIGN EFSA9120.DOC-TEXT ( 4 ) := #NOTE-LINE-4
            pdaEfsa9120.getEfsa9120_Doc_Category().setValue("I");                                                                                                         //Natural: ASSIGN EFSA9120.DOC-CATEGORY := 'I'
            pdaEfsa9120.getEfsa9120_Doc_Class().setValue("NOT");                                                                                                          //Natural: ASSIGN EFSA9120.DOC-CLASS := 'NOT'
            pdaEfsa9120.getEfsa9120_Doc_Direction().setValue("N");                                                                                                        //Natural: ASSIGN EFSA9120.DOC-DIRECTION := 'N'
            pdaEfsa9120.getEfsa9120_Doc_Format_Cde().setValue("T");                                                                                                       //Natural: ASSIGN EFSA9120.DOC-FORMAT-CDE := 'T'
            pdaEfsa9120.getEfsa9120_Doc_Retention_Cde().setValue("P");                                                                                                    //Natural: ASSIGN EFSA9120.DOC-RETENTION-CDE := 'P'
            DbsUtil.callnat(Efsn9120.class , getCurrentProcessState(), pdaEfsa9120.getEfsa9120(), pdaEfsa9120.getEfsa9120_Id(), pdaEfsa902r.getEfsa902r(),                //Natural: CALLNAT 'EFSN9120' EFSA9120 EFSA9120-ID EFSA902R CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
                pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
            if (condition(Global.isEscape())) return;
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().notEquals(" ") || pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().notEquals(" ")                    //Natural: IF MSG-INFO-SUB.##MSG NE ' ' OR ##ERROR-FIELD NE ' ' OR ##MSG-NR NE 0
                || pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().notEquals(getZero())))
            {
                pnd_Var_Pnd_Msg.setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                                      //Natural: MOVE ##MSG TO #MSG
                getReports().write(0, "Error Message NOTE:",pdaEfsa9120.getEfsa9120_Pin_Nbr(),pnd_Var_Pnd_Msg);                                                           //Natural: WRITE 'Error Message NOTE:' EFSA9120.PIN-NBR #MSG
                if (Global.isEscape()) return;
                getReports().write(1, "Error Message NOTE:",pdaEfsa9120.getEfsa9120_Pin_Nbr(),pnd_Var_Pnd_Msg);                                                           //Natural: WRITE ( 1 ) 'Error Message NOTE:' EFSA9120.PIN-NBR #MSG
                if (Global.isEscape()) return;
                getCurrentProcessState().getDbConv().dbRollback();                                                                                                        //Natural: BACKOUT TRANSACTION
                //*  BOTTOM /* IMMEDIATE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            pnd_Var_Pnd_Return_Date_Time.reset();                                                                                                                         //Natural: RESET #RETURN-DATE-TIME EFSA9120
            pdaEfsa9120.getEfsa9120().reset();
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Update_Table_Date() throws Exception                                                                                                                 //Natural: UPDATE-TABLE-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind.setValue("S ");                                                                                                         //Natural: MOVE 'S ' TO #TBL-SCRTY-LEVEL-IND
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme.setValue("BERWYN-MIT-LOG-DATE");                                                                                              //Natural: MOVE 'BERWYN-MIT-LOG-DATE' TO #TBL-TABLE-NME
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field.setValue("LAST-RUN-DATE");                                                                                                    //Natural: MOVE 'LAST-RUN-DATE' TO #TBL-KEY-FIELD
        vw_cwf_Suptb.startDatabaseRead                                                                                                                                    //Natural: READ ( 1 ) CWF-SUPTB WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
        (
        "READ02",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") },
        1
        );
        READ02:
        while (condition(vw_cwf_Suptb.readNextRow("READ02")))
        {
            if (condition(cwf_Suptb_Tbl_Scrty_Level_Ind.notEquals(pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind) || cwf_Suptb_Tbl_Table_Nme.notEquals(pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme)  //Natural: IF TBL-SCRTY-LEVEL-IND NE #TBL-SCRTY-LEVEL-IND OR TBL-TABLE-NME NE #TBL-TABLE-NME OR TBL-KEY-FIELD NE #TBL-KEY-FIELD
                || cwf_Suptb_Tbl_Key_Field.notEquals(pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field)))
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            cwf_Suptb_Tbl_Data_Field.setValue(pnd_Var_Pnd_Date);                                                                                                          //Natural: MOVE #DATE TO TBL-DATA-FIELD
            vw_cwf_Suptb.updateDBRow("READ02");                                                                                                                           //Natural: UPDATE
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Natural Error", Global.getERROR_NR(), "on line", Global.getERROR_LINE(),                     //Natural: COMPRESS 'Natural Error' *ERROR-NR 'on line' *ERROR-LINE 'of' *PROGRAM INTO MSG-INFO-SUB.##MSG
            "of", Global.getPROGRAM()));
        getReports().write(0, pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                                                 //Natural: WRITE MSG-INFO-SUB.##MSG
        if (condition(true)) return;                                                                                                                                      //Natural: ESCAPE ROUTINE
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=60 SG=OFF");
        Global.format(1, "LS=132 PS=60 SG=OFF");
    }
}
