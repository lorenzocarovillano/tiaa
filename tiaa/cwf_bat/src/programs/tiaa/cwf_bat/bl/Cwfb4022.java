/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:32:17 PM
**        * FROM NATURAL PROGRAM : Cwfb4022
************************************************************
**        * FILE NAME            : Cwfb4022.java
**        * CLASS NAME           : Cwfb4022
**        * INSTANCE NAME        : Cwfb4022
************************************************************
************************************************************************
* PROGRAM  : CWFB4022
* SYSTEM   : CRPCWF
* TITLE    : LOB/COMPANY/PRODUCT EXTRA
* GENERATED: SEP 15,95 AT 11:30 AM
* FUNCTION : THIS PROGRAM READS THE CWF-WP-LOB--TBL AND WRITES
*            A WORK FILE TO BE DOWNLOADED TO THE SQL SERVER.
*
*
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb4022 extends BLNatBase
{
    // Data Areas
    private LdaCdbatxa ldaCdbatxa;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Cur_Lang;
    private DbsField pnd_Program;

    private DataAccessProgramView vw_cwf_Wp_Lob_Tbl;
    private DbsField cwf_Wp_Lob_Tbl_Lob_Cmpny_Prdct_Cde;
    private DbsField cwf_Wp_Lob_Tbl_Lob_Cmpny_Prdct_Nme;
    private DbsField cwf_Wp_Lob_Tbl_Actve_Ind;

    private DbsGroup cwf_Wp_Lob_Tbl__R_Field_1;
    private DbsField cwf_Wp_Lob_Tbl_Fill_1;
    private DbsField cwf_Wp_Lob_Tbl_Actve_Ind_2_2;
    private DbsField cwf_Wp_Lob_Tbl_Entry_Dte_Tme;
    private DbsField cwf_Wp_Lob_Tbl_Entry_Oprtr_Cde;
    private DbsField cwf_Wp_Lob_Tbl_Updte_Dte;
    private DbsField cwf_Wp_Lob_Tbl_Updte_Dte_Tme;
    private DbsField cwf_Wp_Lob_Tbl_Updte_Oprtr_Cde;
    private DbsField cwf_Wp_Lob_Tbl_Dlte_Dte_Tme;
    private DbsField cwf_Wp_Lob_Tbl_Dlte_Oprtr_Cde;
    private DbsField pnd_Header1_1;
    private DbsField pnd_Header1_2;

    private DbsGroup work_Record;
    private DbsField work_Record_Lob_Cmpny_Prdct_Cde;
    private DbsField work_Record_Pnd_C1;
    private DbsField work_Record_Lob_Cmpny_Prdct_Nme;

    private DbsGroup pnd_Counters;
    private DbsField pnd_Counters_Pnd_Rex_Read;
    private DbsField pnd_Counters_Pnd_Rex_Written;

    private DbsGroup pnd_Report_Fields;
    private DbsField pnd_Report_Fields_Pnd_Error_Msg;

    private DbsGroup pnd_Contstants;
    private DbsField pnd_Contstants_Pnd_Delimiter;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCdbatxa = new LdaCdbatxa();
        registerRecord(ldaCdbatxa);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Cur_Lang = localVariables.newFieldInRecord("pnd_Cur_Lang", "#CUR-LANG", FieldType.PACKED_DECIMAL, 1);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);

        vw_cwf_Wp_Lob_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Wp_Lob_Tbl", "CWF-WP-LOB-TBL"), "CWF_WP_LOB_TBL", "CWF_PROFILE");
        cwf_Wp_Lob_Tbl_Lob_Cmpny_Prdct_Cde = vw_cwf_Wp_Lob_Tbl.getRecord().newFieldInGroup("cwf_Wp_Lob_Tbl_Lob_Cmpny_Prdct_Cde", "LOB-CMPNY-PRDCT-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "LOB_CMPNY_PRDCT_CDE");
        cwf_Wp_Lob_Tbl_Lob_Cmpny_Prdct_Cde.setDdmHeader("CO./LOB//PRODUCT");
        cwf_Wp_Lob_Tbl_Lob_Cmpny_Prdct_Nme = vw_cwf_Wp_Lob_Tbl.getRecord().newFieldInGroup("cwf_Wp_Lob_Tbl_Lob_Cmpny_Prdct_Nme", "LOB-CMPNY-PRDCT-NME", 
            FieldType.STRING, 45, RepeatingFieldStrategy.None, "LOB_CMPNY_PRDCT_NME");
        cwf_Wp_Lob_Tbl_Lob_Cmpny_Prdct_Nme.setDdmHeader("NAME");
        cwf_Wp_Lob_Tbl_Actve_Ind = vw_cwf_Wp_Lob_Tbl.getRecord().newFieldInGroup("cwf_Wp_Lob_Tbl_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTVE_IND");

        cwf_Wp_Lob_Tbl__R_Field_1 = vw_cwf_Wp_Lob_Tbl.getRecord().newGroupInGroup("cwf_Wp_Lob_Tbl__R_Field_1", "REDEFINE", cwf_Wp_Lob_Tbl_Actve_Ind);
        cwf_Wp_Lob_Tbl_Fill_1 = cwf_Wp_Lob_Tbl__R_Field_1.newFieldInGroup("cwf_Wp_Lob_Tbl_Fill_1", "FILL-1", FieldType.STRING, 1);
        cwf_Wp_Lob_Tbl_Actve_Ind_2_2 = cwf_Wp_Lob_Tbl__R_Field_1.newFieldInGroup("cwf_Wp_Lob_Tbl_Actve_Ind_2_2", "ACTVE-IND-2-2", FieldType.STRING, 1);
        cwf_Wp_Lob_Tbl_Entry_Dte_Tme = vw_cwf_Wp_Lob_Tbl.getRecord().newFieldInGroup("cwf_Wp_Lob_Tbl_Entry_Dte_Tme", "ENTRY-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ENTRY_DTE_TME");
        cwf_Wp_Lob_Tbl_Entry_Dte_Tme.setDdmHeader("ENTRY/DT-TM");
        cwf_Wp_Lob_Tbl_Entry_Oprtr_Cde = vw_cwf_Wp_Lob_Tbl.getRecord().newFieldInGroup("cwf_Wp_Lob_Tbl_Entry_Oprtr_Cde", "ENTRY-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ENTRY_OPRTR_CDE");
        cwf_Wp_Lob_Tbl_Entry_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Wp_Lob_Tbl_Updte_Dte = vw_cwf_Wp_Lob_Tbl.getRecord().newFieldInGroup("cwf_Wp_Lob_Tbl_Updte_Dte", "UPDTE-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "UPDTE_DTE");
        cwf_Wp_Lob_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        cwf_Wp_Lob_Tbl_Updte_Dte_Tme = vw_cwf_Wp_Lob_Tbl.getRecord().newFieldInGroup("cwf_Wp_Lob_Tbl_Updte_Dte_Tme", "UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "UPDTE_DTE_TME");
        cwf_Wp_Lob_Tbl_Updte_Oprtr_Cde = vw_cwf_Wp_Lob_Tbl.getRecord().newFieldInGroup("cwf_Wp_Lob_Tbl_Updte_Oprtr_Cde", "UPDTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "UPDTE_OPRTR_CDE");
        cwf_Wp_Lob_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPER");
        cwf_Wp_Lob_Tbl_Dlte_Dte_Tme = vw_cwf_Wp_Lob_Tbl.getRecord().newFieldInGroup("cwf_Wp_Lob_Tbl_Dlte_Dte_Tme", "DLTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "DLTE_DTE_TME");
        cwf_Wp_Lob_Tbl_Dlte_Dte_Tme.setDdmHeader("DELETE/DT-TM");
        cwf_Wp_Lob_Tbl_Dlte_Oprtr_Cde = vw_cwf_Wp_Lob_Tbl.getRecord().newFieldInGroup("cwf_Wp_Lob_Tbl_Dlte_Oprtr_Cde", "DLTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "DLTE_OPRTR_CDE");
        cwf_Wp_Lob_Tbl_Dlte_Oprtr_Cde.setDdmHeader("DELETE/OPER");
        registerRecord(vw_cwf_Wp_Lob_Tbl);

        pnd_Header1_1 = localVariables.newFieldInRecord("pnd_Header1_1", "#HEADER1-1", FieldType.STRING, 50);
        pnd_Header1_2 = localVariables.newFieldInRecord("pnd_Header1_2", "#HEADER1-2", FieldType.STRING, 50);

        work_Record = localVariables.newGroupInRecord("work_Record", "WORK-RECORD");
        work_Record_Lob_Cmpny_Prdct_Cde = work_Record.newFieldInGroup("work_Record_Lob_Cmpny_Prdct_Cde", "LOB-CMPNY-PRDCT-CDE", FieldType.STRING, 2);
        work_Record_Pnd_C1 = work_Record.newFieldInGroup("work_Record_Pnd_C1", "#C1", FieldType.STRING, 1);
        work_Record_Lob_Cmpny_Prdct_Nme = work_Record.newFieldInGroup("work_Record_Lob_Cmpny_Prdct_Nme", "LOB-CMPNY-PRDCT-NME", FieldType.STRING, 45);

        pnd_Counters = localVariables.newGroupInRecord("pnd_Counters", "#COUNTERS");
        pnd_Counters_Pnd_Rex_Read = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Rex_Read", "#REX-READ", FieldType.PACKED_DECIMAL, 5);
        pnd_Counters_Pnd_Rex_Written = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Rex_Written", "#REX-WRITTEN", FieldType.PACKED_DECIMAL, 5);

        pnd_Report_Fields = localVariables.newGroupInRecord("pnd_Report_Fields", "#REPORT-FIELDS");
        pnd_Report_Fields_Pnd_Error_Msg = pnd_Report_Fields.newFieldInGroup("pnd_Report_Fields_Pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 60);

        pnd_Contstants = localVariables.newGroupInRecord("pnd_Contstants", "#CONTSTANTS");
        pnd_Contstants_Pnd_Delimiter = pnd_Contstants.newFieldInGroup("pnd_Contstants_Pnd_Delimiter", "#DELIMITER", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Wp_Lob_Tbl.reset();

        ldaCdbatxa.initializeValues();

        localVariables.reset();
        pnd_Header1_1.setInitialValue("      CWF LOB/Company/Product Extract Report");
        pnd_Header1_2.setInitialValue("                        �");
        pnd_Contstants_Pnd_Delimiter.setInitialValue("@");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb4022() throws Exception
    {
        super("Cwfb4022");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  DEFINE PRINTERS AND FORMATS
        getReports().definePrinter(2, "NOT DEFINED");                                                                                                                     //Natural: DEFINE PRINTER ( 1 )
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: ASSIGN #PROGRAM = *PROGRAM
        //*  MAP THE CURRENT LANGUAGE CODE TO THE CODE IN THE BATCH MODEL TEXT LDA.
        pnd_Cur_Lang.setValue(Global.getLANGUAGE());                                                                                                                      //Natural: ASSIGN #CUR-LANG = *LANGUAGE
        pnd_Cur_Lang.setValue(ldaCdbatxa.getCdbatxa_Pnd_Lang_Map().getValue(pnd_Cur_Lang));                                                                               //Natural: ASSIGN #CUR-LANG = CDBATXA.#LANG-MAP ( #CUR-LANG )
        //* *SAG DEFINE EXIT START-OF-PROGRAM
        //* *SAG END-EXIT
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH' THEN
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA = 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        //* *SAG DEFINE EXIT BEFORE-READ
        //* *SAG END-EXIT
        //*  PRIMARY FILE
        vw_cwf_Wp_Lob_Tbl.startDatabaseRead                                                                                                                               //Natural: READ CWF-WP-LOB-TBL BY LOB-CMPNY-PRDCT-KEY
        (
        "READ_PRIME",
        new Oc[] { new Oc("LOB_CMPNY_PRDCT_KEY", "ASC") }
        );
        READ_PRIME:
        while (condition(vw_cwf_Wp_Lob_Tbl.readNextRow("READ_PRIME")))
        {
            //* *SAG DEFINE EXIT PRIME-WRITE-FIELDS
            if (condition(cwf_Wp_Lob_Tbl_Dlte_Oprtr_Cde.greater(" ")))                                                                                                    //Natural: REJECT IF DLTE-OPRTR-CDE GT ' '
            {
                continue;
            }
            pnd_Counters_Pnd_Rex_Read.nadd(1);                                                                                                                            //Natural: ADD 1 TO #REX-READ
                                                                                                                                                                          //Natural: PERFORM FORMAT-RECORD
            sub_Format_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-FILE
            sub_Write_Work_File();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* *SAG END-EXIT
            //*  PRIMARY FILE.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* *SAG DEFINE EXIT END-OF-PROGRAM
        getReports().write(1, ReportOption.NOTITLE,"Total Number of LOB/COMPANY/PRODUCT Table Records Read:",pnd_Counters_Pnd_Rex_Read,NEWLINE,"Total Number of Work File Records Written             :",pnd_Counters_Pnd_Rex_Written,NEWLINE,NEWLINE,new  //Natural: WRITE ( 1 ) 'Total Number of LOB/COMPANY/PRODUCT Table Records Read:' #REX-READ / 'Total Number of Work File Records Written             :' #REX-WRITTEN //56T 'End of Report'
            TabSetting(56),"End of Report");
        if (Global.isEscape()) return;
        //* *SAG END-EXIT
        //* *SAG DEFINE EXIT MISCELLANEOUS-SUBROUTINES
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-RECORD
        //* *************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WORK-FILE
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT
        //* *****************************
        //* *************
        //* *SAG END-EXIT
    }
    private void sub_Format_Record() throws Exception                                                                                                                     //Natural: FORMAT-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        work_Record.reset();                                                                                                                                              //Natural: RESET WORK-RECORD
        work_Record.setValuesByName(vw_cwf_Wp_Lob_Tbl);                                                                                                                   //Natural: MOVE BY NAME CWF-WP-LOB-TBL TO WORK-RECORD
        work_Record_Pnd_C1.setValue(pnd_Contstants_Pnd_Delimiter);                                                                                                        //Natural: MOVE #DELIMITER TO #C1
        //* *************
        //*  FORMAT-RECORD
    }
    private void sub_Write_Work_File() throws Exception                                                                                                                   //Natural: WRITE-WORK-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getWorkFiles().write(1, false, work_Record);                                                                                                                      //Natural: WRITE WORK FILE 1 WORK-RECORD
        pnd_Counters_Pnd_Rex_Written.nadd(1);                                                                                                                             //Natural: ADD 1 TO #REX-WRITTEN
        //*  PERFORM WRITE-REPORT
        //* ********************************
        //*  WRITE-WORK-FILE
    }
    private void sub_Write_Report() throws Exception                                                                                                                      //Natural: WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(1, "Lob Cmpny/Prd Code", new FieldAttributes ("AD=I"),                                                                                       //Natural: DISPLAY ( 1 ) 'Lob Cmpny/Prd Code' ( I ) CWF-WP-LOB-TBL.LOB-CMPNY-PRDCT-CDE ( LC = ���� ) '/Name' ( I ) CWF-WP-LOB-TBL.LOB-CMPNY-PRDCT-NME
        		cwf_Wp_Lob_Tbl_Lob_Cmpny_Prdct_Cde, new FieldAttributes("LC=����"),"/Name", new FieldAttributes ("AD=I"),
        		cwf_Wp_Lob_Tbl_Lob_Cmpny_Prdct_Nme);
        if (Global.isEscape()) return;
        //* *************
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(41),pnd_Header1_1,new TabSetting(124),ldaCdbatxa.getCdbatxa_Pnd_Page_Txt().getValue(pnd_Cur_Lang),getReports().getPageNumberDbs(1),  //Natural: WRITE ( 1 ) NOTITLE *PROGRAM 41T #HEADER1-1 124T CDBATXA.#PAGE-TXT ( #CUR-LANG ) *PAGE-NUMBER ( 1 ) ( NL = 4 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) #HEADER1-2 124T *TIMX ( EM = HH':'II' 'AP )
                        new NumericLength (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),pnd_Header1_2,new 
                        TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"));
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");

        getReports().setDisplayColumns(1, "Lob Cmpny/Prd Code", new FieldAttributes ("AD=I"),
        		cwf_Wp_Lob_Tbl_Lob_Cmpny_Prdct_Cde, new FieldAttributes("LC=����"),"/Name", new FieldAttributes ("AD=I"),
        		cwf_Wp_Lob_Tbl_Lob_Cmpny_Prdct_Nme);
    }
}
