/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:06:11 PM
**        * FROM NATURAL PROGRAM : Efsb2110
************************************************************
**        * FILE NAME            : Efsb2110.java
**        * CLASS NAME           : Efsb2110
**        * INSTANCE NAME        : Efsb2110
************************************************************
**********************************************************************
* PROGRAM NAME: EFSB2110
* WRITTEN BY  : J HOFSTETTER
* DESCRIPTION : REPORT 'Document Statistics for Closed Work Requests'
*               . READS WORK FILE FROM EFSB2100 SORTED BY WPID AND UNIT
*               . PRINTS REPORT BY WPID AND UNIT
* DATE WRITTEN: JUL 20, 1995
* MODIFIED    :
* ------------
* 02/06/96 JHH - CHANGED SEQUENCE OF #WORK-SYSTEM TESTS; IMAGE LAST
* 04/24/2017 PIN-EXP - PIN EXPANSION PROJECT AUG 2017
**********************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsb2110 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Work_Record;

    private DbsGroup pnd_Work_Record__R_Field_1;
    private DbsField pnd_Work_Record_Pnd_Work_Type;
    private DbsField pnd_Work_Record_Pnd_Work_Folder;

    private DbsGroup pnd_Work_Record__R_Field_2;
    private DbsField pnd_Work_Record_Pnd_Work_Cabinet;
    private DbsField pnd_Work_Record_Pnd_Work_Date;
    private DbsField pnd_Work_Record_Pnd_Work_Case;
    private DbsField pnd_Work_Record_Pnd_Work_Wpid;
    private DbsField pnd_Work_Record_Pnd_Work_Unit_1st;
    private DbsField pnd_Work_Record_Pnd_Work_Doc_Type;

    private DbsGroup pnd_Work_Record__R_Field_3;
    private DbsField pnd_Work_Record_Pnd_Work_Cat;
    private DbsField pnd_Work_Record_Pnd_Work_Sub_Cat;
    private DbsField pnd_Work_Record_Pnd_Work_Detail;
    private DbsField pnd_Work_Record_Pnd_Work_Doc_Sub_Type;
    private DbsField pnd_Work_Record_Pnd_Work_System;
    private DbsField pnd_Work_Record_Pnd_Work_Drctn_Ind;
    private DbsField pnd_Work_Record_Pnd_Work_Unit_Entry;

    private DbsGroup pnd_Work_Record__R_Field_4;
    private DbsField pnd_Work_Record_Pnd_Work_Typ10;

    private DbsGroup pnd_Work_Record_Pnd_Input_Dates;
    private DbsField pnd_Work_Record_Pnd_F_Yy;
    private DbsField pnd_Work_Record_Pnd_F_Mm;
    private DbsField pnd_Work_Record_Pnd_F_Dd;
    private DbsField pnd_Work_Record_Pnd_T_Yy;
    private DbsField pnd_Work_Record_Pnd_T_Mm;
    private DbsField pnd_Work_Record_Pnd_T_Dd;

    private DbsGroup h10;
    private DbsField h10_H10a;
    private DbsField h10_H10b;
    private DbsField h10_H10c;

    private DbsGroup h10__R_Field_5;

    private DbsGroup h10_H10_Dates;
    private DbsField h10_Pnd_F_Mm;
    private DbsField h10_Sl1;
    private DbsField h10_Pnd_F_Dd;
    private DbsField h10_Sl2;
    private DbsField h10_Pnd_F_Yy;
    private DbsField h10_Dash;
    private DbsField h10_Pnd_T_Mm;
    private DbsField h10_Sl3;
    private DbsField h10_Pnd_T_Dd;
    private DbsField h10_Sl4;
    private DbsField h10_Pnd_T_Yy;
    private DbsField h10_H10d;
    private DbsField h10_H10pge;

    private DbsGroup h20;
    private DbsField h20_H20a;
    private DbsField h20_H20b;

    private DbsGroup h30;
    private DbsField h30_H30a;
    private DbsField h30_H30b;

    private DbsGroup h32;
    private DbsField h32_H32a;
    private DbsField h32_H32b;
    private DbsField h32_H32c;

    private DbsGroup h35;
    private DbsField h35_H35a;
    private DbsField h35_H35b;
    private DbsField h35_H35c;

    private DbsGroup h40;
    private DbsField h40_H40a;
    private DbsField h40_H40b;
    private DbsField h40_H40c;

    private DbsGroup d50;
    private DbsField d50_D50_Wpid;
    private DbsField d50_D50_Unit;
    private DbsField d50_D50_Item;
    private DbsField pnd_Doc_Cnt;
    private DbsField pnd_Pge_Cnt;
    private DbsField pnd_Hold_Wpid;
    private DbsField pnd_Hold_Unit;
    private DbsField pnd_Hold_Folder;

    private DbsGroup d50_Cntrs;
    private DbsField d50_Cntrs_Pnd_Wreq;
    private DbsField d50_Cntrs_Pnd_Imgi;
    private DbsField d50_Cntrs_Pnd_Imgo;
    private DbsField d50_Cntrs_Pnd_Appg_Txt;
    private DbsField d50_Cntrs_Pnd_Pss;
    private DbsField d50_Cntrs_Pnd_Mcss;
    private DbsField d50_Cntrs_Pnd_Icss;
    private DbsField d50_Cntrs_Pnd_Not;
    private DbsField d50_Cntrs_Pnd_Scr;
    private DbsField d50_Cntrs_Pnd_Wks;
    private DbsField d50_Cntrs_Pnd_Rss;

    private DbsGroup d50_Cntrs__R_Field_6;
    private DbsField d50_Cntrs_D50_Cnt;
    private DbsField d50_Tot;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Work_Record = localVariables.newFieldInRecord("pnd_Work_Record", "#WORK-RECORD", FieldType.STRING, 65);

        pnd_Work_Record__R_Field_1 = localVariables.newGroupInRecord("pnd_Work_Record__R_Field_1", "REDEFINE", pnd_Work_Record);
        pnd_Work_Record_Pnd_Work_Type = pnd_Work_Record__R_Field_1.newFieldInGroup("pnd_Work_Record_Pnd_Work_Type", "#WORK-TYPE", FieldType.STRING, 2);
        pnd_Work_Record_Pnd_Work_Folder = pnd_Work_Record__R_Field_1.newFieldInGroup("pnd_Work_Record_Pnd_Work_Folder", "#WORK-FOLDER", FieldType.STRING, 
            27);

        pnd_Work_Record__R_Field_2 = pnd_Work_Record__R_Field_1.newGroupInGroup("pnd_Work_Record__R_Field_2", "REDEFINE", pnd_Work_Record_Pnd_Work_Folder);
        pnd_Work_Record_Pnd_Work_Cabinet = pnd_Work_Record__R_Field_2.newFieldInGroup("pnd_Work_Record_Pnd_Work_Cabinet", "#WORK-CABINET", FieldType.STRING, 
            14);
        pnd_Work_Record_Pnd_Work_Date = pnd_Work_Record__R_Field_2.newFieldInGroup("pnd_Work_Record_Pnd_Work_Date", "#WORK-DATE", FieldType.DATE);
        pnd_Work_Record_Pnd_Work_Case = pnd_Work_Record__R_Field_2.newFieldInGroup("pnd_Work_Record_Pnd_Work_Case", "#WORK-CASE", FieldType.STRING, 1);
        pnd_Work_Record_Pnd_Work_Wpid = pnd_Work_Record__R_Field_2.newFieldInGroup("pnd_Work_Record_Pnd_Work_Wpid", "#WORK-WPID", FieldType.STRING, 6);
        pnd_Work_Record_Pnd_Work_Unit_1st = pnd_Work_Record__R_Field_1.newFieldInGroup("pnd_Work_Record_Pnd_Work_Unit_1st", "#WORK-UNIT-1ST", FieldType.STRING, 
            8);
        pnd_Work_Record_Pnd_Work_Doc_Type = pnd_Work_Record__R_Field_1.newFieldInGroup("pnd_Work_Record_Pnd_Work_Doc_Type", "#WORK-DOC-TYPE", FieldType.STRING, 
            8);

        pnd_Work_Record__R_Field_3 = pnd_Work_Record__R_Field_1.newGroupInGroup("pnd_Work_Record__R_Field_3", "REDEFINE", pnd_Work_Record_Pnd_Work_Doc_Type);
        pnd_Work_Record_Pnd_Work_Cat = pnd_Work_Record__R_Field_3.newFieldInGroup("pnd_Work_Record_Pnd_Work_Cat", "#WORK-CAT", FieldType.STRING, 1);
        pnd_Work_Record_Pnd_Work_Sub_Cat = pnd_Work_Record__R_Field_3.newFieldInGroup("pnd_Work_Record_Pnd_Work_Sub_Cat", "#WORK-SUB-CAT", FieldType.STRING, 
            3);
        pnd_Work_Record_Pnd_Work_Detail = pnd_Work_Record__R_Field_3.newFieldInGroup("pnd_Work_Record_Pnd_Work_Detail", "#WORK-DETAIL", FieldType.STRING, 
            4);
        pnd_Work_Record_Pnd_Work_Doc_Sub_Type = pnd_Work_Record__R_Field_1.newFieldInGroup("pnd_Work_Record_Pnd_Work_Doc_Sub_Type", "#WORK-DOC-SUB-TYPE", 
            FieldType.STRING, 1);
        pnd_Work_Record_Pnd_Work_System = pnd_Work_Record__R_Field_1.newFieldInGroup("pnd_Work_Record_Pnd_Work_System", "#WORK-SYSTEM", FieldType.STRING, 
            8);
        pnd_Work_Record_Pnd_Work_Drctn_Ind = pnd_Work_Record__R_Field_1.newFieldInGroup("pnd_Work_Record_Pnd_Work_Drctn_Ind", "#WORK-DRCTN-IND", FieldType.STRING, 
            1);
        pnd_Work_Record_Pnd_Work_Unit_Entry = pnd_Work_Record__R_Field_1.newFieldInGroup("pnd_Work_Record_Pnd_Work_Unit_Entry", "#WORK-UNIT-ENTRY", FieldType.STRING, 
            8);

        pnd_Work_Record__R_Field_4 = localVariables.newGroupInRecord("pnd_Work_Record__R_Field_4", "REDEFINE", pnd_Work_Record);
        pnd_Work_Record_Pnd_Work_Typ10 = pnd_Work_Record__R_Field_4.newFieldInGroup("pnd_Work_Record_Pnd_Work_Typ10", "#WORK-TYP10", FieldType.STRING, 
            2);

        pnd_Work_Record_Pnd_Input_Dates = pnd_Work_Record__R_Field_4.newGroupInGroup("pnd_Work_Record_Pnd_Input_Dates", "#INPUT-DATES");
        pnd_Work_Record_Pnd_F_Yy = pnd_Work_Record_Pnd_Input_Dates.newFieldInGroup("pnd_Work_Record_Pnd_F_Yy", "#F-YY", FieldType.STRING, 4);
        pnd_Work_Record_Pnd_F_Mm = pnd_Work_Record_Pnd_Input_Dates.newFieldInGroup("pnd_Work_Record_Pnd_F_Mm", "#F-MM", FieldType.STRING, 2);
        pnd_Work_Record_Pnd_F_Dd = pnd_Work_Record_Pnd_Input_Dates.newFieldInGroup("pnd_Work_Record_Pnd_F_Dd", "#F-DD", FieldType.STRING, 2);
        pnd_Work_Record_Pnd_T_Yy = pnd_Work_Record_Pnd_Input_Dates.newFieldInGroup("pnd_Work_Record_Pnd_T_Yy", "#T-YY", FieldType.STRING, 4);
        pnd_Work_Record_Pnd_T_Mm = pnd_Work_Record_Pnd_Input_Dates.newFieldInGroup("pnd_Work_Record_Pnd_T_Mm", "#T-MM", FieldType.STRING, 2);
        pnd_Work_Record_Pnd_T_Dd = pnd_Work_Record_Pnd_Input_Dates.newFieldInGroup("pnd_Work_Record_Pnd_T_Dd", "#T-DD", FieldType.STRING, 2);

        h10 = localVariables.newGroupInRecord("h10", "H10");
        h10_H10a = h10.newFieldInGroup("h10_H10a", "H10A", FieldType.STRING, 14);
        h10_H10b = h10.newFieldInGroup("h10_H10b", "H10B", FieldType.STRING, 60);
        h10_H10c = h10.newFieldInGroup("h10_H10c", "H10C", FieldType.STRING, 23);

        h10__R_Field_5 = h10.newGroupInGroup("h10__R_Field_5", "REDEFINE", h10_H10c);

        h10_H10_Dates = h10__R_Field_5.newGroupInGroup("h10_H10_Dates", "H10-DATES");
        h10_Pnd_F_Mm = h10_H10_Dates.newFieldInGroup("h10_Pnd_F_Mm", "#F-MM", FieldType.STRING, 2);
        h10_Sl1 = h10_H10_Dates.newFieldInGroup("h10_Sl1", "SL1", FieldType.STRING, 1);
        h10_Pnd_F_Dd = h10_H10_Dates.newFieldInGroup("h10_Pnd_F_Dd", "#F-DD", FieldType.STRING, 2);
        h10_Sl2 = h10_H10_Dates.newFieldInGroup("h10_Sl2", "SL2", FieldType.STRING, 1);
        h10_Pnd_F_Yy = h10_H10_Dates.newFieldInGroup("h10_Pnd_F_Yy", "#F-YY", FieldType.STRING, 4);
        h10_Dash = h10_H10_Dates.newFieldInGroup("h10_Dash", "DASH", FieldType.STRING, 3);
        h10_Pnd_T_Mm = h10_H10_Dates.newFieldInGroup("h10_Pnd_T_Mm", "#T-MM", FieldType.STRING, 2);
        h10_Sl3 = h10_H10_Dates.newFieldInGroup("h10_Sl3", "SL3", FieldType.STRING, 1);
        h10_Pnd_T_Dd = h10_H10_Dates.newFieldInGroup("h10_Pnd_T_Dd", "#T-DD", FieldType.STRING, 2);
        h10_Sl4 = h10_H10_Dates.newFieldInGroup("h10_Sl4", "SL4", FieldType.STRING, 1);
        h10_Pnd_T_Yy = h10_H10_Dates.newFieldInGroup("h10_Pnd_T_Yy", "#T-YY", FieldType.STRING, 4);
        h10_H10d = h10.newFieldInGroup("h10_H10d", "H10D", FieldType.STRING, 15);
        h10_H10pge = h10.newFieldInGroup("h10_H10pge", "H10PGE", FieldType.NUMERIC, 3);

        h20 = localVariables.newGroupInRecord("h20", "H20");
        h20_H20a = h20.newFieldInGroup("h20_H20a", "H20A", FieldType.STRING, 30);
        h20_H20b = h20.newFieldInGroup("h20_H20b", "H20B", FieldType.STRING, 79);

        h30 = localVariables.newGroupInRecord("h30", "H30");
        h30_H30a = h30.newFieldInGroup("h30_H30a", "H30A", FieldType.STRING, 59);
        h30_H30b = h30.newFieldInGroup("h30_H30b", "H30B", FieldType.STRING, 59);

        h32 = localVariables.newGroupInRecord("h32", "H32");
        h32_H32a = h32.newFieldInGroup("h32_H32a", "H32A", FieldType.STRING, 59);
        h32_H32b = h32.newFieldInGroup("h32_H32b", "H32B", FieldType.STRING, 59);
        h32_H32c = h32.newFieldInGroup("h32_H32c", "H32C", FieldType.STRING, 8);

        h35 = localVariables.newGroupInRecord("h35", "H35");
        h35_H35a = h35.newFieldInGroup("h35_H35a", "H35A", FieldType.STRING, 59);
        h35_H35b = h35.newFieldInGroup("h35_H35b", "H35B", FieldType.STRING, 59);
        h35_H35c = h35.newFieldInGroup("h35_H35c", "H35C", FieldType.STRING, 8);

        h40 = localVariables.newGroupInRecord("h40", "H40");
        h40_H40a = h40.newFieldInGroup("h40_H40a", "H40A", FieldType.STRING, 59);
        h40_H40b = h40.newFieldInGroup("h40_H40b", "H40B", FieldType.STRING, 59);
        h40_H40c = h40.newFieldInGroup("h40_H40c", "H40C", FieldType.STRING, 8);

        d50 = localVariables.newGroupInRecord("d50", "D50");
        d50_D50_Wpid = d50.newFieldInGroup("d50_D50_Wpid", "D50-WPID", FieldType.STRING, 8);
        d50_D50_Unit = d50.newFieldInGroup("d50_D50_Unit", "D50-UNIT", FieldType.STRING, 8);
        d50_D50_Item = d50.newFieldArrayInGroup("d50_D50_Item", "D50-ITEM", FieldType.NUMERIC, 9, new DbsArrayController(1, 11));
        pnd_Doc_Cnt = localVariables.newFieldInRecord("pnd_Doc_Cnt", "#DOC-CNT", FieldType.NUMERIC, 7);
        pnd_Pge_Cnt = localVariables.newFieldInRecord("pnd_Pge_Cnt", "#PGE-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_Hold_Wpid = localVariables.newFieldInRecord("pnd_Hold_Wpid", "#HOLD-WPID", FieldType.STRING, 6);
        pnd_Hold_Unit = localVariables.newFieldInRecord("pnd_Hold_Unit", "#HOLD-UNIT", FieldType.STRING, 8);
        pnd_Hold_Folder = localVariables.newFieldInRecord("pnd_Hold_Folder", "#HOLD-FOLDER", FieldType.STRING, 22);

        d50_Cntrs = localVariables.newGroupInRecord("d50_Cntrs", "D50-CNTRS");
        d50_Cntrs_Pnd_Wreq = d50_Cntrs.newFieldInGroup("d50_Cntrs_Pnd_Wreq", "#WREQ", FieldType.NUMERIC, 9);
        d50_Cntrs_Pnd_Imgi = d50_Cntrs.newFieldInGroup("d50_Cntrs_Pnd_Imgi", "#IMGI", FieldType.NUMERIC, 9);
        d50_Cntrs_Pnd_Imgo = d50_Cntrs.newFieldInGroup("d50_Cntrs_Pnd_Imgo", "#IMGO", FieldType.NUMERIC, 9);
        d50_Cntrs_Pnd_Appg_Txt = d50_Cntrs.newFieldInGroup("d50_Cntrs_Pnd_Appg_Txt", "#APPG-TXT", FieldType.NUMERIC, 9);
        d50_Cntrs_Pnd_Pss = d50_Cntrs.newFieldInGroup("d50_Cntrs_Pnd_Pss", "#PSS", FieldType.NUMERIC, 9);
        d50_Cntrs_Pnd_Mcss = d50_Cntrs.newFieldInGroup("d50_Cntrs_Pnd_Mcss", "#MCSS", FieldType.NUMERIC, 9);
        d50_Cntrs_Pnd_Icss = d50_Cntrs.newFieldInGroup("d50_Cntrs_Pnd_Icss", "#ICSS", FieldType.NUMERIC, 9);
        d50_Cntrs_Pnd_Not = d50_Cntrs.newFieldInGroup("d50_Cntrs_Pnd_Not", "#NOT", FieldType.NUMERIC, 9);
        d50_Cntrs_Pnd_Scr = d50_Cntrs.newFieldInGroup("d50_Cntrs_Pnd_Scr", "#SCR", FieldType.NUMERIC, 9);
        d50_Cntrs_Pnd_Wks = d50_Cntrs.newFieldInGroup("d50_Cntrs_Pnd_Wks", "#WKS", FieldType.NUMERIC, 9);
        d50_Cntrs_Pnd_Rss = d50_Cntrs.newFieldInGroup("d50_Cntrs_Pnd_Rss", "#RSS", FieldType.NUMERIC, 9);

        d50_Cntrs__R_Field_6 = localVariables.newGroupInRecord("d50_Cntrs__R_Field_6", "REDEFINE", d50_Cntrs);
        d50_Cntrs_D50_Cnt = d50_Cntrs__R_Field_6.newFieldArrayInGroup("d50_Cntrs_D50_Cnt", "D50-CNT", FieldType.NUMERIC, 9, new DbsArrayController(1, 
            11));
        d50_Tot = localVariables.newFieldArrayInRecord("d50_Tot", "D50-TOT", FieldType.NUMERIC, 9, new DbsArrayController(1, 11));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        h10_H10b.setInitialValue(" EFM Document Statistics for Closed Work Requests for Period");
        h10_H10c.setInitialValue("00/00/0000 - 00/00/0000");
        h10_H10d.setInitialValue("           PAGE");
        h20_H20b.setInitialValue(" Document Sources by Work Process ID");
        h30_H30a.setInitialValue("                             ! Incoming  Outgoing          ");
        h30_H30b.setInitialValue("                                 --- Processing Units ---  ");
        h32_H32a.setInitialValue("         1st           Work  !  CIRS     Applictns Generatd");
        h32_H32b.setInitialValue("   ---- Contact Sheets ----               Scrns      Work  ");
        h32_H32c.setInitialValue("   RSS  ");
        h35_H35a.setInitialValue("WPID     Unit          Rqsts ! Scanned    Images     Text  ");
        h35_H35b.setInitialValue("    PSS   Mainfrme      ISS      Notes    Captrd    Sheets ");
        h35_H35c.setInitialValue(" Scanned");
        h40_H40a.setInitialValue("------   --------      ----- ! -------    ------    ------ ");
        h40_H40b.setInitialValue("  ------   -------    ------    ------    ------    ------ ");
        h40_H40c.setInitialValue(" -------");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Efsb2110() throws Exception
    {
        super("Efsb2110");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  ===================================================================
        //*  -----------------------------                                                                                                                                //Natural: FORMAT ( 1 ) LS = 132 PS = 58;//Natural: AT TOP OF PAGE ( 1 )
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #WORK-RECORD
        while (condition(getWorkFiles().read(1, pnd_Work_Record)))
        {
            if (condition(pnd_Work_Record_Pnd_Work_Type.equals("10")))                                                                                                    //Natural: IF #WORK-TYPE = '10'
            {
                h10_H10_Dates.setValuesByName(pnd_Work_Record_Pnd_Input_Dates);                                                                                           //Natural: MOVE BY NAME #INPUT-DATES TO H10-DATES
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Doc_Cnt.equals(getZero())))                                                                                                                 //Natural: IF #DOC-CNT = 0
            {
                pnd_Hold_Wpid.setValue(pnd_Work_Record_Pnd_Work_Wpid);                                                                                                    //Natural: MOVE #WORK-WPID TO #HOLD-WPID
                pnd_Hold_Unit.setValue(pnd_Work_Record_Pnd_Work_Unit_1st);                                                                                                //Natural: MOVE #WORK-UNIT-1ST TO #HOLD-UNIT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(! (pnd_Work_Record_Pnd_Work_Wpid.equals(pnd_Hold_Wpid) && pnd_Work_Record_Pnd_Work_Unit_1st.equals(pnd_Hold_Unit))))                            //Natural: IF NOT ( #WORK-WPID = #HOLD-WPID AND #WORK-UNIT-1ST = #HOLD-UNIT )
            {
                                                                                                                                                                          //Natural: PERFORM LINE-BREAK
                sub_Line_Break();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Record_Pnd_Work_Folder.notEquals(pnd_Hold_Folder)))                                                                                    //Natural: IF #WORK-FOLDER NOT = #HOLD-FOLDER
            {
                pnd_Hold_Folder.setValue(pnd_Work_Record_Pnd_Work_Folder);                                                                                                //Natural: MOVE #WORK-FOLDER TO #HOLD-FOLDER
                d50_Cntrs_Pnd_Wreq.nadd(1);                                                                                                                               //Natural: ADD 1 TO #WREQ
            }                                                                                                                                                             //Natural: END-IF
            pnd_Doc_Cnt.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #DOC-CNT
            //*  -----------------------------     SEGREGATE BY DOCUMENT ENTRY-SYSTEM
            if (condition(pnd_Work_Record_Pnd_Work_System.equals("PSS")))                                                                                                 //Natural: IF #WORK-SYSTEM = 'PSS'
            {
                //*  PSS CONTACT SHEET
                d50_Cntrs_Pnd_Pss.nadd(1);                                                                                                                                //Natural: ADD 1 TO #PSS
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Record_Pnd_Work_System.equals("MCSG")))                                                                                                //Natural: IF #WORK-SYSTEM = 'MCSG'
            {
                //*  MAINFRAME CONTACT SHEET
                d50_Cntrs_Pnd_Mcss.nadd(1);                                                                                                                               //Natural: ADD 1 TO #MCSS
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Record_Pnd_Work_System.equals("ICSS")))                                                                                                //Natural: IF #WORK-SYSTEM = 'ICSS'
            {
                //*  INSURANCE CONTACT SHEET
                d50_Cntrs_Pnd_Icss.nadd(1);                                                                                                                               //Natural: ADD 1 TO #ICSS
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Record_Pnd_Work_System.equals("NOTE")))                                                                                                //Natural: IF #WORK-SYSTEM = 'NOTE'
            {
                //*  NOTE
                d50_Cntrs_Pnd_Not.nadd(1);                                                                                                                                //Natural: ADD 1 TO #NOT
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Record_Pnd_Work_System.equals("SCRCPT")))                                                                                              //Natural: IF #WORK-SYSTEM = 'SCRCPT'
            {
                //*  SCREEN CAPTURE
                d50_Cntrs_Pnd_Scr.nadd(1);                                                                                                                                //Natural: ADD 1 TO #SCR
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Record_Pnd_Work_System.equals("WKS")))                                                                                                 //Natural: IF #WORK-SYSTEM = 'WKS'
            {
                //*  WORKSHEET
                d50_Cntrs_Pnd_Wks.nadd(1);                                                                                                                                //Natural: ADD 1 TO #WKS
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Record_Pnd_Work_System.equals("BES")))                                                                                                 //Natural: IF #WORK-SYSTEM = 'BES'
            {
                //*  SCANNED BY RSS
                d50_Cntrs_Pnd_Rss.nadd(1);                                                                                                                                //Natural: ADD 1 TO #RSS
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  -----------------------------     REMAINING IMAGES
            if (condition(pnd_Work_Record_Pnd_Work_Doc_Sub_Type.equals("I")))                                                                                             //Natural: IF #WORK-DOC-SUB-TYPE = 'I'
            {
                if (condition(pnd_Work_Record_Pnd_Work_Drctn_Ind.equals("O")))                                                                                            //Natural: IF #WORK-DRCTN-IND = 'O'
                {
                    //*  OUTGOING
                    d50_Cntrs_Pnd_Imgo.nadd(1);                                                                                                                           //Natural: ADD 1 TO #IMGO
                    //*  INCOMING, INTERNAL
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    d50_Cntrs_Pnd_Imgi.nadd(1);                                                                                                                           //Natural: ADD 1 TO #IMGI
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  -----------------------------     ALL OTHER GENERATED TEXT
            //*  APPLICATION GENERATED TEXT
            d50_Cntrs_Pnd_Appg_Txt.nadd(1);                                                                                                                               //Natural: ADD 1 TO #APPG-TXT
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  ==============================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LINE-BREAK
        //*  ==============================================
                                                                                                                                                                          //Natural: PERFORM LINE-BREAK
        sub_Line_Break();
        if (condition(Global.isEscape())) {return;}
        d50_D50_Wpid.setValue("Totals");                                                                                                                                  //Natural: ASSIGN D50-WPID = 'Totals'
        d50_D50_Unit.reset();                                                                                                                                             //Natural: RESET D50-UNIT
        d50_D50_Item.getValue("*").setValue(d50_Tot.getValue("*"));                                                                                                       //Natural: ASSIGN D50-ITEM ( * ) = D50-TOT ( * )
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,d50_D50_Wpid,d50_D50_Unit,d50_D50_Item.getValue(1),d50_D50_Item.getValue(2),d50_D50_Item.getValue(3),          //Natural: WRITE ( 1 ) / D50
            d50_D50_Item.getValue(4),d50_D50_Item.getValue(5),d50_D50_Item.getValue(6),d50_D50_Item.getValue(7),d50_D50_Item.getValue(8),d50_D50_Item.getValue(9),
            d50_D50_Item.getValue(10),d50_D50_Item.getValue(11));
        if (Global.isEscape()) return;
        //*  ----------------------------------------
        getReports().write(0, NEWLINE,pnd_Doc_Cnt,"Documents");                                                                                                           //Natural: WRITE / #DOC-CNT 'Documents'
        if (Global.isEscape()) return;
    }
    private void sub_Line_Break() throws Exception                                                                                                                        //Natural: LINE-BREAK
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------
        d50_D50_Wpid.setValue(pnd_Hold_Wpid);                                                                                                                             //Natural: ASSIGN D50-WPID = #HOLD-WPID
        d50_D50_Unit.setValue(pnd_Hold_Unit);                                                                                                                             //Natural: ASSIGN D50-UNIT = #HOLD-UNIT
        d50_D50_Item.getValue("*").setValue(d50_Cntrs_D50_Cnt.getValue("*"));                                                                                             //Natural: ASSIGN D50-ITEM ( * ) = D50-CNT ( * )
        d50_Tot.getValue("*").nadd(d50_Cntrs_D50_Cnt.getValue("*"));                                                                                                      //Natural: ADD D50-CNT ( * ) TO D50-TOT ( * )
        d50_Cntrs_D50_Cnt.getValue("*").reset();                                                                                                                          //Natural: RESET D50-CNT ( * )
        pnd_Hold_Wpid.setValue(pnd_Work_Record_Pnd_Work_Wpid);                                                                                                            //Natural: ASSIGN #HOLD-WPID = #WORK-WPID
        pnd_Hold_Unit.setValue(pnd_Work_Record_Pnd_Work_Unit_1st);                                                                                                        //Natural: ASSIGN #HOLD-UNIT = #WORK-UNIT-1ST
        pnd_Hold_Folder.reset();                                                                                                                                          //Natural: RESET #HOLD-FOLDER
        getReports().write(1, ReportOption.NOTITLE,d50_D50_Wpid,d50_D50_Unit,d50_D50_Item.getValue(1),d50_D50_Item.getValue(2),d50_D50_Item.getValue(3),                  //Natural: WRITE ( 1 ) D50
            d50_D50_Item.getValue(4),d50_D50_Item.getValue(5),d50_D50_Item.getValue(6),d50_D50_Item.getValue(7),d50_D50_Item.getValue(8),d50_D50_Item.getValue(9),
            d50_D50_Item.getValue(10),d50_D50_Item.getValue(11));
        if (Global.isEscape()) return;
        //*  LINE-BREAK
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Pge_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #PGE-CNT
                    h10_H10pge.setValue(pnd_Pge_Cnt);                                                                                                                     //Natural: MOVE #PGE-CNT TO H10PGE
                    getReports().write(1, ReportOption.NOTITLE,Global.getDATU(),h10_H10a,h10_H10b,h10_H10c,h10_H10d,h10_H10pge);                                          //Natural: WRITE ( 1 ) NOTITLE *DATU H10
                    getReports().write(1, ReportOption.NOTITLE,Global.getTIMX(),h20_H20a,h20_H20b,Global.getPROGRAM());                                                   //Natural: WRITE ( 1 ) *TIMX H20 *PROGRAM
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,h30_H30a,h30_H30b);                                                                                //Natural: WRITE ( 1 ) / H30
                    getReports().write(1, ReportOption.NOTITLE,h32_H32a,h32_H32b,h32_H32c);                                                                               //Natural: WRITE ( 1 ) H32
                    getReports().write(1, ReportOption.NOTITLE,h35_H35a,h35_H35b,h35_H35c);                                                                               //Natural: WRITE ( 1 ) H35
                    getReports().write(1, ReportOption.NOTITLE,h40_H40a,h40_H40b,h40_H40c);                                                                               //Natural: WRITE ( 1 ) H40
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=58");
    }
}
