/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:29:59 PM
**        * FROM NATURAL PROGRAM : Cwfb3494
************************************************************
**        * FILE NAME            : Cwfb3494.java
**        * CLASS NAME           : Cwfb3494
**        * INSTANCE NAME        : Cwfb3494
************************************************************
************************************************************************
* PROGRAM  : CWFB3494
* SYSTEM   : CWF
* TITLE    :
* GENERATED: OCT 05,2000
* FUNCTION : EXTRACTS ACTIVE PROJECT IDS FOR LOADING TO ORACLE
*          :   (FOR SMART)
*          :
*          :
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* --------   -------- --------------------------------------------------
*
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3494 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Project;
    private DbsField cwf_Project_Prjct_Id;
    private DbsField cwf_Project_Prjct_Nme;
    private DbsField cwf_Project_Sub_Prjct_Nme;
    private DbsField cwf_Project_Owner_Unit;
    private DbsField cwf_Project_Initiation_Unit;
    private DbsField cwf_Project_Entry_Oprtr_Cde;
    private DbsField cwf_Project_Entry_Dte_Tme;
    private DbsField cwf_Project_Last_Updte_Unit_Cde;
    private DbsField cwf_Project_Last_Updte_Oprtr_Cde;
    private DbsField cwf_Project_Last_Updte_Dte_Tme;
    private DbsField cwf_Project_Start_Date;
    private DbsField cwf_Project_End_Date;
    private DbsField cwf_Project_Dlte_Oprtr_Cde;
    private DbsField cwf_Project_Dlte_Dte_Tme;
    private DbsField cwf_Project_Actve_Ind;
    private DbsField cwf_Project_Ref_Consult_Racf;
    private DbsField cwf_Project_Dest_Unit;
    private DbsField cwf_Project_Prjct_Ctgry;
    private DbsField cwf_Project_Prjct_Type;

    private DbsGroup pnd_Work_File;
    private DbsField pnd_Work_File_Prjct_Id;
    private DbsField pnd_Work_File_Actve_Ind;
    private DbsField pnd_Work_File_Owner_Unit;
    private DbsField pnd_Work_File_Prjct_Nme;
    private DbsField pnd_Work_File_Sub_Prjct_Nme;
    private DbsField pnd_Work_File_Initiation_Unit;
    private DbsField pnd_Work_File_Entry_Oprtr_Cde;
    private DbsField pnd_Work_File_Pnd_Entry_Dte_Tme;
    private DbsField pnd_Work_File_Last_Updte_Unit_Cde;
    private DbsField pnd_Work_File_Last_Updte_Oprtr_Cde;
    private DbsField pnd_Work_File_Pnd_Last_Updte_Dte_Tme;
    private DbsField pnd_Work_File_Start_Date;
    private DbsField pnd_Work_File_End_Date;
    private DbsField pnd_Work_File_Dlte_Oprtr_Cde;
    private DbsField pnd_Work_File_Pnd_Dlte_Dte_Tme;
    private DbsField pnd_Work_File_Ref_Consult_Racf;
    private DbsField pnd_Work_File_Dest_Unit;
    private DbsField pnd_Work_File_Prjct_Ctgry;
    private DbsField pnd_Work_File_Prjct_Type;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Project = new DataAccessProgramView(new NameInfo("vw_cwf_Project", "CWF-PROJECT"), "CWF_PROJECT", "CWF_PROJECT");
        cwf_Project_Prjct_Id = vw_cwf_Project.getRecord().newFieldInGroup("cwf_Project_Prjct_Id", "PRJCT-ID", FieldType.STRING, 28, RepeatingFieldStrategy.None, 
            "PRJCT_ID");
        cwf_Project_Prjct_Nme = vw_cwf_Project.getRecord().newFieldInGroup("cwf_Project_Prjct_Nme", "PRJCT-NME", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "PRJCT_NME");
        cwf_Project_Sub_Prjct_Nme = vw_cwf_Project.getRecord().newFieldInGroup("cwf_Project_Sub_Prjct_Nme", "SUB-PRJCT-NME", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "SUB_PRJCT_NME");
        cwf_Project_Owner_Unit = vw_cwf_Project.getRecord().newFieldInGroup("cwf_Project_Owner_Unit", "OWNER-UNIT", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "OWNER_UNIT");
        cwf_Project_Initiation_Unit = vw_cwf_Project.getRecord().newFieldInGroup("cwf_Project_Initiation_Unit", "INITIATION-UNIT", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "INITIATION_UNIT");
        cwf_Project_Entry_Oprtr_Cde = vw_cwf_Project.getRecord().newFieldInGroup("cwf_Project_Entry_Oprtr_Cde", "ENTRY-OPRTR-CDE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "ENTRY_OPRTR_CDE");
        cwf_Project_Entry_Dte_Tme = vw_cwf_Project.getRecord().newFieldInGroup("cwf_Project_Entry_Dte_Tme", "ENTRY-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "ENTRY_DTE_TME");
        cwf_Project_Last_Updte_Unit_Cde = vw_cwf_Project.getRecord().newFieldInGroup("cwf_Project_Last_Updte_Unit_Cde", "LAST-UPDTE-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "LAST_UPDTE_UNIT_CDE");
        cwf_Project_Last_Updte_Oprtr_Cde = vw_cwf_Project.getRecord().newFieldInGroup("cwf_Project_Last_Updte_Oprtr_Cde", "LAST-UPDTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "LAST_UPDTE_OPRTR_CDE");
        cwf_Project_Last_Updte_Dte_Tme = vw_cwf_Project.getRecord().newFieldInGroup("cwf_Project_Last_Updte_Dte_Tme", "LAST-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LAST_UPDTE_DTE_TME");
        cwf_Project_Start_Date = vw_cwf_Project.getRecord().newFieldInGroup("cwf_Project_Start_Date", "START-DATE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "START_DATE");
        cwf_Project_End_Date = vw_cwf_Project.getRecord().newFieldInGroup("cwf_Project_End_Date", "END-DATE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "END_DATE");
        cwf_Project_Dlte_Oprtr_Cde = vw_cwf_Project.getRecord().newFieldInGroup("cwf_Project_Dlte_Oprtr_Cde", "DLTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "DLTE_OPRTR_CDE");
        cwf_Project_Dlte_Dte_Tme = vw_cwf_Project.getRecord().newFieldInGroup("cwf_Project_Dlte_Dte_Tme", "DLTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "DLTE_DTE_TME");
        cwf_Project_Actve_Ind = vw_cwf_Project.getRecord().newFieldInGroup("cwf_Project_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTVE_IND");
        cwf_Project_Ref_Consult_Racf = vw_cwf_Project.getRecord().newFieldInGroup("cwf_Project_Ref_Consult_Racf", "REF-CONSULT-RACF", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "REF_CONSULT_RACF");
        cwf_Project_Dest_Unit = vw_cwf_Project.getRecord().newFieldInGroup("cwf_Project_Dest_Unit", "DEST-UNIT", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "DEST_UNIT");
        cwf_Project_Prjct_Ctgry = vw_cwf_Project.getRecord().newFieldInGroup("cwf_Project_Prjct_Ctgry", "PRJCT-CTGRY", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "PRJCT_CTGRY");
        cwf_Project_Prjct_Type = vw_cwf_Project.getRecord().newFieldInGroup("cwf_Project_Prjct_Type", "PRJCT-TYPE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "PRJCT_TYPE");
        registerRecord(vw_cwf_Project);

        pnd_Work_File = localVariables.newGroupInRecord("pnd_Work_File", "#WORK-FILE");
        pnd_Work_File_Prjct_Id = pnd_Work_File.newFieldInGroup("pnd_Work_File_Prjct_Id", "PRJCT-ID", FieldType.STRING, 28);
        pnd_Work_File_Actve_Ind = pnd_Work_File.newFieldInGroup("pnd_Work_File_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2);
        pnd_Work_File_Owner_Unit = pnd_Work_File.newFieldInGroup("pnd_Work_File_Owner_Unit", "OWNER-UNIT", FieldType.STRING, 8);
        pnd_Work_File_Prjct_Nme = pnd_Work_File.newFieldInGroup("pnd_Work_File_Prjct_Nme", "PRJCT-NME", FieldType.STRING, 20);
        pnd_Work_File_Sub_Prjct_Nme = pnd_Work_File.newFieldInGroup("pnd_Work_File_Sub_Prjct_Nme", "SUB-PRJCT-NME", FieldType.STRING, 20);
        pnd_Work_File_Initiation_Unit = pnd_Work_File.newFieldInGroup("pnd_Work_File_Initiation_Unit", "INITIATION-UNIT", FieldType.STRING, 8);
        pnd_Work_File_Entry_Oprtr_Cde = pnd_Work_File.newFieldInGroup("pnd_Work_File_Entry_Oprtr_Cde", "ENTRY-OPRTR-CDE", FieldType.STRING, 8);
        pnd_Work_File_Pnd_Entry_Dte_Tme = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Entry_Dte_Tme", "#ENTRY-DTE-TME", FieldType.STRING, 15);
        pnd_Work_File_Last_Updte_Unit_Cde = pnd_Work_File.newFieldInGroup("pnd_Work_File_Last_Updte_Unit_Cde", "LAST-UPDTE-UNIT-CDE", FieldType.STRING, 
            8);
        pnd_Work_File_Last_Updte_Oprtr_Cde = pnd_Work_File.newFieldInGroup("pnd_Work_File_Last_Updte_Oprtr_Cde", "LAST-UPDTE-OPRTR-CDE", FieldType.STRING, 
            8);
        pnd_Work_File_Pnd_Last_Updte_Dte_Tme = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Last_Updte_Dte_Tme", "#LAST-UPDTE-DTE-TME", FieldType.STRING, 
            15);
        pnd_Work_File_Start_Date = pnd_Work_File.newFieldInGroup("pnd_Work_File_Start_Date", "START-DATE", FieldType.NUMERIC, 8);
        pnd_Work_File_End_Date = pnd_Work_File.newFieldInGroup("pnd_Work_File_End_Date", "END-DATE", FieldType.NUMERIC, 8);
        pnd_Work_File_Dlte_Oprtr_Cde = pnd_Work_File.newFieldInGroup("pnd_Work_File_Dlte_Oprtr_Cde", "DLTE-OPRTR-CDE", FieldType.STRING, 8);
        pnd_Work_File_Pnd_Dlte_Dte_Tme = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Dlte_Dte_Tme", "#DLTE-DTE-TME", FieldType.STRING, 15);
        pnd_Work_File_Ref_Consult_Racf = pnd_Work_File.newFieldInGroup("pnd_Work_File_Ref_Consult_Racf", "REF-CONSULT-RACF", FieldType.STRING, 8);
        pnd_Work_File_Dest_Unit = pnd_Work_File.newFieldInGroup("pnd_Work_File_Dest_Unit", "DEST-UNIT", FieldType.STRING, 8);
        pnd_Work_File_Prjct_Ctgry = pnd_Work_File.newFieldInGroup("pnd_Work_File_Prjct_Ctgry", "PRJCT-CTGRY", FieldType.STRING, 2);
        pnd_Work_File_Prjct_Type = pnd_Work_File.newFieldInGroup("pnd_Work_File_Prjct_Type", "PRJCT-TYPE", FieldType.STRING, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Project.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb3494() throws Exception
    {
        super("Cwfb3494");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        vw_cwf_Project.startDatabaseRead                                                                                                                                  //Natural: READ CWF-PROJECT
        (
        "READ01",
        new Oc[] { new Oc("ISN", "ASC") }
        );
        READ01:
        while (condition(vw_cwf_Project.readNextRow("READ01")))
        {
            //*  REJECT IF CWF-PROJECT.DLTE-DTE-TME GT 0 OR
            //*            CWF-PROJECT.DLTE-OPRTR-CDE GT ' '
            pnd_Work_File.setValuesByName(vw_cwf_Project);                                                                                                                //Natural: MOVE BY NAME CWF-PROJECT TO #WORK-FILE
            pnd_Work_File_Pnd_Entry_Dte_Tme.setValueEdited(cwf_Project_Entry_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                              //Natural: MOVE EDITED ENTRY-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #ENTRY-DTE-TME
            pnd_Work_File_Pnd_Last_Updte_Dte_Tme.setValueEdited(cwf_Project_Last_Updte_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                    //Natural: MOVE EDITED LAST-UPDTE-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #LAST-UPDTE-DTE-TME
            pnd_Work_File_Pnd_Dlte_Dte_Tme.setValueEdited(cwf_Project_Dlte_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                                //Natural: MOVE EDITED DLTE-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #DLTE-DTE-TME
            getWorkFiles().write(1, false, pnd_Work_File);                                                                                                                //Natural: WRITE WORK FILE 1 #WORK-FILE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //
}
