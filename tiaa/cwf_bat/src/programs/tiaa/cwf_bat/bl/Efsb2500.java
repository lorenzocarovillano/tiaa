/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:06:42 PM
**        * FROM NATURAL PROGRAM : Efsb2500
************************************************************
**        * FILE NAME            : Efsb2500.java
**        * CLASS NAME           : Efsb2500
**        * INSTANCE NAME        : Efsb2500
************************************************************
************************************************************************
* PROGRAM  : EFSB2500
* SYSTEM   : CWF/EFM
* TITLE    : PRINT INCOMPLETE WORKSHEETS THAT ARE PARTICIPANT CLOSED.
* WRITTEN  : MAY 22, 1995
* FUNCTION : THIS PROGRAM WILL PRINT ALL INCOMPLETE WORKSHEETS.
*
* HISTORY
* 10/14/96  BE  ADDED AN ESCAPE ROUTINE TO NO LONGER ACCESS/READ
*               CWF-WP-DCMNT-TYPE-TBL-VIEW IF DESC. ALREADY EXISTS
*               ON #DOC-NAMES-ARR.
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsb2500 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Efm_Wks_Tran;
    private DbsField cwf_Efm_Wks_Tran_Kdo_Entry_Dte_Tme;
    private DbsField cwf_Efm_Wks_Tran_Doc_Entry_Dte_Tme;
    private DbsField cwf_Efm_Wks_Tran_Kdo_Status;
    private DbsField cwf_Efm_Wks_Tran_Kdo_Pin_Nbr;
    private DbsField cwf_Efm_Wks_Tran_Kdo_Mit_Work_Rqst;
    private DbsField cwf_Efm_Wks_Tran_Last_Updte_Oprtr_Cde;

    private DataAccessProgramView vw_cwf_Master_Index_View;
    private DbsField cwf_Master_Index_View_Rqst_Log_Dte_Tme;
    private DbsField cwf_Master_Index_View_Physcl_Fldr_Id_Nbr;
    private DbsField cwf_Master_Index_View_Mj_Pull_Ind;
    private DbsField cwf_Master_Index_View_Unit_Cde;
    private DbsField cwf_Master_Index_View_Admin_Status_Cde;
    private DbsField cwf_Master_Index_View_Tiaa_Rcvd_Dte;
    private DbsField cwf_Master_Index_View_Multi_Rqst_Ind;
    private DbsField cwf_Master_Index_View_Work_Prcss_Id;
    private DbsField cwf_Master_Index_View_Rqst_Id;

    private DbsGroup cwf_Master_Index_View__R_Field_1;
    private DbsField cwf_Master_Index_View_Rqst_Work_Prcss_Id;
    private DbsField cwf_Master_Index_View_Rqst_Tiaa_Rcvd_Dte;
    private DbsField cwf_Master_Index_View_Rqst_Case_Id_Cde;

    private DbsGroup cwf_Master_Index_View__R_Field_2;
    private DbsField cwf_Master_Index_View_Case_Ind;
    private DbsField cwf_Master_Index_View_Subrqst_Cde;
    private DbsField cwf_Master_Index_View_Rqst_Pin_Nbr;
    private DbsField cwf_Master_Index_View_Crprte_Status_Ind;
    private DbsField cwf_Master_Index_View_Status_Freeze_Ind;

    private DataAccessProgramView vw_cwf_Efm_Document_View;
    private DbsField cwf_Efm_Document_View_Document_Category;
    private DbsField cwf_Efm_Document_View_Document_Sub_Category;
    private DbsField cwf_Efm_Document_View_Document_Detail;
    private DbsField cwf_Efm_Document_View_Entry_Empl_Racf_Id;
    private DbsField cwf_Efm_Document_View_Entry_System_Or_Unit;

    private DataAccessProgramView vw_cwf_Wp_Dcmnt_Type_Tbl_View;
    private DbsField cwf_Wp_Dcmnt_Type_Tbl_View_Dcmnt_Type_Cde;
    private DbsField cwf_Wp_Dcmnt_Type_Tbl_View_Dcmnt_Nme;
    private DbsField cwf_Wp_Dcmnt_Type_Tbl_View_Actve_Ind;

    private DbsGroup pnd_Report_Vars;
    private DbsField pnd_Report_Vars_Pnd_Rpt_Unit_Code;
    private DbsField pnd_Report_Vars_Pnd_Rpt_Unit_Desc;
    private DbsField pnd_Report_Vars_Pnd_Rpt_Entry_Oprtr_Name;
    private DbsField pnd_Report_Vars_Pnd_Rpt_Physical_Folder_Id;
    private DbsField pnd_Report_Vars_Pnd_Rpt_Pin_No;
    private DbsField pnd_Report_Vars_Pnd_Rpt_Tiaa_Rcvd_Dte;
    private DbsField pnd_Report_Vars_Pnd_Rpt_Wpid_Desc;
    private DbsField pnd_Report_Vars_Pnd_Rpt_Doc_Desc;
    private DbsField pnd_Report_Vars_Pnd_Rpt_Corp_Status;
    private DbsField pnd_Unit_Total;
    private DbsField pnd_Grand_Total;
    private DbsField pnd_Actv_Unque_Key;

    private DbsGroup pnd_Actv_Unque_Key__R_Field_3;
    private DbsField pnd_Actv_Unque_Key_Pnd_Rqst_Log_Dte_Tme;
    private DbsField pnd_Actv_Unque_Key_Pnd_Actve_Ind;
    private DbsField pnd_Physcl_Fldr_Id_Nbr;

    private DbsGroup pnd_Physcl_Fldr_Id_Nbr__R_Field_4;
    private DbsField pnd_Physcl_Fldr_Id_Nbr_Pnd_Prefix;
    private DbsField pnd_Physcl_Fldr_Id_Nbr_Pnd_Physcl_Fldr_2_7;

    private DbsGroup pnd_Parms_Cwfn0013;
    private DbsField pnd_Parms_Cwfn0013_Pnd_Wpid;
    private DbsField pnd_Parms_Cwfn0013_Pnd_Old_Rt_Cde;
    private DbsField pnd_Parms_Cwfn0013_Pnd_Work_Prcss_Long_Nme;
    private DbsField pnd_Parms_Cwfn0013_Pnd_Work_Prcss_Short_Nme;

    private DbsGroup pnd_Parms_Cwfn1103;
    private DbsField pnd_Parms_Cwfn1103_Pnd_Unit_Cde;
    private DbsField pnd_Parms_Cwfn1103_Pnd_Unit_Name;

    private DbsGroup pnd_Parms_Cwfn1107;
    private DbsField pnd_Parms_Cwfn1107_Pnd_Parm_Racf_Id;
    private DbsField pnd_Parms_Cwfn1107_Pnd_Parm_Empl_Nme;
    private DbsField pnd_Doc_Key;

    private DbsGroup pnd_Doc_Key__R_Field_5;
    private DbsField pnd_Doc_Key_Pnd_Cabinet_Id;

    private DbsGroup pnd_Doc_Key__R_Field_6;
    private DbsField pnd_Doc_Key_Pnd_Cabinet_Prefix;
    private DbsField pnd_Doc_Key_Pnd_Cabinet_Pin_No;
    private DbsField pnd_Doc_Key_Pnd_Tiaa_Rcvd_Dte;
    private DbsField pnd_Doc_Key_Pnd_Case_Wpid_Multi_Subrqst;

    private DbsGroup pnd_Doc_Key__R_Field_7;
    private DbsField pnd_Doc_Key_Pnd_Case_Ind;
    private DbsField pnd_Doc_Key_Pnd_D_Wpid;
    private DbsField pnd_Doc_Key_Pnd_Multi;
    private DbsField pnd_Doc_Key_Pnd_Subrqst;
    private DbsField pnd_Doc_Key_Pnd_Entry_Dte_Tme;
    private DbsField pnd_Curr_Rpt_Unit_Code;
    private DbsField pnd_Curr_Rpt_Unit_Desc;
    private DbsField pnd_Doc_Types_Arr;
    private DbsField pnd_Doc_Names_Arr;
    private DbsField pnd_Doc_Types_Ctr;
    private DbsField pnd_Doc_Type;

    private DbsGroup pnd_Doc_Type__R_Field_8;
    private DbsField pnd_Doc_Type_Pnd_Doc_Category;
    private DbsField pnd_Doc_Type_Pnd_Doc_Sub_Category;
    private DbsField pnd_Doc_Type_Pnd_Doc_Detail;
    private DbsField pnd_Index;

    private DbsRecord internalLoopRecord;
    private DbsField sort01Pnd_Rpt_Unit_CodeOld;
    private DbsField sort01Pnd_Rpt_Unit_DescOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Efm_Wks_Tran = new DataAccessProgramView(new NameInfo("vw_cwf_Efm_Wks_Tran", "CWF-EFM-WKS-TRAN"), "CWF_EFM_WKS_TRAN", "CWF_EFM_WKS_TRAN");
        cwf_Efm_Wks_Tran_Kdo_Entry_Dte_Tme = vw_cwf_Efm_Wks_Tran.getRecord().newFieldInGroup("cwf_Efm_Wks_Tran_Kdo_Entry_Dte_Tme", "KDO-ENTRY-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "KDO_ENTRY_DTE_TME");
        cwf_Efm_Wks_Tran_Kdo_Entry_Dte_Tme.setDdmHeader("ENTRY/DATE TIME");
        cwf_Efm_Wks_Tran_Doc_Entry_Dte_Tme = vw_cwf_Efm_Wks_Tran.getRecord().newFieldInGroup("cwf_Efm_Wks_Tran_Doc_Entry_Dte_Tme", "DOC-ENTRY-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "DOC_ENTRY_DTE_TME");
        cwf_Efm_Wks_Tran_Doc_Entry_Dte_Tme.setDdmHeader("DOC. ENTRY/DATE TIME");
        cwf_Efm_Wks_Tran_Kdo_Status = vw_cwf_Efm_Wks_Tran.getRecord().newFieldInGroup("cwf_Efm_Wks_Tran_Kdo_Status", "KDO-STATUS", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "KDO_STATUS");
        cwf_Efm_Wks_Tran_Kdo_Status.setDdmHeader("STATUS");
        cwf_Efm_Wks_Tran_Kdo_Pin_Nbr = vw_cwf_Efm_Wks_Tran.getRecord().newFieldInGroup("cwf_Efm_Wks_Tran_Kdo_Pin_Nbr", "KDO-PIN-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "KDO_PIN_NBR");
        cwf_Efm_Wks_Tran_Kdo_Pin_Nbr.setDdmHeader("PIN");
        cwf_Efm_Wks_Tran_Kdo_Mit_Work_Rqst = vw_cwf_Efm_Wks_Tran.getRecord().newFieldInGroup("cwf_Efm_Wks_Tran_Kdo_Mit_Work_Rqst", "KDO-MIT-WORK-RQST", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "KDO_MIT_WORK_RQST");
        cwf_Efm_Wks_Tran_Kdo_Mit_Work_Rqst.setDdmHeader("LOG/DATE TIME");
        cwf_Efm_Wks_Tran_Last_Updte_Oprtr_Cde = vw_cwf_Efm_Wks_Tran.getRecord().newFieldInGroup("cwf_Efm_Wks_Tran_Last_Updte_Oprtr_Cde", "LAST-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_UPDTE_OPRTR_CDE");
        registerRecord(vw_cwf_Efm_Wks_Tran);

        vw_cwf_Master_Index_View = new DataAccessProgramView(new NameInfo("vw_cwf_Master_Index_View", "CWF-MASTER-INDEX-VIEW"), "CWF_MASTER_INDEX_VIEW", 
            "CWF_MASTER_INDEX");
        cwf_Master_Index_View_Rqst_Log_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Master_Index_View_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        cwf_Master_Index_View_Physcl_Fldr_Id_Nbr = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Physcl_Fldr_Id_Nbr", "PHYSCL-FLDR-ID-NBR", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "PHYSCL_FLDR_ID_NBR");
        cwf_Master_Index_View_Physcl_Fldr_Id_Nbr.setDdmHeader("PHYSICAL FOLDER/ID NUMBER");
        cwf_Master_Index_View_Mj_Pull_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Mj_Pull_Ind", "MJ-PULL-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "MJ_PULL_IND");
        cwf_Master_Index_View_Mj_Pull_Ind.setDdmHeader("MJ PULL/IND");
        cwf_Master_Index_View_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Unit_Cde", "UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "UNIT_CDE");
        cwf_Master_Index_View_Unit_Cde.setDdmHeader("UNIT/CODE");
        cwf_Master_Index_View_Admin_Status_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Admin_Status_Cde", "ADMIN-STATUS-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "ADMIN_STATUS_CDE");
        cwf_Master_Index_View_Tiaa_Rcvd_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_RCVD_DTE");
        cwf_Master_Index_View_Multi_Rqst_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Multi_Rqst_Ind", "MULTI-RQST-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "MULTI_RQST_IND");
        cwf_Master_Index_View_Multi_Rqst_Ind.setDdmHeader("MULTI/IND");
        cwf_Master_Index_View_Work_Prcss_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Work_Prcss_Id", "WORK-PRCSS-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        cwf_Master_Index_View_Work_Prcss_Id.setDdmHeader("WORK/ID");
        cwf_Master_Index_View_Rqst_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Id", "RQST-ID", FieldType.STRING, 
            28, RepeatingFieldStrategy.None, "RQST_ID");
        cwf_Master_Index_View_Rqst_Id.setDdmHeader("REQUEST ID");

        cwf_Master_Index_View__R_Field_1 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_1", "REDEFINE", cwf_Master_Index_View_Rqst_Id);
        cwf_Master_Index_View_Rqst_Work_Prcss_Id = cwf_Master_Index_View__R_Field_1.newFieldInGroup("cwf_Master_Index_View_Rqst_Work_Prcss_Id", "RQST-WORK-PRCSS-ID", 
            FieldType.STRING, 6);
        cwf_Master_Index_View_Rqst_Tiaa_Rcvd_Dte = cwf_Master_Index_View__R_Field_1.newFieldInGroup("cwf_Master_Index_View_Rqst_Tiaa_Rcvd_Dte", "RQST-TIAA-RCVD-DTE", 
            FieldType.STRING, 8);
        cwf_Master_Index_View_Rqst_Case_Id_Cde = cwf_Master_Index_View__R_Field_1.newFieldInGroup("cwf_Master_Index_View_Rqst_Case_Id_Cde", "RQST-CASE-ID-CDE", 
            FieldType.STRING, 2);

        cwf_Master_Index_View__R_Field_2 = cwf_Master_Index_View__R_Field_1.newGroupInGroup("cwf_Master_Index_View__R_Field_2", "REDEFINE", cwf_Master_Index_View_Rqst_Case_Id_Cde);
        cwf_Master_Index_View_Case_Ind = cwf_Master_Index_View__R_Field_2.newFieldInGroup("cwf_Master_Index_View_Case_Ind", "CASE-IND", FieldType.STRING, 
            1);
        cwf_Master_Index_View_Subrqst_Cde = cwf_Master_Index_View__R_Field_2.newFieldInGroup("cwf_Master_Index_View_Subrqst_Cde", "SUBRQST-CDE", FieldType.STRING, 
            1);
        cwf_Master_Index_View_Rqst_Pin_Nbr = cwf_Master_Index_View__R_Field_1.newFieldInGroup("cwf_Master_Index_View_Rqst_Pin_Nbr", "RQST-PIN-NBR", FieldType.NUMERIC, 
            12);
        cwf_Master_Index_View_Crprte_Status_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Crprte_Status_Ind", "CRPRTE-STATUS-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CRPRTE_STATUS_IND");
        cwf_Master_Index_View_Crprte_Status_Ind.setDdmHeader("CORP/STAT");
        cwf_Master_Index_View_Status_Freeze_Ind = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Status_Freeze_Ind", "STATUS-FREEZE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "STATUS_FREEZE_IND");
        cwf_Master_Index_View_Status_Freeze_Ind.setDdmHeader("FREEZE/STATUS");
        registerRecord(vw_cwf_Master_Index_View);

        vw_cwf_Efm_Document_View = new DataAccessProgramView(new NameInfo("vw_cwf_Efm_Document_View", "CWF-EFM-DOCUMENT-VIEW"), "CWF_EFM_DOCUMENT", "CWF_EFM_DOCUMENT");
        cwf_Efm_Document_View_Document_Category = vw_cwf_Efm_Document_View.getRecord().newFieldInGroup("cwf_Efm_Document_View_Document_Category", "DOCUMENT-CATEGORY", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "DOCUMENT_CATEGORY");
        cwf_Efm_Document_View_Document_Category.setDdmHeader("DOC/CAT");
        cwf_Efm_Document_View_Document_Sub_Category = vw_cwf_Efm_Document_View.getRecord().newFieldInGroup("cwf_Efm_Document_View_Document_Sub_Category", 
            "DOCUMENT-SUB-CATEGORY", FieldType.STRING, 3, RepeatingFieldStrategy.None, "DOCUMENT_SUB_CATEGORY");
        cwf_Efm_Document_View_Document_Sub_Category.setDdmHeader("DOC SUB-/CATEGORY");
        cwf_Efm_Document_View_Document_Detail = vw_cwf_Efm_Document_View.getRecord().newFieldInGroup("cwf_Efm_Document_View_Document_Detail", "DOCUMENT-DETAIL", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "DOCUMENT_DETAIL");
        cwf_Efm_Document_View_Document_Detail.setDdmHeader("DOCUMENT/DETAIL");
        cwf_Efm_Document_View_Entry_Empl_Racf_Id = vw_cwf_Efm_Document_View.getRecord().newFieldInGroup("cwf_Efm_Document_View_Entry_Empl_Racf_Id", "ENTRY-EMPL-RACF-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ENTRY_EMPL_RACF_ID");
        cwf_Efm_Document_View_Entry_Empl_Racf_Id.setDdmHeader("CREATED BY/RACF ID");
        cwf_Efm_Document_View_Entry_System_Or_Unit = vw_cwf_Efm_Document_View.getRecord().newFieldInGroup("cwf_Efm_Document_View_Entry_System_Or_Unit", 
            "ENTRY-SYSTEM-OR-UNIT", FieldType.STRING, 8, RepeatingFieldStrategy.None, "ENTRY_SYSTEM_OR_UNIT");
        cwf_Efm_Document_View_Entry_System_Or_Unit.setDdmHeader("CREATED BY/SYSTEM OR UNIT");
        registerRecord(vw_cwf_Efm_Document_View);

        vw_cwf_Wp_Dcmnt_Type_Tbl_View = new DataAccessProgramView(new NameInfo("vw_cwf_Wp_Dcmnt_Type_Tbl_View", "CWF-WP-DCMNT-TYPE-TBL-VIEW"), "CWF_WP_DCMNT_TYPE_TBL", 
            "CWF_PROFILE");
        cwf_Wp_Dcmnt_Type_Tbl_View_Dcmnt_Type_Cde = vw_cwf_Wp_Dcmnt_Type_Tbl_View.getRecord().newFieldInGroup("cwf_Wp_Dcmnt_Type_Tbl_View_Dcmnt_Type_Cde", 
            "DCMNT-TYPE-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "DCMNT_TYPE_CDE");
        cwf_Wp_Dcmnt_Type_Tbl_View_Dcmnt_Type_Cde.setDdmHeader("DOCTYPE");
        cwf_Wp_Dcmnt_Type_Tbl_View_Dcmnt_Nme = vw_cwf_Wp_Dcmnt_Type_Tbl_View.getRecord().newFieldInGroup("cwf_Wp_Dcmnt_Type_Tbl_View_Dcmnt_Nme", "DCMNT-NME", 
            FieldType.STRING, 45, RepeatingFieldStrategy.None, "DCMNT_NME");
        cwf_Wp_Dcmnt_Type_Tbl_View_Dcmnt_Nme.setDdmHeader("DOCNAME");
        cwf_Wp_Dcmnt_Type_Tbl_View_Actve_Ind = vw_cwf_Wp_Dcmnt_Type_Tbl_View.getRecord().newFieldInGroup("cwf_Wp_Dcmnt_Type_Tbl_View_Actve_Ind", "ACTVE-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "ACTVE_IND");
        registerRecord(vw_cwf_Wp_Dcmnt_Type_Tbl_View);

        pnd_Report_Vars = localVariables.newGroupInRecord("pnd_Report_Vars", "#REPORT-VARS");
        pnd_Report_Vars_Pnd_Rpt_Unit_Code = pnd_Report_Vars.newFieldInGroup("pnd_Report_Vars_Pnd_Rpt_Unit_Code", "#RPT-UNIT-CODE", FieldType.STRING, 8);
        pnd_Report_Vars_Pnd_Rpt_Unit_Desc = pnd_Report_Vars.newFieldInGroup("pnd_Report_Vars_Pnd_Rpt_Unit_Desc", "#RPT-UNIT-DESC", FieldType.STRING, 80);
        pnd_Report_Vars_Pnd_Rpt_Entry_Oprtr_Name = pnd_Report_Vars.newFieldInGroup("pnd_Report_Vars_Pnd_Rpt_Entry_Oprtr_Name", "#RPT-ENTRY-OPRTR-NAME", 
            FieldType.STRING, 30);
        pnd_Report_Vars_Pnd_Rpt_Physical_Folder_Id = pnd_Report_Vars.newFieldInGroup("pnd_Report_Vars_Pnd_Rpt_Physical_Folder_Id", "#RPT-PHYSICAL-FOLDER-ID", 
            FieldType.STRING, 7);
        pnd_Report_Vars_Pnd_Rpt_Pin_No = pnd_Report_Vars.newFieldInGroup("pnd_Report_Vars_Pnd_Rpt_Pin_No", "#RPT-PIN-NO", FieldType.NUMERIC, 12);
        pnd_Report_Vars_Pnd_Rpt_Tiaa_Rcvd_Dte = pnd_Report_Vars.newFieldInGroup("pnd_Report_Vars_Pnd_Rpt_Tiaa_Rcvd_Dte", "#RPT-TIAA-RCVD-DTE", FieldType.STRING, 
            8);
        pnd_Report_Vars_Pnd_Rpt_Wpid_Desc = pnd_Report_Vars.newFieldInGroup("pnd_Report_Vars_Pnd_Rpt_Wpid_Desc", "#RPT-WPID-DESC", FieldType.STRING, 15);
        pnd_Report_Vars_Pnd_Rpt_Doc_Desc = pnd_Report_Vars.newFieldInGroup("pnd_Report_Vars_Pnd_Rpt_Doc_Desc", "#RPT-DOC-DESC", FieldType.STRING, 30);
        pnd_Report_Vars_Pnd_Rpt_Corp_Status = pnd_Report_Vars.newFieldInGroup("pnd_Report_Vars_Pnd_Rpt_Corp_Status", "#RPT-CORP-STATUS", FieldType.STRING, 
            6);
        pnd_Unit_Total = localVariables.newFieldInRecord("pnd_Unit_Total", "#UNIT-TOTAL", FieldType.NUMERIC, 7);
        pnd_Grand_Total = localVariables.newFieldInRecord("pnd_Grand_Total", "#GRAND-TOTAL", FieldType.NUMERIC, 7);
        pnd_Actv_Unque_Key = localVariables.newFieldInRecord("pnd_Actv_Unque_Key", "#ACTV-UNQUE-KEY", FieldType.STRING, 16);

        pnd_Actv_Unque_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Actv_Unque_Key__R_Field_3", "REDEFINE", pnd_Actv_Unque_Key);
        pnd_Actv_Unque_Key_Pnd_Rqst_Log_Dte_Tme = pnd_Actv_Unque_Key__R_Field_3.newFieldInGroup("pnd_Actv_Unque_Key_Pnd_Rqst_Log_Dte_Tme", "#RQST-LOG-DTE-TME", 
            FieldType.STRING, 15);
        pnd_Actv_Unque_Key_Pnd_Actve_Ind = pnd_Actv_Unque_Key__R_Field_3.newFieldInGroup("pnd_Actv_Unque_Key_Pnd_Actve_Ind", "#ACTVE-IND", FieldType.STRING, 
            1);
        pnd_Physcl_Fldr_Id_Nbr = localVariables.newFieldInRecord("pnd_Physcl_Fldr_Id_Nbr", "#PHYSCL-FLDR-ID-NBR", FieldType.STRING, 7);

        pnd_Physcl_Fldr_Id_Nbr__R_Field_4 = localVariables.newGroupInRecord("pnd_Physcl_Fldr_Id_Nbr__R_Field_4", "REDEFINE", pnd_Physcl_Fldr_Id_Nbr);
        pnd_Physcl_Fldr_Id_Nbr_Pnd_Prefix = pnd_Physcl_Fldr_Id_Nbr__R_Field_4.newFieldInGroup("pnd_Physcl_Fldr_Id_Nbr_Pnd_Prefix", "#PREFIX", FieldType.STRING, 
            1);
        pnd_Physcl_Fldr_Id_Nbr_Pnd_Physcl_Fldr_2_7 = pnd_Physcl_Fldr_Id_Nbr__R_Field_4.newFieldInGroup("pnd_Physcl_Fldr_Id_Nbr_Pnd_Physcl_Fldr_2_7", "#PHYSCL-FLDR-2-7", 
            FieldType.STRING, 6);

        pnd_Parms_Cwfn0013 = localVariables.newGroupInRecord("pnd_Parms_Cwfn0013", "#PARMS-CWFN0013");
        pnd_Parms_Cwfn0013_Pnd_Wpid = pnd_Parms_Cwfn0013.newFieldInGroup("pnd_Parms_Cwfn0013_Pnd_Wpid", "#WPID", FieldType.STRING, 6);
        pnd_Parms_Cwfn0013_Pnd_Old_Rt_Cde = pnd_Parms_Cwfn0013.newFieldInGroup("pnd_Parms_Cwfn0013_Pnd_Old_Rt_Cde", "#OLD-RT-CDE", FieldType.STRING, 4);
        pnd_Parms_Cwfn0013_Pnd_Work_Prcss_Long_Nme = pnd_Parms_Cwfn0013.newFieldInGroup("pnd_Parms_Cwfn0013_Pnd_Work_Prcss_Long_Nme", "#WORK-PRCSS-LONG-NME", 
            FieldType.STRING, 45);
        pnd_Parms_Cwfn0013_Pnd_Work_Prcss_Short_Nme = pnd_Parms_Cwfn0013.newFieldInGroup("pnd_Parms_Cwfn0013_Pnd_Work_Prcss_Short_Nme", "#WORK-PRCSS-SHORT-NME", 
            FieldType.STRING, 15);

        pnd_Parms_Cwfn1103 = localVariables.newGroupInRecord("pnd_Parms_Cwfn1103", "#PARMS-CWFN1103");
        pnd_Parms_Cwfn1103_Pnd_Unit_Cde = pnd_Parms_Cwfn1103.newFieldInGroup("pnd_Parms_Cwfn1103_Pnd_Unit_Cde", "#UNIT-CDE", FieldType.STRING, 8);
        pnd_Parms_Cwfn1103_Pnd_Unit_Name = pnd_Parms_Cwfn1103.newFieldInGroup("pnd_Parms_Cwfn1103_Pnd_Unit_Name", "#UNIT-NAME", FieldType.STRING, 45);

        pnd_Parms_Cwfn1107 = localVariables.newGroupInRecord("pnd_Parms_Cwfn1107", "#PARMS-CWFN1107");
        pnd_Parms_Cwfn1107_Pnd_Parm_Racf_Id = pnd_Parms_Cwfn1107.newFieldInGroup("pnd_Parms_Cwfn1107_Pnd_Parm_Racf_Id", "#PARM-RACF-ID", FieldType.STRING, 
            8);
        pnd_Parms_Cwfn1107_Pnd_Parm_Empl_Nme = pnd_Parms_Cwfn1107.newFieldInGroup("pnd_Parms_Cwfn1107_Pnd_Parm_Empl_Nme", "#PARM-EMPL-NME", FieldType.STRING, 
            30);
        pnd_Doc_Key = localVariables.newFieldInRecord("pnd_Doc_Key", "#DOC-KEY", FieldType.STRING, 34);

        pnd_Doc_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Doc_Key__R_Field_5", "REDEFINE", pnd_Doc_Key);
        pnd_Doc_Key_Pnd_Cabinet_Id = pnd_Doc_Key__R_Field_5.newFieldInGroup("pnd_Doc_Key_Pnd_Cabinet_Id", "#CABINET-ID", FieldType.STRING, 14);

        pnd_Doc_Key__R_Field_6 = pnd_Doc_Key__R_Field_5.newGroupInGroup("pnd_Doc_Key__R_Field_6", "REDEFINE", pnd_Doc_Key_Pnd_Cabinet_Id);
        pnd_Doc_Key_Pnd_Cabinet_Prefix = pnd_Doc_Key__R_Field_6.newFieldInGroup("pnd_Doc_Key_Pnd_Cabinet_Prefix", "#CABINET-PREFIX", FieldType.STRING, 
            1);
        pnd_Doc_Key_Pnd_Cabinet_Pin_No = pnd_Doc_Key__R_Field_6.newFieldInGroup("pnd_Doc_Key_Pnd_Cabinet_Pin_No", "#CABINET-PIN-NO", FieldType.STRING, 
            12);
        pnd_Doc_Key_Pnd_Tiaa_Rcvd_Dte = pnd_Doc_Key__R_Field_5.newFieldInGroup("pnd_Doc_Key_Pnd_Tiaa_Rcvd_Dte", "#TIAA-RCVD-DTE", FieldType.DATE);
        pnd_Doc_Key_Pnd_Case_Wpid_Multi_Subrqst = pnd_Doc_Key__R_Field_5.newFieldInGroup("pnd_Doc_Key_Pnd_Case_Wpid_Multi_Subrqst", "#CASE-WPID-MULTI-SUBRQST", 
            FieldType.STRING, 9);

        pnd_Doc_Key__R_Field_7 = pnd_Doc_Key__R_Field_5.newGroupInGroup("pnd_Doc_Key__R_Field_7", "REDEFINE", pnd_Doc_Key_Pnd_Case_Wpid_Multi_Subrqst);
        pnd_Doc_Key_Pnd_Case_Ind = pnd_Doc_Key__R_Field_7.newFieldInGroup("pnd_Doc_Key_Pnd_Case_Ind", "#CASE-IND", FieldType.STRING, 1);
        pnd_Doc_Key_Pnd_D_Wpid = pnd_Doc_Key__R_Field_7.newFieldInGroup("pnd_Doc_Key_Pnd_D_Wpid", "#D-WPID", FieldType.STRING, 6);
        pnd_Doc_Key_Pnd_Multi = pnd_Doc_Key__R_Field_7.newFieldInGroup("pnd_Doc_Key_Pnd_Multi", "#MULTI", FieldType.STRING, 1);
        pnd_Doc_Key_Pnd_Subrqst = pnd_Doc_Key__R_Field_7.newFieldInGroup("pnd_Doc_Key_Pnd_Subrqst", "#SUBRQST", FieldType.STRING, 1);
        pnd_Doc_Key_Pnd_Entry_Dte_Tme = pnd_Doc_Key__R_Field_5.newFieldInGroup("pnd_Doc_Key_Pnd_Entry_Dte_Tme", "#ENTRY-DTE-TME", FieldType.TIME);
        pnd_Curr_Rpt_Unit_Code = localVariables.newFieldInRecord("pnd_Curr_Rpt_Unit_Code", "#CURR-RPT-UNIT-CODE", FieldType.STRING, 8);
        pnd_Curr_Rpt_Unit_Desc = localVariables.newFieldInRecord("pnd_Curr_Rpt_Unit_Desc", "#CURR-RPT-UNIT-DESC", FieldType.STRING, 80);
        pnd_Doc_Types_Arr = localVariables.newFieldArrayInRecord("pnd_Doc_Types_Arr", "#DOC-TYPES-ARR", FieldType.STRING, 8, new DbsArrayController(1, 
            100));
        pnd_Doc_Names_Arr = localVariables.newFieldArrayInRecord("pnd_Doc_Names_Arr", "#DOC-NAMES-ARR", FieldType.STRING, 30, new DbsArrayController(1, 
            100));
        pnd_Doc_Types_Ctr = localVariables.newFieldInRecord("pnd_Doc_Types_Ctr", "#DOC-TYPES-CTR", FieldType.PACKED_DECIMAL, 4);
        pnd_Doc_Type = localVariables.newFieldInRecord("pnd_Doc_Type", "#DOC-TYPE", FieldType.STRING, 8);

        pnd_Doc_Type__R_Field_8 = localVariables.newGroupInRecord("pnd_Doc_Type__R_Field_8", "REDEFINE", pnd_Doc_Type);
        pnd_Doc_Type_Pnd_Doc_Category = pnd_Doc_Type__R_Field_8.newFieldInGroup("pnd_Doc_Type_Pnd_Doc_Category", "#DOC-CATEGORY", FieldType.STRING, 1);
        pnd_Doc_Type_Pnd_Doc_Sub_Category = pnd_Doc_Type__R_Field_8.newFieldInGroup("pnd_Doc_Type_Pnd_Doc_Sub_Category", "#DOC-SUB-CATEGORY", FieldType.STRING, 
            3);
        pnd_Doc_Type_Pnd_Doc_Detail = pnd_Doc_Type__R_Field_8.newFieldInGroup("pnd_Doc_Type_Pnd_Doc_Detail", "#DOC-DETAIL", FieldType.STRING, 4);
        pnd_Index = localVariables.newFieldInRecord("pnd_Index", "#INDEX", FieldType.PACKED_DECIMAL, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        sort01Pnd_Rpt_Unit_CodeOld = internalLoopRecord.newFieldInRecord("Sort01_Pnd_Rpt_Unit_Code_OLD", "Pnd_Rpt_Unit_Code_OLD", FieldType.STRING, 8);
        sort01Pnd_Rpt_Unit_DescOld = internalLoopRecord.newFieldInRecord("Sort01_Pnd_Rpt_Unit_Desc_OLD", "Pnd_Rpt_Unit_Desc_OLD", FieldType.STRING, 80);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Efm_Wks_Tran.reset();
        vw_cwf_Master_Index_View.reset();
        vw_cwf_Efm_Document_View.reset();
        vw_cwf_Wp_Dcmnt_Type_Tbl_View.reset();
        internalLoopRecord.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Efsb2500() throws Exception
    {
        super("Efsb2500");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 55 LS = 132
        vw_cwf_Efm_Wks_Tran.startDatabaseRead                                                                                                                             //Natural: READ CWF-EFM-WKS-TRAN BY KDO-WORKSHEET-KEY
        (
        "READ01",
        new Oc[] { new Oc("KDO_WORKSHEET_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_cwf_Efm_Wks_Tran.readNextRow("READ01")))
        {
            if (condition(cwf_Efm_Wks_Tran_Kdo_Status.notEquals("I") || cwf_Efm_Wks_Tran_Kdo_Pin_Nbr.equals(1901802) || cwf_Efm_Wks_Tran_Kdo_Pin_Nbr.equals(2851169)      //Natural: REJECT IF ( KDO-STATUS NE 'I' ) OR ( KDO-PIN-NBR = 1901802 OR = 2851169 OR = 2851222 )
                || cwf_Efm_Wks_Tran_Kdo_Pin_Nbr.equals(2851222)))
            {
                continue;
            }
            pnd_Actv_Unque_Key_Pnd_Actve_Ind.reset();                                                                                                                     //Natural: RESET #ACTVE-IND #REPORT-VARS
            pnd_Report_Vars.reset();
            pnd_Actv_Unque_Key_Pnd_Rqst_Log_Dte_Tme.setValue(cwf_Efm_Wks_Tran_Kdo_Mit_Work_Rqst);                                                                         //Natural: ASSIGN #RQST-LOG-DTE-TME := KDO-MIT-WORK-RQST
            vw_cwf_Master_Index_View.startDatabaseRead                                                                                                                    //Natural: READ ( 1 ) CWF-MASTER-INDEX-VIEW BY ACTV-UNQUE-KEY STARTING FROM #ACTV-UNQUE-KEY
            (
            "READ02",
            new Wc[] { new Wc("ACTV_UNQUE_KEY", ">=", pnd_Actv_Unque_Key, WcType.BY) },
            new Oc[] { new Oc("ACTV_UNQUE_KEY", "ASC") },
            1
            );
            READ02:
            while (condition(vw_cwf_Master_Index_View.readNextRow("READ02")))
            {
                if (condition(cwf_Master_Index_View_Rqst_Log_Dte_Tme.notEquals(pnd_Actv_Unque_Key_Pnd_Rqst_Log_Dte_Tme) || cwf_Master_Index_View_Status_Freeze_Ind.equals(" "))) //Natural: IF RQST-LOG-DTE-TME NE #RQST-LOG-DTE-TME OR STATUS-FREEZE-IND = ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                pnd_Report_Vars_Pnd_Rpt_Pin_No.setValue(cwf_Efm_Wks_Tran_Kdo_Pin_Nbr);                                                                                    //Natural: ASSIGN #RPT-PIN-NO := KDO-PIN-NBR
                if (condition(cwf_Master_Index_View_Physcl_Fldr_Id_Nbr.greater(getZero())))                                                                               //Natural: IF PHYSCL-FLDR-ID-NBR > 0
                {
                    if (condition(cwf_Master_Index_View_Mj_Pull_Ind.equals("N")))                                                                                         //Natural: IF MJ-PULL-IND = 'N'
                    {
                        pnd_Physcl_Fldr_Id_Nbr_Pnd_Prefix.setValue("F");                                                                                                  //Natural: MOVE 'F' TO #PREFIX
                        pnd_Physcl_Fldr_Id_Nbr_Pnd_Physcl_Fldr_2_7.setValue(cwf_Master_Index_View_Physcl_Fldr_Id_Nbr);                                                    //Natural: MOVE PHYSCL-FLDR-ID-NBR TO #PHYSCL-FLDR-2-7
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Physcl_Fldr_Id_Nbr_Pnd_Prefix.setValue("M");                                                                                                  //Natural: MOVE 'M' TO #PREFIX
                        pnd_Physcl_Fldr_Id_Nbr_Pnd_Physcl_Fldr_2_7.setValue(cwf_Master_Index_View_Physcl_Fldr_Id_Nbr);                                                    //Natural: MOVE PHYSCL-FLDR-ID-NBR TO #PHYSCL-FLDR-2-7
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Physcl_Fldr_Id_Nbr.setValue("NONE");                                                                                                              //Natural: MOVE 'NONE' TO #PHYSCL-FLDR-ID-NBR
                }                                                                                                                                                         //Natural: END-IF
                if (condition(cwf_Master_Index_View_Crprte_Status_Ind.equals("9")))                                                                                       //Natural: IF CRPRTE-STATUS-IND = '9'
                {
                    pnd_Report_Vars_Pnd_Rpt_Corp_Status.setValue("Closed");                                                                                               //Natural: ASSIGN #RPT-CORP-STATUS := 'Closed'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Report_Vars_Pnd_Rpt_Corp_Status.setValue("Open");                                                                                                 //Natural: ASSIGN #RPT-CORP-STATUS := 'Open'
                }                                                                                                                                                         //Natural: END-IF
                pnd_Doc_Key_Pnd_Cabinet_Prefix.setValue("P");                                                                                                             //Natural: ASSIGN #CABINET-PREFIX := 'P'
                pnd_Doc_Key_Pnd_Cabinet_Pin_No.setValue(cwf_Efm_Wks_Tran_Kdo_Pin_Nbr);                                                                                    //Natural: ASSIGN #CABINET-PIN-NO := KDO-PIN-NBR
                pnd_Doc_Key_Pnd_Tiaa_Rcvd_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),cwf_Master_Index_View_Rqst_Tiaa_Rcvd_Dte);                                    //Natural: MOVE EDITED RQST-TIAA-RCVD-DTE TO #TIAA-RCVD-DTE ( EM = YYYYMMDD )
                pnd_Doc_Key_Pnd_Case_Ind.setValue(cwf_Master_Index_View_Case_Ind);                                                                                        //Natural: ASSIGN #CASE-IND := CASE-IND
                pnd_Doc_Key_Pnd_D_Wpid.setValue(cwf_Master_Index_View_Rqst_Work_Prcss_Id);                                                                                //Natural: ASSIGN #D-WPID := RQST-WORK-PRCSS-ID
                pnd_Doc_Key_Pnd_Multi.setValue(cwf_Master_Index_View_Multi_Rqst_Ind);                                                                                     //Natural: ASSIGN #MULTI := MULTI-RQST-IND
                pnd_Doc_Key_Pnd_Subrqst.setValue(cwf_Master_Index_View_Subrqst_Cde);                                                                                      //Natural: ASSIGN #SUBRQST := SUBRQST-CDE
                pnd_Doc_Key_Pnd_Entry_Dte_Tme.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),cwf_Efm_Wks_Tran_Doc_Entry_Dte_Tme);                                   //Natural: MOVE EDITED DOC-ENTRY-DTE-TME TO #ENTRY-DTE-TME ( EM = YYYYMMDDHHIISST )
                vw_cwf_Efm_Document_View.startDatabaseFind                                                                                                                //Natural: FIND CWF-EFM-DOCUMENT-VIEW WITH DOCUMENT-KEY = #DOC-KEY
                (
                "FIND01",
                new Wc[] { new Wc("DOCUMENT_KEY", "=", pnd_Doc_Key.getBinary(), WcType.WITH) }
                );
                FIND01:
                while (condition(vw_cwf_Efm_Document_View.readNextRow("FIND01")))
                {
                    vw_cwf_Efm_Document_View.setIfNotFoundControlFlag(false);
                    pnd_Report_Vars_Pnd_Rpt_Unit_Code.setValue(cwf_Efm_Document_View_Entry_System_Or_Unit);                                                               //Natural: ASSIGN #RPT-UNIT-CODE := ENTRY-SYSTEM-OR-UNIT
                    pnd_Parms_Cwfn1107_Pnd_Parm_Racf_Id.setValue(cwf_Efm_Document_View_Entry_Empl_Racf_Id);                                                               //Natural: ASSIGN #PARM-RACF-ID := ENTRY-EMPL-RACF-ID
                    //*  GET EMPLOYEE NAME
                    DbsUtil.callnat(Cwfn1107.class , getCurrentProcessState(), pnd_Parms_Cwfn1107);                                                                       //Natural: CALLNAT 'CWFN1107' #PARMS-CWFN1107
                    if (condition(Global.isEscape())) return;
                    pnd_Report_Vars_Pnd_Rpt_Entry_Oprtr_Name.setValue(pnd_Parms_Cwfn1107_Pnd_Parm_Empl_Nme);                                                              //Natural: ASSIGN #RPT-ENTRY-OPRTR-NAME := #PARM-EMPL-NME
                    pnd_Doc_Type_Pnd_Doc_Category.setValue(cwf_Efm_Document_View_Document_Category);                                                                      //Natural: ASSIGN #DOC-CATEGORY := DOCUMENT-CATEGORY
                    pnd_Doc_Type_Pnd_Doc_Sub_Category.setValue(cwf_Efm_Document_View_Document_Sub_Category);                                                              //Natural: ASSIGN #DOC-SUB-CATEGORY := DOCUMENT-SUB-CATEGORY
                    pnd_Doc_Type_Pnd_Doc_Detail.setValue(cwf_Efm_Document_View_Document_Detail);                                                                          //Natural: ASSIGN #DOC-DETAIL := DOCUMENT-DETAIL
                                                                                                                                                                          //Natural: PERFORM GET-DOCUMENT-NAME
                    sub_Get_Document_Name();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Parms_Cwfn0013_Pnd_Wpid.setValue(cwf_Master_Index_View_Work_Prcss_Id);                                                                                //Natural: ASSIGN #WPID := WORK-PRCSS-ID
                //*  GET WPID-DESC
                DbsUtil.callnat(Cwfn0013.class , getCurrentProcessState(), pnd_Parms_Cwfn0013);                                                                           //Natural: CALLNAT 'CWFN0013' #PARMS-CWFN0013
                if (condition(Global.isEscape())) return;
                pnd_Parms_Cwfn1103_Pnd_Unit_Cde.setValue(pnd_Report_Vars_Pnd_Rpt_Unit_Code);                                                                              //Natural: ASSIGN #UNIT-CDE := #RPT-UNIT-CODE
                //*  GET UNIT DESC
                DbsUtil.callnat(Cwfn1103.class , getCurrentProcessState(), pnd_Parms_Cwfn1103);                                                                           //Natural: CALLNAT 'CWFN1103' #PARMS-CWFN1103
                if (condition(Global.isEscape())) return;
                pnd_Report_Vars_Pnd_Rpt_Unit_Desc.setValue(pnd_Parms_Cwfn1103_Pnd_Unit_Name);                                                                             //Natural: ASSIGN #RPT-UNIT-DESC := #UNIT-NAME
                pnd_Report_Vars_Pnd_Rpt_Physical_Folder_Id.setValue(pnd_Physcl_Fldr_Id_Nbr);                                                                              //Natural: ASSIGN #RPT-PHYSICAL-FOLDER-ID := #PHYSCL-FLDR-ID-NBR
                pnd_Report_Vars_Pnd_Rpt_Tiaa_Rcvd_Dte.setValue(cwf_Master_Index_View_Tiaa_Rcvd_Dte);                                                                      //Natural: ASSIGN #RPT-TIAA-RCVD-DTE := TIAA-RCVD-DTE
                pnd_Report_Vars_Pnd_Rpt_Wpid_Desc.setValue(pnd_Parms_Cwfn0013_Pnd_Work_Prcss_Short_Nme);                                                                  //Natural: ASSIGN #RPT-WPID-DESC := #WORK-PRCSS-SHORT-NME
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(cwf_Master_Index_View_Status_Freeze_Ind.equals(" ") || cwf_Master_Index_View_Rqst_Log_Dte_Tme.notEquals(pnd_Actv_Unque_Key_Pnd_Rqst_Log_Dte_Tme))) //Natural: IF CWF-MASTER-INDEX-VIEW.STATUS-FREEZE-IND = ' ' OR CWF-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME NE #RQST-LOG-DTE-TME
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            getSort().writeSortInData(pnd_Report_Vars_Pnd_Rpt_Unit_Code, pnd_Report_Vars_Pnd_Rpt_Tiaa_Rcvd_Dte, pnd_Report_Vars_Pnd_Rpt_Entry_Oprtr_Name,                 //Natural: END-ALL
                pnd_Report_Vars_Pnd_Rpt_Unit_Desc, pnd_Report_Vars_Pnd_Rpt_Physical_Folder_Id, pnd_Report_Vars_Pnd_Rpt_Pin_No, pnd_Report_Vars_Pnd_Rpt_Doc_Desc, 
                pnd_Report_Vars_Pnd_Rpt_Wpid_Desc, pnd_Report_Vars_Pnd_Rpt_Corp_Status);
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getSort().sortData(pnd_Report_Vars_Pnd_Rpt_Unit_Code, pnd_Report_Vars_Pnd_Rpt_Tiaa_Rcvd_Dte, pnd_Report_Vars_Pnd_Rpt_Entry_Oprtr_Name);                           //Natural: SORT BY #RPT-UNIT-CODE #RPT-TIAA-RCVD-DTE #RPT-ENTRY-OPRTR-NAME USING #RPT-UNIT-DESC #RPT-PHYSICAL-FOLDER-ID #RPT-PIN-NO #RPT-DOC-DESC #RPT-WPID-DESC #RPT-CORP-STATUS
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(pnd_Report_Vars_Pnd_Rpt_Unit_Code, pnd_Report_Vars_Pnd_Rpt_Tiaa_Rcvd_Dte, pnd_Report_Vars_Pnd_Rpt_Entry_Oprtr_Name, 
            pnd_Report_Vars_Pnd_Rpt_Unit_Desc, pnd_Report_Vars_Pnd_Rpt_Physical_Folder_Id, pnd_Report_Vars_Pnd_Rpt_Pin_No, pnd_Report_Vars_Pnd_Rpt_Doc_Desc, 
            pnd_Report_Vars_Pnd_Rpt_Wpid_Desc, pnd_Report_Vars_Pnd_Rpt_Corp_Status)))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT BREAK OF #RPT-UNIT-CODE
            pnd_Unit_Total.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #UNIT-TOTAL
            pnd_Grand_Total.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #GRAND-TOTAL
            getReports().write(1, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Efsf2501.class));                                                      //Natural: WRITE ( 1 ) NOTITLE NOHDR USING FORM 'EFSF2501'
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            sort01Pnd_Rpt_Unit_CodeOld.setValue(pnd_Report_Vars_Pnd_Rpt_Unit_Code);                                                                                       //Natural: END-SORT
            sort01Pnd_Rpt_Unit_DescOld.setValue(pnd_Report_Vars_Pnd_Rpt_Unit_Desc);
        }
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        endSort();
        pnd_Report_Vars_Pnd_Rpt_Unit_Code.reset();                                                                                                                        //Natural: RESET #RPT-UNIT-CODE #RPT-UNIT-DESC
        pnd_Report_Vars_Pnd_Rpt_Unit_Desc.reset();
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().skip(1, 3);                                                                                                                                          //Natural: SKIP ( 1 ) 3
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,"TOTAL INCOMPLETE WORKSHEETS :",pnd_Grand_Total, new ReportEditMask ("Z,ZZZ,ZZ9"));                 //Natural: WRITE ( 1 ) NOTITLE NOHDR 'TOTAL INCOMPLETE WORKSHEETS :' #GRAND-TOTAL ( EM = Z,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-DOCUMENT-NAME
    }
    private void sub_Get_Document_Name() throws Exception                                                                                                                 //Natural: GET-DOCUMENT-NAME
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        DbsUtil.examine(new ExamineSource(pnd_Doc_Types_Arr.getValue("*")), new ExamineSearch(pnd_Doc_Type), new ExamineGivingIndex(pnd_Index));                          //Natural: EXAMINE #DOC-TYPES-ARR ( * ) FOR #DOC-TYPE GIVING INDEX #INDEX
        if (condition(pnd_Index.greater(getZero())))                                                                                                                      //Natural: IF #INDEX GT 0
        {
            pnd_Report_Vars_Pnd_Rpt_Doc_Desc.setValue(pnd_Doc_Names_Arr.getValue(pnd_Index));                                                                             //Natural: ASSIGN #RPT-DOC-DESC := #DOC-NAMES-ARR ( #INDEX )
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        vw_cwf_Wp_Dcmnt_Type_Tbl_View.startDatabaseRead                                                                                                                   //Natural: READ ( 1 ) CWF-WP-DCMNT-TYPE-TBL-VIEW BY DCMNT-TYPE-CDE-KEY STARTING FROM #DOC-TYPE
        (
        "READ03",
        new Wc[] { new Wc("DCMNT_TYPE_CDE_KEY", ">=", pnd_Doc_Type, WcType.BY) },
        new Oc[] { new Oc("DCMNT_TYPE_CDE_KEY", "ASC") },
        1
        );
        READ03:
        while (condition(vw_cwf_Wp_Dcmnt_Type_Tbl_View.readNextRow("READ03")))
        {
            if (condition(cwf_Wp_Dcmnt_Type_Tbl_View_Dcmnt_Type_Cde.notEquals(pnd_Doc_Type)))                                                                             //Natural: IF DCMNT-TYPE-CDE NE #DOC-TYPE
            {
                pnd_Report_Vars_Pnd_Rpt_Doc_Desc.setValue("Description not defined");                                                                                     //Natural: ASSIGN #RPT-DOC-DESC := 'Description not defined'
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Doc_Types_Ctr.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #DOC-TYPES-CTR
            pnd_Doc_Types_Arr.getValue(pnd_Doc_Types_Ctr).setValue(pnd_Doc_Type);                                                                                         //Natural: ASSIGN #DOC-TYPES-ARR ( #DOC-TYPES-CTR ) := #DOC-TYPE
            pnd_Doc_Names_Arr.getValue(pnd_Doc_Types_Ctr).setValue(cwf_Wp_Dcmnt_Type_Tbl_View_Dcmnt_Nme);                                                                 //Natural: ASSIGN #DOC-NAMES-ARR ( #DOC-TYPES-CTR ) := #RPT-DOC-DESC := DCMNT-NME
            pnd_Report_Vars_Pnd_Rpt_Doc_Desc.setValue(cwf_Wp_Dcmnt_Type_Tbl_View_Dcmnt_Nme);
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Efsf2500.class));                                              //Natural: WRITE ( 1 ) NOTITLE NOHDR USING FORM 'EFSF2500'
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean pnd_Report_Vars_Pnd_Rpt_Unit_CodeIsBreak = pnd_Report_Vars_Pnd_Rpt_Unit_Code.isBreak(endOfData);
        if (condition(pnd_Report_Vars_Pnd_Rpt_Unit_CodeIsBreak))
        {
            pnd_Curr_Rpt_Unit_Code.setValue(pnd_Report_Vars_Pnd_Rpt_Unit_Code);                                                                                           //Natural: ASSIGN #CURR-RPT-UNIT-CODE := #RPT-UNIT-CODE
            pnd_Curr_Rpt_Unit_Desc.setValue(pnd_Report_Vars_Pnd_Rpt_Unit_Desc);                                                                                           //Natural: ASSIGN #CURR-RPT-UNIT-DESC := #RPT-UNIT-DESC
            pnd_Report_Vars_Pnd_Rpt_Unit_Code.setValue(sort01Pnd_Rpt_Unit_CodeOld);                                                                                       //Natural: ASSIGN #RPT-UNIT-CODE := OLD ( #RPT-UNIT-CODE )
            pnd_Report_Vars_Pnd_Rpt_Unit_Desc.setValue(sort01Pnd_Rpt_Unit_DescOld);                                                                                       //Natural: ASSIGN #RPT-UNIT-DESC := OLD ( #RPT-UNIT-DESC )
            getReports().skip(1, 3);                                                                                                                                      //Natural: SKIP ( 1 ) 3
            getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,"Total Incomplete Worksheets for this Unit:",pnd_Unit_Total, new ReportEditMask                 //Natural: WRITE ( 1 ) NOTITLE NOHDR 'Total Incomplete Worksheets for this Unit:' #UNIT-TOTAL ( EM = Z,ZZZ,ZZ9 )
                ("Z,ZZZ,ZZ9"));
            if (condition(Global.isEscape())) return;
            pnd_Unit_Total.reset();                                                                                                                                       //Natural: RESET #UNIT-TOTAL
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            pnd_Report_Vars_Pnd_Rpt_Unit_Code.setValue(pnd_Curr_Rpt_Unit_Code);                                                                                           //Natural: ASSIGN #RPT-UNIT-CODE := #CURR-RPT-UNIT-CODE
            pnd_Report_Vars_Pnd_Rpt_Unit_Desc.setValue(pnd_Curr_Rpt_Unit_Desc);                                                                                           //Natural: ASSIGN #RPT-UNIT-DESC := #CURR-RPT-UNIT-DESC
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=55 LS=132");
    }
}
