/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:32:12 PM
**        * FROM NATURAL PROGRAM : Cwfb4020
************************************************************
**        * FILE NAME            : Cwfb4020.java
**        * CLASS NAME           : Cwfb4020
**        * INSTANCE NAME        : Cwfb4020
************************************************************
************************************************************************
* PROGRAM  : CWFB4020
* SYSTEM   : CRPCWF
* TITLE    : UNIT TABLE FILE EXTRACT
* GENERATED: SEP 15,95 AT 11:25 AM
* FUNCTION : THIS PROGRAM READS THE CWF-ORG-UNIT-TBL AND WRITES A
*            WORK FILE BO BE DOWNLOADED TO THE SQL SERVER.
************************************************************************
*  JVH - 6/23/99 - ADD INTGRTD-PRCSSNG-IND TO EXTRACT OUPUT FILE
*  L.E - 7/20/00 - ADD UNIT-DGTZE-IND, IMAGE-ENBLD-IND, TEAM-IND
*  L.E - 7/20/00 - TO EXTRACT OUTPUT FILE(IES)
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb4020 extends BLNatBase
{
    // Data Areas
    private LdaCdbatxa ldaCdbatxa;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Cur_Lang;
    private DbsField pnd_Program;

    private DataAccessProgramView vw_cwf_Org_Unit_Tbl;
    private DbsField cwf_Org_Unit_Tbl_Unit_Cde;

    private DbsGroup cwf_Org_Unit_Tbl__R_Field_1;
    private DbsField cwf_Org_Unit_Tbl_Unit_Id_Cde;
    private DbsField cwf_Org_Unit_Tbl_Unit_Rgn_Cde;
    private DbsField cwf_Org_Unit_Tbl_Unit_Spcl_Dsgntn_Cde;
    private DbsField cwf_Org_Unit_Tbl_Unit_Brnch_Group_Cde;
    private DbsField cwf_Org_Unit_Tbl_Unit_Short_Nme;
    private DbsField cwf_Org_Unit_Tbl_Unit_Long_Nme;
    private DbsField cwf_Org_Unit_Tbl_Unit_Floor_Nbr;
    private DbsField cwf_Org_Unit_Tbl_Unit_Bldg_Nbr_Brnch_Loc;
    private DbsField cwf_Org_Unit_Tbl_Unit_Drop_Off_Point;
    private DbsField cwf_Org_Unit_Tbl_Unit_Bdgt_Cde;
    private DbsField cwf_Org_Unit_Tbl_Unit_Sprvsr_Racf_Id;
    private DbsField cwf_Org_Unit_Tbl_Unit_Cntct_Nme;

    private DbsGroup cwf_Org_Unit_Tbl__R_Field_2;
    private DbsField cwf_Org_Unit_Tbl_Unit_Cntct_First_Nme;
    private DbsField cwf_Org_Unit_Tbl_Unit_Cntct_Last_Nme;
    private DbsField cwf_Org_Unit_Tbl_Unit_Cntct_Extnsn_Nbr;
    private DbsField cwf_Org_Unit_Tbl_Unit_Prntr_Id;
    private DbsField cwf_Org_Unit_Tbl_Actve_Ind;

    private DbsGroup cwf_Org_Unit_Tbl__R_Field_3;
    private DbsField cwf_Org_Unit_Tbl_Fill_1;
    private DbsField cwf_Org_Unit_Tbl_Actve_Ind_2_2;
    private DbsField cwf_Org_Unit_Tbl_Dlte_Dte_Tme;
    private DbsField cwf_Org_Unit_Tbl_Dlte_Oprtr_Cde;
    private DbsField cwf_Org_Unit_Tbl_Entry_Dte_Tme;
    private DbsField cwf_Org_Unit_Tbl_Entry_Oprtr_Cde;
    private DbsField cwf_Org_Unit_Tbl_Updte_Actn_Cde;
    private DbsField cwf_Org_Unit_Tbl_Updte_Dte;
    private DbsField cwf_Org_Unit_Tbl_Updte_Dte_Tme;
    private DbsField cwf_Org_Unit_Tbl_Updte_Oprtr_Cde;
    private DbsField cwf_Org_Unit_Tbl_Ca_Admin_View_Ind;
    private DbsField cwf_Org_Unit_Tbl_Unit_Dgtze_Ind;
    private DbsField cwf_Org_Unit_Tbl_Image_Enbld_Ind;
    private DbsField cwf_Org_Unit_Tbl_Team_Ind;
    private DbsField cwf_Org_Unit_Tbl_Intgrtd_Prcssng_Ind;
    private DbsField pnd_Header1_1;
    private DbsField pnd_Header1_2;

    private DbsGroup work_Record;
    private DbsField work_Record_Unit_Cde;
    private DbsField work_Record_Pnd_C1;
    private DbsField work_Record_Unit_Short_Nme;
    private DbsField work_Record_Pnd_C2;
    private DbsField work_Record_Unit_Long_Nme;
    private DbsField work_Record_Pnd_C3;
    private DbsField work_Record_Unit_Floor_Nbr;
    private DbsField work_Record_Pnd_C4;
    private DbsField work_Record_Unit_Bldg_Nbr_Brnch_Loc;
    private DbsField work_Record_Pnd_C5;
    private DbsField work_Record_Unit_Drop_Off_Point;
    private DbsField work_Record_Pnd_C6;
    private DbsField work_Record_Unit_Bdgt_Cde;
    private DbsField work_Record_Pnd_C7;
    private DbsField work_Record_Unit_Prntr_Id;
    private DbsField work_Record_Pnd_C8;
    private DbsField work_Record_Unit_Sprvsr_Racf_Id;
    private DbsField work_Record_Pnd_C9;
    private DbsField work_Record_Unit_Cntct_Nme_8;
    private DbsField work_Record_Pnd_C10;
    private DbsField work_Record_Unit_Cntct_Extnsn_Nbr;
    private DbsField work_Record_Pnd_C11;
    private DbsField work_Record_Ca_Admin_View_Ind;
    private DbsField work_Record_Pnd_C12;
    private DbsField work_Record_Intgrtd_Prcssng_Ind;
    private DbsField work_Record_Pnd_C13;
    private DbsField work_Record_Unit_Dgtze_Ind;
    private DbsField work_Record_Pnd_C14;
    private DbsField work_Record_Image_Enbld_Ind;
    private DbsField work_Record_Pnd_C15;
    private DbsField work_Record_Team_Ind;

    private DbsGroup pnd_Counters;
    private DbsField pnd_Counters_Pnd_Rex_Read;
    private DbsField pnd_Counters_Pnd_Rex_Written;

    private DbsGroup pnd_Report_Fields;
    private DbsField pnd_Report_Fields_Pnd_Error_Msg;

    private DbsGroup pnd_Contstants;
    private DbsField pnd_Contstants_Pnd_Delimiter;
    private DbsField pnd_Contact_Name;

    private DbsGroup pnd_Contact_Name__R_Field_4;
    private DbsField pnd_Contact_Name_Pnd_Contact_Name_8;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCdbatxa = new LdaCdbatxa();
        registerRecord(ldaCdbatxa);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Cur_Lang = localVariables.newFieldInRecord("pnd_Cur_Lang", "#CUR-LANG", FieldType.PACKED_DECIMAL, 1);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);

        vw_cwf_Org_Unit_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Org_Unit_Tbl", "CWF-ORG-UNIT-TBL"), "CWF_ORG_UNIT_TBL", "CWF_ASSIGN_RULE");
        cwf_Org_Unit_Tbl_Unit_Cde = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "UNIT_CDE");
        cwf_Org_Unit_Tbl_Unit_Cde.setDdmHeader("UNIT/CODE");

        cwf_Org_Unit_Tbl__R_Field_1 = vw_cwf_Org_Unit_Tbl.getRecord().newGroupInGroup("cwf_Org_Unit_Tbl__R_Field_1", "REDEFINE", cwf_Org_Unit_Tbl_Unit_Cde);
        cwf_Org_Unit_Tbl_Unit_Id_Cde = cwf_Org_Unit_Tbl__R_Field_1.newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Id_Cde", "UNIT-ID-CDE", FieldType.STRING, 5);
        cwf_Org_Unit_Tbl_Unit_Rgn_Cde = cwf_Org_Unit_Tbl__R_Field_1.newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Rgn_Cde", "UNIT-RGN-CDE", FieldType.STRING, 
            1);
        cwf_Org_Unit_Tbl_Unit_Spcl_Dsgntn_Cde = cwf_Org_Unit_Tbl__R_Field_1.newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Spcl_Dsgntn_Cde", "UNIT-SPCL-DSGNTN-CDE", 
            FieldType.STRING, 1);
        cwf_Org_Unit_Tbl_Unit_Brnch_Group_Cde = cwf_Org_Unit_Tbl__R_Field_1.newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Brnch_Group_Cde", "UNIT-BRNCH-GROUP-CDE", 
            FieldType.STRING, 1);
        cwf_Org_Unit_Tbl_Unit_Short_Nme = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Short_Nme", "UNIT-SHORT-NME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "UNIT_SHORT_NME");
        cwf_Org_Unit_Tbl_Unit_Short_Nme.setDdmHeader("UNIT SHORT NAME");
        cwf_Org_Unit_Tbl_Unit_Long_Nme = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Long_Nme", "UNIT-LONG-NME", FieldType.STRING, 
            45, RepeatingFieldStrategy.None, "UNIT_LONG_NME");
        cwf_Org_Unit_Tbl_Unit_Long_Nme.setDdmHeader("UNIT LONG NAME");
        cwf_Org_Unit_Tbl_Unit_Floor_Nbr = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Floor_Nbr", "UNIT-FLOOR-NBR", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "UNIT_FLOOR_NBR");
        cwf_Org_Unit_Tbl_Unit_Floor_Nbr.setDdmHeader("FLOOR/NUMBER");
        cwf_Org_Unit_Tbl_Unit_Bldg_Nbr_Brnch_Loc = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Bldg_Nbr_Brnch_Loc", "UNIT-BLDG-NBR-BRNCH-LOC", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "UNIT_BLDG_NBR_BRNCH_LOC");
        cwf_Org_Unit_Tbl_Unit_Bldg_Nbr_Brnch_Loc.setDdmHeader("BLDG NO //BR. CODE");
        cwf_Org_Unit_Tbl_Unit_Drop_Off_Point = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Drop_Off_Point", "UNIT-DROP-OFF-POINT", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "UNIT_DROP_OFF_POINT");
        cwf_Org_Unit_Tbl_Unit_Bdgt_Cde = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Bdgt_Cde", "UNIT-BDGT-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "UNIT_BDGT_CDE");
        cwf_Org_Unit_Tbl_Unit_Bdgt_Cde.setDdmHeader("BUDGET/CODE");
        cwf_Org_Unit_Tbl_Unit_Sprvsr_Racf_Id = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Sprvsr_Racf_Id", "UNIT-SPRVSR-RACF-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "UNIT_SPRVSR_RACF_ID");
        cwf_Org_Unit_Tbl_Unit_Sprvsr_Racf_Id.setDdmHeader("SUPERVISOR/RACF/ID");
        cwf_Org_Unit_Tbl_Unit_Cntct_Nme = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Cntct_Nme", "UNIT-CNTCT-NME", FieldType.STRING, 
            40, RepeatingFieldStrategy.None, "UNIT_CNTCT_NME");
        cwf_Org_Unit_Tbl_Unit_Cntct_Nme.setDdmHeader("CONTACT NAME");

        cwf_Org_Unit_Tbl__R_Field_2 = vw_cwf_Org_Unit_Tbl.getRecord().newGroupInGroup("cwf_Org_Unit_Tbl__R_Field_2", "REDEFINE", cwf_Org_Unit_Tbl_Unit_Cntct_Nme);
        cwf_Org_Unit_Tbl_Unit_Cntct_First_Nme = cwf_Org_Unit_Tbl__R_Field_2.newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Cntct_First_Nme", "UNIT-CNTCT-FIRST-NME", 
            FieldType.STRING, 20);
        cwf_Org_Unit_Tbl_Unit_Cntct_Last_Nme = cwf_Org_Unit_Tbl__R_Field_2.newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Cntct_Last_Nme", "UNIT-CNTCT-LAST-NME", 
            FieldType.STRING, 20);
        cwf_Org_Unit_Tbl_Unit_Cntct_Extnsn_Nbr = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Cntct_Extnsn_Nbr", "UNIT-CNTCT-EXTNSN-NBR", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "UNIT_CNTCT_EXTNSN_NBR");
        cwf_Org_Unit_Tbl_Unit_Cntct_Extnsn_Nbr.setDdmHeader("CONTACT EXT");
        cwf_Org_Unit_Tbl_Unit_Prntr_Id = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Prntr_Id", "UNIT-PRNTR-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "UNIT_PRNTR_ID");
        cwf_Org_Unit_Tbl_Unit_Prntr_Id.setDdmHeader("PRINTER");
        cwf_Org_Unit_Tbl_Actve_Ind = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTVE_IND");

        cwf_Org_Unit_Tbl__R_Field_3 = vw_cwf_Org_Unit_Tbl.getRecord().newGroupInGroup("cwf_Org_Unit_Tbl__R_Field_3", "REDEFINE", cwf_Org_Unit_Tbl_Actve_Ind);
        cwf_Org_Unit_Tbl_Fill_1 = cwf_Org_Unit_Tbl__R_Field_3.newFieldInGroup("cwf_Org_Unit_Tbl_Fill_1", "FILL-1", FieldType.STRING, 1);
        cwf_Org_Unit_Tbl_Actve_Ind_2_2 = cwf_Org_Unit_Tbl__R_Field_3.newFieldInGroup("cwf_Org_Unit_Tbl_Actve_Ind_2_2", "ACTVE-IND-2-2", FieldType.STRING, 
            1);
        cwf_Org_Unit_Tbl_Dlte_Dte_Tme = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Dlte_Dte_Tme", "DLTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "DLTE_DTE_TME");
        cwf_Org_Unit_Tbl_Dlte_Dte_Tme.setDdmHeader("DELETE/DATE-TIME");
        cwf_Org_Unit_Tbl_Dlte_Oprtr_Cde = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Dlte_Oprtr_Cde", "DLTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "DLTE_OPRTR_CDE");
        cwf_Org_Unit_Tbl_Dlte_Oprtr_Cde.setDdmHeader("DELETE/OPERATOR");
        cwf_Org_Unit_Tbl_Entry_Dte_Tme = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Entry_Dte_Tme", "ENTRY-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ENTRY_DTE_TME");
        cwf_Org_Unit_Tbl_Entry_Dte_Tme.setDdmHeader("ENTRY/DT-TM");
        cwf_Org_Unit_Tbl_Entry_Oprtr_Cde = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Entry_Oprtr_Cde", "ENTRY-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ENTRY_OPRTR_CDE");
        cwf_Org_Unit_Tbl_Entry_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Org_Unit_Tbl_Updte_Actn_Cde = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Updte_Actn_Cde", "UPDTE-ACTN-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "UPDTE_ACTN_CDE");
        cwf_Org_Unit_Tbl_Updte_Actn_Cde.setDdmHeader("UPDT/ACTN");
        cwf_Org_Unit_Tbl_Updte_Dte = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Updte_Dte", "UPDTE-DTE", FieldType.NUMERIC, 8, 
            RepeatingFieldStrategy.None, "UPDTE_DTE");
        cwf_Org_Unit_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        cwf_Org_Unit_Tbl_Updte_Dte_Tme = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Updte_Dte_Tme", "UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "UPDTE_DTE_TME");
        cwf_Org_Unit_Tbl_Updte_Oprtr_Cde = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Updte_Oprtr_Cde", "UPDTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "UPDTE_OPRTR_CDE");
        cwf_Org_Unit_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        cwf_Org_Unit_Tbl_Ca_Admin_View_Ind = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Ca_Admin_View_Ind", "CA-ADMIN-VIEW-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CA_ADMIN_VIEW_IND");
        cwf_Org_Unit_Tbl_Ca_Admin_View_Ind.setDdmHeader("CA-ADMIN-VIEW");
        cwf_Org_Unit_Tbl_Unit_Dgtze_Ind = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Dgtze_Ind", "UNIT-DGTZE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "UNIT_DGTZE_IND");
        cwf_Org_Unit_Tbl_Unit_Dgtze_Ind.setDdmHeader("UNIT-DGTZE/IND");
        cwf_Org_Unit_Tbl_Image_Enbld_Ind = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Image_Enbld_Ind", "IMAGE-ENBLD-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "IMAGE_ENBLD_IND");
        cwf_Org_Unit_Tbl_Image_Enbld_Ind.setDdmHeader("IMAGE/ENABLED");
        cwf_Org_Unit_Tbl_Team_Ind = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Team_Ind", "TEAM-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TEAM_IND");
        cwf_Org_Unit_Tbl_Intgrtd_Prcssng_Ind = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Intgrtd_Prcssng_Ind", "INTGRTD-PRCSSNG-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "INTGRTD_PRCSSNG_IND");
        registerRecord(vw_cwf_Org_Unit_Tbl);

        pnd_Header1_1 = localVariables.newFieldInRecord("pnd_Header1_1", "#HEADER1-1", FieldType.STRING, 50);
        pnd_Header1_2 = localVariables.newFieldInRecord("pnd_Header1_2", "#HEADER1-2", FieldType.STRING, 50);

        work_Record = localVariables.newGroupInRecord("work_Record", "WORK-RECORD");
        work_Record_Unit_Cde = work_Record.newFieldInGroup("work_Record_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8);
        work_Record_Pnd_C1 = work_Record.newFieldInGroup("work_Record_Pnd_C1", "#C1", FieldType.STRING, 1);
        work_Record_Unit_Short_Nme = work_Record.newFieldInGroup("work_Record_Unit_Short_Nme", "UNIT-SHORT-NME", FieldType.STRING, 15);
        work_Record_Pnd_C2 = work_Record.newFieldInGroup("work_Record_Pnd_C2", "#C2", FieldType.STRING, 1);
        work_Record_Unit_Long_Nme = work_Record.newFieldInGroup("work_Record_Unit_Long_Nme", "UNIT-LONG-NME", FieldType.STRING, 45);
        work_Record_Pnd_C3 = work_Record.newFieldInGroup("work_Record_Pnd_C3", "#C3", FieldType.STRING, 1);
        work_Record_Unit_Floor_Nbr = work_Record.newFieldInGroup("work_Record_Unit_Floor_Nbr", "UNIT-FLOOR-NBR", FieldType.NUMERIC, 2);
        work_Record_Pnd_C4 = work_Record.newFieldInGroup("work_Record_Pnd_C4", "#C4", FieldType.STRING, 1);
        work_Record_Unit_Bldg_Nbr_Brnch_Loc = work_Record.newFieldInGroup("work_Record_Unit_Bldg_Nbr_Brnch_Loc", "UNIT-BLDG-NBR-BRNCH-LOC", FieldType.STRING, 
            3);
        work_Record_Pnd_C5 = work_Record.newFieldInGroup("work_Record_Pnd_C5", "#C5", FieldType.STRING, 1);
        work_Record_Unit_Drop_Off_Point = work_Record.newFieldInGroup("work_Record_Unit_Drop_Off_Point", "UNIT-DROP-OFF-POINT", FieldType.STRING, 2);
        work_Record_Pnd_C6 = work_Record.newFieldInGroup("work_Record_Pnd_C6", "#C6", FieldType.STRING, 1);
        work_Record_Unit_Bdgt_Cde = work_Record.newFieldInGroup("work_Record_Unit_Bdgt_Cde", "UNIT-BDGT-CDE", FieldType.STRING, 5);
        work_Record_Pnd_C7 = work_Record.newFieldInGroup("work_Record_Pnd_C7", "#C7", FieldType.STRING, 1);
        work_Record_Unit_Prntr_Id = work_Record.newFieldInGroup("work_Record_Unit_Prntr_Id", "UNIT-PRNTR-ID", FieldType.STRING, 8);
        work_Record_Pnd_C8 = work_Record.newFieldInGroup("work_Record_Pnd_C8", "#C8", FieldType.STRING, 1);
        work_Record_Unit_Sprvsr_Racf_Id = work_Record.newFieldInGroup("work_Record_Unit_Sprvsr_Racf_Id", "UNIT-SPRVSR-RACF-ID", FieldType.STRING, 8);
        work_Record_Pnd_C9 = work_Record.newFieldInGroup("work_Record_Pnd_C9", "#C9", FieldType.STRING, 1);
        work_Record_Unit_Cntct_Nme_8 = work_Record.newFieldInGroup("work_Record_Unit_Cntct_Nme_8", "UNIT-CNTCT-NME-8", FieldType.STRING, 8);
        work_Record_Pnd_C10 = work_Record.newFieldInGroup("work_Record_Pnd_C10", "#C10", FieldType.STRING, 1);
        work_Record_Unit_Cntct_Extnsn_Nbr = work_Record.newFieldInGroup("work_Record_Unit_Cntct_Extnsn_Nbr", "UNIT-CNTCT-EXTNSN-NBR", FieldType.NUMERIC, 
            4);
        work_Record_Pnd_C11 = work_Record.newFieldInGroup("work_Record_Pnd_C11", "#C11", FieldType.STRING, 1);
        work_Record_Ca_Admin_View_Ind = work_Record.newFieldInGroup("work_Record_Ca_Admin_View_Ind", "CA-ADMIN-VIEW-IND", FieldType.STRING, 1);
        work_Record_Pnd_C12 = work_Record.newFieldInGroup("work_Record_Pnd_C12", "#C12", FieldType.STRING, 1);
        work_Record_Intgrtd_Prcssng_Ind = work_Record.newFieldInGroup("work_Record_Intgrtd_Prcssng_Ind", "INTGRTD-PRCSSNG-IND", FieldType.STRING, 1);
        work_Record_Pnd_C13 = work_Record.newFieldInGroup("work_Record_Pnd_C13", "#C13", FieldType.STRING, 1);
        work_Record_Unit_Dgtze_Ind = work_Record.newFieldInGroup("work_Record_Unit_Dgtze_Ind", "UNIT-DGTZE-IND", FieldType.STRING, 1);
        work_Record_Pnd_C14 = work_Record.newFieldInGroup("work_Record_Pnd_C14", "#C14", FieldType.STRING, 1);
        work_Record_Image_Enbld_Ind = work_Record.newFieldInGroup("work_Record_Image_Enbld_Ind", "IMAGE-ENBLD-IND", FieldType.STRING, 1);
        work_Record_Pnd_C15 = work_Record.newFieldInGroup("work_Record_Pnd_C15", "#C15", FieldType.STRING, 1);
        work_Record_Team_Ind = work_Record.newFieldInGroup("work_Record_Team_Ind", "TEAM-IND", FieldType.STRING, 1);

        pnd_Counters = localVariables.newGroupInRecord("pnd_Counters", "#COUNTERS");
        pnd_Counters_Pnd_Rex_Read = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Rex_Read", "#REX-READ", FieldType.PACKED_DECIMAL, 5);
        pnd_Counters_Pnd_Rex_Written = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Rex_Written", "#REX-WRITTEN", FieldType.PACKED_DECIMAL, 5);

        pnd_Report_Fields = localVariables.newGroupInRecord("pnd_Report_Fields", "#REPORT-FIELDS");
        pnd_Report_Fields_Pnd_Error_Msg = pnd_Report_Fields.newFieldInGroup("pnd_Report_Fields_Pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 60);

        pnd_Contstants = localVariables.newGroupInRecord("pnd_Contstants", "#CONTSTANTS");
        pnd_Contstants_Pnd_Delimiter = pnd_Contstants.newFieldInGroup("pnd_Contstants_Pnd_Delimiter", "#DELIMITER", FieldType.STRING, 1);
        pnd_Contact_Name = localVariables.newFieldInRecord("pnd_Contact_Name", "#CONTACT-NAME", FieldType.STRING, 40);

        pnd_Contact_Name__R_Field_4 = localVariables.newGroupInRecord("pnd_Contact_Name__R_Field_4", "REDEFINE", pnd_Contact_Name);
        pnd_Contact_Name_Pnd_Contact_Name_8 = pnd_Contact_Name__R_Field_4.newFieldInGroup("pnd_Contact_Name_Pnd_Contact_Name_8", "#CONTACT-NAME-8", FieldType.STRING, 
            8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Org_Unit_Tbl.reset();

        ldaCdbatxa.initializeValues();

        localVariables.reset();
        pnd_Header1_1.setInitialValue("          CWF Unit Table Download Report");
        pnd_Header1_2.setInitialValue("                        �");
        pnd_Contstants_Pnd_Delimiter.setInitialValue("@");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb4020() throws Exception
    {
        super("Cwfb4020");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  DEFINE PRINTERS AND FORMATS
        getReports().definePrinter(2, "NOT DEFINED");                                                                                                                     //Natural: DEFINE PRINTER ( 1 )
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: ASSIGN #PROGRAM = *PROGRAM
        //*  MAP THE CURRENT LANGUAGE CODE TO THE CODE IN THE BATCH MODEL TEXT LDA.
        pnd_Cur_Lang.setValue(Global.getLANGUAGE());                                                                                                                      //Natural: ASSIGN #CUR-LANG = *LANGUAGE
        pnd_Cur_Lang.setValue(ldaCdbatxa.getCdbatxa_Pnd_Lang_Map().getValue(pnd_Cur_Lang));                                                                               //Natural: ASSIGN #CUR-LANG = CDBATXA.#LANG-MAP ( #CUR-LANG )
        //* *SAG DEFINE EXIT START-OF-PROGRAM
        //* *SAG END-EXIT
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH' THEN
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA = 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        //* *SAG DEFINE EXIT BEFORE-READ
        //* *SAG END-EXIT
        //*  PRIMARY FILE
        vw_cwf_Org_Unit_Tbl.startDatabaseRead                                                                                                                             //Natural: READ CWF-ORG-UNIT-TBL BY UNIT-BR-GROUP-KEY
        (
        "READ_PRIME",
        new Oc[] { new Oc("UNIT_BR_GROUP_KEY", "ASC") }
        );
        READ_PRIME:
        while (condition(vw_cwf_Org_Unit_Tbl.readNextRow("READ_PRIME")))
        {
            //* *SAG DEFINE EXIT PRIME-WRITE-FIELDS
            if (condition(cwf_Org_Unit_Tbl_Dlte_Oprtr_Cde.greater(" ")))                                                                                                  //Natural: REJECT IF DLTE-OPRTR-CDE GT ' '
            {
                continue;
            }
            pnd_Counters_Pnd_Rex_Read.nadd(1);                                                                                                                            //Natural: ADD 1 TO #REX-READ
                                                                                                                                                                          //Natural: PERFORM FORMAT-RECORD
            sub_Format_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-FILE
            sub_Write_Work_File();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* *SAG END-EXIT
            //*  PRIMARY FILE.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* *SAG DEFINE EXIT END-OF-PROGRAM
        getReports().write(1, ReportOption.NOTITLE,"Total Number of Unit Table Records Read  :",pnd_Counters_Pnd_Rex_Read,NEWLINE,"Total Number of Work File Records Written:",pnd_Counters_Pnd_Rex_Written,NEWLINE,NEWLINE,new  //Natural: WRITE ( 1 ) 'Total Number of Unit Table Records Read  :' #REX-READ / 'Total Number of Work File Records Written:' #REX-WRITTEN //56T 'End of Report'
            TabSetting(56),"End of Report");
        if (Global.isEscape()) return;
        //* *SAG END-EXIT
        //* *SAG DEFINE EXIT MISCELLANEOUS-SUBROUTINES
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-RECORD
        //* *************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WORK-FILE
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT
        //* *****************************
        //* *SAG END-EXIT
    }
    private void sub_Format_Record() throws Exception                                                                                                                     //Natural: FORMAT-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        work_Record.reset();                                                                                                                                              //Natural: RESET WORK-RECORD
        work_Record.setValuesByName(vw_cwf_Org_Unit_Tbl);                                                                                                                 //Natural: MOVE BY NAME CWF-ORG-UNIT-TBL TO WORK-RECORD
        //* ONLY WANT FIRST EIGHT(8) BYTES OF CONTACT NAME FOR NOW
        pnd_Contact_Name.setValue(cwf_Org_Unit_Tbl_Unit_Cntct_Nme);                                                                                                       //Natural: MOVE CWF-ORG-UNIT-TBL.UNIT-CNTCT-NME TO #CONTACT-NAME
        work_Record_Unit_Cntct_Nme_8.setValue(pnd_Contact_Name_Pnd_Contact_Name_8);                                                                                       //Natural: MOVE #CONTACT-NAME-8 TO WORK-RECORD.UNIT-CNTCT-NME-8
        work_Record_Pnd_C1.setValue(pnd_Contstants_Pnd_Delimiter);                                                                                                        //Natural: MOVE #DELIMITER TO #C1 #C2 #C3 #C4 #C5 #C6 #C7 #C8 #C9 #C10 #C11 #C12 #C13 #C14 #C15
        work_Record_Pnd_C2.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C3.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C4.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C5.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C6.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C7.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C8.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C9.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C10.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C11.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C12.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C13.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C14.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C15.setValue(pnd_Contstants_Pnd_Delimiter);
        //* *************
        //*  FORMAT-RECORD
    }
    private void sub_Write_Work_File() throws Exception                                                                                                                   //Natural: WRITE-WORK-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getWorkFiles().write(1, false, work_Record);                                                                                                                      //Natural: WRITE WORK FILE 1 WORK-RECORD
        pnd_Counters_Pnd_Rex_Written.nadd(1);                                                                                                                             //Natural: ADD 1 TO #REX-WRITTEN
        //*  PERFORM WRITE-REPORT
        //* ********************************
        //*  WRITE-WORK-FILE
    }
    private void sub_Write_Report() throws Exception                                                                                                                      //Natural: WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        getReports().display(1, "Unit/Code",                                                                                                                              //Natural: DISPLAY ( 1 ) 'Unit/Code' CWF-ORG-UNIT-TBL.UNIT-CDE 'Short/Name' CWF-ORG-UNIT-TBL.UNIT-SHORT-NME 'Long/Name' CWF-ORG-UNIT-TBL.UNIT-LONG-NME ( AL = 21 ) 'Floor/Nbr' CWF-ORG-UNIT-TBL.UNIT-FLOOR-NBR '/Location' CWF-ORG-UNIT-TBL.UNIT-BLDG-NBR-BRNCH-LOC 'Mail/Stop' CWF-ORG-UNIT-TBL.UNIT-DROP-OFF-POINT 'Budget/Code' CWF-ORG-UNIT-TBL.UNIT-BDGT-CDE 'Racf/Id' CWF-ORG-UNIT-TBL.UNIT-SPRVSR-RACF-ID 'Contact/Name' CWF-ORG-UNIT-TBL.UNIT-CNTCT-NME ( AL = 8 ) 'Cntct/Extsn' CWF-ORG-UNIT-TBL.UNIT-CNTCT-EXTNSN-NBR ( AL = 4 ) 'Printer/Id' CWF-ORG-UNIT-TBL.UNIT-PRNTR-ID 'Act/Ind' CWF-ORG-UNIT-TBL.ACTVE-IND 'CA/Inq' CWF-ORG-UNIT-TBL.CA-ADMIN-VIEW-IND
        		cwf_Org_Unit_Tbl_Unit_Cde,"Short/Name",
        		cwf_Org_Unit_Tbl_Unit_Short_Nme,"Long/Name",
        		cwf_Org_Unit_Tbl_Unit_Long_Nme, new AlphanumericLength (21),"Floor/Nbr",
        		cwf_Org_Unit_Tbl_Unit_Floor_Nbr,"/Location",
        		cwf_Org_Unit_Tbl_Unit_Bldg_Nbr_Brnch_Loc,"Mail/Stop",
        		cwf_Org_Unit_Tbl_Unit_Drop_Off_Point,"Budget/Code",
        		cwf_Org_Unit_Tbl_Unit_Bdgt_Cde,"Racf/Id",
        		cwf_Org_Unit_Tbl_Unit_Sprvsr_Racf_Id,"Contact/Name",
        		cwf_Org_Unit_Tbl_Unit_Cntct_Nme, new AlphanumericLength (8),"Cntct/Extsn",
        		cwf_Org_Unit_Tbl_Unit_Cntct_Extnsn_Nbr, new AlphanumericLength (4),"Printer/Id",
        		cwf_Org_Unit_Tbl_Unit_Prntr_Id,"Act/Ind",
        		cwf_Org_Unit_Tbl_Actve_Ind,"CA/Inq",
        		cwf_Org_Unit_Tbl_Ca_Admin_View_Ind);
        if (Global.isEscape()) return;
        //* *****************************
        //* WRITE-REPORT
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(41),pnd_Header1_1,new TabSetting(124),ldaCdbatxa.getCdbatxa_Pnd_Page_Txt().getValue(pnd_Cur_Lang),getReports().getPageNumberDbs(1),  //Natural: WRITE ( 1 ) NOTITLE *PROGRAM 41T #HEADER1-1 124T CDBATXA.#PAGE-TXT ( #CUR-LANG ) *PAGE-NUMBER ( 1 ) ( NL = 4 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) #HEADER1-2 124T *TIMX ( EM = HH':'II' 'AP )
                        new NumericLength (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),pnd_Header1_2,new 
                        TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"));
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");

        getReports().setDisplayColumns(1, "Unit/Code",
        		cwf_Org_Unit_Tbl_Unit_Cde,"Short/Name",
        		cwf_Org_Unit_Tbl_Unit_Short_Nme,"Long/Name",
        		cwf_Org_Unit_Tbl_Unit_Long_Nme, new AlphanumericLength (21),"Floor/Nbr",
        		cwf_Org_Unit_Tbl_Unit_Floor_Nbr,"/Location",
        		cwf_Org_Unit_Tbl_Unit_Bldg_Nbr_Brnch_Loc,"Mail/Stop",
        		cwf_Org_Unit_Tbl_Unit_Drop_Off_Point,"Budget/Code",
        		cwf_Org_Unit_Tbl_Unit_Bdgt_Cde,"Racf/Id",
        		cwf_Org_Unit_Tbl_Unit_Sprvsr_Racf_Id,"Contact/Name",
        		cwf_Org_Unit_Tbl_Unit_Cntct_Nme, new AlphanumericLength (8),"Cntct/Extsn",
        		cwf_Org_Unit_Tbl_Unit_Cntct_Extnsn_Nbr, new AlphanumericLength (4),"Printer/Id",
        		cwf_Org_Unit_Tbl_Unit_Prntr_Id,"Act/Ind",
        		cwf_Org_Unit_Tbl_Actve_Ind,"CA/Inq",
        		cwf_Org_Unit_Tbl_Ca_Admin_View_Ind);
    }
}
