/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:28:16 PM
**        * FROM NATURAL PROGRAM : Cwfb3201
************************************************************
**        * FILE NAME            : Cwfb3201.java
**        * CLASS NAME           : Cwfb3201
**        * INSTANCE NAME        : Cwfb3201
************************************************************
**SAG GENERATOR: SHELL-TIAA                       VERSION: 3.2.2
**SAG TITLE: REPORT 5
**SAG SYSTEM: CRPCWF
************************************************************************
* PROGRAM  : CWFB3201
* SYSTEM   : CRPCWF
* TITLE    : REPORT 5 EXTRACT -WEEKLY VERSION
* GENERATED: AUG 11,93 AT 10:46 AM
* FUNCTION : REPORT OF WORK ACTIVITY FOR A PERIOD
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* MMM DD YY  ________ ______________________________________________
* ______________________________________________
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3201 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Work_Record;
    private DbsField pnd_Work_Record_Tbl_Pin;
    private DbsField pnd_Work_Record_Tbl_Wpid_Act;
    private DbsField pnd_Work_Record_Tbl_Wpid;

    private DbsGroup pnd_Work_Record__R_Field_1;
    private DbsField pnd_Work_Record_Tbl_Wpid_Action;
    private DbsField pnd_Work_Record_Tbl_Status_Cde;

    private DbsGroup pnd_Work_Record__R_Field_2;
    private DbsField pnd_Work_Record_Tbl_Status_Cde_N;
    private DbsField pnd_Work_Record_Tbl_Last_Updte_Dte;

    private DbsGroup pnd_Work_Record__R_Field_3;
    private DbsField pnd_Work_Record_Tbl_Last_Updte_Dte_A;
    private DbsField pnd_Work_Record_Tbl_Last_Chge_Dte;
    private DbsField pnd_Work_Record_Tbl_Log_Dte_Tme;
    private DbsField pnd_Work_Record_Tbl_Log_Unit_Cde;
    private DbsField pnd_Work_Record_Tbl_Chnge_Unit;
    private DbsField pnd_Work_Record_Tbl_Admin_Unit;
    private DbsField pnd_Work_Record_Tbl_Log_By;
    private DbsField pnd_Work_Record_Pnd_New_Ind;
    private DbsField pnd_Work_Record_Pnd_Merg_Ind;
    private DbsField pnd_Work_Record_Pnd_E_Pended_Ind;
    private DbsField pnd_Work_Record_Pnd_Partic_Closed_Ind;
    private DbsField pnd_Work_Record_Pnd_Assigned_Ind;
    private DbsField pnd_Work_Record_Pnd_Unassigned_Ind;

    private DbsGroup pnd_Report_Data;
    private DbsField pnd_Report_Data_Pnd_Rep_Unit_Cde;

    private DbsGroup pnd_Report_Data__R_Field_4;
    private DbsField pnd_Report_Data_Pnd_Rep_Unit_Id_Cde;

    private DbsGroup pnd_Misc_Data;
    private DbsField pnd_Misc_Data_Pnd_Start_Date;

    private DbsGroup pnd_Misc_Data__R_Field_5;
    private DbsField pnd_Misc_Data_Pnd_Start_Date_N;
    private DbsField pnd_Misc_Data_Pnd_End_Date;

    private DbsGroup pnd_Misc_Data__R_Field_6;
    private DbsField pnd_Misc_Data_Pnd_End_Date_N;
    private DbsField pnd_Misc_Data_Pnd_Work_Start_Date_A;

    private DbsGroup pnd_Misc_Data__R_Field_7;
    private DbsField pnd_Misc_Data_Pnd_Work_Mm;
    private DbsField pnd_Misc_Data_Pnd_Filler1;
    private DbsField pnd_Misc_Data_Pnd_Work_Dd;
    private DbsField pnd_Misc_Data_Pnd_Filler2;
    private DbsField pnd_Misc_Data_Pnd_Work_Yy;
    private DbsField pnd_Misc_Data_Pnd_Work_End_Date_A;

    private DbsGroup pnd_Misc_Data__R_Field_8;
    private DbsField pnd_Misc_Data_Pnd_Work_Mmm;
    private DbsField pnd_Misc_Data_Pnd_Fillera;
    private DbsField pnd_Misc_Data_Pnd_Work_Ddd;
    private DbsField pnd_Misc_Data_Pnd_Fillerb;
    private DbsField pnd_Misc_Data_Pnd_Work_Yyy;

    private DataAccessProgramView vw_cwf_Master_Index;
    private DbsField cwf_Master_Index_Pin_Nbr;
    private DbsField cwf_Master_Index_Rqst_Log_Dte_Tme;
    private DbsField cwf_Master_Index_Rqst_Log_Oprtr_Cde;
    private DbsField cwf_Master_Index_Rqst_Orgn_Cde;
    private DbsField cwf_Master_Index_Work_Prcss_Id;
    private DbsField cwf_Master_Index_Empl_Oprtr_Cde;

    private DbsGroup cwf_Master_Index__R_Field_9;
    private DbsField cwf_Master_Index_Empl_Racf_Id;
    private DbsField cwf_Master_Index_Last_Chnge_Dte_Tme;

    private DbsGroup cwf_Master_Index__R_Field_10;
    private DbsField cwf_Master_Index_Last_Chnge_Dte_A;

    private DbsGroup cwf_Master_Index__R_Field_11;
    private DbsField cwf_Master_Index_Last_Chnge_Dte_N;
    private DbsField cwf_Master_Index_Last_Chnge_Tme_A;
    private DbsField cwf_Master_Index_Last_Chnge_Oprtr_Cde;
    private DbsField cwf_Master_Index_Last_Chnge_Unit_Cde;

    private DbsGroup cwf_Master_Index__R_Field_12;
    private DbsField cwf_Master_Index_Last_Chnge_Unit_Id_Cde;
    private DbsField cwf_Master_Index_Admin_Unit_Cde;

    private DbsGroup cwf_Master_Index__R_Field_13;
    private DbsField cwf_Master_Index_Admin_Unit_Id_Cde;
    private DbsField cwf_Master_Index_Admin_Status_Cde;
    private DbsField cwf_Master_Index_Last_Updte_Dte;
    private DbsField cwf_Master_Index_Last_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_Actve_Ind;

    private DataAccessProgramView vw_hist_Mstr_Index;
    private DbsField hist_Mstr_Index_Rqst_Log_Dte_Tme;
    private DbsField hist_Mstr_Index_Admin_Unit_Cde;

    private DbsGroup hist_Mstr_Index__R_Field_14;
    private DbsField hist_Mstr_Index_Admin_Unit_Id_Cde;
    private DbsField hist_Mstr_Index_Admin_Status_Cde;
    private DbsField hist_Mstr_Index_Last_Updte_Dte;
    private DbsField hist_Mstr_Index_Last_Updte_Dte_Tme;
    private DbsField hist_Mstr_Index_Actve_Ind;
    private DbsField pnd_Totrec;
    private DbsField pnd_Conv_Ctr;
    private DbsField pnd_Nconv_Ctr;
    private DbsField pnd_Tbl_Run_Flag;
    private DbsField pnd_Save_Unit;
    private DbsField pnd_First_Unit;
    private DbsField pnd_New_Unit;
    private DbsField pnd_Env;
    private DbsField pnd_Comp_Date;
    private DbsField pnd_Work_Date;
    private DbsField pnd_Parm_Report_No;
    private DbsField pnd_Parm_Empl_Racf_Id;
    private DbsField pnd_Parm_Unit_Code;
    private DbsField pnd_Parm_Unit_Cde;
    private DbsField pnd_Parm_Floor;
    private DbsField pnd_Parm_Bldg;
    private DbsField pnd_Parm_Drop_Off;
    private DbsField pnd_Rep_Unit_Nme;
    private DbsField pnd_Parm_Type;
    private DbsField pnd_Report_Parm;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Work_Record = localVariables.newGroupInRecord("pnd_Work_Record", "#WORK-RECORD");
        pnd_Work_Record_Tbl_Pin = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Pin", "TBL-PIN", FieldType.NUMERIC, 12);
        pnd_Work_Record_Tbl_Wpid_Act = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Wpid_Act", "TBL-WPID-ACT", FieldType.STRING, 1);
        pnd_Work_Record_Tbl_Wpid = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Wpid", "TBL-WPID", FieldType.STRING, 6);

        pnd_Work_Record__R_Field_1 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record__R_Field_1", "REDEFINE", pnd_Work_Record_Tbl_Wpid);
        pnd_Work_Record_Tbl_Wpid_Action = pnd_Work_Record__R_Field_1.newFieldInGroup("pnd_Work_Record_Tbl_Wpid_Action", "TBL-WPID-ACTION", FieldType.STRING, 
            1);
        pnd_Work_Record_Tbl_Status_Cde = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Status_Cde", "TBL-STATUS-CDE", FieldType.STRING, 4);

        pnd_Work_Record__R_Field_2 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record__R_Field_2", "REDEFINE", pnd_Work_Record_Tbl_Status_Cde);
        pnd_Work_Record_Tbl_Status_Cde_N = pnd_Work_Record__R_Field_2.newFieldInGroup("pnd_Work_Record_Tbl_Status_Cde_N", "TBL-STATUS-CDE-N", FieldType.NUMERIC, 
            4);
        pnd_Work_Record_Tbl_Last_Updte_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Last_Updte_Dte", "TBL-LAST-UPDTE-DTE", FieldType.NUMERIC, 
            8);

        pnd_Work_Record__R_Field_3 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record__R_Field_3", "REDEFINE", pnd_Work_Record_Tbl_Last_Updte_Dte);
        pnd_Work_Record_Tbl_Last_Updte_Dte_A = pnd_Work_Record__R_Field_3.newFieldInGroup("pnd_Work_Record_Tbl_Last_Updte_Dte_A", "TBL-LAST-UPDTE-DTE-A", 
            FieldType.STRING, 8);
        pnd_Work_Record_Tbl_Last_Chge_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Last_Chge_Dte", "TBL-LAST-CHGE-DTE", FieldType.STRING, 
            15);
        pnd_Work_Record_Tbl_Log_Dte_Tme = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Log_Dte_Tme", "TBL-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Work_Record_Tbl_Log_Unit_Cde = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Log_Unit_Cde", "TBL-LOG-UNIT-CDE", FieldType.STRING, 8);
        pnd_Work_Record_Tbl_Chnge_Unit = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Chnge_Unit", "TBL-CHNGE-UNIT", FieldType.STRING, 8);
        pnd_Work_Record_Tbl_Admin_Unit = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Admin_Unit", "TBL-ADMIN-UNIT", FieldType.STRING, 8);
        pnd_Work_Record_Tbl_Log_By = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Log_By", "TBL-LOG-BY", FieldType.STRING, 8);
        pnd_Work_Record_Pnd_New_Ind = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_New_Ind", "#NEW-IND", FieldType.NUMERIC, 2);
        pnd_Work_Record_Pnd_Merg_Ind = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Merg_Ind", "#MERG-IND", FieldType.NUMERIC, 2);
        pnd_Work_Record_Pnd_E_Pended_Ind = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_E_Pended_Ind", "#E-PENDED-IND", FieldType.NUMERIC, 2);
        pnd_Work_Record_Pnd_Partic_Closed_Ind = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Partic_Closed_Ind", "#PARTIC-CLOSED-IND", FieldType.NUMERIC, 
            2);
        pnd_Work_Record_Pnd_Assigned_Ind = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Assigned_Ind", "#ASSIGNED-IND", FieldType.NUMERIC, 2);
        pnd_Work_Record_Pnd_Unassigned_Ind = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Unassigned_Ind", "#UNASSIGNED-IND", FieldType.NUMERIC, 
            2);

        pnd_Report_Data = localVariables.newGroupInRecord("pnd_Report_Data", "#REPORT-DATA");
        pnd_Report_Data_Pnd_Rep_Unit_Cde = pnd_Report_Data.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Unit_Cde", "#REP-UNIT-CDE", FieldType.STRING, 8);

        pnd_Report_Data__R_Field_4 = pnd_Report_Data.newGroupInGroup("pnd_Report_Data__R_Field_4", "REDEFINE", pnd_Report_Data_Pnd_Rep_Unit_Cde);
        pnd_Report_Data_Pnd_Rep_Unit_Id_Cde = pnd_Report_Data__R_Field_4.newFieldInGroup("pnd_Report_Data_Pnd_Rep_Unit_Id_Cde", "#REP-UNIT-ID-CDE", FieldType.STRING, 
            5);

        pnd_Misc_Data = localVariables.newGroupInRecord("pnd_Misc_Data", "#MISC-DATA");
        pnd_Misc_Data_Pnd_Start_Date = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Start_Date", "#START-DATE", FieldType.STRING, 8);

        pnd_Misc_Data__R_Field_5 = pnd_Misc_Data.newGroupInGroup("pnd_Misc_Data__R_Field_5", "REDEFINE", pnd_Misc_Data_Pnd_Start_Date);
        pnd_Misc_Data_Pnd_Start_Date_N = pnd_Misc_Data__R_Field_5.newFieldInGroup("pnd_Misc_Data_Pnd_Start_Date_N", "#START-DATE-N", FieldType.NUMERIC, 
            8);
        pnd_Misc_Data_Pnd_End_Date = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_End_Date", "#END-DATE", FieldType.STRING, 8);

        pnd_Misc_Data__R_Field_6 = pnd_Misc_Data.newGroupInGroup("pnd_Misc_Data__R_Field_6", "REDEFINE", pnd_Misc_Data_Pnd_End_Date);
        pnd_Misc_Data_Pnd_End_Date_N = pnd_Misc_Data__R_Field_6.newFieldInGroup("pnd_Misc_Data_Pnd_End_Date_N", "#END-DATE-N", FieldType.NUMERIC, 8);
        pnd_Misc_Data_Pnd_Work_Start_Date_A = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Work_Start_Date_A", "#WORK-START-DATE-A", FieldType.STRING, 
            8);

        pnd_Misc_Data__R_Field_7 = pnd_Misc_Data.newGroupInGroup("pnd_Misc_Data__R_Field_7", "REDEFINE", pnd_Misc_Data_Pnd_Work_Start_Date_A);
        pnd_Misc_Data_Pnd_Work_Mm = pnd_Misc_Data__R_Field_7.newFieldInGroup("pnd_Misc_Data_Pnd_Work_Mm", "#WORK-MM", FieldType.STRING, 2);
        pnd_Misc_Data_Pnd_Filler1 = pnd_Misc_Data__R_Field_7.newFieldInGroup("pnd_Misc_Data_Pnd_Filler1", "#FILLER1", FieldType.STRING, 1);
        pnd_Misc_Data_Pnd_Work_Dd = pnd_Misc_Data__R_Field_7.newFieldInGroup("pnd_Misc_Data_Pnd_Work_Dd", "#WORK-DD", FieldType.STRING, 2);
        pnd_Misc_Data_Pnd_Filler2 = pnd_Misc_Data__R_Field_7.newFieldInGroup("pnd_Misc_Data_Pnd_Filler2", "#FILLER2", FieldType.STRING, 1);
        pnd_Misc_Data_Pnd_Work_Yy = pnd_Misc_Data__R_Field_7.newFieldInGroup("pnd_Misc_Data_Pnd_Work_Yy", "#WORK-YY", FieldType.STRING, 2);
        pnd_Misc_Data_Pnd_Work_End_Date_A = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Work_End_Date_A", "#WORK-END-DATE-A", FieldType.STRING, 8);

        pnd_Misc_Data__R_Field_8 = pnd_Misc_Data.newGroupInGroup("pnd_Misc_Data__R_Field_8", "REDEFINE", pnd_Misc_Data_Pnd_Work_End_Date_A);
        pnd_Misc_Data_Pnd_Work_Mmm = pnd_Misc_Data__R_Field_8.newFieldInGroup("pnd_Misc_Data_Pnd_Work_Mmm", "#WORK-MMM", FieldType.STRING, 2);
        pnd_Misc_Data_Pnd_Fillera = pnd_Misc_Data__R_Field_8.newFieldInGroup("pnd_Misc_Data_Pnd_Fillera", "#FILLERA", FieldType.STRING, 1);
        pnd_Misc_Data_Pnd_Work_Ddd = pnd_Misc_Data__R_Field_8.newFieldInGroup("pnd_Misc_Data_Pnd_Work_Ddd", "#WORK-DDD", FieldType.STRING, 2);
        pnd_Misc_Data_Pnd_Fillerb = pnd_Misc_Data__R_Field_8.newFieldInGroup("pnd_Misc_Data_Pnd_Fillerb", "#FILLERB", FieldType.STRING, 1);
        pnd_Misc_Data_Pnd_Work_Yyy = pnd_Misc_Data__R_Field_8.newFieldInGroup("pnd_Misc_Data_Pnd_Work_Yyy", "#WORK-YYY", FieldType.STRING, 2);

        vw_cwf_Master_Index = new DataAccessProgramView(new NameInfo("vw_cwf_Master_Index", "CWF-MASTER-INDEX"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        cwf_Master_Index_Pin_Nbr = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "PIN_NBR");
        cwf_Master_Index_Pin_Nbr.setDdmHeader("PIN");
        cwf_Master_Index_Rqst_Log_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Master_Index_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        cwf_Master_Index_Rqst_Log_Oprtr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_LOG_OPRTR_CDE");
        cwf_Master_Index_Rqst_Log_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Master_Index_Rqst_Orgn_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Rqst_Orgn_Cde", "RQST-ORGN-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RQST_ORGN_CDE");
        cwf_Master_Index_Work_Prcss_Id = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        cwf_Master_Index_Work_Prcss_Id.setDdmHeader("WORK/ID");
        cwf_Master_Index_Empl_Oprtr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "EMPL_OPRTR_CDE");
        cwf_Master_Index_Empl_Oprtr_Cde.setDdmHeader("EMPLOYEE/ID");

        cwf_Master_Index__R_Field_9 = vw_cwf_Master_Index.getRecord().newGroupInGroup("cwf_Master_Index__R_Field_9", "REDEFINE", cwf_Master_Index_Empl_Oprtr_Cde);
        cwf_Master_Index_Empl_Racf_Id = cwf_Master_Index__R_Field_9.newFieldInGroup("cwf_Master_Index_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 
            8);
        cwf_Master_Index_Last_Chnge_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "LAST_CHNGE_DTE_TME");
        cwf_Master_Index_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");

        cwf_Master_Index__R_Field_10 = vw_cwf_Master_Index.getRecord().newGroupInGroup("cwf_Master_Index__R_Field_10", "REDEFINE", cwf_Master_Index_Last_Chnge_Dte_Tme);
        cwf_Master_Index_Last_Chnge_Dte_A = cwf_Master_Index__R_Field_10.newFieldInGroup("cwf_Master_Index_Last_Chnge_Dte_A", "LAST-CHNGE-DTE-A", FieldType.STRING, 
            8);

        cwf_Master_Index__R_Field_11 = cwf_Master_Index__R_Field_10.newGroupInGroup("cwf_Master_Index__R_Field_11", "REDEFINE", cwf_Master_Index_Last_Chnge_Dte_A);
        cwf_Master_Index_Last_Chnge_Dte_N = cwf_Master_Index__R_Field_11.newFieldInGroup("cwf_Master_Index_Last_Chnge_Dte_N", "LAST-CHNGE-DTE-N", FieldType.NUMERIC, 
            8);
        cwf_Master_Index_Last_Chnge_Tme_A = cwf_Master_Index__R_Field_10.newFieldInGroup("cwf_Master_Index_Last_Chnge_Tme_A", "LAST-CHNGE-TME-A", FieldType.STRING, 
            7);
        cwf_Master_Index_Last_Chnge_Oprtr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Last_Chnge_Oprtr_Cde", "LAST-CHNGE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_CHNGE_OPRTR_CDE");
        cwf_Master_Index_Last_Chnge_Oprtr_Cde.setDdmHeader("LOG CREATE/OPERATOR");
        cwf_Master_Index_Last_Chnge_Unit_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Last_Chnge_Unit_Cde", "LAST-CHNGE-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_CHNGE_UNIT_CDE");
        cwf_Master_Index_Last_Chnge_Unit_Cde.setDdmHeader("LAST/CHANGE/UNIT");

        cwf_Master_Index__R_Field_12 = vw_cwf_Master_Index.getRecord().newGroupInGroup("cwf_Master_Index__R_Field_12", "REDEFINE", cwf_Master_Index_Last_Chnge_Unit_Cde);
        cwf_Master_Index_Last_Chnge_Unit_Id_Cde = cwf_Master_Index__R_Field_12.newFieldInGroup("cwf_Master_Index_Last_Chnge_Unit_Id_Cde", "LAST-CHNGE-UNIT-ID-CDE", 
            FieldType.STRING, 5);
        cwf_Master_Index_Admin_Unit_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ADMIN_UNIT_CDE");

        cwf_Master_Index__R_Field_13 = vw_cwf_Master_Index.getRecord().newGroupInGroup("cwf_Master_Index__R_Field_13", "REDEFINE", cwf_Master_Index_Admin_Unit_Cde);
        cwf_Master_Index_Admin_Unit_Id_Cde = cwf_Master_Index__R_Field_13.newFieldInGroup("cwf_Master_Index_Admin_Unit_Id_Cde", "ADMIN-UNIT-ID-CDE", FieldType.STRING, 
            5);
        cwf_Master_Index_Admin_Status_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "ADMIN_STATUS_CDE");
        cwf_Master_Index_Last_Updte_Dte = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Last_Updte_Dte", "LAST-UPDTE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "LAST_UPDTE_DTE");
        cwf_Master_Index_Last_Updte_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Last_Updte_Dte_Tme", "LAST-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LAST_UPDTE_DTE_TME");
        cwf_Master_Index_Actve_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTVE_IND");
        registerRecord(vw_cwf_Master_Index);

        vw_hist_Mstr_Index = new DataAccessProgramView(new NameInfo("vw_hist_Mstr_Index", "HIST-MSTR-INDEX"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        hist_Mstr_Index_Rqst_Log_Dte_Tme = vw_hist_Mstr_Index.getRecord().newFieldInGroup("hist_Mstr_Index_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        hist_Mstr_Index_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        hist_Mstr_Index_Admin_Unit_Cde = vw_hist_Mstr_Index.getRecord().newFieldInGroup("hist_Mstr_Index_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ADMIN_UNIT_CDE");

        hist_Mstr_Index__R_Field_14 = vw_hist_Mstr_Index.getRecord().newGroupInGroup("hist_Mstr_Index__R_Field_14", "REDEFINE", hist_Mstr_Index_Admin_Unit_Cde);
        hist_Mstr_Index_Admin_Unit_Id_Cde = hist_Mstr_Index__R_Field_14.newFieldInGroup("hist_Mstr_Index_Admin_Unit_Id_Cde", "ADMIN-UNIT-ID-CDE", FieldType.STRING, 
            5);
        hist_Mstr_Index_Admin_Status_Cde = vw_hist_Mstr_Index.getRecord().newFieldInGroup("hist_Mstr_Index_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "ADMIN_STATUS_CDE");
        hist_Mstr_Index_Last_Updte_Dte = vw_hist_Mstr_Index.getRecord().newFieldInGroup("hist_Mstr_Index_Last_Updte_Dte", "LAST-UPDTE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "LAST_UPDTE_DTE");
        hist_Mstr_Index_Last_Updte_Dte_Tme = vw_hist_Mstr_Index.getRecord().newFieldInGroup("hist_Mstr_Index_Last_Updte_Dte_Tme", "LAST-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LAST_UPDTE_DTE_TME");
        hist_Mstr_Index_Actve_Ind = vw_hist_Mstr_Index.getRecord().newFieldInGroup("hist_Mstr_Index_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTVE_IND");
        registerRecord(vw_hist_Mstr_Index);

        pnd_Totrec = localVariables.newFieldInRecord("pnd_Totrec", "#TOTREC", FieldType.PACKED_DECIMAL, 12);
        pnd_Conv_Ctr = localVariables.newFieldInRecord("pnd_Conv_Ctr", "#CONV-CTR", FieldType.PACKED_DECIMAL, 12);
        pnd_Nconv_Ctr = localVariables.newFieldInRecord("pnd_Nconv_Ctr", "#NCONV-CTR", FieldType.PACKED_DECIMAL, 12);
        pnd_Tbl_Run_Flag = localVariables.newFieldInRecord("pnd_Tbl_Run_Flag", "#TBL-RUN-FLAG", FieldType.STRING, 1);
        pnd_Save_Unit = localVariables.newFieldInRecord("pnd_Save_Unit", "#SAVE-UNIT", FieldType.STRING, 8);
        pnd_First_Unit = localVariables.newFieldInRecord("pnd_First_Unit", "#FIRST-UNIT", FieldType.BOOLEAN, 1);
        pnd_New_Unit = localVariables.newFieldInRecord("pnd_New_Unit", "#NEW-UNIT", FieldType.BOOLEAN, 1);
        pnd_Env = localVariables.newFieldInRecord("pnd_Env", "#ENV", FieldType.STRING, 10);
        pnd_Comp_Date = localVariables.newFieldInRecord("pnd_Comp_Date", "#COMP-DATE", FieldType.STRING, 8);
        pnd_Work_Date = localVariables.newFieldInRecord("pnd_Work_Date", "#WORK-DATE", FieldType.DATE);
        pnd_Parm_Report_No = localVariables.newFieldInRecord("pnd_Parm_Report_No", "#PARM-REPORT-NO", FieldType.NUMERIC, 2);
        pnd_Parm_Empl_Racf_Id = localVariables.newFieldInRecord("pnd_Parm_Empl_Racf_Id", "#PARM-EMPL-RACF-ID", FieldType.STRING, 8);
        pnd_Parm_Unit_Code = localVariables.newFieldInRecord("pnd_Parm_Unit_Code", "#PARM-UNIT-CODE", FieldType.STRING, 8);
        pnd_Parm_Unit_Cde = localVariables.newFieldInRecord("pnd_Parm_Unit_Cde", "#PARM-UNIT-CDE", FieldType.STRING, 7);
        pnd_Parm_Floor = localVariables.newFieldInRecord("pnd_Parm_Floor", "#PARM-FLOOR", FieldType.NUMERIC, 2);
        pnd_Parm_Bldg = localVariables.newFieldInRecord("pnd_Parm_Bldg", "#PARM-BLDG", FieldType.STRING, 3);
        pnd_Parm_Drop_Off = localVariables.newFieldInRecord("pnd_Parm_Drop_Off", "#PARM-DROP-OFF", FieldType.STRING, 2);
        pnd_Rep_Unit_Nme = localVariables.newFieldInRecord("pnd_Rep_Unit_Nme", "#REP-UNIT-NME", FieldType.STRING, 45);
        pnd_Parm_Type = localVariables.newFieldInRecord("pnd_Parm_Type", "#PARM-TYPE", FieldType.STRING, 1);
        pnd_Report_Parm = localVariables.newFieldInRecord("pnd_Report_Parm", "#REPORT-PARM", FieldType.STRING, 17);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Master_Index.reset();
        vw_hist_Mstr_Index.reset();

        localVariables.reset();
        pnd_Parm_Report_No.setInitialValue(23);
        pnd_Parm_Unit_Code.setInitialValue("CWF");
        pnd_Parm_Type.setInitialValue("W");
        pnd_Report_Parm.setInitialValue("CWFB3005W*");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb3201() throws Exception
    {
        super("Cwfb3201");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        pnd_Misc_Data_Pnd_Filler1.setValue("/");                                                                                                                          //Natural: MOVE '/' TO #FILLER1 #FILLER2
        pnd_Misc_Data_Pnd_Filler2.setValue("/");
        pnd_Misc_Data_Pnd_Fillera.setValue("/");                                                                                                                          //Natural: MOVE '/' TO #FILLERA #FILLERB
        pnd_Misc_Data_Pnd_Fillerb.setValue("/");
        DbsUtil.callnat(Cwfn3912.class , getCurrentProcessState(), pnd_Comp_Date, pnd_Parm_Type);                                                                         //Natural: CALLNAT 'CWFN3912' #COMP-DATE #PARM-TYPE
        if (condition(Global.isEscape())) return;
        //*  RESETS THE Y/N FLAG BUT DOES NOT BUMP UP THE DATE
        //*  YET
        DbsUtil.callnat(Cwfn3913.class , getCurrentProcessState(), pnd_Report_Parm, pnd_Parm_Empl_Racf_Id, pnd_Parm_Unit_Cde, pnd_Parm_Floor, pnd_Parm_Bldg,              //Natural: CALLNAT 'CWFN3913' #REPORT-PARM #PARM-EMPL-RACF-ID #PARM-UNIT-CDE #PARM-FLOOR #PARM-BLDG #PARM-DROP-OFF #START-DATE #TBL-RUN-FLAG
            pnd_Parm_Drop_Off, pnd_Misc_Data_Pnd_Start_Date, pnd_Tbl_Run_Flag);
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Comp_Date.greater(pnd_Misc_Data_Pnd_Start_Date)))                                                                                               //Natural: IF #COMP-DATE GT #START-DATE
        {
            pnd_Misc_Data_Pnd_End_Date.setValue(pnd_Comp_Date);                                                                                                           //Natural: MOVE #COMP-DATE TO #END-DATE
            pnd_Work_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Misc_Data_Pnd_End_Date);                                                                      //Natural: MOVE EDITED #END-DATE TO #WORK-DATE ( EM = YYYYMMDD )
            pnd_Work_Date.nsubtract(6);                                                                                                                                   //Natural: COMPUTE #WORK-DATE = #WORK-DATE - 6
            pnd_Misc_Data_Pnd_Start_Date.setValueEdited(pnd_Work_Date,new ReportEditMask("YYYYMMDD"));                                                                    //Natural: MOVE EDITED #WORK-DATE ( EM = YYYYMMDD ) TO #START-DATE
            pnd_Work_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Misc_Data_Pnd_Start_Date);                                                                    //Natural: MOVE EDITED #START-DATE TO #WORK-DATE ( EM = YYYYMMDD )
            pnd_Misc_Data_Pnd_Work_Start_Date_A.setValueEdited(pnd_Work_Date,new ReportEditMask("MM'/'DD'/'YY"));                                                         //Natural: MOVE EDITED #WORK-DATE ( EM = MM'/'DD'/'YY ) TO #WORK-START-DATE-A
            pnd_Work_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Misc_Data_Pnd_End_Date);                                                                      //Natural: MOVE EDITED #END-DATE TO #WORK-DATE ( EM = YYYYMMDD )
            pnd_Misc_Data_Pnd_Work_End_Date_A.setValueEdited(pnd_Work_Date,new ReportEditMask("MM'/'DD'/'YY"));                                                           //Natural: MOVE EDITED #WORK-DATE ( EM = MM'/'DD'/'YY ) TO #WORK-END-DATE-A
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            DbsUtil.terminate();  if (true) return;                                                                                                                       //Natural: TERMINATE
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Cwfn3910.class , getCurrentProcessState(), pnd_Parm_Report_No, pnd_Parm_Empl_Racf_Id, pnd_Parm_Unit_Code, pnd_Parm_Floor, pnd_Parm_Bldg,          //Natural: CALLNAT 'CWFN3910' #PARM-REPORT-NO #PARM-EMPL-RACF-ID #PARM-UNIT-CODE #PARM-FLOOR #PARM-BLDG #PARM-DROP-OFF #COMP-DATE
            pnd_Parm_Drop_Off, pnd_Comp_Date);
        if (condition(Global.isEscape())) return;
        vw_cwf_Master_Index.startDatabaseRead                                                                                                                             //Natural: READ CWF-MASTER-INDEX BY LAST-CHNGE-DTE-KEY FROM #START-DATE-N
        (
        "READ_MASTER",
        new Wc[] { new Wc("LAST_CHNGE_DTE_KEY", ">=", pnd_Misc_Data_Pnd_Start_Date_N, WcType.BY) },
        new Oc[] { new Oc("LAST_CHNGE_DTE_KEY", "ASC") }
        );
        READ_MASTER:
        while (condition(vw_cwf_Master_Index.readNextRow("READ_MASTER")))
        {
            if (condition(cwf_Master_Index_Last_Chnge_Dte_N.greater(pnd_Misc_Data_Pnd_End_Date_N)))                                                                       //Natural: IF CWF-MASTER-INDEX.LAST-CHNGE-DTE-N GT #END-DATE-N
            {
                if (true) break READ_MASTER;                                                                                                                              //Natural: ESCAPE BOTTOM ( READ-MASTER. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(cwf_Master_Index_Actve_Ind.greater(" ") || (cwf_Master_Index_Last_Chnge_Unit_Id_Cde.notEquals(cwf_Master_Index_Admin_Unit_Id_Cde)             //Natural: ACCEPT IF CWF-MASTER-INDEX.ACTVE-IND GT ' ' OR ( CWF-MASTER-INDEX.LAST-CHNGE-UNIT-ID-CDE NE CWF-MASTER-INDEX.ADMIN-UNIT-ID-CDE AND CWF-MASTER-INDEX.ACTVE-IND = ' ' )
                && cwf_Master_Index_Actve_Ind.equals(" ")))))
            {
                continue;
            }
            //* ******************************************************
            if (condition(((cwf_Master_Index_Admin_Status_Cde.greaterOrEqual("0000") && cwf_Master_Index_Admin_Status_Cde.lessOrEqual("8999")) || cwf_Master_Index_Admin_Status_Cde.equals("MERG")))) //Natural: IF CWF-MASTER-INDEX.ADMIN-STATUS-CDE = '0000' THRU '8999' OR CWF-MASTER-INDEX.ADMIN-STATUS-CDE = 'MERG'
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-FILE
                sub_Write_Work_File();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_MASTER"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_MASTER"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(cwf_Master_Index_Rqst_Orgn_Cde.equals("J")))                                                                                                //Natural: IF CWF-MASTER-INDEX.RQST-ORGN-CDE = 'J'
                {
                    pnd_Conv_Ctr.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #CONV-CTR
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Nconv_Ctr.nadd(1);                                                                                                                                //Natural: ADD 1 TO #NCONV-CTR
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //* ******************************************************
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Totrec.setValue(vw_cwf_Master_Index.getAstCOUNTER());                                                                                                         //Natural: MOVE *COUNTER ( READ-MASTER. ) TO #TOTREC
        getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),pnd_Env,new TabSetting(26),"CORPORATE WORKFLOW FACILITIES",new TabSetting(72),"PAGE",getReports().getPageNumberDbs(1),  //Natural: WRITE ( 1 ) NOTITLE *PROGRAM #ENV 26T 'CORPORATE WORKFLOW FACILITIES' 72T 'PAGE' *PAGE-NUMBER ( 1 ) ( NL = 3 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 14T 'WEEKLY EXTRACT (REPORT OF WORK ACTIVITY FOR A PERIOD)' 72T *TIMX ( EM = HH':'II' 'AP )
            new NumericLength (3), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),new 
            TabSetting(14),"WEEKLY EXTRACT (REPORT OF WORK ACTIVITY FOR A PERIOD)",new TabSetting(72),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"));
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,"=",new RepeatItem(79));                                                                                               //Natural: WRITE ( 1 ) '=' ( 79 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TOTAL NUMBER OF RECORDS READ                  ",pnd_Totrec,NEWLINE,"TOTAL NUMBER OF CONVERTED RECORDS WRITTEN     ",pnd_Conv_Ctr,NEWLINE,"TOTAL NUMBER OF NON-CONVERTED RECORDS WRITTEN ",pnd_Nconv_Ctr,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"*************************** END OF REPORT ***************************",NEWLINE,"=",new  //Natural: WRITE ( 1 ) 'TOTAL NUMBER OF RECORDS READ                  ' #TOTREC / 'TOTAL NUMBER OF CONVERTED RECORDS WRITTEN     ' #CONV-CTR / 'TOTAL NUMBER OF NON-CONVERTED RECORDS WRITTEN ' #NCONV-CTR //// '*************************** END OF REPORT ***************************' / '=' ( 79 )
            RepeatItem(79));
        if (Global.isEscape()) return;
        getReports().eject(1, true);                                                                                                                                      //Natural: EJECT ( 1 )
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WORK-FILE
        DbsUtil.callnat(Cwfn3911.class , getCurrentProcessState());                                                                                                       //Natural: CALLNAT 'CWFN3911'
        if (condition(Global.isEscape())) return;
    }
    private void sub_Write_Work_File() throws Exception                                                                                                                   //Natural: WRITE-WORK-FILE
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(cwf_Master_Index_Actve_Ind.greater(" ")))                                                                                                           //Natural: IF CWF-MASTER-INDEX.ACTVE-IND GT ' '
        {
            vw_hist_Mstr_Index.startDatabaseRead                                                                                                                          //Natural: READ HIST-MSTR-INDEX BY RQST-ROUTING-KEY FROM CWF-MASTER-INDEX.RQST-LOG-DTE-TME
            (
            "READ01",
            new Wc[] { new Wc("RQST_ROUTING_KEY", ">=", cwf_Master_Index_Rqst_Log_Dte_Tme, WcType.BY) },
            new Oc[] { new Oc("RQST_ROUTING_KEY", "ASC") }
            );
            READ01:
            while (condition(vw_hist_Mstr_Index.readNextRow("READ01")))
            {
                if (condition(hist_Mstr_Index_Rqst_Log_Dte_Tme.greater(cwf_Master_Index_Rqst_Log_Dte_Tme)))                                                               //Natural: IF HIST-MSTR-INDEX.RQST-LOG-DTE-TME GT CWF-MASTER-INDEX.RQST-LOG-DTE-TME
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(!(hist_Mstr_Index_Admin_Unit_Id_Cde.equals(pnd_Report_Data_Pnd_Rep_Unit_Id_Cde) && hist_Mstr_Index_Actve_Ind.equals(" "))))                 //Natural: ACCEPT IF HIST-MSTR-INDEX.ADMIN-UNIT-ID-CDE = #REP-UNIT-ID-CDE AND HIST-MSTR-INDEX.ACTVE-IND = ' '
                {
                    continue;
                }
                short decideConditionsMet188 = 0;                                                                                                                         //Natural: DECIDE ON EVERY VALUE OF HIST-MSTR-INDEX.ADMIN-STATUS-CDE;//Natural: VALUES '0900':'0999'
                if (condition(hist_Mstr_Index_Admin_Status_Cde.greaterOrEqual("0900") && hist_Mstr_Index_Admin_Status_Cde.lessOrEqual("0999")))
                {
                    decideConditionsMet188++;
                    if (condition(hist_Mstr_Index_Last_Updte_Dte.greater(pnd_Misc_Data_Pnd_Start_Date_N)))                                                                //Natural: IF HIST-MSTR-INDEX.LAST-UPDTE-DTE GT #START-DATE-N
                    {
                        pnd_Work_Record_Pnd_New_Ind.nadd(1);                                                                                                              //Natural: ADD 1 TO #WORK-RECORD.#NEW-IND
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: VALUES '1000':'2999'
                if (condition(hist_Mstr_Index_Admin_Status_Cde.greaterOrEqual("1000") && hist_Mstr_Index_Admin_Status_Cde.lessOrEqual("2999")))
                {
                    decideConditionsMet188++;
                    if (condition(hist_Mstr_Index_Admin_Status_Cde.lessOrEqual("1999") && hist_Mstr_Index_Last_Updte_Dte.equals(pnd_Misc_Data_Pnd_End_Date_N)))           //Natural: IF HIST-MSTR-INDEX.ADMIN-STATUS-CDE LE '1999' AND HIST-MSTR-INDEX.LAST-UPDTE-DTE = #END-DATE-N
                    {
                        pnd_Work_Record_Pnd_Unassigned_Ind.nadd(1);                                                                                                       //Natural: ADD 1 TO #WORK-RECORD.#UNASSIGNED-IND
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(hist_Mstr_Index_Last_Updte_Dte.greater(pnd_Misc_Data_Pnd_Start_Date_N)))                                                                //Natural: IF HIST-MSTR-INDEX.LAST-UPDTE-DTE GT #START-DATE-N
                    {
                        pnd_Work_Record_Pnd_New_Ind.nadd(1);                                                                                                              //Natural: ADD 1 TO #WORK-RECORD.#NEW-IND
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: VALUES '2000':'3999'
                if (condition(hist_Mstr_Index_Admin_Status_Cde.greaterOrEqual("2000") && hist_Mstr_Index_Admin_Status_Cde.lessOrEqual("3999")))
                {
                    decideConditionsMet188++;
                    if (condition(hist_Mstr_Index_Last_Updte_Dte.equals(pnd_Misc_Data_Pnd_End_Date_N)))                                                                   //Natural: IF HIST-MSTR-INDEX.LAST-UPDTE-DTE = #END-DATE-N
                    {
                        pnd_Work_Record_Pnd_Assigned_Ind.nadd(1);                                                                                                         //Natural: ADD 1 TO #WORK-RECORD.#ASSIGNED-IND
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: VALUES '4500':'4997'
                if (condition(hist_Mstr_Index_Admin_Status_Cde.greaterOrEqual("4500") && hist_Mstr_Index_Admin_Status_Cde.lessOrEqual("4997")))
                {
                    decideConditionsMet188++;
                    pnd_Work_Record_Pnd_E_Pended_Ind.nadd(1);                                                                                                             //Natural: ADD 1 TO #WORK-RECORD.#E-PENDED-IND
                }                                                                                                                                                         //Natural: VALUES '4998':'4999'
                if (condition(hist_Mstr_Index_Admin_Status_Cde.greaterOrEqual("4998") && hist_Mstr_Index_Admin_Status_Cde.lessOrEqual("4999")))
                {
                    decideConditionsMet188++;
                    if (condition(hist_Mstr_Index_Last_Updte_Dte.equals(pnd_Misc_Data_Pnd_End_Date_N)))                                                                   //Natural: IF HIST-MSTR-INDEX.LAST-UPDTE-DTE = #END-DATE-N
                    {
                        pnd_Work_Record_Pnd_Unassigned_Ind.nadd(1);                                                                                                       //Natural: ADD 1 TO #WORK-RECORD.#UNASSIGNED-IND
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: VALUES '5000':'6999'
                if (condition(hist_Mstr_Index_Admin_Status_Cde.greaterOrEqual("5000") && hist_Mstr_Index_Admin_Status_Cde.lessOrEqual("6999")))
                {
                    decideConditionsMet188++;
                    if (condition(hist_Mstr_Index_Last_Updte_Dte.equals(pnd_Misc_Data_Pnd_End_Date_N)))                                                                   //Natural: IF HIST-MSTR-INDEX.LAST-UPDTE-DTE = #END-DATE-N
                    {
                        pnd_Work_Record_Pnd_Assigned_Ind.nadd(1);                                                                                                         //Natural: ADD 1 TO #WORK-RECORD.#ASSIGNED-IND
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: VALUES '5500':'5999'
                if (condition(hist_Mstr_Index_Admin_Status_Cde.greaterOrEqual("5500") && hist_Mstr_Index_Admin_Status_Cde.lessOrEqual("5999")))
                {
                    decideConditionsMet188++;
                    if (condition(hist_Mstr_Index_Last_Updte_Dte.greater(pnd_Misc_Data_Pnd_Start_Date_N)))                                                                //Natural: IF HIST-MSTR-INDEX.LAST-UPDTE-DTE GT #START-DATE-N
                    {
                        pnd_Work_Record_Pnd_New_Ind.nadd(1);                                                                                                              //Natural: ADD 1 TO #WORK-RECORD.#NEW-IND
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: VALUES '7000':'7499'
                if (condition(hist_Mstr_Index_Admin_Status_Cde.greaterOrEqual("7000") && hist_Mstr_Index_Admin_Status_Cde.lessOrEqual("7499")))
                {
                    decideConditionsMet188++;
                    if (condition(hist_Mstr_Index_Last_Updte_Dte.equals(pnd_Misc_Data_Pnd_End_Date_N)))                                                                   //Natural: IF HIST-MSTR-INDEX.LAST-UPDTE-DTE = #END-DATE-N
                    {
                        pnd_Work_Record_Pnd_Unassigned_Ind.nadd(1);                                                                                                       //Natural: ADD 1 TO #WORK-RECORD.#UNASSIGNED-IND
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: VALUES '8000':'8999'
                if (condition(hist_Mstr_Index_Admin_Status_Cde.greaterOrEqual("8000") && hist_Mstr_Index_Admin_Status_Cde.lessOrEqual("8999")))
                {
                    decideConditionsMet188++;
                    if (condition(hist_Mstr_Index_Last_Updte_Dte.lessOrEqual(pnd_Misc_Data_Pnd_End_Date_N)))                                                              //Natural: IF HIST-MSTR-INDEX.LAST-UPDTE-DTE LE #END-DATE-N
                    {
                        pnd_Work_Record_Pnd_Partic_Closed_Ind.nadd(1);                                                                                                    //Natural: ADD 1 TO #WORK-RECORD.#PARTIC-CLOSED-IND
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: VALUE 'MERG'
                if (condition(hist_Mstr_Index_Admin_Status_Cde.equals("MERG")))
                {
                    decideConditionsMet188++;
                    pnd_Work_Record_Pnd_Merg_Ind.nadd(1);                                                                                                                 //Natural: ADD 1 TO #WORK-RECORD.#MERG-IND
                }                                                                                                                                                         //Natural: NONE
                if (condition(decideConditionsMet188 == 0))
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            short decideConditionsMet233 = 0;                                                                                                                             //Natural: DECIDE ON EVERY VALUE OF CWF-MASTER-INDEX.ADMIN-STATUS-CDE;//Natural: VALUES '0900':'0999'
            if (condition(cwf_Master_Index_Admin_Status_Cde.greaterOrEqual("0900") && cwf_Master_Index_Admin_Status_Cde.lessOrEqual("0999")))
            {
                decideConditionsMet233++;
                if (condition(cwf_Master_Index_Last_Updte_Dte.greater(pnd_Misc_Data_Pnd_Start_Date_N)))                                                                   //Natural: IF CWF-MASTER-INDEX.LAST-UPDTE-DTE GT #START-DATE-N
                {
                    pnd_Work_Record_Pnd_New_Ind.nadd(1);                                                                                                                  //Natural: ADD 1 TO #WORK-RECORD.#NEW-IND
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUES '1000':'2999'
            if (condition(cwf_Master_Index_Admin_Status_Cde.greaterOrEqual("1000") && cwf_Master_Index_Admin_Status_Cde.lessOrEqual("2999")))
            {
                decideConditionsMet233++;
                if (condition(cwf_Master_Index_Admin_Status_Cde.lessOrEqual("1999") && cwf_Master_Index_Last_Updte_Dte.equals(pnd_Misc_Data_Pnd_End_Date_N)))             //Natural: IF CWF-MASTER-INDEX.ADMIN-STATUS-CDE LE '1999' AND CWF-MASTER-INDEX.LAST-UPDTE-DTE = #END-DATE-N
                {
                    pnd_Work_Record_Pnd_Unassigned_Ind.nadd(1);                                                                                                           //Natural: ADD 1 TO #WORK-RECORD.#UNASSIGNED-IND
                }                                                                                                                                                         //Natural: END-IF
                if (condition(cwf_Master_Index_Last_Updte_Dte.greater(pnd_Misc_Data_Pnd_Start_Date_N)))                                                                   //Natural: IF CWF-MASTER-INDEX.LAST-UPDTE-DTE GT #START-DATE-N
                {
                    pnd_Work_Record_Pnd_New_Ind.nadd(1);                                                                                                                  //Natural: ADD 1 TO #WORK-RECORD.#NEW-IND
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUES '2000':'3999'
            if (condition(cwf_Master_Index_Admin_Status_Cde.greaterOrEqual("2000") && cwf_Master_Index_Admin_Status_Cde.lessOrEqual("3999")))
            {
                decideConditionsMet233++;
                if (condition(hist_Mstr_Index_Last_Updte_Dte.equals(pnd_Misc_Data_Pnd_End_Date_N)))                                                                       //Natural: IF HIST-MSTR-INDEX.LAST-UPDTE-DTE = #END-DATE-N
                {
                    pnd_Work_Record_Pnd_Assigned_Ind.nadd(1);                                                                                                             //Natural: ADD 1 TO #WORK-RECORD.#ASSIGNED-IND
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUES '4500':'4997'
            if (condition(cwf_Master_Index_Admin_Status_Cde.greaterOrEqual("4500") && cwf_Master_Index_Admin_Status_Cde.lessOrEqual("4997")))
            {
                decideConditionsMet233++;
                pnd_Work_Record_Pnd_E_Pended_Ind.nadd(1);                                                                                                                 //Natural: ADD 1 TO #WORK-RECORD.#E-PENDED-IND
            }                                                                                                                                                             //Natural: VALUES '4998':'4999'
            if (condition(cwf_Master_Index_Admin_Status_Cde.greaterOrEqual("4998") && cwf_Master_Index_Admin_Status_Cde.lessOrEqual("4999")))
            {
                decideConditionsMet233++;
                if (condition(cwf_Master_Index_Last_Updte_Dte.equals(pnd_Misc_Data_Pnd_End_Date_N)))                                                                      //Natural: IF CWF-MASTER-INDEX.LAST-UPDTE-DTE = #END-DATE-N
                {
                    pnd_Work_Record_Pnd_Unassigned_Ind.nadd(1);                                                                                                           //Natural: ADD 1 TO #WORK-RECORD.#UNASSIGNED-IND
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUES '5500':'5999'
            if (condition(cwf_Master_Index_Admin_Status_Cde.greaterOrEqual("5500") && cwf_Master_Index_Admin_Status_Cde.lessOrEqual("5999")))
            {
                decideConditionsMet233++;
                if (condition(cwf_Master_Index_Last_Updte_Dte.greater(pnd_Misc_Data_Pnd_Start_Date_N)))                                                                   //Natural: IF CWF-MASTER-INDEX.LAST-UPDTE-DTE GT #START-DATE-N
                {
                    pnd_Work_Record_Pnd_New_Ind.nadd(1);                                                                                                                  //Natural: ADD 1 TO #WORK-RECORD.#NEW-IND
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUES '7000':'7499'
            if (condition(cwf_Master_Index_Admin_Status_Cde.greaterOrEqual("7000") && cwf_Master_Index_Admin_Status_Cde.lessOrEqual("7499")))
            {
                decideConditionsMet233++;
                if (condition(cwf_Master_Index_Last_Updte_Dte.equals(pnd_Misc_Data_Pnd_End_Date_N)))                                                                      //Natural: IF CWF-MASTER-INDEX.LAST-UPDTE-DTE = #END-DATE-N
                {
                    pnd_Work_Record_Pnd_Unassigned_Ind.nadd(1);                                                                                                           //Natural: ADD 1 TO #WORK-RECORD.#UNASSIGNED-IND
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUES '8000':'8999'
            if (condition(cwf_Master_Index_Admin_Status_Cde.greaterOrEqual("8000") && cwf_Master_Index_Admin_Status_Cde.lessOrEqual("8999")))
            {
                decideConditionsMet233++;
                if (condition(cwf_Master_Index_Last_Updte_Dte.lessOrEqual(pnd_Misc_Data_Pnd_End_Date_N)))                                                                 //Natural: IF CWF-MASTER-INDEX.LAST-UPDTE-DTE LE #END-DATE-N
                {
                    pnd_Work_Record_Pnd_Partic_Closed_Ind.nadd(1);                                                                                                        //Natural: ADD 1 TO #WORK-RECORD.#PARTIC-CLOSED-IND
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'MERG'
            if (condition(cwf_Master_Index_Admin_Status_Cde.equals("MERG")))
            {
                decideConditionsMet233++;
                pnd_Work_Record_Pnd_Merg_Ind.nadd(1);                                                                                                                     //Natural: ADD 1 TO #WORK-RECORD.#MERG-IND
            }                                                                                                                                                             //Natural: NONE
            if (condition(decideConditionsMet233 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(DbsUtil.maskMatches(cwf_Master_Index_Work_Prcss_Id,"N.....")))                                                                                      //Natural: IF CWF-MASTER-INDEX.WORK-PRCSS-ID = MASK ( N..... )
        {
            pnd_Work_Record_Tbl_Wpid_Act.setValue("Z");                                                                                                                   //Natural: MOVE 'Z' TO #WORK-RECORD.TBL-WPID-ACT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Work_Record_Tbl_Wpid_Act.setValue(cwf_Master_Index_Work_Prcss_Id);                                                                                        //Natural: MOVE CWF-MASTER-INDEX.WORK-PRCSS-ID TO #WORK-RECORD.TBL-WPID-ACT
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Work_Record_Tbl_Wpid.setValue(cwf_Master_Index_Work_Prcss_Id);                                                                                                //Natural: MOVE CWF-MASTER-INDEX.WORK-PRCSS-ID TO #WORK-RECORD.TBL-WPID
        pnd_Work_Record_Tbl_Status_Cde.setValue(cwf_Master_Index_Admin_Status_Cde);                                                                                       //Natural: MOVE CWF-MASTER-INDEX.ADMIN-STATUS-CDE TO #WORK-RECORD.TBL-STATUS-CDE
        pnd_Work_Record_Tbl_Pin.setValue(cwf_Master_Index_Pin_Nbr);                                                                                                       //Natural: MOVE CWF-MASTER-INDEX.PIN-NBR TO #WORK-RECORD.TBL-PIN
        pnd_Work_Record_Tbl_Log_Dte_Tme.setValue(cwf_Master_Index_Rqst_Log_Dte_Tme);                                                                                      //Natural: MOVE CWF-MASTER-INDEX.RQST-LOG-DTE-TME TO #WORK-RECORD.TBL-LOG-DTE-TME
        pnd_Work_Record_Tbl_Log_Unit_Cde.setValue(cwf_Master_Index_Admin_Unit_Cde);                                                                                       //Natural: MOVE CWF-MASTER-INDEX.ADMIN-UNIT-CDE TO #WORK-RECORD.TBL-LOG-UNIT-CDE #WORK-RECORD.TBL-ADMIN-UNIT
        pnd_Work_Record_Tbl_Admin_Unit.setValue(cwf_Master_Index_Admin_Unit_Cde);
        pnd_Work_Record_Tbl_Log_By.setValue(cwf_Master_Index_Empl_Racf_Id);                                                                                               //Natural: MOVE CWF-MASTER-INDEX.EMPL-RACF-ID TO #WORK-RECORD.TBL-LOG-BY
        pnd_Work_Record_Tbl_Last_Chge_Dte.setValue(cwf_Master_Index_Last_Chnge_Dte_Tme);                                                                                  //Natural: MOVE CWF-MASTER-INDEX.LAST-CHNGE-DTE-TME TO #WORK-RECORD.TBL-LAST-CHGE-DTE
        pnd_Work_Record_Tbl_Chnge_Unit.setValue(cwf_Master_Index_Last_Chnge_Unit_Cde);                                                                                    //Natural: MOVE CWF-MASTER-INDEX.LAST-CHNGE-UNIT-CDE TO #WORK-RECORD.TBL-CHNGE-UNIT
        pnd_Work_Record_Tbl_Last_Updte_Dte.setValue(cwf_Master_Index_Last_Updte_Dte);                                                                                     //Natural: MOVE CWF-MASTER-INDEX.LAST-UPDTE-DTE TO #WORK-RECORD.TBL-LAST-UPDTE-DTE
        if (condition((((((pnd_Work_Record_Tbl_Status_Cde.greaterOrEqual("0900") && pnd_Work_Record_Tbl_Status_Cde.lessOrEqual("7999")) && pnd_Work_Record_Tbl_Last_Updte_Dte.equals(pnd_Misc_Data_Pnd_Start_Date_N))  //Natural: IF ( #WORK-RECORD.TBL-STATUS-CDE = '0900' THRU '7999' AND #WORK-RECORD.TBL-LAST-UPDTE-DTE = #START-DATE-N ) OR ( #WORK-RECORD.TBL-STATUS-CDE = '0000' THRU '7999' AND #WORK-RECORD.TBL-LAST-UPDTE-DTE = #END-DATE-N ) OR ( #WORK-RECORD.TBL-STATUS-CDE = 'MERG' OR #WORK-RECORD.TBL-STATUS-CDE = '0001' THRU '0199' OR #WORK-RECORD.TBL-STATUS-CDE = '0300' THRU '0899' OR #WORK-RECORD.TBL-STATUS-CDE = '4500' THRU '4997' OR #WORK-RECORD.TBL-STATUS-CDE = '7000' THRU '7499' OR #WORK-RECORD.TBL-STATUS-CDE = '8000' THRU '8999' ) OR ( #WORK-RECORD.TBL-STATUS-CDE = '4000' THRU '4449' AND CWF-MASTER-INDEX.LAST-CHNGE-UNIT-ID-CDE NE CWF-MASTER-INDEX.ADMIN-UNIT-ID-CDE )
            || ((pnd_Work_Record_Tbl_Status_Cde.greaterOrEqual("0000") && pnd_Work_Record_Tbl_Status_Cde.lessOrEqual("7999")) && pnd_Work_Record_Tbl_Last_Updte_Dte.equals(pnd_Misc_Data_Pnd_End_Date_N))) 
            || (((((pnd_Work_Record_Tbl_Status_Cde.equals("MERG") || (pnd_Work_Record_Tbl_Status_Cde.greaterOrEqual("0001") && pnd_Work_Record_Tbl_Status_Cde.lessOrEqual("0199"))) 
            || (pnd_Work_Record_Tbl_Status_Cde.greaterOrEqual("0300") && pnd_Work_Record_Tbl_Status_Cde.lessOrEqual("0899"))) || (pnd_Work_Record_Tbl_Status_Cde.greaterOrEqual("4500") 
            && pnd_Work_Record_Tbl_Status_Cde.lessOrEqual("4997"))) || (pnd_Work_Record_Tbl_Status_Cde.greaterOrEqual("7000") && pnd_Work_Record_Tbl_Status_Cde.lessOrEqual("7499"))) 
            || (pnd_Work_Record_Tbl_Status_Cde.greaterOrEqual("8000") && pnd_Work_Record_Tbl_Status_Cde.lessOrEqual("8999")))) || ((pnd_Work_Record_Tbl_Status_Cde.greaterOrEqual("4000") 
            && pnd_Work_Record_Tbl_Status_Cde.lessOrEqual("4449")) && cwf_Master_Index_Last_Chnge_Unit_Id_Cde.notEquals(cwf_Master_Index_Admin_Unit_Id_Cde)))))
        {
            getWorkFiles().write(1, false, pnd_Work_Record);                                                                                                              //Natural: WRITE WORK FILE 1 #WORK-RECORD
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Work_Record_Pnd_New_Ind.reset();                                                                                                                              //Natural: RESET #WORK-RECORD.#NEW-IND #WORK-RECORD.#MERG-IND #WORK-RECORD.#E-PENDED-IND #WORK-RECORD.#PARTIC-CLOSED-IND #WORK-RECORD.#ASSIGNED-IND #WORK-RECORD.#UNASSIGNED-IND
        pnd_Work_Record_Pnd_Merg_Ind.reset();
        pnd_Work_Record_Pnd_E_Pended_Ind.reset();
        pnd_Work_Record_Pnd_Partic_Closed_Ind.reset();
        pnd_Work_Record_Pnd_Assigned_Ind.reset();
        pnd_Work_Record_Pnd_Unassigned_Ind.reset();
    }

    //
}
