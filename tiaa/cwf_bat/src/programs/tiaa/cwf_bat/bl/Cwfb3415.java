/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:29:36 PM
**        * FROM NATURAL PROGRAM : Cwfb3415
************************************************************
**        * FILE NAME            : Cwfb3415.java
**        * CLASS NAME           : Cwfb3415
**        * INSTANCE NAME        : Cwfb3415
************************************************************
**SAG GENERATOR: BATCH-TIAA                       VERSION: 3.2.2
**SAG TITLE: EXTERNAL AFFAIRS REPORT
**SAG SYSTEM: CRPCWF
**SAG GDA: CWFG000
**SAG REPORT-HEADING(1): CORPORATE WORKFLOW FACILITIES
**SAG PRINT-FILE(1): 1
**SAG REPORT-HEADING(2): ACTUARIAL SUMMARY OF DUE DATES IN UNIT
**SAG PRINT-FILE(2): 1
**SAG REPORT-HEADING(3):
**SAG PRINT-FILE(3): 1
**SAG REPORT-HEADING(4):
**SAG PRINT-FILE(4): 1
**SAG HEADING-LINE: 12340000
**SAG DESCS(1): THIS PROGRAM PRINTS THE ACTUARIAL SUMMARY OF
**SAG DESCS(2): DUE DATES IN THE UNIT
**SAG DESCS(3): THIS REPORT IS ONLY FOR ACTUARIAL
**SAG DESCS(4): (ALL UNIT CODE W/ ACT PREFIX)
**SAG PRIMARY-FILE: CWF-MASTER-INDEX-VIEW
**SAG PRIMARY-KEY: UNIT-WPID-KEY
************************************************************************
* PROGRAM  : CWFB3415
* SYSTEM   : CRPCWF
* TITLE    : ACTUARIAL REPORT
* GENERATED: DEC 27,94 AT 08:13 AM
* FUNCTION : THIS PROGRAM PRINTS ACTUARIAL REPORT
*
* HISTORY
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3415 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Program;

    private DataAccessProgramView vw_cwf_Master_Index_View;
    private DbsField cwf_Master_Index_View_Rqst_Log_Dte_Tme;
    private DbsField cwf_Master_Index_View_Orgnl_Log_Dte_Tme;
    private DbsField cwf_Master_Index_View_Work_Prcss_Id;
    private DbsField cwf_Master_Index_View_Empl_Oprtr_Cde;

    private DbsGroup cwf_Master_Index_View__R_Field_1;
    private DbsField cwf_Master_Index_View_Empl_Racf_Id;
    private DbsField cwf_Master_Index_View_Admin_Unit_Cde;
    private DbsField cwf_Master_Index_View_Tiaa_Rcvd_Dte;
    private DbsField cwf_Master_Index_View_Due_Dte_Chg_Prty_Cde;

    private DbsGroup cwf_Master_Index_View__R_Field_2;
    private DbsField cwf_Master_Index_View_Due_Date;
    private DbsField cwf_Master_Index_View_Due_Dte_Chg_Ind;
    private DbsField cwf_Master_Index_View_Due_Dte_Prty_Cde;
    private DbsField pnd_Header1_1;
    private DbsField pnd_Header1_2;

    private DbsGroup pnd_Parm;
    private DbsField pnd_Parm_Old_Rt_Cde;
    private DbsField pnd_Parm_Work_Prcss_Long_Nme;
    private DbsField pnd_Parm_Work_Prcss_Short_Nme;
    private DbsField pnd_Unit_Cde;
    private DbsField pnd_Unit_Name;
    private DbsField pnd_Save_Oprtr_Nme;
    private DbsField pnd_Empl_Nme;
    private DbsField pnd_Wpid;
    private DbsField pnd_Date_Now;
    private DbsField pnd_Work_Due_Date;
    private DbsField pnd_Actuarial_Due_Date;
    private DbsField pnd_Already_Late;
    private DbsField pnd_Due_Today;
    private DbsField pnd_Due_Tommorow;
    private DbsField pnd_Due_In_2_Days;
    private DbsField pnd_Due_More_Than_2_Days;
    private DbsField pnd_Al;
    private DbsField pnd_Dt;
    private DbsField pnd_Dm;
    private DbsField pnd_D2;
    private DbsField pnd_D9;
    private DbsField pnd_Due_Days;

    private DbsRecord internalLoopRecord;
    private DbsField sort01Admin_Unit_CdeOld;
    private DbsField sort01Work_Prcss_IdOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);

        vw_cwf_Master_Index_View = new DataAccessProgramView(new NameInfo("vw_cwf_Master_Index_View", "CWF-MASTER-INDEX-VIEW"), "CWF_MASTER_INDEX_VIEW", 
            "CWF_MASTER_INDEX");
        cwf_Master_Index_View_Rqst_Log_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Master_Index_View_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        cwf_Master_Index_View_Orgnl_Log_Dte_Tme = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Orgnl_Log_Dte_Tme", "ORGNL-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "ORGNL_LOG_DTE_TME");
        cwf_Master_Index_View_Orgnl_Log_Dte_Tme.setDdmHeader("ORIGINAL/LOG DTE/TME");
        cwf_Master_Index_View_Work_Prcss_Id = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Work_Prcss_Id", "WORK-PRCSS-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        cwf_Master_Index_View_Work_Prcss_Id.setDdmHeader("WORK/ID");
        cwf_Master_Index_View_Empl_Oprtr_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "EMPL_OPRTR_CDE");
        cwf_Master_Index_View_Empl_Oprtr_Cde.setDdmHeader("EMPLOYEE/ID");

        cwf_Master_Index_View__R_Field_1 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_1", "REDEFINE", cwf_Master_Index_View_Empl_Oprtr_Cde);
        cwf_Master_Index_View_Empl_Racf_Id = cwf_Master_Index_View__R_Field_1.newFieldInGroup("cwf_Master_Index_View_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 
            8);
        cwf_Master_Index_View_Admin_Unit_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Admin_Unit_Cde", "ADMIN-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ADMIN_UNIT_CDE");
        cwf_Master_Index_View_Tiaa_Rcvd_Dte = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_RCVD_DTE");
        cwf_Master_Index_View_Due_Dte_Chg_Prty_Cde = vw_cwf_Master_Index_View.getRecord().newFieldInGroup("cwf_Master_Index_View_Due_Dte_Chg_Prty_Cde", 
            "DUE-DTE-CHG-PRTY-CDE", FieldType.STRING, 16, RepeatingFieldStrategy.None, "DUE_DTE_CHG_PRTY_CDE");
        cwf_Master_Index_View_Due_Dte_Chg_Prty_Cde.setDdmHeader("DUE DATE CHANGE IND. PRIORITY");

        cwf_Master_Index_View__R_Field_2 = vw_cwf_Master_Index_View.getRecord().newGroupInGroup("cwf_Master_Index_View__R_Field_2", "REDEFINE", cwf_Master_Index_View_Due_Dte_Chg_Prty_Cde);
        cwf_Master_Index_View_Due_Date = cwf_Master_Index_View__R_Field_2.newFieldInGroup("cwf_Master_Index_View_Due_Date", "DUE-DATE", FieldType.STRING, 
            8);
        cwf_Master_Index_View_Due_Dte_Chg_Ind = cwf_Master_Index_View__R_Field_2.newFieldInGroup("cwf_Master_Index_View_Due_Dte_Chg_Ind", "DUE-DTE-CHG-IND", 
            FieldType.STRING, 1);
        cwf_Master_Index_View_Due_Dte_Prty_Cde = cwf_Master_Index_View__R_Field_2.newFieldInGroup("cwf_Master_Index_View_Due_Dte_Prty_Cde", "DUE-DTE-PRTY-CDE", 
            FieldType.STRING, 1);
        registerRecord(vw_cwf_Master_Index_View);

        pnd_Header1_1 = localVariables.newFieldInRecord("pnd_Header1_1", "#HEADER1-1", FieldType.STRING, 50);
        pnd_Header1_2 = localVariables.newFieldInRecord("pnd_Header1_2", "#HEADER1-2", FieldType.STRING, 50);

        pnd_Parm = localVariables.newGroupInRecord("pnd_Parm", "#PARM");
        pnd_Parm_Old_Rt_Cde = pnd_Parm.newFieldInGroup("pnd_Parm_Old_Rt_Cde", "OLD-RT-CDE", FieldType.STRING, 4);
        pnd_Parm_Work_Prcss_Long_Nme = pnd_Parm.newFieldInGroup("pnd_Parm_Work_Prcss_Long_Nme", "WORK-PRCSS-LONG-NME", FieldType.STRING, 45);
        pnd_Parm_Work_Prcss_Short_Nme = pnd_Parm.newFieldInGroup("pnd_Parm_Work_Prcss_Short_Nme", "WORK-PRCSS-SHORT-NME", FieldType.STRING, 15);
        pnd_Unit_Cde = localVariables.newFieldInRecord("pnd_Unit_Cde", "#UNIT-CDE", FieldType.STRING, 8);
        pnd_Unit_Name = localVariables.newFieldInRecord("pnd_Unit_Name", "#UNIT-NAME", FieldType.STRING, 45);
        pnd_Save_Oprtr_Nme = localVariables.newFieldInRecord("pnd_Save_Oprtr_Nme", "#SAVE-OPRTR-NME", FieldType.STRING, 30);
        pnd_Empl_Nme = localVariables.newFieldInRecord("pnd_Empl_Nme", "#EMPL-NME", FieldType.STRING, 30);
        pnd_Wpid = localVariables.newFieldInRecord("pnd_Wpid", "#WPID", FieldType.STRING, 6);
        pnd_Date_Now = localVariables.newFieldInRecord("pnd_Date_Now", "#DATE-NOW", FieldType.DATE);
        pnd_Work_Due_Date = localVariables.newFieldInRecord("pnd_Work_Due_Date", "#WORK-DUE-DATE", FieldType.DATE);
        pnd_Actuarial_Due_Date = localVariables.newFieldInRecord("pnd_Actuarial_Due_Date", "#ACTUARIAL-DUE-DATE", FieldType.DATE);
        pnd_Already_Late = localVariables.newFieldInRecord("pnd_Already_Late", "#ALREADY-LATE", FieldType.NUMERIC, 4);
        pnd_Due_Today = localVariables.newFieldInRecord("pnd_Due_Today", "#DUE-TODAY", FieldType.NUMERIC, 4);
        pnd_Due_Tommorow = localVariables.newFieldInRecord("pnd_Due_Tommorow", "#DUE-TOMMOROW", FieldType.NUMERIC, 4);
        pnd_Due_In_2_Days = localVariables.newFieldInRecord("pnd_Due_In_2_Days", "#DUE-IN-2-DAYS", FieldType.NUMERIC, 4);
        pnd_Due_More_Than_2_Days = localVariables.newFieldInRecord("pnd_Due_More_Than_2_Days", "#DUE-MORE-THAN-2-DAYS", FieldType.NUMERIC, 4);
        pnd_Al = localVariables.newFieldInRecord("pnd_Al", "#AL", FieldType.NUMERIC, 4);
        pnd_Dt = localVariables.newFieldInRecord("pnd_Dt", "#DT", FieldType.NUMERIC, 4);
        pnd_Dm = localVariables.newFieldInRecord("pnd_Dm", "#DM", FieldType.NUMERIC, 4);
        pnd_D2 = localVariables.newFieldInRecord("pnd_D2", "#D2", FieldType.NUMERIC, 4);
        pnd_D9 = localVariables.newFieldInRecord("pnd_D9", "#D9", FieldType.NUMERIC, 4);
        pnd_Due_Days = localVariables.newFieldInRecord("pnd_Due_Days", "#DUE-DAYS", FieldType.NUMERIC, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        sort01Admin_Unit_CdeOld = internalLoopRecord.newFieldInRecord("Sort01_Admin_Unit_Cde_OLD", "Admin_Unit_Cde_OLD", FieldType.STRING, 8);
        sort01Work_Prcss_IdOld = internalLoopRecord.newFieldInRecord("Sort01_Work_Prcss_Id_OLD", "Work_Prcss_Id_OLD", FieldType.STRING, 6);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Master_Index_View.reset();
        internalLoopRecord.reset();

        localVariables.reset();
        pnd_Header1_1.setInitialValue("          Corporate Workflow Facilities");
        pnd_Header1_2.setInitialValue("     Actuarial Summary of Due Dates in Unit");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Cwfb3415() throws Exception
    {
        super("Cwfb3415");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        getReports().getPageNumberDbs(1).reset();                                                                                                                         //Natural: RESET *PAGE-NUMBER ( 1 )
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        //*  DEFINE PRINTERS AND FORMATS
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: ASSIGN #PROGRAM = *PROGRAM
        //*  MAP THE CURRENT LANGUAGE CODE TO THE CODE IN THE BATCH MODEL TEXT LDA.
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH' THEN
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        //*  PRIMARY FILE
        vw_cwf_Master_Index_View.startDatabaseRead                                                                                                                        //Natural: READ CWF-MASTER-INDEX-VIEW BY UNIT-WORK-WPID-TME-KEY1 FROM 'ACT'
        (
        "READ_PRIME",
        new Wc[] { new Wc("UNIT_WORK_WPID_TME_KEY1", ">=", "ACT", WcType.BY) },
        new Oc[] { new Oc("UNIT_WORK_WPID_TME_KEY1", "ASC") }
        );
        READ_PRIME:
        while (condition(vw_cwf_Master_Index_View.readNextRow("READ_PRIME")))
        {
            //* *SAG DEFINE EXIT AFTER-PRIME-READ
            if (condition(cwf_Master_Index_View_Admin_Unit_Cde.greater("ACT99999")))                                                                                      //Natural: IF CWF-MASTER-INDEX-VIEW.ADMIN-UNIT-CDE GT 'ACT99999'
            {
                if (true) break READ_PRIME;                                                                                                                               //Natural: ESCAPE BOTTOM ( READ-PRIME. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cwf_Master_Index_View_Orgnl_Log_Dte_Tme.greater(" ")))                                                                                          //Natural: IF CWF-MASTER-INDEX-VIEW.ORGNL-LOG-DTE-TME GT ' '
            {
                getWorkFiles().write(1, false, vw_cwf_Master_Index_View);                                                                                                 //Natural: WRITE WORK FILE 1 CWF-MASTER-INDEX-VIEW
            }                                                                                                                                                             //Natural: END-IF
            getSort().writeSortInData(cwf_Master_Index_View_Admin_Unit_Cde, cwf_Master_Index_View_Empl_Oprtr_Cde, cwf_Master_Index_View_Work_Prcss_Id,                    //Natural: END-ALL
                cwf_Master_Index_View_Rqst_Log_Dte_Tme, cwf_Master_Index_View_Orgnl_Log_Dte_Tme);
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getSort().sortData(cwf_Master_Index_View_Admin_Unit_Cde, cwf_Master_Index_View_Empl_Oprtr_Cde, cwf_Master_Index_View_Work_Prcss_Id);                              //Natural: SORT CWF-MASTER-INDEX-VIEW.ADMIN-UNIT-CDE ASCENDING CWF-MASTER-INDEX-VIEW.EMPL-OPRTR-CDE ASCENDING CWF-MASTER-INDEX-VIEW.WORK-PRCSS-ID ASCENDING USING CWF-MASTER-INDEX-VIEW.RQST-LOG-DTE-TME CWF-MASTER-INDEX-VIEW.ORGNL-LOG-DTE-TME
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(cwf_Master_Index_View_Admin_Unit_Cde, cwf_Master_Index_View_Empl_Oprtr_Cde, cwf_Master_Index_View_Work_Prcss_Id, 
            cwf_Master_Index_View_Rqst_Log_Dte_Tme, cwf_Master_Index_View_Orgnl_Log_Dte_Tme)))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            if (condition(pnd_Unit_Cde.equals(" ")))                                                                                                                      //Natural: IF #UNIT-CDE = ' '
            {
                pnd_Unit_Cde.setValue(cwf_Master_Index_View_Admin_Unit_Cde);                                                                                              //Natural: MOVE CWF-MASTER-INDEX-VIEW.ADMIN-UNIT-CDE TO #UNIT-CDE
                DbsUtil.callnat(Cwfn1103.class , getCurrentProcessState(), pnd_Unit_Cde, pnd_Unit_Name);                                                                  //Natural: CALLNAT 'CWFN1103' #UNIT-CDE #UNIT-NAME
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Unit_Cde.notEquals(cwf_Master_Index_View_Admin_Unit_Cde)))                                                                              //Natural: IF #UNIT-CDE NE CWF-MASTER-INDEX-VIEW.ADMIN-UNIT-CDE
                {
                    pnd_Unit_Cde.setValue(cwf_Master_Index_View_Admin_Unit_Cde);                                                                                          //Natural: MOVE CWF-MASTER-INDEX-VIEW.ADMIN-UNIT-CDE TO #UNIT-CDE
                    DbsUtil.callnat(Cwfn1103.class , getCurrentProcessState(), pnd_Unit_Cde, pnd_Unit_Name);                                                              //Natural: CALLNAT 'CWFN1103' #UNIT-CDE #UNIT-NAME
                    if (condition(Global.isEscape())) return;
                    pnd_Already_Late.reset();                                                                                                                             //Natural: RESET #ALREADY-LATE #DUE-TODAY #DUE-TOMMOROW #DUE-IN-2-DAYS #DUE-MORE-THAN-2-DAYS #SAVE-OPRTR-NME
                    pnd_Due_Today.reset();
                    pnd_Due_Tommorow.reset();
                    pnd_Due_In_2_Days.reset();
                    pnd_Due_More_Than_2_Days.reset();
                    pnd_Save_Oprtr_Nme.reset();
                    getReports().newPage(new ReportSpecification(1));                                                                                                     //Natural: NEWPAGE ( 1 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Empl_Nme.setValue(pnd_Unit_Cde);                                                                                                                          //Natural: MOVE #UNIT-CDE TO #EMPL-NME
            DbsUtil.callnat(Cwfn1107.class , getCurrentProcessState(), cwf_Master_Index_View_Empl_Racf_Id, pnd_Empl_Nme);                                                 //Natural: CALLNAT 'CWFN1107' CWF-MASTER-INDEX-VIEW.EMPL-RACF-ID #EMPL-NME
            if (condition(Global.isEscape())) return;
            if (condition(pnd_Save_Oprtr_Nme.equals(" ")))                                                                                                                //Natural: IF #SAVE-OPRTR-NME = ' '
            {
                if (condition(pnd_Empl_Nme.equals("UNASSIGNED") && cwf_Master_Index_View_Empl_Racf_Id.greater(" ")))                                                      //Natural: IF #EMPL-NME = 'UNASSIGNED' AND CWF-MASTER-INDEX-VIEW.EMPL-RACF-ID GT ' '
                {
                    pnd_Empl_Nme.setValue(cwf_Master_Index_View_Empl_Racf_Id);                                                                                            //Natural: MOVE CWF-MASTER-INDEX-VIEW.EMPL-RACF-ID TO #EMPL-NME
                }                                                                                                                                                         //Natural: END-IF
                pnd_Save_Oprtr_Nme.setValue(pnd_Empl_Nme);                                                                                                                //Natural: MOVE #EMPL-NME TO #SAVE-OPRTR-NME
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"Employee :",pnd_Empl_Nme,NEWLINE,"----------",NEWLINE);                                               //Natural: WRITE ( 1 ) / 'Employee :' #EMPL-NME / '----------' /
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Empl_Nme.notEquals(pnd_Save_Oprtr_Nme)))                                                                                                //Natural: IF #EMPL-NME NE #SAVE-OPRTR-NME
                {
                    if (condition(pnd_Empl_Nme.equals("UNASSIGNED") && cwf_Master_Index_View_Empl_Racf_Id.greater(" ")))                                                  //Natural: IF #EMPL-NME = 'UNASSIGNED' AND CWF-MASTER-INDEX-VIEW.EMPL-RACF-ID GT ' '
                    {
                        pnd_Empl_Nme.setValue(cwf_Master_Index_View_Empl_Racf_Id);                                                                                        //Natural: MOVE CWF-MASTER-INDEX-VIEW.EMPL-RACF-ID TO #EMPL-NME
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Save_Oprtr_Nme.setValue(pnd_Empl_Nme);                                                                                                            //Natural: MOVE #EMPL-NME TO #SAVE-OPRTR-NME
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"Employee :",pnd_Empl_Nme,NEWLINE,"----------",NEWLINE);                                           //Natural: WRITE ( 1 ) / 'Employee :' #EMPL-NME / '----------' /
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //* *SAG END-EXIT
            //* *SAG DEFINE EXIT PRIME-WRITE-FIELDS
            pnd_Work_Due_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),cwf_Master_Index_View_Due_Date);                                                              //Natural: MOVE EDITED CWF-MASTER-INDEX-VIEW.DUE-DATE TO #WORK-DUE-DATE ( EM = YYYYMMDD )
            if (condition(DbsUtil.maskMatches(cwf_Master_Index_View_Work_Prcss_Id,"'I'.....")))                                                                           //Natural: IF CWF-MASTER-INDEX-VIEW.WORK-PRCSS-ID = MASK ( 'I'..... )
            {
                pnd_Actuarial_Due_Date.compute(new ComputeParameters(false, pnd_Actuarial_Due_Date), pnd_Work_Due_Date.subtract(2));                                      //Natural: COMPUTE #ACTUARIAL-DUE-DATE = #WORK-DUE-DATE - 2
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(DbsUtil.maskMatches(cwf_Master_Index_View_Work_Prcss_Id,"'T'.....")))                                                                       //Natural: IF CWF-MASTER-INDEX-VIEW.WORK-PRCSS-ID = MASK ( 'T'..... )
                {
                    pnd_Actuarial_Due_Date.compute(new ComputeParameters(false, pnd_Actuarial_Due_Date), pnd_Work_Due_Date.subtract(1));                                  //Natural: COMPUTE #ACTUARIAL-DUE-DATE = #WORK-DUE-DATE - 1
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Actuarial_Due_Date.setValue(pnd_Work_Due_Date);                                                                                                   //Natural: MOVE #WORK-DUE-DATE TO #ACTUARIAL-DUE-DATE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Date_Now.setValue(Global.getDATX());                                                                                                                      //Natural: MOVE *DATX TO #DATE-NOW
            pnd_Due_Days.compute(new ComputeParameters(false, pnd_Due_Days), pnd_Actuarial_Due_Date.subtract(pnd_Date_Now));                                              //Natural: COMPUTE #DUE-DAYS = #ACTUARIAL-DUE-DATE - #DATE-NOW
            short decideConditionsMet162 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #DUE-DAYS;//Natural: VALUES -9999:-1
            if (condition(((pnd_Due_Days.greaterOrEqual(- 9999) && pnd_Due_Days.lessOrEqual(- 1)))))
            {
                decideConditionsMet162++;
                pnd_Already_Late.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #ALREADY-LATE
            }                                                                                                                                                             //Natural: VALUE 0
            else if (condition((pnd_Due_Days.equals(0))))
            {
                decideConditionsMet162++;
                pnd_Due_Today.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #DUE-TODAY
            }                                                                                                                                                             //Natural: VALUE 1
            else if (condition((pnd_Due_Days.equals(1))))
            {
                decideConditionsMet162++;
                pnd_Due_Tommorow.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #DUE-TOMMOROW
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_Due_Days.equals(2))))
            {
                decideConditionsMet162++;
                pnd_Due_In_2_Days.nadd(1);                                                                                                                                //Natural: ADD 1 TO #DUE-IN-2-DAYS
            }                                                                                                                                                             //Natural: VALUE 3:9999
            else if (condition(((pnd_Due_Days.greaterOrEqual(3) && pnd_Due_Days.lessOrEqual(9999)))))
            {
                decideConditionsMet162++;
                pnd_Due_More_Than_2_Days.nadd(1);                                                                                                                         //Natural: ADD 1 TO #DUE-MORE-THAN-2-DAYS
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //* *SAG END-EXIT                                                                                                                                             //Natural: AT TOP OF PAGE ( 1 );//Natural: AT BREAK OF CWF-MASTER-INDEX-VIEW.WORK-PRCSS-ID;//Natural: AT BREAK OF CWF-MASTER-INDEX-VIEW.ADMIN-UNIT-CDE
            //*  PRIMARY FILE.
            sort01Admin_Unit_CdeOld.setValue(cwf_Master_Index_View_Admin_Unit_Cde);                                                                                       //Natural: END-SORT
            sort01Work_Prcss_IdOld.setValue(cwf_Master_Index_View_Work_Prcss_Id);
        }
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        endSort();
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(41),pnd_Header1_1,new TabSetting(124),"Page",getReports().getPageNumberDbs(1),  //Natural: WRITE ( 1 ) NOTITLE *PROGRAM 41T #HEADER1-1 124T 'Page' *PAGE-NUMBER ( 1 ) ( NL = 4 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 41T #HEADER1-2 124T *TIMX ( EM = HH':'II' 'AP ) // 'Unit :' #UNIT-CDE '-' #UNIT-NAME /
                        new NumericLength (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),new 
                        TabSetting(41),pnd_Header1_2,new TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"),NEWLINE,NEWLINE,"Unit :",
                        pnd_Unit_Cde,"-",pnd_Unit_Name,NEWLINE);
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean cwf_Master_Index_View_Work_Prcss_IdIsBreak = cwf_Master_Index_View_Work_Prcss_Id.isBreak(endOfData);
        boolean cwf_Master_Index_View_Admin_Unit_CdeIsBreak = cwf_Master_Index_View_Admin_Unit_Cde.isBreak(endOfData);
        if (condition(cwf_Master_Index_View_Work_Prcss_IdIsBreak || cwf_Master_Index_View_Admin_Unit_CdeIsBreak))
        {
            if (condition(DbsUtil.maskMatches(sort01Admin_Unit_CdeOld,"'ACT'.....")))                                                                                     //Natural: IF OLD ( CWF-MASTER-INDEX-VIEW.ADMIN-UNIT-CDE ) = MASK ( 'ACT'..... )
            {
                pnd_Wpid.setValue(sort01Work_Prcss_IdOld);                                                                                                                //Natural: MOVE OLD ( CWF-MASTER-INDEX-VIEW.WORK-PRCSS-ID ) TO #WPID
                DbsUtil.callnat(Cwfn0013.class , getCurrentProcessState(), pnd_Wpid, pnd_Parm);                                                                           //Natural: CALLNAT 'CWFN0013' #WPID #PARM
                if (condition(Global.isEscape())) return;
                getReports().display(1, "Work Process/ID",                                                                                                                //Natural: DISPLAY ( 1 ) 'Work Process/ID' #WPID '/' #PARM.WORK-PRCSS-SHORT-NME 'Already/Late' #ALREADY-LATE 'Due/Today' #DUE-TODAY 'Due/Tommorrow' #DUE-TOMMOROW 'Due In/2 Days' #DUE-IN-2-DAYS 'Due In More/Than 2 Days' #DUE-MORE-THAN-2-DAYS
                		pnd_Wpid,"/",
                		pnd_Parm_Work_Prcss_Short_Nme,"Already/Late",
                		pnd_Already_Late,"Due/Today",
                		pnd_Due_Today,"Due/Tommorrow",
                		pnd_Due_Tommorow,"Due In/2 Days",
                		pnd_Due_In_2_Days,"Due In More/Than 2 Days",
                		pnd_Due_More_Than_2_Days);
                if (condition(Global.isEscape())) return;
                pnd_Al.nadd(pnd_Already_Late);                                                                                                                            //Natural: COMPUTE #AL = #AL + #ALREADY-LATE
                pnd_Dt.nadd(pnd_Due_Today);                                                                                                                               //Natural: COMPUTE #DT = #DT + #DUE-TODAY
                pnd_Dm.nadd(pnd_Due_Tommorow);                                                                                                                            //Natural: COMPUTE #DM = #DM + #DUE-TOMMOROW
                pnd_D2.nadd(pnd_Due_In_2_Days);                                                                                                                           //Natural: COMPUTE #D2 = #D2 + #DUE-IN-2-DAYS
                pnd_D9.nadd(pnd_Due_More_Than_2_Days);                                                                                                                    //Natural: COMPUTE #D9 = #D9 + #DUE-MORE-THAN-2-DAYS
                pnd_Already_Late.reset();                                                                                                                                 //Natural: RESET #ALREADY-LATE #DUE-TODAY #DUE-TOMMOROW #DUE-IN-2-DAYS #DUE-MORE-THAN-2-DAYS
                pnd_Due_Today.reset();
                pnd_Due_Tommorow.reset();
                pnd_Due_In_2_Days.reset();
                pnd_Due_More_Than_2_Days.reset();
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(cwf_Master_Index_View_Admin_Unit_CdeIsBreak))
        {
            if (condition(DbsUtil.maskMatches(sort01Admin_Unit_CdeOld,"'ACT'.....")))                                                                                     //Natural: IF OLD ( CWF-MASTER-INDEX-VIEW.ADMIN-UNIT-CDE ) = MASK ( 'ACT'..... )
            {
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new ReportTAsterisk(pnd_Parm_Work_Prcss_Short_Nme),"Unit Total",new ReportTAsterisk(pnd_Already_Late),pnd_Al,new  //Natural: WRITE ( 1 ) / T*#PARM.WORK-PRCSS-SHORT-NME 'Unit Total' T*#ALREADY-LATE #AL T*#DUE-TODAY #DT T*#DUE-TOMMOROW #DM T*#DUE-IN-2-DAYS #D2 T*#DUE-MORE-THAN-2-DAYS #D9
                    ReportTAsterisk(pnd_Due_Today),pnd_Dt,new ReportTAsterisk(pnd_Due_Tommorow),pnd_Dm,new ReportTAsterisk(pnd_Due_In_2_Days),pnd_D2,new 
                    ReportTAsterisk(pnd_Due_More_Than_2_Days),pnd_D9);
                if (condition(Global.isEscape())) return;
                pnd_Al.reset();                                                                                                                                           //Natural: RESET #AL #DT #DM #D2 #D9
                pnd_Dt.reset();
                pnd_Dm.reset();
                pnd_D2.reset();
                pnd_D9.reset();
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");

        getReports().setDisplayColumns(1, "Work Process/ID",
        		pnd_Wpid,"/",
        		pnd_Parm_Work_Prcss_Short_Nme,"Already/Late",
        		pnd_Already_Late,"Due/Today",
        		pnd_Due_Today,"Due/Tommorrow",
        		pnd_Due_Tommorow,"Due In/2 Days",
        		pnd_Due_In_2_Days,"Due In More/Than 2 Days",
        		pnd_Due_More_Than_2_Days);
    }
}
