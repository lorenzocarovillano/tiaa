/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:42:37 PM
**        * FROM NATURAL PROGRAM : Cwfb5910
************************************************************
**        * FILE NAME            : Cwfb5910.java
**        * CLASS NAME           : Cwfb5910
**        * INSTANCE NAME        : Cwfb5910
************************************************************
************************************************************************
**  PROGRAM NAME: CWFB5900
**  SYSTEM      : POWERIMAGE - BANKING MODEL
**  DESCRIPTION : READ PI EXTRACT FOR BANKING MODEL AND UPDATE CWF
**                CORPORATE SUPPORT TABLE
**
**  HISTORY     :
**  09/19/2005    LS        CREATED PROGRAM
**
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb5910 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Wk_Omni_Rec;
    private DbsField pnd_Wk_Omni_Rec_Pnd_Wk_Omni_Plan_Id;
    private DbsField pnd_Wk_Omni_Rec_Pnd_Wk_Omni_Data;

    private DbsGroup pnd_Wk_Omni_Rec__R_Field_1;
    private DbsField pnd_Wk_Omni_Rec_Pnd_Omni_Pl167;
    private DbsField pnd_Wk_Omni_Rec_Pnd_Omni_Pl168;
    private DbsField pnd_Wk_Omni_Rec_Pnd_Omni_Pl170;
    private DbsField pnd_Wk_Omni_Rec_Pnd_Wk_Omni_Pm505;

    private DataAccessProgramView vw_conv2;
    private DbsField conv2_Omni_Institutional_Code;
    private DbsField conv2_Omni_Plan_Number;
    private DbsField conv2_Omni_Lob_Sublob;
    private DbsField conv2_Tiaa_Related_Ppg_Code;
    private DbsField conv2_Tiaa_Related_Source;
    private DbsField conv2_Plan_Comp_Record_Type;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_2;
    private DbsField cwf_Support_Tbl_Ppg;
    private DbsField cwf_Support_Tbl_Planid;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_3;
    private DbsField cwf_Support_Tbl_Pnd_Tbl_Pm505;
    private DbsField cwf_Support_Tbl_Tbl_Actve_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Entry_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Dte;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde;

    private DataAccessProgramView vw_upd_Cwf_Support_Tbl;
    private DbsField upd_Cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField upd_Cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField upd_Cwf_Support_Tbl_Tbl_Key_Field;

    private DbsGroup upd_Cwf_Support_Tbl__R_Field_4;
    private DbsField upd_Cwf_Support_Tbl_Ppg;
    private DbsField upd_Cwf_Support_Tbl_Planid;
    private DbsField upd_Cwf_Support_Tbl_Tbl_Data_Field;

    private DbsGroup upd_Cwf_Support_Tbl__R_Field_5;
    private DbsField upd_Cwf_Support_Tbl_Pnd_Tbl_Pm505;
    private DbsField upd_Cwf_Support_Tbl_Tbl_Actve_Ind;
    private DbsField upd_Cwf_Support_Tbl_Tbl_Entry_Dte_Tme;
    private DbsField upd_Cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde;
    private DbsField upd_Cwf_Support_Tbl_Tbl_Updte_Dte;
    private DbsField upd_Cwf_Support_Tbl_Tbl_Updte_Dte_Tme;
    private DbsField upd_Cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde;
    private DbsField pnd_Tbl_Key;

    private DbsGroup pnd_Tbl_Key__R_Field_6;
    private DbsField pnd_Tbl_Key_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Key_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Key_Tbl_Key_Field;

    private DbsGroup pnd_Tbl_Key__R_Field_7;
    private DbsField pnd_Tbl_Key_Tbl_Ppg;
    private DbsField pnd_Tbl_Key_Tbl_Omniplan;
    private DbsField pnd_Tbl_Key_Tbl_Actve_Ind;

    private DbsGroup pnd_Banking_Model_Omni;
    private DbsField pnd_Banking_Model_Omni_Pnd_Banking_Desc;
    private DbsField pnd_Banking_Model_Omni_Pnd_Banking_Pm505;

    private DbsGroup pnd_Misc_Variables;
    private DbsField pnd_Misc_Variables_Pnd_I;
    private DbsField pnd_Misc_Variables_Pnd_J;
    private DbsField pnd_Misc_Variables_Pnd_No_Of_Model;
    private DbsField pnd_Misc_Variables_Pnd_Found_Match;
    private DbsField pnd_Misc_Variables_Pnd_Found_Ppg;
    private DbsField pnd_Misc_Variables_Pnd_Prev_Ppg;
    private DbsField pnd_Misc_Variables_Pnd_Isn;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Wk_Omni_Rec = localVariables.newGroupInRecord("pnd_Wk_Omni_Rec", "#WK-OMNI-REC");
        pnd_Wk_Omni_Rec_Pnd_Wk_Omni_Plan_Id = pnd_Wk_Omni_Rec.newFieldInGroup("pnd_Wk_Omni_Rec_Pnd_Wk_Omni_Plan_Id", "#WK-OMNI-PLAN-ID", FieldType.STRING, 
            6);
        pnd_Wk_Omni_Rec_Pnd_Wk_Omni_Data = pnd_Wk_Omni_Rec.newFieldInGroup("pnd_Wk_Omni_Rec_Pnd_Wk_Omni_Data", "#WK-OMNI-DATA", FieldType.STRING, 28);

        pnd_Wk_Omni_Rec__R_Field_1 = pnd_Wk_Omni_Rec.newGroupInGroup("pnd_Wk_Omni_Rec__R_Field_1", "REDEFINE", pnd_Wk_Omni_Rec_Pnd_Wk_Omni_Data);
        pnd_Wk_Omni_Rec_Pnd_Omni_Pl167 = pnd_Wk_Omni_Rec__R_Field_1.newFieldInGroup("pnd_Wk_Omni_Rec_Pnd_Omni_Pl167", "#OMNI-PL167", FieldType.STRING, 
            10);
        pnd_Wk_Omni_Rec_Pnd_Omni_Pl168 = pnd_Wk_Omni_Rec__R_Field_1.newFieldInGroup("pnd_Wk_Omni_Rec_Pnd_Omni_Pl168", "#OMNI-PL168", FieldType.STRING, 
            4);
        pnd_Wk_Omni_Rec_Pnd_Omni_Pl170 = pnd_Wk_Omni_Rec__R_Field_1.newFieldInGroup("pnd_Wk_Omni_Rec_Pnd_Omni_Pl170", "#OMNI-PL170", FieldType.STRING, 
            4);
        pnd_Wk_Omni_Rec_Pnd_Wk_Omni_Pm505 = pnd_Wk_Omni_Rec__R_Field_1.newFieldInGroup("pnd_Wk_Omni_Rec_Pnd_Wk_Omni_Pm505", "#WK-OMNI-PM505", FieldType.STRING, 
            10);

        vw_conv2 = new DataAccessProgramView(new NameInfo("vw_conv2", "CONV2"), "SGC_PLAN_COMPONENTS_CONV_HIST_FL", "SGC_PLAN_CMPNT");
        conv2_Omni_Institutional_Code = vw_conv2.getRecord().newFieldInGroup("conv2_Omni_Institutional_Code", "OMNI-INSTITUTIONAL-CODE", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "OMNI_INSTITUTIONAL_CODE");
        conv2_Omni_Plan_Number = vw_conv2.getRecord().newFieldInGroup("conv2_Omni_Plan_Number", "OMNI-PLAN-NUMBER", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "OMNI_PLAN_NUMBER");
        conv2_Omni_Lob_Sublob = vw_conv2.getRecord().newFieldInGroup("conv2_Omni_Lob_Sublob", "OMNI-LOB-SUBLOB", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "OMNI_LOB_SUBLOB");
        conv2_Tiaa_Related_Ppg_Code = vw_conv2.getRecord().newFieldInGroup("conv2_Tiaa_Related_Ppg_Code", "TIAA-RELATED-PPG-CODE", FieldType.STRING, 6, 
            RepeatingFieldStrategy.None, "TIAA_RELATED_PPG_CODE");
        conv2_Tiaa_Related_Source = vw_conv2.getRecord().newFieldInGroup("conv2_Tiaa_Related_Source", "TIAA-RELATED-SOURCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIAA_RELATED_SOURCE");
        conv2_Plan_Comp_Record_Type = vw_conv2.getRecord().newFieldInGroup("conv2_Plan_Comp_Record_Type", "PLAN-COMP-RECORD-TYPE", FieldType.STRING, 2, 
            RepeatingFieldStrategy.None, "PLAN_COMP_RECORD_TYPE");
        registerRecord(vw_conv2);

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");

        cwf_Support_Tbl__R_Field_2 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_2", "REDEFINE", cwf_Support_Tbl_Tbl_Key_Field);
        cwf_Support_Tbl_Ppg = cwf_Support_Tbl__R_Field_2.newFieldInGroup("cwf_Support_Tbl_Ppg", "PPG", FieldType.STRING, 6);
        cwf_Support_Tbl_Planid = cwf_Support_Tbl__R_Field_2.newFieldInGroup("cwf_Support_Tbl_Planid", "PLANID", FieldType.STRING, 6);
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Support_Tbl__R_Field_3 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_3", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Pnd_Tbl_Pm505 = cwf_Support_Tbl__R_Field_3.newFieldInGroup("cwf_Support_Tbl_Pnd_Tbl_Pm505", "#TBL-PM505", FieldType.STRING, 10);
        cwf_Support_Tbl_Tbl_Actve_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Actve_Ind", "TBL-ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TBL_ACTVE_IND");
        cwf_Support_Tbl_Tbl_Entry_Dte_Tme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Entry_Dte_Tme", "TBL-ENTRY-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_ENTRY_DTE_TME");
        cwf_Support_Tbl_Tbl_Entry_Dte_Tme.setDdmHeader("ENTRY DATE/TIME");
        cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde", "TBL-ENTRY-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_ENTRY_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Support_Tbl_Tbl_Updte_Dte = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Dte", "TBL-UPDTE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE");
        cwf_Support_Tbl_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        cwf_Support_Tbl_Tbl_Updte_Dte_Tme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Dte_Tme", "TBL-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE_TME");
        cwf_Support_Tbl_Tbl_Updte_Dte_Tme.setDdmHeader("UPDATE DATE/TIME");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        registerRecord(vw_cwf_Support_Tbl);

        vw_upd_Cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_upd_Cwf_Support_Tbl", "UPD-CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        upd_Cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_upd_Cwf_Support_Tbl.getRecord().newFieldInGroup("upd_Cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        upd_Cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        upd_Cwf_Support_Tbl_Tbl_Table_Nme = vw_upd_Cwf_Support_Tbl.getRecord().newFieldInGroup("upd_Cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        upd_Cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        upd_Cwf_Support_Tbl_Tbl_Key_Field = vw_upd_Cwf_Support_Tbl.getRecord().newFieldInGroup("upd_Cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");

        upd_Cwf_Support_Tbl__R_Field_4 = vw_upd_Cwf_Support_Tbl.getRecord().newGroupInGroup("upd_Cwf_Support_Tbl__R_Field_4", "REDEFINE", upd_Cwf_Support_Tbl_Tbl_Key_Field);
        upd_Cwf_Support_Tbl_Ppg = upd_Cwf_Support_Tbl__R_Field_4.newFieldInGroup("upd_Cwf_Support_Tbl_Ppg", "PPG", FieldType.STRING, 6);
        upd_Cwf_Support_Tbl_Planid = upd_Cwf_Support_Tbl__R_Field_4.newFieldInGroup("upd_Cwf_Support_Tbl_Planid", "PLANID", FieldType.STRING, 6);
        upd_Cwf_Support_Tbl_Tbl_Data_Field = vw_upd_Cwf_Support_Tbl.getRecord().newFieldInGroup("upd_Cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", 
            FieldType.STRING, 253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        upd_Cwf_Support_Tbl__R_Field_5 = vw_upd_Cwf_Support_Tbl.getRecord().newGroupInGroup("upd_Cwf_Support_Tbl__R_Field_5", "REDEFINE", upd_Cwf_Support_Tbl_Tbl_Data_Field);
        upd_Cwf_Support_Tbl_Pnd_Tbl_Pm505 = upd_Cwf_Support_Tbl__R_Field_5.newFieldInGroup("upd_Cwf_Support_Tbl_Pnd_Tbl_Pm505", "#TBL-PM505", FieldType.STRING, 
            10);
        upd_Cwf_Support_Tbl_Tbl_Actve_Ind = vw_upd_Cwf_Support_Tbl.getRecord().newFieldInGroup("upd_Cwf_Support_Tbl_Tbl_Actve_Ind", "TBL-ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TBL_ACTVE_IND");
        upd_Cwf_Support_Tbl_Tbl_Entry_Dte_Tme = vw_upd_Cwf_Support_Tbl.getRecord().newFieldInGroup("upd_Cwf_Support_Tbl_Tbl_Entry_Dte_Tme", "TBL-ENTRY-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "TBL_ENTRY_DTE_TME");
        upd_Cwf_Support_Tbl_Tbl_Entry_Dte_Tme.setDdmHeader("ENTRY DATE/TIME");
        upd_Cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde = vw_upd_Cwf_Support_Tbl.getRecord().newFieldInGroup("upd_Cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde", "TBL-ENTRY-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_ENTRY_OPRTR_CDE");
        upd_Cwf_Support_Tbl_Tbl_Entry_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        upd_Cwf_Support_Tbl_Tbl_Updte_Dte = vw_upd_Cwf_Support_Tbl.getRecord().newFieldInGroup("upd_Cwf_Support_Tbl_Tbl_Updte_Dte", "TBL-UPDTE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE");
        upd_Cwf_Support_Tbl_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        upd_Cwf_Support_Tbl_Tbl_Updte_Dte_Tme = vw_upd_Cwf_Support_Tbl.getRecord().newFieldInGroup("upd_Cwf_Support_Tbl_Tbl_Updte_Dte_Tme", "TBL-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "TBL_UPDTE_DTE_TME");
        upd_Cwf_Support_Tbl_Tbl_Updte_Dte_Tme.setDdmHeader("UPDATE DATE/TIME");
        upd_Cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde = vw_upd_Cwf_Support_Tbl.getRecord().newFieldInGroup("upd_Cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        upd_Cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        registerRecord(vw_upd_Cwf_Support_Tbl);

        pnd_Tbl_Key = localVariables.newFieldInRecord("pnd_Tbl_Key", "#TBL-KEY", FieldType.STRING, 47);

        pnd_Tbl_Key__R_Field_6 = localVariables.newGroupInRecord("pnd_Tbl_Key__R_Field_6", "REDEFINE", pnd_Tbl_Key);
        pnd_Tbl_Key_Tbl_Scrty_Level_Ind = pnd_Tbl_Key__R_Field_6.newFieldInGroup("pnd_Tbl_Key_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", FieldType.STRING, 
            2);
        pnd_Tbl_Key_Tbl_Table_Nme = pnd_Tbl_Key__R_Field_6.newFieldInGroup("pnd_Tbl_Key_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 20);
        pnd_Tbl_Key_Tbl_Key_Field = pnd_Tbl_Key__R_Field_6.newFieldInGroup("pnd_Tbl_Key_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 12);

        pnd_Tbl_Key__R_Field_7 = pnd_Tbl_Key__R_Field_6.newGroupInGroup("pnd_Tbl_Key__R_Field_7", "REDEFINE", pnd_Tbl_Key_Tbl_Key_Field);
        pnd_Tbl_Key_Tbl_Ppg = pnd_Tbl_Key__R_Field_7.newFieldInGroup("pnd_Tbl_Key_Tbl_Ppg", "TBL-PPG", FieldType.STRING, 6);
        pnd_Tbl_Key_Tbl_Omniplan = pnd_Tbl_Key__R_Field_7.newFieldInGroup("pnd_Tbl_Key_Tbl_Omniplan", "TBL-OMNIPLAN", FieldType.STRING, 6);
        pnd_Tbl_Key_Tbl_Actve_Ind = pnd_Tbl_Key__R_Field_6.newFieldInGroup("pnd_Tbl_Key_Tbl_Actve_Ind", "TBL-ACTVE-IND", FieldType.STRING, 1);

        pnd_Banking_Model_Omni = localVariables.newGroupArrayInRecord("pnd_Banking_Model_Omni", "#BANKING-MODEL-OMNI", new DbsArrayController(1, 20));
        pnd_Banking_Model_Omni_Pnd_Banking_Desc = pnd_Banking_Model_Omni.newFieldInGroup("pnd_Banking_Model_Omni_Pnd_Banking_Desc", "#BANKING-DESC", FieldType.STRING, 
            30);
        pnd_Banking_Model_Omni_Pnd_Banking_Pm505 = pnd_Banking_Model_Omni.newFieldInGroup("pnd_Banking_Model_Omni_Pnd_Banking_Pm505", "#BANKING-PM505", 
            FieldType.STRING, 10);

        pnd_Misc_Variables = localVariables.newGroupInRecord("pnd_Misc_Variables", "#MISC-VARIABLES");
        pnd_Misc_Variables_Pnd_I = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_Misc_Variables_Pnd_J = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_J", "#J", FieldType.NUMERIC, 3);
        pnd_Misc_Variables_Pnd_No_Of_Model = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_No_Of_Model", "#NO-OF-MODEL", FieldType.NUMERIC, 
            3);
        pnd_Misc_Variables_Pnd_Found_Match = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Found_Match", "#FOUND-MATCH", FieldType.STRING, 
            1);
        pnd_Misc_Variables_Pnd_Found_Ppg = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Found_Ppg", "#FOUND-PPG", FieldType.STRING, 1);
        pnd_Misc_Variables_Pnd_Prev_Ppg = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Prev_Ppg", "#PREV-PPG", FieldType.STRING, 6);
        pnd_Misc_Variables_Pnd_Isn = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 10);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_conv2.reset();
        vw_cwf_Support_Tbl.reset();
        vw_upd_Cwf_Support_Tbl.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb5910() throws Exception
    {
        super("Cwfb5910");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
                                                                                                                                                                          //Natural: PERFORM GET-BANKING-MODEL-OMNI
        sub_Get_Banking_Model_Omni();
        if (condition(Global.isEscape())) {return;}
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #WK-OMNI-REC
        while (condition(getWorkFiles().read(1, pnd_Wk_Omni_Rec)))
        {
            pnd_Misc_Variables_Pnd_Found_Match.setValue("N");                                                                                                             //Natural: ASSIGN #FOUND-MATCH = 'N'
            FOR01:                                                                                                                                                        //Natural: FOR #I = 1 TO #NO-OF-MODEL
            for (pnd_Misc_Variables_Pnd_I.setValue(1); condition(pnd_Misc_Variables_Pnd_I.lessOrEqual(pnd_Misc_Variables_Pnd_No_Of_Model)); pnd_Misc_Variables_Pnd_I.nadd(1))
            {
                if (condition(pnd_Wk_Omni_Rec_Pnd_Wk_Omni_Pm505.equals(pnd_Banking_Model_Omni_Pnd_Banking_Pm505.getValue(pnd_Misc_Variables_Pnd_I))))                     //Natural: IF #WK-OMNI-PM505 = #BANKING-PM505 ( #I )
                {
                    pnd_Misc_Variables_Pnd_Found_Match.setValue("Y");                                                                                                     //Natural: ASSIGN #FOUND-MATCH = 'Y'
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Misc_Variables_Pnd_Found_Match.equals("Y")))                                                                                                //Natural: IF #FOUND-MATCH = 'Y'
            {
                                                                                                                                                                          //Natural: PERFORM UPDATE-CWF-SUPPORT-TABLE
                sub_Update_Cwf_Support_Table();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //* ****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-BANKING-MODEL-OMNI
        //* ****************************************
        //* *****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-CWF-SUPPORT-TABLE
    }
    private void sub_Get_Banking_Model_Omni() throws Exception                                                                                                            //Natural: GET-BANKING-MODEL-OMNI
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Tbl_Key_Tbl_Scrty_Level_Ind.setValue("A");                                                                                                                    //Natural: ASSIGN #TBL-KEY.TBL-SCRTY-LEVEL-IND := 'A'
        pnd_Tbl_Key_Tbl_Table_Nme.setValue("BANKING-MODEL-OMNI");                                                                                                         //Natural: ASSIGN #TBL-KEY.TBL-TABLE-NME := 'BANKING-MODEL-OMNI'
        pnd_Tbl_Key_Tbl_Key_Field.setValue(" ");                                                                                                                          //Natural: ASSIGN #TBL-KEY.TBL-KEY-FIELD := ' '
        vw_cwf_Support_Tbl.startDatabaseRead                                                                                                                              //Natural: READ CWF-SUPPORT-TBL BY TBL-PRIME-KEY STARTING FROM #TBL-KEY
        (
        "READ02",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
        );
        READ02:
        while (condition(vw_cwf_Support_Tbl.readNextRow("READ02")))
        {
            if (condition(cwf_Support_Tbl_Tbl_Table_Nme.notEquals(pnd_Tbl_Key_Tbl_Table_Nme)))                                                                            //Natural: IF CWF-SUPPORT-TBL.TBL-TABLE-NME NE #TBL-KEY.TBL-TABLE-NME
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            FOR02:                                                                                                                                                        //Natural: FOR #I = 1 TO 20
            for (pnd_Misc_Variables_Pnd_I.setValue(1); condition(pnd_Misc_Variables_Pnd_I.lessOrEqual(20)); pnd_Misc_Variables_Pnd_I.nadd(1))
            {
                if (condition(pnd_Banking_Model_Omni_Pnd_Banking_Desc.getValue(pnd_Misc_Variables_Pnd_I).equals(" ")))                                                    //Natural: IF #BANKING-DESC ( #I ) = ' '
                {
                    pnd_Banking_Model_Omni_Pnd_Banking_Desc.getValue(pnd_Misc_Variables_Pnd_I).setValue(cwf_Support_Tbl_Tbl_Key_Field);                                   //Natural: ASSIGN #BANKING-DESC ( #I ) := CWF-SUPPORT-TBL.TBL-KEY-FIELD
                    pnd_Banking_Model_Omni_Pnd_Banking_Pm505.getValue(pnd_Misc_Variables_Pnd_I).setValue(cwf_Support_Tbl_Pnd_Tbl_Pm505);                                  //Natural: ASSIGN #BANKING-PM505 ( #I ) := #TBL-PM505
                    pnd_Misc_Variables_Pnd_No_Of_Model.nadd(1);                                                                                                           //Natural: ADD 1 TO #NO-OF-MODEL
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Update_Cwf_Support_Table() throws Exception                                                                                                          //Natural: UPDATE-CWF-SUPPORT-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************
        pnd_Misc_Variables_Pnd_Prev_Ppg.reset();                                                                                                                          //Natural: RESET #PREV-PPG
        vw_conv2.startDatabaseRead                                                                                                                                        //Natural: READ CONV2 BY OMNI-PLAN-NUMBER STARTING FROM #WK-OMNI-PLAN-ID
        (
        "R1",
        new Wc[] { new Wc("OMNI_PLAN_NUMBER", ">=", pnd_Wk_Omni_Rec_Pnd_Wk_Omni_Plan_Id, WcType.BY) },
        new Oc[] { new Oc("OMNI_PLAN_NUMBER", "ASC") }
        );
        R1:
        while (condition(vw_conv2.readNextRow("R1")))
        {
            if (condition(conv2_Omni_Plan_Number.notEquals(pnd_Wk_Omni_Rec_Pnd_Wk_Omni_Plan_Id)))                                                                         //Natural: IF OMNI-PLAN-NUMBER NE #WK-OMNI-PLAN-ID
            {
                if (true) break R1;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R1. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(conv2_Plan_Comp_Record_Type.equals("15") || conv2_Plan_Comp_Record_Type.equals("25") || conv2_Tiaa_Related_Ppg_Code.equals(" ")                 //Natural: IF ( PLAN-COMP-RECORD-TYPE = '15' OR = '25' ) OR TIAA-RELATED-PPG-CODE = ' ' OR #PREV-PPG = TIAA-RELATED-PPG-CODE
                || pnd_Misc_Variables_Pnd_Prev_Ppg.equals(conv2_Tiaa_Related_Ppg_Code)))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Misc_Variables_Pnd_Prev_Ppg.setValue(conv2_Tiaa_Related_Ppg_Code);                                                                                        //Natural: ASSIGN #PREV-PPG := TIAA-RELATED-PPG-CODE
            pnd_Tbl_Key_Tbl_Scrty_Level_Ind.setValue("A");                                                                                                                //Natural: ASSIGN #TBL-KEY.TBL-SCRTY-LEVEL-IND := 'A'
            pnd_Tbl_Key_Tbl_Table_Nme.setValue("BANKING-MODEL-PPG");                                                                                                      //Natural: ASSIGN #TBL-KEY.TBL-TABLE-NME := 'BANKING-MODEL-PPG'
            pnd_Tbl_Key_Tbl_Ppg.setValue(conv2_Tiaa_Related_Ppg_Code);                                                                                                    //Natural: ASSIGN #TBL-KEY.TBL-PPG := TIAA-RELATED-PPG-CODE
            pnd_Tbl_Key_Tbl_Omniplan.setValue(pnd_Wk_Omni_Rec_Pnd_Wk_Omni_Plan_Id);                                                                                       //Natural: ASSIGN #TBL-KEY.TBL-OMNIPLAN := #WK-OMNI-PLAN-ID
            pnd_Misc_Variables_Pnd_Found_Ppg.setValue("N");                                                                                                               //Natural: ASSIGN #FOUND-PPG := 'N'
            pnd_Misc_Variables_Pnd_Isn.setValue(0);                                                                                                                       //Natural: ASSIGN #ISN := 0
            vw_cwf_Support_Tbl.startDatabaseRead                                                                                                                          //Natural: READ ( 1 ) CWF-SUPPORT-TBL BY TBL-PRIME-KEY STARTING FROM #TBL-KEY
            (
            "R2",
            new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Key, WcType.BY) },
            new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") },
            1
            );
            R2:
            while (condition(vw_cwf_Support_Tbl.readNextRow("R2")))
            {
                if (condition(cwf_Support_Tbl_Tbl_Table_Nme.notEquals(pnd_Tbl_Key_Tbl_Table_Nme)))                                                                        //Natural: IF CWF-SUPPORT-TBL.TBL-TABLE-NME NE #TBL-KEY.TBL-TABLE-NME
                {
                    if (true) break R2;                                                                                                                                   //Natural: ESCAPE BOTTOM ( R2. )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Tbl_Key_Tbl_Key_Field.equals(cwf_Support_Tbl_Tbl_Key_Field)))                                                                           //Natural: IF #TBL-KEY.TBL-KEY-FIELD = CWF-SUPPORT-TBL.TBL-KEY-FIELD
                {
                    pnd_Misc_Variables_Pnd_Isn.setValue(vw_cwf_Support_Tbl.getAstISN("R2"));                                                                              //Natural: ASSIGN #ISN := *ISN
                    pnd_Misc_Variables_Pnd_Found_Ppg.setValue("Y");                                                                                                       //Natural: ASSIGN #FOUND-PPG := 'Y'
                    if (true) break R2;                                                                                                                                   //Natural: ESCAPE BOTTOM ( R2. )
                }                                                                                                                                                         //Natural: END-IF
                //*  (1660)
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Misc_Variables_Pnd_Found_Ppg.equals("N")))                                                                                                  //Natural: IF #FOUND-PPG = 'N'
            {
                upd_Cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setValue("A");                                                                                                    //Natural: ASSIGN UPD-CWF-SUPPORT-TBL.TBL-SCRTY-LEVEL-IND := 'A'
                upd_Cwf_Support_Tbl_Tbl_Table_Nme.setValue("BANKING-MODEL-PPG");                                                                                          //Natural: ASSIGN UPD-CWF-SUPPORT-TBL.TBL-TABLE-NME := 'BANKING-MODEL-PPG'
                upd_Cwf_Support_Tbl_Tbl_Key_Field.setValue(pnd_Tbl_Key_Tbl_Key_Field);                                                                                    //Natural: ASSIGN UPD-CWF-SUPPORT-TBL.TBL-KEY-FIELD := #TBL-KEY.TBL-KEY-FIELD
                upd_Cwf_Support_Tbl_Tbl_Data_Field.setValue(pnd_Banking_Model_Omni_Pnd_Banking_Desc.getValue(pnd_Misc_Variables_Pnd_I));                                  //Natural: ASSIGN UPD-CWF-SUPPORT-TBL.TBL-DATA-FIELD := #BANKING-DESC ( #I )
                upd_Cwf_Support_Tbl_Tbl_Actve_Ind.setValue("A");                                                                                                          //Natural: ASSIGN UPD-CWF-SUPPORT-TBL.TBL-ACTVE-IND := 'A'
                upd_Cwf_Support_Tbl_Tbl_Entry_Dte_Tme.setValue(Global.getTIMX());                                                                                         //Natural: ASSIGN UPD-CWF-SUPPORT-TBL.TBL-ENTRY-DTE-TME := *TIMX
                upd_Cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde.setValue(Global.getPROGRAM());                                                                                    //Natural: ASSIGN UPD-CWF-SUPPORT-TBL.TBL-UPDTE-OPRTR-CDE := *PROGRAM
                vw_upd_Cwf_Support_Tbl.insertDBRow();                                                                                                                     //Natural: STORE UPD-CWF-SUPPORT-TBL
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                G1:                                                                                                                                                       //Natural: GET UPD-CWF-SUPPORT-TBL #ISN
                vw_upd_Cwf_Support_Tbl.readByID(pnd_Misc_Variables_Pnd_Isn.getLong(), "G1");
                upd_Cwf_Support_Tbl_Tbl_Data_Field.setValue(pnd_Banking_Model_Omni_Pnd_Banking_Desc.getValue(pnd_Misc_Variables_Pnd_I));                                  //Natural: ASSIGN UPD-CWF-SUPPORT-TBL.TBL-DATA-FIELD := #BANKING-DESC ( #I )
                upd_Cwf_Support_Tbl_Tbl_Updte_Dte_Tme.setValue(Global.getTIMX());                                                                                         //Natural: ASSIGN UPD-CWF-SUPPORT-TBL.TBL-UPDTE-DTE-TME := *TIMX
                upd_Cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde.setValue(Global.getPROGRAM());                                                                                    //Natural: ASSIGN UPD-CWF-SUPPORT-TBL.TBL-UPDTE-OPRTR-CDE := *PROGRAM
                vw_upd_Cwf_Support_Tbl.updateDBRow("G1");                                                                                                                 //Natural: UPDATE ( G1. )
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
            //*  (1440)
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //
}
