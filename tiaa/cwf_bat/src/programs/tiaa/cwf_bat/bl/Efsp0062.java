/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:11:14 PM
**        * FROM NATURAL PROGRAM : Efsp0062
************************************************************
**        * FILE NAME            : Efsp0062.java
**        * CLASS NAME           : Efsp0062
**        * INSTANCE NAME        : Efsp0062
************************************************************
************************************************************************
* PROGRAM  : EFSP0062
* SYSTEM   : CRPCWF
* TITLE    : PRINT IMAGE UPLOAD EXCEPTION REPORT - OUTSTANDING BATCHES
* GENERATED: OCT 4,93 AT 10:00 AM
* FUNCTION : THIS PROGRAM READS THE CWF-UPLOAD-AUDIT FILE AND PRINTS
*            THE BATCHES WHICH ARE OUTSTANDING AND NOT PROCESSED TODAY
* HISTORY
* 04/28/95  BE  REJECT RECORDS WITH MINS UPLOADED COUNT = 0 AND
*               IF THE ELAPSED NO. OF DAYS IS = 17.
* 01/31/96  BE  EXCLUDE OUTSTANDING BATCHES THAT ARE 1 MONTH OLD.
* 04/05/96  BE  REJECT RECORDS WITH THE ELAPSED NO. OF DAYS IS = 17.
*               CHANGED LOGIC FOR 04/28/95 MODIFICATIONS.
* 05/13/96  BE  WRITE 'Nothing to Report' IF THERE ARE NO RECS TO RPT.
* 03/31/97  CS  FORCE LANDSCAPE PRINT MODE IF SUBMITTED ONLINE
* 12/17/97  CS  INCLUDED 'FORMS' WHEN REPORTING 'CIRS'
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsp0062 extends BLNatBase
{
    // Data Areas
    private GdaCwfg000 gdaCwfg000;
    private LdaEfsl0040 ldaEfsl0040;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_upload_Batch;
    private DbsField upload_Batch_Batch_Nbr;
    private DbsField upload_Batch_Batch_Ext_Nbr;
    private DbsField upload_Batch_Upld_Date_Time;
    private DbsField upload_Batch_Mins_Bch_Hdr_Cnt;
    private DbsField upload_Batch_Mins_Uploaded_Cnt;
    private DbsField upload_Batch_Source_Id;
    private DbsField upload_Batch_Capture_Id;
    private DbsGroup upload_Batch_Error_MsgMuGroup;
    private DbsField upload_Batch_Error_Msg;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_1;
    private DbsField cwf_Support_Tbl_Pnd_Regular_Run_Parms;
    private DbsField cwf_Support_Tbl_Pnd_Override_Time_Parms;
    private DbsField cwf_Support_Tbl_Pnd_Override_Flag;
    private DbsField cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_2;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind;
    private DbsField pnd_I;
    private DbsField pnd_Program;
    private DbsField pnd_Old_Batch_Nbr;
    private DbsField pnd_Incomplete_Inactive_Batch;

    private DbsGroup pnd_Line;
    private DbsField pnd_Line_Pnd_First_Date;
    private DbsField pnd_Line_Pnd_Hdr_Min_Count;
    private DbsField pnd_Line_Pnd_Nbr_Of_Days;
    private DbsField pnd_Line_Pnd_Days_In_Process;
    private DbsField pnd_Line_Pnd_Mins_Uploaded;
    private DbsField pnd_Line_Pnd_Mins_Not_Uploaded;
    private DbsField pnd_Input_Date_D;
    private DbsField pnd_Input_Source_Id;
    private DbsField pnd_Input_Data;

    private DbsGroup pnd_Input_Data__R_Field_3;
    private DbsField pnd_Input_Data_Pnd_Input_Time_Start;

    private DbsGroup pnd_Input_Data__R_Field_4;
    private DbsField pnd_Input_Data_Pnd_Input_Time_N;
    private DbsField pnd_Input_Data_Pnd_Input_Time_End;
    private DbsField pnd_Input_Data_Pnd_Input_Date;
    private DbsField pnd_Upld_Date;
    private DbsField pnd_Upld_Date_A;

    private DbsGroup pnd_Upld_Date_A__R_Field_5;
    private DbsField pnd_Upld_Date_A_Pnd_Upld_Date_N;
    private DbsField pnd_Upld_Time;

    private DbsGroup pnd_Upld_Time__R_Field_6;
    private DbsField pnd_Upld_Time_Pnd_Upld_Timen;
    private DbsField pnd_Upld_To_Date;
    private DbsField pnd_Source_Id_Batch_Nbr_Key;

    private DbsGroup pnd_Source_Id_Batch_Nbr_Key__R_Field_7;
    private DbsField pnd_Source_Id_Batch_Nbr_Key_Pnd_Srce_Id;
    private DbsField pnd_Source_Id_Batch_Nbr_Key_Pnd_Batch_Nbr;
    private DbsField pnd_Source_Id_Batch_Nbr_Key_Pnd_Batch_Ext;
    private DbsField pnd_Elapsed_No_Days;
    private DbsField pnd_Date_A_Month_Ago;

    private DbsGroup pnd_Date_A_Month_Ago__R_Field_8;
    private DbsField pnd_Date_A_Month_Ago_Pnd_Date_A_Month_Ago_N;

    private DbsGroup pnd_Date_A_Month_Ago__R_Field_9;
    private DbsField pnd_Date_A_Month_Ago_Pnd_Yyyy;
    private DbsField pnd_Date_A_Month_Ago_Pnd_Mm;
    private DbsField pnd_Date_A_Month_Ago_Pnd_Dd;
    private DbsField pnd_Nothing_To_Rpt;
    private DbsField pnd_Record;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaCwfg000 = GdaCwfg000.getInstance(getCallnatLevel());
        registerRecord(gdaCwfg000);
        if (gdaOnly) return;

        ldaEfsl0040 = new LdaEfsl0040();
        registerRecord(ldaEfsl0040);
        registerRecord(ldaEfsl0040.getVw_cwf_Upload_Audit());

        // Local Variables
        localVariables = new DbsRecord();

        vw_upload_Batch = new DataAccessProgramView(new NameInfo("vw_upload_Batch", "UPLOAD-BATCH"), "CWF_UPLOAD_AUDIT", "CWF_UPLOAD_AUDIT");
        upload_Batch_Batch_Nbr = vw_upload_Batch.getRecord().newFieldInGroup("upload_Batch_Batch_Nbr", "BATCH-NBR", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "BATCH_NBR");
        upload_Batch_Batch_Nbr.setDdmHeader("BATCH/NUMBER");
        upload_Batch_Batch_Ext_Nbr = vw_upload_Batch.getRecord().newFieldInGroup("upload_Batch_Batch_Ext_Nbr", "BATCH-EXT-NBR", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "BATCH_EXT_NBR");
        upload_Batch_Batch_Ext_Nbr.setDdmHeader("BATCH/EXT");
        upload_Batch_Upld_Date_Time = vw_upload_Batch.getRecord().newFieldInGroup("upload_Batch_Upld_Date_Time", "UPLD-DATE-TIME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "UPLD_DATE_TIME");
        upload_Batch_Upld_Date_Time.setDdmHeader("UPLOAD DATE/AND TIME");
        upload_Batch_Mins_Bch_Hdr_Cnt = vw_upload_Batch.getRecord().newFieldInGroup("upload_Batch_Mins_Bch_Hdr_Cnt", "MINS-BCH-HDR-CNT", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "MINS_BCH_HDR_CNT");
        upload_Batch_Mins_Bch_Hdr_Cnt.setDdmHeader("NBR OF/MINS");
        upload_Batch_Mins_Uploaded_Cnt = vw_upload_Batch.getRecord().newFieldInGroup("upload_Batch_Mins_Uploaded_Cnt", "MINS-UPLOADED-CNT", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "MINS_UPLOADED_CNT");
        upload_Batch_Mins_Uploaded_Cnt.setDdmHeader("MINS/UPLOADED");
        upload_Batch_Source_Id = vw_upload_Batch.getRecord().newFieldInGroup("upload_Batch_Source_Id", "SOURCE-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "SOURCE_ID");
        upload_Batch_Capture_Id = vw_upload_Batch.getRecord().newFieldInGroup("upload_Batch_Capture_Id", "CAPTURE-ID", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "CAPTURE_ID");
        upload_Batch_Error_MsgMuGroup = vw_upload_Batch.getRecord().newGroupInGroup("UPLOAD_BATCH_ERROR_MSGMuGroup", "ERROR_MSGMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "CWF_UPLOAD_AUDIT_ERROR_MSG");
        upload_Batch_Error_Msg = upload_Batch_Error_MsgMuGroup.newFieldArrayInGroup("upload_Batch_Error_Msg", "ERROR-MSG", FieldType.STRING, 79, new DbsArrayController(1, 
            100), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ERROR_MSG");
        upload_Batch_Error_Msg.setDdmHeader("ERROR/MESSAGES");
        registerRecord(vw_upload_Batch);

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Support_Tbl__R_Field_1 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_1", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Pnd_Regular_Run_Parms = cwf_Support_Tbl__R_Field_1.newFieldInGroup("cwf_Support_Tbl_Pnd_Regular_Run_Parms", "#REGULAR-RUN-PARMS", 
            FieldType.STRING, 20);
        cwf_Support_Tbl_Pnd_Override_Time_Parms = cwf_Support_Tbl__R_Field_1.newFieldInGroup("cwf_Support_Tbl_Pnd_Override_Time_Parms", "#OVERRIDE-TIME-PARMS", 
            FieldType.STRING, 12);
        cwf_Support_Tbl_Pnd_Override_Flag = cwf_Support_Tbl__R_Field_1.newFieldInGroup("cwf_Support_Tbl_Pnd_Override_Flag", "#OVERRIDE-FLAG", FieldType.STRING, 
            1);
        cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde", "TBL-DLTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_DLTE_OPRTR_CDE");
        registerRecord(vw_cwf_Support_Tbl);

        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_2", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_2.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_2.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_2.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind = pnd_Tbl_Prime_Key__R_Field_2.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", FieldType.STRING, 
            1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Old_Batch_Nbr = localVariables.newFieldInRecord("pnd_Old_Batch_Nbr", "#OLD-BATCH-NBR", FieldType.NUMERIC, 8);
        pnd_Incomplete_Inactive_Batch = localVariables.newFieldInRecord("pnd_Incomplete_Inactive_Batch", "#INCOMPLETE-INACTIVE-BATCH", FieldType.BOOLEAN, 
            1);

        pnd_Line = localVariables.newGroupInRecord("pnd_Line", "#LINE");
        pnd_Line_Pnd_First_Date = pnd_Line.newFieldInGroup("pnd_Line_Pnd_First_Date", "#FIRST-DATE", FieldType.DATE);
        pnd_Line_Pnd_Hdr_Min_Count = pnd_Line.newFieldInGroup("pnd_Line_Pnd_Hdr_Min_Count", "#HDR-MIN-COUNT", FieldType.NUMERIC, 3);
        pnd_Line_Pnd_Nbr_Of_Days = pnd_Line.newFieldInGroup("pnd_Line_Pnd_Nbr_Of_Days", "#NBR-OF-DAYS", FieldType.NUMERIC, 7);
        pnd_Line_Pnd_Days_In_Process = pnd_Line.newFieldInGroup("pnd_Line_Pnd_Days_In_Process", "#DAYS-IN-PROCESS", FieldType.STRING, 17);
        pnd_Line_Pnd_Mins_Uploaded = pnd_Line.newFieldInGroup("pnd_Line_Pnd_Mins_Uploaded", "#MINS-UPLOADED", FieldType.NUMERIC, 3);
        pnd_Line_Pnd_Mins_Not_Uploaded = pnd_Line.newFieldInGroup("pnd_Line_Pnd_Mins_Not_Uploaded", "#MINS-NOT-UPLOADED", FieldType.NUMERIC, 3);
        pnd_Input_Date_D = localVariables.newFieldInRecord("pnd_Input_Date_D", "#INPUT-DATE-D", FieldType.DATE);
        pnd_Input_Source_Id = localVariables.newFieldInRecord("pnd_Input_Source_Id", "#INPUT-SOURCE-ID", FieldType.STRING, 6);
        pnd_Input_Data = localVariables.newFieldInRecord("pnd_Input_Data", "#INPUT-DATA", FieldType.STRING, 60);

        pnd_Input_Data__R_Field_3 = localVariables.newGroupInRecord("pnd_Input_Data__R_Field_3", "REDEFINE", pnd_Input_Data);
        pnd_Input_Data_Pnd_Input_Time_Start = pnd_Input_Data__R_Field_3.newFieldInGroup("pnd_Input_Data_Pnd_Input_Time_Start", "#INPUT-TIME-START", FieldType.STRING, 
            6);

        pnd_Input_Data__R_Field_4 = pnd_Input_Data__R_Field_3.newGroupInGroup("pnd_Input_Data__R_Field_4", "REDEFINE", pnd_Input_Data_Pnd_Input_Time_Start);
        pnd_Input_Data_Pnd_Input_Time_N = pnd_Input_Data__R_Field_4.newFieldInGroup("pnd_Input_Data_Pnd_Input_Time_N", "#INPUT-TIME-N", FieldType.NUMERIC, 
            6);
        pnd_Input_Data_Pnd_Input_Time_End = pnd_Input_Data__R_Field_3.newFieldInGroup("pnd_Input_Data_Pnd_Input_Time_End", "#INPUT-TIME-END", FieldType.STRING, 
            6);
        pnd_Input_Data_Pnd_Input_Date = pnd_Input_Data__R_Field_3.newFieldInGroup("pnd_Input_Data_Pnd_Input_Date", "#INPUT-DATE", FieldType.STRING, 8);
        pnd_Upld_Date = localVariables.newFieldInRecord("pnd_Upld_Date", "#UPLD-DATE", FieldType.DATE);
        pnd_Upld_Date_A = localVariables.newFieldInRecord("pnd_Upld_Date_A", "#UPLD-DATE-A", FieldType.STRING, 8);

        pnd_Upld_Date_A__R_Field_5 = localVariables.newGroupInRecord("pnd_Upld_Date_A__R_Field_5", "REDEFINE", pnd_Upld_Date_A);
        pnd_Upld_Date_A_Pnd_Upld_Date_N = pnd_Upld_Date_A__R_Field_5.newFieldInGroup("pnd_Upld_Date_A_Pnd_Upld_Date_N", "#UPLD-DATE-N", FieldType.NUMERIC, 
            8);
        pnd_Upld_Time = localVariables.newFieldInRecord("pnd_Upld_Time", "#UPLD-TIME", FieldType.STRING, 6);

        pnd_Upld_Time__R_Field_6 = localVariables.newGroupInRecord("pnd_Upld_Time__R_Field_6", "REDEFINE", pnd_Upld_Time);
        pnd_Upld_Time_Pnd_Upld_Timen = pnd_Upld_Time__R_Field_6.newFieldInGroup("pnd_Upld_Time_Pnd_Upld_Timen", "#UPLD-TIMEN", FieldType.NUMERIC, 6);
        pnd_Upld_To_Date = localVariables.newFieldInRecord("pnd_Upld_To_Date", "#UPLD-TO-DATE", FieldType.NUMERIC, 7);
        pnd_Source_Id_Batch_Nbr_Key = localVariables.newFieldInRecord("pnd_Source_Id_Batch_Nbr_Key", "#SOURCE-ID-BATCH-NBR-KEY", FieldType.STRING, 16);

        pnd_Source_Id_Batch_Nbr_Key__R_Field_7 = localVariables.newGroupInRecord("pnd_Source_Id_Batch_Nbr_Key__R_Field_7", "REDEFINE", pnd_Source_Id_Batch_Nbr_Key);
        pnd_Source_Id_Batch_Nbr_Key_Pnd_Srce_Id = pnd_Source_Id_Batch_Nbr_Key__R_Field_7.newFieldInGroup("pnd_Source_Id_Batch_Nbr_Key_Pnd_Srce_Id", "#SRCE-ID", 
            FieldType.STRING, 6);
        pnd_Source_Id_Batch_Nbr_Key_Pnd_Batch_Nbr = pnd_Source_Id_Batch_Nbr_Key__R_Field_7.newFieldInGroup("pnd_Source_Id_Batch_Nbr_Key_Pnd_Batch_Nbr", 
            "#BATCH-NBR", FieldType.NUMERIC, 8);
        pnd_Source_Id_Batch_Nbr_Key_Pnd_Batch_Ext = pnd_Source_Id_Batch_Nbr_Key__R_Field_7.newFieldInGroup("pnd_Source_Id_Batch_Nbr_Key_Pnd_Batch_Ext", 
            "#BATCH-EXT", FieldType.NUMERIC, 2);
        pnd_Elapsed_No_Days = localVariables.newFieldInRecord("pnd_Elapsed_No_Days", "#ELAPSED-NO-DAYS", FieldType.NUMERIC, 6);
        pnd_Date_A_Month_Ago = localVariables.newFieldInRecord("pnd_Date_A_Month_Ago", "#DATE-A-MONTH-AGO", FieldType.STRING, 8);

        pnd_Date_A_Month_Ago__R_Field_8 = localVariables.newGroupInRecord("pnd_Date_A_Month_Ago__R_Field_8", "REDEFINE", pnd_Date_A_Month_Ago);
        pnd_Date_A_Month_Ago_Pnd_Date_A_Month_Ago_N = pnd_Date_A_Month_Ago__R_Field_8.newFieldInGroup("pnd_Date_A_Month_Ago_Pnd_Date_A_Month_Ago_N", "#DATE-A-MONTH-AGO-N", 
            FieldType.NUMERIC, 8);

        pnd_Date_A_Month_Ago__R_Field_9 = localVariables.newGroupInRecord("pnd_Date_A_Month_Ago__R_Field_9", "REDEFINE", pnd_Date_A_Month_Ago);
        pnd_Date_A_Month_Ago_Pnd_Yyyy = pnd_Date_A_Month_Ago__R_Field_9.newFieldInGroup("pnd_Date_A_Month_Ago_Pnd_Yyyy", "#YYYY", FieldType.NUMERIC, 4);
        pnd_Date_A_Month_Ago_Pnd_Mm = pnd_Date_A_Month_Ago__R_Field_9.newFieldInGroup("pnd_Date_A_Month_Ago_Pnd_Mm", "#MM", FieldType.NUMERIC, 2);
        pnd_Date_A_Month_Ago_Pnd_Dd = pnd_Date_A_Month_Ago__R_Field_9.newFieldInGroup("pnd_Date_A_Month_Ago_Pnd_Dd", "#DD", FieldType.NUMERIC, 2);
        pnd_Nothing_To_Rpt = localVariables.newFieldInRecord("pnd_Nothing_To_Rpt", "#NOTHING-TO-RPT", FieldType.BOOLEAN, 1);
        pnd_Record = localVariables.newFieldInRecord("pnd_Record", "#RECORD", FieldType.STRING, 54);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_upload_Batch.reset();
        vw_cwf_Support_Tbl.reset();

        ldaEfsl0040.initializeValues();

        localVariables.reset();
        pnd_Nothing_To_Rpt.setInitialValue(true);
        pnd_Record.setInitialValue("�E�&l1o2a5.6c72p3e66F�&a2L�(9905X�&l0L");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Efsp0062() throws Exception
    {
        super("Efsp0062");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Efsp0062|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        while(true)
        {
            try
            {
                getReports().definePrinter(2, "AUDIT");                                                                                                                   //Natural: DEFINE PRINTER ( AUDIT = 1 )
                pnd_Program.setValue(Global.getPROGRAM());                                                                                                                //Natural: FORMAT ( AUDIT ) LS = 133 PS = 60 ZP = OFF;//Natural: ASSIGN #PROGRAM = *PROGRAM
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Source_Id,pnd_Input_Data);                                                                   //Natural: INPUT #INPUT-SOURCE-ID #INPUT-DATA
                pnd_Input_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Data_Pnd_Input_Date);                                                            //Natural: MOVE EDITED #INPUT-DATE TO #INPUT-DATE-D ( EM = YYYYMMDD )
                //*  COMPUTE FOR DATE 1 MONTH AGO.
                pnd_Date_A_Month_Ago.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                     //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO #DATE-A-MONTH-AGO
                if (condition(pnd_Date_A_Month_Ago_Pnd_Mm.greater(1)))                                                                                                    //Natural: IF #MM GT 1
                {
                    pnd_Date_A_Month_Ago_Pnd_Mm.nsubtract(1);                                                                                                             //Natural: SUBTRACT 1 FROM #MM
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Date_A_Month_Ago_Pnd_Mm.setValue(12);                                                                                                             //Natural: ASSIGN #MM := 12
                    pnd_Date_A_Month_Ago_Pnd_Yyyy.nsubtract(1);                                                                                                           //Natural: SUBTRACT 1 FROM #YYYY
                }                                                                                                                                                         //Natural: END-IF
                //*  READ THE AUDIT RECORDS BY BATCH KEY
                pnd_Source_Id_Batch_Nbr_Key_Pnd_Srce_Id.setValue(pnd_Input_Source_Id);                                                                                    //Natural: ASSIGN #SOURCE-ID-BATCH-NBR-KEY.#SRCE-ID := #INPUT-SOURCE-ID
                ldaEfsl0040.getVw_cwf_Upload_Audit().startDatabaseRead                                                                                                    //Natural: READ CWF-UPLOAD-AUDIT BY SOURCE-ID-BATCH-NBR-KEY STARTING FROM #SOURCE-ID-BATCH-NBR-KEY
                (
                "RD1",
                new Wc[] { new Wc("SOURCE_ID_BATCH_NBR_KEY", ">=", pnd_Source_Id_Batch_Nbr_Key, WcType.BY) },
                new Oc[] { new Oc("SOURCE_ID_BATCH_NBR_KEY", "ASC") }
                );
                RD1:
                while (condition(ldaEfsl0040.getVw_cwf_Upload_Audit().readNextRow("RD1")))
                {
                    //* ---  COS 12/24/97
                    if (condition(pnd_Source_Id_Batch_Nbr_Key_Pnd_Srce_Id.equals("CIRS")))                                                                                //Natural: IF #SRCE-ID EQ 'CIRS'
                    {
                        if (condition(ldaEfsl0040.getCwf_Upload_Audit_Source_Id().greater("FORMS")))                                                                      //Natural: IF CWF-UPLOAD-AUDIT.SOURCE-ID GT 'FORMS'
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(! (ldaEfsl0040.getCwf_Upload_Audit_Source_Id().equals("CIRS") || ldaEfsl0040.getCwf_Upload_Audit_Source_Id().equals("FORMS"))))     //Natural: IF NOT ( CWF-UPLOAD-AUDIT.SOURCE-ID EQ 'CIRS' OR EQ 'FORMS' )
                        {
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(ldaEfsl0040.getCwf_Upload_Audit_Source_Id().notEquals(pnd_Source_Id_Batch_Nbr_Key_Pnd_Srce_Id)))                                    //Natural: IF CWF-UPLOAD-AUDIT.SOURCE-ID NE #SRCE-ID
                        {
                            if (true) break RD1;                                                                                                                          //Natural: ESCAPE BOTTOM ( RD1. )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*  IGNORE
                    if (condition(ldaEfsl0040.getCwf_Upload_Audit_Mins_Uploaded_Cnt().equals(getZero()) && ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(1).notEquals(" "))) //Natural: REJECT IF CWF-UPLOAD-AUDIT.MINS-UPLOADED-CNT = 0 AND CWF-UPLOAD-AUDIT.ERROR-MSG ( 1 ) NE ' '
                    {
                        continue;
                    }
                    //*  DETERMINE THE EFFECTIVE DAY OF WORK
                    pnd_Upld_Date.setValue(ldaEfsl0040.getCwf_Upload_Audit_Upld_Date_Time());                                                                             //Natural: ASSIGN #UPLD-DATE = CWF-UPLOAD-AUDIT.UPLD-DATE-TIME
                    pnd_Upld_Date_A.setValueEdited(pnd_Upld_Date,new ReportEditMask("YYYYMMDD"));                                                                         //Natural: MOVE EDITED #UPLD-DATE ( EM = YYYYMMDD ) TO #UPLD-DATE-A
                    pnd_Upld_Time.setValueEdited(ldaEfsl0040.getCwf_Upload_Audit_Upld_Date_Time(),new ReportEditMask("HHIISS"));                                          //Natural: MOVE EDITED CWF-UPLOAD-AUDIT.UPLD-DATE-TIME ( EM = HHIISS ) TO #UPLD-TIME
                    //*  BEFORE INPUT TIME, IT IS PREVIOUS
                    if (condition(pnd_Upld_Time_Pnd_Upld_Timen.less(pnd_Input_Data_Pnd_Input_Time_N)))                                                                    //Natural: IF #UPLD-TIMEN < #INPUT-TIME-N
                    {
                        //*  DAY's work
                        pnd_Upld_Date.nsubtract(1);                                                                                                                       //Natural: SUBTRACT 1 FROM #UPLD-DATE
                    }                                                                                                                                                     //Natural: END-IF
                    //*  REJECT ANY UPLOADS PAST INPUT PARAMETER DATE
                    //*   OR OUTSTANDING BATCHES THAT ARE 1 MONTH OLD - (BE 01/26/96)
                    if (condition(pnd_Upld_Date.greater(pnd_Input_Date_D) || pnd_Upld_Date_A_Pnd_Upld_Date_N.less(pnd_Date_A_Month_Ago_Pnd_Date_A_Month_Ago_N)))          //Natural: IF #UPLD-DATE > #INPUT-DATE-D OR #UPLD-DATE-N < #DATE-A-MONTH-AGO-N
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    //*  AT BREAK OF CWF-UPLOAD-AUDIT.SOURCE-ID   /*---  COS 12/24/97
                    //*    RESET *PAGE-NUMBER (AUDIT)
                    //*    NEWPAGE (AUDIT)
                    //*  END-BREAK
                    //*  DO NOT PRINT THE RECORD IF MINS UPLOADED COUNT = 0 AND
                    //*  IF THE ELAPSED NO. OF DAYS IS = 17. (1 '*' = 1 DAY)
                    pnd_Elapsed_No_Days.compute(new ComputeParameters(false, pnd_Elapsed_No_Days), pnd_Input_Date_D.subtract(pnd_Upld_Date));                             //Natural: COMPUTE #ELAPSED-NO-DAYS = #INPUT-DATE-D - #UPLD-DATE
                    //*  IF CWF-UPLOAD-AUDIT.MINS-UPLOADED-CNT = 0 AND /* COMMENTED BY ELLO
                    //*                                                       4/5/96.
                    if (condition(pnd_Elapsed_No_Days.greater(17)))                                                                                                       //Natural: IF #ELAPSED-NO-DAYS GT 17
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    //*  AT THE CHANGE OF BATCH NBR,
                    //*    DETERMINE IF THIS IS AN INCOMPLETE BATCH WHICH HAS NOT BEEN
                    //*    PROCESSED AS OF THE INPUT PARAMETER DATE
                    if (condition(ldaEfsl0040.getCwf_Upload_Audit_Batch_Nbr().notEquals(pnd_Old_Batch_Nbr)))                                                              //Natural: IF CWF-UPLOAD-AUDIT.BATCH-NBR NE #OLD-BATCH-NBR
                    {
                        if (condition(pnd_Incomplete_Inactive_Batch.getBoolean()))                                                                                        //Natural: IF #INCOMPLETE-INACTIVE-BATCH
                        {
                            getReports().skip(0, 1);                                                                                                                      //Natural: SKIP ( AUDIT ) 1 LINES
                        }                                                                                                                                                 //Natural: END-IF
                        //*  RESET BATCH AS WELL AS LINE LEVEL FIELDS
                        pnd_Line.reset();                                                                                                                                 //Natural: RESET #LINE
                        pnd_Old_Batch_Nbr.setValue(ldaEfsl0040.getCwf_Upload_Audit_Batch_Nbr());                                                                          //Natural: ASSIGN #OLD-BATCH-NBR = CWF-UPLOAD-AUDIT.BATCH-NBR
                        pnd_Line_Pnd_Hdr_Min_Count.setValue(ldaEfsl0040.getCwf_Upload_Audit_Mins_Bch_Hdr_Cnt());                                                          //Natural: ASSIGN #LINE.#HDR-MIN-COUNT = CWF-UPLOAD-AUDIT.MINS-BCH-HDR-CNT
                        pnd_Incomplete_Inactive_Batch.reset();                                                                                                            //Natural: RESET #INCOMPLETE-INACTIVE-BATCH
                                                                                                                                                                          //Natural: PERFORM DETERMINE-IF-INCOMPLETE-AND-INACTIVE-BATCH
                        sub_Determine_If_Incomplete_And_Inactive_Batch();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        if (condition(pnd_Incomplete_Inactive_Batch.getBoolean()))                                                                                        //Natural: IF #INCOMPLETE-INACTIVE-BATCH
                        {
                            pnd_Line_Pnd_Nbr_Of_Days.compute(new ComputeParameters(false, pnd_Line_Pnd_Nbr_Of_Days), pnd_Input_Date_D.subtract(pnd_Line_Pnd_First_Date)); //Natural: COMPUTE #NBR-OF-DAYS = #INPUT-DATE-D - #LINE.#FIRST-DATE
                            FOR01:                                                                                                                                        //Natural: FOR #I = 1 TO #NBR-OF-DAYS
                            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Line_Pnd_Nbr_Of_Days)); pnd_I.nadd(1))
                            {
                                if (condition(pnd_I.greater(17)))                                                                                                         //Natural: IF #I GT 17
                                {
                                    if (condition(true)) break;                                                                                                           //Natural: ESCAPE BOTTOM
                                }                                                                                                                                         //Natural: END-IF
                                pnd_Line_Pnd_Days_In_Process.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Line_Pnd_Days_In_Process, "*"));                //Natural: COMPRESS #DAYS-IN-PROCESS '*' INTO #DAYS-IN-PROCESS LEAVING NO
                            }                                                                                                                                             //Natural: END-FOR
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*  ONLY BATCHES WHICH ARE INCOMPLETE AND INACTIVE ARE REPORTED
                    if (condition(! (pnd_Incomplete_Inactive_Batch.getBoolean())))                                                                                        //Natural: IF NOT #INCOMPLETE-INACTIVE-BATCH
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    //*  ADD MINS UPLOADED ON A CUMULATIVE BASIS
                    pnd_Line_Pnd_Mins_Uploaded.nadd(ldaEfsl0040.getCwf_Upload_Audit_Mins_Uploaded_Cnt());                                                                 //Natural: ADD CWF-UPLOAD-AUDIT.MINS-UPLOADED-CNT TO #LINE.#MINS-UPLOADED
                    pnd_Line_Pnd_Mins_Not_Uploaded.compute(new ComputeParameters(false, pnd_Line_Pnd_Mins_Not_Uploaded), ldaEfsl0040.getCwf_Upload_Audit_Mins_Bch_Hdr_Cnt().subtract(pnd_Line_Pnd_Mins_Uploaded)); //Natural: COMPUTE #LINE.#MINS-NOT-UPLOADED = CWF-UPLOAD-AUDIT.MINS-BCH-HDR-CNT - #LINE.#MINS-UPLOADED
                    getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Efsf0061.class));                                                                  //Natural: WRITE ( AUDIT ) NOTITLE USING FORM 'EFSF0061'
                    pnd_Nothing_To_Rpt.setValue(false);                                                                                                                   //Natural: ASSIGN #NOTHING-TO-RPT := FALSE
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                if (condition(pnd_Nothing_To_Rpt.getBoolean()))                                                                                                           //Natural: IF #NOTHING-TO-RPT
                {
                    getReports().write(2, ReportOption.NOTITLE,"Nothing to report.");                                                                                     //Natural: WRITE ( AUDIT ) NOTITLE 'Nothing to report.'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                getReports().newPage(new ReportSpecification(0));                                                                                                         //Natural: NEWPAGE ( AUDIT )
                if (condition(Global.isEscape())){return;}
                //* ************************************************************
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( AUDIT )
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Determine_If_Incomplete_And_Inactive_Batch() throws Exception                                                                                        //Natural: DETERMINE-IF-INCOMPLETE-AND-INACTIVE-BATCH
    {
        if (BLNatReinput.isReinput()) return;

        //* * ----------------------------------------------------------
        pnd_Upld_To_Date.reset();                                                                                                                                         //Natural: RESET #UPLD-TO-DATE
        if (condition(ldaEfsl0040.getCwf_Upload_Audit_Mins_Bch_Hdr_Cnt().equals(ldaEfsl0040.getCwf_Upload_Audit_Mins_Uploaded_Cnt())))                                    //Natural: IF CWF-UPLOAD-AUDIT.MINS-BCH-HDR-CNT = CWF-UPLOAD-AUDIT.MINS-UPLOADED-CNT
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Source_Id_Batch_Nbr_Key_Pnd_Srce_Id.setValue(ldaEfsl0040.getCwf_Upload_Audit_Source_Id());                                                                    //Natural: ASSIGN #SOURCE-ID-BATCH-NBR-KEY.#SRCE-ID = CWF-UPLOAD-AUDIT.SOURCE-ID
        pnd_Source_Id_Batch_Nbr_Key_Pnd_Batch_Nbr.setValue(ldaEfsl0040.getCwf_Upload_Audit_Batch_Nbr());                                                                  //Natural: ASSIGN #SOURCE-ID-BATCH-NBR-KEY.#BATCH-NBR = CWF-UPLOAD-AUDIT.BATCH-NBR
        vw_upload_Batch.startDatabaseRead                                                                                                                                 //Natural: READ UPLOAD-BATCH BY SOURCE-ID-BATCH-NBR-KEY STARTING FROM #SOURCE-ID-BATCH-NBR-KEY
        (
        "READ_BATCH",
        new Wc[] { new Wc("SOURCE_ID_BATCH_NBR_KEY", ">=", pnd_Source_Id_Batch_Nbr_Key, WcType.BY) },
        new Oc[] { new Oc("SOURCE_ID_BATCH_NBR_KEY", "ASC") }
        );
        READ_BATCH:
        while (condition(vw_upload_Batch.readNextRow("READ_BATCH")))
        {
            if (condition(upload_Batch_Source_Id.notEquals(pnd_Source_Id_Batch_Nbr_Key_Pnd_Srce_Id) || upload_Batch_Batch_Nbr.notEquals(pnd_Source_Id_Batch_Nbr_Key_Pnd_Batch_Nbr))) //Natural: IF UPLOAD-BATCH.SOURCE-ID NE #SOURCE-ID-BATCH-NBR-KEY.#SRCE-ID OR UPLOAD-BATCH.BATCH-NBR NE #SOURCE-ID-BATCH-NBR-KEY.#BATCH-NBR
            {
                if (true) break READ_BATCH;                                                                                                                               //Natural: ESCAPE BOTTOM ( READ-BATCH. )
            }                                                                                                                                                             //Natural: END-IF
            //*  REJECT IF UPLOAD-BATCH.EXCEPTION-MSG NE ' ' /* IGNORE ERROR LOGS
            //*  IGNORE
            if (condition(upload_Batch_Mins_Uploaded_Cnt.equals(getZero()) && upload_Batch_Error_Msg.getValue(1).notEquals(" ")))                                         //Natural: REJECT IF UPLOAD-BATCH.MINS-UPLOADED-CNT = 0 AND UPLOAD-BATCH.ERROR-MSG ( 1 ) NE ' '
            {
                continue;
            }
            pnd_Upld_Date.setValue(upload_Batch_Upld_Date_Time);                                                                                                          //Natural: ASSIGN #UPLD-DATE = UPLOAD-BATCH.UPLD-DATE-TIME
            pnd_Upld_Time.setValueEdited(upload_Batch_Upld_Date_Time,new ReportEditMask("HHIISS"));                                                                       //Natural: MOVE EDITED UPLOAD-BATCH.UPLD-DATE-TIME ( EM = HHIISS ) TO #UPLD-TIME
            //*  BEFORE 6 AM, IT IS PREV DAY's work
            if (condition(pnd_Upld_Time_Pnd_Upld_Timen.less(pnd_Input_Data_Pnd_Input_Time_N)))                                                                            //Natural: IF #UPLD-TIMEN < #INPUT-TIME-N
            {
                pnd_Upld_Date.nsubtract(1);                                                                                                                               //Natural: SUBTRACT 1 FROM #UPLD-DATE
            }                                                                                                                                                             //Natural: END-IF
            //*  IF ANY ACTIVITY ON INPUT DATE, IT IS ACTIVE
            if (condition(pnd_Upld_Date.equals(pnd_Input_Date_D)))                                                                                                        //Natural: IF #UPLD-DATE = #INPUT-DATE-D
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*  REJECT RECORDS FOR FUTURE OR RECORDS THAT ARE 1 MONTH AGO.
            if (condition(pnd_Upld_Date.greater(pnd_Input_Date_D) || pnd_Upld_Date_A_Pnd_Upld_Date_N.less(pnd_Date_A_Month_Ago_Pnd_Date_A_Month_Ago_N)))                  //Natural: IF #UPLD-DATE > #INPUT-DATE-D OR #UPLD-DATE-N < #DATE-A-MONTH-AGO-N
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Line_Pnd_First_Date.equals(getZero())))                                                                                                     //Natural: IF #LINE.#FIRST-DATE = 0
            {
                pnd_Line_Pnd_First_Date.setValue(pnd_Upld_Date);                                                                                                          //Natural: ASSIGN #LINE.#FIRST-DATE = #UPLD-DATE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Upld_To_Date.nadd(upload_Batch_Mins_Uploaded_Cnt);                                                                                                        //Natural: ADD UPLOAD-BATCH.MINS-UPLOADED-CNT TO #UPLD-TO-DATE
            pnd_Line_Pnd_Hdr_Min_Count.setValue(upload_Batch_Mins_Bch_Hdr_Cnt);                                                                                           //Natural: ASSIGN #LINE.#HDR-MIN-COUNT = UPLOAD-BATCH.MINS-BCH-HDR-CNT
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Line_Pnd_Hdr_Min_Count.notEquals(pnd_Upld_To_Date)))                                                                                            //Natural: IF #LINE.#HDR-MIN-COUNT NE #UPLD-TO-DATE
        {
            pnd_Incomplete_Inactive_Batch.setValue(true);                                                                                                                 //Natural: ASSIGN #INCOMPLETE-INACTIVE-BATCH = TRUE
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //*  BEING SUBMITTED VIA CTP8000
                    //*   SET LANDSCAPE
                    if (condition(! (DbsUtil.maskMatches(Global.getINIT_PROGRAM(),".....'CWD'"))))                                                                        //Natural: IF *INIT-PROGRAM NE MASK ( .....'CWD' )
                    {
                        getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,pnd_Record);                                                                        //Natural: WRITE ( AUDIT ) NOTITLE NOHDR #RECORD
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(2, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Efsf0068.class));                                              //Natural: WRITE ( AUDIT ) NOTITLE NOHDR USING FORM 'EFSF0068'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(2, "LS=133 PS=60 ZP=OFF");
    }
}
