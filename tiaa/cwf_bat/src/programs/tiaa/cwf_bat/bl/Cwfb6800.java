/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:43:37 PM
**        * FROM NATURAL PROGRAM : Cwfb6800
************************************************************
**        * FILE NAME            : Cwfb6800.java
**        * CLASS NAME           : Cwfb6800
**        * INSTANCE NAME        : Cwfb6800
************************************************************
************************************************************************
* PROGRAM  : CWFB6800
* SYSTEM   : CWF MUTUAL FUNDS
* TITLE    : CWF PACKAGE AND PRINT SHARE SERVICE REQUEST
* FUNCTION : THIS PACKAGE SSR READS THRU THE 'EXTERNAL PEND' WORK LIST
*          : OF THE 'MFTA' (TRANSFER AGENT) UNIT.
*          :
*          : IT WILL UPDATE THE STATUS OF REQUESTS THAT WERE NOT
*          : INCLUDED IN THE LAST RUN OF THIS JOB FROM 'PEND/SEND TO TA'
*          : (4800) TO 'PEND/WAIT FOR TA' (4820).
*          :
*          : IT WILL PRINT A TRANSMITTAL LIST, CONTACT SHEETS, AND
*          : IMAGES OF REQUESTS.   .
*          |
* 02/07/97 | GH  CREATED NEW.
* 07/18/97 | GH  RE-CALCULATE THE PAGES FOR EACH IMAGE SSR
* 11/05/97 | GH  IF 'ReSend' DO NOT PRINT IMAGE SSRS.
*          |     IF 'ReSend' DO NOT READ CWF-TEXT FILE.
* 01/12/98 | GH  MODIFY THE LOGIC FOR INOTMFA:
*          |     FOR PSS, THE CCLL (CALL) COMES BEFORE INOTMFA;
*          |     FOR INSURANCE (ICSS), THE INOTMFA COMES BEFORE CCLL.
* 05/13/98 | GH  RESET #TEXT-SQNCE-NBR-MAX TO CORRECT PAGE COUNT.
* 06/05/98 | GH  REVISE THE CALCULATION OF THE TOTAL PAGES OF IMAGE.
* 06/08/98 | GH  EITHER ADMIN STATUS OR STATUS IS 4820, CHANGE REMARK
*          |     TO BE 'Resend'.
* 02/01/99 | GH  ISSUE THE TERMINATE 20 WHEN IMAGE PRINT SERVER ERROR
*          |     OCCURRED.
* 02/02/99 | GH  MOVE THE TABLE READ OUT FROM THE LOOP.
* 05/06/99 | GH  INCREASE COUNTERS FROM N3 TO N5, #TOTAL-SSR N4 TO N6.
* 10/19/99 | GH  IF CNNCTD-RQST-IND-1 = 'Y' DON't print Images/CS.
* 10/22/99 | GH  DON't print Images if number of pages > 10
* 01/31/01 | GH  WRITE PIN-NBR,WORK-PRCSS-ID AND #DATE-FOR-RERUN
*          |     INTO WORK FILE(10 GDGS) FOR ATI.
* 01/17/05 | GH  ELIMINATE THE SNA PROTOCOL:
*          |     1. REMOVES THE PRINT-IMAGE (KEEP THE ROUTINE NAME).
*          |     2. WRITE THE LIST OF IMAGE ADDRESS TO WORKFILE 2
*          |     3. THE LIST WILL BE TRANSFERRED TO LAN ENV. VIA
*          |        CONNECT:DIRECT (NDM).
* 07/23/09 | GH  DO NOT SEND 1. OLD REQUEST 2. ADDR CHG  TO THE REPORT
* 09/23/14 | VR  PROD FIX - INC2541855 BATCH JOB P5020CWD FAILURE
*                CHANGE TO FIX NATURAL ERROR 1305 AT LINE 5370
* 03/25/16 | VR  REPORT CW5020D1 ENHANCEMENT
*                TERMINATED CONTRACTS ELIMINATED FROM THE REPORT
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb6800 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private PdaMdma111 pdaMdma111;
    private PdaCwfl5312 pdaCwfl5312;
    private LdaCwfl6800 ldaCwfl6800;
    private PdaErla1000 pdaErla1000;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Doc;
    private DbsField cwf_Doc_Cabinet_Id;
    private DbsField cwf_Doc_Tiaa_Rcvd_Dte;
    private DbsField cwf_Doc_Case_Workprcss_Multi_Subrqst;

    private DbsGroup cwf_Doc_Document_Type;
    private DbsField cwf_Doc_Document_Category;
    private DbsField cwf_Doc_Document_Sub_Category;
    private DbsField cwf_Doc_Document_Detail;
    private DbsField cwf_Doc_Document_Direction_Ind;
    private DbsField cwf_Doc_Secured_Document_Ind;
    private DbsField cwf_Doc_Document_Subtype;
    private DbsField cwf_Doc_Document_Retention_Ind;
    private DbsField cwf_Doc_Entry_System_Or_Unit;
    private DbsField cwf_Doc_Entry_Dte_Tme;
    private DbsField cwf_Doc_Text_Pointer;
    private DbsField cwf_Doc_Image_Min;
    private DbsField cwf_Doc_Image_Source_Id;
    private DbsField cwf_Doc_Dgtze_Doc_End_Page;

    private DataAccessProgramView vw_cwf_Image_Xref;
    private DbsField cwf_Image_Xref_Mail_Item_Nbr;
    private DbsField cwf_Image_Xref_Image_Document_Address_Cde;

    private DbsGroup cwf_Image_Xref__R_Field_1;
    private DbsField cwf_Image_Xref_Image_Document_Address_1;
    private DbsField cwf_Image_Xref_Image_Number_Page;
    private DbsField cwf_Image_Xref_Ph_Unique_Id_Nbr;
    private DbsField cwf_Image_Xref_Image_Quality_Ind;
    private DbsField cwf_Image_Xref_Upld_Date_Time;
    private DbsField cwf_Image_Xref_Number_Of_Pages;
    private DbsField cwf_Image_Xref_Source_Id;

    private DataAccessProgramView vw_cwf_Text;
    private DbsField cwf_Text_Cabinet_Id;
    private DbsField cwf_Text_Text_Pointer;
    private DbsField cwf_Text_Text_Sqnce_Nbr;
    private DbsField cwf_Text_Last_Text_Lines_Group_Ind;
    private DbsField cwf_Text_Count_Casttext_Document_Line_Group;

    private DbsGroup cwf_Text_Text_Document_Line_Group;
    private DbsField cwf_Text_Text_Document_Line;

    private DbsGroup cwf_Text__R_Field_2;
    private DbsField cwf_Text_Text_04;

    private DbsGroup cwf_Text__R_Field_3;
    private DbsField cwf_Text_Text_1;
    private DbsField cwf_Text_Text_2_And_3;
    private DbsField cwf_Text_Text_4_Thru_77;
    private DbsField cwf_Text_Text_78;

    private DataAccessProgramView vw_cwf_Wpid;
    private DbsField cwf_Wpid_Work_Prcss_Id;
    private DbsField cwf_Wpid_Work_Prcss_Short_Nme;

    private DbsGroup msg_Info;
    private DbsField msg_Info_Pnd_Pnd_Msg;
    private DbsField msg_Info_Pnd_Pnd_Msg_Nr;
    private DbsField msg_Info_Pnd_Pnd_Msg_Data;
    private DbsField msg_Info_Pnd_Pnd_Return_Code;
    private DbsField msg_Info_Pnd_Pnd_Error_Field;
    private DbsField msg_Info_Pnd_Pnd_Error_Field_Index1;
    private DbsField msg_Info_Pnd_Pnd_Error_Field_Index2;
    private DbsField msg_Info_Pnd_Pnd_Error_Field_Index3;

    private DbsGroup pnd_Vars;
    private DbsField pnd_Vars_Pnd_Account_Nbr;
    private DbsField pnd_Vars_Pnd_Account_Nbr_N;
    private DbsField pnd_Vars_Pnd_Connect_Ind;
    private DbsField pnd_Vars_Pnd_Counter;
    private DbsField pnd_Vars_Pnd_Counter_C;
    private DbsField pnd_Vars_Pnd_Counter_I;
    private DbsField pnd_Vars_Pnd_Counter_Send;
    private DbsField pnd_Vars_Pnd_Counter_N;
    private DbsField pnd_Vars_Pnd_Counter_C_N;
    private DbsField pnd_Vars_Pnd_Counter_I_N;
    private DbsField pnd_Vars_Pnd_Counter_Send_N;
    private DbsField pnd_Vars_Pnd_Crrnt_Due_Dte;

    private DbsGroup pnd_Vars__R_Field_4;
    private DbsField pnd_Vars_Pnd_Yyyy;
    private DbsField pnd_Vars_Pnd_Mm;
    private DbsField pnd_Vars_Pnd_Dd;
    private DbsField pnd_Vars_Pnd_Crrnt_Due_Dte_O;
    private DbsField pnd_Vars_Pnd_Cs_Found;
    private DbsField pnd_Vars_Pnd_Date_For_Rerun_N;

    private DbsGroup pnd_Vars__R_Field_5;
    private DbsField pnd_Vars_Pnd_Date_For_Rerun;
    private DbsField pnd_Vars_Pnd_Document_Key;

    private DbsGroup pnd_Vars__R_Field_6;
    private DbsField pnd_Vars_Pnd_Doc_Cabinet_Id;

    private DbsGroup pnd_Vars__R_Field_7;
    private DbsField pnd_Vars_Pnd_Doc_Cabinet_Type;
    private DbsField pnd_Vars_Pnd_Doc_Cabinet_Name;

    private DbsGroup pnd_Vars__R_Field_8;
    private DbsField pnd_Vars_Pnd_Doc_Cabinet_Name_A12;
    private DbsField pnd_Vars_Pnd_Doc_Tiaa_Rcvd_Dte;
    private DbsField pnd_Vars_Pnd_Case_Workprcss_Multi_Subrqst;

    private DbsGroup pnd_Vars__R_Field_9;
    private DbsField pnd_Vars_Pnd_Doc_Case_Ind;
    private DbsField pnd_Vars_Pnd_Doc_Originating_Work_Prcss_Id;
    private DbsField pnd_Vars_Pnd_Doc_Multi_Rqst_Ind;
    private DbsField pnd_Vars_Pnd_Doc_Sub_Rqst_Ind;
    private DbsField pnd_Vars_Pnd_Doc_Entry_Dte_Tme;
    private DbsField pnd_Vars_Pnd_End_Min_Page;
    private DbsField pnd_Vars_Pnd_I;
    private DbsField pnd_Vars_Pnd_Image_Document_Address_Cde;
    private DbsField pnd_Vars_Pnd_Image_Min;

    private DbsGroup pnd_Vars__R_Field_10;
    private DbsField pnd_Vars_Pnd_Mail_Item_Nbr_A;

    private DbsGroup pnd_Vars__R_Field_11;
    private DbsField pnd_Vars_Pnd_Mail_Item_Nbr;
    private DbsField pnd_Vars_Pnd_Start_Min_Page;
    private DbsField pnd_Vars_Pnd_Inotmfa_Found;
    private DbsField pnd_Vars_Pnd_Isn;
    private DbsField pnd_Vars_Pnd_Mail_Item_Nbr_Save;
    private DbsField pnd_Vars_Pnd_Pages;
    private DbsField pnd_Vars_Pnd_Pass_Name;

    private DbsGroup pnd_Vars__R_Field_12;
    private DbsField pnd_Vars_Pnd_Pass_Name_S;
    private DbsField pnd_Vars_Pnd_Pass_Ssn;

    private DbsGroup pnd_Vars__R_Field_13;
    private DbsField pnd_Vars_Pnd_Pass_Ssn_A;
    private DbsField pnd_Vars_Pnd_Pass_Tlc;
    private DbsField pnd_Vars_Pnd_Pda_Key;
    private DbsField pnd_Vars_Pnd_Remainder;
    private DbsField pnd_Vars_Pnd_Remarks;
    private DbsField pnd_Vars_Pnd_Store_Msg;
    private DbsField pnd_Vars_Pnd_Text_Object_Key;

    private DbsGroup pnd_Vars__R_Field_14;
    private DbsField pnd_Vars_Pnd_Cabinet_Id;
    private DbsField pnd_Vars_Pnd_Text_Pointer;
    private DbsField pnd_Vars_Pnd_Text_Sqnce_Nbr;
    private DbsField pnd_Vars_Pnd_Source_Id;
    private DbsField pnd_Vars_Pnd_Source_Id_Min_Key;

    private DbsGroup pnd_Vars__R_Field_15;
    private DbsField pnd_Vars_Pnd_Source_Id_Min_Key1;
    private DbsField pnd_Vars_Pnd_Source_Id_Min_Key2;
    private DbsField pnd_Vars_Pnd_Text_Sqnce_Nbr_Max;
    private DbsField pnd_Vars_Pnd_Total_Line;
    private DbsField pnd_Vars_Pnd_Total_Num;
    private DbsField pnd_Vars_Pnd_Total_Pages;
    private DbsField pnd_Vars_Pnd_Total_Pages_C;
    private DbsField pnd_Vars_Pnd_Total_Pages_I;
    private DbsField pnd_Vars_Pnd_Total_Pages_Ci;
    private DbsField pnd_Vars_Pnd_Total_Ssr;
    private DbsField pnd_Vars_Pnd_Total_Pages_N;
    private DbsField pnd_Vars_Pnd_Total_Pages_C_N;
    private DbsField pnd_Vars_Pnd_Total_Pages_I_N;
    private DbsField pnd_Vars_Pnd_Total_Pages_Ci_N;
    private DbsField pnd_Vars_Pnd_Total_Ssr_N;
    private DbsField pnd_Vars_Pnd_Type_Of_Document;
    private DbsField pnd_Vars_Pnd_Unit_Work_Wpid_Tme_Key3;

    private DbsGroup pnd_Vars__R_Field_16;
    private DbsField pnd_Vars_Pnd_Admin_Unit_Cde;
    private DbsField pnd_Vars_Pnd_Work_List_Ind;
    private DbsField pnd_Vars_Pnd_Work_Prcss_Id;
    private DbsField pnd_Vars_Pnd_Work_Prcss_Short_Nme;
    private DbsField pnd_Vars_Pnd_X;
    private DbsField pnd_Valid_Contracts;
    private DbsField pnd_Ctr;
    private DbsField pnd_J;
    private DbsField pnd_Found_Dup;
    private DbsField pnd_Arr_Cnt;

    private DbsGroup pnd_Array;
    private DbsField pnd_Array_Pnd_Arr_Crrnt_Due_Dte_O;
    private DbsField pnd_Array_Pnd_Arr_Work_Prcss_Id;
    private DbsField pnd_Array_Pnd_Arr_Account_Nbr;
    private DbsField pnd_Array_Pnd_Arr_Pass_Ssn;
    private DbsField pnd_Array_Pnd_Arr_Pass_Name;
    private DbsField pnd_Array_Pnd_Arr_Remarks;
    private DbsField pnd_Array_Pnd_Arr_Pages;

    private DbsRecord internalLoopRecord;
    private DbsField sort01Pnd_Type_Of_DocumentOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaMdma111 = new PdaMdma111(localVariables);
        pdaCwfl5312 = new PdaCwfl5312(localVariables);
        ldaCwfl6800 = new LdaCwfl6800();
        registerRecord(ldaCwfl6800);
        registerRecord(ldaCwfl6800.getVw_cwf_Mit());
        pdaErla1000 = new PdaErla1000(localVariables);

        // Local Variables

        vw_cwf_Doc = new DataAccessProgramView(new NameInfo("vw_cwf_Doc", "CWF-DOC"), "CWF_EFM_DOCUMENT", "CWF_EFM_DOCUMENT");
        cwf_Doc_Cabinet_Id = vw_cwf_Doc.getRecord().newFieldInGroup("cwf_Doc_Cabinet_Id", "CABINET-ID", FieldType.STRING, 14, RepeatingFieldStrategy.None, 
            "CABINET_ID");
        cwf_Doc_Cabinet_Id.setDdmHeader("CABINET/ID");
        cwf_Doc_Tiaa_Rcvd_Dte = vw_cwf_Doc.getRecord().newFieldInGroup("cwf_Doc_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIAA_RCVD_DTE");
        cwf_Doc_Tiaa_Rcvd_Dte.setDdmHeader("TIAA RECVD/DATE");
        cwf_Doc_Case_Workprcss_Multi_Subrqst = vw_cwf_Doc.getRecord().newFieldInGroup("cwf_Doc_Case_Workprcss_Multi_Subrqst", "CASE-WORKPRCSS-MULTI-SUBRQST", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CASE_WORKPRCSS_MULTI_SUBRQST");
        cwf_Doc_Case_Workprcss_Multi_Subrqst.setDdmHeader("CASE/WPID/MULTI/SUB-REQ");

        cwf_Doc_Document_Type = vw_cwf_Doc.getRecord().newGroupInGroup("CWF_DOC_DOCUMENT_TYPE", "DOCUMENT-TYPE");
        cwf_Doc_Document_Type.setDdmHeader("DOCUMENT/TYPE");
        cwf_Doc_Document_Category = cwf_Doc_Document_Type.newFieldInGroup("cwf_Doc_Document_Category", "DOCUMENT-CATEGORY", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "DOCUMENT_CATEGORY");
        cwf_Doc_Document_Category.setDdmHeader("DOC/CAT");
        cwf_Doc_Document_Sub_Category = cwf_Doc_Document_Type.newFieldInGroup("cwf_Doc_Document_Sub_Category", "DOCUMENT-SUB-CATEGORY", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "DOCUMENT_SUB_CATEGORY");
        cwf_Doc_Document_Sub_Category.setDdmHeader("DOC SUB-/CATEGORY");
        cwf_Doc_Document_Detail = cwf_Doc_Document_Type.newFieldInGroup("cwf_Doc_Document_Detail", "DOCUMENT-DETAIL", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "DOCUMENT_DETAIL");
        cwf_Doc_Document_Detail.setDdmHeader("DOCUMENT/DETAIL");
        cwf_Doc_Document_Direction_Ind = vw_cwf_Doc.getRecord().newFieldInGroup("cwf_Doc_Document_Direction_Ind", "DOCUMENT-DIRECTION-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DOCUMENT_DIRECTION_IND");
        cwf_Doc_Document_Direction_Ind.setDdmHeader("DOC/DIR");
        cwf_Doc_Secured_Document_Ind = vw_cwf_Doc.getRecord().newFieldInGroup("cwf_Doc_Secured_Document_Ind", "SECURED-DOCUMENT-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "SECURED_DOCUMENT_IND");
        cwf_Doc_Secured_Document_Ind.setDdmHeader("SECRD/DOC");
        cwf_Doc_Document_Subtype = vw_cwf_Doc.getRecord().newFieldInGroup("cwf_Doc_Document_Subtype", "DOCUMENT-SUBTYPE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "DOCUMENT_SUBTYPE");
        cwf_Doc_Document_Subtype.setDdmHeader("DOC/SUBTP");
        cwf_Doc_Document_Retention_Ind = vw_cwf_Doc.getRecord().newFieldInGroup("cwf_Doc_Document_Retention_Ind", "DOCUMENT-RETENTION-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DOCUMENT_RETENTION_IND");
        cwf_Doc_Document_Retention_Ind.setDdmHeader("DOC/RETNTN");
        cwf_Doc_Entry_System_Or_Unit = vw_cwf_Doc.getRecord().newFieldInGroup("cwf_Doc_Entry_System_Or_Unit", "ENTRY-SYSTEM-OR-UNIT", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ENTRY_SYSTEM_OR_UNIT");
        cwf_Doc_Entry_System_Or_Unit.setDdmHeader("CREATED BY/SYSTEM OR UNIT");
        cwf_Doc_Entry_Dte_Tme = vw_cwf_Doc.getRecord().newFieldInGroup("cwf_Doc_Entry_Dte_Tme", "ENTRY-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "ENTRY_DTE_TME");
        cwf_Doc_Entry_Dte_Tme.setDdmHeader("ENTRY/DATE AND TIME");
        cwf_Doc_Text_Pointer = vw_cwf_Doc.getRecord().newFieldInGroup("cwf_Doc_Text_Pointer", "TEXT-POINTER", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, 
            "TEXT_POINTER");
        cwf_Doc_Text_Pointer.setDdmHeader("TEXT/POINTER");
        cwf_Doc_Image_Min = vw_cwf_Doc.getRecord().newFieldInGroup("cwf_Doc_Image_Min", "IMAGE-MIN", FieldType.STRING, 14, RepeatingFieldStrategy.None, 
            "IMAGE_MIN");
        cwf_Doc_Image_Min.setDdmHeader("IMAGE/POINTER");
        cwf_Doc_Image_Source_Id = vw_cwf_Doc.getRecord().newFieldInGroup("cwf_Doc_Image_Source_Id", "IMAGE-SOURCE-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "IMAGE_SOURCE_ID");
        cwf_Doc_Image_Source_Id.setDdmHeader("IMAGE/SOURCE ID");
        cwf_Doc_Dgtze_Doc_End_Page = vw_cwf_Doc.getRecord().newFieldInGroup("cwf_Doc_Dgtze_Doc_End_Page", "DGTZE-DOC-END-PAGE", FieldType.NUMERIC, 3, 
            RepeatingFieldStrategy.None, "DGTZE_DOC_END_PAGE");
        cwf_Doc_Dgtze_Doc_End_Page.setDdmHeader("DOCUMENT/END PGE");
        registerRecord(vw_cwf_Doc);

        vw_cwf_Image_Xref = new DataAccessProgramView(new NameInfo("vw_cwf_Image_Xref", "CWF-IMAGE-XREF"), "CWF_IMAGE_XREF", "CWF_IMAGE_XREF");
        cwf_Image_Xref_Mail_Item_Nbr = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Mail_Item_Nbr", "MAIL-ITEM-NBR", FieldType.NUMERIC, 
            11, RepeatingFieldStrategy.None, "MAIL_ITEM_NBR");
        cwf_Image_Xref_Mail_Item_Nbr.setDdmHeader("MAIL ITEM/NUMBER");
        cwf_Image_Xref_Image_Document_Address_Cde = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Image_Document_Address_Cde", "IMAGE-DOCUMENT-ADDRESS-CDE", 
            FieldType.STRING, 22, RepeatingFieldStrategy.None, "IMAGE_DOCUMENT_ADDRESS_CDE");
        cwf_Image_Xref_Image_Document_Address_Cde.setDdmHeader("IMNET DOC/ADDRESS");

        cwf_Image_Xref__R_Field_1 = vw_cwf_Image_Xref.getRecord().newGroupInGroup("cwf_Image_Xref__R_Field_1", "REDEFINE", cwf_Image_Xref_Image_Document_Address_Cde);
        cwf_Image_Xref_Image_Document_Address_1 = cwf_Image_Xref__R_Field_1.newFieldInGroup("cwf_Image_Xref_Image_Document_Address_1", "IMAGE-DOCUMENT-ADDRESS-1", 
            FieldType.STRING, 16);
        cwf_Image_Xref_Image_Number_Page = cwf_Image_Xref__R_Field_1.newFieldInGroup("cwf_Image_Xref_Image_Number_Page", "IMAGE-NUMBER-PAGE", FieldType.NUMERIC, 
            5);
        cwf_Image_Xref_Ph_Unique_Id_Nbr = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Ph_Unique_Id_Nbr", "PH-UNIQUE-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "PH_UNIQUE_ID_NBR");
        cwf_Image_Xref_Ph_Unique_Id_Nbr.setDdmHeader("PIN");
        cwf_Image_Xref_Image_Quality_Ind = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Image_Quality_Ind", "IMAGE-QUALITY-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "IMAGE_QUALITY_IND");
        cwf_Image_Xref_Image_Quality_Ind.setDdmHeader("POOR/QLTY");
        cwf_Image_Xref_Upld_Date_Time = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Upld_Date_Time", "UPLD-DATE-TIME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "UPLD_DATE_TIME");
        cwf_Image_Xref_Upld_Date_Time.setDdmHeader("UPLOAD/DATE TIME");
        cwf_Image_Xref_Number_Of_Pages = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Number_Of_Pages", "NUMBER-OF-PAGES", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "NUMBER_OF_PAGES");
        cwf_Image_Xref_Number_Of_Pages.setDdmHeader("NBR OF/PAGES");
        cwf_Image_Xref_Source_Id = vw_cwf_Image_Xref.getRecord().newFieldInGroup("cwf_Image_Xref_Source_Id", "SOURCE-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "SOURCE_ID");
        registerRecord(vw_cwf_Image_Xref);

        vw_cwf_Text = new DataAccessProgramView(new NameInfo("vw_cwf_Text", "CWF-TEXT"), "CWF_EFM_TEXT_OBJECT", "CWF_EMF_TEXT_OBJ", DdmPeriodicGroups.getInstance().getGroups("CWF_EFM_TEXT_OBJECT"));
        cwf_Text_Cabinet_Id = vw_cwf_Text.getRecord().newFieldInGroup("cwf_Text_Cabinet_Id", "CABINET-ID", FieldType.STRING, 14, RepeatingFieldStrategy.None, 
            "CABINET_ID");
        cwf_Text_Cabinet_Id.setDdmHeader("CABINET/ID");
        cwf_Text_Text_Pointer = vw_cwf_Text.getRecord().newFieldInGroup("cwf_Text_Text_Pointer", "TEXT-POINTER", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, 
            "TEXT_POINTER");
        cwf_Text_Text_Pointer.setDdmHeader("TEXT/POINTER");
        cwf_Text_Text_Sqnce_Nbr = vw_cwf_Text.getRecord().newFieldInGroup("cwf_Text_Text_Sqnce_Nbr", "TEXT-SQNCE-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TEXT_SQNCE_NBR");
        cwf_Text_Text_Sqnce_Nbr.setDdmHeader("SQNCE/NBR");
        cwf_Text_Last_Text_Lines_Group_Ind = vw_cwf_Text.getRecord().newFieldInGroup("cwf_Text_Last_Text_Lines_Group_Ind", "LAST-TEXT-LINES-GROUP-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "LAST_TEXT_LINES_GROUP_IND");
        cwf_Text_Last_Text_Lines_Group_Ind.setDdmHeader("LAST/TEXT IND");
        cwf_Text_Count_Casttext_Document_Line_Group = vw_cwf_Text.getRecord().newFieldInGroup("cwf_Text_Count_Casttext_Document_Line_Group", "C*TEXT-DOCUMENT-LINE-GROUP", 
            RepeatingFieldStrategy.CAsteriskVariable, "CWF_EMF_TEXT_OBJ_TEXT_DOCUMENT_LINE_GROUP");

        cwf_Text_Text_Document_Line_Group = vw_cwf_Text.getRecord().newGroupArrayInGroup("cwf_Text_Text_Document_Line_Group", "TEXT-DOCUMENT-LINE-GROUP", 
            new DbsArrayController(1, 30) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CWF_EMF_TEXT_OBJ_TEXT_DOCUMENT_LINE_GROUP");
        cwf_Text_Text_Document_Line_Group.setDdmHeader("TEXT/LINES");
        cwf_Text_Text_Document_Line = cwf_Text_Text_Document_Line_Group.newFieldInGroup("cwf_Text_Text_Document_Line", "TEXT-DOCUMENT-LINE", FieldType.STRING, 
            80, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "TEXT_DOCUMENT_LINE", "CWF_EMF_TEXT_OBJ_TEXT_DOCUMENT_LINE_GROUP");
        cwf_Text_Text_Document_Line.setDdmHeader("TEXT DOCUMENT/LINE");

        cwf_Text__R_Field_2 = cwf_Text_Text_Document_Line_Group.newGroupInGroup("cwf_Text__R_Field_2", "REDEFINE", cwf_Text_Text_Document_Line);
        cwf_Text_Text_04 = cwf_Text__R_Field_2.newFieldInGroup("cwf_Text_Text_04", "TEXT-04", FieldType.STRING, 4);

        cwf_Text__R_Field_3 = cwf_Text_Text_Document_Line_Group.newGroupInGroup("cwf_Text__R_Field_3", "REDEFINE", cwf_Text_Text_Document_Line);
        cwf_Text_Text_1 = cwf_Text__R_Field_3.newFieldInGroup("cwf_Text_Text_1", "TEXT-1", FieldType.STRING, 1);
        cwf_Text_Text_2_And_3 = cwf_Text__R_Field_3.newFieldInGroup("cwf_Text_Text_2_And_3", "TEXT-2-AND-3", FieldType.STRING, 2);
        cwf_Text_Text_4_Thru_77 = cwf_Text__R_Field_3.newFieldInGroup("cwf_Text_Text_4_Thru_77", "TEXT-4-THRU-77", FieldType.STRING, 74);
        cwf_Text_Text_78 = cwf_Text__R_Field_3.newFieldInGroup("cwf_Text_Text_78", "TEXT-78", FieldType.STRING, 3);
        registerRecord(vw_cwf_Text);

        vw_cwf_Wpid = new DataAccessProgramView(new NameInfo("vw_cwf_Wpid", "CWF-WPID"), "CWF_WP_WORK_PRCSS_ID", "CWF_PROFILE");
        cwf_Wpid_Work_Prcss_Id = vw_cwf_Wpid.getRecord().newFieldInGroup("cwf_Wpid_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        cwf_Wpid_Work_Prcss_Id.setDdmHeader("WORK PRCSS ID");
        cwf_Wpid_Work_Prcss_Short_Nme = vw_cwf_Wpid.getRecord().newFieldInGroup("cwf_Wpid_Work_Prcss_Short_Nme", "WORK-PRCSS-SHORT-NME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "WORK_PRCSS_SHORT_NME");
        cwf_Wpid_Work_Prcss_Short_Nme.setDdmHeader("SHORT NAME");
        registerRecord(vw_cwf_Wpid);

        msg_Info = localVariables.newGroupInRecord("msg_Info", "MSG-INFO");
        msg_Info_Pnd_Pnd_Msg = msg_Info.newFieldInGroup("msg_Info_Pnd_Pnd_Msg", "##MSG", FieldType.STRING, 79);
        msg_Info_Pnd_Pnd_Msg_Nr = msg_Info.newFieldInGroup("msg_Info_Pnd_Pnd_Msg_Nr", "##MSG-NR", FieldType.NUMERIC, 4);
        msg_Info_Pnd_Pnd_Msg_Data = msg_Info.newFieldArrayInGroup("msg_Info_Pnd_Pnd_Msg_Data", "##MSG-DATA", FieldType.STRING, 32, new DbsArrayController(1, 
            3));
        msg_Info_Pnd_Pnd_Return_Code = msg_Info.newFieldInGroup("msg_Info_Pnd_Pnd_Return_Code", "##RETURN-CODE", FieldType.STRING, 1);
        msg_Info_Pnd_Pnd_Error_Field = msg_Info.newFieldInGroup("msg_Info_Pnd_Pnd_Error_Field", "##ERROR-FIELD", FieldType.STRING, 32);
        msg_Info_Pnd_Pnd_Error_Field_Index1 = msg_Info.newFieldInGroup("msg_Info_Pnd_Pnd_Error_Field_Index1", "##ERROR-FIELD-INDEX1", FieldType.PACKED_DECIMAL, 
            3);
        msg_Info_Pnd_Pnd_Error_Field_Index2 = msg_Info.newFieldInGroup("msg_Info_Pnd_Pnd_Error_Field_Index2", "##ERROR-FIELD-INDEX2", FieldType.PACKED_DECIMAL, 
            3);
        msg_Info_Pnd_Pnd_Error_Field_Index3 = msg_Info.newFieldInGroup("msg_Info_Pnd_Pnd_Error_Field_Index3", "##ERROR-FIELD-INDEX3", FieldType.PACKED_DECIMAL, 
            3);

        pnd_Vars = localVariables.newGroupInRecord("pnd_Vars", "#VARS");
        pnd_Vars_Pnd_Account_Nbr = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Account_Nbr", "#ACCOUNT-NBR", FieldType.STRING, 10);
        pnd_Vars_Pnd_Account_Nbr_N = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Account_Nbr_N", "#ACCOUNT-NBR-N", FieldType.STRING, 10);
        pnd_Vars_Pnd_Connect_Ind = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Connect_Ind", "#CONNECT-IND", FieldType.STRING, 1);
        pnd_Vars_Pnd_Counter = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Counter", "#COUNTER", FieldType.NUMERIC, 5);
        pnd_Vars_Pnd_Counter_C = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Counter_C", "#COUNTER-C", FieldType.NUMERIC, 5);
        pnd_Vars_Pnd_Counter_I = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Counter_I", "#COUNTER-I", FieldType.NUMERIC, 5);
        pnd_Vars_Pnd_Counter_Send = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Counter_Send", "#COUNTER-SEND", FieldType.NUMERIC, 5);
        pnd_Vars_Pnd_Counter_N = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Counter_N", "#COUNTER-N", FieldType.NUMERIC, 5);
        pnd_Vars_Pnd_Counter_C_N = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Counter_C_N", "#COUNTER-C-N", FieldType.NUMERIC, 5);
        pnd_Vars_Pnd_Counter_I_N = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Counter_I_N", "#COUNTER-I-N", FieldType.NUMERIC, 5);
        pnd_Vars_Pnd_Counter_Send_N = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Counter_Send_N", "#COUNTER-SEND-N", FieldType.NUMERIC, 5);
        pnd_Vars_Pnd_Crrnt_Due_Dte = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Crrnt_Due_Dte", "#CRRNT-DUE-DTE", FieldType.STRING, 8);

        pnd_Vars__R_Field_4 = pnd_Vars.newGroupInGroup("pnd_Vars__R_Field_4", "REDEFINE", pnd_Vars_Pnd_Crrnt_Due_Dte);
        pnd_Vars_Pnd_Yyyy = pnd_Vars__R_Field_4.newFieldInGroup("pnd_Vars_Pnd_Yyyy", "#YYYY", FieldType.STRING, 4);
        pnd_Vars_Pnd_Mm = pnd_Vars__R_Field_4.newFieldInGroup("pnd_Vars_Pnd_Mm", "#MM", FieldType.STRING, 2);
        pnd_Vars_Pnd_Dd = pnd_Vars__R_Field_4.newFieldInGroup("pnd_Vars_Pnd_Dd", "#DD", FieldType.STRING, 2);
        pnd_Vars_Pnd_Crrnt_Due_Dte_O = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Crrnt_Due_Dte_O", "#CRRNT-DUE-DTE-O", FieldType.STRING, 10);
        pnd_Vars_Pnd_Cs_Found = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Cs_Found", "#CS-FOUND", FieldType.BOOLEAN, 1);
        pnd_Vars_Pnd_Date_For_Rerun_N = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Date_For_Rerun_N", "#DATE-FOR-RERUN-N", FieldType.NUMERIC, 8);

        pnd_Vars__R_Field_5 = pnd_Vars.newGroupInGroup("pnd_Vars__R_Field_5", "REDEFINE", pnd_Vars_Pnd_Date_For_Rerun_N);
        pnd_Vars_Pnd_Date_For_Rerun = pnd_Vars__R_Field_5.newFieldInGroup("pnd_Vars_Pnd_Date_For_Rerun", "#DATE-FOR-RERUN", FieldType.STRING, 8);
        pnd_Vars_Pnd_Document_Key = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Document_Key", "#DOCUMENT-KEY", FieldType.STRING, 34);

        pnd_Vars__R_Field_6 = pnd_Vars.newGroupInGroup("pnd_Vars__R_Field_6", "REDEFINE", pnd_Vars_Pnd_Document_Key);
        pnd_Vars_Pnd_Doc_Cabinet_Id = pnd_Vars__R_Field_6.newFieldInGroup("pnd_Vars_Pnd_Doc_Cabinet_Id", "#DOC-CABINET-ID", FieldType.STRING, 14);

        pnd_Vars__R_Field_7 = pnd_Vars__R_Field_6.newGroupInGroup("pnd_Vars__R_Field_7", "REDEFINE", pnd_Vars_Pnd_Doc_Cabinet_Id);
        pnd_Vars_Pnd_Doc_Cabinet_Type = pnd_Vars__R_Field_7.newFieldInGroup("pnd_Vars_Pnd_Doc_Cabinet_Type", "#DOC-CABINET-TYPE", FieldType.STRING, 1);
        pnd_Vars_Pnd_Doc_Cabinet_Name = pnd_Vars__R_Field_7.newFieldInGroup("pnd_Vars_Pnd_Doc_Cabinet_Name", "#DOC-CABINET-NAME", FieldType.STRING, 13);

        pnd_Vars__R_Field_8 = pnd_Vars__R_Field_7.newGroupInGroup("pnd_Vars__R_Field_8", "REDEFINE", pnd_Vars_Pnd_Doc_Cabinet_Name);
        pnd_Vars_Pnd_Doc_Cabinet_Name_A12 = pnd_Vars__R_Field_8.newFieldInGroup("pnd_Vars_Pnd_Doc_Cabinet_Name_A12", "#DOC-CABINET-NAME-A12", FieldType.STRING, 
            12);
        pnd_Vars_Pnd_Doc_Tiaa_Rcvd_Dte = pnd_Vars__R_Field_6.newFieldInGroup("pnd_Vars_Pnd_Doc_Tiaa_Rcvd_Dte", "#DOC-TIAA-RCVD-DTE", FieldType.DATE);
        pnd_Vars_Pnd_Case_Workprcss_Multi_Subrqst = pnd_Vars__R_Field_6.newFieldInGroup("pnd_Vars_Pnd_Case_Workprcss_Multi_Subrqst", "#CASE-WORKPRCSS-MULTI-SUBRQST", 
            FieldType.STRING, 9);

        pnd_Vars__R_Field_9 = pnd_Vars__R_Field_6.newGroupInGroup("pnd_Vars__R_Field_9", "REDEFINE", pnd_Vars_Pnd_Case_Workprcss_Multi_Subrqst);
        pnd_Vars_Pnd_Doc_Case_Ind = pnd_Vars__R_Field_9.newFieldInGroup("pnd_Vars_Pnd_Doc_Case_Ind", "#DOC-CASE-IND", FieldType.STRING, 1);
        pnd_Vars_Pnd_Doc_Originating_Work_Prcss_Id = pnd_Vars__R_Field_9.newFieldInGroup("pnd_Vars_Pnd_Doc_Originating_Work_Prcss_Id", "#DOC-ORIGINATING-WORK-PRCSS-ID", 
            FieldType.STRING, 6);
        pnd_Vars_Pnd_Doc_Multi_Rqst_Ind = pnd_Vars__R_Field_9.newFieldInGroup("pnd_Vars_Pnd_Doc_Multi_Rqst_Ind", "#DOC-MULTI-RQST-IND", FieldType.STRING, 
            1);
        pnd_Vars_Pnd_Doc_Sub_Rqst_Ind = pnd_Vars__R_Field_9.newFieldInGroup("pnd_Vars_Pnd_Doc_Sub_Rqst_Ind", "#DOC-SUB-RQST-IND", FieldType.STRING, 1);
        pnd_Vars_Pnd_Doc_Entry_Dte_Tme = pnd_Vars__R_Field_6.newFieldInGroup("pnd_Vars_Pnd_Doc_Entry_Dte_Tme", "#DOC-ENTRY-DTE-TME", FieldType.TIME);
        pnd_Vars_Pnd_End_Min_Page = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_End_Min_Page", "#END-MIN-PAGE", FieldType.NUMERIC, 3);
        pnd_Vars_Pnd_I = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_Vars_Pnd_Image_Document_Address_Cde = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Image_Document_Address_Cde", "#IMAGE-DOCUMENT-ADDRESS-CDE", FieldType.STRING, 
            22);
        pnd_Vars_Pnd_Image_Min = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Image_Min", "#IMAGE-MIN", FieldType.STRING, 14);

        pnd_Vars__R_Field_10 = pnd_Vars.newGroupInGroup("pnd_Vars__R_Field_10", "REDEFINE", pnd_Vars_Pnd_Image_Min);
        pnd_Vars_Pnd_Mail_Item_Nbr_A = pnd_Vars__R_Field_10.newFieldInGroup("pnd_Vars_Pnd_Mail_Item_Nbr_A", "#MAIL-ITEM-NBR-A", FieldType.STRING, 11);

        pnd_Vars__R_Field_11 = pnd_Vars__R_Field_10.newGroupInGroup("pnd_Vars__R_Field_11", "REDEFINE", pnd_Vars_Pnd_Mail_Item_Nbr_A);
        pnd_Vars_Pnd_Mail_Item_Nbr = pnd_Vars__R_Field_11.newFieldInGroup("pnd_Vars_Pnd_Mail_Item_Nbr", "#MAIL-ITEM-NBR", FieldType.NUMERIC, 11);
        pnd_Vars_Pnd_Start_Min_Page = pnd_Vars__R_Field_10.newFieldInGroup("pnd_Vars_Pnd_Start_Min_Page", "#START-MIN-PAGE", FieldType.NUMERIC, 3);
        pnd_Vars_Pnd_Inotmfa_Found = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Inotmfa_Found", "#INOTMFA-FOUND", FieldType.BOOLEAN, 1);
        pnd_Vars_Pnd_Isn = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Vars_Pnd_Mail_Item_Nbr_Save = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Mail_Item_Nbr_Save", "#MAIL-ITEM-NBR-SAVE", FieldType.NUMERIC, 11);
        pnd_Vars_Pnd_Pages = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Pages", "#PAGES", FieldType.NUMERIC, 4);
        pnd_Vars_Pnd_Pass_Name = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Pass_Name", "#PASS-NAME", FieldType.STRING, 30);

        pnd_Vars__R_Field_12 = pnd_Vars.newGroupInGroup("pnd_Vars__R_Field_12", "REDEFINE", pnd_Vars_Pnd_Pass_Name);
        pnd_Vars_Pnd_Pass_Name_S = pnd_Vars__R_Field_12.newFieldInGroup("pnd_Vars_Pnd_Pass_Name_S", "#PASS-NAME-S", FieldType.STRING, 20);
        pnd_Vars_Pnd_Pass_Ssn = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Pass_Ssn", "#PASS-SSN", FieldType.NUMERIC, 9);

        pnd_Vars__R_Field_13 = pnd_Vars.newGroupInGroup("pnd_Vars__R_Field_13", "REDEFINE", pnd_Vars_Pnd_Pass_Ssn);
        pnd_Vars_Pnd_Pass_Ssn_A = pnd_Vars__R_Field_13.newFieldInGroup("pnd_Vars_Pnd_Pass_Ssn_A", "#PASS-SSN-A", FieldType.STRING, 9);
        pnd_Vars_Pnd_Pass_Tlc = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Pass_Tlc", "#PASS-TLC", FieldType.STRING, 7);
        pnd_Vars_Pnd_Pda_Key = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Pda_Key", "#PDA-KEY", FieldType.NUMERIC, 7);
        pnd_Vars_Pnd_Remainder = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Remainder", "#REMAINDER", FieldType.NUMERIC, 1);
        pnd_Vars_Pnd_Remarks = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Remarks", "#REMARKS", FieldType.STRING, 7);
        pnd_Vars_Pnd_Store_Msg = pnd_Vars.newFieldArrayInGroup("pnd_Vars_Pnd_Store_Msg", "#STORE-MSG", FieldType.STRING, 70, new DbsArrayController(1, 
            2));
        pnd_Vars_Pnd_Text_Object_Key = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Text_Object_Key", "#TEXT-OBJECT-KEY", FieldType.STRING, 24);

        pnd_Vars__R_Field_14 = pnd_Vars.newGroupInGroup("pnd_Vars__R_Field_14", "REDEFINE", pnd_Vars_Pnd_Text_Object_Key);
        pnd_Vars_Pnd_Cabinet_Id = pnd_Vars__R_Field_14.newFieldInGroup("pnd_Vars_Pnd_Cabinet_Id", "#CABINET-ID", FieldType.STRING, 14);
        pnd_Vars_Pnd_Text_Pointer = pnd_Vars__R_Field_14.newFieldInGroup("pnd_Vars_Pnd_Text_Pointer", "#TEXT-POINTER", FieldType.NUMERIC, 7);
        pnd_Vars_Pnd_Text_Sqnce_Nbr = pnd_Vars__R_Field_14.newFieldInGroup("pnd_Vars_Pnd_Text_Sqnce_Nbr", "#TEXT-SQNCE-NBR", FieldType.NUMERIC, 3);
        pnd_Vars_Pnd_Source_Id = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Source_Id", "#SOURCE-ID", FieldType.STRING, 6);
        pnd_Vars_Pnd_Source_Id_Min_Key = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Source_Id_Min_Key", "#SOURCE-ID-MIN-KEY", FieldType.STRING, 17);

        pnd_Vars__R_Field_15 = pnd_Vars.newGroupInGroup("pnd_Vars__R_Field_15", "REDEFINE", pnd_Vars_Pnd_Source_Id_Min_Key);
        pnd_Vars_Pnd_Source_Id_Min_Key1 = pnd_Vars__R_Field_15.newFieldInGroup("pnd_Vars_Pnd_Source_Id_Min_Key1", "#SOURCE-ID-MIN-KEY1", FieldType.STRING, 
            6);
        pnd_Vars_Pnd_Source_Id_Min_Key2 = pnd_Vars__R_Field_15.newFieldInGroup("pnd_Vars_Pnd_Source_Id_Min_Key2", "#SOURCE-ID-MIN-KEY2", FieldType.NUMERIC, 
            11);
        pnd_Vars_Pnd_Text_Sqnce_Nbr_Max = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Text_Sqnce_Nbr_Max", "#TEXT-SQNCE-NBR-MAX", FieldType.NUMERIC, 3);
        pnd_Vars_Pnd_Total_Line = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Total_Line", "#TOTAL-LINE", FieldType.STRING, 40);
        pnd_Vars_Pnd_Total_Num = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Total_Num", "#TOTAL-NUM", FieldType.NUMERIC, 3);
        pnd_Vars_Pnd_Total_Pages = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Total_Pages", "#TOTAL-PAGES", FieldType.NUMERIC, 4);
        pnd_Vars_Pnd_Total_Pages_C = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Total_Pages_C", "#TOTAL-PAGES-C", FieldType.NUMERIC, 4);
        pnd_Vars_Pnd_Total_Pages_I = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Total_Pages_I", "#TOTAL-PAGES-I", FieldType.NUMERIC, 4);
        pnd_Vars_Pnd_Total_Pages_Ci = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Total_Pages_Ci", "#TOTAL-PAGES-CI", FieldType.NUMERIC, 4);
        pnd_Vars_Pnd_Total_Ssr = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Total_Ssr", "#TOTAL-SSR", FieldType.NUMERIC, 6);
        pnd_Vars_Pnd_Total_Pages_N = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Total_Pages_N", "#TOTAL-PAGES-N", FieldType.NUMERIC, 4);
        pnd_Vars_Pnd_Total_Pages_C_N = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Total_Pages_C_N", "#TOTAL-PAGES-C-N", FieldType.NUMERIC, 4);
        pnd_Vars_Pnd_Total_Pages_I_N = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Total_Pages_I_N", "#TOTAL-PAGES-I-N", FieldType.NUMERIC, 4);
        pnd_Vars_Pnd_Total_Pages_Ci_N = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Total_Pages_Ci_N", "#TOTAL-PAGES-CI-N", FieldType.NUMERIC, 4);
        pnd_Vars_Pnd_Total_Ssr_N = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Total_Ssr_N", "#TOTAL-SSR-N", FieldType.NUMERIC, 6);
        pnd_Vars_Pnd_Type_Of_Document = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Type_Of_Document", "#TYPE-OF-DOCUMENT", FieldType.STRING, 1);
        pnd_Vars_Pnd_Unit_Work_Wpid_Tme_Key3 = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Unit_Work_Wpid_Tme_Key3", "#UNIT-WORK-WPID-TME-KEY3", FieldType.STRING, 
            32);

        pnd_Vars__R_Field_16 = pnd_Vars.newGroupInGroup("pnd_Vars__R_Field_16", "REDEFINE", pnd_Vars_Pnd_Unit_Work_Wpid_Tme_Key3);
        pnd_Vars_Pnd_Admin_Unit_Cde = pnd_Vars__R_Field_16.newFieldInGroup("pnd_Vars_Pnd_Admin_Unit_Cde", "#ADMIN-UNIT-CDE", FieldType.STRING, 8);
        pnd_Vars_Pnd_Work_List_Ind = pnd_Vars__R_Field_16.newFieldInGroup("pnd_Vars_Pnd_Work_List_Ind", "#WORK-LIST-IND", FieldType.STRING, 1);
        pnd_Vars_Pnd_Work_Prcss_Id = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Work_Prcss_Id", "#WORK-PRCSS-ID", FieldType.STRING, 6);
        pnd_Vars_Pnd_Work_Prcss_Short_Nme = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Work_Prcss_Short_Nme", "#WORK-PRCSS-SHORT-NME", FieldType.STRING, 15);
        pnd_Vars_Pnd_X = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_X", "#X", FieldType.STRING, 1);
        pnd_Valid_Contracts = localVariables.newFieldInRecord("pnd_Valid_Contracts", "#VALID-CONTRACTS", FieldType.NUMERIC, 2);
        pnd_Ctr = localVariables.newFieldInRecord("pnd_Ctr", "#CTR", FieldType.NUMERIC, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 3);
        pnd_Found_Dup = localVariables.newFieldInRecord("pnd_Found_Dup", "#FOUND-DUP", FieldType.BOOLEAN, 1);
        pnd_Arr_Cnt = localVariables.newFieldInRecord("pnd_Arr_Cnt", "#ARR-CNT", FieldType.NUMERIC, 3);

        pnd_Array = localVariables.newGroupArrayInRecord("pnd_Array", "#ARRAY", new DbsArrayController(1, 999));
        pnd_Array_Pnd_Arr_Crrnt_Due_Dte_O = pnd_Array.newFieldInGroup("pnd_Array_Pnd_Arr_Crrnt_Due_Dte_O", "#ARR-CRRNT-DUE-DTE-O", FieldType.STRING, 10);
        pnd_Array_Pnd_Arr_Work_Prcss_Id = pnd_Array.newFieldInGroup("pnd_Array_Pnd_Arr_Work_Prcss_Id", "#ARR-WORK-PRCSS-ID", FieldType.STRING, 6);
        pnd_Array_Pnd_Arr_Account_Nbr = pnd_Array.newFieldInGroup("pnd_Array_Pnd_Arr_Account_Nbr", "#ARR-ACCOUNT-NBR", FieldType.STRING, 10);
        pnd_Array_Pnd_Arr_Pass_Ssn = pnd_Array.newFieldInGroup("pnd_Array_Pnd_Arr_Pass_Ssn", "#ARR-PASS-SSN", FieldType.STRING, 9);
        pnd_Array_Pnd_Arr_Pass_Name = pnd_Array.newFieldInGroup("pnd_Array_Pnd_Arr_Pass_Name", "#ARR-PASS-NAME", FieldType.STRING, 30);
        pnd_Array_Pnd_Arr_Remarks = pnd_Array.newFieldInGroup("pnd_Array_Pnd_Arr_Remarks", "#ARR-REMARKS", FieldType.STRING, 7);
        pnd_Array_Pnd_Arr_Pages = pnd_Array.newFieldInGroup("pnd_Array_Pnd_Arr_Pages", "#ARR-PAGES", FieldType.NUMERIC, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        sort01Pnd_Type_Of_DocumentOld = internalLoopRecord.newFieldInRecord("Sort01_Pnd_Type_Of_Document_OLD", "Pnd_Type_Of_Document_OLD", FieldType.STRING, 
            1);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Doc.reset();
        vw_cwf_Image_Xref.reset();
        vw_cwf_Text.reset();
        vw_cwf_Wpid.reset();
        internalLoopRecord.reset();

        ldaCwfl6800.initializeValues();

        localVariables.reset();
        pnd_Vars_Pnd_Counter.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Cwfb6800() throws Exception
    {
        super("Cwfb6800");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("CWFB6800", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt4, 4);
        getReports().atTopOfPage(atTopEventRpt5, 5);
        setupReports();
        //*  VR 03/25/2016 START
        //*  OPEN MQ TO FACILITATE MDM CALL
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        //*  VR 03/25/2016 END
        //*  SSR TRANSMITTAL LIST (ARM)
        //*  SSR CONTACT SHEETS   (ARM)
        //*  ERROR MESSAGES       (ARM)
        //*  SSR TRANSMITTAL LIST (LOCAL PRINTER)
        //*  SSR CONTACT SHEETS   (LOCAL PRINTER)
        //*                                                                                                                                                               //Natural: FORMAT LS = 132 PS = 60 SG = OFF;//Natural: FORMAT ( 1 ) LS = 132 PS = 60 SG = OFF;//Natural: FORMAT ( 2 ) LS = 132 PS = 60 SG = OFF;//Natural: FORMAT ( 3 ) LS = 132 PS = 60 SG = OFF;//Natural: FORMAT ( 4 ) LS = 132 PS = 60 SG = OFF;//Natural: FORMAT ( 5 ) LS = 132 PS = 60 SG = OFF
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 4 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 5 )
        //*  GH  01/31/01
        pnd_Vars_Pnd_Date_For_Rerun_N.setValue(Global.getDATN());                                                                                                         //Natural: MOVE *DATN TO #DATE-FOR-RERUN-N
        pnd_Vars_Pnd_Admin_Unit_Cde.setValue("MFTA");                                                                                                                     //Natural: MOVE 'MFTA' TO #ADMIN-UNIT-CDE
        pnd_Vars_Pnd_Work_List_Ind.setValue("3");                                                                                                                         //Natural: MOVE '3' TO #WORK-LIST-IND
        ldaCwfl6800.getVw_cwf_Mit().startDatabaseRead                                                                                                                     //Natural: READ CWF-MIT WITH UNIT-WORK-WPID-TME-KEY3 STARTING FROM #UNIT-WORK-WPID-TME-KEY3
        (
        "READ01",
        new Wc[] { new Wc("UNIT_WORK_WPID_TME_KEY3", ">=", pnd_Vars_Pnd_Unit_Work_Wpid_Tme_Key3, WcType.BY) },
        new Oc[] { new Oc("UNIT_WORK_WPID_TME_KEY3", "ASC") }
        );
        READ01:
        while (condition(ldaCwfl6800.getVw_cwf_Mit().readNextRow("READ01")))
        {
            if (condition(ldaCwfl6800.getCwf_Mit_Admin_Unit_Cde().notEquals(pnd_Vars_Pnd_Admin_Unit_Cde) || ! (DbsUtil.maskMatches(ldaCwfl6800.getCwf_Mit_Work_List_Ind(), //Natural: IF CWF-MIT.ADMIN-UNIT-CDE NE #ADMIN-UNIT-CDE OR CWF-MIT.WORK-LIST-IND NE MASK ( '3'.. )
                "'3'.."))))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Vars_Pnd_Doc_Cabinet_Type.setValue("P");                                                                                                                  //Natural: MOVE 'P' TO #DOC-CABINET-TYPE
            //*  MOVE CWF-MIT.PIN-NBR            TO #DOC-CABINET-NAME     /* PIN-EXP
            //*  PIN-EXP
            pnd_Vars_Pnd_Doc_Cabinet_Name_A12.moveAll(ldaCwfl6800.getCwf_Mit_Pin_Nbr());                                                                                  //Natural: MOVE ALL CWF-MIT.PIN-NBR TO #DOC-CABINET-NAME-A12
            pnd_Vars_Pnd_Doc_Tiaa_Rcvd_Dte.setValue(ldaCwfl6800.getCwf_Mit_Tiaa_Rcvd_Dte());                                                                              //Natural: MOVE CWF-MIT.TIAA-RCVD-DTE TO #DOC-TIAA-RCVD-DTE
            pnd_Vars_Pnd_Doc_Case_Ind.setValue(ldaCwfl6800.getCwf_Mit_Case_Ind());                                                                                        //Natural: MOVE CWF-MIT.CASE-IND TO #DOC-CASE-IND
            pnd_Vars_Pnd_Doc_Originating_Work_Prcss_Id.setValue(ldaCwfl6800.getCwf_Mit_Rqst_Work_Prcss_Id());                                                             //Natural: MOVE CWF-MIT.RQST-WORK-PRCSS-ID TO #DOC-ORIGINATING-WORK-PRCSS-ID
            pnd_Vars_Pnd_Doc_Multi_Rqst_Ind.setValue(ldaCwfl6800.getCwf_Mit_Multi_Rqst_Ind());                                                                            //Natural: MOVE CWF-MIT.MULTI-RQST-IND TO #DOC-MULTI-RQST-IND
            pnd_Vars_Pnd_Doc_Sub_Rqst_Ind.setValue(ldaCwfl6800.getCwf_Mit_Sub_Rqst_Sqnce_Ind());                                                                          //Natural: MOVE CWF-MIT.SUB-RQST-SQNCE-IND TO #DOC-SUB-RQST-IND
            //*  10/19/99 GH
            pnd_Vars_Pnd_Connect_Ind.setValue(ldaCwfl6800.getCwf_Mit_Cnnctd_Rqst_Ind_1());                                                                                //Natural: MOVE CWF-MIT.CNNCTD-RQST-IND-1 TO #CONNECT-IND
            pnd_Vars_Pnd_Doc_Entry_Dte_Tme.reset();                                                                                                                       //Natural: RESET #DOC-ENTRY-DTE-TME
            if (condition(ldaCwfl6800.getCwf_Mit_Admin_Status_Cde().equals("4800") || ldaCwfl6800.getCwf_Mit_Status_Cde().equals("4800")))                                //Natural: IF CWF-MIT.ADMIN-STATUS-CDE = '4800' OR CWF-MIT.STATUS-CDE = '4800'
            {
                getWorkFiles().write(1, false, ldaCwfl6800.getCwf_Mit_Rqst_Log_Dte_Tme(), ldaCwfl6800.getCwf_Mit_Pin_Nbr(), ldaCwfl6800.getCwf_Mit_Work_Prcss_Id(),       //Natural: WRITE WORK FILE 1 CWF-MIT.RQST-LOG-DTE-TME CWF-MIT.PIN-NBR CWF-MIT.WORK-PRCSS-ID #DATE-FOR-RERUN
                    pnd_Vars_Pnd_Date_For_Rerun);
            }                                                                                                                                                             //Natural: END-IF
            pnd_Vars_Pnd_Type_Of_Document.reset();                                                                                                                        //Natural: RESET #TYPE-OF-DOCUMENT
            vw_cwf_Doc.startDatabaseRead                                                                                                                                  //Natural: READ ( 1 ) CWF-DOC WITH DOCUMENT-KEY = #DOCUMENT-KEY
            (
            "READ02",
            new Wc[] { new Wc("DOCUMENT_KEY", ">=", pnd_Vars_Pnd_Document_Key.getBinary(), WcType.BY) },
            new Oc[] { new Oc("DOCUMENT_KEY", "ASC") },
            1
            );
            READ02:
            while (condition(vw_cwf_Doc.readNextRow("READ02")))
            {
                if (condition(cwf_Doc_Cabinet_Id.notEquals(pnd_Vars_Pnd_Doc_Cabinet_Id) || cwf_Doc_Tiaa_Rcvd_Dte.notEquals(pnd_Vars_Pnd_Doc_Tiaa_Rcvd_Dte)                //Natural: IF CWF-DOC.CABINET-ID NE #DOC-CABINET-ID OR CWF-DOC.TIAA-RCVD-DTE NE #DOC-TIAA-RCVD-DTE OR CWF-DOC.CASE-WORKPRCSS-MULTI-SUBRQST NE #CASE-WORKPRCSS-MULTI-SUBRQST
                    || cwf_Doc_Case_Workprcss_Multi_Subrqst.notEquals(pnd_Vars_Pnd_Case_Workprcss_Multi_Subrqst)))
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM IMMEDIATE
                }                                                                                                                                                         //Natural: END-IF
                //*  IMAGE
                if (condition(cwf_Doc_Document_Subtype.equals(" I ")))                                                                                                    //Natural: IF CWF-DOC.DOCUMENT-SUBTYPE = ' I '
                {
                    pnd_Vars_Pnd_Type_Of_Document.setValue("I");                                                                                                          //Natural: MOVE 'I' TO #TYPE-OF-DOCUMENT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Vars_Pnd_Type_Of_Document.setValue("C");                                                                                                          //Natural: MOVE 'C' TO #TYPE-OF-DOCUMENT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Vars_Pnd_Type_Of_Document.equals(" ")))                                                                                                     //Natural: IF #TYPE-OF-DOCUMENT = ' '
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            getSort().writeSortInData(pnd_Vars_Pnd_Type_Of_Document, ldaCwfl6800.getCwf_Mit_Status_Cde(), ldaCwfl6800.getCwf_Mit_Crrnt_Due_Dte(), ldaCwfl6800.getCwf_Mit_Work_Prcss_Id(),  //Natural: END-ALL
                ldaCwfl6800.getCwf_Mit_Admin_Status_Cde(), ldaCwfl6800.getCwf_Mit_Pin_Nbr(), ldaCwfl6800.getCwf_Mit_Tiaa_Rcvd_Dte(), ldaCwfl6800.getCwf_Mit_Case_Ind(), 
                ldaCwfl6800.getCwf_Mit_Rqst_Work_Prcss_Id(), ldaCwfl6800.getCwf_Mit_Multi_Rqst_Ind(), ldaCwfl6800.getCwf_Mit_Sub_Rqst_Sqnce_Ind(), ldaCwfl6800.getCwf_Mit_Bypss_Mutual_Fund_Ind(), 
                pnd_Vars_Pnd_Connect_Ind);
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  10/19/99
        getSort().sortData(pnd_Vars_Pnd_Type_Of_Document, ldaCwfl6800.getCwf_Mit_Status_Cde(), ldaCwfl6800.getCwf_Mit_Crrnt_Due_Dte(), ldaCwfl6800.getCwf_Mit_Work_Prcss_Id()); //Natural: SORT BY #TYPE-OF-DOCUMENT CWF-MIT.STATUS-CDE CWF-MIT.CRRNT-DUE-DTE CWF-MIT.WORK-PRCSS-ID USING CWF-MIT.ADMIN-STATUS-CDE CWF-MIT.PIN-NBR CWF-MIT.TIAA-RCVD-DTE CWF-MIT.CASE-IND CWF-MIT.RQST-WORK-PRCSS-ID CWF-MIT.MULTI-RQST-IND CWF-MIT.SUB-RQST-SQNCE-IND CWF-MIT.BYPSS-MUTUAL-FUND-IND #CONNECT-IND
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(pnd_Vars_Pnd_Type_Of_Document, ldaCwfl6800.getCwf_Mit_Status_Cde(), ldaCwfl6800.getCwf_Mit_Crrnt_Due_Dte(), 
            ldaCwfl6800.getCwf_Mit_Work_Prcss_Id(), ldaCwfl6800.getCwf_Mit_Admin_Status_Cde(), ldaCwfl6800.getCwf_Mit_Pin_Nbr(), ldaCwfl6800.getCwf_Mit_Tiaa_Rcvd_Dte(), 
            ldaCwfl6800.getCwf_Mit_Case_Ind(), ldaCwfl6800.getCwf_Mit_Rqst_Work_Prcss_Id(), ldaCwfl6800.getCwf_Mit_Multi_Rqst_Ind(), ldaCwfl6800.getCwf_Mit_Sub_Rqst_Sqnce_Ind(), 
            ldaCwfl6800.getCwf_Mit_Bypss_Mutual_Fund_Ind(), pnd_Vars_Pnd_Connect_Ind)))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            if (condition(pnd_Vars_Pnd_Type_Of_Document.equals("C")))                                                                                                     //Natural: IF #TYPE-OF-DOCUMENT = 'C'
            {
                pnd_Vars_Pnd_Remarks.setValue("CSheet");                                                                                                                  //Natural: MOVE 'CSheet' TO #REMARKS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Vars_Pnd_Remarks.setValue("Image ");                                                                                                                  //Natural: MOVE 'Image ' TO #REMARKS
            }                                                                                                                                                             //Natural: END-IF
            //*  LEAVE THE 'ReSent' LOGIC FOR OLD OPEN TFDHV RECORDS
            //*                03/07/01 GH
            if (condition(ldaCwfl6800.getCwf_Mit_Admin_Status_Cde().equals("4820") || ldaCwfl6800.getCwf_Mit_Status_Cde().equals("4820")))                                //Natural: IF CWF-MIT.ADMIN-STATUS-CDE = '4820' OR CWF-MIT.STATUS-CDE = '4820'
            {
                pnd_Vars_Pnd_Remarks.setValue("ReSend");                                                                                                                  //Natural: MOVE 'ReSend' TO #REMARKS
            }                                                                                                                                                             //Natural: END-IF
            pnd_Vars_Pnd_Crrnt_Due_Dte.setValue(ldaCwfl6800.getCwf_Mit_Crrnt_Due_Dte());                                                                                  //Natural: MOVE CWF-MIT.CRRNT-DUE-DTE TO #CRRNT-DUE-DTE
            pnd_Vars_Pnd_Crrnt_Due_Dte_O.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Vars_Pnd_Mm, "/", pnd_Vars_Pnd_Dd, "/", pnd_Vars_Pnd_Yyyy));        //Natural: COMPRESS #MM '/' #DD '/' #YYYY INTO #CRRNT-DUE-DTE-O LEAVING NO SPACE
            pnd_Vars_Pnd_Work_Prcss_Id.setValue(ldaCwfl6800.getCwf_Mit_Work_Prcss_Id());                                                                                  //Natural: MOVE CWF-MIT.WORK-PRCSS-ID TO #WORK-PRCSS-ID
            pnd_Vars_Pnd_Counter.nadd(1);                                                                                                                                 //Natural: ASSIGN #COUNTER := #COUNTER + 1
            //*  VR 3/25/2016
            pnd_Vars_Pnd_Account_Nbr.reset();                                                                                                                             //Natural: RESET #ACCOUNT-NBR #ACCOUNT-NBR-N
            pnd_Vars_Pnd_Account_Nbr_N.reset();
                                                                                                                                                                          //Natural: PERFORM ACCOUNT-NUMBER-PAGE
            sub_Account_Number_Page();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*   10/19/99 GH
            //*   07/23/09 GH
            if (condition(pnd_Vars_Pnd_Remarks.notEquals("ReSend") && pnd_Vars_Pnd_Remarks.notEquals("NoPrint") && pnd_Vars_Pnd_Work_Prcss_Id.notEquals("TFDHV")))        //Natural: IF #REMARKS NE 'ReSend' AND #REMARKS NE 'NoPrint' AND #WORK-PRCSS-ID NE 'TFDHV' THEN
            {
                pnd_Vars_Pnd_Counter_Send.nadd(1);                                                                                                                        //Natural: ASSIGN #COUNTER-SEND := #COUNTER-SEND + 1
                pnd_Vars_Pnd_Total_Pages.nadd(pnd_Vars_Pnd_Pages);                                                                                                        //Natural: ASSIGN #TOTAL-PAGES := #TOTAL-PAGES + #PAGES
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Vars_Pnd_Pages.reset();                                                                                                                               //Natural: RESET #PAGES
            }                                                                                                                                                             //Natural: END-IF
            //*   DO NOT SEND 1. OLD REQUEST 2. ADDR CHG : TO THE REPORT /* GH 7/21/09
            //*   07/23/09 GH
            if (condition(pnd_Vars_Pnd_Remarks.notEquals("ReSend") && pnd_Vars_Pnd_Remarks.notEquals("NoPrint") && pnd_Vars_Pnd_Work_Prcss_Id.notEquals("TFDHV")))        //Natural: IF #REMARKS NE 'ReSend' AND #REMARKS NE 'NoPrint' AND #WORK-PRCSS-ID NE 'TFDHV'
            {
                //*  VR 03/25/2016 START
                //*     RESET #MDMA110          /*MDM PIN EXPANSION
                //* MDM PIN EXPANSION
                pdaMdma111.getPnd_Mdma111().reset();                                                                                                                      //Natural: RESET #MDMA111
                //*    #MDMA110.#I-PIN := CWF-MIT.PIN-NBR
                pdaMdma111.getPnd_Mdma111_Pnd_I_Pin_N12().setValue(ldaCwfl6800.getCwf_Mit_Pin_Nbr());                                                                     //Natural: ASSIGN #MDMA111.#I-PIN-N12 := CWF-MIT.PIN-NBR
                //* MDM PIN EXPANSION
                if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                        //Natural: IF *DEVICE = 'BATCH'
                {
                    //*      CALLNAT 'MDMN110A' #MDMA110
                    //* MDM PIN EXPANSION
                    DbsUtil.callnat(Mdmn111a.class , getCurrentProcessState(), pdaMdma111.getPnd_Mdma111());                                                              //Natural: CALLNAT 'MDMN111A' #MDMA111
                    if (condition(Global.isEscape())) return;
                    //* MDM PIN EXPANSION
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*      CALLNAT 'MDMN110' #MDMA110 /*MDM PIN EXPANSION
                    //* MDM PIN EXPANSION
                    DbsUtil.callnat(Mdmn111.class , getCurrentProcessState(), pdaMdma111.getPnd_Mdma111());                                                               //Natural: CALLNAT 'MDMN111' #MDMA111
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: END-IF
                //*    IF #MDMA110.#O-RETURN-CODE EQ '0000'
                //* MDM PIN EXPANSION
                if (condition(pdaMdma111.getPnd_Mdma111_Pnd_O_Return_Code().equals("0000")))                                                                              //Natural: IF #MDMA111.#O-RETURN-CODE EQ '0000'
                {
                    //* MDM PIN EXPANSION
                    pnd_Vars_Pnd_Pass_Name.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pdaMdma111.getPnd_Mdma111_Pnd_O_Last_Name(), ","));                   //Natural: COMPRESS #O-LAST-NAME ',' INTO #PASS-NAME LEAVING NO SPACE
                    pnd_Vars_Pnd_Pass_Name.setValue(DbsUtil.compress(pnd_Vars_Pnd_Pass_Name, pdaMdma111.getPnd_Mdma111_Pnd_O_First_Name(), pdaMdma111.getPnd_Mdma111_Pnd_O_Middle_Name())); //Natural: COMPRESS #PASS-NAME #O-FIRST-NAME #O-MIDDLE-NAME INTO #PASS-NAME
                    if (condition(pdaMdma111.getPnd_Mdma111_Pnd_O_Suffix().greater(" ")))                                                                                 //Natural: IF #O-SUFFIX GT ' '
                    {
                        pnd_Vars_Pnd_Pass_Name.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Vars_Pnd_Pass_Name, ","));                                    //Natural: COMPRESS #PASS-NAME ',' INTO #PASS-NAME LEAVING NO
                        pnd_Vars_Pnd_Pass_Name.setValue(DbsUtil.compress(pnd_Vars_Pnd_Pass_Name, pdaMdma111.getPnd_Mdma111_Pnd_O_Suffix()));                              //Natural: COMPRESS #PASS-NAME #O-SUFFIX INTO #PASS-NAME
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Vars_Pnd_Pass_Ssn.setValue(pdaMdma111.getPnd_Mdma111_Pnd_O_Ssn());                                                                                //Natural: MOVE #O-SSN TO #PASS-SSN
                    if (condition(pdaMdma111.getPnd_Mdma111_Pnd_O_Active_Vip_Ind().notEquals("Y") && pdaMdma111.getPnd_Mdma111_Pnd_O_Active_Vip_Ind().notEquals("1")))    //Natural: IF #O-ACTIVE-VIP-IND NE 'Y' AND #O-ACTIVE-VIP-IND NE '1'
                    {
                        pnd_Vars_Pnd_Pass_Tlc.setValue(" ");                                                                                                              //Natural: MOVE ' ' TO #PASS-TLC
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Vars_Pnd_Pass_Tlc.setValue("**TLC**");                                                                                                        //Natural: MOVE '**TLC**' TO #PASS-TLC
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Valid_Contracts.reset();                                                                                                                          //Natural: RESET #VALID-CONTRACTS
                                                                                                                                                                          //Natural: PERFORM CONTRACT-CHECK
                    sub_Contract_Check();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_Valid_Contracts.greater(getZero())))                                                                                                //Natural: IF #VALID-CONTRACTS > 0
                    {
                        pnd_Found_Dup.reset();                                                                                                                            //Natural: RESET #FOUND-DUP
                        FOR01:                                                                                                                                            //Natural: FOR #J 1 TO #ARR-CNT
                        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_Arr_Cnt)); pnd_J.nadd(1))
                        {
                            if (condition(pnd_Vars_Pnd_Crrnt_Due_Dte_O.equals(pnd_Array_Pnd_Arr_Crrnt_Due_Dte_O.getValue(pnd_J)) && pnd_Vars_Pnd_Work_Prcss_Id.equals(pnd_Array_Pnd_Arr_Work_Prcss_Id.getValue(pnd_J))  //Natural: IF #CRRNT-DUE-DTE-O EQ #ARR-CRRNT-DUE-DTE-O ( #J ) AND #WORK-PRCSS-ID EQ #ARR-WORK-PRCSS-ID ( #J ) AND #ACCOUNT-NBR-N EQ #ARR-ACCOUNT-NBR ( #J ) AND #PASS-SSN-A EQ #ARR-PASS-SSN ( #J ) AND #PASS-NAME EQ #ARR-PASS-NAME ( #J ) AND #REMARKS EQ #ARR-REMARKS ( #J )
                                && pnd_Vars_Pnd_Account_Nbr_N.equals(pnd_Array_Pnd_Arr_Account_Nbr.getValue(pnd_J)) && pnd_Vars_Pnd_Pass_Ssn_A.equals(pnd_Array_Pnd_Arr_Pass_Ssn.getValue(pnd_J)) 
                                && pnd_Vars_Pnd_Pass_Name.equals(pnd_Array_Pnd_Arr_Pass_Name.getValue(pnd_J)) && pnd_Vars_Pnd_Remarks.equals(pnd_Array_Pnd_Arr_Remarks.getValue(pnd_J))))
                            {
                                pnd_Found_Dup.setValue(true);                                                                                                             //Natural: MOVE TRUE TO #FOUND-DUP
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-FOR
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(! (pnd_Found_Dup.getBoolean())))                                                                                                    //Natural: IF NOT #FOUND-DUP
                        {
                            pnd_Vars_Pnd_Counter_Send_N.nadd(1);                                                                                                          //Natural: ASSIGN #COUNTER-SEND-N := #COUNTER-SEND-N + 1
                            pnd_Vars_Pnd_Total_Pages_N.nadd(pnd_Vars_Pnd_Pages);                                                                                          //Natural: ASSIGN #TOTAL-PAGES-N := #TOTAL-PAGES-N + #PAGES
                            getReports().write(4, pnd_Vars_Pnd_Counter,pnd_Vars_Pnd_Crrnt_Due_Dte_O,new ColumnSpacing(2),pnd_Vars_Pnd_Work_Prcss_Id,pnd_Vars_Pnd_Work_Prcss_Short_Nme,pnd_Vars_Pnd_Account_Nbr_N,new  //Natural: WRITE ( 4 ) #COUNTER #CRRNT-DUE-DTE-O 2X #WORK-PRCSS-ID #WORK-PRCSS-SHORT-NME #ACCOUNT-NBR-N 2X #PASS-SSN ( EM = 999999999 ) #PASS-NAME-S #REMARKS 3X #PAGES
                                ColumnSpacing(2),pnd_Vars_Pnd_Pass_Ssn, new ReportEditMask ("999999999"),pnd_Vars_Pnd_Pass_Name_S,pnd_Vars_Pnd_Remarks,new 
                                ColumnSpacing(3),pnd_Vars_Pnd_Pages);
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            pnd_Arr_Cnt.nadd(1);                                                                                                                          //Natural: ADD 1 TO #ARR-CNT
                            pnd_Array_Pnd_Arr_Crrnt_Due_Dte_O.getValue(pnd_Arr_Cnt).setValue(pnd_Vars_Pnd_Crrnt_Due_Dte_O);                                               //Natural: MOVE #CRRNT-DUE-DTE-O TO #ARR-CRRNT-DUE-DTE-O ( #ARR-CNT )
                            pnd_Array_Pnd_Arr_Work_Prcss_Id.getValue(pnd_Arr_Cnt).setValue(pnd_Vars_Pnd_Work_Prcss_Id);                                                   //Natural: MOVE #WORK-PRCSS-ID TO #ARR-WORK-PRCSS-ID ( #ARR-CNT )
                            pnd_Array_Pnd_Arr_Account_Nbr.getValue(pnd_Arr_Cnt).setValue(pnd_Vars_Pnd_Account_Nbr_N);                                                     //Natural: MOVE #ACCOUNT-NBR-N TO #ARR-ACCOUNT-NBR ( #ARR-CNT )
                            pnd_Array_Pnd_Arr_Pass_Ssn.getValue(pnd_Arr_Cnt).setValue(pnd_Vars_Pnd_Pass_Ssn);                                                             //Natural: MOVE #PASS-SSN TO #ARR-PASS-SSN ( #ARR-CNT )
                            pnd_Array_Pnd_Arr_Pass_Name.getValue(pnd_Arr_Cnt).setValue(pnd_Vars_Pnd_Pass_Name);                                                           //Natural: MOVE #PASS-NAME TO #ARR-PASS-NAME ( #ARR-CNT )
                            pnd_Array_Pnd_Arr_Remarks.getValue(pnd_Arr_Cnt).setValue(pnd_Vars_Pnd_Remarks);                                                               //Natural: MOVE #REMARKS TO #ARR-REMARKS ( #ARR-CNT )
                            pnd_Array_Pnd_Arr_Pages.getValue(pnd_Arr_Cnt).setValue(pnd_Vars_Pnd_Pages);                                                                   //Natural: MOVE #PAGES TO #ARR-PAGES ( #ARR-CNT )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*      WRITE 'N100:' CWF-MIT.PIN-NBR #MDMA110.#O-RETURN-TEXT
                    //* MDM PIN EXPANSION
                    getReports().write(0, "N100:",ldaCwfl6800.getCwf_Mit_Pin_Nbr(),pdaMdma111.getPnd_Mdma111_Pnd_O_Return_Text());                                        //Natural: WRITE 'N100:' CWF-MIT.PIN-NBR #MDMA111.#O-RETURN-TEXT
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //* MDM PIN EXPANSION
                }                                                                                                                                                         //Natural: END-IF
                //*  VR 03/25/2016 END
                getReports().write(1, pnd_Vars_Pnd_Counter,pnd_Vars_Pnd_Crrnt_Due_Dte_O,new ColumnSpacing(2),pnd_Vars_Pnd_Work_Prcss_Id,pnd_Vars_Pnd_Work_Prcss_Short_Nme,pnd_Vars_Pnd_Account_Nbr,new  //Natural: WRITE ( 1 ) #COUNTER #CRRNT-DUE-DTE-O 2X #WORK-PRCSS-ID #WORK-PRCSS-SHORT-NME #ACCOUNT-NBR 2X #PASS-SSN ( EM = 999999999 ) #PASS-NAME-S #REMARKS 3X #PAGES
                    ColumnSpacing(2),pnd_Vars_Pnd_Pass_Ssn, new ReportEditMask ("999999999"),pnd_Vars_Pnd_Pass_Name_S,pnd_Vars_Pnd_Remarks,new ColumnSpacing(3),
                    pnd_Vars_Pnd_Pages);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  VR 03/25/2016 START
                pnd_Vars_Pnd_Crrnt_Due_Dte_O.reset();                                                                                                                     //Natural: RESET #CRRNT-DUE-DTE-O #WORK-PRCSS-ID #ACCOUNT-NBR-N #PASS-SSN #PASS-NAME #REMARKS #PAGES
                pnd_Vars_Pnd_Work_Prcss_Id.reset();
                pnd_Vars_Pnd_Account_Nbr_N.reset();
                pnd_Vars_Pnd_Pass_Ssn.reset();
                pnd_Vars_Pnd_Pass_Name.reset();
                pnd_Vars_Pnd_Remarks.reset();
                pnd_Vars_Pnd_Pages.reset();
                //*  VR 03/25/2016 END
            }                                                                                                                                                             //Natural: END-IF
            //*                                                                                                                                                           //Natural: AT BREAK OF #TYPE-OF-DOCUMENT
            //*                                                                                                                                                           //Natural: AT END OF DATA
            sort01Pnd_Type_Of_DocumentOld.setValue(pnd_Vars_Pnd_Type_Of_Document);                                                                                        //Natural: END-SORT
        }
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        if (condition(getSort().getAtEndOfData()))
        {
            pnd_Vars_Pnd_Total_Ssr.compute(new ComputeParameters(false, pnd_Vars_Pnd_Total_Ssr), pnd_Vars_Pnd_Counter_C.add(pnd_Vars_Pnd_Counter_I));                     //Natural: ASSIGN #TOTAL-SSR := #COUNTER-C + #COUNTER-I
            pnd_Vars_Pnd_Total_Pages_Ci.compute(new ComputeParameters(false, pnd_Vars_Pnd_Total_Pages_Ci), pnd_Vars_Pnd_Total_Pages_C.add(pnd_Vars_Pnd_Total_Pages_I));   //Natural: ASSIGN #TOTAL-PAGES-CI := #TOTAL-PAGES-C + #TOTAL-PAGES-I
            //*  VR 03/25/2016 START
            pnd_Vars_Pnd_Total_Ssr_N.compute(new ComputeParameters(false, pnd_Vars_Pnd_Total_Ssr_N), pnd_Vars_Pnd_Counter_C_N.add(pnd_Vars_Pnd_Counter_I_N));             //Natural: ASSIGN #TOTAL-SSR-N := #COUNTER-C-N + #COUNTER-I-N
            pnd_Vars_Pnd_Total_Pages_Ci_N.compute(new ComputeParameters(false, pnd_Vars_Pnd_Total_Pages_Ci_N), pnd_Vars_Pnd_Total_Pages_C_N.add(pnd_Vars_Pnd_Total_Pages_I_N)); //Natural: ASSIGN #TOTAL-PAGES-CI-N := #TOTAL-PAGES-C-N + #TOTAL-PAGES-I-N
            //*  VR 03/25/2016 END
            pnd_Vars_Pnd_Total_Line.setValue("Total Participants/Shared Service Rqsts:");                                                                                 //Natural: MOVE 'Total Participants/Shared Service Rqsts:' TO #TOTAL-LINE
        }                                                                                                                                                                 //Natural: END-ENDDATA
        endSort();
        getReports().write(1, NEWLINE,NEWLINE,pnd_Vars_Pnd_Total_Line,pnd_Vars_Pnd_Total_Ssr,new TabSetting(65),"Grand Total Pages Send : ",pnd_Vars_Pnd_Total_Pages_Ci); //Natural: WRITE ( 1 ) // #TOTAL-LINE #TOTAL-SSR 65T 'Grand Total Pages Send : ' #TOTAL-PAGES-CI
        if (Global.isEscape()) return;
        //*  VR 03/25/2016 START
        //*  WRITE(4) //
        //*    #TOTAL-LINE #TOTAL-SSR 65T 'Grand Total Pages Send : '
        //*                                #TOTAL-PAGES-CI
        getReports().write(4, NEWLINE,NEWLINE,pnd_Vars_Pnd_Total_Line,pnd_Vars_Pnd_Total_Ssr_N,new TabSetting(65),"Grand Total Pages Send : ",pnd_Vars_Pnd_Total_Pages_Ci_N); //Natural: WRITE ( 4 ) // #TOTAL-LINE #TOTAL-SSR-N 65T 'Grand Total Pages Send : ' #TOTAL-PAGES-CI-N
        if (Global.isEscape()) return;
        //*  VR 03/25/2016 END
                                                                                                                                                                          //Natural: PERFORM LAST-RECORD
        sub_Last_Record();
        if (condition(Global.isEscape())) {return;}
        //*  VR 03/25/2016 START
        //*  CLOSE MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        //*  VR 03/25/2016 END
        //* *************************** *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LAST-RECORD
        //* *************************** *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCOUNT-NUMBER-PAGE
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-DOC
        //* ****************************
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-IMAGE
        //*  VR 03/25/2016 START
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CONTRACT-CHECK
        //* *------------------------------
        //*  VR 03/25/2016 END
        //* *************************** *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOG-MSG
        //*  ******************************************************************
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Last_Record() throws Exception                                                                                                                       //Natural: LAST-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************** *
        pnd_Vars_Pnd_Crrnt_Due_Dte_O.reset();                                                                                                                             //Natural: RESET #CRRNT-DUE-DTE-O #WORK-PRCSS-ID #DOC-CABINET-NAME #IMAGE-DOCUMENT-ADDRESS-CDE #START-MIN-PAGE #END-MIN-PAGE
        pnd_Vars_Pnd_Work_Prcss_Id.reset();
        pnd_Vars_Pnd_Doc_Cabinet_Name.reset();
        pnd_Vars_Pnd_Image_Document_Address_Cde.reset();
        pnd_Vars_Pnd_Start_Min_Page.reset();
        pnd_Vars_Pnd_End_Min_Page.reset();
        pnd_Vars_Pnd_Counter.setValue(99999);                                                                                                                             //Natural: ASSIGN #COUNTER := 99999
        pnd_Vars_Pnd_End_Min_Page.setValue(pnd_Vars_Pnd_Total_Pages_I);                                                                                                   //Natural: ASSIGN #END-MIN-PAGE := #TOTAL-PAGES-I
        //*  SEQUENCE NUMBER
        //*  DUE DATE
        //*  WPID....
        //*  PIN .......
        //*  DOCUMENT-ID
        //*  START-PAGE
        //*  END PAGE
        getWorkFiles().write(2, false, pnd_Vars_Pnd_Counter, pnd_Vars_Pnd_X, pnd_Vars_Pnd_Crrnt_Due_Dte_O, pnd_Vars_Pnd_X, pnd_Vars_Pnd_Work_Prcss_Id,                    //Natural: WRITE WORK FILE 2 #COUNTER #X #CRRNT-DUE-DTE-O #X #WORK-PRCSS-ID #X #DOC-CABINET-NAME #X #IMAGE-DOCUMENT-ADDRESS-CDE #X #START-MIN-PAGE #X #END-MIN-PAGE
            pnd_Vars_Pnd_X, pnd_Vars_Pnd_Doc_Cabinet_Name, pnd_Vars_Pnd_X, pnd_Vars_Pnd_Image_Document_Address_Cde, pnd_Vars_Pnd_X, pnd_Vars_Pnd_Start_Min_Page, 
            pnd_Vars_Pnd_X, pnd_Vars_Pnd_End_Min_Page);
    }
    private void sub_Account_Number_Page() throws Exception                                                                                                               //Natural: ACCOUNT-NUMBER-PAGE
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************** *
        pnd_Vars_Pnd_Pages.reset();                                                                                                                                       //Natural: RESET #PAGES
        vw_cwf_Wpid.startDatabaseRead                                                                                                                                     //Natural: READ ( 1 ) CWF-WPID BY WPID-UNIQ-KEY = CWF-MIT.WORK-PRCSS-ID
        (
        "READ03",
        new Wc[] { new Wc("WPID_UNIQ_KEY", ">=", ldaCwfl6800.getCwf_Mit_Work_Prcss_Id(), WcType.BY) },
        new Oc[] { new Oc("WPID_UNIQ_KEY", "ASC") },
        1
        );
        READ03:
        while (condition(vw_cwf_Wpid.readNextRow("READ03")))
        {
            pnd_Vars_Pnd_Work_Prcss_Short_Nme.setValue(cwf_Wpid_Work_Prcss_Short_Nme);                                                                                    //Natural: MOVE WORK-PRCSS-SHORT-NME TO #WORK-PRCSS-SHORT-NME
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*             READ FROM ELECTRONIC DOCUMENT FILE
        pnd_Vars_Pnd_Doc_Cabinet_Type.setValue("P");                                                                                                                      //Natural: MOVE 'P' TO #DOC-CABINET-TYPE
        //*  MOVE CWF-MIT.PIN-NBR            TO #DOC-CABINET-NAME     /* PIN-EXP
        //*  PIN-EXP
        pnd_Vars_Pnd_Doc_Cabinet_Name_A12.moveAll(ldaCwfl6800.getCwf_Mit_Pin_Nbr());                                                                                      //Natural: MOVE ALL CWF-MIT.PIN-NBR TO #DOC-CABINET-NAME-A12
        pnd_Vars_Pnd_Doc_Tiaa_Rcvd_Dte.setValue(ldaCwfl6800.getCwf_Mit_Tiaa_Rcvd_Dte());                                                                                  //Natural: MOVE CWF-MIT.TIAA-RCVD-DTE TO #DOC-TIAA-RCVD-DTE
        pnd_Vars_Pnd_Doc_Case_Ind.setValue(ldaCwfl6800.getCwf_Mit_Case_Ind());                                                                                            //Natural: MOVE CWF-MIT.CASE-IND TO #DOC-CASE-IND
        pnd_Vars_Pnd_Doc_Originating_Work_Prcss_Id.setValue(ldaCwfl6800.getCwf_Mit_Rqst_Work_Prcss_Id());                                                                 //Natural: MOVE CWF-MIT.RQST-WORK-PRCSS-ID TO #DOC-ORIGINATING-WORK-PRCSS-ID
        pnd_Vars_Pnd_Doc_Multi_Rqst_Ind.setValue(ldaCwfl6800.getCwf_Mit_Multi_Rqst_Ind());                                                                                //Natural: MOVE CWF-MIT.MULTI-RQST-IND TO #DOC-MULTI-RQST-IND
        pnd_Vars_Pnd_Doc_Sub_Rqst_Ind.setValue(ldaCwfl6800.getCwf_Mit_Sub_Rqst_Sqnce_Ind());                                                                              //Natural: MOVE CWF-MIT.SUB-RQST-SQNCE-IND TO #DOC-SUB-RQST-IND
        pnd_Vars_Pnd_Doc_Entry_Dte_Tme.reset();                                                                                                                           //Natural: RESET #DOC-ENTRY-DTE-TME #CS-FOUND #INOTMFA-FOUND
        pnd_Vars_Pnd_Cs_Found.reset();
        pnd_Vars_Pnd_Inotmfa_Found.reset();
        vw_cwf_Doc.startDatabaseRead                                                                                                                                      //Natural: READ CWF-DOC WITH DOCUMENT-KEY = #DOCUMENT-KEY
        (
        "READ04",
        new Wc[] { new Wc("DOCUMENT_KEY", ">=", pnd_Vars_Pnd_Document_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("DOCUMENT_KEY", "ASC") }
        );
        READ04:
        while (condition(vw_cwf_Doc.readNextRow("READ04")))
        {
            if (condition(cwf_Doc_Cabinet_Id.notEquals(pnd_Vars_Pnd_Doc_Cabinet_Id) || cwf_Doc_Tiaa_Rcvd_Dte.notEquals(pnd_Vars_Pnd_Doc_Tiaa_Rcvd_Dte)                    //Natural: IF CWF-DOC.CABINET-ID NE #DOC-CABINET-ID OR CWF-DOC.TIAA-RCVD-DTE NE #DOC-TIAA-RCVD-DTE OR CWF-DOC.CASE-WORKPRCSS-MULTI-SUBRQST NE #CASE-WORKPRCSS-MULTI-SUBRQST
                || cwf_Doc_Case_Workprcss_Multi_Subrqst.notEquals(pnd_Vars_Pnd_Case_Workprcss_Multi_Subrqst)))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            //*  VR 03/25/2016 START
            //*  MOVE CWF-MIT.PIN-NBR TO #PDA-KEY
            //*  CALLNAT 'CWFN5371' #PDA-KEY
            //*    #PASS-NAME
            //*    #PASS-SSN
            //*    #PASS-TLC
            //*  VR 03/25/2016 END
            //*  WITH #START-MIN-PAGE
            pnd_Vars_Pnd_Image_Min.setValue(cwf_Doc_Image_Min);                                                                                                           //Natural: MOVE CWF-DOC.IMAGE-MIN TO #IMAGE-MIN
            pnd_Vars_Pnd_Cabinet_Id.setValue(cwf_Doc_Cabinet_Id);                                                                                                         //Natural: MOVE CWF-DOC.CABINET-ID TO #CABINET-ID
            pnd_Vars_Pnd_Text_Pointer.setValue(cwf_Doc_Text_Pointer);                                                                                                     //Natural: MOVE CWF-DOC.TEXT-POINTER TO #TEXT-POINTER
            //*  IMAGE
            if (condition(cwf_Doc_Document_Subtype.equals(" I ")))                                                                                                        //Natural: IF CWF-DOC.DOCUMENT-SUBTYPE = ' I '
            {
                //*         CHECK IF MIN ALREADY PRINTED, ESCAPE TOP
                //*   07/23/09 GH
                if (condition(pnd_Vars_Pnd_Mail_Item_Nbr.notEquals(pnd_Vars_Pnd_Mail_Item_Nbr_Save) && pnd_Vars_Pnd_Remarks.notEquals("ReSend") && pnd_Vars_Pnd_Work_Prcss_Id.notEquals("TFDHV"))) //Natural: IF #MAIL-ITEM-NBR NE #MAIL-ITEM-NBR-SAVE AND #REMARKS NE 'ReSend' AND #WORK-PRCSS-ID NE 'TFDHV'
                {
                    //*  06/05/98 GH
                    pnd_Vars_Pnd_End_Min_Page.setValue(cwf_Doc_Dgtze_Doc_End_Page);                                                                                       //Natural: MOVE CWF-DOC.DGTZE-DOC-END-PAGE TO #END-MIN-PAGE
                    //*  10/19/99 GH
                    if (condition(pnd_Vars_Pnd_Connect_Ind.notEquals("Y")))                                                                                               //Natural: IF #CONNECT-IND NE 'Y'
                    {
                        //*  PRINT IMAGE
                                                                                                                                                                          //Natural: PERFORM PRINT-IMAGE
                        sub_Print_Image();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Vars_Pnd_Mail_Item_Nbr_Save.setValue(pnd_Vars_Pnd_Mail_Item_Nbr);                                                                                 //Natural: MOVE #MAIL-ITEM-NBR TO #MAIL-ITEM-NBR-SAVE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*  CONTACT SHEET
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*        ------  GH  01/12/98 ------
                //*  PS  : CONTACT SHEET (CCLL) COMES BEFORE INOTMFA.
                //*  ICSS: INOTMFA COMES BEFORE CONTACT SHEET(CCLL).
                if (condition(cwf_Doc_Document_Category.equals("I") && cwf_Doc_Document_Sub_Category.equals("NOT") && cwf_Doc_Document_Detail.equals("MFA")))             //Natural: IF DOCUMENT-CATEGORY = 'I' AND DOCUMENT-SUB-CATEGORY = 'NOT' AND DOCUMENT-DETAIL = 'MFA'
                {
                    vw_cwf_Text.startDatabaseRead                                                                                                                         //Natural: READ ( 1 ) CWF-TEXT WITH TEXT-OBJECT-KEY = #TEXT-OBJECT-KEY
                    (
                    "READ05",
                    new Wc[] { new Wc("TEXT_OBJECT_KEY", ">=", pnd_Vars_Pnd_Text_Object_Key, WcType.BY) },
                    new Oc[] { new Oc("TEXT_OBJECT_KEY", "ASC") },
                    1
                    );
                    READ05:
                    while (condition(vw_cwf_Text.readNextRow("READ05")))
                    {
                        pnd_Vars_Pnd_Account_Nbr.setValue(cwf_Text_Text_Document_Line.getValue(1));                                                                       //Natural: MOVE TEXT-DOCUMENT-LINE ( 1 ) TO #ACCOUNT-NBR
                    }                                                                                                                                                     //Natural: END-READ
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*   GH 01/12/98
                    pnd_Vars_Pnd_Inotmfa_Found.setValue(true);                                                                                                            //Natural: MOVE TRUE TO #INOTMFA-FOUND
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*   GH 01/12/98
                    pnd_Vars_Pnd_Cs_Found.setValue(true);                                                                                                                 //Natural: MOVE TRUE TO #CS-FOUND
                    //*  GH 10/19/99
                    //*   07/23/09 GH
                    //*   GH 01/12/98
                    if (condition(pnd_Vars_Pnd_Remarks.notEquals("ReSend") && pnd_Vars_Pnd_Connect_Ind.notEquals("Y") && pnd_Vars_Pnd_Work_Prcss_Id.notEquals("TFDHV")    //Natural: IF #REMARKS NE 'ReSend' AND #CONNECT-IND NE 'Y' AND #WORK-PRCSS-ID NE 'TFDHV' AND CWF-MIT.BYPSS-MUTUAL-FUND-IND = 'P'
                        && ldaCwfl6800.getCwf_Mit_Bypss_Mutual_Fund_Ind().equals("P")))
                    {
                        //*   GH 05/13/98
                        pnd_Vars_Pnd_Text_Sqnce_Nbr_Max.reset();                                                                                                          //Natural: RESET #TEXT-SQNCE-NBR-MAX #REMAINDER
                        pnd_Vars_Pnd_Remainder.reset();
                        vw_cwf_Text.startDatabaseRead                                                                                                                     //Natural: READ CWF-TEXT WITH TEXT-OBJECT-KEY = #TEXT-OBJECT-KEY
                        (
                        "RT",
                        new Wc[] { new Wc("TEXT_OBJECT_KEY", ">=", pnd_Vars_Pnd_Text_Object_Key, WcType.BY) },
                        new Oc[] { new Oc("TEXT_OBJECT_KEY", "ASC") }
                        );
                        RT:
                        while (condition(vw_cwf_Text.readNextRow("RT")))
                        {
                            if (condition(cwf_Text_Cabinet_Id.notEquals(pnd_Vars_Pnd_Cabinet_Id) || cwf_Text_Text_Pointer.notEquals(pnd_Vars_Pnd_Text_Pointer)))          //Natural: IF CWF-TEXT.CABINET-ID NE #CABINET-ID OR CWF-TEXT.TEXT-POINTER NE #TEXT-POINTER
                            {
                                pnd_Vars_Pnd_Remainder.compute(new ComputeParameters(false, pnd_Vars_Pnd_Remainder), pnd_Vars_Pnd_Text_Sqnce_Nbr_Max.mod(2));             //Natural: DIVIDE 2 INTO #TEXT-SQNCE-NBR-MAX GIVING #PAGES REMAINDER #REMAINDER
                                pnd_Vars_Pnd_Pages.compute(new ComputeParameters(false, pnd_Vars_Pnd_Pages), pnd_Vars_Pnd_Text_Sqnce_Nbr_Max.divide(2));
                                if (condition(pnd_Vars_Pnd_Remainder.notEquals(getZero())))                                                                               //Natural: IF #REMAINDER NE 0
                                {
                                    pnd_Vars_Pnd_Pages.nadd(1);                                                                                                           //Natural: ASSIGN #PAGES := #PAGES + 1
                                }                                                                                                                                         //Natural: END-IF
                                if (condition(true)) break;                                                                                                               //Natural: ESCAPE BOTTOM IMMEDIATE
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(vw_cwf_Text.getAstCOUNTER().equals(1)))                                                                                         //Natural: IF *COUNTER ( RT. ) = 1
                            {
                                getReports().newPage(new ReportSpecification(2));                                                                                         //Natural: NEWPAGE ( 2 )
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom("RT"))) break;
                                    else if (condition(Global.isEscapeBottomImmediate("RT"))) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                getReports().newPage(new ReportSpecification(5));                                                                                         //Natural: NEWPAGE ( 5 )
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom("RT"))) break;
                                    else if (condition(Global.isEscapeBottomImmediate("RT"))) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                            }                                                                                                                                             //Natural: END-IF
                            //*   ONLY PRINT CONTACT SHEETS
                                                                                                                                                                          //Natural: PERFORM PRINT-DOC
                            sub_Print_Doc();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RT"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RT"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            //*   FOR THE PRIMARY OWNER (IF ACC. BASE)
                            if (condition(cwf_Text_Text_Sqnce_Nbr.greater(pnd_Vars_Pnd_Text_Sqnce_Nbr_Max)))                                                              //Natural: IF TEXT-SQNCE-NBR > #TEXT-SQNCE-NBR-MAX
                            {
                                pnd_Vars_Pnd_Text_Sqnce_Nbr_Max.setValue(cwf_Text_Text_Sqnce_Nbr);                                                                        //Natural: MOVE TEXT-SQNCE-NBR TO #TEXT-SQNCE-NBR-MAX
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-READ
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*   GH 01/12/98
                if (condition(pnd_Vars_Pnd_Inotmfa_Found.getBoolean() && pnd_Vars_Pnd_Cs_Found.getBoolean()))                                                             //Natural: IF #INOTMFA-FOUND AND #CS-FOUND
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*   END OF THE DOCUMENT FILE READ  */
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Print_Doc() throws Exception                                                                                                                         //Natural: PRINT-DOC
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 TO C*TEXT-DOCUMENT-LINE-GROUP
        for (pnd_Vars_Pnd_I.setValue(1); condition(pnd_Vars_Pnd_I.lessOrEqual(cwf_Text_Count_Casttext_Document_Line_Group)); pnd_Vars_Pnd_I.nadd(1))
        {
            if (condition(cwf_Text_Text_1.getValue(pnd_Vars_Pnd_I).equals("|") || cwf_Text_Text_1.getValue(pnd_Vars_Pnd_I).equals("+")))                                  //Natural: IF TEXT-1 ( #I ) = '|' OR = '+'
            {
                cwf_Text_Text_78.getValue(pnd_Vars_Pnd_I).setValue(cwf_Text_Text_1.getValue(pnd_Vars_Pnd_I));                                                             //Natural: MOVE TEXT-1 ( #I ) TO TEXT-78 ( #I )
                if (condition(cwf_Text_Text_2_And_3.getValue(pnd_Vars_Pnd_I).equals("__") || cwf_Text_Text_2_And_3.getValue(pnd_Vars_Pnd_I).equals("--")                  //Natural: IF TEXT-2-AND-3 ( #I ) = '__' OR = '--' OR = '=='
                    || cwf_Text_Text_2_And_3.getValue(pnd_Vars_Pnd_I).equals("==")))
                {
                    cwf_Text_Text_4_Thru_77.getValue(pnd_Vars_Pnd_I).moveAll(cwf_Text_Text_2_And_3.getValue(pnd_Vars_Pnd_I));                                             //Natural: MOVE ALL TEXT-2-AND-3 ( #I ) TO TEXT-4-THRU-77 ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(2, ReportOption.NOTITLE,cwf_Text_Text_Document_Line.getValue(pnd_Vars_Pnd_I));                                                             //Natural: WRITE ( 2 ) TEXT-DOCUMENT-LINE ( #I )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(5, ReportOption.NOTITLE,cwf_Text_Text_Document_Line.getValue(pnd_Vars_Pnd_I));                                                             //Natural: WRITE ( 5 ) TEXT-DOCUMENT-LINE ( #I )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(cwf_Text_Text_04.getValue(pnd_Vars_Pnd_I).equals("PAGE")))                                                                                      //Natural: IF TEXT-04 ( #I ) = 'PAGE'
            {
                getReports().newPage(new ReportSpecification(2));                                                                                                         //Natural: NEWPAGE ( 2 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().newPage(new ReportSpecification(5));                                                                                                         //Natural: NEWPAGE ( 5 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Print_Image() throws Exception                                                                                                                       //Natural: PRINT-IMAGE
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        pnd_Vars_Pnd_Source_Id.setValue(cwf_Doc_Image_Source_Id);                                                                                                         //Natural: MOVE CWF-DOC.IMAGE-SOURCE-ID TO #SOURCE-ID #SOURCE-ID-MIN-KEY1
        pnd_Vars_Pnd_Source_Id_Min_Key1.setValue(cwf_Doc_Image_Source_Id);
        pnd_Vars_Pnd_Source_Id_Min_Key2.setValue(pnd_Vars_Pnd_Mail_Item_Nbr);                                                                                             //Natural: MOVE #MAIL-ITEM-NBR TO #SOURCE-ID-MIN-KEY2
        vw_cwf_Image_Xref.startDatabaseRead                                                                                                                               //Natural: READ ( 1 ) CWF-IMAGE-XREF WITH SOURCE-ID-MIN-KEY = #SOURCE-ID-MIN-KEY
        (
        "READ06",
        new Wc[] { new Wc("SOURCE_ID_MIN_KEY", ">=", pnd_Vars_Pnd_Source_Id_Min_Key, WcType.BY) },
        new Oc[] { new Oc("SOURCE_ID_MIN_KEY", "ASC") },
        1
        );
        READ06:
        while (condition(vw_cwf_Image_Xref.readNextRow("READ06")))
        {
            if (condition(cwf_Image_Xref_Source_Id.notEquals(pnd_Vars_Pnd_Source_Id) || cwf_Image_Xref_Mail_Item_Nbr.notEquals(pnd_Vars_Pnd_Mail_Item_Nbr)))              //Natural: IF SOURCE-ID NE #SOURCE-ID OR MAIL-ITEM-NBR NE #MAIL-ITEM-NBR
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Vars_Pnd_Pages.compute(new ComputeParameters(false, pnd_Vars_Pnd_Pages), pnd_Vars_Pnd_End_Min_Page.subtract(pnd_Vars_Pnd_Start_Min_Page).add(1));         //Natural: ASSIGN #PAGES := #END-MIN-PAGE - #START-MIN-PAGE + 1
            pnd_Vars_Pnd_Image_Document_Address_Cde.setValue(cwf_Image_Xref_Image_Document_Address_Cde);                                                                  //Natural: MOVE IMAGE-DOCUMENT-ADDRESS-CDE TO #IMAGE-DOCUMENT-ADDRESS-CDE
            //*  DO NOT PRINT MORE THAN 10 IMAGE PAGES  -------        10/22/99 GH
            if (condition(cwf_Image_Xref_Image_Number_Page.greater(10)))                                                                                                  //Natural: IF IMAGE-NUMBER-PAGE > 10
            {
                pnd_Vars_Pnd_Remarks.setValue("NoPrint");                                                                                                                 //Natural: MOVE 'NoPrint' TO #REMARKS
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //* **      GH 01/17/05  FOR NDM DATA RANSFER  *** START  ***
            //*  SEQUENCE NUMBER
            //*  DUE DATE
            //*  WPID....
            //*  PIN .......
            //*  DOCUMENT-ID
            //*  START PAGE
            //*  END PAGE
            getWorkFiles().write(2, false, pnd_Vars_Pnd_Counter, pnd_Vars_Pnd_X, pnd_Vars_Pnd_Crrnt_Due_Dte_O, pnd_Vars_Pnd_X, pnd_Vars_Pnd_Work_Prcss_Id,                //Natural: WRITE WORK FILE 2 #COUNTER #X #CRRNT-DUE-DTE-O #X #WORK-PRCSS-ID #X #DOC-CABINET-NAME #X #IMAGE-DOCUMENT-ADDRESS-CDE #X #START-MIN-PAGE #X #END-MIN-PAGE
                pnd_Vars_Pnd_X, pnd_Vars_Pnd_Doc_Cabinet_Name, pnd_Vars_Pnd_X, pnd_Vars_Pnd_Image_Document_Address_Cde, pnd_Vars_Pnd_X, pnd_Vars_Pnd_Start_Min_Page, 
                pnd_Vars_Pnd_X, pnd_Vars_Pnd_End_Min_Page);
            //* **      GH 01/17/05  FOR NDM DATA RANSFER  ***  END   ***
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Contract_Check() throws Exception                                                                                                                    //Natural: CONTRACT-CHECK
    {
        if (BLNatReinput.isReinput()) return;

        FOR03:                                                                                                                                                            //Natural: FOR #CTR = 1 TO 20
        for (pnd_Ctr.setValue(1); condition(pnd_Ctr.lessOrEqual(20)); pnd_Ctr.nadd(1))
        {
            //*  DIFFERENT CONTRACT-STATUS-CODE'S
            //*  --------------------------------
            //*  C    CANCELLED/CONVERTED      
            //*  D    DEATH                    
            //*  E    EXPIRED                  
            //*  F    FINANCIAL PLAN DELIVERED 
            //*  G    INACTIVE SUNGARD CONTRACT
            //*  H    ACTIVE SUNGARD CONTRACT  
            //*  I    INVESTMENT PERFORMANCE   
            //*  L    LAPSED                   
            //*  M    MATURED                  
            //*  N    NOT ISSUED               
            //*  P    REPURCHASED              
            //*  R    REJECTION (MEDICAL)      
            //*  S    SURRENDER FOR CASH                 
            //*  T    TERMINATED/TRANSFERRED             
            //*  U    INACTIVE NON TIAA-CREF RECORDKEEPER
            //*  W    WITHDRAWN                          
            //*  Y    NON TIAA-CREF RECORDKEEPER
            if (condition(pdaMdma111.getPnd_Mdma111_Pnd_O_Contract_Number().getValue(pnd_Ctr).notEquals(" ") && DbsUtil.maskMatches(pdaMdma111.getPnd_Mdma111_Pnd_O_Contract_Number().getValue(pnd_Ctr),"'MF'...")  //Natural: IF #O-CONTRACT-NUMBER ( #CTR ) NE ' ' AND #O-CONTRACT-NUMBER ( #CTR ) EQ MASK ( 'MF'... ) AND #O-CONTRACT-STATUS-CODE ( #CTR ) EQ ' '
                && pdaMdma111.getPnd_Mdma111_Pnd_O_Contract_Status_Code().getValue(pnd_Ctr).equals(" ")))
            {
                //*      (#O-CONTRACT-STATUS-CODE (#CTR) EQ ' ' OR
                //*      #O-CONTRACT-STATUS-CODE  (#CTR) EQ 'H' OR
                //*      #O-CONTRACT-STATUS-CODE  (#CTR) EQ 'Y')
                pnd_Vars_Pnd_Account_Nbr_N.setValue(pdaMdma111.getPnd_Mdma111_Pnd_O_Contract_Number().getValue(pnd_Ctr));                                                 //Natural: MOVE #O-CONTRACT-NUMBER ( #CTR ) TO #ACCOUNT-NBR-N
                pnd_Valid_Contracts.nadd(1);                                                                                                                              //Natural: ADD 1 TO #VALID-CONTRACTS
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Log_Msg() throws Exception                                                                                                                           //Natural: LOG-MSG
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************** *
        //*  OBJECT..: ERLC1000
        //*  FUNCTION: INVOKE INFRASTRUCTURE MODULE TO STORE ERROR DETAILS TO
        //*            ERROR-HANDLER FILE.
        //*  MODIFICATION LOG:
        //*  =================
        //* ********************************************
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        pdaErla1000.getErla1000_Err_Pgm_Name().setValue(Global.getPROGRAM());                                                                                             //Natural: ASSIGN ERLA1000.ERR-PGM-NAME = *PROGRAM
        pdaErla1000.getErla1000_Err_Pgm_Level().setValue(Global.getLEVEL());                                                                                              //Natural: ASSIGN ERLA1000.ERR-PGM-LEVEL = *LEVEL
        pdaErla1000.getErla1000_Err_Nbr().setValue(Global.getERROR_NR());                                                                                                 //Natural: ASSIGN ERLA1000.ERR-NBR = *ERROR-NR
        pdaErla1000.getErla1000_Err_Line_Nbr().setValue(Global.getERROR_LINE());                                                                                          //Natural: ASSIGN ERLA1000.ERR-LINE-NBR = *ERROR-LINE
        pdaErla1000.getErla1000_Err_Status_Cde().setValue("U");                                                                                                           //Natural: ASSIGN ERLA1000.ERR-STATUS-CDE = 'U'
        pdaErla1000.getErla1000_Err_Type_Cde().setValue("N");                                                                                                             //Natural: ASSIGN ERLA1000.ERR-TYPE-CDE = 'N'
        pdaErla1000.getErla1000_Err_Notes().setValue(DbsUtil.compress(pnd_Vars_Pnd_Store_Msg.getValue(1)));                                                               //Natural: COMPRESS #STORE-MSG ( 1 ) INTO ERLA1000.ERR-NOTES
        DbsUtil.callnat(Erln1000.class , getCurrentProcessState(), pdaErla1000.getErla1000());                                                                            //Natural: CALLNAT 'ERLN1000' ERLA1000
        if (condition(Global.isEscape())) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, new TabSetting(33),"Shared Service Request Transmittal List",NEWLINE,new TabSetting(4),"-Due-Date--  WPID-  ----Request----  Account-No","---SSN--- --------NAME-------- Remarks  Pgs",NEWLINE,new  //Natural: WRITE ( 1 ) 33T 'Shared Service Request Transmittal List' / 4T '-Due-Date--  WPID-  ----Request----  Account-No' '---SSN--- --------NAME-------- Remarks  Pgs' / 4T '-----------  -----  ---------------  ----------' '--------- -------------------- -------  ---' //
                        TabSetting(4),"-----------  -----  ---------------  ----------","--------- -------------------- -------  ---",NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,pnd_Vars_Pnd_Counter,new ColumnSpacing(5),pnd_Vars_Pnd_Crrnt_Due_Dte_O,new              //Natural: WRITE ( 2 ) NOTITLE NOHDR #COUNTER 5X #CRRNT-DUE-DTE-O 5X #WORK-PRCSS-ID 5X #WORK-PRCSS-SHORT-NME / '+============================================================================+' /
                        ColumnSpacing(5),pnd_Vars_Pnd_Work_Prcss_Id,new ColumnSpacing(5),pnd_Vars_Pnd_Work_Prcss_Short_Nme,NEWLINE,"+============================================================================+",
                        NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt4 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(4, new TabSetting(33),"Shared Service Request Transmittal List",NEWLINE,new TabSetting(4),"-Due-Date--  WPID-  ----Request----  Account-No","---SSN--- --------NAME-------- Remarks  Pgs",NEWLINE,new  //Natural: WRITE ( 4 ) 33T 'Shared Service Request Transmittal List' / 4T '-Due-Date--  WPID-  ----Request----  Account-No' '---SSN--- --------NAME-------- Remarks  Pgs' / 4T '-----------  -----  ---------------  ----------' '--------- -------------------- -------  ---' //
                        TabSetting(4),"-----------  -----  ---------------  ----------","--------- -------------------- -------  ---",NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt5 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(5, ReportOption.NOTITLE,ReportOption.NOHDR,pnd_Vars_Pnd_Counter,new ColumnSpacing(5),pnd_Vars_Pnd_Crrnt_Due_Dte_O,new              //Natural: WRITE ( 5 ) NOTITLE NOHDR #COUNTER 5X #CRRNT-DUE-DTE-O 5X #WORK-PRCSS-ID 5X #WORK-PRCSS-SHORT-NME / '+============================================================================+' /
                        ColumnSpacing(5),pnd_Vars_Pnd_Work_Prcss_Id,new ColumnSpacing(5),pnd_Vars_Pnd_Work_Prcss_Short_Nme,NEWLINE,"+============================================================================+",
                        NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        msg_Info_Pnd_Pnd_Msg.setValue(DbsUtil.compress("Natural Error", Global.getERROR_NR(), "on line", Global.getERROR_LINE(), "of", Global.getPROGRAM()));             //Natural: COMPRESS 'Natural Error' *ERROR-NR 'on line' *ERROR-LINE 'of' *PROGRAM INTO MSG-INFO.##MSG
        getReports().write(3, msg_Info_Pnd_Pnd_Msg);                                                                                                                      //Natural: WRITE ( 3 ) MSG-INFO.##MSG
                                                                                                                                                                          //Natural: PERFORM LOG-MSG
        sub_Log_Msg();
        if (condition(Global.isEscape())) {return;}
        DbsUtil.terminate(4);  if (true) return;                                                                                                                          //Natural: TERMINATE 4
    };                                                                                                                                                                    //Natural: END-ERROR

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean pnd_Vars_Pnd_Type_Of_DocumentIsBreak = pnd_Vars_Pnd_Type_Of_Document.isBreak(endOfData);
        if (condition(pnd_Vars_Pnd_Type_Of_DocumentIsBreak))
        {
            if (condition(sort01Pnd_Type_Of_DocumentOld.equals("C")))                                                                                                     //Natural: IF OLD ( #TYPE-OF-DOCUMENT ) = 'C'
            {
                pnd_Vars_Pnd_Total_Line.setValue("Total Participants with Contact Sheets :");                                                                             //Natural: MOVE 'Total Participants with Contact Sheets :' TO #TOTAL-LINE
                pnd_Vars_Pnd_Counter_C.setValue(pnd_Vars_Pnd_Counter_Send);                                                                                               //Natural: MOVE #COUNTER-SEND TO #COUNTER-C
                //*      MOVE OLD(#COUNTER)  TO #COUNTER-C
                pnd_Vars_Pnd_Total_Pages_C.setValue(pnd_Vars_Pnd_Total_Pages);                                                                                            //Natural: MOVE #TOTAL-PAGES TO #TOTAL-PAGES-C
                //*  VR 03/25/2016 START
                pnd_Vars_Pnd_Counter_C_N.setValue(pnd_Vars_Pnd_Counter_Send_N);                                                                                           //Natural: MOVE #COUNTER-SEND-N TO #COUNTER-C-N
                pnd_Vars_Pnd_Total_Pages_C_N.setValue(pnd_Vars_Pnd_Total_Pages_N);                                                                                        //Natural: MOVE #TOTAL-PAGES-N TO #TOTAL-PAGES-C-N
                //*  VR 03/25/2016 END
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Vars_Pnd_Total_Line.setValue("Total Participants with Images ........:");                                                                             //Natural: MOVE 'Total Participants with Images ........:' TO #TOTAL-LINE
                pnd_Vars_Pnd_Counter_I.setValue(pnd_Vars_Pnd_Counter_Send);                                                                                               //Natural: MOVE #COUNTER-SEND TO #COUNTER-I
                //*      MOVE OLD(#COUNTER)  TO #COUNTER-I
                pnd_Vars_Pnd_Total_Pages_I.setValue(pnd_Vars_Pnd_Total_Pages);                                                                                            //Natural: MOVE #TOTAL-PAGES TO #TOTAL-PAGES-I
                //*  VR 03/25/2016 START
                pnd_Vars_Pnd_Counter_I_N.setValue(pnd_Vars_Pnd_Counter_Send_N);                                                                                           //Natural: MOVE #COUNTER-SEND-N TO #COUNTER-I-N
                pnd_Vars_Pnd_Total_Pages_I_N.setValue(pnd_Vars_Pnd_Total_Pages_N);                                                                                        //Natural: MOVE #TOTAL-PAGES-N TO #TOTAL-PAGES-I-N
                //*  VR 03/25/2016 END
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(1, NEWLINE,pnd_Vars_Pnd_Total_Line,new ColumnSpacing(2),pnd_Vars_Pnd_Counter_Send,new TabSetting(71),"Total Pages Send : ",                //Natural: WRITE ( 1 ) / #TOTAL-LINE 2X #COUNTER-SEND 71T 'Total Pages Send : ' #TOTAL-PAGES /
                pnd_Vars_Pnd_Total_Pages,NEWLINE);
            if (condition(Global.isEscape())) return;
            //*  VR 03/25/2016 START
            //*    WRITE(4) /
            //*      #TOTAL-LINE 2X #COUNTER-SEND
            //*      71T 'Total Pages Send : ' #TOTAL-PAGES   /
            getReports().write(4, NEWLINE,pnd_Vars_Pnd_Total_Line,new ColumnSpacing(2),pnd_Vars_Pnd_Counter_Send_N,new TabSetting(71),"Total Pages Send : ",              //Natural: WRITE ( 4 ) / #TOTAL-LINE 2X #COUNTER-SEND-N 71T 'Total Pages Send : ' #TOTAL-PAGES-N /
                pnd_Vars_Pnd_Total_Pages_N,NEWLINE);
            if (condition(Global.isEscape())) return;
            //*  VR 03/25/2016 END
            //*  VR 03/25/2016
            pnd_Vars_Pnd_Counter.reset();                                                                                                                                 //Natural: RESET #COUNTER #COUNTER-SEND #TOTAL-PAGES #COUNTER-SEND-N #TOTAL-PAGES-N
            pnd_Vars_Pnd_Counter_Send.reset();
            pnd_Vars_Pnd_Total_Pages.reset();
            pnd_Vars_Pnd_Counter_Send_N.reset();
            pnd_Vars_Pnd_Total_Pages_N.reset();
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=60 SG=OFF");
        Global.format(1, "LS=132 PS=60 SG=OFF");
        Global.format(2, "LS=132 PS=60 SG=OFF");
        Global.format(3, "LS=132 PS=60 SG=OFF");
        Global.format(4, "LS=132 PS=60 SG=OFF");
        Global.format(5, "LS=132 PS=60 SG=OFF");
    }
}
