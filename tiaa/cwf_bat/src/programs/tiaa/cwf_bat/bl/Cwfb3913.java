/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:31:00 PM
**        * FROM NATURAL PROGRAM : Cwfb3913
************************************************************
**        * FILE NAME            : Cwfb3913.java
**        * CLASS NAME           : Cwfb3913
**        * INSTANCE NAME        : Cwfb3913
************************************************************
************************************************************************
* PROGRAM  : CWFB3913
* SYSTEM   : CRPCWF
* TITLE    : STANDARD WEEKLY STREAM END OF BATCH PROGRAM
* GENERATED: OCT 05,93 AT 08:41 AM
* FUNCTION : THIS WILL BE PLACED IN A STEP IN THE
*          | STREAM WHICH WILL BE EXECUTED WHEN AT LEAST ONE
*          | OF THE STEPS EXECUTED SUCCESSFULLY
*          |
*          |
*          |
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* MMM DD YY  ________ ______________________________________________
* ______________________________________________
************************************************************************
*
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3913 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_1;
    private DbsField cwf_Support_Tbl_Tbl_Last_Run_Date;
    private DbsField cwf_Support_Tbl_Tbl_Last_Run_Flag;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Dlte_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Table_Rectype;

    private DataAccessProgramView vw_rep_Run_Table;
    private DbsField rep_Run_Table_Tbl_Scrty_Level_Ind;
    private DbsField rep_Run_Table_Tbl_Table_Nme;
    private DbsField rep_Run_Table_Tbl_Key_Field;
    private DbsField rep_Run_Table_Tbl_Data_Field;

    private DbsGroup rep_Run_Table__R_Field_2;
    private DbsField rep_Run_Table_Tbl_Run_Date;
    private DbsField rep_Run_Table_Tbl_Run_Flag;
    private DbsField rep_Run_Table_Tbl_Updte_Oprtr_Cde;
    private DbsField rep_Run_Table_Tbl_Dlte_Dte_Tme;
    private DbsField rep_Run_Table_Tbl_Dlte_Oprtr_Cde;
    private DbsField rep_Run_Table_Tbl_Table_Rectype;

    private DataAccessProgramView vw_cwf_Support_Tbl1;
    private DbsField cwf_Support_Tbl1_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl1_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl1_Tbl_Key_Field;

    private DbsGroup cwf_Support_Tbl1__R_Field_3;
    private DbsField cwf_Support_Tbl1_Tbl_Report_Name;
    private DbsField cwf_Support_Tbl1_Tbl_Report_Freq;
    private DbsField cwf_Support_Tbl1_Tbl_Data_Field;

    private DbsGroup cwf_Support_Tbl1__R_Field_4;
    private DbsField cwf_Support_Tbl1_Deliver_To;
    private DbsField cwf_Support_Tbl1_Requesting_Unit;
    private DbsField cwf_Support_Tbl1_Floor;
    private DbsField cwf_Support_Tbl1_Bldg;
    private DbsField cwf_Support_Tbl1_Drop_Off;
    private DbsField cwf_Support_Tbl1_Tbl_Run_Date;
    private DbsField cwf_Support_Tbl1_Tbl_Run_Flag;
    private DbsField cwf_Support_Tbl1_Tbl_Updte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl1_Tbl_Dlte_Dte_Tme;
    private DbsField cwf_Support_Tbl1_Tbl_Dlte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl1_Tbl_Table_Rectype;
    private DbsField pnd_Tbl_Key;

    private DbsGroup pnd_Tbl_Key__R_Field_5;

    private DbsGroup pnd_Tbl_Key_Data_Nme;
    private DbsField pnd_Tbl_Key_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Key_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Key_Tbl_Key_Field;

    private DbsGroup pnd_Tbl_Prime_Key;
    private DbsField pnd_Tbl_Prime_Key_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Tbl_Key_Field;
    private DbsField pnd_Tbl_Ky;

    private DbsGroup pnd_Tbl_Ky__R_Field_6;

    private DbsGroup pnd_Tbl_Ky_Data_Nme;
    private DbsField pnd_Tbl_Ky_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Ky_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Ky_Tbl_Key_Field;

    private DbsGroup pnd_Tbl_Prime_Ky;
    private DbsField pnd_Tbl_Prime_Ky_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Ky_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Ky_Tbl_Key_Field;

    private DbsGroup pnd_Tbl_Prime_Key1;
    private DbsField pnd_Tbl_Prime_Key1_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key1_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key1_Tbl_Key_Field;
    private DbsField pnd_Work_Date;
    private DbsField pnd_Msg;
    private DbsField pnd_Sub;
    private DbsField pnd_Report_Parm;
    private DbsField pnd_Racf_Id;
    private DbsField pnd_Parm_Unit;
    private DbsField pnd_Parm_Floor;
    private DbsField pnd_Parm_Bldg;
    private DbsField pnd_Parm_Drop_Off;
    private DbsField pnd_Tbl_Run_Date;
    private DbsField pnd_Tbl_Run_Flag;
    private DbsField pnd_Comp_Date;
    private DbsField pnd_Prog_Count;
    private DbsField pnd_Report_No;
    private DbsField pnd_Bldg;
    private DbsField pnd_Empl_Racf_Id;
    private DbsField pnd_Empl_Unit_Cde;
    private DbsField pnd_Deliver_To;
    private DbsField pnd_Drop_Off;
    private DbsField pnd_Env;
    private DbsField pnd_Floor;
    private DbsField pnd_Record;
    private DbsField pnd_Report_Name;
    private DbsField pnd_Run_Date;
    private DbsField pnd_Unit;
    private DbsField pnd_Parm_Type;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Support_Tbl__R_Field_1 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_1", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Tbl_Last_Run_Date = cwf_Support_Tbl__R_Field_1.newFieldInGroup("cwf_Support_Tbl_Tbl_Last_Run_Date", "TBL-LAST-RUN-DATE", FieldType.STRING, 
            8);
        cwf_Support_Tbl_Tbl_Last_Run_Flag = cwf_Support_Tbl__R_Field_1.newFieldInGroup("cwf_Support_Tbl_Tbl_Last_Run_Flag", "TBL-LAST-RUN-FLAG", FieldType.STRING, 
            1);
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        cwf_Support_Tbl_Tbl_Dlte_Dte_Tme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Dlte_Dte_Tme", "TBL-DLTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_DLTE_DTE_TME");
        cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde", "TBL-DLTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_DLTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Table_Rectype = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Rectype", "TBL-TABLE-RECTYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TBL_TABLE_RECTYPE");
        registerRecord(vw_cwf_Support_Tbl);

        vw_rep_Run_Table = new DataAccessProgramView(new NameInfo("vw_rep_Run_Table", "REP-RUN-TABLE"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        rep_Run_Table_Tbl_Scrty_Level_Ind = vw_rep_Run_Table.getRecord().newFieldInGroup("rep_Run_Table_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        rep_Run_Table_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        rep_Run_Table_Tbl_Table_Nme = vw_rep_Run_Table.getRecord().newFieldInGroup("rep_Run_Table_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 20, 
            RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        rep_Run_Table_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        rep_Run_Table_Tbl_Key_Field = vw_rep_Run_Table.getRecord().newFieldInGroup("rep_Run_Table_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 30, 
            RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        rep_Run_Table_Tbl_Data_Field = vw_rep_Run_Table.getRecord().newFieldInGroup("rep_Run_Table_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        rep_Run_Table__R_Field_2 = vw_rep_Run_Table.getRecord().newGroupInGroup("rep_Run_Table__R_Field_2", "REDEFINE", rep_Run_Table_Tbl_Data_Field);
        rep_Run_Table_Tbl_Run_Date = rep_Run_Table__R_Field_2.newFieldInGroup("rep_Run_Table_Tbl_Run_Date", "TBL-RUN-DATE", FieldType.STRING, 8);
        rep_Run_Table_Tbl_Run_Flag = rep_Run_Table__R_Field_2.newFieldInGroup("rep_Run_Table_Tbl_Run_Flag", "TBL-RUN-FLAG", FieldType.STRING, 1);
        rep_Run_Table_Tbl_Updte_Oprtr_Cde = vw_rep_Run_Table.getRecord().newFieldInGroup("rep_Run_Table_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        rep_Run_Table_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        rep_Run_Table_Tbl_Dlte_Dte_Tme = vw_rep_Run_Table.getRecord().newFieldInGroup("rep_Run_Table_Tbl_Dlte_Dte_Tme", "TBL-DLTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_DLTE_DTE_TME");
        rep_Run_Table_Tbl_Dlte_Oprtr_Cde = vw_rep_Run_Table.getRecord().newFieldInGroup("rep_Run_Table_Tbl_Dlte_Oprtr_Cde", "TBL-DLTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TBL_DLTE_OPRTR_CDE");
        rep_Run_Table_Tbl_Table_Rectype = vw_rep_Run_Table.getRecord().newFieldInGroup("rep_Run_Table_Tbl_Table_Rectype", "TBL-TABLE-RECTYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TBL_TABLE_RECTYPE");
        registerRecord(vw_rep_Run_Table);

        vw_cwf_Support_Tbl1 = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl1", "CWF-SUPPORT-TBL1"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl1_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl1.getRecord().newFieldInGroup("cwf_Support_Tbl1_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl1_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl1_Tbl_Table_Nme = vw_cwf_Support_Tbl1.getRecord().newFieldInGroup("cwf_Support_Tbl1_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl1_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl1_Tbl_Key_Field = vw_cwf_Support_Tbl1.getRecord().newFieldInGroup("cwf_Support_Tbl1_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");

        cwf_Support_Tbl1__R_Field_3 = vw_cwf_Support_Tbl1.getRecord().newGroupInGroup("cwf_Support_Tbl1__R_Field_3", "REDEFINE", cwf_Support_Tbl1_Tbl_Key_Field);
        cwf_Support_Tbl1_Tbl_Report_Name = cwf_Support_Tbl1__R_Field_3.newFieldInGroup("cwf_Support_Tbl1_Tbl_Report_Name", "TBL-REPORT-NAME", FieldType.STRING, 
            8);
        cwf_Support_Tbl1_Tbl_Report_Freq = cwf_Support_Tbl1__R_Field_3.newFieldInGroup("cwf_Support_Tbl1_Tbl_Report_Freq", "TBL-REPORT-FREQ", FieldType.STRING, 
            1);
        cwf_Support_Tbl1_Tbl_Data_Field = vw_cwf_Support_Tbl1.getRecord().newFieldInGroup("cwf_Support_Tbl1_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Support_Tbl1__R_Field_4 = vw_cwf_Support_Tbl1.getRecord().newGroupInGroup("cwf_Support_Tbl1__R_Field_4", "REDEFINE", cwf_Support_Tbl1_Tbl_Data_Field);
        cwf_Support_Tbl1_Deliver_To = cwf_Support_Tbl1__R_Field_4.newFieldInGroup("cwf_Support_Tbl1_Deliver_To", "DELIVER-TO", FieldType.STRING, 8);
        cwf_Support_Tbl1_Requesting_Unit = cwf_Support_Tbl1__R_Field_4.newFieldInGroup("cwf_Support_Tbl1_Requesting_Unit", "REQUESTING-UNIT", FieldType.STRING, 
            7);
        cwf_Support_Tbl1_Floor = cwf_Support_Tbl1__R_Field_4.newFieldInGroup("cwf_Support_Tbl1_Floor", "FLOOR", FieldType.NUMERIC, 2);
        cwf_Support_Tbl1_Bldg = cwf_Support_Tbl1__R_Field_4.newFieldInGroup("cwf_Support_Tbl1_Bldg", "BLDG", FieldType.STRING, 3);
        cwf_Support_Tbl1_Drop_Off = cwf_Support_Tbl1__R_Field_4.newFieldInGroup("cwf_Support_Tbl1_Drop_Off", "DROP-OFF", FieldType.STRING, 2);
        cwf_Support_Tbl1_Tbl_Run_Date = cwf_Support_Tbl1__R_Field_4.newFieldInGroup("cwf_Support_Tbl1_Tbl_Run_Date", "TBL-RUN-DATE", FieldType.STRING, 
            8);
        cwf_Support_Tbl1_Tbl_Run_Flag = cwf_Support_Tbl1__R_Field_4.newFieldInGroup("cwf_Support_Tbl1_Tbl_Run_Flag", "TBL-RUN-FLAG", FieldType.STRING, 
            1);
        cwf_Support_Tbl1_Tbl_Updte_Oprtr_Cde = vw_cwf_Support_Tbl1.getRecord().newFieldInGroup("cwf_Support_Tbl1_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Support_Tbl1_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        cwf_Support_Tbl1_Tbl_Dlte_Dte_Tme = vw_cwf_Support_Tbl1.getRecord().newFieldInGroup("cwf_Support_Tbl1_Tbl_Dlte_Dte_Tme", "TBL-DLTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_DLTE_DTE_TME");
        cwf_Support_Tbl1_Tbl_Dlte_Oprtr_Cde = vw_cwf_Support_Tbl1.getRecord().newFieldInGroup("cwf_Support_Tbl1_Tbl_Dlte_Oprtr_Cde", "TBL-DLTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_DLTE_OPRTR_CDE");
        cwf_Support_Tbl1_Tbl_Table_Rectype = vw_cwf_Support_Tbl1.getRecord().newFieldInGroup("cwf_Support_Tbl1_Tbl_Table_Rectype", "TBL-TABLE-RECTYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TBL_TABLE_RECTYPE");
        registerRecord(vw_cwf_Support_Tbl1);

        pnd_Tbl_Key = localVariables.newFieldInRecord("pnd_Tbl_Key", "#TBL-KEY", FieldType.STRING, 53);

        pnd_Tbl_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Tbl_Key__R_Field_5", "REDEFINE", pnd_Tbl_Key);

        pnd_Tbl_Key_Data_Nme = pnd_Tbl_Key__R_Field_5.newGroupInGroup("pnd_Tbl_Key_Data_Nme", "DATA-NME");
        pnd_Tbl_Key_Tbl_Scrty_Level_Ind = pnd_Tbl_Key_Data_Nme.newFieldInGroup("pnd_Tbl_Key_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", FieldType.STRING, 
            2);
        pnd_Tbl_Key_Tbl_Table_Nme = pnd_Tbl_Key_Data_Nme.newFieldInGroup("pnd_Tbl_Key_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 20);
        pnd_Tbl_Key_Tbl_Key_Field = pnd_Tbl_Key_Data_Nme.newFieldInGroup("pnd_Tbl_Key_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 30);

        pnd_Tbl_Prime_Key = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY");
        pnd_Tbl_Prime_Key_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key.newFieldInGroup("pnd_Tbl_Prime_Key_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", FieldType.STRING, 
            2);
        pnd_Tbl_Prime_Key_Tbl_Table_Nme = pnd_Tbl_Prime_Key.newFieldInGroup("pnd_Tbl_Prime_Key_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 20);
        pnd_Tbl_Prime_Key_Tbl_Key_Field = pnd_Tbl_Prime_Key.newFieldInGroup("pnd_Tbl_Prime_Key_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 30);
        pnd_Tbl_Ky = localVariables.newFieldInRecord("pnd_Tbl_Ky", "#TBL-KY", FieldType.STRING, 53);

        pnd_Tbl_Ky__R_Field_6 = localVariables.newGroupInRecord("pnd_Tbl_Ky__R_Field_6", "REDEFINE", pnd_Tbl_Ky);

        pnd_Tbl_Ky_Data_Nme = pnd_Tbl_Ky__R_Field_6.newGroupInGroup("pnd_Tbl_Ky_Data_Nme", "DATA-NME");
        pnd_Tbl_Ky_Tbl_Scrty_Level_Ind = pnd_Tbl_Ky_Data_Nme.newFieldInGroup("pnd_Tbl_Ky_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", FieldType.STRING, 
            2);
        pnd_Tbl_Ky_Tbl_Table_Nme = pnd_Tbl_Ky_Data_Nme.newFieldInGroup("pnd_Tbl_Ky_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 20);
        pnd_Tbl_Ky_Tbl_Key_Field = pnd_Tbl_Ky_Data_Nme.newFieldInGroup("pnd_Tbl_Ky_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 30);

        pnd_Tbl_Prime_Ky = localVariables.newGroupInRecord("pnd_Tbl_Prime_Ky", "#TBL-PRIME-KY");
        pnd_Tbl_Prime_Ky_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Ky.newFieldInGroup("pnd_Tbl_Prime_Ky_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", FieldType.STRING, 
            2);
        pnd_Tbl_Prime_Ky_Tbl_Table_Nme = pnd_Tbl_Prime_Ky.newFieldInGroup("pnd_Tbl_Prime_Ky_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 20);
        pnd_Tbl_Prime_Ky_Tbl_Key_Field = pnd_Tbl_Prime_Ky.newFieldInGroup("pnd_Tbl_Prime_Ky_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 30);

        pnd_Tbl_Prime_Key1 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key1", "#TBL-PRIME-KEY1");
        pnd_Tbl_Prime_Key1_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key1.newFieldInGroup("pnd_Tbl_Prime_Key1_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", FieldType.STRING, 
            2);
        pnd_Tbl_Prime_Key1_Tbl_Table_Nme = pnd_Tbl_Prime_Key1.newFieldInGroup("pnd_Tbl_Prime_Key1_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 20);
        pnd_Tbl_Prime_Key1_Tbl_Key_Field = pnd_Tbl_Prime_Key1.newFieldInGroup("pnd_Tbl_Prime_Key1_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 30);
        pnd_Work_Date = localVariables.newFieldInRecord("pnd_Work_Date", "#WORK-DATE", FieldType.DATE);
        pnd_Msg = localVariables.newFieldInRecord("pnd_Msg", "#MSG", FieldType.STRING, 79);
        pnd_Sub = localVariables.newFieldInRecord("pnd_Sub", "#SUB", FieldType.PACKED_DECIMAL, 4);
        pnd_Report_Parm = localVariables.newFieldInRecord("pnd_Report_Parm", "#REPORT-PARM", FieldType.STRING, 17);
        pnd_Racf_Id = localVariables.newFieldInRecord("pnd_Racf_Id", "#RACF-ID", FieldType.STRING, 8);
        pnd_Parm_Unit = localVariables.newFieldInRecord("pnd_Parm_Unit", "#PARM-UNIT", FieldType.STRING, 7);
        pnd_Parm_Floor = localVariables.newFieldInRecord("pnd_Parm_Floor", "#PARM-FLOOR", FieldType.NUMERIC, 2);
        pnd_Parm_Bldg = localVariables.newFieldInRecord("pnd_Parm_Bldg", "#PARM-BLDG", FieldType.STRING, 3);
        pnd_Parm_Drop_Off = localVariables.newFieldInRecord("pnd_Parm_Drop_Off", "#PARM-DROP-OFF", FieldType.STRING, 2);
        pnd_Tbl_Run_Date = localVariables.newFieldInRecord("pnd_Tbl_Run_Date", "#TBL-RUN-DATE", FieldType.STRING, 8);
        pnd_Tbl_Run_Flag = localVariables.newFieldInRecord("pnd_Tbl_Run_Flag", "#TBL-RUN-FLAG", FieldType.STRING, 1);
        pnd_Comp_Date = localVariables.newFieldInRecord("pnd_Comp_Date", "#COMP-DATE", FieldType.STRING, 8);
        pnd_Prog_Count = localVariables.newFieldInRecord("pnd_Prog_Count", "#PROG-COUNT", FieldType.NUMERIC, 1);
        pnd_Report_No = localVariables.newFieldInRecord("pnd_Report_No", "#REPORT-NO", FieldType.NUMERIC, 2);
        pnd_Bldg = localVariables.newFieldInRecord("pnd_Bldg", "#BLDG", FieldType.STRING, 3);
        pnd_Empl_Racf_Id = localVariables.newFieldInRecord("pnd_Empl_Racf_Id", "#EMPL-RACF-ID", FieldType.STRING, 8);
        pnd_Empl_Unit_Cde = localVariables.newFieldInRecord("pnd_Empl_Unit_Cde", "#EMPL-UNIT-CDE", FieldType.STRING, 8);
        pnd_Deliver_To = localVariables.newFieldInRecord("pnd_Deliver_To", "#DELIVER-TO", FieldType.STRING, 45);
        pnd_Drop_Off = localVariables.newFieldInRecord("pnd_Drop_Off", "#DROP-OFF", FieldType.STRING, 2);
        pnd_Env = localVariables.newFieldInRecord("pnd_Env", "#ENV", FieldType.STRING, 10);
        pnd_Floor = localVariables.newFieldInRecord("pnd_Floor", "#FLOOR", FieldType.NUMERIC, 2);
        pnd_Record = localVariables.newFieldInRecord("pnd_Record", "#RECORD", FieldType.STRING, 54);
        pnd_Report_Name = localVariables.newFieldInRecord("pnd_Report_Name", "#REPORT-NAME", FieldType.STRING, 45);
        pnd_Run_Date = localVariables.newFieldInRecord("pnd_Run_Date", "#RUN-DATE", FieldType.DATE);
        pnd_Unit = localVariables.newFieldInRecord("pnd_Unit", "#UNIT", FieldType.STRING, 45);
        pnd_Parm_Type = localVariables.newFieldInRecord("pnd_Parm_Type", "#PARM-TYPE", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Support_Tbl.reset();
        vw_rep_Run_Table.reset();
        vw_cwf_Support_Tbl1.reset();

        localVariables.reset();
        pnd_Tbl_Prime_Key_Tbl_Scrty_Level_Ind.setInitialValue("A");
        pnd_Tbl_Prime_Key_Tbl_Table_Nme.setInitialValue("CWF-RPRT-RUN-TBL");
        pnd_Tbl_Prime_Key_Tbl_Key_Field.setInitialValue("W");
        pnd_Tbl_Prime_Ky_Tbl_Scrty_Level_Ind.setInitialValue("A");
        pnd_Tbl_Prime_Ky_Tbl_Table_Nme.setInitialValue("CWF-RPRT-RUN-TBL");
        pnd_Tbl_Prime_Ky_Tbl_Key_Field.setInitialValue("W");
        pnd_Tbl_Prime_Key1_Tbl_Scrty_Level_Ind.setInitialValue("A");
        pnd_Tbl_Prime_Key1_Tbl_Table_Nme.setInitialValue("CWF-STNDRD-RPRT-TBL");
        pnd_Tbl_Prime_Key1_Tbl_Key_Field.setInitialValue("CWF");
        pnd_Report_No.setInitialValue(25);
        pnd_Bldg.setInitialValue("485");
        pnd_Empl_Unit_Cde.setInitialValue("CWF");
        pnd_Deliver_To.setInitialValue("W. Paisner EXT. 4970 / J. Patingo EXT. 6437");
        pnd_Floor.setInitialValue(3);
        pnd_Report_Name.setInitialValue("CWF WEEKLY STREAM END STATISTICS");
        pnd_Unit.setInitialValue("CWF Systems");
        pnd_Parm_Type.setInitialValue("W");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb3913() throws Exception
    {
        super("Cwfb3913");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        //* *********************************
        //*  COPYCODE : CWFC3000
        //*  FUNCTION : SETUP ENVONMENT MESSAGE
        //*  AUTHOR   : PATINGO, JOSEPH S.
        //* *********************************
        pnd_Env.setValue(Global.getLIBRARY_ID());                                                                                                                         //Natural: MOVE *LIBRARY-ID TO #ENV
        if (condition(pnd_Env.equals("PROJCWF") || pnd_Env.equals("PROJCWF")))                                                                                            //Natural: IF #ENV = 'PROJCWF' OR #ENV = 'PROJCWF'
        {
            pnd_Env.setValue("DEV'T ");                                                                                                                                   //Natural: MOVE 'DEV"T ' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("PRACANN")))                                                                                                                         //Natural: IF #ENV = 'PRACANN'
        {
            pnd_Env.setValue("PRACTICE");                                                                                                                                 //Natural: MOVE 'PRACTICE' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("PRODANN")))                                                                                                                         //Natural: IF #ENV = 'PRODANN'
        {
            pnd_Env.setValue(" ");                                                                                                                                        //Natural: MOVE ' ' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT034")))                                                                                                                         //Natural: IF #ENV = 'ACPT034'
        {
            pnd_Env.setValue("RGN:AT07");                                                                                                                                 //Natural: MOVE 'RGN:AT07' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT023")))                                                                                                                         //Natural: IF #ENV = 'ACPT023'
        {
            pnd_Env.setValue("RGN:AT06");                                                                                                                                 //Natural: MOVE 'RGN:AT06' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT028")))                                                                                                                         //Natural: IF #ENV = 'ACPT028'
        {
            pnd_Env.setValue("RGN:AT05");                                                                                                                                 //Natural: MOVE 'RGN:AT05' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        //* ****** END COPYCODE *************
        if (condition(Global.getLIBRARY_ID().notEquals("PRODANN")))                                                                                                       //Natural: IF *LIBRARY-ID NE 'PRODANN'
        {
            pnd_Env.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "(", pnd_Env, ")"));                                                                         //Natural: COMPRESS '(' #ENV ')' INTO #ENV LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Cwfn3912.class , getCurrentProcessState(), pnd_Comp_Date, pnd_Parm_Type);                                                                         //Natural: CALLNAT 'CWFN3912' #COMP-DATE #PARM-TYPE
        if (condition(Global.isEscape())) return;
        pnd_Run_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Comp_Date);                                                                                        //Natural: MOVE EDITED #COMP-DATE TO #RUN-DATE ( EM = YYYYMMDD )
        pnd_Prog_Count.reset();                                                                                                                                           //Natural: RESET #PROG-COUNT
        DbsUtil.callnat(Cwfn3910.class , getCurrentProcessState(), pnd_Report_No, pnd_Empl_Racf_Id, pnd_Empl_Unit_Cde, pnd_Floor, pnd_Bldg, pnd_Drop_Off,                 //Natural: CALLNAT 'CWFN3910' #REPORT-NO #EMPL-RACF-ID #EMPL-UNIT-CDE #FLOOR #BLDG #DROP-OFF #COMP-DATE
            pnd_Comp_Date);
        if (condition(Global.isEscape())) return;
        getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),pnd_Env,new TabSetting(26),"CORPORATE WORKFLOW FACILITIES",new TabSetting(72),"PAGE",getReports().getPageNumberDbs(1),  //Natural: WRITE ( 1 ) NOTITLE *PROGRAM #ENV 26T 'CORPORATE WORKFLOW FACILITIES' 72T 'PAGE' *PAGE-NUMBER ( 1 ) ( NL = 3 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 34T 'ENDSTREAM' 72T *TIMX ( EM = HH':'II' 'AP )
            new NumericLength (3), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),new 
            TabSetting(34),"ENDSTREAM",new TabSetting(72),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"));
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,"=",new RepeatItem(79));                                                                                               //Natural: WRITE ( 1 ) '=' ( 79 )
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM CHECK-PROGRAMS
        sub_Check_Programs();
        if (condition(Global.isEscape())) {return;}
        pnd_Tbl_Key_Data_Nme.setValuesByName(pnd_Tbl_Prime_Key);                                                                                                          //Natural: MOVE BY NAME #TBL-PRIME-KEY TO #TBL-KEY.DATA-NME
        //*  READ CWF-SUPPORT-TBL
        vw_cwf_Support_Tbl.startDatabaseRead                                                                                                                              //Natural: READ ( 1 ) CWF-SUPPORT-TBL BY TBL-PRIME-KEY FROM #TBL-KEY
        (
        "READ",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") },
        1
        );
        READ:
        while (condition(vw_cwf_Support_Tbl.readNextRow("READ")))
        {
            PND_PND_L1490:                                                                                                                                                //Natural: GET CWF-SUPPORT-TBL *ISN ( READ. )
            vw_cwf_Support_Tbl.readByID(vw_cwf_Support_Tbl.getAstISN("READ"), "PND_PND_L1490");
            cwf_Support_Tbl_Tbl_Last_Run_Flag.setValue("Y");                                                                                                              //Natural: MOVE 'Y' TO TBL-LAST-RUN-FLAG
            vw_cwf_Support_Tbl.updateDBRow("PND_PND_L1490");                                                                                                              //Natural: UPDATE ( ##L1490. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            //*  READ.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* ***********************************************************************
        pnd_Tbl_Ky_Data_Nme.setValuesByName(pnd_Tbl_Prime_Ky);                                                                                                            //Natural: MOVE BY NAME #TBL-PRIME-KY TO #TBL-KY.DATA-NME
        //*  READ CWF-SUPPORT-TBL
        vw_rep_Run_Table.startDatabaseRead                                                                                                                                //Natural: READ ( 1 ) REP-RUN-TABLE BY TBL-PRIME-KEY FROM #TBL-KY
        (
        "READ2",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Ky, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") },
        1
        );
        READ2:
        while (condition(vw_rep_Run_Table.readNextRow("READ2")))
        {
            getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(14),"LAST RUN DATE:",rep_Run_Table_Tbl_Run_Date,"          RUN FLAG:",rep_Run_Table_Tbl_Run_Flag, //Natural: WRITE ( 1 ) 14X 'LAST RUN DATE:' REP-RUN-TABLE.TBL-RUN-DATE '          RUN FLAG:' REP-RUN-TABLE.TBL-RUN-FLAG /
                NEWLINE);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ2"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ2"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  READ.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* ******************** START OF MAIN PROGRAM LOGIC **********************
        pnd_Tbl_Key_Data_Nme.setValuesByName(pnd_Tbl_Prime_Key1);                                                                                                         //Natural: MOVE BY NAME #TBL-PRIME-KEY1 TO #TBL-KEY.DATA-NME
        //*  READ CWF-SUPPORT-TBL
        vw_cwf_Support_Tbl1.startDatabaseRead                                                                                                                             //Natural: READ CWF-SUPPORT-TBL1 BY TBL-PRIME-KEY FROM #TBL-KEY
        (
        "READ3",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
        );
        READ3:
        while (condition(vw_cwf_Support_Tbl1.readNextRow("READ3")))
        {
            if (condition(cwf_Support_Tbl1_Tbl_Key_Field.greater("CWFB9999")))                                                                                            //Natural: IF CWF-SUPPORT-TBL1.TBL-KEY-FIELD GT 'CWFB9999'
            {
                if (true) break READ3;                                                                                                                                    //Natural: ESCAPE BOTTOM ( READ3. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(cwf_Support_Tbl1_Tbl_Report_Freq.equals("W"))))                                                                                               //Natural: ACCEPT IF CWF-SUPPORT-TBL1.TBL-REPORT-FREQ = 'W'
            {
                continue;
            }
            getReports().write(1, ReportOption.NOTITLE,"REPORT  :",cwf_Support_Tbl1_Tbl_Key_Field, new AlphanumericLength (8),"LAST RUN:",cwf_Support_Tbl1_Tbl_Run_Date,  //Natural: WRITE ( 1 ) 'REPORT  :' CWF-SUPPORT-TBL1.TBL-KEY-FIELD ( AL = 8 ) 'LAST RUN:' CWF-SUPPORT-TBL1.TBL-RUN-DATE '          RUN FLAG:' CWF-SUPPORT-TBL1.TBL-RUN-FLAG
                "          RUN FLAG:",cwf_Support_Tbl1_Tbl_Run_Flag);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ3"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ3"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  READ.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().eject(1, true);                                                                                                                                      //Natural: EJECT ( 1 )
        getReports().write(1, writeMapToStringOutput(Cwff3911.class));                                                                                                    //Natural: WRITE ( 1 ) USING MAP 'CWFF3911'
        getReports().write(1, writeMapToStringOutput(Cwff3911.class));                                                                                                    //Natural: WRITE ( 1 ) USING MAP 'CWFF3911'
        getReports().eject(1, true);                                                                                                                                      //Natural: EJECT ( 1 )
    }
    private void sub_Check_Programs() throws Exception                                                                                                                    //Natural: CHECK-PROGRAMS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Report_Parm.setValue("CWFB3001W*");                                                                                                                           //Natural: MOVE 'CWFB3001W*' TO #REPORT-PARM
        DbsUtil.callnat(Cwfn3916.class , getCurrentProcessState(), pnd_Report_Parm, pnd_Racf_Id, pnd_Parm_Unit, pnd_Parm_Floor, pnd_Parm_Bldg, pnd_Parm_Drop_Off,         //Natural: CALLNAT 'CWFN3916' #REPORT-PARM #RACF-ID #PARM-UNIT #PARM-FLOOR #PARM-BLDG #PARM-DROP-OFF #TBL-RUN-DATE #TBL-RUN-FLAG
            pnd_Tbl_Run_Date, pnd_Tbl_Run_Flag);
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Comp_Date.equals(pnd_Tbl_Run_Date) && pnd_Tbl_Run_Flag.equals("Y")))                                                                            //Natural: IF #COMP-DATE = #TBL-RUN-DATE AND #TBL-RUN-FLAG = 'Y'
        {
            pnd_Prog_Count.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #PROG-COUNT
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Report_Parm.setValue("CWFB3002W*");                                                                                                                           //Natural: MOVE 'CWFB3002W*' TO #REPORT-PARM
        DbsUtil.callnat(Cwfn3916.class , getCurrentProcessState(), pnd_Report_Parm, pnd_Racf_Id, pnd_Parm_Unit, pnd_Parm_Floor, pnd_Parm_Bldg, pnd_Parm_Drop_Off,         //Natural: CALLNAT 'CWFN3916' #REPORT-PARM #RACF-ID #PARM-UNIT #PARM-FLOOR #PARM-BLDG #PARM-DROP-OFF #TBL-RUN-DATE #TBL-RUN-FLAG
            pnd_Tbl_Run_Date, pnd_Tbl_Run_Flag);
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Comp_Date.equals(pnd_Tbl_Run_Date) && pnd_Tbl_Run_Flag.equals("Y")))                                                                            //Natural: IF #COMP-DATE = #TBL-RUN-DATE AND #TBL-RUN-FLAG = 'Y'
        {
            pnd_Prog_Count.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #PROG-COUNT
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Report_Parm.setValue("CWFB3003W*");                                                                                                                           //Natural: MOVE 'CWFB3003W*' TO #REPORT-PARM
        DbsUtil.callnat(Cwfn3916.class , getCurrentProcessState(), pnd_Report_Parm, pnd_Racf_Id, pnd_Parm_Unit, pnd_Parm_Floor, pnd_Parm_Bldg, pnd_Parm_Drop_Off,         //Natural: CALLNAT 'CWFN3916' #REPORT-PARM #RACF-ID #PARM-UNIT #PARM-FLOOR #PARM-BLDG #PARM-DROP-OFF #TBL-RUN-DATE #TBL-RUN-FLAG
            pnd_Tbl_Run_Date, pnd_Tbl_Run_Flag);
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Comp_Date.equals(pnd_Tbl_Run_Date) && pnd_Tbl_Run_Flag.equals("Y")))                                                                            //Natural: IF #COMP-DATE = #TBL-RUN-DATE AND #TBL-RUN-FLAG = 'Y'
        {
            pnd_Prog_Count.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #PROG-COUNT
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Report_Parm.setValue("CWFB3005W*");                                                                                                                           //Natural: MOVE 'CWFB3005W*' TO #REPORT-PARM
        DbsUtil.callnat(Cwfn3916.class , getCurrentProcessState(), pnd_Report_Parm, pnd_Racf_Id, pnd_Parm_Unit, pnd_Parm_Floor, pnd_Parm_Bldg, pnd_Parm_Drop_Off,         //Natural: CALLNAT 'CWFN3916' #REPORT-PARM #RACF-ID #PARM-UNIT #PARM-FLOOR #PARM-BLDG #PARM-DROP-OFF #TBL-RUN-DATE #TBL-RUN-FLAG
            pnd_Tbl_Run_Date, pnd_Tbl_Run_Flag);
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Comp_Date.equals(pnd_Tbl_Run_Date) && pnd_Tbl_Run_Flag.equals("Y")))                                                                            //Natural: IF #COMP-DATE = #TBL-RUN-DATE AND #TBL-RUN-FLAG = 'Y'
        {
            pnd_Prog_Count.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #PROG-COUNT
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Report_Parm.setValue("CWFB3011W*");                                                                                                                           //Natural: MOVE 'CWFB3011W*' TO #REPORT-PARM
        DbsUtil.callnat(Cwfn3916.class , getCurrentProcessState(), pnd_Report_Parm, pnd_Racf_Id, pnd_Parm_Unit, pnd_Parm_Floor, pnd_Parm_Bldg, pnd_Parm_Drop_Off,         //Natural: CALLNAT 'CWFN3916' #REPORT-PARM #RACF-ID #PARM-UNIT #PARM-FLOOR #PARM-BLDG #PARM-DROP-OFF #TBL-RUN-DATE #TBL-RUN-FLAG
            pnd_Tbl_Run_Date, pnd_Tbl_Run_Flag);
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Comp_Date.equals(pnd_Tbl_Run_Date) && pnd_Tbl_Run_Flag.equals("Y")))                                                                            //Natural: IF #COMP-DATE = #TBL-RUN-DATE AND #TBL-RUN-FLAG = 'Y'
        {
            pnd_Prog_Count.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #PROG-COUNT
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
