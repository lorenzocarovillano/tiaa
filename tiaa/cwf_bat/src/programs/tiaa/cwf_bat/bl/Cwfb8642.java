/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:46:38 PM
**        * FROM NATURAL PROGRAM : Cwfb8642
************************************************************
**        * FILE NAME            : Cwfb8642.java
**        * CLASS NAME           : Cwfb8642
**        * INSTANCE NAME        : Cwfb8642
************************************************************
************************************************************************
* SYSTEM   : CORPORATE WORKFLOW (CWF)
* PROGRAM  : CWFB8642
* TITLE    : EAE - BENEFICIARY ALERTS
* FUNCTION : READ CWF-WHO-WORK-REQUEST & MDM TO CREATE 3
*            BENEFICIARY ALERT FILES
*              1. STG_PRTY_ALERT_LD_CWF.TXT
*              2. STG_PRTY_ALERT_ATTRB_LD_CWF.TXT
*              3. STG_PRTY_ALERT_DLV_METHD_LD_CWF.TXT
* JOBS     : PCW4700D (EXTRACT) & PCW4700X (NDM TO CHAPDC3FTMQD1A)
* SCHEDULE : TUE - SAT @ 12:05 AM
* RUN DATE : PCW4700D-RUN-DATE (CWF-SUPPORT-TBL)
* HISTORY  : CREATED ON MAY 24, 2016 - VINODHKUMAR RAMACHANDRAN
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
* 09/15/2017 - VR REMOVE DUPLICATE RECORDS WITH SAME
*                 RQST LOG DATE + PIN + WPID
* 01/16/2018 - CG EXCLUDE THE WPID 'TA MBM' AND 'TAIMBM'
*                 FROM THE ALERT NOTIFICATION. REFER INC#4094767
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb8642 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private PdaMdma111 pdaMdma111;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_master;
    private DbsField master_Rqst_Log_Dte_Tme;

    private DbsGroup master__R_Field_1;
    private DbsField master_Pnd_Rqst_Log_Dte;
    private DbsField master_Pnd_Rqst_Log_Tme;
    private DbsField master_Last_Chnge_Oprtr_Cde;
    private DbsField master_Admin_Status_Cde;
    private DbsGroup master_Cntrct_NbrMuGroup;
    private DbsField master_Cntrct_Nbr;
    private DbsField master_Work_Prcss_Id;
    private DbsField master_Pin_Nbr;
    private DbsField master_Rqst_Orgn_Cde;
    private DbsField master_Status_Cde;
    private DbsField master_Tiaa_Rcvd_Dte;
    private DbsField master_Crprte_Status_Ind;
    private DbsField master_Empl_Oprtr_Cde;
    private DbsField master_Last_Chnge_Invrt_Dte_Tme;
    private DbsField master_Last_Chnge_Dte_Tme;

    private DbsGroup master__R_Field_2;
    private DbsField master_Last_Chnge_Dte;
    private DbsField master_Last_Chnge_Tme;

    private DataAccessProgramView vw_master2;
    private DbsField master2_Rqst_Log_Dte_Tme;
    private DbsField master2_Pin_Nbr;
    private DbsField master2_Last_Chnge_Oprtr_Cde;
    private DbsField master2_Work_Prcss_Id;
    private DbsField master2_Rqst_Orgn_Cde;
    private DbsField master2_Status_Cde;
    private DbsField master2_Last_Chnge_Dte_Tme;

    private DbsGroup master2__R_Field_3;
    private DbsField master2_Last_Chnge_Dte;
    private DbsField master2_Last_Chnge_Tme;
    private DbsField pnd_Last_Chnge_Dte_Tme;

    private DbsGroup pnd_Last_Chnge_Dte_Tme__R_Field_4;
    private DbsField pnd_Last_Chnge_Dte_Tme_Pnd_Last_Chnge_Dte;
    private DbsField pnd_Last_Chnge_Dte_Tme_Pnd_Last_Chnge_Tme;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Actve_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_5;
    private DbsField cwf_Support_Tbl_Pnd_Tbl_Data_Field1;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Dte;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Dte_Tme;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_6;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind;
    private DbsField pnd_Output_Workfile1;

    private DbsGroup pnd_Output_Workfile1__R_Field_7;
    private DbsField pnd_Output_Workfile1_Pnd_O1_Stg_Id;

    private DbsGroup pnd_Output_Workfile1__R_Field_8;
    private DbsField pnd_Output_Workfile1_Pnd_O1_Stg_Id_Date;
    private DbsField pnd_Output_Workfile1_Pnd_O1_Stg_Id_Time;
    private DbsField pnd_Output_Workfile1_Pnd_O1_Stg_Id_System_Cde;
    private DbsField pnd_Output_Workfile1_Pnd_O1_Stg_Id_Seq_No;

    private DbsGroup pnd_Output_Workfile1__R_Field_9;
    private DbsField pnd_Output_Workfile1_Pnd_O1_Stg_Id_Seq_No_N;
    private DbsField pnd_Output_Workfile1_Pnd_O1_Filler1;
    private DbsField pnd_Output_Workfile1_Pnd_O1_Cmnct_Cd;
    private DbsField pnd_Output_Workfile1_Pnd_O1_Filler2;
    private DbsField pnd_Output_Workfile1_Pnd_O1_Prty_Id;

    private DbsGroup pnd_Output_Workfile1__R_Field_10;
    private DbsField pnd_Output_Workfile1_Pnd_O1_Prty_Id_N7;
    private DbsField pnd_Output_Workfile1_Pnd_O1_Filler3;
    private DbsField pnd_Output_Workfile1_Pnd_O1_Prty_Id_Typ_Cd;
    private DbsField pnd_Output_Workfile1_Pnd_O1_Filler4;
    private DbsField pnd_Output_Workfile1_Pnd_O1_Prty_Typ_Cd;
    private DbsField pnd_Output_Workfile1_Pnd_O1_Filler5;
    private DbsField pnd_Output_Workfile1_Pnd_O1_Trg_Typ_Cd;
    private DbsField pnd_Output_Workfile1_Pnd_O1_Filler6;
    private DbsField pnd_Output_Workfile1_Pnd_O1_Eff_Frm_Dt;

    private DbsGroup pnd_Output_Workfile1__R_Field_11;
    private DbsField pnd_Output_Workfile1_Pnd_O1_Eff_Frm_Dt_D;
    private DbsField pnd_Output_Workfile1_Pnd_O1_Eff_Frm_Dt_F;
    private DbsField pnd_Output_Workfile1_Pnd_O1_Eff_Frm_Dt_T;
    private DbsField pnd_Output_Workfile1_Pnd_O1_Filler7;
    private DbsField pnd_Output_Workfile1_Pnd_O1_Eff_To_Dt;

    private DbsGroup pnd_Output_Workfile1__R_Field_12;
    private DbsField pnd_Output_Workfile1_Pnd_O1_Eff_To_Dt_D;
    private DbsField pnd_Output_Workfile1_Pnd_O1_Eff_To_Dt_F;
    private DbsField pnd_Output_Workfile1_Pnd_O1_Eff_To_Dt_T;
    private DbsField pnd_Output_Workfile1_Pnd_O1_Filler8;
    private DbsField pnd_Output_Workfile1_Pnd_O1_Syst_Cd;
    private DbsField pnd_Output_Workfile1_Pnd_O1_Filler9;
    private DbsField pnd_Output_Workfile1_Pnd_O1_Tm_Prd_Typ_Cd;
    private DbsField pnd_Output_Workfile1_Pnd_O1_Filler10;
    private DbsField pnd_Output_Workfile1_Pnd_O1_Wpid;
    private DbsField pnd_Output_Workfile1_Pnd_O1_Filler11;
    private DbsField pnd_Output_Workfile1_Pnd_O1_Moc;
    private DbsField pnd_Output_Workfile1_Pnd_O1_Filler12;
    private DbsField pnd_Output_Workfile1_Pnd_O1_Status_Cde;
    private DbsField pnd_Output_Workfile1_Pnd_O1_Filler13;
    private DbsField pnd_Output_Workfile1_Final;
    private DbsField pnd_Output_Workfile2;
    private DbsField pnd_Output_Workfile3;
    private DbsField pnd_P_Rqst_Log_Dte_Tme;

    private DbsGroup pnd_Procd_Array;
    private DbsField pnd_Procd_Array_Pnd_Procd_Rqst_Log_Dte_Tme;
    private DbsField pnd_Procd_Array_Pnd_Procd_Pin_Nbr;
    private DbsField pnd_Procd_Array_Pnd_Procd_Wpid;
    private DbsField pnd_Procd_Array_Pnd_Procd_Date;
    private DbsField pnd_Procd_Array_Cnt;
    private DbsField pnd_Dup_P_Rec_Cnt;
    private DbsField pnd_Procd_Rec_Found;
    private DbsField pnd_R_Rqst_Log_Dte_Tme;

    private DbsGroup pnd_Recvd_Array;
    private DbsField pnd_Recvd_Array_Pnd_Recvd_Rqst_Log_Dte_Tme;
    private DbsField pnd_Recvd_Array_Pnd_Recvd_Pin_Nbr;
    private DbsField pnd_Recvd_Array_Pnd_Recvd_Wpid;
    private DbsField pnd_Recvd_Array_Pnd_Recvd_Date;
    private DbsField pnd_Recvd_Array_Cnt;
    private DbsField pnd_Dup_R_Rec_Cnt;
    private DbsField pnd_Recvd_Rec_Found;
    private DbsField pnd_Current_Dte_Tme;

    private DbsGroup pnd_Current_Dte_Tme__R_Field_13;
    private DbsField pnd_Current_Dte_Tme_Pnd_Current_Dte;
    private DbsField pnd_Current_Dte_Tme_Pnd_Current_Tme;
    private DbsField pnd_Start_Date;
    private DbsField pnd_End_Date;

    private DbsGroup pnd_End_Date__R_Field_14;
    private DbsField pnd_End_Date_Pnd_End_Date_A;
    private DbsField pnd_End_Date_D;
    private DbsField pnd_Eff_To_Date;

    private DbsGroup pnd_Eff_To_Date__R_Field_15;
    private DbsField pnd_Eff_To_Date_Pnd_Eff_To_Date_Yyyy;
    private DbsField pnd_Eff_To_Date_Pnd_Eff_To_Date_Mm;
    private DbsField pnd_Eff_To_Date_Pnd_Eff_To_Date_Dd;
    private DbsField pnd_Eff_To_Date_D;
    private DbsField pnd_Rec_Count;
    private DbsField pnd_Rec_Count_Work2;
    private DbsField pnd_I;
    private DbsField pnd_Error_Msg;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaMdma111 = new PdaMdma111(localVariables);

        // Local Variables

        vw_master = new DataAccessProgramView(new NameInfo("vw_master", "MASTER"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        master_Rqst_Log_Dte_Tme = vw_master.getRecord().newFieldInGroup("master_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "RQST_LOG_DTE_TME");
        master_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");

        master__R_Field_1 = vw_master.getRecord().newGroupInGroup("master__R_Field_1", "REDEFINE", master_Rqst_Log_Dte_Tme);
        master_Pnd_Rqst_Log_Dte = master__R_Field_1.newFieldInGroup("master_Pnd_Rqst_Log_Dte", "#RQST-LOG-DTE", FieldType.NUMERIC, 8);
        master_Pnd_Rqst_Log_Tme = master__R_Field_1.newFieldInGroup("master_Pnd_Rqst_Log_Tme", "#RQST-LOG-TME", FieldType.NUMERIC, 7);
        master_Last_Chnge_Oprtr_Cde = vw_master.getRecord().newFieldInGroup("master_Last_Chnge_Oprtr_Cde", "LAST-CHNGE-OPRTR-CDE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "LAST_CHNGE_OPRTR_CDE");
        master_Last_Chnge_Oprtr_Cde.setDdmHeader("LOG CREATE/OPERATOR");
        master_Admin_Status_Cde = vw_master.getRecord().newFieldInGroup("master_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "ADMIN_STATUS_CDE");
        master_Cntrct_NbrMuGroup = vw_master.getRecord().newGroupInGroup("MASTER_CNTRCT_NBRMuGroup", "CNTRCT_NBRMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "CWF_MASTER_INDEX_CNTRCT_NBR");
        master_Cntrct_Nbr = master_Cntrct_NbrMuGroup.newFieldArrayInGroup("master_Cntrct_Nbr", "CNTRCT-NBR", FieldType.STRING, 8, new DbsArrayController(1, 
            10), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_NBR");
        master_Work_Prcss_Id = vw_master.getRecord().newFieldInGroup("master_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        master_Work_Prcss_Id.setDdmHeader("WORK/ID");
        master_Pin_Nbr = vw_master.getRecord().newFieldInGroup("master_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "PIN_NBR");
        master_Pin_Nbr.setDdmHeader("PIN");
        master_Rqst_Orgn_Cde = vw_master.getRecord().newFieldInGroup("master_Rqst_Orgn_Cde", "RQST-ORGN-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RQST_ORGN_CDE");
        master_Status_Cde = vw_master.getRecord().newFieldInGroup("master_Status_Cde", "STATUS-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "STATUS_CDE");
        master_Tiaa_Rcvd_Dte = vw_master.getRecord().newFieldInGroup("master_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIAA_RCVD_DTE");
        master_Crprte_Status_Ind = vw_master.getRecord().newFieldInGroup("master_Crprte_Status_Ind", "CRPRTE-STATUS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CRPRTE_STATUS_IND");
        master_Crprte_Status_Ind.setDdmHeader("CORP/STAT");
        master_Empl_Oprtr_Cde = vw_master.getRecord().newFieldInGroup("master_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "EMPL_OPRTR_CDE");
        master_Empl_Oprtr_Cde.setDdmHeader("EMPLOYEE/ID");
        master_Last_Chnge_Invrt_Dte_Tme = vw_master.getRecord().newFieldInGroup("master_Last_Chnge_Invrt_Dte_Tme", "LAST-CHNGE-INVRT-DTE-TME", FieldType.NUMERIC, 
            15, RepeatingFieldStrategy.None, "LAST_CHNGE_INVRT_DTE_TME");
        master_Last_Chnge_Dte_Tme = vw_master.getRecord().newFieldInGroup("master_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, 
            "LAST_CHNGE_DTE_TME");
        master_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");

        master__R_Field_2 = vw_master.getRecord().newGroupInGroup("master__R_Field_2", "REDEFINE", master_Last_Chnge_Dte_Tme);
        master_Last_Chnge_Dte = master__R_Field_2.newFieldInGroup("master_Last_Chnge_Dte", "LAST-CHNGE-DTE", FieldType.NUMERIC, 8);
        master_Last_Chnge_Tme = master__R_Field_2.newFieldInGroup("master_Last_Chnge_Tme", "LAST-CHNGE-TME", FieldType.NUMERIC, 6);
        registerRecord(vw_master);

        vw_master2 = new DataAccessProgramView(new NameInfo("vw_master2", "MASTER2"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        master2_Rqst_Log_Dte_Tme = vw_master2.getRecord().newFieldInGroup("master2_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "RQST_LOG_DTE_TME");
        master2_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        master2_Pin_Nbr = vw_master2.getRecord().newFieldInGroup("master2_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "PIN_NBR");
        master2_Pin_Nbr.setDdmHeader("PIN");
        master2_Last_Chnge_Oprtr_Cde = vw_master2.getRecord().newFieldInGroup("master2_Last_Chnge_Oprtr_Cde", "LAST-CHNGE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "LAST_CHNGE_OPRTR_CDE");
        master2_Last_Chnge_Oprtr_Cde.setDdmHeader("LOG CREATE/OPERATOR");
        master2_Work_Prcss_Id = vw_master2.getRecord().newFieldInGroup("master2_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        master2_Work_Prcss_Id.setDdmHeader("WORK/ID");
        master2_Rqst_Orgn_Cde = vw_master2.getRecord().newFieldInGroup("master2_Rqst_Orgn_Cde", "RQST-ORGN-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RQST_ORGN_CDE");
        master2_Status_Cde = vw_master2.getRecord().newFieldInGroup("master2_Status_Cde", "STATUS-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "STATUS_CDE");
        master2_Last_Chnge_Dte_Tme = vw_master2.getRecord().newFieldInGroup("master2_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 15, 
            RepeatingFieldStrategy.None, "LAST_CHNGE_DTE_TME");
        master2_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");

        master2__R_Field_3 = vw_master2.getRecord().newGroupInGroup("master2__R_Field_3", "REDEFINE", master2_Last_Chnge_Dte_Tme);
        master2_Last_Chnge_Dte = master2__R_Field_3.newFieldInGroup("master2_Last_Chnge_Dte", "LAST-CHNGE-DTE", FieldType.NUMERIC, 8);
        master2_Last_Chnge_Tme = master2__R_Field_3.newFieldInGroup("master2_Last_Chnge_Tme", "LAST-CHNGE-TME", FieldType.NUMERIC, 6);
        registerRecord(vw_master2);

        pnd_Last_Chnge_Dte_Tme = localVariables.newFieldInRecord("pnd_Last_Chnge_Dte_Tme", "#LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 15);

        pnd_Last_Chnge_Dte_Tme__R_Field_4 = localVariables.newGroupInRecord("pnd_Last_Chnge_Dte_Tme__R_Field_4", "REDEFINE", pnd_Last_Chnge_Dte_Tme);
        pnd_Last_Chnge_Dte_Tme_Pnd_Last_Chnge_Dte = pnd_Last_Chnge_Dte_Tme__R_Field_4.newFieldInGroup("pnd_Last_Chnge_Dte_Tme_Pnd_Last_Chnge_Dte", "#LAST-CHNGE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Last_Chnge_Dte_Tme_Pnd_Last_Chnge_Tme = pnd_Last_Chnge_Dte_Tme__R_Field_4.newFieldInGroup("pnd_Last_Chnge_Dte_Tme_Pnd_Last_Chnge_Tme", "#LAST-CHNGE-TME", 
            FieldType.NUMERIC, 7);

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Actve_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Actve_Ind", "TBL-ACTVE-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TBL_ACTVE_IND");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Support_Tbl__R_Field_5 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_5", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Pnd_Tbl_Data_Field1 = cwf_Support_Tbl__R_Field_5.newFieldInGroup("cwf_Support_Tbl_Pnd_Tbl_Data_Field1", "#TBL-DATA-FIELD1", FieldType.NUMERIC, 
            8);
        cwf_Support_Tbl_Tbl_Updte_Dte = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Dte", "TBL-UPDTE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE");
        cwf_Support_Tbl_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        cwf_Support_Tbl_Tbl_Updte_Dte_Tme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Dte_Tme", "TBL-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE_TME");
        cwf_Support_Tbl_Tbl_Updte_Dte_Tme.setDdmHeader("UPDATE DATE/TIME");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        registerRecord(vw_cwf_Support_Tbl);

        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_6 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_6", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind = pnd_Tbl_Prime_Key__R_Field_6.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", FieldType.STRING, 
            1);
        pnd_Output_Workfile1 = localVariables.newFieldInRecord("pnd_Output_Workfile1", "#OUTPUT-WORKFILE1", FieldType.STRING, 532);

        pnd_Output_Workfile1__R_Field_7 = localVariables.newGroupInRecord("pnd_Output_Workfile1__R_Field_7", "REDEFINE", pnd_Output_Workfile1);
        pnd_Output_Workfile1_Pnd_O1_Stg_Id = pnd_Output_Workfile1__R_Field_7.newFieldInGroup("pnd_Output_Workfile1_Pnd_O1_Stg_Id", "#O1-STG-ID", FieldType.STRING, 
            22);

        pnd_Output_Workfile1__R_Field_8 = pnd_Output_Workfile1__R_Field_7.newGroupInGroup("pnd_Output_Workfile1__R_Field_8", "REDEFINE", pnd_Output_Workfile1_Pnd_O1_Stg_Id);
        pnd_Output_Workfile1_Pnd_O1_Stg_Id_Date = pnd_Output_Workfile1__R_Field_8.newFieldInGroup("pnd_Output_Workfile1_Pnd_O1_Stg_Id_Date", "#O1-STG-ID-DATE", 
            FieldType.STRING, 8);
        pnd_Output_Workfile1_Pnd_O1_Stg_Id_Time = pnd_Output_Workfile1__R_Field_8.newFieldInGroup("pnd_Output_Workfile1_Pnd_O1_Stg_Id_Time", "#O1-STG-ID-TIME", 
            FieldType.STRING, 6);
        pnd_Output_Workfile1_Pnd_O1_Stg_Id_System_Cde = pnd_Output_Workfile1__R_Field_8.newFieldInGroup("pnd_Output_Workfile1_Pnd_O1_Stg_Id_System_Cde", 
            "#O1-STG-ID-SYSTEM-CDE", FieldType.STRING, 2);
        pnd_Output_Workfile1_Pnd_O1_Stg_Id_Seq_No = pnd_Output_Workfile1__R_Field_8.newFieldInGroup("pnd_Output_Workfile1_Pnd_O1_Stg_Id_Seq_No", "#O1-STG-ID-SEQ-NO", 
            FieldType.STRING, 6);

        pnd_Output_Workfile1__R_Field_9 = pnd_Output_Workfile1__R_Field_8.newGroupInGroup("pnd_Output_Workfile1__R_Field_9", "REDEFINE", pnd_Output_Workfile1_Pnd_O1_Stg_Id_Seq_No);
        pnd_Output_Workfile1_Pnd_O1_Stg_Id_Seq_No_N = pnd_Output_Workfile1__R_Field_9.newFieldInGroup("pnd_Output_Workfile1_Pnd_O1_Stg_Id_Seq_No_N", "#O1-STG-ID-SEQ-NO-N", 
            FieldType.NUMERIC, 6);
        pnd_Output_Workfile1_Pnd_O1_Filler1 = pnd_Output_Workfile1__R_Field_7.newFieldInGroup("pnd_Output_Workfile1_Pnd_O1_Filler1", "#O1-FILLER1", FieldType.STRING, 
            1);
        pnd_Output_Workfile1_Pnd_O1_Cmnct_Cd = pnd_Output_Workfile1__R_Field_7.newFieldInGroup("pnd_Output_Workfile1_Pnd_O1_Cmnct_Cd", "#O1-CMNCT-CD", 
            FieldType.STRING, 32);
        pnd_Output_Workfile1_Pnd_O1_Filler2 = pnd_Output_Workfile1__R_Field_7.newFieldInGroup("pnd_Output_Workfile1_Pnd_O1_Filler2", "#O1-FILLER2", FieldType.STRING, 
            1);
        pnd_Output_Workfile1_Pnd_O1_Prty_Id = pnd_Output_Workfile1__R_Field_7.newFieldInGroup("pnd_Output_Workfile1_Pnd_O1_Prty_Id", "#O1-PRTY-ID", FieldType.STRING, 
            256);

        pnd_Output_Workfile1__R_Field_10 = pnd_Output_Workfile1__R_Field_7.newGroupInGroup("pnd_Output_Workfile1__R_Field_10", "REDEFINE", pnd_Output_Workfile1_Pnd_O1_Prty_Id);
        pnd_Output_Workfile1_Pnd_O1_Prty_Id_N7 = pnd_Output_Workfile1__R_Field_10.newFieldInGroup("pnd_Output_Workfile1_Pnd_O1_Prty_Id_N7", "#O1-PRTY-ID-N7", 
            FieldType.NUMERIC, 12);
        pnd_Output_Workfile1_Pnd_O1_Filler3 = pnd_Output_Workfile1__R_Field_7.newFieldInGroup("pnd_Output_Workfile1_Pnd_O1_Filler3", "#O1-FILLER3", FieldType.STRING, 
            1);
        pnd_Output_Workfile1_Pnd_O1_Prty_Id_Typ_Cd = pnd_Output_Workfile1__R_Field_7.newFieldInGroup("pnd_Output_Workfile1_Pnd_O1_Prty_Id_Typ_Cd", "#O1-PRTY-ID-TYP-CD", 
            FieldType.STRING, 32);
        pnd_Output_Workfile1_Pnd_O1_Filler4 = pnd_Output_Workfile1__R_Field_7.newFieldInGroup("pnd_Output_Workfile1_Pnd_O1_Filler4", "#O1-FILLER4", FieldType.STRING, 
            1);
        pnd_Output_Workfile1_Pnd_O1_Prty_Typ_Cd = pnd_Output_Workfile1__R_Field_7.newFieldInGroup("pnd_Output_Workfile1_Pnd_O1_Prty_Typ_Cd", "#O1-PRTY-TYP-CD", 
            FieldType.STRING, 32);
        pnd_Output_Workfile1_Pnd_O1_Filler5 = pnd_Output_Workfile1__R_Field_7.newFieldInGroup("pnd_Output_Workfile1_Pnd_O1_Filler5", "#O1-FILLER5", FieldType.STRING, 
            1);
        pnd_Output_Workfile1_Pnd_O1_Trg_Typ_Cd = pnd_Output_Workfile1__R_Field_7.newFieldInGroup("pnd_Output_Workfile1_Pnd_O1_Trg_Typ_Cd", "#O1-TRG-TYP-CD", 
            FieldType.STRING, 32);
        pnd_Output_Workfile1_Pnd_O1_Filler6 = pnd_Output_Workfile1__R_Field_7.newFieldInGroup("pnd_Output_Workfile1_Pnd_O1_Filler6", "#O1-FILLER6", FieldType.STRING, 
            1);
        pnd_Output_Workfile1_Pnd_O1_Eff_Frm_Dt = pnd_Output_Workfile1__R_Field_7.newFieldInGroup("pnd_Output_Workfile1_Pnd_O1_Eff_Frm_Dt", "#O1-EFF-FRM-DT", 
            FieldType.STRING, 19);

        pnd_Output_Workfile1__R_Field_11 = pnd_Output_Workfile1__R_Field_7.newGroupInGroup("pnd_Output_Workfile1__R_Field_11", "REDEFINE", pnd_Output_Workfile1_Pnd_O1_Eff_Frm_Dt);
        pnd_Output_Workfile1_Pnd_O1_Eff_Frm_Dt_D = pnd_Output_Workfile1__R_Field_11.newFieldInGroup("pnd_Output_Workfile1_Pnd_O1_Eff_Frm_Dt_D", "#O1-EFF-FRM-DT-D", 
            FieldType.STRING, 10);
        pnd_Output_Workfile1_Pnd_O1_Eff_Frm_Dt_F = pnd_Output_Workfile1__R_Field_11.newFieldInGroup("pnd_Output_Workfile1_Pnd_O1_Eff_Frm_Dt_F", "#O1-EFF-FRM-DT-F", 
            FieldType.STRING, 1);
        pnd_Output_Workfile1_Pnd_O1_Eff_Frm_Dt_T = pnd_Output_Workfile1__R_Field_11.newFieldInGroup("pnd_Output_Workfile1_Pnd_O1_Eff_Frm_Dt_T", "#O1-EFF-FRM-DT-T", 
            FieldType.STRING, 8);
        pnd_Output_Workfile1_Pnd_O1_Filler7 = pnd_Output_Workfile1__R_Field_7.newFieldInGroup("pnd_Output_Workfile1_Pnd_O1_Filler7", "#O1-FILLER7", FieldType.STRING, 
            1);
        pnd_Output_Workfile1_Pnd_O1_Eff_To_Dt = pnd_Output_Workfile1__R_Field_7.newFieldInGroup("pnd_Output_Workfile1_Pnd_O1_Eff_To_Dt", "#O1-EFF-TO-DT", 
            FieldType.STRING, 19);

        pnd_Output_Workfile1__R_Field_12 = pnd_Output_Workfile1__R_Field_7.newGroupInGroup("pnd_Output_Workfile1__R_Field_12", "REDEFINE", pnd_Output_Workfile1_Pnd_O1_Eff_To_Dt);
        pnd_Output_Workfile1_Pnd_O1_Eff_To_Dt_D = pnd_Output_Workfile1__R_Field_12.newFieldInGroup("pnd_Output_Workfile1_Pnd_O1_Eff_To_Dt_D", "#O1-EFF-TO-DT-D", 
            FieldType.STRING, 10);
        pnd_Output_Workfile1_Pnd_O1_Eff_To_Dt_F = pnd_Output_Workfile1__R_Field_12.newFieldInGroup("pnd_Output_Workfile1_Pnd_O1_Eff_To_Dt_F", "#O1-EFF-TO-DT-F", 
            FieldType.STRING, 1);
        pnd_Output_Workfile1_Pnd_O1_Eff_To_Dt_T = pnd_Output_Workfile1__R_Field_12.newFieldInGroup("pnd_Output_Workfile1_Pnd_O1_Eff_To_Dt_T", "#O1-EFF-TO-DT-T", 
            FieldType.STRING, 8);
        pnd_Output_Workfile1_Pnd_O1_Filler8 = pnd_Output_Workfile1__R_Field_7.newFieldInGroup("pnd_Output_Workfile1_Pnd_O1_Filler8", "#O1-FILLER8", FieldType.STRING, 
            1);
        pnd_Output_Workfile1_Pnd_O1_Syst_Cd = pnd_Output_Workfile1__R_Field_7.newFieldInGroup("pnd_Output_Workfile1_Pnd_O1_Syst_Cd", "#O1-SYST-CD", FieldType.STRING, 
            32);
        pnd_Output_Workfile1_Pnd_O1_Filler9 = pnd_Output_Workfile1__R_Field_7.newFieldInGroup("pnd_Output_Workfile1_Pnd_O1_Filler9", "#O1-FILLER9", FieldType.STRING, 
            1);
        pnd_Output_Workfile1_Pnd_O1_Tm_Prd_Typ_Cd = pnd_Output_Workfile1__R_Field_7.newFieldInGroup("pnd_Output_Workfile1_Pnd_O1_Tm_Prd_Typ_Cd", "#O1-TM-PRD-TYP-CD", 
            FieldType.STRING, 32);
        pnd_Output_Workfile1_Pnd_O1_Filler10 = pnd_Output_Workfile1__R_Field_7.newFieldInGroup("pnd_Output_Workfile1_Pnd_O1_Filler10", "#O1-FILLER10", 
            FieldType.STRING, 1);
        pnd_Output_Workfile1_Pnd_O1_Wpid = pnd_Output_Workfile1__R_Field_7.newFieldInGroup("pnd_Output_Workfile1_Pnd_O1_Wpid", "#O1-WPID", FieldType.STRING, 
            6);
        pnd_Output_Workfile1_Pnd_O1_Filler11 = pnd_Output_Workfile1__R_Field_7.newFieldInGroup("pnd_Output_Workfile1_Pnd_O1_Filler11", "#O1-FILLER11", 
            FieldType.STRING, 1);
        pnd_Output_Workfile1_Pnd_O1_Moc = pnd_Output_Workfile1__R_Field_7.newFieldInGroup("pnd_Output_Workfile1_Pnd_O1_Moc", "#O1-MOC", FieldType.STRING, 
            1);
        pnd_Output_Workfile1_Pnd_O1_Filler12 = pnd_Output_Workfile1__R_Field_7.newFieldInGroup("pnd_Output_Workfile1_Pnd_O1_Filler12", "#O1-FILLER12", 
            FieldType.STRING, 1);
        pnd_Output_Workfile1_Pnd_O1_Status_Cde = pnd_Output_Workfile1__R_Field_7.newFieldInGroup("pnd_Output_Workfile1_Pnd_O1_Status_Cde", "#O1-STATUS-CDE", 
            FieldType.STRING, 4);
        pnd_Output_Workfile1_Pnd_O1_Filler13 = pnd_Output_Workfile1__R_Field_7.newFieldInGroup("pnd_Output_Workfile1_Pnd_O1_Filler13", "#O1-FILLER13", 
            FieldType.STRING, 1);
        pnd_Output_Workfile1_Final = localVariables.newFieldInRecord("pnd_Output_Workfile1_Final", "#OUTPUT-WORKFILE1-FINAL", FieldType.STRING, 269);
        pnd_Output_Workfile2 = localVariables.newFieldInRecord("pnd_Output_Workfile2", "#OUTPUT-WORKFILE2", FieldType.STRING, 60);
        pnd_Output_Workfile3 = localVariables.newFieldInRecord("pnd_Output_Workfile3", "#OUTPUT-WORKFILE3", FieldType.STRING, 99);
        pnd_P_Rqst_Log_Dte_Tme = localVariables.newFieldInRecord("pnd_P_Rqst_Log_Dte_Tme", "#P-RQST-LOG-DTE-TME", FieldType.STRING, 15);

        pnd_Procd_Array = localVariables.newGroupArrayInRecord("pnd_Procd_Array", "#PROCD-ARRAY", new DbsArrayController(1, 9999));
        pnd_Procd_Array_Pnd_Procd_Rqst_Log_Dte_Tme = pnd_Procd_Array.newFieldInGroup("pnd_Procd_Array_Pnd_Procd_Rqst_Log_Dte_Tme", "#PROCD-RQST-LOG-DTE-TME", 
            FieldType.STRING, 15);
        pnd_Procd_Array_Pnd_Procd_Pin_Nbr = pnd_Procd_Array.newFieldInGroup("pnd_Procd_Array_Pnd_Procd_Pin_Nbr", "#PROCD-PIN-NBR", FieldType.NUMERIC, 
            12);
        pnd_Procd_Array_Pnd_Procd_Wpid = pnd_Procd_Array.newFieldInGroup("pnd_Procd_Array_Pnd_Procd_Wpid", "#PROCD-WPID", FieldType.STRING, 6);
        pnd_Procd_Array_Pnd_Procd_Date = pnd_Procd_Array.newFieldInGroup("pnd_Procd_Array_Pnd_Procd_Date", "#PROCD-DATE", FieldType.NUMERIC, 8);
        pnd_Procd_Array_Cnt = localVariables.newFieldInRecord("pnd_Procd_Array_Cnt", "#PROCD-ARRAY-CNT", FieldType.NUMERIC, 4);
        pnd_Dup_P_Rec_Cnt = localVariables.newFieldInRecord("pnd_Dup_P_Rec_Cnt", "#DUP-P-REC-CNT", FieldType.NUMERIC, 4);
        pnd_Procd_Rec_Found = localVariables.newFieldInRecord("pnd_Procd_Rec_Found", "#PROCD-REC-FOUND", FieldType.BOOLEAN, 1);
        pnd_R_Rqst_Log_Dte_Tme = localVariables.newFieldInRecord("pnd_R_Rqst_Log_Dte_Tme", "#R-RQST-LOG-DTE-TME", FieldType.STRING, 15);

        pnd_Recvd_Array = localVariables.newGroupArrayInRecord("pnd_Recvd_Array", "#RECVD-ARRAY", new DbsArrayController(1, 9999));
        pnd_Recvd_Array_Pnd_Recvd_Rqst_Log_Dte_Tme = pnd_Recvd_Array.newFieldInGroup("pnd_Recvd_Array_Pnd_Recvd_Rqst_Log_Dte_Tme", "#RECVD-RQST-LOG-DTE-TME", 
            FieldType.STRING, 15);
        pnd_Recvd_Array_Pnd_Recvd_Pin_Nbr = pnd_Recvd_Array.newFieldInGroup("pnd_Recvd_Array_Pnd_Recvd_Pin_Nbr", "#RECVD-PIN-NBR", FieldType.NUMERIC, 
            12);
        pnd_Recvd_Array_Pnd_Recvd_Wpid = pnd_Recvd_Array.newFieldInGroup("pnd_Recvd_Array_Pnd_Recvd_Wpid", "#RECVD-WPID", FieldType.STRING, 6);
        pnd_Recvd_Array_Pnd_Recvd_Date = pnd_Recvd_Array.newFieldInGroup("pnd_Recvd_Array_Pnd_Recvd_Date", "#RECVD-DATE", FieldType.NUMERIC, 8);
        pnd_Recvd_Array_Cnt = localVariables.newFieldInRecord("pnd_Recvd_Array_Cnt", "#RECVD-ARRAY-CNT", FieldType.NUMERIC, 4);
        pnd_Dup_R_Rec_Cnt = localVariables.newFieldInRecord("pnd_Dup_R_Rec_Cnt", "#DUP-R-REC-CNT", FieldType.NUMERIC, 4);
        pnd_Recvd_Rec_Found = localVariables.newFieldInRecord("pnd_Recvd_Rec_Found", "#RECVD-REC-FOUND", FieldType.BOOLEAN, 1);
        pnd_Current_Dte_Tme = localVariables.newFieldInRecord("pnd_Current_Dte_Tme", "#CURRENT-DTE-TME", FieldType.STRING, 15);

        pnd_Current_Dte_Tme__R_Field_13 = localVariables.newGroupInRecord("pnd_Current_Dte_Tme__R_Field_13", "REDEFINE", pnd_Current_Dte_Tme);
        pnd_Current_Dte_Tme_Pnd_Current_Dte = pnd_Current_Dte_Tme__R_Field_13.newFieldInGroup("pnd_Current_Dte_Tme_Pnd_Current_Dte", "#CURRENT-DTE", FieldType.STRING, 
            8);
        pnd_Current_Dte_Tme_Pnd_Current_Tme = pnd_Current_Dte_Tme__R_Field_13.newFieldInGroup("pnd_Current_Dte_Tme_Pnd_Current_Tme", "#CURRENT-TME", FieldType.STRING, 
            6);
        pnd_Start_Date = localVariables.newFieldInRecord("pnd_Start_Date", "#START-DATE", FieldType.NUMERIC, 8);
        pnd_End_Date = localVariables.newFieldInRecord("pnd_End_Date", "#END-DATE", FieldType.NUMERIC, 8);

        pnd_End_Date__R_Field_14 = localVariables.newGroupInRecord("pnd_End_Date__R_Field_14", "REDEFINE", pnd_End_Date);
        pnd_End_Date_Pnd_End_Date_A = pnd_End_Date__R_Field_14.newFieldInGroup("pnd_End_Date_Pnd_End_Date_A", "#END-DATE-A", FieldType.STRING, 8);
        pnd_End_Date_D = localVariables.newFieldInRecord("pnd_End_Date_D", "#END-DATE-D", FieldType.DATE);
        pnd_Eff_To_Date = localVariables.newFieldInRecord("pnd_Eff_To_Date", "#EFF-TO-DATE", FieldType.NUMERIC, 8);

        pnd_Eff_To_Date__R_Field_15 = localVariables.newGroupInRecord("pnd_Eff_To_Date__R_Field_15", "REDEFINE", pnd_Eff_To_Date);
        pnd_Eff_To_Date_Pnd_Eff_To_Date_Yyyy = pnd_Eff_To_Date__R_Field_15.newFieldInGroup("pnd_Eff_To_Date_Pnd_Eff_To_Date_Yyyy", "#EFF-TO-DATE-YYYY", 
            FieldType.STRING, 4);
        pnd_Eff_To_Date_Pnd_Eff_To_Date_Mm = pnd_Eff_To_Date__R_Field_15.newFieldInGroup("pnd_Eff_To_Date_Pnd_Eff_To_Date_Mm", "#EFF-TO-DATE-MM", FieldType.STRING, 
            2);
        pnd_Eff_To_Date_Pnd_Eff_To_Date_Dd = pnd_Eff_To_Date__R_Field_15.newFieldInGroup("pnd_Eff_To_Date_Pnd_Eff_To_Date_Dd", "#EFF-TO-DATE-DD", FieldType.STRING, 
            2);
        pnd_Eff_To_Date_D = localVariables.newFieldInRecord("pnd_Eff_To_Date_D", "#EFF-TO-DATE-D", FieldType.DATE);
        pnd_Rec_Count = localVariables.newFieldInRecord("pnd_Rec_Count", "#REC-COUNT", FieldType.NUMERIC, 7);
        pnd_Rec_Count_Work2 = localVariables.newFieldInRecord("pnd_Rec_Count_Work2", "#REC-COUNT-WORK2", FieldType.NUMERIC, 7);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 4);
        pnd_Error_Msg = localVariables.newFieldInRecord("pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 79);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_master.reset();
        vw_master2.reset();
        vw_cwf_Support_Tbl.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb8642() throws Exception
    {
        super("Cwfb8642");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("CWFB8642", onError);
        //* *----------------------------------------------------------------------
        //*  OPEN MQ TO FACILITATE MDM CALL
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
                                                                                                                                                                          //Natural: PERFORM GET-RUN-DATE
        sub_Get_Run_Date();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM INITIALIZE
        sub_Initialize();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM ALERT1-REPORT-GENERATION
        sub_Alert1_Report_Generation();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM ALERT2-3-REPORT-GENERATION
        sub_Alert2_3_Report_Generation();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM UPDATE-RUN-DATE
        sub_Update_Run_Date();
        if (condition(Global.isEscape())) {return;}
        //*  CLOSE MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-RUN-DATE
        //* ***************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZE
        //* *****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ALERT1-REPORT-GENERATION
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-PROCD-DETAILS
        //*      AND R1.WORK-PRCSS-ID NE 'TA MBM'
        //*      AND R1.WORK-PRCSS-ID NE 'TAIMBM'
        //*      AND R1.RQST-ORGN-CDE NE 'I'
        //*    PRINT '-' (72)
        //*    PRINT R1.RQST-LOG-DTE-TME   ','
        //*          R1.#RQST-LOG-DTE      ','
        //*          R1.PIN-NBR            ','
        //*          R1.WORK-PRCSS-ID      ','
        //*          R1.STATUS-CDE         ','
        //*    WRITE WORK 5 R1.RQST-LOG-DTE-TME   ','
        //*          R1.LAST-CHNGE-DTE-TME ','
        //*          R1.PIN-NBR            ','
        //*          R1.WORK-PRCSS-ID      ','
        //*          R1.STATUS-CDE         ','
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-RECVD-DETAILS
        //*      AND R2.WORK-PRCSS-ID NE 'TA MBM'
        //*      AND R2.WORK-PRCSS-ID NE 'TAIMBM'
        //*      AND R2.RQST-ORGN-CDE NE 'I'
        //*      AND R2.RQST-ORGN-CDE NE 'N'
        //* *******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ALERT2-3-REPORT-GENERATION
        //*  #MDMA110.#I-PIN := #O1-PRTY-ID-N7
        //*      '=' #MDMA110.#O-RETURN-TEXT
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-DUPLICATE-PROCD
        //*      AND R3.WORK-PRCSS-ID NE 'TA MBM'
        //*      AND R3.WORK-PRCSS-ID NE 'TAIMBM'
        //*      AND R3.RQST-ORGN-CDE NE 'I'
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-DUPLICATE-RECVD
        //*      AND R4.WORK-PRCSS-ID NE 'TA MBM'
        //*      AND R4.WORK-PRCSS-ID NE 'TAIMBM'
        //*      AND R4.RQST-ORGN-CDE NE 'I'
        //*      AND R4.RQST-ORGN-CDE NE 'N'
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-RUN-DATE
        //* ***********************************************************************
        //* ***********************************************************************                                                                                       //Natural: ON ERROR
    }
    private void sub_Get_Run_Date() throws Exception                                                                                                                      //Natural: GET-RUN-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------
        pnd_Rec_Count.reset();                                                                                                                                            //Natural: RESET #REC-COUNT #TBL-PRIME-KEY
        pnd_Tbl_Prime_Key.reset();
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind.setValue("A ");                                                                                                         //Natural: ASSIGN #TBL-SCRTY-LEVEL-IND := 'A '
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme.setValue("PCW4700D-RUN-DATE");                                                                                                //Natural: ASSIGN #TBL-TABLE-NME := 'PCW4700D-RUN-DATE'
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field.setValue("PCW4700D-RUN-DATE-KEY");                                                                                            //Natural: ASSIGN #TBL-KEY-FIELD := 'PCW4700D-RUN-DATE-KEY'
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind.setValue(" ");                                                                                                                //Natural: ASSIGN #TBL-ACTVE-IND := ' '
        vw_cwf_Support_Tbl.startDatabaseFind                                                                                                                              //Natural: FIND CWF-SUPPORT-TBL WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
        (
        "F1",
        new Wc[] { new Wc("TBL_PRIME_KEY", "=", pnd_Tbl_Prime_Key, WcType.WITH) }
        );
        F1:
        while (condition(vw_cwf_Support_Tbl.readNextRow("F1", true)))
        {
            vw_cwf_Support_Tbl.setIfNotFoundControlFlag(false);
            if (condition(vw_cwf_Support_Tbl.getAstCOUNTER().equals(0)))                                                                                                  //Natural: IF NO RECORDS FOUND
            {
                getReports().print(0, "Read: No PCW4700D-RUN-DATE Record Found!");                                                                                        //Natural: PRINT 'Read: No PCW4700D-RUN-DATE Record Found!'
                DbsUtil.terminate(4);  if (true) return;                                                                                                                  //Natural: TERMINATE 4
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Start_Date.setValue(cwf_Support_Tbl_Pnd_Tbl_Data_Field1);                                                                                                 //Natural: MOVE CWF-SUPPORT-TBL.#TBL-DATA-FIELD1 TO #START-DATE
            if (condition(pnd_Start_Date.equals(getZero())))                                                                                                              //Natural: IF #START-DATE EQ 0
            {
                getReports().print(0, "Read: PCW4700D-RUN-DATE Record is Blank!");                                                                                        //Natural: PRINT 'Read: PCW4700D-RUN-DATE Record is Blank!'
                DbsUtil.terminate(4);  if (true) return;                                                                                                                  //Natural: TERMINATE 4
            }                                                                                                                                                             //Natural: END-IF
            //*  (F1.)
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Initialize() throws Exception                                                                                                                        //Natural: INITIALIZE
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------
        pnd_End_Date_D.compute(new ComputeParameters(false, pnd_End_Date_D), Global.getDATX().subtract(1));                                                               //Natural: COMPUTE #END-DATE-D := *DATX - 1
        pnd_Eff_To_Date_D.compute(new ComputeParameters(false, pnd_Eff_To_Date_D), Global.getDATX().add(30));                                                             //Natural: COMPUTE #EFF-TO-DATE-D := *DATX + 30
        getReports().print(0, "=",pnd_Start_Date,NEWLINE,"=",pnd_End_Date_D, new ReportEditMask ("YYYYMMDD"));                                                            //Natural: PRINT '=' #START-DATE / '=' #END-DATE-D ( EM = YYYYMMDD )
        pnd_Output_Workfile1.reset();                                                                                                                                     //Natural: RESET #OUTPUT-WORKFILE1
        pnd_Current_Dte_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("YYYYMMDDHHIISST"));                                                                       //Natural: MOVE EDITED *TIMX ( EM = YYYYMMDDHHIISST ) TO #CURRENT-DTE-TME
        pnd_Output_Workfile1_Pnd_O1_Stg_Id_Date.setValue(pnd_Current_Dte_Tme_Pnd_Current_Dte);                                                                            //Natural: ASSIGN #O1-STG-ID-DATE := #CURRENT-DTE
        pnd_Output_Workfile1_Pnd_O1_Stg_Id_Time.setValue(pnd_Current_Dte_Tme_Pnd_Current_Tme);                                                                            //Natural: ASSIGN #O1-STG-ID-TIME := #CURRENT-TME
        pnd_Output_Workfile1_Pnd_O1_Stg_Id_System_Cde.setValue("38");                                                                                                     //Natural: ASSIGN #O1-STG-ID-SYSTEM-CDE := '38'
        pnd_Output_Workfile1_Pnd_O1_Prty_Id_Typ_Cd.setValue("PIN");                                                                                                       //Natural: ASSIGN #O1-PRTY-ID-TYP-CD := 'PIN'
        pnd_Output_Workfile1_Pnd_O1_Prty_Typ_Cd.setValue("PRTCPNT");                                                                                                      //Natural: ASSIGN #O1-PRTY-TYP-CD := 'PRTCPNT'
        pnd_Output_Workfile1_Pnd_O1_Trg_Typ_Cd.setValue("SELF");                                                                                                          //Natural: ASSIGN #O1-TRG-TYP-CD := 'SELF'
        pnd_Output_Workfile1_Pnd_O1_Syst_Cd.setValue("CWF");                                                                                                              //Natural: ASSIGN #O1-SYST-CD := 'CWF'
        pnd_Output_Workfile1_Pnd_O1_Tm_Prd_Typ_Cd.setValue("DAY");                                                                                                        //Natural: ASSIGN #O1-TM-PRD-TYP-CD := 'DAY'
        pnd_Output_Workfile1_Pnd_O1_Eff_Frm_Dt_D.setValueEdited(Global.getTIMX(),new ReportEditMask("YYYY-MM-DD"));                                                       //Natural: MOVE EDITED *TIMX ( EM = YYYY-MM-DD ) TO #O1-EFF-FRM-DT-D
        pnd_Output_Workfile1_Pnd_O1_Eff_Frm_Dt_T.setValue("00:05:00");                                                                                                    //Natural: ASSIGN #O1-EFF-FRM-DT-T := '00:05:00'
        pnd_Output_Workfile1_Pnd_O1_Eff_To_Dt_D.setValueEdited(pnd_Eff_To_Date_D,new ReportEditMask("YYYY-MM-DD"));                                                       //Natural: MOVE EDITED #EFF-TO-DATE-D ( EM = YYYY-MM-DD ) TO #O1-EFF-TO-DT-D
        pnd_Output_Workfile1_Pnd_O1_Eff_To_Dt_T.setValue("10:00:00");                                                                                                     //Natural: ASSIGN #O1-EFF-TO-DT-T := '10:00:00'
        getReports().print(0, "=",pnd_Output_Workfile1_Pnd_O1_Eff_Frm_Dt,NEWLINE,"=",pnd_Output_Workfile1_Pnd_O1_Eff_To_Dt);                                              //Natural: PRINT '=' #O1-EFF-FRM-DT / '=' #O1-EFF-TO-DT
        pnd_Output_Workfile1_Pnd_O1_Filler1.setValue("|");                                                                                                                //Natural: MOVE '|' TO #O1-FILLER1 #O1-FILLER2 #O1-FILLER3 #O1-FILLER4 #O1-FILLER5 #O1-FILLER6 #O1-FILLER7 #O1-FILLER8 #O1-FILLER9 #O1-FILLER10 #O1-FILLER11 #O1-FILLER12 #O1-FILLER13
        pnd_Output_Workfile1_Pnd_O1_Filler2.setValue("|");
        pnd_Output_Workfile1_Pnd_O1_Filler3.setValue("|");
        pnd_Output_Workfile1_Pnd_O1_Filler4.setValue("|");
        pnd_Output_Workfile1_Pnd_O1_Filler5.setValue("|");
        pnd_Output_Workfile1_Pnd_O1_Filler6.setValue("|");
        pnd_Output_Workfile1_Pnd_O1_Filler7.setValue("|");
        pnd_Output_Workfile1_Pnd_O1_Filler8.setValue("|");
        pnd_Output_Workfile1_Pnd_O1_Filler9.setValue("|");
        pnd_Output_Workfile1_Pnd_O1_Filler10.setValue("|");
        pnd_Output_Workfile1_Pnd_O1_Filler11.setValue("|");
        pnd_Output_Workfile1_Pnd_O1_Filler12.setValue("|");
        pnd_Output_Workfile1_Pnd_O1_Filler13.setValue("|");
        pnd_Output_Workfile1_Final.setValue("STG_ID|CMNCT_CD|PRTY_ID|PRTY_ID_TYP_CD|PRTY_TYP_CD|TRG_TYP_CD|EFF_FRM_DT|EFF_TO_DT|SYST_CD|TM_PRD_TYP_CD");                  //Natural: MOVE 'STG_ID|CMNCT_CD|PRTY_ID|PRTY_ID_TYP_CD|PRTY_TYP_CD|TRG_TYP_CD|EFF_FRM_DT|EFF_TO_DT|SYST_CD|TM_PRD_TYP_CD' TO #OUTPUT-WORKFILE1-FINAL
        getWorkFiles().write(2, false, pnd_Output_Workfile1_Final);                                                                                                       //Natural: WRITE WORK FILE 2 #OUTPUT-WORKFILE1-FINAL
        pnd_Output_Workfile2.setValue("STG_ID|ATTRB_CD|ATTRB_VAL");                                                                                                       //Natural: MOVE 'STG_ID|ATTRB_CD|ATTRB_VAL' TO #OUTPUT-WORKFILE2
        getWorkFiles().write(3, false, pnd_Output_Workfile2);                                                                                                             //Natural: WRITE WORK FILE 3 #OUTPUT-WORKFILE2
        pnd_Output_Workfile3.setValue("STG_ID|DLV_METHD_TYP_CD|DLV_METHD_ID");                                                                                            //Natural: MOVE 'STG_ID|DLV_METHD_TYP_CD|DLV_METHD_ID' TO #OUTPUT-WORKFILE3
        getWorkFiles().write(4, false, pnd_Output_Workfile3);                                                                                                             //Natural: WRITE WORK FILE 4 #OUTPUT-WORKFILE3
    }
    private void sub_Alert1_Report_Generation() throws Exception                                                                                                          //Natural: ALERT1-REPORT-GENERATION
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------
                                                                                                                                                                          //Natural: PERFORM GET-PROCD-DETAILS
        sub_Get_Procd_Details();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-RECVD-DETAILS
        sub_Get_Recvd_Details();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Get_Procd_Details() throws Exception                                                                                                                 //Natural: GET-PROCD-DETAILS
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------
        pnd_Last_Chnge_Dte_Tme.reset();                                                                                                                                   //Natural: RESET #LAST-CHNGE-DTE-TME
        pnd_Last_Chnge_Dte_Tme_Pnd_Last_Chnge_Dte.setValue(pnd_Start_Date);                                                                                               //Natural: ASSIGN #LAST-CHNGE-DTE := #START-DATE
        pnd_End_Date_Pnd_End_Date_A.setValueEdited(pnd_End_Date_D,new ReportEditMask("YYYYMMDD"));                                                                        //Natural: MOVE EDITED #END-DATE-D ( EM = YYYYMMDD ) TO #END-DATE-A
        getReports().print(0, "READ KEY =",pnd_Last_Chnge_Dte_Tme);                                                                                                       //Natural: PRINT 'READ KEY =' #LAST-CHNGE-DTE-TME
        vw_master.startDatabaseRead                                                                                                                                       //Natural: READ MASTER BY CHNGE-DTE-TME-LOG-DTE-TME-KEY = #LAST-CHNGE-DTE-TME
        (
        "R1",
        new Wc[] { new Wc("CHNGE_DTE_TME_LOG_DTE_TME_KEY", ">=", pnd_Last_Chnge_Dte_Tme, WcType.BY) },
        new Oc[] { new Oc("CHNGE_DTE_TME_LOG_DTE_TME_KEY", "ASC") }
        );
        R1:
        while (condition(vw_master.readNextRow("R1")))
        {
            if (condition(master_Last_Chnge_Dte.greater(pnd_End_Date)))                                                                                                   //Natural: IF MASTER.LAST-CHNGE-DTE GT #END-DATE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(master_Work_Prcss_Id.notEquals("TA MB") && master_Work_Prcss_Id.notEquals("TA MBW") && master_Work_Prcss_Id.notEquals("TA MBZ")                 //Natural: IF MASTER.WORK-PRCSS-ID NE 'TA MB' AND MASTER.WORK-PRCSS-ID NE 'TA MBW' AND MASTER.WORK-PRCSS-ID NE 'TA MBZ' AND MASTER.WORK-PRCSS-ID NE 'TAIMB' AND MASTER.WORK-PRCSS-ID NE 'TAIMBW' AND MASTER.WORK-PRCSS-ID NE 'TAIMBZ'
                && master_Work_Prcss_Id.notEquals("TAIMB") && master_Work_Prcss_Id.notEquals("TAIMBW") && master_Work_Prcss_Id.notEquals("TAIMBZ")))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(master_Rqst_Orgn_Cde.notEquals("C") && master_Rqst_Orgn_Cde.notEquals("M") && master_Rqst_Orgn_Cde.notEquals("N") && master_Rqst_Orgn_Cde.notEquals("V")  //Natural: IF MASTER.RQST-ORGN-CDE NE 'C' AND MASTER.RQST-ORGN-CDE NE 'M' AND MASTER.RQST-ORGN-CDE NE 'N' AND MASTER.RQST-ORGN-CDE NE 'V' AND MASTER.RQST-ORGN-CDE NE 'X'
                && master_Rqst_Orgn_Cde.notEquals("X")))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(master_Status_Cde.equals("7501") || master_Status_Cde.equals("7502") || master_Status_Cde.equals("8000") || master_Status_Cde.equals("8010")    //Natural: IF MASTER.STATUS-CDE EQ '7501' OR MASTER.STATUS-CDE EQ '7502' OR MASTER.STATUS-CDE EQ '8000' OR MASTER.STATUS-CDE EQ '8010' OR MASTER.STATUS-CDE EQ '8350' OR MASTER.STATUS-CDE EQ '8400'
                || master_Status_Cde.equals("8350") || master_Status_Cde.equals("8400")))
            {
                pnd_Procd_Rec_Found.reset();                                                                                                                              //Natural: RESET #PROCD-REC-FOUND
                FOR01:                                                                                                                                                    //Natural: FOR #I 1 TO #PROCD-ARRAY-CNT
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Procd_Array_Cnt)); pnd_I.nadd(1))
                {
                    if (condition(master_Rqst_Log_Dte_Tme.equals(pnd_Procd_Array_Pnd_Procd_Rqst_Log_Dte_Tme.getValue(pnd_I))))                                            //Natural: IF MASTER.RQST-LOG-DTE-TME EQ #PROCD-RQST-LOG-DTE-TME ( #I )
                    {
                        pnd_Procd_Rec_Found.setValue(true);                                                                                                               //Natural: ASSIGN #PROCD-REC-FOUND := TRUE
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    //*      PRINT #I ',' #PROCD-DATE(#I) ',' #PROCD-PIN-NBR(#I) ','
                    //*            #PROCD-WPID(#I) ','
                    //*  VR 09/15/2017 START
                    if (condition(master_Pin_Nbr.equals(pnd_Procd_Array_Pnd_Procd_Pin_Nbr.getValue(pnd_I)) && master_Work_Prcss_Id.equals(pnd_Procd_Array_Pnd_Procd_Wpid.getValue(pnd_I))  //Natural: IF MASTER.PIN-NBR EQ #PROCD-PIN-NBR ( #I ) AND MASTER.WORK-PRCSS-ID EQ #PROCD-WPID ( #I ) AND MASTER.#RQST-LOG-DTE EQ #PROCD-DATE ( #I )
                        && master_Pnd_Rqst_Log_Dte.equals(pnd_Procd_Array_Pnd_Procd_Date.getValue(pnd_I))))
                    {
                        pnd_Procd_Rec_Found.setValue(true);                                                                                                               //Natural: ASSIGN #PROCD-REC-FOUND := TRUE
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    //*  VR 09/15/2017 END
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_P_Rqst_Log_Dte_Tme.reset();                                                                                                                           //Natural: RESET #P-RQST-LOG-DTE-TME #DUP-P-REC-CNT
                pnd_Dup_P_Rec_Cnt.reset();
                pnd_P_Rqst_Log_Dte_Tme.setValue(master_Rqst_Log_Dte_Tme);                                                                                                 //Natural: MOVE MASTER.RQST-LOG-DTE-TME TO #P-RQST-LOG-DTE-TME
                                                                                                                                                                          //Natural: PERFORM CHECK-DUPLICATE-PROCD
                sub_Check_Duplicate_Procd();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*    PRINT '=' #PROCD-REC-FOUND '=' #DUP-P-REC-CNT
                if (condition(! (pnd_Procd_Rec_Found.getBoolean()) && pnd_Dup_P_Rec_Cnt.equals(getZero())))                                                               //Natural: IF NOT #PROCD-REC-FOUND AND #DUP-P-REC-CNT EQ 0
                {
                    pnd_Procd_Array_Cnt.nadd(1);                                                                                                                          //Natural: ADD 1 TO #PROCD-ARRAY-CNT
                    pnd_Procd_Array_Pnd_Procd_Rqst_Log_Dte_Tme.getValue(pnd_Procd_Array_Cnt).setValue(master_Rqst_Log_Dte_Tme);                                           //Natural: MOVE MASTER.RQST-LOG-DTE-TME TO #PROCD-RQST-LOG-DTE-TME ( #PROCD-ARRAY-CNT )
                    pnd_Procd_Array_Pnd_Procd_Pin_Nbr.getValue(pnd_Procd_Array_Cnt).setValue(master_Pin_Nbr);                                                             //Natural: MOVE MASTER.PIN-NBR TO #PROCD-PIN-NBR ( #PROCD-ARRAY-CNT )
                    //*  VR 09/15/2017 START
                    pnd_Procd_Array_Pnd_Procd_Wpid.getValue(pnd_Procd_Array_Cnt).setValue(master_Work_Prcss_Id);                                                          //Natural: MOVE MASTER.WORK-PRCSS-ID TO #PROCD-WPID ( #PROCD-ARRAY-CNT )
                    pnd_Procd_Array_Pnd_Procd_Date.getValue(pnd_Procd_Array_Cnt).setValue(master_Pnd_Rqst_Log_Dte);                                                       //Natural: MOVE MASTER.#RQST-LOG-DTE TO #PROCD-DATE ( #PROCD-ARRAY-CNT )
                    //*  VR 09/15/2017 END
                    pnd_Output_Workfile1_Pnd_O1_Cmnct_Cd.reset();                                                                                                         //Natural: RESET #O1-CMNCT-CD #O1-PRTY-ID #O1-WPID #O1-MOC #O1-STATUS-CDE
                    pnd_Output_Workfile1_Pnd_O1_Prty_Id.reset();
                    pnd_Output_Workfile1_Pnd_O1_Wpid.reset();
                    pnd_Output_Workfile1_Pnd_O1_Moc.reset();
                    pnd_Output_Workfile1_Pnd_O1_Status_Cde.reset();
                    //*      MOVE R1.PIN-NBR             TO #O1-PRTY-ID  /* PIN-EXP
                    //*  PIN-EXP
                    pnd_Output_Workfile1_Pnd_O1_Prty_Id_N7.setValue(master_Pin_Nbr);                                                                                      //Natural: MOVE MASTER.PIN-NBR TO #O1-PRTY-ID-N7
                    pnd_Output_Workfile1_Pnd_O1_Cmnct_Cd.setValue("BENE_PROCD");                                                                                          //Natural: MOVE 'BENE_PROCD' TO #O1-CMNCT-CD
                    pnd_Output_Workfile1_Pnd_O1_Wpid.setValue(master_Work_Prcss_Id);                                                                                      //Natural: MOVE MASTER.WORK-PRCSS-ID TO #O1-WPID
                    pnd_Output_Workfile1_Pnd_O1_Moc.setValue(master_Rqst_Orgn_Cde);                                                                                       //Natural: MOVE MASTER.RQST-ORGN-CDE TO #O1-MOC
                    pnd_Output_Workfile1_Pnd_O1_Status_Cde.setValue(master_Status_Cde);                                                                                   //Natural: MOVE MASTER.STATUS-CDE TO #O1-STATUS-CDE
                    pnd_Output_Workfile1_Pnd_O1_Stg_Id_Seq_No_N.nadd(1);                                                                                                  //Natural: ADD 1 TO #O1-STG-ID-SEQ-NO-N
                    pnd_Rec_Count.nadd(1);                                                                                                                                //Natural: ADD 1 TO #REC-COUNT
                    getWorkFiles().write(1, false, pnd_Output_Workfile1);                                                                                                 //Natural: WRITE WORK FILE 1 #OUTPUT-WORKFILE1
                    //*      PRINT '=' #OUTPUT-WORKFILE1
                    pnd_Output_Workfile1_Final.reset();                                                                                                                   //Natural: RESET #OUTPUT-WORKFILE1-FINAL
                    pnd_Output_Workfile1_Final.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Output_Workfile1_Pnd_O1_Stg_Id, "|", pnd_Output_Workfile1_Pnd_O1_Cmnct_Cd,  //Natural: COMPRESS #O1-STG-ID '|' #O1-CMNCT-CD '|' #O1-PRTY-ID-N7 '|' #O1-PRTY-ID-TYP-CD '|' #O1-PRTY-TYP-CD '|' #O1-TRG-TYP-CD '|' #O1-EFF-FRM-DT '|' #O1-EFF-TO-DT '|' #O1-SYST-CD '|' #O1-TM-PRD-TYP-CD INTO #OUTPUT-WORKFILE1-FINAL LEAVING NO
                        "|", pnd_Output_Workfile1_Pnd_O1_Prty_Id_N7, "|", pnd_Output_Workfile1_Pnd_O1_Prty_Id_Typ_Cd, "|", pnd_Output_Workfile1_Pnd_O1_Prty_Typ_Cd, 
                        "|", pnd_Output_Workfile1_Pnd_O1_Trg_Typ_Cd, "|", pnd_Output_Workfile1_Pnd_O1_Eff_Frm_Dt, "|", pnd_Output_Workfile1_Pnd_O1_Eff_To_Dt, 
                        "|", pnd_Output_Workfile1_Pnd_O1_Syst_Cd, "|", pnd_Output_Workfile1_Pnd_O1_Tm_Prd_Typ_Cd));
                    getWorkFiles().write(2, false, pnd_Output_Workfile1_Final);                                                                                           //Natural: WRITE WORK FILE 2 #OUTPUT-WORKFILE1-FINAL
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().print(0, "Number of Processed Records :",pnd_Rec_Count, new ReportEditMask ("Z,ZZZ,ZZ9"));                                                           //Natural: PRINT 'Number of Processed Records :' #REC-COUNT ( EM = Z,ZZZ,ZZ9 )
    }
    private void sub_Get_Recvd_Details() throws Exception                                                                                                                 //Natural: GET-RECVD-DETAILS
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------
        pnd_Last_Chnge_Dte_Tme.reset();                                                                                                                                   //Natural: RESET #LAST-CHNGE-DTE-TME #REC-COUNT
        pnd_Rec_Count.reset();
        pnd_Last_Chnge_Dte_Tme_Pnd_Last_Chnge_Dte.setValue(pnd_Start_Date);                                                                                               //Natural: ASSIGN #LAST-CHNGE-DTE := #START-DATE
        pnd_End_Date_Pnd_End_Date_A.setValueEdited(pnd_End_Date_D,new ReportEditMask("YYYYMMDD"));                                                                        //Natural: MOVE EDITED #END-DATE-D ( EM = YYYYMMDD ) TO #END-DATE-A
        getReports().print(0, "READ KEY =",pnd_Last_Chnge_Dte_Tme);                                                                                                       //Natural: PRINT 'READ KEY =' #LAST-CHNGE-DTE-TME
        vw_master.startDatabaseRead                                                                                                                                       //Natural: READ MASTER BY CHNGE-DTE-TME-LOG-DTE-TME-KEY = #LAST-CHNGE-DTE-TME
        (
        "R2",
        new Wc[] { new Wc("CHNGE_DTE_TME_LOG_DTE_TME_KEY", ">=", pnd_Last_Chnge_Dte_Tme, WcType.BY) },
        new Oc[] { new Oc("CHNGE_DTE_TME_LOG_DTE_TME_KEY", "ASC") }
        );
        R2:
        while (condition(vw_master.readNextRow("R2")))
        {
            if (condition(master_Last_Chnge_Dte.greater(pnd_End_Date)))                                                                                                   //Natural: IF MASTER.LAST-CHNGE-DTE GT #END-DATE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(master_Work_Prcss_Id.notEquals("TA MB") && master_Work_Prcss_Id.notEquals("TA MBZ") && master_Work_Prcss_Id.notEquals("TAIMB")                  //Natural: IF MASTER.WORK-PRCSS-ID NE 'TA MB' AND MASTER.WORK-PRCSS-ID NE 'TA MBZ' AND MASTER.WORK-PRCSS-ID NE 'TAIMB' AND MASTER.WORK-PRCSS-ID NE 'TAIMBZ'
                && master_Work_Prcss_Id.notEquals("TAIMBZ")))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(master_Rqst_Orgn_Cde.notEquals("C") && master_Rqst_Orgn_Cde.notEquals("M") && master_Rqst_Orgn_Cde.notEquals("V") && master_Rqst_Orgn_Cde.notEquals("X"))) //Natural: IF MASTER.RQST-ORGN-CDE NE 'C' AND MASTER.RQST-ORGN-CDE NE 'M' AND MASTER.RQST-ORGN-CDE NE 'V' AND MASTER.RQST-ORGN-CDE NE 'X'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(master_Status_Cde.equals("1000") || master_Status_Cde.equals("2000") || master_Status_Cde.equals("2100")))                                      //Natural: IF MASTER.STATUS-CDE EQ '1000' OR MASTER.STATUS-CDE EQ '2000' OR MASTER.STATUS-CDE EQ '2100'
            {
                //*    PRINT '-' (72)
                //*    PRINT R2.RQST-LOG-DTE-TME   ','
                //*          R2.LAST-CHNGE-DTE     ','
                //*          R2.PIN-NBR            ','
                //*          R2.WORK-PRCSS-ID      ','
                //*          R2.STATUS-CDE         ','
                //*    WRITE WORK 6 R2.RQST-LOG-DTE-TME   ','
                //*          R2.LAST-CHNGE-DTE-TME ','
                //*          R2.PIN-NBR            ','
                //*          R2.WORK-PRCSS-ID      ','
                //*          R2.STATUS-CDE         ','
                pnd_Recvd_Rec_Found.reset();                                                                                                                              //Natural: RESET #RECVD-REC-FOUND
                FOR02:                                                                                                                                                    //Natural: FOR #I 1 TO #RECVD-ARRAY-CNT
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Recvd_Array_Cnt)); pnd_I.nadd(1))
                {
                    if (condition(master_Rqst_Log_Dte_Tme.equals(pnd_Recvd_Array_Pnd_Recvd_Rqst_Log_Dte_Tme.getValue(pnd_I))))                                            //Natural: IF MASTER.RQST-LOG-DTE-TME EQ #RECVD-RQST-LOG-DTE-TME ( #I )
                    {
                        pnd_Recvd_Rec_Found.setValue(true);                                                                                                               //Natural: ASSIGN #RECVD-REC-FOUND := TRUE
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    //*      PRINT #I ',' #RECVD-DATE(#I) ',' #RECVD-PIN-NBR(#I) ','
                    //*            #RECVD-WPID(#I) ','
                    //*  VR 09/15/2017 START
                    if (condition(master_Pin_Nbr.equals(pnd_Recvd_Array_Pnd_Recvd_Pin_Nbr.getValue(pnd_I)) && master_Work_Prcss_Id.equals(pnd_Recvd_Array_Pnd_Recvd_Wpid.getValue(pnd_I))  //Natural: IF MASTER.PIN-NBR EQ #RECVD-PIN-NBR ( #I ) AND MASTER.WORK-PRCSS-ID EQ #RECVD-WPID ( #I ) AND MASTER.#RQST-LOG-DTE EQ #RECVD-DATE ( #I )
                        && master_Pnd_Rqst_Log_Dte.equals(pnd_Recvd_Array_Pnd_Recvd_Date.getValue(pnd_I))))
                    {
                        pnd_Recvd_Rec_Found.setValue(true);                                                                                                               //Natural: ASSIGN #RECVD-REC-FOUND := TRUE
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    //*  VR 09/15/2017 END
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_R_Rqst_Log_Dte_Tme.reset();                                                                                                                           //Natural: RESET #R-RQST-LOG-DTE-TME #DUP-R-REC-CNT
                pnd_Dup_R_Rec_Cnt.reset();
                pnd_R_Rqst_Log_Dte_Tme.setValue(master_Rqst_Log_Dte_Tme);                                                                                                 //Natural: MOVE MASTER.RQST-LOG-DTE-TME TO #R-RQST-LOG-DTE-TME
                                                                                                                                                                          //Natural: PERFORM CHECK-DUPLICATE-RECVD
                sub_Check_Duplicate_Recvd();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*    PRINT '=' #RECVD-REC-FOUND '=' #DUP-R-REC-CNT
                if (condition(! (pnd_Recvd_Rec_Found.getBoolean()) && pnd_Dup_R_Rec_Cnt.equals(getZero())))                                                               //Natural: IF NOT #RECVD-REC-FOUND AND #DUP-R-REC-CNT EQ 0
                {
                    pnd_Recvd_Array_Cnt.nadd(1);                                                                                                                          //Natural: ADD 1 TO #RECVD-ARRAY-CNT
                    pnd_Recvd_Array_Pnd_Recvd_Rqst_Log_Dte_Tme.getValue(pnd_Recvd_Array_Cnt).setValue(master_Rqst_Log_Dte_Tme);                                           //Natural: MOVE MASTER.RQST-LOG-DTE-TME TO #RECVD-RQST-LOG-DTE-TME ( #RECVD-ARRAY-CNT )
                    pnd_Recvd_Array_Pnd_Recvd_Pin_Nbr.getValue(pnd_Recvd_Array_Cnt).setValue(master_Pin_Nbr);                                                             //Natural: MOVE MASTER.PIN-NBR TO #RECVD-PIN-NBR ( #RECVD-ARRAY-CNT )
                    //*  VR 09/15/2017 START
                    pnd_Recvd_Array_Pnd_Recvd_Wpid.getValue(pnd_Recvd_Array_Cnt).setValue(master_Work_Prcss_Id);                                                          //Natural: MOVE MASTER.WORK-PRCSS-ID TO #RECVD-WPID ( #RECVD-ARRAY-CNT )
                    pnd_Recvd_Array_Pnd_Recvd_Date.getValue(pnd_Recvd_Array_Cnt).setValue(master_Pnd_Rqst_Log_Dte);                                                       //Natural: MOVE MASTER.#RQST-LOG-DTE TO #RECVD-DATE ( #RECVD-ARRAY-CNT )
                    //*  VR 09/15/2017 END
                    pnd_Output_Workfile1_Pnd_O1_Cmnct_Cd.reset();                                                                                                         //Natural: RESET #O1-CMNCT-CD #O1-PRTY-ID #O1-WPID #O1-MOC #O1-STATUS-CDE
                    pnd_Output_Workfile1_Pnd_O1_Prty_Id.reset();
                    pnd_Output_Workfile1_Pnd_O1_Wpid.reset();
                    pnd_Output_Workfile1_Pnd_O1_Moc.reset();
                    pnd_Output_Workfile1_Pnd_O1_Status_Cde.reset();
                    //*      MOVE R2.PIN-NBR                    TO #O1-PRTY-ID
                    pnd_Output_Workfile1_Pnd_O1_Prty_Id_N7.setValue(master_Pin_Nbr);                                                                                      //Natural: MOVE MASTER.PIN-NBR TO #O1-PRTY-ID-N7
                    pnd_Output_Workfile1_Pnd_O1_Cmnct_Cd.setValue("BENE_PAPER_RCVD");                                                                                     //Natural: MOVE 'BENE_PAPER_RCVD' TO #O1-CMNCT-CD
                    pnd_Output_Workfile1_Pnd_O1_Wpid.setValue(master_Work_Prcss_Id);                                                                                      //Natural: MOVE MASTER.WORK-PRCSS-ID TO #O1-WPID
                    pnd_Output_Workfile1_Pnd_O1_Moc.setValue(master_Rqst_Orgn_Cde);                                                                                       //Natural: MOVE MASTER.RQST-ORGN-CDE TO #O1-MOC
                    pnd_Output_Workfile1_Pnd_O1_Status_Cde.setValue(master_Status_Cde);                                                                                   //Natural: MOVE MASTER.STATUS-CDE TO #O1-STATUS-CDE
                    pnd_Output_Workfile1_Pnd_O1_Stg_Id_Seq_No_N.nadd(1);                                                                                                  //Natural: ADD 1 TO #O1-STG-ID-SEQ-NO-N
                    pnd_Rec_Count.nadd(1);                                                                                                                                //Natural: ADD 1 TO #REC-COUNT
                    getWorkFiles().write(1, false, pnd_Output_Workfile1);                                                                                                 //Natural: WRITE WORK FILE 1 #OUTPUT-WORKFILE1
                    //*      PRINT '=' #OUTPUT-WORKFILE1
                    pnd_Output_Workfile1_Final.reset();                                                                                                                   //Natural: RESET #OUTPUT-WORKFILE1-FINAL
                    pnd_Output_Workfile1_Final.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Output_Workfile1_Pnd_O1_Stg_Id, "|", pnd_Output_Workfile1_Pnd_O1_Cmnct_Cd,  //Natural: COMPRESS #O1-STG-ID '|' #O1-CMNCT-CD '|' #O1-PRTY-ID-N7 '|' #O1-PRTY-ID-TYP-CD '|' #O1-PRTY-TYP-CD '|' #O1-TRG-TYP-CD '|' #O1-EFF-FRM-DT '|' #O1-EFF-TO-DT '|' #O1-SYST-CD '|' #O1-TM-PRD-TYP-CD INTO #OUTPUT-WORKFILE1-FINAL LEAVING NO
                        "|", pnd_Output_Workfile1_Pnd_O1_Prty_Id_N7, "|", pnd_Output_Workfile1_Pnd_O1_Prty_Id_Typ_Cd, "|", pnd_Output_Workfile1_Pnd_O1_Prty_Typ_Cd, 
                        "|", pnd_Output_Workfile1_Pnd_O1_Trg_Typ_Cd, "|", pnd_Output_Workfile1_Pnd_O1_Eff_Frm_Dt, "|", pnd_Output_Workfile1_Pnd_O1_Eff_To_Dt, 
                        "|", pnd_Output_Workfile1_Pnd_O1_Syst_Cd, "|", pnd_Output_Workfile1_Pnd_O1_Tm_Prd_Typ_Cd));
                    getWorkFiles().write(2, false, pnd_Output_Workfile1_Final);                                                                                           //Natural: WRITE WORK FILE 2 #OUTPUT-WORKFILE1-FINAL
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().print(0, "Number of Received Records:",pnd_Rec_Count, new ReportEditMask ("Z,ZZZ,ZZ9"));                                                             //Natural: PRINT 'Number of Received Records:' #REC-COUNT ( EM = Z,ZZZ,ZZ9 )
    }
    private void sub_Alert2_3_Report_Generation() throws Exception                                                                                                        //Natural: ALERT2-3-REPORT-GENERATION
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------------
        getReports().print(0, "-",new RepeatItem(72));                                                                                                                    //Natural: PRINT '-' ( 72 )
        getReports().print(0, "BEFORE MDM CALL",Global.getDATX(),Global.getTIMX());                                                                                       //Natural: PRINT 'BEFORE MDM CALL' *DATX *TIMX
        pnd_Rec_Count_Work2.reset();                                                                                                                                      //Natural: RESET #REC-COUNT-WORK2
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #OUTPUT-WORKFILE1
        while (condition(getWorkFiles().read(1, pnd_Output_Workfile1)))
        {
            //*  PRINT '-' (72)
            //*  PRINT '=' #OUTPUT-WORKFILE1
            //*  RESET #MDMA110             /*MDM PIN EXPANSION
            //* MDM PIN EXPANSION
            //* MDM PIN EXPANSION
            pdaMdma111.getPnd_Mdma111().reset();                                                                                                                          //Natural: RESET #MDMA111
            pdaMdma111.getPnd_Mdma111_Pnd_I_Pin_N12().setValue(pnd_Output_Workfile1_Pnd_O1_Prty_Id_N7);                                                                   //Natural: ASSIGN #MDMA111.#I-PIN-N12 := #O1-PRTY-ID-N7
            if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                            //Natural: IF *DEVICE = 'BATCH'
            {
                //*    CALLNAT 'MDMN110A' #MDMA110
                //* MDM PIN EXPANSION
                DbsUtil.callnat(Mdmn111a.class , getCurrentProcessState(), pdaMdma111.getPnd_Mdma111());                                                                  //Natural: CALLNAT 'MDMN111A' #MDMA111
                if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                //* MDM PIN EXPANSION
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*    CALLNAT 'MDMN110' #MDMA110 /*MDM PIN EXPANSION
                //* MDM PIN EXPANSION
                DbsUtil.callnat(Mdmn111.class , getCurrentProcessState(), pdaMdma111.getPnd_Mdma111());                                                                   //Natural: CALLNAT 'MDMN111' #MDMA111
                if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
            }                                                                                                                                                             //Natural: END-IF
            //*  PRINT '=' #MDMA110.#O-RETURN-CODE
            //*  IF #MDMA110.#O-RETURN-CODE EQ '0000'
            //* MDM PIN EXPANSION
            if (condition(pdaMdma111.getPnd_Mdma111_Pnd_O_Return_Code().equals("0000")))                                                                                  //Natural: IF #MDMA111.#O-RETURN-CODE EQ '0000'
            {
                //* MDM PIN EXPANSION
                pnd_Rec_Count_Work2.nadd(1);                                                                                                                              //Natural: ADD 1 TO #REC-COUNT-WORK2
                //*    PRINT '=' #REC-COUNT-WORK2
                pnd_Output_Workfile2.reset();                                                                                                                             //Natural: RESET #OUTPUT-WORKFILE2
                pnd_Output_Workfile2.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Output_Workfile1_Pnd_O1_Stg_Id, "|PFNAME|", pdaMdma111.getPnd_Mdma111_Pnd_O_First_Name())); //Natural: COMPRESS #O1-STG-ID '|PFNAME|' #O-FIRST-NAME INTO #OUTPUT-WORKFILE2 LEAVING NO
                getWorkFiles().write(3, false, pnd_Output_Workfile2);                                                                                                     //Natural: WRITE WORK FILE 3 #OUTPUT-WORKFILE2
                pnd_Output_Workfile2.reset();                                                                                                                             //Natural: RESET #OUTPUT-WORKFILE2
                pnd_Output_Workfile2.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Output_Workfile1_Pnd_O1_Stg_Id, "|PLNAME|", pdaMdma111.getPnd_Mdma111_Pnd_O_Last_Name())); //Natural: COMPRESS #O1-STG-ID '|PLNAME|' #O-LAST-NAME INTO #OUTPUT-WORKFILE2 LEAVING NO
                getWorkFiles().write(3, false, pnd_Output_Workfile2);                                                                                                     //Natural: WRITE WORK FILE 3 #OUTPUT-WORKFILE2
                pnd_Output_Workfile2.reset();                                                                                                                             //Natural: RESET #OUTPUT-WORKFILE2
                pnd_Output_Workfile2.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Output_Workfile1_Pnd_O1_Stg_Id, "|BENEWPID|", pnd_Output_Workfile1_Pnd_O1_Wpid)); //Natural: COMPRESS #O1-STG-ID '|BENEWPID|' #O1-WPID INTO #OUTPUT-WORKFILE2 LEAVING NO
                getWorkFiles().write(3, false, pnd_Output_Workfile2);                                                                                                     //Natural: WRITE WORK FILE 3 #OUTPUT-WORKFILE2
                pnd_Output_Workfile2.reset();                                                                                                                             //Natural: RESET #OUTPUT-WORKFILE2
                pnd_Output_Workfile2.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Output_Workfile1_Pnd_O1_Stg_Id, "|BENEMOC|", pnd_Output_Workfile1_Pnd_O1_Moc)); //Natural: COMPRESS #O1-STG-ID '|BENEMOC|' #O1-MOC INTO #OUTPUT-WORKFILE2 LEAVING NO
                getWorkFiles().write(3, false, pnd_Output_Workfile2);                                                                                                     //Natural: WRITE WORK FILE 3 #OUTPUT-WORKFILE2
                pnd_Output_Workfile2.reset();                                                                                                                             //Natural: RESET #OUTPUT-WORKFILE2
                pnd_Output_Workfile2.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Output_Workfile1_Pnd_O1_Stg_Id, "|BENESTATUS|", pnd_Output_Workfile1_Pnd_O1_Status_Cde)); //Natural: COMPRESS #O1-STG-ID '|BENESTATUS|' #O1-STATUS-CDE INTO #OUTPUT-WORKFILE2 LEAVING NO
                getWorkFiles().write(3, false, pnd_Output_Workfile2);                                                                                                     //Natural: WRITE WORK FILE 3 #OUTPUT-WORKFILE2
                pnd_Output_Workfile3.reset();                                                                                                                             //Natural: RESET #OUTPUT-WORKFILE3
                pnd_Output_Workfile3.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Output_Workfile1_Pnd_O1_Stg_Id, "|EMAIL|", pdaMdma111.getPnd_Mdma111_Pnd_O_Primary_Email_Address())); //Natural: COMPRESS #O1-STG-ID '|EMAIL|' #O-PRIMARY-EMAIL-ADDRESS INTO #OUTPUT-WORKFILE3 LEAVING NO
                getWorkFiles().write(4, false, pnd_Output_Workfile3);                                                                                                     //Natural: WRITE WORK FILE 4 #OUTPUT-WORKFILE3
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().print(0, "-",new RepeatItem(72));                                                                                                            //Natural: PRINT '-' ( 72 )
                getReports().print(0, "=",pnd_Output_Workfile1);                                                                                                          //Natural: PRINT '=' #OUTPUT-WORKFILE1
                //*    PRINT 'MDMN110A: PIN' #O1-PRTY-ID-N7 'NOT FOUND'
                //* MDM PIN EXPANSION
                //* MDM PIN EXPANSION
                getReports().print(0, "MDMN111A: PIN",pnd_Output_Workfile1_Pnd_O1_Prty_Id_N7,"NOT FOUND","=",pdaMdma111.getPnd_Mdma111_Pnd_O_Return_Text());              //Natural: PRINT 'MDMN111A: PIN' #O1-PRTY-ID-N7 'NOT FOUND' '=' #MDMA111.#O-RETURN-TEXT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getReports().print(0, "AFTER MDM CALL",Global.getDATX(),Global.getTIMX());                                                                                        //Natural: PRINT 'AFTER MDM CALL' *DATX *TIMX
    }
    private void sub_Check_Duplicate_Procd() throws Exception                                                                                                             //Natural: CHECK-DUPLICATE-PROCD
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------------
        pnd_Dup_P_Rec_Cnt.reset();                                                                                                                                        //Natural: RESET #DUP-P-REC-CNT
        vw_master2.startDatabaseRead                                                                                                                                      //Natural: READ MASTER2 BY RQST-HISTORY-KEY = #P-RQST-LOG-DTE-TME
        (
        "R3",
        new Wc[] { new Wc("RQST_HISTORY_KEY", ">=", pnd_P_Rqst_Log_Dte_Tme, WcType.BY) },
        new Oc[] { new Oc("RQST_HISTORY_KEY", "ASC") }
        );
        R3:
        while (condition(vw_master2.readNextRow("R3")))
        {
            if (condition(master2_Rqst_Log_Dte_Tme.notEquals(pnd_P_Rqst_Log_Dte_Tme)))                                                                                    //Natural: IF MASTER2.RQST-LOG-DTE-TME NE #P-RQST-LOG-DTE-TME
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(master2_Work_Prcss_Id.notEquals("TA MB") && master2_Work_Prcss_Id.notEquals("TA MBW") && master2_Work_Prcss_Id.notEquals("TA MBZ")              //Natural: IF MASTER2.WORK-PRCSS-ID NE 'TA MB' AND MASTER2.WORK-PRCSS-ID NE 'TA MBW' AND MASTER2.WORK-PRCSS-ID NE 'TA MBZ' AND MASTER2.WORK-PRCSS-ID NE 'TAIMB' AND MASTER2.WORK-PRCSS-ID NE 'TAIMBW' AND MASTER2.WORK-PRCSS-ID NE 'TAIMBZ'
                && master2_Work_Prcss_Id.notEquals("TAIMB") && master2_Work_Prcss_Id.notEquals("TAIMBW") && master2_Work_Prcss_Id.notEquals("TAIMBZ")))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(master2_Rqst_Orgn_Cde.notEquals("C") && master2_Rqst_Orgn_Cde.notEquals("M") && master2_Rqst_Orgn_Cde.notEquals("N") && master2_Rqst_Orgn_Cde.notEquals("V")  //Natural: IF MASTER2.RQST-ORGN-CDE NE 'C' AND MASTER2.RQST-ORGN-CDE NE 'M' AND MASTER2.RQST-ORGN-CDE NE 'N' AND MASTER2.RQST-ORGN-CDE NE 'V' AND MASTER2.RQST-ORGN-CDE NE 'X'
                && master2_Rqst_Orgn_Cde.notEquals("X")))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(master2_Status_Cde.equals("7501") || master2_Status_Cde.equals("7502") || master2_Status_Cde.equals("8000") || master2_Status_Cde.equals("8010")  //Natural: IF MASTER2.STATUS-CDE EQ '7501' OR MASTER2.STATUS-CDE EQ '7502' OR MASTER2.STATUS-CDE EQ '8000' OR MASTER2.STATUS-CDE EQ '8010' OR MASTER2.STATUS-CDE EQ '8350' OR MASTER2.STATUS-CDE EQ '8400'
                || master2_Status_Cde.equals("8350") || master2_Status_Cde.equals("8400")))
            {
                if (condition(master2_Last_Chnge_Dte.less(pnd_Last_Chnge_Dte_Tme_Pnd_Last_Chnge_Dte)))                                                                    //Natural: IF MASTER2.LAST-CHNGE-DTE LT #LAST-CHNGE-DTE
                {
                    //*      PRINT '-' (25)
                    //*      PRINT '=' R3.RQST-LOG-DTE-TME / R3.PIN-NBR / R3.STATUS-CDE
                    pnd_Dup_P_Rec_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #DUP-P-REC-CNT
                    if (true) break R3;                                                                                                                                   //Natural: ESCAPE BOTTOM ( R3. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Check_Duplicate_Recvd() throws Exception                                                                                                             //Natural: CHECK-DUPLICATE-RECVD
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------------
        pnd_Dup_R_Rec_Cnt.reset();                                                                                                                                        //Natural: RESET #DUP-R-REC-CNT
        vw_master2.startDatabaseRead                                                                                                                                      //Natural: READ MASTER2 BY RQST-HISTORY-KEY = #R-RQST-LOG-DTE-TME
        (
        "R4",
        new Wc[] { new Wc("RQST_HISTORY_KEY", ">=", pnd_R_Rqst_Log_Dte_Tme, WcType.BY) },
        new Oc[] { new Oc("RQST_HISTORY_KEY", "ASC") }
        );
        R4:
        while (condition(vw_master2.readNextRow("R4")))
        {
            if (condition(master2_Rqst_Log_Dte_Tme.notEquals(pnd_R_Rqst_Log_Dte_Tme)))                                                                                    //Natural: IF MASTER2.RQST-LOG-DTE-TME NE #R-RQST-LOG-DTE-TME
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(master2_Work_Prcss_Id.notEquals("TA MB") && master2_Work_Prcss_Id.notEquals("TA MBZ") && master2_Work_Prcss_Id.notEquals("TAIMB")               //Natural: IF MASTER2.WORK-PRCSS-ID NE 'TA MB' AND MASTER2.WORK-PRCSS-ID NE 'TA MBZ' AND MASTER2.WORK-PRCSS-ID NE 'TAIMB' AND MASTER2.WORK-PRCSS-ID NE 'TAIMBZ'
                && master2_Work_Prcss_Id.notEquals("TAIMBZ")))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(master2_Rqst_Orgn_Cde.notEquals("C") && master2_Rqst_Orgn_Cde.notEquals("M") && master2_Rqst_Orgn_Cde.notEquals("V") && master2_Rqst_Orgn_Cde.notEquals("X"))) //Natural: IF MASTER2.RQST-ORGN-CDE NE 'C' AND MASTER2.RQST-ORGN-CDE NE 'M' AND MASTER2.RQST-ORGN-CDE NE 'V' AND MASTER2.RQST-ORGN-CDE NE 'X'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(master2_Status_Cde.equals("1000") || master2_Status_Cde.equals("2000") || master2_Status_Cde.equals("2100")))                                   //Natural: IF MASTER2.STATUS-CDE EQ '1000' OR MASTER2.STATUS-CDE EQ '2000' OR MASTER2.STATUS-CDE EQ '2100'
            {
                if (condition(master2_Last_Chnge_Dte.less(pnd_Last_Chnge_Dte_Tme_Pnd_Last_Chnge_Dte)))                                                                    //Natural: IF MASTER2.LAST-CHNGE-DTE LT #LAST-CHNGE-DTE
                {
                    //*      PRINT '-' (25)
                    //*      PRINT '=' R4.RQST-LOG-DTE-TME / R4.PIN-NBR / R4.STATUS-CDE
                    pnd_Dup_R_Rec_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #DUP-R-REC-CNT
                    if (true) break R4;                                                                                                                                   //Natural: ESCAPE BOTTOM ( R4. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Update_Run_Date() throws Exception                                                                                                                   //Natural: UPDATE-RUN-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------
        pnd_Rec_Count.reset();                                                                                                                                            //Natural: RESET #REC-COUNT #TBL-PRIME-KEY
        pnd_Tbl_Prime_Key.reset();
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind.setValue("A ");                                                                                                         //Natural: ASSIGN #TBL-SCRTY-LEVEL-IND := 'A '
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme.setValue("PCW4700D-RUN-DATE");                                                                                                //Natural: ASSIGN #TBL-TABLE-NME := 'PCW4700D-RUN-DATE'
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field.setValue("PCW4700D-RUN-DATE-KEY");                                                                                            //Natural: ASSIGN #TBL-KEY-FIELD := 'PCW4700D-RUN-DATE-KEY'
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind.setValue(" ");                                                                                                                //Natural: ASSIGN #TBL-ACTVE-IND := ' '
        vw_cwf_Support_Tbl.startDatabaseFind                                                                                                                              //Natural: FIND CWF-SUPPORT-TBL WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
        (
        "F2",
        new Wc[] { new Wc("TBL_PRIME_KEY", "=", pnd_Tbl_Prime_Key, WcType.WITH) }
        );
        F2:
        while (condition(vw_cwf_Support_Tbl.readNextRow("F2", true)))
        {
            vw_cwf_Support_Tbl.setIfNotFoundControlFlag(false);
            if (condition(vw_cwf_Support_Tbl.getAstCOUNTER().equals(0)))                                                                                                  //Natural: IF NO RECORDS FOUND
            {
                getReports().print(0, "Update: No PCW4700D-RUN-DATE Record Found!");                                                                                      //Natural: PRINT 'Update: No PCW4700D-RUN-DATE Record Found!'
                DbsUtil.terminate(4);  if (true) return;                                                                                                                  //Natural: TERMINATE 4
            }                                                                                                                                                             //Natural: END-NOREC
            G1:                                                                                                                                                           //Natural: GET CWF-SUPPORT-TBL *ISN ( F2. )
            vw_cwf_Support_Tbl.readByID(vw_cwf_Support_Tbl.getAstISN("F2"), "G1");
            cwf_Support_Tbl_Pnd_Tbl_Data_Field1.setValue(Global.getDATN());                                                                                               //Natural: MOVE *DATN TO CWF-SUPPORT-TBL.#TBL-DATA-FIELD1
            cwf_Support_Tbl_Tbl_Updte_Dte.setValue(Global.getDATX());                                                                                                     //Natural: MOVE *DATX TO CWF-SUPPORT-TBL.TBL-UPDTE-DTE
            cwf_Support_Tbl_Tbl_Updte_Dte_Tme.setValue(Global.getTIMX());                                                                                                 //Natural: MOVE *TIMX TO CWF-SUPPORT-TBL.TBL-UPDTE-DTE-TME
            cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde.setValue("PCW4700D");                                                                                                     //Natural: MOVE 'PCW4700D' TO CWF-SUPPORT-TBL.TBL-UPDTE-OPRTR-CDE
            vw_cwf_Support_Tbl.updateDBRow("G1");                                                                                                                         //Natural: UPDATE ( G1. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            //*  (F2.)
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        pnd_Error_Msg.setValue(DbsUtil.compress("Natural Error", Global.getERROR_NR(), "on line", Global.getERROR_LINE(), "of", Global.getPROGRAM()));                    //Natural: COMPRESS 'Natural Error' *ERROR-NR 'on line' *ERROR-LINE 'of' *PROGRAM INTO #ERROR-MSG
        getReports().print(0, "-",new RepeatItem(72));                                                                                                                    //Natural: PRINT '-' ( 72 )
        getReports().print(0, pnd_Error_Msg);                                                                                                                             //Natural: PRINT #ERROR-MSG
        getReports().print(0, "-",new RepeatItem(72));                                                                                                                    //Natural: PRINT '-' ( 72 )
        DbsUtil.terminate(4);  if (true) return;                                                                                                                          //Natural: TERMINATE 4
    };                                                                                                                                                                    //Natural: END-ERROR
}
