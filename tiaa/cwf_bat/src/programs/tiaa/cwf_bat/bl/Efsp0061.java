/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:11:13 PM
**        * FROM NATURAL PROGRAM : Efsp0061
************************************************************
**        * FILE NAME            : Efsp0061.java
**        * CLASS NAME           : Efsp0061
**        * INSTANCE NAME        : Efsp0061
************************************************************
************************************************************************
* PROGRAM  : EFSP0061
* SYSTEM   : CRPCWF
* TITLE    : PRINT IMAGE UPLOAD AUDIT REPORT (UPLOAD DATE/TIME ORDER)
* GENERATED: JUN 28,93 AT 10:00 AM
* FUNCTION : THIS PROGRAM READS THE CWF-UPLOAD-AUDIT FILE AND PRINTS
*            THE AUDIT REPORT SORTED BY UPLOAD DATE/TIME
* HISTORY
* 04/28/95  BE  ADDED TOTAL NO. OF MINS WITHOUT A PIN IN THE REPORT.
* 05/13/96  BE  WRITE 'Nothing to Report' IF THERE ARE NO RECS TO RPT.
* 03/31/97  CS  FORCE LANDSCAPE PRINT MODE IF SUBMITTED ONLINE
* 12/17/97  CS  INCLUDED 'FORMS' WHEN REPORTING 'CIRS'
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsp0061 extends BLNatBase
{
    // Data Areas
    private GdaCwfg000 gdaCwfg000;
    private LdaEfsl0040 ldaEfsl0040;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_I;
    private DbsField pnd_Lines_Reqd;
    private DbsField pnd_Program;

    private DbsGroup pnd_Total;
    private DbsField pnd_Total_Pnd_Batch_Count;
    private DbsField pnd_Total_Pnd_Mins_Count;
    private DbsField pnd_Total_Pnd_Mins_No_Pin_Count;
    private DbsField pnd_Total_Pnd_Images_Count;
    private DbsField pnd_Input_Date_Time;
    private DbsField pnd_Input_Date_D;
    private DbsField pnd_Start_Key;
    private DbsField pnd_End_Key;
    private DbsField pnd_Input_Data;

    private DbsGroup pnd_Input_Data__R_Field_1;
    private DbsField pnd_Input_Data_Pnd_Input_Time_Start;
    private DbsField pnd_Input_Data_Pnd_Input_Time_End;
    private DbsField pnd_Input_Data_Pnd_Input_Date;
    private DbsField pnd_Input_Source_Id;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_2;
    private DbsField cwf_Support_Tbl_Pnd_Regular_Run_Parms;
    private DbsField cwf_Support_Tbl_Pnd_Override_Time_Parms;
    private DbsField cwf_Support_Tbl_Pnd_Override_Flag;
    private DbsField cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_3;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind;
    private DbsField pnd_Nothing_To_Rpt;
    private DbsField pnd_Record;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaCwfg000 = GdaCwfg000.getInstance(getCallnatLevel());
        registerRecord(gdaCwfg000);
        if (gdaOnly) return;

        ldaEfsl0040 = new LdaEfsl0040();
        registerRecord(ldaEfsl0040);
        registerRecord(ldaEfsl0040.getVw_cwf_Upload_Audit());

        // Local Variables
        localVariables = new DbsRecord();
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Lines_Reqd = localVariables.newFieldInRecord("pnd_Lines_Reqd", "#LINES-REQD", FieldType.PACKED_DECIMAL, 3);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);

        pnd_Total = localVariables.newGroupInRecord("pnd_Total", "#TOTAL");
        pnd_Total_Pnd_Batch_Count = pnd_Total.newFieldInGroup("pnd_Total_Pnd_Batch_Count", "#BATCH-COUNT", FieldType.NUMERIC, 5);
        pnd_Total_Pnd_Mins_Count = pnd_Total.newFieldInGroup("pnd_Total_Pnd_Mins_Count", "#MINS-COUNT", FieldType.NUMERIC, 7);
        pnd_Total_Pnd_Mins_No_Pin_Count = pnd_Total.newFieldInGroup("pnd_Total_Pnd_Mins_No_Pin_Count", "#MINS-NO-PIN-COUNT", FieldType.NUMERIC, 7);
        pnd_Total_Pnd_Images_Count = pnd_Total.newFieldInGroup("pnd_Total_Pnd_Images_Count", "#IMAGES-COUNT", FieldType.NUMERIC, 7);
        pnd_Input_Date_Time = localVariables.newFieldInRecord("pnd_Input_Date_Time", "#INPUT-DATE-TIME", FieldType.STRING, 14);
        pnd_Input_Date_D = localVariables.newFieldInRecord("pnd_Input_Date_D", "#INPUT-DATE-D", FieldType.DATE);
        pnd_Start_Key = localVariables.newFieldInRecord("pnd_Start_Key", "#START-KEY", FieldType.TIME);
        pnd_End_Key = localVariables.newFieldInRecord("pnd_End_Key", "#END-KEY", FieldType.TIME);
        pnd_Input_Data = localVariables.newFieldInRecord("pnd_Input_Data", "#INPUT-DATA", FieldType.STRING, 60);

        pnd_Input_Data__R_Field_1 = localVariables.newGroupInRecord("pnd_Input_Data__R_Field_1", "REDEFINE", pnd_Input_Data);
        pnd_Input_Data_Pnd_Input_Time_Start = pnd_Input_Data__R_Field_1.newFieldInGroup("pnd_Input_Data_Pnd_Input_Time_Start", "#INPUT-TIME-START", FieldType.STRING, 
            6);
        pnd_Input_Data_Pnd_Input_Time_End = pnd_Input_Data__R_Field_1.newFieldInGroup("pnd_Input_Data_Pnd_Input_Time_End", "#INPUT-TIME-END", FieldType.STRING, 
            6);
        pnd_Input_Data_Pnd_Input_Date = pnd_Input_Data__R_Field_1.newFieldInGroup("pnd_Input_Data_Pnd_Input_Date", "#INPUT-DATE", FieldType.STRING, 8);
        pnd_Input_Source_Id = localVariables.newFieldInRecord("pnd_Input_Source_Id", "#INPUT-SOURCE-ID", FieldType.STRING, 6);

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Support_Tbl__R_Field_2 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_2", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Pnd_Regular_Run_Parms = cwf_Support_Tbl__R_Field_2.newFieldInGroup("cwf_Support_Tbl_Pnd_Regular_Run_Parms", "#REGULAR-RUN-PARMS", 
            FieldType.STRING, 20);
        cwf_Support_Tbl_Pnd_Override_Time_Parms = cwf_Support_Tbl__R_Field_2.newFieldInGroup("cwf_Support_Tbl_Pnd_Override_Time_Parms", "#OVERRIDE-TIME-PARMS", 
            FieldType.STRING, 12);
        cwf_Support_Tbl_Pnd_Override_Flag = cwf_Support_Tbl__R_Field_2.newFieldInGroup("cwf_Support_Tbl_Pnd_Override_Flag", "#OVERRIDE-FLAG", FieldType.STRING, 
            1);
        cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde", "TBL-DLTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_DLTE_OPRTR_CDE");
        registerRecord(vw_cwf_Support_Tbl);

        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_3", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind = pnd_Tbl_Prime_Key__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", FieldType.STRING, 
            1);
        pnd_Nothing_To_Rpt = localVariables.newFieldInRecord("pnd_Nothing_To_Rpt", "#NOTHING-TO-RPT", FieldType.BOOLEAN, 1);
        pnd_Record = localVariables.newFieldInRecord("pnd_Record", "#RECORD", FieldType.STRING, 54);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Support_Tbl.reset();

        ldaEfsl0040.initializeValues();

        localVariables.reset();
        pnd_Nothing_To_Rpt.setInitialValue(true);
        pnd_Record.setInitialValue("�E�&l1o2a5.6c72p3e66F�&a2L�(9905X�&l0L");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Efsp0061() throws Exception
    {
        super("Efsp0061");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Efsp0061|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        while(true)
        {
            try
            {
                getReports().definePrinter(2, "AUDIT");                                                                                                                   //Natural: DEFINE PRINTER ( AUDIT = 1 )
                pnd_Program.setValue(Global.getPROGRAM());                                                                                                                //Natural: FORMAT ( AUDIT ) LS = 133 PS = 60 ZP = OFF;//Natural: ASSIGN #PROGRAM = *PROGRAM
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Source_Id,pnd_Input_Data);                                                                   //Natural: INPUT #INPUT-SOURCE-ID #INPUT-DATA
                pnd_Input_Date_Time.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Input_Data_Pnd_Input_Date, pnd_Input_Data_Pnd_Input_Time_Start));        //Natural: COMPRESS #INPUT-DATE #INPUT-TIME-START INTO #INPUT-DATE-TIME LEAVING NO SPACE
                pnd_Start_Key.setValueEdited(new ReportEditMask("YYYYMMDDHHIISS"),pnd_Input_Date_Time);                                                                   //Natural: MOVE EDITED #INPUT-DATE-TIME TO #START-KEY ( EM = YYYYMMDDHHIISS )
                pnd_Input_Date_Time.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Input_Data_Pnd_Input_Date, pnd_Input_Data_Pnd_Input_Time_End));          //Natural: COMPRESS #INPUT-DATE #INPUT-TIME-END INTO #INPUT-DATE-TIME LEAVING NO SPACE
                pnd_End_Key.setValueEdited(new ReportEditMask("YYYYMMDDHHIISS"),pnd_Input_Date_Time);                                                                     //Natural: MOVE EDITED #INPUT-DATE-TIME TO #END-KEY ( EM = YYYYMMDDHHIISS )
                //*  ADD 1 TO THE DAY COUNT
                pnd_End_Key.nadd(864000);                                                                                                                                 //Natural: ADD 864000 TO #END-KEY
                pnd_Input_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Data_Pnd_Input_Date);                                                            //Natural: MOVE EDITED #INPUT-DATE TO #INPUT-DATE-D ( EM = YYYYMMDD )
                //*  READ THE AUDIT RECORDS FOR A SELECTED DAY
                ldaEfsl0040.getVw_cwf_Upload_Audit().startDatabaseRead                                                                                                    //Natural: READ CWF-UPLOAD-AUDIT BY UPLD-DATE-TIME STARTING FROM #START-KEY ENDING AT #END-KEY
                (
                "READ_AUDIT",
                new Wc[] { new Wc("UPLD_DATE_TIME", ">=", pnd_Start_Key, "And", WcType.BY) ,
                new Wc("UPLD_DATE_TIME", "<=", pnd_End_Key, WcType.BY) },
                new Oc[] { new Oc("UPLD_DATE_TIME", "ASC") }
                );
                READ_AUDIT:
                while (condition(ldaEfsl0040.getVw_cwf_Upload_Audit().readNextRow("READ_AUDIT")))
                {
                    //*   REJECT IF CWF-UPLOAD-AUDIT.EXCEPTION-MSG NE ' ' /* IGNORE ERROR LOGS
                    //*  IGNORE
                    if (condition(ldaEfsl0040.getCwf_Upload_Audit_Mins_Uploaded_Cnt().equals(getZero()) && ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(1).notEquals(" "))) //Natural: REJECT IF CWF-UPLOAD-AUDIT.MINS-UPLOADED-CNT EQ 0 AND CWF-UPLOAD-AUDIT.ERROR-MSG ( 1 ) NE ' '
                    {
                        continue;
                    }
                    //* ---  COS 12.17.97
                    //*   INCLUDE 'FORMS' IF 'CIRS'
                    if (condition(ldaEfsl0040.getCwf_Upload_Audit_Source_Id().notEquals(pnd_Input_Source_Id) && ! (ldaEfsl0040.getCwf_Upload_Audit_Source_Id().equals("FORMS")  //Natural: REJECT IF CWF-UPLOAD-AUDIT.SOURCE-ID NE #INPUT-SOURCE-ID AND NOT ( CWF-UPLOAD-AUDIT.SOURCE-ID EQ 'FORMS' AND #INPUT-SOURCE-ID EQ 'CIRS' )
                        && pnd_Input_Source_Id.equals("CIRS"))))
                    {
                        continue;
                    }
                    getSort().writeSortInData(ldaEfsl0040.getCwf_Upload_Audit_Source_Id(), ldaEfsl0040.getCwf_Upload_Audit_Upld_Date_Time(), ldaEfsl0040.getCwf_Upload_Audit_Batch_Ext_Nbr(),  //Natural: END-ALL
                        ldaEfsl0040.getCwf_Upload_Audit_Batch_Label_Txt(), ldaEfsl0040.getCwf_Upload_Audit_Batch_Nbr(), ldaEfsl0040.getCwf_Upload_Audit_Images_Uploaded_Cnt(), 
                        ldaEfsl0040.getCwf_Upload_Audit_Mins_Bch_Hdr_Cnt(), ldaEfsl0040.getCwf_Upload_Audit_Mins_Uploaded_Cnt(), ldaEfsl0040.getCwf_Upload_Audit_Count_Castunidentified_Min_Nbr());
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                getSort().sortData(ldaEfsl0040.getCwf_Upload_Audit_Source_Id(), ldaEfsl0040.getCwf_Upload_Audit_Upld_Date_Time());                                        //Natural: SORT BY CWF-UPLOAD-AUDIT.SOURCE-ID CWF-UPLOAD-AUDIT.UPLD-DATE-TIME USING CWF-UPLOAD-AUDIT.BATCH-EXT-NBR CWF-UPLOAD-AUDIT.BATCH-LABEL-TXT CWF-UPLOAD-AUDIT.BATCH-NBR CWF-UPLOAD-AUDIT.IMAGES-UPLOADED-CNT CWF-UPLOAD-AUDIT.MINS-BCH-HDR-CNT CWF-UPLOAD-AUDIT.MINS-UPLOADED-CNT CWF-UPLOAD-AUDIT.C*UNIDENTIFIED-MIN-NBR
                boolean endOfDataSort01 = true;
                boolean firstSort01 = true;
                SORT01:
                while (condition(getSort().readSortOutData(ldaEfsl0040.getCwf_Upload_Audit_Source_Id(), ldaEfsl0040.getCwf_Upload_Audit_Upld_Date_Time(), 
                    ldaEfsl0040.getCwf_Upload_Audit_Batch_Ext_Nbr(), ldaEfsl0040.getCwf_Upload_Audit_Batch_Label_Txt(), ldaEfsl0040.getCwf_Upload_Audit_Batch_Nbr(), 
                    ldaEfsl0040.getCwf_Upload_Audit_Images_Uploaded_Cnt(), ldaEfsl0040.getCwf_Upload_Audit_Mins_Bch_Hdr_Cnt(), ldaEfsl0040.getCwf_Upload_Audit_Mins_Uploaded_Cnt(), 
                    ldaEfsl0040.getCwf_Upload_Audit_Count_Castunidentified_Min_Nbr())))
                {
                    if (condition(getSort().getAstCOUNTER().greater(0)))
                    {
                        atBreakEventSort01(false);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom()))
                                break;
                            else if (condition(Global.isEscapeBottomImmediate()))
                            {
                                endOfDataSort01 = false;
                                break;
                            }
                            else if (condition(Global.isEscapeTop()))
                            continue;
                            else if (condition())
                            return;
                        }
                    }
                    //*                                                                                                                                                   //Natural: AT BREAK OF CWF-UPLOAD-AUDIT.SOURCE-ID
                    getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Efsf0067.class));                                                                  //Natural: WRITE ( AUDIT ) NOTITLE USING FORM 'EFSF0067'
                    //*  ADD THE TOTALS
                    pnd_Total_Pnd_Mins_Count.nadd(ldaEfsl0040.getCwf_Upload_Audit_Mins_Uploaded_Cnt());                                                                   //Natural: ADD CWF-UPLOAD-AUDIT.MINS-UPLOADED-CNT TO #TOTAL.#MINS-COUNT
                    pnd_Total_Pnd_Images_Count.nadd(ldaEfsl0040.getCwf_Upload_Audit_Images_Uploaded_Cnt());                                                               //Natural: ADD CWF-UPLOAD-AUDIT.IMAGES-UPLOADED-CNT TO #TOTAL.#IMAGES-COUNT
                    pnd_Total_Pnd_Batch_Count.nadd(1);                                                                                                                    //Natural: ADD 1 TO #TOTAL.#BATCH-COUNT
                    pnd_Total_Pnd_Mins_No_Pin_Count.nadd(ldaEfsl0040.getCwf_Upload_Audit_Count_Castunidentified_Min_Nbr());                                               //Natural: COMPUTE #MINS-NO-PIN-COUNT = #MINS-NO-PIN-COUNT + CWF-UPLOAD-AUDIT.C*UNIDENTIFIED-MIN-NBR
                    pnd_Nothing_To_Rpt.setValue(false);                                                                                                                   //Natural: ASSIGN #NOTHING-TO-RPT := FALSE
                }                                                                                                                                                         //Natural: END-SORT
                if (condition(getSort().getAstCOUNTER().greater(0)))
                {
                    atBreakEventSort01(endOfDataSort01);
                }
                endSort();
                if (condition(pnd_Nothing_To_Rpt.getBoolean()))                                                                                                           //Natural: IF #NOTHING-TO-RPT
                {
                    getReports().write(2, ReportOption.NOTITLE,"Nothing to report.");                                                                                     //Natural: WRITE ( AUDIT ) NOTITLE 'Nothing to report.'
                    if (Global.isEscape()) return;
                    //* ---  COS 12.17.97
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(getReports().getAstLinesLeft(0).less(16)))                                                                                              //Natural: NEWPAGE ( AUDIT ) IF LESS THAN 16 LINES LEFT
                    {
                        getReports().newPage(0);
                        if (condition(Global.isEscape())){return;}
                    }
                    getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Efsf0065.class));                                                                  //Natural: WRITE ( AUDIT ) NOTITLE USING FORM 'EFSF0065'
                    //*  RESET #TOTAL
                }                                                                                                                                                         //Natural: END-IF
                //*  BEING SUBMITTED VIA CTP8000
                //*   SET LANDSCAPE
                getReports().newPage(new ReportSpecification(0));                                                                                                         //Natural: NEWPAGE ( AUDIT )
                if (condition(Global.isEscape())){return;}
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( AUDIT )
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    if (condition(! (DbsUtil.maskMatches(Global.getINIT_PROGRAM(),".....'CWD'"))))                                                                        //Natural: IF *INIT-PROGRAM NE MASK ( .....'CWD' )
                    {
                        getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,pnd_Record);                                                                        //Natural: WRITE ( AUDIT ) NOTITLE NOHDR #RECORD
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(2, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Efsf0066.class));                                              //Natural: WRITE ( AUDIT ) NOTITLE NOHDR USING FORM 'EFSF0066'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean ldaEfsl0040_getCwf_Upload_Audit_Source_IdIsBreak = ldaEfsl0040.getCwf_Upload_Audit_Source_Id().isBreak(endOfData);
        if (condition(ldaEfsl0040_getCwf_Upload_Audit_Source_IdIsBreak))
        {
            //* ---  COS 12.17.97
            getReports().newPage(new ReportSpecification(0));                                                                                                             //Natural: NEWPAGE ( AUDIT )
            if (condition(Global.isEscape())){return;}
            //*  WRITE TOTALS WERE MOVED DOWN BELOW
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(2, "LS=133 PS=60 ZP=OFF");
    }
}
