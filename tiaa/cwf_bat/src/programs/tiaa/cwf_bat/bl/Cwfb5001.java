/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:36:19 PM
**        * FROM NATURAL PROGRAM : Cwfb5001
************************************************************
**        * FILE NAME            : Cwfb5001.java
**        * CLASS NAME           : Cwfb5001
**        * INSTANCE NAME        : Cwfb5001
************************************************************
************************************************************************
* PROGRAM  : CWFB5000
* SYSTEM   : CRPCWF
* TITLE    : OPEN AND BATCH FORMATTING REPORT
* FUNCTION : PRINTS THE LIST OF TRACKING SLIP BATCHES THAT ARE IN
*          :   OPEN AND BATCH-FORMATTING STATUS
*          |
*          |
* 05/25/95 | OB  CHANGED KEY USED IN READING FROM PRINT-BATCHES-KEY TO
*          |    PRINT-QUEUE-KEY TO GENERATE 2 REPORTS INSTEAD OF 1
*          |       - 1ST REPORT WILL CONTAIN ALL OPEN BATCHES
*          |       - 2ND REPORT WILL CONTAIN ALL REQUESTS IN BATCH
*          |        FORMATTING & NOT-OPEN-NOT-CLOSE PRINT-STATUS
*          |        (EX. 05, 20)
* 06/08/95 | OB  ADD PRINT-BATCH-IND AND PRINT-BATCH-SQNCE-NBR COLUMNS
* 11/10/95 | LE  ADD WORK FILE 2
* 06/12/96 | OB  WRITE LIST OF RESCANS IN (1) AT END OF PROGRAM
* 06/18/96 | OB  ADD COLUMNS IN RESCAN REPORT
* 10/04/96 | JHH READ CWF-IMAGE-XREF IF MIT RESCAN-IND = 'Y' AND RESET
*          |     RESCAN-IND IF MIN IS FOUND
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb5001 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Master;
    private DbsField cwf_Master_Pin_Nbr;
    private DbsField cwf_Master_Rqst_Log_Dte_Tme;

    private DbsGroup cwf_Master__R_Field_1;
    private DbsField cwf_Master_Rqst_Log_Index_Dte;
    private DbsField cwf_Master_Rqst_Log_Oprtr_Cde;
    private DbsField cwf_Master_Rqst_Orgn_Cde;
    private DbsField cwf_Master_Rescan_Ind;
    private DbsField cwf_Master_Orgnl_Unit_Cde;
    private DbsField cwf_Master_Work_Prcss_Id;
    private DbsField cwf_Master_Last_Chnge_Dte_Tme;

    private DbsGroup cwf_Master__R_Field_2;
    private DbsField cwf_Master_Last_Chnge_Dte;
    private DbsField cwf_Master_Last_Chnge_Oprtr_Cde;
    private DbsField cwf_Master_Last_Chnge_Unit_Cde;
    private DbsField cwf_Master_Status_Cde;
    private DbsField cwf_Master_Admin_Unit_Cde;
    private DbsField cwf_Master_Crprte_Status_Ind;
    private DbsField cwf_Master_Tiaa_Rcvd_Dte;
    private DbsField cwf_Master_Physcl_Fldr_Id_Nbr;
    private DbsField cwf_Master_Mj_Chrge_Dte_Tme;
    private DbsField cwf_Master_Mj_Chrge_Oprtr_Cde;
    private DbsField cwf_Master_Mstr_Indx_Actn_Cde;
    private DbsField cwf_Master_Mj_Pull_Ind;
    private DbsField cwf_Master_Print_Batch_Id_Nbr;

    private DbsGroup cwf_Master__R_Field_3;
    private DbsField cwf_Master_Print_Prefix_Ind;
    private DbsField cwf_Master_Print_Batch_Nbr;
    private DbsField cwf_Master_Print_Batch_Sqnce_Nbr;
    private DbsField cwf_Master_Print_Batch_Ind;
    private DbsGroup cwf_Master_Mail_Item_NbrMuGroup;
    private DbsField cwf_Master_Mail_Item_Nbr;

    private DataAccessProgramView vw_wpid;
    private DbsField wpid_Work_Prcss_Short_Nme;

    private DataAccessProgramView vw_cwf_Image_Xref;
    private DbsField pnd_Source_Id_Min_Key;

    private DbsGroup pnd_Source_Id_Min_Key__R_Field_4;
    private DbsField pnd_Source_Id_Min_Key_Pnd_Source_Id;
    private DbsField pnd_Source_Id_Min_Key_Pnd_Min_Nbr;
    private DbsField pnd_X;
    private DbsField pnd_Work_Record_2;

    private DbsGroup pnd_Work_Record_2__R_Field_5;
    private DbsField pnd_Work_Record_2_Pnd_Wrk_Batch_Id_Nbr;
    private DbsField pnd_Batch_Ky;

    private DbsGroup pnd_Batch_Ky__R_Field_6;
    private DbsField pnd_Batch_Ky_Pnd_Batch_Id;

    private DbsGroup pnd_Batch_Ky__R_Field_7;
    private DbsField pnd_Batch_Ky_Pnd_Batch_Prefix;
    private DbsField pnd_Batch_Ky_Pnd_Batch_Nbr;
    private DbsField pnd_Ctr;
    private DbsField pnd_Ctr1;
    private DbsField pnd_Ctr2;
    private DbsField pnd_Ptr;

    private DbsGroup pnd_Rescan_Vars;
    private DbsField pnd_Rescan_Vars_Pnd_Pfid;
    private DbsField pnd_Rescan_Vars_Pnd_R_Ptr;
    private DbsField pnd_Rescan_Vars_Pnd_Rescan_Isn;
    private DbsField pnd_Rescan_Vars_Pnd_Total_Rescan;
    private DbsField pnd_Rescan_Vars_Pnd_Write_Rescan;
    private DbsField pnd_Stat;
    private DbsField pnd_Sv_Mstat;

    private DbsRecord setTimeRecord;
    private DbsField st;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Master = new DataAccessProgramView(new NameInfo("vw_cwf_Master", "CWF-MASTER"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        cwf_Master_Pin_Nbr = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "PIN_NBR");
        cwf_Master_Pin_Nbr.setDdmHeader("PIN");
        cwf_Master_Rqst_Log_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15, 
            RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Master_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");

        cwf_Master__R_Field_1 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master__R_Field_1", "REDEFINE", cwf_Master_Rqst_Log_Dte_Tme);
        cwf_Master_Rqst_Log_Index_Dte = cwf_Master__R_Field_1.newFieldInGroup("cwf_Master_Rqst_Log_Index_Dte", "RQST-LOG-INDEX-DTE", FieldType.STRING, 
            8);
        cwf_Master_Rqst_Log_Oprtr_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "RQST_LOG_OPRTR_CDE");
        cwf_Master_Rqst_Log_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Master_Rqst_Orgn_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Rqst_Orgn_Cde", "RQST-ORGN-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RQST_ORGN_CDE");
        cwf_Master_Rescan_Ind = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Rescan_Ind", "RESCAN-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RESCAN_IND");
        cwf_Master_Rescan_Ind.setDdmHeader("RE-SCAN/INDICATOR");
        cwf_Master_Orgnl_Unit_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ORGNL_UNIT_CDE");
        cwf_Master_Orgnl_Unit_Cde.setDdmHeader("LOG/UNIT");
        cwf_Master_Work_Prcss_Id = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        cwf_Master_Work_Prcss_Id.setDdmHeader("WORK/ID");
        cwf_Master_Last_Chnge_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 
            15, RepeatingFieldStrategy.None, "LAST_CHNGE_DTE_TME");
        cwf_Master_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");

        cwf_Master__R_Field_2 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master__R_Field_2", "REDEFINE", cwf_Master_Last_Chnge_Dte_Tme);
        cwf_Master_Last_Chnge_Dte = cwf_Master__R_Field_2.newFieldInGroup("cwf_Master_Last_Chnge_Dte", "LAST-CHNGE-DTE", FieldType.NUMERIC, 8);
        cwf_Master_Last_Chnge_Oprtr_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Last_Chnge_Oprtr_Cde", "LAST-CHNGE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "LAST_CHNGE_OPRTR_CDE");
        cwf_Master_Last_Chnge_Oprtr_Cde.setDdmHeader("LOG CREATE/OPERATOR");
        cwf_Master_Last_Chnge_Unit_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Last_Chnge_Unit_Cde", "LAST-CHNGE-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "LAST_CHNGE_UNIT_CDE");
        cwf_Master_Last_Chnge_Unit_Cde.setDdmHeader("LAST/CHANGE/UNIT");
        cwf_Master_Status_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Status_Cde", "STATUS-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "STATUS_CDE");
        cwf_Master_Admin_Unit_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ADMIN_UNIT_CDE");
        cwf_Master_Crprte_Status_Ind = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Crprte_Status_Ind", "CRPRTE-STATUS-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CRPRTE_STATUS_IND");
        cwf_Master_Crprte_Status_Ind.setDdmHeader("CORP/STAT");
        cwf_Master_Tiaa_Rcvd_Dte = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIAA_RCVD_DTE");
        cwf_Master_Physcl_Fldr_Id_Nbr = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Physcl_Fldr_Id_Nbr", "PHYSCL-FLDR-ID-NBR", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "PHYSCL_FLDR_ID_NBR");
        cwf_Master_Physcl_Fldr_Id_Nbr.setDdmHeader("PHYSICAL FOLDER/ID NUMBER");
        cwf_Master_Mj_Chrge_Dte_Tme = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Mj_Chrge_Dte_Tme", "MJ-CHRGE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "MJ_CHRGE_DTE_TME");
        cwf_Master_Mj_Chrge_Dte_Tme.setDdmHeader("CHARGEOUT/DATE-TIME");
        cwf_Master_Mj_Chrge_Oprtr_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Mj_Chrge_Oprtr_Cde", "MJ-CHRGE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "MJ_CHRGE_OPRTR_CDE");
        cwf_Master_Mj_Chrge_Oprtr_Cde.setDdmHeader("CHARGEOUT/OPERATOR");
        cwf_Master_Mstr_Indx_Actn_Cde = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Mstr_Indx_Actn_Cde", "MSTR-INDX-ACTN-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "MSTR_INDX_ACTN_CDE");
        cwf_Master_Mstr_Indx_Actn_Cde.setDdmHeader("CHARGEOUT/ACTION CODE");
        cwf_Master_Mj_Pull_Ind = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Mj_Pull_Ind", "MJ-PULL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "MJ_PULL_IND");
        cwf_Master_Mj_Pull_Ind.setDdmHeader("MJ PULL/IND");
        cwf_Master_Print_Batch_Id_Nbr = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Print_Batch_Id_Nbr", "PRINT-BATCH-ID-NBR", FieldType.STRING, 
            9, RepeatingFieldStrategy.None, "PRINT_BATCH_ID_NBR");

        cwf_Master__R_Field_3 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master__R_Field_3", "REDEFINE", cwf_Master_Print_Batch_Id_Nbr);
        cwf_Master_Print_Prefix_Ind = cwf_Master__R_Field_3.newFieldInGroup("cwf_Master_Print_Prefix_Ind", "PRINT-PREFIX-IND", FieldType.STRING, 1);
        cwf_Master_Print_Batch_Nbr = cwf_Master__R_Field_3.newFieldInGroup("cwf_Master_Print_Batch_Nbr", "PRINT-BATCH-NBR", FieldType.NUMERIC, 8);
        cwf_Master_Print_Batch_Sqnce_Nbr = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Print_Batch_Sqnce_Nbr", "PRINT-BATCH-SQNCE-NBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "PRINT_BATCH_SQNCE_NBR");
        cwf_Master_Print_Batch_Ind = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Print_Batch_Ind", "PRINT-BATCH-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PRINT_BATCH_IND");
        cwf_Master_Mail_Item_NbrMuGroup = vw_cwf_Master.getRecord().newGroupInGroup("CWF_MASTER_MAIL_ITEM_NBRMuGroup", "MAIL_ITEM_NBRMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "CWF_MASTER_INDEX_MAIL_ITEM_NBR");
        cwf_Master_Mail_Item_Nbr = cwf_Master_Mail_Item_NbrMuGroup.newFieldArrayInGroup("cwf_Master_Mail_Item_Nbr", "MAIL-ITEM-NBR", FieldType.STRING, 
            11, new DbsArrayController(1, 5), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "MAIL_ITEM_NBR");
        cwf_Master_Mail_Item_Nbr.setDdmHeader("MIN");
        registerRecord(vw_cwf_Master);

        vw_wpid = new DataAccessProgramView(new NameInfo("vw_wpid", "WPID"), "CWF_WP_WORK_PRCSS_ID", "CWF_PROFILE");
        wpid_Work_Prcss_Short_Nme = vw_wpid.getRecord().newFieldInGroup("wpid_Work_Prcss_Short_Nme", "WORK-PRCSS-SHORT-NME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_SHORT_NME");
        wpid_Work_Prcss_Short_Nme.setDdmHeader("SHORT NAME");
        registerRecord(vw_wpid);

        vw_cwf_Image_Xref = new DataAccessProgramView(new NameInfo("vw_cwf_Image_Xref", "CWF-IMAGE-XREF"), "CWF_IMAGE_XREF", "CWF_IMAGE_XREF");
        registerRecord(vw_cwf_Image_Xref);

        pnd_Source_Id_Min_Key = localVariables.newFieldInRecord("pnd_Source_Id_Min_Key", "#SOURCE-ID-MIN-KEY", FieldType.STRING, 17);

        pnd_Source_Id_Min_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Source_Id_Min_Key__R_Field_4", "REDEFINE", pnd_Source_Id_Min_Key);
        pnd_Source_Id_Min_Key_Pnd_Source_Id = pnd_Source_Id_Min_Key__R_Field_4.newFieldInGroup("pnd_Source_Id_Min_Key_Pnd_Source_Id", "#SOURCE-ID", FieldType.STRING, 
            6);
        pnd_Source_Id_Min_Key_Pnd_Min_Nbr = pnd_Source_Id_Min_Key__R_Field_4.newFieldInGroup("pnd_Source_Id_Min_Key_Pnd_Min_Nbr", "#MIN-NBR", FieldType.STRING, 
            11);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.PACKED_DECIMAL, 3);
        pnd_Work_Record_2 = localVariables.newFieldInRecord("pnd_Work_Record_2", "#WORK-RECORD-2", FieldType.STRING, 9);

        pnd_Work_Record_2__R_Field_5 = localVariables.newGroupInRecord("pnd_Work_Record_2__R_Field_5", "REDEFINE", pnd_Work_Record_2);
        pnd_Work_Record_2_Pnd_Wrk_Batch_Id_Nbr = pnd_Work_Record_2__R_Field_5.newFieldInGroup("pnd_Work_Record_2_Pnd_Wrk_Batch_Id_Nbr", "#WRK-BATCH-ID-NBR", 
            FieldType.STRING, 9);
        pnd_Batch_Ky = localVariables.newFieldInRecord("pnd_Batch_Ky", "#BATCH-KY", FieldType.STRING, 11);

        pnd_Batch_Ky__R_Field_6 = localVariables.newGroupInRecord("pnd_Batch_Ky__R_Field_6", "REDEFINE", pnd_Batch_Ky);
        pnd_Batch_Ky_Pnd_Batch_Id = pnd_Batch_Ky__R_Field_6.newFieldInGroup("pnd_Batch_Ky_Pnd_Batch_Id", "#BATCH-ID", FieldType.STRING, 9);

        pnd_Batch_Ky__R_Field_7 = pnd_Batch_Ky__R_Field_6.newGroupInGroup("pnd_Batch_Ky__R_Field_7", "REDEFINE", pnd_Batch_Ky_Pnd_Batch_Id);
        pnd_Batch_Ky_Pnd_Batch_Prefix = pnd_Batch_Ky__R_Field_7.newFieldInGroup("pnd_Batch_Ky_Pnd_Batch_Prefix", "#BATCH-PREFIX", FieldType.STRING, 1);
        pnd_Batch_Ky_Pnd_Batch_Nbr = pnd_Batch_Ky__R_Field_7.newFieldInGroup("pnd_Batch_Ky_Pnd_Batch_Nbr", "#BATCH-NBR", FieldType.NUMERIC, 8);
        pnd_Ctr = localVariables.newFieldInRecord("pnd_Ctr", "#CTR", FieldType.NUMERIC, 10);
        pnd_Ctr1 = localVariables.newFieldInRecord("pnd_Ctr1", "#CTR1", FieldType.NUMERIC, 6);
        pnd_Ctr2 = localVariables.newFieldInRecord("pnd_Ctr2", "#CTR2", FieldType.NUMERIC, 6);
        pnd_Ptr = localVariables.newFieldInRecord("pnd_Ptr", "#PTR", FieldType.NUMERIC, 6);

        pnd_Rescan_Vars = localVariables.newGroupInRecord("pnd_Rescan_Vars", "#RESCAN-VARS");
        pnd_Rescan_Vars_Pnd_Pfid = pnd_Rescan_Vars.newFieldInGroup("pnd_Rescan_Vars_Pnd_Pfid", "#PFID", FieldType.STRING, 7);
        pnd_Rescan_Vars_Pnd_R_Ptr = pnd_Rescan_Vars.newFieldInGroup("pnd_Rescan_Vars_Pnd_R_Ptr", "#R-PTR", FieldType.NUMERIC, 8);
        pnd_Rescan_Vars_Pnd_Rescan_Isn = pnd_Rescan_Vars.newFieldArrayInGroup("pnd_Rescan_Vars_Pnd_Rescan_Isn", "#RESCAN-ISN", FieldType.PACKED_DECIMAL, 
            10, new DbsArrayController(1, 300));
        pnd_Rescan_Vars_Pnd_Total_Rescan = pnd_Rescan_Vars.newFieldInGroup("pnd_Rescan_Vars_Pnd_Total_Rescan", "#TOTAL-RESCAN", FieldType.NUMERIC, 8);
        pnd_Rescan_Vars_Pnd_Write_Rescan = pnd_Rescan_Vars.newFieldInGroup("pnd_Rescan_Vars_Pnd_Write_Rescan", "#WRITE-RESCAN", FieldType.BOOLEAN, 1);
        pnd_Stat = localVariables.newFieldInRecord("pnd_Stat", "#STAT", FieldType.STRING, 4);
        pnd_Sv_Mstat = localVariables.newFieldInRecord("pnd_Sv_Mstat", "#SV-MSTAT", FieldType.STRING, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        setTimeRecord = new DbsRecord();
        st = setTimeRecord.newFieldInRecord("ST", "ST", FieldType.NUMERIC, 7);
        registerRecord(setTimeRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Master.reset();
        vw_wpid.reset();
        vw_cwf_Image_Xref.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb5001() throws Exception
    {
        super("Cwfb5001");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*  =======================================================
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 56 LS = 132;//Natural: FORMAT ( 2 ) PS = 56 LS = 132
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        st = Global.getTIMN();                                                                                                                                            //Natural: SET TIME
        vw_cwf_Master.startDatabaseRead                                                                                                                                   //Natural: READ CWF-MASTER BY PRINT-QUEUE-KEY
        (
        "R1",
        new Oc[] { new Oc("PRINT_QUEUE_KEY", "ASC") }
        );
        R1:
        while (condition(vw_cwf_Master.readNextRow("R1")))
        {
            pnd_Ctr.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #CTR
            //*  ------------------------------------- 10/4/96 JHH BEGIN
            if (condition(cwf_Master_Rescan_Ind.equals("Y")))                                                                                                             //Natural: IF CWF-MASTER.RESCAN-IND EQ 'Y'
            {
                if (condition(cwf_Master_Print_Prefix_Ind.equals("D")))                                                                                                   //Natural: IF CWF-MASTER.PRINT-PREFIX-IND = 'D'
                {
                    pnd_Source_Id_Min_Key_Pnd_Source_Id.setValue("DEN");                                                                                                  //Natural: MOVE 'DEN' TO #SOURCE-ID
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  IND='I'?
                    pnd_Source_Id_Min_Key_Pnd_Source_Id.setValue("CIRS");                                                                                                 //Natural: MOVE 'CIRS' TO #SOURCE-ID
                }                                                                                                                                                         //Natural: END-IF
                MIN_LOOP:                                                                                                                                                 //Natural: FOR #X = 1 TO 10
                for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(10)); pnd_X.nadd(1))
                {
                    if (condition(cwf_Master_Mail_Item_Nbr.getValue(pnd_X).equals(" ")))                                                                                  //Natural: IF CWF-MASTER.MAIL-ITEM-NBR ( #X ) = ' '
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Source_Id_Min_Key_Pnd_Min_Nbr.setValue(cwf_Master_Mail_Item_Nbr.getValue(pnd_X));                                                                 //Natural: MOVE CWF-MASTER.MAIL-ITEM-NBR ( #X ) TO #MIN-NBR
                    vw_cwf_Image_Xref.startDatabaseFind                                                                                                                   //Natural: FIND ( 1 ) CWF-IMAGE-XREF WITH SOURCE-ID-MIN-KEY = #SOURCE-ID-MIN-KEY
                    (
                    "FIND01",
                    new Wc[] { new Wc("SOURCE_ID_MIN_KEY", "=", pnd_Source_Id_Min_Key, WcType.WITH) },
                    1
                    );
                    FIND01:
                    while (condition(vw_cwf_Image_Xref.readNextRow("FIND01")))
                    {
                        vw_cwf_Image_Xref.setIfNotFoundControlFlag(false);
                        GET_MIT:                                                                                                                                          //Natural: GET CWF-MASTER *ISN ( R1. )
                        vw_cwf_Master.readByID(vw_cwf_Master.getAstISN("Find01"), "GET_MIT");
                        cwf_Master_Rescan_Ind.reset();                                                                                                                    //Natural: RESET CWF-MASTER.RESCAN-IND
                        vw_cwf_Master.updateDBRow("GET_MIT");                                                                                                             //Natural: UPDATE ( GET-MIT. )
                        getCurrentProcessState().getDbConv().dbCommit();                                                                                                  //Natural: END TRANSACTION
                        if (true) break MIN_LOOP;                                                                                                                         //Natural: ESCAPE BOTTOM ( MIN-LOOP. )
                    }                                                                                                                                                     //Natural: END-FIND
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("MIN_LOOP"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("MIN_LOOP"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  MIN-LOOP.
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  ------------------------------------- 10/4/96 JHH END
            if (condition(cwf_Master_Crprte_Status_Ind.notEquals("0")))                                                                                                   //Natural: REJECT IF CRPRTE-STATUS-IND NE '0'
            {
                continue;
            }
            if (condition(cwf_Master_Rescan_Ind.equals("Y")))                                                                                                             //Natural: IF CWF-MASTER.RESCAN-IND EQ 'Y'
            {
                pnd_Rescan_Vars_Pnd_R_Ptr.nadd(1);                                                                                                                        //Natural: ADD 1 TO #R-PTR
                if (condition(pnd_Rescan_Vars_Pnd_R_Ptr.lessOrEqual(300)))                                                                                                //Natural: IF #R-PTR LE 300
                {
                    pnd_Rescan_Vars_Pnd_Rescan_Isn.getValue(pnd_Rescan_Vars_Pnd_R_Ptr).setValue(vw_cwf_Master.getAstISN("R1"));                                           //Natural: MOVE *ISN TO #RESCAN-ISN ( #R-PTR )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cwf_Master_Mstr_Indx_Actn_Cde.greater("20") || cwf_Master_Mstr_Indx_Actn_Cde.equals(" ")))                                                      //Natural: REJECT IF MSTR-INDX-ACTN-CDE GT '20' OR MSTR-INDX-ACTN-CDE EQ ' '
            {
                continue;
            }
            if (condition(cwf_Master_Mstr_Indx_Actn_Cde.equals("20") && cwf_Master_Pin_Nbr.equals(getZero()) && cwf_Master_Orgnl_Unit_Cde.notEquals("CRC")))              //Natural: REJECT IF MSTR-INDX-ACTN-CDE EQ '20' AND PIN-NBR EQ 0 AND ORGNL-UNIT-CDE NE 'CRC'
            {
                continue;
            }
            //*   OR RQST-ORGN-CDE EQ 'S'
            if (condition(cwf_Master_Print_Batch_Nbr.less(1)))                                                                                                            //Natural: REJECT IF PRINT-BATCH-NBR LT 1
            {
                continue;
            }
            if (condition(cwf_Master_Print_Batch_Id_Nbr.equals(pnd_Batch_Ky_Pnd_Batch_Id) && cwf_Master_Mstr_Indx_Actn_Cde.lessOrEqual("10") && pnd_Sv_Mstat.lessOrEqual("10"))) //Natural: REJECT IF PRINT-BATCH-ID-NBR EQ #BATCH-ID AND MSTR-INDX-ACTN-CDE LE '10' AND #SV-MSTAT LE '10'
            {
                continue;
            }
            short decideConditionsMet215 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF MSTR-INDX-ACTN-CDE;//Natural: VALUE '05'
            if (condition((cwf_Master_Mstr_Indx_Actn_Cde.equals("05"))))
            {
                decideConditionsMet215++;
                pnd_Stat.setValue("STS");                                                                                                                                 //Natural: MOVE 'STS' TO #STAT
            }                                                                                                                                                             //Natural: VALUE '10'
            else if (condition((cwf_Master_Mstr_Indx_Actn_Cde.equals("10"))))
            {
                decideConditionsMet215++;
                pnd_Stat.setValue("OPEN");                                                                                                                                //Natural: MOVE 'OPEN' TO #STAT
            }                                                                                                                                                             //Natural: VALUE '20'
            else if (condition((cwf_Master_Mstr_Indx_Actn_Cde.equals("20"))))
            {
                decideConditionsMet215++;
                pnd_Stat.setValue("FRMT");                                                                                                                                //Natural: MOVE 'FRMT' TO #STAT
                                                                                                                                                                          //Natural: PERFORM WRITE-WRK-FILE2
                sub_Write_Wrk_File2();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Stat.setValue(cwf_Master_Mstr_Indx_Actn_Cde);                                                                                                         //Natural: MOVE MSTR-INDX-ACTN-CDE TO #STAT
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(cwf_Master_Print_Batch_Id_Nbr.notEquals(pnd_Batch_Ky_Pnd_Batch_Id)))                                                                            //Natural: IF PRINT-BATCH-ID-NBR NE #BATCH-ID
            {
                pnd_Batch_Ky_Pnd_Batch_Id.setValue(cwf_Master_Print_Batch_Id_Nbr);                                                                                        //Natural: MOVE PRINT-BATCH-ID-NBR TO #BATCH-ID
                pnd_Sv_Mstat.setValue(cwf_Master_Mstr_Indx_Actn_Cde);                                                                                                     //Natural: MOVE MSTR-INDX-ACTN-CDE TO #SV-MSTAT
                if (condition(cwf_Master_Mstr_Indx_Actn_Cde.lessOrEqual("10")))                                                                                           //Natural: IF MSTR-INDX-ACTN-CDE LE '10'
                {
                    pnd_Ctr1.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #CTR1
                    getReports().write(1, pnd_Ctr1, new ReportEditMask ("ZZZ,ZZ9"),cwf_Master_Print_Batch_Id_Nbr,cwf_Master_Pin_Nbr,cwf_Master_Admin_Unit_Cde,cwf_Master_Work_Prcss_Id,cwf_Master_Rqst_Log_Index_Dte,cwf_Master_Rqst_Log_Oprtr_Cde,cwf_Master_Orgnl_Unit_Cde,cwf_Master_Rqst_Orgn_Cde,new  //Natural: WRITE ( 1 ) #CTR1 ( EM = ZZZ,ZZ9 ) PRINT-BATCH-ID-NBR PIN-NBR ADMIN-UNIT-CDE WORK-PRCSS-ID RQST-LOG-INDEX-DTE RQST-LOG-OPRTR-CDE ORGNL-UNIT-CDE RQST-ORGN-CDE 2X #STAT PRINT-BATCH-SQNCE-NBR PRINT-BATCH-IND
                        ColumnSpacing(2),pnd_Stat,cwf_Master_Print_Batch_Sqnce_Nbr,cwf_Master_Print_Batch_Ind);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cwf_Master_Mstr_Indx_Actn_Cde.greater("10")))                                                                                                   //Natural: IF MSTR-INDX-ACTN-CDE GT '10'
            {
                pnd_Ctr2.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CTR2
                getReports().write(2, pnd_Ctr2, new ReportEditMask ("ZZZ,ZZ9"),cwf_Master_Print_Batch_Id_Nbr,cwf_Master_Print_Batch_Sqnce_Nbr,cwf_Master_Physcl_Fldr_Id_Nbr,  //Natural: WRITE ( 2 ) #CTR2 ( EM = ZZZ,ZZ9 ) PRINT-BATCH-ID-NBR PRINT-BATCH-SQNCE-NBR PHYSCL-FLDR-ID-NBR ( EM = 999999 ) PIN-NBR ADMIN-UNIT-CDE TIAA-RCVD-DTE WORK-PRCSS-ID LAST-CHNGE-DTE LAST-CHNGE-OPRTR-CDE LAST-CHNGE-UNIT-CDE RQST-ORGN-CDE 2X #STAT 2X STATUS-CDE MJ-CHRGE-OPRTR-CDE MJ-CHRGE-DTE-TME ( EM = YYYYMMDD' 'HHIISS )
                    new ReportEditMask ("999999"),cwf_Master_Pin_Nbr,cwf_Master_Admin_Unit_Cde,cwf_Master_Tiaa_Rcvd_Dte,cwf_Master_Work_Prcss_Id,cwf_Master_Last_Chnge_Dte,cwf_Master_Last_Chnge_Oprtr_Cde,cwf_Master_Last_Chnge_Unit_Cde,cwf_Master_Rqst_Orgn_Cde,new 
                    ColumnSpacing(2),pnd_Stat,new ColumnSpacing(2),cwf_Master_Status_Cde,cwf_Master_Mj_Chrge_Oprtr_Cde,cwf_Master_Mj_Chrge_Dte_Tme, new 
                    ReportEditMask ("YYYYMMDD' 'HHIISS"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*   WRITE LIST OF RESCANS
        pnd_Rescan_Vars_Pnd_Total_Rescan.setValue(pnd_Rescan_Vars_Pnd_R_Ptr);                                                                                             //Natural: MOVE #R-PTR TO #TOTAL-RESCAN
        pnd_Rescan_Vars_Pnd_Write_Rescan.setValue(true);                                                                                                                  //Natural: MOVE TRUE TO #WRITE-RESCAN
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        FOR01:                                                                                                                                                            //Natural: FOR #R-PTR 1 300
        for (pnd_Rescan_Vars_Pnd_R_Ptr.setValue(1); condition(pnd_Rescan_Vars_Pnd_R_Ptr.lessOrEqual(300)); pnd_Rescan_Vars_Pnd_R_Ptr.nadd(1))
        {
            if (condition(pnd_Rescan_Vars_Pnd_Rescan_Isn.getValue(pnd_Rescan_Vars_Pnd_R_Ptr).equals(getZero())))                                                          //Natural: IF #RESCAN-ISN ( #R-PTR ) EQ 0
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            vw_cwf_Master.startDatabaseRead                                                                                                                               //Natural: READ ( 1 ) CWF-MASTER BY ISN FROM #RESCAN-ISN ( #R-PTR )
            (
            "READ01",
            new Wc[] { new Wc("ISN", ">=", pnd_Rescan_Vars_Pnd_Rescan_Isn.getValue(pnd_Rescan_Vars_Pnd_R_Ptr), WcType.BY) },
            new Oc[] { new Oc("ISN", "ASC") },
            1
            );
            READ01:
            while (condition(vw_cwf_Master.readNextRow("READ01")))
            {
                if (condition(cwf_Master_Rescan_Ind.equals("Y")))                                                                                                         //Natural: IF CWF-MASTER.RESCAN-IND EQ 'Y'
                {
                    short decideConditionsMet250 = 0;                                                                                                                     //Natural: DECIDE ON FIRST VALUE OF MSTR-INDX-ACTN-CDE;//Natural: VALUE '05'
                    if (condition((cwf_Master_Mstr_Indx_Actn_Cde.equals("05"))))
                    {
                        decideConditionsMet250++;
                        pnd_Stat.setValue("S");                                                                                                                           //Natural: MOVE 'S' TO #STAT
                    }                                                                                                                                                     //Natural: VALUE '10'
                    else if (condition((cwf_Master_Mstr_Indx_Actn_Cde.equals("10"))))
                    {
                        decideConditionsMet250++;
                        pnd_Stat.setValue("O");                                                                                                                           //Natural: MOVE 'O' TO #STAT
                    }                                                                                                                                                     //Natural: VALUE '20'
                    else if (condition((cwf_Master_Mstr_Indx_Actn_Cde.equals("20"))))
                    {
                        decideConditionsMet250++;
                        pnd_Stat.setValue("F");                                                                                                                           //Natural: MOVE 'F' TO #STAT
                    }                                                                                                                                                     //Natural: VALUE '30'
                    else if (condition((cwf_Master_Mstr_Indx_Actn_Cde.equals("30"))))
                    {
                        decideConditionsMet250++;
                        pnd_Stat.setValue("C");                                                                                                                           //Natural: MOVE 'C' TO #STAT
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        pnd_Stat.setValue(cwf_Master_Mstr_Indx_Actn_Cde);                                                                                                 //Natural: MOVE MSTR-INDX-ACTN-CDE TO #STAT
                    }                                                                                                                                                     //Natural: END-DECIDE
                    pnd_Rescan_Vars_Pnd_Pfid.setValueEdited(cwf_Master_Physcl_Fldr_Id_Nbr,new ReportEditMask("999999"));                                                  //Natural: MOVE EDITED PHYSCL-FLDR-ID-NBR ( EM = 999999 ) TO #PFID
                    if (condition(cwf_Master_Mj_Pull_Ind.equals("Y") || cwf_Master_Mj_Pull_Ind.equals("J") || cwf_Master_Mj_Pull_Ind.equals("R")))                        //Natural: IF CWF-MASTER.MJ-PULL-IND EQ 'Y' OR EQ 'J' OR EQ 'R'
                    {
                        pnd_Rescan_Vars_Pnd_Pfid.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "M", pnd_Rescan_Vars_Pnd_Pfid));                                //Natural: COMPRESS 'M' #PFID INTO #PFID LEAVING NO
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Rescan_Vars_Pnd_Pfid.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "F", pnd_Rescan_Vars_Pnd_Pfid));                                //Natural: COMPRESS 'F' #PFID INTO #PFID LEAVING NO
                    }                                                                                                                                                     //Natural: END-IF
                    vw_wpid.reset();                                                                                                                                      //Natural: RESET WPID
                    vw_wpid.startDatabaseFind                                                                                                                             //Natural: FIND ( 1 ) WPID WITH WPID-UNIQ-KEY EQ CWF-MASTER.WORK-PRCSS-ID
                    (
                    "FIND02",
                    new Wc[] { new Wc("WPID_UNIQ_KEY", "=", cwf_Master_Work_Prcss_Id, WcType.WITH) },
                    1
                    );
                    FIND02:
                    while (condition(vw_wpid.readNextRow("FIND02")))
                    {
                        vw_wpid.setIfNotFoundControlFlag(false);
                    }                                                                                                                                                     //Natural: END-FIND
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    short decideConditionsMet271 = 0;                                                                                                                     //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN MAIL-ITEM-NBR ( 5 ) GT ' '
                    if (condition(cwf_Master_Mail_Item_Nbr.getValue(5).greater(" ")))
                    {
                        decideConditionsMet271++;
                        pnd_Ptr.setValue(5);                                                                                                                              //Natural: MOVE 5 TO #PTR
                    }                                                                                                                                                     //Natural: WHEN MAIL-ITEM-NBR ( 4 ) GT ' '
                    else if (condition(cwf_Master_Mail_Item_Nbr.getValue(4).greater(" ")))
                    {
                        decideConditionsMet271++;
                        pnd_Ptr.setValue(4);                                                                                                                              //Natural: MOVE 4 TO #PTR
                    }                                                                                                                                                     //Natural: WHEN MAIL-ITEM-NBR ( 3 ) GT ' '
                    else if (condition(cwf_Master_Mail_Item_Nbr.getValue(3).greater(" ")))
                    {
                        decideConditionsMet271++;
                        pnd_Ptr.setValue(3);                                                                                                                              //Natural: MOVE 3 TO #PTR
                    }                                                                                                                                                     //Natural: WHEN MAIL-ITEM-NBR ( 2 ) GT ' '
                    else if (condition(cwf_Master_Mail_Item_Nbr.getValue(2).greater(" ")))
                    {
                        decideConditionsMet271++;
                        pnd_Ptr.setValue(2);                                                                                                                              //Natural: MOVE 2 TO #PTR
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        pnd_Ptr.setValue(1);                                                                                                                              //Natural: MOVE 1 TO #PTR
                    }                                                                                                                                                     //Natural: END-DECIDE
                    getReports().write(1, pnd_Rescan_Vars_Pnd_R_Ptr, new ReportEditMask ("ZZZ,ZZ9"),cwf_Master_Mail_Item_Nbr.getValue(pnd_Ptr), new ReportEditMask        //Natural: WRITE ( 1 ) #R-PTR ( EM = ZZZ,ZZ9 ) MAIL-ITEM-NBR ( #PTR ) ( EM = '"IXXXXXXXX-XXX' ) 2X #PFID 2X PIN-NBR 3X ADMIN-UNIT-CDE 2X WORK-PRCSS-ID 2X WPID.WORK-PRCSS-SHORT-NME 2X RQST-LOG-INDEX-DTE 2X RQST-LOG-OPRTR-CDE 2X ORGNL-UNIT-CDE 4X RQST-ORGN-CDE 3X #STAT 2X PRINT-BATCH-IND
                        ("''IXXXXXXXX-XXX'"),new ColumnSpacing(2),pnd_Rescan_Vars_Pnd_Pfid,new ColumnSpacing(2),cwf_Master_Pin_Nbr,new ColumnSpacing(3),cwf_Master_Admin_Unit_Cde,new 
                        ColumnSpacing(2),cwf_Master_Work_Prcss_Id,new ColumnSpacing(2),wpid_Work_Prcss_Short_Nme,new ColumnSpacing(2),cwf_Master_Rqst_Log_Index_Dte,new 
                        ColumnSpacing(2),cwf_Master_Rqst_Log_Oprtr_Cde,new ColumnSpacing(2),cwf_Master_Orgnl_Unit_Cde,new ColumnSpacing(4),cwf_Master_Rqst_Orgn_Cde,new 
                        ColumnSpacing(3),pnd_Stat,new ColumnSpacing(2),cwf_Master_Print_Batch_Ind);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*        PRINT-BATCH-ID-NBR
                    //*        PRINT-BATCH-SQNCE-NBR (EM=999)
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(1, NEWLINE,NEWLINE,"Total Number of RESCANs:",pnd_Rescan_Vars_Pnd_Total_Rescan);                                                               //Natural: WRITE ( 1 ) // 'Total Number of RESCANs:' #TOTAL-RESCAN
        if (Global.isEscape()) return;
        //* ************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WRK-FILE2
        //* ************
        getReports().write(1, NEWLINE,NEWLINE,"Records read:",pnd_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"Time elapsed:",st, new ReportEditMask                    //Natural: WRITE ( 1 ) // 'Records read:' #CTR ( EM = Z,ZZZ,ZZ9 ) / 'Time elapsed:' *TIMD ( ST. ) ( EM = 99':'99':'99'.'9 )
            ("99':'99':'99'.'9"));
        if (Global.isEscape()) return;
        getReports().write(2, NEWLINE,NEWLINE,"Records read:",pnd_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"Time elapsed:",st, new ReportEditMask                    //Natural: WRITE ( 2 ) // 'Records read:' #CTR ( EM = Z,ZZZ,ZZ9 ) / 'Time elapsed:' *TIMD ( ST. ) ( EM = 99':'99':'99'.'9 )
            ("99':'99':'99'.'9"));
        if (Global.isEscape()) return;
    }
    private void sub_Write_Wrk_File2() throws Exception                                                                                                                   //Natural: WRITE-WRK-FILE2
    {
        if (BLNatReinput.isReinput()) return;

        //* ************
        pnd_Work_Record_2_Pnd_Wrk_Batch_Id_Nbr.setValue(cwf_Master_Print_Batch_Id_Nbr);                                                                                   //Natural: MOVE PRINT-BATCH-ID-NBR TO #WRK-BATCH-ID-NBR
        getWorkFiles().write(2, false, pnd_Work_Record_2);                                                                                                                //Natural: WRITE WORK FILE 2 #WORK-RECORD-2
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    if (condition(! (pnd_Rescan_Vars_Pnd_Write_Rescan.getBoolean())))                                                                                     //Natural: IF NOT #WRITE-RESCAN
                    {
                        getReports().write(1, new TabSetting(15),"List of Tracking Slip Batches in OPEN status","(",Global.getPROGRAM(),")",NEWLINE,NEWLINE,new           //Natural: WRITE ( 1 ) 15T 'List of Tracking Slip Batches in OPEN status' '(' *PROGRAM ')' // 9T 'BATCH #' 19T ' PIN  ' 33T 'UNIT CD' 42T 'WPID ' 49T 'LOG-DATE' 58T 'LOG-USER' 67T 'LOG-UNIT' 76T 'MOC' 80T 'STAT' 85T 'SEQ' 89T 'IND' / 11T '(STATs: OPEN = open batch), STS = suppress tracking slip)' /
                            TabSetting(9),"BATCH #",new TabSetting(19)," PIN  ",new TabSetting(33),"UNIT CD",new TabSetting(42),"WPID ",new TabSetting(49),"LOG-DATE",new 
                            TabSetting(58),"LOG-USER",new TabSetting(67),"LOG-UNIT",new TabSetting(76),"MOC",new TabSetting(80),"STAT",new TabSetting(85),"SEQ",new 
                            TabSetting(89),"IND",NEWLINE,new TabSetting(11),"(STATs: OPEN = open batch), STS = suppress tracking slip)",NEWLINE);
                        //*      28T 'UNIT CD'
                        //*      37T 'WPID '
                        //*      44T 'LOG-DATE'
                        //*      53T 'LOG-USER'
                        //*      62T 'LOG-UNIT'
                        //*      71T 'MOC'
                        //*      75T 'STAT'
                        //*      80T 'SEQ'
                        //*      84T 'IND'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(1, new TabSetting(15),"Work Requests Marked for RESCAN","(",Global.getPROGRAM(),")",NEWLINE,NEWLINE,new TabSetting(9),"BATCH #  MIN",new  //Natural: WRITE ( 1 ) 15T 'Work Requests Marked for RESCAN' '(' *PROGRAM ')' // 9T 'BATCH #  MIN' 24T 'P_Folder' 34T '__PIN__' 49T 'UNIT_CD' 59T '_WPID_' 67T 'WPID_SHORT_NAME' 84T 'LOG_DATE' 94T 'LOG_USER' 104T 'LOG_UNIT' 115T 'MOC' 120T 'STAT' 126T 'IND' /
                            TabSetting(24),"P_Folder",new TabSetting(34),"__PIN__",new TabSetting(49),"UNIT_CD",new TabSetting(59),"_WPID_",new TabSetting(67),"WPID_SHORT_NAME",new 
                            TabSetting(84),"LOG_DATE",new TabSetting(94),"LOG_USER",new TabSetting(104),"LOG_UNIT",new TabSetting(115),"MOC",new TabSetting(120),"STAT",new 
                            TabSetting(126),"IND",NEWLINE);
                        //*      19T 'Seq'
                        //*      44T 'UNIT_CD'
                        //*      54T '_WPID_'
                        //*      62T 'WPID_SHORT_NAME'
                        //*      79T 'LOG_DATE'
                        //*      89T 'LOG_USER'
                        //*      99T 'LOG_UNIT'
                        //*      110T 'MOC'
                        //*      115T 'STAT'
                        //*      121T 'IND'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, new TabSetting(15),"List of Tracking Slips in Not-Open-Not-Close status","(",Global.getPROGRAM(),")",NEWLINE,NEWLINE,new        //Natural: WRITE ( 2 ) 15T 'List of Tracking Slips in Not-Open-Not-Close status' '(' *PROGRAM ')' // 9T 'BATCH #' 19T 'Seq' 24T 'Fldr ID' 33T ' PIN  ' 45T 'UNIT CD' 54T 'TIAA DTE' 63T ' WPID ' 71T 'LCHGDATE' 80T 'LCHGUSER' 89T 'LCHGUNIT' 98T 'MOC' 102T 'PSTT' 107T 'STAT' 112T 'CHRGEBY' 121T 'MJ-CHRGE-DTETME' / 25T '(STATs: FRMT = batch formatting)' /
                        TabSetting(9),"BATCH #",new TabSetting(19),"Seq",new TabSetting(24),"Fldr ID",new TabSetting(33)," PIN  ",new TabSetting(45),"UNIT CD",new 
                        TabSetting(54),"TIAA DTE",new TabSetting(63)," WPID ",new TabSetting(71),"LCHGDATE",new TabSetting(80),"LCHGUSER",new TabSetting(89),"LCHGUNIT",new 
                        TabSetting(98),"MOC",new TabSetting(102),"PSTT",new TabSetting(107),"STAT",new TabSetting(112),"CHRGEBY",new TabSetting(121),"MJ-CHRGE-DTETME",NEWLINE,new 
                        TabSetting(25),"(STATs: FRMT = batch formatting)",NEWLINE);
                    //*    40T 'UNIT CD'
                    //*    49T 'TIAA DTE'
                    //*    58T ' WPID '
                    //*    66T 'LCHGDATE'
                    //*    75T 'LCHGUSER'
                    //*    84T 'LCHGUNIT'
                    //*    93T 'MOC'
                    //*    97T 'PSTT'
                    //*    102T 'STAT'
                    //*    107T 'CHRGEBY'
                    //*    116T 'MJ-CHRGE-DTETME'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=56 LS=132");
        Global.format(2, "PS=56 LS=132");
    }
}
