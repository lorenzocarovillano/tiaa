/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:44:47 PM
**        * FROM NATURAL PROGRAM : Cwfb7976
************************************************************
**        * FILE NAME            : Cwfb7976.java
**        * CLASS NAME           : Cwfb7976
**        * INSTANCE NAME        : Cwfb7976
************************************************************
* =============================================================
* REPORT FROZEN AND DIGITIZED MICROJACKETS DAILY
* --------------------------------------------------------
*   READ CWF-SUPPORT-TBL TO ASCERTAIN WHICH DAY's records to select
* FROM FILE 080 (CWF-EFM-CABINET) USING DESCRIPTOR MEDIA-IND-UPDTE-KEY.
* MEDIA-IND-UPDTE-KEY.
* - MEDIA-IND = 2 = FROZEN MICROJACKET
* - MEDIA-IND = 3 = DIGITIZED MICROJACKET
*
*   THE SELECTED RECORDS ARE SORTED BY PIN-NBR AND A SERIES OF
* MICROJACKET REPORTS ARE PRODUCED IN CWFB7977:
*
*   CAB.                                CAB.     MIT    MIT
*  MEDIA  RPT                          SOURCE    ORIG   MJ
*   IND   NBR   REPORT                  CODE     UNIT   PULL
*   ---   ---   ---------------------   ----     ----   ----
*    2    01    CIRS LOGGED NON-MICRO   L,U      CRC
*    2    02    CIRS LOGGED MICRO       L,U      CRC    Y,R,J
*    2    03    USER LOGGED NON-MICRO   L,U
*    2    04    USER LOGGED MICRO       L,U             Y,R,J
*    2    05    BACK-END IMAGING        B
*    2    06    PARTIC. FILE UPDATE     P
*    2    07    MERGED RQST (NO MIT)    L,U,B,P
*
*    3    01    CIRS LOGGED             L        CRC
*    3    03    USER LOGGED             L
*    3    05    MICROJACKET REQUESTED   T
*    3    06    DIGITIZE SELECT         S
*    3    07    MERGED RQST (NO MIT)    L,T,S
*
* HISTORY
* ------------
* 11/13/96 JHH - FORCE '19960731' INTO LAST-DTE FOR MEDIA 3
* 11/14/96 JHH - SUPPRESS CHANGE OF 11/13/96
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
* =============================================================

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb7976 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Tbl;
    private DbsField cwf_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Tbl__R_Field_1;
    private DbsField cwf_Tbl_Next;
    private DbsField cwf_Tbl_Last_Dte;
    private DbsField cwf_Tbl_Prev;
    private DbsField cwf_Tbl_Prev_Dte;
    private DbsField cwf_Tbl_Dash;
    private DbsField cwf_Tbl_Run_Dte;
    private DbsField cwf_Tbl_Tbl_Updte_Dte_Tme;
    private DbsField cwf_Tbl_Tbl_Updte_Dte;
    private DbsField cwf_Tbl_Tbl_Updte_Oprtr_Cde;
    private DbsField pnd_Tbl_Prime_Key;

    private DataAccessProgramView vw_cwf_Cab;
    private DbsField cwf_Cab_Cabinet_Id;

    private DbsGroup cwf_Cab__R_Field_2;
    private DbsField cwf_Cab_Pin_Pfx;
    private DbsField cwf_Cab_Pin_Nbr;
    private DbsField cwf_Cab_Media_Ind;
    private DbsField cwf_Cab_Media_Updte_Dte_Tme;
    private DbsField cwf_Cab_Mit_Rqst_Log_Dte_Tme;
    private DbsField cwf_Cab_Updte_Srce_Cde;
    private DbsField cwf_Cab_Dgtze_Srce_Cd;
    private DbsField cwf_Cab_Dgtze_Rqst_Log_Dte_Tme;
    private DbsField pnd_Cab_Key;

    private DbsGroup pnd_Cab_Key__R_Field_3;
    private DbsField pnd_Cab_Key_Pnd_Media_Ind;
    private DbsField pnd_Cab_Key_Pnd_Media_Updte_Dte_Tme;

    private DataAccessProgramView vw_cwf_Mit;
    private DbsField cwf_Mit_Orgnl_Unit_Cde;
    private DbsField cwf_Mit_Work_Prcss_Id;
    private DbsField cwf_Mit_Tiaa_Rcvd_Dte;
    private DbsField cwf_Mit_Mj_Pull_Ind;
    private DbsField pnd_Work;

    private DbsGroup pnd_Work__R_Field_4;
    private DbsField pnd_Work_Media;
    private DbsField pnd_Work_Sort_Date;
    private DbsField pnd_Work_Pin;
    private DbsField pnd_Work_Rpt;
    private DbsField pnd_Work_Date;
    private DbsField pnd_Work_Wpid;
    private DbsField pnd_Work_Dte_Tme;

    private DbsGroup pnd_Table;
    private DbsField pnd_Table_Pnd_T201;
    private DbsField pnd_Table_Pnd_T202;
    private DbsField pnd_Table_Pnd_T203;
    private DbsField pnd_Table_Pnd_T204;
    private DbsField pnd_Table_Pnd_T205;
    private DbsField pnd_Table_Pnd_T206;
    private DbsField pnd_Table_Pnd_T207;
    private DbsField pnd_Table_Pnd_T301;
    private DbsField pnd_Table_Pnd_T303;
    private DbsField pnd_Table_Pnd_T305;
    private DbsField pnd_Table_Pnd_T306;
    private DbsField pnd_Table_Pnd_T307;

    private DbsGroup pnd_Table__R_Field_5;

    private DbsGroup pnd_Table_Pnd_Tall;
    private DbsField pnd_Table_Pnd_Tbl_Media;
    private DbsField pnd_Table_Pnd_Tbl_Rpt;
    private DbsField pnd_Table_Fill1;
    private DbsField pnd_Table_Pnd_Tbl_Cnt;
    private DbsField pnd_Table_Fill2;
    private DbsField pnd_Table_Pnd_Tbl_Nme;
    private DbsField pnd_In_Cnt;
    private DbsField pnd_Sel_Cnt;
    private DbsField pnd_Dte_From;
    private DbsField pnd_Dte_To;
    private DbsField pnd_Dte_To_T;
    private DbsField pnd_X;
    private DbsField pnd_Tbl_Isn;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Tbl", "CWF-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Tbl_Tbl_Data_Field = vw_cwf_Tbl.getRecord().newFieldInGroup("cwf_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 253, RepeatingFieldStrategy.None, 
            "TBL_DATA_FIELD");

        cwf_Tbl__R_Field_1 = vw_cwf_Tbl.getRecord().newGroupInGroup("cwf_Tbl__R_Field_1", "REDEFINE", cwf_Tbl_Tbl_Data_Field);
        cwf_Tbl_Next = cwf_Tbl__R_Field_1.newFieldInGroup("cwf_Tbl_Next", "NEXT", FieldType.STRING, 5);
        cwf_Tbl_Last_Dte = cwf_Tbl__R_Field_1.newFieldInGroup("cwf_Tbl_Last_Dte", "LAST-DTE", FieldType.STRING, 8);
        cwf_Tbl_Prev = cwf_Tbl__R_Field_1.newFieldInGroup("cwf_Tbl_Prev", "PREV", FieldType.STRING, 7);
        cwf_Tbl_Prev_Dte = cwf_Tbl__R_Field_1.newFieldInGroup("cwf_Tbl_Prev_Dte", "PREV-DTE", FieldType.STRING, 8);
        cwf_Tbl_Dash = cwf_Tbl__R_Field_1.newFieldInGroup("cwf_Tbl_Dash", "DASH", FieldType.STRING, 3);
        cwf_Tbl_Run_Dte = cwf_Tbl__R_Field_1.newFieldInGroup("cwf_Tbl_Run_Dte", "RUN-DTE", FieldType.STRING, 8);
        cwf_Tbl_Tbl_Updte_Dte_Tme = vw_cwf_Tbl.getRecord().newFieldInGroup("cwf_Tbl_Tbl_Updte_Dte_Tme", "TBL-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TBL_UPDTE_DTE_TME");
        cwf_Tbl_Tbl_Updte_Dte_Tme.setDdmHeader("UPDATE DATE/TIME");
        cwf_Tbl_Tbl_Updte_Dte = vw_cwf_Tbl.getRecord().newFieldInGroup("cwf_Tbl_Tbl_Updte_Dte", "TBL-UPDTE-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TBL_UPDTE_DTE");
        cwf_Tbl_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        cwf_Tbl_Tbl_Updte_Oprtr_Cde = vw_cwf_Tbl.getRecord().newFieldInGroup("cwf_Tbl_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Tbl_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        registerRecord(vw_cwf_Tbl);

        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        vw_cwf_Cab = new DataAccessProgramView(new NameInfo("vw_cwf_Cab", "CWF-CAB"), "CWF_EFM_CABINET", "CWF_EFM_CABINET");
        cwf_Cab_Cabinet_Id = vw_cwf_Cab.getRecord().newFieldInGroup("cwf_Cab_Cabinet_Id", "CABINET-ID", FieldType.STRING, 14, RepeatingFieldStrategy.None, 
            "CABINET_ID");
        cwf_Cab_Cabinet_Id.setDdmHeader("CABINET/ID");

        cwf_Cab__R_Field_2 = vw_cwf_Cab.getRecord().newGroupInGroup("cwf_Cab__R_Field_2", "REDEFINE", cwf_Cab_Cabinet_Id);
        cwf_Cab_Pin_Pfx = cwf_Cab__R_Field_2.newFieldInGroup("cwf_Cab_Pin_Pfx", "PIN-PFX", FieldType.STRING, 1);
        cwf_Cab_Pin_Nbr = cwf_Cab__R_Field_2.newFieldInGroup("cwf_Cab_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12);
        cwf_Cab_Media_Ind = vw_cwf_Cab.getRecord().newFieldInGroup("cwf_Cab_Media_Ind", "MEDIA-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "MEDIA_IND");
        cwf_Cab_Media_Ind.setDdmHeader("MEDIA IND");
        cwf_Cab_Media_Updte_Dte_Tme = vw_cwf_Cab.getRecord().newFieldInGroup("cwf_Cab_Media_Updte_Dte_Tme", "MEDIA-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "MEDIA_UPDTE_DTE_TME");
        cwf_Cab_Media_Updte_Dte_Tme.setDdmHeader("MEDIA UPDATE");
        cwf_Cab_Mit_Rqst_Log_Dte_Tme = vw_cwf_Cab.getRecord().newFieldInGroup("cwf_Cab_Mit_Rqst_Log_Dte_Tme", "MIT-RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "MIT_RQST_LOG_DTE_TME");
        cwf_Cab_Mit_Rqst_Log_Dte_Tme.setDdmHeader("MIT-RLDT");
        cwf_Cab_Updte_Srce_Cde = vw_cwf_Cab.getRecord().newFieldInGroup("cwf_Cab_Updte_Srce_Cde", "UPDTE-SRCE-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "UPDTE_SRCE_CDE");
        cwf_Cab_Updte_Srce_Cde.setDdmHeader("UPDTE SRCE");
        cwf_Cab_Dgtze_Srce_Cd = vw_cwf_Cab.getRecord().newFieldInGroup("cwf_Cab_Dgtze_Srce_Cd", "DGTZE-SRCE-CD", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "DGTZE_SRCE_CD");
        cwf_Cab_Dgtze_Srce_Cd.setDdmHeader("DGTZE SOURCE/CODE");
        cwf_Cab_Dgtze_Rqst_Log_Dte_Tme = vw_cwf_Cab.getRecord().newFieldInGroup("cwf_Cab_Dgtze_Rqst_Log_Dte_Tme", "DGTZE-RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "DGTZE_RQST_LOG_DTE_TME");
        cwf_Cab_Dgtze_Rqst_Log_Dte_Tme.setDdmHeader("DGTZE RQST LOG/DTE TME");
        registerRecord(vw_cwf_Cab);

        pnd_Cab_Key = localVariables.newFieldInRecord("pnd_Cab_Key", "#CAB-KEY", FieldType.STRING, 8);

        pnd_Cab_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Cab_Key__R_Field_3", "REDEFINE", pnd_Cab_Key);
        pnd_Cab_Key_Pnd_Media_Ind = pnd_Cab_Key__R_Field_3.newFieldInGroup("pnd_Cab_Key_Pnd_Media_Ind", "#MEDIA-IND", FieldType.STRING, 1);
        pnd_Cab_Key_Pnd_Media_Updte_Dte_Tme = pnd_Cab_Key__R_Field_3.newFieldInGroup("pnd_Cab_Key_Pnd_Media_Updte_Dte_Tme", "#MEDIA-UPDTE-DTE-TME", FieldType.TIME);

        vw_cwf_Mit = new DataAccessProgramView(new NameInfo("vw_cwf_Mit", "CWF-MIT"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        cwf_Mit_Orgnl_Unit_Cde = vw_cwf_Mit.getRecord().newFieldInGroup("cwf_Mit_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ORGNL_UNIT_CDE");
        cwf_Mit_Orgnl_Unit_Cde.setDdmHeader("LOG/UNIT");
        cwf_Mit_Work_Prcss_Id = vw_cwf_Mit.getRecord().newFieldInGroup("cwf_Mit_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        cwf_Mit_Work_Prcss_Id.setDdmHeader("WORK/ID");
        cwf_Mit_Tiaa_Rcvd_Dte = vw_cwf_Mit.getRecord().newFieldInGroup("cwf_Mit_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIAA_RCVD_DTE");
        cwf_Mit_Mj_Pull_Ind = vw_cwf_Mit.getRecord().newFieldInGroup("cwf_Mit_Mj_Pull_Ind", "MJ-PULL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "MJ_PULL_IND");
        cwf_Mit_Mj_Pull_Ind.setDdmHeader("MJ PULL/IND");
        registerRecord(vw_cwf_Mit);

        pnd_Work = localVariables.newFieldInRecord("pnd_Work", "#WORK", FieldType.STRING, 40);

        pnd_Work__R_Field_4 = localVariables.newGroupInRecord("pnd_Work__R_Field_4", "REDEFINE", pnd_Work);
        pnd_Work_Media = pnd_Work__R_Field_4.newFieldInGroup("pnd_Work_Media", "MEDIA", FieldType.STRING, 1);
        pnd_Work_Sort_Date = pnd_Work__R_Field_4.newFieldInGroup("pnd_Work_Sort_Date", "SORT-DATE", FieldType.DATE);
        pnd_Work_Pin = pnd_Work__R_Field_4.newFieldInGroup("pnd_Work_Pin", "PIN", FieldType.NUMERIC, 12);
        pnd_Work_Rpt = pnd_Work__R_Field_4.newFieldInGroup("pnd_Work_Rpt", "RPT", FieldType.NUMERIC, 2);
        pnd_Work_Date = pnd_Work__R_Field_4.newFieldInGroup("pnd_Work_Date", "DATE", FieldType.DATE);
        pnd_Work_Wpid = pnd_Work__R_Field_4.newFieldInGroup("pnd_Work_Wpid", "WPID", FieldType.STRING, 6);
        pnd_Work_Dte_Tme = pnd_Work__R_Field_4.newFieldInGroup("pnd_Work_Dte_Tme", "DTE-TME", FieldType.TIME);

        pnd_Table = localVariables.newGroupInRecord("pnd_Table", "#TABLE");
        pnd_Table_Pnd_T201 = pnd_Table.newFieldInGroup("pnd_Table_Pnd_T201", "#T201", FieldType.STRING, 44);
        pnd_Table_Pnd_T202 = pnd_Table.newFieldInGroup("pnd_Table_Pnd_T202", "#T202", FieldType.STRING, 44);
        pnd_Table_Pnd_T203 = pnd_Table.newFieldInGroup("pnd_Table_Pnd_T203", "#T203", FieldType.STRING, 44);
        pnd_Table_Pnd_T204 = pnd_Table.newFieldInGroup("pnd_Table_Pnd_T204", "#T204", FieldType.STRING, 44);
        pnd_Table_Pnd_T205 = pnd_Table.newFieldInGroup("pnd_Table_Pnd_T205", "#T205", FieldType.STRING, 44);
        pnd_Table_Pnd_T206 = pnd_Table.newFieldInGroup("pnd_Table_Pnd_T206", "#T206", FieldType.STRING, 44);
        pnd_Table_Pnd_T207 = pnd_Table.newFieldInGroup("pnd_Table_Pnd_T207", "#T207", FieldType.STRING, 44);
        pnd_Table_Pnd_T301 = pnd_Table.newFieldInGroup("pnd_Table_Pnd_T301", "#T301", FieldType.STRING, 44);
        pnd_Table_Pnd_T303 = pnd_Table.newFieldInGroup("pnd_Table_Pnd_T303", "#T303", FieldType.STRING, 44);
        pnd_Table_Pnd_T305 = pnd_Table.newFieldInGroup("pnd_Table_Pnd_T305", "#T305", FieldType.STRING, 44);
        pnd_Table_Pnd_T306 = pnd_Table.newFieldInGroup("pnd_Table_Pnd_T306", "#T306", FieldType.STRING, 44);
        pnd_Table_Pnd_T307 = pnd_Table.newFieldInGroup("pnd_Table_Pnd_T307", "#T307", FieldType.STRING, 44);

        pnd_Table__R_Field_5 = localVariables.newGroupInRecord("pnd_Table__R_Field_5", "REDEFINE", pnd_Table);

        pnd_Table_Pnd_Tall = pnd_Table__R_Field_5.newGroupArrayInGroup("pnd_Table_Pnd_Tall", "#TALL", new DbsArrayController(1, 12));
        pnd_Table_Pnd_Tbl_Media = pnd_Table_Pnd_Tall.newFieldInGroup("pnd_Table_Pnd_Tbl_Media", "#TBL-MEDIA", FieldType.STRING, 1);
        pnd_Table_Pnd_Tbl_Rpt = pnd_Table_Pnd_Tall.newFieldInGroup("pnd_Table_Pnd_Tbl_Rpt", "#TBL-RPT", FieldType.NUMERIC, 2);
        pnd_Table_Fill1 = pnd_Table_Pnd_Tall.newFieldInGroup("pnd_Table_Fill1", "FILL1", FieldType.STRING, 1);
        pnd_Table_Pnd_Tbl_Cnt = pnd_Table_Pnd_Tall.newFieldInGroup("pnd_Table_Pnd_Tbl_Cnt", "#TBL-CNT", FieldType.NUMERIC, 7);
        pnd_Table_Fill2 = pnd_Table_Pnd_Tall.newFieldInGroup("pnd_Table_Fill2", "FILL2", FieldType.STRING, 1);
        pnd_Table_Pnd_Tbl_Nme = pnd_Table_Pnd_Tall.newFieldInGroup("pnd_Table_Pnd_Tbl_Nme", "#TBL-NME", FieldType.STRING, 32);
        pnd_In_Cnt = localVariables.newFieldInRecord("pnd_In_Cnt", "#IN-CNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Sel_Cnt = localVariables.newFieldInRecord("pnd_Sel_Cnt", "#SEL-CNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Dte_From = localVariables.newFieldInRecord("pnd_Dte_From", "#DTE-FROM", FieldType.DATE);
        pnd_Dte_To = localVariables.newFieldInRecord("pnd_Dte_To", "#DTE-TO", FieldType.DATE);
        pnd_Dte_To_T = localVariables.newFieldInRecord("pnd_Dte_To_T", "#DTE-TO-T", FieldType.TIME);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.PACKED_DECIMAL, 3);
        pnd_Tbl_Isn = localVariables.newFieldInRecord("pnd_Tbl_Isn", "#TBL-ISN", FieldType.PACKED_DECIMAL, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Tbl.reset();
        vw_cwf_Cab.reset();
        vw_cwf_Mit.reset();

        localVariables.reset();
        pnd_Tbl_Prime_Key.setInitialValue("A CWF-REPORT12-INPUT  CWFB7976");
        pnd_Cab_Key.setInitialValue("2");
        pnd_Table_Pnd_T201.setInitialValue("201 0000000 CIRS Logged Non-Microjacketable ");
        pnd_Table_Pnd_T202.setInitialValue("202 0000000 CIRS Logged Microjacketable     ");
        pnd_Table_Pnd_T203.setInitialValue("203 0000000 User Logged Non-Microjacketable ");
        pnd_Table_Pnd_T204.setInitialValue("204 0000000 User Logged Microjacketable     ");
        pnd_Table_Pnd_T205.setInitialValue("205 0000000 Back-End Imaging                ");
        pnd_Table_Pnd_T206.setInitialValue("206 0000000 Participant File Update         ");
        pnd_Table_Pnd_T207.setInitialValue("207 0000000 Merged Request (No MIT)         ");
        pnd_Table_Pnd_T301.setInitialValue("301 0000000 CIRS Logged                     ");
        pnd_Table_Pnd_T303.setInitialValue("303 0000000 User Logged                     ");
        pnd_Table_Pnd_T305.setInitialValue("305 0000000 Microjacket Requested           ");
        pnd_Table_Pnd_T306.setInitialValue("306 0000000 Digitize Select                 ");
        pnd_Table_Pnd_T307.setInitialValue("307 0000000 Merged Request (No MIT)         ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb7976() throws Exception
    {
        super("Cwfb7976");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("CWFB7976", onError);
        //*  =============================================
        //*  -------------------------------                                                                                                                              //Natural: ON ERROR
        //*  FILE 183
        vw_cwf_Tbl.startDatabaseFind                                                                                                                                      //Natural: FIND ( 1 ) CWF-TBL WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
        (
        "FIND01",
        new Wc[] { new Wc("TBL_PRIME_KEY", "=", pnd_Tbl_Prime_Key, WcType.WITH) },
        1
        );
        FIND01:
        while (condition(vw_cwf_Tbl.readNextRow("FIND01")))
        {
            vw_cwf_Tbl.setIfNotFoundControlFlag(false);
            pnd_Tbl_Isn.setValue(vw_cwf_Tbl.getAstISN("Find01"));                                                                                                         //Natural: MOVE *ISN TO #TBL-ISN
            pnd_Dte_From.setValueEdited(new ReportEditMask("YYYYMMDD"),cwf_Tbl_Last_Dte);                                                                                 //Natural: MOVE EDITED CWF-TBL.LAST-DTE TO #DTE-FROM ( EM = YYYYMMDD )
            //*  TODAY's date
            pnd_Dte_To.setValue(Global.getDATX());                                                                                                                        //Natural: MOVE *DATX TO #DTE-TO
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  *************************************
        //* * MOVE EDITED '19960701' TO #DTE-FROM (EM=YYYYMMDD)  /* COMMENT ..
        //* * MOVE EDITED '19960731' TO #DTE-TO   (EM=YYYYMMDD)  /* UPDATE BELOW
        //*  *************************************
        pnd_Work.reset();                                                                                                                                                 //Natural: RESET #WORK
        getWorkFiles().write(1, false, pnd_Work);                                                                                                                         //Natural: WRITE WORK FILE 1 #WORK
        pnd_Dte_To_T.setValue(pnd_Dte_To);                                                                                                                                //Natural: MOVE #DTE-TO TO #DTE-TO-T
        pnd_Cab_Key_Pnd_Media_Updte_Dte_Tme.setValue(pnd_Dte_From);                                                                                                       //Natural: MOVE #DTE-FROM TO #MEDIA-UPDTE-DTE-TME
        if (condition(pnd_Dte_To.lessOrEqual(pnd_Dte_From)))                                                                                                              //Natural: IF #DTE-TO NOT > #DTE-FROM
        {
            getReports().write(0, "CWFB7976 Canceled - today,",pnd_Dte_To,"not later than last full day reported,",pnd_Dte_From);                                         //Natural: WRITE 'CWFB7976 Canceled - today,' #DTE-TO 'not later than last full day reported,' #DTE-FROM
            if (Global.isEscape()) return;
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE IMMEDIATE
        }                                                                                                                                                                 //Natural: END-IF
        //*  ---------------------
        //*  FROZEN MICROJACKETS
        vw_cwf_Cab.startDatabaseRead                                                                                                                                      //Natural: READ CWF-CAB BY MEDIA-IND-UPDTE-KEY STARTING FROM #CAB-KEY
        (
        "READ01",
        new Wc[] { new Wc("MEDIA_IND_UPDTE_KEY", ">=", pnd_Cab_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("MEDIA_IND_UPDTE_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_cwf_Cab.readNextRow("READ01")))
        {
            if (condition((cwf_Cab_Media_Ind.notEquals(pnd_Cab_Key_Pnd_Media_Ind)) || (cwf_Cab_Media_Updte_Dte_Tme.greater(pnd_Dte_To_T))))                               //Natural: IF ( CWF-CAB.MEDIA-IND NOT = #MEDIA-IND ) OR ( CWF-CAB.MEDIA-UPDTE-DTE-TME > #DTE-TO-T )
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_In_Cnt.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #IN-CNT
            if (condition(cwf_Cab_Updte_Srce_Cde.equals("L") || cwf_Cab_Updte_Srce_Cde.equals("U") || cwf_Cab_Updte_Srce_Cde.equals("B") || cwf_Cab_Updte_Srce_Cde.equals("P"))) //Natural: IF CWF-CAB.UPDTE-SRCE-CDE = 'L' OR = 'U' OR = 'B' OR = 'P'
            {
                pnd_Work.reset();                                                                                                                                         //Natural: RESET #WORK CWF-MIT.TIAA-RCVD-DTE CWF-MIT.WORK-PRCSS-ID
                cwf_Mit_Tiaa_Rcvd_Dte.reset();
                cwf_Mit_Work_Prcss_Id.reset();
                //*  ------------------------
                vw_cwf_Mit.startDatabaseFind                                                                                                                              //Natural: FIND ( 1 ) CWF-MIT WITH ACTV-UNQUE-KEY = CWF-CAB.MIT-RQST-LOG-DTE-TME
                (
                "FIND02",
                new Wc[] { new Wc("ACTV_UNQUE_KEY", "=", cwf_Cab_Mit_Rqst_Log_Dte_Tme, WcType.WITH) },
                1
                );
                FIND02:
                while (condition(vw_cwf_Mit.readNextRow("FIND02")))
                {
                    vw_cwf_Mit.setIfNotFoundControlFlag(false);
                    //*  BACK END IMAGING
                    if (condition(cwf_Cab_Updte_Srce_Cde.equals("B")))                                                                                                    //Natural: IF CWF-CAB.UPDTE-SRCE-CDE = 'B'
                    {
                        pnd_Work_Rpt.setValue(5);                                                                                                                         //Natural: MOVE 05 TO #WORK.RPT
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    //*  PARTICIPANT FILE UPDATE
                    if (condition(cwf_Cab_Updte_Srce_Cde.equals("P")))                                                                                                    //Natural: IF CWF-CAB.UPDTE-SRCE-CDE = 'P'
                    {
                        pnd_Work_Rpt.setValue(6);                                                                                                                         //Natural: MOVE 06 TO #WORK.RPT
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    //*                                         /* SRCE-CDE = 'L' OR 'U'
                    if (condition(cwf_Mit_Orgnl_Unit_Cde.equals("CRC")))                                                                                                  //Natural: IF CWF-MIT.ORGNL-UNIT-CDE = 'CRC'
                    {
                        if (condition(cwf_Mit_Mj_Pull_Ind.equals("Y") || cwf_Mit_Mj_Pull_Ind.equals("R") || cwf_Mit_Mj_Pull_Ind.equals("J")))                             //Natural: IF CWF-MIT.MJ-PULL-IND = 'Y' OR = 'R' OR = 'J'
                        {
                            //*  CIRC MICROJACKETABLE
                            pnd_Work_Rpt.setValue(2);                                                                                                                     //Natural: MOVE 02 TO #WORK.RPT
                            //*  CIRC NON-MICROJACKETABLE
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Work_Rpt.setValue(1);                                                                                                                     //Natural: MOVE 01 TO #WORK.RPT
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(cwf_Mit_Mj_Pull_Ind.equals("Y") || cwf_Mit_Mj_Pull_Ind.equals("R") || cwf_Mit_Mj_Pull_Ind.equals("J")))                             //Natural: IF CWF-MIT.MJ-PULL-IND = 'Y' OR = 'R' OR = 'J'
                        {
                            //*  USER MICROJACKETABLE
                            pnd_Work_Rpt.setValue(4);                                                                                                                     //Natural: MOVE 04 TO #WORK.RPT
                            //*  USER NON-MICROJACKETABLE
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Work_Rpt.setValue(3);                                                                                                                     //Natural: MOVE 03 TO #WORK.RPT
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  ---------------------
                                                                                                                                                                          //Natural: PERFORM PROCESS-CABINETS
                sub_Process_Cabinets();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  --------------------------------------------------------------
        pnd_Cab_Key_Pnd_Media_Ind.setValue("3");                                                                                                                          //Natural: ASSIGN #MEDIA-IND = '3'
        //*  *************************************
        //*  MOVE EDITED '19960731' TO #MEDIA-UPDTE-DTE-TME (EM=YYYYMMDD) /* 11/14
        //*  *************************************
        //*  DIGITIZED MICROJACKETS
        vw_cwf_Cab.startDatabaseRead                                                                                                                                      //Natural: READ CWF-CAB BY MEDIA-IND-UPDTE-KEY STARTING FROM #CAB-KEY
        (
        "READ02",
        new Wc[] { new Wc("MEDIA_IND_UPDTE_KEY", ">=", pnd_Cab_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("MEDIA_IND_UPDTE_KEY", "ASC") }
        );
        READ02:
        while (condition(vw_cwf_Cab.readNextRow("READ02")))
        {
            if (condition((cwf_Cab_Media_Ind.notEquals(pnd_Cab_Key_Pnd_Media_Ind)) || (cwf_Cab_Media_Updte_Dte_Tme.greater(pnd_Dte_To_T))))                               //Natural: IF ( CWF-CAB.MEDIA-IND NOT = #MEDIA-IND ) OR ( CWF-CAB.MEDIA-UPDTE-DTE-TME > #DTE-TO-T )
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_In_Cnt.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #IN-CNT
            if (condition(cwf_Cab_Dgtze_Srce_Cd.equals("L") || cwf_Cab_Dgtze_Srce_Cd.equals("S") || cwf_Cab_Dgtze_Srce_Cd.equals("T")))                                   //Natural: IF CWF-CAB.DGTZE-SRCE-CD = 'L' OR = 'S' OR = 'T'
            {
                pnd_Work.reset();                                                                                                                                         //Natural: RESET #WORK CWF-MIT.TIAA-RCVD-DTE CWF-MIT.WORK-PRCSS-ID
                cwf_Mit_Tiaa_Rcvd_Dte.reset();
                cwf_Mit_Work_Prcss_Id.reset();
                //*  ------------------------
                //*  DIGITIZE SELECT
                if (condition(cwf_Cab_Dgtze_Srce_Cd.equals("S")))                                                                                                         //Natural: IF CWF-CAB.DGTZE-SRCE-CD = 'S'
                {
                    pnd_Work_Rpt.setValue(6);                                                                                                                             //Natural: MOVE 06 TO #WORK.RPT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    vw_cwf_Mit.startDatabaseFind                                                                                                                          //Natural: FIND ( 1 ) CWF-MIT WITH ACTV-UNQUE-KEY = CWF-CAB.DGTZE-RQST-LOG-DTE-TME
                    (
                    "FIND03",
                    new Wc[] { new Wc("ACTV_UNQUE_KEY", "=", cwf_Cab_Dgtze_Rqst_Log_Dte_Tme, WcType.WITH) },
                    1
                    );
                    FIND03:
                    while (condition(vw_cwf_Mit.readNextRow("FIND03")))
                    {
                        vw_cwf_Mit.setIfNotFoundControlFlag(false);
                        //*  MICROJACKET REQUESTED
                        if (condition(cwf_Cab_Dgtze_Srce_Cd.equals("T")))                                                                                                 //Natural: IF CWF-CAB.DGTZE-SRCE-CD = 'T'
                        {
                            pnd_Work_Rpt.setValue(5);                                                                                                                     //Natural: MOVE 05 TO #WORK.RPT
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                        //*                                         /* SOURCE CODE = 'L'
                        if (condition(cwf_Mit_Orgnl_Unit_Cde.equals("CRC")))                                                                                              //Natural: IF CWF-MIT.ORGNL-UNIT-CDE = 'CRC'
                        {
                            //*  CIRS LOGGED
                            pnd_Work_Rpt.setValue(1);                                                                                                                     //Natural: MOVE 01 TO #WORK.RPT
                            //*  USER LOGGED
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Work_Rpt.setValue(3);                                                                                                                     //Natural: MOVE 03 TO #WORK.RPT
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FIND
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  ------------------------
                                                                                                                                                                          //Natural: PERFORM PROCESS-CABINETS
                sub_Process_Cabinets();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  =============================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-CABINETS
        //*  ------------------------
        //*  ============================================================
        GET_TBL:                                                                                                                                                          //Natural: GET CWF-TBL #TBL-ISN
        vw_cwf_Tbl.readByID(pnd_Tbl_Isn.getLong(), "GET_TBL");
        cwf_Tbl_Prev_Dte.setValue(cwf_Tbl_Last_Dte);                                                                                                                      //Natural: MOVE CWF-TBL.LAST-DTE TO CWF-TBL.PREV-DTE
        cwf_Tbl_Last_Dte.setValueEdited(pnd_Dte_To,new ReportEditMask("YYYYMMDD"));                                                                                       //Natural: MOVE EDITED #DTE-TO ( EM = YYYYMMDD ) TO CWF-TBL.LAST-DTE
        cwf_Tbl_Run_Dte.setValue(Global.getDATU());                                                                                                                       //Natural: MOVE *DATU TO CWF-TBL.RUN-DTE
        cwf_Tbl_Tbl_Updte_Dte_Tme.setValue(Global.getTIMX());                                                                                                             //Natural: MOVE *TIMX TO CWF-TBL.TBL-UPDTE-DTE-TME
        cwf_Tbl_Tbl_Updte_Dte.setValue(Global.getDATX());                                                                                                                 //Natural: MOVE *DATX TO CWF-TBL.TBL-UPDTE-DTE
        cwf_Tbl_Tbl_Updte_Oprtr_Cde.setValue(Global.getINIT_USER());                                                                                                      //Natural: MOVE *INIT-USER TO CWF-TBL.TBL-UPDTE-OPRTR-CDE
        //*  SEE 'MOVE EDITED' ABOVE
        vw_cwf_Tbl.updateDBRow("GET_TBL");                                                                                                                                //Natural: UPDATE ( GET-TBL. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  ----------------------------------------------
        getReports().write(0, NEWLINE,pnd_In_Cnt,"Cabinet Records read");                                                                                                 //Natural: WRITE / #IN-CNT 'Cabinet Records read'
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,pnd_Sel_Cnt,"Cabinet Records selected",NEWLINE);                                                                                    //Natural: WRITE / #SEL-CNT 'Cabinet Records selected' /
        if (Global.isEscape()) return;
        FOR01:                                                                                                                                                            //Natural: FOR #X = 1 TO 12
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(12)); pnd_X.nadd(1))
        {
            getReports().write(0, pnd_Table_Pnd_Tbl_Media.getValue(pnd_X),new ColumnSpacing(3),pnd_Table_Pnd_Tbl_Cnt.getValue(pnd_X),new ColumnSpacing(3),                //Natural: WRITE #TBL-MEDIA ( #X ) 3X #TBL-CNT ( #X ) 3X #TBL-NME ( #X )
                pnd_Table_Pnd_Tbl_Nme.getValue(pnd_X));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Dte_To.nsubtract(1);                                                                                                                                          //Natural: SUBTRACT 1 FROM #DTE-TO
        getReports().write(0, pnd_Sel_Cnt,"Records from",pnd_Dte_From,"thru",pnd_Dte_To,"reported");                                                                      //Natural: WRITE #SEL-CNT 'Records from' #DTE-FROM 'thru' #DTE-TO 'reported'
        if (Global.isEscape()) return;
    }
    private void sub_Process_Cabinets() throws Exception                                                                                                                  //Natural: PROCESS-CABINETS
    {
        if (BLNatReinput.isReinput()) return;

        //*  =============================================================
        pnd_Sel_Cnt.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #SEL-CNT
        //*  MASTER INDEX MISSING ..
        if (condition(pnd_Work_Rpt.equals(getZero())))                                                                                                                    //Natural: IF #WORK.RPT = 0
        {
            //*  MERGED REQUEST
            pnd_Work_Rpt.setValue(7);                                                                                                                                     //Natural: MOVE 07 TO #WORK.RPT
        }                                                                                                                                                                 //Natural: END-IF
        FOR02:                                                                                                                                                            //Natural: FOR #X = 1 TO 12
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(12)); pnd_X.nadd(1))
        {
            if (condition((cwf_Cab_Media_Ind.equals(pnd_Table_Pnd_Tbl_Media.getValue(pnd_X))) && (pnd_Work_Rpt.equals(pnd_Table_Pnd_Tbl_Rpt.getValue(pnd_X)))))           //Natural: IF ( CWF-CAB.MEDIA-IND = #TBL-MEDIA ( #X ) ) AND ( #WORK.RPT = #TBL-RPT ( #X ) )
            {
                pnd_Table_Pnd_Tbl_Cnt.getValue(pnd_X).nadd(1);                                                                                                            //Natural: ADD 1 TO #TBL-CNT ( #X )
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Work_Media.setValue(cwf_Cab_Media_Ind);                                                                                                                       //Natural: MOVE CWF-CAB.MEDIA-IND TO #WORK.MEDIA
        pnd_Work_Dte_Tme.setValue(cwf_Cab_Media_Updte_Dte_Tme);                                                                                                           //Natural: MOVE CWF-CAB.MEDIA-UPDTE-DTE-TME TO #WORK.DTE-TME #WORK.SORT-DATE
        pnd_Work_Sort_Date.setValue(cwf_Cab_Media_Updte_Dte_Tme);
        pnd_Work_Pin.setValue(cwf_Cab_Pin_Nbr);                                                                                                                           //Natural: MOVE CWF-CAB.PIN-NBR TO #WORK.PIN
        pnd_Work_Date.setValue(cwf_Mit_Tiaa_Rcvd_Dte);                                                                                                                    //Natural: MOVE CWF-MIT.TIAA-RCVD-DTE TO #WORK.DATE
        pnd_Work_Wpid.setValue(cwf_Mit_Work_Prcss_Id);                                                                                                                    //Natural: MOVE CWF-MIT.WORK-PRCSS-ID TO #WORK.WPID
        getWorkFiles().write(1, false, pnd_Work);                                                                                                                         //Natural: WRITE WORK FILE 1 #WORK
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, "NATURAL error",Global.getERROR_NR(),"in",Global.getPROGRAM(),"at line",Global.getERROR_LINE(),"on record",pnd_In_Cnt);                     //Natural: WRITE 'NATURAL error' *ERROR-NR 'in' *PROGRAM 'at line' *ERROR-LINE 'on record' #IN-CNT
    };                                                                                                                                                                    //Natural: END-ERROR
}
