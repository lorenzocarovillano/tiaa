/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:43:21 PM
**        * FROM NATURAL PROGRAM : Cwfb6300
************************************************************
**        * FILE NAME            : Cwfb6300.java
**        * CLASS NAME           : Cwfb6300
**        * INSTANCE NAME        : Cwfb6300
************************************************************
************************************************************************
* PRODUCE EXTRACT FILE & REPORT
*
*
* PROGRAM  : CWFB6300
* FUNCTION : LEGACY CLEANUP: CREATE A FEED FROM INSTITUTIONAL CWF FOR A
*          : LIST OF WPIDS FOR SPECIFIC AREA TO ASSIST WITH OPS
*          : CONVERSION
*
* 02/01/08 :
*
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb6300 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_icwm;
    private DbsField icwm_Rqst_Orgn_Cde;
    private DbsField icwm_Ppg_Cde;
    private DbsField icwm_Elctrn_Fld_Ind;
    private DbsField icwm_Work_Prcss_Id;
    private DbsField icwm_Unit_Cde;

    private DbsGroup icwm__R_Field_1;
    private DbsField icwm_Unit_Id_Cde;
    private DbsField icwm_Unit_Rgn_Cde;
    private DbsField icwm_Unit_Spcl_Dsgntn_Cde;
    private DbsField icwm_Unit_Brnch_Group_Cde;
    private DbsField icwm_Status_Cde;
    private DbsField icwm_Status_Updte_Dte_Tme;
    private DbsField icwm_Admin_Unit_Cde;
    private DbsField icwm_Crprte_Status_Ind;
    private DbsField icwm_Tiaa_Rcvd_Dte_Tme;
    private DbsField icwm_Invrt_Tiaa_Rcvd_Dte;

    private DataAccessProgramView vw_cwf_Wpid;
    private DbsField cwf_Wpid_Work_Prcss_Id;
    private DbsField cwf_Wpid_Work_Prcss_Long_Nme;
    private DbsField cwf_Wpid_Work_Prcss_Short_Nme;
    private DbsField cwf_Wpid_Actve_Ind;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;

    private DbsGroup pnd_Wpid_Table;
    private DbsField pnd_Wpid_Table_Pnd_T_Wpid;
    private DbsField pnd_Wpid_Desc;
    private DbsField pnd_Org;
    private DbsField pnd_Unit_Status_Key;

    private DbsGroup pnd_Unit_Status_Key__R_Field_2;
    private DbsField pnd_Unit_Status_Key_Pnd_Usk_Unit;
    private DbsField pnd_Unit_Status_Key_Pnd_Usk_Stat;
    private DbsField pnd_Unit_Status_Desc;

    private DbsGroup pnd_Supp_Table;
    private DbsField pnd_Supp_Table_Pnd_Supp_Tbl_Key;

    private DbsGroup pnd_Supp_Table__R_Field_3;
    private DbsField pnd_Supp_Table_Pnd_Supp_Tbl_Authrty;
    private DbsField pnd_Supp_Table_Pnd_Supp_Tbl_Name;
    private DbsField pnd_Supp_Table_Pnd_Supp_Tbl_Code;
    private DbsField pnd_Supp_Table_Pnd_Supp_Tbl_Desc;

    private DbsGroup pnd_Out_File;
    private DbsField pnd_Out_File_Pnd_Out_Icwm_Unit_Cde;
    private DbsField pnd_Out_File_Pnd_Out_Icwm_Ppg_Cde;
    private DbsField pnd_Out_File_Pnd_Out_Icwm_Tiaa_Rcvd_Dte_Tme;
    private DbsField pnd_Out_File_Pnd_Out_Icwm_Work_Prcss_Id;
    private DbsField pnd_Out_File_Pnd_Out_Pnd_Wpid_Desc;
    private DbsField pnd_Out_File_Pnd_Out_Icwm_Status_Updte_Dte_Tme;
    private DbsField pnd_Out_File_Pnd_Out_Pnd_Unit_Status_Desc;
    private DbsField pnd_Out_File_Pnd_Out_Pnd_Org;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_icwm = new DataAccessProgramView(new NameInfo("vw_icwm", "ICWM"), "ICW_MASTER_INDEX", "ICW_MASTER_INDEX");
        icwm_Rqst_Orgn_Cde = vw_icwm.getRecord().newFieldInGroup("icwm_Rqst_Orgn_Cde", "RQST-ORGN-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RQST_ORGN_CDE");
        icwm_Rqst_Orgn_Cde.setDdmHeader("ENTRY/OPERATOR");
        icwm_Ppg_Cde = vw_icwm.getRecord().newFieldInGroup("icwm_Ppg_Cde", "PPG-CDE", FieldType.STRING, 6, RepeatingFieldStrategy.None, "PPG_CDE");
        icwm_Elctrn_Fld_Ind = vw_icwm.getRecord().newFieldInGroup("icwm_Elctrn_Fld_Ind", "ELCTRN-FLD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ELCTRN_FLD_IND");
        icwm_Elctrn_Fld_Ind.setDdmHeader("ELECTRNC/FLDR IND");
        icwm_Work_Prcss_Id = vw_icwm.getRecord().newFieldInGroup("icwm_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        icwm_Work_Prcss_Id.setDdmHeader("WORK/ID");
        icwm_Unit_Cde = vw_icwm.getRecord().newFieldInGroup("icwm_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "UNIT_CDE");
        icwm_Unit_Cde.setDdmHeader("UNIT/CODE");

        icwm__R_Field_1 = vw_icwm.getRecord().newGroupInGroup("icwm__R_Field_1", "REDEFINE", icwm_Unit_Cde);
        icwm_Unit_Id_Cde = icwm__R_Field_1.newFieldInGroup("icwm_Unit_Id_Cde", "UNIT-ID-CDE", FieldType.STRING, 5);
        icwm_Unit_Rgn_Cde = icwm__R_Field_1.newFieldInGroup("icwm_Unit_Rgn_Cde", "UNIT-RGN-CDE", FieldType.STRING, 1);
        icwm_Unit_Spcl_Dsgntn_Cde = icwm__R_Field_1.newFieldInGroup("icwm_Unit_Spcl_Dsgntn_Cde", "UNIT-SPCL-DSGNTN-CDE", FieldType.STRING, 1);
        icwm_Unit_Brnch_Group_Cde = icwm__R_Field_1.newFieldInGroup("icwm_Unit_Brnch_Group_Cde", "UNIT-BRNCH-GROUP-CDE", FieldType.STRING, 1);
        icwm_Status_Cde = vw_icwm.getRecord().newFieldInGroup("icwm_Status_Cde", "STATUS-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, "STATUS_CDE");
        icwm_Status_Updte_Dte_Tme = vw_icwm.getRecord().newFieldInGroup("icwm_Status_Updte_Dte_Tme", "STATUS-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "STATUS_UPDTE_DTE_TME");
        icwm_Status_Updte_Dte_Tme.setDdmHeader("STATUS UPDATE/DATE-TIME");
        icwm_Admin_Unit_Cde = vw_icwm.getRecord().newFieldInGroup("icwm_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ADMIN_UNIT_CDE");
        icwm_Crprte_Status_Ind = vw_icwm.getRecord().newFieldInGroup("icwm_Crprte_Status_Ind", "CRPRTE-STATUS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CRPRTE_STATUS_IND");
        icwm_Crprte_Status_Ind.setDdmHeader("CORP/STAT");
        icwm_Tiaa_Rcvd_Dte_Tme = vw_icwm.getRecord().newFieldInGroup("icwm_Tiaa_Rcvd_Dte_Tme", "TIAA-RCVD-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TIAA_RCVD_DTE_TME");
        icwm_Tiaa_Rcvd_Dte_Tme.setDdmHeader("TIAA-RCV/DTE-TME");
        icwm_Invrt_Tiaa_Rcvd_Dte = vw_icwm.getRecord().newFieldInGroup("icwm_Invrt_Tiaa_Rcvd_Dte", "INVRT-TIAA-RCVD-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "INVRT_TIAA_RCVD_DTE");
        icwm_Invrt_Tiaa_Rcvd_Dte.setDdmHeader("INVERT TIAA/RECVD DATE");
        registerRecord(vw_icwm);

        vw_cwf_Wpid = new DataAccessProgramView(new NameInfo("vw_cwf_Wpid", "CWF-WPID"), "CWF_WP_WORK_PRCSS_ID", "CWF_PROFILE");
        cwf_Wpid_Work_Prcss_Id = vw_cwf_Wpid.getRecord().newFieldInGroup("cwf_Wpid_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        cwf_Wpid_Work_Prcss_Id.setDdmHeader("WORK PRCSS ID");
        cwf_Wpid_Work_Prcss_Long_Nme = vw_cwf_Wpid.getRecord().newFieldInGroup("cwf_Wpid_Work_Prcss_Long_Nme", "WORK-PRCSS-LONG-NME", FieldType.STRING, 
            45, RepeatingFieldStrategy.None, "WORK_PRCSS_LONG_NME");
        cwf_Wpid_Work_Prcss_Long_Nme.setDdmHeader("NAME");
        cwf_Wpid_Work_Prcss_Short_Nme = vw_cwf_Wpid.getRecord().newFieldInGroup("cwf_Wpid_Work_Prcss_Short_Nme", "WORK-PRCSS-SHORT-NME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "WORK_PRCSS_SHORT_NME");
        cwf_Wpid_Work_Prcss_Short_Nme.setDdmHeader("SHORT NAME");
        cwf_Wpid_Actve_Ind = vw_cwf_Wpid.getRecord().newFieldInGroup("cwf_Wpid_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTVE_IND");
        registerRecord(vw_cwf_Wpid);

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");
        registerRecord(vw_cwf_Support_Tbl);

        pnd_Wpid_Table = localVariables.newGroupInRecord("pnd_Wpid_Table", "#WPID-TABLE");
        pnd_Wpid_Table_Pnd_T_Wpid = pnd_Wpid_Table.newFieldArrayInGroup("pnd_Wpid_Table_Pnd_T_Wpid", "#T-WPID", FieldType.STRING, 6, new DbsArrayController(1, 
            50));
        pnd_Wpid_Desc = localVariables.newFieldInRecord("pnd_Wpid_Desc", "#WPID-DESC", FieldType.STRING, 45);
        pnd_Org = localVariables.newFieldInRecord("pnd_Org", "#ORG", FieldType.STRING, 2);
        pnd_Unit_Status_Key = localVariables.newFieldInRecord("pnd_Unit_Status_Key", "#UNIT-STATUS-KEY", FieldType.STRING, 12);

        pnd_Unit_Status_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Unit_Status_Key__R_Field_2", "REDEFINE", pnd_Unit_Status_Key);
        pnd_Unit_Status_Key_Pnd_Usk_Unit = pnd_Unit_Status_Key__R_Field_2.newFieldInGroup("pnd_Unit_Status_Key_Pnd_Usk_Unit", "#USK-UNIT", FieldType.STRING, 
            8);
        pnd_Unit_Status_Key_Pnd_Usk_Stat = pnd_Unit_Status_Key__R_Field_2.newFieldInGroup("pnd_Unit_Status_Key_Pnd_Usk_Stat", "#USK-STAT", FieldType.STRING, 
            4);
        pnd_Unit_Status_Desc = localVariables.newFieldInRecord("pnd_Unit_Status_Desc", "#UNIT-STATUS-DESC", FieldType.STRING, 25);

        pnd_Supp_Table = localVariables.newGroupInRecord("pnd_Supp_Table", "#SUPP-TABLE");
        pnd_Supp_Table_Pnd_Supp_Tbl_Key = pnd_Supp_Table.newFieldInGroup("pnd_Supp_Table_Pnd_Supp_Tbl_Key", "#SUPP-TBL-KEY", FieldType.STRING, 53);

        pnd_Supp_Table__R_Field_3 = pnd_Supp_Table.newGroupInGroup("pnd_Supp_Table__R_Field_3", "REDEFINE", pnd_Supp_Table_Pnd_Supp_Tbl_Key);
        pnd_Supp_Table_Pnd_Supp_Tbl_Authrty = pnd_Supp_Table__R_Field_3.newFieldInGroup("pnd_Supp_Table_Pnd_Supp_Tbl_Authrty", "#SUPP-TBL-AUTHRTY", FieldType.STRING, 
            2);
        pnd_Supp_Table_Pnd_Supp_Tbl_Name = pnd_Supp_Table__R_Field_3.newFieldInGroup("pnd_Supp_Table_Pnd_Supp_Tbl_Name", "#SUPP-TBL-NAME", FieldType.STRING, 
            20);
        pnd_Supp_Table_Pnd_Supp_Tbl_Code = pnd_Supp_Table__R_Field_3.newFieldInGroup("pnd_Supp_Table_Pnd_Supp_Tbl_Code", "#SUPP-TBL-CODE", FieldType.STRING, 
            30);
        pnd_Supp_Table_Pnd_Supp_Tbl_Desc = pnd_Supp_Table.newFieldInGroup("pnd_Supp_Table_Pnd_Supp_Tbl_Desc", "#SUPP-TBL-DESC", FieldType.STRING, 30);

        pnd_Out_File = localVariables.newGroupInRecord("pnd_Out_File", "#OUT-FILE");
        pnd_Out_File_Pnd_Out_Icwm_Unit_Cde = pnd_Out_File.newFieldInGroup("pnd_Out_File_Pnd_Out_Icwm_Unit_Cde", "#OUT-ICWM-UNIT-CDE", FieldType.STRING, 
            7);
        pnd_Out_File_Pnd_Out_Icwm_Ppg_Cde = pnd_Out_File.newFieldInGroup("pnd_Out_File_Pnd_Out_Icwm_Ppg_Cde", "#OUT-ICWM-PPG-CDE", FieldType.STRING, 4);
        pnd_Out_File_Pnd_Out_Icwm_Tiaa_Rcvd_Dte_Tme = pnd_Out_File.newFieldInGroup("pnd_Out_File_Pnd_Out_Icwm_Tiaa_Rcvd_Dte_Tme", "#OUT-ICWM-TIAA-RCVD-DTE-TME", 
            FieldType.STRING, 8);
        pnd_Out_File_Pnd_Out_Icwm_Work_Prcss_Id = pnd_Out_File.newFieldInGroup("pnd_Out_File_Pnd_Out_Icwm_Work_Prcss_Id", "#OUT-ICWM-WORK-PRCSS-ID", FieldType.STRING, 
            6);
        pnd_Out_File_Pnd_Out_Pnd_Wpid_Desc = pnd_Out_File.newFieldInGroup("pnd_Out_File_Pnd_Out_Pnd_Wpid_Desc", "#OUT-#WPID-DESC", FieldType.STRING, 30);
        pnd_Out_File_Pnd_Out_Icwm_Status_Updte_Dte_Tme = pnd_Out_File.newFieldInGroup("pnd_Out_File_Pnd_Out_Icwm_Status_Updte_Dte_Tme", "#OUT-ICWM-STATUS-UPDTE-DTE-TME", 
            FieldType.STRING, 8);
        pnd_Out_File_Pnd_Out_Pnd_Unit_Status_Desc = pnd_Out_File.newFieldInGroup("pnd_Out_File_Pnd_Out_Pnd_Unit_Status_Desc", "#OUT-#UNIT-STATUS-DESC", 
            FieldType.STRING, 20);
        pnd_Out_File_Pnd_Out_Pnd_Org = pnd_Out_File.newFieldInGroup("pnd_Out_File_Pnd_Out_Pnd_Org", "#OUT-#ORG", FieldType.STRING, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_icwm.reset();
        vw_cwf_Wpid.reset();
        vw_cwf_Support_Tbl.reset();

        localVariables.reset();
        pnd_Wpid_Table_Pnd_T_Wpid.getValue(1).setInitialValue("TABRSP");
        pnd_Wpid_Table_Pnd_T_Wpid.getValue(2).setInitialValue("TAFRAC");
        pnd_Wpid_Table_Pnd_T_Wpid.getValue(3).setInitialValue("TAFRAR");
        pnd_Wpid_Table_Pnd_T_Wpid.getValue(4).setInitialValue("TAFRCR");
        pnd_Wpid_Table_Pnd_T_Wpid.getValue(5).setInitialValue("TAFRSP");
        pnd_Wpid_Table_Pnd_T_Wpid.getValue(6).setInitialValue("TAGRAC");
        pnd_Wpid_Table_Pnd_T_Wpid.getValue(7).setInitialValue("TAGRAR");
        pnd_Wpid_Table_Pnd_T_Wpid.getValue(8).setInitialValue("TAGRCR");
        pnd_Wpid_Table_Pnd_T_Wpid.getValue(9).setInitialValue("TAGRSP");
        pnd_Wpid_Table_Pnd_T_Wpid.getValue(10).setInitialValue("TAGRYA");
        pnd_Wpid_Table_Pnd_T_Wpid.getValue(11).setInitialValue("TARRAC");
        pnd_Wpid_Table_Pnd_T_Wpid.getValue(12).setInitialValue("TARRAR");
        pnd_Wpid_Table_Pnd_T_Wpid.getValue(13).setInitialValue("TARRFC");
        pnd_Wpid_Table_Pnd_T_Wpid.getValue(14).setInitialValue("TARRSP");
        pnd_Wpid_Table_Pnd_T_Wpid.getValue(15).setInitialValue("TARRYA");
        pnd_Wpid_Table_Pnd_T_Wpid.getValue(16).setInitialValue("TASRSP");
        pnd_Wpid_Table_Pnd_T_Wpid.getValue(17).setInitialValue("TJARAC");
        pnd_Wpid_Table_Pnd_T_Wpid.getValue(18).setInitialValue("TJARAR");
        pnd_Wpid_Table_Pnd_T_Wpid.getValue(19).setInitialValue("TJARCR");
        pnd_Wpid_Table_Pnd_T_Wpid.getValue(20).setInitialValue("TJARSP");
        pnd_Wpid_Table_Pnd_T_Wpid.getValue(21).setInitialValue("TJBRAC");
        pnd_Wpid_Table_Pnd_T_Wpid.getValue(22).setInitialValue("TJBRAR");
        pnd_Wpid_Table_Pnd_T_Wpid.getValue(23).setInitialValue("TJBRCR");
        pnd_Wpid_Table_Pnd_T_Wpid.getValue(24).setInitialValue("TJBRSP");
        pnd_Wpid_Table_Pnd_T_Wpid.getValue(25).setInitialValue("TJCRAC");
        pnd_Wpid_Table_Pnd_T_Wpid.getValue(26).setInitialValue("TJCRAR");
        pnd_Wpid_Table_Pnd_T_Wpid.getValue(27).setInitialValue("TJCRCR");
        pnd_Wpid_Table_Pnd_T_Wpid.getValue(28).setInitialValue("TJCRSP");
        pnd_Wpid_Table_Pnd_T_Wpid.getValue(29).setInitialValue("TJCRYA");
        pnd_Wpid_Table_Pnd_T_Wpid.getValue(30).setInitialValue("TJCRYC");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb6300() throws Exception
    {
        super("Cwfb6300");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) LS = 132 PS = 52;//Natural: FORMAT ( 1 ) LS = 132 PS = 52
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT *PROGRAM 35X 'Legacy Clean Up - Institutional CWF Report' / *DATX ( EM = LLL' 'DD', 'YYYY ) / 'Page:' *PAGE-NUMBER ( 1 ) ( EM = ZZZ9 )
        vw_icwm.startDatabaseRead                                                                                                                                         //Natural: READ ICWM BY CRPRTE-STATUS-CHNGE-DTE-KEY
        (
        "READ01",
        new Oc[] { new Oc("CRPRTE_STATUS_CHNGE_DTE_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_icwm.readNextRow("READ01")))
        {
            //*        STARTING FROM '0'
            //*  OPEN WORK ORDERS ONLY
            if (condition(icwm_Crprte_Status_Ind.greaterOrEqual("9")))                                                                                                    //Natural: IF ICWM.CRPRTE-STATUS-IND GE '9'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(icwm_Work_Prcss_Id.equals(pnd_Wpid_Table_Pnd_T_Wpid.getValue("*"))))                                                                            //Natural: IF ICWM.WORK-PRCSS-ID = #T-WPID ( * )
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  GET WPID DESCRIPTION
            vw_cwf_Wpid.startDatabaseRead                                                                                                                                 //Natural: READ ( 1 ) CWF-WPID BY WPID-UNIQ-KEY STARTING FROM ICWM.WORK-PRCSS-ID
            (
            "READ02",
            new Wc[] { new Wc("WPID_UNIQ_KEY", ">=", icwm_Work_Prcss_Id, WcType.BY) },
            new Oc[] { new Oc("WPID_UNIQ_KEY", "ASC") },
            1
            );
            READ02:
            while (condition(vw_cwf_Wpid.readNextRow("READ02")))
            {
                if (condition(icwm_Work_Prcss_Id.equals(cwf_Wpid_Work_Prcss_Id)))                                                                                         //Natural: IF ICWM.WORK-PRCSS-ID = CWF-WPID.WORK-PRCSS-ID
                {
                    pnd_Wpid_Desc.setValue(cwf_Wpid_Work_Prcss_Long_Nme);                                                                                                 //Natural: ASSIGN #WPID-DESC := CWF-WPID.WORK-PRCSS-LONG-NME
                    pnd_Wpid_Desc.setValue(cwf_Wpid_Work_Prcss_Short_Nme);                                                                                                //Natural: ASSIGN #WPID-DESC := CWF-WPID.WORK-PRCSS-SHORT-NME
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Wpid_Desc.setValue(" Unknown WPID");                                                                                                              //Natural: ASSIGN #WPID-DESC := ' Unknown WPID'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* ***********************************************************************
            //*  GET STATUS DESCRIPTION
            pnd_Unit_Status_Key_Pnd_Usk_Unit.setValue(icwm_Admin_Unit_Cde);                                                                                               //Natural: ASSIGN #USK-UNIT := ICWM.ADMIN-UNIT-CDE
            pnd_Unit_Status_Key_Pnd_Usk_Stat.setValue(icwm_Status_Cde);                                                                                                   //Natural: ASSIGN #USK-STAT := ICWM.STATUS-CDE
            DbsUtil.callnat(Icwn5398.class , getCurrentProcessState(), pnd_Unit_Status_Key, pnd_Unit_Status_Desc);                                                        //Natural: CALLNAT 'ICWN5398' USING #UNIT-STATUS-KEY #UNIT-STATUS-DESC
            if (condition(Global.isEscape())) return;
            if (condition(pnd_Unit_Status_Desc.equals(" ")))                                                                                                              //Natural: IF #UNIT-STATUS-DESC = ' '
            {
                pnd_Unit_Status_Desc.setValue("Unknown Status");                                                                                                          //Natural: ASSIGN #UNIT-STATUS-DESC := 'Unknown Status'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(icwm_Elctrn_Fld_Ind.equals("Y")))                                                                                                               //Natural: IF ICWM.ELCTRN-FLD-IND = 'Y'
            {
                pnd_Org.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, icwm_Rqst_Orgn_Cde, "F"));                                                               //Natural: COMPRESS ICWM.RQST-ORGN-CDE 'F' INTO #ORG LEAVING NO
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Org.setValue(icwm_Rqst_Orgn_Cde);                                                                                                                     //Natural: ASSIGN #ORG := ICWM.RQST-ORGN-CDE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Out_File_Pnd_Out_Icwm_Tiaa_Rcvd_Dte_Tme.setValueEdited(icwm_Tiaa_Rcvd_Dte_Tme,new ReportEditMask("MMDDYYYY"));                                            //Natural: MOVE EDITED TIAA-RCVD-DTE-TME ( EM = MMDDYYYY ) TO #OUT-ICWM-TIAA-RCVD-DTE-TME
            pnd_Out_File_Pnd_Out_Icwm_Status_Updte_Dte_Tme.setValueEdited(icwm_Status_Updte_Dte_Tme,new ReportEditMask("MMDDYYYY"));                                      //Natural: MOVE EDITED ICWM.STATUS-UPDTE-DTE-TME ( EM = MMDDYYYY ) TO #OUT-ICWM-STATUS-UPDTE-DTE-TME
            pnd_Out_File_Pnd_Out_Icwm_Unit_Cde.setValue(icwm_Unit_Cde);                                                                                                   //Natural: ASSIGN #OUT-ICWM-UNIT-CDE := ICWM.UNIT-CDE
            pnd_Out_File_Pnd_Out_Icwm_Ppg_Cde.setValue(icwm_Ppg_Cde);                                                                                                     //Natural: ASSIGN #OUT-ICWM-PPG-CDE := ICWM.PPG-CDE
            pnd_Out_File_Pnd_Out_Icwm_Work_Prcss_Id.setValue(icwm_Work_Prcss_Id);                                                                                         //Natural: ASSIGN #OUT-ICWM-WORK-PRCSS-ID := ICWM.WORK-PRCSS-ID
            pnd_Out_File_Pnd_Out_Pnd_Wpid_Desc.setValue(pnd_Wpid_Desc);                                                                                                   //Natural: MOVE #WPID-DESC TO #OUT-#WPID-DESC
            pnd_Out_File_Pnd_Out_Pnd_Unit_Status_Desc.setValueEdited(pnd_Unit_Status_Desc,new ReportEditMask("XXXXXXXXXXXXXXXXXXXX"));                                    //Natural: MOVE EDITED #UNIT-STATUS-DESC ( EM = X ( 20 ) ) TO #OUT-#UNIT-STATUS-DESC
            pnd_Out_File_Pnd_Out_Pnd_Org.setValue(pnd_Org);                                                                                                               //Natural: ASSIGN #OUT-#ORG := #ORG
            getWorkFiles().write(1, false, pnd_Out_File);                                                                                                                 //Natural: WRITE WORK FILE 1 #OUT-FILE
            getReports().display(1, "Unit/Team",                                                                                                                          //Natural: DISPLAY ( 1 ) 'Unit/Team' ICWM.UNIT-CDE 'PPG/Code' ICWM.PPG-CDE 'Received/Date' ICWM.TIAA-RCVD-DTE-TME ( EM = MM/DD/YYYY ) '/WPID' ICWM.WORK-PRCSS-ID 'WPID/Description' #WPID-DESC ( EM = X ( 20 ) ) 'Status/Date' ICWM.STATUS-UPDTE-DTE-TME ( EM = MM/DD/YYYY ) 'STATUS/DESCRIPTION' #UNIT-STATUS-DESC ( EM = X ( 20 ) ) 'Org' #ORG
            		icwm_Unit_Cde,"PPG/Code",
            		icwm_Ppg_Cde,"Received/Date",
            		icwm_Tiaa_Rcvd_Dte_Tme, new ReportEditMask ("MM/DD/YYYY"),"/WPID",
            		icwm_Work_Prcss_Id,"WPID/Description",
            		pnd_Wpid_Desc, new ReportEditMask ("XXXXXXXXXXXXXXXXXXXX"),"Status/Date",
            		icwm_Status_Updte_Dte_Tme, new ReportEditMask ("MM/DD/YYYY"),"STATUS/DESCRIPTION",
            		pnd_Unit_Status_Desc, new ReportEditMask ("XXXXXXXXXXXXXXXXXXXX"),"Org",
            		pnd_Org);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  DISPLAY (0)
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=52");
        Global.format(1, "LS=132 PS=52");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getPROGRAM(),new ColumnSpacing(35),"Legacy Clean Up - Institutional CWF Report",NEWLINE,Global.getDATX(), 
            new ReportEditMask ("LLL' 'DD', 'YYYY"),NEWLINE,"Page:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZZ9"));

        getReports().setDisplayColumns(1, "Unit/Team",
        		icwm_Unit_Cde,"PPG/Code",
        		icwm_Ppg_Cde,"Received/Date",
        		icwm_Tiaa_Rcvd_Dte_Tme, new ReportEditMask ("MM/DD/YYYY"),"/WPID",
        		icwm_Work_Prcss_Id,"WPID/Description",
        		pnd_Wpid_Desc, new ReportEditMask ("XXXXXXXXXXXXXXXXXXXX"),"Status/Date",
        		icwm_Status_Updte_Dte_Tme, new ReportEditMask ("MM/DD/YYYY"),"STATUS/DESCRIPTION",
        		pnd_Unit_Status_Desc, new ReportEditMask ("XXXXXXXXXXXXXXXXXXXX"),"Org",
        		pnd_Org);
    }
}
