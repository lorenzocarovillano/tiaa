/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:32:37 PM
**        * FROM NATURAL PROGRAM : Cwfb4030
************************************************************
**        * FILE NAME            : Cwfb4030.java
**        * CLASS NAME           : Cwfb4030
**        * INSTANCE NAME        : Cwfb4030
************************************************************
**SAG GENERATOR: BATCH-TIAA                       VERSION: 3.2.2
**SAG TITLE: ROUTING SEQUENCE
**SAG SYSTEM: CRPCWF
**SAG REPORT-HEADING(1): CWF ROUTING SEQUENCE CODES EXTRACT REPORT
**SAG PRINT-FILE(1): 1
**SAG HEADING-LINE: 10000000
**SAG DESCS(1): THIS PROGRAM READS THE CWF-WP-ROUTING TABLE AND WRITES
**SAG DESCS(2): A WORK FILE TO BE DOWNLOADED TO THE SQL SERVER.
**SAG PRIMARY-FILE: CWF-WP-ROUTING
**SAG PRIMARY-KEY: ROUTING-SQNCE-KEY
************************************************************************
* PROGRAM  : CWFB4030
* SYSTEM   : CRPCWF
* TITLE    : ROUTING SEQUENCE
* GENERATED: SEP 15,95 AT 12:01 PM
* FUNCTION : THIS PROGRAM READS THE CWF-WP-ROUTING TABLE AND WRITES
*            A WORK FILE TO BE DOWNLOADED TO THE SQL SERVER.
*
*
* HISTORY
**SAG DEFINE EXIT CHANGE-HISTORY
* CHANGED ON SEP 12,95 BY HARGRAV FOR RELEASE ____
* >
* >
* >
**SAG END-EXIT
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb4030 extends BLNatBase
{
    // Data Areas
    private LdaCdbatxa ldaCdbatxa;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Cur_Lang;
    private DbsField pnd_Program;

    private DataAccessProgramView vw_cwf_Wp_Routing;
    private DbsField cwf_Wp_Routing_Rt_Sqnce_Prcss_Id;

    private DbsGroup cwf_Wp_Routing__R_Field_1;
    private DbsField cwf_Wp_Routing_Rt_Sqnce_Actn_Rqstd_Cde;
    private DbsField cwf_Wp_Routing_Rt_Sqnce_Lob_Cmpny_Prdct_Cde;
    private DbsField cwf_Wp_Routing_Rt_Sqnce_Mjr_Bsnss_Prcss_Cde;
    private DbsField cwf_Wp_Routing_Rt_Sqnce_Spcfc_Bsnss_Prcss_Cde;
    private DbsField cwf_Wp_Routing_Rt_Sqnce_Nbr;
    private DbsField cwf_Wp_Routing_Unit_Cde;

    private DbsGroup cwf_Wp_Routing__R_Field_2;
    private DbsField cwf_Wp_Routing_Unit_Id_Cde;
    private DbsField cwf_Wp_Routing_Unit_Rgn_Cde;
    private DbsField cwf_Wp_Routing_Unit_Spcl_Dsgntn_Cde;
    private DbsField cwf_Wp_Routing_Unit_Brnch_Group_Cde;
    private DbsField cwf_Wp_Routing_Crprte_Srvce_Time_Stndrd_Grp;

    private DbsGroup cwf_Wp_Routing__R_Field_3;
    private DbsField cwf_Wp_Routing_Crprte_Srvce_Stndrd_Days_Nbr;
    private DbsField cwf_Wp_Routing_Crprte_Srvce_Stndrd_Hours_Nbr;
    private DbsField cwf_Wp_Routing_Crprte_Srvce_Stndrd_Mins_Nbr;
    private DbsField cwf_Wp_Routing_Actve_Ind;

    private DbsGroup cwf_Wp_Routing__R_Field_4;
    private DbsField cwf_Wp_Routing_Fill_1;
    private DbsField cwf_Wp_Routing_Actve_Ind_2_2;
    private DbsField cwf_Wp_Routing_Entry_Dte_Tme;
    private DbsField cwf_Wp_Routing_Entry_Oprtr_Cde;
    private DbsField cwf_Wp_Routing_Updte_Dte;
    private DbsField cwf_Wp_Routing_Updte_Dte_Tme;
    private DbsField cwf_Wp_Routing_Updte_Oprtr_Cde;
    private DbsField cwf_Wp_Routing_Dlte_Dte_Tme;
    private DbsField cwf_Wp_Routing_Dlte_Oprtr_Cde;
    private DbsField pnd_Header1_1;
    private DbsField pnd_Header1_2;

    private DbsGroup work_Record;
    private DbsField work_Record_Rt_Sqnce_Prcss_Id;
    private DbsField work_Record_Pnd_C1;
    private DbsField work_Record_Rt_Sqnce_Nbr;
    private DbsField work_Record_Pnd_C2;
    private DbsField work_Record_Unit_Rgn_Cde;
    private DbsField work_Record_Pnd_C3;
    private DbsField work_Record_Unit_Spcl_Dsgntn_Cde;
    private DbsField work_Record_Pnd_C4;
    private DbsField work_Record_Crprte_Srvce_Stndrd_Days_Nbr;
    private DbsField work_Record_Pnd_C5;
    private DbsField work_Record_Crprte_Srvce_Stndrd_Hours_Nbr;
    private DbsField work_Record_Pnd_C6;
    private DbsField work_Record_Crprte_Srvce_Stndrd_Mins_Nbr;
    private DbsField work_Record_Pnd_C7;
    private DbsField work_Record_Unit_Cde;

    private DbsGroup pnd_Counters;
    private DbsField pnd_Counters_Pnd_Rex_Read;
    private DbsField pnd_Counters_Pnd_Rex_Written;

    private DbsGroup pnd_Report_Fields;
    private DbsField pnd_Report_Fields_Pnd_Error_Msg;

    private DbsGroup pnd_Contstants;
    private DbsField pnd_Contstants_Pnd_Delimiter;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCdbatxa = new LdaCdbatxa();
        registerRecord(ldaCdbatxa);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Cur_Lang = localVariables.newFieldInRecord("pnd_Cur_Lang", "#CUR-LANG", FieldType.PACKED_DECIMAL, 1);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);

        vw_cwf_Wp_Routing = new DataAccessProgramView(new NameInfo("vw_cwf_Wp_Routing", "CWF-WP-ROUTING"), "CWF_WP_ROUTING", "CWF_PROFILE");
        cwf_Wp_Routing_Rt_Sqnce_Prcss_Id = vw_cwf_Wp_Routing.getRecord().newFieldInGroup("cwf_Wp_Routing_Rt_Sqnce_Prcss_Id", "RT-SQNCE-PRCSS-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "RT_SQNCE_PRCSS_ID");

        cwf_Wp_Routing__R_Field_1 = vw_cwf_Wp_Routing.getRecord().newGroupInGroup("cwf_Wp_Routing__R_Field_1", "REDEFINE", cwf_Wp_Routing_Rt_Sqnce_Prcss_Id);
        cwf_Wp_Routing_Rt_Sqnce_Actn_Rqstd_Cde = cwf_Wp_Routing__R_Field_1.newFieldInGroup("cwf_Wp_Routing_Rt_Sqnce_Actn_Rqstd_Cde", "RT-SQNCE-ACTN-RQSTD-CDE", 
            FieldType.STRING, 1);
        cwf_Wp_Routing_Rt_Sqnce_Lob_Cmpny_Prdct_Cde = cwf_Wp_Routing__R_Field_1.newFieldInGroup("cwf_Wp_Routing_Rt_Sqnce_Lob_Cmpny_Prdct_Cde", "RT-SQNCE-LOB-CMPNY-PRDCT-CDE", 
            FieldType.STRING, 2);
        cwf_Wp_Routing_Rt_Sqnce_Mjr_Bsnss_Prcss_Cde = cwf_Wp_Routing__R_Field_1.newFieldInGroup("cwf_Wp_Routing_Rt_Sqnce_Mjr_Bsnss_Prcss_Cde", "RT-SQNCE-MJR-BSNSS-PRCSS-CDE", 
            FieldType.STRING, 1);
        cwf_Wp_Routing_Rt_Sqnce_Spcfc_Bsnss_Prcss_Cde = cwf_Wp_Routing__R_Field_1.newFieldInGroup("cwf_Wp_Routing_Rt_Sqnce_Spcfc_Bsnss_Prcss_Cde", "RT-SQNCE-SPCFC-BSNSS-PRCSS-CDE", 
            FieldType.STRING, 2);
        cwf_Wp_Routing_Rt_Sqnce_Nbr = vw_cwf_Wp_Routing.getRecord().newFieldInGroup("cwf_Wp_Routing_Rt_Sqnce_Nbr", "RT-SQNCE-NBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "RT_SQNCE_NBR");
        cwf_Wp_Routing_Rt_Sqnce_Nbr.setDdmHeader("ROUTING/SEQUENCE");
        cwf_Wp_Routing_Unit_Cde = vw_cwf_Wp_Routing.getRecord().newFieldInGroup("cwf_Wp_Routing_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "UNIT_CDE");
        cwf_Wp_Routing_Unit_Cde.setDdmHeader("UNIT/CODE");

        cwf_Wp_Routing__R_Field_2 = vw_cwf_Wp_Routing.getRecord().newGroupInGroup("cwf_Wp_Routing__R_Field_2", "REDEFINE", cwf_Wp_Routing_Unit_Cde);
        cwf_Wp_Routing_Unit_Id_Cde = cwf_Wp_Routing__R_Field_2.newFieldInGroup("cwf_Wp_Routing_Unit_Id_Cde", "UNIT-ID-CDE", FieldType.STRING, 5);
        cwf_Wp_Routing_Unit_Rgn_Cde = cwf_Wp_Routing__R_Field_2.newFieldInGroup("cwf_Wp_Routing_Unit_Rgn_Cde", "UNIT-RGN-CDE", FieldType.STRING, 1);
        cwf_Wp_Routing_Unit_Spcl_Dsgntn_Cde = cwf_Wp_Routing__R_Field_2.newFieldInGroup("cwf_Wp_Routing_Unit_Spcl_Dsgntn_Cde", "UNIT-SPCL-DSGNTN-CDE", 
            FieldType.STRING, 1);
        cwf_Wp_Routing_Unit_Brnch_Group_Cde = cwf_Wp_Routing__R_Field_2.newFieldInGroup("cwf_Wp_Routing_Unit_Brnch_Group_Cde", "UNIT-BRNCH-GROUP-CDE", 
            FieldType.STRING, 1);
        cwf_Wp_Routing_Crprte_Srvce_Time_Stndrd_Grp = vw_cwf_Wp_Routing.getRecord().newFieldInGroup("cwf_Wp_Routing_Crprte_Srvce_Time_Stndrd_Grp", "CRPRTE-SRVCE-TIME-STNDRD-GRP", 
            FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, "CRPRTE_SRVCE_TIME_STNDRD_GRP");
        cwf_Wp_Routing_Crprte_Srvce_Time_Stndrd_Grp.setDdmHeader("CORPORATE SERVICE/TIME STANDARD");

        cwf_Wp_Routing__R_Field_3 = vw_cwf_Wp_Routing.getRecord().newGroupInGroup("cwf_Wp_Routing__R_Field_3", "REDEFINE", cwf_Wp_Routing_Crprte_Srvce_Time_Stndrd_Grp);
        cwf_Wp_Routing_Crprte_Srvce_Stndrd_Days_Nbr = cwf_Wp_Routing__R_Field_3.newFieldInGroup("cwf_Wp_Routing_Crprte_Srvce_Stndrd_Days_Nbr", "CRPRTE-SRVCE-STNDRD-DAYS-NBR", 
            FieldType.NUMERIC, 3);
        cwf_Wp_Routing_Crprte_Srvce_Stndrd_Hours_Nbr = cwf_Wp_Routing__R_Field_3.newFieldInGroup("cwf_Wp_Routing_Crprte_Srvce_Stndrd_Hours_Nbr", "CRPRTE-SRVCE-STNDRD-HOURS-NBR", 
            FieldType.NUMERIC, 2);
        cwf_Wp_Routing_Crprte_Srvce_Stndrd_Mins_Nbr = cwf_Wp_Routing__R_Field_3.newFieldInGroup("cwf_Wp_Routing_Crprte_Srvce_Stndrd_Mins_Nbr", "CRPRTE-SRVCE-STNDRD-MINS-NBR", 
            FieldType.NUMERIC, 2);
        cwf_Wp_Routing_Actve_Ind = vw_cwf_Wp_Routing.getRecord().newFieldInGroup("cwf_Wp_Routing_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTVE_IND");

        cwf_Wp_Routing__R_Field_4 = vw_cwf_Wp_Routing.getRecord().newGroupInGroup("cwf_Wp_Routing__R_Field_4", "REDEFINE", cwf_Wp_Routing_Actve_Ind);
        cwf_Wp_Routing_Fill_1 = cwf_Wp_Routing__R_Field_4.newFieldInGroup("cwf_Wp_Routing_Fill_1", "FILL-1", FieldType.STRING, 1);
        cwf_Wp_Routing_Actve_Ind_2_2 = cwf_Wp_Routing__R_Field_4.newFieldInGroup("cwf_Wp_Routing_Actve_Ind_2_2", "ACTVE-IND-2-2", FieldType.STRING, 1);
        cwf_Wp_Routing_Entry_Dte_Tme = vw_cwf_Wp_Routing.getRecord().newFieldInGroup("cwf_Wp_Routing_Entry_Dte_Tme", "ENTRY-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ENTRY_DTE_TME");
        cwf_Wp_Routing_Entry_Dte_Tme.setDdmHeader("ENTRY/DT-TM");
        cwf_Wp_Routing_Entry_Oprtr_Cde = vw_cwf_Wp_Routing.getRecord().newFieldInGroup("cwf_Wp_Routing_Entry_Oprtr_Cde", "ENTRY-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ENTRY_OPRTR_CDE");
        cwf_Wp_Routing_Entry_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Wp_Routing_Updte_Dte = vw_cwf_Wp_Routing.getRecord().newFieldInGroup("cwf_Wp_Routing_Updte_Dte", "UPDTE-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "UPDTE_DTE");
        cwf_Wp_Routing_Updte_Dte.setDdmHeader("UPDATE/DATE");
        cwf_Wp_Routing_Updte_Dte_Tme = vw_cwf_Wp_Routing.getRecord().newFieldInGroup("cwf_Wp_Routing_Updte_Dte_Tme", "UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "UPDTE_DTE_TME");
        cwf_Wp_Routing_Updte_Oprtr_Cde = vw_cwf_Wp_Routing.getRecord().newFieldInGroup("cwf_Wp_Routing_Updte_Oprtr_Cde", "UPDTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "UPDTE_OPRTR_CDE");
        cwf_Wp_Routing_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPER");
        cwf_Wp_Routing_Dlte_Dte_Tme = vw_cwf_Wp_Routing.getRecord().newFieldInGroup("cwf_Wp_Routing_Dlte_Dte_Tme", "DLTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "DLTE_DTE_TME");
        cwf_Wp_Routing_Dlte_Dte_Tme.setDdmHeader("DELETE/DT-TM");
        cwf_Wp_Routing_Dlte_Oprtr_Cde = vw_cwf_Wp_Routing.getRecord().newFieldInGroup("cwf_Wp_Routing_Dlte_Oprtr_Cde", "DLTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "DLTE_OPRTR_CDE");
        cwf_Wp_Routing_Dlte_Oprtr_Cde.setDdmHeader("DELETE/OPER");
        registerRecord(vw_cwf_Wp_Routing);

        pnd_Header1_1 = localVariables.newFieldInRecord("pnd_Header1_1", "#HEADER1-1", FieldType.STRING, 50);
        pnd_Header1_2 = localVariables.newFieldInRecord("pnd_Header1_2", "#HEADER1-2", FieldType.STRING, 50);

        work_Record = localVariables.newGroupInRecord("work_Record", "WORK-RECORD");
        work_Record_Rt_Sqnce_Prcss_Id = work_Record.newFieldInGroup("work_Record_Rt_Sqnce_Prcss_Id", "RT-SQNCE-PRCSS-ID", FieldType.STRING, 6);
        work_Record_Pnd_C1 = work_Record.newFieldInGroup("work_Record_Pnd_C1", "#C1", FieldType.STRING, 1);
        work_Record_Rt_Sqnce_Nbr = work_Record.newFieldInGroup("work_Record_Rt_Sqnce_Nbr", "RT-SQNCE-NBR", FieldType.NUMERIC, 3);
        work_Record_Pnd_C2 = work_Record.newFieldInGroup("work_Record_Pnd_C2", "#C2", FieldType.STRING, 1);
        work_Record_Unit_Rgn_Cde = work_Record.newFieldInGroup("work_Record_Unit_Rgn_Cde", "UNIT-RGN-CDE", FieldType.STRING, 1);
        work_Record_Pnd_C3 = work_Record.newFieldInGroup("work_Record_Pnd_C3", "#C3", FieldType.STRING, 1);
        work_Record_Unit_Spcl_Dsgntn_Cde = work_Record.newFieldInGroup("work_Record_Unit_Spcl_Dsgntn_Cde", "UNIT-SPCL-DSGNTN-CDE", FieldType.STRING, 1);
        work_Record_Pnd_C4 = work_Record.newFieldInGroup("work_Record_Pnd_C4", "#C4", FieldType.STRING, 1);
        work_Record_Crprte_Srvce_Stndrd_Days_Nbr = work_Record.newFieldInGroup("work_Record_Crprte_Srvce_Stndrd_Days_Nbr", "CRPRTE-SRVCE-STNDRD-DAYS-NBR", 
            FieldType.NUMERIC, 3);
        work_Record_Pnd_C5 = work_Record.newFieldInGroup("work_Record_Pnd_C5", "#C5", FieldType.STRING, 1);
        work_Record_Crprte_Srvce_Stndrd_Hours_Nbr = work_Record.newFieldInGroup("work_Record_Crprte_Srvce_Stndrd_Hours_Nbr", "CRPRTE-SRVCE-STNDRD-HOURS-NBR", 
            FieldType.NUMERIC, 2);
        work_Record_Pnd_C6 = work_Record.newFieldInGroup("work_Record_Pnd_C6", "#C6", FieldType.STRING, 1);
        work_Record_Crprte_Srvce_Stndrd_Mins_Nbr = work_Record.newFieldInGroup("work_Record_Crprte_Srvce_Stndrd_Mins_Nbr", "CRPRTE-SRVCE-STNDRD-MINS-NBR", 
            FieldType.NUMERIC, 2);
        work_Record_Pnd_C7 = work_Record.newFieldInGroup("work_Record_Pnd_C7", "#C7", FieldType.STRING, 1);
        work_Record_Unit_Cde = work_Record.newFieldInGroup("work_Record_Unit_Cde", "UNIT-CDE", FieldType.STRING, 5);

        pnd_Counters = localVariables.newGroupInRecord("pnd_Counters", "#COUNTERS");
        pnd_Counters_Pnd_Rex_Read = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Rex_Read", "#REX-READ", FieldType.PACKED_DECIMAL, 5);
        pnd_Counters_Pnd_Rex_Written = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Rex_Written", "#REX-WRITTEN", FieldType.PACKED_DECIMAL, 5);

        pnd_Report_Fields = localVariables.newGroupInRecord("pnd_Report_Fields", "#REPORT-FIELDS");
        pnd_Report_Fields_Pnd_Error_Msg = pnd_Report_Fields.newFieldInGroup("pnd_Report_Fields_Pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 60);

        pnd_Contstants = localVariables.newGroupInRecord("pnd_Contstants", "#CONTSTANTS");
        pnd_Contstants_Pnd_Delimiter = pnd_Contstants.newFieldInGroup("pnd_Contstants_Pnd_Delimiter", "#DELIMITER", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Wp_Routing.reset();

        ldaCdbatxa.initializeValues();

        localVariables.reset();
        pnd_Header1_1.setInitialValue("    CWF Routing Sequence Codes Extract Report");
        pnd_Header1_2.setInitialValue("                        �");
        pnd_Contstants_Pnd_Delimiter.setInitialValue("@");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb4030() throws Exception
    {
        super("Cwfb4030");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  DEFINE PRINTERS AND FORMATS
        getReports().definePrinter(2, "NOT DEFINED");                                                                                                                     //Natural: DEFINE PRINTER ( 1 )
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: ASSIGN #PROGRAM = *PROGRAM
        //*  MAP THE CURRENT LANGUAGE CODE TO THE CODE IN THE BATCH MODEL TEXT LDA.
        pnd_Cur_Lang.setValue(Global.getLANGUAGE());                                                                                                                      //Natural: ASSIGN #CUR-LANG = *LANGUAGE
        pnd_Cur_Lang.setValue(ldaCdbatxa.getCdbatxa_Pnd_Lang_Map().getValue(pnd_Cur_Lang));                                                                               //Natural: ASSIGN #CUR-LANG = CDBATXA.#LANG-MAP ( #CUR-LANG )
        //* *SAG DEFINE EXIT START-OF-PROGRAM
        //* *SAG END-EXIT
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH' THEN
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA = 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        //* *SAG DEFINE EXIT BEFORE-READ
        //* *SAG END-EXIT
        //*  PRIMARY FILE
        vw_cwf_Wp_Routing.startDatabaseRead                                                                                                                               //Natural: READ CWF-WP-ROUTING BY ROUTING-SQNCE-KEY
        (
        "READ_PRIME",
        new Oc[] { new Oc("ROUTING_SQNCE_KEY", "ASC") }
        );
        READ_PRIME:
        while (condition(vw_cwf_Wp_Routing.readNextRow("READ_PRIME")))
        {
            //* *SAG DEFINE EXIT PRIME-WRITE-FIELDS
            if (condition(cwf_Wp_Routing_Dlte_Oprtr_Cde.greater(" ")))                                                                                                    //Natural: REJECT IF DLTE-OPRTR-CDE GT ' '
            {
                continue;
            }
            pnd_Counters_Pnd_Rex_Read.nadd(1);                                                                                                                            //Natural: ADD 1 TO #REX-READ
                                                                                                                                                                          //Natural: PERFORM FORMAT-RECORD
            sub_Format_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-FILE
            sub_Write_Work_File();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* *SAG END-EXIT
            //*  PRIMARY FILE.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* *SAG DEFINE EXIT END-OF-PROGRAM
        getReports().write(1, ReportOption.NOTITLE,"Total Number of Routing Table Records Read:",pnd_Counters_Pnd_Rex_Read,NEWLINE,"Total Number of Work File Records Written :",pnd_Counters_Pnd_Rex_Written,NEWLINE,NEWLINE,new  //Natural: WRITE ( 1 ) 'Total Number of Routing Table Records Read:' #REX-READ / 'Total Number of Work File Records Written :' #REX-WRITTEN //56T 'End of Report'
            TabSetting(56),"End of Report");
        if (Global.isEscape()) return;
        //* *SAG END-EXIT
        //* *SAG DEFINE EXIT MISCELLANEOUS-SUBROUTINES
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-RECORD
        //* *************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WORK-FILE
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT
        //* *****************************
        //* *************
        //* *SAG END-EXIT
    }
    private void sub_Format_Record() throws Exception                                                                                                                     //Natural: FORMAT-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        work_Record.reset();                                                                                                                                              //Natural: RESET WORK-RECORD
        work_Record.setValuesByName(vw_cwf_Wp_Routing);                                                                                                                   //Natural: MOVE BY NAME CWF-WP-ROUTING TO WORK-RECORD
        work_Record_Pnd_C1.setValue(pnd_Contstants_Pnd_Delimiter);                                                                                                        //Natural: MOVE #DELIMITER TO #C1 #C2 #C3 #C4 #C5 #C6 #C7
        work_Record_Pnd_C2.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C3.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C4.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C5.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C6.setValue(pnd_Contstants_Pnd_Delimiter);
        work_Record_Pnd_C7.setValue(pnd_Contstants_Pnd_Delimiter);
        //* *************
        //*  FORMAT-RECORD
    }
    private void sub_Write_Work_File() throws Exception                                                                                                                   //Natural: WRITE-WORK-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getWorkFiles().write(1, false, work_Record);                                                                                                                      //Natural: WRITE WORK FILE 1 WORK-RECORD
        pnd_Counters_Pnd_Rex_Written.nadd(1);                                                                                                                             //Natural: ADD 1 TO #REX-WRITTEN
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
        sub_Write_Report();
        if (condition(Global.isEscape())) {return;}
        //* ********************************
        //*  WRITE-WORK-FILE
    }
    private void sub_Write_Report() throws Exception                                                                                                                      //Natural: WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(1, "WPID",                                                                                                                                   //Natural: DISPLAY ( 1 ) 'WPID' CWF-WP-ROUTING.RT-SQNCE-PRCSS-ID 'Seq/Num' CWF-WP-ROUTING.RT-SQNCE-NBR 'Rgn/Cde' CWF-WP-ROUTING.UNIT-RGN-CDE 'Spcl/Dgntn' CWF-WP-ROUTING.UNIT-SPCL-DSGNTN-CDE '/Days' CWF-WP-ROUTING.CRPRTE-SRVCE-STNDRD-DAYS-NBR '/Hours' CWF-WP-ROUTING.CRPRTE-SRVCE-STNDRD-HOURS-NBR '/Mins' CWF-WP-ROUTING.CRPRTE-SRVCE-STNDRD-MINS-NBR 'Unit' CWF-WP-ROUTING.UNIT-CDE
        		cwf_Wp_Routing_Rt_Sqnce_Prcss_Id,"Seq/Num",
        		cwf_Wp_Routing_Rt_Sqnce_Nbr,"Rgn/Cde",
        		cwf_Wp_Routing_Unit_Rgn_Cde,"Spcl/Dgntn",
        		cwf_Wp_Routing_Unit_Spcl_Dsgntn_Cde,"/Days",
        		cwf_Wp_Routing_Crprte_Srvce_Stndrd_Days_Nbr,"/Hours",
        		cwf_Wp_Routing_Crprte_Srvce_Stndrd_Hours_Nbr,"/Mins",
        		cwf_Wp_Routing_Crprte_Srvce_Stndrd_Mins_Nbr,"Unit",
        		cwf_Wp_Routing_Unit_Cde);
        if (Global.isEscape()) return;
        //* *************
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(41),pnd_Header1_1,new TabSetting(124),ldaCdbatxa.getCdbatxa_Pnd_Page_Txt().getValue(pnd_Cur_Lang),getReports().getPageNumberDbs(1),  //Natural: WRITE ( 1 ) NOTITLE *PROGRAM 41T #HEADER1-1 124T CDBATXA.#PAGE-TXT ( #CUR-LANG ) *PAGE-NUMBER ( 1 ) ( NL = 4 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) #HEADER1-2 124T *TIMX ( EM = HH':'II' 'AP )
                        new NumericLength (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),pnd_Header1_2,new 
                        TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"));
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");

        getReports().setDisplayColumns(1, "WPID",
        		cwf_Wp_Routing_Rt_Sqnce_Prcss_Id,"Seq/Num",
        		cwf_Wp_Routing_Rt_Sqnce_Nbr,"Rgn/Cde",
        		cwf_Wp_Routing_Unit_Rgn_Cde,"Spcl/Dgntn",
        		cwf_Wp_Routing_Unit_Spcl_Dsgntn_Cde,"/Days",
        		cwf_Wp_Routing_Crprte_Srvce_Stndrd_Days_Nbr,"/Hours",
        		cwf_Wp_Routing_Crprte_Srvce_Stndrd_Hours_Nbr,"/Mins",
        		cwf_Wp_Routing_Crprte_Srvce_Stndrd_Mins_Nbr,"Unit",
        		cwf_Wp_Routing_Unit_Cde);
    }
}
