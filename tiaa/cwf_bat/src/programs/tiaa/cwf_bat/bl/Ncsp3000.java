/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:02:21 PM
**        * FROM NATURAL PROGRAM : Ncsp3000
************************************************************
**        * FILE NAME            : Ncsp3000.java
**        * CLASS NAME           : Ncsp3000
**        * INSTANCE NAME        : Ncsp3000
************************************************************
************************************************************************
* PROGRAM  : NCSP3010
* TITLE    : NON-PARTICIPANT AUDIT REPORT
* FUNCTION : DISPLAY NON-PARTICIPANT CONTACTS ON 046 IN NP-PIN ORDER
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Ncsp3000 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_1;
    private DbsField cwf_Support_Tbl_Pnd_Regular_Run_Parms;

    private DbsGroup cwf_Support_Tbl__R_Field_2;
    private DbsField cwf_Support_Tbl_Pnd_Regular_Run_Date;
    private DbsField cwf_Support_Tbl_Pnd_Prior_Date_Parm;
    private DbsField cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde;
    private DbsField cwf_Support_Tbl_Tbl_Updte_Dte;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_3;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind;
    private DbsField pnd_B;
    private DbsField pnd_Cc;
    private DbsField pnd_Field;
    private DbsField pnd_Title_Literal;
    private DbsField pnd_Input_Dte_Tme;
    private DbsField pnd_Input_Dte_D;
    private DbsField pnd_Last_Processed_Date_A;
    private DbsField pnd_Last_Processed_Date;
    private DbsField pnd_Next_Processing_Date;
    private DbsField pnd_Isn_Tbl;
    private DbsField pnd_Last_Page_Flag;
    private DbsField pnd_Input_Data;

    private DbsGroup pnd_Input_Data__R_Field_4;
    private DbsField pnd_Input_Data_Pnd_Input_Date;
    private DbsField pnd_Report_Ran;
    private DbsField pnd_Successful;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Support_Tbl__R_Field_1 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_1", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Pnd_Regular_Run_Parms = cwf_Support_Tbl__R_Field_1.newFieldInGroup("cwf_Support_Tbl_Pnd_Regular_Run_Parms", "#REGULAR-RUN-PARMS", 
            FieldType.STRING, 30);

        cwf_Support_Tbl__R_Field_2 = cwf_Support_Tbl__R_Field_1.newGroupInGroup("cwf_Support_Tbl__R_Field_2", "REDEFINE", cwf_Support_Tbl_Pnd_Regular_Run_Parms);
        cwf_Support_Tbl_Pnd_Regular_Run_Date = cwf_Support_Tbl__R_Field_2.newFieldInGroup("cwf_Support_Tbl_Pnd_Regular_Run_Date", "#REGULAR-RUN-DATE", 
            FieldType.STRING, 8);
        cwf_Support_Tbl_Pnd_Prior_Date_Parm = cwf_Support_Tbl__R_Field_1.newFieldInGroup("cwf_Support_Tbl_Pnd_Prior_Date_Parm", "#PRIOR-DATE-PARM", FieldType.STRING, 
            8);
        cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Dlte_Oprtr_Cde", "TBL-DLTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_DLTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        cwf_Support_Tbl_Tbl_Updte_Dte = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Updte_Dte", "TBL-UPDTE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TBL_UPDTE_DTE");
        cwf_Support_Tbl_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        registerRecord(vw_cwf_Support_Tbl);

        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_3", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind = pnd_Tbl_Prime_Key__R_Field_3.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Actve_Ind", "#TBL-ACTVE-IND", FieldType.STRING, 
            1);
        pnd_B = localVariables.newFieldInRecord("pnd_B", "#B", FieldType.NUMERIC, 3);
        pnd_Cc = localVariables.newFieldInRecord("pnd_Cc", "#CC", FieldType.NUMERIC, 3);
        pnd_Field = localVariables.newFieldInRecord("pnd_Field", "#FIELD", FieldType.STRING, 30);
        pnd_Title_Literal = localVariables.newFieldInRecord("pnd_Title_Literal", "#TITLE-LITERAL", FieldType.STRING, 54);
        pnd_Input_Dte_Tme = localVariables.newFieldInRecord("pnd_Input_Dte_Tme", "#INPUT-DTE-TME", FieldType.TIME);
        pnd_Input_Dte_D = localVariables.newFieldInRecord("pnd_Input_Dte_D", "#INPUT-DTE-D", FieldType.DATE);
        pnd_Last_Processed_Date_A = localVariables.newFieldInRecord("pnd_Last_Processed_Date_A", "#LAST-PROCESSED-DATE-A", FieldType.STRING, 8);
        pnd_Last_Processed_Date = localVariables.newFieldInRecord("pnd_Last_Processed_Date", "#LAST-PROCESSED-DATE", FieldType.DATE);
        pnd_Next_Processing_Date = localVariables.newFieldInRecord("pnd_Next_Processing_Date", "#NEXT-PROCESSING-DATE", FieldType.DATE);
        pnd_Isn_Tbl = localVariables.newFieldInRecord("pnd_Isn_Tbl", "#ISN-TBL", FieldType.PACKED_DECIMAL, 8);
        pnd_Last_Page_Flag = localVariables.newFieldInRecord("pnd_Last_Page_Flag", "#LAST-PAGE-FLAG", FieldType.STRING, 1);
        pnd_Input_Data = localVariables.newFieldInRecord("pnd_Input_Data", "#INPUT-DATA", FieldType.STRING, 30);

        pnd_Input_Data__R_Field_4 = localVariables.newGroupInRecord("pnd_Input_Data__R_Field_4", "REDEFINE", pnd_Input_Data);
        pnd_Input_Data_Pnd_Input_Date = pnd_Input_Data__R_Field_4.newFieldInGroup("pnd_Input_Data_Pnd_Input_Date", "#INPUT-DATE", FieldType.STRING, 8);
        pnd_Report_Ran = localVariables.newFieldInRecord("pnd_Report_Ran", "#REPORT-RAN", FieldType.BOOLEAN, 1);
        pnd_Successful = localVariables.newFieldInRecord("pnd_Successful", "#SUCCESSFUL", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Support_Tbl.reset();

        localVariables.reset();
        pnd_Report_Ran.setInitialValue(false);
        pnd_Successful.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Ncsp3000() throws Exception
    {
        super("Ncsp3000");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 60 LS = 133 ZP = OFF
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind.setValue("A");                                                                                                          //Natural: ASSIGN #TBL-PRIME-KEY.#TBL-SCRTY-LEVEL-IND := 'A'
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme.setValue("CONTACT-SYS-PRCSS-DT");                                                                                             //Natural: ASSIGN #TBL-PRIME-KEY.#TBL-TABLE-NME := 'CONTACT-SYS-PRCSS-DT'
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field.setValue("NON-PARTICIPANT-CONTACT");                                                                                          //Natural: ASSIGN #TBL-PRIME-KEY.#TBL-KEY-FIELD := 'NON-PARTICIPANT-CONTACT'
        vw_cwf_Support_Tbl.startDatabaseFind                                                                                                                              //Natural: FIND ( 1 ) CWF-SUPPORT-TBL WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
        (
        "FIND_TBL",
        new Wc[] { new Wc("TBL_PRIME_KEY", "=", pnd_Tbl_Prime_Key, WcType.WITH) },
        1
        );
        FIND_TBL:
        while (condition(vw_cwf_Support_Tbl.readNextRow("FIND_TBL")))
        {
            vw_cwf_Support_Tbl.setIfNotFoundControlFlag(false);
            pnd_Input_Data.setValue(cwf_Support_Tbl_Pnd_Regular_Run_Parms);                                                                                               //Natural: ASSIGN #INPUT-DATA := CWF-SUPPORT-TBL.#REGULAR-RUN-PARMS
            pnd_Isn_Tbl.setValue(vw_cwf_Support_Tbl.getAstISN("FIND_TBL"));                                                                                               //Natural: ASSIGN #ISN-TBL := *ISN ( FIND-TBL. )
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        if (condition(pnd_Input_Data_Pnd_Input_Date.notEquals(" ")))                                                                                                      //Natural: IF #INPUT-DATE NE ' '
        {
            if (condition(! (DbsUtil.maskMatches(pnd_Input_Data_Pnd_Input_Date,"19-20YYMMDD"))))                                                                          //Natural: IF #INPUT-DATE NE MASK ( 19-20YYMMDD )
            {
                getReports().write(1, NEWLINE,NEWLINE,new TabSetting(30),"*",new RepeatItem(70),NEWLINE,new TabSetting(30),"*",new TabSetting(99),"*",NEWLINE,new         //Natural: WRITE ( 1 ) // 30T '*' ( 70 ) / 30T '*' 99T '*' / 30T '* INVALID INPUT DATE (YYYYMMDD) =' #INPUT-DATE 99T '*' / 30T '*' 99T '*' / 30T '*' ( 70 )
                    TabSetting(30),"* INVALID INPUT DATE (YYYYMMDD) =",pnd_Input_Data_Pnd_Input_Date,new TabSetting(99),"*",NEWLINE,new TabSetting(30),"*",new 
                    TabSetting(99),"*",NEWLINE,new TabSetting(30),"*",new RepeatItem(70));
                if (Global.isEscape()) return;
                DbsUtil.stop();  if (true) return;                                                                                                                        //Natural: STOP
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Input_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Data_Pnd_Input_Date);                                                             //Natural: MOVE EDITED #INPUT-DATE TO #INPUT-DTE-D ( EM = YYYYMMDD )
                //*       MOVE #INPUT-DTE-D TO #INPUT-DTE-TME
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(1, NEWLINE,NEWLINE,new TabSetting(30),"*",new RepeatItem(70),NEWLINE,new TabSetting(30),"*",new TabSetting(99),"*",NEWLINE,new             //Natural: WRITE ( 1 ) // 30T '*' ( 70 ) / 30T '*' 99T '*' / 30T '* NO SUPPORT TABLE RECORD FOR NON-PARTICIAPNT CONTACT *' 99T '*' / 30T '*' 99T '*' / 30T '*' ( 70 )
                TabSetting(30),"* NO SUPPORT TABLE RECORD FOR NON-PARTICIAPNT CONTACT *",new TabSetting(99),"*",NEWLINE,new TabSetting(30),"*",new TabSetting(99),"*",NEWLINE,new 
                TabSetting(30),"*",new RepeatItem(70));
            if (Global.isEscape()) return;
            DbsUtil.stop();  if (true) return;                                                                                                                            //Natural: STOP
        }                                                                                                                                                                 //Natural: END-IF
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            pnd_Input_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Data_Pnd_Input_Date);                                                                 //Natural: MOVE EDITED #INPUT-DATE TO #INPUT-DTE-D ( EM = YYYYMMDD )
            //*  MOVE #INPUT-DTE-D TO #INPUT-DTE-TME
            getReports().write(0, "INPUT-DATA = ",pnd_Input_Data);                                                                                                        //Natural: WRITE 'INPUT-DATA = ' #INPUT-DATA
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  IF #INPUT-DTE-D GE *DATX
            //*    IF #REPORT-RAN
            //*      #SUCCESSFUL := TRUE
            //*    END-IF
            //*    ESCAPE BOTTOM
            //*  END-IF
            if (condition(pnd_Input_Dte_D.greater(Global.getDATX())))                                                                                                     //Natural: IF #INPUT-DTE-D GT *DATX
            {
                if (condition(pnd_Report_Ran.getBoolean()))                                                                                                               //Natural: IF #REPORT-RAN
                {
                    pnd_Successful.setValue(true);                                                                                                                        //Natural: ASSIGN #SUCCESSFUL := TRUE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Input_Dte_D.equals(Global.getDATX())))                                                                                                  //Natural: IF #INPUT-DTE-D = *DATX
                {
                    if (condition(Global.getTIME().greaterOrEqual("00:00:00.0") && Global.getTIME().lessOrEqual("06:59:59.9")))                                           //Natural: IF *TIME = '00:00:00.0' THRU '06:59:59.9'
                    {
                        if (condition(pnd_Report_Ran.getBoolean()))                                                                                                       //Natural: IF #REPORT-RAN
                        {
                            pnd_Successful.setValue(true);                                                                                                                //Natural: ASSIGN #SUCCESSFUL := TRUE
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  FETCH PROGRAM TO PRINT NON-PARTICIPANT CONTACTS
            Global.getSTACK().pushData(StackOption.TOP, pnd_Input_Data);                                                                                                  //Natural: FETCH RETURN 'NCSP3020' #INPUT-DATA
            DbsUtil.invokeMain(DbsUtil.getBlType("NCSP3020"), getCurrentProcessState());
            if (condition(Global.isEscape())) return;
            pnd_Report_Ran.setValue(true);                                                                                                                                //Natural: ASSIGN #REPORT-RAN := TRUE
            pnd_Last_Processed_Date_A.setValue(pnd_Input_Data_Pnd_Input_Date);                                                                                            //Natural: ASSIGN #LAST-PROCESSED-DATE-A := #INPUT-DATE
            pnd_Input_Dte_D.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #INPUT-DTE-D
            pnd_Input_Data_Pnd_Input_Date.setValueEdited(pnd_Input_Dte_D,new ReportEditMask("YYYYMMDD"));                                                                 //Natural: MOVE EDITED #INPUT-DTE-D ( EM = YYYYMMDD ) TO #INPUT-DATE
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  REPORT PROGRAMS WERE EXECUTED.
        if (condition(pnd_Successful.getBoolean()))                                                                                                                       //Natural: IF #SUCCESSFUL
        {
                                                                                                                                                                          //Natural: PERFORM UPDATE-NEXT-PROCESSING-DATE
            sub_Update_Next_Processing_Date();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-NEXT-PROCESSING-DATE
    }
    private void sub_Update_Next_Processing_Date() throws Exception                                                                                                       //Natural: UPDATE-NEXT-PROCESSING-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        //*  UPDATE TABLE FILE WITH THE NEXT PROCESSING DATE.
        pnd_Last_Processed_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Last_Processed_Date_A);                                                                 //Natural: MOVE EDITED #LAST-PROCESSED-DATE-A TO #LAST-PROCESSED-DATE ( EM = YYYYMMDD )
        L_GET:                                                                                                                                                            //Natural: GET CWF-SUPPORT-TBL #ISN-TBL
        vw_cwf_Support_Tbl.readByID(pnd_Isn_Tbl.getLong(), "L_GET");
        DbsUtil.callnat(Mcsn3810.class , getCurrentProcessState(), pnd_Last_Processed_Date, pnd_Next_Processing_Date);                                                    //Natural: CALLNAT 'MCSN3810' #LAST-PROCESSED-DATE #NEXT-PROCESSING-DATE
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Next_Processing_Date.greater(getZero())))                                                                                                       //Natural: IF #NEXT-PROCESSING-DATE GT 0
        {
            cwf_Support_Tbl_Pnd_Regular_Run_Date.setValueEdited(pnd_Next_Processing_Date,new ReportEditMask("YYYYMMDD"));                                                 //Natural: MOVE EDITED #NEXT-PROCESSING-DATE ( EM = YYYYMMDD ) TO CWF-SUPPORT-TBL.#REGULAR-RUN-DATE
            cwf_Support_Tbl_Pnd_Prior_Date_Parm.setValueEdited(pnd_Last_Processed_Date,new ReportEditMask("YYYYMMDD"));                                                   //Natural: MOVE EDITED #LAST-PROCESSED-DATE ( EM = YYYYMMDD ) TO CWF-SUPPORT-TBL.#PRIOR-DATE-PARM
        }                                                                                                                                                                 //Natural: END-IF
        cwf_Support_Tbl_Tbl_Updte_Oprtr_Cde.setValue("BATCH");                                                                                                            //Natural: ASSIGN CWF-SUPPORT-TBL.TBL-UPDTE-OPRTR-CDE := 'BATCH'
        cwf_Support_Tbl_Tbl_Updte_Dte.setValue(Global.getDATX());                                                                                                         //Natural: ASSIGN CWF-SUPPORT-TBL.TBL-UPDTE-DTE := *DATX
        vw_cwf_Support_Tbl.updateDBRow("L_GET");                                                                                                                          //Natural: UPDATE ( L-GET. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=133 ZP=OFF");
    }
}
