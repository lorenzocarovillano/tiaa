/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:43:41 PM
**        * FROM NATURAL PROGRAM : Cwfb6810
************************************************************
**        * FILE NAME            : Cwfb6810.java
**        * CLASS NAME           : Cwfb6810
**        * INSTANCE NAME        : Cwfb6810
************************************************************
************************************************************************
* PROGRAM  : CWFB6810
* SYSTEM   : CWF MUTUAL FUNDS
* TITLE    : CWF UPDATE THE STATUS FROM 'send to TA' (4800) TO
*          :     'wait for TA update' (4820) FOR NON ADDRESS CHANGE, OR
*          :     'TA step complete/Route to'(7000) FOR ADDRESS CHANGE.
*          |
* 02/17/97 | GH  CREATED NEW.
* 03/24/99 | AS RESTOWED FOR CHANGES IN PDA's CWFA5012 & CWFA1800
* 06/12/00 | CS RESTOWED TO INCLUDE ROUTING RULE FIELDS IN CWFA1800 AND
*          |    CWFA5012
* 02/05/01 | GH ROUTE TO 'CDMSA'/STATUS 7000.(INSTEAD OF 'MFTA'/4820)
*          |    FOR 'TFDHx ' ONLY.
* 12/26/01 | GH ADDED THE COUNTER FOR ET.
* 09/15/03 | GH UPDATE UNIT-CDE FROM 'MFTA' TO CDMSA
* 01/17/05 | GH UPDATE UNIT-CDE FROM 'MFTA' TO CDMSA FOR ALL SSRS
* 10/15/10 | GH  COMMENT OUT "TFDHV' - MH Address Change
* 11/19/10 | GH  ROUTE POA(TFDMAU) TO MFTAL WITH CLOSE STATUS 8046.
* 08/15/13 | VR MF ROUTES THE WORK REQUESTS TO CDMSC CREATING DUPLICATE
*          |    WORK REQUESTS FOR CDMSC GROUP. SO MODIFIED THE STATUS
*          |    CODE FROM 7000 TO 8046 (CWF BULK CLOSURE) AND THE
*          |    UNIT AS RSSMP AS THE WORK REQUEST WOULD HAVE BEEN
*          |    PROCESSED BY CDMSC GROUP PER GARNET EVANS
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017 <STOW ONLY>
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb6810 extends BLNatBase
{
    // Data Areas
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfa1800 pdaCwfa1800;
    private PdaCwfa5011 pdaCwfa5011;
    private PdaCwfa5012 pdaCwfa5012;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_P pdaCwfpda_P;
    private PdaCwfpdaun pdaCwfpdaun;
    private PdaErla1000 pdaErla1000;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Counter;
    private DbsField pnd_Counter_Tfdhv;
    private DbsField pnd_Counter_Et;
    private DbsField pnd_Crrnt_Dte_Tme;
    private DbsField pnd_Crrnt_Dte_Tme_A;

    private DbsGroup pnd_Crrnt_Dte_Tme_A__R_Field_1;
    private DbsField pnd_Crrnt_Dte_Tme_A_Pnd_Crrnt_Dte_Tme_N;

    private DbsGroup pnd_Crrnt_Dte_Tme_A__R_Field_2;
    private DbsField pnd_Crrnt_Dte_Tme_A_Pnd_Crrnt_Dte_N;
    private DbsField pnd_Crrnt_Dte_Tme_A_Pnd_Crrnt_Tme_N;
    private DbsField pnd_Mitssp1;
    private DbsField pnd_Nines;
    private DbsField pnd_Rqst_Log_Dte_Tme;
    private DbsField pnd_Store_Msg;
    private DbsField pnd_Unit;
    private DbsField pnd_Counter_Dummy;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfa1800 = new PdaCwfa1800(localVariables);
        pdaCwfa5011 = new PdaCwfa5011(localVariables);
        pdaCwfa5012 = new PdaCwfa5012(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        pdaCwfpdaun = new PdaCwfpdaun(localVariables);
        pdaErla1000 = new PdaErla1000(localVariables);

        // Local Variables
        pnd_Counter = localVariables.newFieldInRecord("pnd_Counter", "#COUNTER", FieldType.NUMERIC, 5);
        pnd_Counter_Tfdhv = localVariables.newFieldInRecord("pnd_Counter_Tfdhv", "#COUNTER-TFDHV", FieldType.NUMERIC, 5);
        pnd_Counter_Et = localVariables.newFieldInRecord("pnd_Counter_Et", "#COUNTER-ET", FieldType.NUMERIC, 5);
        pnd_Crrnt_Dte_Tme = localVariables.newFieldInRecord("pnd_Crrnt_Dte_Tme", "#CRRNT-DTE-TME", FieldType.TIME);
        pnd_Crrnt_Dte_Tme_A = localVariables.newFieldInRecord("pnd_Crrnt_Dte_Tme_A", "#CRRNT-DTE-TME-A", FieldType.STRING, 15);

        pnd_Crrnt_Dte_Tme_A__R_Field_1 = localVariables.newGroupInRecord("pnd_Crrnt_Dte_Tme_A__R_Field_1", "REDEFINE", pnd_Crrnt_Dte_Tme_A);
        pnd_Crrnt_Dte_Tme_A_Pnd_Crrnt_Dte_Tme_N = pnd_Crrnt_Dte_Tme_A__R_Field_1.newFieldInGroup("pnd_Crrnt_Dte_Tme_A_Pnd_Crrnt_Dte_Tme_N", "#CRRNT-DTE-TME-N", 
            FieldType.NUMERIC, 15);

        pnd_Crrnt_Dte_Tme_A__R_Field_2 = localVariables.newGroupInRecord("pnd_Crrnt_Dte_Tme_A__R_Field_2", "REDEFINE", pnd_Crrnt_Dte_Tme_A);
        pnd_Crrnt_Dte_Tme_A_Pnd_Crrnt_Dte_N = pnd_Crrnt_Dte_Tme_A__R_Field_2.newFieldInGroup("pnd_Crrnt_Dte_Tme_A_Pnd_Crrnt_Dte_N", "#CRRNT-DTE-N", FieldType.NUMERIC, 
            8);
        pnd_Crrnt_Dte_Tme_A_Pnd_Crrnt_Tme_N = pnd_Crrnt_Dte_Tme_A__R_Field_2.newFieldInGroup("pnd_Crrnt_Dte_Tme_A_Pnd_Crrnt_Tme_N", "#CRRNT-TME-N", FieldType.NUMERIC, 
            7);
        pnd_Mitssp1 = localVariables.newFieldInRecord("pnd_Mitssp1", "#MITSSP1", FieldType.STRING, 8);
        pnd_Nines = localVariables.newFieldInRecord("pnd_Nines", "#NINES", FieldType.NUMERIC, 15);
        pnd_Rqst_Log_Dte_Tme = localVariables.newFieldInRecord("pnd_Rqst_Log_Dte_Tme", "#RQST-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Store_Msg = localVariables.newFieldArrayInRecord("pnd_Store_Msg", "#STORE-MSG", FieldType.STRING, 70, new DbsArrayController(1, 2));
        pnd_Unit = localVariables.newFieldInRecord("pnd_Unit", "#UNIT", FieldType.STRING, 8);
        pnd_Counter_Dummy = localVariables.newFieldInRecord("pnd_Counter_Dummy", "#COUNTER-DUMMY", FieldType.NUMERIC, 5);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Mitssp1.setInitialValue("MITSSP1");
        pnd_Nines.setInitialValue(-2147483648);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb6810() throws Exception
    {
        super("Cwfb6810");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("CWFB6810", onError);
        //*  INITIALIZE
        //*  PASS THE UNIT FOR REGIONALIZATION. GH 02/05/01
        pdaCwfpda_M.getMsg_Info_Sub().reset();                                                                                                                            //Natural: RESET MSG-INFO-SUB
        pdaCwfpdaun.getParm_Unit_Info_Sub_Pnd_Pnd_Unit_Cde().setValue("MFTA");                                                                                            //Natural: ASSIGN PARM-UNIT-INFO-SUB.##UNIT-CDE := 'MFTA'
        pnd_Unit.setValue("CDMSA");                                                                                                                                       //Natural: ASSIGN #UNIT := 'CDMSA'
        pnd_Crrnt_Dte_Tme_A.setValueEdited(Global.getTIMX(),new ReportEditMask("YYYYMMDDHHIISST"));                                                                       //Natural: MOVE EDITED *TIMX ( EM = YYYYMMDDHHIISST ) TO #CRRNT-DTE-TME-A
        pnd_Crrnt_Dte_Tme.setValue(Global.getTIMX());                                                                                                                     //Natural: MOVE *TIMX TO #CRRNT-DTE-TME
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 RECORD #RQST-LOG-DTE-TME
        while (condition(getWorkFiles().read(1, pnd_Rqst_Log_Dte_Tme)))
        {
            //* **    GET REQUEST DATA
            pdaCdaobj.getCdaobj_Pnd_Function().setValue("GET");                                                                                                           //Natural: MOVE 'GET' TO #FUNCTION
            pdaCwfa5012.getCwfa5012_Rqst_Log_Dte_Tme().setValue(pnd_Rqst_Log_Dte_Tme);                                                                                    //Natural: MOVE #RQST-LOG-DTE-TME TO CWFA5012.RQST-LOG-DTE-TME
                                                                                                                                                                          //Natural: PERFORM CALLNAT-CWFN5011
            sub_Callnat_Cwfn5011();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().greater(" ")))                                                                                //Natural: IF MSG-INFO-SUB.##ERROR-FIELD GT ' '
            {
                                                                                                                                                                          //Natural: PERFORM LOG-MSG
                sub_Log_Msg();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            //*   IF ADDRESS CHANGE SSR, STATUS IS '7000' AND ROUTE TO 'CDMSA_x'
            //*   ELSE STATUS IS '7000' AND ROUTE TO 'CDMSC'
            //*  GH REMOVED 10/16/2010
            if (condition(DbsUtil.maskMatches(pdaCwfa5012.getCwfa5012_Work_Prcss_Id(),"'TFDH'..")))                                                                       //Natural: IF CWFA5012.WORK-PRCSS-ID = MASK ( 'TFDH'.. )
            {
                //*     PERFORM SSR-ADDRESS-CHANGE               /* GH REMOVED 10/16/2010
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM SSR-OTHERS
                sub_Ssr_Others();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM UPDTE-DATE-USER
            sub_Updte_Date_User();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            DbsUtil.callnat(Cwfn2330.class , getCurrentProcessState(), pdaCwfa5012.getCwfa5012(), pdaCwfa5012.getCwfa5012_Id(), pdaCwfa1800.getCwfa1800(),                //Natural: CALLNAT 'CWFN2330' CWFA5012 CWFA5012-ID CWFA1800 CWFA1800-ID MSG-INFO-SUB #CRRNT-DTE-TME-A
                pdaCwfa1800.getCwfa1800_Id(), pdaCwfpda_M.getMsg_Info_Sub(), pnd_Crrnt_Dte_Tme_A);
            if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().greater(" ")))                                                                                //Natural: IF MSG-INFO-SUB.##ERROR-FIELD GT ' '
            {
                                                                                                                                                                          //Natural: PERFORM LOG-MSG
                sub_Log_Msg();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CREATE-INACTIVE-RECORD
            sub_Create_Inactive_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().notEquals(" ")))                                                                              //Natural: IF ##ERROR-FIELD NE ' '
            {
                                                                                                                                                                          //Natural: PERFORM LOG-MSG
                sub_Log_Msg();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            pdaCdaobj.getCdaobj_Pnd_Function().setValue("UPDATE");                                                                                                        //Natural: MOVE 'UPDATE' TO #FUNCTION
                                                                                                                                                                          //Natural: PERFORM CALLNAT-CWFN5011
            sub_Callnat_Cwfn5011();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().greater(" ")))                                                                                //Natural: IF MSG-INFO-SUB.##ERROR-FIELD GT ' '
            {
                                                                                                                                                                          //Natural: PERFORM LOG-MSG
                sub_Log_Msg();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Counter.nadd(1);                                                                                                                                          //Natural: ASSIGN #COUNTER := #COUNTER + 1
            //*  12/26/2001  ADDED BY GH **
            pnd_Counter_Et.nadd(1);                                                                                                                                       //Natural: ASSIGN #COUNTER-ET := #COUNTER-ET + 1
            if (condition(pnd_Counter_Et.equals(50)))                                                                                                                     //Natural: IF #COUNTER-ET = 50
            {
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                pnd_Counter_Et.reset();                                                                                                                                   //Natural: RESET #COUNTER-ET
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        getReports().write(0, "Total Record Read From Work File :",pnd_Counter,NEWLINE,"Total Record  for Address Change :",pnd_Counter_Tfdhv);                           //Natural: WRITE 'Total Record Read From Work File :' #COUNTER / 'Total Record  for Address Change :' #COUNTER-TFDHV
        if (Global.isEscape()) return;
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SSR-ADDRESS-CHANGE
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SSR-OTHERS
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-INACTIVE-RECORD
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDTE-DATE-USER
        //* *************************** *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALLNAT-CWFN5011
        //* *************************** *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOG-MSG
        //*  ************
        //*  ******************************************************************                                                                                           //Natural: ON ERROR
    }
    private void sub_Ssr_Address_Change() throws Exception                                                                                                                //Natural: SSR-ADDRESS-CHANGE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        //*  CALL REGIONALIZATION PROGRAM
        //*  REGIONIZE CDMSA TO BE CDMSA_I OR CDMAS_D.
        DbsUtil.callnat(Cwfn5382.class , getCurrentProcessState(), pdaCwfa5012.getCwfa5012_Pin_Nbr(), pnd_Unit, pdaCwfa5012.getCwfa5012_Rqst_Instn_Cde(),                 //Natural: CALLNAT 'CWFN5382' CWFA5012.PIN-NBR #UNIT CWFA5012.RQST-INSTN-CDE CWFA5012.RQST-RGN-CDE CWFA5012.RQST-SPCL-DSGNTN-CDE CWFA5012.RQST-BRNCH-CDE CWFA5012.INSTN-CDE CWFA5012.SPCL-POLICY-SRCE-CDE ( 1:10 ) CWFA5012.WORK-PRCSS-ID
            pdaCwfa5012.getCwfa5012_Rqst_Rgn_Cde(), pdaCwfa5012.getCwfa5012_Rqst_Spcl_Dsgntn_Cde(), pdaCwfa5012.getCwfa5012_Rqst_Brnch_Cde(), pdaCwfa5012.getCwfa5012_Instn_Cde(), 
            pdaCwfa5012.getCwfa5012_Spcl_Policy_Srce_Cde().getValue(1,":",10), pdaCwfa5012.getCwfa5012_Work_Prcss_Id());
        if (condition(Global.isEscape())) return;
        //*  IF RECORD ALREADY UPDATED
        //*  IF RECORD ALREADY CLOSED
        if (condition(pdaCwfa5012.getCwfa5012_Status_Cde().equals("7000") || pdaCwfa5012.getCwfa5012_Admin_Unit_Cde().notEquals("MFTA") || pdaCwfa5012.getCwfa5012_Crprte_Status_Ind().equals("9"))) //Natural: IF CWFA5012.STATUS-CDE EQ '7000' OR CWFA5012.ADMIN-UNIT-CDE NE 'MFTA' OR CWFA5012.CRPRTE-STATUS-IND = '9'
        {
            Global.setEscapeCode(EscapeType.Top); if (true) return;                                                                                                       //Natural: ESCAPE TOP
        }                                                                                                                                                                 //Natural: END-IF
        //*  KEEP HISTORY
        pdaCwfa1800.getCwfa1800().setValuesByName(pdaCwfa5012.getCwfa5012());                                                                                             //Natural: MOVE BY NAME CWFA5012 TO CWFA1800
        //*   RECEIVED THE #UNIT FROM REGIONALIZATION
        //*  09/11/2003 GH
        pdaCwfa5012.getCwfa5012_Admin_Unit_Cde().setValue(pnd_Unit);                                                                                                      //Natural: MOVE #UNIT TO CWFA5012.ADMIN-UNIT-CDE CWFA5012.UNIT-CDE
        pdaCwfa5012.getCwfa5012_Unit_Cde().setValue(pnd_Unit);
        pdaCwfa5012.getCwfa5012_Admin_Status_Cde().setValue("7000");                                                                                                      //Natural: MOVE '7000' TO CWFA5012.ADMIN-STATUS-CDE CWFA5012.STATUS-CDE
        pdaCwfa5012.getCwfa5012_Status_Cde().setValue("7000");
        pdaCwfa5012.getCwfa5012_Admin_Status_Updte_Oprtr_Cde().setValue(pnd_Mitssp1);                                                                                     //Natural: MOVE #MITSSP1 TO CWFA5012.ADMIN-STATUS-UPDTE-OPRTR-CDE CWFA5012.STATUS-UPDTE-OPRTR-CDE
        pdaCwfa5012.getCwfa5012_Status_Updte_Oprtr_Cde().setValue(pnd_Mitssp1);
        pdaCwfa5012.getCwfa5012_Admin_Status_Updte_Dte_Tme().setValue(pnd_Crrnt_Dte_Tme);                                                                                 //Natural: MOVE #CRRNT-DTE-TME TO CWFA5012.ADMIN-STATUS-UPDTE-DTE-TME CWFA5012.STATUS-UPDTE-DTE-TME
        pdaCwfa5012.getCwfa5012_Status_Updte_Dte_Tme().setValue(pnd_Crrnt_Dte_Tme);
        pnd_Counter_Tfdhv.nadd(1);                                                                                                                                        //Natural: ASSIGN #COUNTER-TFDHV := #COUNTER-TFDHV + 1
    }
    private void sub_Ssr_Others() throws Exception                                                                                                                        //Natural: SSR-OTHERS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        //*  IF RECORD ALREADY UPDATED
        //*  IF RECORD ALREADY CLOSED
        if (condition(pdaCwfa5012.getCwfa5012_Status_Cde().equals("7000") || pdaCwfa5012.getCwfa5012_Admin_Unit_Cde().notEquals("MFTA") || pdaCwfa5012.getCwfa5012_Crprte_Status_Ind().equals("9"))) //Natural: IF CWFA5012.STATUS-CDE EQ '7000' OR CWFA5012.ADMIN-UNIT-CDE NE 'MFTA' OR CWFA5012.CRPRTE-STATUS-IND = '9'
        {
            Global.setEscapeCode(EscapeType.Top); if (true) return;                                                                                                       //Natural: ESCAPE TOP
        }                                                                                                                                                                 //Natural: END-IF
        //*  KEEP HISTORY
        pdaCwfa1800.getCwfa1800().setValuesByName(pdaCwfa5012.getCwfa5012());                                                                                             //Natural: MOVE BY NAME CWFA5012 TO CWFA1800
        //*  VINODH 8/15/2013 STARTS
        //*   MOVE 'CDMSC'          TO CWFA5012.ADMIN-UNIT-CDE
        //*                            CWFA5012.UNIT-CDE         /* 09/11/2003 GH
        //*   MOVE '7000'           TO CWFA5012.ADMIN-STATUS-CDE
        //*                            CWFA5012.STATUS-CDE
        pdaCwfa5012.getCwfa5012_Admin_Unit_Cde().setValue("RSSMP");                                                                                                       //Natural: MOVE 'RSSMP' TO CWFA5012.ADMIN-UNIT-CDE CWFA5012.UNIT-CDE
        pdaCwfa5012.getCwfa5012_Unit_Cde().setValue("RSSMP");
        pdaCwfa5012.getCwfa5012_Admin_Status_Cde().setValue("8046");                                                                                                      //Natural: MOVE '8046' TO CWFA5012.ADMIN-STATUS-CDE CWFA5012.STATUS-CDE
        pdaCwfa5012.getCwfa5012_Status_Cde().setValue("8046");
        pdaCwfa5012.getCwfa5012_Crprte_Status_Ind().setValue("9");                                                                                                        //Natural: MOVE '9' TO CWFA5012.CRPRTE-STATUS-IND
        //*  VINODH 8/15/2013 ENDS
        pdaCwfa5012.getCwfa5012_Admin_Status_Updte_Oprtr_Cde().setValue(pnd_Mitssp1);                                                                                     //Natural: MOVE #MITSSP1 TO CWFA5012.ADMIN-STATUS-UPDTE-OPRTR-CDE CWFA5012.STATUS-UPDTE-OPRTR-CDE
        pdaCwfa5012.getCwfa5012_Status_Updte_Oprtr_Cde().setValue(pnd_Mitssp1);
        pdaCwfa5012.getCwfa5012_Admin_Status_Updte_Dte_Tme().setValue(pnd_Crrnt_Dte_Tme);                                                                                 //Natural: MOVE #CRRNT-DTE-TME TO CWFA5012.ADMIN-STATUS-UPDTE-DTE-TME CWFA5012.STATUS-UPDTE-DTE-TME
        pdaCwfa5012.getCwfa5012_Status_Updte_Dte_Tme().setValue(pnd_Crrnt_Dte_Tme);
        //*  11/19/2010 GH - FOR POA
        if (condition(pdaCwfa5012.getCwfa5012_Work_Prcss_Id().equals("TFDMAU")))                                                                                          //Natural: IF CWFA5012.WORK-PRCSS-ID = 'TFDMAU'
        {
            pdaCwfa5012.getCwfa5012_Admin_Unit_Cde().setValue("MFTAL");                                                                                                   //Natural: MOVE 'MFTAL' TO CWFA5012.ADMIN-UNIT-CDE CWFA5012.UNIT-CDE
            pdaCwfa5012.getCwfa5012_Unit_Cde().setValue("MFTAL");
            pdaCwfa5012.getCwfa5012_Admin_Status_Cde().setValue("8046");                                                                                                  //Natural: MOVE '8046' TO CWFA5012.ADMIN-STATUS-CDE CWFA5012.STATUS-CDE
            pdaCwfa5012.getCwfa5012_Status_Cde().setValue("8046");
            pdaCwfa5012.getCwfa5012_Crprte_Status_Ind().setValue("9");                                                                                                    //Natural: MOVE '9' TO CWFA5012.CRPRTE-STATUS-IND
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Create_Inactive_Record() throws Exception                                                                                                            //Natural: CREATE-INACTIVE-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        DbsUtil.callnat(Cwfn0005.class , getCurrentProcessState(), pdaCwfa1800.getCwfa1800(), pdaCwfa1800.getCwfa1800_Id(), pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(),  //Natural: CALLNAT 'CWFN0005' CWFA1800 CWFA1800-ID CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
            pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
        if (condition(Global.isEscape())) return;
    }
    private void sub_Updte_Date_User() throws Exception                                                                                                                   //Natural: UPDTE-DATE-USER
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        pdaCwfa5012.getCwfa5012_Last_Updte_Dte().setValue(pnd_Crrnt_Dte_Tme_A_Pnd_Crrnt_Dte_N);                                                                           //Natural: MOVE #CRRNT-DTE-N TO CWFA5012.LAST-UPDTE-DTE
        pdaCwfa5012.getCwfa5012_Last_Chnge_Dte_Tme().setValue(pnd_Crrnt_Dte_Tme_A_Pnd_Crrnt_Dte_Tme_N);                                                                   //Natural: MOVE #CRRNT-DTE-TME-N TO CWFA5012.LAST-CHNGE-DTE-TME
        pdaCwfa5012.getCwfa5012_Last_Chnge_Oprtr_Cde().setValue(pnd_Mitssp1);                                                                                             //Natural: MOVE #MITSSP1 TO CWFA5012.LAST-CHNGE-OPRTR-CDE CWFA5012.LAST-UPDTE-OPRTR-CDE
        pdaCwfa5012.getCwfa5012_Last_Updte_Oprtr_Cde().setValue(pnd_Mitssp1);
        pdaCwfa5012.getCwfa5012_Last_Chnge_Invrt_Dte_Tme().compute(new ComputeParameters(false, pdaCwfa5012.getCwfa5012_Last_Chnge_Invrt_Dte_Tme()), pnd_Nines.subtract(pdaCwfa5012.getCwfa5012_Last_Chnge_Dte_Tme())); //Natural: COMPUTE CWFA5012.LAST-CHNGE-INVRT-DTE-TME = #NINES - CWFA5012.LAST-CHNGE-DTE-TME
        pdaCwfa5012.getCwfa5012_Last_Chnge_Unit_Cde().setValue(pdaCwfpdaun.getParm_Unit_Info_Sub_Pnd_Pnd_Unit_Cde());                                                     //Natural: MOVE PARM-UNIT-INFO-SUB.##UNIT-CDE TO CWFA5012.LAST-CHNGE-UNIT-CDE
        pdaCwfa5012.getCwfa5012_Cntct_Orgn_Type_Cde().setValue(" ");                                                                                                      //Natural: MOVE ' ' TO CWFA5012.CNTCT-ORGN-TYPE-CDE
        pdaCwfa5012.getCwfa5012_Cntct_Dte_Tme().setValue(0);                                                                                                              //Natural: MOVE 0 TO CWFA5012.CNTCT-DTE-TME
        pdaCwfa5012.getCwfa5012_Cntct_Invrt_Dte_Tme().setValue(0);                                                                                                        //Natural: MOVE 0 TO CWFA5012.CNTCT-INVRT-DTE-TME
        pdaCwfa5012.getCwfa5012_Cntct_Oprtr_Id().setValue(" ");                                                                                                           //Natural: MOVE ' ' TO CWFA5012.CNTCT-OPRTR-ID
    }
    private void sub_Callnat_Cwfn5011() throws Exception                                                                                                                  //Natural: CALLNAT-CWFN5011
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************** *
        DbsUtil.callnat(Cwfn5011.class , getCurrentProcessState(), pdaCwfa5012.getCwfa5012(), pdaCwfa5012.getCwfa5012_Id(), pdaCwfa5011.getCwfa5011(),                    //Natural: CALLNAT 'CWFN5011' CWFA5012 CWFA5012-ID CWFA5011 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
        if (condition(Global.isEscape())) return;
        //*  CALLNAT-CWFN5011
    }
    private void sub_Log_Msg() throws Exception                                                                                                                           //Natural: LOG-MSG
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************** *
        //*  OBJECT..: ERLC1000
        //*  FUNCTION: INVOKE INFRASTRUCTURE MODULE TO STORE ERROR DETAILS TO
        //*            ERROR-HANDLER FILE.
        //*  MODIFICATION LOG:
        //*  =================
        //* ***********************************************************************
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        pdaErla1000.getErla1000_Err_Pgm_Name().setValue(Global.getPROGRAM());                                                                                             //Natural: ASSIGN ERLA1000.ERR-PGM-NAME = *PROGRAM
        pdaErla1000.getErla1000_Err_Pgm_Level().setValue(Global.getLEVEL());                                                                                              //Natural: ASSIGN ERLA1000.ERR-PGM-LEVEL = *LEVEL
        pdaErla1000.getErla1000_Err_Nbr().setValue(0);                                                                                                                    //Natural: ASSIGN ERLA1000.ERR-NBR = 0
        pdaErla1000.getErla1000_Err_Line_Nbr().setValue(2150);                                                                                                            //Natural: ASSIGN ERLA1000.ERR-LINE-NBR = 2150
        pdaErla1000.getErla1000_Err_Status_Cde().setValue("U");                                                                                                           //Natural: ASSIGN ERLA1000.ERR-STATUS-CDE = 'U'
        pdaErla1000.getErla1000_Err_Type_Cde().setValue("N");                                                                                                             //Natural: ASSIGN ERLA1000.ERR-TYPE-CDE = 'N'
        pdaErla1000.getErla1000_Err_Notes().setValue(DbsUtil.compress(pnd_Store_Msg.getValue(1)));                                                                        //Natural: COMPRESS #STORE-MSG ( 1 ) INTO ERLA1000.ERR-NOTES
        DbsUtil.callnat(Erln1000.class , getCurrentProcessState(), pdaErla1000.getErla1000());                                                                            //Natural: CALLNAT 'ERLN1000' ERLA1000
        if (condition(Global.isEscape())) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Natural Error", Global.getERROR_NR(), "on line", Global.getERROR_LINE(),                     //Natural: COMPRESS 'Natural Error' *ERROR-NR 'on line' *ERROR-LINE 'of' *PROGRAM INTO MSG-INFO-SUB.##MSG
            "of", Global.getPROGRAM()));
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue("NATURAL");                                                                                            //Natural: MOVE 'NATURAL' TO MSG-INFO-SUB.##ERROR-FIELD
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE EQ 'BATCH'
        {
            //*    WRITE (1) MSG-INFO-SUB.##MSG
            getReports().write(0, pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                                             //Natural: WRITE ( 0 ) MSG-INFO-SUB.##MSG
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM LOG-MSG
        sub_Log_Msg();
        if (condition(Global.isEscape())) {return;}
        DbsUtil.terminate(4);  if (true) return;                                                                                                                          //Natural: TERMINATE 4
    };                                                                                                                                                                    //Natural: END-ERROR
}
