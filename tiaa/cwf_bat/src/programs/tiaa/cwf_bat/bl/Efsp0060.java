/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:11:11 PM
**        * FROM NATURAL PROGRAM : Efsp0060
************************************************************
**        * FILE NAME            : Efsp0060.java
**        * CLASS NAME           : Efsp0060
**        * INSTANCE NAME        : Efsp0060
************************************************************
************************************************************************
* PROGRAM  : EFSP0060
* SYSTEM   : CRPCWF
* TITLE    : PRINT IMAGE UPLOAD AUDIT REPORT (BATCH NUMBER ORDER)
* GENERATED: JUN 25,93 AT 10:00 AM
* FUNCTION : THIS PROGRAM READS THE CWF-UPLOAD-AUDIT FILE AND PRINTS
*            THE AUDIT REPORT SORTED BY BATCH NUMBER, UPLOAD DATE/TIME
* HISTORY
* 05/13/96  BE  WRITE 'Nothing to Report' IF THERE ARE NO RECS TO RPT.
* 03/31/97  CS  FORCE LANDSCAPE PRINT MODE IF SUBMITTED ONLINE
* 12/17/97  CS  INCLUDED 'FORMS' WHEN REPORTING 'CIRS'
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsp0060 extends BLNatBase
{
    // Data Areas
    private GdaCwfg000 gdaCwfg000;
    private LdaEfsl0040 ldaEfsl0040;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_upload_Batch;
    private DbsField upload_Batch_Source_Id;
    private DbsField upload_Batch_Batch_Nbr;
    private DbsField upload_Batch_Batch_Ext_Nbr;
    private DbsField upload_Batch_Upld_Date_Time;
    private DbsField upload_Batch_Mins_Bch_Hdr_Cnt;
    private DbsField upload_Batch_Mins_Uploaded_Cnt;
    private DbsGroup upload_Batch_Error_MsgMuGroup;
    private DbsField upload_Batch_Error_Msg;
    private DbsField pnd_I;
    private DbsField pnd_Program;

    private DbsGroup pnd_Line;
    private DbsField pnd_Line_Pnd_First_Date;
    private DbsField pnd_Line_Pnd_Mins_Uploaded;
    private DbsField pnd_Line_Pnd_Mins_Not_Uploaded;
    private DbsField pnd_Line_Pnd_Nbr_Of_Days;
    private DbsField pnd_Line_Pnd_Days_In_Process;

    private DbsGroup pnd_Total;
    private DbsField pnd_Total_Pnd_Upload_Count;
    private DbsField pnd_Total_Pnd_Batch_Finished;
    private DbsField pnd_Total_Pnd_Batch_Unfinished;
    private DbsField pnd_Total_Pnd_Mins_Uploaded;
    private DbsField pnd_Total_Pnd_Mins_Not_Uploaded;
    private DbsField pnd_Total_Pnd_Images_Processed;
    private DbsField pnd_Input_Source_Id;
    private DbsField pnd_Input_Date_Time;
    private DbsField pnd_Input_Date_D;
    private DbsField pnd_Input_Data;

    private DbsGroup pnd_Input_Data__R_Field_1;
    private DbsField pnd_Input_Data_Pnd_Input_Time_Start;

    private DbsGroup pnd_Input_Data__R_Field_2;
    private DbsField pnd_Input_Data_Pnd_Input_Time_N;
    private DbsField pnd_Input_Data_Pnd_Input_Time_End;
    private DbsField pnd_Input_Data_Pnd_Input_Date;
    private DbsField pnd_Start_Key;
    private DbsField pnd_End_Key;
    private DbsField pnd_Upld_Date;
    private DbsField pnd_Upld_Time;

    private DbsGroup pnd_Upld_Time__R_Field_3;
    private DbsField pnd_Upld_Time_Pnd_Upld_Timen;
    private DbsField pnd_Upld_To_Date;
    private DbsField pnd_Batch_Completed;
    private DbsField pnd_Source_Id_Batch_Nbr_Key;

    private DbsGroup pnd_Source_Id_Batch_Nbr_Key__R_Field_4;
    private DbsField pnd_Source_Id_Batch_Nbr_Key_Pnd_Srce_Id;
    private DbsField pnd_Source_Id_Batch_Nbr_Key_Pnd_Batch_Nbr;
    private DbsField pnd_Source_Id_Batch_Nbr_Key_Pnd_Batch_Ext;
    private DbsField pnd_Nothing_To_Rpt;
    private DbsField pnd_Record;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaCwfg000 = GdaCwfg000.getInstance(getCallnatLevel());
        registerRecord(gdaCwfg000);
        if (gdaOnly) return;

        ldaEfsl0040 = new LdaEfsl0040();
        registerRecord(ldaEfsl0040);
        registerRecord(ldaEfsl0040.getVw_cwf_Upload_Audit());

        // Local Variables
        localVariables = new DbsRecord();

        vw_upload_Batch = new DataAccessProgramView(new NameInfo("vw_upload_Batch", "UPLOAD-BATCH"), "CWF_UPLOAD_AUDIT", "CWF_UPLOAD_AUDIT");
        upload_Batch_Source_Id = vw_upload_Batch.getRecord().newFieldInGroup("upload_Batch_Source_Id", "SOURCE-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "SOURCE_ID");
        upload_Batch_Batch_Nbr = vw_upload_Batch.getRecord().newFieldInGroup("upload_Batch_Batch_Nbr", "BATCH-NBR", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "BATCH_NBR");
        upload_Batch_Batch_Nbr.setDdmHeader("BATCH/NUMBER");
        upload_Batch_Batch_Ext_Nbr = vw_upload_Batch.getRecord().newFieldInGroup("upload_Batch_Batch_Ext_Nbr", "BATCH-EXT-NBR", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "BATCH_EXT_NBR");
        upload_Batch_Batch_Ext_Nbr.setDdmHeader("BATCH/EXT");
        upload_Batch_Upld_Date_Time = vw_upload_Batch.getRecord().newFieldInGroup("upload_Batch_Upld_Date_Time", "UPLD-DATE-TIME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "UPLD_DATE_TIME");
        upload_Batch_Upld_Date_Time.setDdmHeader("UPLOAD DATE/AND TIME");
        upload_Batch_Mins_Bch_Hdr_Cnt = vw_upload_Batch.getRecord().newFieldInGroup("upload_Batch_Mins_Bch_Hdr_Cnt", "MINS-BCH-HDR-CNT", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "MINS_BCH_HDR_CNT");
        upload_Batch_Mins_Bch_Hdr_Cnt.setDdmHeader("NBR OF/MINS");
        upload_Batch_Mins_Uploaded_Cnt = vw_upload_Batch.getRecord().newFieldInGroup("upload_Batch_Mins_Uploaded_Cnt", "MINS-UPLOADED-CNT", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "MINS_UPLOADED_CNT");
        upload_Batch_Mins_Uploaded_Cnt.setDdmHeader("MINS/UPLOADED");
        upload_Batch_Error_MsgMuGroup = vw_upload_Batch.getRecord().newGroupInGroup("UPLOAD_BATCH_ERROR_MSGMuGroup", "ERROR_MSGMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "CWF_UPLOAD_AUDIT_ERROR_MSG");
        upload_Batch_Error_Msg = upload_Batch_Error_MsgMuGroup.newFieldArrayInGroup("upload_Batch_Error_Msg", "ERROR-MSG", FieldType.STRING, 79, new DbsArrayController(1, 
            100), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ERROR_MSG");
        upload_Batch_Error_Msg.setDdmHeader("ERROR/MESSAGES");
        registerRecord(vw_upload_Batch);

        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);

        pnd_Line = localVariables.newGroupInRecord("pnd_Line", "#LINE");
        pnd_Line_Pnd_First_Date = pnd_Line.newFieldInGroup("pnd_Line_Pnd_First_Date", "#FIRST-DATE", FieldType.DATE);
        pnd_Line_Pnd_Mins_Uploaded = pnd_Line.newFieldInGroup("pnd_Line_Pnd_Mins_Uploaded", "#MINS-UPLOADED", FieldType.NUMERIC, 3);
        pnd_Line_Pnd_Mins_Not_Uploaded = pnd_Line.newFieldInGroup("pnd_Line_Pnd_Mins_Not_Uploaded", "#MINS-NOT-UPLOADED", FieldType.NUMERIC, 3);
        pnd_Line_Pnd_Nbr_Of_Days = pnd_Line.newFieldInGroup("pnd_Line_Pnd_Nbr_Of_Days", "#NBR-OF-DAYS", FieldType.NUMERIC, 7);
        pnd_Line_Pnd_Days_In_Process = pnd_Line.newFieldInGroup("pnd_Line_Pnd_Days_In_Process", "#DAYS-IN-PROCESS", FieldType.STRING, 17);

        pnd_Total = localVariables.newGroupInRecord("pnd_Total", "#TOTAL");
        pnd_Total_Pnd_Upload_Count = pnd_Total.newFieldInGroup("pnd_Total_Pnd_Upload_Count", "#UPLOAD-COUNT", FieldType.NUMERIC, 5);
        pnd_Total_Pnd_Batch_Finished = pnd_Total.newFieldInGroup("pnd_Total_Pnd_Batch_Finished", "#BATCH-FINISHED", FieldType.NUMERIC, 5);
        pnd_Total_Pnd_Batch_Unfinished = pnd_Total.newFieldInGroup("pnd_Total_Pnd_Batch_Unfinished", "#BATCH-UNFINISHED", FieldType.NUMERIC, 5);
        pnd_Total_Pnd_Mins_Uploaded = pnd_Total.newFieldInGroup("pnd_Total_Pnd_Mins_Uploaded", "#MINS-UPLOADED", FieldType.NUMERIC, 7);
        pnd_Total_Pnd_Mins_Not_Uploaded = pnd_Total.newFieldInGroup("pnd_Total_Pnd_Mins_Not_Uploaded", "#MINS-NOT-UPLOADED", FieldType.NUMERIC, 7);
        pnd_Total_Pnd_Images_Processed = pnd_Total.newFieldInGroup("pnd_Total_Pnd_Images_Processed", "#IMAGES-PROCESSED", FieldType.NUMERIC, 7);
        pnd_Input_Source_Id = localVariables.newFieldInRecord("pnd_Input_Source_Id", "#INPUT-SOURCE-ID", FieldType.STRING, 6);
        pnd_Input_Date_Time = localVariables.newFieldInRecord("pnd_Input_Date_Time", "#INPUT-DATE-TIME", FieldType.STRING, 14);
        pnd_Input_Date_D = localVariables.newFieldInRecord("pnd_Input_Date_D", "#INPUT-DATE-D", FieldType.DATE);
        pnd_Input_Data = localVariables.newFieldInRecord("pnd_Input_Data", "#INPUT-DATA", FieldType.STRING, 60);

        pnd_Input_Data__R_Field_1 = localVariables.newGroupInRecord("pnd_Input_Data__R_Field_1", "REDEFINE", pnd_Input_Data);
        pnd_Input_Data_Pnd_Input_Time_Start = pnd_Input_Data__R_Field_1.newFieldInGroup("pnd_Input_Data_Pnd_Input_Time_Start", "#INPUT-TIME-START", FieldType.STRING, 
            6);

        pnd_Input_Data__R_Field_2 = pnd_Input_Data__R_Field_1.newGroupInGroup("pnd_Input_Data__R_Field_2", "REDEFINE", pnd_Input_Data_Pnd_Input_Time_Start);
        pnd_Input_Data_Pnd_Input_Time_N = pnd_Input_Data__R_Field_2.newFieldInGroup("pnd_Input_Data_Pnd_Input_Time_N", "#INPUT-TIME-N", FieldType.NUMERIC, 
            4);
        pnd_Input_Data_Pnd_Input_Time_End = pnd_Input_Data__R_Field_1.newFieldInGroup("pnd_Input_Data_Pnd_Input_Time_End", "#INPUT-TIME-END", FieldType.STRING, 
            6);
        pnd_Input_Data_Pnd_Input_Date = pnd_Input_Data__R_Field_1.newFieldInGroup("pnd_Input_Data_Pnd_Input_Date", "#INPUT-DATE", FieldType.STRING, 8);
        pnd_Start_Key = localVariables.newFieldInRecord("pnd_Start_Key", "#START-KEY", FieldType.TIME);
        pnd_End_Key = localVariables.newFieldInRecord("pnd_End_Key", "#END-KEY", FieldType.TIME);
        pnd_Upld_Date = localVariables.newFieldInRecord("pnd_Upld_Date", "#UPLD-DATE", FieldType.DATE);
        pnd_Upld_Time = localVariables.newFieldInRecord("pnd_Upld_Time", "#UPLD-TIME", FieldType.STRING, 4);

        pnd_Upld_Time__R_Field_3 = localVariables.newGroupInRecord("pnd_Upld_Time__R_Field_3", "REDEFINE", pnd_Upld_Time);
        pnd_Upld_Time_Pnd_Upld_Timen = pnd_Upld_Time__R_Field_3.newFieldInGroup("pnd_Upld_Time_Pnd_Upld_Timen", "#UPLD-TIMEN", FieldType.NUMERIC, 4);
        pnd_Upld_To_Date = localVariables.newFieldInRecord("pnd_Upld_To_Date", "#UPLD-TO-DATE", FieldType.NUMERIC, 7);
        pnd_Batch_Completed = localVariables.newFieldInRecord("pnd_Batch_Completed", "#BATCH-COMPLETED", FieldType.BOOLEAN, 1);
        pnd_Source_Id_Batch_Nbr_Key = localVariables.newFieldInRecord("pnd_Source_Id_Batch_Nbr_Key", "#SOURCE-ID-BATCH-NBR-KEY", FieldType.STRING, 16);

        pnd_Source_Id_Batch_Nbr_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Source_Id_Batch_Nbr_Key__R_Field_4", "REDEFINE", pnd_Source_Id_Batch_Nbr_Key);
        pnd_Source_Id_Batch_Nbr_Key_Pnd_Srce_Id = pnd_Source_Id_Batch_Nbr_Key__R_Field_4.newFieldInGroup("pnd_Source_Id_Batch_Nbr_Key_Pnd_Srce_Id", "#SRCE-ID", 
            FieldType.STRING, 6);
        pnd_Source_Id_Batch_Nbr_Key_Pnd_Batch_Nbr = pnd_Source_Id_Batch_Nbr_Key__R_Field_4.newFieldInGroup("pnd_Source_Id_Batch_Nbr_Key_Pnd_Batch_Nbr", 
            "#BATCH-NBR", FieldType.NUMERIC, 8);
        pnd_Source_Id_Batch_Nbr_Key_Pnd_Batch_Ext = pnd_Source_Id_Batch_Nbr_Key__R_Field_4.newFieldInGroup("pnd_Source_Id_Batch_Nbr_Key_Pnd_Batch_Ext", 
            "#BATCH-EXT", FieldType.NUMERIC, 2);
        pnd_Nothing_To_Rpt = localVariables.newFieldInRecord("pnd_Nothing_To_Rpt", "#NOTHING-TO-RPT", FieldType.BOOLEAN, 1);
        pnd_Record = localVariables.newFieldInRecord("pnd_Record", "#RECORD", FieldType.STRING, 54);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_upload_Batch.reset();

        ldaEfsl0040.initializeValues();

        localVariables.reset();
        pnd_Nothing_To_Rpt.setInitialValue(true);
        pnd_Record.setInitialValue("�E�&l1o2a5.6c72p3e66F�&a2L�(9905X�&l0L");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Efsp0060() throws Exception
    {
        super("Efsp0060");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Efsp0060|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        while(true)
        {
            try
            {
                getReports().definePrinter(2, "AUDIT");                                                                                                                   //Natural: DEFINE PRINTER ( AUDIT = 1 )
                pnd_Program.setValue(Global.getPROGRAM());                                                                                                                //Natural: FORMAT ( AUDIT ) LS = 133 PS = 60 ZP = OFF;//Natural: ASSIGN #PROGRAM = *PROGRAM
                //*  INPUT THE SOURCE ID AND DATE-TIME PARAMETERS
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Source_Id,pnd_Input_Data);                                                                   //Natural: INPUT #INPUT-SOURCE-ID #INPUT-DATA
                pnd_Input_Date_Time.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Input_Data_Pnd_Input_Date, pnd_Input_Data_Pnd_Input_Time_Start));        //Natural: COMPRESS #INPUT-DATE #INPUT-TIME-START INTO #INPUT-DATE-TIME LEAVING NO SPACE
                pnd_Start_Key.setValueEdited(new ReportEditMask("YYYYMMDDHHIISS"),pnd_Input_Date_Time);                                                                   //Natural: MOVE EDITED #INPUT-DATE-TIME TO #START-KEY ( EM = YYYYMMDDHHIISS )
                pnd_Input_Date_Time.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Input_Data_Pnd_Input_Date, pnd_Input_Data_Pnd_Input_Time_End));          //Natural: COMPRESS #INPUT-DATE #INPUT-TIME-END INTO #INPUT-DATE-TIME LEAVING NO
                pnd_End_Key.setValueEdited(new ReportEditMask("YYYYMMDDHHIISS"),pnd_Input_Date_Time);                                                                     //Natural: MOVE EDITED #INPUT-DATE-TIME TO #END-KEY ( EM = YYYYMMDDHHIISS )
                pnd_Input_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Data_Pnd_Input_Date);                                                            //Natural: MOVE EDITED #INPUT-DATE TO #INPUT-DATE-D ( EM = YYYYMMDD )
                //*  ADD 1 TO THE DAY COUNT
                pnd_End_Key.nadd(864000);                                                                                                                                 //Natural: ADD 864000 TO #END-KEY
                //*  READ THE AUDIT RECORDS FOR A SELECTED DAY
                ldaEfsl0040.getVw_cwf_Upload_Audit().startDatabaseRead                                                                                                    //Natural: READ CWF-UPLOAD-AUDIT BY UPLD-DATE-TIME STARTING FROM #START-KEY ENDING AT #END-KEY
                (
                "READ_AUDIT",
                new Wc[] { new Wc("UPLD_DATE_TIME", ">=", pnd_Start_Key, "And", WcType.BY) ,
                new Wc("UPLD_DATE_TIME", "<=", pnd_End_Key, WcType.BY) },
                new Oc[] { new Oc("UPLD_DATE_TIME", "ASC") }
                );
                READ_AUDIT:
                while (condition(ldaEfsl0040.getVw_cwf_Upload_Audit().readNextRow("READ_AUDIT")))
                {
                    //*  REJECT ERROR BATCHES...THESE WILL BE REPORTED ON EXCEPTION REPORT
                    //*  REJECT IF CWF-UPLOAD-AUDIT.EXCEPTION-MSG NE ' '
                    //* ---  COS 12.17.97
                    //*   INCLUDE 'FORMS' IF 'CIRS'
                    if (condition(ldaEfsl0040.getCwf_Upload_Audit_Source_Id().notEquals(pnd_Input_Source_Id) && ! (ldaEfsl0040.getCwf_Upload_Audit_Source_Id().equals("FORMS")  //Natural: REJECT IF CWF-UPLOAD-AUDIT.SOURCE-ID NE #INPUT-SOURCE-ID AND NOT ( CWF-UPLOAD-AUDIT.SOURCE-ID EQ 'FORMS' AND #INPUT-SOURCE-ID EQ 'CIRS' )
                        && pnd_Input_Source_Id.equals("CIRS"))))
                    {
                        continue;
                    }
                    //*  IGNORE
                    if (condition(ldaEfsl0040.getCwf_Upload_Audit_Mins_Uploaded_Cnt().equals(getZero()) && ldaEfsl0040.getCwf_Upload_Audit_Error_Msg().getValue(1).notEquals(" "))) //Natural: REJECT IF CWF-UPLOAD-AUDIT.MINS-UPLOADED-CNT = 0 AND CWF-UPLOAD-AUDIT.ERROR-MSG ( 1 ) NE ' '
                    {
                        continue;
                    }
                    getSort().writeSortInData(ldaEfsl0040.getCwf_Upload_Audit_Source_Id(), ldaEfsl0040.getCwf_Upload_Audit_Batch_Nbr(), ldaEfsl0040.getCwf_Upload_Audit_Upld_Date_Time(),  //Natural: END-ALL
                        ldaEfsl0040.getCwf_Upload_Audit_Batch_Ext_Nbr(), ldaEfsl0040.getCwf_Upload_Audit_Batch_Label_Txt(), ldaEfsl0040.getCwf_Upload_Audit_Mins_Bch_Hdr_Cnt(), 
                        ldaEfsl0040.getCwf_Upload_Audit_Mins_Uploaded_Cnt(), ldaEfsl0040.getCwf_Upload_Audit_Images_Uploaded_Cnt());
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                getSort().sortData(ldaEfsl0040.getCwf_Upload_Audit_Source_Id(), ldaEfsl0040.getCwf_Upload_Audit_Batch_Nbr(), ldaEfsl0040.getCwf_Upload_Audit_Upld_Date_Time(),  //Natural: SORT BY CWF-UPLOAD-AUDIT.SOURCE-ID CWF-UPLOAD-AUDIT.BATCH-NBR CWF-UPLOAD-AUDIT.UPLD-DATE-TIME CWF-UPLOAD-AUDIT.BATCH-EXT-NBR USING CWF-UPLOAD-AUDIT.BATCH-LABEL-TXT CWF-UPLOAD-AUDIT.MINS-BCH-HDR-CNT CWF-UPLOAD-AUDIT.MINS-UPLOADED-CNT CWF-UPLOAD-AUDIT.IMAGES-UPLOADED-CNT
                    ldaEfsl0040.getCwf_Upload_Audit_Batch_Ext_Nbr());
                boolean endOfDataSort01 = true;
                boolean firstSort01 = true;
                SORT01:
                while (condition(getSort().readSortOutData(ldaEfsl0040.getCwf_Upload_Audit_Source_Id(), ldaEfsl0040.getCwf_Upload_Audit_Batch_Nbr(), ldaEfsl0040.getCwf_Upload_Audit_Upld_Date_Time(), 
                    ldaEfsl0040.getCwf_Upload_Audit_Batch_Ext_Nbr(), ldaEfsl0040.getCwf_Upload_Audit_Batch_Label_Txt(), ldaEfsl0040.getCwf_Upload_Audit_Mins_Bch_Hdr_Cnt(), 
                    ldaEfsl0040.getCwf_Upload_Audit_Mins_Uploaded_Cnt(), ldaEfsl0040.getCwf_Upload_Audit_Images_Uploaded_Cnt())))
                {
                    if (condition(getSort().getAstCOUNTER().greater(0)))
                    {
                        atBreakEventSort01(false);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom()))
                                break;
                            else if (condition(Global.isEscapeBottomImmediate()))
                            {
                                endOfDataSort01 = false;
                                break;
                            }
                            else if (condition(Global.isEscapeTop()))
                            continue;
                            else if (condition())
                            return;
                        }
                    }
                    pnd_Line.reset();                                                                                                                                     //Natural: RESET #LINE
                    //* ---  FOR EACH UPLOAD, COMPUTE OUTSTANDING MINS ETC.
                                                                                                                                                                          //Natural: PERFORM DETERMINE-OUTSTANDING-MINS-IF-ANY
                    sub_Determine_Outstanding_Mins_If_Any();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    //*                                                                                                                                                   //Natural: AT BREAK CWF-UPLOAD-AUDIT.BATCH-NBR
                    //*                                                                                                                                                   //Natural: AT BREAK CWF-UPLOAD-AUDIT.SOURCE-ID
                    pnd_Total_Pnd_Upload_Count.nadd(1);                                                                                                                   //Natural: ADD 1 TO #TOTAL.#UPLOAD-COUNT
                    pnd_Line_Pnd_Mins_Not_Uploaded.compute(new ComputeParameters(false, pnd_Line_Pnd_Mins_Not_Uploaded), ldaEfsl0040.getCwf_Upload_Audit_Mins_Bch_Hdr_Cnt().subtract(ldaEfsl0040.getCwf_Upload_Audit_Mins_Uploaded_Cnt())); //Natural: COMPUTE #LINE.#MINS-NOT-UPLOADED = CWF-UPLOAD-AUDIT.MINS-BCH-HDR-CNT - CWF-UPLOAD-AUDIT.MINS-UPLOADED-CNT
                    pnd_Total_Pnd_Images_Processed.nadd(ldaEfsl0040.getCwf_Upload_Audit_Images_Uploaded_Cnt());                                                           //Natural: ADD CWF-UPLOAD-AUDIT.IMAGES-UPLOADED-CNT TO #TOTAL.#IMAGES-PROCESSED
                    pnd_Total_Pnd_Mins_Uploaded.nadd(ldaEfsl0040.getCwf_Upload_Audit_Mins_Uploaded_Cnt());                                                                //Natural: ADD CWF-UPLOAD-AUDIT.MINS-UPLOADED-CNT TO #TOTAL.#MINS-UPLOADED
                    if (condition(! (pnd_Batch_Completed.getBoolean())))                                                                                                  //Natural: IF NOT #BATCH-COMPLETED
                    {
                        pnd_Line_Pnd_Nbr_Of_Days.compute(new ComputeParameters(false, pnd_Line_Pnd_Nbr_Of_Days), pnd_Input_Date_D.subtract(pnd_Line_Pnd_First_Date));     //Natural: COMPUTE #NBR-OF-DAYS = #INPUT-DATE-D - #LINE.#FIRST-DATE
                        //*  FOR INCOMPLETE BATCH - INITIAL ELAPSED DAY
                        if (condition(pnd_Line_Pnd_Nbr_Of_Days.equals(getZero())))                                                                                        //Natural: IF #NBR-OF-DAYS = 0
                        {
                            pnd_Line_Pnd_Nbr_Of_Days.setValue(1);                                                                                                         //Natural: ASSIGN #NBR-OF-DAYS := 1
                            //*  STARTS FROM THE UPLOAD DATE.
                            //*   NOTHING WILL HAPPEN IF NEGATIVE
                        }                                                                                                                                                 //Natural: END-IF
                        FOR01:                                                                                                                                            //Natural: FOR #I = 1 TO #NBR-OF-DAYS
                        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Line_Pnd_Nbr_Of_Days)); pnd_I.nadd(1))
                        {
                            if (condition(pnd_I.greater(17)))                                                                                                             //Natural: IF #I GT 17
                            {
                                if (condition(true)) break;                                                                                                               //Natural: ESCAPE BOTTOM
                            }                                                                                                                                             //Natural: END-IF
                            pnd_Line_Pnd_Days_In_Process.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Line_Pnd_Days_In_Process, "*"));                    //Natural: COMPRESS #DAYS-IN-PROCESS '*' INTO #DAYS-IN-PROCESS LEAVING NO
                        }                                                                                                                                                 //Natural: END-FOR
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Efsf0061.class));                                                                  //Natural: WRITE ( AUDIT ) NOTITLE USING FORM 'EFSF0061'
                    pnd_Nothing_To_Rpt.setValue(false);                                                                                                                   //Natural: ASSIGN #NOTHING-TO-RPT := FALSE
                }                                                                                                                                                         //Natural: END-SORT
                if (condition(getSort().getAstCOUNTER().greater(0)))
                {
                    atBreakEventSort01(endOfDataSort01);
                }
                endSort();
                if (condition(pnd_Nothing_To_Rpt.getBoolean()))                                                                                                           //Natural: IF #NOTHING-TO-RPT
                {
                    getReports().write(2, ReportOption.NOTITLE,"Nothing to report.");                                                                                     //Natural: WRITE ( AUDIT ) NOTITLE 'Nothing to report.'
                    if (Global.isEscape()) return;
                    //*   IF LESS THAN 23 LINES LEFT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().newPage(new ReportSpecification(0));                                                                                                     //Natural: NEWPAGE ( AUDIT )
                    if (condition(Global.isEscape())){return;}
                    getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Efsf0062.class));                                                                  //Natural: WRITE ( AUDIT ) NOTITLE USING FORM 'EFSF0062'
                }                                                                                                                                                         //Natural: END-IF
                //*  BEING SUBMITTED VIA CTP8000
                //*   SET LANDSCAPE
                getReports().newPage(new ReportSpecification(0));                                                                                                         //Natural: NEWPAGE ( AUDIT )
                if (condition(Global.isEscape())){return;}
                //* ***********************************************************************
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( AUDIT )
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Determine_Outstanding_Mins_If_Any() throws Exception                                                                                                 //Natural: DETERMINE-OUTSTANDING-MINS-IF-ANY
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  INITIALIZE TO INCOMPLETE
        //*  MINS UPLOADED TO DATE FOR THE BATCH
        pnd_Batch_Completed.reset();                                                                                                                                      //Natural: RESET #BATCH-COMPLETED #UPLD-TO-DATE
        pnd_Upld_To_Date.reset();
        if (condition(ldaEfsl0040.getCwf_Upload_Audit_Mins_Bch_Hdr_Cnt().equals(ldaEfsl0040.getCwf_Upload_Audit_Mins_Uploaded_Cnt())))                                    //Natural: IF CWF-UPLOAD-AUDIT.MINS-BCH-HDR-CNT = CWF-UPLOAD-AUDIT.MINS-UPLOADED-CNT
        {
            pnd_Line_Pnd_Mins_Uploaded.setValue(ldaEfsl0040.getCwf_Upload_Audit_Mins_Uploaded_Cnt());                                                                     //Natural: ASSIGN #LINE.#MINS-UPLOADED = CWF-UPLOAD-AUDIT.MINS-UPLOADED-CNT
            pnd_Batch_Completed.setValue(true);                                                                                                                           //Natural: ASSIGN #BATCH-COMPLETED = TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Source_Id_Batch_Nbr_Key_Pnd_Srce_Id.setValue(ldaEfsl0040.getCwf_Upload_Audit_Source_Id());                                                                    //Natural: ASSIGN #SOURCE-ID-BATCH-NBR-KEY.#SRCE-ID = CWF-UPLOAD-AUDIT.SOURCE-ID
        pnd_Source_Id_Batch_Nbr_Key_Pnd_Batch_Nbr.setValue(ldaEfsl0040.getCwf_Upload_Audit_Batch_Nbr());                                                                  //Natural: ASSIGN #SOURCE-ID-BATCH-NBR-KEY.#BATCH-NBR = CWF-UPLOAD-AUDIT.BATCH-NBR
        vw_upload_Batch.startDatabaseRead                                                                                                                                 //Natural: READ UPLOAD-BATCH BY SOURCE-ID-BATCH-NBR-KEY STARTING FROM #SOURCE-ID-BATCH-NBR-KEY
        (
        "R1",
        new Wc[] { new Wc("SOURCE_ID_BATCH_NBR_KEY", ">=", pnd_Source_Id_Batch_Nbr_Key, WcType.BY) },
        new Oc[] { new Oc("SOURCE_ID_BATCH_NBR_KEY", "ASC") }
        );
        R1:
        while (condition(vw_upload_Batch.readNextRow("R1")))
        {
            if (condition(upload_Batch_Source_Id.notEquals(ldaEfsl0040.getCwf_Upload_Audit_Source_Id()) || upload_Batch_Batch_Nbr.notEquals(ldaEfsl0040.getCwf_Upload_Audit_Batch_Nbr()))) //Natural: IF UPLOAD-BATCH.SOURCE-ID NE CWF-UPLOAD-AUDIT.SOURCE-ID OR UPLOAD-BATCH.BATCH-NBR NE CWF-UPLOAD-AUDIT.BATCH-NBR
            {
                if (true) break R1;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R1. )
            }                                                                                                                                                             //Natural: END-IF
            //*  REJECT IF UPLOAD-BATCH.EXCEPTION-MSG NE ' '  /* IGNORE ERROR LOGS
            //*  IGNORE
            if (condition(upload_Batch_Mins_Uploaded_Cnt.equals(getZero()) && upload_Batch_Error_Msg.getValue(1).notEquals(" ")))                                         //Natural: REJECT IF UPLOAD-BATCH.MINS-UPLOADED-CNT = 0 AND UPLOAD-BATCH.ERROR-MSG ( 1 ) NE ' '
            {
                continue;
            }
            //*  DETERMINE THE EFFECTIVE DAY OF WORK
            pnd_Upld_Date.setValue(upload_Batch_Upld_Date_Time);                                                                                                          //Natural: ASSIGN #UPLD-DATE = UPLOAD-BATCH.UPLD-DATE-TIME
            pnd_Upld_Time.setValueEdited(upload_Batch_Upld_Date_Time,new ReportEditMask("HHII"));                                                                         //Natural: MOVE EDITED UPLOAD-BATCH.UPLD-DATE-TIME ( EM = HHII ) TO #UPLD-TIME
            //*  IT IS PREVIOUS DAY'S WORK
            if (condition(pnd_Upld_Time_Pnd_Upld_Timen.less(pnd_Input_Data_Pnd_Input_Time_N)))                                                                            //Natural: IF #UPLD-TIMEN < #INPUT-TIME-N
            {
                pnd_Upld_Date.nsubtract(1);                                                                                                                               //Natural: SUBTRACT 1 FROM #UPLD-DATE
            }                                                                                                                                                             //Natural: END-IF
            //*  REJECT IF IT IS FUTURE DAY's work
            if (condition(pnd_Upld_Date.greater(pnd_Input_Date_D)))                                                                                                       //Natural: REJECT IF #UPLD-DATE > #INPUT-DATE-D
            {
                continue;
            }
            //*  ACCUMULATE TOTAL UPLOAD UNTIL PARAMTER DATE TO DETERMINE IF THE
            //*  IS OUTSTANDING AS OF THE PARAMETER DATE
            pnd_Upld_To_Date.nadd(upload_Batch_Mins_Uploaded_Cnt);                                                                                                        //Natural: ADD UPLOAD-BATCH.MINS-UPLOADED-CNT TO #UPLD-TO-DATE
            //*  ACCEPT ONLY UPLOADS UNTIL THE LINE BEING PRINTED
            if (condition(upload_Batch_Upld_Date_Time.greater(ldaEfsl0040.getCwf_Upload_Audit_Upld_Date_Time())))                                                         //Natural: REJECT IF UPLOAD-BATCH.UPLD-DATE-TIME > CWF-UPLOAD-AUDIT.UPLD-DATE-TIME
            {
                continue;
            }
            if (condition(pnd_Line_Pnd_First_Date.equals(getZero())))                                                                                                     //Natural: IF #LINE.#FIRST-DATE = 0
            {
                pnd_Line_Pnd_First_Date.setValue(pnd_Upld_Date);                                                                                                          //Natural: ASSIGN #LINE.#FIRST-DATE = #UPLD-DATE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Line_Pnd_Mins_Uploaded.nadd(upload_Batch_Mins_Uploaded_Cnt);                                                                                              //Natural: ADD UPLOAD-BATCH.MINS-UPLOADED-CNT TO #LINE.#MINS-UPLOADED
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(ldaEfsl0040.getCwf_Upload_Audit_Mins_Bch_Hdr_Cnt().equals(pnd_Upld_To_Date)))                                                                       //Natural: IF CWF-UPLOAD-AUDIT.MINS-BCH-HDR-CNT = #UPLD-TO-DATE
        {
            pnd_Batch_Completed.setValue(true);                                                                                                                           //Natural: ASSIGN #BATCH-COMPLETED = TRUE
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    if (condition(! (DbsUtil.maskMatches(Global.getINIT_PROGRAM(),".....'CWD'"))))                                                                        //Natural: IF *INIT-PROGRAM NE MASK ( .....'CWD' )
                    {
                        getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,pnd_Record);                                                                        //Natural: WRITE ( AUDIT ) NOTITLE NOHDR #RECORD
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(2, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Efsf0060.class));                                              //Natural: WRITE ( AUDIT ) NOTITLE NOHDR USING FORM 'EFSF0060'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean ldaEfsl0040_getCwf_Upload_Audit_Batch_NbrIsBreak = ldaEfsl0040.getCwf_Upload_Audit_Batch_Nbr().isBreak(endOfData);
        boolean ldaEfsl0040_getCwf_Upload_Audit_Source_IdIsBreak = ldaEfsl0040.getCwf_Upload_Audit_Source_Id().isBreak(endOfData);
        if (condition(ldaEfsl0040_getCwf_Upload_Audit_Batch_NbrIsBreak || ldaEfsl0040_getCwf_Upload_Audit_Source_IdIsBreak))
        {
            getReports().skip(0, 1);                                                                                                                                      //Natural: SKIP ( AUDIT ) 1 LINES
            if (condition(pnd_Batch_Completed.getBoolean()))                                                                                                              //Natural: IF #BATCH-COMPLETED
            {
                pnd_Total_Pnd_Batch_Finished.nadd(1);                                                                                                                     //Natural: ADD 1 TO #TOTAL.#BATCH-FINISHED
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Total_Pnd_Batch_Unfinished.nadd(1);                                                                                                                   //Natural: ADD 1 TO #TOTAL.#BATCH-UNFINISHED
                pnd_Total_Pnd_Mins_Not_Uploaded.nadd(pnd_Line_Pnd_Mins_Not_Uploaded);                                                                                     //Natural: ADD #LINE.#MINS-NOT-UPLOADED TO #TOTAL.#MINS-NOT-UPLOADED
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaEfsl0040_getCwf_Upload_Audit_Source_IdIsBreak))
        {
            getReports().newPage(new ReportSpecification(0));                                                                                                             //Natural: NEWPAGE ( AUDIT )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(2, "LS=133 PS=60 ZP=OFF");
    }
}
