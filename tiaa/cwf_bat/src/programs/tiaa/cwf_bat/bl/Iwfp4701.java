/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:52:27 PM
**        * FROM NATURAL PROGRAM : Iwfp4701
************************************************************
**        * FILE NAME            : Iwfp4701.java
**        * CLASS NAME           : Iwfp4701
**        * INSTANCE NAME        : Iwfp4701
************************************************************
************************************************************************
* PROGRAM   : IWFP4701
* SYSTEM    : CORPORATE WORK FLOW - ENHANCED WORK LISTS
* TITLE     : CHANGE SUMMARY REPORTS
* GENERATED : 04/02/1999
* FUNCTION  : DISPLAYS PARTICIPANT, NON-PARTICIPANT,
* AND INSTITUTION RECORD CHANGES BY VOLUME.
* HISTORY :  ZIVY JOHNSON
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iwfp4701 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Initial_Load;
    private DbsField pnd_Start_Time;
    private DbsField pnd_End_Time;
    private DbsField pnd_Icw_U_Total;
    private DbsField pnd_Cwf_U_Total;
    private DbsField pnd_Ncw_U_Total;
    private DbsField pnd_Current_Rec_Date;
    private DbsField pnd_Total_Processed;
    private DbsField pnd_Current_Rec_Date_A;
    private DbsField pnd_Font;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Initial_Load = localVariables.newFieldInRecord("pnd_Initial_Load", "#INITIAL-LOAD", FieldType.STRING, 1);
        pnd_Start_Time = localVariables.newFieldInRecord("pnd_Start_Time", "#START-TIME", FieldType.NUMERIC, 7);
        pnd_End_Time = localVariables.newFieldInRecord("pnd_End_Time", "#END-TIME", FieldType.NUMERIC, 7);
        pnd_Icw_U_Total = localVariables.newFieldInRecord("pnd_Icw_U_Total", "#ICW-U-TOTAL", FieldType.NUMERIC, 9);
        pnd_Cwf_U_Total = localVariables.newFieldInRecord("pnd_Cwf_U_Total", "#CWF-U-TOTAL", FieldType.NUMERIC, 9);
        pnd_Ncw_U_Total = localVariables.newFieldInRecord("pnd_Ncw_U_Total", "#NCW-U-TOTAL", FieldType.NUMERIC, 9);
        pnd_Current_Rec_Date = localVariables.newFieldInRecord("pnd_Current_Rec_Date", "#CURRENT-REC-DATE", FieldType.NUMERIC, 8);
        pnd_Total_Processed = localVariables.newFieldInRecord("pnd_Total_Processed", "#TOTAL-PROCESSED", FieldType.NUMERIC, 9);
        pnd_Current_Rec_Date_A = localVariables.newFieldInRecord("pnd_Current_Rec_Date_A", "#CURRENT-REC-DATE-A", FieldType.NUMERIC, 8);
        pnd_Font = localVariables.newFieldInRecord("pnd_Font", "#FONT", FieldType.STRING, 54);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Font.setInitialValue("-�E�&l1o2a5.6c72p3e66F�&a2L�(9905X");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iwfp4701() throws Exception
    {
        super("Iwfp4701");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atEndOfPage(atEndEventRpt2, 2);
        setupReports();
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 01 #CURRENT-REC-DATE #TOTAL-PROCESSED #INITIAL-LOAD #START-TIME #END-TIME #ICW-U-TOTAL #CWF-U-TOTAL #NCW-U-TOTAL
        while (condition(getWorkFiles().read(1, pnd_Current_Rec_Date, pnd_Total_Processed, pnd_Initial_Load, pnd_Start_Time, pnd_End_Time, pnd_Icw_U_Total, 
            pnd_Cwf_U_Total, pnd_Ncw_U_Total)))
        {
            if (condition(pnd_Current_Rec_Date.equals(pnd_Current_Rec_Date_A)))                                                                                           //Natural: FORMAT ( 02 ) LS = 133 PS = 60 ZP = OFF;//Natural: IF #CURRENT-REC-DATE = #CURRENT-REC-DATE-A
            {
                                                                                                                                                                          //Natural: PERFORM WRITE2
                sub_Write2();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Current_Rec_Date.greater(pnd_Current_Rec_Date_A)))                                                                                          //Natural: IF #CURRENT-REC-DATE > #CURRENT-REC-DATE-A
            {
                pnd_Current_Rec_Date_A.setValue(pnd_Current_Rec_Date);                                                                                                    //Natural: ASSIGN #CURRENT-REC-DATE-A := #CURRENT-REC-DATE
                getReports().newPage(new ReportSpecification(2));                                                                                                         //Natural: NEWPAGE ( 02 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM WRITE1
                sub_Write1();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM WRITE2
                sub_Write2();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: AT END OF PAGE ( 02 );//Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
    }
    private void sub_Write1() throws Exception                                                                                                                            //Natural: WRITE1
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(2, ReportOption.NOTITLE,pnd_Font);                                                                                                             //Natural: WRITE ( 02 ) NOTITLE #FONT
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(2),"PROGRAM NAME:",Global.getPROGRAM(),new TabSetting(114),"DATE :",Global.getDATN(),           //Natural: WRITE ( 02 ) / 2T 'PROGRAM NAME:' *PROGRAM 114T 'DATE :' *DATN ( EM = 9999'/'99'/'99 ) /#CURRENT-REC-DATE 'ENHANCED WORK LIST - DAILY RUNS BY VOLUME' 114T 'PAGE :' *PAGE-NUMBER ( 2 ) / 2T 'TOTAL PROCESSED' 5X'TYPE' 2X'START TIME' 2X'END TIME' 2X'TOTAL ICW' 2X'TOTAL CWF' 2X'TOTAL NCW'
            new ReportEditMask ("9999'/'99'/'99"),NEWLINE,pnd_Current_Rec_Date,"ENHANCED WORK LIST - DAILY RUNS BY VOLUME",new TabSetting(114),"PAGE :",getReports().getPageNumberDbs(2),NEWLINE,new 
            TabSetting(2),"TOTAL PROCESSED",new ColumnSpacing(5),"TYPE",new ColumnSpacing(2),"START TIME",new ColumnSpacing(2),"END TIME",new ColumnSpacing(2),"TOTAL ICW",new 
            ColumnSpacing(2),"TOTAL CWF",new ColumnSpacing(2),"TOTAL NCW");
        if (Global.isEscape()) return;
    }
    private void sub_Write2() throws Exception                                                                                                                            //Natural: WRITE2
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new ColumnSpacing(6),pnd_Total_Processed,new ColumnSpacing(8),pnd_Initial_Load,new ColumnSpacing(4),pnd_Start_Time,new  //Natural: WRITE ( 02 ) /6X #TOTAL-PROCESSED 8X #INITIAL-LOAD 4X#START-TIME 2X#END-TIME #ICW-U-TOTAL #CWF-U-TOTAL #NCW-U-TOTAL
            ColumnSpacing(2),pnd_End_Time,pnd_Icw_U_Total,pnd_Cwf_U_Total,pnd_Ncw_U_Total);
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atEndEventRpt2 = (Object sender, EventArgs e) ->
    {
        String localMethod = "IWFP4701|atEndEventRpt2";
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(2),"_",new RepeatItem(128),NEWLINE,new TabSetting(2),"_",new RepeatItem(128));      //Natural: WRITE ( 02 ) /2T '_' ( 128 ) /2T '_' ( 128 )
                }                                                                                                                                                         //Natural: END-ENDPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(2, "LS=133 PS=60 ZP=OFF");
    }
}
