/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:46:27 PM
**        * FROM NATURAL PROGRAM : Cwfb8637
************************************************************
**        * FILE NAME            : Cwfb8637.java
**        * CLASS NAME           : Cwfb8637
**        * INSTANCE NAME        : Cwfb8637
************************************************************
************************************************************************
* SYSTEM   : CORPORATE WORKFLOW
* PROGRAM  : CWFB8637
* TITLE    : CWF REPORTING - BENEFICIARY CHANGE PRODUCTIVITY REPORT
* FUNCTION : READ CWF-MASTER-INDEX-VIEW (045/181); EXTRACT OPERATOR
*          : INFORMATION FOR A MONTH; CREATE A WORKFILE.
* HISTORY  : CLONED FROM CWFB8629, MAY 2013 - VINODH KUMAR RAMACHANDRAN
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb8637 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_mit;
    private DbsField mit_Pin_Nbr;
    private DbsField mit_Rqst_Log_Dte_Tme;

    private DbsGroup mit__R_Field_1;
    private DbsField mit_Rqst_Log_Dte;
    private DbsField mit_Rqst_Log_Tme;
    private DbsField mit_Rqst_Log_Invrt_Dte_Tme;
    private DbsField mit_Work_Prcss_Id;
    private DbsField mit_Last_Chnge_Dte_Tme;
    private DbsField mit_Tiaa_Rcvd_Dte_Tme;
    private DbsField mit_Crprte_Due_Dte_Tme;
    private DbsField mit_Unit_Clock_Start_Dte_Tme;
    private DbsField mit_Unit_Clock_End_Dte_Tme;
    private DbsField mit_Empl_Clock_Start_Dte_Tme;
    private DbsField mit_Empl_Clock_End_Dte_Tme;
    private DbsField mit_Step_Clock_Start_Dte_Tme;
    private DbsField mit_Step_Clock_End_Dte_Tme;
    private DbsField mit_Empl_Elpsd_Clndr_Days_Tme;
    private DbsField mit_Empl_Elpsd_Bsnss_Days_Tme;
    private DbsField mit_Unit_Elpsd_Clndr_Days_Tme;
    private DbsField mit_Unit_Elpsd_Bsnss_Days_Tme;
    private DbsField mit_Assgn_Dte_Tme;
    private DbsField mit_Status_Cde;

    private DataAccessProgramView vw_mit_Hist;
    private DbsField mit_Hist_Pin_Nbr;
    private DbsField mit_Hist_Empl_Oprtr_Cde;
    private DbsField mit_Hist_Rqst_Log_Dte_Tme;

    private DbsGroup mit_Hist__R_Field_2;
    private DbsField mit_Hist_Rqst_Log_Dte;
    private DbsField mit_Hist_Rqst_Log_Tme;
    private DbsField mit_Hist_Rqst_Log_Invrt_Dte_Tme;
    private DbsField mit_Hist_Work_Prcss_Id;
    private DbsField mit_Hist_Orgnl_Unit_Cde;
    private DbsField mit_Hist_Unit_Cde;
    private DbsField mit_Hist_Status_Cde;
    private DbsField mit_Hist_Last_Chnge_Dte_Tme;

    private DbsGroup mit_Hist__R_Field_3;
    private DbsField mit_Hist_Pnd_Last_Chnge_Dte;
    private DbsField mit_Hist_Last_Chnge_Oprtr_Cde;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_4;
    private DbsField cwf_Support_Tbl_Tbl_Unit_Key;

    private DbsGroup cwf_Support_Tbl__R_Field_5;
    private DbsField cwf_Support_Tbl_Pnd_Tbl_User_Key;

    private DbsGroup cwf_Support_Tbl__R_Field_6;
    private DbsField cwf_Support_Tbl_Tbl_Wpid_Key;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_7;
    private DbsField cwf_Support_Tbl_Tbl_Unit;

    private DbsGroup cwf_Support_Tbl__R_Field_8;
    private DbsField cwf_Support_Tbl_Tbl_User;

    private DbsGroup cwf_Support_Tbl__R_Field_9;
    private DbsField cwf_Support_Tbl_Tbl_Wpid;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_10;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field;
    private DbsField pnd_I_Unit;
    private DbsField pnd_I_User;
    private DbsField pnd_I_Wpid;

    private DbsGroup pnd_Units;
    private DbsField pnd_Units_Pnd_T_Unit;

    private DbsGroup pnd_Operators;
    private DbsField pnd_Operators_Pnd_T_Oprtr;

    private DbsGroup pnd_Wpids;
    private DbsField pnd_Wpids_Pnd_T_Wpid;
    private DbsField pnd_Status_Date_Key;

    private DbsGroup pnd_Status_Date_Key__R_Field_11;
    private DbsField pnd_Status_Date_Key_Pnd_K_Status;
    private DbsField pnd_Status_Date_Key_Pnd_K_Invert_Upd_Dte;

    private DbsGroup pnd_Status_Date_Key__R_Field_12;
    private DbsField pnd_Status_Date_Key_Pnd_K_Invert_Yymmdd;
    private DbsField pnd_Status_Date_Key_Pnd_K_Invert_Hhiisst;
    private DbsField pnd_Status_Date_Key__Filler1;
    private DbsField pnd_History_Key;

    private DbsGroup pnd_History_Key__R_Field_13;
    private DbsField pnd_History_Key_Pnd_K_Log_Dte_Tme;
    private DbsField pnd_History_Key_Pnd_K_Last_Chnge_Invrt_D_T;
    private DbsField pnd_Frequency;
    private DbsField pnd_Work_File;

    private DbsGroup pnd_Work_File__R_Field_14;
    private DbsField pnd_Work_File_Pnd_Wf_Oprtr;
    private DbsField pnd_Work_File_Pnd_Wf_Wpid;
    private DbsField pnd_Work_File_Pnd_Wf_Pin;
    private DbsField pnd_Work_File_Pnd_Wf_Recvd_Dte_Tme;

    private DbsGroup pnd_Work_File__R_Field_15;
    private DbsField pnd_Work_File_Pnd_Wf_Recvd_Dte;
    private DbsField pnd_Work_File_Pnd_Wf_Recvd_Tme;
    private DbsField pnd_Work_File__Filler2;
    private DbsField pnd_Work_File_Pnd_Wf_Due_Dte_Tme;

    private DbsGroup pnd_Work_File__R_Field_16;
    private DbsField pnd_Work_File_Pnd_Wf_Due_Dte;
    private DbsField pnd_Work_File_Pnd_Wf_Due_Tme;
    private DbsField pnd_Work_File_Pnd_Wf_Compltd_Dte_Tme;

    private DbsGroup pnd_Work_File__R_Field_17;
    private DbsField pnd_Work_File_Pnd_Wf_Compltd_Dte;
    private DbsField pnd_Work_File_Pnd_Wf_Compltd_Tme;
    private DbsField pnd_Work_File_Pnd_Wf_Last_Chnge_Oprtr;
    private DbsField pnd_Work_File_Pnd_Wf_Unit_Open_Stts;
    private DbsField pnd_Work_File_Pnd_Wf_Unit_Open_Dte_Tme;

    private DbsGroup pnd_Work_File__R_Field_18;
    private DbsField pnd_Work_File_Pnd_Wf_Unit_Open_Dte;
    private DbsField pnd_Work_File_Pnd_Wf_Unit_Open_Tme;
    private DbsField pnd_Work_File_Pnd_Wf_Unit_Clsd_Stts;
    private DbsField pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte_Tme;

    private DbsGroup pnd_Work_File__R_Field_19;
    private DbsField pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte;
    private DbsField pnd_Work_File_Pnd_Wf_Unit_Clsd_Tme;
    private DbsField pnd_Work_File_Pnd_Wf_Status_Cde;

    private DbsGroup pnd_Work_File__R_Field_20;
    private DbsField pnd_Work_File__Filler3;
    private DbsField pnd_Work_File_Pnd_Wf_Run;
    private DbsField pnd_Work_File_Pnd_Wf_Start_Dte;
    private DbsField pnd_Work_File_Pnd_Wf_End_Dte;
    private DbsField pnd_Work_File_Pnd_Wf_Report_Name;
    private DbsField pnd_Start_Datd;
    private DbsField pnd_Start_Yyyymmdd;

    private DbsGroup pnd_Start_Yyyymmdd__R_Field_21;
    private DbsField pnd_Start_Yyyymmdd_Pnd_Start_Date;

    private DbsGroup pnd_Start_Yyyymmdd__R_Field_22;
    private DbsField pnd_Start_Yyyymmdd_Pnd_Start_Yyyymm;
    private DbsField pnd_Start_Yyyymmdd_Pnd_Start_Dd;
    private DbsField pnd_End_Datd;
    private DbsField pnd_End_Yyyymmdd;

    private DbsGroup pnd_End_Yyyymmdd__R_Field_23;
    private DbsField pnd_End_Yyyymmdd_Pnd_End_Date;

    private DbsGroup pnd_End_Yyyymmdd__R_Field_24;
    private DbsField pnd_End_Yyyymmdd_Pnd_End_Yyyymm;
    private DbsField pnd_End_Yyyymmdd_Pnd_End_Dd;
    private DbsField pnd_Last_Chnge_Dte_Tme;

    private DbsGroup pnd_Last_Chnge_Dte_Tme__R_Field_25;
    private DbsField pnd_Last_Chnge_Dte_Tme_Pnd_Last_Chnge_Yyyymmdd;
    private DbsField pnd_Last_Chnge_Dte_D;
    private DbsField pnd_Log_Dte_Tme;
    private DbsField pnd_Write_Count;
    private DbsField pnd_Write_Count2;
    private DbsField pnd_Closed_Status_Codes;
    private DbsField pnd_Arr_Rec_Cnt;

    private DbsGroup pnd_Array_Group;
    private DbsField pnd_Array_Group_Pnd_Arr_Pin_Nbr;
    private DbsField pnd_Array_Group_Pnd_Arr_Wpid;
    private DbsField pnd_Array_Group_Pnd_Arr_Status_Cde;
    private DbsField pnd_Array_Group_Pnd_Arr_Empl_Oprtr;
    private DbsField pnd_Array_Group_Pnd_Arr_Last_Chg_Dt;
    private DbsField pnd_Array_Group_Pnd_Arr_Last_Chg_Op;
    private DbsField pnd_Prev_Status_Code;
    private DbsField pnd_Qc_Cnt;
    private DbsField pnd_I;
    private DbsField pnd_J;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_mit = new DataAccessProgramView(new NameInfo("vw_mit", "MIT"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        mit_Pin_Nbr = vw_mit.getRecord().newFieldInGroup("mit_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "PIN_NBR");
        mit_Pin_Nbr.setDdmHeader("PIN");
        mit_Rqst_Log_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "RQST_LOG_DTE_TME");
        mit_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");

        mit__R_Field_1 = vw_mit.getRecord().newGroupInGroup("mit__R_Field_1", "REDEFINE", mit_Rqst_Log_Dte_Tme);
        mit_Rqst_Log_Dte = mit__R_Field_1.newFieldInGroup("mit_Rqst_Log_Dte", "RQST-LOG-DTE", FieldType.STRING, 8);
        mit_Rqst_Log_Tme = mit__R_Field_1.newFieldInGroup("mit_Rqst_Log_Tme", "RQST-LOG-TME", FieldType.STRING, 7);
        mit_Rqst_Log_Invrt_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Rqst_Log_Invrt_Dte_Tme", "RQST-LOG-INVRT-DTE-TME", FieldType.NUMERIC, 15, 
            RepeatingFieldStrategy.None, "RQST_LOG_INVRT_DTE_TME");
        mit_Work_Prcss_Id = vw_mit.getRecord().newFieldInGroup("mit_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        mit_Work_Prcss_Id.setDdmHeader("WORK/ID");
        mit_Last_Chnge_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, 
            "LAST_CHNGE_DTE_TME");
        mit_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");
        mit_Tiaa_Rcvd_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Tiaa_Rcvd_Dte_Tme", "TIAA-RCVD-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TIAA_RCVD_DTE_TME");
        mit_Tiaa_Rcvd_Dte_Tme.setDdmHeader("TIAA-RCV/DTE-TME");
        mit_Crprte_Due_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Crprte_Due_Dte_Tme", "CRPRTE-DUE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "CRPRTE_DUE_DTE_TME");
        mit_Crprte_Due_Dte_Tme.setDdmHeader("WORKRQST/DUE DATE");
        mit_Unit_Clock_Start_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Unit_Clock_Start_Dte_Tme", "UNIT-CLOCK-START-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "UNIT_CLOCK_START_DTE_TME");
        mit_Unit_Clock_Start_Dte_Tme.setDdmHeader("UNIT CLOCK START DATE TIME");
        mit_Unit_Clock_End_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Unit_Clock_End_Dte_Tme", "UNIT-CLOCK-END-DTE-TME", FieldType.STRING, 15, 
            RepeatingFieldStrategy.None, "UNIT_CLOCK_END_DTE_TME");
        mit_Unit_Clock_End_Dte_Tme.setDdmHeader("UNIT CLOCK END DATE TIME");
        mit_Empl_Clock_Start_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Empl_Clock_Start_Dte_Tme", "EMPL-CLOCK-START-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "EMPL_CLOCK_START_DTE_TME");
        mit_Empl_Clock_Start_Dte_Tme.setDdmHeader("EMPLOYEE CLOCK START DATE TIME");
        mit_Empl_Clock_End_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Empl_Clock_End_Dte_Tme", "EMPL-CLOCK-END-DTE-TME", FieldType.STRING, 15, 
            RepeatingFieldStrategy.None, "EMPL_CLOCK_END_DTE_TME");
        mit_Empl_Clock_End_Dte_Tme.setDdmHeader("EMPLOYEE CLOCK END DATE TIME");
        mit_Step_Clock_Start_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Step_Clock_Start_Dte_Tme", "STEP-CLOCK-START-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "STEP_CLOCK_START_DTE_TME");
        mit_Step_Clock_Start_Dte_Tme.setDdmHeader("STEP START DATE TIME");
        mit_Step_Clock_End_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Step_Clock_End_Dte_Tme", "STEP-CLOCK-END-DTE-TME", FieldType.STRING, 15, 
            RepeatingFieldStrategy.None, "STEP_CLOCK_END_DTE_TME");
        mit_Step_Clock_End_Dte_Tme.setDdmHeader("STEP CLOCK END DATE TIME");
        mit_Empl_Elpsd_Clndr_Days_Tme = vw_mit.getRecord().newFieldInGroup("mit_Empl_Elpsd_Clndr_Days_Tme", "EMPL-ELPSD-CLNDR-DAYS-TME", FieldType.STRING, 
            7, RepeatingFieldStrategy.None, "EMPL_ELPSD_CLNDR_DAYS_TME");
        mit_Empl_Elpsd_Clndr_Days_Tme.setDdmHeader("ASSOCIATE WAIT TIME");
        mit_Empl_Elpsd_Bsnss_Days_Tme = vw_mit.getRecord().newFieldInGroup("mit_Empl_Elpsd_Bsnss_Days_Tme", "EMPL-ELPSD-BSNSS-DAYS-TME", FieldType.STRING, 
            7, RepeatingFieldStrategy.None, "EMPL_ELPSD_BSNSS_DAYS_TME");
        mit_Empl_Elpsd_Bsnss_Days_Tme.setDdmHeader("ASSOCIATE PROCESS TIME");
        mit_Unit_Elpsd_Clndr_Days_Tme = vw_mit.getRecord().newFieldInGroup("mit_Unit_Elpsd_Clndr_Days_Tme", "UNIT-ELPSD-CLNDR-DAYS-TME", FieldType.STRING, 
            7, RepeatingFieldStrategy.None, "UNIT_ELPSD_CLNDR_DAYS_TME");
        mit_Unit_Elpsd_Clndr_Days_Tme.setDdmHeader("UNIT WAIT TIME");
        mit_Unit_Elpsd_Bsnss_Days_Tme = vw_mit.getRecord().newFieldInGroup("mit_Unit_Elpsd_Bsnss_Days_Tme", "UNIT-ELPSD-BSNSS-DAYS-TME", FieldType.STRING, 
            7, RepeatingFieldStrategy.None, "UNIT_ELPSD_BSNSS_DAYS_TME");
        mit_Unit_Elpsd_Bsnss_Days_Tme.setDdmHeader("UNIT PROCESS TIME");
        mit_Assgn_Dte_Tme = vw_mit.getRecord().newFieldInGroup("mit_Assgn_Dte_Tme", "ASSGN-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, "ASSGN_DTE_TME");
        mit_Assgn_Dte_Tme.setDdmHeader("ASSIGNMENT/DATE-TIME");
        mit_Status_Cde = vw_mit.getRecord().newFieldInGroup("mit_Status_Cde", "STATUS-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, "STATUS_CDE");
        registerRecord(vw_mit);

        vw_mit_Hist = new DataAccessProgramView(new NameInfo("vw_mit_Hist", "MIT-HIST"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        mit_Hist_Pin_Nbr = vw_mit_Hist.getRecord().newFieldInGroup("mit_Hist_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "PIN_NBR");
        mit_Hist_Pin_Nbr.setDdmHeader("PIN");
        mit_Hist_Empl_Oprtr_Cde = vw_mit_Hist.getRecord().newFieldInGroup("mit_Hist_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "EMPL_OPRTR_CDE");
        mit_Hist_Empl_Oprtr_Cde.setDdmHeader("EMPLOYEE/ID");
        mit_Hist_Rqst_Log_Dte_Tme = vw_mit_Hist.getRecord().newFieldInGroup("mit_Hist_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "RQST_LOG_DTE_TME");
        mit_Hist_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");

        mit_Hist__R_Field_2 = vw_mit_Hist.getRecord().newGroupInGroup("mit_Hist__R_Field_2", "REDEFINE", mit_Hist_Rqst_Log_Dte_Tme);
        mit_Hist_Rqst_Log_Dte = mit_Hist__R_Field_2.newFieldInGroup("mit_Hist_Rqst_Log_Dte", "RQST-LOG-DTE", FieldType.STRING, 8);
        mit_Hist_Rqst_Log_Tme = mit_Hist__R_Field_2.newFieldInGroup("mit_Hist_Rqst_Log_Tme", "RQST-LOG-TME", FieldType.STRING, 7);
        mit_Hist_Rqst_Log_Invrt_Dte_Tme = vw_mit_Hist.getRecord().newFieldInGroup("mit_Hist_Rqst_Log_Invrt_Dte_Tme", "RQST-LOG-INVRT-DTE-TME", FieldType.NUMERIC, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_INVRT_DTE_TME");
        mit_Hist_Work_Prcss_Id = vw_mit_Hist.getRecord().newFieldInGroup("mit_Hist_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        mit_Hist_Work_Prcss_Id.setDdmHeader("WORK/ID");
        mit_Hist_Orgnl_Unit_Cde = vw_mit_Hist.getRecord().newFieldInGroup("mit_Hist_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ORGNL_UNIT_CDE");
        mit_Hist_Orgnl_Unit_Cde.setDdmHeader("LOG/UNIT");
        mit_Hist_Unit_Cde = vw_mit_Hist.getRecord().newFieldInGroup("mit_Hist_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "UNIT_CDE");
        mit_Hist_Unit_Cde.setDdmHeader("UNIT/CODE");
        mit_Hist_Status_Cde = vw_mit_Hist.getRecord().newFieldInGroup("mit_Hist_Status_Cde", "STATUS-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "STATUS_CDE");
        mit_Hist_Last_Chnge_Dte_Tme = vw_mit_Hist.getRecord().newFieldInGroup("mit_Hist_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 
            15, RepeatingFieldStrategy.None, "LAST_CHNGE_DTE_TME");
        mit_Hist_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");

        mit_Hist__R_Field_3 = vw_mit_Hist.getRecord().newGroupInGroup("mit_Hist__R_Field_3", "REDEFINE", mit_Hist_Last_Chnge_Dte_Tme);
        mit_Hist_Pnd_Last_Chnge_Dte = mit_Hist__R_Field_3.newFieldInGroup("mit_Hist_Pnd_Last_Chnge_Dte", "#LAST-CHNGE-DTE", FieldType.STRING, 8);
        mit_Hist_Last_Chnge_Oprtr_Cde = vw_mit_Hist.getRecord().newFieldInGroup("mit_Hist_Last_Chnge_Oprtr_Cde", "LAST-CHNGE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "LAST_CHNGE_OPRTR_CDE");
        mit_Hist_Last_Chnge_Oprtr_Cde.setDdmHeader("LOG CREATE/OPERATOR");
        registerRecord(vw_mit_Hist);

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");

        cwf_Support_Tbl__R_Field_4 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_4", "REDEFINE", cwf_Support_Tbl_Tbl_Key_Field);
        cwf_Support_Tbl_Tbl_Unit_Key = cwf_Support_Tbl__R_Field_4.newFieldInGroup("cwf_Support_Tbl_Tbl_Unit_Key", "TBL-UNIT-KEY", FieldType.STRING, 15);

        cwf_Support_Tbl__R_Field_5 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_5", "REDEFINE", cwf_Support_Tbl_Tbl_Key_Field);
        cwf_Support_Tbl_Pnd_Tbl_User_Key = cwf_Support_Tbl__R_Field_5.newFieldInGroup("cwf_Support_Tbl_Pnd_Tbl_User_Key", "#TBL-USER-KEY", FieldType.STRING, 
            15);

        cwf_Support_Tbl__R_Field_6 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_6", "REDEFINE", cwf_Support_Tbl_Tbl_Key_Field);
        cwf_Support_Tbl_Tbl_Wpid_Key = cwf_Support_Tbl__R_Field_6.newFieldInGroup("cwf_Support_Tbl_Tbl_Wpid_Key", "TBL-WPID-KEY", FieldType.STRING, 15);
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Support_Tbl__R_Field_7 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_7", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Tbl_Unit = cwf_Support_Tbl__R_Field_7.newFieldInGroup("cwf_Support_Tbl_Tbl_Unit", "TBL-UNIT", FieldType.STRING, 8);

        cwf_Support_Tbl__R_Field_8 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_8", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Tbl_User = cwf_Support_Tbl__R_Field_8.newFieldInGroup("cwf_Support_Tbl_Tbl_User", "TBL-USER", FieldType.STRING, 8);

        cwf_Support_Tbl__R_Field_9 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_9", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Tbl_Wpid = cwf_Support_Tbl__R_Field_9.newFieldInGroup("cwf_Support_Tbl_Tbl_Wpid", "TBL-WPID", FieldType.STRING, 6);
        registerRecord(vw_cwf_Support_Tbl);

        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_10 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_10", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_10.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_10.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_10.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);
        pnd_I_Unit = localVariables.newFieldInRecord("pnd_I_Unit", "#I-UNIT", FieldType.NUMERIC, 4);
        pnd_I_User = localVariables.newFieldInRecord("pnd_I_User", "#I-USER", FieldType.NUMERIC, 4);
        pnd_I_Wpid = localVariables.newFieldInRecord("pnd_I_Wpid", "#I-WPID", FieldType.NUMERIC, 4);

        pnd_Units = localVariables.newGroupInRecord("pnd_Units", "#UNITS");
        pnd_Units_Pnd_T_Unit = pnd_Units.newFieldArrayInGroup("pnd_Units_Pnd_T_Unit", "#T-UNIT", FieldType.STRING, 8, new DbsArrayController(1, 9999));

        pnd_Operators = localVariables.newGroupInRecord("pnd_Operators", "#OPERATORS");
        pnd_Operators_Pnd_T_Oprtr = pnd_Operators.newFieldArrayInGroup("pnd_Operators_Pnd_T_Oprtr", "#T-OPRTR", FieldType.STRING, 8, new DbsArrayController(1, 
            9999));

        pnd_Wpids = localVariables.newGroupInRecord("pnd_Wpids", "#WPIDS");
        pnd_Wpids_Pnd_T_Wpid = pnd_Wpids.newFieldArrayInGroup("pnd_Wpids_Pnd_T_Wpid", "#T-WPID", FieldType.STRING, 6, new DbsArrayController(1, 9999));
        pnd_Status_Date_Key = localVariables.newFieldInRecord("pnd_Status_Date_Key", "#STATUS-DATE-KEY", FieldType.STRING, 25);

        pnd_Status_Date_Key__R_Field_11 = localVariables.newGroupInRecord("pnd_Status_Date_Key__R_Field_11", "REDEFINE", pnd_Status_Date_Key);
        pnd_Status_Date_Key_Pnd_K_Status = pnd_Status_Date_Key__R_Field_11.newFieldInGroup("pnd_Status_Date_Key_Pnd_K_Status", "#K-STATUS", FieldType.STRING, 
            1);
        pnd_Status_Date_Key_Pnd_K_Invert_Upd_Dte = pnd_Status_Date_Key__R_Field_11.newFieldInGroup("pnd_Status_Date_Key_Pnd_K_Invert_Upd_Dte", "#K-INVERT-UPD-DTE", 
            FieldType.NUMERIC, 15);

        pnd_Status_Date_Key__R_Field_12 = pnd_Status_Date_Key__R_Field_11.newGroupInGroup("pnd_Status_Date_Key__R_Field_12", "REDEFINE", pnd_Status_Date_Key_Pnd_K_Invert_Upd_Dte);
        pnd_Status_Date_Key_Pnd_K_Invert_Yymmdd = pnd_Status_Date_Key__R_Field_12.newFieldInGroup("pnd_Status_Date_Key_Pnd_K_Invert_Yymmdd", "#K-INVERT-YYMMDD", 
            FieldType.NUMERIC, 8);
        pnd_Status_Date_Key_Pnd_K_Invert_Hhiisst = pnd_Status_Date_Key__R_Field_12.newFieldInGroup("pnd_Status_Date_Key_Pnd_K_Invert_Hhiisst", "#K-INVERT-HHIISST", 
            FieldType.NUMERIC, 7);
        pnd_Status_Date_Key__Filler1 = pnd_Status_Date_Key__R_Field_11.newFieldInGroup("pnd_Status_Date_Key__Filler1", "_FILLER1", FieldType.STRING, 9);
        pnd_History_Key = localVariables.newFieldInRecord("pnd_History_Key", "#HISTORY-KEY", FieldType.STRING, 30);

        pnd_History_Key__R_Field_13 = localVariables.newGroupInRecord("pnd_History_Key__R_Field_13", "REDEFINE", pnd_History_Key);
        pnd_History_Key_Pnd_K_Log_Dte_Tme = pnd_History_Key__R_Field_13.newFieldInGroup("pnd_History_Key_Pnd_K_Log_Dte_Tme", "#K-LOG-DTE-TME", FieldType.STRING, 
            15);
        pnd_History_Key_Pnd_K_Last_Chnge_Invrt_D_T = pnd_History_Key__R_Field_13.newFieldInGroup("pnd_History_Key_Pnd_K_Last_Chnge_Invrt_D_T", "#K-LAST-CHNGE-INVRT-D-T", 
            FieldType.NUMERIC, 15);
        pnd_Frequency = localVariables.newFieldInRecord("pnd_Frequency", "#FREQUENCY", FieldType.STRING, 7);
        pnd_Work_File = localVariables.newFieldInRecord("pnd_Work_File", "#WORK-FILE", FieldType.STRING, 250);

        pnd_Work_File__R_Field_14 = localVariables.newGroupInRecord("pnd_Work_File__R_Field_14", "REDEFINE", pnd_Work_File);
        pnd_Work_File_Pnd_Wf_Oprtr = pnd_Work_File__R_Field_14.newFieldInGroup("pnd_Work_File_Pnd_Wf_Oprtr", "#WF-OPRTR", FieldType.STRING, 8);
        pnd_Work_File_Pnd_Wf_Wpid = pnd_Work_File__R_Field_14.newFieldInGroup("pnd_Work_File_Pnd_Wf_Wpid", "#WF-WPID", FieldType.STRING, 6);
        pnd_Work_File_Pnd_Wf_Pin = pnd_Work_File__R_Field_14.newFieldInGroup("pnd_Work_File_Pnd_Wf_Pin", "#WF-PIN", FieldType.NUMERIC, 12);
        pnd_Work_File_Pnd_Wf_Recvd_Dte_Tme = pnd_Work_File__R_Field_14.newFieldInGroup("pnd_Work_File_Pnd_Wf_Recvd_Dte_Tme", "#WF-RECVD-DTE-TME", FieldType.STRING, 
            15);

        pnd_Work_File__R_Field_15 = pnd_Work_File__R_Field_14.newGroupInGroup("pnd_Work_File__R_Field_15", "REDEFINE", pnd_Work_File_Pnd_Wf_Recvd_Dte_Tme);
        pnd_Work_File_Pnd_Wf_Recvd_Dte = pnd_Work_File__R_Field_15.newFieldInGroup("pnd_Work_File_Pnd_Wf_Recvd_Dte", "#WF-RECVD-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_File_Pnd_Wf_Recvd_Tme = pnd_Work_File__R_Field_15.newFieldInGroup("pnd_Work_File_Pnd_Wf_Recvd_Tme", "#WF-RECVD-TME", FieldType.NUMERIC, 
            4);
        pnd_Work_File__Filler2 = pnd_Work_File__R_Field_15.newFieldInGroup("pnd_Work_File__Filler2", "_FILLER2", FieldType.STRING, 3);
        pnd_Work_File_Pnd_Wf_Due_Dte_Tme = pnd_Work_File__R_Field_14.newFieldInGroup("pnd_Work_File_Pnd_Wf_Due_Dte_Tme", "#WF-DUE-DTE-TME", FieldType.STRING, 
            15);

        pnd_Work_File__R_Field_16 = pnd_Work_File__R_Field_14.newGroupInGroup("pnd_Work_File__R_Field_16", "REDEFINE", pnd_Work_File_Pnd_Wf_Due_Dte_Tme);
        pnd_Work_File_Pnd_Wf_Due_Dte = pnd_Work_File__R_Field_16.newFieldInGroup("pnd_Work_File_Pnd_Wf_Due_Dte", "#WF-DUE-DTE", FieldType.NUMERIC, 8);
        pnd_Work_File_Pnd_Wf_Due_Tme = pnd_Work_File__R_Field_16.newFieldInGroup("pnd_Work_File_Pnd_Wf_Due_Tme", "#WF-DUE-TME", FieldType.NUMERIC, 7);
        pnd_Work_File_Pnd_Wf_Compltd_Dte_Tme = pnd_Work_File__R_Field_14.newFieldInGroup("pnd_Work_File_Pnd_Wf_Compltd_Dte_Tme", "#WF-COMPLTD-DTE-TME", 
            FieldType.STRING, 15);

        pnd_Work_File__R_Field_17 = pnd_Work_File__R_Field_14.newGroupInGroup("pnd_Work_File__R_Field_17", "REDEFINE", pnd_Work_File_Pnd_Wf_Compltd_Dte_Tme);
        pnd_Work_File_Pnd_Wf_Compltd_Dte = pnd_Work_File__R_Field_17.newFieldInGroup("pnd_Work_File_Pnd_Wf_Compltd_Dte", "#WF-COMPLTD-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_File_Pnd_Wf_Compltd_Tme = pnd_Work_File__R_Field_17.newFieldInGroup("pnd_Work_File_Pnd_Wf_Compltd_Tme", "#WF-COMPLTD-TME", FieldType.NUMERIC, 
            7);
        pnd_Work_File_Pnd_Wf_Last_Chnge_Oprtr = pnd_Work_File__R_Field_14.newFieldInGroup("pnd_Work_File_Pnd_Wf_Last_Chnge_Oprtr", "#WF-LAST-CHNGE-OPRTR", 
            FieldType.STRING, 8);
        pnd_Work_File_Pnd_Wf_Unit_Open_Stts = pnd_Work_File__R_Field_14.newFieldInGroup("pnd_Work_File_Pnd_Wf_Unit_Open_Stts", "#WF-UNIT-OPEN-STTS", FieldType.STRING, 
            4);
        pnd_Work_File_Pnd_Wf_Unit_Open_Dte_Tme = pnd_Work_File__R_Field_14.newFieldInGroup("pnd_Work_File_Pnd_Wf_Unit_Open_Dte_Tme", "#WF-UNIT-OPEN-DTE-TME", 
            FieldType.STRING, 15);

        pnd_Work_File__R_Field_18 = pnd_Work_File__R_Field_14.newGroupInGroup("pnd_Work_File__R_Field_18", "REDEFINE", pnd_Work_File_Pnd_Wf_Unit_Open_Dte_Tme);
        pnd_Work_File_Pnd_Wf_Unit_Open_Dte = pnd_Work_File__R_Field_18.newFieldInGroup("pnd_Work_File_Pnd_Wf_Unit_Open_Dte", "#WF-UNIT-OPEN-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_File_Pnd_Wf_Unit_Open_Tme = pnd_Work_File__R_Field_18.newFieldInGroup("pnd_Work_File_Pnd_Wf_Unit_Open_Tme", "#WF-UNIT-OPEN-TME", FieldType.NUMERIC, 
            7);
        pnd_Work_File_Pnd_Wf_Unit_Clsd_Stts = pnd_Work_File__R_Field_14.newFieldInGroup("pnd_Work_File_Pnd_Wf_Unit_Clsd_Stts", "#WF-UNIT-CLSD-STTS", FieldType.STRING, 
            4);
        pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte_Tme = pnd_Work_File__R_Field_14.newFieldInGroup("pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte_Tme", "#WF-UNIT-CLSD-DTE-TME", 
            FieldType.STRING, 15);

        pnd_Work_File__R_Field_19 = pnd_Work_File__R_Field_14.newGroupInGroup("pnd_Work_File__R_Field_19", "REDEFINE", pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte_Tme);
        pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte = pnd_Work_File__R_Field_19.newFieldInGroup("pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte", "#WF-UNIT-CLSD-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_File_Pnd_Wf_Unit_Clsd_Tme = pnd_Work_File__R_Field_19.newFieldInGroup("pnd_Work_File_Pnd_Wf_Unit_Clsd_Tme", "#WF-UNIT-CLSD-TME", FieldType.NUMERIC, 
            7);
        pnd_Work_File_Pnd_Wf_Status_Cde = pnd_Work_File__R_Field_14.newFieldInGroup("pnd_Work_File_Pnd_Wf_Status_Cde", "#WF-STATUS-CDE", FieldType.STRING, 
            4);

        pnd_Work_File__R_Field_20 = localVariables.newGroupInRecord("pnd_Work_File__R_Field_20", "REDEFINE", pnd_Work_File);
        pnd_Work_File__Filler3 = pnd_Work_File__R_Field_20.newFieldInGroup("pnd_Work_File__Filler3", "_FILLER3", FieldType.STRING, 8);
        pnd_Work_File_Pnd_Wf_Run = pnd_Work_File__R_Field_20.newFieldInGroup("pnd_Work_File_Pnd_Wf_Run", "#WF-RUN", FieldType.STRING, 8);
        pnd_Work_File_Pnd_Wf_Start_Dte = pnd_Work_File__R_Field_20.newFieldInGroup("pnd_Work_File_Pnd_Wf_Start_Dte", "#WF-START-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_File_Pnd_Wf_End_Dte = pnd_Work_File__R_Field_20.newFieldInGroup("pnd_Work_File_Pnd_Wf_End_Dte", "#WF-END-DTE", FieldType.NUMERIC, 8);
        pnd_Work_File_Pnd_Wf_Report_Name = pnd_Work_File__R_Field_20.newFieldInGroup("pnd_Work_File_Pnd_Wf_Report_Name", "#WF-REPORT-NAME", FieldType.STRING, 
            1);
        pnd_Start_Datd = localVariables.newFieldInRecord("pnd_Start_Datd", "#START-DATD", FieldType.DATE);
        pnd_Start_Yyyymmdd = localVariables.newFieldInRecord("pnd_Start_Yyyymmdd", "#START-YYYYMMDD", FieldType.NUMERIC, 8);

        pnd_Start_Yyyymmdd__R_Field_21 = localVariables.newGroupInRecord("pnd_Start_Yyyymmdd__R_Field_21", "REDEFINE", pnd_Start_Yyyymmdd);
        pnd_Start_Yyyymmdd_Pnd_Start_Date = pnd_Start_Yyyymmdd__R_Field_21.newFieldInGroup("pnd_Start_Yyyymmdd_Pnd_Start_Date", "#START-DATE", FieldType.STRING, 
            8);

        pnd_Start_Yyyymmdd__R_Field_22 = localVariables.newGroupInRecord("pnd_Start_Yyyymmdd__R_Field_22", "REDEFINE", pnd_Start_Yyyymmdd);
        pnd_Start_Yyyymmdd_Pnd_Start_Yyyymm = pnd_Start_Yyyymmdd__R_Field_22.newFieldInGroup("pnd_Start_Yyyymmdd_Pnd_Start_Yyyymm", "#START-YYYYMM", FieldType.NUMERIC, 
            6);
        pnd_Start_Yyyymmdd_Pnd_Start_Dd = pnd_Start_Yyyymmdd__R_Field_22.newFieldInGroup("pnd_Start_Yyyymmdd_Pnd_Start_Dd", "#START-DD", FieldType.NUMERIC, 
            2);
        pnd_End_Datd = localVariables.newFieldInRecord("pnd_End_Datd", "#END-DATD", FieldType.DATE);
        pnd_End_Yyyymmdd = localVariables.newFieldInRecord("pnd_End_Yyyymmdd", "#END-YYYYMMDD", FieldType.NUMERIC, 8);

        pnd_End_Yyyymmdd__R_Field_23 = localVariables.newGroupInRecord("pnd_End_Yyyymmdd__R_Field_23", "REDEFINE", pnd_End_Yyyymmdd);
        pnd_End_Yyyymmdd_Pnd_End_Date = pnd_End_Yyyymmdd__R_Field_23.newFieldInGroup("pnd_End_Yyyymmdd_Pnd_End_Date", "#END-DATE", FieldType.STRING, 8);

        pnd_End_Yyyymmdd__R_Field_24 = localVariables.newGroupInRecord("pnd_End_Yyyymmdd__R_Field_24", "REDEFINE", pnd_End_Yyyymmdd);
        pnd_End_Yyyymmdd_Pnd_End_Yyyymm = pnd_End_Yyyymmdd__R_Field_24.newFieldInGroup("pnd_End_Yyyymmdd_Pnd_End_Yyyymm", "#END-YYYYMM", FieldType.NUMERIC, 
            6);
        pnd_End_Yyyymmdd_Pnd_End_Dd = pnd_End_Yyyymmdd__R_Field_24.newFieldInGroup("pnd_End_Yyyymmdd_Pnd_End_Dd", "#END-DD", FieldType.NUMERIC, 2);
        pnd_Last_Chnge_Dte_Tme = localVariables.newFieldInRecord("pnd_Last_Chnge_Dte_Tme", "#LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 15);

        pnd_Last_Chnge_Dte_Tme__R_Field_25 = localVariables.newGroupInRecord("pnd_Last_Chnge_Dte_Tme__R_Field_25", "REDEFINE", pnd_Last_Chnge_Dte_Tme);
        pnd_Last_Chnge_Dte_Tme_Pnd_Last_Chnge_Yyyymmdd = pnd_Last_Chnge_Dte_Tme__R_Field_25.newFieldInGroup("pnd_Last_Chnge_Dte_Tme_Pnd_Last_Chnge_Yyyymmdd", 
            "#LAST-CHNGE-YYYYMMDD", FieldType.NUMERIC, 8);
        pnd_Last_Chnge_Dte_D = localVariables.newFieldInRecord("pnd_Last_Chnge_Dte_D", "#LAST-CHNGE-DTE-D", FieldType.DATE);
        pnd_Log_Dte_Tme = localVariables.newFieldInRecord("pnd_Log_Dte_Tme", "#LOG-DTE-TME", FieldType.NUMERIC, 15);
        pnd_Write_Count = localVariables.newFieldInRecord("pnd_Write_Count", "#WRITE-COUNT", FieldType.NUMERIC, 9);
        pnd_Write_Count2 = localVariables.newFieldInRecord("pnd_Write_Count2", "#WRITE-COUNT2", FieldType.NUMERIC, 9);
        pnd_Closed_Status_Codes = localVariables.newFieldArrayInRecord("pnd_Closed_Status_Codes", "#CLOSED-STATUS-CODES", FieldType.STRING, 4, new DbsArrayController(1, 
            68));
        pnd_Arr_Rec_Cnt = localVariables.newFieldInRecord("pnd_Arr_Rec_Cnt", "#ARR-REC-CNT", FieldType.NUMERIC, 3);

        pnd_Array_Group = localVariables.newGroupArrayInRecord("pnd_Array_Group", "#ARRAY-GROUP", new DbsArrayController(1, 1000));
        pnd_Array_Group_Pnd_Arr_Pin_Nbr = pnd_Array_Group.newFieldInGroup("pnd_Array_Group_Pnd_Arr_Pin_Nbr", "#ARR-PIN-NBR", FieldType.STRING, 12);
        pnd_Array_Group_Pnd_Arr_Wpid = pnd_Array_Group.newFieldInGroup("pnd_Array_Group_Pnd_Arr_Wpid", "#ARR-WPID", FieldType.STRING, 6);
        pnd_Array_Group_Pnd_Arr_Status_Cde = pnd_Array_Group.newFieldInGroup("pnd_Array_Group_Pnd_Arr_Status_Cde", "#ARR-STATUS-CDE", FieldType.STRING, 
            4);
        pnd_Array_Group_Pnd_Arr_Empl_Oprtr = pnd_Array_Group.newFieldInGroup("pnd_Array_Group_Pnd_Arr_Empl_Oprtr", "#ARR-EMPL-OPRTR", FieldType.STRING, 
            10);
        pnd_Array_Group_Pnd_Arr_Last_Chg_Dt = pnd_Array_Group.newFieldInGroup("pnd_Array_Group_Pnd_Arr_Last_Chg_Dt", "#ARR-LAST-CHG-DT", FieldType.NUMERIC, 
            15);
        pnd_Array_Group_Pnd_Arr_Last_Chg_Op = pnd_Array_Group.newFieldInGroup("pnd_Array_Group_Pnd_Arr_Last_Chg_Op", "#ARR-LAST-CHG-OP", FieldType.STRING, 
            8);
        pnd_Prev_Status_Code = localVariables.newFieldInRecord("pnd_Prev_Status_Code", "#PREV-STATUS-CODE", FieldType.STRING, 4);
        pnd_Qc_Cnt = localVariables.newFieldInRecord("pnd_Qc_Cnt", "#QC-CNT", FieldType.NUMERIC, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_mit.reset();
        vw_mit_Hist.reset();
        vw_cwf_Support_Tbl.reset();

        localVariables.reset();
        pnd_Closed_Status_Codes.getValue(1).setInitialValue(6005);
        pnd_Closed_Status_Codes.getValue(2).setInitialValue(6200);
        pnd_Closed_Status_Codes.getValue(3).setInitialValue(7200);
        pnd_Closed_Status_Codes.getValue(4).setInitialValue(7492);
        pnd_Closed_Status_Codes.getValue(5).setInitialValue(7501);
        pnd_Closed_Status_Codes.getValue(6).setInitialValue(7502);
        pnd_Closed_Status_Codes.getValue(7).setInitialValue(7503);
        pnd_Closed_Status_Codes.getValue(8).setInitialValue(7591);
        pnd_Closed_Status_Codes.getValue(9).setInitialValue(7592);
        pnd_Closed_Status_Codes.getValue(10).setInitialValue(7594);
        pnd_Closed_Status_Codes.getValue(11).setInitialValue(7596);
        pnd_Closed_Status_Codes.getValue(12).setInitialValue(7599);
        pnd_Closed_Status_Codes.getValue(13).setInitialValue(7630);
        pnd_Closed_Status_Codes.getValue(14).setInitialValue(7631);
        pnd_Closed_Status_Codes.getValue(15).setInitialValue(7632);
        pnd_Closed_Status_Codes.getValue(16).setInitialValue(7909);
        pnd_Closed_Status_Codes.getValue(17).setInitialValue(7910);
        pnd_Closed_Status_Codes.getValue(18).setInitialValue(7911);
        pnd_Closed_Status_Codes.getValue(19).setInitialValue(7912);
        pnd_Closed_Status_Codes.getValue(20).setInitialValue(7913);
        pnd_Closed_Status_Codes.getValue(21).setInitialValue(7914);
        pnd_Closed_Status_Codes.getValue(22).setInitialValue(7915);
        pnd_Closed_Status_Codes.getValue(23).setInitialValue(7916);
        pnd_Closed_Status_Codes.getValue(24).setInitialValue(7917);
        pnd_Closed_Status_Codes.getValue(25).setInitialValue(7920);
        pnd_Closed_Status_Codes.getValue(26).setInitialValue(8000);
        pnd_Closed_Status_Codes.getValue(27).setInitialValue(8010);
        pnd_Closed_Status_Codes.getValue(28).setInitialValue(8011);
        pnd_Closed_Status_Codes.getValue(29).setInitialValue(8012);
        pnd_Closed_Status_Codes.getValue(30).setInitialValue(8021);
        pnd_Closed_Status_Codes.getValue(31).setInitialValue(8044);
        pnd_Closed_Status_Codes.getValue(32).setInitialValue(8045);
        pnd_Closed_Status_Codes.getValue(33).setInitialValue(8046);
        pnd_Closed_Status_Codes.getValue(34).setInitialValue(8060);
        pnd_Closed_Status_Codes.getValue(35).setInitialValue(8071);
        pnd_Closed_Status_Codes.getValue(36).setInitialValue(8072);
        pnd_Closed_Status_Codes.getValue(37).setInitialValue(8080);
        pnd_Closed_Status_Codes.getValue(38).setInitialValue(8100);
        pnd_Closed_Status_Codes.getValue(39).setInitialValue(8118);
        pnd_Closed_Status_Codes.getValue(40).setInitialValue(8121);
        pnd_Closed_Status_Codes.getValue(41).setInitialValue(8250);
        pnd_Closed_Status_Codes.getValue(42).setInitialValue(8255);
        pnd_Closed_Status_Codes.getValue(43).setInitialValue(8300);
        pnd_Closed_Status_Codes.getValue(44).setInitialValue(8330);
        pnd_Closed_Status_Codes.getValue(45).setInitialValue(8350);
        pnd_Closed_Status_Codes.getValue(46).setInitialValue(8351);
        pnd_Closed_Status_Codes.getValue(47).setInitialValue(8352);
        pnd_Closed_Status_Codes.getValue(48).setInitialValue(8400);
        pnd_Closed_Status_Codes.getValue(49).setInitialValue(8444);
        pnd_Closed_Status_Codes.getValue(50).setInitialValue(8500);
        pnd_Closed_Status_Codes.getValue(51).setInitialValue(8520);
        pnd_Closed_Status_Codes.getValue(52).setInitialValue(8521);
        pnd_Closed_Status_Codes.getValue(53).setInitialValue(8530);
        pnd_Closed_Status_Codes.getValue(54).setInitialValue(8600);
        pnd_Closed_Status_Codes.getValue(55).setInitialValue(8701);
        pnd_Closed_Status_Codes.getValue(56).setInitialValue(8780);
        pnd_Closed_Status_Codes.getValue(57).setInitialValue(8781);
        pnd_Closed_Status_Codes.getValue(58).setInitialValue(8782);
        pnd_Closed_Status_Codes.getValue(59).setInitialValue(8783);
        pnd_Closed_Status_Codes.getValue(60).setInitialValue(8784);
        pnd_Closed_Status_Codes.getValue(61).setInitialValue(8785);
        pnd_Closed_Status_Codes.getValue(62).setInitialValue(8786);
        pnd_Closed_Status_Codes.getValue(63).setInitialValue(8787);
        pnd_Closed_Status_Codes.getValue(64).setInitialValue(8788);
        pnd_Closed_Status_Codes.getValue(65).setInitialValue(8789);
        pnd_Closed_Status_Codes.getValue(66).setInitialValue(9400);
        pnd_Closed_Status_Codes.getValue(67).setInitialValue(9401);
        pnd_Closed_Status_Codes.getValue(68).setInitialValue(9993);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb8637() throws Exception
    {
        super("Cwfb8637");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Cwfb8637|Main");
        setupReports();
        while(true)
        {
            try
            {
                //*                                                                                                                                                       //Natural: FORMAT PS = 22 LS = 132
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Frequency);                                                                                        //Natural: INPUT #FREQUENCY
                //*  RUN EVERY WEEK
                if (condition(pnd_Frequency.equals("WEEKLY ")))                                                                                                           //Natural: IF #FREQUENCY = 'WEEKLY '
                {
                    pnd_Start_Datd.compute(new ComputeParameters(false, pnd_Start_Datd), Global.getDATX().subtract(7));                                                   //Natural: ASSIGN #START-DATD := *DATX - 7
                    pnd_End_Datd.compute(new ComputeParameters(false, pnd_End_Datd), Global.getDATX().subtract(1));                                                       //Natural: ASSIGN #END-DATD := *DATX - 1
                    pnd_Start_Yyyymmdd_Pnd_Start_Date.setValueEdited(pnd_Start_Datd,new ReportEditMask("YYYYMMDD"));                                                      //Natural: MOVE EDITED #START-DATD ( EM = YYYYMMDD ) TO #START-DATE
                    pnd_End_Yyyymmdd_Pnd_End_Date.setValueEdited(pnd_End_Datd,new ReportEditMask("YYYYMMDD"));                                                            //Natural: MOVE EDITED #END-DATD ( EM = YYYYMMDD ) TO #END-DATE
                }                                                                                                                                                         //Natural: END-IF
                //*  GET PRIOR MONTH's date
                if (condition(pnd_Frequency.equals("MONTHLY")))                                                                                                           //Natural: IF #FREQUENCY = 'MONTHLY'
                {
                    pnd_Start_Datd.compute(new ComputeParameters(false, pnd_Start_Datd), Global.getDATX().subtract(28));                                                  //Natural: ASSIGN #START-DATD := *DATX - 28
                    //*  FIRST DAY OF
                    pnd_Start_Yyyymmdd_Pnd_Start_Date.setValueEdited(pnd_Start_Datd,new ReportEditMask("YYYYMMDD"));                                                      //Natural: MOVE EDITED #START-DATD ( EM = YYYYMMDD ) TO #START-DATE
                    //*    PRIOR MONTH
                    pnd_Start_Yyyymmdd_Pnd_Start_Dd.setValue(1);                                                                                                          //Natural: MOVE 01 TO #START-DD
                    pnd_End_Datd.setValue(Global.getDATX());                                                                                                              //Natural: ASSIGN #END-DATD := *DATX
                    //*  FIRST DAY OF
                    pnd_End_Yyyymmdd_Pnd_End_Date.setValueEdited(pnd_End_Datd,new ReportEditMask("YYYYMMDD"));                                                            //Natural: MOVE EDITED #END-DATD ( EM = YYYYMMDD ) TO #END-DATE
                    //*  CURRENT MONTH
                    pnd_End_Yyyymmdd_Pnd_End_Dd.setValue(1);                                                                                                              //Natural: MOVE 01 TO #END-DD
                    pnd_End_Datd.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_End_Yyyymmdd_Pnd_End_Date);                                                            //Natural: MOVE EDITED #END-DATE TO #END-DATD ( EM = YYYYMMDD )
                    //*  CLOSED REQUEST
                }                                                                                                                                                         //Natural: END-IF
                //* **************** TEST ** TEST ** TEST ** TEST ** TEST ** TEST ** TEST
                //*  #START-DATE  := 20111201
                //*  #END-DATE    := 20111231
                //* **************** TEST ** TEST ** TEST ** TEST ** TEST ** TEST ** TEST
                //* *
                //*  READ BY INVERT DATE
                //* *
                pnd_Status_Date_Key.setValue(" ");                                                                                                                        //Natural: ASSIGN #STATUS-DATE-KEY := ' '
                pnd_Status_Date_Key_Pnd_K_Status.setValue("9");                                                                                                           //Natural: ASSIGN #K-STATUS := '9'
                pnd_Status_Date_Key_Pnd_K_Invert_Yymmdd.compute(new ComputeParameters(false, pnd_Status_Date_Key_Pnd_K_Invert_Yymmdd), DbsField.subtract(99999999,        //Natural: COMPUTE #K-INVERT-YYMMDD = 99999999 - #END-YYYYMMDD
                    pnd_End_Yyyymmdd));
                pnd_Status_Date_Key_Pnd_K_Invert_Hhiisst.setValue(0);                                                                                                     //Natural: ASSIGN #K-INVERT-HHIISST := 0000000
                getReports().write(0, "=",pnd_Start_Yyyymmdd,"=",pnd_End_Yyyymmdd,"=",pnd_End_Datd);                                                                      //Natural: WRITE '=' #START-YYYYMMDD '=' #END-YYYYMMDD '=' #END-DATD
                if (Global.isEscape()) return;
                //*  LOAD TABLES (UNITS, OPERATORS, WPIDS)
                pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind.setValue("A");                                                                                                  //Natural: ASSIGN #TBL-SCRTY-LEVEL-IND := 'A'
                pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme.setValue("CWF-REPORTING-BENE");                                                                                       //Natural: ASSIGN #TBL-TABLE-NME := 'CWF-REPORTING-BENE'
                pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field.setValue(" ");                                                                                                        //Natural: ASSIGN #TBL-KEY-FIELD := ' '
                vw_cwf_Support_Tbl.startDatabaseRead                                                                                                                      //Natural: READ CWF-SUPPORT-TBL BY TBL-PRIME-KEY STARTING FROM #TBL-PRIME-KEY
                (
                "READ01",
                new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key, WcType.BY) },
                new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
                );
                READ01:
                while (condition(vw_cwf_Support_Tbl.readNextRow("READ01")))
                {
                    if (condition(cwf_Support_Tbl_Tbl_Scrty_Level_Ind.notEquals("A ") || cwf_Support_Tbl_Tbl_Table_Nme.notEquals("CWF-REPORTING-BENE")))                  //Natural: IF TBL-SCRTY-LEVEL-IND NE 'A ' OR TBL-TABLE-NME NE 'CWF-REPORTING-BENE'
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    short decideConditionsMet242 = 0;                                                                                                                     //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN SUBSTRING ( CWF-SUPPORT-TBL.TBL-KEY-FIELD,1,9 ) = 'BENE-UNIT'
                    if (condition(cwf_Support_Tbl_Tbl_Key_Field.getSubstring(1,9).equals("BENE-UNIT")))
                    {
                        decideConditionsMet242++;
                        pnd_I_Unit.nadd(1);                                                                                                                               //Natural: ADD 1 TO #I-UNIT
                        pnd_Units_Pnd_T_Unit.getValue(pnd_I_Unit).setValue(cwf_Support_Tbl_Tbl_Unit);                                                                     //Natural: ASSIGN #T-UNIT ( #I-UNIT ) := CWF-SUPPORT-TBL.TBL-UNIT
                    }                                                                                                                                                     //Natural: WHEN SUBSTRING ( CWF-SUPPORT-TBL.TBL-KEY-FIELD,1,9 ) = 'BENE-USER'
                    else if (condition(cwf_Support_Tbl_Tbl_Key_Field.getSubstring(1,9).equals("BENE-USER")))
                    {
                        decideConditionsMet242++;
                        pnd_I_User.nadd(1);                                                                                                                               //Natural: ADD 1 TO #I-USER
                        pnd_Operators_Pnd_T_Oprtr.getValue(pnd_I_User).setValue(cwf_Support_Tbl_Tbl_User);                                                                //Natural: ASSIGN #T-OPRTR ( #I-USER ) := CWF-SUPPORT-TBL.TBL-USER
                    }                                                                                                                                                     //Natural: WHEN SUBSTRING ( CWF-SUPPORT-TBL.TBL-KEY-FIELD,1,9 ) = 'BENE-WPID'
                    else if (condition(cwf_Support_Tbl_Tbl_Key_Field.getSubstring(1,9).equals("BENE-WPID")))
                    {
                        decideConditionsMet242++;
                        pnd_I_Wpid.nadd(1);                                                                                                                               //Natural: ADD 1 TO #I-WPID
                        pnd_Wpids_Pnd_T_Wpid.getValue(pnd_I_Wpid).setValue(cwf_Support_Tbl_Tbl_Wpid);                                                                     //Natural: ASSIGN #T-WPID ( #I-WPID ) := CWF-SUPPORT-TBL.TBL-WPID
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                    //*  PRODUCTIVITY REPORT
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                //*  HEADER RECORD
                pnd_Work_File_Pnd_Wf_Run.setValue(pnd_Frequency);                                                                                                         //Natural: ASSIGN #WF-RUN := #FREQUENCY
                pnd_Work_File_Pnd_Wf_Start_Dte.setValue(pnd_Start_Yyyymmdd);                                                                                              //Natural: ASSIGN #WF-START-DTE := #START-YYYYMMDD
                pnd_Work_File_Pnd_Wf_End_Dte.setValue(pnd_End_Yyyymmdd);                                                                                                  //Natural: ASSIGN #WF-END-DTE := #END-YYYYMMDD
                pnd_Work_File_Pnd_Wf_Report_Name.setValue("P");                                                                                                           //Natural: ASSIGN #WF-REPORT-NAME := 'P'
                //*  QUALITY REPORT
                getWorkFiles().write(1, false, pnd_Work_File);                                                                                                            //Natural: WRITE WORK FILE 1 #WORK-FILE
                pnd_Work_File_Pnd_Wf_Report_Name.setValue("Q");                                                                                                           //Natural: ASSIGN #WF-REPORT-NAME := 'Q'
                getWorkFiles().write(2, false, pnd_Work_File);                                                                                                            //Natural: WRITE WORK FILE 2 #WORK-FILE
                pnd_Work_File.reset();                                                                                                                                    //Natural: RESET #WORK-FILE
                vw_mit.startDatabaseRead                                                                                                                                  //Natural: READ MIT BY CRPRTE-STATUS-CHNGE-DTE-KEY STARTING FROM #STATUS-DATE-KEY
                (
                "READ02",
                new Wc[] { new Wc("CRPRTE_STATUS_CHNGE_DTE_KEY", ">=", pnd_Status_Date_Key, WcType.BY) },
                new Oc[] { new Oc("CRPRTE_STATUS_CHNGE_DTE_KEY", "ASC") }
                );
                READ02:
                while (condition(vw_mit.readNextRow("READ02")))
                {
                    pnd_Last_Chnge_Dte_Tme.setValue(mit_Last_Chnge_Dte_Tme);                                                                                              //Natural: ASSIGN #LAST-CHNGE-DTE-TME := MIT.LAST-CHNGE-DTE-TME
                    //*  REQUESTS ARE READ IN REVERSE DATE ORDER
                    if (condition(pnd_Last_Chnge_Dte_Tme_Pnd_Last_Chnge_Yyyymmdd.less(pnd_Start_Yyyymmdd)))                                                               //Natural: IF #LAST-CHNGE-YYYYMMDD LT #START-YYYYMMDD
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_History_Key.reset();                                                                                                                              //Natural: RESET #HISTORY-KEY
                    pnd_History_Key_Pnd_K_Log_Dte_Tme.setValue(mit_Rqst_Log_Dte_Tme);                                                                                     //Natural: ASSIGN #K-LOG-DTE-TME := MIT.RQST-LOG-DTE-TME
                    pnd_History_Key_Pnd_K_Last_Chnge_Invrt_D_T.setValue(0);                                                                                               //Natural: ASSIGN #K-LAST-CHNGE-INVRT-D-T := 0
                                                                                                                                                                          //Natural: PERFORM DETERMINE-REQUEST-HISTORY
                    sub_Determine_Request_History();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    //*  TESTING
                    if (condition(mit_Work_Prcss_Id.equals(pnd_Wpids_Pnd_T_Wpid.getValue("*"))))                                                                          //Natural: IF MIT.WORK-PRCSS-ID = #T-WPID ( * )
                    {
                        //*  IF MIT.WORK-PRCSS-ID  = MASK('F') OR = MASK('I') OR = MASK('R')
                        //*  IF MIT.WORK-PRCSS-ID  = MASK('T')
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Work_File_Pnd_Wf_Wpid.setValue(mit_Work_Prcss_Id);                                                                                                //Natural: ASSIGN #WF-WPID := MIT.WORK-PRCSS-ID
                    pnd_Work_File_Pnd_Wf_Pin.setValue(mit_Pin_Nbr);                                                                                                       //Natural: ASSIGN #WF-PIN := MIT.PIN-NBR
                    pnd_Work_File_Pnd_Wf_Recvd_Dte_Tme.setValueEdited(mit_Tiaa_Rcvd_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                       //Natural: MOVE EDITED MIT.TIAA-RCVD-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #WF-RECVD-DTE-TME
                    pnd_Work_File_Pnd_Wf_Due_Dte_Tme.setValueEdited(mit_Crprte_Due_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                        //Natural: MOVE EDITED MIT.CRPRTE-DUE-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #WF-DUE-DTE-TME
                    pnd_Work_File_Pnd_Wf_Compltd_Dte_Tme.setValue(mit_Last_Chnge_Dte_Tme);                                                                                //Natural: ASSIGN #WF-COMPLTD-DTE-TME := MIT.LAST-CHNGE-DTE-TME
                    pnd_Work_File_Pnd_Wf_Status_Cde.setValue(mit_Status_Cde);                                                                                             //Natural: ASSIGN #WF-STATUS-CDE := MIT.STATUS-CDE
                    if (condition(pnd_Arr_Rec_Cnt.notEquals(getZero())))                                                                                                  //Natural: IF #ARR-REC-CNT NE 0
                    {
                        //*    WRITE '=' MIT.PIN-NBR
                        pnd_Work_File_Pnd_Wf_Unit_Open_Stts.reset();                                                                                                      //Natural: RESET #WF-UNIT-OPEN-STTS #WF-UNIT-CLSD-STTS #WF-UNIT-OPEN-DTE-TME #WF-UNIT-CLSD-DTE-TME #QC-CNT
                        pnd_Work_File_Pnd_Wf_Unit_Clsd_Stts.reset();
                        pnd_Work_File_Pnd_Wf_Unit_Open_Dte_Tme.reset();
                        pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte_Tme.reset();
                        pnd_Qc_Cnt.reset();
                        if (condition(DbsUtil.maskMatches(mit_Work_Prcss_Id,"'F'") || DbsUtil.maskMatches(mit_Work_Prcss_Id,"'I'") || DbsUtil.maskMatches(mit_Work_Prcss_Id, //Natural: IF MIT.WORK-PRCSS-ID = MASK ( 'F' ) OR = MASK ( 'I' ) OR = MASK ( 'R' )
                            "'R'")))
                        {
                            //*      WRITE '------------------------ARRAY START-----------------------'
                            FOR01:                                                                                                                                        //Natural: FOR #J #ARR-REC-CNT TO 1 -1
                            for (pnd_J.setValue(pnd_Arr_Rec_Cnt); condition(pnd_J.greaterOrEqual(1)); pnd_J.nsubtract(1))
                            {
                                //*        WRITE '=' #ARR-PIN-NBR (#J) /
                                //*          '=' #ARR-WPID(#J)       /
                                //*          '=' #ARR-STATUS-CDE(#J) /
                                //*          '=' #ARR-EMPL-OPRTR(#J) /
                                //*          '=' #ARR-LAST-CHG-DT(#J)/
                                //*          '=' #ARR-LAST-CHG-OP(#J)
                                //*          '=' #J   '***' /
                                if (condition(DbsUtil.maskMatches(pnd_Array_Group_Pnd_Arr_Status_Cde.getValue(pnd_J),"'2'...")))                                          //Natural: IF #ARR-STATUS-CDE ( #J ) = MASK ( '2'... )
                                {
                                    pnd_Work_File_Pnd_Wf_Unit_Open_Stts.setValue(pnd_Array_Group_Pnd_Arr_Status_Cde.getValue(pnd_J));                                     //Natural: ASSIGN #WF-UNIT-OPEN-STTS := #ARR-STATUS-CDE ( #J )
                                    pnd_Work_File_Pnd_Wf_Unit_Open_Dte_Tme.setValue(pnd_Array_Group_Pnd_Arr_Last_Chg_Dt.getValue(pnd_J));                                 //Natural: ASSIGN #WF-UNIT-OPEN-DTE-TME := #ARR-LAST-CHG-DT ( #J )
                                }                                                                                                                                         //Natural: END-IF
                                if (condition(pnd_Array_Group_Pnd_Arr_Status_Cde.getValue(pnd_J).equals(pnd_Closed_Status_Codes.getValue("*")) && pnd_Array_Group_Pnd_Arr_Last_Chg_Op.getValue(pnd_J).equals(pnd_Operators_Pnd_T_Oprtr.getValue("*")))) //Natural: IF #ARR-STATUS-CDE ( #J ) = #CLOSED-STATUS-CODES ( * ) AND #ARR-LAST-CHG-OP ( #J ) = #T-OPRTR ( * )
                                {
                                    pnd_Work_File_Pnd_Wf_Unit_Clsd_Stts.setValue(pnd_Array_Group_Pnd_Arr_Status_Cde.getValue(pnd_J));                                     //Natural: ASSIGN #WF-UNIT-CLSD-STTS := #ARR-STATUS-CDE ( #J )
                                    pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte_Tme.setValue(pnd_Array_Group_Pnd_Arr_Last_Chg_Dt.getValue(pnd_J));                                 //Natural: ASSIGN #WF-UNIT-CLSD-DTE-TME := #ARR-LAST-CHG-DT ( #J )
                                    pnd_Work_File_Pnd_Wf_Last_Chnge_Oprtr.setValue(pnd_Array_Group_Pnd_Arr_Last_Chg_Op.getValue(pnd_J));                                  //Natural: ASSIGN #WF-LAST-CHNGE-OPRTR := #ARR-LAST-CHG-OP ( #J )
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-FOR
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            //*      WRITE '-----------------------ARRAY END--------------------------'
                            if (condition(pnd_Work_File_Pnd_Wf_Unit_Open_Stts.notEquals(" ") && pnd_Work_File_Pnd_Wf_Unit_Clsd_Stts.notEquals(" ")))                      //Natural: IF #WF-UNIT-OPEN-STTS NE ' ' AND #WF-UNIT-CLSD-STTS NE ' '
                            {
                                pnd_Work_File_Pnd_Wf_Oprtr.setValue(pnd_Work_File_Pnd_Wf_Last_Chnge_Oprtr);                                                               //Natural: ASSIGN #WF-OPRTR := #WF-LAST-CHNGE-OPRTR
                                //*        PRINT '=' #WORK-FILE
                                getWorkFiles().write(1, false, pnd_Work_File);                                                                                            //Natural: WRITE WORK FILE 1 #WORK-FILE
                                pnd_Write_Count.nadd(1);                                                                                                                  //Natural: ADD 1 TO #WRITE-COUNT
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(DbsUtil.maskMatches(mit_Work_Prcss_Id,"'T'")))                                                                                      //Natural: IF MIT.WORK-PRCSS-ID = MASK ( 'T' )
                        {
                            //*      WRITE '------------------------ARRAY START-----------------------'
                            FOR02:                                                                                                                                        //Natural: FOR #J #ARR-REC-CNT TO 1 -1
                            for (pnd_J.setValue(pnd_Arr_Rec_Cnt); condition(pnd_J.greaterOrEqual(1)); pnd_J.nsubtract(1))
                            {
                                //*        WRITE '=' #ARR-PIN-NBR (#J) /
                                //*          '=' #ARR-WPID(#J)       /
                                //*          '=' #ARR-STATUS-CDE(#J) /
                                //*          '=' #ARR-EMPL-OPRTR(#J) /
                                //*          '=' #ARR-LAST-CHG-DT(#J)/
                                //*          '=' #ARR-LAST-CHG-OP(#J)
                                //*          '=' #J   '***' /
                                pnd_Work_File_Pnd_Wf_Unit_Clsd_Stts.reset();                                                                                              //Natural: RESET #WF-UNIT-CLSD-STTS #PREV-STATUS-CODE
                                pnd_Prev_Status_Code.reset();
                                if (condition(DbsUtil.maskMatches(pnd_Array_Group_Pnd_Arr_Status_Cde.getValue(pnd_J),"'2'...") && pnd_Work_File_Pnd_Wf_Unit_Open_Stts.equals(" "))) //Natural: IF #ARR-STATUS-CDE ( #J ) = MASK ( '2'... ) AND #WF-UNIT-OPEN-STTS = ' '
                                {
                                    pnd_Work_File_Pnd_Wf_Unit_Open_Stts.setValue(pnd_Array_Group_Pnd_Arr_Status_Cde.getValue(pnd_J));                                     //Natural: ASSIGN #WF-UNIT-OPEN-STTS := #ARR-STATUS-CDE ( #J )
                                    pnd_Work_File_Pnd_Wf_Unit_Open_Dte_Tme.setValue(pnd_Array_Group_Pnd_Arr_Last_Chg_Dt.getValue(pnd_J));                                 //Natural: ASSIGN #WF-UNIT-OPEN-DTE-TME := #ARR-LAST-CHG-DT ( #J )
                                }                                                                                                                                         //Natural: END-IF
                                pnd_I.compute(new ComputeParameters(false, pnd_I), pnd_J.add(1));                                                                         //Natural: ASSIGN #I := #J + 1
                                if (condition(pnd_I.equals(getZero())))                                                                                                   //Natural: IF #I EQ 0
                                {
                                    pnd_Prev_Status_Code.setValue(" ");                                                                                                   //Natural: ASSIGN #PREV-STATUS-CODE := ' '
                                }                                                                                                                                         //Natural: ELSE
                                else if (condition())
                                {
                                    pnd_Prev_Status_Code.setValue(pnd_Array_Group_Pnd_Arr_Status_Cde.getValue(pnd_I));                                                    //Natural: ASSIGN #PREV-STATUS-CODE := #ARR-STATUS-CDE ( #I )
                                }                                                                                                                                         //Natural: END-IF
                                //*        WRITE '=' #ARR-STATUS-CDE(#J)     /
                                //*          '=' #PREV-STATUS-CODE /
                                //*          '=' #ARR-LAST-CHG-OP(#J)
                                if (condition(pnd_Array_Group_Pnd_Arr_Status_Cde.getValue(pnd_J).equals("7500") && pnd_Prev_Status_Code.notEquals("7500")                 //Natural: IF #ARR-STATUS-CDE ( #J ) = '7500' AND #PREV-STATUS-CODE NE '7500' AND #ARR-LAST-CHG-OP ( #J ) = #T-OPRTR ( * )
                                    && pnd_Array_Group_Pnd_Arr_Last_Chg_Op.getValue(pnd_J).equals(pnd_Operators_Pnd_T_Oprtr.getValue("*"))))
                                {
                                    pnd_Work_File_Pnd_Wf_Unit_Clsd_Stts.setValue(pnd_Array_Group_Pnd_Arr_Status_Cde.getValue(pnd_J));                                     //Natural: ASSIGN #WF-UNIT-CLSD-STTS := #ARR-STATUS-CDE ( #J )
                                    pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte_Tme.setValue(pnd_Array_Group_Pnd_Arr_Last_Chg_Dt.getValue(pnd_J));                                 //Natural: ASSIGN #WF-UNIT-CLSD-DTE-TME := #ARR-LAST-CHG-DT ( #J )
                                    pnd_Work_File_Pnd_Wf_Last_Chnge_Oprtr.setValue(pnd_Array_Group_Pnd_Arr_Last_Chg_Op.getValue(pnd_J));                                  //Natural: ASSIGN #WF-LAST-CHNGE-OPRTR := #ARR-LAST-CHG-OP ( #J )
                                    pnd_Qc_Cnt.nadd(1);                                                                                                                   //Natural: ASSIGN #QC-CNT := #QC-CNT + 1
                                }                                                                                                                                         //Natural: END-IF
                                if (condition(pnd_Array_Group_Pnd_Arr_Status_Cde.getValue(pnd_J).equals("2104") && pnd_Prev_Status_Code.notEquals("2104")                 //Natural: IF #ARR-STATUS-CDE ( #J ) = '2104' AND #PREV-STATUS-CODE NE '2104' AND #ARR-LAST-CHG-OP ( #J ) = #T-OPRTR ( * )
                                    && pnd_Array_Group_Pnd_Arr_Last_Chg_Op.getValue(pnd_J).equals(pnd_Operators_Pnd_T_Oprtr.getValue("*"))))
                                {
                                    pnd_Work_File_Pnd_Wf_Unit_Clsd_Stts.setValue(pnd_Array_Group_Pnd_Arr_Status_Cde.getValue(pnd_J));                                     //Natural: ASSIGN #WF-UNIT-CLSD-STTS := #ARR-STATUS-CDE ( #J )
                                    pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte_Tme.setValue(pnd_Array_Group_Pnd_Arr_Last_Chg_Dt.getValue(pnd_J));                                 //Natural: ASSIGN #WF-UNIT-CLSD-DTE-TME := #ARR-LAST-CHG-DT ( #J )
                                    pnd_Work_File_Pnd_Wf_Last_Chnge_Oprtr.setValue(pnd_Array_Group_Pnd_Arr_Last_Chg_Op.getValue(pnd_J));                                  //Natural: ASSIGN #WF-LAST-CHNGE-OPRTR := #ARR-LAST-CHG-OP ( #J )
                                }                                                                                                                                         //Natural: END-IF
                                if (condition(pnd_Array_Group_Pnd_Arr_Status_Cde.getValue(pnd_J).equals(pnd_Closed_Status_Codes.getValue("*")) && pnd_Array_Group_Pnd_Arr_Last_Chg_Op.getValue(pnd_J).equals(pnd_Operators_Pnd_T_Oprtr.getValue("*")))) //Natural: IF #ARR-STATUS-CDE ( #J ) = #CLOSED-STATUS-CODES ( * ) AND #ARR-LAST-CHG-OP ( #J ) = #T-OPRTR ( * )
                                {
                                    pnd_Work_File_Pnd_Wf_Unit_Clsd_Stts.setValue(pnd_Array_Group_Pnd_Arr_Status_Cde.getValue(pnd_J));                                     //Natural: ASSIGN #WF-UNIT-CLSD-STTS := #ARR-STATUS-CDE ( #J )
                                    pnd_Work_File_Pnd_Wf_Unit_Clsd_Dte_Tme.setValue(pnd_Array_Group_Pnd_Arr_Last_Chg_Dt.getValue(pnd_J));                                 //Natural: ASSIGN #WF-UNIT-CLSD-DTE-TME := #ARR-LAST-CHG-DT ( #J )
                                    pnd_Work_File_Pnd_Wf_Last_Chnge_Oprtr.setValue(pnd_Array_Group_Pnd_Arr_Last_Chg_Op.getValue(pnd_J));                                  //Natural: ASSIGN #WF-LAST-CHNGE-OPRTR := #ARR-LAST-CHG-OP ( #J )
                                }                                                                                                                                         //Natural: END-IF
                                //*        WRITE '=' #WF-UNIT-OPEN-STTS /
                                //*          '=' #WF-UNIT-CLSD-STTS
                                if (condition(pnd_Work_File_Pnd_Wf_Unit_Open_Stts.notEquals(" ") && pnd_Work_File_Pnd_Wf_Unit_Clsd_Stts.notEquals(" ")))                  //Natural: IF #WF-UNIT-OPEN-STTS NE ' ' AND #WF-UNIT-CLSD-STTS NE ' '
                                {
                                    pnd_Work_File_Pnd_Wf_Oprtr.setValue(pnd_Work_File_Pnd_Wf_Last_Chnge_Oprtr);                                                           //Natural: ASSIGN #WF-OPRTR := #WF-LAST-CHNGE-OPRTR
                                    //*          PRINT '=' #WORK-FILE
                                    getWorkFiles().write(1, false, pnd_Work_File);                                                                                        //Natural: WRITE WORK FILE 1 #WORK-FILE
                                    pnd_Write_Count.nadd(1);                                                                                                              //Natural: ADD 1 TO #WRITE-COUNT
                                    if (condition(pnd_Qc_Cnt.equals(1) && pnd_Array_Group_Pnd_Arr_Status_Cde.getValue(pnd_J).equals("7500")))                             //Natural: IF #QC-CNT = 1 AND #ARR-STATUS-CDE ( #J ) = '7500'
                                    {
                                        //*            PRINT '=' #WORK-FILE
                                        getWorkFiles().write(2, false, pnd_Work_File);                                                                                    //Natural: WRITE WORK FILE 2 #WORK-FILE
                                        pnd_Write_Count2.nadd(1);                                                                                                         //Natural: ADD 1 TO #WRITE-COUNT2
                                    }                                                                                                                                     //Natural: END-IF
                                }                                                                                                                                         //Natural: END-IF
                                //*        WRITE '----------------------------'
                            }                                                                                                                                             //Natural: END-FOR
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            //*      WRITE '-----------------------ARRAY END--------------------------'
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Work_File.reset();                                                                                                                                //Natural: RESET #WORK-FILE
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                getReports().write(0, "PRODUCTIVITY REPORT =",pnd_Write_Count,NEWLINE,"QUALITY REPORT      =",pnd_Write_Count2);                                          //Natural: WRITE 'PRODUCTIVITY REPORT =' #WRITE-COUNT / 'QUALITY REPORT      =' #WRITE-COUNT2
                if (Global.isEscape()) return;
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-REQUEST-HISTORY
                //*  IF MIT-HIST.WORK-PRCSS-ID  = MASK('F') OR = MASK('I') OR = MASK('R')
                //*  IF MIT-HIST.WORK-PRCSS-ID  = MASK('T')
                //* ***********************************************************************
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Determine_Request_History() throws Exception                                                                                                         //Natural: DETERMINE-REQUEST-HISTORY
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Arr_Rec_Cnt.reset();                                                                                                                                          //Natural: RESET #ARR-REC-CNT #ARRAY-GROUP ( * )
        pnd_Array_Group.getValue("*").reset();
        vw_mit_Hist.startDatabaseRead                                                                                                                                     //Natural: READ MIT-HIST BY RQST-HISTORY-KEY STARTING FROM #HISTORY-KEY
        (
        "READ03",
        new Wc[] { new Wc("RQST_HISTORY_KEY", ">=", pnd_History_Key, WcType.BY) },
        new Oc[] { new Oc("RQST_HISTORY_KEY", "ASC") }
        );
        READ03:
        while (condition(vw_mit_Hist.readNextRow("READ03")))
        {
            if (condition(mit_Hist_Rqst_Log_Dte_Tme.greater(mit_Rqst_Log_Dte_Tme)))                                                                                       //Natural: IF MIT-HIST.RQST-LOG-DTE-TME > MIT.RQST-LOG-DTE-TME
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(mit_Hist_Pin_Nbr.notEquals(mit_Pin_Nbr)))                                                                                                       //Natural: IF MIT-HIST.PIN-NBR NE MIT.PIN-NBR
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Last_Chnge_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),mit_Hist_Pnd_Last_Chnge_Dte);                                                              //Natural: MOVE EDITED MIT-HIST.#LAST-CHNGE-DTE TO #LAST-CHNGE-DTE-D ( EM = YYYYMMDD )
            if (condition(pnd_Last_Chnge_Dte_D.greaterOrEqual(pnd_End_Datd)))                                                                                             //Natural: IF #LAST-CHNGE-DTE-D GE #END-DATD
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  TESTING
            if (condition(mit_Hist_Work_Prcss_Id.equals(pnd_Wpids_Pnd_T_Wpid.getValue("*"))))                                                                             //Natural: IF MIT-HIST.WORK-PRCSS-ID = #T-WPID ( * )
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(mit_Hist_Orgnl_Unit_Cde.equals(pnd_Units_Pnd_T_Unit.getValue("*")) || mit_Hist_Unit_Cde.equals(pnd_Units_Pnd_T_Unit.getValue("*"))))            //Natural: IF MIT-HIST.ORGNL-UNIT-CDE = #T-UNIT ( * ) OR MIT-HIST.UNIT-CDE = #T-UNIT ( * )
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Arr_Rec_Cnt.nadd(1);                                                                                                                                      //Natural: ASSIGN #ARR-REC-CNT := #ARR-REC-CNT + 1
            pnd_Array_Group_Pnd_Arr_Pin_Nbr.getValue(pnd_Arr_Rec_Cnt).setValue(mit_Hist_Pin_Nbr);                                                                         //Natural: ASSIGN #ARR-PIN-NBR ( #ARR-REC-CNT ) := MIT-HIST.PIN-NBR
            pnd_Array_Group_Pnd_Arr_Wpid.getValue(pnd_Arr_Rec_Cnt).setValue(mit_Hist_Work_Prcss_Id);                                                                      //Natural: ASSIGN #ARR-WPID ( #ARR-REC-CNT ) := MIT-HIST.WORK-PRCSS-ID
            pnd_Array_Group_Pnd_Arr_Status_Cde.getValue(pnd_Arr_Rec_Cnt).setValue(mit_Hist_Status_Cde);                                                                   //Natural: ASSIGN #ARR-STATUS-CDE ( #ARR-REC-CNT ) := MIT-HIST.STATUS-CDE
            pnd_Array_Group_Pnd_Arr_Empl_Oprtr.getValue(pnd_Arr_Rec_Cnt).setValue(mit_Hist_Empl_Oprtr_Cde);                                                               //Natural: ASSIGN #ARR-EMPL-OPRTR ( #ARR-REC-CNT ) := MIT-HIST.EMPL-OPRTR-CDE
            pnd_Array_Group_Pnd_Arr_Last_Chg_Dt.getValue(pnd_Arr_Rec_Cnt).setValue(mit_Hist_Last_Chnge_Dte_Tme);                                                          //Natural: ASSIGN #ARR-LAST-CHG-DT ( #ARR-REC-CNT ) := MIT-HIST.LAST-CHNGE-DTE-TME
            pnd_Array_Group_Pnd_Arr_Last_Chg_Op.getValue(pnd_Arr_Rec_Cnt).setValue(mit_Hist_Last_Chnge_Oprtr_Cde);                                                        //Natural: ASSIGN #ARR-LAST-CHG-OP ( #ARR-REC-CNT ) := MIT-HIST.LAST-CHNGE-OPRTR-CDE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  DETERMINE-REQUEST-HISTORY
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=22 LS=132");
    }
}
