/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:29:01 PM
**        * FROM NATURAL PROGRAM : Cwfb3215
************************************************************
**        * FILE NAME            : Cwfb3215.java
**        * CLASS NAME           : Cwfb3215
**        * INSTANCE NAME        : Cwfb3215
************************************************************
************************************************************************
* PROGRAM  : CWFB3215
* SYSTEM   : CRPCWF
* TITLE    : EXTRACT (ONE LOOP METHOD)
* GENERATED: OCT 05,93 AT 08:41 AM
* FUNCTION : EXTRACT ALL OPEN EXTERNALLY PENDED MASTER INDEX RECORDS
*          | (ACTIVE AND CRPRTE STATUS IND NE 9)
*
************************************************************************
*                    M O D I F I C A T I O N S                         *
************************************************************************
* NAME         DATE    DESCRIPTION                                     *
* ----------- -------- ------------------------------------------------*
* J. HARGRAVE 08/21/95 CHANGE TO READ CORE FILE INSTEAD OF PIF FILE    *
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3215 extends BLNatBase
{
    // Data Areas
    private PdaCwfa5372 pdaCwfa5372;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Master_Index;
    private DbsField cwf_Master_Index_Pin_Nbr;
    private DbsField cwf_Master_Index_Rqst_Log_Dte_Tme;

    private DbsGroup cwf_Master_Index__R_Field_1;
    private DbsField cwf_Master_Index_Rqst_Log_Index_Dte;
    private DbsField cwf_Master_Index_Rqst_Log_Index_Tme;
    private DbsField cwf_Master_Index_Rqst_Log_Oprtr_Cde;
    private DbsField cwf_Master_Index_Rqst_Orgn_Cde;
    private DbsField cwf_Master_Index_Orgnl_Log_Dte_Tme;
    private DbsField cwf_Master_Index_Orgnl_Unit_Cde;
    private DbsField cwf_Master_Index_Work_Prcss_Id;
    private DbsField cwf_Master_Index_Unit_Cde;
    private DbsField cwf_Master_Index_Unit_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_Empl_Oprtr_Cde;
    private DbsField cwf_Master_Index_Last_Chnge_Dte_Tme;
    private DbsField cwf_Master_Index_Last_Chnge_Oprtr_Cde;
    private DbsField cwf_Master_Index_Last_Chnge_Unit_Cde;
    private DbsField cwf_Master_Index_Step_Id;
    private DbsField cwf_Master_Index_Admin_Unit_Cde;
    private DbsField cwf_Master_Index_Admin_Status_Cde;
    private DbsField cwf_Master_Index_Admin_Status_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_Admin_Status_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Index_Status_Cde;
    private DbsField cwf_Master_Index_Status_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_Status_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Index_Last_Updte_Dte;
    private DbsField cwf_Master_Index_Last_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_Last_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Index_Actve_Ind;
    private DbsField cwf_Master_Index_Crprte_Status_Ind;
    private DbsField cwf_Master_Index_Tiaa_Rcvd_Dte;
    private DbsField cwf_Master_Index_Effctve_Dte;
    private DbsField cwf_Master_Index_Trans_Dte;
    private DbsField cwf_Master_Index_Mj_Pull_Ind;
    private DbsField cwf_Master_Index_Physcl_Fldr_Id_Nbr;
    private DbsGroup cwf_Master_Index_Cntrct_NbrMuGroup;
    private DbsField cwf_Master_Index_Cntrct_Nbr;
    private DbsField cwf_Master_Index_Extrnl_Pend_Rcv_Dte;
    private DbsField cwf_Master_Index_Work_List_Ind;
    private DbsField cwf_Master_Index_Due_Dte_Chg_Prty_Cde;

    private DbsGroup cwf_Master_Index__R_Field_2;
    private DbsField cwf_Master_Index_Due_Dte;
    private DbsField cwf_Master_Index_Due_Dte_Chg_Ind;
    private DbsField cwf_Master_Index_Due_Dte_Prty_Cde;
    private DbsField cwf_Master_Index_Due_Dte_Filler;
    private DbsField cwf_Master_Index_Bsnss_Reply_Ind;
    private DbsField cwf_Master_Index_Status_Freeze_Ind;
    private DbsField cwf_Master_Index_Elctrnc_Fldr_Ind;
    private DbsField cwf_Master_Index_Unit_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Index_Unit_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Index_Empl_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Index_Empl_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Index_Intrnl_Pnd_Start_Dte_Tme;
    private DbsField cwf_Master_Index_Intrnl_Pnd_End_Dte_Tme;
    private DbsField cwf_Master_Index_Intrnl_Pnd_Days;
    private DbsField cwf_Master_Index_Step_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Index_Step_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Index_Crprte_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Index_Unit_En_Rte_To_Dte_Tme;

    private DbsGroup extract_Record;
    private DbsField extract_Record_Pin_Nbr;
    private DbsField extract_Record_Work_Prcss_Id;
    private DbsField extract_Record_Empl_Oprtr_Cde;

    private DbsGroup extract_Record__R_Field_3;
    private DbsField extract_Record_Empl_Racf_Id;
    private DbsField extract_Record_Last_Chnge_Dte_Tme;
    private DbsField extract_Record_Last_Chnge_Oprtr_Cde;
    private DbsField extract_Record_Step_Id;
    private DbsField extract_Record_Admin_Unit_Cde;
    private DbsField extract_Record_Unit_Cde;
    private DbsField extract_Record_Status_Cde;
    private DbsField extract_Record_F1;
    private DbsField extract_Record_Last_Chnge_Unit_Cde;
    private DbsField extract_Record_Admin_Status_Cde;
    private DbsField extract_Record_F2;
    private DbsField extract_Record_Admin_Status_Updte_Dte_Tme;
    private DbsField extract_Record_Tiaa_Rcvd_Dte;
    private DbsField extract_Record_Cntrct_Nbr;
    private DbsField extract_Record_Pnd_Calendar_Days_In_Tiaa;
    private DbsField extract_Record_Pnd_Business_Days_In_Tiaa;
    private DbsField extract_Record_Extrnl_Pend_Rcv_Dte;
    private DbsField extract_Record_Pnd_Received_In_Unit;
    private DbsField extract_Record_Pnd_Bsnss_Days_In_Unit;
    private DbsField extract_Record_Pnd_Intrnl_Pend_Days;
    private DbsField extract_Record_Pnd_Extrnl_Pend_Days;
    private DbsField extract_Record_Pnd_Partic_Sname;

    private DbsGroup extract_Record__R_Field_4;
    private DbsField extract_Record_Pnd_Folder_Prefix;
    private DbsField extract_Record_Pnd_Folder_Nbr;
    private DbsField extract_Record_Tbl_Class;
    private DbsField pnd_Env;
    private DbsField pnd_Racf_Id;
    private DbsField pnd_Sub;
    private DbsField pnd_Timx;
    private DbsField pnd_Receive_Date;
    private DbsField pnd_Report_Date;
    private DbsField pnd_Start_Date;
    private DbsField pnd_Report_Date_Time;
    private DbsField pnd_Intrnl_Pend_Date;
    private DbsField pnd_Unit_Start_Date_Time;
    private DbsField pnd_Intrnl_Pend_Daysx;
    private DbsField pnd_Extrnl_Pend_Daysx;
    private DbsField pnd_Bsnss_Days_In_Unitx;
    private DbsField pnd_No_Work_Days;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCwfa5372 = new PdaCwfa5372(localVariables);

        // Local Variables

        vw_cwf_Master_Index = new DataAccessProgramView(new NameInfo("vw_cwf_Master_Index", "CWF-MASTER-INDEX"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        cwf_Master_Index_Pin_Nbr = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "PIN_NBR");
        cwf_Master_Index_Pin_Nbr.setDdmHeader("PIN");
        cwf_Master_Index_Rqst_Log_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Master_Index_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");

        cwf_Master_Index__R_Field_1 = vw_cwf_Master_Index.getRecord().newGroupInGroup("cwf_Master_Index__R_Field_1", "REDEFINE", cwf_Master_Index_Rqst_Log_Dte_Tme);
        cwf_Master_Index_Rqst_Log_Index_Dte = cwf_Master_Index__R_Field_1.newFieldInGroup("cwf_Master_Index_Rqst_Log_Index_Dte", "RQST-LOG-INDEX-DTE", 
            FieldType.STRING, 8);
        cwf_Master_Index_Rqst_Log_Index_Tme = cwf_Master_Index__R_Field_1.newFieldInGroup("cwf_Master_Index_Rqst_Log_Index_Tme", "RQST-LOG-INDEX-TME", 
            FieldType.STRING, 7);
        cwf_Master_Index_Rqst_Log_Oprtr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_LOG_OPRTR_CDE");
        cwf_Master_Index_Rqst_Log_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Master_Index_Rqst_Orgn_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Rqst_Orgn_Cde", "RQST-ORGN-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RQST_ORGN_CDE");
        cwf_Master_Index_Orgnl_Log_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Orgnl_Log_Dte_Tme", "ORGNL-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "ORGNL_LOG_DTE_TME");
        cwf_Master_Index_Orgnl_Log_Dte_Tme.setDdmHeader("ORIGINAL/LOG DTE/TME");
        cwf_Master_Index_Orgnl_Unit_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ORGNL_UNIT_CDE");
        cwf_Master_Index_Orgnl_Unit_Cde.setDdmHeader("LOG/UNIT");
        cwf_Master_Index_Work_Prcss_Id = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        cwf_Master_Index_Work_Prcss_Id.setDdmHeader("WORK/ID");
        cwf_Master_Index_Unit_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "UNIT_CDE");
        cwf_Master_Index_Unit_Cde.setDdmHeader("UNIT/CODE");
        cwf_Master_Index_Unit_Updte_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Unit_Updte_Dte_Tme", "UNIT-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "UNIT_UPDTE_DTE_TME");
        cwf_Master_Index_Unit_Updte_Dte_Tme.setDdmHeader("UNIT UPDATE/DATE-TIME");
        cwf_Master_Index_Empl_Oprtr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "EMPL_OPRTR_CDE");
        cwf_Master_Index_Empl_Oprtr_Cde.setDdmHeader("EMPLOYEE/ID");
        cwf_Master_Index_Last_Chnge_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "LAST_CHNGE_DTE_TME");
        cwf_Master_Index_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");
        cwf_Master_Index_Last_Chnge_Oprtr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Last_Chnge_Oprtr_Cde", "LAST-CHNGE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_CHNGE_OPRTR_CDE");
        cwf_Master_Index_Last_Chnge_Oprtr_Cde.setDdmHeader("LOG CREATE/OPERATOR");
        cwf_Master_Index_Last_Chnge_Unit_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Last_Chnge_Unit_Cde", "LAST-CHNGE-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_CHNGE_UNIT_CDE");
        cwf_Master_Index_Last_Chnge_Unit_Cde.setDdmHeader("LAST/CHANGE/UNIT");
        cwf_Master_Index_Step_Id = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Step_Id", "STEP-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "STEP_ID");
        cwf_Master_Index_Step_Id.setDdmHeader("STEP/ID");
        cwf_Master_Index_Admin_Unit_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ADMIN_UNIT_CDE");
        cwf_Master_Index_Admin_Status_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "ADMIN_STATUS_CDE");
        cwf_Master_Index_Admin_Status_Updte_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Admin_Status_Updte_Dte_Tme", "ADMIN-STATUS-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_DTE_TME");
        cwf_Master_Index_Admin_Status_Updte_Oprtr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Admin_Status_Updte_Oprtr_Cde", 
            "ADMIN-STATUS-UPDTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_OPRTR_CDE");
        cwf_Master_Index_Status_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Status_Cde", "STATUS-CDE", FieldType.STRING, 4, 
            RepeatingFieldStrategy.None, "STATUS_CDE");
        cwf_Master_Index_Status_Updte_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Status_Updte_Dte_Tme", "STATUS-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "STATUS_UPDTE_DTE_TME");
        cwf_Master_Index_Status_Updte_Dte_Tme.setDdmHeader("STATUS UPDATE/DATE-TIME");
        cwf_Master_Index_Status_Updte_Oprtr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Status_Updte_Oprtr_Cde", "STATUS-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "STATUS_UPDTE_OPRTR_CDE");
        cwf_Master_Index_Last_Updte_Dte = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Last_Updte_Dte", "LAST-UPDTE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "LAST_UPDTE_DTE");
        cwf_Master_Index_Last_Updte_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Last_Updte_Dte_Tme", "LAST-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LAST_UPDTE_DTE_TME");
        cwf_Master_Index_Last_Updte_Oprtr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Last_Updte_Oprtr_Cde", "LAST-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_UPDTE_OPRTR_CDE");
        cwf_Master_Index_Actve_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTVE_IND");
        cwf_Master_Index_Crprte_Status_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Crprte_Status_Ind", "CRPRTE-STATUS-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CRPRTE_STATUS_IND");
        cwf_Master_Index_Crprte_Status_Ind.setDdmHeader("CORP/STAT");
        cwf_Master_Index_Tiaa_Rcvd_Dte = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TIAA_RCVD_DTE");
        cwf_Master_Index_Effctve_Dte = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Effctve_Dte", "EFFCTVE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "EFFCTVE_DTE");
        cwf_Master_Index_Trans_Dte = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Trans_Dte", "TRANS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        cwf_Master_Index_Trans_Dte.setDdmHeader("CHECK MAIL/DATE");
        cwf_Master_Index_Mj_Pull_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Mj_Pull_Ind", "MJ-PULL-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "MJ_PULL_IND");
        cwf_Master_Index_Mj_Pull_Ind.setDdmHeader("MJ PULL/IND");
        cwf_Master_Index_Physcl_Fldr_Id_Nbr = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Physcl_Fldr_Id_Nbr", "PHYSCL-FLDR-ID-NBR", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "PHYSCL_FLDR_ID_NBR");
        cwf_Master_Index_Physcl_Fldr_Id_Nbr.setDdmHeader("PHYSICAL FOLDER/ID NUMBER");
        cwf_Master_Index_Cntrct_NbrMuGroup = vw_cwf_Master_Index.getRecord().newGroupInGroup("CWF_MASTER_INDEX_CNTRCT_NBRMuGroup", "CNTRCT_NBRMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "CWF_MASTER_INDEX_CNTRCT_NBR");
        cwf_Master_Index_Cntrct_Nbr = cwf_Master_Index_Cntrct_NbrMuGroup.newFieldArrayInGroup("cwf_Master_Index_Cntrct_Nbr", "CNTRCT-NBR", FieldType.STRING, 
            8, new DbsArrayController(1, 1), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_NBR");
        cwf_Master_Index_Extrnl_Pend_Rcv_Dte = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Extrnl_Pend_Rcv_Dte", "EXTRNL-PEND-RCV-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "EXTRNL_PEND_RCV_DTE");
        cwf_Master_Index_Extrnl_Pend_Rcv_Dte.setDdmHeader("LAST UNPEND DATE");
        cwf_Master_Index_Work_List_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Work_List_Ind", "WORK-LIST-IND", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "WORK_LIST_IND");
        cwf_Master_Index_Work_List_Ind.setDdmHeader("WORK LIST INDICATOR");
        cwf_Master_Index_Due_Dte_Chg_Prty_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Due_Dte_Chg_Prty_Cde", "DUE-DTE-CHG-PRTY-CDE", 
            FieldType.STRING, 16, RepeatingFieldStrategy.None, "DUE_DTE_CHG_PRTY_CDE");
        cwf_Master_Index_Due_Dte_Chg_Prty_Cde.setDdmHeader("DUE DATE CHANGE IND. PRIORITY");

        cwf_Master_Index__R_Field_2 = vw_cwf_Master_Index.getRecord().newGroupInGroup("cwf_Master_Index__R_Field_2", "REDEFINE", cwf_Master_Index_Due_Dte_Chg_Prty_Cde);
        cwf_Master_Index_Due_Dte = cwf_Master_Index__R_Field_2.newFieldInGroup("cwf_Master_Index_Due_Dte", "DUE-DTE", FieldType.STRING, 8);
        cwf_Master_Index_Due_Dte_Chg_Ind = cwf_Master_Index__R_Field_2.newFieldInGroup("cwf_Master_Index_Due_Dte_Chg_Ind", "DUE-DTE-CHG-IND", FieldType.STRING, 
            1);
        cwf_Master_Index_Due_Dte_Prty_Cde = cwf_Master_Index__R_Field_2.newFieldInGroup("cwf_Master_Index_Due_Dte_Prty_Cde", "DUE-DTE-PRTY-CDE", FieldType.STRING, 
            1);
        cwf_Master_Index_Due_Dte_Filler = cwf_Master_Index__R_Field_2.newFieldInGroup("cwf_Master_Index_Due_Dte_Filler", "DUE-DTE-FILLER", FieldType.STRING, 
            6);
        cwf_Master_Index_Bsnss_Reply_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Bsnss_Reply_Ind", "BSNSS-REPLY-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BSNSS_REPLY_IND");
        cwf_Master_Index_Bsnss_Reply_Ind.setDdmHeader("BUSINESS/REPLY");
        cwf_Master_Index_Status_Freeze_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Status_Freeze_Ind", "STATUS-FREEZE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "STATUS_FREEZE_IND");
        cwf_Master_Index_Status_Freeze_Ind.setDdmHeader("FREEZE/STATUS");
        cwf_Master_Index_Elctrnc_Fldr_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Elctrnc_Fldr_Ind", "ELCTRNC-FLDR-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ELCTRNC_FLDR_IND");
        cwf_Master_Index_Elctrnc_Fldr_Ind.setDdmHeader("ELECTRONIC/FOLDER IND");
        cwf_Master_Index_Unit_Clock_Start_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Unit_Clock_Start_Dte_Tme", "UNIT-CLOCK-START-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "UNIT_CLOCK_START_DTE_TME");
        cwf_Master_Index_Unit_Clock_Start_Dte_Tme.setDdmHeader("UNIT CLOCK START DATE TIME");
        cwf_Master_Index_Unit_Clock_End_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Unit_Clock_End_Dte_Tme", "UNIT-CLOCK-END-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "UNIT_CLOCK_END_DTE_TME");
        cwf_Master_Index_Unit_Clock_End_Dte_Tme.setDdmHeader("UNIT CLOCK END DATE TIME");
        cwf_Master_Index_Empl_Clock_Start_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Empl_Clock_Start_Dte_Tme", "EMPL-CLOCK-START-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "EMPL_CLOCK_START_DTE_TME");
        cwf_Master_Index_Empl_Clock_Start_Dte_Tme.setDdmHeader("EMPLOYEE CLOCK START DATE TIME");
        cwf_Master_Index_Empl_Clock_End_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Empl_Clock_End_Dte_Tme", "EMPL-CLOCK-END-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "EMPL_CLOCK_END_DTE_TME");
        cwf_Master_Index_Empl_Clock_End_Dte_Tme.setDdmHeader("EMPLOYEE CLOCK END DATE TIME");
        cwf_Master_Index_Intrnl_Pnd_Start_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Intrnl_Pnd_Start_Dte_Tme", "INTRNL-PND-START-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "INTRNL_PND_START_DTE_TME");
        cwf_Master_Index_Intrnl_Pnd_Start_Dte_Tme.setDdmHeader("ACKNOWLEDGEMENT DATE TIME");
        cwf_Master_Index_Intrnl_Pnd_End_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Intrnl_Pnd_End_Dte_Tme", "INTRNL-PND-END-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "INTRNL_PND_END_DTE_TME");
        cwf_Master_Index_Intrnl_Pnd_End_Dte_Tme.setDdmHeader("INTERNAL PEND END DATE TIME");
        cwf_Master_Index_Intrnl_Pnd_Days = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Intrnl_Pnd_Days", "INTRNL-PND-DAYS", FieldType.PACKED_DECIMAL, 
            5, RepeatingFieldStrategy.None, "INTRNL_PND_DAYS");
        cwf_Master_Index_Intrnl_Pnd_Days.setDdmHeader("INTERNAL PEND DAYS");
        cwf_Master_Index_Step_Clock_Start_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Step_Clock_Start_Dte_Tme", "STEP-CLOCK-START-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "STEP_CLOCK_START_DTE_TME");
        cwf_Master_Index_Step_Clock_Start_Dte_Tme.setDdmHeader("STEP START DATE TIME");
        cwf_Master_Index_Step_Clock_End_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Step_Clock_End_Dte_Tme", "STEP-CLOCK-END-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "STEP_CLOCK_END_DTE_TME");
        cwf_Master_Index_Step_Clock_End_Dte_Tme.setDdmHeader("STEP CLOCK END DATE TIME");
        cwf_Master_Index_Crprte_Clock_End_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Crprte_Clock_End_Dte_Tme", "CRPRTE-CLOCK-END-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "CRPRTE_CLOCK_END_DTE_TME");
        cwf_Master_Index_Crprte_Clock_End_Dte_Tme.setDdmHeader("CORPORATE CLOCK END DATE TIME");
        cwf_Master_Index_Unit_En_Rte_To_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Unit_En_Rte_To_Dte_Tme", "UNIT-EN-RTE-TO-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "UNIT_EN_RTE_TO_DTE_TME");
        cwf_Master_Index_Unit_En_Rte_To_Dte_Tme.setDdmHeader("UNIT EN ROUTE TO DATE TIME");
        registerRecord(vw_cwf_Master_Index);

        extract_Record = localVariables.newGroupInRecord("extract_Record", "EXTRACT-RECORD");
        extract_Record_Pin_Nbr = extract_Record.newFieldInGroup("extract_Record_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12);
        extract_Record_Work_Prcss_Id = extract_Record.newFieldInGroup("extract_Record_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6);
        extract_Record_Empl_Oprtr_Cde = extract_Record.newFieldInGroup("extract_Record_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", FieldType.STRING, 10);

        extract_Record__R_Field_3 = extract_Record.newGroupInGroup("extract_Record__R_Field_3", "REDEFINE", extract_Record_Empl_Oprtr_Cde);
        extract_Record_Empl_Racf_Id = extract_Record__R_Field_3.newFieldInGroup("extract_Record_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 8);
        extract_Record_Last_Chnge_Dte_Tme = extract_Record.newFieldInGroup("extract_Record_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 
            15);
        extract_Record_Last_Chnge_Oprtr_Cde = extract_Record.newFieldInGroup("extract_Record_Last_Chnge_Oprtr_Cde", "LAST-CHNGE-OPRTR-CDE", FieldType.STRING, 
            8);
        extract_Record_Step_Id = extract_Record.newFieldInGroup("extract_Record_Step_Id", "STEP-ID", FieldType.STRING, 6);
        extract_Record_Admin_Unit_Cde = extract_Record.newFieldInGroup("extract_Record_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 8);
        extract_Record_Unit_Cde = extract_Record.newFieldInGroup("extract_Record_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8);
        extract_Record_Status_Cde = extract_Record.newFieldInGroup("extract_Record_Status_Cde", "STATUS-CDE", FieldType.STRING, 4);
        extract_Record_F1 = extract_Record.newFieldInGroup("extract_Record_F1", "F1", FieldType.STRING, 1);
        extract_Record_Last_Chnge_Unit_Cde = extract_Record.newFieldInGroup("extract_Record_Last_Chnge_Unit_Cde", "LAST-CHNGE-UNIT-CDE", FieldType.STRING, 
            8);
        extract_Record_Admin_Status_Cde = extract_Record.newFieldInGroup("extract_Record_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 4);
        extract_Record_F2 = extract_Record.newFieldInGroup("extract_Record_F2", "F2", FieldType.STRING, 1);
        extract_Record_Admin_Status_Updte_Dte_Tme = extract_Record.newFieldInGroup("extract_Record_Admin_Status_Updte_Dte_Tme", "ADMIN-STATUS-UPDTE-DTE-TME", 
            FieldType.TIME);
        extract_Record_Tiaa_Rcvd_Dte = extract_Record.newFieldInGroup("extract_Record_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE);
        extract_Record_Cntrct_Nbr = extract_Record.newFieldInGroup("extract_Record_Cntrct_Nbr", "CNTRCT-NBR", FieldType.STRING, 8);
        extract_Record_Pnd_Calendar_Days_In_Tiaa = extract_Record.newFieldInGroup("extract_Record_Pnd_Calendar_Days_In_Tiaa", "#CALENDAR-DAYS-IN-TIAA", 
            FieldType.NUMERIC, 5);
        extract_Record_Pnd_Business_Days_In_Tiaa = extract_Record.newFieldInGroup("extract_Record_Pnd_Business_Days_In_Tiaa", "#BUSINESS-DAYS-IN-TIAA", 
            FieldType.NUMERIC, 5);
        extract_Record_Extrnl_Pend_Rcv_Dte = extract_Record.newFieldInGroup("extract_Record_Extrnl_Pend_Rcv_Dte", "EXTRNL-PEND-RCV-DTE", FieldType.TIME);
        extract_Record_Pnd_Received_In_Unit = extract_Record.newFieldInGroup("extract_Record_Pnd_Received_In_Unit", "#RECEIVED-IN-UNIT", FieldType.TIME);
        extract_Record_Pnd_Bsnss_Days_In_Unit = extract_Record.newFieldInGroup("extract_Record_Pnd_Bsnss_Days_In_Unit", "#BSNSS-DAYS-IN-UNIT", FieldType.NUMERIC, 
            5, 1);
        extract_Record_Pnd_Intrnl_Pend_Days = extract_Record.newFieldInGroup("extract_Record_Pnd_Intrnl_Pend_Days", "#INTRNL-PEND-DAYS", FieldType.NUMERIC, 
            5, 1);
        extract_Record_Pnd_Extrnl_Pend_Days = extract_Record.newFieldInGroup("extract_Record_Pnd_Extrnl_Pend_Days", "#EXTRNL-PEND-DAYS", FieldType.NUMERIC, 
            5, 1);
        extract_Record_Pnd_Partic_Sname = extract_Record.newFieldInGroup("extract_Record_Pnd_Partic_Sname", "#PARTIC-SNAME", FieldType.STRING, 7);

        extract_Record__R_Field_4 = extract_Record.newGroupInGroup("extract_Record__R_Field_4", "REDEFINE", extract_Record_Pnd_Partic_Sname);
        extract_Record_Pnd_Folder_Prefix = extract_Record__R_Field_4.newFieldInGroup("extract_Record_Pnd_Folder_Prefix", "#FOLDER-PREFIX", FieldType.STRING, 
            1);
        extract_Record_Pnd_Folder_Nbr = extract_Record__R_Field_4.newFieldInGroup("extract_Record_Pnd_Folder_Nbr", "#FOLDER-NBR", FieldType.NUMERIC, 6);
        extract_Record_Tbl_Class = extract_Record.newFieldInGroup("extract_Record_Tbl_Class", "TBL-CLASS", FieldType.STRING, 1);
        pnd_Env = localVariables.newFieldInRecord("pnd_Env", "#ENV", FieldType.STRING, 10);
        pnd_Racf_Id = localVariables.newFieldInRecord("pnd_Racf_Id", "#RACF-ID", FieldType.STRING, 8);
        pnd_Sub = localVariables.newFieldInRecord("pnd_Sub", "#SUB", FieldType.PACKED_DECIMAL, 4);
        pnd_Timx = localVariables.newFieldInRecord("pnd_Timx", "#TIMX", FieldType.TIME);
        pnd_Receive_Date = localVariables.newFieldInRecord("pnd_Receive_Date", "#RECEIVE-DATE", FieldType.DATE);
        pnd_Report_Date = localVariables.newFieldInRecord("pnd_Report_Date", "#REPORT-DATE", FieldType.DATE);
        pnd_Start_Date = localVariables.newFieldInRecord("pnd_Start_Date", "#START-DATE", FieldType.DATE);
        pnd_Report_Date_Time = localVariables.newFieldInRecord("pnd_Report_Date_Time", "#REPORT-DATE-TIME", FieldType.TIME);
        pnd_Intrnl_Pend_Date = localVariables.newFieldInRecord("pnd_Intrnl_Pend_Date", "#INTRNL-PEND-DATE", FieldType.TIME);
        pnd_Unit_Start_Date_Time = localVariables.newFieldInRecord("pnd_Unit_Start_Date_Time", "#UNIT-START-DATE-TIME", FieldType.TIME);
        pnd_Intrnl_Pend_Daysx = localVariables.newFieldInRecord("pnd_Intrnl_Pend_Daysx", "#INTRNL-PEND-DAYSX", FieldType.PACKED_DECIMAL, 19, 1);
        pnd_Extrnl_Pend_Daysx = localVariables.newFieldInRecord("pnd_Extrnl_Pend_Daysx", "#EXTRNL-PEND-DAYSX", FieldType.PACKED_DECIMAL, 19, 1);
        pnd_Bsnss_Days_In_Unitx = localVariables.newFieldInRecord("pnd_Bsnss_Days_In_Unitx", "#BSNSS-DAYS-IN-UNITX", FieldType.PACKED_DECIMAL, 19, 1);
        pnd_No_Work_Days = localVariables.newFieldInRecord("pnd_No_Work_Days", "#NO-WORK-DAYS", FieldType.NUMERIC, 18);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Master_Index.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb3215() throws Exception
    {
        super("Cwfb3215");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        pnd_Timx.setValue(Global.getTIMX());                                                                                                                              //Natural: MOVE *TIMX TO #TIMX
        pnd_Report_Date.setValue(pnd_Timx);                                                                                                                               //Natural: MOVE #TIMX TO #REPORT-DATE
        pnd_Report_Date_Time.setValue(pnd_Timx);                                                                                                                          //Natural: MOVE #TIMX TO #REPORT-DATE-TIME
        //* ******************** START OF MAIN PROGRAM LOGIC **********************
        //* *
        //* *********************************
        //*  COPYCODE : CWFC3000
        //*  FUNCTION : SETUP ENVONMENT MESSAGE
        //*  AUTHOR   : PATINGO, JOSEPH S.
        //* *********************************
        pnd_Env.setValue(Global.getLIBRARY_ID());                                                                                                                         //Natural: MOVE *LIBRARY-ID TO #ENV
        if (condition(pnd_Env.equals("PROJCWF") || pnd_Env.equals("PROJCWF")))                                                                                            //Natural: IF #ENV = 'PROJCWF' OR #ENV = 'PROJCWF'
        {
            pnd_Env.setValue("DEV'T ");                                                                                                                                   //Natural: MOVE 'DEV"T ' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("PRACANN")))                                                                                                                         //Natural: IF #ENV = 'PRACANN'
        {
            pnd_Env.setValue("PRACTICE");                                                                                                                                 //Natural: MOVE 'PRACTICE' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("PRODANN")))                                                                                                                         //Natural: IF #ENV = 'PRODANN'
        {
            pnd_Env.setValue(" ");                                                                                                                                        //Natural: MOVE ' ' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT034")))                                                                                                                         //Natural: IF #ENV = 'ACPT034'
        {
            pnd_Env.setValue("RGN:AT07");                                                                                                                                 //Natural: MOVE 'RGN:AT07' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT023")))                                                                                                                         //Natural: IF #ENV = 'ACPT023'
        {
            pnd_Env.setValue("RGN:AT06");                                                                                                                                 //Natural: MOVE 'RGN:AT06' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT028")))                                                                                                                         //Natural: IF #ENV = 'ACPT028'
        {
            pnd_Env.setValue("RGN:AT05");                                                                                                                                 //Natural: MOVE 'RGN:AT05' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        //* ****** END COPYCODE *************
        if (condition(Global.getLIBRARY_ID().notEquals("PRODANN")))                                                                                                       //Natural: IF *LIBRARY-ID NE 'PRODANN'
        {
            pnd_Env.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "(", pnd_Env, ")"));                                                                         //Natural: COMPRESS '(' #ENV ')' INTO #ENV LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-IF
        //*  MAIN PROGRAM LOOP
        vw_cwf_Master_Index.startDatabaseRead                                                                                                                             //Natural: READ CWF-MASTER-INDEX BY CRPRTE-STATUS-CHNGE-DTE-KEY FROM '0'
        (
        "PROG",
        new Wc[] { new Wc("CRPRTE_STATUS_CHNGE_DTE_KEY", ">=", "0", WcType.BY) },
        new Oc[] { new Oc("CRPRTE_STATUS_CHNGE_DTE_KEY", "ASC") }
        );
        PROG:
        while (condition(vw_cwf_Master_Index.readNextRow("PROG")))
        {
            if (condition(cwf_Master_Index_Crprte_Status_Ind.greater("0")))                                                                                               //Natural: IF CWF-MASTER-INDEX.CRPRTE-STATUS-IND GT '0'
            {
                if (true) break PROG;                                                                                                                                     //Natural: ESCAPE BOTTOM ( PROG. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(((cwf_Master_Index_Status_Cde.greaterOrEqual("4500") && cwf_Master_Index_Status_Cde.lessOrEqual("4997")) || (cwf_Master_Index_Admin_Status_Cde.greaterOrEqual("4500")  //Natural: ACCEPT IF CWF-MASTER-INDEX.STATUS-CDE = '4500' THRU '4997' OR CWF-MASTER-INDEX.ADMIN-STATUS-CDE = '4500' THRU '4997'
                && cwf_Master_Index_Admin_Status_Cde.lessOrEqual("4997"))))))
            {
                continue;
            }
            if (condition(cwf_Master_Index_Rqst_Orgn_Cde.notEquals("J")))                                                                                                 //Natural: IF CWF-MASTER-INDEX.RQST-ORGN-CDE NE 'J'
            {
                //*  ---------------------
                //*  CALENDAR DAYS IN TIAA
                //*  ---------------------
                if (condition(cwf_Master_Index_Extrnl_Pend_Rcv_Dte.greater(getZero())))                                                                                   //Natural: IF CWF-MASTER-INDEX.EXTRNL-PEND-RCV-DTE GT 0
                {
                    pnd_Receive_Date.setValue(cwf_Master_Index_Extrnl_Pend_Rcv_Dte);                                                                                      //Natural: MOVE CWF-MASTER-INDEX.EXTRNL-PEND-RCV-DTE TO #RECEIVE-DATE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Receive_Date.setValue(cwf_Master_Index_Tiaa_Rcvd_Dte);                                                                                            //Natural: MOVE CWF-MASTER-INDEX.TIAA-RCVD-DTE TO #RECEIVE-DATE
                }                                                                                                                                                         //Natural: END-IF
                extract_Record_Pnd_Calendar_Days_In_Tiaa.compute(new ComputeParameters(false, extract_Record_Pnd_Calendar_Days_In_Tiaa), pnd_Report_Date.subtract(pnd_Receive_Date)); //Natural: COMPUTE #CALENDAR-DAYS-IN-TIAA = #REPORT-DATE - #RECEIVE-DATE
                DbsUtil.callnat(Cwfn3901.class , getCurrentProcessState(), pnd_Receive_Date, pnd_Report_Date, pnd_No_Work_Days);                                          //Natural: CALLNAT 'CWFN3901' #RECEIVE-DATE #REPORT-DATE #NO-WORK-DAYS
                if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                extract_Record_Pnd_Business_Days_In_Tiaa.compute(new ComputeParameters(false, extract_Record_Pnd_Business_Days_In_Tiaa), extract_Record_Pnd_Calendar_Days_In_Tiaa.subtract(pnd_No_Work_Days)); //Natural: COMPUTE #BUSINESS-DAYS-IN-TIAA = #CALENDAR-DAYS-IN-TIAA - #NO-WORK-DAYS
                //*  ---------------------
                //*  RECEIVED IN UNIT DATE
                //*  ---------------------
                if (condition(DbsUtil.maskMatches(cwf_Master_Index_Unit_Clock_Start_Dte_Tme,"YYYYMMDD")))                                                                 //Natural: IF CWF-MASTER-INDEX.UNIT-CLOCK-START-DTE-TME = MASK ( YYYYMMDD )
                {
                    extract_Record_Pnd_Received_In_Unit.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),cwf_Master_Index_Unit_Clock_Start_Dte_Tme);                  //Natural: MOVE EDITED CWF-MASTER-INDEX.UNIT-CLOCK-START-DTE-TME TO #RECEIVED-IN-UNIT ( EM = YYYYMMDDHHIISST )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    extract_Record_Pnd_Received_In_Unit.reset();                                                                                                          //Natural: RESET #RECEIVED-IN-UNIT
                }                                                                                                                                                         //Natural: END-IF
                //*  ------------------
                //*  EXTERNAL-PEND-DAYS
                //*  ------------------
                pnd_Extrnl_Pend_Daysx.compute(new ComputeParameters(false, pnd_Extrnl_Pend_Daysx), pnd_Report_Date_Time.subtract(cwf_Master_Index_Admin_Status_Updte_Dte_Tme)); //Natural: COMPUTE #EXTRNL-PEND-DAYSX = #REPORT-DATE-TIME - CWF-MASTER-INDEX.ADMIN-STATUS-UPDTE-DTE-TME
                extract_Record_Pnd_Extrnl_Pend_Days.compute(new ComputeParameters(false, extract_Record_Pnd_Extrnl_Pend_Days), pnd_Extrnl_Pend_Daysx.divide(864000));     //Natural: COMPUTE #EXTRNL-PEND-DAYS = #EXTRNL-PEND-DAYSX / 864000
                //*  ------------------
                //*  INTERNAL-PEND-DAYS
                //*  ------------------
                extract_Record_Pnd_Intrnl_Pend_Days.compute(new ComputeParameters(false, extract_Record_Pnd_Intrnl_Pend_Days), (cwf_Master_Index_Intrnl_Pnd_Days.divide(10))); //Natural: COMPUTE #INTRNL-PEND-DAYS = ( CWF-MASTER-INDEX.INTRNL-PND-DAYS / 10 )
                //*  -------------------------------------------------------------
                //*  BUSINESS DAYS IN UNIT (LESS HOLIDAYS/WEEKENDS/INTERNAL PENDS)
                //*  -------------------------------------------------------------
                //*        COMPUTE HOLIDAYS/WEEKENDS BETWEEN DAYS
                if (condition(cwf_Master_Index_Unit_Clock_Start_Dte_Tme.greater(" ")))                                                                                    //Natural: IF CWF-MASTER-INDEX.UNIT-CLOCK-START-DTE-TME GT ' '
                {
                    pnd_Start_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),cwf_Master_Index_Unit_Clock_Start_Dte_Tme);                                              //Natural: MOVE EDITED CWF-MASTER-INDEX.UNIT-CLOCK-START-DTE-TME TO #START-DATE ( EM = YYYYMMDD )
                    pnd_Unit_Start_Date_Time.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),cwf_Master_Index_Unit_Clock_Start_Dte_Tme);                             //Natural: MOVE EDITED CWF-MASTER-INDEX.UNIT-CLOCK-START-DTE-TME TO #UNIT-START-DATE-TIME ( EM = YYYYMMDDHHIISST )
                    DbsUtil.callnat(Cwfn3901.class , getCurrentProcessState(), pnd_Start_Date, pnd_Report_Date, pnd_No_Work_Days);                                        //Natural: CALLNAT 'CWFN3901' #START-DATE #REPORT-DATE #NO-WORK-DAYS
                    if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                    pnd_Bsnss_Days_In_Unitx.compute(new ComputeParameters(false, pnd_Bsnss_Days_In_Unitx), pnd_Report_Date_Time.subtract(pnd_Unit_Start_Date_Time));      //Natural: COMPUTE #BSNSS-DAYS-IN-UNITX = #REPORT-DATE-TIME - #UNIT-START-DATE-TIME
                    extract_Record_Pnd_Bsnss_Days_In_Unit.compute(new ComputeParameters(false, extract_Record_Pnd_Bsnss_Days_In_Unit), (pnd_Bsnss_Days_In_Unitx.divide(864000)).subtract(extract_Record_Pnd_Intrnl_Pend_Days).subtract(pnd_No_Work_Days)); //Natural: COMPUTE #BSNSS-DAYS-IN-UNIT = ( #BSNSS-DAYS-IN-UNITX / 864000 ) - #INTRNL-PEND-DAYS - #NO-WORK-DAYS
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    extract_Record_Pnd_Bsnss_Days_In_Unit.setValue(0);                                                                                                    //Natural: MOVE 0 TO #BSNSS-DAYS-IN-UNIT
                }                                                                                                                                                         //Natural: END-IF
                //* *****************************************************************
            }                                                                                                                                                             //Natural: END-IF
            //* *
            if (condition(cwf_Master_Index_Mj_Pull_Ind.equals("Y") || cwf_Master_Index_Mj_Pull_Ind.equals("R")))                                                          //Natural: IF CWF-MASTER-INDEX.MJ-PULL-IND = 'Y' OR = 'R'
            {
                extract_Record_Pnd_Folder_Prefix.setValue("M");                                                                                                           //Natural: MOVE 'M' TO #FOLDER-PREFIX
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                extract_Record_Pnd_Folder_Prefix.setValue("F");                                                                                                           //Natural: MOVE 'F' TO #FOLDER-PREFIX
            }                                                                                                                                                             //Natural: END-IF
            extract_Record_Pnd_Folder_Nbr.setValue(cwf_Master_Index_Physcl_Fldr_Id_Nbr);                                                                                  //Natural: MOVE CWF-MASTER-INDEX.PHYSCL-FLDR-ID-NBR TO #FOLDER-NBR
            //* *
            short decideConditionsMet264 = 0;                                                                                                                             //Natural: DECIDE ON EVERY VALUE OF #EXTRNL-PEND-DAYS;//Natural: VALUE 00.0:29.9
            if (condition(extract_Record_Pnd_Extrnl_Pend_Days.greaterOrEqual(0) && extract_Record_Pnd_Extrnl_Pend_Days.lessOrEqual(29)))
            {
                decideConditionsMet264++;
                extract_Record_Tbl_Class.setValue("A");                                                                                                                   //Natural: MOVE 'A' TO TBL-CLASS
            }                                                                                                                                                             //Natural: VALUE 30.0:59.9
            if (condition(extract_Record_Pnd_Extrnl_Pend_Days.greaterOrEqual(30) && extract_Record_Pnd_Extrnl_Pend_Days.lessOrEqual(59)))
            {
                decideConditionsMet264++;
                extract_Record_Tbl_Class.setValue("B");                                                                                                                   //Natural: MOVE 'B' TO TBL-CLASS
            }                                                                                                                                                             //Natural: VALUE 60.0:89.9
            if (condition(extract_Record_Pnd_Extrnl_Pend_Days.greaterOrEqual(60) && extract_Record_Pnd_Extrnl_Pend_Days.lessOrEqual(89)))
            {
                decideConditionsMet264++;
                extract_Record_Tbl_Class.setValue("C");                                                                                                                   //Natural: MOVE 'C' TO TBL-CLASS
            }                                                                                                                                                             //Natural: VALUE 90.0:364.9
            if (condition(extract_Record_Pnd_Extrnl_Pend_Days.greaterOrEqual(90) && extract_Record_Pnd_Extrnl_Pend_Days.lessOrEqual(364)))
            {
                decideConditionsMet264++;
                //*  1 TO < 2 YEARS
                extract_Record_Tbl_Class.setValue("D");                                                                                                                   //Natural: MOVE 'D' TO TBL-CLASS
            }                                                                                                                                                             //Natural: VALUE 365.0:729.9
            if (condition(extract_Record_Pnd_Extrnl_Pend_Days.greaterOrEqual(365) && extract_Record_Pnd_Extrnl_Pend_Days.lessOrEqual(729)))
            {
                decideConditionsMet264++;
                //*  2 TO < 3 YEARS
                extract_Record_Tbl_Class.setValue("E");                                                                                                                   //Natural: MOVE 'E' TO TBL-CLASS
            }                                                                                                                                                             //Natural: VALUE 730.0:1094.9
            if (condition(extract_Record_Pnd_Extrnl_Pend_Days.greaterOrEqual(730) && extract_Record_Pnd_Extrnl_Pend_Days.lessOrEqual(1094)))
            {
                decideConditionsMet264++;
                //*  3 TO < 4 YEARS
                extract_Record_Tbl_Class.setValue("F");                                                                                                                   //Natural: MOVE 'F' TO TBL-CLASS
            }                                                                                                                                                             //Natural: VALUE 1095.0:1459.9
            if (condition(extract_Record_Pnd_Extrnl_Pend_Days.greaterOrEqual(1095) && extract_Record_Pnd_Extrnl_Pend_Days.lessOrEqual(1459)))
            {
                decideConditionsMet264++;
                //*  4 TO < 5 YEARS
                extract_Record_Tbl_Class.setValue("G");                                                                                                                   //Natural: MOVE 'G' TO TBL-CLASS
            }                                                                                                                                                             //Natural: VALUE 1460.0:1824.9
            if (condition(extract_Record_Pnd_Extrnl_Pend_Days.greaterOrEqual(1460) && extract_Record_Pnd_Extrnl_Pend_Days.lessOrEqual(1824)))
            {
                decideConditionsMet264++;
                //*  5 TO < 6 YEARS
                extract_Record_Tbl_Class.setValue("H");                                                                                                                   //Natural: MOVE 'H' TO TBL-CLASS
            }                                                                                                                                                             //Natural: VALUE 1825.0:2189.9
            if (condition(extract_Record_Pnd_Extrnl_Pend_Days.greaterOrEqual(1825) && extract_Record_Pnd_Extrnl_Pend_Days.lessOrEqual(2189)))
            {
                decideConditionsMet264++;
                //*  6 TO < 7 YEARS
                extract_Record_Tbl_Class.setValue("I");                                                                                                                   //Natural: MOVE 'I' TO TBL-CLASS
            }                                                                                                                                                             //Natural: VALUE 2190.0:2554.9
            if (condition(extract_Record_Pnd_Extrnl_Pend_Days.greaterOrEqual(2190) && extract_Record_Pnd_Extrnl_Pend_Days.lessOrEqual(2554)))
            {
                decideConditionsMet264++;
                //*  7 TO < 8 YEARS
                extract_Record_Tbl_Class.setValue("J");                                                                                                                   //Natural: MOVE 'J' TO TBL-CLASS
            }                                                                                                                                                             //Natural: VALUE 2555.0:2919.9
            if (condition(extract_Record_Pnd_Extrnl_Pend_Days.greaterOrEqual(2555) && extract_Record_Pnd_Extrnl_Pend_Days.lessOrEqual(2919)))
            {
                decideConditionsMet264++;
                //*  8 TO < 9 YEARS
                extract_Record_Tbl_Class.setValue("K");                                                                                                                   //Natural: MOVE 'K' TO TBL-CLASS
            }                                                                                                                                                             //Natural: VALUE 2920.0:3284.9
            if (condition(extract_Record_Pnd_Extrnl_Pend_Days.greaterOrEqual(2920) && extract_Record_Pnd_Extrnl_Pend_Days.lessOrEqual(3284)))
            {
                decideConditionsMet264++;
                //*  9 TO < 10 YEARS
                extract_Record_Tbl_Class.setValue("L");                                                                                                                   //Natural: MOVE 'L' TO TBL-CLASS
            }                                                                                                                                                             //Natural: VALUE 3285.0:3649.9
            if (condition(extract_Record_Pnd_Extrnl_Pend_Days.greaterOrEqual(3285) && extract_Record_Pnd_Extrnl_Pend_Days.lessOrEqual(3649)))
            {
                decideConditionsMet264++;
                //*  10 YEARS OR MORE
                extract_Record_Tbl_Class.setValue("M");                                                                                                                   //Natural: MOVE 'M' TO TBL-CLASS
            }                                                                                                                                                             //Natural: VALUE 3650.0:9999.9
            if (condition(extract_Record_Pnd_Extrnl_Pend_Days.greaterOrEqual(3650) && extract_Record_Pnd_Extrnl_Pend_Days.lessOrEqual(9999)))
            {
                decideConditionsMet264++;
                extract_Record_Tbl_Class.setValue("N");                                                                                                                   //Natural: MOVE 'N' TO TBL-CLASS
            }                                                                                                                                                             //Natural: NONE
            if (condition(decideConditionsMet264 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //* *
            extract_Record.setValuesByName(vw_cwf_Master_Index);                                                                                                          //Natural: MOVE BY NAME CWF-MASTER-INDEX TO EXTRACT-RECORD
            //* *
            extract_Record_Pnd_Partic_Sname.reset();                                                                                                                      //Natural: RESET EXTRACT-RECORD.#PARTIC-SNAME
            pdaCwfa5372.getCwfa5372_Pnd_Pin_Key().setValue(extract_Record_Pin_Nbr);                                                                                       //Natural: ASSIGN CWFA5372.#PIN-KEY := EXTRACT-RECORD.PIN-NBR
            DbsUtil.callnat(Cwfn5372.class , getCurrentProcessState(), pdaCwfa5372.getCwfa5372_Pnd_Pin_Key(), pdaCwfa5372.getCwfa5372_Pnd_Pass_Name(),                    //Natural: CALLNAT 'CWFN5372' CWFA5372.#PIN-KEY CWFA5372.#PASS-NAME CWFA5372.#PASS-SSN CWFA5372.#PASS-TLC CWFA5372.#PASS-DOB CWFA5372.#PASS-FOUND
                pdaCwfa5372.getCwfa5372_Pnd_Pass_Ssn(), pdaCwfa5372.getCwfa5372_Pnd_Pass_Tlc(), pdaCwfa5372.getCwfa5372_Pnd_Pass_Dob(), pdaCwfa5372.getCwfa5372_Pnd_Pass_Found());
            if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
            if (condition(pdaCwfa5372.getCwfa5372_Pnd_Pass_Found().getBoolean()))                                                                                         //Natural: IF CWFA5372.#PASS-FOUND
            {
                extract_Record_Pnd_Partic_Sname.setValue(pdaCwfa5372.getCwfa5372_Pnd_Pass_Name());                                                                        //Natural: MOVE CWFA5372.#PASS-NAME TO EXTRACT-RECORD.#PARTIC-SNAME
            }                                                                                                                                                             //Natural: END-IF
            //* *
            getWorkFiles().write(1, false, extract_Record);                                                                                                               //Natural: WRITE WORK FILE 1 EXTRACT-RECORD
            //* *
            extract_Record.reset();                                                                                                                                       //Natural: RESET EXTRACT-RECORD
            //* *
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* *
    }

    //
}
