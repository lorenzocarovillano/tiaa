/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:48:35 PM
**        * FROM NATURAL PROGRAM : Iefp9022
************************************************************
**        * FILE NAME            : Iefp9022.java
**        * CLASS NAME           : Iefp9022
**        * INSTANCE NAME        : Iefp9022
************************************************************
************************************************************************
* PROGRAM  : IEFP9022
* SYSTEM   : CRPCWF
* TITLE    : CONTROL MODULE TRANSACTION AUDIT LIST
* GENERATED: MARCH 10, 1997 AT  10:00 AM
* FUNCTION : THIS PROGRAM PRINTS TRANSACTIONS LOGGED BY THE ICW CONTROL
*          : MODULE. THE TRANSACTIONS WERE SELECTED IN EFSP9020 AND
*          : SORTED BY LOG-DTE-TME.  THE REPORT IS PRODUCED DAILY.
* HISTORY
* ---------------------------------------
* 10/11/95 JHH - READ SORTED WORK FILE RATHER THAN ICW-EFM-AUDIT
************************************************************************
* GLOBAL USING CWFG000

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iefp9022 extends BLNatBase
{
    // Data Areas
    private LdaIefl9013 ldaIefl9013;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Program;
    private DbsField pnd_I;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIefl9013 = new LdaIefl9013();
        registerRecord(ldaIefl9013);
        registerRecord(ldaIefl9013.getVw_icw_Efm_Audit());

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIefl9013.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iefp9022() throws Exception
    {
        super("Iefp9022");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        getReports().definePrinter(2, "REPT");                                                                                                                            //Natural: DEFINE PRINTER ( REPT = 1 )
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: FORMAT ( REPT ) LS = 132 PS = 60 ZP = OFF SG = OFF;//Natural: ASSIGN #PROGRAM = *PROGRAM
        //*  READ THE FOLDER AUDIT WORK FILE AND PRINT THE REPORT
        //*  NEW 10/11/95 - JHH
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 ICW-EFM-AUDIT
        while (condition(getWorkFiles().read(1, ldaIefl9013.getVw_icw_Efm_Audit())))
        {
            if (condition(ldaIefl9013.getIcw_Efm_Audit_Log_Dte_Tme().equals(getZero())))                                                                                  //Natural: REJECT IF ICW-EFM-AUDIT.LOG-DTE-TME = 0
            {
                continue;
            }
            getReports().write(2, ReportOption.NOHDR, writeMapToStringOutput(Ieff9041.class));                                                                            //Natural: WRITE ( REPT ) NOHDR USING FORM 'IEFF9041'
            if (condition(ldaIefl9013.getIcw_Efm_Audit_Count_Castrqst_Id().greater(1)))                                                                                   //Natural: IF ICW-EFM-AUDIT.C*RQST-ID > 1
            {
                FOR01:                                                                                                                                                    //Natural: FOR #I = 2 TO ICW-EFM-AUDIT.C*RQST-ID
                for (pnd_I.setValue(2); condition(pnd_I.lessOrEqual(ldaIefl9013.getIcw_Efm_Audit_Count_Castrqst_Id())); pnd_I.nadd(1))
                {
                    getReports().write(2, ReportOption.NOHDR,new TabSetting(116),ldaIefl9013.getIcw_Efm_Audit_Rqst_Id().getValue(pnd_I));                                 //Natural: WRITE ( REPT ) NOHDR 116T ICW-EFM-AUDIT.RQST-ID ( #I )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( REPT )
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Ieff9040.class));                                              //Natural: WRITE ( REPT ) NOTITLE NOHDR USING FORM 'IEFF9040'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(2, "LS=132 PS=60 ZP=OFF SG=OFF");
    }
}
