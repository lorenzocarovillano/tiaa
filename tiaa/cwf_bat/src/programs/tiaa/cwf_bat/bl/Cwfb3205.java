/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:28:29 PM
**        * FROM NATURAL PROGRAM : Cwfb3205
************************************************************
**        * FILE NAME            : Cwfb3205.java
**        * CLASS NAME           : Cwfb3205
**        * INSTANCE NAME        : Cwfb3205
************************************************************
**SAG GENERATOR: SHELL-TIAA                       VERSION: 3.2.2
**SAG TITLE: REPORT 11-A
**SAG SYSTEM: CRPCWF
************************************************************************
* PROGRAM  : CWFB3203
* SYSTEM   : CRPCWF
* TITLE    : REPORT 11-A
* GENERATED: AUG 11,93 AT 10:46 AM
* FUNCTION : REPORT OF UNIT EFFECT ON TURN AROUND
*          | EXTRACT
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* MMM DD YY  ________ ______________________________________________
* ______________________________________________
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3205 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Wrk;
    private DbsField pnd_Wrk_Admin_Unit_Cde;
    private DbsField pnd_Wrk_Empl_Racf_Id;
    private DbsField pnd_Wrk_Tbl_Calendar_Days;
    private DbsField pnd_Wrk_Tbl_Tiaa_Bsnss_Days;
    private DbsField pnd_Wrk_Tbl_Business_Days;
    private DbsField pnd_Wrk_Tbl_Complete_Days;
    private DbsField pnd_Wrk_Wpid_Action;
    private DbsField pnd_Wrk_Work_Prcss_Id;
    private DbsField pnd_Wrk_Tbl_Status_Key;

    private DbsGroup pnd_Wrk__R_Field_1;
    private DbsField pnd_Wrk_Last_Chnge_Unit_Cde;
    private DbsField pnd_Wrk_Status_Cde;
    private DbsField pnd_Wrk_Tbl_Close_Unit;
    private DbsField pnd_Wrk_Tbl_Pin;
    private DbsField pnd_Wrk_Cntrct_Nbr;
    private DbsField pnd_Wrk_Tbl_Log_Dte_Tme;
    private DbsField pnd_Wrk_Tiaa_Rcvd_Dte;
    private DbsField pnd_Wrk_Admin_Status_Updte_Dte_Tme;
    private DbsField pnd_Wrk_Return_Doc_Rec_Dte_Tme;
    private DbsField pnd_Wrk_Return_Rcvd_Dte_Tme;
    private DbsField pnd_Wrk_Pnd_Last_Chnge_Dte_Tme;
    private DbsField pnd_Wrk_Pnd_Partic_Sname;

    private DbsGroup pnd_Wrk__R_Field_2;
    private DbsField pnd_Wrk_Pnd_Fldr_Prefix;
    private DbsField pnd_Wrk_Pnd_Physcl_Fldr_Id_Nbr;
    private DbsField pnd_Wrk_Rqst_Log_Dte_Tme;
    private DbsField pnd_Wrk_Last_Chnge_Dte_Tme;

    private DbsGroup pnd_Cwfb3013;
    private DbsField pnd_Cwfb3013_Admin_Unit_Cde;
    private DbsField pnd_Cwfb3013_Empl_Racf_Id;
    private DbsField pnd_Cwfb3013_Tbl_Calendar_Days;
    private DbsField pnd_Cwfb3013_Tbl_Tiaa_Bsnss_Days;
    private DbsField pnd_Cwfb3013_Tbl_Business_Days;
    private DbsField pnd_Cwfb3013_Tbl_Complete_Days;
    private DbsField pnd_Cwfb3013_Wpid_Action;
    private DbsField pnd_Cwfb3013_Work_Prcss_Id;
    private DbsField pnd_Cwfb3013_Tbl_Status_Key;

    private DbsGroup pnd_Cwfb3013__R_Field_3;
    private DbsField pnd_Cwfb3013_Last_Chnge_Unit_Cde;
    private DbsField pnd_Cwfb3013_Status_Cde;
    private DbsField pnd_Cwfb3013_Tbl_Close_Unit;
    private DbsField pnd_Cwfb3013_Tbl_Pin;
    private DbsField pnd_Cwfb3013_Cntrct_Nbr;
    private DbsField pnd_Cwfb3013_Tbl_Log_Dte_Tme;
    private DbsField pnd_Cwfb3013_Tiaa_Rcvd_Dte;
    private DbsField pnd_Cwfb3013_Admin_Status_Updte_Dte_Tme;
    private DbsField pnd_Cwfb3013_Return_Doc_Rec_Dte_Tme;
    private DbsField pnd_Cwfb3013_Return_Rcvd_Dte_Tme;
    private DbsField pnd_Cwfb3013_Pnd_Last_Chnge_Dte_Tme;
    private DbsField pnd_Cwfb3013_Pnd_Partic_Sname;

    private DbsGroup pnd_Cwfb3013__R_Field_4;
    private DbsField pnd_Cwfb3013_Pnd_Fldr_Prefix;
    private DbsField pnd_Cwfb3013_Pnd_Physcl_Fldr_Id_Nbr;
    private DbsField pnd_Cwfb3013_Rqst_Log_Dte_Tme;
    private DbsField pnd_Cwfb3013_Last_Chnge_Dte_Tme;

    private DbsGroup pnd_Misc_Parm;
    private DbsField pnd_Misc_Parm_Pnd_Business_Daysx;
    private DbsField pnd_Misc_Parm_Pnd_Read_Count;
    private DbsField pnd_Misc_Parm_Pnd_Actve_Unque_Key;
    private DbsField pnd_Misc_Parm_Pnd_Todays_Time;
    private DbsField pnd_Misc_Parm_Pnd_Return_Code;
    private DbsField pnd_Misc_Parm_Pnd_Return_Msg;
    private DbsField pnd_Misc_Parm_Pnd_Work_Date_D;
    private DbsField pnd_Misc_Parm_Pnd_Confirmed;

    private DataAccessProgramView vw_cwf_Master_Index;
    private DbsField cwf_Master_Index_Pin_Nbr;
    private DbsField cwf_Master_Index_Rqst_Log_Dte_Tme;

    private DbsGroup cwf_Master_Index__R_Field_5;
    private DbsField cwf_Master_Index_Rqst_Log_Index_Dte;
    private DbsField cwf_Master_Index_Rqst_Log_Index_Tme;
    private DbsField cwf_Master_Index_Rqst_Log_Invrt_Dte_Tme;
    private DbsField cwf_Master_Index_Rqst_Log_Oprtr_Cde;
    private DbsField cwf_Master_Index_Rqst_Orgn_Cde;
    private DbsField cwf_Master_Index_Rqst_Rgn_Cde;
    private DbsField cwf_Master_Index_Rqst_Spcl_Dsgntn_Cde;
    private DbsField cwf_Master_Index_Rqst_Brnch_Cde;
    private DbsField cwf_Master_Index_Orgnl_Log_Dte_Tme;
    private DbsField cwf_Master_Index_Sub_Rqst_Ind;
    private DbsField cwf_Master_Index_Case_Id_Cde;

    private DbsGroup cwf_Master_Index__R_Field_6;
    private DbsField cwf_Master_Index_Case_Ind;
    private DbsField cwf_Master_Index_Sub_Rqst_Sqnce_Ind;
    private DbsField cwf_Master_Index_Multi_Rqst_Ind;
    private DbsField cwf_Master_Index_Orgnl_Unit_Cde;
    private DbsField cwf_Master_Index_Work_Prcss_Id;

    private DbsGroup cwf_Master_Index__R_Field_7;
    private DbsField cwf_Master_Index_Work_Actn_Rqstd_Cde;
    private DbsField cwf_Master_Index_Work_Lob_Cmpny_Prdct_Cde;
    private DbsField cwf_Master_Index_Work_Mjr_Bsnss_Prcss_Cde;
    private DbsField cwf_Master_Index_Work_Spcfc_Bsnss_Prcss_Cde;
    private DbsField cwf_Master_Index_Wpid_Vldte_Ind;
    private DbsField cwf_Master_Index_Unit_Cde;

    private DbsGroup cwf_Master_Index__R_Field_8;
    private DbsField cwf_Master_Index_Unit_Id_Cde;
    private DbsField cwf_Master_Index_Unit_Rgn_Cde;
    private DbsField cwf_Master_Index_Unit_Spcl_Dsgntn_Cde;
    private DbsField cwf_Master_Index_Unit_Brnch_Group_Cde;
    private DbsField cwf_Master_Index_Unit_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_Old_Route_Cde;
    private DbsField cwf_Master_Index_Work_Rqst_Prty_Cde;
    private DbsField cwf_Master_Index_Assgn_Sprvsr_Oprtr_Cde;
    private DbsField cwf_Master_Index_Assgn_Dte_Tme;
    private DbsField cwf_Master_Index_Empl_Oprtr_Cde;

    private DbsGroup cwf_Master_Index__R_Field_9;
    private DbsField cwf_Master_Index_Empl_Racf_Id;
    private DbsField cwf_Master_Index_Empl_Sffx_Cde;
    private DbsField cwf_Master_Index_Last_Chnge_Dte_Tme;
    private DbsField cwf_Master_Index_Last_Chnge_Oprtr_Cde;
    private DbsField cwf_Master_Index_Last_Chnge_Invrt_Dte_Tme;
    private DbsField cwf_Master_Index_Last_Chnge_Unit_Cde;
    private DbsField cwf_Master_Index_Step_Id;
    private DbsField cwf_Master_Index_Rt_Sqnce_Nbr;
    private DbsField cwf_Master_Index_Step_Sqnce_Nbr;
    private DbsField cwf_Master_Index_Step_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_Admin_Unit_Cde;
    private DbsField cwf_Master_Index_Admin_Status_Cde;
    private DbsField cwf_Master_Index_Admin_Status_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_Admin_Status_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Index_Status_Cde;
    private DbsField cwf_Master_Index_Status_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_Status_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Index_Last_Updte_Dte;
    private DbsField cwf_Master_Index_Last_Updte_Dte_Tme;
    private DbsField cwf_Master_Index_Last_Updte_Oprtr_Cde;
    private DbsField cwf_Master_Index_Cntct_Orgn_Type_Cde;
    private DbsField cwf_Master_Index_Cntct_Dte_Tme;
    private DbsField cwf_Master_Index_Cntct_Invrt_Dte_Tme;
    private DbsField cwf_Master_Index_Cntct_Oprtr_Id;
    private DbsField cwf_Master_Index_Actve_Ind;
    private DbsField cwf_Master_Index_Crprte_Status_Ind;
    private DbsField cwf_Master_Index_Cnflct_Ind;
    private DbsField cwf_Master_Index_Instn_Cde;
    private DbsField cwf_Master_Index_Rqst_Instn_Cde;
    private DbsField cwf_Master_Index_Check_Ind;
    private DbsField cwf_Master_Index_Tiaa_Rcvd_Dte;
    private DbsField cwf_Master_Index_Rqst_Invrt_Rcvd_Dte_Tme;
    private DbsField cwf_Master_Index_Effctve_Dte;
    private DbsField cwf_Master_Index_Trnsctn_Dte;
    private DbsField cwf_Master_Index_Trans_Dte;
    private DbsField cwf_Master_Index_Physcl_Fldr_Id_Nbr;
    private DbsField cwf_Master_Index_Mj_Chrge_Dte_Tme;
    private DbsField cwf_Master_Index_Mj_Chrge_Oprtr_Cde;
    private DbsField cwf_Master_Index_Mstr_Indx_Actn_Cde;
    private DbsField cwf_Master_Index_Mj_Pull_Ind;
    private DbsField cwf_Master_Index_Final_Close_Out_Dte_Tme;
    private DbsField cwf_Master_Index_Final_Close_Out_Oprtr_Cde;
    private DbsField cwf_Master_Index_Mj_Emrgncy_Rqst_Dte_Tme;
    private DbsField cwf_Master_Index_Mj_Emrgncy_Rqst_Oprtr_Cde;
    private DbsField cwf_Master_Index_Print_Q_Ind;
    private DbsField cwf_Master_Index_Print_Dte_Tme;
    private DbsField cwf_Master_Index_Print_Batch_Id_Nbr;

    private DbsGroup cwf_Master_Index__R_Field_10;
    private DbsField cwf_Master_Index_Print_Prefix_Ind;
    private DbsField cwf_Master_Index_Print_Batch_Nbr;
    private DbsField cwf_Master_Index_Print_Batch_Sqnce_Nbr;
    private DbsField cwf_Master_Index_Print_Batch_Ind;
    private DbsField cwf_Master_Index_Printer_Id_Cde;
    private DbsField cwf_Master_Index_Rescan_Ind;
    private DbsField cwf_Master_Index_Dup_Ind;
    private DbsField cwf_Master_Index_Cmplnt_Ind;
    private DbsGroup cwf_Master_Index_Cntrct_NbrMuGroup;
    private DbsField cwf_Master_Index_Cntrct_Nbr;
    private DbsField cwf_Master_Index_Extrnl_Pend_Rcv_Dte;
    private DbsField cwf_Master_Index_Work_List_Ind;
    private DbsField cwf_Master_Index_Due_Dte_Chg_Prty_Cde;

    private DbsGroup cwf_Master_Index__R_Field_11;
    private DbsField cwf_Master_Index_Due_Dte;
    private DbsField cwf_Master_Index_Due_Dte_Chg_Ind;
    private DbsField cwf_Master_Index_Due_Dte_Prty_Cde;
    private DbsField cwf_Master_Index_Due_Dte_Filler;
    private DbsField cwf_Master_Index_Bsnss_Reply_Ind;
    private DbsField cwf_Master_Index_Status_Freeze_Ind;
    private DbsField cwf_Master_Index_Elctrnc_Fldr_Ind;
    private DbsField cwf_Master_Index_Log_Insttn_Srce_Cde;
    private DbsField cwf_Master_Index_Log_Rqstr_Cde;
    private DbsField cwf_Master_Index_Unit_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Index_Unit_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Index_Empl_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Index_Empl_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Index_Intrnl_Pnd_Start_Dte_Tme;
    private DbsField cwf_Master_Index_Intrnl_Pnd_End_Dte_Tme;
    private DbsField cwf_Master_Index_Intrnl_Pnd_Days;
    private DbsField cwf_Master_Index_Step_Clock_Start_Dte_Tme;
    private DbsField cwf_Master_Index_Step_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Index_Crprte_Clock_End_Dte_Tme;
    private DbsField cwf_Master_Index_Unit_En_Rte_To_Dte_Tme;
    private DbsField cwf_Master_Index_Acknwldgmnt_Cde;
    private DbsField cwf_Master_Index_Acknwldgmnt_Oprtr_Cde;
    private DbsField cwf_Master_Index_Acknwldgmnt_Dte_Tme;
    private DbsField cwf_Master_Index_Cntct_Sheet_Print_Cde;
    private DbsField cwf_Master_Index_Cntct_Sheet_Print_Dte_Tme;
    private DbsField cwf_Master_Index_Cntct_Sheet_Printer_Id_Cde;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_12;
    private DbsField cwf_Support_Tbl_Tbl_Last_Run_Date;
    private DbsField cwf_Support_Tbl_Tbl_Last_Run_Flag;

    private DbsGroup pnd_End_Var;
    private DbsField pnd_End_Var_Pnd_End_Tbl_Key;

    private DbsGroup pnd_End_Var__R_Field_13;
    private DbsField pnd_End_Var_Pnd_End_Tbl_Authrty;
    private DbsField pnd_End_Var_Pnd_End_Tbl_Name;
    private DbsField pnd_End_Var_Pnd_End_Tbl_Code;

    private DbsGroup pnd_Gen_Var;
    private DbsField pnd_Gen_Var_Pnd_Gen_Tbl_Key;

    private DbsGroup pnd_Gen_Var__R_Field_14;
    private DbsField pnd_Gen_Var_Pnd_Gen_Tbl_Authrty;
    private DbsField pnd_Gen_Var_Pnd_Gen_Tbl_Name;
    private DbsField pnd_Gen_Var_Pnd_Gen_Tbl_Code;
    private DbsField pnd_Gen_Var_Pnd_Gen_Tbl_Desc;
    private DbsField pnd_Report_No;
    private DbsField pnd_Racf_Id;
    private DbsField pnd_Floor;
    private DbsField pnd_Bldg;
    private DbsField pnd_Drop_Off;
    private DbsField pnd_Unit_Code;
    private DbsField pnd_Report_Parm;
    private DbsField pnd_Tbl_Run_Flag;
    private DbsField pnd_Parm_Unit;
    private DbsField pnd_Comp_Date;
    private DbsField pnd_Env;
    private DbsField pnd_Unit_Start_Dte_Tme;
    private DbsField pnd_Unit_End_Dte_Tme;
    private DbsField pnd_Start_Date;
    private DbsField pnd_Start_Dte_Tme;
    private DbsField pnd_Start_Dte_Tme_A;

    private DbsGroup pnd_Start_Dte_Tme_A__R_Field_15;
    private DbsField pnd_Start_Dte_Tme_A_Pnd_Start_Dte_Tme_N;
    private DbsField pnd_End_Dte_Tme;
    private DbsField pnd_End_Dte_Tme_A;

    private DbsGroup pnd_End_Dte_Tme_A__R_Field_16;
    private DbsField pnd_End_Dte_Tme_A_Pnd_End_Dte_Tme_N;
    private DbsField pnd_Work_St_Date;
    private DbsField pnd_Work_End_Date;
    private DbsField pnd_Work_Date_Time;

    private DbsGroup pnd_Work_Date_Time__R_Field_17;
    private DbsField pnd_Work_Date_Time_Pnd_Work_Date;
    private DbsField pnd_Work_Date_Time_Pnd_Work_Time;
    private DbsField pnd_Read_Ctr;
    private DbsField pnd_Conv_Ctr;
    private DbsField pnd_Nconv_Ctr;
    private DbsField pnd_Parm_Type;
    private DbsField pnd_Sub;
    private DbsField pnd_Timx;
    private DbsField pnd_Receive_Date;
    private DbsField pnd_Report_Date;
    private DbsField pnd_Status_Updte_Dte;
    private DbsField pnd_Work_Read;
    private DbsField pnd_Work_Write;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Wrk = localVariables.newGroupInRecord("pnd_Wrk", "#WRK");
        pnd_Wrk_Admin_Unit_Cde = pnd_Wrk.newFieldInGroup("pnd_Wrk_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 8);
        pnd_Wrk_Empl_Racf_Id = pnd_Wrk.newFieldInGroup("pnd_Wrk_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 8);
        pnd_Wrk_Tbl_Calendar_Days = pnd_Wrk.newFieldInGroup("pnd_Wrk_Tbl_Calendar_Days", "TBL-CALENDAR-DAYS", FieldType.NUMERIC, 5);
        pnd_Wrk_Tbl_Tiaa_Bsnss_Days = pnd_Wrk.newFieldInGroup("pnd_Wrk_Tbl_Tiaa_Bsnss_Days", "TBL-TIAA-BSNSS-DAYS", FieldType.NUMERIC, 5);
        pnd_Wrk_Tbl_Business_Days = pnd_Wrk.newFieldInGroup("pnd_Wrk_Tbl_Business_Days", "TBL-BUSINESS-DAYS", FieldType.NUMERIC, 5, 1);
        pnd_Wrk_Tbl_Complete_Days = pnd_Wrk.newFieldInGroup("pnd_Wrk_Tbl_Complete_Days", "TBL-COMPLETE-DAYS", FieldType.NUMERIC, 19, 1);
        pnd_Wrk_Wpid_Action = pnd_Wrk.newFieldInGroup("pnd_Wrk_Wpid_Action", "WPID-ACTION", FieldType.STRING, 1);
        pnd_Wrk_Work_Prcss_Id = pnd_Wrk.newFieldInGroup("pnd_Wrk_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6);
        pnd_Wrk_Tbl_Status_Key = pnd_Wrk.newFieldInGroup("pnd_Wrk_Tbl_Status_Key", "TBL-STATUS-KEY", FieldType.STRING, 13);

        pnd_Wrk__R_Field_1 = pnd_Wrk.newGroupInGroup("pnd_Wrk__R_Field_1", "REDEFINE", pnd_Wrk_Tbl_Status_Key);
        pnd_Wrk_Last_Chnge_Unit_Cde = pnd_Wrk__R_Field_1.newFieldInGroup("pnd_Wrk_Last_Chnge_Unit_Cde", "LAST-CHNGE-UNIT-CDE", FieldType.STRING, 8);
        pnd_Wrk_Status_Cde = pnd_Wrk__R_Field_1.newFieldInGroup("pnd_Wrk_Status_Cde", "STATUS-CDE", FieldType.STRING, 4);
        pnd_Wrk_Tbl_Close_Unit = pnd_Wrk.newFieldInGroup("pnd_Wrk_Tbl_Close_Unit", "TBL-CLOSE-UNIT", FieldType.STRING, 8);
        pnd_Wrk_Tbl_Pin = pnd_Wrk.newFieldInGroup("pnd_Wrk_Tbl_Pin", "TBL-PIN", FieldType.NUMERIC, 12);
        pnd_Wrk_Cntrct_Nbr = pnd_Wrk.newFieldInGroup("pnd_Wrk_Cntrct_Nbr", "CNTRCT-NBR", FieldType.STRING, 8);
        pnd_Wrk_Tbl_Log_Dte_Tme = pnd_Wrk.newFieldInGroup("pnd_Wrk_Tbl_Log_Dte_Tme", "TBL-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Wrk_Tiaa_Rcvd_Dte = pnd_Wrk.newFieldInGroup("pnd_Wrk_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE);
        pnd_Wrk_Admin_Status_Updte_Dte_Tme = pnd_Wrk.newFieldInGroup("pnd_Wrk_Admin_Status_Updte_Dte_Tme", "ADMIN-STATUS-UPDTE-DTE-TME", FieldType.TIME);
        pnd_Wrk_Return_Doc_Rec_Dte_Tme = pnd_Wrk.newFieldInGroup("pnd_Wrk_Return_Doc_Rec_Dte_Tme", "RETURN-DOC-REC-DTE-TME", FieldType.TIME);
        pnd_Wrk_Return_Rcvd_Dte_Tme = pnd_Wrk.newFieldInGroup("pnd_Wrk_Return_Rcvd_Dte_Tme", "RETURN-RCVD-DTE-TME", FieldType.TIME);
        pnd_Wrk_Pnd_Last_Chnge_Dte_Tme = pnd_Wrk.newFieldInGroup("pnd_Wrk_Pnd_Last_Chnge_Dte_Tme", "#LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 15);
        pnd_Wrk_Pnd_Partic_Sname = pnd_Wrk.newFieldInGroup("pnd_Wrk_Pnd_Partic_Sname", "#PARTIC-SNAME", FieldType.STRING, 7);

        pnd_Wrk__R_Field_2 = pnd_Wrk.newGroupInGroup("pnd_Wrk__R_Field_2", "REDEFINE", pnd_Wrk_Pnd_Partic_Sname);
        pnd_Wrk_Pnd_Fldr_Prefix = pnd_Wrk__R_Field_2.newFieldInGroup("pnd_Wrk_Pnd_Fldr_Prefix", "#FLDR-PREFIX", FieldType.STRING, 1);
        pnd_Wrk_Pnd_Physcl_Fldr_Id_Nbr = pnd_Wrk__R_Field_2.newFieldInGroup("pnd_Wrk_Pnd_Physcl_Fldr_Id_Nbr", "#PHYSCL-FLDR-ID-NBR", FieldType.NUMERIC, 
            6);
        pnd_Wrk_Rqst_Log_Dte_Tme = pnd_Wrk.newFieldInGroup("pnd_Wrk_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Wrk_Last_Chnge_Dte_Tme = pnd_Wrk.newFieldInGroup("pnd_Wrk_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", FieldType.STRING, 15);

        pnd_Cwfb3013 = localVariables.newGroupInRecord("pnd_Cwfb3013", "#CWFB3013");
        pnd_Cwfb3013_Admin_Unit_Cde = pnd_Cwfb3013.newFieldInGroup("pnd_Cwfb3013_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 8);
        pnd_Cwfb3013_Empl_Racf_Id = pnd_Cwfb3013.newFieldInGroup("pnd_Cwfb3013_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 8);
        pnd_Cwfb3013_Tbl_Calendar_Days = pnd_Cwfb3013.newFieldInGroup("pnd_Cwfb3013_Tbl_Calendar_Days", "TBL-CALENDAR-DAYS", FieldType.NUMERIC, 5);
        pnd_Cwfb3013_Tbl_Tiaa_Bsnss_Days = pnd_Cwfb3013.newFieldInGroup("pnd_Cwfb3013_Tbl_Tiaa_Bsnss_Days", "TBL-TIAA-BSNSS-DAYS", FieldType.NUMERIC, 
            5);
        pnd_Cwfb3013_Tbl_Business_Days = pnd_Cwfb3013.newFieldInGroup("pnd_Cwfb3013_Tbl_Business_Days", "TBL-BUSINESS-DAYS", FieldType.NUMERIC, 5, 1);
        pnd_Cwfb3013_Tbl_Complete_Days = pnd_Cwfb3013.newFieldInGroup("pnd_Cwfb3013_Tbl_Complete_Days", "TBL-COMPLETE-DAYS", FieldType.NUMERIC, 19, 1);
        pnd_Cwfb3013_Wpid_Action = pnd_Cwfb3013.newFieldInGroup("pnd_Cwfb3013_Wpid_Action", "WPID-ACTION", FieldType.STRING, 1);
        pnd_Cwfb3013_Work_Prcss_Id = pnd_Cwfb3013.newFieldInGroup("pnd_Cwfb3013_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6);
        pnd_Cwfb3013_Tbl_Status_Key = pnd_Cwfb3013.newFieldInGroup("pnd_Cwfb3013_Tbl_Status_Key", "TBL-STATUS-KEY", FieldType.STRING, 13);

        pnd_Cwfb3013__R_Field_3 = pnd_Cwfb3013.newGroupInGroup("pnd_Cwfb3013__R_Field_3", "REDEFINE", pnd_Cwfb3013_Tbl_Status_Key);
        pnd_Cwfb3013_Last_Chnge_Unit_Cde = pnd_Cwfb3013__R_Field_3.newFieldInGroup("pnd_Cwfb3013_Last_Chnge_Unit_Cde", "LAST-CHNGE-UNIT-CDE", FieldType.STRING, 
            8);
        pnd_Cwfb3013_Status_Cde = pnd_Cwfb3013__R_Field_3.newFieldInGroup("pnd_Cwfb3013_Status_Cde", "STATUS-CDE", FieldType.STRING, 4);
        pnd_Cwfb3013_Tbl_Close_Unit = pnd_Cwfb3013.newFieldInGroup("pnd_Cwfb3013_Tbl_Close_Unit", "TBL-CLOSE-UNIT", FieldType.STRING, 8);
        pnd_Cwfb3013_Tbl_Pin = pnd_Cwfb3013.newFieldInGroup("pnd_Cwfb3013_Tbl_Pin", "TBL-PIN", FieldType.NUMERIC, 12);
        pnd_Cwfb3013_Cntrct_Nbr = pnd_Cwfb3013.newFieldInGroup("pnd_Cwfb3013_Cntrct_Nbr", "CNTRCT-NBR", FieldType.STRING, 8);
        pnd_Cwfb3013_Tbl_Log_Dte_Tme = pnd_Cwfb3013.newFieldInGroup("pnd_Cwfb3013_Tbl_Log_Dte_Tme", "TBL-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Cwfb3013_Tiaa_Rcvd_Dte = pnd_Cwfb3013.newFieldInGroup("pnd_Cwfb3013_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE);
        pnd_Cwfb3013_Admin_Status_Updte_Dte_Tme = pnd_Cwfb3013.newFieldInGroup("pnd_Cwfb3013_Admin_Status_Updte_Dte_Tme", "ADMIN-STATUS-UPDTE-DTE-TME", 
            FieldType.TIME);
        pnd_Cwfb3013_Return_Doc_Rec_Dte_Tme = pnd_Cwfb3013.newFieldInGroup("pnd_Cwfb3013_Return_Doc_Rec_Dte_Tme", "RETURN-DOC-REC-DTE-TME", FieldType.TIME);
        pnd_Cwfb3013_Return_Rcvd_Dte_Tme = pnd_Cwfb3013.newFieldInGroup("pnd_Cwfb3013_Return_Rcvd_Dte_Tme", "RETURN-RCVD-DTE-TME", FieldType.TIME);
        pnd_Cwfb3013_Pnd_Last_Chnge_Dte_Tme = pnd_Cwfb3013.newFieldInGroup("pnd_Cwfb3013_Pnd_Last_Chnge_Dte_Tme", "#LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 
            15);
        pnd_Cwfb3013_Pnd_Partic_Sname = pnd_Cwfb3013.newFieldInGroup("pnd_Cwfb3013_Pnd_Partic_Sname", "#PARTIC-SNAME", FieldType.STRING, 7);

        pnd_Cwfb3013__R_Field_4 = pnd_Cwfb3013.newGroupInGroup("pnd_Cwfb3013__R_Field_4", "REDEFINE", pnd_Cwfb3013_Pnd_Partic_Sname);
        pnd_Cwfb3013_Pnd_Fldr_Prefix = pnd_Cwfb3013__R_Field_4.newFieldInGroup("pnd_Cwfb3013_Pnd_Fldr_Prefix", "#FLDR-PREFIX", FieldType.STRING, 1);
        pnd_Cwfb3013_Pnd_Physcl_Fldr_Id_Nbr = pnd_Cwfb3013__R_Field_4.newFieldInGroup("pnd_Cwfb3013_Pnd_Physcl_Fldr_Id_Nbr", "#PHYSCL-FLDR-ID-NBR", FieldType.NUMERIC, 
            6);
        pnd_Cwfb3013_Rqst_Log_Dte_Tme = pnd_Cwfb3013.newFieldInGroup("pnd_Cwfb3013_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Cwfb3013_Last_Chnge_Dte_Tme = pnd_Cwfb3013.newFieldInGroup("pnd_Cwfb3013_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", FieldType.STRING, 15);

        pnd_Misc_Parm = localVariables.newGroupInRecord("pnd_Misc_Parm", "#MISC-PARM");
        pnd_Misc_Parm_Pnd_Business_Daysx = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Business_Daysx", "#BUSINESS-DAYSX", FieldType.NUMERIC, 19, 
            1);
        pnd_Misc_Parm_Pnd_Read_Count = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Read_Count", "#READ-COUNT", FieldType.NUMERIC, 5);
        pnd_Misc_Parm_Pnd_Actve_Unque_Key = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Actve_Unque_Key", "#ACTVE-UNQUE-KEY", FieldType.STRING, 15);
        pnd_Misc_Parm_Pnd_Todays_Time = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Todays_Time", "#TODAYS-TIME", FieldType.TIME);
        pnd_Misc_Parm_Pnd_Return_Code = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Return_Code", "#RETURN-CODE", FieldType.NUMERIC, 1);
        pnd_Misc_Parm_Pnd_Return_Msg = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Return_Msg", "#RETURN-MSG", FieldType.STRING, 20);
        pnd_Misc_Parm_Pnd_Work_Date_D = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Work_Date_D", "#WORK-DATE-D", FieldType.DATE);
        pnd_Misc_Parm_Pnd_Confirmed = pnd_Misc_Parm.newFieldInGroup("pnd_Misc_Parm_Pnd_Confirmed", "#CONFIRMED", FieldType.BOOLEAN, 1);

        vw_cwf_Master_Index = new DataAccessProgramView(new NameInfo("vw_cwf_Master_Index", "CWF-MASTER-INDEX"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        cwf_Master_Index_Pin_Nbr = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "PIN_NBR");
        cwf_Master_Index_Pin_Nbr.setDdmHeader("PIN");
        cwf_Master_Index_Rqst_Log_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Master_Index_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");

        cwf_Master_Index__R_Field_5 = vw_cwf_Master_Index.getRecord().newGroupInGroup("cwf_Master_Index__R_Field_5", "REDEFINE", cwf_Master_Index_Rqst_Log_Dte_Tme);
        cwf_Master_Index_Rqst_Log_Index_Dte = cwf_Master_Index__R_Field_5.newFieldInGroup("cwf_Master_Index_Rqst_Log_Index_Dte", "RQST-LOG-INDEX-DTE", 
            FieldType.STRING, 8);
        cwf_Master_Index_Rqst_Log_Index_Tme = cwf_Master_Index__R_Field_5.newFieldInGroup("cwf_Master_Index_Rqst_Log_Index_Tme", "RQST-LOG-INDEX-TME", 
            FieldType.STRING, 7);
        cwf_Master_Index_Rqst_Log_Invrt_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Rqst_Log_Invrt_Dte_Tme", "RQST-LOG-INVRT-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "RQST_LOG_INVRT_DTE_TME");
        cwf_Master_Index_Rqst_Log_Oprtr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_LOG_OPRTR_CDE");
        cwf_Master_Index_Rqst_Log_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Master_Index_Rqst_Orgn_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Rqst_Orgn_Cde", "RQST-ORGN-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RQST_ORGN_CDE");
        cwf_Master_Index_Rqst_Rgn_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Rqst_Rgn_Cde", "RQST-RGN-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RQST_RGN_CDE");
        cwf_Master_Index_Rqst_Spcl_Dsgntn_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Rqst_Spcl_Dsgntn_Cde", "RQST-SPCL-DSGNTN-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_SPCL_DSGNTN_CDE");
        cwf_Master_Index_Rqst_Brnch_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Rqst_Brnch_Cde", "RQST-BRNCH-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RQST_BRNCH_CDE");
        cwf_Master_Index_Orgnl_Log_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Orgnl_Log_Dte_Tme", "ORGNL-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "ORGNL_LOG_DTE_TME");
        cwf_Master_Index_Orgnl_Log_Dte_Tme.setDdmHeader("ORIGINAL/LOG DTE/TME");
        cwf_Master_Index_Sub_Rqst_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Sub_Rqst_Ind", "SUB-RQST-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "SUB_RQST_IND");
        cwf_Master_Index_Sub_Rqst_Ind.setDdmHeader("SUB-REQUEST COUNT");
        cwf_Master_Index_Case_Id_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Case_Id_Cde", "CASE-ID-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CASE_ID_CDE");
        cwf_Master_Index_Case_Id_Cde.setDdmHeader("CASE/ID");

        cwf_Master_Index__R_Field_6 = vw_cwf_Master_Index.getRecord().newGroupInGroup("cwf_Master_Index__R_Field_6", "REDEFINE", cwf_Master_Index_Case_Id_Cde);
        cwf_Master_Index_Case_Ind = cwf_Master_Index__R_Field_6.newFieldInGroup("cwf_Master_Index_Case_Ind", "CASE-IND", FieldType.STRING, 1);
        cwf_Master_Index_Sub_Rqst_Sqnce_Ind = cwf_Master_Index__R_Field_6.newFieldInGroup("cwf_Master_Index_Sub_Rqst_Sqnce_Ind", "SUB-RQST-SQNCE-IND", 
            FieldType.STRING, 1);
        cwf_Master_Index_Multi_Rqst_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Multi_Rqst_Ind", "MULTI-RQST-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "MULTI_RQST_IND");
        cwf_Master_Index_Multi_Rqst_Ind.setDdmHeader("MULTI/IND");
        cwf_Master_Index_Orgnl_Unit_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ORGNL_UNIT_CDE");
        cwf_Master_Index_Orgnl_Unit_Cde.setDdmHeader("LOG/UNIT");
        cwf_Master_Index_Work_Prcss_Id = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        cwf_Master_Index_Work_Prcss_Id.setDdmHeader("WORK/ID");

        cwf_Master_Index__R_Field_7 = vw_cwf_Master_Index.getRecord().newGroupInGroup("cwf_Master_Index__R_Field_7", "REDEFINE", cwf_Master_Index_Work_Prcss_Id);
        cwf_Master_Index_Work_Actn_Rqstd_Cde = cwf_Master_Index__R_Field_7.newFieldInGroup("cwf_Master_Index_Work_Actn_Rqstd_Cde", "WORK-ACTN-RQSTD-CDE", 
            FieldType.STRING, 1);
        cwf_Master_Index_Work_Lob_Cmpny_Prdct_Cde = cwf_Master_Index__R_Field_7.newFieldInGroup("cwf_Master_Index_Work_Lob_Cmpny_Prdct_Cde", "WORK-LOB-CMPNY-PRDCT-CDE", 
            FieldType.STRING, 2);
        cwf_Master_Index_Work_Mjr_Bsnss_Prcss_Cde = cwf_Master_Index__R_Field_7.newFieldInGroup("cwf_Master_Index_Work_Mjr_Bsnss_Prcss_Cde", "WORK-MJR-BSNSS-PRCSS-CDE", 
            FieldType.STRING, 1);
        cwf_Master_Index_Work_Spcfc_Bsnss_Prcss_Cde = cwf_Master_Index__R_Field_7.newFieldInGroup("cwf_Master_Index_Work_Spcfc_Bsnss_Prcss_Cde", "WORK-SPCFC-BSNSS-PRCSS-CDE", 
            FieldType.STRING, 2);
        cwf_Master_Index_Wpid_Vldte_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Wpid_Vldte_Ind", "WPID-VLDTE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "WPID_VLDTE_IND");
        cwf_Master_Index_Unit_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "UNIT_CDE");
        cwf_Master_Index_Unit_Cde.setDdmHeader("UNIT/CODE");

        cwf_Master_Index__R_Field_8 = vw_cwf_Master_Index.getRecord().newGroupInGroup("cwf_Master_Index__R_Field_8", "REDEFINE", cwf_Master_Index_Unit_Cde);
        cwf_Master_Index_Unit_Id_Cde = cwf_Master_Index__R_Field_8.newFieldInGroup("cwf_Master_Index_Unit_Id_Cde", "UNIT-ID-CDE", FieldType.STRING, 5);
        cwf_Master_Index_Unit_Rgn_Cde = cwf_Master_Index__R_Field_8.newFieldInGroup("cwf_Master_Index_Unit_Rgn_Cde", "UNIT-RGN-CDE", FieldType.STRING, 
            1);
        cwf_Master_Index_Unit_Spcl_Dsgntn_Cde = cwf_Master_Index__R_Field_8.newFieldInGroup("cwf_Master_Index_Unit_Spcl_Dsgntn_Cde", "UNIT-SPCL-DSGNTN-CDE", 
            FieldType.STRING, 1);
        cwf_Master_Index_Unit_Brnch_Group_Cde = cwf_Master_Index__R_Field_8.newFieldInGroup("cwf_Master_Index_Unit_Brnch_Group_Cde", "UNIT-BRNCH-GROUP-CDE", 
            FieldType.STRING, 1);
        cwf_Master_Index_Unit_Updte_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Unit_Updte_Dte_Tme", "UNIT-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "UNIT_UPDTE_DTE_TME");
        cwf_Master_Index_Unit_Updte_Dte_Tme.setDdmHeader("UNIT UPDATE/DATE-TIME");
        cwf_Master_Index_Old_Route_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Old_Route_Cde", "OLD-ROUTE-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "OLD_ROUTE_CDE");
        cwf_Master_Index_Old_Route_Cde.setDdmHeader("ACTIVITY-END/STATUS");
        cwf_Master_Index_Work_Rqst_Prty_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Work_Rqst_Prty_Cde", "WORK-RQST-PRTY-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "WORK_RQST_PRTY_CDE");
        cwf_Master_Index_Work_Rqst_Prty_Cde.setDdmHeader("PRIO");
        cwf_Master_Index_Assgn_Sprvsr_Oprtr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Assgn_Sprvsr_Oprtr_Cde", "ASSGN-SPRVSR-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ASSGN_SPRVSR_OPRTR_CDE");
        cwf_Master_Index_Assgn_Sprvsr_Oprtr_Cde.setDdmHeader("SUPER/VISOR");
        cwf_Master_Index_Assgn_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Assgn_Dte_Tme", "ASSGN-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ASSGN_DTE_TME");
        cwf_Master_Index_Assgn_Dte_Tme.setDdmHeader("ASSIGNMENT/DATE-TIME");
        cwf_Master_Index_Empl_Oprtr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "EMPL_OPRTR_CDE");
        cwf_Master_Index_Empl_Oprtr_Cde.setDdmHeader("EMPLOYEE/ID");

        cwf_Master_Index__R_Field_9 = vw_cwf_Master_Index.getRecord().newGroupInGroup("cwf_Master_Index__R_Field_9", "REDEFINE", cwf_Master_Index_Empl_Oprtr_Cde);
        cwf_Master_Index_Empl_Racf_Id = cwf_Master_Index__R_Field_9.newFieldInGroup("cwf_Master_Index_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 
            8);
        cwf_Master_Index_Empl_Sffx_Cde = cwf_Master_Index__R_Field_9.newFieldInGroup("cwf_Master_Index_Empl_Sffx_Cde", "EMPL-SFFX-CDE", FieldType.STRING, 
            2);
        cwf_Master_Index_Last_Chnge_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "LAST_CHNGE_DTE_TME");
        cwf_Master_Index_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");
        cwf_Master_Index_Last_Chnge_Oprtr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Last_Chnge_Oprtr_Cde", "LAST-CHNGE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_CHNGE_OPRTR_CDE");
        cwf_Master_Index_Last_Chnge_Oprtr_Cde.setDdmHeader("LOG CREATE/OPERATOR");
        cwf_Master_Index_Last_Chnge_Invrt_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Last_Chnge_Invrt_Dte_Tme", "LAST-CHNGE-INVRT-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "LAST_CHNGE_INVRT_DTE_TME");
        cwf_Master_Index_Last_Chnge_Unit_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Last_Chnge_Unit_Cde", "LAST-CHNGE-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_CHNGE_UNIT_CDE");
        cwf_Master_Index_Last_Chnge_Unit_Cde.setDdmHeader("LAST/CHANGE/UNIT");
        cwf_Master_Index_Step_Id = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Step_Id", "STEP-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "STEP_ID");
        cwf_Master_Index_Step_Id.setDdmHeader("STEP/ID");
        cwf_Master_Index_Rt_Sqnce_Nbr = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Rt_Sqnce_Nbr", "RT-SQNCE-NBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "RT_SQNCE_NBR");
        cwf_Master_Index_Rt_Sqnce_Nbr.setDdmHeader("ROUTE/SEQ.");
        cwf_Master_Index_Step_Sqnce_Nbr = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Step_Sqnce_Nbr", "STEP-SQNCE-NBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "STEP_SQNCE_NBR");
        cwf_Master_Index_Step_Updte_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Step_Updte_Dte_Tme", "STEP-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "STEP_UPDTE_DTE_TME");
        cwf_Master_Index_Step_Updte_Dte_Tme.setDdmHeader("STEP UPDATE/DATE-TIME");
        cwf_Master_Index_Admin_Unit_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ADMIN_UNIT_CDE");
        cwf_Master_Index_Admin_Status_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "ADMIN_STATUS_CDE");
        cwf_Master_Index_Admin_Status_Updte_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Admin_Status_Updte_Dte_Tme", "ADMIN-STATUS-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_DTE_TME");
        cwf_Master_Index_Admin_Status_Updte_Oprtr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Admin_Status_Updte_Oprtr_Cde", 
            "ADMIN-STATUS-UPDTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "ADMIN_STATUS_UPDTE_OPRTR_CDE");
        cwf_Master_Index_Status_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Status_Cde", "STATUS-CDE", FieldType.STRING, 4, 
            RepeatingFieldStrategy.None, "STATUS_CDE");
        cwf_Master_Index_Status_Updte_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Status_Updte_Dte_Tme", "STATUS-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "STATUS_UPDTE_DTE_TME");
        cwf_Master_Index_Status_Updte_Dte_Tme.setDdmHeader("STATUS UPDATE/DATE-TIME");
        cwf_Master_Index_Status_Updte_Oprtr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Status_Updte_Oprtr_Cde", "STATUS-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "STATUS_UPDTE_OPRTR_CDE");
        cwf_Master_Index_Last_Updte_Dte = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Last_Updte_Dte", "LAST-UPDTE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "LAST_UPDTE_DTE");
        cwf_Master_Index_Last_Updte_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Last_Updte_Dte_Tme", "LAST-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LAST_UPDTE_DTE_TME");
        cwf_Master_Index_Last_Updte_Oprtr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Last_Updte_Oprtr_Cde", "LAST-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_UPDTE_OPRTR_CDE");
        cwf_Master_Index_Cntct_Orgn_Type_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Cntct_Orgn_Type_Cde", "CNTCT-ORGN-TYPE-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTCT_ORGN_TYPE_CDE");
        cwf_Master_Index_Cntct_Orgn_Type_Cde.setDdmHeader("CONTACT/CODE");
        cwf_Master_Index_Cntct_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Cntct_Dte_Tme", "CNTCT-DTE-TME", FieldType.NUMERIC, 
            15, RepeatingFieldStrategy.None, "CNTCT_DTE_TME");
        cwf_Master_Index_Cntct_Invrt_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Cntct_Invrt_Dte_Tme", "CNTCT-INVRT-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "CNTCT_INVRT_DTE_TME");
        cwf_Master_Index_Cntct_Oprtr_Id = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Cntct_Oprtr_Id", "CNTCT-OPRTR-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CNTCT_OPRTR_ID");
        cwf_Master_Index_Actve_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTVE_IND");
        cwf_Master_Index_Crprte_Status_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Crprte_Status_Ind", "CRPRTE-STATUS-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CRPRTE_STATUS_IND");
        cwf_Master_Index_Crprte_Status_Ind.setDdmHeader("CORP/STAT");
        cwf_Master_Index_Cnflct_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Cnflct_Ind", "CNFLCT-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "CNFLCT_IND");
        cwf_Master_Index_Cnflct_Ind.setDdmHeader("CONFLICT/REQUEST");
        cwf_Master_Index_Instn_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Instn_Cde", "INSTN-CDE", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "INSTN_CDE");
        cwf_Master_Index_Rqst_Instn_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Rqst_Instn_Cde", "RQST-INSTN-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "RQST_INSTN_CDE");
        cwf_Master_Index_Check_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Check_Ind", "CHECK-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CHECK_IND");
        cwf_Master_Index_Check_Ind.setDdmHeader("CHK");
        cwf_Master_Index_Tiaa_Rcvd_Dte = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TIAA_RCVD_DTE");
        cwf_Master_Index_Rqst_Invrt_Rcvd_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Rqst_Invrt_Rcvd_Dte_Tme", "RQST-INVRT-RCVD-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "RQST_INVRT_RCVD_DTE_TME");
        cwf_Master_Index_Effctve_Dte = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Effctve_Dte", "EFFCTVE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "EFFCTVE_DTE");
        cwf_Master_Index_Trnsctn_Dte = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Trnsctn_Dte", "TRNSCTN-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TRNSCTN_DTE");
        cwf_Master_Index_Trnsctn_Dte.setDdmHeader("TRANSACTION/DATE");
        cwf_Master_Index_Trans_Dte = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Trans_Dte", "TRANS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        cwf_Master_Index_Trans_Dte.setDdmHeader("CHECK MAIL/DATE");
        cwf_Master_Index_Physcl_Fldr_Id_Nbr = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Physcl_Fldr_Id_Nbr", "PHYSCL-FLDR-ID-NBR", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "PHYSCL_FLDR_ID_NBR");
        cwf_Master_Index_Physcl_Fldr_Id_Nbr.setDdmHeader("PHYSICAL FOLDER/ID NUMBER");
        cwf_Master_Index_Mj_Chrge_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Mj_Chrge_Dte_Tme", "MJ-CHRGE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "MJ_CHRGE_DTE_TME");
        cwf_Master_Index_Mj_Chrge_Dte_Tme.setDdmHeader("CHARGEOUT/DATE-TIME");
        cwf_Master_Index_Mj_Chrge_Oprtr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Mj_Chrge_Oprtr_Cde", "MJ-CHRGE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "MJ_CHRGE_OPRTR_CDE");
        cwf_Master_Index_Mj_Chrge_Oprtr_Cde.setDdmHeader("CHARGEOUT/OPERATOR");
        cwf_Master_Index_Mstr_Indx_Actn_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Mstr_Indx_Actn_Cde", "MSTR-INDX-ACTN-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "MSTR_INDX_ACTN_CDE");
        cwf_Master_Index_Mstr_Indx_Actn_Cde.setDdmHeader("CHARGEOUT/ACTION CODE");
        cwf_Master_Index_Mj_Pull_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Mj_Pull_Ind", "MJ-PULL-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "MJ_PULL_IND");
        cwf_Master_Index_Mj_Pull_Ind.setDdmHeader("MJ PULL/IND");
        cwf_Master_Index_Final_Close_Out_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Final_Close_Out_Dte_Tme", "FINAL-CLOSE-OUT-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "FINAL_CLOSE_OUT_DTE_TME");
        cwf_Master_Index_Final_Close_Out_Dte_Tme.setDdmHeader("FINAL/CLOSEOUT/DATE-TIME");
        cwf_Master_Index_Final_Close_Out_Oprtr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Final_Close_Out_Oprtr_Cde", "FINAL-CLOSE-OUT-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "FINAL_CLOSE_OUT_OPRTR_CDE");
        cwf_Master_Index_Mj_Emrgncy_Rqst_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Mj_Emrgncy_Rqst_Dte_Tme", "MJ-EMRGNCY-RQST-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "MJ_EMRGNCY_RQST_DTE_TME");
        cwf_Master_Index_Mj_Emrgncy_Rqst_Dte_Tme.setDdmHeader("EMERGENCY REQ/DATE-TIME");
        cwf_Master_Index_Mj_Emrgncy_Rqst_Oprtr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Mj_Emrgncy_Rqst_Oprtr_Cde", "MJ-EMRGNCY-RQST-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "MJ_EMRGNCY_RQST_OPRTR_CDE");
        cwf_Master_Index_Mj_Emrgncy_Rqst_Oprtr_Cde.setDdmHeader("EMERGENCY/REQUESTOR");
        cwf_Master_Index_Print_Q_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Print_Q_Ind", "PRINT-Q-IND", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "PRINT_Q_IND");
        cwf_Master_Index_Print_Q_Ind.setDdmHeader("MJ PRINT/QUEUE");
        cwf_Master_Index_Print_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Print_Dte_Tme", "PRINT-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "PRINT_DTE_TME");
        cwf_Master_Index_Print_Dte_Tme.setDdmHeader("MJ PRINT/DATE-TIME");
        cwf_Master_Index_Print_Batch_Id_Nbr = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Print_Batch_Id_Nbr", "PRINT-BATCH-ID-NBR", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "PRINT_BATCH_ID_NBR");

        cwf_Master_Index__R_Field_10 = vw_cwf_Master_Index.getRecord().newGroupInGroup("cwf_Master_Index__R_Field_10", "REDEFINE", cwf_Master_Index_Print_Batch_Id_Nbr);
        cwf_Master_Index_Print_Prefix_Ind = cwf_Master_Index__R_Field_10.newFieldInGroup("cwf_Master_Index_Print_Prefix_Ind", "PRINT-PREFIX-IND", FieldType.STRING, 
            1);
        cwf_Master_Index_Print_Batch_Nbr = cwf_Master_Index__R_Field_10.newFieldInGroup("cwf_Master_Index_Print_Batch_Nbr", "PRINT-BATCH-NBR", FieldType.NUMERIC, 
            8);
        cwf_Master_Index_Print_Batch_Sqnce_Nbr = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Print_Batch_Sqnce_Nbr", "PRINT-BATCH-SQNCE-NBR", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "PRINT_BATCH_SQNCE_NBR");
        cwf_Master_Index_Print_Batch_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Print_Batch_Ind", "PRINT-BATCH-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PRINT_BATCH_IND");
        cwf_Master_Index_Printer_Id_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Printer_Id_Cde", "PRINTER-ID-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "PRINTER_ID_CDE");
        cwf_Master_Index_Rescan_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Rescan_Ind", "RESCAN-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "RESCAN_IND");
        cwf_Master_Index_Rescan_Ind.setDdmHeader("RE-SCAN/INDICATOR");
        cwf_Master_Index_Dup_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Dup_Ind", "DUP-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "DUP_IND");
        cwf_Master_Index_Dup_Ind.setDdmHeader("DUP/IND");
        cwf_Master_Index_Cmplnt_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Cmplnt_Ind", "CMPLNT-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "CMPLNT_IND");
        cwf_Master_Index_Cntrct_NbrMuGroup = vw_cwf_Master_Index.getRecord().newGroupInGroup("CWF_MASTER_INDEX_CNTRCT_NBRMuGroup", "CNTRCT_NBRMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "CWF_MASTER_INDEX_CNTRCT_NBR");
        cwf_Master_Index_Cntrct_Nbr = cwf_Master_Index_Cntrct_NbrMuGroup.newFieldArrayInGroup("cwf_Master_Index_Cntrct_Nbr", "CNTRCT-NBR", FieldType.STRING, 
            8, new DbsArrayController(1, 10), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_NBR");
        cwf_Master_Index_Extrnl_Pend_Rcv_Dte = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Extrnl_Pend_Rcv_Dte", "EXTRNL-PEND-RCV-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "EXTRNL_PEND_RCV_DTE");
        cwf_Master_Index_Extrnl_Pend_Rcv_Dte.setDdmHeader("LAST UNPEND DATE");
        cwf_Master_Index_Work_List_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Work_List_Ind", "WORK-LIST-IND", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "WORK_LIST_IND");
        cwf_Master_Index_Work_List_Ind.setDdmHeader("WORK LIST INDICATOR");
        cwf_Master_Index_Due_Dte_Chg_Prty_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Due_Dte_Chg_Prty_Cde", "DUE-DTE-CHG-PRTY-CDE", 
            FieldType.STRING, 16, RepeatingFieldStrategy.None, "DUE_DTE_CHG_PRTY_CDE");
        cwf_Master_Index_Due_Dte_Chg_Prty_Cde.setDdmHeader("DUE DATE CHANGE IND. PRIORITY");

        cwf_Master_Index__R_Field_11 = vw_cwf_Master_Index.getRecord().newGroupInGroup("cwf_Master_Index__R_Field_11", "REDEFINE", cwf_Master_Index_Due_Dte_Chg_Prty_Cde);
        cwf_Master_Index_Due_Dte = cwf_Master_Index__R_Field_11.newFieldInGroup("cwf_Master_Index_Due_Dte", "DUE-DTE", FieldType.STRING, 8);
        cwf_Master_Index_Due_Dte_Chg_Ind = cwf_Master_Index__R_Field_11.newFieldInGroup("cwf_Master_Index_Due_Dte_Chg_Ind", "DUE-DTE-CHG-IND", FieldType.STRING, 
            1);
        cwf_Master_Index_Due_Dte_Prty_Cde = cwf_Master_Index__R_Field_11.newFieldInGroup("cwf_Master_Index_Due_Dte_Prty_Cde", "DUE-DTE-PRTY-CDE", FieldType.STRING, 
            1);
        cwf_Master_Index_Due_Dte_Filler = cwf_Master_Index__R_Field_11.newFieldInGroup("cwf_Master_Index_Due_Dte_Filler", "DUE-DTE-FILLER", FieldType.STRING, 
            6);
        cwf_Master_Index_Bsnss_Reply_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Bsnss_Reply_Ind", "BSNSS-REPLY-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BSNSS_REPLY_IND");
        cwf_Master_Index_Bsnss_Reply_Ind.setDdmHeader("BUSINESS/REPLY");
        cwf_Master_Index_Status_Freeze_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Status_Freeze_Ind", "STATUS-FREEZE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "STATUS_FREEZE_IND");
        cwf_Master_Index_Status_Freeze_Ind.setDdmHeader("FREEZE/STATUS");
        cwf_Master_Index_Elctrnc_Fldr_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Elctrnc_Fldr_Ind", "ELCTRNC-FLDR-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ELCTRNC_FLDR_IND");
        cwf_Master_Index_Elctrnc_Fldr_Ind.setDdmHeader("ELECTRONIC/FOLDER IND");
        cwf_Master_Index_Log_Insttn_Srce_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Log_Insttn_Srce_Cde", "LOG-INSTTN-SRCE-CDE", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "LOG_INSTTN_SRCE_CDE");
        cwf_Master_Index_Log_Insttn_Srce_Cde.setDdmHeader("REQUESTOR/INSTITUTION");
        cwf_Master_Index_Log_Rqstr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Log_Rqstr_Cde", "LOG-RQSTR-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "LOG_RQSTR_CDE");
        cwf_Master_Index_Log_Rqstr_Cde.setDdmHeader("REQUESTOR/TYPE");
        cwf_Master_Index_Unit_Clock_Start_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Unit_Clock_Start_Dte_Tme", "UNIT-CLOCK-START-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "UNIT_CLOCK_START_DTE_TME");
        cwf_Master_Index_Unit_Clock_Start_Dte_Tme.setDdmHeader("UNIT CLOCK START DATE TIME");
        cwf_Master_Index_Unit_Clock_End_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Unit_Clock_End_Dte_Tme", "UNIT-CLOCK-END-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "UNIT_CLOCK_END_DTE_TME");
        cwf_Master_Index_Unit_Clock_End_Dte_Tme.setDdmHeader("UNIT CLOCK END DATE TIME");
        cwf_Master_Index_Empl_Clock_Start_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Empl_Clock_Start_Dte_Tme", "EMPL-CLOCK-START-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "EMPL_CLOCK_START_DTE_TME");
        cwf_Master_Index_Empl_Clock_Start_Dte_Tme.setDdmHeader("EMPLOYEE CLOCK START DATE TIME");
        cwf_Master_Index_Empl_Clock_End_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Empl_Clock_End_Dte_Tme", "EMPL-CLOCK-END-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "EMPL_CLOCK_END_DTE_TME");
        cwf_Master_Index_Empl_Clock_End_Dte_Tme.setDdmHeader("EMPLOYEE CLOCK END DATE TIME");
        cwf_Master_Index_Intrnl_Pnd_Start_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Intrnl_Pnd_Start_Dte_Tme", "INTRNL-PND-START-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "INTRNL_PND_START_DTE_TME");
        cwf_Master_Index_Intrnl_Pnd_Start_Dte_Tme.setDdmHeader("ACKNOWLEDGEMENT DATE TIME");
        cwf_Master_Index_Intrnl_Pnd_End_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Intrnl_Pnd_End_Dte_Tme", "INTRNL-PND-END-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "INTRNL_PND_END_DTE_TME");
        cwf_Master_Index_Intrnl_Pnd_End_Dte_Tme.setDdmHeader("INTERNAL PEND END DATE TIME");
        cwf_Master_Index_Intrnl_Pnd_Days = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Intrnl_Pnd_Days", "INTRNL-PND-DAYS", FieldType.PACKED_DECIMAL, 
            5, RepeatingFieldStrategy.None, "INTRNL_PND_DAYS");
        cwf_Master_Index_Intrnl_Pnd_Days.setDdmHeader("INTERNAL PEND DAYS");
        cwf_Master_Index_Step_Clock_Start_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Step_Clock_Start_Dte_Tme", "STEP-CLOCK-START-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "STEP_CLOCK_START_DTE_TME");
        cwf_Master_Index_Step_Clock_Start_Dte_Tme.setDdmHeader("STEP START DATE TIME");
        cwf_Master_Index_Step_Clock_End_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Step_Clock_End_Dte_Tme", "STEP-CLOCK-END-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "STEP_CLOCK_END_DTE_TME");
        cwf_Master_Index_Step_Clock_End_Dte_Tme.setDdmHeader("STEP CLOCK END DATE TIME");
        cwf_Master_Index_Crprte_Clock_End_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Crprte_Clock_End_Dte_Tme", "CRPRTE-CLOCK-END-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "CRPRTE_CLOCK_END_DTE_TME");
        cwf_Master_Index_Crprte_Clock_End_Dte_Tme.setDdmHeader("CORPORATE CLOCK END DATE TIME");
        cwf_Master_Index_Unit_En_Rte_To_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Unit_En_Rte_To_Dte_Tme", "UNIT-EN-RTE-TO-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "UNIT_EN_RTE_TO_DTE_TME");
        cwf_Master_Index_Unit_En_Rte_To_Dte_Tme.setDdmHeader("UNIT EN ROUTE TO DATE TIME");
        cwf_Master_Index_Acknwldgmnt_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Acknwldgmnt_Cde", "ACKNWLDGMNT-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "ACKNWLDGMNT_CDE");
        cwf_Master_Index_Acknwldgmnt_Cde.setDdmHeader("ACKNOWLEDGEMENT/CODE");
        cwf_Master_Index_Acknwldgmnt_Oprtr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Acknwldgmnt_Oprtr_Cde", "ACKNWLDGMNT-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ACKNWLDGMNT_OPRTR_CDE");
        cwf_Master_Index_Acknwldgmnt_Oprtr_Cde.setDdmHeader("ACKNOWLEDGED/BY");
        cwf_Master_Index_Acknwldgmnt_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Acknwldgmnt_Dte_Tme", "ACKNWLDGMNT-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "ACKNWLDGMNT_DTE_TME");
        cwf_Master_Index_Acknwldgmnt_Dte_Tme.setDdmHeader("ACKNOWLEDGED/ON");
        cwf_Master_Index_Cntct_Sheet_Print_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Cntct_Sheet_Print_Cde", "CNTCT-SHEET-PRINT-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTCT_SHEET_PRINT_CDE");
        cwf_Master_Index_Cntct_Sheet_Print_Cde.setDdmHeader("CONTACT SHEET/PRINT CODE");
        cwf_Master_Index_Cntct_Sheet_Print_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Cntct_Sheet_Print_Dte_Tme", "CNTCT-SHEET-PRINT-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CNTCT_SHEET_PRINT_DTE_TME");
        cwf_Master_Index_Cntct_Sheet_Printer_Id_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Cntct_Sheet_Printer_Id_Cde", "CNTCT-SHEET-PRINTER-ID-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTCT_SHEET_PRINTER_ID_CDE");
        registerRecord(vw_cwf_Master_Index);

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Support_Tbl__R_Field_12 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_12", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Tbl_Last_Run_Date = cwf_Support_Tbl__R_Field_12.newFieldInGroup("cwf_Support_Tbl_Tbl_Last_Run_Date", "TBL-LAST-RUN-DATE", FieldType.STRING, 
            8);
        cwf_Support_Tbl_Tbl_Last_Run_Flag = cwf_Support_Tbl__R_Field_12.newFieldInGroup("cwf_Support_Tbl_Tbl_Last_Run_Flag", "TBL-LAST-RUN-FLAG", FieldType.STRING, 
            1);
        registerRecord(vw_cwf_Support_Tbl);

        pnd_End_Var = localVariables.newGroupInRecord("pnd_End_Var", "#END-VAR");
        pnd_End_Var_Pnd_End_Tbl_Key = pnd_End_Var.newFieldInGroup("pnd_End_Var_Pnd_End_Tbl_Key", "#END-TBL-KEY", FieldType.STRING, 53);

        pnd_End_Var__R_Field_13 = pnd_End_Var.newGroupInGroup("pnd_End_Var__R_Field_13", "REDEFINE", pnd_End_Var_Pnd_End_Tbl_Key);
        pnd_End_Var_Pnd_End_Tbl_Authrty = pnd_End_Var__R_Field_13.newFieldInGroup("pnd_End_Var_Pnd_End_Tbl_Authrty", "#END-TBL-AUTHRTY", FieldType.STRING, 
            2);
        pnd_End_Var_Pnd_End_Tbl_Name = pnd_End_Var__R_Field_13.newFieldInGroup("pnd_End_Var_Pnd_End_Tbl_Name", "#END-TBL-NAME", FieldType.STRING, 20);
        pnd_End_Var_Pnd_End_Tbl_Code = pnd_End_Var__R_Field_13.newFieldInGroup("pnd_End_Var_Pnd_End_Tbl_Code", "#END-TBL-CODE", FieldType.STRING, 30);

        pnd_Gen_Var = localVariables.newGroupInRecord("pnd_Gen_Var", "#GEN-VAR");
        pnd_Gen_Var_Pnd_Gen_Tbl_Key = pnd_Gen_Var.newFieldInGroup("pnd_Gen_Var_Pnd_Gen_Tbl_Key", "#GEN-TBL-KEY", FieldType.STRING, 53);

        pnd_Gen_Var__R_Field_14 = pnd_Gen_Var.newGroupInGroup("pnd_Gen_Var__R_Field_14", "REDEFINE", pnd_Gen_Var_Pnd_Gen_Tbl_Key);
        pnd_Gen_Var_Pnd_Gen_Tbl_Authrty = pnd_Gen_Var__R_Field_14.newFieldInGroup("pnd_Gen_Var_Pnd_Gen_Tbl_Authrty", "#GEN-TBL-AUTHRTY", FieldType.STRING, 
            2);
        pnd_Gen_Var_Pnd_Gen_Tbl_Name = pnd_Gen_Var__R_Field_14.newFieldInGroup("pnd_Gen_Var_Pnd_Gen_Tbl_Name", "#GEN-TBL-NAME", FieldType.STRING, 20);
        pnd_Gen_Var_Pnd_Gen_Tbl_Code = pnd_Gen_Var__R_Field_14.newFieldInGroup("pnd_Gen_Var_Pnd_Gen_Tbl_Code", "#GEN-TBL-CODE", FieldType.STRING, 30);
        pnd_Gen_Var_Pnd_Gen_Tbl_Desc = pnd_Gen_Var.newFieldInGroup("pnd_Gen_Var_Pnd_Gen_Tbl_Desc", "#GEN-TBL-DESC", FieldType.STRING, 30);
        pnd_Report_No = localVariables.newFieldInRecord("pnd_Report_No", "#REPORT-NO", FieldType.NUMERIC, 2);
        pnd_Racf_Id = localVariables.newFieldInRecord("pnd_Racf_Id", "#RACF-ID", FieldType.STRING, 8);
        pnd_Floor = localVariables.newFieldInRecord("pnd_Floor", "#FLOOR", FieldType.NUMERIC, 2);
        pnd_Bldg = localVariables.newFieldInRecord("pnd_Bldg", "#BLDG", FieldType.STRING, 3);
        pnd_Drop_Off = localVariables.newFieldInRecord("pnd_Drop_Off", "#DROP-OFF", FieldType.STRING, 2);
        pnd_Unit_Code = localVariables.newFieldInRecord("pnd_Unit_Code", "#UNIT-CODE", FieldType.STRING, 8);
        pnd_Report_Parm = localVariables.newFieldInRecord("pnd_Report_Parm", "#REPORT-PARM", FieldType.STRING, 17);
        pnd_Tbl_Run_Flag = localVariables.newFieldInRecord("pnd_Tbl_Run_Flag", "#TBL-RUN-FLAG", FieldType.STRING, 1);
        pnd_Parm_Unit = localVariables.newFieldInRecord("pnd_Parm_Unit", "#PARM-UNIT", FieldType.STRING, 7);
        pnd_Comp_Date = localVariables.newFieldInRecord("pnd_Comp_Date", "#COMP-DATE", FieldType.STRING, 8);
        pnd_Env = localVariables.newFieldInRecord("pnd_Env", "#ENV", FieldType.STRING, 10);
        pnd_Unit_Start_Dte_Tme = localVariables.newFieldInRecord("pnd_Unit_Start_Dte_Tme", "#UNIT-START-DTE-TME", FieldType.TIME);
        pnd_Unit_End_Dte_Tme = localVariables.newFieldInRecord("pnd_Unit_End_Dte_Tme", "#UNIT-END-DTE-TME", FieldType.TIME);
        pnd_Start_Date = localVariables.newFieldInRecord("pnd_Start_Date", "#START-DATE", FieldType.STRING, 8);
        pnd_Start_Dte_Tme = localVariables.newFieldInRecord("pnd_Start_Dte_Tme", "#START-DTE-TME", FieldType.TIME);
        pnd_Start_Dte_Tme_A = localVariables.newFieldInRecord("pnd_Start_Dte_Tme_A", "#START-DTE-TME-A", FieldType.STRING, 15);

        pnd_Start_Dte_Tme_A__R_Field_15 = localVariables.newGroupInRecord("pnd_Start_Dte_Tme_A__R_Field_15", "REDEFINE", pnd_Start_Dte_Tme_A);
        pnd_Start_Dte_Tme_A_Pnd_Start_Dte_Tme_N = pnd_Start_Dte_Tme_A__R_Field_15.newFieldInGroup("pnd_Start_Dte_Tme_A_Pnd_Start_Dte_Tme_N", "#START-DTE-TME-N", 
            FieldType.NUMERIC, 15);
        pnd_End_Dte_Tme = localVariables.newFieldInRecord("pnd_End_Dte_Tme", "#END-DTE-TME", FieldType.TIME);
        pnd_End_Dte_Tme_A = localVariables.newFieldInRecord("pnd_End_Dte_Tme_A", "#END-DTE-TME-A", FieldType.STRING, 15);

        pnd_End_Dte_Tme_A__R_Field_16 = localVariables.newGroupInRecord("pnd_End_Dte_Tme_A__R_Field_16", "REDEFINE", pnd_End_Dte_Tme_A);
        pnd_End_Dte_Tme_A_Pnd_End_Dte_Tme_N = pnd_End_Dte_Tme_A__R_Field_16.newFieldInGroup("pnd_End_Dte_Tme_A_Pnd_End_Dte_Tme_N", "#END-DTE-TME-N", FieldType.NUMERIC, 
            15);
        pnd_Work_St_Date = localVariables.newFieldInRecord("pnd_Work_St_Date", "#WORK-ST-DATE", FieldType.STRING, 8);
        pnd_Work_End_Date = localVariables.newFieldInRecord("pnd_Work_End_Date", "#WORK-END-DATE", FieldType.STRING, 8);
        pnd_Work_Date_Time = localVariables.newFieldInRecord("pnd_Work_Date_Time", "#WORK-DATE-TIME", FieldType.NUMERIC, 15);

        pnd_Work_Date_Time__R_Field_17 = localVariables.newGroupInRecord("pnd_Work_Date_Time__R_Field_17", "REDEFINE", pnd_Work_Date_Time);
        pnd_Work_Date_Time_Pnd_Work_Date = pnd_Work_Date_Time__R_Field_17.newFieldInGroup("pnd_Work_Date_Time_Pnd_Work_Date", "#WORK-DATE", FieldType.STRING, 
            8);
        pnd_Work_Date_Time_Pnd_Work_Time = pnd_Work_Date_Time__R_Field_17.newFieldInGroup("pnd_Work_Date_Time_Pnd_Work_Time", "#WORK-TIME", FieldType.STRING, 
            7);
        pnd_Read_Ctr = localVariables.newFieldInRecord("pnd_Read_Ctr", "#READ-CTR", FieldType.PACKED_DECIMAL, 12);
        pnd_Conv_Ctr = localVariables.newFieldInRecord("pnd_Conv_Ctr", "#CONV-CTR", FieldType.PACKED_DECIMAL, 12);
        pnd_Nconv_Ctr = localVariables.newFieldInRecord("pnd_Nconv_Ctr", "#NCONV-CTR", FieldType.PACKED_DECIMAL, 12);
        pnd_Parm_Type = localVariables.newFieldInRecord("pnd_Parm_Type", "#PARM-TYPE", FieldType.STRING, 1);
        pnd_Sub = localVariables.newFieldInRecord("pnd_Sub", "#SUB", FieldType.PACKED_DECIMAL, 5);
        pnd_Timx = localVariables.newFieldInRecord("pnd_Timx", "#TIMX", FieldType.TIME);
        pnd_Receive_Date = localVariables.newFieldInRecord("pnd_Receive_Date", "#RECEIVE-DATE", FieldType.DATE);
        pnd_Report_Date = localVariables.newFieldInRecord("pnd_Report_Date", "#REPORT-DATE", FieldType.DATE);
        pnd_Status_Updte_Dte = localVariables.newFieldInRecord("pnd_Status_Updte_Dte", "#STATUS-UPDTE-DTE", FieldType.DATE);
        pnd_Work_Read = localVariables.newFieldInRecord("pnd_Work_Read", "#WORK-READ", FieldType.PACKED_DECIMAL, 9);
        pnd_Work_Write = localVariables.newFieldInRecord("pnd_Work_Write", "#WORK-WRITE", FieldType.PACKED_DECIMAL, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Master_Index.reset();
        vw_cwf_Support_Tbl.reset();

        localVariables.reset();
        pnd_Report_No.setInitialValue(24);
        pnd_Unit_Code.setInitialValue("CWF");
        pnd_Report_Parm.setInitialValue("CWFB3011W*");
        pnd_Parm_Type.setInitialValue("W");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb3205() throws Exception
    {
        super("Cwfb3205");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("CWFB3205", onError);
        setupReports();
        //* *********************************
        //*  COPYCODE : CWFC3000
        //*  FUNCTION : SETUP ENVONMENT MESSAGE
        //*  AUTHOR   : PATINGO, JOSEPH S.
        //* *********************************
        pnd_Env.setValue(Global.getLIBRARY_ID());                                                                                                                         //Natural: MOVE *LIBRARY-ID TO #ENV
        if (condition(pnd_Env.equals("PROJCWF") || pnd_Env.equals("PROJCWF")))                                                                                            //Natural: IF #ENV = 'PROJCWF' OR #ENV = 'PROJCWF'
        {
            pnd_Env.setValue("DEV'T ");                                                                                                                                   //Natural: MOVE 'DEV"T ' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("PRACANN")))                                                                                                                         //Natural: IF #ENV = 'PRACANN'
        {
            pnd_Env.setValue("PRACTICE");                                                                                                                                 //Natural: MOVE 'PRACTICE' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("PRODANN")))                                                                                                                         //Natural: IF #ENV = 'PRODANN'
        {
            pnd_Env.setValue(" ");                                                                                                                                        //Natural: MOVE ' ' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT034")))                                                                                                                         //Natural: IF #ENV = 'ACPT034'
        {
            pnd_Env.setValue("RGN:AT07");                                                                                                                                 //Natural: MOVE 'RGN:AT07' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT023")))                                                                                                                         //Natural: IF #ENV = 'ACPT023'
        {
            pnd_Env.setValue("RGN:AT06");                                                                                                                                 //Natural: MOVE 'RGN:AT06' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT028")))                                                                                                                         //Natural: IF #ENV = 'ACPT028'
        {
            pnd_Env.setValue("RGN:AT05");                                                                                                                                 //Natural: MOVE 'RGN:AT05' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        //* ****** END COPYCODE *************
        if (condition(Global.getLIBRARY_ID().notEquals("PRODANN")))                                                                                                       //Natural: IF *LIBRARY-ID NE 'PRODANN'
        {
            pnd_Env.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "(", pnd_Env, ")"));                                                                         //Natural: COMPRESS '(' #ENV ')' INTO #ENV LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Misc_Parm_Pnd_Todays_Time.setValue(Global.getTIMX());                                                                                                         //Natural: MOVE *TIMX TO #TODAYS-TIME
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 142 PS = 58
        //*  GETS SYSTEM RUN DATE ( NEW ONE )
        DbsUtil.callnat(Cwfn3912.class , getCurrentProcessState(), pnd_Comp_Date, pnd_Parm_Type);                                                                         //Natural: CALLNAT 'CWFN3912' #COMP-DATE #PARM-TYPE
        if (condition(Global.isEscape())) return;
        READ_WORK:                                                                                                                                                        //Natural: READ WORK 5 #WRK
        while (condition(getWorkFiles().read(5, pnd_Wrk)))
        {
            pnd_Work_Read.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #WORK-READ
            vw_cwf_Master_Index.startDatabaseRead                                                                                                                         //Natural: READ CWF-MASTER-INDEX BY RQST-ROUTING-KEY FROM #WRK.TBL-LOG-DTE-TME
            (
            "READ_MASTER",
            new Wc[] { new Wc("RQST_ROUTING_KEY", ">=", pnd_Wrk_Tbl_Log_Dte_Tme, WcType.BY) },
            new Oc[] { new Oc("RQST_ROUTING_KEY", "ASC") }
            );
            READ_MASTER:
            while (condition(vw_cwf_Master_Index.readNextRow("READ_MASTER")))
            {
                if (condition(cwf_Master_Index_Rqst_Log_Dte_Tme.greater(pnd_Wrk_Tbl_Log_Dte_Tme)))                                                                        //Natural: IF CWF-MASTER-INDEX.RQST-LOG-DTE-TME GT #WRK.TBL-LOG-DTE-TME
                {
                    if (true) break READ_MASTER;                                                                                                                          //Natural: ESCAPE BOTTOM ( READ-MASTER. )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(!(DbsUtil.maskMatches(cwf_Master_Index_Unit_Clock_Start_Dte_Tme,"YYYYMMDD.......") && DbsUtil.maskMatches(cwf_Master_Index_Unit_Clock_End_Dte_Tme, //Natural: ACCEPT IF CWF-MASTER-INDEX.UNIT-CLOCK-START-DTE-TME = MASK ( YYYYMMDD....... ) AND CWF-MASTER-INDEX.UNIT-CLOCK-END-DTE-TME = MASK ( YYYYMMDD....... )
                    "YYYYMMDD......."))))
                {
                    continue;
                }
                if (condition(cwf_Master_Index_Admin_Unit_Cde.notEquals("RSSMP")))                                                                                        //Natural: IF CWF-MASTER-INDEX.ADMIN-UNIT-CDE NE 'RSSMP'
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-FILE
                    sub_Write_Work_File();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_MASTER"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_MASTER"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  READ-MASTER-2.
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_WORK"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_WORK"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  READ-WORK.
        }                                                                                                                                                                 //Natural: END-WORK
        READ_WORK_Exit:
        if (Global.isEscape()) return;
        getReports().write(0, "CWFB3205 EXTRACT",NEWLINE,"NO OF WORK RECORDS READ  ",pnd_Work_Read,NEWLINE,"NO OF WRITES ISSUED      ",pnd_Work_Write,                    //Natural: WRITE 'CWFB3205 EXTRACT' / 'NO OF WORK RECORDS READ  ' #WORK-READ / 'NO OF WRITES ISSUED      ' #WORK-WRITE // 'END OF STAT REPORT'
            NEWLINE,NEWLINE,"END OF STAT REPORT");
        if (Global.isEscape()) return;
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WORK-FILE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-START-DATE
    }                                                                                                                                                                     //Natural: ON ERROR
    private void sub_Write_Work_File() throws Exception                                                                                                                   //Natural: WRITE-WORK-FILE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cwfb3013.reset();                                                                                                                                             //Natural: RESET #CWFB3013
        pnd_Cwfb3013.setValuesByName(pnd_Wrk);                                                                                                                            //Natural: MOVE BY NAME #WRK TO #CWFB3013
        //*  ----------------------------------------------------------------------
        pnd_Cwfb3013_Admin_Unit_Cde.setValue(pnd_Wrk_Admin_Unit_Cde);                                                                                                     //Natural: MOVE #WRK.ADMIN-UNIT-CDE TO #CWFB3013.ADMIN-UNIT-CDE
        pnd_Cwfb3013_Empl_Racf_Id.setValue(pnd_Wrk_Empl_Racf_Id);                                                                                                         //Natural: MOVE #WRK.EMPL-RACF-ID TO #CWFB3013.EMPL-RACF-ID
        pnd_Cwfb3013_Tbl_Calendar_Days.setValue(pnd_Wrk_Tbl_Calendar_Days);                                                                                               //Natural: MOVE #WRK.TBL-CALENDAR-DAYS TO #CWFB3013.TBL-CALENDAR-DAYS
        pnd_Cwfb3013_Tbl_Tiaa_Bsnss_Days.setValue(pnd_Wrk_Tbl_Tiaa_Bsnss_Days);                                                                                           //Natural: MOVE #WRK.TBL-TIAA-BSNSS-DAYS TO #CWFB3013.TBL-TIAA-BSNSS-DAYS
        pnd_Cwfb3013_Tbl_Business_Days.setValue(pnd_Wrk_Tbl_Business_Days);                                                                                               //Natural: MOVE #WRK.TBL-BUSINESS-DAYS TO #CWFB3013.TBL-BUSINESS-DAYS
        pnd_Cwfb3013_Tbl_Complete_Days.setValue(pnd_Wrk_Tbl_Complete_Days);                                                                                               //Natural: MOVE #WRK.TBL-COMPLETE-DAYS TO #CWFB3013.TBL-COMPLETE-DAYS
        pnd_Cwfb3013_Wpid_Action.setValue(pnd_Wrk_Wpid_Action);                                                                                                           //Natural: MOVE #WRK.WPID-ACTION TO #CWFB3013.WPID-ACTION
        pnd_Cwfb3013_Work_Prcss_Id.setValue(pnd_Wrk_Work_Prcss_Id);                                                                                                       //Natural: MOVE #WRK.WORK-PRCSS-ID TO #CWFB3013.WORK-PRCSS-ID
        pnd_Cwfb3013_Tbl_Status_Key.setValue(pnd_Wrk_Tbl_Status_Key);                                                                                                     //Natural: MOVE #WRK.TBL-STATUS-KEY TO #CWFB3013.TBL-STATUS-KEY
        pnd_Cwfb3013_Tbl_Close_Unit.setValue(pnd_Wrk_Tbl_Close_Unit);                                                                                                     //Natural: MOVE #WRK.TBL-CLOSE-UNIT TO #CWFB3013.TBL-CLOSE-UNIT
        pnd_Cwfb3013_Tbl_Pin.setValue(pnd_Wrk_Tbl_Pin);                                                                                                                   //Natural: MOVE #WRK.TBL-PIN TO #CWFB3013.TBL-PIN
        pnd_Cwfb3013_Cntrct_Nbr.setValue(pnd_Wrk_Cntrct_Nbr);                                                                                                             //Natural: MOVE #WRK.CNTRCT-NBR TO #CWFB3013.CNTRCT-NBR
        pnd_Cwfb3013_Tbl_Log_Dte_Tme.setValue(pnd_Wrk_Tbl_Log_Dte_Tme);                                                                                                   //Natural: MOVE #WRK.TBL-LOG-DTE-TME TO #CWFB3013.TBL-LOG-DTE-TME
        pnd_Cwfb3013_Tiaa_Rcvd_Dte.setValue(pnd_Wrk_Tiaa_Rcvd_Dte);                                                                                                       //Natural: MOVE #WRK.TIAA-RCVD-DTE TO #CWFB3013.TIAA-RCVD-DTE
        pnd_Cwfb3013_Admin_Status_Updte_Dte_Tme.setValue(pnd_Wrk_Admin_Status_Updte_Dte_Tme);                                                                             //Natural: MOVE #WRK.ADMIN-STATUS-UPDTE-DTE-TME TO #CWFB3013.ADMIN-STATUS-UPDTE-DTE-TME
        pnd_Cwfb3013_Return_Doc_Rec_Dte_Tme.setValue(pnd_Wrk_Return_Doc_Rec_Dte_Tme);                                                                                     //Natural: MOVE #WRK.RETURN-DOC-REC-DTE-TME TO #CWFB3013.RETURN-DOC-REC-DTE-TME
        pnd_Cwfb3013_Return_Rcvd_Dte_Tme.setValue(pnd_Wrk_Return_Rcvd_Dte_Tme);                                                                                           //Natural: MOVE #WRK.RETURN-RCVD-DTE-TME TO #CWFB3013.RETURN-RCVD-DTE-TME
        pnd_Cwfb3013_Pnd_Last_Chnge_Dte_Tme.setValue(pnd_Wrk_Pnd_Last_Chnge_Dte_Tme);                                                                                     //Natural: MOVE #WRK.#LAST-CHNGE-DTE-TME TO #CWFB3013.#LAST-CHNGE-DTE-TME
        pnd_Cwfb3013_Pnd_Partic_Sname.setValue(pnd_Wrk_Pnd_Partic_Sname);                                                                                                 //Natural: MOVE #WRK.#PARTIC-SNAME TO #CWFB3013.#PARTIC-SNAME
        pnd_Cwfb3013_Rqst_Log_Dte_Tme.setValue(pnd_Wrk_Rqst_Log_Dte_Tme);                                                                                                 //Natural: MOVE #WRK.RQST-LOG-DTE-TME TO #CWFB3013.RQST-LOG-DTE-TME
        pnd_Cwfb3013_Last_Chnge_Dte_Tme.setValue(pnd_Wrk_Last_Chnge_Dte_Tme);                                                                                             //Natural: MOVE #WRK.LAST-CHNGE-DTE-TME TO #CWFB3013.LAST-CHNGE-DTE-TME
        //*  ----------------------------------------------------------------------
        //*  --------------------------
        //*  BUSINESS DAYS IN UNIT     /* FROM UNIT START CLOCK TO UNIT END CLOCK
        //*  --------------------------
        pnd_Unit_Start_Dte_Tme.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),cwf_Master_Index_Unit_Clock_Start_Dte_Tme);                                           //Natural: MOVE EDITED CWF-MASTER-INDEX.UNIT-CLOCK-START-DTE-TME TO #UNIT-START-DTE-TME ( EM = YYYYMMDDHHIISST )
        pnd_Unit_End_Dte_Tme.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),cwf_Master_Index_Unit_Clock_End_Dte_Tme);                                               //Natural: MOVE EDITED CWF-MASTER-INDEX.UNIT-CLOCK-END-DTE-TME TO #UNIT-END-DTE-TME ( EM = YYYYMMDDHHIISST )
        pnd_Misc_Parm_Pnd_Business_Daysx.compute(new ComputeParameters(false, pnd_Misc_Parm_Pnd_Business_Daysx), pnd_Unit_End_Dte_Tme.subtract(pnd_Unit_Start_Dte_Tme));  //Natural: COMPUTE #BUSINESS-DAYSX = #UNIT-END-DTE-TME - #UNIT-START-DTE-TME
        pnd_Cwfb3013_Tbl_Business_Days.compute(new ComputeParameters(false, pnd_Cwfb3013_Tbl_Business_Days), pnd_Misc_Parm_Pnd_Business_Daysx.divide(864000));            //Natural: COMPUTE #CWFB3013.TBL-BUSINESS-DAYS = #BUSINESS-DAYSX / 864000
        //*  ---------------------
        //*  RECEIVED IN UNIT DATE
        //*  ---------------------
        if (condition(DbsUtil.maskMatches(cwf_Master_Index_Unit_Clock_Start_Dte_Tme,"YYYYMMDD")))                                                                         //Natural: IF CWF-MASTER-INDEX.UNIT-CLOCK-START-DTE-TME = MASK ( YYYYMMDD )
        {
            pnd_Cwfb3013_Return_Rcvd_Dte_Tme.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),cwf_Master_Index_Unit_Clock_Start_Dte_Tme);                             //Natural: MOVE EDITED CWF-MASTER-INDEX.UNIT-CLOCK-START-DTE-TME TO #CWFB3013.RETURN-RCVD-DTE-TME ( EM = YYYYMMDDHHIISST )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Cwfb3013_Return_Rcvd_Dte_Tme.reset();                                                                                                                     //Natural: RESET #CWFB3013.RETURN-RCVD-DTE-TME
        }                                                                                                                                                                 //Natural: END-IF
        //*  ------------------------
        pnd_Cwfb3013_Admin_Unit_Cde.setValue(cwf_Master_Index_Admin_Unit_Cde);                                                                                            //Natural: MOVE CWF-MASTER-INDEX.ADMIN-UNIT-CDE TO #CWFB3013.ADMIN-UNIT-CDE
        pnd_Cwfb3013_Tbl_Close_Unit.setValue(cwf_Master_Index_Unit_Cde);                                                                                                  //Natural: MOVE CWF-MASTER-INDEX.UNIT-CDE TO #CWFB3013.TBL-CLOSE-UNIT
        pnd_Cwfb3013_Last_Chnge_Unit_Cde.setValue(cwf_Master_Index_Last_Chnge_Unit_Cde);                                                                                  //Natural: MOVE CWF-MASTER-INDEX.LAST-CHNGE-UNIT-CDE TO #CWFB3013.LAST-CHNGE-UNIT-CDE
        pnd_Cwfb3013_Status_Cde.setValue(cwf_Master_Index_Status_Cde);                                                                                                    //Natural: MOVE CWF-MASTER-INDEX.STATUS-CDE TO #CWFB3013.STATUS-CDE
        pnd_Cwfb3013_Empl_Racf_Id.setValue(cwf_Master_Index_Empl_Racf_Id);                                                                                                //Natural: MOVE CWF-MASTER-INDEX.EMPL-RACF-ID TO #CWFB3013.EMPL-RACF-ID
        pnd_Cwfb3013_Tiaa_Rcvd_Dte.setValue(cwf_Master_Index_Tiaa_Rcvd_Dte);                                                                                              //Natural: MOVE CWF-MASTER-INDEX.TIAA-RCVD-DTE TO #CWFB3013.TIAA-RCVD-DTE
        pnd_Cwfb3013_Admin_Status_Updte_Dte_Tme.setValue(cwf_Master_Index_Admin_Status_Updte_Dte_Tme);                                                                    //Natural: MOVE CWF-MASTER-INDEX.ADMIN-STATUS-UPDTE-DTE-TME TO #CWFB3013.ADMIN-STATUS-UPDTE-DTE-TME
        pnd_Cwfb3013_Cntrct_Nbr.setValue(cwf_Master_Index_Cntrct_Nbr.getValue(1));                                                                                        //Natural: MOVE CWF-MASTER-INDEX.CNTRCT-NBR ( 1 ) TO #CWFB3013.CNTRCT-NBR
        pnd_Cwfb3013_Rqst_Log_Dte_Tme.setValue(cwf_Master_Index_Rqst_Log_Dte_Tme);                                                                                        //Natural: MOVE CWF-MASTER-INDEX.RQST-LOG-DTE-TME TO #CWFB3013.RQST-LOG-DTE-TME
        pnd_Cwfb3013_Pnd_Last_Chnge_Dte_Tme.setValue(cwf_Master_Index_Last_Chnge_Dte_Tme);                                                                                //Natural: MOVE CWF-MASTER-INDEX.LAST-CHNGE-DTE-TME TO #CWFB3013.#LAST-CHNGE-DTE-TME #CWFB3013.LAST-CHNGE-DTE-TME
        pnd_Cwfb3013_Last_Chnge_Dte_Tme.setValue(cwf_Master_Index_Last_Chnge_Dte_Tme);
        pnd_Cwfb3013_Pnd_Partic_Sname.setValue(pnd_Wrk_Pnd_Partic_Sname);                                                                                                 //Natural: MOVE #WRK.#PARTIC-SNAME TO #CWFB3013.#PARTIC-SNAME
        getWorkFiles().write(1, false, pnd_Cwfb3013);                                                                                                                     //Natural: WRITE WORK FILE 1 #CWFB3013
        pnd_Work_Write.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #WORK-WRITE
        pnd_Cwfb3013.reset();                                                                                                                                             //Natural: RESET #CWFB3013
    }
    private void sub_Get_Start_Date() throws Exception                                                                                                                    //Natural: GET-START-DATE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Misc_Parm_Pnd_Work_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Comp_Date);                                                                       //Natural: MOVE EDITED #COMP-DATE TO #WORK-DATE-D ( EM = YYYYMMDD )
        pnd_Work_End_Date.setValueEdited(pnd_Misc_Parm_Pnd_Work_Date_D,new ReportEditMask("MM'/'DD'/'YY"));                                                               //Natural: MOVE EDITED #WORK-DATE-D ( EM = MM'/'DD'/'YY ) TO #WORK-END-DATE
        pnd_Work_Date_Time_Pnd_Work_Date.setValueEdited(pnd_Misc_Parm_Pnd_Work_Date_D,new ReportEditMask("YYYYMMDD"));                                                    //Natural: MOVE EDITED #WORK-DATE-D ( EM = YYYYMMDD ) TO #WORK-DATE
        pnd_End_Dte_Tme.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Work_Date_Time_Pnd_Work_Date);                                                                  //Natural: MOVE EDITED #WORK-DATE TO #END-DTE-TME ( EM = YYYYMMDD )
        pnd_End_Dte_Tme.nadd(863999);                                                                                                                                     //Natural: ADD 863999 TO #END-DTE-TME
        pnd_End_Dte_Tme_A.setValueEdited(pnd_End_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                                                          //Natural: MOVE EDITED #END-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #END-DTE-TME-A
        pnd_Misc_Parm_Pnd_Work_Date_D.nsubtract(6);                                                                                                                       //Natural: COMPUTE #WORK-DATE-D = #WORK-DATE-D - 6
        pnd_Work_St_Date.setValueEdited(pnd_Misc_Parm_Pnd_Work_Date_D,new ReportEditMask("MM'/'DD'/'YY"));                                                                //Natural: MOVE EDITED #WORK-DATE-D ( EM = MM'/'DD'/'YY ) TO #WORK-ST-DATE
        pnd_Work_Date_Time_Pnd_Work_Date.setValueEdited(pnd_Misc_Parm_Pnd_Work_Date_D,new ReportEditMask("YYYYMMDD"));                                                    //Natural: MOVE EDITED #WORK-DATE-D ( EM = YYYYMMDD ) TO #WORK-DATE
        pnd_Start_Dte_Tme.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Work_Date_Time_Pnd_Work_Date);                                                                //Natural: MOVE EDITED #WORK-DATE TO #START-DTE-TME ( EM = YYYYMMDD )
        pnd_Start_Dte_Tme_A.setValueEdited(pnd_Start_Dte_Tme,new ReportEditMask("YYYYMMDDHHIISST"));                                                                      //Natural: MOVE EDITED #START-DTE-TME ( EM = YYYYMMDDHHIISST ) TO #START-DTE-TME-A
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, NEWLINE,"*************** START OF ERROR LOG *****************",NEWLINE,"ERROR NO----->",Global.getERROR_NR(),NEWLINE,"LINE--------->",      //Natural: WRITE / '*************** START OF ERROR LOG *****************' / 'ERROR NO----->' *ERROR-NR / 'LINE--------->' *ERROR-LINE / 'PROGRAM------>' *PROGRAM / '*************** END  OF  ERROR LOG *****************' /
            Global.getERROR_LINE(),NEWLINE,"PROGRAM------>",Global.getPROGRAM(),NEWLINE,"*************** END  OF  ERROR LOG *****************",NEWLINE);
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=142 PS=58");
    }
}
