/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:12:12 PM
**        * FROM NATURAL PROGRAM : Efsp0092
************************************************************
**        * FILE NAME            : Efsp0092.java
**        * CLASS NAME           : Efsp0092
**        * INSTANCE NAME        : Efsp0092
************************************************************
************************************************************************
* PROGRAM  : EFSP0092
* SYSTEM   : CRPCWF
* TITLE    : COUNT OF CABINETS AND FOLDERS ON MASTER INDEX
* GENERATED: DEC 2,93 AT 10:00 AM
* FUNCTION : THIS PROGRAM READS THE CWF-MASTER-INDEX-VIEW AND COUNTS
*            THE NUMBER OF RECORDS AND INDIVIDUAL POLICYHOLDERS.
* HISTORY
* 06/13/2017 - PIN EXPANSION - AUG 2017
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsp0092 extends BLNatBase
{
    // Data Areas
    private GdaCwfg000 gdaCwfg000;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Master_Index;
    private DbsField cwf_Master_Index_Pin_History_Key;

    private DbsGroup cwf_Master_Index__R_Field_1;
    private DbsField cwf_Master_Index_Pnd_Pin_Nbr;
    private DbsField pnd_Old_Pin_Nbr;
    private DbsField pnd_Pin_Count;
    private DbsField pnd_Mit_Count;
    private DbsField pnd_I;
    private DbsField pnd_Lines_Reqd;
    private DbsField pnd_Program;
    private DbsField pnd_Record;
    private DbsField pnd_Env;
    private DbsField pnd_Report_Name;
    private DbsField pnd_Date_Desc;
    private DbsField pnd_Run_Date;
    private DbsField pnd_Deliver_To;
    private DbsField pnd_Unit;
    private DbsField pnd_Floor;
    private DbsField pnd_Bldg;
    private DbsField pnd_Drop_Off;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaCwfg000 = GdaCwfg000.getInstance(getCallnatLevel());
        registerRecord(gdaCwfg000);
        if (gdaOnly) return;

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Master_Index = new DataAccessProgramView(new NameInfo("vw_cwf_Master_Index", "CWF-MASTER-INDEX"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        cwf_Master_Index_Pin_History_Key = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Pin_History_Key", "PIN-HISTORY-KEY", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "PIN_HISTORY_KEY");
        cwf_Master_Index_Pin_History_Key.setSuperDescriptor(true);

        cwf_Master_Index__R_Field_1 = vw_cwf_Master_Index.getRecord().newGroupInGroup("cwf_Master_Index__R_Field_1", "REDEFINE", cwf_Master_Index_Pin_History_Key);
        cwf_Master_Index_Pnd_Pin_Nbr = cwf_Master_Index__R_Field_1.newFieldInGroup("cwf_Master_Index_Pnd_Pin_Nbr", "#PIN-NBR", FieldType.NUMERIC, 12);
        registerRecord(vw_cwf_Master_Index);

        pnd_Old_Pin_Nbr = localVariables.newFieldInRecord("pnd_Old_Pin_Nbr", "#OLD-PIN-NBR", FieldType.NUMERIC, 12);
        pnd_Pin_Count = localVariables.newFieldInRecord("pnd_Pin_Count", "#PIN-COUNT", FieldType.NUMERIC, 10);
        pnd_Mit_Count = localVariables.newFieldInRecord("pnd_Mit_Count", "#MIT-COUNT", FieldType.NUMERIC, 9);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Lines_Reqd = localVariables.newFieldInRecord("pnd_Lines_Reqd", "#LINES-REQD", FieldType.PACKED_DECIMAL, 3);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Record = localVariables.newFieldInRecord("pnd_Record", "#RECORD", FieldType.STRING, 54);
        pnd_Env = localVariables.newFieldInRecord("pnd_Env", "#ENV", FieldType.STRING, 10);
        pnd_Report_Name = localVariables.newFieldInRecord("pnd_Report_Name", "#REPORT-NAME", FieldType.STRING, 45);
        pnd_Date_Desc = localVariables.newFieldInRecord("pnd_Date_Desc", "#DATE-DESC", FieldType.STRING, 20);
        pnd_Run_Date = localVariables.newFieldInRecord("pnd_Run_Date", "#RUN-DATE", FieldType.DATE);
        pnd_Deliver_To = localVariables.newFieldInRecord("pnd_Deliver_To", "#DELIVER-TO", FieldType.STRING, 45);
        pnd_Unit = localVariables.newFieldInRecord("pnd_Unit", "#UNIT", FieldType.STRING, 45);
        pnd_Floor = localVariables.newFieldInRecord("pnd_Floor", "#FLOOR", FieldType.NUMERIC, 2);
        pnd_Bldg = localVariables.newFieldInRecord("pnd_Bldg", "#BLDG", FieldType.STRING, 3);
        pnd_Drop_Off = localVariables.newFieldInRecord("pnd_Drop_Off", "#DROP-OFF", FieldType.STRING, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Master_Index.reset();

        localVariables.reset();
        pnd_Report_Name.setInitialValue("MASTER INDEX FILE STATISTICS");
        pnd_Date_Desc.setInitialValue("RUN DATE -->");
        pnd_Deliver_To.setInitialValue("GERI DEZENDORF");
        pnd_Unit.setInitialValue("CWF SYSTEMS");
        pnd_Floor.setInitialValue(3);
        pnd_Bldg.setInitialValue("485");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Efsp0092() throws Exception
    {
        super("Efsp0092");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        getReports().definePrinter(2, "AUDIT");                                                                                                                           //Natural: DEFINE PRINTER ( AUDIT = 1 )
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: FORMAT ( AUDIT ) LS = 133 PS = 60 ZP = OFF;//Natural: ASSIGN #PROGRAM = *PROGRAM
        //*  ASSIGN BANNER PAGE VARIABLES
        pnd_Run_Date.setValue(Global.getDATX());                                                                                                                          //Natural: ASSIGN #RUN-DATE = *DATX
        if (condition(Global.getLIBRARY_ID().notEquals("PRODANN")))                                                                                                       //Natural: IF *LIBRARY-ID NE 'PRODANN'
        {
            pnd_Env.setValue(Global.getLIBRARY_ID());                                                                                                                     //Natural: ASSIGN #ENV = *LIBRARY-ID
        }                                                                                                                                                                 //Natural: END-IF
        //*  READ THE MIT RECORDS
        vw_cwf_Master_Index.createHistogram                                                                                                                               //Natural: HISTOGRAM CWF-MASTER-INDEX FOR PIN-HISTORY-KEY
        (
        "HIST_MIT",
        "PIN_HISTORY_KEY"
        );
        HIST_MIT:
        while (condition(vw_cwf_Master_Index.readNextRow("HIST_MIT")))
        {
            pnd_Mit_Count.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #MIT-COUNT
            if (condition(cwf_Master_Index_Pnd_Pin_Nbr.notEquals(pnd_Old_Pin_Nbr)))                                                                                       //Natural: IF #PIN-NBR NE #OLD-PIN-NBR
            {
                pnd_Pin_Count.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #PIN-COUNT
                pnd_Old_Pin_Nbr.setValue(cwf_Master_Index_Pnd_Pin_Nbr);                                                                                                   //Natural: ASSIGN #OLD-PIN-NBR = #PIN-NBR
            }                                                                                                                                                             //Natural: END-IF
            //*  BANNER PAGE
            //*  BANNER PAGE (DUPLEX)
            //*  END BANNER PAGE
            //*  END BANNER PAGE (DUPLEX)
        }                                                                                                                                                                 //Natural: END-HISTOGRAM
        if (Global.isEscape()) return;
        //*  WRITE DISTRIBUTION BANNER PAGE
        getReports().write(2, ReportOption.NOHDR, writeMapToStringOutput(Cwff3910.class));                                                                                //Natural: WRITE ( AUDIT ) NOHDR USING FORM 'CWFF3910'
        getReports().write(2, ReportOption.NOHDR, writeMapToStringOutput(Cwff3910.class));                                                                                //Natural: WRITE ( AUDIT ) NOHDR USING FORM 'CWFF3910'
        getReports().write(2, Global.getPROGRAM(),new ColumnSpacing(10),"MASTER INDEX FILE STATISTICS AS OF ",Global.getDATE(),NEWLINE,"-",new RepeatItem(70),            //Natural: WRITE ( AUDIT ) *PROGRAM 10X 'MASTER INDEX FILE STATISTICS AS OF ' *DATE / '-' ( 70 ) // 'TOTAL NUMBER OF ACTIVE RECORDS' #MIT-COUNT // 'TOTAL NUMBER OF POLICYHOLDERS' #PIN-COUNT
            NEWLINE,NEWLINE,"TOTAL NUMBER OF ACTIVE RECORDS",pnd_Mit_Count,NEWLINE,NEWLINE,"TOTAL NUMBER OF POLICYHOLDERS",pnd_Pin_Count);
        if (Global.isEscape()) return;
        //*  WRITE END BANNER PAGE
        getReports().write(2, ReportOption.NOHDR, writeMapToStringOutput(Cwff3911.class));                                                                                //Natural: WRITE ( AUDIT ) NOHDR USING FORM 'CWFF3911'
        getReports().write(2, ReportOption.NOHDR, writeMapToStringOutput(Cwff3911.class));                                                                                //Natural: WRITE ( AUDIT ) NOHDR USING FORM 'CWFF3911'
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(2, "LS=133 PS=60 ZP=OFF");
    }
}
