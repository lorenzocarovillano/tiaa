/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:02:23 PM
**        * FROM NATURAL PROGRAM : Ncsp3020
************************************************************
**        * FILE NAME            : Ncsp3020.java
**        * CLASS NAME           : Ncsp3020
**        * INSTANCE NAME        : Ncsp3020
************************************************************
************************************************************************
* PROGRAM  : NCSP2000
* SYSTEM   : PROJCWF
* TITLE    : PRINT AUDIT REPORT FOR NON-PARTICIPANT CONTACTS (NPCON)
* GENERATED: MARCH 20, 1998
* FUNCTION : THIS PROGRAM READS THE UPLOAD NP AUDIT FILE AND PRODUCES
*            THE FOLLOWING REPORT :
*
* 1.  CMPRT01 - SUCCESSFUL/FAILED NON-PARTICIPANT CONTACT AUDIT REPORT
*
* 02/23/2017 - BHATTKA - PIN EXPANSION - AUG 2017
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Ncsp3020 extends BLNatBase
{
    // Data Areas
    private GdaMcsg000 gdaMcsg000;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_ncw_Mcss_Calls;
    private DbsField ncw_Mcss_Calls_Rcrd_Type_Cde;
    private DbsField ncw_Mcss_Calls_Contact_Id_Cde;
    private DbsField ncw_Mcss_Calls_Np_Pin;
    private DbsField ncw_Mcss_Calls_Status_Cde;
    private DbsField ncw_Mcss_Calls_Lst_Pg_Flg;
    private DbsField ncw_Mcss_Calls_Systm_Ind;
    private DbsField ncw_Mcss_Calls_Rcrd_Stamp_Tme;
    private DbsField ncw_Mcss_Calls_Rcrd_Upld_Tme;
    private DbsField ncw_Mcss_Calls_Rcrd_Contn_Ind;
    private DbsField ncw_Mcss_Calls_Call_Rcvd_Dt_Tme;
    private DbsField ncw_Mcss_Calls_Origin_Racf_Id;
    private DbsField ncw_Mcss_Calls_Origin_Unit_Cde;
    private DbsField ncw_Mcss_Calls_Doc_Ctgry_Cde;
    private DbsField ncw_Mcss_Calls_Doc_Clss_Cde;
    private DbsField ncw_Mcss_Calls_Doc_Spcfc_Cde;
    private DbsField ncw_Mcss_Calls_Spcl_Hndlng_Txt;
    private DbsField ncw_Mcss_Calls_Error_Msg_Txt;
    private DbsField ncw_Mcss_Calls_Wpids_Nbr;
    private DbsField ncw_Mcss_Calls_Text_Lines_Nbr;
    private DbsField ncw_Mcss_Calls_Wpids_Total_Nbr;
    private DbsField ncw_Mcss_Calls_Lines_Total_Nbr;
    private DbsField ncw_Mcss_Calls_Cntct_Total_Nbr;
    private DbsField ncw_Mcss_Calls_Count_Castrequest_Array_Grp;

    private DbsGroup ncw_Mcss_Calls_Request_Array_Grp;
    private DbsField ncw_Mcss_Calls_Action_Cde;
    private DbsField ncw_Mcss_Calls_Wpid_Cde;
    private DbsField ncw_Mcss_Calls_Mit_Log_Dte_Tme;
    private DbsField ncw_Mcss_Calls_Corp_Status_Cde;
    private DbsField ncw_Mcss_Calls_Dstntn_Unt_Cde;
    private DbsField ncw_Mcss_Calls_Type_Of_Prcssng;
    private DbsField ncw_Mcss_Calls_Wrk_Rqst_Priotiry;
    private DbsField ncw_Mcss_Calls_Admin_Status;
    private DbsField ncw_Mcss_Calls_Count_Castdoc_Text_Area_Grp;

    private DbsGroup ncw_Mcss_Calls_Doc_Text_Area_Grp;
    private DbsField ncw_Mcss_Calls_Doc_Txt;

    private DataAccessProgramView vw_ncw_Mcss_Audit;
    private DbsField ncw_Mcss_Audit_Rcrd_Stamp_Tme;
    private DbsField ncw_Mcss_Audit_Rcrd_Upld_Tme;
    private DbsField ncw_Mcss_Audit_Rcrd_Type_Cde;
    private DbsField ncw_Mcss_Audit_Rcrd_Contn_Ind;
    private DbsField ncw_Mcss_Audit_Systm_Ind;
    private DbsField ncw_Mcss_Audit_Np_Pin;
    private DbsField ncw_Mcss_Audit_Contact_Id_Cde;
    private DbsField ncw_Mcss_Audit_Call_Rcvd_Dt_Tme;
    private DbsField ncw_Mcss_Audit_Origin_Racf_Id;
    private DbsField ncw_Mcss_Audit_Origin_Unit_Cde;
    private DbsField ncw_Mcss_Audit_Wpids_Total_Nbr;
    private DbsField ncw_Mcss_Audit_Lines_Total_Nbr;
    private DbsField ncw_Mcss_Audit_Cntct_Total_Nbr;
    private DbsField ncw_Mcss_Audit_Error_Msg_Txt;
    private DbsField ncw_Mcss_Audit_Spcl_Hndlng_Txt;

    private DataAccessProgramView vw_ncw_Mcss_Stats;
    private DbsField ncw_Mcss_Stats_Rcrd_Stamp_Tme;
    private DbsField ncw_Mcss_Stats_Rcrd_Upld_Tme;
    private DbsField ncw_Mcss_Stats_Rcrd_Type_Cde;
    private DbsField ncw_Mcss_Stats_Rcrd_Contn_Ind;
    private DbsField ncw_Mcss_Stats_Systm_Ind;
    private DbsField ncw_Mcss_Stats_Np_Pin;
    private DbsField ncw_Mcss_Stats_Contact_Id_Cde;
    private DbsField ncw_Mcss_Stats_Wpids_Nbr;
    private DbsField ncw_Mcss_Stats_Wpids_Total_Nbr;
    private DbsField ncw_Mcss_Stats_Lines_Total_Nbr;
    private DbsField ncw_Mcss_Stats_Cntct_Total_Nbr;
    private DbsField ncw_Mcss_Stats_Cntct_Error_Nbr;
    private DbsField pnd_Unque_Key;

    private DbsGroup pnd_Unque_Key__R_Field_1;
    private DbsField pnd_Unque_Key_Pnd_Systm;
    private DbsField pnd_Unque_Key_Pnd_Rcrd_Upld_Tme;
    private DbsField pnd_Unque_Key_Pnd_Rcrd_Type_Cde;
    private DbsField pnd_Unque_Key_Pnd_Np_Pin_Nbr;
    private DbsField pnd_Unque_Key_Pnd_Rcrd_Contn_Ind;
    private DbsField pnd_Date_Time;

    private DbsGroup pnd_Date_Time__R_Field_2;
    private DbsField pnd_Date_Time_Pnd_Date;
    private DbsField pnd_Date_Time_Pnd_Time;
    private DbsField pnd_Total_Attempts_Np_Contact;
    private DbsField pnd_Total_Success_Np_Contact;
    private DbsField pnd_Total_Data_Fail_Np_Contact;
    private DbsField pnd_Total_Ntrl_Fail_Np_Contact;
    private DbsField pnd_Total_Good_Wpids;
    private DbsField pnd_Total_Good_Lines;
    private DbsField pnd_Total_Errors;
    private DbsField pnd_Hold_Mit_Log_Dt_Tme;
    private DbsField pnd_Wrk_Rqst_Priotiry;
    private DbsField pnd_Type_Of_Prcssng;
    private DbsField pnd_Corp_Status_Cde;
    private DbsField pnd_Display_Status;
    private DbsField pnd_Input_Data;

    private DbsGroup pnd_Input_Data__R_Field_3;
    private DbsField pnd_Input_Data_Pnd_Input_Date;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;
    private DbsField pnd_I;
    private DbsField pnd_Program;
    private DbsField pnd_Input_Time;
    private DbsField pnd_First_Rec;
    private DbsField pnd_Prev_Btch;
    private DbsField pnd_No_Error;
    private DbsField pnd_Error_Cnt;
    private DbsField pnd_Accept_Reg;
    private DbsField pnd_No_Sign;
    private DbsField pnd_Chk_Dtetme;

    private DbsGroup pnd_Chk_Dtetme__R_Field_4;
    private DbsField pnd_Chk_Dtetme_Pnd_Chk_Date;
    private DbsField pnd_Chk_Dtetme_Pnd_Chk_Time;
    private DbsField pnd_Date_A;

    private DbsGroup pnd_Date_A__R_Field_5;
    private DbsField pnd_Date_A_Pnd_Date_Num;
    private DbsField pnd_Upld_Date;
    private DbsField pnd_Upld_Time;

    private DbsGroup pnd_Upld_Time__R_Field_6;
    private DbsField pnd_Upld_Time_Pnd_Upld_Timen;

    private DbsGroup pnd_Input;
    private DbsField pnd_Input_Pnd_Input_Sys;
    private DbsField pnd_Input_Pnd_Input_From;
    private DbsField pnd_Input_Pnd_Input_Batch;
    private DbsField pnd_Input_From_D;
    private DbsField pnd_Cwf_Mcss_Key;

    private DbsGroup pnd_Cwf_Mcss_Key__R_Field_7;
    private DbsField pnd_Cwf_Mcss_Key_Systm_Ind;
    private DbsField pnd_Cwf_Mcss_Key_Rcrd_Upld_Tme;
    private DbsField pnd_Cwf_Mcss_Key_Rcrd_Type_Cde;
    private DbsField pnd_Cwf_Mcss_Key_Btch_Nbr;
    private DbsField pnd_Cwf_Mcss_Key_Contact_Id_Cde;
    private DbsField pnd_Contact_Key;

    private DbsGroup pnd_Contact_Key__R_Field_8;
    private DbsField pnd_Contact_Key_Systm_Ind;
    private DbsField pnd_Contact_Key_Rcrd_Type_Cde;
    private DbsField pnd_Contact_Key_Contact_Id_Cde;
    private DbsField pnd_Contact_Key_Rcrd_Upld_Tme;
    private DbsField pnd_Cwf_Mcss_Skey;

    private DbsGroup pnd_Cwf_Mcss_Skey__R_Field_9;
    private DbsField pnd_Cwf_Mcss_Skey_Systm_Ind;
    private DbsField pnd_Cwf_Mcss_Skey_Rcrd_Upld_Tme;
    private DbsField pnd_Cwf_Mcss_Skey_Rcrd_Type_Cde;
    private DbsField pnd_Cwf_Mcss_Skey_Btch_Nbr;
    private DbsField pnd_Cwf_Mcss_Skey_Contact_Id_Cde;
    private DbsField pnd_Gen_Tbl_Key;

    private DbsGroup pnd_Gen_Tbl_Key__R_Field_10;
    private DbsField pnd_Gen_Tbl_Key_Pnd_Gen_Tbl_Authrty;
    private DbsField pnd_Gen_Tbl_Key_Pnd_Gen_Tbl_Name;
    private DbsField pnd_Gen_Tbl_Key_Pnd_Gen_Tbl_Code;
    private DbsField pnd_Gen_Tbl_Desc;
    private DbsField pnd_Accepted;
    private DbsField pnd_Prev_Id;
    private DbsField pnd_Printed_Id;
    private DbsField pnd_Prev_Sys;
    private DbsField pnd_Prev_Tme;
    private DbsField pnd_Prev_Error;
    private DbsField pnd_Blank;
    private DbsField pnd_Cnt;
    private DbsField pnd_Contact_Sheet_Systems;
    private DbsField pnd_Tot_Diff;
    private DbsField pnd_Count;
    private DbsField pnd_Rcount;
    private DbsField pnd_Success;
    private DbsField pnd_Date_D;
    private DbsField pnd_Time_S;

    private DbsGroup pnd_Time_S__R_Field_11;
    private DbsField pnd_Time_S_Pnd_Time_N;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMcsg000 = GdaMcsg000.getInstance(getCallnatLevel());
        registerRecord(gdaMcsg000);
        if (gdaOnly) return;

        // Local Variables
        localVariables = new DbsRecord();

        vw_ncw_Mcss_Calls = new DataAccessProgramView(new NameInfo("vw_ncw_Mcss_Calls", "NCW-MCSS-CALLS"), "NCW_MCSS_CALLS", "NCW_MCSS_CALLS", DdmPeriodicGroups.getInstance().getGroups("NCW_MCSS_CALLS"));
        ncw_Mcss_Calls_Rcrd_Type_Cde = vw_ncw_Mcss_Calls.getRecord().newFieldInGroup("ncw_Mcss_Calls_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RCRD_TYPE_CDE");
        ncw_Mcss_Calls_Rcrd_Type_Cde.setDdmHeader("RECORD/TYPE");
        ncw_Mcss_Calls_Contact_Id_Cde = vw_ncw_Mcss_Calls.getRecord().newFieldInGroup("ncw_Mcss_Calls_Contact_Id_Cde", "CONTACT-ID-CDE", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CONTACT_ID_CDE");
        ncw_Mcss_Calls_Contact_Id_Cde.setDdmHeader("CONTACT/ID");
        ncw_Mcss_Calls_Np_Pin = vw_ncw_Mcss_Calls.getRecord().newFieldInGroup("ncw_Mcss_Calls_Np_Pin", "NP-PIN", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "NP_PIN");
        ncw_Mcss_Calls_Np_Pin.setDdmHeader("PIN");
        ncw_Mcss_Calls_Status_Cde = vw_ncw_Mcss_Calls.getRecord().newFieldInGroup("ncw_Mcss_Calls_Status_Cde", "STATUS-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "STATUS_CDE");
        ncw_Mcss_Calls_Lst_Pg_Flg = vw_ncw_Mcss_Calls.getRecord().newFieldInGroup("ncw_Mcss_Calls_Lst_Pg_Flg", "LST-PG-FLG", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "LST_PG_FLG");
        ncw_Mcss_Calls_Systm_Ind = vw_ncw_Mcss_Calls.getRecord().newFieldInGroup("ncw_Mcss_Calls_Systm_Ind", "SYSTM-IND", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "SYSTM_IND");
        ncw_Mcss_Calls_Systm_Ind.setDdmHeader("SYSTEM/OF/ORIGIN");
        ncw_Mcss_Calls_Rcrd_Stamp_Tme = vw_ncw_Mcss_Calls.getRecord().newFieldInGroup("ncw_Mcss_Calls_Rcrd_Stamp_Tme", "RCRD-STAMP-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "RCRD_STAMP_TME");
        ncw_Mcss_Calls_Rcrd_Stamp_Tme.setDdmHeader("RECORD/TIME/STAMP");
        ncw_Mcss_Calls_Rcrd_Upld_Tme = vw_ncw_Mcss_Calls.getRecord().newFieldInGroup("ncw_Mcss_Calls_Rcrd_Upld_Tme", "RCRD-UPLD-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "RCRD_UPLD_TME");
        ncw_Mcss_Calls_Rcrd_Upld_Tme.setDdmHeader("RECORD/UPLOAD/TIME");
        ncw_Mcss_Calls_Rcrd_Contn_Ind = vw_ncw_Mcss_Calls.getRecord().newFieldInGroup("ncw_Mcss_Calls_Rcrd_Contn_Ind", "RCRD-CONTN-IND", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "RCRD_CONTN_IND");
        ncw_Mcss_Calls_Call_Rcvd_Dt_Tme = vw_ncw_Mcss_Calls.getRecord().newFieldInGroup("ncw_Mcss_Calls_Call_Rcvd_Dt_Tme", "CALL-RCVD-DT-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "CALL_RCVD_DT_TME");
        ncw_Mcss_Calls_Origin_Racf_Id = vw_ncw_Mcss_Calls.getRecord().newFieldInGroup("ncw_Mcss_Calls_Origin_Racf_Id", "ORIGIN-RACF-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ORIGIN_RACF_ID");
        ncw_Mcss_Calls_Origin_Racf_Id.setDdmHeader("ORIGIN/RACF/ID");
        ncw_Mcss_Calls_Origin_Unit_Cde = vw_ncw_Mcss_Calls.getRecord().newFieldInGroup("ncw_Mcss_Calls_Origin_Unit_Cde", "ORIGIN-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ORIGIN_UNIT_CDE");
        ncw_Mcss_Calls_Origin_Unit_Cde.setDdmHeader("ORIGIN/UNIT");
        ncw_Mcss_Calls_Doc_Ctgry_Cde = vw_ncw_Mcss_Calls.getRecord().newFieldInGroup("ncw_Mcss_Calls_Doc_Ctgry_Cde", "DOC-CTGRY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DOC_CTGRY_CDE");
        ncw_Mcss_Calls_Doc_Clss_Cde = vw_ncw_Mcss_Calls.getRecord().newFieldInGroup("ncw_Mcss_Calls_Doc_Clss_Cde", "DOC-CLSS-CDE", FieldType.STRING, 3, 
            RepeatingFieldStrategy.None, "DOC_CLSS_CDE");
        ncw_Mcss_Calls_Doc_Spcfc_Cde = vw_ncw_Mcss_Calls.getRecord().newFieldInGroup("ncw_Mcss_Calls_Doc_Spcfc_Cde", "DOC-SPCFC-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "DOC_SPCFC_CDE");
        ncw_Mcss_Calls_Spcl_Hndlng_Txt = vw_ncw_Mcss_Calls.getRecord().newFieldInGroup("ncw_Mcss_Calls_Spcl_Hndlng_Txt", "SPCL-HNDLNG-TXT", FieldType.STRING, 
            45, RepeatingFieldStrategy.None, "SPCL_HNDLNG_TXT");
        ncw_Mcss_Calls_Spcl_Hndlng_Txt.setDdmHeader("SPECIAL/HANDLING");
        ncw_Mcss_Calls_Error_Msg_Txt = vw_ncw_Mcss_Calls.getRecord().newFieldInGroup("ncw_Mcss_Calls_Error_Msg_Txt", "ERROR-MSG-TXT", FieldType.STRING, 
            80, RepeatingFieldStrategy.None, "ERROR_MSG_TXT");
        ncw_Mcss_Calls_Error_Msg_Txt.setDdmHeader("ERROR/MSG.");
        ncw_Mcss_Calls_Wpids_Nbr = vw_ncw_Mcss_Calls.getRecord().newFieldInGroup("ncw_Mcss_Calls_Wpids_Nbr", "WPIDS-NBR", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, 
            "WPIDS_NBR");
        ncw_Mcss_Calls_Text_Lines_Nbr = vw_ncw_Mcss_Calls.getRecord().newFieldInGroup("ncw_Mcss_Calls_Text_Lines_Nbr", "TEXT-LINES-NBR", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "TEXT_LINES_NBR");
        ncw_Mcss_Calls_Wpids_Total_Nbr = vw_ncw_Mcss_Calls.getRecord().newFieldInGroup("ncw_Mcss_Calls_Wpids_Total_Nbr", "WPIDS-TOTAL-NBR", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "WPIDS_TOTAL_NBR");
        ncw_Mcss_Calls_Lines_Total_Nbr = vw_ncw_Mcss_Calls.getRecord().newFieldInGroup("ncw_Mcss_Calls_Lines_Total_Nbr", "LINES-TOTAL-NBR", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "LINES_TOTAL_NBR");
        ncw_Mcss_Calls_Cntct_Total_Nbr = vw_ncw_Mcss_Calls.getRecord().newFieldInGroup("ncw_Mcss_Calls_Cntct_Total_Nbr", "CNTCT-TOTAL-NBR", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTCT_TOTAL_NBR");
        ncw_Mcss_Calls_Count_Castrequest_Array_Grp = vw_ncw_Mcss_Calls.getRecord().newFieldInGroup("ncw_Mcss_Calls_Count_Castrequest_Array_Grp", "C*REQUEST-ARRAY-GRP", 
            RepeatingFieldStrategy.CAsteriskVariable, "NCW_MCSS_CALLS_REQUEST_ARRAY_GRP");

        ncw_Mcss_Calls_Request_Array_Grp = vw_ncw_Mcss_Calls.getRecord().newGroupArrayInGroup("ncw_Mcss_Calls_Request_Array_Grp", "REQUEST-ARRAY-GRP", 
            new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NCW_MCSS_CALLS_REQUEST_ARRAY_GRP");
        ncw_Mcss_Calls_Action_Cde = ncw_Mcss_Calls_Request_Array_Grp.newFieldInGroup("ncw_Mcss_Calls_Action_Cde", "ACTION-CDE", FieldType.STRING, 2, null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACTION_CDE", "NCW_MCSS_CALLS_REQUEST_ARRAY_GRP");
        ncw_Mcss_Calls_Wpid_Cde = ncw_Mcss_Calls_Request_Array_Grp.newFieldInGroup("ncw_Mcss_Calls_Wpid_Cde", "WPID-CDE", FieldType.STRING, 6, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "WPID_CDE", "NCW_MCSS_CALLS_REQUEST_ARRAY_GRP");
        ncw_Mcss_Calls_Mit_Log_Dte_Tme = ncw_Mcss_Calls_Request_Array_Grp.newFieldInGroup("ncw_Mcss_Calls_Mit_Log_Dte_Tme", "MIT-LOG-DTE-TME", FieldType.TIME, 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "MIT_LOG_DTE_TME", "NCW_MCSS_CALLS_REQUEST_ARRAY_GRP");
        ncw_Mcss_Calls_Corp_Status_Cde = ncw_Mcss_Calls_Request_Array_Grp.newFieldInGroup("ncw_Mcss_Calls_Corp_Status_Cde", "CORP-STATUS-CDE", FieldType.STRING, 
            1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CORP_STATUS_CDE", "NCW_MCSS_CALLS_REQUEST_ARRAY_GRP");
        ncw_Mcss_Calls_Dstntn_Unt_Cde = ncw_Mcss_Calls_Request_Array_Grp.newFieldInGroup("ncw_Mcss_Calls_Dstntn_Unt_Cde", "DSTNTN-UNT-CDE", FieldType.STRING, 
            8, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "DSTNTN_UNT_CDE", "NCW_MCSS_CALLS_REQUEST_ARRAY_GRP");
        ncw_Mcss_Calls_Type_Of_Prcssng = ncw_Mcss_Calls_Request_Array_Grp.newFieldInGroup("ncw_Mcss_Calls_Type_Of_Prcssng", "TYPE-OF-PRCSSNG", FieldType.STRING, 
            1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "TYPE_OF_PRCSSNG", "NCW_MCSS_CALLS_REQUEST_ARRAY_GRP");
        ncw_Mcss_Calls_Wrk_Rqst_Priotiry = ncw_Mcss_Calls_Request_Array_Grp.newFieldInGroup("ncw_Mcss_Calls_Wrk_Rqst_Priotiry", "WRK-RQST-PRIOTIRY", FieldType.STRING, 
            1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "WRK_RQST_PRIOTIRY", "NCW_MCSS_CALLS_REQUEST_ARRAY_GRP");
        ncw_Mcss_Calls_Admin_Status = ncw_Mcss_Calls_Request_Array_Grp.newFieldInGroup("ncw_Mcss_Calls_Admin_Status", "ADMIN-STATUS", FieldType.STRING, 
            4, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADMIN_STATUS", "NCW_MCSS_CALLS_REQUEST_ARRAY_GRP");
        ncw_Mcss_Calls_Count_Castdoc_Text_Area_Grp = vw_ncw_Mcss_Calls.getRecord().newFieldInGroup("ncw_Mcss_Calls_Count_Castdoc_Text_Area_Grp", "C*DOC-TEXT-AREA-GRP", 
            RepeatingFieldStrategy.CAsteriskVariable, "NCW_MCSS_CALLS_DOC_TEXT_AREA_GRP");

        ncw_Mcss_Calls_Doc_Text_Area_Grp = vw_ncw_Mcss_Calls.getRecord().newGroupArrayInGroup("ncw_Mcss_Calls_Doc_Text_Area_Grp", "DOC-TEXT-AREA-GRP", 
            new DbsArrayController(1, 50) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "NCW_MCSS_CALLS_DOC_TEXT_AREA_GRP");
        ncw_Mcss_Calls_Doc_Txt = ncw_Mcss_Calls_Doc_Text_Area_Grp.newFieldInGroup("ncw_Mcss_Calls_Doc_Txt", "DOC-TXT", FieldType.STRING, 80, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "DOC_TXT", "NCW_MCSS_CALLS_DOC_TEXT_AREA_GRP");
        registerRecord(vw_ncw_Mcss_Calls);

        vw_ncw_Mcss_Audit = new DataAccessProgramView(new NameInfo("vw_ncw_Mcss_Audit", "NCW-MCSS-AUDIT"), "NCW_MCSS_CALLS", "NCW_MCSS_CALLS");
        ncw_Mcss_Audit_Rcrd_Stamp_Tme = vw_ncw_Mcss_Audit.getRecord().newFieldInGroup("ncw_Mcss_Audit_Rcrd_Stamp_Tme", "RCRD-STAMP-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "RCRD_STAMP_TME");
        ncw_Mcss_Audit_Rcrd_Stamp_Tme.setDdmHeader("RECORD/TIME/STAMP");
        ncw_Mcss_Audit_Rcrd_Upld_Tme = vw_ncw_Mcss_Audit.getRecord().newFieldInGroup("ncw_Mcss_Audit_Rcrd_Upld_Tme", "RCRD-UPLD-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "RCRD_UPLD_TME");
        ncw_Mcss_Audit_Rcrd_Upld_Tme.setDdmHeader("RECORD/UPLOAD/TIME");
        ncw_Mcss_Audit_Rcrd_Type_Cde = vw_ncw_Mcss_Audit.getRecord().newFieldInGroup("ncw_Mcss_Audit_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RCRD_TYPE_CDE");
        ncw_Mcss_Audit_Rcrd_Type_Cde.setDdmHeader("RECORD/TYPE");
        ncw_Mcss_Audit_Rcrd_Contn_Ind = vw_ncw_Mcss_Audit.getRecord().newFieldInGroup("ncw_Mcss_Audit_Rcrd_Contn_Ind", "RCRD-CONTN-IND", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "RCRD_CONTN_IND");
        ncw_Mcss_Audit_Systm_Ind = vw_ncw_Mcss_Audit.getRecord().newFieldInGroup("ncw_Mcss_Audit_Systm_Ind", "SYSTM-IND", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "SYSTM_IND");
        ncw_Mcss_Audit_Systm_Ind.setDdmHeader("SYSTEM/OF/ORIGIN");
        ncw_Mcss_Audit_Np_Pin = vw_ncw_Mcss_Audit.getRecord().newFieldInGroup("ncw_Mcss_Audit_Np_Pin", "NP-PIN", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "NP_PIN");
        ncw_Mcss_Audit_Np_Pin.setDdmHeader("PIN");
        ncw_Mcss_Audit_Contact_Id_Cde = vw_ncw_Mcss_Audit.getRecord().newFieldInGroup("ncw_Mcss_Audit_Contact_Id_Cde", "CONTACT-ID-CDE", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CONTACT_ID_CDE");
        ncw_Mcss_Audit_Contact_Id_Cde.setDdmHeader("CONTACT/ID");
        ncw_Mcss_Audit_Call_Rcvd_Dt_Tme = vw_ncw_Mcss_Audit.getRecord().newFieldInGroup("ncw_Mcss_Audit_Call_Rcvd_Dt_Tme", "CALL-RCVD-DT-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "CALL_RCVD_DT_TME");
        ncw_Mcss_Audit_Origin_Racf_Id = vw_ncw_Mcss_Audit.getRecord().newFieldInGroup("ncw_Mcss_Audit_Origin_Racf_Id", "ORIGIN-RACF-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ORIGIN_RACF_ID");
        ncw_Mcss_Audit_Origin_Racf_Id.setDdmHeader("ORIGIN/RACF/ID");
        ncw_Mcss_Audit_Origin_Unit_Cde = vw_ncw_Mcss_Audit.getRecord().newFieldInGroup("ncw_Mcss_Audit_Origin_Unit_Cde", "ORIGIN-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ORIGIN_UNIT_CDE");
        ncw_Mcss_Audit_Origin_Unit_Cde.setDdmHeader("ORIGIN/UNIT");
        ncw_Mcss_Audit_Wpids_Total_Nbr = vw_ncw_Mcss_Audit.getRecord().newFieldInGroup("ncw_Mcss_Audit_Wpids_Total_Nbr", "WPIDS-TOTAL-NBR", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "WPIDS_TOTAL_NBR");
        ncw_Mcss_Audit_Lines_Total_Nbr = vw_ncw_Mcss_Audit.getRecord().newFieldInGroup("ncw_Mcss_Audit_Lines_Total_Nbr", "LINES-TOTAL-NBR", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "LINES_TOTAL_NBR");
        ncw_Mcss_Audit_Cntct_Total_Nbr = vw_ncw_Mcss_Audit.getRecord().newFieldInGroup("ncw_Mcss_Audit_Cntct_Total_Nbr", "CNTCT-TOTAL-NBR", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTCT_TOTAL_NBR");
        ncw_Mcss_Audit_Error_Msg_Txt = vw_ncw_Mcss_Audit.getRecord().newFieldInGroup("ncw_Mcss_Audit_Error_Msg_Txt", "ERROR-MSG-TXT", FieldType.STRING, 
            80, RepeatingFieldStrategy.None, "ERROR_MSG_TXT");
        ncw_Mcss_Audit_Error_Msg_Txt.setDdmHeader("ERROR/MSG.");
        ncw_Mcss_Audit_Spcl_Hndlng_Txt = vw_ncw_Mcss_Audit.getRecord().newFieldInGroup("ncw_Mcss_Audit_Spcl_Hndlng_Txt", "SPCL-HNDLNG-TXT", FieldType.STRING, 
            45, RepeatingFieldStrategy.None, "SPCL_HNDLNG_TXT");
        ncw_Mcss_Audit_Spcl_Hndlng_Txt.setDdmHeader("SPECIAL/HANDLING");
        registerRecord(vw_ncw_Mcss_Audit);

        vw_ncw_Mcss_Stats = new DataAccessProgramView(new NameInfo("vw_ncw_Mcss_Stats", "NCW-MCSS-STATS"), "NCW_MCSS_CALLS", "NCW_MCSS_CALLS");
        ncw_Mcss_Stats_Rcrd_Stamp_Tme = vw_ncw_Mcss_Stats.getRecord().newFieldInGroup("ncw_Mcss_Stats_Rcrd_Stamp_Tme", "RCRD-STAMP-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "RCRD_STAMP_TME");
        ncw_Mcss_Stats_Rcrd_Stamp_Tme.setDdmHeader("RECORD/TIME/STAMP");
        ncw_Mcss_Stats_Rcrd_Upld_Tme = vw_ncw_Mcss_Stats.getRecord().newFieldInGroup("ncw_Mcss_Stats_Rcrd_Upld_Tme", "RCRD-UPLD-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "RCRD_UPLD_TME");
        ncw_Mcss_Stats_Rcrd_Upld_Tme.setDdmHeader("RECORD/UPLOAD/TIME");
        ncw_Mcss_Stats_Rcrd_Type_Cde = vw_ncw_Mcss_Stats.getRecord().newFieldInGroup("ncw_Mcss_Stats_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RCRD_TYPE_CDE");
        ncw_Mcss_Stats_Rcrd_Type_Cde.setDdmHeader("RECORD/TYPE");
        ncw_Mcss_Stats_Rcrd_Contn_Ind = vw_ncw_Mcss_Stats.getRecord().newFieldInGroup("ncw_Mcss_Stats_Rcrd_Contn_Ind", "RCRD-CONTN-IND", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "RCRD_CONTN_IND");
        ncw_Mcss_Stats_Systm_Ind = vw_ncw_Mcss_Stats.getRecord().newFieldInGroup("ncw_Mcss_Stats_Systm_Ind", "SYSTM-IND", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "SYSTM_IND");
        ncw_Mcss_Stats_Systm_Ind.setDdmHeader("SYSTEM/OF/ORIGIN");
        ncw_Mcss_Stats_Np_Pin = vw_ncw_Mcss_Stats.getRecord().newFieldInGroup("ncw_Mcss_Stats_Np_Pin", "NP-PIN", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "NP_PIN");
        ncw_Mcss_Stats_Np_Pin.setDdmHeader("PIN");
        ncw_Mcss_Stats_Contact_Id_Cde = vw_ncw_Mcss_Stats.getRecord().newFieldInGroup("ncw_Mcss_Stats_Contact_Id_Cde", "CONTACT-ID-CDE", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CONTACT_ID_CDE");
        ncw_Mcss_Stats_Contact_Id_Cde.setDdmHeader("CONTACT/ID");
        ncw_Mcss_Stats_Wpids_Nbr = vw_ncw_Mcss_Stats.getRecord().newFieldInGroup("ncw_Mcss_Stats_Wpids_Nbr", "WPIDS-NBR", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, 
            "WPIDS_NBR");
        ncw_Mcss_Stats_Wpids_Total_Nbr = vw_ncw_Mcss_Stats.getRecord().newFieldInGroup("ncw_Mcss_Stats_Wpids_Total_Nbr", "WPIDS-TOTAL-NBR", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "WPIDS_TOTAL_NBR");
        ncw_Mcss_Stats_Lines_Total_Nbr = vw_ncw_Mcss_Stats.getRecord().newFieldInGroup("ncw_Mcss_Stats_Lines_Total_Nbr", "LINES-TOTAL-NBR", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "LINES_TOTAL_NBR");
        ncw_Mcss_Stats_Cntct_Total_Nbr = vw_ncw_Mcss_Stats.getRecord().newFieldInGroup("ncw_Mcss_Stats_Cntct_Total_Nbr", "CNTCT-TOTAL-NBR", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTCT_TOTAL_NBR");
        ncw_Mcss_Stats_Cntct_Error_Nbr = vw_ncw_Mcss_Stats.getRecord().newFieldInGroup("ncw_Mcss_Stats_Cntct_Error_Nbr", "CNTCT-ERROR-NBR", FieldType.NUMERIC, 
            4, RepeatingFieldStrategy.None, "CNTCT_ERROR_NBR");
        registerRecord(vw_ncw_Mcss_Stats);

        pnd_Unque_Key = localVariables.newFieldInRecord("pnd_Unque_Key", "#UNQUE-KEY", FieldType.STRING, 37);

        pnd_Unque_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Unque_Key__R_Field_1", "REDEFINE", pnd_Unque_Key);
        pnd_Unque_Key_Pnd_Systm = pnd_Unque_Key__R_Field_1.newFieldInGroup("pnd_Unque_Key_Pnd_Systm", "#SYSTM", FieldType.STRING, 15);
        pnd_Unque_Key_Pnd_Rcrd_Upld_Tme = pnd_Unque_Key__R_Field_1.newFieldInGroup("pnd_Unque_Key_Pnd_Rcrd_Upld_Tme", "#RCRD-UPLD-TME", FieldType.TIME);
        pnd_Unque_Key_Pnd_Rcrd_Type_Cde = pnd_Unque_Key__R_Field_1.newFieldInGroup("pnd_Unque_Key_Pnd_Rcrd_Type_Cde", "#RCRD-TYPE-CDE", FieldType.STRING, 
            1);
        pnd_Unque_Key_Pnd_Np_Pin_Nbr = pnd_Unque_Key__R_Field_1.newFieldInGroup("pnd_Unque_Key_Pnd_Np_Pin_Nbr", "#NP-PIN-NBR", FieldType.NUMERIC, 12);
        pnd_Unque_Key_Pnd_Rcrd_Contn_Ind = pnd_Unque_Key__R_Field_1.newFieldInGroup("pnd_Unque_Key_Pnd_Rcrd_Contn_Ind", "#RCRD-CONTN-IND", FieldType.NUMERIC, 
            2);
        pnd_Date_Time = localVariables.newFieldInRecord("pnd_Date_Time", "#DATE-TIME", FieldType.STRING, 15);

        pnd_Date_Time__R_Field_2 = localVariables.newGroupInRecord("pnd_Date_Time__R_Field_2", "REDEFINE", pnd_Date_Time);
        pnd_Date_Time_Pnd_Date = pnd_Date_Time__R_Field_2.newFieldInGroup("pnd_Date_Time_Pnd_Date", "#DATE", FieldType.STRING, 8);
        pnd_Date_Time_Pnd_Time = pnd_Date_Time__R_Field_2.newFieldInGroup("pnd_Date_Time_Pnd_Time", "#TIME", FieldType.STRING, 7);
        pnd_Total_Attempts_Np_Contact = localVariables.newFieldInRecord("pnd_Total_Attempts_Np_Contact", "#TOTAL-ATTEMPTS-NP-CONTACT", FieldType.NUMERIC, 
            10);
        pnd_Total_Success_Np_Contact = localVariables.newFieldInRecord("pnd_Total_Success_Np_Contact", "#TOTAL-SUCCESS-NP-CONTACT", FieldType.NUMERIC, 
            10);
        pnd_Total_Data_Fail_Np_Contact = localVariables.newFieldInRecord("pnd_Total_Data_Fail_Np_Contact", "#TOTAL-DATA-FAIL-NP-CONTACT", FieldType.NUMERIC, 
            10);
        pnd_Total_Ntrl_Fail_Np_Contact = localVariables.newFieldInRecord("pnd_Total_Ntrl_Fail_Np_Contact", "#TOTAL-NTRL-FAIL-NP-CONTACT", FieldType.NUMERIC, 
            10);
        pnd_Total_Good_Wpids = localVariables.newFieldInRecord("pnd_Total_Good_Wpids", "#TOTAL-GOOD-WPIDS", FieldType.NUMERIC, 10);
        pnd_Total_Good_Lines = localVariables.newFieldInRecord("pnd_Total_Good_Lines", "#TOTAL-GOOD-LINES", FieldType.NUMERIC, 10);
        pnd_Total_Errors = localVariables.newFieldInRecord("pnd_Total_Errors", "#TOTAL-ERRORS", FieldType.NUMERIC, 10);
        pnd_Hold_Mit_Log_Dt_Tme = localVariables.newFieldArrayInRecord("pnd_Hold_Mit_Log_Dt_Tme", "#HOLD-MIT-LOG-DT-TME", FieldType.TIME, new DbsArrayController(1, 
            10));
        pnd_Wrk_Rqst_Priotiry = localVariables.newFieldInRecord("pnd_Wrk_Rqst_Priotiry", "#WRK-RQST-PRIOTIRY", FieldType.STRING, 6);
        pnd_Type_Of_Prcssng = localVariables.newFieldInRecord("pnd_Type_Of_Prcssng", "#TYPE-OF-PRCSSNG", FieldType.STRING, 6);
        pnd_Corp_Status_Cde = localVariables.newFieldInRecord("pnd_Corp_Status_Cde", "#CORP-STATUS-CDE", FieldType.STRING, 6);
        pnd_Display_Status = localVariables.newFieldInRecord("pnd_Display_Status", "#DISPLAY-STATUS", FieldType.STRING, 10);
        pnd_Input_Data = localVariables.newFieldInRecord("pnd_Input_Data", "#INPUT-DATA", FieldType.STRING, 30);

        pnd_Input_Data__R_Field_3 = localVariables.newGroupInRecord("pnd_Input_Data__R_Field_3", "REDEFINE", pnd_Input_Data);
        pnd_Input_Data_Pnd_Input_Date = pnd_Input_Data__R_Field_3.newFieldInGroup("pnd_Input_Data_Pnd_Input_Date", "#INPUT-DATE", FieldType.STRING, 8);

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Support_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");
        registerRecord(vw_cwf_Support_Tbl);

        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Input_Time = localVariables.newFieldInRecord("pnd_Input_Time", "#INPUT-TIME", FieldType.PACKED_DECIMAL, 6);
        pnd_First_Rec = localVariables.newFieldInRecord("pnd_First_Rec", "#FIRST-REC", FieldType.STRING, 1);
        pnd_Prev_Btch = localVariables.newFieldInRecord("pnd_Prev_Btch", "#PREV-BTCH", FieldType.NUMERIC, 8);
        pnd_No_Error = localVariables.newFieldInRecord("pnd_No_Error", "#NO-ERROR", FieldType.NUMERIC, 4);
        pnd_Error_Cnt = localVariables.newFieldInRecord("pnd_Error_Cnt", "#ERROR-CNT", FieldType.NUMERIC, 4);
        pnd_Accept_Reg = localVariables.newFieldInRecord("pnd_Accept_Reg", "#ACCEPT-REG", FieldType.STRING, 1);
        pnd_No_Sign = localVariables.newFieldInRecord("pnd_No_Sign", "#NO-SIGN", FieldType.STRING, 1);
        pnd_Chk_Dtetme = localVariables.newFieldInRecord("pnd_Chk_Dtetme", "#CHK-DTETME", FieldType.STRING, 14);

        pnd_Chk_Dtetme__R_Field_4 = localVariables.newGroupInRecord("pnd_Chk_Dtetme__R_Field_4", "REDEFINE", pnd_Chk_Dtetme);
        pnd_Chk_Dtetme_Pnd_Chk_Date = pnd_Chk_Dtetme__R_Field_4.newFieldInGroup("pnd_Chk_Dtetme_Pnd_Chk_Date", "#CHK-DATE", FieldType.STRING, 8);
        pnd_Chk_Dtetme_Pnd_Chk_Time = pnd_Chk_Dtetme__R_Field_4.newFieldInGroup("pnd_Chk_Dtetme_Pnd_Chk_Time", "#CHK-TIME", FieldType.STRING, 6);
        pnd_Date_A = localVariables.newFieldInRecord("pnd_Date_A", "#DATE-A", FieldType.STRING, 14);

        pnd_Date_A__R_Field_5 = localVariables.newGroupInRecord("pnd_Date_A__R_Field_5", "REDEFINE", pnd_Date_A);
        pnd_Date_A_Pnd_Date_Num = pnd_Date_A__R_Field_5.newFieldInGroup("pnd_Date_A_Pnd_Date_Num", "#DATE-NUM", FieldType.NUMERIC, 14);
        pnd_Upld_Date = localVariables.newFieldInRecord("pnd_Upld_Date", "#UPLD-DATE", FieldType.DATE);
        pnd_Upld_Time = localVariables.newFieldInRecord("pnd_Upld_Time", "#UPLD-TIME", FieldType.STRING, 4);

        pnd_Upld_Time__R_Field_6 = localVariables.newGroupInRecord("pnd_Upld_Time__R_Field_6", "REDEFINE", pnd_Upld_Time);
        pnd_Upld_Time_Pnd_Upld_Timen = pnd_Upld_Time__R_Field_6.newFieldInGroup("pnd_Upld_Time_Pnd_Upld_Timen", "#UPLD-TIMEN", FieldType.NUMERIC, 4);

        pnd_Input = localVariables.newGroupInRecord("pnd_Input", "#INPUT");
        pnd_Input_Pnd_Input_Sys = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Input_Sys", "#INPUT-SYS", FieldType.STRING, 15);
        pnd_Input_Pnd_Input_From = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Input_From", "#INPUT-FROM", FieldType.STRING, 8);
        pnd_Input_Pnd_Input_Batch = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Input_Batch", "#INPUT-BATCH", FieldType.NUMERIC, 8);
        pnd_Input_From_D = localVariables.newFieldInRecord("pnd_Input_From_D", "#INPUT-FROM-D", FieldType.DATE);
        pnd_Cwf_Mcss_Key = localVariables.newFieldInRecord("pnd_Cwf_Mcss_Key", "#CWF-MCSS-KEY", FieldType.STRING, 41);

        pnd_Cwf_Mcss_Key__R_Field_7 = localVariables.newGroupInRecord("pnd_Cwf_Mcss_Key__R_Field_7", "REDEFINE", pnd_Cwf_Mcss_Key);
        pnd_Cwf_Mcss_Key_Systm_Ind = pnd_Cwf_Mcss_Key__R_Field_7.newFieldInGroup("pnd_Cwf_Mcss_Key_Systm_Ind", "SYSTM-IND", FieldType.STRING, 15);
        pnd_Cwf_Mcss_Key_Rcrd_Upld_Tme = pnd_Cwf_Mcss_Key__R_Field_7.newFieldInGroup("pnd_Cwf_Mcss_Key_Rcrd_Upld_Tme", "RCRD-UPLD-TME", FieldType.TIME);
        pnd_Cwf_Mcss_Key_Rcrd_Type_Cde = pnd_Cwf_Mcss_Key__R_Field_7.newFieldInGroup("pnd_Cwf_Mcss_Key_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 
            1);
        pnd_Cwf_Mcss_Key_Btch_Nbr = pnd_Cwf_Mcss_Key__R_Field_7.newFieldInGroup("pnd_Cwf_Mcss_Key_Btch_Nbr", "BTCH-NBR", FieldType.NUMERIC, 8);
        pnd_Cwf_Mcss_Key_Contact_Id_Cde = pnd_Cwf_Mcss_Key__R_Field_7.newFieldInGroup("pnd_Cwf_Mcss_Key_Contact_Id_Cde", "CONTACT-ID-CDE", FieldType.STRING, 
            10);
        pnd_Contact_Key = localVariables.newFieldInRecord("pnd_Contact_Key", "#CONTACT-KEY", FieldType.STRING, 33);

        pnd_Contact_Key__R_Field_8 = localVariables.newGroupInRecord("pnd_Contact_Key__R_Field_8", "REDEFINE", pnd_Contact_Key);
        pnd_Contact_Key_Systm_Ind = pnd_Contact_Key__R_Field_8.newFieldInGroup("pnd_Contact_Key_Systm_Ind", "SYSTM-IND", FieldType.STRING, 15);
        pnd_Contact_Key_Rcrd_Type_Cde = pnd_Contact_Key__R_Field_8.newFieldInGroup("pnd_Contact_Key_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 
            1);
        pnd_Contact_Key_Contact_Id_Cde = pnd_Contact_Key__R_Field_8.newFieldInGroup("pnd_Contact_Key_Contact_Id_Cde", "CONTACT-ID-CDE", FieldType.STRING, 
            10);
        pnd_Contact_Key_Rcrd_Upld_Tme = pnd_Contact_Key__R_Field_8.newFieldInGroup("pnd_Contact_Key_Rcrd_Upld_Tme", "RCRD-UPLD-TME", FieldType.TIME);
        pnd_Cwf_Mcss_Skey = localVariables.newFieldInRecord("pnd_Cwf_Mcss_Skey", "#CWF-MCSS-SKEY", FieldType.STRING, 41);

        pnd_Cwf_Mcss_Skey__R_Field_9 = localVariables.newGroupInRecord("pnd_Cwf_Mcss_Skey__R_Field_9", "REDEFINE", pnd_Cwf_Mcss_Skey);
        pnd_Cwf_Mcss_Skey_Systm_Ind = pnd_Cwf_Mcss_Skey__R_Field_9.newFieldInGroup("pnd_Cwf_Mcss_Skey_Systm_Ind", "SYSTM-IND", FieldType.STRING, 15);
        pnd_Cwf_Mcss_Skey_Rcrd_Upld_Tme = pnd_Cwf_Mcss_Skey__R_Field_9.newFieldInGroup("pnd_Cwf_Mcss_Skey_Rcrd_Upld_Tme", "RCRD-UPLD-TME", FieldType.TIME);
        pnd_Cwf_Mcss_Skey_Rcrd_Type_Cde = pnd_Cwf_Mcss_Skey__R_Field_9.newFieldInGroup("pnd_Cwf_Mcss_Skey_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 
            1);
        pnd_Cwf_Mcss_Skey_Btch_Nbr = pnd_Cwf_Mcss_Skey__R_Field_9.newFieldInGroup("pnd_Cwf_Mcss_Skey_Btch_Nbr", "BTCH-NBR", FieldType.NUMERIC, 8);
        pnd_Cwf_Mcss_Skey_Contact_Id_Cde = pnd_Cwf_Mcss_Skey__R_Field_9.newFieldInGroup("pnd_Cwf_Mcss_Skey_Contact_Id_Cde", "CONTACT-ID-CDE", FieldType.STRING, 
            10);
        pnd_Gen_Tbl_Key = localVariables.newFieldInRecord("pnd_Gen_Tbl_Key", "#GEN-TBL-KEY", FieldType.STRING, 53);

        pnd_Gen_Tbl_Key__R_Field_10 = localVariables.newGroupInRecord("pnd_Gen_Tbl_Key__R_Field_10", "REDEFINE", pnd_Gen_Tbl_Key);
        pnd_Gen_Tbl_Key_Pnd_Gen_Tbl_Authrty = pnd_Gen_Tbl_Key__R_Field_10.newFieldInGroup("pnd_Gen_Tbl_Key_Pnd_Gen_Tbl_Authrty", "#GEN-TBL-AUTHRTY", FieldType.STRING, 
            2);
        pnd_Gen_Tbl_Key_Pnd_Gen_Tbl_Name = pnd_Gen_Tbl_Key__R_Field_10.newFieldInGroup("pnd_Gen_Tbl_Key_Pnd_Gen_Tbl_Name", "#GEN-TBL-NAME", FieldType.STRING, 
            20);
        pnd_Gen_Tbl_Key_Pnd_Gen_Tbl_Code = pnd_Gen_Tbl_Key__R_Field_10.newFieldInGroup("pnd_Gen_Tbl_Key_Pnd_Gen_Tbl_Code", "#GEN-TBL-CODE", FieldType.STRING, 
            30);
        pnd_Gen_Tbl_Desc = localVariables.newFieldInRecord("pnd_Gen_Tbl_Desc", "#GEN-TBL-DESC", FieldType.STRING, 30);
        pnd_Accepted = localVariables.newFieldInRecord("pnd_Accepted", "#ACCEPTED", FieldType.STRING, 1);
        pnd_Prev_Id = localVariables.newFieldInRecord("pnd_Prev_Id", "#PREV-ID", FieldType.STRING, 10);
        pnd_Printed_Id = localVariables.newFieldInRecord("pnd_Printed_Id", "#PRINTED-ID", FieldType.STRING, 10);
        pnd_Prev_Sys = localVariables.newFieldInRecord("pnd_Prev_Sys", "#PREV-SYS", FieldType.STRING, 15);
        pnd_Prev_Tme = localVariables.newFieldInRecord("pnd_Prev_Tme", "#PREV-TME", FieldType.TIME);
        pnd_Prev_Error = localVariables.newFieldInRecord("pnd_Prev_Error", "#PREV-ERROR", FieldType.STRING, 60);
        pnd_Blank = localVariables.newFieldInRecord("pnd_Blank", "#BLANK", FieldType.STRING, 10);
        pnd_Cnt = localVariables.newFieldInRecord("pnd_Cnt", "#CNT", FieldType.NUMERIC, 3);
        pnd_Contact_Sheet_Systems = localVariables.newFieldArrayInRecord("pnd_Contact_Sheet_Systems", "#CONTACT-SHEET-SYSTEMS", FieldType.STRING, 8, new 
            DbsArrayController(1, 100));
        pnd_Tot_Diff = localVariables.newFieldInRecord("pnd_Tot_Diff", "#TOT-DIFF", FieldType.TIME);
        pnd_Count = localVariables.newFieldInRecord("pnd_Count", "#COUNT", FieldType.NUMERIC, 4);
        pnd_Rcount = localVariables.newFieldInRecord("pnd_Rcount", "#RCOUNT", FieldType.NUMERIC, 4);
        pnd_Success = localVariables.newFieldInRecord("pnd_Success", "#SUCCESS", FieldType.NUMERIC, 6);
        pnd_Date_D = localVariables.newFieldInRecord("pnd_Date_D", "#DATE-D", FieldType.DATE);
        pnd_Time_S = localVariables.newFieldInRecord("pnd_Time_S", "#TIME-S", FieldType.STRING, 6);

        pnd_Time_S__R_Field_11 = localVariables.newGroupInRecord("pnd_Time_S__R_Field_11", "REDEFINE", pnd_Time_S);
        pnd_Time_S_Pnd_Time_N = pnd_Time_S__R_Field_11.newFieldInGroup("pnd_Time_S_Pnd_Time_N", "#TIME-N", FieldType.NUMERIC, 6);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ncw_Mcss_Calls.reset();
        vw_ncw_Mcss_Audit.reset();
        vw_ncw_Mcss_Stats.reset();
        vw_cwf_Support_Tbl.reset();

        localVariables.reset();
        pnd_First_Rec.setInitialValue("Y");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Ncsp3020() throws Exception
    {
        super("Ncsp3020");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Ncsp3020|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        while(true)
        {
            try
            {
                //*                                                                                                                                                       //Natural: FORMAT ( 1 ) LS = 133 PS = 60 ZP = OFF
                pnd_Program.setValue(Global.getPROGRAM());                                                                                                                //Natural: ASSIGN #PROGRAM := *PROGRAM
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Data);                                                                                       //Natural: INPUT #INPUT-DATA
                pnd_Total_Success_Np_Contact.reset();                                                                                                                     //Natural: RESET #TOTAL-SUCCESS-NP-CONTACT #TOTAL-DATA-FAIL-NP-CONTACT #TOTAL-NTRL-FAIL-NP-CONTACT #TOTAL-GOOD-WPIDS #TOTAL-ATTEMPTS-NP-CONTACT #TOTAL-GOOD-LINES
                pnd_Total_Data_Fail_Np_Contact.reset();
                pnd_Total_Ntrl_Fail_Np_Contact.reset();
                pnd_Total_Good_Wpids.reset();
                pnd_Total_Attempts_Np_Contact.reset();
                pnd_Total_Good_Lines.reset();
                if (condition(pnd_Input_Data_Pnd_Input_Date.notEquals(" ")))                                                                                              //Natural: IF #INPUT-DATE NE ' '
                {
                    pnd_Input_Pnd_Input_From.setValue(pnd_Input_Data_Pnd_Input_Date);                                                                                     //Natural: MOVE #INPUT-DATE TO #INPUT-FROM
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"*** NO SUPPORT TABLE RECORD FOR ATI PROCESSING ***");                             //Natural: WRITE ( 1 ) /// '*** NO SUPPORT TABLE RECORD FOR ATI PROCESSING ***'
                    if (Global.isEscape()) return;
                    DbsUtil.stop();  if (true) return;                                                                                                                    //Natural: STOP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Input_Data_Pnd_Input_Date.notEquals(" ")))                                                                                              //Natural: IF #INPUT-DATE NE ' '
                {
                    if (condition(! (DbsUtil.maskMatches(pnd_Input_Data_Pnd_Input_Date,"19-20YYMMDD"))))                                                                  //Natural: IF #INPUT-DATE NE MASK ( 19-20YYMMDD )
                    {
                        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(30),"*",new RepeatItem(70),NEWLINE,new TabSetting(30),"*",new           //Natural: WRITE ( 1 ) // 30T '*' ( 70 ) / 30T '*' 99T '*' / 30T '* INVALID INPUT DATE (YYYYMMDD) =' #INPUT-DATE 99T '*' / 30T '*' 99T '*' / 30T '*' ( 70 )
                            TabSetting(99),"*",NEWLINE,new TabSetting(30),"* INVALID INPUT DATE (YYYYMMDD) =",pnd_Input_Data_Pnd_Input_Date,new TabSetting(99),"*",NEWLINE,new 
                            TabSetting(30),"*",new TabSetting(99),"*",NEWLINE,new TabSetting(30),"*",new RepeatItem(70));
                        if (Global.isEscape()) return;
                        DbsUtil.stop();  if (true) return;                                                                                                                //Natural: STOP
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Input_Pnd_Input_From.setValue(pnd_Input_Data_Pnd_Input_Date);                                                                                 //Natural: MOVE #INPUT-DATE TO #INPUT-FROM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(30),"*",new RepeatItem(70),NEWLINE,new TabSetting(30),"*",new               //Natural: WRITE ( 1 ) // 30T '*' ( 70 ) / 30T '*' 99T '*' / 30T '* NO INPUT-DATE PASSED FROM NCSP3000*' 99T '*' / 30T '*' 99T '*' / 30T '*' ( 70 )
                        TabSetting(99),"*",NEWLINE,new TabSetting(30),"* NO INPUT-DATE PASSED FROM NCSP3000*",new TabSetting(99),"*",NEWLINE,new TabSetting(30),"*",new 
                        TabSetting(99),"*",NEWLINE,new TabSetting(30),"*",new RepeatItem(70));
                    if (Global.isEscape()) return;
                    DbsUtil.stop();  if (true) return;                                                                                                                    //Natural: STOP
                }                                                                                                                                                         //Natural: END-IF
                pnd_Input_Pnd_Input_From.setValue(pnd_Input_Data_Pnd_Input_Date);                                                                                         //Natural: MOVE #INPUT-DATE TO #INPUT-FROM
                pnd_Input_From_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Pnd_Input_From);                                                                 //Natural: MOVE EDITED #INPUT-FROM TO #INPUT-FROM-D ( EM = YYYYMMDD )
                //*  #INPUT-FROM := '19980323' /* TEMPORARY FOR TESTING
                                                                                                                                                                          //Natural: PERFORM CHECK-INPUT-PARAMETERS
                sub_Check_Input_Parameters();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-ALL-CONTACT-SHEET-SYSTEMS
                sub_Get_All_Contact_Sheet_Systems();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //* ******************      R E P O R T S   1 & 2   **********************
                pnd_Unque_Key_Pnd_Systm.setValue("NPCON");                                                                                                                //Natural: ASSIGN #UNQUE-KEY.#SYSTM := 'NPCON'
                pnd_Input_Pnd_Input_Sys.setValue("NPCON");                                                                                                                //Natural: ASSIGN #INPUT-SYS := 'NPCON'
                pnd_Unque_Key_Pnd_Rcrd_Type_Cde.setValue("A");                                                                                                            //Natural: ASSIGN #UNQUE-KEY.#RCRD-TYPE-CDE := 'A'
                pnd_Unque_Key_Pnd_Np_Pin_Nbr.reset();                                                                                                                     //Natural: RESET #UNQUE-KEY.#NP-PIN-NBR #UNQUE-KEY.#RCRD-CONTN-IND
                pnd_Unque_Key_Pnd_Rcrd_Contn_Ind.reset();
                pnd_Date_Time_Pnd_Date.setValue(pnd_Input_Pnd_Input_From);                                                                                                //Natural: ASSIGN #DATE := #INPUT-FROM
                pnd_Date_Time_Pnd_Time.setValue("0000001");                                                                                                               //Natural: ASSIGN #TIME := '0000001'
                //*  WRITE NEWPAGE WHEN MULTIPLE REPORTS ARE CREATED PER RUN.
                pnd_Unque_Key_Pnd_Rcrd_Upld_Tme.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),pnd_Date_Time);                                                      //Natural: MOVE EDITED #DATE-TIME TO #UNQUE-KEY.#RCRD-UPLD-TME ( EM = YYYYMMDDHHIISST )
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape())){return;}
                //*  WRITE(1) '#DATE-TIME = ' #DATE-TIME
                vw_ncw_Mcss_Calls.startDatabaseRead                                                                                                                       //Natural: READ NCW-MCSS-CALLS BY UNQUE-KEY STARTING FROM #UNQUE-KEY
                (
                "READ_AUDIT",
                new Wc[] { new Wc("UNQUE_KEY", ">=", pnd_Unque_Key.getBinary(), WcType.BY) },
                new Oc[] { new Oc("UNQUE_KEY", "ASC") }
                );
                READ_AUDIT:
                while (condition(vw_ncw_Mcss_Calls.readNextRow("READ_AUDIT")))
                {
                    if (condition(!(ncw_Mcss_Calls_Systm_Ind.equals("NPCON") && (ncw_Mcss_Calls_Rcrd_Type_Cde.equals("A") || ncw_Mcss_Calls_Rcrd_Type_Cde.equals("W"))))) //Natural: ACCEPT IF NCW-MCSS-CALLS.SYSTM-IND = 'NPCON' AND ( NCW-MCSS-CALLS.RCRD-TYPE-CDE = 'A' OR NCW-MCSS-CALLS.RCRD-TYPE-CDE = 'W' )
                    {
                        continue;
                    }
                    if (condition(ncw_Mcss_Calls_Systm_Ind.notEquals("NPCON")))                                                                                           //Natural: IF NCW-MCSS-CALLS.SYSTM-IND NE 'NPCON'
                    {
                        if (true) break READ_AUDIT;                                                                                                                       //Natural: ESCAPE BOTTOM ( READ-AUDIT. ) IMMEDIATE
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Chk_Dtetme.setValueEdited(ncw_Mcss_Calls_Rcrd_Upld_Tme,new ReportEditMask("YYYYMMDDHHIISS"));                                                     //Natural: MOVE EDITED NCW-MCSS-CALLS.RCRD-UPLD-TME ( EM = YYYYMMDDHHIISS ) TO #CHK-DTETME
                    //*  ASTERICKED OUT IF ONLY FOR TESTING TO DISPLAY MULTIPLE DAYS
                    if (condition(pnd_Chk_Dtetme_Pnd_Chk_Date.notEquals(pnd_Input_Pnd_Input_From)))                                                                       //Natural: IF #CHK-DATE NE #INPUT-FROM
                    {
                        if (true) break READ_AUDIT;                                                                                                                       //Natural: ESCAPE BOTTOM ( READ-AUDIT. ) IMMEDIATE
                    }                                                                                                                                                     //Natural: END-IF
                    getSort().writeSortInData(ncw_Mcss_Calls_Systm_Ind, ncw_Mcss_Calls_Np_Pin, ncw_Mcss_Calls_Rcrd_Upld_Tme, ncw_Mcss_Calls_Rcrd_Type_Cde,                //Natural: END-ALL
                        ncw_Mcss_Calls_Contact_Id_Cde, ncw_Mcss_Calls_Call_Rcvd_Dt_Tme, ncw_Mcss_Calls_Origin_Racf_Id, ncw_Mcss_Calls_Origin_Unit_Cde, ncw_Mcss_Calls_Error_Msg_Txt, 
                        ncw_Mcss_Calls_Wpids_Total_Nbr, ncw_Mcss_Calls_Lines_Total_Nbr, ncw_Mcss_Calls_Lst_Pg_Flg, ncw_Mcss_Calls_Status_Cde, ncw_Mcss_Calls_Action_Cde.getValue(1), 
                        ncw_Mcss_Calls_Action_Cde.getValue(2), ncw_Mcss_Calls_Action_Cde.getValue(3), ncw_Mcss_Calls_Action_Cde.getValue(4), ncw_Mcss_Calls_Action_Cde.getValue(5), 
                        ncw_Mcss_Calls_Action_Cde.getValue(6), ncw_Mcss_Calls_Action_Cde.getValue(7), ncw_Mcss_Calls_Action_Cde.getValue(8), ncw_Mcss_Calls_Action_Cde.getValue(9), 
                        ncw_Mcss_Calls_Action_Cde.getValue(10), ncw_Mcss_Calls_Wpid_Cde.getValue(1), ncw_Mcss_Calls_Wpid_Cde.getValue(2), ncw_Mcss_Calls_Wpid_Cde.getValue(3), 
                        ncw_Mcss_Calls_Wpid_Cde.getValue(4), ncw_Mcss_Calls_Wpid_Cde.getValue(5), ncw_Mcss_Calls_Wpid_Cde.getValue(6), ncw_Mcss_Calls_Wpid_Cde.getValue(7), 
                        ncw_Mcss_Calls_Wpid_Cde.getValue(8), ncw_Mcss_Calls_Wpid_Cde.getValue(9), ncw_Mcss_Calls_Wpid_Cde.getValue(10), ncw_Mcss_Calls_Mit_Log_Dte_Tme.getValue(1), 
                        ncw_Mcss_Calls_Mit_Log_Dte_Tme.getValue(2), ncw_Mcss_Calls_Mit_Log_Dte_Tme.getValue(3), ncw_Mcss_Calls_Mit_Log_Dte_Tme.getValue(4), 
                        ncw_Mcss_Calls_Mit_Log_Dte_Tme.getValue(5), ncw_Mcss_Calls_Mit_Log_Dte_Tme.getValue(6), ncw_Mcss_Calls_Mit_Log_Dte_Tme.getValue(7), 
                        ncw_Mcss_Calls_Mit_Log_Dte_Tme.getValue(8), ncw_Mcss_Calls_Mit_Log_Dte_Tme.getValue(9), ncw_Mcss_Calls_Mit_Log_Dte_Tme.getValue(10), 
                        ncw_Mcss_Calls_Corp_Status_Cde.getValue(1), ncw_Mcss_Calls_Corp_Status_Cde.getValue(2), ncw_Mcss_Calls_Corp_Status_Cde.getValue(3), 
                        ncw_Mcss_Calls_Corp_Status_Cde.getValue(4), ncw_Mcss_Calls_Corp_Status_Cde.getValue(5), ncw_Mcss_Calls_Corp_Status_Cde.getValue(6), 
                        ncw_Mcss_Calls_Corp_Status_Cde.getValue(7), ncw_Mcss_Calls_Corp_Status_Cde.getValue(8), ncw_Mcss_Calls_Corp_Status_Cde.getValue(9), 
                        ncw_Mcss_Calls_Corp_Status_Cde.getValue(10), ncw_Mcss_Calls_Dstntn_Unt_Cde.getValue(1), ncw_Mcss_Calls_Dstntn_Unt_Cde.getValue(2), 
                        ncw_Mcss_Calls_Dstntn_Unt_Cde.getValue(3), ncw_Mcss_Calls_Dstntn_Unt_Cde.getValue(4), ncw_Mcss_Calls_Dstntn_Unt_Cde.getValue(5), 
                        ncw_Mcss_Calls_Dstntn_Unt_Cde.getValue(6), ncw_Mcss_Calls_Dstntn_Unt_Cde.getValue(7), ncw_Mcss_Calls_Dstntn_Unt_Cde.getValue(8), 
                        ncw_Mcss_Calls_Dstntn_Unt_Cde.getValue(9), ncw_Mcss_Calls_Dstntn_Unt_Cde.getValue(10), ncw_Mcss_Calls_Type_Of_Prcssng.getValue(1), 
                        ncw_Mcss_Calls_Type_Of_Prcssng.getValue(2), ncw_Mcss_Calls_Type_Of_Prcssng.getValue(3), ncw_Mcss_Calls_Type_Of_Prcssng.getValue(4), 
                        ncw_Mcss_Calls_Type_Of_Prcssng.getValue(5), ncw_Mcss_Calls_Type_Of_Prcssng.getValue(6), ncw_Mcss_Calls_Type_Of_Prcssng.getValue(7), 
                        ncw_Mcss_Calls_Type_Of_Prcssng.getValue(8), ncw_Mcss_Calls_Type_Of_Prcssng.getValue(9), ncw_Mcss_Calls_Type_Of_Prcssng.getValue(10), 
                        ncw_Mcss_Calls_Wrk_Rqst_Priotiry.getValue(1), ncw_Mcss_Calls_Wrk_Rqst_Priotiry.getValue(2), ncw_Mcss_Calls_Wrk_Rqst_Priotiry.getValue(3), 
                        ncw_Mcss_Calls_Wrk_Rqst_Priotiry.getValue(4), ncw_Mcss_Calls_Wrk_Rqst_Priotiry.getValue(5), ncw_Mcss_Calls_Wrk_Rqst_Priotiry.getValue(6), 
                        ncw_Mcss_Calls_Wrk_Rqst_Priotiry.getValue(7), ncw_Mcss_Calls_Wrk_Rqst_Priotiry.getValue(8), ncw_Mcss_Calls_Wrk_Rqst_Priotiry.getValue(9), 
                        ncw_Mcss_Calls_Wrk_Rqst_Priotiry.getValue(10));
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                getSort().sortData(ncw_Mcss_Calls_Systm_Ind, ncw_Mcss_Calls_Np_Pin, ncw_Mcss_Calls_Rcrd_Upld_Tme, ncw_Mcss_Calls_Rcrd_Type_Cde);                          //Natural: SORT RECORDS BY NCW-MCSS-CALLS.SYSTM-IND NCW-MCSS-CALLS.NP-PIN NCW-MCSS-CALLS.RCRD-UPLD-TME NCW-MCSS-CALLS.RCRD-TYPE-CDE USING NCW-MCSS-CALLS.CONTACT-ID-CDE NCW-MCSS-CALLS.CALL-RCVD-DT-TME NCW-MCSS-CALLS.ORIGIN-RACF-ID NCW-MCSS-CALLS.ORIGIN-UNIT-CDE NCW-MCSS-CALLS.ERROR-MSG-TXT NCW-MCSS-CALLS.WPIDS-TOTAL-NBR NCW-MCSS-CALLS.LINES-TOTAL-NBR NCW-MCSS-CALLS.LST-PG-FLG NCW-MCSS-CALLS.STATUS-CDE NCW-MCSS-CALLS.ACTION-CDE ( * ) NCW-MCSS-CALLS.WPID-CDE ( * ) NCW-MCSS-CALLS.MIT-LOG-DTE-TME ( * ) NCW-MCSS-CALLS.CORP-STATUS-CDE ( * ) NCW-MCSS-CALLS.DSTNTN-UNT-CDE ( * ) NCW-MCSS-CALLS.TYPE-OF-PRCSSNG ( * ) NCW-MCSS-CALLS.WRK-RQST-PRIOTIRY ( * )
                boolean endOfDataSort01 = true;
                boolean firstSort01 = true;
                SORT01:
                while (condition(getSort().readSortOutData(ncw_Mcss_Calls_Systm_Ind, ncw_Mcss_Calls_Np_Pin, ncw_Mcss_Calls_Rcrd_Upld_Tme, ncw_Mcss_Calls_Rcrd_Type_Cde, 
                    ncw_Mcss_Calls_Contact_Id_Cde, ncw_Mcss_Calls_Call_Rcvd_Dt_Tme, ncw_Mcss_Calls_Origin_Racf_Id, ncw_Mcss_Calls_Origin_Unit_Cde, ncw_Mcss_Calls_Error_Msg_Txt, 
                    ncw_Mcss_Calls_Wpids_Total_Nbr, ncw_Mcss_Calls_Lines_Total_Nbr, ncw_Mcss_Calls_Lst_Pg_Flg, ncw_Mcss_Calls_Status_Cde, ncw_Mcss_Calls_Action_Cde.getValue(1), 
                    ncw_Mcss_Calls_Action_Cde.getValue(2), ncw_Mcss_Calls_Action_Cde.getValue(3), ncw_Mcss_Calls_Action_Cde.getValue(4), ncw_Mcss_Calls_Action_Cde.getValue(5), 
                    ncw_Mcss_Calls_Action_Cde.getValue(6), ncw_Mcss_Calls_Action_Cde.getValue(7), ncw_Mcss_Calls_Action_Cde.getValue(8), ncw_Mcss_Calls_Action_Cde.getValue(9), 
                    ncw_Mcss_Calls_Action_Cde.getValue(10), ncw_Mcss_Calls_Wpid_Cde.getValue(1), ncw_Mcss_Calls_Wpid_Cde.getValue(2), ncw_Mcss_Calls_Wpid_Cde.getValue(3), 
                    ncw_Mcss_Calls_Wpid_Cde.getValue(4), ncw_Mcss_Calls_Wpid_Cde.getValue(5), ncw_Mcss_Calls_Wpid_Cde.getValue(6), ncw_Mcss_Calls_Wpid_Cde.getValue(7), 
                    ncw_Mcss_Calls_Wpid_Cde.getValue(8), ncw_Mcss_Calls_Wpid_Cde.getValue(9), ncw_Mcss_Calls_Wpid_Cde.getValue(10), ncw_Mcss_Calls_Mit_Log_Dte_Tme.getValue(1), 
                    ncw_Mcss_Calls_Mit_Log_Dte_Tme.getValue(2), ncw_Mcss_Calls_Mit_Log_Dte_Tme.getValue(3), ncw_Mcss_Calls_Mit_Log_Dte_Tme.getValue(4), 
                    ncw_Mcss_Calls_Mit_Log_Dte_Tme.getValue(5), ncw_Mcss_Calls_Mit_Log_Dte_Tme.getValue(6), ncw_Mcss_Calls_Mit_Log_Dte_Tme.getValue(7), 
                    ncw_Mcss_Calls_Mit_Log_Dte_Tme.getValue(8), ncw_Mcss_Calls_Mit_Log_Dte_Tme.getValue(9), ncw_Mcss_Calls_Mit_Log_Dte_Tme.getValue(10), 
                    ncw_Mcss_Calls_Corp_Status_Cde.getValue(1), ncw_Mcss_Calls_Corp_Status_Cde.getValue(2), ncw_Mcss_Calls_Corp_Status_Cde.getValue(3), 
                    ncw_Mcss_Calls_Corp_Status_Cde.getValue(4), ncw_Mcss_Calls_Corp_Status_Cde.getValue(5), ncw_Mcss_Calls_Corp_Status_Cde.getValue(6), 
                    ncw_Mcss_Calls_Corp_Status_Cde.getValue(7), ncw_Mcss_Calls_Corp_Status_Cde.getValue(8), ncw_Mcss_Calls_Corp_Status_Cde.getValue(9), 
                    ncw_Mcss_Calls_Corp_Status_Cde.getValue(10), ncw_Mcss_Calls_Dstntn_Unt_Cde.getValue(1), ncw_Mcss_Calls_Dstntn_Unt_Cde.getValue(2), ncw_Mcss_Calls_Dstntn_Unt_Cde.getValue(3), 
                    ncw_Mcss_Calls_Dstntn_Unt_Cde.getValue(4), ncw_Mcss_Calls_Dstntn_Unt_Cde.getValue(5), ncw_Mcss_Calls_Dstntn_Unt_Cde.getValue(6), ncw_Mcss_Calls_Dstntn_Unt_Cde.getValue(7), 
                    ncw_Mcss_Calls_Dstntn_Unt_Cde.getValue(8), ncw_Mcss_Calls_Dstntn_Unt_Cde.getValue(9), ncw_Mcss_Calls_Dstntn_Unt_Cde.getValue(10), ncw_Mcss_Calls_Type_Of_Prcssng.getValue(1), 
                    ncw_Mcss_Calls_Type_Of_Prcssng.getValue(2), ncw_Mcss_Calls_Type_Of_Prcssng.getValue(3), ncw_Mcss_Calls_Type_Of_Prcssng.getValue(4), 
                    ncw_Mcss_Calls_Type_Of_Prcssng.getValue(5), ncw_Mcss_Calls_Type_Of_Prcssng.getValue(6), ncw_Mcss_Calls_Type_Of_Prcssng.getValue(7), 
                    ncw_Mcss_Calls_Type_Of_Prcssng.getValue(8), ncw_Mcss_Calls_Type_Of_Prcssng.getValue(9), ncw_Mcss_Calls_Type_Of_Prcssng.getValue(10), 
                    ncw_Mcss_Calls_Wrk_Rqst_Priotiry.getValue(1), ncw_Mcss_Calls_Wrk_Rqst_Priotiry.getValue(2), ncw_Mcss_Calls_Wrk_Rqst_Priotiry.getValue(3), 
                    ncw_Mcss_Calls_Wrk_Rqst_Priotiry.getValue(4), ncw_Mcss_Calls_Wrk_Rqst_Priotiry.getValue(5), ncw_Mcss_Calls_Wrk_Rqst_Priotiry.getValue(6), 
                    ncw_Mcss_Calls_Wrk_Rqst_Priotiry.getValue(7), ncw_Mcss_Calls_Wrk_Rqst_Priotiry.getValue(8), ncw_Mcss_Calls_Wrk_Rqst_Priotiry.getValue(9), 
                    ncw_Mcss_Calls_Wrk_Rqst_Priotiry.getValue(10))))
                {
                    if (condition(getSort().getAstCOUNTER().greater(0)))
                    {
                        atBreakEventSort01(false);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom()))
                                break;
                            else if (condition(Global.isEscapeBottomImmediate()))
                            {
                                endOfDataSort01 = false;
                                break;
                            }
                            else if (condition(Global.isEscapeTop()))
                            continue;
                            else if (condition())
                            return;
                        }
                    }
                    //*   WRITE 'KEY: '
                    //*    NCW-MCSS-CALLS.SYSTM-IND
                    //*    NCW-MCSS-CALLS.RCRD-UPLD-TME (EM=YYYYMMDDHHIISST)
                    //*    NCW-MCSS-CALLS.RCRD-TYPE-CDE
                    //*    NCW-MCSS-CALLS.NP-PIN
                    //*    NCW-MCSS-CALLS.CONTACT-ID-CDE
                    if (condition(ncw_Mcss_Calls_Rcrd_Type_Cde.equals("A")))                                                                                              //Natural: IF NCW-MCSS-CALLS.RCRD-TYPE-CDE = 'A'
                    {
                                                                                                                                                                          //Natural: PERFORM WRITE-AUDIT-DETAIL
                        sub_Write_Audit_Detail();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(ncw_Mcss_Calls_Rcrd_Type_Cde.equals("W")))                                                                                          //Natural: IF NCW-MCSS-CALLS.RCRD-TYPE-CDE = 'W'
                        {
                                                                                                                                                                          //Natural: PERFORM WRITE-WPID-DETAIL
                            sub_Write_Wpid_Detail();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(30),"*",new RepeatItem(70),NEWLINE,new TabSetting(30),"*",new       //Natural: WRITE ( 1 ) // 30T '*' ( 70 ) / 30T '*' 99T '*' / 30T '* INVALID INPUT DATE (YYYYMMDD) =' #INPUT-FROM 99T '*' / 30T '*' 99T '*' / 30T '*' ( 70 )
                                TabSetting(99),"*",NEWLINE,new TabSetting(30),"* INVALID INPUT DATE (YYYYMMDD) =",pnd_Input_Pnd_Input_From,new TabSetting(99),"*",NEWLINE,new 
                                TabSetting(30),"*",new TabSetting(99),"*",NEWLINE,new TabSetting(30),"*",new RepeatItem(70));
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*                                                                                                                                                   //Natural: AT BREAK OF NCW-MCSS-CALLS.RCRD-UPLD-TME
                }                                                                                                                                                         //Natural: END-SORT
                if (condition(getSort().getAstCOUNTER().greater(0)))
                {
                    atBreakEventSort01(endOfDataSort01);
                }
                endSort();
                                                                                                                                                                          //Natural: PERFORM WRITE-TOTAL-TOTALS
                sub_Write_Total_Totals();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( 1 )
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-ALL-CONTACT-SHEET-SYSTEMS
                //* ***********************************************************************
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-INPUT-PARAMETERS
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-AUDIT-DETAIL
                //* ***********************************************************************
                //*  CHECK FOR 'E' IS TEMPORARY
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WPID-DETAIL
                //* ***********************************************************************
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TOTAL-TOTALS
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Get_All_Contact_Sheet_Systems() throws Exception                                                                                                     //Natural: GET-ALL-CONTACT-SHEET-SYSTEMS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Gen_Tbl_Key_Pnd_Gen_Tbl_Authrty.setValue("S");                                                                                                                //Natural: ASSIGN #GEN-TBL-AUTHRTY := 'S'
        pnd_Gen_Tbl_Key_Pnd_Gen_Tbl_Name.setValue("CWF-CNTCT-SHT-SYSTMS");                                                                                                //Natural: ASSIGN #GEN-TBL-NAME := 'CWF-CNTCT-SHT-SYSTMS'
        pnd_Gen_Tbl_Key_Pnd_Gen_Tbl_Code.reset();                                                                                                                         //Natural: RESET #GEN-TBL-CODE #CNT
        pnd_Cnt.reset();
        vw_cwf_Support_Tbl.startDatabaseRead                                                                                                                              //Natural: READ CWF-SUPPORT-TBL BY TBL-PRIME-KEY FROM #GEN-TBL-KEY
        (
        "READ01",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Gen_Tbl_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_cwf_Support_Tbl.readNextRow("READ01")))
        {
            if (condition(cwf_Support_Tbl_Tbl_Scrty_Level_Ind.notEquals("S") || cwf_Support_Tbl_Tbl_Table_Nme.notEquals("CWF-CNTCT-SHT-SYSTMS")))                         //Natural: IF TBL-SCRTY-LEVEL-IND NE 'S' OR TBL-TABLE-NME NE 'CWF-CNTCT-SHT-SYSTMS'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Cnt.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #CNT
            pnd_Contact_Sheet_Systems.getValue(pnd_Cnt).setValue(cwf_Support_Tbl_Tbl_Key_Field);                                                                          //Natural: ASSIGN #CONTACT-SHEET-SYSTEMS ( #CNT ) := TBL-KEY-FIELD
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Cnt.equals(getZero())))                                                                                                                         //Natural: IF #CNT = 0
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(30),"*",new RepeatItem(70),NEWLINE,new TabSetting(30),"*",new TabSetting(99),"*",NEWLINE,new  //Natural: WRITE ( 1 ) NOTITLE // 30T '*' ( 70 ) / 30T '*' 99T '*' / 30T '* NO SYSTEM IS SETUP IN MIT SUPPORT TABLE.' 99T '*' / 30T '*' 99T '*' / 30T '*' ( 70 )
                TabSetting(30),"* NO SYSTEM IS SETUP IN MIT SUPPORT TABLE.",new TabSetting(99),"*",NEWLINE,new TabSetting(30),"*",new TabSetting(99),"*",NEWLINE,new 
                TabSetting(30),"*",new RepeatItem(70));
            if (Global.isEscape()) return;
            DbsUtil.stop();  if (true) return;                                                                                                                            //Natural: STOP
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Check_Input_Parameters() throws Exception                                                                                                            //Natural: CHECK-INPUT-PARAMETERS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(! (DbsUtil.maskMatches(pnd_Input_Pnd_Input_From,"19-20YYMMDD"))))                                                                                   //Natural: IF #INPUT-FROM NE MASK ( 19-20YYMMDD )
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(30),"*",new RepeatItem(70),NEWLINE,new TabSetting(30),"*",new TabSetting(99),"*",NEWLINE,new  //Natural: WRITE ( 1 ) // 30T '*' ( 70 ) / 30T '*' 99T '*' / 30T '* INVALID INPUT DATE (YYYYMMDD) =' #INPUT-FROM 99T '*' / 30T '*' 99T '*' / 30T '*' ( 70 )
                TabSetting(30),"* INVALID INPUT DATE (YYYYMMDD) =",pnd_Input_Pnd_Input_From,new TabSetting(99),"*",NEWLINE,new TabSetting(30),"*",new TabSetting(99),"*",NEWLINE,new 
                TabSetting(30),"*",new RepeatItem(70));
            if (Global.isEscape()) return;
            DbsUtil.stop();  if (true) return;                                                                                                                            //Natural: STOP
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Audit_Detail() throws Exception                                                                                                                //Natural: WRITE-AUDIT-DETAIL
    {
        if (BLNatReinput.isReinput()) return;

        FOR01:                                                                                                                                                            //Natural: FOR #I 1 10
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(10)); pnd_I.nadd(1))
        {
            if (condition(ncw_Mcss_Calls_Mit_Log_Dte_Tme.getValue(pnd_I).equals(getZero())))                                                                              //Natural: IF NCW-MCSS-CALLS.MIT-LOG-DTE-TME ( #I ) = 0
            {
                pnd_I.setValue(10);                                                                                                                                       //Natural: ASSIGN #I = 10
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Hold_Mit_Log_Dt_Tme.getValue(pnd_I).setValue(ncw_Mcss_Calls_Mit_Log_Dte_Tme.getValue(pnd_I));                                                         //Natural: MOVE NCW-MCSS-CALLS.MIT-LOG-DTE-TME ( #I ) TO #HOLD-MIT-LOG-DT-TME ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(getReports().getAstLinesLeft(1).less(10)))                                                                                                          //Natural: NEWPAGE ( 1 ) IF LESS THAN 10 LINES LEFT
        {
            getReports().newPage(1);
            if (condition(Global.isEscape())){return;}
        }
        pnd_Display_Status.reset();                                                                                                                                       //Natural: RESET #DISPLAY-STATUS
        pnd_Total_Attempts_Np_Contact.nadd(1);                                                                                                                            //Natural: ADD 1 TO #TOTAL-ATTEMPTS-NP-CONTACT
        if (condition(ncw_Mcss_Calls_Status_Cde.equals("S")))                                                                                                             //Natural: IF NCW-MCSS-CALLS.STATUS-CDE = 'S'
        {
            pnd_Total_Success_Np_Contact.nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-SUCCESS-NP-CONTACT
            pnd_Total_Good_Wpids.nadd(ncw_Mcss_Calls_Wpids_Total_Nbr);                                                                                                    //Natural: ADD NCW-MCSS-CALLS.WPIDS-TOTAL-NBR TO #TOTAL-GOOD-WPIDS
            pnd_Total_Good_Lines.nadd(ncw_Mcss_Calls_Lines_Total_Nbr);                                                                                                    //Natural: ADD NCW-MCSS-CALLS.LINES-TOTAL-NBR TO #TOTAL-GOOD-LINES
            pnd_Display_Status.setValue("SUCCESS");                                                                                                                       //Natural: MOVE 'SUCCESS' TO #DISPLAY-STATUS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ncw_Mcss_Calls_Status_Cde.equals("D")))                                                                                                         //Natural: IF NCW-MCSS-CALLS.STATUS-CDE = 'D'
            {
                pnd_Total_Data_Fail_Np_Contact.nadd(1);                                                                                                                   //Natural: ADD 1 TO #TOTAL-DATA-FAIL-NP-CONTACT
                pnd_Display_Status.setValue("ERROR-DATA");                                                                                                                //Natural: MOVE 'ERROR-DATA' TO #DISPLAY-STATUS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ncw_Mcss_Calls_Status_Cde.equals("N")))                                                                                                     //Natural: IF NCW-MCSS-CALLS.STATUS-CDE = 'N'
                {
                    pnd_Total_Ntrl_Fail_Np_Contact.nadd(1);                                                                                                               //Natural: ADD 1 TO #TOTAL-NTRL-FAIL-NP-CONTACT
                    pnd_Display_Status.setValue("ERROR-NTRL");                                                                                                            //Natural: MOVE 'ERROR-NTRL' TO #DISPLAY-STATUS
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Display_Status.setValue("INVALID");                                                                                                               //Natural: MOVE 'INVALID' TO #DISPLAY-STATUS
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),ncw_Mcss_Calls_Np_Pin, new FieldAttributes ("AD=L"),new TabSetting(10),ncw_Mcss_Calls_Contact_Id_Cde,  //Natural: WRITE ( 1 ) NOTITLE / 1T NCW-MCSS-CALLS.NP-PIN ( AD = L ) 10T NCW-MCSS-CALLS.CONTACT-ID-CDE ( AD = R ) 22T #DISPLAY-STATUS 34T NCW-MCSS-CALLS.CALL-RCVD-DT-TME ( EM = MM/DD/YYYY' 'HH:II:SS ) 55T NCW-MCSS-CALLS.RCRD-UPLD-TME ( EM = MM/DD/YYYY' 'HH:II:SS:T ) 80T NCW-MCSS-CALLS.ORIGIN-RACF-ID 90T NCW-MCSS-CALLS.ORIGIN-UNIT-CDE 99T NCW-MCSS-CALLS.WPIDS-TOTAL-NBR ( EM = ZZZZZZZ9 ) 109T NCW-MCSS-CALLS.LINES-TOTAL-NBR ( EM = ZZZZZZZ9 ) 127T NCW-MCSS-CALLS.LST-PG-FLG
            new FieldAttributes ("AD=R"),new TabSetting(22),pnd_Display_Status,new TabSetting(34),ncw_Mcss_Calls_Call_Rcvd_Dt_Tme, new ReportEditMask ("MM/DD/YYYY' 'HH:II:SS"),new 
            TabSetting(55),ncw_Mcss_Calls_Rcrd_Upld_Tme, new ReportEditMask ("MM/DD/YYYY' 'HH:II:SS:T"),new TabSetting(80),ncw_Mcss_Calls_Origin_Racf_Id,new 
            TabSetting(90),ncw_Mcss_Calls_Origin_Unit_Cde,new TabSetting(99),ncw_Mcss_Calls_Wpids_Total_Nbr, new ReportEditMask ("ZZZZZZZ9"),new TabSetting(109),ncw_Mcss_Calls_Lines_Total_Nbr, 
            new ReportEditMask ("ZZZZZZZ9"),new TabSetting(127),ncw_Mcss_Calls_Lst_Pg_Flg);
        if (Global.isEscape()) return;
        if (condition(ncw_Mcss_Calls_Status_Cde.equals("D") || ncw_Mcss_Calls_Status_Cde.equals("N") || ncw_Mcss_Calls_Status_Cde.equals("E")))                           //Natural: IF NCW-MCSS-CALLS.STATUS-CDE = 'D' OR NCW-MCSS-CALLS.STATUS-CDE = 'N' OR NCW-MCSS-CALLS.STATUS-CDE = 'E'
        {
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(22),"ERROR MSG =",new TabSetting(34),ncw_Mcss_Calls_Error_Msg_Txt);                                 //Natural: WRITE ( 1 ) NOTITLE 22T 'ERROR MSG =' 34T NCW-MCSS-CALLS.ERROR-MSG-TXT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Wpid_Detail() throws Exception                                                                                                                 //Natural: WRITE-WPID-DETAIL
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #I 1 10
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(10)); pnd_I.nadd(1))
        {
            if (condition(ncw_Mcss_Calls_Wpid_Cde.getValue(pnd_I).greater(" ")))                                                                                          //Natural: IF NCW-MCSS-CALLS.WPID-CDE ( #I ) > ' '
            {
                if (condition(ncw_Mcss_Calls_Corp_Status_Cde.getValue(pnd_I).equals("0") || ncw_Mcss_Calls_Corp_Status_Cde.getValue(pnd_I).equals(" ")))                  //Natural: IF CORP-STATUS-CDE ( #I ) = '0' OR CORP-STATUS-CDE ( #I ) = ' '
                {
                    pnd_Corp_Status_Cde.setValue("OPEN");                                                                                                                 //Natural: MOVE 'OPEN' TO #CORP-STATUS-CDE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ncw_Mcss_Calls_Corp_Status_Cde.getValue(pnd_I).equals("9")))                                                                            //Natural: IF CORP-STATUS-CDE ( #I ) = '9'
                    {
                        pnd_Corp_Status_Cde.setValue("CLOSED");                                                                                                           //Natural: MOVE 'CLOSED' TO #CORP-STATUS-CDE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Corp_Status_Cde.setValue("INVLD");                                                                                                            //Natural: MOVE 'INVLD' TO #CORP-STATUS-CDE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ncw_Mcss_Calls_Type_Of_Prcssng.getValue(pnd_I).equals("A")))                                                                                //Natural: IF TYPE-OF-PRCSSNG ( #I ) = 'A'
                {
                    pnd_Type_Of_Prcssng.setValue("AUTO");                                                                                                                 //Natural: MOVE 'AUTO' TO #TYPE-OF-PRCSSNG
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ncw_Mcss_Calls_Type_Of_Prcssng.getValue(pnd_I).equals(" ")))                                                                            //Natural: IF TYPE-OF-PRCSSNG ( #I ) = ' '
                    {
                        pnd_Type_Of_Prcssng.setValue("MANUAL");                                                                                                           //Natural: MOVE 'MANUAL' TO #TYPE-OF-PRCSSNG
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Type_Of_Prcssng.setValue("INVLD");                                                                                                            //Natural: MOVE 'INVLD' TO #TYPE-OF-PRCSSNG
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ncw_Mcss_Calls_Wrk_Rqst_Priotiry.getValue(pnd_I).equals("R")))                                                                              //Natural: IF WRK-RQST-PRIOTIRY ( #I ) = 'R'
                {
                    pnd_Wrk_Rqst_Priotiry.setValue("RUSH");                                                                                                               //Natural: MOVE 'RUSH' TO #WRK-RQST-PRIOTIRY
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ncw_Mcss_Calls_Wrk_Rqst_Priotiry.getValue(pnd_I).equals(" ")))                                                                          //Natural: IF WRK-RQST-PRIOTIRY ( #I ) = ' '
                    {
                        pnd_Wrk_Rqst_Priotiry.setValue("NORMAL");                                                                                                         //Natural: MOVE 'NORMAL' TO #WRK-RQST-PRIOTIRY
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Wrk_Rqst_Priotiry.setValue("INVLD");                                                                                                          //Natural: MOVE 'INVLD' TO #WRK-RQST-PRIOTIRY
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_I.equals(1)))                                                                                                                           //Natural: IF #I = 1
                {
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(33),"ACTION    WPID           MIT-LOG-DT-TIME       CORP/STAT",new                  //Natural: WRITE ( 1 ) NOTITLE / 33T 'ACTION    WPID           MIT-LOG-DT-TIME       CORP/STAT' 92T 'PRCS/TYPE   PRIORITY    DEST/UNIT'
                        TabSetting(92),"PRCS/TYPE   PRIORITY    DEST/UNIT");
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(35),ncw_Mcss_Calls_Action_Cde.getValue(pnd_I),new TabSetting(43),ncw_Mcss_Calls_Wpid_Cde.getValue(pnd_I),new  //Natural: WRITE ( 1 ) NOTITLE 35T NCW-MCSS-CALLS.ACTION-CDE ( #I ) 43T NCW-MCSS-CALLS.WPID-CDE ( #I ) 55T #HOLD-MIT-LOG-DT-TME ( #I ) ( EM = MM/DD/YYYY' 'HH:II:SS:T ) 81T #CORP-STATUS-CDE 93T #TYPE-OF-PRCSSNG 105T #WRK-RQST-PRIOTIRY 117T NCW-MCSS-CALLS.DSTNTN-UNT-CDE ( #I )
                        TabSetting(55),pnd_Hold_Mit_Log_Dt_Tme.getValue(pnd_I), new ReportEditMask ("MM/DD/YYYY' 'HH:II:SS:T"),new TabSetting(81),pnd_Corp_Status_Cde,new 
                        TabSetting(93),pnd_Type_Of_Prcssng,new TabSetting(105),pnd_Wrk_Rqst_Priotiry,new TabSetting(117),ncw_Mcss_Calls_Dstntn_Unt_Cde.getValue(pnd_I));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(35),ncw_Mcss_Calls_Action_Cde.getValue(pnd_I),new TabSetting(43),ncw_Mcss_Calls_Wpid_Cde.getValue(pnd_I),new  //Natural: WRITE ( 1 ) NOTITLE 35T NCW-MCSS-CALLS.ACTION-CDE ( #I ) 43T NCW-MCSS-CALLS.WPID-CDE ( #I ) 55T #HOLD-MIT-LOG-DT-TME ( #I ) ( EM = MM/DD/YYYY' 'HH:II:SS:T ) 81T #CORP-STATUS-CDE 93T #TYPE-OF-PRCSSNG 105T #WRK-RQST-PRIOTIRY 117T NCW-MCSS-CALLS.DSTNTN-UNT-CDE ( #I )
                        TabSetting(55),pnd_Hold_Mit_Log_Dt_Tme.getValue(pnd_I), new ReportEditMask ("MM/DD/YYYY' 'HH:II:SS:T"),new TabSetting(81),pnd_Corp_Status_Cde,new 
                        TabSetting(93),pnd_Type_Of_Prcssng,new TabSetting(105),pnd_Wrk_Rqst_Priotiry,new TabSetting(117),ncw_Mcss_Calls_Dstntn_Unt_Cde.getValue(pnd_I));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_I.setValue(10);                                                                                                                                       //Natural: ASSIGN #I = 10
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Write_Total_Totals() throws Exception                                                                                                                //Natural: WRITE-TOTAL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        pnd_Total_Errors.compute(new ComputeParameters(false, pnd_Total_Errors), pnd_Total_Data_Fail_Np_Contact.add(pnd_Total_Ntrl_Fail_Np_Contact));                     //Natural: COMPUTE #TOTAL-ERRORS = #TOTAL-DATA-FAIL-NP-CONTACT + #TOTAL-NTRL-FAIL-NP-CONTACT
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(40),                    //Natural: WRITE ( 1 ) NOTITLE ////////// 40T 'SUMMARY OF NON-PARTICIPANT CONTACTS UPLOADED'
            "SUMMARY OF NON-PARTICIPANT CONTACTS UPLOADED");
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(40),"--------------------------------------------");                                                    //Natural: WRITE ( 1 ) NOTITLE 40T '--------------------------------------------'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(20),"TOTAL SUCCESSFUL          :",new TabSetting(50),pnd_Total_Success_Np_Contact);     //Natural: WRITE ( 1 ) NOTITLE // 20T 'TOTAL SUCCESSFUL          :' 50T #TOTAL-SUCCESS-NP-CONTACT
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(20),"TOTAL ERRORS              :",new TabSetting(50),pnd_Total_Errors,new               //Natural: WRITE ( 1 ) NOTITLE // 20T 'TOTAL ERRORS              :' 50T #TOTAL-ERRORS 66T '(DATA ERRORS =' 81T #TOTAL-DATA-FAIL-NP-CONTACT 95T ') (NTRL ERRORS =' 112T #TOTAL-NTRL-FAIL-NP-CONTACT 124T ')'
            TabSetting(66),"(DATA ERRORS =",new TabSetting(81),pnd_Total_Data_Fail_Np_Contact,new TabSetting(95),") (NTRL ERRORS =",new TabSetting(112),pnd_Total_Ntrl_Fail_Np_Contact,new 
            TabSetting(124),")");
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(50),"-----------");                                                                             //Natural: WRITE ( 1 ) NOTITLE / 50T '-----------'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(20),"TOTAL UPLOADS             :",new TabSetting(50),pnd_Total_Attempts_Np_Contact);                    //Natural: WRITE ( 1 ) NOTITLE 20T 'TOTAL UPLOADS             :' 50T #TOTAL-ATTEMPTS-NP-CONTACT
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(50),"-----------");                                                                                     //Natural: WRITE ( 1 ) NOTITLE 50T '-----------'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(20),"SUCCESSFUL WPID'S         :",new TabSetting(50),                   //Natural: WRITE ( 1 ) NOTITLE //// 20T 'SUCCESSFUL WPID"S         :' 50T #TOTAL-GOOD-WPIDS
            pnd_Total_Good_Wpids);
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(20),"SUCCESSFUL TEXT LINES     :",new TabSetting(50),pnd_Total_Good_Lines);             //Natural: WRITE ( 1 ) NOTITLE // 20T 'SUCCESSFUL TEXT LINES     :' 50T #TOTAL-GOOD-LINES
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),pnd_Program,new TabSetting(48),"CORPORATE WORKFLOW SYSTEM",new TabSetting(101),Global.getDATU(),new  //Natural: WRITE ( 1 ) NOTITLE 001T #PROGRAM 048T 'CORPORATE WORKFLOW SYSTEM' 101T *DATU 110T *TIME ( AL = 005 ) 121T 'PAGE:' 127T *PAGE-NUMBER ( 1 ) ( AD = L ) / 040T 'NON-PARTICIPANT CONTACT AUDIT REPORT FOR:' 085T #INPUT-FROM-D ( AD = L EM = MM/DD/YYYY ) / 050T 'SYSTEM :' 059T #INPUT-SYS / 001T '-' ( 131 ) / 038T 'DATE & TIME' 059T 'DATE & TIME' 078T 'EMPLOYEE' 089T 'ORIGIN' 108T 'NUMBER OF' 123T 'LAST PAGE' / 001T 'NP-PIN' 010T 'CONTACT-ID' 023T 'STATUS' 037T 'CALL RECEIVED' 057T 'UPLOAD COMPLETED' 079T 'RACF-ID' 090T 'UNIT' 104T 'WPIDS   TEXT LINES' 126T 'FLAG' / 001T '-' ( 131 ) /
                        TabSetting(110),Global.getTIME(), new AlphanumericLength (5),new TabSetting(121),"PAGE:",new TabSetting(127),getReports().getPageNumberDbs(1), 
                        new FieldAttributes ("AD=L"),NEWLINE,new TabSetting(40),"NON-PARTICIPANT CONTACT AUDIT REPORT FOR:",new TabSetting(85),pnd_Input_From_D, 
                        new FieldAttributes ("AD=L"), new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new TabSetting(50),"SYSTEM :",new TabSetting(59),pnd_Input_Pnd_Input_Sys,NEWLINE,new 
                        TabSetting(1),"-",new RepeatItem(131),NEWLINE,new TabSetting(38),"DATE & TIME",new TabSetting(59),"DATE & TIME",new TabSetting(78),"EMPLOYEE",new 
                        TabSetting(89),"ORIGIN",new TabSetting(108),"NUMBER OF",new TabSetting(123),"LAST PAGE",NEWLINE,new TabSetting(1),"NP-PIN",new TabSetting(10),"CONTACT-ID",new 
                        TabSetting(23),"STATUS",new TabSetting(37),"CALL RECEIVED",new TabSetting(57),"UPLOAD COMPLETED",new TabSetting(79),"RACF-ID",new 
                        TabSetting(90),"UNIT",new TabSetting(104),"WPIDS   TEXT LINES",new TabSetting(126),"FLAG",NEWLINE,new TabSetting(1),"-",new RepeatItem(131),
                        NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean ncw_Mcss_Calls_Rcrd_Upld_TmeIsBreak = ncw_Mcss_Calls_Rcrd_Upld_Tme.isBreak(endOfData);
        if (condition(ncw_Mcss_Calls_Rcrd_Upld_TmeIsBreak))
        {
            pnd_Hold_Mit_Log_Dt_Tme.getValue("*").reset();                                                                                                                //Natural: RESET #HOLD-MIT-LOG-DT-TME ( * ) #WRK-RQST-PRIOTIRY #TYPE-OF-PRCSSNG #CORP-STATUS-CDE
            pnd_Wrk_Rqst_Priotiry.reset();
            pnd_Type_Of_Prcssng.reset();
            pnd_Corp_Status_Cde.reset();
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"-",new RepeatItem(131));                                                                        //Natural: WRITE ( 1 ) 01T'-' ( 131 )
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=60 ZP=OFF");
    }
}
