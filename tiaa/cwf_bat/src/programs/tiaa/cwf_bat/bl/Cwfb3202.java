/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:28:19 PM
**        * FROM NATURAL PROGRAM : Cwfb3202
************************************************************
**        * FILE NAME            : Cwfb3202.java
**        * CLASS NAME           : Cwfb3202
**        * INSTANCE NAME        : Cwfb3202
************************************************************
**SAG GENERATOR: SHELL-TIAA                       VERSION: 3.2.2
**SAG TITLE: REPORT 5
**SAG SYSTEM: CRPCWF
************************************************************************
* PROGRAM  : CWFB3202
* SYSTEM   : CRPCWF
* TITLE    : REPORT 5 SORT
* GENERATED: AUG 11,93 AT 10:46 AM
* FUNCTION : REPORT OF WORK ACTIVITY FOR A PERIOD
*          |
*          |
*          |
*          |
*          |
*          |
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* MMM DD YY  ________ ______________________________________________
* ______________________________________________
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3202 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Work_Record;
    private DbsField pnd_Work_Record_Tbl_Pin;
    private DbsField pnd_Work_Record_Tbl_Wpid_Act;
    private DbsField pnd_Work_Record_Tbl_Wpid;

    private DbsGroup pnd_Work_Record__R_Field_1;
    private DbsField pnd_Work_Record_Tbl_Wpid_Action;
    private DbsField pnd_Work_Record_Tbl_Status_Cde;

    private DbsGroup pnd_Work_Record__R_Field_2;
    private DbsField pnd_Work_Record_Tbl_Status_Cde_N;
    private DbsField pnd_Work_Record_Tbl_Last_Updte_Dte;

    private DbsGroup pnd_Work_Record__R_Field_3;
    private DbsField pnd_Work_Record_Tbl_Last_Updte_Dte_A;
    private DbsField pnd_Work_Record_Tbl_Last_Chge_Dte;
    private DbsField pnd_Work_Record_Tbl_Log_Dte_Tme;
    private DbsField pnd_Work_Record_Tbl_Log_Unit_Cde;
    private DbsField pnd_Work_Record_Tbl_Chnge_Unit;
    private DbsField pnd_Work_Record_Tbl_Admin_Unit;
    private DbsField pnd_Work_Record_Tbl_Log_By;
    private DbsField pnd_Work_Record_Pnd_New_Ind;
    private DbsField pnd_Work_Record_Pnd_Merg_Ind;
    private DbsField pnd_Work_Record_Pnd_E_Pended_Ind;
    private DbsField pnd_Work_Record_Pnd_Partic_Closed_Ind;
    private DbsField pnd_Work_Record_Pnd_Assigned_Ind;
    private DbsField pnd_Work_Record_Pnd_Unassigned_Ind;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Work_Record = localVariables.newGroupInRecord("pnd_Work_Record", "#WORK-RECORD");
        pnd_Work_Record_Tbl_Pin = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Pin", "TBL-PIN", FieldType.NUMERIC, 12);
        pnd_Work_Record_Tbl_Wpid_Act = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Wpid_Act", "TBL-WPID-ACT", FieldType.STRING, 1);
        pnd_Work_Record_Tbl_Wpid = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Wpid", "TBL-WPID", FieldType.STRING, 6);

        pnd_Work_Record__R_Field_1 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record__R_Field_1", "REDEFINE", pnd_Work_Record_Tbl_Wpid);
        pnd_Work_Record_Tbl_Wpid_Action = pnd_Work_Record__R_Field_1.newFieldInGroup("pnd_Work_Record_Tbl_Wpid_Action", "TBL-WPID-ACTION", FieldType.STRING, 
            1);
        pnd_Work_Record_Tbl_Status_Cde = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Status_Cde", "TBL-STATUS-CDE", FieldType.STRING, 4);

        pnd_Work_Record__R_Field_2 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record__R_Field_2", "REDEFINE", pnd_Work_Record_Tbl_Status_Cde);
        pnd_Work_Record_Tbl_Status_Cde_N = pnd_Work_Record__R_Field_2.newFieldInGroup("pnd_Work_Record_Tbl_Status_Cde_N", "TBL-STATUS-CDE-N", FieldType.NUMERIC, 
            4);
        pnd_Work_Record_Tbl_Last_Updte_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Last_Updte_Dte", "TBL-LAST-UPDTE-DTE", FieldType.NUMERIC, 
            8);

        pnd_Work_Record__R_Field_3 = pnd_Work_Record.newGroupInGroup("pnd_Work_Record__R_Field_3", "REDEFINE", pnd_Work_Record_Tbl_Last_Updte_Dte);
        pnd_Work_Record_Tbl_Last_Updte_Dte_A = pnd_Work_Record__R_Field_3.newFieldInGroup("pnd_Work_Record_Tbl_Last_Updte_Dte_A", "TBL-LAST-UPDTE-DTE-A", 
            FieldType.STRING, 8);
        pnd_Work_Record_Tbl_Last_Chge_Dte = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Last_Chge_Dte", "TBL-LAST-CHGE-DTE", FieldType.STRING, 
            15);
        pnd_Work_Record_Tbl_Log_Dte_Tme = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Log_Dte_Tme", "TBL-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Work_Record_Tbl_Log_Unit_Cde = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Log_Unit_Cde", "TBL-LOG-UNIT-CDE", FieldType.STRING, 8);
        pnd_Work_Record_Tbl_Chnge_Unit = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Chnge_Unit", "TBL-CHNGE-UNIT", FieldType.STRING, 8);
        pnd_Work_Record_Tbl_Admin_Unit = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Admin_Unit", "TBL-ADMIN-UNIT", FieldType.STRING, 8);
        pnd_Work_Record_Tbl_Log_By = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Tbl_Log_By", "TBL-LOG-BY", FieldType.STRING, 8);
        pnd_Work_Record_Pnd_New_Ind = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_New_Ind", "#NEW-IND", FieldType.NUMERIC, 2);
        pnd_Work_Record_Pnd_Merg_Ind = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Merg_Ind", "#MERG-IND", FieldType.NUMERIC, 2);
        pnd_Work_Record_Pnd_E_Pended_Ind = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_E_Pended_Ind", "#E-PENDED-IND", FieldType.NUMERIC, 2);
        pnd_Work_Record_Pnd_Partic_Closed_Ind = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Partic_Closed_Ind", "#PARTIC-CLOSED-IND", FieldType.NUMERIC, 
            2);
        pnd_Work_Record_Pnd_Assigned_Ind = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Assigned_Ind", "#ASSIGNED-IND", FieldType.NUMERIC, 2);
        pnd_Work_Record_Pnd_Unassigned_Ind = pnd_Work_Record.newFieldInGroup("pnd_Work_Record_Pnd_Unassigned_Ind", "#UNASSIGNED-IND", FieldType.NUMERIC, 
            2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Cwfb3202() throws Exception
    {
        super("Cwfb3202");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #WORK-RECORD
        while (condition(getWorkFiles().read(1, pnd_Work_Record)))
        {
            getSort().writeSortInData(pnd_Work_Record_Tbl_Log_Unit_Cde, pnd_Work_Record_Tbl_Wpid_Act, pnd_Work_Record_Tbl_Wpid, pnd_Work_Record_Tbl_Status_Cde,           //Natural: END-ALL
                pnd_Work_Record_Tbl_Admin_Unit, pnd_Work_Record_Tbl_Pin, pnd_Work_Record_Tbl_Log_Dte_Tme, pnd_Work_Record_Tbl_Log_By, pnd_Work_Record_Tbl_Last_Chge_Dte, 
                pnd_Work_Record_Tbl_Chnge_Unit, pnd_Work_Record_Tbl_Last_Updte_Dte);
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getSort().sortData(pnd_Work_Record_Tbl_Log_Unit_Cde, pnd_Work_Record_Tbl_Wpid_Act, pnd_Work_Record_Tbl_Wpid, pnd_Work_Record_Tbl_Status_Cde);                     //Natural: SORT BY #WORK-RECORD.TBL-LOG-UNIT-CDE #WORK-RECORD.TBL-WPID-ACT #WORK-RECORD.TBL-WPID #WORK-RECORD.TBL-STATUS-CDE USING #WORK-RECORD.TBL-ADMIN-UNIT #WORK-RECORD.TBL-PIN #WORK-RECORD.TBL-LOG-DTE-TME #WORK-RECORD.TBL-LOG-BY #WORK-RECORD.TBL-LAST-CHGE-DTE #WORK-RECORD.TBL-CHNGE-UNIT #WORK-RECORD.TBL-LAST-UPDTE-DTE
        SORT01:
        while (condition(getSort().readSortOutData(pnd_Work_Record_Tbl_Log_Unit_Cde, pnd_Work_Record_Tbl_Wpid_Act, pnd_Work_Record_Tbl_Wpid, pnd_Work_Record_Tbl_Status_Cde, 
            pnd_Work_Record_Tbl_Admin_Unit, pnd_Work_Record_Tbl_Pin, pnd_Work_Record_Tbl_Log_Dte_Tme, pnd_Work_Record_Tbl_Log_By, pnd_Work_Record_Tbl_Last_Chge_Dte, 
            pnd_Work_Record_Tbl_Chnge_Unit, pnd_Work_Record_Tbl_Last_Updte_Dte)))
        {
            getWorkFiles().write(2, false, pnd_Work_Record);                                                                                                              //Natural: WRITE WORK FILE 2 #WORK-RECORD
        }                                                                                                                                                                 //Natural: END-SORT
        endSort();
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }

    //
}
