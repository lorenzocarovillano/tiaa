/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:48:03 PM
**        * FROM NATURAL PROGRAM : Cwfbcwf9
************************************************************
**        * FILE NAME            : Cwfbcwf9.java
**        * CLASS NAME           : Cwfbcwf9
**        * INSTANCE NAME        : Cwfbcwf9
************************************************************
************************************************************************
* PROGRAM  : CWFBCWF9
* SYSTEM   : MIT TABLE EXTRACT - CWF
* TITLE    : #65 - AVEKSA PROJECT - MIT TABLE CWF EXTRACT
* FUNCTION :
*          |
* 02/04/08 | GH  CREATE NEW.
* 03/10/08 | GH  WRITE TO FILE WITH COMMA AS DELIMETER FOR .CSV FORMAT
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfbcwf9 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Org_Empl_Tbl;
    private DbsField cwf_Org_Empl_Tbl_Empl_Unit_Cde;
    private DbsField cwf_Org_Empl_Tbl_Empl_Racf_Id;
    private DbsField cwf_Org_Empl_Tbl_Empl_Signatory;
    private DbsField cwf_Org_Empl_Tbl_Empl_Unit_Work_Nme;
    private DbsField cwf_Org_Empl_Tbl_Empl_Nme;

    private DbsGroup cwf_Org_Empl_Tbl__R_Field_1;
    private DbsField cwf_Org_Empl_Tbl_Empl_First_Nme;
    private DbsField cwf_Org_Empl_Tbl_Empl_Last_Nme;
    private DbsField cwf_Org_Empl_Tbl_Entry_Dte_Tme;
    private DbsField cwf_Org_Empl_Tbl_Entry_Oprtr_Cde;
    private DbsField cwf_Org_Empl_Tbl_Empl_Table_Updte_Authrty_Cde;
    private DbsField cwf_Org_Empl_Tbl_Count_Castempl_Scrty_Group;

    private DbsGroup cwf_Org_Empl_Tbl_Empl_Scrty_Group;
    private DbsField cwf_Org_Empl_Tbl_Empl_Systm_Cde;
    private DbsField cwf_Org_Empl_Tbl_Empl_Scrty_Lvl_Ind;
    private DbsField cwf_Org_Empl_Tbl_Updte_Oprtr_Cde;
    private DbsField cwf_Org_Empl_Tbl_Updte_Dte;
    private DbsField cwf_Org_Empl_Tbl_Updte_Dte_Tme;
    private DbsField cwf_Org_Empl_Tbl_Dlte_Oprtr_Cde;
    private DbsField cwf_Org_Empl_Tbl_Dlte_Dte_Tme;

    private DataAccessProgramView vw_cwf_Org_Unit_Tbl;
    private DbsField cwf_Org_Unit_Tbl_Unit_Cde;
    private DbsField cwf_Org_Unit_Tbl_Unit_Short_Nme;
    private DbsField pnd_Delimeter;
    private DbsField pnd_C;

    private DbsGroup pnd_Empl_Systm_Cde;
    private DbsField pnd_Empl_Systm_Cde_Pnd_Empl_Systm_Cde_1;
    private DbsField pnd_Empl_Systm_Cde_Pnd_Empl_Systm_Cde_2;

    private DbsGroup pnd_Empl_Scrty_Lvl_Ind;
    private DbsField pnd_Empl_Scrty_Lvl_Ind_Pnd_Empl_Scrty_Lvl_Ind_1;
    private DbsField pnd_Empl_Scrty_Lvl_Ind_Pnd_Empl_Scrty_Lvl_Ind_2;
    private DbsField pnd_Unit_Cde;
    private DbsField pnd_Unit_Short_Nme;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Org_Empl_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Org_Empl_Tbl", "CWF-ORG-EMPL-TBL"), "CWF_ORG_EMPL_TBL", "CWF_ASSIGN_RULE", 
            DdmPeriodicGroups.getInstance().getGroups("CWF_ORG_EMPL_TBL"));
        cwf_Org_Empl_Tbl_Empl_Unit_Cde = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Empl_Unit_Cde", "EMPL-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "EMPL_UNIT_CDE");
        cwf_Org_Empl_Tbl_Empl_Unit_Cde.setDdmHeader("UNIT/CODE");
        cwf_Org_Empl_Tbl_Empl_Racf_Id = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "EMPL_RACF_ID");
        cwf_Org_Empl_Tbl_Empl_Racf_Id.setDdmHeader("EMPLOYEE/RACF  ID");
        cwf_Org_Empl_Tbl_Empl_Signatory = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Empl_Signatory", "EMPL-SIGNATORY", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "EMPL_SIGNATORY");
        cwf_Org_Empl_Tbl_Empl_Signatory.setDdmHeader("SIGNATORY");
        cwf_Org_Empl_Tbl_Empl_Unit_Work_Nme = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Empl_Unit_Work_Nme", "EMPL-UNIT-WORK-NME", 
            FieldType.STRING, 45, RepeatingFieldStrategy.None, "EMPL_UNIT_WORK_NME");
        cwf_Org_Empl_Tbl_Empl_Unit_Work_Nme.setDdmHeader("EXTERNAL UNIT/NAME");
        cwf_Org_Empl_Tbl_Empl_Nme = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Empl_Nme", "EMPL-NME", FieldType.STRING, 40, RepeatingFieldStrategy.None, 
            "EMPL_NME");
        cwf_Org_Empl_Tbl_Empl_Nme.setDdmHeader("EMPLOYEE NAME");

        cwf_Org_Empl_Tbl__R_Field_1 = vw_cwf_Org_Empl_Tbl.getRecord().newGroupInGroup("cwf_Org_Empl_Tbl__R_Field_1", "REDEFINE", cwf_Org_Empl_Tbl_Empl_Nme);
        cwf_Org_Empl_Tbl_Empl_First_Nme = cwf_Org_Empl_Tbl__R_Field_1.newFieldInGroup("cwf_Org_Empl_Tbl_Empl_First_Nme", "EMPL-FIRST-NME", FieldType.STRING, 
            20);
        cwf_Org_Empl_Tbl_Empl_Last_Nme = cwf_Org_Empl_Tbl__R_Field_1.newFieldInGroup("cwf_Org_Empl_Tbl_Empl_Last_Nme", "EMPL-LAST-NME", FieldType.STRING, 
            20);
        cwf_Org_Empl_Tbl_Entry_Dte_Tme = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Entry_Dte_Tme", "ENTRY-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ENTRY_DTE_TME");
        cwf_Org_Empl_Tbl_Entry_Dte_Tme.setDdmHeader("ENTRY/DT-TM");
        cwf_Org_Empl_Tbl_Entry_Oprtr_Cde = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Entry_Oprtr_Cde", "ENTRY-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ENTRY_OPRTR_CDE");
        cwf_Org_Empl_Tbl_Entry_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Org_Empl_Tbl_Empl_Table_Updte_Authrty_Cde = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Empl_Table_Updte_Authrty_Cde", 
            "EMPL-TABLE-UPDTE-AUTHRTY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "EMPL_TABLE_UPDTE_AUTHRTY_CDE");
        cwf_Org_Empl_Tbl_Empl_Table_Updte_Authrty_Cde.setDdmHeader("TABLE UPDATE/AUTHORITY");
        cwf_Org_Empl_Tbl_Count_Castempl_Scrty_Group = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Count_Castempl_Scrty_Group", "C*EMPL-SCRTY-GROUP", 
            RepeatingFieldStrategy.CAsteriskVariable, "CWF_ASSIGN_RULE_EMPL_SCRTY_GROUP");

        cwf_Org_Empl_Tbl_Empl_Scrty_Group = vw_cwf_Org_Empl_Tbl.getRecord().newGroupArrayInGroup("cwf_Org_Empl_Tbl_Empl_Scrty_Group", "EMPL-SCRTY-GROUP", 
            new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CWF_ASSIGN_RULE_EMPL_SCRTY_GROUP");
        cwf_Org_Empl_Tbl_Empl_Systm_Cde = cwf_Org_Empl_Tbl_Empl_Scrty_Group.newFieldInGroup("cwf_Org_Empl_Tbl_Empl_Systm_Cde", "EMPL-SYSTM-CDE", FieldType.STRING, 
            8, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "EMPL_SYSTM_CDE", "CWF_ASSIGN_RULE_EMPL_SCRTY_GROUP");
        cwf_Org_Empl_Tbl_Empl_Systm_Cde.setDdmHeader("SYSTEM/APPLICATION");
        cwf_Org_Empl_Tbl_Empl_Scrty_Lvl_Ind = cwf_Org_Empl_Tbl_Empl_Scrty_Group.newFieldInGroup("cwf_Org_Empl_Tbl_Empl_Scrty_Lvl_Ind", "EMPL-SCRTY-LVL-IND", 
            FieldType.STRING, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "EMPL_SCRTY_LVL_IND", "CWF_ASSIGN_RULE_EMPL_SCRTY_GROUP");
        cwf_Org_Empl_Tbl_Empl_Scrty_Lvl_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Org_Empl_Tbl_Updte_Oprtr_Cde = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Updte_Oprtr_Cde", "UPDTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "UPDTE_OPRTR_CDE");
        cwf_Org_Empl_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        cwf_Org_Empl_Tbl_Updte_Dte = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Updte_Dte", "UPDTE-DTE", FieldType.NUMERIC, 8, 
            RepeatingFieldStrategy.None, "UPDTE_DTE");
        cwf_Org_Empl_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        cwf_Org_Empl_Tbl_Updte_Dte_Tme = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Updte_Dte_Tme", "UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "UPDTE_DTE_TME");
        cwf_Org_Empl_Tbl_Dlte_Oprtr_Cde = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Dlte_Oprtr_Cde", "DLTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "DLTE_OPRTR_CDE");
        cwf_Org_Empl_Tbl_Dlte_Oprtr_Cde.setDdmHeader("DELETE/OPERATOR");
        cwf_Org_Empl_Tbl_Dlte_Dte_Tme = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Dlte_Dte_Tme", "DLTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "DLTE_DTE_TME");
        cwf_Org_Empl_Tbl_Dlte_Dte_Tme.setDdmHeader("DELETE/DATE-TIME");
        registerRecord(vw_cwf_Org_Empl_Tbl);

        vw_cwf_Org_Unit_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Org_Unit_Tbl", "CWF-ORG-UNIT-TBL"), "CWF_ORG_UNIT_TBL", "CWF_ASSIGN_RULE");
        cwf_Org_Unit_Tbl_Unit_Cde = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "UNIT_CDE");
        cwf_Org_Unit_Tbl_Unit_Cde.setDdmHeader("UNIT/CODE");
        cwf_Org_Unit_Tbl_Unit_Short_Nme = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Short_Nme", "UNIT-SHORT-NME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "UNIT_SHORT_NME");
        cwf_Org_Unit_Tbl_Unit_Short_Nme.setDdmHeader("UNIT SHORT NAME");
        registerRecord(vw_cwf_Org_Unit_Tbl);

        pnd_Delimeter = localVariables.newFieldInRecord("pnd_Delimeter", "#DELIMETER", FieldType.STRING, 1);
        pnd_C = localVariables.newFieldInRecord("pnd_C", "#C", FieldType.NUMERIC, 2);

        pnd_Empl_Systm_Cde = localVariables.newGroupArrayInRecord("pnd_Empl_Systm_Cde", "#EMPL-SYSTM-CDE", new DbsArrayController(1, 20));
        pnd_Empl_Systm_Cde_Pnd_Empl_Systm_Cde_1 = pnd_Empl_Systm_Cde.newFieldInGroup("pnd_Empl_Systm_Cde_Pnd_Empl_Systm_Cde_1", "#EMPL-SYSTM-CDE-1", FieldType.STRING, 
            8);
        pnd_Empl_Systm_Cde_Pnd_Empl_Systm_Cde_2 = pnd_Empl_Systm_Cde.newFieldInGroup("pnd_Empl_Systm_Cde_Pnd_Empl_Systm_Cde_2", "#EMPL-SYSTM-CDE-2", FieldType.STRING, 
            1);

        pnd_Empl_Scrty_Lvl_Ind = localVariables.newGroupArrayInRecord("pnd_Empl_Scrty_Lvl_Ind", "#EMPL-SCRTY-LVL-IND", new DbsArrayController(1, 20));
        pnd_Empl_Scrty_Lvl_Ind_Pnd_Empl_Scrty_Lvl_Ind_1 = pnd_Empl_Scrty_Lvl_Ind.newFieldInGroup("pnd_Empl_Scrty_Lvl_Ind_Pnd_Empl_Scrty_Lvl_Ind_1", "#EMPL-SCRTY-LVL-IND-1", 
            FieldType.STRING, 2);
        pnd_Empl_Scrty_Lvl_Ind_Pnd_Empl_Scrty_Lvl_Ind_2 = pnd_Empl_Scrty_Lvl_Ind.newFieldInGroup("pnd_Empl_Scrty_Lvl_Ind_Pnd_Empl_Scrty_Lvl_Ind_2", "#EMPL-SCRTY-LVL-IND-2", 
            FieldType.STRING, 1);
        pnd_Unit_Cde = localVariables.newFieldInRecord("pnd_Unit_Cde", "#UNIT-CDE", FieldType.STRING, 8);
        pnd_Unit_Short_Nme = localVariables.newFieldInRecord("pnd_Unit_Short_Nme", "#UNIT-SHORT-NME", FieldType.STRING, 15);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Org_Empl_Tbl.reset();
        vw_cwf_Org_Unit_Tbl.reset();

        localVariables.reset();
        pnd_Delimeter.setInitialValue(",");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfbcwf9() throws Exception
    {
        super("Cwfbcwf9");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        vw_cwf_Org_Empl_Tbl.startDatabaseRead                                                                                                                             //Natural: READ CWF-ORG-EMPL-TBL BY EMPL-RACF-ID-KEY = ' '
        (
        "READ01",
        new Wc[] { new Wc("EMPL_RACF_ID_KEY", ">=", " ", WcType.BY) },
        new Oc[] { new Oc("EMPL_RACF_ID_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_cwf_Org_Empl_Tbl.readNextRow("READ01")))
        {
            if (condition(cwf_Org_Empl_Tbl_Dlte_Oprtr_Cde.notEquals(" ") || cwf_Org_Empl_Tbl_Dlte_Dte_Tme.notEquals(getZero())))                                          //Natural: IF DLTE-OPRTR-CDE NE ' ' OR DLTE-DTE-TME NE 0
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Unit_Cde.setValue(cwf_Org_Empl_Tbl_Empl_Unit_Cde);                                                                                                        //Natural: MOVE EMPL-UNIT-CDE TO #UNIT-CDE
            vw_cwf_Org_Unit_Tbl.startDatabaseRead                                                                                                                         //Natural: READ ( 1 ) CWF-ORG-UNIT-TBL WITH UNIQ-UNIT-CDE = #UNIT-CDE
            (
            "READ02",
            new Wc[] { new Wc("UNIQ_UNIT_CDE", ">=", pnd_Unit_Cde, WcType.BY) },
            new Oc[] { new Oc("UNIQ_UNIT_CDE", "ASC") },
            1
            );
            READ02:
            while (condition(vw_cwf_Org_Unit_Tbl.readNextRow("READ02")))
            {
                pnd_Unit_Short_Nme.setValue(cwf_Org_Unit_Tbl_Unit_Short_Nme);                                                                                             //Natural: MOVE UNIT-SHORT-NME TO #UNIT-SHORT-NME
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getWorkFiles().write(1, false, cwf_Org_Empl_Tbl_Empl_Racf_Id, pnd_Delimeter, cwf_Org_Empl_Tbl_Empl_Unit_Cde, pnd_Delimeter, pnd_Unit_Short_Nme,               //Natural: WRITE WORK FILE 1 EMPL-RACF-ID #DELIMETER EMPL-UNIT-CDE #DELIMETER #UNIT-SHORT-NME #DELIMETER EMPL-TABLE-UPDTE-AUTHRTY-CDE
                pnd_Delimeter, cwf_Org_Empl_Tbl_Empl_Table_Updte_Authrty_Cde);
            cwf_Org_Empl_Tbl_Empl_Racf_Id.reset();                                                                                                                        //Natural: RESET EMPL-RACF-ID EMPL-UNIT-CDE EMPL-TABLE-UPDTE-AUTHRTY-CDE #UNIT-SHORT-NME
            cwf_Org_Empl_Tbl_Empl_Unit_Cde.reset();
            cwf_Org_Empl_Tbl_Empl_Table_Updte_Authrty_Cde.reset();
            pnd_Unit_Short_Nme.reset();
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //
}
