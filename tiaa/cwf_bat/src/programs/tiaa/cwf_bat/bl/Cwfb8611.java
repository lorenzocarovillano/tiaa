/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:45:38 PM
**        * FROM NATURAL PROGRAM : Cwfb8611
************************************************************
**        * FILE NAME            : Cwfb8611.java
**        * CLASS NAME           : Cwfb8611
**        * INSTANCE NAME        : Cwfb8611
************************************************************
************************************************************************
*
* SECURITY CERTIFICATION REMEDIATION
*
* READ SORTED WORKFILE CREATED FROM CWF-ORG-EMPL-TBL4 (045/184);
* CREATE CWF AND EWS REPORTS.
*
* 2008, 06 - GH USING CWF TABLE FOR RACF CRITERIA.
*
* 11/2008  - CS CHANGES FOR INCREASEING TABLE-VAR TO ACCOMODATE 30
*            RACF ID'S INSTEAD OF PREVIOUS HARD-CODE VALUE OF 5
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb8611 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Work_File;
    private DbsField pnd_Work_File_Pnd_Wf_A;
    private DbsField pnd_Work_File_Pnd_Wf_B;

    private DbsGroup pnd_Work_File__R_Field_1;
    private DbsField pnd_Work_File_Pnd_Wf_System;
    private DbsField pnd_Work_File_Pnd_Wf_Action_Dte;
    private DbsField pnd_Work_File_Pnd_Wf_Empl_Racf;
    private DbsField pnd_Work_File_Pnd_Wf_Unit_Cde;
    private DbsField pnd_Work_File_Pnd_Wf_Action;
    private DbsField pnd_Work_File_Pnd_Wf_Admin_Racf;
    private DbsField pnd_Work_File_Pnd_Wf_Empl_System;
    private DbsField pnd_Work_File_Pnd_Wf_Empl_Scrty;

    private DbsGroup pnd_Work_File__R_Field_2;
    private DbsField pnd_Work_File__Filler1;
    private DbsField pnd_Work_File_Pnd_Wf_Start_Dte;
    private DbsField pnd_Work_File_Pnd_Wf_End_Dte;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Cwf_Id;
    private DbsField pnd_Cwf_Ct;
    private DbsField pnd_Ews_Id;
    private DbsField pnd_Ews_Ct;
    private DbsField pnd_T_Start_Dte;
    private DbsField pnd_T_End_Dte;
    private DbsField pnd_Hold_Cwf_Sec_Id;

    private DbsGroup pnd_Hold_Cwf_Sec_Id__R_Field_3;
    private DbsField pnd_Hold_Cwf_Sec_Id_Pnd_Hold_Cwf_Sec_Id_1;
    private DbsField pnd_Hold_Cwf_Sec_Id_Pnd_Hold_Cwf_Sec_Id_X;

    private DataAccessProgramView vw_cwf_Suptb;
    private DbsField cwf_Suptb_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Suptb_Tbl_Table_Nme;
    private DbsField cwf_Suptb_Tbl_Key_Field;
    private DbsField cwf_Suptb_Tbl_Data_Field;
    private DbsField cwf_Suptb_Tbl_Actve_Ind;
    private DbsField cwf_Suptb_Tbl_Updte_Dte_Tme;
    private DbsField cwf_Suptb_Tbl_Updte_Dte;
    private DbsField cwf_Suptb_Tbl_Updte_Oprtr_Cde;

    private DbsGroup pnd_Table_Var;
    private DbsField pnd_Table_Var_Pnd_Cwf_Sec_Id;

    private DbsGroup pnd_Table_Var__R_Field_4;
    private DbsField pnd_Table_Var_Pnd_Cwf_Sec_Id_Array;
    private DbsField pnd_Table_Var_Pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Table_Var__R_Field_5;
    private DbsField pnd_Table_Var_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Table_Var_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Table_Var_Pnd_Tbl_Key_Field;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Work_File = localVariables.newGroupInRecord("pnd_Work_File", "#WORK-FILE");
        pnd_Work_File_Pnd_Wf_A = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Wf_A", "#WF-A", FieldType.STRING, 240);
        pnd_Work_File_Pnd_Wf_B = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Wf_B", "#WF-B", FieldType.STRING, 60);

        pnd_Work_File__R_Field_1 = localVariables.newGroupInRecord("pnd_Work_File__R_Field_1", "REDEFINE", pnd_Work_File);
        pnd_Work_File_Pnd_Wf_System = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wf_System", "#WF-SYSTEM", FieldType.STRING, 3);
        pnd_Work_File_Pnd_Wf_Action_Dte = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wf_Action_Dte", "#WF-ACTION-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_File_Pnd_Wf_Empl_Racf = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wf_Empl_Racf", "#WF-EMPL-RACF", FieldType.STRING, 
            8);
        pnd_Work_File_Pnd_Wf_Unit_Cde = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wf_Unit_Cde", "#WF-UNIT-CDE", FieldType.STRING, 8);
        pnd_Work_File_Pnd_Wf_Action = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wf_Action", "#WF-ACTION", FieldType.STRING, 5);
        pnd_Work_File_Pnd_Wf_Admin_Racf = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wf_Admin_Racf", "#WF-ADMIN-RACF", FieldType.STRING, 
            8);
        pnd_Work_File_Pnd_Wf_Empl_System = pnd_Work_File__R_Field_1.newFieldArrayInGroup("pnd_Work_File_Pnd_Wf_Empl_System", "#WF-EMPL-SYSTEM", FieldType.STRING, 
            8, new DbsArrayController(1, 20));
        pnd_Work_File_Pnd_Wf_Empl_Scrty = pnd_Work_File__R_Field_1.newFieldArrayInGroup("pnd_Work_File_Pnd_Wf_Empl_Scrty", "#WF-EMPL-SCRTY", FieldType.STRING, 
            2, new DbsArrayController(1, 20));

        pnd_Work_File__R_Field_2 = localVariables.newGroupInRecord("pnd_Work_File__R_Field_2", "REDEFINE", pnd_Work_File);
        pnd_Work_File__Filler1 = pnd_Work_File__R_Field_2.newFieldInGroup("pnd_Work_File__Filler1", "_FILLER1", FieldType.STRING, 3);
        pnd_Work_File_Pnd_Wf_Start_Dte = pnd_Work_File__R_Field_2.newFieldInGroup("pnd_Work_File_Pnd_Wf_Start_Dte", "#WF-START-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_File_Pnd_Wf_End_Dte = pnd_Work_File__R_Field_2.newFieldInGroup("pnd_Work_File_Pnd_Wf_End_Dte", "#WF-END-DTE", FieldType.NUMERIC, 8);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 2);
        pnd_Cwf_Id = localVariables.newFieldArrayInRecord("pnd_Cwf_Id", "#CWF-ID", FieldType.NUMERIC, 7, new DbsArrayController(1, 30));
        pnd_Cwf_Ct = localVariables.newFieldInRecord("pnd_Cwf_Ct", "#CWF-CT", FieldType.NUMERIC, 7);
        pnd_Ews_Id = localVariables.newFieldArrayInRecord("pnd_Ews_Id", "#EWS-ID", FieldType.NUMERIC, 7, new DbsArrayController(1, 30));
        pnd_Ews_Ct = localVariables.newFieldInRecord("pnd_Ews_Ct", "#EWS-CT", FieldType.NUMERIC, 7);
        pnd_T_Start_Dte = localVariables.newFieldInRecord("pnd_T_Start_Dte", "#T-START-DTE", FieldType.NUMERIC, 8);
        pnd_T_End_Dte = localVariables.newFieldInRecord("pnd_T_End_Dte", "#T-END-DTE", FieldType.NUMERIC, 8);
        pnd_Hold_Cwf_Sec_Id = localVariables.newFieldInRecord("pnd_Hold_Cwf_Sec_Id", "#HOLD-CWF-SEC-ID", FieldType.STRING, 8);

        pnd_Hold_Cwf_Sec_Id__R_Field_3 = localVariables.newGroupInRecord("pnd_Hold_Cwf_Sec_Id__R_Field_3", "REDEFINE", pnd_Hold_Cwf_Sec_Id);
        pnd_Hold_Cwf_Sec_Id_Pnd_Hold_Cwf_Sec_Id_1 = pnd_Hold_Cwf_Sec_Id__R_Field_3.newFieldInGroup("pnd_Hold_Cwf_Sec_Id_Pnd_Hold_Cwf_Sec_Id_1", "#HOLD-CWF-SEC-ID-1", 
            FieldType.STRING, 1);
        pnd_Hold_Cwf_Sec_Id_Pnd_Hold_Cwf_Sec_Id_X = pnd_Hold_Cwf_Sec_Id__R_Field_3.newFieldInGroup("pnd_Hold_Cwf_Sec_Id_Pnd_Hold_Cwf_Sec_Id_X", "#HOLD-CWF-SEC-ID-X", 
            FieldType.STRING, 7);

        vw_cwf_Suptb = new DataAccessProgramView(new NameInfo("vw_cwf_Suptb", "CWF-SUPTB"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Suptb_Tbl_Scrty_Level_Ind = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Suptb_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Suptb_Tbl_Table_Nme = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "TBL_TABLE_NME");
        cwf_Suptb_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Suptb_Tbl_Key_Field = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "TBL_KEY_FIELD");
        cwf_Suptb_Tbl_Data_Field = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 253, RepeatingFieldStrategy.None, 
            "TBL_DATA_FIELD");
        cwf_Suptb_Tbl_Actve_Ind = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Actve_Ind", "TBL-ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TBL_ACTVE_IND");
        cwf_Suptb_Tbl_Updte_Dte_Tme = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Updte_Dte_Tme", "TBL-UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TBL_UPDTE_DTE_TME");
        cwf_Suptb_Tbl_Updte_Dte_Tme.setDdmHeader("UPDATE DATE/TIME");
        cwf_Suptb_Tbl_Updte_Dte = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Updte_Dte", "TBL-UPDTE-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TBL_UPDTE_DTE");
        cwf_Suptb_Tbl_Updte_Dte.setDdmHeader("UPDATE/DATE");
        cwf_Suptb_Tbl_Updte_Oprtr_Cde = vw_cwf_Suptb.getRecord().newFieldInGroup("cwf_Suptb_Tbl_Updte_Oprtr_Cde", "TBL-UPDTE-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TBL_UPDTE_OPRTR_CDE");
        cwf_Suptb_Tbl_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        registerRecord(vw_cwf_Suptb);

        pnd_Table_Var = localVariables.newGroupInRecord("pnd_Table_Var", "#TABLE-VAR");
        pnd_Table_Var_Pnd_Cwf_Sec_Id = pnd_Table_Var.newFieldInGroup("pnd_Table_Var_Pnd_Cwf_Sec_Id", "#CWF-SEC-ID", FieldType.STRING, 240);

        pnd_Table_Var__R_Field_4 = pnd_Table_Var.newGroupInGroup("pnd_Table_Var__R_Field_4", "REDEFINE", pnd_Table_Var_Pnd_Cwf_Sec_Id);
        pnd_Table_Var_Pnd_Cwf_Sec_Id_Array = pnd_Table_Var__R_Field_4.newFieldArrayInGroup("pnd_Table_Var_Pnd_Cwf_Sec_Id_Array", "#CWF-SEC-ID-ARRAY", 
            FieldType.STRING, 8, new DbsArrayController(1, 30));
        pnd_Table_Var_Pnd_Tbl_Prime_Key = pnd_Table_Var.newFieldInGroup("pnd_Table_Var_Pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Table_Var__R_Field_5 = pnd_Table_Var.newGroupInGroup("pnd_Table_Var__R_Field_5", "REDEFINE", pnd_Table_Var_Pnd_Tbl_Prime_Key);
        pnd_Table_Var_Pnd_Tbl_Scrty_Level_Ind = pnd_Table_Var__R_Field_5.newFieldInGroup("pnd_Table_Var_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Table_Var_Pnd_Tbl_Table_Nme = pnd_Table_Var__R_Field_5.newFieldInGroup("pnd_Table_Var_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Table_Var_Pnd_Tbl_Key_Field = pnd_Table_Var__R_Field_5.newFieldInGroup("pnd_Table_Var_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Suptb.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb8611() throws Exception
    {
        super("Cwfb8611");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 55;//Natural: FORMAT ( 2 ) PS = 55
        //*  RETRIEVE THE CWF SEC IDS    /* GH 2008, JUNE
        pnd_Table_Var_Pnd_Tbl_Scrty_Level_Ind.setValue("S ");                                                                                                             //Natural: MOVE 'S ' TO #TBL-SCRTY-LEVEL-IND
        pnd_Table_Var_Pnd_Tbl_Table_Nme.setValue("CWF-EWS-SEC-CERTIFY");                                                                                                  //Natural: MOVE 'CWF-EWS-SEC-CERTIFY' TO #TBL-TABLE-NME
        pnd_Table_Var_Pnd_Tbl_Key_Field.setValue("CWF PRODUCTION SUPPORT ID");                                                                                            //Natural: MOVE 'CWF PRODUCTION SUPPORT ID' TO #TBL-KEY-FIELD
        vw_cwf_Suptb.startDatabaseRead                                                                                                                                    //Natural: READ ( 1 ) CWF-SUPTB WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
        (
        "READ01",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Table_Var_Pnd_Tbl_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(vw_cwf_Suptb.readNextRow("READ01")))
        {
            if (condition(cwf_Suptb_Tbl_Scrty_Level_Ind.notEquals(pnd_Table_Var_Pnd_Tbl_Scrty_Level_Ind) || cwf_Suptb_Tbl_Table_Nme.notEquals(pnd_Table_Var_Pnd_Tbl_Table_Nme)  //Natural: IF TBL-SCRTY-LEVEL-IND NE #TBL-SCRTY-LEVEL-IND OR TBL-TABLE-NME NE #TBL-TABLE-NME OR TBL-KEY-FIELD NE #TBL-KEY-FIELD
                || cwf_Suptb_Tbl_Key_Field.notEquals(pnd_Table_Var_Pnd_Tbl_Key_Field)))
            {
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*  MIT TABLE MOVED TO TBL-VAR
            pnd_Table_Var_Pnd_Cwf_Sec_Id.setValue(cwf_Suptb_Tbl_Data_Field);                                                                                              //Natural: MOVE TBL-DATA-FIELD TO #CWF-SEC-ID
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getWorkFiles().read(1, pnd_Work_File);                                                                                                                            //Natural: READ WORK 1 ONCE #WORK-FILE
        pnd_T_Start_Dte.setValue(pnd_Work_File_Pnd_Wf_Start_Dte);                                                                                                         //Natural: ASSIGN #T-START-DTE := #WF-START-DTE
        pnd_T_End_Dte.setValue(pnd_Work_File_Pnd_Wf_End_Dte);                                                                                                             //Natural: ASSIGN #T-END-DTE := #WF-END-DTE
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT *PROGRAM 20X 'Corporate Workflow' / *DATX ( EM = LLL' 'DD', 'YYYY ) 10X 'Security Compliance Remediation' / 'Page:' *PAGE-NUMBER ( 1 ) ( EM = ZZZ9 ) 14X 'From' #T-START-DTE 'To' #T-END-DTE /
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 2 ) TITLE LEFT *PROGRAM 22X 'Electronic Warrant System' / *DATX ( EM = LLL' 'DD', 'YYYY ) 15X 'Security Compliance Remediation' / 'Page:' *PAGE-NUMBER ( 2 ) ( EM = ZZZ9 ) 19X 'From' #T-START-DTE 'To' #T-END-DTE /
        READWORK02:                                                                                                                                                       //Natural: READ WORK 1 #WORK-FILE
        while (condition(getWorkFiles().read(1, pnd_Work_File)))
        {
            short decideConditionsMet112 = 0;                                                                                                                             //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #WF-SYSTEM = 'CWF'
            if (condition(pnd_Work_File_Pnd_Wf_System.equals("CWF")))
            {
                decideConditionsMet112++;
                getReports().display(1, new ColumnSpacing(15),"Process/Date",                                                                                             //Natural: DISPLAY ( 1 ) 15X 'Process/Date' #WF-ACTION-DTE 'Racf/Modified' #WF-EMPL-RACF '/Unit' #WF-UNIT-CDE '/Action' #WF-ACTION 'Admin/Racf id' #WF-ADMIN-RACF
                		pnd_Work_File_Pnd_Wf_Action_Dte,"Racf/Modified",
                		pnd_Work_File_Pnd_Wf_Empl_Racf,"/Unit",
                		pnd_Work_File_Pnd_Wf_Unit_Cde,"/Action",
                		pnd_Work_File_Pnd_Wf_Action,"Admin/Racf id",
                		pnd_Work_File_Pnd_Wf_Admin_Racf);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                FOR01:                                                                                                                                                    //Natural: FOR #J 1 30
                for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(30)); pnd_J.nadd(1))
                {
                    short decideConditionsMet117 = 0;                                                                                                                     //Natural: DECIDE ON FIRST VALUE OF #WF-ADMIN-RACF;//Natural: VALUE #CWF-SEC-ID-ARRAY ( #J )
                    if (condition((pnd_Work_File_Pnd_Wf_Admin_Racf.equals(pnd_Table_Var_Pnd_Cwf_Sec_Id_Array.getValue(pnd_J)))))
                    {
                        decideConditionsMet117++;
                        pnd_Cwf_Id.getValue(pnd_J).nadd(1);                                                                                                               //Natural: ADD 1 TO #CWF-ID ( #J )
                        pnd_Cwf_Ct.nadd(1);                                                                                                                               //Natural: ADD 1 TO #CWF-CT
                        //*        VALUE  #CWF-SEC-ID-ARRAY(1)
                        //*          ADD 1 TO #CWF-ID1
                        //*        VALUE  #CWF-SEC-ID-ARRAY(2)
                        //*          ADD 1 TO #CWF-ID2
                        //*        VALUE  #CWF-SEC-ID-ARRAY(3)
                        //*          ADD 1 TO #CWF-ID3
                        //*        VALUE  #CWF-SEC-ID-ARRAY(4)
                        //*          ADD 1 TO #CWF-ID4
                        //*        VALUE  #CWF-SEC-ID-ARRAY(5)
                        //*          ADD 1 TO #CWF-ID5
                        //*        ANY VALUES
                        //*          ADD 1 TO #CWF-CT
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN #WF-SYSTEM = 'EWS'
            if (condition(pnd_Work_File_Pnd_Wf_System.equals("EWS")))
            {
                decideConditionsMet112++;
                getReports().display(2, new ColumnSpacing(10),"Process/Date",                                                                                             //Natural: DISPLAY ( 2 ) 10X 'Process/Date' #WF-ACTION-DTE 'Racf/Modified' #WF-EMPL-RACF '/Unit' #WF-UNIT-CDE '/Action' #WF-ACTION 'Admin/Racf id' #WF-ADMIN-RACF '/System(s)' #WF-EMPL-SYSTEM ( 1 ) '/Security' #WF-EMPL-SCRTY ( 1 )
                		pnd_Work_File_Pnd_Wf_Action_Dte,"Racf/Modified",
                		pnd_Work_File_Pnd_Wf_Empl_Racf,"/Unit",
                		pnd_Work_File_Pnd_Wf_Unit_Cde,"/Action",
                		pnd_Work_File_Pnd_Wf_Action,"Admin/Racf id",
                		pnd_Work_File_Pnd_Wf_Admin_Racf,"/System(s)",
                		pnd_Work_File_Pnd_Wf_Empl_System.getValue(1),"/Security",
                		pnd_Work_File_Pnd_Wf_Empl_Scrty.getValue(1));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                FOR02:                                                                                                                                                    //Natural: FOR #I 2 8
                for (pnd_I.setValue(2); condition(pnd_I.lessOrEqual(8)); pnd_I.nadd(1))
                {
                    if (condition(pnd_Work_File_Pnd_Wf_Empl_System.getValue(pnd_I).notEquals(" ")))                                                                       //Natural: IF #WF-EMPL-SYSTEM ( #I ) NE ' '
                    {
                        getReports().write(2, new ReportTAsterisk(pnd_Work_File_Pnd_Wf_Empl_System.getValue(1)),pnd_Work_File_Pnd_Wf_Empl_System.getValue(pnd_I),new      //Natural: WRITE ( 2 ) T*#WF-EMPL-SYSTEM ( 1 ) #WF-EMPL-SYSTEM ( #I ) T*#WF-EMPL-SCRTY ( 1 ) #WF-EMPL-SCRTY ( #I )
                            ReportTAsterisk(pnd_Work_File_Pnd_Wf_Empl_Scrty.getValue(1)),pnd_Work_File_Pnd_Wf_Empl_Scrty.getValue(pnd_I));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                FOR03:                                                                                                                                                    //Natural: FOR #J 1 30
                for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(30)); pnd_J.nadd(1))
                {
                    short decideConditionsMet148 = 0;                                                                                                                     //Natural: DECIDE ON FIRST VALUE OF #WF-ADMIN-RACF;//Natural: VALUE #CWF-SEC-ID-ARRAY ( #J )
                    if (condition((pnd_Work_File_Pnd_Wf_Admin_Racf.equals(pnd_Table_Var_Pnd_Cwf_Sec_Id_Array.getValue(pnd_J)))))
                    {
                        decideConditionsMet148++;
                        pnd_Ews_Id.getValue(pnd_J).nadd(1);                                                                                                               //Natural: ADD 1 TO #EWS-ID ( #J )
                        pnd_Ews_Ct.nadd(1);                                                                                                                               //Natural: ADD 1 TO #EWS-CT
                        //*        VALUE  #CWF-SEC-ID-ARRAY(1)
                        //*          ADD 1 TO #EWS-ID1
                        //*        VALUE  #CWF-SEC-ID-ARRAY(2)
                        //*          ADD 1 TO #EWS-ID2
                        //*        VALUE  #CWF-SEC-ID-ARRAY(3)
                        //*          ADD 1 TO #EWS-ID3
                        //*        VALUE  #CWF-SEC-ID-ARRAY(4)
                        //*          ADD 1 TO #EWS-ID4
                        //*        VALUE  #CWF-SEC-ID-ARRAY(5)
                        //*          ADD 1 TO #EWS-ID5
                        //*        ANY VALUES
                        //*          ADD 1 TO #EWS-CT
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet112 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_Cwf_Ct.equals(getZero())))                                                                                                                      //Natural: IF #CWF-CT = 0
        {
            getReports().write(1, NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"******************************************************************",                   //Natural: WRITE ( 1 ) ///// / '******************************************************************' / '***                                                            ***' / '***           No Activities for Corporate Workflow             ***' / '***                                                            ***' / '******************************************************************'
                NEWLINE,"***                                                            ***",NEWLINE,"***           No Activities for Corporate Workflow             ***",
                NEWLINE,"***                                                            ***",NEWLINE,"******************************************************************");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ews_Ct.equals(getZero())))                                                                                                                      //Natural: IF #EWS-CT = 0
        {
            getReports().write(2, NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"******************************************************************",                   //Natural: WRITE ( 2 ) ///// / '******************************************************************' / '***                                                            ***' / '***           No Activities for Electronic Warrants            ***' / '***                                                            ***' / '******************************************************************'
                NEWLINE,"***                                                            ***",NEWLINE,"***           No Activities for Electronic Warrants            ***",
                NEWLINE,"***                                                            ***",NEWLINE,"******************************************************************");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        FOR04:                                                                                                                                                            //Natural: FOR #J 1 30
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(30)); pnd_J.nadd(1))
        {
            pnd_Hold_Cwf_Sec_Id.setValue(pnd_Table_Var_Pnd_Cwf_Sec_Id_Array.getValue(pnd_J));                                                                             //Natural: MOVE #CWF-SEC-ID-ARRAY ( #J ) TO #HOLD-CWF-SEC-ID
            if (condition(pnd_Table_Var_Pnd_Cwf_Sec_Id_Array.getValue(pnd_J).equals(" ") || pnd_Hold_Cwf_Sec_Id_Pnd_Hold_Cwf_Sec_Id_1.equals("<") || pnd_Hold_Cwf_Sec_Id_Pnd_Hold_Cwf_Sec_Id_1.equals("-")  //Natural: IF ( ( #CWF-SEC-ID-ARRAY ( #J ) = ' ' ) OR ( #HOLD-CWF-SEC-ID-1 = '<' OR = '-' OR = ' ' ) )
                || pnd_Hold_Cwf_Sec_Id_Pnd_Hold_Cwf_Sec_Id_1.equals(" ")))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*   WRITE (1) ///
            getReports().write(1, pnd_Cwf_Id.getValue(pnd_J), new ReportEditMask ("ZZZZ,ZZ9"),pnd_Table_Var_Pnd_Cwf_Sec_Id_Array.getValue(pnd_J));                        //Natural: WRITE ( 1 ) #CWF-ID ( #J ) ( EM = ZZZZ,ZZ9 ) #CWF-SEC-ID-ARRAY ( #J )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  / #CWF-ID1             (EM=ZZZZ,ZZ9) #CWF-SEC-ID-ARRAY(1)
            //*  / #CWF-ID2             (EM=ZZZZ,ZZ9) #CWF-SEC-ID-ARRAY(2)
            //*  / #CWF-ID3             (EM=ZZZZ,ZZ9) #CWF-SEC-ID-ARRAY(3)
            //*  / #CWF-ID4             (EM=ZZZZ,ZZ9) #CWF-SEC-ID-ARRAY(4)
            //*  / #CWF-ID5             (EM=ZZZZ,ZZ9) #CWF-SEC-ID-ARRAY(5)
            //*  / #CWF-CT              (EM=ZZZZ,ZZ9) 'TOTAL CWF'
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(1, pnd_Cwf_Ct, new ReportEditMask ("ZZZZ,ZZ9"),"TOTAL CWF");                                                                                   //Natural: WRITE ( 1 ) #CWF-CT ( EM = ZZZZ,ZZ9 ) 'TOTAL CWF'
        if (Global.isEscape()) return;
        FOR05:                                                                                                                                                            //Natural: FOR #J 1 30
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(30)); pnd_J.nadd(1))
        {
            pnd_Hold_Cwf_Sec_Id.setValue(pnd_Table_Var_Pnd_Cwf_Sec_Id_Array.getValue(pnd_J));                                                                             //Natural: MOVE #CWF-SEC-ID-ARRAY ( #J ) TO #HOLD-CWF-SEC-ID
            if (condition(pnd_Table_Var_Pnd_Cwf_Sec_Id_Array.getValue(pnd_J).equals(" ") || pnd_Hold_Cwf_Sec_Id_Pnd_Hold_Cwf_Sec_Id_1.equals("<") || pnd_Hold_Cwf_Sec_Id_Pnd_Hold_Cwf_Sec_Id_1.equals("-")  //Natural: IF ( ( #CWF-SEC-ID-ARRAY ( #J ) = ' ' ) OR ( #HOLD-CWF-SEC-ID-1 = '<' OR = '-' OR = ' ' ) )
                || pnd_Hold_Cwf_Sec_Id_Pnd_Hold_Cwf_Sec_Id_1.equals(" ")))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  WRITE (2) ///
            getReports().write(2, pnd_Ews_Id.getValue(pnd_J), new ReportEditMask ("ZZZZ,ZZ9"),pnd_Table_Var_Pnd_Cwf_Sec_Id_Array.getValue(pnd_J));                        //Natural: WRITE ( 2 ) #EWS-ID ( #J ) ( EM = ZZZZ,ZZ9 ) #CWF-SEC-ID-ARRAY ( #J )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  / #EWS-ID1             (EM=ZZZZ,ZZ9) #CWF-SEC-ID-ARRAY(1)
            //*  / #EWS-ID2             (EM=ZZZZ,ZZ9) #CWF-SEC-ID-ARRAY(2)
            //*  / #EWS-ID3             (EM=ZZZZ,ZZ9) #CWF-SEC-ID-ARRAY(3)
            //*  / #EWS-ID4             (EM=ZZZZ,ZZ9) #CWF-SEC-ID-ARRAY(4)
            //*  / #EWS-ID5             (EM=ZZZZ,ZZ9) #CWF-SEC-ID-ARRAY(5)
            //*  / #EWS-CT              (EM=ZZZZ,ZZ9) 'TOTAL EWS'
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(2, pnd_Ews_Ct, new ReportEditMask ("ZZZZ,ZZ9"),"TOTAL EWS");                                                                                   //Natural: WRITE ( 2 ) #EWS-CT ( EM = ZZZZ,ZZ9 ) 'TOTAL EWS'
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "PS=55");
        Global.format(2, "PS=55");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getPROGRAM(),new ColumnSpacing(20),"Corporate Workflow",NEWLINE,Global.getDATX(), 
            new ReportEditMask ("LLL' 'DD', 'YYYY"),new ColumnSpacing(10),"Security Compliance Remediation",NEWLINE,"Page:",getReports().getPageNumberDbs(1), 
            new ReportEditMask ("ZZZ9"),new ColumnSpacing(14),"From",pnd_T_Start_Dte,"To",pnd_T_End_Dte,NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getPROGRAM(),new ColumnSpacing(22),"Electronic Warrant System",NEWLINE,Global.getDATX(), 
            new ReportEditMask ("LLL' 'DD', 'YYYY"),new ColumnSpacing(15),"Security Compliance Remediation",NEWLINE,"Page:",getReports().getPageNumberDbs(2), 
            new ReportEditMask ("ZZZ9"),new ColumnSpacing(19),"From",pnd_T_Start_Dte,"To",pnd_T_End_Dte,NEWLINE);

        getReports().setDisplayColumns(1, new ColumnSpacing(15),"Process/Date",
        		pnd_Work_File_Pnd_Wf_Action_Dte,"Racf/Modified",
        		pnd_Work_File_Pnd_Wf_Empl_Racf,"/Unit",
        		pnd_Work_File_Pnd_Wf_Unit_Cde,"/Action",
        		pnd_Work_File_Pnd_Wf_Action,"Admin/Racf id",
        		pnd_Work_File_Pnd_Wf_Admin_Racf);
        getReports().setDisplayColumns(2, new ColumnSpacing(10),"Process/Date",
        		pnd_Work_File_Pnd_Wf_Action_Dte,"Racf/Modified",
        		pnd_Work_File_Pnd_Wf_Empl_Racf,"/Unit",
        		pnd_Work_File_Pnd_Wf_Unit_Cde,"/Action",
        		pnd_Work_File_Pnd_Wf_Action,"Admin/Racf id",
        		pnd_Work_File_Pnd_Wf_Admin_Racf,"/System(s)",
        		pnd_Work_File_Pnd_Wf_Empl_System,"/Security",
        		pnd_Work_File_Pnd_Wf_Empl_Scrty);
    }
}
