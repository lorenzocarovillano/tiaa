/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:52:07 PM
**        * FROM NATURAL PROGRAM : Iwfp1600
************************************************************
**        * FILE NAME            : Iwfp1600.java
**        * CLASS NAME           : Iwfp1600
**        * INSTANCE NAME        : Iwfp1600
************************************************************
************************************************************************
* PROGRAM  : IWFP1600
* SYSTEM   : CRPCWF
* WRITTEN  : JUNE 4, 1999
* FUNCTION : THIS PROGRAM READS THE IWF-MCSS-CALLS AND DELETES ALL
*             RECORDS LONGER THAN 3 MONTHS
* HISTORY
* 04/22/02 : IE CHANGES MADE TO FIX THE NAT3047 ERROR. PLEASE SCAN FOR
*            EPPEL
* 02/23/2017 - SINGAK - PIN EXPANSION - AUG 2017
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iwfp1600 extends BLNatBase
{
    // Data Areas
    private GdaCwfg000 gdaCwfg000;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iwf_Mcss_Calls;
    private DbsField iwf_Mcss_Calls_Rcrd_Stamp_Tme;
    private DbsField iwf_Mcss_Calls_Rcrd_Upld_Tme;
    private DbsField iwf_Mcss_Calls_Source_Id;
    private DbsField iwf_Mcss_Calls_Btch_Nbr;
    private DbsField iwf_Mcss_Calls_Rqst_Type_Cde;
    private DbsField iwf_Mcss_Calls_Cstmr_Id;

    private DbsGroup iwf_Mcss_Calls__R_Field_1;
    private DbsField iwf_Mcss_Calls_Iis_Hrchy_Level_Cde;
    private DbsField iwf_Mcss_Calls_Ppg_Cde;

    private DbsGroup iwf_Mcss_Calls__R_Field_2;
    private DbsField iwf_Mcss_Calls_Iis_Hrchy_Key_Cde;

    private DbsGroup iwf_Mcss_Calls__R_Field_3;
    private DbsField iwf_Mcss_Calls_Np_Pin;

    private DbsGroup iwf_Mcss_Calls__R_Field_4;
    private DbsField iwf_Mcss_Calls_Pin;
    private DbsField iwf_Mcss_Calls_Log_Sqnce_Nbr;
    private DbsField iwf_Mcss_Calls_Psig_Lvl_Cde;

    private DbsGroup iwf_Mcss_Calls__R_Field_5;
    private DbsField iwf_Mcss_Calls_Psig_Lvl;
    private DbsField iwf_Mcss_Calls_Psig_Cde;
    private DbsField iwf_Mcss_Calls_Rqst_Entry_Op_Cde;
    private DbsField iwf_Mcss_Calls_Rqst_Origin_Unit_Cde;
    private DbsField iwf_Mcss_Calls_Spcl_Hndlng_Txt;
    private DbsField iwf_Mcss_Calls_Attn_Txt;
    private DbsField iwf_Mcss_Calls_Rcrd_Type_Cde;
    private DbsField iwf_Mcss_Calls_Work_Prcss_Id;
    private DbsField iwf_Mcss_Calls_Error_Msg_Txt;

    private DbsGroup iwf_Mcss_Calls_Request_Array_Grp;
    private DbsField iwf_Mcss_Calls_Dstntn_Unt_Cde;
    private DbsField iwf_Mcss_Calls_Corp_Status_Cde;
    private DbsField iwf_Mcss_Calls_Unit_Status_Cde;
    private DbsField iwf_Mcss_Calls_Work_Req_Prty_Cde;

    private DbsGroup iwf_Mcss_Calls_Wfo_All_Othr_Flds;
    private DbsField iwf_Mcss_Calls_Wfo_Othr_Flds;

    private DbsGroup iwf_Mcss_Calls__R_Field_6;
    private DbsField iwf_Mcss_Calls_Default_Ppg_Cde_Ind;
    private DbsField iwf_Mcss_Calls_Init_Undrlying_Rqst_Cnt;
    private DbsField iwf_Mcss_Calls_Rqst_Orgn_Cde;
    private DbsField iwf_Mcss_Calls_Rqst_Log_Dte_Tme;

    private DbsGroup iwf_Mcss_Calls__R_Field_7;
    private DbsField iwf_Mcss_Calls_Rqst_Log_Dte;
    private DbsField iwf_Mcss_Calls_Rqst_Log_Tme;
    private DbsField iwf_Mcss_Calls_Tiaa_Rcvd_Dte_Tme;

    private DbsGroup iwf_Mcss_Calls__R_Field_8;
    private DbsField iwf_Mcss_Calls_Tiaa_Rcvd_Dte;
    private DbsField iwf_Mcss_Calls_Tiaa_Rcvd_Tme;
    private DbsField iwf_Mcss_Calls_Check_Ind;
    private DbsField iwf_Mcss_Calls_Applctns_Rcvd_Ind;
    private DbsField iwf_Mcss_Calls_Addtnl_Wpid_Ind;
    private DbsField iwf_Mcss_Calls_Img_Paper_Prcssng_Ind;
    private DbsField iwf_Mcss_Calls_Prvte_Rqst_Ind;
    private DbsField iwf_Mcss_Calls_Rescan_Ind;
    private DbsField iwf_Mcss_Calls_Prefill_Ind;
    private DbsField iwf_Mcss_Calls_Intgrtd_Unit_Ind;
    private DbsField iwf_Mcss_Calls_Print_Q_Ind;
    private DbsField iwf_Mcss_Calls_Print_Dte_Tme;
    private DbsField iwf_Mcss_Calls_Print_Batch_Id_Nbr;

    private DbsGroup iwf_Mcss_Calls__R_Field_9;
    private DbsField iwf_Mcss_Calls_Print_Prefix_Ind;
    private DbsField iwf_Mcss_Calls_Print_Batch_Nbr;
    private DbsField iwf_Mcss_Calls_Print_Batch_Sqnce_Nbr;
    private DbsField iwf_Mcss_Calls_Print_Batch_Ind;
    private DbsField iwf_Mcss_Calls_Printer_Id_Cde;
    private DbsField iwf_Mcss_Calls_Mstr_Indx_Actn_Cde;
    private DbsField iwf_Mcss_Calls_Cases_Mismatch;

    private DbsGroup iwf_Mcss_Calls_Dcmnt_Array;
    private DbsField iwf_Mcss_Calls_Dcmnt_Ctgry;
    private DbsField iwf_Mcss_Calls_Dcmnt_Sub_Ctgry;
    private DbsField iwf_Mcss_Calls_Dcmnt_Dtl;
    private DbsField iwf_Mcss_Calls_Dcmnt_Frmt;
    private DbsField iwf_Mcss_Calls_Dcmnt_Drctn;
    private DbsField iwf_Mcss_Calls_Dcmnt_Rtntn;
    private DbsField iwf_Mcss_Calls_Dcmnt_Scrty_Ind;
    private DbsField iwf_Mcss_Calls_Dcmnt_Anttn_Ind;
    private DbsField iwf_Mcss_Calls_Dcmnt_Copy_Ind;
    private DbsField iwf_Mcss_Calls_Dcmnt_Rcvd_Dte;
    private DbsField iwf_Mcss_Calls_Prvte_Dcmnt_Ind;
    private DbsField iwf_Mcss_Calls_Mail_Item_Nbr;
    private DbsField iwf_Mcss_Calls_Start_Page_Nbr;
    private DbsField iwf_Mcss_Calls_Image_End_Page;
    private DbsField pnd_Tot_Del;
    private DbsField pnd_Tot_Read;
    private DbsField pnd_Input_To_D;
    private DbsField pnd_Cnnctd_Purg_Key;

    private DbsGroup pnd_Cnnctd_Purg_Key__R_Field_10;
    private DbsField pnd_Cnnctd_Purg_Key_Pnd_Start_Upld_Tme;
    private DbsField pnd_Cnnctd_Purg_Key_Pnd_Source_Id;
    private DbsField pnd_Cnnctd_Purg_Key_Pnd_Btch_Nbr;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaCwfg000 = GdaCwfg000.getInstance(getCallnatLevel());
        registerRecord(gdaCwfg000);
        if (gdaOnly) return;

        // Local Variables
        localVariables = new DbsRecord();

        vw_iwf_Mcss_Calls = new DataAccessProgramView(new NameInfo("vw_iwf_Mcss_Calls", "IWF-MCSS-CALLS"), "IWF_MCSS_CALLS", "CWF_MCSS_CALLS", DdmPeriodicGroups.getInstance().getGroups("IWF_MCSS_CALLS"));
        iwf_Mcss_Calls_Rcrd_Stamp_Tme = vw_iwf_Mcss_Calls.getRecord().newFieldInGroup("iwf_Mcss_Calls_Rcrd_Stamp_Tme", "RCRD-STAMP-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "RCRD_STAMP_TME");
        iwf_Mcss_Calls_Rcrd_Stamp_Tme.setDdmHeader("RECORD/TIME/STAMP");
        iwf_Mcss_Calls_Rcrd_Upld_Tme = vw_iwf_Mcss_Calls.getRecord().newFieldInGroup("iwf_Mcss_Calls_Rcrd_Upld_Tme", "RCRD-UPLD-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "RCRD_UPLD_TME");
        iwf_Mcss_Calls_Rcrd_Upld_Tme.setDdmHeader("RECORD/UPLOAD/TIME");
        iwf_Mcss_Calls_Source_Id = vw_iwf_Mcss_Calls.getRecord().newFieldInGroup("iwf_Mcss_Calls_Source_Id", "SOURCE-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "SOURCE_ID");
        iwf_Mcss_Calls_Btch_Nbr = vw_iwf_Mcss_Calls.getRecord().newFieldInGroup("iwf_Mcss_Calls_Btch_Nbr", "BTCH-NBR", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "BTCH_NBR");
        iwf_Mcss_Calls_Btch_Nbr.setDdmHeader("BATCH/NUM.");
        iwf_Mcss_Calls_Rqst_Type_Cde = vw_iwf_Mcss_Calls.getRecord().newFieldInGroup("iwf_Mcss_Calls_Rqst_Type_Cde", "RQST-TYPE-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RQST_TYPE_CDE");
        iwf_Mcss_Calls_Cstmr_Id = vw_iwf_Mcss_Calls.getRecord().newFieldInGroup("iwf_Mcss_Calls_Cstmr_Id", "CSTMR-ID", FieldType.STRING, 12, RepeatingFieldStrategy.None, 
            "CSTMR_ID");

        iwf_Mcss_Calls__R_Field_1 = vw_iwf_Mcss_Calls.getRecord().newGroupInGroup("iwf_Mcss_Calls__R_Field_1", "REDEFINE", iwf_Mcss_Calls_Cstmr_Id);
        iwf_Mcss_Calls_Iis_Hrchy_Level_Cde = iwf_Mcss_Calls__R_Field_1.newFieldInGroup("iwf_Mcss_Calls_Iis_Hrchy_Level_Cde", "IIS-HRCHY-LEVEL-CDE", FieldType.STRING, 
            1);
        iwf_Mcss_Calls_Ppg_Cde = iwf_Mcss_Calls__R_Field_1.newFieldInGroup("iwf_Mcss_Calls_Ppg_Cde", "PPG-CDE", FieldType.STRING, 6);

        iwf_Mcss_Calls__R_Field_2 = iwf_Mcss_Calls__R_Field_1.newGroupInGroup("iwf_Mcss_Calls__R_Field_2", "REDEFINE", iwf_Mcss_Calls_Ppg_Cde);
        iwf_Mcss_Calls_Iis_Hrchy_Key_Cde = iwf_Mcss_Calls__R_Field_2.newFieldInGroup("iwf_Mcss_Calls_Iis_Hrchy_Key_Cde", "IIS-HRCHY-KEY-CDE", FieldType.NUMERIC, 
            6);

        iwf_Mcss_Calls__R_Field_3 = vw_iwf_Mcss_Calls.getRecord().newGroupInGroup("iwf_Mcss_Calls__R_Field_3", "REDEFINE", iwf_Mcss_Calls_Cstmr_Id);
        iwf_Mcss_Calls_Np_Pin = iwf_Mcss_Calls__R_Field_3.newFieldInGroup("iwf_Mcss_Calls_Np_Pin", "NP-PIN", FieldType.STRING, 12);

        iwf_Mcss_Calls__R_Field_4 = iwf_Mcss_Calls__R_Field_3.newGroupInGroup("iwf_Mcss_Calls__R_Field_4", "REDEFINE", iwf_Mcss_Calls_Np_Pin);
        iwf_Mcss_Calls_Pin = iwf_Mcss_Calls__R_Field_4.newFieldInGroup("iwf_Mcss_Calls_Pin", "PIN", FieldType.NUMERIC, 12);
        iwf_Mcss_Calls_Log_Sqnce_Nbr = vw_iwf_Mcss_Calls.getRecord().newFieldInGroup("iwf_Mcss_Calls_Log_Sqnce_Nbr", "LOG-SQNCE-NBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "LOG_SQNCE_NBR");
        iwf_Mcss_Calls_Psig_Lvl_Cde = vw_iwf_Mcss_Calls.getRecord().newFieldInGroup("iwf_Mcss_Calls_Psig_Lvl_Cde", "PSIG-LVL-CDE", FieldType.STRING, 7, 
            RepeatingFieldStrategy.None, "PSIG_LVL_CDE");

        iwf_Mcss_Calls__R_Field_5 = vw_iwf_Mcss_Calls.getRecord().newGroupInGroup("iwf_Mcss_Calls__R_Field_5", "REDEFINE", iwf_Mcss_Calls_Psig_Lvl_Cde);
        iwf_Mcss_Calls_Psig_Lvl = iwf_Mcss_Calls__R_Field_5.newFieldInGroup("iwf_Mcss_Calls_Psig_Lvl", "PSIG-LVL", FieldType.STRING, 1);
        iwf_Mcss_Calls_Psig_Cde = iwf_Mcss_Calls__R_Field_5.newFieldInGroup("iwf_Mcss_Calls_Psig_Cde", "PSIG-CDE", FieldType.STRING, 6);
        iwf_Mcss_Calls_Rqst_Entry_Op_Cde = vw_iwf_Mcss_Calls.getRecord().newFieldInGroup("iwf_Mcss_Calls_Rqst_Entry_Op_Cde", "RQST-ENTRY-OP-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "RQST_ENTRY_OP_CDE");
        iwf_Mcss_Calls_Rqst_Entry_Op_Cde.setDdmHeader("ORIGIN/RACF/ID");
        iwf_Mcss_Calls_Rqst_Origin_Unit_Cde = vw_iwf_Mcss_Calls.getRecord().newFieldInGroup("iwf_Mcss_Calls_Rqst_Origin_Unit_Cde", "RQST-ORIGIN-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_ORIGIN_UNIT_CDE");
        iwf_Mcss_Calls_Rqst_Origin_Unit_Cde.setDdmHeader("ORIGIN/UNIT");
        iwf_Mcss_Calls_Spcl_Hndlng_Txt = vw_iwf_Mcss_Calls.getRecord().newFieldInGroup("iwf_Mcss_Calls_Spcl_Hndlng_Txt", "SPCL-HNDLNG-TXT", FieldType.STRING, 
            45, RepeatingFieldStrategy.None, "SPCL_HNDLNG_TXT");
        iwf_Mcss_Calls_Spcl_Hndlng_Txt.setDdmHeader("SPECIAL/HANDLING");
        iwf_Mcss_Calls_Attn_Txt = vw_iwf_Mcss_Calls.getRecord().newFieldInGroup("iwf_Mcss_Calls_Attn_Txt", "ATTN-TXT", FieldType.STRING, 25, RepeatingFieldStrategy.None, 
            "ATTN_TXT");
        iwf_Mcss_Calls_Attn_Txt.setDdmHeader("ATTN./TEXT");
        iwf_Mcss_Calls_Rcrd_Type_Cde = vw_iwf_Mcss_Calls.getRecord().newFieldInGroup("iwf_Mcss_Calls_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RCRD_TYPE_CDE");
        iwf_Mcss_Calls_Rcrd_Type_Cde.setDdmHeader("RECORD/TYPE");
        iwf_Mcss_Calls_Work_Prcss_Id = vw_iwf_Mcss_Calls.getRecord().newFieldInGroup("iwf_Mcss_Calls_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        iwf_Mcss_Calls_Error_Msg_Txt = vw_iwf_Mcss_Calls.getRecord().newFieldInGroup("iwf_Mcss_Calls_Error_Msg_Txt", "ERROR-MSG-TXT", FieldType.STRING, 
            60, RepeatingFieldStrategy.None, "ERROR_MSG_TXT");
        iwf_Mcss_Calls_Error_Msg_Txt.setDdmHeader("ERROR/MSG.");

        iwf_Mcss_Calls_Request_Array_Grp = vw_iwf_Mcss_Calls.getRecord().newGroupInGroup("iwf_Mcss_Calls_Request_Array_Grp", "REQUEST-ARRAY-GRP", null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "CWF_MCSS_CALLS_REQUEST_ARRAY_GRP");
        iwf_Mcss_Calls_Dstntn_Unt_Cde = iwf_Mcss_Calls_Request_Array_Grp.newFieldArrayInGroup("iwf_Mcss_Calls_Dstntn_Unt_Cde", "DSTNTN-UNT-CDE", FieldType.STRING, 
            8, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "DSTNTN_UNT_CDE", "CWF_MCSS_CALLS_REQUEST_ARRAY_GRP");
        iwf_Mcss_Calls_Corp_Status_Cde = iwf_Mcss_Calls_Request_Array_Grp.newFieldArrayInGroup("iwf_Mcss_Calls_Corp_Status_Cde", "CORP-STATUS-CDE", FieldType.STRING, 
            1, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CORP_STATUS_CDE", "CWF_MCSS_CALLS_REQUEST_ARRAY_GRP");
        iwf_Mcss_Calls_Unit_Status_Cde = iwf_Mcss_Calls_Request_Array_Grp.newFieldArrayInGroup("iwf_Mcss_Calls_Unit_Status_Cde", "UNIT-STATUS-CDE", FieldType.STRING, 
            4, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "UNIT_STATUS_CDE", "CWF_MCSS_CALLS_REQUEST_ARRAY_GRP");
        iwf_Mcss_Calls_Work_Req_Prty_Cde = iwf_Mcss_Calls_Request_Array_Grp.newFieldArrayInGroup("iwf_Mcss_Calls_Work_Req_Prty_Cde", "WORK-REQ-PRTY-CDE", 
            FieldType.STRING, 1, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "WORK_REQ_PRTY_CDE", "CWF_MCSS_CALLS_REQUEST_ARRAY_GRP");

        iwf_Mcss_Calls_Wfo_All_Othr_Flds = vw_iwf_Mcss_Calls.getRecord().newGroupInGroup("iwf_Mcss_Calls_Wfo_All_Othr_Flds", "WFO-ALL-OTHR-FLDS", null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "CWF_MCSS_CALLS_WFO_ALL_OTHR_FLDS");
        iwf_Mcss_Calls_Wfo_Othr_Flds = iwf_Mcss_Calls_Wfo_All_Othr_Flds.newFieldArrayInGroup("iwf_Mcss_Calls_Wfo_Othr_Flds", "WFO-OTHR-FLDS", FieldType.STRING, 
            100, new DbsArrayController(1, 50) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "WFO_OTHR_FLDS", "CWF_MCSS_CALLS_WFO_ALL_OTHR_FLDS");

        iwf_Mcss_Calls__R_Field_6 = iwf_Mcss_Calls_Wfo_All_Othr_Flds.newGroupInGroup("iwf_Mcss_Calls__R_Field_6", "REDEFINE", iwf_Mcss_Calls_Wfo_Othr_Flds);
        iwf_Mcss_Calls_Default_Ppg_Cde_Ind = iwf_Mcss_Calls__R_Field_6.newFieldInGroup("iwf_Mcss_Calls_Default_Ppg_Cde_Ind", "DEFAULT-PPG-CDE-IND", FieldType.STRING, 
            1);
        iwf_Mcss_Calls_Init_Undrlying_Rqst_Cnt = iwf_Mcss_Calls__R_Field_6.newFieldInGroup("iwf_Mcss_Calls_Init_Undrlying_Rqst_Cnt", "INIT-UNDRLYING-RQST-CNT", 
            FieldType.NUMERIC, 3);
        iwf_Mcss_Calls_Rqst_Orgn_Cde = iwf_Mcss_Calls__R_Field_6.newFieldInGroup("iwf_Mcss_Calls_Rqst_Orgn_Cde", "RQST-ORGN-CDE", FieldType.STRING, 1);
        iwf_Mcss_Calls_Rqst_Log_Dte_Tme = iwf_Mcss_Calls__R_Field_6.newFieldInGroup("iwf_Mcss_Calls_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15);

        iwf_Mcss_Calls__R_Field_7 = iwf_Mcss_Calls__R_Field_6.newGroupInGroup("iwf_Mcss_Calls__R_Field_7", "REDEFINE", iwf_Mcss_Calls_Rqst_Log_Dte_Tme);
        iwf_Mcss_Calls_Rqst_Log_Dte = iwf_Mcss_Calls__R_Field_7.newFieldInGroup("iwf_Mcss_Calls_Rqst_Log_Dte", "RQST-LOG-DTE", FieldType.STRING, 8);
        iwf_Mcss_Calls_Rqst_Log_Tme = iwf_Mcss_Calls__R_Field_7.newFieldInGroup("iwf_Mcss_Calls_Rqst_Log_Tme", "RQST-LOG-TME", FieldType.STRING, 7);
        iwf_Mcss_Calls_Tiaa_Rcvd_Dte_Tme = iwf_Mcss_Calls__R_Field_6.newFieldInGroup("iwf_Mcss_Calls_Tiaa_Rcvd_Dte_Tme", "TIAA-RCVD-DTE-TME", FieldType.STRING, 
            15);

        iwf_Mcss_Calls__R_Field_8 = iwf_Mcss_Calls__R_Field_6.newGroupInGroup("iwf_Mcss_Calls__R_Field_8", "REDEFINE", iwf_Mcss_Calls_Tiaa_Rcvd_Dte_Tme);
        iwf_Mcss_Calls_Tiaa_Rcvd_Dte = iwf_Mcss_Calls__R_Field_8.newFieldInGroup("iwf_Mcss_Calls_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.STRING, 8);
        iwf_Mcss_Calls_Tiaa_Rcvd_Tme = iwf_Mcss_Calls__R_Field_8.newFieldInGroup("iwf_Mcss_Calls_Tiaa_Rcvd_Tme", "TIAA-RCVD-TME", FieldType.STRING, 7);
        iwf_Mcss_Calls_Check_Ind = iwf_Mcss_Calls__R_Field_6.newFieldInGroup("iwf_Mcss_Calls_Check_Ind", "CHECK-IND", FieldType.STRING, 1);
        iwf_Mcss_Calls_Applctns_Rcvd_Ind = iwf_Mcss_Calls__R_Field_6.newFieldInGroup("iwf_Mcss_Calls_Applctns_Rcvd_Ind", "APPLCTNS-RCVD-IND", FieldType.STRING, 
            1);
        iwf_Mcss_Calls_Addtnl_Wpid_Ind = iwf_Mcss_Calls__R_Field_6.newFieldInGroup("iwf_Mcss_Calls_Addtnl_Wpid_Ind", "ADDTNL-WPID-IND", FieldType.STRING, 
            1);
        iwf_Mcss_Calls_Img_Paper_Prcssng_Ind = iwf_Mcss_Calls__R_Field_6.newFieldInGroup("iwf_Mcss_Calls_Img_Paper_Prcssng_Ind", "IMG-PAPER-PRCSSNG-IND", 
            FieldType.STRING, 1);
        iwf_Mcss_Calls_Prvte_Rqst_Ind = iwf_Mcss_Calls__R_Field_6.newFieldInGroup("iwf_Mcss_Calls_Prvte_Rqst_Ind", "PRVTE-RQST-IND", FieldType.STRING, 
            1);
        iwf_Mcss_Calls_Rescan_Ind = iwf_Mcss_Calls__R_Field_6.newFieldInGroup("iwf_Mcss_Calls_Rescan_Ind", "RESCAN-IND", FieldType.STRING, 1);
        iwf_Mcss_Calls_Prefill_Ind = iwf_Mcss_Calls__R_Field_6.newFieldInGroup("iwf_Mcss_Calls_Prefill_Ind", "PREFILL-IND", FieldType.STRING, 1);
        iwf_Mcss_Calls_Intgrtd_Unit_Ind = iwf_Mcss_Calls__R_Field_6.newFieldInGroup("iwf_Mcss_Calls_Intgrtd_Unit_Ind", "INTGRTD-UNIT-IND", FieldType.STRING, 
            1);
        iwf_Mcss_Calls_Print_Q_Ind = iwf_Mcss_Calls__R_Field_6.newFieldInGroup("iwf_Mcss_Calls_Print_Q_Ind", "PRINT-Q-IND", FieldType.NUMERIC, 1);
        iwf_Mcss_Calls_Print_Dte_Tme = iwf_Mcss_Calls__R_Field_6.newFieldInGroup("iwf_Mcss_Calls_Print_Dte_Tme", "PRINT-DTE-TME", FieldType.STRING, 15);
        iwf_Mcss_Calls_Print_Batch_Id_Nbr = iwf_Mcss_Calls__R_Field_6.newFieldInGroup("iwf_Mcss_Calls_Print_Batch_Id_Nbr", "PRINT-BATCH-ID-NBR", FieldType.STRING, 
            9);

        iwf_Mcss_Calls__R_Field_9 = iwf_Mcss_Calls__R_Field_6.newGroupInGroup("iwf_Mcss_Calls__R_Field_9", "REDEFINE", iwf_Mcss_Calls_Print_Batch_Id_Nbr);
        iwf_Mcss_Calls_Print_Prefix_Ind = iwf_Mcss_Calls__R_Field_9.newFieldInGroup("iwf_Mcss_Calls_Print_Prefix_Ind", "PRINT-PREFIX-IND", FieldType.STRING, 
            1);
        iwf_Mcss_Calls_Print_Batch_Nbr = iwf_Mcss_Calls__R_Field_6.newFieldInGroup("iwf_Mcss_Calls_Print_Batch_Nbr", "PRINT-BATCH-NBR", FieldType.NUMERIC, 
            8);
        iwf_Mcss_Calls_Print_Batch_Sqnce_Nbr = iwf_Mcss_Calls__R_Field_6.newFieldInGroup("iwf_Mcss_Calls_Print_Batch_Sqnce_Nbr", "PRINT-BATCH-SQNCE-NBR", 
            FieldType.NUMERIC, 3);
        iwf_Mcss_Calls_Print_Batch_Ind = iwf_Mcss_Calls__R_Field_6.newFieldInGroup("iwf_Mcss_Calls_Print_Batch_Ind", "PRINT-BATCH-IND", FieldType.STRING, 
            1);
        iwf_Mcss_Calls_Printer_Id_Cde = iwf_Mcss_Calls__R_Field_6.newFieldInGroup("iwf_Mcss_Calls_Printer_Id_Cde", "PRINTER-ID-CDE", FieldType.STRING, 
            4);
        iwf_Mcss_Calls_Mstr_Indx_Actn_Cde = iwf_Mcss_Calls__R_Field_6.newFieldInGroup("iwf_Mcss_Calls_Mstr_Indx_Actn_Cde", "MSTR-INDX-ACTN-CDE", FieldType.STRING, 
            2);
        iwf_Mcss_Calls_Cases_Mismatch = iwf_Mcss_Calls__R_Field_6.newFieldInGroup("iwf_Mcss_Calls_Cases_Mismatch", "CASES-MISMATCH", FieldType.STRING, 
            1);

        iwf_Mcss_Calls_Dcmnt_Array = iwf_Mcss_Calls__R_Field_6.newGroupArrayInGroup("iwf_Mcss_Calls_Dcmnt_Array", "DCMNT-ARRAY", new DbsArrayController(1, 
            15));
        iwf_Mcss_Calls_Dcmnt_Ctgry = iwf_Mcss_Calls_Dcmnt_Array.newFieldInGroup("iwf_Mcss_Calls_Dcmnt_Ctgry", "DCMNT-CTGRY", FieldType.STRING, 1);
        iwf_Mcss_Calls_Dcmnt_Sub_Ctgry = iwf_Mcss_Calls_Dcmnt_Array.newFieldInGroup("iwf_Mcss_Calls_Dcmnt_Sub_Ctgry", "DCMNT-SUB-CTGRY", FieldType.STRING, 
            3);
        iwf_Mcss_Calls_Dcmnt_Dtl = iwf_Mcss_Calls_Dcmnt_Array.newFieldInGroup("iwf_Mcss_Calls_Dcmnt_Dtl", "DCMNT-DTL", FieldType.STRING, 4);
        iwf_Mcss_Calls_Dcmnt_Frmt = iwf_Mcss_Calls_Dcmnt_Array.newFieldInGroup("iwf_Mcss_Calls_Dcmnt_Frmt", "DCMNT-FRMT", FieldType.STRING, 3);
        iwf_Mcss_Calls_Dcmnt_Drctn = iwf_Mcss_Calls_Dcmnt_Array.newFieldInGroup("iwf_Mcss_Calls_Dcmnt_Drctn", "DCMNT-DRCTN", FieldType.STRING, 1);
        iwf_Mcss_Calls_Dcmnt_Rtntn = iwf_Mcss_Calls_Dcmnt_Array.newFieldInGroup("iwf_Mcss_Calls_Dcmnt_Rtntn", "DCMNT-RTNTN", FieldType.STRING, 1);
        iwf_Mcss_Calls_Dcmnt_Scrty_Ind = iwf_Mcss_Calls_Dcmnt_Array.newFieldInGroup("iwf_Mcss_Calls_Dcmnt_Scrty_Ind", "DCMNT-SCRTY-IND", FieldType.STRING, 
            1);
        iwf_Mcss_Calls_Dcmnt_Anttn_Ind = iwf_Mcss_Calls_Dcmnt_Array.newFieldInGroup("iwf_Mcss_Calls_Dcmnt_Anttn_Ind", "DCMNT-ANTTN-IND", FieldType.STRING, 
            1);
        iwf_Mcss_Calls_Dcmnt_Copy_Ind = iwf_Mcss_Calls_Dcmnt_Array.newFieldInGroup("iwf_Mcss_Calls_Dcmnt_Copy_Ind", "DCMNT-COPY-IND", FieldType.STRING, 
            1);
        iwf_Mcss_Calls_Dcmnt_Rcvd_Dte = iwf_Mcss_Calls_Dcmnt_Array.newFieldInGroup("iwf_Mcss_Calls_Dcmnt_Rcvd_Dte", "DCMNT-RCVD-DTE", FieldType.NUMERIC, 
            8);
        iwf_Mcss_Calls_Prvte_Dcmnt_Ind = iwf_Mcss_Calls_Dcmnt_Array.newFieldInGroup("iwf_Mcss_Calls_Prvte_Dcmnt_Ind", "PRVTE-DCMNT-IND", FieldType.STRING, 
            1);
        iwf_Mcss_Calls_Mail_Item_Nbr = iwf_Mcss_Calls_Dcmnt_Array.newFieldInGroup("iwf_Mcss_Calls_Mail_Item_Nbr", "MAIL-ITEM-NBR", FieldType.NUMERIC, 
            11);
        iwf_Mcss_Calls_Start_Page_Nbr = iwf_Mcss_Calls_Dcmnt_Array.newFieldInGroup("iwf_Mcss_Calls_Start_Page_Nbr", "START-PAGE-NBR", FieldType.NUMERIC, 
            3);
        iwf_Mcss_Calls_Image_End_Page = iwf_Mcss_Calls_Dcmnt_Array.newFieldInGroup("iwf_Mcss_Calls_Image_End_Page", "IMAGE-END-PAGE", FieldType.NUMERIC, 
            3);
        registerRecord(vw_iwf_Mcss_Calls);

        pnd_Tot_Del = localVariables.newFieldInRecord("pnd_Tot_Del", "#TOT-DEL", FieldType.PACKED_DECIMAL, 8);
        pnd_Tot_Read = localVariables.newFieldInRecord("pnd_Tot_Read", "#TOT-READ", FieldType.PACKED_DECIMAL, 3);
        pnd_Input_To_D = localVariables.newFieldInRecord("pnd_Input_To_D", "#INPUT-TO-D", FieldType.DATE);
        pnd_Cnnctd_Purg_Key = localVariables.newFieldInRecord("pnd_Cnnctd_Purg_Key", "#CNNCTD-PURG-KEY", FieldType.STRING, 21);

        pnd_Cnnctd_Purg_Key__R_Field_10 = localVariables.newGroupInRecord("pnd_Cnnctd_Purg_Key__R_Field_10", "REDEFINE", pnd_Cnnctd_Purg_Key);
        pnd_Cnnctd_Purg_Key_Pnd_Start_Upld_Tme = pnd_Cnnctd_Purg_Key__R_Field_10.newFieldInGroup("pnd_Cnnctd_Purg_Key_Pnd_Start_Upld_Tme", "#START-UPLD-TME", 
            FieldType.TIME);
        pnd_Cnnctd_Purg_Key_Pnd_Source_Id = pnd_Cnnctd_Purg_Key__R_Field_10.newFieldInGroup("pnd_Cnnctd_Purg_Key_Pnd_Source_Id", "#SOURCE-ID", FieldType.STRING, 
            6);
        pnd_Cnnctd_Purg_Key_Pnd_Btch_Nbr = pnd_Cnnctd_Purg_Key__R_Field_10.newFieldInGroup("pnd_Cnnctd_Purg_Key_Pnd_Btch_Nbr", "#BTCH-NBR", FieldType.NUMERIC, 
            8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iwf_Mcss_Calls.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iwfp1600() throws Exception
    {
        super("Iwfp1600");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 58 ZP = OFF
        pnd_Cnnctd_Purg_Key_Pnd_Start_Upld_Tme.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),"199001010000001");                                                   //Natural: MOVE EDITED '199001010000001' TO #START-UPLD-TME ( EM = YYYYMMDDHHIISST )
        //*  APPROX 3 MONTHS
        pnd_Input_To_D.setValue(Global.getDATX());                                                                                                                        //Natural: MOVE *DATX TO #INPUT-TO-D
        pnd_Input_To_D.nsubtract(91);                                                                                                                                     //Natural: ASSIGN #INPUT-TO-D := #INPUT-TO-D - 91
        vw_iwf_Mcss_Calls.startDatabaseRead                                                                                                                               //Natural: READ IWF-MCSS-CALLS BY CNNCTD-PURG-KEY #CNNCTD-PURG-KEY
        (
        "RD",
        new Wc[] { new Wc("CNNCTD_PURG_KEY", ">=", pnd_Cnnctd_Purg_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("CNNCTD_PURG_KEY", "ASC") }
        );
        RD:
        while (condition(vw_iwf_Mcss_Calls.readNextRow("RD")))
        {
            if (condition(iwf_Mcss_Calls_Rcrd_Upld_Tme.greaterOrEqual(pnd_Input_To_D)))                                                                                   //Natural: IF IWF-MCSS-CALLS.RCRD-UPLD-TME GE #INPUT-TO-D
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  PIN-EXP<<
            if (condition(!(iwf_Mcss_Calls_Mstr_Indx_Actn_Cde.equals("20") || iwf_Mcss_Calls_Mstr_Indx_Actn_Cde.equals("30"))))                                           //Natural: ACCEPT IF IWF-MCSS-CALLS.MSTR-INDX-ACTN-CDE = '20' OR = '30'
            {
                continue;
            }
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),iwf_Mcss_Calls_Rcrd_Upld_Tme, new ReportEditMask ("YYYYMMDDHHIISST"),new TabSetting(18),iwf_Mcss_Calls_Source_Id,new  //Natural: WRITE ( 1 ) 01T IWF-MCSS-CALLS.RCRD-UPLD-TME ( EM = YYYYMMDDHHIISST ) 18T IWF-MCSS-CALLS.SOURCE-ID 25T IWF-MCSS-CALLS.BTCH-NBR 36T IWF-MCSS-CALLS.PSIG-LVL-CDE 48T IWF-MCSS-CALLS.RQST-TYPE-CDE 54T IWF-MCSS-CALLS.CSTMR-ID 69T IWF-MCSS-CALLS.LOG-SQNCE-NBR 76T IWF-MCSS-CALLS.RQST-ENTRY-OP-CDE
                TabSetting(25),iwf_Mcss_Calls_Btch_Nbr,new TabSetting(36),iwf_Mcss_Calls_Psig_Lvl_Cde,new TabSetting(48),iwf_Mcss_Calls_Rqst_Type_Cde,new 
                TabSetting(54),iwf_Mcss_Calls_Cstmr_Id,new TabSetting(69),iwf_Mcss_Calls_Log_Sqnce_Nbr,new TabSetting(76),iwf_Mcss_Calls_Rqst_Entry_Op_Cde);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*    64T IWF-MCSS-CALLS.LOG-SQNCE-NBR
            //*    71T IWF-MCSS-CALLS.RQST-ENTRY-OP-CDE
            //*  EPPEL
            G1:                                                                                                                                                           //Natural: GET IWF-MCSS-CALLS *ISN ( RD. )
            vw_iwf_Mcss_Calls.readByID(vw_iwf_Mcss_Calls.getAstISN("RD"), "G1");
            vw_iwf_Mcss_Calls.deleteDBRow("G1");                                                                                                                          //Natural: DELETE ( G1. )
            //*     DELETE (RD.)
            //*  EPPEL
            pnd_Tot_Del.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #TOT-DEL
            pnd_Tot_Read.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #TOT-READ
            if (condition(pnd_Tot_Read.greater(100)))                                                                                                                     //Natural: IF #TOT-READ GT 100
            {
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                pnd_Tot_Read.reset();                                                                                                                                     //Natural: RESET #TOT-READ
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"    NUMBER OF RECORDS DELETED : ",pnd_Tot_Del, new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,new          //Natural: WRITE ( 1 ) / '    NUMBER OF RECORDS DELETED : ' #TOT-DEL ( EM = ZZ,ZZZ,ZZ9 ) // 30T '*** end of report ***'
            TabSetting(30),"*** end of report ***");
        if (Global.isEscape()) return;
        //*          // 16T '** UPLOAD CONTROL RECORDS DELETED SUCCESSFULLY **'
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //*  WRITE #RECORD
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(69),"Page:",getReports().getPageNumberDbs(1), //Natural: WRITE ( 1 ) NOTITLE / 01T *PROGRAM 69T 'Page:' *PAGE-NUMBER ( 1 ) ( NL = 4 AD = L SG = OFF ) // 22T 'Integrated Corporate Workflow Facility' / 17T 'Audit Records Deleted on' 42T *DATX ( EM = LLL' 'DD','YYYY ) 54T *TIMX ( EM = HH':'II':'SS' 'AP ) // 02T 'RECORD UPLOAD   SOURCE   BATCH     LEVEL    RQST' 54T 'CUSTOMER  SQNCE  LOG OPRTR' / 02T ' TIME STAMP       ID      NBR      CODE    TYP CDE' 54T '   ID     NBR      CODE' / 01T '-' ( 15 ) 18T '-' ( 6 ) 26T '-' ( 8 ) 36T '-' ( 7 ) 45T '-' ( 7 ) 54T '-' ( 8 ) 64T '-' ( 5 ) 71T '-' ( 9 )
                        new NumericLength (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,NEWLINE,new TabSetting(22),"Integrated Corporate Workflow Facility",NEWLINE,new 
                        TabSetting(17),"Audit Records Deleted on",new TabSetting(42),Global.getDATX(), new ReportEditMask ("LLL' 'DD','YYYY"),new TabSetting(54),Global.getTIMX(), 
                        new ReportEditMask ("HH':'II':'SS' 'AP"),NEWLINE,NEWLINE,new TabSetting(2),"RECORD UPLOAD   SOURCE   BATCH     LEVEL    RQST",new 
                        TabSetting(54),"CUSTOMER  SQNCE  LOG OPRTR",NEWLINE,new TabSetting(2)," TIME STAMP       ID      NBR      CODE    TYP CDE",new TabSetting(54),"   ID     NBR      CODE",NEWLINE,new 
                        TabSetting(1),"-",new RepeatItem(15),new TabSetting(18),"-",new RepeatItem(6),new TabSetting(26),"-",new RepeatItem(8),new TabSetting(36),"-",new 
                        RepeatItem(7),new TabSetting(45),"-",new RepeatItem(7),new TabSetting(54),"-",new RepeatItem(8),new TabSetting(64),"-",new RepeatItem(5),new 
                        TabSetting(71),"-",new RepeatItem(9));
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=58 ZP=OFF");
    }
}
