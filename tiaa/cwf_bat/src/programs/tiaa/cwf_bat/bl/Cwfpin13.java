/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:01:02 PM
**        * FROM NATURAL PROGRAM : Cwfpin13
************************************************************
**        * FILE NAME            : Cwfpin13.java
**        * CLASS NAME           : Cwfpin13
**        * INSTANCE NAME        : Cwfpin13
************************************************************
* PROGRAM  : CWFPIN13
* SYSTEM   : CWF
* TITLE    : UPDATE PIN....
* GENERATED: JUN 12,17
* FUNCTION : ALPHANUMERIC PIN UPDATE WITH '00000' AS PREFIX
*
* HISTORY
*
* 12/06/17  ADDED AS ONE TIME UPDATE PROGRAM FOR 12 DIGIT PIN
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfpin13 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Sb;
    private DbsField cwf_Sb_Srvvr_Mit_Idntfr;

    private DbsGroup cwf_Sb__R_Field_1;
    private DbsField cwf_Sb_Srvvr_Mit_Idntfr_Pin;
    private DbsField cwf_Sb_Srvvr_Mit_Idntfr_Ind;
    private DbsField cwf_Sb_Srvvr_Mit_Idntfr_F;
    private DbsField target_Field;

    private DbsGroup target_Field__R_Field_2;
    private DbsField target_Field_New_Srvvr_Mit_Idntfr_F;
    private DbsField target_Field_New_Srvvr_Mit_Idntfr_Pin;
    private DbsField target_Field_New_Srvvr_Mit_Idntfr_Ind;
    private DbsField pnd_Read_Cnt;
    private DbsField pnd_Trans_Cnt;
    private DbsField pnd_Update_Cnt;
    private DbsField pnd_Disp_Cnt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Sb = new DataAccessProgramView(new NameInfo("vw_cwf_Sb", "CWF-SB"), "CWF_SB_WORK_REQ", "CWF_SB_WORK_REQ");
        cwf_Sb_Srvvr_Mit_Idntfr = vw_cwf_Sb.getRecord().newFieldInGroup("cwf_Sb_Srvvr_Mit_Idntfr", "SRVVR-MIT-IDNTFR", FieldType.STRING, 14, RepeatingFieldStrategy.None, 
            "SRVVR_MIT_IDNTFR");
        cwf_Sb_Srvvr_Mit_Idntfr.setDdmHeader("SURVIVOR MIT/IDENTIFIER");

        cwf_Sb__R_Field_1 = vw_cwf_Sb.getRecord().newGroupInGroup("cwf_Sb__R_Field_1", "REDEFINE", cwf_Sb_Srvvr_Mit_Idntfr);
        cwf_Sb_Srvvr_Mit_Idntfr_Pin = cwf_Sb__R_Field_1.newFieldInGroup("cwf_Sb_Srvvr_Mit_Idntfr_Pin", "SRVVR-MIT-IDNTFR-PIN", FieldType.STRING, 7);
        cwf_Sb_Srvvr_Mit_Idntfr_Ind = cwf_Sb__R_Field_1.newFieldInGroup("cwf_Sb_Srvvr_Mit_Idntfr_Ind", "SRVVR-MIT-IDNTFR-IND", FieldType.STRING, 2);
        cwf_Sb_Srvvr_Mit_Idntfr_F = cwf_Sb__R_Field_1.newFieldInGroup("cwf_Sb_Srvvr_Mit_Idntfr_F", "SRVVR-MIT-IDNTFR-F", FieldType.STRING, 5);
        registerRecord(vw_cwf_Sb);

        target_Field = localVariables.newFieldInRecord("target_Field", "TARGET-FIELD", FieldType.STRING, 14);

        target_Field__R_Field_2 = localVariables.newGroupInRecord("target_Field__R_Field_2", "REDEFINE", target_Field);
        target_Field_New_Srvvr_Mit_Idntfr_F = target_Field__R_Field_2.newFieldInGroup("target_Field_New_Srvvr_Mit_Idntfr_F", "NEW-SRVVR-MIT-IDNTFR-F", 
            FieldType.STRING, 5);
        target_Field_New_Srvvr_Mit_Idntfr_Pin = target_Field__R_Field_2.newFieldInGroup("target_Field_New_Srvvr_Mit_Idntfr_Pin", "NEW-SRVVR-MIT-IDNTFR-PIN", 
            FieldType.STRING, 7);
        target_Field_New_Srvvr_Mit_Idntfr_Ind = target_Field__R_Field_2.newFieldInGroup("target_Field_New_Srvvr_Mit_Idntfr_Ind", "NEW-SRVVR-MIT-IDNTFR-IND", 
            FieldType.STRING, 2);
        pnd_Read_Cnt = localVariables.newFieldInRecord("pnd_Read_Cnt", "#READ-CNT", FieldType.NUMERIC, 9);
        pnd_Trans_Cnt = localVariables.newFieldInRecord("pnd_Trans_Cnt", "#TRANS-CNT", FieldType.NUMERIC, 9);
        pnd_Update_Cnt = localVariables.newFieldInRecord("pnd_Update_Cnt", "#UPDATE-CNT", FieldType.NUMERIC, 9);
        pnd_Disp_Cnt = localVariables.newFieldInRecord("pnd_Disp_Cnt", "#DISP-CNT", FieldType.NUMERIC, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Sb.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfpin13() throws Exception
    {
        super("Cwfpin13");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        vw_cwf_Sb.startDatabaseRead                                                                                                                                       //Natural: READ CWF-SB IN PHYSICAL SEQUENCE
        (
        "READ_FILE",
        new Oc[] { new Oc("ISN", "ASC") }
        );
        READ_FILE:
        while (condition(vw_cwf_Sb.readNextRow("READ_FILE")))
        {
            pnd_Read_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #READ-CNT
            if (condition(cwf_Sb_Srvvr_Mit_Idntfr.notEquals(" ")))                                                                                                        //Natural: IF SRVVR-MIT-IDNTFR NE ' '
            {
                if (condition(cwf_Sb_Srvvr_Mit_Idntfr_F.equals(" ")))                                                                                                     //Natural: IF SRVVR-MIT-IDNTFR-F EQ ' '
                {
                    if (condition(DbsUtil.maskMatches(cwf_Sb_Srvvr_Mit_Idntfr_Pin,"1000000:9999999")))                                                                    //Natural: IF SRVVR-MIT-IDNTFR-PIN EQ MASK ( 1000000:9999999 )
                    {
                        target_Field_New_Srvvr_Mit_Idntfr_F.setValue("00000");                                                                                            //Natural: MOVE '00000' TO NEW-SRVVR-MIT-IDNTFR-F
                        target_Field_New_Srvvr_Mit_Idntfr_Pin.setValue(cwf_Sb_Srvvr_Mit_Idntfr_Pin);                                                                      //Natural: MOVE SRVVR-MIT-IDNTFR-PIN TO NEW-SRVVR-MIT-IDNTFR-PIN
                        target_Field_New_Srvvr_Mit_Idntfr_Ind.setValue(cwf_Sb_Srvvr_Mit_Idntfr_Ind);                                                                      //Natural: MOVE SRVVR-MIT-IDNTFR-IND TO NEW-SRVVR-MIT-IDNTFR-IND
                        G1:                                                                                                                                               //Natural: GET CWF-SB *ISN ( READ-FILE. )
                        vw_cwf_Sb.readByID(vw_cwf_Sb.getAstISN("READ_FILE"), "G1");
                        cwf_Sb_Srvvr_Mit_Idntfr.moveAll(target_Field);                                                                                                    //Natural: MOVE ALL TARGET-FIELD TO SRVVR-MIT-IDNTFR
                        pnd_Update_Cnt.nadd(1);                                                                                                                           //Natural: ADD 1 TO #UPDATE-CNT
                        pnd_Disp_Cnt.nadd(1);                                                                                                                             //Natural: ADD 1 TO #DISP-CNT
                        pnd_Trans_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #TRANS-CNT
                        vw_cwf_Sb.updateDBRow("G1");                                                                                                                      //Natural: UPDATE ( G1. )
                        if (condition(pnd_Trans_Cnt.greater(5000)))                                                                                                       //Natural: IF #TRANS-CNT > 5000
                        {
                            getCurrentProcessState().getDbConv().dbCommit();                                                                                              //Natural: END TRANSACTION
                            pnd_Trans_Cnt.reset();                                                                                                                        //Natural: RESET #TRANS-CNT
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pnd_Disp_Cnt.greater(100000)))                                                                                                      //Natural: IF #DISP-CNT > 100000
                        {
                            getReports().write(0, " No of records updated so far:",pnd_Update_Cnt);                                                                       //Natural: WRITE ' No of records updated so far:' #UPDATE-CNT
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("READ_FILE"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("READ_FILE"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            pnd_Disp_Cnt.reset();                                                                                                                         //Natural: RESET #DISP-CNT
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        getReports().write(0, "Total Read count:",pnd_Read_Cnt);                                                                                                          //Natural: WRITE 'Total Read count:' #READ-CNT
        if (Global.isEscape()) return;
        getReports().write(0, "Updated Record count ",pnd_Update_Cnt);                                                                                                    //Natural: WRITE 'Updated Record count ' #UPDATE-CNT
        if (Global.isEscape()) return;
    }

    //
}
