/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:46:26 PM
**        * FROM NATURAL PROGRAM : Cwfb8635
************************************************************
**        * FILE NAME            : Cwfb8635.java
**        * CLASS NAME           : Cwfb8635
**        * INSTANCE NAME        : Cwfb8635
************************************************************
************************************************************************
* SYSTEM   : CORPORATE WORKFLOW
* PROGRAM  : CWFB8635
* TITLE    : WPID / PI TASK TYPE TRANSLATION WEEKLY REPORT
* FUNCTION : READ SORTED WORKFILE CREATED FROM CWF-WP-WORK-PRCSS-ID
*          : (045/182) TO CREATE WEEKLY WPID / PI TASK TYPE REPORT
* HISTORY  : CREATED ON JULY 26 2012 - VINODHKUMAR RAMACHANDRAN
************************************************************************
*
* A HEADER IS INSERTED TO THE SORTED WORK FILE (FROM CWFB8634)
* AT THE TOP AND FEED INTO THE MOBIUS REPORT - CW4501W1
*
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb8635 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Workfile1;
    private DbsField pnd_Workfile2;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Workfile1 = localVariables.newFieldInRecord("pnd_Workfile1", "#WORKFILE1", FieldType.STRING, 80);
        pnd_Workfile2 = localVariables.newFieldInRecord("pnd_Workfile2", "#WORKFILE2", FieldType.STRING, 80);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb8635() throws Exception
    {
        super("Cwfb8635");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        pnd_Workfile1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, " WPID ,              WPID DESCRIPTION               ,", "PI TASK TYPE,  PI IND  "));      //Natural: COMPRESS ' WPID ,              WPID DESCRIPTION               ,' 'PI TASK TYPE,  PI IND  ' INTO #WORKFILE1 LEAVING NO SPACE
        getWorkFiles().write(2, false, pnd_Workfile1);                                                                                                                    //Natural: WRITE WORK FILE 2 #WORKFILE1
        pnd_Workfile1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "------,---------------------------------------------,", "------------,----------"));      //Natural: COMPRESS '------,---------------------------------------------,' '------------,----------' INTO #WORKFILE1 LEAVING NO SPACE
        getWorkFiles().write(2, false, pnd_Workfile1);                                                                                                                    //Natural: WRITE WORK FILE 2 #WORKFILE1
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #WORKFILE1
        while (condition(getWorkFiles().read(1, pnd_Workfile1)))
        {
            getWorkFiles().write(2, false, pnd_Workfile1);                                                                                                                //Natural: WRITE WORK FILE 2 #WORKFILE1
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
    }

    //
}
