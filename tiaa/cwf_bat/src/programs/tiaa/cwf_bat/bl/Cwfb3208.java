/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:28:47 PM
**        * FROM NATURAL PROGRAM : Cwfb3208
************************************************************
**        * FILE NAME            : Cwfb3208.java
**        * CLASS NAME           : Cwfb3208
**        * INSTANCE NAME        : Cwfb3208
************************************************************
**SAG GENERATOR: BATCH-TIAA                       VERSION: 3.2.2
**SAG TITLE: SRA TXNS FROM SRA REQUEST
**SAG SYSTEM: CRPCWF
**SAG REPORT-HEADING(1): CORPORATE WORKFLOW FACILITIES
**SAG PRINT-FILE(1): 1
**SAG REPORT-HEADING(2): NO TRANSACTIONS RECEIVED
**SAG PRINT-FILE(2): 1
**SAG HEADING-LINE: 12000000
**SAG DESCS(1): THIS PROGRAM READS THE WORK FILE CREATED BY CWFB3207
**SAG DESCS(2): WHICH HAS SRA FORMS WITH NO TXNS RECIEVED.  IT SORTS
**SAG DESCS(3): BY UNIT CODE AND TIAA REC'd date and writes a report.
**SAG PRIMARY-FILE: FORMS-WITHOUT-TXNS
************************************************************************
* PROGRAM  : CWFB3208
* SYSTEM   : CRPCWF
* TITLE    : SRA TXNS FROM SRA REQUEST
* GENERATED: SEP 27,95 AT 03:36 PM
* FUNCTION : THIS PROGRAM READS THE WORK FILE CREATED BY CWFB3207
*            WHICH HAS SRA FORMS WITH NO TXNS RECIEVED.  IT SORTS
*            BY UNIT CODE AND TIAA REC'd date and writes a report.
*
* HISTORY
**SAG DEFINE EXIT CHANGE-HISTORY
* CHANGED ON SEP 26,95 BY HARGRAV FOR RELEASE ____
* >
* >
* >
**SAG END-EXIT
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3208 extends BLNatBase
{
    // Data Areas
    private LdaCdbatxa ldaCdbatxa;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Cur_Lang;
    private DbsField pnd_Program;
    private DbsField pnd_Prime_Counter;

    private DbsGroup forms_Without_Txns;
    private DbsField forms_Without_Txns_Pin_Nbr;
    private DbsField forms_Without_Txns_Work_Prcss_Id;
    private DbsField forms_Without_Txns_Tiaa_Rcvd_Dte;
    private DbsField forms_Without_Txns_Name;
    private DbsField forms_Without_Txns_Ssn;
    private DbsField forms_Without_Txns_Orgnl_Unit_Cde;
    private DbsField pnd_Header1_1;
    private DbsField pnd_Header1_2;

    private DbsGroup pnd_Counters;
    private DbsField pnd_Counters_Pnd_Rex_Read;
    private DbsField pnd_Counters_Pnd_Sra_Forms_Without;
    private DbsField pnd_Counters_Pnd_Tot_Unit;
    private DbsField pnd_Counters_Pnd_Wpid_Unit_Ctr;
    private DbsField pnd_Counters_Pnd_Wpid_Tot_Ctr;
    private DbsField pnd_Wpid_Tbl;
    private DbsField unit_Tot_Line_Tbl;
    private DbsField grand_Tot_Line_Tbl;
    private DbsField pnd_I;

    private DbsGroup pnd_Report;
    private DbsField pnd_Report_Pnd_Unit_Cde;

    private DbsGroup pnd_Misc_Data;
    private DbsField pnd_Misc_Data_Pnd_Start_Date;

    private DbsGroup pnd_Misc_Data__R_Field_1;
    private DbsField pnd_Misc_Data_Pnd_Start_Date_N;
    private DbsField pnd_Misc_Data_Pnd_End_Date;

    private DbsGroup pnd_Misc_Data__R_Field_2;
    private DbsField pnd_Misc_Data_Pnd_End_Date_N;
    private DbsField pnd_Misc_Data_Pnd_Work_Start_Date_A;

    private DbsGroup pnd_Misc_Data__R_Field_3;
    private DbsField pnd_Misc_Data_Pnd_Work_Mm;
    private DbsField pnd_Misc_Data_Pnd_Filler1;
    private DbsField pnd_Misc_Data_Pnd_Work_Dd;
    private DbsField pnd_Misc_Data_Pnd_Filler2;
    private DbsField pnd_Misc_Data_Pnd_Work_Yy;
    private DbsField pnd_Misc_Data_Pnd_Work_End_Date_A;

    private DbsGroup pnd_Misc_Data__R_Field_4;
    private DbsField pnd_Misc_Data_Pnd_Work_Mmm;
    private DbsField pnd_Misc_Data_Pnd_Fillera;
    private DbsField pnd_Misc_Data_Pnd_Work_Ddd;
    private DbsField pnd_Misc_Data_Pnd_Fillerb;
    private DbsField pnd_Misc_Data_Pnd_Work_Yyy;
    private DbsField pnd_Report_End_Date;
    private DbsField pnd_Report_Today;
    private DbsField pnd_Tiaa_Comp_Date_Start;
    private DbsField pnd_Tiaa_Comp_Date_End;
    private DbsField pnd_Tiaa_Recvd_Date;
    private DbsField pnd_Work_Date;
    private DbsField pnd_Comp_Date;
    private DbsField pnd_Clock_Dte_Tme;

    private DbsGroup pnd_Clock_Dte_Tme__R_Field_5;
    private DbsField pnd_Clock_Dte_Tme_Pnd_Clock_Check_Date;
    private DbsField pnd_Parm_Type;
    private DbsField pnd_Today;

    private DbsGroup pnd_Today__R_Field_6;
    private DbsField pnd_Today_Pnd_Today_N;

    private DbsRecord internalLoopRecord;
    private DbsField sort01Orgnl_Unit_CdeOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCdbatxa = new LdaCdbatxa();
        registerRecord(ldaCdbatxa);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Cur_Lang = localVariables.newFieldInRecord("pnd_Cur_Lang", "#CUR-LANG", FieldType.PACKED_DECIMAL, 1);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Prime_Counter = localVariables.newFieldInRecord("pnd_Prime_Counter", "#PRIME-COUNTER", FieldType.PACKED_DECIMAL, 7);

        forms_Without_Txns = localVariables.newGroupInRecord("forms_Without_Txns", "FORMS-WITHOUT-TXNS");
        forms_Without_Txns_Pin_Nbr = forms_Without_Txns.newFieldInGroup("forms_Without_Txns_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12);
        forms_Without_Txns_Work_Prcss_Id = forms_Without_Txns.newFieldInGroup("forms_Without_Txns_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6);
        forms_Without_Txns_Tiaa_Rcvd_Dte = forms_Without_Txns.newFieldInGroup("forms_Without_Txns_Tiaa_Rcvd_Dte", "TIAA-RCVD-DTE", FieldType.STRING, 8);
        forms_Without_Txns_Name = forms_Without_Txns.newFieldInGroup("forms_Without_Txns_Name", "NAME", FieldType.STRING, 40);
        forms_Without_Txns_Ssn = forms_Without_Txns.newFieldInGroup("forms_Without_Txns_Ssn", "SSN", FieldType.NUMERIC, 9);
        forms_Without_Txns_Orgnl_Unit_Cde = forms_Without_Txns.newFieldInGroup("forms_Without_Txns_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", FieldType.STRING, 
            8);
        pnd_Header1_1 = localVariables.newFieldInRecord("pnd_Header1_1", "#HEADER1-1", FieldType.STRING, 50);
        pnd_Header1_2 = localVariables.newFieldInRecord("pnd_Header1_2", "#HEADER1-2", FieldType.STRING, 50);

        pnd_Counters = localVariables.newGroupInRecord("pnd_Counters", "#COUNTERS");
        pnd_Counters_Pnd_Rex_Read = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Rex_Read", "#REX-READ", FieldType.PACKED_DECIMAL, 7);
        pnd_Counters_Pnd_Sra_Forms_Without = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Sra_Forms_Without", "#SRA-FORMS-WITHOUT", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Counters_Pnd_Tot_Unit = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Tot_Unit", "#TOT-UNIT", FieldType.PACKED_DECIMAL, 7);
        pnd_Counters_Pnd_Wpid_Unit_Ctr = pnd_Counters.newFieldArrayInGroup("pnd_Counters_Pnd_Wpid_Unit_Ctr", "#WPID-UNIT-CTR", FieldType.PACKED_DECIMAL, 
            7, new DbsArrayController(1, 6));
        pnd_Counters_Pnd_Wpid_Tot_Ctr = pnd_Counters.newFieldArrayInGroup("pnd_Counters_Pnd_Wpid_Tot_Ctr", "#WPID-TOT-CTR", FieldType.PACKED_DECIMAL, 7, 
            new DbsArrayController(1, 6));
        pnd_Wpid_Tbl = localVariables.newFieldArrayInRecord("pnd_Wpid_Tbl", "#WPID-TBL", FieldType.STRING, 6, new DbsArrayController(1, 4));
        unit_Tot_Line_Tbl = localVariables.newFieldArrayInRecord("unit_Tot_Line_Tbl", "UNIT-TOT-LINE-TBL", FieldType.STRING, 42, new DbsArrayController(1, 
            4));
        grand_Tot_Line_Tbl = localVariables.newFieldArrayInRecord("grand_Tot_Line_Tbl", "GRAND-TOT-LINE-TBL", FieldType.STRING, 42, new DbsArrayController(1, 
            4));
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);

        pnd_Report = localVariables.newGroupInRecord("pnd_Report", "#REPORT");
        pnd_Report_Pnd_Unit_Cde = pnd_Report.newFieldInGroup("pnd_Report_Pnd_Unit_Cde", "#UNIT-CDE", FieldType.STRING, 8);

        pnd_Misc_Data = localVariables.newGroupInRecord("pnd_Misc_Data", "#MISC-DATA");
        pnd_Misc_Data_Pnd_Start_Date = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Start_Date", "#START-DATE", FieldType.STRING, 8);

        pnd_Misc_Data__R_Field_1 = pnd_Misc_Data.newGroupInGroup("pnd_Misc_Data__R_Field_1", "REDEFINE", pnd_Misc_Data_Pnd_Start_Date);
        pnd_Misc_Data_Pnd_Start_Date_N = pnd_Misc_Data__R_Field_1.newFieldInGroup("pnd_Misc_Data_Pnd_Start_Date_N", "#START-DATE-N", FieldType.NUMERIC, 
            8);
        pnd_Misc_Data_Pnd_End_Date = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_End_Date", "#END-DATE", FieldType.STRING, 8);

        pnd_Misc_Data__R_Field_2 = pnd_Misc_Data.newGroupInGroup("pnd_Misc_Data__R_Field_2", "REDEFINE", pnd_Misc_Data_Pnd_End_Date);
        pnd_Misc_Data_Pnd_End_Date_N = pnd_Misc_Data__R_Field_2.newFieldInGroup("pnd_Misc_Data_Pnd_End_Date_N", "#END-DATE-N", FieldType.NUMERIC, 8);
        pnd_Misc_Data_Pnd_Work_Start_Date_A = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Work_Start_Date_A", "#WORK-START-DATE-A", FieldType.STRING, 
            8);

        pnd_Misc_Data__R_Field_3 = pnd_Misc_Data.newGroupInGroup("pnd_Misc_Data__R_Field_3", "REDEFINE", pnd_Misc_Data_Pnd_Work_Start_Date_A);
        pnd_Misc_Data_Pnd_Work_Mm = pnd_Misc_Data__R_Field_3.newFieldInGroup("pnd_Misc_Data_Pnd_Work_Mm", "#WORK-MM", FieldType.STRING, 2);
        pnd_Misc_Data_Pnd_Filler1 = pnd_Misc_Data__R_Field_3.newFieldInGroup("pnd_Misc_Data_Pnd_Filler1", "#FILLER1", FieldType.STRING, 1);
        pnd_Misc_Data_Pnd_Work_Dd = pnd_Misc_Data__R_Field_3.newFieldInGroup("pnd_Misc_Data_Pnd_Work_Dd", "#WORK-DD", FieldType.STRING, 2);
        pnd_Misc_Data_Pnd_Filler2 = pnd_Misc_Data__R_Field_3.newFieldInGroup("pnd_Misc_Data_Pnd_Filler2", "#FILLER2", FieldType.STRING, 1);
        pnd_Misc_Data_Pnd_Work_Yy = pnd_Misc_Data__R_Field_3.newFieldInGroup("pnd_Misc_Data_Pnd_Work_Yy", "#WORK-YY", FieldType.STRING, 2);
        pnd_Misc_Data_Pnd_Work_End_Date_A = pnd_Misc_Data.newFieldInGroup("pnd_Misc_Data_Pnd_Work_End_Date_A", "#WORK-END-DATE-A", FieldType.STRING, 8);

        pnd_Misc_Data__R_Field_4 = pnd_Misc_Data.newGroupInGroup("pnd_Misc_Data__R_Field_4", "REDEFINE", pnd_Misc_Data_Pnd_Work_End_Date_A);
        pnd_Misc_Data_Pnd_Work_Mmm = pnd_Misc_Data__R_Field_4.newFieldInGroup("pnd_Misc_Data_Pnd_Work_Mmm", "#WORK-MMM", FieldType.STRING, 2);
        pnd_Misc_Data_Pnd_Fillera = pnd_Misc_Data__R_Field_4.newFieldInGroup("pnd_Misc_Data_Pnd_Fillera", "#FILLERA", FieldType.STRING, 1);
        pnd_Misc_Data_Pnd_Work_Ddd = pnd_Misc_Data__R_Field_4.newFieldInGroup("pnd_Misc_Data_Pnd_Work_Ddd", "#WORK-DDD", FieldType.STRING, 2);
        pnd_Misc_Data_Pnd_Fillerb = pnd_Misc_Data__R_Field_4.newFieldInGroup("pnd_Misc_Data_Pnd_Fillerb", "#FILLERB", FieldType.STRING, 1);
        pnd_Misc_Data_Pnd_Work_Yyy = pnd_Misc_Data__R_Field_4.newFieldInGroup("pnd_Misc_Data_Pnd_Work_Yyy", "#WORK-YYY", FieldType.STRING, 2);
        pnd_Report_End_Date = localVariables.newFieldInRecord("pnd_Report_End_Date", "#REPORT-END-DATE", FieldType.STRING, 8);
        pnd_Report_Today = localVariables.newFieldInRecord("pnd_Report_Today", "#REPORT-TODAY", FieldType.STRING, 8);
        pnd_Tiaa_Comp_Date_Start = localVariables.newFieldInRecord("pnd_Tiaa_Comp_Date_Start", "#TIAA-COMP-DATE-START", FieldType.STRING, 8);
        pnd_Tiaa_Comp_Date_End = localVariables.newFieldInRecord("pnd_Tiaa_Comp_Date_End", "#TIAA-COMP-DATE-END", FieldType.STRING, 8);
        pnd_Tiaa_Recvd_Date = localVariables.newFieldInRecord("pnd_Tiaa_Recvd_Date", "#TIAA-RECVD-DATE", FieldType.STRING, 8);
        pnd_Work_Date = localVariables.newFieldInRecord("pnd_Work_Date", "#WORK-DATE", FieldType.DATE);
        pnd_Comp_Date = localVariables.newFieldInRecord("pnd_Comp_Date", "#COMP-DATE", FieldType.STRING, 8);
        pnd_Clock_Dte_Tme = localVariables.newFieldInRecord("pnd_Clock_Dte_Tme", "#CLOCK-DTE-TME", FieldType.STRING, 15);

        pnd_Clock_Dte_Tme__R_Field_5 = localVariables.newGroupInRecord("pnd_Clock_Dte_Tme__R_Field_5", "REDEFINE", pnd_Clock_Dte_Tme);
        pnd_Clock_Dte_Tme_Pnd_Clock_Check_Date = pnd_Clock_Dte_Tme__R_Field_5.newFieldInGroup("pnd_Clock_Dte_Tme_Pnd_Clock_Check_Date", "#CLOCK-CHECK-DATE", 
            FieldType.STRING, 8);
        pnd_Parm_Type = localVariables.newFieldInRecord("pnd_Parm_Type", "#PARM-TYPE", FieldType.STRING, 1);
        pnd_Today = localVariables.newFieldInRecord("pnd_Today", "#TODAY", FieldType.STRING, 8);

        pnd_Today__R_Field_6 = localVariables.newGroupInRecord("pnd_Today__R_Field_6", "REDEFINE", pnd_Today);
        pnd_Today_Pnd_Today_N = pnd_Today__R_Field_6.newFieldInGroup("pnd_Today_Pnd_Today_N", "#TODAY-N", FieldType.NUMERIC, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        sort01Orgnl_Unit_CdeOld = internalLoopRecord.newFieldInRecord("Sort01_Orgnl_Unit_Cde_OLD", "Orgnl_Unit_Cde_OLD", FieldType.STRING, 8);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaCdbatxa.initializeValues();

        localVariables.reset();
        pnd_Header1_1.setInitialValue("          Corporate Workflow Facilities");
        pnd_Header1_2.setInitialValue("             No Transactions Received");
        pnd_Parm_Type.setInitialValue("W");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Cwfb3208() throws Exception
    {
        super("Cwfb3208");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  DEFINE PRINTERS AND FORMATS
        getReports().definePrinter(2, "NOT DEFINED");                                                                                                                     //Natural: DEFINE PRINTER ( 1 )
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: ASSIGN #PROGRAM = *PROGRAM
        //*  MAP THE CURRENT LANGUAGE CODE TO THE CODE IN THE BATCH MODEL TEXT LDA.
        pnd_Cur_Lang.setValue(Global.getLANGUAGE());                                                                                                                      //Natural: ASSIGN #CUR-LANG = *LANGUAGE
        pnd_Cur_Lang.setValue(ldaCdbatxa.getCdbatxa_Pnd_Lang_Map().getValue(pnd_Cur_Lang));                                                                               //Natural: ASSIGN #CUR-LANG = CDBATXA.#LANG-MAP ( #CUR-LANG )
        //* *SAG DEFINE EXIT START-OF-PROGRAM
        pnd_Misc_Data_Pnd_Filler1.setValue("/");                                                                                                                          //Natural: MOVE '/' TO #FILLER1 #FILLER2
        pnd_Misc_Data_Pnd_Filler2.setValue("/");
        pnd_Misc_Data_Pnd_Fillera.setValue("/");                                                                                                                          //Natural: MOVE '/' TO #FILLERA #FILLERB
        pnd_Misc_Data_Pnd_Fillerb.setValue("/");
        //*  GET THE RUN DATE
        DbsUtil.callnat(Cwfn3912.class , getCurrentProcessState(), pnd_Comp_Date, pnd_Parm_Type);                                                                         //Natural: CALLNAT 'CWFN3912' #COMP-DATE #PARM-TYPE
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Comp_Date.greater(pnd_Misc_Data_Pnd_Start_Date)))                                                                                               //Natural: IF #COMP-DATE GT #START-DATE
        {
            pnd_Today.setValue(pnd_Comp_Date);                                                                                                                            //Natural: MOVE #COMP-DATE TO #TODAY
            pnd_Work_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Today);                                                                                       //Natural: MOVE EDITED #TODAY TO #WORK-DATE ( EM = YYYYMMDD )
            //*  WEEK ENDING DATE 3 WEEKS AGO
            pnd_Report_Today.setValueEdited(pnd_Work_Date,new ReportEditMask("MM/DD/YY"));                                                                                //Natural: MOVE EDITED #WORK-DATE ( EM = MM/DD/YY ) TO #REPORT-TODAY
            pnd_Work_Date.nsubtract(21);                                                                                                                                  //Natural: ASSIGN #WORK-DATE := #WORK-DATE - 21
            pnd_Tiaa_Comp_Date_End.setValueEdited(pnd_Work_Date,new ReportEditMask("YYYYMMDD"));                                                                          //Natural: MOVE EDITED #WORK-DATE ( EM = YYYYMMDD ) TO #TIAA-COMP-DATE-END
            //*  WEEK STARTING DATE 3 WEEKS AGO
            pnd_Report_End_Date.setValueEdited(pnd_Work_Date,new ReportEditMask("MM/DD/YY"));                                                                             //Natural: MOVE EDITED #WORK-DATE ( EM = MM/DD/YY ) TO #REPORT-END-DATE
            pnd_Work_Date.nsubtract(6);                                                                                                                                   //Natural: ASSIGN #WORK-DATE := #WORK-DATE - 6
            pnd_Tiaa_Comp_Date_Start.setValueEdited(pnd_Work_Date,new ReportEditMask("YYYYMMDD"));                                                                        //Natural: MOVE EDITED #WORK-DATE ( EM = YYYYMMDD ) TO #TIAA-COMP-DATE-START
            //*  INITIALIZE WPID TABLE FOR SRA FORMS REQUESTS
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Wpid_Tbl.getValue(1).setValue("FABIT");                                                                                                                       //Natural: ASSIGN #WPID-TBL ( 1 ) := 'FABIT'
        pnd_Wpid_Tbl.getValue(2).setValue("FASIT");                                                                                                                       //Natural: ASSIGN #WPID-TBL ( 2 ) := 'FASIT'
        pnd_Wpid_Tbl.getValue(3).setValue("BABC");                                                                                                                        //Natural: ASSIGN #WPID-TBL ( 3 ) := 'BABC'
        pnd_Wpid_Tbl.getValue(4).setValue("BASB");                                                                                                                        //Natural: ASSIGN #WPID-TBL ( 4 ) := 'BASB'
        unit_Tot_Line_Tbl.getValue(1).setValue("Unit Total Requests Without Txns for FABIT is:");                                                                         //Natural: ASSIGN UNIT-TOT-LINE-TBL ( 1 ) := 'Unit Total Requests Without Txns for FABIT is:'
        unit_Tot_Line_Tbl.getValue(2).setValue("Unit Total Requests Without Txns for FASIT is:");                                                                         //Natural: ASSIGN UNIT-TOT-LINE-TBL ( 2 ) := 'Unit Total Requests Without Txns for FASIT is:'
        unit_Tot_Line_Tbl.getValue(3).setValue("Unit Total Requests Without Txns for BABC  is:");                                                                         //Natural: ASSIGN UNIT-TOT-LINE-TBL ( 3 ) := 'Unit Total Requests Without Txns for BABC  is:'
        unit_Tot_Line_Tbl.getValue(4).setValue("Unit Total Requests Without Txns for BASB  is:");                                                                         //Natural: ASSIGN UNIT-TOT-LINE-TBL ( 4 ) := 'Unit Total Requests Without Txns for BASB  is:'
        grand_Tot_Line_Tbl.getValue(1).setValue("Total Requests Without Txns for FABIT  is:");                                                                            //Natural: ASSIGN GRAND-TOT-LINE-TBL ( 1 ) := 'Total Requests Without Txns for FABIT  is:'
        grand_Tot_Line_Tbl.getValue(2).setValue("Total Requests Without Txns for FASIT  is:");                                                                            //Natural: ASSIGN GRAND-TOT-LINE-TBL ( 2 ) := 'Total Requests Without Txns for FASIT  is:'
        grand_Tot_Line_Tbl.getValue(3).setValue("Total Requests Without Txns for BABC   is:");                                                                            //Natural: ASSIGN GRAND-TOT-LINE-TBL ( 3 ) := 'Total Requests Without Txns for BABC   is:'
        grand_Tot_Line_Tbl.getValue(4).setValue("Total Requests Without Txns for BASB   is:");                                                                            //Natural: ASSIGN GRAND-TOT-LINE-TBL ( 4 ) := 'Total Requests Without Txns for BASB   is:'
        //* *SAG END-EXIT
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH' THEN
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA = 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        //*  PRIMARY FILE
        pnd_Prime_Counter.reset();                                                                                                                                        //Natural: RESET #PRIME-COUNTER
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 FORMS-WITHOUT-TXNS
        while (condition(getWorkFiles().read(1, forms_Without_Txns)))
        {
            pnd_Prime_Counter.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #PRIME-COUNTER
            //* *SAG DEFINE EXIT PRIME-WRITE-FIELDS
            //* *SAG END-EXIT
            //* *SAG DEFINE EXIT SORT-FIELDS
            getSort().writeSortInData(forms_Without_Txns_Orgnl_Unit_Cde, forms_Without_Txns_Tiaa_Rcvd_Dte, forms_Without_Txns_Pin_Nbr, forms_Without_Txns_Work_Prcss_Id,  //Natural: END-ALL
                forms_Without_Txns_Name, forms_Without_Txns_Ssn);
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getSort().sortData(forms_Without_Txns_Orgnl_Unit_Cde, forms_Without_Txns_Tiaa_Rcvd_Dte);                                                                          //Natural: SORT FORMS-WITHOUT-TXNS.ORGNL-UNIT-CDE FORMS-WITHOUT-TXNS.TIAA-RCVD-DTE USING FORMS-WITHOUT-TXNS.PIN-NBR FORMS-WITHOUT-TXNS.WORK-PRCSS-ID FORMS-WITHOUT-TXNS.NAME FORMS-WITHOUT-TXNS.SSN
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(forms_Without_Txns_Orgnl_Unit_Cde, forms_Without_Txns_Tiaa_Rcvd_Dte, forms_Without_Txns_Pin_Nbr, forms_Without_Txns_Work_Prcss_Id, 
            forms_Without_Txns_Name, forms_Without_Txns_Ssn)))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            pnd_Counters_Pnd_Tot_Unit.nadd(1);                                                                                                                            //Natural: ADD 1 TO #TOT-UNIT
            pnd_Counters_Pnd_Sra_Forms_Without.nadd(1);                                                                                                                   //Natural: ADD 1 TO #SRA-FORMS-WITHOUT
            FOR1:                                                                                                                                                         //Natural: FOR #I = 1 TO 4
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(4)); pnd_I.nadd(1))
            {
                if (condition(forms_Without_Txns_Work_Prcss_Id.equals(pnd_Wpid_Tbl.getValue(pnd_I))))                                                                     //Natural: IF FORMS-WITHOUT-TXNS.WORK-PRCSS-ID EQ #WPID-TBL ( #I )
                {
                    pnd_Counters_Pnd_Wpid_Unit_Ctr.getValue(pnd_I).nadd(1);                                                                                               //Natural: ADD 1 TO #WPID-UNIT-CTR ( #I )
                    pnd_Counters_Pnd_Wpid_Tot_Ctr.getValue(pnd_I).nadd(1);                                                                                                //Natural: ADD 1 TO #WPID-TOT-CTR ( #I )
                    if (true) break FOR1;                                                                                                                                 //Natural: ESCAPE BOTTOM ( FOR1. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
            sub_Write_Report();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* *SAG END-EXIT                                                                                                                                             //Natural: AT BREAK OF FORMS-WITHOUT-TXNS.ORGNL-UNIT-CDE
            sort01Orgnl_Unit_CdeOld.setValue(forms_Without_Txns_Orgnl_Unit_Cde);                                                                                          //Natural: END-SORT
        }
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        endSort();
        //* *SAG DEFINE EXIT END-OF-PROGRAM
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"Total Number of Work File Records Read   :",pnd_Prime_Counter);                                       //Natural: WRITE ( 1 ) // 'Total Number of Work File Records Read   :' #PRIME-COUNTER
        if (Global.isEscape()) return;
        FOR02:                                                                                                                                                            //Natural: FOR #I 1 TO 4
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(4)); pnd_I.nadd(1))
        {
            getReports().write(1, ReportOption.NOTITLE,grand_Tot_Line_Tbl.getValue(pnd_I),pnd_Counters_Pnd_Wpid_Tot_Ctr.getValue(pnd_I));                                 //Natural: WRITE ( 1 ) GRAND-TOT-LINE-TBL ( #I ) #WPID-TOT-CTR ( #I )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Total SRA Forms Requests Without Txns    :",pnd_Counters_Pnd_Sra_Forms_Without,NEWLINE,NEWLINE,new                    //Natural: WRITE ( 1 ) 'Total SRA Forms Requests Without Txns    :' #SRA-FORMS-WITHOUT //56T '**** End of Report ****'
            TabSetting(56),"**** End of Report ****");
        if (Global.isEscape()) return;
        //* *SAG END-EXIT
        //* *SAG DEFINE EXIT MISCELLANEOUS-SUBROUTINES
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT
        //*  'PIN/NBR'              FORMS-WITHOUT-TXNS.PIN-NBR
        //*                         (EM=9999999)
        //* *************
        //* *SAG END-EXIT
    }
    private void sub_Write_Report() throws Exception                                                                                                                      //Natural: WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        //* PIN-EXP <<
        getReports().display(1, "/NAME",                                                                                                                                  //Natural: DISPLAY ( 1 ) '/NAME' FORMS-WITHOUT-TXNS.NAME '/SSN' FORMS-WITHOUT-TXNS.SSN 'PIN/NBR' FORMS-WITHOUT-TXNS.PIN-NBR ( EM = 999999999999 ) 'FORMS/WPID' FORMS-WITHOUT-TXNS.WORK-PRCSS-ID '/TIAA REC"D DATE' FORMS-WITHOUT-TXNS.TIAA-RCVD-DTE
        		forms_Without_Txns_Name,"/SSN",
        		forms_Without_Txns_Ssn,"PIN/NBR",
        		forms_Without_Txns_Pin_Nbr, new ReportEditMask ("999999999999"),"FORMS/WPID",
        		forms_Without_Txns_Work_Prcss_Id,"/TIAA REC'D DATE",
        		forms_Without_Txns_Tiaa_Rcvd_Dte);
        if (Global.isEscape()) return;
        //* *************
        //*  WRITE-WORK-WITHOUT
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(41),pnd_Header1_1,new TabSetting(124),ldaCdbatxa.getCdbatxa_Pnd_Page_Txt().getValue(pnd_Cur_Lang),getReports().getPageNumberDbs(1),  //Natural: WRITE ( 1 ) NOTITLE *PROGRAM 41T #HEADER1-1 124T CDBATXA.#PAGE-TXT ( #CUR-LANG ) *PAGE-NUMBER ( 1 ) ( NL = 4 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 41T #HEADER1-2 124T *TIMX ( EM = HH':'II' 'AP ) / 46T 'Cases Closed During the Week Ended:' #REPORT-END-DATE / 37T 'With No Corresponding Transactions Received Before:' #REPORT-TODAY / '_' ( 132 )
                        new NumericLength (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),new 
                        TabSetting(41),pnd_Header1_2,new TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"),NEWLINE,new TabSetting(46),"Cases Closed During the Week Ended:",pnd_Report_End_Date,NEWLINE,new 
                        TabSetting(37),"With No Corresponding Transactions Received Before:",pnd_Report_Today,NEWLINE,"_",new RepeatItem(132));
                    //* *SAG DEFINE EXIT REPORT1-AT-TOP-OF-PAGE
                    //* *SAG END-EXIT
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean forms_Without_Txns_Orgnl_Unit_CdeIsBreak = forms_Without_Txns_Orgnl_Unit_Cde.isBreak(endOfData);
        if (condition(forms_Without_Txns_Orgnl_Unit_CdeIsBreak))
        {
            if (condition(getReports().getAstLinesLeft(1).less(6)))                                                                                                       //Natural: NEWPAGE ( 1 ) IF LESS THAN 6 LINES LEFT
            {
                getReports().newPage(1);
                if (condition(Global.isEscape())){return;}
            }
            pnd_Report_Pnd_Unit_Cde.setValue(sort01Orgnl_Unit_CdeOld);                                                                                                    //Natural: ASSIGN #REPORT.#UNIT-CDE := OLD ( FORMS-WITHOUT-TXNS.ORGNL-UNIT-CDE )
            getReports().write(1, ReportOption.NOTITLE,NEWLINE);                                                                                                          //Natural: WRITE ( 1 ) /
            if (condition(Global.isEscape())) return;
            FOR01:                                                                                                                                                        //Natural: FOR #I 1 TO 4
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(4)); pnd_I.nadd(1))
            {
                getReports().write(1, ReportOption.NOTITLE,unit_Tot_Line_Tbl.getValue(pnd_I),pnd_Counters_Pnd_Wpid_Unit_Ctr.getValue(pnd_I));                             //Natural: WRITE ( 1 ) UNIT-TOT-LINE-TBL ( #I ) #WPID-UNIT-CTR ( #I )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape())) return;
            getReports().write(1, ReportOption.NOTITLE,"Total SRA Forms Requests With no Txns for Unit:",pnd_Report_Pnd_Unit_Cde,"is:",pnd_Counters_Pnd_Tot_Unit);        //Natural: WRITE ( 1 ) 'Total SRA Forms Requests With no Txns for Unit:' #REPORT.#UNIT-CDE 'is:' #TOT-UNIT
            if (condition(Global.isEscape())) return;
            pnd_Counters_Pnd_Wpid_Unit_Ctr.getValue("*").reset();                                                                                                         //Natural: RESET #WPID-UNIT-CTR ( * ) #TOT-UNIT
            pnd_Counters_Pnd_Tot_Unit.reset();
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");

        getReports().setDisplayColumns(1, "/NAME",
        		forms_Without_Txns_Name,"/SSN",
        		forms_Without_Txns_Ssn,"PIN/NBR",
        		forms_Without_Txns_Pin_Nbr, new ReportEditMask ("999999999999"),"FORMS/WPID",
        		forms_Without_Txns_Work_Prcss_Id,"/TIAA REC'D DATE",
        		forms_Without_Txns_Tiaa_Rcvd_Dte);
    }
}
