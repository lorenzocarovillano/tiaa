/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:44:50 PM
**        * FROM NATURAL PROGRAM : Cwfb7977
************************************************************
**        * FILE NAME            : Cwfb7977.java
**        * CLASS NAME           : Cwfb7977
**        * INSTANCE NAME        : Cwfb7977
************************************************************
* =============================================================
* REPORT DAILY ACTIVITY - FROZEN MICROJACKETS
* --------------------------------------------------------
*   THIS PROGRAM RECEIVES SORTED DATA FROM PROGRAM CWFB7976
* IN SEQUENCE BY MEDIA-IND, SORT-DATE AND PIN (NBR).
*
* - MEDIA-IND = 2 = FROZEN MICROJACKET
* - MEDIA-IND = 3 = DIGITIZED MICROJACKET
*
* 1. A SERIES OF MICROJACKET REPORTS ARE PRODUCED:
*
*   CAB.                                CAB.     MIT    MIT
*  MEDIA  RPT                          SOURCE    ORIG   MJ
*   IND   NBR   REPORT                  CODE     UNIT   PULL
*   ---   ---   ---------------------   ----     ----   ----
*    2    01    CIRS LOGGED NON-MICRO   L,U      CRC
*    2    02    CIRS LOGGED MICRO       L,U      CRC    Y,R,J
*    2    03    USER LOGGED NON-MICRO   L,U
*    2    04    USER LOGGED MICRO       L,U             Y,R,J
*    2    05    BACK-END IMAGING        B
*    2    06    PARTIC. FILE UPDATE     P
*    2    07    MERGED RQST (NO MIT)    L,U,B,P
*
*    3    01    CIRS LOGGED             L        CRC
*    3    02    ** UNUSED
*    3    03    USER LOGGED             L
*    3    04    ** UNUSED
*    3    05    MICROJACKET REQUESTED   T
*    3    06    DIGITIZE SELECT         S
*    3    07    MERGED RQST (NO MIT)    L,T,S
*
* HISTORY
* ------------
* 07/19/96 JHH - ADD MEDIA-IND '3' REPORTS
*
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
* =============================================================

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb7977 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Work;

    private DbsGroup pnd_Work__R_Field_1;
    private DbsField pnd_Work_Media;
    private DbsField pnd_Work_Sort_Date;
    private DbsField pnd_Work_Pin;
    private DbsField pnd_Work_Rpt;
    private DbsField pnd_Work_Date;
    private DbsField pnd_Work_Wpid;
    private DbsField pnd_Work_Dte_Tme;

    private DataAccessProgramView vw_cwf_Tbl;
    private DbsField cwf_Tbl_Tbl_Data_Field;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_2;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Level;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field;

    private DataAccessProgramView vw_cwf_Wpid;
    private DbsField cwf_Wpid_Work_Prcss_Id;
    private DbsField cwf_Wpid_Work_Prcss_Short_Nme;

    private DataAccessProgramView vw_cwf_Stats;
    private DbsField cwf_Stats_Bes_Cde;
    private DbsField cwf_Stats_Bes_Dte;

    private DbsGroup cwf_Stats__R_Field_3;
    private DbsField cwf_Stats_Bes_Yyyymm;

    private DbsGroup cwf_Stats__R_Field_4;
    private DbsField cwf_Stats_Bes_Yyyy;
    private DbsField cwf_Stats_Bes_Mm;

    private DbsGroup cwf_Stats_Bes_Cnts;
    private DbsField cwf_Stats_Cirs_Nonm;
    private DbsField cwf_Stats_Cirs_Micr;
    private DbsField cwf_Stats_User_Nonm;
    private DbsField cwf_Stats_User_Micr;
    private DbsField cwf_Stats_Bend_Img;
    private DbsField cwf_Stats_Part_Updt;
    private DbsField cwf_Stats_Mit_Msng;
    private DbsField cwf_Stats_Bes_08;
    private DbsField cwf_Stats_Bes_09;
    private DbsField cwf_Stats_Bes_10;

    private DbsGroup cwf_Stats__R_Field_5;
    private DbsField cwf_Stats_Pnd_Bes_Cnts;
    private DbsField pnd_Stat_Bes_Key;

    private DbsGroup pnd_Stat_Bes_Key__R_Field_6;
    private DbsField pnd_Stat_Bes_Key_Pnd_Stat_Bes_Cde;
    private DbsField pnd_Stat_Bes_Key_Pnd_Stat_Bes_Dte;

    private DbsGroup pnd_Stat_Bes_Key__R_Field_7;
    private DbsField pnd_Stat_Bes_Key_Pnd_Stat_Bes_Yyyy;
    private DbsField pnd_Stat_Bes_Key_Pnd_Stat_Bes_Mm;
    private DbsField pnd_Stat_Bes_Key_Pnd_Stat_Bes_Dd;
    private DbsField pnd_Bes_Dte_Hi;
    private DbsField pnd_Bes_Dte_Lo;
    private DbsField pnd_Wrk_Cnt;
    private DbsField pnd_Tot_Cnt;
    private DbsField pnd_Med_Cnt;
    private DbsField pnd_Dte_Cnt;
    private DbsField pnd_Prt_Dest1;
    private DbsField pnd_Prt_Dest2;
    private DbsField pnd_Pge_Cnt1;
    private DbsField pnd_Pge_Cnt2;
    private DbsField pnd_A;
    private DbsField pnd_B;
    private DbsField pnd_D;
    private DbsField pnd_X;
    private DbsField pnd_W;
    private DbsField pnd_First_Time;
    private DbsField pnd_Isn;
    private DbsField pnd_Ranges;

    private DbsGroup pnd_Ranges__R_Field_8;

    private DbsGroup pnd_Ranges_Pnd_Range;
    private DbsField pnd_Ranges_Pnd_R_Limit;
    private DbsField pnd_Ranges_Pnd_R_Space;
    private DbsField pnd_R;
    private DbsField pnd_Hold_R;
    private DbsField pnd_Hold_Sort_Date;
    private DbsField pnd_Hold_Key;

    private DbsGroup pnd_Hold_Key__R_Field_9;
    private DbsField pnd_Hold_Key_Pnd_Hold_Media;
    private DbsField pnd_Hold_Key_Pnd_Hold_Rpt;

    private DbsGroup pnd_Media_Table;
    private DbsField pnd_Media_Table_Pnd_M1;
    private DbsField pnd_Media_Table_Pnd_M2;
    private DbsField pnd_Media_Table_Pnd_M3;

    private DbsGroup pnd_Media_Table__R_Field_10;

    private DbsGroup pnd_Media_Table_Pnd_Media_Tbl;
    private DbsField pnd_Media_Table_Pnd_Med_Key;
    private DbsField pnd_Media_Table_Fill0;
    private DbsField pnd_Media_Table_Pnd_Med_Nme;

    private DbsGroup pnd_Rpt_Table;
    private DbsField pnd_Rpt_Table_Pnd_R201;
    private DbsField pnd_Rpt_Table_Pnd_R202;
    private DbsField pnd_Rpt_Table_Pnd_R203;
    private DbsField pnd_Rpt_Table_Pnd_R204;
    private DbsField pnd_Rpt_Table_Pnd_R205;
    private DbsField pnd_Rpt_Table_Pnd_R206;
    private DbsField pnd_Rpt_Table_Pnd_R207;
    private DbsField pnd_Rpt_Table_Pnd_R301;
    private DbsField pnd_Rpt_Table_Pnd_R302;
    private DbsField pnd_Rpt_Table_Pnd_R303;
    private DbsField pnd_Rpt_Table_Pnd_R304;
    private DbsField pnd_Rpt_Table_Pnd_R305;
    private DbsField pnd_Rpt_Table_Pnd_R306;
    private DbsField pnd_Rpt_Table_Pnd_R307;

    private DbsGroup pnd_Rpt_Table__R_Field_11;

    private DbsGroup pnd_Rpt_Table_Pnd_Rpt_Tbl;
    private DbsField pnd_Rpt_Table_Pnd_Tbl_Key;

    private DbsGroup pnd_Rpt_Table__R_Field_12;
    private DbsField pnd_Rpt_Table_Pnd_Tbl_Media;
    private DbsField pnd_Rpt_Table_Pnd_Tbl_Rpt;
    private DbsField pnd_Rpt_Table_Fill1;
    private DbsField pnd_Rpt_Table_Pnd_Tbl_Nme;
    private DbsField pnd_Dte_Tot;
    private DbsField pnd_Med_Tot;
    private DbsField pnd_Grd_Tot;
    private DbsField pnd_Wrk_Tot;

    private DbsGroup h10;
    private DbsField h10_H10a;
    private DbsField h10_H10b;
    private DbsField h10_H10c;
    private DbsField h10_H10d;
    private DbsField h10_H10pge;
    private DbsField h20;
    private DbsField h22;
    private DbsField h25;

    private DbsGroup d10;
    private DbsField d10_D_Pin_Nbr;
    private DbsField d10_D_Tiaa_Rcvd_Dte;
    private DbsField d10_D_Wpid_Desc;
    private DbsField d10_D_Cor_Dte_Tme;
    private DbsField d10_D_Report;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Work = localVariables.newFieldInRecord("pnd_Work", "#WORK", FieldType.STRING, 45);

        pnd_Work__R_Field_1 = localVariables.newGroupInRecord("pnd_Work__R_Field_1", "REDEFINE", pnd_Work);
        pnd_Work_Media = pnd_Work__R_Field_1.newFieldInGroup("pnd_Work_Media", "MEDIA", FieldType.NUMERIC, 1);
        pnd_Work_Sort_Date = pnd_Work__R_Field_1.newFieldInGroup("pnd_Work_Sort_Date", "SORT-DATE", FieldType.DATE);
        pnd_Work_Pin = pnd_Work__R_Field_1.newFieldInGroup("pnd_Work_Pin", "PIN", FieldType.NUMERIC, 12);
        pnd_Work_Rpt = pnd_Work__R_Field_1.newFieldInGroup("pnd_Work_Rpt", "RPT", FieldType.NUMERIC, 2);
        pnd_Work_Date = pnd_Work__R_Field_1.newFieldInGroup("pnd_Work_Date", "DATE", FieldType.DATE);
        pnd_Work_Wpid = pnd_Work__R_Field_1.newFieldInGroup("pnd_Work_Wpid", "WPID", FieldType.STRING, 6);
        pnd_Work_Dte_Tme = pnd_Work__R_Field_1.newFieldInGroup("pnd_Work_Dte_Tme", "DTE-TME", FieldType.TIME);

        vw_cwf_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Tbl", "CWF-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Tbl_Tbl_Data_Field = vw_cwf_Tbl.getRecord().newFieldInGroup("cwf_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 253, RepeatingFieldStrategy.None, 
            "TBL_DATA_FIELD");
        registerRecord(vw_cwf_Tbl);

        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_2", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Level = pnd_Tbl_Prime_Key__R_Field_2.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Level", "#TBL-LEVEL", FieldType.STRING, 
            2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_2.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_2.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            30);

        vw_cwf_Wpid = new DataAccessProgramView(new NameInfo("vw_cwf_Wpid", "CWF-WPID"), "CWF_WP_WORK_PRCSS_ID", "CWF_PROFILE");
        cwf_Wpid_Work_Prcss_Id = vw_cwf_Wpid.getRecord().newFieldInGroup("cwf_Wpid_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        cwf_Wpid_Work_Prcss_Id.setDdmHeader("WORK PRCSS ID");
        cwf_Wpid_Work_Prcss_Short_Nme = vw_cwf_Wpid.getRecord().newFieldInGroup("cwf_Wpid_Work_Prcss_Short_Nme", "WORK-PRCSS-SHORT-NME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "WORK_PRCSS_SHORT_NME");
        cwf_Wpid_Work_Prcss_Short_Nme.setDdmHeader("SHORT NAME");
        registerRecord(vw_cwf_Wpid);

        vw_cwf_Stats = new DataAccessProgramView(new NameInfo("vw_cwf_Stats", "CWF-STATS"), "CWF_FOLDER_STATS", "CWF_FOLDER_STATS", DdmPeriodicGroups.getInstance().getGroups("CWF_FOLDER_STATS"));
        cwf_Stats_Bes_Cde = vw_cwf_Stats.getRecord().newFieldInGroup("cwf_Stats_Bes_Cde", "BES-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "BES_CDE");
        cwf_Stats_Bes_Dte = vw_cwf_Stats.getRecord().newFieldInGroup("cwf_Stats_Bes_Dte", "BES-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BES_DTE");

        cwf_Stats__R_Field_3 = vw_cwf_Stats.getRecord().newGroupInGroup("cwf_Stats__R_Field_3", "REDEFINE", cwf_Stats_Bes_Dte);
        cwf_Stats_Bes_Yyyymm = cwf_Stats__R_Field_3.newFieldInGroup("cwf_Stats_Bes_Yyyymm", "BES-YYYYMM", FieldType.STRING, 6);

        cwf_Stats__R_Field_4 = cwf_Stats__R_Field_3.newGroupInGroup("cwf_Stats__R_Field_4", "REDEFINE", cwf_Stats_Bes_Yyyymm);
        cwf_Stats_Bes_Yyyy = cwf_Stats__R_Field_4.newFieldInGroup("cwf_Stats_Bes_Yyyy", "BES-YYYY", FieldType.NUMERIC, 4);
        cwf_Stats_Bes_Mm = cwf_Stats__R_Field_4.newFieldInGroup("cwf_Stats_Bes_Mm", "BES-MM", FieldType.NUMERIC, 2);

        cwf_Stats_Bes_Cnts = vw_cwf_Stats.getRecord().newGroupInGroup("cwf_Stats_Bes_Cnts", "BES-CNTS", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "CWF_FOLDER_STATS_BES_CNTS");
        cwf_Stats_Cirs_Nonm = cwf_Stats_Bes_Cnts.newFieldArrayInGroup("cwf_Stats_Cirs_Nonm", "CIRS-NONM", FieldType.NUMERIC, 7, new DbsArrayController(1, 
            32) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIRS_NONM", "CWF_FOLDER_STATS_BES_CNTS");
        cwf_Stats_Cirs_Micr = cwf_Stats_Bes_Cnts.newFieldArrayInGroup("cwf_Stats_Cirs_Micr", "CIRS-MICR", FieldType.NUMERIC, 7, new DbsArrayController(1, 
            32) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIRS_MICR", "CWF_FOLDER_STATS_BES_CNTS");
        cwf_Stats_User_Nonm = cwf_Stats_Bes_Cnts.newFieldArrayInGroup("cwf_Stats_User_Nonm", "USER-NONM", FieldType.NUMERIC, 7, new DbsArrayController(1, 
            32) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "USER_NONM", "CWF_FOLDER_STATS_BES_CNTS");
        cwf_Stats_User_Micr = cwf_Stats_Bes_Cnts.newFieldArrayInGroup("cwf_Stats_User_Micr", "USER-MICR", FieldType.NUMERIC, 7, new DbsArrayController(1, 
            32) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "USER_MICR", "CWF_FOLDER_STATS_BES_CNTS");
        cwf_Stats_Bend_Img = cwf_Stats_Bes_Cnts.newFieldArrayInGroup("cwf_Stats_Bend_Img", "BEND-IMG", FieldType.NUMERIC, 7, new DbsArrayController(1, 
            32) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "BEND_IMG", "CWF_FOLDER_STATS_BES_CNTS");
        cwf_Stats_Part_Updt = cwf_Stats_Bes_Cnts.newFieldArrayInGroup("cwf_Stats_Part_Updt", "PART-UPDT", FieldType.NUMERIC, 7, new DbsArrayController(1, 
            32) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PART_UPDT", "CWF_FOLDER_STATS_BES_CNTS");
        cwf_Stats_Mit_Msng = cwf_Stats_Bes_Cnts.newFieldArrayInGroup("cwf_Stats_Mit_Msng", "MIT-MSNG", FieldType.NUMERIC, 7, new DbsArrayController(1, 
            32) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "MIT_MSNG", "CWF_FOLDER_STATS_BES_CNTS");
        cwf_Stats_Bes_08 = cwf_Stats_Bes_Cnts.newFieldArrayInGroup("cwf_Stats_Bes_08", "BES-08", FieldType.NUMERIC, 7, new DbsArrayController(1, 32) , 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "BES_08", "CWF_FOLDER_STATS_BES_CNTS");
        cwf_Stats_Bes_09 = cwf_Stats_Bes_Cnts.newFieldArrayInGroup("cwf_Stats_Bes_09", "BES-09", FieldType.NUMERIC, 7, new DbsArrayController(1, 32) , 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "BES_09", "CWF_FOLDER_STATS_BES_CNTS");
        cwf_Stats_Bes_10 = cwf_Stats_Bes_Cnts.newFieldArrayInGroup("cwf_Stats_Bes_10", "BES-10", FieldType.NUMERIC, 7, new DbsArrayController(1, 32) , 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "BES_10", "CWF_FOLDER_STATS_BES_CNTS");

        cwf_Stats__R_Field_5 = vw_cwf_Stats.getRecord().newGroupInGroup("cwf_Stats__R_Field_5", "REDEFINE", cwf_Stats_Bes_Cnts);
        cwf_Stats_Pnd_Bes_Cnts = cwf_Stats__R_Field_5.newFieldArrayInGroup("cwf_Stats_Pnd_Bes_Cnts", "#BES-CNTS", FieldType.NUMERIC, 7, new DbsArrayController(1, 
            10, 1, 32));
        registerRecord(vw_cwf_Stats);

        pnd_Stat_Bes_Key = localVariables.newFieldInRecord("pnd_Stat_Bes_Key", "#STAT-BES-KEY", FieldType.STRING, 11);

        pnd_Stat_Bes_Key__R_Field_6 = localVariables.newGroupInRecord("pnd_Stat_Bes_Key__R_Field_6", "REDEFINE", pnd_Stat_Bes_Key);
        pnd_Stat_Bes_Key_Pnd_Stat_Bes_Cde = pnd_Stat_Bes_Key__R_Field_6.newFieldInGroup("pnd_Stat_Bes_Key_Pnd_Stat_Bes_Cde", "#STAT-BES-CDE", FieldType.STRING, 
            3);
        pnd_Stat_Bes_Key_Pnd_Stat_Bes_Dte = pnd_Stat_Bes_Key__R_Field_6.newFieldInGroup("pnd_Stat_Bes_Key_Pnd_Stat_Bes_Dte", "#STAT-BES-DTE", FieldType.STRING, 
            8);

        pnd_Stat_Bes_Key__R_Field_7 = pnd_Stat_Bes_Key__R_Field_6.newGroupInGroup("pnd_Stat_Bes_Key__R_Field_7", "REDEFINE", pnd_Stat_Bes_Key_Pnd_Stat_Bes_Dte);
        pnd_Stat_Bes_Key_Pnd_Stat_Bes_Yyyy = pnd_Stat_Bes_Key__R_Field_7.newFieldInGroup("pnd_Stat_Bes_Key_Pnd_Stat_Bes_Yyyy", "#STAT-BES-YYYY", FieldType.NUMERIC, 
            4);
        pnd_Stat_Bes_Key_Pnd_Stat_Bes_Mm = pnd_Stat_Bes_Key__R_Field_7.newFieldInGroup("pnd_Stat_Bes_Key_Pnd_Stat_Bes_Mm", "#STAT-BES-MM", FieldType.NUMERIC, 
            2);
        pnd_Stat_Bes_Key_Pnd_Stat_Bes_Dd = pnd_Stat_Bes_Key__R_Field_7.newFieldInGroup("pnd_Stat_Bes_Key_Pnd_Stat_Bes_Dd", "#STAT-BES-DD", FieldType.NUMERIC, 
            2);
        pnd_Bes_Dte_Hi = localVariables.newFieldInRecord("pnd_Bes_Dte_Hi", "#BES-DTE-HI", FieldType.STRING, 8);
        pnd_Bes_Dte_Lo = localVariables.newFieldInRecord("pnd_Bes_Dte_Lo", "#BES-DTE-LO", FieldType.STRING, 8);
        pnd_Wrk_Cnt = localVariables.newFieldInRecord("pnd_Wrk_Cnt", "#WRK-CNT", FieldType.NUMERIC, 8);
        pnd_Tot_Cnt = localVariables.newFieldInRecord("pnd_Tot_Cnt", "#TOT-CNT", FieldType.NUMERIC, 7);
        pnd_Med_Cnt = localVariables.newFieldInRecord("pnd_Med_Cnt", "#MED-CNT", FieldType.NUMERIC, 7);
        pnd_Dte_Cnt = localVariables.newFieldInRecord("pnd_Dte_Cnt", "#DTE-CNT", FieldType.NUMERIC, 7);
        pnd_Prt_Dest1 = localVariables.newFieldInRecord("pnd_Prt_Dest1", "#PRT-DEST1", FieldType.STRING, 8);
        pnd_Prt_Dest2 = localVariables.newFieldInRecord("pnd_Prt_Dest2", "#PRT-DEST2", FieldType.STRING, 8);
        pnd_Pge_Cnt1 = localVariables.newFieldInRecord("pnd_Pge_Cnt1", "#PGE-CNT1", FieldType.PACKED_DECIMAL, 4);
        pnd_Pge_Cnt2 = localVariables.newFieldInRecord("pnd_Pge_Cnt2", "#PGE-CNT2", FieldType.PACKED_DECIMAL, 3);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.PACKED_DECIMAL, 3);
        pnd_B = localVariables.newFieldInRecord("pnd_B", "#B", FieldType.PACKED_DECIMAL, 3);
        pnd_D = localVariables.newFieldInRecord("pnd_D", "#D", FieldType.PACKED_DECIMAL, 3);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.PACKED_DECIMAL, 3);
        pnd_W = localVariables.newFieldInRecord("pnd_W", "#W", FieldType.PACKED_DECIMAL, 3);
        pnd_First_Time = localVariables.newFieldInRecord("pnd_First_Time", "#FIRST-TIME", FieldType.BOOLEAN, 1);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 8);
        pnd_Ranges = localVariables.newFieldInRecord("pnd_Ranges", "#RANGES", FieldType.STRING, 325);

        pnd_Ranges__R_Field_8 = localVariables.newGroupInRecord("pnd_Ranges__R_Field_8", "REDEFINE", pnd_Ranges);

        pnd_Ranges_Pnd_Range = pnd_Ranges__R_Field_8.newGroupArrayInGroup("pnd_Ranges_Pnd_Range", "#RANGE", new DbsArrayController(1, 25));
        pnd_Ranges_Pnd_R_Limit = pnd_Ranges_Pnd_Range.newFieldInGroup("pnd_Ranges_Pnd_R_Limit", "#R-LIMIT", FieldType.NUMERIC, 12);
        pnd_Ranges_Pnd_R_Space = pnd_Ranges_Pnd_Range.newFieldInGroup("pnd_Ranges_Pnd_R_Space", "#R-SPACE", FieldType.STRING, 1);
        pnd_R = localVariables.newFieldInRecord("pnd_R", "#R", FieldType.PACKED_DECIMAL, 3);
        pnd_Hold_R = localVariables.newFieldInRecord("pnd_Hold_R", "#HOLD-R", FieldType.PACKED_DECIMAL, 3);
        pnd_Hold_Sort_Date = localVariables.newFieldInRecord("pnd_Hold_Sort_Date", "#HOLD-SORT-DATE", FieldType.DATE);
        pnd_Hold_Key = localVariables.newFieldInRecord("pnd_Hold_Key", "#HOLD-KEY", FieldType.STRING, 3);

        pnd_Hold_Key__R_Field_9 = localVariables.newGroupInRecord("pnd_Hold_Key__R_Field_9", "REDEFINE", pnd_Hold_Key);
        pnd_Hold_Key_Pnd_Hold_Media = pnd_Hold_Key__R_Field_9.newFieldInGroup("pnd_Hold_Key_Pnd_Hold_Media", "#HOLD-MEDIA", FieldType.NUMERIC, 1);
        pnd_Hold_Key_Pnd_Hold_Rpt = pnd_Hold_Key__R_Field_9.newFieldInGroup("pnd_Hold_Key_Pnd_Hold_Rpt", "#HOLD-RPT", FieldType.NUMERIC, 2);

        pnd_Media_Table = localVariables.newGroupInRecord("pnd_Media_Table", "#MEDIA-TABLE");
        pnd_Media_Table_Pnd_M1 = pnd_Media_Table.newFieldInGroup("pnd_Media_Table_Pnd_M1", "#M1", FieldType.STRING, 32);
        pnd_Media_Table_Pnd_M2 = pnd_Media_Table.newFieldInGroup("pnd_Media_Table_Pnd_M2", "#M2", FieldType.STRING, 32);
        pnd_Media_Table_Pnd_M3 = pnd_Media_Table.newFieldInGroup("pnd_Media_Table_Pnd_M3", "#M3", FieldType.STRING, 32);

        pnd_Media_Table__R_Field_10 = localVariables.newGroupInRecord("pnd_Media_Table__R_Field_10", "REDEFINE", pnd_Media_Table);

        pnd_Media_Table_Pnd_Media_Tbl = pnd_Media_Table__R_Field_10.newGroupArrayInGroup("pnd_Media_Table_Pnd_Media_Tbl", "#MEDIA-TBL", new DbsArrayController(1, 
            3));
        pnd_Media_Table_Pnd_Med_Key = pnd_Media_Table_Pnd_Media_Tbl.newFieldInGroup("pnd_Media_Table_Pnd_Med_Key", "#MED-KEY", FieldType.STRING, 3);
        pnd_Media_Table_Fill0 = pnd_Media_Table_Pnd_Media_Tbl.newFieldInGroup("pnd_Media_Table_Fill0", "FILL0", FieldType.STRING, 1);
        pnd_Media_Table_Pnd_Med_Nme = pnd_Media_Table_Pnd_Media_Tbl.newFieldInGroup("pnd_Media_Table_Pnd_Med_Nme", "#MED-NME", FieldType.STRING, 28);

        pnd_Rpt_Table = localVariables.newGroupInRecord("pnd_Rpt_Table", "#RPT-TABLE");
        pnd_Rpt_Table_Pnd_R201 = pnd_Rpt_Table.newFieldInGroup("pnd_Rpt_Table_Pnd_R201", "#R201", FieldType.STRING, 36);
        pnd_Rpt_Table_Pnd_R202 = pnd_Rpt_Table.newFieldInGroup("pnd_Rpt_Table_Pnd_R202", "#R202", FieldType.STRING, 36);
        pnd_Rpt_Table_Pnd_R203 = pnd_Rpt_Table.newFieldInGroup("pnd_Rpt_Table_Pnd_R203", "#R203", FieldType.STRING, 36);
        pnd_Rpt_Table_Pnd_R204 = pnd_Rpt_Table.newFieldInGroup("pnd_Rpt_Table_Pnd_R204", "#R204", FieldType.STRING, 36);
        pnd_Rpt_Table_Pnd_R205 = pnd_Rpt_Table.newFieldInGroup("pnd_Rpt_Table_Pnd_R205", "#R205", FieldType.STRING, 36);
        pnd_Rpt_Table_Pnd_R206 = pnd_Rpt_Table.newFieldInGroup("pnd_Rpt_Table_Pnd_R206", "#R206", FieldType.STRING, 36);
        pnd_Rpt_Table_Pnd_R207 = pnd_Rpt_Table.newFieldInGroup("pnd_Rpt_Table_Pnd_R207", "#R207", FieldType.STRING, 36);
        pnd_Rpt_Table_Pnd_R301 = pnd_Rpt_Table.newFieldInGroup("pnd_Rpt_Table_Pnd_R301", "#R301", FieldType.STRING, 36);
        pnd_Rpt_Table_Pnd_R302 = pnd_Rpt_Table.newFieldInGroup("pnd_Rpt_Table_Pnd_R302", "#R302", FieldType.STRING, 36);
        pnd_Rpt_Table_Pnd_R303 = pnd_Rpt_Table.newFieldInGroup("pnd_Rpt_Table_Pnd_R303", "#R303", FieldType.STRING, 36);
        pnd_Rpt_Table_Pnd_R304 = pnd_Rpt_Table.newFieldInGroup("pnd_Rpt_Table_Pnd_R304", "#R304", FieldType.STRING, 36);
        pnd_Rpt_Table_Pnd_R305 = pnd_Rpt_Table.newFieldInGroup("pnd_Rpt_Table_Pnd_R305", "#R305", FieldType.STRING, 36);
        pnd_Rpt_Table_Pnd_R306 = pnd_Rpt_Table.newFieldInGroup("pnd_Rpt_Table_Pnd_R306", "#R306", FieldType.STRING, 36);
        pnd_Rpt_Table_Pnd_R307 = pnd_Rpt_Table.newFieldInGroup("pnd_Rpt_Table_Pnd_R307", "#R307", FieldType.STRING, 36);

        pnd_Rpt_Table__R_Field_11 = localVariables.newGroupInRecord("pnd_Rpt_Table__R_Field_11", "REDEFINE", pnd_Rpt_Table);

        pnd_Rpt_Table_Pnd_Rpt_Tbl = pnd_Rpt_Table__R_Field_11.newGroupArrayInGroup("pnd_Rpt_Table_Pnd_Rpt_Tbl", "#RPT-TBL", new DbsArrayController(1, 
            14));
        pnd_Rpt_Table_Pnd_Tbl_Key = pnd_Rpt_Table_Pnd_Rpt_Tbl.newFieldInGroup("pnd_Rpt_Table_Pnd_Tbl_Key", "#TBL-KEY", FieldType.STRING, 3);

        pnd_Rpt_Table__R_Field_12 = pnd_Rpt_Table_Pnd_Rpt_Tbl.newGroupInGroup("pnd_Rpt_Table__R_Field_12", "REDEFINE", pnd_Rpt_Table_Pnd_Tbl_Key);
        pnd_Rpt_Table_Pnd_Tbl_Media = pnd_Rpt_Table__R_Field_12.newFieldInGroup("pnd_Rpt_Table_Pnd_Tbl_Media", "#TBL-MEDIA", FieldType.NUMERIC, 1);
        pnd_Rpt_Table_Pnd_Tbl_Rpt = pnd_Rpt_Table__R_Field_12.newFieldInGroup("pnd_Rpt_Table_Pnd_Tbl_Rpt", "#TBL-RPT", FieldType.NUMERIC, 2);
        pnd_Rpt_Table_Fill1 = pnd_Rpt_Table_Pnd_Rpt_Tbl.newFieldInGroup("pnd_Rpt_Table_Fill1", "FILL1", FieldType.STRING, 1);
        pnd_Rpt_Table_Pnd_Tbl_Nme = pnd_Rpt_Table_Pnd_Rpt_Tbl.newFieldInGroup("pnd_Rpt_Table_Pnd_Tbl_Nme", "#TBL-NME", FieldType.STRING, 32);
        pnd_Dte_Tot = localVariables.newFieldArrayInRecord("pnd_Dte_Tot", "#DTE-TOT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 7));
        pnd_Med_Tot = localVariables.newFieldArrayInRecord("pnd_Med_Tot", "#MED-TOT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 7));
        pnd_Grd_Tot = localVariables.newFieldArrayInRecord("pnd_Grd_Tot", "#GRD-TOT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 7));
        pnd_Wrk_Tot = localVariables.newFieldArrayInRecord("pnd_Wrk_Tot", "#WRK-TOT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 7));

        h10 = localVariables.newGroupInRecord("h10", "H10");
        h10_H10a = h10.newFieldInGroup("h10_H10a", "H10A", FieldType.STRING, 3);
        h10_H10b = h10.newFieldInGroup("h10_H10b", "H10B", FieldType.STRING, 23);
        h10_H10c = h10.newFieldInGroup("h10_H10c", "H10C", FieldType.STRING, 32);
        h10_H10d = h10.newFieldInGroup("h10_H10d", "H10D", FieldType.STRING, 5);
        h10_H10pge = h10.newFieldInGroup("h10_H10pge", "H10PGE", FieldType.NUMERIC, 4);
        h20 = localVariables.newFieldInRecord("h20", "H20", FieldType.STRING, 70);
        h22 = localVariables.newFieldInRecord("h22", "H22", FieldType.STRING, 70);
        h25 = localVariables.newFieldInRecord("h25", "H25", FieldType.STRING, 70);

        d10 = localVariables.newGroupInRecord("d10", "D10");
        d10_D_Pin_Nbr = d10.newFieldInGroup("d10_D_Pin_Nbr", "D-PIN-NBR", FieldType.STRING, 14);
        d10_D_Tiaa_Rcvd_Dte = d10.newFieldInGroup("d10_D_Tiaa_Rcvd_Dte", "D-TIAA-RCVD-DTE", FieldType.STRING, 12);
        d10_D_Wpid_Desc = d10.newFieldInGroup("d10_D_Wpid_Desc", "D-WPID-DESC", FieldType.STRING, 17);
        d10_D_Cor_Dte_Tme = d10.newFieldInGroup("d10_D_Cor_Dte_Tme", "D-COR-DTE-TME", FieldType.STRING, 25);
        d10_D_Report = d10.newFieldInGroup("d10_D_Report", "D-REPORT", FieldType.STRING, 36);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Tbl.reset();
        vw_cwf_Wpid.reset();
        vw_cwf_Stats.reset();

        localVariables.reset();
        pnd_Tbl_Prime_Key.setInitialValue("A CWF-REPORT12-INPUT  CWFB7975");
        pnd_Stat_Bes_Key.setInitialValue("BES");
        pnd_Bes_Dte_Hi.setInitialValue("00000000");
        pnd_Bes_Dte_Lo.setInitialValue("99999999");
        pnd_Prt_Dest1.setInitialValue("CMPRT01");
        pnd_Prt_Dest2.setInitialValue("CMPRT02");
        pnd_A.setInitialValue(1);
        pnd_B.setInitialValue(7);
        pnd_First_Time.setInitialValue(true);
        pnd_R.setInitialValue(1);
        pnd_Hold_R.setInitialValue(1);
        pnd_Media_Table_Pnd_M2.setInitialValue("BES Frozen Microjackets");
        pnd_Media_Table_Pnd_M3.setInitialValue("DIG Digitized Microjackets");
        pnd_Rpt_Table_Pnd_R201.setInitialValue("201 CIRS Logged Non-Microjacketable ");
        pnd_Rpt_Table_Pnd_R202.setInitialValue("202 CIRS Logged Microjacketable     ");
        pnd_Rpt_Table_Pnd_R203.setInitialValue("203 User Logged Non-Microjacketable ");
        pnd_Rpt_Table_Pnd_R204.setInitialValue("204 User Logged Microjacketable     ");
        pnd_Rpt_Table_Pnd_R205.setInitialValue("205 Back-End Imaging                ");
        pnd_Rpt_Table_Pnd_R206.setInitialValue("206 Participant File Update         ");
        pnd_Rpt_Table_Pnd_R207.setInitialValue("207 Merged Request (No MIT)         ");
        pnd_Rpt_Table_Pnd_R301.setInitialValue("301 CIRS Logged                     ");
        pnd_Rpt_Table_Pnd_R302.setInitialValue("303 ** unused                       ");
        pnd_Rpt_Table_Pnd_R303.setInitialValue("303 User Logged                     ");
        pnd_Rpt_Table_Pnd_R304.setInitialValue("304 ** unused                       ");
        pnd_Rpt_Table_Pnd_R305.setInitialValue("305 Microjacket Requested           ");
        pnd_Rpt_Table_Pnd_R306.setInitialValue("306 Digitize Select                 ");
        pnd_Rpt_Table_Pnd_R307.setInitialValue("307 Merged Request (No MIT)         ");
        h10_H10b.setInitialValue("     Reports by Media -");
        h10_H10d.setInitialValue(" Page");
        h20.setInitialValue("                  TIAA                            --- MEDIA ---");
        h22.setInitialValue("PIN NBR         RCVD DTE     WPID DESC            DATE AND TIME");
        h25.setInitialValue("------------   ----------  ---------------  ----------------------");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfb7977() throws Exception
    {
        super("Cwfb7977");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("CWFB7977", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*  ===========================================================
        //*  --------------------------------------                                                                                                                       //Natural: ON ERROR
        getReports().definePrinter(2, "#PRT-DEST1");                                                                                                                      //Natural: DEFINE PRINTER ( 1 ) OUTPUT #PRT-DEST1
        //*  --------------------------------------                                                                                                                       //Natural: FORMAT ( 1 ) LS = 133 PS = 60 ZP = OFF;//Natural: AT TOP OF PAGE ( 1 )
        getReports().definePrinter(3, "#PRT-DEST2");                                                                                                                      //Natural: DEFINE PRINTER ( 2 ) OUTPUT #PRT-DEST2
        //*  --------------------------------------------------------------------                                                                                         //Natural: FORMAT ( 2 ) LS = 133 PS = 60 ZP = OFF;//Natural: AT TOP OF PAGE ( 2 )
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #WORK
        while (condition(getWorkFiles().read(1, pnd_Work)))
        {
            //*  DUMMY RECORD
            if (condition(pnd_Work_Rpt.equals(getZero())))                                                                                                                //Natural: IF #WORK.RPT = 0
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  ------------------------
            pnd_Hold_Key_Pnd_Hold_Rpt.setValue(pnd_Work_Rpt);                                                                                                             //Natural: MOVE #WORK.RPT TO #HOLD-RPT
            if (condition(pnd_First_Time.getBoolean()))                                                                                                                   //Natural: IF #FIRST-TIME
            {
                pnd_Hold_Key_Pnd_Hold_Media.setValue(pnd_Work_Media);                                                                                                     //Natural: MOVE #WORK.MEDIA TO #HOLD-MEDIA
                pnd_Hold_Sort_Date.setValue(pnd_Work_Sort_Date);                                                                                                          //Natural: MOVE #WORK.SORT-DATE TO #HOLD-SORT-DATE
                                                                                                                                                                          //Natural: PERFORM PIN-BREAK-TEST
                sub_Pin_Break_Test();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Hold_R.setValue(pnd_R);                                                                                                                               //Natural: MOVE #R TO #HOLD-R
                h10_H10c.setValue(pnd_Media_Table_Pnd_Med_Nme.getValue(pnd_Hold_Key_Pnd_Hold_Media));                                                                     //Natural: MOVE #MED-NME ( #HOLD-MEDIA ) TO H10C
                pnd_First_Time.setValue(false);                                                                                                                           //Natural: ASSIGN #FIRST-TIME = FALSE
            }                                                                                                                                                             //Natural: END-IF
            //*  ------------------------
            //*  NEXT MEDIA IND
            if (condition(pnd_Work_Media.notEquals(pnd_Hold_Key_Pnd_Hold_Media)))                                                                                         //Natural: IF #WORK.MEDIA NOT = #HOLD-MEDIA
            {
                                                                                                                                                                          //Natural: PERFORM MEDIA-BREAK
                sub_Media_Break();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().eject(1, true);                                                                                                                              //Natural: EJECT ( 1 )
                getReports().write(1, ReportOption.NOTITLE," ");                                                                                                          //Natural: WRITE ( 1 ) ' '
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                h10_H10c.setValue(pnd_Media_Table_Pnd_Med_Nme.getValue(pnd_Hold_Key_Pnd_Hold_Media));                                                                     //Natural: MOVE #MED-NME ( #HOLD-MEDIA ) TO H10C
            }                                                                                                                                                             //Natural: END-IF
            //*  NEXT DATE
            if (condition((pnd_Work_Sort_Date.notEquals(pnd_Hold_Sort_Date))))                                                                                            //Natural: IF ( #WORK.SORT-DATE NOT = #HOLD-SORT-DATE )
            {
                                                                                                                                                                          //Natural: PERFORM DATE-BREAK
                sub_Date_Break();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PIN-BREAK-TEST
            sub_Pin_Break_Test();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  PIN RANGE BREAK
            if (condition(pnd_R.notEquals(pnd_Hold_R)))                                                                                                                   //Natural: IF #R NOT = #HOLD-R
            {
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Hold_R.setValue(pnd_R);                                                                                                                               //Natural: MOVE #R TO #HOLD-R
            }                                                                                                                                                             //Natural: END-IF
            //*  ------------------------------------
            //*  GROSS TOTALS
            pnd_Tot_Cnt.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #TOT-CNT
            //*  GROSS TOTALS FOR MEDIA
            pnd_Med_Cnt.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #MED-CNT
            //*  GROSS TOTALS FOR DAY
            pnd_Dte_Cnt.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #DTE-CNT
            //*  DATE TOTALS PER RPT
            pnd_Dte_Tot.getValue(pnd_Hold_Key_Pnd_Hold_Rpt).nadd(1);                                                                                                      //Natural: ADD 1 TO #DTE-TOT ( #HOLD-RPT )
            //*  MEDIA TOTALS ..
            pnd_Med_Tot.getValue(pnd_Hold_Key_Pnd_Hold_Rpt).nadd(1);                                                                                                      //Natural: ADD 1 TO #MED-TOT ( #HOLD-RPT )
            //*  GROSS TOTALS ..
            pnd_Grd_Tot.getValue(pnd_Hold_Key_Pnd_Hold_Rpt).nadd(1);                                                                                                      //Natural: ADD 1 TO #GRD-TOT ( #HOLD-RPT )
            d10_D_Pin_Nbr.setValueEdited(pnd_Work_Pin,new ReportEditMask("999999999999"));                                                                                //Natural: MOVE EDITED #WORK.PIN ( EM = 999999999999 ) TO D-PIN-NBR
            d10_D_Tiaa_Rcvd_Dte.setValueEdited(pnd_Work_Date,new ReportEditMask("MM/DD/YYYY"));                                                                           //Natural: MOVE EDITED #WORK.DATE ( EM = MM/DD/YYYY ) TO D-TIAA-RCVD-DTE
            d10_D_Wpid_Desc.reset();                                                                                                                                      //Natural: RESET D-WPID-DESC
            vw_cwf_Wpid.startDatabaseFind                                                                                                                                 //Natural: FIND ( 1 ) CWF-WPID WITH WPID-UNIQ-KEY = #WORK.WPID
            (
            "FIND01",
            new Wc[] { new Wc("WPID_UNIQ_KEY", "=", pnd_Work_Wpid, WcType.WITH) },
            1
            );
            FIND01:
            while (condition(vw_cwf_Wpid.readNextRow("FIND01")))
            {
                vw_cwf_Wpid.setIfNotFoundControlFlag(false);
                d10_D_Wpid_Desc.setValue(cwf_Wpid_Work_Prcss_Short_Nme);                                                                                                  //Natural: MOVE CWF-WPID.WORK-PRCSS-SHORT-NME TO D-WPID-DESC
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            d10_D_Cor_Dte_Tme.setValueEdited(pnd_Work_Dte_Tme,new ReportEditMask("MM/DD/YYYY'  'HH:II:SS.T"));                                                            //Natural: MOVE EDITED #WORK.DTE-TME ( EM = MM/DD/YYYY'  'HH:II:SS.T ) TO D-COR-DTE-TME
            pnd_X.compute(new ComputeParameters(false, pnd_X), pnd_A.add(pnd_Hold_Key_Pnd_Hold_Rpt).subtract(1));                                                         //Natural: COMPUTE #X = #A + #HOLD-RPT - 1
            d10_D_Report.setValue(pnd_Rpt_Table_Pnd_Tbl_Nme.getValue(pnd_X));                                                                                             //Natural: MOVE #RPT-TABLE.#TBL-NME ( #X ) TO D-REPORT
            getReports().write(1, ReportOption.NOTITLE,d10_D_Pin_Nbr,d10_D_Tiaa_Rcvd_Dte,d10_D_Wpid_Desc,d10_D_Cor_Dte_Tme,d10_D_Report);                                 //Natural: WRITE ( 1 ) D10
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  ------------------------------------- END OF JOB
                                                                                                                                                                          //Natural: PERFORM MEDIA-BREAK
        sub_Media_Break();
        if (condition(Global.isEscape())) {return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"All Media",pnd_Tot_Cnt,"Grand Total");                                                                        //Natural: WRITE ( 1 ) / 'All Media' #TOT-CNT 'Grand Total'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"=",new RepeatItem(65));                                                                                               //Natural: WRITE ( 1 ) '=' ( 65 )
        if (Global.isEscape()) return;
        //*  ------------------------------                         /* READ SUMMS
        h10_H10b.setValue("      Summary Reports -");                                                                                                                     //Natural: MOVE '      Summary Reports -' TO H10B
        pnd_Stat_Bes_Key_Pnd_Stat_Bes_Dte.setValue(pnd_Bes_Dte_Lo);                                                                                                       //Natural: MOVE #BES-DTE-LO TO #STAT-BES-DTE
        pnd_Stat_Bes_Key_Pnd_Stat_Bes_Mm.reset();                                                                                                                         //Natural: RESET #STAT-BES-MM
        pnd_Hold_Key_Pnd_Hold_Media.setValue(2);                                                                                                                          //Natural: MOVE 2 TO #HOLD-MEDIA
        h10_H10c.setValue(pnd_Media_Table_Pnd_Med_Nme.getValue(pnd_Hold_Key_Pnd_Hold_Media));                                                                             //Natural: MOVE #MED-NME ( #HOLD-MEDIA ) TO H10C
        //*  'BES'
        pnd_Stat_Bes_Key_Pnd_Stat_Bes_Cde.setValue(pnd_Media_Table_Pnd_Med_Key.getValue(pnd_Hold_Key_Pnd_Hold_Media));                                                    //Natural: MOVE #MED-KEY ( #HOLD-MEDIA ) TO #STAT-BES-CDE
        //*  YTD CNTRS
        pnd_Wrk_Tot.getValue("*").reset();                                                                                                                                //Natural: RESET #WRK-TOT ( * )
        pnd_A.setValue(1);                                                                                                                                                //Natural: ASSIGN #A = 1
        pnd_B.setValue(7);                                                                                                                                                //Natural: ASSIGN #B = 7
                                                                                                                                                                          //Natural: PERFORM GET-SUMMARY-STATS
        sub_Get_Summary_Stats();
        if (condition(Global.isEscape())) {return;}
        getReports().eject(2, true);                                                                                                                                      //Natural: EJECT ( 2 )
        getReports().write(2, ReportOption.NOTITLE," ");                                                                                                                  //Natural: WRITE ( 2 ) ' '
        if (Global.isEscape()) return;
        pnd_Hold_Key_Pnd_Hold_Media.nadd(1);                                                                                                                              //Natural: ADD 1 TO #HOLD-MEDIA
        h10_H10c.setValue(pnd_Media_Table_Pnd_Med_Nme.getValue(pnd_Hold_Key_Pnd_Hold_Media));                                                                             //Natural: MOVE #MED-NME ( #HOLD-MEDIA ) TO H10C
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        //*  'DIG'
        pnd_Stat_Bes_Key_Pnd_Stat_Bes_Cde.setValue(pnd_Media_Table_Pnd_Med_Key.getValue(pnd_Hold_Key_Pnd_Hold_Media));                                                    //Natural: MOVE #MED-KEY ( #HOLD-MEDIA ) TO #STAT-BES-CDE
        //*  YTD CNTRS
        pnd_Wrk_Tot.getValue("*").reset();                                                                                                                                //Natural: RESET #WRK-TOT ( * )
        pnd_A.nadd(7);                                                                                                                                                    //Natural: ADD 7 TO #A
        pnd_B.nadd(7);                                                                                                                                                    //Natural: ADD 7 TO #B
                                                                                                                                                                          //Natural: PERFORM GET-SUMMARY-STATS
        sub_Get_Summary_Stats();
        if (condition(Global.isEscape())) {return;}
        //*                                     /* BEGIN SUBROUTINES !!!
        //*  ========================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MEDIA-BREAK
        //*  ========================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DATE-BREAK
        //*  ========================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PIN-BREAK-TEST
        //*  ============================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-SUMMARY-STATS
        //*  -----------------
        //*  ==============================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-SUMMARY-STATS
        //*  ----------------------------------------------
    }
    //*  =============================================================
    private void sub_Media_Break() throws Exception                                                                                                                       //Natural: MEDIA-BREAK
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------
                                                                                                                                                                          //Natural: PERFORM DATE-BREAK
        sub_Date_Break();
        if (condition(Global.isEscape())) {return;}
        //*  #W = 1 TO 7
        //*  #A = 1 OR 8; #B = 7 OR 14
        getReports().write(1, ReportOption.NOTITLE,NEWLINE);                                                                                                              //Natural: WRITE ( 1 ) /
        if (Global.isEscape()) return;
        pnd_W.reset();                                                                                                                                                    //Natural: RESET #W
        FOR01:                                                                                                                                                            //Natural: FOR #X = #A TO #B
        for (pnd_X.setValue(pnd_A); condition(pnd_X.lessOrEqual(pnd_B)); pnd_X.nadd(1))
        {
            pnd_W.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #W
            if (condition(pnd_Med_Tot.getValue(pnd_W).greater(getZero())))                                                                                                //Natural: IF #MED-TOT ( #W ) > 0
            {
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(11),pnd_Med_Tot.getValue(pnd_W),pnd_Rpt_Table_Pnd_Tbl_Nme.getValue(pnd_X));                     //Natural: WRITE ( 1 ) 11T #MED-TOT ( #W ) #TBL-NME ( #X )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_Media_Table_Pnd_Med_Key.getValue(pnd_Hold_Key_Pnd_Hold_Media),new ColumnSpacing(7),pnd_Med_Cnt,            //Natural: WRITE ( 1 ) / #MED-KEY ( #HOLD-MEDIA ) 7X #MED-CNT 'Total' #MED-NME ( #HOLD-MEDIA )
            "Total",pnd_Media_Table_Pnd_Med_Nme.getValue(pnd_Hold_Key_Pnd_Hold_Media));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(65));                                                                                               //Natural: WRITE ( 1 ) '-' ( 65 )
        if (Global.isEscape()) return;
        pnd_Med_Tot.getValue("*").reset();                                                                                                                                //Natural: RESET #MED-TOT ( * ) #MED-CNT
        pnd_Med_Cnt.reset();
        pnd_Hold_Key_Pnd_Hold_Media.setValue(pnd_Work_Media);                                                                                                             //Natural: MOVE #WORK.MEDIA TO #HOLD-MEDIA
        //*  #A = 1 OR 8; #B = 7 OR 14
        pnd_A.nadd(7);                                                                                                                                                    //Natural: ADD 7 TO #A
        pnd_B.nadd(7);                                                                                                                                                    //Natural: ADD 7 TO #B
        //*  MEDIA-BREAK
    }
    private void sub_Date_Break() throws Exception                                                                                                                        //Natural: DATE-BREAK
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------
        //*  #W = 1 TO 7
        //*  #A = 1 OR 8; #B = 7 OR 14
        getReports().write(1, ReportOption.NOTITLE,NEWLINE);                                                                                                              //Natural: WRITE ( 1 ) /
        if (Global.isEscape()) return;
        pnd_W.reset();                                                                                                                                                    //Natural: RESET #W
        FOR02:                                                                                                                                                            //Natural: FOR #X = #A TO #B
        for (pnd_X.setValue(pnd_A); condition(pnd_X.lessOrEqual(pnd_B)); pnd_X.nadd(1))
        {
            pnd_W.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #W
            if (condition(pnd_Dte_Tot.getValue(pnd_W).greater(getZero())))                                                                                                //Natural: IF #DTE-TOT ( #W ) > 0
            {
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(11),pnd_Dte_Tot.getValue(pnd_W),pnd_Rpt_Table_Pnd_Tbl_Nme.getValue(pnd_X));                     //Natural: WRITE ( 1 ) 11T #DTE-TOT ( #W ) #TBL-NME ( #X )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_Hold_Sort_Date, new ReportEditMask ("MM/DD/YY"),new ColumnSpacing(2),pnd_Dte_Cnt,"Total");                 //Natural: WRITE ( 1 ) / #HOLD-SORT-DATE ( EM = MM/DD/YY ) 2X #DTE-CNT 'Total'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(70));                                                                                               //Natural: WRITE ( 1 ) '-' ( 70 )
        if (Global.isEscape()) return;
        //*  --------------------
        //*  JHH 03/14/96
                                                                                                                                                                          //Natural: PERFORM UPDATE-SUMMARY-STATS
        sub_Update_Summary_Stats();
        if (condition(Global.isEscape())) {return;}
        //*  --------------------
        pnd_Dte_Tot.getValue("*").reset();                                                                                                                                //Natural: RESET #DTE-TOT ( * ) #DTE-CNT
        pnd_Dte_Cnt.reset();
        pnd_Hold_Sort_Date.setValue(pnd_Work_Sort_Date);                                                                                                                  //Natural: MOVE #WORK.SORT-DATE TO #HOLD-SORT-DATE
        //*  FORCE PIN BREAK
        pnd_R.resetInitial();                                                                                                                                             //Natural: RESET INITIAL #R
        pnd_Hold_R.reset();                                                                                                                                               //Natural: RESET #HOLD-R
        //*  DATE-BREAK
    }
    private void sub_Pin_Break_Test() throws Exception                                                                                                                    //Natural: PIN-BREAK-TEST
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------
        if (condition(pnd_First_Time.getBoolean()))                                                                                                                       //Natural: IF #FIRST-TIME
        {
            //*  PIN RANGE ON SUPPORT TBL
            vw_cwf_Tbl.startDatabaseFind                                                                                                                                  //Natural: FIND ( 1 ) CWF-TBL WITH TBL-PRIME-KEY = #TBL-PRIME-KEY
            (
            "FIND02",
            new Wc[] { new Wc("TBL_PRIME_KEY", "=", pnd_Tbl_Prime_Key, WcType.WITH) },
            1
            );
            FIND02:
            while (condition(vw_cwf_Tbl.readNextRow("FIND02")))
            {
                vw_cwf_Tbl.setIfNotFoundControlFlag(false);
                pnd_Ranges.setValue(cwf_Tbl_Tbl_Data_Field);                                                                                                              //Natural: MOVE CWF-TBL.TBL-DATA-FIELD TO #RANGES
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        FOR03:                                                                                                                                                            //Natural: FOR #R = #R TO 25
        for (pnd_R.setValue(pnd_R); condition(pnd_R.lessOrEqual(25)); pnd_R.nadd(1))
        {
            if (condition(pnd_Work_Pin.less(pnd_Ranges_Pnd_R_Limit.getValue(pnd_R))))                                                                                     //Natural: IF #WORK.PIN < #R-LIMIT ( #R )
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  PIN-BREAK-TEST
    }
    //*  JHH 03/14/96
    private void sub_Update_Summary_Stats() throws Exception                                                                                                              //Natural: UPDATE-SUMMARY-STATS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------
        pnd_Stat_Bes_Key_Pnd_Stat_Bes_Cde.setValue(pnd_Media_Table_Pnd_Med_Key.getValue(pnd_Hold_Key_Pnd_Hold_Media));                                                    //Natural: MOVE #MED-KEY ( #HOLD-MEDIA ) TO #STAT-BES-CDE
        pnd_Stat_Bes_Key_Pnd_Stat_Bes_Dte.setValueEdited(pnd_Hold_Sort_Date,new ReportEditMask("YYYYMMDD"));                                                              //Natural: MOVE EDITED #HOLD-SORT-DATE ( EM = YYYYMMDD ) TO #STAT-BES-DTE
        //*  SET 'day' INDEX
        pnd_D.setValue(pnd_Stat_Bes_Key_Pnd_Stat_Bes_Dd);                                                                                                                 //Natural: MOVE #STAT-BES-DD TO #D
        pnd_Stat_Bes_Key_Pnd_Stat_Bes_Dd.reset();                                                                                                                         //Natural: RESET #STAT-BES-DD
        if (condition(pnd_Stat_Bes_Key_Pnd_Stat_Bes_Dte.greater(pnd_Bes_Dte_Hi)))                                                                                         //Natural: IF #STAT-BES-DTE > #BES-DTE-HI
        {
            //*  SET HIGH YYYYMM
            pnd_Bes_Dte_Hi.setValue(pnd_Stat_Bes_Key_Pnd_Stat_Bes_Dte);                                                                                                   //Natural: MOVE #STAT-BES-DTE TO #BES-DTE-HI
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Stat_Bes_Key_Pnd_Stat_Bes_Dte.less(pnd_Bes_Dte_Lo)))                                                                                            //Natural: IF #STAT-BES-DTE < #BES-DTE-LO
        {
            //*  SET LOW YYYYMM
            pnd_Bes_Dte_Lo.setValue(pnd_Stat_Bes_Key_Pnd_Stat_Bes_Dte);                                                                                                   //Natural: MOVE #STAT-BES-DTE TO #BES-DTE-LO
        }                                                                                                                                                                 //Natural: END-IF
        //*  -----------------------------------------
        LOOP:                                                                                                                                                             //Natural: REPEAT
        while (condition(whileTrue))
        {
            //*  TEST PREVIOUS READ
            if (condition((cwf_Stats_Bes_Cde.notEquals(pnd_Stat_Bes_Key_Pnd_Stat_Bes_Cde)) || (cwf_Stats_Bes_Dte.notEquals(pnd_Stat_Bes_Key_Pnd_Stat_Bes_Dte))))          //Natural: IF ( BES-CDE NOT = #STAT-BES-CDE ) OR ( BES-DTE NOT = #STAT-BES-DTE )
            {
                //*  ------------------------------
                vw_cwf_Stats.startDatabaseFind                                                                                                                            //Natural: FIND ( 1 ) CWF-STATS WITH BES-KEY = #STAT-BES-KEY
                (
                "FIND_STATS",
                new Wc[] { new Wc("BES_KEY", "=", pnd_Stat_Bes_Key, WcType.WITH) },
                1
                );
                FIND_STATS:
                while (condition(vw_cwf_Stats.readNextRow("FIND_STATS", true)))
                {
                    vw_cwf_Stats.setIfNotFoundControlFlag(false);
                    //*  -----------------
                    //*  ADD A DAY
                    if (condition(vw_cwf_Stats.getAstCOUNTER().equals(0)))                                                                                                //Natural: IF NO RECORDS FOUND
                    {
                        vw_cwf_Stats.reset();                                                                                                                             //Natural: RESET CWF-STATS
                        cwf_Stats_Bes_Cde.setValue(pnd_Stat_Bes_Key_Pnd_Stat_Bes_Cde);                                                                                    //Natural: MOVE #STAT-BES-CDE TO BES-CDE
                        cwf_Stats_Bes_Dte.setValue(pnd_Stat_Bes_Key_Pnd_Stat_Bes_Dte);                                                                                    //Natural: MOVE #STAT-BES-DTE TO BES-DTE
                        FOR04:                                                                                                                                            //Natural: FOR #W = 1 TO 7
                        for (pnd_W.setValue(1); condition(pnd_W.lessOrEqual(7)); pnd_W.nadd(1))
                        {
                            //*  DAILY BUCKETS
                            cwf_Stats_Pnd_Bes_Cnts.getValue(pnd_W,pnd_D).nadd(pnd_Dte_Tot.getValue(pnd_W));                                                               //Natural: ADD #DTE-TOT ( #W ) TO #BES-CNTS ( #W,#D )
                            //*  MNTHLY SUMMRY BCKTS
                            cwf_Stats_Pnd_Bes_Cnts.getValue(pnd_W,32).nadd(pnd_Dte_Tot.getValue(pnd_W));                                                                  //Natural: ADD #DTE-TOT ( #W ) TO #BES-CNTS ( #W,32 )
                        }                                                                                                                                                 //Natural: END-FOR
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FIND_STATS"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FIND_STATS"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        STORE_STATS:                                                                                                                                      //Natural: STORE CWF-STATS
                        vw_cwf_Stats.insertDBRow("STORE_STATS");
                        pnd_Isn.setValue(vw_cwf_Stats.getAstISN("FIND_STATS"));                                                                                           //Natural: MOVE *ISN ( STORE-STATS. ) TO #ISN
                        if (true) break LOOP;                                                                                                                             //Natural: ESCAPE BOTTOM ( LOOP. )
                    }                                                                                                                                                     //Natural: END-NOREC
                    //*  -----------------
                    pnd_Isn.setValue(vw_cwf_Stats.getAstISN("FIND_STATS"));                                                                                               //Natural: MOVE *ISN ( FIND-STATS. ) TO #ISN
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("LOOP"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("LOOP"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  ------------------------------
            }                                                                                                                                                             //Natural: END-IF
            //*  UPDATE A DAY
            GET_STATS:                                                                                                                                                    //Natural: GET CWF-STATS #ISN
            vw_cwf_Stats.readByID(pnd_Isn.getLong(), "GET_STATS");
            FOR05:                                                                                                                                                        //Natural: FOR #W = 1 TO 7
            for (pnd_W.setValue(1); condition(pnd_W.lessOrEqual(7)); pnd_W.nadd(1))
            {
                //*  REDUCE MTD (IF PREV)
                cwf_Stats_Pnd_Bes_Cnts.getValue(pnd_W,32).nsubtract(cwf_Stats_Pnd_Bes_Cnts.getValue(pnd_W,pnd_D));                                                        //Natural: SUBTRACT #BES-CNTS ( #W,#D ) FROM #BES-CNTS ( #W,32 )
                //*  RESET DAY (IF PREV)
                cwf_Stats_Pnd_Bes_Cnts.getValue(pnd_W,pnd_D).setValue(pnd_Dte_Tot.getValue(pnd_W));                                                                       //Natural: MOVE #DTE-TOT ( #W ) TO #BES-CNTS ( #W,#D )
                //*  MNTHLY SUMMRY BCKTS
                cwf_Stats_Pnd_Bes_Cnts.getValue(pnd_W,32).nadd(pnd_Dte_Tot.getValue(pnd_W));                                                                              //Natural: ADD #DTE-TOT ( #W ) TO #BES-CNTS ( #W,32 )
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("LOOP"))) break;
                else if (condition(Global.isEscapeBottomImmediate("LOOP"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  -----------------
            vw_cwf_Stats.updateDBRow("GET_STATS");                                                                                                                        //Natural: UPDATE ( GET-STATS. )
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
            //*  LOOP
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  -----------------------------------------
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  UPDATE-SUMMARY-STATS
    }
    private void sub_Get_Summary_Stats() throws Exception                                                                                                                 //Natural: GET-SUMMARY-STATS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------
        //*  1READ=1MONTH
        vw_cwf_Stats.startDatabaseRead                                                                                                                                    //Natural: READ CWF-STATS BY BES-KEY STARTING FROM #STAT-BES-KEY
        (
        "READ02",
        new Wc[] { new Wc("BES_KEY", ">=", pnd_Stat_Bes_Key, WcType.BY) },
        new Oc[] { new Oc("BES_KEY", "ASC") }
        );
        READ02:
        while (condition(vw_cwf_Stats.readNextRow("READ02")))
        {
            if (condition((cwf_Stats_Bes_Cde.notEquals(pnd_Media_Table_Pnd_Med_Key.getValue(pnd_Hold_Key_Pnd_Hold_Media))) || (cwf_Stats_Bes_Dte.greater(pnd_Bes_Dte_Hi)))) //Natural: IF ( BES-CDE NOT = #MED-KEY ( #HOLD-MEDIA ) ) OR ( BES-DTE > #BES-DTE-HI )
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  ---------------------------------------                /* YEAR BREAK
            if (condition(cwf_Stats_Bes_Yyyy.notEquals(pnd_Stat_Bes_Key_Pnd_Stat_Bes_Yyyy)))                                                                              //Natural: IF BES-YYYY NOT = #STAT-BES-YYYY
            {
                //*  #W = 1 TO 7
                //*  #A = 1 OR 8; #B = 7 OR 14
                pnd_Wrk_Cnt.reset();                                                                                                                                      //Natural: RESET #WRK-CNT #W
                pnd_W.reset();
                FOR06:                                                                                                                                                    //Natural: FOR #X = #A TO #B
                for (pnd_X.setValue(pnd_A); condition(pnd_X.lessOrEqual(pnd_B)); pnd_X.nadd(1))
                {
                    pnd_W.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #W
                    if (condition(pnd_Wrk_Tot.getValue(pnd_W).greater(getZero())))                                                                                        //Natural: IF #WRK-TOT ( #W ) > 0
                    {
                        //*  YTD WRITE
                        getReports().write(2, ReportOption.NOTITLE,new ColumnSpacing(10),pnd_Wrk_Tot.getValue(pnd_W),pnd_Rpt_Table_Pnd_Tbl_Nme.getValue(pnd_X));          //Natural: WRITE ( 2 ) 10X #WRK-TOT ( #W ) #TBL-NME ( #X )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Wrk_Cnt.nadd(pnd_Wrk_Tot.getValue(pnd_W));                                                                                                    //Natural: ADD #WRK-TOT ( #W ) TO #WRK-CNT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(2, ReportOption.NOTITLE,pnd_Stat_Bes_Key_Pnd_Stat_Bes_Yyyy,"  ",pnd_Wrk_Cnt,"Yearly Total");                                           //Natural: WRITE ( 2 ) #STAT-BES-YYYY '  ' #WRK-CNT 'Yearly Total'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(65));                                                                                       //Natural: WRITE ( 2 ) '-' ( 65 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Stat_Bes_Key_Pnd_Stat_Bes_Yyyy.setValue(cwf_Stats_Bes_Yyyy);                                                                                          //Natural: MOVE BES-YYYY TO #STAT-BES-YYYY
                pnd_Wrk_Tot.getValue("*").reset();                                                                                                                        //Natural: RESET #WRK-TOT ( * )
            }                                                                                                                                                             //Natural: END-IF
            //*  ---------------------------------------
            //*  #W = 1 TO 7
            //*  #A = 1 OR 8; #B = 7 OR 14
            pnd_Wrk_Cnt.reset();                                                                                                                                          //Natural: RESET #WRK-CNT #W
            pnd_W.reset();
            FOR07:                                                                                                                                                        //Natural: FOR #X = #A TO #B
            for (pnd_X.setValue(pnd_A); condition(pnd_X.lessOrEqual(pnd_B)); pnd_X.nadd(1))
            {
                pnd_W.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #W
                if (condition(cwf_Stats_Pnd_Bes_Cnts.getValue(pnd_W,32).greater(getZero())))                                                                              //Natural: IF #BES-CNTS ( #W,32 ) > 0
                {
                    //*  MTD WRITE
                    getReports().write(2, ReportOption.NOTITLE,new ColumnSpacing(10),cwf_Stats_Pnd_Bes_Cnts.getValue(pnd_W,32),pnd_Rpt_Table_Pnd_Tbl_Nme.getValue(pnd_X)); //Natural: WRITE ( 2 ) 10X #BES-CNTS ( #W,32 ) #TBL-NME ( #X )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Wrk_Cnt.nadd(cwf_Stats_Pnd_Bes_Cnts.getValue(pnd_W,32));                                                                                          //Natural: ADD #BES-CNTS ( #W,32 ) TO #WRK-CNT
                    //*  YTD ADD
                    pnd_Wrk_Tot.getValue(pnd_W).nadd(cwf_Stats_Pnd_Bes_Cnts.getValue(pnd_W,32));                                                                          //Natural: ADD #BES-CNTS ( #W,32 ) TO #WRK-TOT ( #W )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(2, ReportOption.NOTITLE,cwf_Stats_Bes_Yyyymm, new ReportEditMask ("XXXX/XX"),new ColumnSpacing(2),pnd_Wrk_Cnt,"Monthly Total");            //Natural: WRITE ( 2 ) BES-YYYYMM ( EM = XXXX/XX ) 2X #WRK-CNT 'Monthly Total'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(65));                                                                                           //Natural: WRITE ( 2 ) '-' ( 65 )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  ------------------------------
        //*  #W = 1 TO 7
        //*  #A = 1 OR 8; #B = 7 OR 14
        pnd_Wrk_Cnt.reset();                                                                                                                                              //Natural: RESET #WRK-CNT #W
        pnd_W.reset();
        FOR08:                                                                                                                                                            //Natural: FOR #X = #A TO #B
        for (pnd_X.setValue(pnd_A); condition(pnd_X.lessOrEqual(pnd_B)); pnd_X.nadd(1))
        {
            pnd_W.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #W
            if (condition(pnd_Wrk_Tot.getValue(pnd_W).greater(getZero())))                                                                                                //Natural: IF #WRK-TOT ( #W ) > 0
            {
                //*  YTD WRITE
                getReports().write(2, ReportOption.NOTITLE,new ColumnSpacing(10),pnd_Wrk_Tot.getValue(pnd_W),pnd_Rpt_Table_Pnd_Tbl_Nme.getValue(pnd_X));                  //Natural: WRITE ( 2 ) 10X #WRK-TOT ( #W ) #TBL-NME ( #X )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Wrk_Cnt.nadd(pnd_Wrk_Tot.getValue(pnd_W));                                                                                                            //Natural: ADD #WRK-TOT ( #W ) TO #WRK-CNT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,pnd_Stat_Bes_Key_Pnd_Stat_Bes_Yyyy,"  ",pnd_Wrk_Cnt,"Yearly Total");                                                   //Natural: WRITE ( 2 ) #STAT-BES-YYYY '  ' #WRK-CNT 'Yearly Total'
        if (Global.isEscape()) return;
        //*  GET-SUMMARY-STATS
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Pge_Cnt1.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #PGE-CNT1
                    h10_H10pge.setValue(pnd_Pge_Cnt1);                                                                                                                    //Natural: MOVE #PGE-CNT1 TO H10PGE
                    getReports().write(1, ReportOption.NOTITLE,Global.getDATU(),h10_H10a,h10_H10b,h10_H10c,h10_H10d,h10_H10pge);                                          //Natural: WRITE ( 1 ) NOTITLE *DATU H10
                    getReports().write(1, ReportOption.NOTITLE,Global.getTIMX(),new ColumnSpacing(23),"For",pnd_Hold_Sort_Date, new ReportEditMask ("MM/DD/YY"),new       //Natural: WRITE ( 1 ) *TIMX 23X 'For' #HOLD-SORT-DATE ( EM = MM/DD/YY ) 30X *PROGRAM
                        ColumnSpacing(30),Global.getPROGRAM());
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,h20);                                                                                              //Natural: WRITE ( 1 ) / H20
                    getReports().write(1, ReportOption.NOTITLE,h22);                                                                                                      //Natural: WRITE ( 1 ) H22
                    getReports().write(1, ReportOption.NOTITLE,h25);                                                                                                      //Natural: WRITE ( 1 ) H25
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,Global.getDATU(),h10_H10a,h10_H10b,h10_H10c);                                                              //Natural: WRITE ( 2 ) NOTITLE *DATU H10A H10B H10C
                    getReports().write(2, ReportOption.NOTITLE,Global.getTIMX(),new ColumnSpacing(23),"For",pnd_Hold_Sort_Date, new ReportEditMask ("MM/DD/YY"),new       //Natural: WRITE ( 2 ) *TIMX 23X 'For' #HOLD-SORT-DATE ( EM = MM/DD/YY ) 30X *PROGRAM
                        ColumnSpacing(30),Global.getPROGRAM());
                    getReports().write(2, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 2 ) ' '
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, "Natural error",Global.getERROR_NR(),"in",Global.getPROGRAM(),"at line",Global.getERROR_LINE(),"on record",pnd_Tot_Cnt);                    //Natural: WRITE 'Natural error' *ERROR-NR 'in' *PROGRAM 'at line' *ERROR-LINE 'on record' #TOT-CNT
        DbsUtil.terminate(5);  if (true) return;                                                                                                                          //Natural: TERMINATE 05
        if (condition(true)) return;                                                                                                                                      //Natural: ESCAPE ROUTINE
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=60 ZP=OFF");
        Global.format(2, "LS=133 PS=60 ZP=OFF");
    }
}
