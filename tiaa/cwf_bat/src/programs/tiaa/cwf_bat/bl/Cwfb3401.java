/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:29:12 PM
**        * FROM NATURAL PROGRAM : Cwfb3401
************************************************************
**        * FILE NAME            : Cwfb3401.java
**        * CLASS NAME           : Cwfb3401
**        * INSTANCE NAME        : Cwfb3401
************************************************************
**SAG GENERATOR: SHELL-TIAA                       VERSION: 3.2.2
**SAG TITLE: REPORT 1
**SAG SYSTEM: CRPCWF
************************************************************************
* PROGRAM  : CWFB3001
* SYSTEM   : CRPCWF
* TITLE    : REPORT 1
* GENERATED: AUG 11,93 AT 10:46 AM
* FUNCTION : REPORT OF WORK REQUEST LOGGED AND INDEXED
*          | BATCH SPECIAL - CRC WEEKLY REPORT OF LOGGED + RETURNING DOC
*          |
*          |
*          |
*          |
*          |
* MOD DATE | MOD
* MM DD YY | BY  DESCRIPTION OF CHANGES
* -------- + --  -------------------------------------------------------
* 01/02/02 | GY  ADDED PROCESSING FOR NEW DATA (OWNER UNIT)
* 02/28/02 | JRF INCLUDE NON-PARTICIPANT PROCESSING  REF. JF022802
* 04/10/02 | JRF INCREASED LENGTH OF #RECCOUNT FROM N5 TO N7
*          |     REF. JF041002
* 04/16/02 | JRF REVISED REPORT DISTRIBUTION FOR ARM. REF. JF041602
*          |       CMPRT01 - CHARLOTTE (DETAILS & SUMMARY)
*          |       CMPRT02 - DENVER    (DETAILS & SUMMARY)
*          |       CMPRT03 - NEW YORK  (DETAILS & SUMMARY)
*          |       CMPRT04 - OTHERS    (DETAILS ONLY)
*          |     FIXED PROBLEM ON LAST-CHNGE-OPRTR-CDE
* 02/23/2017 - PIN EXPANSION - AUG 2017 <STOW ONLY>
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3401 extends BLNatBase
{
    // Data Areas
    private PdaCwfa2100 pdaCwfa2100;
    private PdaCwfa2101 pdaCwfa2101;
    private PdaCdaobjr pdaCdaobjr;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_P pdaCwfpda_P;
    private PdaCwfpda_M pdaCwfpda_M;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Rep_Parm;
    private DbsField pnd_Rep_Parm_Pnd_Rep_Racf_Id;
    private DbsField pnd_Rep_Parm_Pnd_Rep_Unit_Cde;
    private DbsField pnd_Rep_Parm_Pnd_Start_Dte_Tme;
    private DbsField pnd_Rep_Parm_Pnd_Start_Date;

    private DbsGroup pnd_Rep_Parm__R_Field_1;
    private DbsField pnd_Rep_Parm_Pnd_Start_Date_N;

    private DbsGroup pnd_Rep_Parm__R_Field_2;
    private DbsField pnd_Rep_Parm_Pnd_Start_Date_Fill;
    private DbsField pnd_Rep_Parm_Pnd_Start_Date_Dd;
    private DbsField pnd_Rep_Parm_Pnd_End_Date;

    private DbsGroup pnd_Rep_Parm__R_Field_3;
    private DbsField pnd_Rep_Parm_Pnd_End_Date_N;
    private DbsField pnd_Rep_Parm_Pnd_Work_Start_Date_A;

    private DbsGroup pnd_Rep_Parm__R_Field_4;
    private DbsField pnd_Rep_Parm_Pnd_Work_Mm;
    private DbsField pnd_Rep_Parm_Pnd_Filler1;
    private DbsField pnd_Rep_Parm_Pnd_Work_Dd;
    private DbsField pnd_Rep_Parm_Pnd_Filler2;
    private DbsField pnd_Rep_Parm_Pnd_Work_Yy;
    private DbsField pnd_Rep_Parm_Pnd_Work_End_Date_A;

    private DbsGroup pnd_Rep_Parm__R_Field_5;
    private DbsField pnd_Rep_Parm_Pnd_Work_Mmm;
    private DbsField pnd_Rep_Parm_Pnd_Fillera;
    private DbsField pnd_Rep_Parm_Pnd_Work_Ddd;
    private DbsField pnd_Rep_Parm_Pnd_Fillerb;
    private DbsField pnd_Rep_Parm_Pnd_Work_Yyy;

    private DbsGroup pnd_Local_Data;

    private DbsGroup pnd_Local_Data_Pnd_Work_Record;
    private DbsField pnd_Local_Data_Tbl_Wpid;

    private DbsGroup pnd_Local_Data__R_Field_6;
    private DbsField pnd_Local_Data_Tbl_Wpid_Action;
    private DbsField pnd_Local_Data_Tbl_Wpid_Lob;
    private DbsField pnd_Local_Data_Tbl_Wpid_Mbp;
    private DbsField pnd_Local_Data_Tbl_Wpid_Sbp;
    private DbsField pnd_Local_Data_Tbl_Wpid_Act;
    private DbsField pnd_Local_Data_Tbl_Status;
    private DbsField pnd_Local_Data_Tbl_Racf_Id;
    private DbsField pnd_Local_Data_Tbl_Empl_Name;
    private DbsField pnd_Local_Data_Tbl_Site_Id;
    private DbsField pnd_Local_Data_Tbl_Rqst_Type;
    private DbsField pnd_Local_Data_Pnd_Work_Admin_Status;
    private DbsField pnd_Local_Data_Pnd_Work_Admin_Unit_Cde;
    private DbsField pnd_Local_Data_Pnd_Site_Id;
    private DbsField pnd_Local_Data_Pnd_Run_Type;
    private DbsField pnd_Local_Data_Pnd_Work_Date_D;
    private DbsField pnd_Local_Data_Pnd_Rep_Empl_Name;
    private DbsField pnd_Local_Data_Pnd_Rep_Empl_Name_Racf;
    private DbsField pnd_Local_Data_Pnd_Work_Prcss_Id;
    private DbsField pnd_Local_Data_Pnd_Wpid_Code;
    private DbsField pnd_Local_Data_Pnd_Wpid_Desc;
    private DbsField pnd_Local_Data_Pnd_Work_Status;
    private DbsField pnd_Local_Data_Pnd_Work_Type;
    private DbsField pnd_Local_Data_Pnd_Total_New;
    private DbsField pnd_Local_Data_Pnd_Total_Logged;
    private DbsField pnd_Local_Data_Pnd_Wpid_Count;
    private DbsField pnd_Local_Data_Pnd_Wpid_New;
    private DbsField pnd_Local_Data_Pnd_Returned_Doc;
    private DbsField pnd_Local_Data_Pnd_Total_Returned_Doc;
    private DbsField pnd_Local_Data_Pnd_Total_Unclear;
    private DbsField pnd_Local_Data_Pnd_Wpid_Values;
    private DbsField pnd_Local_Data_Pnd_Counters;
    private DbsField pnd_Local_Data_Pnd_Log_Ret;
    private DbsField pnd_Local_Data_Pnd_Pos;
    private DbsField pnd_Local_Data_Pnd_End_Of_Data;

    private DataAccessProgramView vw_cwf_Master_Index;
    private DbsField cwf_Master_Index_Pin_Nbr;
    private DbsField cwf_Master_Index_Rqst_Log_Dte_Tme;

    private DbsGroup cwf_Master_Index__R_Field_7;
    private DbsField cwf_Master_Index_Rqst_Log_Index_Dte;
    private DbsField cwf_Master_Index_Rqst_Log_Index_Tme;
    private DbsField cwf_Master_Index_Orgnl_Unit_Cde;
    private DbsField cwf_Master_Index_Rqst_Log_Oprtr_Cde;
    private DbsField cwf_Master_Index_Last_Chnge_Unit_Cde;
    private DbsField cwf_Master_Index_Last_Chnge_Dte_Tme;

    private DbsGroup cwf_Master_Index__R_Field_8;
    private DbsField cwf_Master_Index_Last_Chnge_Dte_N;
    private DbsField cwf_Master_Index_Last_Chnge_Tme_N;
    private DbsField cwf_Master_Index_Last_Chnge_Oprtr_Cde;
    private DbsField cwf_Master_Index_Work_Prcss_Id;
    private DbsField cwf_Master_Index_Status_Cde;
    private DbsField cwf_Master_Index_Admin_Status_Cde;
    private DbsField cwf_Master_Index_Admin_Unit_Cde;
    private DbsField cwf_Master_Index_Extrnl_Pend_Rcv_Dte;
    private DbsField cwf_Master_Index_Actve_Ind;

    private DataAccessProgramView vw_cwf_Master_Log_View;
    private DbsField cwf_Master_Log_View_Rqst_Log_Dte_Tme;
    private DbsField cwf_Master_Log_View_Last_Chnge_Dte_Tme;

    private DataAccessProgramView vw_ncw_Master_Index;
    private DbsField ncw_Master_Index_Np_Pin;
    private DbsField ncw_Master_Index_Rqst_Log_Dte_Tme;

    private DbsGroup ncw_Master_Index__R_Field_9;
    private DbsField ncw_Master_Index_Rqst_Log_Index_Dte;
    private DbsField ncw_Master_Index_Rqst_Log_Index_Tme;
    private DbsField ncw_Master_Index_Rqst_Log_Unit_Cde;
    private DbsField ncw_Master_Index_Rqst_Log_Oprtr_Cde;
    private DbsField ncw_Master_Index_System_Updte_Dte_Tme;
    private DbsField ncw_Master_Index_Modify_Unit_Cde;
    private DbsField ncw_Master_Index_Modify_Oprtr_Cde;
    private DbsField ncw_Master_Index_Work_Prcss_Id;
    private DbsField ncw_Master_Index_Status_Cde;
    private DbsField ncw_Master_Index_Admin_Status_Cde;
    private DbsField ncw_Master_Index_Admin_Unit_Cde;
    private DbsField ncw_Master_Index_Extrnl_Pend_Rcv_Dte;
    private DbsField ncw_Master_Index_Actve_Ind;

    private DataAccessProgramView vw_ncw_Master_Log_View;
    private DbsField ncw_Master_Log_View_Rqst_Log_Dte_Tme;
    private DbsField ncw_Master_Log_View_System_Updte_Dte_Tme;
    private DbsField pnd_Page;
    private DbsField pnd_Page2;
    private DbsField pnd_Page3;
    private DbsField pnd_Page4;
    private DbsField pnd_New_Racf;
    private DbsField pnd_New_Unit;
    private DbsField pnd_Yyyymmdd;

    private DbsGroup pnd_Yyyymmdd__R_Field_10;
    private DbsField pnd_Yyyymmdd_Pnd_Century;
    private DbsField pnd_Yyyymmdd_Pnd_Yy;
    private DbsField pnd_Yyyymmdd_Pnd_Mm;
    private DbsField pnd_Yyyymmdd_Pnd_Dd;
    private DbsField pnd_Report_No;
    private DbsField pnd_Racf_Id;
    private DbsField pnd_Floor;
    private DbsField pnd_Bldg;
    private DbsField pnd_Drop_Off;
    private DbsField pnd_Unit_Name;
    private DbsField pnd_Comp_Date;
    private DbsField pnd_Reccount;
    private DbsField pnd_Work_Comp_Date;
    private DbsField pnd_Oprtr_Cde;
    private DbsField pnd_Day_Of_Week;
    private DbsField pnd_Days_To_Subtract;

    private DbsGroup pnd_Days_To_Subtract__R_Field_11;
    private DbsField pnd_Days_To_Subtract_Pnd_Days_To_Subtract_A;
    private DbsField pnd_Crc_Unit;
    private DbsField pnd_Owner_Unit_Cde;
    private DbsField pnd_Pend_Dte_Tme;

    private DbsGroup pnd_Pend_Dte_Tme__R_Field_12;
    private DbsField pnd_Pend_Dte_Tme_Pnd_Pend_Dte;
    private DbsField pnd_I;
    private DbsField pnd_Hold_Ctr;

    private DbsGroup pnd_H1;
    private DbsField pnd_H1_Pnd_H1_Pgm;
    private DbsField pnd_H1_Pnd_H1_Env;
    private DbsField pnd_H1_Pnd_H1_Fill_1;
    private DbsField pnd_H1_Pnd_H1_Title;
    private DbsField pnd_H1_Pnd_H1_Fill_2;
    private DbsField pnd_H1_Pnd_H1_Page;

    private DbsGroup pnd_H1__R_Field_13;
    private DbsField pnd_H1_Pnd_Hdr_1;

    private DbsGroup pnd_H2;
    private DbsField pnd_H2_Pnd_H2_Date;
    private DbsField pnd_H2_Pnd_H2_Fill_1;
    private DbsField pnd_H2_Pnd_H2_Title;
    private DbsField pnd_H2_Pnd_H2_Fill_2;
    private DbsField pnd_H2_Pnd_H2_Time;

    private DbsGroup pnd_H2__R_Field_14;
    private DbsField pnd_H2_Pnd_Hdr_2;

    private DbsGroup pnd_H5;
    private DbsField pnd_H5_Pnd_H5_Title_1;
    private DbsField pnd_H5_Pnd_H5_Start_Date;
    private DbsField pnd_H5_Pnd_Hdr_Fill_1;
    private DbsField pnd_H5_Pnd_H5_Title_2;
    private DbsField pnd_H5_Pnd_H5_End_Date;

    private DbsGroup pnd_H5__R_Field_15;
    private DbsField pnd_H5_Pnd_Hdr_5;

    private DbsGroup pnd_H8;
    private DbsField pnd_H8_Pnd_H8_Fill_1;
    private DbsField pnd_H8_Pnd_H8_Desc_2;
    private DbsField pnd_H8_Pnd_H8_Desc_3;

    private DbsGroup pnd_H8__R_Field_16;
    private DbsField pnd_H8_Pnd_Hdr_8;

    private DbsGroup pnd_H9;
    private DbsField pnd_H9_Pnd_H9_Desc_1;
    private DbsField pnd_H9_Pnd_H9_Desc_2;
    private DbsField pnd_H9_Pnd_H9_Desc_3;

    private DbsGroup pnd_H9__R_Field_17;
    private DbsField pnd_H9_Pnd_Hdr_9;

    private DbsGroup pnd_H10;
    private DbsField pnd_H10_Pnd_H10_Fill_1;
    private DbsField pnd_H10_Pnd_H10_Fill_2;
    private DbsField pnd_H10_Pnd_H10_Fill_3;
    private DbsField pnd_H10_Pnd_H10_Fill_4;

    private DbsGroup pnd_H10__R_Field_18;
    private DbsField pnd_H10_Pnd_Hdr_10;
    private DbsField pnd_Hdr_11;

    private DbsGroup pnd_H12;
    private DbsField pnd_H12_Pnd_H12_Fill_1;
    private DbsField pnd_H12_Pnd_H12_Fill_2;
    private DbsField pnd_H12_Pnd_H12_Fill_3;

    private DbsGroup pnd_H12__R_Field_19;
    private DbsField pnd_H12_Pnd_Hdr_12;

    private DbsGroup pnd_H13;
    private DbsField pnd_H13_Pnd_H13_Fill_1;
    private DbsField pnd_H13_Pnd_H13_Fill_2;
    private DbsField pnd_H13_Pnd_H13_Fill_3;

    private DbsGroup pnd_H13__R_Field_20;
    private DbsField pnd_H13_Pnd_Hdr_13;

    private DbsGroup pnd_H14;
    private DbsField pnd_H14_Pnd_H14_Fill_1;
    private DbsField pnd_H14_Pnd_H14_Fill_2;
    private DbsField pnd_H14_Pnd_H14_Fill_3;

    private DbsGroup pnd_H14__R_Field_21;
    private DbsField pnd_H14_Pnd_Hdr_14;

    private DbsGroup pnd_D1;
    private DbsField pnd_D1_Pnd_Dtl1_Wpid_Code;
    private DbsField pnd_D1_Pnd_Dtl1_Fill_1;
    private DbsField pnd_D1_Pnd_Dtl1_Wpid_Desc;
    private DbsField pnd_D1_Pnd_Dtl1_Fill_2;
    private DbsField pnd_D1_Pnd_Dtl1_Owner_Cde;
    private DbsField pnd_D1_Pnd_Dtl1_Fill_3;
    private DbsField pnd_D1_Pnd_Dtl1_Wpid_New;
    private DbsField pnd_D1_Pnd_Dtl1_Fill_4;
    private DbsField pnd_D1_Pnd_Dtl1_Retrn_Doc;
    private DbsField pnd_D1_Pnd_Dtl1_Fill_5;
    private DbsField pnd_D1_Pnd_Dtl1_Wpid_Cnt;

    private DbsGroup pnd_D1__R_Field_22;
    private DbsField pnd_D1_Pnd_Dtl_1;

    private DbsGroup pnd_T1;
    private DbsField pnd_T1_Pnd_Tot1_Fill_1;
    private DbsField pnd_T1_Pnd_Tot1_Fill_2;
    private DbsField pnd_T1_Pnd_Tot1_Total_New;
    private DbsField pnd_T1_Pnd_Tot1_Fill_3;
    private DbsField pnd_T1_Pnd_Tot1_Retrn_Doc;
    private DbsField pnd_T1_Pnd_Tot1_Fill_4;
    private DbsField pnd_T1_Pnd_Tot1_Logged;

    private DbsGroup pnd_T1__R_Field_23;
    private DbsField pnd_T1_Pnd_Tot_1;

    private DbsGroup pnd_T2;
    private DbsField pnd_T2_Pnd_Tot2_Fill_1;
    private DbsField pnd_T2_Pnd_Tot2_Fill_2;
    private DbsField pnd_T2_Pnd_Tot2_Unclear;

    private DbsGroup pnd_T2__R_Field_24;
    private DbsField pnd_T2_Pnd_Tot_2;

    private DbsGroup pnd_T3;
    private DbsField pnd_T3_Pnd_Tot3_Fill_1;

    private DbsGroup pnd_T3_Pnd_Tot3_Array;
    private DbsField pnd_T3_Pnd_Tot3_Ctrs;
    private DbsField pnd_T3_Pnd_Tot3_Fill_2;
    private DbsField pnd_T3_Pnd_Tot3_Message;

    private DbsGroup pnd_T3__R_Field_25;
    private DbsField pnd_T3_Pnd_Tot_3;
    private DbsField pnd_Override_Sw;
    private DbsField pnd_Override_Key;
    private DbsField pnd_Npin_Count;
    private DbsField pnd_Rqst_Log_Fl;
    private DbsField pnd_Mit_Rqst_Routing_Key;

    private DbsGroup pnd_Mit_Rqst_Routing_Key__R_Field_26;
    private DbsField pnd_Mit_Rqst_Routing_Key_Rqst_Log_Dte_Tme;
    private DbsField pnd_Mit_Rqst_Routing_Key_Last_Chnge_Dte_Tme;
    private DbsField pnd_Nmit_Rqst_Routing_Key;

    private DbsGroup pnd_Nmit_Rqst_Routing_Key__R_Field_27;
    private DbsField pnd_Nmit_Rqst_Routing_Key_Rqst_Log_Dte_Tme;
    private DbsField pnd_Nmit_Rqst_Routing_Key_Strtng_Event_Dte_Tme;

    private DbsRecord internalLoopRecord;
    private DbsField sort01Tbl_WpidOld;
    private DbsField sort01Tbl_WpidCount861;
    private DbsField sort01Tbl_WpidCount;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCwfa2100 = new PdaCwfa2100(localVariables);
        pdaCwfa2101 = new PdaCwfa2101(localVariables);
        pdaCdaobjr = new PdaCdaobjr(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);

        // Local Variables

        pnd_Rep_Parm = localVariables.newGroupInRecord("pnd_Rep_Parm", "#REP-PARM");
        pnd_Rep_Parm_Pnd_Rep_Racf_Id = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_Racf_Id", "#REP-RACF-ID", FieldType.STRING, 8);
        pnd_Rep_Parm_Pnd_Rep_Unit_Cde = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Rep_Unit_Cde", "#REP-UNIT-CDE", FieldType.STRING, 8);
        pnd_Rep_Parm_Pnd_Start_Dte_Tme = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Start_Dte_Tme", "#START-DTE-TME", FieldType.TIME);
        pnd_Rep_Parm_Pnd_Start_Date = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Start_Date", "#START-DATE", FieldType.STRING, 8);

        pnd_Rep_Parm__R_Field_1 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_1", "REDEFINE", pnd_Rep_Parm_Pnd_Start_Date);
        pnd_Rep_Parm_Pnd_Start_Date_N = pnd_Rep_Parm__R_Field_1.newFieldInGroup("pnd_Rep_Parm_Pnd_Start_Date_N", "#START-DATE-N", FieldType.NUMERIC, 8);

        pnd_Rep_Parm__R_Field_2 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_2", "REDEFINE", pnd_Rep_Parm_Pnd_Start_Date);
        pnd_Rep_Parm_Pnd_Start_Date_Fill = pnd_Rep_Parm__R_Field_2.newFieldInGroup("pnd_Rep_Parm_Pnd_Start_Date_Fill", "#START-DATE-FILL", FieldType.STRING, 
            6);
        pnd_Rep_Parm_Pnd_Start_Date_Dd = pnd_Rep_Parm__R_Field_2.newFieldInGroup("pnd_Rep_Parm_Pnd_Start_Date_Dd", "#START-DATE-DD", FieldType.STRING, 
            2);
        pnd_Rep_Parm_Pnd_End_Date = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_End_Date", "#END-DATE", FieldType.STRING, 8);

        pnd_Rep_Parm__R_Field_3 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_3", "REDEFINE", pnd_Rep_Parm_Pnd_End_Date);
        pnd_Rep_Parm_Pnd_End_Date_N = pnd_Rep_Parm__R_Field_3.newFieldInGroup("pnd_Rep_Parm_Pnd_End_Date_N", "#END-DATE-N", FieldType.NUMERIC, 8);
        pnd_Rep_Parm_Pnd_Work_Start_Date_A = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Start_Date_A", "#WORK-START-DATE-A", FieldType.STRING, 
            8);

        pnd_Rep_Parm__R_Field_4 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_4", "REDEFINE", pnd_Rep_Parm_Pnd_Work_Start_Date_A);
        pnd_Rep_Parm_Pnd_Work_Mm = pnd_Rep_Parm__R_Field_4.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Mm", "#WORK-MM", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Filler1 = pnd_Rep_Parm__R_Field_4.newFieldInGroup("pnd_Rep_Parm_Pnd_Filler1", "#FILLER1", FieldType.STRING, 1);
        pnd_Rep_Parm_Pnd_Work_Dd = pnd_Rep_Parm__R_Field_4.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Dd", "#WORK-DD", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Filler2 = pnd_Rep_Parm__R_Field_4.newFieldInGroup("pnd_Rep_Parm_Pnd_Filler2", "#FILLER2", FieldType.STRING, 1);
        pnd_Rep_Parm_Pnd_Work_Yy = pnd_Rep_Parm__R_Field_4.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Yy", "#WORK-YY", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Work_End_Date_A = pnd_Rep_Parm.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_End_Date_A", "#WORK-END-DATE-A", FieldType.STRING, 8);

        pnd_Rep_Parm__R_Field_5 = pnd_Rep_Parm.newGroupInGroup("pnd_Rep_Parm__R_Field_5", "REDEFINE", pnd_Rep_Parm_Pnd_Work_End_Date_A);
        pnd_Rep_Parm_Pnd_Work_Mmm = pnd_Rep_Parm__R_Field_5.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Mmm", "#WORK-MMM", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Fillera = pnd_Rep_Parm__R_Field_5.newFieldInGroup("pnd_Rep_Parm_Pnd_Fillera", "#FILLERA", FieldType.STRING, 1);
        pnd_Rep_Parm_Pnd_Work_Ddd = pnd_Rep_Parm__R_Field_5.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Ddd", "#WORK-DDD", FieldType.STRING, 2);
        pnd_Rep_Parm_Pnd_Fillerb = pnd_Rep_Parm__R_Field_5.newFieldInGroup("pnd_Rep_Parm_Pnd_Fillerb", "#FILLERB", FieldType.STRING, 1);
        pnd_Rep_Parm_Pnd_Work_Yyy = pnd_Rep_Parm__R_Field_5.newFieldInGroup("pnd_Rep_Parm_Pnd_Work_Yyy", "#WORK-YYY", FieldType.STRING, 2);

        pnd_Local_Data = localVariables.newGroupInRecord("pnd_Local_Data", "#LOCAL-DATA");

        pnd_Local_Data_Pnd_Work_Record = pnd_Local_Data.newGroupInGroup("pnd_Local_Data_Pnd_Work_Record", "#WORK-RECORD");
        pnd_Local_Data_Tbl_Wpid = pnd_Local_Data_Pnd_Work_Record.newFieldInGroup("pnd_Local_Data_Tbl_Wpid", "TBL-WPID", FieldType.STRING, 6);

        pnd_Local_Data__R_Field_6 = pnd_Local_Data_Pnd_Work_Record.newGroupInGroup("pnd_Local_Data__R_Field_6", "REDEFINE", pnd_Local_Data_Tbl_Wpid);
        pnd_Local_Data_Tbl_Wpid_Action = pnd_Local_Data__R_Field_6.newFieldInGroup("pnd_Local_Data_Tbl_Wpid_Action", "TBL-WPID-ACTION", FieldType.STRING, 
            1);
        pnd_Local_Data_Tbl_Wpid_Lob = pnd_Local_Data__R_Field_6.newFieldInGroup("pnd_Local_Data_Tbl_Wpid_Lob", "TBL-WPID-LOB", FieldType.STRING, 2);
        pnd_Local_Data_Tbl_Wpid_Mbp = pnd_Local_Data__R_Field_6.newFieldInGroup("pnd_Local_Data_Tbl_Wpid_Mbp", "TBL-WPID-MBP", FieldType.STRING, 1);
        pnd_Local_Data_Tbl_Wpid_Sbp = pnd_Local_Data__R_Field_6.newFieldInGroup("pnd_Local_Data_Tbl_Wpid_Sbp", "TBL-WPID-SBP", FieldType.STRING, 2);
        pnd_Local_Data_Tbl_Wpid_Act = pnd_Local_Data_Pnd_Work_Record.newFieldInGroup("pnd_Local_Data_Tbl_Wpid_Act", "TBL-WPID-ACT", FieldType.STRING, 
            1);
        pnd_Local_Data_Tbl_Status = pnd_Local_Data_Pnd_Work_Record.newFieldInGroup("pnd_Local_Data_Tbl_Status", "TBL-STATUS", FieldType.STRING, 4);
        pnd_Local_Data_Tbl_Racf_Id = pnd_Local_Data_Pnd_Work_Record.newFieldInGroup("pnd_Local_Data_Tbl_Racf_Id", "TBL-RACF-ID", FieldType.STRING, 8);
        pnd_Local_Data_Tbl_Empl_Name = pnd_Local_Data_Pnd_Work_Record.newFieldInGroup("pnd_Local_Data_Tbl_Empl_Name", "TBL-EMPL-NAME", FieldType.STRING, 
            30);
        pnd_Local_Data_Tbl_Site_Id = pnd_Local_Data_Pnd_Work_Record.newFieldInGroup("pnd_Local_Data_Tbl_Site_Id", "TBL-SITE-ID", FieldType.STRING, 2);
        pnd_Local_Data_Tbl_Rqst_Type = pnd_Local_Data_Pnd_Work_Record.newFieldInGroup("pnd_Local_Data_Tbl_Rqst_Type", "TBL-RQST-TYPE", FieldType.STRING, 
            1);
        pnd_Local_Data_Pnd_Work_Admin_Status = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Work_Admin_Status", "#WORK-ADMIN-STATUS", FieldType.STRING, 
            4);
        pnd_Local_Data_Pnd_Work_Admin_Unit_Cde = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Work_Admin_Unit_Cde", "#WORK-ADMIN-UNIT-CDE", FieldType.STRING, 
            8);
        pnd_Local_Data_Pnd_Site_Id = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Site_Id", "#SITE-ID", FieldType.STRING, 2);
        pnd_Local_Data_Pnd_Run_Type = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Run_Type", "#RUN-TYPE", FieldType.STRING, 7);
        pnd_Local_Data_Pnd_Work_Date_D = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Work_Date_D", "#WORK-DATE-D", FieldType.DATE);
        pnd_Local_Data_Pnd_Rep_Empl_Name = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Rep_Empl_Name", "#REP-EMPL-NAME", FieldType.STRING, 30);
        pnd_Local_Data_Pnd_Rep_Empl_Name_Racf = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Rep_Empl_Name_Racf", "#REP-EMPL-NAME-RACF", FieldType.STRING, 
            50);
        pnd_Local_Data_Pnd_Work_Prcss_Id = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Work_Prcss_Id", "#WORK-PRCSS-ID", FieldType.STRING, 6);
        pnd_Local_Data_Pnd_Wpid_Code = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Wpid_Code", "#WPID-CODE", FieldType.STRING, 6);
        pnd_Local_Data_Pnd_Wpid_Desc = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Wpid_Desc", "#WPID-DESC", FieldType.STRING, 45);
        pnd_Local_Data_Pnd_Work_Status = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Work_Status", "#WORK-STATUS", FieldType.STRING, 4);
        pnd_Local_Data_Pnd_Work_Type = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Work_Type", "#WORK-TYPE", FieldType.STRING, 1);
        pnd_Local_Data_Pnd_Total_New = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Total_New", "#TOTAL-NEW", FieldType.NUMERIC, 5);
        pnd_Local_Data_Pnd_Total_Logged = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Total_Logged", "#TOTAL-LOGGED", FieldType.NUMERIC, 5);
        pnd_Local_Data_Pnd_Wpid_Count = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Wpid_Count", "#WPID-COUNT", FieldType.NUMERIC, 5);
        pnd_Local_Data_Pnd_Wpid_New = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Wpid_New", "#WPID-NEW", FieldType.NUMERIC, 5);
        pnd_Local_Data_Pnd_Returned_Doc = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Returned_Doc", "#RETURNED-DOC", FieldType.NUMERIC, 5);
        pnd_Local_Data_Pnd_Total_Returned_Doc = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Total_Returned_Doc", "#TOTAL-RETURNED-DOC", FieldType.NUMERIC, 
            5);
        pnd_Local_Data_Pnd_Total_Unclear = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Total_Unclear", "#TOTAL-UNCLEAR", FieldType.NUMERIC, 5);
        pnd_Local_Data_Pnd_Wpid_Values = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Wpid_Values", "#WPID-VALUES", FieldType.STRING, 7);
        pnd_Local_Data_Pnd_Counters = pnd_Local_Data.newFieldArrayInGroup("pnd_Local_Data_Pnd_Counters", "#COUNTERS", FieldType.PACKED_DECIMAL, 5, new 
            DbsArrayController(1, 2, 1, 8));
        pnd_Local_Data_Pnd_Log_Ret = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Log_Ret", "#LOG-RET", FieldType.PACKED_DECIMAL, 1);
        pnd_Local_Data_Pnd_Pos = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_Pos", "#POS", FieldType.PACKED_DECIMAL, 1);
        pnd_Local_Data_Pnd_End_Of_Data = pnd_Local_Data.newFieldInGroup("pnd_Local_Data_Pnd_End_Of_Data", "#END-OF-DATA", FieldType.BOOLEAN, 1);

        vw_cwf_Master_Index = new DataAccessProgramView(new NameInfo("vw_cwf_Master_Index", "CWF-MASTER-INDEX"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        cwf_Master_Index_Pin_Nbr = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "PIN_NBR");
        cwf_Master_Index_Pin_Nbr.setDdmHeader("PIN");
        cwf_Master_Index_Rqst_Log_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Master_Index_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");

        cwf_Master_Index__R_Field_7 = vw_cwf_Master_Index.getRecord().newGroupInGroup("cwf_Master_Index__R_Field_7", "REDEFINE", cwf_Master_Index_Rqst_Log_Dte_Tme);
        cwf_Master_Index_Rqst_Log_Index_Dte = cwf_Master_Index__R_Field_7.newFieldInGroup("cwf_Master_Index_Rqst_Log_Index_Dte", "RQST-LOG-INDEX-DTE", 
            FieldType.STRING, 8);
        cwf_Master_Index_Rqst_Log_Index_Tme = cwf_Master_Index__R_Field_7.newFieldInGroup("cwf_Master_Index_Rqst_Log_Index_Tme", "RQST-LOG-INDEX-TME", 
            FieldType.STRING, 7);
        cwf_Master_Index_Orgnl_Unit_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ORGNL_UNIT_CDE");
        cwf_Master_Index_Orgnl_Unit_Cde.setDdmHeader("LOG/UNIT");
        cwf_Master_Index_Rqst_Log_Oprtr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_LOG_OPRTR_CDE");
        cwf_Master_Index_Rqst_Log_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        cwf_Master_Index_Last_Chnge_Unit_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Last_Chnge_Unit_Cde", "LAST-CHNGE-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_CHNGE_UNIT_CDE");
        cwf_Master_Index_Last_Chnge_Unit_Cde.setDdmHeader("LAST/CHANGE/UNIT");
        cwf_Master_Index_Last_Chnge_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "LAST_CHNGE_DTE_TME");
        cwf_Master_Index_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");

        cwf_Master_Index__R_Field_8 = vw_cwf_Master_Index.getRecord().newGroupInGroup("cwf_Master_Index__R_Field_8", "REDEFINE", cwf_Master_Index_Last_Chnge_Dte_Tme);
        cwf_Master_Index_Last_Chnge_Dte_N = cwf_Master_Index__R_Field_8.newFieldInGroup("cwf_Master_Index_Last_Chnge_Dte_N", "LAST-CHNGE-DTE-N", FieldType.NUMERIC, 
            8);
        cwf_Master_Index_Last_Chnge_Tme_N = cwf_Master_Index__R_Field_8.newFieldInGroup("cwf_Master_Index_Last_Chnge_Tme_N", "LAST-CHNGE-TME-N", FieldType.NUMERIC, 
            7);
        cwf_Master_Index_Last_Chnge_Oprtr_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Last_Chnge_Oprtr_Cde", "LAST-CHNGE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_CHNGE_OPRTR_CDE");
        cwf_Master_Index_Last_Chnge_Oprtr_Cde.setDdmHeader("LOG CREATE/OPERATOR");
        cwf_Master_Index_Work_Prcss_Id = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        cwf_Master_Index_Work_Prcss_Id.setDdmHeader("WORK/ID");
        cwf_Master_Index_Status_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Status_Cde", "STATUS-CDE", FieldType.STRING, 4, 
            RepeatingFieldStrategy.None, "STATUS_CDE");
        cwf_Master_Index_Admin_Status_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "ADMIN_STATUS_CDE");
        cwf_Master_Index_Admin_Unit_Cde = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ADMIN_UNIT_CDE");
        cwf_Master_Index_Extrnl_Pend_Rcv_Dte = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Extrnl_Pend_Rcv_Dte", "EXTRNL-PEND-RCV-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "EXTRNL_PEND_RCV_DTE");
        cwf_Master_Index_Extrnl_Pend_Rcv_Dte.setDdmHeader("LAST UNPEND DATE");
        cwf_Master_Index_Actve_Ind = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTVE_IND");
        registerRecord(vw_cwf_Master_Index);

        vw_cwf_Master_Log_View = new DataAccessProgramView(new NameInfo("vw_cwf_Master_Log_View", "CWF-MASTER-LOG-VIEW"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        cwf_Master_Log_View_Rqst_Log_Dte_Tme = vw_cwf_Master_Log_View.getRecord().newFieldInGroup("cwf_Master_Log_View_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Master_Log_View_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        cwf_Master_Log_View_Last_Chnge_Dte_Tme = vw_cwf_Master_Log_View.getRecord().newFieldInGroup("cwf_Master_Log_View_Last_Chnge_Dte_Tme", "LAST-CHNGE-DTE-TME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "LAST_CHNGE_DTE_TME");
        cwf_Master_Log_View_Last_Chnge_Dte_Tme.setDdmHeader("LOG CREATE/DATE-TIME");
        registerRecord(vw_cwf_Master_Log_View);

        vw_ncw_Master_Index = new DataAccessProgramView(new NameInfo("vw_ncw_Master_Index", "NCW-MASTER-INDEX"), "NCW_MASTER_INDEX", "NCW_MASTER_INDEX");
        ncw_Master_Index_Np_Pin = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Np_Pin", "NP-PIN", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "NP_PIN");
        ncw_Master_Index_Rqst_Log_Dte_Tme = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");

        ncw_Master_Index__R_Field_9 = vw_ncw_Master_Index.getRecord().newGroupInGroup("ncw_Master_Index__R_Field_9", "REDEFINE", ncw_Master_Index_Rqst_Log_Dte_Tme);
        ncw_Master_Index_Rqst_Log_Index_Dte = ncw_Master_Index__R_Field_9.newFieldInGroup("ncw_Master_Index_Rqst_Log_Index_Dte", "RQST-LOG-INDEX-DTE", 
            FieldType.STRING, 8);
        ncw_Master_Index_Rqst_Log_Index_Tme = ncw_Master_Index__R_Field_9.newFieldInGroup("ncw_Master_Index_Rqst_Log_Index_Tme", "RQST-LOG-INDEX-TME", 
            FieldType.STRING, 7);
        ncw_Master_Index_Rqst_Log_Unit_Cde = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Rqst_Log_Unit_Cde", "RQST-LOG-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_LOG_UNIT_CDE");
        ncw_Master_Index_Rqst_Log_Unit_Cde.setDdmHeader("LOG/UNIT");
        ncw_Master_Index_Rqst_Log_Oprtr_Cde = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_LOG_OPRTR_CDE");
        ncw_Master_Index_Rqst_Log_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        ncw_Master_Index_System_Updte_Dte_Tme = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_System_Updte_Dte_Tme", "SYSTEM-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "SYSTEM_UPDTE_DTE_TME");
        ncw_Master_Index_Modify_Unit_Cde = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Modify_Unit_Cde", "MODIFY-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "MODIFY_UNIT_CDE");
        ncw_Master_Index_Modify_Oprtr_Cde = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Modify_Oprtr_Cde", "MODIFY-OPRTR-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "MODIFY_OPRTR_CDE");
        ncw_Master_Index_Work_Prcss_Id = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        ncw_Master_Index_Work_Prcss_Id.setDdmHeader("WORK/ID");
        ncw_Master_Index_Status_Cde = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Status_Cde", "STATUS-CDE", FieldType.STRING, 4, 
            RepeatingFieldStrategy.None, "STATUS_CDE");
        ncw_Master_Index_Admin_Status_Cde = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "ADMIN_STATUS_CDE");
        ncw_Master_Index_Admin_Unit_Cde = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ADMIN_UNIT_CDE");
        ncw_Master_Index_Extrnl_Pend_Rcv_Dte = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Extrnl_Pend_Rcv_Dte", "EXTRNL-PEND-RCV-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "EXTRNL_PEND_RCV_DTE");
        ncw_Master_Index_Actve_Ind = vw_ncw_Master_Index.getRecord().newFieldInGroup("ncw_Master_Index_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTVE_IND");
        registerRecord(vw_ncw_Master_Index);

        vw_ncw_Master_Log_View = new DataAccessProgramView(new NameInfo("vw_ncw_Master_Log_View", "NCW-MASTER-LOG-VIEW"), "NCW_MASTER_INDEX", "NCW_MASTER_INDEX");
        ncw_Master_Log_View_Rqst_Log_Dte_Tme = vw_ncw_Master_Log_View.getRecord().newFieldInGroup("ncw_Master_Log_View_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        ncw_Master_Log_View_System_Updte_Dte_Tme = vw_ncw_Master_Log_View.getRecord().newFieldInGroup("ncw_Master_Log_View_System_Updte_Dte_Tme", "SYSTEM-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "SYSTEM_UPDTE_DTE_TME");
        registerRecord(vw_ncw_Master_Log_View);

        pnd_Page = localVariables.newFieldInRecord("pnd_Page", "#PAGE", FieldType.NUMERIC, 5);
        pnd_Page2 = localVariables.newFieldInRecord("pnd_Page2", "#PAGE2", FieldType.NUMERIC, 5);
        pnd_Page3 = localVariables.newFieldInRecord("pnd_Page3", "#PAGE3", FieldType.NUMERIC, 5);
        pnd_Page4 = localVariables.newFieldInRecord("pnd_Page4", "#PAGE4", FieldType.NUMERIC, 5);
        pnd_New_Racf = localVariables.newFieldInRecord("pnd_New_Racf", "#NEW-RACF", FieldType.BOOLEAN, 1);
        pnd_New_Unit = localVariables.newFieldInRecord("pnd_New_Unit", "#NEW-UNIT", FieldType.BOOLEAN, 1);
        pnd_Yyyymmdd = localVariables.newFieldInRecord("pnd_Yyyymmdd", "#YYYYMMDD", FieldType.STRING, 8);

        pnd_Yyyymmdd__R_Field_10 = localVariables.newGroupInRecord("pnd_Yyyymmdd__R_Field_10", "REDEFINE", pnd_Yyyymmdd);
        pnd_Yyyymmdd_Pnd_Century = pnd_Yyyymmdd__R_Field_10.newFieldInGroup("pnd_Yyyymmdd_Pnd_Century", "#CENTURY", FieldType.STRING, 2);
        pnd_Yyyymmdd_Pnd_Yy = pnd_Yyyymmdd__R_Field_10.newFieldInGroup("pnd_Yyyymmdd_Pnd_Yy", "#YY", FieldType.STRING, 2);
        pnd_Yyyymmdd_Pnd_Mm = pnd_Yyyymmdd__R_Field_10.newFieldInGroup("pnd_Yyyymmdd_Pnd_Mm", "#MM", FieldType.STRING, 2);
        pnd_Yyyymmdd_Pnd_Dd = pnd_Yyyymmdd__R_Field_10.newFieldInGroup("pnd_Yyyymmdd_Pnd_Dd", "#DD", FieldType.STRING, 2);
        pnd_Report_No = localVariables.newFieldInRecord("pnd_Report_No", "#REPORT-NO", FieldType.NUMERIC, 2);
        pnd_Racf_Id = localVariables.newFieldInRecord("pnd_Racf_Id", "#RACF-ID", FieldType.STRING, 8);
        pnd_Floor = localVariables.newFieldInRecord("pnd_Floor", "#FLOOR", FieldType.NUMERIC, 2);
        pnd_Bldg = localVariables.newFieldInRecord("pnd_Bldg", "#BLDG", FieldType.STRING, 3);
        pnd_Drop_Off = localVariables.newFieldInRecord("pnd_Drop_Off", "#DROP-OFF", FieldType.STRING, 2);
        pnd_Unit_Name = localVariables.newFieldInRecord("pnd_Unit_Name", "#UNIT-NAME", FieldType.STRING, 45);
        pnd_Comp_Date = localVariables.newFieldInRecord("pnd_Comp_Date", "#COMP-DATE", FieldType.STRING, 8);
        pnd_Reccount = localVariables.newFieldInRecord("pnd_Reccount", "#RECCOUNT", FieldType.NUMERIC, 7);
        pnd_Work_Comp_Date = localVariables.newFieldInRecord("pnd_Work_Comp_Date", "#WORK-COMP-DATE", FieldType.DATE);
        pnd_Oprtr_Cde = localVariables.newFieldInRecord("pnd_Oprtr_Cde", "#OPRTR-CDE", FieldType.STRING, 8);
        pnd_Day_Of_Week = localVariables.newFieldInRecord("pnd_Day_Of_Week", "#DAY-OF-WEEK", FieldType.STRING, 3);
        pnd_Days_To_Subtract = localVariables.newFieldInRecord("pnd_Days_To_Subtract", "#DAYS-TO-SUBTRACT", FieldType.NUMERIC, 2);

        pnd_Days_To_Subtract__R_Field_11 = localVariables.newGroupInRecord("pnd_Days_To_Subtract__R_Field_11", "REDEFINE", pnd_Days_To_Subtract);
        pnd_Days_To_Subtract_Pnd_Days_To_Subtract_A = pnd_Days_To_Subtract__R_Field_11.newFieldInGroup("pnd_Days_To_Subtract_Pnd_Days_To_Subtract_A", 
            "#DAYS-TO-SUBTRACT-A", FieldType.STRING, 2);
        pnd_Crc_Unit = localVariables.newFieldInRecord("pnd_Crc_Unit", "#CRC-UNIT", FieldType.STRING, 8);
        pnd_Owner_Unit_Cde = localVariables.newFieldInRecord("pnd_Owner_Unit_Cde", "#OWNER-UNIT-CDE", FieldType.STRING, 8);
        pnd_Pend_Dte_Tme = localVariables.newFieldInRecord("pnd_Pend_Dte_Tme", "#PEND-DTE-TME", FieldType.STRING, 15);

        pnd_Pend_Dte_Tme__R_Field_12 = localVariables.newGroupInRecord("pnd_Pend_Dte_Tme__R_Field_12", "REDEFINE", pnd_Pend_Dte_Tme);
        pnd_Pend_Dte_Tme_Pnd_Pend_Dte = pnd_Pend_Dte_Tme__R_Field_12.newFieldInGroup("pnd_Pend_Dte_Tme_Pnd_Pend_Dte", "#PEND-DTE", FieldType.STRING, 8);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 1);
        pnd_Hold_Ctr = localVariables.newFieldInRecord("pnd_Hold_Ctr", "#HOLD-CTR", FieldType.NUMERIC, 5);

        pnd_H1 = localVariables.newGroupInRecord("pnd_H1", "#H1");
        pnd_H1_Pnd_H1_Pgm = pnd_H1.newFieldInGroup("pnd_H1_Pnd_H1_Pgm", "#H1-PGM", FieldType.STRING, 9);
        pnd_H1_Pnd_H1_Env = pnd_H1.newFieldInGroup("pnd_H1_Pnd_H1_Env", "#H1-ENV", FieldType.STRING, 10);
        pnd_H1_Pnd_H1_Fill_1 = pnd_H1.newFieldInGroup("pnd_H1_Pnd_H1_Fill_1", "#H1-FILL-1", FieldType.STRING, 7);
        pnd_H1_Pnd_H1_Title = pnd_H1.newFieldInGroup("pnd_H1_Pnd_H1_Title", "#H1-TITLE", FieldType.STRING, 29);
        pnd_H1_Pnd_H1_Fill_2 = pnd_H1.newFieldInGroup("pnd_H1_Pnd_H1_Fill_2", "#H1-FILL-2", FieldType.STRING, 14);
        pnd_H1_Pnd_H1_Page = pnd_H1.newFieldInGroup("pnd_H1_Pnd_H1_Page", "#H1-PAGE", FieldType.STRING, 10);

        pnd_H1__R_Field_13 = localVariables.newGroupInRecord("pnd_H1__R_Field_13", "REDEFINE", pnd_H1);
        pnd_H1_Pnd_Hdr_1 = pnd_H1__R_Field_13.newFieldInGroup("pnd_H1_Pnd_Hdr_1", "#HDR-1", FieldType.STRING, 79);

        pnd_H2 = localVariables.newGroupInRecord("pnd_H2", "#H2");
        pnd_H2_Pnd_H2_Date = pnd_H2.newFieldInGroup("pnd_H2_Pnd_H2_Date", "#H2-DATE", FieldType.STRING, 12);
        pnd_H2_Pnd_H2_Fill_1 = pnd_H2.newFieldInGroup("pnd_H2_Pnd_H2_Fill_1", "#H2-FILL-1", FieldType.STRING, 12);
        pnd_H2_Pnd_H2_Title = pnd_H2.newFieldInGroup("pnd_H2_Pnd_H2_Title", "#H2-TITLE", FieldType.STRING, 32);
        pnd_H2_Pnd_H2_Fill_2 = pnd_H2.newFieldInGroup("pnd_H2_Pnd_H2_Fill_2", "#H2-FILL-2", FieldType.STRING, 15);
        pnd_H2_Pnd_H2_Time = pnd_H2.newFieldInGroup("pnd_H2_Pnd_H2_Time", "#H2-TIME", FieldType.STRING, 8);

        pnd_H2__R_Field_14 = localVariables.newGroupInRecord("pnd_H2__R_Field_14", "REDEFINE", pnd_H2);
        pnd_H2_Pnd_Hdr_2 = pnd_H2__R_Field_14.newFieldInGroup("pnd_H2_Pnd_Hdr_2", "#HDR-2", FieldType.STRING, 79);

        pnd_H5 = localVariables.newGroupInRecord("pnd_H5", "#H5");
        pnd_H5_Pnd_H5_Title_1 = pnd_H5.newFieldInGroup("pnd_H5_Pnd_H5_Title_1", "#H5-TITLE-1", FieldType.STRING, 12);
        pnd_H5_Pnd_H5_Start_Date = pnd_H5.newFieldInGroup("pnd_H5_Pnd_H5_Start_Date", "#H5-START-DATE", FieldType.STRING, 8);
        pnd_H5_Pnd_Hdr_Fill_1 = pnd_H5.newFieldInGroup("pnd_H5_Pnd_Hdr_Fill_1", "#HDR-FILL-1", FieldType.STRING, 3);
        pnd_H5_Pnd_H5_Title_2 = pnd_H5.newFieldInGroup("pnd_H5_Pnd_H5_Title_2", "#H5-TITLE-2", FieldType.STRING, 12);
        pnd_H5_Pnd_H5_End_Date = pnd_H5.newFieldInGroup("pnd_H5_Pnd_H5_End_Date", "#H5-END-DATE", FieldType.STRING, 8);

        pnd_H5__R_Field_15 = localVariables.newGroupInRecord("pnd_H5__R_Field_15", "REDEFINE", pnd_H5);
        pnd_H5_Pnd_Hdr_5 = pnd_H5__R_Field_15.newFieldInGroup("pnd_H5_Pnd_Hdr_5", "#HDR-5", FieldType.STRING, 43);

        pnd_H8 = localVariables.newGroupInRecord("pnd_H8", "#H8");
        pnd_H8_Pnd_H8_Fill_1 = pnd_H8.newFieldInGroup("pnd_H8_Pnd_H8_Fill_1", "#H8-FILL-1", FieldType.STRING, 53);
        pnd_H8_Pnd_H8_Desc_2 = pnd_H8.newFieldInGroup("pnd_H8_Pnd_H8_Desc_2", "#H8-DESC-2", FieldType.STRING, 19);
        pnd_H8_Pnd_H8_Desc_3 = pnd_H8.newFieldInGroup("pnd_H8_Pnd_H8_Desc_3", "#H8-DESC-3", FieldType.STRING, 22);

        pnd_H8__R_Field_16 = localVariables.newGroupInRecord("pnd_H8__R_Field_16", "REDEFINE", pnd_H8);
        pnd_H8_Pnd_Hdr_8 = pnd_H8__R_Field_16.newFieldInGroup("pnd_H8_Pnd_Hdr_8", "#HDR-8", FieldType.STRING, 94);

        pnd_H9 = localVariables.newGroupInRecord("pnd_H9", "#H9");
        pnd_H9_Pnd_H9_Desc_1 = pnd_H9.newFieldInGroup("pnd_H9_Pnd_H9_Desc_1", "#H9-DESC-1", FieldType.STRING, 53);
        pnd_H9_Pnd_H9_Desc_2 = pnd_H9.newFieldInGroup("pnd_H9_Pnd_H9_Desc_2", "#H9-DESC-2", FieldType.STRING, 19);
        pnd_H9_Pnd_H9_Desc_3 = pnd_H9.newFieldInGroup("pnd_H9_Pnd_H9_Desc_3", "#H9-DESC-3", FieldType.STRING, 18);

        pnd_H9__R_Field_17 = localVariables.newGroupInRecord("pnd_H9__R_Field_17", "REDEFINE", pnd_H9);
        pnd_H9_Pnd_Hdr_9 = pnd_H9__R_Field_17.newFieldInGroup("pnd_H9_Pnd_Hdr_9", "#HDR-9", FieldType.STRING, 90);

        pnd_H10 = localVariables.newGroupInRecord("pnd_H10", "#H10");
        pnd_H10_Pnd_H10_Fill_1 = pnd_H10.newFieldInGroup("pnd_H10_Pnd_H10_Fill_1", "#H10-FILL-1", FieldType.STRING, 7);
        pnd_H10_Pnd_H10_Fill_2 = pnd_H10.newFieldInGroup("pnd_H10_Pnd_H10_Fill_2", "#H10-FILL-2", FieldType.STRING, 45);
        pnd_H10_Pnd_H10_Fill_3 = pnd_H10.newFieldInGroup("pnd_H10_Pnd_H10_Fill_3", "#H10-FILL-3", FieldType.STRING, 20);
        pnd_H10_Pnd_H10_Fill_4 = pnd_H10.newFieldInGroup("pnd_H10_Pnd_H10_Fill_4", "#H10-FILL-4", FieldType.STRING, 19);

        pnd_H10__R_Field_18 = localVariables.newGroupInRecord("pnd_H10__R_Field_18", "REDEFINE", pnd_H10);
        pnd_H10_Pnd_Hdr_10 = pnd_H10__R_Field_18.newFieldInGroup("pnd_H10_Pnd_Hdr_10", "#HDR-10", FieldType.STRING, 91);
        pnd_Hdr_11 = localVariables.newFieldInRecord("pnd_Hdr_11", "#HDR-11", FieldType.STRING, 130);

        pnd_H12 = localVariables.newGroupInRecord("pnd_H12", "#H12");
        pnd_H12_Pnd_H12_Fill_1 = pnd_H12.newFieldInGroup("pnd_H12_Pnd_H12_Fill_1", "#H12-FILL-1", FieldType.STRING, 15);
        pnd_H12_Pnd_H12_Fill_2 = pnd_H12.newFieldInGroup("pnd_H12_Pnd_H12_Fill_2", "#H12-FILL-2", FieldType.STRING, 46);
        pnd_H12_Pnd_H12_Fill_3 = pnd_H12.newFieldInGroup("pnd_H12_Pnd_H12_Fill_3", "#H12-FILL-3", FieldType.STRING, 15);

        pnd_H12__R_Field_19 = localVariables.newGroupInRecord("pnd_H12__R_Field_19", "REDEFINE", pnd_H12);
        pnd_H12_Pnd_Hdr_12 = pnd_H12__R_Field_19.newFieldInGroup("pnd_H12_Pnd_Hdr_12", "#HDR-12", FieldType.STRING, 76);

        pnd_H13 = localVariables.newGroupInRecord("pnd_H13", "#H13");
        pnd_H13_Pnd_H13_Fill_1 = pnd_H13.newFieldInGroup("pnd_H13_Pnd_H13_Fill_1", "#H13-FILL-1", FieldType.STRING, 31);
        pnd_H13_Pnd_H13_Fill_2 = pnd_H13.newFieldInGroup("pnd_H13_Pnd_H13_Fill_2", "#H13-FILL-2", FieldType.STRING, 30);
        pnd_H13_Pnd_H13_Fill_3 = pnd_H13.newFieldInGroup("pnd_H13_Pnd_H13_Fill_3", "#H13-FILL-3", FieldType.STRING, 18);

        pnd_H13__R_Field_20 = localVariables.newGroupInRecord("pnd_H13__R_Field_20", "REDEFINE", pnd_H13);
        pnd_H13_Pnd_Hdr_13 = pnd_H13__R_Field_20.newFieldInGroup("pnd_H13_Pnd_Hdr_13", "#HDR-13", FieldType.STRING, 79);

        pnd_H14 = localVariables.newGroupInRecord("pnd_H14", "#H14");
        pnd_H14_Pnd_H14_Fill_1 = pnd_H14.newFieldInGroup("pnd_H14_Pnd_H14_Fill_1", "#H14-FILL-1", FieldType.STRING, 31);
        pnd_H14_Pnd_H14_Fill_2 = pnd_H14.newFieldInGroup("pnd_H14_Pnd_H14_Fill_2", "#H14-FILL-2", FieldType.STRING, 30);
        pnd_H14_Pnd_H14_Fill_3 = pnd_H14.newFieldInGroup("pnd_H14_Pnd_H14_Fill_3", "#H14-FILL-3", FieldType.STRING, 18);

        pnd_H14__R_Field_21 = localVariables.newGroupInRecord("pnd_H14__R_Field_21", "REDEFINE", pnd_H14);
        pnd_H14_Pnd_Hdr_14 = pnd_H14__R_Field_21.newFieldInGroup("pnd_H14_Pnd_Hdr_14", "#HDR-14", FieldType.STRING, 79);

        pnd_D1 = localVariables.newGroupInRecord("pnd_D1", "#D1");
        pnd_D1_Pnd_Dtl1_Wpid_Code = pnd_D1.newFieldInGroup("pnd_D1_Pnd_Dtl1_Wpid_Code", "#DTL1-WPID-CODE", FieldType.STRING, 6);
        pnd_D1_Pnd_Dtl1_Fill_1 = pnd_D1.newFieldInGroup("pnd_D1_Pnd_Dtl1_Fill_1", "#DTL1-FILL-1", FieldType.STRING, 1);
        pnd_D1_Pnd_Dtl1_Wpid_Desc = pnd_D1.newFieldInGroup("pnd_D1_Pnd_Dtl1_Wpid_Desc", "#DTL1-WPID-DESC", FieldType.STRING, 45);
        pnd_D1_Pnd_Dtl1_Fill_2 = pnd_D1.newFieldInGroup("pnd_D1_Pnd_Dtl1_Fill_2", "#DTL1-FILL-2", FieldType.STRING, 1);
        pnd_D1_Pnd_Dtl1_Owner_Cde = pnd_D1.newFieldInGroup("pnd_D1_Pnd_Dtl1_Owner_Cde", "#DTL1-OWNER-CDE", FieldType.STRING, 8);
        pnd_D1_Pnd_Dtl1_Fill_3 = pnd_D1.newFieldInGroup("pnd_D1_Pnd_Dtl1_Fill_3", "#DTL1-FILL-3", FieldType.STRING, 3);
        pnd_D1_Pnd_Dtl1_Wpid_New = pnd_D1.newFieldInGroup("pnd_D1_Pnd_Dtl1_Wpid_New", "#DTL1-WPID-NEW", FieldType.STRING, 6);
        pnd_D1_Pnd_Dtl1_Fill_4 = pnd_D1.newFieldInGroup("pnd_D1_Pnd_Dtl1_Fill_4", "#DTL1-FILL-4", FieldType.STRING, 3);
        pnd_D1_Pnd_Dtl1_Retrn_Doc = pnd_D1.newFieldInGroup("pnd_D1_Pnd_Dtl1_Retrn_Doc", "#DTL1-RETRN-DOC", FieldType.STRING, 6);
        pnd_D1_Pnd_Dtl1_Fill_5 = pnd_D1.newFieldInGroup("pnd_D1_Pnd_Dtl1_Fill_5", "#DTL1-FILL-5", FieldType.STRING, 3);
        pnd_D1_Pnd_Dtl1_Wpid_Cnt = pnd_D1.newFieldInGroup("pnd_D1_Pnd_Dtl1_Wpid_Cnt", "#DTL1-WPID-CNT", FieldType.STRING, 6);

        pnd_D1__R_Field_22 = localVariables.newGroupInRecord("pnd_D1__R_Field_22", "REDEFINE", pnd_D1);
        pnd_D1_Pnd_Dtl_1 = pnd_D1__R_Field_22.newFieldInGroup("pnd_D1_Pnd_Dtl_1", "#DTL-1", FieldType.STRING, 88);

        pnd_T1 = localVariables.newGroupInRecord("pnd_T1", "#T1");
        pnd_T1_Pnd_Tot1_Fill_1 = pnd_T1.newFieldInGroup("pnd_T1_Pnd_Tot1_Fill_1", "#TOT1-FILL-1", FieldType.STRING, 47);
        pnd_T1_Pnd_Tot1_Fill_2 = pnd_T1.newFieldInGroup("pnd_T1_Pnd_Tot1_Fill_2", "#TOT1-FILL-2", FieldType.STRING, 17);
        pnd_T1_Pnd_Tot1_Total_New = pnd_T1.newFieldInGroup("pnd_T1_Pnd_Tot1_Total_New", "#TOT1-TOTAL-NEW", FieldType.STRING, 6);
        pnd_T1_Pnd_Tot1_Fill_3 = pnd_T1.newFieldInGroup("pnd_T1_Pnd_Tot1_Fill_3", "#TOT1-FILL-3", FieldType.STRING, 3);
        pnd_T1_Pnd_Tot1_Retrn_Doc = pnd_T1.newFieldInGroup("pnd_T1_Pnd_Tot1_Retrn_Doc", "#TOT1-RETRN-DOC", FieldType.STRING, 6);
        pnd_T1_Pnd_Tot1_Fill_4 = pnd_T1.newFieldInGroup("pnd_T1_Pnd_Tot1_Fill_4", "#TOT1-FILL-4", FieldType.STRING, 3);
        pnd_T1_Pnd_Tot1_Logged = pnd_T1.newFieldInGroup("pnd_T1_Pnd_Tot1_Logged", "#TOT1-LOGGED", FieldType.STRING, 6);

        pnd_T1__R_Field_23 = localVariables.newGroupInRecord("pnd_T1__R_Field_23", "REDEFINE", pnd_T1);
        pnd_T1_Pnd_Tot_1 = pnd_T1__R_Field_23.newFieldInGroup("pnd_T1_Pnd_Tot_1", "#TOT-1", FieldType.STRING, 88);

        pnd_T2 = localVariables.newGroupInRecord("pnd_T2", "#T2");
        pnd_T2_Pnd_Tot2_Fill_1 = pnd_T2.newFieldInGroup("pnd_T2_Pnd_Tot2_Fill_1", "#TOT2-FILL-1", FieldType.STRING, 35);
        pnd_T2_Pnd_Tot2_Fill_2 = pnd_T2.newFieldInGroup("pnd_T2_Pnd_Tot2_Fill_2", "#TOT2-FILL-2", FieldType.STRING, 32);
        pnd_T2_Pnd_Tot2_Unclear = pnd_T2.newFieldInGroup("pnd_T2_Pnd_Tot2_Unclear", "#TOT2-UNCLEAR", FieldType.STRING, 6);

        pnd_T2__R_Field_24 = localVariables.newGroupInRecord("pnd_T2__R_Field_24", "REDEFINE", pnd_T2);
        pnd_T2_Pnd_Tot_2 = pnd_T2__R_Field_24.newFieldInGroup("pnd_T2_Pnd_Tot_2", "#TOT-2", FieldType.STRING, 73);

        pnd_T3 = localVariables.newGroupInRecord("pnd_T3", "#T3");
        pnd_T3_Pnd_Tot3_Fill_1 = pnd_T3.newFieldInGroup("pnd_T3_Pnd_Tot3_Fill_1", "#TOT3-FILL-1", FieldType.STRING, 4);

        pnd_T3_Pnd_Tot3_Array = pnd_T3.newGroupArrayInGroup("pnd_T3_Pnd_Tot3_Array", "#TOT3-ARRAY", new DbsArrayController(1, 8));
        pnd_T3_Pnd_Tot3_Ctrs = pnd_T3_Pnd_Tot3_Array.newFieldInGroup("pnd_T3_Pnd_Tot3_Ctrs", "#TOT3-CTRS", FieldType.STRING, 5);
        pnd_T3_Pnd_Tot3_Fill_2 = pnd_T3_Pnd_Tot3_Array.newFieldInGroup("pnd_T3_Pnd_Tot3_Fill_2", "#TOT3-FILL-2", FieldType.STRING, 5);
        pnd_T3_Pnd_Tot3_Message = pnd_T3.newFieldInGroup("pnd_T3_Pnd_Tot3_Message", "#TOT3-MESSAGE", FieldType.STRING, 21);

        pnd_T3__R_Field_25 = localVariables.newGroupInRecord("pnd_T3__R_Field_25", "REDEFINE", pnd_T3);
        pnd_T3_Pnd_Tot_3 = pnd_T3__R_Field_25.newFieldInGroup("pnd_T3_Pnd_Tot_3", "#TOT-3", FieldType.STRING, 105);
        pnd_Override_Sw = localVariables.newFieldInRecord("pnd_Override_Sw", "#OVERRIDE-SW", FieldType.BOOLEAN, 1);
        pnd_Override_Key = localVariables.newFieldInRecord("pnd_Override_Key", "#OVERRIDE-KEY", FieldType.STRING, 30);
        pnd_Npin_Count = localVariables.newFieldInRecord("pnd_Npin_Count", "#NPIN-COUNT", FieldType.NUMERIC, 7);
        pnd_Rqst_Log_Fl = localVariables.newFieldInRecord("pnd_Rqst_Log_Fl", "#RQST-LOG-FL", FieldType.BOOLEAN, 1);
        pnd_Mit_Rqst_Routing_Key = localVariables.newFieldInRecord("pnd_Mit_Rqst_Routing_Key", "#MIT-RQST-ROUTING-KEY", FieldType.STRING, 30);

        pnd_Mit_Rqst_Routing_Key__R_Field_26 = localVariables.newGroupInRecord("pnd_Mit_Rqst_Routing_Key__R_Field_26", "REDEFINE", pnd_Mit_Rqst_Routing_Key);
        pnd_Mit_Rqst_Routing_Key_Rqst_Log_Dte_Tme = pnd_Mit_Rqst_Routing_Key__R_Field_26.newFieldInGroup("pnd_Mit_Rqst_Routing_Key_Rqst_Log_Dte_Tme", 
            "RQST-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Mit_Rqst_Routing_Key_Last_Chnge_Dte_Tme = pnd_Mit_Rqst_Routing_Key__R_Field_26.newFieldInGroup("pnd_Mit_Rqst_Routing_Key_Last_Chnge_Dte_Tme", 
            "LAST-CHNGE-DTE-TME", FieldType.NUMERIC, 15);
        pnd_Nmit_Rqst_Routing_Key = localVariables.newFieldInRecord("pnd_Nmit_Rqst_Routing_Key", "#NMIT-RQST-ROUTING-KEY", FieldType.STRING, 30);

        pnd_Nmit_Rqst_Routing_Key__R_Field_27 = localVariables.newGroupInRecord("pnd_Nmit_Rqst_Routing_Key__R_Field_27", "REDEFINE", pnd_Nmit_Rqst_Routing_Key);
        pnd_Nmit_Rqst_Routing_Key_Rqst_Log_Dte_Tme = pnd_Nmit_Rqst_Routing_Key__R_Field_27.newFieldInGroup("pnd_Nmit_Rqst_Routing_Key_Rqst_Log_Dte_Tme", 
            "RQST-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Nmit_Rqst_Routing_Key_Strtng_Event_Dte_Tme = pnd_Nmit_Rqst_Routing_Key__R_Field_27.newFieldInGroup("pnd_Nmit_Rqst_Routing_Key_Strtng_Event_Dte_Tme", 
            "STRTNG-EVENT-DTE-TME", FieldType.TIME);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        sort01Tbl_WpidOld = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Wpid_OLD", "Tbl_Wpid_OLD", FieldType.STRING, 6);
        sort01Tbl_WpidCount861 = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Wpid_COUNT_861", "Tbl_Wpid_COUNT_861", FieldType.NUMERIC, 9);
        sort01Tbl_WpidCount = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Wpid_COUNT", "Tbl_Wpid_COUNT", FieldType.NUMERIC, 9);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Master_Index.reset();
        vw_cwf_Master_Log_View.reset();
        vw_ncw_Master_Index.reset();
        vw_ncw_Master_Log_View.reset();
        internalLoopRecord.reset();

        localVariables.reset();
        pnd_Local_Data_Pnd_Wpid_Values.setInitialValue("BFIRTXZ");
        pnd_Report_No.setInitialValue(1);
        pnd_Crc_Unit.setInitialValue("CRC");
        pnd_H1_Pnd_H1_Title.setInitialValue("CORPORATE WORKFLOW FACILITIES");
        pnd_H2_Pnd_H2_Title.setInitialValue("WORK REQUESTS LOGGED AND INDEXED");
        pnd_H5_Pnd_H5_Title_1.setInitialValue("START DATE:");
        pnd_H5_Pnd_H5_Title_2.setInitialValue("END DATE:");
        pnd_H8_Pnd_H8_Desc_2.setInitialValue("OWNER    NEW");
        pnd_H8_Pnd_H8_Desc_3.setInitialValue("RETURNING TOTAL LOGGED");
        pnd_H9_Pnd_H9_Desc_1.setInitialValue("WPID   DESCRIPTION");
        pnd_H9_Pnd_H9_Desc_2.setInitialValue("UNIT     REQUESTS");
        pnd_H9_Pnd_H9_Desc_3.setInitialValue("DOCUMENTS REQUESTS");
        pnd_H10_Pnd_H10_Fill_1.setInitialValue("------ ");
        pnd_H10_Pnd_H10_Fill_2.setInitialValue("---------------------------------------------");
        pnd_H10_Pnd_H10_Fill_3.setInitialValue(" -------- ---------");
        pnd_H10_Pnd_H10_Fill_4.setInitialValue("--------- --------");
        pnd_Hdr_11.setInitialValue("**********************************************************************************************************************************");
        pnd_H12_Pnd_H12_Fill_1.setInitialValue("BOOKLETS  FORMS");
        pnd_H12_Pnd_H12_Fill_3.setInitialValue("MISC NEW");
        pnd_H13_Pnd_H13_Fill_1.setInitialValue("REQUESTED REQUESTED INQUIRIES");
        pnd_H13_Pnd_H13_Fill_2.setInitialValue("RESEARCH TRANSACTS CMPLAINTS");
        pnd_H13_Pnd_H13_Fill_3.setInitialValue("REQUESTS    OTHERS");
        pnd_H14_Pnd_H14_Fill_1.setInitialValue("--------- --------- ---------");
        pnd_H14_Pnd_H14_Fill_2.setInitialValue("-------- --------- --------- ");
        pnd_H14_Pnd_H14_Fill_3.setInitialValue("--------  --------");
        pnd_T1_Pnd_Tot1_Fill_2.setInitialValue("TOTAL -------->");
        pnd_T2_Pnd_Tot2_Fill_1.setInitialValue("NUMBER OF WORK REQUESTS LOGGED AND");
        pnd_T2_Pnd_Tot2_Fill_2.setInitialValue("INDEXED WITH 'U' IN ANY ELEMENT:");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Cwfb3401() throws Exception
    {
        super("Cwfb3401");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Cwfb3401|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        getReports().atTopOfPage(atTopEventRpt4, 4);
        setupReports();
        while(true)
        {
            try
            {
                if (condition(Global.getLIBRARY_ID().notEquals("PRODANN")))                                                                                               //Natural: IF *LIBRARY-ID NE 'PRODANN'
                {
                    pnd_H1_Pnd_H1_Env.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "(", Global.getLIBRARY_ID(), ")"));                                        //Natural: COMPRESS '(' *LIBRARY-ID ')' INTO #H1-ENV LEAVING NO SPACE
                    //* -- JF041602
                }                                                                                                                                                         //Natural: END-IF
                //* ********************
                //*                    *
                //*  REPORT SECTION    *
                //* ********************
                //*                                                                                                                                                       //Natural: FORMAT ( 1 ) LS = 132 PS = 58;//Natural: FORMAT ( 2 ) LS = 132 PS = 58;//Natural: FORMAT ( 3 ) LS = 132 PS = 58;//Natural: FORMAT ( 4 ) LS = 132 PS = 58
                if (condition(Global.getSTACK().getDatacount().equals(1), INPUT_1))                                                                                       //Natural: IF *DATA = 1
                {
                    DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Local_Data_Pnd_Run_Type, new FieldAttributes ("AD=A"));                                        //Natural: INPUT #RUN-TYPE ( AD = A )
                }                                                                                                                                                         //Natural: END-IF
                //*  -----
                pnd_H1_Pnd_H1_Pgm.setValue(Global.getPROGRAM());                                                                                                          //Natural: MOVE *PROGRAM TO #H1-PGM
                pnd_H2_Pnd_H2_Date.setValueEdited(Global.getDATX(),new ReportEditMask("LLL' 'DD', 'YYYY"));                                                               //Natural: MOVE EDITED *DATX ( EM = LLL' 'DD', 'YYYY ) TO #H2-DATE
                pnd_H2_Pnd_H2_Time.setValueEdited(Global.getTIMX(),new ReportEditMask("HH':'II' 'AP"));                                                                   //Natural: MOVE EDITED *TIMX ( EM = HH':'II' 'AP ) TO #H2-TIME
                pnd_Rep_Parm_Pnd_Filler1.setValue("/");                                                                                                                   //Natural: MOVE '/' TO #FILLER1 #FILLER2
                pnd_Rep_Parm_Pnd_Filler2.setValue("/");
                pnd_Rep_Parm_Pnd_Fillera.setValue("/");                                                                                                                   //Natural: MOVE '/' TO #FILLERA #FILLERB
                pnd_Rep_Parm_Pnd_Fillerb.setValue("/");
                //*  ------------------------------------------
                //*  GET OVERRIDE PARAMETERS FROM SUPPORT TABLE -- JF022802
                //*  ------------------------------------------
                pdaCwfa2100.getCwfa2100().reset();                                                                                                                        //Natural: RESET CWFA2100 #OVERRIDE-SW #OVERRIDE-KEY
                pnd_Override_Sw.reset();
                pnd_Override_Key.reset();
                if (condition(pnd_Local_Data_Pnd_Run_Type.equals("MONTHLY")))                                                                                             //Natural: IF #RUN-TYPE = 'MONTHLY'
                {
                    pdaCwfa2100.getCwfa2100_Tbl_Key_Field().setValue("MONTHLY-OVRDE");                                                                                    //Natural: ASSIGN CWFA2100.TBL-KEY-FIELD := 'MONTHLY-OVRDE'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaCwfa2100.getCwfa2100_Tbl_Key_Field().setValue("WEEKLY-OVRDE");                                                                                     //Natural: ASSIGN CWFA2100.TBL-KEY-FIELD := 'WEEKLY-OVRDE'
                }                                                                                                                                                         //Natural: END-IF
                pdaCwfa2100.getCwfa2100_Tbl_Scrty_Level_Ind().setValue("A");                                                                                              //Natural: ASSIGN CWFA2100.TBL-SCRTY-LEVEL-IND := 'A'
                pdaCwfa2100.getCwfa2100_Tbl_Actve_Ind().setValue("A");                                                                                                    //Natural: ASSIGN CWFA2100.TBL-ACTVE-IND := 'A'
                pdaCwfa2100.getCwfa2100_Tbl_Table_Nme().setValue("CWF-REPORT-CWFB3401");                                                                                  //Natural: ASSIGN CWFA2100.TBL-TABLE-NME := 'CWF-REPORT-CWFB3401'
                pdaCdaobjr.getCdaobj_Pnd_Function().setValue("GET");                                                                                                      //Natural: ASSIGN CDAOBJ.#FUNCTION := 'GET'
                                                                                                                                                                          //Natural: PERFORM CALL-CWFN2100
                sub_Call_Cwfn2100();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals(" ") && pdaCwfa2100.getCwfa2100_Tbl_Data3().getSubstring(1,1).equals("Y")))        //Natural: IF MSG-INFO-SUB.##RETURN-CODE = ' ' AND SUBSTRING ( CWFA2100.TBL-DATA3,1,1 ) = 'Y'
                {
                    pnd_Rep_Parm_Pnd_Start_Date.setValue(pdaCwfa2100.getCwfa2100_Tbl_Data1().getSubstring(1,8));                                                          //Natural: MOVE SUBSTRING ( CWFA2100.TBL-DATA1,1,8 ) TO #START-DATE
                    pnd_Rep_Parm_Pnd_End_Date.setValue(pdaCwfa2100.getCwfa2100_Tbl_Data2().getSubstring(1,8));                                                            //Natural: MOVE SUBSTRING ( CWFA2100.TBL-DATA2,1,8 ) TO #END-DATE
                    pnd_Override_Key.setValue(pdaCwfa2100.getCwfa2100_Tbl_Key_Field());                                                                                   //Natural: MOVE CWFA2100.TBL-KEY-FIELD TO #OVERRIDE-KEY
                }                                                                                                                                                         //Natural: END-IF
                //*  -----
                //* -- OVERRIDE REPORT DATE
                if (condition(DbsUtil.maskMatches(pnd_Rep_Parm_Pnd_Start_Date,"YYYYMMDD") && DbsUtil.maskMatches(pnd_Rep_Parm_Pnd_End_Date,"YYYYMMDD")))                  //Natural: IF #START-DATE = MASK ( YYYYMMDD ) AND #END-DATE = MASK ( YYYYMMDD )
                {
                    pnd_Override_Sw.setValue(true);                                                                                                                       //Natural: ASSIGN #OVERRIDE-SW := TRUE
                    //* -- COMPUTE FOR REPORT DATE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Override_Sw.setValue(false);                                                                                                                      //Natural: ASSIGN #OVERRIDE-SW := FALSE
                    //*  MOVE EDITED *DATX(EM=YYYYMMDD) TO #COMP-DATE
                    pnd_Work_Comp_Date.setValue(Global.getDATX());                                                                                                        //Natural: MOVE *DATX TO #WORK-COMP-DATE
                    pnd_Day_Of_Week.setValueEdited(Global.getDATX(),new ReportEditMask("NNN"));                                                                           //Natural: MOVE EDITED *DATX ( EM = NNN ) TO #DAY-OF-WEEK
                    //* *
                    //* * REPORT GENERATED FROM LAST SUNDAY TO LATEST SATURDAY (7 DAYS)
                    //* *
                    if (condition(pnd_Local_Data_Pnd_Run_Type.notEquals("MONTHLY")))                                                                                      //Natural: IF #RUN-TYPE NE 'MONTHLY'
                    {
                        //*  THIS IS A WEEKLY RUN
                        short decideConditionsMet578 = 0;                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #DAY-OF-WEEK;//Natural: VALUE 'Sat'
                        if (condition((pnd_Day_Of_Week.equals("Sat"))))
                        {
                            decideConditionsMet578++;
                            pnd_Days_To_Subtract.setValue(6);                                                                                                             //Natural: MOVE 6 TO #DAYS-TO-SUBTRACT
                        }                                                                                                                                                 //Natural: VALUE 'Sun'
                        else if (condition((pnd_Day_Of_Week.equals("Sun"))))
                        {
                            decideConditionsMet578++;
                            pnd_Days_To_Subtract.setValue(7);                                                                                                             //Natural: MOVE 7 TO #DAYS-TO-SUBTRACT
                        }                                                                                                                                                 //Natural: VALUE 'Mon'
                        else if (condition((pnd_Day_Of_Week.equals("Mon"))))
                        {
                            decideConditionsMet578++;
                            pnd_Days_To_Subtract.setValue(8);                                                                                                             //Natural: MOVE 8 TO #DAYS-TO-SUBTRACT
                        }                                                                                                                                                 //Natural: VALUE 'Tue'
                        else if (condition((pnd_Day_Of_Week.equals("Tue"))))
                        {
                            decideConditionsMet578++;
                            pnd_Days_To_Subtract.setValue(9);                                                                                                             //Natural: MOVE 9 TO #DAYS-TO-SUBTRACT
                        }                                                                                                                                                 //Natural: VALUE 'Wed'
                        else if (condition((pnd_Day_Of_Week.equals("Wed"))))
                        {
                            decideConditionsMet578++;
                            pnd_Days_To_Subtract.setValue(10);                                                                                                            //Natural: MOVE 10 TO #DAYS-TO-SUBTRACT
                        }                                                                                                                                                 //Natural: VALUE 'Thu'
                        else if (condition((pnd_Day_Of_Week.equals("Thu"))))
                        {
                            decideConditionsMet578++;
                            pnd_Days_To_Subtract.setValue(11);                                                                                                            //Natural: MOVE 11 TO #DAYS-TO-SUBTRACT
                        }                                                                                                                                                 //Natural: VALUE 'Fri'
                        else if (condition((pnd_Day_Of_Week.equals("Fri"))))
                        {
                            decideConditionsMet578++;
                            pnd_Days_To_Subtract.setValue(12);                                                                                                            //Natural: MOVE 12 TO #DAYS-TO-SUBTRACT
                        }                                                                                                                                                 //Natural: NONE
                        else if (condition())
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: END-DECIDE
                        pnd_Work_Comp_Date.nsubtract(pnd_Days_To_Subtract);                                                                                               //Natural: SUBTRACT #DAYS-TO-SUBTRACT FROM #WORK-COMP-DATE
                        pnd_Rep_Parm_Pnd_Start_Date.setValueEdited(pnd_Work_Comp_Date,new ReportEditMask("YYYYMMDD"));                                                    //Natural: MOVE EDITED #WORK-COMP-DATE ( EM = YYYYMMDD ) TO #START-DATE
                        pnd_Work_Comp_Date.nadd(6);                                                                                                                       //Natural: ADD 6 TO #WORK-COMP-DATE
                        pnd_Rep_Parm_Pnd_End_Date.setValueEdited(pnd_Work_Comp_Date,new ReportEditMask("YYYYMMDD"));                                                      //Natural: MOVE EDITED #WORK-COMP-DATE ( EM = YYYYMMDD ) TO #END-DATE
                        //*  THIS IS A MONTHLY RUN
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //* *
                        //* *  WE MUST CALCULATE THE FIRST AND LAST DAYS OF LAST MONTH. IF YOU
                        //* *  SUBTRACT THE VALUE OF TODAYS DAY, YOU ALWAYS GET THE LAST DAY
                        //* *  OF LAST MONTH. EXAMPLE: TODAY IS JAN 3. 3 DAYS AGO IS DEC 31.
                        //* *
                        pnd_Days_To_Subtract_Pnd_Days_To_Subtract_A.setValueEdited(Global.getDATX(),new ReportEditMask("DD"));                                            //Natural: MOVE EDITED *DATX ( EM = DD ) TO #DAYS-TO-SUBTRACT-A
                        pnd_Work_Comp_Date.nsubtract(pnd_Days_To_Subtract);                                                                                               //Natural: SUBTRACT #DAYS-TO-SUBTRACT FROM #WORK-COMP-DATE
                        pnd_Rep_Parm_Pnd_End_Date.setValueEdited(pnd_Work_Comp_Date,new ReportEditMask("YYYYMMDD"));                                                      //Natural: MOVE EDITED #WORK-COMP-DATE ( EM = YYYYMMDD ) TO #END-DATE
                        pnd_Rep_Parm_Pnd_Start_Date.setValue(pnd_Rep_Parm_Pnd_End_Date);                                                                                  //Natural: MOVE #END-DATE TO #START-DATE
                        pnd_Rep_Parm_Pnd_Start_Date_Dd.setValue("01");                                                                                                    //Natural: MOVE '01' TO #START-DATE-DD
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  -----
                pnd_Comp_Date.setValue(pnd_Rep_Parm_Pnd_End_Date);                                                                                                        //Natural: MOVE #END-DATE TO #COMP-DATE
                pnd_Yyyymmdd.setValue(pnd_Rep_Parm_Pnd_Start_Date);                                                                                                       //Natural: MOVE #START-DATE TO #YYYYMMDD
                pnd_Rep_Parm_Pnd_Work_Mm.setValue(pnd_Yyyymmdd_Pnd_Mm);                                                                                                   //Natural: MOVE #MM TO #WORK-MM
                pnd_Rep_Parm_Pnd_Work_Dd.setValue(pnd_Yyyymmdd_Pnd_Dd);                                                                                                   //Natural: MOVE #DD TO #WORK-DD
                pnd_Rep_Parm_Pnd_Work_Yy.setValue(pnd_Yyyymmdd_Pnd_Yy);                                                                                                   //Natural: MOVE #YY TO #WORK-YY
                pnd_Yyyymmdd.setValue(pnd_Rep_Parm_Pnd_End_Date);                                                                                                         //Natural: MOVE #END-DATE TO #YYYYMMDD
                pnd_Rep_Parm_Pnd_Work_Mmm.setValue(pnd_Yyyymmdd_Pnd_Mm);                                                                                                  //Natural: MOVE #MM TO #WORK-MMM
                pnd_Rep_Parm_Pnd_Work_Ddd.setValue(pnd_Yyyymmdd_Pnd_Dd);                                                                                                  //Natural: MOVE #DD TO #WORK-DDD
                pnd_Rep_Parm_Pnd_Work_Yyy.setValue(pnd_Yyyymmdd_Pnd_Yy);                                                                                                  //Natural: MOVE #YY TO #WORK-YYY
                pnd_H5_Pnd_H5_Start_Date.setValue(pnd_Rep_Parm_Pnd_Work_Start_Date_A);                                                                                    //Natural: MOVE #WORK-START-DATE-A TO #H5-START-DATE
                pnd_H5_Pnd_H5_End_Date.setValue(pnd_Rep_Parm_Pnd_Work_End_Date_A);                                                                                        //Natural: MOVE #WORK-END-DATE-A TO #H5-END-DATE
                pnd_Rep_Parm_Pnd_Rep_Unit_Cde.setValue(pnd_Crc_Unit);                                                                                                     //Natural: MOVE #CRC-UNIT TO #REP-UNIT-CDE
                //*  GET UNIT NAME
                DbsUtil.callnat(Cwfn1103.class , getCurrentProcessState(), pnd_Rep_Parm_Pnd_Rep_Unit_Cde, pnd_Unit_Name);                                                 //Natural: CALLNAT 'CWFN1103' #REP-UNIT-CDE #UNIT-NAME
                if (condition(Global.isEscape())) return;
                //*  ---------------
                //*  PARTICIPANT MIT
                //*  ---------------
                vw_cwf_Master_Index.startDatabaseRead                                                                                                                     //Natural: READ CWF-MASTER-INDEX BY LAST-CHNGE-DTE-KEY FROM #START-DATE-N
                (
                "READ_MASTER",
                new Wc[] { new Wc("LAST_CHNGE_DTE_KEY", ">=", pnd_Rep_Parm_Pnd_Start_Date_N, WcType.BY) },
                new Oc[] { new Oc("LAST_CHNGE_DTE_KEY", "ASC") }
                );
                READ_MASTER:
                while (condition(vw_cwf_Master_Index.readNextRow("READ_MASTER")))
                {
                    if (condition(cwf_Master_Index_Last_Chnge_Dte_N.greater(pnd_Rep_Parm_Pnd_End_Date_N)))                                                                //Natural: IF LAST-CHNGE-DTE-N GT #END-DATE-N
                    {
                        if (true) break READ_MASTER;                                                                                                                      //Natural: ESCAPE BOTTOM ( READ-MASTER. )
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(((cwf_Master_Index_Last_Chnge_Unit_Cde.equals(pnd_Crc_Unit) && cwf_Master_Index_Admin_Status_Cde.equals("0200")) ||                     //Natural: IF ( CWF-MASTER-INDEX.LAST-CHNGE-UNIT-CDE = #CRC-UNIT AND CWF-MASTER-INDEX.ADMIN-STATUS-CDE = '0200' ) OR ( CWF-MASTER-INDEX.ORGNL-UNIT-CDE = #CRC-UNIT AND CWF-MASTER-INDEX.RQST-LOG-INDEX-DTE = #START-DATE THRU #END-DATE )
                        (cwf_Master_Index_Orgnl_Unit_Cde.equals(pnd_Crc_Unit) && (cwf_Master_Index_Rqst_Log_Index_Dte.greaterOrEqual(pnd_Rep_Parm_Pnd_Start_Date) 
                        && cwf_Master_Index_Rqst_Log_Index_Dte.lessOrEqual(pnd_Rep_Parm_Pnd_End_Date))))))
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(cwf_Master_Index_Admin_Status_Cde.equals("0200")))                                                                                      //Natural: IF CWF-MASTER-INDEX.ADMIN-STATUS-CDE = '0200'
                    {
                        pnd_Oprtr_Cde.setValue(cwf_Master_Index_Last_Chnge_Oprtr_Cde);                                                                                    //Natural: MOVE CWF-MASTER-INDEX.LAST-CHNGE-OPRTR-CDE TO #OPRTR-CDE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  CHK FOR DUP ENTRIES
                                                                                                                                                                          //Natural: PERFORM RQST-MIT-LOOKUP
                        sub_Rqst_Mit_Lookup();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_MASTER"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_MASTER"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        if (condition(! (pnd_Rqst_Log_Fl.getBoolean())))                                                                                                  //Natural: IF NOT #RQST-LOG-FL
                        {
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Oprtr_Cde.setValue(cwf_Master_Index_Rqst_Log_Oprtr_Cde);                                                                                      //Natural: MOVE CWF-MASTER-INDEX.RQST-LOG-OPRTR-CDE TO #OPRTR-CDE
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Local_Data_Pnd_Work_Type.setValue("P");                                                                                                           //Natural: MOVE 'P' TO #WORK-TYPE
                    pnd_Local_Data_Pnd_Work_Prcss_Id.setValue(cwf_Master_Index_Work_Prcss_Id);                                                                            //Natural: MOVE CWF-MASTER-INDEX.WORK-PRCSS-ID TO #WORK-PRCSS-ID
                    //* -- JF022802
                    pnd_Local_Data_Pnd_Work_Status.setValue(cwf_Master_Index_Status_Cde);                                                                                 //Natural: MOVE CWF-MASTER-INDEX.STATUS-CDE TO #WORK-STATUS
                    pnd_Local_Data_Pnd_Work_Admin_Status.setValue(cwf_Master_Index_Admin_Status_Cde);                                                                     //Natural: MOVE CWF-MASTER-INDEX.ADMIN-STATUS-CDE TO #WORK-ADMIN-STATUS
                    //* --
                    pnd_Local_Data_Pnd_Work_Admin_Unit_Cde.setValue(cwf_Master_Index_Admin_Unit_Cde);                                                                     //Natural: MOVE CWF-MASTER-INDEX.ADMIN-UNIT-CDE TO #WORK-ADMIN-UNIT-CDE
                    pnd_Pend_Dte_Tme.setValueEdited(cwf_Master_Index_Extrnl_Pend_Rcv_Dte,new ReportEditMask("YYYYMMDDHHIISST"));                                          //Natural: MOVE EDITED CWF-MASTER-INDEX.EXTRNL-PEND-RCV-DTE ( EM = YYYYMMDDHHIISST ) TO #PEND-DTE-TME
                    //* EMPL+UNIT
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-FILE
                    sub_Write_Work_File();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_MASTER"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_MASTER"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    //*  READ-MASTER.
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                //*  -------------------
                //*  NON-PARTICIPANT MIT  /*-- JF022802
                //*  -------------------
                pnd_Rep_Parm_Pnd_Start_Dte_Tme.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Rep_Parm_Pnd_Start_Date);                                                //Natural: MOVE EDITED #START-DATE TO #START-DTE-TME ( EM = YYYYMMDD )
                vw_ncw_Master_Index.startDatabaseRead                                                                                                                     //Natural: READ NCW-MASTER-INDEX BY SYSTEM-UPDTE-DTE-TME FROM #START-DTE-TME
                (
                "READ_NCW",
                new Wc[] { new Wc("SYSTEM_UPDTE_DTE_TME", ">=", pnd_Rep_Parm_Pnd_Start_Dte_Tme, WcType.BY) },
                new Oc[] { new Oc("SYSTEM_UPDTE_DTE_TME", "ASC") }
                );
                READ_NCW:
                while (condition(vw_ncw_Master_Index.readNextRow("READ_NCW")))
                {
                    pnd_Yyyymmdd.setValueEdited(ncw_Master_Index_System_Updte_Dte_Tme,new ReportEditMask("YYYYMMDD"));                                                    //Natural: MOVE EDITED SYSTEM-UPDTE-DTE-TME ( EM = YYYYMMDD ) TO #YYYYMMDD
                    if (condition(pnd_Yyyymmdd.greater(pnd_Rep_Parm_Pnd_End_Date)))                                                                                       //Natural: IF #YYYYMMDD GT #END-DATE
                    {
                        if (true) break READ_NCW;                                                                                                                         //Natural: ESCAPE BOTTOM ( READ-NCW. )
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(((ncw_Master_Index_Modify_Unit_Cde.equals(pnd_Crc_Unit) && ncw_Master_Index_Admin_Status_Cde.equals("0200")) || (ncw_Master_Index_Rqst_Log_Unit_Cde.equals(pnd_Crc_Unit)  //Natural: IF ( NCW-MASTER-INDEX.MODIFY-UNIT-CDE = #CRC-UNIT AND NCW-MASTER-INDEX.ADMIN-STATUS-CDE = '0200' ) OR ( NCW-MASTER-INDEX.RQST-LOG-UNIT-CDE = #CRC-UNIT AND NCW-MASTER-INDEX.RQST-LOG-INDEX-DTE = #START-DATE THRU #END-DATE )
                        && (ncw_Master_Index_Rqst_Log_Index_Dte.greaterOrEqual(pnd_Rep_Parm_Pnd_Start_Date) && ncw_Master_Index_Rqst_Log_Index_Dte.lessOrEqual(pnd_Rep_Parm_Pnd_End_Date))))))
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ncw_Master_Index_Admin_Status_Cde.equals("0200")))                                                                                      //Natural: IF NCW-MASTER-INDEX.ADMIN-STATUS-CDE = '0200'
                    {
                        pnd_Oprtr_Cde.setValue(ncw_Master_Index_Modify_Oprtr_Cde);                                                                                        //Natural: MOVE NCW-MASTER-INDEX.MODIFY-OPRTR-CDE TO #OPRTR-CDE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  CHK FOR DUP ENTRIES
                                                                                                                                                                          //Natural: PERFORM RQST-NMIT-LOOKUP
                        sub_Rqst_Nmit_Lookup();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_NCW"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_NCW"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        if (condition(! (pnd_Rqst_Log_Fl.getBoolean())))                                                                                                  //Natural: IF NOT #RQST-LOG-FL
                        {
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Oprtr_Cde.setValue(ncw_Master_Index_Rqst_Log_Oprtr_Cde);                                                                                      //Natural: MOVE NCW-MASTER-INDEX.RQST-LOG-OPRTR-CDE TO #OPRTR-CDE
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Local_Data_Pnd_Work_Type.setValue("N");                                                                                                           //Natural: MOVE 'N' TO #WORK-TYPE
                    pnd_Local_Data_Pnd_Work_Prcss_Id.setValue(ncw_Master_Index_Work_Prcss_Id);                                                                            //Natural: MOVE NCW-MASTER-INDEX.WORK-PRCSS-ID TO #WORK-PRCSS-ID
                    pnd_Local_Data_Pnd_Work_Status.setValue(ncw_Master_Index_Status_Cde);                                                                                 //Natural: MOVE NCW-MASTER-INDEX.STATUS-CDE TO #WORK-STATUS
                    pnd_Local_Data_Pnd_Work_Admin_Status.setValue(ncw_Master_Index_Admin_Status_Cde);                                                                     //Natural: MOVE NCW-MASTER-INDEX.ADMIN-STATUS-CDE TO #WORK-ADMIN-STATUS
                    pnd_Local_Data_Pnd_Work_Admin_Unit_Cde.setValue(ncw_Master_Index_Admin_Unit_Cde);                                                                     //Natural: MOVE NCW-MASTER-INDEX.ADMIN-UNIT-CDE TO #WORK-ADMIN-UNIT-CDE
                    pnd_Pend_Dte_Tme.setValueEdited(ncw_Master_Index_Extrnl_Pend_Rcv_Dte,new ReportEditMask("YYYYMMDDHHIISST"));                                          //Natural: MOVE EDITED NCW-MASTER-INDEX.EXTRNL-PEND-RCV-DTE ( EM = YYYYMMDDHHIISST ) TO #PEND-DTE-TME
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-FILE
                    sub_Write_Work_File();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_NCW"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_NCW"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    pnd_Npin_Count.nadd(1);                                                                                                                               //Natural: ADD 1 TO #NPIN-COUNT
                    //*  READ-NCW.
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                //*  -----
                if (condition(pnd_Reccount.equals(getZero())))                                                                                                            //Natural: IF #RECCOUNT = 0
                {
                    getReports().newPage(new ReportSpecification(1));                                                                                                     //Natural: NEWPAGE ( 1 )
                    if (condition(Global.isEscape())){return;}
                    //*  JF041002
                    if (condition(pnd_Local_Data_Pnd_Run_Type.equals("MONTHLY")))                                                                                         //Natural: IF #RUN-TYPE = 'MONTHLY'
                    {
                        pnd_Bldg.setValue("M");                                                                                                                           //Natural: MOVE 'M' TO #BLDG
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Bldg.setValue("W");                                                                                                                           //Natural: MOVE 'W' TO #BLDG
                    }                                                                                                                                                     //Natural: END-IF
                    //*  PRINT "START OF REPORT" SEPARATOR PAGE
                    DbsUtil.callnat(Cwfn3910.class , getCurrentProcessState(), pnd_Report_No, pnd_Racf_Id, pnd_Crc_Unit, pnd_Floor, pnd_Bldg, pnd_Drop_Off,               //Natural: CALLNAT 'CWFN3910' #REPORT-NO #RACF-ID #CRC-UNIT #FLOOR #BLDG #DROP-OFF #COMP-DATE
                        pnd_Comp_Date);
                    if (condition(Global.isEscape())) return;
                    //*  PRINT "NO DATA"  SEPARATOR PAGE
                    DbsUtil.callnat(Cwfn3915.class , getCurrentProcessState(), pnd_Report_No);                                                                            //Natural: CALLNAT 'CWFN3915' #REPORT-NO
                    if (condition(Global.isEscape())) return;
                    //*  PRINT "END OF REPORT" PAGE
                    DbsUtil.callnat(Cwfn3911.class , getCurrentProcessState());                                                                                           //Natural: CALLNAT 'CWFN3911'
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: END-IF
                //*  MOVE TRUE TO #NEW-UNIT #NEW-RACF
                setControl("WB");                                                                                                                                         //Natural: SET CONTROL 'WB'
                //*  -----
                READWORK01:                                                                                                                                               //Natural: READ WORK 1 #WORK-RECORD
                while (condition(getWorkFiles().read(1, pnd_Local_Data_Pnd_Work_Record)))
                {
                    getSort().writeSortInData(pnd_Local_Data_Tbl_Site_Id, pnd_Local_Data_Tbl_Racf_Id, pnd_Local_Data_Tbl_Wpid, pnd_Local_Data_Tbl_Wpid_Act,               //Natural: END-ALL
                        pnd_Local_Data_Tbl_Status, pnd_Local_Data_Tbl_Empl_Name, pnd_Local_Data_Tbl_Rqst_Type);
                }                                                                                                                                                         //Natural: END-WORK
                READWORK01_Exit:
                if (Global.isEscape()) return;
                getSort().sortData(pnd_Local_Data_Tbl_Site_Id, pnd_Local_Data_Tbl_Racf_Id, pnd_Local_Data_Tbl_Wpid);                                                      //Natural: SORT BY TBL-SITE-ID TBL-RACF-ID TBL-WPID USING TBL-WPID-ACT TBL-STATUS TBL-EMPL-NAME TBL-RQST-TYPE
                sort01Tbl_WpidCount861.setDec(new DbsDecimal(0));
                sort01Tbl_WpidCount.setDec(new DbsDecimal(0));
                boolean endOfDataSort01 = true;
                boolean firstSort01 = true;
                SORT01:
                while (condition(getSort().readSortOutData(pnd_Local_Data_Tbl_Site_Id, pnd_Local_Data_Tbl_Racf_Id, pnd_Local_Data_Tbl_Wpid, pnd_Local_Data_Tbl_Wpid_Act, 
                    pnd_Local_Data_Tbl_Status, pnd_Local_Data_Tbl_Empl_Name, pnd_Local_Data_Tbl_Rqst_Type)))
                {
                    CheckAtStartofData733();

                    if (condition(getSort().getAstCOUNTER().greater(0)))
                    {
                        atBreakEventSort01(false);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom()))
                                break;
                            else if (condition(Global.isEscapeBottomImmediate()))
                            {
                                endOfDataSort01 = false;
                                break;
                            }
                            else if (condition(Global.isEscapeTop()))
                            continue;
                            else if (condition())
                            return;
                        }
                    }
                    sort01Tbl_WpidCount861.setInt(sort01Tbl_WpidCount861.getInt() + 1);
                    sort01Tbl_WpidCount.setInt(sort01Tbl_WpidCount.getInt() + 1);
                    //*                                                                                                                                                   //Natural: AT START OF DATA
                    //*                                                                                                                                                   //Natural: AT TOP OF PAGE ( 1 )
                    //*                                                                                                                                                   //Natural: AT TOP OF PAGE ( 2 )
                    //*                                                                                                                                                   //Natural: AT TOP OF PAGE ( 3 )
                    //*  ----------------------------------------------------------------                                                                                 //Natural: AT TOP OF PAGE ( 4 )
                    //*  WPID VALUES
                    //*  B=BOOKLET  F=FORMS  I=INQUIRIES  R=RESEARCH  T=TRANSACTIONS
                    //*  X=COMPLAINTS  Z=MISC NEW     ALL OTHERS=OTHER
                    //*  ----------------------------------------------------------------
                    DbsUtil.examine(new ExamineSource(pnd_Local_Data_Pnd_Wpid_Values), new ExamineSearch(pnd_Local_Data_Tbl_Wpid_Act), new ExamineGivingPosition(pnd_Local_Data_Pnd_Pos)); //Natural: EXAMINE #WPID-VALUES FOR TBL-WPID-ACT GIVING POSITION #POS
                    if (condition(pnd_Local_Data_Pnd_Pos.equals(getZero())))                                                                                              //Natural: IF #POS = 0
                    {
                        pnd_Local_Data_Pnd_Pos.setValue(8);                                                                                                               //Natural: MOVE 8 TO #POS
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Local_Data_Tbl_Status.notEquals("0200")))                                                                                           //Natural: IF TBL-STATUS NE '0200'
                    {
                        //*  LOGGED
                        pnd_Local_Data_Pnd_Log_Ret.setValue(1);                                                                                                           //Natural: MOVE 1 TO #LOG-RET
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  RETURNING DOCUMENTS
                        pnd_Local_Data_Pnd_Log_Ret.setValue(2);                                                                                                           //Natural: MOVE 2 TO #LOG-RET
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Local_Data_Pnd_Counters.getValue(pnd_Local_Data_Pnd_Log_Ret,pnd_Local_Data_Pnd_Pos).nadd(1);                                                      //Natural: ADD 1 TO #COUNTERS ( #LOG-RET, #POS )
                    if (condition(pnd_Local_Data_Tbl_Status.equals("0200")))                                                                                              //Natural: IF TBL-STATUS = '0200'
                    {
                        pnd_Local_Data_Pnd_Returned_Doc.nadd(1);                                                                                                          //Natural: ADD 1 TO #RETURNED-DOC
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Local_Data_Tbl_Wpid_Action.equals("U") || pnd_Local_Data_Tbl_Wpid_Lob.equals("U") || pnd_Local_Data_Tbl_Wpid_Mbp.equals("U")        //Natural: IF TBL-WPID-ACTION = 'U' OR TBL-WPID-LOB = 'U' OR TBL-WPID-MBP = 'U' OR TBL-WPID-SBP = 'U'
                        || pnd_Local_Data_Tbl_Wpid_Sbp.equals("U")))
                    {
                        pnd_Local_Data_Pnd_Total_Unclear.nadd(1);                                                                                                         //Natural: ADD 1 TO #TOTAL-UNCLEAR
                    }                                                                                                                                                     //Natural: END-IF
                    //*                                                                                                                                                   //Natural: AT BREAK OF TBL-WPID
                    //*                                                                                                                                                   //Natural: AT BREAK OF TBL-RACF-ID
                    //*                                                                                                                                                   //Natural: AT BREAK OF TBL-SITE-ID
                    //*                                                                                                                                                   //Natural: AT END OF DATA
                    //*  READ-2.
                    sort01Tbl_WpidOld.setValue(pnd_Local_Data_Tbl_Wpid);                                                                                                  //Natural: END-SORT
                }
                if (condition(getSort().getAstCOUNTER().greater(0)))
                {
                    sort01Tbl_WpidCount861.resetBreak();
                    sort01Tbl_WpidCount.resetBreak();
                    atBreakEventSort01(endOfDataSort01);
                }
                if (condition(getSort().getAtEndOfData()))
                {
                    pnd_Local_Data_Pnd_Total_New.reset();                                                                                                                 //Natural: RESET #TOTAL-NEW #TOTAL-RETURNED-DOC #TOTAL-LOGGED #TOTAL-UNCLEAR
                    pnd_Local_Data_Pnd_Total_Returned_Doc.reset();
                    pnd_Local_Data_Pnd_Total_Logged.reset();
                    pnd_Local_Data_Pnd_Total_Unclear.reset();
                    //*      #PAGE
                }                                                                                                                                                         //Natural: END-ENDDATA
                endSort();
                //*  ----------------------------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WORK-FILE
                //*  ----------------------------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-CWFN2100
                //*  ----------------------------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RQST-MIT-LOOKUP
                //*  ----------------------------------------------------------------------
                //*  ----------------------------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RQST-NMIT-LOOKUP
                //*  ----------------------------------------------------------------------
                //*  -----
                Global.getSTACK().pushData(StackOption.TOP, pnd_Local_Data_Pnd_Run_Type);                                                                                 //Natural: FETCH RETURN 'CWFB3402' #RUN-TYPE
                DbsUtil.invokeMain(DbsUtil.getBlType("CWFB3402"), getCurrentProcessState());
                if (condition(Global.isEscape())) return;
                //*  -----
                //*  SET OVERRIDE INDICATOR TO "N"
                if (condition(pnd_Override_Sw.getBoolean()))                                                                                                              //Natural: IF #OVERRIDE-SW
                {
                    pdaCwfa2100.getCwfa2100().reset();                                                                                                                    //Natural: RESET CWFA2100
                    pdaCwfa2100.getCwfa2100_Tbl_Scrty_Level_Ind().setValue("A");                                                                                          //Natural: ASSIGN CWFA2100.TBL-SCRTY-LEVEL-IND := 'A'
                    pdaCwfa2100.getCwfa2100_Tbl_Actve_Ind().setValue("A");                                                                                                //Natural: ASSIGN CWFA2100.TBL-ACTVE-IND := 'A'
                    pdaCwfa2100.getCwfa2100_Tbl_Table_Nme().setValue("CWF-REPORT-CWFB3401");                                                                              //Natural: ASSIGN CWFA2100.TBL-TABLE-NME := 'CWF-REPORT-CWFB3401'
                    pdaCwfa2100.getCwfa2100_Tbl_Key_Field().setValue(pnd_Override_Key);                                                                                   //Natural: ASSIGN CWFA2100.TBL-KEY-FIELD := #OVERRIDE-KEY
                    pdaCdaobjr.getCdaobj_Pnd_Function().setValue("GET");                                                                                                  //Natural: ASSIGN CDAOBJ.#FUNCTION := 'GET'
                                                                                                                                                                          //Natural: PERFORM CALL-CWFN2100
                    sub_Call_Cwfn2100();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    pdaCdaobjr.getCdaobj_Pnd_Function().setValue("UPDATE");                                                                                               //Natural: ASSIGN CDAOBJ.#FUNCTION := 'UPDATE'
                    setValueToSubstring("N",pdaCwfa2100.getCwfa2100_Tbl_Data3(),1,1);                                                                                     //Natural: MOVE 'N' TO SUBSTRING ( CWFA2100.TBL-DATA3,1,1 )
                                                                                                                                                                          //Natural: PERFORM CALL-CWFN2100
                    sub_Call_Cwfn2100();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION
                }                                                                                                                                                         //Natural: END-IF
                //*                                  /* END JF022802
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Write_Work_File() throws Exception                                                                                                                   //Natural: WRITE-WORK-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        pnd_Local_Data_Pnd_Work_Record.reset();                                                                                                                           //Natural: RESET #WORK-RECORD
        if (condition(DbsUtil.maskMatches(pnd_Local_Data_Pnd_Work_Prcss_Id,"N.....")))                                                                                    //Natural: IF #WORK-PRCSS-ID = MASK ( N..... )
        {
            pnd_Local_Data_Tbl_Wpid_Act.setValue("Z");                                                                                                                    //Natural: MOVE 'Z' TO TBL-WPID-ACT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Local_Data_Tbl_Wpid_Act.setValue(pnd_Local_Data_Pnd_Work_Prcss_Id);                                                                                       //Natural: MOVE #WORK-PRCSS-ID TO TBL-WPID-ACT
        }                                                                                                                                                                 //Natural: END-IF
        //* -- JF022802
        pnd_Local_Data_Tbl_Rqst_Type.setValue(pnd_Local_Data_Pnd_Work_Type);                                                                                              //Natural: MOVE #WORK-TYPE TO TBL-RQST-TYPE
        pnd_Local_Data_Tbl_Wpid.setValue(pnd_Local_Data_Pnd_Work_Prcss_Id);                                                                                               //Natural: MOVE #WORK-PRCSS-ID TO TBL-WPID
        pnd_Local_Data_Tbl_Racf_Id.setValue(pnd_Oprtr_Cde);                                                                                                               //Natural: MOVE #OPRTR-CDE TO TBL-RACF-ID
        //* -- JF022802
        pnd_Local_Data_Tbl_Status.setValue(pnd_Local_Data_Pnd_Work_Admin_Status);                                                                                         //Natural: MOVE #WORK-ADMIN-STATUS TO TBL-STATUS
        pnd_Local_Data_Pnd_Rep_Empl_Name.setValue(pnd_Crc_Unit);                                                                                                          //Natural: MOVE #CRC-UNIT TO #REP-EMPL-NAME
        pnd_Rep_Parm_Pnd_Rep_Racf_Id.setValue(pnd_Local_Data_Tbl_Racf_Id);                                                                                                //Natural: MOVE TBL-RACF-ID TO #REP-RACF-ID
        //*  GET EMPLOYEE NAME AND SITE CODE
        DbsUtil.callnat(Cwfn1124.class , getCurrentProcessState(), pnd_Rep_Parm_Pnd_Rep_Racf_Id, pnd_Local_Data_Pnd_Rep_Empl_Name, pnd_Local_Data_Tbl_Site_Id);           //Natural: CALLNAT 'CWFN1124' #REP-RACF-ID #REP-EMPL-NAME TBL-SITE-ID
        if (condition(Global.isEscape())) return;
        pnd_Local_Data_Tbl_Empl_Name.setValue(pnd_Local_Data_Pnd_Rep_Empl_Name);                                                                                          //Natural: MOVE #REP-EMPL-NAME TO TBL-EMPL-NAME
        //*  IF CWF-MASTER-INDEX.ADMIN-STATUS-CDE = '0200'      /*-- JF022802
        if (condition(pnd_Local_Data_Pnd_Work_Admin_Status.equals("0200")))                                                                                               //Natural: IF #WORK-ADMIN-STATUS = '0200'
        {
            if (condition(pnd_Pend_Dte_Tme_Pnd_Pend_Dte.greaterOrEqual(pnd_Rep_Parm_Pnd_Start_Date) && pnd_Pend_Dte_Tme_Pnd_Pend_Dte.lessOrEqual(pnd_Rep_Parm_Pnd_End_Date))) //Natural: IF #PEND-DTE = #START-DATE THRU #END-DATE
            {
                getWorkFiles().write(1, false, pnd_Local_Data_Pnd_Work_Record);                                                                                           //Natural: WRITE WORK FILE 1 #WORK-RECORD
                pnd_Reccount.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #RECCOUNT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getWorkFiles().write(1, false, pnd_Local_Data_Pnd_Work_Record);                                                                                               //Natural: WRITE WORK FILE 1 #WORK-RECORD
            pnd_Reccount.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #RECCOUNT
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  START JF022802
    private void sub_Call_Cwfn2100() throws Exception                                                                                                                     //Natural: CALL-CWFN2100
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        DbsUtil.callnat(Cwfn2100.class , getCurrentProcessState(), pdaCwfa2100.getCwfa2100(), pdaCwfa2100.getCwfa2100_Id(), pdaCwfa2101.getCwfa2101(),                    //Natural: CALLNAT 'CWFN2100' CWFA2100 CWFA2100-ID CWFA2101 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
            pdaCdaobjr.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
        if (condition(Global.isEscape())) return;
    }
    private void sub_Rqst_Mit_Lookup() throws Exception                                                                                                                   //Natural: RQST-MIT-LOOKUP
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Rqst_Log_Fl.setValue(false);                                                                                                                                  //Natural: ASSIGN #RQST-LOG-FL := FALSE
        pnd_Mit_Rqst_Routing_Key_Rqst_Log_Dte_Tme.setValue(cwf_Master_Index_Rqst_Log_Dte_Tme);                                                                            //Natural: ASSIGN #MIT-RQST-ROUTING-KEY.RQST-LOG-DTE-TME := CWF-MASTER-INDEX.RQST-LOG-DTE-TME
        pnd_Mit_Rqst_Routing_Key_Last_Chnge_Dte_Tme.reset();                                                                                                              //Natural: RESET #MIT-RQST-ROUTING-KEY.LAST-CHNGE-DTE-TME
        vw_cwf_Master_Log_View.startDatabaseRead                                                                                                                          //Natural: READ ( 1 ) CWF-MASTER-LOG-VIEW BY RQST-ROUTING-KEY FROM #MIT-RQST-ROUTING-KEY
        (
        "RD_MIT_LOG",
        new Wc[] { new Wc("RQST_ROUTING_KEY", ">=", pnd_Mit_Rqst_Routing_Key, WcType.BY) },
        new Oc[] { new Oc("RQST_ROUTING_KEY", "ASC") },
        1
        );
        RD_MIT_LOG:
        while (condition(vw_cwf_Master_Log_View.readNextRow("RD_MIT_LOG")))
        {
            if (condition((cwf_Master_Log_View_Rqst_Log_Dte_Tme.equals(cwf_Master_Index_Rqst_Log_Dte_Tme)) && (cwf_Master_Log_View_Last_Chnge_Dte_Tme.equals(cwf_Master_Index_Last_Chnge_Dte_Tme)))) //Natural: IF ( CWF-MASTER-LOG-VIEW.RQST-LOG-DTE-TME = CWF-MASTER-INDEX.RQST-LOG-DTE-TME ) AND ( CWF-MASTER-LOG-VIEW.LAST-CHNGE-DTE-TME = CWF-MASTER-INDEX.LAST-CHNGE-DTE-TME )
            {
                pnd_Rqst_Log_Fl.setValue(true);                                                                                                                           //Natural: ASSIGN #RQST-LOG-FL := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Rqst_Nmit_Lookup() throws Exception                                                                                                                  //Natural: RQST-NMIT-LOOKUP
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Rqst_Log_Fl.setValue(false);                                                                                                                                  //Natural: ASSIGN #RQST-LOG-FL := FALSE
        pnd_Nmit_Rqst_Routing_Key_Rqst_Log_Dte_Tme.setValue(ncw_Master_Index_Rqst_Log_Dte_Tme);                                                                           //Natural: ASSIGN #NMIT-RQST-ROUTING-KEY.RQST-LOG-DTE-TME := NCW-MASTER-INDEX.RQST-LOG-DTE-TME
        pnd_Nmit_Rqst_Routing_Key_Strtng_Event_Dte_Tme.reset();                                                                                                           //Natural: RESET #NMIT-RQST-ROUTING-KEY.STRTNG-EVENT-DTE-TME
        vw_ncw_Master_Log_View.startDatabaseRead                                                                                                                          //Natural: READ ( 1 ) NCW-MASTER-LOG-VIEW BY RQST-ROUTING-KEY FROM #NMIT-RQST-ROUTING-KEY
        (
        "RD_NMIT_LOG",
        new Wc[] { new Wc("RQST_ROUTING_KEY", ">=", pnd_Nmit_Rqst_Routing_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("RQST_ROUTING_KEY", "ASC") },
        1
        );
        RD_NMIT_LOG:
        while (condition(vw_ncw_Master_Log_View.readNextRow("RD_NMIT_LOG")))
        {
            if (condition((ncw_Master_Log_View_Rqst_Log_Dte_Tme.equals(ncw_Master_Index_Rqst_Log_Dte_Tme)) && (ncw_Master_Log_View_System_Updte_Dte_Tme.equals(ncw_Master_Index_System_Updte_Dte_Tme)))) //Natural: IF ( NCW-MASTER-LOG-VIEW.RQST-LOG-DTE-TME = NCW-MASTER-INDEX.RQST-LOG-DTE-TME ) AND ( NCW-MASTER-LOG-VIEW.SYSTEM-UPDTE-DTE-TME = NCW-MASTER-INDEX.SYSTEM-UPDTE-DTE-TME )
            {
                pnd_Rqst_Log_Fl.setValue(true);                                                                                                                           //Natural: ASSIGN #RQST-LOG-FL := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Page.nadd(1);                                                                                                                                     //Natural: COMPUTE #PAGE = #PAGE + 1
                    pnd_H1_Pnd_H1_Page.setValue(DbsUtil.compress("PAGE", pnd_Page));                                                                                      //Natural: COMPRESS 'PAGE' #PAGE INTO #H1-PAGE
                    pnd_H1_Pnd_H1_Page.setValue(pnd_H1_Pnd_H1_Page, MoveOption.RightJustified);                                                                           //Natural: MOVE RIGHT #H1-PAGE TO #H1-PAGE
                    getReports().write(1, ReportOption.NOTITLE,pnd_H1_Pnd_Hdr_1,NEWLINE,pnd_H2_Pnd_Hdr_2,NEWLINE);                                                        //Natural: WRITE ( 1 ) NOTITLE #HDR-1 / #HDR-2 /
                    getReports().write(1, ReportOption.NOTITLE,"UNIT      :",pnd_Rep_Parm_Pnd_Rep_Unit_Cde,pnd_Unit_Name);                                                //Natural: WRITE ( 1 ) 'UNIT      :' #REP-UNIT-CDE #UNIT-NAME
                    getReports().write(1, ReportOption.NOTITLE,"SITE ID   :",pnd_Local_Data_Pnd_Site_Id);                                                                 //Natural: WRITE ( 1 ) 'SITE ID   :' #SITE-ID
                    getReports().write(1, ReportOption.NOTITLE,pnd_H5_Pnd_Hdr_5);                                                                                         //Natural: WRITE ( 1 ) #HDR-5
                    if (condition(pnd_New_Racf.getBoolean()))                                                                                                             //Natural: IF #NEW-RACF
                    {
                        pnd_Local_Data_Pnd_Rep_Empl_Name_Racf.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "(", pnd_Rep_Parm_Pnd_Rep_Racf_Id,                 //Natural: COMPRESS '(' #REP-RACF-ID ')' INTO #REP-EMPL-NAME-RACF LEAVING NO
                            ")"));
                        pnd_Local_Data_Pnd_Rep_Empl_Name_Racf.setValue(DbsUtil.compress(pnd_Local_Data_Pnd_Rep_Empl_Name, pnd_Local_Data_Pnd_Rep_Empl_Name_Racf));        //Natural: COMPRESS #REP-EMPL-NAME #REP-EMPL-NAME-RACF INTO #REP-EMPL-NAME-RACF
                        pnd_New_Racf.setValue(false);                                                                                                                     //Natural: MOVE FALSE TO #NEW-RACF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Local_Data_Pnd_Rep_Empl_Name_Racf.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "(", pnd_Rep_Parm_Pnd_Rep_Racf_Id,                 //Natural: COMPRESS '(' #REP-RACF-ID ')' INTO #REP-EMPL-NAME-RACF LEAVING NO
                            ")"));
                        pnd_Local_Data_Pnd_Rep_Empl_Name_Racf.setValue(DbsUtil.compress(pnd_Local_Data_Pnd_Rep_Empl_Name, pnd_Local_Data_Pnd_Rep_Empl_Name_Racf,          //Natural: COMPRESS #REP-EMPL-NAME #REP-EMPL-NAME-RACF '- CONTINUED' INTO #REP-EMPL-NAME-RACF
                            "- CONTINUED"));
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE,"EMPLOYEE  :",pnd_Local_Data_Pnd_Rep_Empl_Name_Racf);                                                      //Natural: WRITE ( 1 ) 'EMPLOYEE  :' #REP-EMPL-NAME-RACF
                    getReports().write(1, ReportOption.NOTITLE,"=",new RepeatItem(130),NEWLINE);                                                                          //Natural: WRITE ( 1 ) '=' ( 130 ) /
                    getReports().write(1, ReportOption.NOTITLE,pnd_H8_Pnd_Hdr_8,NEWLINE,pnd_H9_Pnd_Hdr_9,NEWLINE,pnd_H10_Pnd_Hdr_10);                                     //Natural: WRITE ( 1 ) #HDR-8 / #HDR-9 / #HDR-10
                    //*  JF041602
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Page2.nadd(1);                                                                                                                                    //Natural: COMPUTE #PAGE2 = #PAGE2 + 1
                    pnd_H1_Pnd_H1_Page.setValue(DbsUtil.compress("PAGE", pnd_Page2));                                                                                     //Natural: COMPRESS 'PAGE' #PAGE2 INTO #H1-PAGE
                    pnd_H1_Pnd_H1_Page.setValue(pnd_H1_Pnd_H1_Page, MoveOption.RightJustified);                                                                           //Natural: MOVE RIGHT #H1-PAGE TO #H1-PAGE
                    getReports().write(2, ReportOption.NOTITLE,pnd_H1_Pnd_Hdr_1,NEWLINE,pnd_H2_Pnd_Hdr_2,NEWLINE);                                                        //Natural: WRITE ( 2 ) NOTITLE #HDR-1 / #HDR-2 /
                    getReports().write(2, ReportOption.NOTITLE,"UNIT      :",pnd_Rep_Parm_Pnd_Rep_Unit_Cde,pnd_Unit_Name);                                                //Natural: WRITE ( 2 ) 'UNIT      :' #REP-UNIT-CDE #UNIT-NAME
                    getReports().write(2, ReportOption.NOTITLE,"SITE ID   :",pnd_Local_Data_Pnd_Site_Id);                                                                 //Natural: WRITE ( 2 ) 'SITE ID   :' #SITE-ID
                    getReports().write(2, ReportOption.NOTITLE,pnd_H5_Pnd_Hdr_5);                                                                                         //Natural: WRITE ( 2 ) #HDR-5
                    if (condition(pnd_New_Racf.getBoolean()))                                                                                                             //Natural: IF #NEW-RACF
                    {
                        pnd_Local_Data_Pnd_Rep_Empl_Name_Racf.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "(", pnd_Rep_Parm_Pnd_Rep_Racf_Id,                 //Natural: COMPRESS '(' #REP-RACF-ID ')' INTO #REP-EMPL-NAME-RACF LEAVING NO
                            ")"));
                        pnd_Local_Data_Pnd_Rep_Empl_Name_Racf.setValue(DbsUtil.compress(pnd_Local_Data_Pnd_Rep_Empl_Name, pnd_Local_Data_Pnd_Rep_Empl_Name_Racf));        //Natural: COMPRESS #REP-EMPL-NAME #REP-EMPL-NAME-RACF INTO #REP-EMPL-NAME-RACF
                        pnd_New_Racf.setValue(false);                                                                                                                     //Natural: MOVE FALSE TO #NEW-RACF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Local_Data_Pnd_Rep_Empl_Name_Racf.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "(", pnd_Rep_Parm_Pnd_Rep_Racf_Id,                 //Natural: COMPRESS '(' #REP-RACF-ID ')' INTO #REP-EMPL-NAME-RACF LEAVING NO
                            ")"));
                        pnd_Local_Data_Pnd_Rep_Empl_Name_Racf.setValue(DbsUtil.compress(pnd_Local_Data_Pnd_Rep_Empl_Name, pnd_Local_Data_Pnd_Rep_Empl_Name_Racf,          //Natural: COMPRESS #REP-EMPL-NAME #REP-EMPL-NAME-RACF '- CONTINUED' INTO #REP-EMPL-NAME-RACF
                            "- CONTINUED"));
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(2, ReportOption.NOTITLE,"EMPLOYEE  :",pnd_Local_Data_Pnd_Rep_Empl_Name_Racf);                                                      //Natural: WRITE ( 2 ) 'EMPLOYEE  :' #REP-EMPL-NAME-RACF
                    getReports().write(2, ReportOption.NOTITLE,"=",new RepeatItem(130),NEWLINE);                                                                          //Natural: WRITE ( 2 ) '=' ( 130 ) /
                    getReports().write(2, ReportOption.NOTITLE,pnd_H8_Pnd_Hdr_8,NEWLINE,pnd_H9_Pnd_Hdr_9,NEWLINE,pnd_H10_Pnd_Hdr_10);                                     //Natural: WRITE ( 2 ) #HDR-8 / #HDR-9 / #HDR-10
                    //*  JF041602
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Page3.nadd(1);                                                                                                                                    //Natural: COMPUTE #PAGE3 = #PAGE3 + 1
                    pnd_H1_Pnd_H1_Page.setValue(DbsUtil.compress("PAGE", pnd_Page3));                                                                                     //Natural: COMPRESS 'PAGE' #PAGE3 INTO #H1-PAGE
                    pnd_H1_Pnd_H1_Page.setValue(pnd_H1_Pnd_H1_Page, MoveOption.RightJustified);                                                                           //Natural: MOVE RIGHT #H1-PAGE TO #H1-PAGE
                    getReports().write(3, ReportOption.NOTITLE,pnd_H1_Pnd_Hdr_1,NEWLINE,pnd_H2_Pnd_Hdr_2,NEWLINE);                                                        //Natural: WRITE ( 3 ) NOTITLE #HDR-1 / #HDR-2 /
                    getReports().write(3, ReportOption.NOTITLE,"UNIT      :",pnd_Rep_Parm_Pnd_Rep_Unit_Cde,pnd_Unit_Name);                                                //Natural: WRITE ( 3 ) 'UNIT      :' #REP-UNIT-CDE #UNIT-NAME
                    getReports().write(3, ReportOption.NOTITLE,"SITE ID   :",pnd_Local_Data_Pnd_Site_Id);                                                                 //Natural: WRITE ( 3 ) 'SITE ID   :' #SITE-ID
                    getReports().write(3, ReportOption.NOTITLE,pnd_H5_Pnd_Hdr_5);                                                                                         //Natural: WRITE ( 3 ) #HDR-5
                    if (condition(pnd_New_Racf.getBoolean()))                                                                                                             //Natural: IF #NEW-RACF
                    {
                        pnd_Local_Data_Pnd_Rep_Empl_Name_Racf.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "(", pnd_Rep_Parm_Pnd_Rep_Racf_Id,                 //Natural: COMPRESS '(' #REP-RACF-ID ')' INTO #REP-EMPL-NAME-RACF LEAVING NO
                            ")"));
                        pnd_Local_Data_Pnd_Rep_Empl_Name_Racf.setValue(DbsUtil.compress(pnd_Local_Data_Pnd_Rep_Empl_Name, pnd_Local_Data_Pnd_Rep_Empl_Name_Racf));        //Natural: COMPRESS #REP-EMPL-NAME #REP-EMPL-NAME-RACF INTO #REP-EMPL-NAME-RACF
                        pnd_New_Racf.setValue(false);                                                                                                                     //Natural: MOVE FALSE TO #NEW-RACF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Local_Data_Pnd_Rep_Empl_Name_Racf.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "(", pnd_Rep_Parm_Pnd_Rep_Racf_Id,                 //Natural: COMPRESS '(' #REP-RACF-ID ')' INTO #REP-EMPL-NAME-RACF LEAVING NO
                            ")"));
                        pnd_Local_Data_Pnd_Rep_Empl_Name_Racf.setValue(DbsUtil.compress(pnd_Local_Data_Pnd_Rep_Empl_Name, pnd_Local_Data_Pnd_Rep_Empl_Name_Racf,          //Natural: COMPRESS #REP-EMPL-NAME #REP-EMPL-NAME-RACF '- CONTINUED' INTO #REP-EMPL-NAME-RACF
                            "- CONTINUED"));
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(3, ReportOption.NOTITLE,"EMPLOYEE  :",pnd_Local_Data_Pnd_Rep_Empl_Name_Racf);                                                      //Natural: WRITE ( 3 ) 'EMPLOYEE  :' #REP-EMPL-NAME-RACF
                    getReports().write(3, ReportOption.NOTITLE,"=",new RepeatItem(130),NEWLINE);                                                                          //Natural: WRITE ( 3 ) '=' ( 130 ) /
                    getReports().write(3, ReportOption.NOTITLE,pnd_H8_Pnd_Hdr_8,NEWLINE,pnd_H9_Pnd_Hdr_9,NEWLINE,pnd_H10_Pnd_Hdr_10);                                     //Natural: WRITE ( 3 ) #HDR-8 / #HDR-9 / #HDR-10
                    //*  JF041602
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt4 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Page4.nadd(1);                                                                                                                                    //Natural: COMPUTE #PAGE4 = #PAGE4 + 1
                    pnd_H1_Pnd_H1_Page.setValue(DbsUtil.compress("PAGE", pnd_Page4));                                                                                     //Natural: COMPRESS 'PAGE' #PAGE4 INTO #H1-PAGE
                    pnd_H1_Pnd_H1_Page.setValue(pnd_H1_Pnd_H1_Page, MoveOption.RightJustified);                                                                           //Natural: MOVE RIGHT #H1-PAGE TO #H1-PAGE
                    getReports().write(4, ReportOption.NOTITLE,pnd_H1_Pnd_Hdr_1,NEWLINE,pnd_H2_Pnd_Hdr_2,NEWLINE);                                                        //Natural: WRITE ( 4 ) NOTITLE #HDR-1 / #HDR-2 /
                    getReports().write(4, ReportOption.NOTITLE,"UNIT      :",pnd_Rep_Parm_Pnd_Rep_Unit_Cde,pnd_Unit_Name);                                                //Natural: WRITE ( 4 ) 'UNIT      :' #REP-UNIT-CDE #UNIT-NAME
                    getReports().write(4, ReportOption.NOTITLE,"SITE ID   :",pnd_Local_Data_Pnd_Site_Id);                                                                 //Natural: WRITE ( 4 ) 'SITE ID   :' #SITE-ID
                    getReports().write(4, ReportOption.NOTITLE,pnd_H5_Pnd_Hdr_5);                                                                                         //Natural: WRITE ( 4 ) #HDR-5
                    if (condition(pnd_New_Racf.getBoolean()))                                                                                                             //Natural: IF #NEW-RACF
                    {
                        pnd_Local_Data_Pnd_Rep_Empl_Name_Racf.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "(", pnd_Rep_Parm_Pnd_Rep_Racf_Id,                 //Natural: COMPRESS '(' #REP-RACF-ID ')' INTO #REP-EMPL-NAME-RACF LEAVING NO
                            ")"));
                        pnd_Local_Data_Pnd_Rep_Empl_Name_Racf.setValue(DbsUtil.compress(pnd_Local_Data_Pnd_Rep_Empl_Name, pnd_Local_Data_Pnd_Rep_Empl_Name_Racf));        //Natural: COMPRESS #REP-EMPL-NAME #REP-EMPL-NAME-RACF INTO #REP-EMPL-NAME-RACF
                        pnd_New_Racf.setValue(false);                                                                                                                     //Natural: MOVE FALSE TO #NEW-RACF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Local_Data_Pnd_Rep_Empl_Name_Racf.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "(", pnd_Rep_Parm_Pnd_Rep_Racf_Id,                 //Natural: COMPRESS '(' #REP-RACF-ID ')' INTO #REP-EMPL-NAME-RACF LEAVING NO
                            ")"));
                        pnd_Local_Data_Pnd_Rep_Empl_Name_Racf.setValue(DbsUtil.compress(pnd_Local_Data_Pnd_Rep_Empl_Name, pnd_Local_Data_Pnd_Rep_Empl_Name_Racf,          //Natural: COMPRESS #REP-EMPL-NAME #REP-EMPL-NAME-RACF '- CONTINUED' INTO #REP-EMPL-NAME-RACF
                            "- CONTINUED"));
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(4, ReportOption.NOTITLE,"EMPLOYEE  :",pnd_Local_Data_Pnd_Rep_Empl_Name_Racf);                                                      //Natural: WRITE ( 4 ) 'EMPLOYEE  :' #REP-EMPL-NAME-RACF
                    getReports().write(4, ReportOption.NOTITLE,"=",new RepeatItem(130),NEWLINE);                                                                          //Natural: WRITE ( 4 ) '=' ( 130 ) /
                    getReports().write(4, ReportOption.NOTITLE,pnd_H8_Pnd_Hdr_8,NEWLINE,pnd_H9_Pnd_Hdr_9,NEWLINE,pnd_H10_Pnd_Hdr_10);                                     //Natural: WRITE ( 4 ) #HDR-8 / #HDR-9 / #HDR-10
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean pnd_Local_Data_Tbl_WpidIsBreak = pnd_Local_Data_Tbl_Wpid.isBreak(endOfData);
        boolean pnd_Local_Data_Tbl_Racf_IdIsBreak = pnd_Local_Data_Tbl_Racf_Id.isBreak(endOfData);
        boolean pnd_Local_Data_Tbl_Site_IdIsBreak = pnd_Local_Data_Tbl_Site_Id.isBreak(endOfData);
        if (condition(pnd_Local_Data_Tbl_WpidIsBreak || pnd_Local_Data_Tbl_Racf_IdIsBreak || pnd_Local_Data_Tbl_Site_IdIsBreak))
        {
            pnd_D1_Pnd_Dtl1_Wpid_Code.setValue(sort01Tbl_WpidOld);                                                                                                        //Natural: MOVE OLD ( TBL-WPID ) TO #DTL1-WPID-CODE
            //*  GET WPID DESCRIPTION AND OWNER UNIT CODE
            DbsUtil.callnat(Cwfn1125.class , getCurrentProcessState(), pnd_D1_Pnd_Dtl1_Wpid_Code, pnd_D1_Pnd_Dtl1_Wpid_Desc, pnd_D1_Pnd_Dtl1_Owner_Cde);                  //Natural: CALLNAT 'CWFN1125' #DTL1-WPID-CODE #DTL1-WPID-DESC #DTL1-OWNER-CDE
            if (condition(Global.isEscape())) return;
            pnd_Local_Data_Pnd_Wpid_Count.setValue(sort01Tbl_WpidCount861);                                                                                               //Natural: MOVE COUNT ( TBL-WPID ) TO #WPID-COUNT
            pnd_Local_Data_Pnd_Wpid_New.compute(new ComputeParameters(false, pnd_Local_Data_Pnd_Wpid_New), pnd_Local_Data_Pnd_Wpid_Count.subtract(pnd_Local_Data_Pnd_Returned_Doc)); //Natural: COMPUTE #WPID-NEW = #WPID-COUNT - #RETURNED-DOC
            pnd_Local_Data_Pnd_Total_New.nadd(pnd_Local_Data_Pnd_Wpid_New);                                                                                               //Natural: COMPUTE #TOTAL-NEW = #TOTAL-NEW + #WPID-NEW
            pnd_Local_Data_Pnd_Total_Returned_Doc.nadd(pnd_Local_Data_Pnd_Returned_Doc);                                                                                  //Natural: COMPUTE #TOTAL-RETURNED-DOC = #TOTAL-RETURNED-DOC + #RETURNED-DOC
            pnd_Local_Data_Pnd_Total_Logged.nadd(pnd_Local_Data_Pnd_Wpid_Count);                                                                                          //Natural: COMPUTE #TOTAL-LOGGED = #TOTAL-LOGGED + #WPID-COUNT
            pnd_D1_Pnd_Dtl1_Wpid_New.setValueEdited(pnd_Local_Data_Pnd_Wpid_New,new ReportEditMask("ZZ,ZZ9"));                                                            //Natural: MOVE EDITED #WPID-NEW ( EM = ZZ,ZZ9 ) TO #DTL1-WPID-NEW
            pnd_D1_Pnd_Dtl1_Retrn_Doc.setValueEdited(pnd_Local_Data_Pnd_Returned_Doc,new ReportEditMask("ZZ,ZZ9"));                                                       //Natural: MOVE EDITED #RETURNED-DOC ( EM = ZZ,ZZ9 ) TO #DTL1-RETRN-DOC
            pnd_D1_Pnd_Dtl1_Wpid_Cnt.setValueEdited(pnd_Local_Data_Pnd_Wpid_Count,new ReportEditMask("ZZ,ZZ9"));                                                          //Natural: MOVE EDITED #WPID-COUNT ( EM = ZZ,ZZ9 ) TO #DTL1-WPID-CNT
            //* -- CHARLOTTE
            //* -- DENVER
            //* -- NEW YORK
            //* -- OTHERS
            short decideConditionsMet878 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #SITE-ID;//Natural: VALUE 'C'
            if (condition((pnd_Local_Data_Pnd_Site_Id.equals("C"))))
            {
                decideConditionsMet878++;
                getReports().write(1, ReportOption.NOTITLE,pnd_D1_Pnd_Dtl_1);                                                                                             //Natural: WRITE ( 1 ) #DTL-1
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'D'
            else if (condition((pnd_Local_Data_Pnd_Site_Id.equals("D"))))
            {
                decideConditionsMet878++;
                getReports().write(2, ReportOption.NOTITLE,pnd_D1_Pnd_Dtl_1);                                                                                             //Natural: WRITE ( 2 ) #DTL-1
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'N'
            else if (condition((pnd_Local_Data_Pnd_Site_Id.equals("N"))))
            {
                decideConditionsMet878++;
                getReports().write(3, ReportOption.NOTITLE,pnd_D1_Pnd_Dtl_1);                                                                                             //Natural: WRITE ( 3 ) #DTL-1
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                getReports().write(4, ReportOption.NOTITLE,pnd_D1_Pnd_Dtl_1);                                                                                             //Natural: WRITE ( 4 ) #DTL-1
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Local_Data_Pnd_Returned_Doc.reset();                                                                                                                      //Natural: RESET #RETURNED-DOC
            sort01Tbl_WpidCount861.setDec(new DbsDecimal(0));                                                                                                             //Natural: END-BREAK
        }
        if (condition(pnd_Local_Data_Tbl_Racf_IdIsBreak || pnd_Local_Data_Tbl_Site_IdIsBreak))
        {
            pnd_T1_Pnd_Tot1_Total_New.setValueEdited(pnd_Local_Data_Pnd_Total_New,new ReportEditMask("ZZ,ZZ9"));                                                          //Natural: MOVE EDITED #TOTAL-NEW ( EM = ZZ,ZZ9 ) TO #TOT1-TOTAL-NEW
            pnd_T1_Pnd_Tot1_Retrn_Doc.setValueEdited(pnd_Local_Data_Pnd_Total_Returned_Doc,new ReportEditMask("ZZ,ZZ9"));                                                 //Natural: MOVE EDITED #TOTAL-RETURNED-DOC ( EM = ZZ,ZZ9 ) TO #TOT1-RETRN-DOC
            pnd_T1_Pnd_Tot1_Logged.setValueEdited(pnd_Local_Data_Pnd_Total_Logged,new ReportEditMask("ZZ,ZZ9"));                                                          //Natural: MOVE EDITED #TOTAL-LOGGED ( EM = ZZ,ZZ9 ) TO #TOT1-LOGGED
            pnd_T2_Pnd_Tot2_Unclear.setValueEdited(pnd_Local_Data_Pnd_Total_Unclear,new ReportEditMask("ZZ,ZZ9"));                                                        //Natural: MOVE EDITED #TOTAL-UNCLEAR ( EM = ZZ,ZZ9 ) TO #TOT2-UNCLEAR
            //*  CHARLOTTE
            short decideConditionsMet897 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #SITE-ID;//Natural: VALUE 'C'
            if (condition((pnd_Local_Data_Pnd_Site_Id.equals("C"))))
            {
                decideConditionsMet897++;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_T1_Pnd_Tot_1,NEWLINE);                                                                             //Natural: WRITE ( 1 ) / #TOT-1 /
                if (condition(Global.isEscape())) return;
                getReports().write(1, ReportOption.NOTITLE,pnd_T2_Pnd_Tot_2);                                                                                             //Natural: WRITE ( 1 ) #TOT-2
                if (condition(Global.isEscape())) return;
                if (condition(getReports().getAstLineCount(1).greater(53)))                                                                                               //Natural: IF *LINE-COUNT ( 1 ) GT 53
                {
                    getReports().newPage(new ReportSpecification(1));                                                                                                     //Natural: NEWPAGE ( 1 )
                    if (condition(Global.isEscape())){return;}
                    //*  DENVER
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,pnd_Hdr_11,NEWLINE,NEWLINE,pnd_H12_Pnd_Hdr_12,NEWLINE,pnd_H13_Pnd_Hdr_13,NEWLINE,              //Natural: WRITE ( 1 ) NOTITLE // #HDR-11 // #HDR-12 / #HDR-13 / #HDR-14
                    pnd_H14_Pnd_Hdr_14);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'D'
            else if (condition((pnd_Local_Data_Pnd_Site_Id.equals("D"))))
            {
                decideConditionsMet897++;
                getReports().write(2, ReportOption.NOTITLE,NEWLINE,pnd_T1_Pnd_Tot_1,NEWLINE);                                                                             //Natural: WRITE ( 2 ) / #TOT-1 /
                if (condition(Global.isEscape())) return;
                getReports().write(2, ReportOption.NOTITLE,pnd_T2_Pnd_Tot_2);                                                                                             //Natural: WRITE ( 2 ) #TOT-2
                if (condition(Global.isEscape())) return;
                if (condition(getReports().getAstLineCount(2).greater(53)))                                                                                               //Natural: IF *LINE-COUNT ( 2 ) GT 53
                {
                    getReports().newPage(new ReportSpecification(2));                                                                                                     //Natural: NEWPAGE ( 2 )
                    if (condition(Global.isEscape())){return;}
                    //*  NEW YORK
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,pnd_Hdr_11,NEWLINE,NEWLINE,pnd_H12_Pnd_Hdr_12,NEWLINE,pnd_H13_Pnd_Hdr_13,NEWLINE,              //Natural: WRITE ( 2 ) NOTITLE // #HDR-11 // #HDR-12 / #HDR-13 / #HDR-14
                    pnd_H14_Pnd_Hdr_14);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'N'
            else if (condition((pnd_Local_Data_Pnd_Site_Id.equals("N"))))
            {
                decideConditionsMet897++;
                getReports().write(3, ReportOption.NOTITLE,NEWLINE,pnd_T1_Pnd_Tot_1,NEWLINE);                                                                             //Natural: WRITE ( 3 ) / #TOT-1 /
                if (condition(Global.isEscape())) return;
                getReports().write(3, ReportOption.NOTITLE,pnd_T2_Pnd_Tot_2);                                                                                             //Natural: WRITE ( 3 ) #TOT-2
                if (condition(Global.isEscape())) return;
                if (condition(getReports().getAstLineCount(3).greater(53)))                                                                                               //Natural: IF *LINE-COUNT ( 3 ) GT 53
                {
                    getReports().newPage(new ReportSpecification(3));                                                                                                     //Natural: NEWPAGE ( 3 )
                    if (condition(Global.isEscape())){return;}
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(3, ReportOption.NOTITLE,NEWLINE,NEWLINE,pnd_Hdr_11,NEWLINE,NEWLINE,pnd_H12_Pnd_Hdr_12,NEWLINE,pnd_H13_Pnd_Hdr_13,NEWLINE,              //Natural: WRITE ( 3 ) NOTITLE // #HDR-11 // #HDR-12 / #HDR-13 / #HDR-14
                    pnd_H14_Pnd_Hdr_14);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                getReports().write(4, ReportOption.NOTITLE,NEWLINE,pnd_T1_Pnd_Tot_1,NEWLINE);                                                                             //Natural: WRITE ( 4 ) / #TOT-1 /
                if (condition(Global.isEscape())) return;
                getReports().write(4, ReportOption.NOTITLE,pnd_T2_Pnd_Tot_2);                                                                                             //Natural: WRITE ( 4 ) #TOT-2
                if (condition(Global.isEscape())) return;
                if (condition(getReports().getAstLineCount(4).greater(53)))                                                                                               //Natural: IF *LINE-COUNT ( 4 ) GT 53
                {
                    getReports().newPage(new ReportSpecification(4));                                                                                                     //Natural: NEWPAGE ( 4 )
                    if (condition(Global.isEscape())){return;}
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(4, ReportOption.NOTITLE,NEWLINE,NEWLINE,pnd_Hdr_11,NEWLINE,NEWLINE,pnd_H12_Pnd_Hdr_12,NEWLINE,pnd_H13_Pnd_Hdr_13,NEWLINE,              //Natural: WRITE ( 4 ) NOTITLE // #HDR-11 // #HDR-12 / #HDR-13 / #HDR-14
                    pnd_H14_Pnd_Hdr_14);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_T3_Pnd_Tot3_Ctrs.getValue("*").setValueEdited(pnd_Local_Data_Pnd_Counters.getValue(1,"*"),new ReportEditMask("ZZZZ9"));                                   //Natural: MOVE EDITED #COUNTERS ( 1,* ) ( EM = ZZZZ9 ) TO #TOT3-CTRS ( * )
            pnd_T3_Pnd_Tot3_Message.setValue("(LOGGED)");                                                                                                                 //Natural: MOVE '(LOGGED)' TO #TOT3-MESSAGE
            //*  CHARLOTTE
            //*  DENVER
            //*  NEW YORK
            //*  OTHERS
            short decideConditionsMet935 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #SITE-ID;//Natural: VALUE 'C'
            if (condition((pnd_Local_Data_Pnd_Site_Id.equals("C"))))
            {
                decideConditionsMet935++;
                getReports().write(1, ReportOption.NOTITLE,pnd_T3_Pnd_Tot_3);                                                                                             //Natural: WRITE ( 1 ) #TOT-3
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'D'
            else if (condition((pnd_Local_Data_Pnd_Site_Id.equals("D"))))
            {
                decideConditionsMet935++;
                getReports().write(2, ReportOption.NOTITLE,pnd_T3_Pnd_Tot_3);                                                                                             //Natural: WRITE ( 2 ) #TOT-3
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: VALUE 'N'
            else if (condition((pnd_Local_Data_Pnd_Site_Id.equals("N"))))
            {
                decideConditionsMet935++;
                getReports().write(3, ReportOption.NOTITLE,pnd_T3_Pnd_Tot_3);                                                                                             //Natural: WRITE ( 3 ) #TOT-3
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                getReports().write(4, ReportOption.NOTITLE,pnd_T3_Pnd_Tot_3);                                                                                             //Natural: WRITE ( 4 ) #TOT-3
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_T3_Pnd_Tot3_Ctrs.getValue("*").setValueEdited(pnd_Local_Data_Pnd_Counters.getValue(2,"*"),new ReportEditMask("ZZZZ9"));                                   //Natural: MOVE EDITED #COUNTERS ( 2,* ) ( EM = ZZZZ9 ) TO #TOT3-CTRS ( * )
            pnd_T3_Pnd_Tot3_Message.setValue("(RETURNING DOCUMENTS)");                                                                                                    //Natural: MOVE '(RETURNING DOCUMENTS)' TO #TOT3-MESSAGE
            //*  CHARLOTTE
            short decideConditionsMet948 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #SITE-ID;//Natural: VALUE 'C'
            if (condition((pnd_Local_Data_Pnd_Site_Id.equals("C"))))
            {
                decideConditionsMet948++;
                getReports().write(1, ReportOption.NOTITLE,pnd_T3_Pnd_Tot_3);                                                                                             //Natural: WRITE ( 1 ) #TOT-3
                if (condition(Global.isEscape())) return;
                pnd_Rep_Parm_Pnd_Rep_Racf_Id.setValue(pnd_Local_Data_Tbl_Racf_Id);                                                                                        //Natural: MOVE TBL-RACF-ID TO #REP-RACF-ID
                //*  DENVER
                pnd_Local_Data_Pnd_Rep_Empl_Name.setValue(pnd_Local_Data_Tbl_Empl_Name);                                                                                  //Natural: MOVE TBL-EMPL-NAME TO #REP-EMPL-NAME
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: VALUE 'D'
            else if (condition((pnd_Local_Data_Pnd_Site_Id.equals("D"))))
            {
                decideConditionsMet948++;
                getReports().write(2, ReportOption.NOTITLE,pnd_T3_Pnd_Tot_3);                                                                                             //Natural: WRITE ( 2 ) #TOT-3
                if (condition(Global.isEscape())) return;
                pnd_Rep_Parm_Pnd_Rep_Racf_Id.setValue(pnd_Local_Data_Tbl_Racf_Id);                                                                                        //Natural: MOVE TBL-RACF-ID TO #REP-RACF-ID
                //*  NEW YORK
                pnd_Local_Data_Pnd_Rep_Empl_Name.setValue(pnd_Local_Data_Tbl_Empl_Name);                                                                                  //Natural: MOVE TBL-EMPL-NAME TO #REP-EMPL-NAME
                getReports().newPage(new ReportSpecification(2));                                                                                                         //Natural: NEWPAGE ( 2 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: VALUE 'N'
            else if (condition((pnd_Local_Data_Pnd_Site_Id.equals("N"))))
            {
                decideConditionsMet948++;
                getReports().write(3, ReportOption.NOTITLE,pnd_T3_Pnd_Tot_3);                                                                                             //Natural: WRITE ( 3 ) #TOT-3
                if (condition(Global.isEscape())) return;
                pnd_Rep_Parm_Pnd_Rep_Racf_Id.setValue(pnd_Local_Data_Tbl_Racf_Id);                                                                                        //Natural: MOVE TBL-RACF-ID TO #REP-RACF-ID
                pnd_Local_Data_Pnd_Rep_Empl_Name.setValue(pnd_Local_Data_Tbl_Empl_Name);                                                                                  //Natural: MOVE TBL-EMPL-NAME TO #REP-EMPL-NAME
                getReports().newPage(new ReportSpecification(3));                                                                                                         //Natural: NEWPAGE ( 3 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                getReports().write(4, ReportOption.NOTITLE,pnd_T3_Pnd_Tot_3);                                                                                             //Natural: WRITE ( 4 ) #TOT-3
                if (condition(Global.isEscape())) return;
                pnd_Rep_Parm_Pnd_Rep_Racf_Id.setValue(pnd_Local_Data_Tbl_Racf_Id);                                                                                        //Natural: MOVE TBL-RACF-ID TO #REP-RACF-ID
                pnd_Local_Data_Pnd_Rep_Empl_Name.setValue(pnd_Local_Data_Tbl_Empl_Name);                                                                                  //Natural: MOVE TBL-EMPL-NAME TO #REP-EMPL-NAME
                getReports().newPage(new ReportSpecification(4));                                                                                                         //Natural: NEWPAGE ( 4 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Local_Data_Pnd_Counters.getValue("*","*").reset();                                                                                                        //Natural: RESET #COUNTERS ( *,* ) #TOTAL-NEW #TOTAL-RETURNED-DOC #TOTAL-LOGGED #TOTAL-UNCLEAR
            pnd_Local_Data_Pnd_Total_New.reset();
            pnd_Local_Data_Pnd_Total_Returned_Doc.reset();
            pnd_Local_Data_Pnd_Total_Logged.reset();
            pnd_Local_Data_Pnd_Total_Unclear.reset();
            pnd_New_Racf.setValue(true);                                                                                                                                  //Natural: MOVE TRUE TO #NEW-RACF
            //* -- JF022802
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pnd_Local_Data_Tbl_Site_IdIsBreak))
        {
            pnd_Local_Data_Pnd_Site_Id.setValue(pnd_Local_Data_Tbl_Site_Id);                                                                                              //Natural: MOVE TBL-SITE-ID TO #SITE-ID
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=58");
        Global.format(2, "LS=132 PS=58");
        Global.format(3, "LS=132 PS=58");
        Global.format(4, "LS=132 PS=58");
    }
    private void CheckAtStartofData733() throws Exception
    {
        if (condition(getSort().getAtStartOfData()))
        {
            //*  JF041002
            if (condition(pnd_Local_Data_Pnd_Run_Type.equals("MONTHLY")))                                                                                                 //Natural: IF #RUN-TYPE = 'MONTHLY'
            {
                pnd_Bldg.setValue("M");                                                                                                                                   //Natural: MOVE 'M' TO #BLDG
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Bldg.setValue("W");                                                                                                                                   //Natural: MOVE 'W' TO #BLDG
            }                                                                                                                                                             //Natural: END-IF
            //*  PRINT "START OF REPORT" SEPARATOR PAGE
            DbsUtil.callnat(Cwfn3910.class , getCurrentProcessState(), pnd_Report_No, pnd_Racf_Id, pnd_Rep_Parm_Pnd_Rep_Unit_Cde, pnd_Floor, pnd_Bldg,                    //Natural: CALLNAT 'CWFN3910' #REPORT-NO #RACF-ID #REP-UNIT-CDE #FLOOR #BLDG #DROP-OFF #COMP-DATE
                pnd_Drop_Off, pnd_Comp_Date);
            if (condition(Global.isEscape())) return;
            pnd_Local_Data_Pnd_Site_Id.setValue(pnd_Local_Data_Tbl_Site_Id);                                                                                              //Natural: MOVE TBL-SITE-ID TO #SITE-ID
            pnd_Rep_Parm_Pnd_Rep_Racf_Id.setValue(pnd_Local_Data_Tbl_Racf_Id);                                                                                            //Natural: MOVE TBL-RACF-ID TO #REP-RACF-ID
            pnd_Local_Data_Pnd_Rep_Empl_Name.setValue(pnd_Local_Data_Tbl_Empl_Name);                                                                                      //Natural: MOVE TBL-EMPL-NAME TO #REP-EMPL-NAME
            pnd_New_Racf.setValue(true);                                                                                                                                  //Natural: MOVE TRUE TO #NEW-RACF
        }                                                                                                                                                                 //Natural: END-START
    }
}
