/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:39:37 PM
**        * FROM NATURAL PROGRAM : Icwb3410
************************************************************
**        * FILE NAME            : Icwb3410.java
**        * CLASS NAME           : Icwb3410
**        * INSTANCE NAME        : Icwb3410
************************************************************
************************************************************************
* PROGRAM  : IWFB3410
* SYSTEM   : CRPICW
* FUNCTION : PRINT MONTHLY REPORT OF INSTITUTIONAL REQUESTS ARRIVED
*          : VIA FAX
*          : INSTITUTIONAL WORK FLOW REPORT
*          |
*          |    INPUT: YYYYMMNN = YYYYMM - STARTING MONTH
*          |                      NN     - NUMBER OF MONTHS TO INCLUDE
*          |
* 07/26/96 | OB  IF NOT INITIATED FROM P12..... ASSUME MONTH
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Icwb3410 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_icw_Master_Index;
    private DbsField icw_Master_Index_Rqst_Log_Dte_Tme;

    private DbsGroup icw_Master_Index__R_Field_1;
    private DbsField icw_Master_Index_Rqst_Log_Index_Dte;
    private DbsField icw_Master_Index_Rqst_Log_Index_Tme;
    private DbsField icw_Master_Index_Rqst_Log_Unit_Cde;
    private DbsField icw_Master_Index_Rqst_Log_Oprtr_Cde;
    private DbsField icw_Master_Index_Rqst_Orgn_Cde;
    private DbsField icw_Master_Index_Modify_Unit_Cde;
    private DbsField icw_Master_Index_Ppg_Cde;
    private DbsField icw_Master_Index_Work_Prcss_Id;
    private DbsField icw_Master_Index_Tiaa_Rcvd_Dte_Tme;
    private DbsField icw_Master_Index_Unit_Cde;
    private DbsField icw_Master_Index_Unit_Updte_Dte_Tme;
    private DbsField icw_Master_Index_Admin_Unit_Cde;
    private DbsField icw_Master_Index_Admin_Status_Cde;
    private DbsField icw_Master_Index_Empl_Racf_Id;
    private DbsField icw_Master_Index_Status_Cde;
    private DbsField icw_Master_Index_Actve_Ind;

    private DbsGroup icw_Master_Index__R_Field_2;
    private DbsField icw_Master_Index_Actve_Ind_1;
    private DbsField icw_Master_Index_Actve_Ind_2;
    private DbsField icw_Master_Index_Actv_Unque_Key;

    private DataAccessProgramView vw_unit_Tbl;
    private DbsField unit_Tbl_Unit_Long_Nme;

    private DataAccessProgramView vw_wpid_Tbl;
    private DbsField wpid_Tbl_Work_Prcss_Long_Nme;

    private DbsGroup pnd_Vars;
    private DbsField pnd_Vars_Pnd_Combined_Ctr;
    private DbsField pnd_Vars_Pnd_Combined_Ctr_Emi;
    private DbsField pnd_Vars_Pnd_Date_Time;

    private DbsGroup pnd_Vars__R_Field_3;
    private DbsField pnd_Vars_Pnd_Date;
    private DbsField pnd_Vars_Pnd_Dte;
    private DbsField pnd_Vars_Pnd_End_Dte;

    private DbsGroup pnd_Vars__R_Field_4;
    private DbsField pnd_Vars_Pnd_Dateym;
    private DbsField pnd_Vars_Pnd_Dated;
    private DbsField pnd_Vars_Pnd_Input_Dte;
    private DbsField pnd_Vars_Pnd_Phys_Id;

    private DbsGroup pnd_Vars__R_Field_5;
    private DbsField pnd_Vars_Pnd_Phys_P;
    private DbsField pnd_Vars_Pnd_Phys_N;
    private DbsField pnd_Vars_Pnd_Spcl_Msg;

    private DbsGroup pnd_Vars__R_Field_6;
    private DbsField pnd_Vars_Pnd_Msg;
    private DbsField pnd_Vars_Pnd_Ssn;
    private DbsField pnd_Vars_Pnd_Name;
    private DbsField pnd_Vars_Pnd_Tcrc_Ctr;
    private DbsField pnd_Vars_Pnd_Tcrc_Ctr_Emi;
    private DbsField pnd_Vars_Pnd_Tctr_Noncrc;
    private DbsField pnd_Vars_Pnd_Tctr_Noncrc_Emi;
    private DbsField pnd_Vars_Pnd_Tctr;
    private DbsField pnd_Vars_Pnd_Tctr_Emi;
    private DbsField pnd_Vars_Pnd_Time;
    private DbsField pnd_Vars_Pnd_Cirs_Ctr;
    private DbsField pnd_Vars_Pnd_Cirs_Ctr_Emi;
    private DbsField pnd_Vars_Pnd_Wpid_Ctr;
    private DbsField pnd_Vars_Pnd_Wpid_Ctr_Emi;
    private DbsField pnd_Vars_Pnd_Wpid_Combined_Ctr;
    private DbsField pnd_Vars_Pnd_Wpid_Combined_Ctr_Emi;
    private DbsField pnd_Vars_Pnd_Wpid_Crc_Ctr;
    private DbsField pnd_Vars_Pnd_Wpid_Crc_Ctr_Emi;
    private DbsField pnd_Vars_Pnd_Wpid_Non_Crc_Ctr;
    private DbsField pnd_Vars_Pnd_Wpid_Non_Crc_Ctr_Emi;
    private DbsField pnd_Vars_Pnd_Wpid_Sv;

    private DbsRecord internalLoopRecord;
    private DbsField sort01Work_Prcss_IdOld;

    private DbsRecord setTimeRecord;
    private DbsField st;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_icw_Master_Index = new DataAccessProgramView(new NameInfo("vw_icw_Master_Index", "ICW-MASTER-INDEX"), "ICW_MASTER_INDEX", "ICW_MASTER_INDEX");
        icw_Master_Index_Rqst_Log_Dte_Tme = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");

        icw_Master_Index__R_Field_1 = vw_icw_Master_Index.getRecord().newGroupInGroup("icw_Master_Index__R_Field_1", "REDEFINE", icw_Master_Index_Rqst_Log_Dte_Tme);
        icw_Master_Index_Rqst_Log_Index_Dte = icw_Master_Index__R_Field_1.newFieldInGroup("icw_Master_Index_Rqst_Log_Index_Dte", "RQST-LOG-INDEX-DTE", 
            FieldType.STRING, 8);
        icw_Master_Index_Rqst_Log_Index_Tme = icw_Master_Index__R_Field_1.newFieldInGroup("icw_Master_Index_Rqst_Log_Index_Tme", "RQST-LOG-INDEX-TME", 
            FieldType.STRING, 7);
        icw_Master_Index_Rqst_Log_Unit_Cde = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Rqst_Log_Unit_Cde", "RQST-LOG-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_LOG_UNIT_CDE");
        icw_Master_Index_Rqst_Log_Oprtr_Cde = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_LOG_OPRTR_CDE");
        icw_Master_Index_Rqst_Log_Oprtr_Cde.setDdmHeader("ENTRY/TIME");
        icw_Master_Index_Rqst_Orgn_Cde = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Rqst_Orgn_Cde", "RQST-ORGN-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RQST_ORGN_CDE");
        icw_Master_Index_Rqst_Orgn_Cde.setDdmHeader("ENTRY/OPERATOR");
        icw_Master_Index_Modify_Unit_Cde = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Modify_Unit_Cde", "MODIFY-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "MODIFY_UNIT_CDE");
        icw_Master_Index_Ppg_Cde = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Ppg_Cde", "PPG-CDE", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "PPG_CDE");
        icw_Master_Index_Work_Prcss_Id = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        icw_Master_Index_Work_Prcss_Id.setDdmHeader("WORK/ID");
        icw_Master_Index_Tiaa_Rcvd_Dte_Tme = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Tiaa_Rcvd_Dte_Tme", "TIAA-RCVD-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "TIAA_RCVD_DTE_TME");
        icw_Master_Index_Tiaa_Rcvd_Dte_Tme.setDdmHeader("TIAA-RCV/DTE-TME");
        icw_Master_Index_Unit_Cde = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "UNIT_CDE");
        icw_Master_Index_Unit_Cde.setDdmHeader("UNIT/CODE");
        icw_Master_Index_Unit_Updte_Dte_Tme = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Unit_Updte_Dte_Tme", "UNIT-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "UNIT_UPDTE_DTE_TME");
        icw_Master_Index_Unit_Updte_Dte_Tme.setDdmHeader("UNIT UPDATE/DATE-TIME");
        icw_Master_Index_Admin_Unit_Cde = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ADMIN_UNIT_CDE");
        icw_Master_Index_Admin_Status_Cde = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "ADMIN_STATUS_CDE");
        icw_Master_Index_Empl_Racf_Id = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "EMPL_RACF_ID");
        icw_Master_Index_Status_Cde = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Status_Cde", "STATUS-CDE", FieldType.STRING, 4, 
            RepeatingFieldStrategy.None, "STATUS_CDE");
        icw_Master_Index_Actve_Ind = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Actve_Ind", "ACTVE-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ACTVE_IND");

        icw_Master_Index__R_Field_2 = vw_icw_Master_Index.getRecord().newGroupInGroup("icw_Master_Index__R_Field_2", "REDEFINE", icw_Master_Index_Actve_Ind);
        icw_Master_Index_Actve_Ind_1 = icw_Master_Index__R_Field_2.newFieldInGroup("icw_Master_Index_Actve_Ind_1", "ACTVE-IND-1", FieldType.STRING, 1);
        icw_Master_Index_Actve_Ind_2 = icw_Master_Index__R_Field_2.newFieldInGroup("icw_Master_Index_Actve_Ind_2", "ACTVE-IND-2", FieldType.STRING, 1);
        icw_Master_Index_Actv_Unque_Key = vw_icw_Master_Index.getRecord().newFieldInGroup("icw_Master_Index_Actv_Unque_Key", "ACTV-UNQUE-KEY", FieldType.STRING, 
            16, RepeatingFieldStrategy.None, "ACTV_UNQUE_KEY");
        icw_Master_Index_Actv_Unque_Key.setDdmHeader("UNIQUE/KEY");
        icw_Master_Index_Actv_Unque_Key.setSuperDescriptor(true);
        registerRecord(vw_icw_Master_Index);

        vw_unit_Tbl = new DataAccessProgramView(new NameInfo("vw_unit_Tbl", "UNIT-TBL"), "CWF_ORG_UNIT_TBL", "CWF_ASSIGN_RULE");
        unit_Tbl_Unit_Long_Nme = vw_unit_Tbl.getRecord().newFieldInGroup("unit_Tbl_Unit_Long_Nme", "UNIT-LONG-NME", FieldType.STRING, 45, RepeatingFieldStrategy.None, 
            "UNIT_LONG_NME");
        unit_Tbl_Unit_Long_Nme.setDdmHeader("UNIT LONG NAME");
        registerRecord(vw_unit_Tbl);

        vw_wpid_Tbl = new DataAccessProgramView(new NameInfo("vw_wpid_Tbl", "WPID-TBL"), "CWF_WP_WORK_PRCSS_ID", "CWF_PROFILE");
        wpid_Tbl_Work_Prcss_Long_Nme = vw_wpid_Tbl.getRecord().newFieldInGroup("wpid_Tbl_Work_Prcss_Long_Nme", "WORK-PRCSS-LONG-NME", FieldType.STRING, 
            45, RepeatingFieldStrategy.None, "WORK_PRCSS_LONG_NME");
        wpid_Tbl_Work_Prcss_Long_Nme.setDdmHeader("NAME");
        registerRecord(vw_wpid_Tbl);

        pnd_Vars = localVariables.newGroupInRecord("pnd_Vars", "#VARS");
        pnd_Vars_Pnd_Combined_Ctr = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Combined_Ctr", "#COMBINED-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Vars_Pnd_Combined_Ctr_Emi = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Combined_Ctr_Emi", "#COMBINED-CTR-EMI", FieldType.PACKED_DECIMAL, 7);
        pnd_Vars_Pnd_Date_Time = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Date_Time", "#DATE-TIME", FieldType.STRING, 15);

        pnd_Vars__R_Field_3 = pnd_Vars.newGroupInGroup("pnd_Vars__R_Field_3", "REDEFINE", pnd_Vars_Pnd_Date_Time);
        pnd_Vars_Pnd_Date = pnd_Vars__R_Field_3.newFieldInGroup("pnd_Vars_Pnd_Date", "#DATE", FieldType.STRING, 8);
        pnd_Vars_Pnd_Dte = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Dte", "#DTE", FieldType.DATE);
        pnd_Vars_Pnd_End_Dte = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_End_Dte", "#END-DTE", FieldType.STRING, 8);

        pnd_Vars__R_Field_4 = pnd_Vars.newGroupInGroup("pnd_Vars__R_Field_4", "REDEFINE", pnd_Vars_Pnd_End_Dte);
        pnd_Vars_Pnd_Dateym = pnd_Vars__R_Field_4.newFieldInGroup("pnd_Vars_Pnd_Dateym", "#DATEYM", FieldType.STRING, 6);
        pnd_Vars_Pnd_Dated = pnd_Vars__R_Field_4.newFieldInGroup("pnd_Vars_Pnd_Dated", "#DATED", FieldType.STRING, 2);
        pnd_Vars_Pnd_Input_Dte = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Input_Dte", "#INPUT-DTE", FieldType.STRING, 8);
        pnd_Vars_Pnd_Phys_Id = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Phys_Id", "#PHYS-ID", FieldType.STRING, 7);

        pnd_Vars__R_Field_5 = pnd_Vars.newGroupInGroup("pnd_Vars__R_Field_5", "REDEFINE", pnd_Vars_Pnd_Phys_Id);
        pnd_Vars_Pnd_Phys_P = pnd_Vars__R_Field_5.newFieldInGroup("pnd_Vars_Pnd_Phys_P", "#PHYS-P", FieldType.STRING, 1);
        pnd_Vars_Pnd_Phys_N = pnd_Vars__R_Field_5.newFieldInGroup("pnd_Vars_Pnd_Phys_N", "#PHYS-N", FieldType.NUMERIC, 6);
        pnd_Vars_Pnd_Spcl_Msg = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Spcl_Msg", "#SPCL-MSG", FieldType.STRING, 72);

        pnd_Vars__R_Field_6 = pnd_Vars.newGroupInGroup("pnd_Vars__R_Field_6", "REDEFINE", pnd_Vars_Pnd_Spcl_Msg);
        pnd_Vars_Pnd_Msg = pnd_Vars__R_Field_6.newFieldInGroup("pnd_Vars_Pnd_Msg", "#MSG", FieldType.STRING, 45);
        pnd_Vars_Pnd_Ssn = pnd_Vars__R_Field_6.newFieldInGroup("pnd_Vars_Pnd_Ssn", "#SSN", FieldType.STRING, 9);
        pnd_Vars_Pnd_Name = pnd_Vars__R_Field_6.newFieldInGroup("pnd_Vars_Pnd_Name", "#NAME", FieldType.STRING, 18);
        pnd_Vars_Pnd_Tcrc_Ctr = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Tcrc_Ctr", "#TCRC-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Vars_Pnd_Tcrc_Ctr_Emi = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Tcrc_Ctr_Emi", "#TCRC-CTR-EMI", FieldType.PACKED_DECIMAL, 7);
        pnd_Vars_Pnd_Tctr_Noncrc = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Tctr_Noncrc", "#TCTR-NONCRC", FieldType.PACKED_DECIMAL, 7);
        pnd_Vars_Pnd_Tctr_Noncrc_Emi = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Tctr_Noncrc_Emi", "#TCTR-NONCRC-EMI", FieldType.PACKED_DECIMAL, 7);
        pnd_Vars_Pnd_Tctr = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Tctr", "#TCTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Vars_Pnd_Tctr_Emi = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Tctr_Emi", "#TCTR-EMI", FieldType.PACKED_DECIMAL, 7);
        pnd_Vars_Pnd_Time = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Time", "#TIME", FieldType.NUMERIC, 7);
        pnd_Vars_Pnd_Cirs_Ctr = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Cirs_Ctr", "#CIRS-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Vars_Pnd_Cirs_Ctr_Emi = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Cirs_Ctr_Emi", "#CIRS-CTR-EMI", FieldType.PACKED_DECIMAL, 7);
        pnd_Vars_Pnd_Wpid_Ctr = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Wpid_Ctr", "#WPID-CTR", FieldType.PACKED_DECIMAL, 5);
        pnd_Vars_Pnd_Wpid_Ctr_Emi = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Wpid_Ctr_Emi", "#WPID-CTR-EMI", FieldType.PACKED_DECIMAL, 5);
        pnd_Vars_Pnd_Wpid_Combined_Ctr = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Wpid_Combined_Ctr", "#WPID-COMBINED-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Vars_Pnd_Wpid_Combined_Ctr_Emi = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Wpid_Combined_Ctr_Emi", "#WPID-COMBINED-CTR-EMI", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Vars_Pnd_Wpid_Crc_Ctr = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Wpid_Crc_Ctr", "#WPID-CRC-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Vars_Pnd_Wpid_Crc_Ctr_Emi = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Wpid_Crc_Ctr_Emi", "#WPID-CRC-CTR-EMI", FieldType.PACKED_DECIMAL, 7);
        pnd_Vars_Pnd_Wpid_Non_Crc_Ctr = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Wpid_Non_Crc_Ctr", "#WPID-NON-CRC-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Vars_Pnd_Wpid_Non_Crc_Ctr_Emi = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Wpid_Non_Crc_Ctr_Emi", "#WPID-NON-CRC-CTR-EMI", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Vars_Pnd_Wpid_Sv = pnd_Vars.newFieldInGroup("pnd_Vars_Pnd_Wpid_Sv", "#WPID-SV", FieldType.STRING, 6);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        sort01Work_Prcss_IdOld = internalLoopRecord.newFieldInRecord("Sort01_Work_Prcss_Id_OLD", "Work_Prcss_Id_OLD", FieldType.STRING, 6);
        registerRecord(internalLoopRecord);

        setTimeRecord = new DbsRecord();
        st = setTimeRecord.newFieldInRecord("ST", "ST", FieldType.NUMERIC, 7);
        registerRecord(setTimeRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_icw_Master_Index.reset();
        vw_unit_Tbl.reset();
        vw_wpid_Tbl.reset();
        internalLoopRecord.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Icwb3410() throws Exception
    {
        super("Icwb3410");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Icwb3410|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        while(true)
        {
            try
            {
                st = Global.getTIMN();                                                                                                                                    //Natural: SET TIME
                //*  FROM MONHTLY JOB (P2000CWM)
                if (condition(DbsUtil.maskMatches(Global.getINIT_PROGRAM(),"'P'......'M'")))                                                                              //Natural: IF *INIT-PROGRAM EQ MASK ( 'P'......'M' )
                {
                    pnd_Vars_Pnd_End_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                 //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO #END-DTE
                    if (condition(pnd_Vars_Pnd_Dated.greaterOrEqual("23")))                                                                                               //Natural: IF #DATED GE '23'
                    {
                        //*  PROCESS CURRENT MONTH
                        pnd_Vars_Pnd_Dated.setValue("01");                                                                                                                //Natural: MOVE '01' TO #DATED
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Vars_Pnd_Dte.setValue(Global.getDATX());                                                                                                      //Natural: MOVE *DATX TO #DTE
                        //*  GET PREVIOUS MONTH
                        pnd_Vars_Pnd_Dte.nsubtract(23);                                                                                                                   //Natural: SUBTRACT 23 FROM #DTE
                        pnd_Vars_Pnd_End_Dte.setValueEdited(pnd_Vars_Pnd_Dte,new ReportEditMask("YYYYMMDD"));                                                             //Natural: MOVE EDITED #DTE ( EM = YYYYMMDD ) TO #END-DTE
                        //*  PROCESS PREVIOUS MONTH
                        pnd_Vars_Pnd_Dated.setValue("01");                                                                                                                //Natural: MOVE '01' TO #DATED
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Vars_Pnd_Input_Dte.setValue(pnd_Vars_Pnd_End_Dte);                                                                                                //Natural: MOVE #END-DTE TO #INPUT-DTE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition(INPUT_1))
                {
                    DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Vars_Pnd_Input_Dte);                                                                           //Natural: INPUT #INPUT-DTE
                }                                                                                                                                                         //Natural: END-IF
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: FORMAT ( 1 ) PS = 58 LS = 132;//Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape())){return;}
                getReports().newPage(new ReportSpecification(2));                                                                                                         //Natural: FORMAT ( 2 ) PS = 58 LS = 132;//Natural: NEWPAGE ( 2 )
                if (condition(Global.isEscape())){return;}
                getReports().getPageNumberDbs(1).reset();                                                                                                                 //Natural: RESET *PAGE-NUMBER ( 1 )
                getReports().getPageNumberDbs(2).reset();                                                                                                                 //Natural: RESET *PAGE-NUMBER ( 2 )
                if (condition(DbsUtil.maskMatches(pnd_Vars_Pnd_Input_Dte,"NNNNNNNN")))                                                                                    //Natural: IF #INPUT-DTE EQ MASK ( NNNNNNNN )
                {
                    pnd_Vars_Pnd_End_Dte.setValue(pnd_Vars_Pnd_Input_Dte);                                                                                                //Natural: MOVE #INPUT-DTE TO #END-DTE
                    pnd_Vars_Pnd_Dated.setValue("31");                                                                                                                    //Natural: MOVE '31' TO #DATED
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Vars_Pnd_Input_Dte.setValue("19950801");                                                                                                          //Natural: MOVE '19950801' TO #INPUT-DTE
                    pnd_Vars_Pnd_End_Dte.setValue("19950831");                                                                                                            //Natural: MOVE '19950831' TO #END-DTE
                }                                                                                                                                                         //Natural: END-IF
                vw_icw_Master_Index.startDatabaseRead                                                                                                                     //Natural: READ ICW-MASTER-INDEX BY ACTV-UNQUE-KEY FROM #INPUT-DTE
                (
                "READ01",
                new Wc[] { new Wc("ACTV_UNQUE_KEY", ">=", pnd_Vars_Pnd_Input_Dte, WcType.BY) },
                new Oc[] { new Oc("ACTV_UNQUE_KEY", "ASC") }
                );
                READ01:
                while (condition(vw_icw_Master_Index.readNextRow("READ01")))
                {
                    pnd_Vars_Pnd_Date_Time.setValue(icw_Master_Index_Rqst_Log_Dte_Tme);                                                                                   //Natural: MOVE RQST-LOG-DTE-TME TO #DATE-TIME
                    if (condition(pnd_Vars_Pnd_Date.greater(pnd_Vars_Pnd_End_Dte)))                                                                                       //Natural: IF #DATE GT #END-DTE
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM IMMEDIATE
                    }                                                                                                                                                     //Natural: END-IF
                    //*   TEST, DEVELOPMENT
                    if (condition(DbsUtil.maskMatches(Global.getLIBRARY_ID(),"'PROJ'*")))                                                                                 //Natural: IF *LIBRARY-ID EQ MASK ( 'PROJ'* )
                    {
                        if (condition(!(icw_Master_Index_Rqst_Orgn_Cde.equals("M") || icw_Master_Index_Rqst_Orgn_Cde.equals("I"))))                                       //Natural: ACCEPT IF RQST-ORGN-CDE = 'M' OR = 'I'
                        {
                            continue;
                        }
                        //*  PRODUCTION
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(!(icw_Master_Index_Rqst_Orgn_Cde.equals("X") || icw_Master_Index_Rqst_Orgn_Cde.equals("E") || icw_Master_Index_Rqst_Orgn_Cde.equals("N")))) //Natural: ACCEPT IF RQST-ORGN-CDE = 'X' OR = 'E' OR = 'N'
                        {
                            continue;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    //*  MOVE PHYSCL-FLDR-ID-NBR TO #PHYS-N
                    //*  IF RQST-ORGN-CDE = 'J'
                    //*    MOVE 'R' TO #PHYS-P
                    //*  ELSE
                    //*    IF MJ-PULL-IND = 'Y' OR = 'R'
                    //*      MOVE 'M' TO #PHYS-P
                    //*    ELSE
                    //*      MOVE 'F' TO #PHYS-P
                    //*    END-IF
                    //*  END-IF
                    getSort().writeSortInData(icw_Master_Index_Work_Prcss_Id, icw_Master_Index_Tiaa_Rcvd_Dte_Tme, icw_Master_Index_Ppg_Cde, icw_Master_Index_Rqst_Log_Unit_Cde,  //Natural: END-ALL
                        icw_Master_Index_Rqst_Orgn_Cde, icw_Master_Index_Admin_Unit_Cde, icw_Master_Index_Unit_Cde, icw_Master_Index_Unit_Updte_Dte_Tme, 
                        icw_Master_Index_Status_Cde, icw_Master_Index_Empl_Racf_Id);
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                getSort().sortData(icw_Master_Index_Work_Prcss_Id, icw_Master_Index_Tiaa_Rcvd_Dte_Tme);                                                                   //Natural: SORT BY WORK-PRCSS-ID TIAA-RCVD-DTE-TME USING PPG-CDE RQST-LOG-UNIT-CDE RQST-ORGN-CDE ADMIN-UNIT-CDE UNIT-CDE UNIT-UPDTE-DTE-TME STATUS-CDE EMPL-RACF-ID
                boolean endOfDataSort01 = true;
                boolean firstSort01 = true;
                SORT01:
                while (condition(getSort().readSortOutData(icw_Master_Index_Work_Prcss_Id, icw_Master_Index_Tiaa_Rcvd_Dte_Tme, icw_Master_Index_Ppg_Cde, 
                    icw_Master_Index_Rqst_Log_Unit_Cde, icw_Master_Index_Rqst_Orgn_Cde, icw_Master_Index_Admin_Unit_Cde, icw_Master_Index_Unit_Cde, icw_Master_Index_Unit_Updte_Dte_Tme, 
                    icw_Master_Index_Status_Cde, icw_Master_Index_Empl_Racf_Id)))
                {
                    if (condition(getSort().getAstCOUNTER().greater(0)))
                    {
                        atBreakEventSort01(false);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom()))
                                break;
                            else if (condition(Global.isEscapeBottomImmediate()))
                            {
                                endOfDataSort01 = false;
                                break;
                            }
                            else if (condition(Global.isEscapeTop()))
                            continue;
                            else if (condition())
                            return;
                        }
                    }
                    //*    PHYSCL-FLDR-ID-NBR
                    pnd_Vars_Pnd_Date_Time.setValue(icw_Master_Index_Unit_Updte_Dte_Tme);                                                                                 //Natural: AT BREAK OF TIAA-RCVD-DTE-TME;//Natural: AT BREAK OF WORK-PRCSS-ID;//Natural: MOVE UNIT-UPDTE-DTE-TME TO #DATE-TIME
                    //*  PRODUCTION
                    if (condition(! (DbsUtil.maskMatches(Global.getLIBRARY_ID(),"'PROJ'*"))))                                                                             //Natural: IF *LIBRARY-ID NE MASK ( 'PROJ'* )
                    {
                        //*  FAX
                        if (condition(icw_Master_Index_Rqst_Orgn_Cde.equals("X")))                                                                                        //Natural: IF RQST-ORGN-CDE EQ 'X'
                        {
                            pnd_Vars_Pnd_Combined_Ctr.nadd(1);                                                                                                            //Natural: ADD 1 TO #COMBINED-CTR
                            //*      IF UNIT-CDE EQ 'CIRS'
                            if (condition(icw_Master_Index_Rqst_Log_Unit_Cde.equals("CIRS")))                                                                             //Natural: IF RQST-LOG-UNIT-CDE EQ 'CIRS'
                            {
                                pnd_Vars_Pnd_Cirs_Ctr.nadd(1);                                                                                                            //Natural: ADD 1 TO #CIRS-CTR
                            }                                                                                                                                             //Natural: END-IF
                            //*  E-MAIL, INTERNET
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Vars_Pnd_Combined_Ctr_Emi.nadd(1);                                                                                                        //Natural: ADD 1 TO #COMBINED-CTR-EMI
                            if (condition(icw_Master_Index_Rqst_Log_Unit_Cde.equals("CIRS")))                                                                             //Natural: IF RQST-LOG-UNIT-CDE EQ 'CIRS'
                            {
                                pnd_Vars_Pnd_Cirs_Ctr_Emi.nadd(1);                                                                                                        //Natural: ADD 1 TO #CIRS-CTR-EMI
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                        //*  TEST/DEVELOPMENT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  FAX
                        if (condition(icw_Master_Index_Rqst_Orgn_Cde.equals("M")))                                                                                        //Natural: IF RQST-ORGN-CDE EQ 'M'
                        {
                            pnd_Vars_Pnd_Combined_Ctr.nadd(1);                                                                                                            //Natural: ADD 1 TO #COMBINED-CTR
                            if (condition(icw_Master_Index_Rqst_Log_Unit_Cde.equals("CIRS")))                                                                             //Natural: IF RQST-LOG-UNIT-CDE EQ 'CIRS'
                            {
                                pnd_Vars_Pnd_Cirs_Ctr.nadd(1);                                                                                                            //Natural: ADD 1 TO #CIRS-CTR
                            }                                                                                                                                             //Natural: END-IF
                            //*  E-MAIL, INTERNET
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Vars_Pnd_Combined_Ctr_Emi.nadd(1);                                                                                                        //Natural: ADD 1 TO #COMBINED-CTR-EMI
                            if (condition(icw_Master_Index_Rqst_Log_Unit_Cde.equals("CIRS")))                                                                             //Natural: IF RQST-LOG-UNIT-CDE EQ 'CIRS'
                            {
                                pnd_Vars_Pnd_Cirs_Ctr_Emi.nadd(1);                                                                                                        //Natural: ADD 1 TO #CIRS-CTR-EMI
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    sort01Work_Prcss_IdOld.setValue(icw_Master_Index_Work_Prcss_Id);                                                                                      //Natural: END-SORT
                }
                if (condition(getSort().getAstCOUNTER().greater(0)))
                {
                    atBreakEventSort01(endOfDataSort01);
                }
                endSort();
                getReports().write(1, ReportOption.NOTITLE,"TIME ELAPSED: ",st, new ReportEditMask ("99':'99':'99'.'9"));                                                 //Natural: WRITE ( 1 ) 'TIME ELAPSED: ' *TIMD ( ST. ) ( EM = 99':'99':'99'.'9 )
                if (Global.isEscape()) return;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(3),"GRAND TOTALS  ",new TabSetting(72),pnd_Vars_Pnd_Tcrc_Ctr,new                //Natural: WRITE ( 1 ) // 3T 'GRAND TOTALS  ' 72T #TCRC-CTR 87T #TCTR-NONCRC 102T #TCTR
                    TabSetting(87),pnd_Vars_Pnd_Tctr_Noncrc,new TabSetting(102),pnd_Vars_Pnd_Tctr);
                if (Global.isEscape()) return;
                //*  WRITE (2) 'TIME ELAPSED: ' *TIMD(ST.) (EM=99':'99':'99'.'9)
                //*  WRITE (2) // 3T 'GRAND TOTALS  '
                //*     72T #TCRC-CTR-EMI
                //*     87T #TCTR-NONCRC-EMI
                //*    102T #TCTR-EMI
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( 1 )
                //*  AT TOP OF PAGE (2)
                //*   WRITE (2) NOTITLE NOHDR 'Page:' *PAGE-NUMBER(2)  3X *PROGRAM /
                //*  'Stats on E-Mail/Internet Docs Logged and Indexed on
                //*   INSTITUTIONAL FILE'
                //*     75T '   By'
                //*     90T '   By    '
                //*    105T 'By CIRS &' /
                //*     '   For the Period' #INPUT-DTE '-' #END-DTE
                //*     75T '  CIRS'
                //*     90T 'Non-CIRS '
                //*    105T 'Non-CIRS' //
                //*  END-TOPPAGE
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,"Page:",getReports().getPageNumberDbs(1),new ColumnSpacing(3),Global.getPROGRAM(),NEWLINE,"Stats on Fax Docs Logged and Indexed on Institutional file",new  //Natural: WRITE ( 1 ) NOTITLE NOHDR 'Page:' *PAGE-NUMBER ( 1 ) 3X *PROGRAM / 'Stats on Fax Docs Logged and Indexed on Institutional file' 75T '   By' 90T '   By    ' 105T 'By CIRS &' / '   For the Period' #INPUT-DTE '-' #END-DTE 75T '  CIRS' 90T 'Non-CIRS ' 105T 'Non-CIRS' //
                        TabSetting(75),"   By",new TabSetting(90),"   By    ",new TabSetting(105),"By CIRS &",NEWLINE,"   For the Period",pnd_Vars_Pnd_Input_Dte,"-",pnd_Vars_Pnd_End_Dte,new 
                        TabSetting(75),"  CIRS",new TabSetting(90),"Non-CIRS ",new TabSetting(105),"Non-CIRS",NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean icw_Master_Index_Tiaa_Rcvd_Dte_TmeIsBreak = icw_Master_Index_Tiaa_Rcvd_Dte_Tme.isBreak(endOfData);
        boolean icw_Master_Index_Work_Prcss_IdIsBreak = icw_Master_Index_Work_Prcss_Id.isBreak(endOfData);
        if (condition(icw_Master_Index_Tiaa_Rcvd_Dte_TmeIsBreak || icw_Master_Index_Work_Prcss_IdIsBreak))
        {
            //*    WRITE (1) 5T 'Total for TIAA-RCVD-DT' OLD(TIAA-RCVD-DTE)
            //*      'of' OLD(WORK-PRCSS-ID) ' = ' 67T #COMBINED-CTR
            //*      / 50T 'LOGGED BY CRC = ' 67T #CIRS-CTR
            pnd_Vars_Pnd_Wpid_Crc_Ctr.nadd(pnd_Vars_Pnd_Cirs_Ctr);                                                                                                        //Natural: ADD #CIRS-CTR TO #WPID-CRC-CTR
            pnd_Vars_Pnd_Wpid_Combined_Ctr.nadd(pnd_Vars_Pnd_Combined_Ctr);                                                                                               //Natural: ADD #COMBINED-CTR TO #WPID-COMBINED-CTR
            pnd_Vars_Pnd_Cirs_Ctr.setValue(0);                                                                                                                            //Natural: MOVE 0 TO #CIRS-CTR
            pnd_Vars_Pnd_Combined_Ctr.setValue(0);                                                                                                                        //Natural: MOVE 0 TO #COMBINED-CTR
            pnd_Vars_Pnd_Wpid_Crc_Ctr_Emi.nadd(pnd_Vars_Pnd_Cirs_Ctr_Emi);                                                                                                //Natural: ADD #CIRS-CTR-EMI TO #WPID-CRC-CTR-EMI
            pnd_Vars_Pnd_Wpid_Combined_Ctr_Emi.nadd(pnd_Vars_Pnd_Combined_Ctr_Emi);                                                                                       //Natural: ADD #COMBINED-CTR-EMI TO #WPID-COMBINED-CTR-EMI
            pnd_Vars_Pnd_Cirs_Ctr_Emi.setValue(0);                                                                                                                        //Natural: MOVE 0 TO #CIRS-CTR-EMI
            pnd_Vars_Pnd_Combined_Ctr_Emi.setValue(0);                                                                                                                    //Natural: MOVE 0 TO #COMBINED-CTR-EMI
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(icw_Master_Index_Work_Prcss_IdIsBreak))
        {
            vw_wpid_Tbl.reset();                                                                                                                                          //Natural: RESET WPID-TBL
            pnd_Vars_Pnd_Wpid_Sv.setValue(sort01Work_Prcss_IdOld);                                                                                                        //Natural: MOVE OLD ( WORK-PRCSS-ID ) TO #WPID-SV
            vw_wpid_Tbl.startDatabaseFind                                                                                                                                 //Natural: FIND ( 01 ) WPID-TBL WITH WPID-UNIQ-KEY = #WPID-SV
            (
            "FIND01",
            new Wc[] { new Wc("WPID_UNIQ_KEY", "=", pnd_Vars_Pnd_Wpid_Sv, WcType.WITH) },
            1
            );
            FIND01:
            while (condition(vw_wpid_Tbl.readNextRow("FIND01")))
            {
                vw_wpid_Tbl.setIfNotFoundControlFlag(false);
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape())) return;
            pnd_Vars_Pnd_Wpid_Non_Crc_Ctr.compute(new ComputeParameters(false, pnd_Vars_Pnd_Wpid_Non_Crc_Ctr), pnd_Vars_Pnd_Wpid_Combined_Ctr.subtract(pnd_Vars_Pnd_Wpid_Crc_Ctr)); //Natural: COMPUTE #WPID-NON-CRC-CTR = #WPID-COMBINED-CTR - #WPID-CRC-CTR
            if (condition(pnd_Vars_Pnd_Wpid_Combined_Ctr.greater(getZero()) || pnd_Vars_Pnd_Wpid_Crc_Ctr.greater(getZero())))                                             //Natural: IF #WPID-COMBINED-CTR GT 0 OR #WPID-CRC-CTR GT 0
            {
                pnd_Vars_Pnd_Wpid_Ctr.nadd(1);                                                                                                                            //Natural: ADD 1 TO #WPID-CTR
                getReports().write(1, ReportOption.NOTITLE,pnd_Vars_Pnd_Wpid_Ctr,"(",sort01Work_Prcss_IdOld,")",wpid_Tbl_Work_Prcss_Long_Nme,"=",new TabSetting(72),pnd_Vars_Pnd_Wpid_Crc_Ctr,new  //Natural: WRITE ( 1 ) #WPID-CTR '(' OLD ( WORK-PRCSS-ID ) ')' WPID-TBL.WORK-PRCSS-LONG-NME '=' 72T #WPID-CRC-CTR 87T #WPID-NON-CRC-CTR 102T #WPID-COMBINED-CTR /
                    TabSetting(87),pnd_Vars_Pnd_Wpid_Non_Crc_Ctr,new TabSetting(102),pnd_Vars_Pnd_Wpid_Combined_Ctr,NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            pnd_Vars_Pnd_Tcrc_Ctr.nadd(pnd_Vars_Pnd_Wpid_Crc_Ctr);                                                                                                        //Natural: ADD #WPID-CRC-CTR TO #TCRC-CTR
            pnd_Vars_Pnd_Tctr_Noncrc.nadd(pnd_Vars_Pnd_Wpid_Non_Crc_Ctr);                                                                                                 //Natural: ADD #WPID-NON-CRC-CTR TO #TCTR-NONCRC
            pnd_Vars_Pnd_Tctr.nadd(pnd_Vars_Pnd_Wpid_Combined_Ctr);                                                                                                       //Natural: ADD #WPID-COMBINED-CTR TO #TCTR
            pnd_Vars_Pnd_Wpid_Crc_Ctr.setValue(0);                                                                                                                        //Natural: MOVE 0 TO #WPID-CRC-CTR
            pnd_Vars_Pnd_Wpid_Combined_Ctr.setValue(0);                                                                                                                   //Natural: MOVE 0 TO #WPID-COMBINED-CTR
            pnd_Vars_Pnd_Wpid_Non_Crc_Ctr_Emi.compute(new ComputeParameters(false, pnd_Vars_Pnd_Wpid_Non_Crc_Ctr_Emi), pnd_Vars_Pnd_Wpid_Combined_Ctr_Emi.subtract(pnd_Vars_Pnd_Wpid_Crc_Ctr_Emi)); //Natural: COMPUTE #WPID-NON-CRC-CTR-EMI = #WPID-COMBINED-CTR-EMI - #WPID-CRC-CTR-EMI
            if (condition(pnd_Vars_Pnd_Wpid_Combined_Ctr_Emi.greater(getZero()) || pnd_Vars_Pnd_Wpid_Crc_Ctr_Emi.greater(getZero())))                                     //Natural: IF #WPID-COMBINED-CTR-EMI GT 0 OR #WPID-CRC-CTR-EMI GT 0
            {
                pnd_Vars_Pnd_Wpid_Ctr_Emi.nadd(1);                                                                                                                        //Natural: ADD 1 TO #WPID-CTR-EMI
                getReports().write(2, pnd_Vars_Pnd_Wpid_Ctr_Emi,"(",sort01Work_Prcss_IdOld,")",wpid_Tbl_Work_Prcss_Long_Nme,"=",new TabSetting(72),pnd_Vars_Pnd_Wpid_Crc_Ctr_Emi,new  //Natural: WRITE ( 2 ) #WPID-CTR-EMI '(' OLD ( WORK-PRCSS-ID ) ')' WPID-TBL.WORK-PRCSS-LONG-NME '=' 72T #WPID-CRC-CTR-EMI 87T #WPID-NON-CRC-CTR-EMI 102T #WPID-COMBINED-CTR-EMI /
                    TabSetting(87),pnd_Vars_Pnd_Wpid_Non_Crc_Ctr_Emi,new TabSetting(102),pnd_Vars_Pnd_Wpid_Combined_Ctr_Emi,NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            pnd_Vars_Pnd_Tcrc_Ctr_Emi.nadd(pnd_Vars_Pnd_Wpid_Crc_Ctr_Emi);                                                                                                //Natural: ADD #WPID-CRC-CTR-EMI TO #TCRC-CTR-EMI
            pnd_Vars_Pnd_Tctr_Noncrc_Emi.nadd(pnd_Vars_Pnd_Wpid_Non_Crc_Ctr_Emi);                                                                                         //Natural: ADD #WPID-NON-CRC-CTR-EMI TO #TCTR-NONCRC-EMI
            pnd_Vars_Pnd_Tctr_Emi.nadd(pnd_Vars_Pnd_Wpid_Combined_Ctr_Emi);                                                                                               //Natural: ADD #WPID-COMBINED-CTR-EMI TO #TCTR-EMI
            pnd_Vars_Pnd_Wpid_Crc_Ctr_Emi.setValue(0);                                                                                                                    //Natural: MOVE 0 TO #WPID-CRC-CTR-EMI
            pnd_Vars_Pnd_Wpid_Combined_Ctr_Emi.setValue(0);                                                                                                               //Natural: MOVE 0 TO #WPID-COMBINED-CTR-EMI
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=58 LS=132");
        Global.format(2, "PS=58 LS=132");
    }
}
