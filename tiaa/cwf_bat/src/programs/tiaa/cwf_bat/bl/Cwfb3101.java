/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:28:07 PM
**        * FROM NATURAL PROGRAM : Cwfb3101
************************************************************
**        * FILE NAME            : Cwfb3101.java
**        * CLASS NAME           : Cwfb3101
**        * INSTANCE NAME        : Cwfb3101
************************************************************
**SAG GENERATOR: SHELL-TIAA                       VERSION: 3.2.2
**SAG TITLE: REPORT 1
**SAG SYSTEM: CRPCWF
************************************************************************
* PROGRAM  : CWFB3001
* SYSTEM   : CRPCWF
* TITLE    : REPORT 1
* GENERATED: AUG 11,93 AT 10:46 AM
* FUNCTION : REPORT OF WORK REQUEST LOGGED AND INDEXED
*          | BATCH REGULAR
*          |
*          |
*          |
*          |
*          |
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* MMM DD YY  ________ ______________________________________________
* ______________________________________________
************************************************************************
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfb3101 extends BLNatBase
{
    // Data Areas
    private PdaCwfa3001 pdaCwfa3001;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Master_Index;
    private DbsField cwf_Master_Index_Rqst_Log_Dte_Tme;

    private DbsGroup cwf_Master_Index__R_Field_1;
    private DbsField cwf_Master_Index_Rqst_Log_Index_Dte;
    private DbsField cwf_Master_Index_Rqst_Log_Index_Tme;

    private DataAccessProgramView vw_hst_Master_Index;
    private DbsField hst_Master_Index_Rqst_Log_Dte_Tme;
    private DbsField hst_Master_Index_Rqst_Log_Oprtr_Cde;
    private DbsField hst_Master_Index_Work_Prcss_Id;

    private DbsGroup hst_Master_Index__R_Field_2;
    private DbsField hst_Master_Index_Work_Actn_Rqstd_Cde;
    private DbsField hst_Master_Index_Work_Lob_Cmpny_Prdct_Cde;
    private DbsField hst_Master_Index_Work_Mjr_Bsnss_Prcss_Cde;
    private DbsField hst_Master_Index_Work_Spcfc_Bsnss_Prcss_Cde;
    private DbsField hst_Master_Index_Last_Chnge_Unit_Cde;
    private DbsField pnd_Env;
    private DbsField pnd_Page;
    private DbsField pnd_Sort_Unit;
    private DbsField pnd_Work_Date;
    private DbsField pnd_To;
    private DbsField pnd_From;
    private DbsField pnd_Day;
    private DbsField pnd_Sub;
    private DbsField pnd_New_Racf;
    private DbsField pnd_New_Unit;
    private DbsField pnd_Yyyymmdd;

    private DbsGroup pnd_Yyyymmdd__R_Field_3;
    private DbsField pnd_Yyyymmdd_Pnd_Century;
    private DbsField pnd_Yyyymmdd_Pnd_Yy;
    private DbsField pnd_Yyyymmdd_Pnd_Mm;
    private DbsField pnd_Yyyymmdd_Pnd_Dd;
    private DbsField pnd_Report_No;
    private DbsField pnd_Racf_Id;
    private DbsField pnd_Floor;
    private DbsField pnd_Bldg;
    private DbsField pnd_Drop_Off;
    private DbsField pnd_Unit_Name;
    private DbsField pnd_Report_Parm;
    private DbsField pnd_Tbl_Run_Flag;
    private DbsField pnd_Parm_Unit;
    private DbsField pnd_Comp_Date;
    private DbsField pnd_Reccount;
    private DbsField pnd_Work_Start_Date;
    private DbsField pnd_Work_Comp_Date;
    private DbsField pnd_Date_Diff;
    private DbsField pnd_Oprtr_Cde;
    private DbsField pnd_Work_Unit;
    private DbsField pnd_Parm_Type;

    private DbsRecord internalLoopRecord;
    private DbsField sort01Tbl_WpidOld;
    private DbsField sort01Tbl_WpidCount399;
    private DbsField sort01Tbl_WpidCount;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCwfa3001 = new PdaCwfa3001(localVariables);

        // Local Variables

        vw_cwf_Master_Index = new DataAccessProgramView(new NameInfo("vw_cwf_Master_Index", "CWF-MASTER-INDEX"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        cwf_Master_Index_Rqst_Log_Dte_Tme = vw_cwf_Master_Index.getRecord().newFieldInGroup("cwf_Master_Index_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Master_Index_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");

        cwf_Master_Index__R_Field_1 = vw_cwf_Master_Index.getRecord().newGroupInGroup("cwf_Master_Index__R_Field_1", "REDEFINE", cwf_Master_Index_Rqst_Log_Dte_Tme);
        cwf_Master_Index_Rqst_Log_Index_Dte = cwf_Master_Index__R_Field_1.newFieldInGroup("cwf_Master_Index_Rqst_Log_Index_Dte", "RQST-LOG-INDEX-DTE", 
            FieldType.STRING, 8);
        cwf_Master_Index_Rqst_Log_Index_Tme = cwf_Master_Index__R_Field_1.newFieldInGroup("cwf_Master_Index_Rqst_Log_Index_Tme", "RQST-LOG-INDEX-TME", 
            FieldType.STRING, 7);
        registerRecord(vw_cwf_Master_Index);

        vw_hst_Master_Index = new DataAccessProgramView(new NameInfo("vw_hst_Master_Index", "HST-MASTER-INDEX"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        hst_Master_Index_Rqst_Log_Dte_Tme = vw_hst_Master_Index.getRecord().newFieldInGroup("hst_Master_Index_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        hst_Master_Index_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        hst_Master_Index_Rqst_Log_Oprtr_Cde = vw_hst_Master_Index.getRecord().newFieldInGroup("hst_Master_Index_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_LOG_OPRTR_CDE");
        hst_Master_Index_Rqst_Log_Oprtr_Cde.setDdmHeader("ENTRY/OPERATOR");
        hst_Master_Index_Work_Prcss_Id = vw_hst_Master_Index.getRecord().newFieldInGroup("hst_Master_Index_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "WORK_PRCSS_ID");
        hst_Master_Index_Work_Prcss_Id.setDdmHeader("WORK/ID");

        hst_Master_Index__R_Field_2 = vw_hst_Master_Index.getRecord().newGroupInGroup("hst_Master_Index__R_Field_2", "REDEFINE", hst_Master_Index_Work_Prcss_Id);
        hst_Master_Index_Work_Actn_Rqstd_Cde = hst_Master_Index__R_Field_2.newFieldInGroup("hst_Master_Index_Work_Actn_Rqstd_Cde", "WORK-ACTN-RQSTD-CDE", 
            FieldType.STRING, 1);
        hst_Master_Index_Work_Lob_Cmpny_Prdct_Cde = hst_Master_Index__R_Field_2.newFieldInGroup("hst_Master_Index_Work_Lob_Cmpny_Prdct_Cde", "WORK-LOB-CMPNY-PRDCT-CDE", 
            FieldType.STRING, 2);
        hst_Master_Index_Work_Mjr_Bsnss_Prcss_Cde = hst_Master_Index__R_Field_2.newFieldInGroup("hst_Master_Index_Work_Mjr_Bsnss_Prcss_Cde", "WORK-MJR-BSNSS-PRCSS-CDE", 
            FieldType.STRING, 1);
        hst_Master_Index_Work_Spcfc_Bsnss_Prcss_Cde = hst_Master_Index__R_Field_2.newFieldInGroup("hst_Master_Index_Work_Spcfc_Bsnss_Prcss_Cde", "WORK-SPCFC-BSNSS-PRCSS-CDE", 
            FieldType.STRING, 2);
        hst_Master_Index_Last_Chnge_Unit_Cde = vw_hst_Master_Index.getRecord().newFieldInGroup("hst_Master_Index_Last_Chnge_Unit_Cde", "LAST-CHNGE-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "LAST_CHNGE_UNIT_CDE");
        hst_Master_Index_Last_Chnge_Unit_Cde.setDdmHeader("LAST/CHANGE/UNIT");
        registerRecord(vw_hst_Master_Index);

        pnd_Env = localVariables.newFieldInRecord("pnd_Env", "#ENV", FieldType.STRING, 10);
        pnd_Page = localVariables.newFieldInRecord("pnd_Page", "#PAGE", FieldType.NUMERIC, 5);
        pnd_Sort_Unit = localVariables.newFieldInRecord("pnd_Sort_Unit", "#SORT-UNIT", FieldType.STRING, 1);
        pnd_Work_Date = localVariables.newFieldInRecord("pnd_Work_Date", "#WORK-DATE", FieldType.DATE);
        pnd_To = localVariables.newFieldInRecord("pnd_To", "#TO", FieldType.DATE);
        pnd_From = localVariables.newFieldInRecord("pnd_From", "#FROM", FieldType.DATE);
        pnd_Day = localVariables.newFieldInRecord("pnd_Day", "#DAY", FieldType.STRING, 3);
        pnd_Sub = localVariables.newFieldInRecord("pnd_Sub", "#SUB", FieldType.NUMERIC, 1);
        pnd_New_Racf = localVariables.newFieldInRecord("pnd_New_Racf", "#NEW-RACF", FieldType.BOOLEAN, 1);
        pnd_New_Unit = localVariables.newFieldInRecord("pnd_New_Unit", "#NEW-UNIT", FieldType.BOOLEAN, 1);
        pnd_Yyyymmdd = localVariables.newFieldInRecord("pnd_Yyyymmdd", "#YYYYMMDD", FieldType.STRING, 8);

        pnd_Yyyymmdd__R_Field_3 = localVariables.newGroupInRecord("pnd_Yyyymmdd__R_Field_3", "REDEFINE", pnd_Yyyymmdd);
        pnd_Yyyymmdd_Pnd_Century = pnd_Yyyymmdd__R_Field_3.newFieldInGroup("pnd_Yyyymmdd_Pnd_Century", "#CENTURY", FieldType.STRING, 2);
        pnd_Yyyymmdd_Pnd_Yy = pnd_Yyyymmdd__R_Field_3.newFieldInGroup("pnd_Yyyymmdd_Pnd_Yy", "#YY", FieldType.STRING, 2);
        pnd_Yyyymmdd_Pnd_Mm = pnd_Yyyymmdd__R_Field_3.newFieldInGroup("pnd_Yyyymmdd_Pnd_Mm", "#MM", FieldType.STRING, 2);
        pnd_Yyyymmdd_Pnd_Dd = pnd_Yyyymmdd__R_Field_3.newFieldInGroup("pnd_Yyyymmdd_Pnd_Dd", "#DD", FieldType.STRING, 2);
        pnd_Report_No = localVariables.newFieldInRecord("pnd_Report_No", "#REPORT-NO", FieldType.NUMERIC, 2);
        pnd_Racf_Id = localVariables.newFieldInRecord("pnd_Racf_Id", "#RACF-ID", FieldType.STRING, 8);
        pnd_Floor = localVariables.newFieldInRecord("pnd_Floor", "#FLOOR", FieldType.NUMERIC, 2);
        pnd_Bldg = localVariables.newFieldInRecord("pnd_Bldg", "#BLDG", FieldType.STRING, 3);
        pnd_Drop_Off = localVariables.newFieldInRecord("pnd_Drop_Off", "#DROP-OFF", FieldType.STRING, 2);
        pnd_Unit_Name = localVariables.newFieldInRecord("pnd_Unit_Name", "#UNIT-NAME", FieldType.STRING, 45);
        pnd_Report_Parm = localVariables.newFieldInRecord("pnd_Report_Parm", "#REPORT-PARM", FieldType.STRING, 17);
        pnd_Tbl_Run_Flag = localVariables.newFieldInRecord("pnd_Tbl_Run_Flag", "#TBL-RUN-FLAG", FieldType.STRING, 1);
        pnd_Parm_Unit = localVariables.newFieldInRecord("pnd_Parm_Unit", "#PARM-UNIT", FieldType.STRING, 7);
        pnd_Comp_Date = localVariables.newFieldInRecord("pnd_Comp_Date", "#COMP-DATE", FieldType.STRING, 8);
        pnd_Reccount = localVariables.newFieldInRecord("pnd_Reccount", "#RECCOUNT", FieldType.PACKED_DECIMAL, 10);
        pnd_Work_Start_Date = localVariables.newFieldInRecord("pnd_Work_Start_Date", "#WORK-START-DATE", FieldType.DATE);
        pnd_Work_Comp_Date = localVariables.newFieldInRecord("pnd_Work_Comp_Date", "#WORK-COMP-DATE", FieldType.DATE);
        pnd_Date_Diff = localVariables.newFieldInRecord("pnd_Date_Diff", "#DATE-DIFF", FieldType.NUMERIC, 3);
        pnd_Oprtr_Cde = localVariables.newFieldInRecord("pnd_Oprtr_Cde", "#OPRTR-CDE", FieldType.STRING, 8);
        pnd_Work_Unit = localVariables.newFieldInRecord("pnd_Work_Unit", "#WORK-UNIT", FieldType.STRING, 8);
        pnd_Parm_Type = localVariables.newFieldInRecord("pnd_Parm_Type", "#PARM-TYPE", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        sort01Tbl_WpidOld = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Wpid_OLD", "Tbl_Wpid_OLD", FieldType.STRING, 6);
        sort01Tbl_WpidCount399 = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Wpid_COUNT_399", "Tbl_Wpid_COUNT_399", FieldType.NUMERIC, 9);
        sort01Tbl_WpidCount = internalLoopRecord.newFieldInRecord("Sort01_Tbl_Wpid_COUNT", "Tbl_Wpid_COUNT", FieldType.NUMERIC, 9);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Master_Index.reset();
        vw_hst_Master_Index.reset();
        internalLoopRecord.reset();

        localVariables.reset();
        pnd_Report_No.setInitialValue(1);
        pnd_Report_Parm.setInitialValue("CWFB3001W*");
        pnd_Parm_Type.setInitialValue("W");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Cwfb3101() throws Exception
    {
        super("Cwfb3101");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("CWFB3101", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //* *********************************
        //*  COPYCODE : CWFC3000
        //*  FUNCTION : SETUP ENVONMENT MESSAGE
        //*  AUTHOR   : PATINGO, JOSEPH S.
        //* *********************************
        pnd_Env.setValue(Global.getLIBRARY_ID());                                                                                                                         //Natural: MOVE *LIBRARY-ID TO #ENV
        if (condition(pnd_Env.equals("PROJCWF") || pnd_Env.equals("PROJCWF")))                                                                                            //Natural: IF #ENV = 'PROJCWF' OR #ENV = 'PROJCWF'
        {
            pnd_Env.setValue("DEV'T ");                                                                                                                                   //Natural: MOVE 'DEV"T ' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("PRACANN")))                                                                                                                         //Natural: IF #ENV = 'PRACANN'
        {
            pnd_Env.setValue("PRACTICE");                                                                                                                                 //Natural: MOVE 'PRACTICE' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("PRODANN")))                                                                                                                         //Natural: IF #ENV = 'PRODANN'
        {
            pnd_Env.setValue(" ");                                                                                                                                        //Natural: MOVE ' ' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT034")))                                                                                                                         //Natural: IF #ENV = 'ACPT034'
        {
            pnd_Env.setValue("RGN:AT07");                                                                                                                                 //Natural: MOVE 'RGN:AT07' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT023")))                                                                                                                         //Natural: IF #ENV = 'ACPT023'
        {
            pnd_Env.setValue("RGN:AT06");                                                                                                                                 //Natural: MOVE 'RGN:AT06' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Env.equals("ACPT028")))                                                                                                                         //Natural: IF #ENV = 'ACPT028'
        {
            pnd_Env.setValue("RGN:AT05");                                                                                                                                 //Natural: MOVE 'RGN:AT05' TO #ENV
        }                                                                                                                                                                 //Natural: END-IF
        //* ****** END COPYCODE *************
        if (condition(Global.getLIBRARY_ID().notEquals("PRODANN")))                                                                                                       //Natural: IF *LIBRARY-ID NE 'PRODANN'
        {
            pnd_Env.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "(", pnd_Env, ")"));                                                                         //Natural: COMPRESS '(' #ENV ')' INTO #ENV LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-IF
        //* ********************
        //*                    *
        //*  REPORT SECTION    *
        //* ********************
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 80 PS = 58
        pdaCwfa3001.getPnd_Rep_Parm_Pnd_Filler1().setValue("/");                                                                                                          //Natural: MOVE '/' TO #FILLER1 #FILLER2
        pdaCwfa3001.getPnd_Rep_Parm_Pnd_Filler2().setValue("/");
        pdaCwfa3001.getPnd_Rep_Parm_Pnd_Fillera().setValue("/");                                                                                                          //Natural: MOVE '/' TO #FILLERA #FILLERB
        pdaCwfa3001.getPnd_Rep_Parm_Pnd_Fillerb().setValue("/");
        //*  GETS SYSTEM RUN DATE ( NEW ONE )
        DbsUtil.callnat(Cwfn3912.class , getCurrentProcessState(), pnd_Comp_Date, pnd_Parm_Type);                                                                         //Natural: CALLNAT 'CWFN3912' #COMP-DATE #PARM-TYPE
        if (condition(Global.isEscape())) return;
        //*  RESETS THE Y/N FLAG BUT DOES NOT BUMP UP THE DATE
        //*  YET
        DbsUtil.callnat(Cwfn3913.class , getCurrentProcessState(), pnd_Report_Parm, pnd_Racf_Id, pnd_Parm_Unit, pnd_Floor, pnd_Bldg, pnd_Drop_Off, pdaCwfa3001.getPnd_Rep_Parm_Pnd_Start_Date(),  //Natural: CALLNAT 'CWFN3913' #REPORT-PARM #RACF-ID #PARM-UNIT #FLOOR #BLDG #DROP-OFF #START-DATE #TBL-RUN-FLAG
            pnd_Tbl_Run_Flag);
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Comp_Date.greater(pdaCwfa3001.getPnd_Rep_Parm_Pnd_Start_Date())))                                                                               //Natural: IF #COMP-DATE GT #START-DATE
        {
                                                                                                                                                                          //Natural: PERFORM GET-START-DATE
            sub_Get_Start_Date();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            DbsUtil.terminate();  if (true) return;                                                                                                                       //Natural: TERMINATE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Yyyymmdd.setValue(pdaCwfa3001.getPnd_Rep_Parm_Pnd_Start_Date());                                                                                              //Natural: MOVE #START-DATE TO #YYYYMMDD
        pdaCwfa3001.getPnd_Rep_Parm_Pnd_Work_Mm().setValue(pnd_Yyyymmdd_Pnd_Mm);                                                                                          //Natural: MOVE #MM TO #WORK-MM
        pdaCwfa3001.getPnd_Rep_Parm_Pnd_Work_Dd().setValue(pnd_Yyyymmdd_Pnd_Dd);                                                                                          //Natural: MOVE #DD TO #WORK-DD
        pdaCwfa3001.getPnd_Rep_Parm_Pnd_Work_Yy().setValue(pnd_Yyyymmdd_Pnd_Yy);                                                                                          //Natural: MOVE #YY TO #WORK-YY
        pdaCwfa3001.getPnd_Rep_Parm_Pnd_End_Date().setValue(pnd_Comp_Date);                                                                                               //Natural: MOVE #COMP-DATE TO #END-DATE #YYYYMMDD
        pnd_Yyyymmdd.setValue(pnd_Comp_Date);
        pdaCwfa3001.getPnd_Rep_Parm_Pnd_Work_Mmm().setValue(pnd_Yyyymmdd_Pnd_Mm);                                                                                         //Natural: MOVE #MM TO #WORK-MMM
        pdaCwfa3001.getPnd_Rep_Parm_Pnd_Work_Ddd().setValue(pnd_Yyyymmdd_Pnd_Dd);                                                                                         //Natural: MOVE #DD TO #WORK-DDD
        pdaCwfa3001.getPnd_Rep_Parm_Pnd_Work_Yyy().setValue(pnd_Yyyymmdd_Pnd_Yy);                                                                                         //Natural: MOVE #YY TO #WORK-YYY
        getReports().write(0, "START DATE IS :",pdaCwfa3001.getPnd_Rep_Parm_Pnd_Start_Date(),NEWLINE,"END DATE IS:   ",pdaCwfa3001.getPnd_Rep_Parm_Pnd_End_Date());       //Natural: WRITE 'START DATE IS :' #START-DATE / 'END DATE IS:   ' #END-DATE
        if (Global.isEscape()) return;
        vw_cwf_Master_Index.startDatabaseRead                                                                                                                             //Natural: READ CWF-MASTER-INDEX BY ACTV-UNQUE-KEY FROM #START-DATE
        (
        "READ_MASTER",
        new Wc[] { new Wc("ACTV_UNQUE_KEY", ">=", pdaCwfa3001.getPnd_Rep_Parm_Pnd_Start_Date(), WcType.BY) },
        new Oc[] { new Oc("ACTV_UNQUE_KEY", "ASC") }
        );
        READ_MASTER:
        while (condition(vw_cwf_Master_Index.readNextRow("READ_MASTER")))
        {
            if (condition(cwf_Master_Index_Rqst_Log_Index_Dte.greater(pdaCwfa3001.getPnd_Rep_Parm_Pnd_End_Date())))                                                       //Natural: IF CWF-MASTER-INDEX.RQST-LOG-INDEX-DTE GT #END-DATE
            {
                if (true) break READ_MASTER;                                                                                                                              //Natural: ESCAPE BOTTOM ( READ-MASTER. ) IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            vw_hst_Master_Index.startDatabaseRead                                                                                                                         //Natural: READ ( 1 ) HST-MASTER-INDEX BY RQST-ROUTING-KEY FROM CWF-MASTER-INDEX.RQST-LOG-DTE-TME
            (
            "GET_WPID",
            new Wc[] { new Wc("RQST_ROUTING_KEY", ">=", cwf_Master_Index_Rqst_Log_Dte_Tme, WcType.BY) },
            new Oc[] { new Oc("RQST_ROUTING_KEY", "ASC") },
            1
            );
            GET_WPID:
            while (condition(vw_hst_Master_Index.readNextRow("GET_WPID")))
            {
                pdaCwfa3001.getPnd_Local_Data_Pnd_Work_Prcss_Id().setValue(hst_Master_Index_Work_Prcss_Id);                                                               //Natural: MOVE HST-MASTER-INDEX.WORK-PRCSS-ID TO #WORK-PRCSS-ID
                pnd_Oprtr_Cde.setValue(hst_Master_Index_Rqst_Log_Oprtr_Cde);                                                                                              //Natural: MOVE HST-MASTER-INDEX.RQST-LOG-OPRTR-CDE TO #OPRTR-CDE
                pnd_Work_Unit.setValue(hst_Master_Index_Last_Chnge_Unit_Cde);                                                                                             //Natural: MOVE HST-MASTER-INDEX.LAST-CHNGE-UNIT-CDE TO #WORK-UNIT
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_MASTER"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_MASTER"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* EMPL+UNIT
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-FILE
            sub_Write_Work_File();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_MASTER"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_MASTER"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  READ-MASTER.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Reccount.equals(getZero())))                                                                                                                    //Natural: IF #RECCOUNT = 0
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            pnd_Bldg.setValue("W");                                                                                                                                       //Natural: MOVE 'W' TO #BLDG
            DbsUtil.callnat(Cwfn3910.class , getCurrentProcessState(), pnd_Report_No, pnd_Racf_Id, pdaCwfa3001.getPnd_Local_Data_Tbl_Unit(), pnd_Floor,                   //Natural: CALLNAT 'CWFN3910' #REPORT-NO #RACF-ID TBL-UNIT #FLOOR #BLDG #DROP-OFF #COMP-DATE
                pnd_Bldg, pnd_Drop_Off, pnd_Comp_Date);
            if (condition(Global.isEscape())) return;
            DbsUtil.callnat(Cwfn3915.class , getCurrentProcessState(), pnd_Report_No);                                                                                    //Natural: CALLNAT 'CWFN3915' #REPORT-NO
            if (condition(Global.isEscape())) return;
            DbsUtil.callnat(Cwfn3911.class , getCurrentProcessState());                                                                                                   //Natural: CALLNAT 'CWFN3911'
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_New_Unit.setValue(true);                                                                                                                                      //Natural: MOVE TRUE TO #NEW-UNIT #NEW-RACF
        pnd_New_Racf.setValue(true);
        setControl("WB");                                                                                                                                                 //Natural: SET CONTROL 'WB'
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #WORK-RECORD
        while (condition(getWorkFiles().read(1, pdaCwfa3001.getPnd_Local_Data_Pnd_Work_Record())))
        {
            getSort().writeSortInData(pdaCwfa3001.getPnd_Local_Data_Tbl_Unit(), pdaCwfa3001.getPnd_Local_Data_Tbl_Racf_Id(), pdaCwfa3001.getPnd_Local_Data_Tbl_Wpid_Act(),  //Natural: END-ALL
                pdaCwfa3001.getPnd_Local_Data_Tbl_Wpid(), pnd_Sort_Unit);
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getSort().sortData(pdaCwfa3001.getPnd_Local_Data_Tbl_Unit(), pdaCwfa3001.getPnd_Local_Data_Tbl_Racf_Id(), pdaCwfa3001.getPnd_Local_Data_Tbl_Wpid_Act(),           //Natural: SORT BY TBL-UNIT TBL-RACF-ID TBL-WPID-ACT TBL-WPID USING #SORT-UNIT
            pdaCwfa3001.getPnd_Local_Data_Tbl_Wpid());
        sort01Tbl_WpidCount399.setDec(new DbsDecimal(0));
        sort01Tbl_WpidCount.setDec(new DbsDecimal(0));
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(pdaCwfa3001.getPnd_Local_Data_Tbl_Unit(), pdaCwfa3001.getPnd_Local_Data_Tbl_Racf_Id(), pdaCwfa3001.getPnd_Local_Data_Tbl_Wpid_Act(), 
            pdaCwfa3001.getPnd_Local_Data_Tbl_Wpid(), pnd_Sort_Unit)))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            sort01Tbl_WpidCount399.setInt(sort01Tbl_WpidCount399.getInt() + 1);
            sort01Tbl_WpidCount.setInt(sort01Tbl_WpidCount.getInt() + 1);
            pdaCwfa3001.getPnd_Rep_Parm_Pnd_Rep_Unit_Cde().getValue(1).setValue(pdaCwfa3001.getPnd_Local_Data_Tbl_Unit());                                                //Natural: MOVE TBL-UNIT TO #REP-UNIT-CDE ( 1 ) #REP-EMPL-NAME
            pdaCwfa3001.getPnd_Local_Data_Pnd_Rep_Empl_Name().setValue(pdaCwfa3001.getPnd_Local_Data_Tbl_Unit());
            pdaCwfa3001.getPnd_Rep_Parm_Pnd_Rep_Racf_Id().setValue(pdaCwfa3001.getPnd_Local_Data_Tbl_Racf_Id());                                                          //Natural: MOVE TBL-RACF-ID TO #REP-RACF-ID
            DbsUtil.callnat(Cwfn1107.class , getCurrentProcessState(), pdaCwfa3001.getPnd_Rep_Parm_Pnd_Rep_Racf_Id(), pdaCwfa3001.getPnd_Local_Data_Pnd_Rep_Empl_Name()); //Natural: CALLNAT 'CWFN1107' #REP-RACF-ID #REP-EMPL-NAME
            if (condition(Global.isEscape())) return;
            if (condition(pnd_New_Unit.getBoolean()))                                                                                                                     //Natural: IF #NEW-UNIT
            {
                pnd_Bldg.setValue("W");                                                                                                                                   //Natural: MOVE 'W' TO #BLDG
                DbsUtil.callnat(Cwfn3910.class , getCurrentProcessState(), pnd_Report_No, pnd_Racf_Id, pdaCwfa3001.getPnd_Local_Data_Tbl_Unit(), pnd_Floor,               //Natural: CALLNAT 'CWFN3910' #REPORT-NO #RACF-ID TBL-UNIT #FLOOR #BLDG #DROP-OFF #COMP-DATE
                    pnd_Bldg, pnd_Drop_Off, pnd_Comp_Date);
                if (condition(Global.isEscape())) return;
                pnd_New_Unit.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #NEW-UNIT
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet361 = 0;                                                                                                                             //Natural: AT TOP OF PAGE ( 1 );//Natural: DECIDE ON FIRST VALUE OF TBL-WPID-ACT;//Natural: VALUE 'B'
            if (condition((pdaCwfa3001.getPnd_Local_Data_Tbl_Wpid_Act().equals("B"))))
            {
                decideConditionsMet361++;
                pdaCwfa3001.getPnd_Local_Data_Pnd_Booklet_Ctr().nadd(1);                                                                                                  //Natural: ADD 1 TO #BOOKLET-CTR
            }                                                                                                                                                             //Natural: VALUE 'F'
            else if (condition((pdaCwfa3001.getPnd_Local_Data_Tbl_Wpid_Act().equals("F"))))
            {
                decideConditionsMet361++;
                pdaCwfa3001.getPnd_Local_Data_Pnd_Forms_Ctr().nadd(1);                                                                                                    //Natural: ADD 1 TO #FORMS-CTR
            }                                                                                                                                                             //Natural: VALUE 'I'
            else if (condition((pdaCwfa3001.getPnd_Local_Data_Tbl_Wpid_Act().equals("I"))))
            {
                decideConditionsMet361++;
                pdaCwfa3001.getPnd_Local_Data_Pnd_Inquire_Ctr().nadd(1);                                                                                                  //Natural: ADD 1 TO #INQUIRE-CTR
            }                                                                                                                                                             //Natural: VALUE 'R'
            else if (condition((pdaCwfa3001.getPnd_Local_Data_Tbl_Wpid_Act().equals("R"))))
            {
                decideConditionsMet361++;
                pdaCwfa3001.getPnd_Local_Data_Pnd_Research_Ctr().nadd(1);                                                                                                 //Natural: ADD 1 TO #RESEARCH-CTR
            }                                                                                                                                                             //Natural: VALUE 'T'
            else if (condition((pdaCwfa3001.getPnd_Local_Data_Tbl_Wpid_Act().equals("T"))))
            {
                decideConditionsMet361++;
                pdaCwfa3001.getPnd_Local_Data_Pnd_Trans_Ctr().nadd(1);                                                                                                    //Natural: ADD 1 TO #TRANS-CTR
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaCwfa3001.getPnd_Local_Data_Tbl_Wpid_Act().equals("X"))))
            {
                decideConditionsMet361++;
                pdaCwfa3001.getPnd_Local_Data_Pnd_Complaint_Ctr().nadd(1);                                                                                                //Natural: ADD 1 TO #COMPLAINT-CTR
            }                                                                                                                                                             //Natural: VALUE 'Z'
            else if (condition((pdaCwfa3001.getPnd_Local_Data_Tbl_Wpid_Act().equals("Z"))))
            {
                decideConditionsMet361++;
                pdaCwfa3001.getPnd_Local_Data_Pnd_Other_Ctr().nadd(1);                                                                                                    //Natural: ADD 1 TO #OTHER-CTR
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            pdaCwfa3001.getPnd_Local_Data_Pnd_Unclear_Ctr().reset();                                                                                                      //Natural: RESET #UNCLEAR-CTR
            if (condition(pdaCwfa3001.getPnd_Local_Data_Tbl_Wpid_Action().equals("U")))                                                                                   //Natural: IF TBL-WPID-ACTION = 'U'
            {
                pdaCwfa3001.getPnd_Local_Data_Pnd_Unclear_Ctr().nadd(1);                                                                                                  //Natural: ADD 1 TO #UNCLEAR-CTR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaCwfa3001.getPnd_Local_Data_Tbl_Wpid_Lob().equals("U")))                                                                                      //Natural: IF TBL-WPID-LOB = 'U'
            {
                pdaCwfa3001.getPnd_Local_Data_Pnd_Unclear_Ctr().nadd(1);                                                                                                  //Natural: ADD 1 TO #UNCLEAR-CTR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaCwfa3001.getPnd_Local_Data_Tbl_Wpid_Mbp().equals("U")))                                                                                      //Natural: IF TBL-WPID-MBP = 'U'
            {
                pdaCwfa3001.getPnd_Local_Data_Pnd_Unclear_Ctr().nadd(1);                                                                                                  //Natural: ADD 1 TO #UNCLEAR-CTR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaCwfa3001.getPnd_Local_Data_Tbl_Wpid_Sbp().equals("U")))                                                                                      //Natural: IF TBL-WPID-SBP = 'U'
            {
                pdaCwfa3001.getPnd_Local_Data_Pnd_Unclear_Ctr().nadd(1);                                                                                                  //Natural: ADD 1 TO #UNCLEAR-CTR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaCwfa3001.getPnd_Local_Data_Pnd_Unclear_Ctr().greater(getZero())))                                                                            //Natural: IF #UNCLEAR-CTR GT 0
            {
                pdaCwfa3001.getPnd_Local_Data_Pnd_Unclear_Ctr().reset();                                                                                                  //Natural: RESET #UNCLEAR-CTR
                pdaCwfa3001.getPnd_Local_Data_Pnd_Total_Unclear().nadd(1);                                                                                                //Natural: COMPUTE #TOTAL-UNCLEAR = #TOTAL-UNCLEAR + 1
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.callnat(Cwfn1105.class , getCurrentProcessState(), pdaCwfa3001.getPnd_Local_Data_Tbl_Wpid(), pdaCwfa3001.getPnd_Local_Data_Pnd_Wpid_Desc());          //Natural: CALLNAT 'CWFN1105' TBL-WPID #WPID-DESC
            if (condition(Global.isEscape())) return;
            //*                                                                                                                                                           //Natural: AT BREAK OF TBL-WPID
            //*                                                                                                                                                           //Natural: AT BREAK OF TBL-RACF-ID;//Natural: AT BREAK OF TBL-UNIT
            //*  READ-2.
            sort01Tbl_WpidOld.setValue(pdaCwfa3001.getPnd_Local_Data_Tbl_Wpid());                                                                                         //Natural: END-SORT
        }
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            sort01Tbl_WpidCount399.resetBreak();
            sort01Tbl_WpidCount.resetBreak();
            atBreakEventSort01(endOfDataSort01);
        }
        endSort();
        pnd_Tbl_Run_Flag.setValue("Y");                                                                                                                                   //Natural: ON ERROR;//Natural: MOVE 'Y' TO #TBL-RUN-FLAG
        DbsUtil.callnat(Cwfn3914.class , getCurrentProcessState(), pnd_Report_Parm, pnd_Comp_Date, pnd_Tbl_Run_Flag);                                                     //Natural: CALLNAT 'CWFN3914' #REPORT-PARM #COMP-DATE #TBL-RUN-FLAG
        if (condition(Global.isEscape())) return;
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WORK-FILE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-START-DATE
    }
    private void sub_Write_Work_File() throws Exception                                                                                                                   //Natural: WRITE-WORK-FILE
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(DbsUtil.maskMatches(pdaCwfa3001.getPnd_Local_Data_Pnd_Work_Prcss_Id(),"N.....")))                                                                   //Natural: IF #WORK-PRCSS-ID = MASK ( N..... )
        {
            pdaCwfa3001.getPnd_Local_Data_Tbl_Wpid_Act().setValue("Z");                                                                                                   //Natural: MOVE 'Z' TO TBL-WPID-ACT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaCwfa3001.getPnd_Local_Data_Tbl_Wpid_Act().setValue(pdaCwfa3001.getPnd_Local_Data_Pnd_Work_Prcss_Id());                                                     //Natural: MOVE #WORK-PRCSS-ID TO TBL-WPID-ACT
        }                                                                                                                                                                 //Natural: END-IF
        pdaCwfa3001.getPnd_Local_Data_Tbl_Wpid().setValue(pdaCwfa3001.getPnd_Local_Data_Pnd_Work_Prcss_Id());                                                             //Natural: MOVE #WORK-PRCSS-ID TO TBL-WPID
        pdaCwfa3001.getPnd_Local_Data_Tbl_Unit().setValue(pnd_Work_Unit);                                                                                                 //Natural: MOVE #WORK-UNIT TO TBL-UNIT
        pdaCwfa3001.getPnd_Local_Data_Tbl_Racf_Id().setValue(pnd_Oprtr_Cde);                                                                                              //Natural: MOVE #OPRTR-CDE TO TBL-RACF-ID
        getWorkFiles().write(1, false, pdaCwfa3001.getPnd_Local_Data_Pnd_Work_Record());                                                                                  //Natural: WRITE WORK FILE 1 #WORK-RECORD
        pnd_Reccount.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #RECCOUNT
    }
    private void sub_Get_Start_Date() throws Exception                                                                                                                    //Natural: GET-START-DATE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Work_Comp_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Comp_Date);                                                                                  //Natural: MOVE EDITED #COMP-DATE TO #WORK-COMP-DATE ( EM = YYYYMMDD )
        pnd_Work_Start_Date.setValue(pnd_Work_Comp_Date);                                                                                                                 //Natural: MOVE #WORK-COMP-DATE TO #WORK-START-DATE
        pnd_Work_Start_Date.nsubtract(6);                                                                                                                                 //Natural: SUBTRACT 6 FROM #WORK-START-DATE
        pdaCwfa3001.getPnd_Rep_Parm_Pnd_Start_Date().setValueEdited(pnd_Work_Start_Date,new ReportEditMask("YYYYMMDD"));                                                  //Natural: MOVE EDITED #WORK-START-DATE ( EM = YYYYMMDD ) TO #START-DATE
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Page.nadd(1);                                                                                                                                     //Natural: COMPUTE #PAGE = #PAGE + 1
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),pnd_Env,new TabSetting(26),"CORPORATE WORKFLOW FACILITIES",new TabSetting(70),"PAGE",pnd_Page,  //Natural: WRITE ( 1 ) NOTITLE *PROGRAM #ENV 26T 'CORPORATE WORKFLOW FACILITIES' 70T 'PAGE' #PAGE ( NL = 5 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 25T 'WORK REQUESTS LOGGED AND INDEXED' 72T *TIMX ( EM = HH':'II' 'AP )
                        new NumericLength (5), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),new 
                        TabSetting(25),"WORK REQUESTS LOGGED AND INDEXED",new TabSetting(72),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"));
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                    if (condition(pnd_New_Racf.getBoolean()))                                                                                                             //Natural: IF #NEW-RACF
                    {
                        DbsUtil.callnat(Cwfn1103.class , getCurrentProcessState(), pdaCwfa3001.getPnd_Rep_Parm_Pnd_Rep_Unit_Cde().getValue(1), pnd_Unit_Name);            //Natural: CALLNAT 'CWFN1103' #REP-UNIT-CDE ( 1 ) #UNIT-NAME
                        if (condition(Global.isEscape())) return;
                        getReports().write(1, ReportOption.NOTITLE,"UNIT      :",pdaCwfa3001.getPnd_Rep_Parm_Pnd_Rep_Unit_Cde().getValue(1),pnd_Unit_Name);               //Natural: WRITE ( 1 ) 'UNIT      :' #REP-UNIT-CDE ( 1 ) #UNIT-NAME
                        getReports().write(1, ReportOption.NOTITLE,"START-DATE:",pdaCwfa3001.getPnd_Rep_Parm_Pnd_Work_Start_Date_A());                                    //Natural: WRITE ( 1 ) 'START-DATE:' #WORK-START-DATE-A
                        getReports().write(1, ReportOption.NOTITLE,"END-DATE  :",pdaCwfa3001.getPnd_Rep_Parm_Pnd_Work_End_Date_A());                                      //Natural: WRITE ( 1 ) 'END-DATE  :' #WORK-END-DATE-A
                        if (condition(pdaCwfa3001.getPnd_Local_Data_Pnd_Rep_Empl_Name().equals("NOT FOUND")))                                                             //Natural: IF #REP-EMPL-NAME = 'NOT FOUND'
                        {
                            pdaCwfa3001.getPnd_Local_Data_Pnd_Rep_Empl_Name().setValue(pdaCwfa3001.getPnd_Rep_Parm_Pnd_Rep_Racf_Id());                                    //Natural: MOVE #REP-RACF-ID TO #REP-EMPL-NAME
                        }                                                                                                                                                 //Natural: END-IF
                        getReports().write(1, ReportOption.NOTITLE,"EMPLOYEE  :",pdaCwfa3001.getPnd_Local_Data_Pnd_Rep_Empl_Name());                                      //Natural: WRITE ( 1 ) 'EMPLOYEE  :' #REP-EMPL-NAME
                        pnd_New_Racf.setValue(false);                                                                                                                     //Natural: MOVE FALSE TO #NEW-RACF
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE,"=",new RepeatItem(79));                                                                                   //Natural: WRITE ( 1 ) '=' ( 79 )
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                    if (condition(! (pdaCwfa3001.getPnd_Local_Data_Pnd_End_Of_Data().getBoolean())))                                                                      //Natural: IF NOT #END-OF-DATA
                    {
                        getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(62),"NUMBER OF",NEWLINE,new ColumnSpacing(10),"WPID",new ColumnSpacing(19),"DESCRIPTION",new  //Natural: WRITE ( 1 ) 62X 'NUMBER OF' / 10X 'WPID' 19X 'DESCRIPTION' 18X 'REQUESTS' / 9X '-' ( 6 ) '-' ( 45 ) '-' ( 9 )
                            ColumnSpacing(18),"REQUESTS",NEWLINE,new ColumnSpacing(9),"-",new RepeatItem(6),"-",new RepeatItem(45),"-",new RepeatItem(9));
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        pnd_Tbl_Run_Flag.setValue(" ");                                                                                                                                   //Natural: MOVE ' ' TO #TBL-RUN-FLAG
        DbsUtil.callnat(Cwfn3914.class , getCurrentProcessState(), pnd_Report_Parm, pnd_Comp_Date, pnd_Tbl_Run_Flag);                                                     //Natural: CALLNAT 'CWFN3914' #REPORT-PARM #COMP-DATE #TBL-RUN-FLAG
        if (condition(Global.isEscape())) return;
    };                                                                                                                                                                    //Natural: END-ERROR

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean pdaCwfa3001_getPnd_Local_Data_Tbl_WpidIsBreak = pdaCwfa3001.getPnd_Local_Data_Tbl_Wpid().isBreak(endOfData);
        boolean pdaCwfa3001_getPnd_Local_Data_Tbl_Racf_IdIsBreak = pdaCwfa3001.getPnd_Local_Data_Tbl_Racf_Id().isBreak(endOfData);
        boolean pdaCwfa3001_getPnd_Local_Data_Tbl_UnitIsBreak = pdaCwfa3001.getPnd_Local_Data_Tbl_Unit().isBreak(endOfData);
        if (condition(pdaCwfa3001_getPnd_Local_Data_Tbl_WpidIsBreak || pdaCwfa3001_getPnd_Local_Data_Tbl_Racf_IdIsBreak || pdaCwfa3001_getPnd_Local_Data_Tbl_UnitIsBreak))
        {
            pdaCwfa3001.getPnd_Local_Data_Pnd_Wpid_Code().setValue(sort01Tbl_WpidOld);                                                                                    //Natural: MOVE OLD ( TBL-WPID ) TO #WPID-CODE
            pdaCwfa3001.getPnd_Local_Data_Pnd_Wpid_Count().setValue(sort01Tbl_WpidCount399);                                                                              //Natural: MOVE COUNT ( TBL-WPID ) TO #WPID-COUNT
            pdaCwfa3001.getPnd_Local_Data_Pnd_Total_Logged().nadd(pdaCwfa3001.getPnd_Local_Data_Pnd_Wpid_Count());                                                        //Natural: COMPUTE #TOTAL-LOGGED = #TOTAL-LOGGED + #WPID-COUNT
            getReports().write(1, ReportOption.NOTITLE,pdaCwfa3001.getPnd_Local_Data_Pnd_Pad_Space(),pdaCwfa3001.getPnd_Local_Data_Pnd_Wpid_Code(),pdaCwfa3001.getPnd_Local_Data_Pnd_Wpid_Desc(),pdaCwfa3001.getPnd_Local_Data_Pnd_Wpid_Count(),  //Natural: WRITE ( 1 ) #PAD-SPACE #WPID-CODE #WPID-DESC #WPID-COUNT ( EM = ZZ,ZZ9 )
                new ReportEditMask ("ZZ,ZZ9"));
            if (condition(Global.isEscape())) return;
            sort01Tbl_WpidCount399.setDec(new DbsDecimal(0));                                                                                                             //Natural: END-BREAK
        }
        if (condition(pdaCwfa3001_getPnd_Local_Data_Tbl_Racf_IdIsBreak || pdaCwfa3001_getPnd_Local_Data_Tbl_UnitIsBreak))
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(35),"TOTAL ",new TabSetting(63),pdaCwfa3001.getPnd_Local_Data_Pnd_Total_Logged(),NEWLINE,NEWLINE,"NUMBER OF WORK REQUESTS LOGGED AND INDEXED WITH 'U' IN ANY ELEMENT:",pdaCwfa3001.getPnd_Local_Data_Pnd_Total_Unclear(),  //Natural: WRITE ( 1 ) NOTITLE / 35T 'TOTAL ' 63T #TOTAL-LOGGED / / 'NUMBER OF WORK REQUESTS LOGGED AND INDEXED WITH "U" IN ANY ELEMENT:' #TOTAL-UNCLEAR ( EM = ZZ,ZZ9 )
                new ReportEditMask ("ZZ,ZZ9"));
            if (condition(Global.isEscape())) return;
            pdaCwfa3001.getPnd_Rep_Parm_Pnd_Rep_Racf_Id().setValue(pdaCwfa3001.getPnd_Local_Data_Tbl_Racf_Id());                                                          //Natural: MOVE TBL-RACF-ID TO #REP-RACF-ID
            if (condition(getReports().getAstLineCount(1).greater(53)))                                                                                                   //Natural: IF *LINE-COUNT ( 1 ) GT 53
            {
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,NEWLINE,"BOOKLETS  FORMS",NEWLINE,"REQUESTED REQUESTED INQUIRIES RESEARCH TRANSACTIONS COMPLAINTS  OTHERS   ",NEWLINE,"--------- --------- --------- -------- ------------ ----------  ---------",NEWLINE,new  //Natural: WRITE ( 1 ) NOTITLE / / '*' ( 79 ) // 'BOOKLETS  FORMS' / 'REQUESTED REQUESTED INQUIRIES RESEARCH TRANSACTIONS COMPLAINTS  OTHERS   ' / '--------- --------- --------- -------- ------------ ----------  ---------' / 4T #BOOKLET-CTR 14T #FORMS-CTR 24T #INQUIRE-CTR 33T #RESEARCH-CTR 46T #TRANS-CTR 57T #COMPLAINT-CTR 68T #OTHER-CTR
                TabSetting(4),pdaCwfa3001.getPnd_Local_Data_Pnd_Booklet_Ctr(),new TabSetting(14),pdaCwfa3001.getPnd_Local_Data_Pnd_Forms_Ctr(),new TabSetting(24),pdaCwfa3001.getPnd_Local_Data_Pnd_Inquire_Ctr(),new 
                TabSetting(33),pdaCwfa3001.getPnd_Local_Data_Pnd_Research_Ctr(),new TabSetting(46),pdaCwfa3001.getPnd_Local_Data_Pnd_Trans_Ctr(),new TabSetting(57),pdaCwfa3001.getPnd_Local_Data_Pnd_Complaint_Ctr(),new 
                TabSetting(68),pdaCwfa3001.getPnd_Local_Data_Pnd_Other_Ctr());
            if (condition(Global.isEscape())) return;
            pdaCwfa3001.getPnd_Local_Data_Pnd_Booklet_Ctr().reset();                                                                                                      //Natural: RESET #BOOKLET-CTR #FORMS-CTR #INQUIRE-CTR #RESEARCH-CTR #TRANS-CTR #COMPLAINT-CTR #OTHER-CTR #TOTAL-LOGGED #TOTAL-UNCLEAR
            pdaCwfa3001.getPnd_Local_Data_Pnd_Forms_Ctr().reset();
            pdaCwfa3001.getPnd_Local_Data_Pnd_Inquire_Ctr().reset();
            pdaCwfa3001.getPnd_Local_Data_Pnd_Research_Ctr().reset();
            pdaCwfa3001.getPnd_Local_Data_Pnd_Trans_Ctr().reset();
            pdaCwfa3001.getPnd_Local_Data_Pnd_Complaint_Ctr().reset();
            pdaCwfa3001.getPnd_Local_Data_Pnd_Other_Ctr().reset();
            pdaCwfa3001.getPnd_Local_Data_Pnd_Total_Logged().reset();
            pdaCwfa3001.getPnd_Local_Data_Pnd_Total_Unclear().reset();
            pnd_New_Racf.setValue(true);                                                                                                                                  //Natural: MOVE TRUE TO #NEW-RACF
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pdaCwfa3001_getPnd_Local_Data_Tbl_UnitIsBreak))
        {
            pdaCwfa3001.getPnd_Local_Data_Pnd_Booklet_Ctr().reset();                                                                                                      //Natural: RESET #BOOKLET-CTR #FORMS-CTR #INQUIRE-CTR #RESEARCH-CTR #TRANS-CTR #COMPLAINT-CTR #TOTAL-LOGGED #TOTAL-UNCLEAR #PAGE
            pdaCwfa3001.getPnd_Local_Data_Pnd_Forms_Ctr().reset();
            pdaCwfa3001.getPnd_Local_Data_Pnd_Inquire_Ctr().reset();
            pdaCwfa3001.getPnd_Local_Data_Pnd_Research_Ctr().reset();
            pdaCwfa3001.getPnd_Local_Data_Pnd_Trans_Ctr().reset();
            pdaCwfa3001.getPnd_Local_Data_Pnd_Complaint_Ctr().reset();
            pdaCwfa3001.getPnd_Local_Data_Pnd_Total_Logged().reset();
            pdaCwfa3001.getPnd_Local_Data_Pnd_Total_Unclear().reset();
            pnd_Page.reset();
            DbsUtil.callnat(Cwfn3911.class , getCurrentProcessState());                                                                                                   //Natural: CALLNAT 'CWFN3911'
            if (condition(Global.isEscape())) return;
            pnd_New_Unit.setValue(true);                                                                                                                                  //Natural: MOVE TRUE TO #NEW-UNIT #NEW-RACF
            pnd_New_Racf.setValue(true);
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=80 PS=58");
    }
}
