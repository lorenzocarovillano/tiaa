/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:23:57 PM
**        * FROM NATURAL PROGRAM : Swfb4420
************************************************************
**        * FILE NAME            : Swfb4420.java
**        * CLASS NAME           : Swfb4420
**        * INSTANCE NAME        : Swfb4420
************************************************************
************************************************************************
* PROGRAM  : SWFB4420
* SYSTEM   : SURVIVOR BENEFITS
* TITLE    : 90 DAY FOLLOW-UP AND MINIMUM DISTRIBUTION LETTERS
* CREATED  : FEB 06,98
* AUTHOR   : CHRIS SARMIENTO
* FUNCTION : GENERATES 90 DAY FORMS FOLLOW-UP LETTERS AND/OR
*            MINIMUM DISTRIBUTION LETTERS FOR SURVIVORS REQUESTS WITH
*            'Awaiting return of forms' STATUS.
* HISTORY
*
* 12/11/98 SARMIEC  ADDED LOGIC FOR THE CORRESPONDENCE FOLLOW-UP LETTERS
* 03/23/00 ARI G.   ADDED IA FORMS, IA AND DA PAYMENTS, AND SPECIALIZED
*                   LETTERS LOGIC.
* 09/06/00 ARI G.   ADDED IA FOLLOWUPS PROCESSING DURING MD QUARTER
* 08/16/16 VR       COR/NAAD SUNSET
* 02/23/2017 - DASRAHU - PIN EXPANSION - AUG 2017
************************************************************************

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Swfb4420 extends BLNatBase
{
    // Data Areas
    private PdaCwfpdaus pdaCwfpdaus;
    private PdaCwfpdaun pdaCwfpdaun;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private PdaAnta002 pdaAnta002;
    private PdaMdma101 pdaMdma101;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Sb_Ph;
    private DbsField cwf_Sb_Ph_Rcrd_Type;
    private DbsField cwf_Sb_Ph_Srvvr_Mit_Idntfr;

    private DbsGroup cwf_Sb_Ph__R_Field_1;
    private DbsField cwf_Sb_Ph_Ph_Pin_Nbr;
    private DbsField cwf_Sb_Ph_Ph_Dod_Dte;

    private DbsGroup cwf_Sb_Ph__R_Field_2;
    private DbsField cwf_Sb_Ph_Dod_Yyyy;
    private DbsField cwf_Sb_Ph_Sb_Converted;
    private DbsField cwf_Sb_Ph_Work_List_Ind;
    private DbsField pnd_Ph_Key;

    private DbsGroup pnd_Ph_Key__R_Field_3;
    private DbsField pnd_Ph_Key_Rcrd_Type;
    private DbsField pnd_Ph_Key_Srvvr_Mit_Idntfr;

    private DataAccessProgramView vw_master_1;
    private DbsField master_1_Pin_Nbr;
    private DbsField master_1_Crprte_Status_Ind;
    private DbsField master_1_Work_Prcss_Id;
    private DbsField master_1_Unit_Cde;
    private DbsField master_1_Status_Cde;
    private DbsField master_1_Rqst_Log_Dte_Tme;
    private DbsField master_1_Status_Updte_Dte_Tme;
    private DbsField pnd_Pin_History_Key;

    private DbsGroup pnd_Pin_History_Key__R_Field_4;
    private DbsField pnd_Pin_History_Key_Pin_Nbr;
    private DbsField pnd_Pin_History_Key_Crprte_Status_Ind;

    private DataAccessProgramView vw_cwf_Sb_Work_Req;
    private DbsField cwf_Sb_Work_Req_Srvvr_Mit_Idntfr;

    private DbsGroup cwf_Sb_Work_Req__R_Field_5;
    private DbsField cwf_Sb_Work_Req_Ph_Pin_Nbr;
    private DbsField cwf_Sb_Work_Req_Srvvr_Mit_Sqnce;
    private DbsField cwf_Sb_Work_Req_Rqst_Log_Dte_Tme;
    private DbsField cwf_Sb_Work_Req_Forms_Mail_Dte;

    private DbsGroup cwf_Sb_Work_Req__R_Field_6;
    private DbsField cwf_Sb_Work_Req_Forms_Mail_Dte_A8;
    private DbsField cwf_Sb_Work_Req_Thirty_Day_Crte_Dte;
    private DbsField cwf_Sb_Work_Req_Ninety_Day_Crte_Dte;
    private DbsField cwf_Sb_Work_Req_Ninety_Day_Crte_Nbr;
    private DbsField cwf_Sb_Work_Req_Md_Mds_Crte_Dte;

    private DbsGroup cwf_Sb_Work_Req__R_Field_7;
    private DbsField cwf_Sb_Work_Req_Md_Mds_Yyyy;
    private DbsField cwf_Sb_Work_Req_Md_Mds_Crte_Nbr;
    private DbsField cwf_Sb_Work_Req_Md_Md1_Crte_Dte;

    private DbsGroup cwf_Sb_Work_Req__R_Field_8;
    private DbsField cwf_Sb_Work_Req_Md_Md1_Yyyy;
    private DbsField cwf_Sb_Work_Req_Md_Md1_Crte_Nbr;
    private DbsField cwf_Sb_Work_Req_Md_Md2_Crte_Dte;

    private DbsGroup cwf_Sb_Work_Req__R_Field_9;
    private DbsField cwf_Sb_Work_Req_Md_Md2_Yyyy;
    private DbsField cwf_Sb_Work_Req_Md_Md2_Crte_Nbr;
    private DbsField cwf_Sb_Work_Req_Corresp_Crte_Dte;
    private DbsField cwf_Sb_Work_Req_Corresp_30d_Crte_Dte;
    private DbsField cwf_Sb_Work_Req_Corresp_90d_Crte_Dte;
    private DbsField cwf_Sb_Work_Req_Corresp_90d_Crte_Nbr;

    private DataAccessProgramView vw_cwf_Sb_Bene;
    private DbsField cwf_Sb_Bene_Rcrd_Type;
    private DbsField cwf_Sb_Bene_Srvvr_Mit_Idntfr;

    private DbsGroup cwf_Sb_Bene__R_Field_10;
    private DbsField cwf_Sb_Bene_Ph_Pin_Nbr;
    private DbsField cwf_Sb_Bene_Srvvr_Mit_Sqnce;
    private DbsField cwf_Sb_Bene_Srvvr_Nme_Fr_Frm;
    private DbsField cwf_Sb_Bene_Srvvr_Nme_Fr_Fr2;
    private DbsField cwf_Sb_Bene_Srvvr_Type;

    private DbsGroup cwf_Sb_Bene_Srvvr_Addr;
    private DbsField cwf_Sb_Bene_Srvvr_Addr_Line1;
    private DbsField cwf_Sb_Bene_Srvvr_Addr_Line2;
    private DbsField cwf_Sb_Bene_Srvvr_Addr_Line3;
    private DbsField cwf_Sb_Bene_Srvvr_Addr_Line4;
    private DbsField cwf_Sb_Bene_Srvvr_Addr_Line5;
    private DbsField cwf_Sb_Bene_Srvvr_Addr_Line6;
    private DbsField cwf_Sb_Bene_Srvvr_Zip_Cde;

    private DbsGroup cwf_Sb_Bene__R_Field_11;
    private DbsField cwf_Sb_Bene_Zip_Cde_1_5;
    private DbsField cwf_Sb_Bene_Zip_Cde_6_9;
    private DbsField cwf_Sb_Bene_State;

    private DbsGroup cwf_Sb_Bene__R_Field_12;
    private DbsField cwf_Sb_Bene_Addr_Arr;

    private DbsGroup cwf_Sb_Bene__R_Field_13;
    private DbsField cwf_Sb_Bene_Addr_All;
    private DbsField cwf_Sb_Bene_Us_Address;
    private DbsField cwf_Sb_Bene_Srvvr_Spse_Ind;
    private DbsField cwf_Sb_Bene_Srvvr_Rltnshp;
    private DbsField cwf_Sb_Bene_Last_Updte_Dte_Tme;
    private DbsField pnd_Srvvr_Key;

    private DbsGroup pnd_Srvvr_Key__R_Field_14;
    private DbsField pnd_Srvvr_Key_Rcrd_Type;
    private DbsField pnd_Srvvr_Key_Srvvr_Mit_Idntfr;

    private DataAccessProgramView vw_cwf_Support_Tbl;
    private DbsField cwf_Support_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Support_Tbl_Tbl_Key_Field;
    private DbsField cwf_Support_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Support_Tbl__R_Field_15;
    private DbsField cwf_Support_Tbl_Tbl_Reps;

    private DbsGroup cwf_Support_Tbl__R_Field_16;

    private DbsGroup cwf_Support_Tbl_Tbl_Reps_Redef;
    private DbsField cwf_Support_Tbl_Rep_Actv_Ind;
    private DbsField cwf_Support_Tbl_Rep_Id;
    private DbsField cwf_Support_Tbl__Filler1;

    private DbsGroup cwf_Support_Tbl__R_Field_17;
    private DbsField cwf_Support_Tbl_Stats;

    private DbsGroup cwf_Support_Tbl__R_Field_18;

    private DbsGroup cwf_Support_Tbl_Stats_Redef;
    private DbsField cwf_Support_Tbl_Stat_Cde;
    private DbsField cwf_Support_Tbl__Filler2;
    private DbsField cwf_Support_Tbl_Stage;
    private DbsField cwf_Support_Tbl__Filler3;
    private DbsField cwf_Support_Tbl_Advoc;
    private DbsField cwf_Support_Tbl__Filler4;
    private DbsField pnd_Tbl_Key;

    private DbsGroup pnd_Tbl_Key__R_Field_19;
    private DbsField pnd_Tbl_Key_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Key_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Key_Tbl_Key_Field;
    private DbsField pnd_Md_Control_Rec;
    private DbsField pnd_90d_Control_Rec;
    private DbsField pnd_Control_Rec;

    private DbsGroup pnd_Control_Rec__R_Field_20;
    private DbsField pnd_Control_Rec_Pnd_Ia_Da;
    private DbsField pnd_Control_Rec__Filler5;
    private DbsField pnd_Control_Rec_Pnd_Max_Pins;
    private DbsField pnd_Control_Rec__Filler6;
    private DbsField pnd_Control_Rec_Pnd_Last_Pin;
    private DbsField pnd_Control_Rec__Filler7;
    private DbsField pnd_Control_Rec_Pnd_Last_Run_Date;
    private DbsField pnd_Control_Rec__Filler8;
    private DbsField pnd_Control_Rec_Pnd_Printer_Id;
    private DbsField pnd_Control_Rec__Filler9;
    private DbsField pnd_Control_Rec_Pnd_Printed_Pin_Cnt;
    private DbsField pnd_Control_Rec__Filler10;
    private DbsField pnd_Control_Rec_Pnd_Printed_Letter_Cnt;
    private DbsField pnd_Control_Rec__Filler11;
    private DbsField pnd_Control_Rec_Pnd_Md_Start_Mmdd;
    private DbsField pnd_Control_Rec__Filler12;
    private DbsField pnd_Control_Rec_Pnd_Md_End_Mmdd;
    private DbsField pnd_Todays_Date_D;
    private DbsField pnd_Todays_Date_A8;

    private DbsGroup pnd_Todays_Date_A8__R_Field_21;
    private DbsField pnd_Todays_Date_A8_Pnd_Todays_Date_N8;

    private DbsGroup pnd_Todays_Date_A8__R_Field_22;
    private DbsField pnd_Todays_Date_A8_Pnd_Todays_Yyyy;

    private DbsGroup pnd_Todays_Date_A8__R_Field_23;
    private DbsField pnd_Todays_Date_A8_Pnd_Todays_Yyyy_N;
    private DbsField pnd_Last_Years_Yyyy;
    private DbsField pnd_Last_Date_A8;

    private DbsGroup pnd_Last_Date_A8__R_Field_24;
    private DbsField pnd_Last_Date_A8_Pnd_Last_Date_N8;
    private DbsField pnd_Last_Date_D;
    private DbsField pnd_Md_Start_A8;
    private DbsField pnd_Md_End_A8;
    private DbsField pnd_Md;
    private DbsField pnd_90d;
    private DbsField pnd_Report_Title;
    private DbsField pnd_Letter_Type;
    private DbsField pnd_Letter_Type_Desc;
    private DbsField pnd_Addr_Typ_Cde;
    private DbsField pnd_Md_Dte_C_Or_F;
    private DbsField pnd_Md_Dte;

    private DbsGroup pnd_Md_Dte__R_Field_25;
    private DbsField pnd_Md_Dte_Pnd_Md_Dte_Yyyy;
    private DbsField pnd_Err_Msg;
    private DbsField pnd_Letter_Cnt;
    private DbsField pnd_Starting_Pin_Cnt;
    private DbsField pnd_Total_Pins;
    private DbsField pnd_Total_Letters;
    private DbsField pnd_Total_Errors;
    private DbsField pnd_Total_90d_Corresp;
    private DbsField pnd_Total_90d;
    private DbsField pnd_Total_Mds;
    private DbsField pnd_Total_Md1;
    private DbsField pnd_Total_Md2;
    private DbsField pnd_Et_Cnt;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Lob;
    private DbsField pnd_Int_Rate_N;
    private DbsField pnd_Int_Rate_A;
    private DbsField pnd_Year;
    private DbsField pnd_Ph_Name;
    private DbsField pnd_Ph_Last_Name;
    private DbsField pnd_Ph_First_Name;
    private DbsField pnd_Ph_Middle_Name;
    private DbsField pnd_Ph_Isn;
    private DbsField pnd_Ph_Ssn;
    private DbsField pnd_Ph_Dob;
    private DbsField pnd_Rep_Name;
    private DbsField pnd_Rep_Ix;
    private DbsField pnd_Rep_Cnt;
    private DbsField pnd_Rep_Key;

    private DbsGroup pnd_Rep_Key__R_Field_26;

    private DbsGroup pnd_Rep_Key_Pnd_Rep_Key_Redef;
    private DbsField pnd_Rep_Key_Pnd_Rep_Unit;
    private DbsField pnd_Rep_Key__Filler13;
    private DbsField pnd_Rep_Arr;

    private DbsGroup pnd_Rep_Arr__R_Field_27;

    private DbsGroup pnd_Rep_Arr_Pnd_Rep_Arr_Redef;
    private DbsField pnd_Rep_Arr_Pnd_Rep_Act_Ind;
    private DbsField pnd_Rep_Arr_Pnd_Rep_Id;
    private DbsField pnd_Rep_Arr_Pnd_Rep_Lob;
    private DbsField pnd_Rep_Arr_Pnd_Rep_Case_Cnt;
    private DbsField pnd_Rep_Arr_Pnd_Rep_Letter_Cnt;
    private DbsField pnd_Rep_Arr__Filler14;
    private DbsField pnd_Str_Arr;

    private DbsGroup pnd_Status_Arr;
    private DbsField pnd_Status_Arr_Pnd_Status_Cnt;
    private DbsField pnd_Status_Arr_Pnd_Status;
    private DbsField pnd_Status_Arr_Pnd_Stage;
    private DbsField pnd_Status_Arr_Pnd_Advoc;
    private DbsField pnd_Status_Arr_Pnd_Status_Ix;
    private DbsField pnd_Payments_Status;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCwfpdaus = new PdaCwfpdaus(localVariables);
        pdaCwfpdaun = new PdaCwfpdaun(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        pdaAnta002 = new PdaAnta002(localVariables);
        pdaMdma101 = new PdaMdma101(localVariables);

        // Local Variables

        vw_cwf_Sb_Ph = new DataAccessProgramView(new NameInfo("vw_cwf_Sb_Ph", "CWF-SB-PH"), "CWF_SB_PH", "CWF_SB_BENE");
        cwf_Sb_Ph_Rcrd_Type = vw_cwf_Sb_Ph.getRecord().newFieldInGroup("cwf_Sb_Ph_Rcrd_Type", "RCRD-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RCRD_TYPE");
        cwf_Sb_Ph_Rcrd_Type.setDdmHeader("RECORD/TYPE");
        cwf_Sb_Ph_Srvvr_Mit_Idntfr = vw_cwf_Sb_Ph.getRecord().newFieldInGroup("cwf_Sb_Ph_Srvvr_Mit_Idntfr", "SRVVR-MIT-IDNTFR", FieldType.STRING, 14, 
            RepeatingFieldStrategy.None, "SRVVR_MIT_IDNTFR");
        cwf_Sb_Ph_Srvvr_Mit_Idntfr.setDdmHeader("SURVIVOR MIT/IDENTIFIER");

        cwf_Sb_Ph__R_Field_1 = vw_cwf_Sb_Ph.getRecord().newGroupInGroup("cwf_Sb_Ph__R_Field_1", "REDEFINE", cwf_Sb_Ph_Srvvr_Mit_Idntfr);
        cwf_Sb_Ph_Ph_Pin_Nbr = cwf_Sb_Ph__R_Field_1.newFieldInGroup("cwf_Sb_Ph_Ph_Pin_Nbr", "PH-PIN-NBR", FieldType.NUMERIC, 12);
        cwf_Sb_Ph_Ph_Dod_Dte = vw_cwf_Sb_Ph.getRecord().newFieldInGroup("cwf_Sb_Ph_Ph_Dod_Dte", "PH-DOD-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "PH_DOD_DTE");
        cwf_Sb_Ph_Ph_Dod_Dte.setDdmHeader("PH/DOD");

        cwf_Sb_Ph__R_Field_2 = vw_cwf_Sb_Ph.getRecord().newGroupInGroup("cwf_Sb_Ph__R_Field_2", "REDEFINE", cwf_Sb_Ph_Ph_Dod_Dte);
        cwf_Sb_Ph_Dod_Yyyy = cwf_Sb_Ph__R_Field_2.newFieldInGroup("cwf_Sb_Ph_Dod_Yyyy", "DOD-YYYY", FieldType.STRING, 4);
        cwf_Sb_Ph_Sb_Converted = vw_cwf_Sb_Ph.getRecord().newFieldInGroup("cwf_Sb_Ph_Sb_Converted", "SB-CONVERTED", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "SB_CONVERTED");
        cwf_Sb_Ph_Work_List_Ind = vw_cwf_Sb_Ph.getRecord().newFieldInGroup("cwf_Sb_Ph_Work_List_Ind", "WORK-LIST-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "WORK_LIST_IND");
        registerRecord(vw_cwf_Sb_Ph);

        pnd_Ph_Key = localVariables.newFieldInRecord("pnd_Ph_Key", "#PH-KEY", FieldType.STRING, 13);

        pnd_Ph_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Ph_Key__R_Field_3", "REDEFINE", pnd_Ph_Key);
        pnd_Ph_Key_Rcrd_Type = pnd_Ph_Key__R_Field_3.newFieldInGroup("pnd_Ph_Key_Rcrd_Type", "RCRD-TYPE", FieldType.STRING, 1);
        pnd_Ph_Key_Srvvr_Mit_Idntfr = pnd_Ph_Key__R_Field_3.newFieldInGroup("pnd_Ph_Key_Srvvr_Mit_Idntfr", "SRVVR-MIT-IDNTFR", FieldType.NUMERIC, 12);

        vw_master_1 = new DataAccessProgramView(new NameInfo("vw_master_1", "MASTER-1"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        master_1_Pin_Nbr = vw_master_1.getRecord().newFieldInGroup("master_1_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "PIN_NBR");
        master_1_Pin_Nbr.setDdmHeader("PIN");
        master_1_Crprte_Status_Ind = vw_master_1.getRecord().newFieldInGroup("master_1_Crprte_Status_Ind", "CRPRTE-STATUS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CRPRTE_STATUS_IND");
        master_1_Crprte_Status_Ind.setDdmHeader("CORP/STAT");
        master_1_Work_Prcss_Id = vw_master_1.getRecord().newFieldInGroup("master_1_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "WORK_PRCSS_ID");
        master_1_Work_Prcss_Id.setDdmHeader("WORK/ID");
        master_1_Unit_Cde = vw_master_1.getRecord().newFieldInGroup("master_1_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "UNIT_CDE");
        master_1_Unit_Cde.setDdmHeader("UNIT/CODE");
        master_1_Status_Cde = vw_master_1.getRecord().newFieldInGroup("master_1_Status_Cde", "STATUS-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "STATUS_CDE");
        master_1_Rqst_Log_Dte_Tme = vw_master_1.getRecord().newFieldInGroup("master_1_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "RQST_LOG_DTE_TME");
        master_1_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        master_1_Status_Updte_Dte_Tme = vw_master_1.getRecord().newFieldInGroup("master_1_Status_Updte_Dte_Tme", "STATUS-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "STATUS_UPDTE_DTE_TME");
        master_1_Status_Updte_Dte_Tme.setDdmHeader("STATUS UPDATE/DATE-TIME");
        registerRecord(vw_master_1);

        pnd_Pin_History_Key = localVariables.newFieldInRecord("pnd_Pin_History_Key", "#PIN-HISTORY-KEY", FieldType.STRING, 30);

        pnd_Pin_History_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Pin_History_Key__R_Field_4", "REDEFINE", pnd_Pin_History_Key);
        pnd_Pin_History_Key_Pin_Nbr = pnd_Pin_History_Key__R_Field_4.newFieldInGroup("pnd_Pin_History_Key_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12);
        pnd_Pin_History_Key_Crprte_Status_Ind = pnd_Pin_History_Key__R_Field_4.newFieldInGroup("pnd_Pin_History_Key_Crprte_Status_Ind", "CRPRTE-STATUS-IND", 
            FieldType.STRING, 1);

        vw_cwf_Sb_Work_Req = new DataAccessProgramView(new NameInfo("vw_cwf_Sb_Work_Req", "CWF-SB-WORK-REQ"), "CWF_SB_WORK_REQ", "CWF_SB_WORK_REQ");
        cwf_Sb_Work_Req_Srvvr_Mit_Idntfr = vw_cwf_Sb_Work_Req.getRecord().newFieldInGroup("cwf_Sb_Work_Req_Srvvr_Mit_Idntfr", "SRVVR-MIT-IDNTFR", FieldType.STRING, 
            14, RepeatingFieldStrategy.None, "SRVVR_MIT_IDNTFR");
        cwf_Sb_Work_Req_Srvvr_Mit_Idntfr.setDdmHeader("SURVIVOR MIT/IDENTIFIER");

        cwf_Sb_Work_Req__R_Field_5 = vw_cwf_Sb_Work_Req.getRecord().newGroupInGroup("cwf_Sb_Work_Req__R_Field_5", "REDEFINE", cwf_Sb_Work_Req_Srvvr_Mit_Idntfr);
        cwf_Sb_Work_Req_Ph_Pin_Nbr = cwf_Sb_Work_Req__R_Field_5.newFieldInGroup("cwf_Sb_Work_Req_Ph_Pin_Nbr", "PH-PIN-NBR", FieldType.NUMERIC, 12);
        cwf_Sb_Work_Req_Srvvr_Mit_Sqnce = cwf_Sb_Work_Req__R_Field_5.newFieldInGroup("cwf_Sb_Work_Req_Srvvr_Mit_Sqnce", "SRVVR-MIT-SQNCE", FieldType.NUMERIC, 
            2);
        cwf_Sb_Work_Req_Rqst_Log_Dte_Tme = vw_cwf_Sb_Work_Req.getRecord().newFieldInGroup("cwf_Sb_Work_Req_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Sb_Work_Req_Rqst_Log_Dte_Tme.setDdmHeader("RQST LOG/DATE/TIME");
        cwf_Sb_Work_Req_Forms_Mail_Dte = vw_cwf_Sb_Work_Req.getRecord().newFieldInGroup("cwf_Sb_Work_Req_Forms_Mail_Dte", "FORMS-MAIL-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "FORMS_MAIL_DTE");

        cwf_Sb_Work_Req__R_Field_6 = vw_cwf_Sb_Work_Req.getRecord().newGroupInGroup("cwf_Sb_Work_Req__R_Field_6", "REDEFINE", cwf_Sb_Work_Req_Forms_Mail_Dte);
        cwf_Sb_Work_Req_Forms_Mail_Dte_A8 = cwf_Sb_Work_Req__R_Field_6.newFieldInGroup("cwf_Sb_Work_Req_Forms_Mail_Dte_A8", "FORMS-MAIL-DTE-A8", FieldType.STRING, 
            8);
        cwf_Sb_Work_Req_Thirty_Day_Crte_Dte = vw_cwf_Sb_Work_Req.getRecord().newFieldInGroup("cwf_Sb_Work_Req_Thirty_Day_Crte_Dte", "THIRTY-DAY-CRTE-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "THIRTY_DAY_CRTE_DTE");
        cwf_Sb_Work_Req_Ninety_Day_Crte_Dte = vw_cwf_Sb_Work_Req.getRecord().newFieldInGroup("cwf_Sb_Work_Req_Ninety_Day_Crte_Dte", "NINETY-DAY-CRTE-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "NINETY_DAY_CRTE_DTE");
        cwf_Sb_Work_Req_Ninety_Day_Crte_Nbr = vw_cwf_Sb_Work_Req.getRecord().newFieldInGroup("cwf_Sb_Work_Req_Ninety_Day_Crte_Nbr", "NINETY-DAY-CRTE-NBR", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "NINETY_DAY_CRTE_NBR");
        cwf_Sb_Work_Req_Md_Mds_Crte_Dte = vw_cwf_Sb_Work_Req.getRecord().newFieldInGroup("cwf_Sb_Work_Req_Md_Mds_Crte_Dte", "MD-MDS-CRTE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "MD_MDS_CRTE_DTE");

        cwf_Sb_Work_Req__R_Field_7 = vw_cwf_Sb_Work_Req.getRecord().newGroupInGroup("cwf_Sb_Work_Req__R_Field_7", "REDEFINE", cwf_Sb_Work_Req_Md_Mds_Crte_Dte);
        cwf_Sb_Work_Req_Md_Mds_Yyyy = cwf_Sb_Work_Req__R_Field_7.newFieldInGroup("cwf_Sb_Work_Req_Md_Mds_Yyyy", "MD-MDS-YYYY", FieldType.STRING, 4);
        cwf_Sb_Work_Req_Md_Mds_Crte_Nbr = vw_cwf_Sb_Work_Req.getRecord().newFieldInGroup("cwf_Sb_Work_Req_Md_Mds_Crte_Nbr", "MD-MDS-CRTE-NBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "MD_MDS_CRTE_NBR");
        cwf_Sb_Work_Req_Md_Md1_Crte_Dte = vw_cwf_Sb_Work_Req.getRecord().newFieldInGroup("cwf_Sb_Work_Req_Md_Md1_Crte_Dte", "MD-MD1-CRTE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "MD_MD1_CRTE_DTE");

        cwf_Sb_Work_Req__R_Field_8 = vw_cwf_Sb_Work_Req.getRecord().newGroupInGroup("cwf_Sb_Work_Req__R_Field_8", "REDEFINE", cwf_Sb_Work_Req_Md_Md1_Crte_Dte);
        cwf_Sb_Work_Req_Md_Md1_Yyyy = cwf_Sb_Work_Req__R_Field_8.newFieldInGroup("cwf_Sb_Work_Req_Md_Md1_Yyyy", "MD-MD1-YYYY", FieldType.STRING, 4);
        cwf_Sb_Work_Req_Md_Md1_Crte_Nbr = vw_cwf_Sb_Work_Req.getRecord().newFieldInGroup("cwf_Sb_Work_Req_Md_Md1_Crte_Nbr", "MD-MD1-CRTE-NBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "MD_MD1_CRTE_NBR");
        cwf_Sb_Work_Req_Md_Md2_Crte_Dte = vw_cwf_Sb_Work_Req.getRecord().newFieldInGroup("cwf_Sb_Work_Req_Md_Md2_Crte_Dte", "MD-MD2-CRTE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "MD_MD2_CRTE_DTE");

        cwf_Sb_Work_Req__R_Field_9 = vw_cwf_Sb_Work_Req.getRecord().newGroupInGroup("cwf_Sb_Work_Req__R_Field_9", "REDEFINE", cwf_Sb_Work_Req_Md_Md2_Crte_Dte);
        cwf_Sb_Work_Req_Md_Md2_Yyyy = cwf_Sb_Work_Req__R_Field_9.newFieldInGroup("cwf_Sb_Work_Req_Md_Md2_Yyyy", "MD-MD2-YYYY", FieldType.STRING, 4);
        cwf_Sb_Work_Req_Md_Md2_Crte_Nbr = vw_cwf_Sb_Work_Req.getRecord().newFieldInGroup("cwf_Sb_Work_Req_Md_Md2_Crte_Nbr", "MD-MD2-CRTE-NBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "MD_MD2_CRTE_NBR");
        cwf_Sb_Work_Req_Corresp_Crte_Dte = vw_cwf_Sb_Work_Req.getRecord().newFieldInGroup("cwf_Sb_Work_Req_Corresp_Crte_Dte", "CORRESP-CRTE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CORRESP_CRTE_DTE");
        cwf_Sb_Work_Req_Corresp_30d_Crte_Dte = vw_cwf_Sb_Work_Req.getRecord().newFieldInGroup("cwf_Sb_Work_Req_Corresp_30d_Crte_Dte", "CORRESP-30D-CRTE-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CORRESP_30D_CRTE_DTE");
        cwf_Sb_Work_Req_Corresp_90d_Crte_Dte = vw_cwf_Sb_Work_Req.getRecord().newFieldInGroup("cwf_Sb_Work_Req_Corresp_90d_Crte_Dte", "CORRESP-90D-CRTE-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CORRESP_90D_CRTE_DTE");
        cwf_Sb_Work_Req_Corresp_90d_Crte_Nbr = vw_cwf_Sb_Work_Req.getRecord().newFieldInGroup("cwf_Sb_Work_Req_Corresp_90d_Crte_Nbr", "CORRESP-90D-CRTE-NBR", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CORRESP_90D_CRTE_NBR");
        registerRecord(vw_cwf_Sb_Work_Req);

        vw_cwf_Sb_Bene = new DataAccessProgramView(new NameInfo("vw_cwf_Sb_Bene", "CWF-SB-BENE"), "CWF_SB_BENE", "CWF_SB_BENE");
        cwf_Sb_Bene_Rcrd_Type = vw_cwf_Sb_Bene.getRecord().newFieldInGroup("cwf_Sb_Bene_Rcrd_Type", "RCRD-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RCRD_TYPE");
        cwf_Sb_Bene_Rcrd_Type.setDdmHeader("RECORD/TYPE");
        cwf_Sb_Bene_Srvvr_Mit_Idntfr = vw_cwf_Sb_Bene.getRecord().newFieldInGroup("cwf_Sb_Bene_Srvvr_Mit_Idntfr", "SRVVR-MIT-IDNTFR", FieldType.STRING, 
            14, RepeatingFieldStrategy.None, "SRVVR_MIT_IDNTFR");
        cwf_Sb_Bene_Srvvr_Mit_Idntfr.setDdmHeader("SURVIVOR MIT/IDENTIFIER");

        cwf_Sb_Bene__R_Field_10 = vw_cwf_Sb_Bene.getRecord().newGroupInGroup("cwf_Sb_Bene__R_Field_10", "REDEFINE", cwf_Sb_Bene_Srvvr_Mit_Idntfr);
        cwf_Sb_Bene_Ph_Pin_Nbr = cwf_Sb_Bene__R_Field_10.newFieldInGroup("cwf_Sb_Bene_Ph_Pin_Nbr", "PH-PIN-NBR", FieldType.NUMERIC, 12);
        cwf_Sb_Bene_Srvvr_Mit_Sqnce = cwf_Sb_Bene__R_Field_10.newFieldInGroup("cwf_Sb_Bene_Srvvr_Mit_Sqnce", "SRVVR-MIT-SQNCE", FieldType.NUMERIC, 2);
        cwf_Sb_Bene_Srvvr_Nme_Fr_Frm = vw_cwf_Sb_Bene.getRecord().newFieldInGroup("cwf_Sb_Bene_Srvvr_Nme_Fr_Frm", "SRVVR-NME-FR-FRM", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "SRVVR_NME_FR_FRM");
        cwf_Sb_Bene_Srvvr_Nme_Fr_Frm.setDdmHeader("SURVIVOR NME/FREE FORM");
        cwf_Sb_Bene_Srvvr_Nme_Fr_Fr2 = vw_cwf_Sb_Bene.getRecord().newFieldInGroup("cwf_Sb_Bene_Srvvr_Nme_Fr_Fr2", "SRVVR-NME-FR-FR2", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "SRVVR_NME_FR_FR2");
        cwf_Sb_Bene_Srvvr_Type = vw_cwf_Sb_Bene.getRecord().newFieldInGroup("cwf_Sb_Bene_Srvvr_Type", "SRVVR-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "SRVVR_TYPE");
        cwf_Sb_Bene_Srvvr_Type.setDdmHeader("SURVIVOR/TYPE");

        cwf_Sb_Bene_Srvvr_Addr = vw_cwf_Sb_Bene.getRecord().newGroupInGroup("CWF_SB_BENE_SRVVR_ADDR", "SRVVR-ADDR");
        cwf_Sb_Bene_Srvvr_Addr.setDdmHeader("SURVIVOR/ADDRESS");
        cwf_Sb_Bene_Srvvr_Addr_Line1 = cwf_Sb_Bene_Srvvr_Addr.newFieldInGroup("cwf_Sb_Bene_Srvvr_Addr_Line1", "SRVVR-ADDR-LINE1", FieldType.STRING, 35, 
            RepeatingFieldStrategy.None, "SRVVR_ADDR_LINE1");
        cwf_Sb_Bene_Srvvr_Addr_Line1.setDdmHeader("SURVIVOR/ADDR LINE1");
        cwf_Sb_Bene_Srvvr_Addr_Line2 = cwf_Sb_Bene_Srvvr_Addr.newFieldInGroup("cwf_Sb_Bene_Srvvr_Addr_Line2", "SRVVR-ADDR-LINE2", FieldType.STRING, 35, 
            RepeatingFieldStrategy.None, "SRVVR_ADDR_LINE2");
        cwf_Sb_Bene_Srvvr_Addr_Line2.setDdmHeader("SURVIVOR/ADDR LINE2");
        cwf_Sb_Bene_Srvvr_Addr_Line3 = cwf_Sb_Bene_Srvvr_Addr.newFieldInGroup("cwf_Sb_Bene_Srvvr_Addr_Line3", "SRVVR-ADDR-LINE3", FieldType.STRING, 35, 
            RepeatingFieldStrategy.None, "SRVVR_ADDR_LINE3");
        cwf_Sb_Bene_Srvvr_Addr_Line3.setDdmHeader("SURVIVOR/ADDR LINE3");
        cwf_Sb_Bene_Srvvr_Addr_Line4 = cwf_Sb_Bene_Srvvr_Addr.newFieldInGroup("cwf_Sb_Bene_Srvvr_Addr_Line4", "SRVVR-ADDR-LINE4", FieldType.STRING, 35, 
            RepeatingFieldStrategy.None, "SRVVR_ADDR_LINE4");
        cwf_Sb_Bene_Srvvr_Addr_Line4.setDdmHeader("SURVIVOR/ADDR LINE4");
        cwf_Sb_Bene_Srvvr_Addr_Line5 = cwf_Sb_Bene_Srvvr_Addr.newFieldInGroup("cwf_Sb_Bene_Srvvr_Addr_Line5", "SRVVR-ADDR-LINE5", FieldType.STRING, 35, 
            RepeatingFieldStrategy.None, "SRVVR_ADDR_LINE5");
        cwf_Sb_Bene_Srvvr_Addr_Line5.setDdmHeader("SURVIVOR/ADDR LINE5");
        cwf_Sb_Bene_Srvvr_Addr_Line6 = cwf_Sb_Bene_Srvvr_Addr.newFieldInGroup("cwf_Sb_Bene_Srvvr_Addr_Line6", "SRVVR-ADDR-LINE6", FieldType.STRING, 35, 
            RepeatingFieldStrategy.None, "SRVVR_ADDR_LINE6");
        cwf_Sb_Bene_Srvvr_Addr_Line6.setDdmHeader("SURVIVOR/ADDR LINE6");
        cwf_Sb_Bene_Srvvr_Zip_Cde = cwf_Sb_Bene_Srvvr_Addr.newFieldInGroup("cwf_Sb_Bene_Srvvr_Zip_Cde", "SRVVR-ZIP-CDE", FieldType.STRING, 9, RepeatingFieldStrategy.None, 
            "SRVVR_ZIP_CDE");
        cwf_Sb_Bene_Srvvr_Zip_Cde.setDdmHeader("SURVIVOR/ZIP CODE");

        cwf_Sb_Bene__R_Field_11 = cwf_Sb_Bene_Srvvr_Addr.newGroupInGroup("cwf_Sb_Bene__R_Field_11", "REDEFINE", cwf_Sb_Bene_Srvvr_Zip_Cde);
        cwf_Sb_Bene_Zip_Cde_1_5 = cwf_Sb_Bene__R_Field_11.newFieldInGroup("cwf_Sb_Bene_Zip_Cde_1_5", "ZIP-CDE-1-5", FieldType.STRING, 5);
        cwf_Sb_Bene_Zip_Cde_6_9 = cwf_Sb_Bene__R_Field_11.newFieldInGroup("cwf_Sb_Bene_Zip_Cde_6_9", "ZIP-CDE-6-9", FieldType.STRING, 4);
        cwf_Sb_Bene_State = cwf_Sb_Bene_Srvvr_Addr.newFieldInGroup("cwf_Sb_Bene_State", "STATE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "STATE");
        cwf_Sb_Bene_State.setDdmHeader("SURVIVOR/RES CODE");

        cwf_Sb_Bene__R_Field_12 = vw_cwf_Sb_Bene.getRecord().newGroupInGroup("cwf_Sb_Bene__R_Field_12", "REDEFINE", cwf_Sb_Bene_Srvvr_Addr);
        cwf_Sb_Bene_Addr_Arr = cwf_Sb_Bene__R_Field_12.newFieldArrayInGroup("cwf_Sb_Bene_Addr_Arr", "ADDR-ARR", FieldType.STRING, 35, new DbsArrayController(1, 
            6));

        cwf_Sb_Bene__R_Field_13 = vw_cwf_Sb_Bene.getRecord().newGroupInGroup("cwf_Sb_Bene__R_Field_13", "REDEFINE", cwf_Sb_Bene_Srvvr_Addr);
        cwf_Sb_Bene_Addr_All = cwf_Sb_Bene__R_Field_13.newFieldInGroup("cwf_Sb_Bene_Addr_All", "ADDR-ALL", FieldType.STRING, 210);
        cwf_Sb_Bene_Us_Address = vw_cwf_Sb_Bene.getRecord().newFieldInGroup("cwf_Sb_Bene_Us_Address", "US-ADDRESS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "US_ADDRESS");
        cwf_Sb_Bene_Srvvr_Spse_Ind = vw_cwf_Sb_Bene.getRecord().newFieldInGroup("cwf_Sb_Bene_Srvvr_Spse_Ind", "SRVVR-SPSE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "SRVVR_SPSE_IND");
        cwf_Sb_Bene_Srvvr_Spse_Ind.setDdmHeader("SURVIVOR/SPOUSE");
        cwf_Sb_Bene_Srvvr_Rltnshp = vw_cwf_Sb_Bene.getRecord().newFieldInGroup("cwf_Sb_Bene_Srvvr_Rltnshp", "SRVVR-RLTNSHP", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "SRVVR_RLTNSHP");
        cwf_Sb_Bene_Srvvr_Rltnshp.setDdmHeader("SURVIVOR/RELATIONSHIP");
        cwf_Sb_Bene_Last_Updte_Dte_Tme = vw_cwf_Sb_Bene.getRecord().newFieldInGroup("cwf_Sb_Bene_Last_Updte_Dte_Tme", "LAST-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LAST_UPDTE_DTE_TME");
        cwf_Sb_Bene_Last_Updte_Dte_Tme.setDdmHeader("LAST UPDATE/DATE/TIME");
        registerRecord(vw_cwf_Sb_Bene);

        pnd_Srvvr_Key = localVariables.newFieldInRecord("pnd_Srvvr_Key", "#SRVVR-KEY", FieldType.STRING, 15);

        pnd_Srvvr_Key__R_Field_14 = localVariables.newGroupInRecord("pnd_Srvvr_Key__R_Field_14", "REDEFINE", pnd_Srvvr_Key);
        pnd_Srvvr_Key_Rcrd_Type = pnd_Srvvr_Key__R_Field_14.newFieldInGroup("pnd_Srvvr_Key_Rcrd_Type", "RCRD-TYPE", FieldType.STRING, 1);
        pnd_Srvvr_Key_Srvvr_Mit_Idntfr = pnd_Srvvr_Key__R_Field_14.newFieldInGroup("pnd_Srvvr_Key_Srvvr_Mit_Idntfr", "SRVVR-MIT-IDNTFR", FieldType.STRING, 
            14);

        vw_cwf_Support_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Support_Tbl", "CWF-SUPPORT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Support_Tbl_Tbl_Table_Nme = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Support_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Support_Tbl_Tbl_Key_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");
        cwf_Support_Tbl_Tbl_Data_Field = vw_cwf_Support_Tbl.getRecord().newFieldInGroup("cwf_Support_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Support_Tbl__R_Field_15 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_15", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Tbl_Reps = cwf_Support_Tbl__R_Field_15.newFieldArrayInGroup("cwf_Support_Tbl_Tbl_Reps", "TBL-REPS", FieldType.STRING, 25, new 
            DbsArrayController(1, 10));

        cwf_Support_Tbl__R_Field_16 = cwf_Support_Tbl__R_Field_15.newGroupInGroup("cwf_Support_Tbl__R_Field_16", "REDEFINE", cwf_Support_Tbl_Tbl_Reps);

        cwf_Support_Tbl_Tbl_Reps_Redef = cwf_Support_Tbl__R_Field_16.newGroupArrayInGroup("cwf_Support_Tbl_Tbl_Reps_Redef", "TBL-REPS-REDEF", new DbsArrayController(1, 
            10));
        cwf_Support_Tbl_Rep_Actv_Ind = cwf_Support_Tbl_Tbl_Reps_Redef.newFieldInGroup("cwf_Support_Tbl_Rep_Actv_Ind", "REP-ACTV-IND", FieldType.STRING, 
            1);
        cwf_Support_Tbl_Rep_Id = cwf_Support_Tbl_Tbl_Reps_Redef.newFieldInGroup("cwf_Support_Tbl_Rep_Id", "REP-ID", FieldType.STRING, 8);
        cwf_Support_Tbl__Filler1 = cwf_Support_Tbl_Tbl_Reps_Redef.newFieldInGroup("cwf_Support_Tbl__Filler1", "_FILLER1", FieldType.STRING, 16);

        cwf_Support_Tbl__R_Field_17 = vw_cwf_Support_Tbl.getRecord().newGroupInGroup("cwf_Support_Tbl__R_Field_17", "REDEFINE", cwf_Support_Tbl_Tbl_Data_Field);
        cwf_Support_Tbl_Stats = cwf_Support_Tbl__R_Field_17.newFieldArrayInGroup("cwf_Support_Tbl_Stats", "STATS", FieldType.STRING, 9, new DbsArrayController(1, 
            28));

        cwf_Support_Tbl__R_Field_18 = cwf_Support_Tbl__R_Field_17.newGroupInGroup("cwf_Support_Tbl__R_Field_18", "REDEFINE", cwf_Support_Tbl_Stats);

        cwf_Support_Tbl_Stats_Redef = cwf_Support_Tbl__R_Field_18.newGroupArrayInGroup("cwf_Support_Tbl_Stats_Redef", "STATS-REDEF", new DbsArrayController(1, 
            28));
        cwf_Support_Tbl_Stat_Cde = cwf_Support_Tbl_Stats_Redef.newFieldInGroup("cwf_Support_Tbl_Stat_Cde", "STAT-CDE", FieldType.STRING, 4);
        cwf_Support_Tbl__Filler2 = cwf_Support_Tbl_Stats_Redef.newFieldInGroup("cwf_Support_Tbl__Filler2", "_FILLER2", FieldType.STRING, 1);
        cwf_Support_Tbl_Stage = cwf_Support_Tbl_Stats_Redef.newFieldInGroup("cwf_Support_Tbl_Stage", "STAGE", FieldType.STRING, 1);
        cwf_Support_Tbl__Filler3 = cwf_Support_Tbl_Stats_Redef.newFieldInGroup("cwf_Support_Tbl__Filler3", "_FILLER3", FieldType.STRING, 1);
        cwf_Support_Tbl_Advoc = cwf_Support_Tbl_Stats_Redef.newFieldInGroup("cwf_Support_Tbl_Advoc", "ADVOC", FieldType.STRING, 1);
        cwf_Support_Tbl__Filler4 = cwf_Support_Tbl_Stats_Redef.newFieldInGroup("cwf_Support_Tbl__Filler4", "_FILLER4", FieldType.STRING, 1);
        registerRecord(vw_cwf_Support_Tbl);

        pnd_Tbl_Key = localVariables.newFieldInRecord("pnd_Tbl_Key", "#TBL-KEY", FieldType.STRING, 53);

        pnd_Tbl_Key__R_Field_19 = localVariables.newGroupInRecord("pnd_Tbl_Key__R_Field_19", "REDEFINE", pnd_Tbl_Key);
        pnd_Tbl_Key_Tbl_Scrty_Level_Ind = pnd_Tbl_Key__R_Field_19.newFieldInGroup("pnd_Tbl_Key_Tbl_Scrty_Level_Ind", "TBL-SCRTY-LEVEL-IND", FieldType.STRING, 
            2);
        pnd_Tbl_Key_Tbl_Table_Nme = pnd_Tbl_Key__R_Field_19.newFieldInGroup("pnd_Tbl_Key_Tbl_Table_Nme", "TBL-TABLE-NME", FieldType.STRING, 20);
        pnd_Tbl_Key_Tbl_Key_Field = pnd_Tbl_Key__R_Field_19.newFieldInGroup("pnd_Tbl_Key_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 30);
        pnd_Md_Control_Rec = localVariables.newFieldInRecord("pnd_Md_Control_Rec", "#MD-CONTROL-REC", FieldType.STRING, 253);
        pnd_90d_Control_Rec = localVariables.newFieldInRecord("pnd_90d_Control_Rec", "#90D-CONTROL-REC", FieldType.STRING, 253);
        pnd_Control_Rec = localVariables.newFieldInRecord("pnd_Control_Rec", "#CONTROL-REC", FieldType.STRING, 253);

        pnd_Control_Rec__R_Field_20 = localVariables.newGroupInRecord("pnd_Control_Rec__R_Field_20", "REDEFINE", pnd_Control_Rec);
        pnd_Control_Rec_Pnd_Ia_Da = pnd_Control_Rec__R_Field_20.newFieldInGroup("pnd_Control_Rec_Pnd_Ia_Da", "#IA-DA", FieldType.STRING, 2);
        pnd_Control_Rec__Filler5 = pnd_Control_Rec__R_Field_20.newFieldInGroup("pnd_Control_Rec__Filler5", "_FILLER5", FieldType.STRING, 1);
        pnd_Control_Rec_Pnd_Max_Pins = pnd_Control_Rec__R_Field_20.newFieldInGroup("pnd_Control_Rec_Pnd_Max_Pins", "#MAX-PINS", FieldType.NUMERIC, 7);
        pnd_Control_Rec__Filler6 = pnd_Control_Rec__R_Field_20.newFieldInGroup("pnd_Control_Rec__Filler6", "_FILLER6", FieldType.STRING, 1);
        pnd_Control_Rec_Pnd_Last_Pin = pnd_Control_Rec__R_Field_20.newFieldInGroup("pnd_Control_Rec_Pnd_Last_Pin", "#LAST-PIN", FieldType.NUMERIC, 12);
        pnd_Control_Rec__Filler7 = pnd_Control_Rec__R_Field_20.newFieldInGroup("pnd_Control_Rec__Filler7", "_FILLER7", FieldType.STRING, 1);
        pnd_Control_Rec_Pnd_Last_Run_Date = pnd_Control_Rec__R_Field_20.newFieldInGroup("pnd_Control_Rec_Pnd_Last_Run_Date", "#LAST-RUN-DATE", FieldType.STRING, 
            8);
        pnd_Control_Rec__Filler8 = pnd_Control_Rec__R_Field_20.newFieldInGroup("pnd_Control_Rec__Filler8", "_FILLER8", FieldType.STRING, 1);
        pnd_Control_Rec_Pnd_Printer_Id = pnd_Control_Rec__R_Field_20.newFieldInGroup("pnd_Control_Rec_Pnd_Printer_Id", "#PRINTER-ID", FieldType.STRING, 
            4);
        pnd_Control_Rec__Filler9 = pnd_Control_Rec__R_Field_20.newFieldInGroup("pnd_Control_Rec__Filler9", "_FILLER9", FieldType.STRING, 1);
        pnd_Control_Rec_Pnd_Printed_Pin_Cnt = pnd_Control_Rec__R_Field_20.newFieldInGroup("pnd_Control_Rec_Pnd_Printed_Pin_Cnt", "#PRINTED-PIN-CNT", FieldType.NUMERIC, 
            7);
        pnd_Control_Rec__Filler10 = pnd_Control_Rec__R_Field_20.newFieldInGroup("pnd_Control_Rec__Filler10", "_FILLER10", FieldType.STRING, 1);
        pnd_Control_Rec_Pnd_Printed_Letter_Cnt = pnd_Control_Rec__R_Field_20.newFieldInGroup("pnd_Control_Rec_Pnd_Printed_Letter_Cnt", "#PRINTED-LETTER-CNT", 
            FieldType.NUMERIC, 7);
        pnd_Control_Rec__Filler11 = pnd_Control_Rec__R_Field_20.newFieldInGroup("pnd_Control_Rec__Filler11", "_FILLER11", FieldType.STRING, 1);
        pnd_Control_Rec_Pnd_Md_Start_Mmdd = pnd_Control_Rec__R_Field_20.newFieldInGroup("pnd_Control_Rec_Pnd_Md_Start_Mmdd", "#MD-START-MMDD", FieldType.STRING, 
            4);
        pnd_Control_Rec__Filler12 = pnd_Control_Rec__R_Field_20.newFieldInGroup("pnd_Control_Rec__Filler12", "_FILLER12", FieldType.STRING, 1);
        pnd_Control_Rec_Pnd_Md_End_Mmdd = pnd_Control_Rec__R_Field_20.newFieldInGroup("pnd_Control_Rec_Pnd_Md_End_Mmdd", "#MD-END-MMDD", FieldType.STRING, 
            4);
        pnd_Todays_Date_D = localVariables.newFieldInRecord("pnd_Todays_Date_D", "#TODAYS-DATE-D", FieldType.DATE);
        pnd_Todays_Date_A8 = localVariables.newFieldInRecord("pnd_Todays_Date_A8", "#TODAYS-DATE-A8", FieldType.STRING, 8);

        pnd_Todays_Date_A8__R_Field_21 = localVariables.newGroupInRecord("pnd_Todays_Date_A8__R_Field_21", "REDEFINE", pnd_Todays_Date_A8);
        pnd_Todays_Date_A8_Pnd_Todays_Date_N8 = pnd_Todays_Date_A8__R_Field_21.newFieldInGroup("pnd_Todays_Date_A8_Pnd_Todays_Date_N8", "#TODAYS-DATE-N8", 
            FieldType.NUMERIC, 8);

        pnd_Todays_Date_A8__R_Field_22 = localVariables.newGroupInRecord("pnd_Todays_Date_A8__R_Field_22", "REDEFINE", pnd_Todays_Date_A8);
        pnd_Todays_Date_A8_Pnd_Todays_Yyyy = pnd_Todays_Date_A8__R_Field_22.newFieldInGroup("pnd_Todays_Date_A8_Pnd_Todays_Yyyy", "#TODAYS-YYYY", FieldType.STRING, 
            4);

        pnd_Todays_Date_A8__R_Field_23 = localVariables.newGroupInRecord("pnd_Todays_Date_A8__R_Field_23", "REDEFINE", pnd_Todays_Date_A8);
        pnd_Todays_Date_A8_Pnd_Todays_Yyyy_N = pnd_Todays_Date_A8__R_Field_23.newFieldInGroup("pnd_Todays_Date_A8_Pnd_Todays_Yyyy_N", "#TODAYS-YYYY-N", 
            FieldType.NUMERIC, 4);
        pnd_Last_Years_Yyyy = localVariables.newFieldInRecord("pnd_Last_Years_Yyyy", "#LAST-YEARS-YYYY", FieldType.STRING, 4);
        pnd_Last_Date_A8 = localVariables.newFieldInRecord("pnd_Last_Date_A8", "#LAST-DATE-A8", FieldType.STRING, 8);

        pnd_Last_Date_A8__R_Field_24 = localVariables.newGroupInRecord("pnd_Last_Date_A8__R_Field_24", "REDEFINE", pnd_Last_Date_A8);
        pnd_Last_Date_A8_Pnd_Last_Date_N8 = pnd_Last_Date_A8__R_Field_24.newFieldInGroup("pnd_Last_Date_A8_Pnd_Last_Date_N8", "#LAST-DATE-N8", FieldType.NUMERIC, 
            8);
        pnd_Last_Date_D = localVariables.newFieldInRecord("pnd_Last_Date_D", "#LAST-DATE-D", FieldType.DATE);
        pnd_Md_Start_A8 = localVariables.newFieldInRecord("pnd_Md_Start_A8", "#MD-START-A8", FieldType.STRING, 8);
        pnd_Md_End_A8 = localVariables.newFieldInRecord("pnd_Md_End_A8", "#MD-END-A8", FieldType.STRING, 8);
        pnd_Md = localVariables.newFieldInRecord("pnd_Md", "#MD", FieldType.BOOLEAN, 1);
        pnd_90d = localVariables.newFieldInRecord("pnd_90d", "#90D", FieldType.BOOLEAN, 1);
        pnd_Report_Title = localVariables.newFieldInRecord("pnd_Report_Title", "#REPORT-TITLE", FieldType.STRING, 65);
        pnd_Letter_Type = localVariables.newFieldInRecord("pnd_Letter_Type", "#LETTER-TYPE", FieldType.STRING, 1);
        pnd_Letter_Type_Desc = localVariables.newFieldInRecord("pnd_Letter_Type_Desc", "#LETTER-TYPE-DESC", FieldType.STRING, 8);
        pnd_Addr_Typ_Cde = localVariables.newFieldInRecord("pnd_Addr_Typ_Cde", "#ADDR-TYP-CDE", FieldType.STRING, 1);
        pnd_Md_Dte_C_Or_F = localVariables.newFieldInRecord("pnd_Md_Dte_C_Or_F", "#MD-DTE-C-OR-F", FieldType.STRING, 1);
        pnd_Md_Dte = localVariables.newFieldInRecord("pnd_Md_Dte", "#MD-DTE", FieldType.NUMERIC, 8);

        pnd_Md_Dte__R_Field_25 = localVariables.newGroupInRecord("pnd_Md_Dte__R_Field_25", "REDEFINE", pnd_Md_Dte);
        pnd_Md_Dte_Pnd_Md_Dte_Yyyy = pnd_Md_Dte__R_Field_25.newFieldInGroup("pnd_Md_Dte_Pnd_Md_Dte_Yyyy", "#MD-DTE-YYYY", FieldType.STRING, 4);
        pnd_Err_Msg = localVariables.newFieldInRecord("pnd_Err_Msg", "#ERR-MSG", FieldType.STRING, 70);
        pnd_Letter_Cnt = localVariables.newFieldInRecord("pnd_Letter_Cnt", "#LETTER-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Starting_Pin_Cnt = localVariables.newFieldInRecord("pnd_Starting_Pin_Cnt", "#STARTING-PIN-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Pins = localVariables.newFieldInRecord("pnd_Total_Pins", "#TOTAL-PINS", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Letters = localVariables.newFieldInRecord("pnd_Total_Letters", "#TOTAL-LETTERS", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Errors = localVariables.newFieldInRecord("pnd_Total_Errors", "#TOTAL-ERRORS", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_90d_Corresp = localVariables.newFieldInRecord("pnd_Total_90d_Corresp", "#TOTAL-90D-CORRESP", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_90d = localVariables.newFieldInRecord("pnd_Total_90d", "#TOTAL-90D", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Mds = localVariables.newFieldInRecord("pnd_Total_Mds", "#TOTAL-MDS", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Md1 = localVariables.newFieldInRecord("pnd_Total_Md1", "#TOTAL-MD1", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Md2 = localVariables.newFieldInRecord("pnd_Total_Md2", "#TOTAL-MD2", FieldType.PACKED_DECIMAL, 7);
        pnd_Et_Cnt = localVariables.newFieldInRecord("pnd_Et_Cnt", "#ET-CNT", FieldType.INTEGER, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 1);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 1);
        pnd_Lob = localVariables.newFieldInRecord("pnd_Lob", "#LOB", FieldType.STRING, 1);
        pnd_Int_Rate_N = localVariables.newFieldInRecord("pnd_Int_Rate_N", "#INT-RATE-N", FieldType.NUMERIC, 5, 2);
        pnd_Int_Rate_A = localVariables.newFieldInRecord("pnd_Int_Rate_A", "#INT-RATE-A", FieldType.STRING, 6);
        pnd_Year = localVariables.newFieldInRecord("pnd_Year", "#YEAR", FieldType.STRING, 4);
        pnd_Ph_Name = localVariables.newFieldInRecord("pnd_Ph_Name", "#PH-NAME", FieldType.STRING, 35);
        pnd_Ph_Last_Name = localVariables.newFieldInRecord("pnd_Ph_Last_Name", "#PH-LAST-NAME", FieldType.STRING, 16);
        pnd_Ph_First_Name = localVariables.newFieldInRecord("pnd_Ph_First_Name", "#PH-FIRST-NAME", FieldType.STRING, 10);
        pnd_Ph_Middle_Name = localVariables.newFieldInRecord("pnd_Ph_Middle_Name", "#PH-MIDDLE-NAME", FieldType.STRING, 12);
        pnd_Ph_Isn = localVariables.newFieldInRecord("pnd_Ph_Isn", "#PH-ISN", FieldType.NUMERIC, 10);
        pnd_Ph_Ssn = localVariables.newFieldInRecord("pnd_Ph_Ssn", "#PH-SSN", FieldType.NUMERIC, 9);
        pnd_Ph_Dob = localVariables.newFieldInRecord("pnd_Ph_Dob", "#PH-DOB", FieldType.NUMERIC, 8);
        pnd_Rep_Name = localVariables.newFieldInRecord("pnd_Rep_Name", "#REP-NAME", FieldType.STRING, 30);
        pnd_Rep_Ix = localVariables.newFieldInRecord("pnd_Rep_Ix", "#REP-IX", FieldType.INTEGER, 1);
        pnd_Rep_Cnt = localVariables.newFieldInRecord("pnd_Rep_Cnt", "#REP-CNT", FieldType.INTEGER, 1);
        pnd_Rep_Key = localVariables.newFieldArrayInRecord("pnd_Rep_Key", "#REP-KEY", FieldType.STRING, 15, new DbsArrayController(1, 100));

        pnd_Rep_Key__R_Field_26 = localVariables.newGroupInRecord("pnd_Rep_Key__R_Field_26", "REDEFINE", pnd_Rep_Key);

        pnd_Rep_Key_Pnd_Rep_Key_Redef = pnd_Rep_Key__R_Field_26.newGroupArrayInGroup("pnd_Rep_Key_Pnd_Rep_Key_Redef", "#REP-KEY-REDEF", new DbsArrayController(1, 
            100));
        pnd_Rep_Key_Pnd_Rep_Unit = pnd_Rep_Key_Pnd_Rep_Key_Redef.newFieldInGroup("pnd_Rep_Key_Pnd_Rep_Unit", "#REP-UNIT", FieldType.STRING, 8);
        pnd_Rep_Key__Filler13 = pnd_Rep_Key_Pnd_Rep_Key_Redef.newFieldInGroup("pnd_Rep_Key__Filler13", "_FILLER13", FieldType.STRING, 7);
        pnd_Rep_Arr = localVariables.newFieldArrayInRecord("pnd_Rep_Arr", "#REP-ARR", FieldType.STRING, 25, new DbsArrayController(1, 100));

        pnd_Rep_Arr__R_Field_27 = localVariables.newGroupInRecord("pnd_Rep_Arr__R_Field_27", "REDEFINE", pnd_Rep_Arr);

        pnd_Rep_Arr_Pnd_Rep_Arr_Redef = pnd_Rep_Arr__R_Field_27.newGroupArrayInGroup("pnd_Rep_Arr_Pnd_Rep_Arr_Redef", "#REP-ARR-REDEF", new DbsArrayController(1, 
            100));
        pnd_Rep_Arr_Pnd_Rep_Act_Ind = pnd_Rep_Arr_Pnd_Rep_Arr_Redef.newFieldInGroup("pnd_Rep_Arr_Pnd_Rep_Act_Ind", "#REP-ACT-IND", FieldType.STRING, 1);
        pnd_Rep_Arr_Pnd_Rep_Id = pnd_Rep_Arr_Pnd_Rep_Arr_Redef.newFieldInGroup("pnd_Rep_Arr_Pnd_Rep_Id", "#REP-ID", FieldType.STRING, 8);
        pnd_Rep_Arr_Pnd_Rep_Lob = pnd_Rep_Arr_Pnd_Rep_Arr_Redef.newFieldInGroup("pnd_Rep_Arr_Pnd_Rep_Lob", "#REP-LOB", FieldType.STRING, 1);
        pnd_Rep_Arr_Pnd_Rep_Case_Cnt = pnd_Rep_Arr_Pnd_Rep_Arr_Redef.newFieldInGroup("pnd_Rep_Arr_Pnd_Rep_Case_Cnt", "#REP-CASE-CNT", FieldType.NUMERIC, 
            7);
        pnd_Rep_Arr_Pnd_Rep_Letter_Cnt = pnd_Rep_Arr_Pnd_Rep_Arr_Redef.newFieldInGroup("pnd_Rep_Arr_Pnd_Rep_Letter_Cnt", "#REP-LETTER-CNT", FieldType.NUMERIC, 
            7);
        pnd_Rep_Arr__Filler14 = pnd_Rep_Arr_Pnd_Rep_Arr_Redef.newFieldInGroup("pnd_Rep_Arr__Filler14", "_FILLER14", FieldType.STRING, 1);
        pnd_Str_Arr = localVariables.newFieldArrayInRecord("pnd_Str_Arr", "#STR-ARR", FieldType.STRING, 35, new DbsArrayController(1, 50));

        pnd_Status_Arr = localVariables.newGroupInRecord("pnd_Status_Arr", "#STATUS-ARR");
        pnd_Status_Arr_Pnd_Status_Cnt = pnd_Status_Arr.newFieldInGroup("pnd_Status_Arr_Pnd_Status_Cnt", "#STATUS-CNT", FieldType.INTEGER, 1);
        pnd_Status_Arr_Pnd_Status = pnd_Status_Arr.newFieldArrayInGroup("pnd_Status_Arr_Pnd_Status", "#STATUS", FieldType.STRING, 4, new DbsArrayController(1, 
            50));
        pnd_Status_Arr_Pnd_Stage = pnd_Status_Arr.newFieldArrayInGroup("pnd_Status_Arr_Pnd_Stage", "#STAGE", FieldType.STRING, 1, new DbsArrayController(1, 
            50));
        pnd_Status_Arr_Pnd_Advoc = pnd_Status_Arr.newFieldArrayInGroup("pnd_Status_Arr_Pnd_Advoc", "#ADVOC", FieldType.STRING, 1, new DbsArrayController(1, 
            50));
        pnd_Status_Arr_Pnd_Status_Ix = pnd_Status_Arr.newFieldInGroup("pnd_Status_Arr_Pnd_Status_Ix", "#STATUS-IX", FieldType.INTEGER, 1);
        pnd_Payments_Status = localVariables.newFieldArrayInRecord("pnd_Payments_Status", "#PAYMENTS-STATUS", FieldType.STRING, 4, new DbsArrayController(1, 
            11));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Sb_Ph.reset();
        vw_master_1.reset();
        vw_cwf_Sb_Work_Req.reset();
        vw_cwf_Sb_Bene.reset();
        vw_cwf_Support_Tbl.reset();

        localVariables.reset();
        pnd_Ph_Key.setInitialValue("P");
        pnd_Srvvr_Key.setInitialValue("S");
        pnd_Todays_Date_D.setInitialValue(Global.getDATX());
        pnd_Payments_Status.getValue(1).setInitialValue("4901");
        pnd_Payments_Status.getValue(2).setInitialValue("4902");
        pnd_Payments_Status.getValue(3).setInitialValue("4903");
        pnd_Payments_Status.getValue(4).setInitialValue("4904");
        pnd_Payments_Status.getValue(5).setInitialValue("4905");
        pnd_Payments_Status.getValue(6).setInitialValue("4906");
        pnd_Payments_Status.getValue(7).setInitialValue("4907");
        pnd_Payments_Status.getValue(8).setInitialValue("4909");
        pnd_Payments_Status.getValue(9).setInitialValue("4941");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Swfb4420() throws Exception
    {
        super("Swfb4420");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        getReports().definePrinter(2, "PRT1");                                                                                                                            //Natural: DEFINE PRINTER ( PRT1 = 1 )
        getReports().definePrinter(3, "PRT2");                                                                                                                            //Natural: DEFINE PRINTER ( PRT2 = 2 )
        //*                                                                                                                                                               //Natural: FORMAT ( PRT1 ) LS = 133 SG = OFF PS = 56;//Natural: FORMAT ( PRT2 ) LS = 133 SG = OFF PS = 56
        //*  IF *DATA GT 0
        //*   INPUT #TODAYS-DATE-A8
        //*   MOVE EDITED #TODAYS-DATE-A8 TO #TODAYS-DATE-D (EM=YYYYMMDD)
        //*  END-IF
        pnd_Todays_Date_A8.setValueEdited(pnd_Todays_Date_D,new ReportEditMask("YYYYMMDD"));                                                                              //Natural: MOVE EDITED #TODAYS-DATE-D ( EM = YYYYMMDD ) TO #TODAYS-DATE-A8
        //* *---  READ FORMS REQUEST STATUS CODES
        pnd_Tbl_Key_Tbl_Scrty_Level_Ind.setValue("S");                                                                                                                    //Natural: ASSIGN #TBL-KEY.TBL-SCRTY-LEVEL-IND := 'S'
        pnd_Tbl_Key_Tbl_Table_Nme.setValue("CWF-SB-CORRESP-CNTRL");                                                                                                       //Natural: ASSIGN #TBL-KEY.TBL-TABLE-NME := 'CWF-SB-CORRESP-CNTRL'
        pnd_Tbl_Key_Tbl_Key_Field.setValue("FORMS STATUS");                                                                                                               //Natural: ASSIGN #TBL-KEY.TBL-KEY-FIELD := 'FORMS STATUS'
        vw_cwf_Support_Tbl.startDatabaseRead                                                                                                                              //Natural: READ CWF-SUPPORT-TBL WITH TBL-PRIME-KEY EQ #TBL-KEY
        (
        "READ01",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_cwf_Support_Tbl.readNextRow("READ01")))
        {
            if (condition(cwf_Support_Tbl_Tbl_Table_Nme.greater(pnd_Tbl_Key_Tbl_Table_Nme)))                                                                              //Natural: IF CWF-SUPPORT-TBL.TBL-TABLE-NME GT #TBL-KEY.TBL-TABLE-NME
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(! (DbsUtil.maskMatches(cwf_Support_Tbl_Tbl_Key_Field,"'FORMS STATUS'"))))                                                                       //Natural: IF NOT CWF-SUPPORT-TBL.TBL-KEY-FIELD EQ MASK ( 'FORMS STATUS' )
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            FOR01:                                                                                                                                                        //Natural: FOR #J EQ 1 TO 28
            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(28)); pnd_J.nadd(1))
            {
                if (condition(cwf_Support_Tbl_Stats.getValue(pnd_J).equals(" ")))                                                                                         //Natural: IF CWF-SUPPORT-TBL.STATS ( #J ) EQ ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                pnd_Status_Arr_Pnd_Status_Cnt.nadd(1);                                                                                                                    //Natural: ADD 1 TO #STATUS-CNT
                pnd_Status_Arr_Pnd_Status.getValue(pnd_Status_Arr_Pnd_Status_Cnt).setValue(cwf_Support_Tbl_Stats.getValue(pnd_J));                                        //Natural: ASSIGN #STATUS ( #STATUS-CNT ) := CWF-SUPPORT-TBL.STATS ( #J )
                pnd_Status_Arr_Pnd_Stage.getValue(pnd_Status_Arr_Pnd_Status_Cnt).setValue(cwf_Support_Tbl_Stage.getValue(pnd_J));                                         //Natural: ASSIGN #STAGE ( #STATUS-CNT ) := CWF-SUPPORT-TBL.STAGE ( #J )
                pnd_Status_Arr_Pnd_Advoc.getValue(pnd_Status_Arr_Pnd_Status_Cnt).setValue(cwf_Support_Tbl_Advoc.getValue(pnd_J));                                         //Natural: ASSIGN #ADVOC ( #STATUS-CNT ) := CWF-SUPPORT-TBL.ADVOC ( #J )
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* *---  FORCE THESE STATUSES THAT ARE NOT ON THE TABLE
        pnd_Status_Arr_Pnd_Status_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #STATUS-CNT
        pnd_Status_Arr_Pnd_Status.getValue(pnd_Status_Arr_Pnd_Status_Cnt).setValue("4500");                                                                               //Natural: ASSIGN #STATUS ( #STATUS-CNT ) := '4500'
        pnd_Status_Arr_Pnd_Stage.getValue(pnd_Status_Arr_Pnd_Status_Cnt).setValue("2");                                                                                   //Natural: ASSIGN #STAGE ( #STATUS-CNT ) := '2'
        pnd_Status_Arr_Pnd_Status_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #STATUS-CNT
        pnd_Status_Arr_Pnd_Status.getValue(pnd_Status_Arr_Pnd_Status_Cnt).setValue("4590");                                                                               //Natural: ASSIGN #STATUS ( #STATUS-CNT ) := '4590'
        pnd_Status_Arr_Pnd_Stage.getValue(pnd_Status_Arr_Pnd_Status_Cnt).setValue("2");                                                                                   //Natural: ASSIGN #STAGE ( #STATUS-CNT ) := '2'
        pnd_Status_Arr_Pnd_Status_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #STATUS-CNT
        pnd_Status_Arr_Pnd_Status.getValue(pnd_Status_Arr_Pnd_Status_Cnt).setValue("4591");                                                                               //Natural: ASSIGN #STATUS ( #STATUS-CNT ) := '4591'
        pnd_Status_Arr_Pnd_Stage.getValue(pnd_Status_Arr_Pnd_Status_Cnt).setValue("2");                                                                                   //Natural: ASSIGN #STAGE ( #STATUS-CNT ) := '2'
        //*  AG
                                                                                                                                                                          //Natural: PERFORM #FILL-STATUS-TABLE
        sub_Pnd_Fill_Status_Table();
        if (condition(Global.isEscape())) {return;}
        //* *---  READ LETTER CONTROL RECORDS
        pnd_Tbl_Key_Tbl_Scrty_Level_Ind.setValue("S");                                                                                                                    //Natural: ASSIGN #TBL-KEY.TBL-SCRTY-LEVEL-IND := 'S'
        pnd_Tbl_Key_Tbl_Table_Nme.setValue("CWF-SB-LETTER-CNTRL");                                                                                                        //Natural: ASSIGN #TBL-KEY.TBL-TABLE-NME := 'CWF-SB-LETTER-CNTRL'
        pnd_Tbl_Key_Tbl_Key_Field.reset();                                                                                                                                //Natural: RESET #TBL-KEY.TBL-KEY-FIELD
        vw_cwf_Support_Tbl.startDatabaseRead                                                                                                                              //Natural: READ CWF-SUPPORT-TBL BY TBL-PRIME-KEY EQ #TBL-KEY
        (
        "READ02",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
        );
        READ02:
        while (condition(vw_cwf_Support_Tbl.readNextRow("READ02")))
        {
            if (condition(cwf_Support_Tbl_Tbl_Table_Nme.greater(pnd_Tbl_Key_Tbl_Table_Nme)))                                                                              //Natural: IF CWF-SUPPORT-TBL.TBL-TABLE-NME GT #TBL-KEY.TBL-TABLE-NME
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet585 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CWF-SUPPORT-TBL.TBL-KEY-FIELD EQ MASK ( 'CONTROL MD' )
            if (condition(DbsUtil.maskMatches(cwf_Support_Tbl_Tbl_Key_Field,"'CONTROL MD'")))
            {
                decideConditionsMet585++;
                pnd_Md_Control_Rec.setValue(cwf_Support_Tbl_Tbl_Data_Field);                                                                                              //Natural: ASSIGN #MD-CONTROL-REC := CWF-SUPPORT-TBL.TBL-DATA-FIELD
            }                                                                                                                                                             //Natural: WHEN CWF-SUPPORT-TBL.TBL-KEY-FIELD EQ MASK ( 'CONTROL 90' )
            else if (condition(DbsUtil.maskMatches(cwf_Support_Tbl_Tbl_Key_Field,"'CONTROL 90'")))
            {
                decideConditionsMet585++;
                pnd_90d_Control_Rec.setValue(cwf_Support_Tbl_Tbl_Data_Field);                                                                                             //Natural: ASSIGN #90D-CONTROL-REC := CWF-SUPPORT-TBL.TBL-DATA-FIELD
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Md_Control_Rec.equals(" ")))                                                                                                                    //Natural: IF #MD-CONTROL-REC EQ ' '
        {
            pnd_Err_Msg.setValue("MD CONTROL RECORD IS MISSING FROM THE TABLE.  PROCESS TERMINATED.");                                                                    //Natural: ASSIGN #ERR-MSG := 'MD CONTROL RECORD IS MISSING FROM THE TABLE.  PROCESS TERMINATED.'
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REPORT
            sub_Write_Error_Report();
            if (condition(Global.isEscape())) {return;}
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //* *---   DETERMINE IF IT IS MD PERIOD
        pnd_Control_Rec.setValue(pnd_Md_Control_Rec);                                                                                                                     //Natural: ASSIGN #CONTROL-REC := #MD-CONTROL-REC
        pnd_Md_Start_A8.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Todays_Date_A8_Pnd_Todays_Yyyy, pnd_Control_Rec_Pnd_Md_Start_Mmdd));                 //Natural: COMPRESS #TODAYS-YYYY #MD-START-MMDD INTO #MD-START-A8 LEAVING NO
        pnd_Md_End_A8.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Todays_Date_A8_Pnd_Todays_Yyyy, pnd_Control_Rec_Pnd_Md_End_Mmdd));                     //Natural: COMPRESS #TODAYS-YYYY #MD-END-MMDD INTO #MD-END-A8 LEAVING NO
        if (condition(pnd_Todays_Date_A8.less(pnd_Md_Start_A8) || pnd_Todays_Date_A8.greater(pnd_Md_End_A8)))                                                             //Natural: IF #TODAYS-DATE-A8 LT #MD-START-A8 OR #TODAYS-DATE-A8 GT #MD-END-A8
        {
            pnd_Report_Title.setValue("SURVIVOR BENEFITS - 90 DAY FOLLOW-UP LETTERS");                                                                                    //Natural: ASSIGN #REPORT-TITLE := 'SURVIVOR BENEFITS - 90 DAY FOLLOW-UP LETTERS'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Md.setValue(true);                                                                                                                                        //Natural: ASSIGN #MD := TRUE
            pnd_Report_Title.setValue("SURVIVOR BENEFITS - MIN DISTRIBUTION/90 DAY FOLLOW-UP LETTERS");                                                                   //Natural: ASSIGN #REPORT-TITLE := 'SURVIVOR BENEFITS - MIN DISTRIBUTION/90 DAY FOLLOW-UP LETTERS'
            //*  CONTINUE FROM THE 30D COUNT
            if (condition(pnd_Control_Rec_Pnd_Last_Run_Date.equals(pnd_Todays_Date_A8)))                                                                                  //Natural: IF #LAST-RUN-DATE EQ #TODAYS-DATE-A8
            {
                pnd_Starting_Pin_Cnt.setValue(pnd_Control_Rec_Pnd_Printed_Pin_Cnt);                                                                                       //Natural: ASSIGN #STARTING-PIN-CNT := #PRINTED-PIN-CNT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* *---  IF NOT MD PERIOD THEN USE THE FOLLOW-UP LETTER CONTROL RECORD
        if (condition(! (pnd_Md.getBoolean())))                                                                                                                           //Natural: IF NOT #MD
        {
            if (condition(pnd_90d_Control_Rec.equals(" ")))                                                                                                               //Natural: IF #90D-CONTROL-REC EQ ' '
            {
                pnd_Err_Msg.setValue("CONTROL RECORD IS MISSING FROM THE TABLE.  PROCESS TERMINATED.");                                                                   //Natural: ASSIGN #ERR-MSG := 'CONTROL RECORD IS MISSING FROM THE TABLE.  PROCESS TERMINATED.'
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REPORT
                sub_Write_Error_Report();
                if (condition(Global.isEscape())) {return;}
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Control_Rec.setValue(pnd_90d_Control_Rec);                                                                                                                //Natural: ASSIGN #CONTROL-REC := #90D-CONTROL-REC
        }                                                                                                                                                                 //Natural: END-IF
        //*  #MAX-PINS := 01
        //*  #LAST-PIN := 3811163
        //* *---  GET PREVIOUS YEAR
        pnd_Last_Years_Yyyy.compute(new ComputeParameters(false, pnd_Last_Years_Yyyy), pnd_Todays_Date_A8_Pnd_Todays_Yyyy_N.subtract(1));                                 //Natural: ASSIGN #LAST-YEARS-YYYY := #TODAYS-YYYY-N - 1
        //* *---  GET REPS FROM THE TABLE
        pnd_Tbl_Key_Tbl_Scrty_Level_Ind.setValue("S");                                                                                                                    //Natural: ASSIGN #TBL-KEY.TBL-SCRTY-LEVEL-IND := 'S'
        pnd_Tbl_Key_Tbl_Table_Nme.setValue("CWF-SB-LETTER-REPS");                                                                                                         //Natural: ASSIGN #TBL-KEY.TBL-TABLE-NME := 'CWF-SB-LETTER-REPS'
        pnd_Tbl_Key_Tbl_Key_Field.reset();                                                                                                                                //Natural: RESET #TBL-KEY.TBL-KEY-FIELD
        vw_cwf_Support_Tbl.startDatabaseRead                                                                                                                              //Natural: READ CWF-SUPPORT-TBL WITH TBL-PRIME-KEY EQ #TBL-KEY
        (
        "READ03",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
        );
        READ03:
        while (condition(vw_cwf_Support_Tbl.readNextRow("READ03")))
        {
            if (condition(cwf_Support_Tbl_Tbl_Table_Nme.greater(pnd_Tbl_Key_Tbl_Table_Nme)))                                                                              //Natural: IF CWF-SUPPORT-TBL.TBL-TABLE-NME GT #TBL-KEY.TBL-TABLE-NME
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            FOR02:                                                                                                                                                        //Natural: FOR #I EQ 1 TO 10
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(10)); pnd_I.nadd(1))
            {
                if (condition(cwf_Support_Tbl_Tbl_Reps.getValue(pnd_I).greater(" ")))                                                                                     //Natural: IF CWF-SUPPORT-TBL.TBL-REPS ( #I ) GT ' '
                {
                    pnd_Rep_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #REP-CNT
                    pnd_Rep_Key_Pnd_Rep_Unit.getValue(pnd_Rep_Cnt).setValue(cwf_Support_Tbl_Tbl_Key_Field);                                                               //Natural: ASSIGN #REP-UNIT ( #REP-CNT ) := CWF-SUPPORT-TBL.TBL-KEY-FIELD
                    pnd_Rep_Arr.getValue(pnd_Rep_Cnt).setValue(cwf_Support_Tbl_Tbl_Reps.getValue(pnd_I));                                                                 //Natural: ASSIGN #REP-ARR ( #REP-CNT ) := CWF-SUPPORT-TBL.TBL-REPS ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Rep_Cnt.equals(getZero())))                                                                                                                     //Natural: IF #REP-CNT EQ 0
        {
            pnd_Err_Msg.setValue("REPRESENTATIVE TABLE IS MISSING.  PROCESS TERMINATED.");                                                                                //Natural: ASSIGN #ERR-MSG := 'REPRESENTATIVE TABLE IS MISSING.  PROCESS TERMINATED.'
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REPORT
            sub_Write_Error_Report();
            if (condition(Global.isEscape())) {return;}
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pnd_Rep_Arr_Pnd_Rep_Act_Ind.getValue("*").equals("A"))))                                                                                         //Natural: IF NOT ( #REP-ACT-IND ( * ) EQ 'A' )
        {
            pnd_Err_Msg.setValue("NO ACTIVE REP FOUND ON TABLE.  PROCESS TERMINATED.");                                                                                   //Natural: ASSIGN #ERR-MSG := 'NO ACTIVE REP FOUND ON TABLE.  PROCESS TERMINATED.'
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REPORT
            sub_Write_Error_Report();
            if (condition(Global.isEscape())) {return;}
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //* *---  GET THE PREVAILING INTEREST RATE
        if (condition(! (pnd_Md.getBoolean())))                                                                                                                           //Natural: IF NOT #MD
        {
            pdaAnta002.getPnd_Ls_Pamp_I_Info_Pnd_Ls_Curr_Bus_Date().setValue(pnd_Todays_Date_A8_Pnd_Todays_Date_N8);                                                      //Natural: ASSIGN #LS-CURR-BUS-DATE := #TODAYS-DATE-N8
            DbsUtil.callnat(Antn002.class , getCurrentProcessState(), pdaAnta002.getPnd_Ls_Pamp_I_Info());                                                                //Natural: CALLNAT 'ANTN002' #LS-P&I-INFO
            if (condition(Global.isEscape())) return;
            if (condition(pdaAnta002.getPnd_Ls_Pamp_I_Info_Pnd_Ls_Ret_Code().notEquals(getZero())))                                                                       //Natural: IF #LS-RET-CODE NE 0
            {
                pnd_Err_Msg.setValue("UNSUCCESSFUL READING INTEREST RATE TABLE.  PROCESS TERMINATED.");                                                                   //Natural: ASSIGN #ERR-MSG := 'UNSUCCESSFUL READING INTEREST RATE TABLE.  PROCESS TERMINATED.'
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REPORT
                sub_Write_Error_Report();
                if (condition(Global.isEscape())) {return;}
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Int_Rate_N.compute(new ComputeParameters(false, pnd_Int_Rate_N), pdaAnta002.getPnd_Ls_Pamp_I_Info_Pnd_Ls_Pamp_I_Tiaa_Rate().multiply(100));               //Natural: ASSIGN #INT-RATE-N := #LS-P&I-TIAA-RATE * 100
            pnd_Int_Rate_A.setValueEdited(pnd_Int_Rate_N,new ReportEditMask("ZZ9.99"));                                                                                   //Natural: MOVE EDITED #INT-RATE-N ( EM = ZZ9.99 ) TO #INT-RATE-A
            pnd_Year.setValueEdited(pnd_Todays_Date_D,new ReportEditMask("YYYY"));                                                                                        //Natural: MOVE EDITED #TODAYS-DATE-D ( EM = YYYY ) TO #YEAR
            //*   START FROM NEXT PIN
        }                                                                                                                                                                 //Natural: END-IF
        //* *---  MAIN READ  ---------------------------------------------------
        pnd_Ph_Key_Srvvr_Mit_Idntfr.compute(new ComputeParameters(false, pnd_Ph_Key_Srvvr_Mit_Idntfr), pnd_Control_Rec_Pnd_Last_Pin.add(1));                              //Natural: ASSIGN #PH-KEY.SRVVR-MIT-IDNTFR := #LAST-PIN + 1
        vw_cwf_Sb_Ph.startDatabaseRead                                                                                                                                    //Natural: READ CWF-SB-PH BY PH-KEY EQ #PH-KEY
        (
        "READ04",
        new Wc[] { new Wc("PH_KEY", ">=", pnd_Ph_Key, WcType.BY) },
        new Oc[] { new Oc("PH_KEY", "ASC") }
        );
        READ04:
        while (condition(vw_cwf_Sb_Ph.readNextRow("READ04")))
        {
            if (condition(cwf_Sb_Ph_Rcrd_Type.greater(pnd_Ph_Key_Rcrd_Type)))                                                                                             //Natural: IF CWF-SB-PH.RCRD-TYPE GT #PH-KEY.RCRD-TYPE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Control_Rec_Pnd_Max_Pins.equals(getZero()) || pnd_Total_Pins.add(pnd_Starting_Pin_Cnt).greaterOrEqual(pnd_Control_Rec_Pnd_Max_Pins)))       //Natural: IF #MAX-PINS EQ 0 OR #TOTAL-PINS + #STARTING-PIN-CNT GE #MAX-PINS
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //* *---  SAVE THE LAST PIN PROCESSED
            pnd_Control_Rec_Pnd_Last_Pin.setValue(cwf_Sb_Ph_Ph_Pin_Nbr);                                                                                                  //Natural: ASSIGN #LAST-PIN := CWF-SB-PH.PH-PIN-NBR
            //* *---  READ MIT REQUESTS
            pnd_Pin_History_Key_Pin_Nbr.setValue(cwf_Sb_Ph_Ph_Pin_Nbr);                                                                                                   //Natural: ASSIGN #PIN-HISTORY-KEY.PIN-NBR := CWF-SB-PH.PH-PIN-NBR
            pnd_Pin_History_Key_Crprte_Status_Ind.setValue("0");                                                                                                          //Natural: ASSIGN #PIN-HISTORY-KEY.CRPRTE-STATUS-IND := '0'
            vw_master_1.startDatabaseRead                                                                                                                                 //Natural: READ MASTER-1 BY PIN-HISTORY-KEY EQ #PIN-HISTORY-KEY
            (
            "READ05",
            new Wc[] { new Wc("PIN_HISTORY_KEY", ">=", pnd_Pin_History_Key, WcType.BY) },
            new Oc[] { new Oc("PIN_HISTORY_KEY", "ASC") }
            );
            READ05:
            while (condition(vw_master_1.readNextRow("READ05")))
            {
                if (condition(master_1_Crprte_Status_Ind.greater("0") || master_1_Pin_Nbr.greater(pnd_Pin_History_Key_Pin_Nbr)))                                          //Natural: IF MASTER-1.CRPRTE-STATUS-IND GT '0' OR MASTER-1.PIN-NBR GT #PIN-HISTORY-KEY.PIN-NBR
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                //*                                                   /* AG BEGIN
                //* *---  PROCESS DA ONLY OR IA ONLY OR BOTH IA AND DA
                //* *  IF  (#IA-DA EQ 'DA' AND MASTER-1.WORK-PRCSS-ID EQ MASK(..'I'.)) OR
                //* *      (#IA-DA EQ 'IA' AND MASTER-1.WORK-PRCSS-ID NE MASK(..'I'.))
                //* *    ESCAPE TOP
                //* *  END-IF
                //*                                                     /*
                //* *---  PROCESS FORMS REQUESTS ONLY
                //*    IF  MASTER-1.WORK-PRCSS-ID NE MASK('FA'.'D') OR
                //*        MASTER-1.WORK-PRCSS-ID EQ MASK(....'MM')
                //*      ESCAPE TOP
                //*    END-IF
                if (condition(master_1_Work_Prcss_Id.equals("FAIDF ") || master_1_Work_Prcss_Id.equals("FAIDS ") || master_1_Work_Prcss_Id.equals("FAID ")                //Natural: IF MASTER-1.WORK-PRCSS-ID EQ 'FAIDF ' OR MASTER-1.WORK-PRCSS-ID EQ 'FAIDS ' OR MASTER-1.WORK-PRCSS-ID EQ 'FAID  ' OR MASTER-1.WORK-PRCSS-ID EQ 'FA DD ' OR MASTER-1.WORK-PRCSS-ID EQ 'FA DMM' OR MASTER-1.WORK-PRCSS-ID EQ 'FA DDM' OR MASTER-1.WORK-PRCSS-ID EQ 'FA DDR' OR MASTER-1.WORK-PRCSS-ID EQ MASK ( 'TAID' ) OR MASTER-1.WORK-PRCSS-ID EQ MASK ( 'TA'.'D' )
                    || master_1_Work_Prcss_Id.equals("FA DD ") || master_1_Work_Prcss_Id.equals("FA DMM") || master_1_Work_Prcss_Id.equals("FA DDM") || 
                    master_1_Work_Prcss_Id.equals("FA DDR") || DbsUtil.maskMatches(master_1_Work_Prcss_Id,"'TAID'") || DbsUtil.maskMatches(master_1_Work_Prcss_Id,
                    "'TA'.'D'")))
                {
                    //*         MASTER-1.WORK-PRCSS-ID EQ 'RA DMD' OR
                    //*         MASTER-1.WORK-PRCSS-ID EQ 'RAIDMD' OR
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                    //*  AG END
                }                                                                                                                                                         //Natural: END-IF
                //* ---  PROCESS EXTERNALLY PENDED, NON-ADVOCATE REQUESTS ONLY
                //*  SPLAY #STATUS(*)
                DbsUtil.examine(new ExamineSource(pnd_Status_Arr_Pnd_Status.getValue("*")), new ExamineSearch(master_1_Status_Cde), new ExamineGivingIndex(pnd_Status_Arr_Pnd_Status_Ix)); //Natural: EXAMINE #STATUS ( * ) FOR MASTER-1.STATUS-CDE INDEX #STATUS-IX
                //*  IF NOT EXT. PENDED REQUEST
                if (condition(pnd_Status_Arr_Pnd_Status_Ix.equals(getZero())))                                                                                            //Natural: IF #STATUS-IX EQ 0
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*  IF ADVOCATE
                if (condition(pnd_Status_Arr_Pnd_Advoc.getValue(pnd_Status_Arr_Pnd_Status_Ix).equals("A")))                                                               //Natural: IF #ADVOC ( #STATUS-IX ) EQ 'A'
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                    //*  DA
                }                                                                                                                                                         //Natural: END-IF
                //* *  IF MASTER-1.WORK-PRCSS-ID EQ MASK(..'I'.)
                //* *    #LOB := 'I'
                //* *  ELSE
                pnd_Lob.setValue("D");                                                                                                                                    //Natural: ASSIGN #LOB := 'D'
                //* *  END-IF
                //* *---  CHECK IF THE REQUEST IS LINKED TO THE SURVIVOR
                vw_cwf_Sb_Work_Req.startDatabaseFind                                                                                                                      //Natural: FIND CWF-SB-WORK-REQ WITH RQST-LOG-DTE-TME EQ MASTER-1.RQST-LOG-DTE-TME
                (
                "F1",
                new Wc[] { new Wc("RQST_LOG_DTE_TME", "=", master_1_Rqst_Log_Dte_Tme, WcType.WITH) }
                );
                F1:
                while (condition(vw_cwf_Sb_Work_Req.readNextRow("F1")))
                {
                    vw_cwf_Sb_Work_Req.setIfNotFoundControlFlag(false);
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(vw_cwf_Sb_Work_Req.getAstNUMBER().equals(getZero())))                                                                                       //Natural: IF *NUMBER ( F1. ) EQ 0
                {
                    pnd_Err_Msg.setValue("Request is not linked to a survivor.");                                                                                         //Natural: ASSIGN #ERR-MSG := 'Request is not linked to a survivor.'
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REPORT
                    sub_Write_Error_Report();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //* *---  CHECK THAT THE CASE WAS VERIFIED
                if (condition(! (cwf_Sb_Ph_Work_List_Ind.equals("P") || cwf_Sb_Ph_Work_List_Ind.equals("Q"))))                                                            //Natural: IF NOT ( CWF-SB-PH.WORK-LIST-IND EQ 'P' OR EQ 'Q' )
                {
                    pnd_Err_Msg.setValue("Case was not verified.");                                                                                                       //Natural: ASSIGN #ERR-MSG := 'Case was not verified.'
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REPORT
                    sub_Write_Error_Report();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //* *---  CHECK SURVIVOR DATA
                pnd_Srvvr_Key_Srvvr_Mit_Idntfr.setValue(cwf_Sb_Work_Req_Srvvr_Mit_Idntfr);                                                                                //Natural: ASSIGN #SRVVR-KEY.SRVVR-MIT-IDNTFR := CWF-SB-WORK-REQ.SRVVR-MIT-IDNTFR
                vw_cwf_Sb_Bene.startDatabaseFind                                                                                                                          //Natural: FIND CWF-SB-BENE WITH SRVVR-KEY EQ #SRVVR-KEY
                (
                "F2",
                new Wc[] { new Wc("SRVVR_KEY", "=", pnd_Srvvr_Key, WcType.WITH) }
                );
                F2:
                while (condition(vw_cwf_Sb_Bene.readNextRow("F2")))
                {
                    vw_cwf_Sb_Bene.setIfNotFoundControlFlag(false);
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(vw_cwf_Sb_Bene.getAstNUMBER().equals(getZero())))                                                                                           //Natural: IF *NUMBER ( F2. ) EQ 0
                {
                    pnd_Err_Msg.setValue("Survivor record is missing.");                                                                                                  //Natural: ASSIGN #ERR-MSG := 'Survivor record is missing.'
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REPORT
                    sub_Write_Error_Report();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //* *---  GET THE LAST MD DATE
                pnd_Md_Dte.reset();                                                                                                                                       //Natural: RESET #MD-DTE #MD-DTE-C-OR-F
                pnd_Md_Dte_C_Or_F.reset();
                short decideConditionsMet772 = 0;                                                                                                                         //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN CWF-SB-WORK-REQ.MD-MDS-CRTE-DTE GT #MD-DTE
                if (condition(cwf_Sb_Work_Req_Md_Mds_Crte_Dte.greater(pnd_Md_Dte)))
                {
                    decideConditionsMet772++;
                    pnd_Md_Dte.setValue(cwf_Sb_Work_Req_Md_Mds_Crte_Dte);                                                                                                 //Natural: ASSIGN #MD-DTE := CWF-SB-WORK-REQ.MD-MDS-CRTE-DTE
                }                                                                                                                                                         //Natural: WHEN CWF-SB-WORK-REQ.MD-MD1-CRTE-DTE GT #MD-DTE
                if (condition(cwf_Sb_Work_Req_Md_Md1_Crte_Dte.greater(pnd_Md_Dte)))
                {
                    decideConditionsMet772++;
                    pnd_Md_Dte.setValue(cwf_Sb_Work_Req_Md_Md1_Crte_Dte);                                                                                                 //Natural: ASSIGN #MD-DTE := CWF-SB-WORK-REQ.MD-MD1-CRTE-DTE
                }                                                                                                                                                         //Natural: WHEN CWF-SB-WORK-REQ.MD-MD2-CRTE-DTE GT #MD-DTE
                if (condition(cwf_Sb_Work_Req_Md_Md2_Crte_Dte.greater(pnd_Md_Dte)))
                {
                    decideConditionsMet772++;
                    pnd_Md_Dte.setValue(cwf_Sb_Work_Req_Md_Md2_Crte_Dte);                                                                                                 //Natural: ASSIGN #MD-DTE := CWF-SB-WORK-REQ.MD-MD2-CRTE-DTE
                }                                                                                                                                                         //Natural: WHEN ANY
                if (condition(decideConditionsMet772 > 0))
                {
                    //*   THE MD WAS FOR THE CORRESP
                    if (condition(pnd_Md_Dte.less(cwf_Sb_Work_Req_Forms_Mail_Dte)))                                                                                       //Natural: IF #MD-DTE LT CWF-SB-WORK-REQ.FORMS-MAIL-DTE
                    {
                        pnd_Md_Dte_C_Or_F.setValue("C");                                                                                                                  //Natural: ASSIGN #MD-DTE-C-OR-F := 'C'
                        //*   THE MD WAS FOR THE FORMS STAGE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Md_Dte_C_Or_F.setValue("F");                                                                                                                  //Natural: ASSIGN #MD-DTE-C-OR-F := 'F'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN NONE
                if (condition(decideConditionsMet772 == 0))
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                //* *---
                pnd_Last_Date_A8_Pnd_Last_Date_N8.reset();                                                                                                                //Natural: RESET #LAST-DATE-N8
                //* ---  CORRESPONDENCE STAGE
                if (condition(pnd_Status_Arr_Pnd_Stage.getValue(pnd_Status_Arr_Pnd_Status_Ix).equals("1")))                                                               //Natural: IF #STAGE ( #STATUS-IX ) EQ '1'
                {
                    if (condition(cwf_Sb_Work_Req_Corresp_30d_Crte_Dte.equals(getZero()) && pnd_Md_Dte.equals(getZero())))                                                //Natural: IF CWF-SB-WORK-REQ.CORRESP-30D-CRTE-DTE EQ 0 AND #MD-DTE EQ 0
                    {
                        //*   LET THE 30D PROGRAM TAKE CARE OF THIS
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    //* ---  GET THE DATE OF THE LAST CORRESP FOLLOW-UP LETTER
                    //*   USE THE MD DATE IF IT WAS
                    //*   AT THE CORRESP STAGE
                    if (condition(pnd_Md_Dte_C_Or_F.equals("C")))                                                                                                         //Natural: IF #MD-DTE-C-OR-F EQ 'C'
                    {
                        pnd_Last_Date_A8_Pnd_Last_Date_N8.setValue(pnd_Md_Dte);                                                                                           //Natural: ASSIGN #LAST-DATE-N8 := #MD-DTE
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(cwf_Sb_Work_Req_Corresp_30d_Crte_Dte.greater(pnd_Last_Date_A8_Pnd_Last_Date_N8)))                                                       //Natural: IF CWF-SB-WORK-REQ.CORRESP-30D-CRTE-DTE GT #LAST-DATE-N8
                    {
                        pnd_Last_Date_A8_Pnd_Last_Date_N8.setValue(cwf_Sb_Work_Req_Corresp_30d_Crte_Dte);                                                                 //Natural: ASSIGN #LAST-DATE-N8 := CWF-SB-WORK-REQ.CORRESP-30D-CRTE-DTE
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(cwf_Sb_Work_Req_Corresp_90d_Crte_Dte.greater(pnd_Last_Date_A8_Pnd_Last_Date_N8)))                                                       //Natural: IF CWF-SB-WORK-REQ.CORRESP-90D-CRTE-DTE GT #LAST-DATE-N8
                    {
                        pnd_Last_Date_A8_Pnd_Last_Date_N8.setValue(cwf_Sb_Work_Req_Corresp_90d_Crte_Dte);                                                                 //Natural: ASSIGN #LAST-DATE-N8 := CWF-SB-WORK-REQ.CORRESP-90D-CRTE-DTE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //* ---  IF THIS WENT THROUGH CORRESP STAGE AND THERE's no FORMS
                    //*      30D AND MD YET THEN THIS MUST BE HANDLED BY THE 30D PROGRAM
                    if (condition(cwf_Sb_Work_Req_Corresp_Crte_Dte.greater(getZero()) && cwf_Sb_Work_Req_Thirty_Day_Crte_Dte.equals(getZero()) && pnd_Md_Dte_C_Or_F.notEquals("F"))) //Natural: IF CWF-SB-WORK-REQ.CORRESP-CRTE-DTE GT 0 AND CWF-SB-WORK-REQ.THIRTY-DAY-CRTE-DTE EQ 0 AND #MD-DTE-C-OR-F NE 'F'
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    //*   USE ONLY THE MD DATE IF
                    //*   IT WAS AT THE FORMS STAGE
                    if (condition(pnd_Md_Dte_C_Or_F.equals("F")))                                                                                                         //Natural: IF #MD-DTE-C-OR-F EQ 'F'
                    {
                        pnd_Last_Date_A8_Pnd_Last_Date_N8.setValue(pnd_Md_Dte);                                                                                           //Natural: ASSIGN #LAST-DATE-N8 := #MD-DTE
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(cwf_Sb_Work_Req_Thirty_Day_Crte_Dte.greater(pnd_Last_Date_A8_Pnd_Last_Date_N8)))                                                        //Natural: IF CWF-SB-WORK-REQ.THIRTY-DAY-CRTE-DTE GT #LAST-DATE-N8
                    {
                        pnd_Last_Date_A8_Pnd_Last_Date_N8.setValue(cwf_Sb_Work_Req_Thirty_Day_Crte_Dte);                                                                  //Natural: ASSIGN #LAST-DATE-N8 := CWF-SB-WORK-REQ.THIRTY-DAY-CRTE-DTE
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(cwf_Sb_Work_Req_Ninety_Day_Crte_Dte.greater(pnd_Last_Date_A8_Pnd_Last_Date_N8)))                                                        //Natural: IF CWF-SB-WORK-REQ.NINETY-DAY-CRTE-DTE GT #LAST-DATE-N8
                    {
                        pnd_Last_Date_A8_Pnd_Last_Date_N8.setValue(cwf_Sb_Work_Req_Ninety_Day_Crte_Dte);                                                                  //Natural: ASSIGN #LAST-DATE-N8 := CWF-SB-WORK-REQ.NINETY-DAY-CRTE-DTE
                    }                                                                                                                                                     //Natural: END-IF
                    //* ---  ADD'l conditions for converted/non-converted cases
                    //*                                                                                                                                                   //Natural: DECIDE FOR FIRST CONDITION
                    short decideConditionsMet834 = 0;                                                                                                                     //Natural: WHEN MASTER-1.STATUS-CDE EQ '4500' AND ( CWF-SB-PH.SB-CONVERTED EQ 'M' OR EQ 'C' OR EQ '1' OR EQ '2' )
                    if (condition((master_1_Status_Cde.equals("4500") && (((cwf_Sb_Ph_Sb_Converted.equals("M") || cwf_Sb_Ph_Sb_Converted.equals("C")) 
                        || cwf_Sb_Ph_Sb_Converted.equals("1")) || cwf_Sb_Ph_Sb_Converted.equals("2")))))
                    {
                        decideConditionsMet834++;
                        ignore();
                    }                                                                                                                                                     //Natural: WHEN MASTER-1.STATUS-CDE EQ '4590' OR EQ '4591'
                    else if (condition(master_1_Status_Cde.equals("4590") || master_1_Status_Cde.equals("4591")))
                    {
                        decideConditionsMet834++;
                        if (condition(pnd_Last_Date_A8_Pnd_Last_Date_N8.equals(getZero())))                                                                               //Natural: IF #LAST-DATE-N8 EQ 0
                        {
                            pnd_Last_Date_A8.setValueEdited(master_1_Status_Updte_Dte_Tme,new ReportEditMask("YYYYMMDD"));                                                //Natural: MOVE EDITED MASTER-1.STATUS-UPDTE-DTE-TME ( EM = YYYYMMDD ) TO #LAST-DATE-A8
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        //*   IF THERE's no 30 or 90 day letter
                        if (condition(pnd_Last_Date_A8_Pnd_Last_Date_N8.equals(getZero())))                                                                               //Natural: IF #LAST-DATE-N8 EQ 0
                        {
                            if (condition(cwf_Sb_Work_Req_Forms_Mail_Dte.greater(getZero())))                                                                             //Natural: IF CWF-SB-WORK-REQ.FORMS-MAIL-DTE GT 0
                            {
                                pnd_Last_Date_A8_Pnd_Last_Date_N8.setValue(cwf_Sb_Work_Req_Forms_Mail_Dte);                                                               //Natural: ASSIGN #LAST-DATE-N8 := CWF-SB-WORK-REQ.FORMS-MAIL-DTE
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Last_Date_A8.setValueEdited(master_1_Status_Updte_Dte_Tme,new ReportEditMask("YYYYMMDD"));                                            //Natural: MOVE EDITED MASTER-1.STATUS-UPDTE-DTE-TME ( EM = YYYYMMDD ) TO #LAST-DATE-A8
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(pnd_Last_Date_A8.greater("19980201")))                                                                                          //Natural: IF #LAST-DATE-A8 GT '19980201'
                            {
                                if (condition(true)) continue;                                                                                                            //Natural: ESCAPE TOP
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-IF
                //* *---  CHECK IF 90 DAYS PAST LAST MAILING DATE
                pnd_Last_Date_D.reset();                                                                                                                                  //Natural: RESET #LAST-DATE-D
                if (condition(DbsUtil.maskMatches(pnd_Last_Date_A8,"YYYYMMDD")))                                                                                          //Natural: IF #LAST-DATE-A8 EQ MASK ( YYYYMMDD )
                {
                    pnd_Last_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Last_Date_A8);                                                                      //Natural: MOVE EDITED #LAST-DATE-A8 TO #LAST-DATE-D ( EM = YYYYMMDD )
                    //*         SUBTRACT 100 FROM #LAST-DATE-D
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Todays_Date_D.subtract(pnd_Last_Date_D).less(90)))                                                                                      //Natural: IF #TODAYS-DATE-D - #LAST-DATE-D LT 90
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //* *---  CHECK IF MD HAS NOT BEEN CREATED YET (FOR THIS YEAR)
                pnd_90d.setValue(false);                                                                                                                                  //Natural: ASSIGN #90D := FALSE
                //*   WE're doing a 90D instead of MD
                if (condition(pnd_Md.getBoolean() && pnd_Md_Dte_Pnd_Md_Dte_Yyyy.greaterOrEqual(pnd_Todays_Date_A8_Pnd_Todays_Yyyy)))                                      //Natural: IF #MD AND #MD-DTE-YYYY GE #TODAYS-YYYY
                {
                    pnd_90d.setValue(true);                                                                                                                               //Natural: ASSIGN #90D := TRUE
                }                                                                                                                                                         //Natural: END-IF
                //* *---  GET NAME OF SURVIVOR
                DbsUtil.callnat(Swfn4435.class , getCurrentProcessState(), cwf_Sb_Bene_Srvvr_Nme_Fr_Frm, cwf_Sb_Bene_Srvvr_Nme_Fr_Fr2, cwf_Sb_Bene_Last_Updte_Dte_Tme);   //Natural: CALLNAT 'SWFN4435' CWF-SB-BENE.SRVVR-NME-FR-FRM CWF-SB-BENE.SRVVR-NME-FR-FR2 CWF-SB-BENE.LAST-UPDTE-DTE-TME
                if (condition(Global.isEscape())) return;
                if (condition(cwf_Sb_Bene_Srvvr_Nme_Fr_Frm.equals(" ")))                                                                                                  //Natural: IF CWF-SB-BENE.SRVVR-NME-FR-FRM EQ ' '
                {
                    pnd_Err_Msg.setValue("Survivor name is not valid for correspondence.");                                                                               //Natural: ASSIGN #ERR-MSG := 'Survivor name is not valid for correspondence.'
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REPORT
                    sub_Write_Error_Report();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //* *---  CHECK IF ADDRESS IS KNOWN
                if (condition(cwf_Sb_Bene_Addr_Arr.getValue("*").equals("UNKNOWN") || ! (cwf_Sb_Bene_Addr_Arr.getValue("*").greater(" "))))                               //Natural: IF CWF-SB-BENE.ADDR-ARR ( * ) EQ 'UNKNOWN' OR NOT ( CWF-SB-BENE.ADDR-ARR ( * ) GT ' ' )
                {
                    pnd_Err_Msg.setValue("Survivor address is not known.");                                                                                               //Natural: ASSIGN #ERR-MSG := 'Survivor address is not known.'
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REPORT
                    sub_Write_Error_Report();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //* *---  ADD THE STATE AND ZIP INTO THE 6 LINES
                if (condition(cwf_Sb_Bene_State.greater(" ")))                                                                                                            //Natural: IF CWF-SB-BENE.STATE GT ' '
                {
                    cwf_Sb_Bene_Addr_All.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, cwf_Sb_Bene_Addr_All, ","));                                            //Natural: COMPRESS CWF-SB-BENE.ADDR-ALL ',' TO CWF-SB-BENE.ADDR-ALL LEAVING NO
                    cwf_Sb_Bene_Addr_All.setValue(DbsUtil.compress(cwf_Sb_Bene_Addr_All, cwf_Sb_Bene_State, cwf_Sb_Bene_Zip_Cde_1_5));                                    //Natural: COMPRESS CWF-SB-BENE.ADDR-ALL CWF-SB-BENE.STATE CWF-SB-BENE.ZIP-CDE-1-5 INTO CWF-SB-BENE.ADDR-ALL
                    if (condition(cwf_Sb_Bene_Zip_Cde_6_9.greater(" ")))                                                                                                  //Natural: IF CWF-SB-BENE.ZIP-CDE-6-9 GT ' '
                    {
                        cwf_Sb_Bene_Addr_All.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, cwf_Sb_Bene_Addr_All, "-", cwf_Sb_Bene_Zip_Cde_6_9));               //Natural: COMPRESS CWF-SB-BENE.ADDR-ALL '-' CWF-SB-BENE.ZIP-CDE-6-9 INTO CWF-SB-BENE.ADDR-ALL LEAVING NO
                    }                                                                                                                                                     //Natural: END-IF
                    DbsUtil.examine(new ExamineSource(cwf_Sb_Bene_Addr_All), new ExamineSearch(",,"), new ExamineReplace(","));                                           //Natural: EXAMINE CWF-SB-BENE.ADDR-ALL FOR ',,' REPLACE ','
                }                                                                                                                                                         //Natural: END-IF
                //*   FOREIGN
                if (condition(cwf_Sb_Bene_Us_Address.equals("N")))                                                                                                        //Natural: IF CWF-SB-BENE.US-ADDRESS EQ 'N'
                {
                    pnd_Addr_Typ_Cde.setValue("F");                                                                                                                       //Natural: ASSIGN #ADDR-TYP-CDE := 'F'
                    //*   DOMESTIC
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Addr_Typ_Cde.setValue("U");                                                                                                                       //Natural: ASSIGN #ADDR-TYP-CDE := 'U'
                }                                                                                                                                                         //Natural: END-IF
                //* *---  GET POLICYHOLDER NAME
                //*  OPEN MQ TO FACILITATE MDM CALL
                DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                              //Natural: FETCH RETURN 'MDMP0011'
                if (condition(Global.isEscape())) return;
                DbsUtil.callnat(Cwfn5374.class , getCurrentProcessState(), cwf_Sb_Ph_Ph_Pin_Nbr, pnd_Ph_Isn, pnd_Ph_Last_Name, pnd_Ph_First_Name, pnd_Ph_Middle_Name,     //Natural: CALLNAT 'CWFN5374' CWF-SB-PH.PH-PIN-NBR #PH-ISN #PH-LAST-NAME #PH-FIRST-NAME #PH-MIDDLE-NAME #PH-SSN #PH-DOB
                    pnd_Ph_Ssn, pnd_Ph_Dob);
                if (condition(Global.isEscape())) return;
                if (condition(pnd_Ph_First_Name.greater(" ") && pnd_Ph_First_Name.getSubstring(2).equals(" ")))                                                           //Natural: IF #PH-FIRST-NAME GT ' ' AND SUBSTR ( #PH-FIRST-NAME,2 ) EQ ' '
                {
                    setValueToSubstring(".",pnd_Ph_First_Name,2);                                                                                                         //Natural: MOVE '.' TO SUBSTR ( #PH-FIRST-NAME,2 )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Ph_Middle_Name.greater(" ")))                                                                                                           //Natural: IF #PH-MIDDLE-NAME GT ' '
                {
                    setValueToSubstring(".",pnd_Ph_Middle_Name,2);                                                                                                        //Natural: MOVE '.' TO SUBSTR ( #PH-MIDDLE-NAME,2 )
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ph_Name.setValue(DbsUtil.compress(pnd_Ph_First_Name, pnd_Ph_Middle_Name, pnd_Ph_Last_Name));                                                          //Natural: COMPRESS #PH-FIRST-NAME #PH-MIDDLE-NAME #PH-LAST-NAME INTO #PH-NAME
                //*  CLOSE MQ
                DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                              //Natural: FETCH RETURN 'MDMP0012'
                if (condition(Global.isEscape())) return;
                //* *---  DETERMINE WHICH REP THE LETTER WILL BE ASSIGNED TO
                pnd_Letter_Cnt.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #LETTER-CNT
                if (condition(pnd_Letter_Cnt.equals(1)))                                                                                                                  //Natural: IF #LETTER-CNT EQ 1
                {
                    //*  RETURNS #REP-IX(INDEX TO REP ARRAY)
                                                                                                                                                                          //Natural: PERFORM GET-REPRESENTATIVE
                    sub_Get_Representative();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Rep_Arr_Pnd_Rep_Case_Cnt.getValue(pnd_Rep_Ix).nadd(1);                                                                                            //Natural: ADD 1 TO #REP-CASE-CNT ( #REP-IX )
                }                                                                                                                                                         //Natural: END-IF
                pnd_Rep_Arr_Pnd_Rep_Letter_Cnt.getValue(pnd_Rep_Ix).nadd(1);                                                                                              //Natural: ADD 1 TO #REP-LETTER-CNT ( #REP-IX )
                pnd_Rep_Name.setValue(pnd_Rep_Key_Pnd_Rep_Unit.getValue(pnd_Rep_Ix));                                                                                     //Natural: ASSIGN #REP-NAME := #REP-UNIT ( #REP-IX )
                DbsUtil.callnat(Cwfn1107.class , getCurrentProcessState(), pnd_Rep_Arr_Pnd_Rep_Id.getValue(pnd_Rep_Ix), pnd_Rep_Name);                                    //Natural: CALLNAT 'CWFN1107' #REP-ID ( #REP-IX ) #REP-NAME
                if (condition(Global.isEscape())) return;
                pnd_Rep_Name.separate(SeparateOption.WithAnyDelimiters, " ", pnd_Str_Arr.getValue("*"));                                                                  //Natural: SEPARATE #REP-NAME INTO #STR-ARR ( * ) WITH DELIMITER ' ' NUMBER #J
                FOR03:                                                                                                                                                    //Natural: FOR #I EQ 1 TO #J
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_J)); pnd_I.nadd(1))
                {
                    DbsUtil.examine(new ExamineSource(pnd_Str_Arr.getValue(pnd_I),1,2), new ExamineTranslate(TranslateOption.Lower));                                     //Natural: EXAMINE SUBSTR ( #STR-ARR ( #I ) ,2 ) TRANSLATE INTO LOWER
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Rep_Name.setValue(DbsUtil.compress(pnd_Str_Arr.getValue("*")));                                                                                       //Natural: COMPRESS #STR-ARR ( * ) INTO #REP-NAME
                //* *--- DETERMINE WHAT TYPE OF LETTER TO PRINT
                if (condition(pnd_90d.getBoolean() || ! (pnd_Md.getBoolean()) || DbsUtil.maskMatches(master_1_Work_Prcss_Id,"'FAI'") || DbsUtil.maskMatches(master_1_Work_Prcss_Id, //Natural: IF ( #90D OR NOT #MD ) OR ( MASTER-1.WORK-PRCSS-ID EQ MASK ( 'FAI' ) OR MASTER-1.WORK-PRCSS-ID EQ MASK ( 'TAI' ) )
                    "'TAI'")))
                {
                    //*   90 DAY CORRESP FOLLOW-UP
                    if (condition(pnd_Status_Arr_Pnd_Stage.getValue(pnd_Status_Arr_Pnd_Status_Ix).equals("1")))                                                           //Natural: IF #STAGE ( #STATUS-IX ) EQ '1'
                    {
                        pnd_Letter_Type.setValue("7");                                                                                                                    //Natural: ASSIGN #LETTER-TYPE := '7'
                        pnd_Letter_Type_Desc.setValue("90D");                                                                                                             //Natural: ASSIGN #LETTER-TYPE-DESC := '90D'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  AG BEGIN
                        //*  DA
                        short decideConditionsMet952 = 0;                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ( MASTER-1.WORK-PRCSS-ID EQ MASK ( 'FAI' ) AND MASTER-1.STATUS-CDE = '4500' ) OR ( MASTER-1.WORK-PRCSS-ID EQ MASK ( 'TAI' ) AND MASTER-1.STATUS-CDE = #PAYMENTS-STATUS ( * ) )
                        if (condition(((DbsUtil.maskMatches(master_1_Work_Prcss_Id,"'FAI'") && master_1_Status_Cde.equals("4500")) || (DbsUtil.maskMatches(master_1_Work_Prcss_Id,"'TAI'") 
                            && master_1_Status_Cde.equals(pnd_Payments_Status.getValue("*"))))))
                        {
                            decideConditionsMet952++;
                            pnd_Letter_Type.setValue("B");                                                                                                                //Natural: ASSIGN #LETTER-TYPE := 'B'
                            pnd_Letter_Type_Desc.setValue("9OD-IA");                                                                                                      //Natural: ASSIGN #LETTER-TYPE-DESC := '9OD-IA'
                            //*           WHEN (MASTER-1.WORK-PRCSS-ID EQ 'RA DMD' AND
                            //*                 (MASTER-1.STATUS-CDE = '4751' OR = '4770') )
                            //*               #LETTER-TYPE      := 'B'
                            //*               #LETTER-TYPE-DESC := 'GENERIC DA'
                            //*           WHEN (MASTER-1.WORK-PRCSS-ID EQ 'RAIDMD' AND
                            //*                 (MASTER-1.STATUS-CDE = '4751' OR = '4770') )
                            //*               #LETTER-TYPE      := 'B'
                            //*               #LETTER-TYPE-DESC := 'GENERIC IA'
                        }                                                                                                                                                 //Natural: WHEN ( MASTER-1.WORK-PRCSS-ID EQ 'TAMDD ' OR MASTER-1.WORK-PRCSS-ID EQ 'TA DM ' OR MASTER-1.WORK-PRCSS-ID EQ 'FAMDD ' OR MASTER-1.WORK-PRCSS-ID EQ 'TA DLR' OR MASTER-1.WORK-PRCSS-ID EQ 'TAPDAM' OR MASTER-1.WORK-PRCSS-ID EQ 'TATDAM' OR MASTER-1.WORK-PRCSS-ID EQ 'TAMDAM' ) AND MASTER-1.STATUS-CDE = '4500'
                        else if (condition((((((((master_1_Work_Prcss_Id.equals("TAMDD ") || master_1_Work_Prcss_Id.equals("TA DM ")) || master_1_Work_Prcss_Id.equals("FAMDD ")) 
                            || master_1_Work_Prcss_Id.equals("TA DLR")) || master_1_Work_Prcss_Id.equals("TAPDAM")) || master_1_Work_Prcss_Id.equals("TATDAM")) 
                            || master_1_Work_Prcss_Id.equals("TAMDAM")) && master_1_Status_Cde.equals("4500"))))
                        {
                            decideConditionsMet952++;
                            pnd_Letter_Type.setValue("B");                                                                                                                //Natural: ASSIGN #LETTER-TYPE := 'B'
                            pnd_Letter_Type_Desc.setValue("GENERIC");                                                                                                     //Natural: ASSIGN #LETTER-TYPE-DESC := 'GENERIC'
                        }                                                                                                                                                 //Natural: WHEN MASTER-1.STATUS-CDE = '4710' OR MASTER-1.STATUS-CDE = '4908'
                        else if (condition(master_1_Status_Cde.equals("4710") || master_1_Status_Cde.equals("4908")))
                        {
                            decideConditionsMet952++;
                            pnd_Letter_Type.setValue("9");                                                                                                                //Natural: ASSIGN #LETTER-TYPE := '9'
                            pnd_Letter_Type_Desc.setValue("GUARDIAN");                                                                                                    //Natural: ASSIGN #LETTER-TYPE-DESC := 'GUARDIAN'
                        }                                                                                                                                                 //Natural: WHEN MASTER-1.STATUS-CDE = '4750' OR MASTER-1.STATUS-CDE = '4910'
                        else if (condition(master_1_Status_Cde.equals("4750") || master_1_Status_Cde.equals("4910")))
                        {
                            decideConditionsMet952++;
                            pnd_Letter_Type.setValue("8");                                                                                                                //Natural: ASSIGN #LETTER-TYPE := '8'
                            pnd_Letter_Type_Desc.setValue("CHILDREN");                                                                                                    //Natural: ASSIGN #LETTER-TYPE-DESC := 'CHILDREN'
                        }                                                                                                                                                 //Natural: WHEN MASTER-1.STATUS-CDE = '4762' OR MASTER-1.STATUS-CDE = '4938'
                        else if (condition(master_1_Status_Cde.equals("4762") || master_1_Status_Cde.equals("4938")))
                        {
                            decideConditionsMet952++;
                            pnd_Letter_Type.setValue("A");                                                                                                                //Natural: ASSIGN #LETTER-TYPE := 'A'
                            pnd_Letter_Type_Desc.setValue("ESTATE");                                                                                                      //Natural: ASSIGN #LETTER-TYPE-DESC := 'ESTATE'
                        }                                                                                                                                                 //Natural: WHEN MASTER-1.WORK-PRCSS-ID EQ MASK ( 'FA'.'D' ) OR ( MASTER-1.WORK-PRCSS-ID EQ MASK ( 'TA'.'D' ) AND MASTER-1.STATUS-CDE = #PAYMENTS-STATUS ( * ) )
                        else if (condition((DbsUtil.maskMatches(master_1_Work_Prcss_Id,"'FA'.'D'") || (DbsUtil.maskMatches(master_1_Work_Prcss_Id,"'TA'.'D'") 
                            && master_1_Status_Cde.equals(pnd_Payments_Status.getValue("*"))))))
                        {
                            decideConditionsMet952++;
                            pnd_Letter_Type.setValue("2");                                                                                                                //Natural: ASSIGN #LETTER-TYPE := '2'
                            pnd_Letter_Type_Desc.setValue("9OD-DA");                                                                                                      //Natural: ASSIGN #LETTER-TYPE-DESC := '9OD-DA'
                        }                                                                                                                                                 //Natural: WHEN NONE
                        else if (condition())
                        {
                            getReports().write(0, " NO CONDITION MET FOR","=",master_1_Pin_Nbr,"=",master_1_Work_Prcss_Id,"=",master_1_Status_Cde);                       //Natural: WRITE ' NO CONDITION MET FOR' '=' MASTER-1.PIN-NBR '=' MASTER-1.WORK-PRCSS-ID '=' MASTER-1.STATUS-CDE
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                            //*  AG END
                        }                                                                                                                                                 //Natural: END-DECIDE
                        //*         #LETTER-TYPE := '2'     /*  90 DAY FORMS FOLLOW-UP
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*   WHAT TYPE OF MD
                    //*   MD SPOUSE
                    //*   MD NON SPOUSE
                    //*   MD DOD 2 YEARS+ PRIOR
                    short decideConditionsMet999 = 0;                                                                                                                     //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CWF-SB-BENE.SRVVR-SPSE-IND EQ 'Y' OR ( CWF-SB-BENE.SRVVR-RLTNSHP EQ 'SPOUSE' OR EQ 'HUSBAND' OR EQ 'WIFE' )
                    if (condition(cwf_Sb_Bene_Srvvr_Spse_Ind.equals("Y") || cwf_Sb_Bene_Srvvr_Rltnshp.equals("SPOUSE") || cwf_Sb_Bene_Srvvr_Rltnshp.equals("HUSBAND") 
                        || cwf_Sb_Bene_Srvvr_Rltnshp.equals("WIFE")))
                    {
                        decideConditionsMet999++;
                        pnd_Letter_Type.setValue("3");                                                                                                                    //Natural: ASSIGN #LETTER-TYPE := '3'
                        pnd_Letter_Type_Desc.setValue("MDS");                                                                                                             //Natural: ASSIGN #LETTER-TYPE-DESC := 'MDS'
                    }                                                                                                                                                     //Natural: WHEN CWF-SB-PH.DOD-YYYY EQ #TODAYS-YYYY OR EQ #LAST-YEARS-YYYY
                    else if (condition(cwf_Sb_Ph_Dod_Yyyy.equals(pnd_Todays_Date_A8_Pnd_Todays_Yyyy) || cwf_Sb_Ph_Dod_Yyyy.equals(pnd_Last_Years_Yyyy)))
                    {
                        decideConditionsMet999++;
                        pnd_Letter_Type.setValue("4");                                                                                                                    //Natural: ASSIGN #LETTER-TYPE := '4'
                        pnd_Letter_Type_Desc.setValue("MD1");                                                                                                             //Natural: ASSIGN #LETTER-TYPE-DESC := 'MD1'
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        pnd_Letter_Type.setValue("5");                                                                                                                    //Natural: ASSIGN #LETTER-TYPE := '5'
                        pnd_Letter_Type_Desc.setValue("MD2");                                                                                                             //Natural: ASSIGN #LETTER-TYPE-DESC := 'MD2'
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-IF
                //* ---  CORRESPONDENCE STAGE
                if (condition(pnd_Status_Arr_Pnd_Stage.getValue(pnd_Status_Arr_Pnd_Status_Ix).equals("1")))                                                               //Natural: IF #STAGE ( #STATUS-IX ) EQ '1'
                {
                    pnd_Letter_Type_Desc.setValue(DbsUtil.compress(pnd_Letter_Type_Desc, "Corr"));                                                                        //Natural: COMPRESS #LETTER-TYPE-DESC 'Corr' INTO #LETTER-TYPE-DESC
                }                                                                                                                                                         //Natural: END-IF
                //* *---  CALL THE POST DRIVER PROGRAM
                DbsUtil.callnat(Swfn4410.class , getCurrentProcessState(), pnd_Letter_Type, pnd_Todays_Date_D, cwf_Sb_Ph_Ph_Pin_Nbr, pnd_Ph_Name, cwf_Sb_Bene_Srvvr_Nme_Fr_Frm,  //Natural: CALLNAT 'SWFN4410' #LETTER-TYPE #TODAYS-DATE-D CWF-SB-PH.PH-PIN-NBR #PH-NAME CWF-SB-BENE.SRVVR-NME-FR-FRM CWF-SB-BENE.SRVVR-NME-FR-FR2 #ADDR-TYP-CDE CWF-SB-BENE.ADDR-ARR ( 1:6 ) CWF-SB-BENE.SRVVR-ZIP-CDE #INT-RATE-A #YEAR #REP-UNIT ( #REP-IX ) #REP-ID ( #REP-IX ) #REP-NAME #PRINTER-ID MASTER-1.RQST-LOG-DTE-TME PARM-EMPLOYEE-INFO-SUB PARM-UNIT-INFO-SUB DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
                    cwf_Sb_Bene_Srvvr_Nme_Fr_Fr2, pnd_Addr_Typ_Cde, cwf_Sb_Bene_Addr_Arr.getValue(1,":",6), cwf_Sb_Bene_Srvvr_Zip_Cde, pnd_Int_Rate_A, pnd_Year, 
                    pnd_Rep_Key_Pnd_Rep_Unit.getValue(pnd_Rep_Ix), pnd_Rep_Arr_Pnd_Rep_Id.getValue(pnd_Rep_Ix), pnd_Rep_Name, pnd_Control_Rec_Pnd_Printer_Id, 
                    master_1_Rqst_Log_Dte_Tme, pdaCwfpdaus.getParm_Employee_Info_Sub(), pdaCwfpdaun.getParm_Unit_Info_Sub(), pdaCwfpda_D.getDialog_Info_Sub(), 
                    pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
                if (condition(Global.isEscape())) return;
                //*  COR/NAAD SUNSET START
                //*      PARM-EMPLOYEE-INFO
                //*      PARM-UNIT-INFO
                //*      DIALOG-INFO
                //*      MSG-INFO
                //*      PASS
                //*  COR/NAAD SUNSET END
                if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().greater(" ")))                                                                            //Natural: IF ##RETURN-CODE GT ' '
                {
                    pnd_Err_Msg.setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                                      //Natural: ASSIGN #ERR-MSG := ##MSG
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REPORT
                    sub_Write_Error_Report();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //* *---  WRITE THE REPORT
                pnd_Total_Letters.nadd(1);                                                                                                                                //Natural: ADD 1 TO #TOTAL-LETTERS
                getReports().display(0, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/PIN",                                                        //Natural: DISPLAY ( PRT1 ) ( HC = L ) '/PIN' CWF-SB-PH.PH-PIN-NBR ( IS = ON ) 'Policyholder/Name' #PH-NAME ( IS = ON AL = 30 ) '/Survivor' CWF-SB-BENE.SRVVR-NME-FR-FRM ( AL = 30 ) 'Letter/Type' #LETTER-TYPE-DESC
                		cwf_Sb_Ph_Ph_Pin_Nbr, new IdenticalSuppress(true),"Policyholder/Name",
                		pnd_Ph_Name, new IdenticalSuppress(true), new AlphanumericLength (30),"/Survivor",
                		cwf_Sb_Bene_Srvvr_Nme_Fr_Frm, new AlphanumericLength (30),"Letter/Type",
                		pnd_Letter_Type_Desc);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //* *---  UPDATE CWF-SB-WORK-REQ FIELDS
                G1:                                                                                                                                                       //Natural: GET CWF-SB-WORK-REQ *ISN ( F1. )
                vw_cwf_Sb_Work_Req.readByID(vw_cwf_Sb_Work_Req.getAstISN("Read05"), "G1");
                short decideConditionsMet1041 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE OF #LETTER-TYPE;//Natural: VALUE '7'
                if (condition((pnd_Letter_Type.equals("7"))))
                {
                    decideConditionsMet1041++;
                    cwf_Sb_Work_Req_Corresp_90d_Crte_Dte.setValue(pnd_Todays_Date_A8_Pnd_Todays_Date_N8);                                                                 //Natural: ASSIGN CWF-SB-WORK-REQ.CORRESP-90D-CRTE-DTE := #TODAYS-DATE-N8
                    cwf_Sb_Work_Req_Corresp_90d_Crte_Nbr.nadd(1);                                                                                                         //Natural: ADD 1 TO CWF-SB-WORK-REQ.CORRESP-90D-CRTE-NBR
                    pnd_Total_90d_Corresp.nadd(1);                                                                                                                        //Natural: ADD 1 TO #TOTAL-90D-CORRESP
                }                                                                                                                                                         //Natural: VALUE '2', '8' , '9', 'A', 'B'
                else if (condition((pnd_Letter_Type.equals("2") || pnd_Letter_Type.equals("8") || pnd_Letter_Type.equals("9") || pnd_Letter_Type.equals("A") 
                    || pnd_Letter_Type.equals("B"))))
                {
                    decideConditionsMet1041++;
                    cwf_Sb_Work_Req_Ninety_Day_Crte_Dte.setValue(pnd_Todays_Date_A8_Pnd_Todays_Date_N8);                                                                  //Natural: ASSIGN CWF-SB-WORK-REQ.NINETY-DAY-CRTE-DTE := #TODAYS-DATE-N8
                    cwf_Sb_Work_Req_Ninety_Day_Crte_Nbr.nadd(1);                                                                                                          //Natural: ADD 1 TO CWF-SB-WORK-REQ.NINETY-DAY-CRTE-NBR
                    pnd_Total_90d.nadd(1);                                                                                                                                //Natural: ADD 1 TO #TOTAL-90D
                }                                                                                                                                                         //Natural: VALUE '3'
                else if (condition((pnd_Letter_Type.equals("3"))))
                {
                    decideConditionsMet1041++;
                    cwf_Sb_Work_Req_Md_Mds_Crte_Dte.setValue(pnd_Todays_Date_A8_Pnd_Todays_Date_N8);                                                                      //Natural: ASSIGN CWF-SB-WORK-REQ.MD-MDS-CRTE-DTE := #TODAYS-DATE-N8
                    cwf_Sb_Work_Req_Md_Mds_Crte_Nbr.nadd(1);                                                                                                              //Natural: ADD 1 TO CWF-SB-WORK-REQ.MD-MDS-CRTE-NBR
                    pnd_Total_Mds.nadd(1);                                                                                                                                //Natural: ADD 1 TO #TOTAL-MDS
                }                                                                                                                                                         //Natural: VALUE '4'
                else if (condition((pnd_Letter_Type.equals("4"))))
                {
                    decideConditionsMet1041++;
                    cwf_Sb_Work_Req_Md_Md1_Crte_Dte.setValue(pnd_Todays_Date_A8_Pnd_Todays_Date_N8);                                                                      //Natural: ASSIGN CWF-SB-WORK-REQ.MD-MD1-CRTE-DTE := #TODAYS-DATE-N8
                    cwf_Sb_Work_Req_Md_Md1_Crte_Nbr.nadd(1);                                                                                                              //Natural: ADD 1 TO CWF-SB-WORK-REQ.MD-MD1-CRTE-NBR
                    pnd_Total_Md1.nadd(1);                                                                                                                                //Natural: ADD 1 TO #TOTAL-MD1
                }                                                                                                                                                         //Natural: VALUE '5'
                else if (condition((pnd_Letter_Type.equals("5"))))
                {
                    decideConditionsMet1041++;
                    cwf_Sb_Work_Req_Md_Md2_Crte_Dte.setValue(pnd_Todays_Date_A8_Pnd_Todays_Date_N8);                                                                      //Natural: ASSIGN CWF-SB-WORK-REQ.MD-MD2-CRTE-DTE := #TODAYS-DATE-N8
                    cwf_Sb_Work_Req_Md_Md2_Crte_Nbr.nadd(1);                                                                                                              //Natural: ADD 1 TO CWF-SB-WORK-REQ.MD-MD2-CRTE-NBR
                    pnd_Total_Md2.nadd(1);                                                                                                                                //Natural: ADD 1 TO #TOTAL-MD2
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                vw_cwf_Sb_Work_Req.updateDBRow("G1");                                                                                                                     //Natural: UPDATE ( G1. )
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Letter_Cnt.equals(getZero())))                                                                                                              //Natural: IF #LETTER-CNT EQ 0
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Letter_Cnt.reset();                                                                                                                                       //Natural: RESET #LETTER-CNT
            //* *---  CHECK HOW MANY PINS HAVE BEEN PROCESSED SO FAR
            pnd_Total_Pins.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #TOTAL-PINS
            if (condition(pnd_Total_Pins.add(pnd_Starting_Pin_Cnt).greaterOrEqual(pnd_Control_Rec_Pnd_Max_Pins)))                                                         //Natural: IF #TOTAL-PINS + #STARTING-PIN-CNT GE #MAX-PINS
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //* *---
            //*  ADD 1 TO #ET-CNT
            //*  IF #ET-CNT GE 20
            //*    RESET #ET-CNT
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTROL-RECORDS
            sub_Update_Control_Records();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            //*   BACKOUT TRANSACTION
            //*  END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* *---  IF LAST PIN HAS BEEN PROCESSED THEN START ALL OVER
        //*   PIN-EXP
        if (condition(pnd_Total_Pins.add(pnd_Starting_Pin_Cnt).less(pnd_Control_Rec_Pnd_Max_Pins)))                                                                       //Natural: IF #TOTAL-PINS + #STARTING-PIN-CNT LT #MAX-PINS
        {
            //*  #LAST-PIN := 0000000
            pnd_Control_Rec_Pnd_Last_Pin.setValue(0);                                                                                                                     //Natural: ASSIGN #LAST-PIN := 000000000000
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTROL-RECORDS
        sub_Update_Control_Records();
        if (condition(Global.isEscape())) {return;}
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  BACKOUT TRANSACTION
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-CONTROL-RECORDS
        //* ****************************
        //* *---  UPDATE THE CONTROL TBL
        //* *---  UPDATE REPS RECORDS
        //* ************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-REPRESENTATIVE
        //* *---  IF NO REP WAS FOUND FOR THE REQUEST
        //* ************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-ERROR-REPORT
        //* ********************
        //*  ERROR-REPORT
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( PRT2 ) TITLE LEFT / #REPORT-TITLE 70T 'PAGE:' *PAGE-NUMBER ( PRT2 ) ( AD = L ) / 'ERROR REPORT FOR' #TODAYS-DATE-D ( EM = LLL�DD,�YYYY ) / 'RUN DATE/TIME:' *TIMX ( EM = MM/DD/YY�HH:II:SS ) 70T *PROGRAM / '-' ( 79 )
        //* ********************
        if (condition(getReports().getAstLinesLeft(0).less(6)))                                                                                                           //Natural: NEWPAGE ( PRT2 ) IF LESS THAN 6 LINES LEFT
        {
            getReports().newPage(0);
            if (condition(Global.isEscape())){return;}
        }
        getReports().write(3, NEWLINE,NEWLINE,NEWLINE,NEWLINE,"******************************************",NEWLINE,"TOTAL NUMBER OF ERRORS FOUND:",pnd_Total_Errors,      //Natural: WRITE ( PRT2 ) //// '******************************************' / 'TOTAL NUMBER OF ERRORS FOUND:' #TOTAL-ERRORS / '******************************************'
            NEWLINE,"******************************************");
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( PRT2 )
        if (condition(Global.isEscape())){return;}
        //* **********************
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( PRT1 ) TITLE LEFT / #REPORT-TITLE 70T 'PAGE:' *PAGE-NUMBER ( PRT1 ) ( AD = L ) / 'DAILY PROCESSING REPORT FOR' #TODAYS-DATE-D ( EM = LLL�DD,�YYYY ) / 'RUN DATE/TIME:' *TIMX ( EM = MM/DD/YY�HH:II:SS ) 70T *PROGRAM / '-' ( 79 )
        //* **********************
        if (condition(getReports().getAstLinesLeft(0).less(12)))                                                                                                          //Natural: NEWPAGE ( PRT1 ) IF LESS THAN 12 LINES LEFT
        {
            getReports().newPage(0);
            if (condition(Global.isEscape())){return;}
        }
        getReports().write(2, NEWLINE,NEWLINE,"*******************************************",NEWLINE,"TOTAL NUMBER OF",NEWLINE,"       POLICYHOLDERS PROCESSED:",          //Natural: WRITE ( PRT1 ) // '*******************************************' / 'TOTAL NUMBER OF' / '       POLICYHOLDERS PROCESSED:' #TOTAL-PINS / '       LETTERS PRINTED        :' #TOTAL-LETTERS
            pnd_Total_Pins,NEWLINE,"       LETTERS PRINTED        :",pnd_Total_Letters);
        if (Global.isEscape()) return;
        if (condition(pnd_Md.getBoolean()))                                                                                                                               //Natural: IF #MD
        {
            getReports().write(2, NEWLINE,"       CORRESP 90D:",pnd_Total_90d_Corresp,NEWLINE,"         FORMS 90D:",pnd_Total_90d,NEWLINE,"               MDS:",          //Natural: WRITE ( PRT1 ) / '       CORRESP 90D:' #TOTAL-90D-CORRESP / '         FORMS 90D:' #TOTAL-90D / '               MDS:' #TOTAL-MDS / '               MD1:' #TOTAL-MD1 / '               MD2:' #TOTAL-MD2
                pnd_Total_Mds,NEWLINE,"               MD1:",pnd_Total_Md1,NEWLINE,"               MD2:",pnd_Total_Md2);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(2, "*******************************************");                                                                                             //Natural: WRITE ( PRT1 ) '*******************************************'
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( PRT1 )
        if (condition(Global.isEscape())){return;}
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #FILL-STATUS-TABLE
        //* **********************************************************************
    }
    private void sub_Update_Control_Records() throws Exception                                                                                                            //Natural: UPDATE-CONTROL-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Tbl_Key_Tbl_Scrty_Level_Ind.setValue("S");                                                                                                                    //Natural: ASSIGN #TBL-KEY.TBL-SCRTY-LEVEL-IND := 'S'
        pnd_Tbl_Key_Tbl_Table_Nme.setValue("CWF-SB-LETTER-CNTRL");                                                                                                        //Natural: ASSIGN #TBL-KEY.TBL-TABLE-NME := 'CWF-SB-LETTER-CNTRL'
        if (condition(pnd_Md.getBoolean()))                                                                                                                               //Natural: IF #MD
        {
            pnd_Tbl_Key_Tbl_Key_Field.setValue("CONTROL MD");                                                                                                             //Natural: ASSIGN #TBL-KEY.TBL-KEY-FIELD := 'CONTROL MD'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Tbl_Key_Tbl_Key_Field.setValue("CONTROL 90");                                                                                                             //Natural: ASSIGN #TBL-KEY.TBL-KEY-FIELD := 'CONTROL 90'
        }                                                                                                                                                                 //Natural: END-IF
        vw_cwf_Support_Tbl.startDatabaseFind                                                                                                                              //Natural: FIND CWF-SUPPORT-TBL WITH TBL-PRIME-KEY EQ #TBL-KEY
        (
        "UPD1",
        new Wc[] { new Wc("TBL_PRIME_KEY", "=", pnd_Tbl_Key, WcType.WITH) }
        );
        UPD1:
        while (condition(vw_cwf_Support_Tbl.readNextRow("UPD1")))
        {
            vw_cwf_Support_Tbl.setIfNotFoundControlFlag(false);
            pnd_Control_Rec_Pnd_Last_Run_Date.setValue(pnd_Todays_Date_A8);                                                                                               //Natural: ASSIGN #LAST-RUN-DATE := #TODAYS-DATE-A8
            pnd_Control_Rec_Pnd_Printed_Pin_Cnt.compute(new ComputeParameters(false, pnd_Control_Rec_Pnd_Printed_Pin_Cnt), pnd_Total_Pins.add(pnd_Starting_Pin_Cnt));     //Natural: ASSIGN #PRINTED-PIN-CNT := #TOTAL-PINS + #STARTING-PIN-CNT
            pnd_Control_Rec_Pnd_Printed_Letter_Cnt.setValue(pnd_Total_Letters);                                                                                           //Natural: ASSIGN #PRINTED-LETTER-CNT := #TOTAL-LETTERS
            cwf_Support_Tbl_Tbl_Data_Field.setValue(pnd_Control_Rec);                                                                                                     //Natural: ASSIGN CWF-SUPPORT-TBL.TBL-DATA-FIELD := #CONTROL-REC
            vw_cwf_Support_Tbl.updateDBRow("UPD1");                                                                                                                       //Natural: UPDATE ( UPD1. )
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        pnd_Tbl_Key_Tbl_Scrty_Level_Ind.setValue("S");                                                                                                                    //Natural: ASSIGN #TBL-KEY.TBL-SCRTY-LEVEL-IND := 'S'
        pnd_Tbl_Key_Tbl_Table_Nme.setValue("CWF-SB-LETTER-REPS");                                                                                                         //Natural: ASSIGN #TBL-KEY.TBL-TABLE-NME := 'CWF-SB-LETTER-REPS'
        FOR04:                                                                                                                                                            //Natural: FOR #I EQ 1 TO #REP-CNT
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Rep_Cnt)); pnd_I.nadd(1))
        {
            pnd_Tbl_Key_Tbl_Key_Field.setValue(pnd_Rep_Key.getValue(pnd_I));                                                                                              //Natural: ASSIGN #TBL-KEY.TBL-KEY-FIELD := #REP-KEY ( #I )
            vw_cwf_Support_Tbl.startDatabaseFind                                                                                                                          //Natural: FIND CWF-SUPPORT-TBL WITH TBL-PRIME-KEY EQ #TBL-KEY
            (
            "UPD2",
            new Wc[] { new Wc("TBL_PRIME_KEY", "=", pnd_Tbl_Key, WcType.WITH) }
            );
            UPD2:
            while (condition(vw_cwf_Support_Tbl.readNextRow("UPD2")))
            {
                vw_cwf_Support_Tbl.setIfNotFoundControlFlag(false);
                DbsUtil.examine(new ExamineSource(cwf_Support_Tbl_Rep_Id.getValue("*")), new ExamineSearch(pnd_Rep_Arr_Pnd_Rep_Id.getValue(pnd_I)), new                   //Natural: EXAMINE CWF-SUPPORT-TBL.REP-ID ( * ) FOR #REP-ID ( #I ) GIVING INDEX #J
                    ExamineGivingIndex(pnd_J));
                if (condition(pnd_J.greater(getZero())))                                                                                                                  //Natural: IF #J GT 0
                {
                    cwf_Support_Tbl_Tbl_Reps.getValue(pnd_J).setValue(pnd_Rep_Arr.getValue(pnd_I));                                                                       //Natural: ASSIGN CWF-SUPPORT-TBL.TBL-REPS ( #J ) := #REP-ARR ( #I )
                    vw_cwf_Support_Tbl.updateDBRow("UPD2");                                                                                                               //Natural: UPDATE ( UPD2. )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Err_Msg.setValue("SYSTEM ERROR -  Representative record is missing.");                                                                            //Natural: ASSIGN #ERR-MSG := 'SYSTEM ERROR -  Representative record is missing.'
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REPORT
                    sub_Write_Error_Report();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("UPD2"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("UPD2"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Get_Representative() throws Exception                                                                                                                //Natural: GET-REPRESENTATIVE
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************
        //*   REPS W/ UNIT AND LOB MATCHING THE RQST
        pnd_Rep_Ix.reset();                                                                                                                                               //Natural: RESET #REP-IX
        FOR05:                                                                                                                                                            //Natural: FOR #I EQ 1 TO #REP-CNT
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Rep_Cnt)); pnd_I.nadd(1))
        {
            if (condition(pnd_Rep_Arr_Pnd_Rep_Act_Ind.getValue(pnd_I).notEquals("A") || pnd_Rep_Key_Pnd_Rep_Unit.getValue(pnd_I).notEquals(master_1_Unit_Cde)             //Natural: IF #REP-ACT-IND ( #I ) NE 'A' OR #REP-UNIT ( #I ) NE MASTER-1.UNIT-CDE OR #REP-LOB ( #I ) NE #LOB
                || pnd_Rep_Arr_Pnd_Rep_Lob.getValue(pnd_I).notEquals(pnd_Lob)))
            {
                //* *****SUBSTR(#REP-UNIT(#I),1,5) NE SUBSTR(MASTER-1.UNIT-CDE,1,5) OR
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*   START WITH FIRST REP FOUND
            if (condition(pnd_Rep_Ix.equals(getZero())))                                                                                                                  //Natural: IF #REP-IX EQ 0
            {
                pnd_Rep_Ix.setValue(pnd_I);                                                                                                                               //Natural: ASSIGN #REP-IX := #I
                //*   BUT GET THE NEXT IF IT's count is less
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*   THIS IS THE NEXT REP
                if (condition(pnd_Rep_Arr_Pnd_Rep_Case_Cnt.getValue(pnd_I).less(pnd_Rep_Arr_Pnd_Rep_Case_Cnt.getValue(pnd_Rep_Ix))))                                      //Natural: IF #REP-CASE-CNT ( #I ) LT #REP-CASE-CNT ( #REP-IX )
                {
                    pnd_Rep_Ix.setValue(pnd_I);                                                                                                                           //Natural: ASSIGN #REP-IX := #I
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Rep_Ix.greater(getZero())))                                                                                                                     //Natural: IF #REP-IX GT 0
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
            //*   GET NEXT REP W/ THE LOWEST COUNT
        }                                                                                                                                                                 //Natural: END-IF
        FOR06:                                                                                                                                                            //Natural: FOR #I EQ 1 TO #REP-CNT
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Rep_Cnt)); pnd_I.nadd(1))
        {
            if (condition(pnd_Rep_Arr_Pnd_Rep_Act_Ind.getValue(pnd_I).notEquals("A")))                                                                                    //Natural: IF #REP-ACT-IND ( #I ) NE 'A'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Rep_Ix.equals(getZero())))                                                                                                                  //Natural: IF #REP-IX EQ 0
            {
                pnd_Rep_Ix.setValue(pnd_I);                                                                                                                               //Natural: ASSIGN #REP-IX := #I
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Rep_Arr_Pnd_Rep_Case_Cnt.getValue(pnd_I).less(pnd_Rep_Arr_Pnd_Rep_Case_Cnt.getValue(pnd_Rep_Ix))))                                      //Natural: IF #REP-CASE-CNT ( #I ) LT #REP-CASE-CNT ( #REP-IX )
                {
                    pnd_Rep_Ix.setValue(pnd_I);                                                                                                                           //Natural: ASSIGN #REP-IX := #I
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Write_Error_Report() throws Exception                                                                                                                //Natural: WRITE-ERROR-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************
        getReports().write(3, NEWLINE,cwf_Sb_Ph_Ph_Pin_Nbr,master_1_Work_Prcss_Id,master_1_Status_Cde,master_1_Rqst_Log_Dte_Tme,NEWLINE,"*** ERROR: ",pnd_Err_Msg,        //Natural: WRITE ( PRT2 ) / CWF-SB-PH.PH-PIN-NBR MASTER-1.WORK-PRCSS-ID MASTER-1.STATUS-CDE MASTER-1.RQST-LOG-DTE-TME / '*** ERROR: ' #ERR-MSG ( AL = 65 )
            new AlphanumericLength (65));
        if (Global.isEscape()) return;
        pnd_Err_Msg.reset();                                                                                                                                              //Natural: RESET #ERR-MSG
        pnd_Total_Errors.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #TOTAL-ERRORS
    }
    private void sub_Pnd_Fill_Status_Table() throws Exception                                                                                                             //Natural: #FILL-STATUS-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //* * THIS IS A SHORT TERM SOLUTION. IT SHOULD EVENTUALLY BE MOVED
        //* * INTO A TABLE          A.G.
        pnd_Status_Arr_Pnd_Status_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #STATUS-CNT
        pnd_Status_Arr_Pnd_Status.getValue(pnd_Status_Arr_Pnd_Status_Cnt).setValue("4710");                                                                               //Natural: ASSIGN #STATUS ( #STATUS-CNT ) := '4710'
        pnd_Status_Arr_Pnd_Status_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #STATUS-CNT
        pnd_Status_Arr_Pnd_Status.getValue(pnd_Status_Arr_Pnd_Status_Cnt).setValue("4750");                                                                               //Natural: ASSIGN #STATUS ( #STATUS-CNT ) := '4750'
        pnd_Status_Arr_Pnd_Status_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #STATUS-CNT
        pnd_Status_Arr_Pnd_Status.getValue(pnd_Status_Arr_Pnd_Status_Cnt).setValue("4762");                                                                               //Natural: ASSIGN #STATUS ( #STATUS-CNT ) := '4762'
        pnd_Status_Arr_Pnd_Status_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #STATUS-CNT
        pnd_Status_Arr_Pnd_Status.getValue(pnd_Status_Arr_Pnd_Status_Cnt).setValue("4901");                                                                               //Natural: ASSIGN #STATUS ( #STATUS-CNT ) := '4901'
        pnd_Status_Arr_Pnd_Status_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #STATUS-CNT
        pnd_Status_Arr_Pnd_Status.getValue(pnd_Status_Arr_Pnd_Status_Cnt).setValue("4902");                                                                               //Natural: ASSIGN #STATUS ( #STATUS-CNT ) := '4902'
        pnd_Status_Arr_Pnd_Status_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #STATUS-CNT
        pnd_Status_Arr_Pnd_Status.getValue(pnd_Status_Arr_Pnd_Status_Cnt).setValue("4903");                                                                               //Natural: ASSIGN #STATUS ( #STATUS-CNT ) := '4903'
        pnd_Status_Arr_Pnd_Status_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #STATUS-CNT
        pnd_Status_Arr_Pnd_Status.getValue(pnd_Status_Arr_Pnd_Status_Cnt).setValue("4904");                                                                               //Natural: ASSIGN #STATUS ( #STATUS-CNT ) := '4904'
        pnd_Status_Arr_Pnd_Status_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #STATUS-CNT
        pnd_Status_Arr_Pnd_Status.getValue(pnd_Status_Arr_Pnd_Status_Cnt).setValue("4905");                                                                               //Natural: ASSIGN #STATUS ( #STATUS-CNT ) := '4905'
        pnd_Status_Arr_Pnd_Status_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #STATUS-CNT
        pnd_Status_Arr_Pnd_Status.getValue(pnd_Status_Arr_Pnd_Status_Cnt).setValue("4906");                                                                               //Natural: ASSIGN #STATUS ( #STATUS-CNT ) := '4906'
        pnd_Status_Arr_Pnd_Status_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #STATUS-CNT
        pnd_Status_Arr_Pnd_Status.getValue(pnd_Status_Arr_Pnd_Status_Cnt).setValue("4907");                                                                               //Natural: ASSIGN #STATUS ( #STATUS-CNT ) := '4907'
        pnd_Status_Arr_Pnd_Status_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #STATUS-CNT
        pnd_Status_Arr_Pnd_Status.getValue(pnd_Status_Arr_Pnd_Status_Cnt).setValue("4908");                                                                               //Natural: ASSIGN #STATUS ( #STATUS-CNT ) := '4908'
        pnd_Status_Arr_Pnd_Status_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #STATUS-CNT
        pnd_Status_Arr_Pnd_Status.getValue(pnd_Status_Arr_Pnd_Status_Cnt).setValue("4909");                                                                               //Natural: ASSIGN #STATUS ( #STATUS-CNT ) := '4909'
        pnd_Status_Arr_Pnd_Status_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #STATUS-CNT
        pnd_Status_Arr_Pnd_Status.getValue(pnd_Status_Arr_Pnd_Status_Cnt).setValue("4910");                                                                               //Natural: ASSIGN #STATUS ( #STATUS-CNT ) := '4910'
        pnd_Status_Arr_Pnd_Status_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #STATUS-CNT
        pnd_Status_Arr_Pnd_Status.getValue(pnd_Status_Arr_Pnd_Status_Cnt).setValue("4938");                                                                               //Natural: ASSIGN #STATUS ( #STATUS-CNT ) := '4938'
        pnd_Status_Arr_Pnd_Status_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #STATUS-CNT
        pnd_Status_Arr_Pnd_Status.getValue(pnd_Status_Arr_Pnd_Status_Cnt).setValue("4941");                                                                               //Natural: ASSIGN #STATUS ( #STATUS-CNT ) := '4941'
        pnd_Status_Arr_Pnd_Status_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #STATUS-CNT
        pnd_Status_Arr_Pnd_Status.getValue(pnd_Status_Arr_Pnd_Status_Cnt).setValue("4751");                                                                               //Natural: ASSIGN #STATUS ( #STATUS-CNT ) := '4751'
        pnd_Status_Arr_Pnd_Status_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #STATUS-CNT
        pnd_Status_Arr_Pnd_Status.getValue(pnd_Status_Arr_Pnd_Status_Cnt).setValue("4770");                                                                               //Natural: ASSIGN #STATUS ( #STATUS-CNT ) := '4770'
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(2, "LS=133 SG=OFF PS=56");
        Global.format(3, "LS=133 SG=OFF PS=56");

        getReports().write(3, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,NEWLINE,pnd_Report_Title,new TabSetting(70),"PAGE:",getReports().getPageNumberDbs(3), 
            new FieldAttributes ("AD=L"),NEWLINE,"ERROR REPORT FOR",pnd_Todays_Date_D, new ReportEditMask ("LLL DD, YYYY"),NEWLINE,"RUN DATE/TIME:",Global.getTIMX(), 
            new ReportEditMask ("MM/DD/YY HH:II:SS"),new TabSetting(70),Global.getPROGRAM(),NEWLINE,"-",new RepeatItem(79));
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,NEWLINE,pnd_Report_Title,new TabSetting(70),"PAGE:",getReports().getPageNumberDbs(2), 
            new FieldAttributes ("AD=L"),NEWLINE,"DAILY PROCESSING REPORT FOR",pnd_Todays_Date_D, new ReportEditMask ("LLL DD, YYYY"),NEWLINE,"RUN DATE/TIME:",Global.getTIMX(), 
            new ReportEditMask ("MM/DD/YY HH:II:SS"),new TabSetting(70),Global.getPROGRAM(),NEWLINE,"-",new RepeatItem(79));

        getReports().setDisplayColumns(0, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/PIN",
        		cwf_Sb_Ph_Ph_Pin_Nbr, new IdenticalSuppress(true),"Policyholder/Name",
        		pnd_Ph_Name, new IdenticalSuppress(true), new AlphanumericLength (30),"/Survivor",
        		cwf_Sb_Bene_Srvvr_Nme_Fr_Frm, new AlphanumericLength (30),"Letter/Type",
        		pnd_Letter_Type_Desc);
    }
}
