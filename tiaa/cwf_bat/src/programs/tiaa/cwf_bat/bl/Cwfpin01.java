/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:01:00 PM
**        * FROM NATURAL PROGRAM : Cwfpin01
************************************************************
**        * FILE NAME            : Cwfpin01.java
**        * CLASS NAME           : Cwfpin01
**        * INSTANCE NAME        : Cwfpin01
************************************************************
* PROGRAM  : CWFPIN01
* SYSTEM   : CWF
* TITLE    : UPDATE PIN....
* GENERATED: JUN 12,17
* FUNCTION : ALPHANUMERIC PIN UPDATE WITH '00000' AS PREFIX
*
* HISTORY
* 12/06/17  ADDED AS ONE TIME UPDATE PROGRAM FOR 12 DIGIT PIN
*

************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwfpin01 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cwf_Master;
    private DbsField cwf_Master_Rlte_Rqst_Id;

    private DbsGroup cwf_Master__R_Field_1;
    private DbsField cwf_Master_Rlte_Work_Prcss_Id;
    private DbsField cwf_Master_Rlte_Tiaa_Rcvd_Dte;
    private DbsField cwf_Master_Rlte_Case_Id_Cde;
    private DbsField cwf_Master_Rlte_Pin_Nbr;
    private DbsField cwf_Master_Filler_A;
    private DbsField cwf_Master_Rqst_Id;

    private DbsGroup cwf_Master__R_Field_2;
    private DbsField cwf_Master_Rqst_Work_Prcss_Id;
    private DbsField cwf_Master_Rqst_Tiaa_Rcvd_Dte;
    private DbsField cwf_Master_Rqst_Case_Id_Cde;
    private DbsField cwf_Master_Rqst_Pin_Nbr;
    private DbsField cwf_Master_Filler_C;
    private DbsField target_Field;

    private DbsGroup target_Field__R_Field_3;
    private DbsField target_Field_New_Rlte_Work_Prcss_Id;
    private DbsField target_Field_New_Rlte_Tiaa_Rcvd_Dte;
    private DbsField target_Field_New_Rlte_Case_Id_Cde;
    private DbsField target_Field_Filler_B;
    private DbsField target_Field_New_Rlte_Pin_Nbr;
    private DbsField target_Field2;

    private DbsGroup target_Field2__R_Field_4;
    private DbsField target_Field2_New_Rqst_Work_Prcss_Id;
    private DbsField target_Field2_New_Rqst_Tiaa_Rcvd_Dte;
    private DbsField target_Field2_New_Rqst_Case_Id_Cde;
    private DbsField target_Field2_Filler_D;
    private DbsField target_Field2_New_Rqst_Pin_Nbr;
    private DbsField pnd_Update_Cnt;
    private DbsField pnd_Disp_Cnt;
    private DbsField pnd_Update_Cnt1;
    private DbsField pnd_Update_Cnt2;
    private DbsField pnd_Rec_Read_Cnt;
    private DbsField pnd_Rlte_Rqst_Id_Flag;
    private DbsField pnd_Rqst_Id_Flag;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_cwf_Master = new DataAccessProgramView(new NameInfo("vw_cwf_Master", "CWF-MASTER"), "CWF_MASTER_INDEX_VIEW", "CWF_MASTER_INDEX");
        cwf_Master_Rlte_Rqst_Id = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Rlte_Rqst_Id", "RLTE-RQST-ID", FieldType.STRING, 28, RepeatingFieldStrategy.None, 
            "RLTE_RQST_ID");

        cwf_Master__R_Field_1 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master__R_Field_1", "REDEFINE", cwf_Master_Rlte_Rqst_Id);
        cwf_Master_Rlte_Work_Prcss_Id = cwf_Master__R_Field_1.newFieldInGroup("cwf_Master_Rlte_Work_Prcss_Id", "RLTE-WORK-PRCSS-ID", FieldType.STRING, 
            6);
        cwf_Master_Rlte_Tiaa_Rcvd_Dte = cwf_Master__R_Field_1.newFieldInGroup("cwf_Master_Rlte_Tiaa_Rcvd_Dte", "RLTE-TIAA-RCVD-DTE", FieldType.STRING, 
            8);
        cwf_Master_Rlte_Case_Id_Cde = cwf_Master__R_Field_1.newFieldInGroup("cwf_Master_Rlte_Case_Id_Cde", "RLTE-CASE-ID-CDE", FieldType.STRING, 2);
        cwf_Master_Rlte_Pin_Nbr = cwf_Master__R_Field_1.newFieldInGroup("cwf_Master_Rlte_Pin_Nbr", "RLTE-PIN-NBR", FieldType.NUMERIC, 7);
        cwf_Master_Filler_A = cwf_Master__R_Field_1.newFieldInGroup("cwf_Master_Filler_A", "FILLER-A", FieldType.STRING, 5);
        cwf_Master_Rqst_Id = vw_cwf_Master.getRecord().newFieldInGroup("cwf_Master_Rqst_Id", "RQST-ID", FieldType.STRING, 28, RepeatingFieldStrategy.None, 
            "RQST_ID");
        cwf_Master_Rqst_Id.setDdmHeader("REQUEST ID");

        cwf_Master__R_Field_2 = vw_cwf_Master.getRecord().newGroupInGroup("cwf_Master__R_Field_2", "REDEFINE", cwf_Master_Rqst_Id);
        cwf_Master_Rqst_Work_Prcss_Id = cwf_Master__R_Field_2.newFieldInGroup("cwf_Master_Rqst_Work_Prcss_Id", "RQST-WORK-PRCSS-ID", FieldType.STRING, 
            6);
        cwf_Master_Rqst_Tiaa_Rcvd_Dte = cwf_Master__R_Field_2.newFieldInGroup("cwf_Master_Rqst_Tiaa_Rcvd_Dte", "RQST-TIAA-RCVD-DTE", FieldType.STRING, 
            8);
        cwf_Master_Rqst_Case_Id_Cde = cwf_Master__R_Field_2.newFieldInGroup("cwf_Master_Rqst_Case_Id_Cde", "RQST-CASE-ID-CDE", FieldType.STRING, 2);
        cwf_Master_Rqst_Pin_Nbr = cwf_Master__R_Field_2.newFieldInGroup("cwf_Master_Rqst_Pin_Nbr", "RQST-PIN-NBR", FieldType.NUMERIC, 7);
        cwf_Master_Filler_C = cwf_Master__R_Field_2.newFieldInGroup("cwf_Master_Filler_C", "FILLER-C", FieldType.STRING, 5);
        registerRecord(vw_cwf_Master);

        target_Field = localVariables.newFieldInRecord("target_Field", "TARGET-FIELD", FieldType.STRING, 28);

        target_Field__R_Field_3 = localVariables.newGroupInRecord("target_Field__R_Field_3", "REDEFINE", target_Field);
        target_Field_New_Rlte_Work_Prcss_Id = target_Field__R_Field_3.newFieldInGroup("target_Field_New_Rlte_Work_Prcss_Id", "NEW-RLTE-WORK-PRCSS-ID", 
            FieldType.STRING, 6);
        target_Field_New_Rlte_Tiaa_Rcvd_Dte = target_Field__R_Field_3.newFieldInGroup("target_Field_New_Rlte_Tiaa_Rcvd_Dte", "NEW-RLTE-TIAA-RCVD-DTE", 
            FieldType.STRING, 8);
        target_Field_New_Rlte_Case_Id_Cde = target_Field__R_Field_3.newFieldInGroup("target_Field_New_Rlte_Case_Id_Cde", "NEW-RLTE-CASE-ID-CDE", FieldType.STRING, 
            2);
        target_Field_Filler_B = target_Field__R_Field_3.newFieldInGroup("target_Field_Filler_B", "FILLER-B", FieldType.STRING, 5);
        target_Field_New_Rlte_Pin_Nbr = target_Field__R_Field_3.newFieldInGroup("target_Field_New_Rlte_Pin_Nbr", "NEW-RLTE-PIN-NBR", FieldType.NUMERIC, 
            7);
        target_Field2 = localVariables.newFieldInRecord("target_Field2", "TARGET-FIELD2", FieldType.STRING, 28);

        target_Field2__R_Field_4 = localVariables.newGroupInRecord("target_Field2__R_Field_4", "REDEFINE", target_Field2);
        target_Field2_New_Rqst_Work_Prcss_Id = target_Field2__R_Field_4.newFieldInGroup("target_Field2_New_Rqst_Work_Prcss_Id", "NEW-RQST-WORK-PRCSS-ID", 
            FieldType.STRING, 6);
        target_Field2_New_Rqst_Tiaa_Rcvd_Dte = target_Field2__R_Field_4.newFieldInGroup("target_Field2_New_Rqst_Tiaa_Rcvd_Dte", "NEW-RQST-TIAA-RCVD-DTE", 
            FieldType.STRING, 8);
        target_Field2_New_Rqst_Case_Id_Cde = target_Field2__R_Field_4.newFieldInGroup("target_Field2_New_Rqst_Case_Id_Cde", "NEW-RQST-CASE-ID-CDE", FieldType.STRING, 
            2);
        target_Field2_Filler_D = target_Field2__R_Field_4.newFieldInGroup("target_Field2_Filler_D", "FILLER-D", FieldType.STRING, 5);
        target_Field2_New_Rqst_Pin_Nbr = target_Field2__R_Field_4.newFieldInGroup("target_Field2_New_Rqst_Pin_Nbr", "NEW-RQST-PIN-NBR", FieldType.NUMERIC, 
            7);
        pnd_Update_Cnt = localVariables.newFieldInRecord("pnd_Update_Cnt", "#UPDATE-CNT", FieldType.NUMERIC, 9);
        pnd_Disp_Cnt = localVariables.newFieldInRecord("pnd_Disp_Cnt", "#DISP-CNT", FieldType.NUMERIC, 9);
        pnd_Update_Cnt1 = localVariables.newFieldInRecord("pnd_Update_Cnt1", "#UPDATE-CNT1", FieldType.NUMERIC, 9);
        pnd_Update_Cnt2 = localVariables.newFieldInRecord("pnd_Update_Cnt2", "#UPDATE-CNT2", FieldType.NUMERIC, 9);
        pnd_Rec_Read_Cnt = localVariables.newFieldInRecord("pnd_Rec_Read_Cnt", "#REC-READ-CNT", FieldType.NUMERIC, 9);
        pnd_Rlte_Rqst_Id_Flag = localVariables.newFieldInRecord("pnd_Rlte_Rqst_Id_Flag", "#RLTE-RQST-ID-FLAG", FieldType.BOOLEAN, 1);
        pnd_Rqst_Id_Flag = localVariables.newFieldInRecord("pnd_Rqst_Id_Flag", "#RQST-ID-FLAG", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cwf_Master.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cwfpin01() throws Exception
    {
        super("Cwfpin01");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        vw_cwf_Master.startDatabaseRead                                                                                                                                   //Natural: READ CWF-MASTER IN PHYSICAL SEQUENCE
        (
        "READ_FILE",
        new Oc[] { new Oc("ISN", "ASC") }
        );
        READ_FILE:
        while (condition(vw_cwf_Master.readNextRow("READ_FILE")))
        {
            pnd_Rlte_Rqst_Id_Flag.reset();                                                                                                                                //Natural: RESET #RLTE-RQST-ID-FLAG #RQST-ID-FLAG
            pnd_Rqst_Id_Flag.reset();
            pnd_Rec_Read_Cnt.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #REC-READ-CNT
            if (condition(cwf_Master_Rlte_Rqst_Id.notEquals(" ")))                                                                                                        //Natural: IF RLTE-RQST-ID NE ' '
            {
                if (condition(cwf_Master_Filler_A.equals(" ")))                                                                                                           //Natural: IF FILLER-A EQ ' '
                {
                    if (condition(DbsUtil.maskMatches(cwf_Master_Rlte_Pin_Nbr,"NNNNNNN")))                                                                                //Natural: IF RLTE-PIN-NBR = MASK ( NNNNNNN )
                    {
                        if (condition(cwf_Master_Rlte_Pin_Nbr.greater(getZero())))                                                                                        //Natural: IF RLTE-PIN-NBR > 0
                        {
                            target_Field_New_Rlte_Work_Prcss_Id.setValue(cwf_Master_Rlte_Work_Prcss_Id);                                                                  //Natural: MOVE RLTE-WORK-PRCSS-ID TO NEW-RLTE-WORK-PRCSS-ID
                            target_Field_New_Rlte_Tiaa_Rcvd_Dte.setValue(cwf_Master_Rlte_Tiaa_Rcvd_Dte);                                                                  //Natural: MOVE RLTE-TIAA-RCVD-DTE TO NEW-RLTE-TIAA-RCVD-DTE
                            target_Field_New_Rlte_Case_Id_Cde.setValue(cwf_Master_Rlte_Case_Id_Cde);                                                                      //Natural: MOVE RLTE-CASE-ID-CDE TO NEW-RLTE-CASE-ID-CDE
                            target_Field_Filler_B.setValue("00000");                                                                                                      //Natural: MOVE '00000' TO FILLER-B
                            target_Field_New_Rlte_Pin_Nbr.setValue(cwf_Master_Rlte_Pin_Nbr);                                                                              //Natural: MOVE RLTE-PIN-NBR TO NEW-RLTE-PIN-NBR
                            pnd_Rlte_Rqst_Id_Flag.setValue(true);                                                                                                         //Natural: MOVE TRUE TO #RLTE-RQST-ID-FLAG
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cwf_Master_Rqst_Id.notEquals(" ")))                                                                                                             //Natural: IF RQST-ID NE ' '
            {
                if (condition(cwf_Master_Filler_C.equals(" ")))                                                                                                           //Natural: IF FILLER-C EQ ' '
                {
                    if (condition(DbsUtil.maskMatches(cwf_Master_Rqst_Pin_Nbr,"NNNNNNN")))                                                                                //Natural: IF RQST-PIN-NBR = MASK ( NNNNNNN )
                    {
                        target_Field2_Filler_D.setValue("00000");                                                                                                         //Natural: MOVE '00000' TO FILLER-D
                        target_Field2_New_Rqst_Work_Prcss_Id.setValue(cwf_Master_Rqst_Work_Prcss_Id);                                                                     //Natural: MOVE RQST-WORK-PRCSS-ID TO NEW-RQST-WORK-PRCSS-ID
                        target_Field2_New_Rqst_Tiaa_Rcvd_Dte.setValue(cwf_Master_Rqst_Tiaa_Rcvd_Dte);                                                                     //Natural: MOVE RQST-TIAA-RCVD-DTE TO NEW-RQST-TIAA-RCVD-DTE
                        target_Field2_New_Rqst_Case_Id_Cde.setValue(cwf_Master_Rqst_Case_Id_Cde);                                                                         //Natural: MOVE RQST-CASE-ID-CDE TO NEW-RQST-CASE-ID-CDE
                        target_Field2_New_Rqst_Pin_Nbr.setValue(cwf_Master_Rqst_Pin_Nbr);                                                                                 //Natural: MOVE RQST-PIN-NBR TO NEW-RQST-PIN-NBR
                        pnd_Rqst_Id_Flag.setValue(true);                                                                                                                  //Natural: MOVE TRUE TO #RQST-ID-FLAG
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Rlte_Rqst_Id_Flag.getBoolean() || pnd_Rqst_Id_Flag.getBoolean()))                                                                           //Natural: IF #RLTE-RQST-ID-FLAG OR #RQST-ID-FLAG
            {
                GET_ISN:                                                                                                                                                  //Natural: GET CWF-MASTER *ISN ( READ-FILE. )
                vw_cwf_Master.readByID(vw_cwf_Master.getAstISN("READ_FILE"), "GET_ISN");
                if (condition(pnd_Rlte_Rqst_Id_Flag.getBoolean()))                                                                                                        //Natural: IF #RLTE-RQST-ID-FLAG
                {
                    cwf_Master_Rlte_Rqst_Id.moveAll(target_Field);                                                                                                        //Natural: MOVE ALL TARGET-FIELD TO RLTE-RQST-ID
                    pnd_Update_Cnt1.nadd(1);                                                                                                                              //Natural: ASSIGN #UPDATE-CNT1 := #UPDATE-CNT1 + 1
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Rqst_Id_Flag.getBoolean()))                                                                                                             //Natural: IF #RQST-ID-FLAG
                {
                    cwf_Master_Rqst_Id.moveAll(target_Field2);                                                                                                            //Natural: MOVE ALL TARGET-FIELD2 TO RQST-ID
                    pnd_Update_Cnt2.nadd(1);                                                                                                                              //Natural: ASSIGN #UPDATE-CNT2 := #UPDATE-CNT2 + 1
                }                                                                                                                                                         //Natural: END-IF
                pnd_Update_Cnt.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #UPDATE-CNT
                pnd_Disp_Cnt.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #DISP-CNT
                vw_cwf_Master.updateDBRow("GET_ISN");                                                                                                                     //Natural: UPDATE ( GET-ISN. )
                if (condition(pnd_Update_Cnt1.greater(5000) || pnd_Update_Cnt2.greater(5000)))                                                                            //Natural: IF #UPDATE-CNT1 > 5000 OR #UPDATE-CNT2 > 5000
                {
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION
                    pnd_Update_Cnt1.reset();                                                                                                                              //Natural: RESET #UPDATE-CNT1 #UPDATE-CNT2
                    pnd_Update_Cnt2.reset();
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Disp_Cnt.greater(1000000)))                                                                                                             //Natural: IF #DISP-CNT > 1000000
                {
                    getReports().write(0, "No of Records updated so far:",pnd_Update_Cnt);                                                                                //Natural: WRITE 'No of Records updated so far:' #UPDATE-CNT
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_FILE"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_FILE"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Disp_Cnt.reset();                                                                                                                                 //Natural: RESET #DISP-CNT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        getReports().write(0, "Total Read count: ",pnd_Rec_Read_Cnt);                                                                                                     //Natural: WRITE 'Total Read count: '#REC-READ-CNT
        if (Global.isEscape()) return;
        getReports().write(0, "Updated Record count: ",pnd_Update_Cnt);                                                                                                   //Natural: WRITE 'Updated Record count: '#UPDATE-CNT
        if (Global.isEscape()) return;
    }

    //
}
