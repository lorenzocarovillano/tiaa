/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:45:45 PM
**        *   FROM NATURAL MAP   :  Efsf9040
************************************************************
**        * FILE NAME               : Efsf9040.java
**        * CLASS NAME              : Efsf9040
**        * INSTANCE NAME           : Efsf9040
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     #PROGRAM
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsf9040 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Program;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Program = parameters.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        parameters.reset();
    }

    public Efsf9040() throws Exception
    {
        super("Efsf9040");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=005 LS=132 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Efsf9040", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Efsf9040"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Program", pnd_Program, true, 1, 1, 8, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_1", "CWF - CONTROL MODULE TRANSACTIONS AUDIT LIST", "BLUE", 1, 23, 44);
            uiForm.setUiLabel("label_2", "AS OF", "BLUE", 1, 78, 5);
            uiForm.setUiControl("astDATU", Global.getDATU(), true, 1, 84, 8, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("astTIME", Global.getTIME(), true, 1, 93, 10, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_3", "PAGE:", "BLUE", 1, 100, 5);
            uiForm.setUiControl("astPAGE_NUMBER", getReports().getPageNumberDbs(0), true, 1, 106, 5, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_4", "-----------------------------------------------------------------------------------------------------------------------------------", 
                "", 2, 1, 131);
            uiForm.setUiLabel("label_5", "AUDIT RECORD", "BLUE", 3, 3, 12);
            uiForm.setUiLabel("label_6", "CALLER", "BLUE", 3, 23, 6);
            uiForm.setUiLabel("label_7", "EMPLOYEE ELAPSED", "BLUE", 3, 35, 16);
            uiForm.setUiLabel("label_8", "RECUR ERR", "BLUE", 3, 53, 9);
            uiForm.setUiLabel("label_9", "MASTER INDEX", "BLUE", 3, 64, 12);
            uiForm.setUiLabel("label_10", "FOLDERS", "BLUE", 3, 81, 7);
            uiForm.setUiLabel("label_11", "DOCUMENTS", "BLUE", 3, 95, 9);
            uiForm.setUiLabel("label_12", "PIN", "BLUE", 3, 110, 3);
            uiForm.setUiLabel("label_13", "RQST", "BLUE", 3, 116, 4);
            uiForm.setUiLabel("label_14", "LOG DATE / TIME", "BLUE", 4, 2, 15);
            uiForm.setUiLabel("label_15", "SYSTEM ACTN OPERATOR XTN TIME", "BLUE", 4, 23, 29);
            uiForm.setUiLabel("label_16", "IND", "BLUE", 4, 54, 3);
            uiForm.setUiLabel("label_17", "CD", "BLUE", 4, 59, 2);
            uiForm.setUiLabel("label_18", "ADDs", "BLUE", 4, 65, 4);
            uiForm.setUiLabel("label_19", "UPDs", "BLUE", 4, 71, 4);
            uiForm.setUiLabel("label_20", "ADDs", "BLUE", 4, 77, 4);
            uiForm.setUiLabel("label_21", "UPDs", "BLUE", 4, 83, 4);
            uiForm.setUiLabel("label_22", "DELs", "BLUE", 4, 89, 4);
            uiForm.setUiLabel("label_23", "ADDs RENs", "BLUE", 4, 95, 9);
            uiForm.setUiLabel("label_24", "NBR", "BLUE", 4, 110, 3);
            uiForm.setUiLabel("label_25", "ID", "BLUE", 4, 117, 2);
            uiForm.setUiLabel("label_26", "-----------------------------------------------------------------------------------------------------------------------------------", 
                "", 5, 1, 131);
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
