/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:45:44 PM
**        *   FROM NATURAL MAP   :  Efsf9031
************************************************************
**        * FILE NAME               : Efsf9031.java
**        * CLASS NAME              : Efsf9031
**        * INSTANCE NAME           : Efsf9031
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     #PROGRAM
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsf9031 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Program;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Program = parameters.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        parameters.reset();
    }

    public Efsf9031() throws Exception
    {
        super("Efsf9031");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=005 LS=132 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Efsf9031", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Efsf9031"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Program", pnd_Program, true, 1, 1, 8, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_1", "CWF - ERROR RETURNS FROM CONTROL MODULE COMMITTED BY", "BLUE", 1, 14, 52);
            uiForm.setUiLabel("label_2", "APPLICATIONS", "BLUE", 1, 67, 12);
            uiForm.setUiLabel("label_3", "AS OF", "BLUE", 1, 85, 5);
            uiForm.setUiControl("astDATU", Global.getDATU(), true, 1, 91, 8, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("astTIME", Global.getTIME(), true, 1, 100, 10, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_4", "PAGE:", "BLUE", 1, 107, 5);
            uiForm.setUiControl("astPAGE_NUMBER", getReports().getPageNumberDbs(0), true, 1, 113, 5, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_5", "-----------------------------------------------------------------------------------------------------------------------------------", 
                "", 2, 1, 131);
            uiForm.setUiLabel("label_6", "AUDIT RECORD", "BLUE", 3, 3, 12);
            uiForm.setUiLabel("label_7", "CALLER", "BLUE", 3, 23, 6);
            uiForm.setUiLabel("label_8", "EMPLOYEE ELAPSED", "BLUE", 3, 35, 16);
            uiForm.setUiLabel("label_9", "RECUR ERROR", "BLUE", 3, 53, 11);
            uiForm.setUiLabel("label_10", "MASTER INDEX", "BLUE", 3, 66, 12);
            uiForm.setUiLabel("label_11", "FOLDERS", "BLUE", 3, 83, 7);
            uiForm.setUiLabel("label_12", "DOCUMENTS", "BLUE", 3, 97, 9);
            uiForm.setUiLabel("label_13", "PIN", "BLUE", 3, 112, 3);
            uiForm.setUiLabel("label_14", "RQST", "BLUE", 3, 118, 4);
            uiForm.setUiLabel("label_15", "LOG DATE / TIME", "BLUE", 4, 2, 15);
            uiForm.setUiLabel("label_16", "SYSTEM ACTN OPERATOR XTN TIME", "BLUE", 4, 23, 29);
            uiForm.setUiLabel("label_17", "IND", "BLUE", 4, 54, 3);
            uiForm.setUiLabel("label_18", "CODE", "BLUE", 4, 59, 4);
            uiForm.setUiLabel("label_19", "ADDs", "BLUE", 4, 67, 4);
            uiForm.setUiLabel("label_20", "UPDs", "BLUE", 4, 73, 4);
            uiForm.setUiLabel("label_21", "ADDs", "BLUE", 4, 79, 4);
            uiForm.setUiLabel("label_22", "UPDs", "BLUE", 4, 85, 4);
            uiForm.setUiLabel("label_23", "DELs", "BLUE", 4, 91, 4);
            uiForm.setUiLabel("label_24", "ADDs RENs", "BLUE", 4, 97, 9);
            uiForm.setUiLabel("label_25", "NBR", "BLUE", 4, 112, 3);
            uiForm.setUiLabel("label_26", "ID", "BLUE", 4, 119, 2);
            uiForm.setUiLabel("label_27", "-----------------------------------------------------------------------------------------------------------------------------------", 
                "", 5, 1, 131);
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
