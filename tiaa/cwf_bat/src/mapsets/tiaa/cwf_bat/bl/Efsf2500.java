/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:45:35 PM
**        *   FROM NATURAL MAP   :  Efsf2500
************************************************************
**        * FILE NAME               : Efsf2500.java
**        * CLASS NAME              : Efsf2500
**        * INSTANCE NAME           : Efsf2500
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     #REPORT-VARS.#RPT-UNIT-CODE #REPORT-VARS.#RPT-UNIT-DESC
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsf2500 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Report_Vars_Pnd_Rpt_Unit_Code;
    private DbsField pnd_Report_Vars_Pnd_Rpt_Unit_Desc;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Report_Vars_Pnd_Rpt_Unit_Code = parameters.newFieldInRecord("pnd_Report_Vars_Pnd_Rpt_Unit_Code", "#REPORT-VARS.#RPT-UNIT-CODE", FieldType.STRING, 
            8);
        pnd_Report_Vars_Pnd_Rpt_Unit_Desc = parameters.newFieldInRecord("pnd_Report_Vars_Pnd_Rpt_Unit_Desc", "#REPORT-VARS.#RPT-UNIT-DESC", FieldType.STRING, 
            80);
        parameters.reset();
    }

    public Efsf2500() throws Exception
    {
        super("Efsf2500");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Efsf2500", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Efsf2500"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("astPROGRAM", Global.getPROGRAM(), true, 1, 1, 8, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_1", "INCOMPLETE WORKSHEET REPORT FOR PARTICIPANT COMPLETED WORK", "BLUE", 1, 37, 58);
            uiForm.setUiLabel("label_2", "PAGE:", "BLUE", 1, 120, 5);
            uiForm.setUiControl("astPAGE_NUMBER", getReports().getPageNumberDbs(0), true, 1, 126, 5, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_3", "AS OF", "BLUE", 2, 58, 5);
            uiForm.setUiControl("astDATU", Global.getDATU(), true, 2, 65, 8, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_4", "UNIT :", "BLUE", 4, 5, 6);
            uiForm.setUiControl("pnd_Report_Vars_Pnd_Rpt_Unit_Code", pnd_Report_Vars_Pnd_Rpt_Unit_Code, true, 4, 12, 8, "WHITE", true, false, null, null, 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_5", "-", "BLUE", 4, 21, 1);
            uiForm.setUiControl("pnd_Report_Vars_Pnd_Rpt_Unit_Desc", pnd_Report_Vars_Pnd_Rpt_Unit_Desc, true, 4, 23, 80, "WHITE", true, false, null, null, 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_6", "-----------------------------------------------------------------------------------------------------------------------------------", 
                "", 6, 1, 131);
            uiForm.setUiLabel("label_7", "PHYSICAL", "BLUE", 7, 17, 8);
            uiForm.setUiLabel("label_8", "TIAA", "BLUE", 7, 33, 4);
            uiForm.setUiLabel("label_9", "CORP.", "BLUE", 7, 63, 5);
            uiForm.setUiLabel("label_10", "PIN NO.", "BLUE", 8, 2, 7);
            uiForm.setUiLabel("label_11", "FOLDER ID", "BLUE", 8, 17, 9);
            uiForm.setUiLabel("label_12", "RECVD DATE", "BLUE", 8, 30, 10);
            uiForm.setUiLabel("label_13", "WPID DESC", "BLUE", 8, 45, 9);
            uiForm.setUiLabel("label_14", "STATUS", "BLUE", 8, 62, 6);
            uiForm.setUiLabel("label_15", "DOCUMENT DESC", "BLUE", 8, 72, 13);
            uiForm.setUiLabel("label_16", "WORKSHEET CREATED BY", "BLUE", 8, 103, 20);
            uiForm.setUiLabel("label_17", "-----------------------------------------------------------------------------------------------------------------------------------", 
                "", 9, 1, 131);
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
