/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:45:30 PM
**        *   FROM NATURAL MAP   :  Efsf0068
************************************************************
**        * FILE NAME               : Efsf0068.java
**        * CLASS NAME              : Efsf0068
**        * INSTANCE NAME           : Efsf0068
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #INPUT-DATE-D #INPUT-SOURCE-ID #PROGRAM
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsf0068 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Input_Date_D;
    private DbsField pnd_Input_Source_Id;
    private DbsField pnd_Program;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Input_Date_D = parameters.newFieldInRecord("pnd_Input_Date_D", "#INPUT-DATE-D", FieldType.DATE);
        pnd_Input_Source_Id = parameters.newFieldInRecord("pnd_Input_Source_Id", "#INPUT-SOURCE-ID", FieldType.STRING, 6);
        pnd_Program = parameters.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        parameters.reset();
    }

    public Efsf0068() throws Exception
    {
        super("Efsf0068");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Efsf0068", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Efsf0068"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Program", pnd_Program, true, 1, 1, 8, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_1", "CWF IMAGE UPLOAD EXCEPTION - OUTSTANDING BATCHES NOT PROCESSED ON", "BLUE", 1, 22, 65);
            uiForm.setUiControl("pnd_Input_Date_D", pnd_Input_Date_D, true, 1, 88, 10, "WHITE", "MM/DD/YYYY", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_2", "FOR", "BLUE", 1, 99, 3);
            uiForm.setUiControl("pnd_Input_Source_Id", pnd_Input_Source_Id, true, 1, 103, 6, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_3", "PAGE:", "BLUE", 1, 121, 5);
            uiForm.setUiControl("astPAGE_NUMBER", getReports().getPageNumberDbs(0), true, 1, 127, 5, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_4", "AS OF", "BLUE", 2, 56, 5);
            uiForm.setUiControl("astDATU", Global.getDATU(), true, 2, 62, 8, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("astTIME", Global.getTIME(), true, 2, 71, 10, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_5", "-----------------------------------------------------------------------------------------------------------------------------------", 
                "", 3, 1, 131);
            uiForm.setUiLabel("label_6", "BATCH", "BLUE", 4, 3, 5);
            uiForm.setUiLabel("label_7", "BATCH", "BLUE", 4, 10, 5);
            uiForm.setUiLabel("label_8", "BATCH", "BLUE", 4, 17, 5);
            uiForm.setUiLabel("label_9", "DATE & TIME", "BLUE", 4, 40, 11);
            uiForm.setUiLabel("label_10", "NUMBER", "BLUE", 4, 56, 6);
            uiForm.setUiLabel("label_11", "NUMBER OF", "BLUE", 4, 67, 9);
            uiForm.setUiLabel("label_12", "NUMBER OF MINS", "BLUE", 4, 80, 14);
            uiForm.setUiLabel("label_13", "NUMBER OF", "BLUE", 4, 99, 9);
            uiForm.setUiLabel("label_14", "ELAPSED DAYS", "BLUE", 4, 115, 12);
            uiForm.setUiLabel("label_15", "NUMBER EXTN", "BLUE", 5, 3, 11);
            uiForm.setUiLabel("label_16", "LABEL", "BLUE", 5, 17, 5);
            uiForm.setUiLabel("label_17", "UPLOAD COMPLETED", "BLUE", 5, 38, 16);
            uiForm.setUiLabel("label_18", "OF MINS", "BLUE", 5, 56, 7);
            uiForm.setUiLabel("label_19", "MINS UPLOADED", "BLUE", 5, 65, 13);
            uiForm.setUiLabel("label_20", "OUTSTANDING", "BLUE", 5, 81, 11);
            uiForm.setUiLabel("label_21", "IMAGES PROCESSED", "BLUE", 5, 96, 16);
            uiForm.setUiLabel("label_22", "FROM FIRST UPLOAD", "BLUE", 5, 115, 17);
            uiForm.setUiLabel("label_23", "-----------------------------------------------------------------------------------------------------------------------------------", 
                "", 6, 1, 131);
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
