/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:45:36 PM
**        *   FROM NATURAL MAP   :  Efsf2501
************************************************************
**        * FILE NAME               : Efsf2501.java
**        * CLASS NAME              : Efsf2501
**        * INSTANCE NAME           : Efsf2501
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     #REPORT-VARS.#RPT-CORP-STATUS #REPORT-VARS.#RPT-DOC-DESC                                                                 *     #REPORT-VARS.#RPT-ENTRY-OPRTR-NAME 
    *     #REPORT-VARS.#RPT-PHYSICAL-FOLDER-ID #REPORT-VARS.#RPT-PIN-NO                                                            *     #REPORT-VARS.#RPT-TIAA-RCVD-DTE 
    #REPORT-VARS.#RPT-WPID-DESC
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsf2501 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Report_Vars_Pnd_Rpt_Corp_Status;
    private DbsField pnd_Report_Vars_Pnd_Rpt_Doc_Desc;
    private DbsField pnd_Report_Vars_Pnd_Rpt_Entry_Oprtr_Name;
    private DbsField pnd_Report_Vars_Pnd_Rpt_Physical_Folder_Id;
    private DbsField pnd_Report_Vars_Pnd_Rpt_Pin_No;
    private DbsField pnd_Report_Vars_Pnd_Rpt_Tiaa_Rcvd_Dte;
    private DbsField pnd_Report_Vars_Pnd_Rpt_Wpid_Desc;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Report_Vars_Pnd_Rpt_Corp_Status = parameters.newFieldInRecord("pnd_Report_Vars_Pnd_Rpt_Corp_Status", "#REPORT-VARS.#RPT-CORP-STATUS", FieldType.STRING, 
            6);
        pnd_Report_Vars_Pnd_Rpt_Doc_Desc = parameters.newFieldInRecord("pnd_Report_Vars_Pnd_Rpt_Doc_Desc", "#REPORT-VARS.#RPT-DOC-DESC", FieldType.STRING, 
            30);
        pnd_Report_Vars_Pnd_Rpt_Entry_Oprtr_Name = parameters.newFieldInRecord("pnd_Report_Vars_Pnd_Rpt_Entry_Oprtr_Name", "#REPORT-VARS.#RPT-ENTRY-OPRTR-NAME", 
            FieldType.STRING, 30);
        pnd_Report_Vars_Pnd_Rpt_Physical_Folder_Id = parameters.newFieldInRecord("pnd_Report_Vars_Pnd_Rpt_Physical_Folder_Id", "#REPORT-VARS.#RPT-PHYSICAL-FOLDER-ID", 
            FieldType.STRING, 7);
        pnd_Report_Vars_Pnd_Rpt_Pin_No = parameters.newFieldInRecord("pnd_Report_Vars_Pnd_Rpt_Pin_No", "#REPORT-VARS.#RPT-PIN-NO", FieldType.NUMERIC, 
            12);
        pnd_Report_Vars_Pnd_Rpt_Tiaa_Rcvd_Dte = parameters.newFieldInRecord("pnd_Report_Vars_Pnd_Rpt_Tiaa_Rcvd_Dte", "#REPORT-VARS.#RPT-TIAA-RCVD-DTE", 
            FieldType.STRING, 8);
        pnd_Report_Vars_Pnd_Rpt_Wpid_Desc = parameters.newFieldInRecord("pnd_Report_Vars_Pnd_Rpt_Wpid_Desc", "#REPORT-VARS.#RPT-WPID-DESC", FieldType.STRING, 
            15);
        parameters.reset();
    }

    public Efsf2501() throws Exception
    {
        super("Efsf2501");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=060 LS=132 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Efsf2501", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Efsf2501"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Report_Vars_Pnd_Rpt_Pin_No", pnd_Report_Vars_Pnd_Rpt_Pin_No, true, 1, 2, 12, "WHITE", true, false, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Report_Vars_Pnd_Rpt_Physical_Folder_Id", pnd_Report_Vars_Pnd_Rpt_Physical_Folder_Id, true, 1, 17, 7, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Report_Vars_Pnd_Rpt_Tiaa_Rcvd_Dte", pnd_Report_Vars_Pnd_Rpt_Tiaa_Rcvd_Dte, true, 1, 28, 8, "WHITE", true, false, 
                null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Report_Vars_Pnd_Rpt_Wpid_Desc", pnd_Report_Vars_Pnd_Rpt_Wpid_Desc, true, 1, 39, 15, "WHITE", true, false, null, null, 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Report_Vars_Pnd_Rpt_Corp_Status", pnd_Report_Vars_Pnd_Rpt_Corp_Status, true, 1, 56, 6, "WHITE", true, false, null, 
                null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Report_Vars_Pnd_Rpt_Doc_Desc", pnd_Report_Vars_Pnd_Rpt_Doc_Desc, true, 1, 64, 30, "WHITE", true, false, null, null, 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Report_Vars_Pnd_Rpt_Entry_Oprtr_Name", pnd_Report_Vars_Pnd_Rpt_Entry_Oprtr_Name, true, 1, 98, 30, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
