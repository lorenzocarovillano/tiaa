/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:45:37 PM
**        *   FROM NATURAL MAP   :  Efsf7064
************************************************************
**        * FILE NAME               : Efsf7064.java
**        * CLASS NAME              : Efsf7064
**        * INSTANCE NAME           : Efsf7064
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #END-DT #IMAGE-PLATTER-OLD #LAST-DT #SUMMARY.#BATCH-COUNT                                                                *     #SUMMARY.#IMAGES-COUNT 
    #SUMMARY.#MINS-COUNT
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsf7064 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_End_Dt;
    private DbsField pnd_Image_Platter_Old;
    private DbsField pnd_Last_Dt;
    private DbsField pnd_Summary_Pnd_Batch_Count;
    private DbsField pnd_Summary_Pnd_Images_Count;
    private DbsField pnd_Summary_Pnd_Mins_Count;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_End_Dt = parameters.newFieldInRecord("pnd_End_Dt", "#END-DT", FieldType.STRING, 8);
        pnd_Image_Platter_Old = parameters.newFieldInRecord("pnd_Image_Platter_Old", "#IMAGE-PLATTER-OLD", FieldType.STRING, 10);
        pnd_Last_Dt = parameters.newFieldInRecord("pnd_Last_Dt", "#LAST-DT", FieldType.STRING, 8);
        pnd_Summary_Pnd_Batch_Count = parameters.newFieldInRecord("pnd_Summary_Pnd_Batch_Count", "#SUMMARY.#BATCH-COUNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Summary_Pnd_Images_Count = parameters.newFieldInRecord("pnd_Summary_Pnd_Images_Count", "#SUMMARY.#IMAGES-COUNT", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Summary_Pnd_Mins_Count = parameters.newFieldInRecord("pnd_Summary_Pnd_Mins_Count", "#SUMMARY.#MINS-COUNT", FieldType.PACKED_DECIMAL, 7);
        parameters.reset();
    }

    public Efsf7064() throws Exception
    {
        super("Efsf7064");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Efsf7064", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Efsf7064"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Last_Dt", pnd_Last_Dt, true, 1, 4, 8, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_End_Dt", pnd_End_Dt, true, 1, 20, 8, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Summary_Pnd_Batch_Count", pnd_Summary_Pnd_Batch_Count, true, 1, 41, 6, "WHITE", "ZZ,ZZ9", true, false, null, "0123456789+-, ", 
                "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Summary_Pnd_Mins_Count", pnd_Summary_Pnd_Mins_Count, true, 1, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, false, null, "0123456789+-, ", 
                "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Summary_Pnd_Images_Count", pnd_Summary_Pnd_Images_Count, true, 1, 84, 9, "WHITE", "Z,ZZZ,ZZ9", true, false, null, 
                "0123456789+-, ", "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Image_Platter_Old", pnd_Image_Platter_Old, true, 1, 105, 10, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
