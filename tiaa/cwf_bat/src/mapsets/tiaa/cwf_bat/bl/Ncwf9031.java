/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:55:12 PM
**        *   FROM NATURAL MAP   :  Ncwf9031
************************************************************
**        * FILE NAME               : Ncwf9031.java
**        * CLASS NAME              : Ncwf9031
**        * INSTANCE NAME           : Ncwf9031
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #PROGRAM
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Ncwf9031 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Program;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Program = parameters.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        parameters.reset();
    }

    public Ncwf9031() throws Exception
    {
        super("Ncwf9031");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=005 LS=132 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Ncwf9031", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Ncwf9031"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Program", pnd_Program, true, 1, 1, 8, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_1", "ICW - ERROR RETURNS FROM CONTROL MODULE COMMITTED BY APPLICATIONS", "BLUE", 1, 14, 65);
            uiForm.setUiLabel("label_2", "AS OF", "BLUE", 1, 85, 5);
            uiForm.setUiControl("astDATU", Global.getDATU(), true, 1, 91, 8, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("astTIME", Global.getTIME(), true, 1, 100, 10, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_3", "PAGE:", "BLUE", 1, 107, 5);
            uiForm.setUiControl("astPAGE_NUMBER", getReports().getPageNumberDbs(0), true, 1, 113, 5, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_4", "-----------------------------------------------------------------------------------------------------------------------------------", 
                "", 2, 1, 131);
            uiForm.setUiLabel("label_5", "AUDIT RECORD", "BLUE", 3, 3, 12);
            uiForm.setUiLabel("label_6", "CALLER", "BLUE", 3, 23, 6);
            uiForm.setUiLabel("label_7", "EMPLOYEE ELAPSED RECUR ERROR MASTER INDEX DOCUMENTS", "BLUE", 3, 35, 51);
            uiForm.setUiLabel("label_8", "CABINET", "BLUE", 3, 89, 7);
            uiForm.setUiLabel("label_9", "RQST", "BLUE", 3, 101, 4);
            uiForm.setUiLabel("label_10", "LOG DATE / TIME", "BLUE", 4, 2, 15);
            uiForm.setUiLabel("label_11", "SYSTEM ACTN OPERATOR XTN TIME IND", "BLUE", 4, 23, 33);
            uiForm.setUiLabel("label_12", "CODE", "BLUE", 4, 58, 4);
            uiForm.setUiLabel("label_13", "ADDs", "BLUE", 4, 64, 4);
            uiForm.setUiLabel("label_14", "UPDs", "BLUE", 4, 70, 4);
            uiForm.setUiLabel("label_15", "ADDs RENAMEs", "BLUE", 4, 76, 12);
            uiForm.setUiLabel("label_16", "ID", "BLUE", 4, 91, 2);
            uiForm.setUiLabel("label_17", "ID", "BLUE", 4, 102, 2);
            uiForm.setUiLabel("label_18", "-----------------------------------------------------------------------------------------------------------------------------------", 
                "", 5, 1, 131);
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
