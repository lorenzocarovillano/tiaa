/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:45:31 PM
**        *   FROM NATURAL MAP   :  Efsf0069
************************************************************
**        * FILE NAME               : Efsf0069.java
**        * CLASS NAME              : Efsf0069
**        * INSTANCE NAME           : Efsf0069
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #END-KEY #INPUT-SOURCE-ID #INPUT-TIME-END #INPUT-TIME-START                                                              *     #PROGRAM #START-KEY
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsf0069 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_End_Key;
    private DbsField pnd_Input_Source_Id;
    private DbsField pnd_Input_Time_End;
    private DbsField pnd_Input_Time_Start;
    private DbsField pnd_Program;
    private DbsField pnd_Start_Key;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_End_Key = parameters.newFieldInRecord("pnd_End_Key", "#END-KEY", FieldType.TIME);
        pnd_Input_Source_Id = parameters.newFieldInRecord("pnd_Input_Source_Id", "#INPUT-SOURCE-ID", FieldType.STRING, 6);
        pnd_Input_Time_End = parameters.newFieldInRecord("pnd_Input_Time_End", "#INPUT-TIME-END", FieldType.STRING, 6);
        pnd_Input_Time_Start = parameters.newFieldInRecord("pnd_Input_Time_Start", "#INPUT-TIME-START", FieldType.STRING, 6);
        pnd_Program = parameters.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Start_Key = parameters.newFieldInRecord("pnd_Start_Key", "#START-KEY", FieldType.TIME);
        parameters.reset();
    }

    public Efsf0069() throws Exception
    {
        super("Efsf0069");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Efsf0069", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Efsf0069"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Program", pnd_Program, true, 1, 1, 8, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_1", "CWF - IMAGE UPLOAD UNIDENTIFIED MINS REPORT FOR", "BLUE", 1, 17, 47);
            uiForm.setUiControl("pnd_Input_Source_Id", pnd_Input_Source_Id, true, 1, 65, 6, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Start_Key", pnd_Start_Key, true, 1, 72, 10, "WHITE", "MM/DD/YYYY", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Input_Time_Start", pnd_Input_Time_Start, true, 1, 83, 8, "WHITE", "XX':'XX':'XX", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_2", "TO", "BLUE", 1, 92, 2);
            uiForm.setUiControl("pnd_End_Key", pnd_End_Key, true, 1, 95, 10, "WHITE", "MM/DD/YYYY", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Input_Time_End", pnd_Input_Time_End, true, 1, 106, 8, "WHITE", "XX':'XX':'XX", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_3", "PAGE:", "BLUE", 1, 121, 5);
            uiForm.setUiControl("astPAGE_NUMBER", getReports().getPageNumberDbs(0), true, 1, 127, 5, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_4", "AS OF", "BLUE", 2, 56, 5);
            uiForm.setUiControl("astDATU", Global.getDATU(), true, 2, 62, 8, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("astTIME", Global.getTIME(), true, 2, 71, 10, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_5", "-----------------------------------------------------------------------------------------------------------------------------------", 
                "", 3, 1, 131);
            uiForm.setUiLabel("label_6", "BATCH", "BLUE", 4, 3, 5);
            uiForm.setUiLabel("label_7", "BATCH", "BLUE", 4, 10, 5);
            uiForm.setUiLabel("label_8", "BATCH", "BLUE", 4, 17, 5);
            uiForm.setUiLabel("label_9", "DATE & TIME", "BLUE", 4, 40, 11);
            uiForm.setUiLabel("label_10", "NUMBER", "BLUE", 4, 56, 6);
            uiForm.setUiLabel("label_11", "NUMBER EXTN", "BLUE", 5, 3, 11);
            uiForm.setUiLabel("label_12", "LABEL", "BLUE", 5, 17, 5);
            uiForm.setUiLabel("label_13", "UPLOAD COMPLETED", "BLUE", 5, 38, 16);
            uiForm.setUiLabel("label_14", "OF MINS", "BLUE", 5, 56, 7);
            uiForm.setUiLabel("label_15", "UNIDENTIFIED MINS / EXCEPTION MESSAGE", "BLUE", 5, 66, 37);
            uiForm.setUiLabel("label_16", "-----------------------------------------------------------------------------------------------------------------------------------", 
                "", 6, 1, 131);
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
