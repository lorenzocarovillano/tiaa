/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:45:39 PM
**        *   FROM NATURAL MAP   :  Efsf7164
************************************************************
**        * FILE NAME               : Efsf7164.java
**        * CLASS NAME              : Efsf7164
**        * INSTANCE NAME           : Efsf7164
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #DAILY.#BATCH-COUNT #DAILY.#IMAGES-COUNT #DAILY.#MINS-COUNT                                                              *     #END-DT #LAST-DT
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsf7164 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Daily_Pnd_Batch_Count;
    private DbsField pnd_Daily_Pnd_Images_Count;
    private DbsField pnd_Daily_Pnd_Mins_Count;
    private DbsField pnd_End_Dt;
    private DbsField pnd_Last_Dt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Daily_Pnd_Batch_Count = parameters.newFieldInRecord("pnd_Daily_Pnd_Batch_Count", "#DAILY.#BATCH-COUNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Daily_Pnd_Images_Count = parameters.newFieldInRecord("pnd_Daily_Pnd_Images_Count", "#DAILY.#IMAGES-COUNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Daily_Pnd_Mins_Count = parameters.newFieldInRecord("pnd_Daily_Pnd_Mins_Count", "#DAILY.#MINS-COUNT", FieldType.PACKED_DECIMAL, 7);
        pnd_End_Dt = parameters.newFieldInRecord("pnd_End_Dt", "#END-DT", FieldType.STRING, 8);
        pnd_Last_Dt = parameters.newFieldInRecord("pnd_Last_Dt", "#LAST-DT", FieldType.STRING, 8);
        parameters.reset();
    }

    public Efsf7164() throws Exception
    {
        super("Efsf7164");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Efsf7164", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Efsf7164"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "-----------------------------------------------------------------------------------------------------------------------------------", 
                "", 1, 1, 131);
            uiForm.setUiControl("pnd_Last_Dt", pnd_Last_Dt, true, 2, 4, 8, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_End_Dt", pnd_End_Dt, true, 2, 20, 8, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Daily_Pnd_Batch_Count", pnd_Daily_Pnd_Batch_Count, true, 2, 41, 6, "WHITE", "ZZ,ZZ9", true, false, null, "0123456789+-, ", 
                "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Daily_Pnd_Mins_Count", pnd_Daily_Pnd_Mins_Count, true, 2, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, false, null, "0123456789+-, ", 
                "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Daily_Pnd_Images_Count", pnd_Daily_Pnd_Images_Count, true, 2, 84, 9, "WHITE", "Z,ZZZ,ZZ9", true, false, null, "0123456789+-, ", 
                "AD=DROFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
