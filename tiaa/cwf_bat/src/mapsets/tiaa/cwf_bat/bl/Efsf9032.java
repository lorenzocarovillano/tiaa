/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:45:44 PM
**        *   FROM NATURAL MAP   :  Efsf9032
************************************************************
**        * FILE NAME               : Efsf9032.java
**        * CLASS NAME              : Efsf9032
**        * INSTANCE NAME           : Efsf9032
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     CWF-FOLDER-AUDIT.ACTION-CDE CWF-FOLDER-AUDIT.DCMT-ADDED-CNT                                                              *     CWF-FOLDER-AUDIT.DCMT-RENAMED-CNT 
    CWF-FOLDER-AUDIT.EMPL-OPRTR-CDE                                                        *     CWF-FOLDER-AUDIT.ERROR-CDE CWF-FOLDER-AUDIT.FLDR-ADDED-CNT 
    *     CWF-FOLDER-AUDIT.FLDR-DELETED-CNT                                                                                        *     CWF-FOLDER-AUDIT.FLDR-UPDATED-CNT 
    CWF-FOLDER-AUDIT.LOG-DTE-TME                                                           *     CWF-FOLDER-AUDIT.MIT-ADDED-CNT CWF-FOLDER-AUDIT.MIT-UPDATED-CNT 
    *     CWF-FOLDER-AUDIT.PIN-NBR CWF-FOLDER-AUDIT.RECURSIVE-IND                                                                  *     CWF-FOLDER-AUDIT.RQST-ID(*) 
    CWF-FOLDER-AUDIT.RQST-PRCSS-TME                                                              *     CWF-FOLDER-AUDIT.SYSTEM-CDE
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsf9032 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField cwf_Folder_Audit_Action_Cde;
    private DbsField cwf_Folder_Audit_Dcmt_Added_Cnt;
    private DbsField cwf_Folder_Audit_Dcmt_Renamed_Cnt;
    private DbsField cwf_Folder_Audit_Empl_Oprtr_Cde;
    private DbsField cwf_Folder_Audit_Error_Cde;
    private DbsField cwf_Folder_Audit_Fldr_Added_Cnt;
    private DbsField cwf_Folder_Audit_Fldr_Deleted_Cnt;
    private DbsField cwf_Folder_Audit_Fldr_Updated_Cnt;
    private DbsField cwf_Folder_Audit_Log_Dte_Tme;
    private DbsField cwf_Folder_Audit_Mit_Added_Cnt;
    private DbsField cwf_Folder_Audit_Mit_Updated_Cnt;
    private DbsField cwf_Folder_Audit_Pin_Nbr;
    private DbsField cwf_Folder_Audit_Recursive_Ind;
    private DbsField cwf_Folder_Audit_Rqst_Id;
    private DbsField cwf_Folder_Audit_Rqst_Prcss_Tme;
    private DbsField cwf_Folder_Audit_System_Cde;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        cwf_Folder_Audit_Action_Cde = parameters.newFieldInRecord("cwf_Folder_Audit_Action_Cde", "CWF-FOLDER-AUDIT.ACTION-CDE", FieldType.STRING, 2);
        cwf_Folder_Audit_Dcmt_Added_Cnt = parameters.newFieldInRecord("cwf_Folder_Audit_Dcmt_Added_Cnt", "CWF-FOLDER-AUDIT.DCMT-ADDED-CNT", FieldType.NUMERIC, 
            3);
        cwf_Folder_Audit_Dcmt_Renamed_Cnt = parameters.newFieldInRecord("cwf_Folder_Audit_Dcmt_Renamed_Cnt", "CWF-FOLDER-AUDIT.DCMT-RENAMED-CNT", FieldType.NUMERIC, 
            3);
        cwf_Folder_Audit_Empl_Oprtr_Cde = parameters.newFieldInRecord("cwf_Folder_Audit_Empl_Oprtr_Cde", "CWF-FOLDER-AUDIT.EMPL-OPRTR-CDE", FieldType.STRING, 
            8);
        cwf_Folder_Audit_Error_Cde = parameters.newFieldInRecord("cwf_Folder_Audit_Error_Cde", "CWF-FOLDER-AUDIT.ERROR-CDE", FieldType.NUMERIC, 3);
        cwf_Folder_Audit_Fldr_Added_Cnt = parameters.newFieldInRecord("cwf_Folder_Audit_Fldr_Added_Cnt", "CWF-FOLDER-AUDIT.FLDR-ADDED-CNT", FieldType.NUMERIC, 
            3);
        cwf_Folder_Audit_Fldr_Deleted_Cnt = parameters.newFieldInRecord("cwf_Folder_Audit_Fldr_Deleted_Cnt", "CWF-FOLDER-AUDIT.FLDR-DELETED-CNT", FieldType.NUMERIC, 
            3);
        cwf_Folder_Audit_Fldr_Updated_Cnt = parameters.newFieldInRecord("cwf_Folder_Audit_Fldr_Updated_Cnt", "CWF-FOLDER-AUDIT.FLDR-UPDATED-CNT", FieldType.NUMERIC, 
            3);
        cwf_Folder_Audit_Log_Dte_Tme = parameters.newFieldInRecord("cwf_Folder_Audit_Log_Dte_Tme", "CWF-FOLDER-AUDIT.LOG-DTE-TME", FieldType.TIME);
        cwf_Folder_Audit_Mit_Added_Cnt = parameters.newFieldInRecord("cwf_Folder_Audit_Mit_Added_Cnt", "CWF-FOLDER-AUDIT.MIT-ADDED-CNT", FieldType.NUMERIC, 
            3);
        cwf_Folder_Audit_Mit_Updated_Cnt = parameters.newFieldInRecord("cwf_Folder_Audit_Mit_Updated_Cnt", "CWF-FOLDER-AUDIT.MIT-UPDATED-CNT", FieldType.NUMERIC, 
            3);
        cwf_Folder_Audit_Pin_Nbr = parameters.newFieldInRecord("cwf_Folder_Audit_Pin_Nbr", "CWF-FOLDER-AUDIT.PIN-NBR", FieldType.NUMERIC, 12);
        cwf_Folder_Audit_Recursive_Ind = parameters.newFieldInRecord("cwf_Folder_Audit_Recursive_Ind", "CWF-FOLDER-AUDIT.RECURSIVE-IND", FieldType.STRING, 
            1);
        cwf_Folder_Audit_Rqst_Id = parameters.newFieldArrayInRecord("cwf_Folder_Audit_Rqst_Id", "CWF-FOLDER-AUDIT.RQST-ID", FieldType.STRING, 16, new 
            DbsArrayController(1, 10));
        cwf_Folder_Audit_Rqst_Prcss_Tme = parameters.newFieldInRecord("cwf_Folder_Audit_Rqst_Prcss_Tme", "CWF-FOLDER-AUDIT.RQST-PRCSS-TME", FieldType.NUMERIC, 
            5);
        cwf_Folder_Audit_System_Cde = parameters.newFieldInRecord("cwf_Folder_Audit_System_Cde", "CWF-FOLDER-AUDIT.SYSTEM-CDE", FieldType.STRING, 15);
        parameters.reset();
    }

    public Efsf9032() throws Exception
    {
        super("Efsf9032");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=001 LS=132 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Efsf9032", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Efsf9032"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("cwf_Folder_Audit_Log_Dte_Tme", cwf_Folder_Audit_Log_Dte_Tme, true, 1, 1, 21, "WHITE", "MM/DD/YYYY' 'HH:II:SS:T", true, 
                false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("cwf_Folder_Audit_System_Cde", cwf_Folder_Audit_System_Cde, true, 1, 23, 6, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("cwf_Folder_Audit_Action_Cde", cwf_Folder_Audit_Action_Cde, true, 1, 31, 2, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("cwf_Folder_Audit_Empl_Oprtr_Cde", cwf_Folder_Audit_Empl_Oprtr_Cde, true, 1, 35, 8, "WHITE", true, false, null, null, 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("cwf_Folder_Audit_Rqst_Prcss_Tme", cwf_Folder_Audit_Rqst_Prcss_Tme, true, 1, 44, 7, "WHITE", "99:99:9", true, false, null, 
                "0123456789+-, ", "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("cwf_Folder_Audit_Recursive_Ind", cwf_Folder_Audit_Recursive_Ind, true, 1, 55, 1, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("cwf_Folder_Audit_Error_Cde", cwf_Folder_Audit_Error_Cde, true, 1, 60, 3, "WHITE", true, false, null, "0123456789+-, ", 
                "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("cwf_Folder_Audit_Mit_Added_Cnt", cwf_Folder_Audit_Mit_Added_Cnt, true, 1, 67, 3, "WHITE", true, false, null, "0123456789+-, ", 
                "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("cwf_Folder_Audit_Mit_Updated_Cnt", cwf_Folder_Audit_Mit_Updated_Cnt, true, 1, 73, 3, "WHITE", true, false, null, "0123456789+-, ", 
                "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("cwf_Folder_Audit_Fldr_Added_Cnt", cwf_Folder_Audit_Fldr_Added_Cnt, true, 1, 80, 3, "WHITE", true, false, null, "0123456789+-, ", 
                "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("cwf_Folder_Audit_Fldr_Updated_Cnt", cwf_Folder_Audit_Fldr_Updated_Cnt, true, 1, 85, 3, "WHITE", true, false, null, "0123456789+-, ", 
                "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("cwf_Folder_Audit_Fldr_Deleted_Cnt", cwf_Folder_Audit_Fldr_Deleted_Cnt, true, 1, 90, 3, "WHITE", true, false, null, "0123456789+-, ", 
                "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("cwf_Folder_Audit_Dcmt_Added_Cnt", cwf_Folder_Audit_Dcmt_Added_Cnt, true, 1, 95, 3, "WHITE", true, false, null, "0123456789+-, ", 
                "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("cwf_Folder_Audit_Dcmt_Renamed_Cnt", cwf_Folder_Audit_Dcmt_Renamed_Cnt, true, 1, 100, 3, "WHITE", true, false, null, "0123456789+-, ", 
                "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("cwf_Folder_Audit_Pin_Nbr", cwf_Folder_Audit_Pin_Nbr, true, 1, 105, 12, "WHITE", true, false, null, "0123456789+-, ", 
                "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("cwf_Folder_Audit_Rqst_Id_1pls0", cwf_Folder_Audit_Rqst_Id.getValue(1+0), true, 1, 118, 14, "WHITE", true, false, null, 
                null, "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
