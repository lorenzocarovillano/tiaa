/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:53:07 PM
**        *   FROM NATURAL MAP   :  Ieff9041
************************************************************
**        * FILE NAME               : Ieff9041.java
**        * CLASS NAME              : Ieff9041
**        * INSTANCE NAME           : Ieff9041
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     ICW-EFM-AUDIT.ACTION-CDE ICW-EFM-AUDIT.CABINET-ID                                                                        *     ICW-EFM-AUDIT.DCMNT-ADDED-CNT 
    ICW-EFM-AUDIT.DCMNT-RENAMED-CNT                                                            *     ICW-EFM-AUDIT.EMPL-OPRTR-CDE ICW-EFM-AUDIT.ERROR-CDE 
    *     ICW-EFM-AUDIT.LOG-DTE-TME ICW-EFM-AUDIT.MIT-ADDED-CNT                                                                    *     ICW-EFM-AUDIT.MIT-UPDATED-CNT 
    ICW-EFM-AUDIT.RECURSIVE-IND                                                                *     ICW-EFM-AUDIT.RQST-ID(*) ICW-EFM-AUDIT.RQST-PRCSS-TME 
    *     ICW-EFM-AUDIT.SYSTEM-CDE
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Ieff9041 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField icw_Efm_Audit_Action_Cde;
    private DbsField icw_Efm_Audit_Cabinet_Id;
    private DbsField icw_Efm_Audit_Dcmnt_Added_Cnt;
    private DbsField icw_Efm_Audit_Dcmnt_Renamed_Cnt;
    private DbsField icw_Efm_Audit_Empl_Oprtr_Cde;
    private DbsField icw_Efm_Audit_Error_Cde;
    private DbsField icw_Efm_Audit_Log_Dte_Tme;
    private DbsField icw_Efm_Audit_Mit_Added_Cnt;
    private DbsField icw_Efm_Audit_Mit_Updated_Cnt;
    private DbsField icw_Efm_Audit_Recursive_Ind;
    private DbsField icw_Efm_Audit_Rqst_Id;
    private DbsField icw_Efm_Audit_Rqst_Prcss_Tme;
    private DbsField icw_Efm_Audit_System_Cde;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        icw_Efm_Audit_Action_Cde = parameters.newFieldInRecord("icw_Efm_Audit_Action_Cde", "ICW-EFM-AUDIT.ACTION-CDE", FieldType.STRING, 2);
        icw_Efm_Audit_Cabinet_Id = parameters.newFieldInRecord("icw_Efm_Audit_Cabinet_Id", "ICW-EFM-AUDIT.CABINET-ID", FieldType.STRING, 12);
        icw_Efm_Audit_Dcmnt_Added_Cnt = parameters.newFieldInRecord("icw_Efm_Audit_Dcmnt_Added_Cnt", "ICW-EFM-AUDIT.DCMNT-ADDED-CNT", FieldType.NUMERIC, 
            3);
        icw_Efm_Audit_Dcmnt_Renamed_Cnt = parameters.newFieldInRecord("icw_Efm_Audit_Dcmnt_Renamed_Cnt", "ICW-EFM-AUDIT.DCMNT-RENAMED-CNT", FieldType.NUMERIC, 
            3);
        icw_Efm_Audit_Empl_Oprtr_Cde = parameters.newFieldInRecord("icw_Efm_Audit_Empl_Oprtr_Cde", "ICW-EFM-AUDIT.EMPL-OPRTR-CDE", FieldType.STRING, 8);
        icw_Efm_Audit_Error_Cde = parameters.newFieldInRecord("icw_Efm_Audit_Error_Cde", "ICW-EFM-AUDIT.ERROR-CDE", FieldType.NUMERIC, 3);
        icw_Efm_Audit_Log_Dte_Tme = parameters.newFieldInRecord("icw_Efm_Audit_Log_Dte_Tme", "ICW-EFM-AUDIT.LOG-DTE-TME", FieldType.TIME);
        icw_Efm_Audit_Mit_Added_Cnt = parameters.newFieldInRecord("icw_Efm_Audit_Mit_Added_Cnt", "ICW-EFM-AUDIT.MIT-ADDED-CNT", FieldType.NUMERIC, 3);
        icw_Efm_Audit_Mit_Updated_Cnt = parameters.newFieldInRecord("icw_Efm_Audit_Mit_Updated_Cnt", "ICW-EFM-AUDIT.MIT-UPDATED-CNT", FieldType.NUMERIC, 
            3);
        icw_Efm_Audit_Recursive_Ind = parameters.newFieldInRecord("icw_Efm_Audit_Recursive_Ind", "ICW-EFM-AUDIT.RECURSIVE-IND", FieldType.STRING, 1);
        icw_Efm_Audit_Rqst_Id = parameters.newFieldArrayInRecord("icw_Efm_Audit_Rqst_Id", "ICW-EFM-AUDIT.RQST-ID", FieldType.STRING, 22, new DbsArrayController(1, 
            10));
        icw_Efm_Audit_Rqst_Prcss_Tme = parameters.newFieldInRecord("icw_Efm_Audit_Rqst_Prcss_Tme", "ICW-EFM-AUDIT.RQST-PRCSS-TME", FieldType.NUMERIC, 
            5);
        icw_Efm_Audit_System_Cde = parameters.newFieldInRecord("icw_Efm_Audit_System_Cde", "ICW-EFM-AUDIT.SYSTEM-CDE", FieldType.STRING, 15);
        parameters.reset();
    }

    public Ieff9041() throws Exception
    {
        super("Ieff9041");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=001 LS=132 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Ieff9041", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Ieff9041"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("icw_Efm_Audit_Log_Dte_Tme", icw_Efm_Audit_Log_Dte_Tme, true, 1, 1, 21, "WHITE", "MM/DD/YYYY' 'HH:II:SS:T", true, false, 
                null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("icw_Efm_Audit_System_Cde", icw_Efm_Audit_System_Cde, true, 1, 23, 6, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("icw_Efm_Audit_Action_Cde", icw_Efm_Audit_Action_Cde, true, 1, 31, 2, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("icw_Efm_Audit_Empl_Oprtr_Cde", icw_Efm_Audit_Empl_Oprtr_Cde, true, 1, 35, 8, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("icw_Efm_Audit_Rqst_Prcss_Tme", icw_Efm_Audit_Rqst_Prcss_Tme, true, 1, 44, 7, "WHITE", "99:99:9", true, false, null, "0123456789+-, ", 
                "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("icw_Efm_Audit_Recursive_Ind", icw_Efm_Audit_Recursive_Ind, true, 1, 54, 1, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("icw_Efm_Audit_Error_Cde", icw_Efm_Audit_Error_Cde, true, 1, 58, 3, "WHITE", true, false, null, "0123456789+-, ", "AD=DROFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("icw_Efm_Audit_Mit_Added_Cnt", icw_Efm_Audit_Mit_Added_Cnt, true, 1, 64, 3, "WHITE", true, false, null, "0123456789+-, ", 
                "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("icw_Efm_Audit_Mit_Updated_Cnt", icw_Efm_Audit_Mit_Updated_Cnt, true, 1, 70, 3, "WHITE", true, false, null, "0123456789+-, ", 
                "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("icw_Efm_Audit_Dcmnt_Added_Cnt", icw_Efm_Audit_Dcmnt_Added_Cnt, true, 1, 76, 3, "WHITE", true, false, null, "0123456789+-, ", 
                "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("icw_Efm_Audit_Dcmnt_Renamed_Cnt", icw_Efm_Audit_Dcmnt_Renamed_Cnt, true, 1, 82, 3, "WHITE", true, false, null, "0123456789+-, ", 
                "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("icw_Efm_Audit_Cabinet_Id", icw_Efm_Audit_Cabinet_Id, true, 1, 88, 12, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("icw_Efm_Audit_Rqst_Id_1", icw_Efm_Audit_Rqst_Id.getValue(1), true, 1, 101, 18, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
