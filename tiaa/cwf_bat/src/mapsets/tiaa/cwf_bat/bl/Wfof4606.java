/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:59:20 PM
**        *   FROM NATURAL MAP   :  Wfof4606
************************************************************
**        * FILE NAME               : Wfof4606.java
**        * CLASS NAME              : Wfof4606
**        * INSTANCE NAME           : Wfof4606
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     CMCV.BATCH-ID CMCV.DCMNT-DIRECTION CMCV.DCMNT-OBJ-ID                                                                     *     CMCV.DOC-CLSS-CDE 
    CMCV.DOC-CTGRY-CDE CMCV.DOC-TXT(*)                                                                     *     CMCV.DOC-TYPE-SPCFC-CDE CMCV.IMAGE-ADDRSS 
    CMCV.IMG-START-PAGE-NBR                                                        *     CMCV.IMG-TOTAL-PAGES CMCV.MAIL-ITEM-NO CMCV.SOURCE-ID          
    *     CMCV.T-CLIENT-APP-CDE
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Wfof4606 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField cmcv_Batch_Id;
    private DbsField cmcv_Dcmnt_Direction;
    private DbsField cmcv_Dcmnt_Obj_Id;
    private DbsField cmcv_Doc_Clss_Cde;
    private DbsField cmcv_Doc_Ctgry_Cde;
    private DbsField cmcv_Doc_Txt;
    private DbsField cmcv_Doc_Type_Spcfc_Cde;
    private DbsField cmcv_Image_Addrss;
    private DbsField cmcv_Img_Start_Page_Nbr;
    private DbsField cmcv_Img_Total_Pages;
    private DbsField cmcv_Mail_Item_No;
    private DbsField cmcv_Source_Id;
    private DbsField cmcv_T_Client_App_Cde;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        cmcv_Batch_Id = parameters.newFieldInRecord("cmcv_Batch_Id", "CMCV.BATCH-ID", FieldType.NUMERIC, 11, 2);
        cmcv_Dcmnt_Direction = parameters.newFieldInRecord("cmcv_Dcmnt_Direction", "CMCV.DCMNT-DIRECTION", FieldType.STRING, 1);
        cmcv_Dcmnt_Obj_Id = parameters.newFieldInRecord("cmcv_Dcmnt_Obj_Id", "CMCV.DCMNT-OBJ-ID", FieldType.STRING, 20);
        cmcv_Doc_Clss_Cde = parameters.newFieldInRecord("cmcv_Doc_Clss_Cde", "CMCV.DOC-CLSS-CDE", FieldType.STRING, 3);
        cmcv_Doc_Ctgry_Cde = parameters.newFieldInRecord("cmcv_Doc_Ctgry_Cde", "CMCV.DOC-CTGRY-CDE", FieldType.STRING, 1);
        cmcv_Doc_Txt = parameters.newFieldArrayInRecord("cmcv_Doc_Txt", "CMCV.DOC-TXT", FieldType.STRING, 80, new DbsArrayController(1, 60));
        cmcv_Doc_Type_Spcfc_Cde = parameters.newFieldInRecord("cmcv_Doc_Type_Spcfc_Cde", "CMCV.DOC-TYPE-SPCFC-CDE", FieldType.STRING, 4);
        cmcv_Image_Addrss = parameters.newFieldInRecord("cmcv_Image_Addrss", "CMCV.IMAGE-ADDRSS", FieldType.STRING, 22);
        cmcv_Img_Start_Page_Nbr = parameters.newFieldInRecord("cmcv_Img_Start_Page_Nbr", "CMCV.IMG-START-PAGE-NBR", FieldType.NUMERIC, 3);
        cmcv_Img_Total_Pages = parameters.newFieldInRecord("cmcv_Img_Total_Pages", "CMCV.IMG-TOTAL-PAGES", FieldType.NUMERIC, 3);
        cmcv_Mail_Item_No = parameters.newFieldInRecord("cmcv_Mail_Item_No", "CMCV.MAIL-ITEM-NO", FieldType.STRING, 11);
        cmcv_Source_Id = parameters.newFieldInRecord("cmcv_Source_Id", "CMCV.SOURCE-ID", FieldType.STRING, 6);
        cmcv_T_Client_App_Cde = parameters.newFieldInRecord("cmcv_T_Client_App_Cde", "CMCV.T-CLIENT-APP-CDE", FieldType.STRING, 8);
        parameters.reset();
    }

    public Wfof4606() throws Exception
    {
        super("Wfof4606");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=008 LS=133 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Wfof4606", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Wfof4606"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "Doc Txt.....:", "", 1, 41, 13);
            uiForm.setUiControl("cmcv_Doc_Txt_1", cmcv_Doc_Txt.getValue(1), true, 1, 55, 60, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_2", "ClntApp:", "", 1, 116, 8);
            uiForm.setUiControl("cmcv_T_Client_App_Cde", cmcv_T_Client_App_Cde, true, 1, 125, 8, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("cmcv_Doc_Txt_2", cmcv_Doc_Txt.getValue(2), true, 2, 55, 60, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("cmcv_Doc_Txt_3", cmcv_Doc_Txt.getValue(3), true, 3, 55, 60, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("cmcv_Doc_Txt_4", cmcv_Doc_Txt.getValue(4), true, 4, 55, 60, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("cmcv_Doc_Txt_5", cmcv_Doc_Txt.getValue(5), true, 5, 55, 60, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_3", "DocCtgy:", "", 6, 41, 8);
            uiForm.setUiControl("cmcv_Doc_Ctgry_Cde", cmcv_Doc_Ctgry_Cde, true, 6, 50, 1, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_4", "DCC:", "", 6, 52, 4);
            uiForm.setUiControl("cmcv_Doc_Clss_Cde", cmcv_Doc_Clss_Cde, true, 6, 57, 3, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_5", "DocSpcf:", "", 6, 61, 8);
            uiForm.setUiControl("cmcv_Doc_Type_Spcfc_Cde", cmcv_Doc_Type_Spcfc_Cde, true, 6, 70, 4, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_6", "ImageAddr:", "", 6, 77, 10);
            uiForm.setUiControl("cmcv_Image_Addrss", cmcv_Image_Addrss, true, 6, 88, 22, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_7", "ISP:", "", 6, 112, 4);
            uiForm.setUiControl("cmcv_Img_Start_Page_Nbr", cmcv_Img_Start_Page_Nbr, true, 6, 117, 3, "", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_8", "DocObjId....:", "", 7, 41, 13);
            uiForm.setUiControl("cmcv_Dcmnt_Obj_Id", cmcv_Dcmnt_Obj_Id, true, 7, 55, 20, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_9", "MailItNbr:", "", 7, 77, 10);
            uiForm.setUiControl("cmcv_Mail_Item_No", cmcv_Mail_Item_No, true, 7, 88, 11, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_10", "DocDrctn:", "", 7, 100, 9);
            uiForm.setUiControl("cmcv_Dcmnt_Direction", cmcv_Dcmnt_Direction, true, 7, 110, 1, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_11", "ITP:", "", 7, 112, 4);
            uiForm.setUiControl("cmcv_Img_Total_Pages", cmcv_Img_Total_Pages, true, 7, 117, 3, "", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_12", "BatchID.....:", "", 8, 41, 13);
            uiForm.setUiControl("cmcv_Batch_Id", cmcv_Batch_Id, true, 8, 55, 10, "", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_13", "SrceID...:", "", 8, 77, 10);
            uiForm.setUiControl("cmcv_Source_Id", cmcv_Source_Id, true, 8, 88, 6, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
