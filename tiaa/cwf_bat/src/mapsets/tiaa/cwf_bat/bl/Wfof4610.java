/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:59:21 PM
**        *   FROM NATURAL MAP   :  Wfof4610
************************************************************
**        * FILE NAME               : Wfof4610.java
**        * CLASS NAME              : Wfof4610
**        * INSTANCE NAME           : Wfof4610
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #SAVE-CLIENT-APP-CDE CMCV.ACTION-CDE(*) CMCV.DOC-TXT(*)                                                                  *     CMCV.TRACKING-RCRD-NBR(*) 
    CMCV.WFO-IND
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Wfof4610 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Save_Client_App_Cde;
    private DbsField cmcv_Action_Cde;
    private DbsField cmcv_Doc_Txt;
    private DbsField cmcv_Tracking_Rcrd_Nbr;
    private DbsField cmcv_Wfo_Ind;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Save_Client_App_Cde = parameters.newFieldInRecord("pnd_Save_Client_App_Cde", "#SAVE-CLIENT-APP-CDE", FieldType.STRING, 8);
        cmcv_Action_Cde = parameters.newFieldArrayInRecord("cmcv_Action_Cde", "CMCV.ACTION-CDE", FieldType.STRING, 2, new DbsArrayController(1, 10));
        cmcv_Doc_Txt = parameters.newFieldArrayInRecord("cmcv_Doc_Txt", "CMCV.DOC-TXT", FieldType.STRING, 80, new DbsArrayController(1, 60));
        cmcv_Tracking_Rcrd_Nbr = parameters.newFieldArrayInRecord("cmcv_Tracking_Rcrd_Nbr", "CMCV.TRACKING-RCRD-NBR", FieldType.STRING, 20, new DbsArrayController(1, 
            10));
        cmcv_Wfo_Ind = parameters.newFieldInRecord("cmcv_Wfo_Ind", "CMCV.WFO-IND", FieldType.STRING, 1);
        parameters.reset();
    }

    public Wfof4610() throws Exception
    {
        super("Wfof4610");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=006 LS=133 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Wfof4610", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Wfof4610"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "Doc Txt:", "", 1, 41, 8);
            uiForm.setUiControl("cmcv_Doc_Txt_1", cmcv_Doc_Txt.getValue(1), true, 1, 50, 80, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("cmcv_Doc_Txt_2", cmcv_Doc_Txt.getValue(2), true, 2, 50, 80, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("cmcv_Doc_Txt_3", cmcv_Doc_Txt.getValue(3), true, 3, 50, 80, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("cmcv_Doc_Txt_4", cmcv_Doc_Txt.getValue(4), true, 4, 50, 80, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("cmcv_Doc_Txt_5", cmcv_Doc_Txt.getValue(5), true, 5, 50, 80, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_2", "AC:", "", 6, 41, 3);
            uiForm.setUiControl("cmcv_Action_Cde_1", cmcv_Action_Cde.getValue(1), true, 6, 45, 2, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_3", "WFO Ind:", "", 6, 49, 8);
            uiForm.setUiControl("cmcv_Wfo_Ind", cmcv_Wfo_Ind, true, 6, 58, 1, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_4", "TrackingRcrdNbr:", "", 6, 60, 16);
            uiForm.setUiControl("cmcv_Tracking_Rcrd_Nbr_1", cmcv_Tracking_Rcrd_Nbr.getValue(1), true, 6, 77, 20, "", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_5", "ClntApp:", "", 6, 116, 8);
            uiForm.setUiControl("pnd_Save_Client_App_Cde", pnd_Save_Client_App_Cde, true, 6, 125, 8, "", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
