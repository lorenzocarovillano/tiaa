/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:45:36 PM
**        *   FROM NATURAL MAP   :  Efsf7061
************************************************************
**        * FILE NAME               : Efsf7061.java
**        * CLASS NAME              : Efsf7061
**        * INSTANCE NAME           : Efsf7061
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #LINE.#ASTRIX #LINE.#MINS-NOT-UPLOADED #LINE.#PLATTER                                                                    *     CWF-UPLOAD-AUDIT.BATCH-EXT-NBR 
    CWF-UPLOAD-AUDIT.BATCH-LABEL-TXT                                                          *     CWF-UPLOAD-AUDIT.BATCH-NBR CWF-UPLOAD-AUDIT.IMAGES-UPLOADED-CNT 
    *     CWF-UPLOAD-AUDIT.MINS-BCH-HDR-CNT                                                                                        *     CWF-UPLOAD-AUDIT.MINS-UPLOADED-CNT 
    *     CWF-UPLOAD-AUDIT.UPLD-DATE-TIME
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsf7061 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Line_Pnd_Astrix;
    private DbsField pnd_Line_Pnd_Mins_Not_Uploaded;
    private DbsField pnd_Line_Pnd_Platter;
    private DbsField cwf_Upload_Audit_Batch_Ext_Nbr;
    private DbsField cwf_Upload_Audit_Batch_Label_Txt;
    private DbsField cwf_Upload_Audit_Batch_Nbr;
    private DbsField cwf_Upload_Audit_Images_Uploaded_Cnt;
    private DbsField cwf_Upload_Audit_Mins_Bch_Hdr_Cnt;
    private DbsField cwf_Upload_Audit_Mins_Uploaded_Cnt;
    private DbsField cwf_Upload_Audit_Upld_Date_Time;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Line_Pnd_Astrix = parameters.newFieldInRecord("pnd_Line_Pnd_Astrix", "#LINE.#ASTRIX", FieldType.STRING, 1);
        pnd_Line_Pnd_Mins_Not_Uploaded = parameters.newFieldInRecord("pnd_Line_Pnd_Mins_Not_Uploaded", "#LINE.#MINS-NOT-UPLOADED", FieldType.NUMERIC, 
            3);
        pnd_Line_Pnd_Platter = parameters.newFieldInRecord("pnd_Line_Pnd_Platter", "#LINE.#PLATTER", FieldType.STRING, 10);
        cwf_Upload_Audit_Batch_Ext_Nbr = parameters.newFieldInRecord("cwf_Upload_Audit_Batch_Ext_Nbr", "CWF-UPLOAD-AUDIT.BATCH-EXT-NBR", FieldType.NUMERIC, 
            2);
        cwf_Upload_Audit_Batch_Label_Txt = parameters.newFieldInRecord("cwf_Upload_Audit_Batch_Label_Txt", "CWF-UPLOAD-AUDIT.BATCH-LABEL-TXT", FieldType.STRING, 
            20);
        cwf_Upload_Audit_Batch_Nbr = parameters.newFieldInRecord("cwf_Upload_Audit_Batch_Nbr", "CWF-UPLOAD-AUDIT.BATCH-NBR", FieldType.NUMERIC, 8);
        cwf_Upload_Audit_Images_Uploaded_Cnt = parameters.newFieldInRecord("cwf_Upload_Audit_Images_Uploaded_Cnt", "CWF-UPLOAD-AUDIT.IMAGES-UPLOADED-CNT", 
            FieldType.NUMERIC, 5);
        cwf_Upload_Audit_Mins_Bch_Hdr_Cnt = parameters.newFieldInRecord("cwf_Upload_Audit_Mins_Bch_Hdr_Cnt", "CWF-UPLOAD-AUDIT.MINS-BCH-HDR-CNT", FieldType.NUMERIC, 
            3);
        cwf_Upload_Audit_Mins_Uploaded_Cnt = parameters.newFieldInRecord("cwf_Upload_Audit_Mins_Uploaded_Cnt", "CWF-UPLOAD-AUDIT.MINS-UPLOADED-CNT", FieldType.NUMERIC, 
            3);
        cwf_Upload_Audit_Upld_Date_Time = parameters.newFieldInRecord("cwf_Upload_Audit_Upld_Date_Time", "CWF-UPLOAD-AUDIT.UPLD-DATE-TIME", FieldType.TIME);
        parameters.reset();
    }

    public Efsf7061() throws Exception
    {
        super("Efsf7061");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Efsf7061", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Efsf7061"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("cwf_Upload_Audit_Batch_Nbr", cwf_Upload_Audit_Batch_Nbr, true, 1, 1, 8, "WHITE", "99999999", true, true, null, "0123456789+-, ", 
                "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("cwf_Upload_Audit_Batch_Ext_Nbr", cwf_Upload_Audit_Batch_Ext_Nbr, true, 1, 11, 2, "WHITE", "99", true, true, null, "0123456789+-, ", 
                "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("cwf_Upload_Audit_Batch_Label_Txt", cwf_Upload_Audit_Batch_Label_Txt, true, 1, 17, 20, "WHITE", true, false, null, null, 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("cwf_Upload_Audit_Upld_Date_Time", cwf_Upload_Audit_Upld_Date_Time, true, 1, 38, 17, "WHITE", "MM/DD/YY' 'HH:II:SS", true, 
                false, null, null, "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("cwf_Upload_Audit_Mins_Bch_Hdr_Cnt", cwf_Upload_Audit_Mins_Bch_Hdr_Cnt, true, 1, 58, 3, "WHITE", true, true, null, "0123456789+-, ", 
                "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("cwf_Upload_Audit_Mins_Uploaded_Cnt", cwf_Upload_Audit_Mins_Uploaded_Cnt, true, 1, 69, 3, "WHITE", true, true, null, "0123456789+-, ", 
                "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Line_Pnd_Mins_Not_Uploaded", pnd_Line_Pnd_Mins_Not_Uploaded, true, 1, 86, 3, "WHITE", true, false, null, "0123456789+-, ", 
                "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("cwf_Upload_Audit_Images_Uploaded_Cnt", cwf_Upload_Audit_Images_Uploaded_Cnt, true, 1, 102, 5, "WHITE", true, true, null, 
                "0123456789+-, ", "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Line_Pnd_Platter", pnd_Line_Pnd_Platter, true, 1, 115, 10, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Line_Pnd_Astrix", pnd_Line_Pnd_Astrix, true, 1, 127, 1, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
