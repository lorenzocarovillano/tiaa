/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:59:12 PM
**        *   FROM NATURAL MAP   :  Wfof4501
************************************************************
**        * FILE NAME               : Wfof4501.java
**        * CLASS NAME              : Wfof4501
**        * INSTANCE NAME           : Wfof4501
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     CMCV.CALL-STAMP-TME CMCV.ERROR-MSG-TXT CMCV.EXPRESS-IND                                                                  *     CMCV.RQST-ENTRY-OP-CDE 
    CMCV.RQST-ORIGIN-UNIT-CDE
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Wfof4501 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField cmcv_Call_Stamp_Tme;
    private DbsField cmcv_Error_Msg_Txt;
    private DbsField cmcv_Express_Ind;
    private DbsField cmcv_Rqst_Entry_Op_Cde;
    private DbsField cmcv_Rqst_Origin_Unit_Cde;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        cmcv_Call_Stamp_Tme = parameters.newFieldInRecord("cmcv_Call_Stamp_Tme", "CMCV.CALL-STAMP-TME", FieldType.TIME);
        cmcv_Error_Msg_Txt = parameters.newFieldInRecord("cmcv_Error_Msg_Txt", "CMCV.ERROR-MSG-TXT", FieldType.STRING, 60);
        cmcv_Express_Ind = parameters.newFieldInRecord("cmcv_Express_Ind", "CMCV.EXPRESS-IND", FieldType.STRING, 1);
        cmcv_Rqst_Entry_Op_Cde = parameters.newFieldInRecord("cmcv_Rqst_Entry_Op_Cde", "CMCV.RQST-ENTRY-OP-CDE", FieldType.STRING, 8);
        cmcv_Rqst_Origin_Unit_Cde = parameters.newFieldInRecord("cmcv_Rqst_Origin_Unit_Cde", "CMCV.RQST-ORIGIN-UNIT-CDE", FieldType.STRING, 8);
        parameters.reset();
    }

    public Wfof4501() throws Exception
    {
        super("Wfof4501");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=002 LS=133 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Wfof4501", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Wfof4501"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "WRK RQST ErrorMsgTxt.:", "", 1, 32, 22);
            uiForm.setUiControl("cmcv_Error_Msg_Txt", cmcv_Error_Msg_Txt, true, 1, 55, 60, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_2", "CallStmpTme.:", "", 2, 41, 13);
            uiForm.setUiControl("cmcv_Call_Stamp_Tme", cmcv_Call_Stamp_Tme, true, 2, 55, 15, "", "YYYYMMDDHHIISST", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_3", "Oper...:", "", 2, 77, 8);
            uiForm.setUiControl("cmcv_Rqst_Entry_Op_Cde", cmcv_Rqst_Entry_Op_Cde, true, 2, 86, 8, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_4", "OrigUnit..:", "", 2, 96, 11);
            uiForm.setUiControl("cmcv_Rqst_Origin_Unit_Cde", cmcv_Rqst_Origin_Unit_Cde, true, 2, 108, 8, "", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_5", "Exp:", "", 2, 120, 4);
            uiForm.setUiControl("cmcv_Express_Ind", cmcv_Express_Ind, true, 2, 125, 1, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
