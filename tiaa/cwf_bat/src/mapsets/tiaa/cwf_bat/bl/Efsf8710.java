/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:45:42 PM
**        *   FROM NATURAL MAP   :  Efsf8710
************************************************************
**        * FILE NAME               : Efsf8710.java
**        * CLASS NAME              : Efsf8710
**        * INSTANCE NAME           : Efsf8710
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     #DOC-CREATED #IDENTIFY-DTE-TME #MIN-SAVE #PCKGE-CODE #PIN-PPG                                                            *     CWF-IMAGE-XREF.IMAGE-DOCUMENT-ADDRESS-CDE 
    *     CWF-IMAGE-XREF.IMAGE-QUALITY-IND CWF-IMAGE-XREF.NUMBER-OF-PAGES                                                          *     CWF-IMAGE-XREF.UPLD-DATE-TIME
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsf8710 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Doc_Created;
    private DbsField pnd_Identify_Dte_Tme;
    private DbsField pnd_Min_Save;
    private DbsField pnd_Pckge_Code;
    private DbsField pnd_Pin_Ppg;
    private DbsField cwf_Image_Xref_Image_Document_Address_Cde;
    private DbsField cwf_Image_Xref_Image_Quality_Ind;
    private DbsField cwf_Image_Xref_Number_Of_Pages;
    private DbsField cwf_Image_Xref_Upld_Date_Time;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Doc_Created = parameters.newFieldInRecord("pnd_Doc_Created", "#DOC-CREATED", FieldType.STRING, 1);
        pnd_Identify_Dte_Tme = parameters.newFieldInRecord("pnd_Identify_Dte_Tme", "#IDENTIFY-DTE-TME", FieldType.STRING, 19);
        pnd_Min_Save = parameters.newFieldInRecord("pnd_Min_Save", "#MIN-SAVE", FieldType.NUMERIC, 11);
        pnd_Pckge_Code = parameters.newFieldInRecord("pnd_Pckge_Code", "#PCKGE-CODE", FieldType.STRING, 8);
        pnd_Pin_Ppg = parameters.newFieldInRecord("pnd_Pin_Ppg", "#PIN-PPG", FieldType.STRING, 12);
        cwf_Image_Xref_Image_Document_Address_Cde = parameters.newFieldInRecord("cwf_Image_Xref_Image_Document_Address_Cde", "CWF-IMAGE-XREF.IMAGE-DOCUMENT-ADDRESS-CDE", 
            FieldType.STRING, 22);
        cwf_Image_Xref_Image_Quality_Ind = parameters.newFieldInRecord("cwf_Image_Xref_Image_Quality_Ind", "CWF-IMAGE-XREF.IMAGE-QUALITY-IND", FieldType.STRING, 
            1);
        cwf_Image_Xref_Number_Of_Pages = parameters.newFieldInRecord("cwf_Image_Xref_Number_Of_Pages", "CWF-IMAGE-XREF.NUMBER-OF-PAGES", FieldType.NUMERIC, 
            3);
        cwf_Image_Xref_Upld_Date_Time = parameters.newFieldInRecord("cwf_Image_Xref_Upld_Date_Time", "CWF-IMAGE-XREF.UPLD-DATE-TIME", FieldType.TIME);
        parameters.reset();
    }

    public Efsf8710() throws Exception
    {
        super("Efsf8710");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=001 LS=132 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Efsf8710", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Efsf8710"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Min_Save", pnd_Min_Save, true, 1, 2, 11, "WHITE", true, true, null, "0123456789+-, ", "AD=DZOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pckge_Code", pnd_Pckge_Code, true, 1, 18, 8, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pin_Ppg", pnd_Pin_Ppg, true, 1, 27, 12, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("cwf_Image_Xref_Image_Quality_Ind", cwf_Image_Xref_Image_Quality_Ind, true, 1, 46, 1, "WHITE", true, false, null, null, 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("cwf_Image_Xref_Upld_Date_Time", cwf_Image_Xref_Upld_Date_Time, true, 1, 53, 17, "WHITE", "MM/DD/YY' 'HH:II:SS", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Doc_Created", pnd_Doc_Created, true, 1, 74, 1, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("cwf_Image_Xref_Number_Of_Pages", cwf_Image_Xref_Number_Of_Pages, true, 1, 82, 3, "WHITE", true, false, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("cwf_Image_Xref_Image_Document_Address_Cde", cwf_Image_Xref_Image_Document_Address_Cde, true, 1, 88, 22, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Identify_Dte_Tme", pnd_Identify_Dte_Tme, true, 1, 113, 19, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
