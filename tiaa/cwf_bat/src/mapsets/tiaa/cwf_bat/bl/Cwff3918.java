/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:41:05 PM
**        *   FROM NATURAL MAP   :  Cwff3918
************************************************************
**        * FILE NAME               : Cwff3918.java
**        * CLASS NAME              : Cwff3918
**        * INSTANCE NAME           : Cwff3918
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #ENV #RECORD #REPORT-NAME
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwff3918 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Env;
    private DbsField pnd_Record;
    private DbsField pnd_Report_Name;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Env = parameters.newFieldInRecord("pnd_Env", "#ENV", FieldType.STRING, 10);
        pnd_Record = parameters.newFieldInRecord("pnd_Record", "#RECORD", FieldType.STRING, 54);
        pnd_Report_Name = parameters.newFieldInRecord("pnd_Report_Name", "#REPORT-NAME", FieldType.STRING, 45);
        parameters.reset();
    }

    public Cwff3918() throws Exception
    {
        super("Cwff3918");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=060 LS=133 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Cwff3918", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Cwff3918"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Record", pnd_Record, true, 1, 2, 54, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_1", "************************************************************************************************************************************", 
                "", 2, 1, 132);
            uiForm.setUiControl("pnd_Env", pnd_Env, true, 3, 2, 10, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("astDATX", Global.getDATX(), true, 13, 125, 8, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_2", "CCC", "", 14, 42, 3);
            uiForm.setUiLabel("label_3", "OOO", "", 14, 48, 3);
            uiForm.setUiLabel("label_4", "RRRR", "", 14, 53, 4);
            uiForm.setUiLabel("label_5", "PPPP", "", 14, 59, 4);
            uiForm.setUiLabel("label_6", "OOO", "", 14, 66, 3);
            uiForm.setUiLabel("label_7", "RRRR", "", 14, 71, 4);
            uiForm.setUiLabel("label_8", "A TTTTTT EEEE", "", 14, 79, 13);
            uiForm.setUiControl("astTIMX", Global.getTIMX(), true, 14, 125, 8, "WHITE", "HH:II' 'AP", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_9", "CC", "", 15, 41, 2);
            uiForm.setUiLabel("label_10", "C OO", "", 15, 45, 4);
            uiForm.setUiLabel("label_11", "O RR", "", 15, 51, 4);
            uiForm.setUiLabel("label_12", "R PP", "", 15, 57, 4);
            uiForm.setUiLabel("label_13", "P OO", "", 15, 63, 4);
            uiForm.setUiLabel("label_14", "O RR", "", 15, 69, 4);
            uiForm.setUiLabel("label_15", "R", "", 15, 75, 1);
            uiForm.setUiLabel("label_16", "AAA", "", 15, 78, 3);
            uiForm.setUiLabel("label_17", "TT", "", 15, 83, 2);
            uiForm.setUiLabel("label_18", "EE", "", 15, 88, 2);
            uiForm.setUiLabel("label_19", "CC", "", 16, 41, 2);
            uiForm.setUiLabel("label_20", "OO", "", 16, 47, 2);
            uiForm.setUiLabel("label_21", "O RRRR", "", 16, 51, 6);
            uiForm.setUiLabel("label_22", "PPPP", "", 16, 59, 4);
            uiForm.setUiLabel("label_23", "OO", "", 16, 65, 2);
            uiForm.setUiLabel("label_24", "O RRRR", "", 16, 69, 6);
            uiForm.setUiLabel("label_25", "AA", "", 16, 77, 2);
            uiForm.setUiLabel("label_26", "A TT", "", 16, 81, 4);
            uiForm.setUiLabel("label_27", "EEEE", "", 16, 88, 4);
            uiForm.setUiLabel("label_28", "CC", "", 17, 41, 2);
            uiForm.setUiLabel("label_29", "C OO", "", 17, 45, 4);
            uiForm.setUiLabel("label_30", "O RR R", "", 17, 51, 6);
            uiForm.setUiLabel("label_31", "PP", "", 17, 59, 2);
            uiForm.setUiLabel("label_32", "OO", "", 17, 65, 2);
            uiForm.setUiLabel("label_33", "O RR R", "", 17, 69, 6);
            uiForm.setUiLabel("label_34", "AAAAA TT", "", 17, 77, 8);
            uiForm.setUiLabel("label_35", "EE", "", 17, 88, 2);
            uiForm.setUiLabel("label_36", "CCC", "", 18, 42, 3);
            uiForm.setUiLabel("label_37", "OOO", "", 18, 48, 3);
            uiForm.setUiLabel("label_38", "RR", "", 18, 53, 2);
            uiForm.setUiLabel("label_39", "R PP", "", 18, 57, 4);
            uiForm.setUiLabel("label_40", "OOO", "", 18, 66, 3);
            uiForm.setUiLabel("label_41", "RR", "", 18, 71, 2);
            uiForm.setUiLabel("label_42", "R AA", "", 18, 75, 4);
            uiForm.setUiLabel("label_43", "A TT", "", 18, 81, 4);
            uiForm.setUiLabel("label_44", "EEEE", "", 18, 88, 4);
            uiForm.setUiLabel("label_45", "WW", "", 21, 43, 2);
            uiForm.setUiLabel("label_46", "W", "", 21, 48, 1);
            uiForm.setUiLabel("label_47", "OOO", "", 21, 51, 3);
            uiForm.setUiLabel("label_48", "RRRR", "", 21, 56, 4);
            uiForm.setUiLabel("label_49", "KK", "", 21, 62, 2);
            uiForm.setUiLabel("label_50", "K FFFF LL", "", 21, 66, 9);
            uiForm.setUiLabel("label_51", "OOO", "", 21, 79, 3);
            uiForm.setUiLabel("label_52", "WW", "", 21, 84, 2);
            uiForm.setUiLabel("label_53", "W", "", 21, 89, 1);
            uiForm.setUiLabel("label_54", "WW", "", 22, 43, 2);
            uiForm.setUiLabel("label_55", "W OO", "", 22, 48, 4);
            uiForm.setUiLabel("label_56", "O RR", "", 22, 54, 4);
            uiForm.setUiLabel("label_57", "R KK K", "", 22, 60, 6);
            uiForm.setUiLabel("label_58", "FF", "", 22, 68, 2);
            uiForm.setUiLabel("label_59", "LL", "", 22, 73, 2);
            uiForm.setUiLabel("label_60", "OO", "", 22, 78, 2);
            uiForm.setUiLabel("label_61", "O WW", "", 22, 82, 4);
            uiForm.setUiLabel("label_62", "W", "", 22, 89, 1);
            uiForm.setUiLabel("label_63", "WW W W OO", "", 23, 43, 9);
            uiForm.setUiLabel("label_64", "O RRRR", "", 23, 54, 6);
            uiForm.setUiLabel("label_65", "KKK", "", 23, 62, 3);
            uiForm.setUiLabel("label_66", "FFFF LL", "", 23, 68, 7);
            uiForm.setUiLabel("label_67", "OO", "", 23, 78, 2);
            uiForm.setUiLabel("label_68", "O WW W W", "", 23, 82, 8);
            uiForm.setUiLabel("label_69", "WW W W OO", "", 24, 43, 9);
            uiForm.setUiLabel("label_70", "O RR R", "", 24, 54, 6);
            uiForm.setUiLabel("label_71", "KK K", "", 24, 62, 4);
            uiForm.setUiLabel("label_72", "FF", "", 24, 68, 2);
            uiForm.setUiLabel("label_73", "LL", "", 24, 73, 2);
            uiForm.setUiLabel("label_74", "OO", "", 24, 78, 2);
            uiForm.setUiLabel("label_75", "O WW W W", "", 24, 82, 8);
            uiForm.setUiLabel("label_76", "WW W", "", 25, 44, 4);
            uiForm.setUiLabel("label_77", "OOO", "", 25, 51, 3);
            uiForm.setUiLabel("label_78", "RR", "", 25, 56, 2);
            uiForm.setUiLabel("label_79", "R KK", "", 25, 60, 4);
            uiForm.setUiLabel("label_80", "K FF", "", 25, 66, 4);
            uiForm.setUiLabel("label_81", "LLLLL OOO", "", 25, 73, 9);
            uiForm.setUiLabel("label_82", "WW W", "", 25, 85, 4);
            uiForm.setUiLabel("label_83", "FFFF", "", 28, 42, 4);
            uiForm.setUiLabel("label_84", "A", "", 28, 49, 1);
            uiForm.setUiLabel("label_85", "CCC", "", 28, 54, 3);
            uiForm.setUiLabel("label_86", "II LL", "", 28, 59, 5);
            uiForm.setUiLabel("label_87", "II TTTTTT II EEEE", "", 28, 67, 17);
            uiForm.setUiLabel("label_88", "SSSS", "", 28, 86, 4);
            uiForm.setUiLabel("label_89", "FF", "", 29, 42, 2);
            uiForm.setUiLabel("label_90", "A A", "", 29, 48, 3);
            uiForm.setUiLabel("label_91", "CC", "", 29, 53, 2);
            uiForm.setUiLabel("label_92", "C II LL", "", 29, 57, 7);
            uiForm.setUiLabel("label_93", "II", "", 29, 67, 2);
            uiForm.setUiLabel("label_94", "TT", "", 29, 72, 2);
            uiForm.setUiLabel("label_95", "II EE", "", 29, 77, 5);
            uiForm.setUiLabel("label_96", "SS", "", 29, 85, 2);
            uiForm.setUiLabel("label_97", "S", "", 29, 90, 1);
            uiForm.setUiLabel("label_98", "FFFF AA", "", 30, 42, 7);
            uiForm.setUiLabel("label_99", "A CC", "", 30, 51, 4);
            uiForm.setUiLabel("label_100", "II LL", "", 30, 59, 5);
            uiForm.setUiLabel("label_101", "II", "", 30, 67, 2);
            uiForm.setUiLabel("label_102", "TT", "", 30, 72, 2);
            uiForm.setUiLabel("label_103", "II EEEE", "", 30, 77, 7);
            uiForm.setUiLabel("label_104", "SSSS", "", 30, 86, 4);
            uiForm.setUiLabel("label_105", "FF", "", 31, 42, 2);
            uiForm.setUiLabel("label_106", "AAAAA CC", "", 31, 47, 8);
            uiForm.setUiLabel("label_107", "C II LL", "", 31, 57, 7);
            uiForm.setUiLabel("label_108", "II", "", 31, 67, 2);
            uiForm.setUiLabel("label_109", "TT", "", 31, 72, 2);
            uiForm.setUiLabel("label_110", "II EE", "", 31, 77, 5);
            uiForm.setUiLabel("label_111", "S", "", 31, 85, 1);
            uiForm.setUiLabel("label_112", "SS", "", 31, 89, 2);
            uiForm.setUiLabel("label_113", "FF", "", 32, 42, 2);
            uiForm.setUiLabel("label_114", "AA", "", 32, 47, 2);
            uiForm.setUiLabel("label_115", "A", "", 32, 51, 1);
            uiForm.setUiLabel("label_116", "CCC", "", 32, 54, 3);
            uiForm.setUiLabel("label_117", "II LLLL II", "", 32, 59, 10);
            uiForm.setUiLabel("label_118", "TT", "", 32, 72, 2);
            uiForm.setUiLabel("label_119", "II EEEE", "", 32, 77, 7);
            uiForm.setUiLabel("label_120", "SSSS", "", 32, 86, 4);
            uiForm.setUiLabel("label_121", "************************************************************************************************************************************", 
                "", 38, 1, 132);
            uiForm.setUiLabel("label_122", "REPORT :", "", 40, 5, 8);
            uiForm.setUiControl("pnd_Report_Name", pnd_Report_Name, true, 40, 14, 45, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_123", "************************************************************************************************************************************", 
                "", 42, 1, 132);
            uiForm.setUiLabel("label_124", "There are no requests for the report of cases with", "", 44, 31, 50);
            uiForm.setUiLabel("label_125", "specified status.", "", 44, 82, 17);
            uiForm.setUiLabel("label_126", "There are no requests for the report of cases with", "", 45, 31, 50);
            uiForm.setUiLabel("label_127", "specified status.", "", 45, 82, 17);
            uiForm.setUiLabel("label_128", "There are no requests for the report of cases with", "", 46, 31, 50);
            uiForm.setUiLabel("label_129", "specified status.", "", 46, 82, 17);
            uiForm.setUiLabel("label_130", "************************************************************************************************************************************", 
                "", 58, 1, 132);
            uiForm.setUiLabel("label_131", "************************************************************************************************************************************", 
                "", 59, 1, 132);
            uiForm.setUiLabel("label_132", "************************************************************************************************************************************", 
                "", 60, 1, 132);
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
