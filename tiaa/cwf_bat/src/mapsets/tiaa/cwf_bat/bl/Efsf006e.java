/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:45:32 PM
**        *   FROM NATURAL MAP   :  Efsf006e
************************************************************
**        * FILE NAME               : Efsf006e.java
**        * CLASS NAME              : Efsf006e
**        * INSTANCE NAME           : Efsf006e
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #CONFLICT-DESC CWF-IMAGE-XREF.CABINET-ID                                                                                 *     CWF-IMAGE-XREF.IMAGE-DOCUMENT-ADDRESS-CDE 
    *     CWF-IMAGE-XREF.NUMBER-OF-PAGES CWF-IMAGE-XREF.UPLD-DATE-TIME                                                             *     ICW-EFM-DOCUMENT.CRTE-EMPL-RACF-ID 
    ICW-EFM-DOCUMENT.DCMT-MIN                                                             *     ICW-EFM-DOCUMENT.DCMT-ST-PAGE-NO ICW-EFM-DOCUMENT.PPG-CDE
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsf006e extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Conflict_Desc;
    private DbsField cwf_Image_Xref_Cabinet_Id;
    private DbsField cwf_Image_Xref_Image_Document_Address_Cde;
    private DbsField cwf_Image_Xref_Number_Of_Pages;
    private DbsField cwf_Image_Xref_Upld_Date_Time;
    private DbsField icw_Efm_Document_Crte_Empl_Racf_Id;
    private DbsField icw_Efm_Document_Dcmt_Min;
    private DbsField icw_Efm_Document_Dcmt_St_Page_No;
    private DbsField icw_Efm_Document_Ppg_Cde;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Conflict_Desc = parameters.newFieldInRecord("pnd_Conflict_Desc", "#CONFLICT-DESC", FieldType.STRING, 25);
        cwf_Image_Xref_Cabinet_Id = parameters.newFieldInRecord("cwf_Image_Xref_Cabinet_Id", "CWF-IMAGE-XREF.CABINET-ID", FieldType.STRING, 12);
        cwf_Image_Xref_Image_Document_Address_Cde = parameters.newFieldInRecord("cwf_Image_Xref_Image_Document_Address_Cde", "CWF-IMAGE-XREF.IMAGE-DOCUMENT-ADDRESS-CDE", 
            FieldType.STRING, 22);
        cwf_Image_Xref_Number_Of_Pages = parameters.newFieldInRecord("cwf_Image_Xref_Number_Of_Pages", "CWF-IMAGE-XREF.NUMBER-OF-PAGES", FieldType.NUMERIC, 
            3);
        cwf_Image_Xref_Upld_Date_Time = parameters.newFieldInRecord("cwf_Image_Xref_Upld_Date_Time", "CWF-IMAGE-XREF.UPLD-DATE-TIME", FieldType.TIME);
        icw_Efm_Document_Crte_Empl_Racf_Id = parameters.newFieldInRecord("icw_Efm_Document_Crte_Empl_Racf_Id", "ICW-EFM-DOCUMENT.CRTE-EMPL-RACF-ID", FieldType.STRING, 
            8);
        icw_Efm_Document_Dcmt_Min = parameters.newFieldInRecord("icw_Efm_Document_Dcmt_Min", "ICW-EFM-DOCUMENT.DCMT-MIN", FieldType.NUMERIC, 11);
        icw_Efm_Document_Dcmt_St_Page_No = parameters.newFieldInRecord("icw_Efm_Document_Dcmt_St_Page_No", "ICW-EFM-DOCUMENT.DCMT-ST-PAGE-NO", FieldType.NUMERIC, 
            3);
        icw_Efm_Document_Ppg_Cde = parameters.newFieldInRecord("icw_Efm_Document_Ppg_Cde", "ICW-EFM-DOCUMENT.PPG-CDE", FieldType.STRING, 6);
        parameters.reset();
    }

    public Efsf006e() throws Exception
    {
        super("Efsf006e");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Efsf006e", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Efsf006e"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("icw_Efm_Document_Ppg_Cde", icw_Efm_Document_Ppg_Cde, true, 1, 1, 6, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("icw_Efm_Document_Dcmt_Min", icw_Efm_Document_Dcmt_Min, true, 1, 10, 11, "WHITE", true, false, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("icw_Efm_Document_Dcmt_St_Page_No", icw_Efm_Document_Dcmt_St_Page_No, true, 1, 25, 3, "WHITE", true, false, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("icw_Efm_Document_Crte_Empl_Racf_Id", icw_Efm_Document_Crte_Empl_Racf_Id, true, 1, 34, 8, "WHITE", true, false, null, 
                null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("cwf_Image_Xref_Cabinet_Id", cwf_Image_Xref_Cabinet_Id, true, 1, 45, 9, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("cwf_Image_Xref_Number_Of_Pages", cwf_Image_Xref_Number_Of_Pages, true, 1, 57, 3, "WHITE", true, false, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("cwf_Image_Xref_Image_Document_Address_Cde", cwf_Image_Xref_Image_Document_Address_Cde, true, 1, 65, 22, "WHITE", true, 
                false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("cwf_Image_Xref_Upld_Date_Time", cwf_Image_Xref_Upld_Date_Time, true, 1, 89, 17, "WHITE", "MM/DD/YY' 'HH:II:SS", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Conflict_Desc", pnd_Conflict_Desc, true, 1, 108, 24, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
