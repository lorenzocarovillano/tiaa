/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:45:39 PM
**        *   FROM NATURAL MAP   :  Efsf761r
************************************************************
**        * FILE NAME               : Efsf761r.java
**        * CLASS NAME              : Efsf761r
**        * INSTANCE NAME           : Efsf761r
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #IMAGE-PLATTER-OLD #SUMMARY.#BATCH-COUNT #SUMMARY.#IMAGES-COUNT                                                          *     #SUMMARY.#MINS-COUNT 
    #SUMMARY.#MINS-NO-PIN-COUNT
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsf761r extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Image_Platter_Old;
    private DbsField pnd_Summary_Pnd_Batch_Count;
    private DbsField pnd_Summary_Pnd_Images_Count;
    private DbsField pnd_Summary_Pnd_Mins_Count;
    private DbsField pnd_Summary_Pnd_Mins_No_Pin_Count;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Image_Platter_Old = parameters.newFieldInRecord("pnd_Image_Platter_Old", "#IMAGE-PLATTER-OLD", FieldType.STRING, 10);
        pnd_Summary_Pnd_Batch_Count = parameters.newFieldInRecord("pnd_Summary_Pnd_Batch_Count", "#SUMMARY.#BATCH-COUNT", FieldType.NUMERIC, 5);
        pnd_Summary_Pnd_Images_Count = parameters.newFieldInRecord("pnd_Summary_Pnd_Images_Count", "#SUMMARY.#IMAGES-COUNT", FieldType.NUMERIC, 7);
        pnd_Summary_Pnd_Mins_Count = parameters.newFieldInRecord("pnd_Summary_Pnd_Mins_Count", "#SUMMARY.#MINS-COUNT", FieldType.NUMERIC, 7);
        pnd_Summary_Pnd_Mins_No_Pin_Count = parameters.newFieldInRecord("pnd_Summary_Pnd_Mins_No_Pin_Count", "#SUMMARY.#MINS-NO-PIN-COUNT", FieldType.NUMERIC, 
            7);
        parameters.reset();
    }

    public Efsf761r() throws Exception
    {
        super("Efsf761r");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Efsf761r", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Efsf761r"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Image_Platter_Old", pnd_Image_Platter_Old, true, 1, 26, 10, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Summary_Pnd_Batch_Count", pnd_Summary_Pnd_Batch_Count, true, 1, 39, 5, "WHITE", true, false, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Summary_Pnd_Mins_Count", pnd_Summary_Pnd_Mins_Count, true, 1, 53, 7, "WHITE", true, false, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Summary_Pnd_Mins_No_Pin_Count", pnd_Summary_Pnd_Mins_No_Pin_Count, true, 1, 66, 7, "WHITE", true, false, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Summary_Pnd_Images_Count", pnd_Summary_Pnd_Images_Count, true, 1, 80, 7, "WHITE", true, false, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
