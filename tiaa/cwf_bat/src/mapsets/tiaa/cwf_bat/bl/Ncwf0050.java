/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:55:11 PM
**        *   FROM NATURAL MAP   :  Ncwf0050
************************************************************
**        * FILE NAME               : Ncwf0050.java
**        * CLASS NAME              : Ncwf0050
**        * INSTANCE NAME           : Ncwf0050
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #PROGRAM #SOURCE-ID
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Ncwf0050 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Program;
    private DbsField pnd_Source_Id;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Program = parameters.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Source_Id = parameters.newFieldInRecord("pnd_Source_Id", "#SOURCE-ID", FieldType.STRING, 6);
        parameters.reset();
    }

    public Ncwf0050() throws Exception
    {
        super("Ncwf0050");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Ncwf0050", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Ncwf0050"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Program", pnd_Program, true, 1, 1, 8, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_1", "NCW - MINS DELETED AS OF :", "BLUE", 1, 31, 26);
            uiForm.setUiControl("astDATU", Global.getDATU(), true, 1, 59, 8, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("astTIME", Global.getTIME(), true, 1, 68, 10, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_2", "SOURCE ID :", "BLUE", 1, 77, 11);
            uiForm.setUiControl("pnd_Source_Id", pnd_Source_Id, true, 1, 89, 6, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_3", "PAGE:", "BLUE", 1, 110, 5);
            uiForm.setUiControl("astPAGE_NUMBER", getReports().getPageNumberDbs(0), true, 1, 116, 5, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_4", "-----------------------------------------------------------------------------------------------------------------------------------", 
                "", 2, 1, 131);
            uiForm.setUiLabel("label_5", "MIN", "BLUE", 3, 1, 3);
            uiForm.setUiLabel("label_6", "PIN", "BLUE", 3, 13, 3);
            uiForm.setUiLabel("label_7", "TIAA RCVD CASE SUB", "BLUE", 3, 26, 18);
            uiForm.setUiLabel("label_8", "REQUEST", "BLUE", 3, 58, 7);
            uiForm.setUiLabel("label_9", "EMPLOYEE", "BLUE", 3, 75, 8);
            uiForm.setUiLabel("label_10", "NUMBER", "BLUE", 4, 1, 6);
            uiForm.setUiLabel("label_11", "NUMBER", "BLUE", 4, 13, 6);
            uiForm.setUiLabel("label_12", "DATE", "BLUE", 4, 28, 4);
            uiForm.setUiLabel("label_13", "ID", "BLUE", 4, 37, 2);
            uiForm.setUiLabel("label_14", "IND", "BLUE", 4, 41, 3);
            uiForm.setUiLabel("label_15", "WPID", "BLUE", 4, 46, 4);
            uiForm.setUiLabel("label_16", "LOG DATE TIME", "BLUE", 4, 55, 13);
            uiForm.setUiLabel("label_17", "RACF ID", "BLUE", 4, 75, 7);
            uiForm.setUiLabel("label_18", "ERROR MESSAGE", "BLUE", 4, 87, 13);
            uiForm.setUiLabel("label_19", "-----------------------------------------------------------------------------------------------------------------------------------", 
                "", 5, 1, 131);
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
