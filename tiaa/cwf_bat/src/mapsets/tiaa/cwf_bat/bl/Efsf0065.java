/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:45:29 PM
**        *   FROM NATURAL MAP   :  Efsf0065
************************************************************
**        * FILE NAME               : Efsf0065.java
**        * CLASS NAME              : Efsf0065
**        * INSTANCE NAME           : Efsf0065
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #TOTAL.#BATCH-COUNT #TOTAL.#IMAGES-COUNT #TOTAL.#MINS-COUNT                                                              *     #TOTAL.#MINS-NO-PIN-COUNT
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsf0065 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Total_Pnd_Batch_Count;
    private DbsField pnd_Total_Pnd_Images_Count;
    private DbsField pnd_Total_Pnd_Mins_Count;
    private DbsField pnd_Total_Pnd_Mins_No_Pin_Count;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Total_Pnd_Batch_Count = parameters.newFieldInRecord("pnd_Total_Pnd_Batch_Count", "#TOTAL.#BATCH-COUNT", FieldType.NUMERIC, 5);
        pnd_Total_Pnd_Images_Count = parameters.newFieldInRecord("pnd_Total_Pnd_Images_Count", "#TOTAL.#IMAGES-COUNT", FieldType.NUMERIC, 7);
        pnd_Total_Pnd_Mins_Count = parameters.newFieldInRecord("pnd_Total_Pnd_Mins_Count", "#TOTAL.#MINS-COUNT", FieldType.NUMERIC, 7);
        pnd_Total_Pnd_Mins_No_Pin_Count = parameters.newFieldInRecord("pnd_Total_Pnd_Mins_No_Pin_Count", "#TOTAL.#MINS-NO-PIN-COUNT", FieldType.NUMERIC, 
            7);
        parameters.reset();
    }

    public Efsf0065() throws Exception
    {
        super("Efsf0065");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=026 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Efsf0065", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Efsf0065"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "**********************************************************************", "", 3, 19, 70);
            uiForm.setUiLabel("label_2", "*", "BLUE", 4, 19, 1);
            uiForm.setUiLabel("label_3", "I M A G E", "BLUE", 4, 28, 9);
            uiForm.setUiLabel("label_4", "U P L O A D", "BLUE", 4, 41, 11);
            uiForm.setUiLabel("label_5", "S U M M A R Y", "BLUE", 4, 56, 13);
            uiForm.setUiLabel("label_6", "*", "BLUE", 4, 88, 1);
            uiForm.setUiLabel("label_7", "*", "BLUE", 5, 19, 1);
            uiForm.setUiLabel("label_8", "*", "BLUE", 5, 88, 1);
            uiForm.setUiLabel("label_9", "*", "BLUE", 6, 19, 1);
            uiForm.setUiLabel("label_10", "*", "BLUE", 6, 88, 1);
            uiForm.setUiLabel("label_11", "*", "BLUE", 7, 19, 1);
            uiForm.setUiLabel("label_12", "TOTAL NUMBER OF UPLOADS EXECUTED", "BLUE", 7, 23, 32);
            uiForm.setUiLabel("label_13", "=", "BLUE", 7, 71, 1);
            uiForm.setUiControl("pnd_Total_Pnd_Batch_Count", pnd_Total_Pnd_Batch_Count, true, 7, 76, 5, "WHITE", true, true, null, "0123456789+-, ", "AD=DROFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_14", "*", "BLUE", 7, 88, 1);
            uiForm.setUiLabel("label_15", "*", "BLUE", 8, 19, 1);
            uiForm.setUiLabel("label_16", "*", "BLUE", 8, 88, 1);
            uiForm.setUiLabel("label_17", "*", "BLUE", 9, 19, 1);
            uiForm.setUiLabel("label_18", "TOTAL NUMBER OF MAIL ITEMS UPLOADED", "BLUE", 9, 23, 35);
            uiForm.setUiLabel("label_19", "=", "BLUE", 9, 71, 1);
            uiForm.setUiControl("pnd_Total_Pnd_Mins_Count", pnd_Total_Pnd_Mins_Count, true, 9, 74, 7, "WHITE", true, true, null, "0123456789+-, ", "AD=DROFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_20", "*", "BLUE", 9, 88, 1);
            uiForm.setUiLabel("label_21", "*", "BLUE", 10, 19, 1);
            uiForm.setUiLabel("label_22", "*", "BLUE", 10, 88, 1);
            uiForm.setUiLabel("label_23", "*", "BLUE", 11, 19, 1);
            uiForm.setUiLabel("label_24", "TOTAL NUMBER OF MAIL ITEMS UPLOADED WITH NO PIN =", "BLUE", 11, 23, 49);
            uiForm.setUiControl("pnd_Total_Pnd_Mins_No_Pin_Count", pnd_Total_Pnd_Mins_No_Pin_Count, true, 11, 74, 7, "WHITE", true, false, null, "0123456789+-, ", 
                "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_25", "*", "BLUE", 11, 88, 1);
            uiForm.setUiLabel("label_26", "*", "BLUE", 12, 19, 1);
            uiForm.setUiLabel("label_27", "*", "BLUE", 12, 88, 1);
            uiForm.setUiLabel("label_28", "*", "BLUE", 13, 19, 1);
            uiForm.setUiLabel("label_29", "TOTAL NUMBER OF IMAGES PROCESSED", "BLUE", 13, 23, 32);
            uiForm.setUiLabel("label_30", "=", "BLUE", 13, 71, 1);
            uiForm.setUiControl("pnd_Total_Pnd_Images_Count", pnd_Total_Pnd_Images_Count, true, 13, 74, 7, "WHITE", true, true, null, "0123456789+-, ", 
                "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_31", "*", "BLUE", 13, 88, 1);
            uiForm.setUiLabel("label_32", "*", "BLUE", 14, 19, 1);
            uiForm.setUiLabel("label_33", "*", "BLUE", 14, 88, 1);
            uiForm.setUiLabel("label_34", "**********************************************************************", "", 15, 19, 70);
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
