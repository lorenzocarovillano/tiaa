/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:55:11 PM
**        *   FROM NATURAL MAP   :  Ncwf0051
************************************************************
**        * FILE NAME               : Ncwf0051.java
**        * CLASS NAME              : Ncwf0051
**        * INSTANCE NAME           : Ncwf0051
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #RQST-ID.#CASE-ID #RQST-ID.#SUBRQST-ID #RQST-ID.#TIAA-RCVD-DTE                                                           *     #RQST-ID.#WPID 
    NCW-EFM-AUDIT.EMPL-OPRTR-CDE                                                                              *     NCW-EFM-AUDIT.ERROR-MSG NCW-EFM-AUDIT.IMAGE-MIN 
    *     NCW-EFM-AUDIT.LOG-DTE-TME NCW-EFM-AUDIT.NP-PIN
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Ncwf0051 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Rqst_Id_Pnd_Case_Id;
    private DbsField pnd_Rqst_Id_Pnd_Subrqst_Id;
    private DbsField pnd_Rqst_Id_Pnd_Tiaa_Rcvd_Dte;
    private DbsField pnd_Rqst_Id_Pnd_Wpid;
    private DbsField ncw_Efm_Audit_Empl_Oprtr_Cde;
    private DbsField ncw_Efm_Audit_Error_Msg;
    private DbsField ncw_Efm_Audit_Image_Min;
    private DbsField ncw_Efm_Audit_Log_Dte_Tme;
    private DbsField ncw_Efm_Audit_Np_Pin;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Rqst_Id_Pnd_Case_Id = parameters.newFieldInRecord("pnd_Rqst_Id_Pnd_Case_Id", "#RQST-ID.#CASE-ID", FieldType.STRING, 1);
        pnd_Rqst_Id_Pnd_Subrqst_Id = parameters.newFieldInRecord("pnd_Rqst_Id_Pnd_Subrqst_Id", "#RQST-ID.#SUBRQST-ID", FieldType.STRING, 1);
        pnd_Rqst_Id_Pnd_Tiaa_Rcvd_Dte = parameters.newFieldInRecord("pnd_Rqst_Id_Pnd_Tiaa_Rcvd_Dte", "#RQST-ID.#TIAA-RCVD-DTE", FieldType.STRING, 8);
        pnd_Rqst_Id_Pnd_Wpid = parameters.newFieldInRecord("pnd_Rqst_Id_Pnd_Wpid", "#RQST-ID.#WPID", FieldType.STRING, 6);
        ncw_Efm_Audit_Empl_Oprtr_Cde = parameters.newFieldInRecord("ncw_Efm_Audit_Empl_Oprtr_Cde", "NCW-EFM-AUDIT.EMPL-OPRTR-CDE", FieldType.STRING, 8);
        ncw_Efm_Audit_Error_Msg = parameters.newFieldInRecord("ncw_Efm_Audit_Error_Msg", "NCW-EFM-AUDIT.ERROR-MSG", FieldType.STRING, 30);
        ncw_Efm_Audit_Image_Min = parameters.newFieldInRecord("ncw_Efm_Audit_Image_Min", "NCW-EFM-AUDIT.IMAGE-MIN", FieldType.STRING, 11);
        ncw_Efm_Audit_Log_Dte_Tme = parameters.newFieldInRecord("ncw_Efm_Audit_Log_Dte_Tme", "NCW-EFM-AUDIT.LOG-DTE-TME", FieldType.TIME);
        ncw_Efm_Audit_Np_Pin = parameters.newFieldInRecord("ncw_Efm_Audit_Np_Pin", "NCW-EFM-AUDIT.NP-PIN", FieldType.STRING, 7);
        parameters.reset();
    }

    public Ncwf0051() throws Exception
    {
        super("Ncwf0051");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Ncwf0051", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Ncwf0051"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("ncw_Efm_Audit_Image_Min", ncw_Efm_Audit_Image_Min, true, 1, 1, 11, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("ncw_Efm_Audit_Np_Pin", ncw_Efm_Audit_Np_Pin, true, 1, 13, 7, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rqst_Id_Pnd_Tiaa_Rcvd_Dte", pnd_Rqst_Id_Pnd_Tiaa_Rcvd_Dte, true, 1, 26, 10, "WHITE", "XXXX/XX/XX", true, false, null, 
                null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rqst_Id_Pnd_Case_Id", pnd_Rqst_Id_Pnd_Case_Id, true, 1, 38, 1, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Rqst_Id_Pnd_Subrqst_Id", pnd_Rqst_Id_Pnd_Subrqst_Id, true, 1, 42, 1, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Rqst_Id_Pnd_Wpid", pnd_Rqst_Id_Pnd_Wpid, true, 1, 46, 6, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("ncw_Efm_Audit_Log_Dte_Tme", ncw_Efm_Audit_Log_Dte_Tme, true, 1, 53, 0, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("ncw_Efm_Audit_Empl_Oprtr_Cde", ncw_Efm_Audit_Empl_Oprtr_Cde, true, 1, 76, 8, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("ncw_Efm_Audit_Error_Msg", ncw_Efm_Audit_Error_Msg, true, 1, 87, 30, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
