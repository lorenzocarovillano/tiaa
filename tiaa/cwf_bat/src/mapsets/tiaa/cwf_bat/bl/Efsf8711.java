/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:45:42 PM
**        *   FROM NATURAL MAP   :  Efsf8711
************************************************************
**        * FILE NAME               : Efsf8711.java
**        * CLASS NAME              : Efsf8711
**        * INSTANCE NAME           : Efsf8711
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #PREV-SOURCE #PROGRAM
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsf8711 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Prev_Source;
    private DbsField pnd_Program;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Prev_Source = parameters.newFieldInRecord("pnd_Prev_Source", "#PREV-SOURCE", FieldType.STRING, 6);
        pnd_Program = parameters.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        parameters.reset();
    }

    public Efsf8711() throws Exception
    {
        super("Efsf8711");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=004 LS=132 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Efsf8711", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Efsf8711"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Program", pnd_Program, true, 1, 1, 8, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_1", "CWF - MINs ON XREF BUT NOT ON DOCUMENT FILE FOR", "BLUE", 1, 13, 47);
            uiForm.setUiControl("pnd_Prev_Source", pnd_Prev_Source, true, 1, 61, 6, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_2", "AS OF", "BLUE", 1, 68, 5);
            uiForm.setUiControl("astDATU", Global.getDATU(), true, 1, 74, 8, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("astTIME", Global.getTIME(), true, 1, 83, 10, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_3", "PAGE:", "BLUE", 1, 120, 5);
            uiForm.setUiControl("astPAGE_NUMBER", getReports().getPageNumberDbs(0), true, 1, 126, 5, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_4", "-----------------------------------------------------------------------------------------------------------------------------------", 
                "", 2, 1, 131);
            uiForm.setUiLabel("label_5", "Mail Item Nbr", "BLUE", 3, 2, 13);
            uiForm.setUiLabel("label_6", "Source", "BLUE", 3, 18, 6);
            uiForm.setUiLabel("label_7", "Cabinet ID", "BLUE", 3, 27, 10);
            uiForm.setUiLabel("label_8", "Poor Image", "BLUE", 3, 40, 10);
            uiForm.setUiLabel("label_9", "Upload Date /Time", "BLUE", 3, 53, 17);
            uiForm.setUiLabel("label_10", "Docmnt PageNbr", "BLUE", 3, 72, 14);
            uiForm.setUiLabel("label_11", "Image Document Address", "BLUE", 3, 88, 22);
            uiForm.setUiLabel("label_12", "Identify Date/Time", "BLUE", 3, 113, 18);
            uiForm.setUiLabel("label_13", "-----------------------------------------------------------------------------------------------------------------------------------", 
                "", 4, 1, 131);
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
