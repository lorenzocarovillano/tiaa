/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:45:43 PM
**        *   FROM NATURAL MAP   :  Efsf9030
************************************************************
**        * FILE NAME               : Efsf9030.java
**        * CLASS NAME              : Efsf9030
**        * INSTANCE NAME           : Efsf9030
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #ABORT-MESSAGE(*)
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsf9030 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Abort_Message;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Abort_Message = parameters.newFieldArrayInRecord("pnd_Abort_Message", "#ABORT-MESSAGE", FieldType.STRING, 80, new DbsArrayController(1, 2));
        parameters.reset();
    }

    public Efsf9030() throws Exception
    {
        super("Efsf9030");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=045 LS=132 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Efsf9030", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Efsf9030"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "********** ABORT ********** ABORT ********** ABORT", "BLUE", 1, 1, 50);
            uiForm.setUiLabel("label_2", "********** ABORT ********** ABORT ********** ABORT", "BLUE", 1, 52, 50);
            uiForm.setUiLabel("label_3", "********** ABORT ***********", "BLUE", 1, 103, 28);
            uiForm.setUiLabel("label_4", "********** ABORT ********** ABORT ********** ABORT", "BLUE", 2, 1, 50);
            uiForm.setUiLabel("label_5", "********** ABORT ********** ABORT ********** ABORT", "BLUE", 2, 52, 50);
            uiForm.setUiLabel("label_6", "********** ABORT ***********", "BLUE", 2, 103, 28);
            uiForm.setUiLabel("label_7", "********** ABORT ********** ABORT ********** ABORT", "BLUE", 3, 1, 50);
            uiForm.setUiLabel("label_8", "********** ABORT ********** ABORT ********** ABORT", "BLUE", 3, 52, 50);
            uiForm.setUiLabel("label_9", "********** ABORT ***********", "BLUE", 3, 103, 28);
            uiForm.setUiLabel("label_10", "*************************************************************************************", "", 9, 14, 85);
            uiForm.setUiLabel("label_11", "*", "BLUE", 10, 14, 1);
            uiForm.setUiLabel("label_12", "*", "BLUE", 10, 98, 1);
            uiForm.setUiLabel("label_13", "*", "BLUE", 11, 14, 1);
            uiForm.setUiLabel("label_14", "MIT / EFM CONTROL MODULE STATISTICS SUMMARIZATION ABORTED", "BLUE", 11, 17, 57);
            uiForm.setUiLabel("label_15", "BECAUSE :", "BLUE", 11, 75, 9);
            uiForm.setUiLabel("label_16", "*", "BLUE", 11, 98, 1);
            uiForm.setUiLabel("label_17", "*", "BLUE", 12, 14, 1);
            uiForm.setUiLabel("label_18", "*", "BLUE", 12, 98, 1);
            uiForm.setUiLabel("label_19", "*", "BLUE", 13, 14, 1);
            uiForm.setUiControl("pnd_Abort_Message_1pls0", pnd_Abort_Message.getValue(1+0), true, 13, 17, 80, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_20", "*", "BLUE", 13, 98, 1);
            uiForm.setUiLabel("label_21", "*", "BLUE", 14, 14, 1);
            uiForm.setUiControl("pnd_Abort_Message_1pls1", pnd_Abort_Message.getValue(1+1), true, 14, 17, 80, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_22", "*", "BLUE", 14, 98, 1);
            uiForm.setUiLabel("label_23", "*", "BLUE", 15, 14, 1);
            uiForm.setUiLabel("label_24", "*", "BLUE", 15, 98, 1);
            uiForm.setUiLabel("label_25", "*", "BLUE", 16, 14, 1);
            uiForm.setUiLabel("label_26", "DATE & TIME :", "BLUE", 16, 34, 13);
            uiForm.setUiControl("astDATU", Global.getDATU(), true, 16, 49, 8, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("astTIME", Global.getTIME(), true, 16, 58, 10, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_27", "*", "BLUE", 16, 98, 1);
            uiForm.setUiLabel("label_28", "*", "BLUE", 17, 14, 1);
            uiForm.setUiLabel("label_29", "*", "BLUE", 17, 98, 1);
            uiForm.setUiLabel("label_30", "*", "BLUE", 18, 14, 1);
            uiForm.setUiLabel("label_31", "PROGRAM :", "BLUE", 18, 38, 9);
            uiForm.setUiControl("astPROGRAM", Global.getPROGRAM(), true, 18, 49, 8, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_32", "*", "BLUE", 18, 98, 1);
            uiForm.setUiLabel("label_33", "*", "BLUE", 19, 14, 1);
            uiForm.setUiLabel("label_34", "*", "BLUE", 19, 98, 1);
            uiForm.setUiLabel("label_35", "*", "BLUE", 20, 14, 1);
            uiForm.setUiLabel("label_36", "ERROR NBR :", "BLUE", 20, 36, 11);
            uiForm.setUiControl("astERROR_NR", Global.getERROR_NR(), true, 20, 49, 0, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_37", "*", "BLUE", 20, 98, 1);
            uiForm.setUiLabel("label_38", "*", "BLUE", 21, 14, 1);
            uiForm.setUiLabel("label_39", "*", "BLUE", 21, 98, 1);
            uiForm.setUiLabel("label_40", "*", "BLUE", 22, 14, 1);
            uiForm.setUiLabel("label_41", "ERROR LINE :", "BLUE", 22, 35, 12);
            uiForm.setUiControl("astERROR_LINE", Global.getERROR_LINE(), true, 22, 49, 0, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_42", "*", "BLUE", 22, 98, 1);
            uiForm.setUiLabel("label_43", "*", "BLUE", 23, 14, 1);
            uiForm.setUiLabel("label_44", "*", "BLUE", 23, 98, 1);
            uiForm.setUiLabel("label_45", "*************************************************************************************", "", 24, 14, 85);
            uiForm.setUiLabel("label_46", "********** ABORT ********** ABORT ********** ABORT", "BLUE", 30, 1, 50);
            uiForm.setUiLabel("label_47", "********** ABORT ********** ABORT ********** ABORT", "BLUE", 30, 52, 50);
            uiForm.setUiLabel("label_48", "********** ABORT ***********", "BLUE", 30, 103, 28);
            uiForm.setUiLabel("label_49", "********** ABORT ********** ABORT ********** ABORT", "BLUE", 31, 1, 50);
            uiForm.setUiLabel("label_50", "********** ABORT ********** ABORT ********** ABORT", "BLUE", 31, 52, 50);
            uiForm.setUiLabel("label_51", "********** ABORT ***********", "BLUE", 31, 103, 28);
            uiForm.setUiLabel("label_52", "********** ABORT ********** ABORT ********** ABORT", "BLUE", 32, 1, 50);
            uiForm.setUiLabel("label_53", "********** ABORT ********** ABORT ********** ABORT", "BLUE", 32, 52, 50);
            uiForm.setUiLabel("label_54", "********** ABORT ***********", "BLUE", 32, 103, 28);
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
