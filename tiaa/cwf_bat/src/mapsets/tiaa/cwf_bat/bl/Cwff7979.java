/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:41:11 PM
**        *   FROM NATURAL MAP   :  Cwff7979
************************************************************
**        * FILE NAME               : Cwff7979.java
**        * CLASS NAME              : Cwff7979
**        * INSTANCE NAME           : Cwff7979
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     #POWERFILE #SOURCE-CD CWF-EFM-CABINET.AUDIT-EMPL-RACF-ID                                                                 *     CWF-EFM-CABINET.CABINET-PIN 
    CWF-EFM-CABINET.MEDIA-UPDTE-DTE-TME
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwff7979 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Powerfile;
    private DbsField pnd_Source_Cd;
    private DbsField cwf_Efm_Cabinet_Audit_Empl_Racf_Id;
    private DbsField cwf_Efm_Cabinet_Cabinet_Pin;
    private DbsField cwf_Efm_Cabinet_Media_Updte_Dte_Tme;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Powerfile = parameters.newFieldInRecord("pnd_Powerfile", "#POWERFILE", FieldType.STRING, 2);
        pnd_Source_Cd = parameters.newFieldInRecord("pnd_Source_Cd", "#SOURCE-CD", FieldType.STRING, 12);
        cwf_Efm_Cabinet_Audit_Empl_Racf_Id = parameters.newFieldInRecord("cwf_Efm_Cabinet_Audit_Empl_Racf_Id", "CWF-EFM-CABINET.AUDIT-EMPL-RACF-ID", FieldType.STRING, 
            8);
        cwf_Efm_Cabinet_Cabinet_Pin = parameters.newFieldInRecord("cwf_Efm_Cabinet_Cabinet_Pin", "CWF-EFM-CABINET.CABINET-PIN", FieldType.NUMERIC, 12);
        cwf_Efm_Cabinet_Media_Updte_Dte_Tme = parameters.newFieldInRecord("cwf_Efm_Cabinet_Media_Updte_Dte_Tme", "CWF-EFM-CABINET.MEDIA-UPDTE-DTE-TME", 
            FieldType.TIME);
        parameters.reset();
    }

    public Cwff7979() throws Exception
    {
        super("Cwff7979");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Cwff7979", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Cwff7979"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Powerfile", pnd_Powerfile, true, 1, 4, 2, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("cwf_Efm_Cabinet_Cabinet_Pin", cwf_Efm_Cabinet_Cabinet_Pin, true, 1, 11, 12, "WHITE", true, false, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Source_Cd", pnd_Source_Cd, true, 1, 25, 12, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("cwf_Efm_Cabinet_Media_Updte_Dte_Tme", cwf_Efm_Cabinet_Media_Updte_Dte_Tme, true, 1, 42, 17, "WHITE", "MM/DD/YY' 'HH:II:SS", 
                true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("cwf_Efm_Cabinet_Audit_Empl_Racf_Id", cwf_Efm_Cabinet_Audit_Empl_Racf_Id, true, 1, 69, 8, "WHITE", true, false, null, 
                null, "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
