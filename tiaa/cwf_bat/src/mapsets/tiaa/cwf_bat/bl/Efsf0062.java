/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:45:28 PM
**        *   FROM NATURAL MAP   :  Efsf0062
************************************************************
**        * FILE NAME               : Efsf0062.java
**        * CLASS NAME              : Efsf0062
**        * INSTANCE NAME           : Efsf0062
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #TOTAL.#BATCH-FINISHED #TOTAL.#BATCH-UNFINISHED                                                                          *     #TOTAL.#IMAGES-PROCESSED 
    #TOTAL.#MINS-NOT-UPLOADED                                                                       *     #TOTAL.#MINS-UPLOADED #TOTAL.#UPLOAD-COUNT
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsf0062 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Total_Pnd_Batch_Finished;
    private DbsField pnd_Total_Pnd_Batch_Unfinished;
    private DbsField pnd_Total_Pnd_Images_Processed;
    private DbsField pnd_Total_Pnd_Mins_Not_Uploaded;
    private DbsField pnd_Total_Pnd_Mins_Uploaded;
    private DbsField pnd_Total_Pnd_Upload_Count;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Total_Pnd_Batch_Finished = parameters.newFieldInRecord("pnd_Total_Pnd_Batch_Finished", "#TOTAL.#BATCH-FINISHED", FieldType.NUMERIC, 5);
        pnd_Total_Pnd_Batch_Unfinished = parameters.newFieldInRecord("pnd_Total_Pnd_Batch_Unfinished", "#TOTAL.#BATCH-UNFINISHED", FieldType.NUMERIC, 
            5);
        pnd_Total_Pnd_Images_Processed = parameters.newFieldInRecord("pnd_Total_Pnd_Images_Processed", "#TOTAL.#IMAGES-PROCESSED", FieldType.NUMERIC, 
            7);
        pnd_Total_Pnd_Mins_Not_Uploaded = parameters.newFieldInRecord("pnd_Total_Pnd_Mins_Not_Uploaded", "#TOTAL.#MINS-NOT-UPLOADED", FieldType.NUMERIC, 
            7);
        pnd_Total_Pnd_Mins_Uploaded = parameters.newFieldInRecord("pnd_Total_Pnd_Mins_Uploaded", "#TOTAL.#MINS-UPLOADED", FieldType.NUMERIC, 7);
        pnd_Total_Pnd_Upload_Count = parameters.newFieldInRecord("pnd_Total_Pnd_Upload_Count", "#TOTAL.#UPLOAD-COUNT", FieldType.NUMERIC, 5);
        parameters.reset();
    }

    public Efsf0062() throws Exception
    {
        super("Efsf0062");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=021 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Efsf0062", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Efsf0062"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "*******************************************************************", "", 3, 19, 67);
            uiForm.setUiLabel("label_2", "*", "BLUE", 4, 19, 1);
            uiForm.setUiLabel("label_3", "I M A G E", "BLUE", 4, 30, 9);
            uiForm.setUiLabel("label_4", "U P L O A D", "BLUE", 4, 43, 11);
            uiForm.setUiLabel("label_5", "S U M M A R Y", "BLUE", 4, 58, 13);
            uiForm.setUiLabel("label_6", "*", "BLUE", 4, 85, 1);
            uiForm.setUiLabel("label_7", "*", "BLUE", 5, 19, 1);
            uiForm.setUiLabel("label_8", "*", "BLUE", 5, 85, 1);
            uiForm.setUiLabel("label_9", "*", "BLUE", 6, 19, 1);
            uiForm.setUiLabel("label_10", "*", "BLUE", 6, 85, 1);
            uiForm.setUiLabel("label_11", "*", "BLUE", 7, 19, 1);
            uiForm.setUiLabel("label_12", "TOTAL NUMBER OF UPLOADS EXECUTED", "BLUE", 7, 23, 32);
            uiForm.setUiLabel("label_13", "=", "BLUE", 7, 71, 1);
            uiForm.setUiControl("pnd_Total_Pnd_Upload_Count", pnd_Total_Pnd_Upload_Count, true, 7, 77, 5, "WHITE", true, true, null, "0123456789+-, ", 
                "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_14", "*", "BLUE", 7, 85, 1);
            uiForm.setUiLabel("label_15", "*", "BLUE", 8, 19, 1);
            uiForm.setUiLabel("label_16", "*", "BLUE", 8, 85, 1);
            uiForm.setUiLabel("label_17", "*", "BLUE", 9, 19, 1);
            uiForm.setUiLabel("label_18", "TOTAL NUMBER OF BATCHES COMPLETELY UPLOADED", "BLUE", 9, 23, 43);
            uiForm.setUiLabel("label_19", "=", "BLUE", 9, 71, 1);
            uiForm.setUiControl("pnd_Total_Pnd_Batch_Finished", pnd_Total_Pnd_Batch_Finished, true, 9, 77, 5, "WHITE", true, true, null, "0123456789+-, ", 
                "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_20", "*", "BLUE", 9, 85, 1);
            uiForm.setUiLabel("label_21", "*", "BLUE", 10, 19, 1);
            uiForm.setUiLabel("label_22", "*", "BLUE", 10, 85, 1);
            uiForm.setUiLabel("label_23", "*", "BLUE", 11, 19, 1);
            uiForm.setUiLabel("label_24", "TOTAL NUMBER OF BATCHES NOT COMPLETELY UPLOADED =", "BLUE", 11, 23, 49);
            uiForm.setUiControl("pnd_Total_Pnd_Batch_Unfinished", pnd_Total_Pnd_Batch_Unfinished, true, 11, 77, 5, "WHITE", true, true, null, "0123456789+-, ", 
                "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_25", "*", "BLUE", 11, 85, 1);
            uiForm.setUiLabel("label_26", "*", "BLUE", 12, 19, 1);
            uiForm.setUiLabel("label_27", "*", "BLUE", 12, 85, 1);
            uiForm.setUiLabel("label_28", "*", "BLUE", 13, 19, 1);
            uiForm.setUiLabel("label_29", "*", "BLUE", 13, 85, 1);
            uiForm.setUiLabel("label_30", "*", "BLUE", 14, 19, 1);
            uiForm.setUiLabel("label_31", "TOTAL NUMBER OF MINS UPLOADED", "BLUE", 14, 23, 29);
            uiForm.setUiLabel("label_32", "=", "BLUE", 14, 71, 1);
            uiForm.setUiControl("pnd_Total_Pnd_Mins_Uploaded", pnd_Total_Pnd_Mins_Uploaded, true, 14, 75, 7, "WHITE", true, true, null, "0123456789+-, ", 
                "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_33", "*", "BLUE", 14, 85, 1);
            uiForm.setUiLabel("label_34", "*", "BLUE", 15, 19, 1);
            uiForm.setUiLabel("label_35", "*", "BLUE", 15, 85, 1);
            uiForm.setUiLabel("label_36", "*", "BLUE", 16, 19, 1);
            uiForm.setUiLabel("label_37", "TOTAL NUMBER OF MINS OUTSTANDING", "BLUE", 16, 23, 32);
            uiForm.setUiLabel("label_38", "=", "BLUE", 16, 71, 1);
            uiForm.setUiControl("pnd_Total_Pnd_Mins_Not_Uploaded", pnd_Total_Pnd_Mins_Not_Uploaded, true, 16, 75, 7, "WHITE", true, true, null, "0123456789+-, ", 
                "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_39", "*", "BLUE", 16, 85, 1);
            uiForm.setUiLabel("label_40", "*", "BLUE", 17, 19, 1);
            uiForm.setUiLabel("label_41", "*", "BLUE", 17, 85, 1);
            uiForm.setUiLabel("label_42", "*", "BLUE", 18, 19, 1);
            uiForm.setUiLabel("label_43", "*", "BLUE", 18, 85, 1);
            uiForm.setUiLabel("label_44", "*", "BLUE", 19, 19, 1);
            uiForm.setUiLabel("label_45", "TOTAL NUMBER OF IMAGES PROCESSED", "BLUE", 19, 23, 32);
            uiForm.setUiLabel("label_46", "=", "BLUE", 19, 71, 1);
            uiForm.setUiControl("pnd_Total_Pnd_Images_Processed", pnd_Total_Pnd_Images_Processed, true, 19, 75, 7, "WHITE", true, true, null, "0123456789+-, ", 
                "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_47", "*", "BLUE", 19, 85, 1);
            uiForm.setUiLabel("label_48", "*", "BLUE", 20, 19, 1);
            uiForm.setUiLabel("label_49", "*", "BLUE", 20, 85, 1);
            uiForm.setUiLabel("label_50", "*******************************************************************", "", 21, 19, 67);
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
