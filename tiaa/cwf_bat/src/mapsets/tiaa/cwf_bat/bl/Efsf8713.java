/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:45:43 PM
**        *   FROM NATURAL MAP   :  Efsf8713
************************************************************
**        * FILE NAME               : Efsf8713.java
**        * CLASS NAME              : Efsf8713
**        * INSTANCE NAME           : Efsf8713
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     #CABINET-ID #CASE-WORKPRCSS-MULTI-SUBRQST #CURR-UNIT                                                                     *     #DOCUMENT-KEY-33 
    #ENTRY-DTE-TME #IMAGE-SOURCE-ID #MIN-SAVE                                                               *     #TIAA-RCVD-DTE
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsf8713 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Cabinet_Id;
    private DbsField pnd_Case_Workprcss_Multi_Subrqst;
    private DbsField pnd_Curr_Unit;
    private DbsField pnd_Document_Key_33;
    private DbsField pnd_Entry_Dte_Tme;
    private DbsField pnd_Image_Source_Id;
    private DbsField pnd_Min_Save;
    private DbsField pnd_Tiaa_Rcvd_Dte;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Cabinet_Id = parameters.newFieldInRecord("pnd_Cabinet_Id", "#CABINET-ID", FieldType.STRING, 14);
        pnd_Case_Workprcss_Multi_Subrqst = parameters.newFieldInRecord("pnd_Case_Workprcss_Multi_Subrqst", "#CASE-WORKPRCSS-MULTI-SUBRQST", FieldType.STRING, 
            9);
        pnd_Curr_Unit = parameters.newFieldInRecord("pnd_Curr_Unit", "#CURR-UNIT", FieldType.STRING, 8);
        pnd_Document_Key_33 = parameters.newFieldInRecord("pnd_Document_Key_33", "#DOCUMENT-KEY-33", FieldType.STRING, 38);
        pnd_Entry_Dte_Tme = parameters.newFieldInRecord("pnd_Entry_Dte_Tme", "#ENTRY-DTE-TME", FieldType.TIME);
        pnd_Image_Source_Id = parameters.newFieldInRecord("pnd_Image_Source_Id", "#IMAGE-SOURCE-ID", FieldType.STRING, 6);
        pnd_Min_Save = parameters.newFieldInRecord("pnd_Min_Save", "#MIN-SAVE", FieldType.NUMERIC, 11);
        pnd_Tiaa_Rcvd_Dte = parameters.newFieldInRecord("pnd_Tiaa_Rcvd_Dte", "#TIAA-RCVD-DTE", FieldType.DATE);
        parameters.reset();
    }

    public Efsf8713() throws Exception
    {
        super("Efsf8713");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=001 LS=132 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Efsf8713", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Efsf8713"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Min_Save", pnd_Min_Save, true, 1, 4, 11, "WHITE", true, true, null, "0123456789+-, ", "AD=DZOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Image_Source_Id", pnd_Image_Source_Id, true, 1, 17, 6, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cabinet_Id", pnd_Cabinet_Id, true, 1, 25, 14, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Rcvd_Dte", pnd_Tiaa_Rcvd_Dte, true, 1, 41, 0, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Case_Workprcss_Multi_Subrqst", pnd_Case_Workprcss_Multi_Subrqst, true, 1, 51, 9, "WHITE", true, false, null, null, 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Document_Key_33", pnd_Document_Key_33, true, 1, 62, 38, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Curr_Unit", pnd_Curr_Unit, true, 1, 101, 8, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Entry_Dte_Tme", pnd_Entry_Dte_Tme, true, 1, 111, 19, "WHITE", "MM/DD/YYYY' 'HH:II:SS", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
