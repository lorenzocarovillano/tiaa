/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:45:39 PM
**        *   FROM NATURAL MAP   :  Efsf761s
************************************************************
**        * FILE NAME               : Efsf761s.java
**        * CLASS NAME              : Efsf761s
**        * INSTANCE NAME           : Efsf761s
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #END-KEY #INPUT-SOURCE-ID #INPUT-TIME-END #INPUT-TIME-START                                                              *     #PROGRAM #START-KEY
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsf761s extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_End_Key;
    private DbsField pnd_Input_Source_Id;
    private DbsField pnd_Input_Time_End;
    private DbsField pnd_Input_Time_Start;
    private DbsField pnd_Program;
    private DbsField pnd_Start_Key;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_End_Key = parameters.newFieldInRecord("pnd_End_Key", "#END-KEY", FieldType.TIME);
        pnd_Input_Source_Id = parameters.newFieldInRecord("pnd_Input_Source_Id", "#INPUT-SOURCE-ID", FieldType.STRING, 6);
        pnd_Input_Time_End = parameters.newFieldInRecord("pnd_Input_Time_End", "#INPUT-TIME-END", FieldType.STRING, 6);
        pnd_Input_Time_Start = parameters.newFieldInRecord("pnd_Input_Time_Start", "#INPUT-TIME-START", FieldType.STRING, 6);
        pnd_Program = parameters.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Start_Key = parameters.newFieldInRecord("pnd_Start_Key", "#START-KEY", FieldType.TIME);
        parameters.reset();
    }

    public Efsf761s() throws Exception
    {
        super("Efsf761s");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Efsf761s", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Efsf761s"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Program", pnd_Program, true, 1, 1, 8, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_1", "CWF IMAGE UPLOAD DAILY ACTIVITY (TIME SEQUENCE) FOR", "BLUE", 1, 15, 51);
            uiForm.setUiControl("pnd_Input_Source_Id", pnd_Input_Source_Id, true, 1, 67, 6, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Start_Key", pnd_Start_Key, true, 1, 74, 10, "WHITE", "MM/DD/YYYY", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Input_Time_Start", pnd_Input_Time_Start, true, 1, 85, 8, "WHITE", "XX':'XX':'XX", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_2", "TO", "BLUE", 1, 94, 2);
            uiForm.setUiControl("pnd_End_Key", pnd_End_Key, true, 1, 97, 10, "WHITE", "MM/DD/YYYY", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Input_Time_End", pnd_Input_Time_End, true, 1, 108, 8, "WHITE", "XX':'XX':'XX", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_3", "PAGE:", "BLUE", 1, 121, 5);
            uiForm.setUiControl("astPAGE_NUMBER", getReports().getPageNumberDbs(0), true, 1, 127, 5, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_4", "AS OF", "BLUE", 2, 56, 5);
            uiForm.setUiControl("astDATU", Global.getDATU(), true, 2, 62, 8, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("astTIME", Global.getTIME(), true, 2, 71, 10, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_5", "-----------------------------------------------------------------------------------------------------------------------------------", 
                "", 3, 1, 131);
            uiForm.setUiLabel("label_6", "NO PIN", "BLUE", 4, 66, 6);
            uiForm.setUiLabel("label_7", "UPLOADS", "BLUE", 5, 38, 7);
            uiForm.setUiLabel("label_8", "MINS", "BLUE", 5, 53, 4);
            uiForm.setUiLabel("label_9", "MINS", "BLUE", 5, 66, 4);
            uiForm.setUiLabel("label_10", "IMAGES", "BLUE", 5, 80, 6);
            uiForm.setUiLabel("label_11", "PLATTER", "BLUE", 6, 26, 7);
            uiForm.setUiLabel("label_12", "EXECUTED", "BLUE", 6, 38, 8);
            uiForm.setUiLabel("label_13", "UPLOADED", "BLUE", 6, 53, 8);
            uiForm.setUiLabel("label_14", "UPLOADED", "BLUE", 6, 66, 8);
            uiForm.setUiLabel("label_15", "UPLOADED", "BLUE", 6, 80, 8);
            uiForm.setUiLabel("label_16", "-----------------------------------------------------------------------------------------------------------------------------------", 
                "", 7, 1, 131);
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
