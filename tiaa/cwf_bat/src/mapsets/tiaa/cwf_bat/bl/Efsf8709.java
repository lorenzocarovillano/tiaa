/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:45:42 PM
**        *   FROM NATURAL MAP   :  Efsf8709
************************************************************
**        * FILE NAME               : Efsf8709.java
**        * CLASS NAME              : Efsf8709
**        * INSTANCE NAME           : Efsf8709
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #PROGRAM
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsf8709 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Program;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Program = parameters.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        parameters.reset();
    }

    public Efsf8709() throws Exception
    {
        super("Efsf8709");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=004 LS=132 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Efsf8709", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Efsf8709"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Program", pnd_Program, true, 1, 1, 8, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_1", "ERROR REPORT FOR XREF AND DOCUMENT RECONCILIATION", "BLUE", 1, 14, 49);
            uiForm.setUiLabel("label_2", "AS OF", "BLUE", 1, 69, 5);
            uiForm.setUiControl("astDATU", Global.getDATU(), true, 1, 75, 8, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("astTIME", Global.getTIME(), true, 1, 84, 10, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_3", "PAGE:", "BLUE", 1, 105, 5);
            uiForm.setUiControl("astPAGE_NUMBER", getReports().getPageNumberDbs(0), true, 1, 111, 5, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_4", "-----------------------------------------------------------------------------------------------------------------------------------", 
                "", 2, 1, 131);
            uiForm.setUiLabel("label_5", "ERROR", "BLUE", 3, 2, 5);
            uiForm.setUiLabel("label_6", "MESSAGE", "BLUE", 3, 9, 7);
            uiForm.setUiLabel("label_7", "SOURCE", "BLUE", 3, 52, 6);
            uiForm.setUiLabel("label_8", "MIN", "BLUE", 3, 60, 3);
            uiForm.setUiLabel("label_9", "SYS CABINET", "BLUE", 3, 72, 11);
            uiForm.setUiLabel("label_10", "DTE-TME", "BLUE", 3, 122, 7);
            uiForm.setUiLabel("label_11", "-----------------------------------------------------------------------------------------------------------------------------------", 
                "", 4, 1, 131);
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
