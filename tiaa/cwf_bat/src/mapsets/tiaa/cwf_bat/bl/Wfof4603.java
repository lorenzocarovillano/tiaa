/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:59:19 PM
**        *   FROM NATURAL MAP   :  Wfof4603
************************************************************
**        * FILE NAME               : Wfof4603.java
**        * CLASS NAME              : Wfof4603
**        * INSTANCE NAME           : Wfof4603
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     #WRK-RQST-IDX CMCV.ACTION-CDE(*) CMCV.CABINET-ID(*)                                                                      *     CMCV.CABINET-LEVEL(*) 
    CMCV.CLIENT-APP-CDE(*)                                                                             *     CMCV.CORP-STATUS-CDE(*) CMCV.DSTNTN-UNT-CDE(*) 
    CMCV.DUE-DTE(*)                                                           *     CMCV.ELCTRNC-FLDR-IND(*) CMCV.EMPL-RACF-ID(*)                       
    *     CMCV.IND-TRACKING-ID(*) CMCV.INST-TRACKING-ID(*)                                                                         *     CMCV.RQST-ORGN-CDE(*) 
    CMCV.STEP(*) CMCV.TOPIC(*)                                                                         *     CMCV.UNIT-STATUS-CDE(*) CMCV.WF-IND(*) CMCV.WPID-CDE(*)
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Wfof4603 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Wrk_Rqst_Idx;
    private DbsField cmcv_Action_Cde;
    private DbsField cmcv_Cabinet_Id;
    private DbsField cmcv_Cabinet_Level;
    private DbsField cmcv_Client_App_Cde;
    private DbsField cmcv_Corp_Status_Cde;
    private DbsField cmcv_Dstntn_Unt_Cde;
    private DbsField cmcv_Due_Dte;
    private DbsField cmcv_Elctrnc_Fldr_Ind;
    private DbsField cmcv_Empl_Racf_Id;
    private DbsField cmcv_Ind_Tracking_Id;
    private DbsField cmcv_Inst_Tracking_Id;
    private DbsField cmcv_Rqst_Orgn_Cde;
    private DbsField cmcv_Step;
    private DbsField cmcv_Topic;
    private DbsField cmcv_Unit_Status_Cde;
    private DbsField cmcv_Wf_Ind;
    private DbsField cmcv_Wpid_Cde;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Wrk_Rqst_Idx = parameters.newFieldInRecord("pnd_Wrk_Rqst_Idx", "#WRK-RQST-IDX", FieldType.NUMERIC, 7);
        cmcv_Action_Cde = parameters.newFieldArrayInRecord("cmcv_Action_Cde", "CMCV.ACTION-CDE", FieldType.STRING, 2, new DbsArrayController(1, 10));
        cmcv_Cabinet_Id = parameters.newFieldArrayInRecord("cmcv_Cabinet_Id", "CMCV.CABINET-ID", FieldType.STRING, 17, new DbsArrayController(1, 10));
        cmcv_Cabinet_Level = parameters.newFieldArrayInRecord("cmcv_Cabinet_Level", "CMCV.CABINET-LEVEL", FieldType.STRING, 1, new DbsArrayController(1, 
            10));
        cmcv_Client_App_Cde = parameters.newFieldArrayInRecord("cmcv_Client_App_Cde", "CMCV.CLIENT-APP-CDE", FieldType.STRING, 8, new DbsArrayController(1, 
            10));
        cmcv_Corp_Status_Cde = parameters.newFieldArrayInRecord("cmcv_Corp_Status_Cde", "CMCV.CORP-STATUS-CDE", FieldType.STRING, 1, new DbsArrayController(1, 
            10));
        cmcv_Dstntn_Unt_Cde = parameters.newFieldArrayInRecord("cmcv_Dstntn_Unt_Cde", "CMCV.DSTNTN-UNT-CDE", FieldType.STRING, 8, new DbsArrayController(1, 
            10));
        cmcv_Due_Dte = parameters.newFieldArrayInRecord("cmcv_Due_Dte", "CMCV.DUE-DTE", FieldType.NUMERIC, 8, new DbsArrayController(1, 10));
        cmcv_Elctrnc_Fldr_Ind = parameters.newFieldArrayInRecord("cmcv_Elctrnc_Fldr_Ind", "CMCV.ELCTRNC-FLDR-IND", FieldType.STRING, 1, new DbsArrayController(1, 
            10));
        cmcv_Empl_Racf_Id = parameters.newFieldArrayInRecord("cmcv_Empl_Racf_Id", "CMCV.EMPL-RACF-ID", FieldType.STRING, 8, new DbsArrayController(1, 
            10));
        cmcv_Ind_Tracking_Id = parameters.newFieldArrayInRecord("cmcv_Ind_Tracking_Id", "CMCV.IND-TRACKING-ID", FieldType.STRING, 20, new DbsArrayController(1, 
            10));
        cmcv_Inst_Tracking_Id = parameters.newFieldArrayInRecord("cmcv_Inst_Tracking_Id", "CMCV.INST-TRACKING-ID", FieldType.STRING, 20, new DbsArrayController(1, 
            10));
        cmcv_Rqst_Orgn_Cde = parameters.newFieldArrayInRecord("cmcv_Rqst_Orgn_Cde", "CMCV.RQST-ORGN-CDE", FieldType.STRING, 1, new DbsArrayController(1, 
            10));
        cmcv_Step = parameters.newFieldArrayInRecord("cmcv_Step", "CMCV.STEP", FieldType.STRING, 6, new DbsArrayController(1, 10));
        cmcv_Topic = parameters.newFieldArrayInRecord("cmcv_Topic", "CMCV.TOPIC", FieldType.STRING, 20, new DbsArrayController(1, 10));
        cmcv_Unit_Status_Cde = parameters.newFieldArrayInRecord("cmcv_Unit_Status_Cde", "CMCV.UNIT-STATUS-CDE", FieldType.STRING, 4, new DbsArrayController(1, 
            10));
        cmcv_Wf_Ind = parameters.newFieldArrayInRecord("cmcv_Wf_Ind", "CMCV.WF-IND", FieldType.STRING, 1, new DbsArrayController(1, 10));
        cmcv_Wpid_Cde = parameters.newFieldArrayInRecord("cmcv_Wpid_Cde", "CMCV.WPID-CDE", FieldType.STRING, 6, new DbsArrayController(1, 10));
        parameters.reset();
    }

    public Wfof4603() throws Exception
    {
        super("Wfof4603");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=004 LS=133 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Wfof4603", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Wfof4603"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Wrk_Rqst_Idx", pnd_Wrk_Rqst_Idx, true, 1, 33, 7, "", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_1", "Cabinet Id..:", "", 1, 41, 13);
            uiForm.setUiControl("cmcv_Cabinet_Id_1", cmcv_Cabinet_Id.getValue(1), true, 1, 55, 17, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_2", "DstnCde:", "", 1, 77, 8);
            uiForm.setUiControl("cmcv_Dstntn_Unt_Cde_1", cmcv_Dstntn_Unt_Cde.getValue(1), true, 1, 86, 8, "", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_3", "CrpStatCde:", "", 1, 96, 11);
            uiForm.setUiControl("cmcv_Corp_Status_Cde_1", cmcv_Corp_Status_Cde.getValue(1), true, 1, 108, 1, "", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_4", "UnitStatCde.:", "", 1, 111, 13);
            uiForm.setUiControl("cmcv_Unit_Status_Cde_1", cmcv_Unit_Status_Cde.getValue(1), true, 1, 125, 4, "", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_5", "Ind TrckngId:", "", 2, 41, 13);
            uiForm.setUiControl("cmcv_Ind_Tracking_Id_1", cmcv_Ind_Tracking_Id.getValue(1), true, 2, 55, 20, "", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_6", "WPIDCde:", "", 2, 77, 8);
            uiForm.setUiControl("cmcv_Wpid_Cde_1", cmcv_Wpid_Cde.getValue(1), true, 2, 86, 6, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_7", "Wf Ind....:", "", 2, 96, 11);
            uiForm.setUiControl("cmcv_Wf_Ind_1", cmcv_Wf_Ind.getValue(1), true, 2, 108, 1, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_8", "EmplRacfId..:", "", 2, 111, 13);
            uiForm.setUiControl("cmcv_Empl_Racf_Id_1", cmcv_Empl_Racf_Id.getValue(1), true, 2, 125, 8, "", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_9", "InstTrckngId:", "", 3, 41, 13);
            uiForm.setUiControl("cmcv_Inst_Tracking_Id_1", cmcv_Inst_Tracking_Id.getValue(1), true, 3, 55, 20, "", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_10", "Due Dte:", "", 3, 77, 8);
            uiForm.setUiControl("cmcv_Due_Dte_1", cmcv_Due_Dte.getValue(1), true, 3, 86, 8, "", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_11", "CabinetLvl:", "", 3, 96, 11);
            uiForm.setUiControl("cmcv_Cabinet_Level_1", cmcv_Cabinet_Level.getValue(1), true, 3, 108, 1, "", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_12", "ClientAppCde:", "", 3, 111, 13);
            uiForm.setUiControl("cmcv_Client_App_Cde_1", cmcv_Client_App_Cde.getValue(1), true, 3, 125, 8, "", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_13", "AC:", "", 4, 41, 3);
            uiForm.setUiControl("cmcv_Action_Cde_1", cmcv_Action_Cde.getValue(1), true, 4, 45, 2, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_14", "Topic:", "", 4, 48, 6);
            uiForm.setUiControl("cmcv_Topic_1", cmcv_Topic.getValue(1), true, 4, 55, 20, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_15", "Step Id:", "", 4, 77, 8);
            uiForm.setUiControl("cmcv_Step_1", cmcv_Step.getValue(1), true, 4, 86, 6, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_16", "ElcFldrInd:", "", 4, 96, 11);
            uiForm.setUiControl("cmcv_Elctrnc_Fldr_Ind_1", cmcv_Elctrnc_Fldr_Ind.getValue(1), true, 4, 108, 1, "", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_17", "RqstOrgnCde.:", "", 4, 111, 13);
            uiForm.setUiControl("cmcv_Rqst_Orgn_Cde_1", cmcv_Rqst_Orgn_Cde.getValue(1), true, 4, 125, 1, "", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
