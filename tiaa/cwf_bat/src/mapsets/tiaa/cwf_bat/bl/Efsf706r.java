/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:45:38 PM
**        *   FROM NATURAL MAP   :  Efsf706r
************************************************************
**        * FILE NAME               : Efsf706r.java
**        * CLASS NAME              : Efsf706r
**        * INSTANCE NAME           : Efsf706r
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #IMAGE-PLATTER-OLD #SUMMARY.#BATCH-FINISHED                                                                              *     #SUMMARY.#BATCH-UNFINISHED 
    #SUMMARY.#IMAGES-PROCESSED                                                                    *     #SUMMARY.#MINS-NOT-UPLOADED #SUMMARY.#MINS-UPLOADED 
    *     #SUMMARY.#UPLOAD-COUNT
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsf706r extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Image_Platter_Old;
    private DbsField pnd_Summary_Pnd_Batch_Finished;
    private DbsField pnd_Summary_Pnd_Batch_Unfinished;
    private DbsField pnd_Summary_Pnd_Images_Processed;
    private DbsField pnd_Summary_Pnd_Mins_Not_Uploaded;
    private DbsField pnd_Summary_Pnd_Mins_Uploaded;
    private DbsField pnd_Summary_Pnd_Upload_Count;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Image_Platter_Old = parameters.newFieldInRecord("pnd_Image_Platter_Old", "#IMAGE-PLATTER-OLD", FieldType.STRING, 10);
        pnd_Summary_Pnd_Batch_Finished = parameters.newFieldInRecord("pnd_Summary_Pnd_Batch_Finished", "#SUMMARY.#BATCH-FINISHED", FieldType.NUMERIC, 
            5);
        pnd_Summary_Pnd_Batch_Unfinished = parameters.newFieldInRecord("pnd_Summary_Pnd_Batch_Unfinished", "#SUMMARY.#BATCH-UNFINISHED", FieldType.NUMERIC, 
            5);
        pnd_Summary_Pnd_Images_Processed = parameters.newFieldInRecord("pnd_Summary_Pnd_Images_Processed", "#SUMMARY.#IMAGES-PROCESSED", FieldType.NUMERIC, 
            7);
        pnd_Summary_Pnd_Mins_Not_Uploaded = parameters.newFieldInRecord("pnd_Summary_Pnd_Mins_Not_Uploaded", "#SUMMARY.#MINS-NOT-UPLOADED", FieldType.NUMERIC, 
            7);
        pnd_Summary_Pnd_Mins_Uploaded = parameters.newFieldInRecord("pnd_Summary_Pnd_Mins_Uploaded", "#SUMMARY.#MINS-UPLOADED", FieldType.NUMERIC, 7);
        pnd_Summary_Pnd_Upload_Count = parameters.newFieldInRecord("pnd_Summary_Pnd_Upload_Count", "#SUMMARY.#UPLOAD-COUNT", FieldType.NUMERIC, 5);
        parameters.reset();
    }

    public Efsf706r() throws Exception
    {
        super("Efsf706r");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Efsf706r", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Efsf706r"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Image_Platter_Old", pnd_Image_Platter_Old, true, 1, 23, 10, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Summary_Pnd_Upload_Count", pnd_Summary_Pnd_Upload_Count, true, 1, 37, 5, "WHITE", true, false, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Summary_Pnd_Batch_Finished", pnd_Summary_Pnd_Batch_Finished, true, 1, 51, 5, "WHITE", true, false, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Summary_Pnd_Batch_Unfinished", pnd_Summary_Pnd_Batch_Unfinished, true, 1, 65, 5, "WHITE", true, false, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Summary_Pnd_Mins_Uploaded", pnd_Summary_Pnd_Mins_Uploaded, true, 1, 81, 7, "WHITE", true, false, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Summary_Pnd_Mins_Not_Uploaded", pnd_Summary_Pnd_Mins_Not_Uploaded, true, 1, 93, 7, "WHITE", true, false, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Summary_Pnd_Images_Processed", pnd_Summary_Pnd_Images_Processed, true, 1, 110, 7, "WHITE", true, false, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
