/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:59:18 PM
**        *   FROM NATURAL MAP   :  Wfof4552
************************************************************
**        * FILE NAME               : Wfof4552.java
**        * CLASS NAME              : Wfof4552
**        * INSTANCE NAME           : Wfof4552
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     #TEXT-IDX CMCV.T-CABINET-ID(*) CMCV.T-CABINET-LEVEL(*)                                                                   *     CMCV.T-IND-TRACKING-ID(*) 
    CMCV.T-INST-TRACKING-ID(*)                                                                     *     CMCV.T-WF-IND(*)
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Wfof4552 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Text_Idx;
    private DbsField cmcv_T_Cabinet_Id;
    private DbsField cmcv_T_Cabinet_Level;
    private DbsField cmcv_T_Ind_Tracking_Id;
    private DbsField cmcv_T_Inst_Tracking_Id;
    private DbsField cmcv_T_Wf_Ind;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Text_Idx = parameters.newFieldInRecord("pnd_Text_Idx", "#TEXT-IDX", FieldType.NUMERIC, 7);
        cmcv_T_Cabinet_Id = parameters.newFieldArrayInRecord("cmcv_T_Cabinet_Id", "CMCV.T-CABINET-ID", FieldType.STRING, 17, new DbsArrayController(1, 
            35));
        cmcv_T_Cabinet_Level = parameters.newFieldArrayInRecord("cmcv_T_Cabinet_Level", "CMCV.T-CABINET-LEVEL", FieldType.STRING, 1, new DbsArrayController(1, 
            35));
        cmcv_T_Ind_Tracking_Id = parameters.newFieldArrayInRecord("cmcv_T_Ind_Tracking_Id", "CMCV.T-IND-TRACKING-ID", FieldType.STRING, 20, new DbsArrayController(1, 
            35));
        cmcv_T_Inst_Tracking_Id = parameters.newFieldArrayInRecord("cmcv_T_Inst_Tracking_Id", "CMCV.T-INST-TRACKING-ID", FieldType.STRING, 20, new DbsArrayController(1, 
            35));
        cmcv_T_Wf_Ind = parameters.newFieldArrayInRecord("cmcv_T_Wf_Ind", "CMCV.T-WF-IND", FieldType.STRING, 1, new DbsArrayController(1, 35));
        parameters.reset();
    }

    public Wfof4552() throws Exception
    {
        super("Wfof4552");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=002 LS=133 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Wfof4552", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Wfof4552"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Text_Idx", pnd_Text_Idx, true, 1, 32, 7, "", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_1", "CabinetId:", "", 1, 40, 10);
            uiForm.setUiControl("cmcv_T_Cabinet_Id_1", cmcv_T_Cabinet_Id.getValue(1), true, 1, 51, 17, "", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_2", "WFInd:", "", 1, 69, 6);
            uiForm.setUiControl("cmcv_T_Wf_Ind_1", cmcv_T_Wf_Ind.getValue(1), true, 1, 76, 1, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_3", "CabLvl", "", 1, 78, 6);
            uiForm.setUiControl("cmcv_T_Cabinet_Level_1", cmcv_T_Cabinet_Level.getValue(1), true, 1, 85, 1, "", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_4", "IndTrckngId.:", "", 1, 88, 13);
            uiForm.setUiControl("cmcv_T_Ind_Tracking_Id_1", cmcv_T_Ind_Tracking_Id.getValue(1), true, 1, 102, 20, "", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_5", "InstTrckngId:", "", 2, 88, 13);
            uiForm.setUiControl("cmcv_T_Inst_Tracking_Id_1", cmcv_T_Inst_Tracking_Id.getValue(1), true, 2, 102, 20, "", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
