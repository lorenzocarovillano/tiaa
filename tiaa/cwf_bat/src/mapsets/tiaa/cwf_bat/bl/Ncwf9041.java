/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:55:13 PM
**        *   FROM NATURAL MAP   :  Ncwf9041
************************************************************
**        * FILE NAME               : Ncwf9041.java
**        * CLASS NAME              : Ncwf9041
**        * INSTANCE NAME           : Ncwf9041
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     NCW-EFM-AUDIT.ACTION-CDE NCW-EFM-AUDIT.DCMNT-ADDED-CNT                                                                   *     NCW-EFM-AUDIT.DCMNT-RENAMED-CNT 
    NCW-EFM-AUDIT.EMPL-OPRTR-CDE                                                             *     NCW-EFM-AUDIT.ERROR-CDE NCW-EFM-AUDIT.LOG-DTE-TME    
    *     NCW-EFM-AUDIT.MIT-ADDED-CNT NCW-EFM-AUDIT.MIT-UPDATED-CNT                                                                *     NCW-EFM-AUDIT.NP-PIN 
    NCW-EFM-AUDIT.RECURSIVE-IND                                                                         *     NCW-EFM-AUDIT.RQST-ID(*) NCW-EFM-AUDIT.RQST-PRCSS-TME 
    *     NCW-EFM-AUDIT.SYSTEM-CDE
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Ncwf9041 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField ncw_Efm_Audit_Action_Cde;
    private DbsField ncw_Efm_Audit_Dcmnt_Added_Cnt;
    private DbsField ncw_Efm_Audit_Dcmnt_Renamed_Cnt;
    private DbsField ncw_Efm_Audit_Empl_Oprtr_Cde;
    private DbsField ncw_Efm_Audit_Error_Cde;
    private DbsField ncw_Efm_Audit_Log_Dte_Tme;
    private DbsField ncw_Efm_Audit_Mit_Added_Cnt;
    private DbsField ncw_Efm_Audit_Mit_Updated_Cnt;
    private DbsField ncw_Efm_Audit_Np_Pin;
    private DbsField ncw_Efm_Audit_Recursive_Ind;
    private DbsField ncw_Efm_Audit_Rqst_Id;
    private DbsField ncw_Efm_Audit_Rqst_Prcss_Tme;
    private DbsField ncw_Efm_Audit_System_Cde;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        ncw_Efm_Audit_Action_Cde = parameters.newFieldInRecord("ncw_Efm_Audit_Action_Cde", "NCW-EFM-AUDIT.ACTION-CDE", FieldType.STRING, 2);
        ncw_Efm_Audit_Dcmnt_Added_Cnt = parameters.newFieldInRecord("ncw_Efm_Audit_Dcmnt_Added_Cnt", "NCW-EFM-AUDIT.DCMNT-ADDED-CNT", FieldType.NUMERIC, 
            3);
        ncw_Efm_Audit_Dcmnt_Renamed_Cnt = parameters.newFieldInRecord("ncw_Efm_Audit_Dcmnt_Renamed_Cnt", "NCW-EFM-AUDIT.DCMNT-RENAMED-CNT", FieldType.NUMERIC, 
            3);
        ncw_Efm_Audit_Empl_Oprtr_Cde = parameters.newFieldInRecord("ncw_Efm_Audit_Empl_Oprtr_Cde", "NCW-EFM-AUDIT.EMPL-OPRTR-CDE", FieldType.STRING, 8);
        ncw_Efm_Audit_Error_Cde = parameters.newFieldInRecord("ncw_Efm_Audit_Error_Cde", "NCW-EFM-AUDIT.ERROR-CDE", FieldType.NUMERIC, 3);
        ncw_Efm_Audit_Log_Dte_Tme = parameters.newFieldInRecord("ncw_Efm_Audit_Log_Dte_Tme", "NCW-EFM-AUDIT.LOG-DTE-TME", FieldType.TIME);
        ncw_Efm_Audit_Mit_Added_Cnt = parameters.newFieldInRecord("ncw_Efm_Audit_Mit_Added_Cnt", "NCW-EFM-AUDIT.MIT-ADDED-CNT", FieldType.NUMERIC, 3);
        ncw_Efm_Audit_Mit_Updated_Cnt = parameters.newFieldInRecord("ncw_Efm_Audit_Mit_Updated_Cnt", "NCW-EFM-AUDIT.MIT-UPDATED-CNT", FieldType.NUMERIC, 
            3);
        ncw_Efm_Audit_Np_Pin = parameters.newFieldInRecord("ncw_Efm_Audit_Np_Pin", "NCW-EFM-AUDIT.NP-PIN", FieldType.STRING, 7);
        ncw_Efm_Audit_Recursive_Ind = parameters.newFieldInRecord("ncw_Efm_Audit_Recursive_Ind", "NCW-EFM-AUDIT.RECURSIVE-IND", FieldType.STRING, 1);
        ncw_Efm_Audit_Rqst_Id = parameters.newFieldArrayInRecord("ncw_Efm_Audit_Rqst_Id", "NCW-EFM-AUDIT.RQST-ID", FieldType.STRING, 23, new DbsArrayController(1, 
            10));
        ncw_Efm_Audit_Rqst_Prcss_Tme = parameters.newFieldInRecord("ncw_Efm_Audit_Rqst_Prcss_Tme", "NCW-EFM-AUDIT.RQST-PRCSS-TME", FieldType.NUMERIC, 
            5);
        ncw_Efm_Audit_System_Cde = parameters.newFieldInRecord("ncw_Efm_Audit_System_Cde", "NCW-EFM-AUDIT.SYSTEM-CDE", FieldType.STRING, 15);
        parameters.reset();
    }

    public Ncwf9041() throws Exception
    {
        super("Ncwf9041");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=001 LS=132 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Ncwf9041", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Ncwf9041"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("ncw_Efm_Audit_Log_Dte_Tme", ncw_Efm_Audit_Log_Dte_Tme, true, 1, 1, 21, "WHITE", "MM/DD/YYYY' 'HH:II:SS:T", true, false, 
                null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("ncw_Efm_Audit_System_Cde", ncw_Efm_Audit_System_Cde, true, 1, 23, 6, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("ncw_Efm_Audit_Action_Cde", ncw_Efm_Audit_Action_Cde, true, 1, 31, 2, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("ncw_Efm_Audit_Empl_Oprtr_Cde", ncw_Efm_Audit_Empl_Oprtr_Cde, true, 1, 35, 8, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("ncw_Efm_Audit_Rqst_Prcss_Tme", ncw_Efm_Audit_Rqst_Prcss_Tme, true, 1, 44, 7, "WHITE", "99:99:9", true, false, null, "0123456789+-, ", 
                "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("ncw_Efm_Audit_Recursive_Ind", ncw_Efm_Audit_Recursive_Ind, true, 1, 54, 1, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("ncw_Efm_Audit_Error_Cde", ncw_Efm_Audit_Error_Cde, true, 1, 57, 3, "WHITE", true, false, null, "0123456789+-, ", "AD=DROFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("ncw_Efm_Audit_Mit_Added_Cnt", ncw_Efm_Audit_Mit_Added_Cnt, true, 1, 63, 3, "WHITE", true, false, null, "0123456789+-, ", 
                "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("ncw_Efm_Audit_Mit_Updated_Cnt", ncw_Efm_Audit_Mit_Updated_Cnt, true, 1, 69, 3, "WHITE", true, false, null, "0123456789+-, ", 
                "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("ncw_Efm_Audit_Dcmnt_Added_Cnt", ncw_Efm_Audit_Dcmnt_Added_Cnt, true, 1, 75, 3, "WHITE", true, false, null, "0123456789+-, ", 
                "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("ncw_Efm_Audit_Dcmnt_Renamed_Cnt", ncw_Efm_Audit_Dcmnt_Renamed_Cnt, true, 1, 81, 3, "WHITE", true, false, null, "0123456789+-, ", 
                "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("ncw_Efm_Audit_Np_Pin", ncw_Efm_Audit_Np_Pin, true, 1, 87, 7, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("ncw_Efm_Audit_Rqst_Id_1pls0", ncw_Efm_Audit_Rqst_Id.getValue(1+0), true, 1, 95, 18, "WHITE", true, false, null, null, 
                "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
