/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:59:14 PM
**        *   FROM NATURAL MAP   :  Wfof4512
************************************************************
**        * FILE NAME               : Wfof4512.java
**        * CLASS NAME              : Wfof4512
**        * INSTANCE NAME           : Wfof4512
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #CLIENT-SUCC-CONT-CNT CMCV.CALL-STAMP-TME CMCV.CONTAINER-ID                                                              *     CMCV.RQST-ENTRY-OP-CDE 
    CMCV.RQST-ORIGIN-UNIT-CDE
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Wfof4512 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Client_Succ_Cont_Cnt;
    private DbsField cmcv_Call_Stamp_Tme;
    private DbsField cmcv_Container_Id;
    private DbsField cmcv_Rqst_Entry_Op_Cde;
    private DbsField cmcv_Rqst_Origin_Unit_Cde;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Client_Succ_Cont_Cnt = parameters.newFieldInRecord("pnd_Client_Succ_Cont_Cnt", "#CLIENT-SUCC-CONT-CNT", FieldType.NUMERIC, 7);
        cmcv_Call_Stamp_Tme = parameters.newFieldInRecord("cmcv_Call_Stamp_Tme", "CMCV.CALL-STAMP-TME", FieldType.TIME);
        cmcv_Container_Id = parameters.newFieldInRecord("cmcv_Container_Id", "CMCV.CONTAINER-ID", FieldType.STRING, 20);
        cmcv_Rqst_Entry_Op_Cde = parameters.newFieldInRecord("cmcv_Rqst_Entry_Op_Cde", "CMCV.RQST-ENTRY-OP-CDE", FieldType.STRING, 8);
        cmcv_Rqst_Origin_Unit_Cde = parameters.newFieldInRecord("cmcv_Rqst_Origin_Unit_Cde", "CMCV.RQST-ORIGIN-UNIT-CDE", FieldType.STRING, 8);
        parameters.reset();
    }

    public Wfof4512() throws Exception
    {
        super("Wfof4512");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=001 LS=133 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Wfof4512", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Wfof4512"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Client_Succ_Cont_Cnt", pnd_Client_Succ_Cont_Cnt, true, 1, 2, 7, "", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("cmcv_Container_Id", cmcv_Container_Id, true, 1, 11, 20, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_1", "Txt/Img", "", 1, 32, 7);
            uiForm.setUiLabel("label_2", "CallStmpTme.:", "", 1, 41, 13);
            uiForm.setUiControl("cmcv_Call_Stamp_Tme", cmcv_Call_Stamp_Tme, true, 1, 55, 15, "", "YYYYMMDDHHIISST", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_3", "Oper...:", "", 1, 77, 8);
            uiForm.setUiControl("cmcv_Rqst_Entry_Op_Cde", cmcv_Rqst_Entry_Op_Cde, true, 1, 86, 8, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_4", "OrigUnit..:", "", 1, 96, 11);
            uiForm.setUiControl("cmcv_Rqst_Origin_Unit_Cde", cmcv_Rqst_Origin_Unit_Cde, true, 1, 108, 8, "", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
