/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:41:11 PM
**        *   FROM NATURAL MAP   :  Cwff7978
************************************************************
**        * FILE NAME               : Cwff7978.java
**        * CLASS NAME              : Cwff7978
**        * INSTANCE NAME           : Cwff7978
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     #PROGRAM
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cwff7978 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Program;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Program = parameters.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        parameters.reset();
    }

    public Cwff7978() throws Exception
    {
        super("Cwff7978");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Cwff7978", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Cwff7978"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Program", pnd_Program, true, 1, 1, 8, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_1", "PINS AWAITING DIGITIZING FOR A DAY OR MORE AS OF :", "BLUE", 1, 33, 50);
            uiForm.setUiControl("astDATU", Global.getDATU(), true, 1, 84, 8, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("astTIME", Global.getTIME(), true, 1, 93, 10, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_2", "PAGE:", "BLUE", 1, 120, 5);
            uiForm.setUiControl("astPAGE_NUMBER", getReports().getPageNumberDbs(0), true, 1, 126, 5, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_3", "-----------------------------------------------------------------------------------------------------------------------------------", 
                "", 2, 1, 131);
            uiForm.setUiLabel("label_4", "POWERFILE", "BLUE", 3, 1, 9);
            uiForm.setUiLabel("label_5", "NO.", "BLUE", 4, 4, 3);
            uiForm.setUiLabel("label_6", "PIN", "BLUE", 4, 15, 3);
            uiForm.setUiLabel("label_7", "SOURCE", "BLUE", 4, 28, 6);
            uiForm.setUiLabel("label_8", "MEDIA UPDATE DATE/TIME", "BLUE", 4, 40, 22);
            uiForm.setUiLabel("label_9", "LAST UPDATED BY", "BLUE", 4, 66, 15);
            uiForm.setUiLabel("label_10", "-----------------------------------------------------------------------------------------------------------------------------------", 
                "", 5, 1, 131);
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
