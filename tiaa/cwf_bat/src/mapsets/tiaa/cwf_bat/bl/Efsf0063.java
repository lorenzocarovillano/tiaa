/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:45:29 PM
**        *   FROM NATURAL MAP   :  Efsf0063
************************************************************
**        * FILE NAME               : Efsf0063.java
**        * CLASS NAME              : Efsf0063
**        * INSTANCE NAME           : Efsf0063
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #INPUT-SOURCE-ID #INPUT-TIME-END #INPUT-TIME-START #INPUT-YYMM                                                           *     #PROGRAM
************************************************************ */

package tiaa.cwf_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsf0063 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Input_Source_Id;
    private DbsField pnd_Input_Time_End;
    private DbsField pnd_Input_Time_Start;
    private DbsField pnd_Input_Yymm;
    private DbsField pnd_Program;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Input_Source_Id = parameters.newFieldInRecord("pnd_Input_Source_Id", "#INPUT-SOURCE-ID", FieldType.STRING, 6);
        pnd_Input_Time_End = parameters.newFieldInRecord("pnd_Input_Time_End", "#INPUT-TIME-END", FieldType.STRING, 6);
        pnd_Input_Time_Start = parameters.newFieldInRecord("pnd_Input_Time_Start", "#INPUT-TIME-START", FieldType.STRING, 6);
        pnd_Input_Yymm = parameters.newFieldInRecord("pnd_Input_Yymm", "#INPUT-YYMM", FieldType.STRING, 6);
        pnd_Program = parameters.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        parameters.reset();
    }

    public Efsf0063() throws Exception
    {
        super("Efsf0063");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Efsf0063", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Efsf0063"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Program", pnd_Program, true, 1, 1, 8, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_1", "CWF - IMAGE UPLOAD DAILY TOTALS FOR", "BLUE", 1, 24, 35);
            uiForm.setUiControl("pnd_Input_Source_Id", pnd_Input_Source_Id, true, 1, 60, 6, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Input_Yymm", pnd_Input_Yymm, true, 1, 67, 7, "WHITE", "XXXX'/'XX", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_2", "TIME PERIOD:", "BLUE", 1, 76, 12);
            uiForm.setUiControl("pnd_Input_Time_Start", pnd_Input_Time_Start, true, 1, 89, 8, "WHITE", "XX':'XX':'XX", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_3", "TO", "BLUE", 1, 98, 2);
            uiForm.setUiControl("pnd_Input_Time_End", pnd_Input_Time_End, true, 1, 101, 8, "WHITE", "XX':'XX':'XX", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_4", "PAGE:", "BLUE", 1, 121, 5);
            uiForm.setUiControl("astPAGE_NUMBER", getReports().getPageNumberDbs(0), true, 1, 127, 5, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_5", "AS OF", "BLUE", 2, 56, 5);
            uiForm.setUiControl("astDATU", Global.getDATU(), true, 2, 62, 8, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("astTIME", Global.getTIME(), true, 2, 71, 10, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_6", "-----------------------------------------------------------------------------------------------------------------------------------", 
                "", 3, 1, 131);
            uiForm.setUiLabel("label_7", "FROM", "BLUE", 4, 6, 4);
            uiForm.setUiLabel("label_8", "TO", "BLUE", 4, 23, 2);
            uiForm.setUiLabel("label_9", "NUMBER OF", "BLUE", 4, 39, 9);
            uiForm.setUiLabel("label_10", "NUMBER OF", "BLUE", 4, 62, 9);
            uiForm.setUiLabel("label_11", "NUMBER OF", "BLUE", 4, 84, 9);
            uiForm.setUiLabel("label_12", "DATE", "BLUE", 5, 6, 4);
            uiForm.setUiLabel("label_13", "DATE", "BLUE", 5, 22, 4);
            uiForm.setUiLabel("label_14", "UPLOADS EXECUTED", "BLUE", 5, 36, 16);
            uiForm.setUiLabel("label_15", "MINS UPLOADED", "BLUE", 5, 60, 13);
            uiForm.setUiLabel("label_16", "IMAGES PROCESSED", "BLUE", 5, 81, 16);
            uiForm.setUiLabel("label_17", "-----------------------------------------------------------------------------------------------------------------------------------", 
                "", 6, 1, 131);
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
