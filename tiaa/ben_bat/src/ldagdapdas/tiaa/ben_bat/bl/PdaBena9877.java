/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:18:14 PM
**        * FROM NATURAL PDA     : BENA9877
************************************************************
**        * FILE NAME            : PdaBena9877.java
**        * CLASS NAME           : PdaBena9877
**        * INSTANCE NAME        : PdaBena9877
************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaBena9877 extends PdaBase
{
    // Properties
    private DbsGroup bena9877;
    private DbsField bena9877_Pnd_Bsnss_Dte;
    private DbsField bena9877_Pnd_Next_Bsnss_Dte;
    private DbsField bena9877_Pnd_Cntrct_Lob_Cde;
    private DbsField bena9877_Pnd_Cntrct_Nmbr_Range_Cde;
    private DbsField bena9877_Pnd_Cntrct_Spcl_Cnsdrtn_Cde;
    private DbsField bena9877_Pnd_Mos_Isn;
    private DbsGroup bena9877_Pnd_Dsgntn_Grp;
    private DbsField bena9877_Pnd_Dsgntn_Isn;
    private DbsField bena9877_Pnd_Dsgntn_Rltn_Cde;
    private DbsField bena9877_Pnd_Cref_Cntrct_Nmbr;
    private DbsField bena9877_Pnd_Error_Msg;
    private DbsField bena9877_Pnd_Error_Cde;
    private DbsField bena9877_Pnd_Error_Pgm;
    private DbsField bena9877_Pnd_Edit_Failed;
    private DbsField bena9877_Pnd_Interface_As_Mos;
    private DbsField bena9877_Pnd_Copy_From_Contract;

    public DbsGroup getBena9877() { return bena9877; }

    public DbsField getBena9877_Pnd_Bsnss_Dte() { return bena9877_Pnd_Bsnss_Dte; }

    public DbsField getBena9877_Pnd_Next_Bsnss_Dte() { return bena9877_Pnd_Next_Bsnss_Dte; }

    public DbsField getBena9877_Pnd_Cntrct_Lob_Cde() { return bena9877_Pnd_Cntrct_Lob_Cde; }

    public DbsField getBena9877_Pnd_Cntrct_Nmbr_Range_Cde() { return bena9877_Pnd_Cntrct_Nmbr_Range_Cde; }

    public DbsField getBena9877_Pnd_Cntrct_Spcl_Cnsdrtn_Cde() { return bena9877_Pnd_Cntrct_Spcl_Cnsdrtn_Cde; }

    public DbsField getBena9877_Pnd_Mos_Isn() { return bena9877_Pnd_Mos_Isn; }

    public DbsGroup getBena9877_Pnd_Dsgntn_Grp() { return bena9877_Pnd_Dsgntn_Grp; }

    public DbsField getBena9877_Pnd_Dsgntn_Isn() { return bena9877_Pnd_Dsgntn_Isn; }

    public DbsField getBena9877_Pnd_Dsgntn_Rltn_Cde() { return bena9877_Pnd_Dsgntn_Rltn_Cde; }

    public DbsField getBena9877_Pnd_Cref_Cntrct_Nmbr() { return bena9877_Pnd_Cref_Cntrct_Nmbr; }

    public DbsField getBena9877_Pnd_Error_Msg() { return bena9877_Pnd_Error_Msg; }

    public DbsField getBena9877_Pnd_Error_Cde() { return bena9877_Pnd_Error_Cde; }

    public DbsField getBena9877_Pnd_Error_Pgm() { return bena9877_Pnd_Error_Pgm; }

    public DbsField getBena9877_Pnd_Edit_Failed() { return bena9877_Pnd_Edit_Failed; }

    public DbsField getBena9877_Pnd_Interface_As_Mos() { return bena9877_Pnd_Interface_As_Mos; }

    public DbsField getBena9877_Pnd_Copy_From_Contract() { return bena9877_Pnd_Copy_From_Contract; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        bena9877 = dbsRecord.newGroupInRecord("bena9877", "BENA9877");
        bena9877.setParameterOption(ParameterOption.ByReference);
        bena9877_Pnd_Bsnss_Dte = bena9877.newFieldInGroup("bena9877_Pnd_Bsnss_Dte", "#BSNSS-DTE", FieldType.STRING, 8);
        bena9877_Pnd_Next_Bsnss_Dte = bena9877.newFieldInGroup("bena9877_Pnd_Next_Bsnss_Dte", "#NEXT-BSNSS-DTE", FieldType.STRING, 8);
        bena9877_Pnd_Cntrct_Lob_Cde = bena9877.newFieldInGroup("bena9877_Pnd_Cntrct_Lob_Cde", "#CNTRCT-LOB-CDE", FieldType.STRING, 1);
        bena9877_Pnd_Cntrct_Nmbr_Range_Cde = bena9877.newFieldInGroup("bena9877_Pnd_Cntrct_Nmbr_Range_Cde", "#CNTRCT-NMBR-RANGE-CDE", FieldType.STRING, 
            2);
        bena9877_Pnd_Cntrct_Spcl_Cnsdrtn_Cde = bena9877.newFieldInGroup("bena9877_Pnd_Cntrct_Spcl_Cnsdrtn_Cde", "#CNTRCT-SPCL-CNSDRTN-CDE", FieldType.STRING, 
            2);
        bena9877_Pnd_Mos_Isn = bena9877.newFieldInGroup("bena9877_Pnd_Mos_Isn", "#MOS-ISN", FieldType.PACKED_DECIMAL, 10);
        bena9877_Pnd_Dsgntn_Grp = bena9877.newGroupArrayInGroup("bena9877_Pnd_Dsgntn_Grp", "#DSGNTN-GRP", new DbsArrayController(1,30));
        bena9877_Pnd_Dsgntn_Isn = bena9877_Pnd_Dsgntn_Grp.newFieldInGroup("bena9877_Pnd_Dsgntn_Isn", "#DSGNTN-ISN", FieldType.PACKED_DECIMAL, 10);
        bena9877_Pnd_Dsgntn_Rltn_Cde = bena9877_Pnd_Dsgntn_Grp.newFieldInGroup("bena9877_Pnd_Dsgntn_Rltn_Cde", "#DSGNTN-RLTN-CDE", FieldType.STRING, 2);
        bena9877_Pnd_Cref_Cntrct_Nmbr = bena9877.newFieldInGroup("bena9877_Pnd_Cref_Cntrct_Nmbr", "#CREF-CNTRCT-NMBR", FieldType.STRING, 10);
        bena9877_Pnd_Error_Msg = bena9877.newFieldInGroup("bena9877_Pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 72);
        bena9877_Pnd_Error_Cde = bena9877.newFieldInGroup("bena9877_Pnd_Error_Cde", "#ERROR-CDE", FieldType.STRING, 5);
        bena9877_Pnd_Error_Pgm = bena9877.newFieldInGroup("bena9877_Pnd_Error_Pgm", "#ERROR-PGM", FieldType.STRING, 8);
        bena9877_Pnd_Edit_Failed = bena9877.newFieldInGroup("bena9877_Pnd_Edit_Failed", "#EDIT-FAILED", FieldType.BOOLEAN);
        bena9877_Pnd_Interface_As_Mos = bena9877.newFieldInGroup("bena9877_Pnd_Interface_As_Mos", "#INTERFACE-AS-MOS", FieldType.BOOLEAN);
        bena9877_Pnd_Copy_From_Contract = bena9877.newFieldInGroup("bena9877_Pnd_Copy_From_Contract", "#COPY-FROM-CONTRACT", FieldType.STRING, 10);

        dbsRecord.reset();
    }

    // Constructors
    public PdaBena9877(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

