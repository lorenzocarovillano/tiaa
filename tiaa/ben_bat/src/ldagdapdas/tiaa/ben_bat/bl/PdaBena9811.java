/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:18:10 PM
**        * FROM NATURAL PDA     : BENA9811
************************************************************
**        * FILE NAME            : PdaBena9811.java
**        * CLASS NAME           : PdaBena9811
**        * INSTANCE NAME        : PdaBena9811
************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaBena9811 extends PdaBase
{
    // Properties
    private DbsGroup bena9811;
    private DbsField bena9811_Pnd_Bsnss_Dte;
    private DbsField bena9811_Pnd_Cntrct_Isn;
    private DbsField bena9811_Pnd_Pin_Tiaa_Cntrct;
    private DbsGroup bena9811_Pnd_Pin_Tiaa_CntrctRedef1;
    private DbsField bena9811_Pnd_Pin;
    private DbsField bena9811_Pnd_Tiaa_Cntrct;
    private DbsField bena9811_Pnd_Tiaa_Cref_Ind;
    private DbsField bena9811_Pnd_Rcrd_Crtd_For_Bsnss_Dte;
    private DbsField bena9811_Pnd_Mos_Ind;
    private DbsField bena9811_Pnd_Intrfce_Stts;
    private DbsField bena9811_Pnd_New_Intrfce_Stts;

    public DbsGroup getBena9811() { return bena9811; }

    public DbsField getBena9811_Pnd_Bsnss_Dte() { return bena9811_Pnd_Bsnss_Dte; }

    public DbsField getBena9811_Pnd_Cntrct_Isn() { return bena9811_Pnd_Cntrct_Isn; }

    public DbsField getBena9811_Pnd_Pin_Tiaa_Cntrct() { return bena9811_Pnd_Pin_Tiaa_Cntrct; }

    public DbsGroup getBena9811_Pnd_Pin_Tiaa_CntrctRedef1() { return bena9811_Pnd_Pin_Tiaa_CntrctRedef1; }

    public DbsField getBena9811_Pnd_Pin() { return bena9811_Pnd_Pin; }

    public DbsField getBena9811_Pnd_Tiaa_Cntrct() { return bena9811_Pnd_Tiaa_Cntrct; }

    public DbsField getBena9811_Pnd_Tiaa_Cref_Ind() { return bena9811_Pnd_Tiaa_Cref_Ind; }

    public DbsField getBena9811_Pnd_Rcrd_Crtd_For_Bsnss_Dte() { return bena9811_Pnd_Rcrd_Crtd_For_Bsnss_Dte; }

    public DbsField getBena9811_Pnd_Mos_Ind() { return bena9811_Pnd_Mos_Ind; }

    public DbsField getBena9811_Pnd_Intrfce_Stts() { return bena9811_Pnd_Intrfce_Stts; }

    public DbsField getBena9811_Pnd_New_Intrfce_Stts() { return bena9811_Pnd_New_Intrfce_Stts; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        bena9811 = dbsRecord.newGroupInRecord("bena9811", "BENA9811");
        bena9811.setParameterOption(ParameterOption.ByReference);
        bena9811_Pnd_Bsnss_Dte = bena9811.newFieldInGroup("bena9811_Pnd_Bsnss_Dte", "#BSNSS-DTE", FieldType.STRING, 8);
        bena9811_Pnd_Cntrct_Isn = bena9811.newFieldInGroup("bena9811_Pnd_Cntrct_Isn", "#CNTRCT-ISN", FieldType.PACKED_DECIMAL, 10);
        bena9811_Pnd_Pin_Tiaa_Cntrct = bena9811.newFieldInGroup("bena9811_Pnd_Pin_Tiaa_Cntrct", "#PIN-TIAA-CNTRCT", FieldType.STRING, 22);
        bena9811_Pnd_Pin_Tiaa_CntrctRedef1 = bena9811.newGroupInGroup("bena9811_Pnd_Pin_Tiaa_CntrctRedef1", "Redefines", bena9811_Pnd_Pin_Tiaa_Cntrct);
        bena9811_Pnd_Pin = bena9811_Pnd_Pin_Tiaa_CntrctRedef1.newFieldInGroup("bena9811_Pnd_Pin", "#PIN", FieldType.NUMERIC, 12);
        bena9811_Pnd_Tiaa_Cntrct = bena9811_Pnd_Pin_Tiaa_CntrctRedef1.newFieldInGroup("bena9811_Pnd_Tiaa_Cntrct", "#TIAA-CNTRCT", FieldType.STRING, 10);
        bena9811_Pnd_Tiaa_Cref_Ind = bena9811.newFieldInGroup("bena9811_Pnd_Tiaa_Cref_Ind", "#TIAA-CREF-IND", FieldType.STRING, 1);
        bena9811_Pnd_Rcrd_Crtd_For_Bsnss_Dte = bena9811.newFieldInGroup("bena9811_Pnd_Rcrd_Crtd_For_Bsnss_Dte", "#RCRD-CRTD-FOR-BSNSS-DTE", FieldType.STRING, 
            8);
        bena9811_Pnd_Mos_Ind = bena9811.newFieldInGroup("bena9811_Pnd_Mos_Ind", "#MOS-IND", FieldType.STRING, 1);
        bena9811_Pnd_Intrfce_Stts = bena9811.newFieldInGroup("bena9811_Pnd_Intrfce_Stts", "#INTRFCE-STTS", FieldType.STRING, 1);
        bena9811_Pnd_New_Intrfce_Stts = bena9811.newFieldInGroup("bena9811_Pnd_New_Intrfce_Stts", "#NEW-INTRFCE-STTS", FieldType.STRING, 1);

        dbsRecord.reset();
    }

    // Constructors
    public PdaBena9811(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

