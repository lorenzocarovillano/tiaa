/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:18:15 PM
**        * FROM NATURAL PDA     : BENA9910
************************************************************
**        * FILE NAME            : PdaBena9910.java
**        * CLASS NAME           : PdaBena9910
**        * INSTANCE NAME        : PdaBena9910
************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaBena9910 extends PdaBase
{
    // Properties
    private DbsGroup bena9910;
    private DbsField bena9910_Pnd_Bsnss_Dte;
    private DbsGroup bena9910_Pnd_Bsnss_DteRedef1;
    private DbsField bena9910_Pnd_Bsnss_Dte_N;

    public DbsGroup getBena9910() { return bena9910; }

    public DbsField getBena9910_Pnd_Bsnss_Dte() { return bena9910_Pnd_Bsnss_Dte; }

    public DbsGroup getBena9910_Pnd_Bsnss_DteRedef1() { return bena9910_Pnd_Bsnss_DteRedef1; }

    public DbsField getBena9910_Pnd_Bsnss_Dte_N() { return bena9910_Pnd_Bsnss_Dte_N; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        bena9910 = dbsRecord.newGroupInRecord("bena9910", "BENA9910");
        bena9910.setParameterOption(ParameterOption.ByReference);
        bena9910_Pnd_Bsnss_Dte = bena9910.newFieldInGroup("bena9910_Pnd_Bsnss_Dte", "#BSNSS-DTE", FieldType.STRING, 8);
        bena9910_Pnd_Bsnss_DteRedef1 = bena9910.newGroupInGroup("bena9910_Pnd_Bsnss_DteRedef1", "Redefines", bena9910_Pnd_Bsnss_Dte);
        bena9910_Pnd_Bsnss_Dte_N = bena9910_Pnd_Bsnss_DteRedef1.newFieldInGroup("bena9910_Pnd_Bsnss_Dte_N", "#BSNSS-DTE-N", FieldType.NUMERIC, 8);

        dbsRecord.reset();
    }

    // Constructors
    public PdaBena9910(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

