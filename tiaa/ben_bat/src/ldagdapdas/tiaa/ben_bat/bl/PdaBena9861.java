/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:18:12 PM
**        * FROM NATURAL PDA     : BENA9861
************************************************************
**        * FILE NAME            : PdaBena9861.java
**        * CLASS NAME           : PdaBena9861
**        * INSTANCE NAME        : PdaBena9861
************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaBena9861 extends PdaBase
{
    // Properties
    private DbsGroup bena9861;
    private DbsField bena9861_Pnd_Pin;
    private DbsField bena9861_Pnd_Dte_Of_Dth_Found;
    private DbsField bena9861_Pnd_Dte_Of_Dth;
    private DbsField bena9861_Pnd_Pin_Found;

    public DbsGroup getBena9861() { return bena9861; }

    public DbsField getBena9861_Pnd_Pin() { return bena9861_Pnd_Pin; }

    public DbsField getBena9861_Pnd_Dte_Of_Dth_Found() { return bena9861_Pnd_Dte_Of_Dth_Found; }

    public DbsField getBena9861_Pnd_Dte_Of_Dth() { return bena9861_Pnd_Dte_Of_Dth; }

    public DbsField getBena9861_Pnd_Pin_Found() { return bena9861_Pnd_Pin_Found; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        bena9861 = dbsRecord.newGroupInRecord("bena9861", "BENA9861");
        bena9861.setParameterOption(ParameterOption.ByReference);
        bena9861_Pnd_Pin = bena9861.newFieldInGroup("bena9861_Pnd_Pin", "#PIN", FieldType.NUMERIC, 12);
        bena9861_Pnd_Dte_Of_Dth_Found = bena9861.newFieldInGroup("bena9861_Pnd_Dte_Of_Dth_Found", "#DTE-OF-DTH-FOUND", FieldType.BOOLEAN);
        bena9861_Pnd_Dte_Of_Dth = bena9861.newFieldInGroup("bena9861_Pnd_Dte_Of_Dth", "#DTE-OF-DTH", FieldType.STRING, 8);
        bena9861_Pnd_Pin_Found = bena9861.newFieldInGroup("bena9861_Pnd_Pin_Found", "#PIN-FOUND", FieldType.BOOLEAN);

        dbsRecord.reset();
    }

    // Constructors
    public PdaBena9861(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

