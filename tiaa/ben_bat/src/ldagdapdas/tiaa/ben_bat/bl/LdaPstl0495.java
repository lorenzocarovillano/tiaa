/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:10:11 PM
**        * FROM NATURAL LDA     : PSTL0495
************************************************************
**        * FILE NAME            : LdaPstl0495.java
**        * CLASS NAME           : LdaPstl0495
**        * INSTANCE NAME        : LdaPstl0495
************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaPstl0495 extends DbsRecord
{
    // Properties
    private DbsGroup sort_Key;
    private DbsField sort_Key_Sort_Key_Lgth;
    private DbsField sort_Key_Sort_Key_Array;
    private DbsGroup pstl0495_Data;
    private DbsField pstl0495_Data_Pstl0495_Rec_Id;
    private DbsField pstl0495_Data_Pstl0495_Data_Lgth;
    private DbsField pstl0495_Data_Pstl0495_Data_Occurs;
    private DbsField pstl0495_Data_Pstl0495_Data_Array;
    private DbsGroup pstl0495_Data_Pstl0495_Data_ArrayRedef1;
    private DbsGroup pstl0495_Data_Bene_Common_Data;
    private DbsField pstl0495_Data_Letter_Type;
    private DbsField pstl0495_Data_Case_Manager_Name;
    private DbsField pstl0495_Data_Case_Manager_Phone;
    private DbsField pstl0495_Data_Institution_Name;
    private DbsField pstl0495_Data_Decedent_Pin_Nbr;
    private DbsField pstl0495_Data_Decedent_Name;
    private DbsField pstl0495_Data_Decedent_Date_Of_Birth;
    private DbsField pstl0495_Data_Decedent_Date_Of_Death;
    private DbsField pstl0495_Data_Cont_Beneficiary_Name;
    private DbsField pstl0495_Data_Minor_Org_Estate_Name;
    private DbsField pstl0495_Data_Trust_Name;
    private DbsField pstl0495_Data_Children_Drop_Down;
    private DbsField pstl0495_Data_Pages_Missing;
    private DbsField pstl0495_Data_Death_Cert_Bene_Names;
    private DbsField pstl0495_Data_Pvc_Delivery_Type;
    private DbsField pstl0495_Data_Survivor_Task_Category;
    private DbsField pstl0495_Data_Unclaimed_Funds_State_Name;
    private DbsField pstl0495_Data_Unclaimed_Funds_State_Cde;
    private DbsField pstl0495_Data_Unclaimed_Funds_Contract_Nbr;
    private DbsField pstl0495_Data_Brm_Name;
    private DbsField pstl0495_Data_Footer_Location;
    private DbsGroup pstl0495_Data_Survivor_Forms;
    private DbsField pstl0495_Data_Renun_Waiver_Release_Ind;
    private DbsField pstl0495_Data_Marital_Stat_Addendum_Ind;
    private DbsField pstl0495_Data_Release_Indem_Agree_Ind;
    private DbsGroup pstl0495_Data_Beneficiary_Forms;
    private DbsField pstl0495_Data_Savings_Invest_Appl_Ind;
    private DbsField pstl0495_Data_Survivor_Bene_Dist_Rqst_Ind;
    private DbsField pstl0495_Data_Income_Annuity_Request_Form_Ind;
    private DbsField pstl0495_Data_Immediate_Annuity_Form_Ind;
    private DbsField pstl0495_Data_One_Time_Pay_Request_Form_Ind;
    private DbsGroup pstl0495_Data_Change_Of_Beneficiary_Forms;
    private DbsField pstl0495_Data_Da_Tpa_Desig_Form_Ind;
    private DbsField pstl0495_Data_Da_Tpa_Desig_Form_Ctr;
    private DbsField pstl0495_Data_Ia_Beneficiary_Form_Ind;
    private DbsField pstl0495_Data_Ia_Beneficiary_Form_Ctr;
    private DbsField pstl0495_Data_Ohio_Arp_Bene_Form_Ind;
    private DbsField pstl0495_Data_Ohio_Arp_Bene_Form_Ctr;
    private DbsField pstl0495_Data_Univ_Pittsburgh_Bene_Form_Ind;
    private DbsField pstl0495_Data_Univ_Pittsburgh_Bene_Form_Ctr;
    private DbsField pstl0495_Data_Univ_Michigan_Bene_Form_Ind;
    private DbsField pstl0495_Data_Univ_Michigan_Bene_Form_Ctr;
    private DbsField pstl0495_Data_Successor_Beneficiary_Form_Ind;
    private DbsField pstl0495_Data_Successor_Beneficiary_Form_Ctr;
    private DbsGroup pstl0495_Data_Tax_Forms;
    private DbsField pstl0495_Data_Ohio_Consent_Ind;
    private DbsField pstl0495_Data_Oklahoma_Consent_Ind;
    private DbsField pstl0495_Data_Indiana_Notice_Ind;
    private DbsField pstl0495_Data_Rhode_Island_Notice_Ind;
    private DbsField pstl0495_Data_W9_Form_Ind;
    private DbsField pstl0495_Data_W8ben_Form_Ind;
    private DbsField pstl0495_Data_W7_Form_Ind;
    private DbsField pstl0495_Data_State_Code_Taxint;
    private DbsField pstl0495_Data_State_Name_Taxint;
    private DbsField pstl0495_Data_State_Name_Text;
    private DbsGroup pstl0495_Data_Pre_Printed_Booklets;
    private DbsField pstl0495_Data_Loved_One_Passes_Away_Ind;
    private DbsField pstl0495_Data_Sip_Booklet_Ind;
    private DbsField pstl0495_Data_Taking_Next_Step_Ind;
    private DbsField pstl0495_Data_Qtrly_Performance_Ind;
    private DbsField pstl0495_Data_Tiaa_Cref_Prospectus_Ind;
    private DbsField pstl0495_Data_Cref_Prospectus_Ind;
    private DbsField pstl0495_Data_Tiaa_Rea_Prospectus_Ind;
    private DbsField pstl0495_Data_Lifecycle_Prospectus_Ind;
    private DbsField pstl0495_Data_Tiaa_Rate_Sheet_Ind;
    private DbsGroup pstl0495_Data_Booklets_Prospectuses_With_Pdf;
    private DbsField pstl0495_Data_Atra_Sip_Glance_Indvl_Ind;
    private DbsField pstl0495_Data_Atra_Ssum_Glance_Trust_Ind;
    private DbsField pstl0495_Data_Atra_Ssum_Glance_Indvl_Ind;
    private DbsField pstl0495_Data_Atra_Ssum_Glance_Estate_Ind;
    private DbsField pstl0495_Data_Atra_Annty_Glance_Indvl_Ind;
    private DbsField pstl0495_Data_Atra_Fix_Prd_Ann_Glance_Ind;
    private DbsField pstl0495_Data_Ssum_Glance_Nonspouse_Ind;
    private DbsField pstl0495_Data_Ssum_Glance_Spouse_Ind;
    private DbsField pstl0495_Data_Ssum_Glance_Trust_Ind;
    private DbsField pstl0495_Data_Ssum_Glance_Estate_Ind;
    private DbsField pstl0495_Data_Notice_402f_Ind;
    private DbsField pstl0495_Data_Custodial_Agreement_Ind;
    private DbsField pstl0495_Data_Record_Of_Age_Ind;
    private DbsField pstl0495_Data_Payment_Dest_Form_Ind;
    private DbsField pstl0495_Data_Transfer_At_Settlement_Ind;
    private DbsGroup pstl0495_Data_Step0_Blurbs;
    private DbsField pstl0495_Data_My_Children_Ind;
    private DbsField pstl0495_Data_Minor_Guardian_Hip_Ind;
    private DbsField pstl0495_Data_Exspouse_Divorce_Decree_Ind;
    private DbsField pstl0495_Data_Estate_Ind;
    private DbsField pstl0495_Data_Small_Estate_Ind;
    private DbsField pstl0495_Data_Unable_To_Locate_Trust_Ind;
    private DbsField pstl0495_Data_Need_Bene_Address_Ind;
    private DbsField pstl0495_Data_Organization_Ind;
    private DbsField pstl0495_Data_Trust_Ind;
    private DbsField pstl0495_Data_Msv_Ind;
    private DbsGroup pstl0495_Data_Entitlement_Blurbs;
    private DbsField pstl0495_Data_Roth_Accumulation_Ind;
    private DbsField pstl0495_Data_Atra_Accumulation_Ind;
    private DbsField pstl0495_Data_Investment_Contract_Ind;
    private DbsGroup pstl0495_Data_Decline_Of_Change_Blurbs;
    private DbsField pstl0495_Data_Allocation_Ind;
    private DbsField pstl0495_Data_Blank_Beneficiaries_Ind;
    private DbsField pstl0495_Data_Contract_Numbers_Ind;
    private DbsField pstl0495_Data_Invalid_Date_Ind;
    private DbsField pstl0495_Data_Estate_Trust_Ind;
    private DbsField pstl0495_Data_Executor_Ind;
    private DbsField pstl0495_Data_Fax_Copies_Ind;
    private DbsField pstl0495_Data_Guarantee_Period_Expire_Ind;
    private DbsField pstl0495_Data_No_Guarantee_Period_Ind;
    private DbsField pstl0495_Data_Indvl_Bene_Trust_Ind;
    private DbsField pstl0495_Data_Institution_Maintained_Ind;
    private DbsField pstl0495_Data_Irrevocable_Designation_Ind;
    private DbsField pstl0495_Data_Missing_Pages_Ind;
    private DbsField pstl0495_Data_Inactive_Contracts_Ind;
    private DbsField pstl0495_Data_Or_Used_In_Designation_Ind;
    private DbsField pstl0495_Data_Same_Primary_Contingent_Ind;
    private DbsField pstl0495_Data_Siblings_Ind;
    private DbsField pstl0495_Data_No_Signature_Date_Ind;
    private DbsField pstl0495_Data_Spouse_As_Primary_Bene_Ind;
    private DbsField pstl0495_Data_Surs_Ind;
    private DbsGroup pstl0495_Data_Accept_Blurbs;
    private DbsField pstl0495_Data_Age_Stipulation_Ind;
    private DbsField pstl0495_Data_Deceased_Child_Chk_Gchild_Ind;
    private DbsField pstl0495_Data_Deceased_Child_Bxchk_Schild_Ind;
    private DbsField pstl0495_Data_Difficult_To_Read_Ind;
    private DbsField pstl0495_Data_Divesting_Ind;
    private DbsField pstl0495_Data_For_The_Benefit_Of_Ind;
    private DbsField pstl0495_Data_Guardian_Ind;
    private DbsField pstl0495_Data_Mdo_Contract_Ind;
    private DbsField pstl0495_Data_Missing_Information_Ind;
    private DbsField pstl0495_Data_Department_Ind;
    private DbsField pstl0495_Data_No_Primary_Bene_Ind;
    private DbsField pstl0495_Data_Second_Annuitant_Named_Ind;
    private DbsField pstl0495_Data_Ssn_Not_Shown_Ind;
    private DbsField pstl0495_Data_Spouse_Another_Person_Ind;
    private DbsField pstl0495_Data_Spousal_Waiver_Inc_Ind;
    private DbsField pstl0495_Data_Survivorship_Clause;
    private DbsField pstl0495_Data_Tin_Ind;
    private DbsField pstl0495_Data_Terms_Of_Trust_Ind;
    private DbsField pstl0495_Data_Trust_Date_Ind;
    private DbsField pstl0495_Data_Unborn_Children_Ind;
    private DbsField pstl0495_Data_Unnamed_Spouse_Of_Bene_Ind;
    private DbsGroup pstl0495_Data_Forms_Change_Blurbs;
    private DbsField pstl0495_Data_No_Designation_Found_Ind;
    private DbsField pstl0495_Data_Poor_Copy_Ind;
    private DbsField pstl0495_Data_Retirement_Loan_Ind;
    private DbsField pstl0495_Data_Wpahs_Ind;
    private DbsGroup pstl0495_Data_Change_Of_Bene_Blurb;
    private DbsField pstl0495_Data_Erisa_Ind;
    private DbsGroup pstl0495_Data_Bene_Option_Blurbs;
    private DbsField pstl0495_Data_Death_Certificate_Reqrd_Ind;
    private DbsField pstl0495_Data_Death_Cert_Primary_Bene_Ind;
    private DbsField pstl0495_Data_Statement_Will_Ind;
    private DbsField pstl0495_Data_Statement_Trust_In_Effect_Ind;
    private DbsField pstl0495_Data_Statement_Trust_Accept_Ind;
    private DbsField pstl0495_Data_Taxpayer_Id_Nbr_Ind;
    private DbsField pstl0495_Data_Letters_Of_Appointment_Ind;
    private DbsField pstl0495_Data_Small_Accum_Ind;
    private DbsField pstl0495_Data_Signature_Guarantee_Ind;
    private DbsField pstl0495_Data_Death_Certificate_Ind;
    private DbsField pstl0495_Data_Reason_Code;
    private DbsField pstl0495_Data_Reason_Text;
    private DbsField pstl0495_Data_Non_Spouse_Rmd_Ind;
    private DbsField pstl0495_Data_Spouse_Rmd_Ind;
    private DbsField pstl0495_Data_Doma_Ind;
    private DbsGroup pstl0495_Data_Valuation_Details;
    private DbsField pstl0495_Data_Valuation_Contract_Nbr;
    private DbsField pstl0495_Data_Value_Of_Contract;
    private DbsField pstl0495_Data_Total_At_Valuation_Date;
    private DbsField pstl0495_Data_Alternate_Valuation_Date;
    private DbsField pstl0495_Data_Investment_In_Contract;
    private DbsGroup pstl0495_Data_Entitlement_Details;
    private DbsField pstl0495_Data_Account_Name;
    private DbsField pstl0495_Data_Account_Amount;
    private DbsField pstl0495_Data_Entitlement_Total_Amount;
    private DbsField pstl0495_Data_Roth_Accumulation;
    private DbsField pstl0495_Data_Atra_Accumulation;
    private DbsField pstl0495_Data_Invest_And_Contract_Amount;
    private DbsGroup pstl0495_Data_Incomplete_Bene_Change_Details;
    private DbsField pstl0495_Data_Tiaa_Nbr;
    private DbsField pstl0495_Data_Cref_Nbr;
    private DbsField pstl0495_Data_Decedent_Other_Than_Orig;
    private DbsGroup pstl0495_Data_Mandatory_Tax_Form_Indicators;
    private DbsField pstl0495_Data_Rollover_Ind;
    private DbsField pstl0495_Data_Periodic_Ind;
    private DbsGroup pstl0495_Data_New_Fields;
    private DbsField pstl0495_Data_Wrong_Form_Ind;
    private DbsField pstl0495_Data_Ap_Payment_Freq_Ind;
    private DbsField pstl0495_Data_Payment_Coupon_Ind;
    private DbsField pstl0495_Data_Coupon_Tiaa_Amt;
    private DbsField pstl0495_Data_Coupon_Cref_Amt;
    private DbsField pstl0495_Data_Coupon_Total_Amt;
    private DbsField pstl0495_Data_Bene_Prov_Date_Death;
    private DbsField pstl0495_Data_Free_Text;
    private DbsField pstl0495_Data_Benefit_Type_Ind;
    private DbsField pstl0495_Data_Da_5k_Ind;
    private DbsField pstl0495_Data_Sip_Tiaa_Nbr;
    private DbsField pstl0495_Data_Sip_Cref_Nbr;
    private DbsField pstl0495_Data_Ssum_Tiaa_Nbr;
    private DbsField pstl0495_Data_Ssum_Cref_Nbr;
    private DbsGroup pstl0495_Data_Bene_In_The_Plan_New_Fields;
    private DbsField pstl0495_Data_Spouse_Rmd_Bene_Ind;
    private DbsField pstl0495_Data_Entity_Rmd_Ind;
    private DbsField pstl0495_Data_Non_Spouse_Rmd_Bene_Ind;
    private DbsField pstl0495_Data_Second_Gen_Rmd_Ind;
    private DbsField pstl0495_Data_Phone_Acceptance_Ind;
    private DbsField pstl0495_Data_Register_Online_Acct_Ind;
    private DbsField pstl0495_Data_Trust_Relation_Paragraph_Ind;
    private DbsField pstl0495_Data_Brokerage_Retail_Ind;
    private DbsField pstl0495_Data_Brokerage_Ira_Ind;
    private DbsField pstl0495_Data_Brokerage_Ira_Roth_Ind;
    private DbsField pstl0495_Data_Sole_Spouse_Ira_Rmd_Ind;
    private DbsField pstl0495_Data_Sole_Spouse_Atra_Rmd_Ind;
    private DbsField pstl0495_Data_Sole_Spouse_Both_Rmd_Ind;
    private DbsField pstl0495_Data_Accept_Form_Spouse_Ind;
    private DbsField pstl0495_Data_Accept_Form_Bene_Ind;
    private DbsField pstl0495_Data_Accept_Form_Entity_Ind;
    private DbsField pstl0495_Data_Onetime_Transfer_Spouse_Ind;
    private DbsField pstl0495_Data_Onetime_Transfer_Bene_Ind;
    private DbsField pstl0495_Data_Tpa_Cont_Spouse_Ind;
    private DbsField pstl0495_Data_Tpa_Cont_Bene_Ind;
    private DbsField pstl0495_Data_Cash_Wdraw_Page_Spouse_Ind;
    private DbsField pstl0495_Data_Cash_Wdraw_Page_Bene_Ind;
    private DbsField pstl0495_Data_Rollover_Page_Spouse_Ind;
    private DbsField pstl0495_Data_Rollover_Page_Bene_Ind;
    private DbsField pstl0495_Data_Customer_Agree_Retail_Ind;
    private DbsField pstl0495_Data_Affidavit_Domicile_Ind;
    private DbsField pstl0495_Data_Cert_Invest_Powers_Ind;
    private DbsField pstl0495_Data_Power_Of_Atty_Ind;
    private DbsField pstl0495_Data_Loi_Existing_Ind;
    private DbsField pstl0495_Data_Loi_Trust_Ind;
    private DbsField pstl0495_Data_Loi_New_Ind;
    private DbsField pstl0495_Data_Loi_Jtwros_Ind;
    private DbsField pstl0495_Data_Onetime_Transfer_Amt;
    private DbsField pstl0495_Data_Second_Generation_Amt;
    private DbsField pstl0495_Data_Brokerage_Retail_Amt;
    private DbsField pstl0495_Data_Brokerage_Ira_Amt;
    private DbsField pstl0495_Data_Brokerage_Ira_Roth_Amt;
    private DbsField pstl0495_Data_Brokerage_Retail_Acct_Nbr;
    private DbsField pstl0495_Data_Brokerage_Ira_Acct_Nbr;
    private DbsField pstl0495_Data_Brokerage_Roth_Acct_Nbr;
    private DbsField pstl0495_Data_Ro_Account_Nbr;
    private DbsField pstl0495_Data_Loi_Ira_Ind;
    private DbsField pstl0495_Data_Loi_Estate_Ind;
    private DbsField pstl0495_Data_Tpa_Page;
    private DbsGroup pstl0495_Data_Pstl0495_Data_ArrayRedef2;
    private DbsGroup pstl0495_Data_Pre_Fill_Data;
    private DbsGroup pstl0495_Data_Beneficiary_Information;
    private DbsField pstl0495_Data_Bi_Bene_Type;
    private DbsField pstl0495_Data_Bi_Bene_Name;
    private DbsField pstl0495_Data_Bi_Bene_Trustee_Name;
    private DbsField pstl0495_Data_Bi_Street;
    private DbsField pstl0495_Data_Bi_City;
    private DbsField pstl0495_Data_Bi_State;
    private DbsField pstl0495_Data_Bi_Zip;
    private DbsField pstl0495_Data_Bi_Primary_Phone;
    private DbsField pstl0495_Data_Bi_Alt_Phone;
    private DbsField pstl0495_Data_Bi_Email_Address;
    private DbsField pstl0495_Data_Bi_State_Legal_Residency;
    private DbsField pstl0495_Data_Bi_Citizenship;
    private DbsField pstl0495_Data_Bi_Dob_Trust_Amend_Date;
    private DbsField pstl0495_Data_Bi_Gender;
    private DbsField pstl0495_Data_Bi_Bene_Relation_Decedent;
    private DbsGroup pstl0495_Data_Calc_Bene_For_Trust_Only;
    private DbsField pstl0495_Data_Ct_Trustee_Name;
    private DbsField pstl0495_Data_Ct_Trustee_Dob;
    private DbsField pstl0495_Data_Ct_Trustee_Relation_Decedent;
    private DbsGroup pstl0495_Data_Settlement_Option;
    private DbsField pstl0495_Data_So_Option_Type;
    private DbsField pstl0495_Data_So_Partial_Cash_Amt;
    private DbsField pstl0495_Data_So_Partial_Cash_Pct;
    private DbsGroup pstl0495_Data_Settlement_Option_Spouses;
    private DbsField pstl0495_Data_Ss_Option_Type;
    private DbsField pstl0495_Data_Ss_Partial_Cash_Amt;
    private DbsField pstl0495_Data_Ss_Partial_Cash_Pct;
    private DbsField pstl0495_Data_Ss_Rollover_Acct_Status;
    private DbsField pstl0495_Data_Ss_Rollover_Acct_Type;
    private DbsField pstl0495_Data_Ss_Rollover_Acct_Nbr;
    private DbsField pstl0495_Data_Ss_Tiaacref_Acct_Status;
    private DbsField pstl0495_Data_Ss_Tiaacref_Acct_Type;
    private DbsField pstl0495_Data_Ss_Tiaacref_Acct_Nbr;
    private DbsGroup pstl0495_Data_Settlement_Option_Non_Spouses;
    private DbsField pstl0495_Data_Sn_Option_Type;
    private DbsField pstl0495_Data_Sn_Partial_Cash_Amt;
    private DbsField pstl0495_Data_Sn_Partial_Cash_Pct;
    private DbsField pstl0495_Data_Sn_Rollover_Acct_Status;
    private DbsField pstl0495_Data_Sn_Rollover_Acct_Type;
    private DbsField pstl0495_Data_Sn_Rollover_Acct_Nbr;
    private DbsField pstl0495_Data_Sn_Tiaacref_Acct_Status;
    private DbsField pstl0495_Data_Sn_Tiaacref_Acct_Type;
    private DbsField pstl0495_Data_Sn_Tiaacref_Acct_Nbr;
    private DbsField pstl0495_Data_Sn_Md_Olest_Bene_Dob;
    private DbsGroup pstl0495_Data_Choose_Frequency;
    private DbsField pstl0495_Data_Cf_Mmdd;
    private DbsField pstl0495_Data_Cf_Freq;
    private DbsGroup pstl0495_Data_Type_Of_Rollover;
    private DbsField pstl0495_Data_Tr_Rollover_Type;
    private DbsField pstl0495_Data_Tr_O1_Trad_Ira_Nbr;
    private DbsField pstl0495_Data_Tr_O3_Company_Name;
    private DbsField pstl0495_Data_Tr_O3_Company_Address;
    private DbsField pstl0495_Data_Tr_O3_Company_City;
    private DbsField pstl0495_Data_Tr_O3_Company_State;
    private DbsField pstl0495_Data_Tr_O3_Company_Zip;
    private DbsField pstl0495_Data_Tr_O3_Company_Phone;
    private DbsField pstl0495_Data_Tr_O3_Company_Acct_Nbr;
    private DbsGroup pstl0495_Data_Rollover_To_Another_Invco;
    private DbsField pstl0495_Data_Ri_Option_Cde;
    private DbsField pstl0495_Data_Ri_Invest_Company_Name;
    private DbsField pstl0495_Data_Ri_Invest_Company_Address;
    private DbsField pstl0495_Data_Ri_Invest_Company_City;
    private DbsField pstl0495_Data_Ri_Invest_Company_State;
    private DbsField pstl0495_Data_Ri_Invest_Company_Zip;
    private DbsField pstl0495_Data_Ri_Invest_Company_Phone;
    private DbsField pstl0495_Data_Ri_Invest_Company_Acct_Nbr;
    private DbsGroup pstl0495_Data_Roth_Only;
    private DbsField pstl0495_Data_Ro_Roth_Ind;
    private DbsField pstl0495_Data_Ro_Roth_Amt;
    private DbsField pstl0495_Data_Ro_Roth_Pct;
    private DbsGroup pstl0495_Data_Federal_Tax_Wholding_Amount;
    private DbsField pstl0495_Data_Ft_Whold_Type;
    private DbsField pstl0495_Data_Ft_Da_Dollar_Amt;
    private DbsField pstl0495_Data_Ft_Pc_Percent;
    private DbsField pstl0495_Data_Ft_Re_Dollar_Amt;
    private DbsField pstl0495_Data_Ft_Re_Percent;
    private DbsField pstl0495_Data_Ft_N2_Dollar_Amt;
    private DbsField pstl0495_Data_Ft_N2_Percent;
    private DbsField pstl0495_Data_Ft_N3_Status;
    private DbsField pstl0495_Data_Ft_N3_Nbr_Whold_Allow;
    private DbsField pstl0495_Data_Ft_Op_Dollar_Amt;
    private DbsField pstl0495_Data_Ft_Cv_Dollar_Amt;
    private DbsField pstl0495_Data_Ft_Cv_Percent;
    private DbsGroup pstl0495_Data_Beneficiary_List;
    private DbsGroup pstl0495_Data_Bl_Primary_List;
    private DbsField pstl0495_Data_Bl_Primary_Name;
    private DbsField pstl0495_Data_Bl_Primary_Pct;
    private DbsField pstl0495_Data_Bl_Primary_Dob;
    private DbsField pstl0495_Data_Bl_Primary_Relation;
    private DbsGroup pstl0495_Data_Bl_Contingent_List;
    private DbsField pstl0495_Data_Bl_Contingent_Name;
    private DbsField pstl0495_Data_Bl_Contingent_Pct;
    private DbsField pstl0495_Data_Bl_Contingent_Dob;
    private DbsField pstl0495_Data_Bl_Contingent_Relation;
    private DbsGroup pstl0495_Data_Delivery_Instructions;
    private DbsField pstl0495_Data_Di_Option_Cde;
    private DbsField pstl0495_Data_Di_O2_Acct_Type;
    private DbsField pstl0495_Data_Di_O2_Amt;
    private DbsField pstl0495_Data_Di_O2_Invest_Bank_Name;
    private DbsField pstl0495_Data_Di_O2_Address;
    private DbsField pstl0495_Data_Di_O2_City;
    private DbsField pstl0495_Data_Di_O2_State;
    private DbsField pstl0495_Data_Di_O2_Zip;
    private DbsField pstl0495_Data_Di_O2_Bank_Acct_Phone;
    private DbsField pstl0495_Data_Di_O2_Bank_Acct_Nbr;
    private DbsField pstl0495_Data_Di_O2_Bank_Routing_Nbr;
    private DbsField pstl0495_Data_Di_O3_Address_Type;
    private DbsField pstl0495_Data_Di_O3_Address;
    private DbsField pstl0495_Data_Di_O3_City;
    private DbsField pstl0495_Data_Di_O3_State;
    private DbsField pstl0495_Data_Di_O3_Zip;
    private DbsField pstl0495_Data_Di_O4_Roth_Ira_Acct;
    private DbsGroup pstl0495_Data_Annuity_Method;
    private DbsField pstl0495_Data_Am_Option_Cde;
    private DbsField pstl0495_Data_Am_O1_Payment_Years;
    private DbsField pstl0495_Data_Am_O1_Payment_Freq;
    private DbsField pstl0495_Data_Am_O3_Guaranteed_Prd;
    private DbsField pstl0495_Data_Am_O3_Payment_Freq;
    private DbsGroup pstl0495_Data_Payment_Options;
    private DbsField pstl0495_Data_Po_Option_Cde;
    private DbsField pstl0495_Data_Po_Tiaa_Traditional_Ind;
    private DbsField pstl0495_Data_Po_Cref_Bond_Market_Ind;
    private DbsField pstl0495_Data_Po_Tiaa_Real_Estate_Ind;
    private DbsField pstl0495_Data_Po_Cref_Glbl_Equity_Ind;
    private DbsField pstl0495_Data_Po_Cref_Stock_Ind;
    private DbsField pstl0495_Data_Po_Cref_Growth_Ind;
    private DbsField pstl0495_Data_Po_Cref_Money_Mrkt_Ind;
    private DbsField pstl0495_Data_Po_Cref_Equity_Ndx_Ind;
    private DbsField pstl0495_Data_Po_Cref_Social_Chc_Ind;
    private DbsField pstl0495_Data_Po_Cref_Inf_Linked_Ind;
    private DbsGroup pstl0495_Data_Payment_Method;
    private DbsField pstl0495_Data_Pm_Option_Cde;
    private DbsField pstl0495_Data_Pm_O1_This_Amt;
    private DbsField pstl0495_Data_Pm_O1_Graded_Prd_Amt;
    private DbsField pstl0495_Data_Pm_O4_Freq;
    private DbsField pstl0495_Data_Pm_O4_Start_Life_Income_Dte;
    private DbsField pstl0495_Data_Pm_O4_Payment_Freq;
    private DbsGroup pstl0495_Data_Renunciation_Release_1time_Pay;
    private DbsField pstl0495_Data_Rw_Approx_Retire_Benefits;
    private DbsField pstl0495_Data_Rw_Asof_Date;
    private DbsGroup pstl0495_Data_Mdo_Only;
    private DbsField pstl0495_Data_Md_Withhold_Ind;
    private DbsField pstl0495_Data_Md_Withhold_Amt;
    private DbsGroup pstl0495_Data_Di_Additional;
    private DbsField pstl0495_Data_Di_O4_Tiaa_Acct_Type;
    private DbsField pstl0495_Data_Di_O4_Careof;
    private DbsGroup pstl0495_Data_Pstl0495_Data_ArrayRedef3;
    private DbsGroup pstl0495_Data_Power_Image_Task_Information;
    private DbsField pstl0495_Data_Pi_Begin_Literal;
    private DbsField pstl0495_Data_Pi_Export_Ind;
    private DbsField pstl0495_Data_Pi_Task_Id;
    private DbsField pstl0495_Data_Pi_Task_Type;
    private DbsField pstl0495_Data_Pi_Task_Guid;
    private DbsField pstl0495_Data_Pi_Action_Step;
    private DbsField pstl0495_Data_Pi_Tiaa_Full_Date;
    private DbsGroup pstl0495_Data_Pi_Tiaa_Full_DateRedef4;
    private DbsField pstl0495_Data_Pi_Tiaa_Date_Century;
    private DbsField pstl0495_Data_Pi_Tiaa_Date_Year;
    private DbsField pstl0495_Data_Pi_Tiaa_Date_Month;
    private DbsField pstl0495_Data_Pi_Tiaa_Date_Day;
    private DbsField pstl0495_Data_Pi_Tiaa_Time;
    private DbsField pstl0495_Data_Pi_Task_Status;
    private DbsField pstl0495_Data_Pi_Ssn;
    private DbsField pstl0495_Data_Pi_Pin_Npin_Ppg;
    private DbsField pstl0495_Data_Pi_Pin_Type;
    private DbsField pstl0495_Data_Pi_Contract;
    private DbsGroup pstl0495_Data_Pi_ContractRedef5;
    private DbsField pstl0495_Data_Pi_Contract_A;
    private DbsField pstl0495_Data_Pi_Plan_Id;
    private DbsField pstl0495_Data_Pi_Doc_Content;
    private DbsField pstl0495_Data_Pi_Filler_1;
    private DbsField pstl0495_Data_Pi_Filler_2;
    private DbsField pstl0495_Data_Pi_Filler_3;
    private DbsField pstl0495_Data_Pi_Filler_4;
    private DbsField pstl0495_Data_Pi_Filler_5;
    private DbsField pstl0495_Data_Pi_End_Literal;

    public DbsGroup getSort_Key() { return sort_Key; }

    public DbsField getSort_Key_Sort_Key_Lgth() { return sort_Key_Sort_Key_Lgth; }

    public DbsField getSort_Key_Sort_Key_Array() { return sort_Key_Sort_Key_Array; }

    public DbsGroup getPstl0495_Data() { return pstl0495_Data; }

    public DbsField getPstl0495_Data_Pstl0495_Rec_Id() { return pstl0495_Data_Pstl0495_Rec_Id; }

    public DbsField getPstl0495_Data_Pstl0495_Data_Lgth() { return pstl0495_Data_Pstl0495_Data_Lgth; }

    public DbsField getPstl0495_Data_Pstl0495_Data_Occurs() { return pstl0495_Data_Pstl0495_Data_Occurs; }

    public DbsField getPstl0495_Data_Pstl0495_Data_Array() { return pstl0495_Data_Pstl0495_Data_Array; }

    public DbsGroup getPstl0495_Data_Pstl0495_Data_ArrayRedef1() { return pstl0495_Data_Pstl0495_Data_ArrayRedef1; }

    public DbsGroup getPstl0495_Data_Bene_Common_Data() { return pstl0495_Data_Bene_Common_Data; }

    public DbsField getPstl0495_Data_Letter_Type() { return pstl0495_Data_Letter_Type; }

    public DbsField getPstl0495_Data_Case_Manager_Name() { return pstl0495_Data_Case_Manager_Name; }

    public DbsField getPstl0495_Data_Case_Manager_Phone() { return pstl0495_Data_Case_Manager_Phone; }

    public DbsField getPstl0495_Data_Institution_Name() { return pstl0495_Data_Institution_Name; }

    public DbsField getPstl0495_Data_Decedent_Pin_Nbr() { return pstl0495_Data_Decedent_Pin_Nbr; }

    public DbsField getPstl0495_Data_Decedent_Name() { return pstl0495_Data_Decedent_Name; }

    public DbsField getPstl0495_Data_Decedent_Date_Of_Birth() { return pstl0495_Data_Decedent_Date_Of_Birth; }

    public DbsField getPstl0495_Data_Decedent_Date_Of_Death() { return pstl0495_Data_Decedent_Date_Of_Death; }

    public DbsField getPstl0495_Data_Cont_Beneficiary_Name() { return pstl0495_Data_Cont_Beneficiary_Name; }

    public DbsField getPstl0495_Data_Minor_Org_Estate_Name() { return pstl0495_Data_Minor_Org_Estate_Name; }

    public DbsField getPstl0495_Data_Trust_Name() { return pstl0495_Data_Trust_Name; }

    public DbsField getPstl0495_Data_Children_Drop_Down() { return pstl0495_Data_Children_Drop_Down; }

    public DbsField getPstl0495_Data_Pages_Missing() { return pstl0495_Data_Pages_Missing; }

    public DbsField getPstl0495_Data_Death_Cert_Bene_Names() { return pstl0495_Data_Death_Cert_Bene_Names; }

    public DbsField getPstl0495_Data_Pvc_Delivery_Type() { return pstl0495_Data_Pvc_Delivery_Type; }

    public DbsField getPstl0495_Data_Survivor_Task_Category() { return pstl0495_Data_Survivor_Task_Category; }

    public DbsField getPstl0495_Data_Unclaimed_Funds_State_Name() { return pstl0495_Data_Unclaimed_Funds_State_Name; }

    public DbsField getPstl0495_Data_Unclaimed_Funds_State_Cde() { return pstl0495_Data_Unclaimed_Funds_State_Cde; }

    public DbsField getPstl0495_Data_Unclaimed_Funds_Contract_Nbr() { return pstl0495_Data_Unclaimed_Funds_Contract_Nbr; }

    public DbsField getPstl0495_Data_Brm_Name() { return pstl0495_Data_Brm_Name; }

    public DbsField getPstl0495_Data_Footer_Location() { return pstl0495_Data_Footer_Location; }

    public DbsGroup getPstl0495_Data_Survivor_Forms() { return pstl0495_Data_Survivor_Forms; }

    public DbsField getPstl0495_Data_Renun_Waiver_Release_Ind() { return pstl0495_Data_Renun_Waiver_Release_Ind; }

    public DbsField getPstl0495_Data_Marital_Stat_Addendum_Ind() { return pstl0495_Data_Marital_Stat_Addendum_Ind; }

    public DbsField getPstl0495_Data_Release_Indem_Agree_Ind() { return pstl0495_Data_Release_Indem_Agree_Ind; }

    public DbsGroup getPstl0495_Data_Beneficiary_Forms() { return pstl0495_Data_Beneficiary_Forms; }

    public DbsField getPstl0495_Data_Savings_Invest_Appl_Ind() { return pstl0495_Data_Savings_Invest_Appl_Ind; }

    public DbsField getPstl0495_Data_Survivor_Bene_Dist_Rqst_Ind() { return pstl0495_Data_Survivor_Bene_Dist_Rqst_Ind; }

    public DbsField getPstl0495_Data_Income_Annuity_Request_Form_Ind() { return pstl0495_Data_Income_Annuity_Request_Form_Ind; }

    public DbsField getPstl0495_Data_Immediate_Annuity_Form_Ind() { return pstl0495_Data_Immediate_Annuity_Form_Ind; }

    public DbsField getPstl0495_Data_One_Time_Pay_Request_Form_Ind() { return pstl0495_Data_One_Time_Pay_Request_Form_Ind; }

    public DbsGroup getPstl0495_Data_Change_Of_Beneficiary_Forms() { return pstl0495_Data_Change_Of_Beneficiary_Forms; }

    public DbsField getPstl0495_Data_Da_Tpa_Desig_Form_Ind() { return pstl0495_Data_Da_Tpa_Desig_Form_Ind; }

    public DbsField getPstl0495_Data_Da_Tpa_Desig_Form_Ctr() { return pstl0495_Data_Da_Tpa_Desig_Form_Ctr; }

    public DbsField getPstl0495_Data_Ia_Beneficiary_Form_Ind() { return pstl0495_Data_Ia_Beneficiary_Form_Ind; }

    public DbsField getPstl0495_Data_Ia_Beneficiary_Form_Ctr() { return pstl0495_Data_Ia_Beneficiary_Form_Ctr; }

    public DbsField getPstl0495_Data_Ohio_Arp_Bene_Form_Ind() { return pstl0495_Data_Ohio_Arp_Bene_Form_Ind; }

    public DbsField getPstl0495_Data_Ohio_Arp_Bene_Form_Ctr() { return pstl0495_Data_Ohio_Arp_Bene_Form_Ctr; }

    public DbsField getPstl0495_Data_Univ_Pittsburgh_Bene_Form_Ind() { return pstl0495_Data_Univ_Pittsburgh_Bene_Form_Ind; }

    public DbsField getPstl0495_Data_Univ_Pittsburgh_Bene_Form_Ctr() { return pstl0495_Data_Univ_Pittsburgh_Bene_Form_Ctr; }

    public DbsField getPstl0495_Data_Univ_Michigan_Bene_Form_Ind() { return pstl0495_Data_Univ_Michigan_Bene_Form_Ind; }

    public DbsField getPstl0495_Data_Univ_Michigan_Bene_Form_Ctr() { return pstl0495_Data_Univ_Michigan_Bene_Form_Ctr; }

    public DbsField getPstl0495_Data_Successor_Beneficiary_Form_Ind() { return pstl0495_Data_Successor_Beneficiary_Form_Ind; }

    public DbsField getPstl0495_Data_Successor_Beneficiary_Form_Ctr() { return pstl0495_Data_Successor_Beneficiary_Form_Ctr; }

    public DbsGroup getPstl0495_Data_Tax_Forms() { return pstl0495_Data_Tax_Forms; }

    public DbsField getPstl0495_Data_Ohio_Consent_Ind() { return pstl0495_Data_Ohio_Consent_Ind; }

    public DbsField getPstl0495_Data_Oklahoma_Consent_Ind() { return pstl0495_Data_Oklahoma_Consent_Ind; }

    public DbsField getPstl0495_Data_Indiana_Notice_Ind() { return pstl0495_Data_Indiana_Notice_Ind; }

    public DbsField getPstl0495_Data_Rhode_Island_Notice_Ind() { return pstl0495_Data_Rhode_Island_Notice_Ind; }

    public DbsField getPstl0495_Data_W9_Form_Ind() { return pstl0495_Data_W9_Form_Ind; }

    public DbsField getPstl0495_Data_W8ben_Form_Ind() { return pstl0495_Data_W8ben_Form_Ind; }

    public DbsField getPstl0495_Data_W7_Form_Ind() { return pstl0495_Data_W7_Form_Ind; }

    public DbsField getPstl0495_Data_State_Code_Taxint() { return pstl0495_Data_State_Code_Taxint; }

    public DbsField getPstl0495_Data_State_Name_Taxint() { return pstl0495_Data_State_Name_Taxint; }

    public DbsField getPstl0495_Data_State_Name_Text() { return pstl0495_Data_State_Name_Text; }

    public DbsGroup getPstl0495_Data_Pre_Printed_Booklets() { return pstl0495_Data_Pre_Printed_Booklets; }

    public DbsField getPstl0495_Data_Loved_One_Passes_Away_Ind() { return pstl0495_Data_Loved_One_Passes_Away_Ind; }

    public DbsField getPstl0495_Data_Sip_Booklet_Ind() { return pstl0495_Data_Sip_Booklet_Ind; }

    public DbsField getPstl0495_Data_Taking_Next_Step_Ind() { return pstl0495_Data_Taking_Next_Step_Ind; }

    public DbsField getPstl0495_Data_Qtrly_Performance_Ind() { return pstl0495_Data_Qtrly_Performance_Ind; }

    public DbsField getPstl0495_Data_Tiaa_Cref_Prospectus_Ind() { return pstl0495_Data_Tiaa_Cref_Prospectus_Ind; }

    public DbsField getPstl0495_Data_Cref_Prospectus_Ind() { return pstl0495_Data_Cref_Prospectus_Ind; }

    public DbsField getPstl0495_Data_Tiaa_Rea_Prospectus_Ind() { return pstl0495_Data_Tiaa_Rea_Prospectus_Ind; }

    public DbsField getPstl0495_Data_Lifecycle_Prospectus_Ind() { return pstl0495_Data_Lifecycle_Prospectus_Ind; }

    public DbsField getPstl0495_Data_Tiaa_Rate_Sheet_Ind() { return pstl0495_Data_Tiaa_Rate_Sheet_Ind; }

    public DbsGroup getPstl0495_Data_Booklets_Prospectuses_With_Pdf() { return pstl0495_Data_Booklets_Prospectuses_With_Pdf; }

    public DbsField getPstl0495_Data_Atra_Sip_Glance_Indvl_Ind() { return pstl0495_Data_Atra_Sip_Glance_Indvl_Ind; }

    public DbsField getPstl0495_Data_Atra_Ssum_Glance_Trust_Ind() { return pstl0495_Data_Atra_Ssum_Glance_Trust_Ind; }

    public DbsField getPstl0495_Data_Atra_Ssum_Glance_Indvl_Ind() { return pstl0495_Data_Atra_Ssum_Glance_Indvl_Ind; }

    public DbsField getPstl0495_Data_Atra_Ssum_Glance_Estate_Ind() { return pstl0495_Data_Atra_Ssum_Glance_Estate_Ind; }

    public DbsField getPstl0495_Data_Atra_Annty_Glance_Indvl_Ind() { return pstl0495_Data_Atra_Annty_Glance_Indvl_Ind; }

    public DbsField getPstl0495_Data_Atra_Fix_Prd_Ann_Glance_Ind() { return pstl0495_Data_Atra_Fix_Prd_Ann_Glance_Ind; }

    public DbsField getPstl0495_Data_Ssum_Glance_Nonspouse_Ind() { return pstl0495_Data_Ssum_Glance_Nonspouse_Ind; }

    public DbsField getPstl0495_Data_Ssum_Glance_Spouse_Ind() { return pstl0495_Data_Ssum_Glance_Spouse_Ind; }

    public DbsField getPstl0495_Data_Ssum_Glance_Trust_Ind() { return pstl0495_Data_Ssum_Glance_Trust_Ind; }

    public DbsField getPstl0495_Data_Ssum_Glance_Estate_Ind() { return pstl0495_Data_Ssum_Glance_Estate_Ind; }

    public DbsField getPstl0495_Data_Notice_402f_Ind() { return pstl0495_Data_Notice_402f_Ind; }

    public DbsField getPstl0495_Data_Custodial_Agreement_Ind() { return pstl0495_Data_Custodial_Agreement_Ind; }

    public DbsField getPstl0495_Data_Record_Of_Age_Ind() { return pstl0495_Data_Record_Of_Age_Ind; }

    public DbsField getPstl0495_Data_Payment_Dest_Form_Ind() { return pstl0495_Data_Payment_Dest_Form_Ind; }

    public DbsField getPstl0495_Data_Transfer_At_Settlement_Ind() { return pstl0495_Data_Transfer_At_Settlement_Ind; }

    public DbsGroup getPstl0495_Data_Step0_Blurbs() { return pstl0495_Data_Step0_Blurbs; }

    public DbsField getPstl0495_Data_My_Children_Ind() { return pstl0495_Data_My_Children_Ind; }

    public DbsField getPstl0495_Data_Minor_Guardian_Hip_Ind() { return pstl0495_Data_Minor_Guardian_Hip_Ind; }

    public DbsField getPstl0495_Data_Exspouse_Divorce_Decree_Ind() { return pstl0495_Data_Exspouse_Divorce_Decree_Ind; }

    public DbsField getPstl0495_Data_Estate_Ind() { return pstl0495_Data_Estate_Ind; }

    public DbsField getPstl0495_Data_Small_Estate_Ind() { return pstl0495_Data_Small_Estate_Ind; }

    public DbsField getPstl0495_Data_Unable_To_Locate_Trust_Ind() { return pstl0495_Data_Unable_To_Locate_Trust_Ind; }

    public DbsField getPstl0495_Data_Need_Bene_Address_Ind() { return pstl0495_Data_Need_Bene_Address_Ind; }

    public DbsField getPstl0495_Data_Organization_Ind() { return pstl0495_Data_Organization_Ind; }

    public DbsField getPstl0495_Data_Trust_Ind() { return pstl0495_Data_Trust_Ind; }

    public DbsField getPstl0495_Data_Msv_Ind() { return pstl0495_Data_Msv_Ind; }

    public DbsGroup getPstl0495_Data_Entitlement_Blurbs() { return pstl0495_Data_Entitlement_Blurbs; }

    public DbsField getPstl0495_Data_Roth_Accumulation_Ind() { return pstl0495_Data_Roth_Accumulation_Ind; }

    public DbsField getPstl0495_Data_Atra_Accumulation_Ind() { return pstl0495_Data_Atra_Accumulation_Ind; }

    public DbsField getPstl0495_Data_Investment_Contract_Ind() { return pstl0495_Data_Investment_Contract_Ind; }

    public DbsGroup getPstl0495_Data_Decline_Of_Change_Blurbs() { return pstl0495_Data_Decline_Of_Change_Blurbs; }

    public DbsField getPstl0495_Data_Allocation_Ind() { return pstl0495_Data_Allocation_Ind; }

    public DbsField getPstl0495_Data_Blank_Beneficiaries_Ind() { return pstl0495_Data_Blank_Beneficiaries_Ind; }

    public DbsField getPstl0495_Data_Contract_Numbers_Ind() { return pstl0495_Data_Contract_Numbers_Ind; }

    public DbsField getPstl0495_Data_Invalid_Date_Ind() { return pstl0495_Data_Invalid_Date_Ind; }

    public DbsField getPstl0495_Data_Estate_Trust_Ind() { return pstl0495_Data_Estate_Trust_Ind; }

    public DbsField getPstl0495_Data_Executor_Ind() { return pstl0495_Data_Executor_Ind; }

    public DbsField getPstl0495_Data_Fax_Copies_Ind() { return pstl0495_Data_Fax_Copies_Ind; }

    public DbsField getPstl0495_Data_Guarantee_Period_Expire_Ind() { return pstl0495_Data_Guarantee_Period_Expire_Ind; }

    public DbsField getPstl0495_Data_No_Guarantee_Period_Ind() { return pstl0495_Data_No_Guarantee_Period_Ind; }

    public DbsField getPstl0495_Data_Indvl_Bene_Trust_Ind() { return pstl0495_Data_Indvl_Bene_Trust_Ind; }

    public DbsField getPstl0495_Data_Institution_Maintained_Ind() { return pstl0495_Data_Institution_Maintained_Ind; }

    public DbsField getPstl0495_Data_Irrevocable_Designation_Ind() { return pstl0495_Data_Irrevocable_Designation_Ind; }

    public DbsField getPstl0495_Data_Missing_Pages_Ind() { return pstl0495_Data_Missing_Pages_Ind; }

    public DbsField getPstl0495_Data_Inactive_Contracts_Ind() { return pstl0495_Data_Inactive_Contracts_Ind; }

    public DbsField getPstl0495_Data_Or_Used_In_Designation_Ind() { return pstl0495_Data_Or_Used_In_Designation_Ind; }

    public DbsField getPstl0495_Data_Same_Primary_Contingent_Ind() { return pstl0495_Data_Same_Primary_Contingent_Ind; }

    public DbsField getPstl0495_Data_Siblings_Ind() { return pstl0495_Data_Siblings_Ind; }

    public DbsField getPstl0495_Data_No_Signature_Date_Ind() { return pstl0495_Data_No_Signature_Date_Ind; }

    public DbsField getPstl0495_Data_Spouse_As_Primary_Bene_Ind() { return pstl0495_Data_Spouse_As_Primary_Bene_Ind; }

    public DbsField getPstl0495_Data_Surs_Ind() { return pstl0495_Data_Surs_Ind; }

    public DbsGroup getPstl0495_Data_Accept_Blurbs() { return pstl0495_Data_Accept_Blurbs; }

    public DbsField getPstl0495_Data_Age_Stipulation_Ind() { return pstl0495_Data_Age_Stipulation_Ind; }

    public DbsField getPstl0495_Data_Deceased_Child_Chk_Gchild_Ind() { return pstl0495_Data_Deceased_Child_Chk_Gchild_Ind; }

    public DbsField getPstl0495_Data_Deceased_Child_Bxchk_Schild_Ind() { return pstl0495_Data_Deceased_Child_Bxchk_Schild_Ind; }

    public DbsField getPstl0495_Data_Difficult_To_Read_Ind() { return pstl0495_Data_Difficult_To_Read_Ind; }

    public DbsField getPstl0495_Data_Divesting_Ind() { return pstl0495_Data_Divesting_Ind; }

    public DbsField getPstl0495_Data_For_The_Benefit_Of_Ind() { return pstl0495_Data_For_The_Benefit_Of_Ind; }

    public DbsField getPstl0495_Data_Guardian_Ind() { return pstl0495_Data_Guardian_Ind; }

    public DbsField getPstl0495_Data_Mdo_Contract_Ind() { return pstl0495_Data_Mdo_Contract_Ind; }

    public DbsField getPstl0495_Data_Missing_Information_Ind() { return pstl0495_Data_Missing_Information_Ind; }

    public DbsField getPstl0495_Data_Department_Ind() { return pstl0495_Data_Department_Ind; }

    public DbsField getPstl0495_Data_No_Primary_Bene_Ind() { return pstl0495_Data_No_Primary_Bene_Ind; }

    public DbsField getPstl0495_Data_Second_Annuitant_Named_Ind() { return pstl0495_Data_Second_Annuitant_Named_Ind; }

    public DbsField getPstl0495_Data_Ssn_Not_Shown_Ind() { return pstl0495_Data_Ssn_Not_Shown_Ind; }

    public DbsField getPstl0495_Data_Spouse_Another_Person_Ind() { return pstl0495_Data_Spouse_Another_Person_Ind; }

    public DbsField getPstl0495_Data_Spousal_Waiver_Inc_Ind() { return pstl0495_Data_Spousal_Waiver_Inc_Ind; }

    public DbsField getPstl0495_Data_Survivorship_Clause() { return pstl0495_Data_Survivorship_Clause; }

    public DbsField getPstl0495_Data_Tin_Ind() { return pstl0495_Data_Tin_Ind; }

    public DbsField getPstl0495_Data_Terms_Of_Trust_Ind() { return pstl0495_Data_Terms_Of_Trust_Ind; }

    public DbsField getPstl0495_Data_Trust_Date_Ind() { return pstl0495_Data_Trust_Date_Ind; }

    public DbsField getPstl0495_Data_Unborn_Children_Ind() { return pstl0495_Data_Unborn_Children_Ind; }

    public DbsField getPstl0495_Data_Unnamed_Spouse_Of_Bene_Ind() { return pstl0495_Data_Unnamed_Spouse_Of_Bene_Ind; }

    public DbsGroup getPstl0495_Data_Forms_Change_Blurbs() { return pstl0495_Data_Forms_Change_Blurbs; }

    public DbsField getPstl0495_Data_No_Designation_Found_Ind() { return pstl0495_Data_No_Designation_Found_Ind; }

    public DbsField getPstl0495_Data_Poor_Copy_Ind() { return pstl0495_Data_Poor_Copy_Ind; }

    public DbsField getPstl0495_Data_Retirement_Loan_Ind() { return pstl0495_Data_Retirement_Loan_Ind; }

    public DbsField getPstl0495_Data_Wpahs_Ind() { return pstl0495_Data_Wpahs_Ind; }

    public DbsGroup getPstl0495_Data_Change_Of_Bene_Blurb() { return pstl0495_Data_Change_Of_Bene_Blurb; }

    public DbsField getPstl0495_Data_Erisa_Ind() { return pstl0495_Data_Erisa_Ind; }

    public DbsGroup getPstl0495_Data_Bene_Option_Blurbs() { return pstl0495_Data_Bene_Option_Blurbs; }

    public DbsField getPstl0495_Data_Death_Certificate_Reqrd_Ind() { return pstl0495_Data_Death_Certificate_Reqrd_Ind; }

    public DbsField getPstl0495_Data_Death_Cert_Primary_Bene_Ind() { return pstl0495_Data_Death_Cert_Primary_Bene_Ind; }

    public DbsField getPstl0495_Data_Statement_Will_Ind() { return pstl0495_Data_Statement_Will_Ind; }

    public DbsField getPstl0495_Data_Statement_Trust_In_Effect_Ind() { return pstl0495_Data_Statement_Trust_In_Effect_Ind; }

    public DbsField getPstl0495_Data_Statement_Trust_Accept_Ind() { return pstl0495_Data_Statement_Trust_Accept_Ind; }

    public DbsField getPstl0495_Data_Taxpayer_Id_Nbr_Ind() { return pstl0495_Data_Taxpayer_Id_Nbr_Ind; }

    public DbsField getPstl0495_Data_Letters_Of_Appointment_Ind() { return pstl0495_Data_Letters_Of_Appointment_Ind; }

    public DbsField getPstl0495_Data_Small_Accum_Ind() { return pstl0495_Data_Small_Accum_Ind; }

    public DbsField getPstl0495_Data_Signature_Guarantee_Ind() { return pstl0495_Data_Signature_Guarantee_Ind; }

    public DbsField getPstl0495_Data_Death_Certificate_Ind() { return pstl0495_Data_Death_Certificate_Ind; }

    public DbsField getPstl0495_Data_Reason_Code() { return pstl0495_Data_Reason_Code; }

    public DbsField getPstl0495_Data_Reason_Text() { return pstl0495_Data_Reason_Text; }

    public DbsField getPstl0495_Data_Non_Spouse_Rmd_Ind() { return pstl0495_Data_Non_Spouse_Rmd_Ind; }

    public DbsField getPstl0495_Data_Spouse_Rmd_Ind() { return pstl0495_Data_Spouse_Rmd_Ind; }

    public DbsField getPstl0495_Data_Doma_Ind() { return pstl0495_Data_Doma_Ind; }

    public DbsGroup getPstl0495_Data_Valuation_Details() { return pstl0495_Data_Valuation_Details; }

    public DbsField getPstl0495_Data_Valuation_Contract_Nbr() { return pstl0495_Data_Valuation_Contract_Nbr; }

    public DbsField getPstl0495_Data_Value_Of_Contract() { return pstl0495_Data_Value_Of_Contract; }

    public DbsField getPstl0495_Data_Total_At_Valuation_Date() { return pstl0495_Data_Total_At_Valuation_Date; }

    public DbsField getPstl0495_Data_Alternate_Valuation_Date() { return pstl0495_Data_Alternate_Valuation_Date; }

    public DbsField getPstl0495_Data_Investment_In_Contract() { return pstl0495_Data_Investment_In_Contract; }

    public DbsGroup getPstl0495_Data_Entitlement_Details() { return pstl0495_Data_Entitlement_Details; }

    public DbsField getPstl0495_Data_Account_Name() { return pstl0495_Data_Account_Name; }

    public DbsField getPstl0495_Data_Account_Amount() { return pstl0495_Data_Account_Amount; }

    public DbsField getPstl0495_Data_Entitlement_Total_Amount() { return pstl0495_Data_Entitlement_Total_Amount; }

    public DbsField getPstl0495_Data_Roth_Accumulation() { return pstl0495_Data_Roth_Accumulation; }

    public DbsField getPstl0495_Data_Atra_Accumulation() { return pstl0495_Data_Atra_Accumulation; }

    public DbsField getPstl0495_Data_Invest_And_Contract_Amount() { return pstl0495_Data_Invest_And_Contract_Amount; }

    public DbsGroup getPstl0495_Data_Incomplete_Bene_Change_Details() { return pstl0495_Data_Incomplete_Bene_Change_Details; }

    public DbsField getPstl0495_Data_Tiaa_Nbr() { return pstl0495_Data_Tiaa_Nbr; }

    public DbsField getPstl0495_Data_Cref_Nbr() { return pstl0495_Data_Cref_Nbr; }

    public DbsField getPstl0495_Data_Decedent_Other_Than_Orig() { return pstl0495_Data_Decedent_Other_Than_Orig; }

    public DbsGroup getPstl0495_Data_Mandatory_Tax_Form_Indicators() { return pstl0495_Data_Mandatory_Tax_Form_Indicators; }

    public DbsField getPstl0495_Data_Rollover_Ind() { return pstl0495_Data_Rollover_Ind; }

    public DbsField getPstl0495_Data_Periodic_Ind() { return pstl0495_Data_Periodic_Ind; }

    public DbsGroup getPstl0495_Data_New_Fields() { return pstl0495_Data_New_Fields; }

    public DbsField getPstl0495_Data_Wrong_Form_Ind() { return pstl0495_Data_Wrong_Form_Ind; }

    public DbsField getPstl0495_Data_Ap_Payment_Freq_Ind() { return pstl0495_Data_Ap_Payment_Freq_Ind; }

    public DbsField getPstl0495_Data_Payment_Coupon_Ind() { return pstl0495_Data_Payment_Coupon_Ind; }

    public DbsField getPstl0495_Data_Coupon_Tiaa_Amt() { return pstl0495_Data_Coupon_Tiaa_Amt; }

    public DbsField getPstl0495_Data_Coupon_Cref_Amt() { return pstl0495_Data_Coupon_Cref_Amt; }

    public DbsField getPstl0495_Data_Coupon_Total_Amt() { return pstl0495_Data_Coupon_Total_Amt; }

    public DbsField getPstl0495_Data_Bene_Prov_Date_Death() { return pstl0495_Data_Bene_Prov_Date_Death; }

    public DbsField getPstl0495_Data_Free_Text() { return pstl0495_Data_Free_Text; }

    public DbsField getPstl0495_Data_Benefit_Type_Ind() { return pstl0495_Data_Benefit_Type_Ind; }

    public DbsField getPstl0495_Data_Da_5k_Ind() { return pstl0495_Data_Da_5k_Ind; }

    public DbsField getPstl0495_Data_Sip_Tiaa_Nbr() { return pstl0495_Data_Sip_Tiaa_Nbr; }

    public DbsField getPstl0495_Data_Sip_Cref_Nbr() { return pstl0495_Data_Sip_Cref_Nbr; }

    public DbsField getPstl0495_Data_Ssum_Tiaa_Nbr() { return pstl0495_Data_Ssum_Tiaa_Nbr; }

    public DbsField getPstl0495_Data_Ssum_Cref_Nbr() { return pstl0495_Data_Ssum_Cref_Nbr; }

    public DbsGroup getPstl0495_Data_Bene_In_The_Plan_New_Fields() { return pstl0495_Data_Bene_In_The_Plan_New_Fields; }

    public DbsField getPstl0495_Data_Spouse_Rmd_Bene_Ind() { return pstl0495_Data_Spouse_Rmd_Bene_Ind; }

    public DbsField getPstl0495_Data_Entity_Rmd_Ind() { return pstl0495_Data_Entity_Rmd_Ind; }

    public DbsField getPstl0495_Data_Non_Spouse_Rmd_Bene_Ind() { return pstl0495_Data_Non_Spouse_Rmd_Bene_Ind; }

    public DbsField getPstl0495_Data_Second_Gen_Rmd_Ind() { return pstl0495_Data_Second_Gen_Rmd_Ind; }

    public DbsField getPstl0495_Data_Phone_Acceptance_Ind() { return pstl0495_Data_Phone_Acceptance_Ind; }

    public DbsField getPstl0495_Data_Register_Online_Acct_Ind() { return pstl0495_Data_Register_Online_Acct_Ind; }

    public DbsField getPstl0495_Data_Trust_Relation_Paragraph_Ind() { return pstl0495_Data_Trust_Relation_Paragraph_Ind; }

    public DbsField getPstl0495_Data_Brokerage_Retail_Ind() { return pstl0495_Data_Brokerage_Retail_Ind; }

    public DbsField getPstl0495_Data_Brokerage_Ira_Ind() { return pstl0495_Data_Brokerage_Ira_Ind; }

    public DbsField getPstl0495_Data_Brokerage_Ira_Roth_Ind() { return pstl0495_Data_Brokerage_Ira_Roth_Ind; }

    public DbsField getPstl0495_Data_Sole_Spouse_Ira_Rmd_Ind() { return pstl0495_Data_Sole_Spouse_Ira_Rmd_Ind; }

    public DbsField getPstl0495_Data_Sole_Spouse_Atra_Rmd_Ind() { return pstl0495_Data_Sole_Spouse_Atra_Rmd_Ind; }

    public DbsField getPstl0495_Data_Sole_Spouse_Both_Rmd_Ind() { return pstl0495_Data_Sole_Spouse_Both_Rmd_Ind; }

    public DbsField getPstl0495_Data_Accept_Form_Spouse_Ind() { return pstl0495_Data_Accept_Form_Spouse_Ind; }

    public DbsField getPstl0495_Data_Accept_Form_Bene_Ind() { return pstl0495_Data_Accept_Form_Bene_Ind; }

    public DbsField getPstl0495_Data_Accept_Form_Entity_Ind() { return pstl0495_Data_Accept_Form_Entity_Ind; }

    public DbsField getPstl0495_Data_Onetime_Transfer_Spouse_Ind() { return pstl0495_Data_Onetime_Transfer_Spouse_Ind; }

    public DbsField getPstl0495_Data_Onetime_Transfer_Bene_Ind() { return pstl0495_Data_Onetime_Transfer_Bene_Ind; }

    public DbsField getPstl0495_Data_Tpa_Cont_Spouse_Ind() { return pstl0495_Data_Tpa_Cont_Spouse_Ind; }

    public DbsField getPstl0495_Data_Tpa_Cont_Bene_Ind() { return pstl0495_Data_Tpa_Cont_Bene_Ind; }

    public DbsField getPstl0495_Data_Cash_Wdraw_Page_Spouse_Ind() { return pstl0495_Data_Cash_Wdraw_Page_Spouse_Ind; }

    public DbsField getPstl0495_Data_Cash_Wdraw_Page_Bene_Ind() { return pstl0495_Data_Cash_Wdraw_Page_Bene_Ind; }

    public DbsField getPstl0495_Data_Rollover_Page_Spouse_Ind() { return pstl0495_Data_Rollover_Page_Spouse_Ind; }

    public DbsField getPstl0495_Data_Rollover_Page_Bene_Ind() { return pstl0495_Data_Rollover_Page_Bene_Ind; }

    public DbsField getPstl0495_Data_Customer_Agree_Retail_Ind() { return pstl0495_Data_Customer_Agree_Retail_Ind; }

    public DbsField getPstl0495_Data_Affidavit_Domicile_Ind() { return pstl0495_Data_Affidavit_Domicile_Ind; }

    public DbsField getPstl0495_Data_Cert_Invest_Powers_Ind() { return pstl0495_Data_Cert_Invest_Powers_Ind; }

    public DbsField getPstl0495_Data_Power_Of_Atty_Ind() { return pstl0495_Data_Power_Of_Atty_Ind; }

    public DbsField getPstl0495_Data_Loi_Existing_Ind() { return pstl0495_Data_Loi_Existing_Ind; }

    public DbsField getPstl0495_Data_Loi_Trust_Ind() { return pstl0495_Data_Loi_Trust_Ind; }

    public DbsField getPstl0495_Data_Loi_New_Ind() { return pstl0495_Data_Loi_New_Ind; }

    public DbsField getPstl0495_Data_Loi_Jtwros_Ind() { return pstl0495_Data_Loi_Jtwros_Ind; }

    public DbsField getPstl0495_Data_Onetime_Transfer_Amt() { return pstl0495_Data_Onetime_Transfer_Amt; }

    public DbsField getPstl0495_Data_Second_Generation_Amt() { return pstl0495_Data_Second_Generation_Amt; }

    public DbsField getPstl0495_Data_Brokerage_Retail_Amt() { return pstl0495_Data_Brokerage_Retail_Amt; }

    public DbsField getPstl0495_Data_Brokerage_Ira_Amt() { return pstl0495_Data_Brokerage_Ira_Amt; }

    public DbsField getPstl0495_Data_Brokerage_Ira_Roth_Amt() { return pstl0495_Data_Brokerage_Ira_Roth_Amt; }

    public DbsField getPstl0495_Data_Brokerage_Retail_Acct_Nbr() { return pstl0495_Data_Brokerage_Retail_Acct_Nbr; }

    public DbsField getPstl0495_Data_Brokerage_Ira_Acct_Nbr() { return pstl0495_Data_Brokerage_Ira_Acct_Nbr; }

    public DbsField getPstl0495_Data_Brokerage_Roth_Acct_Nbr() { return pstl0495_Data_Brokerage_Roth_Acct_Nbr; }

    public DbsField getPstl0495_Data_Ro_Account_Nbr() { return pstl0495_Data_Ro_Account_Nbr; }

    public DbsField getPstl0495_Data_Loi_Ira_Ind() { return pstl0495_Data_Loi_Ira_Ind; }

    public DbsField getPstl0495_Data_Loi_Estate_Ind() { return pstl0495_Data_Loi_Estate_Ind; }

    public DbsField getPstl0495_Data_Tpa_Page() { return pstl0495_Data_Tpa_Page; }

    public DbsGroup getPstl0495_Data_Pstl0495_Data_ArrayRedef2() { return pstl0495_Data_Pstl0495_Data_ArrayRedef2; }

    public DbsGroup getPstl0495_Data_Pre_Fill_Data() { return pstl0495_Data_Pre_Fill_Data; }

    public DbsGroup getPstl0495_Data_Beneficiary_Information() { return pstl0495_Data_Beneficiary_Information; }

    public DbsField getPstl0495_Data_Bi_Bene_Type() { return pstl0495_Data_Bi_Bene_Type; }

    public DbsField getPstl0495_Data_Bi_Bene_Name() { return pstl0495_Data_Bi_Bene_Name; }

    public DbsField getPstl0495_Data_Bi_Bene_Trustee_Name() { return pstl0495_Data_Bi_Bene_Trustee_Name; }

    public DbsField getPstl0495_Data_Bi_Street() { return pstl0495_Data_Bi_Street; }

    public DbsField getPstl0495_Data_Bi_City() { return pstl0495_Data_Bi_City; }

    public DbsField getPstl0495_Data_Bi_State() { return pstl0495_Data_Bi_State; }

    public DbsField getPstl0495_Data_Bi_Zip() { return pstl0495_Data_Bi_Zip; }

    public DbsField getPstl0495_Data_Bi_Primary_Phone() { return pstl0495_Data_Bi_Primary_Phone; }

    public DbsField getPstl0495_Data_Bi_Alt_Phone() { return pstl0495_Data_Bi_Alt_Phone; }

    public DbsField getPstl0495_Data_Bi_Email_Address() { return pstl0495_Data_Bi_Email_Address; }

    public DbsField getPstl0495_Data_Bi_State_Legal_Residency() { return pstl0495_Data_Bi_State_Legal_Residency; }

    public DbsField getPstl0495_Data_Bi_Citizenship() { return pstl0495_Data_Bi_Citizenship; }

    public DbsField getPstl0495_Data_Bi_Dob_Trust_Amend_Date() { return pstl0495_Data_Bi_Dob_Trust_Amend_Date; }

    public DbsField getPstl0495_Data_Bi_Gender() { return pstl0495_Data_Bi_Gender; }

    public DbsField getPstl0495_Data_Bi_Bene_Relation_Decedent() { return pstl0495_Data_Bi_Bene_Relation_Decedent; }

    public DbsGroup getPstl0495_Data_Calc_Bene_For_Trust_Only() { return pstl0495_Data_Calc_Bene_For_Trust_Only; }

    public DbsField getPstl0495_Data_Ct_Trustee_Name() { return pstl0495_Data_Ct_Trustee_Name; }

    public DbsField getPstl0495_Data_Ct_Trustee_Dob() { return pstl0495_Data_Ct_Trustee_Dob; }

    public DbsField getPstl0495_Data_Ct_Trustee_Relation_Decedent() { return pstl0495_Data_Ct_Trustee_Relation_Decedent; }

    public DbsGroup getPstl0495_Data_Settlement_Option() { return pstl0495_Data_Settlement_Option; }

    public DbsField getPstl0495_Data_So_Option_Type() { return pstl0495_Data_So_Option_Type; }

    public DbsField getPstl0495_Data_So_Partial_Cash_Amt() { return pstl0495_Data_So_Partial_Cash_Amt; }

    public DbsField getPstl0495_Data_So_Partial_Cash_Pct() { return pstl0495_Data_So_Partial_Cash_Pct; }

    public DbsGroup getPstl0495_Data_Settlement_Option_Spouses() { return pstl0495_Data_Settlement_Option_Spouses; }

    public DbsField getPstl0495_Data_Ss_Option_Type() { return pstl0495_Data_Ss_Option_Type; }

    public DbsField getPstl0495_Data_Ss_Partial_Cash_Amt() { return pstl0495_Data_Ss_Partial_Cash_Amt; }

    public DbsField getPstl0495_Data_Ss_Partial_Cash_Pct() { return pstl0495_Data_Ss_Partial_Cash_Pct; }

    public DbsField getPstl0495_Data_Ss_Rollover_Acct_Status() { return pstl0495_Data_Ss_Rollover_Acct_Status; }

    public DbsField getPstl0495_Data_Ss_Rollover_Acct_Type() { return pstl0495_Data_Ss_Rollover_Acct_Type; }

    public DbsField getPstl0495_Data_Ss_Rollover_Acct_Nbr() { return pstl0495_Data_Ss_Rollover_Acct_Nbr; }

    public DbsField getPstl0495_Data_Ss_Tiaacref_Acct_Status() { return pstl0495_Data_Ss_Tiaacref_Acct_Status; }

    public DbsField getPstl0495_Data_Ss_Tiaacref_Acct_Type() { return pstl0495_Data_Ss_Tiaacref_Acct_Type; }

    public DbsField getPstl0495_Data_Ss_Tiaacref_Acct_Nbr() { return pstl0495_Data_Ss_Tiaacref_Acct_Nbr; }

    public DbsGroup getPstl0495_Data_Settlement_Option_Non_Spouses() { return pstl0495_Data_Settlement_Option_Non_Spouses; }

    public DbsField getPstl0495_Data_Sn_Option_Type() { return pstl0495_Data_Sn_Option_Type; }

    public DbsField getPstl0495_Data_Sn_Partial_Cash_Amt() { return pstl0495_Data_Sn_Partial_Cash_Amt; }

    public DbsField getPstl0495_Data_Sn_Partial_Cash_Pct() { return pstl0495_Data_Sn_Partial_Cash_Pct; }

    public DbsField getPstl0495_Data_Sn_Rollover_Acct_Status() { return pstl0495_Data_Sn_Rollover_Acct_Status; }

    public DbsField getPstl0495_Data_Sn_Rollover_Acct_Type() { return pstl0495_Data_Sn_Rollover_Acct_Type; }

    public DbsField getPstl0495_Data_Sn_Rollover_Acct_Nbr() { return pstl0495_Data_Sn_Rollover_Acct_Nbr; }

    public DbsField getPstl0495_Data_Sn_Tiaacref_Acct_Status() { return pstl0495_Data_Sn_Tiaacref_Acct_Status; }

    public DbsField getPstl0495_Data_Sn_Tiaacref_Acct_Type() { return pstl0495_Data_Sn_Tiaacref_Acct_Type; }

    public DbsField getPstl0495_Data_Sn_Tiaacref_Acct_Nbr() { return pstl0495_Data_Sn_Tiaacref_Acct_Nbr; }

    public DbsField getPstl0495_Data_Sn_Md_Olest_Bene_Dob() { return pstl0495_Data_Sn_Md_Olest_Bene_Dob; }

    public DbsGroup getPstl0495_Data_Choose_Frequency() { return pstl0495_Data_Choose_Frequency; }

    public DbsField getPstl0495_Data_Cf_Mmdd() { return pstl0495_Data_Cf_Mmdd; }

    public DbsField getPstl0495_Data_Cf_Freq() { return pstl0495_Data_Cf_Freq; }

    public DbsGroup getPstl0495_Data_Type_Of_Rollover() { return pstl0495_Data_Type_Of_Rollover; }

    public DbsField getPstl0495_Data_Tr_Rollover_Type() { return pstl0495_Data_Tr_Rollover_Type; }

    public DbsField getPstl0495_Data_Tr_O1_Trad_Ira_Nbr() { return pstl0495_Data_Tr_O1_Trad_Ira_Nbr; }

    public DbsField getPstl0495_Data_Tr_O3_Company_Name() { return pstl0495_Data_Tr_O3_Company_Name; }

    public DbsField getPstl0495_Data_Tr_O3_Company_Address() { return pstl0495_Data_Tr_O3_Company_Address; }

    public DbsField getPstl0495_Data_Tr_O3_Company_City() { return pstl0495_Data_Tr_O3_Company_City; }

    public DbsField getPstl0495_Data_Tr_O3_Company_State() { return pstl0495_Data_Tr_O3_Company_State; }

    public DbsField getPstl0495_Data_Tr_O3_Company_Zip() { return pstl0495_Data_Tr_O3_Company_Zip; }

    public DbsField getPstl0495_Data_Tr_O3_Company_Phone() { return pstl0495_Data_Tr_O3_Company_Phone; }

    public DbsField getPstl0495_Data_Tr_O3_Company_Acct_Nbr() { return pstl0495_Data_Tr_O3_Company_Acct_Nbr; }

    public DbsGroup getPstl0495_Data_Rollover_To_Another_Invco() { return pstl0495_Data_Rollover_To_Another_Invco; }

    public DbsField getPstl0495_Data_Ri_Option_Cde() { return pstl0495_Data_Ri_Option_Cde; }

    public DbsField getPstl0495_Data_Ri_Invest_Company_Name() { return pstl0495_Data_Ri_Invest_Company_Name; }

    public DbsField getPstl0495_Data_Ri_Invest_Company_Address() { return pstl0495_Data_Ri_Invest_Company_Address; }

    public DbsField getPstl0495_Data_Ri_Invest_Company_City() { return pstl0495_Data_Ri_Invest_Company_City; }

    public DbsField getPstl0495_Data_Ri_Invest_Company_State() { return pstl0495_Data_Ri_Invest_Company_State; }

    public DbsField getPstl0495_Data_Ri_Invest_Company_Zip() { return pstl0495_Data_Ri_Invest_Company_Zip; }

    public DbsField getPstl0495_Data_Ri_Invest_Company_Phone() { return pstl0495_Data_Ri_Invest_Company_Phone; }

    public DbsField getPstl0495_Data_Ri_Invest_Company_Acct_Nbr() { return pstl0495_Data_Ri_Invest_Company_Acct_Nbr; }

    public DbsGroup getPstl0495_Data_Roth_Only() { return pstl0495_Data_Roth_Only; }

    public DbsField getPstl0495_Data_Ro_Roth_Ind() { return pstl0495_Data_Ro_Roth_Ind; }

    public DbsField getPstl0495_Data_Ro_Roth_Amt() { return pstl0495_Data_Ro_Roth_Amt; }

    public DbsField getPstl0495_Data_Ro_Roth_Pct() { return pstl0495_Data_Ro_Roth_Pct; }

    public DbsGroup getPstl0495_Data_Federal_Tax_Wholding_Amount() { return pstl0495_Data_Federal_Tax_Wholding_Amount; }

    public DbsField getPstl0495_Data_Ft_Whold_Type() { return pstl0495_Data_Ft_Whold_Type; }

    public DbsField getPstl0495_Data_Ft_Da_Dollar_Amt() { return pstl0495_Data_Ft_Da_Dollar_Amt; }

    public DbsField getPstl0495_Data_Ft_Pc_Percent() { return pstl0495_Data_Ft_Pc_Percent; }

    public DbsField getPstl0495_Data_Ft_Re_Dollar_Amt() { return pstl0495_Data_Ft_Re_Dollar_Amt; }

    public DbsField getPstl0495_Data_Ft_Re_Percent() { return pstl0495_Data_Ft_Re_Percent; }

    public DbsField getPstl0495_Data_Ft_N2_Dollar_Amt() { return pstl0495_Data_Ft_N2_Dollar_Amt; }

    public DbsField getPstl0495_Data_Ft_N2_Percent() { return pstl0495_Data_Ft_N2_Percent; }

    public DbsField getPstl0495_Data_Ft_N3_Status() { return pstl0495_Data_Ft_N3_Status; }

    public DbsField getPstl0495_Data_Ft_N3_Nbr_Whold_Allow() { return pstl0495_Data_Ft_N3_Nbr_Whold_Allow; }

    public DbsField getPstl0495_Data_Ft_Op_Dollar_Amt() { return pstl0495_Data_Ft_Op_Dollar_Amt; }

    public DbsField getPstl0495_Data_Ft_Cv_Dollar_Amt() { return pstl0495_Data_Ft_Cv_Dollar_Amt; }

    public DbsField getPstl0495_Data_Ft_Cv_Percent() { return pstl0495_Data_Ft_Cv_Percent; }

    public DbsGroup getPstl0495_Data_Beneficiary_List() { return pstl0495_Data_Beneficiary_List; }

    public DbsGroup getPstl0495_Data_Bl_Primary_List() { return pstl0495_Data_Bl_Primary_List; }

    public DbsField getPstl0495_Data_Bl_Primary_Name() { return pstl0495_Data_Bl_Primary_Name; }

    public DbsField getPstl0495_Data_Bl_Primary_Pct() { return pstl0495_Data_Bl_Primary_Pct; }

    public DbsField getPstl0495_Data_Bl_Primary_Dob() { return pstl0495_Data_Bl_Primary_Dob; }

    public DbsField getPstl0495_Data_Bl_Primary_Relation() { return pstl0495_Data_Bl_Primary_Relation; }

    public DbsGroup getPstl0495_Data_Bl_Contingent_List() { return pstl0495_Data_Bl_Contingent_List; }

    public DbsField getPstl0495_Data_Bl_Contingent_Name() { return pstl0495_Data_Bl_Contingent_Name; }

    public DbsField getPstl0495_Data_Bl_Contingent_Pct() { return pstl0495_Data_Bl_Contingent_Pct; }

    public DbsField getPstl0495_Data_Bl_Contingent_Dob() { return pstl0495_Data_Bl_Contingent_Dob; }

    public DbsField getPstl0495_Data_Bl_Contingent_Relation() { return pstl0495_Data_Bl_Contingent_Relation; }

    public DbsGroup getPstl0495_Data_Delivery_Instructions() { return pstl0495_Data_Delivery_Instructions; }

    public DbsField getPstl0495_Data_Di_Option_Cde() { return pstl0495_Data_Di_Option_Cde; }

    public DbsField getPstl0495_Data_Di_O2_Acct_Type() { return pstl0495_Data_Di_O2_Acct_Type; }

    public DbsField getPstl0495_Data_Di_O2_Amt() { return pstl0495_Data_Di_O2_Amt; }

    public DbsField getPstl0495_Data_Di_O2_Invest_Bank_Name() { return pstl0495_Data_Di_O2_Invest_Bank_Name; }

    public DbsField getPstl0495_Data_Di_O2_Address() { return pstl0495_Data_Di_O2_Address; }

    public DbsField getPstl0495_Data_Di_O2_City() { return pstl0495_Data_Di_O2_City; }

    public DbsField getPstl0495_Data_Di_O2_State() { return pstl0495_Data_Di_O2_State; }

    public DbsField getPstl0495_Data_Di_O2_Zip() { return pstl0495_Data_Di_O2_Zip; }

    public DbsField getPstl0495_Data_Di_O2_Bank_Acct_Phone() { return pstl0495_Data_Di_O2_Bank_Acct_Phone; }

    public DbsField getPstl0495_Data_Di_O2_Bank_Acct_Nbr() { return pstl0495_Data_Di_O2_Bank_Acct_Nbr; }

    public DbsField getPstl0495_Data_Di_O2_Bank_Routing_Nbr() { return pstl0495_Data_Di_O2_Bank_Routing_Nbr; }

    public DbsField getPstl0495_Data_Di_O3_Address_Type() { return pstl0495_Data_Di_O3_Address_Type; }

    public DbsField getPstl0495_Data_Di_O3_Address() { return pstl0495_Data_Di_O3_Address; }

    public DbsField getPstl0495_Data_Di_O3_City() { return pstl0495_Data_Di_O3_City; }

    public DbsField getPstl0495_Data_Di_O3_State() { return pstl0495_Data_Di_O3_State; }

    public DbsField getPstl0495_Data_Di_O3_Zip() { return pstl0495_Data_Di_O3_Zip; }

    public DbsField getPstl0495_Data_Di_O4_Roth_Ira_Acct() { return pstl0495_Data_Di_O4_Roth_Ira_Acct; }

    public DbsGroup getPstl0495_Data_Annuity_Method() { return pstl0495_Data_Annuity_Method; }

    public DbsField getPstl0495_Data_Am_Option_Cde() { return pstl0495_Data_Am_Option_Cde; }

    public DbsField getPstl0495_Data_Am_O1_Payment_Years() { return pstl0495_Data_Am_O1_Payment_Years; }

    public DbsField getPstl0495_Data_Am_O1_Payment_Freq() { return pstl0495_Data_Am_O1_Payment_Freq; }

    public DbsField getPstl0495_Data_Am_O3_Guaranteed_Prd() { return pstl0495_Data_Am_O3_Guaranteed_Prd; }

    public DbsField getPstl0495_Data_Am_O3_Payment_Freq() { return pstl0495_Data_Am_O3_Payment_Freq; }

    public DbsGroup getPstl0495_Data_Payment_Options() { return pstl0495_Data_Payment_Options; }

    public DbsField getPstl0495_Data_Po_Option_Cde() { return pstl0495_Data_Po_Option_Cde; }

    public DbsField getPstl0495_Data_Po_Tiaa_Traditional_Ind() { return pstl0495_Data_Po_Tiaa_Traditional_Ind; }

    public DbsField getPstl0495_Data_Po_Cref_Bond_Market_Ind() { return pstl0495_Data_Po_Cref_Bond_Market_Ind; }

    public DbsField getPstl0495_Data_Po_Tiaa_Real_Estate_Ind() { return pstl0495_Data_Po_Tiaa_Real_Estate_Ind; }

    public DbsField getPstl0495_Data_Po_Cref_Glbl_Equity_Ind() { return pstl0495_Data_Po_Cref_Glbl_Equity_Ind; }

    public DbsField getPstl0495_Data_Po_Cref_Stock_Ind() { return pstl0495_Data_Po_Cref_Stock_Ind; }

    public DbsField getPstl0495_Data_Po_Cref_Growth_Ind() { return pstl0495_Data_Po_Cref_Growth_Ind; }

    public DbsField getPstl0495_Data_Po_Cref_Money_Mrkt_Ind() { return pstl0495_Data_Po_Cref_Money_Mrkt_Ind; }

    public DbsField getPstl0495_Data_Po_Cref_Equity_Ndx_Ind() { return pstl0495_Data_Po_Cref_Equity_Ndx_Ind; }

    public DbsField getPstl0495_Data_Po_Cref_Social_Chc_Ind() { return pstl0495_Data_Po_Cref_Social_Chc_Ind; }

    public DbsField getPstl0495_Data_Po_Cref_Inf_Linked_Ind() { return pstl0495_Data_Po_Cref_Inf_Linked_Ind; }

    public DbsGroup getPstl0495_Data_Payment_Method() { return pstl0495_Data_Payment_Method; }

    public DbsField getPstl0495_Data_Pm_Option_Cde() { return pstl0495_Data_Pm_Option_Cde; }

    public DbsField getPstl0495_Data_Pm_O1_This_Amt() { return pstl0495_Data_Pm_O1_This_Amt; }

    public DbsField getPstl0495_Data_Pm_O1_Graded_Prd_Amt() { return pstl0495_Data_Pm_O1_Graded_Prd_Amt; }

    public DbsField getPstl0495_Data_Pm_O4_Freq() { return pstl0495_Data_Pm_O4_Freq; }

    public DbsField getPstl0495_Data_Pm_O4_Start_Life_Income_Dte() { return pstl0495_Data_Pm_O4_Start_Life_Income_Dte; }

    public DbsField getPstl0495_Data_Pm_O4_Payment_Freq() { return pstl0495_Data_Pm_O4_Payment_Freq; }

    public DbsGroup getPstl0495_Data_Renunciation_Release_1time_Pay() { return pstl0495_Data_Renunciation_Release_1time_Pay; }

    public DbsField getPstl0495_Data_Rw_Approx_Retire_Benefits() { return pstl0495_Data_Rw_Approx_Retire_Benefits; }

    public DbsField getPstl0495_Data_Rw_Asof_Date() { return pstl0495_Data_Rw_Asof_Date; }

    public DbsGroup getPstl0495_Data_Mdo_Only() { return pstl0495_Data_Mdo_Only; }

    public DbsField getPstl0495_Data_Md_Withhold_Ind() { return pstl0495_Data_Md_Withhold_Ind; }

    public DbsField getPstl0495_Data_Md_Withhold_Amt() { return pstl0495_Data_Md_Withhold_Amt; }

    public DbsGroup getPstl0495_Data_Di_Additional() { return pstl0495_Data_Di_Additional; }

    public DbsField getPstl0495_Data_Di_O4_Tiaa_Acct_Type() { return pstl0495_Data_Di_O4_Tiaa_Acct_Type; }

    public DbsField getPstl0495_Data_Di_O4_Careof() { return pstl0495_Data_Di_O4_Careof; }

    public DbsGroup getPstl0495_Data_Pstl0495_Data_ArrayRedef3() { return pstl0495_Data_Pstl0495_Data_ArrayRedef3; }

    public DbsGroup getPstl0495_Data_Power_Image_Task_Information() { return pstl0495_Data_Power_Image_Task_Information; }

    public DbsField getPstl0495_Data_Pi_Begin_Literal() { return pstl0495_Data_Pi_Begin_Literal; }

    public DbsField getPstl0495_Data_Pi_Export_Ind() { return pstl0495_Data_Pi_Export_Ind; }

    public DbsField getPstl0495_Data_Pi_Task_Id() { return pstl0495_Data_Pi_Task_Id; }

    public DbsField getPstl0495_Data_Pi_Task_Type() { return pstl0495_Data_Pi_Task_Type; }

    public DbsField getPstl0495_Data_Pi_Task_Guid() { return pstl0495_Data_Pi_Task_Guid; }

    public DbsField getPstl0495_Data_Pi_Action_Step() { return pstl0495_Data_Pi_Action_Step; }

    public DbsField getPstl0495_Data_Pi_Tiaa_Full_Date() { return pstl0495_Data_Pi_Tiaa_Full_Date; }

    public DbsGroup getPstl0495_Data_Pi_Tiaa_Full_DateRedef4() { return pstl0495_Data_Pi_Tiaa_Full_DateRedef4; }

    public DbsField getPstl0495_Data_Pi_Tiaa_Date_Century() { return pstl0495_Data_Pi_Tiaa_Date_Century; }

    public DbsField getPstl0495_Data_Pi_Tiaa_Date_Year() { return pstl0495_Data_Pi_Tiaa_Date_Year; }

    public DbsField getPstl0495_Data_Pi_Tiaa_Date_Month() { return pstl0495_Data_Pi_Tiaa_Date_Month; }

    public DbsField getPstl0495_Data_Pi_Tiaa_Date_Day() { return pstl0495_Data_Pi_Tiaa_Date_Day; }

    public DbsField getPstl0495_Data_Pi_Tiaa_Time() { return pstl0495_Data_Pi_Tiaa_Time; }

    public DbsField getPstl0495_Data_Pi_Task_Status() { return pstl0495_Data_Pi_Task_Status; }

    public DbsField getPstl0495_Data_Pi_Ssn() { return pstl0495_Data_Pi_Ssn; }

    public DbsField getPstl0495_Data_Pi_Pin_Npin_Ppg() { return pstl0495_Data_Pi_Pin_Npin_Ppg; }

    public DbsField getPstl0495_Data_Pi_Pin_Type() { return pstl0495_Data_Pi_Pin_Type; }

    public DbsField getPstl0495_Data_Pi_Contract() { return pstl0495_Data_Pi_Contract; }

    public DbsGroup getPstl0495_Data_Pi_ContractRedef5() { return pstl0495_Data_Pi_ContractRedef5; }

    public DbsField getPstl0495_Data_Pi_Contract_A() { return pstl0495_Data_Pi_Contract_A; }

    public DbsField getPstl0495_Data_Pi_Plan_Id() { return pstl0495_Data_Pi_Plan_Id; }

    public DbsField getPstl0495_Data_Pi_Doc_Content() { return pstl0495_Data_Pi_Doc_Content; }

    public DbsField getPstl0495_Data_Pi_Filler_1() { return pstl0495_Data_Pi_Filler_1; }

    public DbsField getPstl0495_Data_Pi_Filler_2() { return pstl0495_Data_Pi_Filler_2; }

    public DbsField getPstl0495_Data_Pi_Filler_3() { return pstl0495_Data_Pi_Filler_3; }

    public DbsField getPstl0495_Data_Pi_Filler_4() { return pstl0495_Data_Pi_Filler_4; }

    public DbsField getPstl0495_Data_Pi_Filler_5() { return pstl0495_Data_Pi_Filler_5; }

    public DbsField getPstl0495_Data_Pi_End_Literal() { return pstl0495_Data_Pi_End_Literal; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        sort_Key = newGroupInRecord("sort_Key", "SORT-KEY");
        sort_Key_Sort_Key_Lgth = sort_Key.newFieldInGroup("sort_Key_Sort_Key_Lgth", "SORT-KEY-LGTH", FieldType.NUMERIC, 3);
        sort_Key_Sort_Key_Array = sort_Key.newFieldArrayInGroup("sort_Key_Sort_Key_Array", "SORT-KEY-ARRAY", FieldType.STRING, 1, new DbsArrayController(1,
            1));

        pstl0495_Data = newGroupInRecord("pstl0495_Data", "PSTL0495-DATA");
        pstl0495_Data_Pstl0495_Rec_Id = pstl0495_Data.newFieldInGroup("pstl0495_Data_Pstl0495_Rec_Id", "PSTL0495-REC-ID", FieldType.STRING, 2);
        pstl0495_Data_Pstl0495_Data_Lgth = pstl0495_Data.newFieldInGroup("pstl0495_Data_Pstl0495_Data_Lgth", "PSTL0495-DATA-LGTH", FieldType.NUMERIC, 
            5);
        pstl0495_Data_Pstl0495_Data_Occurs = pstl0495_Data.newFieldInGroup("pstl0495_Data_Pstl0495_Data_Occurs", "PSTL0495-DATA-OCCURS", FieldType.NUMERIC, 
            5);
        pstl0495_Data_Pstl0495_Data_Array = pstl0495_Data.newFieldArrayInGroup("pstl0495_Data_Pstl0495_Data_Array", "PSTL0495-DATA-ARRAY", FieldType.STRING, 
            1, new DbsArrayController(1,4499));
        pstl0495_Data_Pstl0495_Data_ArrayRedef1 = pstl0495_Data.newGroupInGroup("pstl0495_Data_Pstl0495_Data_ArrayRedef1", "Redefines", pstl0495_Data_Pstl0495_Data_Array);
        pstl0495_Data_Bene_Common_Data = pstl0495_Data_Pstl0495_Data_ArrayRedef1.newGroupInGroup("pstl0495_Data_Bene_Common_Data", "BENE-COMMON-DATA");
        pstl0495_Data_Letter_Type = pstl0495_Data_Bene_Common_Data.newFieldInGroup("pstl0495_Data_Letter_Type", "LETTER-TYPE", FieldType.STRING, 2);
        pstl0495_Data_Case_Manager_Name = pstl0495_Data_Bene_Common_Data.newFieldInGroup("pstl0495_Data_Case_Manager_Name", "CASE-MANAGER-NAME", FieldType.STRING, 
            40);
        pstl0495_Data_Case_Manager_Phone = pstl0495_Data_Bene_Common_Data.newFieldInGroup("pstl0495_Data_Case_Manager_Phone", "CASE-MANAGER-PHONE", FieldType.STRING, 
            6);
        pstl0495_Data_Institution_Name = pstl0495_Data_Bene_Common_Data.newFieldInGroup("pstl0495_Data_Institution_Name", "INSTITUTION-NAME", FieldType.STRING, 
            60);
        pstl0495_Data_Decedent_Pin_Nbr = pstl0495_Data_Bene_Common_Data.newFieldInGroup("pstl0495_Data_Decedent_Pin_Nbr", "DECEDENT-PIN-NBR", FieldType.STRING, 
            12);
        pstl0495_Data_Decedent_Name = pstl0495_Data_Bene_Common_Data.newFieldInGroup("pstl0495_Data_Decedent_Name", "DECEDENT-NAME", FieldType.STRING, 
            40);
        pstl0495_Data_Decedent_Date_Of_Birth = pstl0495_Data_Bene_Common_Data.newFieldInGroup("pstl0495_Data_Decedent_Date_Of_Birth", "DECEDENT-DATE-OF-BIRTH", 
            FieldType.NUMERIC, 8);
        pstl0495_Data_Decedent_Date_Of_Death = pstl0495_Data_Bene_Common_Data.newFieldInGroup("pstl0495_Data_Decedent_Date_Of_Death", "DECEDENT-DATE-OF-DEATH", 
            FieldType.NUMERIC, 8);
        pstl0495_Data_Cont_Beneficiary_Name = pstl0495_Data_Bene_Common_Data.newFieldInGroup("pstl0495_Data_Cont_Beneficiary_Name", "CONT-BENEFICIARY-NAME", 
            FieldType.STRING, 100);
        pstl0495_Data_Minor_Org_Estate_Name = pstl0495_Data_Bene_Common_Data.newFieldInGroup("pstl0495_Data_Minor_Org_Estate_Name", "MINOR-ORG-ESTATE-NAME", 
            FieldType.STRING, 60);
        pstl0495_Data_Trust_Name = pstl0495_Data_Bene_Common_Data.newFieldInGroup("pstl0495_Data_Trust_Name", "TRUST-NAME", FieldType.STRING, 60);
        pstl0495_Data_Children_Drop_Down = pstl0495_Data_Bene_Common_Data.newFieldInGroup("pstl0495_Data_Children_Drop_Down", "CHILDREN-DROP-DOWN", FieldType.STRING, 
            60);
        pstl0495_Data_Pages_Missing = pstl0495_Data_Bene_Common_Data.newFieldInGroup("pstl0495_Data_Pages_Missing", "PAGES-MISSING", FieldType.STRING, 
            20);
        pstl0495_Data_Death_Cert_Bene_Names = pstl0495_Data_Bene_Common_Data.newFieldInGroup("pstl0495_Data_Death_Cert_Bene_Names", "DEATH-CERT-BENE-NAMES", 
            FieldType.STRING, 100);
        pstl0495_Data_Pvc_Delivery_Type = pstl0495_Data_Bene_Common_Data.newFieldInGroup("pstl0495_Data_Pvc_Delivery_Type", "PVC-DELIVERY-TYPE", FieldType.STRING, 
            1);
        pstl0495_Data_Survivor_Task_Category = pstl0495_Data_Bene_Common_Data.newFieldInGroup("pstl0495_Data_Survivor_Task_Category", "SURVIVOR-TASK-CATEGORY", 
            FieldType.STRING, 1);
        pstl0495_Data_Unclaimed_Funds_State_Name = pstl0495_Data_Bene_Common_Data.newFieldInGroup("pstl0495_Data_Unclaimed_Funds_State_Name", "UNCLAIMED-FUNDS-STATE-NAME", 
            FieldType.STRING, 20);
        pstl0495_Data_Unclaimed_Funds_State_Cde = pstl0495_Data_Bene_Common_Data.newFieldInGroup("pstl0495_Data_Unclaimed_Funds_State_Cde", "UNCLAIMED-FUNDS-STATE-CDE", 
            FieldType.STRING, 2);
        pstl0495_Data_Unclaimed_Funds_Contract_Nbr = pstl0495_Data_Bene_Common_Data.newFieldInGroup("pstl0495_Data_Unclaimed_Funds_Contract_Nbr", "UNCLAIMED-FUNDS-CONTRACT-NBR", 
            FieldType.STRING, 8);
        pstl0495_Data_Brm_Name = pstl0495_Data_Bene_Common_Data.newFieldInGroup("pstl0495_Data_Brm_Name", "BRM-NAME", FieldType.STRING, 40);
        pstl0495_Data_Footer_Location = pstl0495_Data_Bene_Common_Data.newFieldInGroup("pstl0495_Data_Footer_Location", "FOOTER-LOCATION", FieldType.STRING, 
            1);
        pstl0495_Data_Survivor_Forms = pstl0495_Data_Pstl0495_Data_ArrayRedef1.newGroupInGroup("pstl0495_Data_Survivor_Forms", "SURVIVOR-FORMS");
        pstl0495_Data_Renun_Waiver_Release_Ind = pstl0495_Data_Survivor_Forms.newFieldInGroup("pstl0495_Data_Renun_Waiver_Release_Ind", "RENUN-WAIVER-RELEASE-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Marital_Stat_Addendum_Ind = pstl0495_Data_Survivor_Forms.newFieldInGroup("pstl0495_Data_Marital_Stat_Addendum_Ind", "MARITAL-STAT-ADDENDUM-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Release_Indem_Agree_Ind = pstl0495_Data_Survivor_Forms.newFieldInGroup("pstl0495_Data_Release_Indem_Agree_Ind", "RELEASE-INDEM-AGREE-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Beneficiary_Forms = pstl0495_Data_Pstl0495_Data_ArrayRedef1.newGroupInGroup("pstl0495_Data_Beneficiary_Forms", "BENEFICIARY-FORMS");
        pstl0495_Data_Savings_Invest_Appl_Ind = pstl0495_Data_Beneficiary_Forms.newFieldInGroup("pstl0495_Data_Savings_Invest_Appl_Ind", "SAVINGS-INVEST-APPL-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Survivor_Bene_Dist_Rqst_Ind = pstl0495_Data_Beneficiary_Forms.newFieldInGroup("pstl0495_Data_Survivor_Bene_Dist_Rqst_Ind", "SURVIVOR-BENE-DIST-RQST-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Income_Annuity_Request_Form_Ind = pstl0495_Data_Beneficiary_Forms.newFieldInGroup("pstl0495_Data_Income_Annuity_Request_Form_Ind", 
            "INCOME-ANNUITY-REQUEST-FORM-IND", FieldType.STRING, 1);
        pstl0495_Data_Immediate_Annuity_Form_Ind = pstl0495_Data_Beneficiary_Forms.newFieldInGroup("pstl0495_Data_Immediate_Annuity_Form_Ind", "IMMEDIATE-ANNUITY-FORM-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_One_Time_Pay_Request_Form_Ind = pstl0495_Data_Beneficiary_Forms.newFieldInGroup("pstl0495_Data_One_Time_Pay_Request_Form_Ind", "ONE-TIME-PAY-REQUEST-FORM-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Change_Of_Beneficiary_Forms = pstl0495_Data_Pstl0495_Data_ArrayRedef1.newGroupInGroup("pstl0495_Data_Change_Of_Beneficiary_Forms", 
            "CHANGE-OF-BENEFICIARY-FORMS");
        pstl0495_Data_Da_Tpa_Desig_Form_Ind = pstl0495_Data_Change_Of_Beneficiary_Forms.newFieldInGroup("pstl0495_Data_Da_Tpa_Desig_Form_Ind", "DA-TPA-DESIG-FORM-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Da_Tpa_Desig_Form_Ctr = pstl0495_Data_Change_Of_Beneficiary_Forms.newFieldInGroup("pstl0495_Data_Da_Tpa_Desig_Form_Ctr", "DA-TPA-DESIG-FORM-CTR", 
            FieldType.NUMERIC, 1);
        pstl0495_Data_Ia_Beneficiary_Form_Ind = pstl0495_Data_Change_Of_Beneficiary_Forms.newFieldInGroup("pstl0495_Data_Ia_Beneficiary_Form_Ind", "IA-BENEFICIARY-FORM-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Ia_Beneficiary_Form_Ctr = pstl0495_Data_Change_Of_Beneficiary_Forms.newFieldInGroup("pstl0495_Data_Ia_Beneficiary_Form_Ctr", "IA-BENEFICIARY-FORM-CTR", 
            FieldType.NUMERIC, 1);
        pstl0495_Data_Ohio_Arp_Bene_Form_Ind = pstl0495_Data_Change_Of_Beneficiary_Forms.newFieldInGroup("pstl0495_Data_Ohio_Arp_Bene_Form_Ind", "OHIO-ARP-BENE-FORM-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Ohio_Arp_Bene_Form_Ctr = pstl0495_Data_Change_Of_Beneficiary_Forms.newFieldInGroup("pstl0495_Data_Ohio_Arp_Bene_Form_Ctr", "OHIO-ARP-BENE-FORM-CTR", 
            FieldType.NUMERIC, 1);
        pstl0495_Data_Univ_Pittsburgh_Bene_Form_Ind = pstl0495_Data_Change_Of_Beneficiary_Forms.newFieldInGroup("pstl0495_Data_Univ_Pittsburgh_Bene_Form_Ind", 
            "UNIV-PITTSBURGH-BENE-FORM-IND", FieldType.STRING, 1);
        pstl0495_Data_Univ_Pittsburgh_Bene_Form_Ctr = pstl0495_Data_Change_Of_Beneficiary_Forms.newFieldInGroup("pstl0495_Data_Univ_Pittsburgh_Bene_Form_Ctr", 
            "UNIV-PITTSBURGH-BENE-FORM-CTR", FieldType.NUMERIC, 1);
        pstl0495_Data_Univ_Michigan_Bene_Form_Ind = pstl0495_Data_Change_Of_Beneficiary_Forms.newFieldInGroup("pstl0495_Data_Univ_Michigan_Bene_Form_Ind", 
            "UNIV-MICHIGAN-BENE-FORM-IND", FieldType.STRING, 1);
        pstl0495_Data_Univ_Michigan_Bene_Form_Ctr = pstl0495_Data_Change_Of_Beneficiary_Forms.newFieldInGroup("pstl0495_Data_Univ_Michigan_Bene_Form_Ctr", 
            "UNIV-MICHIGAN-BENE-FORM-CTR", FieldType.NUMERIC, 1);
        pstl0495_Data_Successor_Beneficiary_Form_Ind = pstl0495_Data_Change_Of_Beneficiary_Forms.newFieldInGroup("pstl0495_Data_Successor_Beneficiary_Form_Ind", 
            "SUCCESSOR-BENEFICIARY-FORM-IND", FieldType.STRING, 1);
        pstl0495_Data_Successor_Beneficiary_Form_Ctr = pstl0495_Data_Change_Of_Beneficiary_Forms.newFieldInGroup("pstl0495_Data_Successor_Beneficiary_Form_Ctr", 
            "SUCCESSOR-BENEFICIARY-FORM-CTR", FieldType.NUMERIC, 1);
        pstl0495_Data_Tax_Forms = pstl0495_Data_Pstl0495_Data_ArrayRedef1.newGroupInGroup("pstl0495_Data_Tax_Forms", "TAX-FORMS");
        pstl0495_Data_Ohio_Consent_Ind = pstl0495_Data_Tax_Forms.newFieldInGroup("pstl0495_Data_Ohio_Consent_Ind", "OHIO-CONSENT-IND", FieldType.STRING, 
            1);
        pstl0495_Data_Oklahoma_Consent_Ind = pstl0495_Data_Tax_Forms.newFieldInGroup("pstl0495_Data_Oklahoma_Consent_Ind", "OKLAHOMA-CONSENT-IND", FieldType.STRING, 
            1);
        pstl0495_Data_Indiana_Notice_Ind = pstl0495_Data_Tax_Forms.newFieldInGroup("pstl0495_Data_Indiana_Notice_Ind", "INDIANA-NOTICE-IND", FieldType.STRING, 
            1);
        pstl0495_Data_Rhode_Island_Notice_Ind = pstl0495_Data_Tax_Forms.newFieldInGroup("pstl0495_Data_Rhode_Island_Notice_Ind", "RHODE-ISLAND-NOTICE-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_W9_Form_Ind = pstl0495_Data_Tax_Forms.newFieldInGroup("pstl0495_Data_W9_Form_Ind", "W9-FORM-IND", FieldType.STRING, 1);
        pstl0495_Data_W8ben_Form_Ind = pstl0495_Data_Tax_Forms.newFieldInGroup("pstl0495_Data_W8ben_Form_Ind", "W8BEN-FORM-IND", FieldType.STRING, 1);
        pstl0495_Data_W7_Form_Ind = pstl0495_Data_Tax_Forms.newFieldInGroup("pstl0495_Data_W7_Form_Ind", "W7-FORM-IND", FieldType.STRING, 1);
        pstl0495_Data_State_Code_Taxint = pstl0495_Data_Tax_Forms.newFieldInGroup("pstl0495_Data_State_Code_Taxint", "STATE-CODE-TAXINT", FieldType.STRING, 
            2);
        pstl0495_Data_State_Name_Taxint = pstl0495_Data_Tax_Forms.newFieldInGroup("pstl0495_Data_State_Name_Taxint", "STATE-NAME-TAXINT", FieldType.STRING, 
            20);
        pstl0495_Data_State_Name_Text = pstl0495_Data_Tax_Forms.newFieldInGroup("pstl0495_Data_State_Name_Text", "STATE-NAME-TEXT", FieldType.STRING, 
            20);
        pstl0495_Data_Pre_Printed_Booklets = pstl0495_Data_Pstl0495_Data_ArrayRedef1.newGroupInGroup("pstl0495_Data_Pre_Printed_Booklets", "PRE-PRINTED-BOOKLETS");
        pstl0495_Data_Loved_One_Passes_Away_Ind = pstl0495_Data_Pre_Printed_Booklets.newFieldInGroup("pstl0495_Data_Loved_One_Passes_Away_Ind", "LOVED-ONE-PASSES-AWAY-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Sip_Booklet_Ind = pstl0495_Data_Pre_Printed_Booklets.newFieldInGroup("pstl0495_Data_Sip_Booklet_Ind", "SIP-BOOKLET-IND", FieldType.STRING, 
            1);
        pstl0495_Data_Taking_Next_Step_Ind = pstl0495_Data_Pre_Printed_Booklets.newFieldInGroup("pstl0495_Data_Taking_Next_Step_Ind", "TAKING-NEXT-STEP-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Qtrly_Performance_Ind = pstl0495_Data_Pre_Printed_Booklets.newFieldInGroup("pstl0495_Data_Qtrly_Performance_Ind", "QTRLY-PERFORMANCE-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Tiaa_Cref_Prospectus_Ind = pstl0495_Data_Pre_Printed_Booklets.newFieldInGroup("pstl0495_Data_Tiaa_Cref_Prospectus_Ind", "TIAA-CREF-PROSPECTUS-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Cref_Prospectus_Ind = pstl0495_Data_Pre_Printed_Booklets.newFieldInGroup("pstl0495_Data_Cref_Prospectus_Ind", "CREF-PROSPECTUS-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Tiaa_Rea_Prospectus_Ind = pstl0495_Data_Pre_Printed_Booklets.newFieldInGroup("pstl0495_Data_Tiaa_Rea_Prospectus_Ind", "TIAA-REA-PROSPECTUS-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Lifecycle_Prospectus_Ind = pstl0495_Data_Pre_Printed_Booklets.newFieldInGroup("pstl0495_Data_Lifecycle_Prospectus_Ind", "LIFECYCLE-PROSPECTUS-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Tiaa_Rate_Sheet_Ind = pstl0495_Data_Pre_Printed_Booklets.newFieldInGroup("pstl0495_Data_Tiaa_Rate_Sheet_Ind", "TIAA-RATE-SHEET-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Booklets_Prospectuses_With_Pdf = pstl0495_Data_Pstl0495_Data_ArrayRedef1.newGroupInGroup("pstl0495_Data_Booklets_Prospectuses_With_Pdf", 
            "BOOKLETS-PROSPECTUSES-WITH-PDF");
        pstl0495_Data_Atra_Sip_Glance_Indvl_Ind = pstl0495_Data_Booklets_Prospectuses_With_Pdf.newFieldInGroup("pstl0495_Data_Atra_Sip_Glance_Indvl_Ind", 
            "ATRA-SIP-GLANCE-INDVL-IND", FieldType.STRING, 1);
        pstl0495_Data_Atra_Ssum_Glance_Trust_Ind = pstl0495_Data_Booklets_Prospectuses_With_Pdf.newFieldInGroup("pstl0495_Data_Atra_Ssum_Glance_Trust_Ind", 
            "ATRA-SSUM-GLANCE-TRUST-IND", FieldType.STRING, 1);
        pstl0495_Data_Atra_Ssum_Glance_Indvl_Ind = pstl0495_Data_Booklets_Prospectuses_With_Pdf.newFieldInGroup("pstl0495_Data_Atra_Ssum_Glance_Indvl_Ind", 
            "ATRA-SSUM-GLANCE-INDVL-IND", FieldType.STRING, 1);
        pstl0495_Data_Atra_Ssum_Glance_Estate_Ind = pstl0495_Data_Booklets_Prospectuses_With_Pdf.newFieldInGroup("pstl0495_Data_Atra_Ssum_Glance_Estate_Ind", 
            "ATRA-SSUM-GLANCE-ESTATE-IND", FieldType.STRING, 1);
        pstl0495_Data_Atra_Annty_Glance_Indvl_Ind = pstl0495_Data_Booklets_Prospectuses_With_Pdf.newFieldInGroup("pstl0495_Data_Atra_Annty_Glance_Indvl_Ind", 
            "ATRA-ANNTY-GLANCE-INDVL-IND", FieldType.STRING, 1);
        pstl0495_Data_Atra_Fix_Prd_Ann_Glance_Ind = pstl0495_Data_Booklets_Prospectuses_With_Pdf.newFieldInGroup("pstl0495_Data_Atra_Fix_Prd_Ann_Glance_Ind", 
            "ATRA-FIX-PRD-ANN-GLANCE-IND", FieldType.STRING, 1);
        pstl0495_Data_Ssum_Glance_Nonspouse_Ind = pstl0495_Data_Booklets_Prospectuses_With_Pdf.newFieldInGroup("pstl0495_Data_Ssum_Glance_Nonspouse_Ind", 
            "SSUM-GLANCE-NONSPOUSE-IND", FieldType.STRING, 1);
        pstl0495_Data_Ssum_Glance_Spouse_Ind = pstl0495_Data_Booklets_Prospectuses_With_Pdf.newFieldInGroup("pstl0495_Data_Ssum_Glance_Spouse_Ind", "SSUM-GLANCE-SPOUSE-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Ssum_Glance_Trust_Ind = pstl0495_Data_Booklets_Prospectuses_With_Pdf.newFieldInGroup("pstl0495_Data_Ssum_Glance_Trust_Ind", "SSUM-GLANCE-TRUST-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Ssum_Glance_Estate_Ind = pstl0495_Data_Booklets_Prospectuses_With_Pdf.newFieldInGroup("pstl0495_Data_Ssum_Glance_Estate_Ind", "SSUM-GLANCE-ESTATE-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Notice_402f_Ind = pstl0495_Data_Booklets_Prospectuses_With_Pdf.newFieldInGroup("pstl0495_Data_Notice_402f_Ind", "NOTICE-402F-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Custodial_Agreement_Ind = pstl0495_Data_Booklets_Prospectuses_With_Pdf.newFieldInGroup("pstl0495_Data_Custodial_Agreement_Ind", 
            "CUSTODIAL-AGREEMENT-IND", FieldType.STRING, 1);
        pstl0495_Data_Record_Of_Age_Ind = pstl0495_Data_Booklets_Prospectuses_With_Pdf.newFieldInGroup("pstl0495_Data_Record_Of_Age_Ind", "RECORD-OF-AGE-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Payment_Dest_Form_Ind = pstl0495_Data_Booklets_Prospectuses_With_Pdf.newFieldInGroup("pstl0495_Data_Payment_Dest_Form_Ind", "PAYMENT-DEST-FORM-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Transfer_At_Settlement_Ind = pstl0495_Data_Booklets_Prospectuses_With_Pdf.newFieldInGroup("pstl0495_Data_Transfer_At_Settlement_Ind", 
            "TRANSFER-AT-SETTLEMENT-IND", FieldType.STRING, 1);
        pstl0495_Data_Step0_Blurbs = pstl0495_Data_Pstl0495_Data_ArrayRedef1.newGroupInGroup("pstl0495_Data_Step0_Blurbs", "STEP0-BLURBS");
        pstl0495_Data_My_Children_Ind = pstl0495_Data_Step0_Blurbs.newFieldInGroup("pstl0495_Data_My_Children_Ind", "MY-CHILDREN-IND", FieldType.STRING, 
            1);
        pstl0495_Data_Minor_Guardian_Hip_Ind = pstl0495_Data_Step0_Blurbs.newFieldInGroup("pstl0495_Data_Minor_Guardian_Hip_Ind", "MINOR-GUARDIAN-HIP-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Exspouse_Divorce_Decree_Ind = pstl0495_Data_Step0_Blurbs.newFieldInGroup("pstl0495_Data_Exspouse_Divorce_Decree_Ind", "EXSPOUSE-DIVORCE-DECREE-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Estate_Ind = pstl0495_Data_Step0_Blurbs.newFieldInGroup("pstl0495_Data_Estate_Ind", "ESTATE-IND", FieldType.STRING, 1);
        pstl0495_Data_Small_Estate_Ind = pstl0495_Data_Step0_Blurbs.newFieldInGroup("pstl0495_Data_Small_Estate_Ind", "SMALL-ESTATE-IND", FieldType.STRING, 
            1);
        pstl0495_Data_Unable_To_Locate_Trust_Ind = pstl0495_Data_Step0_Blurbs.newFieldInGroup("pstl0495_Data_Unable_To_Locate_Trust_Ind", "UNABLE-TO-LOCATE-TRUST-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Need_Bene_Address_Ind = pstl0495_Data_Step0_Blurbs.newFieldInGroup("pstl0495_Data_Need_Bene_Address_Ind", "NEED-BENE-ADDRESS-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Organization_Ind = pstl0495_Data_Step0_Blurbs.newFieldInGroup("pstl0495_Data_Organization_Ind", "ORGANIZATION-IND", FieldType.STRING, 
            1);
        pstl0495_Data_Trust_Ind = pstl0495_Data_Step0_Blurbs.newFieldInGroup("pstl0495_Data_Trust_Ind", "TRUST-IND", FieldType.STRING, 1);
        pstl0495_Data_Msv_Ind = pstl0495_Data_Step0_Blurbs.newFieldInGroup("pstl0495_Data_Msv_Ind", "MSV-IND", FieldType.STRING, 1);
        pstl0495_Data_Entitlement_Blurbs = pstl0495_Data_Pstl0495_Data_ArrayRedef1.newGroupInGroup("pstl0495_Data_Entitlement_Blurbs", "ENTITLEMENT-BLURBS");
        pstl0495_Data_Roth_Accumulation_Ind = pstl0495_Data_Entitlement_Blurbs.newFieldInGroup("pstl0495_Data_Roth_Accumulation_Ind", "ROTH-ACCUMULATION-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Atra_Accumulation_Ind = pstl0495_Data_Entitlement_Blurbs.newFieldInGroup("pstl0495_Data_Atra_Accumulation_Ind", "ATRA-ACCUMULATION-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Investment_Contract_Ind = pstl0495_Data_Entitlement_Blurbs.newFieldInGroup("pstl0495_Data_Investment_Contract_Ind", "INVESTMENT-CONTRACT-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Decline_Of_Change_Blurbs = pstl0495_Data_Pstl0495_Data_ArrayRedef1.newGroupInGroup("pstl0495_Data_Decline_Of_Change_Blurbs", "DECLINE-OF-CHANGE-BLURBS");
        pstl0495_Data_Allocation_Ind = pstl0495_Data_Decline_Of_Change_Blurbs.newFieldInGroup("pstl0495_Data_Allocation_Ind", "ALLOCATION-IND", FieldType.STRING, 
            1);
        pstl0495_Data_Blank_Beneficiaries_Ind = pstl0495_Data_Decline_Of_Change_Blurbs.newFieldInGroup("pstl0495_Data_Blank_Beneficiaries_Ind", "BLANK-BENEFICIARIES-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Contract_Numbers_Ind = pstl0495_Data_Decline_Of_Change_Blurbs.newFieldInGroup("pstl0495_Data_Contract_Numbers_Ind", "CONTRACT-NUMBERS-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Invalid_Date_Ind = pstl0495_Data_Decline_Of_Change_Blurbs.newFieldInGroup("pstl0495_Data_Invalid_Date_Ind", "INVALID-DATE-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Estate_Trust_Ind = pstl0495_Data_Decline_Of_Change_Blurbs.newFieldInGroup("pstl0495_Data_Estate_Trust_Ind", "ESTATE-TRUST-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Executor_Ind = pstl0495_Data_Decline_Of_Change_Blurbs.newFieldInGroup("pstl0495_Data_Executor_Ind", "EXECUTOR-IND", FieldType.STRING, 
            1);
        pstl0495_Data_Fax_Copies_Ind = pstl0495_Data_Decline_Of_Change_Blurbs.newFieldInGroup("pstl0495_Data_Fax_Copies_Ind", "FAX-COPIES-IND", FieldType.STRING, 
            1);
        pstl0495_Data_Guarantee_Period_Expire_Ind = pstl0495_Data_Decline_Of_Change_Blurbs.newFieldInGroup("pstl0495_Data_Guarantee_Period_Expire_Ind", 
            "GUARANTEE-PERIOD-EXPIRE-IND", FieldType.STRING, 1);
        pstl0495_Data_No_Guarantee_Period_Ind = pstl0495_Data_Decline_Of_Change_Blurbs.newFieldInGroup("pstl0495_Data_No_Guarantee_Period_Ind", "NO-GUARANTEE-PERIOD-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Indvl_Bene_Trust_Ind = pstl0495_Data_Decline_Of_Change_Blurbs.newFieldInGroup("pstl0495_Data_Indvl_Bene_Trust_Ind", "INDVL-BENE-TRUST-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Institution_Maintained_Ind = pstl0495_Data_Decline_Of_Change_Blurbs.newFieldInGroup("pstl0495_Data_Institution_Maintained_Ind", 
            "INSTITUTION-MAINTAINED-IND", FieldType.STRING, 1);
        pstl0495_Data_Irrevocable_Designation_Ind = pstl0495_Data_Decline_Of_Change_Blurbs.newFieldInGroup("pstl0495_Data_Irrevocable_Designation_Ind", 
            "IRREVOCABLE-DESIGNATION-IND", FieldType.STRING, 1);
        pstl0495_Data_Missing_Pages_Ind = pstl0495_Data_Decline_Of_Change_Blurbs.newFieldInGroup("pstl0495_Data_Missing_Pages_Ind", "MISSING-PAGES-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Inactive_Contracts_Ind = pstl0495_Data_Decline_Of_Change_Blurbs.newFieldInGroup("pstl0495_Data_Inactive_Contracts_Ind", "INACTIVE-CONTRACTS-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Or_Used_In_Designation_Ind = pstl0495_Data_Decline_Of_Change_Blurbs.newFieldInGroup("pstl0495_Data_Or_Used_In_Designation_Ind", 
            "OR-USED-IN-DESIGNATION-IND", FieldType.STRING, 1);
        pstl0495_Data_Same_Primary_Contingent_Ind = pstl0495_Data_Decline_Of_Change_Blurbs.newFieldInGroup("pstl0495_Data_Same_Primary_Contingent_Ind", 
            "SAME-PRIMARY-CONTINGENT-IND", FieldType.STRING, 1);
        pstl0495_Data_Siblings_Ind = pstl0495_Data_Decline_Of_Change_Blurbs.newFieldInGroup("pstl0495_Data_Siblings_Ind", "SIBLINGS-IND", FieldType.STRING, 
            1);
        pstl0495_Data_No_Signature_Date_Ind = pstl0495_Data_Decline_Of_Change_Blurbs.newFieldInGroup("pstl0495_Data_No_Signature_Date_Ind", "NO-SIGNATURE-DATE-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Spouse_As_Primary_Bene_Ind = pstl0495_Data_Decline_Of_Change_Blurbs.newFieldInGroup("pstl0495_Data_Spouse_As_Primary_Bene_Ind", 
            "SPOUSE-AS-PRIMARY-BENE-IND", FieldType.STRING, 1);
        pstl0495_Data_Surs_Ind = pstl0495_Data_Decline_Of_Change_Blurbs.newFieldInGroup("pstl0495_Data_Surs_Ind", "SURS-IND", FieldType.STRING, 1);
        pstl0495_Data_Accept_Blurbs = pstl0495_Data_Pstl0495_Data_ArrayRedef1.newGroupInGroup("pstl0495_Data_Accept_Blurbs", "ACCEPT-BLURBS");
        pstl0495_Data_Age_Stipulation_Ind = pstl0495_Data_Accept_Blurbs.newFieldInGroup("pstl0495_Data_Age_Stipulation_Ind", "AGE-STIPULATION-IND", FieldType.STRING, 
            1);
        pstl0495_Data_Deceased_Child_Chk_Gchild_Ind = pstl0495_Data_Accept_Blurbs.newFieldInGroup("pstl0495_Data_Deceased_Child_Chk_Gchild_Ind", "DECEASED-CHILD-CHK-GCHILD-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Deceased_Child_Bxchk_Schild_Ind = pstl0495_Data_Accept_Blurbs.newFieldInGroup("pstl0495_Data_Deceased_Child_Bxchk_Schild_Ind", "DECEASED-CHILD-BXCHK-SCHILD-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Difficult_To_Read_Ind = pstl0495_Data_Accept_Blurbs.newFieldInGroup("pstl0495_Data_Difficult_To_Read_Ind", "DIFFICULT-TO-READ-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Divesting_Ind = pstl0495_Data_Accept_Blurbs.newFieldInGroup("pstl0495_Data_Divesting_Ind", "DIVESTING-IND", FieldType.STRING, 1);
        pstl0495_Data_For_The_Benefit_Of_Ind = pstl0495_Data_Accept_Blurbs.newFieldInGroup("pstl0495_Data_For_The_Benefit_Of_Ind", "FOR-THE-BENEFIT-OF-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Guardian_Ind = pstl0495_Data_Accept_Blurbs.newFieldInGroup("pstl0495_Data_Guardian_Ind", "GUARDIAN-IND", FieldType.STRING, 1);
        pstl0495_Data_Mdo_Contract_Ind = pstl0495_Data_Accept_Blurbs.newFieldInGroup("pstl0495_Data_Mdo_Contract_Ind", "MDO-CONTRACT-IND", FieldType.STRING, 
            1);
        pstl0495_Data_Missing_Information_Ind = pstl0495_Data_Accept_Blurbs.newFieldInGroup("pstl0495_Data_Missing_Information_Ind", "MISSING-INFORMATION-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Department_Ind = pstl0495_Data_Accept_Blurbs.newFieldInGroup("pstl0495_Data_Department_Ind", "DEPARTMENT-IND", FieldType.STRING, 
            1);
        pstl0495_Data_No_Primary_Bene_Ind = pstl0495_Data_Accept_Blurbs.newFieldInGroup("pstl0495_Data_No_Primary_Bene_Ind", "NO-PRIMARY-BENE-IND", FieldType.STRING, 
            1);
        pstl0495_Data_Second_Annuitant_Named_Ind = pstl0495_Data_Accept_Blurbs.newFieldInGroup("pstl0495_Data_Second_Annuitant_Named_Ind", "SECOND-ANNUITANT-NAMED-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Ssn_Not_Shown_Ind = pstl0495_Data_Accept_Blurbs.newFieldInGroup("pstl0495_Data_Ssn_Not_Shown_Ind", "SSN-NOT-SHOWN-IND", FieldType.STRING, 
            1);
        pstl0495_Data_Spouse_Another_Person_Ind = pstl0495_Data_Accept_Blurbs.newFieldInGroup("pstl0495_Data_Spouse_Another_Person_Ind", "SPOUSE-ANOTHER-PERSON-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Spousal_Waiver_Inc_Ind = pstl0495_Data_Accept_Blurbs.newFieldInGroup("pstl0495_Data_Spousal_Waiver_Inc_Ind", "SPOUSAL-WAIVER-INC-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Survivorship_Clause = pstl0495_Data_Accept_Blurbs.newFieldInGroup("pstl0495_Data_Survivorship_Clause", "SURVIVORSHIP-CLAUSE", FieldType.STRING, 
            1);
        pstl0495_Data_Tin_Ind = pstl0495_Data_Accept_Blurbs.newFieldInGroup("pstl0495_Data_Tin_Ind", "TIN-IND", FieldType.STRING, 1);
        pstl0495_Data_Terms_Of_Trust_Ind = pstl0495_Data_Accept_Blurbs.newFieldInGroup("pstl0495_Data_Terms_Of_Trust_Ind", "TERMS-OF-TRUST-IND", FieldType.STRING, 
            1);
        pstl0495_Data_Trust_Date_Ind = pstl0495_Data_Accept_Blurbs.newFieldInGroup("pstl0495_Data_Trust_Date_Ind", "TRUST-DATE-IND", FieldType.STRING, 
            1);
        pstl0495_Data_Unborn_Children_Ind = pstl0495_Data_Accept_Blurbs.newFieldInGroup("pstl0495_Data_Unborn_Children_Ind", "UNBORN-CHILDREN-IND", FieldType.STRING, 
            1);
        pstl0495_Data_Unnamed_Spouse_Of_Bene_Ind = pstl0495_Data_Accept_Blurbs.newFieldInGroup("pstl0495_Data_Unnamed_Spouse_Of_Bene_Ind", "UNNAMED-SPOUSE-OF-BENE-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Forms_Change_Blurbs = pstl0495_Data_Pstl0495_Data_ArrayRedef1.newGroupInGroup("pstl0495_Data_Forms_Change_Blurbs", "FORMS-CHANGE-BLURBS");
        pstl0495_Data_No_Designation_Found_Ind = pstl0495_Data_Forms_Change_Blurbs.newFieldInGroup("pstl0495_Data_No_Designation_Found_Ind", "NO-DESIGNATION-FOUND-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Poor_Copy_Ind = pstl0495_Data_Forms_Change_Blurbs.newFieldInGroup("pstl0495_Data_Poor_Copy_Ind", "POOR-COPY-IND", FieldType.STRING, 
            1);
        pstl0495_Data_Retirement_Loan_Ind = pstl0495_Data_Forms_Change_Blurbs.newFieldInGroup("pstl0495_Data_Retirement_Loan_Ind", "RETIREMENT-LOAN-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Wpahs_Ind = pstl0495_Data_Forms_Change_Blurbs.newFieldInGroup("pstl0495_Data_Wpahs_Ind", "WPAHS-IND", FieldType.STRING, 1);
        pstl0495_Data_Change_Of_Bene_Blurb = pstl0495_Data_Pstl0495_Data_ArrayRedef1.newGroupInGroup("pstl0495_Data_Change_Of_Bene_Blurb", "CHANGE-OF-BENE-BLURB");
        pstl0495_Data_Erisa_Ind = pstl0495_Data_Change_Of_Bene_Blurb.newFieldInGroup("pstl0495_Data_Erisa_Ind", "ERISA-IND", FieldType.STRING, 1);
        pstl0495_Data_Bene_Option_Blurbs = pstl0495_Data_Pstl0495_Data_ArrayRedef1.newGroupInGroup("pstl0495_Data_Bene_Option_Blurbs", "BENE-OPTION-BLURBS");
        pstl0495_Data_Death_Certificate_Reqrd_Ind = pstl0495_Data_Bene_Option_Blurbs.newFieldInGroup("pstl0495_Data_Death_Certificate_Reqrd_Ind", "DEATH-CERTIFICATE-REQRD-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Death_Cert_Primary_Bene_Ind = pstl0495_Data_Bene_Option_Blurbs.newFieldInGroup("pstl0495_Data_Death_Cert_Primary_Bene_Ind", "DEATH-CERT-PRIMARY-BENE-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Statement_Will_Ind = pstl0495_Data_Bene_Option_Blurbs.newFieldInGroup("pstl0495_Data_Statement_Will_Ind", "STATEMENT-WILL-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Statement_Trust_In_Effect_Ind = pstl0495_Data_Bene_Option_Blurbs.newFieldInGroup("pstl0495_Data_Statement_Trust_In_Effect_Ind", 
            "STATEMENT-TRUST-IN-EFFECT-IND", FieldType.STRING, 1);
        pstl0495_Data_Statement_Trust_Accept_Ind = pstl0495_Data_Bene_Option_Blurbs.newFieldInGroup("pstl0495_Data_Statement_Trust_Accept_Ind", "STATEMENT-TRUST-ACCEPT-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Taxpayer_Id_Nbr_Ind = pstl0495_Data_Bene_Option_Blurbs.newFieldInGroup("pstl0495_Data_Taxpayer_Id_Nbr_Ind", "TAXPAYER-ID-NBR-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Letters_Of_Appointment_Ind = pstl0495_Data_Bene_Option_Blurbs.newFieldInGroup("pstl0495_Data_Letters_Of_Appointment_Ind", "LETTERS-OF-APPOINTMENT-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Small_Accum_Ind = pstl0495_Data_Bene_Option_Blurbs.newFieldInGroup("pstl0495_Data_Small_Accum_Ind", "SMALL-ACCUM-IND", FieldType.STRING, 
            1);
        pstl0495_Data_Signature_Guarantee_Ind = pstl0495_Data_Bene_Option_Blurbs.newFieldInGroup("pstl0495_Data_Signature_Guarantee_Ind", "SIGNATURE-GUARANTEE-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Death_Certificate_Ind = pstl0495_Data_Bene_Option_Blurbs.newFieldInGroup("pstl0495_Data_Death_Certificate_Ind", "DEATH-CERTIFICATE-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Reason_Code = pstl0495_Data_Bene_Option_Blurbs.newFieldArrayInGroup("pstl0495_Data_Reason_Code", "REASON-CODE", FieldType.STRING, 
            2, new DbsArrayController(1,6));
        pstl0495_Data_Reason_Text = pstl0495_Data_Bene_Option_Blurbs.newFieldInGroup("pstl0495_Data_Reason_Text", "REASON-TEXT", FieldType.STRING, 250);
        pstl0495_Data_Non_Spouse_Rmd_Ind = pstl0495_Data_Bene_Option_Blurbs.newFieldInGroup("pstl0495_Data_Non_Spouse_Rmd_Ind", "NON-SPOUSE-RMD-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Spouse_Rmd_Ind = pstl0495_Data_Bene_Option_Blurbs.newFieldInGroup("pstl0495_Data_Spouse_Rmd_Ind", "SPOUSE-RMD-IND", FieldType.STRING, 
            1);
        pstl0495_Data_Doma_Ind = pstl0495_Data_Bene_Option_Blurbs.newFieldInGroup("pstl0495_Data_Doma_Ind", "DOMA-IND", FieldType.STRING, 1);
        pstl0495_Data_Valuation_Details = pstl0495_Data_Pstl0495_Data_ArrayRedef1.newGroupInGroup("pstl0495_Data_Valuation_Details", "VALUATION-DETAILS");
        pstl0495_Data_Valuation_Contract_Nbr = pstl0495_Data_Valuation_Details.newFieldArrayInGroup("pstl0495_Data_Valuation_Contract_Nbr", "VALUATION-CONTRACT-NBR", 
            FieldType.STRING, 8, new DbsArrayController(1,25));
        pstl0495_Data_Value_Of_Contract = pstl0495_Data_Valuation_Details.newFieldArrayInGroup("pstl0495_Data_Value_Of_Contract", "VALUE-OF-CONTRACT", 
            FieldType.DECIMAL, 11,2, new DbsArrayController(1,25));
        pstl0495_Data_Total_At_Valuation_Date = pstl0495_Data_Valuation_Details.newFieldInGroup("pstl0495_Data_Total_At_Valuation_Date", "TOTAL-AT-VALUATION-DATE", 
            FieldType.DECIMAL, 11,2);
        pstl0495_Data_Alternate_Valuation_Date = pstl0495_Data_Valuation_Details.newFieldInGroup("pstl0495_Data_Alternate_Valuation_Date", "ALTERNATE-VALUATION-DATE", 
            FieldType.NUMERIC, 8);
        pstl0495_Data_Investment_In_Contract = pstl0495_Data_Valuation_Details.newFieldInGroup("pstl0495_Data_Investment_In_Contract", "INVESTMENT-IN-CONTRACT", 
            FieldType.DECIMAL, 11,2);
        pstl0495_Data_Entitlement_Details = pstl0495_Data_Pstl0495_Data_ArrayRedef1.newGroupInGroup("pstl0495_Data_Entitlement_Details", "ENTITLEMENT-DETAILS");
        pstl0495_Data_Account_Name = pstl0495_Data_Entitlement_Details.newFieldArrayInGroup("pstl0495_Data_Account_Name", "ACCOUNT-NAME", FieldType.STRING, 
            40, new DbsArrayController(1,10));
        pstl0495_Data_Account_Amount = pstl0495_Data_Entitlement_Details.newFieldArrayInGroup("pstl0495_Data_Account_Amount", "ACCOUNT-AMOUNT", FieldType.DECIMAL, 
            11,2, new DbsArrayController(1,10));
        pstl0495_Data_Entitlement_Total_Amount = pstl0495_Data_Entitlement_Details.newFieldInGroup("pstl0495_Data_Entitlement_Total_Amount", "ENTITLEMENT-TOTAL-AMOUNT", 
            FieldType.DECIMAL, 11,2);
        pstl0495_Data_Roth_Accumulation = pstl0495_Data_Entitlement_Details.newFieldInGroup("pstl0495_Data_Roth_Accumulation", "ROTH-ACCUMULATION", FieldType.DECIMAL, 
            11,2);
        pstl0495_Data_Atra_Accumulation = pstl0495_Data_Entitlement_Details.newFieldInGroup("pstl0495_Data_Atra_Accumulation", "ATRA-ACCUMULATION", FieldType.DECIMAL, 
            11,2);
        pstl0495_Data_Invest_And_Contract_Amount = pstl0495_Data_Entitlement_Details.newFieldInGroup("pstl0495_Data_Invest_And_Contract_Amount", "INVEST-AND-CONTRACT-AMOUNT", 
            FieldType.DECIMAL, 11,2);
        pstl0495_Data_Incomplete_Bene_Change_Details = pstl0495_Data_Pstl0495_Data_ArrayRedef1.newGroupInGroup("pstl0495_Data_Incomplete_Bene_Change_Details", 
            "INCOMPLETE-BENE-CHANGE-DETAILS");
        pstl0495_Data_Tiaa_Nbr = pstl0495_Data_Incomplete_Bene_Change_Details.newFieldArrayInGroup("pstl0495_Data_Tiaa_Nbr", "TIAA-NBR", FieldType.STRING, 
            8, new DbsArrayController(1,25));
        pstl0495_Data_Cref_Nbr = pstl0495_Data_Incomplete_Bene_Change_Details.newFieldArrayInGroup("pstl0495_Data_Cref_Nbr", "CREF-NBR", FieldType.STRING, 
            8, new DbsArrayController(1,25));
        pstl0495_Data_Decedent_Other_Than_Orig = pstl0495_Data_Pstl0495_Data_ArrayRedef1.newFieldInGroup("pstl0495_Data_Decedent_Other_Than_Orig", "DECEDENT-OTHER-THAN-ORIG", 
            FieldType.STRING, 40);
        pstl0495_Data_Mandatory_Tax_Form_Indicators = pstl0495_Data_Pstl0495_Data_ArrayRedef1.newGroupInGroup("pstl0495_Data_Mandatory_Tax_Form_Indicators", 
            "MANDATORY-TAX-FORM-INDICATORS");
        pstl0495_Data_Rollover_Ind = pstl0495_Data_Mandatory_Tax_Form_Indicators.newFieldInGroup("pstl0495_Data_Rollover_Ind", "ROLLOVER-IND", FieldType.STRING, 
            1);
        pstl0495_Data_Periodic_Ind = pstl0495_Data_Mandatory_Tax_Form_Indicators.newFieldInGroup("pstl0495_Data_Periodic_Ind", "PERIODIC-IND", FieldType.STRING, 
            1);
        pstl0495_Data_New_Fields = pstl0495_Data_Pstl0495_Data_ArrayRedef1.newGroupInGroup("pstl0495_Data_New_Fields", "NEW-FIELDS");
        pstl0495_Data_Wrong_Form_Ind = pstl0495_Data_New_Fields.newFieldInGroup("pstl0495_Data_Wrong_Form_Ind", "WRONG-FORM-IND", FieldType.STRING, 1);
        pstl0495_Data_Ap_Payment_Freq_Ind = pstl0495_Data_New_Fields.newFieldInGroup("pstl0495_Data_Ap_Payment_Freq_Ind", "AP-PAYMENT-FREQ-IND", FieldType.STRING, 
            1);
        pstl0495_Data_Payment_Coupon_Ind = pstl0495_Data_New_Fields.newFieldInGroup("pstl0495_Data_Payment_Coupon_Ind", "PAYMENT-COUPON-IND", FieldType.STRING, 
            1);
        pstl0495_Data_Coupon_Tiaa_Amt = pstl0495_Data_New_Fields.newFieldInGroup("pstl0495_Data_Coupon_Tiaa_Amt", "COUPON-TIAA-AMT", FieldType.DECIMAL, 
            11,2);
        pstl0495_Data_Coupon_Cref_Amt = pstl0495_Data_New_Fields.newFieldInGroup("pstl0495_Data_Coupon_Cref_Amt", "COUPON-CREF-AMT", FieldType.DECIMAL, 
            11,2);
        pstl0495_Data_Coupon_Total_Amt = pstl0495_Data_New_Fields.newFieldInGroup("pstl0495_Data_Coupon_Total_Amt", "COUPON-TOTAL-AMT", FieldType.DECIMAL, 
            11,2);
        pstl0495_Data_Bene_Prov_Date_Death = pstl0495_Data_New_Fields.newFieldInGroup("pstl0495_Data_Bene_Prov_Date_Death", "BENE-PROV-DATE-DEATH", FieldType.NUMERIC, 
            8);
        pstl0495_Data_Free_Text = pstl0495_Data_New_Fields.newFieldInGroup("pstl0495_Data_Free_Text", "FREE-TEXT", FieldType.STRING, 250);
        pstl0495_Data_Benefit_Type_Ind = pstl0495_Data_New_Fields.newFieldInGroup("pstl0495_Data_Benefit_Type_Ind", "BENEFIT-TYPE-IND", FieldType.STRING, 
            1);
        pstl0495_Data_Da_5k_Ind = pstl0495_Data_New_Fields.newFieldInGroup("pstl0495_Data_Da_5k_Ind", "DA-5K-IND", FieldType.STRING, 1);
        pstl0495_Data_Sip_Tiaa_Nbr = pstl0495_Data_New_Fields.newFieldArrayInGroup("pstl0495_Data_Sip_Tiaa_Nbr", "SIP-TIAA-NBR", FieldType.STRING, 8, 
            new DbsArrayController(1,25));
        pstl0495_Data_Sip_Cref_Nbr = pstl0495_Data_New_Fields.newFieldArrayInGroup("pstl0495_Data_Sip_Cref_Nbr", "SIP-CREF-NBR", FieldType.STRING, 8, 
            new DbsArrayController(1,25));
        pstl0495_Data_Ssum_Tiaa_Nbr = pstl0495_Data_New_Fields.newFieldArrayInGroup("pstl0495_Data_Ssum_Tiaa_Nbr", "SSUM-TIAA-NBR", FieldType.STRING, 
            8, new DbsArrayController(1,25));
        pstl0495_Data_Ssum_Cref_Nbr = pstl0495_Data_New_Fields.newFieldArrayInGroup("pstl0495_Data_Ssum_Cref_Nbr", "SSUM-CREF-NBR", FieldType.STRING, 
            8, new DbsArrayController(1,25));
        pstl0495_Data_Bene_In_The_Plan_New_Fields = pstl0495_Data_Pstl0495_Data_ArrayRedef1.newGroupInGroup("pstl0495_Data_Bene_In_The_Plan_New_Fields", 
            "BENE-IN-THE-PLAN-NEW-FIELDS");
        pstl0495_Data_Spouse_Rmd_Bene_Ind = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Spouse_Rmd_Bene_Ind", "SPOUSE-RMD-BENE-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Entity_Rmd_Ind = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Entity_Rmd_Ind", "ENTITY-RMD-IND", FieldType.STRING, 
            1);
        pstl0495_Data_Non_Spouse_Rmd_Bene_Ind = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Non_Spouse_Rmd_Bene_Ind", "NON-SPOUSE-RMD-BENE-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Second_Gen_Rmd_Ind = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Second_Gen_Rmd_Ind", "SECOND-GEN-RMD-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Phone_Acceptance_Ind = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Phone_Acceptance_Ind", "PHONE-ACCEPTANCE-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Register_Online_Acct_Ind = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Register_Online_Acct_Ind", "REGISTER-ONLINE-ACCT-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Trust_Relation_Paragraph_Ind = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Trust_Relation_Paragraph_Ind", 
            "TRUST-RELATION-PARAGRAPH-IND", FieldType.STRING, 1);
        pstl0495_Data_Brokerage_Retail_Ind = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Brokerage_Retail_Ind", "BROKERAGE-RETAIL-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Brokerage_Ira_Ind = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Brokerage_Ira_Ind", "BROKERAGE-IRA-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Brokerage_Ira_Roth_Ind = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Brokerage_Ira_Roth_Ind", "BROKERAGE-IRA-ROTH-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Sole_Spouse_Ira_Rmd_Ind = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Sole_Spouse_Ira_Rmd_Ind", "SOLE-SPOUSE-IRA-RMD-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Sole_Spouse_Atra_Rmd_Ind = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Sole_Spouse_Atra_Rmd_Ind", "SOLE-SPOUSE-ATRA-RMD-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Sole_Spouse_Both_Rmd_Ind = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Sole_Spouse_Both_Rmd_Ind", "SOLE-SPOUSE-BOTH-RMD-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Accept_Form_Spouse_Ind = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Accept_Form_Spouse_Ind", "ACCEPT-FORM-SPOUSE-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Accept_Form_Bene_Ind = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Accept_Form_Bene_Ind", "ACCEPT-FORM-BENE-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Accept_Form_Entity_Ind = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Accept_Form_Entity_Ind", "ACCEPT-FORM-ENTITY-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Onetime_Transfer_Spouse_Ind = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Onetime_Transfer_Spouse_Ind", 
            "ONETIME-TRANSFER-SPOUSE-IND", FieldType.STRING, 1);
        pstl0495_Data_Onetime_Transfer_Bene_Ind = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Onetime_Transfer_Bene_Ind", 
            "ONETIME-TRANSFER-BENE-IND", FieldType.STRING, 1);
        pstl0495_Data_Tpa_Cont_Spouse_Ind = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Tpa_Cont_Spouse_Ind", "TPA-CONT-SPOUSE-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Tpa_Cont_Bene_Ind = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Tpa_Cont_Bene_Ind", "TPA-CONT-BENE-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Cash_Wdraw_Page_Spouse_Ind = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Cash_Wdraw_Page_Spouse_Ind", 
            "CASH-WDRAW-PAGE-SPOUSE-IND", FieldType.STRING, 1);
        pstl0495_Data_Cash_Wdraw_Page_Bene_Ind = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Cash_Wdraw_Page_Bene_Ind", "CASH-WDRAW-PAGE-BENE-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Rollover_Page_Spouse_Ind = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Rollover_Page_Spouse_Ind", "ROLLOVER-PAGE-SPOUSE-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Rollover_Page_Bene_Ind = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Rollover_Page_Bene_Ind", "ROLLOVER-PAGE-BENE-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Customer_Agree_Retail_Ind = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Customer_Agree_Retail_Ind", 
            "CUSTOMER-AGREE-RETAIL-IND", FieldType.STRING, 1);
        pstl0495_Data_Affidavit_Domicile_Ind = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Affidavit_Domicile_Ind", "AFFIDAVIT-DOMICILE-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Cert_Invest_Powers_Ind = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Cert_Invest_Powers_Ind", "CERT-INVEST-POWERS-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Power_Of_Atty_Ind = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Power_Of_Atty_Ind", "POWER-OF-ATTY-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Loi_Existing_Ind = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Loi_Existing_Ind", "LOI-EXISTING-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Loi_Trust_Ind = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Loi_Trust_Ind", "LOI-TRUST-IND", FieldType.STRING, 
            1);
        pstl0495_Data_Loi_New_Ind = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Loi_New_Ind", "LOI-NEW-IND", FieldType.STRING, 
            1);
        pstl0495_Data_Loi_Jtwros_Ind = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Loi_Jtwros_Ind", "LOI-JTWROS-IND", FieldType.STRING, 
            1);
        pstl0495_Data_Onetime_Transfer_Amt = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Onetime_Transfer_Amt", "ONETIME-TRANSFER-AMT", 
            FieldType.DECIMAL, 11,2);
        pstl0495_Data_Second_Generation_Amt = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Second_Generation_Amt", "SECOND-GENERATION-AMT", 
            FieldType.DECIMAL, 11,2);
        pstl0495_Data_Brokerage_Retail_Amt = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Brokerage_Retail_Amt", "BROKERAGE-RETAIL-AMT", 
            FieldType.DECIMAL, 11,2);
        pstl0495_Data_Brokerage_Ira_Amt = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Brokerage_Ira_Amt", "BROKERAGE-IRA-AMT", 
            FieldType.DECIMAL, 11,2);
        pstl0495_Data_Brokerage_Ira_Roth_Amt = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Brokerage_Ira_Roth_Amt", "BROKERAGE-IRA-ROTH-AMT", 
            FieldType.DECIMAL, 11,2);
        pstl0495_Data_Brokerage_Retail_Acct_Nbr = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldArrayInGroup("pstl0495_Data_Brokerage_Retail_Acct_Nbr", 
            "BROKERAGE-RETAIL-ACCT-NBR", FieldType.STRING, 9, new DbsArrayController(1,2));
        pstl0495_Data_Brokerage_Ira_Acct_Nbr = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldArrayInGroup("pstl0495_Data_Brokerage_Ira_Acct_Nbr", 
            "BROKERAGE-IRA-ACCT-NBR", FieldType.STRING, 9, new DbsArrayController(1,2));
        pstl0495_Data_Brokerage_Roth_Acct_Nbr = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldArrayInGroup("pstl0495_Data_Brokerage_Roth_Acct_Nbr", 
            "BROKERAGE-ROTH-ACCT-NBR", FieldType.STRING, 9, new DbsArrayController(1,2));
        pstl0495_Data_Ro_Account_Nbr = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Ro_Account_Nbr", "RO-ACCOUNT-NBR", FieldType.STRING, 
            8);
        pstl0495_Data_Loi_Ira_Ind = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Loi_Ira_Ind", "LOI-IRA-IND", FieldType.STRING, 
            1);
        pstl0495_Data_Loi_Estate_Ind = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Loi_Estate_Ind", "LOI-ESTATE-IND", FieldType.STRING, 
            1);
        pstl0495_Data_Tpa_Page = pstl0495_Data_Bene_In_The_Plan_New_Fields.newFieldInGroup("pstl0495_Data_Tpa_Page", "TPA-PAGE", FieldType.NUMERIC, 1);
        pstl0495_Data_Pstl0495_Data_ArrayRedef2 = pstl0495_Data.newGroupInGroup("pstl0495_Data_Pstl0495_Data_ArrayRedef2", "Redefines", pstl0495_Data_Pstl0495_Data_Array);
        pstl0495_Data_Pre_Fill_Data = pstl0495_Data_Pstl0495_Data_ArrayRedef2.newGroupInGroup("pstl0495_Data_Pre_Fill_Data", "PRE-FILL-DATA");
        pstl0495_Data_Beneficiary_Information = pstl0495_Data_Pre_Fill_Data.newGroupInGroup("pstl0495_Data_Beneficiary_Information", "BENEFICIARY-INFORMATION");
        pstl0495_Data_Bi_Bene_Type = pstl0495_Data_Beneficiary_Information.newFieldInGroup("pstl0495_Data_Bi_Bene_Type", "BI-BENE-TYPE", FieldType.STRING, 
            1);
        pstl0495_Data_Bi_Bene_Name = pstl0495_Data_Beneficiary_Information.newFieldInGroup("pstl0495_Data_Bi_Bene_Name", "BI-BENE-NAME", FieldType.STRING, 
            60);
        pstl0495_Data_Bi_Bene_Trustee_Name = pstl0495_Data_Beneficiary_Information.newFieldInGroup("pstl0495_Data_Bi_Bene_Trustee_Name", "BI-BENE-TRUSTEE-NAME", 
            FieldType.STRING, 60);
        pstl0495_Data_Bi_Street = pstl0495_Data_Beneficiary_Information.newFieldInGroup("pstl0495_Data_Bi_Street", "BI-STREET", FieldType.STRING, 60);
        pstl0495_Data_Bi_City = pstl0495_Data_Beneficiary_Information.newFieldInGroup("pstl0495_Data_Bi_City", "BI-CITY", FieldType.STRING, 25);
        pstl0495_Data_Bi_State = pstl0495_Data_Beneficiary_Information.newFieldInGroup("pstl0495_Data_Bi_State", "BI-STATE", FieldType.STRING, 2);
        pstl0495_Data_Bi_Zip = pstl0495_Data_Beneficiary_Information.newFieldInGroup("pstl0495_Data_Bi_Zip", "BI-ZIP", FieldType.STRING, 10);
        pstl0495_Data_Bi_Primary_Phone = pstl0495_Data_Beneficiary_Information.newFieldInGroup("pstl0495_Data_Bi_Primary_Phone", "BI-PRIMARY-PHONE", FieldType.STRING, 
            15);
        pstl0495_Data_Bi_Alt_Phone = pstl0495_Data_Beneficiary_Information.newFieldInGroup("pstl0495_Data_Bi_Alt_Phone", "BI-ALT-PHONE", FieldType.STRING, 
            15);
        pstl0495_Data_Bi_Email_Address = pstl0495_Data_Beneficiary_Information.newFieldInGroup("pstl0495_Data_Bi_Email_Address", "BI-EMAIL-ADDRESS", FieldType.STRING, 
            55);
        pstl0495_Data_Bi_State_Legal_Residency = pstl0495_Data_Beneficiary_Information.newFieldInGroup("pstl0495_Data_Bi_State_Legal_Residency", "BI-STATE-LEGAL-RESIDENCY", 
            FieldType.STRING, 20);
        pstl0495_Data_Bi_Citizenship = pstl0495_Data_Beneficiary_Information.newFieldInGroup("pstl0495_Data_Bi_Citizenship", "BI-CITIZENSHIP", FieldType.STRING, 
            12);
        pstl0495_Data_Bi_Dob_Trust_Amend_Date = pstl0495_Data_Beneficiary_Information.newFieldInGroup("pstl0495_Data_Bi_Dob_Trust_Amend_Date", "BI-DOB-TRUST-AMEND-DATE", 
            FieldType.STRING, 8);
        pstl0495_Data_Bi_Gender = pstl0495_Data_Beneficiary_Information.newFieldInGroup("pstl0495_Data_Bi_Gender", "BI-GENDER", FieldType.STRING, 1);
        pstl0495_Data_Bi_Bene_Relation_Decedent = pstl0495_Data_Beneficiary_Information.newFieldInGroup("pstl0495_Data_Bi_Bene_Relation_Decedent", "BI-BENE-RELATION-DECEDENT", 
            FieldType.STRING, 30);
        pstl0495_Data_Calc_Bene_For_Trust_Only = pstl0495_Data_Pre_Fill_Data.newGroupInGroup("pstl0495_Data_Calc_Bene_For_Trust_Only", "CALC-BENE-FOR-TRUST-ONLY");
        pstl0495_Data_Ct_Trustee_Name = pstl0495_Data_Calc_Bene_For_Trust_Only.newFieldInGroup("pstl0495_Data_Ct_Trustee_Name", "CT-TRUSTEE-NAME", FieldType.STRING, 
            60);
        pstl0495_Data_Ct_Trustee_Dob = pstl0495_Data_Calc_Bene_For_Trust_Only.newFieldInGroup("pstl0495_Data_Ct_Trustee_Dob", "CT-TRUSTEE-DOB", FieldType.STRING, 
            8);
        pstl0495_Data_Ct_Trustee_Relation_Decedent = pstl0495_Data_Calc_Bene_For_Trust_Only.newFieldInGroup("pstl0495_Data_Ct_Trustee_Relation_Decedent", 
            "CT-TRUSTEE-RELATION-DECEDENT", FieldType.STRING, 13);
        pstl0495_Data_Settlement_Option = pstl0495_Data_Pre_Fill_Data.newGroupInGroup("pstl0495_Data_Settlement_Option", "SETTLEMENT-OPTION");
        pstl0495_Data_So_Option_Type = pstl0495_Data_Settlement_Option.newFieldInGroup("pstl0495_Data_So_Option_Type", "SO-OPTION-TYPE", FieldType.STRING, 
            2);
        pstl0495_Data_So_Partial_Cash_Amt = pstl0495_Data_Settlement_Option.newFieldInGroup("pstl0495_Data_So_Partial_Cash_Amt", "SO-PARTIAL-CASH-AMT", 
            FieldType.DECIMAL, 11,2);
        pstl0495_Data_So_Partial_Cash_Pct = pstl0495_Data_Settlement_Option.newFieldInGroup("pstl0495_Data_So_Partial_Cash_Pct", "SO-PARTIAL-CASH-PCT", 
            FieldType.NUMERIC, 3);
        pstl0495_Data_Settlement_Option_Spouses = pstl0495_Data_Pre_Fill_Data.newGroupInGroup("pstl0495_Data_Settlement_Option_Spouses", "SETTLEMENT-OPTION-SPOUSES");
        pstl0495_Data_Ss_Option_Type = pstl0495_Data_Settlement_Option_Spouses.newFieldInGroup("pstl0495_Data_Ss_Option_Type", "SS-OPTION-TYPE", FieldType.STRING, 
            2);
        pstl0495_Data_Ss_Partial_Cash_Amt = pstl0495_Data_Settlement_Option_Spouses.newFieldInGroup("pstl0495_Data_Ss_Partial_Cash_Amt", "SS-PARTIAL-CASH-AMT", 
            FieldType.DECIMAL, 11,2);
        pstl0495_Data_Ss_Partial_Cash_Pct = pstl0495_Data_Settlement_Option_Spouses.newFieldInGroup("pstl0495_Data_Ss_Partial_Cash_Pct", "SS-PARTIAL-CASH-PCT", 
            FieldType.NUMERIC, 3);
        pstl0495_Data_Ss_Rollover_Acct_Status = pstl0495_Data_Settlement_Option_Spouses.newFieldInGroup("pstl0495_Data_Ss_Rollover_Acct_Status", "SS-ROLLOVER-ACCT-STATUS", 
            FieldType.STRING, 1);
        pstl0495_Data_Ss_Rollover_Acct_Type = pstl0495_Data_Settlement_Option_Spouses.newFieldInGroup("pstl0495_Data_Ss_Rollover_Acct_Type", "SS-ROLLOVER-ACCT-TYPE", 
            FieldType.STRING, 2);
        pstl0495_Data_Ss_Rollover_Acct_Nbr = pstl0495_Data_Settlement_Option_Spouses.newFieldInGroup("pstl0495_Data_Ss_Rollover_Acct_Nbr", "SS-ROLLOVER-ACCT-NBR", 
            FieldType.STRING, 8);
        pstl0495_Data_Ss_Tiaacref_Acct_Status = pstl0495_Data_Settlement_Option_Spouses.newFieldInGroup("pstl0495_Data_Ss_Tiaacref_Acct_Status", "SS-TIAACREF-ACCT-STATUS", 
            FieldType.STRING, 1);
        pstl0495_Data_Ss_Tiaacref_Acct_Type = pstl0495_Data_Settlement_Option_Spouses.newFieldInGroup("pstl0495_Data_Ss_Tiaacref_Acct_Type", "SS-TIAACREF-ACCT-TYPE", 
            FieldType.STRING, 2);
        pstl0495_Data_Ss_Tiaacref_Acct_Nbr = pstl0495_Data_Settlement_Option_Spouses.newFieldInGroup("pstl0495_Data_Ss_Tiaacref_Acct_Nbr", "SS-TIAACREF-ACCT-NBR", 
            FieldType.STRING, 8);
        pstl0495_Data_Settlement_Option_Non_Spouses = pstl0495_Data_Pre_Fill_Data.newGroupInGroup("pstl0495_Data_Settlement_Option_Non_Spouses", "SETTLEMENT-OPTION-NON-SPOUSES");
        pstl0495_Data_Sn_Option_Type = pstl0495_Data_Settlement_Option_Non_Spouses.newFieldInGroup("pstl0495_Data_Sn_Option_Type", "SN-OPTION-TYPE", FieldType.STRING, 
            2);
        pstl0495_Data_Sn_Partial_Cash_Amt = pstl0495_Data_Settlement_Option_Non_Spouses.newFieldInGroup("pstl0495_Data_Sn_Partial_Cash_Amt", "SN-PARTIAL-CASH-AMT", 
            FieldType.DECIMAL, 11,2);
        pstl0495_Data_Sn_Partial_Cash_Pct = pstl0495_Data_Settlement_Option_Non_Spouses.newFieldInGroup("pstl0495_Data_Sn_Partial_Cash_Pct", "SN-PARTIAL-CASH-PCT", 
            FieldType.NUMERIC, 3);
        pstl0495_Data_Sn_Rollover_Acct_Status = pstl0495_Data_Settlement_Option_Non_Spouses.newFieldInGroup("pstl0495_Data_Sn_Rollover_Acct_Status", "SN-ROLLOVER-ACCT-STATUS", 
            FieldType.STRING, 1);
        pstl0495_Data_Sn_Rollover_Acct_Type = pstl0495_Data_Settlement_Option_Non_Spouses.newFieldInGroup("pstl0495_Data_Sn_Rollover_Acct_Type", "SN-ROLLOVER-ACCT-TYPE", 
            FieldType.STRING, 2);
        pstl0495_Data_Sn_Rollover_Acct_Nbr = pstl0495_Data_Settlement_Option_Non_Spouses.newFieldInGroup("pstl0495_Data_Sn_Rollover_Acct_Nbr", "SN-ROLLOVER-ACCT-NBR", 
            FieldType.STRING, 8);
        pstl0495_Data_Sn_Tiaacref_Acct_Status = pstl0495_Data_Settlement_Option_Non_Spouses.newFieldInGroup("pstl0495_Data_Sn_Tiaacref_Acct_Status", "SN-TIAACREF-ACCT-STATUS", 
            FieldType.STRING, 1);
        pstl0495_Data_Sn_Tiaacref_Acct_Type = pstl0495_Data_Settlement_Option_Non_Spouses.newFieldInGroup("pstl0495_Data_Sn_Tiaacref_Acct_Type", "SN-TIAACREF-ACCT-TYPE", 
            FieldType.STRING, 2);
        pstl0495_Data_Sn_Tiaacref_Acct_Nbr = pstl0495_Data_Settlement_Option_Non_Spouses.newFieldInGroup("pstl0495_Data_Sn_Tiaacref_Acct_Nbr", "SN-TIAACREF-ACCT-NBR", 
            FieldType.STRING, 8);
        pstl0495_Data_Sn_Md_Olest_Bene_Dob = pstl0495_Data_Settlement_Option_Non_Spouses.newFieldInGroup("pstl0495_Data_Sn_Md_Olest_Bene_Dob", "SN-MD-OLEST-BENE-DOB", 
            FieldType.NUMERIC, 8);
        pstl0495_Data_Choose_Frequency = pstl0495_Data_Pre_Fill_Data.newGroupInGroup("pstl0495_Data_Choose_Frequency", "CHOOSE-FREQUENCY");
        pstl0495_Data_Cf_Mmdd = pstl0495_Data_Choose_Frequency.newFieldInGroup("pstl0495_Data_Cf_Mmdd", "CF-MMDD", FieldType.STRING, 4);
        pstl0495_Data_Cf_Freq = pstl0495_Data_Choose_Frequency.newFieldInGroup("pstl0495_Data_Cf_Freq", "CF-FREQ", FieldType.STRING, 1);
        pstl0495_Data_Type_Of_Rollover = pstl0495_Data_Pre_Fill_Data.newGroupInGroup("pstl0495_Data_Type_Of_Rollover", "TYPE-OF-ROLLOVER");
        pstl0495_Data_Tr_Rollover_Type = pstl0495_Data_Type_Of_Rollover.newFieldInGroup("pstl0495_Data_Tr_Rollover_Type", "TR-ROLLOVER-TYPE", FieldType.STRING, 
            1);
        pstl0495_Data_Tr_O1_Trad_Ira_Nbr = pstl0495_Data_Type_Of_Rollover.newFieldInGroup("pstl0495_Data_Tr_O1_Trad_Ira_Nbr", "TR-O1-TRAD-IRA-NBR", FieldType.STRING, 
            8);
        pstl0495_Data_Tr_O3_Company_Name = pstl0495_Data_Type_Of_Rollover.newFieldInGroup("pstl0495_Data_Tr_O3_Company_Name", "TR-O3-COMPANY-NAME", FieldType.STRING, 
            45);
        pstl0495_Data_Tr_O3_Company_Address = pstl0495_Data_Type_Of_Rollover.newFieldInGroup("pstl0495_Data_Tr_O3_Company_Address", "TR-O3-COMPANY-ADDRESS", 
            FieldType.STRING, 60);
        pstl0495_Data_Tr_O3_Company_City = pstl0495_Data_Type_Of_Rollover.newFieldInGroup("pstl0495_Data_Tr_O3_Company_City", "TR-O3-COMPANY-CITY", FieldType.STRING, 
            25);
        pstl0495_Data_Tr_O3_Company_State = pstl0495_Data_Type_Of_Rollover.newFieldInGroup("pstl0495_Data_Tr_O3_Company_State", "TR-O3-COMPANY-STATE", 
            FieldType.STRING, 2);
        pstl0495_Data_Tr_O3_Company_Zip = pstl0495_Data_Type_Of_Rollover.newFieldInGroup("pstl0495_Data_Tr_O3_Company_Zip", "TR-O3-COMPANY-ZIP", FieldType.STRING, 
            10);
        pstl0495_Data_Tr_O3_Company_Phone = pstl0495_Data_Type_Of_Rollover.newFieldInGroup("pstl0495_Data_Tr_O3_Company_Phone", "TR-O3-COMPANY-PHONE", 
            FieldType.STRING, 15);
        pstl0495_Data_Tr_O3_Company_Acct_Nbr = pstl0495_Data_Type_Of_Rollover.newFieldInGroup("pstl0495_Data_Tr_O3_Company_Acct_Nbr", "TR-O3-COMPANY-ACCT-NBR", 
            FieldType.STRING, 40);
        pstl0495_Data_Rollover_To_Another_Invco = pstl0495_Data_Pre_Fill_Data.newGroupInGroup("pstl0495_Data_Rollover_To_Another_Invco", "ROLLOVER-TO-ANOTHER-INVCO");
        pstl0495_Data_Ri_Option_Cde = pstl0495_Data_Rollover_To_Another_Invco.newFieldInGroup("pstl0495_Data_Ri_Option_Cde", "RI-OPTION-CDE", FieldType.STRING, 
            1);
        pstl0495_Data_Ri_Invest_Company_Name = pstl0495_Data_Rollover_To_Another_Invco.newFieldInGroup("pstl0495_Data_Ri_Invest_Company_Name", "RI-INVEST-COMPANY-NAME", 
            FieldType.STRING, 45);
        pstl0495_Data_Ri_Invest_Company_Address = pstl0495_Data_Rollover_To_Another_Invco.newFieldInGroup("pstl0495_Data_Ri_Invest_Company_Address", "RI-INVEST-COMPANY-ADDRESS", 
            FieldType.STRING, 60);
        pstl0495_Data_Ri_Invest_Company_City = pstl0495_Data_Rollover_To_Another_Invco.newFieldInGroup("pstl0495_Data_Ri_Invest_Company_City", "RI-INVEST-COMPANY-CITY", 
            FieldType.STRING, 25);
        pstl0495_Data_Ri_Invest_Company_State = pstl0495_Data_Rollover_To_Another_Invco.newFieldInGroup("pstl0495_Data_Ri_Invest_Company_State", "RI-INVEST-COMPANY-STATE", 
            FieldType.STRING, 2);
        pstl0495_Data_Ri_Invest_Company_Zip = pstl0495_Data_Rollover_To_Another_Invco.newFieldInGroup("pstl0495_Data_Ri_Invest_Company_Zip", "RI-INVEST-COMPANY-ZIP", 
            FieldType.STRING, 10);
        pstl0495_Data_Ri_Invest_Company_Phone = pstl0495_Data_Rollover_To_Another_Invco.newFieldInGroup("pstl0495_Data_Ri_Invest_Company_Phone", "RI-INVEST-COMPANY-PHONE", 
            FieldType.STRING, 15);
        pstl0495_Data_Ri_Invest_Company_Acct_Nbr = pstl0495_Data_Rollover_To_Another_Invco.newFieldInGroup("pstl0495_Data_Ri_Invest_Company_Acct_Nbr", 
            "RI-INVEST-COMPANY-ACCT-NBR", FieldType.STRING, 40);
        pstl0495_Data_Roth_Only = pstl0495_Data_Pre_Fill_Data.newGroupInGroup("pstl0495_Data_Roth_Only", "ROTH-ONLY");
        pstl0495_Data_Ro_Roth_Ind = pstl0495_Data_Roth_Only.newFieldInGroup("pstl0495_Data_Ro_Roth_Ind", "RO-ROTH-IND", FieldType.STRING, 1);
        pstl0495_Data_Ro_Roth_Amt = pstl0495_Data_Roth_Only.newFieldInGroup("pstl0495_Data_Ro_Roth_Amt", "RO-ROTH-AMT", FieldType.DECIMAL, 11,2);
        pstl0495_Data_Ro_Roth_Pct = pstl0495_Data_Roth_Only.newFieldInGroup("pstl0495_Data_Ro_Roth_Pct", "RO-ROTH-PCT", FieldType.NUMERIC, 3);
        pstl0495_Data_Federal_Tax_Wholding_Amount = pstl0495_Data_Pre_Fill_Data.newGroupInGroup("pstl0495_Data_Federal_Tax_Wholding_Amount", "FEDERAL-TAX-WHOLDING-AMOUNT");
        pstl0495_Data_Ft_Whold_Type = pstl0495_Data_Federal_Tax_Wholding_Amount.newFieldInGroup("pstl0495_Data_Ft_Whold_Type", "FT-WHOLD-TYPE", FieldType.STRING, 
            2);
        pstl0495_Data_Ft_Da_Dollar_Amt = pstl0495_Data_Federal_Tax_Wholding_Amount.newFieldInGroup("pstl0495_Data_Ft_Da_Dollar_Amt", "FT-DA-DOLLAR-AMT", 
            FieldType.DECIMAL, 11,2);
        pstl0495_Data_Ft_Pc_Percent = pstl0495_Data_Federal_Tax_Wholding_Amount.newFieldInGroup("pstl0495_Data_Ft_Pc_Percent", "FT-PC-PERCENT", FieldType.NUMERIC, 
            3);
        pstl0495_Data_Ft_Re_Dollar_Amt = pstl0495_Data_Federal_Tax_Wholding_Amount.newFieldInGroup("pstl0495_Data_Ft_Re_Dollar_Amt", "FT-RE-DOLLAR-AMT", 
            FieldType.DECIMAL, 11,2);
        pstl0495_Data_Ft_Re_Percent = pstl0495_Data_Federal_Tax_Wholding_Amount.newFieldInGroup("pstl0495_Data_Ft_Re_Percent", "FT-RE-PERCENT", FieldType.NUMERIC, 
            3);
        pstl0495_Data_Ft_N2_Dollar_Amt = pstl0495_Data_Federal_Tax_Wholding_Amount.newFieldInGroup("pstl0495_Data_Ft_N2_Dollar_Amt", "FT-N2-DOLLAR-AMT", 
            FieldType.DECIMAL, 11,2);
        pstl0495_Data_Ft_N2_Percent = pstl0495_Data_Federal_Tax_Wholding_Amount.newFieldInGroup("pstl0495_Data_Ft_N2_Percent", "FT-N2-PERCENT", FieldType.NUMERIC, 
            3);
        pstl0495_Data_Ft_N3_Status = pstl0495_Data_Federal_Tax_Wholding_Amount.newFieldInGroup("pstl0495_Data_Ft_N3_Status", "FT-N3-STATUS", FieldType.STRING, 
            1);
        pstl0495_Data_Ft_N3_Nbr_Whold_Allow = pstl0495_Data_Federal_Tax_Wholding_Amount.newFieldInGroup("pstl0495_Data_Ft_N3_Nbr_Whold_Allow", "FT-N3-NBR-WHOLD-ALLOW", 
            FieldType.NUMERIC, 2);
        pstl0495_Data_Ft_Op_Dollar_Amt = pstl0495_Data_Federal_Tax_Wholding_Amount.newFieldInGroup("pstl0495_Data_Ft_Op_Dollar_Amt", "FT-OP-DOLLAR-AMT", 
            FieldType.DECIMAL, 11,2);
        pstl0495_Data_Ft_Cv_Dollar_Amt = pstl0495_Data_Federal_Tax_Wholding_Amount.newFieldInGroup("pstl0495_Data_Ft_Cv_Dollar_Amt", "FT-CV-DOLLAR-AMT", 
            FieldType.DECIMAL, 11,2);
        pstl0495_Data_Ft_Cv_Percent = pstl0495_Data_Federal_Tax_Wholding_Amount.newFieldInGroup("pstl0495_Data_Ft_Cv_Percent", "FT-CV-PERCENT", FieldType.NUMERIC, 
            3);
        pstl0495_Data_Beneficiary_List = pstl0495_Data_Pre_Fill_Data.newGroupInGroup("pstl0495_Data_Beneficiary_List", "BENEFICIARY-LIST");
        pstl0495_Data_Bl_Primary_List = pstl0495_Data_Beneficiary_List.newGroupArrayInGroup("pstl0495_Data_Bl_Primary_List", "BL-PRIMARY-LIST", new DbsArrayController(1,
            4));
        pstl0495_Data_Bl_Primary_Name = pstl0495_Data_Bl_Primary_List.newFieldInGroup("pstl0495_Data_Bl_Primary_Name", "BL-PRIMARY-NAME", FieldType.STRING, 
            60);
        pstl0495_Data_Bl_Primary_Pct = pstl0495_Data_Bl_Primary_List.newFieldInGroup("pstl0495_Data_Bl_Primary_Pct", "BL-PRIMARY-PCT", FieldType.NUMERIC, 
            3);
        pstl0495_Data_Bl_Primary_Dob = pstl0495_Data_Bl_Primary_List.newFieldInGroup("pstl0495_Data_Bl_Primary_Dob", "BL-PRIMARY-DOB", FieldType.NUMERIC, 
            8);
        pstl0495_Data_Bl_Primary_Relation = pstl0495_Data_Bl_Primary_List.newFieldInGroup("pstl0495_Data_Bl_Primary_Relation", "BL-PRIMARY-RELATION", 
            FieldType.STRING, 13);
        pstl0495_Data_Bl_Contingent_List = pstl0495_Data_Beneficiary_List.newGroupArrayInGroup("pstl0495_Data_Bl_Contingent_List", "BL-CONTINGENT-LIST", 
            new DbsArrayController(1,4));
        pstl0495_Data_Bl_Contingent_Name = pstl0495_Data_Bl_Contingent_List.newFieldInGroup("pstl0495_Data_Bl_Contingent_Name", "BL-CONTINGENT-NAME", 
            FieldType.STRING, 60);
        pstl0495_Data_Bl_Contingent_Pct = pstl0495_Data_Bl_Contingent_List.newFieldInGroup("pstl0495_Data_Bl_Contingent_Pct", "BL-CONTINGENT-PCT", FieldType.NUMERIC, 
            3);
        pstl0495_Data_Bl_Contingent_Dob = pstl0495_Data_Bl_Contingent_List.newFieldInGroup("pstl0495_Data_Bl_Contingent_Dob", "BL-CONTINGENT-DOB", FieldType.NUMERIC, 
            8);
        pstl0495_Data_Bl_Contingent_Relation = pstl0495_Data_Bl_Contingent_List.newFieldInGroup("pstl0495_Data_Bl_Contingent_Relation", "BL-CONTINGENT-RELATION", 
            FieldType.STRING, 13);
        pstl0495_Data_Delivery_Instructions = pstl0495_Data_Pre_Fill_Data.newGroupInGroup("pstl0495_Data_Delivery_Instructions", "DELIVERY-INSTRUCTIONS");
        pstl0495_Data_Di_Option_Cde = pstl0495_Data_Delivery_Instructions.newFieldInGroup("pstl0495_Data_Di_Option_Cde", "DI-OPTION-CDE", FieldType.STRING, 
            1);
        pstl0495_Data_Di_O2_Acct_Type = pstl0495_Data_Delivery_Instructions.newFieldInGroup("pstl0495_Data_Di_O2_Acct_Type", "DI-O2-ACCT-TYPE", FieldType.STRING, 
            1);
        pstl0495_Data_Di_O2_Amt = pstl0495_Data_Delivery_Instructions.newFieldInGroup("pstl0495_Data_Di_O2_Amt", "DI-O2-AMT", FieldType.DECIMAL, 11,2);
        pstl0495_Data_Di_O2_Invest_Bank_Name = pstl0495_Data_Delivery_Instructions.newFieldInGroup("pstl0495_Data_Di_O2_Invest_Bank_Name", "DI-O2-INVEST-BANK-NAME", 
            FieldType.STRING, 45);
        pstl0495_Data_Di_O2_Address = pstl0495_Data_Delivery_Instructions.newFieldInGroup("pstl0495_Data_Di_O2_Address", "DI-O2-ADDRESS", FieldType.STRING, 
            60);
        pstl0495_Data_Di_O2_City = pstl0495_Data_Delivery_Instructions.newFieldInGroup("pstl0495_Data_Di_O2_City", "DI-O2-CITY", FieldType.STRING, 25);
        pstl0495_Data_Di_O2_State = pstl0495_Data_Delivery_Instructions.newFieldInGroup("pstl0495_Data_Di_O2_State", "DI-O2-STATE", FieldType.STRING, 
            2);
        pstl0495_Data_Di_O2_Zip = pstl0495_Data_Delivery_Instructions.newFieldInGroup("pstl0495_Data_Di_O2_Zip", "DI-O2-ZIP", FieldType.STRING, 10);
        pstl0495_Data_Di_O2_Bank_Acct_Phone = pstl0495_Data_Delivery_Instructions.newFieldInGroup("pstl0495_Data_Di_O2_Bank_Acct_Phone", "DI-O2-BANK-ACCT-PHONE", 
            FieldType.STRING, 15);
        pstl0495_Data_Di_O2_Bank_Acct_Nbr = pstl0495_Data_Delivery_Instructions.newFieldInGroup("pstl0495_Data_Di_O2_Bank_Acct_Nbr", "DI-O2-BANK-ACCT-NBR", 
            FieldType.STRING, 40);
        pstl0495_Data_Di_O2_Bank_Routing_Nbr = pstl0495_Data_Delivery_Instructions.newFieldInGroup("pstl0495_Data_Di_O2_Bank_Routing_Nbr", "DI-O2-BANK-ROUTING-NBR", 
            FieldType.STRING, 9);
        pstl0495_Data_Di_O3_Address_Type = pstl0495_Data_Delivery_Instructions.newFieldInGroup("pstl0495_Data_Di_O3_Address_Type", "DI-O3-ADDRESS-TYPE", 
            FieldType.STRING, 1);
        pstl0495_Data_Di_O3_Address = pstl0495_Data_Delivery_Instructions.newFieldInGroup("pstl0495_Data_Di_O3_Address", "DI-O3-ADDRESS", FieldType.STRING, 
            60);
        pstl0495_Data_Di_O3_City = pstl0495_Data_Delivery_Instructions.newFieldInGroup("pstl0495_Data_Di_O3_City", "DI-O3-CITY", FieldType.STRING, 25);
        pstl0495_Data_Di_O3_State = pstl0495_Data_Delivery_Instructions.newFieldInGroup("pstl0495_Data_Di_O3_State", "DI-O3-STATE", FieldType.STRING, 
            2);
        pstl0495_Data_Di_O3_Zip = pstl0495_Data_Delivery_Instructions.newFieldInGroup("pstl0495_Data_Di_O3_Zip", "DI-O3-ZIP", FieldType.STRING, 10);
        pstl0495_Data_Di_O4_Roth_Ira_Acct = pstl0495_Data_Delivery_Instructions.newFieldInGroup("pstl0495_Data_Di_O4_Roth_Ira_Acct", "DI-O4-ROTH-IRA-ACCT", 
            FieldType.STRING, 8);
        pstl0495_Data_Annuity_Method = pstl0495_Data_Pre_Fill_Data.newGroupInGroup("pstl0495_Data_Annuity_Method", "ANNUITY-METHOD");
        pstl0495_Data_Am_Option_Cde = pstl0495_Data_Annuity_Method.newFieldInGroup("pstl0495_Data_Am_Option_Cde", "AM-OPTION-CDE", FieldType.STRING, 1);
        pstl0495_Data_Am_O1_Payment_Years = pstl0495_Data_Annuity_Method.newFieldInGroup("pstl0495_Data_Am_O1_Payment_Years", "AM-O1-PAYMENT-YEARS", FieldType.NUMERIC, 
            2);
        pstl0495_Data_Am_O1_Payment_Freq = pstl0495_Data_Annuity_Method.newFieldInGroup("pstl0495_Data_Am_O1_Payment_Freq", "AM-O1-PAYMENT-FREQ", FieldType.STRING, 
            1);
        pstl0495_Data_Am_O3_Guaranteed_Prd = pstl0495_Data_Annuity_Method.newFieldInGroup("pstl0495_Data_Am_O3_Guaranteed_Prd", "AM-O3-GUARANTEED-PRD", 
            FieldType.STRING, 1);
        pstl0495_Data_Am_O3_Payment_Freq = pstl0495_Data_Annuity_Method.newFieldInGroup("pstl0495_Data_Am_O3_Payment_Freq", "AM-O3-PAYMENT-FREQ", FieldType.STRING, 
            1);
        pstl0495_Data_Payment_Options = pstl0495_Data_Pre_Fill_Data.newGroupInGroup("pstl0495_Data_Payment_Options", "PAYMENT-OPTIONS");
        pstl0495_Data_Po_Option_Cde = pstl0495_Data_Payment_Options.newFieldInGroup("pstl0495_Data_Po_Option_Cde", "PO-OPTION-CDE", FieldType.STRING, 
            1);
        pstl0495_Data_Po_Tiaa_Traditional_Ind = pstl0495_Data_Payment_Options.newFieldInGroup("pstl0495_Data_Po_Tiaa_Traditional_Ind", "PO-TIAA-TRADITIONAL-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Po_Cref_Bond_Market_Ind = pstl0495_Data_Payment_Options.newFieldInGroup("pstl0495_Data_Po_Cref_Bond_Market_Ind", "PO-CREF-BOND-MARKET-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Po_Tiaa_Real_Estate_Ind = pstl0495_Data_Payment_Options.newFieldInGroup("pstl0495_Data_Po_Tiaa_Real_Estate_Ind", "PO-TIAA-REAL-ESTATE-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Po_Cref_Glbl_Equity_Ind = pstl0495_Data_Payment_Options.newFieldInGroup("pstl0495_Data_Po_Cref_Glbl_Equity_Ind", "PO-CREF-GLBL-EQUITY-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Po_Cref_Stock_Ind = pstl0495_Data_Payment_Options.newFieldInGroup("pstl0495_Data_Po_Cref_Stock_Ind", "PO-CREF-STOCK-IND", FieldType.STRING, 
            1);
        pstl0495_Data_Po_Cref_Growth_Ind = pstl0495_Data_Payment_Options.newFieldInGroup("pstl0495_Data_Po_Cref_Growth_Ind", "PO-CREF-GROWTH-IND", FieldType.STRING, 
            1);
        pstl0495_Data_Po_Cref_Money_Mrkt_Ind = pstl0495_Data_Payment_Options.newFieldInGroup("pstl0495_Data_Po_Cref_Money_Mrkt_Ind", "PO-CREF-MONEY-MRKT-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Po_Cref_Equity_Ndx_Ind = pstl0495_Data_Payment_Options.newFieldInGroup("pstl0495_Data_Po_Cref_Equity_Ndx_Ind", "PO-CREF-EQUITY-NDX-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Po_Cref_Social_Chc_Ind = pstl0495_Data_Payment_Options.newFieldInGroup("pstl0495_Data_Po_Cref_Social_Chc_Ind", "PO-CREF-SOCIAL-CHC-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Po_Cref_Inf_Linked_Ind = pstl0495_Data_Payment_Options.newFieldInGroup("pstl0495_Data_Po_Cref_Inf_Linked_Ind", "PO-CREF-INF-LINKED-IND", 
            FieldType.STRING, 1);
        pstl0495_Data_Payment_Method = pstl0495_Data_Pre_Fill_Data.newGroupInGroup("pstl0495_Data_Payment_Method", "PAYMENT-METHOD");
        pstl0495_Data_Pm_Option_Cde = pstl0495_Data_Payment_Method.newFieldInGroup("pstl0495_Data_Pm_Option_Cde", "PM-OPTION-CDE", FieldType.STRING, 1);
        pstl0495_Data_Pm_O1_This_Amt = pstl0495_Data_Payment_Method.newFieldInGroup("pstl0495_Data_Pm_O1_This_Amt", "PM-O1-THIS-AMT", FieldType.DECIMAL, 
            11,2);
        pstl0495_Data_Pm_O1_Graded_Prd_Amt = pstl0495_Data_Payment_Method.newFieldInGroup("pstl0495_Data_Pm_O1_Graded_Prd_Amt", "PM-O1-GRADED-PRD-AMT", 
            FieldType.DECIMAL, 11,2);
        pstl0495_Data_Pm_O4_Freq = pstl0495_Data_Payment_Method.newFieldInGroup("pstl0495_Data_Pm_O4_Freq", "PM-O4-FREQ", FieldType.STRING, 1);
        pstl0495_Data_Pm_O4_Start_Life_Income_Dte = pstl0495_Data_Payment_Method.newFieldInGroup("pstl0495_Data_Pm_O4_Start_Life_Income_Dte", "PM-O4-START-LIFE-INCOME-DTE", 
            FieldType.NUMERIC, 8);
        pstl0495_Data_Pm_O4_Payment_Freq = pstl0495_Data_Payment_Method.newFieldInGroup("pstl0495_Data_Pm_O4_Payment_Freq", "PM-O4-PAYMENT-FREQ", FieldType.STRING, 
            1);
        pstl0495_Data_Renunciation_Release_1time_Pay = pstl0495_Data_Pre_Fill_Data.newGroupInGroup("pstl0495_Data_Renunciation_Release_1time_Pay", "RENUNCIATION-RELEASE-1TIME-PAY");
        pstl0495_Data_Rw_Approx_Retire_Benefits = pstl0495_Data_Renunciation_Release_1time_Pay.newFieldInGroup("pstl0495_Data_Rw_Approx_Retire_Benefits", 
            "RW-APPROX-RETIRE-BENEFITS", FieldType.DECIMAL, 11,2);
        pstl0495_Data_Rw_Asof_Date = pstl0495_Data_Renunciation_Release_1time_Pay.newFieldInGroup("pstl0495_Data_Rw_Asof_Date", "RW-ASOF-DATE", FieldType.NUMERIC, 
            8);
        pstl0495_Data_Mdo_Only = pstl0495_Data_Pre_Fill_Data.newGroupInGroup("pstl0495_Data_Mdo_Only", "MDO-ONLY");
        pstl0495_Data_Md_Withhold_Ind = pstl0495_Data_Mdo_Only.newFieldInGroup("pstl0495_Data_Md_Withhold_Ind", "MD-WITHHOLD-IND", FieldType.STRING, 1);
        pstl0495_Data_Md_Withhold_Amt = pstl0495_Data_Mdo_Only.newFieldInGroup("pstl0495_Data_Md_Withhold_Amt", "MD-WITHHOLD-AMT", FieldType.DECIMAL, 
            11,2);
        pstl0495_Data_Di_Additional = pstl0495_Data_Pre_Fill_Data.newGroupInGroup("pstl0495_Data_Di_Additional", "DI-ADDITIONAL");
        pstl0495_Data_Di_O4_Tiaa_Acct_Type = pstl0495_Data_Di_Additional.newFieldInGroup("pstl0495_Data_Di_O4_Tiaa_Acct_Type", "DI-O4-TIAA-ACCT-TYPE", 
            FieldType.STRING, 2);
        pstl0495_Data_Di_O4_Careof = pstl0495_Data_Di_Additional.newFieldInGroup("pstl0495_Data_Di_O4_Careof", "DI-O4-CAREOF", FieldType.STRING, 60);
        pstl0495_Data_Pstl0495_Data_ArrayRedef3 = pstl0495_Data.newGroupInGroup("pstl0495_Data_Pstl0495_Data_ArrayRedef3", "Redefines", pstl0495_Data_Pstl0495_Data_Array);
        pstl0495_Data_Power_Image_Task_Information = pstl0495_Data_Pstl0495_Data_ArrayRedef3.newGroupInGroup("pstl0495_Data_Power_Image_Task_Information", 
            "POWER-IMAGE-TASK-INFORMATION");
        pstl0495_Data_Pi_Begin_Literal = pstl0495_Data_Power_Image_Task_Information.newFieldInGroup("pstl0495_Data_Pi_Begin_Literal", "PI-BEGIN-LITERAL", 
            FieldType.STRING, 4);
        pstl0495_Data_Pi_Export_Ind = pstl0495_Data_Power_Image_Task_Information.newFieldInGroup("pstl0495_Data_Pi_Export_Ind", "PI-EXPORT-IND", FieldType.STRING, 
            1);
        pstl0495_Data_Pi_Task_Id = pstl0495_Data_Power_Image_Task_Information.newFieldInGroup("pstl0495_Data_Pi_Task_Id", "PI-TASK-ID", FieldType.STRING, 
            11);
        pstl0495_Data_Pi_Task_Type = pstl0495_Data_Power_Image_Task_Information.newFieldInGroup("pstl0495_Data_Pi_Task_Type", "PI-TASK-TYPE", FieldType.STRING, 
            10);
        pstl0495_Data_Pi_Task_Guid = pstl0495_Data_Power_Image_Task_Information.newFieldInGroup("pstl0495_Data_Pi_Task_Guid", "PI-TASK-GUID", FieldType.STRING, 
            47);
        pstl0495_Data_Pi_Action_Step = pstl0495_Data_Power_Image_Task_Information.newFieldInGroup("pstl0495_Data_Pi_Action_Step", "PI-ACTION-STEP", FieldType.STRING, 
            8);
        pstl0495_Data_Pi_Tiaa_Full_Date = pstl0495_Data_Power_Image_Task_Information.newFieldInGroup("pstl0495_Data_Pi_Tiaa_Full_Date", "PI-TIAA-FULL-DATE", 
            FieldType.STRING, 8);
        pstl0495_Data_Pi_Tiaa_Full_DateRedef4 = pstl0495_Data_Power_Image_Task_Information.newGroupInGroup("pstl0495_Data_Pi_Tiaa_Full_DateRedef4", "Redefines", 
            pstl0495_Data_Pi_Tiaa_Full_Date);
        pstl0495_Data_Pi_Tiaa_Date_Century = pstl0495_Data_Pi_Tiaa_Full_DateRedef4.newFieldInGroup("pstl0495_Data_Pi_Tiaa_Date_Century", "PI-TIAA-DATE-CENTURY", 
            FieldType.NUMERIC, 2);
        pstl0495_Data_Pi_Tiaa_Date_Year = pstl0495_Data_Pi_Tiaa_Full_DateRedef4.newFieldInGroup("pstl0495_Data_Pi_Tiaa_Date_Year", "PI-TIAA-DATE-YEAR", 
            FieldType.NUMERIC, 2);
        pstl0495_Data_Pi_Tiaa_Date_Month = pstl0495_Data_Pi_Tiaa_Full_DateRedef4.newFieldInGroup("pstl0495_Data_Pi_Tiaa_Date_Month", "PI-TIAA-DATE-MONTH", 
            FieldType.NUMERIC, 2);
        pstl0495_Data_Pi_Tiaa_Date_Day = pstl0495_Data_Pi_Tiaa_Full_DateRedef4.newFieldInGroup("pstl0495_Data_Pi_Tiaa_Date_Day", "PI-TIAA-DATE-DAY", FieldType.NUMERIC, 
            2);
        pstl0495_Data_Pi_Tiaa_Time = pstl0495_Data_Power_Image_Task_Information.newFieldInGroup("pstl0495_Data_Pi_Tiaa_Time", "PI-TIAA-TIME", FieldType.STRING, 
            8);
        pstl0495_Data_Pi_Task_Status = pstl0495_Data_Power_Image_Task_Information.newFieldInGroup("pstl0495_Data_Pi_Task_Status", "PI-TASK-STATUS", FieldType.STRING, 
            1);
        pstl0495_Data_Pi_Ssn = pstl0495_Data_Power_Image_Task_Information.newFieldInGroup("pstl0495_Data_Pi_Ssn", "PI-SSN", FieldType.STRING, 9);
        pstl0495_Data_Pi_Pin_Npin_Ppg = pstl0495_Data_Power_Image_Task_Information.newFieldInGroup("pstl0495_Data_Pi_Pin_Npin_Ppg", "PI-PIN-NPIN-PPG", 
            FieldType.STRING, 7);
        pstl0495_Data_Pi_Pin_Type = pstl0495_Data_Power_Image_Task_Information.newFieldInGroup("pstl0495_Data_Pi_Pin_Type", "PI-PIN-TYPE", FieldType.STRING, 
            1);
        pstl0495_Data_Pi_Contract = pstl0495_Data_Power_Image_Task_Information.newFieldInGroup("pstl0495_Data_Pi_Contract", "PI-CONTRACT", FieldType.STRING, 
            100);
        pstl0495_Data_Pi_ContractRedef5 = pstl0495_Data_Power_Image_Task_Information.newGroupInGroup("pstl0495_Data_Pi_ContractRedef5", "Redefines", pstl0495_Data_Pi_Contract);
        pstl0495_Data_Pi_Contract_A = pstl0495_Data_Pi_ContractRedef5.newFieldArrayInGroup("pstl0495_Data_Pi_Contract_A", "PI-CONTRACT-A", FieldType.STRING, 
            10, new DbsArrayController(1,10));
        pstl0495_Data_Pi_Plan_Id = pstl0495_Data_Power_Image_Task_Information.newFieldInGroup("pstl0495_Data_Pi_Plan_Id", "PI-PLAN-ID", FieldType.STRING, 
            6);
        pstl0495_Data_Pi_Doc_Content = pstl0495_Data_Power_Image_Task_Information.newFieldInGroup("pstl0495_Data_Pi_Doc_Content", "PI-DOC-CONTENT", FieldType.STRING, 
            30);
        pstl0495_Data_Pi_Filler_1 = pstl0495_Data_Power_Image_Task_Information.newFieldInGroup("pstl0495_Data_Pi_Filler_1", "PI-FILLER-1", FieldType.STRING, 
            20);
        pstl0495_Data_Pi_Filler_2 = pstl0495_Data_Power_Image_Task_Information.newFieldInGroup("pstl0495_Data_Pi_Filler_2", "PI-FILLER-2", FieldType.STRING, 
            20);
        pstl0495_Data_Pi_Filler_3 = pstl0495_Data_Power_Image_Task_Information.newFieldInGroup("pstl0495_Data_Pi_Filler_3", "PI-FILLER-3", FieldType.STRING, 
            20);
        pstl0495_Data_Pi_Filler_4 = pstl0495_Data_Power_Image_Task_Information.newFieldInGroup("pstl0495_Data_Pi_Filler_4", "PI-FILLER-4", FieldType.STRING, 
            20);
        pstl0495_Data_Pi_Filler_5 = pstl0495_Data_Power_Image_Task_Information.newFieldInGroup("pstl0495_Data_Pi_Filler_5", "PI-FILLER-5", FieldType.STRING, 
            20);
        pstl0495_Data_Pi_End_Literal = pstl0495_Data_Power_Image_Task_Information.newFieldInGroup("pstl0495_Data_Pi_End_Literal", "PI-END-LITERAL", FieldType.STRING, 
            4);

        this.setRecordName("LdaPstl0495");
    }

    public void initializeValues() throws Exception
    {
        reset();
        sort_Key_Sort_Key_Lgth.setInitialValue(1);
        pstl0495_Data_Pstl0495_Rec_Id.setInitialValue("AA");
        pstl0495_Data_Pstl0495_Data_Lgth.setInitialValue(4499);
        pstl0495_Data_Pstl0495_Data_Occurs.setInitialValue(1);
    }

    // Constructor
    public LdaPstl0495() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
