/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:18:12 PM
**        * FROM NATURAL PDA     : BENA9863
************************************************************
**        * FILE NAME            : PdaBena9863.java
**        * CLASS NAME           : PdaBena9863
**        * INSTANCE NAME        : PdaBena9863
************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaBena9863 extends PdaBase
{
    // Properties
    private DbsGroup bena9863;
    private DbsField bena9863_Pnd_Systm_Text;
    private DbsField bena9863_Pnd_Rltnshp_Cde;
    private DbsField bena9863_Pnd_Found;

    public DbsGroup getBena9863() { return bena9863; }

    public DbsField getBena9863_Pnd_Systm_Text() { return bena9863_Pnd_Systm_Text; }

    public DbsField getBena9863_Pnd_Rltnshp_Cde() { return bena9863_Pnd_Rltnshp_Cde; }

    public DbsField getBena9863_Pnd_Found() { return bena9863_Pnd_Found; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        bena9863 = dbsRecord.newGroupInRecord("bena9863", "BENA9863");
        bena9863.setParameterOption(ParameterOption.ByReference);
        bena9863_Pnd_Systm_Text = bena9863.newFieldInGroup("bena9863_Pnd_Systm_Text", "#SYSTM-TEXT", FieldType.STRING, 15);
        bena9863_Pnd_Rltnshp_Cde = bena9863.newFieldInGroup("bena9863_Pnd_Rltnshp_Cde", "#RLTNSHP-CDE", FieldType.STRING, 2);
        bena9863_Pnd_Found = bena9863.newFieldInGroup("bena9863_Pnd_Found", "#FOUND", FieldType.BOOLEAN);

        dbsRecord.reset();
    }

    // Constructors
    public PdaBena9863(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

