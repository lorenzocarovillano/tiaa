/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:18:13 PM
**        * FROM NATURAL PDA     : BENA9870
************************************************************
**        * FILE NAME            : PdaBena9870.java
**        * CLASS NAME           : PdaBena9870
**        * INSTANCE NAME        : PdaBena9870
************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaBena9870 extends PdaBase
{
    // Properties
    private DbsGroup bena9870;
    private DbsField bena9870_Pnd_Cntrct_Isn;
    private DbsField bena9870_Pnd_Mos_Isn;
    private DbsGroup bena9870_Pnd_Dsgntn_Grp;
    private DbsField bena9870_Pnd_Dsgntn_Isn;
    private DbsField bena9870_Pnd_Dsgntn_Rltn_Cde;
    private DbsField bena9870_Pnd_Bsnss_Dte;
    private DbsField bena9870_Pnd_Interface_As_Mos;

    public DbsGroup getBena9870() { return bena9870; }

    public DbsField getBena9870_Pnd_Cntrct_Isn() { return bena9870_Pnd_Cntrct_Isn; }

    public DbsField getBena9870_Pnd_Mos_Isn() { return bena9870_Pnd_Mos_Isn; }

    public DbsGroup getBena9870_Pnd_Dsgntn_Grp() { return bena9870_Pnd_Dsgntn_Grp; }

    public DbsField getBena9870_Pnd_Dsgntn_Isn() { return bena9870_Pnd_Dsgntn_Isn; }

    public DbsField getBena9870_Pnd_Dsgntn_Rltn_Cde() { return bena9870_Pnd_Dsgntn_Rltn_Cde; }

    public DbsField getBena9870_Pnd_Bsnss_Dte() { return bena9870_Pnd_Bsnss_Dte; }

    public DbsField getBena9870_Pnd_Interface_As_Mos() { return bena9870_Pnd_Interface_As_Mos; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        bena9870 = dbsRecord.newGroupInRecord("bena9870", "BENA9870");
        bena9870.setParameterOption(ParameterOption.ByReference);
        bena9870_Pnd_Cntrct_Isn = bena9870.newFieldInGroup("bena9870_Pnd_Cntrct_Isn", "#CNTRCT-ISN", FieldType.PACKED_DECIMAL, 10);
        bena9870_Pnd_Mos_Isn = bena9870.newFieldInGroup("bena9870_Pnd_Mos_Isn", "#MOS-ISN", FieldType.PACKED_DECIMAL, 10);
        bena9870_Pnd_Dsgntn_Grp = bena9870.newGroupArrayInGroup("bena9870_Pnd_Dsgntn_Grp", "#DSGNTN-GRP", new DbsArrayController(1,30));
        bena9870_Pnd_Dsgntn_Isn = bena9870_Pnd_Dsgntn_Grp.newFieldInGroup("bena9870_Pnd_Dsgntn_Isn", "#DSGNTN-ISN", FieldType.PACKED_DECIMAL, 10);
        bena9870_Pnd_Dsgntn_Rltn_Cde = bena9870_Pnd_Dsgntn_Grp.newFieldInGroup("bena9870_Pnd_Dsgntn_Rltn_Cde", "#DSGNTN-RLTN-CDE", FieldType.STRING, 2);
        bena9870_Pnd_Bsnss_Dte = bena9870.newFieldInGroup("bena9870_Pnd_Bsnss_Dte", "#BSNSS-DTE", FieldType.STRING, 8);
        bena9870_Pnd_Interface_As_Mos = bena9870.newFieldInGroup("bena9870_Pnd_Interface_As_Mos", "#INTERFACE-AS-MOS", FieldType.BOOLEAN);

        dbsRecord.reset();
    }

    // Constructors
    public PdaBena9870(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

