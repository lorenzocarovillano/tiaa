/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:18:14 PM
**        * FROM NATURAL PDA     : BENA9878
************************************************************
**        * FILE NAME            : PdaBena9878.java
**        * CLASS NAME           : PdaBena9878
**        * INSTANCE NAME        : PdaBena9878
************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaBena9878 extends PdaBase
{
    // Properties
    private DbsGroup bena9878;
    private DbsGroup bena9878_Pnd_Input_Fields;
    private DbsField bena9878_Pnd_Same_As_Cntrct;
    private DbsField bena9878_Pnd_Cntrct_Is_Cref;
    private DbsGroup bena9878_Pnd_Output_Fields;
    private DbsField bena9878_Pnd_Copy_From_Cntrct;
    private DbsField bena9878_Pnd_Copy_From_Txt;
    private DbsField bena9878_Pnd_Copy_From_Cntrct_Isn;
    private DbsField bena9878_Pnd_Copy_From_Cntrct_Tiaa_Cref_Ind;
    private DbsField bena9878_Pnd_Copy_From_Cntrct_Found;
    private DbsField bena9878_Pnd_Dflt_To_Estte_Flag_Out;
    private DbsField bena9878_Pnd_Copy_From_Cntrct_Eff_Dte;
    private DbsField bena9878_Pnd_Mos_Ind;
    private DbsField bena9878_Pnd_Cor_Fields;
    private DbsGroup bena9878_Pnd_Cor_FieldsRedef1;
    private DbsField bena9878_Pnd_Cntrct_Issue_Dte;
    private DbsField bena9878_Pnd_Cntrct_Lob_Cde;
    private DbsField bena9878_Pnd_Cntrct_Nmbr_Range_Cde;
    private DbsField bena9878_Pnd_Cntrct_Spcl_Cnsdrtn_Cde;
    private DbsField bena9878_Pnd_Cntrct_Cref_Nbr;
    private DbsField bena9878_Pnd_Updt_Last_Dsgtn_Srce;
    private DbsGroup bena9878_Pnd_Error_Fields;
    private DbsField bena9878_Pnd_Error_Msg;
    private DbsField bena9878_Pnd_Error_Pgm;
    private DbsField bena9878_Pnd_Error_Cde;

    public DbsGroup getBena9878() { return bena9878; }

    public DbsGroup getBena9878_Pnd_Input_Fields() { return bena9878_Pnd_Input_Fields; }

    public DbsField getBena9878_Pnd_Same_As_Cntrct() { return bena9878_Pnd_Same_As_Cntrct; }

    public DbsField getBena9878_Pnd_Cntrct_Is_Cref() { return bena9878_Pnd_Cntrct_Is_Cref; }

    public DbsGroup getBena9878_Pnd_Output_Fields() { return bena9878_Pnd_Output_Fields; }

    public DbsField getBena9878_Pnd_Copy_From_Cntrct() { return bena9878_Pnd_Copy_From_Cntrct; }

    public DbsField getBena9878_Pnd_Copy_From_Txt() { return bena9878_Pnd_Copy_From_Txt; }

    public DbsField getBena9878_Pnd_Copy_From_Cntrct_Isn() { return bena9878_Pnd_Copy_From_Cntrct_Isn; }

    public DbsField getBena9878_Pnd_Copy_From_Cntrct_Tiaa_Cref_Ind() { return bena9878_Pnd_Copy_From_Cntrct_Tiaa_Cref_Ind; }

    public DbsField getBena9878_Pnd_Copy_From_Cntrct_Found() { return bena9878_Pnd_Copy_From_Cntrct_Found; }

    public DbsField getBena9878_Pnd_Dflt_To_Estte_Flag_Out() { return bena9878_Pnd_Dflt_To_Estte_Flag_Out; }

    public DbsField getBena9878_Pnd_Copy_From_Cntrct_Eff_Dte() { return bena9878_Pnd_Copy_From_Cntrct_Eff_Dte; }

    public DbsField getBena9878_Pnd_Mos_Ind() { return bena9878_Pnd_Mos_Ind; }

    public DbsField getBena9878_Pnd_Cor_Fields() { return bena9878_Pnd_Cor_Fields; }

    public DbsGroup getBena9878_Pnd_Cor_FieldsRedef1() { return bena9878_Pnd_Cor_FieldsRedef1; }

    public DbsField getBena9878_Pnd_Cntrct_Issue_Dte() { return bena9878_Pnd_Cntrct_Issue_Dte; }

    public DbsField getBena9878_Pnd_Cntrct_Lob_Cde() { return bena9878_Pnd_Cntrct_Lob_Cde; }

    public DbsField getBena9878_Pnd_Cntrct_Nmbr_Range_Cde() { return bena9878_Pnd_Cntrct_Nmbr_Range_Cde; }

    public DbsField getBena9878_Pnd_Cntrct_Spcl_Cnsdrtn_Cde() { return bena9878_Pnd_Cntrct_Spcl_Cnsdrtn_Cde; }

    public DbsField getBena9878_Pnd_Cntrct_Cref_Nbr() { return bena9878_Pnd_Cntrct_Cref_Nbr; }

    public DbsField getBena9878_Pnd_Updt_Last_Dsgtn_Srce() { return bena9878_Pnd_Updt_Last_Dsgtn_Srce; }

    public DbsGroup getBena9878_Pnd_Error_Fields() { return bena9878_Pnd_Error_Fields; }

    public DbsField getBena9878_Pnd_Error_Msg() { return bena9878_Pnd_Error_Msg; }

    public DbsField getBena9878_Pnd_Error_Pgm() { return bena9878_Pnd_Error_Pgm; }

    public DbsField getBena9878_Pnd_Error_Cde() { return bena9878_Pnd_Error_Cde; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        bena9878 = dbsRecord.newGroupInRecord("bena9878", "BENA9878");
        bena9878.setParameterOption(ParameterOption.ByReference);
        bena9878_Pnd_Input_Fields = bena9878.newGroupInGroup("bena9878_Pnd_Input_Fields", "#INPUT-FIELDS");
        bena9878_Pnd_Same_As_Cntrct = bena9878_Pnd_Input_Fields.newFieldArrayInGroup("bena9878_Pnd_Same_As_Cntrct", "#SAME-AS-CNTRCT", FieldType.STRING, 
            10, new DbsArrayController(1,2));
        bena9878_Pnd_Cntrct_Is_Cref = bena9878_Pnd_Input_Fields.newFieldArrayInGroup("bena9878_Pnd_Cntrct_Is_Cref", "#CNTRCT-IS-CREF", FieldType.BOOLEAN, 
            new DbsArrayController(1,2));
        bena9878_Pnd_Output_Fields = bena9878.newGroupInGroup("bena9878_Pnd_Output_Fields", "#OUTPUT-FIELDS");
        bena9878_Pnd_Copy_From_Cntrct = bena9878_Pnd_Output_Fields.newFieldInGroup("bena9878_Pnd_Copy_From_Cntrct", "#COPY-FROM-CNTRCT", FieldType.STRING, 
            10);
        bena9878_Pnd_Copy_From_Txt = bena9878_Pnd_Output_Fields.newFieldInGroup("bena9878_Pnd_Copy_From_Txt", "#COPY-FROM-TXT", FieldType.STRING, 72);
        bena9878_Pnd_Copy_From_Cntrct_Isn = bena9878_Pnd_Output_Fields.newFieldInGroup("bena9878_Pnd_Copy_From_Cntrct_Isn", "#COPY-FROM-CNTRCT-ISN", FieldType.PACKED_DECIMAL, 
            10);
        bena9878_Pnd_Copy_From_Cntrct_Tiaa_Cref_Ind = bena9878_Pnd_Output_Fields.newFieldInGroup("bena9878_Pnd_Copy_From_Cntrct_Tiaa_Cref_Ind", "#COPY-FROM-CNTRCT-TIAA-CREF-IND", 
            FieldType.STRING, 1);
        bena9878_Pnd_Copy_From_Cntrct_Found = bena9878_Pnd_Output_Fields.newFieldInGroup("bena9878_Pnd_Copy_From_Cntrct_Found", "#COPY-FROM-CNTRCT-FOUND", 
            FieldType.BOOLEAN);
        bena9878_Pnd_Dflt_To_Estte_Flag_Out = bena9878_Pnd_Output_Fields.newFieldInGroup("bena9878_Pnd_Dflt_To_Estte_Flag_Out", "#DFLT-TO-ESTTE-FLAG-OUT", 
            FieldType.BOOLEAN);
        bena9878_Pnd_Copy_From_Cntrct_Eff_Dte = bena9878_Pnd_Output_Fields.newFieldInGroup("bena9878_Pnd_Copy_From_Cntrct_Eff_Dte", "#COPY-FROM-CNTRCT-EFF-DTE", 
            FieldType.STRING, 8);
        bena9878_Pnd_Mos_Ind = bena9878_Pnd_Output_Fields.newFieldInGroup("bena9878_Pnd_Mos_Ind", "#MOS-IND", FieldType.STRING, 1);
        bena9878_Pnd_Cor_Fields = bena9878.newFieldInGroup("bena9878_Pnd_Cor_Fields", "#COR-FIELDS", FieldType.STRING, 23);
        bena9878_Pnd_Cor_FieldsRedef1 = bena9878.newGroupInGroup("bena9878_Pnd_Cor_FieldsRedef1", "Redefines", bena9878_Pnd_Cor_Fields);
        bena9878_Pnd_Cntrct_Issue_Dte = bena9878_Pnd_Cor_FieldsRedef1.newFieldInGroup("bena9878_Pnd_Cntrct_Issue_Dte", "#CNTRCT-ISSUE-DTE", FieldType.STRING, 
            8);
        bena9878_Pnd_Cntrct_Lob_Cde = bena9878_Pnd_Cor_FieldsRedef1.newFieldInGroup("bena9878_Pnd_Cntrct_Lob_Cde", "#CNTRCT-LOB-CDE", FieldType.STRING, 
            1);
        bena9878_Pnd_Cntrct_Nmbr_Range_Cde = bena9878_Pnd_Cor_FieldsRedef1.newFieldInGroup("bena9878_Pnd_Cntrct_Nmbr_Range_Cde", "#CNTRCT-NMBR-RANGE-CDE", 
            FieldType.STRING, 2);
        bena9878_Pnd_Cntrct_Spcl_Cnsdrtn_Cde = bena9878_Pnd_Cor_FieldsRedef1.newFieldInGroup("bena9878_Pnd_Cntrct_Spcl_Cnsdrtn_Cde", "#CNTRCT-SPCL-CNSDRTN-CDE", 
            FieldType.STRING, 2);
        bena9878_Pnd_Cntrct_Cref_Nbr = bena9878_Pnd_Cor_FieldsRedef1.newFieldInGroup("bena9878_Pnd_Cntrct_Cref_Nbr", "#CNTRCT-CREF-NBR", FieldType.STRING, 
            10);
        bena9878_Pnd_Updt_Last_Dsgtn_Srce = bena9878.newFieldInGroup("bena9878_Pnd_Updt_Last_Dsgtn_Srce", "#UPDT-LAST-DSGTN-SRCE", FieldType.STRING, 1);
        bena9878_Pnd_Error_Fields = bena9878.newGroupInGroup("bena9878_Pnd_Error_Fields", "#ERROR-FIELDS");
        bena9878_Pnd_Error_Msg = bena9878_Pnd_Error_Fields.newFieldInGroup("bena9878_Pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 72);
        bena9878_Pnd_Error_Pgm = bena9878_Pnd_Error_Fields.newFieldInGroup("bena9878_Pnd_Error_Pgm", "#ERROR-PGM", FieldType.STRING, 8);
        bena9878_Pnd_Error_Cde = bena9878_Pnd_Error_Fields.newFieldInGroup("bena9878_Pnd_Error_Cde", "#ERROR-CDE", FieldType.STRING, 5);

        dbsRecord.reset();
    }

    // Constructors
    public PdaBena9878(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

