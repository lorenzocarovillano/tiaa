/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:18:10 PM
**        * FROM NATURAL PDA     : BENA9817
************************************************************
**        * FILE NAME            : PdaBena9817.java
**        * CLASS NAME           : PdaBena9817
**        * INSTANCE NAME        : PdaBena9817
************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaBena9817 extends PdaBase
{
    // Properties
    private DbsGroup bena9817;
    private DbsField bena9817_Pnd_Cntrct_Isn;
    private DbsField bena9817_Pnd_Pin_Tiaa_Cntrct;
    private DbsGroup bena9817_Pnd_Pin_Tiaa_CntrctRedef1;
    private DbsField bena9817_Pnd_Pin;
    private DbsField bena9817_Pnd_Tiaa_Cntrct;
    private DbsField bena9817_Pnd_Tiaa_Cref_Ind;
    private DbsField bena9817_Pnd_Rcrd_Crtd_For_Bsnss_Dte;
    private DbsField bena9817_Pnd_Mos_Ind;
    private DbsField bena9817_Pnd_Intrfce_Stts;
    private DbsField bena9817_Pnd_New_Pin;

    public DbsGroup getBena9817() { return bena9817; }

    public DbsField getBena9817_Pnd_Cntrct_Isn() { return bena9817_Pnd_Cntrct_Isn; }

    public DbsField getBena9817_Pnd_Pin_Tiaa_Cntrct() { return bena9817_Pnd_Pin_Tiaa_Cntrct; }

    public DbsGroup getBena9817_Pnd_Pin_Tiaa_CntrctRedef1() { return bena9817_Pnd_Pin_Tiaa_CntrctRedef1; }

    public DbsField getBena9817_Pnd_Pin() { return bena9817_Pnd_Pin; }

    public DbsField getBena9817_Pnd_Tiaa_Cntrct() { return bena9817_Pnd_Tiaa_Cntrct; }

    public DbsField getBena9817_Pnd_Tiaa_Cref_Ind() { return bena9817_Pnd_Tiaa_Cref_Ind; }

    public DbsField getBena9817_Pnd_Rcrd_Crtd_For_Bsnss_Dte() { return bena9817_Pnd_Rcrd_Crtd_For_Bsnss_Dte; }

    public DbsField getBena9817_Pnd_Mos_Ind() { return bena9817_Pnd_Mos_Ind; }

    public DbsField getBena9817_Pnd_Intrfce_Stts() { return bena9817_Pnd_Intrfce_Stts; }

    public DbsField getBena9817_Pnd_New_Pin() { return bena9817_Pnd_New_Pin; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        bena9817 = dbsRecord.newGroupInRecord("bena9817", "BENA9817");
        bena9817.setParameterOption(ParameterOption.ByReference);
        bena9817_Pnd_Cntrct_Isn = bena9817.newFieldInGroup("bena9817_Pnd_Cntrct_Isn", "#CNTRCT-ISN", FieldType.PACKED_DECIMAL, 10);
        bena9817_Pnd_Pin_Tiaa_Cntrct = bena9817.newFieldInGroup("bena9817_Pnd_Pin_Tiaa_Cntrct", "#PIN-TIAA-CNTRCT", FieldType.STRING, 22);
        bena9817_Pnd_Pin_Tiaa_CntrctRedef1 = bena9817.newGroupInGroup("bena9817_Pnd_Pin_Tiaa_CntrctRedef1", "Redefines", bena9817_Pnd_Pin_Tiaa_Cntrct);
        bena9817_Pnd_Pin = bena9817_Pnd_Pin_Tiaa_CntrctRedef1.newFieldInGroup("bena9817_Pnd_Pin", "#PIN", FieldType.NUMERIC, 12);
        bena9817_Pnd_Tiaa_Cntrct = bena9817_Pnd_Pin_Tiaa_CntrctRedef1.newFieldInGroup("bena9817_Pnd_Tiaa_Cntrct", "#TIAA-CNTRCT", FieldType.STRING, 10);
        bena9817_Pnd_Tiaa_Cref_Ind = bena9817.newFieldInGroup("bena9817_Pnd_Tiaa_Cref_Ind", "#TIAA-CREF-IND", FieldType.STRING, 1);
        bena9817_Pnd_Rcrd_Crtd_For_Bsnss_Dte = bena9817.newFieldInGroup("bena9817_Pnd_Rcrd_Crtd_For_Bsnss_Dte", "#RCRD-CRTD-FOR-BSNSS-DTE", FieldType.STRING, 
            8);
        bena9817_Pnd_Mos_Ind = bena9817.newFieldInGroup("bena9817_Pnd_Mos_Ind", "#MOS-IND", FieldType.STRING, 1);
        bena9817_Pnd_Intrfce_Stts = bena9817.newFieldInGroup("bena9817_Pnd_Intrfce_Stts", "#INTRFCE-STTS", FieldType.STRING, 1);
        bena9817_Pnd_New_Pin = bena9817.newFieldInGroup("bena9817_Pnd_New_Pin", "#NEW-PIN", FieldType.NUMERIC, 12);

        dbsRecord.reset();
    }

    // Constructors
    public PdaBena9817(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

