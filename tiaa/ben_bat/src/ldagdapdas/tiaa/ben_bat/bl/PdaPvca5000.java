/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:23:03 PM
**        * FROM NATURAL PDA     : PVCA5000
************************************************************
**        * FILE NAME            : PdaPvca5000.java
**        * CLASS NAME           : PdaPvca5000
**        * INSTANCE NAME        : PdaPvca5000
************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaPvca5000 extends PdaBase
{
    // Properties
    private DbsGroup pvca5000;
    private DbsField pvca5000_Pvca5000_Array;
    private DbsGroup pvca5000_Pvca5000_ArrayRedef1;
    private DbsField pvca5000_Pnd_Result_Code;
    private DbsField pvca5000_Pnd_Result_Msg;
    private DbsField pvca5000_Pnd_More_Data;
    private DbsField pvca5000_Pnd_A_Universal_Id;
    private DbsField pvca5000_Pnd_A_Universal_Id_Type;
    private DbsField pvca5000_Pnd_A_Requestor_Title;
    private DbsField pvca5000_Pnd_A_Requestor_Suffix;
    private DbsField pvca5000_Pnd_A_Requestor_Trust_Name_1;
    private DbsField pvca5000_Pnd_A_Requestor_Trust_Name_2;
    private DbsField pvca5000_Pnd_A_Requestor_Name;
    private DbsField pvca5000_Pnd_A_Requestor_Addr1;
    private DbsField pvca5000_Pnd_A_Requestor_Addr2;
    private DbsField pvca5000_Pnd_A_Requestor_Addr3;
    private DbsField pvca5000_Pnd_A_Requestor_Addr4;
    private DbsField pvca5000_Pnd_A_Requestor_Addr5;
    private DbsField pvca5000_Pnd_A_Requestor_City;
    private DbsField pvca5000_Pnd_A_Requestor_Zip;
    private DbsField pvca5000_Pnd_A_Requestor_State;
    private DbsField pvca5000_Pnd_A_Requestor_Addr_Typ;
    private DbsField pvca5000_Pnd_A_Requestor_Country;
    private DbsField pvca5000_Pnd_A_Requestor_Trust_Or_Est;
    private DbsField pvca5000_Pnd_A_Requestor_Care;
    private DbsField pvca5000_Pnd_A_Altr_Requestor_Title;
    private DbsField pvca5000_Pnd_A_Altr_Requestor_Suffix;
    private DbsField pvca5000_Pnd_A_Altr_Requestor_Name;
    private DbsField pvca5000_Pnd_A_Altr_Requestor_Addr1;
    private DbsField pvca5000_Pnd_A_Altr_Requestor_Addr2;
    private DbsField pvca5000_Pnd_A_Altr_Requestor_City;
    private DbsField pvca5000_Pnd_A_Altr_Requestor_Zip;
    private DbsField pvca5000_Pnd_A_Altr_Requestor_State;
    private DbsField pvca5000_Pnd_A_Altr_Requestor_Addr_Typ;
    private DbsField pvca5000_Pnd_A_Altr_Requestor_Country;
    private DbsField pvca5000_Pnd_A_Altr_Requestor_Trust_Or_Est;
    private DbsField pvca5000_Pnd_A_Altr_Requestor_Care;
    private DbsField pvca5000_Pnd_A_Salutation_Text;
    private DbsField pvca5000_Pnd_A_Local_Printer;
    private DbsField pvca5000_Pnd_A_Immediate_Printer_Ind;
    private DbsField pvca5000_Pnd_A_Signatory_Racf;
    private DbsField pvca5000_Pnd_A_Signatory_Racf_Unit;
    private DbsField pvca5000_Pnd_A_Signatory_Racf_Name;
    private DbsField pvca5000_Pnd_A_Letter_Type;
    private DbsField pvca5000_Pnd_A_Case_Manager_Name;
    private DbsField pvca5000_Pnd_A_Case_Manage_Phone;
    private DbsField pvca5000_Pnd_A_Institution_Name;
    private DbsField pvca5000_Pnd_A_Decedent_Pin_Nbr;
    private DbsField pvca5000_Pnd_A_Decedent_Other_Than_Org;
    private DbsField pvca5000_Pnd_A_Decedent_Name;
    private DbsField pvca5000_Pnd_A_Decedent_Date_Of_Birth;
    private DbsField pvca5000_Pnd_A_Decedent_Date_Of_Death;
    private DbsField pvca5000_Pnd_A_Cont_Beneficiary_Name;
    private DbsField pvca5000_Pnd_A_Minor_Org_Estate_Name;
    private DbsField pvca5000_Pnd_A_Trust_Name;
    private DbsField pvca5000_Pnd_A_Children_Drop_Down;
    private DbsField pvca5000_Pnd_A_Pages_Missing;
    private DbsField pvca5000_Pnd_A_Death_Cert_Bene_Names;
    private DbsField pvca5000_Pnd_A_Pvc_Delivery_Type;
    private DbsField pvca5000_Pnd_A_Survivor_Task_Category;
    private DbsField pvca5000_Pnd_A_Unclaimed_Funds_State_Name;
    private DbsField pvca5000_Pnd_A_Unclaimed_Funds_State_Cde;
    private DbsField pvca5000_Pnd_A_Unclaimed_Funds_Contract_Nbr;
    private DbsField pvca5000_Pnd_A_Brm_Name;
    private DbsField pvca5000_Pnd_A_Footer_Location;
    private DbsField pvca5000_Pnd_A_Renun_Waiver_Release_Ind;
    private DbsField pvca5000_Pnd_A_Marital_Stat_Addendum_Ind;
    private DbsField pvca5000_Pnd_A_Release_Indem_Agree_Ind;
    private DbsField pvca5000_Pnd_A_Savings_Invest_Appl_Ind;
    private DbsField pvca5000_Pnd_A_Survivor_Bene_Dist_Rqst_Ind;
    private DbsField pvca5000_Pnd_A_Income_Annty_Request_Form_Ind;
    private DbsField pvca5000_Pnd_A_Immediate_Annuity_Form_Ind;
    private DbsField pvca5000_Pnd_A_One_Time_Pay_Request_Form_Ind;
    private DbsField pvca5000_Pnd_A_Da_Tpa_Desig_Form_Ind;
    private DbsField pvca5000_Pnd_A_Da_Tpa_Desig_Form_Ctr;
    private DbsField pvca5000_Pnd_A_Ia_Beneficiary_Form_Ind;
    private DbsField pvca5000_Pnd_A_Ia_Beneficiary_Form_Ctr;
    private DbsField pvca5000_Pnd_A_Ohio_Arp_Bene_Form_Ind;
    private DbsField pvca5000_Pnd_A_Ohio_Arp_Bene_Form_Ctr;
    private DbsField pvca5000_Pnd_A_Univ_Pittsburgh_Bene_Form_Ind;
    private DbsField pvca5000_Pnd_A_Univ_Pittsburgh_Bene_Form_Ctr;
    private DbsField pvca5000_Pnd_A_Univ_Michigan_Bene_Form_Ind;
    private DbsField pvca5000_Pnd_A_Univ_Michigan_Bene_Form_Ctr;
    private DbsField pvca5000_Pnd_A_Successr_Beneficiary_Form_Ind;
    private DbsField pvca5000_Pnd_A_Successr_Beneficiary_Form_Ctr;
    private DbsField pvca5000_Pnd_A_Ohio_Consent_Ind;
    private DbsField pvca5000_Pnd_A_Oklahoma_Consent_Ind;
    private DbsField pvca5000_Pnd_A_Indiana_Notice_Ind;
    private DbsField pvca5000_Pnd_A_Rhode_Island_Notice_Ind;
    private DbsField pvca5000_Pnd_A_W9_Form_Ind;
    private DbsField pvca5000_Pnd_A_W8ben_Form_Ind;
    private DbsField pvca5000_Pnd_A_W7_Form_Ind;
    private DbsField pvca5000_Pnd_A_State_Code_Taxint;
    private DbsField pvca5000_Pnd_A_State_Name_Taxint;
    private DbsField pvca5000_Pnd_A_State_Name_Text;
    private DbsField pvca5000_Pnd_A_Loved_One_Passes_Away_Ind;
    private DbsField pvca5000_Pnd_A_Sip_Booklet_Ind;
    private DbsField pvca5000_Pnd_A_Taking_Next_Step_Ind;
    private DbsField pvca5000_Pnd_A_Qtrly_Performance_Ind;
    private DbsField pvca5000_Pnd_A_Tiaa_Cref_Prospectus_Ind;
    private DbsField pvca5000_Pnd_A_Cref_Prospectus_Ind;
    private DbsField pvca5000_Pnd_A_Tiaa_Rea_Prospectus_Ind;
    private DbsField pvca5000_Pnd_A_Lifecycle_Prospectus_Ind;
    private DbsField pvca5000_Pnd_A_Tiaa_Rate_Sheet_Ind;
    private DbsField pvca5000_Pnd_A_Atra_Sip_Glance_Indvl_Ind;
    private DbsField pvca5000_Pnd_A_Atra_Ssum_Glance_Trust_Ind;
    private DbsField pvca5000_Pnd_A_Atra_Indvl_Bene_Glance_Ind;
    private DbsField pvca5000_Pnd_A_Atra_Ssum_Glance_Estate_Ind;
    private DbsField pvca5000_Pnd_A_Atra_Annty_Glance_Indvl_Ind;
    private DbsField pvca5000_Pnd_A_Atra_Fix_Prd_Ann_Glance_Ind;
    private DbsField pvca5000_Pnd_A_Ssum_Glance_Nonspouse_Ind;
    private DbsField pvca5000_Pnd_A_Ssum_Glance_Spouse_Ind;
    private DbsField pvca5000_Pnd_A_Ssum_Glance_Trust_Ind;
    private DbsField pvca5000_Pnd_A_Ssum_Glance_Estate_Ind;
    private DbsField pvca5000_Pnd_A_Notice_402f_Ind;
    private DbsField pvca5000_Pnd_A_Custodial_Agreement_Ind;
    private DbsField pvca5000_Pnd_A_Record_Of_Age_Ind;
    private DbsField pvca5000_Pnd_A_Payment_Dest_Form_Ind;
    private DbsField pvca5000_Pnd_A_Transfer_At_Settlement_Ind;
    private DbsField pvca5000_Pnd_A_Doma_Ind;
    private DbsField pvca5000_Pnd_A_Minor_Sa;
    private DbsField pvca5000_Pnd_A_My_Children_Ind;
    private DbsField pvca5000_Pnd_A_Minor_Guardian_Hip_Ind;
    private DbsField pvca5000_Pnd_A_Exspouse_Divorce_Decree_Ind;
    private DbsField pvca5000_Pnd_A_Estate_Ind;
    private DbsField pvca5000_Pnd_A_Small_Estate_Ind;
    private DbsField pvca5000_Pnd_A_Unable_To_Locate_Trust_Ind;
    private DbsField pvca5000_Pnd_A_Need_Bene_Address_Ind;
    private DbsField pvca5000_Pnd_A_Organization_Ind;
    private DbsField pvca5000_Pnd_A_Msv_Ind;
    private DbsField pvca5000_Pnd_A_Roth_Accumulation_Ind;
    private DbsField pvca5000_Pnd_A_Atra_Accumulation_Ind;
    private DbsField pvca5000_Pnd_A_Investment_Contract_Ind;
    private DbsField pvca5000_Pnd_A_Allocation_Ind;
    private DbsField pvca5000_Pnd_A_Blank_Beneficiaries_Ind;
    private DbsField pvca5000_Pnd_A_Contract_Numbers_Ind;
    private DbsField pvca5000_Pnd_A_Invalid_Date_Ind;
    private DbsField pvca5000_Pnd_A_Estate_Trust_Ind;
    private DbsField pvca5000_Pnd_A_Executor_Ind;
    private DbsField pvca5000_Pnd_A_Fax_Copies_Ind;
    private DbsField pvca5000_Pnd_A_Guarantee_Period_Expire_Ind;
    private DbsField pvca5000_Pnd_A_No_Guarantee_Period_Ind;
    private DbsField pvca5000_Pnd_A_Indvl_Bene_Trust_Ind;
    private DbsField pvca5000_Pnd_A_Institution_Maintained_Ind;
    private DbsField pvca5000_Pnd_A_Irrevocable_Designation_Ind;
    private DbsField pvca5000_Pnd_A_Missing_Pages_Ind;
    private DbsField pvca5000_Pnd_A_Inactive_Contracts_Ind;
    private DbsField pvca5000_Pnd_A_Or_Used_In_Designation_Ind;
    private DbsField pvca5000_Pnd_A_Same_Primary_Contingent_Ind;
    private DbsField pvca5000_Pnd_A_Siblings_Ind;
    private DbsField pvca5000_Pnd_A_No_Signature_Date_Ind;
    private DbsField pvca5000_Pnd_A_Spouse_As_Primary_Bene_Ind;
    private DbsField pvca5000_Pnd_A_Surs_Ind;
    private DbsField pvca5000_Pnd_A_Age_Stipulation_Ind;
    private DbsField pvca5000_Pnd_A_Deceased_Child_Chk_Gchild_Ind;
    private DbsField pvca5000_Pnd_A_Dec_Child_Bxchk_Schild_Ind;
    private DbsField pvca5000_Pnd_A_Difficult_To_Read_Ind;
    private DbsField pvca5000_Pnd_A_Divesting_Ind;
    private DbsField pvca5000_Pnd_A_For_The_Benefit_Of_Ind;
    private DbsField pvca5000_Pnd_A_Guardian_Ind;
    private DbsField pvca5000_Pnd_A_Mdo_Contract_Ind;
    private DbsField pvca5000_Pnd_A_Missing_Information_Ind;
    private DbsField pvca5000_Pnd_A_Department_Ind;
    private DbsField pvca5000_Pnd_A_No_Primary_Bene_Ind;
    private DbsField pvca5000_Pnd_A_Second_Annuitant_Named_Ind;
    private DbsField pvca5000_Pnd_A_Ssn_Not_Shown_Ind;
    private DbsField pvca5000_Pnd_A_Spouse_Another_Person_Ind;
    private DbsField pvca5000_Pnd_A_Spousal_Waiver_Inc_Ind;
    private DbsField pvca5000_Pnd_A_Survivorship_Clause;
    private DbsField pvca5000_Pnd_A_Tin_Ind;
    private DbsField pvca5000_Pnd_A_Terms_Of_Trust_Ind;
    private DbsField pvca5000_Pnd_A_Trust_Date_Ind;
    private DbsField pvca5000_Pnd_A_Unborn_Children_Ind;
    private DbsField pvca5000_Pnd_A_Unnamed_Spouse_Of_Bene_Ind;
    private DbsField pvca5000_Pnd_A_No_Designation_Found_Ind;
    private DbsField pvca5000_Pnd_A_Poor_Copy_Ind;
    private DbsField pvca5000_Pnd_A_Retirement_Loan_Ind;
    private DbsField pvca5000_Pnd_A_Wpahs_Ind;
    private DbsField pvca5000_Pnd_A_Erisa_Ind;
    private DbsField pvca5000_Pnd_A_Death_Certificate_Reqrd_Ind;
    private DbsField pvca5000_Pnd_A_Death_Cert_Primary_Bene_Ind;
    private DbsField pvca5000_Pnd_A_Statement_Will_Ind;
    private DbsField pvca5000_Pnd_A_Statement_Trust_In_Effect_Ind;
    private DbsField pvca5000_Pnd_A_Statement_Trust_Accept_Ind;
    private DbsField pvca5000_Pnd_A_Taxpayer_Id_Nbr_Ind;
    private DbsField pvca5000_Pnd_A_Letters_Of_Appointment_Ind;
    private DbsField pvca5000_Pnd_A_Small_Accum_Ind;
    private DbsField pvca5000_Pnd_A_Signature_Guarantee_Ind;
    private DbsField pvca5000_Pnd_A_Death_Certificate;
    private DbsField pvca5000_Pnd_A_Reason_Code;
    private DbsField pvca5000_Pnd_A_Reason_Text;
    private DbsField pvca5000_Pnd_A_Non_Spouse_Rmd_Ind;
    private DbsField pvca5000_Pnd_A_Spouse_Rmd_Ind;
    private DbsField pvca5000_Pnd_A_Valuation_Contract_Nbr;
    private DbsField pvca5000_Pnd_A_Value_Of_Contract;
    private DbsField pvca5000_Pnd_A_Total_At_Valuation_Date;
    private DbsField pvca5000_Pnd_A_Alternate_Valuation_Date;
    private DbsField pvca5000_Pnd_A_Investment_In_Contract;
    private DbsField pvca5000_Pnd_A_Account_Name;
    private DbsField pvca5000_Pnd_A_Account_Amount;
    private DbsField pvca5000_Pnd_A_Entitlement_Total_Amount;
    private DbsField pvca5000_Pnd_A_Roth_Accumulation;
    private DbsField pvca5000_Pnd_A_Atra_Accumulation;
    private DbsField pvca5000_Pnd_A_Invest_And_Contract_Amount;
    private DbsField pvca5000_Pnd_A_Tiaa_Nbr;
    private DbsField pvca5000_Pnd_A_Cref_Nbr;
    private DbsField pvca5000_Pnd_A_Bi_Bene_Type;
    private DbsField pvca5000_Pnd_A_Bi_Bene_Name;
    private DbsField pvca5000_Pnd_A_Bi_Bene_Trustee_Name;
    private DbsField pvca5000_Pnd_A_Bi_Street;
    private DbsField pvca5000_Pnd_A_Bi_City;
    private DbsField pvca5000_Pnd_A_Bi_State;
    private DbsField pvca5000_Pnd_A_Bi_Zip;
    private DbsField pvca5000_Pnd_A_Bi_Primary_Phone;
    private DbsField pvca5000_Pnd_A_Bi_Alt_Phone;
    private DbsField pvca5000_Pnd_A_Bi_Email_Address;
    private DbsField pvca5000_Pnd_A_Bi_State_Legal_Residency;
    private DbsField pvca5000_Pnd_A_Bi_Citizenship;
    private DbsField pvca5000_Pnd_A_Bi_Dob_Trust_Amend_Date;
    private DbsField pvca5000_Pnd_A_Bi_Gender;
    private DbsField pvca5000_Pnd_A_Bi_Bene_Relation_Decedent;
    private DbsField pvca5000_Pnd_A_Ct_Trustee_Name;
    private DbsField pvca5000_Pnd_A_Ct_Trustee_Dob;
    private DbsField pvca5000_Pnd_A_Ct_Trustee_Relation_Decedent;
    private DbsField pvca5000_Pnd_A_So_Option_Type;
    private DbsField pvca5000_Pnd_A_So_Partial_Cash_Amt;
    private DbsField pvca5000_Pnd_A_So_Partial_Cash_Pct;
    private DbsField pvca5000_Pnd_A_So_Ann_Option_Type;
    private DbsField pvca5000_Pnd_A_So_Ann_Partial_Cash_Amt;
    private DbsField pvca5000_Pnd_A_So_Ann_Partial_Cash_Pct;
    private DbsField pvca5000_Pnd_A_Ss_Option_Type;
    private DbsField pvca5000_Pnd_A_Ss_Partial_Cash_Amt;
    private DbsField pvca5000_Pnd_A_Ss_Partial_Cash_Pct;
    private DbsField pvca5000_Pnd_A_Ss_Rollover_Acct_Status;
    private DbsField pvca5000_Pnd_A_Ss_Rollover_Acct_Type;
    private DbsField pvca5000_Pnd_A_Ss_Rollover_Acct_Nbr;
    private DbsField pvca5000_Pnd_A_Ss_Tiaacref_Acct_Status;
    private DbsField pvca5000_Pnd_A_Ss_Tiaacref_Acct_Type;
    private DbsField pvca5000_Pnd_A_Ss_Tiaacref_Acct_Nbr;
    private DbsField pvca5000_Pnd_A_Sn_Option_Type;
    private DbsField pvca5000_Pnd_A_Sn_Partial_Cash_Amt;
    private DbsField pvca5000_Pnd_A_Sn_Partial_Cash_Pct;
    private DbsField pvca5000_Pnd_A_Sn_Rollover_Acct_Status;
    private DbsField pvca5000_Pnd_A_Sn_Rollover_Acct_Type;
    private DbsField pvca5000_Pnd_A_Sn_Rollover_Acct_Nbr;
    private DbsField pvca5000_Pnd_A_Sn_Tiaacref_Acct_Status;
    private DbsField pvca5000_Pnd_A_Sn_Tiaacref_Acct_Type;
    private DbsField pvca5000_Pnd_A_Sn_Tiaacref_Acct_Nbr;
    private DbsField pvca5000_Pnd_A_Sn_Md_Olest_Bene_Dob;
    private DbsField pvca5000_Pnd_A_Cf_Mmdd;
    private DbsField pvca5000_Pnd_A_Cf_Freq;
    private DbsField pvca5000_Pnd_A_Tr_Rollover_Type;
    private DbsField pvca5000_Pnd_A_Tr_O1_Trad_Ira_Nbr;
    private DbsField pvca5000_Pnd_A_Tr_O3_Company_Name;
    private DbsField pvca5000_Pnd_A_Tr_O3_Company_Address;
    private DbsField pvca5000_Pnd_A_Tr_O3_Company_City;
    private DbsField pvca5000_Pnd_A_Tr_O3_Company_State;
    private DbsField pvca5000_Pnd_A_Tr_O3_Company_Zip;
    private DbsField pvca5000_Pnd_A_Tr_O3_Company_Phone;
    private DbsField pvca5000_Pnd_A_Tr_O3_Company_Acct_Nbr;
    private DbsField pvca5000_Pnd_A_Ri_Option_Cde;
    private DbsField pvca5000_Pnd_A_Ri_Invest_Company_Name;
    private DbsField pvca5000_Pnd_A_Ri_Invest_Company_Address;
    private DbsField pvca5000_Pnd_A_Ri_Invest_Company_City;
    private DbsField pvca5000_Pnd_A_Ri_Invest_Company_State;
    private DbsField pvca5000_Pnd_A_Ri_Invest_Company_Zip;
    private DbsField pvca5000_Pnd_A_Ri_Invest_Company_Phone;
    private DbsField pvca5000_Pnd_A_Ri_Invest_Company_Acct_Nbr;
    private DbsField pvca5000_Pnd_A_Ri_Ann_Option_Cde;
    private DbsField pvca5000_Pnd_A_Ri_Ann_Invest_Company_Name;
    private DbsField pvca5000_Pnd_A_Ri_Ann_Invest_Company_Address;
    private DbsField pvca5000_Pnd_A_Ri_Ann_Invest_Company_City;
    private DbsField pvca5000_Pnd_A_Ri_Ann_Invest_Company_State;
    private DbsField pvca5000_Pnd_A_Ri_Ann_Invest_Company_Zip;
    private DbsField pvca5000_Pnd_A_Ri_Ann_Invest_Company_Phone;
    private DbsField pvca5000_Pnd_A_Ri_Ann_Invt_Company_Acct_Nbr;
    private DbsField pvca5000_Pnd_A_Ro_Roth_Ind;
    private DbsField pvca5000_Pnd_A_Ro_Roth_Amt;
    private DbsField pvca5000_Pnd_A_Ro_Roth_Pct;
    private DbsField pvca5000_Pnd_A_Ft_Whold_Type;
    private DbsField pvca5000_Pnd_A_Ft_Da_Dollar_Amt;
    private DbsField pvca5000_Pnd_A_Ft_Pc_Percent;
    private DbsField pvca5000_Pnd_A_Ft_Ann_Whold_Type;
    private DbsField pvca5000_Pnd_A_Ft_Ann_Da_Dollar_Amt;
    private DbsField pvca5000_Pnd_A_Ft_Ann_Pc_Percent;
    private DbsField pvca5000_Pnd_A_Ft_Re_Dollar_Amt;
    private DbsField pvca5000_Pnd_A_Ft_Re_Percent;
    private DbsField pvca5000_Pnd_A_Ft_N2_Dollar_Amt;
    private DbsField pvca5000_Pnd_A_Ft_N2_Percent;
    private DbsField pvca5000_Pnd_A_Ft_N3_Status;
    private DbsField pvca5000_Pnd_A_Ft_N3_Nbr_Whold_Allow;
    private DbsField pvca5000_Pnd_A_Ft_Op_Dollar_Amt;
    private DbsField pvca5000_Pnd_A_Ft_Cv_Dollar_Amt;
    private DbsField pvca5000_Pnd_A_Ft_Cv_Percent;
    private DbsField pvca5000_Pnd_A_Bl_Primary_Name;
    private DbsField pvca5000_Pnd_A_Bl_Primary_Pct;
    private DbsField pvca5000_Pnd_A_Bl_Primary_Dob;
    private DbsField pvca5000_Pnd_A_Bl_Primary_Relation;
    private DbsField pvca5000_Pnd_A_Bl_Contingent_Name;
    private DbsField pvca5000_Pnd_A_Bl_Contingent_Pct;
    private DbsField pvca5000_Pnd_A_Bl_Contingent_Dob;
    private DbsField pvca5000_Pnd_A_Bl_Contingent_Relation;
    private DbsField pvca5000_Pnd_A_Di_Option_Cde;
    private DbsField pvca5000_Pnd_A_Di_O2_Acct_Type;
    private DbsField pvca5000_Pnd_A_Di_O2_Amt;
    private DbsField pvca5000_Pnd_A_Di_O2_Invest_Bank_Name;
    private DbsField pvca5000_Pnd_A_Di_O2_Address;
    private DbsField pvca5000_Pnd_A_Di_O2_City;
    private DbsField pvca5000_Pnd_A_Di_O2_State;
    private DbsField pvca5000_Pnd_A_Di_O2_Zip;
    private DbsField pvca5000_Pnd_A_Di_O2_Bank_Acct_Phone;
    private DbsField pvca5000_Pnd_A_Di_O2_Bank_Acct_Nbr;
    private DbsField pvca5000_Pnd_A_Di_O2_Bank_Routing_Nbr;
    private DbsField pvca5000_Pnd_A_Di_O3_Address_Type;
    private DbsField pvca5000_Pnd_A_Di_O3_Address;
    private DbsField pvca5000_Pnd_A_Di_O3_City;
    private DbsField pvca5000_Pnd_A_Di_O3_State;
    private DbsField pvca5000_Pnd_A_Di_O3_Zip;
    private DbsField pvca5000_Pnd_A_Di_O4_Roth_Ira_Acct;
    private DbsField pvca5000_Pnd_A_Am_Option_Cde;
    private DbsField pvca5000_Pnd_A_Am_O1_Payment_Years;
    private DbsField pvca5000_Pnd_A_Am_O1_Payment_Freq;
    private DbsField pvca5000_Pnd_A_Am_O3_Guaranteed_Prd;
    private DbsField pvca5000_Pnd_A_Am_O3_Payment_Freq;
    private DbsField pvca5000_Pnd_A_Po_Option_Cde;
    private DbsField pvca5000_Pnd_A_Po_Tiaa_Traditional_Ind;
    private DbsField pvca5000_Pnd_A_Po_Cref_Bond_Market_Ind;
    private DbsField pvca5000_Pnd_A_Po_Tiaa_Real_Estate_Ind;
    private DbsField pvca5000_Pnd_A_Po_Cref_Glbl_Equity_Ind;
    private DbsField pvca5000_Pnd_A_Po_Cref_Stock_Ind;
    private DbsField pvca5000_Pnd_A_Po_Cref_Growth_Ind;
    private DbsField pvca5000_Pnd_A_Po_Cref_Money_Mrkt_Ind;
    private DbsField pvca5000_Pnd_A_Po_Cref_Equity_Ndx_Ind;
    private DbsField pvca5000_Pnd_A_Po_Cref_Social_Chc_Ind;
    private DbsField pvca5000_Pnd_A_Po_Cref_Inf_Linked_Ind;
    private DbsField pvca5000_Pnd_A_Pm_Option_Cde;
    private DbsField pvca5000_Pnd_A_Pm_O1_This_Amt;
    private DbsField pvca5000_Pnd_A_Pm_O1_Graded_Prd_Amt;
    private DbsField pvca5000_Pnd_A_Pm_O4_Freq;
    private DbsField pvca5000_Pnd_A_Pm_O4_Start_Life_Income_Dte;
    private DbsField pvca5000_Pnd_A_Pm_O4_Payment_Freq;
    private DbsField pvca5000_Pnd_A_Rw_Approx_Retire_Benefits;
    private DbsField pvca5000_Pnd_A_Rw_Asof_Date;
    private DbsField pvca5000_Pnd_A_Pi_Begin_Literal;
    private DbsField pvca5000_Pnd_A_Pi_Export_Ind;
    private DbsField pvca5000_Pnd_A_Pi_Task_Id;
    private DbsField pvca5000_Pnd_A_Pi_Task_Type;
    private DbsField pvca5000_Pnd_A_Pi_Task_Guid;
    private DbsField pvca5000_Pnd_A_Pi_Action_Step;
    private DbsField pvca5000_Pnd_A_Pi_Tiaa_Full_Date;
    private DbsField pvca5000_Pnd_A_Pi_Tiaa_Time;
    private DbsField pvca5000_Pnd_A_Pi_Task_Status;
    private DbsField pvca5000_Pnd_A_Pi_Ssn;
    private DbsField pvca5000_Pnd_A_Pi_Pin_Npin_Ppg;
    private DbsField pvca5000_Pnd_A_Pi_Pin_Type;
    private DbsField pvca5000_Pnd_A_Pi_Contract;
    private DbsField pvca5000_Pnd_A_Pi_Plan_Id;
    private DbsField pvca5000_Pnd_A_Pi_Doc_Content;
    private DbsField pvca5000_Pnd_A_Pi_Filler_1;
    private DbsField pvca5000_Pnd_A_Pi_Filler_2;
    private DbsField pvca5000_Pnd_A_Pi_Filler_3;
    private DbsField pvca5000_Pnd_A_Pi_Filler_4;
    private DbsField pvca5000_Pnd_A_Pi_Filler_5;
    private DbsField pvca5000_Pnd_A_Pi_End_Literal;
    private DbsField pvca5000_Pnd_A_Periodic_Ind;
    private DbsField pvca5000_Pnd_A_Rollover_Ind;
    private DbsField pvca5000_Pnd_A_Wrong_Form_Ind;
    private DbsField pvca5000_Pnd_A_Blurb_Text;
    private DbsField pvca5000_Pnd_A_Payment_Freq_Ind;
    private DbsField pvca5000_Pnd_A_Payment_Coupon_Ind;
    private DbsField pvca5000_Pnd_A_Payment_Coupon_Tiaa_Amount;
    private DbsField pvca5000_Pnd_A_Payment_Coupon_Cref_Amount;
    private DbsField pvca5000_Pnd_A_Payment_Coupon_Total_Amount;
    private DbsField pvca5000_Pnd_A_Misc_Bene_Prov_Form_Date;
    private DbsField pvca5000_Pnd_A_Benefit_Type_Ind;
    private DbsField pvca5000_Pnd_A_Da_5k_Ind;
    private DbsField pvca5000_Pnd_A_Sip_Tiaa_Nbr;
    private DbsField pvca5000_Pnd_A_Sip_Cref_Nbr;
    private DbsField pvca5000_Pnd_A_Ssum_Tiaa_Nbr;
    private DbsField pvca5000_Pnd_A_Ssum_Cref_Nbr;
    private DbsField pvca5000_Pnd_A_Spouse_Rmd_Bene_Ind;
    private DbsField pvca5000_Pnd_A_Entity_Rmd_Ind;
    private DbsField pvca5000_Pnd_A_Non_Spouse_Rmd_Bene_Ind;
    private DbsField pvca5000_Pnd_A_Second_Gen_Rmd_Ind;
    private DbsField pvca5000_Pnd_A_Phone_Acceptance_Ind;
    private DbsField pvca5000_Pnd_A_Register_Online_Acct_Ind;
    private DbsField pvca5000_Pnd_A_Trust_Relation_Para_Ind;
    private DbsField pvca5000_Pnd_A_Brokerage_Retail_Ind;
    private DbsField pvca5000_Pnd_A_Brokerage_Ira_Ind;
    private DbsField pvca5000_Pnd_A_Brokerage_Ira_Roth_Ind;
    private DbsField pvca5000_Pnd_A_Sole_Spouse_Ira_Rmd_Ind;
    private DbsField pvca5000_Pnd_A_Sole_Spouse_Atra_Rmd_Ind;
    private DbsField pvca5000_Pnd_A_Sole_Spouse_Both_Rmd_Ind;
    private DbsField pvca5000_Pnd_A_Accept_Form_Spouse_Ind;
    private DbsField pvca5000_Pnd_A_Accept_Form_Bene_Ind;
    private DbsField pvca5000_Pnd_A_Accept_Form_Entity_Ind;
    private DbsField pvca5000_Pnd_A_Onetime_Transfer_Spouse_Ind;
    private DbsField pvca5000_Pnd_A_Onetime_Transfer_Bene_Ind;
    private DbsField pvca5000_Pnd_A_Tpa_Cont_Spouse_Ind;
    private DbsField pvca5000_Pnd_A_Tpa_Cont_Bene_Ind;
    private DbsField pvca5000_Pnd_A_Cash_Wdraw_Page_Spouse_Ind;
    private DbsField pvca5000_Pnd_A_Cash_Wdraw_Page_Bene_Ind;
    private DbsField pvca5000_Pnd_A_Rollover_Page_Spouse_Ind;
    private DbsField pvca5000_Pnd_A_Rollover_Page_Bene_Ind;
    private DbsField pvca5000_Pnd_A_Customer_Agree_Retail_Ind;
    private DbsField pvca5000_Pnd_A_Affidavit_Domicile_Ind;
    private DbsField pvca5000_Pnd_A_Cert_Invest_Powers_Ind;
    private DbsField pvca5000_Pnd_A_Power_Of_Atty_Ind;
    private DbsField pvca5000_Pnd_A_Loi_Existing_Ind;
    private DbsField pvca5000_Pnd_A_Loi_Trust_Ind;
    private DbsField pvca5000_Pnd_A_Loi_New_Ind;
    private DbsField pvca5000_Pnd_A_Loi_Jtwros_Ind;
    private DbsField pvca5000_Pnd_A_Onetime_Transfer_Amt;
    private DbsField pvca5000_Pnd_A_Second_Generation_Amt;
    private DbsField pvca5000_Pnd_A_Brokerage_Retail_Amt;
    private DbsField pvca5000_Pnd_A_Brokerage_Ira_Amt;
    private DbsField pvca5000_Pnd_A_Brokerage_Ira_Roth_Amt;
    private DbsField pvca5000_Pnd_A_Brokerage_Retail_Acct_Nbr;
    private DbsField pvca5000_Pnd_A_Brokerage_Ira_Acct_Nbr;
    private DbsField pvca5000_Pnd_A_Brokerage_Roth_Acct_Nbr;
    private DbsField pvca5000_Pnd_A_Ro_Account_Nbr;
    private DbsField pvca5000_Pnd_A_Di_O4_Tiaa_Acct_Type;
    private DbsField pvca5000_Pnd_A_Di_O4_Careof;
    private DbsField pvca5000_Pnd_A_Loi_Ira_Ind;
    private DbsField pvca5000_Pnd_A_Loi_Estate_Ind;
    private DbsField pvca5000_Pnd_A_Tpa_Page;

    public DbsGroup getPvca5000() { return pvca5000; }

    public DbsField getPvca5000_Pvca5000_Array() { return pvca5000_Pvca5000_Array; }

    public DbsGroup getPvca5000_Pvca5000_ArrayRedef1() { return pvca5000_Pvca5000_ArrayRedef1; }

    public DbsField getPvca5000_Pnd_Result_Code() { return pvca5000_Pnd_Result_Code; }

    public DbsField getPvca5000_Pnd_Result_Msg() { return pvca5000_Pnd_Result_Msg; }

    public DbsField getPvca5000_Pnd_More_Data() { return pvca5000_Pnd_More_Data; }

    public DbsField getPvca5000_Pnd_A_Universal_Id() { return pvca5000_Pnd_A_Universal_Id; }

    public DbsField getPvca5000_Pnd_A_Universal_Id_Type() { return pvca5000_Pnd_A_Universal_Id_Type; }

    public DbsField getPvca5000_Pnd_A_Requestor_Title() { return pvca5000_Pnd_A_Requestor_Title; }

    public DbsField getPvca5000_Pnd_A_Requestor_Suffix() { return pvca5000_Pnd_A_Requestor_Suffix; }

    public DbsField getPvca5000_Pnd_A_Requestor_Trust_Name_1() { return pvca5000_Pnd_A_Requestor_Trust_Name_1; }

    public DbsField getPvca5000_Pnd_A_Requestor_Trust_Name_2() { return pvca5000_Pnd_A_Requestor_Trust_Name_2; }

    public DbsField getPvca5000_Pnd_A_Requestor_Name() { return pvca5000_Pnd_A_Requestor_Name; }

    public DbsField getPvca5000_Pnd_A_Requestor_Addr1() { return pvca5000_Pnd_A_Requestor_Addr1; }

    public DbsField getPvca5000_Pnd_A_Requestor_Addr2() { return pvca5000_Pnd_A_Requestor_Addr2; }

    public DbsField getPvca5000_Pnd_A_Requestor_Addr3() { return pvca5000_Pnd_A_Requestor_Addr3; }

    public DbsField getPvca5000_Pnd_A_Requestor_Addr4() { return pvca5000_Pnd_A_Requestor_Addr4; }

    public DbsField getPvca5000_Pnd_A_Requestor_Addr5() { return pvca5000_Pnd_A_Requestor_Addr5; }

    public DbsField getPvca5000_Pnd_A_Requestor_City() { return pvca5000_Pnd_A_Requestor_City; }

    public DbsField getPvca5000_Pnd_A_Requestor_Zip() { return pvca5000_Pnd_A_Requestor_Zip; }

    public DbsField getPvca5000_Pnd_A_Requestor_State() { return pvca5000_Pnd_A_Requestor_State; }

    public DbsField getPvca5000_Pnd_A_Requestor_Addr_Typ() { return pvca5000_Pnd_A_Requestor_Addr_Typ; }

    public DbsField getPvca5000_Pnd_A_Requestor_Country() { return pvca5000_Pnd_A_Requestor_Country; }

    public DbsField getPvca5000_Pnd_A_Requestor_Trust_Or_Est() { return pvca5000_Pnd_A_Requestor_Trust_Or_Est; }

    public DbsField getPvca5000_Pnd_A_Requestor_Care() { return pvca5000_Pnd_A_Requestor_Care; }

    public DbsField getPvca5000_Pnd_A_Altr_Requestor_Title() { return pvca5000_Pnd_A_Altr_Requestor_Title; }

    public DbsField getPvca5000_Pnd_A_Altr_Requestor_Suffix() { return pvca5000_Pnd_A_Altr_Requestor_Suffix; }

    public DbsField getPvca5000_Pnd_A_Altr_Requestor_Name() { return pvca5000_Pnd_A_Altr_Requestor_Name; }

    public DbsField getPvca5000_Pnd_A_Altr_Requestor_Addr1() { return pvca5000_Pnd_A_Altr_Requestor_Addr1; }

    public DbsField getPvca5000_Pnd_A_Altr_Requestor_Addr2() { return pvca5000_Pnd_A_Altr_Requestor_Addr2; }

    public DbsField getPvca5000_Pnd_A_Altr_Requestor_City() { return pvca5000_Pnd_A_Altr_Requestor_City; }

    public DbsField getPvca5000_Pnd_A_Altr_Requestor_Zip() { return pvca5000_Pnd_A_Altr_Requestor_Zip; }

    public DbsField getPvca5000_Pnd_A_Altr_Requestor_State() { return pvca5000_Pnd_A_Altr_Requestor_State; }

    public DbsField getPvca5000_Pnd_A_Altr_Requestor_Addr_Typ() { return pvca5000_Pnd_A_Altr_Requestor_Addr_Typ; }

    public DbsField getPvca5000_Pnd_A_Altr_Requestor_Country() { return pvca5000_Pnd_A_Altr_Requestor_Country; }

    public DbsField getPvca5000_Pnd_A_Altr_Requestor_Trust_Or_Est() { return pvca5000_Pnd_A_Altr_Requestor_Trust_Or_Est; }

    public DbsField getPvca5000_Pnd_A_Altr_Requestor_Care() { return pvca5000_Pnd_A_Altr_Requestor_Care; }

    public DbsField getPvca5000_Pnd_A_Salutation_Text() { return pvca5000_Pnd_A_Salutation_Text; }

    public DbsField getPvca5000_Pnd_A_Local_Printer() { return pvca5000_Pnd_A_Local_Printer; }

    public DbsField getPvca5000_Pnd_A_Immediate_Printer_Ind() { return pvca5000_Pnd_A_Immediate_Printer_Ind; }

    public DbsField getPvca5000_Pnd_A_Signatory_Racf() { return pvca5000_Pnd_A_Signatory_Racf; }

    public DbsField getPvca5000_Pnd_A_Signatory_Racf_Unit() { return pvca5000_Pnd_A_Signatory_Racf_Unit; }

    public DbsField getPvca5000_Pnd_A_Signatory_Racf_Name() { return pvca5000_Pnd_A_Signatory_Racf_Name; }

    public DbsField getPvca5000_Pnd_A_Letter_Type() { return pvca5000_Pnd_A_Letter_Type; }

    public DbsField getPvca5000_Pnd_A_Case_Manager_Name() { return pvca5000_Pnd_A_Case_Manager_Name; }

    public DbsField getPvca5000_Pnd_A_Case_Manage_Phone() { return pvca5000_Pnd_A_Case_Manage_Phone; }

    public DbsField getPvca5000_Pnd_A_Institution_Name() { return pvca5000_Pnd_A_Institution_Name; }

    public DbsField getPvca5000_Pnd_A_Decedent_Pin_Nbr() { return pvca5000_Pnd_A_Decedent_Pin_Nbr; }

    public DbsField getPvca5000_Pnd_A_Decedent_Other_Than_Org() { return pvca5000_Pnd_A_Decedent_Other_Than_Org; }

    public DbsField getPvca5000_Pnd_A_Decedent_Name() { return pvca5000_Pnd_A_Decedent_Name; }

    public DbsField getPvca5000_Pnd_A_Decedent_Date_Of_Birth() { return pvca5000_Pnd_A_Decedent_Date_Of_Birth; }

    public DbsField getPvca5000_Pnd_A_Decedent_Date_Of_Death() { return pvca5000_Pnd_A_Decedent_Date_Of_Death; }

    public DbsField getPvca5000_Pnd_A_Cont_Beneficiary_Name() { return pvca5000_Pnd_A_Cont_Beneficiary_Name; }

    public DbsField getPvca5000_Pnd_A_Minor_Org_Estate_Name() { return pvca5000_Pnd_A_Minor_Org_Estate_Name; }

    public DbsField getPvca5000_Pnd_A_Trust_Name() { return pvca5000_Pnd_A_Trust_Name; }

    public DbsField getPvca5000_Pnd_A_Children_Drop_Down() { return pvca5000_Pnd_A_Children_Drop_Down; }

    public DbsField getPvca5000_Pnd_A_Pages_Missing() { return pvca5000_Pnd_A_Pages_Missing; }

    public DbsField getPvca5000_Pnd_A_Death_Cert_Bene_Names() { return pvca5000_Pnd_A_Death_Cert_Bene_Names; }

    public DbsField getPvca5000_Pnd_A_Pvc_Delivery_Type() { return pvca5000_Pnd_A_Pvc_Delivery_Type; }

    public DbsField getPvca5000_Pnd_A_Survivor_Task_Category() { return pvca5000_Pnd_A_Survivor_Task_Category; }

    public DbsField getPvca5000_Pnd_A_Unclaimed_Funds_State_Name() { return pvca5000_Pnd_A_Unclaimed_Funds_State_Name; }

    public DbsField getPvca5000_Pnd_A_Unclaimed_Funds_State_Cde() { return pvca5000_Pnd_A_Unclaimed_Funds_State_Cde; }

    public DbsField getPvca5000_Pnd_A_Unclaimed_Funds_Contract_Nbr() { return pvca5000_Pnd_A_Unclaimed_Funds_Contract_Nbr; }

    public DbsField getPvca5000_Pnd_A_Brm_Name() { return pvca5000_Pnd_A_Brm_Name; }

    public DbsField getPvca5000_Pnd_A_Footer_Location() { return pvca5000_Pnd_A_Footer_Location; }

    public DbsField getPvca5000_Pnd_A_Renun_Waiver_Release_Ind() { return pvca5000_Pnd_A_Renun_Waiver_Release_Ind; }

    public DbsField getPvca5000_Pnd_A_Marital_Stat_Addendum_Ind() { return pvca5000_Pnd_A_Marital_Stat_Addendum_Ind; }

    public DbsField getPvca5000_Pnd_A_Release_Indem_Agree_Ind() { return pvca5000_Pnd_A_Release_Indem_Agree_Ind; }

    public DbsField getPvca5000_Pnd_A_Savings_Invest_Appl_Ind() { return pvca5000_Pnd_A_Savings_Invest_Appl_Ind; }

    public DbsField getPvca5000_Pnd_A_Survivor_Bene_Dist_Rqst_Ind() { return pvca5000_Pnd_A_Survivor_Bene_Dist_Rqst_Ind; }

    public DbsField getPvca5000_Pnd_A_Income_Annty_Request_Form_Ind() { return pvca5000_Pnd_A_Income_Annty_Request_Form_Ind; }

    public DbsField getPvca5000_Pnd_A_Immediate_Annuity_Form_Ind() { return pvca5000_Pnd_A_Immediate_Annuity_Form_Ind; }

    public DbsField getPvca5000_Pnd_A_One_Time_Pay_Request_Form_Ind() { return pvca5000_Pnd_A_One_Time_Pay_Request_Form_Ind; }

    public DbsField getPvca5000_Pnd_A_Da_Tpa_Desig_Form_Ind() { return pvca5000_Pnd_A_Da_Tpa_Desig_Form_Ind; }

    public DbsField getPvca5000_Pnd_A_Da_Tpa_Desig_Form_Ctr() { return pvca5000_Pnd_A_Da_Tpa_Desig_Form_Ctr; }

    public DbsField getPvca5000_Pnd_A_Ia_Beneficiary_Form_Ind() { return pvca5000_Pnd_A_Ia_Beneficiary_Form_Ind; }

    public DbsField getPvca5000_Pnd_A_Ia_Beneficiary_Form_Ctr() { return pvca5000_Pnd_A_Ia_Beneficiary_Form_Ctr; }

    public DbsField getPvca5000_Pnd_A_Ohio_Arp_Bene_Form_Ind() { return pvca5000_Pnd_A_Ohio_Arp_Bene_Form_Ind; }

    public DbsField getPvca5000_Pnd_A_Ohio_Arp_Bene_Form_Ctr() { return pvca5000_Pnd_A_Ohio_Arp_Bene_Form_Ctr; }

    public DbsField getPvca5000_Pnd_A_Univ_Pittsburgh_Bene_Form_Ind() { return pvca5000_Pnd_A_Univ_Pittsburgh_Bene_Form_Ind; }

    public DbsField getPvca5000_Pnd_A_Univ_Pittsburgh_Bene_Form_Ctr() { return pvca5000_Pnd_A_Univ_Pittsburgh_Bene_Form_Ctr; }

    public DbsField getPvca5000_Pnd_A_Univ_Michigan_Bene_Form_Ind() { return pvca5000_Pnd_A_Univ_Michigan_Bene_Form_Ind; }

    public DbsField getPvca5000_Pnd_A_Univ_Michigan_Bene_Form_Ctr() { return pvca5000_Pnd_A_Univ_Michigan_Bene_Form_Ctr; }

    public DbsField getPvca5000_Pnd_A_Successr_Beneficiary_Form_Ind() { return pvca5000_Pnd_A_Successr_Beneficiary_Form_Ind; }

    public DbsField getPvca5000_Pnd_A_Successr_Beneficiary_Form_Ctr() { return pvca5000_Pnd_A_Successr_Beneficiary_Form_Ctr; }

    public DbsField getPvca5000_Pnd_A_Ohio_Consent_Ind() { return pvca5000_Pnd_A_Ohio_Consent_Ind; }

    public DbsField getPvca5000_Pnd_A_Oklahoma_Consent_Ind() { return pvca5000_Pnd_A_Oklahoma_Consent_Ind; }

    public DbsField getPvca5000_Pnd_A_Indiana_Notice_Ind() { return pvca5000_Pnd_A_Indiana_Notice_Ind; }

    public DbsField getPvca5000_Pnd_A_Rhode_Island_Notice_Ind() { return pvca5000_Pnd_A_Rhode_Island_Notice_Ind; }

    public DbsField getPvca5000_Pnd_A_W9_Form_Ind() { return pvca5000_Pnd_A_W9_Form_Ind; }

    public DbsField getPvca5000_Pnd_A_W8ben_Form_Ind() { return pvca5000_Pnd_A_W8ben_Form_Ind; }

    public DbsField getPvca5000_Pnd_A_W7_Form_Ind() { return pvca5000_Pnd_A_W7_Form_Ind; }

    public DbsField getPvca5000_Pnd_A_State_Code_Taxint() { return pvca5000_Pnd_A_State_Code_Taxint; }

    public DbsField getPvca5000_Pnd_A_State_Name_Taxint() { return pvca5000_Pnd_A_State_Name_Taxint; }

    public DbsField getPvca5000_Pnd_A_State_Name_Text() { return pvca5000_Pnd_A_State_Name_Text; }

    public DbsField getPvca5000_Pnd_A_Loved_One_Passes_Away_Ind() { return pvca5000_Pnd_A_Loved_One_Passes_Away_Ind; }

    public DbsField getPvca5000_Pnd_A_Sip_Booklet_Ind() { return pvca5000_Pnd_A_Sip_Booklet_Ind; }

    public DbsField getPvca5000_Pnd_A_Taking_Next_Step_Ind() { return pvca5000_Pnd_A_Taking_Next_Step_Ind; }

    public DbsField getPvca5000_Pnd_A_Qtrly_Performance_Ind() { return pvca5000_Pnd_A_Qtrly_Performance_Ind; }

    public DbsField getPvca5000_Pnd_A_Tiaa_Cref_Prospectus_Ind() { return pvca5000_Pnd_A_Tiaa_Cref_Prospectus_Ind; }

    public DbsField getPvca5000_Pnd_A_Cref_Prospectus_Ind() { return pvca5000_Pnd_A_Cref_Prospectus_Ind; }

    public DbsField getPvca5000_Pnd_A_Tiaa_Rea_Prospectus_Ind() { return pvca5000_Pnd_A_Tiaa_Rea_Prospectus_Ind; }

    public DbsField getPvca5000_Pnd_A_Lifecycle_Prospectus_Ind() { return pvca5000_Pnd_A_Lifecycle_Prospectus_Ind; }

    public DbsField getPvca5000_Pnd_A_Tiaa_Rate_Sheet_Ind() { return pvca5000_Pnd_A_Tiaa_Rate_Sheet_Ind; }

    public DbsField getPvca5000_Pnd_A_Atra_Sip_Glance_Indvl_Ind() { return pvca5000_Pnd_A_Atra_Sip_Glance_Indvl_Ind; }

    public DbsField getPvca5000_Pnd_A_Atra_Ssum_Glance_Trust_Ind() { return pvca5000_Pnd_A_Atra_Ssum_Glance_Trust_Ind; }

    public DbsField getPvca5000_Pnd_A_Atra_Indvl_Bene_Glance_Ind() { return pvca5000_Pnd_A_Atra_Indvl_Bene_Glance_Ind; }

    public DbsField getPvca5000_Pnd_A_Atra_Ssum_Glance_Estate_Ind() { return pvca5000_Pnd_A_Atra_Ssum_Glance_Estate_Ind; }

    public DbsField getPvca5000_Pnd_A_Atra_Annty_Glance_Indvl_Ind() { return pvca5000_Pnd_A_Atra_Annty_Glance_Indvl_Ind; }

    public DbsField getPvca5000_Pnd_A_Atra_Fix_Prd_Ann_Glance_Ind() { return pvca5000_Pnd_A_Atra_Fix_Prd_Ann_Glance_Ind; }

    public DbsField getPvca5000_Pnd_A_Ssum_Glance_Nonspouse_Ind() { return pvca5000_Pnd_A_Ssum_Glance_Nonspouse_Ind; }

    public DbsField getPvca5000_Pnd_A_Ssum_Glance_Spouse_Ind() { return pvca5000_Pnd_A_Ssum_Glance_Spouse_Ind; }

    public DbsField getPvca5000_Pnd_A_Ssum_Glance_Trust_Ind() { return pvca5000_Pnd_A_Ssum_Glance_Trust_Ind; }

    public DbsField getPvca5000_Pnd_A_Ssum_Glance_Estate_Ind() { return pvca5000_Pnd_A_Ssum_Glance_Estate_Ind; }

    public DbsField getPvca5000_Pnd_A_Notice_402f_Ind() { return pvca5000_Pnd_A_Notice_402f_Ind; }

    public DbsField getPvca5000_Pnd_A_Custodial_Agreement_Ind() { return pvca5000_Pnd_A_Custodial_Agreement_Ind; }

    public DbsField getPvca5000_Pnd_A_Record_Of_Age_Ind() { return pvca5000_Pnd_A_Record_Of_Age_Ind; }

    public DbsField getPvca5000_Pnd_A_Payment_Dest_Form_Ind() { return pvca5000_Pnd_A_Payment_Dest_Form_Ind; }

    public DbsField getPvca5000_Pnd_A_Transfer_At_Settlement_Ind() { return pvca5000_Pnd_A_Transfer_At_Settlement_Ind; }

    public DbsField getPvca5000_Pnd_A_Doma_Ind() { return pvca5000_Pnd_A_Doma_Ind; }

    public DbsField getPvca5000_Pnd_A_Minor_Sa() { return pvca5000_Pnd_A_Minor_Sa; }

    public DbsField getPvca5000_Pnd_A_My_Children_Ind() { return pvca5000_Pnd_A_My_Children_Ind; }

    public DbsField getPvca5000_Pnd_A_Minor_Guardian_Hip_Ind() { return pvca5000_Pnd_A_Minor_Guardian_Hip_Ind; }

    public DbsField getPvca5000_Pnd_A_Exspouse_Divorce_Decree_Ind() { return pvca5000_Pnd_A_Exspouse_Divorce_Decree_Ind; }

    public DbsField getPvca5000_Pnd_A_Estate_Ind() { return pvca5000_Pnd_A_Estate_Ind; }

    public DbsField getPvca5000_Pnd_A_Small_Estate_Ind() { return pvca5000_Pnd_A_Small_Estate_Ind; }

    public DbsField getPvca5000_Pnd_A_Unable_To_Locate_Trust_Ind() { return pvca5000_Pnd_A_Unable_To_Locate_Trust_Ind; }

    public DbsField getPvca5000_Pnd_A_Need_Bene_Address_Ind() { return pvca5000_Pnd_A_Need_Bene_Address_Ind; }

    public DbsField getPvca5000_Pnd_A_Organization_Ind() { return pvca5000_Pnd_A_Organization_Ind; }

    public DbsField getPvca5000_Pnd_A_Msv_Ind() { return pvca5000_Pnd_A_Msv_Ind; }

    public DbsField getPvca5000_Pnd_A_Roth_Accumulation_Ind() { return pvca5000_Pnd_A_Roth_Accumulation_Ind; }

    public DbsField getPvca5000_Pnd_A_Atra_Accumulation_Ind() { return pvca5000_Pnd_A_Atra_Accumulation_Ind; }

    public DbsField getPvca5000_Pnd_A_Investment_Contract_Ind() { return pvca5000_Pnd_A_Investment_Contract_Ind; }

    public DbsField getPvca5000_Pnd_A_Allocation_Ind() { return pvca5000_Pnd_A_Allocation_Ind; }

    public DbsField getPvca5000_Pnd_A_Blank_Beneficiaries_Ind() { return pvca5000_Pnd_A_Blank_Beneficiaries_Ind; }

    public DbsField getPvca5000_Pnd_A_Contract_Numbers_Ind() { return pvca5000_Pnd_A_Contract_Numbers_Ind; }

    public DbsField getPvca5000_Pnd_A_Invalid_Date_Ind() { return pvca5000_Pnd_A_Invalid_Date_Ind; }

    public DbsField getPvca5000_Pnd_A_Estate_Trust_Ind() { return pvca5000_Pnd_A_Estate_Trust_Ind; }

    public DbsField getPvca5000_Pnd_A_Executor_Ind() { return pvca5000_Pnd_A_Executor_Ind; }

    public DbsField getPvca5000_Pnd_A_Fax_Copies_Ind() { return pvca5000_Pnd_A_Fax_Copies_Ind; }

    public DbsField getPvca5000_Pnd_A_Guarantee_Period_Expire_Ind() { return pvca5000_Pnd_A_Guarantee_Period_Expire_Ind; }

    public DbsField getPvca5000_Pnd_A_No_Guarantee_Period_Ind() { return pvca5000_Pnd_A_No_Guarantee_Period_Ind; }

    public DbsField getPvca5000_Pnd_A_Indvl_Bene_Trust_Ind() { return pvca5000_Pnd_A_Indvl_Bene_Trust_Ind; }

    public DbsField getPvca5000_Pnd_A_Institution_Maintained_Ind() { return pvca5000_Pnd_A_Institution_Maintained_Ind; }

    public DbsField getPvca5000_Pnd_A_Irrevocable_Designation_Ind() { return pvca5000_Pnd_A_Irrevocable_Designation_Ind; }

    public DbsField getPvca5000_Pnd_A_Missing_Pages_Ind() { return pvca5000_Pnd_A_Missing_Pages_Ind; }

    public DbsField getPvca5000_Pnd_A_Inactive_Contracts_Ind() { return pvca5000_Pnd_A_Inactive_Contracts_Ind; }

    public DbsField getPvca5000_Pnd_A_Or_Used_In_Designation_Ind() { return pvca5000_Pnd_A_Or_Used_In_Designation_Ind; }

    public DbsField getPvca5000_Pnd_A_Same_Primary_Contingent_Ind() { return pvca5000_Pnd_A_Same_Primary_Contingent_Ind; }

    public DbsField getPvca5000_Pnd_A_Siblings_Ind() { return pvca5000_Pnd_A_Siblings_Ind; }

    public DbsField getPvca5000_Pnd_A_No_Signature_Date_Ind() { return pvca5000_Pnd_A_No_Signature_Date_Ind; }

    public DbsField getPvca5000_Pnd_A_Spouse_As_Primary_Bene_Ind() { return pvca5000_Pnd_A_Spouse_As_Primary_Bene_Ind; }

    public DbsField getPvca5000_Pnd_A_Surs_Ind() { return pvca5000_Pnd_A_Surs_Ind; }

    public DbsField getPvca5000_Pnd_A_Age_Stipulation_Ind() { return pvca5000_Pnd_A_Age_Stipulation_Ind; }

    public DbsField getPvca5000_Pnd_A_Deceased_Child_Chk_Gchild_Ind() { return pvca5000_Pnd_A_Deceased_Child_Chk_Gchild_Ind; }

    public DbsField getPvca5000_Pnd_A_Dec_Child_Bxchk_Schild_Ind() { return pvca5000_Pnd_A_Dec_Child_Bxchk_Schild_Ind; }

    public DbsField getPvca5000_Pnd_A_Difficult_To_Read_Ind() { return pvca5000_Pnd_A_Difficult_To_Read_Ind; }

    public DbsField getPvca5000_Pnd_A_Divesting_Ind() { return pvca5000_Pnd_A_Divesting_Ind; }

    public DbsField getPvca5000_Pnd_A_For_The_Benefit_Of_Ind() { return pvca5000_Pnd_A_For_The_Benefit_Of_Ind; }

    public DbsField getPvca5000_Pnd_A_Guardian_Ind() { return pvca5000_Pnd_A_Guardian_Ind; }

    public DbsField getPvca5000_Pnd_A_Mdo_Contract_Ind() { return pvca5000_Pnd_A_Mdo_Contract_Ind; }

    public DbsField getPvca5000_Pnd_A_Missing_Information_Ind() { return pvca5000_Pnd_A_Missing_Information_Ind; }

    public DbsField getPvca5000_Pnd_A_Department_Ind() { return pvca5000_Pnd_A_Department_Ind; }

    public DbsField getPvca5000_Pnd_A_No_Primary_Bene_Ind() { return pvca5000_Pnd_A_No_Primary_Bene_Ind; }

    public DbsField getPvca5000_Pnd_A_Second_Annuitant_Named_Ind() { return pvca5000_Pnd_A_Second_Annuitant_Named_Ind; }

    public DbsField getPvca5000_Pnd_A_Ssn_Not_Shown_Ind() { return pvca5000_Pnd_A_Ssn_Not_Shown_Ind; }

    public DbsField getPvca5000_Pnd_A_Spouse_Another_Person_Ind() { return pvca5000_Pnd_A_Spouse_Another_Person_Ind; }

    public DbsField getPvca5000_Pnd_A_Spousal_Waiver_Inc_Ind() { return pvca5000_Pnd_A_Spousal_Waiver_Inc_Ind; }

    public DbsField getPvca5000_Pnd_A_Survivorship_Clause() { return pvca5000_Pnd_A_Survivorship_Clause; }

    public DbsField getPvca5000_Pnd_A_Tin_Ind() { return pvca5000_Pnd_A_Tin_Ind; }

    public DbsField getPvca5000_Pnd_A_Terms_Of_Trust_Ind() { return pvca5000_Pnd_A_Terms_Of_Trust_Ind; }

    public DbsField getPvca5000_Pnd_A_Trust_Date_Ind() { return pvca5000_Pnd_A_Trust_Date_Ind; }

    public DbsField getPvca5000_Pnd_A_Unborn_Children_Ind() { return pvca5000_Pnd_A_Unborn_Children_Ind; }

    public DbsField getPvca5000_Pnd_A_Unnamed_Spouse_Of_Bene_Ind() { return pvca5000_Pnd_A_Unnamed_Spouse_Of_Bene_Ind; }

    public DbsField getPvca5000_Pnd_A_No_Designation_Found_Ind() { return pvca5000_Pnd_A_No_Designation_Found_Ind; }

    public DbsField getPvca5000_Pnd_A_Poor_Copy_Ind() { return pvca5000_Pnd_A_Poor_Copy_Ind; }

    public DbsField getPvca5000_Pnd_A_Retirement_Loan_Ind() { return pvca5000_Pnd_A_Retirement_Loan_Ind; }

    public DbsField getPvca5000_Pnd_A_Wpahs_Ind() { return pvca5000_Pnd_A_Wpahs_Ind; }

    public DbsField getPvca5000_Pnd_A_Erisa_Ind() { return pvca5000_Pnd_A_Erisa_Ind; }

    public DbsField getPvca5000_Pnd_A_Death_Certificate_Reqrd_Ind() { return pvca5000_Pnd_A_Death_Certificate_Reqrd_Ind; }

    public DbsField getPvca5000_Pnd_A_Death_Cert_Primary_Bene_Ind() { return pvca5000_Pnd_A_Death_Cert_Primary_Bene_Ind; }

    public DbsField getPvca5000_Pnd_A_Statement_Will_Ind() { return pvca5000_Pnd_A_Statement_Will_Ind; }

    public DbsField getPvca5000_Pnd_A_Statement_Trust_In_Effect_Ind() { return pvca5000_Pnd_A_Statement_Trust_In_Effect_Ind; }

    public DbsField getPvca5000_Pnd_A_Statement_Trust_Accept_Ind() { return pvca5000_Pnd_A_Statement_Trust_Accept_Ind; }

    public DbsField getPvca5000_Pnd_A_Taxpayer_Id_Nbr_Ind() { return pvca5000_Pnd_A_Taxpayer_Id_Nbr_Ind; }

    public DbsField getPvca5000_Pnd_A_Letters_Of_Appointment_Ind() { return pvca5000_Pnd_A_Letters_Of_Appointment_Ind; }

    public DbsField getPvca5000_Pnd_A_Small_Accum_Ind() { return pvca5000_Pnd_A_Small_Accum_Ind; }

    public DbsField getPvca5000_Pnd_A_Signature_Guarantee_Ind() { return pvca5000_Pnd_A_Signature_Guarantee_Ind; }

    public DbsField getPvca5000_Pnd_A_Death_Certificate() { return pvca5000_Pnd_A_Death_Certificate; }

    public DbsField getPvca5000_Pnd_A_Reason_Code() { return pvca5000_Pnd_A_Reason_Code; }

    public DbsField getPvca5000_Pnd_A_Reason_Text() { return pvca5000_Pnd_A_Reason_Text; }

    public DbsField getPvca5000_Pnd_A_Non_Spouse_Rmd_Ind() { return pvca5000_Pnd_A_Non_Spouse_Rmd_Ind; }

    public DbsField getPvca5000_Pnd_A_Spouse_Rmd_Ind() { return pvca5000_Pnd_A_Spouse_Rmd_Ind; }

    public DbsField getPvca5000_Pnd_A_Valuation_Contract_Nbr() { return pvca5000_Pnd_A_Valuation_Contract_Nbr; }

    public DbsField getPvca5000_Pnd_A_Value_Of_Contract() { return pvca5000_Pnd_A_Value_Of_Contract; }

    public DbsField getPvca5000_Pnd_A_Total_At_Valuation_Date() { return pvca5000_Pnd_A_Total_At_Valuation_Date; }

    public DbsField getPvca5000_Pnd_A_Alternate_Valuation_Date() { return pvca5000_Pnd_A_Alternate_Valuation_Date; }

    public DbsField getPvca5000_Pnd_A_Investment_In_Contract() { return pvca5000_Pnd_A_Investment_In_Contract; }

    public DbsField getPvca5000_Pnd_A_Account_Name() { return pvca5000_Pnd_A_Account_Name; }

    public DbsField getPvca5000_Pnd_A_Account_Amount() { return pvca5000_Pnd_A_Account_Amount; }

    public DbsField getPvca5000_Pnd_A_Entitlement_Total_Amount() { return pvca5000_Pnd_A_Entitlement_Total_Amount; }

    public DbsField getPvca5000_Pnd_A_Roth_Accumulation() { return pvca5000_Pnd_A_Roth_Accumulation; }

    public DbsField getPvca5000_Pnd_A_Atra_Accumulation() { return pvca5000_Pnd_A_Atra_Accumulation; }

    public DbsField getPvca5000_Pnd_A_Invest_And_Contract_Amount() { return pvca5000_Pnd_A_Invest_And_Contract_Amount; }

    public DbsField getPvca5000_Pnd_A_Tiaa_Nbr() { return pvca5000_Pnd_A_Tiaa_Nbr; }

    public DbsField getPvca5000_Pnd_A_Cref_Nbr() { return pvca5000_Pnd_A_Cref_Nbr; }

    public DbsField getPvca5000_Pnd_A_Bi_Bene_Type() { return pvca5000_Pnd_A_Bi_Bene_Type; }

    public DbsField getPvca5000_Pnd_A_Bi_Bene_Name() { return pvca5000_Pnd_A_Bi_Bene_Name; }

    public DbsField getPvca5000_Pnd_A_Bi_Bene_Trustee_Name() { return pvca5000_Pnd_A_Bi_Bene_Trustee_Name; }

    public DbsField getPvca5000_Pnd_A_Bi_Street() { return pvca5000_Pnd_A_Bi_Street; }

    public DbsField getPvca5000_Pnd_A_Bi_City() { return pvca5000_Pnd_A_Bi_City; }

    public DbsField getPvca5000_Pnd_A_Bi_State() { return pvca5000_Pnd_A_Bi_State; }

    public DbsField getPvca5000_Pnd_A_Bi_Zip() { return pvca5000_Pnd_A_Bi_Zip; }

    public DbsField getPvca5000_Pnd_A_Bi_Primary_Phone() { return pvca5000_Pnd_A_Bi_Primary_Phone; }

    public DbsField getPvca5000_Pnd_A_Bi_Alt_Phone() { return pvca5000_Pnd_A_Bi_Alt_Phone; }

    public DbsField getPvca5000_Pnd_A_Bi_Email_Address() { return pvca5000_Pnd_A_Bi_Email_Address; }

    public DbsField getPvca5000_Pnd_A_Bi_State_Legal_Residency() { return pvca5000_Pnd_A_Bi_State_Legal_Residency; }

    public DbsField getPvca5000_Pnd_A_Bi_Citizenship() { return pvca5000_Pnd_A_Bi_Citizenship; }

    public DbsField getPvca5000_Pnd_A_Bi_Dob_Trust_Amend_Date() { return pvca5000_Pnd_A_Bi_Dob_Trust_Amend_Date; }

    public DbsField getPvca5000_Pnd_A_Bi_Gender() { return pvca5000_Pnd_A_Bi_Gender; }

    public DbsField getPvca5000_Pnd_A_Bi_Bene_Relation_Decedent() { return pvca5000_Pnd_A_Bi_Bene_Relation_Decedent; }

    public DbsField getPvca5000_Pnd_A_Ct_Trustee_Name() { return pvca5000_Pnd_A_Ct_Trustee_Name; }

    public DbsField getPvca5000_Pnd_A_Ct_Trustee_Dob() { return pvca5000_Pnd_A_Ct_Trustee_Dob; }

    public DbsField getPvca5000_Pnd_A_Ct_Trustee_Relation_Decedent() { return pvca5000_Pnd_A_Ct_Trustee_Relation_Decedent; }

    public DbsField getPvca5000_Pnd_A_So_Option_Type() { return pvca5000_Pnd_A_So_Option_Type; }

    public DbsField getPvca5000_Pnd_A_So_Partial_Cash_Amt() { return pvca5000_Pnd_A_So_Partial_Cash_Amt; }

    public DbsField getPvca5000_Pnd_A_So_Partial_Cash_Pct() { return pvca5000_Pnd_A_So_Partial_Cash_Pct; }

    public DbsField getPvca5000_Pnd_A_So_Ann_Option_Type() { return pvca5000_Pnd_A_So_Ann_Option_Type; }

    public DbsField getPvca5000_Pnd_A_So_Ann_Partial_Cash_Amt() { return pvca5000_Pnd_A_So_Ann_Partial_Cash_Amt; }

    public DbsField getPvca5000_Pnd_A_So_Ann_Partial_Cash_Pct() { return pvca5000_Pnd_A_So_Ann_Partial_Cash_Pct; }

    public DbsField getPvca5000_Pnd_A_Ss_Option_Type() { return pvca5000_Pnd_A_Ss_Option_Type; }

    public DbsField getPvca5000_Pnd_A_Ss_Partial_Cash_Amt() { return pvca5000_Pnd_A_Ss_Partial_Cash_Amt; }

    public DbsField getPvca5000_Pnd_A_Ss_Partial_Cash_Pct() { return pvca5000_Pnd_A_Ss_Partial_Cash_Pct; }

    public DbsField getPvca5000_Pnd_A_Ss_Rollover_Acct_Status() { return pvca5000_Pnd_A_Ss_Rollover_Acct_Status; }

    public DbsField getPvca5000_Pnd_A_Ss_Rollover_Acct_Type() { return pvca5000_Pnd_A_Ss_Rollover_Acct_Type; }

    public DbsField getPvca5000_Pnd_A_Ss_Rollover_Acct_Nbr() { return pvca5000_Pnd_A_Ss_Rollover_Acct_Nbr; }

    public DbsField getPvca5000_Pnd_A_Ss_Tiaacref_Acct_Status() { return pvca5000_Pnd_A_Ss_Tiaacref_Acct_Status; }

    public DbsField getPvca5000_Pnd_A_Ss_Tiaacref_Acct_Type() { return pvca5000_Pnd_A_Ss_Tiaacref_Acct_Type; }

    public DbsField getPvca5000_Pnd_A_Ss_Tiaacref_Acct_Nbr() { return pvca5000_Pnd_A_Ss_Tiaacref_Acct_Nbr; }

    public DbsField getPvca5000_Pnd_A_Sn_Option_Type() { return pvca5000_Pnd_A_Sn_Option_Type; }

    public DbsField getPvca5000_Pnd_A_Sn_Partial_Cash_Amt() { return pvca5000_Pnd_A_Sn_Partial_Cash_Amt; }

    public DbsField getPvca5000_Pnd_A_Sn_Partial_Cash_Pct() { return pvca5000_Pnd_A_Sn_Partial_Cash_Pct; }

    public DbsField getPvca5000_Pnd_A_Sn_Rollover_Acct_Status() { return pvca5000_Pnd_A_Sn_Rollover_Acct_Status; }

    public DbsField getPvca5000_Pnd_A_Sn_Rollover_Acct_Type() { return pvca5000_Pnd_A_Sn_Rollover_Acct_Type; }

    public DbsField getPvca5000_Pnd_A_Sn_Rollover_Acct_Nbr() { return pvca5000_Pnd_A_Sn_Rollover_Acct_Nbr; }

    public DbsField getPvca5000_Pnd_A_Sn_Tiaacref_Acct_Status() { return pvca5000_Pnd_A_Sn_Tiaacref_Acct_Status; }

    public DbsField getPvca5000_Pnd_A_Sn_Tiaacref_Acct_Type() { return pvca5000_Pnd_A_Sn_Tiaacref_Acct_Type; }

    public DbsField getPvca5000_Pnd_A_Sn_Tiaacref_Acct_Nbr() { return pvca5000_Pnd_A_Sn_Tiaacref_Acct_Nbr; }

    public DbsField getPvca5000_Pnd_A_Sn_Md_Olest_Bene_Dob() { return pvca5000_Pnd_A_Sn_Md_Olest_Bene_Dob; }

    public DbsField getPvca5000_Pnd_A_Cf_Mmdd() { return pvca5000_Pnd_A_Cf_Mmdd; }

    public DbsField getPvca5000_Pnd_A_Cf_Freq() { return pvca5000_Pnd_A_Cf_Freq; }

    public DbsField getPvca5000_Pnd_A_Tr_Rollover_Type() { return pvca5000_Pnd_A_Tr_Rollover_Type; }

    public DbsField getPvca5000_Pnd_A_Tr_O1_Trad_Ira_Nbr() { return pvca5000_Pnd_A_Tr_O1_Trad_Ira_Nbr; }

    public DbsField getPvca5000_Pnd_A_Tr_O3_Company_Name() { return pvca5000_Pnd_A_Tr_O3_Company_Name; }

    public DbsField getPvca5000_Pnd_A_Tr_O3_Company_Address() { return pvca5000_Pnd_A_Tr_O3_Company_Address; }

    public DbsField getPvca5000_Pnd_A_Tr_O3_Company_City() { return pvca5000_Pnd_A_Tr_O3_Company_City; }

    public DbsField getPvca5000_Pnd_A_Tr_O3_Company_State() { return pvca5000_Pnd_A_Tr_O3_Company_State; }

    public DbsField getPvca5000_Pnd_A_Tr_O3_Company_Zip() { return pvca5000_Pnd_A_Tr_O3_Company_Zip; }

    public DbsField getPvca5000_Pnd_A_Tr_O3_Company_Phone() { return pvca5000_Pnd_A_Tr_O3_Company_Phone; }

    public DbsField getPvca5000_Pnd_A_Tr_O3_Company_Acct_Nbr() { return pvca5000_Pnd_A_Tr_O3_Company_Acct_Nbr; }

    public DbsField getPvca5000_Pnd_A_Ri_Option_Cde() { return pvca5000_Pnd_A_Ri_Option_Cde; }

    public DbsField getPvca5000_Pnd_A_Ri_Invest_Company_Name() { return pvca5000_Pnd_A_Ri_Invest_Company_Name; }

    public DbsField getPvca5000_Pnd_A_Ri_Invest_Company_Address() { return pvca5000_Pnd_A_Ri_Invest_Company_Address; }

    public DbsField getPvca5000_Pnd_A_Ri_Invest_Company_City() { return pvca5000_Pnd_A_Ri_Invest_Company_City; }

    public DbsField getPvca5000_Pnd_A_Ri_Invest_Company_State() { return pvca5000_Pnd_A_Ri_Invest_Company_State; }

    public DbsField getPvca5000_Pnd_A_Ri_Invest_Company_Zip() { return pvca5000_Pnd_A_Ri_Invest_Company_Zip; }

    public DbsField getPvca5000_Pnd_A_Ri_Invest_Company_Phone() { return pvca5000_Pnd_A_Ri_Invest_Company_Phone; }

    public DbsField getPvca5000_Pnd_A_Ri_Invest_Company_Acct_Nbr() { return pvca5000_Pnd_A_Ri_Invest_Company_Acct_Nbr; }

    public DbsField getPvca5000_Pnd_A_Ri_Ann_Option_Cde() { return pvca5000_Pnd_A_Ri_Ann_Option_Cde; }

    public DbsField getPvca5000_Pnd_A_Ri_Ann_Invest_Company_Name() { return pvca5000_Pnd_A_Ri_Ann_Invest_Company_Name; }

    public DbsField getPvca5000_Pnd_A_Ri_Ann_Invest_Company_Address() { return pvca5000_Pnd_A_Ri_Ann_Invest_Company_Address; }

    public DbsField getPvca5000_Pnd_A_Ri_Ann_Invest_Company_City() { return pvca5000_Pnd_A_Ri_Ann_Invest_Company_City; }

    public DbsField getPvca5000_Pnd_A_Ri_Ann_Invest_Company_State() { return pvca5000_Pnd_A_Ri_Ann_Invest_Company_State; }

    public DbsField getPvca5000_Pnd_A_Ri_Ann_Invest_Company_Zip() { return pvca5000_Pnd_A_Ri_Ann_Invest_Company_Zip; }

    public DbsField getPvca5000_Pnd_A_Ri_Ann_Invest_Company_Phone() { return pvca5000_Pnd_A_Ri_Ann_Invest_Company_Phone; }

    public DbsField getPvca5000_Pnd_A_Ri_Ann_Invt_Company_Acct_Nbr() { return pvca5000_Pnd_A_Ri_Ann_Invt_Company_Acct_Nbr; }

    public DbsField getPvca5000_Pnd_A_Ro_Roth_Ind() { return pvca5000_Pnd_A_Ro_Roth_Ind; }

    public DbsField getPvca5000_Pnd_A_Ro_Roth_Amt() { return pvca5000_Pnd_A_Ro_Roth_Amt; }

    public DbsField getPvca5000_Pnd_A_Ro_Roth_Pct() { return pvca5000_Pnd_A_Ro_Roth_Pct; }

    public DbsField getPvca5000_Pnd_A_Ft_Whold_Type() { return pvca5000_Pnd_A_Ft_Whold_Type; }

    public DbsField getPvca5000_Pnd_A_Ft_Da_Dollar_Amt() { return pvca5000_Pnd_A_Ft_Da_Dollar_Amt; }

    public DbsField getPvca5000_Pnd_A_Ft_Pc_Percent() { return pvca5000_Pnd_A_Ft_Pc_Percent; }

    public DbsField getPvca5000_Pnd_A_Ft_Ann_Whold_Type() { return pvca5000_Pnd_A_Ft_Ann_Whold_Type; }

    public DbsField getPvca5000_Pnd_A_Ft_Ann_Da_Dollar_Amt() { return pvca5000_Pnd_A_Ft_Ann_Da_Dollar_Amt; }

    public DbsField getPvca5000_Pnd_A_Ft_Ann_Pc_Percent() { return pvca5000_Pnd_A_Ft_Ann_Pc_Percent; }

    public DbsField getPvca5000_Pnd_A_Ft_Re_Dollar_Amt() { return pvca5000_Pnd_A_Ft_Re_Dollar_Amt; }

    public DbsField getPvca5000_Pnd_A_Ft_Re_Percent() { return pvca5000_Pnd_A_Ft_Re_Percent; }

    public DbsField getPvca5000_Pnd_A_Ft_N2_Dollar_Amt() { return pvca5000_Pnd_A_Ft_N2_Dollar_Amt; }

    public DbsField getPvca5000_Pnd_A_Ft_N2_Percent() { return pvca5000_Pnd_A_Ft_N2_Percent; }

    public DbsField getPvca5000_Pnd_A_Ft_N3_Status() { return pvca5000_Pnd_A_Ft_N3_Status; }

    public DbsField getPvca5000_Pnd_A_Ft_N3_Nbr_Whold_Allow() { return pvca5000_Pnd_A_Ft_N3_Nbr_Whold_Allow; }

    public DbsField getPvca5000_Pnd_A_Ft_Op_Dollar_Amt() { return pvca5000_Pnd_A_Ft_Op_Dollar_Amt; }

    public DbsField getPvca5000_Pnd_A_Ft_Cv_Dollar_Amt() { return pvca5000_Pnd_A_Ft_Cv_Dollar_Amt; }

    public DbsField getPvca5000_Pnd_A_Ft_Cv_Percent() { return pvca5000_Pnd_A_Ft_Cv_Percent; }

    public DbsField getPvca5000_Pnd_A_Bl_Primary_Name() { return pvca5000_Pnd_A_Bl_Primary_Name; }

    public DbsField getPvca5000_Pnd_A_Bl_Primary_Pct() { return pvca5000_Pnd_A_Bl_Primary_Pct; }

    public DbsField getPvca5000_Pnd_A_Bl_Primary_Dob() { return pvca5000_Pnd_A_Bl_Primary_Dob; }

    public DbsField getPvca5000_Pnd_A_Bl_Primary_Relation() { return pvca5000_Pnd_A_Bl_Primary_Relation; }

    public DbsField getPvca5000_Pnd_A_Bl_Contingent_Name() { return pvca5000_Pnd_A_Bl_Contingent_Name; }

    public DbsField getPvca5000_Pnd_A_Bl_Contingent_Pct() { return pvca5000_Pnd_A_Bl_Contingent_Pct; }

    public DbsField getPvca5000_Pnd_A_Bl_Contingent_Dob() { return pvca5000_Pnd_A_Bl_Contingent_Dob; }

    public DbsField getPvca5000_Pnd_A_Bl_Contingent_Relation() { return pvca5000_Pnd_A_Bl_Contingent_Relation; }

    public DbsField getPvca5000_Pnd_A_Di_Option_Cde() { return pvca5000_Pnd_A_Di_Option_Cde; }

    public DbsField getPvca5000_Pnd_A_Di_O2_Acct_Type() { return pvca5000_Pnd_A_Di_O2_Acct_Type; }

    public DbsField getPvca5000_Pnd_A_Di_O2_Amt() { return pvca5000_Pnd_A_Di_O2_Amt; }

    public DbsField getPvca5000_Pnd_A_Di_O2_Invest_Bank_Name() { return pvca5000_Pnd_A_Di_O2_Invest_Bank_Name; }

    public DbsField getPvca5000_Pnd_A_Di_O2_Address() { return pvca5000_Pnd_A_Di_O2_Address; }

    public DbsField getPvca5000_Pnd_A_Di_O2_City() { return pvca5000_Pnd_A_Di_O2_City; }

    public DbsField getPvca5000_Pnd_A_Di_O2_State() { return pvca5000_Pnd_A_Di_O2_State; }

    public DbsField getPvca5000_Pnd_A_Di_O2_Zip() { return pvca5000_Pnd_A_Di_O2_Zip; }

    public DbsField getPvca5000_Pnd_A_Di_O2_Bank_Acct_Phone() { return pvca5000_Pnd_A_Di_O2_Bank_Acct_Phone; }

    public DbsField getPvca5000_Pnd_A_Di_O2_Bank_Acct_Nbr() { return pvca5000_Pnd_A_Di_O2_Bank_Acct_Nbr; }

    public DbsField getPvca5000_Pnd_A_Di_O2_Bank_Routing_Nbr() { return pvca5000_Pnd_A_Di_O2_Bank_Routing_Nbr; }

    public DbsField getPvca5000_Pnd_A_Di_O3_Address_Type() { return pvca5000_Pnd_A_Di_O3_Address_Type; }

    public DbsField getPvca5000_Pnd_A_Di_O3_Address() { return pvca5000_Pnd_A_Di_O3_Address; }

    public DbsField getPvca5000_Pnd_A_Di_O3_City() { return pvca5000_Pnd_A_Di_O3_City; }

    public DbsField getPvca5000_Pnd_A_Di_O3_State() { return pvca5000_Pnd_A_Di_O3_State; }

    public DbsField getPvca5000_Pnd_A_Di_O3_Zip() { return pvca5000_Pnd_A_Di_O3_Zip; }

    public DbsField getPvca5000_Pnd_A_Di_O4_Roth_Ira_Acct() { return pvca5000_Pnd_A_Di_O4_Roth_Ira_Acct; }

    public DbsField getPvca5000_Pnd_A_Am_Option_Cde() { return pvca5000_Pnd_A_Am_Option_Cde; }

    public DbsField getPvca5000_Pnd_A_Am_O1_Payment_Years() { return pvca5000_Pnd_A_Am_O1_Payment_Years; }

    public DbsField getPvca5000_Pnd_A_Am_O1_Payment_Freq() { return pvca5000_Pnd_A_Am_O1_Payment_Freq; }

    public DbsField getPvca5000_Pnd_A_Am_O3_Guaranteed_Prd() { return pvca5000_Pnd_A_Am_O3_Guaranteed_Prd; }

    public DbsField getPvca5000_Pnd_A_Am_O3_Payment_Freq() { return pvca5000_Pnd_A_Am_O3_Payment_Freq; }

    public DbsField getPvca5000_Pnd_A_Po_Option_Cde() { return pvca5000_Pnd_A_Po_Option_Cde; }

    public DbsField getPvca5000_Pnd_A_Po_Tiaa_Traditional_Ind() { return pvca5000_Pnd_A_Po_Tiaa_Traditional_Ind; }

    public DbsField getPvca5000_Pnd_A_Po_Cref_Bond_Market_Ind() { return pvca5000_Pnd_A_Po_Cref_Bond_Market_Ind; }

    public DbsField getPvca5000_Pnd_A_Po_Tiaa_Real_Estate_Ind() { return pvca5000_Pnd_A_Po_Tiaa_Real_Estate_Ind; }

    public DbsField getPvca5000_Pnd_A_Po_Cref_Glbl_Equity_Ind() { return pvca5000_Pnd_A_Po_Cref_Glbl_Equity_Ind; }

    public DbsField getPvca5000_Pnd_A_Po_Cref_Stock_Ind() { return pvca5000_Pnd_A_Po_Cref_Stock_Ind; }

    public DbsField getPvca5000_Pnd_A_Po_Cref_Growth_Ind() { return pvca5000_Pnd_A_Po_Cref_Growth_Ind; }

    public DbsField getPvca5000_Pnd_A_Po_Cref_Money_Mrkt_Ind() { return pvca5000_Pnd_A_Po_Cref_Money_Mrkt_Ind; }

    public DbsField getPvca5000_Pnd_A_Po_Cref_Equity_Ndx_Ind() { return pvca5000_Pnd_A_Po_Cref_Equity_Ndx_Ind; }

    public DbsField getPvca5000_Pnd_A_Po_Cref_Social_Chc_Ind() { return pvca5000_Pnd_A_Po_Cref_Social_Chc_Ind; }

    public DbsField getPvca5000_Pnd_A_Po_Cref_Inf_Linked_Ind() { return pvca5000_Pnd_A_Po_Cref_Inf_Linked_Ind; }

    public DbsField getPvca5000_Pnd_A_Pm_Option_Cde() { return pvca5000_Pnd_A_Pm_Option_Cde; }

    public DbsField getPvca5000_Pnd_A_Pm_O1_This_Amt() { return pvca5000_Pnd_A_Pm_O1_This_Amt; }

    public DbsField getPvca5000_Pnd_A_Pm_O1_Graded_Prd_Amt() { return pvca5000_Pnd_A_Pm_O1_Graded_Prd_Amt; }

    public DbsField getPvca5000_Pnd_A_Pm_O4_Freq() { return pvca5000_Pnd_A_Pm_O4_Freq; }

    public DbsField getPvca5000_Pnd_A_Pm_O4_Start_Life_Income_Dte() { return pvca5000_Pnd_A_Pm_O4_Start_Life_Income_Dte; }

    public DbsField getPvca5000_Pnd_A_Pm_O4_Payment_Freq() { return pvca5000_Pnd_A_Pm_O4_Payment_Freq; }

    public DbsField getPvca5000_Pnd_A_Rw_Approx_Retire_Benefits() { return pvca5000_Pnd_A_Rw_Approx_Retire_Benefits; }

    public DbsField getPvca5000_Pnd_A_Rw_Asof_Date() { return pvca5000_Pnd_A_Rw_Asof_Date; }

    public DbsField getPvca5000_Pnd_A_Pi_Begin_Literal() { return pvca5000_Pnd_A_Pi_Begin_Literal; }

    public DbsField getPvca5000_Pnd_A_Pi_Export_Ind() { return pvca5000_Pnd_A_Pi_Export_Ind; }

    public DbsField getPvca5000_Pnd_A_Pi_Task_Id() { return pvca5000_Pnd_A_Pi_Task_Id; }

    public DbsField getPvca5000_Pnd_A_Pi_Task_Type() { return pvca5000_Pnd_A_Pi_Task_Type; }

    public DbsField getPvca5000_Pnd_A_Pi_Task_Guid() { return pvca5000_Pnd_A_Pi_Task_Guid; }

    public DbsField getPvca5000_Pnd_A_Pi_Action_Step() { return pvca5000_Pnd_A_Pi_Action_Step; }

    public DbsField getPvca5000_Pnd_A_Pi_Tiaa_Full_Date() { return pvca5000_Pnd_A_Pi_Tiaa_Full_Date; }

    public DbsField getPvca5000_Pnd_A_Pi_Tiaa_Time() { return pvca5000_Pnd_A_Pi_Tiaa_Time; }

    public DbsField getPvca5000_Pnd_A_Pi_Task_Status() { return pvca5000_Pnd_A_Pi_Task_Status; }

    public DbsField getPvca5000_Pnd_A_Pi_Ssn() { return pvca5000_Pnd_A_Pi_Ssn; }

    public DbsField getPvca5000_Pnd_A_Pi_Pin_Npin_Ppg() { return pvca5000_Pnd_A_Pi_Pin_Npin_Ppg; }

    public DbsField getPvca5000_Pnd_A_Pi_Pin_Type() { return pvca5000_Pnd_A_Pi_Pin_Type; }

    public DbsField getPvca5000_Pnd_A_Pi_Contract() { return pvca5000_Pnd_A_Pi_Contract; }

    public DbsField getPvca5000_Pnd_A_Pi_Plan_Id() { return pvca5000_Pnd_A_Pi_Plan_Id; }

    public DbsField getPvca5000_Pnd_A_Pi_Doc_Content() { return pvca5000_Pnd_A_Pi_Doc_Content; }

    public DbsField getPvca5000_Pnd_A_Pi_Filler_1() { return pvca5000_Pnd_A_Pi_Filler_1; }

    public DbsField getPvca5000_Pnd_A_Pi_Filler_2() { return pvca5000_Pnd_A_Pi_Filler_2; }

    public DbsField getPvca5000_Pnd_A_Pi_Filler_3() { return pvca5000_Pnd_A_Pi_Filler_3; }

    public DbsField getPvca5000_Pnd_A_Pi_Filler_4() { return pvca5000_Pnd_A_Pi_Filler_4; }

    public DbsField getPvca5000_Pnd_A_Pi_Filler_5() { return pvca5000_Pnd_A_Pi_Filler_5; }

    public DbsField getPvca5000_Pnd_A_Pi_End_Literal() { return pvca5000_Pnd_A_Pi_End_Literal; }

    public DbsField getPvca5000_Pnd_A_Periodic_Ind() { return pvca5000_Pnd_A_Periodic_Ind; }

    public DbsField getPvca5000_Pnd_A_Rollover_Ind() { return pvca5000_Pnd_A_Rollover_Ind; }

    public DbsField getPvca5000_Pnd_A_Wrong_Form_Ind() { return pvca5000_Pnd_A_Wrong_Form_Ind; }

    public DbsField getPvca5000_Pnd_A_Blurb_Text() { return pvca5000_Pnd_A_Blurb_Text; }

    public DbsField getPvca5000_Pnd_A_Payment_Freq_Ind() { return pvca5000_Pnd_A_Payment_Freq_Ind; }

    public DbsField getPvca5000_Pnd_A_Payment_Coupon_Ind() { return pvca5000_Pnd_A_Payment_Coupon_Ind; }

    public DbsField getPvca5000_Pnd_A_Payment_Coupon_Tiaa_Amount() { return pvca5000_Pnd_A_Payment_Coupon_Tiaa_Amount; }

    public DbsField getPvca5000_Pnd_A_Payment_Coupon_Cref_Amount() { return pvca5000_Pnd_A_Payment_Coupon_Cref_Amount; }

    public DbsField getPvca5000_Pnd_A_Payment_Coupon_Total_Amount() { return pvca5000_Pnd_A_Payment_Coupon_Total_Amount; }

    public DbsField getPvca5000_Pnd_A_Misc_Bene_Prov_Form_Date() { return pvca5000_Pnd_A_Misc_Bene_Prov_Form_Date; }

    public DbsField getPvca5000_Pnd_A_Benefit_Type_Ind() { return pvca5000_Pnd_A_Benefit_Type_Ind; }

    public DbsField getPvca5000_Pnd_A_Da_5k_Ind() { return pvca5000_Pnd_A_Da_5k_Ind; }

    public DbsField getPvca5000_Pnd_A_Sip_Tiaa_Nbr() { return pvca5000_Pnd_A_Sip_Tiaa_Nbr; }

    public DbsField getPvca5000_Pnd_A_Sip_Cref_Nbr() { return pvca5000_Pnd_A_Sip_Cref_Nbr; }

    public DbsField getPvca5000_Pnd_A_Ssum_Tiaa_Nbr() { return pvca5000_Pnd_A_Ssum_Tiaa_Nbr; }

    public DbsField getPvca5000_Pnd_A_Ssum_Cref_Nbr() { return pvca5000_Pnd_A_Ssum_Cref_Nbr; }

    public DbsField getPvca5000_Pnd_A_Spouse_Rmd_Bene_Ind() { return pvca5000_Pnd_A_Spouse_Rmd_Bene_Ind; }

    public DbsField getPvca5000_Pnd_A_Entity_Rmd_Ind() { return pvca5000_Pnd_A_Entity_Rmd_Ind; }

    public DbsField getPvca5000_Pnd_A_Non_Spouse_Rmd_Bene_Ind() { return pvca5000_Pnd_A_Non_Spouse_Rmd_Bene_Ind; }

    public DbsField getPvca5000_Pnd_A_Second_Gen_Rmd_Ind() { return pvca5000_Pnd_A_Second_Gen_Rmd_Ind; }

    public DbsField getPvca5000_Pnd_A_Phone_Acceptance_Ind() { return pvca5000_Pnd_A_Phone_Acceptance_Ind; }

    public DbsField getPvca5000_Pnd_A_Register_Online_Acct_Ind() { return pvca5000_Pnd_A_Register_Online_Acct_Ind; }

    public DbsField getPvca5000_Pnd_A_Trust_Relation_Para_Ind() { return pvca5000_Pnd_A_Trust_Relation_Para_Ind; }

    public DbsField getPvca5000_Pnd_A_Brokerage_Retail_Ind() { return pvca5000_Pnd_A_Brokerage_Retail_Ind; }

    public DbsField getPvca5000_Pnd_A_Brokerage_Ira_Ind() { return pvca5000_Pnd_A_Brokerage_Ira_Ind; }

    public DbsField getPvca5000_Pnd_A_Brokerage_Ira_Roth_Ind() { return pvca5000_Pnd_A_Brokerage_Ira_Roth_Ind; }

    public DbsField getPvca5000_Pnd_A_Sole_Spouse_Ira_Rmd_Ind() { return pvca5000_Pnd_A_Sole_Spouse_Ira_Rmd_Ind; }

    public DbsField getPvca5000_Pnd_A_Sole_Spouse_Atra_Rmd_Ind() { return pvca5000_Pnd_A_Sole_Spouse_Atra_Rmd_Ind; }

    public DbsField getPvca5000_Pnd_A_Sole_Spouse_Both_Rmd_Ind() { return pvca5000_Pnd_A_Sole_Spouse_Both_Rmd_Ind; }

    public DbsField getPvca5000_Pnd_A_Accept_Form_Spouse_Ind() { return pvca5000_Pnd_A_Accept_Form_Spouse_Ind; }

    public DbsField getPvca5000_Pnd_A_Accept_Form_Bene_Ind() { return pvca5000_Pnd_A_Accept_Form_Bene_Ind; }

    public DbsField getPvca5000_Pnd_A_Accept_Form_Entity_Ind() { return pvca5000_Pnd_A_Accept_Form_Entity_Ind; }

    public DbsField getPvca5000_Pnd_A_Onetime_Transfer_Spouse_Ind() { return pvca5000_Pnd_A_Onetime_Transfer_Spouse_Ind; }

    public DbsField getPvca5000_Pnd_A_Onetime_Transfer_Bene_Ind() { return pvca5000_Pnd_A_Onetime_Transfer_Bene_Ind; }

    public DbsField getPvca5000_Pnd_A_Tpa_Cont_Spouse_Ind() { return pvca5000_Pnd_A_Tpa_Cont_Spouse_Ind; }

    public DbsField getPvca5000_Pnd_A_Tpa_Cont_Bene_Ind() { return pvca5000_Pnd_A_Tpa_Cont_Bene_Ind; }

    public DbsField getPvca5000_Pnd_A_Cash_Wdraw_Page_Spouse_Ind() { return pvca5000_Pnd_A_Cash_Wdraw_Page_Spouse_Ind; }

    public DbsField getPvca5000_Pnd_A_Cash_Wdraw_Page_Bene_Ind() { return pvca5000_Pnd_A_Cash_Wdraw_Page_Bene_Ind; }

    public DbsField getPvca5000_Pnd_A_Rollover_Page_Spouse_Ind() { return pvca5000_Pnd_A_Rollover_Page_Spouse_Ind; }

    public DbsField getPvca5000_Pnd_A_Rollover_Page_Bene_Ind() { return pvca5000_Pnd_A_Rollover_Page_Bene_Ind; }

    public DbsField getPvca5000_Pnd_A_Customer_Agree_Retail_Ind() { return pvca5000_Pnd_A_Customer_Agree_Retail_Ind; }

    public DbsField getPvca5000_Pnd_A_Affidavit_Domicile_Ind() { return pvca5000_Pnd_A_Affidavit_Domicile_Ind; }

    public DbsField getPvca5000_Pnd_A_Cert_Invest_Powers_Ind() { return pvca5000_Pnd_A_Cert_Invest_Powers_Ind; }

    public DbsField getPvca5000_Pnd_A_Power_Of_Atty_Ind() { return pvca5000_Pnd_A_Power_Of_Atty_Ind; }

    public DbsField getPvca5000_Pnd_A_Loi_Existing_Ind() { return pvca5000_Pnd_A_Loi_Existing_Ind; }

    public DbsField getPvca5000_Pnd_A_Loi_Trust_Ind() { return pvca5000_Pnd_A_Loi_Trust_Ind; }

    public DbsField getPvca5000_Pnd_A_Loi_New_Ind() { return pvca5000_Pnd_A_Loi_New_Ind; }

    public DbsField getPvca5000_Pnd_A_Loi_Jtwros_Ind() { return pvca5000_Pnd_A_Loi_Jtwros_Ind; }

    public DbsField getPvca5000_Pnd_A_Onetime_Transfer_Amt() { return pvca5000_Pnd_A_Onetime_Transfer_Amt; }

    public DbsField getPvca5000_Pnd_A_Second_Generation_Amt() { return pvca5000_Pnd_A_Second_Generation_Amt; }

    public DbsField getPvca5000_Pnd_A_Brokerage_Retail_Amt() { return pvca5000_Pnd_A_Brokerage_Retail_Amt; }

    public DbsField getPvca5000_Pnd_A_Brokerage_Ira_Amt() { return pvca5000_Pnd_A_Brokerage_Ira_Amt; }

    public DbsField getPvca5000_Pnd_A_Brokerage_Ira_Roth_Amt() { return pvca5000_Pnd_A_Brokerage_Ira_Roth_Amt; }

    public DbsField getPvca5000_Pnd_A_Brokerage_Retail_Acct_Nbr() { return pvca5000_Pnd_A_Brokerage_Retail_Acct_Nbr; }

    public DbsField getPvca5000_Pnd_A_Brokerage_Ira_Acct_Nbr() { return pvca5000_Pnd_A_Brokerage_Ira_Acct_Nbr; }

    public DbsField getPvca5000_Pnd_A_Brokerage_Roth_Acct_Nbr() { return pvca5000_Pnd_A_Brokerage_Roth_Acct_Nbr; }

    public DbsField getPvca5000_Pnd_A_Ro_Account_Nbr() { return pvca5000_Pnd_A_Ro_Account_Nbr; }

    public DbsField getPvca5000_Pnd_A_Di_O4_Tiaa_Acct_Type() { return pvca5000_Pnd_A_Di_O4_Tiaa_Acct_Type; }

    public DbsField getPvca5000_Pnd_A_Di_O4_Careof() { return pvca5000_Pnd_A_Di_O4_Careof; }

    public DbsField getPvca5000_Pnd_A_Loi_Ira_Ind() { return pvca5000_Pnd_A_Loi_Ira_Ind; }

    public DbsField getPvca5000_Pnd_A_Loi_Estate_Ind() { return pvca5000_Pnd_A_Loi_Estate_Ind; }

    public DbsField getPvca5000_Pnd_A_Tpa_Page() { return pvca5000_Pnd_A_Tpa_Page; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pvca5000 = dbsRecord.newGroupInRecord("pvca5000", "PVCA5000");
        pvca5000.setParameterOption(ParameterOption.ByReference);
        pvca5000_Pvca5000_Array = pvca5000.newFieldArrayInGroup("pvca5000_Pvca5000_Array", "PVCA5000-ARRAY", FieldType.STRING, 1, new DbsArrayController(1,
            7668));
        pvca5000_Pvca5000_ArrayRedef1 = pvca5000.newGroupInGroup("pvca5000_Pvca5000_ArrayRedef1", "Redefines", pvca5000_Pvca5000_Array);
        pvca5000_Pnd_Result_Code = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_Result_Code", "#RESULT-CODE", FieldType.STRING, 4);
        pvca5000_Pnd_Result_Msg = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_Result_Msg", "#RESULT-MSG", FieldType.STRING, 100);
        pvca5000_Pnd_More_Data = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_More_Data", "#MORE-DATA", FieldType.STRING, 1);
        pvca5000_Pnd_A_Universal_Id = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Universal_Id", "#A-UNIVERSAL-ID", FieldType.STRING, 
            12);
        pvca5000_Pnd_A_Universal_Id_Type = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Universal_Id_Type", "#A-UNIVERSAL-ID-TYPE", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Requestor_Title = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Requestor_Title", "#A-REQUESTOR-TITLE", FieldType.STRING, 
            20);
        pvca5000_Pnd_A_Requestor_Suffix = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Requestor_Suffix", "#A-REQUESTOR-SUFFIX", FieldType.STRING, 
            20);
        pvca5000_Pnd_A_Requestor_Trust_Name_1 = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Requestor_Trust_Name_1", "#A-REQUESTOR-TRUST-NAME-1", 
            FieldType.STRING, 45);
        pvca5000_Pnd_A_Requestor_Trust_Name_2 = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Requestor_Trust_Name_2", "#A-REQUESTOR-TRUST-NAME-2", 
            FieldType.STRING, 45);
        pvca5000_Pnd_A_Requestor_Name = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Requestor_Name", "#A-REQUESTOR-NAME", FieldType.STRING, 
            40);
        pvca5000_Pnd_A_Requestor_Addr1 = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Requestor_Addr1", "#A-REQUESTOR-ADDR1", FieldType.STRING, 
            40);
        pvca5000_Pnd_A_Requestor_Addr2 = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Requestor_Addr2", "#A-REQUESTOR-ADDR2", FieldType.STRING, 
            40);
        pvca5000_Pnd_A_Requestor_Addr3 = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Requestor_Addr3", "#A-REQUESTOR-ADDR3", FieldType.STRING, 
            40);
        pvca5000_Pnd_A_Requestor_Addr4 = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Requestor_Addr4", "#A-REQUESTOR-ADDR4", FieldType.STRING, 
            40);
        pvca5000_Pnd_A_Requestor_Addr5 = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Requestor_Addr5", "#A-REQUESTOR-ADDR5", FieldType.STRING, 
            40);
        pvca5000_Pnd_A_Requestor_City = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Requestor_City", "#A-REQUESTOR-CITY", FieldType.STRING, 
            40);
        pvca5000_Pnd_A_Requestor_Zip = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Requestor_Zip", "#A-REQUESTOR-ZIP", FieldType.STRING, 
            9);
        pvca5000_Pnd_A_Requestor_State = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Requestor_State", "#A-REQUESTOR-STATE", FieldType.STRING, 
            2);
        pvca5000_Pnd_A_Requestor_Addr_Typ = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Requestor_Addr_Typ", "#A-REQUESTOR-ADDR-TYP", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Requestor_Country = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Requestor_Country", "#A-REQUESTOR-COUNTRY", FieldType.STRING, 
            40);
        pvca5000_Pnd_A_Requestor_Trust_Or_Est = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Requestor_Trust_Or_Est", "#A-REQUESTOR-TRUST-OR-EST", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Requestor_Care = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Requestor_Care", "#A-REQUESTOR-CARE", FieldType.STRING, 
            40);
        pvca5000_Pnd_A_Altr_Requestor_Title = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Altr_Requestor_Title", "#A-ALTR-REQUESTOR-TITLE", 
            FieldType.STRING, 20);
        pvca5000_Pnd_A_Altr_Requestor_Suffix = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Altr_Requestor_Suffix", "#A-ALTR-REQUESTOR-SUFFIX", 
            FieldType.STRING, 20);
        pvca5000_Pnd_A_Altr_Requestor_Name = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Altr_Requestor_Name", "#A-ALTR-REQUESTOR-NAME", 
            FieldType.STRING, 40);
        pvca5000_Pnd_A_Altr_Requestor_Addr1 = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Altr_Requestor_Addr1", "#A-ALTR-REQUESTOR-ADDR1", 
            FieldType.STRING, 40);
        pvca5000_Pnd_A_Altr_Requestor_Addr2 = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Altr_Requestor_Addr2", "#A-ALTR-REQUESTOR-ADDR2", 
            FieldType.STRING, 40);
        pvca5000_Pnd_A_Altr_Requestor_City = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Altr_Requestor_City", "#A-ALTR-REQUESTOR-CITY", 
            FieldType.STRING, 40);
        pvca5000_Pnd_A_Altr_Requestor_Zip = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Altr_Requestor_Zip", "#A-ALTR-REQUESTOR-ZIP", 
            FieldType.STRING, 9);
        pvca5000_Pnd_A_Altr_Requestor_State = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Altr_Requestor_State", "#A-ALTR-REQUESTOR-STATE", 
            FieldType.STRING, 2);
        pvca5000_Pnd_A_Altr_Requestor_Addr_Typ = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Altr_Requestor_Addr_Typ", "#A-ALTR-REQUESTOR-ADDR-TYP", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Altr_Requestor_Country = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Altr_Requestor_Country", "#A-ALTR-REQUESTOR-COUNTRY", 
            FieldType.STRING, 40);
        pvca5000_Pnd_A_Altr_Requestor_Trust_Or_Est = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Altr_Requestor_Trust_Or_Est", "#A-ALTR-REQUESTOR-TRUST-OR-EST", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Altr_Requestor_Care = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Altr_Requestor_Care", "#A-ALTR-REQUESTOR-CARE", 
            FieldType.STRING, 40);
        pvca5000_Pnd_A_Salutation_Text = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Salutation_Text", "#A-SALUTATION-TEXT", FieldType.STRING, 
            100);
        pvca5000_Pnd_A_Local_Printer = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Local_Printer", "#A-LOCAL-PRINTER", FieldType.STRING, 
            8);
        pvca5000_Pnd_A_Immediate_Printer_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Immediate_Printer_Ind", "#A-IMMEDIATE-PRINTER-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Signatory_Racf = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Signatory_Racf", "#A-SIGNATORY-RACF", FieldType.STRING, 
            8);
        pvca5000_Pnd_A_Signatory_Racf_Unit = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Signatory_Racf_Unit", "#A-SIGNATORY-RACF-UNIT", 
            FieldType.STRING, 8);
        pvca5000_Pnd_A_Signatory_Racf_Name = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Signatory_Racf_Name", "#A-SIGNATORY-RACF-NAME", 
            FieldType.STRING, 40);
        pvca5000_Pnd_A_Letter_Type = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Letter_Type", "#A-LETTER-TYPE", FieldType.STRING, 2);
        pvca5000_Pnd_A_Case_Manager_Name = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Case_Manager_Name", "#A-CASE-MANAGER-NAME", FieldType.STRING, 
            40);
        pvca5000_Pnd_A_Case_Manage_Phone = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Case_Manage_Phone", "#A-CASE-MANAGE-PHONE", FieldType.STRING, 
            6);
        pvca5000_Pnd_A_Institution_Name = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Institution_Name", "#A-INSTITUTION-NAME", FieldType.STRING, 
            60);
        pvca5000_Pnd_A_Decedent_Pin_Nbr = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Decedent_Pin_Nbr", "#A-DECEDENT-PIN-NBR", FieldType.STRING, 
            12);
        pvca5000_Pnd_A_Decedent_Other_Than_Org = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Decedent_Other_Than_Org", "#A-DECEDENT-OTHER-THAN-ORG", 
            FieldType.STRING, 40);
        pvca5000_Pnd_A_Decedent_Name = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Decedent_Name", "#A-DECEDENT-NAME", FieldType.STRING, 
            40);
        pvca5000_Pnd_A_Decedent_Date_Of_Birth = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Decedent_Date_Of_Birth", "#A-DECEDENT-DATE-OF-BIRTH", 
            FieldType.NUMERIC, 8);
        pvca5000_Pnd_A_Decedent_Date_Of_Death = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Decedent_Date_Of_Death", "#A-DECEDENT-DATE-OF-DEATH", 
            FieldType.NUMERIC, 8);
        pvca5000_Pnd_A_Cont_Beneficiary_Name = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Cont_Beneficiary_Name", "#A-CONT-BENEFICIARY-NAME", 
            FieldType.STRING, 100);
        pvca5000_Pnd_A_Minor_Org_Estate_Name = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Minor_Org_Estate_Name", "#A-MINOR-ORG-ESTATE-NAME", 
            FieldType.STRING, 60);
        pvca5000_Pnd_A_Trust_Name = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Trust_Name", "#A-TRUST-NAME", FieldType.STRING, 60);
        pvca5000_Pnd_A_Children_Drop_Down = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Children_Drop_Down", "#A-CHILDREN-DROP-DOWN", 
            FieldType.STRING, 60);
        pvca5000_Pnd_A_Pages_Missing = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Pages_Missing", "#A-PAGES-MISSING", FieldType.STRING, 
            20);
        pvca5000_Pnd_A_Death_Cert_Bene_Names = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Death_Cert_Bene_Names", "#A-DEATH-CERT-BENE-NAMES", 
            FieldType.STRING, 100);
        pvca5000_Pnd_A_Pvc_Delivery_Type = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Pvc_Delivery_Type", "#A-PVC-DELIVERY-TYPE", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Survivor_Task_Category = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Survivor_Task_Category", "#A-SURVIVOR-TASK-CATEGORY", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Unclaimed_Funds_State_Name = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Unclaimed_Funds_State_Name", "#A-UNCLAIMED-FUNDS-STATE-NAME", 
            FieldType.STRING, 20);
        pvca5000_Pnd_A_Unclaimed_Funds_State_Cde = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Unclaimed_Funds_State_Cde", "#A-UNCLAIMED-FUNDS-STATE-CDE", 
            FieldType.STRING, 2);
        pvca5000_Pnd_A_Unclaimed_Funds_Contract_Nbr = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Unclaimed_Funds_Contract_Nbr", "#A-UNCLAIMED-FUNDS-CONTRACT-NBR", 
            FieldType.STRING, 8);
        pvca5000_Pnd_A_Brm_Name = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Brm_Name", "#A-BRM-NAME", FieldType.STRING, 40);
        pvca5000_Pnd_A_Footer_Location = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Footer_Location", "#A-FOOTER-LOCATION", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Renun_Waiver_Release_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Renun_Waiver_Release_Ind", "#A-RENUN-WAIVER-RELEASE-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Marital_Stat_Addendum_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Marital_Stat_Addendum_Ind", "#A-MARITAL-STAT-ADDENDUM-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Release_Indem_Agree_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Release_Indem_Agree_Ind", "#A-RELEASE-INDEM-AGREE-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Savings_Invest_Appl_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Savings_Invest_Appl_Ind", "#A-SAVINGS-INVEST-APPL-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Survivor_Bene_Dist_Rqst_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Survivor_Bene_Dist_Rqst_Ind", "#A-SURVIVOR-BENE-DIST-RQST-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Income_Annty_Request_Form_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Income_Annty_Request_Form_Ind", "#A-INCOME-ANNTY-REQUEST-FORM-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Immediate_Annuity_Form_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Immediate_Annuity_Form_Ind", "#A-IMMEDIATE-ANNUITY-FORM-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_One_Time_Pay_Request_Form_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_One_Time_Pay_Request_Form_Ind", "#A-ONE-TIME-PAY-REQUEST-FORM-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Da_Tpa_Desig_Form_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Da_Tpa_Desig_Form_Ind", "#A-DA-TPA-DESIG-FORM-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Da_Tpa_Desig_Form_Ctr = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Da_Tpa_Desig_Form_Ctr", "#A-DA-TPA-DESIG-FORM-CTR", 
            FieldType.NUMERIC, 1);
        pvca5000_Pnd_A_Ia_Beneficiary_Form_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ia_Beneficiary_Form_Ind", "#A-IA-BENEFICIARY-FORM-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Ia_Beneficiary_Form_Ctr = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ia_Beneficiary_Form_Ctr", "#A-IA-BENEFICIARY-FORM-CTR", 
            FieldType.NUMERIC, 1);
        pvca5000_Pnd_A_Ohio_Arp_Bene_Form_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ohio_Arp_Bene_Form_Ind", "#A-OHIO-ARP-BENE-FORM-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Ohio_Arp_Bene_Form_Ctr = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ohio_Arp_Bene_Form_Ctr", "#A-OHIO-ARP-BENE-FORM-CTR", 
            FieldType.NUMERIC, 1);
        pvca5000_Pnd_A_Univ_Pittsburgh_Bene_Form_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Univ_Pittsburgh_Bene_Form_Ind", "#A-UNIV-PITTSBURGH-BENE-FORM-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Univ_Pittsburgh_Bene_Form_Ctr = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Univ_Pittsburgh_Bene_Form_Ctr", "#A-UNIV-PITTSBURGH-BENE-FORM-CTR", 
            FieldType.NUMERIC, 1);
        pvca5000_Pnd_A_Univ_Michigan_Bene_Form_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Univ_Michigan_Bene_Form_Ind", "#A-UNIV-MICHIGAN-BENE-FORM-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Univ_Michigan_Bene_Form_Ctr = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Univ_Michigan_Bene_Form_Ctr", "#A-UNIV-MICHIGAN-BENE-FORM-CTR", 
            FieldType.NUMERIC, 1);
        pvca5000_Pnd_A_Successr_Beneficiary_Form_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Successr_Beneficiary_Form_Ind", "#A-SUCCESSR-BENEFICIARY-FORM-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Successr_Beneficiary_Form_Ctr = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Successr_Beneficiary_Form_Ctr", "#A-SUCCESSR-BENEFICIARY-FORM-CTR", 
            FieldType.NUMERIC, 1);
        pvca5000_Pnd_A_Ohio_Consent_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ohio_Consent_Ind", "#A-OHIO-CONSENT-IND", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Oklahoma_Consent_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Oklahoma_Consent_Ind", "#A-OKLAHOMA-CONSENT-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Indiana_Notice_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Indiana_Notice_Ind", "#A-INDIANA-NOTICE-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Rhode_Island_Notice_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Rhode_Island_Notice_Ind", "#A-RHODE-ISLAND-NOTICE-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_W9_Form_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_W9_Form_Ind", "#A-W9-FORM-IND", FieldType.STRING, 1);
        pvca5000_Pnd_A_W8ben_Form_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_W8ben_Form_Ind", "#A-W8BEN-FORM-IND", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_W7_Form_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_W7_Form_Ind", "#A-W7-FORM-IND", FieldType.STRING, 1);
        pvca5000_Pnd_A_State_Code_Taxint = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_State_Code_Taxint", "#A-STATE-CODE-TAXINT", FieldType.STRING, 
            2);
        pvca5000_Pnd_A_State_Name_Taxint = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_State_Name_Taxint", "#A-STATE-NAME-TAXINT", FieldType.STRING, 
            20);
        pvca5000_Pnd_A_State_Name_Text = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_State_Name_Text", "#A-STATE-NAME-TEXT", FieldType.STRING, 
            20);
        pvca5000_Pnd_A_Loved_One_Passes_Away_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Loved_One_Passes_Away_Ind", "#A-LOVED-ONE-PASSES-AWAY-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Sip_Booklet_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Sip_Booklet_Ind", "#A-SIP-BOOKLET-IND", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Taking_Next_Step_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Taking_Next_Step_Ind", "#A-TAKING-NEXT-STEP-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Qtrly_Performance_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Qtrly_Performance_Ind", "#A-QTRLY-PERFORMANCE-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Tiaa_Cref_Prospectus_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Tiaa_Cref_Prospectus_Ind", "#A-TIAA-CREF-PROSPECTUS-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Cref_Prospectus_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Cref_Prospectus_Ind", "#A-CREF-PROSPECTUS-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Tiaa_Rea_Prospectus_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Tiaa_Rea_Prospectus_Ind", "#A-TIAA-REA-PROSPECTUS-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Lifecycle_Prospectus_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Lifecycle_Prospectus_Ind", "#A-LIFECYCLE-PROSPECTUS-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Tiaa_Rate_Sheet_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Tiaa_Rate_Sheet_Ind", "#A-TIAA-RATE-SHEET-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Atra_Sip_Glance_Indvl_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Atra_Sip_Glance_Indvl_Ind", "#A-ATRA-SIP-GLANCE-INDVL-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Atra_Ssum_Glance_Trust_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Atra_Ssum_Glance_Trust_Ind", "#A-ATRA-SSUM-GLANCE-TRUST-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Atra_Indvl_Bene_Glance_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Atra_Indvl_Bene_Glance_Ind", "#A-ATRA-INDVL-BENE-GLANCE-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Atra_Ssum_Glance_Estate_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Atra_Ssum_Glance_Estate_Ind", "#A-ATRA-SSUM-GLANCE-ESTATE-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Atra_Annty_Glance_Indvl_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Atra_Annty_Glance_Indvl_Ind", "#A-ATRA-ANNTY-GLANCE-INDVL-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Atra_Fix_Prd_Ann_Glance_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Atra_Fix_Prd_Ann_Glance_Ind", "#A-ATRA-FIX-PRD-ANN-GLANCE-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Ssum_Glance_Nonspouse_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ssum_Glance_Nonspouse_Ind", "#A-SSUM-GLANCE-NONSPOUSE-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Ssum_Glance_Spouse_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ssum_Glance_Spouse_Ind", "#A-SSUM-GLANCE-SPOUSE-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Ssum_Glance_Trust_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ssum_Glance_Trust_Ind", "#A-SSUM-GLANCE-TRUST-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Ssum_Glance_Estate_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ssum_Glance_Estate_Ind", "#A-SSUM-GLANCE-ESTATE-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Notice_402f_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Notice_402f_Ind", "#A-NOTICE-402F-IND", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Custodial_Agreement_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Custodial_Agreement_Ind", "#A-CUSTODIAL-AGREEMENT-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Record_Of_Age_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Record_Of_Age_Ind", "#A-RECORD-OF-AGE-IND", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Payment_Dest_Form_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Payment_Dest_Form_Ind", "#A-PAYMENT-DEST-FORM-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Transfer_At_Settlement_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Transfer_At_Settlement_Ind", "#A-TRANSFER-AT-SETTLEMENT-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Doma_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Doma_Ind", "#A-DOMA-IND", FieldType.STRING, 1);
        pvca5000_Pnd_A_Minor_Sa = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Minor_Sa", "#A-MINOR-SA", FieldType.STRING, 1);
        pvca5000_Pnd_A_My_Children_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_My_Children_Ind", "#A-MY-CHILDREN-IND", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Minor_Guardian_Hip_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Minor_Guardian_Hip_Ind", "#A-MINOR-GUARDIAN-HIP-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Exspouse_Divorce_Decree_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Exspouse_Divorce_Decree_Ind", "#A-EXSPOUSE-DIVORCE-DECREE-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Estate_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Estate_Ind", "#A-ESTATE-IND", FieldType.STRING, 1);
        pvca5000_Pnd_A_Small_Estate_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Small_Estate_Ind", "#A-SMALL-ESTATE-IND", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Unable_To_Locate_Trust_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Unable_To_Locate_Trust_Ind", "#A-UNABLE-TO-LOCATE-TRUST-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Need_Bene_Address_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Need_Bene_Address_Ind", "#A-NEED-BENE-ADDRESS-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Organization_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Organization_Ind", "#A-ORGANIZATION-IND", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Msv_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Msv_Ind", "#A-MSV-IND", FieldType.STRING, 1);
        pvca5000_Pnd_A_Roth_Accumulation_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Roth_Accumulation_Ind", "#A-ROTH-ACCUMULATION-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Atra_Accumulation_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Atra_Accumulation_Ind", "#A-ATRA-ACCUMULATION-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Investment_Contract_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Investment_Contract_Ind", "#A-INVESTMENT-CONTRACT-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Allocation_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Allocation_Ind", "#A-ALLOCATION-IND", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Blank_Beneficiaries_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Blank_Beneficiaries_Ind", "#A-BLANK-BENEFICIARIES-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Contract_Numbers_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Contract_Numbers_Ind", "#A-CONTRACT-NUMBERS-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Invalid_Date_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Invalid_Date_Ind", "#A-INVALID-DATE-IND", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Estate_Trust_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Estate_Trust_Ind", "#A-ESTATE-TRUST-IND", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Executor_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Executor_Ind", "#A-EXECUTOR-IND", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Fax_Copies_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Fax_Copies_Ind", "#A-FAX-COPIES-IND", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Guarantee_Period_Expire_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Guarantee_Period_Expire_Ind", "#A-GUARANTEE-PERIOD-EXPIRE-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_No_Guarantee_Period_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_No_Guarantee_Period_Ind", "#A-NO-GUARANTEE-PERIOD-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Indvl_Bene_Trust_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Indvl_Bene_Trust_Ind", "#A-INDVL-BENE-TRUST-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Institution_Maintained_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Institution_Maintained_Ind", "#A-INSTITUTION-MAINTAINED-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Irrevocable_Designation_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Irrevocable_Designation_Ind", "#A-IRREVOCABLE-DESIGNATION-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Missing_Pages_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Missing_Pages_Ind", "#A-MISSING-PAGES-IND", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Inactive_Contracts_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Inactive_Contracts_Ind", "#A-INACTIVE-CONTRACTS-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Or_Used_In_Designation_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Or_Used_In_Designation_Ind", "#A-OR-USED-IN-DESIGNATION-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Same_Primary_Contingent_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Same_Primary_Contingent_Ind", "#A-SAME-PRIMARY-CONTINGENT-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Siblings_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Siblings_Ind", "#A-SIBLINGS-IND", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_No_Signature_Date_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_No_Signature_Date_Ind", "#A-NO-SIGNATURE-DATE-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Spouse_As_Primary_Bene_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Spouse_As_Primary_Bene_Ind", "#A-SPOUSE-AS-PRIMARY-BENE-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Surs_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Surs_Ind", "#A-SURS-IND", FieldType.STRING, 1);
        pvca5000_Pnd_A_Age_Stipulation_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Age_Stipulation_Ind", "#A-AGE-STIPULATION-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Deceased_Child_Chk_Gchild_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Deceased_Child_Chk_Gchild_Ind", "#A-DECEASED-CHILD-CHK-GCHILD-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Dec_Child_Bxchk_Schild_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Dec_Child_Bxchk_Schild_Ind", "#A-DEC-CHILD-BXCHK-SCHILD-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Difficult_To_Read_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Difficult_To_Read_Ind", "#A-DIFFICULT-TO-READ-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Divesting_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Divesting_Ind", "#A-DIVESTING-IND", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_For_The_Benefit_Of_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_For_The_Benefit_Of_Ind", "#A-FOR-THE-BENEFIT-OF-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Guardian_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Guardian_Ind", "#A-GUARDIAN-IND", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Mdo_Contract_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Mdo_Contract_Ind", "#A-MDO-CONTRACT-IND", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Missing_Information_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Missing_Information_Ind", "#A-MISSING-INFORMATION-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Department_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Department_Ind", "#A-DEPARTMENT-IND", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_No_Primary_Bene_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_No_Primary_Bene_Ind", "#A-NO-PRIMARY-BENE-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Second_Annuitant_Named_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Second_Annuitant_Named_Ind", "#A-SECOND-ANNUITANT-NAMED-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Ssn_Not_Shown_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ssn_Not_Shown_Ind", "#A-SSN-NOT-SHOWN-IND", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Spouse_Another_Person_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Spouse_Another_Person_Ind", "#A-SPOUSE-ANOTHER-PERSON-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Spousal_Waiver_Inc_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Spousal_Waiver_Inc_Ind", "#A-SPOUSAL-WAIVER-INC-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Survivorship_Clause = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Survivorship_Clause", "#A-SURVIVORSHIP-CLAUSE", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Tin_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Tin_Ind", "#A-TIN-IND", FieldType.STRING, 1);
        pvca5000_Pnd_A_Terms_Of_Trust_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Terms_Of_Trust_Ind", "#A-TERMS-OF-TRUST-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Trust_Date_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Trust_Date_Ind", "#A-TRUST-DATE-IND", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Unborn_Children_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Unborn_Children_Ind", "#A-UNBORN-CHILDREN-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Unnamed_Spouse_Of_Bene_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Unnamed_Spouse_Of_Bene_Ind", "#A-UNNAMED-SPOUSE-OF-BENE-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_No_Designation_Found_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_No_Designation_Found_Ind", "#A-NO-DESIGNATION-FOUND-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Poor_Copy_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Poor_Copy_Ind", "#A-POOR-COPY-IND", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Retirement_Loan_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Retirement_Loan_Ind", "#A-RETIREMENT-LOAN-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Wpahs_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Wpahs_Ind", "#A-WPAHS-IND", FieldType.STRING, 1);
        pvca5000_Pnd_A_Erisa_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Erisa_Ind", "#A-ERISA-IND", FieldType.STRING, 1);
        pvca5000_Pnd_A_Death_Certificate_Reqrd_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Death_Certificate_Reqrd_Ind", "#A-DEATH-CERTIFICATE-REQRD-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Death_Cert_Primary_Bene_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Death_Cert_Primary_Bene_Ind", "#A-DEATH-CERT-PRIMARY-BENE-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Statement_Will_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Statement_Will_Ind", "#A-STATEMENT-WILL-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Statement_Trust_In_Effect_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Statement_Trust_In_Effect_Ind", "#A-STATEMENT-TRUST-IN-EFFECT-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Statement_Trust_Accept_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Statement_Trust_Accept_Ind", "#A-STATEMENT-TRUST-ACCEPT-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Taxpayer_Id_Nbr_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Taxpayer_Id_Nbr_Ind", "#A-TAXPAYER-ID-NBR-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Letters_Of_Appointment_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Letters_Of_Appointment_Ind", "#A-LETTERS-OF-APPOINTMENT-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Small_Accum_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Small_Accum_Ind", "#A-SMALL-ACCUM-IND", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Signature_Guarantee_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Signature_Guarantee_Ind", "#A-SIGNATURE-GUARANTEE-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Death_Certificate = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Death_Certificate", "#A-DEATH-CERTIFICATE", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Reason_Code = pvca5000_Pvca5000_ArrayRedef1.newFieldArrayInGroup("pvca5000_Pnd_A_Reason_Code", "#A-REASON-CODE", FieldType.STRING, 
            2, new DbsArrayController(1,6));
        pvca5000_Pnd_A_Reason_Text = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Reason_Text", "#A-REASON-TEXT", FieldType.STRING, 250);
        pvca5000_Pnd_A_Non_Spouse_Rmd_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Non_Spouse_Rmd_Ind", "#A-NON-SPOUSE-RMD-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Spouse_Rmd_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Spouse_Rmd_Ind", "#A-SPOUSE-RMD-IND", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Valuation_Contract_Nbr = pvca5000_Pvca5000_ArrayRedef1.newFieldArrayInGroup("pvca5000_Pnd_A_Valuation_Contract_Nbr", "#A-VALUATION-CONTRACT-NBR", 
            FieldType.STRING, 8, new DbsArrayController(1,25));
        pvca5000_Pnd_A_Value_Of_Contract = pvca5000_Pvca5000_ArrayRedef1.newFieldArrayInGroup("pvca5000_Pnd_A_Value_Of_Contract", "#A-VALUE-OF-CONTRACT", 
            FieldType.DECIMAL, 11,2, new DbsArrayController(1,25));
        pvca5000_Pnd_A_Total_At_Valuation_Date = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Total_At_Valuation_Date", "#A-TOTAL-AT-VALUATION-DATE", 
            FieldType.DECIMAL, 11,2);
        pvca5000_Pnd_A_Alternate_Valuation_Date = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Alternate_Valuation_Date", "#A-ALTERNATE-VALUATION-DATE", 
            FieldType.NUMERIC, 8);
        pvca5000_Pnd_A_Investment_In_Contract = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Investment_In_Contract", "#A-INVESTMENT-IN-CONTRACT", 
            FieldType.DECIMAL, 11,2);
        pvca5000_Pnd_A_Account_Name = pvca5000_Pvca5000_ArrayRedef1.newFieldArrayInGroup("pvca5000_Pnd_A_Account_Name", "#A-ACCOUNT-NAME", FieldType.STRING, 
            40, new DbsArrayController(1,10));
        pvca5000_Pnd_A_Account_Amount = pvca5000_Pvca5000_ArrayRedef1.newFieldArrayInGroup("pvca5000_Pnd_A_Account_Amount", "#A-ACCOUNT-AMOUNT", FieldType.DECIMAL, 
            11,2, new DbsArrayController(1,10));
        pvca5000_Pnd_A_Entitlement_Total_Amount = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Entitlement_Total_Amount", "#A-ENTITLEMENT-TOTAL-AMOUNT", 
            FieldType.DECIMAL, 11,2);
        pvca5000_Pnd_A_Roth_Accumulation = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Roth_Accumulation", "#A-ROTH-ACCUMULATION", FieldType.DECIMAL, 
            11,2);
        pvca5000_Pnd_A_Atra_Accumulation = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Atra_Accumulation", "#A-ATRA-ACCUMULATION", FieldType.DECIMAL, 
            11,2);
        pvca5000_Pnd_A_Invest_And_Contract_Amount = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Invest_And_Contract_Amount", "#A-INVEST-AND-CONTRACT-AMOUNT", 
            FieldType.DECIMAL, 11,2);
        pvca5000_Pnd_A_Tiaa_Nbr = pvca5000_Pvca5000_ArrayRedef1.newFieldArrayInGroup("pvca5000_Pnd_A_Tiaa_Nbr", "#A-TIAA-NBR", FieldType.STRING, 8, new 
            DbsArrayController(1,25));
        pvca5000_Pnd_A_Cref_Nbr = pvca5000_Pvca5000_ArrayRedef1.newFieldArrayInGroup("pvca5000_Pnd_A_Cref_Nbr", "#A-CREF-NBR", FieldType.STRING, 8, new 
            DbsArrayController(1,25));
        pvca5000_Pnd_A_Bi_Bene_Type = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Bi_Bene_Type", "#A-BI-BENE-TYPE", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Bi_Bene_Name = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Bi_Bene_Name", "#A-BI-BENE-NAME", FieldType.STRING, 
            60);
        pvca5000_Pnd_A_Bi_Bene_Trustee_Name = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Bi_Bene_Trustee_Name", "#A-BI-BENE-TRUSTEE-NAME", 
            FieldType.STRING, 60);
        pvca5000_Pnd_A_Bi_Street = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Bi_Street", "#A-BI-STREET", FieldType.STRING, 60);
        pvca5000_Pnd_A_Bi_City = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Bi_City", "#A-BI-CITY", FieldType.STRING, 25);
        pvca5000_Pnd_A_Bi_State = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Bi_State", "#A-BI-STATE", FieldType.STRING, 2);
        pvca5000_Pnd_A_Bi_Zip = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Bi_Zip", "#A-BI-ZIP", FieldType.STRING, 10);
        pvca5000_Pnd_A_Bi_Primary_Phone = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Bi_Primary_Phone", "#A-BI-PRIMARY-PHONE", FieldType.STRING, 
            15);
        pvca5000_Pnd_A_Bi_Alt_Phone = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Bi_Alt_Phone", "#A-BI-ALT-PHONE", FieldType.STRING, 
            15);
        pvca5000_Pnd_A_Bi_Email_Address = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Bi_Email_Address", "#A-BI-EMAIL-ADDRESS", FieldType.STRING, 
            55);
        pvca5000_Pnd_A_Bi_State_Legal_Residency = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Bi_State_Legal_Residency", "#A-BI-STATE-LEGAL-RESIDENCY", 
            FieldType.STRING, 20);
        pvca5000_Pnd_A_Bi_Citizenship = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Bi_Citizenship", "#A-BI-CITIZENSHIP", FieldType.STRING, 
            12);
        pvca5000_Pnd_A_Bi_Dob_Trust_Amend_Date = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Bi_Dob_Trust_Amend_Date", "#A-BI-DOB-TRUST-AMEND-DATE", 
            FieldType.STRING, 8);
        pvca5000_Pnd_A_Bi_Gender = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Bi_Gender", "#A-BI-GENDER", FieldType.STRING, 1);
        pvca5000_Pnd_A_Bi_Bene_Relation_Decedent = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Bi_Bene_Relation_Decedent", "#A-BI-BENE-RELATION-DECEDENT", 
            FieldType.STRING, 30);
        pvca5000_Pnd_A_Ct_Trustee_Name = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ct_Trustee_Name", "#A-CT-TRUSTEE-NAME", FieldType.STRING, 
            60);
        pvca5000_Pnd_A_Ct_Trustee_Dob = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ct_Trustee_Dob", "#A-CT-TRUSTEE-DOB", FieldType.STRING, 
            8);
        pvca5000_Pnd_A_Ct_Trustee_Relation_Decedent = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ct_Trustee_Relation_Decedent", "#A-CT-TRUSTEE-RELATION-DECEDENT", 
            FieldType.STRING, 13);
        pvca5000_Pnd_A_So_Option_Type = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_So_Option_Type", "#A-SO-OPTION-TYPE", FieldType.STRING, 
            2);
        pvca5000_Pnd_A_So_Partial_Cash_Amt = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_So_Partial_Cash_Amt", "#A-SO-PARTIAL-CASH-AMT", 
            FieldType.DECIMAL, 11,2);
        pvca5000_Pnd_A_So_Partial_Cash_Pct = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_So_Partial_Cash_Pct", "#A-SO-PARTIAL-CASH-PCT", 
            FieldType.NUMERIC, 3);
        pvca5000_Pnd_A_So_Ann_Option_Type = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_So_Ann_Option_Type", "#A-SO-ANN-OPTION-TYPE", 
            FieldType.STRING, 2);
        pvca5000_Pnd_A_So_Ann_Partial_Cash_Amt = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_So_Ann_Partial_Cash_Amt", "#A-SO-ANN-PARTIAL-CASH-AMT", 
            FieldType.DECIMAL, 11,2);
        pvca5000_Pnd_A_So_Ann_Partial_Cash_Pct = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_So_Ann_Partial_Cash_Pct", "#A-SO-ANN-PARTIAL-CASH-PCT", 
            FieldType.NUMERIC, 3);
        pvca5000_Pnd_A_Ss_Option_Type = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ss_Option_Type", "#A-SS-OPTION-TYPE", FieldType.STRING, 
            2);
        pvca5000_Pnd_A_Ss_Partial_Cash_Amt = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ss_Partial_Cash_Amt", "#A-SS-PARTIAL-CASH-AMT", 
            FieldType.DECIMAL, 11,2);
        pvca5000_Pnd_A_Ss_Partial_Cash_Pct = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ss_Partial_Cash_Pct", "#A-SS-PARTIAL-CASH-PCT", 
            FieldType.NUMERIC, 3);
        pvca5000_Pnd_A_Ss_Rollover_Acct_Status = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ss_Rollover_Acct_Status", "#A-SS-ROLLOVER-ACCT-STATUS", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Ss_Rollover_Acct_Type = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ss_Rollover_Acct_Type", "#A-SS-ROLLOVER-ACCT-TYPE", 
            FieldType.STRING, 2);
        pvca5000_Pnd_A_Ss_Rollover_Acct_Nbr = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ss_Rollover_Acct_Nbr", "#A-SS-ROLLOVER-ACCT-NBR", 
            FieldType.STRING, 8);
        pvca5000_Pnd_A_Ss_Tiaacref_Acct_Status = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ss_Tiaacref_Acct_Status", "#A-SS-TIAACREF-ACCT-STATUS", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Ss_Tiaacref_Acct_Type = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ss_Tiaacref_Acct_Type", "#A-SS-TIAACREF-ACCT-TYPE", 
            FieldType.STRING, 2);
        pvca5000_Pnd_A_Ss_Tiaacref_Acct_Nbr = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ss_Tiaacref_Acct_Nbr", "#A-SS-TIAACREF-ACCT-NBR", 
            FieldType.STRING, 8);
        pvca5000_Pnd_A_Sn_Option_Type = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Sn_Option_Type", "#A-SN-OPTION-TYPE", FieldType.STRING, 
            2);
        pvca5000_Pnd_A_Sn_Partial_Cash_Amt = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Sn_Partial_Cash_Amt", "#A-SN-PARTIAL-CASH-AMT", 
            FieldType.DECIMAL, 11,2);
        pvca5000_Pnd_A_Sn_Partial_Cash_Pct = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Sn_Partial_Cash_Pct", "#A-SN-PARTIAL-CASH-PCT", 
            FieldType.NUMERIC, 3);
        pvca5000_Pnd_A_Sn_Rollover_Acct_Status = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Sn_Rollover_Acct_Status", "#A-SN-ROLLOVER-ACCT-STATUS", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Sn_Rollover_Acct_Type = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Sn_Rollover_Acct_Type", "#A-SN-ROLLOVER-ACCT-TYPE", 
            FieldType.STRING, 2);
        pvca5000_Pnd_A_Sn_Rollover_Acct_Nbr = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Sn_Rollover_Acct_Nbr", "#A-SN-ROLLOVER-ACCT-NBR", 
            FieldType.STRING, 8);
        pvca5000_Pnd_A_Sn_Tiaacref_Acct_Status = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Sn_Tiaacref_Acct_Status", "#A-SN-TIAACREF-ACCT-STATUS", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Sn_Tiaacref_Acct_Type = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Sn_Tiaacref_Acct_Type", "#A-SN-TIAACREF-ACCT-TYPE", 
            FieldType.STRING, 2);
        pvca5000_Pnd_A_Sn_Tiaacref_Acct_Nbr = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Sn_Tiaacref_Acct_Nbr", "#A-SN-TIAACREF-ACCT-NBR", 
            FieldType.STRING, 8);
        pvca5000_Pnd_A_Sn_Md_Olest_Bene_Dob = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Sn_Md_Olest_Bene_Dob", "#A-SN-MD-OLEST-BENE-DOB", 
            FieldType.NUMERIC, 8);
        pvca5000_Pnd_A_Cf_Mmdd = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Cf_Mmdd", "#A-CF-MMDD", FieldType.NUMERIC, 4);
        pvca5000_Pnd_A_Cf_Freq = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Cf_Freq", "#A-CF-FREQ", FieldType.STRING, 1);
        pvca5000_Pnd_A_Tr_Rollover_Type = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Tr_Rollover_Type", "#A-TR-ROLLOVER-TYPE", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Tr_O1_Trad_Ira_Nbr = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Tr_O1_Trad_Ira_Nbr", "#A-TR-O1-TRAD-IRA-NBR", 
            FieldType.STRING, 8);
        pvca5000_Pnd_A_Tr_O3_Company_Name = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Tr_O3_Company_Name", "#A-TR-O3-COMPANY-NAME", 
            FieldType.STRING, 45);
        pvca5000_Pnd_A_Tr_O3_Company_Address = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Tr_O3_Company_Address", "#A-TR-O3-COMPANY-ADDRESS", 
            FieldType.STRING, 60);
        pvca5000_Pnd_A_Tr_O3_Company_City = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Tr_O3_Company_City", "#A-TR-O3-COMPANY-CITY", 
            FieldType.STRING, 25);
        pvca5000_Pnd_A_Tr_O3_Company_State = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Tr_O3_Company_State", "#A-TR-O3-COMPANY-STATE", 
            FieldType.STRING, 2);
        pvca5000_Pnd_A_Tr_O3_Company_Zip = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Tr_O3_Company_Zip", "#A-TR-O3-COMPANY-ZIP", FieldType.STRING, 
            10);
        pvca5000_Pnd_A_Tr_O3_Company_Phone = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Tr_O3_Company_Phone", "#A-TR-O3-COMPANY-PHONE", 
            FieldType.STRING, 15);
        pvca5000_Pnd_A_Tr_O3_Company_Acct_Nbr = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Tr_O3_Company_Acct_Nbr", "#A-TR-O3-COMPANY-ACCT-NBR", 
            FieldType.STRING, 40);
        pvca5000_Pnd_A_Ri_Option_Cde = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ri_Option_Cde", "#A-RI-OPTION-CDE", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Ri_Invest_Company_Name = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ri_Invest_Company_Name", "#A-RI-INVEST-COMPANY-NAME", 
            FieldType.STRING, 45);
        pvca5000_Pnd_A_Ri_Invest_Company_Address = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ri_Invest_Company_Address", "#A-RI-INVEST-COMPANY-ADDRESS", 
            FieldType.STRING, 60);
        pvca5000_Pnd_A_Ri_Invest_Company_City = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ri_Invest_Company_City", "#A-RI-INVEST-COMPANY-CITY", 
            FieldType.STRING, 25);
        pvca5000_Pnd_A_Ri_Invest_Company_State = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ri_Invest_Company_State", "#A-RI-INVEST-COMPANY-STATE", 
            FieldType.STRING, 2);
        pvca5000_Pnd_A_Ri_Invest_Company_Zip = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ri_Invest_Company_Zip", "#A-RI-INVEST-COMPANY-ZIP", 
            FieldType.STRING, 10);
        pvca5000_Pnd_A_Ri_Invest_Company_Phone = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ri_Invest_Company_Phone", "#A-RI-INVEST-COMPANY-PHONE", 
            FieldType.STRING, 15);
        pvca5000_Pnd_A_Ri_Invest_Company_Acct_Nbr = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ri_Invest_Company_Acct_Nbr", "#A-RI-INVEST-COMPANY-ACCT-NBR", 
            FieldType.STRING, 40);
        pvca5000_Pnd_A_Ri_Ann_Option_Cde = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ri_Ann_Option_Cde", "#A-RI-ANN-OPTION-CDE", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Ri_Ann_Invest_Company_Name = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ri_Ann_Invest_Company_Name", "#A-RI-ANN-INVEST-COMPANY-NAME", 
            FieldType.STRING, 45);
        pvca5000_Pnd_A_Ri_Ann_Invest_Company_Address = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ri_Ann_Invest_Company_Address", "#A-RI-ANN-INVEST-COMPANY-ADDRESS", 
            FieldType.STRING, 60);
        pvca5000_Pnd_A_Ri_Ann_Invest_Company_City = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ri_Ann_Invest_Company_City", "#A-RI-ANN-INVEST-COMPANY-CITY", 
            FieldType.STRING, 25);
        pvca5000_Pnd_A_Ri_Ann_Invest_Company_State = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ri_Ann_Invest_Company_State", "#A-RI-ANN-INVEST-COMPANY-STATE", 
            FieldType.STRING, 2);
        pvca5000_Pnd_A_Ri_Ann_Invest_Company_Zip = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ri_Ann_Invest_Company_Zip", "#A-RI-ANN-INVEST-COMPANY-ZIP", 
            FieldType.STRING, 10);
        pvca5000_Pnd_A_Ri_Ann_Invest_Company_Phone = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ri_Ann_Invest_Company_Phone", "#A-RI-ANN-INVEST-COMPANY-PHONE", 
            FieldType.STRING, 15);
        pvca5000_Pnd_A_Ri_Ann_Invt_Company_Acct_Nbr = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ri_Ann_Invt_Company_Acct_Nbr", "#A-RI-ANN-INVT-COMPANY-ACCT-NBR", 
            FieldType.STRING, 40);
        pvca5000_Pnd_A_Ro_Roth_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ro_Roth_Ind", "#A-RO-ROTH-IND", FieldType.STRING, 1);
        pvca5000_Pnd_A_Ro_Roth_Amt = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ro_Roth_Amt", "#A-RO-ROTH-AMT", FieldType.DECIMAL, 
            11,2);
        pvca5000_Pnd_A_Ro_Roth_Pct = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ro_Roth_Pct", "#A-RO-ROTH-PCT", FieldType.NUMERIC, 
            3);
        pvca5000_Pnd_A_Ft_Whold_Type = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ft_Whold_Type", "#A-FT-WHOLD-TYPE", FieldType.STRING, 
            2);
        pvca5000_Pnd_A_Ft_Da_Dollar_Amt = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ft_Da_Dollar_Amt", "#A-FT-DA-DOLLAR-AMT", FieldType.DECIMAL, 
            11,2);
        pvca5000_Pnd_A_Ft_Pc_Percent = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ft_Pc_Percent", "#A-FT-PC-PERCENT", FieldType.NUMERIC, 
            3);
        pvca5000_Pnd_A_Ft_Ann_Whold_Type = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ft_Ann_Whold_Type", "#A-FT-ANN-WHOLD-TYPE", FieldType.STRING, 
            2);
        pvca5000_Pnd_A_Ft_Ann_Da_Dollar_Amt = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ft_Ann_Da_Dollar_Amt", "#A-FT-ANN-DA-DOLLAR-AMT", 
            FieldType.DECIMAL, 11,2);
        pvca5000_Pnd_A_Ft_Ann_Pc_Percent = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ft_Ann_Pc_Percent", "#A-FT-ANN-PC-PERCENT", FieldType.NUMERIC, 
            3);
        pvca5000_Pnd_A_Ft_Re_Dollar_Amt = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ft_Re_Dollar_Amt", "#A-FT-RE-DOLLAR-AMT", FieldType.DECIMAL, 
            11,2);
        pvca5000_Pnd_A_Ft_Re_Percent = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ft_Re_Percent", "#A-FT-RE-PERCENT", FieldType.NUMERIC, 
            3);
        pvca5000_Pnd_A_Ft_N2_Dollar_Amt = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ft_N2_Dollar_Amt", "#A-FT-N2-DOLLAR-AMT", FieldType.DECIMAL, 
            11,2);
        pvca5000_Pnd_A_Ft_N2_Percent = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ft_N2_Percent", "#A-FT-N2-PERCENT", FieldType.NUMERIC, 
            3);
        pvca5000_Pnd_A_Ft_N3_Status = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ft_N3_Status", "#A-FT-N3-STATUS", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Ft_N3_Nbr_Whold_Allow = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ft_N3_Nbr_Whold_Allow", "#A-FT-N3-NBR-WHOLD-ALLOW", 
            FieldType.NUMERIC, 2);
        pvca5000_Pnd_A_Ft_Op_Dollar_Amt = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ft_Op_Dollar_Amt", "#A-FT-OP-DOLLAR-AMT", FieldType.DECIMAL, 
            11,2);
        pvca5000_Pnd_A_Ft_Cv_Dollar_Amt = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ft_Cv_Dollar_Amt", "#A-FT-CV-DOLLAR-AMT", FieldType.DECIMAL, 
            11,2);
        pvca5000_Pnd_A_Ft_Cv_Percent = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ft_Cv_Percent", "#A-FT-CV-PERCENT", FieldType.NUMERIC, 
            3);
        pvca5000_Pnd_A_Bl_Primary_Name = pvca5000_Pvca5000_ArrayRedef1.newFieldArrayInGroup("pvca5000_Pnd_A_Bl_Primary_Name", "#A-BL-PRIMARY-NAME", FieldType.STRING, 
            60, new DbsArrayController(1,4));
        pvca5000_Pnd_A_Bl_Primary_Pct = pvca5000_Pvca5000_ArrayRedef1.newFieldArrayInGroup("pvca5000_Pnd_A_Bl_Primary_Pct", "#A-BL-PRIMARY-PCT", FieldType.NUMERIC, 
            3, new DbsArrayController(1,4));
        pvca5000_Pnd_A_Bl_Primary_Dob = pvca5000_Pvca5000_ArrayRedef1.newFieldArrayInGroup("pvca5000_Pnd_A_Bl_Primary_Dob", "#A-BL-PRIMARY-DOB", FieldType.NUMERIC, 
            8, new DbsArrayController(1,4));
        pvca5000_Pnd_A_Bl_Primary_Relation = pvca5000_Pvca5000_ArrayRedef1.newFieldArrayInGroup("pvca5000_Pnd_A_Bl_Primary_Relation", "#A-BL-PRIMARY-RELATION", 
            FieldType.STRING, 13, new DbsArrayController(1,4));
        pvca5000_Pnd_A_Bl_Contingent_Name = pvca5000_Pvca5000_ArrayRedef1.newFieldArrayInGroup("pvca5000_Pnd_A_Bl_Contingent_Name", "#A-BL-CONTINGENT-NAME", 
            FieldType.STRING, 60, new DbsArrayController(1,4));
        pvca5000_Pnd_A_Bl_Contingent_Pct = pvca5000_Pvca5000_ArrayRedef1.newFieldArrayInGroup("pvca5000_Pnd_A_Bl_Contingent_Pct", "#A-BL-CONTINGENT-PCT", 
            FieldType.NUMERIC, 3, new DbsArrayController(1,4));
        pvca5000_Pnd_A_Bl_Contingent_Dob = pvca5000_Pvca5000_ArrayRedef1.newFieldArrayInGroup("pvca5000_Pnd_A_Bl_Contingent_Dob", "#A-BL-CONTINGENT-DOB", 
            FieldType.NUMERIC, 8, new DbsArrayController(1,4));
        pvca5000_Pnd_A_Bl_Contingent_Relation = pvca5000_Pvca5000_ArrayRedef1.newFieldArrayInGroup("pvca5000_Pnd_A_Bl_Contingent_Relation", "#A-BL-CONTINGENT-RELATION", 
            FieldType.STRING, 13, new DbsArrayController(1,4));
        pvca5000_Pnd_A_Di_Option_Cde = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Di_Option_Cde", "#A-DI-OPTION-CDE", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Di_O2_Acct_Type = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Di_O2_Acct_Type", "#A-DI-O2-ACCT-TYPE", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Di_O2_Amt = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Di_O2_Amt", "#A-DI-O2-AMT", FieldType.DECIMAL, 11,2);
        pvca5000_Pnd_A_Di_O2_Invest_Bank_Name = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Di_O2_Invest_Bank_Name", "#A-DI-O2-INVEST-BANK-NAME", 
            FieldType.STRING, 45);
        pvca5000_Pnd_A_Di_O2_Address = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Di_O2_Address", "#A-DI-O2-ADDRESS", FieldType.STRING, 
            60);
        pvca5000_Pnd_A_Di_O2_City = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Di_O2_City", "#A-DI-O2-CITY", FieldType.STRING, 25);
        pvca5000_Pnd_A_Di_O2_State = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Di_O2_State", "#A-DI-O2-STATE", FieldType.STRING, 2);
        pvca5000_Pnd_A_Di_O2_Zip = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Di_O2_Zip", "#A-DI-O2-ZIP", FieldType.STRING, 10);
        pvca5000_Pnd_A_Di_O2_Bank_Acct_Phone = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Di_O2_Bank_Acct_Phone", "#A-DI-O2-BANK-ACCT-PHONE", 
            FieldType.STRING, 15);
        pvca5000_Pnd_A_Di_O2_Bank_Acct_Nbr = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Di_O2_Bank_Acct_Nbr", "#A-DI-O2-BANK-ACCT-NBR", 
            FieldType.STRING, 40);
        pvca5000_Pnd_A_Di_O2_Bank_Routing_Nbr = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Di_O2_Bank_Routing_Nbr", "#A-DI-O2-BANK-ROUTING-NBR", 
            FieldType.STRING, 9);
        pvca5000_Pnd_A_Di_O3_Address_Type = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Di_O3_Address_Type", "#A-DI-O3-ADDRESS-TYPE", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Di_O3_Address = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Di_O3_Address", "#A-DI-O3-ADDRESS", FieldType.STRING, 
            60);
        pvca5000_Pnd_A_Di_O3_City = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Di_O3_City", "#A-DI-O3-CITY", FieldType.STRING, 25);
        pvca5000_Pnd_A_Di_O3_State = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Di_O3_State", "#A-DI-O3-STATE", FieldType.STRING, 2);
        pvca5000_Pnd_A_Di_O3_Zip = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Di_O3_Zip", "#A-DI-O3-ZIP", FieldType.STRING, 10);
        pvca5000_Pnd_A_Di_O4_Roth_Ira_Acct = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Di_O4_Roth_Ira_Acct", "#A-DI-O4-ROTH-IRA-ACCT", 
            FieldType.STRING, 8);
        pvca5000_Pnd_A_Am_Option_Cde = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Am_Option_Cde", "#A-AM-OPTION-CDE", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Am_O1_Payment_Years = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Am_O1_Payment_Years", "#A-AM-O1-PAYMENT-YEARS", 
            FieldType.NUMERIC, 2);
        pvca5000_Pnd_A_Am_O1_Payment_Freq = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Am_O1_Payment_Freq", "#A-AM-O1-PAYMENT-FREQ", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Am_O3_Guaranteed_Prd = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Am_O3_Guaranteed_Prd", "#A-AM-O3-GUARANTEED-PRD", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Am_O3_Payment_Freq = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Am_O3_Payment_Freq", "#A-AM-O3-PAYMENT-FREQ", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Po_Option_Cde = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Po_Option_Cde", "#A-PO-OPTION-CDE", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Po_Tiaa_Traditional_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Po_Tiaa_Traditional_Ind", "#A-PO-TIAA-TRADITIONAL-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Po_Cref_Bond_Market_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Po_Cref_Bond_Market_Ind", "#A-PO-CREF-BOND-MARKET-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Po_Tiaa_Real_Estate_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Po_Tiaa_Real_Estate_Ind", "#A-PO-TIAA-REAL-ESTATE-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Po_Cref_Glbl_Equity_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Po_Cref_Glbl_Equity_Ind", "#A-PO-CREF-GLBL-EQUITY-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Po_Cref_Stock_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Po_Cref_Stock_Ind", "#A-PO-CREF-STOCK-IND", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Po_Cref_Growth_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Po_Cref_Growth_Ind", "#A-PO-CREF-GROWTH-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Po_Cref_Money_Mrkt_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Po_Cref_Money_Mrkt_Ind", "#A-PO-CREF-MONEY-MRKT-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Po_Cref_Equity_Ndx_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Po_Cref_Equity_Ndx_Ind", "#A-PO-CREF-EQUITY-NDX-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Po_Cref_Social_Chc_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Po_Cref_Social_Chc_Ind", "#A-PO-CREF-SOCIAL-CHC-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Po_Cref_Inf_Linked_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Po_Cref_Inf_Linked_Ind", "#A-PO-CREF-INF-LINKED-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Pm_Option_Cde = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Pm_Option_Cde", "#A-PM-OPTION-CDE", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Pm_O1_This_Amt = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Pm_O1_This_Amt", "#A-PM-O1-THIS-AMT", FieldType.DECIMAL, 
            11,2);
        pvca5000_Pnd_A_Pm_O1_Graded_Prd_Amt = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Pm_O1_Graded_Prd_Amt", "#A-PM-O1-GRADED-PRD-AMT", 
            FieldType.DECIMAL, 11,2);
        pvca5000_Pnd_A_Pm_O4_Freq = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Pm_O4_Freq", "#A-PM-O4-FREQ", FieldType.STRING, 1);
        pvca5000_Pnd_A_Pm_O4_Start_Life_Income_Dte = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Pm_O4_Start_Life_Income_Dte", "#A-PM-O4-START-LIFE-INCOME-DTE", 
            FieldType.NUMERIC, 8);
        pvca5000_Pnd_A_Pm_O4_Payment_Freq = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Pm_O4_Payment_Freq", "#A-PM-O4-PAYMENT-FREQ", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Rw_Approx_Retire_Benefits = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Rw_Approx_Retire_Benefits", "#A-RW-APPROX-RETIRE-BENEFITS", 
            FieldType.DECIMAL, 11,2);
        pvca5000_Pnd_A_Rw_Asof_Date = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Rw_Asof_Date", "#A-RW-ASOF-DATE", FieldType.NUMERIC, 
            8);
        pvca5000_Pnd_A_Pi_Begin_Literal = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Pi_Begin_Literal", "#A-PI-BEGIN-LITERAL", FieldType.STRING, 
            4);
        pvca5000_Pnd_A_Pi_Export_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Pi_Export_Ind", "#A-PI-EXPORT-IND", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Pi_Task_Id = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Pi_Task_Id", "#A-PI-TASK-ID", FieldType.STRING, 11);
        pvca5000_Pnd_A_Pi_Task_Type = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Pi_Task_Type", "#A-PI-TASK-TYPE", FieldType.STRING, 
            10);
        pvca5000_Pnd_A_Pi_Task_Guid = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Pi_Task_Guid", "#A-PI-TASK-GUID", FieldType.STRING, 
            47);
        pvca5000_Pnd_A_Pi_Action_Step = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Pi_Action_Step", "#A-PI-ACTION-STEP", FieldType.STRING, 
            8);
        pvca5000_Pnd_A_Pi_Tiaa_Full_Date = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Pi_Tiaa_Full_Date", "#A-PI-TIAA-FULL-DATE", FieldType.STRING, 
            8);
        pvca5000_Pnd_A_Pi_Tiaa_Time = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Pi_Tiaa_Time", "#A-PI-TIAA-TIME", FieldType.STRING, 
            8);
        pvca5000_Pnd_A_Pi_Task_Status = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Pi_Task_Status", "#A-PI-TASK-STATUS", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Pi_Ssn = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Pi_Ssn", "#A-PI-SSN", FieldType.STRING, 9);
        pvca5000_Pnd_A_Pi_Pin_Npin_Ppg = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Pi_Pin_Npin_Ppg", "#A-PI-PIN-NPIN-PPG", FieldType.STRING, 
            7);
        pvca5000_Pnd_A_Pi_Pin_Type = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Pi_Pin_Type", "#A-PI-PIN-TYPE", FieldType.STRING, 1);
        pvca5000_Pnd_A_Pi_Contract = pvca5000_Pvca5000_ArrayRedef1.newFieldArrayInGroup("pvca5000_Pnd_A_Pi_Contract", "#A-PI-CONTRACT", FieldType.STRING, 
            10, new DbsArrayController(1,10));
        pvca5000_Pnd_A_Pi_Plan_Id = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Pi_Plan_Id", "#A-PI-PLAN-ID", FieldType.STRING, 6);
        pvca5000_Pnd_A_Pi_Doc_Content = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Pi_Doc_Content", "#A-PI-DOC-CONTENT", FieldType.STRING, 
            30);
        pvca5000_Pnd_A_Pi_Filler_1 = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Pi_Filler_1", "#A-PI-FILLER-1", FieldType.STRING, 20);
        pvca5000_Pnd_A_Pi_Filler_2 = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Pi_Filler_2", "#A-PI-FILLER-2", FieldType.STRING, 20);
        pvca5000_Pnd_A_Pi_Filler_3 = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Pi_Filler_3", "#A-PI-FILLER-3", FieldType.STRING, 20);
        pvca5000_Pnd_A_Pi_Filler_4 = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Pi_Filler_4", "#A-PI-FILLER-4", FieldType.STRING, 20);
        pvca5000_Pnd_A_Pi_Filler_5 = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Pi_Filler_5", "#A-PI-FILLER-5", FieldType.STRING, 20);
        pvca5000_Pnd_A_Pi_End_Literal = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Pi_End_Literal", "#A-PI-END-LITERAL", FieldType.STRING, 
            4);
        pvca5000_Pnd_A_Periodic_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Periodic_Ind", "#A-PERIODIC-IND", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Rollover_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Rollover_Ind", "#A-ROLLOVER-IND", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Wrong_Form_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Wrong_Form_Ind", "#A-WRONG-FORM-IND", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Blurb_Text = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Blurb_Text", "#A-BLURB-TEXT", FieldType.STRING, 250);
        pvca5000_Pnd_A_Payment_Freq_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Payment_Freq_Ind", "#A-PAYMENT-FREQ-IND", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Payment_Coupon_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Payment_Coupon_Ind", "#A-PAYMENT-COUPON-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Payment_Coupon_Tiaa_Amount = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Payment_Coupon_Tiaa_Amount", "#A-PAYMENT-COUPON-TIAA-AMOUNT", 
            FieldType.DECIMAL, 11,2);
        pvca5000_Pnd_A_Payment_Coupon_Cref_Amount = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Payment_Coupon_Cref_Amount", "#A-PAYMENT-COUPON-CREF-AMOUNT", 
            FieldType.DECIMAL, 11,2);
        pvca5000_Pnd_A_Payment_Coupon_Total_Amount = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Payment_Coupon_Total_Amount", "#A-PAYMENT-COUPON-TOTAL-AMOUNT", 
            FieldType.DECIMAL, 11,2);
        pvca5000_Pnd_A_Misc_Bene_Prov_Form_Date = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Misc_Bene_Prov_Form_Date", "#A-MISC-BENE-PROV-FORM-DATE", 
            FieldType.NUMERIC, 8);
        pvca5000_Pnd_A_Benefit_Type_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Benefit_Type_Ind", "#A-BENEFIT-TYPE-IND", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Da_5k_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Da_5k_Ind", "#A-DA-5K-IND", FieldType.STRING, 1);
        pvca5000_Pnd_A_Sip_Tiaa_Nbr = pvca5000_Pvca5000_ArrayRedef1.newFieldArrayInGroup("pvca5000_Pnd_A_Sip_Tiaa_Nbr", "#A-SIP-TIAA-NBR", FieldType.STRING, 
            8, new DbsArrayController(1,25));
        pvca5000_Pnd_A_Sip_Cref_Nbr = pvca5000_Pvca5000_ArrayRedef1.newFieldArrayInGroup("pvca5000_Pnd_A_Sip_Cref_Nbr", "#A-SIP-CREF-NBR", FieldType.STRING, 
            8, new DbsArrayController(1,25));
        pvca5000_Pnd_A_Ssum_Tiaa_Nbr = pvca5000_Pvca5000_ArrayRedef1.newFieldArrayInGroup("pvca5000_Pnd_A_Ssum_Tiaa_Nbr", "#A-SSUM-TIAA-NBR", FieldType.STRING, 
            8, new DbsArrayController(1,25));
        pvca5000_Pnd_A_Ssum_Cref_Nbr = pvca5000_Pvca5000_ArrayRedef1.newFieldArrayInGroup("pvca5000_Pnd_A_Ssum_Cref_Nbr", "#A-SSUM-CREF-NBR", FieldType.STRING, 
            8, new DbsArrayController(1,25));
        pvca5000_Pnd_A_Spouse_Rmd_Bene_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Spouse_Rmd_Bene_Ind", "#A-SPOUSE-RMD-BENE-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Entity_Rmd_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Entity_Rmd_Ind", "#A-ENTITY-RMD-IND", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Non_Spouse_Rmd_Bene_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Non_Spouse_Rmd_Bene_Ind", "#A-NON-SPOUSE-RMD-BENE-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Second_Gen_Rmd_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Second_Gen_Rmd_Ind", "#A-SECOND-GEN-RMD-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Phone_Acceptance_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Phone_Acceptance_Ind", "#A-PHONE-ACCEPTANCE-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Register_Online_Acct_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Register_Online_Acct_Ind", "#A-REGISTER-ONLINE-ACCT-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Trust_Relation_Para_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Trust_Relation_Para_Ind", "#A-TRUST-RELATION-PARA-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Brokerage_Retail_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Brokerage_Retail_Ind", "#A-BROKERAGE-RETAIL-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Brokerage_Ira_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Brokerage_Ira_Ind", "#A-BROKERAGE-IRA-IND", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Brokerage_Ira_Roth_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Brokerage_Ira_Roth_Ind", "#A-BROKERAGE-IRA-ROTH-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Sole_Spouse_Ira_Rmd_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Sole_Spouse_Ira_Rmd_Ind", "#A-SOLE-SPOUSE-IRA-RMD-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Sole_Spouse_Atra_Rmd_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Sole_Spouse_Atra_Rmd_Ind", "#A-SOLE-SPOUSE-ATRA-RMD-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Sole_Spouse_Both_Rmd_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Sole_Spouse_Both_Rmd_Ind", "#A-SOLE-SPOUSE-BOTH-RMD-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Accept_Form_Spouse_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Accept_Form_Spouse_Ind", "#A-ACCEPT-FORM-SPOUSE-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Accept_Form_Bene_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Accept_Form_Bene_Ind", "#A-ACCEPT-FORM-BENE-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Accept_Form_Entity_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Accept_Form_Entity_Ind", "#A-ACCEPT-FORM-ENTITY-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Onetime_Transfer_Spouse_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Onetime_Transfer_Spouse_Ind", "#A-ONETIME-TRANSFER-SPOUSE-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Onetime_Transfer_Bene_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Onetime_Transfer_Bene_Ind", "#A-ONETIME-TRANSFER-BENE-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Tpa_Cont_Spouse_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Tpa_Cont_Spouse_Ind", "#A-TPA-CONT-SPOUSE-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Tpa_Cont_Bene_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Tpa_Cont_Bene_Ind", "#A-TPA-CONT-BENE-IND", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Cash_Wdraw_Page_Spouse_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Cash_Wdraw_Page_Spouse_Ind", "#A-CASH-WDRAW-PAGE-SPOUSE-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Cash_Wdraw_Page_Bene_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Cash_Wdraw_Page_Bene_Ind", "#A-CASH-WDRAW-PAGE-BENE-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Rollover_Page_Spouse_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Rollover_Page_Spouse_Ind", "#A-ROLLOVER-PAGE-SPOUSE-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Rollover_Page_Bene_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Rollover_Page_Bene_Ind", "#A-ROLLOVER-PAGE-BENE-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Customer_Agree_Retail_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Customer_Agree_Retail_Ind", "#A-CUSTOMER-AGREE-RETAIL-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Affidavit_Domicile_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Affidavit_Domicile_Ind", "#A-AFFIDAVIT-DOMICILE-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Cert_Invest_Powers_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Cert_Invest_Powers_Ind", "#A-CERT-INVEST-POWERS-IND", 
            FieldType.STRING, 1);
        pvca5000_Pnd_A_Power_Of_Atty_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Power_Of_Atty_Ind", "#A-POWER-OF-ATTY-IND", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Loi_Existing_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Loi_Existing_Ind", "#A-LOI-EXISTING-IND", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Loi_Trust_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Loi_Trust_Ind", "#A-LOI-TRUST-IND", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Loi_New_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Loi_New_Ind", "#A-LOI-NEW-IND", FieldType.STRING, 1);
        pvca5000_Pnd_A_Loi_Jtwros_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Loi_Jtwros_Ind", "#A-LOI-JTWROS-IND", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Onetime_Transfer_Amt = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Onetime_Transfer_Amt", "#A-ONETIME-TRANSFER-AMT", 
            FieldType.DECIMAL, 11,2);
        pvca5000_Pnd_A_Second_Generation_Amt = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Second_Generation_Amt", "#A-SECOND-GENERATION-AMT", 
            FieldType.DECIMAL, 11,2);
        pvca5000_Pnd_A_Brokerage_Retail_Amt = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Brokerage_Retail_Amt", "#A-BROKERAGE-RETAIL-AMT", 
            FieldType.DECIMAL, 11,2);
        pvca5000_Pnd_A_Brokerage_Ira_Amt = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Brokerage_Ira_Amt", "#A-BROKERAGE-IRA-AMT", FieldType.DECIMAL, 
            11,2);
        pvca5000_Pnd_A_Brokerage_Ira_Roth_Amt = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Brokerage_Ira_Roth_Amt", "#A-BROKERAGE-IRA-ROTH-AMT", 
            FieldType.DECIMAL, 11,2);
        pvca5000_Pnd_A_Brokerage_Retail_Acct_Nbr = pvca5000_Pvca5000_ArrayRedef1.newFieldArrayInGroup("pvca5000_Pnd_A_Brokerage_Retail_Acct_Nbr", "#A-BROKERAGE-RETAIL-ACCT-NBR", 
            FieldType.STRING, 9, new DbsArrayController(1,2));
        pvca5000_Pnd_A_Brokerage_Ira_Acct_Nbr = pvca5000_Pvca5000_ArrayRedef1.newFieldArrayInGroup("pvca5000_Pnd_A_Brokerage_Ira_Acct_Nbr", "#A-BROKERAGE-IRA-ACCT-NBR", 
            FieldType.STRING, 9, new DbsArrayController(1,2));
        pvca5000_Pnd_A_Brokerage_Roth_Acct_Nbr = pvca5000_Pvca5000_ArrayRedef1.newFieldArrayInGroup("pvca5000_Pnd_A_Brokerage_Roth_Acct_Nbr", "#A-BROKERAGE-ROTH-ACCT-NBR", 
            FieldType.STRING, 9, new DbsArrayController(1,2));
        pvca5000_Pnd_A_Ro_Account_Nbr = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Ro_Account_Nbr", "#A-RO-ACCOUNT-NBR", FieldType.STRING, 
            8);
        pvca5000_Pnd_A_Di_O4_Tiaa_Acct_Type = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Di_O4_Tiaa_Acct_Type", "#A-DI-O4-TIAA-ACCT-TYPE", 
            FieldType.STRING, 2);
        pvca5000_Pnd_A_Di_O4_Careof = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Di_O4_Careof", "#A-DI-O4-CAREOF", FieldType.STRING, 
            60);
        pvca5000_Pnd_A_Loi_Ira_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Loi_Ira_Ind", "#A-LOI-IRA-IND", FieldType.STRING, 1);
        pvca5000_Pnd_A_Loi_Estate_Ind = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Loi_Estate_Ind", "#A-LOI-ESTATE-IND", FieldType.STRING, 
            1);
        pvca5000_Pnd_A_Tpa_Page = pvca5000_Pvca5000_ArrayRedef1.newFieldInGroup("pvca5000_Pnd_A_Tpa_Page", "#A-TPA-PAGE", FieldType.NUMERIC, 1);

        dbsRecord.reset();
    }

    // Constructors
    public PdaPvca5000(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

