/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:18:12 PM
**        * FROM NATURAL PDA     : BENA9866
************************************************************
**        * FILE NAME            : PdaBena9866.java
**        * CLASS NAME           : PdaBena9866
**        * INSTANCE NAME        : PdaBena9866
************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaBena9866 extends PdaBase
{
    // Properties
    private DbsGroup bena9866;
    private DbsField bena9866_Pnd_Error_Cde;
    private DbsField bena9866_Pnd_Error_Msg;
    private DbsField bena9866_Pnd_Error_Pgm;
    private DbsField bena9866_Pnd_Edit_Failed;

    public DbsGroup getBena9866() { return bena9866; }

    public DbsField getBena9866_Pnd_Error_Cde() { return bena9866_Pnd_Error_Cde; }

    public DbsField getBena9866_Pnd_Error_Msg() { return bena9866_Pnd_Error_Msg; }

    public DbsField getBena9866_Pnd_Error_Pgm() { return bena9866_Pnd_Error_Pgm; }

    public DbsField getBena9866_Pnd_Edit_Failed() { return bena9866_Pnd_Edit_Failed; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        bena9866 = dbsRecord.newGroupInRecord("bena9866", "BENA9866");
        bena9866.setParameterOption(ParameterOption.ByReference);
        bena9866_Pnd_Error_Cde = bena9866.newFieldInGroup("bena9866_Pnd_Error_Cde", "#ERROR-CDE", FieldType.STRING, 5);
        bena9866_Pnd_Error_Msg = bena9866.newFieldInGroup("bena9866_Pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 72);
        bena9866_Pnd_Error_Pgm = bena9866.newFieldInGroup("bena9866_Pnd_Error_Pgm", "#ERROR-PGM", FieldType.STRING, 8);
        bena9866_Pnd_Edit_Failed = bena9866.newFieldInGroup("bena9866_Pnd_Edit_Failed", "#EDIT-FAILED", FieldType.BOOLEAN);

        dbsRecord.reset();
    }

    // Constructors
    public PdaBena9866(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

