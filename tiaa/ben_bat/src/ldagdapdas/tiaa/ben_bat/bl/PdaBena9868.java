/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:18:13 PM
**        * FROM NATURAL PDA     : BENA9868
************************************************************
**        * FILE NAME            : PdaBena9868.java
**        * CLASS NAME           : PdaBena9868
**        * INSTANCE NAME        : PdaBena9868
************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaBena9868 extends PdaBase
{
    // Properties
    private DbsGroup bena9868;
    private DbsField bena9868_Pnd_Pin_Tiaa_Cntrct;
    private DbsGroup bena9868_Pnd_Pin_Tiaa_CntrctRedef1;
    private DbsField bena9868_Pnd_Pin;
    private DbsField bena9868_Pnd_Tiaa_Cntrct;
    private DbsField bena9868_Pnd_Tiaa_Cref_Ind;
    private DbsField bena9868_Pnd_Fldr_Log_Dte_Tme;
    private DbsField bena9868_Pnd_Bc_Rqst_Timestamp;
    private DbsField bena9868_Pnd_Bc_Exempt_Spouse_Rights;

    public DbsGroup getBena9868() { return bena9868; }

    public DbsField getBena9868_Pnd_Pin_Tiaa_Cntrct() { return bena9868_Pnd_Pin_Tiaa_Cntrct; }

    public DbsGroup getBena9868_Pnd_Pin_Tiaa_CntrctRedef1() { return bena9868_Pnd_Pin_Tiaa_CntrctRedef1; }

    public DbsField getBena9868_Pnd_Pin() { return bena9868_Pnd_Pin; }

    public DbsField getBena9868_Pnd_Tiaa_Cntrct() { return bena9868_Pnd_Tiaa_Cntrct; }

    public DbsField getBena9868_Pnd_Tiaa_Cref_Ind() { return bena9868_Pnd_Tiaa_Cref_Ind; }

    public DbsField getBena9868_Pnd_Fldr_Log_Dte_Tme() { return bena9868_Pnd_Fldr_Log_Dte_Tme; }

    public DbsField getBena9868_Pnd_Bc_Rqst_Timestamp() { return bena9868_Pnd_Bc_Rqst_Timestamp; }

    public DbsField getBena9868_Pnd_Bc_Exempt_Spouse_Rights() { return bena9868_Pnd_Bc_Exempt_Spouse_Rights; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        bena9868 = dbsRecord.newGroupInRecord("bena9868", "BENA9868");
        bena9868.setParameterOption(ParameterOption.ByReference);
        bena9868_Pnd_Pin_Tiaa_Cntrct = bena9868.newFieldInGroup("bena9868_Pnd_Pin_Tiaa_Cntrct", "#PIN-TIAA-CNTRCT", FieldType.STRING, 22);
        bena9868_Pnd_Pin_Tiaa_CntrctRedef1 = bena9868.newGroupInGroup("bena9868_Pnd_Pin_Tiaa_CntrctRedef1", "Redefines", bena9868_Pnd_Pin_Tiaa_Cntrct);
        bena9868_Pnd_Pin = bena9868_Pnd_Pin_Tiaa_CntrctRedef1.newFieldInGroup("bena9868_Pnd_Pin", "#PIN", FieldType.NUMERIC, 12);
        bena9868_Pnd_Tiaa_Cntrct = bena9868_Pnd_Pin_Tiaa_CntrctRedef1.newFieldInGroup("bena9868_Pnd_Tiaa_Cntrct", "#TIAA-CNTRCT", FieldType.STRING, 10);
        bena9868_Pnd_Tiaa_Cref_Ind = bena9868.newFieldInGroup("bena9868_Pnd_Tiaa_Cref_Ind", "#TIAA-CREF-IND", FieldType.STRING, 1);
        bena9868_Pnd_Fldr_Log_Dte_Tme = bena9868.newFieldInGroup("bena9868_Pnd_Fldr_Log_Dte_Tme", "#FLDR-LOG-DTE-TME", FieldType.STRING, 15);
        bena9868_Pnd_Bc_Rqst_Timestamp = bena9868.newFieldInGroup("bena9868_Pnd_Bc_Rqst_Timestamp", "#BC-RQST-TIMESTAMP", FieldType.STRING, 15);
        bena9868_Pnd_Bc_Exempt_Spouse_Rights = bena9868.newFieldInGroup("bena9868_Pnd_Bc_Exempt_Spouse_Rights", "#BC-EXEMPT-SPOUSE-RIGHTS", FieldType.STRING, 
            1);

        dbsRecord.reset();
    }

    // Constructors
    public PdaBena9868(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

