/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:18:13 PM
**        * FROM NATURAL PDA     : BENA9869
************************************************************
**        * FILE NAME            : PdaBena9869.java
**        * CLASS NAME           : PdaBena9869
**        * INSTANCE NAME        : PdaBena9869
************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaBena9869 extends PdaBase
{
    // Properties
    private DbsGroup bena9869;
    private DbsField bena9869_Pnd_Pin_Tiaa_Cntrct;
    private DbsGroup bena9869_Pnd_Pin_Tiaa_CntrctRedef1;
    private DbsField bena9869_Pnd_Pin;
    private DbsField bena9869_Pnd_Tiaa_Cntrct;
    private DbsField bena9869_Pnd_Tiaa_Cref_Ind;
    private DbsField bena9869_Pnd_Old_Stts;
    private DbsField bena9869_Pnd_New_Stts;

    public DbsGroup getBena9869() { return bena9869; }

    public DbsField getBena9869_Pnd_Pin_Tiaa_Cntrct() { return bena9869_Pnd_Pin_Tiaa_Cntrct; }

    public DbsGroup getBena9869_Pnd_Pin_Tiaa_CntrctRedef1() { return bena9869_Pnd_Pin_Tiaa_CntrctRedef1; }

    public DbsField getBena9869_Pnd_Pin() { return bena9869_Pnd_Pin; }

    public DbsField getBena9869_Pnd_Tiaa_Cntrct() { return bena9869_Pnd_Tiaa_Cntrct; }

    public DbsField getBena9869_Pnd_Tiaa_Cref_Ind() { return bena9869_Pnd_Tiaa_Cref_Ind; }

    public DbsField getBena9869_Pnd_Old_Stts() { return bena9869_Pnd_Old_Stts; }

    public DbsField getBena9869_Pnd_New_Stts() { return bena9869_Pnd_New_Stts; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        bena9869 = dbsRecord.newGroupInRecord("bena9869", "BENA9869");
        bena9869.setParameterOption(ParameterOption.ByReference);
        bena9869_Pnd_Pin_Tiaa_Cntrct = bena9869.newFieldInGroup("bena9869_Pnd_Pin_Tiaa_Cntrct", "#PIN-TIAA-CNTRCT", FieldType.STRING, 22);
        bena9869_Pnd_Pin_Tiaa_CntrctRedef1 = bena9869.newGroupInGroup("bena9869_Pnd_Pin_Tiaa_CntrctRedef1", "Redefines", bena9869_Pnd_Pin_Tiaa_Cntrct);
        bena9869_Pnd_Pin = bena9869_Pnd_Pin_Tiaa_CntrctRedef1.newFieldInGroup("bena9869_Pnd_Pin", "#PIN", FieldType.NUMERIC, 12);
        bena9869_Pnd_Tiaa_Cntrct = bena9869_Pnd_Pin_Tiaa_CntrctRedef1.newFieldInGroup("bena9869_Pnd_Tiaa_Cntrct", "#TIAA-CNTRCT", FieldType.STRING, 10);
        bena9869_Pnd_Tiaa_Cref_Ind = bena9869.newFieldInGroup("bena9869_Pnd_Tiaa_Cref_Ind", "#TIAA-CREF-IND", FieldType.STRING, 1);
        bena9869_Pnd_Old_Stts = bena9869.newFieldInGroup("bena9869_Pnd_Old_Stts", "#OLD-STTS", FieldType.STRING, 1);
        bena9869_Pnd_New_Stts = bena9869.newFieldInGroup("bena9869_Pnd_New_Stts", "#NEW-STTS", FieldType.STRING, 1);

        dbsRecord.reset();
    }

    // Constructors
    public PdaBena9869(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

