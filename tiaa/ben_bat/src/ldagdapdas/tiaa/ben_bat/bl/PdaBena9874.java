/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:18:14 PM
**        * FROM NATURAL PDA     : BENA9874
************************************************************
**        * FILE NAME            : PdaBena9874.java
**        * CLASS NAME           : PdaBena9874
**        * INSTANCE NAME        : PdaBena9874
************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaBena9874 extends PdaBase
{
    // Properties
    private DbsGroup bena9874;
    private DbsField bena9874_Pnd_Pin_Tiaa_Cntrct;
    private DbsGroup bena9874_Pnd_Pin_Tiaa_CntrctRedef1;
    private DbsField bena9874_Pnd_Pin;
    private DbsField bena9874_Pnd_Tiaa_Cntrct;
    private DbsField bena9874_Pnd_Tiaa_Cref_Ind;
    private DbsField bena9874_Pnd_Stts;
    private DbsField bena9874_Pnd_Found;
    private DbsField bena9874_Pnd_Last_Srce;

    public DbsGroup getBena9874() { return bena9874; }

    public DbsField getBena9874_Pnd_Pin_Tiaa_Cntrct() { return bena9874_Pnd_Pin_Tiaa_Cntrct; }

    public DbsGroup getBena9874_Pnd_Pin_Tiaa_CntrctRedef1() { return bena9874_Pnd_Pin_Tiaa_CntrctRedef1; }

    public DbsField getBena9874_Pnd_Pin() { return bena9874_Pnd_Pin; }

    public DbsField getBena9874_Pnd_Tiaa_Cntrct() { return bena9874_Pnd_Tiaa_Cntrct; }

    public DbsField getBena9874_Pnd_Tiaa_Cref_Ind() { return bena9874_Pnd_Tiaa_Cref_Ind; }

    public DbsField getBena9874_Pnd_Stts() { return bena9874_Pnd_Stts; }

    public DbsField getBena9874_Pnd_Found() { return bena9874_Pnd_Found; }

    public DbsField getBena9874_Pnd_Last_Srce() { return bena9874_Pnd_Last_Srce; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        bena9874 = dbsRecord.newGroupInRecord("bena9874", "BENA9874");
        bena9874.setParameterOption(ParameterOption.ByReference);
        bena9874_Pnd_Pin_Tiaa_Cntrct = bena9874.newFieldInGroup("bena9874_Pnd_Pin_Tiaa_Cntrct", "#PIN-TIAA-CNTRCT", FieldType.STRING, 22);
        bena9874_Pnd_Pin_Tiaa_CntrctRedef1 = bena9874.newGroupInGroup("bena9874_Pnd_Pin_Tiaa_CntrctRedef1", "Redefines", bena9874_Pnd_Pin_Tiaa_Cntrct);
        bena9874_Pnd_Pin = bena9874_Pnd_Pin_Tiaa_CntrctRedef1.newFieldInGroup("bena9874_Pnd_Pin", "#PIN", FieldType.NUMERIC, 12);
        bena9874_Pnd_Tiaa_Cntrct = bena9874_Pnd_Pin_Tiaa_CntrctRedef1.newFieldInGroup("bena9874_Pnd_Tiaa_Cntrct", "#TIAA-CNTRCT", FieldType.STRING, 10);
        bena9874_Pnd_Tiaa_Cref_Ind = bena9874.newFieldInGroup("bena9874_Pnd_Tiaa_Cref_Ind", "#TIAA-CREF-IND", FieldType.STRING, 1);
        bena9874_Pnd_Stts = bena9874.newFieldInGroup("bena9874_Pnd_Stts", "#STTS", FieldType.STRING, 1);
        bena9874_Pnd_Found = bena9874.newFieldInGroup("bena9874_Pnd_Found", "#FOUND", FieldType.BOOLEAN);
        bena9874_Pnd_Last_Srce = bena9874.newFieldInGroup("bena9874_Pnd_Last_Srce", "#LAST-SRCE", FieldType.STRING, 1);

        dbsRecord.reset();
    }

    // Constructors
    public PdaBena9874(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

