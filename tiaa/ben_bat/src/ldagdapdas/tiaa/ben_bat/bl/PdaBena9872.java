/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:18:13 PM
**        * FROM NATURAL PDA     : BENA9872
************************************************************
**        * FILE NAME            : PdaBena9872.java
**        * CLASS NAME           : PdaBena9872
**        * INSTANCE NAME        : PdaBena9872
************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaBena9872 extends PdaBase
{
    // Properties
    private DbsGroup bena9872;
    private DbsField bena9872_Pnd_Cntrct_Isn;
    private DbsField bena9872_Pnd_Pin_Tiaa_Cntrct;
    private DbsGroup bena9872_Pnd_Pin_Tiaa_CntrctRedef1;
    private DbsField bena9872_Pnd_Pin;
    private DbsField bena9872_Pnd_Tiaa_Cntrct;
    private DbsField bena9872_Pnd_Tiaa_Cref_Ind;
    private DbsField bena9872_Pnd_Rcrd_Crtd_For_Bsnss_Dte;
    private DbsField bena9872_Pnd_Intrfce_Stts;
    private DbsField bena9872_Pnd_Error_Txt;
    private DbsField bena9872_Pnd_Error_Cde;
    private DbsField bena9872_Pnd_Error_Pgm;
    private DbsField bena9872_Pnd_Bsnss_Dte;

    public DbsGroup getBena9872() { return bena9872; }

    public DbsField getBena9872_Pnd_Cntrct_Isn() { return bena9872_Pnd_Cntrct_Isn; }

    public DbsField getBena9872_Pnd_Pin_Tiaa_Cntrct() { return bena9872_Pnd_Pin_Tiaa_Cntrct; }

    public DbsGroup getBena9872_Pnd_Pin_Tiaa_CntrctRedef1() { return bena9872_Pnd_Pin_Tiaa_CntrctRedef1; }

    public DbsField getBena9872_Pnd_Pin() { return bena9872_Pnd_Pin; }

    public DbsField getBena9872_Pnd_Tiaa_Cntrct() { return bena9872_Pnd_Tiaa_Cntrct; }

    public DbsField getBena9872_Pnd_Tiaa_Cref_Ind() { return bena9872_Pnd_Tiaa_Cref_Ind; }

    public DbsField getBena9872_Pnd_Rcrd_Crtd_For_Bsnss_Dte() { return bena9872_Pnd_Rcrd_Crtd_For_Bsnss_Dte; }

    public DbsField getBena9872_Pnd_Intrfce_Stts() { return bena9872_Pnd_Intrfce_Stts; }

    public DbsField getBena9872_Pnd_Error_Txt() { return bena9872_Pnd_Error_Txt; }

    public DbsField getBena9872_Pnd_Error_Cde() { return bena9872_Pnd_Error_Cde; }

    public DbsField getBena9872_Pnd_Error_Pgm() { return bena9872_Pnd_Error_Pgm; }

    public DbsField getBena9872_Pnd_Bsnss_Dte() { return bena9872_Pnd_Bsnss_Dte; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        bena9872 = dbsRecord.newGroupInRecord("bena9872", "BENA9872");
        bena9872.setParameterOption(ParameterOption.ByReference);
        bena9872_Pnd_Cntrct_Isn = bena9872.newFieldInGroup("bena9872_Pnd_Cntrct_Isn", "#CNTRCT-ISN", FieldType.PACKED_DECIMAL, 10);
        bena9872_Pnd_Pin_Tiaa_Cntrct = bena9872.newFieldInGroup("bena9872_Pnd_Pin_Tiaa_Cntrct", "#PIN-TIAA-CNTRCT", FieldType.STRING, 22);
        bena9872_Pnd_Pin_Tiaa_CntrctRedef1 = bena9872.newGroupInGroup("bena9872_Pnd_Pin_Tiaa_CntrctRedef1", "Redefines", bena9872_Pnd_Pin_Tiaa_Cntrct);
        bena9872_Pnd_Pin = bena9872_Pnd_Pin_Tiaa_CntrctRedef1.newFieldInGroup("bena9872_Pnd_Pin", "#PIN", FieldType.NUMERIC, 12);
        bena9872_Pnd_Tiaa_Cntrct = bena9872_Pnd_Pin_Tiaa_CntrctRedef1.newFieldInGroup("bena9872_Pnd_Tiaa_Cntrct", "#TIAA-CNTRCT", FieldType.STRING, 10);
        bena9872_Pnd_Tiaa_Cref_Ind = bena9872.newFieldInGroup("bena9872_Pnd_Tiaa_Cref_Ind", "#TIAA-CREF-IND", FieldType.STRING, 1);
        bena9872_Pnd_Rcrd_Crtd_For_Bsnss_Dte = bena9872.newFieldInGroup("bena9872_Pnd_Rcrd_Crtd_For_Bsnss_Dte", "#RCRD-CRTD-FOR-BSNSS-DTE", FieldType.STRING, 
            8);
        bena9872_Pnd_Intrfce_Stts = bena9872.newFieldInGroup("bena9872_Pnd_Intrfce_Stts", "#INTRFCE-STTS", FieldType.STRING, 1);
        bena9872_Pnd_Error_Txt = bena9872.newFieldInGroup("bena9872_Pnd_Error_Txt", "#ERROR-TXT", FieldType.STRING, 72);
        bena9872_Pnd_Error_Cde = bena9872.newFieldInGroup("bena9872_Pnd_Error_Cde", "#ERROR-CDE", FieldType.STRING, 5);
        bena9872_Pnd_Error_Pgm = bena9872.newFieldInGroup("bena9872_Pnd_Error_Pgm", "#ERROR-PGM", FieldType.STRING, 8);
        bena9872_Pnd_Bsnss_Dte = bena9872.newFieldInGroup("bena9872_Pnd_Bsnss_Dte", "#BSNSS-DTE", FieldType.STRING, 8);

        dbsRecord.reset();
    }

    // Constructors
    public PdaBena9872(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

