/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:18:14 PM
**        * FROM NATURAL PDA     : BENA9875
************************************************************
**        * FILE NAME            : PdaBena9875.java
**        * CLASS NAME           : PdaBena9875
**        * INSTANCE NAME        : PdaBena9875
************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaBena9875 extends PdaBase
{
    // Properties
    private DbsGroup bena9875;
    private DbsField bena9875_Pnd_Pin;
    private DbsField bena9875_Pnd_Tiaa_Cntrct;
    private DbsField bena9875_Pnd_Tiaa_Cref_Ind;
    private DbsField bena9875_Pnd_Rcrd_Crtd_Dte;
    private DbsField bena9875_Pnd_Rcrd_Crtd_Tme;
    private DbsField bena9875_Pnd_Bsnss_Dte;
    private DbsField bena9875_Pnd_Exists;

    public DbsGroup getBena9875() { return bena9875; }

    public DbsField getBena9875_Pnd_Pin() { return bena9875_Pnd_Pin; }

    public DbsField getBena9875_Pnd_Tiaa_Cntrct() { return bena9875_Pnd_Tiaa_Cntrct; }

    public DbsField getBena9875_Pnd_Tiaa_Cref_Ind() { return bena9875_Pnd_Tiaa_Cref_Ind; }

    public DbsField getBena9875_Pnd_Rcrd_Crtd_Dte() { return bena9875_Pnd_Rcrd_Crtd_Dte; }

    public DbsField getBena9875_Pnd_Rcrd_Crtd_Tme() { return bena9875_Pnd_Rcrd_Crtd_Tme; }

    public DbsField getBena9875_Pnd_Bsnss_Dte() { return bena9875_Pnd_Bsnss_Dte; }

    public DbsField getBena9875_Pnd_Exists() { return bena9875_Pnd_Exists; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        bena9875 = dbsRecord.newGroupInRecord("bena9875", "BENA9875");
        bena9875.setParameterOption(ParameterOption.ByReference);
        bena9875_Pnd_Pin = bena9875.newFieldInGroup("bena9875_Pnd_Pin", "#PIN", FieldType.NUMERIC, 12);
        bena9875_Pnd_Tiaa_Cntrct = bena9875.newFieldInGroup("bena9875_Pnd_Tiaa_Cntrct", "#TIAA-CNTRCT", FieldType.STRING, 10);
        bena9875_Pnd_Tiaa_Cref_Ind = bena9875.newFieldInGroup("bena9875_Pnd_Tiaa_Cref_Ind", "#TIAA-CREF-IND", FieldType.STRING, 1);
        bena9875_Pnd_Rcrd_Crtd_Dte = bena9875.newFieldInGroup("bena9875_Pnd_Rcrd_Crtd_Dte", "#RCRD-CRTD-DTE", FieldType.STRING, 8);
        bena9875_Pnd_Rcrd_Crtd_Tme = bena9875.newFieldInGroup("bena9875_Pnd_Rcrd_Crtd_Tme", "#RCRD-CRTD-TME", FieldType.STRING, 7);
        bena9875_Pnd_Bsnss_Dte = bena9875.newFieldInGroup("bena9875_Pnd_Bsnss_Dte", "#BSNSS-DTE", FieldType.STRING, 8);
        bena9875_Pnd_Exists = bena9875.newFieldInGroup("bena9875_Pnd_Exists", "#EXISTS", FieldType.BOOLEAN);

        dbsRecord.reset();
    }

    // Constructors
    public PdaBena9875(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

