/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:18:09 PM
**        * FROM NATURAL PDA     : BENA971
************************************************************
**        * FILE NAME            : PdaBena971.java
**        * CLASS NAME           : PdaBena971
**        * INSTANCE NAME        : PdaBena971
************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaBena971 extends PdaBase
{
    // Properties
    private DbsGroup bena971;
    private DbsField bena971_Pnd_Intrfce_Bsnss_Dte;
    private DbsField bena971_Pnd_Intrfcng_Systm;
    private DbsField bena971_Pnd_Rqstng_System;
    private DbsField bena971_Pnd_New_Issuefslash_Chng_Ind;
    private DbsField bena971_Pnd_Illgble_Ind;
    private DbsField bena971_Pnd_More_Than_Five_Benes_Ind;
    private DbsField bena971_Pnd_Nmbr_Of_Benes;
    private DbsField bena971_Pnd_Intrfce_Mgrtn_Ind;
    private DbsField bena971_Pnd_Pin;
    private DbsField bena971_Pnd_Tiaa_Cntrct;
    private DbsField bena971_Pnd_Cref_Cntrct;
    private DbsField bena971_Pnd_Cntrct_Type;
    private DbsField bena971_Pnd_Effctve_Dte;
    private DbsField bena971_Pnd_Tiaa_Cref_Ind;
    private DbsField bena971_Pnd_Same_As_Ind;
    private DbsField bena971_Pnd_Mos_Ind;
    private DbsField bena971_Pnd_Mos_Irrvcble_Ind;
    private DbsField bena971_Pnd_Pymnt_Chld_Dcsd_Ind;
    private DbsField bena971_Pnd_Exempt_Spouse_Rights;
    private DbsField bena971_Pnd_Spouse_Waived_Bnfts;
    private DbsField bena971_Pnd_Fldr_Log_Dte_Tme;
    private DbsField bena971_Pnd_Dflt_To_Estate;
    private DbsField bena971_Pnd_Mos_Txt;
    private DbsGroup bena971_Pnd_Bene_Data;
    private DbsField bena971_Pnd_Bene_Name1;
    private DbsField bena971_Pnd_Bene_Name2;
    private DbsField bena971_Pnd_Bene_Type;
    private DbsField bena971_Pnd_Dte_Birth_Trust;
    private DbsField bena971_Pnd_Rtln_Cde;
    private DbsField bena971_Pnd_Rltn_Free_Txt;
    private DbsField bena971_Pnd_Ss_Cd;
    private DbsField bena971_Pnd_Ss_Nbr;
    private DbsField bena971_Pnd_Prctge_Frctn_Ind;
    private DbsField bena971_Pnd_Addr1;
    private DbsField bena971_Pnd_Addr2;
    private DbsField bena971_Pnd_Addr3_City;
    private DbsField bena971_Pnd_State;
    private DbsField bena971_Pnd_Zip;
    private DbsField bena971_Pnd_Country;
    private DbsField bena971_Pnd_Phone;
    private DbsField bena971_Pnd_Gender;
    private DbsField bena971_Pnd_Prctge;
    private DbsField bena971_Pnd_Nmrtr;
    private DbsField bena971_Pnd_Dnmntr;
    private DbsField bena971_Pnd_Irrvcbl_Ind;
    private DbsField bena971_Pnd_Sttlmnt_Rstrctn;
    private DbsField bena971_Pnd_Spcl_Txt;
    private DbsField bena971_Pnd_Mdo_Calc_Bene;
    private DbsField bena971_Pnd_Return_Code;
    private DbsField bena971_Pnd_Error_Msg;

    public DbsGroup getBena971() { return bena971; }

    public DbsField getBena971_Pnd_Intrfce_Bsnss_Dte() { return bena971_Pnd_Intrfce_Bsnss_Dte; }

    public DbsField getBena971_Pnd_Intrfcng_Systm() { return bena971_Pnd_Intrfcng_Systm; }

    public DbsField getBena971_Pnd_Rqstng_System() { return bena971_Pnd_Rqstng_System; }

    public DbsField getBena971_Pnd_New_Issuefslash_Chng_Ind() { return bena971_Pnd_New_Issuefslash_Chng_Ind; }

    public DbsField getBena971_Pnd_Illgble_Ind() { return bena971_Pnd_Illgble_Ind; }

    public DbsField getBena971_Pnd_More_Than_Five_Benes_Ind() { return bena971_Pnd_More_Than_Five_Benes_Ind; }

    public DbsField getBena971_Pnd_Nmbr_Of_Benes() { return bena971_Pnd_Nmbr_Of_Benes; }

    public DbsField getBena971_Pnd_Intrfce_Mgrtn_Ind() { return bena971_Pnd_Intrfce_Mgrtn_Ind; }

    public DbsField getBena971_Pnd_Pin() { return bena971_Pnd_Pin; }

    public DbsField getBena971_Pnd_Tiaa_Cntrct() { return bena971_Pnd_Tiaa_Cntrct; }

    public DbsField getBena971_Pnd_Cref_Cntrct() { return bena971_Pnd_Cref_Cntrct; }

    public DbsField getBena971_Pnd_Cntrct_Type() { return bena971_Pnd_Cntrct_Type; }

    public DbsField getBena971_Pnd_Effctve_Dte() { return bena971_Pnd_Effctve_Dte; }

    public DbsField getBena971_Pnd_Tiaa_Cref_Ind() { return bena971_Pnd_Tiaa_Cref_Ind; }

    public DbsField getBena971_Pnd_Same_As_Ind() { return bena971_Pnd_Same_As_Ind; }

    public DbsField getBena971_Pnd_Mos_Ind() { return bena971_Pnd_Mos_Ind; }

    public DbsField getBena971_Pnd_Mos_Irrvcble_Ind() { return bena971_Pnd_Mos_Irrvcble_Ind; }

    public DbsField getBena971_Pnd_Pymnt_Chld_Dcsd_Ind() { return bena971_Pnd_Pymnt_Chld_Dcsd_Ind; }

    public DbsField getBena971_Pnd_Exempt_Spouse_Rights() { return bena971_Pnd_Exempt_Spouse_Rights; }

    public DbsField getBena971_Pnd_Spouse_Waived_Bnfts() { return bena971_Pnd_Spouse_Waived_Bnfts; }

    public DbsField getBena971_Pnd_Fldr_Log_Dte_Tme() { return bena971_Pnd_Fldr_Log_Dte_Tme; }

    public DbsField getBena971_Pnd_Dflt_To_Estate() { return bena971_Pnd_Dflt_To_Estate; }

    public DbsField getBena971_Pnd_Mos_Txt() { return bena971_Pnd_Mos_Txt; }

    public DbsGroup getBena971_Pnd_Bene_Data() { return bena971_Pnd_Bene_Data; }

    public DbsField getBena971_Pnd_Bene_Name1() { return bena971_Pnd_Bene_Name1; }

    public DbsField getBena971_Pnd_Bene_Name2() { return bena971_Pnd_Bene_Name2; }

    public DbsField getBena971_Pnd_Bene_Type() { return bena971_Pnd_Bene_Type; }

    public DbsField getBena971_Pnd_Dte_Birth_Trust() { return bena971_Pnd_Dte_Birth_Trust; }

    public DbsField getBena971_Pnd_Rtln_Cde() { return bena971_Pnd_Rtln_Cde; }

    public DbsField getBena971_Pnd_Rltn_Free_Txt() { return bena971_Pnd_Rltn_Free_Txt; }

    public DbsField getBena971_Pnd_Ss_Cd() { return bena971_Pnd_Ss_Cd; }

    public DbsField getBena971_Pnd_Ss_Nbr() { return bena971_Pnd_Ss_Nbr; }

    public DbsField getBena971_Pnd_Prctge_Frctn_Ind() { return bena971_Pnd_Prctge_Frctn_Ind; }

    public DbsField getBena971_Pnd_Addr1() { return bena971_Pnd_Addr1; }

    public DbsField getBena971_Pnd_Addr2() { return bena971_Pnd_Addr2; }

    public DbsField getBena971_Pnd_Addr3_City() { return bena971_Pnd_Addr3_City; }

    public DbsField getBena971_Pnd_State() { return bena971_Pnd_State; }

    public DbsField getBena971_Pnd_Zip() { return bena971_Pnd_Zip; }

    public DbsField getBena971_Pnd_Country() { return bena971_Pnd_Country; }

    public DbsField getBena971_Pnd_Phone() { return bena971_Pnd_Phone; }

    public DbsField getBena971_Pnd_Gender() { return bena971_Pnd_Gender; }

    public DbsField getBena971_Pnd_Prctge() { return bena971_Pnd_Prctge; }

    public DbsField getBena971_Pnd_Nmrtr() { return bena971_Pnd_Nmrtr; }

    public DbsField getBena971_Pnd_Dnmntr() { return bena971_Pnd_Dnmntr; }

    public DbsField getBena971_Pnd_Irrvcbl_Ind() { return bena971_Pnd_Irrvcbl_Ind; }

    public DbsField getBena971_Pnd_Sttlmnt_Rstrctn() { return bena971_Pnd_Sttlmnt_Rstrctn; }

    public DbsField getBena971_Pnd_Spcl_Txt() { return bena971_Pnd_Spcl_Txt; }

    public DbsField getBena971_Pnd_Mdo_Calc_Bene() { return bena971_Pnd_Mdo_Calc_Bene; }

    public DbsField getBena971_Pnd_Return_Code() { return bena971_Pnd_Return_Code; }

    public DbsField getBena971_Pnd_Error_Msg() { return bena971_Pnd_Error_Msg; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        bena971 = dbsRecord.newGroupInRecord("bena971", "BENA971");
        bena971.setParameterOption(ParameterOption.ByReference);
        bena971_Pnd_Intrfce_Bsnss_Dte = bena971.newFieldInGroup("bena971_Pnd_Intrfce_Bsnss_Dte", "#INTRFCE-BSNSS-DTE", FieldType.STRING, 8);
        bena971_Pnd_Intrfcng_Systm = bena971.newFieldInGroup("bena971_Pnd_Intrfcng_Systm", "#INTRFCNG-SYSTM", FieldType.STRING, 8);
        bena971_Pnd_Rqstng_System = bena971.newFieldInGroup("bena971_Pnd_Rqstng_System", "#RQSTNG-SYSTEM", FieldType.STRING, 8);
        bena971_Pnd_New_Issuefslash_Chng_Ind = bena971.newFieldInGroup("bena971_Pnd_New_Issuefslash_Chng_Ind", "#NEW-ISSUE/CHNG-IND", FieldType.STRING, 
            1);
        bena971_Pnd_Illgble_Ind = bena971.newFieldInGroup("bena971_Pnd_Illgble_Ind", "#ILLGBLE-IND", FieldType.STRING, 1);
        bena971_Pnd_More_Than_Five_Benes_Ind = bena971.newFieldInGroup("bena971_Pnd_More_Than_Five_Benes_Ind", "#MORE-THAN-FIVE-BENES-IND", FieldType.STRING, 
            1);
        bena971_Pnd_Nmbr_Of_Benes = bena971.newFieldInGroup("bena971_Pnd_Nmbr_Of_Benes", "#NMBR-OF-BENES", FieldType.PACKED_DECIMAL, 3);
        bena971_Pnd_Intrfce_Mgrtn_Ind = bena971.newFieldInGroup("bena971_Pnd_Intrfce_Mgrtn_Ind", "#INTRFCE-MGRTN-IND", FieldType.STRING, 1);
        bena971_Pnd_Pin = bena971.newFieldInGroup("bena971_Pnd_Pin", "#PIN", FieldType.NUMERIC, 12);
        bena971_Pnd_Tiaa_Cntrct = bena971.newFieldInGroup("bena971_Pnd_Tiaa_Cntrct", "#TIAA-CNTRCT", FieldType.STRING, 10);
        bena971_Pnd_Cref_Cntrct = bena971.newFieldInGroup("bena971_Pnd_Cref_Cntrct", "#CREF-CNTRCT", FieldType.STRING, 10);
        bena971_Pnd_Cntrct_Type = bena971.newFieldInGroup("bena971_Pnd_Cntrct_Type", "#CNTRCT-TYPE", FieldType.STRING, 1);
        bena971_Pnd_Effctve_Dte = bena971.newFieldInGroup("bena971_Pnd_Effctve_Dte", "#EFFCTVE-DTE", FieldType.STRING, 8);
        bena971_Pnd_Tiaa_Cref_Ind = bena971.newFieldInGroup("bena971_Pnd_Tiaa_Cref_Ind", "#TIAA-CREF-IND", FieldType.STRING, 1);
        bena971_Pnd_Same_As_Ind = bena971.newFieldInGroup("bena971_Pnd_Same_As_Ind", "#SAME-AS-IND", FieldType.STRING, 1);
        bena971_Pnd_Mos_Ind = bena971.newFieldInGroup("bena971_Pnd_Mos_Ind", "#MOS-IND", FieldType.STRING, 1);
        bena971_Pnd_Mos_Irrvcble_Ind = bena971.newFieldInGroup("bena971_Pnd_Mos_Irrvcble_Ind", "#MOS-IRRVCBLE-IND", FieldType.STRING, 1);
        bena971_Pnd_Pymnt_Chld_Dcsd_Ind = bena971.newFieldInGroup("bena971_Pnd_Pymnt_Chld_Dcsd_Ind", "#PYMNT-CHLD-DCSD-IND", FieldType.STRING, 1);
        bena971_Pnd_Exempt_Spouse_Rights = bena971.newFieldInGroup("bena971_Pnd_Exempt_Spouse_Rights", "#EXEMPT-SPOUSE-RIGHTS", FieldType.STRING, 1);
        bena971_Pnd_Spouse_Waived_Bnfts = bena971.newFieldInGroup("bena971_Pnd_Spouse_Waived_Bnfts", "#SPOUSE-WAIVED-BNFTS", FieldType.STRING, 1);
        bena971_Pnd_Fldr_Log_Dte_Tme = bena971.newFieldInGroup("bena971_Pnd_Fldr_Log_Dte_Tme", "#FLDR-LOG-DTE-TME", FieldType.STRING, 15);
        bena971_Pnd_Dflt_To_Estate = bena971.newFieldInGroup("bena971_Pnd_Dflt_To_Estate", "#DFLT-TO-ESTATE", FieldType.STRING, 1);
        bena971_Pnd_Mos_Txt = bena971.newFieldArrayInGroup("bena971_Pnd_Mos_Txt", "#MOS-TXT", FieldType.STRING, 72, new DbsArrayController(1,60));
        bena971_Pnd_Bene_Data = bena971.newGroupArrayInGroup("bena971_Pnd_Bene_Data", "#BENE-DATA", new DbsArrayController(1,30));
        bena971_Pnd_Bene_Name1 = bena971_Pnd_Bene_Data.newFieldInGroup("bena971_Pnd_Bene_Name1", "#BENE-NAME1", FieldType.STRING, 35);
        bena971_Pnd_Bene_Name2 = bena971_Pnd_Bene_Data.newFieldInGroup("bena971_Pnd_Bene_Name2", "#BENE-NAME2", FieldType.STRING, 35);
        bena971_Pnd_Bene_Type = bena971_Pnd_Bene_Data.newFieldInGroup("bena971_Pnd_Bene_Type", "#BENE-TYPE", FieldType.STRING, 1);
        bena971_Pnd_Dte_Birth_Trust = bena971_Pnd_Bene_Data.newFieldInGroup("bena971_Pnd_Dte_Birth_Trust", "#DTE-BIRTH-TRUST", FieldType.STRING, 8);
        bena971_Pnd_Rtln_Cde = bena971_Pnd_Bene_Data.newFieldInGroup("bena971_Pnd_Rtln_Cde", "#RTLN-CDE", FieldType.STRING, 2);
        bena971_Pnd_Rltn_Free_Txt = bena971_Pnd_Bene_Data.newFieldInGroup("bena971_Pnd_Rltn_Free_Txt", "#RLTN-FREE-TXT", FieldType.STRING, 15);
        bena971_Pnd_Ss_Cd = bena971_Pnd_Bene_Data.newFieldInGroup("bena971_Pnd_Ss_Cd", "#SS-CD", FieldType.STRING, 1);
        bena971_Pnd_Ss_Nbr = bena971_Pnd_Bene_Data.newFieldInGroup("bena971_Pnd_Ss_Nbr", "#SS-NBR", FieldType.STRING, 9);
        bena971_Pnd_Prctge_Frctn_Ind = bena971_Pnd_Bene_Data.newFieldInGroup("bena971_Pnd_Prctge_Frctn_Ind", "#PRCTGE-FRCTN-IND", FieldType.STRING, 1);
        bena971_Pnd_Addr1 = bena971_Pnd_Bene_Data.newFieldInGroup("bena971_Pnd_Addr1", "#ADDR1", FieldType.STRING, 35);
        bena971_Pnd_Addr2 = bena971_Pnd_Bene_Data.newFieldInGroup("bena971_Pnd_Addr2", "#ADDR2", FieldType.STRING, 35);
        bena971_Pnd_Addr3_City = bena971_Pnd_Bene_Data.newFieldInGroup("bena971_Pnd_Addr3_City", "#ADDR3-CITY", FieldType.STRING, 35);
        bena971_Pnd_State = bena971_Pnd_Bene_Data.newFieldInGroup("bena971_Pnd_State", "#STATE", FieldType.STRING, 2);
        bena971_Pnd_Zip = bena971_Pnd_Bene_Data.newFieldInGroup("bena971_Pnd_Zip", "#ZIP", FieldType.STRING, 10);
        bena971_Pnd_Country = bena971_Pnd_Bene_Data.newFieldInGroup("bena971_Pnd_Country", "#COUNTRY", FieldType.STRING, 35);
        bena971_Pnd_Phone = bena971_Pnd_Bene_Data.newFieldInGroup("bena971_Pnd_Phone", "#PHONE", FieldType.STRING, 20);
        bena971_Pnd_Gender = bena971_Pnd_Bene_Data.newFieldInGroup("bena971_Pnd_Gender", "#GENDER", FieldType.STRING, 1);
        bena971_Pnd_Prctge = bena971_Pnd_Bene_Data.newFieldInGroup("bena971_Pnd_Prctge", "#PRCTGE", FieldType.DECIMAL, 5,2);
        bena971_Pnd_Nmrtr = bena971_Pnd_Bene_Data.newFieldInGroup("bena971_Pnd_Nmrtr", "#NMRTR", FieldType.NUMERIC, 3);
        bena971_Pnd_Dnmntr = bena971_Pnd_Bene_Data.newFieldInGroup("bena971_Pnd_Dnmntr", "#DNMNTR", FieldType.NUMERIC, 3);
        bena971_Pnd_Irrvcbl_Ind = bena971_Pnd_Bene_Data.newFieldInGroup("bena971_Pnd_Irrvcbl_Ind", "#IRRVCBL-IND", FieldType.STRING, 1);
        bena971_Pnd_Sttlmnt_Rstrctn = bena971_Pnd_Bene_Data.newFieldInGroup("bena971_Pnd_Sttlmnt_Rstrctn", "#STTLMNT-RSTRCTN", FieldType.STRING, 1);
        bena971_Pnd_Spcl_Txt = bena971_Pnd_Bene_Data.newFieldArrayInGroup("bena971_Pnd_Spcl_Txt", "#SPCL-TXT", FieldType.STRING, 72, new DbsArrayController(1,
            3));
        bena971_Pnd_Mdo_Calc_Bene = bena971_Pnd_Bene_Data.newFieldInGroup("bena971_Pnd_Mdo_Calc_Bene", "#MDO-CALC-BENE", FieldType.STRING, 1);
        bena971_Pnd_Return_Code = bena971.newFieldInGroup("bena971_Pnd_Return_Code", "#RETURN-CODE", FieldType.STRING, 1);
        bena971_Pnd_Error_Msg = bena971.newFieldInGroup("bena971_Pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 60);

        dbsRecord.reset();
    }

    // Constructors
    public PdaBena971(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

