/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:18:12 PM
**        * FROM NATURAL PDA     : BENA9865
************************************************************
**        * FILE NAME            : PdaBena9865.java
**        * CLASS NAME           : PdaBena9865
**        * INSTANCE NAME        : PdaBena9865
************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaBena9865 extends PdaBase
{
    // Properties
    private DbsGroup bena9865;
    private DbsGroup bena9865_Pnd_Input_Fields;
    private DbsField bena9865_Pnd_Cntrct;
    private DbsGroup bena9865_Pnd_Output_Fields;
    private DbsField bena9865_Pnd_Pin;
    private DbsField bena9865_Pnd_Cntrct_Found;
    private DbsField bena9865_Pnd_Cntrct_Is_Active;
    private DbsField bena9865_Pnd_Pin_Found;
    private DbsField bena9865_Pnd_Cntrct_Issue_Dte;
    private DbsField bena9865_Pnd_Cntrct_Lob_Cde;
    private DbsField bena9865_Pnd_Cntrct_Nmbr_Range_Cde;
    private DbsField bena9865_Pnd_Cntrct_Spcl_Cnsdrtn_Cde;
    private DbsField bena9865_Pnd_Cntrct_Cref_Nbr;

    public DbsGroup getBena9865() { return bena9865; }

    public DbsGroup getBena9865_Pnd_Input_Fields() { return bena9865_Pnd_Input_Fields; }

    public DbsField getBena9865_Pnd_Cntrct() { return bena9865_Pnd_Cntrct; }

    public DbsGroup getBena9865_Pnd_Output_Fields() { return bena9865_Pnd_Output_Fields; }

    public DbsField getBena9865_Pnd_Pin() { return bena9865_Pnd_Pin; }

    public DbsField getBena9865_Pnd_Cntrct_Found() { return bena9865_Pnd_Cntrct_Found; }

    public DbsField getBena9865_Pnd_Cntrct_Is_Active() { return bena9865_Pnd_Cntrct_Is_Active; }

    public DbsField getBena9865_Pnd_Pin_Found() { return bena9865_Pnd_Pin_Found; }

    public DbsField getBena9865_Pnd_Cntrct_Issue_Dte() { return bena9865_Pnd_Cntrct_Issue_Dte; }

    public DbsField getBena9865_Pnd_Cntrct_Lob_Cde() { return bena9865_Pnd_Cntrct_Lob_Cde; }

    public DbsField getBena9865_Pnd_Cntrct_Nmbr_Range_Cde() { return bena9865_Pnd_Cntrct_Nmbr_Range_Cde; }

    public DbsField getBena9865_Pnd_Cntrct_Spcl_Cnsdrtn_Cde() { return bena9865_Pnd_Cntrct_Spcl_Cnsdrtn_Cde; }

    public DbsField getBena9865_Pnd_Cntrct_Cref_Nbr() { return bena9865_Pnd_Cntrct_Cref_Nbr; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        bena9865 = dbsRecord.newGroupInRecord("bena9865", "BENA9865");
        bena9865.setParameterOption(ParameterOption.ByReference);
        bena9865_Pnd_Input_Fields = bena9865.newGroupInGroup("bena9865_Pnd_Input_Fields", "#INPUT-FIELDS");
        bena9865_Pnd_Cntrct = bena9865_Pnd_Input_Fields.newFieldInGroup("bena9865_Pnd_Cntrct", "#CNTRCT", FieldType.STRING, 10);
        bena9865_Pnd_Output_Fields = bena9865.newGroupInGroup("bena9865_Pnd_Output_Fields", "#OUTPUT-FIELDS");
        bena9865_Pnd_Pin = bena9865_Pnd_Output_Fields.newFieldInGroup("bena9865_Pnd_Pin", "#PIN", FieldType.NUMERIC, 12);
        bena9865_Pnd_Cntrct_Found = bena9865_Pnd_Output_Fields.newFieldInGroup("bena9865_Pnd_Cntrct_Found", "#CNTRCT-FOUND", FieldType.BOOLEAN);
        bena9865_Pnd_Cntrct_Is_Active = bena9865_Pnd_Output_Fields.newFieldInGroup("bena9865_Pnd_Cntrct_Is_Active", "#CNTRCT-IS-ACTIVE", FieldType.BOOLEAN);
        bena9865_Pnd_Pin_Found = bena9865_Pnd_Output_Fields.newFieldInGroup("bena9865_Pnd_Pin_Found", "#PIN-FOUND", FieldType.BOOLEAN);
        bena9865_Pnd_Cntrct_Issue_Dte = bena9865_Pnd_Output_Fields.newFieldInGroup("bena9865_Pnd_Cntrct_Issue_Dte", "#CNTRCT-ISSUE-DTE", FieldType.STRING, 
            8);
        bena9865_Pnd_Cntrct_Lob_Cde = bena9865_Pnd_Output_Fields.newFieldInGroup("bena9865_Pnd_Cntrct_Lob_Cde", "#CNTRCT-LOB-CDE", FieldType.STRING, 1);
        bena9865_Pnd_Cntrct_Nmbr_Range_Cde = bena9865_Pnd_Output_Fields.newFieldInGroup("bena9865_Pnd_Cntrct_Nmbr_Range_Cde", "#CNTRCT-NMBR-RANGE-CDE", 
            FieldType.STRING, 2);
        bena9865_Pnd_Cntrct_Spcl_Cnsdrtn_Cde = bena9865_Pnd_Output_Fields.newFieldInGroup("bena9865_Pnd_Cntrct_Spcl_Cnsdrtn_Cde", "#CNTRCT-SPCL-CNSDRTN-CDE", 
            FieldType.STRING, 2);
        bena9865_Pnd_Cntrct_Cref_Nbr = bena9865_Pnd_Output_Fields.newFieldInGroup("bena9865_Pnd_Cntrct_Cref_Nbr", "#CNTRCT-CREF-NBR", FieldType.STRING, 
            10);

        dbsRecord.reset();
    }

    // Constructors
    public PdaBena9865(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

