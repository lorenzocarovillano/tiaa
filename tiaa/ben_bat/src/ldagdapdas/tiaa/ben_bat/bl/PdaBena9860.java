/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:18:11 PM
**        * FROM NATURAL PDA     : BENA9860
************************************************************
**        * FILE NAME            : PdaBena9860.java
**        * CLASS NAME           : PdaBena9860
**        * INSTANCE NAME        : PdaBena9860
************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaBena9860 extends PdaBase
{
    // Properties
    private DbsGroup bena9860;
    private DbsField bena9860_Intrfce_Stts;
    private DbsField bena9860_Rcrd_Crtd_For_Bsnss_Dte;
    private DbsField bena9860_Rcrd_Crtd_Dte;
    private DbsField bena9860_Rcrd_Crtd_Tme;
    private DbsField bena9860_Intrfcng_Systm;
    private DbsField bena9860_Rqstng_Systm;
    private DbsField bena9860_Rcrd_Last_Updt_Dte;
    private DbsField bena9860_Rcrd_Last_Updt_Tme;
    private DbsField bena9860_Rcrd_Last_Updt_Userid;
    private DbsField bena9860_Last_Prcssd_Bsnss_Dte;
    private DbsField bena9860_Intrfce_Cmpltd_Dte;
    private DbsField bena9860_Intrfce_Cmpltd_Tme;
    private DbsField bena9860_Dflt_To_Estate_Ind;
    private DbsField bena9860_Illgble_Ind;
    private DbsField bena9860_More_Than_Five_Benes_Ind;
    private DbsField bena9860_Intrfce_Mgrtn_Ind;
    private DbsField bena9860_More_Than_Thirty_Benes_Ind;
    private DbsField bena9860_Pin_Tiaa_Cntrct;
    private DbsGroup bena9860_Pin_Tiaa_CntrctRedef1;
    private DbsField bena9860_Pin;
    private DbsField bena9860_Tiaa_Cntrct;
    private DbsGroup bena9860_Error_Table;
    private DbsField bena9860_Error_Txt;
    private DbsField bena9860_Error_Cde;
    private DbsField bena9860_Error_Dte;
    private DbsField bena9860_Error_Tme;
    private DbsField bena9860_Error_Pgm;
    private DbsField bena9860_Fldr_Log_Dte_Tme;
    private DbsField bena9860_Last_Dsgntn_Srce;
    private DbsField bena9860_Last_Dsgntn_System;
    private DbsField bena9860_Last_Dsgntn_Userid;
    private DbsField bena9860_Last_Dsgntn_Dte;
    private DbsField bena9860_Last_Dsgntn_Tme;
    private DbsField bena9860_Tiaa_Cref_Chng_Dte;
    private DbsField bena9860_Tiaa_Cref_Chng_Tme;
    private DbsField bena9860_Chng_Pwr_Atty;
    private DbsField bena9860_Cref_Cntrct;
    private DbsField bena9860_Tiaa_Cref_Ind;
    private DbsField bena9860_Cntrct_Type;
    private DbsField bena9860_Eff_Dte;
    private DbsField bena9860_Mos_Ind;
    private DbsField bena9860_Irrvcble_Ind;
    private DbsField bena9860_Pymnt_Chld_Dcsd_Ind;
    private DbsField bena9860_Exempt_Spouse_Rights;
    private DbsField bena9860_Spouse_Waived_Bnfts;
    private DbsField bena9860_Chng_New_Issue_Ind;
    private DbsField bena9860_Same_As_Ind;
    private DbsField bena9860_Pnd_Cntrct_Isn;

    public DbsGroup getBena9860() { return bena9860; }

    public DbsField getBena9860_Intrfce_Stts() { return bena9860_Intrfce_Stts; }

    public DbsField getBena9860_Rcrd_Crtd_For_Bsnss_Dte() { return bena9860_Rcrd_Crtd_For_Bsnss_Dte; }

    public DbsField getBena9860_Rcrd_Crtd_Dte() { return bena9860_Rcrd_Crtd_Dte; }

    public DbsField getBena9860_Rcrd_Crtd_Tme() { return bena9860_Rcrd_Crtd_Tme; }

    public DbsField getBena9860_Intrfcng_Systm() { return bena9860_Intrfcng_Systm; }

    public DbsField getBena9860_Rqstng_Systm() { return bena9860_Rqstng_Systm; }

    public DbsField getBena9860_Rcrd_Last_Updt_Dte() { return bena9860_Rcrd_Last_Updt_Dte; }

    public DbsField getBena9860_Rcrd_Last_Updt_Tme() { return bena9860_Rcrd_Last_Updt_Tme; }

    public DbsField getBena9860_Rcrd_Last_Updt_Userid() { return bena9860_Rcrd_Last_Updt_Userid; }

    public DbsField getBena9860_Last_Prcssd_Bsnss_Dte() { return bena9860_Last_Prcssd_Bsnss_Dte; }

    public DbsField getBena9860_Intrfce_Cmpltd_Dte() { return bena9860_Intrfce_Cmpltd_Dte; }

    public DbsField getBena9860_Intrfce_Cmpltd_Tme() { return bena9860_Intrfce_Cmpltd_Tme; }

    public DbsField getBena9860_Dflt_To_Estate_Ind() { return bena9860_Dflt_To_Estate_Ind; }

    public DbsField getBena9860_Illgble_Ind() { return bena9860_Illgble_Ind; }

    public DbsField getBena9860_More_Than_Five_Benes_Ind() { return bena9860_More_Than_Five_Benes_Ind; }

    public DbsField getBena9860_Intrfce_Mgrtn_Ind() { return bena9860_Intrfce_Mgrtn_Ind; }

    public DbsField getBena9860_More_Than_Thirty_Benes_Ind() { return bena9860_More_Than_Thirty_Benes_Ind; }

    public DbsField getBena9860_Pin_Tiaa_Cntrct() { return bena9860_Pin_Tiaa_Cntrct; }

    public DbsGroup getBena9860_Pin_Tiaa_CntrctRedef1() { return bena9860_Pin_Tiaa_CntrctRedef1; }

    public DbsField getBena9860_Pin() { return bena9860_Pin; }

    public DbsField getBena9860_Tiaa_Cntrct() { return bena9860_Tiaa_Cntrct; }

    public DbsGroup getBena9860_Error_Table() { return bena9860_Error_Table; }

    public DbsField getBena9860_Error_Txt() { return bena9860_Error_Txt; }

    public DbsField getBena9860_Error_Cde() { return bena9860_Error_Cde; }

    public DbsField getBena9860_Error_Dte() { return bena9860_Error_Dte; }

    public DbsField getBena9860_Error_Tme() { return bena9860_Error_Tme; }

    public DbsField getBena9860_Error_Pgm() { return bena9860_Error_Pgm; }

    public DbsField getBena9860_Fldr_Log_Dte_Tme() { return bena9860_Fldr_Log_Dte_Tme; }

    public DbsField getBena9860_Last_Dsgntn_Srce() { return bena9860_Last_Dsgntn_Srce; }

    public DbsField getBena9860_Last_Dsgntn_System() { return bena9860_Last_Dsgntn_System; }

    public DbsField getBena9860_Last_Dsgntn_Userid() { return bena9860_Last_Dsgntn_Userid; }

    public DbsField getBena9860_Last_Dsgntn_Dte() { return bena9860_Last_Dsgntn_Dte; }

    public DbsField getBena9860_Last_Dsgntn_Tme() { return bena9860_Last_Dsgntn_Tme; }

    public DbsField getBena9860_Tiaa_Cref_Chng_Dte() { return bena9860_Tiaa_Cref_Chng_Dte; }

    public DbsField getBena9860_Tiaa_Cref_Chng_Tme() { return bena9860_Tiaa_Cref_Chng_Tme; }

    public DbsField getBena9860_Chng_Pwr_Atty() { return bena9860_Chng_Pwr_Atty; }

    public DbsField getBena9860_Cref_Cntrct() { return bena9860_Cref_Cntrct; }

    public DbsField getBena9860_Tiaa_Cref_Ind() { return bena9860_Tiaa_Cref_Ind; }

    public DbsField getBena9860_Cntrct_Type() { return bena9860_Cntrct_Type; }

    public DbsField getBena9860_Eff_Dte() { return bena9860_Eff_Dte; }

    public DbsField getBena9860_Mos_Ind() { return bena9860_Mos_Ind; }

    public DbsField getBena9860_Irrvcble_Ind() { return bena9860_Irrvcble_Ind; }

    public DbsField getBena9860_Pymnt_Chld_Dcsd_Ind() { return bena9860_Pymnt_Chld_Dcsd_Ind; }

    public DbsField getBena9860_Exempt_Spouse_Rights() { return bena9860_Exempt_Spouse_Rights; }

    public DbsField getBena9860_Spouse_Waived_Bnfts() { return bena9860_Spouse_Waived_Bnfts; }

    public DbsField getBena9860_Chng_New_Issue_Ind() { return bena9860_Chng_New_Issue_Ind; }

    public DbsField getBena9860_Same_As_Ind() { return bena9860_Same_As_Ind; }

    public DbsField getBena9860_Pnd_Cntrct_Isn() { return bena9860_Pnd_Cntrct_Isn; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        bena9860 = dbsRecord.newGroupInRecord("bena9860", "BENA9860");
        bena9860.setParameterOption(ParameterOption.ByReference);
        bena9860_Intrfce_Stts = bena9860.newFieldInGroup("bena9860_Intrfce_Stts", "INTRFCE-STTS", FieldType.STRING, 1);
        bena9860_Rcrd_Crtd_For_Bsnss_Dte = bena9860.newFieldInGroup("bena9860_Rcrd_Crtd_For_Bsnss_Dte", "RCRD-CRTD-FOR-BSNSS-DTE", FieldType.STRING, 8);
        bena9860_Rcrd_Crtd_Dte = bena9860.newFieldInGroup("bena9860_Rcrd_Crtd_Dte", "RCRD-CRTD-DTE", FieldType.STRING, 8);
        bena9860_Rcrd_Crtd_Tme = bena9860.newFieldInGroup("bena9860_Rcrd_Crtd_Tme", "RCRD-CRTD-TME", FieldType.STRING, 7);
        bena9860_Intrfcng_Systm = bena9860.newFieldInGroup("bena9860_Intrfcng_Systm", "INTRFCNG-SYSTM", FieldType.STRING, 8);
        bena9860_Rqstng_Systm = bena9860.newFieldInGroup("bena9860_Rqstng_Systm", "RQSTNG-SYSTM", FieldType.STRING, 8);
        bena9860_Rcrd_Last_Updt_Dte = bena9860.newFieldInGroup("bena9860_Rcrd_Last_Updt_Dte", "RCRD-LAST-UPDT-DTE", FieldType.STRING, 8);
        bena9860_Rcrd_Last_Updt_Tme = bena9860.newFieldInGroup("bena9860_Rcrd_Last_Updt_Tme", "RCRD-LAST-UPDT-TME", FieldType.STRING, 7);
        bena9860_Rcrd_Last_Updt_Userid = bena9860.newFieldInGroup("bena9860_Rcrd_Last_Updt_Userid", "RCRD-LAST-UPDT-USERID", FieldType.STRING, 8);
        bena9860_Last_Prcssd_Bsnss_Dte = bena9860.newFieldInGroup("bena9860_Last_Prcssd_Bsnss_Dte", "LAST-PRCSSD-BSNSS-DTE", FieldType.STRING, 8);
        bena9860_Intrfce_Cmpltd_Dte = bena9860.newFieldInGroup("bena9860_Intrfce_Cmpltd_Dte", "INTRFCE-CMPLTD-DTE", FieldType.STRING, 8);
        bena9860_Intrfce_Cmpltd_Tme = bena9860.newFieldInGroup("bena9860_Intrfce_Cmpltd_Tme", "INTRFCE-CMPLTD-TME", FieldType.STRING, 7);
        bena9860_Dflt_To_Estate_Ind = bena9860.newFieldInGroup("bena9860_Dflt_To_Estate_Ind", "DFLT-TO-ESTATE-IND", FieldType.STRING, 1);
        bena9860_Illgble_Ind = bena9860.newFieldInGroup("bena9860_Illgble_Ind", "ILLGBLE-IND", FieldType.STRING, 1);
        bena9860_More_Than_Five_Benes_Ind = bena9860.newFieldInGroup("bena9860_More_Than_Five_Benes_Ind", "MORE-THAN-FIVE-BENES-IND", FieldType.STRING, 
            1);
        bena9860_Intrfce_Mgrtn_Ind = bena9860.newFieldInGroup("bena9860_Intrfce_Mgrtn_Ind", "INTRFCE-MGRTN-IND", FieldType.STRING, 1);
        bena9860_More_Than_Thirty_Benes_Ind = bena9860.newFieldInGroup("bena9860_More_Than_Thirty_Benes_Ind", "MORE-THAN-THIRTY-BENES-IND", FieldType.STRING, 
            1);
        bena9860_Pin_Tiaa_Cntrct = bena9860.newFieldInGroup("bena9860_Pin_Tiaa_Cntrct", "PIN-TIAA-CNTRCT", FieldType.STRING, 22);
        bena9860_Pin_Tiaa_CntrctRedef1 = bena9860.newGroupInGroup("bena9860_Pin_Tiaa_CntrctRedef1", "Redefines", bena9860_Pin_Tiaa_Cntrct);
        bena9860_Pin = bena9860_Pin_Tiaa_CntrctRedef1.newFieldInGroup("bena9860_Pin", "PIN", FieldType.NUMERIC, 12);
        bena9860_Tiaa_Cntrct = bena9860_Pin_Tiaa_CntrctRedef1.newFieldInGroup("bena9860_Tiaa_Cntrct", "TIAA-CNTRCT", FieldType.STRING, 10);
        bena9860_Error_Table = bena9860.newGroupArrayInGroup("bena9860_Error_Table", "ERROR-TABLE", new DbsArrayController(1,10));
        bena9860_Error_Txt = bena9860_Error_Table.newFieldInGroup("bena9860_Error_Txt", "ERROR-TXT", FieldType.STRING, 72);
        bena9860_Error_Cde = bena9860_Error_Table.newFieldInGroup("bena9860_Error_Cde", "ERROR-CDE", FieldType.STRING, 5);
        bena9860_Error_Dte = bena9860_Error_Table.newFieldInGroup("bena9860_Error_Dte", "ERROR-DTE", FieldType.STRING, 8);
        bena9860_Error_Tme = bena9860_Error_Table.newFieldInGroup("bena9860_Error_Tme", "ERROR-TME", FieldType.STRING, 7);
        bena9860_Error_Pgm = bena9860_Error_Table.newFieldInGroup("bena9860_Error_Pgm", "ERROR-PGM", FieldType.STRING, 8);
        bena9860_Fldr_Log_Dte_Tme = bena9860.newFieldInGroup("bena9860_Fldr_Log_Dte_Tme", "FLDR-LOG-DTE-TME", FieldType.STRING, 15);
        bena9860_Last_Dsgntn_Srce = bena9860.newFieldInGroup("bena9860_Last_Dsgntn_Srce", "LAST-DSGNTN-SRCE", FieldType.STRING, 1);
        bena9860_Last_Dsgntn_System = bena9860.newFieldInGroup("bena9860_Last_Dsgntn_System", "LAST-DSGNTN-SYSTEM", FieldType.STRING, 1);
        bena9860_Last_Dsgntn_Userid = bena9860.newFieldInGroup("bena9860_Last_Dsgntn_Userid", "LAST-DSGNTN-USERID", FieldType.STRING, 8);
        bena9860_Last_Dsgntn_Dte = bena9860.newFieldInGroup("bena9860_Last_Dsgntn_Dte", "LAST-DSGNTN-DTE", FieldType.STRING, 8);
        bena9860_Last_Dsgntn_Tme = bena9860.newFieldInGroup("bena9860_Last_Dsgntn_Tme", "LAST-DSGNTN-TME", FieldType.STRING, 7);
        bena9860_Tiaa_Cref_Chng_Dte = bena9860.newFieldInGroup("bena9860_Tiaa_Cref_Chng_Dte", "TIAA-CREF-CHNG-DTE", FieldType.STRING, 8);
        bena9860_Tiaa_Cref_Chng_Tme = bena9860.newFieldInGroup("bena9860_Tiaa_Cref_Chng_Tme", "TIAA-CREF-CHNG-TME", FieldType.STRING, 7);
        bena9860_Chng_Pwr_Atty = bena9860.newFieldInGroup("bena9860_Chng_Pwr_Atty", "CHNG-PWR-ATTY", FieldType.STRING, 1);
        bena9860_Cref_Cntrct = bena9860.newFieldInGroup("bena9860_Cref_Cntrct", "CREF-CNTRCT", FieldType.STRING, 10);
        bena9860_Tiaa_Cref_Ind = bena9860.newFieldInGroup("bena9860_Tiaa_Cref_Ind", "TIAA-CREF-IND", FieldType.STRING, 1);
        bena9860_Cntrct_Type = bena9860.newFieldInGroup("bena9860_Cntrct_Type", "CNTRCT-TYPE", FieldType.STRING, 1);
        bena9860_Eff_Dte = bena9860.newFieldInGroup("bena9860_Eff_Dte", "EFF-DTE", FieldType.STRING, 8);
        bena9860_Mos_Ind = bena9860.newFieldInGroup("bena9860_Mos_Ind", "MOS-IND", FieldType.STRING, 1);
        bena9860_Irrvcble_Ind = bena9860.newFieldInGroup("bena9860_Irrvcble_Ind", "IRRVCBLE-IND", FieldType.STRING, 1);
        bena9860_Pymnt_Chld_Dcsd_Ind = bena9860.newFieldInGroup("bena9860_Pymnt_Chld_Dcsd_Ind", "PYMNT-CHLD-DCSD-IND", FieldType.STRING, 1);
        bena9860_Exempt_Spouse_Rights = bena9860.newFieldInGroup("bena9860_Exempt_Spouse_Rights", "EXEMPT-SPOUSE-RIGHTS", FieldType.STRING, 1);
        bena9860_Spouse_Waived_Bnfts = bena9860.newFieldInGroup("bena9860_Spouse_Waived_Bnfts", "SPOUSE-WAIVED-BNFTS", FieldType.STRING, 1);
        bena9860_Chng_New_Issue_Ind = bena9860.newFieldInGroup("bena9860_Chng_New_Issue_Ind", "CHNG-NEW-ISSUE-IND", FieldType.STRING, 1);
        bena9860_Same_As_Ind = bena9860.newFieldInGroup("bena9860_Same_As_Ind", "SAME-AS-IND", FieldType.STRING, 1);
        bena9860_Pnd_Cntrct_Isn = bena9860.newFieldInGroup("bena9860_Pnd_Cntrct_Isn", "#CNTRCT-ISN", FieldType.PACKED_DECIMAL, 10);

        dbsRecord.reset();
    }

    // Constructors
    public PdaBena9860(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

