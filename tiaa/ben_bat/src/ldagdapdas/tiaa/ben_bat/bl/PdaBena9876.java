/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:18:14 PM
**        * FROM NATURAL PDA     : BENA9876
************************************************************
**        * FILE NAME            : PdaBena9876.java
**        * CLASS NAME           : PdaBena9876
**        * INSTANCE NAME        : PdaBena9876
************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaBena9876 extends PdaBase
{
    // Properties
    private DbsGroup bena9876;
    private DbsField bena9876_Pnd_Pin_Tiaa_Cntrct;
    private DbsGroup bena9876_Pnd_Pin_Tiaa_CntrctRedef1;
    private DbsField bena9876_Pnd_Pin;
    private DbsField bena9876_Pnd_Tiaa_Cntrct;
    private DbsField bena9876_Pnd_Tiaa_Cref_Ind;
    private DbsField bena9876_Pnd_Bc_Eff_Dte;
    private DbsField bena9876_Pnd_Found;

    public DbsGroup getBena9876() { return bena9876; }

    public DbsField getBena9876_Pnd_Pin_Tiaa_Cntrct() { return bena9876_Pnd_Pin_Tiaa_Cntrct; }

    public DbsGroup getBena9876_Pnd_Pin_Tiaa_CntrctRedef1() { return bena9876_Pnd_Pin_Tiaa_CntrctRedef1; }

    public DbsField getBena9876_Pnd_Pin() { return bena9876_Pnd_Pin; }

    public DbsField getBena9876_Pnd_Tiaa_Cntrct() { return bena9876_Pnd_Tiaa_Cntrct; }

    public DbsField getBena9876_Pnd_Tiaa_Cref_Ind() { return bena9876_Pnd_Tiaa_Cref_Ind; }

    public DbsField getBena9876_Pnd_Bc_Eff_Dte() { return bena9876_Pnd_Bc_Eff_Dte; }

    public DbsField getBena9876_Pnd_Found() { return bena9876_Pnd_Found; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        bena9876 = dbsRecord.newGroupInRecord("bena9876", "BENA9876");
        bena9876.setParameterOption(ParameterOption.ByReference);
        bena9876_Pnd_Pin_Tiaa_Cntrct = bena9876.newFieldInGroup("bena9876_Pnd_Pin_Tiaa_Cntrct", "#PIN-TIAA-CNTRCT", FieldType.STRING, 22);
        bena9876_Pnd_Pin_Tiaa_CntrctRedef1 = bena9876.newGroupInGroup("bena9876_Pnd_Pin_Tiaa_CntrctRedef1", "Redefines", bena9876_Pnd_Pin_Tiaa_Cntrct);
        bena9876_Pnd_Pin = bena9876_Pnd_Pin_Tiaa_CntrctRedef1.newFieldInGroup("bena9876_Pnd_Pin", "#PIN", FieldType.NUMERIC, 12);
        bena9876_Pnd_Tiaa_Cntrct = bena9876_Pnd_Pin_Tiaa_CntrctRedef1.newFieldInGroup("bena9876_Pnd_Tiaa_Cntrct", "#TIAA-CNTRCT", FieldType.STRING, 10);
        bena9876_Pnd_Tiaa_Cref_Ind = bena9876.newFieldInGroup("bena9876_Pnd_Tiaa_Cref_Ind", "#TIAA-CREF-IND", FieldType.STRING, 1);
        bena9876_Pnd_Bc_Eff_Dte = bena9876.newFieldInGroup("bena9876_Pnd_Bc_Eff_Dte", "#BC-EFF-DTE", FieldType.STRING, 8);
        bena9876_Pnd_Found = bena9876.newFieldInGroup("bena9876_Pnd_Found", "#FOUND", FieldType.BOOLEAN);

        dbsRecord.reset();
    }

    // Constructors
    public PdaBena9876(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

