/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:18:12 PM
**        * FROM NATURAL PDA     : BENA9862
************************************************************
**        * FILE NAME            : PdaBena9862.java
**        * CLASS NAME           : PdaBena9862
**        * INSTANCE NAME        : PdaBena9862
************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaBena9862 extends PdaBase
{
    // Properties
    private DbsGroup bena9862;
    private DbsField bena9862_Pnd_Systm;
    private DbsField bena9862_Pnd_Systm_Code;
    private DbsField bena9862_Pnd_Systm_Text;
    private DbsField bena9862_Pnd_Rltnshp_Cde;
    private DbsField bena9862_Pnd_Not_On_Rt_Table;

    public DbsGroup getBena9862() { return bena9862; }

    public DbsField getBena9862_Pnd_Systm() { return bena9862_Pnd_Systm; }

    public DbsField getBena9862_Pnd_Systm_Code() { return bena9862_Pnd_Systm_Code; }

    public DbsField getBena9862_Pnd_Systm_Text() { return bena9862_Pnd_Systm_Text; }

    public DbsField getBena9862_Pnd_Rltnshp_Cde() { return bena9862_Pnd_Rltnshp_Cde; }

    public DbsField getBena9862_Pnd_Not_On_Rt_Table() { return bena9862_Pnd_Not_On_Rt_Table; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        bena9862 = dbsRecord.newGroupInRecord("bena9862", "BENA9862");
        bena9862.setParameterOption(ParameterOption.ByReference);
        bena9862_Pnd_Systm = bena9862.newFieldInGroup("bena9862_Pnd_Systm", "#SYSTM", FieldType.STRING, 8);
        bena9862_Pnd_Systm_Code = bena9862.newFieldInGroup("bena9862_Pnd_Systm_Code", "#SYSTM-CODE", FieldType.STRING, 8);
        bena9862_Pnd_Systm_Text = bena9862.newFieldInGroup("bena9862_Pnd_Systm_Text", "#SYSTM-TEXT", FieldType.STRING, 15);
        bena9862_Pnd_Rltnshp_Cde = bena9862.newFieldInGroup("bena9862_Pnd_Rltnshp_Cde", "#RLTNSHP-CDE", FieldType.STRING, 2);
        bena9862_Pnd_Not_On_Rt_Table = bena9862.newFieldInGroup("bena9862_Pnd_Not_On_Rt_Table", "#NOT-ON-RT-TABLE", FieldType.BOOLEAN);

        dbsRecord.reset();
    }

    // Constructors
    public PdaBena9862(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

