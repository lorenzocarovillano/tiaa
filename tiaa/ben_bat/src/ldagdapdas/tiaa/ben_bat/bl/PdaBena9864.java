/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:18:12 PM
**        * FROM NATURAL PDA     : BENA9864
************************************************************
**        * FILE NAME            : PdaBena9864.java
**        * CLASS NAME           : PdaBena9864
**        * INSTANCE NAME        : PdaBena9864
************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaBena9864 extends PdaBase
{
    // Properties
    private DbsGroup bena9864;
    private DbsGroup bena9864_Pnd_Rc_Tab;
    private DbsField bena9864_Pnd_Rc_Tab_Key;
    private DbsField bena9864_Pnd_Rc_Tab_Text;
    private DbsGroup bena9864_Pnd_Rc_Tab_TextRedef1;
    private DbsField bena9864_Pnd_Bt_Table_Text_First_Char;
    private DbsGroup bena9864_Pnd_Dollar_R_Tab;
    private DbsField bena9864_Pnd_Dollar_R_Tab_Key;
    private DbsField bena9864_Pnd_Dollar_R_Tab_Text;
    private DbsField bena9864_Pnd_Xr_Tab_Key;
    private DbsField bena9864_Pnd_Ss_Tab_Key;
    private DbsField bena9864_Pnd_Sr_Tab_Key;

    public DbsGroup getBena9864() { return bena9864; }

    public DbsGroup getBena9864_Pnd_Rc_Tab() { return bena9864_Pnd_Rc_Tab; }

    public DbsField getBena9864_Pnd_Rc_Tab_Key() { return bena9864_Pnd_Rc_Tab_Key; }

    public DbsField getBena9864_Pnd_Rc_Tab_Text() { return bena9864_Pnd_Rc_Tab_Text; }

    public DbsGroup getBena9864_Pnd_Rc_Tab_TextRedef1() { return bena9864_Pnd_Rc_Tab_TextRedef1; }

    public DbsField getBena9864_Pnd_Bt_Table_Text_First_Char() { return bena9864_Pnd_Bt_Table_Text_First_Char; }

    public DbsGroup getBena9864_Pnd_Dollar_R_Tab() { return bena9864_Pnd_Dollar_R_Tab; }

    public DbsField getBena9864_Pnd_Dollar_R_Tab_Key() { return bena9864_Pnd_Dollar_R_Tab_Key; }

    public DbsField getBena9864_Pnd_Dollar_R_Tab_Text() { return bena9864_Pnd_Dollar_R_Tab_Text; }

    public DbsField getBena9864_Pnd_Xr_Tab_Key() { return bena9864_Pnd_Xr_Tab_Key; }

    public DbsField getBena9864_Pnd_Ss_Tab_Key() { return bena9864_Pnd_Ss_Tab_Key; }

    public DbsField getBena9864_Pnd_Sr_Tab_Key() { return bena9864_Pnd_Sr_Tab_Key; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        bena9864 = dbsRecord.newGroupInRecord("bena9864", "BENA9864");
        bena9864.setParameterOption(ParameterOption.ByReference);
        bena9864_Pnd_Rc_Tab = bena9864.newGroupArrayInGroup("bena9864_Pnd_Rc_Tab", "#RC-TAB", new DbsArrayController(1,99));
        bena9864_Pnd_Rc_Tab_Key = bena9864_Pnd_Rc_Tab.newFieldInGroup("bena9864_Pnd_Rc_Tab_Key", "#RC-TAB-KEY", FieldType.STRING, 2);
        bena9864_Pnd_Rc_Tab_Text = bena9864_Pnd_Rc_Tab.newFieldInGroup("bena9864_Pnd_Rc_Tab_Text", "#RC-TAB-TEXT", FieldType.STRING, 15);
        bena9864_Pnd_Rc_Tab_TextRedef1 = bena9864_Pnd_Rc_Tab.newGroupInGroup("bena9864_Pnd_Rc_Tab_TextRedef1", "Redefines", bena9864_Pnd_Rc_Tab_Text);
        bena9864_Pnd_Bt_Table_Text_First_Char = bena9864_Pnd_Rc_Tab_TextRedef1.newFieldInGroup("bena9864_Pnd_Bt_Table_Text_First_Char", "#BT-TABLE-TEXT-FIRST-CHAR", 
            FieldType.STRING, 1);
        bena9864_Pnd_Dollar_R_Tab = bena9864.newGroupArrayInGroup("bena9864_Pnd_Dollar_R_Tab", "#$R-TAB", new DbsArrayController(1,99));
        bena9864_Pnd_Dollar_R_Tab_Key = bena9864_Pnd_Dollar_R_Tab.newFieldInGroup("bena9864_Pnd_Dollar_R_Tab_Key", "#$R-TAB-KEY", FieldType.STRING, 16);
        bena9864_Pnd_Dollar_R_Tab_Text = bena9864_Pnd_Dollar_R_Tab.newFieldInGroup("bena9864_Pnd_Dollar_R_Tab_Text", "#$R-TAB-TEXT", FieldType.STRING, 
            2);
        bena9864_Pnd_Xr_Tab_Key = bena9864.newFieldArrayInGroup("bena9864_Pnd_Xr_Tab_Key", "#XR-TAB-KEY", FieldType.STRING, 2, new DbsArrayController(1,
            20));
        bena9864_Pnd_Ss_Tab_Key = bena9864.newFieldArrayInGroup("bena9864_Pnd_Ss_Tab_Key", "#SS-TAB-KEY", FieldType.STRING, 2, new DbsArrayController(1,
            10));
        bena9864_Pnd_Sr_Tab_Key = bena9864.newFieldArrayInGroup("bena9864_Pnd_Sr_Tab_Key", "#SR-TAB-KEY", FieldType.STRING, 2, new DbsArrayController(1,
            10));

        dbsRecord.reset();
    }

    // Constructors
    public PdaBena9864(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

