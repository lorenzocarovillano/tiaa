/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:18:11 PM
**        * FROM NATURAL PDA     : BENA9850
************************************************************
**        * FILE NAME            : PdaBena9850.java
**        * CLASS NAME           : PdaBena9850
**        * INSTANCE NAME        : PdaBena9850
************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaBena9850 extends PdaBase
{
    // Properties
    private DbsGroup bena9850;
    private DbsField bena9850_Pnd_Cntrct;
    private DbsField bena9850_Pnd_Pin;
    private DbsField bena9850_Pnd_Cntrct_Found;
    private DbsField bena9850_Pnd_Pin_Found;
    private DbsField bena9850_Pnd_Cntrct_Issue_Dte;
    private DbsField bena9850_Pnd_Cntrct_Lob_Cde;
    private DbsField bena9850_Pnd_Cntrct_Nmbr_Range_Cde;
    private DbsField bena9850_Pnd_Cntrct_Spcl_Cnsdrtn_Cde;
    private DbsField bena9850_Pnd_Cntrct_Cref_Nbr;

    public DbsGroup getBena9850() { return bena9850; }

    public DbsField getBena9850_Pnd_Cntrct() { return bena9850_Pnd_Cntrct; }

    public DbsField getBena9850_Pnd_Pin() { return bena9850_Pnd_Pin; }

    public DbsField getBena9850_Pnd_Cntrct_Found() { return bena9850_Pnd_Cntrct_Found; }

    public DbsField getBena9850_Pnd_Pin_Found() { return bena9850_Pnd_Pin_Found; }

    public DbsField getBena9850_Pnd_Cntrct_Issue_Dte() { return bena9850_Pnd_Cntrct_Issue_Dte; }

    public DbsField getBena9850_Pnd_Cntrct_Lob_Cde() { return bena9850_Pnd_Cntrct_Lob_Cde; }

    public DbsField getBena9850_Pnd_Cntrct_Nmbr_Range_Cde() { return bena9850_Pnd_Cntrct_Nmbr_Range_Cde; }

    public DbsField getBena9850_Pnd_Cntrct_Spcl_Cnsdrtn_Cde() { return bena9850_Pnd_Cntrct_Spcl_Cnsdrtn_Cde; }

    public DbsField getBena9850_Pnd_Cntrct_Cref_Nbr() { return bena9850_Pnd_Cntrct_Cref_Nbr; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        bena9850 = dbsRecord.newGroupInRecord("bena9850", "BENA9850");
        bena9850.setParameterOption(ParameterOption.ByReference);
        bena9850_Pnd_Cntrct = bena9850.newFieldInGroup("bena9850_Pnd_Cntrct", "#CNTRCT", FieldType.STRING, 10);
        bena9850_Pnd_Pin = bena9850.newFieldInGroup("bena9850_Pnd_Pin", "#PIN", FieldType.NUMERIC, 12);
        bena9850_Pnd_Cntrct_Found = bena9850.newFieldInGroup("bena9850_Pnd_Cntrct_Found", "#CNTRCT-FOUND", FieldType.BOOLEAN);
        bena9850_Pnd_Pin_Found = bena9850.newFieldInGroup("bena9850_Pnd_Pin_Found", "#PIN-FOUND", FieldType.BOOLEAN);
        bena9850_Pnd_Cntrct_Issue_Dte = bena9850.newFieldInGroup("bena9850_Pnd_Cntrct_Issue_Dte", "#CNTRCT-ISSUE-DTE", FieldType.STRING, 8);
        bena9850_Pnd_Cntrct_Lob_Cde = bena9850.newFieldInGroup("bena9850_Pnd_Cntrct_Lob_Cde", "#CNTRCT-LOB-CDE", FieldType.STRING, 1);
        bena9850_Pnd_Cntrct_Nmbr_Range_Cde = bena9850.newFieldInGroup("bena9850_Pnd_Cntrct_Nmbr_Range_Cde", "#CNTRCT-NMBR-RANGE-CDE", FieldType.STRING, 
            2);
        bena9850_Pnd_Cntrct_Spcl_Cnsdrtn_Cde = bena9850.newFieldInGroup("bena9850_Pnd_Cntrct_Spcl_Cnsdrtn_Cde", "#CNTRCT-SPCL-CNSDRTN-CDE", FieldType.STRING, 
            2);
        bena9850_Pnd_Cntrct_Cref_Nbr = bena9850.newFieldInGroup("bena9850_Pnd_Cntrct_Cref_Nbr", "#CNTRCT-CREF-NBR", FieldType.STRING, 10);

        dbsRecord.reset();
    }

    // Constructors
    public PdaBena9850(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

