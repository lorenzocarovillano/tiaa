/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:18:14 PM
**        * FROM NATURAL PDA     : BENA9879
************************************************************
**        * FILE NAME            : PdaBena9879.java
**        * CLASS NAME           : PdaBena9879
**        * INSTANCE NAME        : PdaBena9879
************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaBena9879 extends PdaBase
{
    // Properties
    private DbsGroup bena9879;
    private DbsField bena9879_Pnd_Pin;
    private DbsField bena9879_Pnd_Tiaa_Cntrct;
    private DbsField bena9879_Pnd_Tiaa_Cref_Ind;
    private DbsField bena9879_Pnd_Rcrd_Crtd_Dte;
    private DbsField bena9879_Pnd_Rcrd_Crtd_Tme;
    private DbsField bena9879_Pnd_Exists;

    public DbsGroup getBena9879() { return bena9879; }

    public DbsField getBena9879_Pnd_Pin() { return bena9879_Pnd_Pin; }

    public DbsField getBena9879_Pnd_Tiaa_Cntrct() { return bena9879_Pnd_Tiaa_Cntrct; }

    public DbsField getBena9879_Pnd_Tiaa_Cref_Ind() { return bena9879_Pnd_Tiaa_Cref_Ind; }

    public DbsField getBena9879_Pnd_Rcrd_Crtd_Dte() { return bena9879_Pnd_Rcrd_Crtd_Dte; }

    public DbsField getBena9879_Pnd_Rcrd_Crtd_Tme() { return bena9879_Pnd_Rcrd_Crtd_Tme; }

    public DbsField getBena9879_Pnd_Exists() { return bena9879_Pnd_Exists; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        bena9879 = dbsRecord.newGroupInRecord("bena9879", "BENA9879");
        bena9879.setParameterOption(ParameterOption.ByReference);
        bena9879_Pnd_Pin = bena9879.newFieldInGroup("bena9879_Pnd_Pin", "#PIN", FieldType.NUMERIC, 12);
        bena9879_Pnd_Tiaa_Cntrct = bena9879.newFieldInGroup("bena9879_Pnd_Tiaa_Cntrct", "#TIAA-CNTRCT", FieldType.STRING, 10);
        bena9879_Pnd_Tiaa_Cref_Ind = bena9879.newFieldInGroup("bena9879_Pnd_Tiaa_Cref_Ind", "#TIAA-CREF-IND", FieldType.STRING, 1);
        bena9879_Pnd_Rcrd_Crtd_Dte = bena9879.newFieldInGroup("bena9879_Pnd_Rcrd_Crtd_Dte", "#RCRD-CRTD-DTE", FieldType.STRING, 8);
        bena9879_Pnd_Rcrd_Crtd_Tme = bena9879.newFieldInGroup("bena9879_Pnd_Rcrd_Crtd_Tme", "#RCRD-CRTD-TME", FieldType.STRING, 7);
        bena9879_Pnd_Exists = bena9879.newFieldInGroup("bena9879_Pnd_Exists", "#EXISTS", FieldType.BOOLEAN);

        dbsRecord.reset();
    }

    // Constructors
    public PdaBena9879(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

