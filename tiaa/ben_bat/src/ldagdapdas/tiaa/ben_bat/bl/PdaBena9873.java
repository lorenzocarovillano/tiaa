/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:18:13 PM
**        * FROM NATURAL PDA     : BENA9873
************************************************************
**        * FILE NAME            : PdaBena9873.java
**        * CLASS NAME           : PdaBena9873
**        * INSTANCE NAME        : PdaBena9873
************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaBena9873 extends PdaBase
{
    // Properties
    private DbsGroup bena9873;
    private DbsField bena9873_Pnd_Bsnss_Dte;
    private DbsGroup bena9873_Pnd_Bsnss_DteRedef1;
    private DbsField bena9873_Pnd_Bsnss_Dte_N;

    public DbsGroup getBena9873() { return bena9873; }

    public DbsField getBena9873_Pnd_Bsnss_Dte() { return bena9873_Pnd_Bsnss_Dte; }

    public DbsGroup getBena9873_Pnd_Bsnss_DteRedef1() { return bena9873_Pnd_Bsnss_DteRedef1; }

    public DbsField getBena9873_Pnd_Bsnss_Dte_N() { return bena9873_Pnd_Bsnss_Dte_N; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        bena9873 = dbsRecord.newGroupInRecord("bena9873", "BENA9873");
        bena9873.setParameterOption(ParameterOption.ByReference);
        bena9873_Pnd_Bsnss_Dte = bena9873.newFieldInGroup("bena9873_Pnd_Bsnss_Dte", "#BSNSS-DTE", FieldType.STRING, 8);
        bena9873_Pnd_Bsnss_DteRedef1 = bena9873.newGroupInGroup("bena9873_Pnd_Bsnss_DteRedef1", "Redefines", bena9873_Pnd_Bsnss_Dte);
        bena9873_Pnd_Bsnss_Dte_N = bena9873_Pnd_Bsnss_DteRedef1.newFieldInGroup("bena9873_Pnd_Bsnss_Dte_N", "#BSNSS-DTE-N", FieldType.NUMERIC, 8);

        dbsRecord.reset();
    }

    // Constructors
    public PdaBena9873(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

