/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:18:09 PM
**        * FROM NATURAL PDA     : BENA9800
************************************************************
**        * FILE NAME            : PdaBena9800.java
**        * CLASS NAME           : PdaBena9800
**        * INSTANCE NAME        : PdaBena9800
************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaBena9800 extends PdaBase
{
    // Properties
    private DbsGroup bena9800;
    private DbsField bena9800_Pnd_Bsnss_Dte;
    private DbsField bena9800_Pnd_Done_For_Today;

    public DbsGroup getBena9800() { return bena9800; }

    public DbsField getBena9800_Pnd_Bsnss_Dte() { return bena9800_Pnd_Bsnss_Dte; }

    public DbsField getBena9800_Pnd_Done_For_Today() { return bena9800_Pnd_Done_For_Today; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        bena9800 = dbsRecord.newGroupInRecord("bena9800", "BENA9800");
        bena9800.setParameterOption(ParameterOption.ByReference);
        bena9800_Pnd_Bsnss_Dte = bena9800.newFieldInGroup("bena9800_Pnd_Bsnss_Dte", "#BSNSS-DTE", FieldType.STRING, 8);
        bena9800_Pnd_Done_For_Today = bena9800.newFieldInGroup("bena9800_Pnd_Done_For_Today", "#DONE-FOR-TODAY", FieldType.BOOLEAN);

        dbsRecord.reset();
    }

    // Constructors
    public PdaBena9800(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

