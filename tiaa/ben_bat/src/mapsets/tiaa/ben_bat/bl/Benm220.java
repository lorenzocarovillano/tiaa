/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:40:13 PM
**        *   FROM NATURAL MAP   :  Benm220
************************************************************
**        * FILE NAME               : Benm220.java
**        * CLASS NAME              : Benm220
**        * INSTANCE NAME           : Benm220
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     ##PROGRAM #DATE-XD #PAGE-NUMBER
************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benm220 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Pnd_Program;
    private DbsField pnd_Date_Xd;
    private DbsField pnd_Page_Number;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Pnd_Program = parameters.newFieldInRecord("pnd_Pnd_Program", "##PROGRAM", FieldType.STRING, 8);
        pnd_Date_Xd = parameters.newFieldInRecord("pnd_Date_Xd", "#DATE-XD", FieldType.STRING, 10);
        pnd_Page_Number = parameters.newFieldInRecord("pnd_Page_Number", "#PAGE-NUMBER", FieldType.NUMERIC, 5);
        parameters.reset();
    }

    public Benm220() throws Exception
    {
        super("Benm220");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=006 LS=133 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Benm220", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Benm220"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Pnd_Program", pnd_Pnd_Program, true, 1, 1, 8, "GREEN", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_1", "BENEFICIARY SYSTEM", "GREEN", 1, 56, 18);
            uiForm.setUiControl("astDATX", Global.getDATX(), true, 1, 122, 10, "GREEN", "MM/DD/YYYY", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_2", "CONFIRMATION/DESIGNATION STATEMENT ERROR REPORT FOR", "GREEN", 2, 35, 51);
            uiForm.setUiControl("pnd_Date_Xd", pnd_Date_Xd, true, 2, 87, 10, "", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_3", "Page", "GREEN", 2, 121, 4);
            uiForm.setUiControl("pnd_Page_Number", pnd_Page_Number, true, 2, 126, 5, "GREEN", true, false, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_4", "Request", "GREEN", 4, 48, 7);
            uiForm.setUiLabel("label_5", "Cntrct or", "GREEN", 4, 59, 9);
            uiForm.setUiLabel("label_6", "Participant", "GREEN", 5, 1, 11);
            uiForm.setUiLabel("label_7", "PIN", "GREEN", 5, 40, 3);
            uiForm.setUiLabel("label_8", "Completed", "GREEN", 5, 48, 9);
            uiForm.setUiLabel("label_9", "Policy", "GREEN", 5, 59, 6);
            uiForm.setUiLabel("label_10", "WPID", "GREEN", 5, 71, 4);
            uiForm.setUiLabel("label_11", "Unit", "GREEN", 5, 79, 4);
            uiForm.setUiLabel("label_12", "Error Message", "GREEN", 5, 86, 13);
            uiForm.setUiLabel("label_13", "---------------------------------", "", 6, 1, 33);
            uiForm.setUiLabel("label_14", "------------ ---------- ---------- ------ --------", "GREEN", 6, 35, 50);
            uiForm.setUiLabel("label_15", "----------------------------------------------", "", 6, 86, 46);
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
