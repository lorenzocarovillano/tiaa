/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:40:14 PM
**        *   FROM NATURAL MAP   :  Benm260
************************************************************
**        * FILE NAME               : Benm260.java
**        * CLASS NAME              : Benm260
**        * INSTANCE NAME           : Benm260
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     #COUNTERS.#TOTAL-ACIS-INTERFACE(*)                                                                                       *     #COUNTERS.#TOTAL-ACIS-MIGRATE(*) 
    #COUNTERS.#TOTAL-BENE(*)                                                                *     #COUNTERS.#TOTAL-BENE-SYS(*) #COUNTERS.#TOTAL-CBENE-GT10(*) 
    *     #COUNTERS.#TOTAL-CBENE-1-2(*) #COUNTERS.#TOTAL-CBENE-3-5(*)                                                              *     #COUNTERS.#TOTAL-CBENE-6-10(*) 
    #COUNTERS.#TOTAL-CIS-INTERFACE(*)                                                         *     #COUNTERS.#TOTAL-CIS-MIGRATE(*) #COUNTERS.#TOTAL-CONTRACTS(*) 
    *     #COUNTERS.#TOTAL-DFLT-ESTATE(*) #COUNTERS.#TOTAL-I-BENE(*)                                                               *     #COUNTERS.#TOTAL-I-MOS(*) 
    #COUNTERS.#TOTAL-MDO-INTERFACE(*)                                                              *     #COUNTERS.#TOTAL-MDO-MIGRATE(*) #COUNTERS.#TOTAL-MOS(*) 
    *     #COUNTERS.#TOTAL-MS-MIGRATE(*) #COUNTERS.#TOTAL-P-C-BENES(*)                                                             *     #COUNTERS.#TOTAL-PBENE-GT10(*) 
    #COUNTERS.#TOTAL-PBENE-ONLY(*)                                                            *     #COUNTERS.#TOTAL-PBENE-1-2(*) #COUNTERS.#TOTAL-PBENE-3-5(*) 
    *     #COUNTERS.#TOTAL-PBENE-6-10(*) #COUNTERS.#TOTAL-WEB(*) #PROGRAM
************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benm260 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Counters_Pnd_Total_Acis_Interface;
    private DbsField pnd_Counters_Pnd_Total_Acis_Migrate;
    private DbsField pnd_Counters_Pnd_Total_Bene;
    private DbsField pnd_Counters_Pnd_Total_Bene_Sys;
    private DbsField pnd_Counters_Pnd_Total_Cbene_Gt10;
    private DbsField pnd_Counters_Pnd_Total_Cbene_1_2;
    private DbsField pnd_Counters_Pnd_Total_Cbene_3_5;
    private DbsField pnd_Counters_Pnd_Total_Cbene_6_10;
    private DbsField pnd_Counters_Pnd_Total_Cis_Interface;
    private DbsField pnd_Counters_Pnd_Total_Cis_Migrate;
    private DbsField pnd_Counters_Pnd_Total_Contracts;
    private DbsField pnd_Counters_Pnd_Total_Dflt_Estate;
    private DbsField pnd_Counters_Pnd_Total_I_Bene;
    private DbsField pnd_Counters_Pnd_Total_I_Mos;
    private DbsField pnd_Counters_Pnd_Total_Mdo_Interface;
    private DbsField pnd_Counters_Pnd_Total_Mdo_Migrate;
    private DbsField pnd_Counters_Pnd_Total_Mos;
    private DbsField pnd_Counters_Pnd_Total_Ms_Migrate;
    private DbsField pnd_Counters_Pnd_Total_P_C_Benes;
    private DbsField pnd_Counters_Pnd_Total_Pbene_Gt10;
    private DbsField pnd_Counters_Pnd_Total_Pbene_Only;
    private DbsField pnd_Counters_Pnd_Total_Pbene_1_2;
    private DbsField pnd_Counters_Pnd_Total_Pbene_3_5;
    private DbsField pnd_Counters_Pnd_Total_Pbene_6_10;
    private DbsField pnd_Counters_Pnd_Total_Web;
    private DbsField pnd_Program;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Counters_Pnd_Total_Acis_Interface = parameters.newFieldArrayInRecord("pnd_Counters_Pnd_Total_Acis_Interface", "#COUNTERS.#TOTAL-ACIS-INTERFACE", 
            FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 4));
        pnd_Counters_Pnd_Total_Acis_Migrate = parameters.newFieldArrayInRecord("pnd_Counters_Pnd_Total_Acis_Migrate", "#COUNTERS.#TOTAL-ACIS-MIGRATE", 
            FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 4));
        pnd_Counters_Pnd_Total_Bene = parameters.newFieldArrayInRecord("pnd_Counters_Pnd_Total_Bene", "#COUNTERS.#TOTAL-BENE", FieldType.PACKED_DECIMAL, 
            9, new DbsArrayController(1, 4));
        pnd_Counters_Pnd_Total_Bene_Sys = parameters.newFieldArrayInRecord("pnd_Counters_Pnd_Total_Bene_Sys", "#COUNTERS.#TOTAL-BENE-SYS", FieldType.PACKED_DECIMAL, 
            9, new DbsArrayController(1, 4));
        pnd_Counters_Pnd_Total_Cbene_Gt10 = parameters.newFieldArrayInRecord("pnd_Counters_Pnd_Total_Cbene_Gt10", "#COUNTERS.#TOTAL-CBENE-GT10", FieldType.PACKED_DECIMAL, 
            9, new DbsArrayController(1, 4));
        pnd_Counters_Pnd_Total_Cbene_1_2 = parameters.newFieldArrayInRecord("pnd_Counters_Pnd_Total_Cbene_1_2", "#COUNTERS.#TOTAL-CBENE-1-2", FieldType.PACKED_DECIMAL, 
            9, new DbsArrayController(1, 4));
        pnd_Counters_Pnd_Total_Cbene_3_5 = parameters.newFieldArrayInRecord("pnd_Counters_Pnd_Total_Cbene_3_5", "#COUNTERS.#TOTAL-CBENE-3-5", FieldType.PACKED_DECIMAL, 
            9, new DbsArrayController(1, 4));
        pnd_Counters_Pnd_Total_Cbene_6_10 = parameters.newFieldArrayInRecord("pnd_Counters_Pnd_Total_Cbene_6_10", "#COUNTERS.#TOTAL-CBENE-6-10", FieldType.PACKED_DECIMAL, 
            9, new DbsArrayController(1, 4));
        pnd_Counters_Pnd_Total_Cis_Interface = parameters.newFieldArrayInRecord("pnd_Counters_Pnd_Total_Cis_Interface", "#COUNTERS.#TOTAL-CIS-INTERFACE", 
            FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 4));
        pnd_Counters_Pnd_Total_Cis_Migrate = parameters.newFieldArrayInRecord("pnd_Counters_Pnd_Total_Cis_Migrate", "#COUNTERS.#TOTAL-CIS-MIGRATE", FieldType.PACKED_DECIMAL, 
            9, new DbsArrayController(1, 4));
        pnd_Counters_Pnd_Total_Contracts = parameters.newFieldArrayInRecord("pnd_Counters_Pnd_Total_Contracts", "#COUNTERS.#TOTAL-CONTRACTS", FieldType.PACKED_DECIMAL, 
            9, new DbsArrayController(1, 4));
        pnd_Counters_Pnd_Total_Dflt_Estate = parameters.newFieldArrayInRecord("pnd_Counters_Pnd_Total_Dflt_Estate", "#COUNTERS.#TOTAL-DFLT-ESTATE", FieldType.PACKED_DECIMAL, 
            9, new DbsArrayController(1, 4));
        pnd_Counters_Pnd_Total_I_Bene = parameters.newFieldArrayInRecord("pnd_Counters_Pnd_Total_I_Bene", "#COUNTERS.#TOTAL-I-BENE", FieldType.PACKED_DECIMAL, 
            9, new DbsArrayController(1, 4));
        pnd_Counters_Pnd_Total_I_Mos = parameters.newFieldArrayInRecord("pnd_Counters_Pnd_Total_I_Mos", "#COUNTERS.#TOTAL-I-MOS", FieldType.PACKED_DECIMAL, 
            9, new DbsArrayController(1, 4));
        pnd_Counters_Pnd_Total_Mdo_Interface = parameters.newFieldArrayInRecord("pnd_Counters_Pnd_Total_Mdo_Interface", "#COUNTERS.#TOTAL-MDO-INTERFACE", 
            FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 4));
        pnd_Counters_Pnd_Total_Mdo_Migrate = parameters.newFieldArrayInRecord("pnd_Counters_Pnd_Total_Mdo_Migrate", "#COUNTERS.#TOTAL-MDO-MIGRATE", FieldType.PACKED_DECIMAL, 
            9, new DbsArrayController(1, 4));
        pnd_Counters_Pnd_Total_Mos = parameters.newFieldArrayInRecord("pnd_Counters_Pnd_Total_Mos", "#COUNTERS.#TOTAL-MOS", FieldType.PACKED_DECIMAL, 9, 
            new DbsArrayController(1, 4));
        pnd_Counters_Pnd_Total_Ms_Migrate = parameters.newFieldArrayInRecord("pnd_Counters_Pnd_Total_Ms_Migrate", "#COUNTERS.#TOTAL-MS-MIGRATE", FieldType.PACKED_DECIMAL, 
            9, new DbsArrayController(1, 4));
        pnd_Counters_Pnd_Total_P_C_Benes = parameters.newFieldArrayInRecord("pnd_Counters_Pnd_Total_P_C_Benes", "#COUNTERS.#TOTAL-P-C-BENES", FieldType.PACKED_DECIMAL, 
            9, new DbsArrayController(1, 4));
        pnd_Counters_Pnd_Total_Pbene_Gt10 = parameters.newFieldArrayInRecord("pnd_Counters_Pnd_Total_Pbene_Gt10", "#COUNTERS.#TOTAL-PBENE-GT10", FieldType.PACKED_DECIMAL, 
            9, new DbsArrayController(1, 4));
        pnd_Counters_Pnd_Total_Pbene_Only = parameters.newFieldArrayInRecord("pnd_Counters_Pnd_Total_Pbene_Only", "#COUNTERS.#TOTAL-PBENE-ONLY", FieldType.PACKED_DECIMAL, 
            9, new DbsArrayController(1, 4));
        pnd_Counters_Pnd_Total_Pbene_1_2 = parameters.newFieldArrayInRecord("pnd_Counters_Pnd_Total_Pbene_1_2", "#COUNTERS.#TOTAL-PBENE-1-2", FieldType.PACKED_DECIMAL, 
            9, new DbsArrayController(1, 4));
        pnd_Counters_Pnd_Total_Pbene_3_5 = parameters.newFieldArrayInRecord("pnd_Counters_Pnd_Total_Pbene_3_5", "#COUNTERS.#TOTAL-PBENE-3-5", FieldType.PACKED_DECIMAL, 
            9, new DbsArrayController(1, 4));
        pnd_Counters_Pnd_Total_Pbene_6_10 = parameters.newFieldArrayInRecord("pnd_Counters_Pnd_Total_Pbene_6_10", "#COUNTERS.#TOTAL-PBENE-6-10", FieldType.PACKED_DECIMAL, 
            9, new DbsArrayController(1, 4));
        pnd_Counters_Pnd_Total_Web = parameters.newFieldArrayInRecord("pnd_Counters_Pnd_Total_Web", "#COUNTERS.#TOTAL-WEB", FieldType.PACKED_DECIMAL, 9, 
            new DbsArrayController(1, 4));
        pnd_Program = parameters.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        parameters.reset();
    }

    public Benm260() throws Exception
    {
        super("Benm260");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=060 LS=132 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Benm260", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Benm260"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Program", pnd_Program, true, 1, 1, 8, "GREEN", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_1", "BENEFICIARY SYSTEM", "GREEN", 1, 47, 18);
            uiForm.setUiControl("astDATX", Global.getDATX(), true, 1, 92, 10, "GREEN", "MM/DD/YYYY", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_2", "STATISTICAL REPORT OF CURRENT CONTRACTS", "GREEN", 2, 37, 39);
            uiForm.setUiLabel("label_3", "Page 1", "GREEN", 2, 96, 6);
            uiForm.setUiLabel("label_4", "General Data", "GREEN", 5, 1, 12);
            uiForm.setUiLabel("label_5", "Total", "GREEN", 5, 55, 5);
            uiForm.setUiLabel("label_6", "DA", "GREEN", 5, 72, 2);
            uiForm.setUiLabel("label_7", "IA", "GREEN", 5, 86, 2);
            uiForm.setUiLabel("label_8", "INS", "GREEN", 5, 99, 3);
            uiForm.setUiLabel("label_9", "-----------", "GREEN", 6, 49, 11);
            uiForm.setUiLabel("label_10", "-----------", "GREEN", 6, 63, 11);
            uiForm.setUiLabel("label_11", "-----------", "GREEN", 6, 77, 11);
            uiForm.setUiLabel("label_12", "-----------", "GREEN", 6, 91, 11);
            uiForm.setUiLabel("label_13", "# Contracts by Contract Type", "GREEN", 7, 2, 28);
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Contracts_1pls0", pnd_Counters_Pnd_Total_Contracts.getValue(1+0), true, 7, 49, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Contracts_1pls1", pnd_Counters_Pnd_Total_Contracts.getValue(1+1), true, 7, 63, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Contracts_1pls2", pnd_Counters_Pnd_Total_Contracts.getValue(1+2), true, 7, 77, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Contracts_1pls3", pnd_Counters_Pnd_Total_Contracts.getValue(1+3), true, 7, 91, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_14", "....................................................................................................", "", 
                8, 2, 100);
            uiForm.setUiLabel("label_15", "# Contracts with Individual Beneficiaries", "GREEN", 9, 2, 41);
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Bene_1pls0", pnd_Counters_Pnd_Total_Bene.getValue(1+0), true, 9, 49, 11, "", "ZZZ,ZZZ,ZZ9", true, 
                true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Bene_1pls1", pnd_Counters_Pnd_Total_Bene.getValue(1+1), true, 9, 63, 11, "", "ZZZ,ZZZ,ZZ9", true, 
                true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Bene_1pls2", pnd_Counters_Pnd_Total_Bene.getValue(1+2), true, 9, 77, 11, "", "ZZZ,ZZZ,ZZ9", true, 
                true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Bene_1pls3", pnd_Counters_Pnd_Total_Bene.getValue(1+3), true, 9, 91, 11, "", "ZZZ,ZZZ,ZZ9", true, 
                true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_16", "# Contracts with Irrevocable Beneficiaries", "GREEN", 10, 2, 42);
            uiForm.setUiControl("pnd_Counters_Pnd_Total_I_Bene_1pls0", pnd_Counters_Pnd_Total_I_Bene.getValue(1+0), true, 10, 49, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_I_Bene_1pls1", pnd_Counters_Pnd_Total_I_Bene.getValue(1+1), true, 10, 63, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_I_Bene_1pls2", pnd_Counters_Pnd_Total_I_Bene.getValue(1+2), true, 10, 77, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_I_Bene_1pls3", pnd_Counters_Pnd_Total_I_Bene.getValue(1+3), true, 10, 91, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_17", "....................................................................................................", "", 
                11, 2, 100);
            uiForm.setUiLabel("label_18", "# Contracts with MOS", "GREEN", 12, 2, 20);
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Mos_1pls0", pnd_Counters_Pnd_Total_Mos.getValue(1+0), true, 12, 49, 11, "", "ZZZ,ZZZ,ZZ9", true, 
                true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Mos_1pls1", pnd_Counters_Pnd_Total_Mos.getValue(1+1), true, 12, 63, 11, "", "ZZZ,ZZZ,ZZ9", true, 
                true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Mos_1pls2", pnd_Counters_Pnd_Total_Mos.getValue(1+2), true, 12, 77, 11, "", "ZZZ,ZZZ,ZZ9", true, 
                true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Mos_1pls3", pnd_Counters_Pnd_Total_Mos.getValue(1+3), true, 12, 91, 11, "", "ZZZ,ZZZ,ZZ9", true, 
                true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_19", "# Contracts with Irrevocable MOS", "GREEN", 13, 2, 32);
            uiForm.setUiControl("pnd_Counters_Pnd_Total_I_Mos_1pls0", pnd_Counters_Pnd_Total_I_Mos.getValue(1+0), true, 13, 49, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_I_Mos_1pls1", pnd_Counters_Pnd_Total_I_Mos.getValue(1+1), true, 13, 63, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_I_Mos_1pls2", pnd_Counters_Pnd_Total_I_Mos.getValue(1+2), true, 13, 77, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_I_Mos_1pls3", pnd_Counters_Pnd_Total_I_Mos.getValue(1+3), true, 13, 91, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_20", "....................................................................................................", "", 
                14, 2, 100);
            uiForm.setUiLabel("label_21", "Last Designation System/Source", "GREEN", 16, 1, 30);
            uiForm.setUiLabel("label_22", "# Contracts Entered in Bene Maintenance", "GREEN", 18, 2, 39);
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Bene_Sys_1pls0", pnd_Counters_Pnd_Total_Bene_Sys.getValue(1+0), true, 18, 49, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Bene_Sys_1pls1", pnd_Counters_Pnd_Total_Bene_Sys.getValue(1+1), true, 18, 63, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Bene_Sys_1pls2", pnd_Counters_Pnd_Total_Bene_Sys.getValue(1+2), true, 18, 77, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Bene_Sys_1pls3", pnd_Counters_Pnd_Total_Bene_Sys.getValue(1+3), true, 18, 91, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_23", "....................................................................................................", "", 
                19, 2, 100);
            uiForm.setUiLabel("label_24", "# Contracts Interfaced from ACIS", "GREEN", 20, 2, 32);
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Acis_Interface_1pls0", pnd_Counters_Pnd_Total_Acis_Interface.getValue(1+0), true, 20, 49, 11, 
                "", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Acis_Interface_1pls1", pnd_Counters_Pnd_Total_Acis_Interface.getValue(1+1), true, 20, 63, 11, 
                "", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Acis_Interface_1pls2", pnd_Counters_Pnd_Total_Acis_Interface.getValue(1+2), true, 20, 77, 11, 
                "", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Acis_Interface_1pls3", pnd_Counters_Pnd_Total_Acis_Interface.getValue(1+3), true, 20, 91, 11, 
                "", "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_25", "# Contracts Interfaced from CIS", "GREEN", 21, 2, 31);
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Cis_Interface_1pls0", pnd_Counters_Pnd_Total_Cis_Interface.getValue(1+0), true, 21, 49, 11, "", 
                "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Cis_Interface_1pls1", pnd_Counters_Pnd_Total_Cis_Interface.getValue(1+1), true, 21, 63, 11, "", 
                "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Cis_Interface_1pls2", pnd_Counters_Pnd_Total_Cis_Interface.getValue(1+2), true, 21, 77, 11, "", 
                "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Cis_Interface_1pls3", pnd_Counters_Pnd_Total_Cis_Interface.getValue(1+3), true, 21, 91, 11, "", 
                "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_26", "# Contracts Interfaced from MDO", "GREEN", 22, 2, 31);
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Mdo_Interface_1pls0", pnd_Counters_Pnd_Total_Mdo_Interface.getValue(1+0), true, 22, 49, 11, "", 
                "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Mdo_Interface_1pls1", pnd_Counters_Pnd_Total_Mdo_Interface.getValue(1+1), true, 22, 63, 11, "", 
                "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Mdo_Interface_1pls2", pnd_Counters_Pnd_Total_Mdo_Interface.getValue(1+2), true, 22, 77, 11, "", 
                "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Mdo_Interface_1pls3", pnd_Counters_Pnd_Total_Mdo_Interface.getValue(1+3), true, 22, 91, 11, "", 
                "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_27", "....................................................................................................", "", 
                23, 2, 100);
            uiForm.setUiLabel("label_28", "# Contracts Migrated from ACIS", "GREEN", 24, 2, 30);
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Acis_Migrate_1pls0", pnd_Counters_Pnd_Total_Acis_Migrate.getValue(1+0), true, 24, 49, 11, "", 
                "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Acis_Migrate_1pls1", pnd_Counters_Pnd_Total_Acis_Migrate.getValue(1+1), true, 24, 63, 11, "", 
                "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Acis_Migrate_1pls2", pnd_Counters_Pnd_Total_Acis_Migrate.getValue(1+2), true, 24, 77, 11, "", 
                "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Acis_Migrate_1pls3", pnd_Counters_Pnd_Total_Acis_Migrate.getValue(1+3), true, 24, 91, 11, "", 
                "ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_29", "# Contracts Migrated from CIS", "GREEN", 25, 2, 29);
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Cis_Migrate_1pls0", pnd_Counters_Pnd_Total_Cis_Migrate.getValue(1+0), true, 25, 49, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Cis_Migrate_1pls1", pnd_Counters_Pnd_Total_Cis_Migrate.getValue(1+1), true, 25, 63, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Cis_Migrate_1pls2", pnd_Counters_Pnd_Total_Cis_Migrate.getValue(1+2), true, 25, 77, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Cis_Migrate_1pls3", pnd_Counters_Pnd_Total_Cis_Migrate.getValue(1+3), true, 25, 91, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_30", "# Contracts Migrated from MDO", "GREEN", 26, 2, 29);
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Mdo_Migrate_1pls0", pnd_Counters_Pnd_Total_Mdo_Migrate.getValue(1+0), true, 26, 49, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Mdo_Migrate_1pls1", pnd_Counters_Pnd_Total_Mdo_Migrate.getValue(1+1), true, 26, 63, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Mdo_Migrate_1pls2", pnd_Counters_Pnd_Total_Mdo_Migrate.getValue(1+2), true, 26, 77, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Mdo_Migrate_1pls3", pnd_Counters_Pnd_Total_Mdo_Migrate.getValue(1+3), true, 26, 91, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_31", "# Contracts Migrated from MS", "GREEN", 27, 2, 28);
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Ms_Migrate_1pls0", pnd_Counters_Pnd_Total_Ms_Migrate.getValue(1+0), true, 27, 49, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Ms_Migrate_1pls1", pnd_Counters_Pnd_Total_Ms_Migrate.getValue(1+1), true, 27, 63, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Ms_Migrate_1pls2", pnd_Counters_Pnd_Total_Ms_Migrate.getValue(1+2), true, 27, 77, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Ms_Migrate_1pls3", pnd_Counters_Pnd_Total_Ms_Migrate.getValue(1+3), true, 27, 91, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_32", "....................................................................................................", "", 
                28, 2, 100);
            uiForm.setUiLabel("label_33", "# Contracts Entered", "GREEN", 29, 2, 19);
            uiForm.setUiLabel("label_34", "from WEB", "GREEN", 29, 23, 8);
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Web_1pls0", pnd_Counters_Pnd_Total_Web.getValue(1+0), true, 29, 49, 11, "", "ZZZ,ZZZ,ZZ9", true, 
                true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Web_1pls1", pnd_Counters_Pnd_Total_Web.getValue(1+1), true, 29, 63, 11, "", "ZZZ,ZZZ,ZZ9", true, 
                true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Web_1pls2", pnd_Counters_Pnd_Total_Web.getValue(1+2), true, 29, 77, 11, "", "ZZZ,ZZZ,ZZ9", true, 
                true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Web_1pls3", pnd_Counters_Pnd_Total_Web.getValue(1+3), true, 29, 91, 11, "", "ZZZ,ZZZ,ZZ9", true, 
                true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_35", "....................................................................................................", "", 
                30, 2, 100);
            uiForm.setUiLabel("label_36", "Beneficiary Data", "GREEN", 32, 1, 16);
            uiForm.setUiLabel("label_37", "# Contracts that are Default to Estate", "GREEN", 34, 2, 38);
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Dflt_Estate_1pls0", pnd_Counters_Pnd_Total_Dflt_Estate.getValue(1+0), true, 34, 49, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Dflt_Estate_1pls1", pnd_Counters_Pnd_Total_Dflt_Estate.getValue(1+1), true, 34, 63, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Dflt_Estate_1pls2", pnd_Counters_Pnd_Total_Dflt_Estate.getValue(1+2), true, 34, 77, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Dflt_Estate_1pls3", pnd_Counters_Pnd_Total_Dflt_Estate.getValue(1+3), true, 34, 91, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_38", "# Contracts that have Primary Benes Only", "GREEN", 35, 2, 40);
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Pbene_Only_1pls0", pnd_Counters_Pnd_Total_Pbene_Only.getValue(1+0), true, 35, 49, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Pbene_Only_1pls1", pnd_Counters_Pnd_Total_Pbene_Only.getValue(1+1), true, 35, 63, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Pbene_Only_1pls2", pnd_Counters_Pnd_Total_Pbene_Only.getValue(1+2), true, 35, 77, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Pbene_Only_1pls3", pnd_Counters_Pnd_Total_Pbene_Only.getValue(1+3), true, 35, 91, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_39", "# Contracts that have Primary and Contingent", "GREEN", 36, 2, 44);
            uiForm.setUiControl("pnd_Counters_Pnd_Total_P_C_Benes_1pls0", pnd_Counters_Pnd_Total_P_C_Benes.getValue(1+0), true, 36, 49, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_P_C_Benes_1pls1", pnd_Counters_Pnd_Total_P_C_Benes.getValue(1+1), true, 36, 63, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_P_C_Benes_1pls2", pnd_Counters_Pnd_Total_P_C_Benes.getValue(1+2), true, 36, 77, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_P_C_Benes_1pls3", pnd_Counters_Pnd_Total_P_C_Benes.getValue(1+3), true, 36, 91, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_40", "....................................................................................................", "", 
                37, 2, 100);
            uiForm.setUiLabel("label_41", "Primary Beneficiary Data", "GREEN", 39, 1, 24);
            uiForm.setUiLabel("label_42", "# Contracts with 1 to 2 Benes", "GREEN", 41, 2, 29);
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Pbene_1_2_1pls0", pnd_Counters_Pnd_Total_Pbene_1_2.getValue(1+0), true, 41, 49, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Pbene_1_2_1pls1", pnd_Counters_Pnd_Total_Pbene_1_2.getValue(1+1), true, 41, 63, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Pbene_1_2_1pls2", pnd_Counters_Pnd_Total_Pbene_1_2.getValue(1+2), true, 41, 77, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Pbene_1_2_1pls3", pnd_Counters_Pnd_Total_Pbene_1_2.getValue(1+3), true, 41, 91, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_43", "# Contracts with 3 to 5 Benes", "GREEN", 42, 2, 29);
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Pbene_3_5_1pls0", pnd_Counters_Pnd_Total_Pbene_3_5.getValue(1+0), true, 42, 49, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Pbene_3_5_1pls1", pnd_Counters_Pnd_Total_Pbene_3_5.getValue(1+1), true, 42, 63, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Pbene_3_5_1pls2", pnd_Counters_Pnd_Total_Pbene_3_5.getValue(1+2), true, 42, 77, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Pbene_3_5_1pls3", pnd_Counters_Pnd_Total_Pbene_3_5.getValue(1+3), true, 42, 91, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_44", "# Contracts with 6 to 10 Benes", "GREEN", 43, 2, 30);
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Pbene_6_10_1pls0", pnd_Counters_Pnd_Total_Pbene_6_10.getValue(1+0), true, 43, 49, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Pbene_6_10_1pls1", pnd_Counters_Pnd_Total_Pbene_6_10.getValue(1+1), true, 43, 63, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Pbene_6_10_1pls2", pnd_Counters_Pnd_Total_Pbene_6_10.getValue(1+2), true, 43, 77, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Pbene_6_10_1pls3", pnd_Counters_Pnd_Total_Pbene_6_10.getValue(1+3), true, 43, 91, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_45", "# Contracts with more than 10 Benes", "GREEN", 44, 2, 35);
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Pbene_Gt10_1pls0", pnd_Counters_Pnd_Total_Pbene_Gt10.getValue(1+0), true, 44, 49, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Pbene_Gt10_1pls1", pnd_Counters_Pnd_Total_Pbene_Gt10.getValue(1+1), true, 44, 63, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Pbene_Gt10_1pls2", pnd_Counters_Pnd_Total_Pbene_Gt10.getValue(1+2), true, 44, 77, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Pbene_Gt10_1pls3", pnd_Counters_Pnd_Total_Pbene_Gt10.getValue(1+3), true, 44, 91, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_46", "....................................................................................................", "", 
                45, 2, 100);
            uiForm.setUiLabel("label_47", "Contingent Beneficiary Data", "GREEN", 47, 1, 27);
            uiForm.setUiLabel("label_48", "# Contracts with 1 to 2 Benes", "GREEN", 49, 2, 29);
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Cbene_1_2_1pls0", pnd_Counters_Pnd_Total_Cbene_1_2.getValue(1+0), true, 49, 49, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Cbene_1_2_1pls1", pnd_Counters_Pnd_Total_Cbene_1_2.getValue(1+1), true, 49, 63, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Cbene_1_2_1pls2", pnd_Counters_Pnd_Total_Cbene_1_2.getValue(1+2), true, 49, 77, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Cbene_1_2_1pls3", pnd_Counters_Pnd_Total_Cbene_1_2.getValue(1+3), true, 49, 91, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_49", "# Contracts with 3 to 5 Benes", "GREEN", 50, 2, 29);
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Cbene_3_5_1pls0", pnd_Counters_Pnd_Total_Cbene_3_5.getValue(1+0), true, 50, 49, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Cbene_3_5_1pls1", pnd_Counters_Pnd_Total_Cbene_3_5.getValue(1+1), true, 50, 63, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Cbene_3_5_1pls2", pnd_Counters_Pnd_Total_Cbene_3_5.getValue(1+2), true, 50, 77, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Cbene_3_5_1pls3", pnd_Counters_Pnd_Total_Cbene_3_5.getValue(1+3), true, 50, 91, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_50", "# Contracts with 6 to 10 Benes", "GREEN", 51, 2, 30);
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Cbene_6_10_1pls0", pnd_Counters_Pnd_Total_Cbene_6_10.getValue(1+0), true, 51, 49, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Cbene_6_10_1pls1", pnd_Counters_Pnd_Total_Cbene_6_10.getValue(1+1), true, 51, 63, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Cbene_6_10_1pls2", pnd_Counters_Pnd_Total_Cbene_6_10.getValue(1+2), true, 51, 77, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Cbene_6_10_1pls3", pnd_Counters_Pnd_Total_Cbene_6_10.getValue(1+3), true, 51, 91, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_51", "# Contracts with more than 10 Benes", "GREEN", 52, 2, 35);
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Cbene_Gt10_1pls0", pnd_Counters_Pnd_Total_Cbene_Gt10.getValue(1+0), true, 52, 49, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Cbene_Gt10_1pls1", pnd_Counters_Pnd_Total_Cbene_Gt10.getValue(1+1), true, 52, 63, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Cbene_Gt10_1pls2", pnd_Counters_Pnd_Total_Cbene_Gt10.getValue(1+2), true, 52, 77, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Counters_Pnd_Total_Cbene_Gt10_1pls3", pnd_Counters_Pnd_Total_Cbene_Gt10.getValue(1+3), true, 52, 91, 11, "", "ZZZ,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_52", "....................................................................................................", "", 
                53, 2, 100);
            uiForm.setUiLabel("label_53", "***", "GREEN", 56, 38, 3);
            uiForm.setUiLabel("label_54", "E N D", "GREEN", 56, 43, 5);
            uiForm.setUiLabel("label_55", "O F", "GREEN", 56, 51, 3);
            uiForm.setUiLabel("label_56", "R E P O R T", "GREEN", 56, 57, 11);
            uiForm.setUiLabel("label_57", "***", "GREEN", 56, 70, 3);
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
