/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:40:27 PM
**        *   FROM NATURAL MAP   :  Benm9910
************************************************************
**        * FILE NAME               : Benm9910.java
**        * CLASS NAME              : Benm9910
**        * INSTANCE NAME           : Benm9910
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #HEADER #INTERFACE-DATE-W #PROGRAM PASS.##HEADER2
************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benm9910 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Header;
    private DbsField pnd_Interface_Date_W;
    private DbsField pnd_Program;
    private DbsField pass_Pnd_Pnd_Header2;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Header = parameters.newFieldInRecord("pnd_Header", "#HEADER", FieldType.STRING, 40);
        pnd_Interface_Date_W = parameters.newFieldInRecord("pnd_Interface_Date_W", "#INTERFACE-DATE-W", FieldType.DATE);
        pnd_Program = parameters.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 10);
        pass_Pnd_Pnd_Header2 = parameters.newFieldInRecord("pass_Pnd_Pnd_Header2", "PASS.##HEADER2", FieldType.STRING, 58);
        parameters.reset();
    }

    public Benm9910() throws Exception
    {
        super("Benm9910");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=004 LS=133 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Benm9910", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Benm9910"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Program", pnd_Program, true, 1, 1, 10, "GREEN", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_1", "BENEFICIARY INTERFACE SYSTEM", "GREEN", 1, 52, 28);
            uiForm.setUiControl("astDATX", Global.getDATX(), true, 1, 122, 10, "GREEN", "MM/DD/YYYY", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_2", "Page", "GREEN", 2, 1, 4);
            uiForm.setUiControl("astPAGE_NUMBER", getReports().getPageNumberDbs(0), true, 2, 6, 5, "GREEN", true, false, null, null, "AD=DROFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Header", pnd_Header, true, 2, 50, 40, "GREEN", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("astTIMX", Global.getTIMX(), true, 2, 124, 8, "GREEN", "HH:II:SS", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_3", "Business Date:", "GREEN", 3, 53, 14);
            uiForm.setUiControl("pnd_Interface_Date_W", pnd_Interface_Date_W, true, 3, 70, 10, "GREEN", "MM'/'DD'/'YYYY", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pass_Pnd_Pnd_Header2", pass_Pnd_Pnd_Header2, true, 4, 37, 58, "GREEN", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
