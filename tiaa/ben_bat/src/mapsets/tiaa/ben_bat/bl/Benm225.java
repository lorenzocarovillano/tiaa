/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:40:13 PM
**        *   FROM NATURAL MAP   :  Benm225
************************************************************
**        * FILE NAME               : Benm225.java
**        * CLASS NAME              : Benm225
**        * INSTANCE NAME           : Benm225
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     ##PROGRAM #HEADER #WPID-UNIT-CURR
************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benm225 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Pnd_Program;
    private DbsField pnd_Header;
    private DbsField pnd_Wpid_Unit_Curr;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Pnd_Program = parameters.newFieldInRecord("pnd_Pnd_Program", "##PROGRAM", FieldType.STRING, 8);
        pnd_Header = parameters.newFieldInRecord("pnd_Header", "#HEADER", FieldType.STRING, 75);
        pnd_Wpid_Unit_Curr = parameters.newFieldInRecord("pnd_Wpid_Unit_Curr", "#WPID-UNIT-CURR", FieldType.STRING, 8);
        parameters.reset();
    }

    public Benm225() throws Exception
    {
        super("Benm225");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=003 LS=133 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Benm225", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Benm225"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Pnd_Program", pnd_Pnd_Program, true, 1, 1, 8, "GREEN", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_1", "BENEFICIARY SYSTEM", "GREEN", 1, 56, 18);
            uiForm.setUiControl("astDATX", Global.getDATX(), true, 1, 122, 10, "GREEN", "MM/DD/YYYY", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Wpid_Unit_Curr", pnd_Wpid_Unit_Curr, true, 2, 1, 8, "GREEN", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Header", pnd_Header, true, 2, 29, 75, "GREEN", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_2", "Page", "GREEN", 2, 122, 4);
            uiForm.setUiControl("astPAGE_NUMBER", getReports().getPageNumberDbs(0), true, 2, 127, 5, "GREEN", true, false, null, null, "AD=DROFHW' '~TG=", 
                ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
