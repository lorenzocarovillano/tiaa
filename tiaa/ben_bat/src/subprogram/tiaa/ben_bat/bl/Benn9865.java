/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:06:41 PM
**        * FROM NATURAL SUBPROGRAM : Benn9865
************************************************************
**        * FILE NAME            : Benn9865.java
**        * CLASS NAME           : Benn9865
**        * INSTANCE NAME        : Benn9865
************************************************************
************************************************************************
* PROGRAM  : BENN9865
* SYSTEM   : BENEFICIARY-SYSTEM /* INTERFACE SAME AS
* TITLE    : READ COR -  CLONED FROM BENN9850 - FONTOUR
* GENERATED: JUNE 29, 1999
************************************************************************
*    DATE      USERID                   DESCRIPTION
* MM/DD/YYYY  XXXXXXXX  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
* 04/01/2017  DURAND    PIN EXPANSION N7 TO N12
* 2015/09/11  DURAND    CONVERT COR/NAS FILES TO PROCOBOL CALLS
* 2007/12/06  DURAND    COR.CNTRCT-STATUS-CDE = ' ','Y','H' = ACTIVE
* 2000/08/30  J.WEBB    UPDATED LOGIC FOR PIN NOT FOUND
* 2000/01/10  T.DEPAUL  ADDED LOGIC FOR PIN NOT FOUND
************************************************************************

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benn9865 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaBena9865 pdaBena9865;
    private PdaBenpda_M pdaBenpda_M;
    private PdaBenpda_E pdaBenpda_E;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Psgm003_Common_Area;

    private DbsGroup pnd_Psgm003_Common_Area_Pnd_Psgm003_Input;
    private DbsField pnd_Psgm003_Common_Area_Pnd_In_Function;

    private DbsGroup pnd_Psgm003_Common_Area_Pnd_In_Data;
    private DbsField pnd_Psgm003_Common_Area_Pnd_In_Contract_Num_Typ;
    private DbsField pnd_Psgm003_Common_Area_Pnd_In_Type;
    private DbsField pnd_Psgm003_Common_Area_Pnd_In_Ssnpin;
    private DbsField pnd_Psgm003_Common_Area_Pnd_In_Contract;
    private DbsField pnd_Psgm003_Common_Area_Pnd_In_Payee_Cd;

    private DbsGroup pnd_Psgm003_Common_Area_Pnd_Rt_Area;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Ret_Code;

    private DbsGroup pnd_Psgm003_Common_Area_Pnd_Rt_Record;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Input_String;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Ssn;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Pin;

    private DbsGroup pnd_Psgm003_Common_Area__R_Field_1;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Pin_Num;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Tiaa_Cntrct_Nmbr;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Xpayee_Cde;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Status_Code;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Ctrct_Status_Dt;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Issue_Dt;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Lob;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cref_Contract_Nbr;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cref_Issued_Ind;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Ownership_Code;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Product_Code;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Option_Code;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Plan_Code;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Fund_Code;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Social_Code;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Custodial_Agreement;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Aas_Ind;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Platform_Ind;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Multi_Plan_Ind;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Tiaa_Mailed_Ind;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cref_Mailed_Ind;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Dflt_Enrollment_Ind;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Lst_Updt_Dt;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Lst_Updt_Time;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Lst_Updt_Usrid;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Co_Per_Addr_Ind;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Type_Cd;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_One;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_Two;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_Three;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_Four;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_Five;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_City;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_State;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Co_Zip;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Co_Country;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Postal_Data;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Geo_Cd;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Status;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Status_Dt;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Lst_Ch_Src_Cd;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Lst_Ch_Dt;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Lst_Ch_Time;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Lst_Ch_Usrid;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Per_Addr_Ind;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Type_Cd;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_One;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_Two;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_Three;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_Four;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_Five;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_City;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_State;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Zip;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Country;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Ch_Saving_Cde;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Ph_Bnk_Pt_Acct_Nbr;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Bnk_Aba_Eft_Nbr;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Eft_Pay_Type_Cde;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Postal_Data;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Geo_Cd;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Status;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Status_Dt;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Lst_Ch_Src_Cd;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Lst_Ch_Dt;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Lst_Ch_Time;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Lst_Ch_Usrid;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Prefix;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Last_Name;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_First_Name;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Given_Name_Two;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Suffix_Desc;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Record_Status;
    private DbsField pls_Trace;
    private int psgm003ReturnCode;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaBena9865 = new PdaBena9865(parameters);
        pdaBenpda_M = new PdaBenpda_M(parameters);
        pdaBenpda_E = new PdaBenpda_E(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Psgm003_Common_Area = localVariables.newGroupInRecord("pnd_Psgm003_Common_Area", "#PSGM003-COMMON-AREA");

        pnd_Psgm003_Common_Area_Pnd_Psgm003_Input = pnd_Psgm003_Common_Area.newGroupInGroup("pnd_Psgm003_Common_Area_Pnd_Psgm003_Input", "#PSGM003-INPUT");
        pnd_Psgm003_Common_Area_Pnd_In_Function = pnd_Psgm003_Common_Area_Pnd_Psgm003_Input.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_In_Function", 
            "#IN-FUNCTION", FieldType.STRING, 2);

        pnd_Psgm003_Common_Area_Pnd_In_Data = pnd_Psgm003_Common_Area_Pnd_Psgm003_Input.newGroupInGroup("pnd_Psgm003_Common_Area_Pnd_In_Data", "#IN-DATA");
        pnd_Psgm003_Common_Area_Pnd_In_Contract_Num_Typ = pnd_Psgm003_Common_Area_Pnd_In_Data.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_In_Contract_Num_Typ", 
            "#IN-CONTRACT-NUM-TYP", FieldType.STRING, 1);
        pnd_Psgm003_Common_Area_Pnd_In_Type = pnd_Psgm003_Common_Area_Pnd_In_Data.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_In_Type", "#IN-TYPE", FieldType.STRING, 
            3);
        pnd_Psgm003_Common_Area_Pnd_In_Ssnpin = pnd_Psgm003_Common_Area_Pnd_In_Data.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_In_Ssnpin", "#IN-SSNPIN", 
            FieldType.STRING, 12);
        pnd_Psgm003_Common_Area_Pnd_In_Contract = pnd_Psgm003_Common_Area_Pnd_In_Data.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_In_Contract", "#IN-CONTRACT", 
            FieldType.STRING, 10);
        pnd_Psgm003_Common_Area_Pnd_In_Payee_Cd = pnd_Psgm003_Common_Area_Pnd_In_Data.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_In_Payee_Cd", "#IN-PAYEE-CD", 
            FieldType.STRING, 2);

        pnd_Psgm003_Common_Area_Pnd_Rt_Area = pnd_Psgm003_Common_Area.newGroupInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Area", "#RT-AREA");
        pnd_Psgm003_Common_Area_Pnd_Rt_Ret_Code = pnd_Psgm003_Common_Area_Pnd_Rt_Area.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Ret_Code", "#RT-RET-CODE", 
            FieldType.STRING, 2);

        pnd_Psgm003_Common_Area_Pnd_Rt_Record = pnd_Psgm003_Common_Area_Pnd_Rt_Area.newGroupInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Record", "#RT-RECORD");
        pnd_Psgm003_Common_Area_Pnd_Rt_Input_String = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Input_String", 
            "#RT-INPUT-STRING", FieldType.STRING, 100);
        pnd_Psgm003_Common_Area_Pnd_Rt_Ssn = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Ssn", "#RT-SSN", FieldType.STRING, 
            50);
        pnd_Psgm003_Common_Area_Pnd_Rt_Pin = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Pin", "#RT-PIN", FieldType.STRING, 
            50);

        pnd_Psgm003_Common_Area__R_Field_1 = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newGroupInGroup("pnd_Psgm003_Common_Area__R_Field_1", "REDEFINE", pnd_Psgm003_Common_Area_Pnd_Rt_Pin);
        pnd_Psgm003_Common_Area_Pnd_Rt_Pin_Num = pnd_Psgm003_Common_Area__R_Field_1.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Pin_Num", "#RT-PIN-NUM", 
            FieldType.NUMERIC, 12);
        pnd_Psgm003_Common_Area_Pnd_Rt_Tiaa_Cntrct_Nmbr = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Tiaa_Cntrct_Nmbr", 
            "#RT-TIAA-CNTRCT-NMBR", FieldType.STRING, 150);
        pnd_Psgm003_Common_Area_Pnd_Rt_Xpayee_Cde = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Xpayee_Cde", 
            "#RT-XPAYEE-CDE", FieldType.STRING, 2);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Status_Code = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Status_Code", 
            "#RT-CNTRCT-STATUS-CODE", FieldType.STRING, 120);
        pnd_Psgm003_Common_Area_Pnd_Rt_Ctrct_Status_Dt = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Ctrct_Status_Dt", 
            "#RT-CTRCT-STATUS-DT", FieldType.STRING, 8);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Issue_Dt = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Issue_Dt", 
            "#RT-CNTRCT-ISSUE-DT", FieldType.STRING, 8);
        pnd_Psgm003_Common_Area_Pnd_Rt_Lob = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Lob", "#RT-LOB", FieldType.STRING, 
            120);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cref_Contract_Nbr = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cref_Contract_Nbr", 
            "#RT-CREF-CONTRACT-NBR", FieldType.STRING, 150);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cref_Issued_Ind = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cref_Issued_Ind", 
            "#RT-CREF-ISSUED-IND", FieldType.STRING, 1);
        pnd_Psgm003_Common_Area_Pnd_Rt_Ownership_Code = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Ownership_Code", 
            "#RT-OWNERSHIP-CODE", FieldType.STRING, 120);
        pnd_Psgm003_Common_Area_Pnd_Rt_Product_Code = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Product_Code", 
            "#RT-PRODUCT-CODE", FieldType.STRING, 1);
        pnd_Psgm003_Common_Area_Pnd_Rt_Option_Code = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Option_Code", 
            "#RT-OPTION-CODE", FieldType.STRING, 2);
        pnd_Psgm003_Common_Area_Pnd_Rt_Plan_Code = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Plan_Code", "#RT-PLAN-CODE", 
            FieldType.STRING, 5);
        pnd_Psgm003_Common_Area_Pnd_Rt_Fund_Code = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Fund_Code", "#RT-FUND-CODE", 
            FieldType.STRING, 120);
        pnd_Psgm003_Common_Area_Pnd_Rt_Social_Code = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Social_Code", 
            "#RT-SOCIAL-CODE", FieldType.STRING, 120);
        pnd_Psgm003_Common_Area_Pnd_Rt_Custodial_Agreement = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Custodial_Agreement", 
            "#RT-CUSTODIAL-AGREEMENT", FieldType.STRING, 1);
        pnd_Psgm003_Common_Area_Pnd_Rt_Aas_Ind = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Aas_Ind", "#RT-AAS-IND", 
            FieldType.STRING, 1);
        pnd_Psgm003_Common_Area_Pnd_Rt_Platform_Ind = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Platform_Ind", 
            "#RT-PLATFORM-IND", FieldType.STRING, 120);
        pnd_Psgm003_Common_Area_Pnd_Rt_Multi_Plan_Ind = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Multi_Plan_Ind", 
            "#RT-MULTI-PLAN-IND", FieldType.STRING, 1);
        pnd_Psgm003_Common_Area_Pnd_Rt_Tiaa_Mailed_Ind = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Tiaa_Mailed_Ind", 
            "#RT-TIAA-MAILED-IND", FieldType.STRING, 1);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cref_Mailed_Ind = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cref_Mailed_Ind", 
            "#RT-CREF-MAILED-IND", FieldType.STRING, 1);
        pnd_Psgm003_Common_Area_Pnd_Rt_Dflt_Enrollment_Ind = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Dflt_Enrollment_Ind", 
            "#RT-DFLT-ENROLLMENT-IND", FieldType.STRING, 1);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Lst_Updt_Dt = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Lst_Updt_Dt", 
            "#RT-CNTRCT-LST-UPDT-DT", FieldType.STRING, 8);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Lst_Updt_Time = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Lst_Updt_Time", 
            "#RT-CNTRCT-LST-UPDT-TIME", FieldType.STRING, 7);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Lst_Updt_Usrid = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Lst_Updt_Usrid", 
            "#RT-CNTRCT-LST-UPDT-USRID", FieldType.STRING, 20);
        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Per_Addr_Ind = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Co_Per_Addr_Ind", 
            "#RT-CO-PER-ADDR-IND", FieldType.STRING, 1);
        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Type_Cd = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Type_Cd", 
            "#RT-CO-ADDR-TYPE-CD", FieldType.STRING, 1);
        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_One = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_One", 
            "#RT-CO-ADDR-LINE-ONE", FieldType.STRING, 100);
        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_Two = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_Two", 
            "#RT-CO-ADDR-LINE-TWO", FieldType.STRING, 100);
        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_Three = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_Three", 
            "#RT-CO-ADDR-LINE-THREE", FieldType.STRING, 100);
        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_Four = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_Four", 
            "#RT-CO-ADDR-LINE-FOUR", FieldType.STRING, 250);
        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_Five = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_Five", 
            "#RT-CO-ADDR-LINE-FIVE", FieldType.STRING, 250);
        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_City = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_City", 
            "#RT-CO-ADDR-CITY", FieldType.STRING, 35);
        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_State = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_State", 
            "#RT-CO-ADDR-STATE", FieldType.STRING, 2);
        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Zip = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Co_Zip", "#RT-CO-ZIP", 
            FieldType.STRING, 25);
        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Country = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Co_Country", 
            "#RT-CO-COUNTRY", FieldType.STRING, 120);
        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Postal_Data = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Postal_Data", 
            "#RT-CO-ADDR-POSTAL-DATA", FieldType.STRING, 32);
        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Geo_Cd = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Geo_Cd", 
            "#RT-CO-ADDR-GEO-CD", FieldType.STRING, 2);
        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Status = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Status", 
            "#RT-CO-ADDR-STATUS", FieldType.STRING, 1);
        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Status_Dt = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Status_Dt", 
            "#RT-CO-ADDR-STATUS-DT", FieldType.STRING, 8);
        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Lst_Ch_Src_Cd = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Lst_Ch_Src_Cd", 
            "#RT-CO-ADDR-LST-CH-SRC-CD", FieldType.STRING, 3);
        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Lst_Ch_Dt = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Lst_Ch_Dt", 
            "#RT-CO-ADDR-LST-CH-DT", FieldType.STRING, 8);
        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Lst_Ch_Time = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Lst_Ch_Time", 
            "#RT-CO-ADDR-LST-CH-TIME", FieldType.STRING, 7);
        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Lst_Ch_Usrid = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Lst_Ch_Usrid", 
            "#RT-CO-ADDR-LST-CH-USRID", FieldType.STRING, 8);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Per_Addr_Ind = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Per_Addr_Ind", 
            "#RT-CM-PER-ADDR-IND", FieldType.STRING, 1);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Type_Cd = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Type_Cd", 
            "#RT-CM-ADDR-TYPE-CD", FieldType.STRING, 1);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_One = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_One", 
            "#RT-CM-ADDR-LINE-ONE", FieldType.STRING, 100);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_Two = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_Two", 
            "#RT-CM-ADDR-LINE-TWO", FieldType.STRING, 100);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_Three = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_Three", 
            "#RT-CM-ADDR-LINE-THREE", FieldType.STRING, 100);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_Four = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_Four", 
            "#RT-CM-ADDR-LINE-FOUR", FieldType.STRING, 250);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_Five = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_Five", 
            "#RT-CM-ADDR-LINE-FIVE", FieldType.STRING, 250);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_City = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_City", 
            "#RT-CM-ADDR-CITY", FieldType.STRING, 35);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_State = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_State", 
            "#RT-CM-ADDR-STATE", FieldType.STRING, 2);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Zip = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Zip", "#RT-CM-ZIP", 
            FieldType.STRING, 25);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Country = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Country", 
            "#RT-CM-COUNTRY", FieldType.STRING, 120);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Ch_Saving_Cde = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Ch_Saving_Cde", 
            "#RT-CM-CH-SAVING-CDE", FieldType.STRING, 1);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Ph_Bnk_Pt_Acct_Nbr = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Ph_Bnk_Pt_Acct_Nbr", 
            "#RT-CM-PH-BNK-PT-ACCT-NBR", FieldType.STRING, 35);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Bnk_Aba_Eft_Nbr = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Bnk_Aba_Eft_Nbr", 
            "#RT-CM-BNK-ABA-EFT-NBR", FieldType.STRING, 35);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Eft_Pay_Type_Cde = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Eft_Pay_Type_Cde", 
            "#RT-CM-EFT-PAY-TYPE-CDE", FieldType.STRING, 2);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Postal_Data = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Postal_Data", 
            "#RT-CM-ADDR-POSTAL-DATA", FieldType.STRING, 32);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Geo_Cd = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Geo_Cd", 
            "#RT-CM-ADDR-GEO-CD", FieldType.STRING, 2);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Status = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Status", 
            "#RT-CM-ADDR-STATUS", FieldType.STRING, 1);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Status_Dt = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Status_Dt", 
            "#RT-CM-ADDR-STATUS-DT", FieldType.STRING, 8);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Lst_Ch_Src_Cd = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Lst_Ch_Src_Cd", 
            "#RT-CM-ADDR-LST-CH-SRC-CD", FieldType.STRING, 3);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Lst_Ch_Dt = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Lst_Ch_Dt", 
            "#RT-CM-ADDR-LST-CH-DT", FieldType.STRING, 8);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Lst_Ch_Time = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Lst_Ch_Time", 
            "#RT-CM-ADDR-LST-CH-TIME", FieldType.STRING, 7);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Lst_Ch_Usrid = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Lst_Ch_Usrid", 
            "#RT-CM-ADDR-LST-CH-USRID", FieldType.STRING, 8);
        pnd_Psgm003_Common_Area_Pnd_Rt_Prefix = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Prefix", "#RT-PREFIX", 
            FieldType.STRING, 120);
        pnd_Psgm003_Common_Area_Pnd_Rt_Last_Name = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Last_Name", "#RT-LAST-NAME", 
            FieldType.STRING, 35);
        pnd_Psgm003_Common_Area_Pnd_Rt_First_Name = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_First_Name", 
            "#RT-FIRST-NAME", FieldType.STRING, 30);
        pnd_Psgm003_Common_Area_Pnd_Rt_Given_Name_Two = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Given_Name_Two", 
            "#RT-GIVEN-NAME-TWO", FieldType.STRING, 30);
        pnd_Psgm003_Common_Area_Pnd_Rt_Suffix_Desc = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Suffix_Desc", 
            "#RT-SUFFIX-DESC", FieldType.STRING, 20);
        pnd_Psgm003_Common_Area_Pnd_Rt_Record_Status = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Record_Status", 
            "#RT-RECORD-STATUS", FieldType.STRING, 1);
        pls_Trace = WsIndependent.getInstance().newFieldInRecord("pls_Trace", "+TRACE", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Benn9865() throws Exception
    {
        super("Benn9865");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ERROR-INFO-SUB.##LAST-PROGRAM := *PROGRAM
        pdaBenpda_M.getMsg_Info_Sub().reset();                                                                                                                            //Natural: RESET MSG-INFO-SUB #CNTRCT-ISSUE-DTE #CNTRCT-FOUND #PIN-FOUND #PIN #CNTRCT-IS-ACTIVE
        pdaBena9865.getBena9865_Pnd_Cntrct_Issue_Dte().reset();
        pdaBena9865.getBena9865_Pnd_Cntrct_Found().reset();
        pdaBena9865.getBena9865_Pnd_Pin_Found().reset();
        pdaBena9865.getBena9865_Pnd_Pin().reset();
        pdaBena9865.getBena9865_Pnd_Cntrct_Is_Active().reset();
        //*  READ FOR ONE-UP CALL
        //*  TIAA
        //*  PAYEE CODE
        pnd_Psgm003_Common_Area.reset();                                                                                                                                  //Natural: RESET #PSGM003-COMMON-AREA
        pnd_Psgm003_Common_Area_Pnd_In_Function.setValue("PR");                                                                                                           //Natural: ASSIGN #IN-FUNCTION := 'PR'
        pnd_Psgm003_Common_Area_Pnd_In_Contract_Num_Typ.setValue("T");                                                                                                    //Natural: ASSIGN #IN-CONTRACT-NUM-TYP := 'T'
        pnd_Psgm003_Common_Area_Pnd_In_Type.setValue(" ");                                                                                                                //Natural: ASSIGN #IN-TYPE := ' '
        pnd_Psgm003_Common_Area_Pnd_In_Ssnpin.setValue(" ");                                                                                                              //Natural: ASSIGN #IN-SSNPIN := ' '
        pnd_Psgm003_Common_Area_Pnd_In_Contract.setValue(pdaBena9865.getBena9865_Pnd_Cntrct());                                                                           //Natural: ASSIGN #IN-CONTRACT := #CNTRCT
        pnd_Psgm003_Common_Area_Pnd_In_Payee_Cd.setValue(" ");                                                                                                            //Natural: ASSIGN #IN-PAYEE-CD := ' '
        psgm003ReturnCode = DbsUtil.callExternalProgram("PSGM003",pnd_Psgm003_Common_Area_Pnd_In_Function,pnd_Psgm003_Common_Area_Pnd_In_Contract_Num_Typ,                //Natural: CALL 'PSGM003' USING #PSGM003-COMMON-AREA
            pnd_Psgm003_Common_Area_Pnd_In_Type,pnd_Psgm003_Common_Area_Pnd_In_Ssnpin,pnd_Psgm003_Common_Area_Pnd_In_Contract,pnd_Psgm003_Common_Area_Pnd_In_Payee_Cd,
            pnd_Psgm003_Common_Area_Pnd_Rt_Ret_Code,pnd_Psgm003_Common_Area_Pnd_Rt_Input_String,pnd_Psgm003_Common_Area_Pnd_Rt_Ssn,pnd_Psgm003_Common_Area_Pnd_Rt_Pin,
            pnd_Psgm003_Common_Area_Pnd_Rt_Tiaa_Cntrct_Nmbr,pnd_Psgm003_Common_Area_Pnd_Rt_Xpayee_Cde,pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Status_Code,
            pnd_Psgm003_Common_Area_Pnd_Rt_Ctrct_Status_Dt,pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Issue_Dt,pnd_Psgm003_Common_Area_Pnd_Rt_Lob,pnd_Psgm003_Common_Area_Pnd_Rt_Cref_Contract_Nbr,
            pnd_Psgm003_Common_Area_Pnd_Rt_Cref_Issued_Ind,pnd_Psgm003_Common_Area_Pnd_Rt_Ownership_Code,pnd_Psgm003_Common_Area_Pnd_Rt_Product_Code,pnd_Psgm003_Common_Area_Pnd_Rt_Option_Code,
            pnd_Psgm003_Common_Area_Pnd_Rt_Plan_Code,pnd_Psgm003_Common_Area_Pnd_Rt_Fund_Code,pnd_Psgm003_Common_Area_Pnd_Rt_Social_Code,pnd_Psgm003_Common_Area_Pnd_Rt_Custodial_Agreement,
            pnd_Psgm003_Common_Area_Pnd_Rt_Aas_Ind,pnd_Psgm003_Common_Area_Pnd_Rt_Platform_Ind,pnd_Psgm003_Common_Area_Pnd_Rt_Multi_Plan_Ind,pnd_Psgm003_Common_Area_Pnd_Rt_Tiaa_Mailed_Ind,
            pnd_Psgm003_Common_Area_Pnd_Rt_Cref_Mailed_Ind,pnd_Psgm003_Common_Area_Pnd_Rt_Dflt_Enrollment_Ind,pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Lst_Updt_Dt,
            pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Lst_Updt_Time,pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Lst_Updt_Usrid,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Per_Addr_Ind,
            pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Type_Cd,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_One,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_Two,
            pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_Three,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_Four,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_Five,
            pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_City,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_State,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Zip,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Country,
            pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Postal_Data,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Geo_Cd,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Status,
            pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Status_Dt,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Lst_Ch_Src_Cd,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Lst_Ch_Dt,
            pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Lst_Ch_Time,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Lst_Ch_Usrid,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Per_Addr_Ind,
            pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Type_Cd,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_One,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_Two,
            pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_Three,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_Four,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_Five,
            pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_City,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_State,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Zip,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Country,
            pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Ch_Saving_Cde,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Ph_Bnk_Pt_Acct_Nbr,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Bnk_Aba_Eft_Nbr,
            pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Eft_Pay_Type_Cde,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Postal_Data,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Geo_Cd,
            pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Status,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Status_Dt,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Lst_Ch_Src_Cd,
            pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Lst_Ch_Dt,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Lst_Ch_Time,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Lst_Ch_Usrid,
            pnd_Psgm003_Common_Area_Pnd_Rt_Prefix,pnd_Psgm003_Common_Area_Pnd_Rt_Last_Name,pnd_Psgm003_Common_Area_Pnd_Rt_First_Name,pnd_Psgm003_Common_Area_Pnd_Rt_Given_Name_Two,
            pnd_Psgm003_Common_Area_Pnd_Rt_Suffix_Desc,pnd_Psgm003_Common_Area_Pnd_Rt_Record_Status);
        //*  SUCCESS
        if (condition(pnd_Psgm003_Common_Area_Pnd_Rt_Record_Status.equals("Y") && pnd_Psgm003_Common_Area_Pnd_Rt_Tiaa_Cntrct_Nmbr.equals(pdaBena9865.getBena9865_Pnd_Cntrct()))) //Natural: IF #RT-RECORD-STATUS = 'Y' AND #RT-TIAA-CNTRCT-NMBR = #CNTRCT
        {
            if (condition(DbsUtil.maskMatches(pnd_Psgm003_Common_Area_Pnd_Rt_Pin,"9999999' '") || DbsUtil.maskMatches(pnd_Psgm003_Common_Area_Pnd_Rt_Pin,                 //Natural: IF #RT-PIN = MASK ( 9999999' ' ) OR #RT-PIN = MASK ( 999999999999' ' )
                "999999999999' '")))
            {
                pdaBena9865.getBena9865_Pnd_Pin_Found().setValue(true);                                                                                                   //Natural: ASSIGN #PIN-FOUND := TRUE
                pdaBena9865.getBena9865_Pnd_Pin().compute(new ComputeParameters(false, pdaBena9865.getBena9865_Pnd_Pin()), pnd_Psgm003_Common_Area_Pnd_Rt_Pin.val());     //Natural: ASSIGN #PIN := VAL ( #RT-PIN )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Status_Code.equals(" ") || pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Status_Code.equals("Y")                  //Natural: IF #RT-CNTRCT-STATUS-CODE = ' ' OR = 'Y' OR = 'H'
                || pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Status_Code.equals("H")))
            {
                pdaBena9865.getBena9865_Pnd_Cntrct_Is_Active().setValue(true);                                                                                            //Natural: ASSIGN #CNTRCT-IS-ACTIVE := TRUE
                //*  CNTRCT-NMBR-RANGE-CDE
                //*  CNTRCT-SPCL-CNSDRTN-CDE
            }                                                                                                                                                             //Natural: END-IF
            pdaBena9865.getBena9865_Pnd_Cntrct_Found().setValue(true);                                                                                                    //Natural: ASSIGN #CNTRCT-FOUND := TRUE
            pdaBena9865.getBena9865_Pnd_Cntrct_Issue_Dte().setValue(pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Issue_Dt);                                                      //Natural: ASSIGN #CNTRCT-ISSUE-DTE := #RT-CNTRCT-ISSUE-DT
            pdaBena9865.getBena9865_Pnd_Cntrct_Lob_Cde().setValue(pnd_Psgm003_Common_Area_Pnd_Rt_Lob);                                                                    //Natural: ASSIGN #CNTRCT-LOB-CDE := #RT-LOB
            pdaBena9865.getBena9865_Pnd_Cntrct_Nmbr_Range_Cde().setValue(" ");                                                                                            //Natural: ASSIGN #CNTRCT-NMBR-RANGE-CDE := ' '
            pdaBena9865.getBena9865_Pnd_Cntrct_Spcl_Cnsdrtn_Cde().setValue(" ");                                                                                          //Natural: ASSIGN #CNTRCT-SPCL-CNSDRTN-CDE := ' '
            pdaBena9865.getBena9865_Pnd_Cntrct_Cref_Nbr().setValue(pnd_Psgm003_Common_Area_Pnd_Rt_Cref_Contract_Nbr);                                                     //Natural: ASSIGN #CNTRCT-CREF-NBR := #RT-CREF-CONTRACT-NBR
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
