/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:06:26 PM
**        * FROM NATURAL SUBPROGRAM : Benn9821
************************************************************
**        * FILE NAME            : Benn9821.java
**        * CLASS NAME           : Benn9821
**        * INSTANCE NAME        : Benn9821
************************************************************
************************************************************************
* PROGRAM  : BENN9821
* SYSTEM   : BENEFICIARY-SYSTEM
* TITLE    : INTERFACE SPECIFIC EDITS
* WRITTEN  : JULY 19, 1999
************************************************************************
*    DATE      USERID                   DESCRIPTION
* MM/DD/YYYY  XXXXXXXX  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
* 05/16/2000  WEBBJ     CHANGE CHECK ON BENE ON-LINE FILES FOR
*                       TIAA-CREF IND
* 10/04/2000  WEBBJ     CODE FOR CORR CORRECTIONS TO EXISTING CONTRACT
* 08/31/2005  SOTTO     DON't pass return-code E for "Illegal COR"
*                       SO JOB P2010BFD WILL NOT TERMINATE.
* 04/01/2017  DURAND    PIN EXPANSION N7 TO N12
************************************************************************

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benn9821 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaBena9801 pdaBena9801;
    private PdaBena9821 pdaBena9821;
    private PdaBenpda_M pdaBenpda_M;
    private PdaBenpda_E pdaBenpda_E;
    private PdaBena9814 pdaBena9814;
    private PdaBena9816 pdaBena9816;
    private PdaBena9702 pdaBena9702;
    private PdaBena9701 pdaBena9701;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Bene_Rcrd_Stts;

    private DbsGroup pnd_Bene_Rcrd_Stts__R_Field_1;
    private DbsField pnd_Bene_Rcrd_Stts_Pnd_Bene_Rcrd_Stts_A1;
    private DbsField pnd_C_Last_Srce;
    private DbsField pnd_M_Last_Srce;
    private DbsField pnd_I;
    private DbsField pnd_J1;
    private DbsField pnd_Stts_Found;
    private DbsField pnd_Other_Check_T_C_Ind;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaBena9814 = new PdaBena9814(localVariables);
        pdaBena9816 = new PdaBena9816(localVariables);
        pdaBena9702 = new PdaBena9702(localVariables);
        pdaBena9701 = new PdaBena9701(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaBena9801 = new PdaBena9801(parameters);
        pdaBena9821 = new PdaBena9821(parameters);
        pdaBenpda_M = new PdaBenpda_M(parameters);
        pdaBenpda_E = new PdaBenpda_E(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Bene_Rcrd_Stts = localVariables.newFieldInRecord("pnd_Bene_Rcrd_Stts", "#BENE-RCRD-STTS", FieldType.STRING, 10);

        pnd_Bene_Rcrd_Stts__R_Field_1 = localVariables.newGroupInRecord("pnd_Bene_Rcrd_Stts__R_Field_1", "REDEFINE", pnd_Bene_Rcrd_Stts);
        pnd_Bene_Rcrd_Stts_Pnd_Bene_Rcrd_Stts_A1 = pnd_Bene_Rcrd_Stts__R_Field_1.newFieldArrayInGroup("pnd_Bene_Rcrd_Stts_Pnd_Bene_Rcrd_Stts_A1", "#BENE-RCRD-STTS-A1", 
            FieldType.STRING, 1, new DbsArrayController(1, 10));
        pnd_C_Last_Srce = localVariables.newFieldInRecord("pnd_C_Last_Srce", "#C-LAST-SRCE", FieldType.STRING, 1);
        pnd_M_Last_Srce = localVariables.newFieldInRecord("pnd_M_Last_Srce", "#M-LAST-SRCE", FieldType.STRING, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_J1 = localVariables.newFieldInRecord("pnd_J1", "#J1", FieldType.PACKED_DECIMAL, 3);
        pnd_Stts_Found = localVariables.newFieldArrayInRecord("pnd_Stts_Found", "#STTS-FOUND", FieldType.BOOLEAN, 1, new DbsArrayController(1, 10));
        pnd_Other_Check_T_C_Ind = localVariables.newFieldInRecord("pnd_Other_Check_T_C_Ind", "#OTHER-CHECK-T-C-IND", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Bene_Rcrd_Stts.setInitialValue("CPRM");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Benn9821() throws Exception
    {
        super("Benn9821");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ERROR-INFO-SUB.##LAST-PROGRAM := *PROGRAM
        pdaBena9821.getBena9821().reset();                                                                                                                                //Natural: RESET BENA9821
                                                                                                                                                                          //Natural: PERFORM GET-EXISTING-BENE-RECORDS-STATUSES
        sub_Get_Existing_Bene_Records_Statuses();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-ERRORED-ON-INTERFACE-FILE
        sub_Check_If_Errored_On_Interface_File();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-EXISTS-ON-INTERFACE-FILE
        sub_Check_If_Exists_On_Interface_File();
        if (condition(Global.isEscape())) {return;}
        short decideConditionsMet204 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN BENA9701.#NBR-RECS-FOUND GT 1
        if (condition(pdaBena9701.getBena9701_Pnd_Nbr_Recs_Found().greater(1)))
        {
            decideConditionsMet204++;
            //*   IF MORE THAN ONE INTERFACE RECORD EXISTS FOR THIS PIN/CNTRCT/IND
            //*     FOR THIS BUSINESS DATE, THEN ERROR.  THIS WILL HAVE TO BE
            //*     RESEARCHED BY THE USERS SO A DETERMINATION OF THE CORRECT
            //*     DESIGNATION CAN BE MADE
            pdaBena9821.getBena9821_Pnd_Error_Msg().setValue(DbsUtil.compress("Multiple Designations found on Intrfce:", pdaBena9801.getBena9801_Pin_Tiaa_Cntrct(),       //Natural: COMPRESS 'Multiple Designations found on Intrfce:' BENA9801.PIN-TIAA-CNTRCT '/' BENA9801.TIAA-CREF-IND '/' BENA9801.RCRD-CRTD-FOR-BSNSS-DTE INTO BENA9821.#ERROR-MSG
                "/", pdaBena9801.getBena9801_Tiaa_Cref_Ind(), "/", pdaBena9801.getBena9801_Rcrd_Crtd_For_Bsnss_Dte()));
            pdaBena9821.getBena9821_Pnd_Edit_Failed().setValue(true);                                                                                                     //Natural: ASSIGN BENA9821.#EDIT-FAILED := TRUE
        }                                                                                                                                                                 //Natural: WHEN BENA9801.CHNG-NEW-ISSUE-IND = 'C' AND BENA9801.RQSTNG-SYSTM NE 'CORR'
        else if (condition(pdaBena9801.getBena9801_Chng_New_Issue_Ind().equals("C") && pdaBena9801.getBena9801_Rqstng_Systm().notEquals("CORR")))
        {
            decideConditionsMet204++;
            short decideConditionsMet214 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN NOT #STTS-FOUND ( * ) NE FALSE
            if (condition(! (pnd_Stts_Found.getValue("*").notEquals(false))))
            {
                decideConditionsMet214++;
                pdaBena9821.getBena9821_Pnd_Error_Msg().setValue(DbsUtil.compress(pdaBena9801.getBena9801_Pin_Tiaa_Cntrct(), "/", pdaBena9801.getBena9801_Tiaa_Cref_Ind(),  //Natural: COMPRESS BENA9801.PIN-TIAA-CNTRCT '/' BENA9801.TIAA-CREF-IND 'does not exist in the Bene System!' INTO BENA9821.#ERROR-MSG
                    "does not exist in the Bene System!"));
                pdaBena9821.getBena9821_Pnd_Error_Cde().setValue("E0001");                                                                                                //Natural: ASSIGN BENA9821.#ERROR-CDE := 'E0001'
                pdaBena9821.getBena9821_Pnd_Edit_Failed().setValue(true);                                                                                                 //Natural: ASSIGN BENA9821.#EDIT-FAILED := TRUE
            }                                                                                                                                                             //Natural: WHEN NOT ( #STTS-FOUND ( 1 ) OR #STTS-FOUND ( 4 ) )
            else if (condition(! ((pnd_Stts_Found.getValue(1).equals(true) || pnd_Stts_Found.getValue(4).equals(true)))))
            {
                decideConditionsMet214++;
                pdaBena9821.getBena9821_Pnd_Error_Msg().setValue(DbsUtil.compress("Designation not current in Bene System for", pdaBena9801.getBena9801_Pin_Tiaa_Cntrct(),  //Natural: COMPRESS 'Designation not current in Bene System for' BENA9801.PIN-TIAA-CNTRCT '/' BENA9801.TIAA-CREF-IND INTO BENA9821.#ERROR-MSG
                    "/", pdaBena9801.getBena9801_Tiaa_Cref_Ind()));
                pdaBena9821.getBena9821_Pnd_Error_Cde().setValue("E0002");                                                                                                //Natural: ASSIGN BENA9821.#ERROR-CDE := 'E0002'
                pdaBena9821.getBena9821_Pnd_Edit_Failed().setValue(true);                                                                                                 //Natural: ASSIGN BENA9821.#EDIT-FAILED := TRUE
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*                                                 HERE        JW 05/16/00
        }                                                                                                                                                                 //Natural: WHEN BENA9801.CHNG-NEW-ISSUE-IND = 'N' AND #STTS-FOUND ( * ) NE FALSE AND #OTHER-CHECK-T-C-IND GT ' '
        else if (condition(pdaBena9801.getBena9801_Chng_New_Issue_Ind().equals("N") && pnd_Stts_Found.getValue("*").notEquals(false) && pnd_Other_Check_T_C_Ind.greater(" ")))
        {
            decideConditionsMet204++;
            pdaBena9821.getBena9821_Pnd_Error_Msg().setValue(DbsUtil.compress(pdaBena9801.getBena9801_Pin_Tiaa_Cntrct(), "/", pnd_Other_Check_T_C_Ind,                    //Natural: COMPRESS BENA9801.PIN-TIAA-CNTRCT '/' #OTHER-CHECK-T-C-IND 'on BENE system awaits verify. Cannot Interface' BENA9801.TIAA-CREF-IND 'des.' INTO BENA9821.#ERROR-MSG
                "on BENE system awaits verify. Cannot Interface", pdaBena9801.getBena9801_Tiaa_Cref_Ind(), "des."));
            pdaBena9821.getBena9821_Pnd_Error_Cde().setValue("E0003");                                                                                                    //Natural: ASSIGN BENA9821.#ERROR-CDE := 'E0003'
            pdaBena9821.getBena9821_Pnd_Edit_Failed().setValue(true);                                                                                                     //Natural: ASSIGN BENA9821.#EDIT-FAILED := TRUE
        }                                                                                                                                                                 //Natural: WHEN ( BENA9801.CHNG-NEW-ISSUE-IND = 'N' OR BENA9801.RQSTNG-SYSTM = 'CORR' ) AND #STTS-FOUND ( * ) NE FALSE AND BENA9814.#TIAA-CREF-IND NE BENA9801.TIAA-CREF-IND
        else if (condition((pdaBena9801.getBena9801_Chng_New_Issue_Ind().equals("N") || pdaBena9801.getBena9801_Rqstng_Systm().equals("CORR")) && pnd_Stts_Found.getValue("*").notEquals(false) 
            && pdaBena9814.getBena9814_Pnd_Tiaa_Cref_Ind().notEquals(pdaBena9801.getBena9801_Tiaa_Cref_Ind())))
        {
            decideConditionsMet204++;
            //*  BENA9814.#TIAA-CREF-IND CARRIES THE CURRENT ONLINE VALUE
            short decideConditionsMet234 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF BENA9814.#TIAA-CREF-IND;//Natural: VALUE 'C'
            if (condition((pdaBena9814.getBena9814_Pnd_Tiaa_Cref_Ind().equals("C"))))
            {
                decideConditionsMet234++;
                pdaBena9821.getBena9821_Pnd_Error_Msg().setValue(DbsUtil.compress(pdaBena9801.getBena9801_Pin_Tiaa_Cntrct(), "already on BENE system with CREF-only ind")); //Natural: COMPRESS BENA9801.PIN-TIAA-CNTRCT 'already on BENE system with CREF-only ind' INTO BENA9821.#ERROR-MSG
            }                                                                                                                                                             //Natural: VALUE 'T'
            else if (condition((pdaBena9814.getBena9814_Pnd_Tiaa_Cref_Ind().equals("T"))))
            {
                decideConditionsMet234++;
                pdaBena9821.getBena9821_Pnd_Error_Msg().setValue(DbsUtil.compress(pdaBena9801.getBena9801_Pin_Tiaa_Cntrct(), "already on BENE system with TIAA-only ind")); //Natural: COMPRESS BENA9801.PIN-TIAA-CNTRCT 'already on BENE system with TIAA-only ind' INTO BENA9821.#ERROR-MSG
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pdaBena9821.getBena9821_Pnd_Error_Msg().setValue(DbsUtil.compress(pdaBena9801.getBena9801_Pin_Tiaa_Cntrct(), "already on BENE system for both TIAA & CREF")); //Natural: COMPRESS BENA9801.PIN-TIAA-CNTRCT 'already on BENE system for both TIAA & CREF' INTO BENA9821.#ERROR-MSG
            }                                                                                                                                                             //Natural: END-DECIDE
            pdaBena9821.getBena9821_Pnd_Error_Cde().setValue("E0003");                                                                                                    //Natural: ASSIGN BENA9821.#ERROR-CDE := 'E0003'
            pdaBena9821.getBena9821_Pnd_Edit_Failed().setValue(true);                                                                                                     //Natural: ASSIGN BENA9821.#EDIT-FAILED := TRUE
        }                                                                                                                                                                 //Natural: WHEN BENA9801.CHNG-NEW-ISSUE-IND = 'C' AND BENA9801.RQSTNG-SYSTM = 'CORR'
        else if (condition(pdaBena9801.getBena9801_Chng_New_Issue_Ind().equals("C") && pdaBena9801.getBena9801_Rqstng_Systm().equals("CORR")))
        {
            decideConditionsMet204++;
            if (condition(pnd_Stts_Found.getValue(2).getBoolean() || pnd_Stts_Found.getValue(3).getBoolean()))                                                            //Natural: IF #STTS-FOUND ( 2 ) OR #STTS-FOUND ( 3 )
            {
                pdaBena9821.getBena9821_Pnd_Error_Msg().setValue(DbsUtil.compress("Designation not current in Bene System for", pdaBena9801.getBena9801_Pin_Tiaa_Cntrct(),  //Natural: COMPRESS 'Designation not current in Bene System for' BENA9801.PIN-TIAA-CNTRCT '/' BENA9801.TIAA-CREF-IND INTO BENA9821.#ERROR-MSG
                    "/", pdaBena9801.getBena9801_Tiaa_Cref_Ind()));
                pdaBena9821.getBena9821_Pnd_Error_Cde().setValue("E0002");                                                                                                //Natural: ASSIGN BENA9821.#ERROR-CDE := 'E0002'
                pdaBena9821.getBena9821_Pnd_Edit_Failed().setValue(true);                                                                                                 //Natural: ASSIGN BENA9821.#EDIT-FAILED := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN BENA9801.CHNG-NEW-ISSUE-IND = 'N' AND #STTS-FOUND ( * ) NE FALSE
        else if (condition(pdaBena9801.getBena9801_Chng_New_Issue_Ind().equals("N") && pnd_Stts_Found.getValue("*").notEquals(false)))
        {
            decideConditionsMet204++;
            pdaBena9821.getBena9821_Pnd_Error_Msg().setValue(DbsUtil.compress(pdaBena9801.getBena9801_Pin_Tiaa_Cntrct(), "/", pdaBena9814.getBena9814_Pnd_Tiaa_Cref_Ind(),  //Natural: COMPRESS BENA9801.PIN-TIAA-CNTRCT '/' BENA9814.#TIAA-CREF-IND 'already on BENE system for NEW ISSUE process' INTO BENA9821.#ERROR-MSG
                "already on BENE system for NEW ISSUE process"));
            pdaBena9821.getBena9821_Pnd_Error_Cde().setValue("E0003");                                                                                                    //Natural: ASSIGN BENA9821.#ERROR-CDE := 'E0003'
            pdaBena9821.getBena9821_Pnd_Edit_Failed().setValue(true);                                                                                                     //Natural: ASSIGN BENA9821.#EDIT-FAILED := TRUE
            //*   FOR A NEW ISSUE:
            //*   IF THIS PIN/CONTRACT/IND EXISTS ON THE INTEFACE FILE IN AN
            //*     "ERRORed" STATUS, KICK IT OUT!
        }                                                                                                                                                                 //Natural: WHEN BENA9801.CHNG-NEW-ISSUE-IND = 'N' AND BENA9702.#ERRORED-EXISTS = TRUE
        else if (condition(pdaBena9801.getBena9801_Chng_New_Issue_Ind().equals("N") && pdaBena9702.getBena9702_Pnd_Errored_Exists().equals(true)))
        {
            decideConditionsMet204++;
            pdaBena9821.getBena9821_Pnd_Error_Msg().setValue(DbsUtil.compress(pdaBena9801.getBena9801_Pin_Tiaa_Cntrct(), "/", pdaBena9801.getBena9801_Tiaa_Cref_Ind(),    //Natural: COMPRESS BENA9801.PIN-TIAA-CNTRCT '/' BENA9801.TIAA-CREF-IND 'already in ERROR status on INTERFACE FILE' INTO BENA9821.#ERROR-MSG
                "already in ERROR status on INTERFACE FILE"));
            pdaBena9821.getBena9821_Pnd_Edit_Failed().setValue(true);                                                                                                     //Natural: ASSIGN BENA9821.#EDIT-FAILED := TRUE
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pdaBena9821.getBena9821_Pnd_Edit_Failed().equals(true)))                                                                                            //Natural: IF BENA9821.#EDIT-FAILED = TRUE
        {
            pdaBena9821.getBena9821_Pnd_Error_Pgm().setValue(Global.getPROGRAM());                                                                                        //Natural: ASSIGN BENA9821.#ERROR-PGM := *PROGRAM
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet274 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN MORE-THAN-FIVE-BENES-IND = 'Y'
        if (condition(pdaBena9801.getBena9801_More_Than_Five_Benes_Ind().equals("Y")))
        {
            decideConditionsMet274++;
            pdaBena9821.getBena9821_Pnd_Error_Msg().setValue("ACIS - More than ten benes");                                                                               //Natural: ASSIGN #ERROR-MSG := 'ACIS - More than ten benes'
        }                                                                                                                                                                 //Natural: WHEN ILLGBLE-IND = 'Y'
        else if (condition(pdaBena9801.getBena9801_Illgble_Ind().equals("Y")))
        {
            decideConditionsMet274++;
            pdaBena9821.getBena9821_Pnd_Error_Msg().setValue("ACIS - Illegible");                                                                                         //Natural: ASSIGN #ERROR-MSG := 'ACIS - Illegible'
        }                                                                                                                                                                 //Natural: WHEN MORE-THAN-THIRTY-BENES-IND = 'Y'
        else if (condition(pdaBena9801.getBena9801_More_Than_Thirty_Benes_Ind().equals("Y")))
        {
            decideConditionsMet274++;
            pdaBena9821.getBena9821_Pnd_Error_Msg().setValue("More than thirty benes");                                                                                   //Natural: ASSIGN #ERROR-MSG := 'More than thirty benes'
        }                                                                                                                                                                 //Natural: WHEN RCRD-CRTD-FOR-BSNSS-DTE NE MASK ( YYYYMMDD )
        else if (condition(! ((DbsUtil.maskMatches(pdaBena9801.getBena9801_Rcrd_Crtd_For_Bsnss_Dte(),"YYYYMMDD")))))
        {
            decideConditionsMet274++;
            pdaBena9821.getBena9821_Pnd_Error_Msg().setValue(DbsUtil.compress("Ctrd For Bsnss Dte Invalid:", pdaBena9801.getBena9801_Rcrd_Crtd_For_Bsnss_Dte()));         //Natural: COMPRESS 'Ctrd For Bsnss Dte Invalid:' RCRD-CRTD-FOR-BSNSS-DTE INTO #ERROR-MSG
        }                                                                                                                                                                 //Natural: WHEN INTRFCNG-SYSTM = ' '
        else if (condition(pdaBena9801.getBena9801_Intrfcng_Systm().equals(" ")))
        {
            decideConditionsMet274++;
            pdaBena9821.getBena9821_Pnd_Error_Msg().setValue(DbsUtil.compress("Invalid interfacing system:", pdaBena9801.getBena9801_Intrfcng_Systm()));                  //Natural: COMPRESS 'Invalid interfacing system:' INTRFCNG-SYSTM INTO #ERROR-MSG
        }                                                                                                                                                                 //Natural: WHEN RQSTNG-SYSTM = ' '
        else if (condition(pdaBena9801.getBena9801_Rqstng_Systm().equals(" ")))
        {
            decideConditionsMet274++;
            pdaBena9821.getBena9821_Pnd_Error_Msg().setValue(DbsUtil.compress("Invalid requesting system:", pdaBena9801.getBena9801_Rqstng_Systm()));                     //Natural: COMPRESS 'Invalid requesting system:' RQSTNG-SYSTM INTO #ERROR-MSG
        }                                                                                                                                                                 //Natural: WHEN TIAA-CNTRCT = ' '
        else if (condition(pdaBena9801.getBena9801_Tiaa_Cntrct().equals(" ")))
        {
            decideConditionsMet274++;
            pdaBena9821.getBena9821_Pnd_Error_Msg().setValue(DbsUtil.compress("Invalid TIAA Contract:", pdaBena9801.getBena9801_Pin(), pdaBena9801.getBena9801_Tiaa_Cntrct())); //Natural: COMPRESS 'Invalid TIAA Contract:' PIN TIAA-CNTRCT INTO #ERROR-MSG
        }                                                                                                                                                                 //Natural: WHEN PIN NE MASK ( 9999999 )
        else if (condition(! ((DbsUtil.maskMatches(pdaBena9801.getBena9801_Pin(),"9999999")))))
        {
            decideConditionsMet274++;
            pdaBena9821.getBena9821_Pnd_Error_Msg().setValue(DbsUtil.compress("Invalid PIN:", pdaBena9801.getBena9801_Pin()));                                            //Natural: COMPRESS 'Invalid PIN:' PIN INTO #ERROR-MSG
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet274 > 0))
        {
            pdaBena9821.getBena9821_Pnd_Edit_Failed().setValue(true);                                                                                                     //Natural: ASSIGN BENA9821.#EDIT-FAILED := TRUE
            pdaBena9821.getBena9821_Pnd_Error_Pgm().setValue(Global.getPROGRAM());                                                                                        //Natural: ASSIGN BENA9821.#ERROR-PGM := *PROGRAM
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  THESE WERE EDITED 'O'NLINE - CANNOT BE
        //*  UPDATED BATCH BY ACIS. (SHOULDN't happen)
        //*  083105 END
        if (condition(pdaBena9801.getBena9801_Rqstng_Systm().equals("CORR") && (pnd_C_Last_Srce.equals("O") || pnd_M_Last_Srce.equals("O"))))                             //Natural: IF BENA9801.RQSTNG-SYSTM = 'CORR' AND ( #C-LAST-SRCE = 'O' OR #M-LAST-SRCE = 'O' )
        {
            //*  CREATE TERMINATE - CONTROL GOES BACK TO DRIVER FOR DISPLAY.
            pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("Illegal CORR Transaction sent from ACIS");                                                                //Natural: ASSIGN ##MSG := 'Illegal CORR Transaction sent from ACIS'
            pdaBena9821.getBena9821_Pnd_Edit_Failed().setValue(true);                                                                                                     //Natural: ASSIGN BENA9821.#EDIT-FAILED := TRUE
            pdaBena9821.getBena9821_Pnd_Error_Pgm().setValue(Global.getPROGRAM());                                                                                        //Natural: ASSIGN BENA9821.#ERROR-PGM := *PROGRAM
            pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                              //Natural: ASSIGN ##RETURN-CODE := 'E'
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-IF-ERRORED-ON-INTERFACE-FILE
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-EXISTING-BENE-RECORDS-STATUSES
        //*  CHECK T/C-IND = ' ' - CANNOT CO-EXIST WITH A 'T' OR A 'C'
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-IF-EXISTS-ON-INTERFACE-FILE
        //* ***********************************************************************
    }
    private void sub_Check_If_Errored_On_Interface_File() throws Exception                                                                                                //Natural: CHECK-IF-ERRORED-ON-INTERFACE-FILE
    {
        if (BLNatReinput.isReinput()) return;

        pdaBena9702.getBena9702_Pnd_Pin().setValue(pdaBena9801.getBena9801_Pin());                                                                                        //Natural: ASSIGN BENA9702.#PIN := BENA9801.PIN
        pdaBena9702.getBena9702_Pnd_Tiaa_Cntrct().setValue(pdaBena9801.getBena9801_Tiaa_Cntrct());                                                                        //Natural: ASSIGN BENA9702.#TIAA-CNTRCT := BENA9801.TIAA-CNTRCT
        pdaBena9702.getBena9702_Pnd_Tiaa_Cref_Ind().setValue(pdaBena9801.getBena9801_Tiaa_Cref_Ind());                                                                    //Natural: ASSIGN BENA9702.#TIAA-CREF-IND := BENA9801.TIAA-CREF-IND
        pdaBena9702.getBena9702_Pnd_Exists().reset();                                                                                                                     //Natural: RESET BENA9702.#EXISTS BENA9702.#ERRORED-EXISTS
        pdaBena9702.getBena9702_Pnd_Errored_Exists().reset();
        DbsUtil.callnat(Benn9702.class , getCurrentProcessState(), pdaBena9702.getBena9702(), pdaBenpda_M.getMsg_Info_Sub());                                             //Natural: CALLNAT 'BENN9702' BENA9702 MSG-INFO-SUB
        if (condition(Global.isEscape())) return;
        //*  CHECK-IF-ERRORED-ON-INTERFACE-FILE
    }
    private void sub_Get_Existing_Bene_Records_Statuses() throws Exception                                                                                                //Natural: GET-EXISTING-BENE-RECORDS-STATUSES
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*   DETERMINE WHICH STATUSES EXIST IN THE BENE SYSTEM.
        pdaBena9816.getBena9816().reset();                                                                                                                                //Natural: RESET BENA9816
        pdaBena9816.getBena9816_Pnd_Pin_Tiaa_Cntrct().setValue(pdaBena9801.getBena9801_Pin_Tiaa_Cntrct());                                                                //Natural: ASSIGN BENA9816.#PIN-TIAA-CNTRCT := BENA9801.PIN-TIAA-CNTRCT
        pdaBena9814.getBena9814_Pnd_Pin_Tiaa_Cntrct().setValue(pdaBena9801.getBena9801_Pin_Tiaa_Cntrct());                                                                //Natural: ASSIGN BENA9814.#PIN-TIAA-CNTRCT := BENA9801.PIN-TIAA-CNTRCT
        pdaBena9814.getBena9814_Pnd_Tiaa_Cref_Ind().setValue(pdaBena9801.getBena9801_Tiaa_Cref_Ind());                                                                    //Natural: ASSIGN BENA9814.#TIAA-CREF-IND := BENA9801.TIAA-CREF-IND
        pnd_Other_Check_T_C_Ind.reset();                                                                                                                                  //Natural: RESET #OTHER-CHECK-T-C-IND
        //* IF NEW ISSUE AND BLANK T/C THEN
        //*  CONTRACT SHOULD'nt be on-line
        if (condition((pdaBena9801.getBena9801_Chng_New_Issue_Ind().equals("N") || pdaBena9801.getBena9801_Rqstng_Systm().equals("CORR")) && pdaBena9801.getBena9801_Tiaa_Cref_Ind().equals(" "))) //Natural: IF ( BENA9801.CHNG-NEW-ISSUE-IND = 'N' OR BENA9801.RQSTNG-SYSTM = 'CORR' ) AND BENA9801.TIAA-CREF-IND = ' '
        {
            //*  FIND BY CONTRACT # ONLY            /* AT ALL
            //*  HOWEVER, CORR CAN BE ON-LINE AS A 'Both' IF THAT's what is interfacing
            DbsUtil.callnat(Benn9816.class , getCurrentProcessState(), pdaBena9816.getBena9816(), pdaBenpda_M.getMsg_Info_Sub(), pdaBenpda_E.getError_Info_Sub());        //Natural: CALLNAT 'BENN9816' BENA9816 MSG-INFO-SUB ERROR-INFO-SUB
            if (condition(Global.isEscape())) return;
            pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                           //Natural: ASSIGN ERROR-INFO-SUB.##LAST-PROGRAM := *PROGRAM
            if (condition(pdaBena9816.getBena9816_Pnd_Found().getBoolean()))                                                                                              //Natural: IF BENA9816.#FOUND
            {
                if (condition(pdaBena9801.getBena9801_Chng_New_Issue_Ind().equals("N") || (pdaBena9816.getBena9816_Pnd_Tiaa_Cref_Ind().greater(" ") &&                    //Natural: IF BENA9801.CHNG-NEW-ISSUE-IND = 'N' OR ( BENA9816.#TIAA-CREF-IND GT ' ' AND BENA9801.RQSTNG-SYSTM = 'CORR' )
                    pdaBena9801.getBena9801_Rqstng_Systm().equals("CORR"))))
                {
                    pnd_Stts_Found.getValue(1).setValue(pdaBena9816.getBena9816_Pnd_Found().getBoolean());                                                                //Natural: ASSIGN #STTS-FOUND ( 1 ) := BENA9816.#FOUND
                    pdaBena9814.getBena9814_Pnd_Tiaa_Cref_Ind().setValue(pdaBena9816.getBena9816_Pnd_Tiaa_Cref_Ind());                                                    //Natural: ASSIGN BENA9814.#TIAA-CREF-IND := BENA9816.#TIAA-CREF-IND
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        F1:                                                                                                                                                               //Natural: FOR #I 1 10
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(10)); pnd_I.nadd(1))
        {
            if (condition(pnd_Bene_Rcrd_Stts_Pnd_Bene_Rcrd_Stts_A1.getValue(pnd_I).equals(" ")))                                                                          //Natural: IF #BENE-RCRD-STTS-A1 ( #I ) = ' '
            {
                if (true) break F1;                                                                                                                                       //Natural: ESCAPE BOTTOM ( F1. )
            }                                                                                                                                                             //Natural: END-IF
            pdaBena9814.getBena9814_Pnd_Stts().setValue(pnd_Bene_Rcrd_Stts_Pnd_Bene_Rcrd_Stts_A1.getValue(pnd_I));                                                        //Natural: ASSIGN BENA9814.#STTS := #BENE-RCRD-STTS-A1 ( #I )
            pdaBena9814.getBena9814_Pnd_Found().reset();                                                                                                                  //Natural: RESET BENA9814.#FOUND BENA9814.#LAST-SRCE
            pdaBena9814.getBena9814_Pnd_Last_Srce().reset();
            DbsUtil.callnat(Benn9814.class , getCurrentProcessState(), pdaBena9814.getBena9814(), pdaBenpda_M.getMsg_Info_Sub(), pdaBenpda_E.getError_Info_Sub());        //Natural: CALLNAT 'BENN9814' BENA9814 MSG-INFO-SUB ERROR-INFO-SUB
            if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
            pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                           //Natural: ASSIGN ERROR-INFO-SUB.##LAST-PROGRAM := *PROGRAM
            pnd_Stts_Found.getValue(pnd_I).setValue(pdaBena9814.getBena9814_Pnd_Found().getBoolean());                                                                    //Natural: ASSIGN #STTS-FOUND ( #I ) := BENA9814.#FOUND
            if (condition(pnd_I.equals(1)))                                                                                                                               //Natural: IF #I = 1
            {
                pnd_C_Last_Srce.setValue(pdaBena9814.getBena9814_Pnd_Last_Srce());                                                                                        //Natural: ASSIGN #C-LAST-SRCE := BENA9814.#LAST-SRCE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_I.equals(4)))                                                                                                                               //Natural: IF #I = 4
            {
                pnd_M_Last_Srce.setValue(pdaBena9814.getBena9814_Pnd_Last_Srce());                                                                                        //Natural: ASSIGN #M-LAST-SRCE := BENA9814.#LAST-SRCE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  WON't happen with MDO 'C'hange
        //*  REJECT CONDITION NOT FOUND YET
        //*  NOTE 9814 AND 9801 T/CIND NOW
        if (condition((pdaBena9801.getBena9801_Tiaa_Cref_Ind().greater(" ") && ((pdaBena9801.getBena9801_Rqstng_Systm().notEquals("CORR") && ! (pnd_Stts_Found.getValue("*").notEquals(false)))  //Natural: IF BENA9801.TIAA-CREF-IND GT ' ' AND ( ( BENA9801.RQSTNG-SYSTM NE 'CORR' AND NOT #STTS-FOUND ( * ) NE FALSE ) OR ( BENA9801.RQSTNG-SYSTM = 'CORR' AND NOT #STTS-FOUND ( 2:3 ) NE FALSE ) )
            || (pdaBena9801.getBena9801_Rqstng_Systm().equals("CORR") && ! (pnd_Stts_Found.getValue(2,":",3).notEquals(false)))))))
        {
            pdaBena9814.getBena9814_Pnd_Tiaa_Cref_Ind().setValue(" ");                                                                                                    //Natural: ASSIGN BENA9814.#TIAA-CREF-IND := ' '
            F2:                                                                                                                                                           //Natural: FOR #I = 1 TO 10
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(10)); pnd_I.nadd(1))
            {
                if (condition(pnd_Bene_Rcrd_Stts_Pnd_Bene_Rcrd_Stts_A1.getValue(pnd_I).equals(" ")))                                                                      //Natural: IF #BENE-RCRD-STTS-A1 ( #I ) = ' '
                {
                    if (true) break F2;                                                                                                                                   //Natural: ESCAPE BOTTOM ( F2. )
                }                                                                                                                                                         //Natural: END-IF
                pdaBena9814.getBena9814_Pnd_Stts().setValue(pnd_Bene_Rcrd_Stts_Pnd_Bene_Rcrd_Stts_A1.getValue(pnd_I));                                                    //Natural: ASSIGN BENA9814.#STTS := #BENE-RCRD-STTS-A1 ( #I )
                pdaBena9814.getBena9814_Pnd_Found().reset();                                                                                                              //Natural: RESET BENA9814.#FOUND
                DbsUtil.callnat(Benn9814.class , getCurrentProcessState(), pdaBena9814.getBena9814(), pdaBenpda_M.getMsg_Info_Sub(), pdaBenpda_E.getError_Info_Sub());    //Natural: CALLNAT 'BENN9814' BENA9814 MSG-INFO-SUB ERROR-INFO-SUB
                if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                       //Natural: ASSIGN ERROR-INFO-SUB.##LAST-PROGRAM := *PROGRAM
                pnd_Stts_Found.getValue(pnd_I).setValue(pdaBena9814.getBena9814_Pnd_Found().getBoolean());                                                                //Natural: ASSIGN #STTS-FOUND ( #I ) := BENA9814.#FOUND
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF CORR (MDO NO TCIND)
        if (condition(pdaBena9801.getBena9801_Chng_New_Issue_Ind().equals("C")))                                                                                          //Natural: IF BENA9801.CHNG-NEW-ISSUE-IND = 'C'
        {
            //*  & 'T' INCOMING AND 'C' ONLINE (OR VICE VERSA) WE DO
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
            //*  NOT NEED TO CHECK COUNTERPART FOR CHANGE -IF COUNTER
        }                                                                                                                                                                 //Natural: END-IF
        //*  PART IS 'unverified' IT WILL NOT AFFECT INCOMING CORR.
        //*  WON't happen with MDO 'C'hange
        //*  REJECT CONDITION NOT FOUND YET
        if (condition(pdaBena9801.getBena9801_Tiaa_Cref_Ind().greater(" ") && ! (pnd_Stts_Found.getValue("*").notEquals(false))))                                         //Natural: IF BENA9801.TIAA-CREF-IND GT ' ' AND NOT #STTS-FOUND ( * ) NE FALSE
        {
            //*  CHECK 'other' T/C-IND - CANNOT BE AWAITING VERIFICATION
            if (condition(pdaBena9801.getBena9801_Tiaa_Cref_Ind().equals("T")))                                                                                           //Natural: IF BENA9801.TIAA-CREF-IND = 'T'
            {
                pdaBena9814.getBena9814_Pnd_Tiaa_Cref_Ind().setValue("C");                                                                                                //Natural: ASSIGN BENA9814.#TIAA-CREF-IND := 'C'
                pnd_Other_Check_T_C_Ind.setValue("C");                                                                                                                    //Natural: ASSIGN #OTHER-CHECK-T-C-IND := 'C'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaBena9801.getBena9801_Tiaa_Cref_Ind().equals("C")))                                                                                           //Natural: IF BENA9801.TIAA-CREF-IND = 'C'
            {
                pdaBena9814.getBena9814_Pnd_Tiaa_Cref_Ind().setValue("T");                                                                                                //Natural: ASSIGN BENA9814.#TIAA-CREF-IND := 'T'
                pnd_Other_Check_T_C_Ind.setValue("T");                                                                                                                    //Natural: ASSIGN #OTHER-CHECK-T-C-IND := 'T'
                //*  'C' STATUS OK
            }                                                                                                                                                             //Natural: END-IF
            F3:                                                                                                                                                           //Natural: FOR #I = 2 TO 10
            for (pnd_I.setValue(2); condition(pnd_I.lessOrEqual(10)); pnd_I.nadd(1))
            {
                if (condition(pnd_Bene_Rcrd_Stts_Pnd_Bene_Rcrd_Stts_A1.getValue(pnd_I).equals(" ")))                                                                      //Natural: IF #BENE-RCRD-STTS-A1 ( #I ) = ' '
                {
                    if (true) break F3;                                                                                                                                   //Natural: ESCAPE BOTTOM ( F3. )
                }                                                                                                                                                         //Natural: END-IF
                pdaBena9814.getBena9814_Pnd_Stts().setValue(pnd_Bene_Rcrd_Stts_Pnd_Bene_Rcrd_Stts_A1.getValue(pnd_I));                                                    //Natural: ASSIGN BENA9814.#STTS := #BENE-RCRD-STTS-A1 ( #I )
                pdaBena9814.getBena9814_Pnd_Found().reset();                                                                                                              //Natural: RESET BENA9814.#FOUND
                DbsUtil.callnat(Benn9814.class , getCurrentProcessState(), pdaBena9814.getBena9814(), pdaBenpda_M.getMsg_Info_Sub(), pdaBenpda_E.getError_Info_Sub());    //Natural: CALLNAT 'BENN9814' BENA9814 MSG-INFO-SUB ERROR-INFO-SUB
                if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                       //Natural: ASSIGN ERROR-INFO-SUB.##LAST-PROGRAM := *PROGRAM
                pnd_Stts_Found.getValue(pnd_I).setValue(pdaBena9814.getBena9814_Pnd_Found().getBoolean());                                                                //Natural: ASSIGN #STTS-FOUND ( #I ) := BENA9814.#FOUND
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  GET-EXISTING-BENE-RECORDS-STATUSES
    }
    private void sub_Check_If_Exists_On_Interface_File() throws Exception                                                                                                 //Natural: CHECK-IF-EXISTS-ON-INTERFACE-FILE
    {
        if (BLNatReinput.isReinput()) return;

        pdaBena9701.getBena9701_Pnd_Pin().setValue(pdaBena9801.getBena9801_Pin());                                                                                        //Natural: ASSIGN BENA9701.#PIN := BENA9801.PIN
        pdaBena9701.getBena9701_Pnd_Tiaa_Cntrct().setValue(pdaBena9801.getBena9801_Tiaa_Cntrct());                                                                        //Natural: ASSIGN BENA9701.#TIAA-CNTRCT := BENA9801.TIAA-CNTRCT
        pdaBena9701.getBena9701_Pnd_Tiaa_Cref_Ind().setValue(pdaBena9801.getBena9801_Tiaa_Cref_Ind());                                                                    //Natural: ASSIGN BENA9701.#TIAA-CREF-IND := BENA9801.TIAA-CREF-IND
        pdaBena9701.getBena9701_Pnd_Rcrd_Crtd_For_Bsnss_Dte().setValue(pdaBena9801.getBena9801_Rcrd_Crtd_For_Bsnss_Dte());                                                //Natural: ASSIGN BENA9701.#RCRD-CRTD-FOR-BSNSS-DTE := BENA9801.RCRD-CRTD-FOR-BSNSS-DTE
        DbsUtil.callnat(Benn9701.class , getCurrentProcessState(), pdaBena9701.getBena9701(), pdaBenpda_M.getMsg_Info_Sub());                                             //Natural: CALLNAT 'BENN9701' BENA9701 MSG-INFO-SUB
        if (condition(Global.isEscape())) return;
        //*  CHECK-IF-EXISTS-ON-INTERFACE-FILE
    }

    //
}
