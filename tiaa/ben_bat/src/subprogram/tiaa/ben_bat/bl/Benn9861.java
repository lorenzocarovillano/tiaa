/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:06:35 PM
**        * FROM NATURAL SUBPROGRAM : Benn9861
************************************************************
**        * FILE NAME            : Benn9861.java
**        * CLASS NAME           : Benn9861
**        * INSTANCE NAME        : Benn9861
************************************************************
************************************************************************
* PROGRAM  : BENN9861
* SYSTEM   : BENEFICIARY SYSTEM INTERFACE SAME AS
* TITLE    : CHECK COR FOR DOD - CLONED FROM BENN8171
* GENERATED: OCTOBER, 26, 2001
************************************************************************
*    DATE      USERID                   DESCRIPTION
* MM/DD/YYYY  XXXXXXXX  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
* 04/01/2017  DURAND    PIN EXPANSION N7 TO N12
* 2015/09/11  DURAND    CONVERT COR/NAS FILES TO PROCOBOL CALLS
************************************************************************

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benn9861 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaBena9861 pdaBena9861;
    private PdaBenpda_M pdaBenpda_M;
    private PdaBenpda_E pdaBenpda_E;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Psgm001_Common_Area;

    private DbsGroup pnd_Psgm001_Common_Area_Pnd_Psgm001_Input;
    private DbsField pnd_Psgm001_Common_Area_Pnd_In_Function;

    private DbsGroup pnd_Psgm001_Common_Area_Pnd_In_Data;
    private DbsField pnd_Psgm001_Common_Area_Pnd_In_Addr_Usg_Typ;
    private DbsField pnd_Psgm001_Common_Area_Pnd_In_Type;
    private DbsField pnd_Psgm001_Common_Area_Pnd_In_Pin_Ssn;

    private DbsGroup pnd_Psgm001_Common_Area_Pnd_Rt_Area;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Ret_Code;

    private DbsGroup pnd_Psgm001_Common_Area_Pnd_Rt_Record;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Input_String;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Out_Pin;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Out_Ssn;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Inact_Date;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Birth_Date;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Dec_Date;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Gender_Code;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Pref;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Last_Name;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_First_Name;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Second_Name;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Suffix_Desc;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Person_Org_Cd;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Addr_1;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Addr_2;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Addr_3;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Addr_4;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Addr_5;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_City;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Postal_Code;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Iso_Code;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Country;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Irs_Code;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Irs_Country_Nm;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_State_Nm;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Geo_Code;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Addr_Type_Cd;
    private DbsField pnd_Psgm001_Common_Area_Pnd_Rt_Rcrd_Status;
    private DbsField pls_Trace;
    private int psgm001ReturnCode;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaBena9861 = new PdaBena9861(parameters);
        pdaBenpda_M = new PdaBenpda_M(parameters);
        pdaBenpda_E = new PdaBenpda_E(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Psgm001_Common_Area = localVariables.newGroupInRecord("pnd_Psgm001_Common_Area", "#PSGM001-COMMON-AREA");

        pnd_Psgm001_Common_Area_Pnd_Psgm001_Input = pnd_Psgm001_Common_Area.newGroupInGroup("pnd_Psgm001_Common_Area_Pnd_Psgm001_Input", "#PSGM001-INPUT");
        pnd_Psgm001_Common_Area_Pnd_In_Function = pnd_Psgm001_Common_Area_Pnd_Psgm001_Input.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_In_Function", 
            "#IN-FUNCTION", FieldType.STRING, 2);

        pnd_Psgm001_Common_Area_Pnd_In_Data = pnd_Psgm001_Common_Area_Pnd_Psgm001_Input.newGroupInGroup("pnd_Psgm001_Common_Area_Pnd_In_Data", "#IN-DATA");
        pnd_Psgm001_Common_Area_Pnd_In_Addr_Usg_Typ = pnd_Psgm001_Common_Area_Pnd_In_Data.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_In_Addr_Usg_Typ", 
            "#IN-ADDR-USG-TYP", FieldType.STRING, 2);
        pnd_Psgm001_Common_Area_Pnd_In_Type = pnd_Psgm001_Common_Area_Pnd_In_Data.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_In_Type", "#IN-TYPE", FieldType.STRING, 
            3);
        pnd_Psgm001_Common_Area_Pnd_In_Pin_Ssn = pnd_Psgm001_Common_Area_Pnd_In_Data.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_In_Pin_Ssn", "#IN-PIN-SSN", 
            FieldType.STRING, 15);

        pnd_Psgm001_Common_Area_Pnd_Rt_Area = pnd_Psgm001_Common_Area.newGroupInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Area", "#RT-AREA");
        pnd_Psgm001_Common_Area_Pnd_Rt_Ret_Code = pnd_Psgm001_Common_Area_Pnd_Rt_Area.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Ret_Code", "#RT-RET-CODE", 
            FieldType.STRING, 2);

        pnd_Psgm001_Common_Area_Pnd_Rt_Record = pnd_Psgm001_Common_Area_Pnd_Rt_Area.newGroupInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Record", "#RT-RECORD");
        pnd_Psgm001_Common_Area_Pnd_Rt_Input_String = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Input_String", 
            "#RT-INPUT-STRING", FieldType.STRING, 100);
        pnd_Psgm001_Common_Area_Pnd_Rt_Out_Pin = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Out_Pin", "#RT-OUT-PIN", 
            FieldType.STRING, 25);
        pnd_Psgm001_Common_Area_Pnd_Rt_Out_Ssn = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Out_Ssn", "#RT-OUT-SSN", 
            FieldType.STRING, 25);
        pnd_Psgm001_Common_Area_Pnd_Rt_Inact_Date = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Inact_Date", 
            "#RT-INACT-DATE", FieldType.STRING, 8);
        pnd_Psgm001_Common_Area_Pnd_Rt_Birth_Date = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Birth_Date", 
            "#RT-BIRTH-DATE", FieldType.STRING, 8);
        pnd_Psgm001_Common_Area_Pnd_Rt_Dec_Date = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Dec_Date", "#RT-DEC-DATE", 
            FieldType.STRING, 8);
        pnd_Psgm001_Common_Area_Pnd_Rt_Gender_Code = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Gender_Code", 
            "#RT-GENDER-CODE", FieldType.STRING, 1);
        pnd_Psgm001_Common_Area_Pnd_Rt_Pref = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Pref", "#RT-PREF", 
            FieldType.STRING, 120);
        pnd_Psgm001_Common_Area_Pnd_Rt_Last_Name = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Last_Name", "#RT-LAST-NAME", 
            FieldType.STRING, 35);
        pnd_Psgm001_Common_Area_Pnd_Rt_First_Name = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_First_Name", 
            "#RT-FIRST-NAME", FieldType.STRING, 30);
        pnd_Psgm001_Common_Area_Pnd_Rt_Second_Name = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Second_Name", 
            "#RT-SECOND-NAME", FieldType.STRING, 30);
        pnd_Psgm001_Common_Area_Pnd_Rt_Suffix_Desc = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Suffix_Desc", 
            "#RT-SUFFIX-DESC", FieldType.STRING, 20);
        pnd_Psgm001_Common_Area_Pnd_Rt_Person_Org_Cd = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Person_Org_Cd", 
            "#RT-PERSON-ORG-CD", FieldType.STRING, 1);
        pnd_Psgm001_Common_Area_Pnd_Rt_Addr_1 = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Addr_1", "#RT-ADDR-1", 
            FieldType.STRING, 100);
        pnd_Psgm001_Common_Area_Pnd_Rt_Addr_2 = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Addr_2", "#RT-ADDR-2", 
            FieldType.STRING, 100);
        pnd_Psgm001_Common_Area_Pnd_Rt_Addr_3 = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Addr_3", "#RT-ADDR-3", 
            FieldType.STRING, 100);
        pnd_Psgm001_Common_Area_Pnd_Rt_Addr_4 = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Addr_4", "#RT-ADDR-4", 
            FieldType.STRING, 250);
        pnd_Psgm001_Common_Area_Pnd_Rt_Addr_5 = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Addr_5", "#RT-ADDR-5", 
            FieldType.STRING, 250);
        pnd_Psgm001_Common_Area_Pnd_Rt_City = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_City", "#RT-CITY", 
            FieldType.STRING, 50);
        pnd_Psgm001_Common_Area_Pnd_Rt_Postal_Code = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Postal_Code", 
            "#RT-POSTAL-CODE", FieldType.STRING, 20);
        pnd_Psgm001_Common_Area_Pnd_Rt_Iso_Code = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Iso_Code", "#RT-ISO-CODE", 
            FieldType.STRING, 20);
        pnd_Psgm001_Common_Area_Pnd_Rt_Country = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Country", "#RT-COUNTRY", 
            FieldType.STRING, 120);
        pnd_Psgm001_Common_Area_Pnd_Rt_Irs_Code = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Irs_Code", "#RT-IRS-CODE", 
            FieldType.STRING, 20);
        pnd_Psgm001_Common_Area_Pnd_Rt_Irs_Country_Nm = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Irs_Country_Nm", 
            "#RT-IRS-COUNTRY-NM", FieldType.STRING, 120);
        pnd_Psgm001_Common_Area_Pnd_Rt_State_Nm = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_State_Nm", "#RT-STATE-NM", 
            FieldType.STRING, 120);
        pnd_Psgm001_Common_Area_Pnd_Rt_Geo_Code = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Geo_Code", "#RT-GEO-CODE", 
            FieldType.STRING, 2);
        pnd_Psgm001_Common_Area_Pnd_Rt_Addr_Type_Cd = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Addr_Type_Cd", 
            "#RT-ADDR-TYPE-CD", FieldType.STRING, 1);
        pnd_Psgm001_Common_Area_Pnd_Rt_Rcrd_Status = pnd_Psgm001_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm001_Common_Area_Pnd_Rt_Rcrd_Status", 
            "#RT-RCRD-STATUS", FieldType.STRING, 1);
        pls_Trace = WsIndependent.getInstance().newFieldInRecord("pls_Trace", "+TRACE", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Benn9861() throws Exception
    {
        super("Benn9861");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ERROR-INFO-SUB.##LAST-PROGRAM := *PROGRAM
        pdaBena9861.getBena9861_Pnd_Dte_Of_Dth().reset();                                                                                                                 //Natural: RESET BENA9861.#DTE-OF-DTH BENA9861.#DTE-OF-DTH-FOUND ##MSG ##RETURN-CODE
        pdaBena9861.getBena9861_Pnd_Dte_Of_Dth_Found().reset();
        pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().reset();
        pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().reset();
        //*  KCD 3/2016
        pnd_Psgm001_Common_Area.reset();                                                                                                                                  //Natural: RESET #PSGM001-COMMON-AREA
        pnd_Psgm001_Common_Area_Pnd_In_Function.setValue("PR");                                                                                                           //Natural: ASSIGN #IN-FUNCTION := 'PR'
        pnd_Psgm001_Common_Area_Pnd_In_Addr_Usg_Typ.setValue("RS");                                                                                                       //Natural: ASSIGN #IN-ADDR-USG-TYP := 'RS'
        pnd_Psgm001_Common_Area_Pnd_In_Type.setValue("PIN");                                                                                                              //Natural: ASSIGN #IN-TYPE := 'PIN'
        pnd_Psgm001_Common_Area_Pnd_In_Pin_Ssn.setValue(pdaBena9861.getBena9861_Pnd_Pin());                                                                               //Natural: ASSIGN #IN-PIN-SSN := BENA9861.#PIN
        psgm001ReturnCode = DbsUtil.callExternalProgram("PSGM001",pnd_Psgm001_Common_Area_Pnd_In_Function,pnd_Psgm001_Common_Area_Pnd_In_Addr_Usg_Typ,                    //Natural: CALL 'PSGM001' #PSGM001-COMMON-AREA
            pnd_Psgm001_Common_Area_Pnd_In_Type,pnd_Psgm001_Common_Area_Pnd_In_Pin_Ssn,pnd_Psgm001_Common_Area_Pnd_Rt_Ret_Code,pnd_Psgm001_Common_Area_Pnd_Rt_Input_String,
            pnd_Psgm001_Common_Area_Pnd_Rt_Out_Pin,pnd_Psgm001_Common_Area_Pnd_Rt_Out_Ssn,pnd_Psgm001_Common_Area_Pnd_Rt_Inact_Date,pnd_Psgm001_Common_Area_Pnd_Rt_Birth_Date,
            pnd_Psgm001_Common_Area_Pnd_Rt_Dec_Date,pnd_Psgm001_Common_Area_Pnd_Rt_Gender_Code,pnd_Psgm001_Common_Area_Pnd_Rt_Pref,pnd_Psgm001_Common_Area_Pnd_Rt_Last_Name,
            pnd_Psgm001_Common_Area_Pnd_Rt_First_Name,pnd_Psgm001_Common_Area_Pnd_Rt_Second_Name,pnd_Psgm001_Common_Area_Pnd_Rt_Suffix_Desc,pnd_Psgm001_Common_Area_Pnd_Rt_Person_Org_Cd,
            pnd_Psgm001_Common_Area_Pnd_Rt_Addr_1,pnd_Psgm001_Common_Area_Pnd_Rt_Addr_2,pnd_Psgm001_Common_Area_Pnd_Rt_Addr_3,pnd_Psgm001_Common_Area_Pnd_Rt_Addr_4,
            pnd_Psgm001_Common_Area_Pnd_Rt_Addr_5,pnd_Psgm001_Common_Area_Pnd_Rt_City,pnd_Psgm001_Common_Area_Pnd_Rt_Postal_Code,pnd_Psgm001_Common_Area_Pnd_Rt_Iso_Code,
            pnd_Psgm001_Common_Area_Pnd_Rt_Country,pnd_Psgm001_Common_Area_Pnd_Rt_Irs_Code,pnd_Psgm001_Common_Area_Pnd_Rt_Irs_Country_Nm,pnd_Psgm001_Common_Area_Pnd_Rt_State_Nm,
            pnd_Psgm001_Common_Area_Pnd_Rt_Geo_Code,pnd_Psgm001_Common_Area_Pnd_Rt_Addr_Type_Cd,pnd_Psgm001_Common_Area_Pnd_Rt_Rcrd_Status);
        if (condition(pnd_Psgm001_Common_Area_Pnd_Rt_Rcrd_Status.equals("Y")))                                                                                            //Natural: IF #RT-RCRD-STATUS = 'Y'
        {
            pdaBena9861.getBena9861_Pnd_Pin_Found().setValue(true);                                                                                                       //Natural: ASSIGN #PIN-FOUND := TRUE
            if (condition(DbsUtil.maskMatches(pnd_Psgm001_Common_Area_Pnd_Rt_Dec_Date,"YYYYMMDD")))                                                                       //Natural: IF #RT-DEC-DATE = MASK ( YYYYMMDD )
            {
                pdaBena9861.getBena9861_Pnd_Dte_Of_Dth().setValue(pnd_Psgm001_Common_Area_Pnd_Rt_Dec_Date);                                                               //Natural: ASSIGN BENA9861.#DTE-OF-DTH := #RT-DEC-DATE
                pdaBena9861.getBena9861_Pnd_Dte_Of_Dth_Found().setValue(true);                                                                                            //Natural: ASSIGN BENA9861.#DTE-OF-DTH-FOUND := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("No MDM record found for PIN:", pdaBena9861.getBena9861_Pnd_Pin()));                      //Natural: COMPRESS 'No MDM record found for PIN:' BENA9861.#PIN INTO ##MSG
            pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                              //Natural: ASSIGN ##RETURN-CODE := 'E'
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
