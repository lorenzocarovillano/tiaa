/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:05:56 PM
**        * FROM NATURAL SUBPROGRAM : Benn9811
************************************************************
**        * FILE NAME            : Benn9811.java
**        * CLASS NAME           : Benn9811
**        * INSTANCE NAME        : Benn9811
************************************************************
************************************************************************
* PROGRAM  : BENN9811
* SYSTEM   : BENEFICIARY-SYSTEM
* TITLE    : UPDATE STATUS
* GENERATED: JULY 1, 1999
************************************************************************
*    DATE      USERID                   DESCRIPTION
* MM/DD/YYYY  XXXXXXXX  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
* 04/01/2017  DURAND    PIN EXPANSION N7 TO N12
************************************************************************

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benn9811 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaBena9811 pdaBena9811;
    private PdaBenpda_M pdaBenpda_M;
    private PdaBenpda_E pdaBenpda_E;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_bci;
    private DbsField bci_Intrfce_Stts;
    private DbsField bci_Rcrd_Crtd_For_Bsnss_Dte;
    private DbsField bci_Rcrd_Crtd_Dte;
    private DbsField bci_Rcrd_Crtd_Tme;
    private DbsField bci_Intrfcng_Systm;
    private DbsField bci_Rqstng_Systm;
    private DbsField bci_Rcrd_Last_Updt_Dte;
    private DbsField bci_Rcrd_Last_Updt_Tme;
    private DbsField bci_Rcrd_Last_Updt_Userid;
    private DbsField bci_Last_Prcssd_Bsnss_Dte;
    private DbsField bci_Intrfce_Cmpltd_Dte;
    private DbsField bci_Intrfce_Cmpltd_Tme;
    private DbsField bci_Dflt_To_Estate_Ind;
    private DbsField bci_Illgble_Ind;
    private DbsField bci_More_Than_Five_Benes_Ind;
    private DbsField bci_Intrfce_Mgrtn_Ind;
    private DbsField bci_More_Than_Thirty_Benes_Ind;
    private DbsField bci_Pin_Tiaa_Cntrct;

    private DbsGroup bci_Error_Table;
    private DbsField bci_Error_Txt;
    private DbsField bci_Error_Cde;
    private DbsField bci_Error_Dte;
    private DbsField bci_Error_Tme;
    private DbsField bci_Error_Pgm;
    private DbsField bci_Fldr_Log_Dte_Tme;
    private DbsField bci_Last_Dsgntn_Srce;
    private DbsField bci_Last_Dsgntn_System;
    private DbsField bci_Last_Dsgntn_Userid;
    private DbsField bci_Last_Dsgntn_Dte;
    private DbsField bci_Last_Dsgntn_Tme;
    private DbsField bci_Tiaa_Cref_Chng_Dte;
    private DbsField bci_Tiaa_Cref_Chng_Tme;
    private DbsField bci_Chng_Pwr_Atty;
    private DbsField bci_Cref_Cntrct;
    private DbsField bci_Tiaa_Cref_Ind;
    private DbsField bci_Cntrct_Type;
    private DbsField bci_Eff_Dte;
    private DbsField bci_Mos_Ind;
    private DbsField bci_Irrvcble_Ind;
    private DbsField bci_Pymnt_Chld_Dcsd_Ind;
    private DbsField bci_Exempt_Spouse_Rights;
    private DbsField bci_Spouse_Waived_Bnfts;
    private DbsField bci_Chng_New_Issue_Ind;
    private DbsField bci_Same_As_Ind;

    private DataAccessProgramView vw_bdi;
    private DbsField bdi_Intrfce_Stts;
    private DbsField bdi_Tiaa_Cref_Ind;
    private DbsField bdi_Seq_Nmbr;
    private DbsField bdi_Rcrd_Crtd_For_Bsnss_Dte;
    private DbsField bdi_Rcrd_Last_Updt_Dte;
    private DbsField bdi_Rcrd_Last_Updt_Tme;
    private DbsField bdi_Rcrd_Last_Updt_Userid;
    private DbsField bdi_Pin_Tiaa_Cntrct;
    private DbsField bdi_Bene_Type;
    private DbsField bdi_Rltn_Cde;
    private DbsField bdi_Bene_Name1;
    private DbsField bdi_Bene_Name2;
    private DbsField bdi_Rltn_Free_Txt;
    private DbsField bdi_Dte_Birth_Trust;
    private DbsField bdi_Ssn_Cde;
    private DbsField bdi_Ssn;
    private DbsField bdi_Prcnt_Frctn_Ind;
    private DbsField bdi_Share_Prcnt;
    private DbsField bdi_Share_Nmrtr;
    private DbsField bdi_Share_Dnmntr;
    private DbsField bdi_Irrvcble_Ind;
    private DbsField bdi_Sttlmnt_Rstrctn;
    private DbsField bdi_Spcl_Txt1;
    private DbsField bdi_Spcl_Txt2;
    private DbsField bdi_Spcl_Txt3;
    private DbsField bdi_Dflt_Estate;
    private DbsField bdi_Mdo_Calc_Bene;
    private DbsField bdi_Bene_Addr1;
    private DbsField bdi_Bene_Addr2;
    private DbsField bdi_Bene_Addr3_City;
    private DbsField bdi_Bene_State;
    private DbsField bdi_Bene_Zip;
    private DbsField bdi_Bene_Phone;
    private DbsField bdi_Bene_Gender;
    private DbsField bdi_Bene_Country;

    private DataAccessProgramView vw_bmi;
    private DbsField bmi_Intrfce_Stts;
    private DbsField bmi_Rcrd_Crtd_For_Bsnss_Dte;
    private DbsField bmi_Tiaa_Cref_Ind;
    private DbsField bmi_Pin_Tiaa_Cntrct;
    private DbsField bmi_Rcrd_Crtd_Dte;
    private DbsField bmi_Rcrd_Crtd_Tme;
    private DbsField bmi_Rcrd_Last_Updt_Dte;
    private DbsField bmi_Rcrd_Last_Updt_Tme;
    private DbsField bmi_Rcrd_Last_Updt_Userid;
    private DbsField bmi_Count_Castmos_Txt_Group;

    private DbsGroup bmi_Mos_Txt_Group;
    private DbsField bmi_Mos_Txt;
    private DbsField pnd_I1;
    private DbsField pnd_J1;
    private DbsField pnd_Intrfce_Super_2;

    private DbsGroup pnd_Intrfce_Super_2__R_Field_1;
    private DbsField pnd_Intrfce_Super_2_Pnd_Intrfce_Stts;
    private DbsField pnd_Intrfce_Super_2_Pnd_Rcrd_Crtd_For_Bsnss_Dte;
    private DbsField pnd_Intrfce_Super_2_Pnd_Pin_Tiaa_Cntrct;
    private DbsField pnd_Intrfce_Super_2_Pnd_Tiaa_Cref_Ind;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaBena9811 = new PdaBena9811(parameters);
        pdaBenpda_M = new PdaBenpda_M(parameters);
        pdaBenpda_E = new PdaBenpda_E(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_bci = new DataAccessProgramView(new NameInfo("vw_bci", "BCI"), "BENE_CONTRACT_INTERFACE_12", "BENE_CONT_INTFCE", DdmPeriodicGroups.getInstance().getGroups("BENE_CONTRACT_INTERFACE_12"));
        bci_Intrfce_Stts = vw_bci.getRecord().newFieldInGroup("bci_Intrfce_Stts", "INTRFCE-STTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, "INTRFCE_STTS");
        bci_Intrfce_Stts.setDdmHeader("INTRFCE/STATUS");
        bci_Rcrd_Crtd_For_Bsnss_Dte = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Crtd_For_Bsnss_Dte", "RCRD-CRTD-FOR-BSNSS-DTE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "RCRD_CRTD_FOR_BSNSS_DTE");
        bci_Rcrd_Crtd_For_Bsnss_Dte.setDdmHeader("RCRD CRTD/FOR/BSNSS DTE");
        bci_Rcrd_Crtd_Dte = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Crtd_Dte", "RCRD-CRTD-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_CRTD_DTE");
        bci_Rcrd_Crtd_Dte.setDdmHeader("RECORD/CREATED/DATE");
        bci_Rcrd_Crtd_Tme = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Crtd_Tme", "RCRD-CRTD-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "RCRD_CRTD_TME");
        bci_Rcrd_Crtd_Tme.setDdmHeader("RECORD/CREATED/TIME");
        bci_Intrfcng_Systm = vw_bci.getRecord().newFieldInGroup("bci_Intrfcng_Systm", "INTRFCNG-SYSTM", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "INTRFCNG_SYSTM");
        bci_Intrfcng_Systm.setDdmHeader("INTRFCNG/SYSTEM");
        bci_Rqstng_Systm = vw_bci.getRecord().newFieldInGroup("bci_Rqstng_Systm", "RQSTNG-SYSTM", FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQSTNG_SYSTM");
        bci_Rqstng_Systm.setDdmHeader("RQSTNG/SYSTEM");
        bci_Rcrd_Last_Updt_Dte = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Last_Updt_Dte", "RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_DTE");
        bci_Rcrd_Last_Updt_Dte.setDdmHeader("RECORD/LAST UPDT/DATE");
        bci_Rcrd_Last_Updt_Tme = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Last_Updt_Tme", "RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_TME");
        bci_Rcrd_Last_Updt_Tme.setDdmHeader("RECORD/LAST UPDT/TIME");
        bci_Rcrd_Last_Updt_Userid = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Last_Updt_Userid", "RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_USERID");
        bci_Rcrd_Last_Updt_Userid.setDdmHeader("RECORD/LAST UPDT/USER ID");
        bci_Last_Prcssd_Bsnss_Dte = vw_bci.getRecord().newFieldInGroup("bci_Last_Prcssd_Bsnss_Dte", "LAST-PRCSSD-BSNSS-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "LAST_PRCSSD_BSNSS_DTE");
        bci_Last_Prcssd_Bsnss_Dte.setDdmHeader("LAST PRCSSD/BUSINESS/DATE");
        bci_Intrfce_Cmpltd_Dte = vw_bci.getRecord().newFieldInGroup("bci_Intrfce_Cmpltd_Dte", "INTRFCE-CMPLTD-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "INTRFCE_CMPLTD_DTE");
        bci_Intrfce_Cmpltd_Dte.setDdmHeader("INTFCE/CMPLTD/DATE");
        bci_Intrfce_Cmpltd_Tme = vw_bci.getRecord().newFieldInGroup("bci_Intrfce_Cmpltd_Tme", "INTRFCE-CMPLTD-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "INTRFCE_CMPLTD_TME");
        bci_Intrfce_Cmpltd_Tme.setDdmHeader("INTRFCE/CMPLTD/TME");
        bci_Dflt_To_Estate_Ind = vw_bci.getRecord().newFieldInGroup("bci_Dflt_To_Estate_Ind", "DFLT-TO-ESTATE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "DFLT_TO_ESTATE_IND");
        bci_Dflt_To_Estate_Ind.setDdmHeader("DEFAULT/TO/ESTATE");
        bci_Illgble_Ind = vw_bci.getRecord().newFieldInGroup("bci_Illgble_Ind", "ILLGBLE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "ILLGBLE_IND");
        bci_Illgble_Ind.setDdmHeader("ILLGBLE/IND");
        bci_More_Than_Five_Benes_Ind = vw_bci.getRecord().newFieldInGroup("bci_More_Than_Five_Benes_Ind", "MORE-THAN-FIVE-BENES-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "MORE_THAN_FIVE_BENES_IND");
        bci_More_Than_Five_Benes_Ind.setDdmHeader("MORE/THAN 5/BENES");
        bci_Intrfce_Mgrtn_Ind = vw_bci.getRecord().newFieldInGroup("bci_Intrfce_Mgrtn_Ind", "INTRFCE-MGRTN-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "INTRFCE_MGRTN_IND");
        bci_Intrfce_Mgrtn_Ind.setDdmHeader("INTRFCE/MGRTN/IND");
        bci_More_Than_Thirty_Benes_Ind = vw_bci.getRecord().newFieldInGroup("bci_More_Than_Thirty_Benes_Ind", "MORE-THAN-THIRTY-BENES-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "MORE_THAN_THIRTY_BENES_IND");
        bci_More_Than_Thirty_Benes_Ind.setDdmHeader("MORE/THAN 30/BENES");
        bci_Pin_Tiaa_Cntrct = vw_bci.getRecord().newFieldInGroup("bci_Pin_Tiaa_Cntrct", "PIN-TIAA-CNTRCT", FieldType.STRING, 22, RepeatingFieldStrategy.None, 
            "PIN_TIAA_CNTRCT");
        bci_Pin_Tiaa_Cntrct.setDdmHeader("PIN/TIAA/CONTRACT");

        bci_Error_Table = vw_bci.getRecord().newGroupInGroup("bci_Error_Table", "ERROR-TABLE", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Error_Txt = bci_Error_Table.newFieldArrayInGroup("bci_Error_Txt", "ERROR-TXT", FieldType.STRING, 72, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ERROR_TXT", "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Error_Txt.setDdmHeader("ERROR/TEXT");
        bci_Error_Cde = bci_Error_Table.newFieldArrayInGroup("bci_Error_Cde", "ERROR-CDE", FieldType.STRING, 5, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ERROR_CDE", "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Error_Cde.setDdmHeader("ERROR/CODE");
        bci_Error_Dte = bci_Error_Table.newFieldArrayInGroup("bci_Error_Dte", "ERROR-DTE", FieldType.STRING, 8, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ERROR_DTE", "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Error_Dte.setDdmHeader("ERROR/DATE");
        bci_Error_Tme = bci_Error_Table.newFieldArrayInGroup("bci_Error_Tme", "ERROR-TME", FieldType.STRING, 7, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ERROR_TME", "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Error_Tme.setDdmHeader("ERROR/TIME");
        bci_Error_Pgm = bci_Error_Table.newFieldArrayInGroup("bci_Error_Pgm", "ERROR-PGM", FieldType.STRING, 8, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ERROR_PGM", "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Fldr_Log_Dte_Tme = vw_bci.getRecord().newFieldInGroup("bci_Fldr_Log_Dte_Tme", "FLDR-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "FLDR_LOG_DTE_TME");
        bci_Fldr_Log_Dte_Tme.setDdmHeader("FOLDER/LOG DATE/TIME");
        bci_Last_Dsgntn_Srce = vw_bci.getRecord().newFieldInGroup("bci_Last_Dsgntn_Srce", "LAST-DSGNTN-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "LAST_DSGNTN_SRCE");
        bci_Last_Dsgntn_Srce.setDdmHeader("LAST/DSGNTN/SOURCE");
        bci_Last_Dsgntn_System = vw_bci.getRecord().newFieldInGroup("bci_Last_Dsgntn_System", "LAST-DSGNTN-SYSTEM", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "LAST_DSGNTN_SYSTEM");
        bci_Last_Dsgntn_System.setDdmHeader("LAST/DSGNTN/SYSTEM");
        bci_Last_Dsgntn_Userid = vw_bci.getRecord().newFieldInGroup("bci_Last_Dsgntn_Userid", "LAST-DSGNTN-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "LAST_DSGNTN_USERID");
        bci_Last_Dsgntn_Userid.setDdmHeader("LAST/DSGNTN/USERID");
        bci_Last_Dsgntn_Dte = vw_bci.getRecord().newFieldInGroup("bci_Last_Dsgntn_Dte", "LAST-DSGNTN-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "LAST_DSGNTN_DTE");
        bci_Last_Dsgntn_Dte.setDdmHeader("LAST/DSGNTN/DATE");
        bci_Last_Dsgntn_Tme = vw_bci.getRecord().newFieldInGroup("bci_Last_Dsgntn_Tme", "LAST-DSGNTN-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "LAST_DSGNTN_TME");
        bci_Last_Dsgntn_Tme.setDdmHeader("LAST/DSGNTN/TIME");
        bci_Tiaa_Cref_Chng_Dte = vw_bci.getRecord().newFieldInGroup("bci_Tiaa_Cref_Chng_Dte", "TIAA-CREF-CHNG-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIAA_CREF_CHNG_DTE");
        bci_Tiaa_Cref_Chng_Dte.setDdmHeader("TIAA CREF/CHANGE/DATE");
        bci_Tiaa_Cref_Chng_Tme = vw_bci.getRecord().newFieldInGroup("bci_Tiaa_Cref_Chng_Tme", "TIAA-CREF-CHNG-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "TIAA_CREF_CHNG_TME");
        bci_Tiaa_Cref_Chng_Tme.setDdmHeader("TIAA CREF/CHANGE/TIME");
        bci_Chng_Pwr_Atty = vw_bci.getRecord().newFieldInGroup("bci_Chng_Pwr_Atty", "CHNG-PWR-ATTY", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CHNG_PWR_ATTY");
        bci_Chng_Pwr_Atty.setDdmHeader("CHANGE/POWER/ATTNY");
        bci_Cref_Cntrct = vw_bci.getRecord().newFieldInGroup("bci_Cref_Cntrct", "CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT");
        bci_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bci_Tiaa_Cref_Ind = vw_bci.getRecord().newFieldInGroup("bci_Tiaa_Cref_Ind", "TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIAA_CREF_IND");
        bci_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bci_Cntrct_Type = vw_bci.getRecord().newFieldInGroup("bci_Cntrct_Type", "CNTRCT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_TYPE");
        bci_Cntrct_Type.setDdmHeader("CNTRCT/TYPE");
        bci_Eff_Dte = vw_bci.getRecord().newFieldInGroup("bci_Eff_Dte", "EFF-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "EFF_DTE");
        bci_Eff_Dte.setDdmHeader("EFFECTIVE/DATE");
        bci_Mos_Ind = vw_bci.getRecord().newFieldInGroup("bci_Mos_Ind", "MOS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "MOS_IND");
        bci_Mos_Ind.setDdmHeader("MOS/IND");
        bci_Irrvcble_Ind = vw_bci.getRecord().newFieldInGroup("bci_Irrvcble_Ind", "IRRVCBLE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "IRRVCBLE_IND");
        bci_Irrvcble_Ind.setDdmHeader("IRVCBL/IND");
        bci_Pymnt_Chld_Dcsd_Ind = vw_bci.getRecord().newFieldInGroup("bci_Pymnt_Chld_Dcsd_Ind", "PYMNT-CHLD-DCSD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PYMNT_CHLD_DCSD_IND");
        bci_Pymnt_Chld_Dcsd_Ind.setDdmHeader("PYMNT/CHILD/DCSD");
        bci_Exempt_Spouse_Rights = vw_bci.getRecord().newFieldInGroup("bci_Exempt_Spouse_Rights", "EXEMPT-SPOUSE-RIGHTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "EXEMPT_SPOUSE_RIGHTS");
        bci_Exempt_Spouse_Rights.setDdmHeader("EXEMPT/SPOUSAL/RIGHTS");
        bci_Spouse_Waived_Bnfts = vw_bci.getRecord().newFieldInGroup("bci_Spouse_Waived_Bnfts", "SPOUSE-WAIVED-BNFTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "SPOUSE_WAIVED_BNFTS");
        bci_Spouse_Waived_Bnfts.setDdmHeader("SPOUSE/WAIVED/BENEFITS");
        bci_Chng_New_Issue_Ind = vw_bci.getRecord().newFieldInGroup("bci_Chng_New_Issue_Ind", "CHNG-NEW-ISSUE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CHNG_NEW_ISSUE_IND");
        bci_Chng_New_Issue_Ind.setDdmHeader("CHANGE/NEW ISSUE/IND");
        bci_Same_As_Ind = vw_bci.getRecord().newFieldInGroup("bci_Same_As_Ind", "SAME-AS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "SAME_AS_IND");
        registerRecord(vw_bci);

        vw_bdi = new DataAccessProgramView(new NameInfo("vw_bdi", "BDI"), "BENE_DESIGNATION_INTERFACE_12", "BENE_DSGN_INTFCE");
        bdi_Intrfce_Stts = vw_bdi.getRecord().newFieldInGroup("bdi_Intrfce_Stts", "INTRFCE-STTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, "INTRFCE_STTS");
        bdi_Intrfce_Stts.setDdmHeader("INTRFCE/STATUS");
        bdi_Tiaa_Cref_Ind = vw_bdi.getRecord().newFieldInGroup("bdi_Tiaa_Cref_Ind", "TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIAA_CREF_IND");
        bdi_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bdi_Seq_Nmbr = vw_bdi.getRecord().newFieldInGroup("bdi_Seq_Nmbr", "SEQ-NMBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "SEQ_NMBR");
        bdi_Seq_Nmbr.setDdmHeader("SEQ/NMBR");
        bdi_Rcrd_Crtd_For_Bsnss_Dte = vw_bdi.getRecord().newFieldInGroup("bdi_Rcrd_Crtd_For_Bsnss_Dte", "RCRD-CRTD-FOR-BSNSS-DTE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "RCRD_CRTD_FOR_BSNSS_DTE");
        bdi_Rcrd_Crtd_For_Bsnss_Dte.setDdmHeader("RCRD CRTD/FOR/BSNSS DTE");
        bdi_Rcrd_Last_Updt_Dte = vw_bdi.getRecord().newFieldInGroup("bdi_Rcrd_Last_Updt_Dte", "RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_DTE");
        bdi_Rcrd_Last_Updt_Dte.setDdmHeader("RECORD/LAST UPDT/DATE");
        bdi_Rcrd_Last_Updt_Tme = vw_bdi.getRecord().newFieldInGroup("bdi_Rcrd_Last_Updt_Tme", "RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_TME");
        bdi_Rcrd_Last_Updt_Tme.setDdmHeader("RECORD/LAST UPDT/TIME");
        bdi_Rcrd_Last_Updt_Userid = vw_bdi.getRecord().newFieldInGroup("bdi_Rcrd_Last_Updt_Userid", "RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_USERID");
        bdi_Rcrd_Last_Updt_Userid.setDdmHeader("RECORD/LAST UPDT/USER ID");
        bdi_Pin_Tiaa_Cntrct = vw_bdi.getRecord().newFieldInGroup("bdi_Pin_Tiaa_Cntrct", "PIN-TIAA-CNTRCT", FieldType.STRING, 22, RepeatingFieldStrategy.None, 
            "PIN_TIAA_CNTRCT");
        bdi_Pin_Tiaa_Cntrct.setDdmHeader("PIN/TIAA/CONTRACT");
        bdi_Bene_Type = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Type", "BENE-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BENE_TYPE");
        bdi_Bene_Type.setDdmHeader("BENE/TYPE");
        bdi_Rltn_Cde = vw_bdi.getRecord().newFieldInGroup("bdi_Rltn_Cde", "RLTN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "RLTN_CDE");
        bdi_Rltn_Cde.setDdmHeader("RELATION/CODE");
        bdi_Bene_Name1 = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Name1", "BENE-NAME1", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BENE_NAME1");
        bdi_Bene_Name1.setDdmHeader("BENEFICIARY/NAME 1");
        bdi_Bene_Name2 = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Name2", "BENE-NAME2", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BENE_NAME2");
        bdi_Bene_Name2.setDdmHeader("BENEFICIARY/NAME 2");
        bdi_Rltn_Free_Txt = vw_bdi.getRecord().newFieldInGroup("bdi_Rltn_Free_Txt", "RLTN-FREE-TXT", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "RLTN_FREE_TXT");
        bdi_Rltn_Free_Txt.setDdmHeader("OTHER/RELATION");
        bdi_Dte_Birth_Trust = vw_bdi.getRecord().newFieldInGroup("bdi_Dte_Birth_Trust", "DTE-BIRTH-TRUST", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "DTE_BIRTH_TRUST");
        bdi_Dte_Birth_Trust.setDdmHeader("DOB/DOT");
        bdi_Ssn_Cde = vw_bdi.getRecord().newFieldInGroup("bdi_Ssn_Cde", "SSN-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "SSN_CDE");
        bdi_Ssn_Cde.setDdmHeader("SSN/CODE");
        bdi_Ssn = vw_bdi.getRecord().newFieldInGroup("bdi_Ssn", "SSN", FieldType.STRING, 9, RepeatingFieldStrategy.None, "SSN");
        bdi_Ssn.setDdmHeader("SSN");
        bdi_Prcnt_Frctn_Ind = vw_bdi.getRecord().newFieldInGroup("bdi_Prcnt_Frctn_Ind", "PRCNT-FRCTN-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PRCNT_FRCTN_IND");
        bdi_Prcnt_Frctn_Ind.setDdmHeader("PRCNT/FRCTN/IND");
        bdi_Share_Prcnt = vw_bdi.getRecord().newFieldInGroup("bdi_Share_Prcnt", "SHARE-PRCNT", FieldType.NUMERIC, 5, 2, RepeatingFieldStrategy.None, "SHARE_PRCNT");
        bdi_Share_Prcnt.setDdmHeader("SHARE/PERCENT");
        bdi_Share_Nmrtr = vw_bdi.getRecord().newFieldInGroup("bdi_Share_Nmrtr", "SHARE-NMRTR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "SHARE_NMRTR");
        bdi_Share_Nmrtr.setDdmHeader("SHARE/NUMERATOR");
        bdi_Share_Dnmntr = vw_bdi.getRecord().newFieldInGroup("bdi_Share_Dnmntr", "SHARE-DNMNTR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "SHARE_DNMNTR");
        bdi_Share_Dnmntr.setDdmHeader("SHARE/DENOMINATOR");
        bdi_Irrvcble_Ind = vw_bdi.getRecord().newFieldInGroup("bdi_Irrvcble_Ind", "IRRVCBLE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "IRRVCBLE_IND");
        bdi_Irrvcble_Ind.setDdmHeader("IRVCBL/IND");
        bdi_Sttlmnt_Rstrctn = vw_bdi.getRecord().newFieldInGroup("bdi_Sttlmnt_Rstrctn", "STTLMNT-RSTRCTN", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "STTLMNT_RSTRCTN");
        bdi_Sttlmnt_Rstrctn.setDdmHeader("SETTLEMENT/RESTRICTION");
        bdi_Spcl_Txt1 = vw_bdi.getRecord().newFieldInGroup("bdi_Spcl_Txt1", "SPCL-TXT1", FieldType.STRING, 72, RepeatingFieldStrategy.None, "SPCL_TXT1");
        bdi_Spcl_Txt1.setDdmHeader("SPECIAL TEXT 1");
        bdi_Spcl_Txt2 = vw_bdi.getRecord().newFieldInGroup("bdi_Spcl_Txt2", "SPCL-TXT2", FieldType.STRING, 72, RepeatingFieldStrategy.None, "SPCL_TXT2");
        bdi_Spcl_Txt2.setDdmHeader("SPECIAL TEXT 2");
        bdi_Spcl_Txt3 = vw_bdi.getRecord().newFieldInGroup("bdi_Spcl_Txt3", "SPCL-TXT3", FieldType.STRING, 72, RepeatingFieldStrategy.None, "SPCL_TXT3");
        bdi_Spcl_Txt3.setDdmHeader("SPECIAL TEXT 3");
        bdi_Dflt_Estate = vw_bdi.getRecord().newFieldInGroup("bdi_Dflt_Estate", "DFLT-ESTATE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "DFLT_ESTATE");
        bdi_Dflt_Estate.setDdmHeader("DEFAULT/ESTATE");
        bdi_Mdo_Calc_Bene = vw_bdi.getRecord().newFieldInGroup("bdi_Mdo_Calc_Bene", "MDO-CALC-BENE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "MDO_CALC_BENE");
        bdi_Mdo_Calc_Bene.setDdmHeader("MDO/CALC/BENE");
        bdi_Bene_Addr1 = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Addr1", "BENE-ADDR1", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BENE_ADDR1");
        bdi_Bene_Addr2 = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Addr2", "BENE-ADDR2", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BENE_ADDR2");
        bdi_Bene_Addr3_City = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Addr3_City", "BENE-ADDR3-CITY", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "BENE_ADDR3_CITY");
        bdi_Bene_State = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_State", "BENE-STATE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "BENE_STATE");
        bdi_Bene_Zip = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Zip", "BENE-ZIP", FieldType.STRING, 10, RepeatingFieldStrategy.None, "BENE_ZIP");
        bdi_Bene_Phone = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Phone", "BENE-PHONE", FieldType.STRING, 20, RepeatingFieldStrategy.None, "BENE_PHONE");
        bdi_Bene_Gender = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Gender", "BENE-GENDER", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BENE_GENDER");
        bdi_Bene_Country = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Country", "BENE-COUNTRY", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BENE_COUNTRY");
        registerRecord(vw_bdi);

        vw_bmi = new DataAccessProgramView(new NameInfo("vw_bmi", "BMI"), "BENE_MOS_INTERFACE_12", "BENE_MOS_INTFCE", DdmPeriodicGroups.getInstance().getGroups("BENE_MOS_INTERFACE_12"));
        bmi_Intrfce_Stts = vw_bmi.getRecord().newFieldInGroup("bmi_Intrfce_Stts", "INTRFCE-STTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, "INTRFCE_STTS");
        bmi_Intrfce_Stts.setDdmHeader("INTRFCE/STATUS");
        bmi_Rcrd_Crtd_For_Bsnss_Dte = vw_bmi.getRecord().newFieldInGroup("bmi_Rcrd_Crtd_For_Bsnss_Dte", "RCRD-CRTD-FOR-BSNSS-DTE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "RCRD_CRTD_FOR_BSNSS_DTE");
        bmi_Rcrd_Crtd_For_Bsnss_Dte.setDdmHeader("RCRD CRTD/FOR/BSNSS DTE");
        bmi_Tiaa_Cref_Ind = vw_bmi.getRecord().newFieldInGroup("bmi_Tiaa_Cref_Ind", "TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIAA_CREF_IND");
        bmi_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bmi_Pin_Tiaa_Cntrct = vw_bmi.getRecord().newFieldInGroup("bmi_Pin_Tiaa_Cntrct", "PIN-TIAA-CNTRCT", FieldType.STRING, 22, RepeatingFieldStrategy.None, 
            "PIN_TIAA_CNTRCT");
        bmi_Pin_Tiaa_Cntrct.setDdmHeader("PIN/TIAA/CONTRACT");
        bmi_Rcrd_Crtd_Dte = vw_bmi.getRecord().newFieldInGroup("bmi_Rcrd_Crtd_Dte", "RCRD-CRTD-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_CRTD_DTE");
        bmi_Rcrd_Crtd_Dte.setDdmHeader("RECORD/CREATED/DATE");
        bmi_Rcrd_Crtd_Tme = vw_bmi.getRecord().newFieldInGroup("bmi_Rcrd_Crtd_Tme", "RCRD-CRTD-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "RCRD_CRTD_TME");
        bmi_Rcrd_Crtd_Tme.setDdmHeader("RECORD/CREATED/TIME");
        bmi_Rcrd_Last_Updt_Dte = vw_bmi.getRecord().newFieldInGroup("bmi_Rcrd_Last_Updt_Dte", "RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_DTE");
        bmi_Rcrd_Last_Updt_Dte.setDdmHeader("RECORD/LAST UPDT/DATE");
        bmi_Rcrd_Last_Updt_Tme = vw_bmi.getRecord().newFieldInGroup("bmi_Rcrd_Last_Updt_Tme", "RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_TME");
        bmi_Rcrd_Last_Updt_Tme.setDdmHeader("RECORD/LAST UPDT/TIME");
        bmi_Rcrd_Last_Updt_Userid = vw_bmi.getRecord().newFieldInGroup("bmi_Rcrd_Last_Updt_Userid", "RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_USERID");
        bmi_Rcrd_Last_Updt_Userid.setDdmHeader("RECORD/LAST UPDT/USER ID");
        bmi_Count_Castmos_Txt_Group = vw_bmi.getRecord().newFieldInGroup("bmi_Count_Castmos_Txt_Group", "C*MOS-TXT-GROUP", RepeatingFieldStrategy.CAsteriskVariable, 
            "BENE_MOS_INTFCE_MOS_TXT_GROUP");

        bmi_Mos_Txt_Group = vw_bmi.getRecord().newGroupInGroup("bmi_Mos_Txt_Group", "MOS-TXT-GROUP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "BENE_MOS_INTFCE_MOS_TXT_GROUP");
        bmi_Mos_Txt = bmi_Mos_Txt_Group.newFieldArrayInGroup("bmi_Mos_Txt", "MOS-TXT", FieldType.STRING, 72, new DbsArrayController(1, 60) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "MOS_TXT", "BENE_MOS_INTFCE_MOS_TXT_GROUP");
        bmi_Mos_Txt.setDdmHeader("MOS TEXT");
        registerRecord(vw_bmi);

        pnd_I1 = localVariables.newFieldInRecord("pnd_I1", "#I1", FieldType.PACKED_DECIMAL, 3);
        pnd_J1 = localVariables.newFieldInRecord("pnd_J1", "#J1", FieldType.PACKED_DECIMAL, 3);
        pnd_Intrfce_Super_2 = localVariables.newFieldInRecord("pnd_Intrfce_Super_2", "#INTRFCE-SUPER-2", FieldType.STRING, 32);

        pnd_Intrfce_Super_2__R_Field_1 = localVariables.newGroupInRecord("pnd_Intrfce_Super_2__R_Field_1", "REDEFINE", pnd_Intrfce_Super_2);
        pnd_Intrfce_Super_2_Pnd_Intrfce_Stts = pnd_Intrfce_Super_2__R_Field_1.newFieldInGroup("pnd_Intrfce_Super_2_Pnd_Intrfce_Stts", "#INTRFCE-STTS", 
            FieldType.STRING, 1);
        pnd_Intrfce_Super_2_Pnd_Rcrd_Crtd_For_Bsnss_Dte = pnd_Intrfce_Super_2__R_Field_1.newFieldInGroup("pnd_Intrfce_Super_2_Pnd_Rcrd_Crtd_For_Bsnss_Dte", 
            "#RCRD-CRTD-FOR-BSNSS-DTE", FieldType.STRING, 8);
        pnd_Intrfce_Super_2_Pnd_Pin_Tiaa_Cntrct = pnd_Intrfce_Super_2__R_Field_1.newFieldInGroup("pnd_Intrfce_Super_2_Pnd_Pin_Tiaa_Cntrct", "#PIN-TIAA-CNTRCT", 
            FieldType.STRING, 22);
        pnd_Intrfce_Super_2_Pnd_Tiaa_Cref_Ind = pnd_Intrfce_Super_2__R_Field_1.newFieldInGroup("pnd_Intrfce_Super_2_Pnd_Tiaa_Cref_Ind", "#TIAA-CREF-IND", 
            FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_bci.reset();
        vw_bdi.reset();
        vw_bmi.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Benn9811() throws Exception
    {
        super("Benn9811");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ERROR-INFO-SUB.##LAST-PROGRAM := *PROGRAM
        pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().reset();                                                                                                        //Natural: RESET MSG-INFO-SUB.##RETURN-CODE MSG-INFO-SUB.##MSG
        pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().reset();
        pnd_Intrfce_Super_2_Pnd_Intrfce_Stts.setValue(pdaBena9811.getBena9811_Pnd_Intrfce_Stts());                                                                        //Natural: ASSIGN #INTRFCE-SUPER-2.#INTRFCE-STTS := BENA9811.#INTRFCE-STTS
        pnd_Intrfce_Super_2_Pnd_Rcrd_Crtd_For_Bsnss_Dte.setValue(pdaBena9811.getBena9811_Pnd_Rcrd_Crtd_For_Bsnss_Dte());                                                  //Natural: ASSIGN #INTRFCE-SUPER-2.#RCRD-CRTD-FOR-BSNSS-DTE := BENA9811.#RCRD-CRTD-FOR-BSNSS-DTE
        pnd_Intrfce_Super_2_Pnd_Pin_Tiaa_Cntrct.setValue(pdaBena9811.getBena9811_Pnd_Pin_Tiaa_Cntrct());                                                                  //Natural: ASSIGN #INTRFCE-SUPER-2.#PIN-TIAA-CNTRCT := BENA9811.#PIN-TIAA-CNTRCT
        pnd_Intrfce_Super_2_Pnd_Tiaa_Cref_Ind.setValue(pdaBena9811.getBena9811_Pnd_Tiaa_Cref_Ind());                                                                      //Natural: ASSIGN #INTRFCE-SUPER-2.#TIAA-CREF-IND := BENA9811.#TIAA-CREF-IND
        if (condition(pdaBena9811.getBena9811_Pnd_Cntrct_Isn().equals(getZero())))                                                                                        //Natural: IF BENA9811.#CNTRCT-ISN = 0
        {
                                                                                                                                                                          //Natural: PERFORM FIND-CONTRACT
            sub_Find_Contract();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM GET-CONTRACT
            sub_Get_Contract();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaBena9811.getBena9811_Pnd_Mos_Ind().equals("Y")))                                                                                                 //Natural: IF BENA9811.#MOS-IND = 'Y'
        {
                                                                                                                                                                          //Natural: PERFORM FIND-MOS
            sub_Find_Mos();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM FIND-DESIGNATION
            sub_Find_Designation();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CONTRACT
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-CONTRACT
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-MOS
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-DESIGNATION
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-CONTRACT-FIELDS
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-MOS-FIELDS
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-DESIGNATION-FIELDS
        //* ***********************************************************************
    }
    private void sub_Get_Contract() throws Exception                                                                                                                      //Natural: GET-CONTRACT
    {
        if (BLNatReinput.isReinput()) return;

        GBCI:                                                                                                                                                             //Natural: GET BCI #CNTRCT-ISN
        vw_bci.readByID(pdaBena9811.getBena9811_Pnd_Cntrct_Isn().getLong(), "GBCI");
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTRACT-FIELDS
        sub_Update_Contract_Fields();
        if (condition(Global.isEscape())) {return;}
        vw_bci.updateDBRow("GBCI");                                                                                                                                       //Natural: UPDATE ( GBCI. )
        //*  GET-CONTRACT
    }
    private void sub_Find_Contract() throws Exception                                                                                                                     //Natural: FIND-CONTRACT
    {
        if (BLNatReinput.isReinput()) return;

        vw_bci.startDatabaseFind                                                                                                                                          //Natural: FIND ( 1 ) BCI WITH INTRFCE-SUPER-2 = #INTRFCE-SUPER-2
        (
        "FBCI",
        new Wc[] { new Wc("INTRFCE_SUPER_2", "=", pnd_Intrfce_Super_2, WcType.WITH) },
        1
        );
        FBCI:
        while (condition(vw_bci.readNextRow("FBCI")))
        {
            vw_bci.setIfNotFoundControlFlag(false);
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTRACT-FIELDS
            sub_Update_Contract_Fields();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FBCI"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FBCI"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            vw_bci.updateDBRow("FBCI");                                                                                                                                   //Natural: UPDATE ( FBCI. )
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  FIND-CONTRACT
    }
    private void sub_Find_Mos() throws Exception                                                                                                                          //Natural: FIND-MOS
    {
        if (BLNatReinput.isReinput()) return;

        vw_bmi.startDatabaseFind                                                                                                                                          //Natural: FIND ( 1 ) BMI WITH INTRFCE-SUPER-2 = #INTRFCE-SUPER-2
        (
        "FBMI",
        new Wc[] { new Wc("INTRFCE_SUPER_2", "=", pnd_Intrfce_Super_2, WcType.WITH) },
        1
        );
        FBMI:
        while (condition(vw_bmi.readNextRow("FBMI")))
        {
            vw_bmi.setIfNotFoundControlFlag(false);
                                                                                                                                                                          //Natural: PERFORM UPDATE-MOS-FIELDS
            sub_Update_Mos_Fields();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FBMI"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FBMI"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            vw_bmi.updateDBRow("FBMI");                                                                                                                                   //Natural: UPDATE ( FBMI. )
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  FIND-MOS
    }
    private void sub_Find_Designation() throws Exception                                                                                                                  //Natural: FIND-DESIGNATION
    {
        if (BLNatReinput.isReinput()) return;

        vw_bdi.startDatabaseRead                                                                                                                                          //Natural: READ BDI BY INTRFCE-SUPER-2 FROM #INTRFCE-SUPER-2
        (
        "RBDI",
        new Wc[] { new Wc("INTRFCE_SUPER_2", ">=", pnd_Intrfce_Super_2, WcType.BY) },
        new Oc[] { new Oc("INTRFCE_SUPER_2", "ASC") }
        );
        RBDI:
        while (condition(vw_bdi.readNextRow("RBDI")))
        {
            if (condition(bdi_Intrfce_Stts.notEquals(pnd_Intrfce_Super_2_Pnd_Intrfce_Stts) || bdi_Rcrd_Crtd_For_Bsnss_Dte.notEquals(pnd_Intrfce_Super_2_Pnd_Rcrd_Crtd_For_Bsnss_Dte)  //Natural: IF BDI.INTRFCE-STTS NE #INTRFCE-SUPER-2.#INTRFCE-STTS OR BDI.RCRD-CRTD-FOR-BSNSS-DTE NE #INTRFCE-SUPER-2.#RCRD-CRTD-FOR-BSNSS-DTE OR BDI.PIN-TIAA-CNTRCT NE #INTRFCE-SUPER-2.#PIN-TIAA-CNTRCT OR BDI.TIAA-CREF-IND NE #INTRFCE-SUPER-2.#TIAA-CREF-IND
                || bdi_Pin_Tiaa_Cntrct.notEquals(pnd_Intrfce_Super_2_Pnd_Pin_Tiaa_Cntrct) || bdi_Tiaa_Cref_Ind.notEquals(pnd_Intrfce_Super_2_Pnd_Tiaa_Cref_Ind)))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM UPDATE-DESIGNATION-FIELDS
            sub_Update_Designation_Fields();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RBDI"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RBDI"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            vw_bdi.updateDBRow("RBDI");                                                                                                                                   //Natural: UPDATE ( RBDI. )
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  FIND-DESIGNATION
    }
    private void sub_Update_Contract_Fields() throws Exception                                                                                                            //Natural: UPDATE-CONTRACT-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        bci_Intrfce_Stts.setValue(pdaBena9811.getBena9811_Pnd_New_Intrfce_Stts());                                                                                        //Natural: ASSIGN BCI.INTRFCE-STTS := BENA9811.#NEW-INTRFCE-STTS
        bci_Last_Prcssd_Bsnss_Dte.setValue(pdaBena9811.getBena9811_Pnd_Bsnss_Dte());                                                                                      //Natural: ASSIGN BCI.LAST-PRCSSD-BSNSS-DTE := BENA9811.#BSNSS-DTE
        bci_Rcrd_Last_Updt_Userid.setValue(Global.getPROGRAM());                                                                                                          //Natural: ASSIGN BCI.RCRD-LAST-UPDT-USERID := *PROGRAM
        if (condition(pdaBena9811.getBena9811_Pnd_New_Intrfce_Stts().equals("S")))                                                                                        //Natural: IF BENA9811.#NEW-INTRFCE-STTS = 'S'
        {
            bci_Intrfce_Cmpltd_Dte.setValue(pdaBena9811.getBena9811_Pnd_Bsnss_Dte());                                                                                     //Natural: ASSIGN BCI.INTRFCE-CMPLTD-DTE := BENA9811.#BSNSS-DTE
            bci_Intrfce_Cmpltd_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("HHIISST"));                                                                        //Natural: MOVE EDITED *TIMX ( EM = HHIISST ) TO BCI.INTRFCE-CMPLTD-TME
        }                                                                                                                                                                 //Natural: END-IF
        bci_Rcrd_Last_Updt_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                           //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO BCI.RCRD-LAST-UPDT-DTE
        bci_Rcrd_Last_Updt_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("HHIISST"));                                                                            //Natural: MOVE EDITED *TIMX ( EM = HHIISST ) TO BCI.RCRD-LAST-UPDT-TME
        //*  UPDATE-CONTRACT-FIELDS
    }
    private void sub_Update_Mos_Fields() throws Exception                                                                                                                 //Natural: UPDATE-MOS-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        bmi_Intrfce_Stts.setValue(pdaBena9811.getBena9811_Pnd_New_Intrfce_Stts());                                                                                        //Natural: ASSIGN BMI.INTRFCE-STTS := BENA9811.#NEW-INTRFCE-STTS
        bmi_Rcrd_Last_Updt_Userid.setValue(Global.getPROGRAM());                                                                                                          //Natural: ASSIGN BMI.RCRD-LAST-UPDT-USERID := *PROGRAM
        bmi_Rcrd_Last_Updt_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                           //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO BMI.RCRD-LAST-UPDT-DTE
        bmi_Rcrd_Last_Updt_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("HHIISST"));                                                                            //Natural: MOVE EDITED *TIMX ( EM = HHIISST ) TO BMI.RCRD-LAST-UPDT-TME
        //*  UPDATE-MOS-FIELDS
    }
    private void sub_Update_Designation_Fields() throws Exception                                                                                                         //Natural: UPDATE-DESIGNATION-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        bdi_Intrfce_Stts.setValue(pdaBena9811.getBena9811_Pnd_New_Intrfce_Stts());                                                                                        //Natural: ASSIGN BDI.INTRFCE-STTS := BENA9811.#NEW-INTRFCE-STTS
        bdi_Rcrd_Last_Updt_Userid.setValue(Global.getPROGRAM());                                                                                                          //Natural: ASSIGN BDI.RCRD-LAST-UPDT-USERID := *PROGRAM
        bdi_Rcrd_Last_Updt_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                           //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO BDI.RCRD-LAST-UPDT-DTE
        bdi_Rcrd_Last_Updt_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("HHIISST"));                                                                            //Natural: MOVE EDITED *TIMX ( EM = HHIISST ) TO BDI.RCRD-LAST-UPDT-TME
        //*  UPDATE-DESIGNATION-FIELDS
    }

    //
}
