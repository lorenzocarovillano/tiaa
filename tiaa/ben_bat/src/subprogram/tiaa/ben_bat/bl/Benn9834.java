/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:06:32 PM
**        * FROM NATURAL SUBPROGRAM : Benn9834
************************************************************
**        * FILE NAME            : Benn9834.java
**        * CLASS NAME           : Benn9834
**        * INSTANCE NAME        : Benn9834
************************************************************
************************************************************************
* PROGRAM  : BENN9834
* SYSTEM   : BENEFICIARY-SYSTEM
* TITLE    : TABLE FILE LOAD PROGRAM
* GENERATED: DEC 14, 1999
*          :
*
*
* HISTORY
* >
* >
************************************************************************

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benn9834 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaBena9834 pdaBena9834;
    private PdaBenpda_M pdaBenpda_M;
    private PdaBenpda_E pdaBenpda_E;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_bt;
    private DbsField bt_Bt_Table_Id;
    private DbsField bt_Bt_Table_Key;
    private DbsField bt_Bt_Table_Text;
    private DbsField pnd_I1;
    private DbsField pnd_J;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaBena9834 = new PdaBena9834(parameters);
        pdaBenpda_M = new PdaBenpda_M(parameters);
        pdaBenpda_E = new PdaBenpda_E(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_bt = new DataAccessProgramView(new NameInfo("vw_bt", "BT"), "BENE_TABLE_FILE", "BENE_TABLE");
        bt_Bt_Table_Id = vw_bt.getRecord().newFieldInGroup("bt_Bt_Table_Id", "BT-TABLE-ID", FieldType.STRING, 2, RepeatingFieldStrategy.None, "BT_TABLE_ID");
        bt_Bt_Table_Id.setDdmHeader("TABLE/ID");
        bt_Bt_Table_Key = vw_bt.getRecord().newFieldInGroup("bt_Bt_Table_Key", "BT-TABLE-KEY", FieldType.STRING, 20, RepeatingFieldStrategy.None, "BT_TABLE_KEY");
        bt_Bt_Table_Key.setDdmHeader("TABLE/KEY");
        bt_Bt_Table_Text = vw_bt.getRecord().newFieldInGroup("bt_Bt_Table_Text", "BT-TABLE-TEXT", FieldType.STRING, 72, RepeatingFieldStrategy.None, "BT_TABLE_TEXT");
        bt_Bt_Table_Text.setDdmHeader("TABLE/TEXT");
        registerRecord(vw_bt);

        pnd_I1 = localVariables.newFieldInRecord("pnd_I1", "#I1", FieldType.PACKED_DECIMAL, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_bt.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Benn9834() throws Exception
    {
        super("Benn9834");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ERROR-INFO-SUB.##LAST-PROGRAM = *PROGRAM
        //*  ENTRIES FROM TABLE FILE NEEDED FOR REL CODE &
                                                                                                                                                                          //Natural: PERFORM LOAD-TABLE-FILE
        sub_Load_Table_File();
        if (condition(Global.isEscape())) {return;}
        //*  /* ETC. LOAD HERE ONCE AND PASS AROUND SUBPROGS. JW 12/3/99
        //* ***************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-TABLE-FILE
        //* ***************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-ERROR
    }
    private void sub_Load_Table_File() throws Exception                                                                                                                   //Natural: LOAD-TABLE-FILE
    {
        if (BLNatReinput.isReinput()) return;

        vw_bt.startDatabaseRead                                                                                                                                           //Natural: READ BT BY BT-TABLE-ID-KEY = 'RC'
        (
        "R1",
        new Wc[] { new Wc("BT_TABLE_ID_KEY", ">=", "RC", WcType.BY) },
        new Oc[] { new Oc("BT_TABLE_ID_KEY", "ASC") }
        );
        R1:
        while (condition(vw_bt.readNextRow("R1")))
        {
            if (condition(bt_Bt_Table_Id.notEquals("RC")))                                                                                                                //Natural: IF BT-TABLE-ID NE 'RC'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  '00' CODE
            if (condition(bt_Bt_Table_Text.equals(" ")))                                                                                                                  //Natural: IF BT-TABLE-TEXT = ' '
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_J.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #J
            if (condition(pnd_J.greater(99)))                                                                                                                             //Natural: IF #J > 99
            {
                pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("Contact Support. Relationship table overflow");                                                       //Natural: ASSIGN ##MSG = 'Contact Support. Relationship table overflow'
                                                                                                                                                                          //Natural: PERFORM FORMAT-ERROR
                sub_Format_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  TERMINATE 01 LET CRASH ON NEXT LINE
            }                                                                                                                                                             //Natural: END-IF
            pdaBena9834.getBena9834_Pnd_Rc_Tab_Key().getValue(pnd_J).setValue(bt_Bt_Table_Key);                                                                           //Natural: ASSIGN #RC-TAB-KEY ( #J ) := BT-TABLE-KEY
            pdaBena9834.getBena9834_Pnd_Rc_Tab_Text().getValue(pnd_J).setValue(bt_Bt_Table_Text);                                                                         //Natural: ASSIGN #RC-TAB-TEXT ( #J ) := BT-TABLE-TEXT
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_J.less(1)))                                                                                                                                     //Natural: IF #J < 1
        {
            pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("Contact Support. No Relationships on file");                                                              //Natural: ASSIGN ##MSG = 'Contact Support. No Relationships on file'
            //*  WILL CRASH & PASS CONTROL TO BENP9800
                                                                                                                                                                          //Natural: PERFORM FORMAT-ERROR
            sub_Format_Error();
            if (condition(Global.isEscape())) {return;}
            pdaBena9834.getBena9834_Pnd_Rc_Tab_Key().getValue(pnd_J).setValue(" ");                                                                                       //Natural: ASSIGN #RC-TAB-KEY ( #J ) := ' '
        }                                                                                                                                                                 //Natural: END-IF
        pnd_J.reset();                                                                                                                                                    //Natural: RESET #J
        vw_bt.startDatabaseRead                                                                                                                                           //Natural: READ BT BY BT-TABLE-ID-KEY = 'SR'
        (
        "R2",
        new Wc[] { new Wc("BT_TABLE_ID_KEY", ">=", "SR", WcType.BY) },
        new Oc[] { new Oc("BT_TABLE_ID_KEY", "ASC") }
        );
        R2:
        while (condition(vw_bt.readNextRow("R2")))
        {
            if (condition(bt_Bt_Table_Id.notEquals("SR")))                                                                                                                //Natural: IF BT-TABLE-ID NE 'SR'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_J.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #J
            if (condition(pnd_J.greater(10)))                                                                                                                             //Natural: IF #J > 10
            {
                pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("Contact Support. Settlement Restriction table overflow");                                             //Natural: ASSIGN ##MSG = 'Contact Support. Settlement Restriction table overflow'
                                                                                                                                                                          //Natural: PERFORM FORMAT-ERROR
                sub_Format_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  TERMINATE 01 LET CRASH ON NEXT LINE
            }                                                                                                                                                             //Natural: END-IF
            pdaBena9834.getBena9834_Pnd_Sr_Tab_Key().getValue(pnd_J).setValue(bt_Bt_Table_Key);                                                                           //Natural: ASSIGN #SR-TAB-KEY ( #J ) := BT-TABLE-KEY
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_J.less(1)))                                                                                                                                     //Natural: IF #J < 1
        {
            pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("Contact Support. No Settlement Restrictions on file");                                                    //Natural: ASSIGN ##MSG = 'Contact Support. No Settlement Restrictions on file'
            //*  WILL CRASH & PASS CONTROL TO BENP9800
                                                                                                                                                                          //Natural: PERFORM FORMAT-ERROR
            sub_Format_Error();
            if (condition(Global.isEscape())) {return;}
            pdaBena9834.getBena9834_Pnd_Sr_Tab_Key().getValue(pnd_J).setValue(" ");                                                                                       //Natural: ASSIGN #SR-TAB-KEY ( #J ) := ' '
        }                                                                                                                                                                 //Natural: END-IF
        pnd_J.reset();                                                                                                                                                    //Natural: RESET #J
        vw_bt.startDatabaseRead                                                                                                                                           //Natural: READ BT BY BT-TABLE-ID-KEY = 'SS'
        (
        "R3",
        new Wc[] { new Wc("BT_TABLE_ID_KEY", ">=", "SS", WcType.BY) },
        new Oc[] { new Oc("BT_TABLE_ID_KEY", "ASC") }
        );
        R3:
        while (condition(vw_bt.readNextRow("R3")))
        {
            if (condition(bt_Bt_Table_Id.notEquals("SS")))                                                                                                                //Natural: IF BT-TABLE-ID NE 'SS'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_J.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #J
            if (condition(pnd_J.greater(10)))                                                                                                                             //Natural: IF #J > 10
            {
                pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("Contact Support. SSN/SocIns table overflow");                                                         //Natural: ASSIGN ##MSG = 'Contact Support. SSN/SocIns table overflow'
                                                                                                                                                                          //Natural: PERFORM FORMAT-ERROR
                sub_Format_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R3"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R3"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  TERMINATE 01 LET CRASH ON NEXT LINE
            }                                                                                                                                                             //Natural: END-IF
            pdaBena9834.getBena9834_Pnd_Ss_Tab_Key().getValue(pnd_J).setValue(bt_Bt_Table_Key);                                                                           //Natural: ASSIGN #SS-TAB-KEY ( #J ) := BT-TABLE-KEY
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_J.less(1)))                                                                                                                                     //Natural: IF #J < 1
        {
            pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("Contact Support. No SSN/SocIns Codes on file");                                                           //Natural: ASSIGN ##MSG = 'Contact Support. No SSN/SocIns Codes on file'
            //*  WILL CRASH & PASS CONTROL TO BENP9800
                                                                                                                                                                          //Natural: PERFORM FORMAT-ERROR
            sub_Format_Error();
            if (condition(Global.isEscape())) {return;}
            pdaBena9834.getBena9834_Pnd_Ss_Tab_Key().getValue(pnd_J).setValue(" ");                                                                                       //Natural: ASSIGN #SS-TAB-KEY ( #J ) := ' '
        }                                                                                                                                                                 //Natural: END-IF
        pnd_J.reset();                                                                                                                                                    //Natural: RESET #J
        vw_bt.startDatabaseRead                                                                                                                                           //Natural: READ BT BY BT-TABLE-ID-KEY = 'XR'
        (
        "R4",
        new Wc[] { new Wc("BT_TABLE_ID_KEY", ">=", "XR", WcType.BY) },
        new Oc[] { new Oc("BT_TABLE_ID_KEY", "ASC") }
        );
        R4:
        while (condition(vw_bt.readNextRow("R4")))
        {
            if (condition(bt_Bt_Table_Id.greater("XR")))                                                                                                                  //Natural: IF BT-TABLE-ID GT 'XR'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_J.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #J
            if (condition(pnd_J.greater(20)))                                                                                                                             //Natural: IF #J GT 20
            {
                pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("Contact Support. Exempt Relationship table overflow");                                                //Natural: ASSIGN ##MSG = 'Contact Support. Exempt Relationship table overflow'
                                                                                                                                                                          //Natural: PERFORM FORMAT-ERROR
                sub_Format_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R4"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R4"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  TERMINATE 01 LET CRASH ON NEXT LINE
            }                                                                                                                                                             //Natural: END-IF
            pdaBena9834.getBena9834_Pnd_Xr_Tab_Key().getValue(pnd_J).setValue(bt_Bt_Table_Key);                                                                           //Natural: ASSIGN #XR-TAB-KEY ( #J ) := BT-TABLE-KEY
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_J.less(1)))                                                                                                                                     //Natural: IF #J < 1
        {
            pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("Contact Support.  No Exempt Relationships on file");                                                      //Natural: ASSIGN ##MSG = 'Contact Support.  No Exempt Relationships on file'
            //*  WILL CRASH & PASS CONTROL TO BENP9800
                                                                                                                                                                          //Natural: PERFORM FORMAT-ERROR
            sub_Format_Error();
            if (condition(Global.isEscape())) {return;}
            pdaBena9834.getBena9834_Pnd_Xr_Tab_Key().getValue(pnd_J).setValue(" ");                                                                                       //Natural: ASSIGN #XR-TAB-KEY ( #J ) := ' '
        }                                                                                                                                                                 //Natural: END-IF
        pnd_J.reset();                                                                                                                                                    //Natural: RESET #J
        vw_bt.startDatabaseRead                                                                                                                                           //Natural: READ BT BY BT-TABLE-ID-KEY = '$R'
        (
        "R5",
        new Wc[] { new Wc("BT_TABLE_ID_KEY", ">=", "$R", WcType.BY) },
        new Oc[] { new Oc("BT_TABLE_ID_KEY", "ASC") }
        );
        R5:
        while (condition(vw_bt.readNextRow("R5")))
        {
            if (condition(bt_Bt_Table_Id.greater("$R")))                                                                                                                  //Natural: IF BT-TABLE-ID GT '$R'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_J.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #J
            if (condition(pnd_J.greater(99)))                                                                                                                             //Natural: IF #J GT 99
            {
                pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("Contact Support. Relationship Translate table overflow");                                             //Natural: ASSIGN ##MSG = 'Contact Support. Relationship Translate table overflow'
                                                                                                                                                                          //Natural: PERFORM FORMAT-ERROR
                sub_Format_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R5"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R5"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  TERMINATE 01 LET CRASH ON NEXT LINE
            }                                                                                                                                                             //Natural: END-IF
            pdaBena9834.getBena9834_Pnd_Dollar_R_Tab_Key().getValue(pnd_J).setValue(bt_Bt_Table_Key);                                                                     //Natural: ASSIGN #$R-TAB-KEY ( #J ) := BT-TABLE-KEY
            pdaBena9834.getBena9834_Pnd_Dollar_R_Tab_Text().getValue(pnd_J).setValue(bt_Bt_Table_Text);                                                                   //Natural: ASSIGN #$R-TAB-TEXT ( #J ) := BT-TABLE-TEXT
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_J.less(1)))                                                                                                                                     //Natural: IF #J < 1
        {
            pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("Contact Support. No Relationship Translate table on file");                                               //Natural: ASSIGN ##MSG = 'Contact Support. No Relationship Translate table on file'
            //*  WILL CRASH & PASS CONTROL TO BENP9800
                                                                                                                                                                          //Natural: PERFORM FORMAT-ERROR
            sub_Format_Error();
            if (condition(Global.isEscape())) {return;}
            pdaBena9834.getBena9834_Pnd_Dollar_R_Tab_Key().getValue(pnd_J).setValue(" ");                                                                                 //Natural: ASSIGN #$R-TAB-KEY ( #J ) := ' '
        }                                                                                                                                                                 //Natural: END-IF
        //*  LOAD-TABLE-FILE
    }
    private void sub_Format_Error() throws Exception                                                                                                                      //Natural: FORMAT-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(0, "*************************************************************");                                                                           //Natural: WRITE '*************************************************************'
        if (Global.isEscape()) return;
        getReports().write(0, "*************************************************************");                                                                           //Natural: WRITE '*************************************************************'
        if (Global.isEscape()) return;
        getReports().write(0, "*                                                           *");                                                                           //Natural: WRITE '*                                                           *'
        if (Global.isEscape()) return;
        getReports().write(0, "*",pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                                             //Natural: WRITE '*' ##MSG
        if (Global.isEscape()) return;
        getReports().write(0, "*                                                           *");                                                                           //Natural: WRITE '*                                                           *'
        if (Global.isEscape()) return;
        getReports().write(0, "*************************************************************");                                                                           //Natural: WRITE '*************************************************************'
        if (Global.isEscape()) return;
        getReports().write(0, "*************************************************************");                                                                           //Natural: WRITE '*************************************************************'
        if (Global.isEscape()) return;
    }

    //
}
