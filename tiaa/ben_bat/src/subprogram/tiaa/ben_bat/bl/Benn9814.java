/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:06:06 PM
**        * FROM NATURAL SUBPROGRAM : Benn9814
************************************************************
**        * FILE NAME            : Benn9814.java
**        * CLASS NAME           : Benn9814
**        * INSTANCE NAME        : Benn9814
************************************************************
************************************************************************
* PROGRAM  : BENN9814
* SYSTEM   : BENEFICIARY-SYSTEM
* TITLE    : CHECK STAT OF BENE RECORDS
* GENERATED: JULY 1, 1999
************************************************************************
*    DATE      USERID                   DESCRIPTION
* MM/DD/YYYY  XXXXXXXX  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
* 04/01/2017  DURAND    PIN EXPANSION N7 TO N12
************************************************************************

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benn9814 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaBena9814 pdaBena9814;
    private PdaBenpda_M pdaBenpda_M;
    private PdaBenpda_E pdaBenpda_E;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_bc;
    private DbsField bc_Bc_Stat;
    private DbsField bc_Bc_Last_Dsgntn_Srce;

    private DataAccessProgramView vw_bd;
    private DbsField bd_Bd_Stat;

    private DataAccessProgramView vw_bm;
    private DbsField bm_Bm_Stat;
    private DbsField pnd_Stat_Pin_Cntrct_Ind_Key;

    private DbsGroup pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_1;
    private DbsField pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Stat;
    private DbsField pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Pin_Cntrct_Ind_Key;

    private DbsGroup pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_2;
    private DbsField pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Pin;
    private DbsField pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Tiaa_Cntrct;
    private DbsField pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Tiaa_Cref_Ind;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaBena9814 = new PdaBena9814(parameters);
        pdaBenpda_M = new PdaBenpda_M(parameters);
        pdaBenpda_E = new PdaBenpda_E(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_bc = new DataAccessProgramView(new NameInfo("vw_bc", "BC"), "BENE_CONTRACT_12", "BENE_CONTRACT");
        bc_Bc_Stat = vw_bc.getRecord().newFieldInGroup("bc_Bc_Stat", "BC-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_STAT");
        bc_Bc_Stat.setDdmHeader("STATUS");
        bc_Bc_Last_Dsgntn_Srce = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Dsgntn_Srce", "BC-LAST-DSGNTN-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_SRCE");
        bc_Bc_Last_Dsgntn_Srce.setDdmHeader("LAST/DSGNTN/SOURCE");
        registerRecord(vw_bc);

        vw_bd = new DataAccessProgramView(new NameInfo("vw_bd", "BD"), "BENE_DESIGNATION_12", "BENE_DESIGNATION");
        bd_Bd_Stat = vw_bd.getRecord().newFieldInGroup("bd_Bd_Stat", "BD-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_STAT");
        bd_Bd_Stat.setDdmHeader("STAT");
        registerRecord(vw_bd);

        vw_bm = new DataAccessProgramView(new NameInfo("vw_bm", "BM"), "BENE_MOS_12", "BENE_MOS");
        bm_Bm_Stat = vw_bm.getRecord().newFieldInGroup("bm_Bm_Stat", "BM-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BM_STAT");
        bm_Bm_Stat.setDdmHeader("STAT");
        registerRecord(vw_bm);

        pnd_Stat_Pin_Cntrct_Ind_Key = localVariables.newFieldInRecord("pnd_Stat_Pin_Cntrct_Ind_Key", "#STAT-PIN-CNTRCT-IND-KEY", FieldType.STRING, 24);

        pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_1", "REDEFINE", pnd_Stat_Pin_Cntrct_Ind_Key);
        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Stat = pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_1.newFieldInGroup("pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Stat", "#STAT", 
            FieldType.STRING, 1);
        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Pin_Cntrct_Ind_Key = pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_1.newFieldInGroup("pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Pin_Cntrct_Ind_Key", 
            "#PIN-CNTRCT-IND-KEY", FieldType.STRING, 23);

        pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_2 = pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_1.newGroupInGroup("pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_2", "REDEFINE", 
            pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Pin_Cntrct_Ind_Key);
        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Pin = pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_2.newFieldInGroup("pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Pin", "#PIN", FieldType.NUMERIC, 
            12);
        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Tiaa_Cntrct = pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_2.newFieldInGroup("pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Tiaa_Cntrct", 
            "#TIAA-CNTRCT", FieldType.STRING, 10);
        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Tiaa_Cref_Ind = pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_2.newFieldInGroup("pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Tiaa_Cref_Ind", 
            "#TIAA-CREF-IND", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_bc.reset();
        vw_bd.reset();
        vw_bm.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Benn9814() throws Exception
    {
        super("Benn9814");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ERROR-INFO-SUB.##LAST-PROGRAM := *PROGRAM
        pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().reset();                                                                                                        //Natural: RESET MSG-INFO-SUB.##RETURN-CODE MSG-INFO-SUB.##MSG
        pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().reset();
        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Stat.setValue(pdaBena9814.getBena9814_Pnd_Stts());                                                                                //Natural: ASSIGN #STAT-PIN-CNTRCT-IND-KEY.#STAT := BENA9814.#STTS
        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Pin.setValue(pdaBena9814.getBena9814_Pnd_Pin());                                                                                  //Natural: ASSIGN #STAT-PIN-CNTRCT-IND-KEY.#PIN := BENA9814.#PIN
        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Tiaa_Cntrct.setValue(pdaBena9814.getBena9814_Pnd_Tiaa_Cntrct());                                                                  //Natural: ASSIGN #STAT-PIN-CNTRCT-IND-KEY.#TIAA-CNTRCT := BENA9814.#TIAA-CNTRCT
        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Tiaa_Cref_Ind.setValue(pdaBena9814.getBena9814_Pnd_Tiaa_Cref_Ind());                                                              //Natural: ASSIGN #STAT-PIN-CNTRCT-IND-KEY.#TIAA-CREF-IND := BENA9814.#TIAA-CREF-IND
        vw_bc.startDatabaseFind                                                                                                                                           //Natural: FIND ( 1 ) BC WITH BC-STAT-PIN-CNTRCT-IND-KEY = #STAT-PIN-CNTRCT-IND-KEY
        (
        "FIND01",
        new Wc[] { new Wc("BC_STAT_PIN_CNTRCT_IND_KEY", "=", pnd_Stat_Pin_Cntrct_Ind_Key, WcType.WITH) },
        1
        );
        FIND01:
        while (condition(vw_bc.readNextRow("FIND01", true)))
        {
            vw_bc.setIfNotFoundControlFlag(false);
            if (condition(vw_bc.getAstCOUNTER().equals(0)))                                                                                                               //Natural: IF NO RECORDS
            {
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            pdaBena9814.getBena9814_Pnd_Found().setValue(true);                                                                                                           //Natural: ASSIGN BENA9814.#FOUND := TRUE
            pdaBena9814.getBena9814_Pnd_Last_Srce().setValue(bc_Bc_Last_Dsgntn_Srce);                                                                                     //Natural: ASSIGN #LAST-SRCE := BC-LAST-DSGNTN-SRCE
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        vw_bm.startDatabaseFind                                                                                                                                           //Natural: FIND ( 1 ) BM WITH BM-STAT-PIN-CNTRCT-IND-KEY = #STAT-PIN-CNTRCT-IND-KEY
        (
        "FIND02",
        new Wc[] { new Wc("BM_STAT_PIN_CNTRCT_IND_KEY", "=", pnd_Stat_Pin_Cntrct_Ind_Key, WcType.WITH) },
        1
        );
        FIND02:
        while (condition(vw_bm.readNextRow("FIND02", true)))
        {
            vw_bm.setIfNotFoundControlFlag(false);
            if (condition(vw_bm.getAstCOUNTER().equals(0)))                                                                                                               //Natural: IF NO RECORDS
            {
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            pdaBena9814.getBena9814_Pnd_Found().setValue(true);                                                                                                           //Natural: ASSIGN BENA9814.#FOUND := TRUE
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        vw_bd.startDatabaseFind                                                                                                                                           //Natural: FIND BD WITH BD-PIN-CNTRCT-IND-KEY = #PIN-CNTRCT-IND-KEY
        (
        "FIND03",
        new Wc[] { new Wc("BD_PIN_CNTRCT_IND_KEY", "=", pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Pin_Cntrct_Ind_Key, WcType.WITH) }
        );
        FIND03:
        while (condition(vw_bd.readNextRow("FIND03", true)))
        {
            vw_bd.setIfNotFoundControlFlag(false);
            if (condition(vw_bd.getAstCOUNTER().equals(0)))                                                                                                               //Natural: IF NO RECORDS
            {
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            if (condition(bd_Bd_Stat.notEquals(pdaBena9814.getBena9814_Pnd_Stts())))                                                                                      //Natural: REJECT IF BD-STAT NE BENA9814.#STTS
            {
                continue;
            }
            pdaBena9814.getBena9814_Pnd_Found().setValue(true);                                                                                                           //Natural: ASSIGN BENA9814.#FOUND := TRUE
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }

    //
}
