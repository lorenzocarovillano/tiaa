/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:05:42 PM
**        * FROM NATURAL SUBPROGRAM : Benn9800
************************************************************
**        * FILE NAME            : Benn9800.java
**        * CLASS NAME           : Benn9800
**        * INSTANCE NAME        : Benn9800
************************************************************
************************************************************************
* PROGRAM  : BENN9800
* SYSTEM   : BENEFICIARY-SYSTEM
* TITLE    : GET NEXT RECORD FOR PROCESSING
* GENERATED: JULY 1, 1999
************************************************************************
*    DATE      USERID                   DESCRIPTION
* MM/DD/YYYY  XXXXXXXX  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
* 04/07/2004  SOTTO     RE-STOWED FOR NAT3.1.6.
* 04/01/2017  DURAND    PIN EXPANSION N7 TO N12
************************************************************************

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benn9800 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaBena9800 pdaBena9800;
    private PdaBena9801 pdaBena9801;
    private PdaBenpda_M pdaBenpda_M;
    private PdaBenpda_E pdaBenpda_E;
    private PdaBena9813 pdaBena9813;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_bci;
    private DbsField bci_Intrfce_Stts;
    private DbsField bci_Rcrd_Crtd_For_Bsnss_Dte;
    private DbsField bci_Rcrd_Crtd_Dte;
    private DbsField bci_Rcrd_Crtd_Tme;
    private DbsField bci_Intrfcng_Systm;
    private DbsField bci_Rqstng_Systm;
    private DbsField bci_Rcrd_Last_Updt_Dte;
    private DbsField bci_Rcrd_Last_Updt_Tme;
    private DbsField bci_Rcrd_Last_Updt_Userid;
    private DbsField bci_Last_Prcssd_Bsnss_Dte;
    private DbsField bci_Intrfce_Cmpltd_Dte;
    private DbsField bci_Intrfce_Cmpltd_Tme;
    private DbsField bci_Dflt_To_Estate_Ind;
    private DbsField bci_Illgble_Ind;
    private DbsField bci_More_Than_Five_Benes_Ind;
    private DbsField bci_Intrfce_Mgrtn_Ind;
    private DbsField bci_More_Than_Thirty_Benes_Ind;
    private DbsField bci_Pin_Tiaa_Cntrct;

    private DbsGroup bci_Error_Table;
    private DbsField bci_Error_Txt;
    private DbsField bci_Error_Cde;
    private DbsField bci_Error_Dte;
    private DbsField bci_Error_Tme;
    private DbsField bci_Error_Pgm;
    private DbsField bci_Fldr_Log_Dte_Tme;
    private DbsField bci_Last_Dsgntn_Srce;
    private DbsField bci_Last_Dsgntn_System;
    private DbsField bci_Last_Dsgntn_Userid;
    private DbsField bci_Last_Dsgntn_Dte;
    private DbsField bci_Last_Dsgntn_Tme;
    private DbsField bci_Tiaa_Cref_Chng_Dte;
    private DbsField bci_Tiaa_Cref_Chng_Tme;
    private DbsField bci_Chng_Pwr_Atty;
    private DbsField bci_Cref_Cntrct;
    private DbsField bci_Tiaa_Cref_Ind;
    private DbsField bci_Cntrct_Type;
    private DbsField bci_Eff_Dte;
    private DbsField bci_Mos_Ind;
    private DbsField bci_Irrvcble_Ind;
    private DbsField bci_Pymnt_Chld_Dcsd_Ind;
    private DbsField bci_Exempt_Spouse_Rights;
    private DbsField bci_Spouse_Waived_Bnfts;
    private DbsField bci_Chng_New_Issue_Ind;
    private DbsField bci_Same_As_Ind;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Record_Found;
    private DbsField pnd_Intrfce_Super_1;

    private DbsGroup pnd_Intrfce_Super_1__R_Field_1;
    private DbsField pnd_Intrfce_Super_1_Pnd_Intrfce_Stts;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaBena9813 = new PdaBena9813(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaBena9800 = new PdaBena9800(parameters);
        pdaBena9801 = new PdaBena9801(parameters);
        pdaBenpda_M = new PdaBenpda_M(parameters);
        pdaBenpda_E = new PdaBenpda_E(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        vw_bci = new DataAccessProgramView(new NameInfo("vw_bci", "BCI"), "BENE_CONTRACT_INTERFACE_12", "BENE_CONT_INTFCE", DdmPeriodicGroups.getInstance().getGroups("BENE_CONTRACT_INTERFACE_12"));
        bci_Intrfce_Stts = vw_bci.getRecord().newFieldInGroup("bci_Intrfce_Stts", "INTRFCE-STTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, "INTRFCE_STTS");
        bci_Intrfce_Stts.setDdmHeader("INTRFCE/STATUS");
        bci_Rcrd_Crtd_For_Bsnss_Dte = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Crtd_For_Bsnss_Dte", "RCRD-CRTD-FOR-BSNSS-DTE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "RCRD_CRTD_FOR_BSNSS_DTE");
        bci_Rcrd_Crtd_For_Bsnss_Dte.setDdmHeader("RCRD CRTD/FOR/BSNSS DTE");
        bci_Rcrd_Crtd_Dte = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Crtd_Dte", "RCRD-CRTD-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_CRTD_DTE");
        bci_Rcrd_Crtd_Dte.setDdmHeader("RECORD/CREATED/DATE");
        bci_Rcrd_Crtd_Tme = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Crtd_Tme", "RCRD-CRTD-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "RCRD_CRTD_TME");
        bci_Rcrd_Crtd_Tme.setDdmHeader("RECORD/CREATED/TIME");
        bci_Intrfcng_Systm = vw_bci.getRecord().newFieldInGroup("bci_Intrfcng_Systm", "INTRFCNG-SYSTM", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "INTRFCNG_SYSTM");
        bci_Intrfcng_Systm.setDdmHeader("INTRFCNG/SYSTEM");
        bci_Rqstng_Systm = vw_bci.getRecord().newFieldInGroup("bci_Rqstng_Systm", "RQSTNG-SYSTM", FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQSTNG_SYSTM");
        bci_Rqstng_Systm.setDdmHeader("RQSTNG/SYSTEM");
        bci_Rcrd_Last_Updt_Dte = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Last_Updt_Dte", "RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_DTE");
        bci_Rcrd_Last_Updt_Dte.setDdmHeader("RECORD/LAST UPDT/DATE");
        bci_Rcrd_Last_Updt_Tme = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Last_Updt_Tme", "RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_TME");
        bci_Rcrd_Last_Updt_Tme.setDdmHeader("RECORD/LAST UPDT/TIME");
        bci_Rcrd_Last_Updt_Userid = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Last_Updt_Userid", "RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_USERID");
        bci_Rcrd_Last_Updt_Userid.setDdmHeader("RECORD/LAST UPDT/USER ID");
        bci_Last_Prcssd_Bsnss_Dte = vw_bci.getRecord().newFieldInGroup("bci_Last_Prcssd_Bsnss_Dte", "LAST-PRCSSD-BSNSS-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "LAST_PRCSSD_BSNSS_DTE");
        bci_Last_Prcssd_Bsnss_Dte.setDdmHeader("LAST PRCSSD/BUSINESS/DATE");
        bci_Intrfce_Cmpltd_Dte = vw_bci.getRecord().newFieldInGroup("bci_Intrfce_Cmpltd_Dte", "INTRFCE-CMPLTD-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "INTRFCE_CMPLTD_DTE");
        bci_Intrfce_Cmpltd_Dte.setDdmHeader("INTFCE/CMPLTD/DATE");
        bci_Intrfce_Cmpltd_Tme = vw_bci.getRecord().newFieldInGroup("bci_Intrfce_Cmpltd_Tme", "INTRFCE-CMPLTD-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "INTRFCE_CMPLTD_TME");
        bci_Intrfce_Cmpltd_Tme.setDdmHeader("INTRFCE/CMPLTD/TME");
        bci_Dflt_To_Estate_Ind = vw_bci.getRecord().newFieldInGroup("bci_Dflt_To_Estate_Ind", "DFLT-TO-ESTATE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "DFLT_TO_ESTATE_IND");
        bci_Dflt_To_Estate_Ind.setDdmHeader("DEFAULT/TO/ESTATE");
        bci_Illgble_Ind = vw_bci.getRecord().newFieldInGroup("bci_Illgble_Ind", "ILLGBLE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "ILLGBLE_IND");
        bci_Illgble_Ind.setDdmHeader("ILLGBLE/IND");
        bci_More_Than_Five_Benes_Ind = vw_bci.getRecord().newFieldInGroup("bci_More_Than_Five_Benes_Ind", "MORE-THAN-FIVE-BENES-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "MORE_THAN_FIVE_BENES_IND");
        bci_More_Than_Five_Benes_Ind.setDdmHeader("MORE/THAN 5/BENES");
        bci_Intrfce_Mgrtn_Ind = vw_bci.getRecord().newFieldInGroup("bci_Intrfce_Mgrtn_Ind", "INTRFCE-MGRTN-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "INTRFCE_MGRTN_IND");
        bci_Intrfce_Mgrtn_Ind.setDdmHeader("INTRFCE/MGRTN/IND");
        bci_More_Than_Thirty_Benes_Ind = vw_bci.getRecord().newFieldInGroup("bci_More_Than_Thirty_Benes_Ind", "MORE-THAN-THIRTY-BENES-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "MORE_THAN_THIRTY_BENES_IND");
        bci_More_Than_Thirty_Benes_Ind.setDdmHeader("MORE/THAN 30/BENES");
        bci_Pin_Tiaa_Cntrct = vw_bci.getRecord().newFieldInGroup("bci_Pin_Tiaa_Cntrct", "PIN-TIAA-CNTRCT", FieldType.STRING, 22, RepeatingFieldStrategy.None, 
            "PIN_TIAA_CNTRCT");
        bci_Pin_Tiaa_Cntrct.setDdmHeader("PIN/TIAA/CONTRACT");

        bci_Error_Table = vw_bci.getRecord().newGroupInGroup("bci_Error_Table", "ERROR-TABLE", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Error_Txt = bci_Error_Table.newFieldArrayInGroup("bci_Error_Txt", "ERROR-TXT", FieldType.STRING, 72, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ERROR_TXT", "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Error_Txt.setDdmHeader("ERROR/TEXT");
        bci_Error_Cde = bci_Error_Table.newFieldArrayInGroup("bci_Error_Cde", "ERROR-CDE", FieldType.STRING, 5, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ERROR_CDE", "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Error_Cde.setDdmHeader("ERROR/CODE");
        bci_Error_Dte = bci_Error_Table.newFieldArrayInGroup("bci_Error_Dte", "ERROR-DTE", FieldType.STRING, 8, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ERROR_DTE", "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Error_Dte.setDdmHeader("ERROR/DATE");
        bci_Error_Tme = bci_Error_Table.newFieldArrayInGroup("bci_Error_Tme", "ERROR-TME", FieldType.STRING, 7, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ERROR_TME", "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Error_Tme.setDdmHeader("ERROR/TIME");
        bci_Error_Pgm = bci_Error_Table.newFieldArrayInGroup("bci_Error_Pgm", "ERROR-PGM", FieldType.STRING, 8, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ERROR_PGM", "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Fldr_Log_Dte_Tme = vw_bci.getRecord().newFieldInGroup("bci_Fldr_Log_Dte_Tme", "FLDR-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "FLDR_LOG_DTE_TME");
        bci_Fldr_Log_Dte_Tme.setDdmHeader("FOLDER/LOG DATE/TIME");
        bci_Last_Dsgntn_Srce = vw_bci.getRecord().newFieldInGroup("bci_Last_Dsgntn_Srce", "LAST-DSGNTN-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "LAST_DSGNTN_SRCE");
        bci_Last_Dsgntn_Srce.setDdmHeader("LAST/DSGNTN/SOURCE");
        bci_Last_Dsgntn_System = vw_bci.getRecord().newFieldInGroup("bci_Last_Dsgntn_System", "LAST-DSGNTN-SYSTEM", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "LAST_DSGNTN_SYSTEM");
        bci_Last_Dsgntn_System.setDdmHeader("LAST/DSGNTN/SYSTEM");
        bci_Last_Dsgntn_Userid = vw_bci.getRecord().newFieldInGroup("bci_Last_Dsgntn_Userid", "LAST-DSGNTN-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "LAST_DSGNTN_USERID");
        bci_Last_Dsgntn_Userid.setDdmHeader("LAST/DSGNTN/USERID");
        bci_Last_Dsgntn_Dte = vw_bci.getRecord().newFieldInGroup("bci_Last_Dsgntn_Dte", "LAST-DSGNTN-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "LAST_DSGNTN_DTE");
        bci_Last_Dsgntn_Dte.setDdmHeader("LAST/DSGNTN/DATE");
        bci_Last_Dsgntn_Tme = vw_bci.getRecord().newFieldInGroup("bci_Last_Dsgntn_Tme", "LAST-DSGNTN-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "LAST_DSGNTN_TME");
        bci_Last_Dsgntn_Tme.setDdmHeader("LAST/DSGNTN/TIME");
        bci_Tiaa_Cref_Chng_Dte = vw_bci.getRecord().newFieldInGroup("bci_Tiaa_Cref_Chng_Dte", "TIAA-CREF-CHNG-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIAA_CREF_CHNG_DTE");
        bci_Tiaa_Cref_Chng_Dte.setDdmHeader("TIAA CREF/CHANGE/DATE");
        bci_Tiaa_Cref_Chng_Tme = vw_bci.getRecord().newFieldInGroup("bci_Tiaa_Cref_Chng_Tme", "TIAA-CREF-CHNG-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "TIAA_CREF_CHNG_TME");
        bci_Tiaa_Cref_Chng_Tme.setDdmHeader("TIAA CREF/CHANGE/TIME");
        bci_Chng_Pwr_Atty = vw_bci.getRecord().newFieldInGroup("bci_Chng_Pwr_Atty", "CHNG-PWR-ATTY", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CHNG_PWR_ATTY");
        bci_Chng_Pwr_Atty.setDdmHeader("CHANGE/POWER/ATTNY");
        bci_Cref_Cntrct = vw_bci.getRecord().newFieldInGroup("bci_Cref_Cntrct", "CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT");
        bci_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bci_Tiaa_Cref_Ind = vw_bci.getRecord().newFieldInGroup("bci_Tiaa_Cref_Ind", "TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIAA_CREF_IND");
        bci_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bci_Cntrct_Type = vw_bci.getRecord().newFieldInGroup("bci_Cntrct_Type", "CNTRCT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_TYPE");
        bci_Cntrct_Type.setDdmHeader("CNTRCT/TYPE");
        bci_Eff_Dte = vw_bci.getRecord().newFieldInGroup("bci_Eff_Dte", "EFF-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "EFF_DTE");
        bci_Eff_Dte.setDdmHeader("EFFECTIVE/DATE");
        bci_Mos_Ind = vw_bci.getRecord().newFieldInGroup("bci_Mos_Ind", "MOS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "MOS_IND");
        bci_Mos_Ind.setDdmHeader("MOS/IND");
        bci_Irrvcble_Ind = vw_bci.getRecord().newFieldInGroup("bci_Irrvcble_Ind", "IRRVCBLE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "IRRVCBLE_IND");
        bci_Irrvcble_Ind.setDdmHeader("IRVCBL/IND");
        bci_Pymnt_Chld_Dcsd_Ind = vw_bci.getRecord().newFieldInGroup("bci_Pymnt_Chld_Dcsd_Ind", "PYMNT-CHLD-DCSD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PYMNT_CHLD_DCSD_IND");
        bci_Pymnt_Chld_Dcsd_Ind.setDdmHeader("PYMNT/CHILD/DCSD");
        bci_Exempt_Spouse_Rights = vw_bci.getRecord().newFieldInGroup("bci_Exempt_Spouse_Rights", "EXEMPT-SPOUSE-RIGHTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "EXEMPT_SPOUSE_RIGHTS");
        bci_Exempt_Spouse_Rights.setDdmHeader("EXEMPT/SPOUSAL/RIGHTS");
        bci_Spouse_Waived_Bnfts = vw_bci.getRecord().newFieldInGroup("bci_Spouse_Waived_Bnfts", "SPOUSE-WAIVED-BNFTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "SPOUSE_WAIVED_BNFTS");
        bci_Spouse_Waived_Bnfts.setDdmHeader("SPOUSE/WAIVED/BENEFITS");
        bci_Chng_New_Issue_Ind = vw_bci.getRecord().newFieldInGroup("bci_Chng_New_Issue_Ind", "CHNG-NEW-ISSUE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CHNG_NEW_ISSUE_IND");
        bci_Chng_New_Issue_Ind.setDdmHeader("CHANGE/NEW ISSUE/IND");
        bci_Same_As_Ind = vw_bci.getRecord().newFieldInGroup("bci_Same_As_Ind", "SAME-AS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "SAME_AS_IND");
        registerRecord(vw_bci);

        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_Record_Found = localVariables.newFieldInRecord("pnd_Record_Found", "#RECORD-FOUND", FieldType.BOOLEAN, 1);
        pnd_Intrfce_Super_1 = localVariables.newFieldInRecord("pnd_Intrfce_Super_1", "#INTRFCE-SUPER-1", FieldType.STRING, 40);

        pnd_Intrfce_Super_1__R_Field_1 = localVariables.newGroupInRecord("pnd_Intrfce_Super_1__R_Field_1", "REDEFINE", pnd_Intrfce_Super_1);
        pnd_Intrfce_Super_1_Pnd_Intrfce_Stts = pnd_Intrfce_Super_1__R_Field_1.newFieldInGroup("pnd_Intrfce_Super_1_Pnd_Intrfce_Stts", "#INTRFCE-STTS", 
            FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_bci.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Benn9800() throws Exception
    {
        super("Benn9800");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ERROR-INFO-SUB.##LAST-PROGRAM := *PROGRAM
        //*  FOR ESCAPE BOTTOM
        MAIN_ROUTINE:                                                                                                                                                     //Natural: REPEAT
        while (condition(whileTrue))
        {
                                                                                                                                                                          //Natural: PERFORM INITIALIZATIONS
            sub_Initializations();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("MAIN_ROUTINE"))) break;
                else if (condition(Global.isEscapeBottomImmediate("MAIN_ROUTINE"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  USED TO RE-READ IF NECESSARY
            REPEAT01:                                                                                                                                                     //Natural: REPEAT
            while (condition(whileTrue))
            {
                                                                                                                                                                          //Natural: PERFORM GET-NEXT-RECORD
                sub_Get_Next_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*   IF WE ARE DONE WITH THE CURRENT STATUS, GO GET THE NEXT ONE
                //*     AND ESCAPE TOP TO READ AGAIN!
                if (condition(pdaBena9813.getBena9813_Pnd_Done_With_Crrnt_Stts().getBoolean()))                                                                           //Natural: IF BENA9813.#DONE-WITH-CRRNT-STTS
                {
                                                                                                                                                                          //Natural: PERFORM DETERMINE-WHICH-STATUS
                    sub_Determine_Which_Status();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  GET RECORDS WITH NEW STATUS.
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-REPEAT
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("MAIN_ROUTINE"))) break;
                else if (condition(Global.isEscapeBottomImmediate("MAIN_ROUTINE"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (true) break MAIN_ROUTINE;                                                                                                                                 //Natural: ESCAPE BOTTOM ( MAIN-ROUTINE. )
            //* ************************************
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ESCAPE-MAIN-ROUTINE
            //*  MAIN-ROUTINE.
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-NEXT-RECORD
        //* ***********************************************************************
        //*   GRAB THE ISN AND MOVE ALL DATA ELEMENTS INTO THE PDA
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-WHICH-STATUS
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZATIONS
    }
    private void sub_Escape_Main_Routine() throws Exception                                                                                                               //Natural: ESCAPE-MAIN-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( MAIN-ROUTINE. )
        Global.setEscapeCode(EscapeType.Bottom, "MAIN_ROUTINE");
        if (true) return;
        //*  ESCAPE-MAIN-ROUTINE
    }
    private void sub_Get_Next_Record() throws Exception                                                                                                                   //Natural: GET-NEXT-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Intrfce_Super_1_Pnd_Intrfce_Stts.setValue(pdaBena9813.getBena9813_Pnd_New_Stts());                                                                            //Natural: ASSIGN #INTRFCE-SUPER-1.#INTRFCE-STTS := BENA9813.#NEW-STTS
        pnd_Record_Found.reset();                                                                                                                                         //Natural: RESET #RECORD-FOUND
        vw_bci.startDatabaseRead                                                                                                                                          //Natural: READ ( 1 ) BCI BY INTRFCE-SUPER-1 FROM #INTRFCE-SUPER-1
        (
        "RBCI",
        new Wc[] { new Wc("INTRFCE_SUPER_1", ">=", pnd_Intrfce_Super_1, WcType.BY) },
        new Oc[] { new Oc("INTRFCE_SUPER_1", "ASC") },
        1
        );
        RBCI:
        while (condition(vw_bci.readNextRow("RBCI")))
        {
            //*   ONCE WE READ SOMETHING THAT WE ALREADY PROCESSED TODAY, WE MOVE
            //*    ON TO THE NEXT STATUS
            if (condition(bci_Last_Prcssd_Bsnss_Dte.greaterOrEqual(pdaBena9800.getBena9800_Pnd_Bsnss_Dte()) || bci_Intrfce_Stts.notEquals(pdaBena9813.getBena9813_Pnd_New_Stts()))) //Natural: IF BCI.LAST-PRCSSD-BSNSS-DTE GE BENA9800.#BSNSS-DTE OR BCI.INTRFCE-STTS NE BENA9813.#NEW-STTS
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pdaBena9801.getBena9801_Pnd_Cntrct_Isn().setValue(vw_bci.getAstISN("RBCI"));                                                                                  //Natural: ASSIGN BENA9801.#CNTRCT-ISN := *ISN ( RBCI. )
            pnd_Record_Found.setValue(true);                                                                                                                              //Natural: ASSIGN #RECORD-FOUND := TRUE
            pdaBena9801.getBena9801().setValuesByName(vw_bci);                                                                                                            //Natural: MOVE BY NAME BCI TO BENA9801
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(! (pnd_Record_Found.getBoolean())))                                                                                                                 //Natural: IF NOT #RECORD-FOUND
        {
            pdaBena9813.getBena9813_Pnd_Done_With_Crrnt_Stts().setValue(true);                                                                                            //Natural: ASSIGN BENA9813.#DONE-WITH-CRRNT-STTS := TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  GET-NEXT-RECORD
    }
    private void sub_Determine_Which_Status() throws Exception                                                                                                            //Natural: DETERMINE-WHICH-STATUS
    {
        if (BLNatReinput.isReinput()) return;

        pdaBena9813.getBena9813_Pnd_Old_Stts().setValue(pdaBena9813.getBena9813_Pnd_New_Stts());                                                                          //Natural: ASSIGN BENA9813.#OLD-STTS := BENA9813.#NEW-STTS
        pdaBena9813.getBena9813_Pnd_New_Stts().reset();                                                                                                                   //Natural: RESET BENA9813.#NEW-STTS
        DbsUtil.callnat(Benn9813.class , getCurrentProcessState(), pdaBena9813.getBena9813(), pdaBenpda_M.getMsg_Info_Sub(), pdaBenpda_E.getError_Info_Sub());            //Natural: CALLNAT 'BENN9813' BENA9813 MSG-INFO-SUB ERROR-INFO-SUB
        if (condition(Global.isEscape())) return;
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ERROR-INFO-SUB.##LAST-PROGRAM := *PROGRAM
        //*   IF WE HAVE GONE THROUGH ALL OF THE STATUSES, GET OUT OF THE PROGRAM.
        //*   WE ARE DONE INTERFACING FOR THE DAY
        if (condition(pdaBena9813.getBena9813_Pnd_Done_For_Today().getBoolean()))                                                                                         //Natural: IF BENA9813.#DONE-FOR-TODAY
        {
            pdaBena9800.getBena9800_Pnd_Done_For_Today().setValue(true);                                                                                                  //Natural: ASSIGN BENA9800.#DONE-FOR-TODAY := TRUE
                                                                                                                                                                          //Natural: PERFORM ESCAPE-MAIN-ROUTINE
            sub_Escape_Main_Routine();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  DETERMINE-WHICH-STATUS
    }
    private void sub_Initializations() throws Exception                                                                                                                   //Natural: INITIALIZATIONS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pdaBena9801.getBena9801().reset();                                                                                                                                //Natural: RESET BENA9801 MSG-INFO-SUB.##RETURN-CODE MSG-INFO-SUB.##MSG BENA9800.#DONE-FOR-TODAY
        pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().reset();
        pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().reset();
        pdaBena9800.getBena9800_Pnd_Done_For_Today().reset();
                                                                                                                                                                          //Natural: PERFORM DETERMINE-WHICH-STATUS
        sub_Determine_Which_Status();
        if (condition(Global.isEscape())) {return;}
        //*  INITIALIZATIONS
    }

    //
}
