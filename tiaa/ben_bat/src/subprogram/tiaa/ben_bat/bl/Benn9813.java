/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:06:04 PM
**        * FROM NATURAL SUBPROGRAM : Benn9813
************************************************************
**        * FILE NAME            : Benn9813.java
**        * CLASS NAME           : Benn9813
**        * INSTANCE NAME        : Benn9813
************************************************************
************************************************************************
* PROGRAM  : BENN9813
* SYSTEM   : BENEFICIARY-SYSTEM
* TITLE    : DETERMINE WHICH STATUS TO READ
* GENERATED: JUNE 29, 1999
*          :
*
*
* HISTORY
* CHANGED ON JUNE 29,99 BY DEPAUL
* >
* >
************************************************************************

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benn9813 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaBena9813 pdaBena9813;
    private PdaBenpda_M pdaBenpda_M;
    private PdaBenpda_E pdaBenpda_E;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_I1;
    private DbsField pnd_Prcssng_Order;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaBena9813 = new PdaBena9813(parameters);
        pdaBenpda_M = new PdaBenpda_M(parameters);
        pdaBenpda_E = new PdaBenpda_E(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_I1 = localVariables.newFieldInRecord("pnd_I1", "#I1", FieldType.PACKED_DECIMAL, 3);
        pnd_Prcssng_Order = localVariables.newFieldInRecord("pnd_Prcssng_Order", "#PRCSSNG-ORDER", FieldType.STRING, 10);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Prcssng_Order.setInitialValue("RA");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Benn9813() throws Exception
    {
        super("Benn9813");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ERROR-INFO-SUB.##LAST-PROGRAM = *PROGRAM
        //*   IF WE HAVEN't read anything, default to the first Status
        if (condition(pdaBena9813.getBena9813_Pnd_Old_Stts().equals(" ")))                                                                                                //Natural: IF BENA9813.#OLD-STTS = ' '
        {
            pdaBena9813.getBena9813_Pnd_Old_Stts().setValue(pnd_Prcssng_Order.getSubstring(1,1));                                                                         //Natural: ASSIGN BENA9813.#OLD-STTS = SUBSTRING ( #PRCSSNG-ORDER,1,1 )
        }                                                                                                                                                                 //Natural: END-IF
        //*   IF WE HAVEN't finished the current (old) status, use it again
        if (condition(! (pdaBena9813.getBena9813_Pnd_Done_With_Crrnt_Stts().getBoolean())))                                                                               //Natural: IF NOT BENA9813.#DONE-WITH-CRRNT-STTS
        {
            pdaBena9813.getBena9813_Pnd_New_Stts().setValue(pdaBena9813.getBena9813_Pnd_Old_Stts());                                                                      //Natural: ASSIGN BENA9813.#NEW-STTS = BENA9813.#OLD-STTS
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*   IF WE HAVE FINISHED WITH THE CURRENT (OLD) STATUS, GET THE NEXT ONE
        pdaBena9813.getBena9813_Pnd_Done_With_Crrnt_Stts().reset();                                                                                                       //Natural: RESET BENA9813.#DONE-WITH-CRRNT-STTS
        DbsUtil.examine(new ExamineSource(pnd_Prcssng_Order), new ExamineSearch(pdaBena9813.getBena9813_Pnd_Old_Stts()), new ExamineGivingPosition(pnd_I1));              //Natural: EXAMINE #PRCSSNG-ORDER FOR BENA9813.#OLD-STTS GIVING POSITION #I1
        pnd_I1.nadd(1);                                                                                                                                                   //Natural: ADD 1 TO #I1
        pdaBena9813.getBena9813_Pnd_New_Stts().setValue(pnd_Prcssng_Order.getSubstring(pnd_I1.getInt(),1));                                                               //Natural: ASSIGN BENA9813.#NEW-STTS = SUBSTRING ( #PRCSSNG-ORDER,#I1,1 )
        //*  IF THE NEXT STATUS IS BLANK, WE're at the end of the list, and
        //*   AT THE END OF THE PROCESSING FOR THE DAY
        if (condition(pdaBena9813.getBena9813_Pnd_New_Stts().equals(" ")))                                                                                                //Natural: IF BENA9813.#NEW-STTS = ' '
        {
            pdaBena9813.getBena9813_Pnd_Done_For_Today().setValue(true);                                                                                                  //Natural: ASSIGN BENA9813.#DONE-FOR-TODAY = TRUE
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
