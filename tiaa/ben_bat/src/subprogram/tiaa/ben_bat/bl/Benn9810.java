/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:05:55 PM
**        * FROM NATURAL SUBPROGRAM : Benn9810
************************************************************
**        * FILE NAME            : Benn9810.java
**        * CLASS NAME           : Benn9810
**        * INSTANCE NAME        : Benn9810
************************************************************
************************************************************************
* PROGRAM  : BENN9810
* SYSTEM   : BENEFICIARY-SYSTEM
* TITLE    : UPDATE BENE RECORDS' STATUS
* GENERATED: JULY 1, 1999
************************************************************************
*    DATE      USERID                   DESCRIPTION
* MM/DD/YYYY  XXXXXXXX  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
* 04/01/2017  DURAND    PIN EXPANSION N7 TO N12
* 11/07/2000  WEBBJ     UPDATE ALL RECS FOR A CONTRACT, REGARDLESS OF
*                       T/C IND (ACIS CORR)
************************************************************************

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benn9810 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaBena9810 pdaBena9810;
    private PdaBenpda_M pdaBenpda_M;
    private PdaBenpda_E pdaBenpda_E;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_bc;
    private DbsField bc_Bc_Stat;
    private DbsField bc_Bc_Tiaa_Cref_Ind;

    private DataAccessProgramView vw_bd;
    private DbsField bd_Bd_Stat;
    private DbsField bd_Bd_Tiaa_Cref_Ind;

    private DataAccessProgramView vw_bm;
    private DbsField bm_Bm_Stat;
    private DbsField bm_Bm_Tiaa_Cref_Ind;
    private DbsField pnd_I1;
    private DbsField pnd_J1;
    private DbsField pnd_Pin_Cntrct_Ind_Key_Lo;

    private DbsGroup pnd_Pin_Cntrct_Ind_Key_Lo__R_Field_1;
    private DbsField pnd_Pin_Cntrct_Ind_Key_Lo_Pnd_Pin_Lo;
    private DbsField pnd_Pin_Cntrct_Ind_Key_Lo_Pnd_Tiaa_Cntrct_Lo;
    private DbsField pnd_Pin_Cntrct_Ind_Key_Lo_Pnd_Tiaa_Cref_Ind_Lo;
    private DbsField pnd_Pin_Cntrct_Ind_Key_Hi;

    private DbsGroup pnd_Pin_Cntrct_Ind_Key_Hi__R_Field_2;
    private DbsField pnd_Pin_Cntrct_Ind_Key_Hi_Pnd_Pin_Hi;
    private DbsField pnd_Pin_Cntrct_Ind_Key_Hi_Pnd_Tiaa_Cntrct_Hi;
    private DbsField pnd_Pin_Cntrct_Ind_Key_Hi_Pnd_Tiaa_Cref_Ind_Hi;
    private DbsField pnd_Stat_Pin_Cntrct_Ind_Key_Lo;

    private DbsGroup pnd_Stat_Pin_Cntrct_Ind_Key_Lo__R_Field_3;
    private DbsField pnd_Stat_Pin_Cntrct_Ind_Key_Lo_Pnd_Stat_Lo;
    private DbsField pnd_Stat_Pin_Cntrct_Ind_Key_Lo_Pnd_Pin_Lo;
    private DbsField pnd_Stat_Pin_Cntrct_Ind_Key_Lo_Pnd_Tiaa_Cntrct_Lo;
    private DbsField pnd_Stat_Pin_Cntrct_Ind_Key_Lo_Pnd_Tiaa_Cref_Ind_Lo;
    private DbsField pnd_Stat_Pin_Cntrct_Ind_Key_Hi;

    private DbsGroup pnd_Stat_Pin_Cntrct_Ind_Key_Hi__R_Field_4;
    private DbsField pnd_Stat_Pin_Cntrct_Ind_Key_Hi_Pnd_Stat_Hi;
    private DbsField pnd_Stat_Pin_Cntrct_Ind_Key_Hi_Pnd_Pin_Hi;
    private DbsField pnd_Stat_Pin_Cntrct_Ind_Key_Hi_Pnd_Tiaa_Cntrct_Hi;
    private DbsField pnd_Stat_Pin_Cntrct_Ind_Key_Hi_Pnd_Tiaa_Cref_Ind_Hi;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaBena9810 = new PdaBena9810(parameters);
        pdaBenpda_M = new PdaBenpda_M(parameters);
        pdaBenpda_E = new PdaBenpda_E(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_bc = new DataAccessProgramView(new NameInfo("vw_bc", "BC"), "BENE_CONTRACT_12", "BENE_CONTRACT");
        bc_Bc_Stat = vw_bc.getRecord().newFieldInGroup("bc_Bc_Stat", "BC-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_STAT");
        bc_Bc_Stat.setDdmHeader("STATUS");
        bc_Bc_Tiaa_Cref_Ind = vw_bc.getRecord().newFieldInGroup("bc_Bc_Tiaa_Cref_Ind", "BC-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_TIAA_CREF_IND");
        bc_Bc_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        registerRecord(vw_bc);

        vw_bd = new DataAccessProgramView(new NameInfo("vw_bd", "BD"), "BENE_DESIGNATION_12", "BENE_DESIGNATION");
        bd_Bd_Stat = vw_bd.getRecord().newFieldInGroup("bd_Bd_Stat", "BD-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_STAT");
        bd_Bd_Stat.setDdmHeader("STAT");
        bd_Bd_Tiaa_Cref_Ind = vw_bd.getRecord().newFieldInGroup("bd_Bd_Tiaa_Cref_Ind", "BD-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_TIAA_CREF_IND");
        bd_Bd_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        registerRecord(vw_bd);

        vw_bm = new DataAccessProgramView(new NameInfo("vw_bm", "BM"), "BENE_MOS_12", "BENE_MOS");
        bm_Bm_Stat = vw_bm.getRecord().newFieldInGroup("bm_Bm_Stat", "BM-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BM_STAT");
        bm_Bm_Stat.setDdmHeader("STAT");
        bm_Bm_Tiaa_Cref_Ind = vw_bm.getRecord().newFieldInGroup("bm_Bm_Tiaa_Cref_Ind", "BM-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BM_TIAA_CREF_IND");
        bm_Bm_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        registerRecord(vw_bm);

        pnd_I1 = localVariables.newFieldInRecord("pnd_I1", "#I1", FieldType.PACKED_DECIMAL, 3);
        pnd_J1 = localVariables.newFieldInRecord("pnd_J1", "#J1", FieldType.PACKED_DECIMAL, 3);
        pnd_Pin_Cntrct_Ind_Key_Lo = localVariables.newFieldInRecord("pnd_Pin_Cntrct_Ind_Key_Lo", "#PIN-CNTRCT-IND-KEY-LO", FieldType.STRING, 23);

        pnd_Pin_Cntrct_Ind_Key_Lo__R_Field_1 = localVariables.newGroupInRecord("pnd_Pin_Cntrct_Ind_Key_Lo__R_Field_1", "REDEFINE", pnd_Pin_Cntrct_Ind_Key_Lo);
        pnd_Pin_Cntrct_Ind_Key_Lo_Pnd_Pin_Lo = pnd_Pin_Cntrct_Ind_Key_Lo__R_Field_1.newFieldInGroup("pnd_Pin_Cntrct_Ind_Key_Lo_Pnd_Pin_Lo", "#PIN-LO", 
            FieldType.NUMERIC, 12);
        pnd_Pin_Cntrct_Ind_Key_Lo_Pnd_Tiaa_Cntrct_Lo = pnd_Pin_Cntrct_Ind_Key_Lo__R_Field_1.newFieldInGroup("pnd_Pin_Cntrct_Ind_Key_Lo_Pnd_Tiaa_Cntrct_Lo", 
            "#TIAA-CNTRCT-LO", FieldType.STRING, 10);
        pnd_Pin_Cntrct_Ind_Key_Lo_Pnd_Tiaa_Cref_Ind_Lo = pnd_Pin_Cntrct_Ind_Key_Lo__R_Field_1.newFieldInGroup("pnd_Pin_Cntrct_Ind_Key_Lo_Pnd_Tiaa_Cref_Ind_Lo", 
            "#TIAA-CREF-IND-LO", FieldType.STRING, 1);
        pnd_Pin_Cntrct_Ind_Key_Hi = localVariables.newFieldInRecord("pnd_Pin_Cntrct_Ind_Key_Hi", "#PIN-CNTRCT-IND-KEY-HI", FieldType.STRING, 23);

        pnd_Pin_Cntrct_Ind_Key_Hi__R_Field_2 = localVariables.newGroupInRecord("pnd_Pin_Cntrct_Ind_Key_Hi__R_Field_2", "REDEFINE", pnd_Pin_Cntrct_Ind_Key_Hi);
        pnd_Pin_Cntrct_Ind_Key_Hi_Pnd_Pin_Hi = pnd_Pin_Cntrct_Ind_Key_Hi__R_Field_2.newFieldInGroup("pnd_Pin_Cntrct_Ind_Key_Hi_Pnd_Pin_Hi", "#PIN-HI", 
            FieldType.NUMERIC, 12);
        pnd_Pin_Cntrct_Ind_Key_Hi_Pnd_Tiaa_Cntrct_Hi = pnd_Pin_Cntrct_Ind_Key_Hi__R_Field_2.newFieldInGroup("pnd_Pin_Cntrct_Ind_Key_Hi_Pnd_Tiaa_Cntrct_Hi", 
            "#TIAA-CNTRCT-HI", FieldType.STRING, 10);
        pnd_Pin_Cntrct_Ind_Key_Hi_Pnd_Tiaa_Cref_Ind_Hi = pnd_Pin_Cntrct_Ind_Key_Hi__R_Field_2.newFieldInGroup("pnd_Pin_Cntrct_Ind_Key_Hi_Pnd_Tiaa_Cref_Ind_Hi", 
            "#TIAA-CREF-IND-HI", FieldType.STRING, 1);
        pnd_Stat_Pin_Cntrct_Ind_Key_Lo = localVariables.newFieldInRecord("pnd_Stat_Pin_Cntrct_Ind_Key_Lo", "#STAT-PIN-CNTRCT-IND-KEY-LO", FieldType.STRING, 
            24);

        pnd_Stat_Pin_Cntrct_Ind_Key_Lo__R_Field_3 = localVariables.newGroupInRecord("pnd_Stat_Pin_Cntrct_Ind_Key_Lo__R_Field_3", "REDEFINE", pnd_Stat_Pin_Cntrct_Ind_Key_Lo);
        pnd_Stat_Pin_Cntrct_Ind_Key_Lo_Pnd_Stat_Lo = pnd_Stat_Pin_Cntrct_Ind_Key_Lo__R_Field_3.newFieldInGroup("pnd_Stat_Pin_Cntrct_Ind_Key_Lo_Pnd_Stat_Lo", 
            "#STAT-LO", FieldType.STRING, 1);
        pnd_Stat_Pin_Cntrct_Ind_Key_Lo_Pnd_Pin_Lo = pnd_Stat_Pin_Cntrct_Ind_Key_Lo__R_Field_3.newFieldInGroup("pnd_Stat_Pin_Cntrct_Ind_Key_Lo_Pnd_Pin_Lo", 
            "#PIN-LO", FieldType.NUMERIC, 12);
        pnd_Stat_Pin_Cntrct_Ind_Key_Lo_Pnd_Tiaa_Cntrct_Lo = pnd_Stat_Pin_Cntrct_Ind_Key_Lo__R_Field_3.newFieldInGroup("pnd_Stat_Pin_Cntrct_Ind_Key_Lo_Pnd_Tiaa_Cntrct_Lo", 
            "#TIAA-CNTRCT-LO", FieldType.STRING, 10);
        pnd_Stat_Pin_Cntrct_Ind_Key_Lo_Pnd_Tiaa_Cref_Ind_Lo = pnd_Stat_Pin_Cntrct_Ind_Key_Lo__R_Field_3.newFieldInGroup("pnd_Stat_Pin_Cntrct_Ind_Key_Lo_Pnd_Tiaa_Cref_Ind_Lo", 
            "#TIAA-CREF-IND-LO", FieldType.STRING, 1);
        pnd_Stat_Pin_Cntrct_Ind_Key_Hi = localVariables.newFieldInRecord("pnd_Stat_Pin_Cntrct_Ind_Key_Hi", "#STAT-PIN-CNTRCT-IND-KEY-HI", FieldType.STRING, 
            24);

        pnd_Stat_Pin_Cntrct_Ind_Key_Hi__R_Field_4 = localVariables.newGroupInRecord("pnd_Stat_Pin_Cntrct_Ind_Key_Hi__R_Field_4", "REDEFINE", pnd_Stat_Pin_Cntrct_Ind_Key_Hi);
        pnd_Stat_Pin_Cntrct_Ind_Key_Hi_Pnd_Stat_Hi = pnd_Stat_Pin_Cntrct_Ind_Key_Hi__R_Field_4.newFieldInGroup("pnd_Stat_Pin_Cntrct_Ind_Key_Hi_Pnd_Stat_Hi", 
            "#STAT-HI", FieldType.STRING, 1);
        pnd_Stat_Pin_Cntrct_Ind_Key_Hi_Pnd_Pin_Hi = pnd_Stat_Pin_Cntrct_Ind_Key_Hi__R_Field_4.newFieldInGroup("pnd_Stat_Pin_Cntrct_Ind_Key_Hi_Pnd_Pin_Hi", 
            "#PIN-HI", FieldType.NUMERIC, 12);
        pnd_Stat_Pin_Cntrct_Ind_Key_Hi_Pnd_Tiaa_Cntrct_Hi = pnd_Stat_Pin_Cntrct_Ind_Key_Hi__R_Field_4.newFieldInGroup("pnd_Stat_Pin_Cntrct_Ind_Key_Hi_Pnd_Tiaa_Cntrct_Hi", 
            "#TIAA-CNTRCT-HI", FieldType.STRING, 10);
        pnd_Stat_Pin_Cntrct_Ind_Key_Hi_Pnd_Tiaa_Cref_Ind_Hi = pnd_Stat_Pin_Cntrct_Ind_Key_Hi__R_Field_4.newFieldInGroup("pnd_Stat_Pin_Cntrct_Ind_Key_Hi_Pnd_Tiaa_Cref_Ind_Hi", 
            "#TIAA-CREF-IND-HI", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_bc.reset();
        vw_bd.reset();
        vw_bm.reset();

        localVariables.reset();
        pnd_Pin_Cntrct_Ind_Key_Lo.setInitialValue("                  ");
        pnd_Pin_Cntrct_Ind_Key_Hi.setInitialValue("                 T");
        pnd_Stat_Pin_Cntrct_Ind_Key_Lo.setInitialValue("                   ");
        pnd_Stat_Pin_Cntrct_Ind_Key_Hi.setInitialValue("                  T");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Benn9810() throws Exception
    {
        super("Benn9810");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ERROR-INFO-SUB.##LAST-PROGRAM := *PROGRAM
        pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().reset();                                                                                                        //Natural: RESET MSG-INFO-SUB.##RETURN-CODE MSG-INFO-SUB.##MSG
        pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().reset();
                                                                                                                                                                          //Natural: PERFORM BUILD-SUPER
        sub_Build_Super();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PROCESS-CONTRACT
        sub_Process_Contract();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PROCESS-MOS
        sub_Process_Mos();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PROCESS-DESIGNATION
        sub_Process_Designation();
        if (condition(Global.isEscape())) {return;}
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUILD-SUPER
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-CONTRACT
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-MOS
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-DESIGNATION
        //* ***********************************************************************
    }
    private void sub_Build_Super() throws Exception                                                                                                                       //Natural: BUILD-SUPER
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Stat_Pin_Cntrct_Ind_Key_Lo_Pnd_Stat_Lo.setValue(pdaBena9810.getBena9810_Pnd_Old_Stts());                                                                      //Natural: ASSIGN #STAT-PIN-CNTRCT-IND-KEY-LO.#STAT-LO := BENA9810.#OLD-STTS
        pnd_Stat_Pin_Cntrct_Ind_Key_Hi_Pnd_Stat_Hi.setValue(pdaBena9810.getBena9810_Pnd_Old_Stts());                                                                      //Natural: ASSIGN #STAT-PIN-CNTRCT-IND-KEY-HI.#STAT-HI := BENA9810.#OLD-STTS
        pnd_Stat_Pin_Cntrct_Ind_Key_Lo_Pnd_Pin_Lo.setValue(pdaBena9810.getBena9810_Pnd_Pin());                                                                            //Natural: ASSIGN #STAT-PIN-CNTRCT-IND-KEY-LO.#PIN-LO := BENA9810.#PIN
        pnd_Stat_Pin_Cntrct_Ind_Key_Hi_Pnd_Pin_Hi.setValue(pdaBena9810.getBena9810_Pnd_Pin());                                                                            //Natural: ASSIGN #STAT-PIN-CNTRCT-IND-KEY-HI.#PIN-HI := BENA9810.#PIN
        pnd_Stat_Pin_Cntrct_Ind_Key_Lo_Pnd_Tiaa_Cntrct_Lo.setValue(pdaBena9810.getBena9810_Pnd_Tiaa_Cntrct());                                                            //Natural: ASSIGN #STAT-PIN-CNTRCT-IND-KEY-LO.#TIAA-CNTRCT-LO := BENA9810.#TIAA-CNTRCT
        pnd_Stat_Pin_Cntrct_Ind_Key_Hi_Pnd_Tiaa_Cntrct_Hi.setValue(pdaBena9810.getBena9810_Pnd_Tiaa_Cntrct());                                                            //Natural: ASSIGN #STAT-PIN-CNTRCT-IND-KEY-HI.#TIAA-CNTRCT-HI := BENA9810.#TIAA-CNTRCT
        pnd_Pin_Cntrct_Ind_Key_Lo_Pnd_Pin_Lo.setValue(pdaBena9810.getBena9810_Pnd_Pin());                                                                                 //Natural: ASSIGN #PIN-CNTRCT-IND-KEY-LO.#PIN-LO := BENA9810.#PIN
        pnd_Pin_Cntrct_Ind_Key_Hi_Pnd_Pin_Hi.setValue(pdaBena9810.getBena9810_Pnd_Pin());                                                                                 //Natural: ASSIGN #PIN-CNTRCT-IND-KEY-HI.#PIN-HI := BENA9810.#PIN
        pnd_Pin_Cntrct_Ind_Key_Lo_Pnd_Tiaa_Cntrct_Lo.setValue(pdaBena9810.getBena9810_Pnd_Tiaa_Cntrct());                                                                 //Natural: ASSIGN #PIN-CNTRCT-IND-KEY-LO.#TIAA-CNTRCT-LO := BENA9810.#TIAA-CNTRCT
        pnd_Pin_Cntrct_Ind_Key_Hi_Pnd_Tiaa_Cntrct_Hi.setValue(pdaBena9810.getBena9810_Pnd_Tiaa_Cntrct());                                                                 //Natural: ASSIGN #PIN-CNTRCT-IND-KEY-HI.#TIAA-CNTRCT-HI := BENA9810.#TIAA-CNTRCT
        //*  BUILD-SUPER
    }
    private void sub_Process_Contract() throws Exception                                                                                                                  //Natural: PROCESS-CONTRACT
    {
        if (BLNatReinput.isReinput()) return;

        vw_bc.startDatabaseFind                                                                                                                                           //Natural: FIND BC WITH BC-STAT-PIN-CNTRCT-IND-KEY = #STAT-PIN-CNTRCT-IND-KEY-LO THRU #STAT-PIN-CNTRCT-IND-KEY-HI
        (
        "FBC",
        new Wc[] { new Wc("BC_STAT_PIN_CNTRCT_IND_KEY", "<=", pnd_Stat_Pin_Cntrct_Ind_Key_Hi, WcType.WITH) }
        );
        FBC:
        while (condition(vw_bc.readNextRow("FBC")))
        {
            vw_bc.setIfNotFoundControlFlag(false);
            if (condition((bc_Bc_Tiaa_Cref_Ind.equals("C") && pdaBena9810.getBena9810_Pnd_Tiaa_Cref_Ind().equals("T")) || (bc_Bc_Tiaa_Cref_Ind.equals("T")                //Natural: IF ( BC-TIAA-CREF-IND = 'C' AND BENA9810.#TIAA-CREF-IND = 'T' ) OR ( BC-TIAA-CREF-IND = 'T' AND BENA9810.#TIAA-CREF-IND = 'C' )
                && pdaBena9810.getBena9810_Pnd_Tiaa_Cref_Ind().equals("C"))))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            bc_Bc_Stat.setValue(pdaBena9810.getBena9810_Pnd_New_Stts());                                                                                                  //Natural: ASSIGN BC-STAT := BENA9810.#NEW-STTS
            vw_bc.updateDBRow("FBC");                                                                                                                                     //Natural: UPDATE ( FBC. )
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  PROCESS-CONTRACT
    }
    private void sub_Process_Mos() throws Exception                                                                                                                       //Natural: PROCESS-MOS
    {
        if (BLNatReinput.isReinput()) return;

        vw_bm.startDatabaseFind                                                                                                                                           //Natural: FIND BM WITH BM-STAT-PIN-CNTRCT-IND-KEY = #STAT-PIN-CNTRCT-IND-KEY-LO THRU #STAT-PIN-CNTRCT-IND-KEY-HI
        (
        "FBM",
        new Wc[] { new Wc("BM_STAT_PIN_CNTRCT_IND_KEY", "<=", pnd_Stat_Pin_Cntrct_Ind_Key_Hi, WcType.WITH) }
        );
        FBM:
        while (condition(vw_bm.readNextRow("FBM")))
        {
            vw_bm.setIfNotFoundControlFlag(false);
            if (condition((bm_Bm_Tiaa_Cref_Ind.equals("C") && pdaBena9810.getBena9810_Pnd_Tiaa_Cref_Ind().equals("T")) || (bm_Bm_Tiaa_Cref_Ind.equals("T")                //Natural: IF ( BM-TIAA-CREF-IND = 'C' AND BENA9810.#TIAA-CREF-IND = 'T' ) OR ( BM-TIAA-CREF-IND = 'T' AND BENA9810.#TIAA-CREF-IND = 'C' )
                && pdaBena9810.getBena9810_Pnd_Tiaa_Cref_Ind().equals("C"))))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            bm_Bm_Stat.setValue(pdaBena9810.getBena9810_Pnd_New_Stts());                                                                                                  //Natural: ASSIGN BM-STAT := BENA9810.#NEW-STTS
            vw_bm.updateDBRow("FBM");                                                                                                                                     //Natural: UPDATE ( FBM. )
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  PROCESS-MOS
    }
    private void sub_Process_Designation() throws Exception                                                                                                               //Natural: PROCESS-DESIGNATION
    {
        if (BLNatReinput.isReinput()) return;

        vw_bd.startDatabaseFind                                                                                                                                           //Natural: FIND BD WITH BD-PIN-CNTRCT-IND-KEY = #PIN-CNTRCT-IND-KEY-LO THRU #PIN-CNTRCT-IND-KEY-HI
        (
        "FBD",
        new Wc[] { new Wc("BD_PIN_CNTRCT_IND_KEY", "<=", pnd_Pin_Cntrct_Ind_Key_Hi, WcType.WITH) }
        );
        FBD:
        while (condition(vw_bd.readNextRow("FBD")))
        {
            vw_bd.setIfNotFoundControlFlag(false);
            if (condition(bd_Bd_Stat.notEquals(pdaBena9810.getBena9810_Pnd_Old_Stts())))                                                                                  //Natural: REJECT IF BD-STAT NE BENA9810.#OLD-STTS
            {
                continue;
            }
            if (condition((bd_Bd_Tiaa_Cref_Ind.equals("C") && pdaBena9810.getBena9810_Pnd_Tiaa_Cref_Ind().equals("T")) || (bd_Bd_Tiaa_Cref_Ind.equals("T")                //Natural: IF ( BD-TIAA-CREF-IND = 'C' AND BENA9810.#TIAA-CREF-IND = 'T' ) OR ( BD-TIAA-CREF-IND = 'T' AND BENA9810.#TIAA-CREF-IND = 'C' )
                && pdaBena9810.getBena9810_Pnd_Tiaa_Cref_Ind().equals("C"))))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            bd_Bd_Stat.setValue(pdaBena9810.getBena9810_Pnd_New_Stts());                                                                                                  //Natural: ASSIGN BD-STAT := BENA9810.#NEW-STTS
            vw_bd.updateDBRow("FBD");                                                                                                                                     //Natural: UPDATE ( FBD. )
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  PROCESS-DESIGNATION
    }

    //
}
