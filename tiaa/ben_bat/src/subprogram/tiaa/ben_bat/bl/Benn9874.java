/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:07:12 PM
**        * FROM NATURAL SUBPROGRAM : Benn9874
************************************************************
**        * FILE NAME            : Benn9874.java
**        * CLASS NAME           : Benn9874
**        * INSTANCE NAME        : Benn9874
************************************************************
************************************************************************
* PROGRAM  : BENN9874
* SYSTEM   : BENEFICIARY-SYSTEM
* TITLE    : CHECK FOR BENE RECORDS
* GENERATED: OCTOBER, 18, 2001
*
* HISTORY
* CHANGED ON JUL 1,99 BY DEPAUL FOR RELEASE ____
* >
* > JW 5/14/01 - LOAD COMPLETE STATUS AND LAST-DES ARRAY.
* > CLONED FROM BENN9814 FONTOUR
************************************************************************
*    DATE      USERID                   DESCRIPTION
* 04/01/2017  DURAND    PIN EXPANSION N7 TO N12
* MM/DD/YYYY  XXXXXXXX  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
************************************************************************

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benn9874 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaBena9874 pdaBena9874;
    private PdaBenpda_M pdaBenpda_M;
    private PdaBenpda_E pdaBenpda_E;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_bene_Contract;
    private DbsField bene_Contract_Bc_Stat;
    private DbsField bene_Contract_Bc_Last_Dsgntn_Srce;

    private DataAccessProgramView vw_bene_Designation;
    private DbsField bene_Designation_Bd_Stat;

    private DataAccessProgramView vw_bene_Mos;
    private DbsField bene_Mos_Bm_Stat;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Pin_Cntrct_Ind_Key;

    private DbsGroup pnd_Pin_Cntrct_Ind_Key__R_Field_1;
    private DbsField pnd_Pin_Cntrct_Ind_Key_Pnd_Pin;
    private DbsField pnd_Pin_Cntrct_Ind_Key_Pnd_Tiaa_Cntrct;
    private DbsField pnd_Pin_Cntrct_Ind_Key_Pnd_Tiaa_Cref_Ind;
    private DbsField pnd_Stat_Pin_Cntrct_Ind_Key;

    private DbsGroup pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_2;
    private DbsField pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Stat;
    private DbsField pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Pin;
    private DbsField pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Tiaa_Cntrct;
    private DbsField pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Tiaa_Cref_Ind;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaBena9874 = new PdaBena9874(parameters);
        pdaBenpda_M = new PdaBenpda_M(parameters);
        pdaBenpda_E = new PdaBenpda_E(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_bene_Contract = new DataAccessProgramView(new NameInfo("vw_bene_Contract", "BENE-CONTRACT"), "BENE_CONTRACT_12", "BENE_CONTRACT");
        bene_Contract_Bc_Stat = vw_bene_Contract.getRecord().newFieldInGroup("bene_Contract_Bc_Stat", "BC-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_STAT");
        bene_Contract_Bc_Stat.setDdmHeader("STATUS");
        bene_Contract_Bc_Last_Dsgntn_Srce = vw_bene_Contract.getRecord().newFieldInGroup("bene_Contract_Bc_Last_Dsgntn_Srce", "BC-LAST-DSGNTN-SRCE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BC_LAST_DSGNTN_SRCE");
        bene_Contract_Bc_Last_Dsgntn_Srce.setDdmHeader("LAST/DSGNTN/SOURCE");
        registerRecord(vw_bene_Contract);

        vw_bene_Designation = new DataAccessProgramView(new NameInfo("vw_bene_Designation", "BENE-DESIGNATION"), "BENE_DESIGNATION_12", "BENE_DESIGNATION");
        bene_Designation_Bd_Stat = vw_bene_Designation.getRecord().newFieldInGroup("bene_Designation_Bd_Stat", "BD-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_STAT");
        bene_Designation_Bd_Stat.setDdmHeader("STAT");
        registerRecord(vw_bene_Designation);

        vw_bene_Mos = new DataAccessProgramView(new NameInfo("vw_bene_Mos", "BENE-MOS"), "BENE_MOS_12", "BENE_MOS");
        bene_Mos_Bm_Stat = vw_bene_Mos.getRecord().newFieldInGroup("bene_Mos_Bm_Stat", "BM-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BM_STAT");
        bene_Mos_Bm_Stat.setDdmHeader("STAT");
        registerRecord(vw_bene_Mos);

        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_Pin_Cntrct_Ind_Key = localVariables.newFieldInRecord("pnd_Pin_Cntrct_Ind_Key", "#PIN-CNTRCT-IND-KEY", FieldType.STRING, 23);

        pnd_Pin_Cntrct_Ind_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Pin_Cntrct_Ind_Key__R_Field_1", "REDEFINE", pnd_Pin_Cntrct_Ind_Key);
        pnd_Pin_Cntrct_Ind_Key_Pnd_Pin = pnd_Pin_Cntrct_Ind_Key__R_Field_1.newFieldInGroup("pnd_Pin_Cntrct_Ind_Key_Pnd_Pin", "#PIN", FieldType.NUMERIC, 
            12);
        pnd_Pin_Cntrct_Ind_Key_Pnd_Tiaa_Cntrct = pnd_Pin_Cntrct_Ind_Key__R_Field_1.newFieldInGroup("pnd_Pin_Cntrct_Ind_Key_Pnd_Tiaa_Cntrct", "#TIAA-CNTRCT", 
            FieldType.STRING, 10);
        pnd_Pin_Cntrct_Ind_Key_Pnd_Tiaa_Cref_Ind = pnd_Pin_Cntrct_Ind_Key__R_Field_1.newFieldInGroup("pnd_Pin_Cntrct_Ind_Key_Pnd_Tiaa_Cref_Ind", "#TIAA-CREF-IND", 
            FieldType.STRING, 1);
        pnd_Stat_Pin_Cntrct_Ind_Key = localVariables.newFieldInRecord("pnd_Stat_Pin_Cntrct_Ind_Key", "#STAT-PIN-CNTRCT-IND-KEY", FieldType.STRING, 24);

        pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_2", "REDEFINE", pnd_Stat_Pin_Cntrct_Ind_Key);
        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Stat = pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_2.newFieldInGroup("pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Stat", "#STAT", 
            FieldType.STRING, 1);
        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Pin = pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_2.newFieldInGroup("pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Pin", "#PIN", FieldType.NUMERIC, 
            12);
        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Tiaa_Cntrct = pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_2.newFieldInGroup("pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Tiaa_Cntrct", 
            "#TIAA-CNTRCT", FieldType.STRING, 10);
        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Tiaa_Cref_Ind = pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_2.newFieldInGroup("pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Tiaa_Cref_Ind", 
            "#TIAA-CREF-IND", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_bene_Contract.reset();
        vw_bene_Designation.reset();
        vw_bene_Mos.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Benn9874() throws Exception
    {
        super("Benn9874");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ERROR-INFO-SUB.##LAST-PROGRAM := *PROGRAM
        //*  FOR ESCAPE BOTTOM
        MAIN_ROUTINE:                                                                                                                                                     //Natural: REPEAT
        while (condition(whileTrue))
        {
                                                                                                                                                                          //Natural: PERFORM INITIALIZATIONS
            sub_Initializations();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("MAIN_ROUTINE"))) break;
                else if (condition(Global.isEscapeBottomImmediate("MAIN_ROUTINE"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM BUILD-SUPER
            sub_Build_Super();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("MAIN_ROUTINE"))) break;
                else if (condition(Global.isEscapeBottomImmediate("MAIN_ROUTINE"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM FIND-CONTRACT
            sub_Find_Contract();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("MAIN_ROUTINE"))) break;
                else if (condition(Global.isEscapeBottomImmediate("MAIN_ROUTINE"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM FIND-MOS
            sub_Find_Mos();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("MAIN_ROUTINE"))) break;
                else if (condition(Global.isEscapeBottomImmediate("MAIN_ROUTINE"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM FIND-DESIGNATION
            sub_Find_Designation();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("MAIN_ROUTINE"))) break;
                else if (condition(Global.isEscapeBottomImmediate("MAIN_ROUTINE"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (true) break MAIN_ROUTINE;                                                                                                                                 //Natural: ESCAPE BOTTOM ( MAIN-ROUTINE. )
            //* ************************************
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ESCAPE-MAIN-ROUTINE
            //*  MAIN-ROUTINE.
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUILD-SUPER
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-CONTRACT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-MOS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-DESIGNATION
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZATIONS
    }
    private void sub_Escape_Main_Routine() throws Exception                                                                                                               //Natural: ESCAPE-MAIN-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( MAIN-ROUTINE. )
        Global.setEscapeCode(EscapeType.Bottom, "MAIN_ROUTINE");
        if (true) return;
        //*  ESCAPE-MAIN-ROUTINE
    }
    private void sub_Build_Super() throws Exception                                                                                                                       //Natural: BUILD-SUPER
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Stat.setValue(pdaBena9874.getBena9874_Pnd_Stts());                                                                                //Natural: ASSIGN #STAT-PIN-CNTRCT-IND-KEY.#STAT := BENA9874.#STTS
        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Pin.setValue(pdaBena9874.getBena9874_Pnd_Pin());                                                                                  //Natural: ASSIGN #STAT-PIN-CNTRCT-IND-KEY.#PIN := BENA9874.#PIN
        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Tiaa_Cntrct.setValue(pdaBena9874.getBena9874_Pnd_Tiaa_Cntrct());                                                                  //Natural: ASSIGN #STAT-PIN-CNTRCT-IND-KEY.#TIAA-CNTRCT := BENA9874.#TIAA-CNTRCT
        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Tiaa_Cref_Ind.setValue(pdaBena9874.getBena9874_Pnd_Tiaa_Cref_Ind());                                                              //Natural: ASSIGN #STAT-PIN-CNTRCT-IND-KEY.#TIAA-CREF-IND := BENA9874.#TIAA-CREF-IND
        pnd_Pin_Cntrct_Ind_Key_Pnd_Pin.setValue(pdaBena9874.getBena9874_Pnd_Pin());                                                                                       //Natural: ASSIGN #PIN-CNTRCT-IND-KEY.#PIN := BENA9874.#PIN
        pnd_Pin_Cntrct_Ind_Key_Pnd_Tiaa_Cntrct.setValue(pdaBena9874.getBena9874_Pnd_Tiaa_Cntrct());                                                                       //Natural: ASSIGN #PIN-CNTRCT-IND-KEY.#TIAA-CNTRCT := BENA9874.#TIAA-CNTRCT
        pnd_Pin_Cntrct_Ind_Key_Pnd_Tiaa_Cref_Ind.setValue(pdaBena9874.getBena9874_Pnd_Tiaa_Cref_Ind());                                                                   //Natural: ASSIGN #PIN-CNTRCT-IND-KEY.#TIAA-CREF-IND := BENA9874.#TIAA-CREF-IND
        //*  BUILD-SUPER
    }
    private void sub_Find_Contract() throws Exception                                                                                                                     //Natural: FIND-CONTRACT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        vw_bene_Contract.startDatabaseFind                                                                                                                                //Natural: FIND ( 1 ) BENE-CONTRACT WITH BC-STAT-PIN-CNTRCT-IND-KEY = #STAT-PIN-CNTRCT-IND-KEY
        (
        "FIND01",
        new Wc[] { new Wc("BC_STAT_PIN_CNTRCT_IND_KEY", "=", pnd_Stat_Pin_Cntrct_Ind_Key, WcType.WITH) },
        1
        );
        FIND01:
        while (condition(vw_bene_Contract.readNextRow("FIND01", true)))
        {
            vw_bene_Contract.setIfNotFoundControlFlag(false);
            if (condition(vw_bene_Contract.getAstCOUNTER().equals(0)))                                                                                                    //Natural: IF NO RECORDS
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            pdaBena9874.getBena9874_Pnd_Found().setValue(true);                                                                                                           //Natural: ASSIGN BENA9874.#FOUND := TRUE
            pdaBena9874.getBena9874_Pnd_Last_Srce().setValue(bene_Contract_Bc_Last_Dsgntn_Srce);                                                                          //Natural: ASSIGN #LAST-SRCE := BC-LAST-DSGNTN-SRCE
                                                                                                                                                                          //Natural: PERFORM ESCAPE-MAIN-ROUTINE
            sub_Escape_Main_Routine();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  FIND-CONTRACT
    }
    private void sub_Find_Mos() throws Exception                                                                                                                          //Natural: FIND-MOS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        vw_bene_Mos.startDatabaseFind                                                                                                                                     //Natural: FIND ( 1 ) BENE-MOS WITH BM-STAT-PIN-CNTRCT-IND-KEY = #STAT-PIN-CNTRCT-IND-KEY
        (
        "FIND02",
        new Wc[] { new Wc("BM_STAT_PIN_CNTRCT_IND_KEY", "=", pnd_Stat_Pin_Cntrct_Ind_Key, WcType.WITH) },
        1
        );
        FIND02:
        while (condition(vw_bene_Mos.readNextRow("FIND02", true)))
        {
            vw_bene_Mos.setIfNotFoundControlFlag(false);
            if (condition(vw_bene_Mos.getAstCOUNTER().equals(0)))                                                                                                         //Natural: IF NO RECORDS
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            pdaBena9874.getBena9874_Pnd_Found().setValue(true);                                                                                                           //Natural: ASSIGN BENA9874.#FOUND := TRUE
                                                                                                                                                                          //Natural: PERFORM ESCAPE-MAIN-ROUTINE
            sub_Escape_Main_Routine();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  FIND-MOS
    }
    private void sub_Find_Designation() throws Exception                                                                                                                  //Natural: FIND-DESIGNATION
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        vw_bene_Designation.startDatabaseFind                                                                                                                             //Natural: FIND BENE-DESIGNATION WITH BD-PIN-CNTRCT-IND-KEY = #PIN-CNTRCT-IND-KEY
        (
        "FIND03",
        new Wc[] { new Wc("BD_PIN_CNTRCT_IND_KEY", "=", pnd_Pin_Cntrct_Ind_Key, WcType.WITH) }
        );
        FIND03:
        while (condition(vw_bene_Designation.readNextRow("FIND03", true)))
        {
            vw_bene_Designation.setIfNotFoundControlFlag(false);
            if (condition(vw_bene_Designation.getAstCOUNTER().equals(0)))                                                                                                 //Natural: IF NO RECORDS
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            if (condition(bene_Designation_Bd_Stat.notEquals(pdaBena9874.getBena9874_Pnd_Stts())))                                                                        //Natural: REJECT IF BD-STAT NE BENA9874.#STTS
            {
                continue;
            }
            pdaBena9874.getBena9874_Pnd_Found().setValue(true);                                                                                                           //Natural: ASSIGN BENA9874.#FOUND := TRUE
                                                                                                                                                                          //Natural: PERFORM ESCAPE-MAIN-ROUTINE
            sub_Escape_Main_Routine();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  FIND-DESIGNATION
    }
    private void sub_Initializations() throws Exception                                                                                                                   //Natural: INITIALIZATIONS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().reset();                                                                                                        //Natural: RESET MSG-INFO-SUB.##RETURN-CODE MSG-INFO-SUB.##MSG
        pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().reset();
        //*  INITIALIZATIONS
    }

    //
}
