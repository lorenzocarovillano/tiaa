/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:05:35 PM
**        * FROM NATURAL SUBPROGRAM : Benn971
************************************************************
**        * FILE NAME            : Benn971.java
**        * CLASS NAME           : Benn971
**        * INSTANCE NAME        : Benn971
************************************************************
************************************************************************
* PROGRAM  : BENN971 (PIN=N12) - CLONE OF BENN970 (N7) FOR PIN EXPANSION
* SYSTEM   : BENEFICIARY-SYSTEM
* TITLE    : INTERFACE COMMON MODULE
* GENERATED: APRIL 1, 2017
*          : CAPTURE DESIGNATION AND STORE ON INTERFACE-FILE
************************************************************************
*    DATE      USERID                   DESCRIPTION
* 12/26/2019  DURAND    MOVE BENE ADDRESS, PHONE, GENDER FIELDS
* MM/DD/YYYY  XXXXXXXX  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
************************************************************************

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benn971 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaBena971 pdaBena971;
    private PdaBena9701 pdaBena9701;
    private PdaBena9704 pdaBena9704;
    private PdaBenpda_M pdaBenpda_M;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_bci;
    private DbsField bci_Intrfce_Stts;
    private DbsField bci_Rcrd_Crtd_For_Bsnss_Dte;
    private DbsField bci_Rcrd_Crtd_Dte;
    private DbsField bci_Rcrd_Crtd_Tme;
    private DbsField bci_Intrfcng_Systm;
    private DbsField bci_Rqstng_Systm;
    private DbsField bci_Rcrd_Last_Updt_Dte;
    private DbsField bci_Rcrd_Last_Updt_Tme;
    private DbsField bci_Rcrd_Last_Updt_Userid;
    private DbsField bci_Last_Prcssd_Bsnss_Dte;
    private DbsField bci_Intrfce_Cmpltd_Dte;
    private DbsField bci_Intrfce_Cmpltd_Tme;
    private DbsField bci_Dflt_To_Estate_Ind;
    private DbsField bci_Illgble_Ind;
    private DbsField bci_More_Than_Five_Benes_Ind;
    private DbsField bci_Intrfce_Mgrtn_Ind;
    private DbsField bci_More_Than_Thirty_Benes_Ind;
    private DbsField bci_Pin_Tiaa_Cntrct;

    private DbsGroup bci_Error_Table;
    private DbsField bci_Error_Txt;
    private DbsField bci_Error_Cde;
    private DbsField bci_Error_Dte;
    private DbsField bci_Error_Tme;
    private DbsField bci_Error_Pgm;
    private DbsField bci_Fldr_Log_Dte_Tme;
    private DbsField bci_Last_Dsgntn_Srce;
    private DbsField bci_Last_Dsgntn_System;
    private DbsField bci_Last_Dsgntn_Userid;
    private DbsField bci_Last_Dsgntn_Dte;
    private DbsField bci_Last_Dsgntn_Tme;
    private DbsField bci_Tiaa_Cref_Chng_Dte;
    private DbsField bci_Tiaa_Cref_Chng_Tme;
    private DbsField bci_Chng_Pwr_Atty;
    private DbsField bci_Cref_Cntrct;
    private DbsField bci_Tiaa_Cref_Ind;
    private DbsField bci_Cntrct_Type;
    private DbsField bci_Eff_Dte;
    private DbsField bci_Mos_Ind;
    private DbsField bci_Irrvcble_Ind;
    private DbsField bci_Pymnt_Chld_Dcsd_Ind;
    private DbsField bci_Exempt_Spouse_Rights;
    private DbsField bci_Spouse_Waived_Bnfts;
    private DbsField bci_Chng_New_Issue_Ind;
    private DbsField bci_Same_As_Ind;

    private DataAccessProgramView vw_bdi;
    private DbsField bdi_Intrfce_Stts;
    private DbsField bdi_Tiaa_Cref_Ind;
    private DbsField bdi_Seq_Nmbr;
    private DbsField bdi_Rcrd_Crtd_For_Bsnss_Dte;
    private DbsField bdi_Rcrd_Last_Updt_Dte;
    private DbsField bdi_Rcrd_Last_Updt_Tme;
    private DbsField bdi_Rcrd_Last_Updt_Userid;
    private DbsField bdi_Pin_Tiaa_Cntrct;
    private DbsField bdi_Bene_Type;
    private DbsField bdi_Rltn_Cde;
    private DbsField bdi_Bene_Name1;
    private DbsField bdi_Bene_Name2;
    private DbsField bdi_Rltn_Free_Txt;
    private DbsField bdi_Dte_Birth_Trust;
    private DbsField bdi_Ssn_Cde;
    private DbsField bdi_Ssn;
    private DbsField bdi_Prcnt_Frctn_Ind;
    private DbsField bdi_Share_Prcnt;
    private DbsField bdi_Share_Nmrtr;
    private DbsField bdi_Share_Dnmntr;
    private DbsField bdi_Irrvcble_Ind;
    private DbsField bdi_Sttlmnt_Rstrctn;
    private DbsField bdi_Spcl_Txt1;
    private DbsField bdi_Spcl_Txt2;
    private DbsField bdi_Spcl_Txt3;
    private DbsField bdi_Dflt_Estate;
    private DbsField bdi_Mdo_Calc_Bene;
    private DbsField bdi_Bene_Addr1;
    private DbsField bdi_Bene_Addr2;
    private DbsField bdi_Bene_Addr3_City;
    private DbsField bdi_Bene_State;
    private DbsField bdi_Bene_Zip;
    private DbsField bdi_Bene_Phone;
    private DbsField bdi_Bene_Gender;
    private DbsField bdi_Bene_Country;

    private DataAccessProgramView vw_bmi;
    private DbsField bmi_Intrfce_Stts;
    private DbsField bmi_Rcrd_Crtd_For_Bsnss_Dte;
    private DbsField bmi_Tiaa_Cref_Ind;
    private DbsField bmi_Pin_Tiaa_Cntrct;
    private DbsField bmi_Rcrd_Crtd_Dte;
    private DbsField bmi_Rcrd_Crtd_Tme;
    private DbsField bmi_Rcrd_Last_Updt_Dte;
    private DbsField bmi_Rcrd_Last_Updt_Tme;
    private DbsField bmi_Rcrd_Last_Updt_Userid;
    private DbsField bmi_Count_Castmos_Txt_Group;

    private DbsGroup bmi_Mos_Txt_Group;
    private DbsField bmi_Mos_Txt;
    private DbsField pnd_I1;
    private DbsField pnd_Pin_Tiaa_Cntrct;

    private DbsGroup pnd_Pin_Tiaa_Cntrct__R_Field_1;
    private DbsField pnd_Pin_Tiaa_Cntrct_Pnd_Pin;
    private DbsField pnd_Pin_Tiaa_Cntrct_Pnd_Tiaa_Cntrct;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaBena9701 = new PdaBena9701(localVariables);
        pdaBena9704 = new PdaBena9704(localVariables);
        pdaBenpda_M = new PdaBenpda_M(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaBena971 = new PdaBena971(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        vw_bci = new DataAccessProgramView(new NameInfo("vw_bci", "BCI"), "BENE_CONTRACT_INTERFACE_12", "BENE_CONT_INTFCE", DdmPeriodicGroups.getInstance().getGroups("BENE_CONTRACT_INTERFACE_12"));
        bci_Intrfce_Stts = vw_bci.getRecord().newFieldInGroup("bci_Intrfce_Stts", "INTRFCE-STTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, "INTRFCE_STTS");
        bci_Intrfce_Stts.setDdmHeader("INTRFCE/STATUS");
        bci_Rcrd_Crtd_For_Bsnss_Dte = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Crtd_For_Bsnss_Dte", "RCRD-CRTD-FOR-BSNSS-DTE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "RCRD_CRTD_FOR_BSNSS_DTE");
        bci_Rcrd_Crtd_For_Bsnss_Dte.setDdmHeader("RCRD CRTD/FOR/BSNSS DTE");
        bci_Rcrd_Crtd_Dte = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Crtd_Dte", "RCRD-CRTD-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_CRTD_DTE");
        bci_Rcrd_Crtd_Dte.setDdmHeader("RECORD/CREATED/DATE");
        bci_Rcrd_Crtd_Tme = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Crtd_Tme", "RCRD-CRTD-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "RCRD_CRTD_TME");
        bci_Rcrd_Crtd_Tme.setDdmHeader("RECORD/CREATED/TIME");
        bci_Intrfcng_Systm = vw_bci.getRecord().newFieldInGroup("bci_Intrfcng_Systm", "INTRFCNG-SYSTM", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "INTRFCNG_SYSTM");
        bci_Intrfcng_Systm.setDdmHeader("INTRFCNG/SYSTEM");
        bci_Rqstng_Systm = vw_bci.getRecord().newFieldInGroup("bci_Rqstng_Systm", "RQSTNG-SYSTM", FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQSTNG_SYSTM");
        bci_Rqstng_Systm.setDdmHeader("RQSTNG/SYSTEM");
        bci_Rcrd_Last_Updt_Dte = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Last_Updt_Dte", "RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_DTE");
        bci_Rcrd_Last_Updt_Dte.setDdmHeader("RECORD/LAST UPDT/DATE");
        bci_Rcrd_Last_Updt_Tme = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Last_Updt_Tme", "RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_TME");
        bci_Rcrd_Last_Updt_Tme.setDdmHeader("RECORD/LAST UPDT/TIME");
        bci_Rcrd_Last_Updt_Userid = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Last_Updt_Userid", "RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_USERID");
        bci_Rcrd_Last_Updt_Userid.setDdmHeader("RECORD/LAST UPDT/USER ID");
        bci_Last_Prcssd_Bsnss_Dte = vw_bci.getRecord().newFieldInGroup("bci_Last_Prcssd_Bsnss_Dte", "LAST-PRCSSD-BSNSS-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "LAST_PRCSSD_BSNSS_DTE");
        bci_Last_Prcssd_Bsnss_Dte.setDdmHeader("LAST PRCSSD/BUSINESS/DATE");
        bci_Intrfce_Cmpltd_Dte = vw_bci.getRecord().newFieldInGroup("bci_Intrfce_Cmpltd_Dte", "INTRFCE-CMPLTD-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "INTRFCE_CMPLTD_DTE");
        bci_Intrfce_Cmpltd_Dte.setDdmHeader("INTFCE/CMPLTD/DATE");
        bci_Intrfce_Cmpltd_Tme = vw_bci.getRecord().newFieldInGroup("bci_Intrfce_Cmpltd_Tme", "INTRFCE-CMPLTD-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "INTRFCE_CMPLTD_TME");
        bci_Intrfce_Cmpltd_Tme.setDdmHeader("INTRFCE/CMPLTD/TME");
        bci_Dflt_To_Estate_Ind = vw_bci.getRecord().newFieldInGroup("bci_Dflt_To_Estate_Ind", "DFLT-TO-ESTATE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "DFLT_TO_ESTATE_IND");
        bci_Dflt_To_Estate_Ind.setDdmHeader("DEFAULT/TO/ESTATE");
        bci_Illgble_Ind = vw_bci.getRecord().newFieldInGroup("bci_Illgble_Ind", "ILLGBLE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "ILLGBLE_IND");
        bci_Illgble_Ind.setDdmHeader("ILLGBLE/IND");
        bci_More_Than_Five_Benes_Ind = vw_bci.getRecord().newFieldInGroup("bci_More_Than_Five_Benes_Ind", "MORE-THAN-FIVE-BENES-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "MORE_THAN_FIVE_BENES_IND");
        bci_More_Than_Five_Benes_Ind.setDdmHeader("MORE/THAN 5/BENES");
        bci_Intrfce_Mgrtn_Ind = vw_bci.getRecord().newFieldInGroup("bci_Intrfce_Mgrtn_Ind", "INTRFCE-MGRTN-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "INTRFCE_MGRTN_IND");
        bci_Intrfce_Mgrtn_Ind.setDdmHeader("INTRFCE/MGRTN/IND");
        bci_More_Than_Thirty_Benes_Ind = vw_bci.getRecord().newFieldInGroup("bci_More_Than_Thirty_Benes_Ind", "MORE-THAN-THIRTY-BENES-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "MORE_THAN_THIRTY_BENES_IND");
        bci_More_Than_Thirty_Benes_Ind.setDdmHeader("MORE/THAN 30/BENES");
        bci_Pin_Tiaa_Cntrct = vw_bci.getRecord().newFieldInGroup("bci_Pin_Tiaa_Cntrct", "PIN-TIAA-CNTRCT", FieldType.STRING, 22, RepeatingFieldStrategy.None, 
            "PIN_TIAA_CNTRCT");
        bci_Pin_Tiaa_Cntrct.setDdmHeader("PIN/TIAA/CONTRACT");

        bci_Error_Table = vw_bci.getRecord().newGroupInGroup("bci_Error_Table", "ERROR-TABLE", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Error_Txt = bci_Error_Table.newFieldArrayInGroup("bci_Error_Txt", "ERROR-TXT", FieldType.STRING, 72, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ERROR_TXT", "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Error_Txt.setDdmHeader("ERROR/TEXT");
        bci_Error_Cde = bci_Error_Table.newFieldArrayInGroup("bci_Error_Cde", "ERROR-CDE", FieldType.STRING, 5, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ERROR_CDE", "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Error_Cde.setDdmHeader("ERROR/CODE");
        bci_Error_Dte = bci_Error_Table.newFieldArrayInGroup("bci_Error_Dte", "ERROR-DTE", FieldType.STRING, 8, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ERROR_DTE", "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Error_Dte.setDdmHeader("ERROR/DATE");
        bci_Error_Tme = bci_Error_Table.newFieldArrayInGroup("bci_Error_Tme", "ERROR-TME", FieldType.STRING, 7, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ERROR_TME", "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Error_Tme.setDdmHeader("ERROR/TIME");
        bci_Error_Pgm = bci_Error_Table.newFieldArrayInGroup("bci_Error_Pgm", "ERROR-PGM", FieldType.STRING, 8, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ERROR_PGM", "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Fldr_Log_Dte_Tme = vw_bci.getRecord().newFieldInGroup("bci_Fldr_Log_Dte_Tme", "FLDR-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "FLDR_LOG_DTE_TME");
        bci_Fldr_Log_Dte_Tme.setDdmHeader("FOLDER/LOG DATE/TIME");
        bci_Last_Dsgntn_Srce = vw_bci.getRecord().newFieldInGroup("bci_Last_Dsgntn_Srce", "LAST-DSGNTN-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "LAST_DSGNTN_SRCE");
        bci_Last_Dsgntn_Srce.setDdmHeader("LAST/DSGNTN/SOURCE");
        bci_Last_Dsgntn_System = vw_bci.getRecord().newFieldInGroup("bci_Last_Dsgntn_System", "LAST-DSGNTN-SYSTEM", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "LAST_DSGNTN_SYSTEM");
        bci_Last_Dsgntn_System.setDdmHeader("LAST/DSGNTN/SYSTEM");
        bci_Last_Dsgntn_Userid = vw_bci.getRecord().newFieldInGroup("bci_Last_Dsgntn_Userid", "LAST-DSGNTN-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "LAST_DSGNTN_USERID");
        bci_Last_Dsgntn_Userid.setDdmHeader("LAST/DSGNTN/USERID");
        bci_Last_Dsgntn_Dte = vw_bci.getRecord().newFieldInGroup("bci_Last_Dsgntn_Dte", "LAST-DSGNTN-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "LAST_DSGNTN_DTE");
        bci_Last_Dsgntn_Dte.setDdmHeader("LAST/DSGNTN/DATE");
        bci_Last_Dsgntn_Tme = vw_bci.getRecord().newFieldInGroup("bci_Last_Dsgntn_Tme", "LAST-DSGNTN-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "LAST_DSGNTN_TME");
        bci_Last_Dsgntn_Tme.setDdmHeader("LAST/DSGNTN/TIME");
        bci_Tiaa_Cref_Chng_Dte = vw_bci.getRecord().newFieldInGroup("bci_Tiaa_Cref_Chng_Dte", "TIAA-CREF-CHNG-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIAA_CREF_CHNG_DTE");
        bci_Tiaa_Cref_Chng_Dte.setDdmHeader("TIAA CREF/CHANGE/DATE");
        bci_Tiaa_Cref_Chng_Tme = vw_bci.getRecord().newFieldInGroup("bci_Tiaa_Cref_Chng_Tme", "TIAA-CREF-CHNG-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "TIAA_CREF_CHNG_TME");
        bci_Tiaa_Cref_Chng_Tme.setDdmHeader("TIAA CREF/CHANGE/TIME");
        bci_Chng_Pwr_Atty = vw_bci.getRecord().newFieldInGroup("bci_Chng_Pwr_Atty", "CHNG-PWR-ATTY", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CHNG_PWR_ATTY");
        bci_Chng_Pwr_Atty.setDdmHeader("CHANGE/POWER/ATTNY");
        bci_Cref_Cntrct = vw_bci.getRecord().newFieldInGroup("bci_Cref_Cntrct", "CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT");
        bci_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bci_Tiaa_Cref_Ind = vw_bci.getRecord().newFieldInGroup("bci_Tiaa_Cref_Ind", "TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIAA_CREF_IND");
        bci_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bci_Cntrct_Type = vw_bci.getRecord().newFieldInGroup("bci_Cntrct_Type", "CNTRCT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_TYPE");
        bci_Cntrct_Type.setDdmHeader("CNTRCT/TYPE");
        bci_Eff_Dte = vw_bci.getRecord().newFieldInGroup("bci_Eff_Dte", "EFF-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "EFF_DTE");
        bci_Eff_Dte.setDdmHeader("EFFECTIVE/DATE");
        bci_Mos_Ind = vw_bci.getRecord().newFieldInGroup("bci_Mos_Ind", "MOS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "MOS_IND");
        bci_Mos_Ind.setDdmHeader("MOS/IND");
        bci_Irrvcble_Ind = vw_bci.getRecord().newFieldInGroup("bci_Irrvcble_Ind", "IRRVCBLE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "IRRVCBLE_IND");
        bci_Irrvcble_Ind.setDdmHeader("IRVCBL/IND");
        bci_Pymnt_Chld_Dcsd_Ind = vw_bci.getRecord().newFieldInGroup("bci_Pymnt_Chld_Dcsd_Ind", "PYMNT-CHLD-DCSD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PYMNT_CHLD_DCSD_IND");
        bci_Pymnt_Chld_Dcsd_Ind.setDdmHeader("PYMNT/CHILD/DCSD");
        bci_Exempt_Spouse_Rights = vw_bci.getRecord().newFieldInGroup("bci_Exempt_Spouse_Rights", "EXEMPT-SPOUSE-RIGHTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "EXEMPT_SPOUSE_RIGHTS");
        bci_Exempt_Spouse_Rights.setDdmHeader("EXEMPT/SPOUSAL/RIGHTS");
        bci_Spouse_Waived_Bnfts = vw_bci.getRecord().newFieldInGroup("bci_Spouse_Waived_Bnfts", "SPOUSE-WAIVED-BNFTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "SPOUSE_WAIVED_BNFTS");
        bci_Spouse_Waived_Bnfts.setDdmHeader("SPOUSE/WAIVED/BENEFITS");
        bci_Chng_New_Issue_Ind = vw_bci.getRecord().newFieldInGroup("bci_Chng_New_Issue_Ind", "CHNG-NEW-ISSUE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CHNG_NEW_ISSUE_IND");
        bci_Chng_New_Issue_Ind.setDdmHeader("CHANGE/NEW ISSUE/IND");
        bci_Same_As_Ind = vw_bci.getRecord().newFieldInGroup("bci_Same_As_Ind", "SAME-AS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "SAME_AS_IND");
        registerRecord(vw_bci);

        vw_bdi = new DataAccessProgramView(new NameInfo("vw_bdi", "BDI"), "BENE_DESIGNATION_INTERFACE_12", "BENE_DSGN_INTFCE");
        bdi_Intrfce_Stts = vw_bdi.getRecord().newFieldInGroup("bdi_Intrfce_Stts", "INTRFCE-STTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, "INTRFCE_STTS");
        bdi_Intrfce_Stts.setDdmHeader("INTRFCE/STATUS");
        bdi_Tiaa_Cref_Ind = vw_bdi.getRecord().newFieldInGroup("bdi_Tiaa_Cref_Ind", "TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIAA_CREF_IND");
        bdi_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bdi_Seq_Nmbr = vw_bdi.getRecord().newFieldInGroup("bdi_Seq_Nmbr", "SEQ-NMBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "SEQ_NMBR");
        bdi_Seq_Nmbr.setDdmHeader("SEQ/NMBR");
        bdi_Rcrd_Crtd_For_Bsnss_Dte = vw_bdi.getRecord().newFieldInGroup("bdi_Rcrd_Crtd_For_Bsnss_Dte", "RCRD-CRTD-FOR-BSNSS-DTE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "RCRD_CRTD_FOR_BSNSS_DTE");
        bdi_Rcrd_Crtd_For_Bsnss_Dte.setDdmHeader("RCRD CRTD/FOR/BSNSS DTE");
        bdi_Rcrd_Last_Updt_Dte = vw_bdi.getRecord().newFieldInGroup("bdi_Rcrd_Last_Updt_Dte", "RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_DTE");
        bdi_Rcrd_Last_Updt_Dte.setDdmHeader("RECORD/LAST UPDT/DATE");
        bdi_Rcrd_Last_Updt_Tme = vw_bdi.getRecord().newFieldInGroup("bdi_Rcrd_Last_Updt_Tme", "RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_TME");
        bdi_Rcrd_Last_Updt_Tme.setDdmHeader("RECORD/LAST UPDT/TIME");
        bdi_Rcrd_Last_Updt_Userid = vw_bdi.getRecord().newFieldInGroup("bdi_Rcrd_Last_Updt_Userid", "RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_USERID");
        bdi_Rcrd_Last_Updt_Userid.setDdmHeader("RECORD/LAST UPDT/USER ID");
        bdi_Pin_Tiaa_Cntrct = vw_bdi.getRecord().newFieldInGroup("bdi_Pin_Tiaa_Cntrct", "PIN-TIAA-CNTRCT", FieldType.STRING, 22, RepeatingFieldStrategy.None, 
            "PIN_TIAA_CNTRCT");
        bdi_Pin_Tiaa_Cntrct.setDdmHeader("PIN/TIAA/CONTRACT");
        bdi_Bene_Type = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Type", "BENE-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BENE_TYPE");
        bdi_Bene_Type.setDdmHeader("BENE/TYPE");
        bdi_Rltn_Cde = vw_bdi.getRecord().newFieldInGroup("bdi_Rltn_Cde", "RLTN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "RLTN_CDE");
        bdi_Rltn_Cde.setDdmHeader("RELATION/CODE");
        bdi_Bene_Name1 = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Name1", "BENE-NAME1", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BENE_NAME1");
        bdi_Bene_Name1.setDdmHeader("BENEFICIARY/NAME 1");
        bdi_Bene_Name2 = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Name2", "BENE-NAME2", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BENE_NAME2");
        bdi_Bene_Name2.setDdmHeader("BENEFICIARY/NAME 2");
        bdi_Rltn_Free_Txt = vw_bdi.getRecord().newFieldInGroup("bdi_Rltn_Free_Txt", "RLTN-FREE-TXT", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "RLTN_FREE_TXT");
        bdi_Rltn_Free_Txt.setDdmHeader("OTHER/RELATION");
        bdi_Dte_Birth_Trust = vw_bdi.getRecord().newFieldInGroup("bdi_Dte_Birth_Trust", "DTE-BIRTH-TRUST", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "DTE_BIRTH_TRUST");
        bdi_Dte_Birth_Trust.setDdmHeader("DOB/DOT");
        bdi_Ssn_Cde = vw_bdi.getRecord().newFieldInGroup("bdi_Ssn_Cde", "SSN-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "SSN_CDE");
        bdi_Ssn_Cde.setDdmHeader("SSN/CODE");
        bdi_Ssn = vw_bdi.getRecord().newFieldInGroup("bdi_Ssn", "SSN", FieldType.STRING, 9, RepeatingFieldStrategy.None, "SSN");
        bdi_Ssn.setDdmHeader("SSN");
        bdi_Prcnt_Frctn_Ind = vw_bdi.getRecord().newFieldInGroup("bdi_Prcnt_Frctn_Ind", "PRCNT-FRCTN-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PRCNT_FRCTN_IND");
        bdi_Prcnt_Frctn_Ind.setDdmHeader("PRCNT/FRCTN/IND");
        bdi_Share_Prcnt = vw_bdi.getRecord().newFieldInGroup("bdi_Share_Prcnt", "SHARE-PRCNT", FieldType.NUMERIC, 5, 2, RepeatingFieldStrategy.None, "SHARE_PRCNT");
        bdi_Share_Prcnt.setDdmHeader("SHARE/PERCENT");
        bdi_Share_Nmrtr = vw_bdi.getRecord().newFieldInGroup("bdi_Share_Nmrtr", "SHARE-NMRTR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "SHARE_NMRTR");
        bdi_Share_Nmrtr.setDdmHeader("SHARE/NUMERATOR");
        bdi_Share_Dnmntr = vw_bdi.getRecord().newFieldInGroup("bdi_Share_Dnmntr", "SHARE-DNMNTR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "SHARE_DNMNTR");
        bdi_Share_Dnmntr.setDdmHeader("SHARE/DENOMINATOR");
        bdi_Irrvcble_Ind = vw_bdi.getRecord().newFieldInGroup("bdi_Irrvcble_Ind", "IRRVCBLE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "IRRVCBLE_IND");
        bdi_Irrvcble_Ind.setDdmHeader("IRVCBL/IND");
        bdi_Sttlmnt_Rstrctn = vw_bdi.getRecord().newFieldInGroup("bdi_Sttlmnt_Rstrctn", "STTLMNT-RSTRCTN", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "STTLMNT_RSTRCTN");
        bdi_Sttlmnt_Rstrctn.setDdmHeader("SETTLEMENT/RESTRICTION");
        bdi_Spcl_Txt1 = vw_bdi.getRecord().newFieldInGroup("bdi_Spcl_Txt1", "SPCL-TXT1", FieldType.STRING, 72, RepeatingFieldStrategy.None, "SPCL_TXT1");
        bdi_Spcl_Txt1.setDdmHeader("SPECIAL TEXT 1");
        bdi_Spcl_Txt2 = vw_bdi.getRecord().newFieldInGroup("bdi_Spcl_Txt2", "SPCL-TXT2", FieldType.STRING, 72, RepeatingFieldStrategy.None, "SPCL_TXT2");
        bdi_Spcl_Txt2.setDdmHeader("SPECIAL TEXT 2");
        bdi_Spcl_Txt3 = vw_bdi.getRecord().newFieldInGroup("bdi_Spcl_Txt3", "SPCL-TXT3", FieldType.STRING, 72, RepeatingFieldStrategy.None, "SPCL_TXT3");
        bdi_Spcl_Txt3.setDdmHeader("SPECIAL TEXT 3");
        bdi_Dflt_Estate = vw_bdi.getRecord().newFieldInGroup("bdi_Dflt_Estate", "DFLT-ESTATE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "DFLT_ESTATE");
        bdi_Dflt_Estate.setDdmHeader("DEFAULT/ESTATE");
        bdi_Mdo_Calc_Bene = vw_bdi.getRecord().newFieldInGroup("bdi_Mdo_Calc_Bene", "MDO-CALC-BENE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "MDO_CALC_BENE");
        bdi_Mdo_Calc_Bene.setDdmHeader("MDO/CALC/BENE");
        bdi_Bene_Addr1 = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Addr1", "BENE-ADDR1", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BENE_ADDR1");
        bdi_Bene_Addr2 = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Addr2", "BENE-ADDR2", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BENE_ADDR2");
        bdi_Bene_Addr3_City = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Addr3_City", "BENE-ADDR3-CITY", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "BENE_ADDR3_CITY");
        bdi_Bene_State = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_State", "BENE-STATE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "BENE_STATE");
        bdi_Bene_Zip = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Zip", "BENE-ZIP", FieldType.STRING, 10, RepeatingFieldStrategy.None, "BENE_ZIP");
        bdi_Bene_Phone = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Phone", "BENE-PHONE", FieldType.STRING, 20, RepeatingFieldStrategy.None, "BENE_PHONE");
        bdi_Bene_Gender = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Gender", "BENE-GENDER", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BENE_GENDER");
        bdi_Bene_Country = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Country", "BENE-COUNTRY", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BENE_COUNTRY");
        registerRecord(vw_bdi);

        vw_bmi = new DataAccessProgramView(new NameInfo("vw_bmi", "BMI"), "BENE_MOS_INTERFACE_12", "BENE_MOS_INTFCE", DdmPeriodicGroups.getInstance().getGroups("BENE_MOS_INTERFACE_12"));
        bmi_Intrfce_Stts = vw_bmi.getRecord().newFieldInGroup("bmi_Intrfce_Stts", "INTRFCE-STTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, "INTRFCE_STTS");
        bmi_Intrfce_Stts.setDdmHeader("INTRFCE/STATUS");
        bmi_Rcrd_Crtd_For_Bsnss_Dte = vw_bmi.getRecord().newFieldInGroup("bmi_Rcrd_Crtd_For_Bsnss_Dte", "RCRD-CRTD-FOR-BSNSS-DTE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "RCRD_CRTD_FOR_BSNSS_DTE");
        bmi_Rcrd_Crtd_For_Bsnss_Dte.setDdmHeader("RCRD CRTD/FOR/BSNSS DTE");
        bmi_Tiaa_Cref_Ind = vw_bmi.getRecord().newFieldInGroup("bmi_Tiaa_Cref_Ind", "TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIAA_CREF_IND");
        bmi_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bmi_Pin_Tiaa_Cntrct = vw_bmi.getRecord().newFieldInGroup("bmi_Pin_Tiaa_Cntrct", "PIN-TIAA-CNTRCT", FieldType.STRING, 22, RepeatingFieldStrategy.None, 
            "PIN_TIAA_CNTRCT");
        bmi_Pin_Tiaa_Cntrct.setDdmHeader("PIN/TIAA/CONTRACT");
        bmi_Rcrd_Crtd_Dte = vw_bmi.getRecord().newFieldInGroup("bmi_Rcrd_Crtd_Dte", "RCRD-CRTD-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_CRTD_DTE");
        bmi_Rcrd_Crtd_Dte.setDdmHeader("RECORD/CREATED/DATE");
        bmi_Rcrd_Crtd_Tme = vw_bmi.getRecord().newFieldInGroup("bmi_Rcrd_Crtd_Tme", "RCRD-CRTD-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "RCRD_CRTD_TME");
        bmi_Rcrd_Crtd_Tme.setDdmHeader("RECORD/CREATED/TIME");
        bmi_Rcrd_Last_Updt_Dte = vw_bmi.getRecord().newFieldInGroup("bmi_Rcrd_Last_Updt_Dte", "RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_DTE");
        bmi_Rcrd_Last_Updt_Dte.setDdmHeader("RECORD/LAST UPDT/DATE");
        bmi_Rcrd_Last_Updt_Tme = vw_bmi.getRecord().newFieldInGroup("bmi_Rcrd_Last_Updt_Tme", "RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_TME");
        bmi_Rcrd_Last_Updt_Tme.setDdmHeader("RECORD/LAST UPDT/TIME");
        bmi_Rcrd_Last_Updt_Userid = vw_bmi.getRecord().newFieldInGroup("bmi_Rcrd_Last_Updt_Userid", "RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_USERID");
        bmi_Rcrd_Last_Updt_Userid.setDdmHeader("RECORD/LAST UPDT/USER ID");
        bmi_Count_Castmos_Txt_Group = vw_bmi.getRecord().newFieldInGroup("bmi_Count_Castmos_Txt_Group", "C*MOS-TXT-GROUP", RepeatingFieldStrategy.CAsteriskVariable, 
            "BENE_MOS_INTFCE_MOS_TXT_GROUP");

        bmi_Mos_Txt_Group = vw_bmi.getRecord().newGroupInGroup("bmi_Mos_Txt_Group", "MOS-TXT-GROUP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "BENE_MOS_INTFCE_MOS_TXT_GROUP");
        bmi_Mos_Txt = bmi_Mos_Txt_Group.newFieldArrayInGroup("bmi_Mos_Txt", "MOS-TXT", FieldType.STRING, 72, new DbsArrayController(1, 60) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "MOS_TXT", "BENE_MOS_INTFCE_MOS_TXT_GROUP");
        bmi_Mos_Txt.setDdmHeader("MOS TEXT");
        registerRecord(vw_bmi);

        pnd_I1 = localVariables.newFieldInRecord("pnd_I1", "#I1", FieldType.NUMERIC, 3);
        pnd_Pin_Tiaa_Cntrct = localVariables.newFieldInRecord("pnd_Pin_Tiaa_Cntrct", "#PIN-TIAA-CNTRCT", FieldType.STRING, 22);

        pnd_Pin_Tiaa_Cntrct__R_Field_1 = localVariables.newGroupInRecord("pnd_Pin_Tiaa_Cntrct__R_Field_1", "REDEFINE", pnd_Pin_Tiaa_Cntrct);
        pnd_Pin_Tiaa_Cntrct_Pnd_Pin = pnd_Pin_Tiaa_Cntrct__R_Field_1.newFieldInGroup("pnd_Pin_Tiaa_Cntrct_Pnd_Pin", "#PIN", FieldType.NUMERIC, 12);
        pnd_Pin_Tiaa_Cntrct_Pnd_Tiaa_Cntrct = pnd_Pin_Tiaa_Cntrct__R_Field_1.newFieldInGroup("pnd_Pin_Tiaa_Cntrct_Pnd_Tiaa_Cntrct", "#TIAA-CNTRCT", FieldType.STRING, 
            10);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_bci.reset();
        vw_bdi.reset();
        vw_bmi.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Benn971() throws Exception
    {
        super("Benn971");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("BENN971", onError);
        MAIN_ROUTINE:                                                                                                                                                     //Natural: REPEAT
        while (condition(whileTrue))
        {
            pdaBena971.getBena971_Pnd_Error_Msg().reset();                                                                                                                //Natural: RESET BENA971.#ERROR-MSG BENA971.#RETURN-CODE
            pdaBena971.getBena971_Pnd_Return_Code().reset();
            if (condition(pdaBena971.getBena971_Pnd_Rqstng_System().equals("CORR")))                                                                                      //Natural: IF BENA971.#RQSTNG-SYSTEM = 'CORR'
            {
                                                                                                                                                                          //Natural: PERFORM DELETE-IF-EXISTS
                sub_Delete_If_Exists();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("MAIN_ROUTINE"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("MAIN_ROUTINE"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  PERFORM COMPARE-DESIGNATION
            //*  LOGIC USED TO CALL BENN9880 TO READ NMD FILE TO COMPARE DESIGNATION
            //*  IF INCOMING IS THE SAME.
                                                                                                                                                                          //Natural: PERFORM POPULATE-CONTRACT
            sub_Populate_Contract();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("MAIN_ROUTINE"))) break;
                else if (condition(Global.isEscapeBottomImmediate("MAIN_ROUTINE"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pdaBena971.getBena971_Pnd_Mos_Ind().equals("Y")))                                                                                               //Natural: IF BENA971.#MOS-IND = 'Y'
            {
                                                                                                                                                                          //Natural: PERFORM POPULATE-MOS
                sub_Populate_Mos();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("MAIN_ROUTINE"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("MAIN_ROUTINE"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaBena971.getBena971_Pnd_Nmbr_Of_Benes().greater(30)))                                                                                     //Natural: IF BENA971.#NMBR-OF-BENES GT 30
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    FOR01:                                                                                                                                                //Natural: FOR #I1 = 1 TO BENA971.#NMBR-OF-BENES
                    for (pnd_I1.setValue(1); condition(pnd_I1.lessOrEqual(pdaBena971.getBena971_Pnd_Nmbr_Of_Benes())); pnd_I1.nadd(1))
                    {
                                                                                                                                                                          //Natural: PERFORM POPULATE-DESIGNATION
                        sub_Populate_Designation();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("MAIN_ROUTINE"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("MAIN_ROUTINE"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ESCAPE-ROUTINE
            if (true) break MAIN_ROUTINE;                                                                                                                                 //Natural: ESCAPE BOTTOM ( MAIN-ROUTINE. )
            //*  MAIN-ROUTINE.
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-IF-EXISTS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DELETE-IF-EXISTS
        //* ***********************************************************************
        //*  CHECK FOR PIN/CONTRACT/IND IN 'A' STATUS , FOR CURRENT BUSINESS DATE.
        //*  THERE SHOULD BE ONLY ONE SUCH CORR 'A'CTIVE AND READY FOR INTERFACE.
        //*  ...SO DELETE ANY THAT CURRENTLY EXIST BEFORE STORING NEW ONE.
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POPULATE-CONTRACT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POPULATE-MOS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POPULATE-DESIGNATION
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Escape_Routine() throws Exception                                                                                                                    //Natural: ESCAPE-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( MAIN-ROUTINE. )
        Global.setEscapeCode(EscapeType.Bottom, "MAIN_ROUTINE");
        if (true) return;
    }
    private void sub_Check_If_Exists() throws Exception                                                                                                                   //Natural: CHECK-IF-EXISTS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  CHECK FOR PIN CONTRACT AND DATE REGARDLESS OF STATUS...WE DON't want
        //*    TO OVERWRITE ANYTHING...IF IT EXISTS, SEND BACK AN ERROR
        //*  STILL N7 HERE
        pdaBena9701.getBena9701_Pnd_Exists().reset();                                                                                                                     //Natural: RESET BENA9701.#EXISTS
        pdaBena9701.getBena9701_Pnd_Pin().setValue(pdaBena971.getBena971_Pnd_Pin());                                                                                      //Natural: ASSIGN BENA9701.#PIN := BENA971.#PIN
        pdaBena9701.getBena9701_Pnd_Tiaa_Cntrct().setValue(pdaBena971.getBena971_Pnd_Tiaa_Cntrct());                                                                      //Natural: ASSIGN BENA9701.#TIAA-CNTRCT := BENA971.#TIAA-CNTRCT
        pdaBena9701.getBena9701_Pnd_Tiaa_Cref_Ind().setValue(pdaBena971.getBena971_Pnd_Tiaa_Cref_Ind());                                                                  //Natural: ASSIGN BENA9701.#TIAA-CREF-IND := BENA971.#TIAA-CREF-IND
        pdaBena9701.getBena9701_Pnd_Rcrd_Crtd_For_Bsnss_Dte().setValue(pdaBena971.getBena971_Pnd_Intrfce_Bsnss_Dte());                                                    //Natural: ASSIGN BENA9701.#RCRD-CRTD-FOR-BSNSS-DTE := BENA971.#INTRFCE-BSNSS-DTE
        DbsUtil.callnat(Benn9701.class , getCurrentProcessState(), pdaBena9701.getBena9701(), pdaBenpda_M.getMsg_Info_Sub());                                             //Natural: CALLNAT 'BENN9701' BENA9701 MSG-INFO-SUB
        if (condition(Global.isEscape())) return;
        if (condition(pdaBena9701.getBena9701_Pnd_Exists().getBoolean()))                                                                                                 //Natural: IF BENA9701.#EXISTS
        {
            pdaBena971.getBena971_Pnd_Error_Msg().setValue(DbsUtil.compress(pdaBena971.getBena971_Pnd_Pin(), "/", pdaBena971.getBena971_Pnd_Tiaa_Cntrct(),                //Natural: COMPRESS BENA971.#PIN '/' BENA971.#TIAA-CNTRCT '/' BENA971.#INTRFCE-BSNSS-DTE 'exists for New Issue Prcssing!' INTO BENA971.#ERROR-MSG
                "/", pdaBena971.getBena971_Pnd_Intrfce_Bsnss_Dte(), "exists for New Issue Prcssing!"));
            pdaBena971.getBena971_Pnd_Return_Code().setValue("E");                                                                                                        //Natural: ASSIGN BENA971.#RETURN-CODE := 'E'
            //*  ESCAPE BOTTOM (MAIN-ROUTINE.) /* UNCOMMENT IF CALLING CHECK-IF-EXISTS
        }                                                                                                                                                                 //Natural: END-IF
        //*  CHECK-IF-EXISTS
    }
    //*  STILL N7 HERE
    private void sub_Delete_If_Exists() throws Exception                                                                                                                  //Natural: DELETE-IF-EXISTS
    {
        if (BLNatReinput.isReinput()) return;

        pdaBena9704.getBena9704_Pnd_Intrfce_Stts().setValue("A");                                                                                                         //Natural: ASSIGN BENA9704.#INTRFCE-STTS := 'A'
        pdaBena9704.getBena9704_Pnd_Rcrd_Crtd_For_Bsnss_Dte().setValue(pdaBena971.getBena971_Pnd_Intrfce_Bsnss_Dte());                                                    //Natural: ASSIGN BENA9704.#RCRD-CRTD-FOR-BSNSS-DTE := BENA971.#INTRFCE-BSNSS-DTE
        pdaBena9704.getBena9704_Pnd_Pin().setValue(pdaBena971.getBena971_Pnd_Pin());                                                                                      //Natural: ASSIGN BENA9704.#PIN := BENA971.#PIN
        pdaBena9704.getBena9704_Pnd_Tiaa_Cntrct().setValue(pdaBena971.getBena971_Pnd_Tiaa_Cntrct());                                                                      //Natural: ASSIGN BENA9704.#TIAA-CNTRCT := BENA971.#TIAA-CNTRCT
        pdaBena9704.getBena9704_Pnd_Tiaa_Cref_Ind().setValue(pdaBena971.getBena971_Pnd_Tiaa_Cref_Ind());                                                                  //Natural: ASSIGN BENA9704.#TIAA-CREF-IND := BENA971.#TIAA-CREF-IND
        DbsUtil.callnat(Benn9704.class , getCurrentProcessState(), pdaBena9704.getBena9704());                                                                            //Natural: CALLNAT 'BENN9704' BENA9704
        if (condition(Global.isEscape())) return;
        //*  DELETE-IF-EXISTS
    }
    private void sub_Populate_Contract() throws Exception                                                                                                                 //Natural: POPULATE-CONTRACT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  STILL N7 HERE
        vw_bci.reset();                                                                                                                                                   //Natural: RESET BCI
        pnd_Pin_Tiaa_Cntrct_Pnd_Pin.setValue(pdaBena971.getBena971_Pnd_Pin());                                                                                            //Natural: ASSIGN #PIN-TIAA-CNTRCT.#PIN := BENA971.#PIN
        pnd_Pin_Tiaa_Cntrct_Pnd_Tiaa_Cntrct.setValue(pdaBena971.getBena971_Pnd_Tiaa_Cntrct());                                                                            //Natural: ASSIGN #PIN-TIAA-CNTRCT.#TIAA-CNTRCT := BENA971.#TIAA-CNTRCT
        bci_Intrfce_Stts.setValue("A");                                                                                                                                   //Natural: ASSIGN BCI.INTRFCE-STTS := 'A'
        bci_Intrfcng_Systm.setValue(pdaBena971.getBena971_Pnd_Intrfcng_Systm());                                                                                          //Natural: ASSIGN BCI.INTRFCNG-SYSTM := BENA971.#INTRFCNG-SYSTM
        bci_Rqstng_Systm.setValue(pdaBena971.getBena971_Pnd_Rqstng_System());                                                                                             //Natural: ASSIGN BCI.RQSTNG-SYSTM := BENA971.#RQSTNG-SYSTEM
        bci_Dflt_To_Estate_Ind.setValue(pdaBena971.getBena971_Pnd_Dflt_To_Estate());                                                                                      //Natural: ASSIGN BCI.DFLT-TO-ESTATE-IND := BENA971.#DFLT-TO-ESTATE
        bci_Illgble_Ind.setValue(pdaBena971.getBena971_Pnd_Illgble_Ind());                                                                                                //Natural: ASSIGN BCI.ILLGBLE-IND := BENA971.#ILLGBLE-IND
        bci_Intrfce_Mgrtn_Ind.setValue(pdaBena971.getBena971_Pnd_Intrfce_Mgrtn_Ind());                                                                                    //Natural: ASSIGN BCI.INTRFCE-MGRTN-IND := BENA971.#INTRFCE-MGRTN-IND
        bci_Pin_Tiaa_Cntrct.setValue(pnd_Pin_Tiaa_Cntrct);                                                                                                                //Natural: ASSIGN BCI.PIN-TIAA-CNTRCT := #PIN-TIAA-CNTRCT
        bci_Fldr_Log_Dte_Tme.setValue(pdaBena971.getBena971_Pnd_Fldr_Log_Dte_Tme());                                                                                      //Natural: ASSIGN BCI.FLDR-LOG-DTE-TME := BENA971.#FLDR-LOG-DTE-TME
        bci_Cref_Cntrct.setValue(pdaBena971.getBena971_Pnd_Cref_Cntrct());                                                                                                //Natural: ASSIGN BCI.CREF-CNTRCT := BENA971.#CREF-CNTRCT
        bci_Tiaa_Cref_Ind.setValue(pdaBena971.getBena971_Pnd_Tiaa_Cref_Ind());                                                                                            //Natural: ASSIGN BCI.TIAA-CREF-IND := BENA971.#TIAA-CREF-IND
        bci_Cntrct_Type.setValue(pdaBena971.getBena971_Pnd_Cntrct_Type());                                                                                                //Natural: ASSIGN BCI.CNTRCT-TYPE := BENA971.#CNTRCT-TYPE
        bci_Eff_Dte.setValue(pdaBena971.getBena971_Pnd_Effctve_Dte());                                                                                                    //Natural: ASSIGN BCI.EFF-DTE := BENA971.#EFFCTVE-DTE
        bci_Mos_Ind.setValue(pdaBena971.getBena971_Pnd_Mos_Ind());                                                                                                        //Natural: ASSIGN BCI.MOS-IND := BENA971.#MOS-IND
        bci_Irrvcble_Ind.setValue(pdaBena971.getBena971_Pnd_Mos_Irrvcble_Ind());                                                                                          //Natural: ASSIGN BCI.IRRVCBLE-IND := BENA971.#MOS-IRRVCBLE-IND
        bci_Same_As_Ind.setValue(pdaBena971.getBena971_Pnd_Same_As_Ind());                                                                                                //Natural: ASSIGN BCI.SAME-AS-IND := BENA971.#SAME-AS-IND
        bci_Rcrd_Crtd_For_Bsnss_Dte.setValue(pdaBena971.getBena971_Pnd_Intrfce_Bsnss_Dte());                                                                              //Natural: ASSIGN BCI.RCRD-CRTD-FOR-BSNSS-DTE := BENA971.#INTRFCE-BSNSS-DTE
        bci_More_Than_Five_Benes_Ind.setValue(pdaBena971.getBena971_Pnd_More_Than_Five_Benes_Ind());                                                                      //Natural: ASSIGN BCI.MORE-THAN-FIVE-BENES-IND := BENA971.#MORE-THAN-FIVE-BENES-IND
        if (condition(pdaBena971.getBena971_Pnd_Nmbr_Of_Benes().greater(30)))                                                                                             //Natural: IF BENA971.#NMBR-OF-BENES GT 30
        {
            bci_More_Than_Thirty_Benes_Ind.setValue("Y");                                                                                                                 //Natural: ASSIGN BCI.MORE-THAN-THIRTY-BENES-IND := 'Y'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaBena971.getBena971_Pnd_Mos_Ind().notEquals("Y")))                                                                                                //Natural: IF BENA971.#MOS-IND NE 'Y'
        {
            bci_Pymnt_Chld_Dcsd_Ind.setValue(pdaBena971.getBena971_Pnd_Pymnt_Chld_Dcsd_Ind());                                                                            //Natural: ASSIGN BCI.PYMNT-CHLD-DCSD-IND := BENA971.#PYMNT-CHLD-DCSD-IND
        }                                                                                                                                                                 //Natural: END-IF
        bci_Exempt_Spouse_Rights.setValue(pdaBena971.getBena971_Pnd_Exempt_Spouse_Rights());                                                                              //Natural: ASSIGN BCI.EXEMPT-SPOUSE-RIGHTS := BENA971.#EXEMPT-SPOUSE-RIGHTS
        bci_Spouse_Waived_Bnfts.setValue(pdaBena971.getBena971_Pnd_Spouse_Waived_Bnfts());                                                                                //Natural: ASSIGN BCI.SPOUSE-WAIVED-BNFTS := BENA971.#SPOUSE-WAIVED-BNFTS
        bci_Chng_New_Issue_Ind.setValue(pdaBena971.getBena971_Pnd_New_Issuefslash_Chng_Ind());                                                                            //Natural: ASSIGN BCI.CHNG-NEW-ISSUE-IND := BENA971.#NEW-ISSUE/CHNG-IND
        bci_Rcrd_Last_Updt_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                           //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO BCI.RCRD-LAST-UPDT-DTE
        bci_Rcrd_Last_Updt_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("HHIISST"));                                                                            //Natural: MOVE EDITED *TIMX ( EM = HHIISST ) TO BCI.RCRD-LAST-UPDT-TME
        bci_Rcrd_Last_Updt_Userid.setValue(Global.getINIT_USER());                                                                                                        //Natural: ASSIGN BCI.RCRD-LAST-UPDT-USERID := *INIT-USER
        bci_Rcrd_Crtd_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                                //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO BCI.RCRD-CRTD-DTE
        bci_Rcrd_Crtd_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("HHIISST"));                                                                                 //Natural: MOVE EDITED *TIMX ( EM = HHIISST ) TO BCI.RCRD-CRTD-TME
        //*  BENE-CONTRACT-INTERFACE_12
        vw_bci.insertDBRow();                                                                                                                                             //Natural: STORE BCI
        //*  POPULATE-CONTRACT
    }
    private void sub_Populate_Mos() throws Exception                                                                                                                      //Natural: POPULATE-MOS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        vw_bmi.reset();                                                                                                                                                   //Natural: RESET BMI
        bmi_Intrfce_Stts.setValue("A");                                                                                                                                   //Natural: ASSIGN BMI.INTRFCE-STTS := 'A'
        bmi_Tiaa_Cref_Ind.setValue(pdaBena971.getBena971_Pnd_Tiaa_Cref_Ind());                                                                                            //Natural: ASSIGN BMI.TIAA-CREF-IND := BENA971.#TIAA-CREF-IND
        bmi_Pin_Tiaa_Cntrct.setValue(pnd_Pin_Tiaa_Cntrct);                                                                                                                //Natural: ASSIGN BMI.PIN-TIAA-CNTRCT := #PIN-TIAA-CNTRCT
        bmi_Mos_Txt.getValue("*").setValue(pdaBena971.getBena971_Pnd_Mos_Txt().getValue("*"));                                                                            //Natural: ASSIGN BMI.MOS-TXT ( * ) := BENA971.#MOS-TXT ( * )
        bmi_Rcrd_Last_Updt_Userid.setValue(Global.getINIT_USER());                                                                                                        //Natural: ASSIGN BMI.RCRD-LAST-UPDT-USERID := *INIT-USER
        bmi_Rcrd_Crtd_For_Bsnss_Dte.setValue(pdaBena971.getBena971_Pnd_Intrfce_Bsnss_Dte());                                                                              //Natural: ASSIGN BMI.RCRD-CRTD-FOR-BSNSS-DTE := BENA971.#INTRFCE-BSNSS-DTE
        bmi_Rcrd_Crtd_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                                //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO BMI.RCRD-CRTD-DTE
        bmi_Rcrd_Crtd_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("HHIISST"));                                                                                 //Natural: MOVE EDITED *TIMX ( EM = HHIISST ) TO BMI.RCRD-CRTD-TME
        bmi_Rcrd_Last_Updt_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                           //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO BMI.RCRD-LAST-UPDT-DTE
        bmi_Rcrd_Last_Updt_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("HHIISST"));                                                                            //Natural: MOVE EDITED *TIMX ( EM = HHIISST ) TO BMI.RCRD-LAST-UPDT-TME
        //*  BENE-MOS-INTERFACE_12
        vw_bmi.insertDBRow();                                                                                                                                             //Natural: STORE BMI
        //*  POPULATE-MOS
    }
    private void sub_Populate_Designation() throws Exception                                                                                                              //Natural: POPULATE-DESIGNATION
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  ACCEPT
        vw_bdi.reset();                                                                                                                                                   //Natural: RESET BDI
        bdi_Intrfce_Stts.setValue("A");                                                                                                                                   //Natural: ASSIGN BDI.INTRFCE-STTS := 'A'
        bdi_Tiaa_Cref_Ind.setValue(pdaBena971.getBena971_Pnd_Tiaa_Cref_Ind());                                                                                            //Natural: ASSIGN BDI.TIAA-CREF-IND := BENA971.#TIAA-CREF-IND
        bdi_Seq_Nmbr.setValue(pnd_I1);                                                                                                                                    //Natural: ASSIGN BDI.SEQ-NMBR := #I1
        bdi_Rcrd_Last_Updt_Userid.setValue(Global.getINIT_USER());                                                                                                        //Natural: ASSIGN BDI.RCRD-LAST-UPDT-USERID := *INIT-USER
        bdi_Pin_Tiaa_Cntrct.setValue(pnd_Pin_Tiaa_Cntrct);                                                                                                                //Natural: ASSIGN BDI.PIN-TIAA-CNTRCT := #PIN-TIAA-CNTRCT
        bdi_Bene_Type.setValue(pdaBena971.getBena971_Pnd_Bene_Type().getValue(pnd_I1));                                                                                   //Natural: ASSIGN BDI.BENE-TYPE := BENA971.#BENE-TYPE ( #I1 )
        bdi_Rltn_Cde.setValue(pdaBena971.getBena971_Pnd_Rtln_Cde().getValue(pnd_I1));                                                                                     //Natural: ASSIGN BDI.RLTN-CDE := BENA971.#RTLN-CDE ( #I1 )
        bdi_Bene_Name1.setValue(pdaBena971.getBena971_Pnd_Bene_Name1().getValue(pnd_I1));                                                                                 //Natural: ASSIGN BDI.BENE-NAME1 := BENA971.#BENE-NAME1 ( #I1 )
        bdi_Bene_Name2.setValue(pdaBena971.getBena971_Pnd_Bene_Name2().getValue(pnd_I1));                                                                                 //Natural: ASSIGN BDI.BENE-NAME2 := BENA971.#BENE-NAME2 ( #I1 )
        bdi_Rltn_Free_Txt.setValue(pdaBena971.getBena971_Pnd_Rltn_Free_Txt().getValue(pnd_I1));                                                                           //Natural: ASSIGN BDI.RLTN-FREE-TXT := BENA971.#RLTN-FREE-TXT ( #I1 )
        bdi_Ssn_Cde.setValue(pdaBena971.getBena971_Pnd_Ss_Cd().getValue(pnd_I1));                                                                                         //Natural: ASSIGN BDI.SSN-CDE := BENA971.#SS-CD ( #I1 )
        bdi_Ssn.setValue(pdaBena971.getBena971_Pnd_Ss_Nbr().getValue(pnd_I1));                                                                                            //Natural: ASSIGN BDI.SSN := BENA971.#SS-NBR ( #I1 )
        bdi_Share_Prcnt.setValue(pdaBena971.getBena971_Pnd_Prctge().getValue(pnd_I1));                                                                                    //Natural: ASSIGN BDI.SHARE-PRCNT := BENA971.#PRCTGE ( #I1 )
        bdi_Share_Nmrtr.setValue(pdaBena971.getBena971_Pnd_Nmrtr().getValue(pnd_I1));                                                                                     //Natural: ASSIGN BDI.SHARE-NMRTR := BENA971.#NMRTR ( #I1 )
        bdi_Share_Dnmntr.setValue(pdaBena971.getBena971_Pnd_Dnmntr().getValue(pnd_I1));                                                                                   //Natural: ASSIGN BDI.SHARE-DNMNTR := BENA971.#DNMNTR ( #I1 )
        bdi_Irrvcble_Ind.setValue(pdaBena971.getBena971_Pnd_Irrvcbl_Ind().getValue(pnd_I1));                                                                              //Natural: ASSIGN BDI.IRRVCBLE-IND := BENA971.#IRRVCBL-IND ( #I1 )
        bdi_Spcl_Txt1.setValue(pdaBena971.getBena971_Pnd_Spcl_Txt().getValue(pnd_I1,1));                                                                                  //Natural: ASSIGN BDI.SPCL-TXT1 := BENA971.#SPCL-TXT ( #I1,1 )
        bdi_Spcl_Txt2.setValue(pdaBena971.getBena971_Pnd_Spcl_Txt().getValue(pnd_I1,2));                                                                                  //Natural: ASSIGN BDI.SPCL-TXT2 := BENA971.#SPCL-TXT ( #I1,2 )
        bdi_Spcl_Txt3.setValue(pdaBena971.getBena971_Pnd_Spcl_Txt().getValue(pnd_I1,3));                                                                                  //Natural: ASSIGN BDI.SPCL-TXT3 := BENA971.#SPCL-TXT ( #I1,3 )
        bdi_Rcrd_Crtd_For_Bsnss_Dte.setValue(pdaBena971.getBena971_Pnd_Intrfce_Bsnss_Dte());                                                                              //Natural: ASSIGN BDI.RCRD-CRTD-FOR-BSNSS-DTE := BENA971.#INTRFCE-BSNSS-DTE
        bdi_Dte_Birth_Trust.setValue(pdaBena971.getBena971_Pnd_Dte_Birth_Trust().getValue(pnd_I1));                                                                       //Natural: ASSIGN BDI.DTE-BIRTH-TRUST := BENA971.#DTE-BIRTH-TRUST ( #I1 )
        bdi_Prcnt_Frctn_Ind.setValue(pdaBena971.getBena971_Pnd_Prctge_Frctn_Ind().getValue(pnd_I1));                                                                      //Natural: ASSIGN BDI.PRCNT-FRCTN-IND := BENA971.#PRCTGE-FRCTN-IND ( #I1 )
        bdi_Sttlmnt_Rstrctn.setValue(pdaBena971.getBena971_Pnd_Sttlmnt_Rstrctn().getValue(pnd_I1));                                                                       //Natural: ASSIGN BDI.STTLMNT-RSTRCTN := BENA971.#STTLMNT-RSTRCTN ( #I1 )
        bdi_Bene_Addr1.setValue(pdaBena971.getBena971_Pnd_Addr1().getValue(pnd_I1));                                                                                      //Natural: ASSIGN BDI.BENE-ADDR1 := BENA971.#ADDR1 ( #I1 )
        bdi_Bene_Addr2.setValue(pdaBena971.getBena971_Pnd_Addr2().getValue(pnd_I1));                                                                                      //Natural: ASSIGN BDI.BENE-ADDR2 := BENA971.#ADDR2 ( #I1 )
        bdi_Bene_Addr3_City.setValue(pdaBena971.getBena971_Pnd_Addr3_City().getValue(pnd_I1));                                                                            //Natural: ASSIGN BDI.BENE-ADDR3-CITY := BENA971.#ADDR3-CITY ( #I1 )
        bdi_Bene_State.setValue(pdaBena971.getBena971_Pnd_State().getValue(pnd_I1));                                                                                      //Natural: ASSIGN BDI.BENE-STATE := BENA971.#STATE ( #I1 )
        bdi_Bene_Zip.setValue(pdaBena971.getBena971_Pnd_Zip().getValue(pnd_I1));                                                                                          //Natural: ASSIGN BDI.BENE-ZIP := BENA971.#ZIP ( #I1 )
        bdi_Bene_Country.setValue(pdaBena971.getBena971_Pnd_Country().getValue(pnd_I1));                                                                                  //Natural: ASSIGN BDI.BENE-COUNTRY := BENA971.#COUNTRY ( #I1 )
        bdi_Bene_Phone.setValue(pdaBena971.getBena971_Pnd_Phone().getValue(pnd_I1));                                                                                      //Natural: ASSIGN BDI.BENE-PHONE := BENA971.#PHONE ( #I1 )
        bdi_Bene_Gender.setValue(pdaBena971.getBena971_Pnd_Gender().getValue(pnd_I1));                                                                                    //Natural: ASSIGN BDI.BENE-GENDER := BENA971.#GENDER ( #I1 )
        bdi_Rcrd_Last_Updt_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                           //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO BDI.RCRD-LAST-UPDT-DTE
        bdi_Rcrd_Last_Updt_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("HHIISST"));                                                                            //Natural: MOVE EDITED *TIMX ( EM = HHIISST ) TO BDI.RCRD-LAST-UPDT-TME
        //*  BENE-DESIGNATION-INTERFACE_12
        vw_bdi.insertDBRow();                                                                                                                                             //Natural: STORE BDI
        //*  POPULATE-DESIGNATION
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        pdaBena971.getBena971_Pnd_Error_Msg().setValue(DbsUtil.compress("NATURAL Error", Global.getERROR_NR(), " at line ", Global.getERROR_LINE(), "in Program",         //Natural: COMPRESS 'NATURAL Error' *ERROR-NR ' at line ' *ERROR-LINE 'in Program' *PROGRAM INTO BENA971.#ERROR-MSG
            Global.getPROGRAM()));
        pdaBena971.getBena971_Pnd_Return_Code().setValue("E");                                                                                                            //Natural: ASSIGN BENA971.#RETURN-CODE := 'E'
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        if (condition(true)) return;                                                                                                                                      //Natural: ESCAPE ROUTINE
    };                                                                                                                                                                    //Natural: END-ERROR
}
