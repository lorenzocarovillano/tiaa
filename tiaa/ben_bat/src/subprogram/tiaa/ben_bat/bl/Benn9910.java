/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:07:42 PM
**        * FROM NATURAL SUBPROGRAM : Benn9910
************************************************************
**        * FILE NAME            : Benn9910.java
**        * CLASS NAME           : Benn9910
**        * INSTANCE NAME        : Benn9910
************************************************************
************************************************************************
* PROGRAM  : BENN9910
* SYSTEM   : BENEFICIARY-SYSTEM
* TITLE    : GET THE ZZ INTERFACE DATE RECORD
* GENERATED: JUNE 29, 1999
*          : GET THE ZZ INTERFACE DATE
*
*
* HISTORY
* CHANGED ON JUNE 29,99 BY DEPAUL FOR RELEASE ____
* CLONED FROM BENN9831
* >
**SAG END-EXIT
************************************************************************

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benn9910 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaBena9910 pdaBena9910;
    private PdaBenpda_M pdaBenpda_M;
    private PdaBenpda_E pdaBenpda_E;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_bene_Table_File;
    private DbsField bene_Table_File_Bt_Table_Id;
    private DbsField bene_Table_File_Bt_Table_Key;
    private DbsField bene_Table_File_Bt_Table_Text;
    private DbsField pnd_Bt_Table_Id_Key;

    private DbsGroup pnd_Bt_Table_Id_Key__R_Field_1;
    private DbsField pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Id;
    private DbsField pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Key;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaBena9910 = new PdaBena9910(parameters);
        pdaBenpda_M = new PdaBenpda_M(parameters);
        pdaBenpda_E = new PdaBenpda_E(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_bene_Table_File = new DataAccessProgramView(new NameInfo("vw_bene_Table_File", "BENE-TABLE-FILE"), "BENE_TABLE_FILE", "BENE_TABLE");
        bene_Table_File_Bt_Table_Id = vw_bene_Table_File.getRecord().newFieldInGroup("bene_Table_File_Bt_Table_Id", "BT-TABLE-ID", FieldType.STRING, 2, 
            RepeatingFieldStrategy.None, "BT_TABLE_ID");
        bene_Table_File_Bt_Table_Id.setDdmHeader("TABLE/ID");
        bene_Table_File_Bt_Table_Key = vw_bene_Table_File.getRecord().newFieldInGroup("bene_Table_File_Bt_Table_Key", "BT-TABLE-KEY", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "BT_TABLE_KEY");
        bene_Table_File_Bt_Table_Key.setDdmHeader("TABLE/KEY");
        bene_Table_File_Bt_Table_Text = vw_bene_Table_File.getRecord().newFieldInGroup("bene_Table_File_Bt_Table_Text", "BT-TABLE-TEXT", FieldType.STRING, 
            72, RepeatingFieldStrategy.None, "BT_TABLE_TEXT");
        bene_Table_File_Bt_Table_Text.setDdmHeader("TABLE/TEXT");
        registerRecord(vw_bene_Table_File);

        pnd_Bt_Table_Id_Key = localVariables.newFieldInRecord("pnd_Bt_Table_Id_Key", "#BT-TABLE-ID-KEY", FieldType.STRING, 22);

        pnd_Bt_Table_Id_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Bt_Table_Id_Key__R_Field_1", "REDEFINE", pnd_Bt_Table_Id_Key);
        pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Id = pnd_Bt_Table_Id_Key__R_Field_1.newFieldInGroup("pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Id", "#BT-TABLE-ID", FieldType.STRING, 
            2);
        pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Key = pnd_Bt_Table_Id_Key__R_Field_1.newFieldInGroup("pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Key", "#BT-TABLE-KEY", 
            FieldType.STRING, 20);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_bene_Table_File.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Benn9910() throws Exception
    {
        super("Benn9910");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ERROR-INFO-SUB.##LAST-PROGRAM = *PROGRAM
        pdaBenpda_M.getMsg_Info_Sub().reset();                                                                                                                            //Natural: RESET MSG-INFO-SUB BENA9910.#BSNSS-DTE
        pdaBena9910.getBena9910_Pnd_Bsnss_Dte().reset();
                                                                                                                                                                          //Natural: PERFORM GET-INTERFACE-RECORD
        sub_Get_Interface_Record();
        if (condition(Global.isEscape())) {return;}
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-INTERFACE-RECORD
    }
    private void sub_Get_Interface_Record() throws Exception                                                                                                              //Natural: GET-INTERFACE-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Id.setValue("ZZ");                                                                                                               //Natural: ASSIGN #BT-TABLE-ID = 'ZZ'
        pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Key.setValue("SAMEAS-DATE");                                                                                                     //Natural: ASSIGN #BT-TABLE-KEY = 'SAMEAS-DATE'
        vw_bene_Table_File.startDatabaseFind                                                                                                                              //Natural: FIND ( 1 ) BENE-TABLE-FILE WITH BT-TABLE-ID-KEY = #BT-TABLE-ID-KEY
        (
        "FIND_RECORD",
        new Wc[] { new Wc("BT_TABLE_ID_KEY", "=", pnd_Bt_Table_Id_Key, WcType.WITH) },
        1
        );
        FIND_RECORD:
        while (condition(vw_bene_Table_File.readNextRow("FIND_RECORD", true)))
        {
            vw_bene_Table_File.setIfNotFoundControlFlag(false);
            if (condition(vw_bene_Table_File.getAstCOUNTER().equals(0)))                                                                                                  //Natural: IF NO RECORDS
            {
                pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                          //Natural: ASSIGN ##RETURN-CODE = 'E'
                pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("Interface Date record not found - contact Systems");                                                  //Natural: ASSIGN ##MSG = 'Interface Date record not found - contact Systems'
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            pdaBena9910.getBena9910_Pnd_Bsnss_Dte().setValue(bene_Table_File_Bt_Table_Text);                                                                              //Natural: ASSIGN BENA9910.#BSNSS-DTE := BENE-TABLE-FILE.BT-TABLE-TEXT
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  GET-INTERFACE-RECORD
    }

    //
}
