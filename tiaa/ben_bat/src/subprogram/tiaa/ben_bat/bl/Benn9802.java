/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:05:46 PM
**        * FROM NATURAL SUBPROGRAM : Benn9802
************************************************************
**        * FILE NAME            : Benn9802.java
**        * CLASS NAME           : Benn9802
**        * INSTANCE NAME        : Benn9802
************************************************************
************************************************************************
* PROGRAM  : BENN9802
* SYSTEM   : BENEFICIARY-SYSTEM
* TITLE    : BENE INTERFACE DRIVER TO WRITE TO BENE FILES
* GENERATED: JULY 6, 1999
************************************************************************
* HISTORY
* DURAND   5/1/12   BYPASS INTERFACE-AS-MOS IF RLTN=56 (PLAN PROVISIONS)
* WEBBJ    3/6/00   /*JW 3/6
* > IRAP & LOAN BEGIN INTERFACING - DUE TO POSSIBLLITY OF 'Special Text'
* > INTERFACING IN DOB/SSN/REL"one of these must be non-blank and if its
* > REL IT CANNOT BE A '38'/OTHER" ==> if this cond not met DES is MOS'd
* WEBBJ    11/10/00 ACIS CORRECTION STORES LAST DSGNTN SYSTEM AS 'X'
* WEBBJ    12/18/00 ADD INTERFACE-AS-MOS PROCESSING SENT FROM N9820.
* WEBBJ    6/29/01 ADD EXEMPT-SPOUSAL-RIGHTS TRANSFER FROM BENN9815 WBBC
* H.KAKADIA 04/08/05 ALLOW ISV TO CALCULATE ALLOCATION LIKE ACIS WHEN TH
*                    THERE IS NO BENE ALLOCATION ENTERED.
* 03/13/2013 ALAGANI ADDED ADDRESS/PHONE/GENDER FIELDS AND INSERTED ALL
*                    LOCAL VIEWS INSIDE THE CODE.
* 04/01/2017  DURAND    PIN EXPANSION N7 TO N12
* MM/DD/YYYY  XXXXXXXX  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
************************************************************************

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benn9802 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaBena9802 pdaBena9802;
    private PdaBena9801 pdaBena9801;
    private PdaBenpda_M pdaBenpda_M;
    private PdaBenpda_E pdaBenpda_E;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_bc;
    private DbsField bc_Bc_Pin;
    private DbsField bc_Bc_Tiaa_Cntrct;
    private DbsField bc_Bc_Cref_Cntrct;
    private DbsField bc_Bc_Tiaa_Cref_Ind;
    private DbsField bc_Bc_Stat;
    private DbsField bc_Bc_Cntrct_Type;
    private DbsField bc_Bc_Rqst_Timestamp;
    private DbsField bc_Bc_Eff_Dte;
    private DbsField bc_Bc_Mos_Ind;
    private DbsField bc_Bc_Irvcbl_Ind;
    private DbsField bc_Bc_Pymnt_Chld_Dcsd_Ind;
    private DbsField bc_Bc_Exempt_Spouse_Rights;
    private DbsField bc_Bc_Spouse_Waived_Bnfts;
    private DbsField bc_Bc_Trust_Data_Fldr;
    private DbsField bc_Bc_Bene_Addr_Fldr;
    private DbsField bc_Bc_Co_Owner_Data_Fldr;
    private DbsField bc_Bc_Fldr_Min;
    private DbsField bc_Bc_Fldr_Srce_Id;
    private DbsField bc_Bc_Fldr_Log_Dte_Tme;
    private DbsField bc_Bc_Rcrd_Last_Updt_Dte;
    private DbsField bc_Bc_Rcrd_Last_Updt_Tme;
    private DbsField bc_Bc_Rcrd_Last_Updt_Userid;
    private DbsField bc_Bc_Last_Dsgntn_Srce;
    private DbsField bc_Bc_Last_Dsgntn_System;
    private DbsField bc_Bc_Last_Dsgntn_Userid;
    private DbsField bc_Bc_Last_Dsgntn_Dte;
    private DbsField bc_Bc_Last_Dsgntn_Tme;
    private DbsField bc_Bc_Last_Vrfy_Dte;
    private DbsField bc_Bc_Last_Vrfy_Tme;
    private DbsField bc_Bc_Last_Vrfy_Userid;
    private DbsField bc_Bc_Tiaa_Cref_Chng_Dte;
    private DbsField bc_Bc_Tiaa_Cref_Chng_Tme;
    private DbsField bc_Bc_Chng_Pwr_Atty;

    private DataAccessProgramView vw_bd;
    private DbsField bd_Bd_Pin;
    private DbsField bd_Bd_Tiaa_Cntrct;
    private DbsField bd_Bd_Cref_Cntrct;
    private DbsField bd_Bd_Tiaa_Cref_Ind;
    private DbsField bd_Bd_Stat;
    private DbsField bd_Bd_Seq_Nmbr;
    private DbsField bd_Bd_Rqst_Timestamp;
    private DbsField bd_Bd_Rcrd_Last_Updt_Dte;
    private DbsField bd_Bd_Rcrd_Last_Updt_Tme;
    private DbsField bd_Bd_Rcrd_Last_Updt_Userid;
    private DbsField bd_Bd_Bene_Type;
    private DbsField bd_Bd_Rltn_Cd;
    private DbsField bd_Bd_Bene_Name1;
    private DbsField bd_Bd_Bene_Name2;
    private DbsField bd_Bd_Rltn_Free_Txt;
    private DbsField bd_Bd_Dte_Birth_Trust;
    private DbsField bd_Bd_Ss_Cd;
    private DbsField bd_Bd_Ss_Nmbr;
    private DbsField bd_Bd_Perc_Share_Ind;
    private DbsField bd_Bd_Share_Perc;
    private DbsField bd_Bd_Share_Ntor;
    private DbsField bd_Bd_Share_Dtor;
    private DbsField bd_Bd_Irvcbl_Ind;
    private DbsField bd_Bd_Stlmnt_Rstrctn;
    private DbsField bd_Bd_Spcl_Txt1;
    private DbsField bd_Bd_Spcl_Txt2;
    private DbsField bd_Bd_Spcl_Txt3;
    private DbsField bd_Bd_Dflt_Estate;
    private DbsField bd_Bd_Addr1;
    private DbsField bd_Bd_Addr2;
    private DbsField bd_Bd_Addr3_City;
    private DbsField bd_Bd_State;
    private DbsField bd_Bd_Zip;
    private DbsField bd_Bd_Country;
    private DbsField bd_Bd_Phone;
    private DbsField bd_Bd_Gender;

    private DataAccessProgramView vw_bm;
    private DbsField bm_Bm_Pin;
    private DbsField bm_Bm_Tiaa_Cntrct;
    private DbsField bm_Bm_Cref_Cntrct;
    private DbsField bm_Bm_Tiaa_Cref_Ind;
    private DbsField bm_Bm_Stat;
    private DbsField bm_Bm_Rqst_Timestamp;
    private DbsField bm_Bm_Rcrd_Last_Updt_Dte;
    private DbsField bm_Bm_Rcrd_Last_Updt_Tme;
    private DbsField bm_Bm_Rcrd_Last_Updt_Userid;

    private DbsGroup bm_Bm_Mos_Txt_Group;
    private DbsField bm_Bm_Mos_Txt;

    private DataAccessProgramView vw_bci;
    private DbsField bci_Intrfce_Stts;
    private DbsField bci_Rcrd_Crtd_For_Bsnss_Dte;
    private DbsField bci_Rcrd_Crtd_Dte;
    private DbsField bci_Rcrd_Crtd_Tme;
    private DbsField bci_Intrfcng_Systm;
    private DbsField bci_Rqstng_Systm;
    private DbsField bci_Rcrd_Last_Updt_Dte;
    private DbsField bci_Rcrd_Last_Updt_Tme;
    private DbsField bci_Rcrd_Last_Updt_Userid;
    private DbsField bci_Last_Prcssd_Bsnss_Dte;
    private DbsField bci_Intrfce_Cmpltd_Dte;
    private DbsField bci_Intrfce_Cmpltd_Tme;
    private DbsField bci_Dflt_To_Estate_Ind;
    private DbsField bci_Illgble_Ind;
    private DbsField bci_More_Than_Five_Benes_Ind;
    private DbsField bci_Intrfce_Mgrtn_Ind;
    private DbsField bci_More_Than_Thirty_Benes_Ind;
    private DbsField bci_Pin_Tiaa_Cntrct;

    private DbsGroup bci_Error_Table;
    private DbsField bci_Error_Txt;
    private DbsField bci_Error_Cde;
    private DbsField bci_Error_Dte;
    private DbsField bci_Error_Tme;
    private DbsField bci_Error_Pgm;
    private DbsField bci_Fldr_Log_Dte_Tme;
    private DbsField bci_Last_Dsgntn_Srce;
    private DbsField bci_Last_Dsgntn_System;
    private DbsField bci_Last_Dsgntn_Userid;
    private DbsField bci_Last_Dsgntn_Dte;
    private DbsField bci_Last_Dsgntn_Tme;
    private DbsField bci_Tiaa_Cref_Chng_Dte;
    private DbsField bci_Tiaa_Cref_Chng_Tme;
    private DbsField bci_Chng_Pwr_Atty;
    private DbsField bci_Cref_Cntrct;
    private DbsField bci_Tiaa_Cref_Ind;
    private DbsField bci_Cntrct_Type;
    private DbsField bci_Eff_Dte;
    private DbsField bci_Mos_Ind;
    private DbsField bci_Irrvcble_Ind;
    private DbsField bci_Pymnt_Chld_Dcsd_Ind;
    private DbsField bci_Exempt_Spouse_Rights;
    private DbsField bci_Spouse_Waived_Bnfts;
    private DbsField bci_Chng_New_Issue_Ind;
    private DbsField bci_Same_As_Ind;

    private DataAccessProgramView vw_bdi;
    private DbsField bdi_Intrfce_Stts;
    private DbsField bdi_Tiaa_Cref_Ind;
    private DbsField bdi_Seq_Nmbr;
    private DbsField bdi_Rcrd_Crtd_For_Bsnss_Dte;
    private DbsField bdi_Rcrd_Last_Updt_Dte;
    private DbsField bdi_Rcrd_Last_Updt_Tme;
    private DbsField bdi_Rcrd_Last_Updt_Userid;
    private DbsField bdi_Pin_Tiaa_Cntrct;
    private DbsField bdi_Bene_Type;
    private DbsField bdi_Rltn_Cde;
    private DbsField bdi_Bene_Name1;
    private DbsField bdi_Bene_Name2;
    private DbsField bdi_Rltn_Free_Txt;
    private DbsField bdi_Dte_Birth_Trust;
    private DbsField bdi_Ssn_Cde;
    private DbsField bdi_Ssn;
    private DbsField bdi_Prcnt_Frctn_Ind;
    private DbsField bdi_Share_Prcnt;
    private DbsField bdi_Share_Nmrtr;
    private DbsField bdi_Share_Dnmntr;
    private DbsField bdi_Irrvcble_Ind;
    private DbsField bdi_Sttlmnt_Rstrctn;
    private DbsField bdi_Spcl_Txt1;
    private DbsField bdi_Spcl_Txt2;
    private DbsField bdi_Spcl_Txt3;
    private DbsField bdi_Dflt_Estate;
    private DbsField bdi_Bene_Addr1;
    private DbsField bdi_Bene_Addr2;
    private DbsField bdi_Bene_Addr3_City;
    private DbsField bdi_Bene_State;
    private DbsField bdi_Bene_Zip;
    private DbsField bdi_Bene_Phone;
    private DbsField bdi_Bene_Gender;
    private DbsField bdi_Bene_Country;

    private DataAccessProgramView vw_bmi;
    private DbsField bmi_Intrfce_Stts;
    private DbsField bmi_Rcrd_Crtd_For_Bsnss_Dte;
    private DbsField bmi_Tiaa_Cref_Ind;
    private DbsField bmi_Pin_Tiaa_Cntrct;
    private DbsField bmi_Rcrd_Crtd_Dte;
    private DbsField bmi_Rcrd_Crtd_Tme;
    private DbsField bmi_Rcrd_Last_Updt_Dte;
    private DbsField bmi_Rcrd_Last_Updt_Tme;
    private DbsField bmi_Rcrd_Last_Updt_Userid;

    private DbsGroup bmi_Mos_Txt_Group;
    private DbsField bmi_Mos_Txt;
    private DbsField pnd_I1;
    private DbsField pnd_J1;
    private DbsField pnd_K1;
    private DbsField pnd_C1;
    private DbsField pnd_Tabend;
    private DbsField pnd_Timestamp;

    private DbsGroup pnd_Timestamp__R_Field_1;
    private DbsField pnd_Timestamp_Pnd_Datx;
    private DbsField pnd_Timestamp_Pnd_Timx;
    private DbsField pnd_Intrfcng_Systm;
    private DbsField pnd_Rqstng_Systm;
    private DbsField pnd_Dflt_To_Estate_Ind;
    private DbsField pnd_Cont_Irvcbl_Set;
    private DbsField pnd_Acis_Mos_Txt;
    private DbsField pnd_Acis_Cntngt_Mos_Txt;
    private DbsField pnd_Acis_Mos_Txt_Data;

    private DbsGroup pnd_Acis_Mos_Txt_Data__R_Field_2;
    private DbsField pnd_Acis_Mos_Txt_Data_Pnd_Acis_Name;
    private DbsField pnd_Acis_Mos_Txt_Data_Pnd_Sp2;
    private DbsField pnd_Acis_Mos_Txt_Data_Pnd_Acis_Mos_Txt_Data_Rest;

    private DbsGroup pnd_Acis_Mos_Txt_Data__R_Field_3;
    private DbsField pnd_Acis_Mos_Txt_Data_Pnd_Acis_Birth;

    private DbsGroup pnd_Acis_Mos_Txt_Data__R_Field_4;
    private DbsField pnd_Acis_Mos_Txt_Data_Pnd_Acis_Birth_Mm;
    private DbsField pnd_Acis_Mos_Txt_Data_Pnd_Acis_Sl1;
    private DbsField pnd_Acis_Mos_Txt_Data_Pnd_Acis_Birth_Dd;
    private DbsField pnd_Acis_Mos_Txt_Data_Pnd_Acis_Sl2;
    private DbsField pnd_Acis_Mos_Txt_Data_Pnd_Acis_Birth_Yy;
    private DbsField pnd_Acis_Mos_Txt_Data_Pnd_Sp3;
    private DbsField pnd_Acis_Mos_Txt_Data_Pnd_Acis_Ssn;

    private DbsGroup pnd_Acis_Mos_Txt_Data__R_Field_5;
    private DbsField pnd_Acis_Mos_Txt_Data_Pnd_Acis_Ssn_A3;
    private DbsField pnd_Acis_Mos_Txt_Data_Pnd_Acis_Ssn__1;
    private DbsField pnd_Acis_Mos_Txt_Data_Pnd_Acis_Ssn_A2;
    private DbsField pnd_Acis_Mos_Txt_Data_Pnd_Acis_Ssn__2;
    private DbsField pnd_Acis_Mos_Txt_Data_Pnd_Acis_Ssn_A4;
    private DbsField pnd_Acis_Mos_Txt_Data_Pnd_Sp4;
    private DbsField pnd_Acis_Mos_Txt_Data_Pnd_Acis_Rltn;

    private DbsGroup pnd_Acis_Mos_Txt_Data__R_Field_6;
    private DbsField pnd_Acis_Mos_Txt_Data_Pnd_Sp3_Il;
    private DbsField pnd_Acis_Mos_Txt_Data_Pnd_Acis_Rltn_Il;
    private DbsField pnd_Acis_Mos_Txt_Data_Pnd_Sp4_Il;
    private DbsField pnd_Acis_Mos_Txt_Data_Pnd_Acis_Ssn_Il;
    private DbsField pnd_Acis_Mos_Txt_Data_Pnd_Sp5_Il;
    private DbsField pnd_Acis_Mos_Txt_Data_Pnd_Acis_Birth_Il;
    private DbsField pnd_Acis_Birth_Hold;
    private DbsField pnd_Acis_Ssn_Field;

    private DbsGroup pnd_Acis_Ssn_Field__R_Field_7;
    private DbsField pnd_Acis_Ssn_Field_Pnd_Ssn_A3;
    private DbsField pnd_Acis_Ssn_Field_Pnd_Ssn_A2;
    private DbsField pnd_Acis_Ssn_Field_Pnd_Ssn_A4;
    private DbsField pnd_Date_Field;

    private DbsGroup pnd_Date_Field__R_Field_8;
    private DbsField pnd_Date_Field_Pnd_Date_Cc;
    private DbsField pnd_Date_Field_Pnd_Date_Yy;
    private DbsField pnd_Date_Field_Pnd_Date_Mm;
    private DbsField pnd_Date_Field_Pnd_Date_Dd;
    private DbsField pnd_Num_Acis_Benes_P;
    private DbsField pnd_Num_Acis_Benes_C;
    private DbsField pnd_Structured_Data_Found;
    private DbsField pnd_1st_Acis_Cntngnt;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaBena9802 = new PdaBena9802(parameters);
        pdaBena9801 = new PdaBena9801(parameters);
        pdaBenpda_M = new PdaBenpda_M(parameters);
        pdaBenpda_E = new PdaBenpda_E(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_bc = new DataAccessProgramView(new NameInfo("vw_bc", "BC"), "BENE_CONTRACT_12", "BENE_CONTRACT");
        bc_Bc_Pin = vw_bc.getRecord().newFieldInGroup("bc_Bc_Pin", "BC-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BC_PIN");
        bc_Bc_Pin.setDdmHeader("PIN");
        bc_Bc_Tiaa_Cntrct = vw_bc.getRecord().newFieldInGroup("bc_Bc_Tiaa_Cntrct", "BC-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BC_TIAA_CNTRCT");
        bc_Bc_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bc_Bc_Cref_Cntrct = vw_bc.getRecord().newFieldInGroup("bc_Bc_Cref_Cntrct", "BC-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BC_CREF_CNTRCT");
        bc_Bc_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bc_Bc_Tiaa_Cref_Ind = vw_bc.getRecord().newFieldInGroup("bc_Bc_Tiaa_Cref_Ind", "BC-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_TIAA_CREF_IND");
        bc_Bc_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bc_Bc_Stat = vw_bc.getRecord().newFieldInGroup("bc_Bc_Stat", "BC-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_STAT");
        bc_Bc_Stat.setDdmHeader("STATUS");
        bc_Bc_Cntrct_Type = vw_bc.getRecord().newFieldInGroup("bc_Bc_Cntrct_Type", "BC-CNTRCT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_CNTRCT_TYPE");
        bc_Bc_Cntrct_Type.setDdmHeader("CNTRCT/TYPE");
        bc_Bc_Rqst_Timestamp = vw_bc.getRecord().newFieldInGroup("bc_Bc_Rqst_Timestamp", "BC-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BC_RQST_TIMESTAMP");
        bc_Bc_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        bc_Bc_Eff_Dte = vw_bc.getRecord().newFieldInGroup("bc_Bc_Eff_Dte", "BC-EFF-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "BC_EFF_DTE");
        bc_Bc_Eff_Dte.setDdmHeader("EFFECTIVE/DATE");
        bc_Bc_Mos_Ind = vw_bc.getRecord().newFieldInGroup("bc_Bc_Mos_Ind", "BC-MOS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_MOS_IND");
        bc_Bc_Mos_Ind.setDdmHeader("MOS/IND");
        bc_Bc_Irvcbl_Ind = vw_bc.getRecord().newFieldInGroup("bc_Bc_Irvcbl_Ind", "BC-IRVCBL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_IRVCBL_IND");
        bc_Bc_Irvcbl_Ind.setDdmHeader("IRVCBL/IND");
        bc_Bc_Pymnt_Chld_Dcsd_Ind = vw_bc.getRecord().newFieldInGroup("bc_Bc_Pymnt_Chld_Dcsd_Ind", "BC-PYMNT-CHLD-DCSD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_PYMNT_CHLD_DCSD_IND");
        bc_Bc_Pymnt_Chld_Dcsd_Ind.setDdmHeader("PYMNT/CHILD/DCSD");
        bc_Bc_Exempt_Spouse_Rights = vw_bc.getRecord().newFieldInGroup("bc_Bc_Exempt_Spouse_Rights", "BC-EXEMPT-SPOUSE-RIGHTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_EXEMPT_SPOUSE_RIGHTS");
        bc_Bc_Exempt_Spouse_Rights.setDdmHeader("EXEMPT/SPOUSAL/RIGHTS");
        bc_Bc_Spouse_Waived_Bnfts = vw_bc.getRecord().newFieldInGroup("bc_Bc_Spouse_Waived_Bnfts", "BC-SPOUSE-WAIVED-BNFTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_SPOUSE_WAIVED_BNFTS");
        bc_Bc_Spouse_Waived_Bnfts.setDdmHeader("SPOUSE/WAIVED/BENEFITS");
        bc_Bc_Trust_Data_Fldr = vw_bc.getRecord().newFieldInGroup("bc_Bc_Trust_Data_Fldr", "BC-TRUST-DATA-FLDR", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_TRUST_DATA_FLDR");
        bc_Bc_Trust_Data_Fldr.setDdmHeader("TRUST/DATA/IN FLDR");
        bc_Bc_Bene_Addr_Fldr = vw_bc.getRecord().newFieldInGroup("bc_Bc_Bene_Addr_Fldr", "BC-BENE-ADDR-FLDR", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_BENE_ADDR_FLDR");
        bc_Bc_Bene_Addr_Fldr.setDdmHeader("BENE/ADDRESS/IN FLDR");
        bc_Bc_Co_Owner_Data_Fldr = vw_bc.getRecord().newFieldInGroup("bc_Bc_Co_Owner_Data_Fldr", "BC-CO-OWNER-DATA-FLDR", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_CO_OWNER_DATA_FLDR");
        bc_Bc_Co_Owner_Data_Fldr.setDdmHeader("CO-OWNER/DATA FLDR");
        bc_Bc_Fldr_Min = vw_bc.getRecord().newFieldInGroup("bc_Bc_Fldr_Min", "BC-FLDR-MIN", FieldType.STRING, 11, RepeatingFieldStrategy.None, "BC_FLDR_MIN");
        bc_Bc_Fldr_Min.setDdmHeader("FOLDER/MIN");
        bc_Bc_Fldr_Srce_Id = vw_bc.getRecord().newFieldInGroup("bc_Bc_Fldr_Srce_Id", "BC-FLDR-SRCE-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "BC_FLDR_SRCE_ID");
        bc_Bc_Fldr_Srce_Id.setDdmHeader("FOLDER/SOURCE/ID");
        bc_Bc_Fldr_Log_Dte_Tme = vw_bc.getRecord().newFieldInGroup("bc_Bc_Fldr_Log_Dte_Tme", "BC-FLDR-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BC_FLDR_LOG_DTE_TME");
        bc_Bc_Fldr_Log_Dte_Tme.setDdmHeader("FOLDER/LOG DATE/AND TIME");
        bc_Bc_Rcrd_Last_Updt_Dte = vw_bc.getRecord().newFieldInGroup("bc_Bc_Rcrd_Last_Updt_Dte", "BC-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_RCRD_LAST_UPDT_DTE");
        bc_Bc_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bc_Bc_Rcrd_Last_Updt_Tme = vw_bc.getRecord().newFieldInGroup("bc_Bc_Rcrd_Last_Updt_Tme", "BC-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_RCRD_LAST_UPDT_TME");
        bc_Bc_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bc_Bc_Rcrd_Last_Updt_Userid = vw_bc.getRecord().newFieldInGroup("bc_Bc_Rcrd_Last_Updt_Userid", "BC-RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "BC_RCRD_LAST_UPDT_USERID");
        bc_Bc_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        bc_Bc_Last_Dsgntn_Srce = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Dsgntn_Srce", "BC-LAST-DSGNTN-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_SRCE");
        bc_Bc_Last_Dsgntn_Srce.setDdmHeader("LAST/DSGNTN/SOURCE");
        bc_Bc_Last_Dsgntn_System = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Dsgntn_System", "BC-LAST-DSGNTN-SYSTEM", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_SYSTEM");
        bc_Bc_Last_Dsgntn_System.setDdmHeader("LAST/DSGNTN/SYSTEM");
        bc_Bc_Last_Dsgntn_Userid = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Dsgntn_Userid", "BC-LAST-DSGNTN-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_USERID");
        bc_Bc_Last_Dsgntn_Userid.setDdmHeader("LAST/DSGNTN/USERID");
        bc_Bc_Last_Dsgntn_Dte = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Dsgntn_Dte", "BC-LAST-DSGNTN-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_DTE");
        bc_Bc_Last_Dsgntn_Dte.setDdmHeader("LAST/DSGNTN/DATE");
        bc_Bc_Last_Dsgntn_Tme = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Dsgntn_Tme", "BC-LAST-DSGNTN-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_TME");
        bc_Bc_Last_Dsgntn_Tme.setDdmHeader("LAST/DSGNTN/TIME");
        bc_Bc_Last_Vrfy_Dte = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Vrfy_Dte", "BC-LAST-VRFY-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_VRFY_DTE");
        bc_Bc_Last_Vrfy_Dte.setDdmHeader("LAST/VRFY/DATE");
        bc_Bc_Last_Vrfy_Tme = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Vrfy_Tme", "BC-LAST-VRFY-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_LAST_VRFY_TME");
        bc_Bc_Last_Vrfy_Tme.setDdmHeader("LAST/VRFY/TIME");
        bc_Bc_Last_Vrfy_Userid = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Vrfy_Userid", "BC-LAST-VRFY-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_VRFY_USERID");
        bc_Bc_Last_Vrfy_Userid.setDdmHeader("LAST/VRFY/USERID");
        bc_Bc_Tiaa_Cref_Chng_Dte = vw_bc.getRecord().newFieldInGroup("bc_Bc_Tiaa_Cref_Chng_Dte", "BC-TIAA-CREF-CHNG-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_TIAA_CREF_CHNG_DTE");
        bc_Bc_Tiaa_Cref_Chng_Dte.setDdmHeader("TIAA CREF/CHANGE/DATE");
        bc_Bc_Tiaa_Cref_Chng_Tme = vw_bc.getRecord().newFieldInGroup("bc_Bc_Tiaa_Cref_Chng_Tme", "BC-TIAA-CREF-CHNG-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_TIAA_CREF_CHNG_TME");
        bc_Bc_Tiaa_Cref_Chng_Tme.setDdmHeader("TIAA CREF/CHANGE/TIME");
        bc_Bc_Chng_Pwr_Atty = vw_bc.getRecord().newFieldInGroup("bc_Bc_Chng_Pwr_Atty", "BC-CHNG-PWR-ATTY", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_CHNG_PWR_ATTY");
        bc_Bc_Chng_Pwr_Atty.setDdmHeader("CHANGE/POWER/ATTNY");
        registerRecord(vw_bc);

        vw_bd = new DataAccessProgramView(new NameInfo("vw_bd", "BD"), "BENE_DESIGNATION_12", "BENE_DESIGNATION");
        bd_Bd_Pin = vw_bd.getRecord().newFieldInGroup("bd_Bd_Pin", "BD-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BD_PIN");
        bd_Bd_Pin.setDdmHeader("PIN");
        bd_Bd_Tiaa_Cntrct = vw_bd.getRecord().newFieldInGroup("bd_Bd_Tiaa_Cntrct", "BD-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BD_TIAA_CNTRCT");
        bd_Bd_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bd_Bd_Cref_Cntrct = vw_bd.getRecord().newFieldInGroup("bd_Bd_Cref_Cntrct", "BD-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BD_CREF_CNTRCT");
        bd_Bd_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bd_Bd_Tiaa_Cref_Ind = vw_bd.getRecord().newFieldInGroup("bd_Bd_Tiaa_Cref_Ind", "BD-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_TIAA_CREF_IND");
        bd_Bd_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bd_Bd_Stat = vw_bd.getRecord().newFieldInGroup("bd_Bd_Stat", "BD-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_STAT");
        bd_Bd_Stat.setDdmHeader("STAT");
        bd_Bd_Seq_Nmbr = vw_bd.getRecord().newFieldInGroup("bd_Bd_Seq_Nmbr", "BD-SEQ-NMBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "BD_SEQ_NMBR");
        bd_Bd_Seq_Nmbr.setDdmHeader(" SEQ/NMBR");
        bd_Bd_Rqst_Timestamp = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rqst_Timestamp", "BD-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BD_RQST_TIMESTAMP");
        bd_Bd_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        bd_Bd_Rcrd_Last_Updt_Dte = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rcrd_Last_Updt_Dte", "BD-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BD_RCRD_LAST_UPDT_DTE");
        bd_Bd_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bd_Bd_Rcrd_Last_Updt_Tme = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rcrd_Last_Updt_Tme", "BD-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BD_RCRD_LAST_UPDT_TME");
        bd_Bd_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bd_Bd_Rcrd_Last_Updt_Userid = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rcrd_Last_Updt_Userid", "BD-RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "BD_RCRD_LAST_UPDT_USERID");
        bd_Bd_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        bd_Bd_Bene_Type = vw_bd.getRecord().newFieldInGroup("bd_Bd_Bene_Type", "BD-BENE-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_BENE_TYPE");
        bd_Bd_Bene_Type.setDdmHeader("BENE/TYPE");
        bd_Bd_Rltn_Cd = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rltn_Cd", "BD-RLTN-CD", FieldType.STRING, 2, RepeatingFieldStrategy.None, "BD_RLTN_CD");
        bd_Bd_Rltn_Cd.setDdmHeader("RELATION/CODE");
        bd_Bd_Bene_Name1 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Bene_Name1", "BD-BENE-NAME1", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BD_BENE_NAME1");
        bd_Bd_Bene_Name1.setDdmHeader("BENEFICIARY/NAME 1");
        bd_Bd_Bene_Name2 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Bene_Name2", "BD-BENE-NAME2", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BD_BENE_NAME2");
        bd_Bd_Bene_Name2.setDdmHeader("BENEFICIARY/NAME 2");
        bd_Bd_Rltn_Free_Txt = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rltn_Free_Txt", "BD-RLTN-FREE-TXT", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BD_RLTN_FREE_TXT");
        bd_Bd_Rltn_Free_Txt.setDdmHeader("OTHER/RELATION");
        bd_Bd_Dte_Birth_Trust = vw_bd.getRecord().newFieldInGroup("bd_Bd_Dte_Birth_Trust", "BD-DTE-BIRTH-TRUST", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BD_DTE_BIRTH_TRUST");
        bd_Bd_Dte_Birth_Trust.setDdmHeader("DOB/DOT");
        bd_Bd_Ss_Cd = vw_bd.getRecord().newFieldInGroup("bd_Bd_Ss_Cd", "BD-SS-CD", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_SS_CD");
        bd_Bd_Ss_Cd.setDdmHeader("SS/CODE");
        bd_Bd_Ss_Nmbr = vw_bd.getRecord().newFieldInGroup("bd_Bd_Ss_Nmbr", "BD-SS-NMBR", FieldType.STRING, 9, RepeatingFieldStrategy.None, "BD_SS_NMBR");
        bd_Bd_Ss_Nmbr.setDdmHeader("SS/NUMBER");
        bd_Bd_Perc_Share_Ind = vw_bd.getRecord().newFieldInGroup("bd_Bd_Perc_Share_Ind", "BD-PERC-SHARE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_PERC_SHARE_IND");
        bd_Bd_Perc_Share_Ind.setDdmHeader("PERCNT/SHARE/IND");
        bd_Bd_Share_Perc = vw_bd.getRecord().newFieldInGroup("bd_Bd_Share_Perc", "BD-SHARE-PERC", FieldType.NUMERIC, 5, 2, RepeatingFieldStrategy.None, 
            "BD_SHARE_PERC");
        bd_Bd_Share_Perc.setDdmHeader("SHARE/PERCENT");
        bd_Bd_Share_Ntor = vw_bd.getRecord().newFieldInGroup("bd_Bd_Share_Ntor", "BD-SHARE-NTOR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "BD_SHARE_NTOR");
        bd_Bd_Share_Ntor.setDdmHeader("SHARE/NUMERATOR");
        bd_Bd_Share_Dtor = vw_bd.getRecord().newFieldInGroup("bd_Bd_Share_Dtor", "BD-SHARE-DTOR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "BD_SHARE_DTOR");
        bd_Bd_Share_Dtor.setDdmHeader("SHARE/DENOMINATOR");
        bd_Bd_Irvcbl_Ind = vw_bd.getRecord().newFieldInGroup("bd_Bd_Irvcbl_Ind", "BD-IRVCBL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_IRVCBL_IND");
        bd_Bd_Irvcbl_Ind.setDdmHeader("IRVCBL/IND");
        bd_Bd_Stlmnt_Rstrctn = vw_bd.getRecord().newFieldInGroup("bd_Bd_Stlmnt_Rstrctn", "BD-STLMNT-RSTRCTN", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_STLMNT_RSTRCTN");
        bd_Bd_Stlmnt_Rstrctn.setDdmHeader("SETTLEMENT/RESTRICTION");
        bd_Bd_Spcl_Txt1 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Spcl_Txt1", "BD-SPCL-TXT1", FieldType.STRING, 72, RepeatingFieldStrategy.None, "BD_SPCL_TXT1");
        bd_Bd_Spcl_Txt1.setDdmHeader("SPECIAL TEXT 1");
        bd_Bd_Spcl_Txt2 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Spcl_Txt2", "BD-SPCL-TXT2", FieldType.STRING, 72, RepeatingFieldStrategy.None, "BD_SPCL_TXT2");
        bd_Bd_Spcl_Txt2.setDdmHeader("SPECIAL TEXT 2");
        bd_Bd_Spcl_Txt3 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Spcl_Txt3", "BD-SPCL-TXT3", FieldType.STRING, 72, RepeatingFieldStrategy.None, "BD_SPCL_TXT3");
        bd_Bd_Spcl_Txt3.setDdmHeader("SPECIAL TEXT 3");
        bd_Bd_Dflt_Estate = vw_bd.getRecord().newFieldInGroup("bd_Bd_Dflt_Estate", "BD-DFLT-ESTATE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_DFLT_ESTATE");
        bd_Bd_Dflt_Estate.setDdmHeader("DEFAULT/ESTATE");
        bd_Bd_Addr1 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Addr1", "BD-ADDR1", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BD_ADDR1");
        bd_Bd_Addr2 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Addr2", "BD-ADDR2", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BD_ADDR2");
        bd_Bd_Addr3_City = vw_bd.getRecord().newFieldInGroup("bd_Bd_Addr3_City", "BD-ADDR3-CITY", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BD_ADDR3_CITY");
        bd_Bd_State = vw_bd.getRecord().newFieldInGroup("bd_Bd_State", "BD-STATE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "BD_STATE");
        bd_Bd_Zip = vw_bd.getRecord().newFieldInGroup("bd_Bd_Zip", "BD-ZIP", FieldType.STRING, 10, RepeatingFieldStrategy.None, "BD_ZIP");
        bd_Bd_Country = vw_bd.getRecord().newFieldInGroup("bd_Bd_Country", "BD-COUNTRY", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BD_COUNTRY");
        bd_Bd_Phone = vw_bd.getRecord().newFieldInGroup("bd_Bd_Phone", "BD-PHONE", FieldType.STRING, 20, RepeatingFieldStrategy.None, "BD_PHONE");
        bd_Bd_Gender = vw_bd.getRecord().newFieldInGroup("bd_Bd_Gender", "BD-GENDER", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_GENDER");
        registerRecord(vw_bd);

        vw_bm = new DataAccessProgramView(new NameInfo("vw_bm", "BM"), "BENE_MOS_12", "BENE_MOS", DdmPeriodicGroups.getInstance().getGroups("BENE_MOS_12"));
        bm_Bm_Pin = vw_bm.getRecord().newFieldInGroup("bm_Bm_Pin", "BM-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BM_PIN");
        bm_Bm_Pin.setDdmHeader("PIN");
        bm_Bm_Tiaa_Cntrct = vw_bm.getRecord().newFieldInGroup("bm_Bm_Tiaa_Cntrct", "BM-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BM_TIAA_CNTRCT");
        bm_Bm_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bm_Bm_Cref_Cntrct = vw_bm.getRecord().newFieldInGroup("bm_Bm_Cref_Cntrct", "BM-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BM_CREF_CNTRCT");
        bm_Bm_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bm_Bm_Tiaa_Cref_Ind = vw_bm.getRecord().newFieldInGroup("bm_Bm_Tiaa_Cref_Ind", "BM-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BM_TIAA_CREF_IND");
        bm_Bm_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bm_Bm_Stat = vw_bm.getRecord().newFieldInGroup("bm_Bm_Stat", "BM-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BM_STAT");
        bm_Bm_Stat.setDdmHeader("STAT");
        bm_Bm_Rqst_Timestamp = vw_bm.getRecord().newFieldInGroup("bm_Bm_Rqst_Timestamp", "BM-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BM_RQST_TIMESTAMP");
        bm_Bm_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        bm_Bm_Rcrd_Last_Updt_Dte = vw_bm.getRecord().newFieldInGroup("bm_Bm_Rcrd_Last_Updt_Dte", "BM-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BM_RCRD_LAST_UPDT_DTE");
        bm_Bm_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bm_Bm_Rcrd_Last_Updt_Tme = vw_bm.getRecord().newFieldInGroup("bm_Bm_Rcrd_Last_Updt_Tme", "BM-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BM_RCRD_LAST_UPDT_TME");
        bm_Bm_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bm_Bm_Rcrd_Last_Updt_Userid = vw_bm.getRecord().newFieldInGroup("bm_Bm_Rcrd_Last_Updt_Userid", "BM-RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "BM_RCRD_LAST_UPDT_USERID");
        bm_Bm_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");

        bm_Bm_Mos_Txt_Group = vw_bm.getRecord().newGroupInGroup("bm_Bm_Mos_Txt_Group", "BM-MOS-TXT-GROUP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "BENE_MOS_BM_MOS_TXT_GROUP");
        bm_Bm_Mos_Txt = bm_Bm_Mos_Txt_Group.newFieldArrayInGroup("bm_Bm_Mos_Txt", "BM-MOS-TXT", FieldType.STRING, 72, new DbsArrayController(1, 60) , 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "BM_MOS_TXT", "BENE_MOS_BM_MOS_TXT_GROUP");
        bm_Bm_Mos_Txt.setDdmHeader("MOS TEXT");
        registerRecord(vw_bm);

        vw_bci = new DataAccessProgramView(new NameInfo("vw_bci", "BCI"), "BENE_CONTRACT_INTERFACE_12", "BENE_CONT_INTFCE", DdmPeriodicGroups.getInstance().getGroups("BENE_CONTRACT_INTERFACE_12"));
        bci_Intrfce_Stts = vw_bci.getRecord().newFieldInGroup("bci_Intrfce_Stts", "INTRFCE-STTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, "INTRFCE_STTS");
        bci_Intrfce_Stts.setDdmHeader("INTRFCE/STATUS");
        bci_Rcrd_Crtd_For_Bsnss_Dte = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Crtd_For_Bsnss_Dte", "RCRD-CRTD-FOR-BSNSS-DTE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "RCRD_CRTD_FOR_BSNSS_DTE");
        bci_Rcrd_Crtd_For_Bsnss_Dte.setDdmHeader("RCRD CRTD/FOR/BSNSS DTE");
        bci_Rcrd_Crtd_Dte = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Crtd_Dte", "RCRD-CRTD-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_CRTD_DTE");
        bci_Rcrd_Crtd_Dte.setDdmHeader("RECORD/CREATED/DATE");
        bci_Rcrd_Crtd_Tme = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Crtd_Tme", "RCRD-CRTD-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "RCRD_CRTD_TME");
        bci_Rcrd_Crtd_Tme.setDdmHeader("RECORD/CREATED/TIME");
        bci_Intrfcng_Systm = vw_bci.getRecord().newFieldInGroup("bci_Intrfcng_Systm", "INTRFCNG-SYSTM", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "INTRFCNG_SYSTM");
        bci_Intrfcng_Systm.setDdmHeader("INTRFCNG/SYSTEM");
        bci_Rqstng_Systm = vw_bci.getRecord().newFieldInGroup("bci_Rqstng_Systm", "RQSTNG-SYSTM", FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQSTNG_SYSTM");
        bci_Rqstng_Systm.setDdmHeader("RQSTNG/SYSTEM");
        bci_Rcrd_Last_Updt_Dte = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Last_Updt_Dte", "RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_DTE");
        bci_Rcrd_Last_Updt_Dte.setDdmHeader("RECORD/LAST UPDT/DATE");
        bci_Rcrd_Last_Updt_Tme = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Last_Updt_Tme", "RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_TME");
        bci_Rcrd_Last_Updt_Tme.setDdmHeader("RECORD/LAST UPDT/TIME");
        bci_Rcrd_Last_Updt_Userid = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Last_Updt_Userid", "RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_USERID");
        bci_Rcrd_Last_Updt_Userid.setDdmHeader("RECORD/LAST UPDT/USER ID");
        bci_Last_Prcssd_Bsnss_Dte = vw_bci.getRecord().newFieldInGroup("bci_Last_Prcssd_Bsnss_Dte", "LAST-PRCSSD-BSNSS-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "LAST_PRCSSD_BSNSS_DTE");
        bci_Last_Prcssd_Bsnss_Dte.setDdmHeader("LAST PRCSSD/BUSINESS/DATE");
        bci_Intrfce_Cmpltd_Dte = vw_bci.getRecord().newFieldInGroup("bci_Intrfce_Cmpltd_Dte", "INTRFCE-CMPLTD-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "INTRFCE_CMPLTD_DTE");
        bci_Intrfce_Cmpltd_Dte.setDdmHeader("INTFCE/CMPLTD/DATE");
        bci_Intrfce_Cmpltd_Tme = vw_bci.getRecord().newFieldInGroup("bci_Intrfce_Cmpltd_Tme", "INTRFCE-CMPLTD-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "INTRFCE_CMPLTD_TME");
        bci_Intrfce_Cmpltd_Tme.setDdmHeader("INTRFCE/CMPLTD/TME");
        bci_Dflt_To_Estate_Ind = vw_bci.getRecord().newFieldInGroup("bci_Dflt_To_Estate_Ind", "DFLT-TO-ESTATE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "DFLT_TO_ESTATE_IND");
        bci_Dflt_To_Estate_Ind.setDdmHeader("DEFAULT/TO/ESTATE");
        bci_Illgble_Ind = vw_bci.getRecord().newFieldInGroup("bci_Illgble_Ind", "ILLGBLE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "ILLGBLE_IND");
        bci_Illgble_Ind.setDdmHeader("ILLGBLE/IND");
        bci_More_Than_Five_Benes_Ind = vw_bci.getRecord().newFieldInGroup("bci_More_Than_Five_Benes_Ind", "MORE-THAN-FIVE-BENES-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "MORE_THAN_FIVE_BENES_IND");
        bci_More_Than_Five_Benes_Ind.setDdmHeader("MORE/THAN 5/BENES");
        bci_Intrfce_Mgrtn_Ind = vw_bci.getRecord().newFieldInGroup("bci_Intrfce_Mgrtn_Ind", "INTRFCE-MGRTN-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "INTRFCE_MGRTN_IND");
        bci_Intrfce_Mgrtn_Ind.setDdmHeader("INTRFCE/MGRTN/IND");
        bci_More_Than_Thirty_Benes_Ind = vw_bci.getRecord().newFieldInGroup("bci_More_Than_Thirty_Benes_Ind", "MORE-THAN-THIRTY-BENES-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "MORE_THAN_THIRTY_BENES_IND");
        bci_More_Than_Thirty_Benes_Ind.setDdmHeader("MORE/THAN 30/BENES");
        bci_Pin_Tiaa_Cntrct = vw_bci.getRecord().newFieldInGroup("bci_Pin_Tiaa_Cntrct", "PIN-TIAA-CNTRCT", FieldType.STRING, 22, RepeatingFieldStrategy.None, 
            "PIN_TIAA_CNTRCT");
        bci_Pin_Tiaa_Cntrct.setDdmHeader("PIN/TIAA/CONTRACT");

        bci_Error_Table = vw_bci.getRecord().newGroupInGroup("bci_Error_Table", "ERROR-TABLE", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Error_Txt = bci_Error_Table.newFieldArrayInGroup("bci_Error_Txt", "ERROR-TXT", FieldType.STRING, 72, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ERROR_TXT", "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Error_Txt.setDdmHeader("ERROR/TEXT");
        bci_Error_Cde = bci_Error_Table.newFieldArrayInGroup("bci_Error_Cde", "ERROR-CDE", FieldType.STRING, 5, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ERROR_CDE", "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Error_Cde.setDdmHeader("ERROR/CODE");
        bci_Error_Dte = bci_Error_Table.newFieldArrayInGroup("bci_Error_Dte", "ERROR-DTE", FieldType.STRING, 8, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ERROR_DTE", "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Error_Dte.setDdmHeader("ERROR/DATE");
        bci_Error_Tme = bci_Error_Table.newFieldArrayInGroup("bci_Error_Tme", "ERROR-TME", FieldType.STRING, 7, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ERROR_TME", "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Error_Tme.setDdmHeader("ERROR/TIME");
        bci_Error_Pgm = bci_Error_Table.newFieldArrayInGroup("bci_Error_Pgm", "ERROR-PGM", FieldType.STRING, 8, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ERROR_PGM", "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Fldr_Log_Dte_Tme = vw_bci.getRecord().newFieldInGroup("bci_Fldr_Log_Dte_Tme", "FLDR-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "FLDR_LOG_DTE_TME");
        bci_Fldr_Log_Dte_Tme.setDdmHeader("FOLDER/LOG DATE/TIME");
        bci_Last_Dsgntn_Srce = vw_bci.getRecord().newFieldInGroup("bci_Last_Dsgntn_Srce", "LAST-DSGNTN-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "LAST_DSGNTN_SRCE");
        bci_Last_Dsgntn_Srce.setDdmHeader("LAST/DSGNTN/SOURCE");
        bci_Last_Dsgntn_System = vw_bci.getRecord().newFieldInGroup("bci_Last_Dsgntn_System", "LAST-DSGNTN-SYSTEM", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "LAST_DSGNTN_SYSTEM");
        bci_Last_Dsgntn_System.setDdmHeader("LAST/DSGNTN/SYSTEM");
        bci_Last_Dsgntn_Userid = vw_bci.getRecord().newFieldInGroup("bci_Last_Dsgntn_Userid", "LAST-DSGNTN-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "LAST_DSGNTN_USERID");
        bci_Last_Dsgntn_Userid.setDdmHeader("LAST/DSGNTN/USERID");
        bci_Last_Dsgntn_Dte = vw_bci.getRecord().newFieldInGroup("bci_Last_Dsgntn_Dte", "LAST-DSGNTN-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "LAST_DSGNTN_DTE");
        bci_Last_Dsgntn_Dte.setDdmHeader("LAST/DSGNTN/DATE");
        bci_Last_Dsgntn_Tme = vw_bci.getRecord().newFieldInGroup("bci_Last_Dsgntn_Tme", "LAST-DSGNTN-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "LAST_DSGNTN_TME");
        bci_Last_Dsgntn_Tme.setDdmHeader("LAST/DSGNTN/TIME");
        bci_Tiaa_Cref_Chng_Dte = vw_bci.getRecord().newFieldInGroup("bci_Tiaa_Cref_Chng_Dte", "TIAA-CREF-CHNG-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIAA_CREF_CHNG_DTE");
        bci_Tiaa_Cref_Chng_Dte.setDdmHeader("TIAA CREF/CHANGE/DATE");
        bci_Tiaa_Cref_Chng_Tme = vw_bci.getRecord().newFieldInGroup("bci_Tiaa_Cref_Chng_Tme", "TIAA-CREF-CHNG-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "TIAA_CREF_CHNG_TME");
        bci_Tiaa_Cref_Chng_Tme.setDdmHeader("TIAA CREF/CHANGE/TIME");
        bci_Chng_Pwr_Atty = vw_bci.getRecord().newFieldInGroup("bci_Chng_Pwr_Atty", "CHNG-PWR-ATTY", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CHNG_PWR_ATTY");
        bci_Chng_Pwr_Atty.setDdmHeader("CHANGE/POWER/ATTNY");
        bci_Cref_Cntrct = vw_bci.getRecord().newFieldInGroup("bci_Cref_Cntrct", "CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT");
        bci_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bci_Tiaa_Cref_Ind = vw_bci.getRecord().newFieldInGroup("bci_Tiaa_Cref_Ind", "TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIAA_CREF_IND");
        bci_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bci_Cntrct_Type = vw_bci.getRecord().newFieldInGroup("bci_Cntrct_Type", "CNTRCT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_TYPE");
        bci_Cntrct_Type.setDdmHeader("CNTRCT/TYPE");
        bci_Eff_Dte = vw_bci.getRecord().newFieldInGroup("bci_Eff_Dte", "EFF-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "EFF_DTE");
        bci_Eff_Dte.setDdmHeader("EFFECTIVE/DATE");
        bci_Mos_Ind = vw_bci.getRecord().newFieldInGroup("bci_Mos_Ind", "MOS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "MOS_IND");
        bci_Mos_Ind.setDdmHeader("MOS/IND");
        bci_Irrvcble_Ind = vw_bci.getRecord().newFieldInGroup("bci_Irrvcble_Ind", "IRRVCBLE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "IRRVCBLE_IND");
        bci_Irrvcble_Ind.setDdmHeader("IRVCBL/IND");
        bci_Pymnt_Chld_Dcsd_Ind = vw_bci.getRecord().newFieldInGroup("bci_Pymnt_Chld_Dcsd_Ind", "PYMNT-CHLD-DCSD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PYMNT_CHLD_DCSD_IND");
        bci_Pymnt_Chld_Dcsd_Ind.setDdmHeader("PYMNT/CHILD/DCSD");
        bci_Exempt_Spouse_Rights = vw_bci.getRecord().newFieldInGroup("bci_Exempt_Spouse_Rights", "EXEMPT-SPOUSE-RIGHTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "EXEMPT_SPOUSE_RIGHTS");
        bci_Exempt_Spouse_Rights.setDdmHeader("EXEMPT/SPOUSAL/RIGHTS");
        bci_Spouse_Waived_Bnfts = vw_bci.getRecord().newFieldInGroup("bci_Spouse_Waived_Bnfts", "SPOUSE-WAIVED-BNFTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "SPOUSE_WAIVED_BNFTS");
        bci_Spouse_Waived_Bnfts.setDdmHeader("SPOUSE/WAIVED/BENEFITS");
        bci_Chng_New_Issue_Ind = vw_bci.getRecord().newFieldInGroup("bci_Chng_New_Issue_Ind", "CHNG-NEW-ISSUE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CHNG_NEW_ISSUE_IND");
        bci_Chng_New_Issue_Ind.setDdmHeader("CHANGE/NEW ISSUE/IND");
        bci_Same_As_Ind = vw_bci.getRecord().newFieldInGroup("bci_Same_As_Ind", "SAME-AS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "SAME_AS_IND");
        registerRecord(vw_bci);

        vw_bdi = new DataAccessProgramView(new NameInfo("vw_bdi", "BDI"), "BENE_DESIGNATION_INTERFACE_12", "BENE_DSGN_INTFCE");
        bdi_Intrfce_Stts = vw_bdi.getRecord().newFieldInGroup("bdi_Intrfce_Stts", "INTRFCE-STTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, "INTRFCE_STTS");
        bdi_Intrfce_Stts.setDdmHeader("INTRFCE/STATUS");
        bdi_Tiaa_Cref_Ind = vw_bdi.getRecord().newFieldInGroup("bdi_Tiaa_Cref_Ind", "TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIAA_CREF_IND");
        bdi_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bdi_Seq_Nmbr = vw_bdi.getRecord().newFieldInGroup("bdi_Seq_Nmbr", "SEQ-NMBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "SEQ_NMBR");
        bdi_Seq_Nmbr.setDdmHeader("SEQ/NMBR");
        bdi_Rcrd_Crtd_For_Bsnss_Dte = vw_bdi.getRecord().newFieldInGroup("bdi_Rcrd_Crtd_For_Bsnss_Dte", "RCRD-CRTD-FOR-BSNSS-DTE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "RCRD_CRTD_FOR_BSNSS_DTE");
        bdi_Rcrd_Crtd_For_Bsnss_Dte.setDdmHeader("RCRD CRTD/FOR/BSNSS DTE");
        bdi_Rcrd_Last_Updt_Dte = vw_bdi.getRecord().newFieldInGroup("bdi_Rcrd_Last_Updt_Dte", "RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_DTE");
        bdi_Rcrd_Last_Updt_Dte.setDdmHeader("RECORD/LAST UPDT/DATE");
        bdi_Rcrd_Last_Updt_Tme = vw_bdi.getRecord().newFieldInGroup("bdi_Rcrd_Last_Updt_Tme", "RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_TME");
        bdi_Rcrd_Last_Updt_Tme.setDdmHeader("RECORD/LAST UPDT/TIME");
        bdi_Rcrd_Last_Updt_Userid = vw_bdi.getRecord().newFieldInGroup("bdi_Rcrd_Last_Updt_Userid", "RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_USERID");
        bdi_Rcrd_Last_Updt_Userid.setDdmHeader("RECORD/LAST UPDT/USER ID");
        bdi_Pin_Tiaa_Cntrct = vw_bdi.getRecord().newFieldInGroup("bdi_Pin_Tiaa_Cntrct", "PIN-TIAA-CNTRCT", FieldType.STRING, 22, RepeatingFieldStrategy.None, 
            "PIN_TIAA_CNTRCT");
        bdi_Pin_Tiaa_Cntrct.setDdmHeader("PIN/TIAA/CONTRACT");
        bdi_Bene_Type = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Type", "BENE-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BENE_TYPE");
        bdi_Bene_Type.setDdmHeader("BENE/TYPE");
        bdi_Rltn_Cde = vw_bdi.getRecord().newFieldInGroup("bdi_Rltn_Cde", "RLTN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "RLTN_CDE");
        bdi_Rltn_Cde.setDdmHeader("RELATION/CODE");
        bdi_Bene_Name1 = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Name1", "BENE-NAME1", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BENE_NAME1");
        bdi_Bene_Name1.setDdmHeader("BENEFICIARY/NAME 1");
        bdi_Bene_Name2 = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Name2", "BENE-NAME2", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BENE_NAME2");
        bdi_Bene_Name2.setDdmHeader("BENEFICIARY/NAME 2");
        bdi_Rltn_Free_Txt = vw_bdi.getRecord().newFieldInGroup("bdi_Rltn_Free_Txt", "RLTN-FREE-TXT", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "RLTN_FREE_TXT");
        bdi_Rltn_Free_Txt.setDdmHeader("OTHER/RELATION");
        bdi_Dte_Birth_Trust = vw_bdi.getRecord().newFieldInGroup("bdi_Dte_Birth_Trust", "DTE-BIRTH-TRUST", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "DTE_BIRTH_TRUST");
        bdi_Dte_Birth_Trust.setDdmHeader("DOB/DOT");
        bdi_Ssn_Cde = vw_bdi.getRecord().newFieldInGroup("bdi_Ssn_Cde", "SSN-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "SSN_CDE");
        bdi_Ssn_Cde.setDdmHeader("SSN/CODE");
        bdi_Ssn = vw_bdi.getRecord().newFieldInGroup("bdi_Ssn", "SSN", FieldType.STRING, 9, RepeatingFieldStrategy.None, "SSN");
        bdi_Ssn.setDdmHeader("SSN");
        bdi_Prcnt_Frctn_Ind = vw_bdi.getRecord().newFieldInGroup("bdi_Prcnt_Frctn_Ind", "PRCNT-FRCTN-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PRCNT_FRCTN_IND");
        bdi_Prcnt_Frctn_Ind.setDdmHeader("PRCNT/FRCTN/IND");
        bdi_Share_Prcnt = vw_bdi.getRecord().newFieldInGroup("bdi_Share_Prcnt", "SHARE-PRCNT", FieldType.NUMERIC, 5, 2, RepeatingFieldStrategy.None, "SHARE_PRCNT");
        bdi_Share_Prcnt.setDdmHeader("SHARE/PERCENT");
        bdi_Share_Nmrtr = vw_bdi.getRecord().newFieldInGroup("bdi_Share_Nmrtr", "SHARE-NMRTR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "SHARE_NMRTR");
        bdi_Share_Nmrtr.setDdmHeader("SHARE/NUMERATOR");
        bdi_Share_Dnmntr = vw_bdi.getRecord().newFieldInGroup("bdi_Share_Dnmntr", "SHARE-DNMNTR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "SHARE_DNMNTR");
        bdi_Share_Dnmntr.setDdmHeader("SHARE/DENOMINATOR");
        bdi_Irrvcble_Ind = vw_bdi.getRecord().newFieldInGroup("bdi_Irrvcble_Ind", "IRRVCBLE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "IRRVCBLE_IND");
        bdi_Irrvcble_Ind.setDdmHeader("IRVCBL/IND");
        bdi_Sttlmnt_Rstrctn = vw_bdi.getRecord().newFieldInGroup("bdi_Sttlmnt_Rstrctn", "STTLMNT-RSTRCTN", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "STTLMNT_RSTRCTN");
        bdi_Sttlmnt_Rstrctn.setDdmHeader("SETTLEMENT/RESTRICTION");
        bdi_Spcl_Txt1 = vw_bdi.getRecord().newFieldInGroup("bdi_Spcl_Txt1", "SPCL-TXT1", FieldType.STRING, 72, RepeatingFieldStrategy.None, "SPCL_TXT1");
        bdi_Spcl_Txt1.setDdmHeader("SPECIAL TEXT 1");
        bdi_Spcl_Txt2 = vw_bdi.getRecord().newFieldInGroup("bdi_Spcl_Txt2", "SPCL-TXT2", FieldType.STRING, 72, RepeatingFieldStrategy.None, "SPCL_TXT2");
        bdi_Spcl_Txt2.setDdmHeader("SPECIAL TEXT 2");
        bdi_Spcl_Txt3 = vw_bdi.getRecord().newFieldInGroup("bdi_Spcl_Txt3", "SPCL-TXT3", FieldType.STRING, 72, RepeatingFieldStrategy.None, "SPCL_TXT3");
        bdi_Spcl_Txt3.setDdmHeader("SPECIAL TEXT 3");
        bdi_Dflt_Estate = vw_bdi.getRecord().newFieldInGroup("bdi_Dflt_Estate", "DFLT-ESTATE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "DFLT_ESTATE");
        bdi_Dflt_Estate.setDdmHeader("DEFAULT/ESTATE");
        bdi_Bene_Addr1 = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Addr1", "BENE-ADDR1", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BENE_ADDR1");
        bdi_Bene_Addr2 = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Addr2", "BENE-ADDR2", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BENE_ADDR2");
        bdi_Bene_Addr3_City = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Addr3_City", "BENE-ADDR3-CITY", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "BENE_ADDR3_CITY");
        bdi_Bene_State = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_State", "BENE-STATE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "BENE_STATE");
        bdi_Bene_Zip = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Zip", "BENE-ZIP", FieldType.STRING, 10, RepeatingFieldStrategy.None, "BENE_ZIP");
        bdi_Bene_Phone = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Phone", "BENE-PHONE", FieldType.STRING, 20, RepeatingFieldStrategy.None, "BENE_PHONE");
        bdi_Bene_Gender = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Gender", "BENE-GENDER", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BENE_GENDER");
        bdi_Bene_Country = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Country", "BENE-COUNTRY", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BENE_COUNTRY");
        registerRecord(vw_bdi);

        vw_bmi = new DataAccessProgramView(new NameInfo("vw_bmi", "BMI"), "BENE_MOS_INTERFACE_12", "BENE_MOS_INTFCE", DdmPeriodicGroups.getInstance().getGroups("BENE_MOS_INTERFACE_12"));
        bmi_Intrfce_Stts = vw_bmi.getRecord().newFieldInGroup("bmi_Intrfce_Stts", "INTRFCE-STTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, "INTRFCE_STTS");
        bmi_Intrfce_Stts.setDdmHeader("INTRFCE/STATUS");
        bmi_Rcrd_Crtd_For_Bsnss_Dte = vw_bmi.getRecord().newFieldInGroup("bmi_Rcrd_Crtd_For_Bsnss_Dte", "RCRD-CRTD-FOR-BSNSS-DTE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "RCRD_CRTD_FOR_BSNSS_DTE");
        bmi_Rcrd_Crtd_For_Bsnss_Dte.setDdmHeader("RCRD CRTD/FOR/BSNSS DTE");
        bmi_Tiaa_Cref_Ind = vw_bmi.getRecord().newFieldInGroup("bmi_Tiaa_Cref_Ind", "TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIAA_CREF_IND");
        bmi_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bmi_Pin_Tiaa_Cntrct = vw_bmi.getRecord().newFieldInGroup("bmi_Pin_Tiaa_Cntrct", "PIN-TIAA-CNTRCT", FieldType.STRING, 22, RepeatingFieldStrategy.None, 
            "PIN_TIAA_CNTRCT");
        bmi_Pin_Tiaa_Cntrct.setDdmHeader("PIN/TIAA/CONTRACT");
        bmi_Rcrd_Crtd_Dte = vw_bmi.getRecord().newFieldInGroup("bmi_Rcrd_Crtd_Dte", "RCRD-CRTD-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_CRTD_DTE");
        bmi_Rcrd_Crtd_Dte.setDdmHeader("RECORD/CREATED/DATE");
        bmi_Rcrd_Crtd_Tme = vw_bmi.getRecord().newFieldInGroup("bmi_Rcrd_Crtd_Tme", "RCRD-CRTD-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "RCRD_CRTD_TME");
        bmi_Rcrd_Crtd_Tme.setDdmHeader("RECORD/CREATED/TIME");
        bmi_Rcrd_Last_Updt_Dte = vw_bmi.getRecord().newFieldInGroup("bmi_Rcrd_Last_Updt_Dte", "RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_DTE");
        bmi_Rcrd_Last_Updt_Dte.setDdmHeader("RECORD/LAST UPDT/DATE");
        bmi_Rcrd_Last_Updt_Tme = vw_bmi.getRecord().newFieldInGroup("bmi_Rcrd_Last_Updt_Tme", "RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_TME");
        bmi_Rcrd_Last_Updt_Tme.setDdmHeader("RECORD/LAST UPDT/TIME");
        bmi_Rcrd_Last_Updt_Userid = vw_bmi.getRecord().newFieldInGroup("bmi_Rcrd_Last_Updt_Userid", "RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_USERID");
        bmi_Rcrd_Last_Updt_Userid.setDdmHeader("RECORD/LAST UPDT/USER ID");

        bmi_Mos_Txt_Group = vw_bmi.getRecord().newGroupInGroup("bmi_Mos_Txt_Group", "MOS-TXT-GROUP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "BENE_MOS_INTFCE_MOS_TXT_GROUP");
        bmi_Mos_Txt = bmi_Mos_Txt_Group.newFieldArrayInGroup("bmi_Mos_Txt", "MOS-TXT", FieldType.STRING, 72, new DbsArrayController(1, 60) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "MOS_TXT", "BENE_MOS_INTFCE_MOS_TXT_GROUP");
        bmi_Mos_Txt.setDdmHeader("MOS TEXT");
        registerRecord(vw_bmi);

        pnd_I1 = localVariables.newFieldInRecord("pnd_I1", "#I1", FieldType.PACKED_DECIMAL, 3);
        pnd_J1 = localVariables.newFieldInRecord("pnd_J1", "#J1", FieldType.PACKED_DECIMAL, 3);
        pnd_K1 = localVariables.newFieldInRecord("pnd_K1", "#K1", FieldType.PACKED_DECIMAL, 3);
        pnd_C1 = localVariables.newFieldInRecord("pnd_C1", "#C1", FieldType.PACKED_DECIMAL, 3);
        pnd_Tabend = localVariables.newFieldInRecord("pnd_Tabend", "#TABEND", FieldType.PACKED_DECIMAL, 3);
        pnd_Timestamp = localVariables.newFieldInRecord("pnd_Timestamp", "#TIMESTAMP", FieldType.STRING, 15);

        pnd_Timestamp__R_Field_1 = localVariables.newGroupInRecord("pnd_Timestamp__R_Field_1", "REDEFINE", pnd_Timestamp);
        pnd_Timestamp_Pnd_Datx = pnd_Timestamp__R_Field_1.newFieldInGroup("pnd_Timestamp_Pnd_Datx", "#DATX", FieldType.STRING, 8);
        pnd_Timestamp_Pnd_Timx = pnd_Timestamp__R_Field_1.newFieldInGroup("pnd_Timestamp_Pnd_Timx", "#TIMX", FieldType.STRING, 7);
        pnd_Intrfcng_Systm = localVariables.newFieldInRecord("pnd_Intrfcng_Systm", "#INTRFCNG-SYSTM", FieldType.STRING, 8);
        pnd_Rqstng_Systm = localVariables.newFieldInRecord("pnd_Rqstng_Systm", "#RQSTNG-SYSTM", FieldType.STRING, 8);
        pnd_Dflt_To_Estate_Ind = localVariables.newFieldInRecord("pnd_Dflt_To_Estate_Ind", "#DFLT-TO-ESTATE-IND", FieldType.STRING, 1);
        pnd_Cont_Irvcbl_Set = localVariables.newFieldInRecord("pnd_Cont_Irvcbl_Set", "#CONT-IRVCBL-SET", FieldType.BOOLEAN, 1);
        pnd_Acis_Mos_Txt = localVariables.newFieldArrayInRecord("pnd_Acis_Mos_Txt", "#ACIS-MOS-TXT", FieldType.STRING, 72, new DbsArrayController(1, 60));
        pnd_Acis_Cntngt_Mos_Txt = localVariables.newFieldArrayInRecord("pnd_Acis_Cntngt_Mos_Txt", "#ACIS-CNTNGT-MOS-TXT", FieldType.STRING, 72, new DbsArrayController(1, 
            60));
        pnd_Acis_Mos_Txt_Data = localVariables.newFieldInRecord("pnd_Acis_Mos_Txt_Data", "#ACIS-MOS-TXT-DATA", FieldType.STRING, 72);

        pnd_Acis_Mos_Txt_Data__R_Field_2 = localVariables.newGroupInRecord("pnd_Acis_Mos_Txt_Data__R_Field_2", "REDEFINE", pnd_Acis_Mos_Txt_Data);
        pnd_Acis_Mos_Txt_Data_Pnd_Acis_Name = pnd_Acis_Mos_Txt_Data__R_Field_2.newFieldInGroup("pnd_Acis_Mos_Txt_Data_Pnd_Acis_Name", "#ACIS-NAME", FieldType.STRING, 
            35);
        pnd_Acis_Mos_Txt_Data_Pnd_Sp2 = pnd_Acis_Mos_Txt_Data__R_Field_2.newFieldInGroup("pnd_Acis_Mos_Txt_Data_Pnd_Sp2", "#SP2", FieldType.STRING, 1);
        pnd_Acis_Mos_Txt_Data_Pnd_Acis_Mos_Txt_Data_Rest = pnd_Acis_Mos_Txt_Data__R_Field_2.newFieldInGroup("pnd_Acis_Mos_Txt_Data_Pnd_Acis_Mos_Txt_Data_Rest", 
            "#ACIS-MOS-TXT-DATA-REST", FieldType.STRING, 36);

        pnd_Acis_Mos_Txt_Data__R_Field_3 = pnd_Acis_Mos_Txt_Data__R_Field_2.newGroupInGroup("pnd_Acis_Mos_Txt_Data__R_Field_3", "REDEFINE", pnd_Acis_Mos_Txt_Data_Pnd_Acis_Mos_Txt_Data_Rest);
        pnd_Acis_Mos_Txt_Data_Pnd_Acis_Birth = pnd_Acis_Mos_Txt_Data__R_Field_3.newFieldInGroup("pnd_Acis_Mos_Txt_Data_Pnd_Acis_Birth", "#ACIS-BIRTH", 
            FieldType.STRING, 8);

        pnd_Acis_Mos_Txt_Data__R_Field_4 = pnd_Acis_Mos_Txt_Data__R_Field_3.newGroupInGroup("pnd_Acis_Mos_Txt_Data__R_Field_4", "REDEFINE", pnd_Acis_Mos_Txt_Data_Pnd_Acis_Birth);
        pnd_Acis_Mos_Txt_Data_Pnd_Acis_Birth_Mm = pnd_Acis_Mos_Txt_Data__R_Field_4.newFieldInGroup("pnd_Acis_Mos_Txt_Data_Pnd_Acis_Birth_Mm", "#ACIS-BIRTH-MM", 
            FieldType.STRING, 2);
        pnd_Acis_Mos_Txt_Data_Pnd_Acis_Sl1 = pnd_Acis_Mos_Txt_Data__R_Field_4.newFieldInGroup("pnd_Acis_Mos_Txt_Data_Pnd_Acis_Sl1", "#ACIS-SL1", FieldType.STRING, 
            1);
        pnd_Acis_Mos_Txt_Data_Pnd_Acis_Birth_Dd = pnd_Acis_Mos_Txt_Data__R_Field_4.newFieldInGroup("pnd_Acis_Mos_Txt_Data_Pnd_Acis_Birth_Dd", "#ACIS-BIRTH-DD", 
            FieldType.STRING, 2);
        pnd_Acis_Mos_Txt_Data_Pnd_Acis_Sl2 = pnd_Acis_Mos_Txt_Data__R_Field_4.newFieldInGroup("pnd_Acis_Mos_Txt_Data_Pnd_Acis_Sl2", "#ACIS-SL2", FieldType.STRING, 
            1);
        pnd_Acis_Mos_Txt_Data_Pnd_Acis_Birth_Yy = pnd_Acis_Mos_Txt_Data__R_Field_4.newFieldInGroup("pnd_Acis_Mos_Txt_Data_Pnd_Acis_Birth_Yy", "#ACIS-BIRTH-YY", 
            FieldType.STRING, 2);
        pnd_Acis_Mos_Txt_Data_Pnd_Sp3 = pnd_Acis_Mos_Txt_Data__R_Field_3.newFieldInGroup("pnd_Acis_Mos_Txt_Data_Pnd_Sp3", "#SP3", FieldType.STRING, 1);
        pnd_Acis_Mos_Txt_Data_Pnd_Acis_Ssn = pnd_Acis_Mos_Txt_Data__R_Field_3.newFieldInGroup("pnd_Acis_Mos_Txt_Data_Pnd_Acis_Ssn", "#ACIS-SSN", FieldType.STRING, 
            11);

        pnd_Acis_Mos_Txt_Data__R_Field_5 = pnd_Acis_Mos_Txt_Data__R_Field_3.newGroupInGroup("pnd_Acis_Mos_Txt_Data__R_Field_5", "REDEFINE", pnd_Acis_Mos_Txt_Data_Pnd_Acis_Ssn);
        pnd_Acis_Mos_Txt_Data_Pnd_Acis_Ssn_A3 = pnd_Acis_Mos_Txt_Data__R_Field_5.newFieldInGroup("pnd_Acis_Mos_Txt_Data_Pnd_Acis_Ssn_A3", "#ACIS-SSN-A3", 
            FieldType.STRING, 3);
        pnd_Acis_Mos_Txt_Data_Pnd_Acis_Ssn__1 = pnd_Acis_Mos_Txt_Data__R_Field_5.newFieldInGroup("pnd_Acis_Mos_Txt_Data_Pnd_Acis_Ssn__1", "#ACIS-SSN--1", 
            FieldType.STRING, 1);
        pnd_Acis_Mos_Txt_Data_Pnd_Acis_Ssn_A2 = pnd_Acis_Mos_Txt_Data__R_Field_5.newFieldInGroup("pnd_Acis_Mos_Txt_Data_Pnd_Acis_Ssn_A2", "#ACIS-SSN-A2", 
            FieldType.STRING, 2);
        pnd_Acis_Mos_Txt_Data_Pnd_Acis_Ssn__2 = pnd_Acis_Mos_Txt_Data__R_Field_5.newFieldInGroup("pnd_Acis_Mos_Txt_Data_Pnd_Acis_Ssn__2", "#ACIS-SSN--2", 
            FieldType.STRING, 1);
        pnd_Acis_Mos_Txt_Data_Pnd_Acis_Ssn_A4 = pnd_Acis_Mos_Txt_Data__R_Field_5.newFieldInGroup("pnd_Acis_Mos_Txt_Data_Pnd_Acis_Ssn_A4", "#ACIS-SSN-A4", 
            FieldType.STRING, 4);
        pnd_Acis_Mos_Txt_Data_Pnd_Sp4 = pnd_Acis_Mos_Txt_Data__R_Field_3.newFieldInGroup("pnd_Acis_Mos_Txt_Data_Pnd_Sp4", "#SP4", FieldType.STRING, 1);
        pnd_Acis_Mos_Txt_Data_Pnd_Acis_Rltn = pnd_Acis_Mos_Txt_Data__R_Field_3.newFieldInGroup("pnd_Acis_Mos_Txt_Data_Pnd_Acis_Rltn", "#ACIS-RLTN", FieldType.STRING, 
            15);

        pnd_Acis_Mos_Txt_Data__R_Field_6 = pnd_Acis_Mos_Txt_Data__R_Field_2.newGroupInGroup("pnd_Acis_Mos_Txt_Data__R_Field_6", "REDEFINE", pnd_Acis_Mos_Txt_Data_Pnd_Acis_Mos_Txt_Data_Rest);
        pnd_Acis_Mos_Txt_Data_Pnd_Sp3_Il = pnd_Acis_Mos_Txt_Data__R_Field_6.newFieldInGroup("pnd_Acis_Mos_Txt_Data_Pnd_Sp3_Il", "#SP3-IL", FieldType.STRING, 
            1);
        pnd_Acis_Mos_Txt_Data_Pnd_Acis_Rltn_Il = pnd_Acis_Mos_Txt_Data__R_Field_6.newFieldInGroup("pnd_Acis_Mos_Txt_Data_Pnd_Acis_Rltn_Il", "#ACIS-RLTN-IL", 
            FieldType.STRING, 10);
        pnd_Acis_Mos_Txt_Data_Pnd_Sp4_Il = pnd_Acis_Mos_Txt_Data__R_Field_6.newFieldInGroup("pnd_Acis_Mos_Txt_Data_Pnd_Sp4_Il", "#SP4-IL", FieldType.STRING, 
            2);
        pnd_Acis_Mos_Txt_Data_Pnd_Acis_Ssn_Il = pnd_Acis_Mos_Txt_Data__R_Field_6.newFieldInGroup("pnd_Acis_Mos_Txt_Data_Pnd_Acis_Ssn_Il", "#ACIS-SSN-IL", 
            FieldType.STRING, 9);
        pnd_Acis_Mos_Txt_Data_Pnd_Sp5_Il = pnd_Acis_Mos_Txt_Data__R_Field_6.newFieldInGroup("pnd_Acis_Mos_Txt_Data_Pnd_Sp5_Il", "#SP5-IL", FieldType.STRING, 
            6);
        pnd_Acis_Mos_Txt_Data_Pnd_Acis_Birth_Il = pnd_Acis_Mos_Txt_Data__R_Field_6.newFieldInGroup("pnd_Acis_Mos_Txt_Data_Pnd_Acis_Birth_Il", "#ACIS-BIRTH-IL", 
            FieldType.STRING, 8);
        pnd_Acis_Birth_Hold = localVariables.newFieldInRecord("pnd_Acis_Birth_Hold", "#ACIS-BIRTH-HOLD", FieldType.STRING, 8);
        pnd_Acis_Ssn_Field = localVariables.newFieldInRecord("pnd_Acis_Ssn_Field", "#ACIS-SSN-FIELD", FieldType.STRING, 9);

        pnd_Acis_Ssn_Field__R_Field_7 = localVariables.newGroupInRecord("pnd_Acis_Ssn_Field__R_Field_7", "REDEFINE", pnd_Acis_Ssn_Field);
        pnd_Acis_Ssn_Field_Pnd_Ssn_A3 = pnd_Acis_Ssn_Field__R_Field_7.newFieldInGroup("pnd_Acis_Ssn_Field_Pnd_Ssn_A3", "#SSN-A3", FieldType.STRING, 3);
        pnd_Acis_Ssn_Field_Pnd_Ssn_A2 = pnd_Acis_Ssn_Field__R_Field_7.newFieldInGroup("pnd_Acis_Ssn_Field_Pnd_Ssn_A2", "#SSN-A2", FieldType.STRING, 2);
        pnd_Acis_Ssn_Field_Pnd_Ssn_A4 = pnd_Acis_Ssn_Field__R_Field_7.newFieldInGroup("pnd_Acis_Ssn_Field_Pnd_Ssn_A4", "#SSN-A4", FieldType.STRING, 4);
        pnd_Date_Field = localVariables.newFieldInRecord("pnd_Date_Field", "#DATE-FIELD", FieldType.STRING, 8);

        pnd_Date_Field__R_Field_8 = localVariables.newGroupInRecord("pnd_Date_Field__R_Field_8", "REDEFINE", pnd_Date_Field);
        pnd_Date_Field_Pnd_Date_Cc = pnd_Date_Field__R_Field_8.newFieldInGroup("pnd_Date_Field_Pnd_Date_Cc", "#DATE-CC", FieldType.STRING, 2);
        pnd_Date_Field_Pnd_Date_Yy = pnd_Date_Field__R_Field_8.newFieldInGroup("pnd_Date_Field_Pnd_Date_Yy", "#DATE-YY", FieldType.STRING, 2);
        pnd_Date_Field_Pnd_Date_Mm = pnd_Date_Field__R_Field_8.newFieldInGroup("pnd_Date_Field_Pnd_Date_Mm", "#DATE-MM", FieldType.STRING, 2);
        pnd_Date_Field_Pnd_Date_Dd = pnd_Date_Field__R_Field_8.newFieldInGroup("pnd_Date_Field_Pnd_Date_Dd", "#DATE-DD", FieldType.STRING, 2);
        pnd_Num_Acis_Benes_P = localVariables.newFieldInRecord("pnd_Num_Acis_Benes_P", "#NUM-ACIS-BENES-P", FieldType.NUMERIC, 2);
        pnd_Num_Acis_Benes_C = localVariables.newFieldInRecord("pnd_Num_Acis_Benes_C", "#NUM-ACIS-BENES-C", FieldType.NUMERIC, 2);
        pnd_Structured_Data_Found = localVariables.newFieldInRecord("pnd_Structured_Data_Found", "#STRUCTURED-DATA-FOUND", FieldType.BOOLEAN, 1);
        pnd_1st_Acis_Cntngnt = localVariables.newFieldInRecord("pnd_1st_Acis_Cntngnt", "#1ST-ACIS-CNTNGNT", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_bc.reset();
        vw_bd.reset();
        vw_bm.reset();
        vw_bci.reset();
        vw_bdi.reset();
        vw_bmi.reset();

        localVariables.reset();
        pnd_Acis_Mos_Txt_Data.setInitialValue("                                      /  /      -  -");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Benn9802() throws Exception
    {
        super("Benn9802");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ERROR-INFO-SUB.##LAST-PROGRAM := *PROGRAM
                                                                                                                                                                          //Natural: PERFORM PROCESS-CONTRACT
        sub_Process_Contract();
        if (condition(Global.isEscape())) {return;}
        //*  SEPARATE PROCESSING TO
        //*  CREATE ALLOCATION FOR ACIS OR
        //*  CREATE MOS IF SPECIAL TEXT.
        if (condition(pnd_Intrfcng_Systm.equals("ACIS") && pdaBena9801.getBena9801_Mos_Ind().notEquals("Y") && pnd_Dflt_To_Estate_Ind.notEquals("Y")))                    //Natural: IF #INTRFCNG-SYSTM = 'ACIS' AND BENA9801.MOS-IND NE 'Y' AND #DFLT-TO-ESTATE-IND NE 'Y'
        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-ACIS
            sub_Process_Acis();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaBena9801.getBena9801_Mos_Ind().equals("Y") || pnd_Acis_Mos_Txt.getValue("*").notEquals(" ")))                                                    //Natural: IF BENA9801.MOS-IND = 'Y' OR #ACIS-MOS-TXT ( * ) NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-MOS
            sub_Process_Mos();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-DESIGNATION
            sub_Process_Designation();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-CONTRACT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-MOS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-DESIGNATION
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-DFLT-ESTATE
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-ACIS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-ACIS-MOS
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-ERROR
    }
    private void sub_Process_Contract() throws Exception                                                                                                                  //Natural: PROCESS-CONTRACT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        GET01:                                                                                                                                                            //Natural: GET BCI BENA9802.#CNTRCT-ISN
        vw_bci.readByID(pdaBena9802.getBena9802_Pnd_Cntrct_Isn().getLong(), "GET01");
        //*  BUILD YYYYMMDDHHIISST TO BE USED FOR BC-, BD-, BM- TIMESTAMP
        pnd_Timestamp_Pnd_Timx.setValueEdited(Global.getTIMX(),new ReportEditMask("HHIISST"));                                                                            //Natural: MOVE EDITED *TIMX ( EM = HHIISST ) TO #TIMX
        pnd_Timestamp_Pnd_Datx.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                           //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO #DATX
        //*  CURRENT
        vw_bc.reset();                                                                                                                                                    //Natural: RESET BC
        bc_Bc_Pin.setValue(pdaBena9801.getBena9801_Pin());                                                                                                                //Natural: ASSIGN BC.BC-PIN := BENA9801.PIN
        bc_Bc_Tiaa_Cntrct.setValue(pdaBena9801.getBena9801_Tiaa_Cntrct());                                                                                                //Natural: ASSIGN BC.BC-TIAA-CNTRCT := BENA9801.TIAA-CNTRCT
        bc_Bc_Cref_Cntrct.setValue(pdaBena9801.getBena9801_Cref_Cntrct());                                                                                                //Natural: ASSIGN BC.BC-CREF-CNTRCT := BENA9801.CREF-CNTRCT
        bc_Bc_Tiaa_Cref_Ind.setValue(pdaBena9801.getBena9801_Tiaa_Cref_Ind());                                                                                            //Natural: ASSIGN BC.BC-TIAA-CREF-IND := BENA9801.TIAA-CREF-IND
        bc_Bc_Stat.setValue("C");                                                                                                                                         //Natural: ASSIGN BC.BC-STAT := 'C'
        bc_Bc_Cntrct_Type.setValue(bci_Cntrct_Type);                                                                                                                      //Natural: ASSIGN BC.BC-CNTRCT-TYPE := BCI.CNTRCT-TYPE
        bc_Bc_Eff_Dte.setValue(bci_Eff_Dte);                                                                                                                              //Natural: ASSIGN BC.BC-EFF-DTE := BCI.EFF-DTE
        if (condition(bci_Mos_Ind.equals("Y")))                                                                                                                           //Natural: IF BCI.MOS-IND = 'Y'
        {
            bc_Bc_Mos_Ind.setValue(bci_Mos_Ind);                                                                                                                          //Natural: ASSIGN BC.BC-MOS-IND := BCI.MOS-IND
            //*  ALWAYS HAS A VALUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            bc_Bc_Mos_Ind.setValue("N");                                                                                                                                  //Natural: ASSIGN BC.BC-MOS-IND := 'N'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(bci_Irrvcble_Ind.equals("Y")))                                                                                                                      //Natural: IF BCI.IRRVCBLE-IND = 'Y'
        {
            bc_Bc_Irvcbl_Ind.setValue(bci_Irrvcble_Ind);                                                                                                                  //Natural: ASSIGN BC.BC-IRVCBL-IND := BCI.IRRVCBLE-IND
            pnd_Cont_Irvcbl_Set.setValue(true);                                                                                                                           //Natural: ASSIGN #CONT-IRVCBL-SET := TRUE
            //*  ALWAYS HAS A VALUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            bc_Bc_Irvcbl_Ind.setValue("N");                                                                                                                               //Natural: ASSIGN BC.BC-IRVCBL-IND := 'N'
            pnd_Cont_Irvcbl_Set.reset();                                                                                                                                  //Natural: RESET #CONT-IRVCBL-SET
            //*  INTERFACE
        }                                                                                                                                                                 //Natural: END-IF
        bc_Bc_Pymnt_Chld_Dcsd_Ind.setValue(bci_Pymnt_Chld_Dcsd_Ind);                                                                                                      //Natural: ASSIGN BC.BC-PYMNT-CHLD-DCSD-IND := BCI.PYMNT-CHLD-DCSD-IND
        bc_Bc_Exempt_Spouse_Rights.setValue(bci_Exempt_Spouse_Rights);                                                                                                    //Natural: ASSIGN BC.BC-EXEMPT-SPOUSE-RIGHTS := BCI.EXEMPT-SPOUSE-RIGHTS
        bc_Bc_Spouse_Waived_Bnfts.setValue(bci_Spouse_Waived_Bnfts);                                                                                                      //Natural: ASSIGN BC.BC-SPOUSE-WAIVED-BNFTS := BCI.SPOUSE-WAIVED-BNFTS
        bc_Bc_Tiaa_Cref_Chng_Dte.setValue(bci_Tiaa_Cref_Chng_Dte);                                                                                                        //Natural: ASSIGN BC.BC-TIAA-CREF-CHNG-DTE := BCI.TIAA-CREF-CHNG-DTE
        bc_Bc_Tiaa_Cref_Chng_Tme.setValue(bci_Tiaa_Cref_Chng_Tme);                                                                                                        //Natural: ASSIGN BC.BC-TIAA-CREF-CHNG-TME := BCI.TIAA-CREF-CHNG-TME
        bc_Bc_Chng_Pwr_Atty.setValue(bci_Chng_Pwr_Atty);                                                                                                                  //Natural: ASSIGN BC.BC-CHNG-PWR-ATTY := BCI.CHNG-PWR-ATTY
        bc_Bc_Rqst_Timestamp.setValue(pnd_Timestamp);                                                                                                                     //Natural: ASSIGN BC.BC-RQST-TIMESTAMP := #TIMESTAMP
        bc_Bc_Rcrd_Last_Updt_Dte.setValue(pnd_Timestamp_Pnd_Datx);                                                                                                        //Natural: ASSIGN BC.BC-RCRD-LAST-UPDT-DTE := #DATX
        bc_Bc_Rcrd_Last_Updt_Tme.setValue(pnd_Timestamp_Pnd_Timx);                                                                                                        //Natural: ASSIGN BC.BC-RCRD-LAST-UPDT-TME := #TIMX
        bc_Bc_Rcrd_Last_Updt_Userid.setValue(Global.getPROGRAM());                                                                                                        //Natural: ASSIGN BC.BC-RCRD-LAST-UPDT-USERID := *PROGRAM
        bc_Bc_Last_Dsgntn_Srce.setValue("I");                                                                                                                             //Natural: ASSIGN BC.BC-LAST-DSGNTN-SRCE := 'I'
        bc_Bc_Last_Dsgntn_Userid.setValue(Global.getPROGRAM());                                                                                                           //Natural: ASSIGN BC.BC-LAST-DSGNTN-USERID := *PROGRAM
        bc_Bc_Last_Dsgntn_Dte.setValue(pdaBena9802.getBena9802_Pnd_Bsnss_Dte());                                                                                          //Natural: ASSIGN BC.BC-LAST-DSGNTN-DTE := BENA9802.#BSNSS-DTE
        bc_Bc_Last_Dsgntn_Tme.setValue(pnd_Timestamp_Pnd_Timx);                                                                                                           //Natural: ASSIGN BC.BC-LAST-DSGNTN-TME := #TIMX
        if (condition(bci_Rqstng_Systm.equals("CORR")))                                                                                                                   //Natural: IF BCI.RQSTNG-SYSTM = 'CORR'
        {
            bc_Bc_Last_Dsgntn_System.setValue("X");                                                                                                                       //Natural: ASSIGN BC.BC-LAST-DSGNTN-SYSTEM := 'X'
            bc_Bc_Fldr_Log_Dte_Tme.setValue(pdaBena9801.getBena9801_Fldr_Log_Dte_Tme());                                                                                  //Natural: ASSIGN BC.BC-FLDR-LOG-DTE-TME := BENA9801.FLDR-LOG-DTE-TME
            //*  TRUNCATE:A/C/M
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            bc_Bc_Last_Dsgntn_System.setValue(bci_Intrfcng_Systm);                                                                                                        //Natural: ASSIGN BC.BC-LAST-DSGNTN-SYSTEM := BCI.INTRFCNG-SYSTM
            bc_Bc_Fldr_Log_Dte_Tme.setValue(bci_Fldr_Log_Dte_Tme);                                                                                                        //Natural: ASSIGN BC.BC-FLDR-LOG-DTE-TME := BCI.FLDR-LOG-DTE-TME
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Intrfcng_Systm.setValue(bci_Intrfcng_Systm);                                                                                                                  //Natural: ASSIGN #INTRFCNG-SYSTM := BCI.INTRFCNG-SYSTM
        pnd_Rqstng_Systm.setValue(bci_Rqstng_Systm);                                                                                                                      //Natural: ASSIGN #RQSTNG-SYSTM := BCI.RQSTNG-SYSTM
        pnd_Dflt_To_Estate_Ind.setValue(bci_Dflt_To_Estate_Ind);                                                                                                          //Natural: ASSIGN #DFLT-TO-ESTATE-IND := BCI.DFLT-TO-ESTATE-IND
        SBC:                                                                                                                                                              //Natural: STORE BC
        vw_bc.insertDBRow("SBC");
        //*  PROCESS-CONTRACT
    }
    private void sub_Process_Mos() throws Exception                                                                                                                       //Natural: PROCESS-MOS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        vw_bm.reset();                                                                                                                                                    //Natural: RESET BM
        bm_Bm_Pin.setValue(bc_Bc_Pin);                                                                                                                                    //Natural: ASSIGN BM.BM-PIN := BC.BC-PIN
        bm_Bm_Tiaa_Cntrct.setValue(bc_Bc_Tiaa_Cntrct);                                                                                                                    //Natural: ASSIGN BM.BM-TIAA-CNTRCT := BC.BC-TIAA-CNTRCT
        bm_Bm_Cref_Cntrct.setValue(bc_Bc_Cref_Cntrct);                                                                                                                    //Natural: ASSIGN BM.BM-CREF-CNTRCT := BC.BC-CREF-CNTRCT
        bm_Bm_Tiaa_Cref_Ind.setValue(bc_Bc_Tiaa_Cref_Ind);                                                                                                                //Natural: ASSIGN BM.BM-TIAA-CREF-IND := BC.BC-TIAA-CREF-IND
        bm_Bm_Stat.setValue(bc_Bc_Stat);                                                                                                                                  //Natural: ASSIGN BM.BM-STAT := BC.BC-STAT
        bm_Bm_Rqst_Timestamp.setValue(bc_Bc_Rqst_Timestamp);                                                                                                              //Natural: ASSIGN BM.BM-RQST-TIMESTAMP := BC.BC-RQST-TIMESTAMP
        bm_Bm_Rcrd_Last_Updt_Dte.setValue(bc_Bc_Rcrd_Last_Updt_Dte);                                                                                                      //Natural: ASSIGN BM.BM-RCRD-LAST-UPDT-DTE := BC.BC-RCRD-LAST-UPDT-DTE
        bm_Bm_Rcrd_Last_Updt_Tme.setValue(bc_Bc_Rcrd_Last_Updt_Tme);                                                                                                      //Natural: ASSIGN BM.BM-RCRD-LAST-UPDT-TME := BC.BC-RCRD-LAST-UPDT-TME
        bm_Bm_Rcrd_Last_Updt_Userid.setValue(bc_Bc_Rcrd_Last_Updt_Userid);                                                                                                //Natural: ASSIGN BM.BM-RCRD-LAST-UPDT-USERID := BC.BC-RCRD-LAST-UPDT-USERID
        if (condition(! (pnd_Acis_Mos_Txt.getValue("*").notEquals(" "))))                                                                                                 //Natural: IF NOT ( #ACIS-MOS-TXT ( * ) NE ' ' )
        {
            GET02:                                                                                                                                                        //Natural: GET BMI BENA9802.#MOS-ISN
            vw_bmi.readByID(pdaBena9802.getBena9802_Pnd_Mos_Isn().getLong(), "GET02");
            bm_Bm_Mos_Txt.getValue("*").setValue(bmi_Mos_Txt.getValue("*"));                                                                                              //Natural: ASSIGN BM.BM-MOS-TXT ( * ) := BMI.MOS-TXT ( * )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            bm_Bm_Mos_Txt.getValue("*").setValue(pnd_Acis_Mos_Txt.getValue("*"));                                                                                         //Natural: ASSIGN BM.BM-MOS-TXT ( * ) := #ACIS-MOS-TXT ( * )
        }                                                                                                                                                                 //Natural: END-IF
        vw_bm.insertDBRow();                                                                                                                                              //Natural: STORE BM
        pnd_Acis_Mos_Txt.getValue("*").reset();                                                                                                                           //Natural: RESET #ACIS-MOS-TXT ( * )
        //*  PROCESS-MOS
    }
    private void sub_Process_Designation() throws Exception                                                                                                               //Natural: PROCESS-DESIGNATION
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_I1.reset();                                                                                                                                                   //Natural: RESET #I1
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            pnd_I1.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #I1
            vw_bd.reset();                                                                                                                                                //Natural: RESET BD
            if (condition(pdaBena9802.getBena9802_Pnd_Dsgntn_Isn().getValue(pnd_I1).equals(getZero())))                                                                   //Natural: IF BENA9802.#DSGNTN-ISN ( #I1 ) = 0
            {
                if (condition(! (bci_Dflt_To_Estate_Ind.equals("Y"))))                                                                                                    //Natural: IF NOT BCI.DFLT-TO-ESTATE-IND = 'Y'
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(! (bci_Dflt_To_Estate_Ind.equals("Y"))))                                                                                                        //Natural: IF NOT BCI.DFLT-TO-ESTATE-IND = 'Y'
            {
                GET03:                                                                                                                                                    //Natural: GET BDI BENA9802.#DSGNTN-ISN ( #I1 )
                vw_bdi.readByID(pdaBena9802.getBena9802_Pnd_Dsgntn_Isn().getValue(pnd_I1).getLong(), "GET03");
            }                                                                                                                                                             //Natural: END-IF
            //*  BYPASS, DONT NEED SINGLE 'NONE' CNTNGNT
            if (condition(bdi_Bene_Name1.equals("NONE")))                                                                                                                 //Natural: IF BDI.BENE-NAME1 = 'NONE'
            {
                if (condition(pdaBena9802.getBena9802_Pnd_Dsgntn_Isn().getValue(pnd_I1.getDec().add(1)).equals(getZero())))                                               //Natural: IF BENA9802.#DSGNTN-ISN ( #I1 + 1 ) = 0
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                    //*  CREATE CRASH - EDIT IN BENN9820 DIDN't work!!
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress(Global.getPROGRAM(), "'NONE' Designation to be reset with bene following!!"));    //Natural: COMPRESS *PROGRAM '"NONE" Designation to be reset with bene following!!' TO ##MSG
                    //*  CRASH & PASS CONTROL TO P9800
                                                                                                                                                                          //Natural: PERFORM FORMAT-ERROR
                    sub_Format_Error();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_I1.setValue(61);                                                                                                                                  //Natural: ASSIGN #I1 := 61
                    pnd_Acis_Cntngt_Mos_Txt.getValue(pnd_I1).setValue(" ");                                                                                               //Natural: ASSIGN #ACIS-CNTNGT-MOS-TXT ( #I1 ) := ' '
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            bd_Bd_Pin.setValue(bc_Bc_Pin);                                                                                                                                //Natural: ASSIGN BD.BD-PIN := BC.BC-PIN
            bd_Bd_Tiaa_Cntrct.setValue(bc_Bc_Tiaa_Cntrct);                                                                                                                //Natural: ASSIGN BD.BD-TIAA-CNTRCT := BC.BC-TIAA-CNTRCT
            bd_Bd_Cref_Cntrct.setValue(bc_Bc_Cref_Cntrct);                                                                                                                //Natural: ASSIGN BD.BD-CREF-CNTRCT := BC.BC-CREF-CNTRCT
            bd_Bd_Tiaa_Cref_Ind.setValue(bc_Bc_Tiaa_Cref_Ind);                                                                                                            //Natural: ASSIGN BD.BD-TIAA-CREF-IND := BC.BC-TIAA-CREF-IND
            bd_Bd_Stat.setValue(bc_Bc_Stat);                                                                                                                              //Natural: ASSIGN BD.BD-STAT := BC.BC-STAT
            bd_Bd_Seq_Nmbr.setValue(pnd_I1);                                                                                                                              //Natural: ASSIGN BD.BD-SEQ-NMBR := #I1
            bd_Bd_Rqst_Timestamp.setValue(bc_Bc_Rqst_Timestamp);                                                                                                          //Natural: ASSIGN BD.BD-RQST-TIMESTAMP := BC.BC-RQST-TIMESTAMP
            bd_Bd_Rcrd_Last_Updt_Dte.setValue(bc_Bc_Rcrd_Last_Updt_Dte);                                                                                                  //Natural: ASSIGN BD.BD-RCRD-LAST-UPDT-DTE := BC.BC-RCRD-LAST-UPDT-DTE
            bd_Bd_Rcrd_Last_Updt_Tme.setValue(bc_Bc_Rcrd_Last_Updt_Tme);                                                                                                  //Natural: ASSIGN BD.BD-RCRD-LAST-UPDT-TME := BC.BC-RCRD-LAST-UPDT-TME
            bd_Bd_Rcrd_Last_Updt_Userid.setValue(bc_Bc_Rcrd_Last_Updt_Userid);                                                                                            //Natural: ASSIGN BD.BD-RCRD-LAST-UPDT-USERID := BC.BC-RCRD-LAST-UPDT-USERID
            //*  CONTRACT LEVEL INT.
            if (condition(bci_Dflt_To_Estate_Ind.equals("Y")))                                                                                                            //Natural: IF BCI.DFLT-TO-ESTATE-IND = 'Y'
            {
                //* COMMENTED AS CONT. LEV. ONLY AUTHORITY
                                                                                                                                                                          //Natural: PERFORM CREATE-DFLT-ESTATE
                sub_Create_Dflt_Estate();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  PRIMARY
                if (condition(bdi_Bene_Type.equals("P")))                                                                                                                 //Natural: IF BDI.BENE-TYPE = 'P'
                {
                    bd_Bd_Bene_Type.setValue("P");                                                                                                                        //Natural: ASSIGN BD.BD-BENE-TYPE := 'P'
                    //*  SECONDARY/CONTINGENT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    bd_Bd_Bene_Type.setValue("S");                                                                                                                        //Natural: ASSIGN BD.BD-BENE-TYPE := 'S'
                }                                                                                                                                                         //Natural: END-IF
                bd_Bd_Rltn_Cd.setValue(pdaBena9802.getBena9802_Pnd_Dsgntn_Rltn_Cde().getValue(pnd_I1));                                                                   //Natural: ASSIGN BD.BD-RLTN-CD := BENA9802.#DSGNTN-RLTN-CDE ( #I1 )
                bd_Bd_Bene_Name1.setValue(bdi_Bene_Name1);                                                                                                                //Natural: ASSIGN BD.BD-BENE-NAME1 := BDI.BENE-NAME1
                bd_Bd_Bene_Name2.setValue(bdi_Bene_Name2);                                                                                                                //Natural: ASSIGN BD.BD-BENE-NAME2 := BDI.BENE-NAME2
                //*  OTHER - MOVE RLTN-FREE-TXT
                if (condition(bd_Bd_Rltn_Cd.equals("38")))                                                                                                                //Natural: IF BD.BD-RLTN-CD = '38'
                {
                    bd_Bd_Rltn_Free_Txt.setValue(bdi_Rltn_Free_Txt);                                                                                                      //Natural: ASSIGN BD.BD-RLTN-FREE-TXT := BDI.RLTN-FREE-TXT
                    //*  CANNOT BE BLANK
                    if (condition(bd_Bd_Rltn_Free_Txt.equals(" ")))                                                                                                       //Natural: IF BD.BD-RLTN-FREE-TXT = ' '
                    {
                        bd_Bd_Rltn_Free_Txt.setValue("OTHER");                                                                                                            //Natural: ASSIGN BD.BD-RLTN-FREE-TXT := 'OTHER'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  OTHER
                if (condition(((bdi_Rltn_Free_Txt.equals("OTHER") || bdi_Rltn_Free_Txt.equals("Other")) && pdaBena9802.getBena9802_Pnd_Dsgntn_Rltn_Cde().getValue(pnd_I1).notEquals("38")))) //Natural: IF BDI.RLTN-FREE-TXT = 'OTHER' OR = 'Other' AND BENA9802.#DSGNTN-RLTN-CDE ( #I1 ) NE '38'
                {
                    bd_Bd_Rltn_Cd.setValue("38");                                                                                                                         //Natural: ASSIGN BD.BD-RLTN-CD := '38'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(bdi_Dte_Birth_Trust.equals(" ") || bdi_Dte_Birth_Trust.equals("0") || bdi_Dte_Birth_Trust.equals("00000000")))                              //Natural: IF BDI.DTE-BIRTH-TRUST = ' ' OR = '0' OR = '00000000'
                {
                    bd_Bd_Dte_Birth_Trust.reset();                                                                                                                        //Natural: RESET BD-DTE-BIRTH-TRUST
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  RESET IF ACIS & DATE INVALID
                    if (condition((bci_Intrfcng_Systm.equals("ACIS") && ((! (DbsUtil.maskMatches(bdi_Dte_Birth_Trust,"YYYYMMDD")) || bdi_Dte_Birth_Trust.greater(pdaBena9802.getBena9802_Pnd_Bsnss_Dte()))  //Natural: IF BCI.INTRFCNG-SYSTM = 'ACIS' AND ( BDI.DTE-BIRTH-TRUST NE MASK ( YYYYMMDD ) OR BDI.DTE-BIRTH-TRUST > #BSNSS-DTE OR BDI.DTE-BIRTH-TRUST < '16000000' )
                        || bdi_Dte_Birth_Trust.less("16000000")))))
                    {
                        bd_Bd_Dte_Birth_Trust.reset();                                                                                                                    //Natural: RESET BD.BD-DTE-BIRTH-TRUST
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        bd_Bd_Dte_Birth_Trust.setValue(bdi_Dte_Birth_Trust);                                                                                              //Natural: ASSIGN BD.BD-DTE-BIRTH-TRUST := BDI.DTE-BIRTH-TRUST
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                bd_Bd_Ss_Cd.setValue(bdi_Ssn_Cde);                                                                                                                        //Natural: ASSIGN BD.BD-SS-CD := BDI.SSN-CDE
                if (condition(bdi_Ssn.equals("0") || bdi_Ssn.equals("000000000")))                                                                                        //Natural: IF BDI.SSN = '0' OR = '000000000'
                {
                    bd_Bd_Ss_Nmbr.reset();                                                                                                                                //Natural: RESET BD.BD-SS-NMBR
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    bd_Bd_Ss_Nmbr.setValue(bdi_Ssn);                                                                                                                      //Natural: ASSIGN BD.BD-SS-NMBR := BDI.SSN
                }                                                                                                                                                         //Natural: END-IF
                //*  THERE IS ONE CONDITION WHEN A PERCENTAGE/FRACTION COULD BE MISSING AND
                //*  THAT IS WHEN THERE IS ONLY ONE PRIMARY OR 1 PRIMARY/1 CONTINGENT (SEE
                //*  BENN9820) AND IT/THEY ARE MISSING THEIR ALLOCATION. THIS HAS
                //*  BEEN ALLOWED THROUGH THE EDITS AS IT IS OBVIOUS THE ALLOCATION IS 100%
                //*  OR 1/1, AND LEGITIMATE EXAMPLES OF THIS DATA EXIST IN CIS. THIS WILL
                //*  ALWAYS BE MADE AS 'F' 1/1, EVEN IF THE PRCNT-FRCTN-IND = 'P'
                //*  HERE's another condition! We'RE CREATING ALLOCATIONS FOR ACIS WHEN
                //*  THERE IS NO SPECIAL TEXT. - ALL EQUAL.
                //*  NO ACIS ALLOCATION, CREATE ONE
                //*  SHARE
                if (condition(((bci_Intrfcng_Systm.equals("ACIS") || bci_Intrfcng_Systm.equals("ISV")) && bdi_Prcnt_Frctn_Ind.equals(" "))))                              //Natural: IF BCI.INTRFCNG-SYSTM = 'ACIS' OR = 'ISV' AND BDI.PRCNT-FRCTN-IND = ' '
                {
                    bd_Bd_Perc_Share_Ind.setValue("S");                                                                                                                   //Natural: ASSIGN BD.BD-PERC-SHARE-IND := 'S'
                    bd_Bd_Share_Ntor.setValue(1);                                                                                                                         //Natural: ASSIGN BD.BD-SHARE-NTOR := 1
                    if (condition(bd_Bd_Bene_Type.equals("P")))                                                                                                           //Natural: IF BD.BD-BENE-TYPE = 'P'
                    {
                        bd_Bd_Share_Dtor.setValue(pnd_Num_Acis_Benes_P);                                                                                                  //Natural: ASSIGN BD.BD-SHARE-DTOR := #NUM-ACIS-BENES-P
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        bd_Bd_Share_Dtor.setValue(pnd_Num_Acis_Benes_C);                                                                                                  //Natural: ASSIGN BD.BD-SHARE-DTOR := #NUM-ACIS-BENES-C
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(bdi_Prcnt_Frctn_Ind.equals("P")))                                                                                                       //Natural: IF BDI.PRCNT-FRCTN-IND = 'P'
                    {
                        bd_Bd_Perc_Share_Ind.setValue(bdi_Prcnt_Frctn_Ind);                                                                                               //Natural: ASSIGN BD.BD-PERC-SHARE-IND := BDI.PRCNT-FRCTN-IND
                        if (condition(bdi_Share_Prcnt.greater(getZero())))                                                                                                //Natural: IF BDI.SHARE-PRCNT > 0
                        {
                            bd_Bd_Share_Perc.setValue(bdi_Share_Prcnt);                                                                                                   //Natural: ASSIGN BD.BD-SHARE-PERC := BDI.SHARE-PRCNT
                            //*  SEE COMMENTS ABOVE
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            bd_Bd_Share_Perc.setValue(100);                                                                                                               //Natural: ASSIGN BD.BD-SHARE-PERC := 100.00
                        }                                                                                                                                                 //Natural: END-IF
                        //*  'F'RACTION ON INTERFACE
                        //*  'S'HARE ON BENE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        bd_Bd_Perc_Share_Ind.setValue("S");                                                                                                               //Natural: ASSIGN BD.BD-PERC-SHARE-IND := 'S'
                        //*  SEE COMMENTS ABOVE
                        if (condition(bdi_Share_Nmrtr.equals(getZero())))                                                                                                 //Natural: IF BDI.SHARE-NMRTR = 0
                        {
                            bd_Bd_Share_Ntor.setValue(1);                                                                                                                 //Natural: ASSIGN BD.BD-SHARE-NTOR := 1
                            bd_Bd_Share_Dtor.setValue(1);                                                                                                                 //Natural: ASSIGN BD.BD-SHARE-DTOR := 1
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            bd_Bd_Share_Ntor.setValue(bdi_Share_Nmrtr);                                                                                                   //Natural: ASSIGN BD.BD-SHARE-NTOR := BDI.SHARE-NMRTR
                            bd_Bd_Share_Dtor.setValue(bdi_Share_Dnmntr);                                                                                                  //Natural: ASSIGN BD.BD-SHARE-DTOR := BDI.SHARE-DNMNTR
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(bdi_Irrvcble_Ind.equals("Y")))                                                                                                              //Natural: IF BDI.IRRVCBLE-IND = 'Y'
                {
                    bd_Bd_Irvcbl_Ind.setValue(bdi_Irrvcble_Ind);                                                                                                          //Natural: ASSIGN BD.BD-IRVCBL-IND := BDI.IRRVCBLE-IND
                    //*  BENES. RULE IS THAT THE CONTRACT LEVEL
                    //*  INDICATOR WILL BE SET IF 1 (OR MORE) OF
                    if (condition(pnd_Cont_Irvcbl_Set.getBoolean()))                                                                                                      //Natural: IF #CONT-IRVCBL-SET
                    {
                        ignore();
                        //*  THE DESIG. LEVL ARE SET.
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Cont_Irvcbl_Set.setValue(true);                                                                                                               //Natural: ASSIGN #CONT-IRVCBL-SET := TRUE
                        G1:                                                                                                                                               //Natural: GET BC *ISN ( SBC. )
                        vw_bc.readByID(vw_bc.getAstISN("SBC"), "G1");
                        bc_Bc_Irvcbl_Ind.setValue("Y");                                                                                                                   //Natural: ASSIGN BC.BC-IRVCBL-IND := 'Y'
                        vw_bc.updateDBRow("G1");                                                                                                                          //Natural: UPDATE ( G1. )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    bd_Bd_Irvcbl_Ind.setValue("N");                                                                                                                       //Natural: ASSIGN BD.BD-IRVCBL-IND := 'N'
                }                                                                                                                                                         //Natural: END-IF
                //*  DEFAULT = 'N'ONE
                if (condition(bdi_Sttlmnt_Rstrctn.notEquals("N")))                                                                                                        //Natural: IF BDI.STTLMNT-RSTRCTN NE 'N'
                {
                    bdi_Sttlmnt_Rstrctn.setValue("N");                                                                                                                    //Natural: ASSIGN BDI.STTLMNT-RSTRCTN := 'N'
                }                                                                                                                                                         //Natural: END-IF
                bd_Bd_Stlmnt_Rstrctn.setValue(bdi_Sttlmnt_Rstrctn);                                                                                                       //Natural: ASSIGN BD.BD-STLMNT-RSTRCTN := BDI.STTLMNT-RSTRCTN
                bd_Bd_Spcl_Txt1.setValue(bdi_Spcl_Txt1);                                                                                                                  //Natural: ASSIGN BD.BD-SPCL-TXT1 := BDI.SPCL-TXT1
                bd_Bd_Spcl_Txt2.setValue(bdi_Spcl_Txt2);                                                                                                                  //Natural: ASSIGN BD.BD-SPCL-TXT2 := BDI.SPCL-TXT2
                bd_Bd_Spcl_Txt3.setValue(bdi_Spcl_Txt3);                                                                                                                  //Natural: ASSIGN BD.BD-SPCL-TXT3 := BDI.SPCL-TXT3
                bd_Bd_Addr1.setValue(bdi_Bene_Addr1);                                                                                                                     //Natural: ASSIGN BD.BD-ADDR1 := BDI.BENE-ADDR1
                bd_Bd_Addr2.setValue(bdi_Bene_Addr2);                                                                                                                     //Natural: ASSIGN BD.BD-ADDR2 := BDI.BENE-ADDR2
                bd_Bd_Addr3_City.setValue(bdi_Bene_Addr3_City);                                                                                                           //Natural: ASSIGN BD.BD-ADDR3-CITY := BDI.BENE-ADDR3-CITY
                bd_Bd_State.setValue(bdi_Bene_State);                                                                                                                     //Natural: ASSIGN BD.BD-STATE := BDI.BENE-STATE
                bd_Bd_Zip.setValue(bdi_Bene_Zip);                                                                                                                         //Natural: ASSIGN BD.BD-ZIP := BDI.BENE-ZIP
                bd_Bd_Phone.setValue(bdi_Bene_Phone);                                                                                                                     //Natural: ASSIGN BD.BD-PHONE := BDI.BENE-PHONE
                bd_Bd_Gender.setValue(bdi_Bene_Gender);                                                                                                                   //Natural: ASSIGN BD.BD-GENDER := BDI.BENE-GENDER
                bd_Bd_Country.setValue(bdi_Bene_Country);                                                                                                                 //Natural: ASSIGN BD.BD-COUNTRY := BDI.BENE-COUNTRY
            }                                                                                                                                                             //Natural: END-IF
            vw_bd.insertDBRow();                                                                                                                                          //Natural: STORE BD
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  PROCESS-DESIGNATION
    }
    //*  ESTATE
    private void sub_Create_Dflt_Estate() throws Exception                                                                                                                //Natural: CREATE-DFLT-ESTATE
    {
        if (BLNatReinput.isReinput()) return;

        bd_Bd_Bene_Name1.setValue("Estate");                                                                                                                              //Natural: ASSIGN BD.BD-BENE-NAME1 := 'Estate'
        bd_Bd_Bene_Name2.setValue("Default to Estate");                                                                                                                   //Natural: ASSIGN BD.BD-BENE-NAME2 := 'Default to Estate'
        bd_Bd_Bene_Type.setValue("P");                                                                                                                                    //Natural: ASSIGN BD.BD-BENE-TYPE := 'P'
        bd_Bd_Rltn_Cd.setValue("05");                                                                                                                                     //Natural: ASSIGN BD.BD-RLTN-CD := '05'
        bd_Bd_Share_Dtor.setValue(1);                                                                                                                                     //Natural: ASSIGN BD.BD-SHARE-DTOR := BD.BD-SHARE-NTOR := 1
        bd_Bd_Share_Ntor.setValue(1);
        bd_Bd_Dflt_Estate.setValue("Y");                                                                                                                                  //Natural: ASSIGN BD.BD-DFLT-ESTATE := 'Y'
        bd_Bd_Perc_Share_Ind.setValue("S");                                                                                                                               //Natural: ASSIGN BD.BD-PERC-SHARE-IND := 'S'
        bd_Bd_Irvcbl_Ind.setValue("N");                                                                                                                                   //Natural: ASSIGN BD.BD-IRVCBL-IND := 'N'
        bd_Bd_Stlmnt_Rstrctn.setValue("N");                                                                                                                               //Natural: ASSIGN BD.BD-STLMNT-RSTRCTN := 'N'
        vw_bd.insertDBRow();                                                                                                                                              //Natural: STORE BD
        //*  CREATE-DFLT-ESTATE
    }
    private void sub_Process_Acis() throws Exception                                                                                                                      //Natural: PROCESS-ACIS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Acis_Mos_Txt.getValue("*").reset();                                                                                                                           //Natural: RESET #ACIS-MOS-TXT ( * ) #NUM-ACIS-BENES-P #NUM-ACIS-BENES-C
        pnd_Num_Acis_Benes_P.reset();
        pnd_Num_Acis_Benes_C.reset();
        if (condition(pdaBena9802.getBena9802_Pnd_Interface_As_Mos().getBoolean()))                                                                                       //Natural: IF BENA9802.#INTERFACE-AS-MOS
        {
                                                                                                                                                                          //Natural: PERFORM CREATE-ACIS-MOS
            sub_Create_Acis_Mos();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            F1ACIS:                                                                                                                                                       //Natural: FOR #I1 = 1 TO 20
            for (pnd_I1.setValue(1); condition(pnd_I1.lessOrEqual(20)); pnd_I1.nadd(1))
            {
                if (condition(pdaBena9802.getBena9802_Pnd_Dsgntn_Isn().getValue(pnd_I1).equals(getZero())))                                                               //Natural: IF BENA9802.#DSGNTN-ISN ( #I1 ) = 0
                {
                    if (true) break F1ACIS;                                                                                                                               //Natural: ESCAPE BOTTOM ( F1ACIS. )
                }                                                                                                                                                         //Natural: END-IF
                BDACIS:                                                                                                                                                   //Natural: GET BDI BENA9802.#DSGNTN-ISN ( #I1 )
                vw_bdi.readByID(pdaBena9802.getBena9802_Pnd_Dsgntn_Isn().getValue(pnd_I1).getLong(), "BDACIS");
                if (condition(pnd_Rqstng_Systm.equals("IRAP") || pnd_Rqstng_Systm.equals("LOAN")))                                                                        //Natural: IF #RQSTNG-SYSTM = 'IRAP' OR = 'LOAN'
                {
                    //*  IF DOB INVALID
                    if (condition(bdi_Dte_Birth_Trust.notEquals(" ") && bdi_Dte_Birth_Trust.notEquals("0") && bdi_Dte_Birth_Trust.notEquals("00000000")                   //Natural: IF ( ( BDI.DTE-BIRTH-TRUST NE ' ' AND BDI.DTE-BIRTH-TRUST NE '0' AND BDI.DTE-BIRTH-TRUST NE '00000000' ) AND NOT ( BDI.DTE-BIRTH-TRUST EQ MASK ( YYYYMMDD ) AND BDI.DTE-BIRTH-TRUST LE #BSNSS-DTE AND BDI.DTE-BIRTH-TRUST GT '16000000' ) )
                        && ! (DbsUtil.maskMatches(bdi_Dte_Birth_Trust,"YYYYMMDD") && bdi_Dte_Birth_Trust.lessOrEqual(pdaBena9802.getBena9802_Pnd_Bsnss_Dte()) 
                        && bdi_Dte_Birth_Trust.greater("16000000"))))
                    {
                                                                                                                                                                          //Natural: PERFORM CREATE-ACIS-MOS
                        sub_Create_Acis_Mos();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("F1ACIS"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("F1ACIS"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (true) break F1ACIS;                                                                                                                           //Natural: ESCAPE BOTTOM ( F1ACIS. )
                    }                                                                                                                                                     //Natural: END-IF
                    //*  VALID SSN
                    //*  OR THERE IS A REL
                    //*  ALLOW REGULAR PROCESS
                    if (condition((bdi_Dte_Birth_Trust.notEquals(" ") && bdi_Dte_Birth_Trust.notEquals("0") && bdi_Dte_Birth_Trust.notEquals("00000000"))                 //Natural: IF ( BDI.DTE-BIRTH-TRUST NE ' ' AND BDI.DTE-BIRTH-TRUST NE '0' AND BDI.DTE-BIRTH-TRUST NE '00000000' ) OR ( BDI.SSN NE ' ' AND BDI.SSN NE '000000000' ) OR BDI.RLTN-FREE-TXT NE ' '
                        || (bdi_Ssn.notEquals(" ") && bdi_Ssn.notEquals("000000000")) || bdi_Rltn_Free_Txt.notEquals(" ")))
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM CREATE-ACIS-MOS
                        sub_Create_Acis_Mos();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("F1ACIS"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("F1ACIS"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (true) break F1ACIS;                                                                                                                           //Natural: ESCAPE BOTTOM ( F1ACIS. )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(((bdi_Spcl_Txt1.notEquals(" ") || bdi_Spcl_Txt2.notEquals(" ")) || (bdi_Spcl_Txt3.notEquals(" ") && ! (((bdi_Spcl_Txt1.equals("You currently have not designated a beneficiary; we will default")  //Natural: IF BDI.SPCL-TXT1 NE ' ' OR BDI.SPCL-TXT2 NE ' ' OR BDI.SPCL-TXT3 NE ' ' AND NOT ( BDI.SPCL-TXT1 = 'You currently have not designated a beneficiary; we will default' AND BDI.SPCL-TXT2 = 'to the provisions of the plan and/or product for this account.' AND BDI.SPCL-TXT3 = 'Please add a beneficiary.' )
                    && bdi_Spcl_Txt2.equals("to the provisions of the plan and/or product for this account.")) && bdi_Spcl_Txt3.equals("Please add a beneficiary.")))))))
                {
                                                                                                                                                                          //Natural: PERFORM CREATE-ACIS-MOS
                    sub_Create_Acis_Mos();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("F1ACIS"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("F1ACIS"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (true) break F1ACIS;                                                                                                                               //Natural: ESCAPE BOTTOM ( F1ACIS. )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(bdi_Bene_Type.equals("P")))                                                                                                             //Natural: IF BDI.BENE-TYPE = 'P'
                    {
                        pnd_Num_Acis_Benes_P.nadd(1);                                                                                                                     //Natural: ADD 1 TO #NUM-ACIS-BENES-P
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Num_Acis_Benes_C.nadd(1);                                                                                                                     //Natural: ADD 1 TO #NUM-ACIS-BENES-C
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Acis_Mos_Txt.getValue("*").notEquals(" ")))                                                                                                     //Natural: IF #ACIS-MOS-TXT ( * ) NE ' '
        {
            GMU:                                                                                                                                                          //Natural: GET BC *ISN ( SBC. )
            vw_bc.readByID(vw_bc.getAstISN("SBC"), "GMU");
            bc_Bc_Mos_Ind.setValue("Y");                                                                                                                                  //Natural: ASSIGN BC-MOS-IND := 'Y'
            vw_bc.updateDBRow("GMU");                                                                                                                                     //Natural: UPDATE ( GMU. )
        }                                                                                                                                                                 //Natural: END-IF
        //*  PROCESS-ACIS
    }
    private void sub_Create_Acis_Mos() throws Exception                                                                                                                   //Natural: CREATE-ACIS-MOS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_K1.setValue(1);                                                                                                                                               //Natural: ASSIGN #K1 := 1
        pnd_C1.reset();                                                                                                                                                   //Natural: RESET #C1 #ACIS-CNTNGT-MOS-TXT ( * )
        pnd_Acis_Cntngt_Mos_Txt.getValue("*").reset();
        pnd_1st_Acis_Cntngnt.setValue(true);                                                                                                                              //Natural: ASSIGN #1ST-ACIS-CNTNGNT := TRUE
        pnd_Structured_Data_Found.reset();                                                                                                                                //Natural: RESET #STRUCTURED-DATA-FOUND
        F2ACIS:                                                                                                                                                           //Natural: FOR #I1 = 1 TO 20
        for (pnd_I1.setValue(1); condition(pnd_I1.lessOrEqual(20)); pnd_I1.nadd(1))
        {
            if (condition(pdaBena9802.getBena9802_Pnd_Dsgntn_Isn().getValue(pnd_I1).equals(getZero())))                                                                   //Natural: IF BENA9802.#DSGNTN-ISN ( #I1 ) = 0
            {
                if (true) break F2ACIS;                                                                                                                                   //Natural: ESCAPE BOTTOM ( F2ACIS. )
            }                                                                                                                                                             //Natural: END-IF
            BDACIS2:                                                                                                                                                      //Natural: GET BDI BENA9802.#DSGNTN-ISN ( #I1 )
            vw_bdi.readByID(pdaBena9802.getBena9802_Pnd_Dsgntn_Isn().getValue(pnd_I1).getLong(), "BDACIS2");
            //*  ACIS DOESN't use Name2
            pnd_Acis_Mos_Txt_Data.resetInitial();                                                                                                                         //Natural: RESET INITIAL #ACIS-MOS-TXT-DATA
            pnd_Acis_Mos_Txt_Data_Pnd_Acis_Name.setValue(bdi_Bene_Name1);                                                                                                 //Natural: ASSIGN #ACIS-NAME := BDI.BENE-NAME1
            if (condition(bdi_Dte_Birth_Trust.notEquals(" ") && bdi_Dte_Birth_Trust.notEquals("0") && bdi_Dte_Birth_Trust.notEquals("00000000")))                         //Natural: IF BDI.DTE-BIRTH-TRUST NE ' ' AND BDI.DTE-BIRTH-TRUST NE '0' AND BDI.DTE-BIRTH-TRUST NE '00000000'
            {
                if (condition(DbsUtil.maskMatches(bdi_Dte_Birth_Trust,"YYYYMMDD") && bdi_Dte_Birth_Trust.lessOrEqual(pdaBena9802.getBena9802_Pnd_Bsnss_Dte())             //Natural: IF BDI.DTE-BIRTH-TRUST = MASK ( YYYYMMDD ) AND BDI.DTE-BIRTH-TRUST LE #BSNSS-DTE AND BDI.DTE-BIRTH-TRUST GT '16000000'
                    && bdi_Dte_Birth_Trust.greater("16000000")))
                {
                    pnd_Date_Field.setValue(bdi_Dte_Birth_Trust);                                                                                                         //Natural: ASSIGN #DATE-FIELD := BDI.DTE-BIRTH-TRUST
                    pnd_Acis_Mos_Txt_Data_Pnd_Acis_Birth_Yy.setValue(pnd_Date_Field_Pnd_Date_Yy);                                                                         //Natural: ASSIGN #ACIS-BIRTH-YY := #DATE-YY
                    pnd_Acis_Mos_Txt_Data_Pnd_Acis_Birth_Mm.setValue(pnd_Date_Field_Pnd_Date_Mm);                                                                         //Natural: ASSIGN #ACIS-BIRTH-MM := #DATE-MM
                    pnd_Acis_Mos_Txt_Data_Pnd_Acis_Birth_Dd.setValue(pnd_Date_Field_Pnd_Date_Dd);                                                                         //Natural: ASSIGN #ACIS-BIRTH-DD := #DATE-DD
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Rqstng_Systm.equals("IRAP") || pnd_Rqstng_Systm.equals("LOAN")))                                                                    //Natural: IF #RQSTNG-SYSTM = 'IRAP' OR = 'LOAN'
                    {
                        pnd_Acis_Mos_Txt_Data_Pnd_Acis_Birth.setValue(bdi_Dte_Birth_Trust);                                                                               //Natural: ASSIGN #ACIS-BIRTH := BDI.DTE-BIRTH-TRUST
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Acis_Ssn_Field.setValue(bdi_Ssn);                                                                                                                         //Natural: ASSIGN #ACIS-SSN-FIELD := BDI.SSN
            pnd_Acis_Mos_Txt_Data_Pnd_Acis_Ssn_A3.setValue(pnd_Acis_Ssn_Field_Pnd_Ssn_A3);                                                                                //Natural: ASSIGN #ACIS-SSN-A3 := #SSN-A3
            pnd_Acis_Mos_Txt_Data_Pnd_Acis_Ssn_A2.setValue(pnd_Acis_Ssn_Field_Pnd_Ssn_A2);                                                                                //Natural: ASSIGN #ACIS-SSN-A2 := #SSN-A2
            pnd_Acis_Mos_Txt_Data_Pnd_Acis_Ssn_A4.setValue(pnd_Acis_Ssn_Field_Pnd_Ssn_A4);                                                                                //Natural: ASSIGN #ACIS-SSN-A4 := #SSN-A4
            pnd_Acis_Mos_Txt_Data_Pnd_Acis_Rltn.setValue(bdi_Rltn_Free_Txt);                                                                                              //Natural: ASSIGN #ACIS-RLTN := BDI.RLTN-FREE-TXT
            if (condition(pnd_Rqstng_Systm.equals("IRAP") || pnd_Rqstng_Systm.equals("LOAN")))                                                                            //Natural: IF #RQSTNG-SYSTM = 'IRAP' OR = 'LOAN'
            {
                pnd_Acis_Birth_Hold.setValue(pnd_Acis_Mos_Txt_Data_Pnd_Acis_Birth);                                                                                       //Natural: ASSIGN #ACIS-BIRTH-HOLD := #ACIS-BIRTH
                pnd_Acis_Mos_Txt_Data_Pnd_Acis_Mos_Txt_Data_Rest.reset();                                                                                                 //Natural: RESET #ACIS-MOS-TXT-DATA-REST
                if (condition(pnd_Acis_Birth_Hold.notEquals("  /  /")))                                                                                                   //Natural: IF #ACIS-BIRTH-HOLD NE '  /  /'
                {
                    pnd_Acis_Mos_Txt_Data_Pnd_Acis_Birth_Il.setValue(pnd_Acis_Birth_Hold);                                                                                //Natural: ASSIGN #ACIS-BIRTH-IL := #ACIS-BIRTH-HOLD
                }                                                                                                                                                         //Natural: END-IF
                pnd_Acis_Mos_Txt_Data_Pnd_Acis_Ssn_Il.setValue(bdi_Ssn);                                                                                                  //Natural: ASSIGN #ACIS-SSN-IL := BDI.SSN
                pnd_Acis_Mos_Txt_Data_Pnd_Acis_Rltn_Il.setValue(bdi_Rltn_Free_Txt);                                                                                       //Natural: ASSIGN #ACIS-RLTN-IL := BDI.RLTN-FREE-TXT
                if (condition(pnd_Acis_Mos_Txt_Data.equals(" ")))                                                                                                         //Natural: IF #ACIS-MOS-TXT-DATA = ' '
                {
                    pnd_Acis_Mos_Txt_Data.resetInitial();                                                                                                                 //Natural: RESET INITIAL #ACIS-MOS-TXT-DATA
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  SECONDARY/CONTINGENT
            if (condition(bdi_Bene_Type.equals("C")))                                                                                                                     //Natural: IF BDI.BENE-TYPE = 'C'
            {
                if (condition(pnd_1st_Acis_Cntngnt.getBoolean()))                                                                                                         //Natural: IF #1ST-ACIS-CNTNGNT
                {
                    pnd_C1.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #C1
                    pnd_1st_Acis_Cntngnt.reset();                                                                                                                         //Natural: RESET #1ST-ACIS-CNTNGNT
                    pnd_Acis_Cntngt_Mos_Txt.getValue(pnd_C1).setValue("Contingent:");                                                                                     //Natural: ASSIGN #ACIS-CNTNGT-MOS-TXT ( #C1 ) := 'Contingent:'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Acis_Mos_Txt_Data.notEquals("                                      /  /      -  -")))                                                   //Natural: IF #ACIS-MOS-TXT-DATA NE '                                      /  /      -  -'
                {
                    pnd_C1.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #C1
                    pnd_Acis_Cntngt_Mos_Txt.getValue(pnd_C1).setValue(pnd_Acis_Mos_Txt_Data);                                                                             //Natural: ASSIGN #ACIS-CNTNGT-MOS-TXT ( #C1 ) := #ACIS-MOS-TXT-DATA
                    pnd_Structured_Data_Found.setValue(true);                                                                                                             //Natural: ASSIGN #STRUCTURED-DATA-FOUND := TRUE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(bdi_Spcl_Txt1.notEquals(" ")))                                                                                                              //Natural: IF BDI.SPCL-TXT1 NE ' '
                {
                    pnd_C1.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #C1
                    pnd_Acis_Cntngt_Mos_Txt.getValue(pnd_C1).setValue(bdi_Spcl_Txt1);                                                                                     //Natural: ASSIGN #ACIS-CNTNGT-MOS-TXT ( #C1 ) := BDI.SPCL-TXT1
                }                                                                                                                                                         //Natural: END-IF
                if (condition(bdi_Spcl_Txt2.notEquals(" ")))                                                                                                              //Natural: IF BDI.SPCL-TXT2 NE ' '
                {
                    pnd_C1.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #C1
                    pnd_Acis_Cntngt_Mos_Txt.getValue(pnd_C1).setValue(bdi_Spcl_Txt2);                                                                                     //Natural: ASSIGN #ACIS-CNTNGT-MOS-TXT ( #C1 ) := BDI.SPCL-TXT2
                }                                                                                                                                                         //Natural: END-IF
                if (condition(bdi_Spcl_Txt3.greater(" ")))                                                                                                                //Natural: IF BDI.SPCL-TXT3 GT ' '
                {
                    pnd_C1.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #C1
                    pnd_Acis_Cntngt_Mos_Txt.getValue(pnd_C1).setValue(bdi_Spcl_Txt3);                                                                                     //Natural: ASSIGN #ACIS-CNTNGT-MOS-TXT ( #C1 ) := BDI.SPCL-TXT3
                }                                                                                                                                                         //Natural: END-IF
                //*  BLANK LINE AFTER BENE
                pnd_C1.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #C1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Acis_Mos_Txt_Data.notEquals("                                      /  /      -  -")))                                                   //Natural: IF #ACIS-MOS-TXT-DATA NE '                                      /  /      -  -'
                {
                    pnd_K1.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #K1
                    pnd_Acis_Mos_Txt.getValue(pnd_K1).setValue(pnd_Acis_Mos_Txt_Data);                                                                                    //Natural: ASSIGN #ACIS-MOS-TXT ( #K1 ) := #ACIS-MOS-TXT-DATA
                    pnd_Structured_Data_Found.setValue(true);                                                                                                             //Natural: ASSIGN #STRUCTURED-DATA-FOUND := TRUE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(bdi_Spcl_Txt1.notEquals(" ")))                                                                                                              //Natural: IF BDI.SPCL-TXT1 NE ' '
                {
                    pnd_K1.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #K1
                    pnd_Acis_Mos_Txt.getValue(pnd_K1).setValue(bdi_Spcl_Txt1);                                                                                            //Natural: ASSIGN #ACIS-MOS-TXT ( #K1 ) := BDI.SPCL-TXT1
                }                                                                                                                                                         //Natural: END-IF
                if (condition(bdi_Spcl_Txt2.notEquals(" ")))                                                                                                              //Natural: IF BDI.SPCL-TXT2 NE ' '
                {
                    pnd_K1.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #K1
                    pnd_Acis_Mos_Txt.getValue(pnd_K1).setValue(bdi_Spcl_Txt2);                                                                                            //Natural: ASSIGN #ACIS-MOS-TXT ( #K1 ) := BDI.SPCL-TXT2
                }                                                                                                                                                         //Natural: END-IF
                if (condition(bdi_Spcl_Txt3.notEquals(" ")))                                                                                                              //Natural: IF BDI.SPCL-TXT3 NE ' '
                {
                    pnd_K1.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #K1
                    pnd_Acis_Mos_Txt.getValue(pnd_K1).setValue(bdi_Spcl_Txt3);                                                                                            //Natural: ASSIGN #ACIS-MOS-TXT ( #K1 ) := BDI.SPCL-TXT3
                }                                                                                                                                                         //Natural: END-IF
                //*  BLANK LINE AFTER BENE
                pnd_K1.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #K1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_C1.greater(getZero())))                                                                                                                         //Natural: IF #C1 > 0
        {
            pnd_Tabend.compute(new ComputeParameters(false, pnd_Tabend), pnd_K1.add(pnd_C1));                                                                             //Natural: ADD #K1 #C1 GIVING #TABEND
            pnd_Acis_Mos_Txt.getValue(pnd_K1.getDec().add(1),":",pnd_Tabend).setValue(pnd_Acis_Cntngt_Mos_Txt.getValue(1,":",pnd_C1));                                    //Natural: ASSIGN #ACIS-MOS-TXT ( #K1+1:#TABEND ) := #ACIS-CNTNGT-MOS-TXT ( 1:#C1 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Structured_Data_Found.getBoolean()))                                                                                                            //Natural: IF #STRUCTURED-DATA-FOUND
        {
            //*  SWAPPED DISPLAY TO MATCH ACIS
            //*  INPUT FORM
            if (condition(pnd_Rqstng_Systm.equals("IRAP") || pnd_Rqstng_Systm.equals("LOAN")))                                                                            //Natural: IF #RQSTNG-SYSTM = 'IRAP' OR = 'LOAN'
            {
                pnd_Acis_Mos_Txt.getValue(1).setValue("Primary: Name                       Relationship    SSN         Birth Dt");                                        //Natural: ASSIGN #ACIS-MOS-TXT ( 1 ) := 'Primary: Name                       Relationship    SSN         Birth Dt'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Acis_Mos_Txt.getValue(1).setValue("Primary: Name                       Birth Dt    SSN      Relationship");                                           //Natural: ASSIGN #ACIS-MOS-TXT ( 1 ) := 'Primary: Name                       Birth Dt    SSN      Relationship'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Acis_Mos_Txt.getValue(1).setValue("Primary:");                                                                                                            //Natural: ASSIGN #ACIS-MOS-TXT ( 1 ) := 'Primary:'
        }                                                                                                                                                                 //Natural: END-IF
        //*  CREATE-ACIS-MOS
    }
    private void sub_Format_Error() throws Exception                                                                                                                      //Natural: FORMAT-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().write(0, "*************************************************************");                                                                           //Natural: WRITE '*************************************************************'
        if (Global.isEscape()) return;
        getReports().write(0, "*************************************************************");                                                                           //Natural: WRITE '*************************************************************'
        if (Global.isEscape()) return;
        getReports().write(0, "*                                                           *");                                                                           //Natural: WRITE '*                                                           *'
        if (Global.isEscape()) return;
        getReports().write(0, "*",pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                                             //Natural: WRITE '*' ##MSG
        if (Global.isEscape()) return;
        getReports().write(0, "*                                                           *");                                                                           //Natural: WRITE '*                                                           *'
        if (Global.isEscape()) return;
        getReports().write(0, "*************************************************************");                                                                           //Natural: WRITE '*************************************************************'
        if (Global.isEscape()) return;
        getReports().write(0, "*************************************************************");                                                                           //Natural: WRITE '*************************************************************'
        if (Global.isEscape()) return;
        //*  FORMAT-ERROR
    }

    //
}
