/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:06:01 PM
**        * FROM NATURAL SUBPROGRAM : Benn9812
************************************************************
**        * FILE NAME            : Benn9812.java
**        * CLASS NAME           : Benn9812
**        * INSTANCE NAME        : Benn9812
************************************************************
************************************************************************
* PROGRAM  : BENN9812
* SYSTEM   : BENEFICIARY-SYSTEM
* TITLE    : UPDATE ERROR TABLE
* GENERATED: JULY 1, 1999
************************************************************************
*    DATE      USERID                   DESCRIPTION
* MM/DD/YYYY  XXXXXXXX  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
* 04/01/2017  DURAND    PIN EXPANSION N7 TO N12
************************************************************************

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benn9812 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaBena9812 pdaBena9812;
    private PdaBenpda_M pdaBenpda_M;
    private PdaBenpda_E pdaBenpda_E;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_bci;
    private DbsField bci_Intrfce_Stts;
    private DbsField bci_Rcrd_Crtd_For_Bsnss_Dte;
    private DbsField bci_Rcrd_Crtd_Dte;
    private DbsField bci_Rcrd_Crtd_Tme;
    private DbsField bci_Intrfcng_Systm;
    private DbsField bci_Rqstng_Systm;
    private DbsField bci_Rcrd_Last_Updt_Dte;
    private DbsField bci_Rcrd_Last_Updt_Tme;
    private DbsField bci_Rcrd_Last_Updt_Userid;
    private DbsField bci_Last_Prcssd_Bsnss_Dte;
    private DbsField bci_Intrfce_Cmpltd_Dte;
    private DbsField bci_Intrfce_Cmpltd_Tme;
    private DbsField bci_Dflt_To_Estate_Ind;
    private DbsField bci_Illgble_Ind;
    private DbsField bci_More_Than_Five_Benes_Ind;
    private DbsField bci_Intrfce_Mgrtn_Ind;
    private DbsField bci_More_Than_Thirty_Benes_Ind;
    private DbsField bci_Pin_Tiaa_Cntrct;
    private DbsField bci_Count_Casterror_Table;

    private DbsGroup bci_Error_Table;
    private DbsField bci_Error_Txt;
    private DbsField bci_Error_Cde;
    private DbsField bci_Error_Dte;
    private DbsField bci_Error_Tme;
    private DbsField bci_Error_Pgm;
    private DbsField bci_Fldr_Log_Dte_Tme;
    private DbsField bci_Last_Dsgntn_Srce;
    private DbsField bci_Last_Dsgntn_System;
    private DbsField bci_Last_Dsgntn_Userid;
    private DbsField bci_Last_Dsgntn_Dte;
    private DbsField bci_Last_Dsgntn_Tme;
    private DbsField bci_Tiaa_Cref_Chng_Dte;
    private DbsField bci_Tiaa_Cref_Chng_Tme;
    private DbsField bci_Chng_Pwr_Atty;
    private DbsField bci_Cref_Cntrct;
    private DbsField bci_Tiaa_Cref_Ind;
    private DbsField bci_Cntrct_Type;
    private DbsField bci_Eff_Dte;
    private DbsField bci_Mos_Ind;
    private DbsField bci_Irrvcble_Ind;
    private DbsField bci_Pymnt_Chld_Dcsd_Ind;
    private DbsField bci_Exempt_Spouse_Rights;
    private DbsField bci_Spouse_Waived_Bnfts;
    private DbsField bci_Chng_New_Issue_Ind;
    private DbsField bci_Same_As_Ind;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Next_Bucket;
    private DbsField pnd_Intrfce_Super_2;

    private DbsGroup pnd_Intrfce_Super_2__R_Field_1;
    private DbsField pnd_Intrfce_Super_2_Pnd_Intrfce_Stts;
    private DbsField pnd_Intrfce_Super_2_Pnd_Rcrd_Crtd_For_Bsnss_Dte;
    private DbsField pnd_Intrfce_Super_2_Pnd_Pin_Tiaa_Cntrct;
    private DbsField pnd_Intrfce_Super_2_Pnd_Tiaa_Cref_Ind;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaBena9812 = new PdaBena9812(parameters);
        pdaBenpda_M = new PdaBenpda_M(parameters);
        pdaBenpda_E = new PdaBenpda_E(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_bci = new DataAccessProgramView(new NameInfo("vw_bci", "BCI"), "BENE_CONTRACT_INTERFACE_12", "BENE_CONT_INTFCE", DdmPeriodicGroups.getInstance().getGroups("BENE_CONTRACT_INTERFACE_12"));
        bci_Intrfce_Stts = vw_bci.getRecord().newFieldInGroup("bci_Intrfce_Stts", "INTRFCE-STTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, "INTRFCE_STTS");
        bci_Intrfce_Stts.setDdmHeader("INTRFCE/STATUS");
        bci_Rcrd_Crtd_For_Bsnss_Dte = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Crtd_For_Bsnss_Dte", "RCRD-CRTD-FOR-BSNSS-DTE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "RCRD_CRTD_FOR_BSNSS_DTE");
        bci_Rcrd_Crtd_For_Bsnss_Dte.setDdmHeader("RCRD CRTD/FOR/BSNSS DTE");
        bci_Rcrd_Crtd_Dte = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Crtd_Dte", "RCRD-CRTD-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_CRTD_DTE");
        bci_Rcrd_Crtd_Dte.setDdmHeader("RECORD/CREATED/DATE");
        bci_Rcrd_Crtd_Tme = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Crtd_Tme", "RCRD-CRTD-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "RCRD_CRTD_TME");
        bci_Rcrd_Crtd_Tme.setDdmHeader("RECORD/CREATED/TIME");
        bci_Intrfcng_Systm = vw_bci.getRecord().newFieldInGroup("bci_Intrfcng_Systm", "INTRFCNG-SYSTM", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "INTRFCNG_SYSTM");
        bci_Intrfcng_Systm.setDdmHeader("INTRFCNG/SYSTEM");
        bci_Rqstng_Systm = vw_bci.getRecord().newFieldInGroup("bci_Rqstng_Systm", "RQSTNG-SYSTM", FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQSTNG_SYSTM");
        bci_Rqstng_Systm.setDdmHeader("RQSTNG/SYSTEM");
        bci_Rcrd_Last_Updt_Dte = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Last_Updt_Dte", "RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_DTE");
        bci_Rcrd_Last_Updt_Dte.setDdmHeader("RECORD/LAST UPDT/DATE");
        bci_Rcrd_Last_Updt_Tme = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Last_Updt_Tme", "RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_TME");
        bci_Rcrd_Last_Updt_Tme.setDdmHeader("RECORD/LAST UPDT/TIME");
        bci_Rcrd_Last_Updt_Userid = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Last_Updt_Userid", "RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_USERID");
        bci_Rcrd_Last_Updt_Userid.setDdmHeader("RECORD/LAST UPDT/USER ID");
        bci_Last_Prcssd_Bsnss_Dte = vw_bci.getRecord().newFieldInGroup("bci_Last_Prcssd_Bsnss_Dte", "LAST-PRCSSD-BSNSS-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "LAST_PRCSSD_BSNSS_DTE");
        bci_Last_Prcssd_Bsnss_Dte.setDdmHeader("LAST PRCSSD/BUSINESS/DATE");
        bci_Intrfce_Cmpltd_Dte = vw_bci.getRecord().newFieldInGroup("bci_Intrfce_Cmpltd_Dte", "INTRFCE-CMPLTD-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "INTRFCE_CMPLTD_DTE");
        bci_Intrfce_Cmpltd_Dte.setDdmHeader("INTFCE/CMPLTD/DATE");
        bci_Intrfce_Cmpltd_Tme = vw_bci.getRecord().newFieldInGroup("bci_Intrfce_Cmpltd_Tme", "INTRFCE-CMPLTD-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "INTRFCE_CMPLTD_TME");
        bci_Intrfce_Cmpltd_Tme.setDdmHeader("INTRFCE/CMPLTD/TME");
        bci_Dflt_To_Estate_Ind = vw_bci.getRecord().newFieldInGroup("bci_Dflt_To_Estate_Ind", "DFLT-TO-ESTATE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "DFLT_TO_ESTATE_IND");
        bci_Dflt_To_Estate_Ind.setDdmHeader("DEFAULT/TO/ESTATE");
        bci_Illgble_Ind = vw_bci.getRecord().newFieldInGroup("bci_Illgble_Ind", "ILLGBLE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "ILLGBLE_IND");
        bci_Illgble_Ind.setDdmHeader("ILLGBLE/IND");
        bci_More_Than_Five_Benes_Ind = vw_bci.getRecord().newFieldInGroup("bci_More_Than_Five_Benes_Ind", "MORE-THAN-FIVE-BENES-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "MORE_THAN_FIVE_BENES_IND");
        bci_More_Than_Five_Benes_Ind.setDdmHeader("MORE/THAN 5/BENES");
        bci_Intrfce_Mgrtn_Ind = vw_bci.getRecord().newFieldInGroup("bci_Intrfce_Mgrtn_Ind", "INTRFCE-MGRTN-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "INTRFCE_MGRTN_IND");
        bci_Intrfce_Mgrtn_Ind.setDdmHeader("INTRFCE/MGRTN/IND");
        bci_More_Than_Thirty_Benes_Ind = vw_bci.getRecord().newFieldInGroup("bci_More_Than_Thirty_Benes_Ind", "MORE-THAN-THIRTY-BENES-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "MORE_THAN_THIRTY_BENES_IND");
        bci_More_Than_Thirty_Benes_Ind.setDdmHeader("MORE/THAN 30/BENES");
        bci_Pin_Tiaa_Cntrct = vw_bci.getRecord().newFieldInGroup("bci_Pin_Tiaa_Cntrct", "PIN-TIAA-CNTRCT", FieldType.STRING, 22, RepeatingFieldStrategy.None, 
            "PIN_TIAA_CNTRCT");
        bci_Pin_Tiaa_Cntrct.setDdmHeader("PIN/TIAA/CONTRACT");
        bci_Count_Casterror_Table = vw_bci.getRecord().newFieldInGroup("bci_Count_Casterror_Table", "C*ERROR-TABLE", RepeatingFieldStrategy.CAsteriskVariable, 
            "BENE_CONT_INTFCE_ERROR_TABLE");

        bci_Error_Table = vw_bci.getRecord().newGroupInGroup("bci_Error_Table", "ERROR-TABLE", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Error_Txt = bci_Error_Table.newFieldArrayInGroup("bci_Error_Txt", "ERROR-TXT", FieldType.STRING, 72, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ERROR_TXT", "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Error_Txt.setDdmHeader("ERROR/TEXT");
        bci_Error_Cde = bci_Error_Table.newFieldArrayInGroup("bci_Error_Cde", "ERROR-CDE", FieldType.STRING, 5, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ERROR_CDE", "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Error_Cde.setDdmHeader("ERROR/CODE");
        bci_Error_Dte = bci_Error_Table.newFieldArrayInGroup("bci_Error_Dte", "ERROR-DTE", FieldType.STRING, 8, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ERROR_DTE", "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Error_Dte.setDdmHeader("ERROR/DATE");
        bci_Error_Tme = bci_Error_Table.newFieldArrayInGroup("bci_Error_Tme", "ERROR-TME", FieldType.STRING, 7, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ERROR_TME", "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Error_Tme.setDdmHeader("ERROR/TIME");
        bci_Error_Pgm = bci_Error_Table.newFieldArrayInGroup("bci_Error_Pgm", "ERROR-PGM", FieldType.STRING, 8, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ERROR_PGM", "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Fldr_Log_Dte_Tme = vw_bci.getRecord().newFieldInGroup("bci_Fldr_Log_Dte_Tme", "FLDR-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "FLDR_LOG_DTE_TME");
        bci_Fldr_Log_Dte_Tme.setDdmHeader("FOLDER/LOG DATE/TIME");
        bci_Last_Dsgntn_Srce = vw_bci.getRecord().newFieldInGroup("bci_Last_Dsgntn_Srce", "LAST-DSGNTN-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "LAST_DSGNTN_SRCE");
        bci_Last_Dsgntn_Srce.setDdmHeader("LAST/DSGNTN/SOURCE");
        bci_Last_Dsgntn_System = vw_bci.getRecord().newFieldInGroup("bci_Last_Dsgntn_System", "LAST-DSGNTN-SYSTEM", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "LAST_DSGNTN_SYSTEM");
        bci_Last_Dsgntn_System.setDdmHeader("LAST/DSGNTN/SYSTEM");
        bci_Last_Dsgntn_Userid = vw_bci.getRecord().newFieldInGroup("bci_Last_Dsgntn_Userid", "LAST-DSGNTN-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "LAST_DSGNTN_USERID");
        bci_Last_Dsgntn_Userid.setDdmHeader("LAST/DSGNTN/USERID");
        bci_Last_Dsgntn_Dte = vw_bci.getRecord().newFieldInGroup("bci_Last_Dsgntn_Dte", "LAST-DSGNTN-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "LAST_DSGNTN_DTE");
        bci_Last_Dsgntn_Dte.setDdmHeader("LAST/DSGNTN/DATE");
        bci_Last_Dsgntn_Tme = vw_bci.getRecord().newFieldInGroup("bci_Last_Dsgntn_Tme", "LAST-DSGNTN-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "LAST_DSGNTN_TME");
        bci_Last_Dsgntn_Tme.setDdmHeader("LAST/DSGNTN/TIME");
        bci_Tiaa_Cref_Chng_Dte = vw_bci.getRecord().newFieldInGroup("bci_Tiaa_Cref_Chng_Dte", "TIAA-CREF-CHNG-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIAA_CREF_CHNG_DTE");
        bci_Tiaa_Cref_Chng_Dte.setDdmHeader("TIAA CREF/CHANGE/DATE");
        bci_Tiaa_Cref_Chng_Tme = vw_bci.getRecord().newFieldInGroup("bci_Tiaa_Cref_Chng_Tme", "TIAA-CREF-CHNG-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "TIAA_CREF_CHNG_TME");
        bci_Tiaa_Cref_Chng_Tme.setDdmHeader("TIAA CREF/CHANGE/TIME");
        bci_Chng_Pwr_Atty = vw_bci.getRecord().newFieldInGroup("bci_Chng_Pwr_Atty", "CHNG-PWR-ATTY", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CHNG_PWR_ATTY");
        bci_Chng_Pwr_Atty.setDdmHeader("CHANGE/POWER/ATTNY");
        bci_Cref_Cntrct = vw_bci.getRecord().newFieldInGroup("bci_Cref_Cntrct", "CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT");
        bci_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bci_Tiaa_Cref_Ind = vw_bci.getRecord().newFieldInGroup("bci_Tiaa_Cref_Ind", "TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIAA_CREF_IND");
        bci_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bci_Cntrct_Type = vw_bci.getRecord().newFieldInGroup("bci_Cntrct_Type", "CNTRCT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_TYPE");
        bci_Cntrct_Type.setDdmHeader("CNTRCT/TYPE");
        bci_Eff_Dte = vw_bci.getRecord().newFieldInGroup("bci_Eff_Dte", "EFF-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "EFF_DTE");
        bci_Eff_Dte.setDdmHeader("EFFECTIVE/DATE");
        bci_Mos_Ind = vw_bci.getRecord().newFieldInGroup("bci_Mos_Ind", "MOS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "MOS_IND");
        bci_Mos_Ind.setDdmHeader("MOS/IND");
        bci_Irrvcble_Ind = vw_bci.getRecord().newFieldInGroup("bci_Irrvcble_Ind", "IRRVCBLE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "IRRVCBLE_IND");
        bci_Irrvcble_Ind.setDdmHeader("IRVCBL/IND");
        bci_Pymnt_Chld_Dcsd_Ind = vw_bci.getRecord().newFieldInGroup("bci_Pymnt_Chld_Dcsd_Ind", "PYMNT-CHLD-DCSD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PYMNT_CHLD_DCSD_IND");
        bci_Pymnt_Chld_Dcsd_Ind.setDdmHeader("PYMNT/CHILD/DCSD");
        bci_Exempt_Spouse_Rights = vw_bci.getRecord().newFieldInGroup("bci_Exempt_Spouse_Rights", "EXEMPT-SPOUSE-RIGHTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "EXEMPT_SPOUSE_RIGHTS");
        bci_Exempt_Spouse_Rights.setDdmHeader("EXEMPT/SPOUSAL/RIGHTS");
        bci_Spouse_Waived_Bnfts = vw_bci.getRecord().newFieldInGroup("bci_Spouse_Waived_Bnfts", "SPOUSE-WAIVED-BNFTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "SPOUSE_WAIVED_BNFTS");
        bci_Spouse_Waived_Bnfts.setDdmHeader("SPOUSE/WAIVED/BENEFITS");
        bci_Chng_New_Issue_Ind = vw_bci.getRecord().newFieldInGroup("bci_Chng_New_Issue_Ind", "CHNG-NEW-ISSUE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CHNG_NEW_ISSUE_IND");
        bci_Chng_New_Issue_Ind.setDdmHeader("CHANGE/NEW ISSUE/IND");
        bci_Same_As_Ind = vw_bci.getRecord().newFieldInGroup("bci_Same_As_Ind", "SAME-AS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "SAME_AS_IND");
        registerRecord(vw_bci);

        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_Next_Bucket = localVariables.newFieldInRecord("pnd_Next_Bucket", "#NEXT-BUCKET", FieldType.PACKED_DECIMAL, 3);
        pnd_Intrfce_Super_2 = localVariables.newFieldInRecord("pnd_Intrfce_Super_2", "#INTRFCE-SUPER-2", FieldType.STRING, 32);

        pnd_Intrfce_Super_2__R_Field_1 = localVariables.newGroupInRecord("pnd_Intrfce_Super_2__R_Field_1", "REDEFINE", pnd_Intrfce_Super_2);
        pnd_Intrfce_Super_2_Pnd_Intrfce_Stts = pnd_Intrfce_Super_2__R_Field_1.newFieldInGroup("pnd_Intrfce_Super_2_Pnd_Intrfce_Stts", "#INTRFCE-STTS", 
            FieldType.STRING, 1);
        pnd_Intrfce_Super_2_Pnd_Rcrd_Crtd_For_Bsnss_Dte = pnd_Intrfce_Super_2__R_Field_1.newFieldInGroup("pnd_Intrfce_Super_2_Pnd_Rcrd_Crtd_For_Bsnss_Dte", 
            "#RCRD-CRTD-FOR-BSNSS-DTE", FieldType.STRING, 8);
        pnd_Intrfce_Super_2_Pnd_Pin_Tiaa_Cntrct = pnd_Intrfce_Super_2__R_Field_1.newFieldInGroup("pnd_Intrfce_Super_2_Pnd_Pin_Tiaa_Cntrct", "#PIN-TIAA-CNTRCT", 
            FieldType.STRING, 22);
        pnd_Intrfce_Super_2_Pnd_Tiaa_Cref_Ind = pnd_Intrfce_Super_2__R_Field_1.newFieldInGroup("pnd_Intrfce_Super_2_Pnd_Tiaa_Cref_Ind", "#TIAA-CREF-IND", 
            FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_bci.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Benn9812() throws Exception
    {
        super("Benn9812");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ERROR-INFO-SUB.##LAST-PROGRAM := *PROGRAM
        pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().reset();                                                                                                        //Natural: RESET MSG-INFO-SUB.##RETURN-CODE MSG-INFO-SUB.##MSG
        pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().reset();
        if (condition(pdaBena9812.getBena9812_Pnd_Cntrct_Isn().equals(getZero())))                                                                                        //Natural: IF BENA9812.#CNTRCT-ISN = 0
        {
                                                                                                                                                                          //Natural: PERFORM FIND-PROCESSING
            sub_Find_Processing();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            GBCI:                                                                                                                                                         //Natural: GET BCI #CNTRCT-ISN
            vw_bci.readByID(pdaBena9812.getBena9812_Pnd_Cntrct_Isn().getLong(), "GBCI");
                                                                                                                                                                          //Natural: PERFORM UPDATE-ERROR-TABLE
            sub_Update_Error_Table();
            if (condition(Global.isEscape())) {return;}
            vw_bci.updateDBRow("GBCI");                                                                                                                                   //Natural: UPDATE ( GBCI. )
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-PROCESSING
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUMP-DOWN-ERRORS
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-ERROR-TABLE
    }
    private void sub_Find_Processing() throws Exception                                                                                                                   //Natural: FIND-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Intrfce_Super_2_Pnd_Intrfce_Stts.setValue(pdaBena9812.getBena9812_Pnd_Intrfce_Stts());                                                                        //Natural: ASSIGN #INTRFCE-SUPER-2.#INTRFCE-STTS := BENA9812.#INTRFCE-STTS
        pnd_Intrfce_Super_2_Pnd_Rcrd_Crtd_For_Bsnss_Dte.setValue(pdaBena9812.getBena9812_Pnd_Rcrd_Crtd_For_Bsnss_Dte());                                                  //Natural: ASSIGN #INTRFCE-SUPER-2.#RCRD-CRTD-FOR-BSNSS-DTE := BENA9812.#RCRD-CRTD-FOR-BSNSS-DTE
        pnd_Intrfce_Super_2_Pnd_Pin_Tiaa_Cntrct.setValue(pdaBena9812.getBena9812_Pnd_Pin_Tiaa_Cntrct());                                                                  //Natural: ASSIGN #INTRFCE-SUPER-2.#PIN-TIAA-CNTRCT := BENA9812.#PIN-TIAA-CNTRCT
        pnd_Intrfce_Super_2_Pnd_Tiaa_Cref_Ind.setValue(pdaBena9812.getBena9812_Pnd_Tiaa_Cref_Ind());                                                                      //Natural: ASSIGN #INTRFCE-SUPER-2.#TIAA-CREF-IND := BENA9812.#TIAA-CREF-IND
        vw_bci.startDatabaseFind                                                                                                                                          //Natural: FIND ( 1 ) BCI WITH INTRFCE-SUPER-2 = #INTRFCE-SUPER-2
        (
        "FBCI",
        new Wc[] { new Wc("INTRFCE_SUPER_2", "=", pnd_Intrfce_Super_2, WcType.WITH) },
        1
        );
        FBCI:
        while (condition(vw_bci.readNextRow("FBCI", true)))
        {
            vw_bci.setIfNotFoundControlFlag(false);
            if (condition(vw_bci.getAstCOUNTER().equals(0)))                                                                                                              //Natural: IF NO RECORDS FOUND
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-NOREC
                                                                                                                                                                          //Natural: PERFORM UPDATE-ERROR-TABLE
            sub_Update_Error_Table();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FBCI"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FBCI"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            vw_bci.updateDBRow("FBCI");                                                                                                                                   //Natural: UPDATE ( FBCI. )
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  GET-RECORD
    }
    private void sub_Bump_Down_Errors() throws Exception                                                                                                                  //Natural: BUMP-DOWN-ERRORS
    {
        if (BLNatReinput.isReinput()) return;

        FOR01:                                                                                                                                                            //Natural: FOR #I = 2 TO 10
        for (pnd_I.setValue(2); condition(pnd_I.lessOrEqual(10)); pnd_I.nadd(1))
        {
            pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_I.subtract(1));                                                                                        //Natural: COMPUTE #J = #I - 1
            bci_Error_Txt.getValue(pnd_J).setValue(bci_Error_Txt.getValue(pnd_I));                                                                                        //Natural: ASSIGN ERROR-TXT ( #J ) := ERROR-TXT ( #I )
            bci_Error_Cde.getValue(pnd_J).setValue(bci_Error_Cde.getValue(pnd_I));                                                                                        //Natural: ASSIGN ERROR-CDE ( #J ) := ERROR-CDE ( #I )
            bci_Error_Dte.getValue(pnd_J).setValue(bci_Error_Dte.getValue(pnd_I));                                                                                        //Natural: ASSIGN ERROR-DTE ( #J ) := ERROR-DTE ( #I )
            bci_Error_Tme.getValue(pnd_J).setValue(bci_Error_Tme.getValue(pnd_I));                                                                                        //Natural: ASSIGN ERROR-TME ( #J ) := ERROR-TME ( #I )
            bci_Error_Pgm.getValue(pnd_J).setValue(bci_Error_Pgm.getValue(pnd_I));                                                                                        //Natural: ASSIGN ERROR-PGM ( #J ) := ERROR-PGM ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        bci_Error_Txt.getValue(10).reset();                                                                                                                               //Natural: RESET ERROR-TXT ( 10 ) ERROR-CDE ( 10 ) ERROR-DTE ( 10 ) ERROR-TME ( 10 ) ERROR-PGM ( 10 )
        bci_Error_Cde.getValue(10).reset();
        bci_Error_Dte.getValue(10).reset();
        bci_Error_Tme.getValue(10).reset();
        bci_Error_Pgm.getValue(10).reset();
        pnd_Next_Bucket.setValue(10);                                                                                                                                     //Natural: ASSIGN #NEXT-BUCKET := 10
        //*  BUMP-DOWN-ERRORS
    }
    private void sub_Update_Error_Table() throws Exception                                                                                                                //Natural: UPDATE-ERROR-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Next_Bucket.compute(new ComputeParameters(false, pnd_Next_Bucket), bci_Count_Casterror_Table.add(1));                                                         //Natural: COMPUTE #NEXT-BUCKET = C*ERROR-TABLE + 1
        if (condition(pnd_Next_Bucket.greater(10)))                                                                                                                       //Natural: IF #NEXT-BUCKET GT 10
        {
                                                                                                                                                                          //Natural: PERFORM BUMP-DOWN-ERRORS
            sub_Bump_Down_Errors();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        bci_Error_Txt.getValue(pnd_Next_Bucket).setValue(pdaBena9812.getBena9812_Pnd_Error_Txt());                                                                        //Natural: ASSIGN BCI.ERROR-TXT ( #NEXT-BUCKET ) := BENA9812.#ERROR-TXT
        bci_Error_Cde.getValue(pnd_Next_Bucket).setValue(pdaBena9812.getBena9812_Pnd_Error_Cde());                                                                        //Natural: ASSIGN BCI.ERROR-CDE ( #NEXT-BUCKET ) := BENA9812.#ERROR-CDE
        bci_Error_Pgm.getValue(pnd_Next_Bucket).setValue(pdaBena9812.getBena9812_Pnd_Error_Pgm());                                                                        //Natural: ASSIGN BCI.ERROR-PGM ( #NEXT-BUCKET ) := BENA9812.#ERROR-PGM
        bci_Error_Dte.getValue(pnd_Next_Bucket).setValue(pdaBena9812.getBena9812_Pnd_Bsnss_Dte());                                                                        //Natural: ASSIGN BCI.ERROR-DTE ( #NEXT-BUCKET ) := BENA9812.#BSNSS-DTE
        bci_Rcrd_Last_Updt_Userid.setValue(Global.getPROGRAM());                                                                                                          //Natural: ASSIGN BCI.RCRD-LAST-UPDT-USERID := *PROGRAM
        bci_Error_Tme.getValue(pnd_Next_Bucket).setValueEdited(Global.getTIMX(),new ReportEditMask("HHIISST"));                                                           //Natural: MOVE EDITED *TIMX ( EM = HHIISST ) TO BCI.ERROR-TME ( #NEXT-BUCKET )
        bci_Rcrd_Last_Updt_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("HHIISST"));                                                                            //Natural: MOVE EDITED *TIMX ( EM = HHIISST ) TO BCI.RCRD-LAST-UPDT-TME
        bci_Rcrd_Last_Updt_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                           //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO BCI.RCRD-LAST-UPDT-DTE
        //*  UPDATE-ERROR-TABLE
    }

    //
}
