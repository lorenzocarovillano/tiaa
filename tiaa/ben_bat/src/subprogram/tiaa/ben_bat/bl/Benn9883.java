/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:07:39 PM
**        * FROM NATURAL SUBPROGRAM : Benn9883
************************************************************
**        * FILE NAME            : Benn9883.java
**        * CLASS NAME           : Benn9883
**        * INSTANCE NAME        : Benn9883
************************************************************
************************************************************************
* PROGRAM  : BENN9883
* SYSTEM   : BENEFICIARY-SYSTEM (INTERFACE BATCH)
* TITLE    : BENE AUDIT TRAIL FOR ERROR
* WRITTEN  : APRIL 27TH 2005
*      BY  : H.KAKADIA
************************************************************************
*    DATE      USERID                   DESCRIPTION
* MM/DD/YYYY  XXXXXXXX  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
* 04/01/2017  DURAND    PIN EXPANSION N7 TO N12
************************************************************************

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benn9883 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaBena9801 pdaBena9801;
    private PdaBena5220 pdaBena5220;
    private PdaBena9812 pdaBena9812;
    private PdaBena9832 pdaBena9832;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_P pdaCwfpda_P;
    private PdaEfsa9120 pdaEfsa9120;
    private PdaEfsa902r pdaEfsa902r;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_bdi;
    private DbsField bdi_Intrfce_Stts;
    private DbsField bdi_Tiaa_Cref_Ind;
    private DbsField bdi_Seq_Nmbr;
    private DbsField bdi_Rcrd_Crtd_For_Bsnss_Dte;
    private DbsField bdi_Rcrd_Last_Updt_Dte;
    private DbsField bdi_Rcrd_Last_Updt_Tme;
    private DbsField bdi_Rcrd_Last_Updt_Userid;
    private DbsField bdi_Pin_Tiaa_Cntrct;

    private DbsGroup bdi__R_Field_1;
    private DbsField bdi_Pin;
    private DbsField bdi_Tiaa_Cntrct;
    private DbsField bdi_Bene_Type;
    private DbsField bdi_Rltn_Cde;
    private DbsField bdi_Bene_Name1;
    private DbsField bdi_Bene_Name2;
    private DbsField bdi_Rltn_Free_Txt;
    private DbsField bdi_Dte_Birth_Trust;
    private DbsField bdi_Ssn_Cde;
    private DbsField bdi_Ssn;
    private DbsField bdi_Prcnt_Frctn_Ind;
    private DbsField bdi_Share_Prcnt;
    private DbsField bdi_Share_Nmrtr;
    private DbsField bdi_Share_Dnmntr;
    private DbsField bdi_Irrvcble_Ind;
    private DbsField bdi_Sttlmnt_Rstrctn;
    private DbsField bdi_Spcl_Txt1;
    private DbsField bdi_Spcl_Txt2;
    private DbsField bdi_Spcl_Txt3;
    private DbsField bdi_Dflt_Estate;
    private DbsField bdi_Mdo_Calc_Bene;
    private DbsField bdi_Bene_Addr1;
    private DbsField bdi_Bene_Addr2;
    private DbsField bdi_Bene_Addr3_City;
    private DbsField bdi_Bene_State;
    private DbsField bdi_Bene_Zip;
    private DbsField bdi_Bene_Phone;
    private DbsField bdi_Bene_Gender;
    private DbsField bdi_Bene_Country;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_Heading;
    private DbsField pnd_Doc_Text_Sw;

    private DbsGroup pnd_Desig_Tab;
    private DbsField pnd_Desig_Tab_Pnd_Bene_Type;
    private DbsField pnd_Desig_Tab_Pnd_Rltn_Cde;
    private DbsField pnd_Desig_Tab_Pnd_Bene_Name1;
    private DbsField pnd_Desig_Tab_Pnd_Bene_Name2;
    private DbsField pnd_Desig_Tab_Pnd_Rltn_Free_Txt;
    private DbsField pnd_Desig_Tab_Pnd_Dte_Birth_Trust;

    private DbsGroup pnd_Desig_Tab__R_Field_2;
    private DbsField pnd_Desig_Tab_Pnd_Dte_Birth_Trust_N;
    private DbsField pnd_Desig_Tab_Pnd_Ssn_Cde;
    private DbsField pnd_Desig_Tab_Pnd_Ssn;

    private DbsGroup pnd_Desig_Tab__R_Field_3;
    private DbsField pnd_Desig_Tab_Pnd_Ssn_N;
    private DbsField pnd_Desig_Tab_Pnd_Prcnt_Frctn_Ind;
    private DbsField pnd_Desig_Tab_Pnd_Share_Prcnt;
    private DbsField pnd_Desig_Tab_Pnd_Share_Nmrtr;
    private DbsField pnd_Desig_Tab_Pnd_Share_Dnmntr;
    private DbsField pnd_Desig_Tab_Pnd_Irrvcbl_Ind;
    private DbsField pnd_Desig_Tab_Pnd_Sttlmnt_Rstrctn;
    private DbsField pnd_Desig_Tab_Pnd_Spcl_Txt1;
    private DbsField pnd_Desig_Tab_Pnd_Spcl_Txt2;
    private DbsField pnd_Desig_Tab_Pnd_Spcl_Txt3;
    private DbsField pnd_Desig_Tab_Pnd_Dflt_Estate;
    private DbsField pnd_Im_Ind;
    private DbsField pnd_Io_Ind;
    private DbsField pnd_N;
    private DbsField pnd_J1;
    private DbsField pnd_K1;
    private DbsField pnd_Intrfce_Super_2;

    private DbsGroup pnd_Intrfce_Super_2__R_Field_4;
    private DbsField pnd_Intrfce_Super_2_Pnd_Intrfce_Stts;
    private DbsField pnd_Intrfce_Super_2_Pnd_Rcrd_Crtd_For_Bsnss_Dte;
    private DbsField pnd_Intrfce_Super_2_Pnd_Pin_Tiaa_Cntrct;
    private DbsField pnd_Intrfce_Super_2_Pnd_Tiaa_Cref_Ind;
    private DbsField pnd_Intrfce_Super_4;

    private DbsGroup pnd_Intrfce_Super_4__R_Field_5;
    private DbsField pnd_Intrfce_Super_4_Pnd_Intrfce_Stts;
    private DbsField pnd_Intrfce_Super_4_Pnd_Rcrd_Crtd_For_Bsnss_Dte;
    private DbsField pnd_Intrfce_Super_4_Pnd_Pin_Tiaa_Cntrct;
    private DbsField pnd_Intrfce_Super_4_Pnd_Tiaa_Cref_Ind;
    private DbsField pnd_Intrfce_Super_4_Pnd_Seq_Nmbr;
    private DbsField pnd_Y_N_Tab;

    private DbsGroup pnd_Y_N_Tab__R_Field_6;
    private DbsField pnd_Y_N_Tab_Pnd_Y_N_Or_Blank;
    private DbsField pnd_Cntrct_Type_Values_Tab;

    private DbsGroup pnd_Cntrct_Type_Values_Tab__R_Field_7;
    private DbsField pnd_Cntrct_Type_Values_Tab_Pnd_Cntrct_Type_Values;
    private DbsField pnd_Pymnt_Child_Dcsd_Tab;

    private DbsGroup pnd_Pymnt_Child_Dcsd_Tab__R_Field_8;
    private DbsField pnd_Pymnt_Child_Dcsd_Tab_Pnd_Pymnt_Child_Dcsd_Values;
    private DbsField pnd_Num_Benes_P;
    private DbsField pnd_Num_Benes_C;
    private DbsField pnd_Error_Cnt;
    private DbsField pnd_Error_Lit;

    private DbsGroup pnd_Confirm_Calc_Tots;
    private DbsField pnd_Confirm_Calc_Tots_Pnd_Res;

    private DbsGroup pnd_Confirm_Calc_Tots__R_Field_9;
    private DbsField pnd_Confirm_Calc_Tots_Pnd_Res_26;
    private DbsField pnd_Confirm_Calc_Tots_Pnd_Res_Decimal;
    private DbsField pnd_Confirm_Calc_Tots_Pnd_P_Perc_Tot;
    private DbsField pnd_Confirm_Calc_Tots_Pnd_C_Perc_Tot;
    private DbsField pnd_Confirm_Calc_Tots_Pnd_P_Ntor_Tot;
    private DbsField pnd_Confirm_Calc_Tots_Pnd_P_Dtor_Tot;
    private DbsField pnd_Confirm_Calc_Tots_Pnd_C_Ntor_Tot;
    private DbsField pnd_Confirm_Calc_Tots_Pnd_C_Dtor_Tot;
    private DbsField pnd_Confirm_Calc_Tots_Pnd_Hold_Ntor_Tot_1;
    private DbsField pnd_Confirm_Calc_Tots_Pnd_Hold_Ntor_Tot_2;
    private DbsField pnd_Confirm_Calc_Tots_Pnd_Comp_Lit;
    private DbsField pnd_1st_C_Bene;
    private DbsField pnd_No_C_Benes_Yet;
    private DbsField pnd_Num_Poss_Mos_Lines;
    private DbsField pnd_Xr_Rel_Code_Found;
    private DbsField pnd_Ss_Code_Found;
    private DbsField pnd_Sr_Code_Found;
    private DbsField pnd_T_I;
    private DbsField pnd_Same_As;

    private DbsGroup pnd_Same_As__R_Field_10;
    private DbsField pnd_Same_As_Pnd_Same_As_Txt;

    private DbsGroup pnd_Same_As__R_Field_11;
    private DbsField pnd_Same_As_Pnd_See_Txt;
    private DbsField pnd_Test_Lit;

    private DbsGroup pnd_Test_Lit__R_Field_12;
    private DbsField pnd_Test_Lit_Pnd_Test_1st_Byte;
    private DbsField pnd_Test_Lit_Pnd_Test_Rest_Byte;
    private DbsField pnd_Spcl_Txt_Indexed;
    private DbsField pnd_St;
    private DbsField pnd_Sep_Tab;
    private DbsField pnd_Name_Only_This_Bene;
    private DbsField pnd_1st_Primary_Name_Only;
    private DbsField pnd_1st_C_Bene_Name_Only;
    private DbsField pnd_Inval_Non_Acis_Rel;
    private DbsField pnd_Same_Hold;
    private DbsField pnd_1stc;
    private DbsField pnd_Doc_Txt;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaBena9832 = new PdaBena9832(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        pdaEfsa9120 = new PdaEfsa9120(localVariables);
        pdaEfsa902r = new PdaEfsa902r(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaBena9801 = new PdaBena9801(parameters);
        pdaBena5220 = new PdaBena5220(parameters);
        pdaBena9812 = new PdaBena9812(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        vw_bdi = new DataAccessProgramView(new NameInfo("vw_bdi", "BDI"), "BENE_DESIGNATION_INTERFACE_12", "BENE_DSGN_INTFCE");
        bdi_Intrfce_Stts = vw_bdi.getRecord().newFieldInGroup("bdi_Intrfce_Stts", "INTRFCE-STTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, "INTRFCE_STTS");
        bdi_Intrfce_Stts.setDdmHeader("INTRFCE/STATUS");
        bdi_Tiaa_Cref_Ind = vw_bdi.getRecord().newFieldInGroup("bdi_Tiaa_Cref_Ind", "TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIAA_CREF_IND");
        bdi_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bdi_Seq_Nmbr = vw_bdi.getRecord().newFieldInGroup("bdi_Seq_Nmbr", "SEQ-NMBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "SEQ_NMBR");
        bdi_Seq_Nmbr.setDdmHeader("SEQ/NMBR");
        bdi_Rcrd_Crtd_For_Bsnss_Dte = vw_bdi.getRecord().newFieldInGroup("bdi_Rcrd_Crtd_For_Bsnss_Dte", "RCRD-CRTD-FOR-BSNSS-DTE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "RCRD_CRTD_FOR_BSNSS_DTE");
        bdi_Rcrd_Crtd_For_Bsnss_Dte.setDdmHeader("RCRD CRTD/FOR/BSNSS DTE");
        bdi_Rcrd_Last_Updt_Dte = vw_bdi.getRecord().newFieldInGroup("bdi_Rcrd_Last_Updt_Dte", "RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_DTE");
        bdi_Rcrd_Last_Updt_Dte.setDdmHeader("RECORD/LAST UPDT/DATE");
        bdi_Rcrd_Last_Updt_Tme = vw_bdi.getRecord().newFieldInGroup("bdi_Rcrd_Last_Updt_Tme", "RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_TME");
        bdi_Rcrd_Last_Updt_Tme.setDdmHeader("RECORD/LAST UPDT/TIME");
        bdi_Rcrd_Last_Updt_Userid = vw_bdi.getRecord().newFieldInGroup("bdi_Rcrd_Last_Updt_Userid", "RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_USERID");
        bdi_Rcrd_Last_Updt_Userid.setDdmHeader("RECORD/LAST UPDT/USER ID");
        bdi_Pin_Tiaa_Cntrct = vw_bdi.getRecord().newFieldInGroup("bdi_Pin_Tiaa_Cntrct", "PIN-TIAA-CNTRCT", FieldType.STRING, 22, RepeatingFieldStrategy.None, 
            "PIN_TIAA_CNTRCT");
        bdi_Pin_Tiaa_Cntrct.setDdmHeader("PIN/TIAA/CONTRACT");

        bdi__R_Field_1 = vw_bdi.getRecord().newGroupInGroup("bdi__R_Field_1", "REDEFINE", bdi_Pin_Tiaa_Cntrct);
        bdi_Pin = bdi__R_Field_1.newFieldInGroup("bdi_Pin", "PIN", FieldType.NUMERIC, 12);
        bdi_Tiaa_Cntrct = bdi__R_Field_1.newFieldInGroup("bdi_Tiaa_Cntrct", "TIAA-CNTRCT", FieldType.STRING, 10);
        bdi_Bene_Type = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Type", "BENE-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BENE_TYPE");
        bdi_Bene_Type.setDdmHeader("BENE/TYPE");
        bdi_Rltn_Cde = vw_bdi.getRecord().newFieldInGroup("bdi_Rltn_Cde", "RLTN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "RLTN_CDE");
        bdi_Rltn_Cde.setDdmHeader("RELATION/CODE");
        bdi_Bene_Name1 = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Name1", "BENE-NAME1", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BENE_NAME1");
        bdi_Bene_Name1.setDdmHeader("BENEFICIARY/NAME 1");
        bdi_Bene_Name2 = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Name2", "BENE-NAME2", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BENE_NAME2");
        bdi_Bene_Name2.setDdmHeader("BENEFICIARY/NAME 2");
        bdi_Rltn_Free_Txt = vw_bdi.getRecord().newFieldInGroup("bdi_Rltn_Free_Txt", "RLTN-FREE-TXT", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "RLTN_FREE_TXT");
        bdi_Rltn_Free_Txt.setDdmHeader("OTHER/RELATION");
        bdi_Dte_Birth_Trust = vw_bdi.getRecord().newFieldInGroup("bdi_Dte_Birth_Trust", "DTE-BIRTH-TRUST", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "DTE_BIRTH_TRUST");
        bdi_Dte_Birth_Trust.setDdmHeader("DOB/DOT");
        bdi_Ssn_Cde = vw_bdi.getRecord().newFieldInGroup("bdi_Ssn_Cde", "SSN-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "SSN_CDE");
        bdi_Ssn_Cde.setDdmHeader("SSN/CODE");
        bdi_Ssn = vw_bdi.getRecord().newFieldInGroup("bdi_Ssn", "SSN", FieldType.STRING, 9, RepeatingFieldStrategy.None, "SSN");
        bdi_Ssn.setDdmHeader("SSN");
        bdi_Prcnt_Frctn_Ind = vw_bdi.getRecord().newFieldInGroup("bdi_Prcnt_Frctn_Ind", "PRCNT-FRCTN-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PRCNT_FRCTN_IND");
        bdi_Prcnt_Frctn_Ind.setDdmHeader("PRCNT/FRCTN/IND");
        bdi_Share_Prcnt = vw_bdi.getRecord().newFieldInGroup("bdi_Share_Prcnt", "SHARE-PRCNT", FieldType.NUMERIC, 5, 2, RepeatingFieldStrategy.None, "SHARE_PRCNT");
        bdi_Share_Prcnt.setDdmHeader("SHARE/PERCENT");
        bdi_Share_Nmrtr = vw_bdi.getRecord().newFieldInGroup("bdi_Share_Nmrtr", "SHARE-NMRTR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "SHARE_NMRTR");
        bdi_Share_Nmrtr.setDdmHeader("SHARE/NUMERATOR");
        bdi_Share_Dnmntr = vw_bdi.getRecord().newFieldInGroup("bdi_Share_Dnmntr", "SHARE-DNMNTR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "SHARE_DNMNTR");
        bdi_Share_Dnmntr.setDdmHeader("SHARE/DENOMINATOR");
        bdi_Irrvcble_Ind = vw_bdi.getRecord().newFieldInGroup("bdi_Irrvcble_Ind", "IRRVCBLE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "IRRVCBLE_IND");
        bdi_Irrvcble_Ind.setDdmHeader("IRVCBL/IND");
        bdi_Sttlmnt_Rstrctn = vw_bdi.getRecord().newFieldInGroup("bdi_Sttlmnt_Rstrctn", "STTLMNT-RSTRCTN", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "STTLMNT_RSTRCTN");
        bdi_Sttlmnt_Rstrctn.setDdmHeader("SETTLEMENT/RESTRICTION");
        bdi_Spcl_Txt1 = vw_bdi.getRecord().newFieldInGroup("bdi_Spcl_Txt1", "SPCL-TXT1", FieldType.STRING, 72, RepeatingFieldStrategy.None, "SPCL_TXT1");
        bdi_Spcl_Txt1.setDdmHeader("SPECIAL TEXT 1");
        bdi_Spcl_Txt2 = vw_bdi.getRecord().newFieldInGroup("bdi_Spcl_Txt2", "SPCL-TXT2", FieldType.STRING, 72, RepeatingFieldStrategy.None, "SPCL_TXT2");
        bdi_Spcl_Txt2.setDdmHeader("SPECIAL TEXT 2");
        bdi_Spcl_Txt3 = vw_bdi.getRecord().newFieldInGroup("bdi_Spcl_Txt3", "SPCL-TXT3", FieldType.STRING, 72, RepeatingFieldStrategy.None, "SPCL_TXT3");
        bdi_Spcl_Txt3.setDdmHeader("SPECIAL TEXT 3");
        bdi_Dflt_Estate = vw_bdi.getRecord().newFieldInGroup("bdi_Dflt_Estate", "DFLT-ESTATE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "DFLT_ESTATE");
        bdi_Dflt_Estate.setDdmHeader("DEFAULT/ESTATE");
        bdi_Mdo_Calc_Bene = vw_bdi.getRecord().newFieldInGroup("bdi_Mdo_Calc_Bene", "MDO-CALC-BENE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "MDO_CALC_BENE");
        bdi_Mdo_Calc_Bene.setDdmHeader("MDO/CALC/BENE");
        bdi_Bene_Addr1 = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Addr1", "BENE-ADDR1", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BENE_ADDR1");
        bdi_Bene_Addr2 = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Addr2", "BENE-ADDR2", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BENE_ADDR2");
        bdi_Bene_Addr3_City = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Addr3_City", "BENE-ADDR3-CITY", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "BENE_ADDR3_CITY");
        bdi_Bene_State = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_State", "BENE-STATE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "BENE_STATE");
        bdi_Bene_Zip = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Zip", "BENE-ZIP", FieldType.STRING, 10, RepeatingFieldStrategy.None, "BENE_ZIP");
        bdi_Bene_Phone = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Phone", "BENE-PHONE", FieldType.STRING, 20, RepeatingFieldStrategy.None, "BENE_PHONE");
        bdi_Bene_Gender = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Gender", "BENE-GENDER", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BENE_GENDER");
        bdi_Bene_Country = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Country", "BENE-COUNTRY", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BENE_COUNTRY");
        registerRecord(vw_bdi);

        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.PACKED_DECIMAL, 3);
        pnd_Heading = localVariables.newFieldInRecord("pnd_Heading", "#HEADING", FieldType.STRING, 80);
        pnd_Doc_Text_Sw = localVariables.newFieldInRecord("pnd_Doc_Text_Sw", "#DOC-TEXT-SW", FieldType.STRING, 1);

        pnd_Desig_Tab = localVariables.newGroupArrayInRecord("pnd_Desig_Tab", "#DESIG-TAB", new DbsArrayController(1, 30));
        pnd_Desig_Tab_Pnd_Bene_Type = pnd_Desig_Tab.newFieldInGroup("pnd_Desig_Tab_Pnd_Bene_Type", "#BENE-TYPE", FieldType.STRING, 1);
        pnd_Desig_Tab_Pnd_Rltn_Cde = pnd_Desig_Tab.newFieldInGroup("pnd_Desig_Tab_Pnd_Rltn_Cde", "#RLTN-CDE", FieldType.STRING, 2);
        pnd_Desig_Tab_Pnd_Bene_Name1 = pnd_Desig_Tab.newFieldInGroup("pnd_Desig_Tab_Pnd_Bene_Name1", "#BENE-NAME1", FieldType.STRING, 35);
        pnd_Desig_Tab_Pnd_Bene_Name2 = pnd_Desig_Tab.newFieldInGroup("pnd_Desig_Tab_Pnd_Bene_Name2", "#BENE-NAME2", FieldType.STRING, 35);
        pnd_Desig_Tab_Pnd_Rltn_Free_Txt = pnd_Desig_Tab.newFieldInGroup("pnd_Desig_Tab_Pnd_Rltn_Free_Txt", "#RLTN-FREE-TXT", FieldType.STRING, 15);
        pnd_Desig_Tab_Pnd_Dte_Birth_Trust = pnd_Desig_Tab.newFieldInGroup("pnd_Desig_Tab_Pnd_Dte_Birth_Trust", "#DTE-BIRTH-TRUST", FieldType.STRING, 8);

        pnd_Desig_Tab__R_Field_2 = pnd_Desig_Tab.newGroupInGroup("pnd_Desig_Tab__R_Field_2", "REDEFINE", pnd_Desig_Tab_Pnd_Dte_Birth_Trust);
        pnd_Desig_Tab_Pnd_Dte_Birth_Trust_N = pnd_Desig_Tab__R_Field_2.newFieldInGroup("pnd_Desig_Tab_Pnd_Dte_Birth_Trust_N", "#DTE-BIRTH-TRUST-N", FieldType.NUMERIC, 
            8);
        pnd_Desig_Tab_Pnd_Ssn_Cde = pnd_Desig_Tab.newFieldInGroup("pnd_Desig_Tab_Pnd_Ssn_Cde", "#SSN-CDE", FieldType.STRING, 1);
        pnd_Desig_Tab_Pnd_Ssn = pnd_Desig_Tab.newFieldInGroup("pnd_Desig_Tab_Pnd_Ssn", "#SSN", FieldType.STRING, 9);

        pnd_Desig_Tab__R_Field_3 = pnd_Desig_Tab.newGroupInGroup("pnd_Desig_Tab__R_Field_3", "REDEFINE", pnd_Desig_Tab_Pnd_Ssn);
        pnd_Desig_Tab_Pnd_Ssn_N = pnd_Desig_Tab__R_Field_3.newFieldInGroup("pnd_Desig_Tab_Pnd_Ssn_N", "#SSN-N", FieldType.NUMERIC, 9);
        pnd_Desig_Tab_Pnd_Prcnt_Frctn_Ind = pnd_Desig_Tab.newFieldInGroup("pnd_Desig_Tab_Pnd_Prcnt_Frctn_Ind", "#PRCNT-FRCTN-IND", FieldType.STRING, 1);
        pnd_Desig_Tab_Pnd_Share_Prcnt = pnd_Desig_Tab.newFieldInGroup("pnd_Desig_Tab_Pnd_Share_Prcnt", "#SHARE-PRCNT", FieldType.NUMERIC, 5, 2);
        pnd_Desig_Tab_Pnd_Share_Nmrtr = pnd_Desig_Tab.newFieldInGroup("pnd_Desig_Tab_Pnd_Share_Nmrtr", "#SHARE-NMRTR", FieldType.NUMERIC, 3);
        pnd_Desig_Tab_Pnd_Share_Dnmntr = pnd_Desig_Tab.newFieldInGroup("pnd_Desig_Tab_Pnd_Share_Dnmntr", "#SHARE-DNMNTR", FieldType.NUMERIC, 3);
        pnd_Desig_Tab_Pnd_Irrvcbl_Ind = pnd_Desig_Tab.newFieldInGroup("pnd_Desig_Tab_Pnd_Irrvcbl_Ind", "#IRRVCBL-IND", FieldType.STRING, 1);
        pnd_Desig_Tab_Pnd_Sttlmnt_Rstrctn = pnd_Desig_Tab.newFieldInGroup("pnd_Desig_Tab_Pnd_Sttlmnt_Rstrctn", "#STTLMNT-RSTRCTN", FieldType.STRING, 1);
        pnd_Desig_Tab_Pnd_Spcl_Txt1 = pnd_Desig_Tab.newFieldInGroup("pnd_Desig_Tab_Pnd_Spcl_Txt1", "#SPCL-TXT1", FieldType.STRING, 72);
        pnd_Desig_Tab_Pnd_Spcl_Txt2 = pnd_Desig_Tab.newFieldInGroup("pnd_Desig_Tab_Pnd_Spcl_Txt2", "#SPCL-TXT2", FieldType.STRING, 72);
        pnd_Desig_Tab_Pnd_Spcl_Txt3 = pnd_Desig_Tab.newFieldInGroup("pnd_Desig_Tab_Pnd_Spcl_Txt3", "#SPCL-TXT3", FieldType.STRING, 72);
        pnd_Desig_Tab_Pnd_Dflt_Estate = pnd_Desig_Tab.newFieldInGroup("pnd_Desig_Tab_Pnd_Dflt_Estate", "#DFLT-ESTATE", FieldType.STRING, 1);
        pnd_Im_Ind = localVariables.newFieldInRecord("pnd_Im_Ind", "#IM-IND", FieldType.STRING, 1);
        pnd_Io_Ind = localVariables.newFieldInRecord("pnd_Io_Ind", "#IO-IND", FieldType.STRING, 1);
        pnd_N = localVariables.newFieldInRecord("pnd_N", "#N", FieldType.PACKED_DECIMAL, 3);
        pnd_J1 = localVariables.newFieldInRecord("pnd_J1", "#J1", FieldType.PACKED_DECIMAL, 3);
        pnd_K1 = localVariables.newFieldInRecord("pnd_K1", "#K1", FieldType.PACKED_DECIMAL, 3);
        pnd_Intrfce_Super_2 = localVariables.newFieldInRecord("pnd_Intrfce_Super_2", "#INTRFCE-SUPER-2", FieldType.STRING, 32);

        pnd_Intrfce_Super_2__R_Field_4 = localVariables.newGroupInRecord("pnd_Intrfce_Super_2__R_Field_4", "REDEFINE", pnd_Intrfce_Super_2);
        pnd_Intrfce_Super_2_Pnd_Intrfce_Stts = pnd_Intrfce_Super_2__R_Field_4.newFieldInGroup("pnd_Intrfce_Super_2_Pnd_Intrfce_Stts", "#INTRFCE-STTS", 
            FieldType.STRING, 1);
        pnd_Intrfce_Super_2_Pnd_Rcrd_Crtd_For_Bsnss_Dte = pnd_Intrfce_Super_2__R_Field_4.newFieldInGroup("pnd_Intrfce_Super_2_Pnd_Rcrd_Crtd_For_Bsnss_Dte", 
            "#RCRD-CRTD-FOR-BSNSS-DTE", FieldType.STRING, 8);
        pnd_Intrfce_Super_2_Pnd_Pin_Tiaa_Cntrct = pnd_Intrfce_Super_2__R_Field_4.newFieldInGroup("pnd_Intrfce_Super_2_Pnd_Pin_Tiaa_Cntrct", "#PIN-TIAA-CNTRCT", 
            FieldType.STRING, 22);
        pnd_Intrfce_Super_2_Pnd_Tiaa_Cref_Ind = pnd_Intrfce_Super_2__R_Field_4.newFieldInGroup("pnd_Intrfce_Super_2_Pnd_Tiaa_Cref_Ind", "#TIAA-CREF-IND", 
            FieldType.STRING, 1);
        pnd_Intrfce_Super_4 = localVariables.newFieldInRecord("pnd_Intrfce_Super_4", "#INTRFCE-SUPER-4", FieldType.STRING, 35);

        pnd_Intrfce_Super_4__R_Field_5 = localVariables.newGroupInRecord("pnd_Intrfce_Super_4__R_Field_5", "REDEFINE", pnd_Intrfce_Super_4);
        pnd_Intrfce_Super_4_Pnd_Intrfce_Stts = pnd_Intrfce_Super_4__R_Field_5.newFieldInGroup("pnd_Intrfce_Super_4_Pnd_Intrfce_Stts", "#INTRFCE-STTS", 
            FieldType.STRING, 1);
        pnd_Intrfce_Super_4_Pnd_Rcrd_Crtd_For_Bsnss_Dte = pnd_Intrfce_Super_4__R_Field_5.newFieldInGroup("pnd_Intrfce_Super_4_Pnd_Rcrd_Crtd_For_Bsnss_Dte", 
            "#RCRD-CRTD-FOR-BSNSS-DTE", FieldType.STRING, 8);
        pnd_Intrfce_Super_4_Pnd_Pin_Tiaa_Cntrct = pnd_Intrfce_Super_4__R_Field_5.newFieldInGroup("pnd_Intrfce_Super_4_Pnd_Pin_Tiaa_Cntrct", "#PIN-TIAA-CNTRCT", 
            FieldType.STRING, 22);
        pnd_Intrfce_Super_4_Pnd_Tiaa_Cref_Ind = pnd_Intrfce_Super_4__R_Field_5.newFieldInGroup("pnd_Intrfce_Super_4_Pnd_Tiaa_Cref_Ind", "#TIAA-CREF-IND", 
            FieldType.STRING, 1);
        pnd_Intrfce_Super_4_Pnd_Seq_Nmbr = pnd_Intrfce_Super_4__R_Field_5.newFieldInGroup("pnd_Intrfce_Super_4_Pnd_Seq_Nmbr", "#SEQ-NMBR", FieldType.STRING, 
            3);
        pnd_Y_N_Tab = localVariables.newFieldInRecord("pnd_Y_N_Tab", "#Y-N-TAB", FieldType.STRING, 3);

        pnd_Y_N_Tab__R_Field_6 = localVariables.newGroupInRecord("pnd_Y_N_Tab__R_Field_6", "REDEFINE", pnd_Y_N_Tab);
        pnd_Y_N_Tab_Pnd_Y_N_Or_Blank = pnd_Y_N_Tab__R_Field_6.newFieldArrayInGroup("pnd_Y_N_Tab_Pnd_Y_N_Or_Blank", "#Y-N-OR-BLANK", FieldType.STRING, 
            1, new DbsArrayController(1, 3));
        pnd_Cntrct_Type_Values_Tab = localVariables.newFieldInRecord("pnd_Cntrct_Type_Values_Tab", "#CNTRCT-TYPE-VALUES-TAB", FieldType.STRING, 3);

        pnd_Cntrct_Type_Values_Tab__R_Field_7 = localVariables.newGroupInRecord("pnd_Cntrct_Type_Values_Tab__R_Field_7", "REDEFINE", pnd_Cntrct_Type_Values_Tab);
        pnd_Cntrct_Type_Values_Tab_Pnd_Cntrct_Type_Values = pnd_Cntrct_Type_Values_Tab__R_Field_7.newFieldArrayInGroup("pnd_Cntrct_Type_Values_Tab_Pnd_Cntrct_Type_Values", 
            "#CNTRCT-TYPE-VALUES", FieldType.STRING, 1, new DbsArrayController(1, 3));
        pnd_Pymnt_Child_Dcsd_Tab = localVariables.newFieldInRecord("pnd_Pymnt_Child_Dcsd_Tab", "#PYMNT-CHILD-DCSD-TAB", FieldType.STRING, 7);

        pnd_Pymnt_Child_Dcsd_Tab__R_Field_8 = localVariables.newGroupInRecord("pnd_Pymnt_Child_Dcsd_Tab__R_Field_8", "REDEFINE", pnd_Pymnt_Child_Dcsd_Tab);
        pnd_Pymnt_Child_Dcsd_Tab_Pnd_Pymnt_Child_Dcsd_Values = pnd_Pymnt_Child_Dcsd_Tab__R_Field_8.newFieldArrayInGroup("pnd_Pymnt_Child_Dcsd_Tab_Pnd_Pymnt_Child_Dcsd_Values", 
            "#PYMNT-CHILD-DCSD-VALUES", FieldType.STRING, 1, new DbsArrayController(1, 7));
        pnd_Num_Benes_P = localVariables.newFieldInRecord("pnd_Num_Benes_P", "#NUM-BENES-P", FieldType.NUMERIC, 2);
        pnd_Num_Benes_C = localVariables.newFieldInRecord("pnd_Num_Benes_C", "#NUM-BENES-C", FieldType.NUMERIC, 2);
        pnd_Error_Cnt = localVariables.newFieldInRecord("pnd_Error_Cnt", "#ERROR-CNT", FieldType.NUMERIC, 4);
        pnd_Error_Lit = localVariables.newFieldInRecord("pnd_Error_Lit", "#ERROR-LIT", FieldType.STRING, 10);

        pnd_Confirm_Calc_Tots = localVariables.newGroupInRecord("pnd_Confirm_Calc_Tots", "#CONFIRM-CALC-TOTS");
        pnd_Confirm_Calc_Tots_Pnd_Res = pnd_Confirm_Calc_Tots.newFieldInGroup("pnd_Confirm_Calc_Tots_Pnd_Res", "#RES", FieldType.NUMERIC, 29, 3);

        pnd_Confirm_Calc_Tots__R_Field_9 = pnd_Confirm_Calc_Tots.newGroupInGroup("pnd_Confirm_Calc_Tots__R_Field_9", "REDEFINE", pnd_Confirm_Calc_Tots_Pnd_Res);
        pnd_Confirm_Calc_Tots_Pnd_Res_26 = pnd_Confirm_Calc_Tots__R_Field_9.newFieldInGroup("pnd_Confirm_Calc_Tots_Pnd_Res_26", "#RES-26", FieldType.NUMERIC, 
            26);
        pnd_Confirm_Calc_Tots_Pnd_Res_Decimal = pnd_Confirm_Calc_Tots__R_Field_9.newFieldInGroup("pnd_Confirm_Calc_Tots_Pnd_Res_Decimal", "#RES-DECIMAL", 
            FieldType.NUMERIC, 3);
        pnd_Confirm_Calc_Tots_Pnd_P_Perc_Tot = pnd_Confirm_Calc_Tots.newFieldInGroup("pnd_Confirm_Calc_Tots_Pnd_P_Perc_Tot", "#P-PERC-TOT", FieldType.NUMERIC, 
            7, 2);
        pnd_Confirm_Calc_Tots_Pnd_C_Perc_Tot = pnd_Confirm_Calc_Tots.newFieldInGroup("pnd_Confirm_Calc_Tots_Pnd_C_Perc_Tot", "#C-PERC-TOT", FieldType.NUMERIC, 
            5, 2);
        pnd_Confirm_Calc_Tots_Pnd_P_Ntor_Tot = pnd_Confirm_Calc_Tots.newFieldInGroup("pnd_Confirm_Calc_Tots_Pnd_P_Ntor_Tot", "#P-NTOR-TOT", FieldType.PACKED_DECIMAL, 
            29);
        pnd_Confirm_Calc_Tots_Pnd_P_Dtor_Tot = pnd_Confirm_Calc_Tots.newFieldInGroup("pnd_Confirm_Calc_Tots_Pnd_P_Dtor_Tot", "#P-DTOR-TOT", FieldType.PACKED_DECIMAL, 
            29);
        pnd_Confirm_Calc_Tots_Pnd_C_Ntor_Tot = pnd_Confirm_Calc_Tots.newFieldInGroup("pnd_Confirm_Calc_Tots_Pnd_C_Ntor_Tot", "#C-NTOR-TOT", FieldType.PACKED_DECIMAL, 
            29);
        pnd_Confirm_Calc_Tots_Pnd_C_Dtor_Tot = pnd_Confirm_Calc_Tots.newFieldInGroup("pnd_Confirm_Calc_Tots_Pnd_C_Dtor_Tot", "#C-DTOR-TOT", FieldType.PACKED_DECIMAL, 
            29);
        pnd_Confirm_Calc_Tots_Pnd_Hold_Ntor_Tot_1 = pnd_Confirm_Calc_Tots.newFieldInGroup("pnd_Confirm_Calc_Tots_Pnd_Hold_Ntor_Tot_1", "#HOLD-NTOR-TOT-1", 
            FieldType.PACKED_DECIMAL, 29);
        pnd_Confirm_Calc_Tots_Pnd_Hold_Ntor_Tot_2 = pnd_Confirm_Calc_Tots.newFieldInGroup("pnd_Confirm_Calc_Tots_Pnd_Hold_Ntor_Tot_2", "#HOLD-NTOR-TOT-2", 
            FieldType.PACKED_DECIMAL, 29);
        pnd_Confirm_Calc_Tots_Pnd_Comp_Lit = pnd_Confirm_Calc_Tots.newFieldInGroup("pnd_Confirm_Calc_Tots_Pnd_Comp_Lit", "#COMP-LIT", FieldType.STRING, 
            10);
        pnd_1st_C_Bene = localVariables.newFieldInRecord("pnd_1st_C_Bene", "#1ST-C-BENE", FieldType.BOOLEAN, 1);
        pnd_No_C_Benes_Yet = localVariables.newFieldInRecord("pnd_No_C_Benes_Yet", "#NO-C-BENES-YET", FieldType.BOOLEAN, 1);
        pnd_Num_Poss_Mos_Lines = localVariables.newFieldInRecord("pnd_Num_Poss_Mos_Lines", "#NUM-POSS-MOS-LINES", FieldType.NUMERIC, 3);
        pnd_Xr_Rel_Code_Found = localVariables.newFieldInRecord("pnd_Xr_Rel_Code_Found", "#XR-REL-CODE-FOUND", FieldType.BOOLEAN, 1);
        pnd_Ss_Code_Found = localVariables.newFieldInRecord("pnd_Ss_Code_Found", "#SS-CODE-FOUND", FieldType.BOOLEAN, 1);
        pnd_Sr_Code_Found = localVariables.newFieldInRecord("pnd_Sr_Code_Found", "#SR-CODE-FOUND", FieldType.BOOLEAN, 1);
        pnd_T_I = localVariables.newFieldInRecord("pnd_T_I", "#T-I", FieldType.PACKED_DECIMAL, 3);
        pnd_Same_As = localVariables.newFieldInRecord("pnd_Same_As", "#SAME-AS", FieldType.STRING, 35);

        pnd_Same_As__R_Field_10 = localVariables.newGroupInRecord("pnd_Same_As__R_Field_10", "REDEFINE", pnd_Same_As);
        pnd_Same_As_Pnd_Same_As_Txt = pnd_Same_As__R_Field_10.newFieldInGroup("pnd_Same_As_Pnd_Same_As_Txt", "#SAME-AS-TXT", FieldType.STRING, 8);

        pnd_Same_As__R_Field_11 = localVariables.newGroupInRecord("pnd_Same_As__R_Field_11", "REDEFINE", pnd_Same_As);
        pnd_Same_As_Pnd_See_Txt = pnd_Same_As__R_Field_11.newFieldInGroup("pnd_Same_As_Pnd_See_Txt", "#SEE-TXT", FieldType.STRING, 4);
        pnd_Test_Lit = localVariables.newFieldInRecord("pnd_Test_Lit", "#TEST-LIT", FieldType.STRING, 72);

        pnd_Test_Lit__R_Field_12 = localVariables.newGroupInRecord("pnd_Test_Lit__R_Field_12", "REDEFINE", pnd_Test_Lit);
        pnd_Test_Lit_Pnd_Test_1st_Byte = pnd_Test_Lit__R_Field_12.newFieldInGroup("pnd_Test_Lit_Pnd_Test_1st_Byte", "#TEST-1ST-BYTE", FieldType.STRING, 
            1);
        pnd_Test_Lit_Pnd_Test_Rest_Byte = pnd_Test_Lit__R_Field_12.newFieldInGroup("pnd_Test_Lit_Pnd_Test_Rest_Byte", "#TEST-REST-BYTE", FieldType.STRING, 
            34);
        pnd_Spcl_Txt_Indexed = localVariables.newFieldArrayInRecord("pnd_Spcl_Txt_Indexed", "#SPCL-TXT-INDEXED", FieldType.STRING, 72, new DbsArrayController(1, 
            3));
        pnd_St = localVariables.newFieldInRecord("pnd_St", "#ST", FieldType.NUMERIC, 1);
        pnd_Sep_Tab = localVariables.newFieldArrayInRecord("pnd_Sep_Tab", "#SEP-TAB", FieldType.STRING, 72, new DbsArrayController(1, 72));
        pnd_Name_Only_This_Bene = localVariables.newFieldInRecord("pnd_Name_Only_This_Bene", "#NAME-ONLY-THIS-BENE", FieldType.BOOLEAN, 1);
        pnd_1st_Primary_Name_Only = localVariables.newFieldInRecord("pnd_1st_Primary_Name_Only", "#1ST-PRIMARY-NAME-ONLY", FieldType.BOOLEAN, 1);
        pnd_1st_C_Bene_Name_Only = localVariables.newFieldInRecord("pnd_1st_C_Bene_Name_Only", "#1ST-C-BENE-NAME-ONLY", FieldType.BOOLEAN, 1);
        pnd_Inval_Non_Acis_Rel = localVariables.newFieldInRecord("pnd_Inval_Non_Acis_Rel", "#INVAL-NON-ACIS-REL", FieldType.BOOLEAN, 1);
        pnd_Same_Hold = localVariables.newFieldInRecord("pnd_Same_Hold", "#SAME-HOLD", FieldType.STRING, 20);
        pnd_1stc = localVariables.newFieldInRecord("pnd_1stc", "#1STC", FieldType.NUMERIC, 2);
        pnd_Doc_Txt = localVariables.newFieldArrayInRecord("pnd_Doc_Txt", "#DOC-TXT", FieldType.STRING, 80, new DbsArrayController(1, 60));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_bdi.reset();

        localVariables.reset();
        pnd_Y_N_Tab.setInitialValue("YN ");
        pnd_Cntrct_Type_Values_Tab.setInitialValue("DIN");
        pnd_Pymnt_Child_Dcsd_Tab.setInitialValue("ABCDEF ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Benn9883() throws Exception
    {
        super("Benn9883");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().reset();                                                                                                        //Natural: RESET MSG-INFO-SUB.##RETURN-CODE MSG-INFO-SUB.##MSG
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().reset();
                                                                                                                                                                          //Natural: PERFORM BUILD-SUPER
        sub_Build_Super();
        if (condition(Global.isEscape())) {return;}
        pdaEfsa9120.getEfsa9120_Action().setValue("AD");                                                                                                                  //Natural: ASSIGN EFSA9120.ACTION := 'AD'
        pdaEfsa9120.getEfsa9120_Cabinet_Prefix().setValue("P");                                                                                                           //Natural: ASSIGN EFSA9120.CABINET-PREFIX := 'P'
        pdaEfsa9120.getEfsa9120_Pin_Nbr().setValue(pdaBena9801.getBena9801_Pin());                                                                                        //Natural: ASSIGN EFSA9120.PIN-NBR := BENA9801.PIN
        pdaEfsa9120.getEfsa9120_Rqst_Log_Dte_Tme().getValue(1).setValue(pdaBena5220.getBena5220_Pnd_Return_Log_Dte_Tme());                                                //Natural: ASSIGN EFSA9120.RQST-LOG-DTE-TME ( 1 ) := BENA5220.#RETURN-LOG-DTE-TME
        pdaEfsa9120.getEfsa9120_System().setValue("BEN");                                                                                                                 //Natural: ASSIGN EFSA9120.SYSTEM := 'BEN'
        pdaEfsa9120.getEfsa9120_Rqst_Origin_Unit_Cde().setValue(pdaBena5220.getBena5220_Pnd_Wpid_Unit());                                                                 //Natural: ASSIGN EFSA9120.RQST-ORIGIN-UNIT-CDE := BENA5220.#WPID-UNIT
        pdaEfsa9120.getEfsa9120_Doc_Direction().setValue("N");                                                                                                            //Natural: ASSIGN EFSA9120.DOC-DIRECTION := 'N'
        pdaEfsa9120.getEfsa9120_Doc_Format_Cde().setValue("T");                                                                                                           //Natural: ASSIGN EFSA9120.DOC-FORMAT-CDE := 'T'
        pdaEfsa9120.getEfsa9120_Doc_Retention_Cde().setValue("P");                                                                                                        //Natural: ASSIGN EFSA9120.DOC-RETENTION-CDE := 'P'
        pdaEfsa9120.getEfsa9120_Doc_Category().setValue("I");                                                                                                             //Natural: ASSIGN EFSA9120.DOC-CATEGORY := 'I'
        pdaEfsa9120.getEfsa9120_Doc_Class().setValue("AUD");                                                                                                              //Natural: ASSIGN EFSA9120.DOC-CLASS := 'AUD'
        pdaEfsa9120.getEfsa9120_Doc_Specific().setValue("BEMN");                                                                                                          //Natural: ASSIGN EFSA9120.DOC-SPECIFIC := 'BEMN'
        pdaEfsa9120.getEfsa9120_Rqst_Entry_Op_Cde().setValue(pdaBena5220.getBena5220_Pnd_User_Id());                                                                      //Natural: ASSIGN EFSA9120.RQST-ENTRY-OP-CDE := BENA5220.#USER-ID
                                                                                                                                                                          //Natural: PERFORM GET-DESIGNATION-RECORDS
        sub_Get_Designation_Records();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CALL-EFSN9120
        sub_Call_Efsn9120();
        if (condition(Global.isEscape())) {return;}
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUILD-SUPER
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-DESIGNATION-RECORDS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-EFSN9120
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-IF-LINES-60
    }
    private void sub_Build_Super() throws Exception                                                                                                                       //Natural: BUILD-SUPER
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pdaEfsa9120.getEfsa9120().reset();                                                                                                                                //Natural: RESET EFSA9120
        pnd_Intrfce_Super_2_Pnd_Intrfce_Stts.setValue(pdaBena9801.getBena9801_Intrfce_Stts());                                                                            //Natural: ASSIGN #INTRFCE-SUPER-2.#INTRFCE-STTS := BENA9801.INTRFCE-STTS
        pnd_Intrfce_Super_2_Pnd_Intrfce_Stts.setValue("A");                                                                                                               //Natural: ASSIGN #INTRFCE-SUPER-2.#INTRFCE-STTS := 'A'
        pnd_Intrfce_Super_2_Pnd_Rcrd_Crtd_For_Bsnss_Dte.setValue(pdaBena9801.getBena9801_Rcrd_Crtd_For_Bsnss_Dte());                                                      //Natural: ASSIGN #INTRFCE-SUPER-2.#RCRD-CRTD-FOR-BSNSS-DTE := BENA9801.RCRD-CRTD-FOR-BSNSS-DTE
        pnd_Intrfce_Super_2_Pnd_Pin_Tiaa_Cntrct.setValue(pdaBena9801.getBena9801_Pin_Tiaa_Cntrct());                                                                      //Natural: ASSIGN #INTRFCE-SUPER-2.#PIN-TIAA-CNTRCT := BENA9801.PIN-TIAA-CNTRCT
        pnd_Intrfce_Super_2_Pnd_Tiaa_Cref_Ind.setValue(pdaBena9801.getBena9801_Tiaa_Cref_Ind());                                                                          //Natural: ASSIGN #INTRFCE-SUPER-2.#TIAA-CREF-IND := BENA9801.TIAA-CREF-IND
        pnd_Intrfce_Super_4_Pnd_Intrfce_Stts.setValue(pdaBena9801.getBena9801_Intrfce_Stts());                                                                            //Natural: ASSIGN #INTRFCE-SUPER-4.#INTRFCE-STTS := BENA9801.INTRFCE-STTS
        pnd_Intrfce_Super_4_Pnd_Rcrd_Crtd_For_Bsnss_Dte.setValue(pdaBena9801.getBena9801_Rcrd_Crtd_For_Bsnss_Dte());                                                      //Natural: ASSIGN #INTRFCE-SUPER-4.#RCRD-CRTD-FOR-BSNSS-DTE := BENA9801.RCRD-CRTD-FOR-BSNSS-DTE
        pnd_Intrfce_Super_4_Pnd_Pin_Tiaa_Cntrct.setValue(pdaBena9801.getBena9801_Pin_Tiaa_Cntrct());                                                                      //Natural: ASSIGN #INTRFCE-SUPER-4.#PIN-TIAA-CNTRCT := BENA9801.PIN-TIAA-CNTRCT
        pnd_Intrfce_Super_4_Pnd_Tiaa_Cref_Ind.setValue(pdaBena9801.getBena9801_Tiaa_Cref_Ind());                                                                          //Natural: ASSIGN #INTRFCE-SUPER-4.#TIAA-CREF-IND := BENA9801.TIAA-CREF-IND
        //*  BUILD-SUPER
    }
    private void sub_Get_Designation_Records() throws Exception                                                                                                           //Natural: GET-DESIGNATION-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_I.reset();                                                                                                                                                    //Natural: RESET #I #DESIG-TAB ( * )
        pnd_Desig_Tab.getValue("*").reset();
        vw_bdi.startDatabaseRead                                                                                                                                          //Natural: READ BDI BY INTRFCE-SUPER-4 FROM #INTRFCE-SUPER-4
        (
        "RBDI",
        new Wc[] { new Wc("INTRFCE_SUPER_4", ">=", pnd_Intrfce_Super_4, WcType.BY) },
        new Oc[] { new Oc("INTRFCE_SUPER_4", "ASC") }
        );
        RBDI:
        while (condition(vw_bdi.readNextRow("RBDI")))
        {
            if (condition(bdi_Intrfce_Stts.notEquals(pdaBena9801.getBena9801_Intrfce_Stts()) || bdi_Rcrd_Crtd_For_Bsnss_Dte.notEquals(pdaBena9801.getBena9801_Rcrd_Crtd_For_Bsnss_Dte())  //Natural: IF BDI.INTRFCE-STTS NE BENA9801.INTRFCE-STTS OR BDI.RCRD-CRTD-FOR-BSNSS-DTE NE BENA9801.RCRD-CRTD-FOR-BSNSS-DTE OR BDI.PIN-TIAA-CNTRCT NE BENA9801.PIN-TIAA-CNTRCT OR BDI.TIAA-CREF-IND NE BENA9801.TIAA-CREF-IND
                || bdi_Pin_Tiaa_Cntrct.notEquals(pdaBena9801.getBena9801_Pin_Tiaa_Cntrct()) || bdi_Tiaa_Cref_Ind.notEquals(pdaBena9801.getBena9801_Tiaa_Cref_Ind())))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-LINES-60
            sub_Check_If_Lines_60();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RBDI"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RBDI"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(pnd_I).setValue("BENEFICIARY DESIGNATION INTERFACE ");                                                            //Natural: ASSIGN DOC-TEXT ( #I ) := 'BENEFICIARY DESIGNATION INTERFACE '
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-LINES-60
            sub_Check_If_Lines_60();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RBDI"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RBDI"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(pnd_I).setValue("Please update the following Corporate records");                                                 //Natural: ASSIGN DOC-TEXT ( #I ) := 'Please update the following Corporate records'
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-LINES-60
            sub_Check_If_Lines_60();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RBDI"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RBDI"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(pnd_I).setValue(DbsUtil.compress("ERROR TXT:", pdaBena9812.getBena9812_Pnd_Error_Txt()));                         //Natural: COMPRESS 'ERROR TXT:' #ERROR-TXT TO DOC-TEXT ( #I )
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-LINES-60
            sub_Check_If_Lines_60();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RBDI"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RBDI"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(pnd_I).setValue(DbsUtil.compress("ERROR-CDE:", pdaBena9812.getBena9812_Pnd_Error_Cde()));                         //Natural: COMPRESS 'ERROR-CDE:' #ERROR-CDE TO DOC-TEXT ( #I )
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-LINES-60
            sub_Check_If_Lines_60();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RBDI"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RBDI"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(pnd_I).setValue(DbsUtil.compress("ERROR PGM:", pdaBena9812.getBena9812_Pnd_Error_Pgm()));                         //Natural: COMPRESS 'ERROR PGM:' #ERROR-PGM TO DOC-TEXT ( #I )
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-LINES-60
            sub_Check_If_Lines_60();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RBDI"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RBDI"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(pnd_I).setValue(DbsUtil.compress("INTRFCE STS:", pdaBena9801.getBena9801_Intrfce_Stts()));                        //Natural: COMPRESS 'INTRFCE STS:' BENA9801.INTRFCE-STTS TO DOC-TEXT ( #I )
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-LINES-60
            sub_Check_If_Lines_60();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RBDI"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RBDI"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(pnd_I).setValue(DbsUtil.compress("RCRD CRTED BSNSS DTE:", pdaBena9801.getBena9801_Rcrd_Crtd_For_Bsnss_Dte()));    //Natural: COMPRESS 'RCRD CRTED BSNSS DTE:' BENA9801.RCRD-CRTD-FOR-BSNSS-DTE TO DOC-TEXT ( #I )
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-LINES-60
            sub_Check_If_Lines_60();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RBDI"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RBDI"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(pnd_I).setValue(DbsUtil.compress("INTRFCNG SYS:", pdaBena9801.getBena9801_Intrfcng_Systm()));                     //Natural: COMPRESS 'INTRFCNG SYS:' INTRFCNG-SYSTM TO DOC-TEXT ( #I )
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-LINES-60
            sub_Check_If_Lines_60();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RBDI"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RBDI"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(pnd_I).setValue(DbsUtil.compress("PIN        :", bdi_Pin));                                                       //Natural: COMPRESS 'PIN        :' BDI.PIN TO DOC-TEXT ( #I )
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-LINES-60
            sub_Check_If_Lines_60();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RBDI"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RBDI"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(pnd_I).setValue(DbsUtil.compress("TIAA CNTRCT:", bdi_Tiaa_Cntrct));                                               //Natural: COMPRESS 'TIAA CNTRCT:' BDI.TIAA-CNTRCT TO DOC-TEXT ( #I )
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-LINES-60
            sub_Check_If_Lines_60();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RBDI"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RBDI"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(pnd_I).setValue(DbsUtil.compress("BENE TYPE  :", bdi_Bene_Type));                                                 //Natural: COMPRESS 'BENE TYPE  :' BDI.BENE-TYPE TO DOC-TEXT ( #I )
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-LINES-60
            sub_Check_If_Lines_60();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RBDI"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RBDI"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(pnd_I).setValue(DbsUtil.compress("RLTN CDE   :", bdi_Rltn_Cde));                                                  //Natural: COMPRESS 'RLTN CDE   :' BDI.RLTN-CDE TO DOC-TEXT ( #I )
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-LINES-60
            sub_Check_If_Lines_60();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RBDI"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RBDI"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(pnd_I).setValue(DbsUtil.compress("BENE NAME1 :", bdi_Bene_Name1));                                                //Natural: COMPRESS 'BENE NAME1 :' BDI.BENE-NAME1 TO DOC-TEXT ( #I )
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-LINES-60
            sub_Check_If_Lines_60();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RBDI"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RBDI"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(pnd_I).setValue(DbsUtil.compress("BENE NAME2 :", bdi_Bene_Name2));                                                //Natural: COMPRESS 'BENE NAME2 :' BDI.BENE-NAME2 TO DOC-TEXT ( #I )
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-LINES-60
            sub_Check_If_Lines_60();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RBDI"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RBDI"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(pnd_I).setValue(DbsUtil.compress("RLTN FREE TEXT :", bdi_Rltn_Free_Txt));                                         //Natural: COMPRESS 'RLTN FREE TEXT :' BDI.RLTN-FREE-TXT TO DOC-TEXT ( #I )
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-LINES-60
            sub_Check_If_Lines_60();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RBDI"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RBDI"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(pnd_I).setValue(DbsUtil.compress("DTE BIRTH TRUST :", bdi_Dte_Birth_Trust));                                      //Natural: COMPRESS 'DTE BIRTH TRUST :' BDI.DTE-BIRTH-TRUST TO DOC-TEXT ( #I )
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-LINES-60
            sub_Check_If_Lines_60();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RBDI"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RBDI"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(pnd_I).setValue(DbsUtil.compress("SSN CDE :", bdi_Ssn_Cde));                                                      //Natural: COMPRESS 'SSN CDE :' BDI.SSN-CDE TO DOC-TEXT ( #I )
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-LINES-60
            sub_Check_If_Lines_60();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RBDI"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RBDI"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(pnd_I).setValue(DbsUtil.compress("SSN :", bdi_Ssn));                                                              //Natural: COMPRESS 'SSN :' BDI.SSN TO DOC-TEXT ( #I )
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-LINES-60
            sub_Check_If_Lines_60();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RBDI"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RBDI"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(pnd_I).setValue(DbsUtil.compress("PRCNT FRC IND :", bdi_Prcnt_Frctn_Ind));                                        //Natural: COMPRESS 'PRCNT FRC IND :' BDI.PRCNT-FRCTN-IND TO DOC-TEXT ( #I )
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-LINES-60
            sub_Check_If_Lines_60();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RBDI"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RBDI"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(pnd_I).setValue(DbsUtil.compress("SHARE PRCNT: ", bdi_Share_Prcnt));                                              //Natural: COMPRESS 'SHARE PRCNT: ' BDI.SHARE-PRCNT TO DOC-TEXT ( #I )
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-LINES-60
            sub_Check_If_Lines_60();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RBDI"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RBDI"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(pnd_I).setValue(DbsUtil.compress("SHARE NMRTR :", bdi_Share_Nmrtr));                                              //Natural: COMPRESS 'SHARE NMRTR :' BDI.SHARE-NMRTR TO DOC-TEXT ( #I )
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-LINES-60
            sub_Check_If_Lines_60();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RBDI"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RBDI"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(pnd_I).setValue(DbsUtil.compress("SHARE DNMNTR:", bdi_Share_Dnmntr));                                             //Natural: COMPRESS 'SHARE DNMNTR:' BDI.SHARE-DNMNTR TO DOC-TEXT ( #I )
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-LINES-60
            sub_Check_If_Lines_60();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RBDI"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RBDI"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(pnd_I).setValue(DbsUtil.compress("IRRVCBLE IND:", bdi_Irrvcble_Ind));                                             //Natural: COMPRESS 'IRRVCBLE IND:' BDI.IRRVCBLE-IND TO DOC-TEXT ( #I )
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-LINES-60
            sub_Check_If_Lines_60();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RBDI"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RBDI"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(pnd_I).setValue(DbsUtil.compress("STTLMNT RSTRCTN :", bdi_Sttlmnt_Rstrctn));                                      //Natural: COMPRESS 'STTLMNT RSTRCTN :' BDI.STTLMNT-RSTRCTN TO DOC-TEXT ( #I )
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-LINES-60
            sub_Check_If_Lines_60();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RBDI"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RBDI"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(pnd_I).setValue(DbsUtil.compress("SPCL TXT1 :", bdi_Spcl_Txt1));                                                  //Natural: COMPRESS 'SPCL TXT1 :' BDI.SPCL-TXT1 TO DOC-TEXT ( #I )
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-LINES-60
            sub_Check_If_Lines_60();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RBDI"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RBDI"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(pnd_I).setValue(DbsUtil.compress("SPCL TXT2:", bdi_Spcl_Txt2));                                                   //Natural: COMPRESS 'SPCL TXT2:' BDI.SPCL-TXT2 TO DOC-TEXT ( #I )
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-LINES-60
            sub_Check_If_Lines_60();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RBDI"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RBDI"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(pnd_I).setValue(DbsUtil.compress("SPCL TXT3:", bdi_Spcl_Txt3));                                                   //Natural: COMPRESS 'SPCL TXT3:' BDI.SPCL-TXT3 TO DOC-TEXT ( #I )
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-LINES-60
            sub_Check_If_Lines_60();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RBDI"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RBDI"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(pnd_I).setValue(DbsUtil.compress("DFLT ESTATE:", bdi_Dflt_Estate));                                               //Natural: COMPRESS 'DFLT ESTATE:' BDI.DFLT-ESTATE TO DOC-TEXT ( #I )
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-LINES-60
            sub_Check_If_Lines_60();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RBDI"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RBDI"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  GET-DESIGNATION-RECORDS
    }
    private void sub_Call_Efsn9120() throws Exception                                                                                                                     //Natural: CALL-EFSN9120
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        DbsUtil.callnat(Efsn9120.class , getCurrentProcessState(), pdaEfsa9120.getEfsa9120(), pdaEfsa9120.getEfsa9120_Id(), pdaEfsa902r.getEfsa902r(),                    //Natural: CALLNAT 'EFSN9120' EFSA9120 EFSA9120-ID EFSA902R CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
        if (condition(Global.isEscape())) return;
        pdaEfsa9120.getEfsa9120_Doc_Text().getValue("*").reset();                                                                                                         //Natural: RESET EFSA9120.DOC-TEXT ( * ) #DOC-TEXT-SW
        pnd_Doc_Text_Sw.reset();
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().notEquals(" ")))                                                                                          //Natural: IF MSG-INFO-SUB.##MSG NE ' '
        {
            pdaBena5220.getBena5220_Pnd_Error_Msg().setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                  //Natural: ASSIGN BENA5220.#ERROR-MSG := MSG-INFO-SUB.##MSG
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  CALL-EFSN9120
    }
    private void sub_Check_If_Lines_60() throws Exception                                                                                                                 //Natural: CHECK-IF-LINES-60
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_I.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #I
        if (condition(pnd_I.greater(60)))                                                                                                                                 //Natural: IF #I GT 60
        {
                                                                                                                                                                          //Natural: PERFORM CALL-EFSN9120
            sub_Call_Efsn9120();
            if (condition(Global.isEscape())) {return;}
            pnd_I.reset();                                                                                                                                                //Natural: RESET #I DOC-TEXT ( * )
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue("*").reset();
            pnd_I.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #I
        }                                                                                                                                                                 //Natural: END-IF
        //*  CHECK-IF-LINES-60
    }

    //
}
