/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:06:42 PM
**        * FROM NATURAL SUBPROGRAM : Benn9866
************************************************************
**        * FILE NAME            : Benn9866.java
**        * CLASS NAME           : Benn9866
**        * INSTANCE NAME        : Benn9866
************************************************************
************************************************************************
* PROGRAM  : BENN9866 - CLONED FROM BENN9821
* SYSTEM   : BENEFICIARY-SYSTEM     /* INTERFACE SAME AS
* TITLE    : INTERFACE SPECIFIC EDITS
* WRITTEN  : OCTOBER, 18, 2001
************************************************************************
*                         FONTOUR
************************************************************************
*    DATE      USERID                   DESCRIPTION
* MM/DD/YYYY  XXXXXXXX  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
* 04/01/2017  DURAND    PIN EXPANSION N7 TO N12
* 05/16/2000  WEBBJ     CHANGE CHECK ON BENE ON-LINE FILES FOR
*                       TIAA-CREF IND
* 10/04/2000  WEBBJ     CODE FOR CORR CORRECTIONS TO EXISTING CONTRACT
************************************************************************

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benn9866 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaBena9860 pdaBena9860;
    private PdaBena9866 pdaBena9866;
    private PdaBena9873 pdaBena9873;
    private PdaBenpda_M pdaBenpda_M;
    private PdaBenpda_E pdaBenpda_E;
    private PdaBena9874 pdaBena9874;
    private PdaBena9876 pdaBena9876;
    private PdaBena9875 pdaBena9875;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_C_Last_Srce;
    private DbsField pnd_M_Last_Srce;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Intrfce_Super_2;

    private DbsGroup pnd_Intrfce_Super_2__R_Field_1;
    private DbsField pnd_Intrfce_Super_2_Pnd_Intrfce_Stts;
    private DbsField pnd_Intrfce_Super_2_Pnd_Rcrd_Crtd_For_Bsnss_Dte;
    private DbsField pnd_Intrfce_Super_2_Pnd_Pin_Tiaa_Cntrct;
    private DbsField pnd_Intrfce_Super_2_Pnd_Tiaa_Cref_Ind;
    private DbsField pnd_Bene_Rcrd_Stts;

    private DbsGroup pnd_Bene_Rcrd_Stts__R_Field_2;
    private DbsField pnd_Bene_Rcrd_Stts_Pnd_Bene_Rcrd_Stts_A1;
    private DbsField pnd_Stts_Found;
    private DbsField pnd_Other_Check_T_C_Ind;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaBena9874 = new PdaBena9874(localVariables);
        pdaBena9876 = new PdaBena9876(localVariables);
        pdaBena9875 = new PdaBena9875(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaBena9860 = new PdaBena9860(parameters);
        pdaBena9866 = new PdaBena9866(parameters);
        pdaBena9873 = new PdaBena9873(parameters);
        pdaBenpda_M = new PdaBenpda_M(parameters);
        pdaBenpda_E = new PdaBenpda_E(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_C_Last_Srce = localVariables.newFieldInRecord("pnd_C_Last_Srce", "#C-LAST-SRCE", FieldType.STRING, 1);
        pnd_M_Last_Srce = localVariables.newFieldInRecord("pnd_M_Last_Srce", "#M-LAST-SRCE", FieldType.STRING, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_Intrfce_Super_2 = localVariables.newFieldInRecord("pnd_Intrfce_Super_2", "#INTRFCE-SUPER-2", FieldType.STRING, 32);

        pnd_Intrfce_Super_2__R_Field_1 = localVariables.newGroupInRecord("pnd_Intrfce_Super_2__R_Field_1", "REDEFINE", pnd_Intrfce_Super_2);
        pnd_Intrfce_Super_2_Pnd_Intrfce_Stts = pnd_Intrfce_Super_2__R_Field_1.newFieldInGroup("pnd_Intrfce_Super_2_Pnd_Intrfce_Stts", "#INTRFCE-STTS", 
            FieldType.STRING, 1);
        pnd_Intrfce_Super_2_Pnd_Rcrd_Crtd_For_Bsnss_Dte = pnd_Intrfce_Super_2__R_Field_1.newFieldInGroup("pnd_Intrfce_Super_2_Pnd_Rcrd_Crtd_For_Bsnss_Dte", 
            "#RCRD-CRTD-FOR-BSNSS-DTE", FieldType.STRING, 8);
        pnd_Intrfce_Super_2_Pnd_Pin_Tiaa_Cntrct = pnd_Intrfce_Super_2__R_Field_1.newFieldInGroup("pnd_Intrfce_Super_2_Pnd_Pin_Tiaa_Cntrct", "#PIN-TIAA-CNTRCT", 
            FieldType.STRING, 22);
        pnd_Intrfce_Super_2_Pnd_Tiaa_Cref_Ind = pnd_Intrfce_Super_2__R_Field_1.newFieldInGroup("pnd_Intrfce_Super_2_Pnd_Tiaa_Cref_Ind", "#TIAA-CREF-IND", 
            FieldType.STRING, 1);
        pnd_Bene_Rcrd_Stts = localVariables.newFieldInRecord("pnd_Bene_Rcrd_Stts", "#BENE-RCRD-STTS", FieldType.STRING, 10);

        pnd_Bene_Rcrd_Stts__R_Field_2 = localVariables.newGroupInRecord("pnd_Bene_Rcrd_Stts__R_Field_2", "REDEFINE", pnd_Bene_Rcrd_Stts);
        pnd_Bene_Rcrd_Stts_Pnd_Bene_Rcrd_Stts_A1 = pnd_Bene_Rcrd_Stts__R_Field_2.newFieldArrayInGroup("pnd_Bene_Rcrd_Stts_Pnd_Bene_Rcrd_Stts_A1", "#BENE-RCRD-STTS-A1", 
            FieldType.STRING, 1, new DbsArrayController(1, 10));
        pnd_Stts_Found = localVariables.newFieldArrayInRecord("pnd_Stts_Found", "#STTS-FOUND", FieldType.BOOLEAN, 1, new DbsArrayController(1, 10));
        pnd_Other_Check_T_C_Ind = localVariables.newFieldInRecord("pnd_Other_Check_T_C_Ind", "#OTHER-CHECK-T-C-IND", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Bene_Rcrd_Stts.setInitialValue("CPRM");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Benn9866() throws Exception
    {
        super("Benn9866");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ERROR-INFO-SUB.##LAST-PROGRAM := *PROGRAM
        pdaBena9866.getBena9866().reset();                                                                                                                                //Natural: RESET BENA9866
                                                                                                                                                                          //Natural: PERFORM GET-EXISTING-BENE-RECORDS-STATUSES
        sub_Get_Existing_Bene_Records_Statuses();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-EXISTS-ON-INTERFACE-FILE
        sub_Check_If_Exists_On_Interface_File();
        if (condition(Global.isEscape())) {return;}
        short decideConditionsMet208 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN BENA9875.#EXISTS
        if (condition(pdaBena9875.getBena9875_Pnd_Exists().getBoolean()))
        {
            decideConditionsMet208++;
            //*  IF MORE THAN ONE INTERFACE RECORD EXISTS FOR THIS PIN/CNTRCT/IND
            //*    FOR THIS BUSINESS DATE, THEN ERROR.  THIS WILL HAVE TO BE
            //*    RESEARCHED BY THE USERS SO A DETERMINATION OF THE CORRECT
            //*    DESIGNATION CAN BE MADE
            //*  MUST BE MDO
            pdaBena9866.getBena9866_Pnd_Error_Msg().setValue(DbsUtil.compress("Multiple Same As Records found on Intrfce:", pdaBena9860.getBena9860_Pin_Tiaa_Cntrct(),    //Natural: COMPRESS 'Multiple Same As Records found on Intrfce:' BENA9860.PIN-TIAA-CNTRCT '/' BENA9860.TIAA-CREF-IND '/' BENA9860.RCRD-CRTD-FOR-BSNSS-DTE INTO BENA9866.#ERROR-MSG
                "/", pdaBena9860.getBena9860_Tiaa_Cref_Ind(), "/", pdaBena9860.getBena9860_Rcrd_Crtd_For_Bsnss_Dte()));
            pdaBena9866.getBena9866_Pnd_Error_Cde().setValue("NSAME");                                                                                                    //Natural: ASSIGN BENA9866.#ERROR-CDE := 'NSAME'
            pdaBena9866.getBena9866_Pnd_Edit_Failed().setValue(true);                                                                                                     //Natural: ASSIGN BENA9866.#EDIT-FAILED := TRUE
        }                                                                                                                                                                 //Natural: WHEN BENA9860.CHNG-NEW-ISSUE-IND = 'C' AND BENA9860.RQSTNG-SYSTM NE 'CORR'
        else if (condition(pdaBena9860.getBena9860_Chng_New_Issue_Ind().equals("C") && pdaBena9860.getBena9860_Rqstng_Systm().notEquals("CORR")))
        {
            decideConditionsMet208++;
            //*  NOTHING FOUND
            short decideConditionsMet221 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN NOT #STTS-FOUND ( * ) NE FALSE
            if (condition(! (pnd_Stts_Found.getValue("*").notEquals(false))))
            {
                decideConditionsMet221++;
                pdaBena9866.getBena9866_Pnd_Error_Msg().setValue(DbsUtil.compress(pdaBena9860.getBena9860_Pin_Tiaa_Cntrct(), "/", pdaBena9860.getBena9860_Tiaa_Cref_Ind(),  //Natural: COMPRESS BENA9860.PIN-TIAA-CNTRCT '/' BENA9860.TIAA-CREF-IND 'does not exist in the Bene System! (Change)' INTO BENA9866.#ERROR-MSG
                    "does not exist in the Bene System! (Change)"));
                pdaBena9866.getBena9866_Pnd_Error_Cde().setValue("NSAME");                                                                                                //Natural: ASSIGN BENA9866.#ERROR-CDE := 'NSAME'
                pdaBena9866.getBena9866_Pnd_Edit_Failed().setValue(true);                                                                                                 //Natural: ASSIGN BENA9866.#EDIT-FAILED := TRUE
            }                                                                                                                                                             //Natural: WHEN NOT ( #STTS-FOUND ( 1 ) OR #STTS-FOUND ( 4 ) )
            else if (condition(! ((pnd_Stts_Found.getValue(1).equals(true) || pnd_Stts_Found.getValue(4).equals(true)))))
            {
                decideConditionsMet221++;
                pdaBena9866.getBena9866_Pnd_Error_Msg().setValue(DbsUtil.compress("Designation not current in Bene System for", pdaBena9860.getBena9860_Pin_Tiaa_Cntrct(),  //Natural: COMPRESS 'Designation not current in Bene System for' BENA9860.PIN-TIAA-CNTRCT '/' BENA9860.TIAA-CREF-IND INTO BENA9866.#ERROR-MSG
                    "/", pdaBena9860.getBena9860_Tiaa_Cref_Ind()));
                pdaBena9866.getBena9866_Pnd_Error_Cde().setValue("SAME");                                                                                                 //Natural: ASSIGN BENA9866.#ERROR-CDE := 'SAME'
                pdaBena9866.getBena9866_Pnd_Edit_Failed().setValue(true);                                                                                                 //Natural: ASSIGN BENA9866.#EDIT-FAILED := TRUE
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN BENA9860.CHNG-NEW-ISSUE-IND = 'N' AND BENA9874.#FOUND NE FALSE AND #OTHER-CHECK-T-C-IND GT ' '
        else if (condition(pdaBena9860.getBena9860_Chng_New_Issue_Ind().equals("N") && pdaBena9874.getBena9874_Pnd_Found().notEquals(false) && pnd_Other_Check_T_C_Ind.greater(" ")))
        {
            decideConditionsMet208++;
            pdaBena9866.getBena9866_Pnd_Error_Msg().setValue(DbsUtil.compress(pdaBena9860.getBena9860_Pin_Tiaa_Cntrct(), "/", pnd_Other_Check_T_C_Ind,                    //Natural: COMPRESS BENA9860.PIN-TIAA-CNTRCT '/' #OTHER-CHECK-T-C-IND 'on BENE system awaits verify. Cannot Interface' BENA9860.TIAA-CREF-IND 'des.' INTO BENA9866.#ERROR-MSG
                "on BENE system awaits verify. Cannot Interface", pdaBena9860.getBena9860_Tiaa_Cref_Ind(), "des."));
            pdaBena9866.getBena9866_Pnd_Error_Cde().setValue("NSAME");                                                                                                    //Natural: ASSIGN BENA9866.#ERROR-CDE := 'NSAME'
            pdaBena9866.getBena9866_Pnd_Edit_Failed().setValue(true);                                                                                                     //Natural: ASSIGN BENA9866.#EDIT-FAILED := TRUE
        }                                                                                                                                                                 //Natural: WHEN ( BENA9860.CHNG-NEW-ISSUE-IND = 'N' OR BENA9860.RQSTNG-SYSTM = 'CORR' ) AND #STTS-FOUND ( * ) NE FALSE AND BENA9874.#TIAA-CREF-IND NE BENA9860.TIAA-CREF-IND
        else if (condition((pdaBena9860.getBena9860_Chng_New_Issue_Ind().equals("N") || pdaBena9860.getBena9860_Rqstng_Systm().equals("CORR")) && pnd_Stts_Found.getValue("*").notEquals(false) 
            && pdaBena9874.getBena9874_Pnd_Tiaa_Cref_Ind().notEquals(pdaBena9860.getBena9860_Tiaa_Cref_Ind())))
        {
            decideConditionsMet208++;
            //*  BENA9874.#TIAA-CREF-IND CARRIES THE CURRENT ONLINE VALUE
            short decideConditionsMet241 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF BENA9874.#TIAA-CREF-IND;//Natural: VALUE 'C'
            if (condition((pdaBena9874.getBena9874_Pnd_Tiaa_Cref_Ind().equals("C"))))
            {
                decideConditionsMet241++;
                pdaBena9866.getBena9866_Pnd_Error_Msg().setValue(DbsUtil.compress(pdaBena9860.getBena9860_Pin_Tiaa_Cntrct(), "already on BENE system with CREF-only ind")); //Natural: COMPRESS BENA9860.PIN-TIAA-CNTRCT 'already on BENE system with CREF-only ind' INTO BENA9866.#ERROR-MSG
            }                                                                                                                                                             //Natural: VALUE 'T'
            else if (condition((pdaBena9874.getBena9874_Pnd_Tiaa_Cref_Ind().equals("T"))))
            {
                decideConditionsMet241++;
                pdaBena9866.getBena9866_Pnd_Error_Msg().setValue(DbsUtil.compress(pdaBena9860.getBena9860_Pin_Tiaa_Cntrct(), "already on BENE system with TIAA-only ind")); //Natural: COMPRESS BENA9860.PIN-TIAA-CNTRCT 'already on BENE system with TIAA-only ind' INTO BENA9866.#ERROR-MSG
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pdaBena9866.getBena9866_Pnd_Error_Msg().setValue(DbsUtil.compress(pdaBena9860.getBena9860_Pin_Tiaa_Cntrct(), "already on BENE system for both TIAA & CREF")); //Natural: COMPRESS BENA9860.PIN-TIAA-CNTRCT 'already on BENE system for both TIAA & CREF' INTO BENA9866.#ERROR-MSG
            }                                                                                                                                                             //Natural: END-DECIDE
            pdaBena9866.getBena9866_Pnd_Error_Cde().setValue("NSAME");                                                                                                    //Natural: ASSIGN BENA9866.#ERROR-CDE := 'NSAME'
            pdaBena9866.getBena9866_Pnd_Edit_Failed().setValue(true);                                                                                                     //Natural: ASSIGN BENA9866.#EDIT-FAILED := TRUE
        }                                                                                                                                                                 //Natural: WHEN BENA9860.CHNG-NEW-ISSUE-IND = 'C' AND BENA9860.RQSTNG-SYSTM = 'CORR'
        else if (condition(pdaBena9860.getBena9860_Chng_New_Issue_Ind().equals("C") && pdaBena9860.getBena9860_Rqstng_Systm().equals("CORR")))
        {
            decideConditionsMet208++;
            //*  'P'
            //*  'R'
            if (condition(pnd_Stts_Found.getValue(2).getBoolean() || pnd_Stts_Found.getValue(3).getBoolean()))                                                            //Natural: IF #STTS-FOUND ( 2 ) OR #STTS-FOUND ( 3 )
            {
                pdaBena9866.getBena9866_Pnd_Error_Msg().setValue(DbsUtil.compress("Designation not current in Bene System for", pdaBena9860.getBena9860_Pin_Tiaa_Cntrct(),  //Natural: COMPRESS 'Designation not current in Bene System for' BENA9860.PIN-TIAA-CNTRCT '/' BENA9860.TIAA-CREF-IND INTO BENA9866.#ERROR-MSG
                    "/", pdaBena9860.getBena9860_Tiaa_Cref_Ind()));
                pdaBena9866.getBena9866_Pnd_Error_Cde().setValue("NSAME");                                                                                                //Natural: ASSIGN BENA9866.#ERROR-CDE := 'NSAME'
                pdaBena9866.getBena9866_Pnd_Edit_Failed().setValue(true);                                                                                                 //Natural: ASSIGN BENA9866.#EDIT-FAILED := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN BENA9860.CHNG-NEW-ISSUE-IND = 'C' AND BENA9860.EFF-DTE <= BENA9876.#BC-EFF-DTE
        else if (condition(pdaBena9860.getBena9860_Chng_New_Issue_Ind().equals("C") && pdaBena9860.getBena9860_Eff_Dte().lessOrEqual(pdaBena9876.getBena9876_Pnd_Bc_Eff_Dte())))
        {
            decideConditionsMet208++;
            pdaBena9866.getBena9866_Pnd_Error_Msg().setValue(DbsUtil.compress("Designation Effective Date is", pdaBena9876.getBena9876_Pnd_Bc_Eff_Dte(),                  //Natural: COMPRESS 'Designation Effective Date is' BENA9876.#BC-EFF-DTE 'for' BENA9860.PIN-TIAA-CNTRCT '/' BENA9860.TIAA-CREF-IND INTO BENA9866.#ERROR-MSG
                "for", pdaBena9860.getBena9860_Pin_Tiaa_Cntrct(), "/", pdaBena9860.getBena9860_Tiaa_Cref_Ind()));
            pdaBena9866.getBena9866_Pnd_Error_Cde().setValue("NSAME");                                                                                                    //Natural: ASSIGN BENA9866.#ERROR-CDE := 'NSAME'
            pdaBena9866.getBena9866_Pnd_Edit_Failed().setValue(true);                                                                                                     //Natural: ASSIGN BENA9866.#EDIT-FAILED := TRUE
        }                                                                                                                                                                 //Natural: WHEN BENA9860.CHNG-NEW-ISSUE-IND = 'N' AND #STTS-FOUND ( * ) NE FALSE
        else if (condition(pdaBena9860.getBena9860_Chng_New_Issue_Ind().equals("N") && pnd_Stts_Found.getValue("*").notEquals(false)))
        {
            decideConditionsMet208++;
            pdaBena9866.getBena9866_Pnd_Error_Msg().setValue(DbsUtil.compress(pdaBena9860.getBena9860_Pin_Tiaa_Cntrct(), "/", pdaBena9874.getBena9874_Pnd_Tiaa_Cref_Ind(),  //Natural: COMPRESS BENA9860.PIN-TIAA-CNTRCT '/' BENA9874.#TIAA-CREF-IND 'already on BENE system for NEW ISSUE process' INTO BENA9866.#ERROR-MSG
                "already on BENE system for NEW ISSUE process"));
            pdaBena9866.getBena9866_Pnd_Error_Cde().setValue("NSAME");                                                                                                    //Natural: ASSIGN BENA9866.#ERROR-CDE := 'NSAME'
            pdaBena9866.getBena9866_Pnd_Edit_Failed().setValue(true);                                                                                                     //Natural: ASSIGN BENA9866.#EDIT-FAILED := TRUE
            //*   FOR A NEW ISSUE:
            //*   IF THIS PIN/CONTRACT/IND EXISTS ON THE INTEFACE FILE IN AN
            //*     "ERRORed" STATUS, KICK IT OUT!
            //*  WHEN BENA9860.CHNG-NEW-ISSUE-IND = 'N' AND
            //*    BENA9702.#ERRORED-EXISTS = TRUE
            //*    COMPRESS BENA9860.PIN-TIAA-CNTRCT '/' BENA9860.TIAA-CREF-IND
            //*      'already in ERROR status on INTERFACE FILE'
            //*      INTO BENA9866.#ERROR-MSG
            //*     BENA9866.#EDIT-FAILED = TRUE
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pdaBena9866.getBena9866_Pnd_Edit_Failed().equals(true)))                                                                                            //Natural: IF BENA9866.#EDIT-FAILED = TRUE
        {
            pdaBena9866.getBena9866_Pnd_Error_Pgm().setValue(Global.getPROGRAM());                                                                                        //Natural: ASSIGN BENA9866.#ERROR-PGM := *PROGRAM
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet289 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN MORE-THAN-FIVE-BENES-IND = 'Y'
        if (condition(pdaBena9860.getBena9860_More_Than_Five_Benes_Ind().equals("Y")))
        {
            decideConditionsMet289++;
            pdaBena9866.getBena9866_Pnd_Error_Msg().setValue("ACIS - More than ten benes");                                                                               //Natural: ASSIGN #ERROR-MSG := 'ACIS - More than ten benes'
        }                                                                                                                                                                 //Natural: WHEN ILLGBLE-IND = 'Y'
        else if (condition(pdaBena9860.getBena9860_Illgble_Ind().equals("Y")))
        {
            decideConditionsMet289++;
            pdaBena9866.getBena9866_Pnd_Error_Msg().setValue("ACIS - Illegible");                                                                                         //Natural: ASSIGN #ERROR-MSG := 'ACIS - Illegible'
        }                                                                                                                                                                 //Natural: WHEN MORE-THAN-THIRTY-BENES-IND = 'Y'
        else if (condition(pdaBena9860.getBena9860_More_Than_Thirty_Benes_Ind().equals("Y")))
        {
            decideConditionsMet289++;
            pdaBena9866.getBena9866_Pnd_Error_Msg().setValue("More than thirty benes");                                                                                   //Natural: ASSIGN #ERROR-MSG := 'More than thirty benes'
        }                                                                                                                                                                 //Natural: WHEN RCRD-CRTD-FOR-BSNSS-DTE NE MASK ( YYYYMMDD )
        else if (condition(! ((DbsUtil.maskMatches(pdaBena9860.getBena9860_Rcrd_Crtd_For_Bsnss_Dte(),"YYYYMMDD")))))
        {
            decideConditionsMet289++;
            pdaBena9866.getBena9866_Pnd_Error_Msg().setValue(DbsUtil.compress("Ctrd For Bsnss Dte Invalid:", pdaBena9860.getBena9860_Rcrd_Crtd_For_Bsnss_Dte()));         //Natural: COMPRESS 'Ctrd For Bsnss Dte Invalid:' RCRD-CRTD-FOR-BSNSS-DTE INTO #ERROR-MSG
        }                                                                                                                                                                 //Natural: WHEN INTRFCNG-SYSTM = ' '
        else if (condition(pdaBena9860.getBena9860_Intrfcng_Systm().equals(" ")))
        {
            decideConditionsMet289++;
            pdaBena9866.getBena9866_Pnd_Error_Msg().setValue(DbsUtil.compress("Invalid interfacing system:", pdaBena9860.getBena9860_Intrfcng_Systm()));                  //Natural: COMPRESS 'Invalid interfacing system:' INTRFCNG-SYSTM INTO #ERROR-MSG
        }                                                                                                                                                                 //Natural: WHEN RQSTNG-SYSTM = ' '
        else if (condition(pdaBena9860.getBena9860_Rqstng_Systm().equals(" ")))
        {
            decideConditionsMet289++;
            pdaBena9866.getBena9866_Pnd_Error_Msg().setValue(DbsUtil.compress("Invalid requesting system:", pdaBena9860.getBena9860_Rqstng_Systm()));                     //Natural: COMPRESS 'Invalid requesting system:' RQSTNG-SYSTM INTO #ERROR-MSG
        }                                                                                                                                                                 //Natural: WHEN TIAA-CNTRCT = ' '
        else if (condition(pdaBena9860.getBena9860_Tiaa_Cntrct().equals(" ")))
        {
            decideConditionsMet289++;
            pdaBena9866.getBena9866_Pnd_Error_Msg().setValue(DbsUtil.compress("Invalid TIAA Contract:", pdaBena9860.getBena9860_Pin(), pdaBena9860.getBena9860_Tiaa_Cntrct())); //Natural: COMPRESS 'Invalid TIAA Contract:' PIN TIAA-CNTRCT INTO #ERROR-MSG
        }                                                                                                                                                                 //Natural: WHEN PIN NE MASK ( 9999999 )
        else if (condition(! ((DbsUtil.maskMatches(pdaBena9860.getBena9860_Pin(),"9999999")))))
        {
            decideConditionsMet289++;
            pdaBena9866.getBena9866_Pnd_Error_Msg().setValue(DbsUtil.compress("Invalid PIN:", pdaBena9860.getBena9860_Pin()));                                            //Natural: COMPRESS 'Invalid PIN:' PIN INTO #ERROR-MSG
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet289 > 0))
        {
            pdaBena9866.getBena9866_Pnd_Edit_Failed().setValue(true);                                                                                                     //Natural: ASSIGN BENA9866.#EDIT-FAILED := TRUE
            pdaBena9866.getBena9866_Pnd_Error_Pgm().setValue(Global.getPROGRAM());                                                                                        //Natural: ASSIGN BENA9866.#ERROR-PGM := *PROGRAM
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  THESE WERE EDITED 'O'NLINE - CANNOT BE
        //*  UPDATED BATCH BY ACIS. (SHOULDN't happen)
        if (condition(pdaBena9860.getBena9860_Rqstng_Systm().equals("CORR") && (pnd_C_Last_Srce.equals("O") || pnd_M_Last_Srce.equals("O"))))                             //Natural: IF BENA9860.RQSTNG-SYSTM = 'CORR' AND ( #C-LAST-SRCE = 'O' OR #M-LAST-SRCE = 'O' )
        {
            //*  CREATE TERMINATE - CONTROL GOES BACK TO DRIVER FOR DISPLAY.
            pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("Illegal CORR Transaction sent from ACIS");                                                                //Natural: ASSIGN ##MSG := 'Illegal CORR Transaction sent from ACIS'
            pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                              //Natural: ASSIGN ##RETURN-CODE := 'E'
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-EXISTING-BENE-RECORDS-STATUSES
        //*  CHECK T/C-IND = ' ' - CANNOT CO-EXIST WITH A 'T' OR A 'C'
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-IF-EXISTS-ON-INTERFACE-FILE
        //* ***********************************************************************
    }
    private void sub_Get_Existing_Bene_Records_Statuses() throws Exception                                                                                                //Natural: GET-EXISTING-BENE-RECORDS-STATUSES
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*   DETERMINE WHICH STATUSES EXIST IN THE BENE SYSTEM.
        pdaBena9876.getBena9876().reset();                                                                                                                                //Natural: RESET BENA9876
        pdaBena9876.getBena9876_Pnd_Pin_Tiaa_Cntrct().setValue(pdaBena9860.getBena9860_Pin_Tiaa_Cntrct());                                                                //Natural: ASSIGN BENA9876.#PIN-TIAA-CNTRCT := BENA9860.PIN-TIAA-CNTRCT
        pdaBena9874.getBena9874_Pnd_Pin_Tiaa_Cntrct().setValue(pdaBena9860.getBena9860_Pin_Tiaa_Cntrct());                                                                //Natural: ASSIGN BENA9874.#PIN-TIAA-CNTRCT := BENA9860.PIN-TIAA-CNTRCT
        pdaBena9874.getBena9874_Pnd_Tiaa_Cref_Ind().setValue(pdaBena9860.getBena9860_Tiaa_Cref_Ind());                                                                    //Natural: ASSIGN BENA9874.#TIAA-CREF-IND := BENA9860.TIAA-CREF-IND
        pnd_Other_Check_T_C_Ind.reset();                                                                                                                                  //Natural: RESET #OTHER-CHECK-T-C-IND
        //* IF NEW ISSUE AND BLANK T/C THEN
        //*  CONTRACT SHOULD'nt be on-line
        if (condition((pdaBena9860.getBena9860_Chng_New_Issue_Ind().equals("N") || pdaBena9860.getBena9860_Rqstng_Systm().equals("CORR")) && pdaBena9860.getBena9860_Tiaa_Cref_Ind().equals(" "))) //Natural: IF ( BENA9860.CHNG-NEW-ISSUE-IND = 'N' OR BENA9860.RQSTNG-SYSTM = 'CORR' ) AND BENA9860.TIAA-CREF-IND = ' '
        {
            //*  FIND BY CONTRACT # ONLY            /* AT ALL
            //*  HOWEVER, CORR CAN BE ON-LINE AS A 'Both' IF THAT's what is interfacing
            DbsUtil.callnat(Benn9876.class , getCurrentProcessState(), pdaBena9876.getBena9876(), pdaBenpda_M.getMsg_Info_Sub(), pdaBenpda_E.getError_Info_Sub());        //Natural: CALLNAT 'BENN9876' BENA9876 MSG-INFO-SUB ERROR-INFO-SUB
            if (condition(Global.isEscape())) return;
            pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                           //Natural: ASSIGN ERROR-INFO-SUB.##LAST-PROGRAM := *PROGRAM
            if (condition(pdaBena9876.getBena9876_Pnd_Found().getBoolean()))                                                                                              //Natural: IF BENA9876.#FOUND
            {
                if (condition(pdaBena9860.getBena9860_Chng_New_Issue_Ind().equals("N") || (pdaBena9876.getBena9876_Pnd_Tiaa_Cref_Ind().greater(" ") &&                    //Natural: IF BENA9860.CHNG-NEW-ISSUE-IND = 'N' OR ( BENA9876.#TIAA-CREF-IND GT ' ' AND BENA9860.RQSTNG-SYSTM = 'CORR' )
                    pdaBena9860.getBena9860_Rqstng_Systm().equals("CORR"))))
                {
                    pnd_Stts_Found.getValue(1).setValue(pdaBena9876.getBena9876_Pnd_Found().getBoolean());                                                                //Natural: ASSIGN #STTS-FOUND ( 1 ) := BENA9876.#FOUND
                    pdaBena9874.getBena9874_Pnd_Tiaa_Cref_Ind().setValue(pdaBena9876.getBena9876_Pnd_Tiaa_Cref_Ind());                                                    //Natural: ASSIGN BENA9874.#TIAA-CREF-IND := BENA9876.#TIAA-CREF-IND
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        F1:                                                                                                                                                               //Natural: FOR #I 1 10
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(10)); pnd_I.nadd(1))
        {
            if (condition(pnd_Bene_Rcrd_Stts_Pnd_Bene_Rcrd_Stts_A1.getValue(pnd_I).equals(" ")))                                                                          //Natural: IF #BENE-RCRD-STTS-A1 ( #I ) = ' '
            {
                if (true) break F1;                                                                                                                                       //Natural: ESCAPE BOTTOM ( F1. )
            }                                                                                                                                                             //Natural: END-IF
            pdaBena9874.getBena9874_Pnd_Stts().setValue(pnd_Bene_Rcrd_Stts_Pnd_Bene_Rcrd_Stts_A1.getValue(pnd_I));                                                        //Natural: ASSIGN BENA9874.#STTS := #BENE-RCRD-STTS-A1 ( #I )
            pdaBena9874.getBena9874_Pnd_Found().reset();                                                                                                                  //Natural: RESET BENA9874.#FOUND BENA9874.#LAST-SRCE
            pdaBena9874.getBena9874_Pnd_Last_Srce().reset();
            DbsUtil.callnat(Benn9874.class , getCurrentProcessState(), pdaBena9874.getBena9874(), pdaBenpda_M.getMsg_Info_Sub(), pdaBenpda_E.getError_Info_Sub());        //Natural: CALLNAT 'BENN9874' BENA9874 MSG-INFO-SUB ERROR-INFO-SUB
            if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
            pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                           //Natural: ASSIGN ERROR-INFO-SUB.##LAST-PROGRAM := *PROGRAM
            pnd_Stts_Found.getValue(pnd_I).setValue(pdaBena9874.getBena9874_Pnd_Found().getBoolean());                                                                    //Natural: ASSIGN #STTS-FOUND ( #I ) := BENA9874.#FOUND
            if (condition(pnd_I.equals(1)))                                                                                                                               //Natural: IF #I = 1
            {
                pnd_C_Last_Srce.setValue(pdaBena9874.getBena9874_Pnd_Last_Srce());                                                                                        //Natural: MOVE BENA9874.#LAST-SRCE TO #C-LAST-SRCE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_I.equals(4)))                                                                                                                               //Natural: IF #I = 4
            {
                pnd_M_Last_Srce.setValue(pdaBena9874.getBena9874_Pnd_Last_Srce());                                                                                        //Natural: MOVE BENA9874.#LAST-SRCE TO #M-LAST-SRCE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  WON't happen with MDO 'C'hange
        //*  REJECT CONDITION NOT FOUND YET
        //*  NOTE 9814 AND 9801 T/CIND NOW
        if (condition((pdaBena9860.getBena9860_Tiaa_Cref_Ind().greater(" ") && ((pdaBena9860.getBena9860_Rqstng_Systm().notEquals("CORR") && ! (pnd_Stts_Found.getValue("*").notEquals(false)))  //Natural: IF BENA9860.TIAA-CREF-IND GT ' ' AND ( ( BENA9860.RQSTNG-SYSTM NE 'CORR' AND NOT #STTS-FOUND ( * ) NE FALSE ) OR ( BENA9860.RQSTNG-SYSTM = 'CORR' AND NOT #STTS-FOUND ( 2:3 ) NE FALSE ) )
            || (pdaBena9860.getBena9860_Rqstng_Systm().equals("CORR") && ! (pnd_Stts_Found.getValue(2,":",3).notEquals(false)))))))
        {
            pdaBena9874.getBena9874_Pnd_Tiaa_Cref_Ind().setValue(" ");                                                                                                    //Natural: ASSIGN BENA9874.#TIAA-CREF-IND := ' '
            F2:                                                                                                                                                           //Natural: FOR #I = 1 TO 10
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(10)); pnd_I.nadd(1))
            {
                if (condition(pnd_Bene_Rcrd_Stts_Pnd_Bene_Rcrd_Stts_A1.getValue(pnd_I).equals(" ")))                                                                      //Natural: IF #BENE-RCRD-STTS-A1 ( #I ) = ' '
                {
                    if (true) break F2;                                                                                                                                   //Natural: ESCAPE BOTTOM ( F2. )
                }                                                                                                                                                         //Natural: END-IF
                pdaBena9874.getBena9874_Pnd_Stts().setValue(pnd_Bene_Rcrd_Stts_Pnd_Bene_Rcrd_Stts_A1.getValue(pnd_I));                                                    //Natural: ASSIGN BENA9874.#STTS := #BENE-RCRD-STTS-A1 ( #I )
                pdaBena9874.getBena9874_Pnd_Found().reset();                                                                                                              //Natural: RESET BENA9874.#FOUND
                DbsUtil.callnat(Benn9874.class , getCurrentProcessState(), pdaBena9874.getBena9874(), pdaBenpda_M.getMsg_Info_Sub(), pdaBenpda_E.getError_Info_Sub());    //Natural: CALLNAT 'BENN9874' BENA9874 MSG-INFO-SUB ERROR-INFO-SUB
                if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                       //Natural: ASSIGN ERROR-INFO-SUB.##LAST-PROGRAM := *PROGRAM
                pnd_Stts_Found.getValue(pnd_I).setValue(pdaBena9874.getBena9874_Pnd_Found().getBoolean());                                                                //Natural: ASSIGN #STTS-FOUND ( #I ) := BENA9874.#FOUND
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF CORR (MDO NO TCIND)
        if (condition(pdaBena9860.getBena9860_Chng_New_Issue_Ind().equals("C")))                                                                                          //Natural: IF BENA9860.CHNG-NEW-ISSUE-IND = 'C'
        {
            //*  & 'T' INCOMING AND 'C' ONLINE (OR VICE VERSA) WE DO
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
            //*  NOT NEED TO CHECK COUNTERPART FOR CHANGE -IF COUNTER
        }                                                                                                                                                                 //Natural: END-IF
        //*  PART IS 'unverified' IT WILL NOT AFFECT INCOMING CORR.
        //*  WON't happen with MDO 'C'hange
        //*  REJECT CONDITION NOT FOUND YET
        if (condition(pdaBena9860.getBena9860_Tiaa_Cref_Ind().greater(" ") && ! (pnd_Stts_Found.getValue("*").notEquals(false))))                                         //Natural: IF BENA9860.TIAA-CREF-IND GT ' ' AND NOT #STTS-FOUND ( * ) NE FALSE
        {
            //*  CHECK 'other' T/C-IND - CANNOT BE AWAITING VERIFICATION
            if (condition(pdaBena9860.getBena9860_Tiaa_Cref_Ind().equals("T")))                                                                                           //Natural: IF BENA9860.TIAA-CREF-IND = 'T'
            {
                pdaBena9874.getBena9874_Pnd_Tiaa_Cref_Ind().setValue("C");                                                                                                //Natural: ASSIGN BENA9874.#TIAA-CREF-IND := 'C'
                pnd_Other_Check_T_C_Ind.setValue("C");                                                                                                                    //Natural: ASSIGN #OTHER-CHECK-T-C-IND := 'C'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaBena9860.getBena9860_Tiaa_Cref_Ind().equals("C")))                                                                                           //Natural: IF BENA9860.TIAA-CREF-IND = 'C'
            {
                pdaBena9874.getBena9874_Pnd_Tiaa_Cref_Ind().setValue("T");                                                                                                //Natural: ASSIGN BENA9874.#TIAA-CREF-IND := 'T'
                pnd_Other_Check_T_C_Ind.setValue("T");                                                                                                                    //Natural: ASSIGN #OTHER-CHECK-T-C-IND := 'T'
                //*  'C' STATUS OK
            }                                                                                                                                                             //Natural: END-IF
            F3:                                                                                                                                                           //Natural: FOR #I = 2 TO 10
            for (pnd_I.setValue(2); condition(pnd_I.lessOrEqual(10)); pnd_I.nadd(1))
            {
                if (condition(pnd_Bene_Rcrd_Stts_Pnd_Bene_Rcrd_Stts_A1.getValue(pnd_I).equals(" ")))                                                                      //Natural: IF #BENE-RCRD-STTS-A1 ( #I ) = ' '
                {
                    if (true) break F3;                                                                                                                                   //Natural: ESCAPE BOTTOM ( F3. )
                }                                                                                                                                                         //Natural: END-IF
                pdaBena9874.getBena9874_Pnd_Stts().setValue(pnd_Bene_Rcrd_Stts_Pnd_Bene_Rcrd_Stts_A1.getValue(pnd_I));                                                    //Natural: ASSIGN BENA9874.#STTS := #BENE-RCRD-STTS-A1 ( #I )
                pnd_Stts_Found.getValue("*").reset();                                                                                                                     //Natural: RESET #STTS-FOUND ( * )
                DbsUtil.callnat(Benn9874.class , getCurrentProcessState(), pdaBena9874.getBena9874(), pdaBenpda_M.getMsg_Info_Sub(), pdaBenpda_E.getError_Info_Sub());    //Natural: CALLNAT 'BENN9874' BENA9874 MSG-INFO-SUB ERROR-INFO-SUB
                if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                       //Natural: ASSIGN ERROR-INFO-SUB.##LAST-PROGRAM := *PROGRAM
                pnd_Stts_Found.getValue(pnd_I).setValue(pdaBena9874.getBena9874_Pnd_Found().getBoolean());                                                                //Natural: ASSIGN #STTS-FOUND ( #I ) := BENA9874.#FOUND
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  GET-EXISTING-BENE-RECORDS-STATUSES
    }
    private void sub_Check_If_Exists_On_Interface_File() throws Exception                                                                                                 //Natural: CHECK-IF-EXISTS-ON-INTERFACE-FILE
    {
        if (BLNatReinput.isReinput()) return;

        pdaBena9875.getBena9875_Pnd_Pin().setValue(pdaBena9860.getBena9860_Pin());                                                                                        //Natural: ASSIGN BENA9875.#PIN := BENA9860.PIN
        pdaBena9875.getBena9875_Pnd_Tiaa_Cntrct().setValue(pdaBena9860.getBena9860_Tiaa_Cntrct());                                                                        //Natural: ASSIGN BENA9875.#TIAA-CNTRCT := BENA9860.TIAA-CNTRCT
        pdaBena9875.getBena9875_Pnd_Tiaa_Cref_Ind().setValue(pdaBena9860.getBena9860_Tiaa_Cref_Ind());                                                                    //Natural: ASSIGN BENA9875.#TIAA-CREF-IND := BENA9860.TIAA-CREF-IND
        pdaBena9875.getBena9875_Pnd_Rcrd_Crtd_Dte().setValue(pdaBena9860.getBena9860_Rcrd_Crtd_Dte());                                                                    //Natural: ASSIGN BENA9875.#RCRD-CRTD-DTE := BENA9860.RCRD-CRTD-DTE
        pdaBena9875.getBena9875_Pnd_Rcrd_Crtd_Tme().setValue(pdaBena9860.getBena9860_Rcrd_Crtd_Tme());                                                                    //Natural: ASSIGN BENA9875.#RCRD-CRTD-TME := BENA9860.RCRD-CRTD-TME
        pdaBena9875.getBena9875_Pnd_Bsnss_Dte().setValue(pdaBena9873.getBena9873_Pnd_Bsnss_Dte());                                                                        //Natural: ASSIGN BENA9875.#BSNSS-DTE := BENA9873.#BSNSS-DTE
        DbsUtil.callnat(Benn9875.class , getCurrentProcessState(), pdaBena9875.getBena9875(), pdaBenpda_M.getMsg_Info_Sub());                                             //Natural: CALLNAT 'BENN9875' BENA9875 MSG-INFO-SUB
        if (condition(Global.isEscape())) return;
        //*  CHECK-IF-EXISTS-ON-INTERFACE-FILE
    }

    //
}
