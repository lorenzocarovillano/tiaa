/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:06:38 PM
**        * FROM NATURAL SUBPROGRAM : Benn9863
************************************************************
**        * FILE NAME            : Benn9863.java
**        * CLASS NAME           : Benn9863
**        * INSTANCE NAME        : Benn9863
************************************************************
************************************************************************
* PROGRAM  : BENN9863
* SYSTEM   : BENEFICIARY-SYSTEM /* INTERFACE SAME AS
* TITLE    : TRANSLATE RELATIONSHIP TEXT
* GENERATED: JULY 6,99 AT 04:10 PM
*          : THIS PROGRAM CONVERTS THE SUPPLIED RELATIONSHIP TEST TO
*          : UPPER CASE, AND ATTEMPTS TO MATCH TO THE
*          : CONVERTED TEXT ON THE RELATIONSHIP TABLE
*
* HISTORY
* CHANGED ON JUNE 3,99 BY DEPAUL FOR RELEASE ____
* CLONED FROM BENN9833 -FONTOUR
* > DEC 03,99. JW GET TABLE FILE FOR $R FROM PDA BENA9834, REMOVE READ
* > DEC 12,00. ADD REL HISTOGRAM - NO MORE DEFAULT TO '38' IF NOT FOUND
************************************************************************

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benn9863 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaBena9863 pdaBena9863;
    private PdaBena9864 pdaBena9864;
    private PdaBenpda_M pdaBenpda_M;
    private PdaBenpda_E pdaBenpda_E;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Bt_Table_Id_Key;

    private DbsGroup pnd_Bt_Table_Id_Key__R_Field_1;
    private DbsField pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Id;
    private DbsField pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Key;

    private DbsGroup pnd_Work;
    private DbsField pnd_Work_Pnd_Systm_Text;

    private DbsGroup pnd_Work__R_Field_2;
    private DbsField pnd_Work_Pnd_I_S_Text_H1;

    private DbsGroup pnd_Work__R_Field_3;
    private DbsField pnd_Work_Pnd_I_S_Text_First_Char;
    private DbsField pnd_Work_Pnd_Text;

    private DbsGroup pnd_Work__R_Field_4;
    private DbsField pnd_Work_Pnd_Text_H1;
    private DbsField pnd_I1;
    private DbsField pnd_T_I;
    private DbsField pnd_J1;

    private DataAccessProgramView vw_rt;
    private DbsField rt_Bt_Table_Id_Key;

    private DbsGroup rt__R_Field_5;
    private DbsField rt_Pnd_Bt_Table_Id;
    private DbsField rt_Pnd_Bt_Table_Key_15;
    private DbsField rt_Pnd_Bt_Table_Key_Rc_Cde;
    private DbsField pnd_Bt_Table_Id_Key_Lo;

    private DbsGroup pnd_Bt_Table_Id_Key_Lo__R_Field_6;
    private DbsField pnd_Bt_Table_Id_Key_Lo_Pnd_Bt_Table_Id_Lo;
    private DbsField pnd_Bt_Table_Id_Key_Lo_Pnd_Bt_Table_Key_Lo;

    private DbsGroup pnd_Bt_Table_Id_Key_Lo__R_Field_7;
    private DbsField pnd_Bt_Table_Id_Key_Lo_Pnd_Bt_Table_Key_Lo_Lit;
    private DbsField pnd_Bt_Table_Id_Key_Hi;

    private DbsGroup pnd_Bt_Table_Id_Key_Hi__R_Field_8;
    private DbsField pnd_Bt_Table_Id_Key_Hi_Pnd_Bt_Table_Id_Hi;
    private DbsField pnd_Bt_Table_Id_Key_Hi_Pnd_Bt_Table_Key_Hi;

    private DbsGroup pnd_Bt_Table_Id_Key_Hi__R_Field_9;
    private DbsField pnd_Bt_Table_Id_Key_Hi_Pnd_Bt_Table_Key_Hi_Lit;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaBena9863 = new PdaBena9863(parameters);
        pdaBena9864 = new PdaBena9864(parameters);
        pdaBenpda_M = new PdaBenpda_M(parameters);
        pdaBenpda_E = new PdaBenpda_E(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Bt_Table_Id_Key = localVariables.newFieldInRecord("pnd_Bt_Table_Id_Key", "#BT-TABLE-ID-KEY", FieldType.STRING, 22);

        pnd_Bt_Table_Id_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Bt_Table_Id_Key__R_Field_1", "REDEFINE", pnd_Bt_Table_Id_Key);
        pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Id = pnd_Bt_Table_Id_Key__R_Field_1.newFieldInGroup("pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Id", "#BT-TABLE-ID", FieldType.STRING, 
            2);
        pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Key = pnd_Bt_Table_Id_Key__R_Field_1.newFieldInGroup("pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Key", "#BT-TABLE-KEY", 
            FieldType.STRING, 20);

        pnd_Work = localVariables.newGroupInRecord("pnd_Work", "#WORK");
        pnd_Work_Pnd_Systm_Text = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Systm_Text", "#SYSTM-TEXT", FieldType.STRING, 15);

        pnd_Work__R_Field_2 = pnd_Work.newGroupInGroup("pnd_Work__R_Field_2", "REDEFINE", pnd_Work_Pnd_Systm_Text);
        pnd_Work_Pnd_I_S_Text_H1 = pnd_Work__R_Field_2.newFieldArrayInGroup("pnd_Work_Pnd_I_S_Text_H1", "#I-S-TEXT-H1", FieldType.BINARY, 1, new DbsArrayController(1, 
            15));

        pnd_Work__R_Field_3 = pnd_Work.newGroupInGroup("pnd_Work__R_Field_3", "REDEFINE", pnd_Work_Pnd_Systm_Text);
        pnd_Work_Pnd_I_S_Text_First_Char = pnd_Work__R_Field_3.newFieldInGroup("pnd_Work_Pnd_I_S_Text_First_Char", "#I-S-TEXT-FIRST-CHAR", FieldType.STRING, 
            1);
        pnd_Work_Pnd_Text = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Text", "#TEXT", FieldType.STRING, 15);

        pnd_Work__R_Field_4 = pnd_Work.newGroupInGroup("pnd_Work__R_Field_4", "REDEFINE", pnd_Work_Pnd_Text);
        pnd_Work_Pnd_Text_H1 = pnd_Work__R_Field_4.newFieldArrayInGroup("pnd_Work_Pnd_Text_H1", "#TEXT-H1", FieldType.BINARY, 1, new DbsArrayController(1, 
            15));
        pnd_I1 = localVariables.newFieldInRecord("pnd_I1", "#I1", FieldType.PACKED_DECIMAL, 3);
        pnd_T_I = localVariables.newFieldInRecord("pnd_T_I", "#T-I", FieldType.PACKED_DECIMAL, 3);
        pnd_J1 = localVariables.newFieldInRecord("pnd_J1", "#J1", FieldType.PACKED_DECIMAL, 9);

        vw_rt = new DataAccessProgramView(new NameInfo("vw_rt", "RT"), "BENE_TABLE_FILE", "BENE_TABLE");
        rt_Bt_Table_Id_Key = vw_rt.getRecord().newFieldInGroup("rt_Bt_Table_Id_Key", "BT-TABLE-ID-KEY", FieldType.STRING, 22, RepeatingFieldStrategy.None, 
            "BT_TABLE_ID_KEY");
        rt_Bt_Table_Id_Key.setDdmHeader("TABLE ID/KEY");
        rt_Bt_Table_Id_Key.setSuperDescriptor(true);

        rt__R_Field_5 = vw_rt.getRecord().newGroupInGroup("rt__R_Field_5", "REDEFINE", rt_Bt_Table_Id_Key);
        rt_Pnd_Bt_Table_Id = rt__R_Field_5.newFieldInGroup("rt_Pnd_Bt_Table_Id", "#BT-TABLE-ID", FieldType.STRING, 2);
        rt_Pnd_Bt_Table_Key_15 = rt__R_Field_5.newFieldInGroup("rt_Pnd_Bt_Table_Key_15", "#BT-TABLE-KEY-15", FieldType.STRING, 15);
        rt_Pnd_Bt_Table_Key_Rc_Cde = rt__R_Field_5.newFieldInGroup("rt_Pnd_Bt_Table_Key_Rc_Cde", "#BT-TABLE-KEY-RC-CDE", FieldType.STRING, 2);
        registerRecord(vw_rt);

        pnd_Bt_Table_Id_Key_Lo = localVariables.newFieldInRecord("pnd_Bt_Table_Id_Key_Lo", "#BT-TABLE-ID-KEY-LO", FieldType.STRING, 22);

        pnd_Bt_Table_Id_Key_Lo__R_Field_6 = localVariables.newGroupInRecord("pnd_Bt_Table_Id_Key_Lo__R_Field_6", "REDEFINE", pnd_Bt_Table_Id_Key_Lo);
        pnd_Bt_Table_Id_Key_Lo_Pnd_Bt_Table_Id_Lo = pnd_Bt_Table_Id_Key_Lo__R_Field_6.newFieldInGroup("pnd_Bt_Table_Id_Key_Lo_Pnd_Bt_Table_Id_Lo", "#BT-TABLE-ID-LO", 
            FieldType.STRING, 2);
        pnd_Bt_Table_Id_Key_Lo_Pnd_Bt_Table_Key_Lo = pnd_Bt_Table_Id_Key_Lo__R_Field_6.newFieldInGroup("pnd_Bt_Table_Id_Key_Lo_Pnd_Bt_Table_Key_Lo", "#BT-TABLE-KEY-LO", 
            FieldType.STRING, 20);

        pnd_Bt_Table_Id_Key_Lo__R_Field_7 = pnd_Bt_Table_Id_Key_Lo__R_Field_6.newGroupInGroup("pnd_Bt_Table_Id_Key_Lo__R_Field_7", "REDEFINE", pnd_Bt_Table_Id_Key_Lo_Pnd_Bt_Table_Key_Lo);
        pnd_Bt_Table_Id_Key_Lo_Pnd_Bt_Table_Key_Lo_Lit = pnd_Bt_Table_Id_Key_Lo__R_Field_7.newFieldInGroup("pnd_Bt_Table_Id_Key_Lo_Pnd_Bt_Table_Key_Lo_Lit", 
            "#BT-TABLE-KEY-LO-LIT", FieldType.STRING, 15);
        pnd_Bt_Table_Id_Key_Hi = localVariables.newFieldInRecord("pnd_Bt_Table_Id_Key_Hi", "#BT-TABLE-ID-KEY-HI", FieldType.STRING, 22);

        pnd_Bt_Table_Id_Key_Hi__R_Field_8 = localVariables.newGroupInRecord("pnd_Bt_Table_Id_Key_Hi__R_Field_8", "REDEFINE", pnd_Bt_Table_Id_Key_Hi);
        pnd_Bt_Table_Id_Key_Hi_Pnd_Bt_Table_Id_Hi = pnd_Bt_Table_Id_Key_Hi__R_Field_8.newFieldInGroup("pnd_Bt_Table_Id_Key_Hi_Pnd_Bt_Table_Id_Hi", "#BT-TABLE-ID-HI", 
            FieldType.STRING, 2);
        pnd_Bt_Table_Id_Key_Hi_Pnd_Bt_Table_Key_Hi = pnd_Bt_Table_Id_Key_Hi__R_Field_8.newFieldInGroup("pnd_Bt_Table_Id_Key_Hi_Pnd_Bt_Table_Key_Hi", "#BT-TABLE-KEY-HI", 
            FieldType.STRING, 20);

        pnd_Bt_Table_Id_Key_Hi__R_Field_9 = pnd_Bt_Table_Id_Key_Hi__R_Field_8.newGroupInGroup("pnd_Bt_Table_Id_Key_Hi__R_Field_9", "REDEFINE", pnd_Bt_Table_Id_Key_Hi_Pnd_Bt_Table_Key_Hi);
        pnd_Bt_Table_Id_Key_Hi_Pnd_Bt_Table_Key_Hi_Lit = pnd_Bt_Table_Id_Key_Hi__R_Field_9.newFieldInGroup("pnd_Bt_Table_Id_Key_Hi_Pnd_Bt_Table_Key_Hi_Lit", 
            "#BT-TABLE-KEY-HI-LIT", FieldType.STRING, 15);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_rt.reset();

        localVariables.reset();
        pnd_Bt_Table_Id_Key_Lo.setInitialValue("RT               00");
        pnd_Bt_Table_Id_Key_Hi.setInitialValue("RT               49");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Benn9863() throws Exception
    {
        super("Benn9863");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ERROR-INFO-SUB.##LAST-PROGRAM = *PROGRAM
        pdaBenpda_M.getMsg_Info_Sub().reset();                                                                                                                            //Natural: RESET MSG-INFO-SUB
        pnd_Work_Pnd_Systm_Text.setValue(pdaBena9863.getBena9863_Pnd_Systm_Text());                                                                                       //Natural: ASSIGN #WORK.#SYSTM-TEXT = BENA9863.#SYSTM-TEXT
        pnd_Work_Pnd_Text.setValue(pnd_Work_Pnd_Systm_Text);                                                                                                              //Natural: ASSIGN #WORK.#TEXT = #WORK.#SYSTM-TEXT
                                                                                                                                                                          //Natural: PERFORM TRANSLATE-TEXT
        sub_Translate_Text();
        if (condition(Global.isEscape())) {return;}
        pnd_Work_Pnd_Systm_Text.setValue(pnd_Work_Pnd_Text);                                                                                                              //Natural: ASSIGN #WORK.#SYSTM-TEXT = #WORK.#TEXT
                                                                                                                                                                          //Natural: PERFORM FIND-MATCH
        sub_Find_Match();
        if (condition(Global.isEscape())) {return;}
        //* ***************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-MATCH
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TRANSLATE-TEXT
        //* *******************************
        //*   TRANSLATE ALL LOWER-CASE LETTERS TO UPPER-CASE.
    }
    private void sub_Find_Match() throws Exception                                                                                                                        //Natural: FIND-MATCH
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************
        //*  JW
        READ_RECORD:                                                                                                                                                      //Natural: FOR #T-I 1 99
        for (pnd_T_I.setValue(1); condition(pnd_T_I.lessOrEqual(99)); pnd_T_I.nadd(1))
        {
            //*  JW BT-TABLE-ID NE 'RC'
            if (condition(pdaBena9864.getBena9864_Pnd_Rc_Tab_Key().getValue(pnd_T_I).equals(" ")))                                                                        //Natural: IF #RC-TAB-KEY ( #T-I ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaBena9864.getBena9864_Pnd_Bt_Table_Text_First_Char().getValue(pnd_T_I).notEquals(pnd_Work_Pnd_I_S_Text_First_Char)))                          //Natural: IF #BT-TABLE-TEXT-FIRST-CHAR ( #T-I ) NE #I-S-TEXT-FIRST-CHAR
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  JW
            pnd_Work_Pnd_Text.setValue(pdaBena9864.getBena9864_Pnd_Rc_Tab_Text().getValue(pnd_T_I));                                                                      //Natural: ASSIGN #WORK.#TEXT = #RC-TAB-TEXT ( #T-I )
                                                                                                                                                                          //Natural: PERFORM TRANSLATE-TEXT
            sub_Translate_Text();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_RECORD"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_RECORD"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Work_Pnd_Text.equals(pnd_Work_Pnd_Systm_Text)))                                                                                             //Natural: IF #WORK.#TEXT = #WORK.#SYSTM-TEXT
            {
                pdaBena9863.getBena9863_Pnd_Rltnshp_Cde().setValue(pdaBena9864.getBena9864_Pnd_Rc_Tab_Key().getValue(pnd_T_I));                                           //Natural: ASSIGN BENA9863.#RLTNSHP-CDE = #RC-TAB-KEY ( #T-I )
                pdaBena9863.getBena9863_Pnd_Found().setValue(true);                                                                                                       //Natural: ASSIGN BENA9863.#FOUND = TRUE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  JW 12/18 FROM HERE
        pnd_Bt_Table_Id_Key_Lo_Pnd_Bt_Table_Key_Lo_Lit.setValue(pnd_Work_Pnd_Systm_Text);                                                                                 //Natural: MOVE #WORK.#SYSTM-TEXT TO #BT-TABLE-KEY-LO-LIT #BT-TABLE-KEY-HI-LIT
        pnd_Bt_Table_Id_Key_Hi_Pnd_Bt_Table_Key_Hi_Lit.setValue(pnd_Work_Pnd_Systm_Text);
        vw_rt.createHistogram                                                                                                                                             //Natural: HISTOGRAM ( 1 ) RT BT-TABLE-ID-KEY STARTING #BT-TABLE-ID-KEY-LO THRU #BT-TABLE-ID-KEY-HI
        (
        "R",
        "BT_TABLE_ID_KEY",
        new Wc[] { new Wc("BT_TABLE_ID_KEY", ">=", pnd_Bt_Table_Id_Key_Lo, "And", WcType.WITH) ,
        new Wc("BT_TABLE_ID_KEY", "<=", pnd_Bt_Table_Id_Key_Hi, WcType.WITH) },
        1
        );
        R:
        while (condition(vw_rt.readNextRow("R")))
        {
            pdaBena9863.getBena9863_Pnd_Rltnshp_Cde().setValue(rt_Pnd_Bt_Table_Key_Rc_Cde);                                                                               //Natural: MOVE #BT-TABLE-KEY-RC-CDE TO BENA9863.#RLTNSHP-CDE
            pdaBena9863.getBena9863_Pnd_Found().setValue(true);                                                                                                           //Natural: ASSIGN BENA9863.#FOUND = TRUE
        }                                                                                                                                                                 //Natural: END-HISTOGRAM
        if (Global.isEscape()) return;
        //*  TO HERE JW 12/18
        //*  FIND-MATCH
    }
    private void sub_Translate_Text() throws Exception                                                                                                                    //Natural: TRANSLATE-TEXT
    {
        if (BLNatReinput.isReinput()) return;

        FOR01:                                                                                                                                                            //Natural: FOR #I1 = 1 15
        for (pnd_I1.setValue(1); condition(pnd_I1.lessOrEqual(15)); pnd_I1.nadd(1))
        {
            pnd_J1.setValue(pnd_Work_Pnd_Text_H1.getValue(pnd_I1));                                                                                                       //Natural: MOVE #WORK.#TEXT-H1 ( #I1 ) TO #J1
            if (condition((pnd_J1.greaterOrEqual(129) && pnd_J1.lessOrEqual(137)) || (pnd_J1.greaterOrEqual(145) && pnd_J1.lessOrEqual(153)) || (pnd_J1.greaterOrEqual(162)  //Natural: IF ( #J1 GE 129 AND #J1 LE 137 ) OR ( #J1 GE 145 AND #J1 LE 153 ) OR ( #J1 GE 162 AND #J1 LE 169 )
                && pnd_J1.lessOrEqual(169))))
            {
                pnd_J1.nadd(64);                                                                                                                                          //Natural: ADD 64 TO #J1
                pnd_Work_Pnd_Text_H1.getValue(pnd_I1).setValue(pnd_J1);                                                                                                   //Natural: ASSIGN #WORK.#TEXT-H1 ( #I1 ) = #J1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  CONVERT-FIRST-CHARACTER
    }

    //
}
