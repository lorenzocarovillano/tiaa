/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:06:21 PM
**        * FROM NATURAL SUBPROGRAM : Benn9820
************************************************************
**        * FILE NAME            : Benn9820.java
**        * CLASS NAME           : Benn9820
**        * INSTANCE NAME        : Benn9820
************************************************************
************************************************************************
* PROGRAM  : BENN9820
* SYSTEM   : BENEFICIARY-SYSTEM (INTERFACE BATCH)
* TITLE    : BENE EDITS - CONTRACT AND DESIGNATION LEVEL INTERFACE DATA.
* WRITTEN  : JULY 19, 1999
*      BY  : JULIAN WEBB
************************************************************************
*    DATE      USERID                   DESCRIPTION
* MM/DD/YYYY  XXXXXXXX  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
* 11/05/1999  WEBBJ     NEXT-BUSI-DATE COMPARE FOR EFFECTIVE-DATE EDIT
* 12/03/1999  WEBBJ     XR ETC. TABLES VIA BENA9834 INSTEAD OF READING
*                       TABLE FILE
* 05/16/2000  WEBBJ     ADD CHECK FOR VALID TIAA-CREF-INDICATOR
* 12/14/2000  WEBBJ     NEW EDITS TO ERROR SAME AS, 'See ' ETC.
* 12/18/2000  WEBBJ     NEW EDITS TO #INTERFACE-AS-MOS
* 02/18/2001  WEBBJ     EMBEDDED ALPHAS WILL BE ALLOWED IN POS. 5, 6 AND
*                       7 OF CONTRACT NUMBER
* 10/11/2001  WEBBJ     ALLOW FORMATTED DEFAULT TO ESTATE (FROM MDO.)
* 12/16/2003  FONTOURA  ALLOW INSURANCE TO BE INTERFACED
* 04/07/2005  H.KAKADIA ALLOW ISV TO INTERFACE BENE RECORDS FOR OES
* 07/20/2015  SINHASN   CHANGED THE EDIT FOR EFFECTIVE DATE
*                       FROM 19000101 TO 16000101
* 04/01/2017  DURAND    PIN EXPANSION N7 TO N12
* 09/19/2017  DURAND    BYPASS OMNI FOR RELATIONSHIP TRANSLATION
************************************************************************

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benn9820 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaBena9820 pdaBena9820;
    private PdaBena9801 pdaBena9801;
    private PdaBena9834 pdaBena9834;
    private PdaBenpda_M pdaBenpda_M;
    private PdaBenpda_E pdaBenpda_E;
    private PdaBena9832 pdaBena9832;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_bdi;
    private DbsField bdi_Intrfce_Stts;
    private DbsField bdi_Tiaa_Cref_Ind;
    private DbsField bdi_Seq_Nmbr;
    private DbsField bdi_Rcrd_Crtd_For_Bsnss_Dte;
    private DbsField bdi_Rcrd_Last_Updt_Dte;
    private DbsField bdi_Rcrd_Last_Updt_Tme;
    private DbsField bdi_Rcrd_Last_Updt_Userid;
    private DbsField bdi_Pin_Tiaa_Cntrct;
    private DbsField bdi_Bene_Type;
    private DbsField bdi_Rltn_Cde;
    private DbsField bdi_Bene_Name1;
    private DbsField bdi_Bene_Name2;
    private DbsField bdi_Rltn_Free_Txt;
    private DbsField bdi_Dte_Birth_Trust;
    private DbsField bdi_Ssn_Cde;
    private DbsField bdi_Ssn;
    private DbsField bdi_Prcnt_Frctn_Ind;
    private DbsField bdi_Share_Prcnt;
    private DbsField bdi_Share_Nmrtr;
    private DbsField bdi_Share_Dnmntr;
    private DbsField bdi_Irrvcble_Ind;
    private DbsField bdi_Sttlmnt_Rstrctn;
    private DbsField bdi_Spcl_Txt1;
    private DbsField bdi_Spcl_Txt2;
    private DbsField bdi_Spcl_Txt3;
    private DbsField bdi_Dflt_Estate;
    private DbsField bdi_Mdo_Calc_Bene;
    private DbsField bdi_Bene_Addr1;
    private DbsField bdi_Bene_Addr2;
    private DbsField bdi_Bene_Addr3_City;
    private DbsField bdi_Bene_State;
    private DbsField bdi_Bene_Zip;
    private DbsField bdi_Bene_Phone;
    private DbsField bdi_Bene_Gender;
    private DbsField bdi_Bene_Country;

    private DataAccessProgramView vw_bmi;
    private DbsField bmi_Intrfce_Stts;
    private DbsField bmi_Rcrd_Crtd_For_Bsnss_Dte;
    private DbsField bmi_Tiaa_Cref_Ind;
    private DbsField bmi_Pin_Tiaa_Cntrct;
    private DbsField bmi_Rcrd_Crtd_Dte;
    private DbsField bmi_Rcrd_Crtd_Tme;
    private DbsField bmi_Rcrd_Last_Updt_Dte;
    private DbsField bmi_Rcrd_Last_Updt_Tme;
    private DbsField bmi_Rcrd_Last_Updt_Userid;
    private DbsField bmi_Count_Castmos_Txt_Group;

    private DbsGroup bmi_Mos_Txt_Group;
    private DbsField bmi_Mos_Txt;
    private DbsField pnd_I1;

    private DbsGroup pnd_Desig_Tab;
    private DbsField pnd_Desig_Tab_Pnd_Bene_Type;
    private DbsField pnd_Desig_Tab_Pnd_Rltn_Cde;
    private DbsField pnd_Desig_Tab_Pnd_Bene_Name1;
    private DbsField pnd_Desig_Tab_Pnd_Bene_Name2;
    private DbsField pnd_Desig_Tab_Pnd_Rltn_Free_Txt;
    private DbsField pnd_Desig_Tab_Pnd_Dte_Birth_Trust;

    private DbsGroup pnd_Desig_Tab__R_Field_1;
    private DbsField pnd_Desig_Tab_Pnd_Dte_Birth_Trust_N;
    private DbsField pnd_Desig_Tab_Pnd_Ssn_Cde;
    private DbsField pnd_Desig_Tab_Pnd_Ssn;

    private DbsGroup pnd_Desig_Tab__R_Field_2;
    private DbsField pnd_Desig_Tab_Pnd_Ssn_N;
    private DbsField pnd_Desig_Tab_Pnd_Prcnt_Frctn_Ind;
    private DbsField pnd_Desig_Tab_Pnd_Share_Prcnt;
    private DbsField pnd_Desig_Tab_Pnd_Share_Nmrtr;
    private DbsField pnd_Desig_Tab_Pnd_Share_Dnmntr;
    private DbsField pnd_Desig_Tab_Pnd_Irrvcbl_Ind;
    private DbsField pnd_Desig_Tab_Pnd_Sttlmnt_Rstrctn;
    private DbsField pnd_Desig_Tab_Pnd_Spcl_Txt1;
    private DbsField pnd_Desig_Tab_Pnd_Spcl_Txt2;
    private DbsField pnd_Desig_Tab_Pnd_Spcl_Txt3;
    private DbsField pnd_Desig_Tab_Pnd_Dflt_Estate;
    private DbsField pnd_Im_Ind;
    private DbsField pnd_Io_Ind;
    private DbsField pnd_I;
    private DbsField pnd_N;
    private DbsField pnd_J1;
    private DbsField pnd_K1;
    private DbsField pnd_Intrfce_Super_2;

    private DbsGroup pnd_Intrfce_Super_2__R_Field_3;
    private DbsField pnd_Intrfce_Super_2_Pnd_Intrfce_Stts;
    private DbsField pnd_Intrfce_Super_2_Pnd_Rcrd_Crtd_For_Bsnss_Dte;
    private DbsField pnd_Intrfce_Super_2_Pnd_Pin_Tiaa_Cntrct;
    private DbsField pnd_Intrfce_Super_2_Pnd_Tiaa_Cref_Ind;
    private DbsField pnd_Intrfce_Super_4;

    private DbsGroup pnd_Intrfce_Super_4__R_Field_4;
    private DbsField pnd_Intrfce_Super_4_Pnd_Intrfce_Stts;
    private DbsField pnd_Intrfce_Super_4_Pnd_Rcrd_Crtd_For_Bsnss_Dte;
    private DbsField pnd_Intrfce_Super_4_Pnd_Pin_Tiaa_Cntrct;
    private DbsField pnd_Intrfce_Super_4_Pnd_Tiaa_Cref_Ind;
    private DbsField pnd_Intrfce_Super_4_Pnd_Seq_Nmbr;
    private DbsField pnd_Y_N_Tab;

    private DbsGroup pnd_Y_N_Tab__R_Field_5;
    private DbsField pnd_Y_N_Tab_Pnd_Y_N_Or_Blank;
    private DbsField pnd_Cntrct_Type_Values_Tab;

    private DbsGroup pnd_Cntrct_Type_Values_Tab__R_Field_6;
    private DbsField pnd_Cntrct_Type_Values_Tab_Pnd_Cntrct_Type_Values;
    private DbsField pnd_Pymnt_Child_Dcsd_Tab;

    private DbsGroup pnd_Pymnt_Child_Dcsd_Tab__R_Field_7;
    private DbsField pnd_Pymnt_Child_Dcsd_Tab_Pnd_Pymnt_Child_Dcsd_Values;
    private DbsField pnd_Num_Benes_P;
    private DbsField pnd_Num_Benes_C;
    private DbsField pnd_Error_Cnt;
    private DbsField pnd_Error_Lit;

    private DbsGroup pnd_Confirm_Calc_Tots;
    private DbsField pnd_Confirm_Calc_Tots_Pnd_Res;

    private DbsGroup pnd_Confirm_Calc_Tots__R_Field_8;
    private DbsField pnd_Confirm_Calc_Tots_Pnd_Res_26;
    private DbsField pnd_Confirm_Calc_Tots_Pnd_Res_Decimal;
    private DbsField pnd_Confirm_Calc_Tots_Pnd_P_Perc_Tot;
    private DbsField pnd_Confirm_Calc_Tots_Pnd_C_Perc_Tot;
    private DbsField pnd_Confirm_Calc_Tots_Pnd_P_Ntor_Tot;
    private DbsField pnd_Confirm_Calc_Tots_Pnd_P_Dtor_Tot;
    private DbsField pnd_Confirm_Calc_Tots_Pnd_C_Ntor_Tot;
    private DbsField pnd_Confirm_Calc_Tots_Pnd_C_Dtor_Tot;
    private DbsField pnd_Confirm_Calc_Tots_Pnd_Hold_Ntor_Tot_1;
    private DbsField pnd_Confirm_Calc_Tots_Pnd_Hold_Ntor_Tot_2;
    private DbsField pnd_Confirm_Calc_Tots_Pnd_Comp_Lit;
    private DbsField pnd_1st_C_Bene;
    private DbsField pnd_No_C_Benes_Yet;
    private DbsField pnd_Num_Poss_Mos_Lines;
    private DbsField pnd_Xr_Rel_Code_Found;
    private DbsField pnd_Ss_Code_Found;
    private DbsField pnd_Sr_Code_Found;
    private DbsField pnd_T_I;
    private DbsField pnd_Same_As;

    private DbsGroup pnd_Same_As__R_Field_9;
    private DbsField pnd_Same_As_Pnd_Same_As_Txt;

    private DbsGroup pnd_Same_As__R_Field_10;
    private DbsField pnd_Same_As_Pnd_See_Txt;
    private DbsField pnd_Test_Lit;

    private DbsGroup pnd_Test_Lit__R_Field_11;
    private DbsField pnd_Test_Lit_Pnd_Test_1st_Byte;
    private DbsField pnd_Test_Lit_Pnd_Test_Rest_Byte;
    private DbsField pnd_Spcl_Txt_Indexed;
    private DbsField pnd_St;
    private DbsField pnd_Sep_Tab;
    private DbsField pnd_Name_Only_This_Bene;
    private DbsField pnd_1st_Primary_Name_Only;
    private DbsField pnd_1st_C_Bene_Name_Only;
    private DbsField pnd_Inval_Non_Acis_Rel;
    private DbsField pnd_Same_Hold;
    private DbsField pnd_1stc;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaBena9832 = new PdaBena9832(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaBena9820 = new PdaBena9820(parameters);
        pdaBena9801 = new PdaBena9801(parameters);
        pdaBena9834 = new PdaBena9834(parameters);
        pdaBenpda_M = new PdaBenpda_M(parameters);
        pdaBenpda_E = new PdaBenpda_E(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        vw_bdi = new DataAccessProgramView(new NameInfo("vw_bdi", "BDI"), "BENE_DESIGNATION_INTERFACE_12", "BENE_DSGN_INTFCE");
        bdi_Intrfce_Stts = vw_bdi.getRecord().newFieldInGroup("bdi_Intrfce_Stts", "INTRFCE-STTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, "INTRFCE_STTS");
        bdi_Intrfce_Stts.setDdmHeader("INTRFCE/STATUS");
        bdi_Tiaa_Cref_Ind = vw_bdi.getRecord().newFieldInGroup("bdi_Tiaa_Cref_Ind", "TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIAA_CREF_IND");
        bdi_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bdi_Seq_Nmbr = vw_bdi.getRecord().newFieldInGroup("bdi_Seq_Nmbr", "SEQ-NMBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "SEQ_NMBR");
        bdi_Seq_Nmbr.setDdmHeader("SEQ/NMBR");
        bdi_Rcrd_Crtd_For_Bsnss_Dte = vw_bdi.getRecord().newFieldInGroup("bdi_Rcrd_Crtd_For_Bsnss_Dte", "RCRD-CRTD-FOR-BSNSS-DTE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "RCRD_CRTD_FOR_BSNSS_DTE");
        bdi_Rcrd_Crtd_For_Bsnss_Dte.setDdmHeader("RCRD CRTD/FOR/BSNSS DTE");
        bdi_Rcrd_Last_Updt_Dte = vw_bdi.getRecord().newFieldInGroup("bdi_Rcrd_Last_Updt_Dte", "RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_DTE");
        bdi_Rcrd_Last_Updt_Dte.setDdmHeader("RECORD/LAST UPDT/DATE");
        bdi_Rcrd_Last_Updt_Tme = vw_bdi.getRecord().newFieldInGroup("bdi_Rcrd_Last_Updt_Tme", "RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_TME");
        bdi_Rcrd_Last_Updt_Tme.setDdmHeader("RECORD/LAST UPDT/TIME");
        bdi_Rcrd_Last_Updt_Userid = vw_bdi.getRecord().newFieldInGroup("bdi_Rcrd_Last_Updt_Userid", "RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_USERID");
        bdi_Rcrd_Last_Updt_Userid.setDdmHeader("RECORD/LAST UPDT/USER ID");
        bdi_Pin_Tiaa_Cntrct = vw_bdi.getRecord().newFieldInGroup("bdi_Pin_Tiaa_Cntrct", "PIN-TIAA-CNTRCT", FieldType.STRING, 22, RepeatingFieldStrategy.None, 
            "PIN_TIAA_CNTRCT");
        bdi_Pin_Tiaa_Cntrct.setDdmHeader("PIN/TIAA/CONTRACT");
        bdi_Bene_Type = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Type", "BENE-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BENE_TYPE");
        bdi_Bene_Type.setDdmHeader("BENE/TYPE");
        bdi_Rltn_Cde = vw_bdi.getRecord().newFieldInGroup("bdi_Rltn_Cde", "RLTN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "RLTN_CDE");
        bdi_Rltn_Cde.setDdmHeader("RELATION/CODE");
        bdi_Bene_Name1 = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Name1", "BENE-NAME1", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BENE_NAME1");
        bdi_Bene_Name1.setDdmHeader("BENEFICIARY/NAME 1");
        bdi_Bene_Name2 = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Name2", "BENE-NAME2", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BENE_NAME2");
        bdi_Bene_Name2.setDdmHeader("BENEFICIARY/NAME 2");
        bdi_Rltn_Free_Txt = vw_bdi.getRecord().newFieldInGroup("bdi_Rltn_Free_Txt", "RLTN-FREE-TXT", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "RLTN_FREE_TXT");
        bdi_Rltn_Free_Txt.setDdmHeader("OTHER/RELATION");
        bdi_Dte_Birth_Trust = vw_bdi.getRecord().newFieldInGroup("bdi_Dte_Birth_Trust", "DTE-BIRTH-TRUST", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "DTE_BIRTH_TRUST");
        bdi_Dte_Birth_Trust.setDdmHeader("DOB/DOT");
        bdi_Ssn_Cde = vw_bdi.getRecord().newFieldInGroup("bdi_Ssn_Cde", "SSN-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "SSN_CDE");
        bdi_Ssn_Cde.setDdmHeader("SSN/CODE");
        bdi_Ssn = vw_bdi.getRecord().newFieldInGroup("bdi_Ssn", "SSN", FieldType.STRING, 9, RepeatingFieldStrategy.None, "SSN");
        bdi_Ssn.setDdmHeader("SSN");
        bdi_Prcnt_Frctn_Ind = vw_bdi.getRecord().newFieldInGroup("bdi_Prcnt_Frctn_Ind", "PRCNT-FRCTN-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PRCNT_FRCTN_IND");
        bdi_Prcnt_Frctn_Ind.setDdmHeader("PRCNT/FRCTN/IND");
        bdi_Share_Prcnt = vw_bdi.getRecord().newFieldInGroup("bdi_Share_Prcnt", "SHARE-PRCNT", FieldType.NUMERIC, 5, 2, RepeatingFieldStrategy.None, "SHARE_PRCNT");
        bdi_Share_Prcnt.setDdmHeader("SHARE/PERCENT");
        bdi_Share_Nmrtr = vw_bdi.getRecord().newFieldInGroup("bdi_Share_Nmrtr", "SHARE-NMRTR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "SHARE_NMRTR");
        bdi_Share_Nmrtr.setDdmHeader("SHARE/NUMERATOR");
        bdi_Share_Dnmntr = vw_bdi.getRecord().newFieldInGroup("bdi_Share_Dnmntr", "SHARE-DNMNTR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "SHARE_DNMNTR");
        bdi_Share_Dnmntr.setDdmHeader("SHARE/DENOMINATOR");
        bdi_Irrvcble_Ind = vw_bdi.getRecord().newFieldInGroup("bdi_Irrvcble_Ind", "IRRVCBLE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "IRRVCBLE_IND");
        bdi_Irrvcble_Ind.setDdmHeader("IRVCBL/IND");
        bdi_Sttlmnt_Rstrctn = vw_bdi.getRecord().newFieldInGroup("bdi_Sttlmnt_Rstrctn", "STTLMNT-RSTRCTN", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "STTLMNT_RSTRCTN");
        bdi_Sttlmnt_Rstrctn.setDdmHeader("SETTLEMENT/RESTRICTION");
        bdi_Spcl_Txt1 = vw_bdi.getRecord().newFieldInGroup("bdi_Spcl_Txt1", "SPCL-TXT1", FieldType.STRING, 72, RepeatingFieldStrategy.None, "SPCL_TXT1");
        bdi_Spcl_Txt1.setDdmHeader("SPECIAL TEXT 1");
        bdi_Spcl_Txt2 = vw_bdi.getRecord().newFieldInGroup("bdi_Spcl_Txt2", "SPCL-TXT2", FieldType.STRING, 72, RepeatingFieldStrategy.None, "SPCL_TXT2");
        bdi_Spcl_Txt2.setDdmHeader("SPECIAL TEXT 2");
        bdi_Spcl_Txt3 = vw_bdi.getRecord().newFieldInGroup("bdi_Spcl_Txt3", "SPCL-TXT3", FieldType.STRING, 72, RepeatingFieldStrategy.None, "SPCL_TXT3");
        bdi_Spcl_Txt3.setDdmHeader("SPECIAL TEXT 3");
        bdi_Dflt_Estate = vw_bdi.getRecord().newFieldInGroup("bdi_Dflt_Estate", "DFLT-ESTATE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "DFLT_ESTATE");
        bdi_Dflt_Estate.setDdmHeader("DEFAULT/ESTATE");
        bdi_Mdo_Calc_Bene = vw_bdi.getRecord().newFieldInGroup("bdi_Mdo_Calc_Bene", "MDO-CALC-BENE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "MDO_CALC_BENE");
        bdi_Mdo_Calc_Bene.setDdmHeader("MDO/CALC/BENE");
        bdi_Bene_Addr1 = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Addr1", "BENE-ADDR1", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BENE_ADDR1");
        bdi_Bene_Addr2 = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Addr2", "BENE-ADDR2", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BENE_ADDR2");
        bdi_Bene_Addr3_City = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Addr3_City", "BENE-ADDR3-CITY", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "BENE_ADDR3_CITY");
        bdi_Bene_State = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_State", "BENE-STATE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "BENE_STATE");
        bdi_Bene_Zip = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Zip", "BENE-ZIP", FieldType.STRING, 10, RepeatingFieldStrategy.None, "BENE_ZIP");
        bdi_Bene_Phone = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Phone", "BENE-PHONE", FieldType.STRING, 20, RepeatingFieldStrategy.None, "BENE_PHONE");
        bdi_Bene_Gender = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Gender", "BENE-GENDER", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BENE_GENDER");
        bdi_Bene_Country = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Country", "BENE-COUNTRY", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BENE_COUNTRY");
        registerRecord(vw_bdi);

        vw_bmi = new DataAccessProgramView(new NameInfo("vw_bmi", "BMI"), "BENE_MOS_INTERFACE_12", "BENE_MOS_INTFCE", DdmPeriodicGroups.getInstance().getGroups("BENE_MOS_INTERFACE_12"));
        bmi_Intrfce_Stts = vw_bmi.getRecord().newFieldInGroup("bmi_Intrfce_Stts", "INTRFCE-STTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, "INTRFCE_STTS");
        bmi_Intrfce_Stts.setDdmHeader("INTRFCE/STATUS");
        bmi_Rcrd_Crtd_For_Bsnss_Dte = vw_bmi.getRecord().newFieldInGroup("bmi_Rcrd_Crtd_For_Bsnss_Dte", "RCRD-CRTD-FOR-BSNSS-DTE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "RCRD_CRTD_FOR_BSNSS_DTE");
        bmi_Rcrd_Crtd_For_Bsnss_Dte.setDdmHeader("RCRD CRTD/FOR/BSNSS DTE");
        bmi_Tiaa_Cref_Ind = vw_bmi.getRecord().newFieldInGroup("bmi_Tiaa_Cref_Ind", "TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIAA_CREF_IND");
        bmi_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bmi_Pin_Tiaa_Cntrct = vw_bmi.getRecord().newFieldInGroup("bmi_Pin_Tiaa_Cntrct", "PIN-TIAA-CNTRCT", FieldType.STRING, 22, RepeatingFieldStrategy.None, 
            "PIN_TIAA_CNTRCT");
        bmi_Pin_Tiaa_Cntrct.setDdmHeader("PIN/TIAA/CONTRACT");
        bmi_Rcrd_Crtd_Dte = vw_bmi.getRecord().newFieldInGroup("bmi_Rcrd_Crtd_Dte", "RCRD-CRTD-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_CRTD_DTE");
        bmi_Rcrd_Crtd_Dte.setDdmHeader("RECORD/CREATED/DATE");
        bmi_Rcrd_Crtd_Tme = vw_bmi.getRecord().newFieldInGroup("bmi_Rcrd_Crtd_Tme", "RCRD-CRTD-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "RCRD_CRTD_TME");
        bmi_Rcrd_Crtd_Tme.setDdmHeader("RECORD/CREATED/TIME");
        bmi_Rcrd_Last_Updt_Dte = vw_bmi.getRecord().newFieldInGroup("bmi_Rcrd_Last_Updt_Dte", "RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_DTE");
        bmi_Rcrd_Last_Updt_Dte.setDdmHeader("RECORD/LAST UPDT/DATE");
        bmi_Rcrd_Last_Updt_Tme = vw_bmi.getRecord().newFieldInGroup("bmi_Rcrd_Last_Updt_Tme", "RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_TME");
        bmi_Rcrd_Last_Updt_Tme.setDdmHeader("RECORD/LAST UPDT/TIME");
        bmi_Rcrd_Last_Updt_Userid = vw_bmi.getRecord().newFieldInGroup("bmi_Rcrd_Last_Updt_Userid", "RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_USERID");
        bmi_Rcrd_Last_Updt_Userid.setDdmHeader("RECORD/LAST UPDT/USER ID");
        bmi_Count_Castmos_Txt_Group = vw_bmi.getRecord().newFieldInGroup("bmi_Count_Castmos_Txt_Group", "C*MOS-TXT-GROUP", RepeatingFieldStrategy.CAsteriskVariable, 
            "BENE_MOS_INTFCE_MOS_TXT_GROUP");

        bmi_Mos_Txt_Group = vw_bmi.getRecord().newGroupInGroup("bmi_Mos_Txt_Group", "MOS-TXT-GROUP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "BENE_MOS_INTFCE_MOS_TXT_GROUP");
        bmi_Mos_Txt = bmi_Mos_Txt_Group.newFieldArrayInGroup("bmi_Mos_Txt", "MOS-TXT", FieldType.STRING, 72, new DbsArrayController(1, 60) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "MOS_TXT", "BENE_MOS_INTFCE_MOS_TXT_GROUP");
        bmi_Mos_Txt.setDdmHeader("MOS TEXT");
        registerRecord(vw_bmi);

        pnd_I1 = localVariables.newFieldInRecord("pnd_I1", "#I1", FieldType.PACKED_DECIMAL, 3);

        pnd_Desig_Tab = localVariables.newGroupArrayInRecord("pnd_Desig_Tab", "#DESIG-TAB", new DbsArrayController(1, 30));
        pnd_Desig_Tab_Pnd_Bene_Type = pnd_Desig_Tab.newFieldInGroup("pnd_Desig_Tab_Pnd_Bene_Type", "#BENE-TYPE", FieldType.STRING, 1);
        pnd_Desig_Tab_Pnd_Rltn_Cde = pnd_Desig_Tab.newFieldInGroup("pnd_Desig_Tab_Pnd_Rltn_Cde", "#RLTN-CDE", FieldType.STRING, 2);
        pnd_Desig_Tab_Pnd_Bene_Name1 = pnd_Desig_Tab.newFieldInGroup("pnd_Desig_Tab_Pnd_Bene_Name1", "#BENE-NAME1", FieldType.STRING, 35);
        pnd_Desig_Tab_Pnd_Bene_Name2 = pnd_Desig_Tab.newFieldInGroup("pnd_Desig_Tab_Pnd_Bene_Name2", "#BENE-NAME2", FieldType.STRING, 35);
        pnd_Desig_Tab_Pnd_Rltn_Free_Txt = pnd_Desig_Tab.newFieldInGroup("pnd_Desig_Tab_Pnd_Rltn_Free_Txt", "#RLTN-FREE-TXT", FieldType.STRING, 15);
        pnd_Desig_Tab_Pnd_Dte_Birth_Trust = pnd_Desig_Tab.newFieldInGroup("pnd_Desig_Tab_Pnd_Dte_Birth_Trust", "#DTE-BIRTH-TRUST", FieldType.STRING, 8);

        pnd_Desig_Tab__R_Field_1 = pnd_Desig_Tab.newGroupInGroup("pnd_Desig_Tab__R_Field_1", "REDEFINE", pnd_Desig_Tab_Pnd_Dte_Birth_Trust);
        pnd_Desig_Tab_Pnd_Dte_Birth_Trust_N = pnd_Desig_Tab__R_Field_1.newFieldInGroup("pnd_Desig_Tab_Pnd_Dte_Birth_Trust_N", "#DTE-BIRTH-TRUST-N", FieldType.NUMERIC, 
            8);
        pnd_Desig_Tab_Pnd_Ssn_Cde = pnd_Desig_Tab.newFieldInGroup("pnd_Desig_Tab_Pnd_Ssn_Cde", "#SSN-CDE", FieldType.STRING, 1);
        pnd_Desig_Tab_Pnd_Ssn = pnd_Desig_Tab.newFieldInGroup("pnd_Desig_Tab_Pnd_Ssn", "#SSN", FieldType.STRING, 9);

        pnd_Desig_Tab__R_Field_2 = pnd_Desig_Tab.newGroupInGroup("pnd_Desig_Tab__R_Field_2", "REDEFINE", pnd_Desig_Tab_Pnd_Ssn);
        pnd_Desig_Tab_Pnd_Ssn_N = pnd_Desig_Tab__R_Field_2.newFieldInGroup("pnd_Desig_Tab_Pnd_Ssn_N", "#SSN-N", FieldType.NUMERIC, 9);
        pnd_Desig_Tab_Pnd_Prcnt_Frctn_Ind = pnd_Desig_Tab.newFieldInGroup("pnd_Desig_Tab_Pnd_Prcnt_Frctn_Ind", "#PRCNT-FRCTN-IND", FieldType.STRING, 1);
        pnd_Desig_Tab_Pnd_Share_Prcnt = pnd_Desig_Tab.newFieldInGroup("pnd_Desig_Tab_Pnd_Share_Prcnt", "#SHARE-PRCNT", FieldType.NUMERIC, 5, 2);
        pnd_Desig_Tab_Pnd_Share_Nmrtr = pnd_Desig_Tab.newFieldInGroup("pnd_Desig_Tab_Pnd_Share_Nmrtr", "#SHARE-NMRTR", FieldType.NUMERIC, 3);
        pnd_Desig_Tab_Pnd_Share_Dnmntr = pnd_Desig_Tab.newFieldInGroup("pnd_Desig_Tab_Pnd_Share_Dnmntr", "#SHARE-DNMNTR", FieldType.NUMERIC, 3);
        pnd_Desig_Tab_Pnd_Irrvcbl_Ind = pnd_Desig_Tab.newFieldInGroup("pnd_Desig_Tab_Pnd_Irrvcbl_Ind", "#IRRVCBL-IND", FieldType.STRING, 1);
        pnd_Desig_Tab_Pnd_Sttlmnt_Rstrctn = pnd_Desig_Tab.newFieldInGroup("pnd_Desig_Tab_Pnd_Sttlmnt_Rstrctn", "#STTLMNT-RSTRCTN", FieldType.STRING, 1);
        pnd_Desig_Tab_Pnd_Spcl_Txt1 = pnd_Desig_Tab.newFieldInGroup("pnd_Desig_Tab_Pnd_Spcl_Txt1", "#SPCL-TXT1", FieldType.STRING, 72);
        pnd_Desig_Tab_Pnd_Spcl_Txt2 = pnd_Desig_Tab.newFieldInGroup("pnd_Desig_Tab_Pnd_Spcl_Txt2", "#SPCL-TXT2", FieldType.STRING, 72);
        pnd_Desig_Tab_Pnd_Spcl_Txt3 = pnd_Desig_Tab.newFieldInGroup("pnd_Desig_Tab_Pnd_Spcl_Txt3", "#SPCL-TXT3", FieldType.STRING, 72);
        pnd_Desig_Tab_Pnd_Dflt_Estate = pnd_Desig_Tab.newFieldInGroup("pnd_Desig_Tab_Pnd_Dflt_Estate", "#DFLT-ESTATE", FieldType.STRING, 1);
        pnd_Im_Ind = localVariables.newFieldInRecord("pnd_Im_Ind", "#IM-IND", FieldType.STRING, 1);
        pnd_Io_Ind = localVariables.newFieldInRecord("pnd_Io_Ind", "#IO-IND", FieldType.STRING, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_N = localVariables.newFieldInRecord("pnd_N", "#N", FieldType.PACKED_DECIMAL, 3);
        pnd_J1 = localVariables.newFieldInRecord("pnd_J1", "#J1", FieldType.PACKED_DECIMAL, 3);
        pnd_K1 = localVariables.newFieldInRecord("pnd_K1", "#K1", FieldType.PACKED_DECIMAL, 3);
        pnd_Intrfce_Super_2 = localVariables.newFieldInRecord("pnd_Intrfce_Super_2", "#INTRFCE-SUPER-2", FieldType.STRING, 32);

        pnd_Intrfce_Super_2__R_Field_3 = localVariables.newGroupInRecord("pnd_Intrfce_Super_2__R_Field_3", "REDEFINE", pnd_Intrfce_Super_2);
        pnd_Intrfce_Super_2_Pnd_Intrfce_Stts = pnd_Intrfce_Super_2__R_Field_3.newFieldInGroup("pnd_Intrfce_Super_2_Pnd_Intrfce_Stts", "#INTRFCE-STTS", 
            FieldType.STRING, 1);
        pnd_Intrfce_Super_2_Pnd_Rcrd_Crtd_For_Bsnss_Dte = pnd_Intrfce_Super_2__R_Field_3.newFieldInGroup("pnd_Intrfce_Super_2_Pnd_Rcrd_Crtd_For_Bsnss_Dte", 
            "#RCRD-CRTD-FOR-BSNSS-DTE", FieldType.STRING, 8);
        pnd_Intrfce_Super_2_Pnd_Pin_Tiaa_Cntrct = pnd_Intrfce_Super_2__R_Field_3.newFieldInGroup("pnd_Intrfce_Super_2_Pnd_Pin_Tiaa_Cntrct", "#PIN-TIAA-CNTRCT", 
            FieldType.STRING, 22);
        pnd_Intrfce_Super_2_Pnd_Tiaa_Cref_Ind = pnd_Intrfce_Super_2__R_Field_3.newFieldInGroup("pnd_Intrfce_Super_2_Pnd_Tiaa_Cref_Ind", "#TIAA-CREF-IND", 
            FieldType.STRING, 1);
        pnd_Intrfce_Super_4 = localVariables.newFieldInRecord("pnd_Intrfce_Super_4", "#INTRFCE-SUPER-4", FieldType.STRING, 35);

        pnd_Intrfce_Super_4__R_Field_4 = localVariables.newGroupInRecord("pnd_Intrfce_Super_4__R_Field_4", "REDEFINE", pnd_Intrfce_Super_4);
        pnd_Intrfce_Super_4_Pnd_Intrfce_Stts = pnd_Intrfce_Super_4__R_Field_4.newFieldInGroup("pnd_Intrfce_Super_4_Pnd_Intrfce_Stts", "#INTRFCE-STTS", 
            FieldType.STRING, 1);
        pnd_Intrfce_Super_4_Pnd_Rcrd_Crtd_For_Bsnss_Dte = pnd_Intrfce_Super_4__R_Field_4.newFieldInGroup("pnd_Intrfce_Super_4_Pnd_Rcrd_Crtd_For_Bsnss_Dte", 
            "#RCRD-CRTD-FOR-BSNSS-DTE", FieldType.STRING, 8);
        pnd_Intrfce_Super_4_Pnd_Pin_Tiaa_Cntrct = pnd_Intrfce_Super_4__R_Field_4.newFieldInGroup("pnd_Intrfce_Super_4_Pnd_Pin_Tiaa_Cntrct", "#PIN-TIAA-CNTRCT", 
            FieldType.STRING, 22);
        pnd_Intrfce_Super_4_Pnd_Tiaa_Cref_Ind = pnd_Intrfce_Super_4__R_Field_4.newFieldInGroup("pnd_Intrfce_Super_4_Pnd_Tiaa_Cref_Ind", "#TIAA-CREF-IND", 
            FieldType.STRING, 1);
        pnd_Intrfce_Super_4_Pnd_Seq_Nmbr = pnd_Intrfce_Super_4__R_Field_4.newFieldInGroup("pnd_Intrfce_Super_4_Pnd_Seq_Nmbr", "#SEQ-NMBR", FieldType.STRING, 
            3);
        pnd_Y_N_Tab = localVariables.newFieldInRecord("pnd_Y_N_Tab", "#Y-N-TAB", FieldType.STRING, 3);

        pnd_Y_N_Tab__R_Field_5 = localVariables.newGroupInRecord("pnd_Y_N_Tab__R_Field_5", "REDEFINE", pnd_Y_N_Tab);
        pnd_Y_N_Tab_Pnd_Y_N_Or_Blank = pnd_Y_N_Tab__R_Field_5.newFieldArrayInGroup("pnd_Y_N_Tab_Pnd_Y_N_Or_Blank", "#Y-N-OR-BLANK", FieldType.STRING, 
            1, new DbsArrayController(1, 3));
        pnd_Cntrct_Type_Values_Tab = localVariables.newFieldInRecord("pnd_Cntrct_Type_Values_Tab", "#CNTRCT-TYPE-VALUES-TAB", FieldType.STRING, 3);

        pnd_Cntrct_Type_Values_Tab__R_Field_6 = localVariables.newGroupInRecord("pnd_Cntrct_Type_Values_Tab__R_Field_6", "REDEFINE", pnd_Cntrct_Type_Values_Tab);
        pnd_Cntrct_Type_Values_Tab_Pnd_Cntrct_Type_Values = pnd_Cntrct_Type_Values_Tab__R_Field_6.newFieldArrayInGroup("pnd_Cntrct_Type_Values_Tab_Pnd_Cntrct_Type_Values", 
            "#CNTRCT-TYPE-VALUES", FieldType.STRING, 1, new DbsArrayController(1, 3));
        pnd_Pymnt_Child_Dcsd_Tab = localVariables.newFieldInRecord("pnd_Pymnt_Child_Dcsd_Tab", "#PYMNT-CHILD-DCSD-TAB", FieldType.STRING, 7);

        pnd_Pymnt_Child_Dcsd_Tab__R_Field_7 = localVariables.newGroupInRecord("pnd_Pymnt_Child_Dcsd_Tab__R_Field_7", "REDEFINE", pnd_Pymnt_Child_Dcsd_Tab);
        pnd_Pymnt_Child_Dcsd_Tab_Pnd_Pymnt_Child_Dcsd_Values = pnd_Pymnt_Child_Dcsd_Tab__R_Field_7.newFieldArrayInGroup("pnd_Pymnt_Child_Dcsd_Tab_Pnd_Pymnt_Child_Dcsd_Values", 
            "#PYMNT-CHILD-DCSD-VALUES", FieldType.STRING, 1, new DbsArrayController(1, 7));
        pnd_Num_Benes_P = localVariables.newFieldInRecord("pnd_Num_Benes_P", "#NUM-BENES-P", FieldType.NUMERIC, 2);
        pnd_Num_Benes_C = localVariables.newFieldInRecord("pnd_Num_Benes_C", "#NUM-BENES-C", FieldType.NUMERIC, 2);
        pnd_Error_Cnt = localVariables.newFieldInRecord("pnd_Error_Cnt", "#ERROR-CNT", FieldType.NUMERIC, 4);
        pnd_Error_Lit = localVariables.newFieldInRecord("pnd_Error_Lit", "#ERROR-LIT", FieldType.STRING, 10);

        pnd_Confirm_Calc_Tots = localVariables.newGroupInRecord("pnd_Confirm_Calc_Tots", "#CONFIRM-CALC-TOTS");
        pnd_Confirm_Calc_Tots_Pnd_Res = pnd_Confirm_Calc_Tots.newFieldInGroup("pnd_Confirm_Calc_Tots_Pnd_Res", "#RES", FieldType.NUMERIC, 29, 3);

        pnd_Confirm_Calc_Tots__R_Field_8 = pnd_Confirm_Calc_Tots.newGroupInGroup("pnd_Confirm_Calc_Tots__R_Field_8", "REDEFINE", pnd_Confirm_Calc_Tots_Pnd_Res);
        pnd_Confirm_Calc_Tots_Pnd_Res_26 = pnd_Confirm_Calc_Tots__R_Field_8.newFieldInGroup("pnd_Confirm_Calc_Tots_Pnd_Res_26", "#RES-26", FieldType.NUMERIC, 
            26);
        pnd_Confirm_Calc_Tots_Pnd_Res_Decimal = pnd_Confirm_Calc_Tots__R_Field_8.newFieldInGroup("pnd_Confirm_Calc_Tots_Pnd_Res_Decimal", "#RES-DECIMAL", 
            FieldType.NUMERIC, 3);
        pnd_Confirm_Calc_Tots_Pnd_P_Perc_Tot = pnd_Confirm_Calc_Tots.newFieldInGroup("pnd_Confirm_Calc_Tots_Pnd_P_Perc_Tot", "#P-PERC-TOT", FieldType.NUMERIC, 
            7, 2);
        pnd_Confirm_Calc_Tots_Pnd_C_Perc_Tot = pnd_Confirm_Calc_Tots.newFieldInGroup("pnd_Confirm_Calc_Tots_Pnd_C_Perc_Tot", "#C-PERC-TOT", FieldType.NUMERIC, 
            5, 2);
        pnd_Confirm_Calc_Tots_Pnd_P_Ntor_Tot = pnd_Confirm_Calc_Tots.newFieldInGroup("pnd_Confirm_Calc_Tots_Pnd_P_Ntor_Tot", "#P-NTOR-TOT", FieldType.PACKED_DECIMAL, 
            29);
        pnd_Confirm_Calc_Tots_Pnd_P_Dtor_Tot = pnd_Confirm_Calc_Tots.newFieldInGroup("pnd_Confirm_Calc_Tots_Pnd_P_Dtor_Tot", "#P-DTOR-TOT", FieldType.PACKED_DECIMAL, 
            29);
        pnd_Confirm_Calc_Tots_Pnd_C_Ntor_Tot = pnd_Confirm_Calc_Tots.newFieldInGroup("pnd_Confirm_Calc_Tots_Pnd_C_Ntor_Tot", "#C-NTOR-TOT", FieldType.PACKED_DECIMAL, 
            29);
        pnd_Confirm_Calc_Tots_Pnd_C_Dtor_Tot = pnd_Confirm_Calc_Tots.newFieldInGroup("pnd_Confirm_Calc_Tots_Pnd_C_Dtor_Tot", "#C-DTOR-TOT", FieldType.PACKED_DECIMAL, 
            29);
        pnd_Confirm_Calc_Tots_Pnd_Hold_Ntor_Tot_1 = pnd_Confirm_Calc_Tots.newFieldInGroup("pnd_Confirm_Calc_Tots_Pnd_Hold_Ntor_Tot_1", "#HOLD-NTOR-TOT-1", 
            FieldType.PACKED_DECIMAL, 29);
        pnd_Confirm_Calc_Tots_Pnd_Hold_Ntor_Tot_2 = pnd_Confirm_Calc_Tots.newFieldInGroup("pnd_Confirm_Calc_Tots_Pnd_Hold_Ntor_Tot_2", "#HOLD-NTOR-TOT-2", 
            FieldType.PACKED_DECIMAL, 29);
        pnd_Confirm_Calc_Tots_Pnd_Comp_Lit = pnd_Confirm_Calc_Tots.newFieldInGroup("pnd_Confirm_Calc_Tots_Pnd_Comp_Lit", "#COMP-LIT", FieldType.STRING, 
            10);
        pnd_1st_C_Bene = localVariables.newFieldInRecord("pnd_1st_C_Bene", "#1ST-C-BENE", FieldType.BOOLEAN, 1);
        pnd_No_C_Benes_Yet = localVariables.newFieldInRecord("pnd_No_C_Benes_Yet", "#NO-C-BENES-YET", FieldType.BOOLEAN, 1);
        pnd_Num_Poss_Mos_Lines = localVariables.newFieldInRecord("pnd_Num_Poss_Mos_Lines", "#NUM-POSS-MOS-LINES", FieldType.NUMERIC, 3);
        pnd_Xr_Rel_Code_Found = localVariables.newFieldInRecord("pnd_Xr_Rel_Code_Found", "#XR-REL-CODE-FOUND", FieldType.BOOLEAN, 1);
        pnd_Ss_Code_Found = localVariables.newFieldInRecord("pnd_Ss_Code_Found", "#SS-CODE-FOUND", FieldType.BOOLEAN, 1);
        pnd_Sr_Code_Found = localVariables.newFieldInRecord("pnd_Sr_Code_Found", "#SR-CODE-FOUND", FieldType.BOOLEAN, 1);
        pnd_T_I = localVariables.newFieldInRecord("pnd_T_I", "#T-I", FieldType.PACKED_DECIMAL, 3);
        pnd_Same_As = localVariables.newFieldInRecord("pnd_Same_As", "#SAME-AS", FieldType.STRING, 35);

        pnd_Same_As__R_Field_9 = localVariables.newGroupInRecord("pnd_Same_As__R_Field_9", "REDEFINE", pnd_Same_As);
        pnd_Same_As_Pnd_Same_As_Txt = pnd_Same_As__R_Field_9.newFieldInGroup("pnd_Same_As_Pnd_Same_As_Txt", "#SAME-AS-TXT", FieldType.STRING, 8);

        pnd_Same_As__R_Field_10 = localVariables.newGroupInRecord("pnd_Same_As__R_Field_10", "REDEFINE", pnd_Same_As);
        pnd_Same_As_Pnd_See_Txt = pnd_Same_As__R_Field_10.newFieldInGroup("pnd_Same_As_Pnd_See_Txt", "#SEE-TXT", FieldType.STRING, 4);
        pnd_Test_Lit = localVariables.newFieldInRecord("pnd_Test_Lit", "#TEST-LIT", FieldType.STRING, 72);

        pnd_Test_Lit__R_Field_11 = localVariables.newGroupInRecord("pnd_Test_Lit__R_Field_11", "REDEFINE", pnd_Test_Lit);
        pnd_Test_Lit_Pnd_Test_1st_Byte = pnd_Test_Lit__R_Field_11.newFieldInGroup("pnd_Test_Lit_Pnd_Test_1st_Byte", "#TEST-1ST-BYTE", FieldType.STRING, 
            1);
        pnd_Test_Lit_Pnd_Test_Rest_Byte = pnd_Test_Lit__R_Field_11.newFieldInGroup("pnd_Test_Lit_Pnd_Test_Rest_Byte", "#TEST-REST-BYTE", FieldType.STRING, 
            34);
        pnd_Spcl_Txt_Indexed = localVariables.newFieldArrayInRecord("pnd_Spcl_Txt_Indexed", "#SPCL-TXT-INDEXED", FieldType.STRING, 72, new DbsArrayController(1, 
            3));
        pnd_St = localVariables.newFieldInRecord("pnd_St", "#ST", FieldType.NUMERIC, 1);
        pnd_Sep_Tab = localVariables.newFieldArrayInRecord("pnd_Sep_Tab", "#SEP-TAB", FieldType.STRING, 72, new DbsArrayController(1, 72));
        pnd_Name_Only_This_Bene = localVariables.newFieldInRecord("pnd_Name_Only_This_Bene", "#NAME-ONLY-THIS-BENE", FieldType.BOOLEAN, 1);
        pnd_1st_Primary_Name_Only = localVariables.newFieldInRecord("pnd_1st_Primary_Name_Only", "#1ST-PRIMARY-NAME-ONLY", FieldType.BOOLEAN, 1);
        pnd_1st_C_Bene_Name_Only = localVariables.newFieldInRecord("pnd_1st_C_Bene_Name_Only", "#1ST-C-BENE-NAME-ONLY", FieldType.BOOLEAN, 1);
        pnd_Inval_Non_Acis_Rel = localVariables.newFieldInRecord("pnd_Inval_Non_Acis_Rel", "#INVAL-NON-ACIS-REL", FieldType.BOOLEAN, 1);
        pnd_Same_Hold = localVariables.newFieldInRecord("pnd_Same_Hold", "#SAME-HOLD", FieldType.STRING, 20);
        pnd_1stc = localVariables.newFieldInRecord("pnd_1stc", "#1STC", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_bdi.reset();
        vw_bmi.reset();

        localVariables.reset();
        pnd_Y_N_Tab.setInitialValue("YN ");
        pnd_Cntrct_Type_Values_Tab.setInitialValue("DIN");
        pnd_Pymnt_Child_Dcsd_Tab.setInitialValue("ABCDEF ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Benn9820() throws Exception
    {
        super("Benn9820");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ERROR-INFO-SUB.##LAST-PROGRAM := *PROGRAM
                                                                                                                                                                          //Natural: PERFORM BUILD-SUPER
        sub_Build_Super();
        if (condition(Global.isEscape())) {return;}
        pdaBena9820.getBena9820_Pnd_Mos_Isn().reset();                                                                                                                    //Natural: RESET #MOS-ISN #DSGNTN-GRP ( * ) #ERROR-MSG #EDIT-FAILED
        pdaBena9820.getBena9820_Pnd_Dsgntn_Grp().getValue("*").reset();
        pdaBena9820.getBena9820_Pnd_Error_Msg().reset();
        pdaBena9820.getBena9820_Pnd_Edit_Failed().reset();
        //*  CHECK FOR BOTH TO ENSURE DATA INTEGRITY.
                                                                                                                                                                          //Natural: PERFORM GET-MOS-RECORD
        sub_Get_Mos_Record();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-DESIGNATION-RECORDS
        sub_Get_Designation_Records();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaBena9820.getBena9820_Pnd_Error_Msg().notEquals(" ")))                                                                                            //Natural: IF BENA9820.#ERROR-MSG NE ' '
        {
            pdaBena9820.getBena9820_Pnd_Edit_Failed().setValue(true);                                                                                                     //Natural: ASSIGN BENA9820.#EDIT-FAILED := TRUE
            pdaBena9820.getBena9820_Pnd_Error_Pgm().setValue(Global.getPROGRAM());                                                                                        //Natural: ASSIGN BENA9820.#ERROR-PGM := *PROGRAM
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM BENE-EDITS
        sub_Bene_Edits();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaBena9820.getBena9820_Pnd_Error_Msg().notEquals(" ")))                                                                                            //Natural: IF BENA9820.#ERROR-MSG NE ' '
        {
            pdaBena9820.getBena9820_Pnd_Edit_Failed().setValue(true);                                                                                                     //Natural: ASSIGN BENA9820.#EDIT-FAILED := TRUE
            pdaBena9820.getBena9820_Pnd_Error_Pgm().setValue(Global.getPROGRAM());                                                                                        //Natural: ASSIGN BENA9820.#ERROR-PGM := *PROGRAM
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUILD-SUPER
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-MOS-RECORD
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-DESIGNATION-RECORDS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BENE-EDITS
        //*   IF THE CREF NUMBER ON COR DOES NOT MATCH WATCH WE've been given,
        //*     ERROR
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EDIT-MOS-RECORD
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EDIT-DESIGNATION-RECORDS
        //*                                                             #BENE-NAME2
        //*  IRREVOCABLE: IF CONTRACT Y AND AND NO DES IS Y, THEN ERROR
        //*               IF CONTRACT N AND SOME DES NOT Y, THEN ALLOW AS WE'll set
        //*  OUR CONTRACT IND TO Y.
        //*               IF CONTRACT N AND ALL DES ARE Y, THEN ERROR
        //*  CIS SOMETIMES SENDS MDOS WHERE THE P BENE HAS AN 'F' ALLOCATION
        //*  AND THE C BENE DOESN't, however it will erroneously have a 'P' -IND.
        //*  THE 'allocation edit' CODE (L 6250) ALLOWS 0 ALLOCATIONS
        //*  THROUGH PROVIDED THERE IS ONLY A PRIMARY OR 1 PRIMARY/1 CONTINGENT MIX
        //*  BENN9802 CREATES A 1/1 'F' ALLOCATION IN THESE CIRCUMSTANCES.
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-SHARES-BALANCE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CONVERT-RELATIONSHIP-CODE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-FOR-TEXT-ALLOCATIONS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-FOR-NUMERIC-SAME-AS
        //* ***********************************************************************
        //*  THE BELOW CODE IS ADAPTED FROM BENN8163. WHEN SAME AS IS IMPLEMENTED
        //*  IT MAY BE USED TO PARSE THE CONTRACT NUMBER (FOR COPYING) ALSO.
        //*  EMBEDDED ALPHAS WILL BE ALLOWED IN POS. 5,6 AND 7. 'C' USED FOR
        //*  REMAINDER OF MASKS AS 'U' CREATES TOO MANY MASKS! 'C' ALLOWS NUMERICS
        //*  AND ALPHAS - LOWER CASE IN BELOW COMBOS SEEMS MOST UNLIKELY. (AND
        //*  ' ' IS RULED OUT BY SEPARATE ITSELF).
        //*        OR= MASK ('#'UNNNNCCC'-'N'. ')  OR= MASK ('#'NNNNCCC'-'N'. ')
        //*        OR= MASK ('#'UNNNNCCCN'. ')     OR= MASK ('#'NNNNCCCN'. ')
        //*  CURRENTLY THESE EXAMINES DON't add anything - if this code is used to
        //*  ACTUALLY PARSE THE COPIED-FROM # THEY WILL BE NEEDED.
        //*      EXAMINE #SAME-HOLD FOR '#' DELETE
        //*      EXAMINE #SAME-HOLD FOR '-' DELETE
    }
    private void sub_Build_Super() throws Exception                                                                                                                       //Natural: BUILD-SUPER
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Intrfce_Super_2_Pnd_Intrfce_Stts.setValue(pdaBena9801.getBena9801_Intrfce_Stts());                                                                            //Natural: ASSIGN #INTRFCE-SUPER-2.#INTRFCE-STTS := BENA9801.INTRFCE-STTS
        pnd_Intrfce_Super_2_Pnd_Rcrd_Crtd_For_Bsnss_Dte.setValue(pdaBena9801.getBena9801_Rcrd_Crtd_For_Bsnss_Dte());                                                      //Natural: ASSIGN #INTRFCE-SUPER-2.#RCRD-CRTD-FOR-BSNSS-DTE := BENA9801.RCRD-CRTD-FOR-BSNSS-DTE
        pnd_Intrfce_Super_2_Pnd_Pin_Tiaa_Cntrct.setValue(pdaBena9801.getBena9801_Pin_Tiaa_Cntrct());                                                                      //Natural: ASSIGN #INTRFCE-SUPER-2.#PIN-TIAA-CNTRCT := BENA9801.PIN-TIAA-CNTRCT
        pnd_Intrfce_Super_2_Pnd_Tiaa_Cref_Ind.setValue(pdaBena9801.getBena9801_Tiaa_Cref_Ind());                                                                          //Natural: ASSIGN #INTRFCE-SUPER-2.#TIAA-CREF-IND := BENA9801.TIAA-CREF-IND
        pnd_Intrfce_Super_4_Pnd_Intrfce_Stts.setValue(pdaBena9801.getBena9801_Intrfce_Stts());                                                                            //Natural: ASSIGN #INTRFCE-SUPER-4.#INTRFCE-STTS := BENA9801.INTRFCE-STTS
        pnd_Intrfce_Super_4_Pnd_Rcrd_Crtd_For_Bsnss_Dte.setValue(pdaBena9801.getBena9801_Rcrd_Crtd_For_Bsnss_Dte());                                                      //Natural: ASSIGN #INTRFCE-SUPER-4.#RCRD-CRTD-FOR-BSNSS-DTE := BENA9801.RCRD-CRTD-FOR-BSNSS-DTE
        pnd_Intrfce_Super_4_Pnd_Pin_Tiaa_Cntrct.setValue(pdaBena9801.getBena9801_Pin_Tiaa_Cntrct());                                                                      //Natural: ASSIGN #INTRFCE-SUPER-4.#PIN-TIAA-CNTRCT := BENA9801.PIN-TIAA-CNTRCT
        pnd_Intrfce_Super_4_Pnd_Tiaa_Cref_Ind.setValue(pdaBena9801.getBena9801_Tiaa_Cref_Ind());                                                                          //Natural: ASSIGN #INTRFCE-SUPER-4.#TIAA-CREF-IND := BENA9801.TIAA-CREF-IND
        //*  BUILD-SUPER
    }
    private void sub_Get_Mos_Record() throws Exception                                                                                                                    //Natural: GET-MOS-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        vw_bmi.startDatabaseFind                                                                                                                                          //Natural: FIND ( 1 ) BMI WITH INTRFCE-SUPER-2 = #INTRFCE-SUPER-2
        (
        "FBMI",
        new Wc[] { new Wc("INTRFCE_SUPER_2", "=", pnd_Intrfce_Super_2, WcType.WITH) },
        1
        );
        FBMI:
        while (condition(vw_bmi.readNextRow("FBMI", true)))
        {
            vw_bmi.setIfNotFoundControlFlag(false);
            if (condition(vw_bmi.getAstCOUNTER().equals(0)))                                                                                                              //Natural: IF NO RECORD FOUND
            {
                if (condition(pdaBena9801.getBena9801_Mos_Ind().equals("Y")))                                                                                             //Natural: IF BENA9801.MOS-IND = 'Y'
                {
                    pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("No Interface rec for MOS Super", pnd_Intrfce_Super_2));                            //Natural: COMPRESS 'No Interface rec for MOS Super' #INTRFCE-SUPER-2 TO BENA9820.#ERROR-MSG
                }                                                                                                                                                         //Natural: END-IF
                if (true) break FBMI;                                                                                                                                     //Natural: ESCAPE BOTTOM ( FBMI. )
            }                                                                                                                                                             //Natural: END-NOREC
            if (condition(! (pdaBena9801.getBena9801_Mos_Ind().equals("Y"))))                                                                                             //Natural: IF NOT BENA9801.MOS-IND = 'Y'
            {
                pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Interface rec for MOS Super", pnd_Intrfce_Super_2, "found! MOS =",                     //Natural: COMPRESS 'Interface rec for MOS Super' #INTRFCE-SUPER-2 'found! MOS =' MOS-IND TO BENA9820.#ERROR-MSG
                    pdaBena9801.getBena9801_Mos_Ind()));
            }                                                                                                                                                             //Natural: END-IF
            pdaBena9820.getBena9820_Pnd_Mos_Isn().setValue(vw_bmi.getAstISN("FBMI"));                                                                                     //Natural: ASSIGN BENA9820.#MOS-ISN := *ISN ( FBMI. )
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  GET-MOS-RECORD
    }
    private void sub_Get_Designation_Records() throws Exception                                                                                                           //Natural: GET-DESIGNATION-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_I1.reset();                                                                                                                                                   //Natural: RESET #I1 #DESIG-TAB ( * )
        pnd_Desig_Tab.getValue("*").reset();
        vw_bdi.startDatabaseRead                                                                                                                                          //Natural: READ BDI BY INTRFCE-SUPER-4 FROM #INTRFCE-SUPER-4
        (
        "RBDI",
        new Wc[] { new Wc("INTRFCE_SUPER_4", ">=", pnd_Intrfce_Super_4, WcType.BY) },
        new Oc[] { new Oc("INTRFCE_SUPER_4", "ASC") }
        );
        RBDI:
        while (condition(vw_bdi.readNextRow("RBDI")))
        {
            if (condition(bdi_Intrfce_Stts.notEquals(pdaBena9801.getBena9801_Intrfce_Stts()) || bdi_Rcrd_Crtd_For_Bsnss_Dte.notEquals(pdaBena9801.getBena9801_Rcrd_Crtd_For_Bsnss_Dte())  //Natural: IF BDI.INTRFCE-STTS NE BENA9801.INTRFCE-STTS OR BDI.RCRD-CRTD-FOR-BSNSS-DTE NE BENA9801.RCRD-CRTD-FOR-BSNSS-DTE OR BDI.PIN-TIAA-CNTRCT NE BENA9801.PIN-TIAA-CNTRCT OR BDI.TIAA-CREF-IND NE BENA9801.TIAA-CREF-IND
                || bdi_Pin_Tiaa_Cntrct.notEquals(pdaBena9801.getBena9801_Pin_Tiaa_Cntrct()) || bdi_Tiaa_Cref_Ind.notEquals(pdaBena9801.getBena9801_Tiaa_Cref_Ind())))
            {
                if (true) break RBDI;                                                                                                                                     //Natural: ESCAPE BOTTOM ( RBDI. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_I1.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #I1
            if (condition(pnd_I1.greater(30)))                                                                                                                            //Natural: IF #I1 > 30
            {
                pdaBena9820.getBena9820_Pnd_Error_Msg().setValue("More than thirty Beneficiaries!!");                                                                     //Natural: ASSIGN BENA9820.#ERROR-MSG = 'More than thirty Beneficiaries!!'
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pdaBena9820.getBena9820_Pnd_Dsgntn_Isn().getValue(pnd_I1).setValue(vw_bdi.getAstISN("RBDI"));                                                                 //Natural: ASSIGN BENA9820.#DSGNTN-ISN ( #I1 ) := *ISN ( RBDI. )
            pnd_Desig_Tab_Pnd_Bene_Type.getValue(pnd_I1).setValue(bdi_Bene_Type);                                                                                         //Natural: ASSIGN #BENE-TYPE ( #I1 ) := BDI.BENE-TYPE
            pnd_Desig_Tab_Pnd_Rltn_Cde.getValue(pnd_I1).setValue(bdi_Rltn_Cde);                                                                                           //Natural: ASSIGN #RLTN-CDE ( #I1 ) := BDI.RLTN-CDE
            pnd_Desig_Tab_Pnd_Bene_Name1.getValue(pnd_I1).setValue(bdi_Bene_Name1);                                                                                       //Natural: ASSIGN #BENE-NAME1 ( #I1 ) := BDI.BENE-NAME1
            pnd_Desig_Tab_Pnd_Bene_Name2.getValue(pnd_I1).setValue(bdi_Bene_Name2);                                                                                       //Natural: ASSIGN #BENE-NAME2 ( #I1 ) := BDI.BENE-NAME2
            pnd_Desig_Tab_Pnd_Rltn_Free_Txt.getValue(pnd_I1).setValue(bdi_Rltn_Free_Txt);                                                                                 //Natural: ASSIGN #RLTN-FREE-TXT ( #I1 ) := BDI.RLTN-FREE-TXT
            pnd_Desig_Tab_Pnd_Dte_Birth_Trust.getValue(pnd_I1).setValue(bdi_Dte_Birth_Trust);                                                                             //Natural: ASSIGN #DTE-BIRTH-TRUST ( #I1 ) := BDI.DTE-BIRTH-TRUST
            pnd_Desig_Tab_Pnd_Ssn_Cde.getValue(pnd_I1).setValue(bdi_Ssn_Cde);                                                                                             //Natural: ASSIGN #SSN-CDE ( #I1 ) := BDI.SSN-CDE
            pnd_Desig_Tab_Pnd_Ssn.getValue(pnd_I1).setValue(bdi_Ssn);                                                                                                     //Natural: ASSIGN #SSN ( #I1 ) := BDI.SSN
            pnd_Desig_Tab_Pnd_Prcnt_Frctn_Ind.getValue(pnd_I1).setValue(bdi_Prcnt_Frctn_Ind);                                                                             //Natural: ASSIGN #PRCNT-FRCTN-IND ( #I1 ) := BDI.PRCNT-FRCTN-IND
            pnd_Desig_Tab_Pnd_Share_Prcnt.getValue(pnd_I1).setValue(bdi_Share_Prcnt);                                                                                     //Natural: ASSIGN #SHARE-PRCNT ( #I1 ) := BDI.SHARE-PRCNT
            pnd_Desig_Tab_Pnd_Share_Nmrtr.getValue(pnd_I1).setValue(bdi_Share_Nmrtr);                                                                                     //Natural: ASSIGN #SHARE-NMRTR ( #I1 ) := BDI.SHARE-NMRTR
            pnd_Desig_Tab_Pnd_Share_Dnmntr.getValue(pnd_I1).setValue(bdi_Share_Dnmntr);                                                                                   //Natural: ASSIGN #SHARE-DNMNTR ( #I1 ) := BDI.SHARE-DNMNTR
            pnd_Desig_Tab_Pnd_Irrvcbl_Ind.getValue(pnd_I1).setValue(bdi_Irrvcble_Ind);                                                                                    //Natural: ASSIGN #IRRVCBL-IND ( #I1 ) := BDI.IRRVCBLE-IND
            pnd_Desig_Tab_Pnd_Sttlmnt_Rstrctn.getValue(pnd_I1).setValue(bdi_Sttlmnt_Rstrctn);                                                                             //Natural: ASSIGN #STTLMNT-RSTRCTN ( #I1 ) := BDI.STTLMNT-RSTRCTN
            pnd_Desig_Tab_Pnd_Spcl_Txt1.getValue(pnd_I1).setValue(bdi_Spcl_Txt1);                                                                                         //Natural: ASSIGN #SPCL-TXT1 ( #I1 ) := BDI.SPCL-TXT1
            pnd_Desig_Tab_Pnd_Spcl_Txt2.getValue(pnd_I1).setValue(bdi_Spcl_Txt2);                                                                                         //Natural: ASSIGN #SPCL-TXT2 ( #I1 ) := BDI.SPCL-TXT2
            pnd_Desig_Tab_Pnd_Spcl_Txt3.getValue(pnd_I1).setValue(bdi_Spcl_Txt3);                                                                                         //Natural: ASSIGN #SPCL-TXT3 ( #I1 ) := BDI.SPCL-TXT3
            pnd_Desig_Tab_Pnd_Dflt_Estate.getValue(pnd_I1).setValue(bdi_Dflt_Estate);                                                                                     //Natural: ASSIGN #DFLT-ESTATE ( #I1 ) := BDI.DFLT-ESTATE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_I1.equals(getZero())))                                                                                                                          //Natural: IF #I1 = 0
        {
            //*  NO DESIGNATION RECORD FOUND
            //*  WE MAKE ONE.
            if (condition(! (pdaBena9801.getBena9801_Mos_Ind().equals("Y")) && ! (pdaBena9801.getBena9801_Dflt_To_Estate_Ind().equals("Y"))))                             //Natural: IF NOT MOS-IND = 'Y' AND NOT DFLT-TO-ESTATE-IND = 'Y'
            {
                pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("No Beneficiary Record found:", pnd_Intrfce_Super_2));                                  //Natural: COMPRESS 'No Beneficiary Record found:' #INTRFCE-SUPER-2 TO BENA9820.#ERROR-MSG
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaBena9801.getBena9801_Mos_Ind().equals("Y")))                                                                                                 //Natural: IF MOS-IND = 'Y'
            {
                pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("'MOS' with Designation rec for", pnd_Intrfce_Super_2, "found! MOS =",                  //Natural: COMPRESS '"MOS" with Designation rec for' #INTRFCE-SUPER-2 'found! MOS =' MOS-IND TO BENA9820.#ERROR-MSG
                    pdaBena9801.getBena9801_Mos_Ind()));
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaBena9801.getBena9801_Dflt_To_Estate_Ind().equals("Y") && (pnd_I1.greater(1) || pnd_Desig_Tab_Pnd_Bene_Type.getValue(pnd_I1).notEquals("P")   //Natural: IF DFLT-TO-ESTATE-IND = 'Y' AND ( #I1 GT 1 OR #BENE-TYPE ( #I1 ) NE 'P' OR #RLTN-CDE ( #I1 ) NE 'E' OR #BENE-NAME1 ( #I1 ) NE 'ESTATE' OR #BENE-NAME2 ( #I1 ) NE ' ' OR #RLTN-FREE-TXT ( #I1 ) GT ' ' OR #DTE-BIRTH-TRUST ( #I1 ) NE '0' OR #SSN-CDE ( #I1 ) NE ' ' OR #SSN ( #I1 ) NE '000000000' OR #PRCNT-FRCTN-IND ( #I1 ) NE 'F' OR #SHARE-NMRTR ( #I1 ) NE 1 OR #SHARE-DNMNTR ( #I1 ) NE 1 OR #IRRVCBL-IND ( #I1 ) NE ' ' OR #STTLMNT-RSTRCTN ( #I1 ) NE ' ' OR #SPCL-TXT1 ( #I1 ) NE ' ' OR #SPCL-TXT2 ( #I1 ) NE ' ' OR #SPCL-TXT3 ( #I1 ) NE ' ' )
                || pnd_Desig_Tab_Pnd_Rltn_Cde.getValue(pnd_I1).notEquals("E") || pnd_Desig_Tab_Pnd_Bene_Name1.getValue(pnd_I1).notEquals("ESTATE") || pnd_Desig_Tab_Pnd_Bene_Name2.getValue(pnd_I1).notEquals(" ") 
                || pnd_Desig_Tab_Pnd_Rltn_Free_Txt.getValue(pnd_I1).greater(" ") || pnd_Desig_Tab_Pnd_Dte_Birth_Trust.getValue(pnd_I1).notEquals("0") || 
                pnd_Desig_Tab_Pnd_Ssn_Cde.getValue(pnd_I1).notEquals(" ") || pnd_Desig_Tab_Pnd_Ssn.getValue(pnd_I1).notEquals("000000000") || pnd_Desig_Tab_Pnd_Prcnt_Frctn_Ind.getValue(pnd_I1).notEquals("F") 
                || pnd_Desig_Tab_Pnd_Share_Nmrtr.getValue(pnd_I1).notEquals(1) || pnd_Desig_Tab_Pnd_Share_Dnmntr.getValue(pnd_I1).notEquals(1) || pnd_Desig_Tab_Pnd_Irrvcbl_Ind.getValue(pnd_I1).notEquals(" ") 
                || pnd_Desig_Tab_Pnd_Sttlmnt_Rstrctn.getValue(pnd_I1).notEquals(" ") || pnd_Desig_Tab_Pnd_Spcl_Txt1.getValue(pnd_I1).notEquals(" ") || pnd_Desig_Tab_Pnd_Spcl_Txt2.getValue(pnd_I1).notEquals(" ") 
                || pnd_Desig_Tab_Pnd_Spcl_Txt3.getValue(pnd_I1).notEquals(" "))))
            {
                pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("DFLT TO ESTATE with Designation rec for", pnd_Intrfce_Super_2));                       //Natural: COMPRESS 'DFLT TO ESTATE with Designation rec for' #INTRFCE-SUPER-2 TO BENA9820.#ERROR-MSG
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  GET-DESIGNATION-RECORDS
    }
    private void sub_Bene_Edits() throws Exception                                                                                                                        //Natural: BENE-EDITS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*   CHECK IF THIS IS A RETIREMENT LOAN CONTRACT
        if (condition(pdaBena9820.getBena9820_Pnd_Cntrct_Lob_Cde().equals("D") && pdaBena9820.getBena9820_Pnd_Cntrct_Nmbr_Range_Cde().equals("07") &&                     //Natural: IF BENA9820.#CNTRCT-LOB-CDE = 'D' AND BENA9820.#CNTRCT-NMBR-RANGE-CDE = '07' AND BENA9820.#CNTRCT-SPCL-CNSDRTN-CDE = '09'
            pdaBena9820.getBena9820_Pnd_Cntrct_Spcl_Cnsdrtn_Cde().equals("09")))
        {
            pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Contract", pdaBena9801.getBena9801_Tiaa_Cntrct(), "is a Retirement Loan",                  //Natural: COMPRESS 'Contract' BENA9801.TIAA-CNTRCT 'is a Retirement Loan' 'contract' INTO BENA9820.#ERROR-MSG
                "contract"));
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*   VALID VALUES IN DEFINED VARIABLE
        //*   FIRST EDIT CONTRACT LEVEL DATA
        short decideConditionsMet626 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN NOT DFLT-TO-ESTATE-IND = #Y-N-OR-BLANK ( * )
        if (condition(! (pdaBena9801.getBena9801_Dflt_To_Estate_Ind().equals(pnd_Y_N_Tab_Pnd_Y_N_Or_Blank.getValue("*")))))
        {
            decideConditionsMet626++;
            pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Contract level Default to Estate Indicator invalid:", pdaBena9801.getBena9801_Dflt_To_Estate_Ind())); //Natural: COMPRESS 'Contract level Default to Estate Indicator invalid:' DFLT-TO-ESTATE-IND TO BENA9820.#ERROR-MSG
        }                                                                                                                                                                 //Natural: WHEN NOT CHNG-PWR-ATTY = #Y-N-OR-BLANK ( * )
        else if (condition(! (pdaBena9801.getBena9801_Chng_Pwr_Atty().equals(pnd_Y_N_Tab_Pnd_Y_N_Or_Blank.getValue("*")))))
        {
            decideConditionsMet626++;
            pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Changed by Power of Attorney indicator invalid:", pdaBena9801.getBena9801_Chng_Pwr_Atty())); //Natural: COMPRESS 'Changed by Power of Attorney indicator invalid:' CHNG-PWR-ATTY TO BENA9820.#ERROR-MSG
        }                                                                                                                                                                 //Natural: WHEN NOT MOS-IND = #Y-N-OR-BLANK ( * )
        else if (condition(! (pdaBena9801.getBena9801_Mos_Ind().equals(pnd_Y_N_Tab_Pnd_Y_N_Or_Blank.getValue("*")))))
        {
            decideConditionsMet626++;
            pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Mode Of Settlement Indicator invalid:", pdaBena9801.getBena9801_Mos_Ind()));               //Natural: COMPRESS 'Mode Of Settlement Indicator invalid:' MOS-IND TO BENA9820.#ERROR-MSG
        }                                                                                                                                                                 //Natural: WHEN NOT CNTRCT-TYPE = #CNTRCT-TYPE-VALUES ( * )
        else if (condition(! (pdaBena9801.getBena9801_Cntrct_Type().equals(pnd_Cntrct_Type_Values_Tab_Pnd_Cntrct_Type_Values.getValue("*")))))
        {
            decideConditionsMet626++;
            //*  NOT DA CONTRACT
            pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Contract Type Indicator invalid:", pdaBena9801.getBena9801_Cntrct_Type()));                //Natural: COMPRESS 'Contract Type Indicator invalid:' CNTRCT-TYPE TO BENA9820.#ERROR-MSG
        }                                                                                                                                                                 //Natural: WHEN ( NOT CNTRCT-TYPE = 'D' ) AND CREF-CNTRCT GT ' '
        else if (condition(! ((pdaBena9801.getBena9801_Cntrct_Type().equals("D")) && pdaBena9801.getBena9801_Cref_Cntrct().greater(" "))))
        {
            decideConditionsMet626++;
            //*  NOT DA CONTRACT
            pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Non DA Contract with CREF#:", pdaBena9801.getBena9801_Cref_Cntrct()));                     //Natural: COMPRESS 'Non DA Contract with CREF#:' CREF-CNTRCT TO BENA9820.#ERROR-MSG
        }                                                                                                                                                                 //Natural: WHEN ( NOT CNTRCT-TYPE = 'D' ) AND BENA9801.TIAA-CREF-IND GT ' '
        else if (condition(! ((pdaBena9801.getBena9801_Cntrct_Type().equals("D")) && pdaBena9801.getBena9801_Tiaa_Cref_Ind().greater(" "))))
        {
            decideConditionsMet626++;
            pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Non DA Contract with Tiaa-Cref-Ind:", pdaBena9801.getBena9801_Tiaa_Cref_Ind()));           //Natural: COMPRESS 'Non DA Contract with Tiaa-Cref-Ind:' BENA9801.TIAA-CREF-IND TO BENA9820.#ERROR-MSG
        }                                                                                                                                                                 //Natural: WHEN BENA9801.TIAA-CREF-IND NE ' ' AND NOT ( BENA9801.TIAA-CREF-IND = 'T' OR = 'C' )
        else if (condition((pdaBena9801.getBena9801_Tiaa_Cref_Ind().notEquals(" ") && ! ((pdaBena9801.getBena9801_Tiaa_Cref_Ind().equals("T") || pdaBena9801.getBena9801_Tiaa_Cref_Ind().equals("C"))))))
        {
            decideConditionsMet626++;
            pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Invalid Tiaa-Cref-Ind:", pdaBena9801.getBena9801_Tiaa_Cref_Ind()));                        //Natural: COMPRESS 'Invalid Tiaa-Cref-Ind:' BENA9801.TIAA-CREF-IND TO BENA9820.#ERROR-MSG
        }                                                                                                                                                                 //Natural: WHEN NOT BENA9801.IRRVCBLE-IND = #Y-N-OR-BLANK ( * )
        else if (condition(! (pdaBena9801.getBena9801_Irrvcble_Ind().equals(pnd_Y_N_Tab_Pnd_Y_N_Or_Blank.getValue("*")))))
        {
            decideConditionsMet626++;
            pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Contract level Irrevocable Indicator invalid:", pdaBena9801.getBena9801_Irrvcble_Ind()));  //Natural: COMPRESS 'Contract level Irrevocable Indicator invalid:' BENA9801.IRRVCBLE-IND TO BENA9820.#ERROR-MSG
        }                                                                                                                                                                 //Natural: WHEN NOT EXEMPT-SPOUSE-RIGHTS = #Y-N-OR-BLANK ( * )
        else if (condition(! (pdaBena9801.getBena9801_Exempt_Spouse_Rights().equals(pnd_Y_N_Tab_Pnd_Y_N_Or_Blank.getValue("*")))))
        {
            decideConditionsMet626++;
            pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Exempt from Spousal Rights Indicator invalid:", pdaBena9801.getBena9801_Exempt_Spouse_Rights())); //Natural: COMPRESS 'Exempt from Spousal Rights Indicator invalid:' EXEMPT-SPOUSE-RIGHTS TO BENA9820.#ERROR-MSG
        }                                                                                                                                                                 //Natural: WHEN NOT SPOUSE-WAIVED-BNFTS = #Y-N-OR-BLANK ( * )
        else if (condition(! (pdaBena9801.getBena9801_Spouse_Waived_Bnfts().equals(pnd_Y_N_Tab_Pnd_Y_N_Or_Blank.getValue("*")))))
        {
            decideConditionsMet626++;
            pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Spouse Waived Benefits Indicator invalid:", pdaBena9801.getBena9801_Spouse_Waived_Bnfts())); //Natural: COMPRESS 'Spouse Waived Benefits Indicator invalid:' SPOUSE-WAIVED-BNFTS TO BENA9820.#ERROR-MSG
        }                                                                                                                                                                 //Natural: WHEN NOT PYMNT-CHLD-DCSD-IND = #PYMNT-CHILD-DCSD-VALUES ( * )
        else if (condition(! (pdaBena9801.getBena9801_Pymnt_Chld_Dcsd_Ind().equals(pnd_Pymnt_Child_Dcsd_Tab_Pnd_Pymnt_Child_Dcsd_Values.getValue("*")))))
        {
            decideConditionsMet626++;
            pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Payment to Children of Deceased Beneficiary invalid:", pdaBena9801.getBena9801_Pymnt_Chld_Dcsd_Ind())); //Natural: COMPRESS 'Payment to Children of Deceased Beneficiary invalid:' PYMNT-CHLD-DCSD-IND TO BENA9820.#ERROR-MSG
        }                                                                                                                                                                 //Natural: WHEN EFF-DTE NE MASK ( YYYYMMDD )
        else if (condition(! ((DbsUtil.maskMatches(pdaBena9801.getBena9801_Eff_Dte(),"YYYYMMDD")))))
        {
            decideConditionsMet626++;
            pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Effective date invalid:", pdaBena9801.getBena9801_Eff_Dte()));                             //Natural: COMPRESS 'Effective date invalid:' EFF-DTE INTO BENA9820.#ERROR-MSG
        }                                                                                                                                                                 //Natural: WHEN EFF-DTE > #NEXT-BSNSS-DTE
        else if (condition(pdaBena9801.getBena9801_Eff_Dte().greater(pdaBena9820.getBena9820_Pnd_Next_Bsnss_Dte())))
        {
            decideConditionsMet626++;
            pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Effective date:", pdaBena9801.getBena9801_Eff_Dte(), "greater than next business date:",   //Natural: COMPRESS 'Effective date:' EFF-DTE 'greater than next business date:' #NEXT-BSNSS-DTE INTO BENA9820.#ERROR-MSG
                pdaBena9820.getBena9820_Pnd_Next_Bsnss_Dte()));
        }                                                                                                                                                                 //Natural: WHEN EFF-DTE < '16000101'
        else if (condition(pdaBena9801.getBena9801_Eff_Dte().less("16000101")))
        {
            decideConditionsMet626++;
            pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Effective date must not be less than 1/1/1600:", pdaBena9801.getBena9801_Eff_Dte()));      //Natural: COMPRESS 'Effective date must not be less than 1/1/1600:' EFF-DTE INTO BENA9820.#ERROR-MSG
        }                                                                                                                                                                 //Natural: WHEN ( EXEMPT-SPOUSE-RIGHTS = "Y" OR SPOUSE-WAIVED-BNFTS = "Y" ) AND NOT CNTRCT-TYPE = 'D'
        else if (condition(((pdaBena9801.getBena9801_Exempt_Spouse_Rights().equals("Y") || pdaBena9801.getBena9801_Spouse_Waived_Bnfts().equals("Y")) 
            && ! (pdaBena9801.getBena9801_Cntrct_Type().equals("D")))))
        {
            decideConditionsMet626++;
            pdaBena9820.getBena9820_Pnd_Error_Msg().setValue("Exempt Spousal Rights/Spouse Waived Benefits set for a non DA contract");                                   //Natural: MOVE 'Exempt Spousal Rights/Spouse Waived Benefits set for a non DA contract' TO BENA9820.#ERROR-MSG
        }                                                                                                                                                                 //Natural: WHEN MOS-IND = 'Y' AND PYMNT-CHLD-DCSD-IND GT ' '
        else if (condition(pdaBena9801.getBena9801_Mos_Ind().equals("Y") && pdaBena9801.getBena9801_Pymnt_Chld_Dcsd_Ind().greater(" ")))
        {
            decideConditionsMet626++;
            pdaBena9820.getBena9820_Pnd_Error_Msg().setValue("MOS Indicator & Payment to Child of Deceased both set.");                                                   //Natural: MOVE 'MOS Indicator & Payment to Child of Deceased both set.' TO BENA9820.#ERROR-MSG
        }                                                                                                                                                                 //Natural: WHEN EXEMPT-SPOUSE-RIGHTS = 'Y' AND SPOUSE-WAIVED-BNFTS = 'Y'
        else if (condition(pdaBena9801.getBena9801_Exempt_Spouse_Rights().equals("Y") && pdaBena9801.getBena9801_Spouse_Waived_Bnfts().equals("Y")))
        {
            decideConditionsMet626++;
            pdaBena9820.getBena9820_Pnd_Error_Msg().setValue("Exempt Spousal Rights/Spouse Waived Benefits both 'Y'.");                                                   //Natural: MOVE 'Exempt Spousal Rights/Spouse Waived Benefits both "Y".' TO BENA9820.#ERROR-MSG
        }                                                                                                                                                                 //Natural: WHEN DFLT-TO-ESTATE-IND = 'Y' AND MOS-IND = 'Y'
        else if (condition(pdaBena9801.getBena9801_Dflt_To_Estate_Ind().equals("Y") && pdaBena9801.getBena9801_Mos_Ind().equals("Y")))
        {
            decideConditionsMet626++;
            pdaBena9820.getBena9820_Pnd_Error_Msg().setValue("Contract level Default to Estate Ind & MOS Ind both 'Y'");                                                  //Natural: ASSIGN BENA9820.#ERROR-MSG := 'Contract level Default to Estate Ind & MOS Ind both "Y"'
        }                                                                                                                                                                 //Natural: WHEN BENA9820.#CREF-CNTRCT-NMBR NE BENA9801.CREF-CNTRCT
        else if (condition(pdaBena9820.getBena9820_Pnd_Cref_Cntrct_Nmbr().notEquals(pdaBena9801.getBena9801_Cref_Cntrct())))
        {
            decideConditionsMet626++;
            pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("CREF# on COR:", pdaBena9820.getBena9820_Pnd_Cref_Cntrct_Nmbr(), "Not Equal to CREF# interfaced:",  //Natural: COMPRESS 'CREF# on COR:' BENA9820.#CREF-CNTRCT-NMBR 'Not Equal to CREF# interfaced:' BENA9801.CREF-CNTRCT INTO BENA9820.#ERROR-MSG
                pdaBena9801.getBena9801_Cref_Cntrct()));
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet626 > 0))
        {
            pnd_Error_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #ERROR-CNT
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pdaBena9801.getBena9801_Mos_Ind().equals("Y")))                                                                                                     //Natural: IF BENA9801.MOS-IND = 'Y'
        {
                                                                                                                                                                          //Natural: PERFORM EDIT-MOS-RECORD
            sub_Edit_Mos_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(! (pdaBena9801.getBena9801_Dflt_To_Estate_Ind().equals("Y"))))                                                                                  //Natural: IF NOT DFLT-TO-ESTATE-IND = 'Y'
            {
                                                                                                                                                                          //Natural: PERFORM EDIT-DESIGNATION-RECORDS
                sub_Edit_Designation_Records();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaBena9801.getBena9801_Same_As_Ind().equals("Y") && pdaBena9820.getBena9820_Pnd_Error_Msg().equals(" ")))                                          //Natural: IF BENA9801.SAME-AS-IND = 'Y' AND BENA9820.#ERROR-MSG = ' '
        {
            pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(pnd_Desig_Tab_Pnd_Bene_Name1.getValue(1));                                                                   //Natural: ASSIGN BENA9820.#ERROR-MSG := #BENE-NAME1 ( 1 )
            pdaBena9820.getBena9820_Pnd_Error_Cde().setValue("SAME");                                                                                                     //Natural: ASSIGN BENA9820.#ERROR-CDE := 'SAME'
        }                                                                                                                                                                 //Natural: END-IF
        //*  BENE-EDITS
    }
    private void sub_Edit_Mos_Record() throws Exception                                                                                                                   //Natural: EDIT-MOS-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  LINES ENTIRELY BLANK
        if (condition(bmi_Count_Castmos_Txt_Group.equals(getZero()) || ! (bmi_Mos_Txt.getValue("*").notEquals(" "))))                                                     //Natural: IF BMI.C*MOS-TXT-GROUP = 0 OR NOT BMI.MOS-TXT ( * ) NE ' '
        {
            pdaBena9820.getBena9820_Pnd_Error_Msg().setValue("MOS Text lines are empty, however MOS Indicator = 'Y'.");                                                   //Natural: ASSIGN BENA9820.#ERROR-MSG := 'MOS Text lines are empty, however MOS Indicator = "Y".'
            pnd_Error_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #ERROR-CNT
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 37 STEP 12
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(37)); pnd_I.nadd(1))
        {
            pnd_N.compute(new ComputeParameters(false, pnd_N), pnd_I.add(12));                                                                                            //Natural: ASSIGN #N := #I + 12
            if (condition(! (bmi_Mos_Txt.getValue(pnd_I,":",pnd_N).notEquals(" ")) && bmi_Mos_Txt.getValue(pnd_N.getDec().add(1),":",60).notEquals(" ")))                 //Natural: IF NOT BMI.MOS-TXT ( #I:#N ) NE ' ' AND BMI.MOS-TXT ( #N+1:60 ) NE ' '
            {
                pdaBena9820.getBena9820_Pnd_Error_Msg().setValue("12 lines or more of empty MOS Text lines within Text block.");                                          //Natural: ASSIGN BENA9820.#ERROR-MSG := '12 lines or more of empty MOS Text lines within Text block.'
                pnd_Error_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #ERROR-CNT
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  INCOMPATIBLE DATA EDITS FOR MOS START HERE JW 12/18
        pnd_N.reset();                                                                                                                                                    //Natural: RESET #N
        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 TO 60
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(60)); pnd_I.nadd(1))
        {
            if (condition(! (bmi_Mos_Txt.getValue(pnd_I,":",60).notEquals(" "))))                                                                                         //Natural: IF NOT BMI.MOS-TXT ( #I:60 ) NE ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(bmi_Mos_Txt.getValue(pnd_I).equals(" ")))                                                                                                       //Natural: IF BMI.MOS-TXT ( #I ) = ' '
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.examine(new ExamineSource(bmi_Mos_Txt.getValue(pnd_I)), new ExamineTranslate(TranslateOption.Upper));                                                 //Natural: EXAMINE BMI.MOS-TXT ( #I ) TRANSLATE INTO UPPER CASE
            //*  FIRST POPULATED LINE
            if (condition(bmi_Mos_Txt.getValue(pnd_I).notEquals(" ") && pnd_N.equals(getZero())))                                                                         //Natural: IF BMI.MOS-TXT ( #I ) NE ' ' AND #N = 0
            {
                pnd_N.setValue(pnd_I);                                                                                                                                    //Natural: ASSIGN #N := #I
                pnd_Same_As.setValue(bmi_Mos_Txt.getValue(pnd_I), MoveOption.LeftJustified);                                                                              //Natural: MOVE LEFT BMI.MOS-TXT ( #I ) TO #SAME-AS
                if (condition(pnd_Same_As_Pnd_Same_As_Txt.equals("SAME AS") || pnd_Same_As_Pnd_Same_As_Txt.equals("SME AS ")))                                            //Natural: IF #SAME-AS-TXT = 'SAME AS' OR = 'SME AS '
                {
                    pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Same As:", pnd_Same_As, ", in MOS"));                                              //Natural: COMPRESS 'Same As:' #SAME-AS ', in MOS' TO BENA9820.#ERROR-MSG
                    pdaBena9820.getBena9820_Pnd_Error_Cde().setValue("SAME");                                                                                             //Natural: ASSIGN BENA9820.#ERROR-CDE := 'SAME'
                    pnd_Error_Cnt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #ERROR-CNT
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(bmi_Mos_Txt.getValue(pnd_I).equals("NONE")))                                                                                                //Natural: IF BMI.MOS-TXT ( #I ) = 'NONE'
                {
                    pdaBena9820.getBena9820_Pnd_Error_Msg().setValue("'NONE' error found in MOS");                                                                        //Natural: ASSIGN BENA9820.#ERROR-MSG := '"NONE" error found in MOS'
                    pdaBena9820.getBena9820_Pnd_Error_Cde().setValue("NONE");                                                                                             //Natural: ASSIGN BENA9820.#ERROR-CDE := 'NONE'
                    pnd_Error_Cnt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #ERROR-CNT
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(bmi_Mos_Txt.getValue(pnd_I).contains ("SEE FOLDER") ||bmi_Mos_Txt.getValue(pnd_I).contains ("SEE FILE")))                                       //Natural: IF BMI.MOS-TXT ( #I ) = SCAN 'SEE FOLDER' OR = SCAN 'SEE FILE'
            {
                pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("'SEE' Error in MOS:", bmi_Mos_Txt.getValue(pnd_I)));                                   //Natural: COMPRESS '"SEE" Error in MOS:' BMI.MOS-TXT ( #I ) TO BENA9820.#ERROR-MSG
                pdaBena9820.getBena9820_Pnd_Error_Cde().setValue("SEE ");                                                                                                 //Natural: ASSIGN BENA9820.#ERROR-CDE := 'SEE '
                pnd_Error_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #ERROR-CNT
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  EDIT-MOS-RECORD
    }
    private void sub_Edit_Designation_Records() throws Exception                                                                                                          //Natural: EDIT-DESIGNATION-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_1st_C_Bene.reset();                                                                                                                                           //Natural: RESET #1ST-C-BENE #NUM-POSS-MOS-LINES #1ST-PRIMARY-NAME-ONLY #1ST-C-BENE-NAME-ONLY
        pnd_Num_Poss_Mos_Lines.reset();
        pnd_1st_Primary_Name_Only.reset();
        pnd_1st_C_Bene_Name_Only.reset();
        pnd_No_C_Benes_Yet.setValue(true);                                                                                                                                //Natural: ASSIGN #NO-C-BENES-YET := TRUE
        FOR03:                                                                                                                                                            //Natural: FOR #K1 = 1 TO #I1
        for (pnd_K1.setValue(1); condition(pnd_K1.lessOrEqual(pnd_I1)); pnd_K1.nadd(1))
        {
            if (condition(pnd_Desig_Tab_Pnd_Dte_Birth_Trust.getValue(pnd_K1).equals("0") || pnd_Desig_Tab_Pnd_Dte_Birth_Trust.getValue(pnd_K1).equals("00000000")))       //Natural: IF #DTE-BIRTH-TRUST ( #K1 ) = '0' OR = '00000000'
            {
                pnd_Desig_Tab_Pnd_Dte_Birth_Trust.getValue(pnd_K1).reset();                                                                                               //Natural: RESET #DTE-BIRTH-TRUST ( #K1 )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Desig_Tab_Pnd_Ssn.getValue(pnd_K1).equals("0") || pnd_Desig_Tab_Pnd_Ssn.getValue(pnd_K1).equals("000000000")))                              //Natural: IF #SSN ( #K1 ) = '0' OR = '000000000'
            {
                pnd_Desig_Tab_Pnd_Ssn.getValue(pnd_K1).reset();                                                                                                           //Natural: RESET #SSN ( #K1 )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Desig_Tab_Pnd_Bene_Type.getValue(pnd_K1).equals("C") && pnd_No_C_Benes_Yet.getBoolean()))                                                   //Natural: IF #BENE-TYPE ( #K1 ) = 'C' AND #NO-C-BENES-YET
            {
                pnd_1st_C_Bene.setValue(true);                                                                                                                            //Natural: ASSIGN #1ST-C-BENE := TRUE
                pnd_1stc.setValue(pnd_K1);                                                                                                                                //Natural: ASSIGN #1STC := #K1
                pnd_No_C_Benes_Yet.reset();                                                                                                                               //Natural: RESET #NO-C-BENES-YET
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_1st_C_Bene.reset();                                                                                                                                   //Natural: RESET #1ST-C-BENE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Inval_Non_Acis_Rel.reset();                                                                                                                               //Natural: RESET #INVAL-NON-ACIS-REL
            if (condition(pdaBena9801.getBena9801_Rqstng_Systm().notEquals("OMNI") && (pnd_Desig_Tab_Pnd_Rltn_Cde.getValue(pnd_K1).greater(" ") || pnd_Desig_Tab_Pnd_Rltn_Free_Txt.getValue(pnd_K1).greater(" ")))) //Natural: IF BENA9801.RQSTNG-SYSTM NE 'OMNI' AND ( #RLTN-CDE ( #K1 ) > ' ' OR #RLTN-FREE-TXT ( #K1 ) > ' ' )
            {
                if (condition(pdaBena9801.getBena9801_Rqstng_Systm().equals("MDO")))                                                                                      //Natural: IF BENA9801.RQSTNG-SYSTM = 'MDO'
                {
                    pdaBena9832.getBena9832_Pnd_Systm().setValue("MDO");                                                                                                  //Natural: ASSIGN BENA9832.#SYSTM := 'MDO'
                    //*  ???
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaBena9832.getBena9832_Pnd_Systm().setValue("CIS");                                                                                                  //Natural: ASSIGN BENA9832.#SYSTM := 'CIS'
                }                                                                                                                                                         //Natural: END-IF
                pdaBena9832.getBena9832().reset();                                                                                                                        //Natural: RESET BENA9832
                pdaBena9832.getBena9832_Pnd_Systm_Code().setValue(pnd_Desig_Tab_Pnd_Rltn_Cde.getValue(pnd_K1));                                                           //Natural: ASSIGN BENA9832.#SYSTM-CODE := #RLTN-CDE ( #K1 )
                pdaBena9832.getBena9832_Pnd_Systm_Text().setValue(pnd_Desig_Tab_Pnd_Rltn_Free_Txt.getValue(pnd_K1));                                                      //Natural: ASSIGN BENA9832.#SYSTM-TEXT := #RLTN-FREE-TXT ( #K1 )
                                                                                                                                                                          //Natural: PERFORM CONVERT-RELATIONSHIP-CODE
                sub_Convert_Relationship_Code();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("E")))                                                                             //Natural: IF ##RETURN-CODE = 'E'
                {
                    pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress(pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg(), "for Beneficiary #",                     //Natural: COMPRESS ##MSG 'for Beneficiary #' #K1 TO BENA9820.#ERROR-MSG
                        pnd_K1));
                    pnd_Error_Cnt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #ERROR-CNT
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaBena9820.getBena9820_Pnd_Dsgntn_Rltn_Cde().getValue(pnd_K1).setValue(pdaBena9832.getBena9832_Pnd_Rltnshp_Cde());                                   //Natural: ASSIGN #DSGNTN-RLTN-CDE ( #K1 ) := BENA9832.#RLTNSHP-CDE
                    if (condition(pdaBena9832.getBena9832_Pnd_Not_On_Rt_Table().getBoolean()))                                                                            //Natural: IF BENA9832.#NOT-ON-RT-TABLE
                    {
                        if (condition(pdaBena9801.getBena9801_Intrfcng_Systm().equals("ACIS")))                                                                           //Natural: IF BENA9801.INTRFCNG-SYSTM = 'ACIS'
                        {
                            pdaBena9820.getBena9820_Pnd_Interface_As_Mos().setValue(true);                                                                                //Natural: ASSIGN BENA9820.#INTERFACE-AS-MOS := TRUE
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Inval_Non_Acis_Rel.setValue(true);                                                                                                        //Natural: ASSIGN #INVAL-NON-ACIS-REL := TRUE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaBena9820.getBena9820_Pnd_Dsgntn_Rltn_Cde().getValue(pnd_K1).setValue(pnd_Desig_Tab_Pnd_Rltn_Cde.getValue(pnd_K1));                                     //Natural: ASSIGN #DSGNTN-RLTN-CDE ( #K1 ) := #RLTN-CDE ( #K1 )
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet800 = 0;                                                                                                                             //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN NOT ( #BENE-TYPE ( #K1 ) = 'P' OR = 'C' )
            if (condition(! ((pnd_Desig_Tab_Pnd_Bene_Type.getValue(pnd_K1).equals("P") || pnd_Desig_Tab_Pnd_Bene_Type.getValue(pnd_K1).equals("C")))))
            {
                decideConditionsMet800++;
                pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Beneficiary Type for bene#", pnd_K1, "on Designation not P/C:", pnd_Desig_Tab_Pnd_Bene_Type.getValue(pnd_K1))); //Natural: COMPRESS 'Beneficiary Type for bene#' #K1 'on Designation not P/C:' #BENE-TYPE ( #K1 ) TO BENA9820.#ERROR-MSG
                pnd_Error_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #ERROR-CNT
            }                                                                                                                                                             //Natural: WHEN #BENE-NAME1 ( #K1 ) LE ' '
            if (condition(pnd_Desig_Tab_Pnd_Bene_Name1.getValue(pnd_K1).lessOrEqual(" ")))
            {
                decideConditionsMet800++;
                //*  REL CODE ON REL EXCEPTIONS TAB?
                if (condition(pnd_Desig_Tab_Pnd_Rltn_Cde.getValue(pnd_K1).notEquals(" ")))                                                                                //Natural: IF #RLTN-CDE ( #K1 ) NE ' '
                {
                    //*  JW
                    pnd_Xr_Rel_Code_Found.reset();                                                                                                                        //Natural: RESET #XR-REL-CODE-FOUND
                    FOR04:                                                                                                                                                //Natural: FOR #T-I 1 20
                    for (pnd_T_I.setValue(1); condition(pnd_T_I.lessOrEqual(20)); pnd_T_I.nadd(1))
                    {
                        if (condition(pdaBena9834.getBena9834_Pnd_Xr_Tab_Key().getValue(pnd_T_I).equals(" ")))                                                            //Natural: IF #XR-TAB-KEY ( #T-I ) = ' '
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pnd_Desig_Tab_Pnd_Rltn_Cde.getValue(pnd_K1).equals(pdaBena9834.getBena9834_Pnd_Xr_Tab_Key().getValue(pnd_T_I))))                    //Natural: IF #RLTN-CDE ( #K1 ) = #XR-TAB-KEY ( #T-I )
                        {
                            pnd_Xr_Rel_Code_Found.setValue(true);                                                                                                         //Natural: ASSIGN #XR-REL-CODE-FOUND := TRUE
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_Xr_Rel_Code_Found.getBoolean()))                                                                                                    //Natural: IF #XR-REL-CODE-FOUND
                    {
                        if (condition(pnd_Desig_Tab_Pnd_Spcl_Txt1.getValue(pnd_K1).lessOrEqual(" ")))                                                                     //Natural: IF #SPCL-TXT1 ( #K1 ) LE ' '
                        {
                            pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Beneficiary Name & Special Text both blank for Beneficiary #",             //Natural: COMPRESS 'Beneficiary Name & Special Text both blank for Beneficiary #' #K1 TO BENA9820.#ERROR-MSG
                                pnd_K1));
                            pnd_Error_Cnt.nadd(1);                                                                                                                        //Natural: ADD 1 TO #ERROR-CNT
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(pnd_Desig_Tab_Pnd_Bene_Name2.getValue(pnd_K1).greater(" ")))                                                                    //Natural: IF #BENE-NAME2 ( #K1 ) GT ' '
                            {
                                pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Beneficiary Name 1 blank, Name 2 non blank for Beneficiary #",         //Natural: COMPRESS 'Beneficiary Name 1 blank, Name 2 non blank for Beneficiary #' #K1 TO BENA9820.#ERROR-MSG
                                    pnd_K1));
                                pnd_Error_Cnt.nadd(1);                                                                                                                    //Natural: ADD 1 TO #ERROR-CNT
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  ALLOW SPECIAL TEXT WITH NON 'XR'
                        //*  REL FOR ACIS- WILL
                        //*  MAKE MOS ANYWAY
                        if (condition(pnd_Desig_Tab_Pnd_Spcl_Txt1.getValue(pnd_K1).greater(" ") && pdaBena9801.getBena9801_Intrfcng_Systm().equals("ACIS")))              //Natural: IF #SPCL-TXT1 ( #K1 ) GT ' ' AND BENA9801.INTRFCNG-SYSTM = 'ACIS'
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Beneficiary Name is blank for Beneficiary #", pnd_K1));                    //Natural: COMPRESS 'Beneficiary Name is blank for Beneficiary #' #K1 TO BENA9820.#ERROR-MSG
                            pnd_Error_Cnt.nadd(1);                                                                                                                        //Natural: ADD 1 TO #ERROR-CNT
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*  REL CODE IS ' '
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  SPCL TEXT+RLTN+NAME ARE ALL ' '
                    if (condition(pnd_Desig_Tab_Pnd_Spcl_Txt1.getValue(pnd_K1).lessOrEqual(" ")))                                                                         //Natural: IF #SPCL-TXT1 ( #K1 ) LE ' '
                    {
                        pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Beneficiary Name is blank for Beneficiary #", pnd_K1));                        //Natural: COMPRESS 'Beneficiary Name is blank for Beneficiary #' #K1 TO BENA9820.#ERROR-MSG
                        pnd_Error_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #ERROR-CNT
                        //* CREATE A REL CODE OF 38/OTHER 'unknown': SPCL-TXT & NAME ' '
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  ONLY -CDE PASSED MUST
                        pnd_Desig_Tab_Pnd_Rltn_Cde.getValue(pnd_K1).setValue("38");                                                                                       //Natural: MOVE '38' TO #RLTN-CDE ( #K1 ) #DSGNTN-RLTN-CDE ( #K1 )
                        pdaBena9820.getBena9820_Pnd_Dsgntn_Rltn_Cde().getValue(pnd_K1).setValue("38");
                        pnd_Desig_Tab_Pnd_Rltn_Free_Txt.getValue(pnd_K1).setValue("OTHER");                                                                               //Natural: ASSIGN #RLTN-FREE-TXT ( #K1 ) := 'OTHER'
                    }                                                                                                                                                     //Natural: END-IF
                    //*  RESET DOB IN BENN9802
                    //*  IF ACIS DOB INVALID
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN #RLTN-CDE ( #K1 ) = '38' AND #RLTN-FREE-TXT ( #K1 ) LE ' '
            if (condition(pnd_Desig_Tab_Pnd_Rltn_Cde.getValue(pnd_K1).equals("38") && pnd_Desig_Tab_Pnd_Rltn_Free_Txt.getValue(pnd_K1).lessOrEqual(" ")))
            {
                decideConditionsMet800++;
                pnd_Desig_Tab_Pnd_Rltn_Free_Txt.getValue(pnd_K1).setValue("OTHER");                                                                                       //Natural: ASSIGN #RLTN-FREE-TXT ( #K1 ) := 'OTHER'
            }                                                                                                                                                             //Natural: WHEN BENA9801.INTRFCNG-SYSTM NE 'ACIS' AND #DTE-BIRTH-TRUST ( #K1 ) GT ' ' AND ( #DTE-BIRTH-TRUST ( #K1 ) NE MASK ( YYYYMMDD ) OR #DTE-BIRTH-TRUST ( #K1 ) GT #BSNSS-DTE OR #DTE-BIRTH-TRUST ( #K1 ) LT '16000000' )
            if (condition(((pdaBena9801.getBena9801_Intrfcng_Systm().notEquals("ACIS") && pnd_Desig_Tab_Pnd_Dte_Birth_Trust.getValue(pnd_K1).greater(" ")) 
                && ((! (DbsUtil.maskMatches(pnd_Desig_Tab_Pnd_Dte_Birth_Trust.getValue(pnd_K1),"YYYYMMDD")) || pnd_Desig_Tab_Pnd_Dte_Birth_Trust.getValue(pnd_K1).greater(pdaBena9820.getBena9820_Pnd_Bsnss_Dte())) 
                || pnd_Desig_Tab_Pnd_Dte_Birth_Trust.getValue(pnd_K1).less("16000000")))))
            {
                decideConditionsMet800++;
                pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Date of Birth/Trust invalid for Beneficiary #", pnd_K1, "DOB:", pnd_Desig_Tab_Pnd_Dte_Birth_Trust.getValue(pnd_K1))); //Natural: COMPRESS 'Date of Birth/Trust invalid for Beneficiary #' #K1 'DOB:' #DTE-BIRTH-TRUST ( #K1 ) TO BENA9820.#ERROR-MSG
                pnd_Error_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #ERROR-CNT
            }                                                                                                                                                             //Natural: WHEN #SSN-CDE ( #K1 ) GT ' '
            if (condition(pnd_Desig_Tab_Pnd_Ssn_Cde.getValue(pnd_K1).greater(" ")))
            {
                decideConditionsMet800++;
                pnd_Ss_Code_Found.reset();                                                                                                                                //Natural: RESET #SS-CODE-FOUND
                FOR05:                                                                                                                                                    //Natural: FOR #T-I = 1 TO 10
                for (pnd_T_I.setValue(1); condition(pnd_T_I.lessOrEqual(10)); pnd_T_I.nadd(1))
                {
                    if (condition(pdaBena9834.getBena9834_Pnd_Ss_Tab_Key().getValue(pnd_T_I).equals(" ")))                                                                //Natural: IF #SS-TAB-KEY ( #T-I ) = ' '
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Desig_Tab_Pnd_Ssn_Cde.getValue(pnd_K1).equals(pdaBena9834.getBena9834_Pnd_Ss_Tab_Key().getValue(pnd_T_I))))                         //Natural: IF #SSN-CDE ( #K1 ) = #SS-TAB-KEY ( #T-I )
                    {
                        pnd_Ss_Code_Found.setValue(true);                                                                                                                 //Natural: ASSIGN #SS-CODE-FOUND := TRUE
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(! (pnd_Ss_Code_Found.getBoolean())))                                                                                                        //Natural: IF NOT #SS-CODE-FOUND
                {
                    pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Invalid Social Security Code:", pnd_Desig_Tab_Pnd_Ssn_Cde.getValue(pnd_K1),        //Natural: COMPRESS 'Invalid Social Security Code:' #SSN-CDE ( #K1 ) 'for Beneficiary #' #K1 TO BENA9820.#ERROR-MSG
                        "for Beneficiary #", pnd_K1));
                    pnd_Error_Cnt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #ERROR-CNT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Desig_Tab_Pnd_Ssn.getValue(pnd_K1).lessOrEqual(" ")))                                                                               //Natural: IF #SSN ( #K1 ) LE ' '
                    {
                        pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Social Security Code but no SSN for Beneficiary #", pnd_K1));                  //Natural: COMPRESS 'Social Security Code but no SSN for Beneficiary #' #K1 TO BENA9820.#ERROR-MSG
                        pnd_Error_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #ERROR-CNT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN #SSN ( #K1 ) GT ' '
            if (condition(pnd_Desig_Tab_Pnd_Ssn.getValue(pnd_K1).greater(" ")))
            {
                decideConditionsMet800++;
                if (condition(pnd_Desig_Tab_Pnd_Ssn_Cde.getValue(pnd_K1).lessOrEqual(" ")))                                                                               //Natural: IF #SSN-CDE ( #K1 ) LE ' '
                {
                    pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("SSN without Social Security Code for Beneficiary #", pnd_K1));                     //Natural: COMPRESS 'SSN without Social Security Code for Beneficiary #' #K1 TO BENA9820.#ERROR-MSG
                    pnd_Error_Cnt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #ERROR-CNT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(! (DbsUtil.maskMatches(pnd_Desig_Tab_Pnd_Ssn.getValue(pnd_K1),"NNNNNNNNN"))))                                                           //Natural: IF #SSN ( #K1 ) NE MASK ( NNNNNNNNN )
                    {
                        pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Social Security Number:", pnd_Desig_Tab_Pnd_Ssn.getValue(pnd_K1),              //Natural: COMPRESS 'Social Security Number:' #SSN ( #K1 ) 'not 9 digits for Beneficiary #' #K1 TO BENA9820.#ERROR-MSG
                            "not 9 digits for Beneficiary #", pnd_K1));
                        pnd_Error_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #ERROR-CNT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN NOT ( #PRCNT-FRCTN-IND ( #K1 ) = 'P' OR = 'F' )
            if (condition(! ((pnd_Desig_Tab_Pnd_Prcnt_Frctn_Ind.getValue(pnd_K1).equals("P") || pnd_Desig_Tab_Pnd_Prcnt_Frctn_Ind.getValue(pnd_K1).equals("F")))))
            {
                decideConditionsMet800++;
                if (condition(! ((pnd_Desig_Tab_Pnd_Prcnt_Frctn_Ind.getValue(pnd_K1).equals(" ") && ((pdaBena9801.getBena9801_Intrfcng_Systm().equals("ACIS")             //Natural: IF NOT ( #PRCNT-FRCTN-IND ( #K1 ) = ' ' AND ( BENA9801.INTRFCNG-SYSTM = 'ACIS' OR BENA9801.INTRFCNG-SYSTM = 'PA' OR BENA9801.INTRFCNG-SYSTM = 'ISV' ) )
                    || pdaBena9801.getBena9801_Intrfcng_Systm().equals("PA")) || pdaBena9801.getBena9801_Intrfcng_Systm().equals("ISV"))))))
                {
                    pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Percent/Fraction indicator:", pnd_Desig_Tab_Pnd_Prcnt_Frctn_Ind.getValue(pnd_K1),  //Natural: COMPRESS 'Percent/Fraction indicator:' #PRCNT-FRCTN-IND ( #K1 ) 'not P/F for beneficiary #' #K1 TO BENA9820.#ERROR-MSG
                        "not P/F for beneficiary #", pnd_K1));
                    pnd_Error_Cnt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #ERROR-CNT
                    //*  ALLOW ONLY 1ST C BENE TO HAVE 0 %
                    //*  ALLOW ONLY 1ST P BENE TO HAVE 0 %
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN #PRCNT-FRCTN-IND ( #K1 ) = 'P' AND #SHARE-PRCNT ( #K1 ) LE 0 AND NOT ( #1ST-C-BENE OR ( #K1 = 1 AND #BENE-TYPE ( #K1 ) = 'P' ) )
            if (condition(((pnd_Desig_Tab_Pnd_Prcnt_Frctn_Ind.getValue(pnd_K1).equals("P") && pnd_Desig_Tab_Pnd_Share_Prcnt.getValue(pnd_K1).lessOrEqual(getZero())) 
                && ! ((pnd_1st_C_Bene.getBoolean() || (pnd_K1.equals(1) && pnd_Desig_Tab_Pnd_Bene_Type.getValue(pnd_K1).equals("P")))))))
            {
                decideConditionsMet800++;
                pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Percent/Fraction ind P without percentage for Beneficiary #", pnd_K1));                //Natural: COMPRESS 'Percent/Fraction ind P without percentage for Beneficiary #' #K1 TO BENA9820.#ERROR-MSG
                //*  ALLOW ONLY 1ST C BENE TO HAVE NO ALLOC.
                //*  ALLOW ONLY 1ST P BENE TO HAVE 0 %
                pnd_Error_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #ERROR-CNT
            }                                                                                                                                                             //Natural: WHEN #PRCNT-FRCTN-IND ( #K1 ) = 'F' AND #SHARE-NMRTR ( #K1 ) LE 0 AND #SHARE-DNMNTR ( #K1 ) LE 0 AND NOT ( #1ST-C-BENE OR ( #K1 = 1 AND #BENE-TYPE ( #K1 ) = 'P' ) )
            if (condition((((pnd_Desig_Tab_Pnd_Prcnt_Frctn_Ind.getValue(pnd_K1).equals("F") && pnd_Desig_Tab_Pnd_Share_Nmrtr.getValue(pnd_K1).lessOrEqual(getZero())) 
                && pnd_Desig_Tab_Pnd_Share_Dnmntr.getValue(pnd_K1).lessOrEqual(getZero())) && ! ((pnd_1st_C_Bene.getBoolean() || (pnd_K1.equals(1) && pnd_Desig_Tab_Pnd_Bene_Type.getValue(pnd_K1).equals("P")))))))
            {
                decideConditionsMet800++;
                pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Percent/Fraction ind F without Numerator/Denominator.", "Beneficiary #",               //Natural: COMPRESS 'Percent/Fraction ind F without Numerator/Denominator.' 'Beneficiary #' #K1 TO BENA9820.#ERROR-MSG
                    pnd_K1));
                pnd_Error_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #ERROR-CNT
            }                                                                                                                                                             //Natural: WHEN #PRCNT-FRCTN-IND ( #K1 ) = 'F' AND #SHARE-NMRTR ( #K1 ) LE 0 AND NOT #SHARE-DNMNTR ( #K1 ) LE 0
            if (condition(pnd_Desig_Tab_Pnd_Prcnt_Frctn_Ind.getValue(pnd_K1).equals("F") && pnd_Desig_Tab_Pnd_Share_Nmrtr.getValue(pnd_K1).lessOrEqual(getZero()) 
                && ! (pnd_Desig_Tab_Pnd_Share_Dnmntr.getValue(pnd_K1).lessOrEqual(getZero()))))
            {
                decideConditionsMet800++;
                pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Percent/Fraction ind F without Numerator for Beneficiary #", pnd_K1));                 //Natural: COMPRESS 'Percent/Fraction ind F without Numerator for Beneficiary #' #K1 TO BENA9820.#ERROR-MSG
                pnd_Error_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #ERROR-CNT
            }                                                                                                                                                             //Natural: WHEN #PRCNT-FRCTN-IND ( #K1 ) = 'F' AND #SHARE-DNMNTR ( #K1 ) LE 0 AND NOT #SHARE-NMRTR ( #K1 ) LE 0
            if (condition(pnd_Desig_Tab_Pnd_Prcnt_Frctn_Ind.getValue(pnd_K1).equals("F") && pnd_Desig_Tab_Pnd_Share_Dnmntr.getValue(pnd_K1).lessOrEqual(getZero()) 
                && ! (pnd_Desig_Tab_Pnd_Share_Nmrtr.getValue(pnd_K1).lessOrEqual(getZero()))))
            {
                decideConditionsMet800++;
                pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Percent/Fraction ind F without Denominator for Beneficiary #", pnd_K1));               //Natural: COMPRESS 'Percent/Fraction ind F without Denominator for Beneficiary #' #K1 TO BENA9820.#ERROR-MSG
                pnd_Error_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #ERROR-CNT
            }                                                                                                                                                             //Natural: WHEN #PRCNT-FRCTN-IND ( #K1 ) = 'F' AND #SHARE-NMRTR ( #K1 ) GT #SHARE-DNMNTR ( #K1 ) AND NOT #SHARE-DNMNTR ( #K1 ) LE 0
            if (condition(pnd_Desig_Tab_Pnd_Prcnt_Frctn_Ind.getValue(pnd_K1).equals("F") && pnd_Desig_Tab_Pnd_Share_Nmrtr.getValue(pnd_K1).greater(pnd_Desig_Tab_Pnd_Share_Dnmntr.getValue(pnd_K1)) 
                && ! (pnd_Desig_Tab_Pnd_Share_Dnmntr.getValue(pnd_K1).lessOrEqual(getZero()))))
            {
                decideConditionsMet800++;
                pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Numerator is greater than denominator for Beneficiary #", pnd_K1));                    //Natural: COMPRESS 'Numerator is greater than denominator for Beneficiary #' #K1 TO BENA9820.#ERROR-MSG
                pnd_Error_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #ERROR-CNT
            }                                                                                                                                                             //Natural: WHEN NOT #IRRVCBL-IND ( #K1 ) = #Y-N-OR-BLANK ( * )
            if (condition(! (pnd_Desig_Tab_Pnd_Irrvcbl_Ind.getValue(pnd_K1).equals(pnd_Y_N_Tab_Pnd_Y_N_Or_Blank.getValue("*")))))
            {
                decideConditionsMet800++;
                pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Designation level Irrevocable Ind invalid:", pnd_Desig_Tab_Pnd_Irrvcbl_Ind.getValue(pnd_K1),  //Natural: COMPRESS 'Designation level Irrevocable Ind invalid:' #IRRVCBL-IND ( #K1 ) 'for Beneficiary #' #K1 TO BENA9820.#ERROR-MSG
                    "for Beneficiary #", pnd_K1));
                pnd_Error_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #ERROR-CNT
            }                                                                                                                                                             //Natural: WHEN #STTLMNT-RSTRCTN ( #K1 ) GT ' '
            if (condition(pnd_Desig_Tab_Pnd_Sttlmnt_Rstrctn.getValue(pnd_K1).greater(" ")))
            {
                decideConditionsMet800++;
                //*  ACCORDING TO EDITS IN CIS THIS
                if (condition(pnd_Desig_Tab_Pnd_Sttlmnt_Rstrctn.getValue(pnd_K1).equals("E")))                                                                            //Natural: IF #STTLMNT-RSTRCTN ( #K1 ) = 'E'
                {
                    //*  SHOULD NOT HAPPEN - ADAM WITH ONE / BOTH LS/AC BLANK? SEE CISP2081.
                    pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Adam with Lump Sum / Auto Commuted conflict", "for Beneficiary #",                 //Natural: COMPRESS 'Adam with Lump Sum / Auto Commuted conflict' 'for Beneficiary #' #K1 TO BENA9820.#ERROR-MSG
                        pnd_K1));
                    pnd_Error_Cnt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #ERROR-CNT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Sr_Code_Found.reset();                                                                                                                            //Natural: RESET #SR-CODE-FOUND
                    FOR06:                                                                                                                                                //Natural: FOR #T-I = 1 TO 10
                    for (pnd_T_I.setValue(1); condition(pnd_T_I.lessOrEqual(10)); pnd_T_I.nadd(1))
                    {
                        if (condition(pdaBena9834.getBena9834_Pnd_Sr_Tab_Key().getValue(pnd_T_I).equals(" ")))                                                            //Natural: IF #SR-TAB-KEY ( #T-I ) = ' '
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pnd_Desig_Tab_Pnd_Sttlmnt_Rstrctn.getValue(pnd_K1).equals(pdaBena9834.getBena9834_Pnd_Sr_Tab_Key().getValue(pnd_T_I))))             //Natural: IF #STTLMNT-RSTRCTN ( #K1 ) = #SR-TAB-KEY ( #T-I )
                        {
                            pnd_Sr_Code_Found.setValue(true);                                                                                                             //Natural: ASSIGN #SR-CODE-FOUND := TRUE
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(! (pnd_Sr_Code_Found.getBoolean())))                                                                                                    //Natural: IF NOT #SR-CODE-FOUND
                    {
                        pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Invalid Settlement Restriction Code:", pnd_Desig_Tab_Pnd_Sttlmnt_Rstrctn.getValue(pnd_K1),  //Natural: COMPRESS 'Invalid Settlement Restriction Code:' #STTLMNT-RSTRCTN ( #K1 ) 'for Beneficiary #' #K1 TO BENA9820.#ERROR-MSG
                            "for Beneficiary #", pnd_K1));
                        pnd_Error_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #ERROR-CNT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN NOT #DFLT-ESTATE ( #K1 ) = #Y-N-OR-BLANK ( * )
            if (condition(! (pnd_Desig_Tab_Pnd_Dflt_Estate.getValue(pnd_K1).equals(pnd_Y_N_Tab_Pnd_Y_N_Or_Blank.getValue("*")))))
            {
                decideConditionsMet800++;
                pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Default to Estate Ind invalid:", pnd_Desig_Tab_Pnd_Dflt_Estate.getValue(pnd_K1),       //Natural: COMPRESS 'Default to Estate Ind invalid:' #DFLT-ESTATE ( #K1 ) 'for Beneficiary #' #K1 TO BENA9820.#ERROR-MSG
                    "for Beneficiary #", pnd_K1));
                pnd_Error_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #ERROR-CNT
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet800 > 0))
            {
                if (condition(pdaBena9820.getBena9820_Pnd_Error_Msg().greater(" ")))                                                                                      //Natural: IF BENA9820.#ERROR-MSG GT ' '
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet800 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  NEW "Incompatible Data" EDITS
            if (condition(pnd_Desig_Tab_Pnd_Bene_Name1.getValue(pnd_K1).greater(" ")))                                                                                    //Natural: IF #BENE-NAME1 ( #K1 ) GT ' '
            {
                pnd_Test_Lit.setValue(pnd_Desig_Tab_Pnd_Bene_Name1.getValue(pnd_K1), MoveOption.LeftJustified);                                                           //Natural: MOVE LEFT #BENE-NAME1 ( #K1 ) TO #TEST-LIT
                pnd_Same_As.setValue(pnd_Desig_Tab_Pnd_Bene_Name1.getValue(pnd_K1), MoveOption.LeftJustified);                                                            //Natural: MOVE LEFT #BENE-NAME1 ( #K1 ) TO #SAME-AS
                DbsUtil.examine(new ExamineSource(pnd_Same_As_Pnd_Same_As_Txt), new ExamineTranslate(TranslateOption.Upper));                                             //Natural: EXAMINE #SAME-AS-TXT TRANSLATE INTO UPPER CASE
                pnd_Name_Only_This_Bene.reset();                                                                                                                          //Natural: RESET #NAME-ONLY-THIS-BENE
                if (condition((((((((pnd_Desig_Tab_Pnd_Bene_Name2.getValue(pnd_K1).equals(" ") && pnd_Desig_Tab_Pnd_Dte_Birth_Trust.getValue(pnd_K1).equals(" "))         //Natural: IF #BENE-NAME2 ( #K1 ) = ' ' AND #DTE-BIRTH-TRUST ( #K1 ) = ' ' AND #SSN ( #K1 ) = ' ' AND #IRRVCBL-IND ( #K1 ) NE 'Y' AND ( #STTLMNT-RSTRCTN ( #K1 ) = ' ' OR = 'N' ) AND #SPCL-TXT1 ( #K1 ) = ' ' AND #SPCL-TXT2 ( #K1 ) = ' ' AND #SPCL-TXT3 ( #K1 ) = ' '
                    && pnd_Desig_Tab_Pnd_Ssn.getValue(pnd_K1).equals(" ")) && pnd_Desig_Tab_Pnd_Irrvcbl_Ind.getValue(pnd_K1).notEquals("Y")) && (pnd_Desig_Tab_Pnd_Sttlmnt_Rstrctn.getValue(pnd_K1).equals(" ") 
                    || pnd_Desig_Tab_Pnd_Sttlmnt_Rstrctn.getValue(pnd_K1).equals("N"))) && pnd_Desig_Tab_Pnd_Spcl_Txt1.getValue(pnd_K1).equals(" ")) && 
                    pnd_Desig_Tab_Pnd_Spcl_Txt2.getValue(pnd_K1).equals(" ")) && pnd_Desig_Tab_Pnd_Spcl_Txt3.getValue(pnd_K1).equals(" "))))
                {
                    if (condition(pnd_Inval_Non_Acis_Rel.getBoolean()))                                                                                                   //Natural: IF #INVAL-NON-ACIS-REL
                    {
                        pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Invalid Relationship for Beneficiary #", pnd_K1));                             //Natural: COMPRESS 'Invalid Relationship for Beneficiary #' #K1 TO BENA9820.#ERROR-MSG
                        pdaBena9820.getBena9820_Pnd_Error_Cde().setValue("INVRL");                                                                                        //Natural: ASSIGN BENA9820.#ERROR-CDE := 'INVRL'
                        pnd_Error_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #ERROR-CNT
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                    //*  THE REST OF THE DATA IS BLANK
                    if (condition(pnd_Desig_Tab_Pnd_Rltn_Cde.getValue(pnd_K1).equals(" ") && pnd_Desig_Tab_Pnd_Rltn_Free_Txt.getValue(pnd_K1).equals(" ")))               //Natural: IF #RLTN-CDE ( #K1 ) = ' ' AND #RLTN-FREE-TXT ( #K1 ) = ' '
                    {
                        pnd_Name_Only_This_Bene.setValue(true);                                                                                                           //Natural: ASSIGN #NAME-ONLY-THIS-BENE := TRUE
                        if (condition(pnd_K1.equals(1)))                                                                                                                  //Natural: IF #K1 = 1
                        {
                            pnd_1st_Primary_Name_Only.setValue(true);                                                                                                     //Natural: ASSIGN #1ST-PRIMARY-NAME-ONLY := TRUE
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pnd_1st_C_Bene.getBoolean()))                                                                                                       //Natural: IF #1ST-C-BENE
                        {
                            pnd_1st_C_Bene_Name_Only.setValue(true);                                                                                                      //Natural: ASSIGN #1ST-C-BENE-NAME-ONLY := TRUE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //* 'E'RRORS SHOULD ALWAYS PRECEDE 'to MOS'
                short decideConditionsMet978 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #TEST-LIT = '.' OR = '1' OR = '-' OR = '0'
                if (condition(pnd_Test_Lit.equals(".") || pnd_Test_Lit.equals("1") || pnd_Test_Lit.equals("-") || pnd_Test_Lit.equals("0")))
                {
                    decideConditionsMet978++;
                    pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Bene Name has an invalid value:", pnd_Desig_Tab_Pnd_Bene_Name1.getValue(pnd_K1),   //Natural: COMPRESS 'Bene Name has an invalid value:' #BENE-NAME1 ( #K1 ) 'at occurence:' #K1 TO BENA9820.#ERROR-MSG
                        "at occurence:", pnd_K1));
                    pdaBena9820.getBena9820_Pnd_Error_Cde().setValue("INVNM");                                                                                            //Natural: ASSIGN BENA9820.#ERROR-CDE := 'INVNM'
                }                                                                                                                                                         //Natural: WHEN #SAME-AS = 'NONE' AND NOT #1ST-C-BENE
                else if (condition(pnd_Same_As.equals("NONE") && ! (pnd_1st_C_Bene.getBoolean())))
                {
                    decideConditionsMet978++;
                    //*  NO MORE CONTINGENTS
                    pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Bene Name is NONE at occurence:", pnd_K1));                                        //Natural: COMPRESS 'Bene Name is NONE at occurence:' #K1 TO BENA9820.#ERROR-MSG
                    pdaBena9820.getBena9820_Pnd_Error_Cde().setValue("NONE");                                                                                             //Natural: ASSIGN BENA9820.#ERROR-CDE := 'NONE'
                }                                                                                                                                                         //Natural: WHEN #SAME-AS = 'NONE' AND #1ST-C-BENE AND NOT ( #K1 = #I1 AND #NAME-ONLY-THIS-BENE )
                else if (condition(pnd_Same_As.equals("NONE") && pnd_1st_C_Bene.getBoolean() && ! (pnd_K1.equals(pnd_I1) && pnd_Name_Only_This_Bene.getBoolean())))
                {
                    decideConditionsMet978++;
                    pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Bene Name is NONE at occurence:", pnd_K1));                                        //Natural: COMPRESS 'Bene Name is NONE at occurence:' #K1 TO BENA9820.#ERROR-MSG
                    pdaBena9820.getBena9820_Pnd_Error_Cde().setValue("NONE");                                                                                             //Natural: ASSIGN BENA9820.#ERROR-CDE := 'NONE'
                }                                                                                                                                                         //Natural: WHEN #SAME-AS-TXT = 'SAME AS' OR = 'SME AS '
                else if (condition(pnd_Same_As_Pnd_Same_As_Txt.equals("SAME AS") || pnd_Same_As_Pnd_Same_As_Txt.equals("SME AS ")))
                {
                    decideConditionsMet978++;
                    pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Same As:", pnd_Desig_Tab_Pnd_Bene_Name1.getValue(pnd_K1), "; at Name #",           //Natural: COMPRESS 'Same As:' #BENE-NAME1 ( #K1 ) '; at Name #' #K1 TO BENA9820.#ERROR-MSG
                        pnd_K1));
                    pdaBena9820.getBena9820_Pnd_Error_Cde().setValue("SAME");                                                                                             //Natural: ASSIGN BENA9820.#ERROR-CDE := 'SAME'
                }                                                                                                                                                         //Natural: WHEN #SEE-TXT = 'SEE '
                else if (condition(pnd_Same_As_Pnd_See_Txt.equals("SEE ")))
                {
                    decideConditionsMet978++;
                    //*  1ST PRIMARY OR CONTINGENT
                    //*  NAME IS ONLY ONE BYTE
                    pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("'SEE' Error:", pnd_Desig_Tab_Pnd_Bene_Name1.getValue(pnd_K1), "; at Name #",       //Natural: COMPRESS '"SEE" Error:' #BENE-NAME1 ( #K1 ) '; at Name #' #K1 TO BENA9820.#ERROR-MSG
                        pnd_K1));
                    pdaBena9820.getBena9820_Pnd_Error_Cde().setValue("SEE ");                                                                                             //Natural: ASSIGN BENA9820.#ERROR-CDE := 'SEE '
                }                                                                                                                                                         //Natural: WHEN #NAME-ONLY-THIS-BENE AND ( #1ST-C-BENE OR #K1 = 1 ) AND #TEST-REST-BYTE = ' ' AND #TEST-1ST-BYTE GT ' '
                else if (condition((((pnd_Name_Only_This_Bene.getBoolean() && (pnd_1st_C_Bene.getBoolean() || pnd_K1.equals(1))) && pnd_Test_Lit_Pnd_Test_Rest_Byte.equals(" ")) 
                    && pnd_Test_Lit_Pnd_Test_1st_Byte.greater(" "))))
                {
                    decideConditionsMet978++;
                    //*  1ST PRIMARY/CONTINGENT
                    pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Bene Name only one byte long:", pnd_Desig_Tab_Pnd_Bene_Name1.getValue(pnd_K1),     //Natural: COMPRESS 'Bene Name only one byte long:' #BENE-NAME1 ( #K1 ) 'at occurence:' #K1 TO BENA9820.#ERROR-MSG
                        "at occurence:", pnd_K1));
                }                                                                                                                                                         //Natural: WHEN ( #NAME-ONLY-THIS-BENE OR #1ST-PRIMARY-NAME-ONLY OR #1ST-C-BENE-NAME-ONLY ) AND NOT ( #1ST-C-BENE OR #K1 = 1 )
                else if (condition((((pnd_Name_Only_This_Bene.getBoolean() || pnd_1st_Primary_Name_Only.getBoolean()) || pnd_1st_C_Bene_Name_Only.getBoolean()) 
                    && ! ((pnd_1st_C_Bene.getBoolean() || pnd_K1.equals(1))))))
                {
                    decideConditionsMet978++;
                    if (condition(pdaBena9801.getBena9801_Intrfcng_Systm().equals("ACIS")))                                                                               //Natural: IF BENA9801.INTRFCNG-SYSTM = 'ACIS'
                    {
                        pdaBena9820.getBena9820_Pnd_Interface_As_Mos().setValue(true);                                                                                    //Natural: ASSIGN BENA9820.#INTERFACE-AS-MOS := TRUE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pdaBena9820.getBena9820_Pnd_Error_Msg().setValue("More than 1 Primary/Contingent and no data besides Name");                                      //Natural: MOVE 'More than 1 Primary/Contingent and no data besides Name' TO BENA9820.#ERROR-MSG
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN ANY
                if (condition(decideConditionsMet978 > 0))
                {
                    if (condition(pdaBena9820.getBena9820_Pnd_Error_Msg().greater(" ")))                                                                                  //Natural: IF BENA9820.#ERROR-MSG GT ' '
                    {
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                //*  SO ALLOW THROUGH ANYHOW
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-TEXT-ALLOCATIONS
                sub_Check_For_Text_Allocations();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pdaBena9820.getBena9820_Pnd_Error_Msg().greater(" ")))                                                                                      //Natural: IF BENA9820.#ERROR-MSG GT ' '
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Desig_Tab_Pnd_Spcl_Txt1.getValue(pnd_K1).greater(" ") || pnd_Desig_Tab_Pnd_Spcl_Txt2.getValue(pnd_K1).greater(" ") || pnd_Desig_Tab_Pnd_Spcl_Txt3.getValue(pnd_K1).greater(" "))) //Natural: IF #SPCL-TXT1 ( #K1 ) GT ' ' OR #SPCL-TXT2 ( #K1 ) GT ' ' OR #SPCL-TXT3 ( #K1 ) GT ' '
            {
                pnd_Spcl_Txt_Indexed.getValue(1).setValue(pnd_Desig_Tab_Pnd_Spcl_Txt1.getValue(pnd_K1));                                                                  //Natural: ASSIGN #SPCL-TXT-INDEXED ( 1 ) := #SPCL-TXT1 ( #K1 )
                pnd_Spcl_Txt_Indexed.getValue(2).setValue(pnd_Desig_Tab_Pnd_Spcl_Txt2.getValue(pnd_K1));                                                                  //Natural: ASSIGN #SPCL-TXT-INDEXED ( 2 ) := #SPCL-TXT2 ( #K1 )
                pnd_Spcl_Txt_Indexed.getValue(3).setValue(pnd_Desig_Tab_Pnd_Spcl_Txt3.getValue(pnd_K1));                                                                  //Natural: ASSIGN #SPCL-TXT-INDEXED ( 3 ) := #SPCL-TXT3 ( #K1 )
                FOR07:                                                                                                                                                    //Natural: FOR #ST = 1 TO 3
                for (pnd_St.setValue(1); condition(pnd_St.lessOrEqual(3)); pnd_St.nadd(1))
                {
                    if (condition(pnd_Spcl_Txt_Indexed.getValue(pnd_St).equals(" ")))                                                                                     //Natural: IF #SPCL-TXT-INDEXED ( #ST ) = ' '
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Same_As.setValue(pnd_Spcl_Txt_Indexed.getValue(pnd_St), MoveOption.LeftJustified);                                                                //Natural: MOVE LEFT #SPCL-TXT-INDEXED ( #ST ) TO #SAME-AS
                    DbsUtil.examine(new ExamineSource(pnd_Same_As_Pnd_Same_As_Txt), new ExamineTranslate(TranslateOption.Upper));                                         //Natural: EXAMINE #SAME-AS-TXT TRANSLATE INTO UPPER CASE
                    short decideConditionsMet1033 = 0;                                                                                                                    //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #SAME-AS-TXT = 'SAME AS' OR = 'SME AS '
                    if (condition(pnd_Same_As_Pnd_Same_As_Txt.equals("SAME AS") || pnd_Same_As_Pnd_Same_As_Txt.equals("SME AS ")))
                    {
                        decideConditionsMet1033++;
                        pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Same As:", pnd_Same_As, "; in Special Text for #", pnd_K1));                   //Natural: COMPRESS 'Same As:' #SAME-AS '; in Special Text for #' #K1 TO BENA9820.#ERROR-MSG
                        pdaBena9820.getBena9820_Pnd_Error_Cde().setValue("SAME");                                                                                         //Natural: ASSIGN BENA9820.#ERROR-CDE := 'SAME'
                    }                                                                                                                                                     //Natural: WHEN #SEE-TXT = 'SEE '
                    else if (condition(pnd_Same_As_Pnd_See_Txt.equals("SEE ")))
                    {
                        decideConditionsMet1033++;
                        if (condition((pnd_Desig_Tab_Pnd_Bene_Name1.getValue(pnd_K1).equals(" ") || (pnd_Desig_Tab_Pnd_Bene_Name1.getValue(pnd_K1).greater(" ")           //Natural: IF #BENE-NAME1 ( #K1 ) = ' ' OR ( #BENE-NAME1 ( #K1 ) GT ' ' AND #SAME-AS-TXT = 'SEE FILE' OR = 'SEE FOLD' )
                            && (pnd_Same_As_Pnd_Same_As_Txt.equals("SEE FILE") || pnd_Same_As_Pnd_Same_As_Txt.equals("SEE FOLD"))))))
                        {
                            pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("'SEE' Error:", pnd_Same_As, "; in Special Text for #",                     //Natural: COMPRESS '"SEE" Error:' #SAME-AS '; in Special Text for #' #K1 TO BENA9820.#ERROR-MSG
                                pnd_K1));
                            pdaBena9820.getBena9820_Pnd_Error_Cde().setValue("SEE ");                                                                                     //Natural: ASSIGN BENA9820.#ERROR-CDE := 'SEE '
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: WHEN ANY
                    if (condition(decideConditionsMet1033 > 0))
                    {
                        if (condition(pdaBena9820.getBena9820_Pnd_Error_Msg().greater(" ")))                                                                              //Natural: IF BENA9820.#ERROR-MSG GT ' '
                        {
                            if (true) return;                                                                                                                             //Natural: ESCAPE ROUTINE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  NEW "Incompatible Data" EDITS
            if (condition(pnd_Desig_Tab_Pnd_Bene_Type.getValue(pnd_K1).equals("C")))                                                                                      //Natural: IF #BENE-TYPE ( #K1 ) = 'C'
            {
                pnd_Num_Benes_C.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #NUM-BENES-C
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Num_Benes_P.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #NUM-BENES-P
            }                                                                                                                                                             //Natural: END-IF
            //*  INCOMPATIBLE DATA EDITS CAN ONLY PUT ACIS INTO MOS JW 12/15
            if (condition(pdaBena9801.getBena9801_Intrfcng_Systm().equals("ACIS")))                                                                                       //Natural: IF BENA9801.INTRFCNG-SYSTM = 'ACIS'
            {
                //*  1 FOR NAME ETC. LINE AND 1 FOR BLANK
                if (condition(pnd_Desig_Tab_Pnd_Bene_Name2.getValue(pnd_K1).equals(" ")))                                                                                 //Natural: IF #BENE-NAME2 ( #K1 ) = ' '
                {
                    pnd_Num_Poss_Mos_Lines.nadd(2);                                                                                                                       //Natural: ADD 2 TO #NUM-POSS-MOS-LINES
                    //*  2 FOR NAME ETC. LINE AND 1 FOR BLANK
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Num_Poss_Mos_Lines.nadd(3);                                                                                                                       //Natural: ADD 3 TO #NUM-POSS-MOS-LINES
                }                                                                                                                                                         //Natural: END-IF
                //*  LINE AFTER BENE
                if (condition(pnd_Desig_Tab_Pnd_Spcl_Txt1.getValue(pnd_K1).greater(" ")))                                                                                 //Natural: IF #SPCL-TXT1 ( #K1 ) GT ' '
                {
                    pnd_Num_Poss_Mos_Lines.nadd(1);                                                                                                                       //Natural: ADD 1 TO #NUM-POSS-MOS-LINES
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Desig_Tab_Pnd_Spcl_Txt2.getValue(pnd_K1).greater(" ")))                                                                                 //Natural: IF #SPCL-TXT2 ( #K1 ) GT ' '
                {
                    pnd_Num_Poss_Mos_Lines.nadd(1);                                                                                                                       //Natural: ADD 1 TO #NUM-POSS-MOS-LINES
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Desig_Tab_Pnd_Spcl_Txt3.getValue(pnd_K1).greater(" ")))                                                                                 //Natural: IF #SPCL-TXT3 ( #K1 ) GT ' '
                {
                    pnd_Num_Poss_Mos_Lines.nadd(1);                                                                                                                       //Natural: ADD 1 TO #NUM-POSS-MOS-LINES
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Num_Poss_Mos_Lines.greater(getZero())))                                                                                                         //Natural: IF #NUM-POSS-MOS-LINES GT 0
        {
            //*  FOR PRIMARY HEADER LINE (SEE BENN9802)
            pnd_Num_Poss_Mos_Lines.nadd(1);                                                                                                                               //Natural: ADD 1 TO #NUM-POSS-MOS-LINES
            if (condition(pnd_Num_Benes_C.greater(getZero())))                                                                                                            //Natural: IF #NUM-BENES-C GT 0
            {
                //*  FOR CONTINGENT HEADER LINE
                pnd_Num_Poss_Mos_Lines.nadd(1);                                                                                                                           //Natural: ADD 1 TO #NUM-POSS-MOS-LINES
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet1092 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN BENA9801.IRRVCBLE-IND = 'Y' AND NOT #IRRVCBL-IND ( * ) = 'Y'
        if (condition(pdaBena9801.getBena9801_Irrvcble_Ind().equals("Y") && ! pnd_Desig_Tab_Pnd_Irrvcbl_Ind.getValue("*").equals("Y")))
        {
            decideConditionsMet1092++;
            pdaBena9820.getBena9820_Pnd_Error_Msg().setValue("Contract level Irrevocable Ind 'Y', Des. level not Y for any Beneficiary");                                 //Natural: MOVE 'Contract level Irrevocable Ind "Y", Des. level not Y for any Beneficiary' TO BENA9820.#ERROR-MSG
            pnd_Error_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #ERROR-CNT
        }                                                                                                                                                                 //Natural: WHEN ( DFLT-TO-ESTATE-IND = 'N' OR = ' ' ) AND #DFLT-ESTATE ( 1 ) = 'Y'
        else if (condition(((pdaBena9801.getBena9801_Dflt_To_Estate_Ind().equals("N") || pdaBena9801.getBena9801_Dflt_To_Estate_Ind().equals(" ")) && 
            pnd_Desig_Tab_Pnd_Dflt_Estate.getValue(1).equals("Y"))))
        {
            decideConditionsMet1092++;
            pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Contract level Default to Estate:", pdaBena9801.getBena9801_Dflt_To_Estate_Ind(),          //Natural: COMPRESS 'Contract level Default to Estate:' DFLT-TO-ESTATE-IND 'conflicts with Des. level:' #DFLT-ESTATE ( 1 ) TO BENA9820.#ERROR-MSG
                "conflicts with Des. level:", pnd_Desig_Tab_Pnd_Dflt_Estate.getValue(1)));
            pnd_Error_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #ERROR-CNT
        }                                                                                                                                                                 //Natural: WHEN #I1 GT 1 AND #DFLT-ESTATE ( 2:#I1 ) = 'Y'
        else if (condition(pnd_I1.greater(1) && pnd_Desig_Tab_Pnd_Dflt_Estate.getValue(2,":",pnd_I1).equals("Y")))
        {
            decideConditionsMet1092++;
            pdaBena9820.getBena9820_Pnd_Error_Msg().setValue("Default to Estate Ind='Y' on other than 1st Beneficiary");                                                  //Natural: ASSIGN BENA9820.#ERROR-MSG := 'Default to Estate Ind="Y" on other than 1st Beneficiary'
            pnd_Error_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #ERROR-CNT
        }                                                                                                                                                                 //Natural: WHEN ( PYMNT-CHLD-DCSD-IND = 'C' OR = 'F' ) AND #NUM-BENES-C LE 0
        else if (condition(((pdaBena9801.getBena9801_Pymnt_Chld_Dcsd_Ind().equals("C") || pdaBena9801.getBena9801_Pymnt_Chld_Dcsd_Ind().equals("F")) && 
            pnd_Num_Benes_C.lessOrEqual(getZero()))))
        {
            decideConditionsMet1092++;
            pdaBena9820.getBena9820_Pnd_Error_Msg().setValue("Contingent Bene must exists if 'Pay to ..D'sed is 'C' or 'F'");                                             //Natural: ASSIGN BENA9820.#ERROR-MSG := 'Contingent Bene must exists if "Pay to ..D"sed is "C" or "F"'
            pnd_Error_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #ERROR-CNT
        }                                                                                                                                                                 //Natural: WHEN #PRCNT-FRCTN-IND ( 1:#I1 ) = 'P' AND #PRCNT-FRCTN-IND ( 1:#I1 ) = 'F' AND NOT ( #NUM-BENES-P = 1 AND #NUM-BENES-C = 1 AND #PRCNT-FRCTN-IND ( 1 ) = 'F' AND #BENE-TYPE ( 1 ) = 'P' AND #PRCNT-FRCTN-IND ( 2 ) = 'P' AND #SHARE-PRCNT ( 2 ) = 0 AND #SHARE-NMRTR ( 2 ) = 0 AND #SHARE-DNMNTR ( 2 ) = 0 )
        else if (condition(pnd_Desig_Tab_Pnd_Prcnt_Frctn_Ind.getValue(1,":",pnd_I1).equals("P") && pnd_Desig_Tab_Pnd_Prcnt_Frctn_Ind.getValue(1,":",pnd_I1).equals("F") 
            && ! (pnd_Num_Benes_P.equals(1) && pnd_Num_Benes_C.equals(1) && pnd_Desig_Tab_Pnd_Prcnt_Frctn_Ind.getValue(1).equals("F") && pnd_Desig_Tab_Pnd_Bene_Type.getValue(1).equals("P") 
            && pnd_Desig_Tab_Pnd_Prcnt_Frctn_Ind.getValue(2).equals("P") && pnd_Desig_Tab_Pnd_Share_Prcnt.getValue(2).equals(getZero()) && pnd_Desig_Tab_Pnd_Share_Nmrtr.getValue(2).equals(getZero()) 
            && pnd_Desig_Tab_Pnd_Share_Dnmntr.getValue(2).equals(getZero()))))
        {
            decideConditionsMet1092++;
            FA:                                                                                                                                                           //Natural: FOR #I = 1 TO #I1
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_I1)); pnd_I.nadd(1))
            {
                if (condition(((pnd_Desig_Tab_Pnd_Share_Nmrtr.getValue(pnd_I).equals(getZero()) || pnd_Desig_Tab_Pnd_Share_Dnmntr.getValue(pnd_I).equals(getZero()))      //Natural: IF ( ( #SHARE-NMRTR ( #I ) = 0 OR #SHARE-DNMNTR ( #I ) = 0 ) AND #PRCNT-FRCTN-IND ( #I ) = 'F' ) OR ( #SHARE-PRCNT ( #I ) = 0 AND #PRCNT-FRCTN-IND ( #I ) = 'P' )
                    && pnd_Desig_Tab_Pnd_Prcnt_Frctn_Ind.getValue(pnd_I).equals("F")) || (pnd_Desig_Tab_Pnd_Share_Prcnt.getValue(pnd_I).equals(getZero()) 
                    && pnd_Desig_Tab_Pnd_Prcnt_Frctn_Ind.getValue(pnd_I).equals("P"))))
                {
                    pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Allocation missing or incorrect for Beneficiary #", pnd_I));                       //Natural: COMPRESS 'Allocation missing or incorrect for Beneficiary #' #I TO BENA9820.#ERROR-MSG
                    pnd_Error_Cnt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #ERROR-CNT
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pdaBena9820.getBena9820_Pnd_Error_Msg().setValue("Percent/Fraction ind has Ps & Fs mixed for Benefiaries");                                                   //Natural: ASSIGN BENA9820.#ERROR-MSG := 'Percent/Fraction ind has Ps & Fs mixed for Benefiaries'
            pnd_Error_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #ERROR-CNT
        }                                                                                                                                                                 //Natural: WHEN #PRCNT-FRCTN-IND ( 1:#I1 ) = ' ' AND #PRCNT-FRCTN-IND ( 1:#I1 ) GT ' '
        else if (condition(pnd_Desig_Tab_Pnd_Prcnt_Frctn_Ind.getValue(1,":",pnd_I1).equals(" ") && pnd_Desig_Tab_Pnd_Prcnt_Frctn_Ind.getValue(1,":",pnd_I1).greater(" ")))
        {
            decideConditionsMet1092++;
            pdaBena9820.getBena9820_Pnd_Error_Msg().setValue("Percent/Fraction indicator has blank/non-blank mixed");                                                     //Natural: ASSIGN BENA9820.#ERROR-MSG := 'Percent/Fraction indicator has blank/non-blank mixed'
            //*  ONLY POSSIBLY > 60 WITH SPL. TXT
            pnd_Error_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #ERROR-CNT
        }                                                                                                                                                                 //Natural: WHEN #NUM-POSS-MOS-LINES GT 60
        else if (condition(pnd_Num_Poss_Mos_Lines.greater(60)))
        {
            decideConditionsMet1092++;
            pdaBena9820.getBena9820_Pnd_Error_Msg().setValue("ACIS w/ Special Text has too many lines to convert to MOS");                                                //Natural: ASSIGN BENA9820.#ERROR-MSG := 'ACIS w/ Special Text has too many lines to convert to MOS'
            //*  ACIS HAS NO ALLOCATION
            pnd_Error_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #ERROR-CNT
        }                                                                                                                                                                 //Natural: WHEN #PRCNT-FRCTN-IND ( 1:#I1 ) = ' ' AND ( #SHARE-NMRTR ( * ) GT 0 OR #SHARE-DNMNTR ( * ) GT 0 OR #SHARE-PRCNT ( * ) GT 0 )
        else if (condition(pnd_Desig_Tab_Pnd_Prcnt_Frctn_Ind.getValue(1,":",pnd_I1).equals(" ") && (pnd_Desig_Tab_Pnd_Share_Nmrtr.getValue("*").greater(getZero()) 
            || pnd_Desig_Tab_Pnd_Share_Dnmntr.getValue("*").greater(getZero()) || pnd_Desig_Tab_Pnd_Share_Prcnt.getValue("*").greater(getZero()))))
        {
            decideConditionsMet1092++;
            pdaBena9820.getBena9820_Pnd_Error_Msg().setValue("ACIS contract with allocation - Bene Interface creates allocation for ACIS.");                              //Natural: MOVE 'ACIS contract with allocation - Bene Interface creates allocation for ACIS.' TO BENA9820.#ERROR-MSG
            //*  ERROR IF
            //*  ESTATE &
            //*  TRUST CO-EXIST
            pnd_Error_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #ERROR-CNT
        }                                                                                                                                                                 //Natural: WHEN #NUM-BENES-C GT 1 AND #RLTN-CDE ( #1STC:#I1 ) = '05' AND #RLTN-CDE ( #1STC:#I1 ) = '08'
        else if (condition(pnd_Num_Benes_C.greater(1) && pnd_Desig_Tab_Pnd_Rltn_Cde.getValue(pnd_1stc,":",pnd_I1).equals("05") && pnd_Desig_Tab_Pnd_Rltn_Cde.getValue(pnd_1stc,
            ":",pnd_I1).equals("08")))
        {
            decideConditionsMet1092++;
            //*  ESTATE AND TRUST ARE BOTH LISTED-THIS IS NOT ACCEPTED WHEN THE ESTATE
            //*  FALLS UNDER THE TRUST OR A WILL. CALL THE PARTICIPANT TO ENSURE THAT
            //*  WE UNDERSTAND THE DESIGNATION CORRECTLY. FROM M. TORRINGTON 2/13/01
            pdaBena9820.getBena9820_Pnd_Error_Msg().setValue("Both Estate and Trust listed for Contingents. Call Participnt to clarify.");                                //Natural: MOVE 'Both Estate and Trust listed for Contingents. Call Participnt to clarify.' TO BENA9820.#ERROR-MSG
            //*  ERROR IF
            //*  ESTATE &
            //*  TRUST CO-EXIST
            pnd_Error_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #ERROR-CNT
        }                                                                                                                                                                 //Natural: WHEN #NUM-BENES-P GT 1 AND #RLTN-CDE ( 1:#NUM-BENES-P ) = '05' AND #RLTN-CDE ( 1:#NUM-BENES-P ) = '08'
        else if (condition(pnd_Num_Benes_P.greater(1) && pnd_Desig_Tab_Pnd_Rltn_Cde.getValue(1,":",pnd_Num_Benes_P).equals("05") && pnd_Desig_Tab_Pnd_Rltn_Cde.getValue(1,
            ":",pnd_Num_Benes_P).equals("08")))
        {
            decideConditionsMet1092++;
            pdaBena9820.getBena9820_Pnd_Error_Msg().setValue("Both Estate and Trust listed for Primaries. Call Participant to clarify.");                                 //Natural: MOVE 'Both Estate and Trust listed for Primaries. Call Participant to clarify.' TO BENA9820.#ERROR-MSG
            //*  ERROR IF
            //*  TRUST & ANY OTHER
            //* AND NO ALLOC
            pnd_Error_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #ERROR-CNT
        }                                                                                                                                                                 //Natural: WHEN #NUM-BENES-C GT 1 AND #RLTN-CDE ( #1STC:#I1 ) = '08' AND #RLTN-CDE ( #1STC:#I1 ) NE '08' AND #PRCNT-FRCTN-IND ( #1STC:#I1 ) = ' '
        else if (condition(pnd_Num_Benes_C.greater(1) && pnd_Desig_Tab_Pnd_Rltn_Cde.getValue(pnd_1stc,":",pnd_I1).equals("08") && pnd_Desig_Tab_Pnd_Rltn_Cde.getValue(pnd_1stc,":",pnd_I1).notEquals("08") 
            && pnd_Desig_Tab_Pnd_Prcnt_Frctn_Ind.getValue(pnd_1stc,":",pnd_I1).equals(" ")))
        {
            decideConditionsMet1092++;
            //*  THIS IS A CATCHALL, OF WHICH THE PRIOR EDIT IS A SPECIAL CASE.  TRUST
            //*  CANNOT CO-EXIST WITH ANY OTHER BENE, UNLESS THE ALLOCATION MAKES IT
            //*  CLEAR HOW MUCH IS FOR THE TRUST AND HOW MUCH FOR OTHER BENES. THEREFOR
            //*  THIS EDIT APPLIES ONLY IF THERE IS NO ALLOCATIONS - IE. ACIS.
            pdaBena9820.getBena9820_Pnd_Error_Msg().setValue("Trust and non-Trust listed for Contingents. No allocation. Must clarify.");                                 //Natural: MOVE 'Trust and non-Trust listed for Contingents. No allocation. Must clarify.' TO BENA9820.#ERROR-MSG
            //*  ERROR IF
            //*  TRUST & ANY OTHER
            //* AND NO ALLOCATION
            pnd_Error_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #ERROR-CNT
        }                                                                                                                                                                 //Natural: WHEN #NUM-BENES-P GT 1 AND #RLTN-CDE ( 1:#NUM-BENES-P ) = '08' AND #RLTN-CDE ( 1:#NUM-BENES-P ) NE '08' AND #PRCNT-FRCTN-IND ( 1:#NUM-BENES-P ) = ' '
        else if (condition(pnd_Num_Benes_P.greater(1) && pnd_Desig_Tab_Pnd_Rltn_Cde.getValue(1,":",pnd_Num_Benes_P).equals("08") && pnd_Desig_Tab_Pnd_Rltn_Cde.getValue(1,":",pnd_Num_Benes_P).notEquals("08") 
            && pnd_Desig_Tab_Pnd_Prcnt_Frctn_Ind.getValue(1,":",pnd_Num_Benes_P).equals(" ")))
        {
            decideConditionsMet1092++;
            pdaBena9820.getBena9820_Pnd_Error_Msg().setValue("Trust and non-Trust listed for Primaries. No allocation. Must clarify.");                                   //Natural: MOVE 'Trust and non-Trust listed for Primaries. No allocation. Must clarify.' TO BENA9820.#ERROR-MSG
            pnd_Error_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #ERROR-CNT
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet1092 > 0))
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
                                                                                                                                                                          //Natural: PERFORM CHECK-SHARES-BALANCE
        sub_Check_Shares_Balance();
        if (condition(Global.isEscape())) {return;}
        //*  EDIT-DESIGNATION-RECORDS
    }
    private void sub_Check_Shares_Balance() throws Exception                                                                                                              //Natural: CHECK-SHARES-BALANCE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Confirm_Calc_Tots.reset();                                                                                                                                    //Natural: RESET #CONFIRM-CALC-TOTS
        if (condition(pnd_Num_Benes_P.less(1)))                                                                                                                           //Natural: IF #NUM-BENES-P < 1
        {
            pdaBena9820.getBena9820_Pnd_Error_Msg().setValue("No Primary Beneficiary found");                                                                             //Natural: ASSIGN BENA9820.#ERROR-MSG := 'No Primary Beneficiary found'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  ACIS WITH NO ALLOCATION
        if (condition(pnd_Desig_Tab_Pnd_Prcnt_Frctn_Ind.getValue(1,":",pnd_I1).equals(" ")))                                                                              //Natural: IF #PRCNT-FRCTN-IND ( 1:#I1 ) = ' '
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Desig_Tab_Pnd_Prcnt_Frctn_Ind.getValue(1).equals("P")))                                                                                         //Natural: IF #PRCNT-FRCTN-IND ( 1 ) = 'P'
        {
            F2:                                                                                                                                                           //Natural: FOR #I = 1 TO #I1
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_I1)); pnd_I.nadd(1))
            {
                if (condition(pnd_Desig_Tab_Pnd_Bene_Type.getValue(pnd_I).equals("P")))                                                                                   //Natural: IF #BENE-TYPE ( #I ) = 'P'
                {
                    pnd_Confirm_Calc_Tots_Pnd_P_Perc_Tot.nadd(pnd_Desig_Tab_Pnd_Share_Prcnt.getValue(pnd_I));                                                             //Natural: ADD #SHARE-PRCNT ( #I ) TO #P-PERC-TOT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Confirm_Calc_Tots_Pnd_C_Perc_Tot.nadd(pnd_Desig_Tab_Pnd_Share_Prcnt.getValue(pnd_I));                                                             //Natural: ADD #SHARE-PRCNT ( #I ) TO #C-PERC-TOT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            //*  WE'll create a 100%
            if (condition(pnd_Confirm_Calc_Tots_Pnd_P_Perc_Tot.notEquals(100) && ! (pnd_Num_Benes_P.equals(1) && pnd_Confirm_Calc_Tots_Pnd_P_Perc_Tot.equals(getZero())))) //Natural: IF #P-PERC-TOT NE 100 AND NOT ( #NUM-BENES-P = 1 AND #P-PERC-TOT = 0 )
            {
                pnd_Confirm_Calc_Tots_Pnd_Comp_Lit.setValueEdited(pnd_Confirm_Calc_Tots_Pnd_P_Perc_Tot,new ReportEditMask("ZZZ99.99"));                                   //Natural: MOVE EDITED #P-PERC-TOT ( EM = ZZZ99.99 ) TO #COMP-LIT
                pnd_Confirm_Calc_Tots_Pnd_Comp_Lit.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Confirm_Calc_Tots_Pnd_Comp_Lit, "%"));                    //Natural: COMPRESS #COMP-LIT '%' TO #COMP-LIT LEAVING NO
                pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Sum of Primary Percentages is", pnd_Confirm_Calc_Tots_Pnd_Comp_Lit));                  //Natural: COMPRESS 'Sum of Primary Percentages is' #COMP-LIT TO BENA9820.#ERROR-MSG
                pnd_Error_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #ERROR-CNT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Num_Benes_C.greater(getZero()) && pnd_Confirm_Calc_Tots_Pnd_C_Perc_Tot.notEquals(100)))                                                     //Natural: IF #NUM-BENES-C > 0 AND #C-PERC-TOT NE 100
            {
                //*  ALLOW THIS - WE'll create a 100% if its like this
                if (condition(pnd_Num_Benes_C.equals(1) && pnd_Confirm_Calc_Tots_Pnd_C_Perc_Tot.equals(getZero())))                                                       //Natural: IF #NUM-BENES-C = 1 AND #C-PERC-TOT = 0
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Confirm_Calc_Tots_Pnd_Comp_Lit.setValueEdited(pnd_Confirm_Calc_Tots_Pnd_C_Perc_Tot,new ReportEditMask("Z99.99"));                                 //Natural: MOVE EDITED #C-PERC-TOT ( EM = ZZZ99.99 ) TO #COMP-LIT
                    pnd_Confirm_Calc_Tots_Pnd_Comp_Lit.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Confirm_Calc_Tots_Pnd_Comp_Lit, "%"));                //Natural: COMPRESS #COMP-LIT '%' TO #COMP-LIT LEAVING NO
                    pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Sum of Contingent Percentages is", pnd_Confirm_Calc_Tots_Pnd_Comp_Lit));           //Natural: COMPRESS 'Sum of Contingent Percentages is' #COMP-LIT TO BENA9820.#ERROR-MSG
                    pnd_Error_Cnt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #ERROR-CNT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  SHARES MUST've been used
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            F3:                                                                                                                                                           //Natural: FOR #I = 1 TO #I1
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_I1)); pnd_I.nadd(1))
            {
                if (condition(pnd_Desig_Tab_Pnd_Bene_Type.getValue(pnd_I).equals("P")))                                                                                   //Natural: IF #BENE-TYPE ( #I ) = 'P'
                {
                    //*  1ST 'P' ITERATION
                    if (condition(pnd_Confirm_Calc_Tots_Pnd_P_Ntor_Tot.equals(getZero())))                                                                                //Natural: IF #P-NTOR-TOT = 0
                    {
                        pnd_Confirm_Calc_Tots_Pnd_P_Ntor_Tot.setValue(pnd_Desig_Tab_Pnd_Share_Nmrtr.getValue(pnd_I));                                                     //Natural: ASSIGN #P-NTOR-TOT := #SHARE-NMRTR ( #I )
                        pnd_Confirm_Calc_Tots_Pnd_P_Dtor_Tot.setValue(pnd_Desig_Tab_Pnd_Share_Dnmntr.getValue(pnd_I));                                                    //Natural: ASSIGN #P-DTOR-TOT := #SHARE-DNMNTR ( #I )
                        //*  1ST 'P' ITERATION IS 0
                        //*  AND MORE FOLLOW
                        if (condition(pnd_Confirm_Calc_Tots_Pnd_P_Ntor_Tot.equals(getZero()) && pnd_Num_Benes_P.greater(1)))                                              //Natural: IF #P-NTOR-TOT = 0 AND #NUM-BENES-P GT 1
                        {
                            pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Percent/Fraction ind F without Numerator/Denominator.",                    //Natural: COMPRESS 'Percent/Fraction ind F without Numerator/Denominator.' 'Beneficiary #' #I TO BENA9820.#ERROR-MSG
                                "Beneficiary #", pnd_I));
                            pnd_Error_Cnt.nadd(1);                                                                                                                        //Natural: ADD 1 TO #ERROR-CNT
                            if (true) return;                                                                                                                             //Natural: ESCAPE ROUTINE
                        }                                                                                                                                                 //Natural: END-IF
                        //*  NOW CROSS-MUTIPLY PROGRESSIVELY
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_Confirm_Calc_Tots_Pnd_P_Dtor_Tot.equals(pnd_Desig_Tab_Pnd_Share_Dnmntr.getValue(pnd_I))))                                       //Natural: IF #P-DTOR-TOT = #SHARE-DNMNTR ( #I )
                        {
                            pnd_Confirm_Calc_Tots_Pnd_P_Ntor_Tot.nadd(pnd_Desig_Tab_Pnd_Share_Nmrtr.getValue(pnd_I));                                                     //Natural: ADD #P-NTOR-TOT #SHARE-NMRTR ( #I ) GIVING #P-NTOR-TOT
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Confirm_Calc_Tots_Pnd_Hold_Ntor_Tot_1.compute(new ComputeParameters(false, pnd_Confirm_Calc_Tots_Pnd_Hold_Ntor_Tot_1),                    //Natural: MULTIPLY #P-NTOR-TOT BY #SHARE-DNMNTR ( #I ) GIVING #HOLD-NTOR-TOT-1
                                pnd_Confirm_Calc_Tots_Pnd_P_Ntor_Tot.multiply(pnd_Desig_Tab_Pnd_Share_Dnmntr.getValue(pnd_I)));
                            pnd_Confirm_Calc_Tots_Pnd_Hold_Ntor_Tot_2.compute(new ComputeParameters(false, pnd_Confirm_Calc_Tots_Pnd_Hold_Ntor_Tot_2),                    //Natural: MULTIPLY #SHARE-NMRTR ( #I ) BY #P-DTOR-TOT GIVING #HOLD-NTOR-TOT-2
                                pnd_Desig_Tab_Pnd_Share_Nmrtr.getValue(pnd_I).multiply(pnd_Confirm_Calc_Tots_Pnd_P_Dtor_Tot));
                            pnd_Confirm_Calc_Tots_Pnd_P_Ntor_Tot.compute(new ComputeParameters(false, pnd_Confirm_Calc_Tots_Pnd_P_Ntor_Tot), pnd_Confirm_Calc_Tots_Pnd_Hold_Ntor_Tot_1.add(pnd_Confirm_Calc_Tots_Pnd_Hold_Ntor_Tot_2)); //Natural: ADD #HOLD-NTOR-TOT-1 #HOLD-NTOR-TOT-2 GIVING #P-NTOR-TOT
                            pnd_Confirm_Calc_Tots_Pnd_Res.compute(new ComputeParameters(false, pnd_Confirm_Calc_Tots_Pnd_Res), pnd_Confirm_Calc_Tots_Pnd_P_Ntor_Tot.divide(pnd_Desig_Tab_Pnd_Share_Dnmntr.getValue(pnd_I))); //Natural: DIVIDE #SHARE-DNMNTR ( #I ) INTO #P-NTOR-TOT GIVING #RES
                            //*  DIVIDES WITHOUT REMAINDER
                            if (condition(pnd_Confirm_Calc_Tots_Pnd_Res_Decimal.equals(getZero())))                                                                       //Natural: IF #RES-DECIMAL = 0
                            {
                                pnd_Confirm_Calc_Tots_Pnd_P_Ntor_Tot.compute(new ComputeParameters(false, pnd_Confirm_Calc_Tots_Pnd_P_Ntor_Tot), pnd_Confirm_Calc_Tots_Pnd_P_Ntor_Tot.divide(pnd_Desig_Tab_Pnd_Share_Dnmntr.getValue(pnd_I))); //Natural: DIVIDE #SHARE-DNMNTR ( #I ) INTO #P-NTOR-TOT
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Confirm_Calc_Tots_Pnd_P_Dtor_Tot.compute(new ComputeParameters(false, pnd_Confirm_Calc_Tots_Pnd_P_Dtor_Tot), pnd_Desig_Tab_Pnd_Share_Dnmntr.getValue(pnd_I).multiply(pnd_Confirm_Calc_Tots_Pnd_P_Dtor_Tot)); //Natural: MULTIPLY #SHARE-DNMNTR ( #I ) BY #P-DTOR-TOT GIVING #P-DTOR-TOT
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*  BENE-TYPE IS 'C'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  1ST 'C' ITERATION
                    if (condition(pnd_Confirm_Calc_Tots_Pnd_C_Ntor_Tot.equals(getZero())))                                                                                //Natural: IF #C-NTOR-TOT = 0
                    {
                        pnd_Confirm_Calc_Tots_Pnd_C_Ntor_Tot.setValue(pnd_Desig_Tab_Pnd_Share_Nmrtr.getValue(pnd_I));                                                     //Natural: ASSIGN #C-NTOR-TOT := #SHARE-NMRTR ( #I )
                        pnd_Confirm_Calc_Tots_Pnd_C_Dtor_Tot.setValue(pnd_Desig_Tab_Pnd_Share_Dnmntr.getValue(pnd_I));                                                    //Natural: ASSIGN #C-DTOR-TOT := #SHARE-DNMNTR ( #I )
                        //*  1ST 'C' ITERATION IS 0
                        //*  AND MORE FOLLOW
                        if (condition(pnd_Confirm_Calc_Tots_Pnd_C_Ntor_Tot.equals(getZero()) && pnd_Num_Benes_C.greater(1)))                                              //Natural: IF #C-NTOR-TOT = 0 AND #NUM-BENES-C GT 1
                        {
                            pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Percent/Fraction ind F without Numerator/Denominator.",                    //Natural: COMPRESS 'Percent/Fraction ind F without Numerator/Denominator.' 'Beneficiary #' #I TO BENA9820.#ERROR-MSG
                                "Beneficiary #", pnd_I));
                            pnd_Error_Cnt.nadd(1);                                                                                                                        //Natural: ADD 1 TO #ERROR-CNT
                            if (true) return;                                                                                                                             //Natural: ESCAPE ROUTINE
                        }                                                                                                                                                 //Natural: END-IF
                        //*  NOW CROSS-MUTIPLY PROGRESSIVELY
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_Confirm_Calc_Tots_Pnd_C_Dtor_Tot.equals(pnd_Desig_Tab_Pnd_Share_Dnmntr.getValue(pnd_I))))                                       //Natural: IF #C-DTOR-TOT = #SHARE-DNMNTR ( #I )
                        {
                            pnd_Confirm_Calc_Tots_Pnd_C_Ntor_Tot.nadd(pnd_Desig_Tab_Pnd_Share_Nmrtr.getValue(pnd_I));                                                     //Natural: ADD #C-NTOR-TOT #SHARE-NMRTR ( #I ) GIVING #C-NTOR-TOT
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Confirm_Calc_Tots_Pnd_Hold_Ntor_Tot_1.compute(new ComputeParameters(false, pnd_Confirm_Calc_Tots_Pnd_Hold_Ntor_Tot_1),                    //Natural: MULTIPLY #C-NTOR-TOT BY #SHARE-DNMNTR ( #I ) GIVING #HOLD-NTOR-TOT-1
                                pnd_Confirm_Calc_Tots_Pnd_C_Ntor_Tot.multiply(pnd_Desig_Tab_Pnd_Share_Dnmntr.getValue(pnd_I)));
                            pnd_Confirm_Calc_Tots_Pnd_Hold_Ntor_Tot_2.compute(new ComputeParameters(false, pnd_Confirm_Calc_Tots_Pnd_Hold_Ntor_Tot_2),                    //Natural: MULTIPLY #SHARE-NMRTR ( #I ) BY #C-DTOR-TOT GIVING #HOLD-NTOR-TOT-2
                                pnd_Desig_Tab_Pnd_Share_Nmrtr.getValue(pnd_I).multiply(pnd_Confirm_Calc_Tots_Pnd_C_Dtor_Tot));
                            pnd_Confirm_Calc_Tots_Pnd_C_Ntor_Tot.compute(new ComputeParameters(false, pnd_Confirm_Calc_Tots_Pnd_C_Ntor_Tot), pnd_Confirm_Calc_Tots_Pnd_Hold_Ntor_Tot_1.add(pnd_Confirm_Calc_Tots_Pnd_Hold_Ntor_Tot_2)); //Natural: ADD #HOLD-NTOR-TOT-1 #HOLD-NTOR-TOT-2 GIVING #C-NTOR-TOT
                            pnd_Confirm_Calc_Tots_Pnd_Res.compute(new ComputeParameters(false, pnd_Confirm_Calc_Tots_Pnd_Res), pnd_Confirm_Calc_Tots_Pnd_C_Ntor_Tot.divide(pnd_Desig_Tab_Pnd_Share_Dnmntr.getValue(pnd_I))); //Natural: DIVIDE #SHARE-DNMNTR ( #I ) INTO #C-NTOR-TOT GIVING #RES
                            //*  DIVIDES WITHOUT REMAINDER
                            if (condition(pnd_Confirm_Calc_Tots_Pnd_Res_Decimal.equals(getZero())))                                                                       //Natural: IF #RES-DECIMAL = 0
                            {
                                pnd_Confirm_Calc_Tots_Pnd_C_Ntor_Tot.compute(new ComputeParameters(false, pnd_Confirm_Calc_Tots_Pnd_C_Ntor_Tot), pnd_Confirm_Calc_Tots_Pnd_C_Ntor_Tot.divide(pnd_Desig_Tab_Pnd_Share_Dnmntr.getValue(pnd_I))); //Natural: DIVIDE #SHARE-DNMNTR ( #I ) INTO #C-NTOR-TOT
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Confirm_Calc_Tots_Pnd_C_Dtor_Tot.compute(new ComputeParameters(false, pnd_Confirm_Calc_Tots_Pnd_C_Dtor_Tot), pnd_Desig_Tab_Pnd_Share_Dnmntr.getValue(pnd_I).multiply(pnd_Confirm_Calc_Tots_Pnd_C_Dtor_Tot)); //Natural: MULTIPLY #SHARE-DNMNTR ( #I ) BY #C-DTOR-TOT GIVING #C-DTOR-TOT
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            if (condition(pnd_Confirm_Calc_Tots_Pnd_C_Dtor_Tot.notEquals(pnd_Confirm_Calc_Tots_Pnd_C_Ntor_Tot)))                                                          //Natural: IF #C-DTOR-TOT NE #C-NTOR-TOT
            {
                pnd_Confirm_Calc_Tots_Pnd_Comp_Lit.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Confirm_Calc_Tots_Pnd_C_Ntor_Tot, "/",                    //Natural: COMPRESS #C-NTOR-TOT '/' #C-DTOR-TOT TO #COMP-LIT LEAVING NO
                    pnd_Confirm_Calc_Tots_Pnd_C_Dtor_Tot));
                pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Sum of fractions for Contingent Beneficiaries is", pnd_Confirm_Calc_Tots_Pnd_Comp_Lit)); //Natural: COMPRESS 'Sum of fractions for Contingent Beneficiaries is' #COMP-LIT TO BENA9820.#ERROR-MSG
                pnd_Error_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #ERROR-CNT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Confirm_Calc_Tots_Pnd_P_Dtor_Tot.notEquals(pnd_Confirm_Calc_Tots_Pnd_P_Ntor_Tot)))                                                          //Natural: IF #P-DTOR-TOT NE #P-NTOR-TOT
            {
                pnd_Confirm_Calc_Tots_Pnd_Comp_Lit.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Confirm_Calc_Tots_Pnd_P_Ntor_Tot, "/",                    //Natural: COMPRESS #P-NTOR-TOT '/' #P-DTOR-TOT TO #COMP-LIT LEAVING NO
                    pnd_Confirm_Calc_Tots_Pnd_P_Dtor_Tot));
                pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Sum of fractions for Primary Beneficiaries is", pnd_Confirm_Calc_Tots_Pnd_Comp_Lit));  //Natural: COMPRESS 'Sum of fractions for Primary Beneficiaries is' #COMP-LIT TO BENA9820.#ERROR-MSG
                pnd_Error_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #ERROR-CNT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  CHECK-SHARES-BALANCE
    }
    private void sub_Convert_Relationship_Code() throws Exception                                                                                                         //Natural: CONVERT-RELATIONSHIP-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        DbsUtil.callnat(Benn9832.class , getCurrentProcessState(), pdaBena9832.getBena9832(), pdaBena9834.getBena9834(), pdaBenpda_M.getMsg_Info_Sub(),                   //Natural: CALLNAT 'BENN9832' BENA9832 BENA9834 MSG-INFO-SUB ERROR-INFO-SUB
            pdaBenpda_E.getError_Info_Sub());
        if (condition(Global.isEscape())) return;
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ERROR-INFO-SUB.##LAST-PROGRAM := *PROGRAM
        //*  CONVERT-RELATIONSHIP-CODE
    }
    private void sub_Check_For_Text_Allocations() throws Exception                                                                                                        //Natural: CHECK-FOR-TEXT-ALLOCATIONS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Test_Lit.contains ("%") ||pnd_Test_Lit.contains (" PERCENT")))                                                                                  //Natural: IF #TEST-LIT = SCAN '%' OR #TEST-LIT = SCAN ' PERCENT'
        {
            if (condition(pdaBena9801.getBena9801_Intrfcng_Systm().equals("ACIS")))                                                                                       //Natural: IF BENA9801.INTRFCNG-SYSTM = 'ACIS'
            {
                pdaBena9820.getBena9820_Pnd_Interface_As_Mos().setValue(true);                                                                                            //Natural: ASSIGN BENA9820.#INTERFACE-AS-MOS := TRUE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Name_Only_This_Bene.getBoolean()))                                                                                                      //Natural: IF #NAME-ONLY-THIS-BENE
                {
                    pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("'%' found:", pnd_Test_Lit, "; at Name #", pnd_K1));                                //Natural: COMPRESS '"%" found:' #TEST-LIT '; at Name #' #K1 TO BENA9820.#ERROR-MSG
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Test_Lit.contains ("1") ||pnd_Test_Lit.contains ("2") ||pnd_Test_Lit.contains ("3") ||pnd_Test_Lit.contains ("4") ||pnd_Test_Lit.contains   //Natural: IF #TEST-LIT = SCAN '1' OR = SCAN '2' OR = SCAN '3' OR = SCAN '4' OR = SCAN '5' OR = SCAN '6' OR = SCAN '7' OR = SCAN '8' OR = SCAN '9'
                ("5") ||pnd_Test_Lit.contains ("6") ||pnd_Test_Lit.contains ("7") ||pnd_Test_Lit.contains ("8") ||pnd_Test_Lit.contains ("9")))
            {
                pnd_Test_Lit.separate(EnumSet.of(SeparateOption.LeftJustified,SeparateOption.WithAnyDelimiters), " ", pnd_Sep_Tab.getValue("*"));                         //Natural: SEPARATE #TEST-LIT LEFT INTO #SEP-TAB ( * ) WITH DELIMITER ' '
                //*  BECAUSE THE LITERAL 'SAME AS'
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-NUMERIC-SAME-AS
                sub_Check_For_Numeric_Same_As();
                if (condition(Global.isEscape())) {return;}
                //*  IS SOMETIMES MISSPELT OR MISSING, ANY NAME WHICH HAS A CONTRACT
                //*  NUMBER FORMAT WILL BE TREATED AS A 'SAME AS'.
                if (condition(pdaBena9820.getBena9820_Pnd_Error_Msg().greater(" ")))                                                                                      //Natural: IF BENA9820.#ERROR-MSG GT ' '
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaBena9801.getBena9801_Intrfcng_Systm().equals("ACIS") || pnd_Name_Only_This_Bene.getBoolean()))                                           //Natural: IF BENA9801.INTRFCNG-SYSTM = 'ACIS' OR #NAME-ONLY-THIS-BENE
                {
                    if (condition(DbsUtil.maskMatches(pnd_Sep_Tab.getValue("*"),"N") || DbsUtil.maskMatches(pnd_Sep_Tab.getValue("*"),"NN") || DbsUtil.maskMatches(pnd_Sep_Tab.getValue("*"),"NNN")  //Natural: IF #SEP-TAB ( * ) = MASK ( N ) OR #SEP-TAB ( * ) = MASK ( NN ) OR #SEP-TAB ( * ) = MASK ( NNN ) OR #SEP-TAB ( * ) = MASK ( NNNN ) OR #SEP-TAB ( * ) = MASK ( NNNNN ) OR #SEP-TAB ( * ) = MASK ( NNNNNN ) OR #SEP-TAB ( * ) = MASK ( NNNNNNN ) OR #SEP-TAB ( * ) = MASK ( N','NNN ) OR #SEP-TAB ( * ) = MASK ( NN','NNN ) OR #SEP-TAB ( * ) = MASK ( NNN','NNN ) OR #SEP-TAB ( * ) = MASK ( N','NNN','NNN ) OR #SEP-TAB ( * ) = MASK ( '$' ) OR #SEP-TAB ( * ) = MASK ( N'/'N' ' ) OR #SEP-TAB ( * ) = MASK ( N'/'NN' ' ) OR #SEP-TAB ( * ) = MASK ( NN'/'NN' ' ) OR #SEP-TAB ( * ) = MASK ( '('N'/'N')' )
                        || DbsUtil.maskMatches(pnd_Sep_Tab.getValue("*"),"NNNN") || DbsUtil.maskMatches(pnd_Sep_Tab.getValue("*"),"NNNNN") || DbsUtil.maskMatches(pnd_Sep_Tab.getValue("*"),"NNNNNN") 
                        || DbsUtil.maskMatches(pnd_Sep_Tab.getValue("*"),"NNNNNNN") || DbsUtil.maskMatches(pnd_Sep_Tab.getValue("*"),"N','NNN") || DbsUtil.maskMatches(pnd_Sep_Tab.getValue("*"),"NN','NNN") 
                        || DbsUtil.maskMatches(pnd_Sep_Tab.getValue("*"),"NNN','NNN") || DbsUtil.maskMatches(pnd_Sep_Tab.getValue("*"),"N','NNN','NNN") 
                        || DbsUtil.maskMatches(pnd_Sep_Tab.getValue("*"),"'$'") || DbsUtil.maskMatches(pnd_Sep_Tab.getValue("*"),"N'/'N' '") || DbsUtil.maskMatches(pnd_Sep_Tab.getValue("*"),"N'/'NN' '") 
                        || DbsUtil.maskMatches(pnd_Sep_Tab.getValue("*"),"NN'/'NN' '") || DbsUtil.maskMatches(pnd_Sep_Tab.getValue("*"),"'('N'/'N')'")))
                    {
                        if (condition(pdaBena9801.getBena9801_Intrfcng_Systm().equals("ACIS")))                                                                           //Natural: IF BENA9801.INTRFCNG-SYSTM = 'ACIS'
                        {
                            pdaBena9820.getBena9820_Pnd_Interface_As_Mos().setValue(true);                                                                                //Natural: ASSIGN BENA9820.#INTERFACE-AS-MOS := TRUE
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            //*  IF DATED TRUST DO NOT ERROR
                            if (condition(! (((((((((((((((((((DbsUtil.maskMatches(pnd_Sep_Tab.getValue("*"),"MM'/'DD'/'YY") || DbsUtil.maskMatches(pnd_Sep_Tab.getValue("*"),"N'/'DD'/'YY"))  //Natural: IF NOT ( ( #SEP-TAB ( * ) = MASK ( MM'/'DD'/'YY ) OR #SEP-TAB ( * ) = MASK ( N'/'DD'/'YY ) OR #SEP-TAB ( * ) = MASK ( N'/'N'/'YY ) OR #SEP-TAB ( * ) = MASK ( N'/'YY ) OR #SEP-TAB ( * ) = MASK ( MM'/'N'/'YY ) OR #SEP-TAB ( * ) = MASK ( MM'/'DD'/'YYYY ) OR #SEP-TAB ( * ) = MASK ( N'/'DD'/'YYYY ) OR #SEP-TAB ( * ) = MASK ( N'/'N'/'YYYY ) OR #SEP-TAB ( * ) = MASK ( MM'/'N'/'YYYY ) OR #SEP-TAB ( * ) = MASK ( MM'-'DD'-'YY ) OR #SEP-TAB ( * ) = MASK ( N'-'DD'-'YY ) OR #SEP-TAB ( * ) = MASK ( N'-'N'-'YY ) OR #SEP-TAB ( * ) = MASK ( MM'-'N'-'YY ) OR #SEP-TAB ( * ) = MASK ( MM'-'DD'-'YYYY ) OR #SEP-TAB ( * ) = MASK ( N'-'DD'-'YYYY ) OR #SEP-TAB ( * ) = MASK ( N'-'N'-'YYYY ) OR #SEP-TAB ( * ) = MASK ( MM'-'N'-'YYYY ) OR #SEP-TAB ( * ) = 'JAN' OR = 'JANUARY' OR = 'FEB' OR = 'FEBRUARY' OR = 'MAR' OR = 'MARCH' OR = 'APR' OR = 'APRIL' OR = 'MAY' OR = 'JUNE' OR = 'JUL' OR = 'JULY' OR = 'AUG' OR = 'AUGUST' OR = 'SEP' OR = 'SEPTEMBER' OR = 'OCT' OR = 'OCTOBER' OR = 'NOV' OR = 'NOVEMBER' OR = 'DEC' OR = 'DECEMBER' ) AND #SEP-TAB ( * ) = 'TRUST' )
                                || DbsUtil.maskMatches(pnd_Sep_Tab.getValue("*"),"N'/'N'/'YY")) || DbsUtil.maskMatches(pnd_Sep_Tab.getValue("*"),"N'/'YY")) 
                                || DbsUtil.maskMatches(pnd_Sep_Tab.getValue("*"),"MM'/'N'/'YY")) || DbsUtil.maskMatches(pnd_Sep_Tab.getValue("*"),"MM'/'DD'/'YYYY")) 
                                || DbsUtil.maskMatches(pnd_Sep_Tab.getValue("*"),"N'/'DD'/'YYYY")) || DbsUtil.maskMatches(pnd_Sep_Tab.getValue("*"),"N'/'N'/'YYYY")) 
                                || DbsUtil.maskMatches(pnd_Sep_Tab.getValue("*"),"MM'/'N'/'YYYY")) || DbsUtil.maskMatches(pnd_Sep_Tab.getValue("*"),"MM'-'DD'-'YY")) 
                                || DbsUtil.maskMatches(pnd_Sep_Tab.getValue("*"),"N'-'DD'-'YY")) || DbsUtil.maskMatches(pnd_Sep_Tab.getValue("*"),"N'-'N'-'YY")) 
                                || DbsUtil.maskMatches(pnd_Sep_Tab.getValue("*"),"MM'-'N'-'YY")) || DbsUtil.maskMatches(pnd_Sep_Tab.getValue("*"),"MM'-'DD'-'YYYY")) 
                                || DbsUtil.maskMatches(pnd_Sep_Tab.getValue("*"),"N'-'DD'-'YYYY")) || DbsUtil.maskMatches(pnd_Sep_Tab.getValue("*"),"N'-'N'-'YYYY")) 
                                || DbsUtil.maskMatches(pnd_Sep_Tab.getValue("*"),"MM'-'N'-'YYYY")) || (((((((((((((((((((((pnd_Sep_Tab.getValue("*").equals("JAN") 
                                || pnd_Sep_Tab.getValue("*").equals("JANUARY")) || pnd_Sep_Tab.getValue("*").equals("FEB")) || pnd_Sep_Tab.getValue("*").equals("FEBRUARY")) 
                                || pnd_Sep_Tab.getValue("*").equals("MAR")) || pnd_Sep_Tab.getValue("*").equals("MARCH")) || pnd_Sep_Tab.getValue("*").equals("APR")) 
                                || pnd_Sep_Tab.getValue("*").equals("APRIL")) || pnd_Sep_Tab.getValue("*").equals("MAY")) || pnd_Sep_Tab.getValue("*").equals("JUNE")) 
                                || pnd_Sep_Tab.getValue("*").equals("JUL")) || pnd_Sep_Tab.getValue("*").equals("JULY")) || pnd_Sep_Tab.getValue("*").equals("AUG")) 
                                || pnd_Sep_Tab.getValue("*").equals("AUGUST")) || pnd_Sep_Tab.getValue("*").equals("SEP")) || pnd_Sep_Tab.getValue("*").equals("SEPTEMBER")) 
                                || pnd_Sep_Tab.getValue("*").equals("OCT")) || pnd_Sep_Tab.getValue("*").equals("OCTOBER")) || pnd_Sep_Tab.getValue("*").equals("NOV")) 
                                || pnd_Sep_Tab.getValue("*").equals("NOVEMBER")) || pnd_Sep_Tab.getValue("*").equals("DEC")) || pnd_Sep_Tab.getValue("*").equals("DECEMBER"))) 
                                && pnd_Sep_Tab.getValue("*").equals("TRUST")))))
                            {
                                pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Numbers/allocation:", pnd_Test_Lit, "; at Name #",                     //Natural: COMPRESS 'Numbers/allocation:' #TEST-LIT '; at Name #' #K1 TO BENA9820.#ERROR-MSG
                                    pnd_K1));
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  CHECK-FOR-TEXT-ALLOCATIONS
    }
    //*  CURRENTLY BEING USED FOR NAME ONLY
    private void sub_Check_For_Numeric_Same_As() throws Exception                                                                                                         //Natural: CHECK-FOR-NUMERIC-SAME-AS
    {
        if (BLNatReinput.isReinput()) return;

        FOR08:                                                                                                                                                            //Natural: FOR #I = 1 TO 35
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(35)); pnd_I.nadd(1))
        {
            if (condition(pnd_Sep_Tab.getValue(pnd_I).equals(" ")))                                                                                                       //Natural: IF #SEP-TAB ( #I ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  MUST BE AT LEAST 6 CHARS LONG
            if (condition(DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I),".....' '")))                                                                                   //Natural: IF #SEP-TAB ( #I ) = MASK ( .....' ' )
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Same_Hold.reset();                                                                                                                                        //Natural: RESET #SAME-HOLD
            if (condition(pnd_Sep_Tab.getValue(pnd_I).contains ("RA") ||pnd_Sep_Tab.getValue(pnd_I).contains ("(") ||pnd_Sep_Tab.getValue(pnd_I).contains                 //Natural: IF #SEP-TAB ( #I ) = SCAN 'RA' OR = SCAN '(' OR = SCAN 'NUMBERS-' OR = SCAN ')' OR = SCAN '.'
                ("NUMBERS-") ||pnd_Sep_Tab.getValue(pnd_I).contains (")") ||pnd_Sep_Tab.getValue(pnd_I).contains (".")))
            {
                //*  EXAMINE/DELETES MUST BE DONE IN RIGHT ORDER
                DbsUtil.examine(new ExamineSource(pnd_Sep_Tab.getValue(pnd_I)), new ExamineSearch("CONTRACTS-"), new ExamineDelete());                                    //Natural: EXAMINE #SEP-TAB ( #I ) FOR 'CONTRACTS-' DELETE
                DbsUtil.examine(new ExamineSource(pnd_Sep_Tab.getValue(pnd_I)), new ExamineSearch("CONTRACT-"), new ExamineDelete());                                     //Natural: EXAMINE #SEP-TAB ( #I ) FOR 'CONTRACT-' DELETE
                DbsUtil.examine(new ExamineSource(pnd_Sep_Tab.getValue(pnd_I)), new ExamineSearch("CONTRACT:"), new ExamineDelete());                                     //Natural: EXAMINE #SEP-TAB ( #I ) FOR 'CONTRACT:' DELETE
                DbsUtil.examine(new ExamineSource(pnd_Sep_Tab.getValue(pnd_I)), new ExamineSearch("CONTRACT_"), new ExamineDelete());                                     //Natural: EXAMINE #SEP-TAB ( #I ) FOR 'CONTRACT_' DELETE
                DbsUtil.examine(new ExamineSource(pnd_Sep_Tab.getValue(pnd_I)), new ExamineSearch("NUMBERS-"), new ExamineDelete());                                      //Natural: EXAMINE #SEP-TAB ( #I ) FOR 'NUMBERS-' DELETE
                DbsUtil.examine(new ExamineSource(pnd_Sep_Tab.getValue(pnd_I)), new ExamineSearch("CONTRACT"), new ExamineDelete());                                      //Natural: EXAMINE #SEP-TAB ( #I ) FOR 'CONTRACT' DELETE
                DbsUtil.examine(new ExamineSource(pnd_Sep_Tab.getValue(pnd_I)), new ExamineSearch("GRA"), new ExamineDelete());                                           //Natural: EXAMINE #SEP-TAB ( #I ) FOR 'GRA' DELETE
                DbsUtil.examine(new ExamineSource(pnd_Sep_Tab.getValue(pnd_I)), new ExamineSearch("RA"), new ExamineDelete());                                            //Natural: EXAMINE #SEP-TAB ( #I ) FOR 'RA' DELETE
                DbsUtil.examine(new ExamineSource(pnd_Sep_Tab.getValue(pnd_I)), new ExamineSearch("("), new ExamineDelete());                                             //Natural: EXAMINE #SEP-TAB ( #I ) FOR '(' DELETE
                DbsUtil.examine(new ExamineSource(pnd_Sep_Tab.getValue(pnd_I)), new ExamineSearch(")"), new ExamineDelete());                                             //Natural: EXAMINE #SEP-TAB ( #I ) FOR ')' DELETE
                //*  DATE, NOT CONTRACT #
                if (condition(! (DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I),"NN'.'NN'.'NNNN"))))                                                                     //Natural: IF #SEP-TAB ( #I ) NE MASK ( NN'.'NN'.'NNNN )
                {
                    DbsUtil.examine(new ExamineSource(pnd_Sep_Tab.getValue(pnd_I)), new ExamineSearch("."), new ExamineDelete());                                         //Natural: EXAMINE #SEP-TAB ( #I ) FOR '.' DELETE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet1375 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #SEP-TAB ( #I ) = MASK ( NNNNNNNN' ' ) OR #SEP-TAB ( #I ) = MASK ( NNNNNNUN' ' ) OR #SEP-TAB ( #I ) = MASK ( NNNNNUUN' ' ) OR #SEP-TAB ( #I ) = MASK ( NNNNNUNN' ' ) OR #SEP-TAB ( #I ) = MASK ( NNNNUUUN' ' ) OR #SEP-TAB ( #I ) = MASK ( NNNNUUNN' ' ) OR #SEP-TAB ( #I ) = MASK ( NNNNUNUN' ' ) OR #SEP-TAB ( #I ) = MASK ( NNNNUNNN' ' ) OR #SEP-TAB ( #I ) = MASK ( UNNNNNNN' ' ) OR #SEP-TAB ( #I ) = MASK ( UNNNNNUN' ' ) OR #SEP-TAB ( #I ) = MASK ( UNNNNUUN' ' ) OR #SEP-TAB ( #I ) = MASK ( UNNNNUNN' ' ) OR #SEP-TAB ( #I ) = MASK ( UNNNUUUN' ' ) OR #SEP-TAB ( #I ) = MASK ( UNNNUUNN' ' ) OR #SEP-TAB ( #I ) = MASK ( UNNNUNUN' ' )
            if (condition(DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I),"NNNNNNNN' '") || DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I),"NNNNNNUN' '") 
                || DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I),"NNNNNUUN' '") || DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I),"NNNNNUNN' '") || DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I),"NNNNUUUN' '") 
                || DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I),"NNNNUUNN' '") || DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I),"NNNNUNUN' '") || DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I),"NNNNUNNN' '") 
                || DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I),"UNNNNNNN' '") || DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I),"UNNNNNUN' '") || DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I),"UNNNNUUN' '") 
                || DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I),"UNNNNUNN' '") || DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I),"UNNNUUUN' '") || DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I),"UNNNUUNN' '") 
                || DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I),"UNNNUNUN' '")))
            {
                decideConditionsMet1375++;
                pnd_Same_Hold.setValue(pnd_Sep_Tab.getValue(pnd_I));                                                                                                      //Natural: ASSIGN #SAME-HOLD := #SEP-TAB ( #I )
            }                                                                                                                                                             //Natural: WHEN #SEP-TAB ( #I ) = MASK ( '#'UNNNCCCN' ' ) OR = MASK ( '#'NNNNCCCN' ' ) OR = MASK ( '#'UNNNCCC'-'N' ' ) OR = MASK ( '#'NNNNCCC'-'N' ' ) OR = MASK ( '#'U'-'NNNCCC'-'N' ' ) OR = MASK ( '#'N'-'NNNCCC'-'N' ' ) OR = MASK ( UNNNCCC'-'N' ' ) OR = MASK ( NNNNCCC'-'N' ' ) OR = MASK ( U'-'NNNCCC'-'N' ' ) OR = MASK ( N'-'NNNCCC'-'N' ' ) OR = MASK ( U'-'NNNCCCN' ' ) OR = MASK ( N'-'NNNCCCN' ' )
            else if (condition(DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I),"'#'UNNNCCCN' '") || DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I),"'#'NNNNCCCN' '") 
                || DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I),"'#'UNNNCCC'-'N' '") || DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I),"'#'NNNNCCC'-'N' '") 
                || DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I),"'#'U'-'NNNCCC'-'N' '") || DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I),"'#'N'-'NNNCCC'-'N' '") 
                || DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I),"UNNNCCC'-'N' '") || DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I),"NNNNCCC'-'N' '") 
                || DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I),"U'-'NNNCCC'-'N' '") || DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I),"N'-'NNNCCC'-'N' '") 
                || DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I),"U'-'NNNCCCN' '") || DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I),"N'-'NNNCCCN' '")))
            {
                decideConditionsMet1375++;
                pnd_Same_Hold.setValue(pnd_Sep_Tab.getValue(pnd_I));                                                                                                      //Natural: ASSIGN #SAME-HOLD := #SEP-TAB ( #I )
            }                                                                                                                                                             //Natural: WHEN #SEP-TAB ( #I ) = MASK ( NNNNCCCN'/'NNNNCCCN' ' ) OR = MASK ( UNNNCCCN'/'UNNNCCCN' ' )
            else if (condition(DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I),"NNNNCCCN'/'NNNNCCCN' '") || DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I),
                "UNNNCCCN'/'UNNNCCCN' '")))
            {
                decideConditionsMet1375++;
                pnd_Same_Hold.setValue(pnd_Sep_Tab.getValue(pnd_I));                                                                                                      //Natural: ASSIGN #SAME-HOLD := #SEP-TAB ( #I )
            }                                                                                                                                                             //Natural: WHEN #SEP-TAB ( #I ) = MASK ( '#'NNNNCCC'-'N'/'NNNNCCC'-'N' ' ) OR = MASK ( '#'NNNNCCCN'/'NNNNCCCN' ' ) OR = MASK ( '#'UNNNCCC'-'N'/'UNNNCCC'-'N' ' ) OR = MASK ( UNNNCCC'-'N'/'UNNNCCC'-'N' ' ) OR = MASK ( NNNNCCC'-'N'/'NNNNCCC'-'N' ' ) OR = MASK ( '#'UNNNCCCN'/'UNNNCCCN' ' )
            else if (condition(DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I),"'#'NNNNCCC'-'N'/'NNNNCCC'-'N' '") || DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I),"'#'NNNNCCCN'/'NNNNCCCN' '") 
                || DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I),"'#'UNNNCCC'-'N'/'UNNNCCC'-'N' '") || DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I),"UNNNCCC'-'N'/'UNNNCCC'-'N' '") 
                || DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I),"NNNNCCC'-'N'/'NNNNCCC'-'N' '") || DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I),"'#'UNNNCCCN'/'UNNNCCCN' '")))
            {
                decideConditionsMet1375++;
                pnd_Same_Hold.setValue(pnd_Sep_Tab.getValue(pnd_I));                                                                                                      //Natural: ASSIGN #SAME-HOLD := #SEP-TAB ( #I )
            }                                                                                                                                                             //Natural: WHEN ( #SEP-TAB ( #I ) = MASK ( NNNCCCN' ' ) OR = MASK ( NNNCCC'-'N' ' ) ) AND #I GT 1 AND ( #SEP-TAB ( #I - 1 ) = MASK ( U' ' ) OR = MASK ( N' ' ) )
            else if (condition((((DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I),"NNNCCCN' '") || DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I),"NNNCCC'-'N' '")) 
                && pnd_I.greater(1)) && (DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I.getDec().subtract(1)),"U' '") || DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I.getDec().subtract(1)),
                "N' '")))))
            {
                decideConditionsMet1375++;
                pnd_Same_Hold.setValue(pnd_Sep_Tab.getValue(pnd_I));                                                                                                      //Natural: ASSIGN #SAME-HOLD := #SEP-TAB ( #I )
            }                                                                                                                                                             //Natural: WHEN #SEP-TAB ( #I ) = MASK ( NNNCCC' ' ) AND #I GT 1 AND ( ( #SEP-TAB ( #I - 1 ) = MASK ( U' ' ) OR = MASK ( N' ' ) ) AND #SEP-TAB ( #I + 1 ) = MASK ( N' ' ) )
            else if (condition(((DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I),"NNNCCC' '") && pnd_I.greater(1)) && ((DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I.getDec().subtract(1)),"U' '") 
                || DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I.getDec().subtract(1)),"N' '")) && DbsUtil.maskMatches(pnd_Sep_Tab.getValue(pnd_I.getDec().add(1)),
                "N' '")))))
            {
                decideConditionsMet1375++;
                pnd_Same_Hold.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Sep_Tab.getValue(pnd_I.getDec().subtract(1)), pnd_Sep_Tab.getValue(pnd_I),     //Natural: COMPRESS #SEP-TAB ( #I - 1 ) #SEP-TAB ( #I ) #SEP-TAB ( #I + 1 ) TO #SAME-HOLD LEAVING NO
                    pnd_Sep_Tab.getValue(pnd_I.getDec().add(1))));
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet1375 > 0))
            {
                pdaBena9820.getBena9820_Pnd_Error_Msg().setValue(DbsUtil.compress("Same As:", pnd_Desig_Tab_Pnd_Bene_Name1.getValue(pnd_K1), "; at Name #",               //Natural: COMPRESS 'Same As:' #BENE-NAME1 ( #K1 ) '; at Name #' #K1 TO BENA9820.#ERROR-MSG
                    pnd_K1));
                pdaBena9820.getBena9820_Pnd_Error_Cde().setValue("SAME");                                                                                                 //Natural: ASSIGN BENA9820.#ERROR-CDE := 'SAME'
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  CHECK-FOR-NUMERIC-SAME-AS
    }

    //
}
