/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:06:08 PM
**        * FROM NATURAL SUBPROGRAM : Benn9815
************************************************************
**        * FILE NAME            : Benn9815.java
**        * CLASS NAME           : Benn9815
**        * INSTANCE NAME        : Benn9815
************************************************************
************************************************************************
* PROGRAM  : BENN9815
* SYSTEM   : BENEFICIARY-SYSTEM
* TITLE    : DELETE EXISTING BENE RECORDS
* GENERATED: SEPT 1, 1999
************************************************************************
*    DATE      USERID                   DESCRIPTION
* MM/DD/YYYY  XXXXXXXX  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
* 04/01/2017  DURAND    PIN EXPANSION N7 TO N12
* CHANGED ON JUL 1,99 BY DEPAUL FOR RELEASE ____
* 10/18/00 ALLOW 'M' STATUS TO BE REPLACED/DLETED/MOVED.
* 11/28/00 FLDR-LOG-DTE-TME GARNERED FROM EXISTING CONTRACT FOR CORR
************************************************************************

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benn9815 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaBena9815 pdaBena9815;
    private PdaBenpda_M pdaBenpda_M;
    private PdaBenpda_E pdaBenpda_E;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_bc;
    private DbsField bc_Bc_Pin;
    private DbsField bc_Bc_Tiaa_Cntrct;
    private DbsField bc_Bc_Cref_Cntrct;
    private DbsField bc_Bc_Tiaa_Cref_Ind;
    private DbsField bc_Bc_Stat;
    private DbsField bc_Bc_Cntrct_Type;
    private DbsField bc_Bc_Rqst_Timestamp;
    private DbsField bc_Bc_Eff_Dte;
    private DbsField bc_Bc_Mos_Ind;
    private DbsField bc_Bc_Irvcbl_Ind;
    private DbsField bc_Bc_Pymnt_Chld_Dcsd_Ind;
    private DbsField bc_Bc_Exempt_Spouse_Rights;
    private DbsField bc_Bc_Spouse_Waived_Bnfts;
    private DbsField bc_Bc_Trust_Data_Fldr;
    private DbsField bc_Bc_Bene_Addr_Fldr;
    private DbsField bc_Bc_Co_Owner_Data_Fldr;
    private DbsField bc_Bc_Fldr_Min;
    private DbsField bc_Bc_Fldr_Srce_Id;
    private DbsField bc_Bc_Fldr_Log_Dte_Tme;
    private DbsField bc_Bc_Rcrd_Last_Updt_Dte;
    private DbsField bc_Bc_Rcrd_Last_Updt_Tme;
    private DbsField bc_Bc_Rcrd_Last_Updt_Userid;
    private DbsField bc_Bc_Last_Dsgntn_Srce;
    private DbsField bc_Bc_Last_Dsgntn_System;
    private DbsField bc_Bc_Last_Dsgntn_Userid;
    private DbsField bc_Bc_Last_Dsgntn_Dte;
    private DbsField bc_Bc_Last_Dsgntn_Tme;
    private DbsField bc_Bc_Last_Vrfy_Dte;
    private DbsField bc_Bc_Last_Vrfy_Tme;
    private DbsField bc_Bc_Last_Vrfy_Userid;
    private DbsField bc_Bc_Tiaa_Cref_Chng_Dte;
    private DbsField bc_Bc_Tiaa_Cref_Chng_Tme;
    private DbsField bc_Bc_Chng_Pwr_Atty;

    private DataAccessProgramView vw_bch;
    private DbsField bch_Bc_Pin;
    private DbsField bch_Bc_Tiaa_Cntrct;
    private DbsField bch_Bc_Cref_Cntrct;
    private DbsField bch_Bc_Tiaa_Cref_Ind;
    private DbsField bch_Bc_Stat;
    private DbsField bch_Bc_Cntrct_Type;
    private DbsField bch_Bc_Rqst_Timestamp;
    private DbsField bch_Bc_Eff_Dte;
    private DbsField bch_Bc_Mos_Ind;
    private DbsField bch_Bc_Irvcbl_Ind;
    private DbsField bch_Bc_Pymnt_Chld_Dcsd_Ind;
    private DbsField bch_Bc_Exempt_Spouse_Rights;
    private DbsField bch_Bc_Spouse_Waived_Bnfts;
    private DbsField bch_Bc_Trust_Data_Fldr;
    private DbsField bch_Bc_Bene_Addr_Fldr;
    private DbsField bch_Bc_Co_Owner_Data_Fldr;
    private DbsField bch_Bc_Fldr_Min;
    private DbsField bch_Bc_Fldr_Srce_Id;
    private DbsField bch_Bc_Fldr_Log_Dte_Tme;
    private DbsField bch_Bc_Rcrd_Last_Updt_Dte;
    private DbsField bch_Bc_Rcrd_Last_Updt_Tme;
    private DbsField bch_Bc_Rcrd_Last_Updt_Userid;
    private DbsField bch_Bc_Last_Dsgntn_Srce;
    private DbsField bch_Bc_Last_Dsgntn_System;
    private DbsField bch_Bc_Last_Dsgntn_Userid;
    private DbsField bch_Bc_Last_Dsgntn_Dte;
    private DbsField bch_Bc_Last_Dsgntn_Tme;
    private DbsField bch_Bc_Last_Vrfy_Dte;
    private DbsField bch_Bc_Last_Vrfy_Tme;
    private DbsField bch_Bc_Last_Vrfy_Userid;
    private DbsField bch_Bc_Tiaa_Cref_Chng_Dte;
    private DbsField bch_Bc_Tiaa_Cref_Chng_Tme;
    private DbsField bch_Bc_Chng_Pwr_Atty;

    private DataAccessProgramView vw_bd;
    private DbsField bd_Bd_Pin;
    private DbsField bd_Bd_Tiaa_Cntrct;
    private DbsField bd_Bd_Cref_Cntrct;
    private DbsField bd_Bd_Tiaa_Cref_Ind;
    private DbsField bd_Bd_Stat;
    private DbsField bd_Bd_Seq_Nmbr;
    private DbsField bd_Bd_Rqst_Timestamp;
    private DbsField bd_Bd_Rcrd_Last_Updt_Dte;
    private DbsField bd_Bd_Rcrd_Last_Updt_Tme;
    private DbsField bd_Bd_Rcrd_Last_Updt_Userid;
    private DbsField bd_Bd_Bene_Type;
    private DbsField bd_Bd_Rltn_Cd;
    private DbsField bd_Bd_Bene_Name1;
    private DbsField bd_Bd_Bene_Name2;
    private DbsField bd_Bd_Rltn_Free_Txt;
    private DbsField bd_Bd_Dte_Birth_Trust;
    private DbsField bd_Bd_Ss_Cd;
    private DbsField bd_Bd_Ss_Nmbr;
    private DbsField bd_Bd_Perc_Share_Ind;
    private DbsField bd_Bd_Share_Perc;
    private DbsField bd_Bd_Share_Ntor;
    private DbsField bd_Bd_Share_Dtor;
    private DbsField bd_Bd_Irvcbl_Ind;
    private DbsField bd_Bd_Stlmnt_Rstrctn;
    private DbsField bd_Bd_Spcl_Txt1;
    private DbsField bd_Bd_Spcl_Txt2;
    private DbsField bd_Bd_Spcl_Txt3;
    private DbsField bd_Bd_Dflt_Estate;

    private DataAccessProgramView vw_bdh;
    private DbsField bdh_Bd_Pin;
    private DbsField bdh_Bd_Tiaa_Cntrct;
    private DbsField bdh_Bd_Cref_Cntrct;
    private DbsField bdh_Bd_Tiaa_Cref_Ind;
    private DbsField bdh_Bd_Stat;
    private DbsField bdh_Bd_Seq_Nmbr;
    private DbsField bdh_Bd_Rqst_Timestamp;
    private DbsField bdh_Bd_Rcrd_Last_Updt_Dte;
    private DbsField bdh_Bd_Rcrd_Last_Updt_Tme;
    private DbsField bdh_Bd_Rcrd_Last_Updt_Userid;
    private DbsField bdh_Bd_Bene_Type;
    private DbsField bdh_Bd_Rltn_Cd;
    private DbsField bdh_Bd_Bene_Name1;
    private DbsField bdh_Bd_Bene_Name2;
    private DbsField bdh_Bd_Rltn_Free_Txt;
    private DbsField bdh_Bd_Dte_Birth_Trust;
    private DbsField bdh_Bd_Ss_Cd;
    private DbsField bdh_Bd_Ss_Nmbr;
    private DbsField bdh_Bd_Perc_Share_Ind;
    private DbsField bdh_Bd_Share_Perc;
    private DbsField bdh_Bd_Share_Ntor;
    private DbsField bdh_Bd_Share_Dtor;
    private DbsField bdh_Bd_Irvcbl_Ind;
    private DbsField bdh_Bd_Stlmnt_Rstrctn;
    private DbsField bdh_Bd_Spcl_Txt1;
    private DbsField bdh_Bd_Spcl_Txt2;
    private DbsField bdh_Bd_Spcl_Txt3;
    private DbsField bdh_Bd_Dflt_Estate;

    private DataAccessProgramView vw_bm;
    private DbsField bm_Bm_Pin;
    private DbsField bm_Bm_Tiaa_Cntrct;
    private DbsField bm_Bm_Cref_Cntrct;
    private DbsField bm_Bm_Tiaa_Cref_Ind;
    private DbsField bm_Bm_Stat;
    private DbsField bm_Bm_Rqst_Timestamp;
    private DbsField bm_Bm_Rcrd_Last_Updt_Dte;
    private DbsField bm_Bm_Rcrd_Last_Updt_Tme;
    private DbsField bm_Bm_Rcrd_Last_Updt_Userid;
    private DbsField bm_Count_Castbm_Mos_Txt_Group;

    private DbsGroup bm_Bm_Mos_Txt_Group;
    private DbsField bm_Bm_Mos_Txt;

    private DataAccessProgramView vw_bmh;
    private DbsField bmh_Bm_Pin;
    private DbsField bmh_Bm_Tiaa_Cntrct;
    private DbsField bmh_Bm_Cref_Cntrct;
    private DbsField bmh_Bm_Tiaa_Cref_Ind;
    private DbsField bmh_Bm_Stat;
    private DbsField bmh_Bm_Rqst_Timestamp;
    private DbsField bmh_Bm_Rcrd_Last_Updt_Dte;
    private DbsField bmh_Bm_Rcrd_Last_Updt_Tme;
    private DbsField bmh_Bm_Rcrd_Last_Updt_Userid;
    private DbsField bmh_Count_Castbm_Mos_Txt_Group;

    private DbsGroup bmh_Bm_Mos_Txt_Group;
    private DbsField bmh_Bm_Mos_Txt;
    private DbsField pnd_I1;
    private DbsField pnd_J1;
    private DbsField pnd_Pin_Cntrct_Ind_Key;

    private DbsGroup pnd_Pin_Cntrct_Ind_Key__R_Field_1;
    private DbsField pnd_Pin_Cntrct_Ind_Key_Pnd_Pin;
    private DbsField pnd_Pin_Cntrct_Ind_Key_Pnd_Tiaa_Cntrct;
    private DbsField pnd_Pin_Cntrct_Ind_Key_Pnd_Tiaa_Cref_Ind;
    private DbsField pnd_Stat_Pin_Cntrct_Ind_Key;

    private DbsGroup pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_2;
    private DbsField pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Stat;
    private DbsField pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Pin;
    private DbsField pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Tiaa_Cntrct;
    private DbsField pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Tiaa_Cref_Ind;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaBena9815 = new PdaBena9815(parameters);
        pdaBenpda_M = new PdaBenpda_M(parameters);
        pdaBenpda_E = new PdaBenpda_E(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_bc = new DataAccessProgramView(new NameInfo("vw_bc", "BC"), "BENE_CONTRACT_12", "BENE_CONTRACT");
        bc_Bc_Pin = vw_bc.getRecord().newFieldInGroup("bc_Bc_Pin", "BC-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BC_PIN");
        bc_Bc_Pin.setDdmHeader("PIN");
        bc_Bc_Tiaa_Cntrct = vw_bc.getRecord().newFieldInGroup("bc_Bc_Tiaa_Cntrct", "BC-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BC_TIAA_CNTRCT");
        bc_Bc_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bc_Bc_Cref_Cntrct = vw_bc.getRecord().newFieldInGroup("bc_Bc_Cref_Cntrct", "BC-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BC_CREF_CNTRCT");
        bc_Bc_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bc_Bc_Tiaa_Cref_Ind = vw_bc.getRecord().newFieldInGroup("bc_Bc_Tiaa_Cref_Ind", "BC-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_TIAA_CREF_IND");
        bc_Bc_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bc_Bc_Stat = vw_bc.getRecord().newFieldInGroup("bc_Bc_Stat", "BC-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_STAT");
        bc_Bc_Stat.setDdmHeader("STATUS");
        bc_Bc_Cntrct_Type = vw_bc.getRecord().newFieldInGroup("bc_Bc_Cntrct_Type", "BC-CNTRCT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_CNTRCT_TYPE");
        bc_Bc_Cntrct_Type.setDdmHeader("CNTRCT/TYPE");
        bc_Bc_Rqst_Timestamp = vw_bc.getRecord().newFieldInGroup("bc_Bc_Rqst_Timestamp", "BC-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BC_RQST_TIMESTAMP");
        bc_Bc_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        bc_Bc_Eff_Dte = vw_bc.getRecord().newFieldInGroup("bc_Bc_Eff_Dte", "BC-EFF-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "BC_EFF_DTE");
        bc_Bc_Eff_Dte.setDdmHeader("EFFECTIVE/DATE");
        bc_Bc_Mos_Ind = vw_bc.getRecord().newFieldInGroup("bc_Bc_Mos_Ind", "BC-MOS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_MOS_IND");
        bc_Bc_Mos_Ind.setDdmHeader("MOS/IND");
        bc_Bc_Irvcbl_Ind = vw_bc.getRecord().newFieldInGroup("bc_Bc_Irvcbl_Ind", "BC-IRVCBL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_IRVCBL_IND");
        bc_Bc_Irvcbl_Ind.setDdmHeader("IRVCBL/IND");
        bc_Bc_Pymnt_Chld_Dcsd_Ind = vw_bc.getRecord().newFieldInGroup("bc_Bc_Pymnt_Chld_Dcsd_Ind", "BC-PYMNT-CHLD-DCSD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_PYMNT_CHLD_DCSD_IND");
        bc_Bc_Pymnt_Chld_Dcsd_Ind.setDdmHeader("PYMNT/CHILD/DCSD");
        bc_Bc_Exempt_Spouse_Rights = vw_bc.getRecord().newFieldInGroup("bc_Bc_Exempt_Spouse_Rights", "BC-EXEMPT-SPOUSE-RIGHTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_EXEMPT_SPOUSE_RIGHTS");
        bc_Bc_Exempt_Spouse_Rights.setDdmHeader("EXEMPT/SPOUSAL/RIGHTS");
        bc_Bc_Spouse_Waived_Bnfts = vw_bc.getRecord().newFieldInGroup("bc_Bc_Spouse_Waived_Bnfts", "BC-SPOUSE-WAIVED-BNFTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_SPOUSE_WAIVED_BNFTS");
        bc_Bc_Spouse_Waived_Bnfts.setDdmHeader("SPOUSE/WAIVED/BENEFITS");
        bc_Bc_Trust_Data_Fldr = vw_bc.getRecord().newFieldInGroup("bc_Bc_Trust_Data_Fldr", "BC-TRUST-DATA-FLDR", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_TRUST_DATA_FLDR");
        bc_Bc_Trust_Data_Fldr.setDdmHeader("TRUST/DATA/IN FLDR");
        bc_Bc_Bene_Addr_Fldr = vw_bc.getRecord().newFieldInGroup("bc_Bc_Bene_Addr_Fldr", "BC-BENE-ADDR-FLDR", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_BENE_ADDR_FLDR");
        bc_Bc_Bene_Addr_Fldr.setDdmHeader("BENE/ADDRESS/IN FLDR");
        bc_Bc_Co_Owner_Data_Fldr = vw_bc.getRecord().newFieldInGroup("bc_Bc_Co_Owner_Data_Fldr", "BC-CO-OWNER-DATA-FLDR", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_CO_OWNER_DATA_FLDR");
        bc_Bc_Co_Owner_Data_Fldr.setDdmHeader("CO-OWNER/DATA FLDR");
        bc_Bc_Fldr_Min = vw_bc.getRecord().newFieldInGroup("bc_Bc_Fldr_Min", "BC-FLDR-MIN", FieldType.STRING, 11, RepeatingFieldStrategy.None, "BC_FLDR_MIN");
        bc_Bc_Fldr_Min.setDdmHeader("FOLDER/MIN");
        bc_Bc_Fldr_Srce_Id = vw_bc.getRecord().newFieldInGroup("bc_Bc_Fldr_Srce_Id", "BC-FLDR-SRCE-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "BC_FLDR_SRCE_ID");
        bc_Bc_Fldr_Srce_Id.setDdmHeader("FOLDER/SOURCE/ID");
        bc_Bc_Fldr_Log_Dte_Tme = vw_bc.getRecord().newFieldInGroup("bc_Bc_Fldr_Log_Dte_Tme", "BC-FLDR-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BC_FLDR_LOG_DTE_TME");
        bc_Bc_Fldr_Log_Dte_Tme.setDdmHeader("FOLDER/LOG DATE/AND TIME");
        bc_Bc_Rcrd_Last_Updt_Dte = vw_bc.getRecord().newFieldInGroup("bc_Bc_Rcrd_Last_Updt_Dte", "BC-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_RCRD_LAST_UPDT_DTE");
        bc_Bc_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bc_Bc_Rcrd_Last_Updt_Tme = vw_bc.getRecord().newFieldInGroup("bc_Bc_Rcrd_Last_Updt_Tme", "BC-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_RCRD_LAST_UPDT_TME");
        bc_Bc_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bc_Bc_Rcrd_Last_Updt_Userid = vw_bc.getRecord().newFieldInGroup("bc_Bc_Rcrd_Last_Updt_Userid", "BC-RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "BC_RCRD_LAST_UPDT_USERID");
        bc_Bc_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        bc_Bc_Last_Dsgntn_Srce = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Dsgntn_Srce", "BC-LAST-DSGNTN-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_SRCE");
        bc_Bc_Last_Dsgntn_Srce.setDdmHeader("LAST/DSGNTN/SOURCE");
        bc_Bc_Last_Dsgntn_System = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Dsgntn_System", "BC-LAST-DSGNTN-SYSTEM", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_SYSTEM");
        bc_Bc_Last_Dsgntn_System.setDdmHeader("LAST/DSGNTN/SYSTEM");
        bc_Bc_Last_Dsgntn_Userid = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Dsgntn_Userid", "BC-LAST-DSGNTN-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_USERID");
        bc_Bc_Last_Dsgntn_Userid.setDdmHeader("LAST/DSGNTN/USERID");
        bc_Bc_Last_Dsgntn_Dte = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Dsgntn_Dte", "BC-LAST-DSGNTN-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_DTE");
        bc_Bc_Last_Dsgntn_Dte.setDdmHeader("LAST/DSGNTN/DATE");
        bc_Bc_Last_Dsgntn_Tme = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Dsgntn_Tme", "BC-LAST-DSGNTN-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_TME");
        bc_Bc_Last_Dsgntn_Tme.setDdmHeader("LAST/DSGNTN/TIME");
        bc_Bc_Last_Vrfy_Dte = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Vrfy_Dte", "BC-LAST-VRFY-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_VRFY_DTE");
        bc_Bc_Last_Vrfy_Dte.setDdmHeader("LAST/VRFY/DATE");
        bc_Bc_Last_Vrfy_Tme = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Vrfy_Tme", "BC-LAST-VRFY-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_LAST_VRFY_TME");
        bc_Bc_Last_Vrfy_Tme.setDdmHeader("LAST/VRFY/TIME");
        bc_Bc_Last_Vrfy_Userid = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Vrfy_Userid", "BC-LAST-VRFY-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_VRFY_USERID");
        bc_Bc_Last_Vrfy_Userid.setDdmHeader("LAST/VRFY/USERID");
        bc_Bc_Tiaa_Cref_Chng_Dte = vw_bc.getRecord().newFieldInGroup("bc_Bc_Tiaa_Cref_Chng_Dte", "BC-TIAA-CREF-CHNG-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_TIAA_CREF_CHNG_DTE");
        bc_Bc_Tiaa_Cref_Chng_Dte.setDdmHeader("TIAA CREF/CHANGE/DATE");
        bc_Bc_Tiaa_Cref_Chng_Tme = vw_bc.getRecord().newFieldInGroup("bc_Bc_Tiaa_Cref_Chng_Tme", "BC-TIAA-CREF-CHNG-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_TIAA_CREF_CHNG_TME");
        bc_Bc_Tiaa_Cref_Chng_Tme.setDdmHeader("TIAA CREF/CHANGE/TIME");
        bc_Bc_Chng_Pwr_Atty = vw_bc.getRecord().newFieldInGroup("bc_Bc_Chng_Pwr_Atty", "BC-CHNG-PWR-ATTY", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_CHNG_PWR_ATTY");
        bc_Bc_Chng_Pwr_Atty.setDdmHeader("CHANGE/POWER/ATTNY");
        registerRecord(vw_bc);

        vw_bch = new DataAccessProgramView(new NameInfo("vw_bch", "BCH"), "BENE_CONTRACT_HIST_12", "BENE_CONT_HIST");
        bch_Bc_Pin = vw_bch.getRecord().newFieldInGroup("bch_Bc_Pin", "BC-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BC_PIN");
        bch_Bc_Pin.setDdmHeader("PIN");
        bch_Bc_Tiaa_Cntrct = vw_bch.getRecord().newFieldInGroup("bch_Bc_Tiaa_Cntrct", "BC-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BC_TIAA_CNTRCT");
        bch_Bc_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bch_Bc_Cref_Cntrct = vw_bch.getRecord().newFieldInGroup("bch_Bc_Cref_Cntrct", "BC-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BC_CREF_CNTRCT");
        bch_Bc_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bch_Bc_Tiaa_Cref_Ind = vw_bch.getRecord().newFieldInGroup("bch_Bc_Tiaa_Cref_Ind", "BC-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_TIAA_CREF_IND");
        bch_Bc_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bch_Bc_Stat = vw_bch.getRecord().newFieldInGroup("bch_Bc_Stat", "BC-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_STAT");
        bch_Bc_Stat.setDdmHeader("STATUS");
        bch_Bc_Cntrct_Type = vw_bch.getRecord().newFieldInGroup("bch_Bc_Cntrct_Type", "BC-CNTRCT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_CNTRCT_TYPE");
        bch_Bc_Cntrct_Type.setDdmHeader("CNTRCT/TYPE");
        bch_Bc_Rqst_Timestamp = vw_bch.getRecord().newFieldInGroup("bch_Bc_Rqst_Timestamp", "BC-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BC_RQST_TIMESTAMP");
        bch_Bc_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        bch_Bc_Eff_Dte = vw_bch.getRecord().newFieldInGroup("bch_Bc_Eff_Dte", "BC-EFF-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "BC_EFF_DTE");
        bch_Bc_Eff_Dte.setDdmHeader("EFFECTIVE/DATE");
        bch_Bc_Mos_Ind = vw_bch.getRecord().newFieldInGroup("bch_Bc_Mos_Ind", "BC-MOS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_MOS_IND");
        bch_Bc_Mos_Ind.setDdmHeader("MOS/IND");
        bch_Bc_Irvcbl_Ind = vw_bch.getRecord().newFieldInGroup("bch_Bc_Irvcbl_Ind", "BC-IRVCBL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_IRVCBL_IND");
        bch_Bc_Irvcbl_Ind.setDdmHeader("IRVCBL/IND");
        bch_Bc_Pymnt_Chld_Dcsd_Ind = vw_bch.getRecord().newFieldInGroup("bch_Bc_Pymnt_Chld_Dcsd_Ind", "BC-PYMNT-CHLD-DCSD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_PYMNT_CHLD_DCSD_IND");
        bch_Bc_Pymnt_Chld_Dcsd_Ind.setDdmHeader("PYMNT/CHILD/DCSD");
        bch_Bc_Exempt_Spouse_Rights = vw_bch.getRecord().newFieldInGroup("bch_Bc_Exempt_Spouse_Rights", "BC-EXEMPT-SPOUSE-RIGHTS", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "BC_EXEMPT_SPOUSE_RIGHTS");
        bch_Bc_Exempt_Spouse_Rights.setDdmHeader("EXEMPT/SPOUSAL/RIGHTS");
        bch_Bc_Spouse_Waived_Bnfts = vw_bch.getRecord().newFieldInGroup("bch_Bc_Spouse_Waived_Bnfts", "BC-SPOUSE-WAIVED-BNFTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_SPOUSE_WAIVED_BNFTS");
        bch_Bc_Spouse_Waived_Bnfts.setDdmHeader("SPOUSE/WAIVED/BENEFITS");
        bch_Bc_Trust_Data_Fldr = vw_bch.getRecord().newFieldInGroup("bch_Bc_Trust_Data_Fldr", "BC-TRUST-DATA-FLDR", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_TRUST_DATA_FLDR");
        bch_Bc_Trust_Data_Fldr.setDdmHeader("TRUST/DATA/IN FLDR");
        bch_Bc_Bene_Addr_Fldr = vw_bch.getRecord().newFieldInGroup("bch_Bc_Bene_Addr_Fldr", "BC-BENE-ADDR-FLDR", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_BENE_ADDR_FLDR");
        bch_Bc_Bene_Addr_Fldr.setDdmHeader("BENE/ADDRESS/IN FLDR");
        bch_Bc_Co_Owner_Data_Fldr = vw_bch.getRecord().newFieldInGroup("bch_Bc_Co_Owner_Data_Fldr", "BC-CO-OWNER-DATA-FLDR", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_CO_OWNER_DATA_FLDR");
        bch_Bc_Co_Owner_Data_Fldr.setDdmHeader("CO-OWNER/DATA FLDR");
        bch_Bc_Fldr_Min = vw_bch.getRecord().newFieldInGroup("bch_Bc_Fldr_Min", "BC-FLDR-MIN", FieldType.STRING, 11, RepeatingFieldStrategy.None, "BC_FLDR_MIN");
        bch_Bc_Fldr_Min.setDdmHeader("FOLDER/MIN");
        bch_Bc_Fldr_Srce_Id = vw_bch.getRecord().newFieldInGroup("bch_Bc_Fldr_Srce_Id", "BC-FLDR-SRCE-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "BC_FLDR_SRCE_ID");
        bch_Bc_Fldr_Srce_Id.setDdmHeader("FOLDER/SOURCE/ID");
        bch_Bc_Fldr_Log_Dte_Tme = vw_bch.getRecord().newFieldInGroup("bch_Bc_Fldr_Log_Dte_Tme", "BC-FLDR-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BC_FLDR_LOG_DTE_TME");
        bch_Bc_Fldr_Log_Dte_Tme.setDdmHeader("FOLDER/LOG DATE/AND TIME");
        bch_Bc_Rcrd_Last_Updt_Dte = vw_bch.getRecord().newFieldInGroup("bch_Bc_Rcrd_Last_Updt_Dte", "BC-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_RCRD_LAST_UPDT_DTE");
        bch_Bc_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bch_Bc_Rcrd_Last_Updt_Tme = vw_bch.getRecord().newFieldInGroup("bch_Bc_Rcrd_Last_Updt_Tme", "BC-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_RCRD_LAST_UPDT_TME");
        bch_Bc_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bch_Bc_Rcrd_Last_Updt_Userid = vw_bch.getRecord().newFieldInGroup("bch_Bc_Rcrd_Last_Updt_Userid", "BC-RCRD-LAST-UPDT-USERID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "BC_RCRD_LAST_UPDT_USERID");
        bch_Bc_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        bch_Bc_Last_Dsgntn_Srce = vw_bch.getRecord().newFieldInGroup("bch_Bc_Last_Dsgntn_Srce", "BC-LAST-DSGNTN-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_SRCE");
        bch_Bc_Last_Dsgntn_Srce.setDdmHeader("LAST/DSGNTN/SOURCE");
        bch_Bc_Last_Dsgntn_System = vw_bch.getRecord().newFieldInGroup("bch_Bc_Last_Dsgntn_System", "BC-LAST-DSGNTN-SYSTEM", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_SYSTEM");
        bch_Bc_Last_Dsgntn_System.setDdmHeader("LAST/DSGNTN/SYSTEM");
        bch_Bc_Last_Dsgntn_Userid = vw_bch.getRecord().newFieldInGroup("bch_Bc_Last_Dsgntn_Userid", "BC-LAST-DSGNTN-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_USERID");
        bch_Bc_Last_Dsgntn_Userid.setDdmHeader("LAST/DSGNTN/USERID");
        bch_Bc_Last_Dsgntn_Dte = vw_bch.getRecord().newFieldInGroup("bch_Bc_Last_Dsgntn_Dte", "BC-LAST-DSGNTN-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_DTE");
        bch_Bc_Last_Dsgntn_Dte.setDdmHeader("LAST/DSGNTN/DATE");
        bch_Bc_Last_Dsgntn_Tme = vw_bch.getRecord().newFieldInGroup("bch_Bc_Last_Dsgntn_Tme", "BC-LAST-DSGNTN-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_TME");
        bch_Bc_Last_Dsgntn_Tme.setDdmHeader("LAST/DSGNTN/TIME");
        bch_Bc_Last_Vrfy_Dte = vw_bch.getRecord().newFieldInGroup("bch_Bc_Last_Vrfy_Dte", "BC-LAST-VRFY-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_VRFY_DTE");
        bch_Bc_Last_Vrfy_Dte.setDdmHeader("LAST/VRFY/DATE");
        bch_Bc_Last_Vrfy_Tme = vw_bch.getRecord().newFieldInGroup("bch_Bc_Last_Vrfy_Tme", "BC-LAST-VRFY-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_LAST_VRFY_TME");
        bch_Bc_Last_Vrfy_Tme.setDdmHeader("LAST/VRFY/TIME");
        bch_Bc_Last_Vrfy_Userid = vw_bch.getRecord().newFieldInGroup("bch_Bc_Last_Vrfy_Userid", "BC-LAST-VRFY-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_VRFY_USERID");
        bch_Bc_Last_Vrfy_Userid.setDdmHeader("LAST/VRFY/USERID");
        bch_Bc_Tiaa_Cref_Chng_Dte = vw_bch.getRecord().newFieldInGroup("bch_Bc_Tiaa_Cref_Chng_Dte", "BC-TIAA-CREF-CHNG-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_TIAA_CREF_CHNG_DTE");
        bch_Bc_Tiaa_Cref_Chng_Dte.setDdmHeader("TIAA CREF/CHANGE/DATE");
        bch_Bc_Tiaa_Cref_Chng_Tme = vw_bch.getRecord().newFieldInGroup("bch_Bc_Tiaa_Cref_Chng_Tme", "BC-TIAA-CREF-CHNG-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_TIAA_CREF_CHNG_TME");
        bch_Bc_Tiaa_Cref_Chng_Tme.setDdmHeader("TIAA CREF/CHANGE/TIME");
        bch_Bc_Chng_Pwr_Atty = vw_bch.getRecord().newFieldInGroup("bch_Bc_Chng_Pwr_Atty", "BC-CHNG-PWR-ATTY", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_CHNG_PWR_ATTY");
        bch_Bc_Chng_Pwr_Atty.setDdmHeader("CHANGE/POWER/ATTNY");
        registerRecord(vw_bch);

        vw_bd = new DataAccessProgramView(new NameInfo("vw_bd", "BD"), "BENE_DESIGNATION_12", "BENE_DESIGNATION");
        bd_Bd_Pin = vw_bd.getRecord().newFieldInGroup("bd_Bd_Pin", "BD-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BD_PIN");
        bd_Bd_Pin.setDdmHeader("PIN");
        bd_Bd_Tiaa_Cntrct = vw_bd.getRecord().newFieldInGroup("bd_Bd_Tiaa_Cntrct", "BD-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BD_TIAA_CNTRCT");
        bd_Bd_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bd_Bd_Cref_Cntrct = vw_bd.getRecord().newFieldInGroup("bd_Bd_Cref_Cntrct", "BD-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BD_CREF_CNTRCT");
        bd_Bd_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bd_Bd_Tiaa_Cref_Ind = vw_bd.getRecord().newFieldInGroup("bd_Bd_Tiaa_Cref_Ind", "BD-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_TIAA_CREF_IND");
        bd_Bd_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bd_Bd_Stat = vw_bd.getRecord().newFieldInGroup("bd_Bd_Stat", "BD-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_STAT");
        bd_Bd_Stat.setDdmHeader("STAT");
        bd_Bd_Seq_Nmbr = vw_bd.getRecord().newFieldInGroup("bd_Bd_Seq_Nmbr", "BD-SEQ-NMBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "BD_SEQ_NMBR");
        bd_Bd_Seq_Nmbr.setDdmHeader(" SEQ/NMBR");
        bd_Bd_Rqst_Timestamp = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rqst_Timestamp", "BD-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BD_RQST_TIMESTAMP");
        bd_Bd_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        bd_Bd_Rcrd_Last_Updt_Dte = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rcrd_Last_Updt_Dte", "BD-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BD_RCRD_LAST_UPDT_DTE");
        bd_Bd_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bd_Bd_Rcrd_Last_Updt_Tme = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rcrd_Last_Updt_Tme", "BD-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BD_RCRD_LAST_UPDT_TME");
        bd_Bd_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bd_Bd_Rcrd_Last_Updt_Userid = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rcrd_Last_Updt_Userid", "BD-RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "BD_RCRD_LAST_UPDT_USERID");
        bd_Bd_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        bd_Bd_Bene_Type = vw_bd.getRecord().newFieldInGroup("bd_Bd_Bene_Type", "BD-BENE-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_BENE_TYPE");
        bd_Bd_Bene_Type.setDdmHeader("BENE/TYPE");
        bd_Bd_Rltn_Cd = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rltn_Cd", "BD-RLTN-CD", FieldType.STRING, 2, RepeatingFieldStrategy.None, "BD_RLTN_CD");
        bd_Bd_Rltn_Cd.setDdmHeader("RELATION/CODE");
        bd_Bd_Bene_Name1 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Bene_Name1", "BD-BENE-NAME1", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BD_BENE_NAME1");
        bd_Bd_Bene_Name1.setDdmHeader("BENEFICIARY/NAME 1");
        bd_Bd_Bene_Name2 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Bene_Name2", "BD-BENE-NAME2", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BD_BENE_NAME2");
        bd_Bd_Bene_Name2.setDdmHeader("BENEFICIARY/NAME 2");
        bd_Bd_Rltn_Free_Txt = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rltn_Free_Txt", "BD-RLTN-FREE-TXT", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BD_RLTN_FREE_TXT");
        bd_Bd_Rltn_Free_Txt.setDdmHeader("OTHER/RELATION");
        bd_Bd_Dte_Birth_Trust = vw_bd.getRecord().newFieldInGroup("bd_Bd_Dte_Birth_Trust", "BD-DTE-BIRTH-TRUST", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BD_DTE_BIRTH_TRUST");
        bd_Bd_Dte_Birth_Trust.setDdmHeader("DOB/DOT");
        bd_Bd_Ss_Cd = vw_bd.getRecord().newFieldInGroup("bd_Bd_Ss_Cd", "BD-SS-CD", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_SS_CD");
        bd_Bd_Ss_Cd.setDdmHeader("SS/CODE");
        bd_Bd_Ss_Nmbr = vw_bd.getRecord().newFieldInGroup("bd_Bd_Ss_Nmbr", "BD-SS-NMBR", FieldType.STRING, 9, RepeatingFieldStrategy.None, "BD_SS_NMBR");
        bd_Bd_Ss_Nmbr.setDdmHeader("SS/NUMBER");
        bd_Bd_Perc_Share_Ind = vw_bd.getRecord().newFieldInGroup("bd_Bd_Perc_Share_Ind", "BD-PERC-SHARE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_PERC_SHARE_IND");
        bd_Bd_Perc_Share_Ind.setDdmHeader("PERCNT/SHARE/IND");
        bd_Bd_Share_Perc = vw_bd.getRecord().newFieldInGroup("bd_Bd_Share_Perc", "BD-SHARE-PERC", FieldType.NUMERIC, 5, 2, RepeatingFieldStrategy.None, 
            "BD_SHARE_PERC");
        bd_Bd_Share_Perc.setDdmHeader("SHARE/PERCENT");
        bd_Bd_Share_Ntor = vw_bd.getRecord().newFieldInGroup("bd_Bd_Share_Ntor", "BD-SHARE-NTOR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "BD_SHARE_NTOR");
        bd_Bd_Share_Ntor.setDdmHeader("SHARE/NUMERATOR");
        bd_Bd_Share_Dtor = vw_bd.getRecord().newFieldInGroup("bd_Bd_Share_Dtor", "BD-SHARE-DTOR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "BD_SHARE_DTOR");
        bd_Bd_Share_Dtor.setDdmHeader("SHARE/DENOMINATOR");
        bd_Bd_Irvcbl_Ind = vw_bd.getRecord().newFieldInGroup("bd_Bd_Irvcbl_Ind", "BD-IRVCBL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_IRVCBL_IND");
        bd_Bd_Irvcbl_Ind.setDdmHeader("IRVCBL/IND");
        bd_Bd_Stlmnt_Rstrctn = vw_bd.getRecord().newFieldInGroup("bd_Bd_Stlmnt_Rstrctn", "BD-STLMNT-RSTRCTN", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_STLMNT_RSTRCTN");
        bd_Bd_Stlmnt_Rstrctn.setDdmHeader("SETTLEMENT/RESTRICTION");
        bd_Bd_Spcl_Txt1 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Spcl_Txt1", "BD-SPCL-TXT1", FieldType.STRING, 72, RepeatingFieldStrategy.None, "BD_SPCL_TXT1");
        bd_Bd_Spcl_Txt1.setDdmHeader("SPECIAL TEXT 1");
        bd_Bd_Spcl_Txt2 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Spcl_Txt2", "BD-SPCL-TXT2", FieldType.STRING, 72, RepeatingFieldStrategy.None, "BD_SPCL_TXT2");
        bd_Bd_Spcl_Txt2.setDdmHeader("SPECIAL TEXT 2");
        bd_Bd_Spcl_Txt3 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Spcl_Txt3", "BD-SPCL-TXT3", FieldType.STRING, 72, RepeatingFieldStrategy.None, "BD_SPCL_TXT3");
        bd_Bd_Spcl_Txt3.setDdmHeader("SPECIAL TEXT 3");
        bd_Bd_Dflt_Estate = vw_bd.getRecord().newFieldInGroup("bd_Bd_Dflt_Estate", "BD-DFLT-ESTATE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_DFLT_ESTATE");
        bd_Bd_Dflt_Estate.setDdmHeader("DEFAULT/ESTATE");
        registerRecord(vw_bd);

        vw_bdh = new DataAccessProgramView(new NameInfo("vw_bdh", "BDH"), "BENE_DESIGNATION_HIST_12", "BENE_DESIGN_HIST");
        bdh_Bd_Pin = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Pin", "BD-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BD_PIN");
        bdh_Bd_Pin.setDdmHeader("PIN");
        bdh_Bd_Tiaa_Cntrct = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Tiaa_Cntrct", "BD-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BD_TIAA_CNTRCT");
        bdh_Bd_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bdh_Bd_Cref_Cntrct = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Cref_Cntrct", "BD-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BD_CREF_CNTRCT");
        bdh_Bd_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bdh_Bd_Tiaa_Cref_Ind = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Tiaa_Cref_Ind", "BD-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_TIAA_CREF_IND");
        bdh_Bd_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bdh_Bd_Stat = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Stat", "BD-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_STAT");
        bdh_Bd_Stat.setDdmHeader("STAT");
        bdh_Bd_Seq_Nmbr = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Seq_Nmbr", "BD-SEQ-NMBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "BD_SEQ_NMBR");
        bdh_Bd_Seq_Nmbr.setDdmHeader(" SEQ/NMBR");
        bdh_Bd_Rqst_Timestamp = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Rqst_Timestamp", "BD-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BD_RQST_TIMESTAMP");
        bdh_Bd_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        bdh_Bd_Rcrd_Last_Updt_Dte = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Rcrd_Last_Updt_Dte", "BD-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BD_RCRD_LAST_UPDT_DTE");
        bdh_Bd_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bdh_Bd_Rcrd_Last_Updt_Tme = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Rcrd_Last_Updt_Tme", "BD-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BD_RCRD_LAST_UPDT_TME");
        bdh_Bd_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bdh_Bd_Rcrd_Last_Updt_Userid = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Rcrd_Last_Updt_Userid", "BD-RCRD-LAST-UPDT-USERID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "BD_RCRD_LAST_UPDT_USERID");
        bdh_Bd_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        bdh_Bd_Bene_Type = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Bene_Type", "BD-BENE-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_BENE_TYPE");
        bdh_Bd_Bene_Type.setDdmHeader("BENE/TYPE");
        bdh_Bd_Rltn_Cd = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Rltn_Cd", "BD-RLTN-CD", FieldType.STRING, 2, RepeatingFieldStrategy.None, "BD_RLTN_CD");
        bdh_Bd_Rltn_Cd.setDdmHeader("RELATION/CODE");
        bdh_Bd_Bene_Name1 = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Bene_Name1", "BD-BENE-NAME1", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "BD_BENE_NAME1");
        bdh_Bd_Bene_Name1.setDdmHeader("BENEFICIARY/NAME 1");
        bdh_Bd_Bene_Name2 = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Bene_Name2", "BD-BENE-NAME2", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "BD_BENE_NAME2");
        bdh_Bd_Bene_Name2.setDdmHeader("BENEFICIARY/NAME 2");
        bdh_Bd_Rltn_Free_Txt = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Rltn_Free_Txt", "BD-RLTN-FREE-TXT", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BD_RLTN_FREE_TXT");
        bdh_Bd_Rltn_Free_Txt.setDdmHeader("OTHER/RELATION");
        bdh_Bd_Dte_Birth_Trust = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Dte_Birth_Trust", "BD-DTE-BIRTH-TRUST", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BD_DTE_BIRTH_TRUST");
        bdh_Bd_Dte_Birth_Trust.setDdmHeader("DOB/DOT");
        bdh_Bd_Ss_Cd = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Ss_Cd", "BD-SS-CD", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_SS_CD");
        bdh_Bd_Ss_Cd.setDdmHeader("SS/CODE");
        bdh_Bd_Ss_Nmbr = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Ss_Nmbr", "BD-SS-NMBR", FieldType.STRING, 9, RepeatingFieldStrategy.None, "BD_SS_NMBR");
        bdh_Bd_Ss_Nmbr.setDdmHeader("SS/NUMBER");
        bdh_Bd_Perc_Share_Ind = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Perc_Share_Ind", "BD-PERC-SHARE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_PERC_SHARE_IND");
        bdh_Bd_Perc_Share_Ind.setDdmHeader("PERCNT/SHARE/IND");
        bdh_Bd_Share_Perc = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Share_Perc", "BD-SHARE-PERC", FieldType.NUMERIC, 5, 2, RepeatingFieldStrategy.None, 
            "BD_SHARE_PERC");
        bdh_Bd_Share_Perc.setDdmHeader("SHARE/PERCENT");
        bdh_Bd_Share_Ntor = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Share_Ntor", "BD-SHARE-NTOR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "BD_SHARE_NTOR");
        bdh_Bd_Share_Ntor.setDdmHeader("SHARE/NUMERATOR");
        bdh_Bd_Share_Dtor = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Share_Dtor", "BD-SHARE-DTOR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "BD_SHARE_DTOR");
        bdh_Bd_Share_Dtor.setDdmHeader("SHARE/DENOMINATOR");
        bdh_Bd_Irvcbl_Ind = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Irvcbl_Ind", "BD-IRVCBL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_IRVCBL_IND");
        bdh_Bd_Irvcbl_Ind.setDdmHeader("IRVCBL/IND");
        bdh_Bd_Stlmnt_Rstrctn = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Stlmnt_Rstrctn", "BD-STLMNT-RSTRCTN", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_STLMNT_RSTRCTN");
        bdh_Bd_Stlmnt_Rstrctn.setDdmHeader("SETTLEMENT/RESTRICTION");
        bdh_Bd_Spcl_Txt1 = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Spcl_Txt1", "BD-SPCL-TXT1", FieldType.STRING, 72, RepeatingFieldStrategy.None, "BD_SPCL_TXT1");
        bdh_Bd_Spcl_Txt1.setDdmHeader("SPECIAL TEXT 1");
        bdh_Bd_Spcl_Txt2 = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Spcl_Txt2", "BD-SPCL-TXT2", FieldType.STRING, 72, RepeatingFieldStrategy.None, "BD_SPCL_TXT2");
        bdh_Bd_Spcl_Txt2.setDdmHeader("SPECIAL TEXT 2");
        bdh_Bd_Spcl_Txt3 = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Spcl_Txt3", "BD-SPCL-TXT3", FieldType.STRING, 72, RepeatingFieldStrategy.None, "BD_SPCL_TXT3");
        bdh_Bd_Spcl_Txt3.setDdmHeader("SPECIAL TEXT 3");
        bdh_Bd_Dflt_Estate = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Dflt_Estate", "BD-DFLT-ESTATE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_DFLT_ESTATE");
        bdh_Bd_Dflt_Estate.setDdmHeader("DEFAULT/ESTATE");
        registerRecord(vw_bdh);

        vw_bm = new DataAccessProgramView(new NameInfo("vw_bm", "BM"), "BENE_MOS_12", "BENE_MOS", DdmPeriodicGroups.getInstance().getGroups("BENE_MOS_12"));
        bm_Bm_Pin = vw_bm.getRecord().newFieldInGroup("bm_Bm_Pin", "BM-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BM_PIN");
        bm_Bm_Pin.setDdmHeader("PIN");
        bm_Bm_Tiaa_Cntrct = vw_bm.getRecord().newFieldInGroup("bm_Bm_Tiaa_Cntrct", "BM-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BM_TIAA_CNTRCT");
        bm_Bm_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bm_Bm_Cref_Cntrct = vw_bm.getRecord().newFieldInGroup("bm_Bm_Cref_Cntrct", "BM-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BM_CREF_CNTRCT");
        bm_Bm_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bm_Bm_Tiaa_Cref_Ind = vw_bm.getRecord().newFieldInGroup("bm_Bm_Tiaa_Cref_Ind", "BM-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BM_TIAA_CREF_IND");
        bm_Bm_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bm_Bm_Stat = vw_bm.getRecord().newFieldInGroup("bm_Bm_Stat", "BM-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BM_STAT");
        bm_Bm_Stat.setDdmHeader("STAT");
        bm_Bm_Rqst_Timestamp = vw_bm.getRecord().newFieldInGroup("bm_Bm_Rqst_Timestamp", "BM-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BM_RQST_TIMESTAMP");
        bm_Bm_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        bm_Bm_Rcrd_Last_Updt_Dte = vw_bm.getRecord().newFieldInGroup("bm_Bm_Rcrd_Last_Updt_Dte", "BM-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BM_RCRD_LAST_UPDT_DTE");
        bm_Bm_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bm_Bm_Rcrd_Last_Updt_Tme = vw_bm.getRecord().newFieldInGroup("bm_Bm_Rcrd_Last_Updt_Tme", "BM-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BM_RCRD_LAST_UPDT_TME");
        bm_Bm_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bm_Bm_Rcrd_Last_Updt_Userid = vw_bm.getRecord().newFieldInGroup("bm_Bm_Rcrd_Last_Updt_Userid", "BM-RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "BM_RCRD_LAST_UPDT_USERID");
        bm_Bm_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        bm_Count_Castbm_Mos_Txt_Group = vw_bm.getRecord().newFieldInGroup("bm_Count_Castbm_Mos_Txt_Group", "C*BM-MOS-TXT-GROUP", RepeatingFieldStrategy.CAsteriskVariable, 
            "BENE_MOS_BM_MOS_TXT_GROUP");

        bm_Bm_Mos_Txt_Group = vw_bm.getRecord().newGroupArrayInGroup("bm_Bm_Mos_Txt_Group", "BM-MOS-TXT-GROUP", new DbsArrayController(1, 60) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "BENE_MOS_BM_MOS_TXT_GROUP");
        bm_Bm_Mos_Txt = bm_Bm_Mos_Txt_Group.newFieldInGroup("bm_Bm_Mos_Txt", "BM-MOS-TXT", FieldType.STRING, 72, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "BM_MOS_TXT", "BENE_MOS_BM_MOS_TXT_GROUP");
        bm_Bm_Mos_Txt.setDdmHeader("MOS TEXT");
        registerRecord(vw_bm);

        vw_bmh = new DataAccessProgramView(new NameInfo("vw_bmh", "BMH"), "BENE_MOS_HIST_12", "BENE_MOS_HIST", DdmPeriodicGroups.getInstance().getGroups("BENE_MOS_HIST_12"));
        bmh_Bm_Pin = vw_bmh.getRecord().newFieldInGroup("bmh_Bm_Pin", "BM-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BM_PIN");
        bmh_Bm_Pin.setDdmHeader("PIN");
        bmh_Bm_Tiaa_Cntrct = vw_bmh.getRecord().newFieldInGroup("bmh_Bm_Tiaa_Cntrct", "BM-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BM_TIAA_CNTRCT");
        bmh_Bm_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bmh_Bm_Cref_Cntrct = vw_bmh.getRecord().newFieldInGroup("bmh_Bm_Cref_Cntrct", "BM-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BM_CREF_CNTRCT");
        bmh_Bm_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bmh_Bm_Tiaa_Cref_Ind = vw_bmh.getRecord().newFieldInGroup("bmh_Bm_Tiaa_Cref_Ind", "BM-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BM_TIAA_CREF_IND");
        bmh_Bm_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bmh_Bm_Stat = vw_bmh.getRecord().newFieldInGroup("bmh_Bm_Stat", "BM-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BM_STAT");
        bmh_Bm_Stat.setDdmHeader("STAT");
        bmh_Bm_Rqst_Timestamp = vw_bmh.getRecord().newFieldInGroup("bmh_Bm_Rqst_Timestamp", "BM-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BM_RQST_TIMESTAMP");
        bmh_Bm_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        bmh_Bm_Rcrd_Last_Updt_Dte = vw_bmh.getRecord().newFieldInGroup("bmh_Bm_Rcrd_Last_Updt_Dte", "BM-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BM_RCRD_LAST_UPDT_DTE");
        bmh_Bm_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bmh_Bm_Rcrd_Last_Updt_Tme = vw_bmh.getRecord().newFieldInGroup("bmh_Bm_Rcrd_Last_Updt_Tme", "BM-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BM_RCRD_LAST_UPDT_TME");
        bmh_Bm_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bmh_Bm_Rcrd_Last_Updt_Userid = vw_bmh.getRecord().newFieldInGroup("bmh_Bm_Rcrd_Last_Updt_Userid", "BM-RCRD-LAST-UPDT-USERID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "BM_RCRD_LAST_UPDT_USERID");
        bmh_Bm_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        bmh_Count_Castbm_Mos_Txt_Group = vw_bmh.getRecord().newFieldInGroup("bmh_Count_Castbm_Mos_Txt_Group", "C*BM-MOS-TXT-GROUP", RepeatingFieldStrategy.CAsteriskVariable, 
            "BENE_MOS_HIST_BM_MOS_TXT_GROUP");

        bmh_Bm_Mos_Txt_Group = vw_bmh.getRecord().newGroupArrayInGroup("bmh_Bm_Mos_Txt_Group", "BM-MOS-TXT-GROUP", new DbsArrayController(1, 60) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "BENE_MOS_HIST_BM_MOS_TXT_GROUP");
        bmh_Bm_Mos_Txt = bmh_Bm_Mos_Txt_Group.newFieldInGroup("bmh_Bm_Mos_Txt", "BM-MOS-TXT", FieldType.STRING, 72, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "BM_MOS_TXT", "BENE_MOS_HIST_BM_MOS_TXT_GROUP");
        bmh_Bm_Mos_Txt.setDdmHeader("MOS TEXT");
        registerRecord(vw_bmh);

        pnd_I1 = localVariables.newFieldInRecord("pnd_I1", "#I1", FieldType.PACKED_DECIMAL, 3);
        pnd_J1 = localVariables.newFieldInRecord("pnd_J1", "#J1", FieldType.PACKED_DECIMAL, 3);
        pnd_Pin_Cntrct_Ind_Key = localVariables.newFieldInRecord("pnd_Pin_Cntrct_Ind_Key", "#PIN-CNTRCT-IND-KEY", FieldType.STRING, 23);

        pnd_Pin_Cntrct_Ind_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Pin_Cntrct_Ind_Key__R_Field_1", "REDEFINE", pnd_Pin_Cntrct_Ind_Key);
        pnd_Pin_Cntrct_Ind_Key_Pnd_Pin = pnd_Pin_Cntrct_Ind_Key__R_Field_1.newFieldInGroup("pnd_Pin_Cntrct_Ind_Key_Pnd_Pin", "#PIN", FieldType.NUMERIC, 
            12);
        pnd_Pin_Cntrct_Ind_Key_Pnd_Tiaa_Cntrct = pnd_Pin_Cntrct_Ind_Key__R_Field_1.newFieldInGroup("pnd_Pin_Cntrct_Ind_Key_Pnd_Tiaa_Cntrct", "#TIAA-CNTRCT", 
            FieldType.STRING, 10);
        pnd_Pin_Cntrct_Ind_Key_Pnd_Tiaa_Cref_Ind = pnd_Pin_Cntrct_Ind_Key__R_Field_1.newFieldInGroup("pnd_Pin_Cntrct_Ind_Key_Pnd_Tiaa_Cref_Ind", "#TIAA-CREF-IND", 
            FieldType.STRING, 1);
        pnd_Stat_Pin_Cntrct_Ind_Key = localVariables.newFieldInRecord("pnd_Stat_Pin_Cntrct_Ind_Key", "#STAT-PIN-CNTRCT-IND-KEY", FieldType.STRING, 24);

        pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_2", "REDEFINE", pnd_Stat_Pin_Cntrct_Ind_Key);
        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Stat = pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_2.newFieldInGroup("pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Stat", "#STAT", 
            FieldType.STRING, 1);
        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Pin = pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_2.newFieldInGroup("pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Pin", "#PIN", FieldType.NUMERIC, 
            12);
        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Tiaa_Cntrct = pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_2.newFieldInGroup("pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Tiaa_Cntrct", 
            "#TIAA-CNTRCT", FieldType.STRING, 10);
        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Tiaa_Cref_Ind = pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_2.newFieldInGroup("pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Tiaa_Cref_Ind", 
            "#TIAA-CREF-IND", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_bc.reset();
        vw_bch.reset();
        vw_bd.reset();
        vw_bdh.reset();
        vw_bm.reset();
        vw_bmh.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Benn9815() throws Exception
    {
        super("Benn9815");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ERROR-INFO-SUB.##LAST-PROGRAM := *PROGRAM
        pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().reset();                                                                                                        //Natural: RESET MSG-INFO-SUB.##RETURN-CODE MSG-INFO-SUB.##MSG
        pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().reset();
                                                                                                                                                                          //Natural: PERFORM BUILD-SUPER
        sub_Build_Super();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PROCESS-CONTRACT
        sub_Process_Contract();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PROCESS-MOS
        sub_Process_Mos();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PROCESS-DESIGNATION
        sub_Process_Designation();
        if (condition(Global.isEscape())) {return;}
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUILD-SUPER
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-CONTRACT
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-MOS
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-DESIGNATION
        //* ***********************************************************************
    }
    private void sub_Build_Super() throws Exception                                                                                                                       //Natural: BUILD-SUPER
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Stat.setValue("C");                                                                                                               //Natural: ASSIGN #STAT-PIN-CNTRCT-IND-KEY.#STAT := 'C'
        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Pin.setValue(pdaBena9815.getBena9815_Pnd_Pin());                                                                                  //Natural: ASSIGN #STAT-PIN-CNTRCT-IND-KEY.#PIN := BENA9815.#PIN
        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Tiaa_Cntrct.setValue(pdaBena9815.getBena9815_Pnd_Tiaa_Cntrct());                                                                  //Natural: ASSIGN #STAT-PIN-CNTRCT-IND-KEY.#TIAA-CNTRCT := BENA9815.#TIAA-CNTRCT
        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Tiaa_Cref_Ind.setValue(pdaBena9815.getBena9815_Pnd_Tiaa_Cref_Ind());                                                              //Natural: ASSIGN #STAT-PIN-CNTRCT-IND-KEY.#TIAA-CREF-IND := BENA9815.#TIAA-CREF-IND
        pnd_Pin_Cntrct_Ind_Key_Pnd_Pin.setValue(pdaBena9815.getBena9815_Pnd_Pin());                                                                                       //Natural: ASSIGN #PIN-CNTRCT-IND-KEY.#PIN := BENA9815.#PIN
        pnd_Pin_Cntrct_Ind_Key_Pnd_Tiaa_Cntrct.setValue(pdaBena9815.getBena9815_Pnd_Tiaa_Cntrct());                                                                       //Natural: ASSIGN #PIN-CNTRCT-IND-KEY.#TIAA-CNTRCT := BENA9815.#TIAA-CNTRCT
        pnd_Pin_Cntrct_Ind_Key_Pnd_Tiaa_Cref_Ind.setValue(pdaBena9815.getBena9815_Pnd_Tiaa_Cref_Ind());                                                                   //Natural: ASSIGN #PIN-CNTRCT-IND-KEY.#TIAA-CREF-IND := BENA9815.#TIAA-CREF-IND
        //*  BUILD-SUPER
    }
    //*  REPEAT AT MOST TWICE
    private void sub_Process_Contract() throws Exception                                                                                                                  //Natural: PROCESS-CONTRACT
    {
        if (BLNatReinput.isReinput()) return;

        RPT:                                                                                                                                                              //Natural: REPEAT
        while (condition(whileTrue))
        {
            vw_bc.startDatabaseFind                                                                                                                                       //Natural: FIND ( 1 ) BC WITH BC-STAT-PIN-CNTRCT-IND-KEY = #STAT-PIN-CNTRCT-IND-KEY
            (
            "FBC",
            new Wc[] { new Wc("BC_STAT_PIN_CNTRCT_IND_KEY", "=", pnd_Stat_Pin_Cntrct_Ind_Key, WcType.WITH) },
            1
            );
            FBC:
            while (condition(vw_bc.readNextRow("FBC", true)))
            {
                vw_bc.setIfNotFoundControlFlag(false);
                if (condition(vw_bc.getAstCOUNTER().equals(0)))                                                                                                           //Natural: IF NO RECORDS FOUND
                {
                    if (condition(pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Stat.equals("M")))                                                                                      //Natural: IF #STAT-PIN-CNTRCT-IND-KEY.#STAT = 'M'
                    {
                        if (true) break RPT;                                                                                                                              //Natural: ESCAPE BOTTOM ( RPT. )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Stat.setValue("M");                                                                                               //Natural: ASSIGN #STAT-PIN-CNTRCT-IND-KEY.#STAT := 'M'
                        if (true) break FBC;                                                                                                                              //Natural: ESCAPE BOTTOM ( FBC. )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-NOREC
                pdaBena9815.getBena9815_Pnd_Fldr_Log_Dte_Tme().setValue(bc_Bc_Fldr_Log_Dte_Tme);                                                                          //Natural: ASSIGN BENA9815.#FLDR-LOG-DTE-TME := BC.BC-FLDR-LOG-DTE-TME
                vw_bch.setValuesByName(vw_bc);                                                                                                                            //Natural: MOVE BY NAME BC TO BCH
                bch_Bc_Rcrd_Last_Updt_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("HHIISST"));                                                                 //Natural: MOVE EDITED *TIMX ( EM = HHIISST ) TO BCH.BC-RCRD-LAST-UPDT-TME
                bch_Bc_Rcrd_Last_Updt_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO BCH.BC-RCRD-LAST-UPDT-DTE
                bch_Bc_Rcrd_Last_Updt_Userid.setValue(Global.getPROGRAM());                                                                                               //Natural: ASSIGN BCH.BC-RCRD-LAST-UPDT-USERID := *PROGRAM
                //*  BENE-CONTRACT-HIST
                vw_bch.insertDBRow();                                                                                                                                     //Natural: STORE BCH
                //*  BENE-CONTRACT
                vw_bc.deleteDBRow("FBC");                                                                                                                                 //Natural: DELETE ( FBC. )
                if (true) break RPT;                                                                                                                                      //Natural: ESCAPE BOTTOM ( RPT. )
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RPT"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RPT"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  WILL ONLY BE HERE IF STATUS 'M' FOR FIRST TIME
            if (condition(true)) continue;                                                                                                                                //Natural: ESCAPE TOP
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  PROCESS-CONTRACT
    }
    private void sub_Process_Mos() throws Exception                                                                                                                       //Natural: PROCESS-MOS
    {
        if (BLNatReinput.isReinput()) return;

        vw_bm.startDatabaseFind                                                                                                                                           //Natural: FIND ( 1 ) BM WITH BM-STAT-PIN-CNTRCT-IND-KEY = #STAT-PIN-CNTRCT-IND-KEY
        (
        "FBM",
        new Wc[] { new Wc("BM_STAT_PIN_CNTRCT_IND_KEY", "=", pnd_Stat_Pin_Cntrct_Ind_Key, WcType.WITH) },
        1
        );
        FBM:
        while (condition(vw_bm.readNextRow("FBM", true)))
        {
            vw_bm.setIfNotFoundControlFlag(false);
            if (condition(vw_bm.getAstCOUNTER().equals(0)))                                                                                                               //Natural: IF NO RECORDS FOUND
            {
                if (true) break FBM;                                                                                                                                      //Natural: ESCAPE BOTTOM ( FBM. )
            }                                                                                                                                                             //Natural: END-NOREC
            vw_bmh.setValuesByName(vw_bm);                                                                                                                                //Natural: MOVE BY NAME BM TO BMH
            bmh_Bm_Rcrd_Last_Updt_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("HHIISST"));                                                                     //Natural: MOVE EDITED *TIMX ( EM = HHIISST ) TO BMH.BM-RCRD-LAST-UPDT-TME
            bmh_Bm_Rcrd_Last_Updt_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                    //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO BMH.BM-RCRD-LAST-UPDT-DTE
            bmh_Bm_Rcrd_Last_Updt_Userid.setValue(Global.getPROGRAM());                                                                                                   //Natural: ASSIGN BMH.BM-RCRD-LAST-UPDT-USERID := *PROGRAM
            //*  BENE-MOS-HIST
            vw_bmh.insertDBRow();                                                                                                                                         //Natural: STORE BMH
            //*  BENE-MOS
            vw_bm.deleteDBRow("FBM");                                                                                                                                     //Natural: DELETE ( FBM. )
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  PROCESS-MOS
    }
    private void sub_Process_Designation() throws Exception                                                                                                               //Natural: PROCESS-DESIGNATION
    {
        if (BLNatReinput.isReinput()) return;

        vw_bd.startDatabaseFind                                                                                                                                           //Natural: FIND BD WITH BD-PIN-CNTRCT-IND-KEY = #PIN-CNTRCT-IND-KEY
        (
        "FBD",
        new Wc[] { new Wc("BD_PIN_CNTRCT_IND_KEY", "=", pnd_Pin_Cntrct_Ind_Key, WcType.WITH) }
        );
        FBD:
        while (condition(vw_bd.readNextRow("FBD", true)))
        {
            vw_bd.setIfNotFoundControlFlag(false);
            if (condition(vw_bd.getAstCOUNTER().equals(0)))                                                                                                               //Natural: IF NO RECORDS FOUND
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-NOREC
            if (condition(bd_Bd_Stat.notEquals(pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Stat)))                                                                                    //Natural: REJECT IF BD-STAT NE #STAT-PIN-CNTRCT-IND-KEY.#STAT
            {
                continue;
            }
            vw_bdh.setValuesByName(vw_bd);                                                                                                                                //Natural: MOVE BY NAME BD TO BDH
            bdh_Bd_Rcrd_Last_Updt_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("HHIISST"));                                                                     //Natural: MOVE EDITED *TIMX ( EM = HHIISST ) TO BDH.BD-RCRD-LAST-UPDT-TME
            bdh_Bd_Rcrd_Last_Updt_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                    //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO BDH.BD-RCRD-LAST-UPDT-DTE
            bdh_Bd_Rcrd_Last_Updt_Userid.setValue(Global.getPROGRAM());                                                                                                   //Natural: ASSIGN BDH.BD-RCRD-LAST-UPDT-USERID := *PROGRAM
            //*  BENE-DESIGNATION-HIST
            vw_bdh.insertDBRow();                                                                                                                                         //Natural: STORE BDH
            vw_bd.deleteDBRow("FBD");                                                                                                                                     //Natural: DELETE ( FBD. )
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  PROCESS-DESIGNATION
    }

    //
}
