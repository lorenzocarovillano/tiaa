/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:06:14 PM
**        * FROM NATURAL SUBPROGRAM : Benn9816
************************************************************
**        * FILE NAME            : Benn9816.java
**        * CLASS NAME           : Benn9816
**        * INSTANCE NAME        : Benn9816
************************************************************
************************************************************************
* PROGRAM  : BENN9816
* SYSTEM   : BENEFICIARY-SYSTEM
* TITLE    : CHECK FOR BENE RECORDS - ANY STATUS, ANY TIAA-CREF-IND
* GENERATED: JULY 31, 2000
*          :
*          :
*
* HISTORY
* >
* >
************************************************************************

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benn9816 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaBena9816 pdaBena9816;
    private PdaBenpda_M pdaBenpda_M;
    private PdaBenpda_E pdaBenpda_E;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_bene_Contract;
    private DbsField bene_Contract_Bc_Tiaa_Cref_Ind;
    private DbsField pnd_I1;
    private DbsField pnd_J1;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaBena9816 = new PdaBena9816(parameters);
        pdaBenpda_M = new PdaBenpda_M(parameters);
        pdaBenpda_E = new PdaBenpda_E(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_bene_Contract = new DataAccessProgramView(new NameInfo("vw_bene_Contract", "BENE-CONTRACT"), "BENE_CONTRACT_12", "BENE_CONTRACT");
        bene_Contract_Bc_Tiaa_Cref_Ind = vw_bene_Contract.getRecord().newFieldInGroup("bene_Contract_Bc_Tiaa_Cref_Ind", "BC-TIAA-CREF-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BC_TIAA_CREF_IND");
        bene_Contract_Bc_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        registerRecord(vw_bene_Contract);

        pnd_I1 = localVariables.newFieldInRecord("pnd_I1", "#I1", FieldType.PACKED_DECIMAL, 3);
        pnd_J1 = localVariables.newFieldInRecord("pnd_J1", "#J1", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_bene_Contract.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Benn9816() throws Exception
    {
        super("Benn9816");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ERROR-INFO-SUB.##LAST-PROGRAM = *PROGRAM
        //*  FOR ESCAPE BOTTOM
        MAIN_ROUTINE:                                                                                                                                                     //Natural: REPEAT
        while (condition(whileTrue))
        {
                                                                                                                                                                          //Natural: PERFORM INITIALIZATIONS
            sub_Initializations();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("MAIN_ROUTINE"))) break;
                else if (condition(Global.isEscapeBottomImmediate("MAIN_ROUTINE"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM FIND-CONTRACT
            sub_Find_Contract();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("MAIN_ROUTINE"))) break;
                else if (condition(Global.isEscapeBottomImmediate("MAIN_ROUTINE"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (true) break MAIN_ROUTINE;                                                                                                                                 //Natural: ESCAPE BOTTOM ( MAIN-ROUTINE. )
            //* ************************************
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ESCAPE-MAIN-ROUTINE
            //*  MAIN-ROUTINE.
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-CONTRACT
        //* ******************************
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZATIONS
    }
    private void sub_Escape_Main_Routine() throws Exception                                                                                                               //Natural: ESCAPE-MAIN-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( MAIN-ROUTINE. )
        Global.setEscapeCode(EscapeType.Bottom, "MAIN_ROUTINE");
        if (true) return;
        //*  ESCAPE-MAIN-ROUTINE
    }
    private void sub_Find_Contract() throws Exception                                                                                                                     //Natural: FIND-CONTRACT
    {
        if (BLNatReinput.isReinput()) return;

        vw_bene_Contract.startDatabaseFind                                                                                                                                //Natural: FIND ( 1 ) BENE-CONTRACT WITH BC-TIAA-CNTRCT = BENA9816.#TIAA-CNTRCT
        (
        "F1",
        new Wc[] { new Wc("BC_TIAA_CNTRCT", "=", pdaBena9816.getBena9816_Pnd_Tiaa_Cntrct(), WcType.WITH) },
        1
        );
        F1:
        while (condition(vw_bene_Contract.readNextRow("F1")))
        {
            vw_bene_Contract.setIfNotFoundControlFlag(false);
            pdaBena9816.getBena9816_Pnd_Tiaa_Cref_Ind().setValue(bene_Contract_Bc_Tiaa_Cref_Ind);                                                                         //Natural: ASSIGN BENA9816.#TIAA-CREF-IND = BENE-CONTRACT.BC-TIAA-CREF-IND
            pdaBena9816.getBena9816_Pnd_Found().setValue(true);                                                                                                           //Natural: ASSIGN BENA9816.#FOUND = TRUE
            //*  PERFORM ESCAPE-MAIN-ROUTINE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  FIND-CONTRACT
    }
    private void sub_Initializations() throws Exception                                                                                                                   //Natural: INITIALIZATIONS
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().reset();                                                                                                        //Natural: RESET MSG-INFO-SUB.##RETURN-CODE MSG-INFO-SUB.##MSG
        pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().reset();
        //*  INITIALIZATIONS
    }

    //
}
