/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:06:58 PM
**        * FROM NATURAL SUBPROGRAM : Benn9870
************************************************************
**        * FILE NAME            : Benn9870.java
**        * CLASS NAME           : Benn9870
**        * INSTANCE NAME        : Benn9870
************************************************************
************************************************************************
* PROGRAM  : BENN9870 - CLONED FROM MIGRATION - BENN8164
* SYSTEM   : BENEFICIARY-SYSTEM MIGRATION TO BENE MAINTENANCE FILES.
* TITLE    : UPDATE BENE MAINTENANCE FOR 'Same As'CONTRACTS
* GENERATED: JUNE 20, 2000
*
* THIS PROGRAM USES THE PASSED SAME-AS-CONTRACT-ISN TO GET THE BENE
* MAINTENANCE CONTRACT FOR WHICH SUBJECT MIGRATION CONTRACT HAS THE SAME
* DESIGNATION. THIS DESIGNATION IS THEN COPIED TO MIGRATE THE CONTRACT.
************************************************************************
*    DATE      USERID                   DESCRIPTION
* MM/DD/YYYY  XXXXXXXX  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
* 04/01/2017  DURAND    PIN EXPANSION N7 TO N12
************************************************************************

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benn9870 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaBena9878 pdaBena9878;
    private PdaBena9860 pdaBena9860;
    private PdaBena9873 pdaBena9873;
    private PdaBenpda_M pdaBenpda_M;
    private PdaBenpda_E pdaBenpda_E;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_bc;
    private DbsField bc_Bc_Pin;
    private DbsField bc_Bc_Tiaa_Cntrct;
    private DbsField bc_Bc_Cref_Cntrct;
    private DbsField bc_Bc_Tiaa_Cref_Ind;
    private DbsField bc_Bc_Stat;
    private DbsField bc_Bc_Cntrct_Type;
    private DbsField bc_Bc_Rqst_Timestamp;
    private DbsField bc_Bc_Eff_Dte;
    private DbsField bc_Bc_Mos_Ind;
    private DbsField bc_Bc_Irvcbl_Ind;
    private DbsField bc_Bc_Pymnt_Chld_Dcsd_Ind;
    private DbsField bc_Bc_Exempt_Spouse_Rights;
    private DbsField bc_Bc_Spouse_Waived_Bnfts;
    private DbsField bc_Bc_Trust_Data_Fldr;
    private DbsField bc_Bc_Bene_Addr_Fldr;
    private DbsField bc_Bc_Co_Owner_Data_Fldr;
    private DbsField bc_Bc_Fldr_Min;
    private DbsField bc_Bc_Fldr_Srce_Id;
    private DbsField bc_Bc_Fldr_Log_Dte_Tme;
    private DbsField bc_Bc_Rcrd_Last_Updt_Dte;
    private DbsField bc_Bc_Rcrd_Last_Updt_Tme;
    private DbsField bc_Bc_Rcrd_Last_Updt_Userid;
    private DbsField bc_Bc_Last_Dsgntn_Srce;
    private DbsField bc_Bc_Last_Dsgntn_System;
    private DbsField bc_Bc_Last_Dsgntn_Userid;
    private DbsField bc_Bc_Last_Dsgntn_Dte;
    private DbsField bc_Bc_Last_Dsgntn_Tme;
    private DbsField bc_Bc_Last_Vrfy_Dte;
    private DbsField bc_Bc_Last_Vrfy_Tme;
    private DbsField bc_Bc_Last_Vrfy_Userid;
    private DbsField bc_Bc_Tiaa_Cref_Chng_Dte;
    private DbsField bc_Bc_Tiaa_Cref_Chng_Tme;
    private DbsField bc_Bc_Chng_Pwr_Atty;

    private DataAccessProgramView vw_bd;
    private DbsField bd_Bd_Pin;
    private DbsField bd_Bd_Tiaa_Cntrct;
    private DbsField bd_Bd_Cref_Cntrct;
    private DbsField bd_Bd_Tiaa_Cref_Ind;
    private DbsField bd_Bd_Stat;
    private DbsField bd_Bd_Seq_Nmbr;
    private DbsField bd_Bd_Rqst_Timestamp;
    private DbsField bd_Bd_Rcrd_Last_Updt_Dte;
    private DbsField bd_Bd_Rcrd_Last_Updt_Tme;
    private DbsField bd_Bd_Rcrd_Last_Updt_Userid;
    private DbsField bd_Bd_Bene_Type;
    private DbsField bd_Bd_Rltn_Cd;
    private DbsField bd_Bd_Bene_Name1;
    private DbsField bd_Bd_Bene_Name2;
    private DbsField bd_Bd_Rltn_Free_Txt;
    private DbsField bd_Bd_Dte_Birth_Trust;
    private DbsField bd_Bd_Ss_Cd;
    private DbsField bd_Bd_Ss_Nmbr;
    private DbsField bd_Bd_Perc_Share_Ind;
    private DbsField bd_Bd_Share_Perc;
    private DbsField bd_Bd_Share_Ntor;
    private DbsField bd_Bd_Share_Dtor;
    private DbsField bd_Bd_Irvcbl_Ind;
    private DbsField bd_Bd_Stlmnt_Rstrctn;
    private DbsField bd_Bd_Spcl_Txt1;
    private DbsField bd_Bd_Spcl_Txt2;
    private DbsField bd_Bd_Spcl_Txt3;
    private DbsField bd_Bd_Dflt_Estate;
    private DbsField bd_Bd_Mdo_Calc_Bene;
    private DbsField bd_Bd_Addr1;
    private DbsField bd_Bd_Addr2;
    private DbsField bd_Bd_Addr3_City;
    private DbsField bd_Bd_State;
    private DbsField bd_Bd_Zip;
    private DbsField bd_Bd_Phone;
    private DbsField bd_Bd_Gender;
    private DbsField bd_Bd_Country;

    private DataAccessProgramView vw_bm;
    private DbsField bm_Bm_Pin;
    private DbsField bm_Bm_Tiaa_Cntrct;
    private DbsField bm_Bm_Cref_Cntrct;
    private DbsField bm_Bm_Tiaa_Cref_Ind;
    private DbsField bm_Bm_Stat;
    private DbsField bm_Bm_Rqst_Timestamp;
    private DbsField bm_Bm_Rcrd_Last_Updt_Dte;
    private DbsField bm_Bm_Rcrd_Last_Updt_Tme;
    private DbsField bm_Bm_Rcrd_Last_Updt_Userid;
    private DbsField bm_Count_Castbm_Mos_Txt_Group;

    private DbsGroup bm_Bm_Mos_Txt_Group;
    private DbsField bm_Bm_Mos_Txt;
    private DbsField pnd_Stat_Pin_Cntrct_Ind_Key;

    private DbsGroup pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_1;
    private DbsField pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Key_Stat;
    private DbsField pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Key_Pin;
    private DbsField pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Key_Tiaa_Cntrct;
    private DbsField pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Key_Tiaa_Cref_Ind;
    private DbsField pnd_Timx;
    private DbsField pnd_Datx;
    private DbsField pnd_I;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaBena9878 = new PdaBena9878(parameters);
        pdaBena9860 = new PdaBena9860(parameters);
        pdaBena9873 = new PdaBena9873(parameters);
        pdaBenpda_M = new PdaBenpda_M(parameters);
        pdaBenpda_E = new PdaBenpda_E(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_bc = new DataAccessProgramView(new NameInfo("vw_bc", "BC"), "BENE_CONTRACT_12", "BENE_CONTRACT");
        bc_Bc_Pin = vw_bc.getRecord().newFieldInGroup("bc_Bc_Pin", "BC-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BC_PIN");
        bc_Bc_Pin.setDdmHeader("PIN");
        bc_Bc_Tiaa_Cntrct = vw_bc.getRecord().newFieldInGroup("bc_Bc_Tiaa_Cntrct", "BC-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BC_TIAA_CNTRCT");
        bc_Bc_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bc_Bc_Cref_Cntrct = vw_bc.getRecord().newFieldInGroup("bc_Bc_Cref_Cntrct", "BC-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BC_CREF_CNTRCT");
        bc_Bc_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bc_Bc_Tiaa_Cref_Ind = vw_bc.getRecord().newFieldInGroup("bc_Bc_Tiaa_Cref_Ind", "BC-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_TIAA_CREF_IND");
        bc_Bc_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bc_Bc_Stat = vw_bc.getRecord().newFieldInGroup("bc_Bc_Stat", "BC-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_STAT");
        bc_Bc_Stat.setDdmHeader("STATUS");
        bc_Bc_Cntrct_Type = vw_bc.getRecord().newFieldInGroup("bc_Bc_Cntrct_Type", "BC-CNTRCT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_CNTRCT_TYPE");
        bc_Bc_Cntrct_Type.setDdmHeader("CNTRCT/TYPE");
        bc_Bc_Rqst_Timestamp = vw_bc.getRecord().newFieldInGroup("bc_Bc_Rqst_Timestamp", "BC-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BC_RQST_TIMESTAMP");
        bc_Bc_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        bc_Bc_Eff_Dte = vw_bc.getRecord().newFieldInGroup("bc_Bc_Eff_Dte", "BC-EFF-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "BC_EFF_DTE");
        bc_Bc_Eff_Dte.setDdmHeader("EFFECTIVE/DATE");
        bc_Bc_Mos_Ind = vw_bc.getRecord().newFieldInGroup("bc_Bc_Mos_Ind", "BC-MOS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_MOS_IND");
        bc_Bc_Mos_Ind.setDdmHeader("MOS/IND");
        bc_Bc_Irvcbl_Ind = vw_bc.getRecord().newFieldInGroup("bc_Bc_Irvcbl_Ind", "BC-IRVCBL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_IRVCBL_IND");
        bc_Bc_Irvcbl_Ind.setDdmHeader("IRVCBL/IND");
        bc_Bc_Pymnt_Chld_Dcsd_Ind = vw_bc.getRecord().newFieldInGroup("bc_Bc_Pymnt_Chld_Dcsd_Ind", "BC-PYMNT-CHLD-DCSD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_PYMNT_CHLD_DCSD_IND");
        bc_Bc_Pymnt_Chld_Dcsd_Ind.setDdmHeader("PYMNT/CHILD/DCSD");
        bc_Bc_Exempt_Spouse_Rights = vw_bc.getRecord().newFieldInGroup("bc_Bc_Exempt_Spouse_Rights", "BC-EXEMPT-SPOUSE-RIGHTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_EXEMPT_SPOUSE_RIGHTS");
        bc_Bc_Exempt_Spouse_Rights.setDdmHeader("EXEMPT/SPOUSAL/RIGHTS");
        bc_Bc_Spouse_Waived_Bnfts = vw_bc.getRecord().newFieldInGroup("bc_Bc_Spouse_Waived_Bnfts", "BC-SPOUSE-WAIVED-BNFTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_SPOUSE_WAIVED_BNFTS");
        bc_Bc_Spouse_Waived_Bnfts.setDdmHeader("SPOUSE/WAIVED/BENEFITS");
        bc_Bc_Trust_Data_Fldr = vw_bc.getRecord().newFieldInGroup("bc_Bc_Trust_Data_Fldr", "BC-TRUST-DATA-FLDR", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_TRUST_DATA_FLDR");
        bc_Bc_Trust_Data_Fldr.setDdmHeader("TRUST/DATA/IN FLDR");
        bc_Bc_Bene_Addr_Fldr = vw_bc.getRecord().newFieldInGroup("bc_Bc_Bene_Addr_Fldr", "BC-BENE-ADDR-FLDR", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_BENE_ADDR_FLDR");
        bc_Bc_Bene_Addr_Fldr.setDdmHeader("BENE/ADDRESS/IN FLDR");
        bc_Bc_Co_Owner_Data_Fldr = vw_bc.getRecord().newFieldInGroup("bc_Bc_Co_Owner_Data_Fldr", "BC-CO-OWNER-DATA-FLDR", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_CO_OWNER_DATA_FLDR");
        bc_Bc_Co_Owner_Data_Fldr.setDdmHeader("CO-OWNER/DATA FLDR");
        bc_Bc_Fldr_Min = vw_bc.getRecord().newFieldInGroup("bc_Bc_Fldr_Min", "BC-FLDR-MIN", FieldType.STRING, 11, RepeatingFieldStrategy.None, "BC_FLDR_MIN");
        bc_Bc_Fldr_Min.setDdmHeader("FOLDER/MIN");
        bc_Bc_Fldr_Srce_Id = vw_bc.getRecord().newFieldInGroup("bc_Bc_Fldr_Srce_Id", "BC-FLDR-SRCE-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "BC_FLDR_SRCE_ID");
        bc_Bc_Fldr_Srce_Id.setDdmHeader("FOLDER/SOURCE/ID");
        bc_Bc_Fldr_Log_Dte_Tme = vw_bc.getRecord().newFieldInGroup("bc_Bc_Fldr_Log_Dte_Tme", "BC-FLDR-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BC_FLDR_LOG_DTE_TME");
        bc_Bc_Fldr_Log_Dte_Tme.setDdmHeader("FOLDER/LOG DATE/AND TIME");
        bc_Bc_Rcrd_Last_Updt_Dte = vw_bc.getRecord().newFieldInGroup("bc_Bc_Rcrd_Last_Updt_Dte", "BC-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_RCRD_LAST_UPDT_DTE");
        bc_Bc_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bc_Bc_Rcrd_Last_Updt_Tme = vw_bc.getRecord().newFieldInGroup("bc_Bc_Rcrd_Last_Updt_Tme", "BC-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_RCRD_LAST_UPDT_TME");
        bc_Bc_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bc_Bc_Rcrd_Last_Updt_Userid = vw_bc.getRecord().newFieldInGroup("bc_Bc_Rcrd_Last_Updt_Userid", "BC-RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "BC_RCRD_LAST_UPDT_USERID");
        bc_Bc_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        bc_Bc_Last_Dsgntn_Srce = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Dsgntn_Srce", "BC-LAST-DSGNTN-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_SRCE");
        bc_Bc_Last_Dsgntn_Srce.setDdmHeader("LAST/DSGNTN/SOURCE");
        bc_Bc_Last_Dsgntn_System = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Dsgntn_System", "BC-LAST-DSGNTN-SYSTEM", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_SYSTEM");
        bc_Bc_Last_Dsgntn_System.setDdmHeader("LAST/DSGNTN/SYSTEM");
        bc_Bc_Last_Dsgntn_Userid = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Dsgntn_Userid", "BC-LAST-DSGNTN-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_USERID");
        bc_Bc_Last_Dsgntn_Userid.setDdmHeader("LAST/DSGNTN/USERID");
        bc_Bc_Last_Dsgntn_Dte = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Dsgntn_Dte", "BC-LAST-DSGNTN-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_DTE");
        bc_Bc_Last_Dsgntn_Dte.setDdmHeader("LAST/DSGNTN/DATE");
        bc_Bc_Last_Dsgntn_Tme = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Dsgntn_Tme", "BC-LAST-DSGNTN-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_TME");
        bc_Bc_Last_Dsgntn_Tme.setDdmHeader("LAST/DSGNTN/TIME");
        bc_Bc_Last_Vrfy_Dte = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Vrfy_Dte", "BC-LAST-VRFY-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_VRFY_DTE");
        bc_Bc_Last_Vrfy_Dte.setDdmHeader("LAST/VRFY/DATE");
        bc_Bc_Last_Vrfy_Tme = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Vrfy_Tme", "BC-LAST-VRFY-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_LAST_VRFY_TME");
        bc_Bc_Last_Vrfy_Tme.setDdmHeader("LAST/VRFY/TIME");
        bc_Bc_Last_Vrfy_Userid = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Vrfy_Userid", "BC-LAST-VRFY-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_VRFY_USERID");
        bc_Bc_Last_Vrfy_Userid.setDdmHeader("LAST/VRFY/USERID");
        bc_Bc_Tiaa_Cref_Chng_Dte = vw_bc.getRecord().newFieldInGroup("bc_Bc_Tiaa_Cref_Chng_Dte", "BC-TIAA-CREF-CHNG-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_TIAA_CREF_CHNG_DTE");
        bc_Bc_Tiaa_Cref_Chng_Dte.setDdmHeader("TIAA CREF/CHANGE/DATE");
        bc_Bc_Tiaa_Cref_Chng_Tme = vw_bc.getRecord().newFieldInGroup("bc_Bc_Tiaa_Cref_Chng_Tme", "BC-TIAA-CREF-CHNG-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_TIAA_CREF_CHNG_TME");
        bc_Bc_Tiaa_Cref_Chng_Tme.setDdmHeader("TIAA CREF/CHANGE/TIME");
        bc_Bc_Chng_Pwr_Atty = vw_bc.getRecord().newFieldInGroup("bc_Bc_Chng_Pwr_Atty", "BC-CHNG-PWR-ATTY", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_CHNG_PWR_ATTY");
        bc_Bc_Chng_Pwr_Atty.setDdmHeader("CHANGE/POWER/ATTNY");
        registerRecord(vw_bc);

        vw_bd = new DataAccessProgramView(new NameInfo("vw_bd", "BD"), "BENE_DESIGNATION_12", "BENE_DESIGNATION");
        bd_Bd_Pin = vw_bd.getRecord().newFieldInGroup("bd_Bd_Pin", "BD-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BD_PIN");
        bd_Bd_Pin.setDdmHeader("PIN");
        bd_Bd_Tiaa_Cntrct = vw_bd.getRecord().newFieldInGroup("bd_Bd_Tiaa_Cntrct", "BD-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BD_TIAA_CNTRCT");
        bd_Bd_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bd_Bd_Cref_Cntrct = vw_bd.getRecord().newFieldInGroup("bd_Bd_Cref_Cntrct", "BD-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BD_CREF_CNTRCT");
        bd_Bd_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bd_Bd_Tiaa_Cref_Ind = vw_bd.getRecord().newFieldInGroup("bd_Bd_Tiaa_Cref_Ind", "BD-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_TIAA_CREF_IND");
        bd_Bd_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bd_Bd_Stat = vw_bd.getRecord().newFieldInGroup("bd_Bd_Stat", "BD-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_STAT");
        bd_Bd_Stat.setDdmHeader("STAT");
        bd_Bd_Seq_Nmbr = vw_bd.getRecord().newFieldInGroup("bd_Bd_Seq_Nmbr", "BD-SEQ-NMBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "BD_SEQ_NMBR");
        bd_Bd_Seq_Nmbr.setDdmHeader(" SEQ/NMBR");
        bd_Bd_Rqst_Timestamp = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rqst_Timestamp", "BD-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BD_RQST_TIMESTAMP");
        bd_Bd_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        bd_Bd_Rcrd_Last_Updt_Dte = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rcrd_Last_Updt_Dte", "BD-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BD_RCRD_LAST_UPDT_DTE");
        bd_Bd_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bd_Bd_Rcrd_Last_Updt_Tme = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rcrd_Last_Updt_Tme", "BD-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BD_RCRD_LAST_UPDT_TME");
        bd_Bd_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bd_Bd_Rcrd_Last_Updt_Userid = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rcrd_Last_Updt_Userid", "BD-RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "BD_RCRD_LAST_UPDT_USERID");
        bd_Bd_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        bd_Bd_Bene_Type = vw_bd.getRecord().newFieldInGroup("bd_Bd_Bene_Type", "BD-BENE-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_BENE_TYPE");
        bd_Bd_Bene_Type.setDdmHeader("BENE/TYPE");
        bd_Bd_Rltn_Cd = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rltn_Cd", "BD-RLTN-CD", FieldType.STRING, 2, RepeatingFieldStrategy.None, "BD_RLTN_CD");
        bd_Bd_Rltn_Cd.setDdmHeader("RELATION/CODE");
        bd_Bd_Bene_Name1 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Bene_Name1", "BD-BENE-NAME1", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BD_BENE_NAME1");
        bd_Bd_Bene_Name1.setDdmHeader("BENEFICIARY/NAME 1");
        bd_Bd_Bene_Name2 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Bene_Name2", "BD-BENE-NAME2", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BD_BENE_NAME2");
        bd_Bd_Bene_Name2.setDdmHeader("BENEFICIARY/NAME 2");
        bd_Bd_Rltn_Free_Txt = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rltn_Free_Txt", "BD-RLTN-FREE-TXT", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BD_RLTN_FREE_TXT");
        bd_Bd_Rltn_Free_Txt.setDdmHeader("OTHER/RELATION");
        bd_Bd_Dte_Birth_Trust = vw_bd.getRecord().newFieldInGroup("bd_Bd_Dte_Birth_Trust", "BD-DTE-BIRTH-TRUST", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BD_DTE_BIRTH_TRUST");
        bd_Bd_Dte_Birth_Trust.setDdmHeader("DOB/DOT");
        bd_Bd_Ss_Cd = vw_bd.getRecord().newFieldInGroup("bd_Bd_Ss_Cd", "BD-SS-CD", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_SS_CD");
        bd_Bd_Ss_Cd.setDdmHeader("SS/CODE");
        bd_Bd_Ss_Nmbr = vw_bd.getRecord().newFieldInGroup("bd_Bd_Ss_Nmbr", "BD-SS-NMBR", FieldType.STRING, 9, RepeatingFieldStrategy.None, "BD_SS_NMBR");
        bd_Bd_Ss_Nmbr.setDdmHeader("SS/NUMBER");
        bd_Bd_Perc_Share_Ind = vw_bd.getRecord().newFieldInGroup("bd_Bd_Perc_Share_Ind", "BD-PERC-SHARE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_PERC_SHARE_IND");
        bd_Bd_Perc_Share_Ind.setDdmHeader("PERCNT/SHARE/IND");
        bd_Bd_Share_Perc = vw_bd.getRecord().newFieldInGroup("bd_Bd_Share_Perc", "BD-SHARE-PERC", FieldType.NUMERIC, 5, 2, RepeatingFieldStrategy.None, 
            "BD_SHARE_PERC");
        bd_Bd_Share_Perc.setDdmHeader("SHARE/PERCENT");
        bd_Bd_Share_Ntor = vw_bd.getRecord().newFieldInGroup("bd_Bd_Share_Ntor", "BD-SHARE-NTOR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "BD_SHARE_NTOR");
        bd_Bd_Share_Ntor.setDdmHeader("SHARE/NUMERATOR");
        bd_Bd_Share_Dtor = vw_bd.getRecord().newFieldInGroup("bd_Bd_Share_Dtor", "BD-SHARE-DTOR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "BD_SHARE_DTOR");
        bd_Bd_Share_Dtor.setDdmHeader("SHARE/DENOMINATOR");
        bd_Bd_Irvcbl_Ind = vw_bd.getRecord().newFieldInGroup("bd_Bd_Irvcbl_Ind", "BD-IRVCBL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_IRVCBL_IND");
        bd_Bd_Irvcbl_Ind.setDdmHeader("IRVCBL/IND");
        bd_Bd_Stlmnt_Rstrctn = vw_bd.getRecord().newFieldInGroup("bd_Bd_Stlmnt_Rstrctn", "BD-STLMNT-RSTRCTN", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_STLMNT_RSTRCTN");
        bd_Bd_Stlmnt_Rstrctn.setDdmHeader("SETTLEMENT/RESTRICTION");
        bd_Bd_Spcl_Txt1 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Spcl_Txt1", "BD-SPCL-TXT1", FieldType.STRING, 72, RepeatingFieldStrategy.None, "BD_SPCL_TXT1");
        bd_Bd_Spcl_Txt1.setDdmHeader("SPECIAL TEXT 1");
        bd_Bd_Spcl_Txt2 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Spcl_Txt2", "BD-SPCL-TXT2", FieldType.STRING, 72, RepeatingFieldStrategy.None, "BD_SPCL_TXT2");
        bd_Bd_Spcl_Txt2.setDdmHeader("SPECIAL TEXT 2");
        bd_Bd_Spcl_Txt3 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Spcl_Txt3", "BD-SPCL-TXT3", FieldType.STRING, 72, RepeatingFieldStrategy.None, "BD_SPCL_TXT3");
        bd_Bd_Spcl_Txt3.setDdmHeader("SPECIAL TEXT 3");
        bd_Bd_Dflt_Estate = vw_bd.getRecord().newFieldInGroup("bd_Bd_Dflt_Estate", "BD-DFLT-ESTATE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_DFLT_ESTATE");
        bd_Bd_Dflt_Estate.setDdmHeader("DEFAULT/ESTATE");
        bd_Bd_Mdo_Calc_Bene = vw_bd.getRecord().newFieldInGroup("bd_Bd_Mdo_Calc_Bene", "BD-MDO-CALC-BENE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_MDO_CALC_BENE");
        bd_Bd_Mdo_Calc_Bene.setDdmHeader("MDO/CALC/BENE");
        bd_Bd_Addr1 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Addr1", "BD-ADDR1", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BD_ADDR1");
        bd_Bd_Addr2 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Addr2", "BD-ADDR2", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BD_ADDR2");
        bd_Bd_Addr3_City = vw_bd.getRecord().newFieldInGroup("bd_Bd_Addr3_City", "BD-ADDR3-CITY", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BD_ADDR3_CITY");
        bd_Bd_State = vw_bd.getRecord().newFieldInGroup("bd_Bd_State", "BD-STATE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "BD_STATE");
        bd_Bd_Zip = vw_bd.getRecord().newFieldInGroup("bd_Bd_Zip", "BD-ZIP", FieldType.STRING, 10, RepeatingFieldStrategy.None, "BD_ZIP");
        bd_Bd_Phone = vw_bd.getRecord().newFieldInGroup("bd_Bd_Phone", "BD-PHONE", FieldType.STRING, 20, RepeatingFieldStrategy.None, "BD_PHONE");
        bd_Bd_Gender = vw_bd.getRecord().newFieldInGroup("bd_Bd_Gender", "BD-GENDER", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_GENDER");
        bd_Bd_Country = vw_bd.getRecord().newFieldInGroup("bd_Bd_Country", "BD-COUNTRY", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BD_COUNTRY");
        registerRecord(vw_bd);

        vw_bm = new DataAccessProgramView(new NameInfo("vw_bm", "BM"), "BENE_MOS_12", "BENE_MOS", DdmPeriodicGroups.getInstance().getGroups("BENE_MOS_12"));
        bm_Bm_Pin = vw_bm.getRecord().newFieldInGroup("bm_Bm_Pin", "BM-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BM_PIN");
        bm_Bm_Pin.setDdmHeader("PIN");
        bm_Bm_Tiaa_Cntrct = vw_bm.getRecord().newFieldInGroup("bm_Bm_Tiaa_Cntrct", "BM-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BM_TIAA_CNTRCT");
        bm_Bm_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bm_Bm_Cref_Cntrct = vw_bm.getRecord().newFieldInGroup("bm_Bm_Cref_Cntrct", "BM-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BM_CREF_CNTRCT");
        bm_Bm_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bm_Bm_Tiaa_Cref_Ind = vw_bm.getRecord().newFieldInGroup("bm_Bm_Tiaa_Cref_Ind", "BM-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BM_TIAA_CREF_IND");
        bm_Bm_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bm_Bm_Stat = vw_bm.getRecord().newFieldInGroup("bm_Bm_Stat", "BM-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BM_STAT");
        bm_Bm_Stat.setDdmHeader("STAT");
        bm_Bm_Rqst_Timestamp = vw_bm.getRecord().newFieldInGroup("bm_Bm_Rqst_Timestamp", "BM-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BM_RQST_TIMESTAMP");
        bm_Bm_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        bm_Bm_Rcrd_Last_Updt_Dte = vw_bm.getRecord().newFieldInGroup("bm_Bm_Rcrd_Last_Updt_Dte", "BM-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BM_RCRD_LAST_UPDT_DTE");
        bm_Bm_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bm_Bm_Rcrd_Last_Updt_Tme = vw_bm.getRecord().newFieldInGroup("bm_Bm_Rcrd_Last_Updt_Tme", "BM-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BM_RCRD_LAST_UPDT_TME");
        bm_Bm_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bm_Bm_Rcrd_Last_Updt_Userid = vw_bm.getRecord().newFieldInGroup("bm_Bm_Rcrd_Last_Updt_Userid", "BM-RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "BM_RCRD_LAST_UPDT_USERID");
        bm_Bm_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        bm_Count_Castbm_Mos_Txt_Group = vw_bm.getRecord().newFieldInGroup("bm_Count_Castbm_Mos_Txt_Group", "C*BM-MOS-TXT-GROUP", RepeatingFieldStrategy.CAsteriskVariable, 
            "BENE_MOS_BM_MOS_TXT_GROUP");

        bm_Bm_Mos_Txt_Group = vw_bm.getRecord().newGroupInGroup("bm_Bm_Mos_Txt_Group", "BM-MOS-TXT-GROUP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "BENE_MOS_BM_MOS_TXT_GROUP");
        bm_Bm_Mos_Txt = bm_Bm_Mos_Txt_Group.newFieldArrayInGroup("bm_Bm_Mos_Txt", "BM-MOS-TXT", FieldType.STRING, 72, new DbsArrayController(1, 60) , 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "BM_MOS_TXT", "BENE_MOS_BM_MOS_TXT_GROUP");
        bm_Bm_Mos_Txt.setDdmHeader("MOS TEXT");
        registerRecord(vw_bm);

        pnd_Stat_Pin_Cntrct_Ind_Key = localVariables.newFieldInRecord("pnd_Stat_Pin_Cntrct_Ind_Key", "#STAT-PIN-CNTRCT-IND-KEY", FieldType.STRING, 24);

        pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_1", "REDEFINE", pnd_Stat_Pin_Cntrct_Ind_Key);
        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Key_Stat = pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_1.newFieldInGroup("pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Key_Stat", 
            "#KEY-STAT", FieldType.STRING, 1);
        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Key_Pin = pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_1.newFieldInGroup("pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Key_Pin", "#KEY-PIN", 
            FieldType.NUMERIC, 12);
        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Key_Tiaa_Cntrct = pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_1.newFieldInGroup("pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Key_Tiaa_Cntrct", 
            "#KEY-TIAA-CNTRCT", FieldType.STRING, 10);
        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Key_Tiaa_Cref_Ind = pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_1.newFieldInGroup("pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Key_Tiaa_Cref_Ind", 
            "#KEY-TIAA-CREF-IND", FieldType.STRING, 1);
        pnd_Timx = localVariables.newFieldInRecord("pnd_Timx", "#TIMX", FieldType.STRING, 7);
        pnd_Datx = localVariables.newFieldInRecord("pnd_Datx", "#DATX", FieldType.STRING, 8);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_bc.reset();
        vw_bd.reset();
        vw_bm.reset();

        localVariables.reset();
        pnd_Stat_Pin_Cntrct_Ind_Key.setInitialValue("C");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Benn9870() throws Exception
    {
        super("Benn9870");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ERROR-INFO-SUB.##LAST-PROGRAM := *PROGRAM
        PROGRAM:                                                                                                                                                          //Natural: REPEAT
        while (condition(whileTrue))
        {
            //*  BENA9878 BENA9860
            if (condition(pdaBena9878.getBena9878_Pnd_Copy_From_Cntrct_Isn().lessOrEqual(getZero())))                                                                     //Natural: IF BENA9878.#COPY-FROM-CNTRCT-ISN LE 0
            {
                //*  CREATE CRASH, HANDLED IN BENP9860
                pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Same AS passed from BENN9878 missing Same AS ISN!!", pdaBena9860.getBena9860_Tiaa_Cntrct(),  //Natural: COMPRESS 'Same AS passed from BENN9878 missing Same AS ISN!!' BENA9860.TIAA-CNTRCT *PROGRAM TO ##MSG
                    Global.getPROGRAM()));
                pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("T");                                                                                          //Natural: ASSIGN ##RETURN-CODE := 'T'
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROGRAM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROGRAM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            GBC:                                                                                                                                                          //Natural: GET BC #COPY-FROM-CNTRCT-ISN
            vw_bc.readByID(pdaBena9878.getBena9878_Pnd_Copy_From_Cntrct_Isn().getLong(), "GBC");
            pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Key_Pin.setValue(bc_Bc_Pin);                                                                                                  //Natural: ASSIGN #KEY-PIN := BC-PIN
            pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Key_Tiaa_Cntrct.setValue(bc_Bc_Tiaa_Cntrct);                                                                                  //Natural: ASSIGN #KEY-TIAA-CNTRCT := BC-TIAA-CNTRCT
            pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Key_Tiaa_Cref_Ind.setValue(bc_Bc_Tiaa_Cref_Ind);                                                                              //Natural: ASSIGN #KEY-TIAA-CREF-IND := BC-TIAA-CREF-IND
            if (condition(bc_Bc_Stat.notEquals("C")))                                                                                                                     //Natural: IF BC-STAT NE 'C'
            {
                //*  CREATE CRASH, HANDLED IN BENP8960
                pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Status of Contract to be copied is not'C'!:", bc_Bc_Stat, pdaBena9860.getBena9860_Tiaa_Cntrct(),  //Natural: COMPRESS 'Status of Contract to be copied is not"C"!:' BC-STAT BENA9860.TIAA-CNTRCT *PROGRAM TO ##MSG
                    Global.getPROGRAM()));
                pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("T");                                                                                          //Natural: ASSIGN ##RETURN-CODE := 'T'
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROGRAM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROGRAM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PROCESS-CONTRACT
            sub_Process_Contract();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PROGRAM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PROGRAM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pdaBena9878.getBena9878_Pnd_Mos_Ind().equals("Y")))                                                                                             //Natural: IF BENA9878.#MOS-IND = 'Y'
            {
                                                                                                                                                                          //Natural: PERFORM PROCESS-MOS
                sub_Process_Mos();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROGRAM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROGRAM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM PROCESS-DESIGNATION
                sub_Process_Designation();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROGRAM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROGRAM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ESCAPE-PROGRAM
            if (true) break PROGRAM;                                                                                                                                      //Natural: ESCAPE BOTTOM ( PROGRAM. )
            //*  PROGRAM
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-CONTRACT
        //*  BENA9860
        //*  JULIAN AND FERN DISCUSSED THIS - WE DECIDED THIS
        //*  FIELD IS NOT IMPORTANT FOR SAME AS COPYING. BC-EXEMPT-SPOUSE-RIGHTS &
        //*  BC-SPOUSE-WAIVED-BNFTS ARE COPIED AS THEY ARE PART OF THE DESIGNTN.
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-MOS
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-DESIGNATION
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-ERROR
        //* ***********************************************************************
    }
    private void sub_Escape_Program() throws Exception                                                                                                                    //Natural: ESCAPE-PROGRAM
    {
        if (BLNatReinput.isReinput()) return;

        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( PROGRAM. )
        Global.setEscapeCode(EscapeType.Bottom, "PROGRAM");
        if (true) return;
        //*  ESCAPE-PROGRAM
    }
    private void sub_Process_Contract() throws Exception                                                                                                                  //Natural: PROCESS-CONTRACT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  BENA9860
        if (condition(bc_Bc_Pin.notEquals(pdaBena9860.getBena9860_Pin())))                                                                                                //Natural: IF BC.BC-PIN NE BENA9860.PIN
        {
            pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Same As PIN different to Interface. Same As:", bc_Bc_Pin, "Intrfce:",                    //Natural: COMPRESS 'Same As PIN different to Interface. Same As:' BC.BC-PIN 'Intrfce:' BENA9860.PIN TO ##MSG
                pdaBena9860.getBena9860_Pin()));
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        bc_Bc_Tiaa_Cntrct.setValue(pdaBena9860.getBena9860_Tiaa_Cntrct());                                                                                                //Natural: ASSIGN BC.BC-TIAA-CNTRCT := BENA9860.TIAA-CNTRCT
        bc_Bc_Cref_Cntrct.setValue(pdaBena9860.getBena9860_Cref_Cntrct());                                                                                                //Natural: ASSIGN BC.BC-CREF-CNTRCT := BENA9860.CREF-CNTRCT
        bc_Bc_Tiaa_Cref_Ind.setValue(pdaBena9860.getBena9860_Tiaa_Cref_Ind());                                                                                            //Natural: ASSIGN BC.BC-TIAA-CREF-IND := BENA9860.TIAA-CREF-IND
        bc_Bc_Cntrct_Type.setValue(pdaBena9860.getBena9860_Cntrct_Type());                                                                                                //Natural: ASSIGN BC.BC-CNTRCT-TYPE := BENA9860.CNTRCT-TYPE
        bc_Bc_Eff_Dte.setValue(pdaBena9860.getBena9860_Eff_Dte());                                                                                                        //Natural: ASSIGN BC.BC-EFF-DTE := BENA9860.EFF-DTE
        pnd_Datx.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                                         //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO #DATX
        pnd_Timx.setValueEdited(Global.getTIMX(),new ReportEditMask("HHIISST"));                                                                                          //Natural: MOVE EDITED *TIMX ( EM = HHIISST ) TO #TIMX
        //*  INTRFACE
        bc_Bc_Rqst_Timestamp.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Datx, pnd_Timx));                                                               //Natural: COMPRESS #DATX #TIMX INTO BC.BC-RQST-TIMESTAMP LEAVING NO
        bc_Bc_Rcrd_Last_Updt_Userid.setValue(Global.getPROGRAM());                                                                                                        //Natural: ASSIGN BC.BC-RCRD-LAST-UPDT-USERID := *PROGRAM
        bc_Bc_Last_Dsgntn_Userid.setValue(Global.getPROGRAM());                                                                                                           //Natural: ASSIGN BC.BC-LAST-DSGNTN-USERID := *PROGRAM
        bc_Bc_Last_Dsgntn_Srce.setValue("I");                                                                                                                             //Natural: ASSIGN BC.BC-LAST-DSGNTN-SRCE := 'I'
        bc_Bc_Rcrd_Last_Updt_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                         //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO BC.BC-RCRD-LAST-UPDT-DTE
        bc_Bc_Rcrd_Last_Updt_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("HHIISST"));                                                                          //Natural: MOVE EDITED *TIMX ( EM = HHIISST ) TO BC.BC-RCRD-LAST-UPDT-TME
        bc_Bc_Last_Dsgntn_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("HHIISST"));                                                                             //Natural: MOVE EDITED *TIMX ( EM = HHIISST ) TO BC.BC-LAST-DSGNTN-TME
        bc_Bc_Last_Dsgntn_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("HHIISST"));                                                                             //Natural: MOVE EDITED *TIMX ( EM = HHIISST ) TO BC-LAST-DSGNTN-TME
        if (condition(pdaBena9860.getBena9860_Rqstng_Systm().equals("CORR")))                                                                                             //Natural: IF BENA9860.RQSTNG-SYSTM = 'CORR'
        {
            bc_Bc_Last_Dsgntn_System.setValue("X");                                                                                                                       //Natural: ASSIGN BC.BC-LAST-DSGNTN-SYSTEM := 'X'
            bc_Bc_Fldr_Log_Dte_Tme.setValue(pdaBena9860.getBena9860_Fldr_Log_Dte_Tme());                                                                                  //Natural: ASSIGN BC.BC-FLDR-LOG-DTE-TME := BENA9860.FLDR-LOG-DTE-TME
            //*  WILL TRUNCATE: A/C/M
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            bc_Bc_Last_Dsgntn_System.setValue(pdaBena9860.getBena9860_Intrfcng_Systm());                                                                                  //Natural: ASSIGN BC.BC-LAST-DSGNTN-SYSTEM := BENA9860.INTRFCNG-SYSTM
            bc_Bc_Fldr_Log_Dte_Tme.setValue(pdaBena9860.getBena9860_Fldr_Log_Dte_Tme());                                                                                  //Natural: ASSIGN BC.BC-FLDR-LOG-DTE-TME := FLDR-LOG-DTE-TME
        }                                                                                                                                                                 //Natural: END-IF
        bc_Bc_Last_Dsgntn_Dte.setValue(pdaBena9873.getBena9873_Pnd_Bsnss_Dte());                                                                                          //Natural: ASSIGN BC-LAST-DSGNTN-DTE := BENA9873.#BSNSS-DTE
        pdaBena9878.getBena9878_Pnd_Mos_Ind().setValue(bc_Bc_Mos_Ind);                                                                                                    //Natural: ASSIGN BENA9878.#MOS-IND := BC.BC-MOS-IND
        //*  BENE-CONTRACT
        bc_Bc_Trust_Data_Fldr.reset();                                                                                                                                    //Natural: RESET BC.BC-TRUST-DATA-FLDR BC.BC-BENE-ADDR-FLDR BC.BC-CO-OWNER-DATA-FLDR BC.BC-FLDR-MIN BC.BC-FLDR-SRCE-ID BC.BC-LAST-VRFY-DTE BC.BC-LAST-VRFY-TME BC.BC-LAST-VRFY-USERID BC.BC-TIAA-CREF-CHNG-DTE BC.BC-TIAA-CREF-CHNG-TME BC.BC-CHNG-PWR-ATTY
        bc_Bc_Bene_Addr_Fldr.reset();
        bc_Bc_Co_Owner_Data_Fldr.reset();
        bc_Bc_Fldr_Min.reset();
        bc_Bc_Fldr_Srce_Id.reset();
        bc_Bc_Last_Vrfy_Dte.reset();
        bc_Bc_Last_Vrfy_Tme.reset();
        bc_Bc_Last_Vrfy_Userid.reset();
        bc_Bc_Tiaa_Cref_Chng_Dte.reset();
        bc_Bc_Tiaa_Cref_Chng_Tme.reset();
        bc_Bc_Chng_Pwr_Atty.reset();
        SC:                                                                                                                                                               //Natural: STORE BC
        vw_bc.insertDBRow("SC");
        //*  PROCESS-CONTRACT
    }
    private void sub_Process_Mos() throws Exception                                                                                                                       //Natural: PROCESS-MOS
    {
        if (BLNatReinput.isReinput()) return;

        vw_bm.startDatabaseFind                                                                                                                                           //Natural: FIND ( 1 ) BM WITH BM-STAT-PIN-CNTRCT-IND-KEY = #STAT-PIN-CNTRCT-IND-KEY
        (
        "FBM",
        new Wc[] { new Wc("BM_STAT_PIN_CNTRCT_IND_KEY", "=", pnd_Stat_Pin_Cntrct_Ind_Key, WcType.WITH) },
        1
        );
        FBM:
        while (condition(vw_bm.readNextRow("FBM", true)))
        {
            vw_bm.setIfNotFoundControlFlag(false);
            if (condition(vw_bm.getAstCOUNTER().equals(0)))                                                                                                               //Natural: IF NO RECORDS FOUND
            {
                //*  CREATE CRASH, HANDLED IN BENP8100
                pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("System Error! No MOS record on Maintenance file", pdaBena9860.getBena9860_Tiaa_Cntrct(),  //Natural: COMPRESS 'System Error! No MOS record on Maintenance file' BENA9860.TIAA-CNTRCT *PROGRAM TO ##MSG
                    Global.getPROGRAM()));
                pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("T");                                                                                          //Natural: ASSIGN ##RETURN-CODE := 'T'
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FBM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FBM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-NOREC
            bm_Bm_Tiaa_Cntrct.setValue(pdaBena9860.getBena9860_Tiaa_Cntrct());                                                                                            //Natural: ASSIGN BM-TIAA-CNTRCT := BENA9860.TIAA-CNTRCT
            bm_Bm_Cref_Cntrct.setValue(pdaBena9860.getBena9860_Cref_Cntrct());                                                                                            //Natural: ASSIGN BM-CREF-CNTRCT := BENA9860.CREF-CNTRCT
            bm_Bm_Tiaa_Cref_Ind.setValue(pdaBena9860.getBena9860_Tiaa_Cref_Ind());                                                                                        //Natural: ASSIGN BM-TIAA-CREF-IND := BENA9860.TIAA-CREF-IND
            bm_Bm_Rqst_Timestamp.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Datx, pnd_Timx));                                                           //Natural: COMPRESS #DATX #TIMX INTO BM-RQST-TIMESTAMP LEAVING NO
            bm_Bm_Rcrd_Last_Updt_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                     //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO BM-RCRD-LAST-UPDT-DTE
            bm_Bm_Rcrd_Last_Updt_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("HHIISST"));                                                                      //Natural: MOVE EDITED *TIMX ( EM = HHIISST ) TO BM-RCRD-LAST-UPDT-TME
            bm_Bm_Rcrd_Last_Updt_Userid.setValue(Global.getPROGRAM());                                                                                                    //Natural: ASSIGN BM-RCRD-LAST-UPDT-USERID := *PROGRAM
            //*  BM-MOS-TXT REMAINS THE SAME AS PARENT XXXXXXXX CONTRACT.
            //*  BENE-MOS
            vw_bm.insertDBRow();                                                                                                                                          //Natural: STORE BM
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  PROCESS-MOS
    }
    private void sub_Process_Designation() throws Exception                                                                                                               //Natural: PROCESS-DESIGNATION
    {
        if (BLNatReinput.isReinput()) return;

        vw_bd.startDatabaseRead                                                                                                                                           //Natural: READ BD BY BD-STAT-PIN-CNTRCT-IND-TYPE-SEQ = #STAT-PIN-CNTRCT-IND-KEY
        (
        "RBD",
        new Wc[] { new Wc("BD_STAT_PIN_CNTRCT_IND_TYPE_SEQ", ">=", pnd_Stat_Pin_Cntrct_Ind_Key, WcType.BY) },
        new Oc[] { new Oc("BD_STAT_PIN_CNTRCT_IND_TYPE_SEQ", "ASC") }
        );
        RBD:
        while (condition(vw_bd.readNextRow("RBD")))
        {
            if (condition(bd_Bd_Stat.notEquals("C") || bd_Bd_Pin.notEquals(pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Key_Pin) || bd_Bd_Tiaa_Cntrct.notEquals(pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Key_Tiaa_Cntrct)  //Natural: IF BD-STAT NE 'C' OR BD-PIN NE #KEY-PIN OR BD-TIAA-CNTRCT NE #KEY-TIAA-CNTRCT OR BD-TIAA-CREF-IND NE #KEY-TIAA-CREF-IND
                || bd_Bd_Tiaa_Cref_Ind.notEquals(pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Key_Tiaa_Cref_Ind)))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            bd_Bd_Tiaa_Cntrct.setValue(pdaBena9860.getBena9860_Tiaa_Cntrct());                                                                                            //Natural: ASSIGN BD-TIAA-CNTRCT := BENA9860.TIAA-CNTRCT
            bd_Bd_Cref_Cntrct.setValue(pdaBena9860.getBena9860_Cref_Cntrct());                                                                                            //Natural: ASSIGN BD-CREF-CNTRCT := BENA9860.CREF-CNTRCT
            bd_Bd_Tiaa_Cref_Ind.setValue(pdaBena9860.getBena9860_Tiaa_Cref_Ind());                                                                                        //Natural: ASSIGN BD-TIAA-CREF-IND := BENA9860.TIAA-CREF-IND
            bd_Bd_Rqst_Timestamp.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Datx, pnd_Timx));                                                           //Natural: COMPRESS #DATX #TIMX INTO BD-RQST-TIMESTAMP LEAVING NO
            bd_Bd_Rcrd_Last_Updt_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                     //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO BD-RCRD-LAST-UPDT-DTE
            bd_Bd_Rcrd_Last_Updt_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("HHIISST"));                                                                      //Natural: MOVE EDITED *TIMX ( EM = HHIISST ) TO BD-RCRD-LAST-UPDT-TME
            bd_Bd_Rcrd_Last_Updt_Userid.setValue(Global.getPROGRAM());                                                                                                    //Natural: ASSIGN BD-RCRD-LAST-UPDT-USERID := *PROGRAM
            //*  OTHER BD FIELDS REMAIN THE SAME AS PARENT XXXXXXXX CONTRACT.
            //*  BENE-DESIGNATION
            vw_bd.insertDBRow();                                                                                                                                          //Natural: STORE BD
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(vw_bd.getAstCOUNTER().lessOrEqual(1)))                                                                                                              //Natural: IF *COUNTER ( RBD. ) LE 1
        {
            //*  CREATE CRASH, HANDLED IN BENP8100
            pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("System Error! No Des rec on Maintenance file", pnd_Stat_Pin_Cntrct_Ind_Key,              //Natural: COMPRESS 'System Error! No Des rec on Maintenance file' #STAT-PIN-CNTRCT-IND-KEY *PROGRAM TO ##MSG
                Global.getPROGRAM()));
            pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("T");                                                                                              //Natural: ASSIGN ##RETURN-CODE := 'T'
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  PROCESS-DESIGNATION
    }
    private void sub_Process_Error() throws Exception                                                                                                                     //Natural: PROCESS-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        pdaBena9878.getBena9878_Pnd_Error_Pgm().setValue(Global.getPROGRAM());                                                                                            //Natural: ASSIGN #ERROR-PGM := *PROGRAM
                                                                                                                                                                          //Natural: PERFORM ESCAPE-PROGRAM
        sub_Escape_Program();
        if (condition(Global.isEscape())) {return;}
        //*  PROCESS-ERROR
    }

    //
}
