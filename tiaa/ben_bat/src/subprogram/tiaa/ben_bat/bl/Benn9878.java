/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:07:25 PM
**        * FROM NATURAL SUBPROGRAM : Benn9878
************************************************************
**        * FILE NAME            : Benn9878.java
**        * CLASS NAME           : Benn9878
**        * INSTANCE NAME        : Benn9878
************************************************************
************************************************************************
* PROGRAM  : BENN9878
* SYSTEM   : BENEFICIARY-SYSTEM INTERFACE SAME AS
* TITLE    : EDIT FOR "Same as" CONTRACT # AND FIND ON BENE MAINTENANCE
* GENERATED: OCTOBER, 19, 2001
* THIS PROGRAM IS TO SEE IF CONTRACT # IN 'Same As' CLAUSE IS VALID AND
* THAT THIS CONTRACT EXISTS IN THE BENE SYSTEM. THE TIAA-CREF-IND ON THE
* MAINTENANCE FILE IS ALSO CHECKED TO SEE THAT THE CONTRACT # IN 'SAME
* AS' clause actually has a designation of the same kind.
* CLONED FROM MIGRATION BENN8163
************************************************************************
*   DATE      USERID                   DESCRIPTION
* MM/DD/YYYY  XXXXXXXX  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
* 04/01/2017  DURAND    PIN EXPANSION N7 TO N12
* 09/16/2005  DURAND    CONVERT COR/NAS FILES TO PROCOBOL CALLS
* 12/06/2007  DURAND    COR.CNTRCT-STATUS-CDE = ' ','Y','H' = ACTIVE
************************************************************************

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benn9878 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaBena9878 pdaBena9878;
    private PdaBenpda_M pdaBenpda_M;
    private PdaBenpda_E pdaBenpda_E;
    private PdaBena9879 pdaBena9879;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_bene_Contract;
    private DbsField bene_Contract_Bc_Stat;
    private DbsField bene_Contract_Bc_Tiaa_Cref_Ind;
    private DbsField bene_Contract_Bc_Cref_Cntrct;
    private DbsField bene_Contract_Bc_Eff_Dte;
    private DbsField pnd_Arr_Crash;
    private DbsField pnd_Cref_On_Maint;
    private DbsField pnd_Cref_Tab;
    private DbsField pnd_Data_After_1st_Bene;
    private DbsField pnd_Des_Found;
    private DbsField pnd_Des_Same_As_Lit;
    private DbsField pnd_I;
    private DbsField pnd_N1;
    private DbsField pnd_Same_Hold;
    private DbsField pnd_Sep_Tab;
    private DbsField pnd_Tiaa_On_Maint;

    private DbsGroup pnd_Psgm003_Common_Area;

    private DbsGroup pnd_Psgm003_Common_Area_Pnd_Psgm003_Input;
    private DbsField pnd_Psgm003_Common_Area_Pnd_In_Function;

    private DbsGroup pnd_Psgm003_Common_Area_Pnd_In_Data;
    private DbsField pnd_Psgm003_Common_Area_Pnd_In_Contract_Num_Typ;
    private DbsField pnd_Psgm003_Common_Area_Pnd_In_Type;
    private DbsField pnd_Psgm003_Common_Area_Pnd_In_Ssnpin;
    private DbsField pnd_Psgm003_Common_Area_Pnd_In_Contract;
    private DbsField pnd_Psgm003_Common_Area_Pnd_In_Payee_Cd;

    private DbsGroup pnd_Psgm003_Common_Area_Pnd_Rt_Area;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Ret_Code;

    private DbsGroup pnd_Psgm003_Common_Area_Pnd_Rt_Record;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Input_String;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Ssn;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Pin;

    private DbsGroup pnd_Psgm003_Common_Area__R_Field_1;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Pin_Num;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Tiaa_Cntrct_Nmbr;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Xpayee_Cde;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Status_Code;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Ctrct_Status_Dt;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Issue_Dt;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Lob;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cref_Contract_Nbr;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cref_Issued_Ind;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Ownership_Code;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Product_Code;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Option_Code;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Plan_Code;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Fund_Code;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Social_Code;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Custodial_Agreement;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Aas_Ind;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Platform_Ind;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Multi_Plan_Ind;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Tiaa_Mailed_Ind;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cref_Mailed_Ind;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Dflt_Enrollment_Ind;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Lst_Updt_Dt;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Lst_Updt_Time;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Lst_Updt_Usrid;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Co_Per_Addr_Ind;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Type_Cd;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_One;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_Two;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_Three;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_Four;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_Five;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_City;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_State;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Co_Zip;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Co_Country;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Postal_Data;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Geo_Cd;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Status;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Status_Dt;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Lst_Ch_Src_Cd;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Lst_Ch_Dt;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Lst_Ch_Time;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Lst_Ch_Usrid;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Per_Addr_Ind;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Type_Cd;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_One;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_Two;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_Three;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_Four;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_Five;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_City;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_State;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Zip;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Country;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Ch_Saving_Cde;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Ph_Bnk_Pt_Acct_Nbr;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Bnk_Aba_Eft_Nbr;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Eft_Pay_Type_Cde;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Postal_Data;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Geo_Cd;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Status;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Status_Dt;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Lst_Ch_Src_Cd;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Lst_Ch_Dt;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Lst_Ch_Time;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Lst_Ch_Usrid;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Prefix;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Last_Name;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_First_Name;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Given_Name_Two;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Suffix_Desc;
    private DbsField pnd_Psgm003_Common_Area_Pnd_Rt_Record_Status;
    private int psgm003ReturnCode;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaBena9879 = new PdaBena9879(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaBena9878 = new PdaBena9878(parameters);
        pdaBenpda_M = new PdaBenpda_M(parameters);
        pdaBenpda_E = new PdaBenpda_E(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        vw_bene_Contract = new DataAccessProgramView(new NameInfo("vw_bene_Contract", "BENE-CONTRACT"), "BENE_CONTRACT_12", "BENE_CONTRACT");
        bene_Contract_Bc_Stat = vw_bene_Contract.getRecord().newFieldInGroup("bene_Contract_Bc_Stat", "BC-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_STAT");
        bene_Contract_Bc_Stat.setDdmHeader("STATUS");
        bene_Contract_Bc_Tiaa_Cref_Ind = vw_bene_Contract.getRecord().newFieldInGroup("bene_Contract_Bc_Tiaa_Cref_Ind", "BC-TIAA-CREF-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BC_TIAA_CREF_IND");
        bene_Contract_Bc_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bene_Contract_Bc_Cref_Cntrct = vw_bene_Contract.getRecord().newFieldInGroup("bene_Contract_Bc_Cref_Cntrct", "BC-CREF-CNTRCT", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "BC_CREF_CNTRCT");
        bene_Contract_Bc_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bene_Contract_Bc_Eff_Dte = vw_bene_Contract.getRecord().newFieldInGroup("bene_Contract_Bc_Eff_Dte", "BC-EFF-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_EFF_DTE");
        bene_Contract_Bc_Eff_Dte.setDdmHeader("EFFECTIVE/DATE");
        registerRecord(vw_bene_Contract);

        pnd_Arr_Crash = localVariables.newFieldArrayInRecord("pnd_Arr_Crash", "#ARR-CRASH", FieldType.NUMERIC, 1, new DbsArrayController(1, 1));
        pnd_Cref_On_Maint = localVariables.newFieldInRecord("pnd_Cref_On_Maint", "#CREF-ON-MAINT", FieldType.BOOLEAN, 1);
        pnd_Cref_Tab = localVariables.newFieldArrayInRecord("pnd_Cref_Tab", "#CREF-TAB", FieldType.STRING, 1, new DbsArrayController(1, 10));
        pnd_Data_After_1st_Bene = localVariables.newFieldInRecord("pnd_Data_After_1st_Bene", "#DATA-AFTER-1ST-BENE", FieldType.BOOLEAN, 1);
        pnd_Des_Found = localVariables.newFieldInRecord("pnd_Des_Found", "#DES-FOUND", FieldType.BOOLEAN, 1);
        pnd_Des_Same_As_Lit = localVariables.newFieldInRecord("pnd_Des_Same_As_Lit", "#DES-SAME-AS-LIT", FieldType.STRING, 7);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_N1 = localVariables.newFieldInRecord("pnd_N1", "#N1", FieldType.NUMERIC, 1);
        pnd_Same_Hold = localVariables.newFieldInRecord("pnd_Same_Hold", "#SAME-HOLD", FieldType.STRING, 20);
        pnd_Sep_Tab = localVariables.newFieldArrayInRecord("pnd_Sep_Tab", "#SEP-TAB", FieldType.STRING, 72, new DbsArrayController(1, 72));
        pnd_Tiaa_On_Maint = localVariables.newFieldInRecord("pnd_Tiaa_On_Maint", "#TIAA-ON-MAINT", FieldType.BOOLEAN, 1);

        pnd_Psgm003_Common_Area = localVariables.newGroupInRecord("pnd_Psgm003_Common_Area", "#PSGM003-COMMON-AREA");

        pnd_Psgm003_Common_Area_Pnd_Psgm003_Input = pnd_Psgm003_Common_Area.newGroupInGroup("pnd_Psgm003_Common_Area_Pnd_Psgm003_Input", "#PSGM003-INPUT");
        pnd_Psgm003_Common_Area_Pnd_In_Function = pnd_Psgm003_Common_Area_Pnd_Psgm003_Input.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_In_Function", 
            "#IN-FUNCTION", FieldType.STRING, 2);

        pnd_Psgm003_Common_Area_Pnd_In_Data = pnd_Psgm003_Common_Area_Pnd_Psgm003_Input.newGroupInGroup("pnd_Psgm003_Common_Area_Pnd_In_Data", "#IN-DATA");
        pnd_Psgm003_Common_Area_Pnd_In_Contract_Num_Typ = pnd_Psgm003_Common_Area_Pnd_In_Data.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_In_Contract_Num_Typ", 
            "#IN-CONTRACT-NUM-TYP", FieldType.STRING, 1);
        pnd_Psgm003_Common_Area_Pnd_In_Type = pnd_Psgm003_Common_Area_Pnd_In_Data.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_In_Type", "#IN-TYPE", FieldType.STRING, 
            3);
        pnd_Psgm003_Common_Area_Pnd_In_Ssnpin = pnd_Psgm003_Common_Area_Pnd_In_Data.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_In_Ssnpin", "#IN-SSNPIN", 
            FieldType.STRING, 12);
        pnd_Psgm003_Common_Area_Pnd_In_Contract = pnd_Psgm003_Common_Area_Pnd_In_Data.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_In_Contract", "#IN-CONTRACT", 
            FieldType.STRING, 10);
        pnd_Psgm003_Common_Area_Pnd_In_Payee_Cd = pnd_Psgm003_Common_Area_Pnd_In_Data.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_In_Payee_Cd", "#IN-PAYEE-CD", 
            FieldType.STRING, 2);

        pnd_Psgm003_Common_Area_Pnd_Rt_Area = pnd_Psgm003_Common_Area.newGroupInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Area", "#RT-AREA");
        pnd_Psgm003_Common_Area_Pnd_Rt_Ret_Code = pnd_Psgm003_Common_Area_Pnd_Rt_Area.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Ret_Code", "#RT-RET-CODE", 
            FieldType.STRING, 2);

        pnd_Psgm003_Common_Area_Pnd_Rt_Record = pnd_Psgm003_Common_Area_Pnd_Rt_Area.newGroupInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Record", "#RT-RECORD");
        pnd_Psgm003_Common_Area_Pnd_Rt_Input_String = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Input_String", 
            "#RT-INPUT-STRING", FieldType.STRING, 100);
        pnd_Psgm003_Common_Area_Pnd_Rt_Ssn = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Ssn", "#RT-SSN", FieldType.STRING, 
            50);
        pnd_Psgm003_Common_Area_Pnd_Rt_Pin = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Pin", "#RT-PIN", FieldType.STRING, 
            50);

        pnd_Psgm003_Common_Area__R_Field_1 = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newGroupInGroup("pnd_Psgm003_Common_Area__R_Field_1", "REDEFINE", pnd_Psgm003_Common_Area_Pnd_Rt_Pin);
        pnd_Psgm003_Common_Area_Pnd_Rt_Pin_Num = pnd_Psgm003_Common_Area__R_Field_1.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Pin_Num", "#RT-PIN-NUM", 
            FieldType.NUMERIC, 12);
        pnd_Psgm003_Common_Area_Pnd_Rt_Tiaa_Cntrct_Nmbr = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Tiaa_Cntrct_Nmbr", 
            "#RT-TIAA-CNTRCT-NMBR", FieldType.STRING, 150);
        pnd_Psgm003_Common_Area_Pnd_Rt_Xpayee_Cde = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Xpayee_Cde", 
            "#RT-XPAYEE-CDE", FieldType.STRING, 2);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Status_Code = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Status_Code", 
            "#RT-CNTRCT-STATUS-CODE", FieldType.STRING, 120);
        pnd_Psgm003_Common_Area_Pnd_Rt_Ctrct_Status_Dt = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Ctrct_Status_Dt", 
            "#RT-CTRCT-STATUS-DT", FieldType.STRING, 8);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Issue_Dt = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Issue_Dt", 
            "#RT-CNTRCT-ISSUE-DT", FieldType.STRING, 8);
        pnd_Psgm003_Common_Area_Pnd_Rt_Lob = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Lob", "#RT-LOB", FieldType.STRING, 
            120);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cref_Contract_Nbr = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cref_Contract_Nbr", 
            "#RT-CREF-CONTRACT-NBR", FieldType.STRING, 150);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cref_Issued_Ind = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cref_Issued_Ind", 
            "#RT-CREF-ISSUED-IND", FieldType.STRING, 1);
        pnd_Psgm003_Common_Area_Pnd_Rt_Ownership_Code = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Ownership_Code", 
            "#RT-OWNERSHIP-CODE", FieldType.STRING, 120);
        pnd_Psgm003_Common_Area_Pnd_Rt_Product_Code = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Product_Code", 
            "#RT-PRODUCT-CODE", FieldType.STRING, 1);
        pnd_Psgm003_Common_Area_Pnd_Rt_Option_Code = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Option_Code", 
            "#RT-OPTION-CODE", FieldType.STRING, 2);
        pnd_Psgm003_Common_Area_Pnd_Rt_Plan_Code = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Plan_Code", "#RT-PLAN-CODE", 
            FieldType.STRING, 5);
        pnd_Psgm003_Common_Area_Pnd_Rt_Fund_Code = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Fund_Code", "#RT-FUND-CODE", 
            FieldType.STRING, 120);
        pnd_Psgm003_Common_Area_Pnd_Rt_Social_Code = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Social_Code", 
            "#RT-SOCIAL-CODE", FieldType.STRING, 120);
        pnd_Psgm003_Common_Area_Pnd_Rt_Custodial_Agreement = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Custodial_Agreement", 
            "#RT-CUSTODIAL-AGREEMENT", FieldType.STRING, 1);
        pnd_Psgm003_Common_Area_Pnd_Rt_Aas_Ind = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Aas_Ind", "#RT-AAS-IND", 
            FieldType.STRING, 1);
        pnd_Psgm003_Common_Area_Pnd_Rt_Platform_Ind = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Platform_Ind", 
            "#RT-PLATFORM-IND", FieldType.STRING, 120);
        pnd_Psgm003_Common_Area_Pnd_Rt_Multi_Plan_Ind = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Multi_Plan_Ind", 
            "#RT-MULTI-PLAN-IND", FieldType.STRING, 1);
        pnd_Psgm003_Common_Area_Pnd_Rt_Tiaa_Mailed_Ind = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Tiaa_Mailed_Ind", 
            "#RT-TIAA-MAILED-IND", FieldType.STRING, 1);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cref_Mailed_Ind = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cref_Mailed_Ind", 
            "#RT-CREF-MAILED-IND", FieldType.STRING, 1);
        pnd_Psgm003_Common_Area_Pnd_Rt_Dflt_Enrollment_Ind = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Dflt_Enrollment_Ind", 
            "#RT-DFLT-ENROLLMENT-IND", FieldType.STRING, 1);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Lst_Updt_Dt = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Lst_Updt_Dt", 
            "#RT-CNTRCT-LST-UPDT-DT", FieldType.STRING, 8);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Lst_Updt_Time = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Lst_Updt_Time", 
            "#RT-CNTRCT-LST-UPDT-TIME", FieldType.STRING, 7);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Lst_Updt_Usrid = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Lst_Updt_Usrid", 
            "#RT-CNTRCT-LST-UPDT-USRID", FieldType.STRING, 20);
        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Per_Addr_Ind = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Co_Per_Addr_Ind", 
            "#RT-CO-PER-ADDR-IND", FieldType.STRING, 1);
        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Type_Cd = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Type_Cd", 
            "#RT-CO-ADDR-TYPE-CD", FieldType.STRING, 1);
        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_One = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_One", 
            "#RT-CO-ADDR-LINE-ONE", FieldType.STRING, 100);
        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_Two = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_Two", 
            "#RT-CO-ADDR-LINE-TWO", FieldType.STRING, 100);
        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_Three = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_Three", 
            "#RT-CO-ADDR-LINE-THREE", FieldType.STRING, 100);
        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_Four = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_Four", 
            "#RT-CO-ADDR-LINE-FOUR", FieldType.STRING, 250);
        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_Five = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_Five", 
            "#RT-CO-ADDR-LINE-FIVE", FieldType.STRING, 250);
        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_City = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_City", 
            "#RT-CO-ADDR-CITY", FieldType.STRING, 35);
        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_State = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_State", 
            "#RT-CO-ADDR-STATE", FieldType.STRING, 2);
        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Zip = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Co_Zip", "#RT-CO-ZIP", 
            FieldType.STRING, 25);
        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Country = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Co_Country", 
            "#RT-CO-COUNTRY", FieldType.STRING, 120);
        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Postal_Data = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Postal_Data", 
            "#RT-CO-ADDR-POSTAL-DATA", FieldType.STRING, 32);
        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Geo_Cd = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Geo_Cd", 
            "#RT-CO-ADDR-GEO-CD", FieldType.STRING, 2);
        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Status = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Status", 
            "#RT-CO-ADDR-STATUS", FieldType.STRING, 1);
        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Status_Dt = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Status_Dt", 
            "#RT-CO-ADDR-STATUS-DT", FieldType.STRING, 8);
        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Lst_Ch_Src_Cd = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Lst_Ch_Src_Cd", 
            "#RT-CO-ADDR-LST-CH-SRC-CD", FieldType.STRING, 3);
        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Lst_Ch_Dt = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Lst_Ch_Dt", 
            "#RT-CO-ADDR-LST-CH-DT", FieldType.STRING, 8);
        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Lst_Ch_Time = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Lst_Ch_Time", 
            "#RT-CO-ADDR-LST-CH-TIME", FieldType.STRING, 7);
        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Lst_Ch_Usrid = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Lst_Ch_Usrid", 
            "#RT-CO-ADDR-LST-CH-USRID", FieldType.STRING, 8);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Per_Addr_Ind = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Per_Addr_Ind", 
            "#RT-CM-PER-ADDR-IND", FieldType.STRING, 1);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Type_Cd = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Type_Cd", 
            "#RT-CM-ADDR-TYPE-CD", FieldType.STRING, 1);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_One = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_One", 
            "#RT-CM-ADDR-LINE-ONE", FieldType.STRING, 100);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_Two = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_Two", 
            "#RT-CM-ADDR-LINE-TWO", FieldType.STRING, 100);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_Three = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_Three", 
            "#RT-CM-ADDR-LINE-THREE", FieldType.STRING, 100);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_Four = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_Four", 
            "#RT-CM-ADDR-LINE-FOUR", FieldType.STRING, 250);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_Five = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_Five", 
            "#RT-CM-ADDR-LINE-FIVE", FieldType.STRING, 250);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_City = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_City", 
            "#RT-CM-ADDR-CITY", FieldType.STRING, 35);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_State = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_State", 
            "#RT-CM-ADDR-STATE", FieldType.STRING, 2);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Zip = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Zip", "#RT-CM-ZIP", 
            FieldType.STRING, 25);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Country = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Country", 
            "#RT-CM-COUNTRY", FieldType.STRING, 120);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Ch_Saving_Cde = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Ch_Saving_Cde", 
            "#RT-CM-CH-SAVING-CDE", FieldType.STRING, 1);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Ph_Bnk_Pt_Acct_Nbr = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Ph_Bnk_Pt_Acct_Nbr", 
            "#RT-CM-PH-BNK-PT-ACCT-NBR", FieldType.STRING, 35);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Bnk_Aba_Eft_Nbr = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Bnk_Aba_Eft_Nbr", 
            "#RT-CM-BNK-ABA-EFT-NBR", FieldType.STRING, 35);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Eft_Pay_Type_Cde = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Eft_Pay_Type_Cde", 
            "#RT-CM-EFT-PAY-TYPE-CDE", FieldType.STRING, 2);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Postal_Data = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Postal_Data", 
            "#RT-CM-ADDR-POSTAL-DATA", FieldType.STRING, 32);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Geo_Cd = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Geo_Cd", 
            "#RT-CM-ADDR-GEO-CD", FieldType.STRING, 2);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Status = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Status", 
            "#RT-CM-ADDR-STATUS", FieldType.STRING, 1);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Status_Dt = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Status_Dt", 
            "#RT-CM-ADDR-STATUS-DT", FieldType.STRING, 8);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Lst_Ch_Src_Cd = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Lst_Ch_Src_Cd", 
            "#RT-CM-ADDR-LST-CH-SRC-CD", FieldType.STRING, 3);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Lst_Ch_Dt = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Lst_Ch_Dt", 
            "#RT-CM-ADDR-LST-CH-DT", FieldType.STRING, 8);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Lst_Ch_Time = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Lst_Ch_Time", 
            "#RT-CM-ADDR-LST-CH-TIME", FieldType.STRING, 7);
        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Lst_Ch_Usrid = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Lst_Ch_Usrid", 
            "#RT-CM-ADDR-LST-CH-USRID", FieldType.STRING, 8);
        pnd_Psgm003_Common_Area_Pnd_Rt_Prefix = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Prefix", "#RT-PREFIX", 
            FieldType.STRING, 120);
        pnd_Psgm003_Common_Area_Pnd_Rt_Last_Name = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Last_Name", "#RT-LAST-NAME", 
            FieldType.STRING, 35);
        pnd_Psgm003_Common_Area_Pnd_Rt_First_Name = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_First_Name", 
            "#RT-FIRST-NAME", FieldType.STRING, 30);
        pnd_Psgm003_Common_Area_Pnd_Rt_Given_Name_Two = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Given_Name_Two", 
            "#RT-GIVEN-NAME-TWO", FieldType.STRING, 30);
        pnd_Psgm003_Common_Area_Pnd_Rt_Suffix_Desc = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Suffix_Desc", 
            "#RT-SUFFIX-DESC", FieldType.STRING, 20);
        pnd_Psgm003_Common_Area_Pnd_Rt_Record_Status = pnd_Psgm003_Common_Area_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Common_Area_Pnd_Rt_Record_Status", 
            "#RT-RECORD-STATUS", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_bene_Contract.reset();

        localVariables.reset();
        pnd_Cref_Tab.getValue(1).setInitialValue("P");
        pnd_Cref_Tab.getValue(2).setInitialValue("Q");
        pnd_Cref_Tab.getValue(3).setInitialValue("U");
        pnd_Cref_Tab.getValue(4).setInitialValue("V");
        pnd_Cref_Tab.getValue(5).setInitialValue("X");
        pnd_Cref_Tab.getValue(6).setInitialValue("1");
        pnd_Cref_Tab.getValue(7).setInitialValue("J");
        pnd_Cref_Tab.getValue(8).setInitialValue("M");
        pnd_Cref_Tab.getValue(9).setInitialValue("4");
        pnd_Cref_Tab.getValue(10).setInitialValue("T");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Benn9878() throws Exception
    {
        super("Benn9878");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ERROR-INFO-SUB.##LAST-PROGRAM := *PROGRAM
        pdaBena9878.getBena9878_Pnd_Output_Fields().reset();                                                                                                              //Natural: RESET BENA9878.#OUTPUT-FIELDS
        pdaBena9878.getBena9878_Pnd_Error_Fields().reset();                                                                                                               //Natural: RESET BENA9878.#ERROR-FIELDS
        PROGRAM:                                                                                                                                                          //Natural: REPEAT
        while (condition(whileTrue))
        {
                                                                                                                                                                          //Natural: PERFORM FIND-COPYFROM-CONTRACT
            sub_Find_Copyfrom_Contract();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PROGRAM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PROGRAM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ESCAPE-PROGRAM
            if (true) break PROGRAM;                                                                                                                                      //Natural: ESCAPE BOTTOM ( PROGRAM. )
            //*  MAIN PROGRAM
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-COPYFROM-CONTRACT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-MDM
        //* *    #IN-CONTRACT-NUM-TYP := 'C'
        //* *#IN-CONTRACT-NUM-TYP := 'C'
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-ERROR
        //* ***********************************************************************
    }
    private void sub_Escape_Program() throws Exception                                                                                                                    //Natural: ESCAPE-PROGRAM
    {
        if (BLNatReinput.isReinput()) return;

        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( PROGRAM. )
        Global.setEscapeCode(EscapeType.Bottom, "PROGRAM");
        if (true) return;
        //*  ESCAPE-PROGRAM
    }
    private void sub_Find_Copyfrom_Contract() throws Exception                                                                                                            //Natural: FIND-COPYFROM-CONTRACT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  1ST CHECK IF CONTRACT NUMBERS ARE INTERNALLY CONSISTENT, AND FORMAT,
        //*  THEN CHECK IF CONTRACT NUMBER CAN BE FOUND IN MDM.
        //*  TWO TIAAS
        short decideConditionsMet293 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ( NOT #CNTRCT-IS-CREF ( 1 ) AND NOT #CNTRCT-IS-CREF ( 2 ) ) AND #SAME-AS-CNTRCT ( 2 ) > ' '
        if (condition(! ((pdaBena9878.getBena9878_Pnd_Cntrct_Is_Cref().getValue(1).equals(true)) && ! (pdaBena9878.getBena9878_Pnd_Cntrct_Is_Cref().getValue(2).equals(true)) 
            && pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(2).greater(" "))))
        {
            decideConditionsMet293++;
            pdaBena9878.getBena9878_Pnd_Error_Msg().setValue(DbsUtil.compress("Copy From Contract numbers are both TIAA:", pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(1),  //Natural: COMPRESS 'Copy From Contract numbers are both TIAA:' #SAME-AS-CNTRCT ( 1 ) #SAME-AS-CNTRCT ( 2 ) INTO BENA9878.#ERROR-MSG
                pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(2)));
            pdaBena9878.getBena9878_Pnd_Error_Cde().setValue("NSAME");                                                                                                    //Natural: ASSIGN BENA9878.#ERROR-CDE := 'NSAME'
            //*  TWO CREFS
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN #CNTRCT-IS-CREF ( 1 ) AND #CNTRCT-IS-CREF ( 2 )
        else if (condition(pdaBena9878.getBena9878_Pnd_Cntrct_Is_Cref().getValue(1).equals(true) && pdaBena9878.getBena9878_Pnd_Cntrct_Is_Cref().getValue(2).equals(true)))
        {
            decideConditionsMet293++;
            pdaBena9878.getBena9878_Pnd_Error_Msg().setValue(DbsUtil.compress("Copy From Contract numbers are both CREF:", pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(1),  //Natural: COMPRESS 'Copy From Contract numbers are both CREF:' #SAME-AS-CNTRCT ( 1 ) #SAME-AS-CNTRCT ( 2 ) INTO BENA9878.#ERROR-MSG
                pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(2)));
            pdaBena9878.getBena9878_Pnd_Error_Cde().setValue("NSAME");                                                                                                    //Natural: ASSIGN BENA9878.#ERROR-CDE := 'NSAME'
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN #SAME-AS-CNTRCT ( 2 ) > ' '
        else if (condition(pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(2).greater(" ")))
        {
            decideConditionsMet293++;
            if (condition(pdaBena9878.getBena9878_Pnd_Cntrct_Is_Cref().getValue(1).getBoolean()))                                                                         //Natural: IF #CNTRCT-IS-CREF ( 1 )
            {
                pnd_Same_Hold.setValue(pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(2));                                                                         //Natural: ASSIGN #SAME-HOLD := #SAME-AS-CNTRCT ( 2 )
                pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(2).setValue(pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(1));                              //Natural: ASSIGN #SAME-AS-CNTRCT ( 2 ) := #SAME-AS-CNTRCT ( 1 )
                pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(1).setValue(pnd_Same_Hold);                                                                         //Natural: ASSIGN #SAME-AS-CNTRCT ( 1 ) := #SAME-HOLD
            }                                                                                                                                                             //Natural: END-IF
            //*  MUST BE NO TIAA
                                                                                                                                                                          //Natural: PERFORM CHECK-MDM
            sub_Check_Mdm();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN #CNTRCT-IS-CREF ( 1 )
        else if (condition(pdaBena9878.getBena9878_Pnd_Cntrct_Is_Cref().getValue(1).equals(true)))
        {
            decideConditionsMet293++;
            pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(2).setValue(pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(1));                                  //Natural: ASSIGN #SAME-AS-CNTRCT ( 2 ) := #SAME-AS-CNTRCT ( 1 )
            pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(1).reset();                                                                                             //Natural: RESET #SAME-AS-CNTRCT ( 1 )
                                                                                                                                                                          //Natural: PERFORM CHECK-MDM
            sub_Check_Mdm();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM CHECK-MDM
            sub_Check_Mdm();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  IF NO ERROR, MDM OK, CAN PROCEED.
        //*  NOW TIAA IS IN (1) AND CREF IN (2)
        //*  THE BELOW PROCESSING IS BASED ON THE IDEA THAT THA SAME AS XXXXXXXX
        //*  TO BE FOUND ON THE MAINTENANCE FILE MUST BE OF THE SAME TYPE AS THE
        //*  DESIGNATION EXISTS FOR. A T/C IND ON MAINTENANCE FILE OF 'T' (FOR THE
        //*  TIAA # ONLY) CANNOT SUPPLY A DESIGNATION FOR A CREF NUMBER, AND
        //*  VICE VERSA. IF THE MAINTENANCE FILE DOESN't have a TIAA designation &
        //*  WHAT IS CALLED FOR IS 'Same As a TIAA Contract'S DESIGNATION', then
        //*  THE MAINTENANCE FILE CANNOT SUPPLY IT, EVEN IF IT HAS A DES. FOR THE
        //*  CREF. (NOTE T/C IND OF ' ' MEANS FOR BOTH T AND C - IT WILL NOT
        //*  CO-EXIST WITH A T OR A C).
        short decideConditionsMet332 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #SAME-AS-CNTRCT ( 1 ) > ' ' AND #SAME-AS-CNTRCT ( 2 ) = ' '
        if (condition(pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(1).greater(" ") && pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(2).equals(" ")))
        {
            decideConditionsMet332++;
            vw_bene_Contract.startDatabaseFind                                                                                                                            //Natural: FIND BENE-CONTRACT WITH BC-TIAA-CNTRCT = #SAME-AS-CNTRCT ( 1 )
            (
            "F1",
            new Wc[] { new Wc("BC_TIAA_CNTRCT", "=", pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(1), WcType.WITH) }
            );
            F1:
            while (condition(vw_bene_Contract.readNextRow("F1", true)))
            {
                vw_bene_Contract.setIfNotFoundControlFlag(false);
                if (condition(vw_bene_Contract.getAstCOUNTER().equals(0)))                                                                                                //Natural: IF NO RECORDS FOUND
                {
                    if (true) break F1;                                                                                                                                   //Natural: ESCAPE BOTTOM ( F1. )
                }                                                                                                                                                         //Natural: END-NOREC
                if (condition(bene_Contract_Bc_Stat.equals("C")))                                                                                                         //Natural: IF BC-STAT = 'C'
                {
                    if (condition(bene_Contract_Bc_Tiaa_Cref_Ind.equals(" ") || bene_Contract_Bc_Tiaa_Cref_Ind.equals("T")))                                              //Natural: IF ( BC-TIAA-CREF-IND = ' ' OR = 'T' )
                    {
                        pdaBena9878.getBena9878_Pnd_Copy_From_Cntrct().setValue(pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(1));                                //Natural: ASSIGN #COPY-FROM-CNTRCT := #SAME-AS-CNTRCT ( 1 )
                        pdaBena9878.getBena9878_Pnd_Copy_From_Cntrct_Isn().setValue(vw_bene_Contract.getAstISN("F1"));                                                    //Natural: ASSIGN #COPY-FROM-CNTRCT-ISN := *ISN ( F1. )
                        pdaBena9878.getBena9878_Pnd_Copy_From_Cntrct_Tiaa_Cref_Ind().setValue(bene_Contract_Bc_Tiaa_Cref_Ind);                                            //Natural: ASSIGN #COPY-FROM-CNTRCT-TIAA-CREF-IND := BC-TIAA-CREF-IND
                        pdaBena9878.getBena9878_Pnd_Copy_From_Cntrct_Eff_Dte().setValue(bene_Contract_Bc_Eff_Dte);                                                        //Natural: ASSIGN #COPY-FROM-CNTRCT-EFF-DTE := BC-EFF-DTE
                        if (true) break F1;                                                                                                                               //Natural: ESCAPE BOTTOM ( F1. )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pdaBena9878.getBena9878_Pnd_Error_Msg().setValue(DbsUtil.compress("Copy from:", pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(1),         //Natural: COMPRESS 'Copy from:' #SAME-AS-CNTRCT ( 1 ) 'is on BENE with' 'T/C ind ' BC-TIAA-CREF-IND INTO BENA9878.#ERROR-MSG
                            "is on BENE with", "T/C ind ", bene_Contract_Bc_Tiaa_Cref_Ind));
                        pdaBena9878.getBena9878_Pnd_Error_Cde().setValue("SAME");                                                                                         //Natural: ASSIGN BENA9878.#ERROR-CDE := 'SAME'
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                        sub_Process_Error();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("F1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(bene_Contract_Bc_Stat.equals("R") || bene_Contract_Bc_Stat.equals("P")))                                                                //Natural: IF BC-STAT = 'R' OR = 'P'
                    {
                        pdaBena9878.getBena9878_Pnd_Error_Msg().setValue(DbsUtil.compress("Copy from:", pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(1),         //Natural: COMPRESS 'Copy from:' #SAME-AS-CNTRCT ( 1 ) 'in Pending Status' INTO BENA9878.#ERROR-MSG
                            "in Pending Status"));
                        pdaBena9878.getBena9878_Pnd_Error_Cde().setValue("SAME");                                                                                         //Natural: ASSIGN BENA9878.#ERROR-CDE := 'SAME'
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                        sub_Process_Error();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("F1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #SAME-AS-CNTRCT ( 1 ) > ' ' AND #SAME-AS-CNTRCT ( 2 ) > ' '
        else if (condition(pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(1).greater(" ") && pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(2).greater(" ")))
        {
            decideConditionsMet332++;
            pnd_Tiaa_On_Maint.reset();                                                                                                                                    //Natural: RESET #TIAA-ON-MAINT #CREF-ON-MAINT
            pnd_Cref_On_Maint.reset();
            vw_bene_Contract.startDatabaseFind                                                                                                                            //Natural: FIND BENE-CONTRACT WITH BC-TIAA-CNTRCT = #SAME-AS-CNTRCT ( 1 )
            (
            "F2",
            new Wc[] { new Wc("BC_TIAA_CNTRCT", "=", pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(1), WcType.WITH) }
            );
            F2:
            while (condition(vw_bene_Contract.readNextRow("F2", true)))
            {
                vw_bene_Contract.setIfNotFoundControlFlag(false);
                if (condition(vw_bene_Contract.getAstCOUNTER().equals(0)))                                                                                                //Natural: IF NO RECORDS FOUND
                {
                    if (true) break F2;                                                                                                                                   //Natural: ESCAPE BOTTOM ( F2. )
                }                                                                                                                                                         //Natural: END-NOREC
                if (condition(bene_Contract_Bc_Stat.equals("C")))                                                                                                         //Natural: IF BC-STAT = 'C'
                {
                    if (condition(bene_Contract_Bc_Tiaa_Cref_Ind.equals(" ")))                                                                                            //Natural: IF BC-TIAA-CREF-IND = ' '
                    {
                        pdaBena9878.getBena9878_Pnd_Copy_From_Cntrct().setValue(pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(1));                                //Natural: ASSIGN #COPY-FROM-CNTRCT := #SAME-AS-CNTRCT ( 1 )
                        pdaBena9878.getBena9878_Pnd_Copy_From_Cntrct_Isn().setValue(vw_bene_Contract.getAstISN("F2"));                                                    //Natural: ASSIGN #COPY-FROM-CNTRCT-ISN := *ISN ( F2. )
                        pdaBena9878.getBena9878_Pnd_Copy_From_Cntrct_Tiaa_Cref_Ind().setValue(bene_Contract_Bc_Tiaa_Cref_Ind);                                            //Natural: ASSIGN #COPY-FROM-CNTRCT-TIAA-CREF-IND := BC-TIAA-CREF-IND
                        pdaBena9878.getBena9878_Pnd_Copy_From_Cntrct_Eff_Dte().setValue(bene_Contract_Bc_Eff_Dte);                                                        //Natural: ASSIGN #COPY-FROM-CNTRCT-EFF-DTE := BC-EFF-DTE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(bene_Contract_Bc_Tiaa_Cref_Ind.equals("C")))                                                                                        //Natural: IF BC-TIAA-CREF-IND = 'C'
                        {
                            pnd_Cref_On_Maint.setValue(true);                                                                                                             //Natural: ASSIGN #CREF-ON-MAINT := TRUE
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Tiaa_On_Maint.setValue(true);                                                                                                             //Natural: ASSIGN #TIAA-ON-MAINT := TRUE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(bene_Contract_Bc_Stat.equals("R") || bene_Contract_Bc_Stat.equals("P")))                                                                //Natural: IF BC-STAT = 'R' OR = 'P'
                    {
                        pdaBena9878.getBena9878_Pnd_Error_Msg().setValue(DbsUtil.compress("Copy from:", pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(1),         //Natural: COMPRESS 'Copy from:' #SAME-AS-CNTRCT ( 1 ) 'in Pending Status' INTO BENA9878.#ERROR-MSG
                            "in Pending Status"));
                        pdaBena9878.getBena9878_Pnd_Error_Cde().setValue("SAME");                                                                                         //Natural: ASSIGN BENA9878.#ERROR-CDE := 'SAME'
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                        sub_Process_Error();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("F2"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("F2"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
            if (condition(pnd_Tiaa_On_Maint.getBoolean() && pnd_Cref_On_Maint.getBoolean()))                                                                              //Natural: IF #TIAA-ON-MAINT AND #CREF-ON-MAINT
            {
                pdaBena9878.getBena9878_Pnd_Error_Msg().setValue(DbsUtil.compress("Copy From Contracts on Maint for both TIAA/CREF:", pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(1),  //Natural: COMPRESS 'Copy From Contracts on Maint for both TIAA/CREF:' #SAME-AS-CNTRCT ( 1 ) #SAME-AS-CNTRCT ( 2 ) TO BENA9878.#ERROR-MSG
                    pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(2)));
                pdaBena9878.getBena9878_Pnd_Error_Cde().setValue("NSAME");                                                                                                //Natural: ASSIGN BENA9878.#ERROR-CDE := 'NSAME'
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Tiaa_On_Maint.getBoolean() || pnd_Cref_On_Maint.getBoolean()))                                                                              //Natural: IF #TIAA-ON-MAINT OR #CREF-ON-MAINT
            {
                pdaBena9878.getBena9878_Pnd_Error_Msg().setValue(DbsUtil.compress("Designation is split on BENI and Same As is for both."));                              //Natural: COMPRESS 'Designation is split on BENI and Same As is for both.' INTO BENA9878.#ERROR-MSG
                pdaBena9878.getBena9878_Pnd_Error_Cde().setValue("NSAME");                                                                                                //Natural: ASSIGN BENA9878.#ERROR-CDE := 'NSAME'
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape())) {return;}
                //*  MUST BE CREF ONLY IN
                //*  SAME AS PHRASE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #SAME-AS-CNTRCT ( 1 ) = ' ' AND #SAME-AS-CNTRCT ( 2 ) > ' '
        else if (condition(pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(1).equals(" ") && pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(2).greater(" ")))
        {
            decideConditionsMet332++;
            vw_bene_Contract.startDatabaseFind                                                                                                                            //Natural: FIND BENE-CONTRACT WITH BC-CREF-CNTRCT = #SAME-AS-CNTRCT ( 2 )
            (
            "F3",
            new Wc[] { new Wc("BC_CREF_CNTRCT", "=", pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(2), WcType.WITH) }
            );
            F3:
            while (condition(vw_bene_Contract.readNextRow("F3", true)))
            {
                vw_bene_Contract.setIfNotFoundControlFlag(false);
                if (condition(vw_bene_Contract.getAstCOUNTER().equals(0)))                                                                                                //Natural: IF NO RECORDS FOUND
                {
                    if (true) break F3;                                                                                                                                   //Natural: ESCAPE BOTTOM ( F3. )
                }                                                                                                                                                         //Natural: END-NOREC
                short decideConditionsMet407 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF BC-STAT;//Natural: VALUE 'C'
                if (condition((bene_Contract_Bc_Stat.equals("C"))))
                {
                    decideConditionsMet407++;
                    if (condition(bene_Contract_Bc_Tiaa_Cref_Ind.equals(" ") || bene_Contract_Bc_Tiaa_Cref_Ind.equals("C")))                                              //Natural: IF BC-TIAA-CREF-IND = ' ' OR = 'C'
                    {
                        pdaBena9878.getBena9878_Pnd_Copy_From_Cntrct().setValue(pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(2));                                //Natural: ASSIGN #COPY-FROM-CNTRCT := #SAME-AS-CNTRCT ( 2 )
                        pdaBena9878.getBena9878_Pnd_Copy_From_Cntrct_Isn().setValue(vw_bene_Contract.getAstISN("F3"));                                                    //Natural: ASSIGN #COPY-FROM-CNTRCT-ISN := *ISN ( F3. )
                        pdaBena9878.getBena9878_Pnd_Copy_From_Cntrct_Tiaa_Cref_Ind().setValue(bene_Contract_Bc_Tiaa_Cref_Ind);                                            //Natural: ASSIGN #COPY-FROM-CNTRCT-TIAA-CREF-IND := BC-TIAA-CREF-IND
                        pdaBena9878.getBena9878_Pnd_Copy_From_Cntrct_Eff_Dte().setValue(bene_Contract_Bc_Eff_Dte);                                                        //Natural: ASSIGN #COPY-FROM-CNTRCT-EFF-DTE := BC-EFF-DTE
                        if (true) break F3;                                                                                                                               //Natural: ESCAPE BOTTOM ( F3. )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pdaBena9878.getBena9878_Pnd_Error_Msg().setValue(DbsUtil.compress("Copy from:", pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(2),         //Natural: COMPRESS 'Copy from:' #SAME-AS-CNTRCT ( 2 ) 'is on BENE with' 'T/C ind' BC-TIAA-CREF-IND INTO BENA9878.#ERROR-MSG
                            "is on BENE with", "T/C ind", bene_Contract_Bc_Tiaa_Cref_Ind));
                        pdaBena9878.getBena9878_Pnd_Error_Cde().setValue("SAME");                                                                                         //Natural: ASSIGN BENA9878.#ERROR-CDE := 'SAME'
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                        sub_Process_Error();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("F3"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("F3"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: VALUE 'R','P'
                else if (condition((bene_Contract_Bc_Stat.equals("R") || bene_Contract_Bc_Stat.equals("P"))))
                {
                    decideConditionsMet407++;
                    pdaBena9878.getBena9878_Pnd_Error_Msg().setValue(DbsUtil.compress("Copy from:", pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(2),             //Natural: COMPRESS 'Copy from:' #SAME-AS-CNTRCT ( 2 ) 'in Pending Status' INTO BENA9878.#ERROR-MSG
                        "in Pending Status"));
                    pdaBena9878.getBena9878_Pnd_Error_Cde().setValue("SAME");                                                                                             //Natural: ASSIGN BENA9878.#ERROR-CDE := 'SAME'
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                    sub_Process_Error();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("F3"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("F3"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pdaBena9878.getBena9878_Pnd_Copy_From_Cntrct().greater(" ")))                                                                                       //Natural: IF #COPY-FROM-CNTRCT > ' '
        {
            pdaBena9878.getBena9878_Pnd_Copy_From_Cntrct_Found().setValue(true);                                                                                          //Natural: ASSIGN #COPY-FROM-CNTRCT-FOUND := TRUE
                                                                                                                                                                          //Natural: PERFORM ESCAPE-PROGRAM
            sub_Escape_Program();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  FIND-COPYFROM-CONTRACT
    }
    private void sub_Check_Mdm() throws Exception                                                                                                                         //Natural: CHECK-MDM
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pdaBena9879.getBena9879().reset();                                                                                                                                //Natural: RESET BENA9879
        if (condition(pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(1).greater(" ")))                                                                             //Natural: IF #SAME-AS-CNTRCT ( 1 ) > ' '
        {
            //*  READ FOR ONE-UP CALL
            //*  TIAA
            //*  PAYEE CODE
            pnd_Psgm003_Common_Area.reset();                                                                                                                              //Natural: RESET #PSGM003-COMMON-AREA
            pnd_Psgm003_Common_Area_Pnd_In_Function.setValue("PR");                                                                                                       //Natural: ASSIGN #IN-FUNCTION := 'PR'
            pnd_Psgm003_Common_Area_Pnd_In_Contract_Num_Typ.setValue("T");                                                                                                //Natural: ASSIGN #IN-CONTRACT-NUM-TYP := 'T'
            pnd_Psgm003_Common_Area_Pnd_In_Type.setValue(" ");                                                                                                            //Natural: ASSIGN #IN-TYPE := ' '
            pnd_Psgm003_Common_Area_Pnd_In_Ssnpin.setValue(" ");                                                                                                          //Natural: ASSIGN #IN-SSNPIN := ' '
            pnd_Psgm003_Common_Area_Pnd_In_Contract.setValue(pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(1));                                                   //Natural: ASSIGN #IN-CONTRACT := #SAME-AS-CNTRCT ( 1 )
            pnd_Psgm003_Common_Area_Pnd_In_Payee_Cd.setValue(" ");                                                                                                        //Natural: ASSIGN #IN-PAYEE-CD := ' '
            psgm003ReturnCode = DbsUtil.callExternalProgram("PSGM003",pnd_Psgm003_Common_Area_Pnd_In_Function,pnd_Psgm003_Common_Area_Pnd_In_Contract_Num_Typ,            //Natural: CALL 'PSGM003' USING #PSGM003-COMMON-AREA
                pnd_Psgm003_Common_Area_Pnd_In_Type,pnd_Psgm003_Common_Area_Pnd_In_Ssnpin,pnd_Psgm003_Common_Area_Pnd_In_Contract,pnd_Psgm003_Common_Area_Pnd_In_Payee_Cd,
                pnd_Psgm003_Common_Area_Pnd_Rt_Ret_Code,pnd_Psgm003_Common_Area_Pnd_Rt_Input_String,pnd_Psgm003_Common_Area_Pnd_Rt_Ssn,pnd_Psgm003_Common_Area_Pnd_Rt_Pin,
                pnd_Psgm003_Common_Area_Pnd_Rt_Tiaa_Cntrct_Nmbr,pnd_Psgm003_Common_Area_Pnd_Rt_Xpayee_Cde,pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Status_Code,
                pnd_Psgm003_Common_Area_Pnd_Rt_Ctrct_Status_Dt,pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Issue_Dt,pnd_Psgm003_Common_Area_Pnd_Rt_Lob,pnd_Psgm003_Common_Area_Pnd_Rt_Cref_Contract_Nbr,
                pnd_Psgm003_Common_Area_Pnd_Rt_Cref_Issued_Ind,pnd_Psgm003_Common_Area_Pnd_Rt_Ownership_Code,pnd_Psgm003_Common_Area_Pnd_Rt_Product_Code,
                pnd_Psgm003_Common_Area_Pnd_Rt_Option_Code,pnd_Psgm003_Common_Area_Pnd_Rt_Plan_Code,pnd_Psgm003_Common_Area_Pnd_Rt_Fund_Code,pnd_Psgm003_Common_Area_Pnd_Rt_Social_Code,
                pnd_Psgm003_Common_Area_Pnd_Rt_Custodial_Agreement,pnd_Psgm003_Common_Area_Pnd_Rt_Aas_Ind,pnd_Psgm003_Common_Area_Pnd_Rt_Platform_Ind,pnd_Psgm003_Common_Area_Pnd_Rt_Multi_Plan_Ind,
                pnd_Psgm003_Common_Area_Pnd_Rt_Tiaa_Mailed_Ind,pnd_Psgm003_Common_Area_Pnd_Rt_Cref_Mailed_Ind,pnd_Psgm003_Common_Area_Pnd_Rt_Dflt_Enrollment_Ind,
                pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Lst_Updt_Dt,pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Lst_Updt_Time,pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Lst_Updt_Usrid,
                pnd_Psgm003_Common_Area_Pnd_Rt_Co_Per_Addr_Ind,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Type_Cd,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_One,
                pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_Two,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_Three,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_Four,
                pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_Five,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_City,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_State,
                pnd_Psgm003_Common_Area_Pnd_Rt_Co_Zip,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Country,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Postal_Data,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Geo_Cd,
                pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Status,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Status_Dt,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Lst_Ch_Src_Cd,
                pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Lst_Ch_Dt,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Lst_Ch_Time,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Lst_Ch_Usrid,
                pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Per_Addr_Ind,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Type_Cd,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_One,
                pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_Two,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_Three,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_Four,
                pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_Five,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_City,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_State,
                pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Zip,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Country,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Ch_Saving_Cde,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Ph_Bnk_Pt_Acct_Nbr,
                pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Bnk_Aba_Eft_Nbr,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Eft_Pay_Type_Cde,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Postal_Data,
                pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Geo_Cd,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Status,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Status_Dt,
                pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Lst_Ch_Src_Cd,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Lst_Ch_Dt,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Lst_Ch_Time,
                pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Lst_Ch_Usrid,pnd_Psgm003_Common_Area_Pnd_Rt_Prefix,pnd_Psgm003_Common_Area_Pnd_Rt_Last_Name,pnd_Psgm003_Common_Area_Pnd_Rt_First_Name,
                pnd_Psgm003_Common_Area_Pnd_Rt_Given_Name_Two,pnd_Psgm003_Common_Area_Pnd_Rt_Suffix_Desc,pnd_Psgm003_Common_Area_Pnd_Rt_Record_Status);
            //*  SUCCESS
            if (condition(pnd_Psgm003_Common_Area_Pnd_Rt_Record_Status.equals("Y")))                                                                                      //Natural: IF #RT-RECORD-STATUS = 'Y'
            {
                if (condition(! (pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Status_Code.equals(" ") || pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Status_Code.equals("Y")           //Natural: IF NOT ( #RT-CNTRCT-STATUS-CODE = ' ' OR = 'Y' OR = 'H' )
                    || pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Status_Code.equals("H"))))
                {
                    pdaBena9878.getBena9878_Pnd_Error_Msg().setValue(DbsUtil.compress("Copy From Contract:", pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(1),    //Natural: COMPRESS 'Copy From Contract:' #SAME-AS-CNTRCT ( 1 ) 'is terminated on COR.' INTO BENA9878.#ERROR-MSG
                        "is terminated on COR."));
                    pdaBena9878.getBena9878_Pnd_Error_Cde().setValue("NSAME");                                                                                            //Natural: ASSIGN BENA9878.#ERROR-CDE := 'NSAME'
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                    sub_Process_Error();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(2).greater(" ") && pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(2).notEquals(pnd_Psgm003_Common_Area_Pnd_Rt_Cref_Contract_Nbr))) //Natural: IF #SAME-AS-CNTRCT ( 2 ) > ' ' AND #SAME-AS-CNTRCT ( 2 ) NE #RT-CREF-CONTRACT-NBR
                {
                    //*  THIS IS TRICKY. THE 'CREF number' IN THE SAME AS PHRASE MAY BE MISS
                    //*  TYPED, OR DELIBERATELY ABBREVIATED - IF SO WE CAN STILL PROCESS VIA
                    //*  THE TIAA #. HOWEVER WE MUST ASSUME THE INTENTION IS 'Same as BOTH'
                    //*  THEREFORE DO NOT RESET THE 'CREF number' SO ONLY A 'both' DESIGNATION
                    //*  ON THE MAINT FILE WILL SATISFY AND ALLOW PROCESSING. (SEE ABOVE CODE).
                    //*  READ FOR ONE-UP CALL
                    //*  CREF
                    //*  PAYEE CODE
                    pnd_Psgm003_Common_Area.reset();                                                                                                                      //Natural: RESET #PSGM003-COMMON-AREA
                    pnd_Psgm003_Common_Area_Pnd_In_Function.setValue("PR");                                                                                               //Natural: ASSIGN #IN-FUNCTION := 'PR'
                    pnd_Psgm003_Common_Area_Pnd_In_Contract_Num_Typ.setValue("T");                                                                                        //Natural: ASSIGN #IN-CONTRACT-NUM-TYP := 'T'
                    pnd_Psgm003_Common_Area_Pnd_In_Type.setValue(" ");                                                                                                    //Natural: ASSIGN #IN-TYPE := ' '
                    pnd_Psgm003_Common_Area_Pnd_In_Ssnpin.setValue(" ");                                                                                                  //Natural: ASSIGN #IN-SSNPIN := ' '
                    pnd_Psgm003_Common_Area_Pnd_In_Contract.setValue(pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(2));                                           //Natural: ASSIGN #IN-CONTRACT := #SAME-AS-CNTRCT ( 2 )
                    pnd_Psgm003_Common_Area_Pnd_In_Payee_Cd.setValue(" ");                                                                                                //Natural: ASSIGN #IN-PAYEE-CD := ' '
                    psgm003ReturnCode = DbsUtil.callExternalProgram("PSGM003",pnd_Psgm003_Common_Area_Pnd_In_Function,pnd_Psgm003_Common_Area_Pnd_In_Contract_Num_Typ,    //Natural: CALL 'PSGM003' USING #PSGM003-COMMON-AREA
                        pnd_Psgm003_Common_Area_Pnd_In_Type,pnd_Psgm003_Common_Area_Pnd_In_Ssnpin,pnd_Psgm003_Common_Area_Pnd_In_Contract,pnd_Psgm003_Common_Area_Pnd_In_Payee_Cd,
                        pnd_Psgm003_Common_Area_Pnd_Rt_Ret_Code,pnd_Psgm003_Common_Area_Pnd_Rt_Input_String,pnd_Psgm003_Common_Area_Pnd_Rt_Ssn,pnd_Psgm003_Common_Area_Pnd_Rt_Pin,
                        pnd_Psgm003_Common_Area_Pnd_Rt_Tiaa_Cntrct_Nmbr,pnd_Psgm003_Common_Area_Pnd_Rt_Xpayee_Cde,pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Status_Code,
                        pnd_Psgm003_Common_Area_Pnd_Rt_Ctrct_Status_Dt,pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Issue_Dt,pnd_Psgm003_Common_Area_Pnd_Rt_Lob,
                        pnd_Psgm003_Common_Area_Pnd_Rt_Cref_Contract_Nbr,pnd_Psgm003_Common_Area_Pnd_Rt_Cref_Issued_Ind,pnd_Psgm003_Common_Area_Pnd_Rt_Ownership_Code,
                        pnd_Psgm003_Common_Area_Pnd_Rt_Product_Code,pnd_Psgm003_Common_Area_Pnd_Rt_Option_Code,pnd_Psgm003_Common_Area_Pnd_Rt_Plan_Code,
                        pnd_Psgm003_Common_Area_Pnd_Rt_Fund_Code,pnd_Psgm003_Common_Area_Pnd_Rt_Social_Code,pnd_Psgm003_Common_Area_Pnd_Rt_Custodial_Agreement,
                        pnd_Psgm003_Common_Area_Pnd_Rt_Aas_Ind,pnd_Psgm003_Common_Area_Pnd_Rt_Platform_Ind,pnd_Psgm003_Common_Area_Pnd_Rt_Multi_Plan_Ind,
                        pnd_Psgm003_Common_Area_Pnd_Rt_Tiaa_Mailed_Ind,pnd_Psgm003_Common_Area_Pnd_Rt_Cref_Mailed_Ind,pnd_Psgm003_Common_Area_Pnd_Rt_Dflt_Enrollment_Ind,
                        pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Lst_Updt_Dt,pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Lst_Updt_Time,pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Lst_Updt_Usrid,
                        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Per_Addr_Ind,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Type_Cd,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_One,
                        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_Two,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_Three,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_Four,
                        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_Five,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_City,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_State,
                        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Zip,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Country,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Postal_Data,
                        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Geo_Cd,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Status,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Status_Dt,
                        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Lst_Ch_Src_Cd,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Lst_Ch_Dt,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Lst_Ch_Time,
                        pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Lst_Ch_Usrid,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Per_Addr_Ind,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Type_Cd,
                        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_One,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_Two,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_Three,
                        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_Four,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_Five,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_City,
                        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_State,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Zip,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Country,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Ch_Saving_Cde,
                        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Ph_Bnk_Pt_Acct_Nbr,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Bnk_Aba_Eft_Nbr,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Eft_Pay_Type_Cde,
                        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Postal_Data,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Geo_Cd,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Status,
                        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Status_Dt,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Lst_Ch_Src_Cd,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Lst_Ch_Dt,
                        pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Lst_Ch_Time,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Lst_Ch_Usrid,pnd_Psgm003_Common_Area_Pnd_Rt_Prefix,
                        pnd_Psgm003_Common_Area_Pnd_Rt_Last_Name,pnd_Psgm003_Common_Area_Pnd_Rt_First_Name,pnd_Psgm003_Common_Area_Pnd_Rt_Given_Name_Two,
                        pnd_Psgm003_Common_Area_Pnd_Rt_Suffix_Desc,pnd_Psgm003_Common_Area_Pnd_Rt_Record_Status);
                    //*  SUCCESS
                    if (condition(pnd_Psgm003_Common_Area_Pnd_Rt_Record_Status.equals("Y")))                                                                              //Natural: IF #RT-RECORD-STATUS = 'Y'
                    {
                        pdaBena9878.getBena9878_Pnd_Error_Msg().setValue(DbsUtil.compress("Copy From CREF# doesn't match COR:", pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(2))); //Natural: COMPRESS 'Copy From CREF# doesn"t match COR:' #SAME-AS-CNTRCT ( 2 ) TO BENA9878.#ERROR-MSG
                        pdaBena9878.getBena9878_Pnd_Error_Cde().setValue("NSAME");                                                                                        //Natural: ASSIGN BENA9878.#ERROR-CDE := 'NSAME'
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                        sub_Process_Error();
                        if (condition(Global.isEscape())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaBena9878.getBena9878_Pnd_Error_Msg().setValue(DbsUtil.compress("Copy From Contract number not on COR:", pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(1),  //Natural: COMPRESS 'Copy From Contract number not on COR:' #SAME-AS-CNTRCT ( 1 ) #SAME-AS-CNTRCT ( 2 ) INTO BENA9878.#ERROR-MSG
                    pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(2)));
                pdaBena9878.getBena9878_Pnd_Error_Cde().setValue("SAME");                                                                                                 //Natural: ASSIGN BENA9878.#ERROR-CDE := 'SAME'
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            pdaBena9879.getBena9879_Pnd_Tiaa_Cntrct().setValue(pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(1));                                                 //Natural: ASSIGN BENA9879.#TIAA-CNTRCT := #SAME-AS-CNTRCT ( 1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  READ FOR ONE-UP CALL
            //*  CREF
            //*  PAYEE CODE
            pnd_Psgm003_Common_Area.reset();                                                                                                                              //Natural: RESET #PSGM003-COMMON-AREA
            pnd_Psgm003_Common_Area_Pnd_In_Function.setValue("PR");                                                                                                       //Natural: ASSIGN #IN-FUNCTION := 'PR'
            pnd_Psgm003_Common_Area_Pnd_In_Contract_Num_Typ.setValue("T");                                                                                                //Natural: ASSIGN #IN-CONTRACT-NUM-TYP := 'T'
            pnd_Psgm003_Common_Area_Pnd_In_Type.setValue(" ");                                                                                                            //Natural: ASSIGN #IN-TYPE := ' '
            pnd_Psgm003_Common_Area_Pnd_In_Ssnpin.setValue(" ");                                                                                                          //Natural: ASSIGN #IN-SSNPIN := ' '
            pnd_Psgm003_Common_Area_Pnd_In_Contract.setValue(pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(2));                                                   //Natural: ASSIGN #IN-CONTRACT := #SAME-AS-CNTRCT ( 2 )
            pnd_Psgm003_Common_Area_Pnd_In_Payee_Cd.setValue(" ");                                                                                                        //Natural: ASSIGN #IN-PAYEE-CD := ' '
            psgm003ReturnCode = DbsUtil.callExternalProgram("PSGM003",pnd_Psgm003_Common_Area_Pnd_In_Function,pnd_Psgm003_Common_Area_Pnd_In_Contract_Num_Typ,            //Natural: CALL 'PSGM003' USING #PSGM003-COMMON-AREA
                pnd_Psgm003_Common_Area_Pnd_In_Type,pnd_Psgm003_Common_Area_Pnd_In_Ssnpin,pnd_Psgm003_Common_Area_Pnd_In_Contract,pnd_Psgm003_Common_Area_Pnd_In_Payee_Cd,
                pnd_Psgm003_Common_Area_Pnd_Rt_Ret_Code,pnd_Psgm003_Common_Area_Pnd_Rt_Input_String,pnd_Psgm003_Common_Area_Pnd_Rt_Ssn,pnd_Psgm003_Common_Area_Pnd_Rt_Pin,
                pnd_Psgm003_Common_Area_Pnd_Rt_Tiaa_Cntrct_Nmbr,pnd_Psgm003_Common_Area_Pnd_Rt_Xpayee_Cde,pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Status_Code,
                pnd_Psgm003_Common_Area_Pnd_Rt_Ctrct_Status_Dt,pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Issue_Dt,pnd_Psgm003_Common_Area_Pnd_Rt_Lob,pnd_Psgm003_Common_Area_Pnd_Rt_Cref_Contract_Nbr,
                pnd_Psgm003_Common_Area_Pnd_Rt_Cref_Issued_Ind,pnd_Psgm003_Common_Area_Pnd_Rt_Ownership_Code,pnd_Psgm003_Common_Area_Pnd_Rt_Product_Code,
                pnd_Psgm003_Common_Area_Pnd_Rt_Option_Code,pnd_Psgm003_Common_Area_Pnd_Rt_Plan_Code,pnd_Psgm003_Common_Area_Pnd_Rt_Fund_Code,pnd_Psgm003_Common_Area_Pnd_Rt_Social_Code,
                pnd_Psgm003_Common_Area_Pnd_Rt_Custodial_Agreement,pnd_Psgm003_Common_Area_Pnd_Rt_Aas_Ind,pnd_Psgm003_Common_Area_Pnd_Rt_Platform_Ind,pnd_Psgm003_Common_Area_Pnd_Rt_Multi_Plan_Ind,
                pnd_Psgm003_Common_Area_Pnd_Rt_Tiaa_Mailed_Ind,pnd_Psgm003_Common_Area_Pnd_Rt_Cref_Mailed_Ind,pnd_Psgm003_Common_Area_Pnd_Rt_Dflt_Enrollment_Ind,
                pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Lst_Updt_Dt,pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Lst_Updt_Time,pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Lst_Updt_Usrid,
                pnd_Psgm003_Common_Area_Pnd_Rt_Co_Per_Addr_Ind,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Type_Cd,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_One,
                pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_Two,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_Three,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_Four,
                pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Line_Five,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_City,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_State,
                pnd_Psgm003_Common_Area_Pnd_Rt_Co_Zip,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Country,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Postal_Data,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Geo_Cd,
                pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Status,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Status_Dt,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Lst_Ch_Src_Cd,
                pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Lst_Ch_Dt,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Lst_Ch_Time,pnd_Psgm003_Common_Area_Pnd_Rt_Co_Addr_Lst_Ch_Usrid,
                pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Per_Addr_Ind,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Type_Cd,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_One,
                pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_Two,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_Three,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_Four,
                pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Line_Five,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_City,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_State,
                pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Zip,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Country,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Ch_Saving_Cde,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Ph_Bnk_Pt_Acct_Nbr,
                pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Bnk_Aba_Eft_Nbr,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Eft_Pay_Type_Cde,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Postal_Data,
                pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Geo_Cd,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Status,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Status_Dt,
                pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Lst_Ch_Src_Cd,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Lst_Ch_Dt,pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Lst_Ch_Time,
                pnd_Psgm003_Common_Area_Pnd_Rt_Cm_Addr_Lst_Ch_Usrid,pnd_Psgm003_Common_Area_Pnd_Rt_Prefix,pnd_Psgm003_Common_Area_Pnd_Rt_Last_Name,pnd_Psgm003_Common_Area_Pnd_Rt_First_Name,
                pnd_Psgm003_Common_Area_Pnd_Rt_Given_Name_Two,pnd_Psgm003_Common_Area_Pnd_Rt_Suffix_Desc,pnd_Psgm003_Common_Area_Pnd_Rt_Record_Status);
            //*  SUCCESS
            if (condition(pnd_Psgm003_Common_Area_Pnd_Rt_Record_Status.equals("Y")))                                                                                      //Natural: IF #RT-RECORD-STATUS = 'Y'
            {
                if (condition(! (pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Status_Code.equals(" ") || pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Status_Code.equals("Y")           //Natural: IF NOT ( #RT-CNTRCT-STATUS-CODE = ' ' OR = 'Y' OR = 'H' )
                    || pnd_Psgm003_Common_Area_Pnd_Rt_Cntrct_Status_Code.equals("H"))))
                {
                    pdaBena9878.getBena9878_Pnd_Error_Msg().setValue(DbsUtil.compress("Copy From Contract:", pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(2),    //Natural: COMPRESS 'Copy From Contract:' #SAME-AS-CNTRCT ( 2 ) 'is terminated on COR.' INTO BENA9878.#ERROR-MSG
                        "is terminated on COR."));
                    pdaBena9878.getBena9878_Pnd_Error_Cde().setValue("NSAME");                                                                                            //Natural: ASSIGN BENA9878.#ERROR-CDE := 'NSAME'
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                    sub_Process_Error();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Psgm003_Common_Area_Pnd_Rt_Cref_Issued_Ind.equals("N") || pnd_Psgm003_Common_Area_Pnd_Rt_Cref_Issued_Ind.equals(" ")))                  //Natural: IF #RT-CREF-ISSUED-IND = 'N' OR = ' '
                {
                    pdaBena9878.getBena9878_Pnd_Error_Msg().setValue(DbsUtil.compress("Copy From CREF#", pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(2),        //Natural: COMPRESS 'Copy From CREF#' #SAME-AS-CNTRCT ( 2 ) 'not issued' INTO BENA9878.#ERROR-MSG
                        "not issued"));
                    pdaBena9878.getBena9878_Pnd_Error_Cde().setValue("SAME");                                                                                             //Natural: ASSIGN BENA9878.#ERROR-CDE := 'SAME'
                }                                                                                                                                                         //Natural: END-IF
                pdaBena9879.getBena9879_Pnd_Tiaa_Cntrct().setValue(pnd_Psgm003_Common_Area_Pnd_Rt_Tiaa_Cntrct_Nmbr);                                                      //Natural: ASSIGN BENA9879.#TIAA-CNTRCT := #RT-TIAA-CNTRCT-NMBR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaBena9878.getBena9878_Pnd_Error_Msg().setValue(DbsUtil.compress("Copy From CREF# only; not on COR:", pdaBena9878.getBena9878_Pnd_Same_As_Cntrct().getValue(2))); //Natural: COMPRESS 'Copy From CREF# only; not on COR:' #SAME-AS-CNTRCT ( 2 ) INTO BENA9878.#ERROR-MSG
                pdaBena9878.getBena9878_Pnd_Error_Cde().setValue("SAME");                                                                                                 //Natural: ASSIGN BENA9878.#ERROR-CDE := 'SAME'
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  CHECK-MDM
    }
    private void sub_Process_Error() throws Exception                                                                                                                     //Natural: PROCESS-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        pdaBena9878.getBena9878_Pnd_Error_Pgm().setValue(Global.getPROGRAM());                                                                                            //Natural: ASSIGN BENA9878.#ERROR-PGM := *PROGRAM
                                                                                                                                                                          //Natural: PERFORM ESCAPE-PROGRAM
        sub_Escape_Program();
        if (condition(Global.isEscape())) {return;}
        //*  PROCESS-ERROR
    }

    //
}
