/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:06:36 PM
**        * FROM NATURAL SUBPROGRAM : Benn9862
************************************************************
**        * FILE NAME            : Benn9862.java
**        * CLASS NAME           : Benn9862
**        * INSTANCE NAME        : Benn9862
************************************************************
************************************************************************
* PROGRAM  : BENN9862
* SYSTEM   : BENEFICIARY-SYSTEM /* INTERFACE SAME AS
* TITLE    : TRANSLATE RELATIONSHIP CODE
* GENERATED: OCTOBER,16TH, 2001
*          :
*          :
*
* HISTORY
* CHANGED ON MAY 10,99 BY DEPAUL FOR RELEASE ____
* > CLONED FROM BENN9832 -  FONTOUR
* > DEC 03,99. JW GET TABLE FILE FOR $R FROM PDA BENA9832, REMOVE FIND
* > DEC 18,00. JW UPDATE CALL TO N9833 - ASSIGN CODE = 99 IF NO RETURN
************************************************************************

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benn9862 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaBena9862 pdaBena9862;
    private PdaBena9864 pdaBena9864;
    private PdaBenpda_M pdaBenpda_M;
    private PdaBenpda_E pdaBenpda_E;
    private PdaBena9863 pdaBena9863;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Bt_Table_Id_Key;

    private DbsGroup pnd_Bt_Table_Id_Key__R_Field_1;
    private DbsField pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Id;
    private DbsField pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Key;

    private DbsGroup pnd_Bt_Table_Id_Key__R_Field_2;
    private DbsField pnd_Bt_Table_Id_Key_Pnd_Intrfcng_Systm;
    private DbsField pnd_Bt_Table_Id_Key_Pnd_Intrfcng_Systm_Rc;
    private DbsField pnd_T_I;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaBena9863 = new PdaBena9863(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaBena9862 = new PdaBena9862(parameters);
        pdaBena9864 = new PdaBena9864(parameters);
        pdaBenpda_M = new PdaBenpda_M(parameters);
        pdaBenpda_E = new PdaBenpda_E(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Bt_Table_Id_Key = localVariables.newFieldInRecord("pnd_Bt_Table_Id_Key", "#BT-TABLE-ID-KEY", FieldType.STRING, 22);

        pnd_Bt_Table_Id_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Bt_Table_Id_Key__R_Field_1", "REDEFINE", pnd_Bt_Table_Id_Key);
        pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Id = pnd_Bt_Table_Id_Key__R_Field_1.newFieldInGroup("pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Id", "#BT-TABLE-ID", FieldType.STRING, 
            2);
        pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Key = pnd_Bt_Table_Id_Key__R_Field_1.newFieldInGroup("pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Key", "#BT-TABLE-KEY", 
            FieldType.STRING, 20);

        pnd_Bt_Table_Id_Key__R_Field_2 = pnd_Bt_Table_Id_Key__R_Field_1.newGroupInGroup("pnd_Bt_Table_Id_Key__R_Field_2", "REDEFINE", pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Key);
        pnd_Bt_Table_Id_Key_Pnd_Intrfcng_Systm = pnd_Bt_Table_Id_Key__R_Field_2.newFieldInGroup("pnd_Bt_Table_Id_Key_Pnd_Intrfcng_Systm", "#INTRFCNG-SYSTM", 
            FieldType.STRING, 8);
        pnd_Bt_Table_Id_Key_Pnd_Intrfcng_Systm_Rc = pnd_Bt_Table_Id_Key__R_Field_2.newFieldInGroup("pnd_Bt_Table_Id_Key_Pnd_Intrfcng_Systm_Rc", "#INTRFCNG-SYSTM-RC", 
            FieldType.STRING, 8);
        pnd_T_I = localVariables.newFieldInRecord("pnd_T_I", "#T-I", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Benn9862() throws Exception
    {
        super("Benn9862");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ERROR-INFO-SUB.##LAST-PROGRAM = *PROGRAM
        pdaBena9862.getBena9862_Pnd_Rltnshp_Cde().reset();                                                                                                                //Natural: RESET BENA9862.#RLTNSHP-CDE #NOT-ON-RT-TABLE MSG-INFO-SUB
        pdaBena9862.getBena9862_Pnd_Not_On_Rt_Table().reset();
        pdaBenpda_M.getMsg_Info_Sub().reset();
        short decideConditionsMet161 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN BENA9862.#SYSTM-CODE NE ' '
        if (condition(pdaBena9862.getBena9862_Pnd_Systm_Code().notEquals(" ")))
        {
            decideConditionsMet161++;
                                                                                                                                                                          //Natural: PERFORM TRANSLATE-CODE
            sub_Translate_Code();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN BENA9862.#SYSTM-CODE EQ ' '
        else if (condition(pdaBena9862.getBena9862_Pnd_Systm_Code().equals(" ")))
        {
            decideConditionsMet161++;
                                                                                                                                                                          //Natural: PERFORM TRANSLATE-TEXT
            sub_Translate_Text();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("No Relationship Code found for:", pdaBena9862.getBena9862_Pnd_Systm(),                   //Natural: COMPRESS 'No Relationship Code found for:' BENA9862.#SYSTM BENA9862.#SYSTM-CODE INTO ##MSG
                pdaBena9862.getBena9862_Pnd_Systm_Code()));
            pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                              //Natural: ASSIGN ##RETURN-CODE = 'E'
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TRANSLATE-CODE
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TRANSLATE-TEXT
    }
    private void sub_Translate_Code() throws Exception                                                                                                                    //Natural: TRANSLATE-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        pnd_Bt_Table_Id_Key_Pnd_Intrfcng_Systm.setValue(pdaBena9862.getBena9862_Pnd_Systm());                                                                             //Natural: ASSIGN #BT-TABLE-ID-KEY.#INTRFCNG-SYSTM = BENA9862.#SYSTM
        pnd_Bt_Table_Id_Key_Pnd_Intrfcng_Systm_Rc.setValue(pdaBena9862.getBena9862_Pnd_Systm_Code());                                                                     //Natural: ASSIGN #BT-TABLE-ID-KEY.#INTRFCNG-SYSTM-RC = BENA9862.#SYSTM-CODE
        //*  JW
        if (condition(pdaBena9864.getBena9864_Pnd_Dollar_R_Tab_Key().getValue("*").equals(pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Key)))                                         //Natural: IF #$R-TAB-KEY ( * ) = #BT-TABLE-KEY
        {
            //*  JW
            FIND_RECORD:                                                                                                                                                  //Natural: FOR #T-I 1 99
            for (pnd_T_I.setValue(1); condition(pnd_T_I.lessOrEqual(99)); pnd_T_I.nadd(1))
            {
                //*  JW - WAS ASSIGN FROM FILE
                if (condition(pdaBena9864.getBena9864_Pnd_Dollar_R_Tab_Key().getValue(pnd_T_I).equals(pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Key)))                             //Natural: IF #$R-TAB-KEY ( #T-I ) = #BT-TABLE-KEY
                {
                    //*  (FIND-RECORD.)
                    pdaBena9862.getBena9862_Pnd_Rltnshp_Cde().setValue(pdaBena9864.getBena9864_Pnd_Dollar_R_Tab_Text().getValue(pnd_T_I));                                //Natural: ASSIGN BENA9862.#RLTNSHP-CDE = #$R-TAB-TEXT ( #T-I )
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaBena9862.getBena9862_Pnd_Systm_Text().greater(" ")))                                                                                         //Natural: IF BENA9862.#SYSTM-TEXT GT ' '
            {
                                                                                                                                                                          //Natural: PERFORM TRANSLATE-TEXT
                sub_Translate_Text();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("No Relationship Code found for:", pdaBena9862.getBena9862_Pnd_Systm(),               //Natural: COMPRESS 'No Relationship Code found for:' BENA9862.#SYSTM BENA9862.#SYSTM-CODE INTO ##MSG
                    pdaBena9862.getBena9862_Pnd_Systm_Code()));
                pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                          //Natural: ASSIGN ##RETURN-CODE = 'E'
            }                                                                                                                                                             //Natural: END-IF
            //*  JW
        }                                                                                                                                                                 //Natural: END-IF
        //*  TRANSLATE-CODE
    }
    private void sub_Translate_Text() throws Exception                                                                                                                    //Natural: TRANSLATE-TEXT
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        pdaBena9863.getBena9863_Pnd_Systm_Text().setValue(pdaBena9862.getBena9862_Pnd_Systm_Text());                                                                      //Natural: MOVE BENA9862.#SYSTM-TEXT TO BENA9863.#SYSTM-TEXT
        //*  JW
        DbsUtil.callnat(Benn9863.class , getCurrentProcessState(), pdaBena9863.getBena9863(), pdaBena9864.getBena9864(), pdaBenpda_M.getMsg_Info_Sub(),                   //Natural: CALLNAT 'BENN9863' BENA9863 BENA9864 MSG-INFO-SUB ERROR-INFO-SUB
            pdaBenpda_E.getError_Info_Sub());
        if (condition(Global.isEscape())) return;
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ERROR-INFO-SUB.##LAST-PROGRAM = *PROGRAM
        if (condition(pdaBena9863.getBena9863_Pnd_Found().getBoolean()))                                                                                                  //Natural: IF BENA9863.#FOUND
        {
            pdaBena9862.getBena9862_Pnd_Rltnshp_Cde().setValue(pdaBena9863.getBena9863_Pnd_Rltnshp_Cde());                                                                //Natural: ASSIGN BENA9862.#RLTNSHP-CDE = BENA9863.#RLTNSHP-CDE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  JW 12/18 - WILL ACTUALLY
            pdaBena9862.getBena9862_Pnd_Rltnshp_Cde().setValue("38");                                                                                                     //Natural: ASSIGN BENA9862.#RLTNSHP-CDE = '38'
            //*  BECOME MOS OR ERROR IN N9820.
            pdaBena9862.getBena9862_Pnd_Not_On_Rt_Table().setValue(true);                                                                                                 //Natural: MOVE TRUE TO #NOT-ON-RT-TABLE
        }                                                                                                                                                                 //Natural: END-IF
        //*  TRANSLATE-TEXT
    }

    //
}
