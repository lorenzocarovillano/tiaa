/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:07:38 PM
**        * FROM NATURAL SUBPROGRAM : Benn9881
************************************************************
**        * FILE NAME            : Benn9881.java
**        * CLASS NAME           : Benn9881
**        * INSTANCE NAME        : Benn9881
************************************************************
************************************************************************
* PROGRAM NAME : BENN9881
* DESCRIPTION  : THIS PROGRAM CALLS CWF MODULES FROM THE BENEFICIARY
*                SYSTEM WEB CHANGES TO CREATE WORK REQUESTS.
* WRITTEN BY   : CINTIA FONTOURA
* BASED ON     : BENN5220
* DATE WRITTEN : MARCH, 05, 2001
************************************************************************
*    DATE      USERID                   DESCRIPTION
* MM/DD/YYYY  XXXXXXXX  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
* 04/01/2017  DURAND    PIN EXPANSION N7 TO N12
* 04/12/2005  KAKADIA   CREATED FOR R5 TOPS RELEASE
************************************************************************

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benn9881 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaBena5220 pdaBena5220;
    private PdaEfsa9120 pdaEfsa9120;
    private PdaEfsa902r pdaEfsa902r;
    private PdaErla1000 pdaErla1000;
    private PdaCwfa8010 pdaCwfa8010;
    private PdaCwfa6510 pdaCwfa6510;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_I;
    private DbsField pnd_Cdms_In_Routing;
    private DbsField pnd_Route_To_Next_Unit;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaEfsa9120 = new PdaEfsa9120(localVariables);
        pdaEfsa902r = new PdaEfsa902r(localVariables);
        pdaErla1000 = new PdaErla1000(localVariables);
        pdaCwfa8010 = new PdaCwfa8010(localVariables);
        pdaCwfa6510 = new PdaCwfa6510(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaBena5220 = new PdaBena5220(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_Cdms_In_Routing = localVariables.newFieldInRecord("pnd_Cdms_In_Routing", "#CDMS-IN-ROUTING", FieldType.STRING, 1);
        pnd_Route_To_Next_Unit = localVariables.newFieldInRecord("pnd_Route_To_Next_Unit", "#ROUTE-TO-NEXT-UNIT", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Benn9881() throws Exception
    {
        super("Benn9881");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().reset();                                                                                                        //Natural: RESET MSG-INFO-SUB.##RETURN-CODE MSG-INFO-SUB.##MSG
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().reset();
        pdaEfsa9120.getEfsa9120().reset();                                                                                                                                //Natural: RESET EFSA9120
        pdaEfsa9120.getEfsa9120_Action().setValue("AR");                                                                                                                  //Natural: ASSIGN EFSA9120.ACTION := 'AR'
        pdaEfsa9120.getEfsa9120_Cabinet_Prefix().setValue("P");                                                                                                           //Natural: ASSIGN EFSA9120.CABINET-PREFIX := 'P'
        pdaEfsa9120.getEfsa9120_Pin_Nbr().setValue(pdaBena5220.getBena5220_Pnd_Wpid_Pin());                                                                               //Natural: ASSIGN EFSA9120.PIN-NBR := BENA5220.#WPID-PIN
        pdaEfsa9120.getEfsa9120_Tiaa_Rcvd_Dte().getValue(1).setValue(Global.getDATN());                                                                                   //Natural: ASSIGN EFSA9120.TIAA-RCVD-DTE ( 1 ) := *DATN
        pdaEfsa9120.getEfsa9120_Wpid().getValue(1).setValue(pdaBena5220.getBena5220_Pnd_Wpid());                                                                          //Natural: ASSIGN EFSA9120.WPID ( 1 ) := BENA5220.#WPID
        pdaEfsa9120.getEfsa9120_Unit_Cde().getValue(1).setValue(pdaBena5220.getBena5220_Pnd_Wpid_Next_Unit());                                                            //Natural: ASSIGN EFSA9120.UNIT-CDE ( 1 ) := BENA5220.#WPID-NEXT-UNIT
        pdaEfsa9120.getEfsa9120_Contract_Nbr().getValue(1,"*").setValue(pdaBena5220.getBena5220_Pnd_Wpid_Contracts().getValue("*"));                                      //Natural: ASSIGN EFSA9120.CONTRACT-NBR ( 1,* ) := BENA5220.#WPID-CONTRACTS ( * )
        pdaEfsa9120.getEfsa9120_Status_Cde().getValue(1).setValue(pdaBena5220.getBena5220_Pnd_Wpid_Status());                                                             //Natural: ASSIGN EFSA9120.STATUS-CDE ( 1 ) := BENA5220.#WPID-STATUS
        pdaEfsa9120.getEfsa9120_Rqst_Origin_Cde().setValue("G");                                                                                                          //Natural: ASSIGN EFSA9120.RQST-ORIGIN-CDE := 'G'
        pdaEfsa9120.getEfsa9120_Multi_Rqst_Ind().getValue(1).setValue("0");                                                                                               //Natural: ASSIGN EFSA9120.MULTI-RQST-IND ( 1 ) := '0'
        pdaEfsa9120.getEfsa9120_Gen_Fldr_Ind().getValue(1).setValue("N");                                                                                                 //Natural: ASSIGN EFSA9120.GEN-FLDR-IND ( 1 ) := 'N'
        pdaEfsa9120.getEfsa9120_Rqst_Entry_Op_Cde().setValue(pdaBena5220.getBena5220_Pnd_User_Id());                                                                      //Natural: ASSIGN EFSA9120.RQST-ENTRY-OP-CDE := BENA5220.#USER-ID
        pdaEfsa9120.getEfsa9120_System().setValue(pdaBena5220.getBena5220_Pnd_Wpid_System());                                                                             //Natural: ASSIGN EFSA9120.SYSTEM := BENA5220.#WPID-SYSTEM
        pdaEfsa9120.getEfsa9120_Rqst_Origin_Cde().setValue("I");                                                                                                          //Natural: ASSIGN EFSA9120.RQST-ORIGIN-CDE := 'I'
        pdaEfsa9120.getEfsa9120_Spcl_Handling_Txt().getValue(1).setValue(pdaBena5220.getBena5220_Pnd_Wpid_Special_Handling_Text());                                       //Natural: ASSIGN EFSA9120.SPCL-HANDLING-TXT ( 1 ) := BENA5220.#WPID-SPECIAL-HANDLING-TEXT
        pdaEfsa9120.getEfsa9120_Rqst_Origin_Unit_Cde().setValue(pdaBena5220.getBena5220_Pnd_Wpid_Unit());                                                                 //Natural: ASSIGN EFSA9120.RQST-ORIGIN-UNIT-CDE := BENA5220.#WPID-UNIT
        pdaEfsa9120.getEfsa9120_Wpid_Validate_Ind().getValue(1).setValue(" ");                                                                                            //Natural: ASSIGN EFSA9120.WPID-VALIDATE-IND ( 1 ) := ' '
        pdaEfsa9120.getEfsa9120_Empl_Oprtr_Cde().getValue(1).setValue(pdaBena5220.getBena5220_Pnd_Empl_Racf_Id());                                                        //Natural: ASSIGN EFSA9120.EMPL-OPRTR-CDE ( 1 ) := BENA5220.#EMPL-RACF-ID
        if (condition(pdaEfsa9120.getEfsa9120_Status_Cde().getValue(1).greaterOrEqual("8000") && pdaEfsa9120.getEfsa9120_Status_Cde().getValue(1).lessOrEqual("8799")))   //Natural: IF EFSA9120.STATUS-CDE ( 1 ) = '8000' THRU '8799'
        {
            pdaEfsa9120.getEfsa9120_Corp_Status().getValue(1).setValue("9");                                                                                              //Natural: ASSIGN EFSA9120.CORP-STATUS ( 1 ) := '9'
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Efsn9120.class , getCurrentProcessState(), pdaEfsa9120.getEfsa9120(), pdaEfsa9120.getEfsa9120_Id(), pdaEfsa902r.getEfsa902r(),                    //Natural: CALLNAT 'EFSN9120' EFSA9120 EFSA9120-ID EFSA902R CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
        if (condition(Global.isEscape())) return;
        pdaBena5220.getBena5220_Pnd_Error_Msg().setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                      //Natural: ASSIGN BENA5220.#ERROR-MSG := MSG-INFO-SUB.##MSG
        if (condition(pdaBena5220.getBena5220_Pnd_Error_Msg().equals(" ")))                                                                                               //Natural: IF BENA5220.#ERROR-MSG = ' '
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaEfsa9120.getEfsa9120_Status_Cde().getValue(1).setValue("7990");                                                                                            //Natural: ASSIGN EFSA9120.STATUS-CDE ( 1 ) := '7990'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaBena5220.getBena5220_Pnd_Error_Msg().notEquals(" ")))                                                                                            //Natural: IF BENA5220.#ERROR-MSG NE ' '
        {
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
            pdaErla1000.getErla1000_Err_Pgm_Name().setValue(Global.getPROGRAM());                                                                                         //Natural: ASSIGN ERLA1000.ERR-PGM-NAME := *PROGRAM
            pdaErla1000.getErla1000_Err_Pgm_Level().setValue(Global.getLEVEL());                                                                                          //Natural: ASSIGN ERLA1000.ERR-PGM-LEVEL := *LEVEL
            pdaErla1000.getErla1000_Err_Nbr().setValue(Global.getERROR_NR());                                                                                             //Natural: ASSIGN ERLA1000.ERR-NBR := *ERROR-NR
            pdaErla1000.getErla1000_Err_Line_Nbr().setValue(Global.getERROR_LINE());                                                                                      //Natural: ASSIGN ERLA1000.ERR-LINE-NBR := *ERROR-LINE
            pdaErla1000.getErla1000_Err_Status_Cde().setValue("U");                                                                                                       //Natural: ASSIGN ERLA1000.ERR-STATUS-CDE := 'U'
            pdaErla1000.getErla1000_Err_Type_Cde().setValue("N");                                                                                                         //Natural: ASSIGN ERLA1000.ERR-TYPE-CDE := 'N'
            //*  ERLA1000.ERR-NOTES      := NASA190.#ERROR-MESSAGE
            pdaErla1000.getErla1000_Err_Notes().setValue(DbsUtil.compress("PIN", pdaBena5220.getBena5220_Pnd_Wpid_Pin(), pdaBena5220.getBena5220_Pnd_Error_Msg()));       //Natural: COMPRESS 'PIN' BENA5220.#WPID-PIN BENA5220.#ERROR-MSG INTO ERLA1000.ERR-NOTES
            DbsUtil.callnat(Erln1000.class , getCurrentProcessState(), pdaErla1000.getErla1000());                                                                        //Natural: CALLNAT 'ERLN1000' ERLA1000
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaBena5220.getBena5220_Pnd_Return_Log_Dte_Tme().setValue(pdaEfsa9120.getEfsa9120_Return_Date_Time().getValue(1));                                            //Natural: ASSIGN BENA5220.#RETURN-LOG-DTE-TME := EFSA9120.RETURN-DATE-TIME ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
