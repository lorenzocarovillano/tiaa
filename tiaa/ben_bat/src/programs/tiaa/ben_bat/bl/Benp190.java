/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:14:09 PM
**        * FROM NATURAL PROGRAM : Benp190
************************************************************
**        * FILE NAME            : Benp190.java
**        * CLASS NAME           : Benp190
**        * INSTANCE NAME        : Benp190
************************************************************
************************************************************************
* PROGRAM NAME : BENP190
* DESCRIPTION  : BENE WEB-E BATCH AUTO-VERIFY REPORT
*                - READ BENE-ACTIVITY FOR WEB-E WAITING VERIFICATION
*                - ADD UPDATE AND VERIFY BENEFICIARY RECORDS.
*                - CREATE AUDIT TRAIL.
*                - UPDATE WORK REQUEST STATUS.
* WRITTEN BY   : DENNIS DURAN
* DATE WRITTEN : NOV 9, 2011
************************************************************************
*    DATE      USERID                   DESCRIPTION
* 10/22/2012  ALAGANI   RE-STOWED DUE TO #BENE-GENDER #BENE-ADDR1
*                       #BENE-ADDR2  #BENE-ADDR3-CITY #BENE-COUNTRY
*                       #BENE-STATE #BENE-ZIP #BENE-PHONE WERE ADDED
*                       TO BENA002
* 04/01/2017  DURAND    PIN EXPANSION N7 TO N12
* MM/DD/YYYY  XXXXXXXX  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
************************************************************************

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benp190 extends BLNatBase
{
    // Data Areas
    private PdaBena009 pdaBena009;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_ba;
    private DbsField ba_Ba_Wpid;
    private DbsField ba_Ba_Pin;
    private DbsField ba_Ba_Rqst_Timestamp;
    private DbsField ba_Ba_Seq_Nmbr;
    private DbsField ba_Ba_Rqst_Cmpltd_Dte;

    private DbsGroup ba_Ba_Slctd_Cntrct_Group;
    private DbsField ba_Ba_Slctd_Tiaa;
    private DbsField ba_Ba_Slctd_Cref;
    private DbsField ba_Ba_Diff_Dsgntn;
    private DbsField ba_Ba_Name;
    private DbsField ba_Ba_Owner_Name;
    private DbsField ba_Ba_Lttr_Optn;
    private DbsField ba_Ba_Lttr_Info;
    private DbsGroup ba_Ba_Mail_AddrMuGroup;
    private DbsField ba_Ba_Mail_Addr;
    private DbsField ba_Ba_Addrss_Postal_Data;

    private DbsGroup ba__R_Field_1;
    private DbsField ba_Ba_Addrss_Zip_Plus_4;
    private DbsField ba_Ba_Addrss_Carrier_Rte;
    private DbsField ba_Ba_Addrss_Walk_Rte;
    private DbsField ba_Ba_Addrss_Usps_Future_Use;
    private DbsField ba_Ba_Us_Frgn;
    private DbsField ba_Ba_Addr_Srce;
    private DbsField ba_Ba_Cnfrm_Stmnt_Prod_Dte;
    private DbsField ba_Ba_Orig_Updt_Userid;
    private DbsField ba_Ba_Orig_Updt_Dte;

    private DbsGroup ba__R_Field_2;
    private DbsField ba_Ba_Orig_Yyyy;
    private DbsField ba_Ba_Orig_Mm;
    private DbsField ba_Ba_Orig_Dd;
    private DbsField ba_Ba_Orig_Updt_Tme;
    private DbsField ba_Ba_Vrfy_Ind;
    private DbsField ba_Ba_Vrfy_Reason;
    private DbsField ba_Ba_Vrfy_Userid;
    private DbsField ba_Ba_Vrfy_Dte;
    private DbsField ba_Ba_Vrfy_Tme;
    private DbsField ba_Ba_Rcrd_Last_Updt_Dte;
    private DbsField ba_Ba_Rcrd_Last_Updt_Tme;
    private DbsField ba_Ba_Rcrd_Last_Updt_Userid;
    private DbsField ba_Ba_Partial_Data_Ind;
    private DbsField ba_Ba_Eff_Dte;
    private DbsField ba_Ba_Ph_Last_Nme;
    private DbsField ba_Ba_Wpid_Unit;

    private DataAccessProgramView vw_bc;
    private DbsField bc_Bc_Pin;
    private DbsField bc_Bc_Tiaa_Cntrct;
    private DbsField bc_Bc_Cref_Cntrct;
    private DbsField bc_Bc_Tiaa_Cref_Ind;
    private DbsField bc_Bc_Stat;
    private DbsField bc_Bc_Cntrct_Type;
    private DbsField bc_Bc_Rqst_Timestamp;
    private DbsField bc_Bc_Eff_Dte;
    private DbsField bc_Bc_Mos_Ind;
    private DbsField bc_Bc_Irvcbl_Ind;
    private DbsField bc_Bc_Pymnt_Chld_Dcsd_Ind;
    private DbsField bc_Bc_Exempt_Spouse_Rights;
    private DbsField bc_Bc_Spouse_Waived_Bnfts;
    private DbsField bc_Bc_Trust_Data_Fldr;
    private DbsField bc_Bc_Bene_Addr_Fldr;
    private DbsField bc_Bc_Co_Owner_Data_Fldr;
    private DbsField bc_Bc_Fldr_Min;
    private DbsField bc_Bc_Fldr_Srce_Id;
    private DbsField bc_Bc_Fldr_Log_Dte_Tme;
    private DbsField bc_Bc_Rcrd_Last_Updt_Dte;
    private DbsField bc_Bc_Rcrd_Last_Updt_Tme;
    private DbsField bc_Bc_Rcrd_Last_Updt_Userid;
    private DbsField bc_Bc_Last_Dsgntn_Srce;
    private DbsField bc_Bc_Last_Dsgntn_System;
    private DbsField bc_Bc_Last_Dsgntn_Userid;
    private DbsField bc_Bc_Last_Dsgntn_Dte;
    private DbsField bc_Bc_Last_Dsgntn_Tme;
    private DbsField bc_Bc_Last_Vrfy_Dte;
    private DbsField bc_Bc_Last_Vrfy_Tme;
    private DbsField bc_Bc_Last_Vrfy_Userid;
    private DbsField bc_Bc_Tiaa_Cref_Chng_Dte;
    private DbsField bc_Bc_Tiaa_Cref_Chng_Tme;
    private DbsField bc_Bc_Chng_Pwr_Atty;

    private DataAccessProgramView vw_bch;
    private DbsField bch_Bc_Pin;
    private DbsField bch_Bc_Tiaa_Cntrct;
    private DbsField bch_Bc_Cref_Cntrct;
    private DbsField bch_Bc_Tiaa_Cref_Ind;
    private DbsField bch_Bc_Stat;
    private DbsField bch_Bc_Cntrct_Type;
    private DbsField bch_Bc_Rqst_Timestamp;
    private DbsField bch_Bc_Eff_Dte;
    private DbsField bch_Bc_Mos_Ind;
    private DbsField bch_Bc_Irvcbl_Ind;
    private DbsField bch_Bc_Pymnt_Chld_Dcsd_Ind;
    private DbsField bch_Bc_Exempt_Spouse_Rights;
    private DbsField bch_Bc_Spouse_Waived_Bnfts;
    private DbsField bch_Bc_Trust_Data_Fldr;
    private DbsField bch_Bc_Bene_Addr_Fldr;
    private DbsField bch_Bc_Co_Owner_Data_Fldr;
    private DbsField bch_Bc_Fldr_Min;
    private DbsField bch_Bc_Fldr_Srce_Id;
    private DbsField bch_Bc_Fldr_Log_Dte_Tme;
    private DbsField bch_Bc_Rcrd_Last_Updt_Dte;
    private DbsField bch_Bc_Rcrd_Last_Updt_Tme;
    private DbsField bch_Bc_Rcrd_Last_Updt_Userid;
    private DbsField bch_Bc_Last_Dsgntn_Srce;
    private DbsField bch_Bc_Last_Dsgntn_System;
    private DbsField bch_Bc_Last_Dsgntn_Userid;
    private DbsField bch_Bc_Last_Dsgntn_Dte;
    private DbsField bch_Bc_Last_Dsgntn_Tme;
    private DbsField bch_Bc_Last_Vrfy_Dte;
    private DbsField bch_Bc_Last_Vrfy_Tme;
    private DbsField bch_Bc_Last_Vrfy_Userid;
    private DbsField bch_Bc_Tiaa_Cref_Chng_Dte;
    private DbsField bch_Bc_Tiaa_Cref_Chng_Tme;
    private DbsField bch_Bc_Chng_Pwr_Atty;

    private DataAccessProgramView vw_bm;
    private DbsField bm_Bm_Pin;
    private DbsField bm_Bm_Tiaa_Cntrct;
    private DbsField bm_Bm_Cref_Cntrct;
    private DbsField bm_Bm_Tiaa_Cref_Ind;
    private DbsField bm_Bm_Stat;
    private DbsField bm_Bm_Rqst_Timestamp;
    private DbsField bm_Bm_Rcrd_Last_Updt_Dte;
    private DbsField bm_Bm_Rcrd_Last_Updt_Tme;
    private DbsField bm_Bm_Rcrd_Last_Updt_Userid;
    private DbsField bm_Count_Castbm_Mos_Txt_Group;

    private DbsGroup bm_Bm_Mos_Txt_Group;
    private DbsField bm_Bm_Mos_Txt;

    private DataAccessProgramView vw_bmh;
    private DbsField bmh_Bm_Pin;
    private DbsField bmh_Bm_Tiaa_Cntrct;
    private DbsField bmh_Bm_Cref_Cntrct;
    private DbsField bmh_Bm_Tiaa_Cref_Ind;
    private DbsField bmh_Bm_Stat;
    private DbsField bmh_Bm_Rqst_Timestamp;
    private DbsField bmh_Bm_Rcrd_Last_Updt_Dte;
    private DbsField bmh_Bm_Rcrd_Last_Updt_Tme;
    private DbsField bmh_Bm_Rcrd_Last_Updt_Userid;
    private DbsField bmh_Count_Castbm_Mos_Txt_Group;

    private DbsGroup bmh_Bm_Mos_Txt_Group;
    private DbsField bmh_Bm_Mos_Txt;

    private DataAccessProgramView vw_bd;
    private DbsField bd_Bd_Pin;
    private DbsField bd_Bd_Tiaa_Cntrct;
    private DbsField bd_Bd_Cref_Cntrct;
    private DbsField bd_Bd_Tiaa_Cref_Ind;
    private DbsField bd_Bd_Stat;
    private DbsField bd_Bd_Seq_Nmbr;
    private DbsField bd_Bd_Rqst_Timestamp;
    private DbsField bd_Bd_Rcrd_Last_Updt_Dte;
    private DbsField bd_Bd_Rcrd_Last_Updt_Tme;
    private DbsField bd_Bd_Rcrd_Last_Updt_Userid;
    private DbsField bd_Bd_Bene_Type;
    private DbsField bd_Bd_Rltn_Cd;
    private DbsField bd_Bd_Bene_Name1;
    private DbsField bd_Bd_Bene_Name2;
    private DbsField bd_Bd_Rltn_Free_Txt;
    private DbsField bd_Bd_Dte_Birth_Trust;
    private DbsField bd_Bd_Ss_Cd;
    private DbsField bd_Bd_Ss_Nmbr;
    private DbsField bd_Bd_Perc_Share_Ind;
    private DbsField bd_Bd_Share_Perc;
    private DbsField bd_Bd_Share_Ntor;
    private DbsField bd_Bd_Share_Dtor;
    private DbsField bd_Bd_Irvcbl_Ind;
    private DbsField bd_Bd_Stlmnt_Rstrctn;
    private DbsField bd_Bd_Spcl_Txt1;
    private DbsField bd_Bd_Spcl_Txt2;
    private DbsField bd_Bd_Spcl_Txt3;
    private DbsField bd_Bd_Dflt_Estate;

    private DataAccessProgramView vw_bdh;
    private DbsField bdh_Bd_Pin;
    private DbsField bdh_Bd_Tiaa_Cntrct;
    private DbsField bdh_Bd_Cref_Cntrct;
    private DbsField bdh_Bd_Tiaa_Cref_Ind;
    private DbsField bdh_Bd_Stat;
    private DbsField bdh_Bd_Seq_Nmbr;
    private DbsField bdh_Bd_Rqst_Timestamp;
    private DbsField bdh_Bd_Rcrd_Last_Updt_Dte;
    private DbsField bdh_Bd_Rcrd_Last_Updt_Tme;
    private DbsField bdh_Bd_Rcrd_Last_Updt_Userid;
    private DbsField bdh_Bd_Bene_Type;
    private DbsField bdh_Bd_Rltn_Cd;
    private DbsField bdh_Bd_Bene_Name1;
    private DbsField bdh_Bd_Bene_Name2;
    private DbsField bdh_Bd_Rltn_Free_Txt;
    private DbsField bdh_Bd_Dte_Birth_Trust;
    private DbsField bdh_Bd_Ss_Cd;
    private DbsField bdh_Bd_Ss_Nmbr;
    private DbsField bdh_Bd_Perc_Share_Ind;
    private DbsField bdh_Bd_Share_Perc;
    private DbsField bdh_Bd_Share_Ntor;
    private DbsField bdh_Bd_Share_Dtor;
    private DbsField bdh_Bd_Irvcbl_Ind;
    private DbsField bdh_Bd_Stlmnt_Rstrctn;
    private DbsField bdh_Bd_Spcl_Txt1;
    private DbsField bdh_Bd_Spcl_Txt2;
    private DbsField bdh_Bd_Spcl_Txt3;
    private DbsField bdh_Bd_Dflt_Estate;

    private DataAccessProgramView vw_bt;
    private DbsField bt_Bt_Table_Id;
    private DbsField bt_Bt_Table_Key;
    private DbsField bt_Bt_Table_Text;

    private DbsGroup bt__R_Field_3;
    private DbsField bt_Bt_Auto_Verify;
    private DbsField pnd_Stat_Pin_Cntrct_Ind_Key;

    private DbsGroup pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_4;
    private DbsField pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Bc_Stat;
    private DbsField pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Bc_Pin_Cntrct_Ind_Key;

    private DbsGroup pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_5;
    private DbsField pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Bc_Pin;
    private DbsField pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Bc_Tiaa_Cntrct;
    private DbsField pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Bc_Tiaa_Cref_Ind;
    private DbsField pnd_Ba_Orig_Updt_Userid_Dte;

    private DbsGroup pnd_Ba_Orig_Updt_Userid_Dte__R_Field_6;
    private DbsField pnd_Ba_Orig_Updt_Userid_Dte_Pnd_Ba_Orig_Updt_Userid;
    private DbsField pnd_Ba_Orig_Updt_Userid_Dte_Pnd_Ba_Vrfy_Ind;
    private DbsField pnd_Ba_Orig_Updt_Userid_Dte_Pnd_Ba_Orig_Updt_Dte;
    private DbsField pnd_Ba_Orig_Updt_Userid_Dte_Pnd_Ba_Seq_Nmbr;
    private DbsField pnd_Bt_Table_Id_Key;

    private DbsGroup pnd_Bt_Table_Id_Key__R_Field_7;
    private DbsField pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Id;
    private DbsField pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Key;
    private DbsField pnd_Auto_Verify_To;
    private DbsField pnd_Auto_Verify_From;
    private DbsField pnd_Ba_Isn;
    private DbsField pnd_Bc_Isn;
    private DbsField pnd_Bd_Isn;
    private DbsField pnd_Bm_Isn;
    private DbsField pnd_Date;
    private DbsField pnd_Datn;

    private DbsGroup pnd_Details;
    private DbsField pnd_Details_Pnd_Add_Hist;
    private DbsField pnd_Details_Pnd_Dsgntn;
    private DbsField pnd_Details_Pnd_Dlt_Old;
    private DbsField pnd_Details_Pnd_Chg_Stat;
    private DbsField pnd_Details_Pnd_Msg;
    private DbsField pnd_Details_Pnd_Verified;
    private DbsField pnd_Details_Pnd_X;
    private DbsField pnd_Timn;
    private DbsField pnd_Total_Auto_Verify;
    private DbsField pnd_Total_Ba_Read;
    private DbsField pnd_Total_Ba_Error;
    private DbsField pls_Trace;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaBena009 = new PdaBena009(localVariables);

        // Local Variables

        vw_ba = new DataAccessProgramView(new NameInfo("vw_ba", "BA"), "BENE_ACTIVITY_12", "BENE_ACTIVITY", DdmPeriodicGroups.getInstance().getGroups("BENE_ACTIVITY_12"));
        ba_Ba_Wpid = vw_ba.getRecord().newFieldInGroup("ba_Ba_Wpid", "BA-WPID", FieldType.STRING, 6, RepeatingFieldStrategy.None, "BA_WPID");
        ba_Ba_Wpid.setDdmHeader("WPID");
        ba_Ba_Pin = vw_ba.getRecord().newFieldInGroup("ba_Ba_Pin", "BA-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BA_PIN");
        ba_Ba_Pin.setDdmHeader("PIN");
        ba_Ba_Rqst_Timestamp = vw_ba.getRecord().newFieldInGroup("ba_Ba_Rqst_Timestamp", "BA-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BA_RQST_TIMESTAMP");
        ba_Ba_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        ba_Ba_Seq_Nmbr = vw_ba.getRecord().newFieldInGroup("ba_Ba_Seq_Nmbr", "BA-SEQ-NMBR", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "BA_SEQ_NMBR");
        ba_Ba_Seq_Nmbr.setDdmHeader("SEQ/NMBR");
        ba_Ba_Rqst_Cmpltd_Dte = vw_ba.getRecord().newFieldInGroup("ba_Ba_Rqst_Cmpltd_Dte", "BA-RQST-CMPLTD-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BA_RQST_CMPLTD_DTE");
        ba_Ba_Rqst_Cmpltd_Dte.setDdmHeader("RQST/COMPLETED/DATE");

        ba_Ba_Slctd_Cntrct_Group = vw_ba.getRecord().newGroupInGroup("ba_Ba_Slctd_Cntrct_Group", "BA-SLCTD-CNTRCT-GROUP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "BENE_ACTIVITY_BA_SLCTD_CNTRCT_GROUP");
        ba_Ba_Slctd_Tiaa = ba_Ba_Slctd_Cntrct_Group.newFieldArrayInGroup("ba_Ba_Slctd_Tiaa", "BA-SLCTD-TIAA", FieldType.STRING, 10, new DbsArrayController(1, 
            20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "BA_SLCTD_TIAA", "BENE_ACTIVITY_BA_SLCTD_CNTRCT_GROUP");
        ba_Ba_Slctd_Tiaa.setDdmHeader("SELECTED/TIAA/CONTRACTS");
        ba_Ba_Slctd_Cref = ba_Ba_Slctd_Cntrct_Group.newFieldArrayInGroup("ba_Ba_Slctd_Cref", "BA-SLCTD-CREF", FieldType.STRING, 10, new DbsArrayController(1, 
            20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "BA_SLCTD_CREF", "BENE_ACTIVITY_BA_SLCTD_CNTRCT_GROUP");
        ba_Ba_Slctd_Cref.setDdmHeader("SELECTED/CREF/CONTRACTS");
        ba_Ba_Diff_Dsgntn = vw_ba.getRecord().newFieldInGroup("ba_Ba_Diff_Dsgntn", "BA-DIFF-DSGNTN", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BA_DIFF_DSGNTN");
        ba_Ba_Diff_Dsgntn.setDdmHeader("DIFF/DSGNTN");
        ba_Ba_Name = vw_ba.getRecord().newFieldInGroup("ba_Ba_Name", "BA-NAME", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BA_NAME");
        ba_Ba_Owner_Name = vw_ba.getRecord().newFieldInGroup("ba_Ba_Owner_Name", "BA-OWNER-NAME", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BA_OWNER_NAME");
        ba_Ba_Lttr_Optn = vw_ba.getRecord().newFieldInGroup("ba_Ba_Lttr_Optn", "BA-LTTR-OPTN", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BA_LTTR_OPTN");
        ba_Ba_Lttr_Optn.setDdmHeader("LETTER/OPTION");
        ba_Ba_Lttr_Info = vw_ba.getRecord().newFieldInGroup("ba_Ba_Lttr_Info", "BA-LTTR-INFO", FieldType.STRING, 10, RepeatingFieldStrategy.None, "BA_LTTR_INFO");
        ba_Ba_Lttr_Info.setDdmHeader("LETTER/INFO");
        ba_Ba_Mail_AddrMuGroup = vw_ba.getRecord().newGroupInGroup("BA_BA_MAIL_ADDRMuGroup", "BA_MAIL_ADDRMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "BENE_ACTIVITY_BA_MAIL_ADDR");
        ba_Ba_Mail_Addr = ba_Ba_Mail_AddrMuGroup.newFieldArrayInGroup("ba_Ba_Mail_Addr", "BA-MAIL-ADDR", FieldType.STRING, 35, new DbsArrayController(1, 
            5), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "BA_MAIL_ADDR");
        ba_Ba_Mail_Addr.setDdmHeader("MAILING/ADDRESS");
        ba_Ba_Addrss_Postal_Data = vw_ba.getRecord().newFieldInGroup("ba_Ba_Addrss_Postal_Data", "BA-ADDRSS-POSTAL-DATA", FieldType.STRING, 32, RepeatingFieldStrategy.None, 
            "BA_ADDRSS_POSTAL_DATA");
        ba_Ba_Addrss_Postal_Data.setDdmHeader("ADDRS/POSTAL/DATA");

        ba__R_Field_1 = vw_ba.getRecord().newGroupInGroup("ba__R_Field_1", "REDEFINE", ba_Ba_Addrss_Postal_Data);
        ba_Ba_Addrss_Zip_Plus_4 = ba__R_Field_1.newFieldInGroup("ba_Ba_Addrss_Zip_Plus_4", "BA-ADDRSS-ZIP-PLUS-4", FieldType.STRING, 9);
        ba_Ba_Addrss_Carrier_Rte = ba__R_Field_1.newFieldInGroup("ba_Ba_Addrss_Carrier_Rte", "BA-ADDRSS-CARRIER-RTE", FieldType.STRING, 4);
        ba_Ba_Addrss_Walk_Rte = ba__R_Field_1.newFieldInGroup("ba_Ba_Addrss_Walk_Rte", "BA-ADDRSS-WALK-RTE", FieldType.STRING, 4);
        ba_Ba_Addrss_Usps_Future_Use = ba__R_Field_1.newFieldInGroup("ba_Ba_Addrss_Usps_Future_Use", "BA-ADDRSS-USPS-FUTURE-USE", FieldType.STRING, 15);
        ba_Ba_Us_Frgn = vw_ba.getRecord().newFieldInGroup("ba_Ba_Us_Frgn", "BA-US-FRGN", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BA_US_FRGN");
        ba_Ba_Us_Frgn.setDdmHeader("US/FRGN/IND");
        ba_Ba_Addr_Srce = vw_ba.getRecord().newFieldInGroup("ba_Ba_Addr_Srce", "BA-ADDR-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BA_ADDR_SRCE");
        ba_Ba_Addr_Srce.setDdmHeader("ADDR/SRCE");
        ba_Ba_Cnfrm_Stmnt_Prod_Dte = vw_ba.getRecord().newFieldInGroup("ba_Ba_Cnfrm_Stmnt_Prod_Dte", "BA-CNFRM-STMNT-PROD-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BA_CNFRM_STMNT_PROD_DTE");
        ba_Ba_Cnfrm_Stmnt_Prod_Dte.setDdmHeader("CONFIRM/STATEMENT/PROD DATE");
        ba_Ba_Orig_Updt_Userid = vw_ba.getRecord().newFieldInGroup("ba_Ba_Orig_Updt_Userid", "BA-ORIG-UPDT-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BA_ORIG_UPDT_USERID");
        ba_Ba_Orig_Updt_Userid.setDdmHeader("ORIG UPDT/USER ID");
        ba_Ba_Orig_Updt_Dte = vw_ba.getRecord().newFieldInGroup("ba_Ba_Orig_Updt_Dte", "BA-ORIG-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BA_ORIG_UPDT_DTE");
        ba_Ba_Orig_Updt_Dte.setDdmHeader("ORIG UPDT/DATE");

        ba__R_Field_2 = vw_ba.getRecord().newGroupInGroup("ba__R_Field_2", "REDEFINE", ba_Ba_Orig_Updt_Dte);
        ba_Ba_Orig_Yyyy = ba__R_Field_2.newFieldInGroup("ba_Ba_Orig_Yyyy", "BA-ORIG-YYYY", FieldType.NUMERIC, 4);
        ba_Ba_Orig_Mm = ba__R_Field_2.newFieldInGroup("ba_Ba_Orig_Mm", "BA-ORIG-MM", FieldType.NUMERIC, 2);
        ba_Ba_Orig_Dd = ba__R_Field_2.newFieldInGroup("ba_Ba_Orig_Dd", "BA-ORIG-DD", FieldType.NUMERIC, 2);
        ba_Ba_Orig_Updt_Tme = vw_ba.getRecord().newFieldInGroup("ba_Ba_Orig_Updt_Tme", "BA-ORIG-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BA_ORIG_UPDT_TME");
        ba_Ba_Orig_Updt_Tme.setDdmHeader("ORIG UPDT/TIME");
        ba_Ba_Vrfy_Ind = vw_ba.getRecord().newFieldInGroup("ba_Ba_Vrfy_Ind", "BA-VRFY-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BA_VRFY_IND");
        ba_Ba_Vrfy_Ind.setDdmHeader("VRFY/IND");
        ba_Ba_Vrfy_Reason = vw_ba.getRecord().newFieldInGroup("ba_Ba_Vrfy_Reason", "BA-VRFY-REASON", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "BA_VRFY_REASON");
        ba_Ba_Vrfy_Reason.setDdmHeader("VRFY/REASON");
        ba_Ba_Vrfy_Userid = vw_ba.getRecord().newFieldInGroup("ba_Ba_Vrfy_Userid", "BA-VRFY-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BA_VRFY_USERID");
        ba_Ba_Vrfy_Userid.setDdmHeader("VERIFY/USER ID");
        ba_Ba_Vrfy_Dte = vw_ba.getRecord().newFieldInGroup("ba_Ba_Vrfy_Dte", "BA-VRFY-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "BA_VRFY_DTE");
        ba_Ba_Vrfy_Dte.setDdmHeader("VERIFY/DATE");
        ba_Ba_Vrfy_Tme = vw_ba.getRecord().newFieldInGroup("ba_Ba_Vrfy_Tme", "BA-VRFY-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, "BA_VRFY_TME");
        ba_Ba_Vrfy_Tme.setDdmHeader("VERIFY/TIME");
        ba_Ba_Rcrd_Last_Updt_Dte = vw_ba.getRecord().newFieldInGroup("ba_Ba_Rcrd_Last_Updt_Dte", "BA-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BA_RCRD_LAST_UPDT_DTE");
        ba_Ba_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        ba_Ba_Rcrd_Last_Updt_Tme = vw_ba.getRecord().newFieldInGroup("ba_Ba_Rcrd_Last_Updt_Tme", "BA-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BA_RCRD_LAST_UPDT_TME");
        ba_Ba_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        ba_Ba_Rcrd_Last_Updt_Userid = vw_ba.getRecord().newFieldInGroup("ba_Ba_Rcrd_Last_Updt_Userid", "BA-RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "BA_RCRD_LAST_UPDT_USERID");
        ba_Ba_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        ba_Ba_Partial_Data_Ind = vw_ba.getRecord().newFieldInGroup("ba_Ba_Partial_Data_Ind", "BA-PARTIAL-DATA-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BA_PARTIAL_DATA_IND");
        ba_Ba_Partial_Data_Ind.setDdmHeader("PARTIAL/DATA/IND");
        ba_Ba_Eff_Dte = vw_ba.getRecord().newFieldInGroup("ba_Ba_Eff_Dte", "BA-EFF-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "BA_EFF_DTE");
        ba_Ba_Eff_Dte.setDdmHeader("EFFECTIVE/DATE");
        ba_Ba_Ph_Last_Nme = vw_ba.getRecord().newFieldInGroup("ba_Ba_Ph_Last_Nme", "BA-PH-LAST-NME", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "BA_PH_LAST_NME");
        ba_Ba_Ph_Last_Nme.setDdmHeader("LAST/NAME");
        ba_Ba_Wpid_Unit = vw_ba.getRecord().newFieldInGroup("ba_Ba_Wpid_Unit", "BA-WPID-UNIT", FieldType.STRING, 8, RepeatingFieldStrategy.None, "BA_WPID_UNIT");
        ba_Ba_Wpid_Unit.setDdmHeader("WPID/UNIT");
        registerRecord(vw_ba);

        vw_bc = new DataAccessProgramView(new NameInfo("vw_bc", "BC"), "BENE_CONTRACT_12", "BENE_CONTRACT");
        bc_Bc_Pin = vw_bc.getRecord().newFieldInGroup("bc_Bc_Pin", "BC-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BC_PIN");
        bc_Bc_Pin.setDdmHeader("PIN");
        bc_Bc_Tiaa_Cntrct = vw_bc.getRecord().newFieldInGroup("bc_Bc_Tiaa_Cntrct", "BC-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BC_TIAA_CNTRCT");
        bc_Bc_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bc_Bc_Cref_Cntrct = vw_bc.getRecord().newFieldInGroup("bc_Bc_Cref_Cntrct", "BC-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BC_CREF_CNTRCT");
        bc_Bc_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bc_Bc_Tiaa_Cref_Ind = vw_bc.getRecord().newFieldInGroup("bc_Bc_Tiaa_Cref_Ind", "BC-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_TIAA_CREF_IND");
        bc_Bc_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bc_Bc_Stat = vw_bc.getRecord().newFieldInGroup("bc_Bc_Stat", "BC-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_STAT");
        bc_Bc_Stat.setDdmHeader("STATUS");
        bc_Bc_Cntrct_Type = vw_bc.getRecord().newFieldInGroup("bc_Bc_Cntrct_Type", "BC-CNTRCT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_CNTRCT_TYPE");
        bc_Bc_Cntrct_Type.setDdmHeader("CNTRCT/TYPE");
        bc_Bc_Rqst_Timestamp = vw_bc.getRecord().newFieldInGroup("bc_Bc_Rqst_Timestamp", "BC-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BC_RQST_TIMESTAMP");
        bc_Bc_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        bc_Bc_Eff_Dte = vw_bc.getRecord().newFieldInGroup("bc_Bc_Eff_Dte", "BC-EFF-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "BC_EFF_DTE");
        bc_Bc_Eff_Dte.setDdmHeader("EFFECTIVE/DATE");
        bc_Bc_Mos_Ind = vw_bc.getRecord().newFieldInGroup("bc_Bc_Mos_Ind", "BC-MOS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_MOS_IND");
        bc_Bc_Mos_Ind.setDdmHeader("MOS/IND");
        bc_Bc_Irvcbl_Ind = vw_bc.getRecord().newFieldInGroup("bc_Bc_Irvcbl_Ind", "BC-IRVCBL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_IRVCBL_IND");
        bc_Bc_Irvcbl_Ind.setDdmHeader("IRVCBL/IND");
        bc_Bc_Pymnt_Chld_Dcsd_Ind = vw_bc.getRecord().newFieldInGroup("bc_Bc_Pymnt_Chld_Dcsd_Ind", "BC-PYMNT-CHLD-DCSD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_PYMNT_CHLD_DCSD_IND");
        bc_Bc_Pymnt_Chld_Dcsd_Ind.setDdmHeader("PYMNT/CHILD/DCSD");
        bc_Bc_Exempt_Spouse_Rights = vw_bc.getRecord().newFieldInGroup("bc_Bc_Exempt_Spouse_Rights", "BC-EXEMPT-SPOUSE-RIGHTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_EXEMPT_SPOUSE_RIGHTS");
        bc_Bc_Exempt_Spouse_Rights.setDdmHeader("EXEMPT/SPOUSAL/RIGHTS");
        bc_Bc_Spouse_Waived_Bnfts = vw_bc.getRecord().newFieldInGroup("bc_Bc_Spouse_Waived_Bnfts", "BC-SPOUSE-WAIVED-BNFTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_SPOUSE_WAIVED_BNFTS");
        bc_Bc_Spouse_Waived_Bnfts.setDdmHeader("SPOUSE/WAIVED/BENEFITS");
        bc_Bc_Trust_Data_Fldr = vw_bc.getRecord().newFieldInGroup("bc_Bc_Trust_Data_Fldr", "BC-TRUST-DATA-FLDR", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_TRUST_DATA_FLDR");
        bc_Bc_Trust_Data_Fldr.setDdmHeader("TRUST/DATA/IN FLDR");
        bc_Bc_Bene_Addr_Fldr = vw_bc.getRecord().newFieldInGroup("bc_Bc_Bene_Addr_Fldr", "BC-BENE-ADDR-FLDR", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_BENE_ADDR_FLDR");
        bc_Bc_Bene_Addr_Fldr.setDdmHeader("BENE/ADDRESS/IN FLDR");
        bc_Bc_Co_Owner_Data_Fldr = vw_bc.getRecord().newFieldInGroup("bc_Bc_Co_Owner_Data_Fldr", "BC-CO-OWNER-DATA-FLDR", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_CO_OWNER_DATA_FLDR");
        bc_Bc_Co_Owner_Data_Fldr.setDdmHeader("CO-OWNER/DATA FLDR");
        bc_Bc_Fldr_Min = vw_bc.getRecord().newFieldInGroup("bc_Bc_Fldr_Min", "BC-FLDR-MIN", FieldType.STRING, 11, RepeatingFieldStrategy.None, "BC_FLDR_MIN");
        bc_Bc_Fldr_Min.setDdmHeader("FOLDER/MIN");
        bc_Bc_Fldr_Srce_Id = vw_bc.getRecord().newFieldInGroup("bc_Bc_Fldr_Srce_Id", "BC-FLDR-SRCE-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "BC_FLDR_SRCE_ID");
        bc_Bc_Fldr_Srce_Id.setDdmHeader("FOLDER/SOURCE/ID");
        bc_Bc_Fldr_Log_Dte_Tme = vw_bc.getRecord().newFieldInGroup("bc_Bc_Fldr_Log_Dte_Tme", "BC-FLDR-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BC_FLDR_LOG_DTE_TME");
        bc_Bc_Fldr_Log_Dte_Tme.setDdmHeader("FOLDER/LOG DATE/AND TIME");
        bc_Bc_Rcrd_Last_Updt_Dte = vw_bc.getRecord().newFieldInGroup("bc_Bc_Rcrd_Last_Updt_Dte", "BC-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_RCRD_LAST_UPDT_DTE");
        bc_Bc_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bc_Bc_Rcrd_Last_Updt_Tme = vw_bc.getRecord().newFieldInGroup("bc_Bc_Rcrd_Last_Updt_Tme", "BC-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_RCRD_LAST_UPDT_TME");
        bc_Bc_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bc_Bc_Rcrd_Last_Updt_Userid = vw_bc.getRecord().newFieldInGroup("bc_Bc_Rcrd_Last_Updt_Userid", "BC-RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "BC_RCRD_LAST_UPDT_USERID");
        bc_Bc_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        bc_Bc_Last_Dsgntn_Srce = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Dsgntn_Srce", "BC-LAST-DSGNTN-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_SRCE");
        bc_Bc_Last_Dsgntn_Srce.setDdmHeader("LAST/DSGNTN/SOURCE");
        bc_Bc_Last_Dsgntn_System = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Dsgntn_System", "BC-LAST-DSGNTN-SYSTEM", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_SYSTEM");
        bc_Bc_Last_Dsgntn_System.setDdmHeader("LAST/DSGNTN/SYSTEM");
        bc_Bc_Last_Dsgntn_Userid = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Dsgntn_Userid", "BC-LAST-DSGNTN-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_USERID");
        bc_Bc_Last_Dsgntn_Userid.setDdmHeader("LAST/DSGNTN/USERID");
        bc_Bc_Last_Dsgntn_Dte = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Dsgntn_Dte", "BC-LAST-DSGNTN-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_DTE");
        bc_Bc_Last_Dsgntn_Dte.setDdmHeader("LAST/DSGNTN/DATE");
        bc_Bc_Last_Dsgntn_Tme = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Dsgntn_Tme", "BC-LAST-DSGNTN-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_TME");
        bc_Bc_Last_Dsgntn_Tme.setDdmHeader("LAST/DSGNTN/TIME");
        bc_Bc_Last_Vrfy_Dte = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Vrfy_Dte", "BC-LAST-VRFY-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_VRFY_DTE");
        bc_Bc_Last_Vrfy_Dte.setDdmHeader("LAST/VRFY/DATE");
        bc_Bc_Last_Vrfy_Tme = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Vrfy_Tme", "BC-LAST-VRFY-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_LAST_VRFY_TME");
        bc_Bc_Last_Vrfy_Tme.setDdmHeader("LAST/VRFY/TIME");
        bc_Bc_Last_Vrfy_Userid = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Vrfy_Userid", "BC-LAST-VRFY-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_VRFY_USERID");
        bc_Bc_Last_Vrfy_Userid.setDdmHeader("LAST/VRFY/USERID");
        bc_Bc_Tiaa_Cref_Chng_Dte = vw_bc.getRecord().newFieldInGroup("bc_Bc_Tiaa_Cref_Chng_Dte", "BC-TIAA-CREF-CHNG-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_TIAA_CREF_CHNG_DTE");
        bc_Bc_Tiaa_Cref_Chng_Dte.setDdmHeader("TIAA CREF/CHANGE/DATE");
        bc_Bc_Tiaa_Cref_Chng_Tme = vw_bc.getRecord().newFieldInGroup("bc_Bc_Tiaa_Cref_Chng_Tme", "BC-TIAA-CREF-CHNG-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_TIAA_CREF_CHNG_TME");
        bc_Bc_Tiaa_Cref_Chng_Tme.setDdmHeader("TIAA CREF/CHANGE/TIME");
        bc_Bc_Chng_Pwr_Atty = vw_bc.getRecord().newFieldInGroup("bc_Bc_Chng_Pwr_Atty", "BC-CHNG-PWR-ATTY", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_CHNG_PWR_ATTY");
        bc_Bc_Chng_Pwr_Atty.setDdmHeader("CHANGE/POWER/ATTNY");
        registerRecord(vw_bc);

        vw_bch = new DataAccessProgramView(new NameInfo("vw_bch", "BCH"), "BENE_CONTRACT_HIST_12", "BENE_CONT_HIST");
        bch_Bc_Pin = vw_bch.getRecord().newFieldInGroup("bch_Bc_Pin", "BC-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BC_PIN");
        bch_Bc_Pin.setDdmHeader("PIN");
        bch_Bc_Tiaa_Cntrct = vw_bch.getRecord().newFieldInGroup("bch_Bc_Tiaa_Cntrct", "BC-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BC_TIAA_CNTRCT");
        bch_Bc_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bch_Bc_Cref_Cntrct = vw_bch.getRecord().newFieldInGroup("bch_Bc_Cref_Cntrct", "BC-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BC_CREF_CNTRCT");
        bch_Bc_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bch_Bc_Tiaa_Cref_Ind = vw_bch.getRecord().newFieldInGroup("bch_Bc_Tiaa_Cref_Ind", "BC-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_TIAA_CREF_IND");
        bch_Bc_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bch_Bc_Stat = vw_bch.getRecord().newFieldInGroup("bch_Bc_Stat", "BC-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_STAT");
        bch_Bc_Stat.setDdmHeader("STATUS");
        bch_Bc_Cntrct_Type = vw_bch.getRecord().newFieldInGroup("bch_Bc_Cntrct_Type", "BC-CNTRCT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_CNTRCT_TYPE");
        bch_Bc_Cntrct_Type.setDdmHeader("CNTRCT/TYPE");
        bch_Bc_Rqst_Timestamp = vw_bch.getRecord().newFieldInGroup("bch_Bc_Rqst_Timestamp", "BC-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BC_RQST_TIMESTAMP");
        bch_Bc_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        bch_Bc_Eff_Dte = vw_bch.getRecord().newFieldInGroup("bch_Bc_Eff_Dte", "BC-EFF-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "BC_EFF_DTE");
        bch_Bc_Eff_Dte.setDdmHeader("EFFECTIVE/DATE");
        bch_Bc_Mos_Ind = vw_bch.getRecord().newFieldInGroup("bch_Bc_Mos_Ind", "BC-MOS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_MOS_IND");
        bch_Bc_Mos_Ind.setDdmHeader("MOS/IND");
        bch_Bc_Irvcbl_Ind = vw_bch.getRecord().newFieldInGroup("bch_Bc_Irvcbl_Ind", "BC-IRVCBL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_IRVCBL_IND");
        bch_Bc_Irvcbl_Ind.setDdmHeader("IRVCBL/IND");
        bch_Bc_Pymnt_Chld_Dcsd_Ind = vw_bch.getRecord().newFieldInGroup("bch_Bc_Pymnt_Chld_Dcsd_Ind", "BC-PYMNT-CHLD-DCSD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_PYMNT_CHLD_DCSD_IND");
        bch_Bc_Pymnt_Chld_Dcsd_Ind.setDdmHeader("PYMNT/CHILD/DCSD");
        bch_Bc_Exempt_Spouse_Rights = vw_bch.getRecord().newFieldInGroup("bch_Bc_Exempt_Spouse_Rights", "BC-EXEMPT-SPOUSE-RIGHTS", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "BC_EXEMPT_SPOUSE_RIGHTS");
        bch_Bc_Exempt_Spouse_Rights.setDdmHeader("EXEMPT/SPOUSAL/RIGHTS");
        bch_Bc_Spouse_Waived_Bnfts = vw_bch.getRecord().newFieldInGroup("bch_Bc_Spouse_Waived_Bnfts", "BC-SPOUSE-WAIVED-BNFTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_SPOUSE_WAIVED_BNFTS");
        bch_Bc_Spouse_Waived_Bnfts.setDdmHeader("SPOUSE/WAIVED/BENEFITS");
        bch_Bc_Trust_Data_Fldr = vw_bch.getRecord().newFieldInGroup("bch_Bc_Trust_Data_Fldr", "BC-TRUST-DATA-FLDR", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_TRUST_DATA_FLDR");
        bch_Bc_Trust_Data_Fldr.setDdmHeader("TRUST/DATA/IN FLDR");
        bch_Bc_Bene_Addr_Fldr = vw_bch.getRecord().newFieldInGroup("bch_Bc_Bene_Addr_Fldr", "BC-BENE-ADDR-FLDR", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_BENE_ADDR_FLDR");
        bch_Bc_Bene_Addr_Fldr.setDdmHeader("BENE/ADDRESS/IN FLDR");
        bch_Bc_Co_Owner_Data_Fldr = vw_bch.getRecord().newFieldInGroup("bch_Bc_Co_Owner_Data_Fldr", "BC-CO-OWNER-DATA-FLDR", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_CO_OWNER_DATA_FLDR");
        bch_Bc_Co_Owner_Data_Fldr.setDdmHeader("CO-OWNER/DATA FLDR");
        bch_Bc_Fldr_Min = vw_bch.getRecord().newFieldInGroup("bch_Bc_Fldr_Min", "BC-FLDR-MIN", FieldType.STRING, 11, RepeatingFieldStrategy.None, "BC_FLDR_MIN");
        bch_Bc_Fldr_Min.setDdmHeader("FOLDER/MIN");
        bch_Bc_Fldr_Srce_Id = vw_bch.getRecord().newFieldInGroup("bch_Bc_Fldr_Srce_Id", "BC-FLDR-SRCE-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "BC_FLDR_SRCE_ID");
        bch_Bc_Fldr_Srce_Id.setDdmHeader("FOLDER/SOURCE/ID");
        bch_Bc_Fldr_Log_Dte_Tme = vw_bch.getRecord().newFieldInGroup("bch_Bc_Fldr_Log_Dte_Tme", "BC-FLDR-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BC_FLDR_LOG_DTE_TME");
        bch_Bc_Fldr_Log_Dte_Tme.setDdmHeader("FOLDER/LOG DATE/AND TIME");
        bch_Bc_Rcrd_Last_Updt_Dte = vw_bch.getRecord().newFieldInGroup("bch_Bc_Rcrd_Last_Updt_Dte", "BC-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_RCRD_LAST_UPDT_DTE");
        bch_Bc_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bch_Bc_Rcrd_Last_Updt_Tme = vw_bch.getRecord().newFieldInGroup("bch_Bc_Rcrd_Last_Updt_Tme", "BC-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_RCRD_LAST_UPDT_TME");
        bch_Bc_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bch_Bc_Rcrd_Last_Updt_Userid = vw_bch.getRecord().newFieldInGroup("bch_Bc_Rcrd_Last_Updt_Userid", "BC-RCRD-LAST-UPDT-USERID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "BC_RCRD_LAST_UPDT_USERID");
        bch_Bc_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        bch_Bc_Last_Dsgntn_Srce = vw_bch.getRecord().newFieldInGroup("bch_Bc_Last_Dsgntn_Srce", "BC-LAST-DSGNTN-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_SRCE");
        bch_Bc_Last_Dsgntn_Srce.setDdmHeader("LAST/DSGNTN/SOURCE");
        bch_Bc_Last_Dsgntn_System = vw_bch.getRecord().newFieldInGroup("bch_Bc_Last_Dsgntn_System", "BC-LAST-DSGNTN-SYSTEM", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_SYSTEM");
        bch_Bc_Last_Dsgntn_System.setDdmHeader("LAST/DSGNTN/SYSTEM");
        bch_Bc_Last_Dsgntn_Userid = vw_bch.getRecord().newFieldInGroup("bch_Bc_Last_Dsgntn_Userid", "BC-LAST-DSGNTN-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_USERID");
        bch_Bc_Last_Dsgntn_Userid.setDdmHeader("LAST/DSGNTN/USERID");
        bch_Bc_Last_Dsgntn_Dte = vw_bch.getRecord().newFieldInGroup("bch_Bc_Last_Dsgntn_Dte", "BC-LAST-DSGNTN-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_DTE");
        bch_Bc_Last_Dsgntn_Dte.setDdmHeader("LAST/DSGNTN/DATE");
        bch_Bc_Last_Dsgntn_Tme = vw_bch.getRecord().newFieldInGroup("bch_Bc_Last_Dsgntn_Tme", "BC-LAST-DSGNTN-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_TME");
        bch_Bc_Last_Dsgntn_Tme.setDdmHeader("LAST/DSGNTN/TIME");
        bch_Bc_Last_Vrfy_Dte = vw_bch.getRecord().newFieldInGroup("bch_Bc_Last_Vrfy_Dte", "BC-LAST-VRFY-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_VRFY_DTE");
        bch_Bc_Last_Vrfy_Dte.setDdmHeader("LAST/VRFY/DATE");
        bch_Bc_Last_Vrfy_Tme = vw_bch.getRecord().newFieldInGroup("bch_Bc_Last_Vrfy_Tme", "BC-LAST-VRFY-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_LAST_VRFY_TME");
        bch_Bc_Last_Vrfy_Tme.setDdmHeader("LAST/VRFY/TIME");
        bch_Bc_Last_Vrfy_Userid = vw_bch.getRecord().newFieldInGroup("bch_Bc_Last_Vrfy_Userid", "BC-LAST-VRFY-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_VRFY_USERID");
        bch_Bc_Last_Vrfy_Userid.setDdmHeader("LAST/VRFY/USERID");
        bch_Bc_Tiaa_Cref_Chng_Dte = vw_bch.getRecord().newFieldInGroup("bch_Bc_Tiaa_Cref_Chng_Dte", "BC-TIAA-CREF-CHNG-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_TIAA_CREF_CHNG_DTE");
        bch_Bc_Tiaa_Cref_Chng_Dte.setDdmHeader("TIAA CREF/CHANGE/DATE");
        bch_Bc_Tiaa_Cref_Chng_Tme = vw_bch.getRecord().newFieldInGroup("bch_Bc_Tiaa_Cref_Chng_Tme", "BC-TIAA-CREF-CHNG-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_TIAA_CREF_CHNG_TME");
        bch_Bc_Tiaa_Cref_Chng_Tme.setDdmHeader("TIAA CREF/CHANGE/TIME");
        bch_Bc_Chng_Pwr_Atty = vw_bch.getRecord().newFieldInGroup("bch_Bc_Chng_Pwr_Atty", "BC-CHNG-PWR-ATTY", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_CHNG_PWR_ATTY");
        bch_Bc_Chng_Pwr_Atty.setDdmHeader("CHANGE/POWER/ATTNY");
        registerRecord(vw_bch);

        vw_bm = new DataAccessProgramView(new NameInfo("vw_bm", "BM"), "BENE_MOS_12", "BENE_MOS", DdmPeriodicGroups.getInstance().getGroups("BENE_MOS_12"));
        bm_Bm_Pin = vw_bm.getRecord().newFieldInGroup("bm_Bm_Pin", "BM-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BM_PIN");
        bm_Bm_Pin.setDdmHeader("PIN");
        bm_Bm_Tiaa_Cntrct = vw_bm.getRecord().newFieldInGroup("bm_Bm_Tiaa_Cntrct", "BM-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BM_TIAA_CNTRCT");
        bm_Bm_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bm_Bm_Cref_Cntrct = vw_bm.getRecord().newFieldInGroup("bm_Bm_Cref_Cntrct", "BM-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BM_CREF_CNTRCT");
        bm_Bm_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bm_Bm_Tiaa_Cref_Ind = vw_bm.getRecord().newFieldInGroup("bm_Bm_Tiaa_Cref_Ind", "BM-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BM_TIAA_CREF_IND");
        bm_Bm_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bm_Bm_Stat = vw_bm.getRecord().newFieldInGroup("bm_Bm_Stat", "BM-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BM_STAT");
        bm_Bm_Stat.setDdmHeader("STAT");
        bm_Bm_Rqst_Timestamp = vw_bm.getRecord().newFieldInGroup("bm_Bm_Rqst_Timestamp", "BM-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BM_RQST_TIMESTAMP");
        bm_Bm_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        bm_Bm_Rcrd_Last_Updt_Dte = vw_bm.getRecord().newFieldInGroup("bm_Bm_Rcrd_Last_Updt_Dte", "BM-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BM_RCRD_LAST_UPDT_DTE");
        bm_Bm_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bm_Bm_Rcrd_Last_Updt_Tme = vw_bm.getRecord().newFieldInGroup("bm_Bm_Rcrd_Last_Updt_Tme", "BM-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BM_RCRD_LAST_UPDT_TME");
        bm_Bm_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bm_Bm_Rcrd_Last_Updt_Userid = vw_bm.getRecord().newFieldInGroup("bm_Bm_Rcrd_Last_Updt_Userid", "BM-RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "BM_RCRD_LAST_UPDT_USERID");
        bm_Bm_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        bm_Count_Castbm_Mos_Txt_Group = vw_bm.getRecord().newFieldInGroup("bm_Count_Castbm_Mos_Txt_Group", "C*BM-MOS-TXT-GROUP", RepeatingFieldStrategy.CAsteriskVariable, 
            "BENE_MOS_BM_MOS_TXT_GROUP");

        bm_Bm_Mos_Txt_Group = vw_bm.getRecord().newGroupArrayInGroup("bm_Bm_Mos_Txt_Group", "BM-MOS-TXT-GROUP", new DbsArrayController(1, 60) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "BENE_MOS_BM_MOS_TXT_GROUP");
        bm_Bm_Mos_Txt = bm_Bm_Mos_Txt_Group.newFieldInGroup("bm_Bm_Mos_Txt", "BM-MOS-TXT", FieldType.STRING, 72, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "BM_MOS_TXT", "BENE_MOS_BM_MOS_TXT_GROUP");
        bm_Bm_Mos_Txt.setDdmHeader("MOS TEXT");
        registerRecord(vw_bm);

        vw_bmh = new DataAccessProgramView(new NameInfo("vw_bmh", "BMH"), "BENE_MOS_HIST_12", "BENE_MOS_HIST", DdmPeriodicGroups.getInstance().getGroups("BENE_MOS_HIST_12"));
        bmh_Bm_Pin = vw_bmh.getRecord().newFieldInGroup("bmh_Bm_Pin", "BM-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BM_PIN");
        bmh_Bm_Pin.setDdmHeader("PIN");
        bmh_Bm_Tiaa_Cntrct = vw_bmh.getRecord().newFieldInGroup("bmh_Bm_Tiaa_Cntrct", "BM-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BM_TIAA_CNTRCT");
        bmh_Bm_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bmh_Bm_Cref_Cntrct = vw_bmh.getRecord().newFieldInGroup("bmh_Bm_Cref_Cntrct", "BM-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BM_CREF_CNTRCT");
        bmh_Bm_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bmh_Bm_Tiaa_Cref_Ind = vw_bmh.getRecord().newFieldInGroup("bmh_Bm_Tiaa_Cref_Ind", "BM-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BM_TIAA_CREF_IND");
        bmh_Bm_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bmh_Bm_Stat = vw_bmh.getRecord().newFieldInGroup("bmh_Bm_Stat", "BM-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BM_STAT");
        bmh_Bm_Stat.setDdmHeader("STAT");
        bmh_Bm_Rqst_Timestamp = vw_bmh.getRecord().newFieldInGroup("bmh_Bm_Rqst_Timestamp", "BM-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BM_RQST_TIMESTAMP");
        bmh_Bm_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        bmh_Bm_Rcrd_Last_Updt_Dte = vw_bmh.getRecord().newFieldInGroup("bmh_Bm_Rcrd_Last_Updt_Dte", "BM-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BM_RCRD_LAST_UPDT_DTE");
        bmh_Bm_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bmh_Bm_Rcrd_Last_Updt_Tme = vw_bmh.getRecord().newFieldInGroup("bmh_Bm_Rcrd_Last_Updt_Tme", "BM-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BM_RCRD_LAST_UPDT_TME");
        bmh_Bm_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bmh_Bm_Rcrd_Last_Updt_Userid = vw_bmh.getRecord().newFieldInGroup("bmh_Bm_Rcrd_Last_Updt_Userid", "BM-RCRD-LAST-UPDT-USERID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "BM_RCRD_LAST_UPDT_USERID");
        bmh_Bm_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        bmh_Count_Castbm_Mos_Txt_Group = vw_bmh.getRecord().newFieldInGroup("bmh_Count_Castbm_Mos_Txt_Group", "C*BM-MOS-TXT-GROUP", RepeatingFieldStrategy.CAsteriskVariable, 
            "BENE_MOS_HIST_BM_MOS_TXT_GROUP");

        bmh_Bm_Mos_Txt_Group = vw_bmh.getRecord().newGroupArrayInGroup("bmh_Bm_Mos_Txt_Group", "BM-MOS-TXT-GROUP", new DbsArrayController(1, 60) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "BENE_MOS_HIST_BM_MOS_TXT_GROUP");
        bmh_Bm_Mos_Txt = bmh_Bm_Mos_Txt_Group.newFieldInGroup("bmh_Bm_Mos_Txt", "BM-MOS-TXT", FieldType.STRING, 72, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "BM_MOS_TXT", "BENE_MOS_HIST_BM_MOS_TXT_GROUP");
        bmh_Bm_Mos_Txt.setDdmHeader("MOS TEXT");
        registerRecord(vw_bmh);

        vw_bd = new DataAccessProgramView(new NameInfo("vw_bd", "BD"), "BENE_DESIGNATION_12", "BENE_DESIGNATION");
        bd_Bd_Pin = vw_bd.getRecord().newFieldInGroup("bd_Bd_Pin", "BD-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BD_PIN");
        bd_Bd_Pin.setDdmHeader("PIN");
        bd_Bd_Tiaa_Cntrct = vw_bd.getRecord().newFieldInGroup("bd_Bd_Tiaa_Cntrct", "BD-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BD_TIAA_CNTRCT");
        bd_Bd_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bd_Bd_Cref_Cntrct = vw_bd.getRecord().newFieldInGroup("bd_Bd_Cref_Cntrct", "BD-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BD_CREF_CNTRCT");
        bd_Bd_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bd_Bd_Tiaa_Cref_Ind = vw_bd.getRecord().newFieldInGroup("bd_Bd_Tiaa_Cref_Ind", "BD-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_TIAA_CREF_IND");
        bd_Bd_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bd_Bd_Stat = vw_bd.getRecord().newFieldInGroup("bd_Bd_Stat", "BD-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_STAT");
        bd_Bd_Stat.setDdmHeader("STAT");
        bd_Bd_Seq_Nmbr = vw_bd.getRecord().newFieldInGroup("bd_Bd_Seq_Nmbr", "BD-SEQ-NMBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "BD_SEQ_NMBR");
        bd_Bd_Seq_Nmbr.setDdmHeader(" SEQ/NMBR");
        bd_Bd_Rqst_Timestamp = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rqst_Timestamp", "BD-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BD_RQST_TIMESTAMP");
        bd_Bd_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        bd_Bd_Rcrd_Last_Updt_Dte = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rcrd_Last_Updt_Dte", "BD-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BD_RCRD_LAST_UPDT_DTE");
        bd_Bd_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bd_Bd_Rcrd_Last_Updt_Tme = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rcrd_Last_Updt_Tme", "BD-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BD_RCRD_LAST_UPDT_TME");
        bd_Bd_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bd_Bd_Rcrd_Last_Updt_Userid = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rcrd_Last_Updt_Userid", "BD-RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "BD_RCRD_LAST_UPDT_USERID");
        bd_Bd_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        bd_Bd_Bene_Type = vw_bd.getRecord().newFieldInGroup("bd_Bd_Bene_Type", "BD-BENE-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_BENE_TYPE");
        bd_Bd_Bene_Type.setDdmHeader("BENE/TYPE");
        bd_Bd_Rltn_Cd = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rltn_Cd", "BD-RLTN-CD", FieldType.STRING, 2, RepeatingFieldStrategy.None, "BD_RLTN_CD");
        bd_Bd_Rltn_Cd.setDdmHeader("RELATION/CODE");
        bd_Bd_Bene_Name1 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Bene_Name1", "BD-BENE-NAME1", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BD_BENE_NAME1");
        bd_Bd_Bene_Name1.setDdmHeader("BENEFICIARY/NAME 1");
        bd_Bd_Bene_Name2 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Bene_Name2", "BD-BENE-NAME2", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BD_BENE_NAME2");
        bd_Bd_Bene_Name2.setDdmHeader("BENEFICIARY/NAME 2");
        bd_Bd_Rltn_Free_Txt = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rltn_Free_Txt", "BD-RLTN-FREE-TXT", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BD_RLTN_FREE_TXT");
        bd_Bd_Rltn_Free_Txt.setDdmHeader("OTHER/RELATION");
        bd_Bd_Dte_Birth_Trust = vw_bd.getRecord().newFieldInGroup("bd_Bd_Dte_Birth_Trust", "BD-DTE-BIRTH-TRUST", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BD_DTE_BIRTH_TRUST");
        bd_Bd_Dte_Birth_Trust.setDdmHeader("DOB/DOT");
        bd_Bd_Ss_Cd = vw_bd.getRecord().newFieldInGroup("bd_Bd_Ss_Cd", "BD-SS-CD", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_SS_CD");
        bd_Bd_Ss_Cd.setDdmHeader("SS/CODE");
        bd_Bd_Ss_Nmbr = vw_bd.getRecord().newFieldInGroup("bd_Bd_Ss_Nmbr", "BD-SS-NMBR", FieldType.STRING, 9, RepeatingFieldStrategy.None, "BD_SS_NMBR");
        bd_Bd_Ss_Nmbr.setDdmHeader("SS/NUMBER");
        bd_Bd_Perc_Share_Ind = vw_bd.getRecord().newFieldInGroup("bd_Bd_Perc_Share_Ind", "BD-PERC-SHARE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_PERC_SHARE_IND");
        bd_Bd_Perc_Share_Ind.setDdmHeader("PERCNT/SHARE/IND");
        bd_Bd_Share_Perc = vw_bd.getRecord().newFieldInGroup("bd_Bd_Share_Perc", "BD-SHARE-PERC", FieldType.NUMERIC, 5, 2, RepeatingFieldStrategy.None, 
            "BD_SHARE_PERC");
        bd_Bd_Share_Perc.setDdmHeader("SHARE/PERCENT");
        bd_Bd_Share_Ntor = vw_bd.getRecord().newFieldInGroup("bd_Bd_Share_Ntor", "BD-SHARE-NTOR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "BD_SHARE_NTOR");
        bd_Bd_Share_Ntor.setDdmHeader("SHARE/NUMERATOR");
        bd_Bd_Share_Dtor = vw_bd.getRecord().newFieldInGroup("bd_Bd_Share_Dtor", "BD-SHARE-DTOR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "BD_SHARE_DTOR");
        bd_Bd_Share_Dtor.setDdmHeader("SHARE/DENOMINATOR");
        bd_Bd_Irvcbl_Ind = vw_bd.getRecord().newFieldInGroup("bd_Bd_Irvcbl_Ind", "BD-IRVCBL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_IRVCBL_IND");
        bd_Bd_Irvcbl_Ind.setDdmHeader("IRVCBL/IND");
        bd_Bd_Stlmnt_Rstrctn = vw_bd.getRecord().newFieldInGroup("bd_Bd_Stlmnt_Rstrctn", "BD-STLMNT-RSTRCTN", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_STLMNT_RSTRCTN");
        bd_Bd_Stlmnt_Rstrctn.setDdmHeader("SETTLEMENT/RESTRICTION");
        bd_Bd_Spcl_Txt1 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Spcl_Txt1", "BD-SPCL-TXT1", FieldType.STRING, 72, RepeatingFieldStrategy.None, "BD_SPCL_TXT1");
        bd_Bd_Spcl_Txt1.setDdmHeader("SPECIAL TEXT 1");
        bd_Bd_Spcl_Txt2 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Spcl_Txt2", "BD-SPCL-TXT2", FieldType.STRING, 72, RepeatingFieldStrategy.None, "BD_SPCL_TXT2");
        bd_Bd_Spcl_Txt2.setDdmHeader("SPECIAL TEXT 2");
        bd_Bd_Spcl_Txt3 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Spcl_Txt3", "BD-SPCL-TXT3", FieldType.STRING, 72, RepeatingFieldStrategy.None, "BD_SPCL_TXT3");
        bd_Bd_Spcl_Txt3.setDdmHeader("SPECIAL TEXT 3");
        bd_Bd_Dflt_Estate = vw_bd.getRecord().newFieldInGroup("bd_Bd_Dflt_Estate", "BD-DFLT-ESTATE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_DFLT_ESTATE");
        bd_Bd_Dflt_Estate.setDdmHeader("DEFAULT/ESTATE");
        registerRecord(vw_bd);

        vw_bdh = new DataAccessProgramView(new NameInfo("vw_bdh", "BDH"), "BENE_DESIGNATION_HIST_12", "BENE_DESIGN_HIST");
        bdh_Bd_Pin = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Pin", "BD-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BD_PIN");
        bdh_Bd_Pin.setDdmHeader("PIN");
        bdh_Bd_Tiaa_Cntrct = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Tiaa_Cntrct", "BD-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BD_TIAA_CNTRCT");
        bdh_Bd_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bdh_Bd_Cref_Cntrct = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Cref_Cntrct", "BD-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BD_CREF_CNTRCT");
        bdh_Bd_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bdh_Bd_Tiaa_Cref_Ind = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Tiaa_Cref_Ind", "BD-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_TIAA_CREF_IND");
        bdh_Bd_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bdh_Bd_Stat = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Stat", "BD-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_STAT");
        bdh_Bd_Stat.setDdmHeader("STAT");
        bdh_Bd_Seq_Nmbr = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Seq_Nmbr", "BD-SEQ-NMBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "BD_SEQ_NMBR");
        bdh_Bd_Seq_Nmbr.setDdmHeader(" SEQ/NMBR");
        bdh_Bd_Rqst_Timestamp = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Rqst_Timestamp", "BD-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BD_RQST_TIMESTAMP");
        bdh_Bd_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        bdh_Bd_Rcrd_Last_Updt_Dte = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Rcrd_Last_Updt_Dte", "BD-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BD_RCRD_LAST_UPDT_DTE");
        bdh_Bd_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bdh_Bd_Rcrd_Last_Updt_Tme = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Rcrd_Last_Updt_Tme", "BD-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BD_RCRD_LAST_UPDT_TME");
        bdh_Bd_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bdh_Bd_Rcrd_Last_Updt_Userid = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Rcrd_Last_Updt_Userid", "BD-RCRD-LAST-UPDT-USERID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "BD_RCRD_LAST_UPDT_USERID");
        bdh_Bd_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        bdh_Bd_Bene_Type = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Bene_Type", "BD-BENE-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_BENE_TYPE");
        bdh_Bd_Bene_Type.setDdmHeader("BENE/TYPE");
        bdh_Bd_Rltn_Cd = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Rltn_Cd", "BD-RLTN-CD", FieldType.STRING, 2, RepeatingFieldStrategy.None, "BD_RLTN_CD");
        bdh_Bd_Rltn_Cd.setDdmHeader("RELATION/CODE");
        bdh_Bd_Bene_Name1 = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Bene_Name1", "BD-BENE-NAME1", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "BD_BENE_NAME1");
        bdh_Bd_Bene_Name1.setDdmHeader("BENEFICIARY/NAME 1");
        bdh_Bd_Bene_Name2 = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Bene_Name2", "BD-BENE-NAME2", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "BD_BENE_NAME2");
        bdh_Bd_Bene_Name2.setDdmHeader("BENEFICIARY/NAME 2");
        bdh_Bd_Rltn_Free_Txt = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Rltn_Free_Txt", "BD-RLTN-FREE-TXT", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BD_RLTN_FREE_TXT");
        bdh_Bd_Rltn_Free_Txt.setDdmHeader("OTHER/RELATION");
        bdh_Bd_Dte_Birth_Trust = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Dte_Birth_Trust", "BD-DTE-BIRTH-TRUST", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BD_DTE_BIRTH_TRUST");
        bdh_Bd_Dte_Birth_Trust.setDdmHeader("DOB/DOT");
        bdh_Bd_Ss_Cd = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Ss_Cd", "BD-SS-CD", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_SS_CD");
        bdh_Bd_Ss_Cd.setDdmHeader("SS/CODE");
        bdh_Bd_Ss_Nmbr = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Ss_Nmbr", "BD-SS-NMBR", FieldType.STRING, 9, RepeatingFieldStrategy.None, "BD_SS_NMBR");
        bdh_Bd_Ss_Nmbr.setDdmHeader("SS/NUMBER");
        bdh_Bd_Perc_Share_Ind = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Perc_Share_Ind", "BD-PERC-SHARE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_PERC_SHARE_IND");
        bdh_Bd_Perc_Share_Ind.setDdmHeader("PERCNT/SHARE/IND");
        bdh_Bd_Share_Perc = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Share_Perc", "BD-SHARE-PERC", FieldType.NUMERIC, 5, 2, RepeatingFieldStrategy.None, 
            "BD_SHARE_PERC");
        bdh_Bd_Share_Perc.setDdmHeader("SHARE/PERCENT");
        bdh_Bd_Share_Ntor = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Share_Ntor", "BD-SHARE-NTOR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "BD_SHARE_NTOR");
        bdh_Bd_Share_Ntor.setDdmHeader("SHARE/NUMERATOR");
        bdh_Bd_Share_Dtor = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Share_Dtor", "BD-SHARE-DTOR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "BD_SHARE_DTOR");
        bdh_Bd_Share_Dtor.setDdmHeader("SHARE/DENOMINATOR");
        bdh_Bd_Irvcbl_Ind = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Irvcbl_Ind", "BD-IRVCBL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_IRVCBL_IND");
        bdh_Bd_Irvcbl_Ind.setDdmHeader("IRVCBL/IND");
        bdh_Bd_Stlmnt_Rstrctn = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Stlmnt_Rstrctn", "BD-STLMNT-RSTRCTN", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_STLMNT_RSTRCTN");
        bdh_Bd_Stlmnt_Rstrctn.setDdmHeader("SETTLEMENT/RESTRICTION");
        bdh_Bd_Spcl_Txt1 = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Spcl_Txt1", "BD-SPCL-TXT1", FieldType.STRING, 72, RepeatingFieldStrategy.None, "BD_SPCL_TXT1");
        bdh_Bd_Spcl_Txt1.setDdmHeader("SPECIAL TEXT 1");
        bdh_Bd_Spcl_Txt2 = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Spcl_Txt2", "BD-SPCL-TXT2", FieldType.STRING, 72, RepeatingFieldStrategy.None, "BD_SPCL_TXT2");
        bdh_Bd_Spcl_Txt2.setDdmHeader("SPECIAL TEXT 2");
        bdh_Bd_Spcl_Txt3 = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Spcl_Txt3", "BD-SPCL-TXT3", FieldType.STRING, 72, RepeatingFieldStrategy.None, "BD_SPCL_TXT3");
        bdh_Bd_Spcl_Txt3.setDdmHeader("SPECIAL TEXT 3");
        bdh_Bd_Dflt_Estate = vw_bdh.getRecord().newFieldInGroup("bdh_Bd_Dflt_Estate", "BD-DFLT-ESTATE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_DFLT_ESTATE");
        bdh_Bd_Dflt_Estate.setDdmHeader("DEFAULT/ESTATE");
        registerRecord(vw_bdh);

        vw_bt = new DataAccessProgramView(new NameInfo("vw_bt", "BT"), "BENE_TABLE_FILE", "BENE_TABLE");
        bt_Bt_Table_Id = vw_bt.getRecord().newFieldInGroup("bt_Bt_Table_Id", "BT-TABLE-ID", FieldType.STRING, 2, RepeatingFieldStrategy.None, "BT_TABLE_ID");
        bt_Bt_Table_Id.setDdmHeader("TABLE/ID");
        bt_Bt_Table_Key = vw_bt.getRecord().newFieldInGroup("bt_Bt_Table_Key", "BT-TABLE-KEY", FieldType.STRING, 20, RepeatingFieldStrategy.None, "BT_TABLE_KEY");
        bt_Bt_Table_Key.setDdmHeader("TABLE/KEY");
        bt_Bt_Table_Text = vw_bt.getRecord().newFieldInGroup("bt_Bt_Table_Text", "BT-TABLE-TEXT", FieldType.STRING, 72, RepeatingFieldStrategy.None, "BT_TABLE_TEXT");
        bt_Bt_Table_Text.setDdmHeader("TABLE/TEXT");

        bt__R_Field_3 = vw_bt.getRecord().newGroupInGroup("bt__R_Field_3", "REDEFINE", bt_Bt_Table_Text);
        bt_Bt_Auto_Verify = bt__R_Field_3.newFieldInGroup("bt_Bt_Auto_Verify", "BT-AUTO-VERIFY", FieldType.STRING, 8);
        registerRecord(vw_bt);

        pnd_Stat_Pin_Cntrct_Ind_Key = localVariables.newFieldInRecord("pnd_Stat_Pin_Cntrct_Ind_Key", "#STAT-PIN-CNTRCT-IND-KEY", FieldType.STRING, 24);

        pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_4", "REDEFINE", pnd_Stat_Pin_Cntrct_Ind_Key);
        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Bc_Stat = pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_4.newFieldInGroup("pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Bc_Stat", "#BC-STAT", 
            FieldType.STRING, 1);
        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Bc_Pin_Cntrct_Ind_Key = pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_4.newFieldInGroup("pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Bc_Pin_Cntrct_Ind_Key", 
            "#BC-PIN-CNTRCT-IND-KEY", FieldType.STRING, 23);

        pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_5 = pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_4.newGroupInGroup("pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_5", "REDEFINE", 
            pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Bc_Pin_Cntrct_Ind_Key);
        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Bc_Pin = pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_5.newFieldInGroup("pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Bc_Pin", "#BC-PIN", 
            FieldType.NUMERIC, 12);
        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Bc_Tiaa_Cntrct = pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_5.newFieldInGroup("pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Bc_Tiaa_Cntrct", 
            "#BC-TIAA-CNTRCT", FieldType.STRING, 10);
        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Bc_Tiaa_Cref_Ind = pnd_Stat_Pin_Cntrct_Ind_Key__R_Field_5.newFieldInGroup("pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Bc_Tiaa_Cref_Ind", 
            "#BC-TIAA-CREF-IND", FieldType.STRING, 1);
        pnd_Ba_Orig_Updt_Userid_Dte = localVariables.newFieldInRecord("pnd_Ba_Orig_Updt_Userid_Dte", "#BA-ORIG-UPDT-USERID-DTE", FieldType.STRING, 19);

        pnd_Ba_Orig_Updt_Userid_Dte__R_Field_6 = localVariables.newGroupInRecord("pnd_Ba_Orig_Updt_Userid_Dte__R_Field_6", "REDEFINE", pnd_Ba_Orig_Updt_Userid_Dte);
        pnd_Ba_Orig_Updt_Userid_Dte_Pnd_Ba_Orig_Updt_Userid = pnd_Ba_Orig_Updt_Userid_Dte__R_Field_6.newFieldInGroup("pnd_Ba_Orig_Updt_Userid_Dte_Pnd_Ba_Orig_Updt_Userid", 
            "#BA-ORIG-UPDT-USERID", FieldType.STRING, 8);
        pnd_Ba_Orig_Updt_Userid_Dte_Pnd_Ba_Vrfy_Ind = pnd_Ba_Orig_Updt_Userid_Dte__R_Field_6.newFieldInGroup("pnd_Ba_Orig_Updt_Userid_Dte_Pnd_Ba_Vrfy_Ind", 
            "#BA-VRFY-IND", FieldType.STRING, 1);
        pnd_Ba_Orig_Updt_Userid_Dte_Pnd_Ba_Orig_Updt_Dte = pnd_Ba_Orig_Updt_Userid_Dte__R_Field_6.newFieldInGroup("pnd_Ba_Orig_Updt_Userid_Dte_Pnd_Ba_Orig_Updt_Dte", 
            "#BA-ORIG-UPDT-DTE", FieldType.STRING, 8);
        pnd_Ba_Orig_Updt_Userid_Dte_Pnd_Ba_Seq_Nmbr = pnd_Ba_Orig_Updt_Userid_Dte__R_Field_6.newFieldInGroup("pnd_Ba_Orig_Updt_Userid_Dte_Pnd_Ba_Seq_Nmbr", 
            "#BA-SEQ-NMBR", FieldType.NUMERIC, 2);
        pnd_Bt_Table_Id_Key = localVariables.newFieldInRecord("pnd_Bt_Table_Id_Key", "#BT-TABLE-ID-KEY", FieldType.STRING, 22);

        pnd_Bt_Table_Id_Key__R_Field_7 = localVariables.newGroupInRecord("pnd_Bt_Table_Id_Key__R_Field_7", "REDEFINE", pnd_Bt_Table_Id_Key);
        pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Id = pnd_Bt_Table_Id_Key__R_Field_7.newFieldInGroup("pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Id", "#BT-TABLE-ID", FieldType.STRING, 
            2);
        pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Key = pnd_Bt_Table_Id_Key__R_Field_7.newFieldInGroup("pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Key", "#BT-TABLE-KEY", 
            FieldType.STRING, 20);
        pnd_Auto_Verify_To = localVariables.newFieldInRecord("pnd_Auto_Verify_To", "#AUTO-VERIFY-TO", FieldType.STRING, 8);
        pnd_Auto_Verify_From = localVariables.newFieldInRecord("pnd_Auto_Verify_From", "#AUTO-VERIFY-FROM", FieldType.STRING, 8);
        pnd_Ba_Isn = localVariables.newFieldInRecord("pnd_Ba_Isn", "#BA-ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Bc_Isn = localVariables.newFieldInRecord("pnd_Bc_Isn", "#BC-ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Bd_Isn = localVariables.newFieldInRecord("pnd_Bd_Isn", "#BD-ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Bm_Isn = localVariables.newFieldInRecord("pnd_Bm_Isn", "#BM-ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.DATE);
        pnd_Datn = localVariables.newFieldInRecord("pnd_Datn", "#DATN", FieldType.STRING, 8);

        pnd_Details = localVariables.newGroupInRecord("pnd_Details", "#DETAILS");
        pnd_Details_Pnd_Add_Hist = pnd_Details.newFieldInGroup("pnd_Details_Pnd_Add_Hist", "#ADD-HIST", FieldType.BOOLEAN, 1);
        pnd_Details_Pnd_Dsgntn = pnd_Details.newFieldInGroup("pnd_Details_Pnd_Dsgntn", "#DSGNTN", FieldType.BOOLEAN, 1);
        pnd_Details_Pnd_Dlt_Old = pnd_Details.newFieldInGroup("pnd_Details_Pnd_Dlt_Old", "#DLT-OLD", FieldType.BOOLEAN, 1);
        pnd_Details_Pnd_Chg_Stat = pnd_Details.newFieldInGroup("pnd_Details_Pnd_Chg_Stat", "#CHG-STAT", FieldType.BOOLEAN, 1);
        pnd_Details_Pnd_Msg = pnd_Details.newFieldInGroup("pnd_Details_Pnd_Msg", "#MSG", FieldType.STRING, 35);
        pnd_Details_Pnd_Verified = pnd_Details.newFieldInGroup("pnd_Details_Pnd_Verified", "#VERIFIED", FieldType.STRING, 10);
        pnd_Details_Pnd_X = pnd_Details.newFieldInGroup("pnd_Details_Pnd_X", "#X", FieldType.NUMERIC, 3);
        pnd_Timn = localVariables.newFieldInRecord("pnd_Timn", "#TIMN", FieldType.STRING, 7);
        pnd_Total_Auto_Verify = localVariables.newFieldInRecord("pnd_Total_Auto_Verify", "#TOTAL-AUTO-VERIFY", FieldType.PACKED_DECIMAL, 10);
        pnd_Total_Ba_Read = localVariables.newFieldInRecord("pnd_Total_Ba_Read", "#TOTAL-BA-READ", FieldType.PACKED_DECIMAL, 10);
        pnd_Total_Ba_Error = localVariables.newFieldInRecord("pnd_Total_Ba_Error", "#TOTAL-BA-ERROR", FieldType.PACKED_DECIMAL, 10);
        pls_Trace = WsIndependent.getInstance().newFieldInRecord("pls_Trace", "+TRACE", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ba.reset();
        vw_bc.reset();
        vw_bch.reset();
        vw_bm.reset();
        vw_bmh.reset();
        vw_bd.reset();
        vw_bdh.reset();
        vw_bt.reset();

        localVariables.reset();
        pnd_Bt_Table_Id_Key.setInitialValue("ZZAUTO-VERIFY");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Benp190() throws Exception
    {
        super("Benp190");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) SG = OFF ZP = ON LS = 133 PS = 60;//Natural: FORMAT ( 1 ) SG = OFF ZP = ON LS = 133 PS = 60;//Natural: FORMAT ( 2 ) SG = OFF ZP = ON LS = 133 PS = 60
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        pnd_Auto_Verify_From.setValue(Global.getDATN());                                                                                                                  //Natural: ASSIGN #AUTO-VERIFY-FROM := *DATN
        vw_bt.startDatabaseFind                                                                                                                                           //Natural: FIND BT WITH BT-TABLE-ID-KEY = #BT-TABLE-ID-KEY
        (
        "FBT1",
        new Wc[] { new Wc("BT_TABLE_ID_KEY", "=", pnd_Bt_Table_Id_Key, WcType.WITH) }
        );
        FBT1:
        while (condition(vw_bt.readNextRow("FBT1")))
        {
            vw_bt.setIfNotFoundControlFlag(false);
            pnd_Auto_Verify_From.setValue(bt_Bt_Auto_Verify);                                                                                                             //Natural: ASSIGN #AUTO-VERIFY-FROM := BT.BT-AUTO-VERIFY
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  5=VERIFY WEB
        getReports().write(0, "Current ZZ Auto-Verify Date :",pnd_Auto_Verify_From);                                                                                      //Natural: WRITE ( 0 ) 'Current ZZ Auto-Verify Date :' #AUTO-VERIFY-FROM
        if (Global.isEscape()) return;
        pnd_Ba_Orig_Updt_Userid_Dte_Pnd_Ba_Orig_Updt_Userid.setValue("WEB-E");                                                                                            //Natural: ASSIGN #BA-ORIG-UPDT-USERID := 'WEB-E'
        pnd_Ba_Orig_Updt_Userid_Dte_Pnd_Ba_Vrfy_Ind.setValue("5");                                                                                                        //Natural: ASSIGN #BA-VRFY-IND := '5'
        pnd_Ba_Orig_Updt_Userid_Dte_Pnd_Ba_Orig_Updt_Dte.setValue(pnd_Auto_Verify_From);                                                                                  //Natural: ASSIGN #BA-ORIG-UPDT-DTE := #AUTO-VERIFY-FROM
        pnd_Ba_Orig_Updt_Userid_Dte_Pnd_Ba_Seq_Nmbr.setValue(0);                                                                                                          //Natural: ASSIGN #BA-SEQ-NMBR := 00
        vw_ba.startDatabaseRead                                                                                                                                           //Natural: READ BA BY BA-ORIG-UPDT-USERID-DTE STARTING FROM #BA-ORIG-UPDT-USERID-DTE
        (
        "RBA",
        new Wc[] { new Wc("BA_ORIG_UPDT_USERID_DTE", ">=", pnd_Ba_Orig_Updt_Userid_Dte, WcType.BY) },
        new Oc[] { new Oc("BA_ORIG_UPDT_USERID_DTE", "ASC") }
        );
        RBA:
        while (condition(vw_ba.readNextRow("RBA")))
        {
            if (condition(ba_Ba_Orig_Updt_Userid.notEquals(pnd_Ba_Orig_Updt_Userid_Dte_Pnd_Ba_Orig_Updt_Userid) || ba_Ba_Vrfy_Ind.notEquals(pnd_Ba_Orig_Updt_Userid_Dte_Pnd_Ba_Vrfy_Ind))) //Natural: IF BA-ORIG-UPDT-USERID NE #BA-ORIG-UPDT-USERID OR BA-VRFY-IND NE #BA-VRFY-IND
            {
                if (true) break RBA;                                                                                                                                      //Natural: ESCAPE BOTTOM ( RBA. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(DbsUtil.maskMatches(ba_Ba_Wpid,"'TI MB'.") || ba_Ba_Wpid_Unit.equals("IIPLS") || ba_Ba_Wpid_Unit.equals("IIUND")))                              //Natural: REJECT IF BA-WPID = MASK ( 'TI MB'. ) OR BA-WPID-UNIT = 'IIPLS' OR = 'IIUND'
            {
                continue;
            }
            if (condition(pls_Trace.getBoolean()))                                                                                                                        //Natural: IF +TRACE
            {
                getReports().write(0, "PIN",ba_Ba_Pin,"BA TIAA",ba_Ba_Slctd_Tiaa.getValue(1),"RLDT",ba_Ba_Rqst_Timestamp);                                                //Natural: WRITE 'PIN' BA-PIN 'BA TIAA' BA-SLCTD-TIAA ( 1 ) 'RLDT' BA-RQST-TIMESTAMP
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RBA"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RBA"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ba_Isn.setValue(vw_ba.getAstISN("RBA"));                                                                                                                  //Natural: ASSIGN #BA-ISN := *ISN ( RBA. )
            if (condition(ba_Ba_Orig_Updt_Dte.greater(pnd_Auto_Verify_To)))                                                                                               //Natural: IF BA-ORIG-UPDT-DTE > #AUTO-VERIFY-TO
            {
                pnd_Auto_Verify_To.setValue(ba_Ba_Orig_Updt_Dte);                                                                                                         //Natural: ASSIGN #AUTO-VERIFY-TO := BA-ORIG-UPDT-DTE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Total_Ba_Read.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #TOTAL-BA-READ
            pnd_Details.reset();                                                                                                                                          //Natural: RESET #DETAILS
            pnd_Datn.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                                     //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO #DATN
            pnd_Timn.setValueEdited(Global.getTIMX(),new ReportEditMask("HHIISST"));                                                                                      //Natural: MOVE EDITED *TIMX ( EM = HHIISST ) TO #TIMN
            short decideConditionsMet340 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN BA-RQST-TIMESTAMP = ' '
            if (condition(ba_Ba_Rqst_Timestamp.equals(" ")))
            {
                decideConditionsMet340++;
                pnd_Details_Pnd_Msg.setValue("No Request Log Date Time");                                                                                                 //Natural: ASSIGN #MSG := 'No Request Log Date Time'
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet340 > 0))
            {
                                                                                                                                                                          //Natural: PERFORM PRINT-ERROR-MESSAGE
                sub_Print_Error_Message();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RBA"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RBA"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
                                                                                                                                                                          //Natural: PERFORM DLT-OLD-BENE-ADD-HIST
            sub_Dlt_Old_Bene_Add_Hist();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RBA"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RBA"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Details_Pnd_Dlt_Old.getBoolean() && ! (pnd_Details_Pnd_Add_Hist.getBoolean())))                                                             //Natural: IF #DLT-OLD AND NOT #ADD-HIST
            {
                                                                                                                                                                          //Natural: PERFORM PRINT-ERROR-MESSAGE
                sub_Print_Error_Message();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RBA"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RBA"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM AUTO-VERIFY-WEB-E
            sub_Auto_Verify_Web_E();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RBA"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RBA"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Details_Pnd_Chg_Stat.getBoolean()))                                                                                                         //Natural: IF #CHG-STAT
            {
                                                                                                                                                                          //Natural: PERFORM UPDATE-BENE-ACTIVITY
                sub_Update_Bene_Activity();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RBA"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RBA"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM UPDATE-CWF-STATUS
                sub_Update_Cwf_Status();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RBA"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RBA"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Details_Pnd_Msg.setValue("For Manual Verify. Auto-Verify failed.");                                                                                   //Natural: ASSIGN #MSG := 'For Manual Verify. Auto-Verify failed.'
                                                                                                                                                                          //Natural: PERFORM PRINT-ERROR-MESSAGE
                sub_Print_Error_Message();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RBA"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RBA"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Auto_Verify_To.notEquals(" ")))                                                                                                                 //Natural: IF #AUTO-VERIFY-TO NE ' '
        {
            pnd_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Auto_Verify_To);                                                                                   //Natural: MOVE EDITED #AUTO-VERIFY-TO TO #DATE ( EM = YYYYMMDD )
            pnd_Auto_Verify_To.setValueEdited(pnd_Date,new ReportEditMask("YYYYMMDD"));                                                                                   //Natural: MOVE EDITED #DATE ( EM = YYYYMMDD ) TO #AUTO-VERIFY-TO
            vw_bt.startDatabaseFind                                                                                                                                       //Natural: FIND BT WITH BT-TABLE-ID-KEY = #BT-TABLE-ID-KEY
            (
            "FBT2",
            new Wc[] { new Wc("BT_TABLE_ID_KEY", "=", pnd_Bt_Table_Id_Key, WcType.WITH) }
            );
            FBT2:
            while (condition(vw_bt.readNextRow("FBT2")))
            {
                vw_bt.setIfNotFoundControlFlag(false);
                bt_Bt_Auto_Verify.setValue(pnd_Auto_Verify_To);                                                                                                           //Natural: ASSIGN BT.BT-AUTO-VERIFY := #AUTO-VERIFY-TO
                vw_bt.updateDBRow("FBT2");                                                                                                                                //Natural: UPDATE ( FBT2. )
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, "Next ZZ Auto-Verify Date :",pnd_Auto_Verify_To);                                                                                           //Natural: WRITE ( 0 ) 'Next ZZ Auto-Verify Date :' #AUTO-VERIFY-TO
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Total WEB-E records waiting verification :",pnd_Total_Ba_Read, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));                 //Natural: WRITE ( 1 ) 'Total WEB-E records waiting verification :' #TOTAL-BA-READ
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Total WEB-E records with Errors          :",pnd_Total_Ba_Error, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));                //Natural: WRITE ( 1 ) 'Total WEB-E records with Errors          :' #TOTAL-BA-ERROR
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Total WEB-E records Auto-Verified        :",pnd_Total_Auto_Verify, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));             //Natural: WRITE ( 1 ) 'Total WEB-E records Auto-Verified        :' #TOTAL-AUTO-VERIFY
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: AUTO-VERIFY-WEB-E
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DLT-OLD-BENE-ADD-HIST
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-ERROR-MESSAGE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-BENE-ACTIVITY
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-CWF-STATUS
    }
    private void sub_Auto_Verify_Web_E() throws Exception                                                                                                                 //Natural: AUTO-VERIFY-WEB-E
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, "SUBROUTINE AUTO-VERIFY-WEB-E");                                                                                                        //Natural: WRITE 'SUBROUTINE AUTO-VERIFY-WEB-E'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        pnd_Stat_Pin_Cntrct_Ind_Key.reset();                                                                                                                              //Natural: RESET #STAT-PIN-CNTRCT-IND-KEY
        pnd_Details_Pnd_Msg.setValue("No Bene Contract Waiting Verify");                                                                                                  //Natural: ASSIGN #MSG := 'No Bene Contract Waiting Verify'
        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Bc_Pin.setValue(ba_Ba_Pin);                                                                                                       //Natural: ASSIGN #BC-PIN := BA-PIN
        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Bc_Tiaa_Cntrct.setValue(ba_Ba_Slctd_Tiaa.getValue(1));                                                                            //Natural: ASSIGN #BC-TIAA-CNTRCT := BA-SLCTD-TIAA ( 1 )
        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Bc_Tiaa_Cref_Ind.setValue(" ");                                                                                                   //Natural: ASSIGN #BC-TIAA-CREF-IND := ' '
        vw_bc.startDatabaseRead                                                                                                                                           //Natural: READ BC BY BC-PIN-CNTRCT-IND-KEY FROM #BC-PIN-CNTRCT-IND-KEY
        (
        "RBC1",
        new Wc[] { new Wc("BC_PIN_CNTRCT_IND_KEY", ">=", pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Bc_Pin_Cntrct_Ind_Key, WcType.BY) },
        new Oc[] { new Oc("BC_PIN_CNTRCT_IND_KEY", "ASC") }
        );
        RBC1:
        while (condition(vw_bc.readNextRow("RBC1")))
        {
            if (condition(bc_Bc_Pin.notEquals(pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Bc_Pin) || bc_Bc_Tiaa_Cntrct.notEquals(pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Bc_Tiaa_Cntrct)))    //Natural: IF BC.BC-PIN NE #BC-PIN OR BC.BC-TIAA-CNTRCT NE #BC-TIAA-CNTRCT
            {
                if (true) break RBC1;                                                                                                                                     //Natural: ESCAPE BOTTOM ( RBC1. )
            }                                                                                                                                                             //Natural: END-IF
            //*  REQUIRES VERIFICATION
            if (condition(!(bc_Bc_Stat.equals("R"))))                                                                                                                     //Natural: ACCEPT IF BC.BC-STAT = 'R'
            {
                continue;
            }
            pnd_Details_Pnd_Chg_Stat.reset();                                                                                                                             //Natural: RESET #CHG-STAT #DSGNTN #MSG #BC-ISN #BD-ISN #BM-ISN
            pnd_Details_Pnd_Dsgntn.reset();
            pnd_Details_Pnd_Msg.reset();
            pnd_Bc_Isn.reset();
            pnd_Bd_Isn.reset();
            pnd_Bm_Isn.reset();
            pnd_Bc_Isn.setValue(vw_bc.getAstISN("RBC1"));                                                                                                                 //Natural: ASSIGN #BC-ISN := *ISN ( RBC1. )
            pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Bc_Stat.setValue(bc_Bc_Stat);                                                                                                 //Natural: ASSIGN #BC-STAT := BC.BC-STAT
            pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Bc_Tiaa_Cref_Ind.setValue(bc_Bc_Tiaa_Cref_Ind);                                                                               //Natural: ASSIGN #BC-TIAA-CREF-IND := BC.BC-TIAA-CREF-IND
            if (condition(bc_Bc_Mos_Ind.equals("Y")))                                                                                                                     //Natural: IF BC.BC-MOS-IND = 'Y'
            {
                vw_bm.startDatabaseFind                                                                                                                                   //Natural: FIND BM WITH BM-STAT-PIN-CNTRCT-IND-KEY = #STAT-PIN-CNTRCT-IND-KEY
                (
                "FBM1",
                new Wc[] { new Wc("BM_STAT_PIN_CNTRCT_IND_KEY", "=", pnd_Stat_Pin_Cntrct_Ind_Key, WcType.WITH) }
                );
                FBM1:
                while (condition(vw_bm.readNextRow("FBM1", true)))
                {
                    vw_bm.setIfNotFoundControlFlag(false);
                    if (condition(vw_bm.getAstCOUNTER().equals(0)))                                                                                                       //Natural: IF NO RECORDS FOUND
                    {
                        if (true) break FBM1;                                                                                                                             //Natural: ESCAPE BOTTOM ( FBM1. )
                        //*  UPDATE TO 'C'URRENT
                    }                                                                                                                                                     //Natural: END-NOREC
                    pnd_Bm_Isn.setValue(vw_bm.getAstISN("FBM1"));                                                                                                         //Natural: ASSIGN #BM-ISN := *ISN ( FBM1. )
                    GBM1:                                                                                                                                                 //Natural: GET BM #BM-ISN
                    vw_bm.readByID(pnd_Bm_Isn.getLong(), "GBM1");
                    bm_Bm_Stat.setValue("C");                                                                                                                             //Natural: ASSIGN BM.BM-STAT := 'C'
                    bm_Bm_Rcrd_Last_Updt_Dte.setValue(pnd_Datn);                                                                                                          //Natural: ASSIGN BM.BM-RCRD-LAST-UPDT-DTE := #DATN
                    bm_Bm_Rcrd_Last_Updt_Tme.setValue(pnd_Timn);                                                                                                          //Natural: ASSIGN BM.BM-RCRD-LAST-UPDT-TME := #TIMN
                    bm_Bm_Rcrd_Last_Updt_Userid.setValue(Global.getPROGRAM());                                                                                            //Natural: ASSIGN BM.BM-RCRD-LAST-UPDT-USERID := *PROGRAM
                    if (condition(pls_Trace.getBoolean()))                                                                                                                //Natural: IF +TRACE
                    {
                        getReports().write(0, "BM stat updte",bm_Bm_Pin,bm_Bm_Tiaa_Cntrct,bm_Bm_Tiaa_Cref_Ind,bm_Bm_Stat,bm_Bm_Rqst_Timestamp);                           //Natural: WRITE ( 0 ) 'BM stat updte' BM.BM-PIN BM.BM-TIAA-CNTRCT BM.BM-TIAA-CREF-IND BM.BM-STAT BM.BM-RQST-TIMESTAMP
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FBM1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FBM1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    vw_bm.updateDBRow("GBM1");                                                                                                                            //Natural: UPDATE ( GBM1. )
                    pnd_Details_Pnd_Dsgntn.setValue(true);                                                                                                                //Natural: ASSIGN #DSGNTN := TRUE
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RBC1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RBC1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                vw_bd.startDatabaseRead                                                                                                                                   //Natural: READ BD BY BD-STAT-PIN-CNTRCT-IND-BENE-KEY STARTING FROM #STAT-PIN-CNTRCT-IND-KEY
                (
                "RBD1",
                new Wc[] { new Wc("BD_STAT_PIN_CNTRCT_IND_BENE_KEY", ">=", pnd_Stat_Pin_Cntrct_Ind_Key, WcType.BY) },
                new Oc[] { new Oc("BD_STAT_PIN_CNTRCT_IND_BENE_KEY", "ASC") }
                );
                RBD1:
                while (condition(vw_bd.readNextRow("RBD1")))
                {
                    if (condition(bd_Bd_Stat.notEquals(pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Bc_Stat) || bd_Bd_Pin.notEquals(pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Bc_Pin)            //Natural: IF BD.BD-STAT NE #BC-STAT OR BD.BD-PIN NE #BC-PIN OR BD.BD-TIAA-CNTRCT NE #BC-TIAA-CNTRCT OR BD.BD-TIAA-CREF-IND NE #BC-TIAA-CREF-IND
                        || bd_Bd_Tiaa_Cntrct.notEquals(pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Bc_Tiaa_Cntrct) || bd_Bd_Tiaa_Cref_Ind.notEquals(pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Bc_Tiaa_Cref_Ind)))
                    {
                        if (true) break RBD1;                                                                                                                             //Natural: ESCAPE BOTTOM ( RBD1. )
                        //*  UPDATE TO 'C'URRENT
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Bd_Isn.setValue(vw_bd.getAstISN("RBD1"));                                                                                                         //Natural: ASSIGN #BD-ISN := *ISN ( RBD1. )
                    GBD1:                                                                                                                                                 //Natural: GET BD #BD-ISN
                    vw_bd.readByID(pnd_Bd_Isn.getLong(), "GBD1");
                    bd_Bd_Stat.setValue("C");                                                                                                                             //Natural: ASSIGN BD.BD-STAT := 'C'
                    bd_Bd_Rcrd_Last_Updt_Dte.setValue(pnd_Datn);                                                                                                          //Natural: ASSIGN BD.BD-RCRD-LAST-UPDT-DTE := #DATN
                    bd_Bd_Rcrd_Last_Updt_Tme.setValue(pnd_Timn);                                                                                                          //Natural: ASSIGN BD.BD-RCRD-LAST-UPDT-TME := #TIMN
                    bd_Bd_Rcrd_Last_Updt_Userid.setValue(Global.getPROGRAM());                                                                                            //Natural: ASSIGN BD.BD-RCRD-LAST-UPDT-USERID := *PROGRAM
                    if (condition(pls_Trace.getBoolean()))                                                                                                                //Natural: IF +TRACE
                    {
                        getReports().write(0, "BD stat updte",bd_Bd_Pin,bd_Bd_Tiaa_Cntrct,bd_Bd_Tiaa_Cref_Ind,bd_Bd_Stat,bd_Bd_Rqst_Timestamp);                           //Natural: WRITE ( 0 ) 'BD stat updte' BD.BD-PIN BD.BD-TIAA-CNTRCT BD.BD-TIAA-CREF-IND BD.BD-STAT BD.BD-RQST-TIMESTAMP
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RBD1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RBD1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    vw_bd.updateDBRow("GBD1");                                                                                                                            //Natural: UPDATE ( GBD1. )
                    pnd_Details_Pnd_Dsgntn.setValue(true);                                                                                                                //Natural: ASSIGN #DSGNTN := TRUE
                }                                                                                                                                                         //Natural: END-READ
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RBC1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RBC1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(! (pnd_Details_Pnd_Dsgntn.getBoolean())))                                                                                                       //Natural: IF NOT #DSGNTN
            {
                getCurrentProcessState().getDbConv().dbRollback();                                                                                                        //Natural: BACKOUT TRANSACTION
                pnd_Details_Pnd_Msg.setValue("Change Verification Status failed.");                                                                                       //Natural: ASSIGN #MSG := 'Change Verification Status failed.'
                                                                                                                                                                          //Natural: PERFORM PRINT-ERROR-MESSAGE
                sub_Print_Error_Message();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RBC1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RBC1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  UPDATE TO 'C'URRENT
            }                                                                                                                                                             //Natural: END-IF
            GBC1:                                                                                                                                                         //Natural: GET BC #BC-ISN
            vw_bc.readByID(pnd_Bc_Isn.getLong(), "GBC1");
            bc_Bc_Stat.setValue("C");                                                                                                                                     //Natural: ASSIGN BC.BC-STAT := 'C'
            bc_Bc_Rcrd_Last_Updt_Dte.setValue(pnd_Datn);                                                                                                                  //Natural: ASSIGN BC.BC-RCRD-LAST-UPDT-DTE := #DATN
            bc_Bc_Rcrd_Last_Updt_Tme.setValue(pnd_Timn);                                                                                                                  //Natural: ASSIGN BC.BC-RCRD-LAST-UPDT-TME := #TIMN
            bc_Bc_Rcrd_Last_Updt_Userid.setValue(Global.getPROGRAM());                                                                                                    //Natural: ASSIGN BC.BC-RCRD-LAST-UPDT-USERID := *PROGRAM
            if (condition(pls_Trace.getBoolean()))                                                                                                                        //Natural: IF +TRACE
            {
                getReports().write(0, "BC stat updte",bc_Bc_Pin,bc_Bc_Tiaa_Cntrct,bc_Bc_Tiaa_Cref_Ind,bc_Bc_Stat,bc_Bc_Rqst_Timestamp);                                   //Natural: WRITE ( 0 ) 'BC stat updte' BC.BC-PIN BC.BC-TIAA-CNTRCT BC.BC-TIAA-CREF-IND BC.BC-STAT BC.BC-RQST-TIMESTAMP
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RBC1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RBC1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            vw_bc.updateDBRow("GBC1");                                                                                                                                    //Natural: UPDATE ( GBC1. )
            pnd_Details_Pnd_Chg_Stat.setValue(true);                                                                                                                      //Natural: ASSIGN #CHG-STAT := TRUE
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  AUTO-VERIFY-WEB-E
    }
    private void sub_Dlt_Old_Bene_Add_Hist() throws Exception                                                                                                             //Natural: DLT-OLD-BENE-ADD-HIST
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, "SUBROUTINE DLT-OLD-BENE-ADD-HIST");                                                                                                    //Natural: WRITE 'SUBROUTINE DLT-OLD-BENE-ADD-HIST'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        pnd_Stat_Pin_Cntrct_Ind_Key.reset();                                                                                                                              //Natural: RESET #STAT-PIN-CNTRCT-IND-KEY
        pnd_Details_Pnd_Msg.setValue("No Pending Bene Contract found");                                                                                                   //Natural: ASSIGN #MSG := 'No Pending Bene Contract found'
        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Bc_Pin.setValue(ba_Ba_Pin);                                                                                                       //Natural: ASSIGN #BC-PIN := BA-PIN
        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Bc_Tiaa_Cntrct.setValue(ba_Ba_Slctd_Tiaa.getValue(1));                                                                            //Natural: ASSIGN #BC-TIAA-CNTRCT := BA-SLCTD-TIAA ( 1 )
        pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Bc_Tiaa_Cref_Ind.setValue(" ");                                                                                                   //Natural: ASSIGN #BC-TIAA-CREF-IND := ' '
        vw_bc.startDatabaseRead                                                                                                                                           //Natural: READ BC BY BC-PIN-CNTRCT-IND-KEY FROM #BC-PIN-CNTRCT-IND-KEY
        (
        "RBC2",
        new Wc[] { new Wc("BC_PIN_CNTRCT_IND_KEY", ">=", pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Bc_Pin_Cntrct_Ind_Key, WcType.BY) },
        new Oc[] { new Oc("BC_PIN_CNTRCT_IND_KEY", "ASC") }
        );
        RBC2:
        while (condition(vw_bc.readNextRow("RBC2")))
        {
            if (condition(bc_Bc_Pin.notEquals(pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Bc_Pin) || bc_Bc_Tiaa_Cntrct.notEquals(pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Bc_Tiaa_Cntrct)))    //Natural: IF BC.BC-PIN NE #BC-PIN OR BC.BC-TIAA-CNTRCT NE #BC-TIAA-CNTRCT
            {
                if (true) break RBC2;                                                                                                                                     //Natural: ESCAPE BOTTOM ( RBC2. )
            }                                                                                                                                                             //Natural: END-IF
            //*  PENDING
            if (condition(!(bc_Bc_Stat.equals("P"))))                                                                                                                     //Natural: ACCEPT IF BC.BC-STAT = 'P'
            {
                continue;
            }
            pnd_Details_Pnd_Add_Hist.reset();                                                                                                                             //Natural: RESET #ADD-HIST #DLT-OLD #MSG #BC-ISN #BD-ISN #BM-ISN
            pnd_Details_Pnd_Dlt_Old.reset();
            pnd_Details_Pnd_Msg.reset();
            pnd_Bc_Isn.reset();
            pnd_Bd_Isn.reset();
            pnd_Bm_Isn.reset();
            pnd_Bc_Isn.setValue(vw_bc.getAstISN("RBC2"));                                                                                                                 //Natural: ASSIGN #BC-ISN := *ISN ( RBC2. )
            pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Bc_Stat.setValue(bc_Bc_Stat);                                                                                                 //Natural: ASSIGN #BC-STAT := BC.BC-STAT
            pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Bc_Tiaa_Cref_Ind.setValue(bc_Bc_Tiaa_Cref_Ind);                                                                               //Natural: ASSIGN #BC-TIAA-CREF-IND := BC.BC-TIAA-CREF-IND
            if (condition(bc_Bc_Mos_Ind.equals("Y")))                                                                                                                     //Natural: IF BC.BC-MOS-IND = 'Y'
            {
                vw_bm.startDatabaseFind                                                                                                                                   //Natural: FIND BM WITH BM-STAT-PIN-CNTRCT-IND-KEY = #STAT-PIN-CNTRCT-IND-KEY
                (
                "FBM2",
                new Wc[] { new Wc("BM_STAT_PIN_CNTRCT_IND_KEY", "=", pnd_Stat_Pin_Cntrct_Ind_Key, WcType.WITH) }
                );
                FBM2:
                while (condition(vw_bm.readNextRow("FBM2", true)))
                {
                    vw_bm.setIfNotFoundControlFlag(false);
                    if (condition(vw_bm.getAstCOUNTER().equals(0)))                                                                                                       //Natural: IF NO RECORDS FOUND
                    {
                        if (true) break FBM2;                                                                                                                             //Natural: ESCAPE BOTTOM ( FBM2. )
                    }                                                                                                                                                     //Natural: END-NOREC
                    pnd_Bm_Isn.setValue(vw_bm.getAstISN("FBM2"));                                                                                                         //Natural: ASSIGN #BM-ISN := *ISN ( FBM2. )
                    GBM2:                                                                                                                                                 //Natural: GET BM #BM-ISN
                    vw_bm.readByID(pnd_Bm_Isn.getLong(), "GBM2");
                    vw_bmh.reset();                                                                                                                                       //Natural: RESET BMH
                    vw_bmh.setValuesByName(vw_bm);                                                                                                                        //Natural: MOVE BY NAME BM TO BMH
                    //*      BMH.BM-RCRD-LAST-UPDT-DTE    := #DATN
                    //*      BMH.BM-RCRD-LAST-UPDT-TME    := #TIMN
                    //*      BMH.BM-RCRD-LAST-UPDT-USERID := *PROGRAM
                    if (condition(pls_Trace.getBoolean()))                                                                                                                //Natural: IF +TRACE
                    {
                        getReports().write(0, "BM>BMH delete",bm_Bm_Pin,bm_Bm_Tiaa_Cntrct,bm_Bm_Tiaa_Cref_Ind,bm_Bm_Stat,bm_Bm_Rqst_Timestamp);                           //Natural: WRITE ( 0 ) 'BM>BMH delete' BM.BM-PIN BM.BM-TIAA-CNTRCT BM.BM-TIAA-CREF-IND BM.BM-STAT BM.BM-RQST-TIMESTAMP
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FBM2"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FBM2"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    vw_bmh.insertDBRow();                                                                                                                                 //Natural: STORE BMH
                    vw_bm.deleteDBRow("GBM2");                                                                                                                            //Natural: DELETE ( GBM2. )
                    pnd_Details_Pnd_Dsgntn.setValue(true);                                                                                                                //Natural: ASSIGN #DSGNTN := TRUE
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RBC2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RBC2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                vw_bd.startDatabaseRead                                                                                                                                   //Natural: READ BD BY BD-STAT-PIN-CNTRCT-IND-BENE-KEY STARTING FROM #STAT-PIN-CNTRCT-IND-KEY
                (
                "RBD2",
                new Wc[] { new Wc("BD_STAT_PIN_CNTRCT_IND_BENE_KEY", ">=", pnd_Stat_Pin_Cntrct_Ind_Key, WcType.BY) },
                new Oc[] { new Oc("BD_STAT_PIN_CNTRCT_IND_BENE_KEY", "ASC") }
                );
                RBD2:
                while (condition(vw_bd.readNextRow("RBD2")))
                {
                    if (condition(bd_Bd_Stat.notEquals(pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Bc_Stat) || bd_Bd_Pin.notEquals(pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Bc_Pin)            //Natural: IF BD.BD-STAT NE #BC-STAT OR BD.BD-PIN NE #BC-PIN OR BD.BD-TIAA-CNTRCT NE #BC-TIAA-CNTRCT OR BD.BD-TIAA-CREF-IND NE #BC-TIAA-CREF-IND
                        || bd_Bd_Tiaa_Cntrct.notEquals(pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Bc_Tiaa_Cntrct) || bd_Bd_Tiaa_Cref_Ind.notEquals(pnd_Stat_Pin_Cntrct_Ind_Key_Pnd_Bc_Tiaa_Cref_Ind)))
                    {
                        if (true) break RBD2;                                                                                                                             //Natural: ESCAPE BOTTOM ( RBD2. )
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Bd_Isn.setValue(vw_bd.getAstISN("RBD2"));                                                                                                         //Natural: ASSIGN #BD-ISN := *ISN ( RBD2. )
                    GBD2:                                                                                                                                                 //Natural: GET BD #BD-ISN
                    vw_bd.readByID(pnd_Bd_Isn.getLong(), "GBD2");
                    vw_bdh.reset();                                                                                                                                       //Natural: RESET BDH
                    vw_bdh.setValuesByName(vw_bd);                                                                                                                        //Natural: MOVE BY NAME BD TO BDH
                    //*      BDH.BD-RCRD-LAST-UPDT-DTE    := #DATN
                    //*      BDH.BD-RCRD-LAST-UPDT-TME    := #TIMN
                    //*      BDH.BD-RCRD-LAST-UPDT-USERID := *PROGRAM
                    if (condition(pls_Trace.getBoolean()))                                                                                                                //Natural: IF +TRACE
                    {
                        getReports().write(0, "BD>BDH delete",bd_Bd_Pin,bd_Bd_Tiaa_Cntrct,bd_Bd_Tiaa_Cref_Ind,bd_Bd_Stat,bd_Bd_Rqst_Timestamp);                           //Natural: WRITE ( 0 ) 'BD>BDH delete' BD.BD-PIN BD.BD-TIAA-CNTRCT BD.BD-TIAA-CREF-IND BD.BD-STAT BD.BD-RQST-TIMESTAMP
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RBD2"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RBD2"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    vw_bdh.insertDBRow();                                                                                                                                 //Natural: STORE BDH
                    vw_bd.deleteDBRow("GBD2");                                                                                                                            //Natural: DELETE ( GBD2. )
                    pnd_Details_Pnd_Dsgntn.setValue(true);                                                                                                                //Natural: ASSIGN #DSGNTN := TRUE
                }                                                                                                                                                         //Natural: END-READ
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RBC2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RBC2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(! (pnd_Details_Pnd_Dsgntn.getBoolean())))                                                                                                       //Natural: IF NOT #DSGNTN
            {
                getCurrentProcessState().getDbConv().dbRollback();                                                                                                        //Natural: BACKOUT TRANSACTION
                pnd_Details_Pnd_Msg.setValue("Delete Pending Status failed.");                                                                                            //Natural: ASSIGN #MSG := 'Delete Pending Status failed.'
                                                                                                                                                                          //Natural: PERFORM PRINT-ERROR-MESSAGE
                sub_Print_Error_Message();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RBC2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RBC2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            GBC2:                                                                                                                                                         //Natural: GET BC #BC-ISN
            vw_bc.readByID(pnd_Bc_Isn.getLong(), "GBC2");
            vw_bch.reset();                                                                                                                                               //Natural: RESET BCH
            vw_bch.setValuesByName(vw_bc);                                                                                                                                //Natural: MOVE BY NAME BC TO BCH
            //*  BCH.BC-RCRD-LAST-UPDT-DTE    := #DATN
            //*  BCH.BC-RCRD-LAST-UPDT-TME    := #TIMN
            //*  BCH.BC-RCRD-LAST-UPDT-USERID := *PROGRAM
            if (condition(pls_Trace.getBoolean()))                                                                                                                        //Natural: IF +TRACE
            {
                getReports().write(0, "BC>BCH delete",bc_Bc_Pin,bc_Bc_Tiaa_Cntrct,bc_Bc_Tiaa_Cref_Ind,bc_Bc_Stat,bc_Bc_Rqst_Timestamp,bc_Bc_Mos_Ind);                     //Natural: WRITE ( 0 ) 'BC>BCH delete' BC.BC-PIN BC.BC-TIAA-CNTRCT BC.BC-TIAA-CREF-IND BC.BC-STAT BC.BC-RQST-TIMESTAMP BC.BC-MOS-IND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RBC2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RBC2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            vw_bch.insertDBRow();                                                                                                                                         //Natural: STORE BCH
            vw_bc.deleteDBRow("GBC2");                                                                                                                                    //Natural: DELETE ( GBC2. )
            pnd_Details_Pnd_Dlt_Old.setValue(true);                                                                                                                       //Natural: ASSIGN #DLT-OLD := #ADD-HIST := TRUE
            pnd_Details_Pnd_Add_Hist.setValue(true);
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  DLT-OLD-BENE-ADD-HIST
    }
    private void sub_Print_Error_Message() throws Exception                                                                                                               //Natural: PRINT-ERROR-MESSAGE
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, "SUBROUTINE PRINT-ERROR-MESSAGE");                                                                                                      //Natural: WRITE 'SUBROUTINE PRINT-ERROR-MESSAGE'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        pnd_Total_Ba_Error.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #TOTAL-BA-ERROR
        getReports().display(2, ReportOption.NOTITLE,"Message",                                                                                                           //Natural: DISPLAY ( 2 ) NOTITLE 'Message' #MSG 'Unit' BA-WPID-UNIT 'WPID' BA-WPID 'RLDT' BA-RQST-TIMESTAMP 'PIN' BA-PIN 'Contract' BA-SLCTD-TIAA ( 1 ) 'Lttr' BA-LTTR-OPTN 'Vrfy' BA-VRFY-IND 'UserID' BA-ORIG-UPDT-USERID 'UpdtDate' BA-ORIG-UPDT-DTE
        		pnd_Details_Pnd_Msg,"Unit",
        		ba_Ba_Wpid_Unit,"WPID",
        		ba_Ba_Wpid,"RLDT",
        		ba_Ba_Rqst_Timestamp,"PIN",
        		ba_Ba_Pin,"Contract",
        		ba_Ba_Slctd_Tiaa.getValue(1),"Lttr",
        		ba_Ba_Lttr_Optn,"Vrfy",
        		ba_Ba_Vrfy_Ind,"UserID",
        		ba_Ba_Orig_Updt_Userid,"UpdtDate",
        		ba_Ba_Orig_Updt_Dte);
        if (Global.isEscape()) return;
        //*  PRINT-ERROR-MESSAGE
    }
    private void sub_Update_Bene_Activity() throws Exception                                                                                                              //Natural: UPDATE-BENE-ACTIVITY
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, "SUBROUTINE UPDATE-BENE-ACTIVITY");                                                                                                     //Natural: WRITE 'SUBROUTINE UPDATE-BENE-ACTIVITY'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //*  HAS BEEN VERIFIED
        pnd_Total_Auto_Verify.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #TOTAL-AUTO-VERIFY
        UBA:                                                                                                                                                              //Natural: GET BA #BA-ISN
        vw_ba.readByID(pnd_Ba_Isn.getLong(), "UBA");
        ba_Ba_Rqst_Cmpltd_Dte.setValue(pnd_Datn);                                                                                                                         //Natural: ASSIGN BA-RQST-CMPLTD-DTE := #DATN
        ba_Ba_Vrfy_Ind.setValue("3");                                                                                                                                     //Natural: ASSIGN BA-VRFY-IND := '3'
        ba_Ba_Vrfy_Userid.setValue(ba_Ba_Orig_Updt_Userid);                                                                                                               //Natural: ASSIGN BA-VRFY-USERID := BA-ORIG-UPDT-USERID
        ba_Ba_Vrfy_Dte.setValue(pnd_Datn);                                                                                                                                //Natural: ASSIGN BA-VRFY-DTE := #DATN
        ba_Ba_Vrfy_Tme.setValue(pnd_Timn);                                                                                                                                //Natural: ASSIGN BA-VRFY-TME := #TIMN
        ba_Ba_Rcrd_Last_Updt_Dte.setValue(pnd_Datn);                                                                                                                      //Natural: ASSIGN BA-RCRD-LAST-UPDT-DTE := #DATN
        ba_Ba_Rcrd_Last_Updt_Tme.setValue(pnd_Timn);                                                                                                                      //Natural: ASSIGN BA-RCRD-LAST-UPDT-TME := #TIMN
        ba_Ba_Rcrd_Last_Updt_Userid.setValue(ba_Ba_Orig_Updt_Userid);                                                                                                     //Natural: ASSIGN BA-RCRD-LAST-UPDT-USERID := BA-ORIG-UPDT-USERID
        vw_ba.updateDBRow("UBA");                                                                                                                                         //Natural: UPDATE ( UBA. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        pnd_Details_Pnd_Verified.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ba_Ba_Vrfy_Ind, "-Verified"));                                                  //Natural: COMPRESS BA-VRFY-IND '-Verified' INTO #VERIFIED LEAVING NO
        getReports().display(1, ReportOption.NOTITLE,"Unit",                                                                                                              //Natural: DISPLAY ( 1 ) NOTITLE 'Unit' BA-WPID-UNIT 'WPID' BA-WPID 'RLDT' BA-RQST-TIMESTAMP 'PIN' BA-PIN 'TIAA' BA-SLCTD-TIAA ( 1 ) 'VrfyInd' #VERIFIED 'Reason' BA-VRFY-REASON 'CmpltDt' BA-RQST-CMPLTD-DTE 'OrigDt' BA-ORIG-UPDT-DTE 'OrigID' BA-ORIG-UPDT-USERID
        		ba_Ba_Wpid_Unit,"WPID",
        		ba_Ba_Wpid,"RLDT",
        		ba_Ba_Rqst_Timestamp,"PIN",
        		ba_Ba_Pin,"TIAA",
        		ba_Ba_Slctd_Tiaa.getValue(1),"VrfyInd",
        		pnd_Details_Pnd_Verified,"Reason",
        		ba_Ba_Vrfy_Reason,"CmpltDt",
        		ba_Ba_Rqst_Cmpltd_Dte,"OrigDt",
        		ba_Ba_Orig_Updt_Dte,"OrigID",
        		ba_Ba_Orig_Updt_Userid);
        if (Global.isEscape()) return;
        //*  UPDATE-BENE-ACTIVITY
    }
    private void sub_Update_Cwf_Status() throws Exception                                                                                                                 //Natural: UPDATE-CWF-STATUS
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, "SUBROUTINE UPDATE-CWF-STATUS");                                                                                                        //Natural: WRITE 'SUBROUTINE UPDATE-CWF-STATUS'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        pdaBena009.getBena009().reset();                                                                                                                                  //Natural: RESET BENA009
        pdaBena009.getBena009_Pnd_Wpid().setValue(ba_Ba_Wpid);                                                                                                            //Natural: ASSIGN BENA009.#WPID := BA-WPID
        pdaBena009.getBena009_Pnd_Gda_User_Id().setValue("BENEWEB");                                                                                                      //Natural: ASSIGN BENA009.#GDA-USER-ID := 'BENEWEB'
        pdaBena009.getBena009_Pnd_Gda_Slo_Exists().setValue("Y");                                                                                                         //Natural: ASSIGN BENA009.#GDA-SLO-EXISTS := 'Y'
        pdaBena009.getBena009_Pnd_Wpid_Pin().setValue(ba_Ba_Pin);                                                                                                         //Natural: ASSIGN BENA009.#WPID-PIN := BA-PIN
        pdaBena009.getBena009_Pnd_Wpid_Unit().setValue(ba_Ba_Wpid_Unit);                                                                                                  //Natural: ASSIGN BENA009.#WPID-UNIT := BA-WPID-UNIT
        pdaBena009.getBena009_Pnd_Empl_Racf_Id().setValue("BENEWEB");                                                                                                     //Natural: ASSIGN BENA009.#EMPL-RACF-ID := 'BENEWEB'
        pdaBena009.getBena009_Pnd_Wpid_System().setValue("BEN");                                                                                                          //Natural: ASSIGN BENA009.#WPID-SYSTEM := 'BEN'
        pdaBena009.getBena009_Pnd_Et_Ind().setValue("N");                                                                                                                 //Natural: ASSIGN BENA009.#ET-IND := 'N'
        pdaBena009.getBena009_Pnd_Slo_Rqst_Log_Dte_Tme().setValue(ba_Ba_Rqst_Timestamp);                                                                                  //Natural: ASSIGN BENA009.#SLO-RQST-LOG-DTE-TME := BA-RQST-TIMESTAMP
        //*  REGULAR MAIL
        //*  UPDATE COMPLT/AWTNG PRINT
        if (condition(ba_Ba_Lttr_Optn.equals("2")))                                                                                                                       //Natural: IF BA-LTTR-OPTN = '2'
        {
            pdaBena009.getBena009_Pnd_Wpid_Status().setValue("7501");                                                                                                     //Natural: ASSIGN BENA009.#WPID-STATUS := '7501'
            //*  BENE UPDT CMPLT/NO STATMT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaBena009.getBena009_Pnd_Wpid_Status().setValue("8330");                                                                                                     //Natural: ASSIGN BENA009.#WPID-STATUS := '8330'
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Benn009.class , getCurrentProcessState(), pdaBena009.getBena009());                                                                               //Natural: CALLNAT 'BENN009' BENA009
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Details_Pnd_Msg.equals(" ")))                                                                                                                   //Natural: IF #MSG = ' '
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
            pnd_Details_Pnd_Msg.setValue(pdaBena009.getBena009_Pnd_Error_Msg());                                                                                          //Natural: ASSIGN #MSG := BENA009.#ERROR-MSG
                                                                                                                                                                          //Natural: PERFORM PRINT-ERROR-MESSAGE
            sub_Print_Error_Message();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  UPDATE-CWF-STATUS
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(57),"Beneficiary System",new TabSetting(120),"Page:",getReports().getPageNumberDbs(1),  //Natural: WRITE ( 1 ) NOTITLE 001T *PROGRAM 057T 'Beneficiary System' 120T 'Page:' *PAGE-NUMBER ( 1 ) ( EM = ZZ,ZZ9 ) / 001T *DATX ( EM = MM/DD/YYYY ) 047T 'Batch Auto-Verify WEB-E Updated Report' 120T *TIMX ( EM = HH:II:SS' 'AP ) /
                        new ReportEditMask ("ZZ,ZZ9"),NEWLINE,new TabSetting(1),Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new TabSetting(47),"Batch Auto-Verify WEB-E Updated Report",new 
                        TabSetting(120),Global.getTIMX(), new ReportEditMask ("HH:II:SS' 'AP"),NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(57),"Beneficiary System",new TabSetting(120),"Page:",getReports().getPageNumberDbs(2),  //Natural: WRITE ( 2 ) NOTITLE 001T *PROGRAM 057T 'Beneficiary System' 120T 'Page:' *PAGE-NUMBER ( 2 ) ( EM = ZZ,ZZ9 ) / 001T *DATX ( EM = MM/DD/YYYY ) 048T 'Batch Auto-Verify WEB-E Error Report' 120T *TIMX ( EM = HH:II:SS' 'AP ) /
                        new ReportEditMask ("ZZ,ZZ9"),NEWLINE,new TabSetting(1),Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new TabSetting(48),"Batch Auto-Verify WEB-E Error Report",new 
                        TabSetting(120),Global.getTIMX(), new ReportEditMask ("HH:II:SS' 'AP"),NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "SG=OFF ZP=ON LS=133 PS=60");
        Global.format(1, "SG=OFF ZP=ON LS=133 PS=60");
        Global.format(2, "SG=OFF ZP=ON LS=133 PS=60");

        getReports().setDisplayColumns(2, ReportOption.NOTITLE,"Message",
        		pnd_Details_Pnd_Msg,"Unit",
        		ba_Ba_Wpid_Unit,"WPID",
        		ba_Ba_Wpid,"RLDT",
        		ba_Ba_Rqst_Timestamp,"PIN",
        		ba_Ba_Pin,"Contract",
        		ba_Ba_Slctd_Tiaa,"Lttr",
        		ba_Ba_Lttr_Optn,"Vrfy",
        		ba_Ba_Vrfy_Ind,"UserID",
        		ba_Ba_Orig_Updt_Userid,"UpdtDate",
        		ba_Ba_Orig_Updt_Dte);
        getReports().setDisplayColumns(1, ReportOption.NOTITLE,"Unit",
        		ba_Ba_Wpid_Unit,"WPID",
        		ba_Ba_Wpid,"RLDT",
        		ba_Ba_Rqst_Timestamp,"PIN",
        		ba_Ba_Pin,"TIAA",
        		ba_Ba_Slctd_Tiaa,"VrfyInd",
        		pnd_Details_Pnd_Verified,"Reason",
        		ba_Ba_Vrfy_Reason,"CmpltDt",
        		ba_Ba_Rqst_Cmpltd_Dte,"OrigDt",
        		ba_Ba_Orig_Updt_Dte,"OrigID",
        		ba_Ba_Orig_Updt_Userid);
    }
}
