/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:20:46 PM
**        * FROM NATURAL PROGRAM : Benp9995
************************************************************
**        * FILE NAME            : Benp9995.java
**        * CLASS NAME           : Benp9995
**        * INSTANCE NAME        : Benp9995
************************************************************
************************************************************************
* PROGRAM NAME : BENP9995
* DESCRIPTION  : BULK LOAD DATA REPAIR W/ CWF AUDIT TRAIL, CONFIRMATION
*                LETTER & BENE-ACTIVITY (CLOSED & PRINTED)
* WRITTEN BY   : DENNIS DURAN
* DATE WRITTEN : 11/17/2020
***************************************************************A********

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benp9995 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private PdaBena002 pdaBena002;
    private PdaBena009 pdaBena009;
    private PdaBena010 pdaBena010;
    private PdaBena011 pdaBena011;
    private PdaMdma101 pdaMdma101;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Workfile;
    private DbsField pnd_Workfile_Pnd_Ph_Ssn;
    private DbsField pnd_Workfile_Pnd_Filler1;
    private DbsField pnd_Workfile_Pnd_Ph_Pin_A12;

    private DbsGroup pnd_Workfile__R_Field_1;
    private DbsField pnd_Workfile_Pnd_Ph_Pin;
    private DbsField pnd_Workfile_Pnd_Filler2;
    private DbsField pnd_Workfile_Pnd_I_Tiaa;
    private DbsField pnd_Workfile_Pnd_Filler3;
    private DbsField pnd_Workfile_Pnd_I_Cref;
    private DbsField pnd_Workfile_Pnd_Filler4;
    private DbsField pnd_Workfile_Pnd_I_Efdt;
    private DbsField pnd_Workfile_Pnd_Filler5;
    private DbsField pnd_Workfile_Pnd_I_Type;
    private DbsField pnd_Workfile_Pnd_Filler6;
    private DbsField pnd_Workfile_Pnd_I_Alloc;

    private DbsGroup pnd_Workfile__R_Field_2;
    private DbsField pnd_Workfile_Pnd_I_Perc;
    private DbsField pnd_Workfile_Pnd_Filler7;
    private DbsField pnd_Workfile_Pnd_I_Rc;
    private DbsField pnd_Workfile_Pnd_Filler8;
    private DbsField pnd_Workfile_Pnd_I_Dob;
    private DbsField pnd_Workfile_Pnd_Filler9;
    private DbsField pnd_Workfile_Pnd_I_Ss_Cd;
    private DbsField pnd_Workfile_Pnd_Filler10;
    private DbsField pnd_Workfile_Pnd_I_Ss_Nmbr;
    private DbsField pnd_Workfile_Pnd_Filler11;
    private DbsField pnd_Workfile_Pnd_I_Name1;
    private DbsField pnd_Workfile_Pnd_Filler12;
    private DbsField pnd_Workfile_Pnd_I_Name2;

    private DataAccessProgramView vw_bca;
    private DbsField bca_Bc_Pin;
    private DbsField bca_Bc_Tiaa_Cntrct;
    private DbsField bca_Bc_Cref_Cntrct;
    private DbsField bca_Bc_Tiaa_Cref_Ind;
    private DbsField bca_Bc_Stat;
    private DbsField bca_Bc_Cntrct_Type;
    private DbsField bca_Bc_Rqst_Timestamp;
    private DbsField bca_Bc_Eff_Dte;
    private DbsField bca_Bc_Mos_Ind;
    private DbsField bca_Bc_Irvcbl_Ind;
    private DbsField bca_Bc_Pymnt_Chld_Dcsd_Ind;
    private DbsField bca_Bc_Exempt_Spouse_Rights;
    private DbsField bca_Bc_Spouse_Waived_Bnfts;
    private DbsField bca_Bc_Trust_Data_Fldr;
    private DbsField bca_Bc_Bene_Addr_Fldr;
    private DbsField bca_Bc_Co_Owner_Data_Fldr;
    private DbsField bca_Bc_Fldr_Min;
    private DbsField bca_Bc_Fldr_Srce_Id;
    private DbsField bca_Bc_Fldr_Log_Dte_Tme;
    private DbsField bca_Bc_Rcrd_Last_Updt_Dte;
    private DbsField bca_Bc_Rcrd_Last_Updt_Tme;
    private DbsField bca_Bc_Rcrd_Last_Updt_Userid;
    private DbsField bca_Bc_Last_Dsgntn_Srce;
    private DbsField bca_Bc_Last_Dsgntn_System;
    private DbsField bca_Bc_Last_Dsgntn_Userid;
    private DbsField bca_Bc_Last_Dsgntn_Dte;
    private DbsField bca_Bc_Last_Dsgntn_Tme;
    private DbsField bca_Bc_Last_Vrfy_Dte;
    private DbsField bca_Bc_Last_Vrfy_Tme;
    private DbsField bca_Bc_Last_Vrfy_Userid;
    private DbsField bca_Bc_Tiaa_Cref_Chng_Dte;
    private DbsField bca_Bc_Tiaa_Cref_Chng_Tme;
    private DbsField bca_Bc_Chng_Pwr_Atty;
    private DbsField bca_Bc_Mgrtn_Cnfrm_Stmnt_Prod_Dte;

    private DataAccessProgramView vw_bda;
    private DbsField bda_Bd_Pin;
    private DbsField bda_Bd_Tiaa_Cntrct;
    private DbsField bda_Bd_Cref_Cntrct;
    private DbsField bda_Bd_Tiaa_Cref_Ind;
    private DbsField bda_Bd_Stat;
    private DbsField bda_Bd_Seq_Nmbr;
    private DbsField bda_Bd_Rqst_Timestamp;
    private DbsField bda_Bd_Rcrd_Last_Updt_Dte;
    private DbsField bda_Bd_Rcrd_Last_Updt_Tme;
    private DbsField bda_Bd_Rcrd_Last_Updt_Userid;
    private DbsField bda_Bd_Bene_Type;
    private DbsField bda_Bd_Rltn_Cd;
    private DbsField bda_Bd_Bene_Name1;
    private DbsField bda_Bd_Bene_Name2;
    private DbsField bda_Bd_Rltn_Free_Txt;
    private DbsField bda_Bd_Dte_Birth_Trust;
    private DbsField bda_Bd_Ss_Cd;
    private DbsField bda_Bd_Ss_Nmbr;
    private DbsField bda_Bd_Perc_Share_Ind;
    private DbsField bda_Bd_Share_Perc;
    private DbsField bda_Bd_Share_Ntor;
    private DbsField bda_Bd_Share_Dtor;
    private DbsField bda_Bd_Irvcbl_Ind;
    private DbsField bda_Bd_Stlmnt_Rstrctn;
    private DbsField bda_Bd_Spcl_Txt1;
    private DbsField bda_Bd_Spcl_Txt2;
    private DbsField bda_Bd_Spcl_Txt3;
    private DbsField bda_Bd_Dflt_Estate;
    private DbsField bda_Bd_Addr1;
    private DbsField bda_Bd_Addr2;
    private DbsField bda_Bd_Addr3_City;
    private DbsField bda_Bd_State;
    private DbsField bda_Bd_Zip;
    private DbsField bda_Bd_Phone;
    private DbsField bda_Bd_Gender;
    private DbsField bda_Bd_Country;

    private DataAccessProgramView vw_bma;
    private DbsField bma_Bm_Pin;
    private DbsField bma_Bm_Tiaa_Cntrct;
    private DbsField bma_Bm_Cref_Cntrct;
    private DbsField bma_Bm_Tiaa_Cref_Ind;
    private DbsField bma_Bm_Stat;
    private DbsField bma_Bm_Rqst_Timestamp;
    private DbsField bma_Bm_Rcrd_Last_Updt_Dte;
    private DbsField bma_Bm_Rcrd_Last_Updt_Tme;
    private DbsField bma_Bm_Rcrd_Last_Updt_Userid;

    private DbsGroup bma_Bm_Mos_Txt_Group;
    private DbsField bma_Bm_Mos_Txt;

    private DataAccessProgramView vw_bt;
    private DbsField bt_Bt_Table_Id;
    private DbsField bt_Bt_Table_Key;
    private DbsField bt_Bt_Table_Text;
    private DbsField bt_Bt_Rcrd_Last_Updt_Dte;
    private DbsField bt_Bt_Rcrd_Last_Updt_Tme;
    private DbsField bt_Bt_Rcrd_Last_Updt_Userid;
    private DbsField pnd_Bt_Table_Id_Key;
    private DbsField pnd_Super_Key;

    private DbsGroup pnd_Super_Key__R_Field_3;
    private DbsField pnd_Super_Key_Pnd_Key_Pin;
    private DbsField pnd_Super_Key_Pnd_Key_Tiaa;
    private DbsField pnd_Super_Key_Pnd_Key_Tc;

    private DbsGroup pnd_Bene;
    private DbsField pnd_Bene_Pnd_Rldt;

    private DbsGroup pnd_Bene__R_Field_4;
    private DbsField pnd_Bene_Pnd_Date;
    private DbsField pnd_Bene_Pnd_Time;
    private DbsField pnd_Bene_Pnd_Bn_Pin;
    private DbsField pnd_Bene_Pnd_Bn_Tiaa;
    private DbsField pnd_Bene_Pnd_Bn_Cref;
    private DbsField pnd_Bene_Pnd_Bn_Efdt;
    private DbsField pnd_Bene_Pnd_Bn_Seq;

    private DbsGroup pnd_Bene_Pnd_Bn;
    private DbsField pnd_Bene_Pnd_Bn_Type;
    private DbsField pnd_Bene_Pnd_Bn_Perc;
    private DbsField pnd_Bene_Pnd_Bn_Rc;
    private DbsField pnd_Bene_Pnd_Bn_Dob;
    private DbsField pnd_Bene_Pnd_Bn_Scd;
    private DbsField pnd_Bene_Pnd_Bn_Ssn;
    private DbsField pnd_Bene_Pnd_Bn_Name1;
    private DbsField pnd_Bene_Pnd_Bn_Name2;
    private DbsField pnd_Bene_Pnd_City_State_Zip;
    private DbsField pnd_Bene_Pnd_Mdm_Lname;
    private DbsField pnd_Bene_Pnd_Mdm_Name;
    private DbsField pnd_Bene_Pnd_Mdm_Ssn;
    private DbsField pnd_Bene_Pnd_Mdm_Addr;
    private DbsField pnd_Bene_Pnd_Mdm_Postal_Data;
    private DbsField pnd_Bene_Pnd_Mdm_Us_Frgn;
    private DbsField pnd_I;
    private DbsField pnd_Total_Read;
    private DbsField pnd_Total_Add;
    private DbsField pnd_Total_Delete;
    private DbsField pnd_Total_Error;
    private DbsField pnd_Total_Print;
    private DbsField pnd_X;
    private DbsField pls_Trace;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaBena002 = new PdaBena002(localVariables);
        pdaBena009 = new PdaBena009(localVariables);
        pdaBena010 = new PdaBena010(localVariables);
        pdaBena011 = new PdaBena011(localVariables);
        pdaMdma101 = new PdaMdma101(localVariables);

        // Local Variables

        pnd_Workfile = localVariables.newGroupInRecord("pnd_Workfile", "#WORKFILE");
        pnd_Workfile_Pnd_Ph_Ssn = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_Ph_Ssn", "#PH-SSN", FieldType.STRING, 9);
        pnd_Workfile_Pnd_Filler1 = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_Filler1", "#FILLER1", FieldType.STRING, 1);
        pnd_Workfile_Pnd_Ph_Pin_A12 = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_Ph_Pin_A12", "#PH-PIN-A12", FieldType.STRING, 12);

        pnd_Workfile__R_Field_1 = pnd_Workfile.newGroupInGroup("pnd_Workfile__R_Field_1", "REDEFINE", pnd_Workfile_Pnd_Ph_Pin_A12);
        pnd_Workfile_Pnd_Ph_Pin = pnd_Workfile__R_Field_1.newFieldInGroup("pnd_Workfile_Pnd_Ph_Pin", "#PH-PIN", FieldType.NUMERIC, 12);
        pnd_Workfile_Pnd_Filler2 = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_Filler2", "#FILLER2", FieldType.STRING, 1);
        pnd_Workfile_Pnd_I_Tiaa = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_I_Tiaa", "#I-TIAA", FieldType.STRING, 8);
        pnd_Workfile_Pnd_Filler3 = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_Filler3", "#FILLER3", FieldType.STRING, 1);
        pnd_Workfile_Pnd_I_Cref = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_I_Cref", "#I-CREF", FieldType.STRING, 8);
        pnd_Workfile_Pnd_Filler4 = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_Filler4", "#FILLER4", FieldType.STRING, 1);
        pnd_Workfile_Pnd_I_Efdt = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_I_Efdt", "#I-EFDT", FieldType.STRING, 8);
        pnd_Workfile_Pnd_Filler5 = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_Filler5", "#FILLER5", FieldType.STRING, 1);
        pnd_Workfile_Pnd_I_Type = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_I_Type", "#I-TYPE", FieldType.STRING, 1);
        pnd_Workfile_Pnd_Filler6 = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_Filler6", "#FILLER6", FieldType.STRING, 1);
        pnd_Workfile_Pnd_I_Alloc = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_I_Alloc", "#I-ALLOC", FieldType.STRING, 3);

        pnd_Workfile__R_Field_2 = pnd_Workfile.newGroupInGroup("pnd_Workfile__R_Field_2", "REDEFINE", pnd_Workfile_Pnd_I_Alloc);
        pnd_Workfile_Pnd_I_Perc = pnd_Workfile__R_Field_2.newFieldInGroup("pnd_Workfile_Pnd_I_Perc", "#I-PERC", FieldType.NUMERIC, 3);
        pnd_Workfile_Pnd_Filler7 = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_Filler7", "#FILLER7", FieldType.STRING, 1);
        pnd_Workfile_Pnd_I_Rc = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_I_Rc", "#I-RC", FieldType.STRING, 2);
        pnd_Workfile_Pnd_Filler8 = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_Filler8", "#FILLER8", FieldType.STRING, 1);
        pnd_Workfile_Pnd_I_Dob = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_I_Dob", "#I-DOB", FieldType.STRING, 8);
        pnd_Workfile_Pnd_Filler9 = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_Filler9", "#FILLER9", FieldType.STRING, 1);
        pnd_Workfile_Pnd_I_Ss_Cd = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_I_Ss_Cd", "#I-SS-CD", FieldType.STRING, 1);
        pnd_Workfile_Pnd_Filler10 = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_Filler10", "#FILLER10", FieldType.STRING, 1);
        pnd_Workfile_Pnd_I_Ss_Nmbr = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_I_Ss_Nmbr", "#I-SS-NMBR", FieldType.STRING, 9);
        pnd_Workfile_Pnd_Filler11 = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_Filler11", "#FILLER11", FieldType.STRING, 1);
        pnd_Workfile_Pnd_I_Name1 = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_I_Name1", "#I-NAME1", FieldType.STRING, 35);
        pnd_Workfile_Pnd_Filler12 = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_Filler12", "#FILLER12", FieldType.STRING, 1);
        pnd_Workfile_Pnd_I_Name2 = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_I_Name2", "#I-NAME2", FieldType.STRING, 35);

        vw_bca = new DataAccessProgramView(new NameInfo("vw_bca", "BCA"), "BENE_CONTRACT_12", "BENE_CONTRACT");
        bca_Bc_Pin = vw_bca.getRecord().newFieldInGroup("bca_Bc_Pin", "BC-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BC_PIN");
        bca_Bc_Pin.setDdmHeader("PIN");
        bca_Bc_Tiaa_Cntrct = vw_bca.getRecord().newFieldInGroup("bca_Bc_Tiaa_Cntrct", "BC-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BC_TIAA_CNTRCT");
        bca_Bc_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bca_Bc_Cref_Cntrct = vw_bca.getRecord().newFieldInGroup("bca_Bc_Cref_Cntrct", "BC-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BC_CREF_CNTRCT");
        bca_Bc_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bca_Bc_Tiaa_Cref_Ind = vw_bca.getRecord().newFieldInGroup("bca_Bc_Tiaa_Cref_Ind", "BC-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_TIAA_CREF_IND");
        bca_Bc_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bca_Bc_Stat = vw_bca.getRecord().newFieldInGroup("bca_Bc_Stat", "BC-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_STAT");
        bca_Bc_Stat.setDdmHeader("STATUS");
        bca_Bc_Cntrct_Type = vw_bca.getRecord().newFieldInGroup("bca_Bc_Cntrct_Type", "BC-CNTRCT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_CNTRCT_TYPE");
        bca_Bc_Cntrct_Type.setDdmHeader("CNTRCT/TYPE");
        bca_Bc_Rqst_Timestamp = vw_bca.getRecord().newFieldInGroup("bca_Bc_Rqst_Timestamp", "BC-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BC_RQST_TIMESTAMP");
        bca_Bc_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        bca_Bc_Eff_Dte = vw_bca.getRecord().newFieldInGroup("bca_Bc_Eff_Dte", "BC-EFF-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "BC_EFF_DTE");
        bca_Bc_Eff_Dte.setDdmHeader("EFFECTIVE/DATE");
        bca_Bc_Mos_Ind = vw_bca.getRecord().newFieldInGroup("bca_Bc_Mos_Ind", "BC-MOS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_MOS_IND");
        bca_Bc_Mos_Ind.setDdmHeader("MOS/IND");
        bca_Bc_Irvcbl_Ind = vw_bca.getRecord().newFieldInGroup("bca_Bc_Irvcbl_Ind", "BC-IRVCBL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_IRVCBL_IND");
        bca_Bc_Irvcbl_Ind.setDdmHeader("IRVCBL/IND");
        bca_Bc_Pymnt_Chld_Dcsd_Ind = vw_bca.getRecord().newFieldInGroup("bca_Bc_Pymnt_Chld_Dcsd_Ind", "BC-PYMNT-CHLD-DCSD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_PYMNT_CHLD_DCSD_IND");
        bca_Bc_Pymnt_Chld_Dcsd_Ind.setDdmHeader("PYMNT/CHILD/DCSD");
        bca_Bc_Exempt_Spouse_Rights = vw_bca.getRecord().newFieldInGroup("bca_Bc_Exempt_Spouse_Rights", "BC-EXEMPT-SPOUSE-RIGHTS", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "BC_EXEMPT_SPOUSE_RIGHTS");
        bca_Bc_Exempt_Spouse_Rights.setDdmHeader("EXEMPT/SPOUSAL/RIGHTS");
        bca_Bc_Spouse_Waived_Bnfts = vw_bca.getRecord().newFieldInGroup("bca_Bc_Spouse_Waived_Bnfts", "BC-SPOUSE-WAIVED-BNFTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_SPOUSE_WAIVED_BNFTS");
        bca_Bc_Spouse_Waived_Bnfts.setDdmHeader("SPOUSE/WAIVED/BENEFITS");
        bca_Bc_Trust_Data_Fldr = vw_bca.getRecord().newFieldInGroup("bca_Bc_Trust_Data_Fldr", "BC-TRUST-DATA-FLDR", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_TRUST_DATA_FLDR");
        bca_Bc_Trust_Data_Fldr.setDdmHeader("TRUST/DATA/IN FLDR");
        bca_Bc_Bene_Addr_Fldr = vw_bca.getRecord().newFieldInGroup("bca_Bc_Bene_Addr_Fldr", "BC-BENE-ADDR-FLDR", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_BENE_ADDR_FLDR");
        bca_Bc_Bene_Addr_Fldr.setDdmHeader("BENE/ADDRESS/IN FLDR");
        bca_Bc_Co_Owner_Data_Fldr = vw_bca.getRecord().newFieldInGroup("bca_Bc_Co_Owner_Data_Fldr", "BC-CO-OWNER-DATA-FLDR", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_CO_OWNER_DATA_FLDR");
        bca_Bc_Co_Owner_Data_Fldr.setDdmHeader("CO-OWNER/DATA FLDR");
        bca_Bc_Fldr_Min = vw_bca.getRecord().newFieldInGroup("bca_Bc_Fldr_Min", "BC-FLDR-MIN", FieldType.STRING, 11, RepeatingFieldStrategy.None, "BC_FLDR_MIN");
        bca_Bc_Fldr_Min.setDdmHeader("FOLDER/MIN");
        bca_Bc_Fldr_Srce_Id = vw_bca.getRecord().newFieldInGroup("bca_Bc_Fldr_Srce_Id", "BC-FLDR-SRCE-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "BC_FLDR_SRCE_ID");
        bca_Bc_Fldr_Srce_Id.setDdmHeader("FOLDER/SOURCE/ID");
        bca_Bc_Fldr_Log_Dte_Tme = vw_bca.getRecord().newFieldInGroup("bca_Bc_Fldr_Log_Dte_Tme", "BC-FLDR-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BC_FLDR_LOG_DTE_TME");
        bca_Bc_Fldr_Log_Dte_Tme.setDdmHeader("FOLDER/LOG DATE/AND TIME");
        bca_Bc_Rcrd_Last_Updt_Dte = vw_bca.getRecord().newFieldInGroup("bca_Bc_Rcrd_Last_Updt_Dte", "BC-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_RCRD_LAST_UPDT_DTE");
        bca_Bc_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bca_Bc_Rcrd_Last_Updt_Tme = vw_bca.getRecord().newFieldInGroup("bca_Bc_Rcrd_Last_Updt_Tme", "BC-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_RCRD_LAST_UPDT_TME");
        bca_Bc_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bca_Bc_Rcrd_Last_Updt_Userid = vw_bca.getRecord().newFieldInGroup("bca_Bc_Rcrd_Last_Updt_Userid", "BC-RCRD-LAST-UPDT-USERID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "BC_RCRD_LAST_UPDT_USERID");
        bca_Bc_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        bca_Bc_Last_Dsgntn_Srce = vw_bca.getRecord().newFieldInGroup("bca_Bc_Last_Dsgntn_Srce", "BC-LAST-DSGNTN-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_SRCE");
        bca_Bc_Last_Dsgntn_Srce.setDdmHeader("LAST/DSGNTN/SOURCE");
        bca_Bc_Last_Dsgntn_System = vw_bca.getRecord().newFieldInGroup("bca_Bc_Last_Dsgntn_System", "BC-LAST-DSGNTN-SYSTEM", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_SYSTEM");
        bca_Bc_Last_Dsgntn_System.setDdmHeader("LAST/DSGNTN/SYSTEM");
        bca_Bc_Last_Dsgntn_Userid = vw_bca.getRecord().newFieldInGroup("bca_Bc_Last_Dsgntn_Userid", "BC-LAST-DSGNTN-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_USERID");
        bca_Bc_Last_Dsgntn_Userid.setDdmHeader("LAST/DSGNTN/USERID");
        bca_Bc_Last_Dsgntn_Dte = vw_bca.getRecord().newFieldInGroup("bca_Bc_Last_Dsgntn_Dte", "BC-LAST-DSGNTN-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_DTE");
        bca_Bc_Last_Dsgntn_Dte.setDdmHeader("LAST/DSGNTN/DATE");
        bca_Bc_Last_Dsgntn_Tme = vw_bca.getRecord().newFieldInGroup("bca_Bc_Last_Dsgntn_Tme", "BC-LAST-DSGNTN-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_TME");
        bca_Bc_Last_Dsgntn_Tme.setDdmHeader("LAST/DSGNTN/TIME");
        bca_Bc_Last_Vrfy_Dte = vw_bca.getRecord().newFieldInGroup("bca_Bc_Last_Vrfy_Dte", "BC-LAST-VRFY-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_VRFY_DTE");
        bca_Bc_Last_Vrfy_Dte.setDdmHeader("LAST/VRFY/DATE");
        bca_Bc_Last_Vrfy_Tme = vw_bca.getRecord().newFieldInGroup("bca_Bc_Last_Vrfy_Tme", "BC-LAST-VRFY-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_LAST_VRFY_TME");
        bca_Bc_Last_Vrfy_Tme.setDdmHeader("LAST/VRFY/TIME");
        bca_Bc_Last_Vrfy_Userid = vw_bca.getRecord().newFieldInGroup("bca_Bc_Last_Vrfy_Userid", "BC-LAST-VRFY-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_VRFY_USERID");
        bca_Bc_Last_Vrfy_Userid.setDdmHeader("LAST/VRFY/USERID");
        bca_Bc_Tiaa_Cref_Chng_Dte = vw_bca.getRecord().newFieldInGroup("bca_Bc_Tiaa_Cref_Chng_Dte", "BC-TIAA-CREF-CHNG-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_TIAA_CREF_CHNG_DTE");
        bca_Bc_Tiaa_Cref_Chng_Dte.setDdmHeader("TIAA CREF/CHANGE/DATE");
        bca_Bc_Tiaa_Cref_Chng_Tme = vw_bca.getRecord().newFieldInGroup("bca_Bc_Tiaa_Cref_Chng_Tme", "BC-TIAA-CREF-CHNG-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_TIAA_CREF_CHNG_TME");
        bca_Bc_Tiaa_Cref_Chng_Tme.setDdmHeader("TIAA CREF/CHANGE/TIME");
        bca_Bc_Chng_Pwr_Atty = vw_bca.getRecord().newFieldInGroup("bca_Bc_Chng_Pwr_Atty", "BC-CHNG-PWR-ATTY", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_CHNG_PWR_ATTY");
        bca_Bc_Chng_Pwr_Atty.setDdmHeader("CHANGE/POWER/ATTNY");
        bca_Bc_Mgrtn_Cnfrm_Stmnt_Prod_Dte = vw_bca.getRecord().newFieldInGroup("bca_Bc_Mgrtn_Cnfrm_Stmnt_Prod_Dte", "BC-MGRTN-CNFRM-STMNT-PROD-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "BC_MGRTN_CNFRM_STMNT_PROD_DTE");
        bca_Bc_Mgrtn_Cnfrm_Stmnt_Prod_Dte.setDdmHeader("MGRTN CONFIRM/STATEMENT/PROD DATE");
        registerRecord(vw_bca);

        vw_bda = new DataAccessProgramView(new NameInfo("vw_bda", "BDA"), "BENE_DESIGNATION_12", "BENE_DESIGNATION");
        bda_Bd_Pin = vw_bda.getRecord().newFieldInGroup("bda_Bd_Pin", "BD-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BD_PIN");
        bda_Bd_Pin.setDdmHeader("PIN");
        bda_Bd_Tiaa_Cntrct = vw_bda.getRecord().newFieldInGroup("bda_Bd_Tiaa_Cntrct", "BD-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BD_TIAA_CNTRCT");
        bda_Bd_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bda_Bd_Cref_Cntrct = vw_bda.getRecord().newFieldInGroup("bda_Bd_Cref_Cntrct", "BD-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BD_CREF_CNTRCT");
        bda_Bd_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bda_Bd_Tiaa_Cref_Ind = vw_bda.getRecord().newFieldInGroup("bda_Bd_Tiaa_Cref_Ind", "BD-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_TIAA_CREF_IND");
        bda_Bd_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bda_Bd_Stat = vw_bda.getRecord().newFieldInGroup("bda_Bd_Stat", "BD-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_STAT");
        bda_Bd_Stat.setDdmHeader("STAT");
        bda_Bd_Seq_Nmbr = vw_bda.getRecord().newFieldInGroup("bda_Bd_Seq_Nmbr", "BD-SEQ-NMBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "BD_SEQ_NMBR");
        bda_Bd_Seq_Nmbr.setDdmHeader(" SEQ/NMBR");
        bda_Bd_Rqst_Timestamp = vw_bda.getRecord().newFieldInGroup("bda_Bd_Rqst_Timestamp", "BD-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BD_RQST_TIMESTAMP");
        bda_Bd_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        bda_Bd_Rcrd_Last_Updt_Dte = vw_bda.getRecord().newFieldInGroup("bda_Bd_Rcrd_Last_Updt_Dte", "BD-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BD_RCRD_LAST_UPDT_DTE");
        bda_Bd_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bda_Bd_Rcrd_Last_Updt_Tme = vw_bda.getRecord().newFieldInGroup("bda_Bd_Rcrd_Last_Updt_Tme", "BD-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BD_RCRD_LAST_UPDT_TME");
        bda_Bd_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bda_Bd_Rcrd_Last_Updt_Userid = vw_bda.getRecord().newFieldInGroup("bda_Bd_Rcrd_Last_Updt_Userid", "BD-RCRD-LAST-UPDT-USERID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "BD_RCRD_LAST_UPDT_USERID");
        bda_Bd_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        bda_Bd_Bene_Type = vw_bda.getRecord().newFieldInGroup("bda_Bd_Bene_Type", "BD-BENE-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_BENE_TYPE");
        bda_Bd_Bene_Type.setDdmHeader("BENE/TYPE");
        bda_Bd_Rltn_Cd = vw_bda.getRecord().newFieldInGroup("bda_Bd_Rltn_Cd", "BD-RLTN-CD", FieldType.STRING, 2, RepeatingFieldStrategy.None, "BD_RLTN_CD");
        bda_Bd_Rltn_Cd.setDdmHeader("RELATION/CODE");
        bda_Bd_Bene_Name1 = vw_bda.getRecord().newFieldInGroup("bda_Bd_Bene_Name1", "BD-BENE-NAME1", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "BD_BENE_NAME1");
        bda_Bd_Bene_Name1.setDdmHeader("BENEFICIARY/NAME 1");
        bda_Bd_Bene_Name2 = vw_bda.getRecord().newFieldInGroup("bda_Bd_Bene_Name2", "BD-BENE-NAME2", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "BD_BENE_NAME2");
        bda_Bd_Bene_Name2.setDdmHeader("BENEFICIARY/NAME 2");
        bda_Bd_Rltn_Free_Txt = vw_bda.getRecord().newFieldInGroup("bda_Bd_Rltn_Free_Txt", "BD-RLTN-FREE-TXT", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BD_RLTN_FREE_TXT");
        bda_Bd_Rltn_Free_Txt.setDdmHeader("OTHER/RELATION");
        bda_Bd_Dte_Birth_Trust = vw_bda.getRecord().newFieldInGroup("bda_Bd_Dte_Birth_Trust", "BD-DTE-BIRTH-TRUST", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BD_DTE_BIRTH_TRUST");
        bda_Bd_Dte_Birth_Trust.setDdmHeader("DOB/DOT");
        bda_Bd_Ss_Cd = vw_bda.getRecord().newFieldInGroup("bda_Bd_Ss_Cd", "BD-SS-CD", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_SS_CD");
        bda_Bd_Ss_Cd.setDdmHeader("SS/CODE");
        bda_Bd_Ss_Nmbr = vw_bda.getRecord().newFieldInGroup("bda_Bd_Ss_Nmbr", "BD-SS-NMBR", FieldType.STRING, 9, RepeatingFieldStrategy.None, "BD_SS_NMBR");
        bda_Bd_Ss_Nmbr.setDdmHeader("SS/NUMBER");
        bda_Bd_Perc_Share_Ind = vw_bda.getRecord().newFieldInGroup("bda_Bd_Perc_Share_Ind", "BD-PERC-SHARE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_PERC_SHARE_IND");
        bda_Bd_Perc_Share_Ind.setDdmHeader("PERCNT/SHARE/IND");
        bda_Bd_Share_Perc = vw_bda.getRecord().newFieldInGroup("bda_Bd_Share_Perc", "BD-SHARE-PERC", FieldType.NUMERIC, 5, 2, RepeatingFieldStrategy.None, 
            "BD_SHARE_PERC");
        bda_Bd_Share_Perc.setDdmHeader("SHARE/PERCENT");
        bda_Bd_Share_Ntor = vw_bda.getRecord().newFieldInGroup("bda_Bd_Share_Ntor", "BD-SHARE-NTOR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "BD_SHARE_NTOR");
        bda_Bd_Share_Ntor.setDdmHeader("SHARE/NUMERATOR");
        bda_Bd_Share_Dtor = vw_bda.getRecord().newFieldInGroup("bda_Bd_Share_Dtor", "BD-SHARE-DTOR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "BD_SHARE_DTOR");
        bda_Bd_Share_Dtor.setDdmHeader("SHARE/DENOMINATOR");
        bda_Bd_Irvcbl_Ind = vw_bda.getRecord().newFieldInGroup("bda_Bd_Irvcbl_Ind", "BD-IRVCBL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_IRVCBL_IND");
        bda_Bd_Irvcbl_Ind.setDdmHeader("IRVCBL/IND");
        bda_Bd_Stlmnt_Rstrctn = vw_bda.getRecord().newFieldInGroup("bda_Bd_Stlmnt_Rstrctn", "BD-STLMNT-RSTRCTN", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_STLMNT_RSTRCTN");
        bda_Bd_Stlmnt_Rstrctn.setDdmHeader("SETTLEMENT/RESTRICTION");
        bda_Bd_Spcl_Txt1 = vw_bda.getRecord().newFieldInGroup("bda_Bd_Spcl_Txt1", "BD-SPCL-TXT1", FieldType.STRING, 72, RepeatingFieldStrategy.None, "BD_SPCL_TXT1");
        bda_Bd_Spcl_Txt1.setDdmHeader("SPECIAL TEXT 1");
        bda_Bd_Spcl_Txt2 = vw_bda.getRecord().newFieldInGroup("bda_Bd_Spcl_Txt2", "BD-SPCL-TXT2", FieldType.STRING, 72, RepeatingFieldStrategy.None, "BD_SPCL_TXT2");
        bda_Bd_Spcl_Txt2.setDdmHeader("SPECIAL TEXT 2");
        bda_Bd_Spcl_Txt3 = vw_bda.getRecord().newFieldInGroup("bda_Bd_Spcl_Txt3", "BD-SPCL-TXT3", FieldType.STRING, 72, RepeatingFieldStrategy.None, "BD_SPCL_TXT3");
        bda_Bd_Spcl_Txt3.setDdmHeader("SPECIAL TEXT 3");
        bda_Bd_Dflt_Estate = vw_bda.getRecord().newFieldInGroup("bda_Bd_Dflt_Estate", "BD-DFLT-ESTATE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_DFLT_ESTATE");
        bda_Bd_Dflt_Estate.setDdmHeader("DEFAULT/ESTATE");
        bda_Bd_Addr1 = vw_bda.getRecord().newFieldInGroup("bda_Bd_Addr1", "BD-ADDR1", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BD_ADDR1");
        bda_Bd_Addr2 = vw_bda.getRecord().newFieldInGroup("bda_Bd_Addr2", "BD-ADDR2", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BD_ADDR2");
        bda_Bd_Addr3_City = vw_bda.getRecord().newFieldInGroup("bda_Bd_Addr3_City", "BD-ADDR3-CITY", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "BD_ADDR3_CITY");
        bda_Bd_State = vw_bda.getRecord().newFieldInGroup("bda_Bd_State", "BD-STATE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "BD_STATE");
        bda_Bd_Zip = vw_bda.getRecord().newFieldInGroup("bda_Bd_Zip", "BD-ZIP", FieldType.STRING, 10, RepeatingFieldStrategy.None, "BD_ZIP");
        bda_Bd_Phone = vw_bda.getRecord().newFieldInGroup("bda_Bd_Phone", "BD-PHONE", FieldType.STRING, 20, RepeatingFieldStrategy.None, "BD_PHONE");
        bda_Bd_Gender = vw_bda.getRecord().newFieldInGroup("bda_Bd_Gender", "BD-GENDER", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_GENDER");
        bda_Bd_Country = vw_bda.getRecord().newFieldInGroup("bda_Bd_Country", "BD-COUNTRY", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BD_COUNTRY");
        registerRecord(vw_bda);

        vw_bma = new DataAccessProgramView(new NameInfo("vw_bma", "BMA"), "BENE_MOS_12", "BENE_MOS", DdmPeriodicGroups.getInstance().getGroups("BENE_MOS_12"));
        bma_Bm_Pin = vw_bma.getRecord().newFieldInGroup("bma_Bm_Pin", "BM-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BM_PIN");
        bma_Bm_Pin.setDdmHeader("PIN");
        bma_Bm_Tiaa_Cntrct = vw_bma.getRecord().newFieldInGroup("bma_Bm_Tiaa_Cntrct", "BM-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BM_TIAA_CNTRCT");
        bma_Bm_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bma_Bm_Cref_Cntrct = vw_bma.getRecord().newFieldInGroup("bma_Bm_Cref_Cntrct", "BM-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BM_CREF_CNTRCT");
        bma_Bm_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bma_Bm_Tiaa_Cref_Ind = vw_bma.getRecord().newFieldInGroup("bma_Bm_Tiaa_Cref_Ind", "BM-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BM_TIAA_CREF_IND");
        bma_Bm_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bma_Bm_Stat = vw_bma.getRecord().newFieldInGroup("bma_Bm_Stat", "BM-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BM_STAT");
        bma_Bm_Stat.setDdmHeader("STAT");
        bma_Bm_Rqst_Timestamp = vw_bma.getRecord().newFieldInGroup("bma_Bm_Rqst_Timestamp", "BM-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BM_RQST_TIMESTAMP");
        bma_Bm_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        bma_Bm_Rcrd_Last_Updt_Dte = vw_bma.getRecord().newFieldInGroup("bma_Bm_Rcrd_Last_Updt_Dte", "BM-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BM_RCRD_LAST_UPDT_DTE");
        bma_Bm_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bma_Bm_Rcrd_Last_Updt_Tme = vw_bma.getRecord().newFieldInGroup("bma_Bm_Rcrd_Last_Updt_Tme", "BM-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BM_RCRD_LAST_UPDT_TME");
        bma_Bm_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bma_Bm_Rcrd_Last_Updt_Userid = vw_bma.getRecord().newFieldInGroup("bma_Bm_Rcrd_Last_Updt_Userid", "BM-RCRD-LAST-UPDT-USERID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "BM_RCRD_LAST_UPDT_USERID");
        bma_Bm_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");

        bma_Bm_Mos_Txt_Group = vw_bma.getRecord().newGroupInGroup("bma_Bm_Mos_Txt_Group", "BM-MOS-TXT-GROUP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "BENE_MOS_BM_MOS_TXT_GROUP");
        bma_Bm_Mos_Txt = bma_Bm_Mos_Txt_Group.newFieldArrayInGroup("bma_Bm_Mos_Txt", "BM-MOS-TXT", FieldType.STRING, 72, new DbsArrayController(1, 60) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "BM_MOS_TXT", "BENE_MOS_BM_MOS_TXT_GROUP");
        bma_Bm_Mos_Txt.setDdmHeader("MOS TEXT");
        registerRecord(vw_bma);

        vw_bt = new DataAccessProgramView(new NameInfo("vw_bt", "BT"), "BENE_TABLE_FILE", "BENE_TABLE");
        bt_Bt_Table_Id = vw_bt.getRecord().newFieldInGroup("bt_Bt_Table_Id", "BT-TABLE-ID", FieldType.STRING, 2, RepeatingFieldStrategy.None, "BT_TABLE_ID");
        bt_Bt_Table_Id.setDdmHeader("TABLE/ID");
        bt_Bt_Table_Key = vw_bt.getRecord().newFieldInGroup("bt_Bt_Table_Key", "BT-TABLE-KEY", FieldType.STRING, 20, RepeatingFieldStrategy.None, "BT_TABLE_KEY");
        bt_Bt_Table_Key.setDdmHeader("TABLE/KEY");
        bt_Bt_Table_Text = vw_bt.getRecord().newFieldInGroup("bt_Bt_Table_Text", "BT-TABLE-TEXT", FieldType.STRING, 72, RepeatingFieldStrategy.None, "BT_TABLE_TEXT");
        bt_Bt_Table_Text.setDdmHeader("TABLE/TEXT");
        bt_Bt_Rcrd_Last_Updt_Dte = vw_bt.getRecord().newFieldInGroup("bt_Bt_Rcrd_Last_Updt_Dte", "BT-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BT_RCRD_LAST_UPDT_DTE");
        bt_Bt_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bt_Bt_Rcrd_Last_Updt_Tme = vw_bt.getRecord().newFieldInGroup("bt_Bt_Rcrd_Last_Updt_Tme", "BT-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BT_RCRD_LAST_UPDT_TME");
        bt_Bt_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bt_Bt_Rcrd_Last_Updt_Userid = vw_bt.getRecord().newFieldInGroup("bt_Bt_Rcrd_Last_Updt_Userid", "BT-RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "BT_RCRD_LAST_UPDT_USERID");
        bt_Bt_Rcrd_Last_Updt_Userid.setDdmHeader("RECRD LAST/UPDATE/USER ID");
        registerRecord(vw_bt);

        pnd_Bt_Table_Id_Key = localVariables.newFieldInRecord("pnd_Bt_Table_Id_Key", "#BT-TABLE-ID-KEY", FieldType.STRING, 22);
        pnd_Super_Key = localVariables.newFieldInRecord("pnd_Super_Key", "#SUPER-KEY", FieldType.STRING, 23);

        pnd_Super_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Super_Key__R_Field_3", "REDEFINE", pnd_Super_Key);
        pnd_Super_Key_Pnd_Key_Pin = pnd_Super_Key__R_Field_3.newFieldInGroup("pnd_Super_Key_Pnd_Key_Pin", "#KEY-PIN", FieldType.NUMERIC, 12);
        pnd_Super_Key_Pnd_Key_Tiaa = pnd_Super_Key__R_Field_3.newFieldInGroup("pnd_Super_Key_Pnd_Key_Tiaa", "#KEY-TIAA", FieldType.STRING, 10);
        pnd_Super_Key_Pnd_Key_Tc = pnd_Super_Key__R_Field_3.newFieldInGroup("pnd_Super_Key_Pnd_Key_Tc", "#KEY-TC", FieldType.STRING, 1);

        pnd_Bene = localVariables.newGroupInRecord("pnd_Bene", "#BENE");
        pnd_Bene_Pnd_Rldt = pnd_Bene.newFieldInGroup("pnd_Bene_Pnd_Rldt", "#RLDT", FieldType.STRING, 15);

        pnd_Bene__R_Field_4 = pnd_Bene.newGroupInGroup("pnd_Bene__R_Field_4", "REDEFINE", pnd_Bene_Pnd_Rldt);
        pnd_Bene_Pnd_Date = pnd_Bene__R_Field_4.newFieldInGroup("pnd_Bene_Pnd_Date", "#DATE", FieldType.STRING, 8);
        pnd_Bene_Pnd_Time = pnd_Bene__R_Field_4.newFieldInGroup("pnd_Bene_Pnd_Time", "#TIME", FieldType.STRING, 7);
        pnd_Bene_Pnd_Bn_Pin = pnd_Bene.newFieldInGroup("pnd_Bene_Pnd_Bn_Pin", "#BN-PIN", FieldType.NUMERIC, 12);
        pnd_Bene_Pnd_Bn_Tiaa = pnd_Bene.newFieldInGroup("pnd_Bene_Pnd_Bn_Tiaa", "#BN-TIAA", FieldType.STRING, 10);
        pnd_Bene_Pnd_Bn_Cref = pnd_Bene.newFieldInGroup("pnd_Bene_Pnd_Bn_Cref", "#BN-CREF", FieldType.STRING, 10);
        pnd_Bene_Pnd_Bn_Efdt = pnd_Bene.newFieldInGroup("pnd_Bene_Pnd_Bn_Efdt", "#BN-EFDT", FieldType.STRING, 8);
        pnd_Bene_Pnd_Bn_Seq = pnd_Bene.newFieldInGroup("pnd_Bene_Pnd_Bn_Seq", "#BN-SEQ", FieldType.NUMERIC, 3);

        pnd_Bene_Pnd_Bn = pnd_Bene.newGroupArrayInGroup("pnd_Bene_Pnd_Bn", "#BN", new DbsArrayController(1, 30));
        pnd_Bene_Pnd_Bn_Type = pnd_Bene_Pnd_Bn.newFieldInGroup("pnd_Bene_Pnd_Bn_Type", "#BN-TYPE", FieldType.STRING, 1);
        pnd_Bene_Pnd_Bn_Perc = pnd_Bene_Pnd_Bn.newFieldInGroup("pnd_Bene_Pnd_Bn_Perc", "#BN-PERC", FieldType.NUMERIC, 5, 2);
        pnd_Bene_Pnd_Bn_Rc = pnd_Bene_Pnd_Bn.newFieldInGroup("pnd_Bene_Pnd_Bn_Rc", "#BN-RC", FieldType.STRING, 2);
        pnd_Bene_Pnd_Bn_Dob = pnd_Bene_Pnd_Bn.newFieldInGroup("pnd_Bene_Pnd_Bn_Dob", "#BN-DOB", FieldType.STRING, 8);
        pnd_Bene_Pnd_Bn_Scd = pnd_Bene_Pnd_Bn.newFieldInGroup("pnd_Bene_Pnd_Bn_Scd", "#BN-SCD", FieldType.STRING, 1);
        pnd_Bene_Pnd_Bn_Ssn = pnd_Bene_Pnd_Bn.newFieldInGroup("pnd_Bene_Pnd_Bn_Ssn", "#BN-SSN", FieldType.STRING, 9);
        pnd_Bene_Pnd_Bn_Name1 = pnd_Bene_Pnd_Bn.newFieldInGroup("pnd_Bene_Pnd_Bn_Name1", "#BN-NAME1", FieldType.STRING, 35);
        pnd_Bene_Pnd_Bn_Name2 = pnd_Bene_Pnd_Bn.newFieldInGroup("pnd_Bene_Pnd_Bn_Name2", "#BN-NAME2", FieldType.STRING, 35);
        pnd_Bene_Pnd_City_State_Zip = pnd_Bene.newFieldInGroup("pnd_Bene_Pnd_City_State_Zip", "#CITY-STATE-ZIP", FieldType.STRING, 35);
        pnd_Bene_Pnd_Mdm_Lname = pnd_Bene.newFieldInGroup("pnd_Bene_Pnd_Mdm_Lname", "#MDM-LNAME", FieldType.STRING, 35);
        pnd_Bene_Pnd_Mdm_Name = pnd_Bene.newFieldInGroup("pnd_Bene_Pnd_Mdm_Name", "#MDM-NAME", FieldType.STRING, 35);
        pnd_Bene_Pnd_Mdm_Ssn = pnd_Bene.newFieldInGroup("pnd_Bene_Pnd_Mdm_Ssn", "#MDM-SSN", FieldType.STRING, 9);
        pnd_Bene_Pnd_Mdm_Addr = pnd_Bene.newFieldArrayInGroup("pnd_Bene_Pnd_Mdm_Addr", "#MDM-ADDR", FieldType.STRING, 35, new DbsArrayController(1, 5));
        pnd_Bene_Pnd_Mdm_Postal_Data = pnd_Bene.newFieldInGroup("pnd_Bene_Pnd_Mdm_Postal_Data", "#MDM-POSTAL-DATA", FieldType.STRING, 32);
        pnd_Bene_Pnd_Mdm_Us_Frgn = pnd_Bene.newFieldInGroup("pnd_Bene_Pnd_Mdm_Us_Frgn", "#MDM-US-FRGN", FieldType.STRING, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_Total_Read = localVariables.newFieldInRecord("pnd_Total_Read", "#TOTAL-READ", FieldType.PACKED_DECIMAL, 10);
        pnd_Total_Add = localVariables.newFieldInRecord("pnd_Total_Add", "#TOTAL-ADD", FieldType.PACKED_DECIMAL, 10);
        pnd_Total_Delete = localVariables.newFieldInRecord("pnd_Total_Delete", "#TOTAL-DELETE", FieldType.PACKED_DECIMAL, 10);
        pnd_Total_Error = localVariables.newFieldInRecord("pnd_Total_Error", "#TOTAL-ERROR", FieldType.PACKED_DECIMAL, 10);
        pnd_Total_Print = localVariables.newFieldInRecord("pnd_Total_Print", "#TOTAL-PRINT", FieldType.PACKED_DECIMAL, 10);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.NUMERIC, 3);
        pls_Trace = WsIndependent.getInstance().newFieldInRecord("pls_Trace", "+TRACE", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_bca.reset();
        vw_bda.reset();
        vw_bma.reset();
        vw_bt.reset();

        localVariables.reset();
        pls_Trace.setInitialValue(true);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Benp9995() throws Exception
    {
        super("Benp9995");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 60 LS = 133 SG = OFF ZP = ON;//Natural: FORMAT ( 1 ) PS = 60 LS = 133 SG = OFF ZP = ON
        //*  OPEN
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        pdaMdma101.getPnd_Mdma101().reset();                                                                                                                              //Natural: RESET #MDMA101
        pdaMdma101.getPnd_Mdma101_Pnd_I_Ssn_A9().setValue("000012345");                                                                                                   //Natural: ASSIGN #MDMA101.#I-SSN-A9 := '000012345'
        DbsUtil.callnat(Mdmn101.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                           //Natural: CALLNAT 'MDMN101' #MDMA101
        if (condition(Global.isEscape())) return;
        pnd_Bene.reset();                                                                                                                                                 //Natural: RESET #BENE #BN ( * ) #BENA002 BENA011 #MDMA101 #SUPER-KEY
        pnd_Bene_Pnd_Bn.getValue("*").reset();
        pdaBena002.getPnd_Bena002().reset();
        pdaBena011.getBena011().reset();
        pdaMdma101.getPnd_Mdma101().reset();
        pnd_Super_Key.reset();
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK 1 #WORKFILE
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, pnd_Workfile)))
        {
            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT BREAK OF #I-TIAA
            if (condition(pnd_Workfile_Pnd_I_Type.equals("C")))                                                                                                           //Natural: IF #I-TYPE = 'C'
            {
                pnd_Workfile_Pnd_I_Type.setValue("S");                                                                                                                    //Natural: ASSIGN #I-TYPE := 'S'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Workfile_Pnd_I_Ss_Nmbr.equals("000000000") || pnd_Workfile_Pnd_I_Ss_Nmbr.equals("999999999")))                                              //Natural: IF #I-SS-NMBR = '000000000' OR = '999999999'
            {
                pnd_Workfile_Pnd_I_Ss_Nmbr.reset();                                                                                                                       //Natural: RESET #I-SS-NMBR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Super_Key_Pnd_Key_Tiaa.notEquals(pnd_Workfile_Pnd_I_Tiaa)))                                                                                 //Natural: IF #KEY-TIAA NE #I-TIAA
            {
                                                                                                                                                                          //Natural: PERFORM GET-MDM-DATA
                sub_Get_Mdm_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM READ-DELETE-EXISTING-BENE
                sub_Read_Delete_Existing_Bene();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pnd_Bene_Pnd_Bn_Seq.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #BN-SEQ
            pnd_Bene_Pnd_Bn_Pin.setValue(pnd_Workfile_Pnd_Ph_Pin);                                                                                                        //Natural: ASSIGN #BN-PIN := #KEY-PIN := #PH-PIN
            pnd_Super_Key_Pnd_Key_Pin.setValue(pnd_Workfile_Pnd_Ph_Pin);
            pnd_Bene_Pnd_Bn_Tiaa.setValue(pnd_Workfile_Pnd_I_Tiaa);                                                                                                       //Natural: ASSIGN #BN-TIAA := #KEY-TIAA := #I-TIAA
            pnd_Super_Key_Pnd_Key_Tiaa.setValue(pnd_Workfile_Pnd_I_Tiaa);
            pnd_Bene_Pnd_Bn_Cref.setValue(pnd_Workfile_Pnd_I_Cref);                                                                                                       //Natural: ASSIGN #BN-CREF := #I-CREF
            pnd_Bene_Pnd_Bn_Efdt.setValue(pnd_Workfile_Pnd_I_Efdt);                                                                                                       //Natural: ASSIGN #BN-EFDT := #I-EFDT
            pnd_Bene_Pnd_Bn_Type.getValue(pnd_Bene_Pnd_Bn_Seq).setValue(pnd_Workfile_Pnd_I_Type);                                                                         //Natural: ASSIGN #BN-TYPE ( #BN-SEQ ) := #I-TYPE
            pnd_Bene_Pnd_Bn_Perc.getValue(pnd_Bene_Pnd_Bn_Seq).setValue(pnd_Workfile_Pnd_I_Perc);                                                                         //Natural: ASSIGN #BN-PERC ( #BN-SEQ ) := #I-PERC
            pnd_Bene_Pnd_Bn_Rc.getValue(pnd_Bene_Pnd_Bn_Seq).setValue(pnd_Workfile_Pnd_I_Rc);                                                                             //Natural: ASSIGN #BN-RC ( #BN-SEQ ) := #I-RC
            pnd_Bene_Pnd_Bn_Dob.getValue(pnd_Bene_Pnd_Bn_Seq).setValue(pnd_Workfile_Pnd_I_Dob);                                                                           //Natural: ASSIGN #BN-DOB ( #BN-SEQ ) := #I-DOB
            pnd_Bene_Pnd_Bn_Scd.getValue(pnd_Bene_Pnd_Bn_Seq).setValue(pnd_Workfile_Pnd_I_Ss_Cd);                                                                         //Natural: ASSIGN #BN-SCD ( #BN-SEQ ) := #I-SS-CD
            pnd_Bene_Pnd_Bn_Ssn.getValue(pnd_Bene_Pnd_Bn_Seq).setValue(pnd_Workfile_Pnd_I_Ss_Nmbr);                                                                       //Natural: ASSIGN #BN-SSN ( #BN-SEQ ) := #I-SS-NMBR
            pnd_Bene_Pnd_Bn_Name1.getValue(pnd_Bene_Pnd_Bn_Seq).setValue(pnd_Workfile_Pnd_I_Name1);                                                                       //Natural: ASSIGN #BN-NAME1 ( #BN-SEQ ) := #I-NAME1
            pnd_Bene_Pnd_Bn_Name2.getValue(pnd_Bene_Pnd_Bn_Seq).setValue(pnd_Workfile_Pnd_I_Name2);                                                                       //Natural: ASSIGN #BN-NAME2 ( #BN-SEQ ) := #I-NAME2
            getReports().display(1, ReportOption.NOTITLE,"SSN",                                                                                                           //Natural: DISPLAY ( 1 ) NOTITLE 'SSN' #PH-SSN 'PIN' #PH-PIN 'TIAA' #I-TIAA 'CREF' #I-CREF 'EfDt' #I-EFDT 'PC' #I-TYPE '%' #I-PERC 'RC' #I-RC 'DoB' #I-DOB 'S' #I-SS-CD 'SSN' #I-SS-NMBR 'Name1' #I-NAME1
            		pnd_Workfile_Pnd_Ph_Ssn,"PIN",
            		pnd_Workfile_Pnd_Ph_Pin,"TIAA",
            		pnd_Workfile_Pnd_I_Tiaa,"CREF",
            		pnd_Workfile_Pnd_I_Cref,"EfDt",
            		pnd_Workfile_Pnd_I_Efdt,"PC",
            		pnd_Workfile_Pnd_I_Type,"%",
            		pnd_Workfile_Pnd_I_Perc,"RC",
            		pnd_Workfile_Pnd_I_Rc,"DoB",
            		pnd_Workfile_Pnd_I_Dob,"S",
            		pnd_Workfile_Pnd_I_Ss_Cd,"SSN",
            		pnd_Workfile_Pnd_I_Ss_Nmbr,"Name1",
            		pnd_Workfile_Pnd_I_Name1);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Workfile_Pnd_I_Name2.notEquals(" ")))                                                                                                       //Natural: IF #I-NAME2 NE ' '
            {
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(82),pnd_Workfile_Pnd_I_Name2);                                                                  //Natural: WRITE ( 1 ) 82T #I-NAME2
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (Global.isEscape()) return;
        getReports().write(0, "Last Seq",pnd_Bene_Pnd_Bn_Seq);                                                                                                            //Natural: WRITE ( 0 ) 'Last Seq' #BN-SEQ
        if (Global.isEscape()) return;
        if (condition(pnd_Bene_Pnd_Bn_Seq.greater(getZero())))                                                                                                            //Natural: IF #BN-SEQ > 0
        {
                                                                                                                                                                          //Natural: PERFORM CREATE-WORK-REQUEST
            sub_Create_Work_Request();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM ADD-BENE-DESIGNATION
            sub_Add_Bene_Designation();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM ADD-BENE-ACTIVITY
            sub_Add_Bene_Activity();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CREATE-AUDIT-TRAIL
            sub_Create_Audit_Trail();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CALL-POST
            sub_Call_Post();
            if (condition(Global.isEscape())) {return;}
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
        //*  CLOSE
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"Total input PIN/Contract   :",pnd_Total_Read, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"Total PIN/Contract errored :",pnd_Total_Error,  //Natural: WRITE ( 1 ) NOTITLE / 'Total input PIN/Contract   :' #TOTAL-READ / 'Total PIN/Contract errored :' #TOTAL-ERROR / 'Total PIN/Contract deleted :' #TOTAL-DELETE / 'Total PIN/Contract added   :' #TOTAL-ADD / 'Total PIN/Contract letter  :' #TOTAL-PRINT
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"Total PIN/Contract deleted :",pnd_Total_Delete, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"Total PIN/Contract added   :",pnd_Total_Add, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"Total PIN/Contract letter  :",pnd_Total_Print, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-BENE-ACTIVITY
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-BENE-DESIGNATION
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-POST
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-AUDIT-TRAIL
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-WORK-REQUEST
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-MDM-DATA
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-DELETE-EXISTING-BENE
    }
    private void sub_Add_Bene_Activity() throws Exception                                                                                                                 //Natural: ADD-BENE-ACTIVITY
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, "Subroutine ADD-BENE-ACTIVITY",pnd_Bene_Pnd_Rldt);                                                                                      //Natural: WRITE ( 0 ) 'Subroutine ADD-BENE-ACTIVITY' #RLDT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //*  ADD
        //*  UPDATE/AWAITING PRINT
        //*  REGULAR MAIL
        //*  MDM
        //*  VERIFY NOT REQUIRED
        //*  END TRANSACTION
        pdaBena010.getPnd_Bena010().reset();                                                                                                                              //Natural: RESET #BENA010
        pdaBena010.getPnd_Bena010_Pnd_Actvty_Mode().setValue("A");                                                                                                        //Natural: ASSIGN #BENA010.#ACTVTY-MODE := 'A'
        pdaBena010.getPnd_Bena010_Pnd_Wpid().setValue("TA MBM");                                                                                                          //Natural: ASSIGN #BENA010.#WPID := 'TA MBM'
        pdaBena010.getPnd_Bena010_Pnd_Wpid_Unit().setValue("CTADME");                                                                                                     //Natural: ASSIGN #BENA010.#WPID-UNIT := 'CTADME'
        pdaBena010.getPnd_Bena010_Pnd_Wpid_Stat().setValue("7501");                                                                                                       //Natural: ASSIGN #BENA010.#WPID-STAT := '7501'
        pdaBena010.getPnd_Bena010_Pnd_Pin().setValue(pnd_Bene_Pnd_Bn_Pin);                                                                                                //Natural: ASSIGN #BENA010.#PIN := #BN-PIN
        pdaBena010.getPnd_Bena010_Pnd_Rqst_Timestamp().setValue(pnd_Bene_Pnd_Rldt);                                                                                       //Natural: ASSIGN #BENA010.#RQST-TIMESTAMP := #RLDT
        pdaBena010.getPnd_Bena010_Pnd_Sys_Dte().setValue(pnd_Bene_Pnd_Date);                                                                                              //Natural: ASSIGN #BENA010.#SYS-DTE := #DATE
        pdaBena010.getPnd_Bena010_Pnd_Rqst_Cmpltd_Dte().setValue(pnd_Bene_Pnd_Date);                                                                                      //Natural: ASSIGN #BENA010.#RQST-CMPLTD-DTE := #DATE
        pdaBena010.getPnd_Bena010_Pnd_Eff_Dte().setValue(pnd_Bene_Pnd_Date);                                                                                              //Natural: ASSIGN #BENA010.#EFF-DTE := #DATE
        pdaBena010.getPnd_Bena010_Pnd_Seq_Nmbr().setValue(1);                                                                                                             //Natural: ASSIGN #BENA010.#SEQ-NMBR := 1
        pdaBena010.getPnd_Bena010_Pnd_Next_Seq_Nmbr().setValue(0);                                                                                                        //Natural: ASSIGN #BENA010.#NEXT-SEQ-NMBR := 0
        pdaBena010.getPnd_Bena010_Pnd_Max_Desig().setValue(1);                                                                                                            //Natural: ASSIGN #BENA010.#MAX-DESIG := 1
        pdaBena010.getPnd_Bena010_Pnd_Tiaa_Cntrct().getValue(1).setValue(pnd_Bene_Pnd_Bn_Tiaa);                                                                           //Natural: ASSIGN #BENA010.#TIAA-CNTRCT ( 1 ) := #BN-TIAA
        pdaBena010.getPnd_Bena010_Pnd_Cref_Cntrct().getValue(1).setValue(pnd_Bene_Pnd_Bn_Cref);                                                                           //Natural: ASSIGN #BENA010.#CREF-CNTRCT ( 1 ) := #BN-CREF
        pdaBena010.getPnd_Bena010_Pnd_Diff_Dsgntn().setValue(" ");                                                                                                        //Natural: ASSIGN #BENA010.#DIFF-DSGNTN := ' '
        pdaBena010.getPnd_Bena010_Pnd_Name().setValue(pnd_Bene_Pnd_Mdm_Name);                                                                                             //Natural: ASSIGN #BENA010.#NAME := #MDM-NAME
        pdaBena010.getPnd_Bena010_Pnd_Ph_Last_Nme().setValue(pnd_Bene_Pnd_Mdm_Lname);                                                                                     //Natural: ASSIGN #BENA010.#PH-LAST-NME := #MDM-LNAME
        pdaBena010.getPnd_Bena010_Pnd_Owner_Name().setValue(" ");                                                                                                         //Natural: ASSIGN #BENA010.#OWNER-NAME := ' '
        pdaBena010.getPnd_Bena010_Pnd_Lttr_Optn().setValue("2");                                                                                                          //Natural: ASSIGN #BENA010.#LTTR-OPTN := '2'
        pdaBena010.getPnd_Bena010_Pnd_Lttr_Info().setValue(" ");                                                                                                          //Natural: ASSIGN #BENA010.#LTTR-INFO := ' '
        pdaBena010.getPnd_Bena010_Pnd_Mail_Addr().getValue(1,":",5).setValue(pnd_Bene_Pnd_Mdm_Addr.getValue(1,":",5));                                                    //Natural: ASSIGN #BENA010.#MAIL-ADDR ( 1:5 ) := #MDM-ADDR ( 1:5 )
        pdaBena010.getPnd_Bena010_Pnd_Mail_Postal_Data().setValue(pnd_Bene_Pnd_Mdm_Postal_Data);                                                                          //Natural: ASSIGN #BENA010.#MAIL-POSTAL-DATA := #MDM-POSTAL-DATA
        pdaBena010.getPnd_Bena010_Pnd_Us_Frgn().setValue(pnd_Bene_Pnd_Mdm_Us_Frgn);                                                                                       //Natural: ASSIGN #BENA010.#US-FRGN := #MDM-US-FRGN
        pdaBena010.getPnd_Bena010_Pnd_Addr_Srce().setValue("1");                                                                                                          //Natural: ASSIGN #BENA010.#ADDR-SRCE := '1'
        pdaBena010.getPnd_Bena010_Pnd_Vrfy_Ind().setValue("2");                                                                                                           //Natural: ASSIGN #BENA010.#VRFY-IND := '2'
        pdaBena010.getPnd_Bena010_Pnd_Vrfy_Reason().setValue(" ");                                                                                                        //Natural: ASSIGN #BENA010.#VRFY-REASON := ' '
        pdaBena010.getPnd_Bena010_Pnd_Last_Updt_Userid().setValue("BENEWEB");                                                                                             //Natural: ASSIGN #BENA010.#LAST-UPDT-USERID := 'BENEWEB'
        pdaBena010.getPnd_Bena010_Pnd_Last_Updt_Dte().setValue(pnd_Bene_Pnd_Date);                                                                                        //Natural: ASSIGN #BENA010.#LAST-UPDT-DTE := #DATE
        pdaBena010.getPnd_Bena010_Pnd_Last_Updt_Tme().setValue(pnd_Bene_Pnd_Time);                                                                                        //Natural: ASSIGN #BENA010.#LAST-UPDT-TME := #TIME
        pdaBena010.getPnd_Bena010_Pnd_Vrfy_Userid().setValue(" ");                                                                                                        //Natural: ASSIGN #BENA010.#VRFY-USERID := ' '
        pdaBena010.getPnd_Bena010_Pnd_Vrfy_Dte().setValue(" ");                                                                                                           //Natural: ASSIGN #BENA010.#VRFY-DTE := ' '
        pdaBena010.getPnd_Bena010_Pnd_Vrfy_Tme().setValue(" ");                                                                                                           //Natural: ASSIGN #BENA010.#VRFY-TME := ' '
        pdaBena010.getPnd_Bena010_Pnd_Partial_Data_Ind().setValue("N");                                                                                                   //Natural: ASSIGN #BENA010.#PARTIAL-DATA-IND := 'N'
        pdaBena010.getPnd_Bena010_Pnd_Et_Ind().setValue("Y");                                                                                                             //Natural: ASSIGN #BENA010.#ET-IND := 'Y'
        DbsUtil.callnat(Benn010.class , getCurrentProcessState(), pdaBena010.getPnd_Bena010());                                                                           //Natural: CALLNAT 'BENN010' #BENA010
        if (condition(Global.isEscape())) return;
        if (condition(pdaBena010.getPnd_Bena010_Pnd_Err_Ind().notEquals(" ")))                                                                                            //Natural: IF #BENA010.#ERR-IND NE ' '
        {
            getReports().write(0, "BENN010",pdaBena010.getPnd_Bena010_Pnd_Rtrn_Msg());                                                                                    //Natural: WRITE ( 0 ) 'BENN010' #BENA010.#RTRN-MSG
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADD-BENE-ACTIVITY
    }
    private void sub_Add_Bene_Designation() throws Exception                                                                                                              //Natural: ADD-BENE-DESIGNATION
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, "Subroutine ADD-BENE-DESIGNATION",pnd_Bene_Pnd_Bn_Seq);                                                                                 //Natural: WRITE ( 0 ) 'Subroutine ADD-BENE-DESIGNATION' #BN-SEQ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        if (condition(pnd_Bene_Pnd_Rldt.equals(" ")))                                                                                                                     //Natural: IF #RLDT = ' '
        {
            pnd_Bene_Pnd_Date.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                            //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO #DATE
            pnd_Bene_Pnd_Time.setValueEdited(Global.getTIMX(),new ReportEditMask("HHIISST"));                                                                             //Natural: MOVE EDITED *TIMX ( EM = HHIISST ) TO #TIME
            //*  ADD
            //*  BOTH
            //*  CURRENT
            //*  DA
            //*  ONLINE
            //*  BATCH
            //*  ISSUE END TRANSACTION
            //*  PERCENT
            //*  CURRENT
        }                                                                                                                                                                 //Natural: END-IF
        pdaBena002.getPnd_Bena002_Pnd_Actvty_Mode().setValue("A");                                                                                                        //Natural: ASSIGN #BENA002.#ACTVTY-MODE := 'A'
        pdaBena002.getPnd_Bena002_Pnd_Sys_Dte().setValue(pnd_Bene_Pnd_Date);                                                                                              //Natural: ASSIGN #BENA002.#SYS-DTE := #DATE
        pdaBena002.getPnd_Bena002_Pnd_Pin().setValue(pnd_Bene_Pnd_Bn_Pin);                                                                                                //Natural: ASSIGN #BENA002.#PIN := #BN-PIN
        pdaBena002.getPnd_Bena002_Pnd_Tiaa_Cntrct().setValue(pnd_Bene_Pnd_Bn_Tiaa);                                                                                       //Natural: ASSIGN #BENA002.#TIAA-CNTRCT := #BN-TIAA
        pdaBena002.getPnd_Bena002_Pnd_Cref_Cntrct().setValue(pnd_Bene_Pnd_Bn_Cref);                                                                                       //Natural: ASSIGN #BENA002.#CREF-CNTRCT := #BN-CREF
        pdaBena002.getPnd_Bena002_Pnd_Tiaa_Cref_Ind().setValue(" ");                                                                                                      //Natural: ASSIGN #BENA002.#TIAA-CREF-IND := ' '
        pdaBena002.getPnd_Bena002_Pnd_Cntrct_Stat().setValue("C");                                                                                                        //Natural: ASSIGN #BENA002.#CNTRCT-STAT := 'C'
        pdaBena002.getPnd_Bena002_Pnd_Cntrct_Type().setValue("D");                                                                                                        //Natural: ASSIGN #BENA002.#CNTRCT-TYPE := 'D'
        pdaBena002.getPnd_Bena002_Pnd_Rqst_Timestamp().setValue(pnd_Bene_Pnd_Rldt);                                                                                       //Natural: ASSIGN #BENA002.#RQST-TIMESTAMP := #RLDT
        pdaBena002.getPnd_Bena002_Pnd_Sys_Dte().setValue(pnd_Bene_Pnd_Date);                                                                                              //Natural: ASSIGN #BENA002.#SYS-DTE := #DATE
        pdaBena002.getPnd_Bena002_Pnd_Eff_Dte().setValue(pnd_Bene_Pnd_Bn_Efdt);                                                                                           //Natural: ASSIGN #BENA002.#EFF-DTE := #BN-EFDT
        pdaBena002.getPnd_Bena002_Pnd_Mos_Ind().setValue("N");                                                                                                            //Natural: ASSIGN #BENA002.#MOS-IND := 'N'
        pdaBena002.getPnd_Bena002_Pnd_Mos_Irvcbl().setValue("N");                                                                                                         //Natural: ASSIGN #BENA002.#MOS-IRVCBL := 'N'
        pdaBena002.getPnd_Bena002_Pnd_Rcrd_Last_Updt_Dte().setValue(pnd_Bene_Pnd_Date);                                                                                   //Natural: ASSIGN #BENA002.#RCRD-LAST-UPDT-DTE := #DATE
        pdaBena002.getPnd_Bena002_Pnd_Rcrd_Last_Updt_Tme().setValue(pnd_Bene_Pnd_Time);                                                                                   //Natural: ASSIGN #BENA002.#RCRD-LAST-UPDT-TME := #TIME
        pdaBena002.getPnd_Bena002_Pnd_Rcrd_Last_Updt_Userid().setValue("BENEWEB");                                                                                        //Natural: ASSIGN #BENA002.#RCRD-LAST-UPDT-USERID := 'BENEWEB'
        pdaBena002.getPnd_Bena002_Pnd_Last_Dsgntn_Srce().setValue("O");                                                                                                   //Natural: ASSIGN #BENA002.#LAST-DSGNTN-SRCE := 'O'
        pdaBena002.getPnd_Bena002_Pnd_Last_Dsgntn_System().setValue("B");                                                                                                 //Natural: ASSIGN #BENA002.#LAST-DSGNTN-SYSTEM := 'B'
        pdaBena002.getPnd_Bena002_Pnd_Last_Dsgntn_Userid().setValue("BENEWEB");                                                                                           //Natural: ASSIGN #BENA002.#LAST-DSGNTN-USERID := 'BENEWEB'
        pdaBena002.getPnd_Bena002_Pnd_Last_Dsgntn_Dte().setValue(pnd_Bene_Pnd_Date);                                                                                      //Natural: ASSIGN #BENA002.#LAST-DSGNTN-DTE := #DATE
        pdaBena002.getPnd_Bena002_Pnd_Last_Dsgntn_Tme().setValue(pnd_Bene_Pnd_Time);                                                                                      //Natural: ASSIGN #BENA002.#LAST-DSGNTN-TME := #TIME
        pdaBena002.getPnd_Bena002_Pnd_Bene_Fix_Dte().setValue(pnd_Bene_Pnd_Date);                                                                                         //Natural: ASSIGN #BENA002.#BENE-FIX-DTE := #DATE
        pdaBena002.getPnd_Bena002_Pnd_Max_Desig().setValue(pnd_Bene_Pnd_Bn_Seq);                                                                                          //Natural: ASSIGN #BENA002.#MAX-DESIG := #BN-SEQ
        pdaBena002.getPnd_Bena002_Pnd_Et_Ind().setValue("Y");                                                                                                             //Natural: ASSIGN #BENA002.#ET-IND := 'Y'
        FOR01:                                                                                                                                                            //Natural: FOR #X = 1 TO #BN-SEQ
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(pnd_Bene_Pnd_Bn_Seq)); pnd_X.nadd(1))
        {
            pdaBena002.getPnd_Bena002_Pnd_Bene_Seq_Nmbr().getValue(pnd_X).setValue(pnd_X);                                                                                //Natural: ASSIGN #BENA002.#BENE-SEQ-NMBR ( #X ) := #X
            pdaBena002.getPnd_Bena002_Pnd_Bene_Type().getValue(pnd_X).setValue(pnd_Bene_Pnd_Bn_Type.getValue(pnd_X));                                                     //Natural: ASSIGN #BENA002.#BENE-TYPE ( #X ) := #BN-TYPE ( #X )
            pdaBena002.getPnd_Bena002_Pnd_Bene_Name1().getValue(pnd_X).setValue(pnd_Bene_Pnd_Bn_Name1.getValue(pnd_X));                                                   //Natural: ASSIGN #BENA002.#BENE-NAME1 ( #X ) := #BN-NAME1 ( #X )
            pdaBena002.getPnd_Bena002_Pnd_Bene_Name2().getValue(pnd_X).setValue(pnd_Bene_Pnd_Bn_Name2.getValue(pnd_X));                                                   //Natural: ASSIGN #BENA002.#BENE-NAME2 ( #X ) := #BN-NAME2 ( #X )
            pdaBena002.getPnd_Bena002_Pnd_Dte_Birth_Trust().getValue(pnd_X).setValue(pnd_Bene_Pnd_Bn_Dob.getValue(pnd_X));                                                //Natural: ASSIGN #BENA002.#DTE-BIRTH-TRUST ( #X ) := #BN-DOB ( #X )
            pdaBena002.getPnd_Bena002_Pnd_Rltn_Cd().getValue(pnd_X).setValue(pnd_Bene_Pnd_Bn_Rc.getValue(pnd_X));                                                         //Natural: ASSIGN #BENA002.#RLTN-CD ( #X ) := #BN-RC ( #X )
            pdaBena002.getPnd_Bena002_Pnd_Rltn_Free_Txt().getValue(pnd_X).setValue(" ");                                                                                  //Natural: ASSIGN #BENA002.#RLTN-FREE-TXT ( #X ) := ' '
            pdaBena002.getPnd_Bena002_Pnd_Ss_Cd().getValue(pnd_X).setValue(pnd_Bene_Pnd_Bn_Scd.getValue(pnd_X));                                                          //Natural: ASSIGN #BENA002.#SS-CD ( #X ) := #BN-SCD ( #X )
            pdaBena002.getPnd_Bena002_Pnd_Ss_Nmbr().getValue(pnd_X).setValue(pnd_Bene_Pnd_Bn_Ssn.getValue(pnd_X));                                                        //Natural: ASSIGN #BENA002.#SS-NMBR ( #X ) := #BN-SSN ( #X )
            pdaBena002.getPnd_Bena002_Pnd_Perc_Share_Ind().getValue(pnd_X).setValue("P");                                                                                 //Natural: ASSIGN #BENA002.#PERC-SHARE-IND ( #X ) := 'P'
            pdaBena002.getPnd_Bena002_Pnd_Share_Perc().getValue(pnd_X).setValue(pnd_Bene_Pnd_Bn_Perc.getValue(pnd_X));                                                    //Natural: ASSIGN #BENA002.#SHARE-PERC ( #X ) := #BN-PERC ( #X )
            pdaBena002.getPnd_Bena002_Pnd_Share_Dtor().getValue(pnd_X).setValue(0);                                                                                       //Natural: ASSIGN #BENA002.#SHARE-DTOR ( #X ) := 0
            pdaBena002.getPnd_Bena002_Pnd_Share_Ntor().getValue(pnd_X).setValue(0);                                                                                       //Natural: ASSIGN #BENA002.#SHARE-NTOR ( #X ) := 0
            pdaBena002.getPnd_Bena002_Pnd_Bene_Irvcbl().getValue(pnd_X).setValue("N");                                                                                    //Natural: ASSIGN #BENA002.#BENE-IRVCBL ( #X ) := 'N'
            pdaBena002.getPnd_Bena002_Pnd_Stlmnt_Rstrctn().getValue(pnd_X).setValue("N");                                                                                 //Natural: ASSIGN #BENA002.#STLMNT-RSTRCTN ( #X ) := 'N'
            pdaBena002.getPnd_Bena002_Pnd_Spcl_Txt1().getValue(pnd_X).setValue(" ");                                                                                      //Natural: ASSIGN #BENA002.#SPCL-TXT1 ( #X ) := ' '
            pdaBena002.getPnd_Bena002_Pnd_Spcl_Txt2().getValue(pnd_X).setValue(" ");                                                                                      //Natural: ASSIGN #BENA002.#SPCL-TXT2 ( #X ) := ' '
            pdaBena002.getPnd_Bena002_Pnd_Spcl_Txt3().getValue(pnd_X).setValue(" ");                                                                                      //Natural: ASSIGN #BENA002.#SPCL-TXT3 ( #X ) := ' '
            pdaBena002.getPnd_Bena002_Pnd_Spcl_Txt2().getValue(pnd_X).setValue(" ");                                                                                      //Natural: ASSIGN #BENA002.#SPCL-TXT2 ( #X ) := ' '
            pdaBena002.getPnd_Bena002_Pnd_Dflt_To_Estate().getValue(pnd_X).setValue("N");                                                                                 //Natural: ASSIGN #BENA002.#DFLT-TO-ESTATE ( #X ) := 'N'
            pdaBena002.getPnd_Bena002_Pnd_Bene_Stat().getValue(pnd_X).setValue("C");                                                                                      //Natural: ASSIGN #BENA002.#BENE-STAT ( #X ) := 'C'
            pdaBena002.getPnd_Bena002_Pnd_Bene_Last_Updt_Dte().getValue(pnd_X).setValue(pnd_Bene_Pnd_Date);                                                               //Natural: ASSIGN #BENA002.#BENE-LAST-UPDT-DTE ( #X ) := #DATE
            pdaBena002.getPnd_Bena002_Pnd_Bene_Last_Updt_Tme().getValue(pnd_X).setValue(pnd_Bene_Pnd_Time);                                                               //Natural: ASSIGN #BENA002.#BENE-LAST-UPDT-TME ( #X ) := #TIME
            pdaBena002.getPnd_Bena002_Pnd_Bene_Addr1().getValue(pnd_X).setValue(" ");                                                                                     //Natural: ASSIGN #BENA002.#BENE-ADDR1 ( #X ) := ' '
            pdaBena002.getPnd_Bena002_Pnd_Bene_Addr2().getValue(pnd_X).setValue(" ");                                                                                     //Natural: ASSIGN #BENA002.#BENE-ADDR2 ( #X ) := ' '
            pdaBena002.getPnd_Bena002_Pnd_Bene_Addr3_City().getValue(pnd_X).setValue(" ");                                                                                //Natural: ASSIGN #BENA002.#BENE-ADDR3-CITY ( #X ) := ' '
            pdaBena002.getPnd_Bena002_Pnd_Bene_Country().getValue(pnd_X).setValue(" ");                                                                                   //Natural: ASSIGN #BENA002.#BENE-COUNTRY ( #X ) := ' '
            pdaBena002.getPnd_Bena002_Pnd_Bene_State().getValue(pnd_X).setValue(" ");                                                                                     //Natural: ASSIGN #BENA002.#BENE-STATE ( #X ) := ' '
            pdaBena002.getPnd_Bena002_Pnd_Bene_Zip().getValue(pnd_X).setValue(" ");                                                                                       //Natural: ASSIGN #BENA002.#BENE-ZIP ( #X ) := ' '
            pdaBena002.getPnd_Bena002_Pnd_Bene_Phone().getValue(pnd_X).setValue(" ");                                                                                     //Natural: ASSIGN #BENA002.#BENE-PHONE ( #X ) := ' '
            pdaBena002.getPnd_Bena002_Pnd_Bene_Gender().getValue(pnd_X).setValue(" ");                                                                                    //Natural: ASSIGN #BENA002.#BENE-GENDER ( #X ) := ' '
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        DbsUtil.callnat(Benn002.class , getCurrentProcessState(), pdaBena002.getPnd_Bena002());                                                                           //Natural: CALLNAT 'BENN002' #BENA002
        if (condition(Global.isEscape())) return;
        if (condition(pdaBena002.getPnd_Bena002_Pnd_Err_Ind().notEquals(" ")))                                                                                            //Natural: IF #BENA002.#ERR-IND NE ' '
        {
            pnd_Total_Error.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #TOTAL-ERROR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Total_Add.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #TOTAL-ADD
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADD-BENE-DESIGNATION
    }
    private void sub_Call_Post() throws Exception                                                                                                                         //Natural: CALL-POST
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, "Subroutine CALL-POST");                                                                                                                //Natural: WRITE ( 0 ) 'Subroutine CALL-POST'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        DbsUtil.callnat(Benn013.class , getCurrentProcessState(), pdaBena011.getBena011(), pdaBena002.getPnd_Bena002());                                                  //Natural: CALLNAT 'BENN013' BENA011 #BENA002
        if (condition(Global.isEscape())) return;
        if (condition(pdaBena011.getBena011_Pnd_Err_Ind().notEquals(" ")))                                                                                                //Natural: IF BENA011.#ERR-IND NE ' '
        {
            getReports().write(0, "BENN013",pdaBena011.getBena011_Pnd_Err_Ind());                                                                                         //Natural: WRITE ( 0 ) 'BENN013' BENA011.#ERR-IND
            if (Global.isEscape()) return;
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Total_Print.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #TOTAL-PRINT
        }                                                                                                                                                                 //Natural: END-IF
        //*  CALL-POST
    }
    private void sub_Create_Audit_Trail() throws Exception                                                                                                                //Natural: CREATE-AUDIT-TRAIL
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, "Subroutine CREATE-AUDIT-TRAIL",pnd_Bene_Pnd_Rldt);                                                                                     //Natural: WRITE ( 0 ) 'Subroutine CREATE-AUDIT-TRAIL' #RLDT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //*  ADD
        //*  UPDATE/AWAITING PRINT
        //*  SELECTED
        //*  BOTH
        //*  REGULAR MAIL
        pdaBena011.getBena011().reset();                                                                                                                                  //Natural: RESET BENA011
        pdaBena011.getBena011_Pnd_Actvty_Mode().setValue("A");                                                                                                            //Natural: ASSIGN BENA011.#ACTVTY-MODE := 'A'
        pdaBena011.getBena011_Pnd_Bene_Title().setValue("**** Beneficiary Maintenance System ****");                                                                      //Natural: ASSIGN BENA011.#BENE-TITLE := '**** Beneficiary Maintenance System ****'
        pdaBena011.getBena011_Pnd_Wpid().setValue("TA MBM");                                                                                                              //Natural: ASSIGN BENA011.#WPID := 'TA MBM'
        pdaBena011.getBena011_Pnd_Wpid_Status().setValue("7501");                                                                                                         //Natural: ASSIGN BENA011.#WPID-STATUS := '7501'
        pdaBena011.getBena011_Pnd_Wpid_Unit().setValue("CTADME");                                                                                                         //Natural: ASSIGN BENA011.#WPID-UNIT := 'CTADME'
        pdaBena011.getBena011_Pnd_Nas_Name().setValue(pnd_Bene_Pnd_Mdm_Name);                                                                                             //Natural: ASSIGN BENA011.#NAS-NAME := #MDM-NAME
        pdaBena011.getBena011_Pnd_Ssn().setValue(pnd_Bene_Pnd_Mdm_Ssn);                                                                                                   //Natural: ASSIGN BENA011.#SSN := #MDM-SSN
        pdaBena011.getBena011_Pnd_Pin().setValue(pnd_Bene_Pnd_Bn_Pin);                                                                                                    //Natural: ASSIGN BENA011.#PIN := #BN-PIN
        pdaBena011.getBena011_Pnd_Userid().setValue("BENEWEB");                                                                                                           //Natural: ASSIGN BENA011.#USERID := 'BENEWEB'
        pdaBena011.getBena011_Pnd_Rqst_Log_Dte_Tme().setValue(pnd_Bene_Pnd_Rldt);                                                                                         //Natural: ASSIGN BENA011.#RQST-LOG-DTE-TME := #RLDT
        pdaBena011.getBena011_Pnd_Sys_Dte().setValue(pnd_Bene_Pnd_Date);                                                                                                  //Natural: ASSIGN BENA011.#SYS-DTE := #DATE
        pdaBena011.getBena011_Pnd_Max_Cntrct().setValue(1);                                                                                                               //Natural: ASSIGN BENA011.#MAX-CNTRCT := 1
        pdaBena011.getBena011_Pnd_All_Cntrcts_Slctd().setValue(1);                                                                                                        //Natural: ASSIGN BENA011.#ALL-CNTRCTS-SLCTD := 1
        pdaBena011.getBena011_Pnd_Addr_Srce().setValue("1");                                                                                                              //Natural: ASSIGN BENA011.#ADDR-SRCE := '1'
        pdaBena011.getBena011_Pnd_Slctd().getValue(1).setValue("S");                                                                                                      //Natural: ASSIGN BENA011.#SLCTD ( 1 ) := 'S'
        pdaBena011.getBena011_Pnd_Tiaa_Cref_Ind().getValue(1).setValue(" ");                                                                                              //Natural: ASSIGN BENA011.#TIAA-CREF-IND ( 1 ) := ' '
        pdaBena011.getBena011_Pnd_Tiaa_Cntrct().getValue(1).setValue(pnd_Bene_Pnd_Bn_Tiaa);                                                                               //Natural: ASSIGN BENA011.#TIAA-CNTRCT ( 1 ) := #BN-TIAA
        pdaBena011.getBena011_Pnd_Cref_Cntrct().getValue(1).setValue(pnd_Bene_Pnd_Bn_Cref);                                                                               //Natural: ASSIGN BENA011.#CREF-CNTRCT ( 1 ) := #BN-CREF
        pdaBena011.getBena011_Pnd_Eff_Dte().setValue(pnd_Bene_Pnd_Bn_Efdt);                                                                                               //Natural: ASSIGN BENA011.#EFF-DTE := #BN-EFDT
        pdaBena011.getBena011_Pnd_Pymnt_Chld_Dcsd_Ind().setValue(" ");                                                                                                    //Natural: ASSIGN BENA011.#PYMNT-CHLD-DCSD-IND := ' '
        pdaBena011.getBena011_Pnd_Exempt_Spouse_Rights().setValue(" ");                                                                                                   //Natural: ASSIGN BENA011.#EXEMPT-SPOUSE-RIGHTS := ' '
        pdaBena011.getBena011_Pnd_Spouse_Waived_Bnfts().setValue(" ");                                                                                                    //Natural: ASSIGN BENA011.#SPOUSE-WAIVED-BNFTS := ' '
        pdaBena011.getBena011_Pnd_Trust_Data_In_Fldr().setValue(" ");                                                                                                     //Natural: ASSIGN BENA011.#TRUST-DATA-IN-FLDR := ' '
        pdaBena011.getBena011_Pnd_Bene_Addr_In_Fldr().setValue(" ");                                                                                                      //Natural: ASSIGN BENA011.#BENE-ADDR-IN-FLDR := ' '
        pdaBena011.getBena011_Pnd_Co_Owner_In_Fldr().setValue(" ");                                                                                                       //Natural: ASSIGN BENA011.#CO-OWNER-IN-FLDR := ' '
        pdaBena011.getBena011_Pnd_Chng_Pwr_Atty().setValue(" ");                                                                                                          //Natural: ASSIGN BENA011.#CHNG-PWR-ATTY := ' '
        pdaBena011.getBena011_Pnd_Fldr_Min().setValue(" ");                                                                                                               //Natural: ASSIGN BENA011.#FLDR-MIN := ' '
        pdaBena011.getBena011_Pnd_Fldr_Srce_Id().setValue(" ");                                                                                                           //Natural: ASSIGN BENA011.#FLDR-SRCE-ID := ' '
        pdaBena011.getBena011_Pnd_Fldr_Log_Dte_Tme().setValue(" ");                                                                                                       //Natural: ASSIGN BENA011.#FLDR-LOG-DTE-TME := ' '
        pdaBena011.getBena011_Pnd_Diff_Dsgntn().setValue(" ");                                                                                                            //Natural: ASSIGN BENA011.#DIFF-DSGNTN := ' '
        pdaBena011.getBena011_Pnd_Lttr_Option().setValue("2");                                                                                                            //Natural: ASSIGN BENA011.#LTTR-OPTION := '2'
        pdaBena011.getBena011_Pnd_Lttr_Info().setValue(" ");                                                                                                              //Natural: ASSIGN BENA011.#LTTR-INFO := ' '
        pdaBena011.getBena011_Pnd_Mos_Ind().setValue("N");                                                                                                                //Natural: ASSIGN BENA011.#MOS-IND := 'N'
        pdaBena011.getBena011_Pnd_Mos_Irvcbl().setValue("N");                                                                                                             //Natural: ASSIGN BENA011.#MOS-IRVCBL := 'N'
        pdaBena011.getBena011_Pnd_Mos_Txt().getValue(1,":",60).setValue(" ");                                                                                             //Natural: ASSIGN BENA011.#MOS-TXT ( 1:60 ) := ' '
        pdaBena011.getBena011_Pnd_C_Mos_Txt().setValue(0);                                                                                                                //Natural: ASSIGN BENA011.#C-MOS-TXT := 0
        pnd_Bt_Table_Id_Key.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "LO", "2"));                                                                         //Natural: COMPRESS 'LO' '2' INTO #BT-TABLE-ID-KEY LEAVING NO
        vw_bt.startDatabaseFind                                                                                                                                           //Natural: FIND ( 1 ) BT WITH BT-TABLE-ID-KEY = #BT-TABLE-ID-KEY
        (
        "FBT",
        new Wc[] { new Wc("BT_TABLE_ID_KEY", "=", pnd_Bt_Table_Id_Key, WcType.WITH) },
        1
        );
        FBT:
        while (condition(vw_bt.readNextRow("FBT")))
        {
            vw_bt.setIfNotFoundControlFlag(false);
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        if (condition(vw_bt.getAstNUMBER().greater(getZero())))                                                                                                           //Natural: IF *NUMBER ( FBT. ) GT 0
        {
            pdaBena011.getBena011_Pnd_Lttr_Literal().setValue(bt_Bt_Table_Text);                                                                                          //Natural: ASSIGN BENA011.#LTTR-LITERAL := BT-TABLE-TEXT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaBena011.getBena011_Pnd_Lttr_Literal().setValue("2");                                                                                                       //Natural: ASSIGN BENA011.#LTTR-LITERAL := '2'
            //*  ONLINE
        }                                                                                                                                                                 //Natural: END-IF
        pdaBena011.getBena011_Pnd_Owner_Name().setValue(" ");                                                                                                             //Natural: ASSIGN BENA011.#OWNER-NAME := ' '
        pdaBena011.getBena011_Pnd_Mail_Addr().getValue(1,":",5).setValue(pnd_Bene_Pnd_Mdm_Addr.getValue(1,":",5));                                                        //Natural: ASSIGN BENA011.#MAIL-ADDR ( 1:5 ) := #MDM-ADDR ( 1:5 )
        pdaBena011.getBena011_Pnd_Mail_Postal_Data().setValue(pnd_Bene_Pnd_Mdm_Postal_Data);                                                                              //Natural: ASSIGN BENA011.#MAIL-POSTAL-DATA := #MDM-POSTAL-DATA
        pdaBena011.getBena011_Pnd_Us_Frgn().setValue(pnd_Bene_Pnd_Mdm_Us_Frgn);                                                                                           //Natural: ASSIGN BENA011.#US-FRGN := #MDM-US-FRGN
        pdaBena011.getBena011_Pnd_Last_Dsgntn_Srce().setValue("O");                                                                                                       //Natural: ASSIGN BENA011.#LAST-DSGNTN-SRCE := 'O'
        pnd_Bt_Table_Id_Key.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "LS", "O"));                                                                         //Natural: COMPRESS 'LS' 'O' INTO #BT-TABLE-ID-KEY LEAVING NO
        vw_bt.startDatabaseFind                                                                                                                                           //Natural: FIND ( 1 ) BT WITH BT-TABLE-ID-KEY = #BT-TABLE-ID-KEY
        (
        "FBT3",
        new Wc[] { new Wc("BT_TABLE_ID_KEY", "=", pnd_Bt_Table_Id_Key, WcType.WITH) },
        1
        );
        FBT3:
        while (condition(vw_bt.readNextRow("FBT3")))
        {
            vw_bt.setIfNotFoundControlFlag(false);
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        if (condition(vw_bt.getAstNUMBER().greater(getZero())))                                                                                                           //Natural: IF *NUMBER ( FBT3. ) GT 0
        {
            pdaBena011.getBena011_Pnd_Last_Dsgntn_Srce().setValue(bt_Bt_Table_Text);                                                                                      //Natural: ASSIGN BENA011.#LAST-DSGNTN-SRCE := BT-TABLE-TEXT
            //*  BENEFICIARY
        }                                                                                                                                                                 //Natural: END-IF
        pdaBena011.getBena011_Pnd_Last_Dsgntn_System().setValue("B");                                                                                                     //Natural: ASSIGN BENA011.#LAST-DSGNTN-SYSTEM := 'B'
        pnd_Bt_Table_Id_Key.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "LM", "B"));                                                                         //Natural: COMPRESS 'LM' 'B' INTO #BT-TABLE-ID-KEY LEAVING NO
        vw_bt.startDatabaseFind                                                                                                                                           //Natural: FIND ( 1 ) BT WITH BT-TABLE-ID-KEY = #BT-TABLE-ID-KEY
        (
        "FBT4",
        new Wc[] { new Wc("BT_TABLE_ID_KEY", "=", pnd_Bt_Table_Id_Key, WcType.WITH) },
        1
        );
        FBT4:
        while (condition(vw_bt.readNextRow("FBT4")))
        {
            vw_bt.setIfNotFoundControlFlag(false);
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        if (condition(vw_bt.getAstNUMBER().greater(getZero())))                                                                                                           //Natural: IF *NUMBER ( FBT4. ) GT 0
        {
            pdaBena011.getBena011_Pnd_Last_Dsgntn_System().setValue(bt_Bt_Table_Text);                                                                                    //Natural: ASSIGN BENA011.#LAST-DSGNTN-SYSTEM := BT-TABLE-TEXT
            //*  CURRENT
        }                                                                                                                                                                 //Natural: END-IF
        pdaBena011.getBena011_Pnd_Last_Dsgntn_Userid().setValue("BENEWEB");                                                                                               //Natural: ASSIGN BENA011.#LAST-DSGNTN-USERID := 'BENEWEB'
        pdaBena011.getBena011_Pnd_Last_Dsgntn_Dte().setValue(pnd_Bene_Pnd_Date);                                                                                          //Natural: ASSIGN BENA011.#LAST-DSGNTN-DTE := #DATE
        pdaBena011.getBena011_Pnd_Last_Dsgntn_Tme().setValue(pnd_Bene_Pnd_Time);                                                                                          //Natural: ASSIGN BENA011.#LAST-DSGNTN-TME := #TIME
        pdaBena011.getBena011_Pnd_Status().setValue("C");                                                                                                                 //Natural: ASSIGN BENA011.#STATUS := 'C'
        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 TO #BENA002.#MAX-DESIG
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pdaBena002.getPnd_Bena002_Pnd_Max_Desig())); pnd_I.nadd(1))
        {
            if (condition(pdaBena002.getPnd_Bena002_Pnd_Rltn_Cd().getValue(pnd_I).equals(" ") || pdaBena002.getPnd_Bena002_Pnd_Rltn_Free_Txt().getValue(pnd_I).notEquals(" "))) //Natural: IF #BENA002.#RLTN-CD ( #I ) = ' ' OR #BENA002.#RLTN-FREE-TXT ( #I ) NE ' '
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Bt_Table_Id_Key.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "RC", pdaBena002.getPnd_Bena002_Pnd_Rltn_Cd().getValue(pnd_I)));                 //Natural: COMPRESS 'RC' #BENA002.#RLTN-CD ( #I ) INTO #BT-TABLE-ID-KEY LEAVING NO
            vw_bt.startDatabaseFind                                                                                                                                       //Natural: FIND ( 1 ) BT WITH BT-TABLE-ID-KEY = #BT-TABLE-ID-KEY
            (
            "FBT2",
            new Wc[] { new Wc("BT_TABLE_ID_KEY", "=", pnd_Bt_Table_Id_Key, WcType.WITH) },
            1
            );
            FBT2:
            while (condition(vw_bt.readNextRow("FBT2")))
            {
                vw_bt.setIfNotFoundControlFlag(false);
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(vw_bt.getAstNUMBER().greater(getZero())))                                                                                                       //Natural: IF *NUMBER ( FBT2. ) GT 0
            {
                pdaBena002.getPnd_Bena002_Pnd_Rltn_Free_Txt().getValue(pnd_I).setValue(bt_Bt_Table_Text);                                                                 //Natural: ASSIGN #BENA002.#RLTN-FREE-TXT ( #I ) := BT-TABLE-TEXT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaBena002.getPnd_Bena002_Pnd_Rltn_Free_Txt().getValue(pnd_I).setValue(pdaBena002.getPnd_Bena002_Pnd_Rltn_Cd().getValue(pnd_I));                          //Natural: ASSIGN #BENA002.#RLTN-FREE-TXT ( #I ) := #BENA002.#RLTN-CD ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        DbsUtil.callnat(Benn011.class , getCurrentProcessState(), pdaBena011.getBena011(), pdaBena002.getPnd_Bena002());                                                  //Natural: CALLNAT 'BENN011' BENA011 #BENA002
        if (condition(Global.isEscape())) return;
        if (condition(pdaBena011.getBena011_Pnd_Err_Ind().notEquals(" ")))                                                                                                //Natural: IF BENA011.#ERR-IND NE ' '
        {
            getReports().write(0, "=",pdaBena011.getBena011_Pnd_Err_Message());                                                                                           //Natural: WRITE ( 0 ) '=' BENA011.#ERR-MESSAGE
            if (Global.isEscape()) return;
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
        //*  CREATE-AUDIT-TRAIL
    }
    private void sub_Create_Work_Request() throws Exception                                                                                                               //Natural: CREATE-WORK-REQUEST
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, "Subroutine CREATE-WORK-REQUEST");                                                                                                      //Natural: WRITE ( 0 ) 'Subroutine CREATE-WORK-REQUEST'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        pdaBena009.getBena009().reset();                                                                                                                                  //Natural: RESET BENA009
        pdaBena009.getBena009_Pnd_Wpid().setValue("TA MBM");                                                                                                              //Natural: ASSIGN BENA009.#WPID := 'TA MBM'
        pdaBena009.getBena009_Pnd_Gda_User_Id().setValue("BENEWEB");                                                                                                      //Natural: ASSIGN BENA009.#GDA-USER-ID := 'BENEWEB'
        pdaBena009.getBena009_Pnd_Gda_Slo_Exists().setValue("N");                                                                                                         //Natural: ASSIGN BENA009.#GDA-SLO-EXISTS := 'N'
        pdaBena009.getBena009_Pnd_Wpid_Pin().setValue(pnd_Bene_Pnd_Bn_Pin);                                                                                               //Natural: ASSIGN BENA009.#WPID-PIN := #BN-PIN
        pdaBena009.getBena009_Pnd_Wpid_Unit().setValue("CTADME");                                                                                                         //Natural: ASSIGN BENA009.#WPID-UNIT := 'CTADME'
        pdaBena009.getBena009_Pnd_Wpid_Next_Unit().setValue("CTADME");                                                                                                    //Natural: ASSIGN BENA009.#WPID-NEXT-UNIT := 'CTADME'
        pdaBena009.getBena009_Pnd_Wpid_Status().setValue("2000");                                                                                                         //Natural: ASSIGN BENA009.#WPID-STATUS := '2000'
        pdaBena009.getBena009_Pnd_Wpid_System().setValue("BEN");                                                                                                          //Natural: ASSIGN BENA009.#WPID-SYSTEM := 'BEN'
        pdaBena009.getBena009_Pnd_Et_Ind().setValue("Y");                                                                                                                 //Natural: ASSIGN BENA009.#ET-IND := 'Y'
        pdaBena009.getBena009_Pnd_Empl_Racf_Id().setValue("BENEWEB");                                                                                                     //Natural: ASSIGN BENA009.#EMPL-RACF-ID := 'BENEWEB'
        DbsUtil.callnat(Benn009.class , getCurrentProcessState(), pdaBena009.getBena009());                                                                               //Natural: CALLNAT 'BENN009' BENA009
        if (condition(Global.isEscape())) return;
        if (condition(pdaBena009.getBena009_Pnd_Error_Msg().equals(" ")))                                                                                                 //Natural: IF BENA009.#ERROR-MSG = ' '
        {
            pnd_Bene_Pnd_Rldt.setValue(pdaBena009.getBena009_Pnd_Return_Log_Dte_Tme());                                                                                   //Natural: ASSIGN #RLDT := BENA009.#RETURN-LOG-DTE-TME
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "=",pdaBena009.getBena009_Pnd_Error_Msg());                                                                                             //Natural: WRITE ( 0 ) '=' BENA009.#ERROR-MSG
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  CREATE-WORK-REQUEST
    }
    private void sub_Get_Mdm_Data() throws Exception                                                                                                                      //Natural: GET-MDM-DATA
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, "Subroutine GET-MDM-DATA",pnd_Workfile_Pnd_Ph_Ssn);                                                                                     //Natural: WRITE ( 0 ) 'Subroutine GET-MDM-DATA' #PH-SSN
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        pdaMdma101.getPnd_Mdma101().reset();                                                                                                                              //Natural: RESET #MDMA101
        pdaMdma101.getPnd_Mdma101_Pnd_I_Ssn_A9().setValue(pnd_Workfile_Pnd_Ph_Ssn);                                                                                       //Natural: ASSIGN #MDMA101.#I-SSN-A9 := #MDM-SSN := #PH-SSN
        pnd_Bene_Pnd_Mdm_Ssn.setValue(pnd_Workfile_Pnd_Ph_Ssn);
        DbsUtil.callnat(Mdmn101.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                           //Natural: CALLNAT 'MDMN101' #MDMA101
        if (condition(Global.isEscape())) return;
        if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code().equals("0000")))                                                                                      //Natural: IF #MDMA101.#O-RETURN-CODE = '0000'
        {
            pnd_Bene_Pnd_Mdm_Name.setValue(DbsUtil.compress(pdaMdma101.getPnd_Mdma101_Pnd_O_First_Name(), pdaMdma101.getPnd_Mdma101_Pnd_O_Last_Name()));                  //Natural: COMPRESS #O-FIRST-NAME #O-LAST-NAME INTO #MDM-NAME
            pnd_Bene_Pnd_Mdm_Lname.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Last_Name());                                                                                 //Natural: ASSIGN #MDM-LNAME := #O-LAST-NAME
            pnd_Bene_Pnd_Mdm_Addr.getValue(1).setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_1());                                                            //Natural: ASSIGN #MDM-ADDR ( 1 ) := #O-BASE-ADDRESS-LINE-1
            pnd_Bene_Pnd_Mdm_Addr.getValue(2).setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_2());                                                            //Natural: ASSIGN #MDM-ADDR ( 2 ) := #O-BASE-ADDRESS-LINE-2
            pnd_Bene_Pnd_Mdm_Addr.getValue(3).setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_3());                                                            //Natural: ASSIGN #MDM-ADDR ( 3 ) := #O-BASE-ADDRESS-LINE-3
            pnd_Bene_Pnd_Mdm_Addr.getValue(4).setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_4());                                                            //Natural: ASSIGN #MDM-ADDR ( 4 ) := #O-BASE-ADDRESS-LINE-4
            pnd_Bene_Pnd_Mdm_Postal_Data.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Postal_Data());                                                            //Natural: ASSIGN #MDM-POSTAL-DATA := #O-BASE-ADDRESS-POSTAL-DATA
            //*  US OR CANADIAN
            if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Type_Code().equals("U") || pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Type_Code().equals("C")))  //Natural: IF #O-BASE-ADDRESS-TYPE-CODE = 'U' OR = 'C'
            {
                pnd_Bene_Pnd_Mdm_Us_Frgn.setValue("Y");                                                                                                                   //Natural: ASSIGN #MDM-US-FRGN := 'Y'
                //*    COMPRESS #O-BASE-ADDRESS-CITY #O-BASE-ADDRESS-ST-PROV
                //*      #O-BASE-ADDRESS-ZIP-CODE INTO #CITY-STATE-ZIP
                //*    DECIDE FOR FIRST CONDITION
                //*      WHEN #MDM-ADDR (2) = ' '
                //*        #MDM-ADDR (2) := #CITY-STATE-ZIP
                //*      WHEN #MDM-ADDR (3) = ' '
                //*        #MDM-ADDR (3) := #CITY-STATE-ZIP
                //*      WHEN #MDM-ADDR (4) = ' '
                //*        #MDM-ADDR (4) := #CITY-STATE-ZIP
                //*      WHEN NONE IGNORE
                //*    END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, "MDMN101",pnd_Bene_Pnd_Mdm_Name,pnd_Bene_Pnd_Mdm_Postal_Data,pnd_Bene_Pnd_Mdm_Us_Frgn,pnd_Bene_Pnd_Mdm_Addr.getValue("*"));             //Natural: WRITE ( 0 ) 'MDMN101' #MDM-NAME #MDM-POSTAL-DATA #MDM-US-FRGN #MDM-ADDR ( * )
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  GET-MDM-DATA
    }
    private void sub_Read_Delete_Existing_Bene() throws Exception                                                                                                         //Natural: READ-DELETE-EXISTING-BENE
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, "Subroutine READ-DELETE-EXISTING-BENE");                                                                                                //Natural: WRITE ( 0 ) 'Subroutine READ-DELETE-EXISTING-BENE'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        pdaBena002.getPnd_Bena002().reset();                                                                                                                              //Natural: RESET #BENA002 #CNTRCT-ISN #MOS-ISN #BENE-ISN ( * )
        pdaBena002.getPnd_Bena002_Pnd_Cntrct_Isn().reset();
        pdaBena002.getPnd_Bena002_Pnd_Mos_Isn().reset();
        pdaBena002.getPnd_Bena002_Pnd_Bene_Isn().getValue("*").reset();
        pnd_Super_Key_Pnd_Key_Pin.setValue(pnd_Workfile_Pnd_Ph_Pin);                                                                                                      //Natural: ASSIGN #KEY-PIN := #PH-PIN
        pnd_Super_Key_Pnd_Key_Tiaa.setValue(pnd_Workfile_Pnd_I_Tiaa);                                                                                                     //Natural: ASSIGN #KEY-TIAA := #I-TIAA
        pnd_Super_Key_Pnd_Key_Tc.setValue(" ");                                                                                                                           //Natural: ASSIGN #KEY-TC := ' '
        vw_bca.startDatabaseRead                                                                                                                                          //Natural: READ BCA BY BC-PIN-CNTRCT-IND-KEY STARTING FROM #SUPER-KEY
        (
        "RBC",
        new Wc[] { new Wc("BC_PIN_CNTRCT_IND_KEY", ">=", pnd_Super_Key, WcType.BY) },
        new Oc[] { new Oc("BC_PIN_CNTRCT_IND_KEY", "ASC") }
        );
        RBC:
        while (condition(vw_bca.readNextRow("RBC")))
        {
            if (condition(bca_Bc_Pin.notEquals(pnd_Super_Key_Pnd_Key_Pin) || bca_Bc_Tiaa_Cntrct.notEquals(pnd_Super_Key_Pnd_Key_Tiaa)))                                   //Natural: IF BC-PIN NE #KEY-PIN OR BC-TIAA-CNTRCT NE #KEY-TIAA
            {
                if (true) break RBC;                                                                                                                                      //Natural: ESCAPE BOTTOM ( RBC. )
                //*  DELETE
            }                                                                                                                                                             //Natural: END-IF
            pdaBena002.getPnd_Bena002_Pnd_Actvty_Mode().setValue("D");                                                                                                    //Natural: ASSIGN #BENA002.#ACTVTY-MODE := 'D'
            pdaBena002.getPnd_Bena002_Pnd_Pin().setValue(bca_Bc_Pin);                                                                                                     //Natural: ASSIGN #BENA002.#PIN := BC-PIN
            pdaBena002.getPnd_Bena002_Pnd_Tiaa_Cntrct().setValue(bca_Bc_Tiaa_Cntrct);                                                                                     //Natural: ASSIGN #BENA002.#TIAA-CNTRCT := BC-TIAA-CNTRCT
            pdaBena002.getPnd_Bena002_Pnd_Cref_Cntrct().setValue(bca_Bc_Cref_Cntrct);                                                                                     //Natural: ASSIGN #BENA002.#CREF-CNTRCT := BC-CREF-CNTRCT
            pdaBena002.getPnd_Bena002_Pnd_Tiaa_Cref_Ind().setValue(bca_Bc_Tiaa_Cref_Ind);                                                                                 //Natural: ASSIGN #BENA002.#TIAA-CREF-IND := #KEY-TC := BC-TIAA-CREF-IND
            pnd_Super_Key_Pnd_Key_Tc.setValue(bca_Bc_Tiaa_Cref_Ind);
            pdaBena002.getPnd_Bena002_Pnd_Cntrct_Stat().setValue(bca_Bc_Stat);                                                                                            //Natural: ASSIGN #BENA002.#CNTRCT-STAT := BC-STAT
            pdaBena002.getPnd_Bena002_Pnd_Cntrct_Type().setValue(bca_Bc_Cntrct_Type);                                                                                     //Natural: ASSIGN #BENA002.#CNTRCT-TYPE := BC-CNTRCT-TYPE
            pdaBena002.getPnd_Bena002_Pnd_Rqst_Timestamp().setValue(bca_Bc_Rqst_Timestamp);                                                                               //Natural: ASSIGN #BENA002.#RQST-TIMESTAMP := BC-RQST-TIMESTAMP
            pdaBena002.getPnd_Bena002_Pnd_Sys_Dte().setValue(Global.getDATN());                                                                                           //Natural: ASSIGN #BENA002.#SYS-DTE := *DATN
            pdaBena002.getPnd_Bena002_Pnd_Mos_Ind().setValue(bca_Bc_Mos_Ind);                                                                                             //Natural: ASSIGN #BENA002.#MOS-IND := BC-MOS-IND
            pdaBena002.getPnd_Bena002_Pnd_Cntrct_Isn().setValue(vw_bca.getAstISN("RBC"));                                                                                 //Natural: ASSIGN #BENA002.#CNTRCT-ISN := *ISN ( RBC. )
            if (condition(bca_Bc_Mos_Ind.equals("Y")))                                                                                                                    //Natural: IF BC-MOS-IND = 'Y'
            {
                vw_bma.startDatabaseFind                                                                                                                                  //Natural: FIND BMA WITH BM-PIN-CNTRCT-IND-KEY = #SUPER-KEY
                (
                "FBM",
                new Wc[] { new Wc("BM_PIN_CNTRCT_IND_KEY", "=", pnd_Super_Key, WcType.WITH) }
                );
                FBM:
                while (condition(vw_bma.readNextRow("FBM", true)))
                {
                    vw_bma.setIfNotFoundControlFlag(false);
                    if (condition(vw_bma.getAstCOUNTER().equals(0)))                                                                                                      //Natural: IF NO RECORDS FOUND
                    {
                        if (true) break FBM;                                                                                                                              //Natural: ESCAPE BOTTOM ( FBM. )
                    }                                                                                                                                                     //Natural: END-NOREC
                    pdaBena002.getPnd_Bena002_Pnd_Mos_Isn().setValue(vw_bma.getAstISN("FBM"));                                                                            //Natural: ASSIGN #BENA002.#MOS-ISN := *ISN ( FBM. )
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RBC"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RBC"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_X.reset();                                                                                                                                            //Natural: RESET #X
                vw_bda.startDatabaseRead                                                                                                                                  //Natural: READ BDA BY BD-PIN-CNTRCT-IND-KEY STARTING FROM #SUPER-KEY
                (
                "RBD",
                new Wc[] { new Wc("BD_PIN_CNTRCT_IND_KEY", ">=", pnd_Super_Key, WcType.BY) },
                new Oc[] { new Oc("BD_PIN_CNTRCT_IND_KEY", "ASC") }
                );
                RBD:
                while (condition(vw_bda.readNextRow("RBD")))
                {
                    if (condition(bda_Bd_Pin.notEquals(pnd_Super_Key_Pnd_Key_Pin) || bda_Bd_Tiaa_Cntrct.notEquals(pnd_Super_Key_Pnd_Key_Tiaa) || bda_Bd_Tiaa_Cref_Ind.notEquals(pnd_Super_Key_Pnd_Key_Tc))) //Natural: IF BD-PIN NE #KEY-PIN OR BD-TIAA-CNTRCT NE #KEY-TIAA OR BD-TIAA-CREF-IND NE #KEY-TC
                    {
                        if (true) break RBD;                                                                                                                              //Natural: ESCAPE BOTTOM ( RBD. )
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_X.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #X
                    pdaBena002.getPnd_Bena002_Pnd_Bene_Isn().getValue(pnd_X).setValue(vw_bda.getAstISN("RBD"));                                                           //Natural: ASSIGN #BENA002.#BENE-ISN ( #X ) := *ISN ( RBD. )
                }                                                                                                                                                         //Natural: END-READ
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RBC"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RBC"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  MOVE CURRENT TO HISTORY
                //*  ISSUE END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
            pdaBena002.getPnd_Bena002_Pnd_Hist_Ind().setValue("Y");                                                                                                       //Natural: ASSIGN #BENA002.#HIST-IND := 'Y'
            pdaBena002.getPnd_Bena002_Pnd_Et_Ind().setValue("Y");                                                                                                         //Natural: ASSIGN #BENA002.#ET-IND := 'Y'
            pdaBena002.getPnd_Bena002_Pnd_Max_Desig().setValue(pnd_X);                                                                                                    //Natural: ASSIGN #BENA002.#MAX-DESIG := #X
            DbsUtil.callnat(Benn002.class , getCurrentProcessState(), pdaBena002.getPnd_Bena002());                                                                       //Natural: CALLNAT 'BENN002' #BENA002
            if (condition(Global.isEscape())) return;
            if (condition(pdaBena002.getPnd_Bena002_Pnd_Err_Ind().notEquals(" ")))                                                                                        //Natural: IF #BENA002.#ERR-IND NE ' '
            {
                pnd_Total_Error.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #TOTAL-ERROR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Total_Delete.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #TOTAL-DELETE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, "=",pnd_Super_Key_Pnd_Key_Pin,"=",pnd_Super_Key_Pnd_Key_Tiaa,"=",pdaBena002.getPnd_Bena002_Pnd_Cntrct_Isn(),"=",pdaBena002.getPnd_Bena002_Pnd_Mos_Isn(), //Natural: WRITE ( 0 ) '=' #KEY-PIN '=' #KEY-TIAA '=' #CNTRCT-ISN '=' #MOS-ISN '=' #BENE-ISN ( 10 )
                "=",pdaBena002.getPnd_Bena002_Pnd_Bene_Isn().getValue(10));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  READ-DELETE-EXISTING-BENE
    }

    //

    // Support Methods

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pnd_Workfile_Pnd_I_TiaaIsBreak = pnd_Workfile_Pnd_I_Tiaa.isBreak(endOfData);
        if (condition(pnd_Workfile_Pnd_I_TiaaIsBreak))
        {
            pnd_Total_Read.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #TOTAL-READ
            if (condition(pnd_Bene_Pnd_Bn_Seq.greater(getZero())))                                                                                                        //Natural: IF #BN-SEQ > 0
            {
                                                                                                                                                                          //Natural: PERFORM CREATE-WORK-REQUEST
                sub_Create_Work_Request();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM ADD-BENE-DESIGNATION
                sub_Add_Bene_Designation();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM ADD-BENE-ACTIVITY
                sub_Add_Bene_Activity();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CREATE-AUDIT-TRAIL
                sub_Create_Audit_Trail();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CALL-POST
                sub_Call_Post();
                if (condition(Global.isEscape())) {return;}
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
            pnd_Bene.reset();                                                                                                                                             //Natural: RESET #BENE #BN ( * ) #BENA002 BENA011 #MDMA101
            pnd_Bene_Pnd_Bn.getValue("*").reset();
            pdaBena002.getPnd_Bena002().reset();
            pdaBena011.getBena011().reset();
            pdaMdma101.getPnd_Mdma101().reset();
            pls_Trace.resetInitial();                                                                                                                                     //Natural: RESET INITIAL +TRACE
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133 SG=OFF ZP=ON");
        Global.format(1, "PS=60 LS=133 SG=OFF ZP=ON");

        getReports().setDisplayColumns(1, ReportOption.NOTITLE,"SSN",
        		pnd_Workfile_Pnd_Ph_Ssn,"PIN",
        		pnd_Workfile_Pnd_Ph_Pin,"TIAA",
        		pnd_Workfile_Pnd_I_Tiaa,"CREF",
        		pnd_Workfile_Pnd_I_Cref,"EfDt",
        		pnd_Workfile_Pnd_I_Efdt,"PC",
        		pnd_Workfile_Pnd_I_Type,"%",
        		pnd_Workfile_Pnd_I_Perc,"RC",
        		pnd_Workfile_Pnd_I_Rc,"DoB",
        		pnd_Workfile_Pnd_I_Dob,"S",
        		pnd_Workfile_Pnd_I_Ss_Cd,"SSN",
        		pnd_Workfile_Pnd_I_Ss_Nmbr,"Name1",
        		pnd_Workfile_Pnd_I_Name1);
    }
}
