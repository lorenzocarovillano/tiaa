/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:20:16 PM
**        * FROM NATURAL PROGRAM : Benp9810
************************************************************
**        * FILE NAME            : Benp9810.java
**        * CLASS NAME           : Benp9810
**        * INSTANCE NAME        : Benp9810
************************************************************
************************************************************************
* PROGRAM  : BENP9810
* SYSTEM   : BENEFICIARY-SYSTEM
* TITLE    : BENE IN THE PLAN - INTERFACE COMMON MODULE
* CREATED  : MAY 13, 2015
*          : READ DATASET FROM OMNI AND MOVE TO BENA971 & CALL BENN971
************************************************************************
*    DATE      USERID                   DESCRIPTION
* MM/DD/YYYY  XXXXXXXX  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
* 04/01/2017  DURAND    PIN EXPANSION N7 TO N12
************************************************************************

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benp9810 extends BLNatBase
{
    // Data Areas
    private PdaBena971 pdaBena971;
    private PdaBenpda_M pdaBenpda_M;
    private PdaBenpda_E pdaBenpda_E;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Omni;

    private DbsGroup pnd_Omni__R_Field_1;

    private DbsGroup pnd_Omni_Pnd_Omni_Data;
    private DbsField pnd_Omni_Pnd_Intrfce_Bsnss_Dte;
    private DbsField pnd_Omni_Pnd_Intrfcng_Systm;
    private DbsField pnd_Omni_Pnd_Rqstng_System;
    private DbsField pnd_Omni_Pnd_New_Issuefslash_Chng_Ind;
    private DbsField pnd_Omni_Pnd_Illgble_Ind;
    private DbsField pnd_Omni_Pnd_More_Than_Five_Benes_Ind;
    private DbsField pnd_Omni_Pnd_Nmbr_Of_Benes;
    private DbsField pnd_Omni_Pnd_Intrfce_Mgrtn_Ind;
    private DbsField pnd_Omni_Pnd_Pin;
    private DbsField pnd_Omni_Pnd_Tiaa_Cntrct;
    private DbsField pnd_Omni_Pnd_Cref_Cntrct;
    private DbsField pnd_Omni_Pnd_Cntrct_Type;
    private DbsField pnd_Omni_Pnd_Effctve_Dte;
    private DbsField pnd_Omni_Pnd_Tiaa_Cref_Ind;
    private DbsField pnd_Omni_Pnd_Same_As_Ind;
    private DbsField pnd_Omni_Pnd_Mos_Ind;
    private DbsField pnd_Omni_Pnd_Mos_Irrvcble_Ind;
    private DbsField pnd_Omni_Pnd_Pymnt_Chld_Dcsd_Ind;
    private DbsField pnd_Omni_Pnd_Exempt_Spouse_Rights;
    private DbsField pnd_Omni_Pnd_Spouse_Waived_Bnfts;
    private DbsField pnd_Omni_Pnd_Fldr_Log_Dte_Tme;
    private DbsField pnd_Omni_Pnd_Dflt_To_Estate;
    private DbsField pnd_Omni_Pnd_Designation;

    private DbsGroup pnd_Omni__R_Field_2;
    private DbsField pnd_Omni_Pnd_Mos_Txt;

    private DbsGroup pnd_Omni__R_Field_3;

    private DbsGroup pnd_Omni_Pnd_Bene_Data;
    private DbsField pnd_Omni_Pnd_Bene_Name1;
    private DbsField pnd_Omni_Pnd_Bene_Name2;
    private DbsField pnd_Omni_Pnd_Bene_Type;
    private DbsField pnd_Omni_Pnd_Dte_Birth_Trust;
    private DbsField pnd_Omni_Pnd_Rtln_Cde;
    private DbsField pnd_Omni_Pnd_Rltn_Free_Txt;
    private DbsField pnd_Omni_Pnd_Ss_Cd;
    private DbsField pnd_Omni_Pnd_Ss_Nbr;
    private DbsField pnd_Omni_Pnd_Prctge_Frctn_Ind;
    private DbsField pnd_Omni_Pnd_Prctge;
    private DbsField pnd_Omni_Pnd_Nmrtr;
    private DbsField pnd_Omni_Pnd_Dnmntr;
    private DbsField pnd_Omni_Pnd_Irrvcbl_Ind;
    private DbsField pnd_Omni_Pnd_Sttlmnt_Rstrctn;
    private DbsField pnd_Omni_Pnd_Spcl_Txt;
    private DbsField pnd_Omni_Pnd_Addr1;
    private DbsField pnd_Omni_Pnd_Addr2;
    private DbsField pnd_Omni_Pnd_Addr3_City;
    private DbsField pnd_Omni_Pnd_State;
    private DbsField pnd_Omni_Pnd_Zip;
    private DbsField pnd_Omni_Pnd_Country;
    private DbsField pnd_Omni_Pnd_Phone;
    private DbsField pnd_Omni_Pnd_Gender;
    private DbsField pnd_Edit;
    private DbsField pnd_Fail_Edit;
    private DbsField pnd_Total_Read;
    private DbsField pnd_Total_Error;
    private DbsField pnd_Total_Add;
    private DbsField pnd_X;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaBena971 = new PdaBena971(localVariables);
        pdaBenpda_M = new PdaBenpda_M(localVariables);
        pdaBenpda_E = new PdaBenpda_E(localVariables);

        // Local Variables
        pnd_Omni = localVariables.newFieldArrayInRecord("pnd_Omni", "#OMNI", FieldType.STRING, 1, new DbsArrayController(1, 15365));

        pnd_Omni__R_Field_1 = localVariables.newGroupInRecord("pnd_Omni__R_Field_1", "REDEFINE", pnd_Omni);

        pnd_Omni_Pnd_Omni_Data = pnd_Omni__R_Field_1.newGroupInGroup("pnd_Omni_Pnd_Omni_Data", "#OMNI-DATA");
        pnd_Omni_Pnd_Intrfce_Bsnss_Dte = pnd_Omni_Pnd_Omni_Data.newFieldInGroup("pnd_Omni_Pnd_Intrfce_Bsnss_Dte", "#INTRFCE-BSNSS-DTE", FieldType.STRING, 
            8);
        pnd_Omni_Pnd_Intrfcng_Systm = pnd_Omni_Pnd_Omni_Data.newFieldInGroup("pnd_Omni_Pnd_Intrfcng_Systm", "#INTRFCNG-SYSTM", FieldType.STRING, 8);
        pnd_Omni_Pnd_Rqstng_System = pnd_Omni_Pnd_Omni_Data.newFieldInGroup("pnd_Omni_Pnd_Rqstng_System", "#RQSTNG-SYSTEM", FieldType.STRING, 8);
        pnd_Omni_Pnd_New_Issuefslash_Chng_Ind = pnd_Omni_Pnd_Omni_Data.newFieldInGroup("pnd_Omni_Pnd_New_Issuefslash_Chng_Ind", "#NEW-ISSUE/CHNG-IND", 
            FieldType.STRING, 1);
        pnd_Omni_Pnd_Illgble_Ind = pnd_Omni_Pnd_Omni_Data.newFieldInGroup("pnd_Omni_Pnd_Illgble_Ind", "#ILLGBLE-IND", FieldType.STRING, 1);
        pnd_Omni_Pnd_More_Than_Five_Benes_Ind = pnd_Omni_Pnd_Omni_Data.newFieldInGroup("pnd_Omni_Pnd_More_Than_Five_Benes_Ind", "#MORE-THAN-FIVE-BENES-IND", 
            FieldType.STRING, 1);
        pnd_Omni_Pnd_Nmbr_Of_Benes = pnd_Omni_Pnd_Omni_Data.newFieldInGroup("pnd_Omni_Pnd_Nmbr_Of_Benes", "#NMBR-OF-BENES", FieldType.NUMERIC, 3);
        pnd_Omni_Pnd_Intrfce_Mgrtn_Ind = pnd_Omni_Pnd_Omni_Data.newFieldInGroup("pnd_Omni_Pnd_Intrfce_Mgrtn_Ind", "#INTRFCE-MGRTN-IND", FieldType.STRING, 
            1);
        pnd_Omni_Pnd_Pin = pnd_Omni_Pnd_Omni_Data.newFieldInGroup("pnd_Omni_Pnd_Pin", "#PIN", FieldType.NUMERIC, 12);
        pnd_Omni_Pnd_Tiaa_Cntrct = pnd_Omni_Pnd_Omni_Data.newFieldInGroup("pnd_Omni_Pnd_Tiaa_Cntrct", "#TIAA-CNTRCT", FieldType.STRING, 10);
        pnd_Omni_Pnd_Cref_Cntrct = pnd_Omni_Pnd_Omni_Data.newFieldInGroup("pnd_Omni_Pnd_Cref_Cntrct", "#CREF-CNTRCT", FieldType.STRING, 10);
        pnd_Omni_Pnd_Cntrct_Type = pnd_Omni_Pnd_Omni_Data.newFieldInGroup("pnd_Omni_Pnd_Cntrct_Type", "#CNTRCT-TYPE", FieldType.STRING, 1);
        pnd_Omni_Pnd_Effctve_Dte = pnd_Omni_Pnd_Omni_Data.newFieldInGroup("pnd_Omni_Pnd_Effctve_Dte", "#EFFCTVE-DTE", FieldType.STRING, 8);
        pnd_Omni_Pnd_Tiaa_Cref_Ind = pnd_Omni_Pnd_Omni_Data.newFieldInGroup("pnd_Omni_Pnd_Tiaa_Cref_Ind", "#TIAA-CREF-IND", FieldType.STRING, 1);
        pnd_Omni_Pnd_Same_As_Ind = pnd_Omni_Pnd_Omni_Data.newFieldInGroup("pnd_Omni_Pnd_Same_As_Ind", "#SAME-AS-IND", FieldType.STRING, 1);
        pnd_Omni_Pnd_Mos_Ind = pnd_Omni_Pnd_Omni_Data.newFieldInGroup("pnd_Omni_Pnd_Mos_Ind", "#MOS-IND", FieldType.STRING, 1);
        pnd_Omni_Pnd_Mos_Irrvcble_Ind = pnd_Omni_Pnd_Omni_Data.newFieldInGroup("pnd_Omni_Pnd_Mos_Irrvcble_Ind", "#MOS-IRRVCBLE-IND", FieldType.STRING, 
            1);
        pnd_Omni_Pnd_Pymnt_Chld_Dcsd_Ind = pnd_Omni_Pnd_Omni_Data.newFieldInGroup("pnd_Omni_Pnd_Pymnt_Chld_Dcsd_Ind", "#PYMNT-CHLD-DCSD-IND", FieldType.STRING, 
            1);
        pnd_Omni_Pnd_Exempt_Spouse_Rights = pnd_Omni_Pnd_Omni_Data.newFieldInGroup("pnd_Omni_Pnd_Exempt_Spouse_Rights", "#EXEMPT-SPOUSE-RIGHTS", FieldType.STRING, 
            1);
        pnd_Omni_Pnd_Spouse_Waived_Bnfts = pnd_Omni_Pnd_Omni_Data.newFieldInGroup("pnd_Omni_Pnd_Spouse_Waived_Bnfts", "#SPOUSE-WAIVED-BNFTS", FieldType.STRING, 
            1);
        pnd_Omni_Pnd_Fldr_Log_Dte_Tme = pnd_Omni_Pnd_Omni_Data.newFieldInGroup("pnd_Omni_Pnd_Fldr_Log_Dte_Tme", "#FLDR-LOG-DTE-TME", FieldType.STRING, 
            15);
        pnd_Omni_Pnd_Dflt_To_Estate = pnd_Omni_Pnd_Omni_Data.newFieldInGroup("pnd_Omni_Pnd_Dflt_To_Estate", "#DFLT-TO-ESTATE", FieldType.STRING, 1);
        pnd_Omni_Pnd_Designation = pnd_Omni_Pnd_Omni_Data.newFieldArrayInGroup("pnd_Omni_Pnd_Designation", "#DESIGNATION", FieldType.STRING, 1, new DbsArrayController(1, 
            15270));

        pnd_Omni__R_Field_2 = pnd_Omni_Pnd_Omni_Data.newGroupInGroup("pnd_Omni__R_Field_2", "REDEFINE", pnd_Omni_Pnd_Designation);
        pnd_Omni_Pnd_Mos_Txt = pnd_Omni__R_Field_2.newFieldArrayInGroup("pnd_Omni_Pnd_Mos_Txt", "#MOS-TXT", FieldType.STRING, 72, new DbsArrayController(1, 
            60));

        pnd_Omni__R_Field_3 = pnd_Omni_Pnd_Omni_Data.newGroupInGroup("pnd_Omni__R_Field_3", "REDEFINE", pnd_Omni_Pnd_Designation);

        pnd_Omni_Pnd_Bene_Data = pnd_Omni__R_Field_3.newGroupArrayInGroup("pnd_Omni_Pnd_Bene_Data", "#BENE-DATA", new DbsArrayController(1, 30));
        pnd_Omni_Pnd_Bene_Name1 = pnd_Omni_Pnd_Bene_Data.newFieldInGroup("pnd_Omni_Pnd_Bene_Name1", "#BENE-NAME1", FieldType.STRING, 35);
        pnd_Omni_Pnd_Bene_Name2 = pnd_Omni_Pnd_Bene_Data.newFieldInGroup("pnd_Omni_Pnd_Bene_Name2", "#BENE-NAME2", FieldType.STRING, 35);
        pnd_Omni_Pnd_Bene_Type = pnd_Omni_Pnd_Bene_Data.newFieldInGroup("pnd_Omni_Pnd_Bene_Type", "#BENE-TYPE", FieldType.STRING, 1);
        pnd_Omni_Pnd_Dte_Birth_Trust = pnd_Omni_Pnd_Bene_Data.newFieldInGroup("pnd_Omni_Pnd_Dte_Birth_Trust", "#DTE-BIRTH-TRUST", FieldType.STRING, 8);
        pnd_Omni_Pnd_Rtln_Cde = pnd_Omni_Pnd_Bene_Data.newFieldInGroup("pnd_Omni_Pnd_Rtln_Cde", "#RTLN-CDE", FieldType.STRING, 2);
        pnd_Omni_Pnd_Rltn_Free_Txt = pnd_Omni_Pnd_Bene_Data.newFieldInGroup("pnd_Omni_Pnd_Rltn_Free_Txt", "#RLTN-FREE-TXT", FieldType.STRING, 15);
        pnd_Omni_Pnd_Ss_Cd = pnd_Omni_Pnd_Bene_Data.newFieldInGroup("pnd_Omni_Pnd_Ss_Cd", "#SS-CD", FieldType.STRING, 1);
        pnd_Omni_Pnd_Ss_Nbr = pnd_Omni_Pnd_Bene_Data.newFieldInGroup("pnd_Omni_Pnd_Ss_Nbr", "#SS-NBR", FieldType.STRING, 9);
        pnd_Omni_Pnd_Prctge_Frctn_Ind = pnd_Omni_Pnd_Bene_Data.newFieldInGroup("pnd_Omni_Pnd_Prctge_Frctn_Ind", "#PRCTGE-FRCTN-IND", FieldType.STRING, 
            1);
        pnd_Omni_Pnd_Prctge = pnd_Omni_Pnd_Bene_Data.newFieldInGroup("pnd_Omni_Pnd_Prctge", "#PRCTGE", FieldType.NUMERIC, 5, 2);
        pnd_Omni_Pnd_Nmrtr = pnd_Omni_Pnd_Bene_Data.newFieldInGroup("pnd_Omni_Pnd_Nmrtr", "#NMRTR", FieldType.NUMERIC, 3);
        pnd_Omni_Pnd_Dnmntr = pnd_Omni_Pnd_Bene_Data.newFieldInGroup("pnd_Omni_Pnd_Dnmntr", "#DNMNTR", FieldType.NUMERIC, 3);
        pnd_Omni_Pnd_Irrvcbl_Ind = pnd_Omni_Pnd_Bene_Data.newFieldInGroup("pnd_Omni_Pnd_Irrvcbl_Ind", "#IRRVCBL-IND", FieldType.STRING, 1);
        pnd_Omni_Pnd_Sttlmnt_Rstrctn = pnd_Omni_Pnd_Bene_Data.newFieldInGroup("pnd_Omni_Pnd_Sttlmnt_Rstrctn", "#STTLMNT-RSTRCTN", FieldType.STRING, 1);
        pnd_Omni_Pnd_Spcl_Txt = pnd_Omni_Pnd_Bene_Data.newFieldArrayInGroup("pnd_Omni_Pnd_Spcl_Txt", "#SPCL-TXT", FieldType.STRING, 72, new DbsArrayController(1, 
            3));
        pnd_Omni_Pnd_Addr1 = pnd_Omni_Pnd_Bene_Data.newFieldInGroup("pnd_Omni_Pnd_Addr1", "#ADDR1", FieldType.STRING, 35);
        pnd_Omni_Pnd_Addr2 = pnd_Omni_Pnd_Bene_Data.newFieldInGroup("pnd_Omni_Pnd_Addr2", "#ADDR2", FieldType.STRING, 35);
        pnd_Omni_Pnd_Addr3_City = pnd_Omni_Pnd_Bene_Data.newFieldInGroup("pnd_Omni_Pnd_Addr3_City", "#ADDR3-CITY", FieldType.STRING, 35);
        pnd_Omni_Pnd_State = pnd_Omni_Pnd_Bene_Data.newFieldInGroup("pnd_Omni_Pnd_State", "#STATE", FieldType.STRING, 2);
        pnd_Omni_Pnd_Zip = pnd_Omni_Pnd_Bene_Data.newFieldInGroup("pnd_Omni_Pnd_Zip", "#ZIP", FieldType.STRING, 10);
        pnd_Omni_Pnd_Country = pnd_Omni_Pnd_Bene_Data.newFieldInGroup("pnd_Omni_Pnd_Country", "#COUNTRY", FieldType.STRING, 35);
        pnd_Omni_Pnd_Phone = pnd_Omni_Pnd_Bene_Data.newFieldInGroup("pnd_Omni_Pnd_Phone", "#PHONE", FieldType.STRING, 20);
        pnd_Omni_Pnd_Gender = pnd_Omni_Pnd_Bene_Data.newFieldInGroup("pnd_Omni_Pnd_Gender", "#GENDER", FieldType.STRING, 1);
        pnd_Edit = localVariables.newFieldInRecord("pnd_Edit", "#EDIT", FieldType.STRING, 50);
        pnd_Fail_Edit = localVariables.newFieldInRecord("pnd_Fail_Edit", "#FAIL-EDIT", FieldType.BOOLEAN, 1);
        pnd_Total_Read = localVariables.newFieldInRecord("pnd_Total_Read", "#TOTAL-READ", FieldType.PACKED_DECIMAL, 10);
        pnd_Total_Error = localVariables.newFieldInRecord("pnd_Total_Error", "#TOTAL-ERROR", FieldType.PACKED_DECIMAL, 10);
        pnd_Total_Add = localVariables.newFieldInRecord("pnd_Total_Add", "#TOTAL-ADD", FieldType.PACKED_DECIMAL, 10);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Benp9810() throws Exception
    {
        super("Benp9810");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("BENP9810", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) LS = 133 PS = 60 SG = OFF ZP = ON;//Natural: FORMAT ( 1 ) LS = 133 PS = 60 SG = OFF ZP = ON;//Natural: FORMAT ( 2 ) LS = 133 PS = 60 SG = OFF ZP = ON
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 01 #OMNI ( * )
        while (condition(getWorkFiles().read(1, pnd_Omni.getValue("*"))))
        {
            if (condition(pnd_Omni_Pnd_Intrfce_Bsnss_Dte.equals(" ") && pnd_Omni_Pnd_Intrfcng_Systm.equals(" ")))                                                         //Natural: IF #OMNI.#INTRFCE-BSNSS-DTE = ' ' AND #OMNI.#INTRFCNG-SYSTM = ' '
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Total_Read.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #TOTAL-READ
                                                                                                                                                                          //Natural: PERFORM DUMP-WORKFILE
            sub_Dump_Workfile();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaBena971.getBena971().reset();                                                                                                                              //Natural: RESET BENA971 #FAIL-EDIT
            pnd_Fail_Edit.reset();
                                                                                                                                                                          //Natural: PERFORM EDIT-ROUTINE
            sub_Edit_Routine();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Fail_Edit.getBoolean()))                                                                                                                    //Natural: IF #FAIL-EDIT
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REPORT
                sub_Write_Error_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pdaBena971.getBena971().setValuesByName(pnd_Omni_Pnd_Omni_Data);                                                                                              //Natural: MOVE BY NAME #OMNI-DATA TO BENA971
            //*  PERFORM DUMP-BENA971
            DbsUtil.callnat(Benn971.class , getCurrentProcessState(), pdaBena971.getBena971());                                                                           //Natural: CALLNAT 'BENN971' BENA971
            if (condition(Global.isEscape())) return;
            if (condition(pdaBena971.getBena971_Pnd_Error_Msg().equals(" ")))                                                                                             //Natural: IF BENA971.#ERROR-MSG = ' '
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-ADD-REPORT
                sub_Write_Add_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Edit.setValue(pdaBena971.getBena971_Pnd_Error_Msg());                                                                                                 //Natural: ASSIGN #EDIT := BENA971.#ERROR-MSG
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REPORT
                sub_Write_Error_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  RESET WORK FILE & GET READY
        pnd_Omni.getValue("*").reset();                                                                                                                                   //Natural: RESET #OMNI ( * )
        //*  FOR NEXT DAY'S PROCESSING
        getWorkFiles().write(2, false, pnd_Omni.getValue("*"));                                                                                                           //Natural: WRITE WORK FILE 02 #OMNI ( * )
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"Total Bene In The Plan records Read    :",pnd_Total_Read, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"Total Bene In The Plan records Errored :",pnd_Total_Error,  //Natural: WRITE ( 1 ) NOTITLE / 'Total Bene In The Plan records Read    :' #TOTAL-READ / 'Total Bene In The Plan records Errored :' #TOTAL-ERROR / 'Total Bene In The Plan records Added   :' #TOTAL-ADD
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"Total Bene In The Plan records Added   :",pnd_Total_Add, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,"Total Bene In The Plan records Read    :",pnd_Total_Read, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"Total Bene In The Plan records Errored :",pnd_Total_Error,  //Natural: WRITE ( 2 ) NOTITLE / 'Total Bene In The Plan records Read    :' #TOTAL-READ / 'Total Bene In The Plan records Errored :' #TOTAL-ERROR / 'Total Bene In The Plan records Added   :' #TOTAL-ADD
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"Total Bene In The Plan records Added   :",pnd_Total_Add, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DUMP-BENA971
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DUMP-WORKFILE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EDIT-ROUTINE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-ADD-REPORT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-ERROR-REPORT
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Dump_Bena971() throws Exception                                                                                                                      //Natural: DUMP-BENA971
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,"=",pdaBena971.getBena971_Pnd_Intrfce_Bsnss_Dte(),NEWLINE,"=",pdaBena971.getBena971_Pnd_Intrfcng_Systm(),      //Natural: WRITE ( 0 ) NOTITLE / '=' BENA971.#INTRFCE-BSNSS-DTE / '=' BENA971.#INTRFCNG-SYSTM / '=' BENA971.#RQSTNG-SYSTEM / '=' BENA971.#NEW-ISSUE/CHNG-IND / '=' BENA971.#ILLGBLE-IND / '=' BENA971.#MORE-THAN-FIVE-BENES-IND / '=' BENA971.#NMBR-OF-BENES / '=' BENA971.#INTRFCE-MGRTN-IND / '=' BENA971.#PIN / '=' BENA971.#TIAA-CNTRCT / '=' BENA971.#CREF-CNTRCT / '=' BENA971.#CNTRCT-TYPE / '=' BENA971.#EFFCTVE-DTE / '=' BENA971.#TIAA-CREF-IND / '=' BENA971.#SAME-AS-IND / '=' BENA971.#MOS-IND / '=' BENA971.#MOS-IRRVCBLE-IND / '=' BENA971.#PYMNT-CHLD-DCSD-IND / '=' BENA971.#EXEMPT-SPOUSE-RIGHTS / '=' BENA971.#SPOUSE-WAIVED-BNFTS / '=' BENA971.#FLDR-LOG-DTE-TME / '=' BENA971.#DFLT-TO-ESTATE / '=' BENA971.#MOS-TXT ( 1 )
            NEWLINE,"=",pdaBena971.getBena971_Pnd_Rqstng_System(),NEWLINE,"=",pdaBena971.getBena971_Pnd_New_Issuefslash_Chng_Ind(),NEWLINE,"=",pdaBena971.getBena971_Pnd_Illgble_Ind(),
            NEWLINE,"=",pdaBena971.getBena971_Pnd_More_Than_Five_Benes_Ind(),NEWLINE,"=",pdaBena971.getBena971_Pnd_Nmbr_Of_Benes(),NEWLINE,"=",pdaBena971.getBena971_Pnd_Intrfce_Mgrtn_Ind(),
            NEWLINE,"=",pdaBena971.getBena971_Pnd_Pin(),NEWLINE,"=",pdaBena971.getBena971_Pnd_Tiaa_Cntrct(),NEWLINE,"=",pdaBena971.getBena971_Pnd_Cref_Cntrct(),
            NEWLINE,"=",pdaBena971.getBena971_Pnd_Cntrct_Type(),NEWLINE,"=",pdaBena971.getBena971_Pnd_Effctve_Dte(),NEWLINE,"=",pdaBena971.getBena971_Pnd_Tiaa_Cref_Ind(),
            NEWLINE,"=",pdaBena971.getBena971_Pnd_Same_As_Ind(),NEWLINE,"=",pdaBena971.getBena971_Pnd_Mos_Ind(),NEWLINE,"=",pdaBena971.getBena971_Pnd_Mos_Irrvcble_Ind(),
            NEWLINE,"=",pdaBena971.getBena971_Pnd_Pymnt_Chld_Dcsd_Ind(),NEWLINE,"=",pdaBena971.getBena971_Pnd_Exempt_Spouse_Rights(),NEWLINE,"=",pdaBena971.getBena971_Pnd_Spouse_Waived_Bnfts(),
            NEWLINE,"=",pdaBena971.getBena971_Pnd_Fldr_Log_Dte_Tme(),NEWLINE,"=",pdaBena971.getBena971_Pnd_Dflt_To_Estate(),NEWLINE,"=",pdaBena971.getBena971_Pnd_Mos_Txt().getValue(1));
        if (Global.isEscape()) return;
        FOR01:                                                                                                                                                            //Natural: FOR #X = 1 TO BENA971.#NMBR-OF-BENES
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(pdaBena971.getBena971_Pnd_Nmbr_Of_Benes())); pnd_X.nadd(1))
        {
            getReports().write(0, ReportOption.NOTITLE,"Bene #",pnd_X,NEWLINE,"=",pdaBena971.getBena971_Pnd_Bene_Name1().getValue(pnd_X),NEWLINE,"=",pdaBena971.getBena971_Pnd_Bene_Name2().getValue(pnd_X), //Natural: WRITE NOTITLE 'Bene #' #X / '=' BENA971.#BENE-NAME1 ( #X ) / '=' BENA971.#BENE-NAME2 ( #X ) / '=' BENA971.#BENE-TYPE ( #X ) / '=' BENA971.#DTE-BIRTH-TRUST ( #X ) / '=' BENA971.#RTLN-CDE ( #X ) / '=' BENA971.#RLTN-FREE-TXT ( #X ) / '=' BENA971.#SS-CD ( #X ) / '=' BENA971.#SS-NBR ( #X ) / '=' BENA971.#PRCTGE-FRCTN-IND ( #X ) / '=' BENA971.#PRCTGE ( #X ) / '=' BENA971.#NMRTR ( #X ) / '=' BENA971.#DNMNTR ( #X ) / '=' BENA971.#IRRVCBL-IND ( #X ) / '=' BENA971.#STTLMNT-RSTRCTN ( #X ) / '=' BENA971.#SPCL-TXT ( #X,1 ) / '=' BENA971.#ADDR1 ( #X ) / '=' BENA971.#ADDR2 ( #X ) / '=' BENA971.#ADDR3-CITY ( #X ) / '=' BENA971.#STATE ( #X ) / '=' BENA971.#ZIP ( #X ) / '=' BENA971.#COUNTRY ( #X ) / '=' BENA971.#PHONE ( #X ) / '=' BENA971.#GENDER ( #X )
                NEWLINE,"=",pdaBena971.getBena971_Pnd_Bene_Type().getValue(pnd_X),NEWLINE,"=",pdaBena971.getBena971_Pnd_Dte_Birth_Trust().getValue(pnd_X),
                NEWLINE,"=",pdaBena971.getBena971_Pnd_Rtln_Cde().getValue(pnd_X),NEWLINE,"=",pdaBena971.getBena971_Pnd_Rltn_Free_Txt().getValue(pnd_X),NEWLINE,
                "=",pdaBena971.getBena971_Pnd_Ss_Cd().getValue(pnd_X),NEWLINE,"=",pdaBena971.getBena971_Pnd_Ss_Nbr().getValue(pnd_X),NEWLINE,"=",pdaBena971.getBena971_Pnd_Prctge_Frctn_Ind().getValue(pnd_X),
                NEWLINE,"=",pdaBena971.getBena971_Pnd_Prctge().getValue(pnd_X),NEWLINE,"=",pdaBena971.getBena971_Pnd_Nmrtr().getValue(pnd_X),NEWLINE,"=",
                pdaBena971.getBena971_Pnd_Dnmntr().getValue(pnd_X),NEWLINE,"=",pdaBena971.getBena971_Pnd_Irrvcbl_Ind().getValue(pnd_X),NEWLINE,"=",pdaBena971.getBena971_Pnd_Sttlmnt_Rstrctn().getValue(pnd_X),
                NEWLINE,"=",pdaBena971.getBena971_Pnd_Spcl_Txt().getValue(pnd_X,1),NEWLINE,"=",pdaBena971.getBena971_Pnd_Addr1().getValue(pnd_X),NEWLINE,
                "=",pdaBena971.getBena971_Pnd_Addr2().getValue(pnd_X),NEWLINE,"=",pdaBena971.getBena971_Pnd_Addr3_City().getValue(pnd_X),NEWLINE,"=",pdaBena971.getBena971_Pnd_State().getValue(pnd_X),
                NEWLINE,"=",pdaBena971.getBena971_Pnd_Zip().getValue(pnd_X),NEWLINE,"=",pdaBena971.getBena971_Pnd_Country().getValue(pnd_X),NEWLINE,"=",
                pdaBena971.getBena971_Pnd_Phone().getValue(pnd_X),NEWLINE,"=",pdaBena971.getBena971_Pnd_Gender().getValue(pnd_X));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  DUMP-BENA971
    }
    private void sub_Dump_Workfile() throws Exception                                                                                                                     //Natural: DUMP-WORKFILE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,"#INTRFCE-BSNSS-DTE       ",pnd_Omni_Pnd_Intrfce_Bsnss_Dte,NEWLINE,"#INTRFCNG-SYSTM          ",                //Natural: WRITE ( 0 ) NOTITLE / '#INTRFCE-BSNSS-DTE       ' #OMNI.#INTRFCE-BSNSS-DTE / '#INTRFCNG-SYSTM          ' #OMNI.#INTRFCNG-SYSTM / '#RQSTNG-SYSTEM           ' #OMNI.#RQSTNG-SYSTEM / '#NEW-ISSUE/CHNG-IND      ' #OMNI.#NEW-ISSUE/CHNG-IND / '#ILLGBLE-IND             ' #OMNI.#ILLGBLE-IND / '#MORE-THAN-FIVE-BENES-IND' #OMNI.#MORE-THAN-FIVE-BENES-IND / '#NMBR-OF-BENES           ' #OMNI.#NMBR-OF-BENES / '#INTRFCE-MGRTN-IND       ' #OMNI.#INTRFCE-MGRTN-IND / '#PIN                     ' #OMNI.#PIN / '#TIAA-CNTRCT             ' #OMNI.#TIAA-CNTRCT / '#CREF-CNTRCT             ' #OMNI.#CREF-CNTRCT / '#CNTRCT-TYPE             ' #OMNI.#CNTRCT-TYPE / '#EFFCTVE-DTE             ' #OMNI.#EFFCTVE-DTE / '#TIAA-CREF-IND           ' #OMNI.#TIAA-CREF-IND / '#SAME-AS-IND             ' #OMNI.#SAME-AS-IND / '#MOS-IND                 ' #OMNI.#MOS-IND / '#MOS-IRRVCBLE-IND        ' #OMNI.#MOS-IRRVCBLE-IND / '#PYMNT-CHLD-DCSD-IND     ' #OMNI.#PYMNT-CHLD-DCSD-IND / '#EXEMPT-SPOUSE-RIGHTS    ' #OMNI.#EXEMPT-SPOUSE-RIGHTS / '#SPOUSE-WAIVED-BNFTS     ' #OMNI.#SPOUSE-WAIVED-BNFTS / '#FLDR-LOG-DTE-TME        ' #OMNI.#FLDR-LOG-DTE-TME / '#DFLT-TO-ESTATE          ' #OMNI.#DFLT-TO-ESTATE / '#MOS-TXT (1)             ' #OMNI.#MOS-TXT ( 1 )
            pnd_Omni_Pnd_Intrfcng_Systm,NEWLINE,"#RQSTNG-SYSTEM           ",pnd_Omni_Pnd_Rqstng_System,NEWLINE,"#NEW-ISSUE/CHNG-IND      ",pnd_Omni_Pnd_New_Issuefslash_Chng_Ind,
            NEWLINE,"#ILLGBLE-IND             ",pnd_Omni_Pnd_Illgble_Ind,NEWLINE,"#MORE-THAN-FIVE-BENES-IND",pnd_Omni_Pnd_More_Than_Five_Benes_Ind,NEWLINE,
            "#NMBR-OF-BENES           ",pnd_Omni_Pnd_Nmbr_Of_Benes,NEWLINE,"#INTRFCE-MGRTN-IND       ",pnd_Omni_Pnd_Intrfce_Mgrtn_Ind,NEWLINE,"#PIN                     ",
            pnd_Omni_Pnd_Pin,NEWLINE,"#TIAA-CNTRCT             ",pnd_Omni_Pnd_Tiaa_Cntrct,NEWLINE,"#CREF-CNTRCT             ",pnd_Omni_Pnd_Cref_Cntrct,NEWLINE,
            "#CNTRCT-TYPE             ",pnd_Omni_Pnd_Cntrct_Type,NEWLINE,"#EFFCTVE-DTE             ",pnd_Omni_Pnd_Effctve_Dte,NEWLINE,"#TIAA-CREF-IND           ",
            pnd_Omni_Pnd_Tiaa_Cref_Ind,NEWLINE,"#SAME-AS-IND             ",pnd_Omni_Pnd_Same_As_Ind,NEWLINE,"#MOS-IND                 ",pnd_Omni_Pnd_Mos_Ind,
            NEWLINE,"#MOS-IRRVCBLE-IND        ",pnd_Omni_Pnd_Mos_Irrvcble_Ind,NEWLINE,"#PYMNT-CHLD-DCSD-IND     ",pnd_Omni_Pnd_Pymnt_Chld_Dcsd_Ind,NEWLINE,
            "#EXEMPT-SPOUSE-RIGHTS    ",pnd_Omni_Pnd_Exempt_Spouse_Rights,NEWLINE,"#SPOUSE-WAIVED-BNFTS     ",pnd_Omni_Pnd_Spouse_Waived_Bnfts,NEWLINE,"#FLDR-LOG-DTE-TME        ",
            pnd_Omni_Pnd_Fldr_Log_Dte_Tme,NEWLINE,"#DFLT-TO-ESTATE          ",pnd_Omni_Pnd_Dflt_To_Estate,NEWLINE,"#MOS-TXT (1)             ",pnd_Omni_Pnd_Mos_Txt.getValue(1));
        if (Global.isEscape()) return;
        FOR02:                                                                                                                                                            //Natural: FOR #X = 1 TO #OMNI.#NMBR-OF-BENES
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(pnd_Omni_Pnd_Nmbr_Of_Benes)); pnd_X.nadd(1))
        {
            getReports().write(0, ReportOption.NOTITLE,"Bene #",pnd_X,NEWLINE,"#BENE-NAME1       ",pnd_Omni_Pnd_Bene_Name1.getValue(pnd_X),NEWLINE,"#BENE-NAME2       ",  //Natural: WRITE NOTITLE 'Bene #' #X / '#BENE-NAME1       ' #OMNI.#BENE-NAME1 ( #X ) / '#BENE-NAME2       ' #OMNI.#BENE-NAME2 ( #X ) / '#BENE-TYPE        ' #OMNI.#BENE-TYPE ( #X ) / '#DTE-BIRTH-TRUST  ' #OMNI.#DTE-BIRTH-TRUST ( #X ) / '#RTLN-CDE         ' #OMNI.#RTLN-CDE ( #X ) / '#RLTN-FREE-TXT    ' #OMNI.#RLTN-FREE-TXT ( #X ) / '#SS-CD            ' #OMNI.#SS-CD ( #X ) / '#SS-NBR           ' #OMNI.#SS-NBR ( #X ) / '#PRCTGE-FRCTN-IND ' #OMNI.#PRCTGE-FRCTN-IND ( #X ) / '#PRCTGE           ' #OMNI.#PRCTGE ( #X ) / '#NMRTR            ' #OMNI.#NMRTR ( #X ) / '#DNMNTR           ' #OMNI.#DNMNTR ( #X ) / '#IRRVCBL-IND      ' #OMNI.#IRRVCBL-IND ( #X ) / '#STTLMNT-RSTRCTN  ' #OMNI.#STTLMNT-RSTRCTN ( #X ) / '#SPCL-TXT         ' #OMNI.#SPCL-TXT ( #X,1 ) / '#ADDR1            ' #OMNI.#ADDR1 ( #X ) / '#ADDR2            ' #OMNI.#ADDR2 ( #X ) / '#ADDR3-CITY       ' #OMNI.#ADDR3-CITY ( #X ) / '#STATE            ' #OMNI.#STATE ( #X ) / '#ZIP              ' #OMNI.#ZIP ( #X ) / '#COUNTRY          ' #OMNI.#COUNTRY ( #X ) / '#PHONE            ' #OMNI.#PHONE ( #X ) / '#GENDER           ' #OMNI.#GENDER ( #X )
                pnd_Omni_Pnd_Bene_Name2.getValue(pnd_X),NEWLINE,"#BENE-TYPE        ",pnd_Omni_Pnd_Bene_Type.getValue(pnd_X),NEWLINE,"#DTE-BIRTH-TRUST  ",
                pnd_Omni_Pnd_Dte_Birth_Trust.getValue(pnd_X),NEWLINE,"#RTLN-CDE         ",pnd_Omni_Pnd_Rtln_Cde.getValue(pnd_X),NEWLINE,"#RLTN-FREE-TXT    ",
                pnd_Omni_Pnd_Rltn_Free_Txt.getValue(pnd_X),NEWLINE,"#SS-CD            ",pnd_Omni_Pnd_Ss_Cd.getValue(pnd_X),NEWLINE,"#SS-NBR           ",
                pnd_Omni_Pnd_Ss_Nbr.getValue(pnd_X),NEWLINE,"#PRCTGE-FRCTN-IND ",pnd_Omni_Pnd_Prctge_Frctn_Ind.getValue(pnd_X),NEWLINE,"#PRCTGE           ",
                pnd_Omni_Pnd_Prctge.getValue(pnd_X),NEWLINE,"#NMRTR            ",pnd_Omni_Pnd_Nmrtr.getValue(pnd_X),NEWLINE,"#DNMNTR           ",pnd_Omni_Pnd_Dnmntr.getValue(pnd_X),
                NEWLINE,"#IRRVCBL-IND      ",pnd_Omni_Pnd_Irrvcbl_Ind.getValue(pnd_X),NEWLINE,"#STTLMNT-RSTRCTN  ",pnd_Omni_Pnd_Sttlmnt_Rstrctn.getValue(pnd_X),
                NEWLINE,"#SPCL-TXT         ",pnd_Omni_Pnd_Spcl_Txt.getValue(pnd_X,1),NEWLINE,"#ADDR1            ",pnd_Omni_Pnd_Addr1.getValue(pnd_X),NEWLINE,
                "#ADDR2            ",pnd_Omni_Pnd_Addr2.getValue(pnd_X),NEWLINE,"#ADDR3-CITY       ",pnd_Omni_Pnd_Addr3_City.getValue(pnd_X),NEWLINE,"#STATE            ",
                pnd_Omni_Pnd_State.getValue(pnd_X),NEWLINE,"#ZIP              ",pnd_Omni_Pnd_Zip.getValue(pnd_X),NEWLINE,"#COUNTRY          ",pnd_Omni_Pnd_Country.getValue(pnd_X),
                NEWLINE,"#PHONE            ",pnd_Omni_Pnd_Phone.getValue(pnd_X),NEWLINE,"#GENDER           ",pnd_Omni_Pnd_Gender.getValue(pnd_X));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  DUMP-WORKFILE
    }
    private void sub_Edit_Routine() throws Exception                                                                                                                      //Natural: EDIT-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Edit.reset();                                                                                                                                                 //Natural: RESET #EDIT
        short decideConditionsMet399 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN NOT ( #OMNI.#INTRFCE-BSNSS-DTE = MASK ( YYYYMMDD ) )
        if (condition(! ((DbsUtil.maskMatches(pnd_Omni_Pnd_Intrfce_Bsnss_Dte,"YYYYMMDD")))))
        {
            decideConditionsMet399++;
            pnd_Edit.setValue("Business Date not in YYYYMMDD format");                                                                                                    //Natural: ASSIGN #EDIT := 'Business Date not in YYYYMMDD format'
        }                                                                                                                                                                 //Natural: WHEN NOT ( #OMNI.#INTRFCNG-SYSTM = 'OMNI' )
        else if (condition(! ((pnd_Omni_Pnd_Intrfcng_Systm.equals("OMNI")))))
        {
            decideConditionsMet399++;
            pnd_Edit.setValue("Interface System must be 'OMNI'");                                                                                                         //Natural: ASSIGN #EDIT := 'Interface System must be "OMNI"'
        }                                                                                                                                                                 //Natural: WHEN NOT ( #OMNI.#RQSTNG-SYSTEM = 'OMNI' )
        else if (condition(! ((pnd_Omni_Pnd_Rqstng_System.equals("OMNI")))))
        {
            decideConditionsMet399++;
            pnd_Edit.setValue("Requesting System must be 'OMNI'");                                                                                                        //Natural: ASSIGN #EDIT := 'Requesting System must be "OMNI"'
        }                                                                                                                                                                 //Natural: WHEN NOT ( #OMNI.#NEW-ISSUE/CHNG-IND = 'N' )
        else if (condition(! ((pnd_Omni_Pnd_New_Issuefslash_Chng_Ind.equals("N")))))
        {
            decideConditionsMet399++;
            pnd_Edit.setValue("New Issue Ind must be 'N' (NEW)");                                                                                                         //Natural: ASSIGN #EDIT := 'New Issue Ind must be "N" (NEW)'
        }                                                                                                                                                                 //Natural: WHEN NOT ( #OMNI.#NMBR-OF-BENES > 0 AND #OMNI.#NMBR-OF-BENES <= 30 )
        else if (condition(! ((pnd_Omni_Pnd_Nmbr_Of_Benes.greater(getZero()) && pnd_Omni_Pnd_Nmbr_Of_Benes.lessOrEqual(30)))))
        {
            decideConditionsMet399++;
            pnd_Edit.setValue("Number of Benes must be 1 thru 30");                                                                                                       //Natural: ASSIGN #EDIT := 'Number of Benes must be 1 thru 30'
        }                                                                                                                                                                 //Natural: WHEN NOT ( #OMNI.#INTRFCE-MGRTN-IND = 'I' )
        else if (condition(! ((pnd_Omni_Pnd_Intrfce_Mgrtn_Ind.equals("I")))))
        {
            decideConditionsMet399++;
            pnd_Edit.setValue("Interface Ind must be 'I' (INTERFACE)");                                                                                                   //Natural: ASSIGN #EDIT := 'Interface Ind must be "I" (INTERFACE)'
        }                                                                                                                                                                 //Natural: WHEN NOT ( #OMNI.#PIN > 0 AND #OMNI.#PIN = MASK ( 9999999 ) )
        else if (condition(! ((pnd_Omni_Pnd_Pin.greater(getZero()) && DbsUtil.maskMatches(pnd_Omni_Pnd_Pin,"9999999")))))
        {
            decideConditionsMet399++;
            pnd_Edit.setValue("Missing PIN");                                                                                                                             //Natural: ASSIGN #EDIT := 'Missing PIN'
        }                                                                                                                                                                 //Natural: WHEN #OMNI.#TIAA-CNTRCT = ' '
        else if (condition(pnd_Omni_Pnd_Tiaa_Cntrct.equals(" ")))
        {
            decideConditionsMet399++;
            pnd_Edit.setValue("Missing TIAA Contract");                                                                                                                   //Natural: ASSIGN #EDIT := 'Missing TIAA Contract'
        }                                                                                                                                                                 //Natural: WHEN #OMNI.#CREF-CNTRCT = ' ' AND #OMNI.#CNTRCT-TYPE = 'D'
        else if (condition(pnd_Omni_Pnd_Cref_Cntrct.equals(" ") && pnd_Omni_Pnd_Cntrct_Type.equals("D")))
        {
            decideConditionsMet399++;
            pnd_Edit.setValue("Missing CREF Contract");                                                                                                                   //Natural: ASSIGN #EDIT := 'Missing CREF Contract'
        }                                                                                                                                                                 //Natural: WHEN NOT ( #OMNI.#CNTRCT-TYPE = 'D' OR = 'I' )
        else if (condition(! ((pnd_Omni_Pnd_Cntrct_Type.equals("D") || pnd_Omni_Pnd_Cntrct_Type.equals("I")))))
        {
            decideConditionsMet399++;
            pnd_Edit.setValue("Contract Type must be 'D' (DA) or 'I' (IA)");                                                                                              //Natural: ASSIGN #EDIT := 'Contract Type must be "D" (DA) or "I" (IA)'
        }                                                                                                                                                                 //Natural: WHEN NOT ( #OMNI.#EFFCTVE-DTE = MASK ( YYYYMMDD ) )
        else if (condition(! ((DbsUtil.maskMatches(pnd_Omni_Pnd_Effctve_Dte,"YYYYMMDD")))))
        {
            decideConditionsMet399++;
            pnd_Edit.setValue("Effective Date not in YYYYMMDD format");                                                                                                   //Natural: ASSIGN #EDIT := 'Effective Date not in YYYYMMDD format'
        }                                                                                                                                                                 //Natural: WHEN NOT ( #OMNI.#TIAA-CREF-IND = ' ' )
        else if (condition(! ((pnd_Omni_Pnd_Tiaa_Cref_Ind.equals(" ")))))
        {
            decideConditionsMet399++;
            pnd_Edit.setValue("Invalid TIAA-CREF Ind");                                                                                                                   //Natural: ASSIGN #EDIT := 'Invalid TIAA-CREF Ind'
        }                                                                                                                                                                 //Natural: WHEN NOT ( #OMNI.#MOS-IND = 'Y' OR = 'N' )
        else if (condition(! ((pnd_Omni_Pnd_Mos_Ind.equals("Y") || pnd_Omni_Pnd_Mos_Ind.equals("N")))))
        {
            decideConditionsMet399++;
            pnd_Edit.setValue("MOS Ind must be 'Y' (MOS) or 'N' (Regular Bene)");                                                                                         //Natural: ASSIGN #EDIT := 'MOS Ind must be "Y" (MOS) or "N" (Regular Bene)'
        }                                                                                                                                                                 //Natural: WHEN #OMNI.#MOS-IND = 'Y' AND #OMNI.#MOS-TXT ( 1 ) = ' '
        else if (condition(pnd_Omni_Pnd_Mos_Ind.equals("Y") && pnd_Omni_Pnd_Mos_Txt.getValue(1).equals(" ")))
        {
            decideConditionsMet399++;
            pnd_Edit.setValue("Missing MOS Text(s)");                                                                                                                     //Natural: ASSIGN #EDIT := 'Missing MOS Text(s)'
        }                                                                                                                                                                 //Natural: WHEN NOT ( #OMNI.#PYMNT-CHLD-DCSD-IND = ' ' )
        else if (condition(! ((pnd_Omni_Pnd_Pymnt_Chld_Dcsd_Ind.equals(" ")))))
        {
            decideConditionsMet399++;
            pnd_Edit.setValue("Invalid Payment to Children of Deceased Ind");                                                                                             //Natural: ASSIGN #EDIT := 'Invalid Payment to Children of Deceased Ind'
        }                                                                                                                                                                 //Natural: WHEN NOT ( #OMNI.#EXEMPT-SPOUSE-RIGHTS = ' ' )
        else if (condition(! ((pnd_Omni_Pnd_Exempt_Spouse_Rights.equals(" ")))))
        {
            decideConditionsMet399++;
            pnd_Edit.setValue("Invalid Exempt from Spousal Rights Ind");                                                                                                  //Natural: ASSIGN #EDIT := 'Invalid Exempt from Spousal Rights Ind'
        }                                                                                                                                                                 //Natural: WHEN NOT ( #OMNI.#SPOUSE-WAIVED-BNFTS = ' ' )
        else if (condition(! ((pnd_Omni_Pnd_Spouse_Waived_Bnfts.equals(" ")))))
        {
            decideConditionsMet399++;
            pnd_Edit.setValue("Invalud Spouse Waived Benefits Ind");                                                                                                      //Natural: ASSIGN #EDIT := 'Invalud Spouse Waived Benefits Ind'
        }                                                                                                                                                                 //Natural: WHEN NOT ( #OMNI.#FLDR-LOG-DTE-TME = ' ' )
        else if (condition(! ((pnd_Omni_Pnd_Fldr_Log_Dte_Tme.equals(" ")))))
        {
            decideConditionsMet399++;
            pnd_Edit.setValue("Folder Log Date Time must be blank");                                                                                                      //Natural: ASSIGN #EDIT := 'Folder Log Date Time must be blank'
        }                                                                                                                                                                 //Natural: WHEN NOT ( #OMNI.#DFLT-TO-ESTATE = ' ' OR = 'Y' OR = 'N' )
        else if (condition(! ((pnd_Omni_Pnd_Dflt_To_Estate.equals(" ") || pnd_Omni_Pnd_Dflt_To_Estate.equals("Y") || pnd_Omni_Pnd_Dflt_To_Estate.equals("N")))))
        {
            decideConditionsMet399++;
            pnd_Edit.setValue("Default To Estate Ind must be Blank or 'Y' (Default)");                                                                                    //Natural: ASSIGN #EDIT := 'Default To Estate Ind must be Blank or "Y" (Default)'
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet399 > 0))
        {
            pnd_Fail_Edit.setValue(true);                                                                                                                                 //Natural: ASSIGN #FAIL-EDIT := TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pnd_Fail_Edit.reset();                                                                                                                                        //Natural: RESET #FAIL-EDIT
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_Omni_Pnd_Mos_Ind.equals("N")))                                                                                                                  //Natural: IF #OMNI.#MOS-IND = 'N'
        {
            FOR03:                                                                                                                                                        //Natural: FOR #X = 1 TO #OMNI.#NMBR-OF-BENES
            for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(pnd_Omni_Pnd_Nmbr_Of_Benes)); pnd_X.nadd(1))
            {
                short decideConditionsMet447 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #OMNI.#BENE-NAME1 ( #X ) = ' '
                if (condition(pnd_Omni_Pnd_Bene_Name1.getValue(pnd_X).equals(" ")))
                {
                    decideConditionsMet447++;
                    pnd_Edit.setValue("Missing Beneficiary Name");                                                                                                        //Natural: ASSIGN #EDIT := 'Missing Beneficiary Name'
                }                                                                                                                                                         //Natural: WHEN NOT ( #OMNI.#BENE-TYPE ( #X ) = 'P' OR = 'C' )
                else if (condition(! ((pnd_Omni_Pnd_Bene_Type.getValue(pnd_X).equals("P") || pnd_Omni_Pnd_Bene_Type.getValue(pnd_X).equals("C")))))
                {
                    decideConditionsMet447++;
                    pnd_Edit.setValue("Bene Type must be 'P' (Primary) or 'C' (Contingent)");                                                                             //Natural: ASSIGN #EDIT := 'Bene Type must be "P" (Primary) or "C" (Contingent)'
                }                                                                                                                                                         //Natural: WHEN #OMNI.#DTE-BIRTH-TRUST ( #X ) NE MASK ( YYYYMMDD ) AND #OMNI.#DTE-BIRTH-TRUST ( #X ) > ' ' AND #OMNI.#DTE-BIRTH-TRUST ( #X ) NE '00000000'
                else if (condition(! ((DbsUtil.maskMatches(pnd_Omni_Pnd_Dte_Birth_Trust.getValue(pnd_X),"YYYYMMDD")) && pnd_Omni_Pnd_Dte_Birth_Trust.getValue(pnd_X).greater(" ") 
                    && pnd_Omni_Pnd_Dte_Birth_Trust.getValue(pnd_X).notEquals("00000000"))))
                {
                    decideConditionsMet447++;
                    pnd_Edit.setValue("Bene Date of Birth/Trust must be in YYYYMMDD format");                                                                             //Natural: ASSIGN #EDIT := 'Bene Date of Birth/Trust must be in YYYYMMDD format'
                }                                                                                                                                                         //Natural: WHEN #OMNI.#RTLN-CDE ( #X ) > ' ' AND #OMNI.#RTLN-CDE ( #X ) NE MASK ( 99 )
                else if (condition(pnd_Omni_Pnd_Rtln_Cde.getValue(pnd_X).greater(" ") && ! (DbsUtil.maskMatches(pnd_Omni_Pnd_Rtln_Cde.getValue(pnd_X),
                    "99"))))
                {
                    decideConditionsMet447++;
                    pnd_Edit.setValue("Invalid Relationship Code format");                                                                                                //Natural: ASSIGN #EDIT := 'Invalid Relationship Code format'
                }                                                                                                                                                         //Natural: WHEN #OMNI.#RTLN-CDE ( #X ) = ' ' AND #OMNI.#RLTN-FREE-TXT ( #X ) NE ' '
                else if (condition(pnd_Omni_Pnd_Rtln_Cde.getValue(pnd_X).equals(" ") && pnd_Omni_Pnd_Rltn_Free_Txt.getValue(pnd_X).notEquals(" ")))
                {
                    decideConditionsMet447++;
                    pnd_Edit.setValue("Missing Relationship Code");                                                                                                       //Natural: ASSIGN #EDIT := 'Missing Relationship Code'
                }                                                                                                                                                         //Natural: WHEN #OMNI.#SS-NBR ( #X ) > ' ' AND #OMNI.#SS-NBR ( #X ) NE '000000000' AND NOT ( #OMNI.#SS-CD ( #X ) = 'S' OR = 'I' OR = 'T' )
                else if (condition(((pnd_Omni_Pnd_Ss_Nbr.getValue(pnd_X).greater(" ") && pnd_Omni_Pnd_Ss_Nbr.getValue(pnd_X).notEquals("000000000")) && 
                    ! (((pnd_Omni_Pnd_Ss_Cd.getValue(pnd_X).equals("S") || pnd_Omni_Pnd_Ss_Cd.getValue(pnd_X).equals("I")) || pnd_Omni_Pnd_Ss_Cd.getValue(pnd_X).equals("T"))))))
                {
                    decideConditionsMet447++;
                    pnd_Edit.setValue("Missing SS Cd (S/I/T) & SS Number");                                                                                               //Natural: ASSIGN #EDIT := 'Missing SS Cd (S/I/T) & SS Number'
                }                                                                                                                                                         //Natural: WHEN NOT ( #OMNI.#PRCTGE-FRCTN-IND ( #X ) = 'P' OR = 'S' )
                else if (condition(! ((pnd_Omni_Pnd_Prctge_Frctn_Ind.getValue(pnd_X).equals("P") || pnd_Omni_Pnd_Prctge_Frctn_Ind.getValue(pnd_X).equals("S")))))
                {
                    decideConditionsMet447++;
                    pnd_Edit.setValue("Ind must be 'P' (Percentage) or 'S' (Share)");                                                                                     //Natural: ASSIGN #EDIT := 'Ind must be "P" (Percentage) or "S" (Share)'
                }                                                                                                                                                         //Natural: WHEN #OMNI.#PRCTGE-FRCTN-IND ( #X ) = 'P' AND NOT ( #OMNI.#PRCTGE ( #X ) > 0 AND #OMNI.#PRCTGE ( #X ) <= 100 )
                else if (condition(pnd_Omni_Pnd_Prctge_Frctn_Ind.getValue(pnd_X).equals("P") && ! (pnd_Omni_Pnd_Prctge.getValue(pnd_X).greater(getZero()) 
                    && pnd_Omni_Pnd_Prctge.getValue(pnd_X).lessOrEqual(100))))
                {
                    decideConditionsMet447++;
                    pnd_Edit.setValue("Percent must be 1 thru 100");                                                                                                      //Natural: ASSIGN #EDIT := 'Percent must be 1 thru 100'
                }                                                                                                                                                         //Natural: WHEN ANY
                if (condition(decideConditionsMet447 > 0))
                {
                    pnd_Fail_Edit.setValue(true);                                                                                                                         //Natural: ASSIGN #FAIL-EDIT := TRUE
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    pnd_Fail_Edit.reset();                                                                                                                                //Natural: RESET #FAIL-EDIT
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT-ROUTINE
    }
    private void sub_Write_Add_Report() throws Exception                                                                                                                  //Natural: WRITE-ADD-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Total_Add.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #TOTAL-ADD
        if (condition(getReports().getAstLinesLeft(1).less(2)))                                                                                                           //Natural: NEWPAGE ( 1 ) WHEN LESS THAN 2 LINES LEFT
        {
            getReports().newPage(1);
            if (condition(Global.isEscape())){return;}
        }
        getReports().display(1, ReportOption.NOTITLE,"PIN",                                                                                                               //Natural: DISPLAY ( 1 ) NOTITLE 'PIN' #OMNI.#PIN 'TIAA' #OMNI.#TIAA-CNTRCT 'CREF' #OMNI.#CREF-CNTRCT 'MOS' #OMNI.#MOS-IND '#' #OMNI.#NMBR-OF-BENES 'IntrfcDt' #OMNI.#INTRFCE-BSNSS-DTE 'EffDate' #OMNI.#EFFCTVE-DTE
        		pnd_Omni_Pnd_Pin,"TIAA",
        		pnd_Omni_Pnd_Tiaa_Cntrct,"CREF",
        		pnd_Omni_Pnd_Cref_Cntrct,"MOS",
        		pnd_Omni_Pnd_Mos_Ind,"#",
        		pnd_Omni_Pnd_Nmbr_Of_Benes,"IntrfcDt",
        		pnd_Omni_Pnd_Intrfce_Bsnss_Dte,"EffDate",
        		pnd_Omni_Pnd_Effctve_Dte);
        if (Global.isEscape()) return;
        //*  WRITE-ADD-REPORT
    }
    private void sub_Write_Error_Report() throws Exception                                                                                                                //Natural: WRITE-ERROR-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Total_Error.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #TOTAL-ERROR
        if (condition(getReports().getAstLinesLeft(2).less(2)))                                                                                                           //Natural: NEWPAGE ( 2 ) WHEN LESS THAN 2 LINES LEFT
        {
            getReports().newPage(2);
            if (condition(Global.isEscape())){return;}
        }
        getReports().display(2, ReportOption.NOTITLE,"PIN",                                                                                                               //Natural: DISPLAY ( 2 ) NOTITLE 'PIN' #OMNI.#PIN 'TIAA' #OMNI.#TIAA-CNTRCT 'Error' #EDIT
        		pnd_Omni_Pnd_Pin,"TIAA",
        		pnd_Omni_Pnd_Tiaa_Cntrct,"Error",
        		pnd_Edit);
        if (Global.isEscape()) return;
        //*  WRITE-ERROR-REPORT
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(57),"Beneficiary System",new TabSetting(120),"Page:",getReports().getPageNumberDbs(1),  //Natural: WRITE ( 1 ) NOTITLE 001T *PROGRAM 057T 'Beneficiary System' 120T 'Page:' *PAGE-NUMBER ( 1 ) ( EM = ZZ,ZZ9 ) / 001T *DATX ( EM = MM/DD/YYYY ) 040T 'Bene-In-The-Plan - Successful Beneficiary Interface' 120T *TIMX ( EM = HH:II:SS' 'AP ) /
                        new ReportEditMask ("ZZ,ZZ9"),NEWLINE,new TabSetting(1),Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new TabSetting(40),"Bene-In-The-Plan - Successful Beneficiary Interface",new 
                        TabSetting(120),Global.getTIMX(), new ReportEditMask ("HH:II:SS' 'AP"),NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(57),"Beneficiary System",new TabSetting(120),"Page:",getReports().getPageNumberDbs(2),  //Natural: WRITE ( 2 ) NOTITLE 001T *PROGRAM 057T 'Beneficiary System' 120T 'Page:' *PAGE-NUMBER ( 2 ) ( EM = ZZ,ZZ9 ) / 001T *DATX ( EM = MM/DD/YYYY ) 042T 'Bene-In-The-Plan - Failed Beneficiary Interface' 120T *TIMX ( EM = HH:II:SS' 'AP ) /
                        new ReportEditMask ("ZZ,ZZ9"),NEWLINE,new TabSetting(1),Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new TabSetting(42),"Bene-In-The-Plan - Failed Beneficiary Interface",new 
                        TabSetting(120),Global.getTIMX(), new ReportEditMask ("HH:II:SS' 'AP"),NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        pdaBena971.getBena971_Pnd_Error_Msg().setValue(DbsUtil.compress("NATURAL Error", Global.getERROR_NR(), "at line", Global.getERROR_LINE(), "in Program",           //Natural: COMPRESS 'NATURAL Error' *ERROR-NR 'at line' *ERROR-LINE 'in Program' *PROGRAM INTO BENA971.#ERROR-MSG
            Global.getPROGRAM()));
        pdaBena971.getBena971_Pnd_Return_Code().setValue("E");                                                                                                            //Natural: ASSIGN BENA971.#RETURN-CODE := 'E'
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        if (condition(true)) return;                                                                                                                                      //Natural: ESCAPE ROUTINE
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=60 SG=OFF ZP=ON");
        Global.format(1, "LS=133 PS=60 SG=OFF ZP=ON");
        Global.format(2, "LS=133 PS=60 SG=OFF ZP=ON");

        getReports().setDisplayColumns(1, ReportOption.NOTITLE,"PIN",
        		pnd_Omni_Pnd_Pin,"TIAA",
        		pnd_Omni_Pnd_Tiaa_Cntrct,"CREF",
        		pnd_Omni_Pnd_Cref_Cntrct,"MOS",
        		pnd_Omni_Pnd_Mos_Ind,"#",
        		pnd_Omni_Pnd_Nmbr_Of_Benes,"IntrfcDt",
        		pnd_Omni_Pnd_Intrfce_Bsnss_Dte,"EffDate",
        		pnd_Omni_Pnd_Effctve_Dte);
        getReports().setDisplayColumns(2, ReportOption.NOTITLE,"PIN",
        		pnd_Omni_Pnd_Pin,"TIAA",
        		pnd_Omni_Pnd_Tiaa_Cntrct,"Error",
        		pnd_Edit);
    }
}
