/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:14:33 PM
**        * FROM NATURAL PROGRAM : Benp225
************************************************************
**        * FILE NAME            : Benp225.java
**        * CLASS NAME           : Benp225
**        * INSTANCE NAME        : Benp225
************************************************************
************************************************************************
* PROGRAM NAME : BENP225
* DESCRIPTION  : BENEFICIARY BATCH SYSTEM - CNFRMTN STATEMENT REGISTER
* WRITTEN BY   : ADRIAN EATON
* DATE WRITTEN : 06/15/1999
************************************************************************
*    DATE      USERID                   DESCRIPTION
* 09/17/1999  EATONA    AMEND PROGRAM TO PRINT PAGE PER UNIT
* 11/28/2006  O SOTTO   FIX FOR PROD PROBLEM. SC 112806.
* 11/30/2006  O SOTTO   FIX FOR PROD PROBLEM. SC 113006.
* 12/21/2006  O SOTTO   RECOMPILED FOR BENG200 CHANGE.
* 04/01/2017  DURAND    PIN EXPANSION N7 TO N12
************************************************************************

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benp225 extends BLNatBase
{
    // Data Areas
    private GdaBeng200 gdaBeng200;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_11;
    private DbsField pnd_Addr_Srce;
    private DbsField pnd_Ba_Lo;

    private DbsGroup pnd_Break_G;
    private DbsField pnd_Break_G_Pnd_Break;

    private DbsGroup pnd_Break_G__R_Field_1;
    private DbsField pnd_Break_G_Ba_Pin;
    private DbsField pnd_Break_G_Ba_Rqst_Timestamp;
    private DbsField pnd_Cntrct;
    private DbsField pnd_Cnt;
    private DbsField pnd_Date_Comp_A8;

    private DbsGroup pnd_Date_Comp_A8__R_Field_2;
    private DbsField pnd_Date_Comp_A8_Pnd_Date_Comp_N8;
    private DbsField pnd_Initial;
    private DbsField pnd_Isn;
    private DbsField pnd_Lo;
    private DbsField pnd_Lo_Key;
    private DbsField pnd_Lo_Text;
    private DbsField pnd_Lttr_Optn;
    private DbsField pnd_Partial;
    private DbsField pnd_Racf;
    private DbsField pnd_Text;
    private DbsField pnd_Web;
    private DbsField pnd_Wpid_I;
    private DbsField pnd_Wpid_X;
    private DbsField pnd_Header;

    private DbsGroup pnd_Form;

    private DbsGroup pnd_Form_Pnd_Lines;
    private DbsField pnd_Form_Pnd_Desc;

    private DbsGroup pnd_Form__R_Field_3;
    private DbsField pnd_Form_Pnd_Wpid;
    private DbsField pnd_Form_Pnd_Sum;
    private DbsField pnd_Form_Pnd_Zero;
    private DbsField pnd_Form_Pnd_Cv_Zero;

    private DbsRecord internalLoopRecord;
    private DbsField pRINTBa_WpidOld;
    private DbsField pRINTPnd_Wpid_IOld;
    private DbsField pRINTBa_Eff_DteOld;
    private DbsField pRINTBa_Addr_SrceOld;
    private DbsField pRINTBa_Partial_Data_IndOld;
    private DbsField pRINTBa_NameOld;
    private DbsField pRINTBa_Owner_NameOld;
    private DbsField pRINTBa_PinOld;
    private DbsField pRINTBa_Lttr_InfoOld;
    private DbsField pRINTPnd_RacfOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaBeng200 = GdaBeng200.getInstance(getCallnatLevel());
        registerRecord(gdaBeng200);
        if (gdaOnly) return;

        // Local Variables
        localVariables = new DbsRecord();
        pnd_11 = localVariables.newFieldInRecord("pnd_11", "#11", FieldType.PACKED_DECIMAL, 3);
        pnd_Addr_Srce = localVariables.newFieldInRecord("pnd_Addr_Srce", "#ADDR-SRCE", FieldType.STRING, 6);
        pnd_Ba_Lo = localVariables.newFieldInRecord("pnd_Ba_Lo", "#BA-LO", FieldType.STRING, 1);

        pnd_Break_G = localVariables.newGroupInRecord("pnd_Break_G", "#BREAK-G");
        pnd_Break_G_Pnd_Break = pnd_Break_G.newFieldInGroup("pnd_Break_G_Pnd_Break", "#BREAK", FieldType.STRING, 27);

        pnd_Break_G__R_Field_1 = localVariables.newGroupInRecord("pnd_Break_G__R_Field_1", "REDEFINE", pnd_Break_G);
        pnd_Break_G_Ba_Pin = pnd_Break_G__R_Field_1.newFieldInGroup("pnd_Break_G_Ba_Pin", "BA-PIN", FieldType.NUMERIC, 12);
        pnd_Break_G_Ba_Rqst_Timestamp = pnd_Break_G__R_Field_1.newFieldInGroup("pnd_Break_G_Ba_Rqst_Timestamp", "BA-RQST-TIMESTAMP", FieldType.STRING, 
            15);
        pnd_Cntrct = localVariables.newFieldArrayInRecord("pnd_Cntrct", "#CNTRCT", FieldType.STRING, 10, new DbsArrayController(1, 100));
        pnd_Cnt = localVariables.newFieldInRecord("pnd_Cnt", "#CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_Date_Comp_A8 = localVariables.newFieldInRecord("pnd_Date_Comp_A8", "#DATE-COMP-A8", FieldType.STRING, 8);

        pnd_Date_Comp_A8__R_Field_2 = localVariables.newGroupInRecord("pnd_Date_Comp_A8__R_Field_2", "REDEFINE", pnd_Date_Comp_A8);
        pnd_Date_Comp_A8_Pnd_Date_Comp_N8 = pnd_Date_Comp_A8__R_Field_2.newFieldInGroup("pnd_Date_Comp_A8_Pnd_Date_Comp_N8", "#DATE-COMP-N8", FieldType.NUMERIC, 
            8);
        pnd_Initial = localVariables.newFieldInRecord("pnd_Initial", "#INITIAL", FieldType.STRING, 1);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Lo = localVariables.newFieldInRecord("pnd_Lo", "#LO", FieldType.PACKED_DECIMAL, 3);
        pnd_Lo_Key = localVariables.newFieldArrayInRecord("pnd_Lo_Key", "#LO-KEY", FieldType.STRING, 1, new DbsArrayController(1, 50));
        pnd_Lo_Text = localVariables.newFieldArrayInRecord("pnd_Lo_Text", "#LO-TEXT", FieldType.STRING, 12, new DbsArrayController(1, 50));
        pnd_Lttr_Optn = localVariables.newFieldInRecord("pnd_Lttr_Optn", "#LTTR-OPTN", FieldType.STRING, 12);
        pnd_Partial = localVariables.newFieldInRecord("pnd_Partial", "#PARTIAL", FieldType.STRING, 7);
        pnd_Racf = localVariables.newFieldArrayInRecord("pnd_Racf", "#RACF", FieldType.STRING, 8, new DbsArrayController(1, 2));
        pnd_Text = localVariables.newFieldInRecord("pnd_Text", "#TEXT", FieldType.STRING, 20);
        pnd_Web = localVariables.newFieldArrayInRecord("pnd_Web", "#WEB", FieldType.STRING, 1, new DbsArrayController(1, 14));
        pnd_Wpid_I = localVariables.newFieldInRecord("pnd_Wpid_I", "#WPID-I", FieldType.PACKED_DECIMAL, 3);
        pnd_Wpid_X = localVariables.newFieldInRecord("pnd_Wpid_X", "#WPID-X", FieldType.STRING, 8);
        pnd_Header = localVariables.newFieldInRecord("pnd_Header", "#HEADER", FieldType.STRING, 75);

        pnd_Form = localVariables.newGroupInRecord("pnd_Form", "#FORM");

        pnd_Form_Pnd_Lines = pnd_Form.newGroupArrayInGroup("pnd_Form_Pnd_Lines", "#LINES", new DbsArrayController(1, 14));
        pnd_Form_Pnd_Desc = pnd_Form_Pnd_Lines.newFieldInGroup("pnd_Form_Pnd_Desc", "#DESC", FieldType.STRING, 33);

        pnd_Form__R_Field_3 = pnd_Form_Pnd_Lines.newGroupInGroup("pnd_Form__R_Field_3", "REDEFINE", pnd_Form_Pnd_Desc);
        pnd_Form_Pnd_Wpid = pnd_Form__R_Field_3.newFieldInGroup("pnd_Form_Pnd_Wpid", "#WPID", FieldType.STRING, 8);
        pnd_Form_Pnd_Sum = pnd_Form_Pnd_Lines.newFieldInGroup("pnd_Form_Pnd_Sum", "#SUM", FieldType.NUMERIC, 9);
        pnd_Form_Pnd_Zero = pnd_Form.newFieldInGroup("pnd_Form_Pnd_Zero", "#ZERO", FieldType.STRING, 68);
        pnd_Form_Pnd_Cv_Zero = pnd_Form.newFieldInGroup("pnd_Form_Pnd_Cv_Zero", "#CV-ZERO", FieldType.ATTRIBUTE_CONTROL, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        pRINTBa_WpidOld = internalLoopRecord.newFieldInRecord("PRINT_Ba_Wpid_OLD", "Ba_Wpid_OLD", FieldType.STRING, 6);
        pRINTPnd_Wpid_IOld = internalLoopRecord.newFieldInRecord("PRINT_Pnd_Wpid_I_OLD", "Pnd_Wpid_I_OLD", FieldType.PACKED_DECIMAL, 3);
        pRINTBa_Eff_DteOld = internalLoopRecord.newFieldInRecord("PRINT_Ba_Eff_Dte_OLD", "Ba_Eff_Dte_OLD", FieldType.STRING, 8);
        pRINTBa_Addr_SrceOld = internalLoopRecord.newFieldInRecord("PRINT_Ba_Addr_Srce_OLD", "Ba_Addr_Srce_OLD", FieldType.STRING, 1);
        pRINTBa_Partial_Data_IndOld = internalLoopRecord.newFieldInRecord("PRINT_Ba_Partial_Data_Ind_OLD", "Ba_Partial_Data_Ind_OLD", FieldType.STRING, 
            1);
        pRINTBa_NameOld = internalLoopRecord.newFieldInRecord("PRINT_Ba_Name_OLD", "Ba_Name_OLD", FieldType.STRING, 35);
        pRINTBa_Owner_NameOld = internalLoopRecord.newFieldInRecord("PRINT_Ba_Owner_Name_OLD", "Ba_Owner_Name_OLD", FieldType.STRING, 35);
        pRINTBa_PinOld = internalLoopRecord.newFieldInRecord("PRINT_Ba_Pin_OLD", "Ba_Pin_OLD", FieldType.NUMERIC, 12);
        pRINTBa_Lttr_InfoOld = internalLoopRecord.newFieldInRecord("PRINT_Ba_Lttr_Info_OLD", "Ba_Lttr_Info_OLD", FieldType.STRING, 10);
        pRINTPnd_RacfOld = internalLoopRecord.newFieldArrayInRecord("PRINT_Pnd_Racf_OLD", "Pnd_Racf_OLD", FieldType.STRING, 8, new DbsArrayController(1, 
            2));
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();

        localVariables.reset();
        pnd_11.setInitialValue(14);
        pnd_Partial.setInitialValue("Partial");
        pnd_Header.setInitialValue("CONFIRMATION/DESIGNATION STATEMENT REGISTER FROM YYYYYYYYYY THRU XXXXXXXXXX");
        pnd_Form_Pnd_Desc.getValue(1).setInitialValue(" T O T A L");
        pnd_Form_Pnd_Zero.setInitialValue("No activity was recorded for the date range of this report");
        pnd_Form_Pnd_Cv_Zero.setInitialAttributeValue("AD=NP");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Benp225() throws Exception
    {
        super("Benp225");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 57 SG = OFF ZP = OFF
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
                                                                                                                                                                          //Natural: PERFORM START-UP
        sub_Start_Up();
        if (condition(Global.isEscape())) {return;}
        gdaBeng200.getVw_act().startDatabaseRead                                                                                                                          //Natural: READ ACT
        (
        "RBA",
        new Oc[] { new Oc("ISN", "ASC") }
        );
        RBA:
        while (condition(gdaBeng200.getVw_act().readNextRow("RBA")))
        {
            //*  IE. UNCONVERTED PRE-UNIT
            //*  CHANGE RECORDS
            if (condition(gdaBeng200.getAct_Ba_Wpid_Unit().equals(" ")))                                                                                                  //Natural: IF ACT.BA-WPID-UNIT = ' '
            {
                gdaBeng200.getAct_Ba_Wpid_Unit().setValue("CTADME");                                                                                                      //Natural: ASSIGN ACT.BA-WPID-UNIT := 'CTADME'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(! (gdaBeng200.getAct_Ba_Cnfrm_Stmnt_Prod_Dte().greaterOrEqual(gdaBeng200.getPnd_Date_Y()) && gdaBeng200.getAct_Ba_Cnfrm_Stmnt_Prod_Dte().lessOrEqual(gdaBeng200.getPnd_Date_X())))) //Natural: IF NOT ACT.BA-CNFRM-STMNT-PROD-DTE = #DATE-Y THRU #DATE-X
            {
                //*  ONLY PRODUCE THOSE PROCESSED ON BUS-DAY
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Isn.setValue(gdaBeng200.getVw_act().getAstISN("RBA"));                                                                                                    //Natural: ASSIGN #ISN := *ISN ( RBA. )
            pnd_Wpid_X.setValue(gdaBeng200.getAct_Ba_Wpid());                                                                                                             //Natural: ASSIGN #WPID-X := ACT.BA-WPID
            //*  EXAMINE #WPID (*) FOR #WPID-X INDEX #WPID-I
            DbsUtil.examine(new ExamineSource(pnd_Form_Pnd_Desc.getValue("*")), new ExamineSearch(pnd_Wpid_X), new ExamineGivingIndex(pnd_Wpid_I));                       //Natural: EXAMINE #DESC ( * ) FOR #WPID-X INDEX #WPID-I
            if (condition(pnd_Wpid_I.equals(getZero())))                                                                                                                  //Natural: IF #WPID-I = 0
            {
                getReports().write(0, "****** TERRIBLE PROBLEM IN",Global.getPROGRAM(),"- WPID OF:",pnd_Wpid_X,"ON ACTIVITY ISN:",pnd_Isn);                               //Natural: WRITE '****** TERRIBLE PROBLEM IN' *PROGRAM '- WPID OF:' #WPID-X 'ON ACTIVITY ISN:' #ISN
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RBA"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RBA"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(1);  if (true) return;                                                                                                                  //Natural: TERMINATE 01
            }                                                                                                                                                             //Natural: END-IF
            pnd_Racf.getValue(1).setValue(gdaBeng200.getAct_Ba_Orig_Updt_Userid());                                                                                       //Natural: ASSIGN #RACF ( 1 ) := ACT.BA-ORIG-UPDT-USERID
            pnd_Racf.getValue(2).setValue(gdaBeng200.getAct_Ba_Vrfy_Userid());                                                                                            //Natural: ASSIGN #RACF ( 2 ) := ACT.BA-VRFY-USERID
            pnd_Initial.setValue(gdaBeng200.getAct_Ba_Name());                                                                                                            //Natural: ASSIGN #INITIAL := ACT.BA-NAME
            getWorkFiles().write(1, false, gdaBeng200.getVw_act(), pnd_Racf.getValue("*"), pnd_Initial, pnd_Isn, pnd_Wpid_I);                                             //Natural: WRITE WORK FILE 1 ACT #RACF ( * ) #INITIAL #ISN #WPID-I
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 ACT #RACF ( * ) #INITIAL #ISN #WPID-I
        while (condition(getWorkFiles().read(1, gdaBeng200.getVw_act(), pnd_Racf.getValue("*"), pnd_Initial, pnd_Isn, pnd_Wpid_I)))
        {
            getSort().writeSortInData(gdaBeng200.getAct_Ba_Wpid_Unit(), gdaBeng200.getAct_Ba_Ph_Last_Nme(), pnd_Initial, gdaBeng200.getAct_Ba_Pin(), gdaBeng200.getAct_Ba_Rqst_Timestamp(),  //Natural: END-ALL
                gdaBeng200.getAct_Ba_Seq_Nmbr(), gdaBeng200.getAct_Ba_Name(), gdaBeng200.getAct_Ba_Owner_Name(), gdaBeng200.getAct_Ba_Wpid(), pnd_Wpid_I, 
                gdaBeng200.getAct_Ba_Lttr_Optn(), gdaBeng200.getAct_Ba_Lttr_Info(), gdaBeng200.getAct_Ba_Addr_Srce(), gdaBeng200.getAct_Ba_Eff_Dte(), gdaBeng200.getAct_Ba_Partial_Data_Ind(), 
                pnd_Racf.getValue(1), pnd_Racf.getValue(2), gdaBeng200.getAct_Count_Castba_Slctd_Cntrct_Group(), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(1), 
                gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(2), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(3), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(4), 
                gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(5), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(6), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(7), 
                gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(8), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(9), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(10), 
                gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(11), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(12), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(13), 
                gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(14), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(15), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(16), 
                gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(17), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(18), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(19), 
                gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(20), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(1), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(2), 
                gdaBeng200.getAct_Ba_Slctd_Cref().getValue(3), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(4), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(5), 
                gdaBeng200.getAct_Ba_Slctd_Cref().getValue(6), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(7), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(8), 
                gdaBeng200.getAct_Ba_Slctd_Cref().getValue(9), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(10), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(11), 
                gdaBeng200.getAct_Ba_Slctd_Cref().getValue(12), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(13), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(14), 
                gdaBeng200.getAct_Ba_Slctd_Cref().getValue(15), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(16), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(17), 
                gdaBeng200.getAct_Ba_Slctd_Cref().getValue(18), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(19), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(20), 
                pnd_Isn);
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getSort().sortData(gdaBeng200.getAct_Ba_Wpid_Unit(), gdaBeng200.getAct_Ba_Ph_Last_Nme(), pnd_Initial, gdaBeng200.getAct_Ba_Pin(), gdaBeng200.getAct_Ba_Rqst_Timestamp(),  //Natural: SORT BY ACT.BA-WPID-UNIT ACT.BA-PH-LAST-NME #INITIAL ACT.BA-PIN ACT.BA-RQST-TIMESTAMP ACT.BA-SEQ-NMBR USING ACT.BA-NAME ACT.BA-OWNER-NAME ACT.BA-WPID #WPID-I ACT.BA-LTTR-OPTN ACT.BA-LTTR-INFO ACT.BA-ADDR-SRCE ACT.BA-EFF-DTE ACT.BA-PARTIAL-DATA-IND #RACF ( * ) ACT.C*BA-SLCTD-CNTRCT-GROUP ACT.BA-SLCTD-TIAA ( * ) ACT.BA-SLCTD-CREF ( * ) #ISN
            gdaBeng200.getAct_Ba_Seq_Nmbr());
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(gdaBeng200.getAct_Ba_Wpid_Unit(), gdaBeng200.getAct_Ba_Ph_Last_Nme(), pnd_Initial, gdaBeng200.getAct_Ba_Pin(), 
            gdaBeng200.getAct_Ba_Rqst_Timestamp(), gdaBeng200.getAct_Ba_Seq_Nmbr(), gdaBeng200.getAct_Ba_Name(), gdaBeng200.getAct_Ba_Owner_Name(), gdaBeng200.getAct_Ba_Wpid(), 
            pnd_Wpid_I, gdaBeng200.getAct_Ba_Lttr_Optn(), gdaBeng200.getAct_Ba_Lttr_Info(), gdaBeng200.getAct_Ba_Addr_Srce(), gdaBeng200.getAct_Ba_Eff_Dte(), 
            gdaBeng200.getAct_Ba_Partial_Data_Ind(), pnd_Racf.getValue(1), pnd_Racf.getValue(2), gdaBeng200.getAct_Count_Castba_Slctd_Cntrct_Group(), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(1), 
            gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(2), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(3), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(4), 
            gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(5), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(6), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(7), 
            gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(8), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(9), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(10), 
            gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(11), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(12), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(13), 
            gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(14), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(15), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(16), 
            gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(17), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(18), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(19), 
            gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(20), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(1), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(2), 
            gdaBeng200.getAct_Ba_Slctd_Cref().getValue(3), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(4), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(5), 
            gdaBeng200.getAct_Ba_Slctd_Cref().getValue(6), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(7), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(8), 
            gdaBeng200.getAct_Ba_Slctd_Cref().getValue(9), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(10), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(11), 
            gdaBeng200.getAct_Ba_Slctd_Cref().getValue(12), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(13), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(14), 
            gdaBeng200.getAct_Ba_Slctd_Cref().getValue(15), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(16), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(17), 
            gdaBeng200.getAct_Ba_Slctd_Cref().getValue(18), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(19), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(20), 
            pnd_Isn)))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //BEFORE BREAK PROCESSING                                                                                                                                     //Natural: BEFORE BREAK PROCESSING
            pnd_Break_G.setValuesByName(gdaBeng200.getVw_act());                                                                                                          //Natural: MOVE BY NAME ACT TO #BREAK-G
            //END-BEFORE                                                                                                                                                  //Natural: END-BEFORE
            //*  AT START OF DATA
            //*    PERFORM BENS202-CHECK-WPID-UNIT
            //*  END-START
            getReports().write(0, gdaBeng200.getAct_Ba_Wpid(),pnd_Wpid_I,pnd_Isn);                                                                                        //Natural: WRITE ACT.BA-WPID #WPID-I #ISN
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*                                                                                                                                                           //Natural: AT BREAK OF #BREAK
            //*                                                                                                                                                           //Natural: AT BREAK OF ACT.BA-WPID-UNIT
            //*  IE, WRITE SECONDARIES TO SAME #CNTRCT
            if (condition(gdaBeng200.getAct_Ba_Seq_Nmbr().equals(1)))                                                                                                     //Natural: IF ACT.BA-SEQ-NMBR = 1
            {
                pnd_Cntrct.getValue("*").reset();                                                                                                                         //Natural: RESET #CNTRCT ( * ) #CNT
                pnd_Cnt.reset();
            }                                                                                                                                                             //Natural: END-IF
            FOR02:                                                                                                                                                        //Natural: FOR #I = 1 TO ACT.C*BA-SLCTD-CNTRCT-GROUP
            for (gdaBeng200.getPnd_I().setValue(1); condition(gdaBeng200.getPnd_I().lessOrEqual(gdaBeng200.getAct_Count_Castba_Slctd_Cntrct_Group())); 
                gdaBeng200.getPnd_I().nadd(1))
            {
                if (condition(! (gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(gdaBeng200.getPnd_I()).equals(" "))))                                                         //Natural: IF NOT ACT.BA-SLCTD-TIAA ( #I ) = ' '
                {
                    pnd_Cnt.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #CNT
                    pnd_Cntrct.getValue(pnd_Cnt).setValue(gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(gdaBeng200.getPnd_I()));                                             //Natural: ASSIGN #CNTRCT ( #CNT ) := ACT.BA-SLCTD-TIAA ( #I )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(! (gdaBeng200.getAct_Ba_Slctd_Cref().getValue(gdaBeng200.getPnd_I()).equals(" "))))                                                         //Natural: IF NOT ACT.BA-SLCTD-CREF ( #I ) = ' '
                {
                    pnd_Cnt.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #CNT
                    pnd_Cntrct.getValue(pnd_Cnt).setValue(gdaBeng200.getAct_Ba_Slctd_Cref().getValue(gdaBeng200.getPnd_I()));                                             //Natural: ASSIGN #CNTRCT ( #CNT ) := ACT.BA-SLCTD-CREF ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-SORT
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        endSort();
                                                                                                                                                                          //Natural: PERFORM UPDATE-BENE-CONTROL
        sub_Update_Bene_Control();
        if (condition(Global.isEscape())) {return;}
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: START-UP
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-BENE-CONTROL
    }
    private void sub_Start_Up() throws Exception                                                                                                                          //Natural: START-UP
    {
        if (BLNatReinput.isReinput()) return;

        gdaBeng200.getPnd_Pnd_Control_I().setValue(gdaBeng200.getPnd_Pnd_Report2());                                                                                      //Natural: ASSIGN ##CONTROL-I := ##REPORT2
        gdaBeng200.getPnd_Pnd_Program().setValue(Global.getPROGRAM());                                                                                                    //Natural: ASSIGN ##PROGRAM := *PROGRAM
        gdaBeng200.getPnd_Datx().setValue(Global.getDATX());                                                                                                              //Natural: ASSIGN #DATX := *DATX
        gdaBeng200.getPnd_Date_X().setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                       //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO #DATE-X
        gdaBeng200.getPnd_Date_Xd().setValueEdited(Global.getDATX(),new ReportEditMask("MM/DD/YYYY"));                                                                    //Natural: MOVE EDITED *DATX ( EM = MM/DD/YYYY ) TO #DATE-XD
        //*  LAST BUSI DATE RUN
        gdaBeng200.getVw_control().startDatabaseRead                                                                                                                      //Natural: READ ( 1 ) CONTROL BY BX-BUSINESS-DTE-COMP
        (
        "READ02",
        new Oc[] { new Oc("BX_BUSINESS_DTE_COMP", "ASC") },
        1
        );
        READ02:
        while (condition(gdaBeng200.getVw_control().readNextRow("READ02")))
        {
            if (condition(gdaBeng200.getControl_Bx_Business_Dte().greaterOrEqual(gdaBeng200.getPnd_Date_X())))                                                            //Natural: IF CONTROL.BX-BUSINESS-DTE >= #DATE-X
            {
                gdaBeng200.getPnd_Datx().nsubtract(1);                                                                                                                    //Natural: SUBTRACT 1 FROM #DATX
                gdaBeng200.getPnd_Date_Y().setValueEdited(gdaBeng200.getPnd_Datx(),new ReportEditMask("YYYYMMDD"));                                                       //Natural: MOVE EDITED #DATX ( EM = YYYYMMDD ) TO #DATE-Y
                gdaBeng200.getPnd_Date_Yd().setValueEdited(gdaBeng200.getPnd_Datx(),new ReportEditMask("MM/DD/YYYY"));                                                    //Natural: MOVE EDITED #DATX ( EM = MM/DD/YYYY ) TO #DATE-YD
                //*  SHOULD BE LAST BATCH DATE RUN
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                gdaBeng200.getPnd_Date_Y().setValue(gdaBeng200.getControl_Bx_Business_Dte());                                                                             //Natural: MOVE CONTROL.BX-BUSINESS-DTE TO #DATE-Y
                gdaBeng200.getPnd_Datx().setValueEdited(new ReportEditMask("YYYYMMDD"),gdaBeng200.getPnd_Date_Y());                                                       //Natural: MOVE EDITED #DATE-Y TO #DATX ( EM = YYYYMMDD )
                gdaBeng200.getPnd_Datx().nadd(1);                                                                                                                         //Natural: ADD 1 TO #DATX
                gdaBeng200.getPnd_Date_Y().setValueEdited(gdaBeng200.getPnd_Datx(),new ReportEditMask("YYYYMMDD"));                                                       //Natural: MOVE EDITED #DATX ( EM = YYYYMMDD ) TO #DATE-Y
                gdaBeng200.getPnd_Date_Yd().setValueEdited(gdaBeng200.getPnd_Datx(),new ReportEditMask("MM'/'DD'/'YYYY"));                                                //Natural: MOVE EDITED #DATX ( EM = MM'/'DD'/'YYYY ) TO #DATE-YD
            }                                                                                                                                                             //Natural: END-IF
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(0, "Bene-Control:",gdaBeng200.getPnd_Date_Yd(),"thru",gdaBeng200.getPnd_Date_Xd());                                                            //Natural: WRITE 'Bene-Control:' #DATE-YD 'thru' #DATE-XD
        if (Global.isEscape()) return;
        //*  PERFORM BENS200-START-UP             /* REMOVED BENE-CONTROL
        pnd_Form_Pnd_Desc.getValue(1,":",13).setValue(gdaBeng200.getPnd_Wpid_And_Descriptions_Pnd_Pnd_Desc().getValue(1,":",13));                                         //Natural: MOVE ##DESC ( 1:13 ) TO #DESC ( 1:13 )
        DbsUtil.examine(new ExamineSource(pnd_Header), new ExamineSearch("YYYYYYYYYY"), new ExamineReplace(gdaBeng200.getPnd_Date_Yd()));                                 //Natural: EXAMINE #HEADER FOR 'YYYYYYYYYY' REPLACE #DATE-YD
        DbsUtil.examine(new ExamineSource(pnd_Header), new ExamineSearch("XXXXXXXXXX"), new ExamineReplace(gdaBeng200.getPnd_Date_Xd()));                                 //Natural: EXAMINE #HEADER FOR 'XXXXXXXXXX' REPLACE #DATE-XD
        FOR03:                                                                                                                                                            //Natural: FOR #I = 1 TO ##WU-TOT
        for (gdaBeng200.getPnd_I().setValue(1); condition(gdaBeng200.getPnd_I().lessOrEqual(gdaBeng200.getPnd_Pnd_Wu_Tot())); gdaBeng200.getPnd_I().nadd(1))
        {
            gdaBeng200.getAct_Ba_Wpid_Unit().setValue(gdaBeng200.getPnd_Pnd_Wpid_Unit().getValue(gdaBeng200.getPnd_I()));                                                 //Natural: MOVE ##WPID-UNIT ( #I ) TO ACT.BA-WPID-UNIT
            getWorkFiles().write(1, false, gdaBeng200.getVw_act(), pnd_Racf.getValue("*"), pnd_Initial, pnd_Isn, pnd_Wpid_I);                                             //Natural: WRITE WORK FILE 1 ACT #RACF ( * ) #INITIAL #ISN #WPID-I
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Lo.reset();                                                                                                                                                   //Natural: RESET #LO #LO-KEY ( * ) #LO-TEXT ( * )
        pnd_Lo_Key.getValue("*").reset();
        pnd_Lo_Text.getValue("*").reset();
        gdaBeng200.getVw_table().startDatabaseRead                                                                                                                        //Natural: READ TABLE BY BT-TABLE-ID-KEY STARTING FROM 'LO'
        (
        "RBT",
        new Wc[] { new Wc("BT_TABLE_ID_KEY", ">=", "LO", WcType.BY) },
        new Oc[] { new Oc("BT_TABLE_ID_KEY", "ASC") }
        );
        RBT:
        while (condition(gdaBeng200.getVw_table().readNextRow("RBT")))
        {
            if (condition(gdaBeng200.getTable_Bt_Table_Id().notEquals("LO")))                                                                                             //Natural: IF TABLE.BT-TABLE-ID NE 'LO'
            {
                if (true) break RBT;                                                                                                                                      //Natural: ESCAPE BOTTOM ( RBT. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Lo.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #LO
            pnd_Lo_Key.getValue(pnd_Lo).setValue(gdaBeng200.getTable_Bt_Table_Key(), MoveOption.LeftJustified);                                                           //Natural: MOVE LEFT TABLE.BT-TABLE-KEY TO #LO-KEY ( #LO )
            pnd_Lo_Text.getValue(pnd_Lo).setValue(gdaBeng200.getTable_Bt_Table_Text(), MoveOption.LeftJustified);                                                         //Natural: MOVE LEFT TABLE.BT-TABLE-TEXT TO #LO-TEXT ( #LO )
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  START-UP
    }
    private void sub_Update_Bene_Control() throws Exception                                                                                                               //Natural: UPDATE-BENE-CONTROL
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Date_Comp_A8_Pnd_Date_Comp_N8.compute(new ComputeParameters(false, pnd_Date_Comp_A8_Pnd_Date_Comp_N8), DbsField.subtract(99999999,Global.getDATN()));         //Natural: ASSIGN #DATE-COMP-N8 := 99999999 - *DATN
        gdaBeng200.getControl_Bx_Business_Dte().setValue(gdaBeng200.getPnd_Date_X());                                                                                     //Natural: ASSIGN CONTROL.BX-BUSINESS-DTE := #DATE-X
        gdaBeng200.getControl_Bx_Business_Dte_Comp().setValue(pnd_Date_Comp_A8);                                                                                          //Natural: ASSIGN CONTROL.BX-BUSINESS-DTE-COMP := #DATE-COMP-A8
        gdaBeng200.getControl_Bx_Dsgntn_Cmpltd_Stat().setValue("Y");                                                                                                      //Natural: ASSIGN CONTROL.BX-DSGNTN-CMPLTD-STAT := 'Y'
        gdaBeng200.getControl_Bx_Cnfrm_Stmnt_Reg_Stat().setValue("Y");                                                                                                    //Natural: ASSIGN CONTROL.BX-CNFRM-STMNT-REG-STAT := 'Y'
        gdaBeng200.getControl_Bx_Cnfrm_Stmnt_Cntl_Rpt_Stat().setValue("Y");                                                                                               //Natural: ASSIGN CONTROL.BX-CNFRM-STMNT-CNTL-RPT-STAT := 'Y'
        gdaBeng200.getControl_Bx_Dly_Actvty_Cntl_Rpt_Stat().setValue("Y");                                                                                                //Natural: ASSIGN CONTROL.BX-DLY-ACTVTY-CNTL-RPT-STAT := 'Y'
        gdaBeng200.getControl_Bx_Rcrd_Last_Updt_Dte().setValue(gdaBeng200.getPnd_Date_X());                                                                               //Natural: ASSIGN CONTROL.BX-RCRD-LAST-UPDT-DTE := #DATE-X
        gdaBeng200.getControl_Bx_Rcrd_Last_Updt_Userid().setValue(Global.getPROGRAM());                                                                                   //Natural: ASSIGN CONTROL.BX-RCRD-LAST-UPDT-USERID := *PROGRAM
        gdaBeng200.getControl_Bx_Rcrd_Last_Updt_Tme().setValueEdited(Global.getTIMX(),new ReportEditMask("HHIISST"));                                                     //Natural: MOVE EDITED *TIMX ( EM = HHIISST ) TO CONTROL.BX-RCRD-LAST-UPDT-TME
        gdaBeng200.getVw_control().insertDBRow();                                                                                                                         //Natural: STORE CONTROL
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  UPDATE-BENE-CONTROL
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Benm225.class));                                                                   //Natural: WRITE ( 1 ) NOTITLE USING FORM 'BENM225'
                    getReports().skip(1, 3);                                                                                                                              //Natural: SKIP ( 1 ) 3
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean pnd_Break_G_Pnd_BreakIsBreak = pnd_Break_G_Pnd_Break.isBreak(endOfData);
        boolean gdaBeng200_getAct_Ba_Wpid_UnitIsBreak = gdaBeng200.getAct_Ba_Wpid_Unit().isBreak(endOfData);
        if (condition(pnd_Break_G_Pnd_BreakIsBreak || gdaBeng200_getAct_Ba_Wpid_UnitIsBreak))
        {
            PRINT:                                                                                                                                                        //Natural: REPEAT
            while (condition(whileTrue))
            {
                pnd_Text.setValue(pRINTBa_WpidOld);                                                                                                                       //Natural: ASSIGN #TEXT := OLD ( ACT.BA-WPID )
                //*  IE, SPECIAL "UNIT"
                if (condition(pnd_Text.equals(" ")))                                                                                                                      //Natural: IF #TEXT = ' '
                {
                    //*  TO PRINT 'Empty' REPORTS
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM ( PRINT. )
                }                                                                                                                                                         //Natural: END-IF
                gdaBeng200.getPnd_I().setValue(pRINTPnd_Wpid_IOld);                                                                                                       //Natural: ASSIGN #I := OLD ( #WPID-I )
                pnd_Form_Pnd_Sum.getValue(gdaBeng200.getPnd_I()).nadd(1);                                                                                                 //Natural: ADD 1 TO #SUM ( #I )
                pnd_Form_Pnd_Sum.getValue(pnd_11).nadd(1);                                                                                                                //Natural: ADD 1 TO #SUM ( #11 )
                pnd_Text.setValue(pRINTBa_Eff_DteOld);                                                                                                                    //Natural: ASSIGN #TEXT := OLD ( ACT.BA-EFF-DTE )
                gdaBeng200.getPnd_Datx().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Text);                                                                         //Natural: MOVE EDITED #TEXT TO #DATX ( EM = YYYYMMDD )
                pnd_Ba_Lo.compute(new ComputeParameters(false, pnd_Ba_Lo), (gdaBeng200.getAct_Ba_Lttr_Optn()));                                                           //Natural: ASSIGN #BA-LO := ( ACT.BA-LTTR-OPTN )
                DbsUtil.examine(new ExamineSource(pnd_Lo_Key.getValue("*")), new ExamineSearch(pnd_Ba_Lo), new ExamineGivingIndex(pnd_Lo));                               //Natural: EXAMINE #LO-KEY ( * ) #BA-LO GIVING INDEX #LO
                if (condition(pnd_Lo.greater(getZero())))                                                                                                                 //Natural: IF #LO > 0
                {
                    pnd_Lttr_Optn.setValue(pnd_Lo_Text.getValue(pnd_Lo));                                                                                                 //Natural: ASSIGN #LTTR-OPTN := #LO-TEXT ( #LO )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Lttr_Optn.reset();                                                                                                                                //Natural: RESET #LTTR-OPTN
                }                                                                                                                                                         //Natural: END-IF
                pnd_Text.setValue(pRINTBa_Addr_SrceOld);                                                                                                                  //Natural: ASSIGN #TEXT := OLD ( ACT.BA-ADDR-SRCE )
                short decideConditionsMet301 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #TEXT;//Natural: VALUE '1'
                if (condition((pnd_Text.equals("1"))))
                {
                    decideConditionsMet301++;
                    pnd_Addr_Srce.setValue("NAAD");                                                                                                                       //Natural: ASSIGN #ADDR-SRCE := 'NAAD'
                }                                                                                                                                                         //Natural: VALUE '2'
                else if (condition((pnd_Text.equals("2"))))
                {
                    decideConditionsMet301++;
                    pnd_Addr_Srce.setValue("DE");                                                                                                                         //Natural: ASSIGN #ADDR-SRCE := 'DE'
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    pnd_Addr_Srce.reset();                                                                                                                                //Natural: RESET #ADDR-SRCE
                }                                                                                                                                                         //Natural: END-DECIDE
                pnd_Text.setValue(pRINTBa_Partial_Data_IndOld);                                                                                                           //Natural: ASSIGN #TEXT := OLD ( ACT.BA-PARTIAL-DATA-IND )
                short decideConditionsMet311 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #TEXT;//Natural: VALUE 'Y'
                if (condition((pnd_Text.equals("Y"))))
                {
                    decideConditionsMet311++;
                    pnd_Partial.resetInitial();                                                                                                                           //Natural: RESET INITIAL #PARTIAL
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    pnd_Partial.reset();                                                                                                                                  //Natural: RESET #PARTIAL
                }                                                                                                                                                         //Natural: END-DECIDE
                if (condition(getReports().getAstLinesLeft(1).less(pnd_Cnt)))                                                                                             //Natural: NEWPAGE ( 1 ) IF LESS THAN #CNT LINES LEFT
                {
                    getReports().newPage(1);
                    if (condition(Global.isEscape())){return;}
                }
                getReports().display(1, new ReportEmptyLineSuppression(true),"Participant",                                                                               //Natural: DISPLAY ( 1 ) ( ES = ON ) 'Participant' OLD ( ACT.BA-NAME ) / 'Owner' OLD ( ACT.BA-OWNER-NAME ) '/PIN' OLD ( ACT.BA-PIN ) 'Contract/Policy' #CNTRCT ( * ) '/WPID' OLD ( ACT.BA-WPID ) 'Fax or/Printer' OLD ( ACT.BA-LTTR-INFO ) 'Letter/Option' #LTTR-OPTN 'Addr/Source' #ADDR-SRCE 'Init RACF/Vrfy RACF' OLD ( #RACF ( * ) ) 'Effective/Date' #DATX ( EM = MM'/'DD'/'YYYY ) 'Partial/Data' #PARTIAL
                		pRINTBa_NameOld,NEWLINE,"Owner",
                		pRINTBa_Owner_NameOld,"/PIN",
                		pRINTBa_PinOld,"Contract/Policy",
                		pnd_Cntrct.getValue("*"),"/WPID",
                		pRINTBa_WpidOld,"Fax or/Printer",
                		pRINTBa_Lttr_InfoOld,"Letter/Option",
                		pnd_Lttr_Optn,"Addr/Source",
                		pnd_Addr_Srce,"Init RACF/Vrfy RACF",
                		pRINTPnd_RacfOld.getValue("*"),"Effective/Date",
                		gdaBeng200.getPnd_Datx(), new ReportEditMask ("MM'/'DD'/'YYYY"),"Partial/Data",
                		pnd_Partial);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PRINT"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PRINT"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().skip(1, 1);                                                                                                                                  //Natural: SKIP ( 1 ) 1
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM ( PRINT. )
                //*  (PRINT.)
            }                                                                                                                                                             //Natural: END-REPEAT
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(gdaBeng200_getAct_Ba_Wpid_UnitIsBreak))
        {
            //*  IE, NO NOTHING
            if (condition(pnd_Form_Pnd_Sum.getValue(pnd_11).equals(getZero())))                                                                                           //Natural: IF #SUM ( #11 ) = 0
            {
                pnd_Form_Pnd_Cv_Zero.reset();                                                                                                                             //Natural: RESET #CV-ZERO
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(45),"*** E N D   O F   R E P O R T ***");           //Natural: WRITE ( 1 ) ////// 45T '*** E N D   O F   R E P O R T ***'
            if (condition(Global.isEscape())) return;
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,NEWLINE,NEWLINE,new TabSetting(45),"T O T A L   B Y   W P I D",NEWLINE,new              //Natural: WRITE ( 1 ) NOTITLE NOHDR /// 45T 'T O T A L   B Y   W P I D' / 45T '-------------------------'
                TabSetting(45),"-------------------------");
            if (condition(Global.isEscape())) return;
            FOR01:                                                                                                                                                        //Natural: FOR #I = 1 TO #11
            for (gdaBeng200.getPnd_I().setValue(1); condition(gdaBeng200.getPnd_I().lessOrEqual(pnd_11)); gdaBeng200.getPnd_I().nadd(1))
            {
                //*      IF #WEB (#I) NE ' '
                //*        RESET #WPID(#I)
                //*      END-IF
                getReports().skip(1, 1);                                                                                                                                  //Natural: SKIP ( 1 ) 1
                getReports().display(1, new TabSetting(40),pnd_Form_Pnd_Desc.getValue(gdaBeng200.getPnd_I()),pnd_Form_Pnd_Sum.getValue(gdaBeng200.getPnd_I()));           //Natural: DISPLAY ( 1 ) 40T #DESC ( #I ) #SUM ( #I )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(gdaBeng200.getPnd_I().equals(3) || gdaBeng200.getPnd_I().equals(7) || gdaBeng200.getPnd_I().equals(10) || gdaBeng200.getPnd_I().equals(13))) //Natural: IF #I = 3 OR = 7 OR = 10 OR = 13
                {
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape())) return;
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),pnd_Form_Pnd_Zero, pnd_Form_Pnd_Cv_Zero);                                                       //Natural: WRITE ( 1 ) 25T #ZERO ( CV = #CV-ZERO )
            if (condition(Global.isEscape())) return;
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(45),"*** E N D   O F   R E P O R T ***");           //Natural: WRITE ( 1 ) ////// 45T '*** E N D   O F   R E P O R T ***'
            if (condition(Global.isEscape())) return;
            //*    PERFORM BENS202-CHECK-WPID-UNIT
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            pnd_Form.resetInitial();                                                                                                                                      //Natural: RESET INITIAL #FORM #SUM ( * )
            pnd_Form_Pnd_Sum.getValue("*").resetInitial();
            pnd_Form_Pnd_Desc.getValue(1,":",13).setValue(gdaBeng200.getPnd_Wpid_And_Descriptions_Pnd_Pnd_Desc().getValue(1,":",13));                                     //Natural: ASSIGN #DESC ( 1:13 ) := ##DESC ( 1:13 )
            pnd_Web.getValue(1,":",13).setValue(gdaBeng200.getPnd_Wpid_And_Descriptions_Pnd_Pnd_Web().getValue(1,":",13));                                                //Natural: ASSIGN #WEB ( 1:13 ) := ##WEB ( 1:13 )
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=57 SG=OFF ZP=OFF");

        getReports().setDisplayColumns(1, new ReportEmptyLineSuppression(true),"Participant",
        		pRINTBa_NameOld,NEWLINE,"Owner",
        		pRINTBa_Owner_NameOld,"/PIN",
        		pRINTBa_PinOld,"Contract/Policy",
        		pnd_Cntrct,"/WPID",
        		pRINTBa_WpidOld,"Fax or/Printer",
        		pRINTBa_Lttr_InfoOld,"Letter/Option",
        		pnd_Lttr_Optn,"Addr/Source",
        		pnd_Addr_Srce,"Init RACF/Vrfy RACF",
        		pRINTPnd_RacfOld,"Effective/Date",
        		gdaBeng200.getPnd_Datx(), new ReportEditMask ("MM'/'DD'/'YYYY"),"Partial/Data",
        		pnd_Partial);
    }
}
