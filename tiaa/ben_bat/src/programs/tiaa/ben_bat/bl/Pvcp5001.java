/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:21:35 PM
**        * FROM NATURAL PROGRAM : Pvcp5001
************************************************************
**        * FILE NAME            : Pvcp5001.java
**        * CLASS NAME           : Pvcp5001
**        * INSTANCE NAME        : Pvcp5001
************************************************************
************************************************************************
* PROGRAM     : PVCP5001  (CLONED FROM PVCN5000)
*             :
* SYSTEM      : SURVIVOR 4 BUNDLED LETTERS
*             :
* TITLE       : INFORM DRIVER - PTRKBBWI
*             :
* DATE        : AUG, 2009
*             :
* AUTHOR      : H. KAKADIA
*             :
*             :
**----------------------------------------------------------------------
*
* HISTORY
* ============================================================
* DATE       PGMR         DESC
* ---------- -----------  ---------------------------------------
* 01/19/2010 O. SOTTO     CHANGES FOR FOREIGN ADDRESS. ALSO
*                         PASS 1000000 IF PIN IS SPACES. SC 011910.
* 01/29/2010              RESET PSTA9612.
* 02/26/2010              PASS 'I' INSTEAD OF 'G' IN PI-EXPORT-IND.
* 04/08/2010              EXPAND #INPUT-GUID TO 47 BYTES.
*                         INCLUDE CHECK FOR TASK-ID OR GUID AND
*                         PASS APPROPRIATE EXPORT IND TO POST.
*                         SC 040810.
* 09/29/2010              ADDED FIELD TASKID AT THE END OF LAYOUT
* 02/24/2011              PROD FIX. GET PI-TASK-ID FROM #INPUT-GUID
*                         IF #INPUT-TASK-ID IS SPACES. SC 022411.
* 07/02/2013              IF PI GUID AND TASK ID MISSING ON OMNI,
*                         PASS PIN AND EXPORT IND 'T' TO POST. SEE
*                         DM 070213
* 12/30/2014              ADD LOGIC FROM PCVN5000 TO ASSIGN #INPUT-GUID
*                         VALUE TO PI-TASK-ID FIELD WHENEVER FIRST
*                         CHARACTER IS "T" OR "t". JAMIE CRUZ
*                         SEE: 20141230
* 08/14/2015              RE-STOW DUE TO NEW FIELDS IN PSTL0495
* 08/26/2015              ADD LETTER TYPE 'F6' AND 'F9' BIP PROJECT
* 11/17/2016 BUDDY NEWSOM OMNI PIN EXPANSION                  (PINEXP)
* 10/19/2017 BUDDY NEWSOM RESTOW TO USED CHANGE PSTL0495   (INC3952468) 
************************************************************************
**
*

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Pvcp5001 extends BLNatBase
{
    // Data Areas
    private PdaPvca5000 pdaPvca5000;
    private LdaPstl0495 ldaPstl0495;
    private PdaErla1000 pdaErla1000;
    private PdaPsta9500 pdaPsta9500;
    private PdaPsta9501 pdaPsta9501;
    private PdaPsta9610 pdaPsta9610;
    private PdaPsta9612 pdaPsta9612;
    private PdaPsta9513 pdaPsta9513;
    private PdaPsta9514 pdaPsta9514;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private PdaCwfpdaus pdaCwfpdaus;
    private PdaCwfpdaun pdaCwfpdaun;
    private PdaPdqa980 pdaPdqa980;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Response_Code;
    private DbsField pnd_Response_Text;
    private DbsField pnd_Appl_Data_Out;
    private DbsField pnd_Appl_Datalength_Out;
    private DbsField pnd_I;
    private DbsField pnd_Sungard_Ind;
    private DbsField pnd_Legacy_Ind;
    private DbsField pnd_Unique_Id;
    private DbsField pnd_Pvc_Key;
    private DbsField pnd_Response_Cde;
    private DbsField pnd_Response_Txt;
    private DbsField pnd_Input_Letter;

    private DbsGroup pnd_Input_Letter__R_Field_1;
    private DbsField pnd_Input_Letter_Pnd_Input_Letter_Iden;
    private DbsField pnd_Input_Letter_Pnd_Input_Letter_Type;
    private DbsField pnd_Input_Letter_Pnd_Input_Plan;
    private DbsField pnd_Input_Letter_Pnd_Input_Partid;
    private DbsField pnd_Input_Letter_Pnd_Input_Pin;

    private DbsGroup pnd_Input_Letter__R_Field_2;
    private DbsField pnd_Input_Letter_Pnd_Input_Pin_N;

    private DbsGroup pnd_Input_Letter__R_Field_3;
    private DbsField pnd_Input_Letter_Pnd_Input_Pin_A7;
    private DbsField pnd_Input_Letter_Pnd_Input_Pin_Filler;
    private DbsField pnd_Input_Letter_Pnd_Input_Dod;
    private DbsField pnd_Input_Letter_Pnd_Input_Decendent_Name;
    private DbsField pnd_Input_Letter_Pnd_Input_Bene_Name;

    private DbsGroup pnd_Input_Letter__R_Field_4;
    private DbsField pnd_Input_Letter_Pnd_Input_Bene_Name_R;
    private DbsField pnd_Input_Letter_Pnd_Input_Bene_Pin;

    private DbsGroup pnd_Input_Letter__R_Field_5;
    private DbsField pnd_Input_Letter_Pnd_Input_Bene_Pin_N;

    private DbsGroup pnd_Input_Letter__R_Field_6;
    private DbsField pnd_Input_Letter_Pnd_Input_Bene_Pin_N7;
    private DbsField pnd_Input_Letter_Pnd_Input_Bene_Pin_Filler;
    private DbsField pnd_Input_Letter_Pnd_Input_Bene_Care_Name;
    private DbsField pnd_Input_Letter_Pnd_Input_Bene_Addr1;
    private DbsField pnd_Input_Letter_Pnd_Input_Bene_Addr2;
    private DbsField pnd_Input_Letter_Pnd_Input_Bene_City;
    private DbsField pnd_Input_Letter_Pnd_Input_Bene_State;
    private DbsField pnd_Input_Letter_Pnd_Input_Bene_Zip;
    private DbsField pnd_Input_Letter_Pnd_Input_Bene_Country;
    private DbsField pnd_Input_Letter_Pnd_Input_Bene_Addr_Type;
    private DbsField pnd_Input_Letter_Pnd_Input_Bene_Letter_Type;
    private DbsField pnd_Input_Letter_Pnd_Input_Guid;
    private DbsField pnd_Input_Letter_Pnd_Input_Task_Type;
    private DbsField pnd_Input_Letter_Pnd_Input_Task_Id;

    private DbsGroup parm_Employee_Info;
    private DbsField parm_Employee_Info_Pnd_Pnd_Racf_Id;
    private DbsField parm_Employee_Info_Pnd_Pnd_Empl_Nme;

    private DbsGroup parm_Employee_Info__R_Field_7;
    private DbsField parm_Employee_Info_Pnd_Pnd_Empl_First_Nme;
    private DbsField parm_Employee_Info_Pnd_Pnd_Empl_Last_Nme;
    private DbsField parm_Employee_Info_Pnd_Pnd_Empl_Table_Updte_Authrty_Cde;
    private DbsField pnd_State_Name;

    private DbsGroup parm_Unit_Info;
    private DbsField parm_Unit_Info_Pnd_Pnd_Unit_Cde;

    private DbsGroup parm_Unit_Info__R_Field_8;
    private DbsField parm_Unit_Info_Pnd_Pnd_Unit_Id_Cde;
    private DbsField parm_Unit_Info_Pnd_Pnd_Unit_Rgn_Cde;
    private DbsField parm_Unit_Info_Pnd_Pnd_Unit_Spcl_Dsgntn_Cde;
    private DbsField parm_Unit_Info_Pnd_Pnd_Unit_Brnch_Cde;
    private DbsField parm_Unit_Info_Pnd_Pnd_Unit_Name_Long_Txt;
    private DbsField pnd_Save_Rqst_Id;
    private DbsField pnd_W_Bene_Name;
    private DbsField pnd_Asterisk;
    private DbsField pnd_A;
    private DbsField pnd_Max_Length;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaPvca5000 = new PdaPvca5000(localVariables);
        ldaPstl0495 = new LdaPstl0495();
        registerRecord(ldaPstl0495);
        pdaErla1000 = new PdaErla1000(localVariables);
        pdaPsta9500 = new PdaPsta9500(localVariables);
        pdaPsta9501 = new PdaPsta9501(localVariables);
        pdaPsta9610 = new PdaPsta9610(localVariables);
        pdaPsta9612 = new PdaPsta9612(localVariables);
        pdaPsta9513 = new PdaPsta9513(localVariables);
        pdaPsta9514 = new PdaPsta9514(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        pdaCwfpdaus = new PdaCwfpdaus(localVariables);
        pdaCwfpdaun = new PdaCwfpdaun(localVariables);
        pdaPdqa980 = new PdaPdqa980(localVariables);

        // Local Variables
        pnd_Response_Code = localVariables.newFieldInRecord("pnd_Response_Code", "#RESPONSE-CODE", FieldType.STRING, 4);
        pnd_Response_Text = localVariables.newFieldInRecord("pnd_Response_Text", "#RESPONSE-TEXT", FieldType.STRING, 100);
        pnd_Appl_Data_Out = localVariables.newFieldArrayInRecord("pnd_Appl_Data_Out", "#APPL-DATA-OUT", FieldType.STRING, 1, new DbsArrayController(1, 
            32360));
        pnd_Appl_Datalength_Out = localVariables.newFieldInRecord("pnd_Appl_Datalength_Out", "#APPL-DATALENGTH-OUT", FieldType.INTEGER, 4);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Sungard_Ind = localVariables.newFieldInRecord("pnd_Sungard_Ind", "#SUNGARD-IND", FieldType.STRING, 1);
        pnd_Legacy_Ind = localVariables.newFieldInRecord("pnd_Legacy_Ind", "#LEGACY-IND", FieldType.STRING, 1);
        pnd_Unique_Id = localVariables.newFieldInRecord("pnd_Unique_Id", "#UNIQUE-ID", FieldType.STRING, 45);
        pnd_Pvc_Key = localVariables.newFieldInRecord("pnd_Pvc_Key", "#PVC-KEY", FieldType.STRING, 45);
        pnd_Response_Cde = localVariables.newFieldInRecord("pnd_Response_Cde", "#RESPONSE-CDE", FieldType.STRING, 4);
        pnd_Response_Txt = localVariables.newFieldInRecord("pnd_Response_Txt", "#RESPONSE-TXT", FieldType.STRING, 100);
        pnd_Input_Letter = localVariables.newFieldArrayInRecord("pnd_Input_Letter", "#INPUT-LETTER", FieldType.STRING, 1, new DbsArrayController(1, 440));

        pnd_Input_Letter__R_Field_1 = localVariables.newGroupInRecord("pnd_Input_Letter__R_Field_1", "REDEFINE", pnd_Input_Letter);
        pnd_Input_Letter_Pnd_Input_Letter_Iden = pnd_Input_Letter__R_Field_1.newFieldInGroup("pnd_Input_Letter_Pnd_Input_Letter_Iden", "#INPUT-LETTER-IDEN", 
            FieldType.STRING, 4);
        pnd_Input_Letter_Pnd_Input_Letter_Type = pnd_Input_Letter__R_Field_1.newFieldInGroup("pnd_Input_Letter_Pnd_Input_Letter_Type", "#INPUT-LETTER-TYPE", 
            FieldType.STRING, 2);
        pnd_Input_Letter_Pnd_Input_Plan = pnd_Input_Letter__R_Field_1.newFieldInGroup("pnd_Input_Letter_Pnd_Input_Plan", "#INPUT-PLAN", FieldType.STRING, 
            6);
        pnd_Input_Letter_Pnd_Input_Partid = pnd_Input_Letter__R_Field_1.newFieldInGroup("pnd_Input_Letter_Pnd_Input_Partid", "#INPUT-PARTID", FieldType.STRING, 
            17);
        pnd_Input_Letter_Pnd_Input_Pin = pnd_Input_Letter__R_Field_1.newFieldInGroup("pnd_Input_Letter_Pnd_Input_Pin", "#INPUT-PIN", FieldType.STRING, 
            12);

        pnd_Input_Letter__R_Field_2 = pnd_Input_Letter__R_Field_1.newGroupInGroup("pnd_Input_Letter__R_Field_2", "REDEFINE", pnd_Input_Letter_Pnd_Input_Pin);
        pnd_Input_Letter_Pnd_Input_Pin_N = pnd_Input_Letter__R_Field_2.newFieldInGroup("pnd_Input_Letter_Pnd_Input_Pin_N", "#INPUT-PIN-N", FieldType.NUMERIC, 
            12);

        pnd_Input_Letter__R_Field_3 = pnd_Input_Letter__R_Field_1.newGroupInGroup("pnd_Input_Letter__R_Field_3", "REDEFINE", pnd_Input_Letter_Pnd_Input_Pin);
        pnd_Input_Letter_Pnd_Input_Pin_A7 = pnd_Input_Letter__R_Field_3.newFieldInGroup("pnd_Input_Letter_Pnd_Input_Pin_A7", "#INPUT-PIN-A7", FieldType.STRING, 
            7);
        pnd_Input_Letter_Pnd_Input_Pin_Filler = pnd_Input_Letter__R_Field_3.newFieldInGroup("pnd_Input_Letter_Pnd_Input_Pin_Filler", "#INPUT-PIN-FILLER", 
            FieldType.STRING, 5);
        pnd_Input_Letter_Pnd_Input_Dod = pnd_Input_Letter__R_Field_1.newFieldInGroup("pnd_Input_Letter_Pnd_Input_Dod", "#INPUT-DOD", FieldType.STRING, 
            8);
        pnd_Input_Letter_Pnd_Input_Decendent_Name = pnd_Input_Letter__R_Field_1.newFieldInGroup("pnd_Input_Letter_Pnd_Input_Decendent_Name", "#INPUT-DECENDENT-NAME", 
            FieldType.STRING, 40);
        pnd_Input_Letter_Pnd_Input_Bene_Name = pnd_Input_Letter__R_Field_1.newFieldInGroup("pnd_Input_Letter_Pnd_Input_Bene_Name", "#INPUT-BENE-NAME", 
            FieldType.STRING, 40);

        pnd_Input_Letter__R_Field_4 = pnd_Input_Letter__R_Field_1.newGroupInGroup("pnd_Input_Letter__R_Field_4", "REDEFINE", pnd_Input_Letter_Pnd_Input_Bene_Name);
        pnd_Input_Letter_Pnd_Input_Bene_Name_R = pnd_Input_Letter__R_Field_4.newFieldArrayInGroup("pnd_Input_Letter_Pnd_Input_Bene_Name_R", "#INPUT-BENE-NAME-R", 
            FieldType.STRING, 1, new DbsArrayController(1, 40));
        pnd_Input_Letter_Pnd_Input_Bene_Pin = pnd_Input_Letter__R_Field_1.newFieldInGroup("pnd_Input_Letter_Pnd_Input_Bene_Pin", "#INPUT-BENE-PIN", FieldType.STRING, 
            12);

        pnd_Input_Letter__R_Field_5 = pnd_Input_Letter__R_Field_1.newGroupInGroup("pnd_Input_Letter__R_Field_5", "REDEFINE", pnd_Input_Letter_Pnd_Input_Bene_Pin);
        pnd_Input_Letter_Pnd_Input_Bene_Pin_N = pnd_Input_Letter__R_Field_5.newFieldInGroup("pnd_Input_Letter_Pnd_Input_Bene_Pin_N", "#INPUT-BENE-PIN-N", 
            FieldType.NUMERIC, 12);

        pnd_Input_Letter__R_Field_6 = pnd_Input_Letter__R_Field_1.newGroupInGroup("pnd_Input_Letter__R_Field_6", "REDEFINE", pnd_Input_Letter_Pnd_Input_Bene_Pin);
        pnd_Input_Letter_Pnd_Input_Bene_Pin_N7 = pnd_Input_Letter__R_Field_6.newFieldInGroup("pnd_Input_Letter_Pnd_Input_Bene_Pin_N7", "#INPUT-BENE-PIN-N7", 
            FieldType.NUMERIC, 7);
        pnd_Input_Letter_Pnd_Input_Bene_Pin_Filler = pnd_Input_Letter__R_Field_6.newFieldInGroup("pnd_Input_Letter_Pnd_Input_Bene_Pin_Filler", "#INPUT-BENE-PIN-FILLER", 
            FieldType.STRING, 5);
        pnd_Input_Letter_Pnd_Input_Bene_Care_Name = pnd_Input_Letter__R_Field_1.newFieldInGroup("pnd_Input_Letter_Pnd_Input_Bene_Care_Name", "#INPUT-BENE-CARE-NAME", 
            FieldType.STRING, 40);
        pnd_Input_Letter_Pnd_Input_Bene_Addr1 = pnd_Input_Letter__R_Field_1.newFieldInGroup("pnd_Input_Letter_Pnd_Input_Bene_Addr1", "#INPUT-BENE-ADDR1", 
            FieldType.STRING, 40);
        pnd_Input_Letter_Pnd_Input_Bene_Addr2 = pnd_Input_Letter__R_Field_1.newFieldInGroup("pnd_Input_Letter_Pnd_Input_Bene_Addr2", "#INPUT-BENE-ADDR2", 
            FieldType.STRING, 40);
        pnd_Input_Letter_Pnd_Input_Bene_City = pnd_Input_Letter__R_Field_1.newFieldInGroup("pnd_Input_Letter_Pnd_Input_Bene_City", "#INPUT-BENE-CITY", 
            FieldType.STRING, 40);
        pnd_Input_Letter_Pnd_Input_Bene_State = pnd_Input_Letter__R_Field_1.newFieldInGroup("pnd_Input_Letter_Pnd_Input_Bene_State", "#INPUT-BENE-STATE", 
            FieldType.STRING, 2);
        pnd_Input_Letter_Pnd_Input_Bene_Zip = pnd_Input_Letter__R_Field_1.newFieldInGroup("pnd_Input_Letter_Pnd_Input_Bene_Zip", "#INPUT-BENE-ZIP", FieldType.STRING, 
            10);
        pnd_Input_Letter_Pnd_Input_Bene_Country = pnd_Input_Letter__R_Field_1.newFieldInGroup("pnd_Input_Letter_Pnd_Input_Bene_Country", "#INPUT-BENE-COUNTRY", 
            FieldType.STRING, 40);
        pnd_Input_Letter_Pnd_Input_Bene_Addr_Type = pnd_Input_Letter__R_Field_1.newFieldInGroup("pnd_Input_Letter_Pnd_Input_Bene_Addr_Type", "#INPUT-BENE-ADDR-TYPE", 
            FieldType.STRING, 1);
        pnd_Input_Letter_Pnd_Input_Bene_Letter_Type = pnd_Input_Letter__R_Field_1.newFieldInGroup("pnd_Input_Letter_Pnd_Input_Bene_Letter_Type", "#INPUT-BENE-LETTER-TYPE", 
            FieldType.STRING, 2);
        pnd_Input_Letter_Pnd_Input_Guid = pnd_Input_Letter__R_Field_1.newFieldInGroup("pnd_Input_Letter_Pnd_Input_Guid", "#INPUT-GUID", FieldType.STRING, 
            47);
        pnd_Input_Letter_Pnd_Input_Task_Type = pnd_Input_Letter__R_Field_1.newFieldInGroup("pnd_Input_Letter_Pnd_Input_Task_Type", "#INPUT-TASK-TYPE", 
            FieldType.STRING, 20);
        pnd_Input_Letter_Pnd_Input_Task_Id = pnd_Input_Letter__R_Field_1.newFieldInGroup("pnd_Input_Letter_Pnd_Input_Task_Id", "#INPUT-TASK-ID", FieldType.STRING, 
            15);

        parm_Employee_Info = localVariables.newGroupInRecord("parm_Employee_Info", "PARM-EMPLOYEE-INFO");
        parm_Employee_Info_Pnd_Pnd_Racf_Id = parm_Employee_Info.newFieldInGroup("parm_Employee_Info_Pnd_Pnd_Racf_Id", "##RACF-ID", FieldType.STRING, 8);
        parm_Employee_Info_Pnd_Pnd_Empl_Nme = parm_Employee_Info.newFieldInGroup("parm_Employee_Info_Pnd_Pnd_Empl_Nme", "##EMPL-NME", FieldType.STRING, 
            40);

        parm_Employee_Info__R_Field_7 = parm_Employee_Info.newGroupInGroup("parm_Employee_Info__R_Field_7", "REDEFINE", parm_Employee_Info_Pnd_Pnd_Empl_Nme);
        parm_Employee_Info_Pnd_Pnd_Empl_First_Nme = parm_Employee_Info__R_Field_7.newFieldInGroup("parm_Employee_Info_Pnd_Pnd_Empl_First_Nme", "##EMPL-FIRST-NME", 
            FieldType.STRING, 20);
        parm_Employee_Info_Pnd_Pnd_Empl_Last_Nme = parm_Employee_Info__R_Field_7.newFieldInGroup("parm_Employee_Info_Pnd_Pnd_Empl_Last_Nme", "##EMPL-LAST-NME", 
            FieldType.STRING, 20);
        parm_Employee_Info_Pnd_Pnd_Empl_Table_Updte_Authrty_Cde = parm_Employee_Info.newFieldInGroup("parm_Employee_Info_Pnd_Pnd_Empl_Table_Updte_Authrty_Cde", 
            "##EMPL-TABLE-UPDTE-AUTHRTY-CDE", FieldType.STRING, 2);
        pnd_State_Name = localVariables.newFieldInRecord("pnd_State_Name", "#STATE-NAME", FieldType.STRING, 20);

        parm_Unit_Info = localVariables.newGroupInRecord("parm_Unit_Info", "PARM-UNIT-INFO");
        parm_Unit_Info_Pnd_Pnd_Unit_Cde = parm_Unit_Info.newFieldInGroup("parm_Unit_Info_Pnd_Pnd_Unit_Cde", "##UNIT-CDE", FieldType.STRING, 8);

        parm_Unit_Info__R_Field_8 = parm_Unit_Info.newGroupInGroup("parm_Unit_Info__R_Field_8", "REDEFINE", parm_Unit_Info_Pnd_Pnd_Unit_Cde);
        parm_Unit_Info_Pnd_Pnd_Unit_Id_Cde = parm_Unit_Info__R_Field_8.newFieldInGroup("parm_Unit_Info_Pnd_Pnd_Unit_Id_Cde", "##UNIT-ID-CDE", FieldType.STRING, 
            5);
        parm_Unit_Info_Pnd_Pnd_Unit_Rgn_Cde = parm_Unit_Info__R_Field_8.newFieldInGroup("parm_Unit_Info_Pnd_Pnd_Unit_Rgn_Cde", "##UNIT-RGN-CDE", FieldType.STRING, 
            1);
        parm_Unit_Info_Pnd_Pnd_Unit_Spcl_Dsgntn_Cde = parm_Unit_Info__R_Field_8.newFieldInGroup("parm_Unit_Info_Pnd_Pnd_Unit_Spcl_Dsgntn_Cde", "##UNIT-SPCL-DSGNTN-CDE", 
            FieldType.STRING, 1);
        parm_Unit_Info_Pnd_Pnd_Unit_Brnch_Cde = parm_Unit_Info__R_Field_8.newFieldInGroup("parm_Unit_Info_Pnd_Pnd_Unit_Brnch_Cde", "##UNIT-BRNCH-CDE", 
            FieldType.STRING, 1);
        parm_Unit_Info_Pnd_Pnd_Unit_Name_Long_Txt = parm_Unit_Info.newFieldInGroup("parm_Unit_Info_Pnd_Pnd_Unit_Name_Long_Txt", "##UNIT-NAME-LONG-TXT", 
            FieldType.STRING, 45);
        pnd_Save_Rqst_Id = localVariables.newFieldInRecord("pnd_Save_Rqst_Id", "#SAVE-RQST-ID", FieldType.STRING, 11);
        pnd_W_Bene_Name = localVariables.newFieldInRecord("pnd_W_Bene_Name", "#W-BENE-NAME", FieldType.STRING, 40);
        pnd_Asterisk = localVariables.newFieldInRecord("pnd_Asterisk", "#ASTERISK", FieldType.BOOLEAN, 1);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.PACKED_DECIMAL, 3);
        pnd_Max_Length = localVariables.newFieldInRecord("pnd_Max_Length", "#MAX-LENGTH", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaPstl0495.initializeValues();

        localVariables.reset();
        pnd_Sungard_Ind.setInitialValue("N");
        pnd_Legacy_Ind.setInitialValue("N");
        pnd_Max_Length.setInitialValue(40);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Pvcp5001() throws Exception
    {
        super("Pvcp5001");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("PVCP5001", onError);
        setupReports();
        //* *
        //* ***********************************************************************
        //* *  M A I N   L I N E
        //* ***********************************************************************
        READWORK01:                                                                                                                                                       //Natural: FORMAT ( 1 ) PS = 60 LS = 132;//Natural: READ WORK FILE 1 #INPUT-LETTER ( * )
        while (condition(getWorkFiles().read(1, pnd_Input_Letter.getValue("*"))))
        {
            if (condition(pnd_Input_Letter_Pnd_Input_Letter_Iden.equals("BNFL")))                                                                                         //Natural: IF #INPUT-LETTER-IDEN EQ 'BNFL'
            {
                if (condition(pnd_Input_Letter_Pnd_Input_Letter_Type.equals("F3")))                                                                                       //Natural: IF #INPUT-LETTER-TYPE EQ 'F3'
                {
                    //*        OR #INPUT-LETTER-TYPE EQ 'F9'   /* JCA 20150826
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Input_Letter_Pnd_Input_Letter_Type.notEquals(" ")))                                                                                     //Natural: IF #INPUT-LETTER-TYPE NE ' '
                {
                                                                                                                                                                          //Natural: PERFORM POST-OPEN
                    sub_Post_Open();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-VARIABLES
                    sub_Initialize_Variables();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM STORE-RECORD
                    sub_Store_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-PI-DATA-FIELDS
                    sub_Initialize_Pi_Data_Fields();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM STORE-RECORD
                    sub_Store_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM POST-CLOSE
                    sub_Post_Close();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
                    sub_Write_Report();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POST-OPEN
        //* *ADDRSS-ZP9-NBR      := #INPUT-BENE-ZIP
        //*     IF #A-SIGNATORY-RACF NE ' '
        //*      PSTA9612.EMPL-SGNTRY-UNIT-CDE := #A-SIGNATORY-RACF-UNIT
        //*      PSTA9612.EMPL-SGNTRY-OPRTR-CDE := #A-SIGNATORY-RACF
        //*    END-IF
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZE-VARIABLES
        //* *PVC-DELIVERY-TYPE:= 'P'
        //* ********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZE-PI-DATA-FIELDS
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STORE-RECORD
        //* ********************************
        //* *
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POST-CLOSE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT
        //* *
        //* *                                                                                                                                                             //Natural: ON ERROR
    }
    private void sub_Post_Open() throws Exception                                                                                                                         //Natural: POST-OPEN
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //* *
        //*   SET UP VARS IN PSTA9612 FOR POST OPEN
        //* *
        //*  012910
        pdaPsta9500.getPsta9500().reset();                                                                                                                                //Natural: RESET PSTA9500 PSTA9612
        pdaPsta9612.getPsta9612().reset();
        pdaPsta9612.getPsta9612_Systm_Id_Cde().setValue("PVC");                                                                                                           //Natural: ASSIGN PSTA9612.SYSTM-ID-CDE := 'PVC'
        short decideConditionsMet2148 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #INPUT-LETTER-TYPE;//Natural: VALUE 'F1','F6','F9'
        if (condition((pnd_Input_Letter_Pnd_Input_Letter_Type.equals("F1") || pnd_Input_Letter_Pnd_Input_Letter_Type.equals("F6") || pnd_Input_Letter_Pnd_Input_Letter_Type.equals("F9"))))
        {
            decideConditionsMet2148++;
            pdaPsta9612.getPsta9612_Pckge_Cde().setValue("PTRKBBWI");                                                                                                     //Natural: ASSIGN PSTA9612.PCKGE-CDE := 'PTRKBBWI'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pdaPvca5000.getPvca5000_Pnd_A_Pvc_Delivery_Type().equals("O")))                                                                                     //Natural: IF #A-PVC-DELIVERY-TYPE = 'O'
        {
            pdaPsta9612.getPsta9612_Pckge_Dlvry_Typ().setValue("TSO");                                                                                                    //Natural: ASSIGN PSTA9612.PCKGE-DLVRY-TYP := 'TSO'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaPsta9612.getPsta9612_Pckge_Dlvry_Typ().setValue("   ");                                                                                                    //Natural: ASSIGN PSTA9612.PCKGE-DLVRY-TYP := '   '
            //*  LETTER DATE IS TAKEN FROM DB8 FILE 217
        }                                                                                                                                                                 //Natural: END-IF
        pdaPsta9612.getPsta9612_Letter_Dte().setValue(Global.getDATX());                                                                                                  //Natural: ASSIGN PSTA9612.LETTER-DTE := *DATX
        //*  PINEXP
        //*  PINEXP
        if (condition(pnd_Input_Letter_Pnd_Input_Bene_Pin.greater(" ")))                                                                                                  //Natural: IF #INPUT-BENE-PIN > ' '
        {
            pdaPsta9612.getPsta9612_Univ_Id().setValue(pnd_Input_Letter_Pnd_Input_Bene_Pin);                                                                              //Natural: ASSIGN PSTA9612.UNIV-ID := #INPUT-BENE-PIN
            pdaPsta9612.getPsta9612_Univ_Id_Typ().setValue("P");                                                                                                          //Natural: ASSIGN PSTA9612.UNIV-ID-TYP := 'P'
            //*  PINEXP
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaPsta9612.getPsta9612_Univ_Id().setValue("111111111111");                                                                                                   //Natural: ASSIGN PSTA9612.UNIV-ID := '111111111111'
            pdaPsta9612.getPsta9612_Univ_Id_Typ().setValue("N");                                                                                                          //Natural: ASSIGN PSTA9612.UNIV-ID-TYP := 'N'
        }                                                                                                                                                                 //Natural: END-IF
        //*  022411 START
        pnd_W_Bene_Name.reset();                                                                                                                                          //Natural: RESET #W-BENE-NAME #ASTERISK
        pnd_Asterisk.reset();
        FOR01:                                                                                                                                                            //Natural: FOR #A 1 #MAX-LENGTH
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pnd_Max_Length)); pnd_A.nadd(1))
        {
            if (condition(pnd_Input_Letter_Pnd_Input_Bene_Name_R.getValue(pnd_A).notEquals(" ")))                                                                         //Natural: IF #INPUT-BENE-NAME-R ( #A ) NE ' '
            {
                pnd_W_Bene_Name.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_W_Bene_Name, pnd_Input_Letter_Pnd_Input_Bene_Name_R.getValue(pnd_A)));       //Natural: COMPRESS #W-BENE-NAME #INPUT-BENE-NAME-R ( #A ) INTO #W-BENE-NAME LEAVING NO
                pnd_Asterisk.setValue(false);                                                                                                                             //Natural: ASSIGN #ASTERISK := FALSE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(! (pnd_Asterisk.getBoolean())))                                                                                                             //Natural: IF NOT #ASTERISK
                {
                    pnd_W_Bene_Name.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_W_Bene_Name, "*"));                                                      //Natural: COMPRESS #W-BENE-NAME '*' INTO #W-BENE-NAME LEAVING NO
                    pnd_Asterisk.setValue(true);                                                                                                                          //Natural: ASSIGN #ASTERISK := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  022411 END
        DbsUtil.examine(new ExamineSource(pnd_W_Bene_Name), new ExamineSearch("*"), new ExamineReplace(" "));                                                             //Natural: EXAMINE #W-BENE-NAME FOR '*' REPLACE WITH ' '
        pnd_Input_Letter_Pnd_Input_Bene_Name.setValue(pnd_W_Bene_Name);                                                                                                   //Natural: ASSIGN #INPUT-BENE-NAME := #W-BENE-NAME
        pdaPsta9612.getPsta9612_Full_Nme().setValue(pnd_Input_Letter_Pnd_Input_Bene_Name);                                                                                //Natural: ASSIGN PSTA9612.FULL-NME := #INPUT-BENE-NAME
        //* *ADDRSS-TYP-CDE      := 'P'                     /* 011910
        if (condition(pnd_Input_Letter_Pnd_Input_Bene_Care_Name.notEquals(" ")))                                                                                          //Natural: IF #INPUT-BENE-CARE-NAME NE ' '
        {
            //*  011910 START
            pdaPsta9612.getPsta9612_Addrss_Line_1().setValue(DbsUtil.compress("C/O", pnd_Input_Letter_Pnd_Input_Bene_Care_Name));                                         //Natural: COMPRESS 'C/O' #INPUT-BENE-CARE-NAME INTO ADDRSS-LINE-1
            pdaPsta9612.getPsta9612_Addrss_Line_2().setValue(pnd_Input_Letter_Pnd_Input_Bene_Addr1);                                                                      //Natural: ASSIGN ADDRSS-LINE-2 := #INPUT-BENE-ADDR1
            pdaPsta9612.getPsta9612_Addrss_Line_3().setValue(pnd_Input_Letter_Pnd_Input_Bene_Addr2);                                                                      //Natural: ASSIGN ADDRSS-LINE-3 := #INPUT-BENE-ADDR2
            pdaPsta9612.getPsta9612_Addrss_Typ_Cde().setValue(pnd_Input_Letter_Pnd_Input_Bene_Addr_Type);                                                                 //Natural: ASSIGN PSTA9612.ADDRSS-TYP-CDE := #INPUT-BENE-ADDR-TYPE
            if (condition(pnd_Input_Letter_Pnd_Input_Bene_Addr_Type.equals("F")))                                                                                         //Natural: IF #INPUT-BENE-ADDR-TYPE = 'F'
            {
                pdaPsta9612.getPsta9612_Addrss_Line_4().setValue(DbsUtil.compress(pnd_Input_Letter_Pnd_Input_Bene_City, pnd_Input_Letter_Pnd_Input_Bene_Zip));            //Natural: COMPRESS #INPUT-BENE-CITY #INPUT-BENE-ZIP INTO ADDRSS-LINE-4
                DbsUtil.examine(new ExamineSource(pnd_Input_Letter_Pnd_Input_Bene_Country), new ExamineTranslate(TranslateOption.Upper));                                 //Natural: EXAMINE #INPUT-BENE-COUNTRY AND TRANSLATE INTO UPPER CASE
                pdaPsta9612.getPsta9612_Addrss_Line_5().setValue(pnd_Input_Letter_Pnd_Input_Bene_Country);                                                                //Natural: ASSIGN ADDRSS-LINE-5 := #INPUT-BENE-COUNTRY
                if (condition(pnd_Input_Letter_Pnd_Input_Bene_Country.equals("CANADA")))                                                                                  //Natural: IF #INPUT-BENE-COUNTRY = 'CANADA'
                {
                    pdaPsta9612.getPsta9612_Addrss_Zp9_Nbr().setValue("CANAD");                                                                                           //Natural: ASSIGN ADDRSS-ZP9-NBR := 'CANAD'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaPsta9612.getPsta9612_Addrss_Zp9_Nbr().setValue("FORGN");                                                                                           //Natural: ASSIGN ADDRSS-ZP9-NBR := 'FORGN'
                }                                                                                                                                                         //Natural: END-IF
                //*  011910 END
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  011910
                pdaPsta9612.getPsta9612_Addrss_Line_4().setValue(DbsUtil.compress(pnd_Input_Letter_Pnd_Input_Bene_City, pnd_Input_Letter_Pnd_Input_Bene_State));          //Natural: COMPRESS #INPUT-BENE-CITY #INPUT-BENE-STATE INTO ADDRSS-LINE-4
                pdaPsta9612.getPsta9612_Addrss_Zp9_Nbr().setValue(pnd_Input_Letter_Pnd_Input_Bene_Zip);                                                                   //Natural: ASSIGN ADDRSS-ZP9-NBR := #INPUT-BENE-ZIP
                //*  011910
            }                                                                                                                                                             //Natural: END-IF
            //*  011910 START
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaPsta9612.getPsta9612_Addrss_Line_1().setValue(pnd_Input_Letter_Pnd_Input_Bene_Addr1);                                                                      //Natural: ASSIGN ADDRSS-LINE-1 := #INPUT-BENE-ADDR1
            pdaPsta9612.getPsta9612_Addrss_Line_2().setValue(pnd_Input_Letter_Pnd_Input_Bene_Addr2);                                                                      //Natural: ASSIGN ADDRSS-LINE-2 := #INPUT-BENE-ADDR2
            pdaPsta9612.getPsta9612_Addrss_Typ_Cde().setValue(pnd_Input_Letter_Pnd_Input_Bene_Addr_Type);                                                                 //Natural: ASSIGN PSTA9612.ADDRSS-TYP-CDE := #INPUT-BENE-ADDR-TYPE
            if (condition(pnd_Input_Letter_Pnd_Input_Bene_Addr_Type.equals("F")))                                                                                         //Natural: IF #INPUT-BENE-ADDR-TYPE = 'F'
            {
                pdaPsta9612.getPsta9612_Addrss_Line_3().setValue(DbsUtil.compress(pnd_Input_Letter_Pnd_Input_Bene_City, pnd_Input_Letter_Pnd_Input_Bene_Zip));            //Natural: COMPRESS #INPUT-BENE-CITY #INPUT-BENE-ZIP INTO ADDRSS-LINE-3
                DbsUtil.examine(new ExamineSource(pnd_Input_Letter_Pnd_Input_Bene_Country), new ExamineTranslate(TranslateOption.Upper));                                 //Natural: EXAMINE #INPUT-BENE-COUNTRY AND TRANSLATE INTO UPPER CASE
                pdaPsta9612.getPsta9612_Addrss_Line_4().setValue(pnd_Input_Letter_Pnd_Input_Bene_Country);                                                                //Natural: ASSIGN ADDRSS-LINE-4 := #INPUT-BENE-COUNTRY
                if (condition(pnd_Input_Letter_Pnd_Input_Bene_Country.equals("CANADA")))                                                                                  //Natural: IF #INPUT-BENE-COUNTRY = 'CANADA'
                {
                    pdaPsta9612.getPsta9612_Addrss_Zp9_Nbr().setValue("CANAD");                                                                                           //Natural: ASSIGN ADDRSS-ZP9-NBR := 'CANAD'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaPsta9612.getPsta9612_Addrss_Zp9_Nbr().setValue("FORGN");                                                                                           //Natural: ASSIGN ADDRSS-ZP9-NBR := 'FORGN'
                }                                                                                                                                                         //Natural: END-IF
                //*  011910 END
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  011910
                pdaPsta9612.getPsta9612_Addrss_Line_3().setValue(DbsUtil.compress(pnd_Input_Letter_Pnd_Input_Bene_City, pnd_Input_Letter_Pnd_Input_Bene_State));          //Natural: COMPRESS #INPUT-BENE-CITY #INPUT-BENE-STATE INTO ADDRSS-LINE-3
                pdaPsta9612.getPsta9612_Addrss_Zp9_Nbr().setValue(pnd_Input_Letter_Pnd_Input_Bene_Zip);                                                                   //Natural: ASSIGN ADDRSS-ZP9-NBR := #INPUT-BENE-ZIP
                //*  011910
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pdaPsta9612.getPsta9612_Dont_Use_Finalist().setValue(true);                                                                                                       //Natural: ASSIGN PSTA9612.DONT-USE-FINALIST := TRUE
        pdaPsta9612.getPsta9612_Addrss_Lines_Rule().setValue(false);                                                                                                      //Natural: ASSIGN PSTA9612.ADDRSS-LINES-RULE := FALSE
        if (condition(pnd_Input_Letter_Pnd_Input_Letter_Type.equals("LE") || pnd_Input_Letter_Pnd_Input_Letter_Type.equals("LT") || pnd_Input_Letter_Pnd_Input_Letter_Type.equals("AT")  //Natural: IF #INPUT-LETTER-TYPE EQ 'LE' OR #INPUT-LETTER-TYPE EQ 'LT' OR #INPUT-LETTER-TYPE EQ 'AT' OR #INPUT-LETTER-TYPE EQ 'EE' OR #INPUT-LETTER-TYPE EQ 'EC' OR #INPUT-LETTER-TYPE EQ 'ET' OR #INPUT-LETTER-TYPE EQ 'SM' OR #INPUT-LETTER-TYPE EQ 'SC'
            || pnd_Input_Letter_Pnd_Input_Letter_Type.equals("EE") || pnd_Input_Letter_Pnd_Input_Letter_Type.equals("EC") || pnd_Input_Letter_Pnd_Input_Letter_Type.equals("ET") 
            || pnd_Input_Letter_Pnd_Input_Letter_Type.equals("SM") || pnd_Input_Letter_Pnd_Input_Letter_Type.equals("SC")))
        {
            pdaPsta9612.getPsta9612_Lttr_Slttn_Txt().setValue(pnd_Input_Letter_Pnd_Input_Bene_Care_Name);                                                                 //Natural: ASSIGN PSTA9612.LTTR-SLTTN-TXT := #INPUT-BENE-CARE-NAME
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaPsta9612.getPsta9612_Lttr_Slttn_Txt().setValue(pdaPsta9612.getPsta9612_Full_Nme());                                                                        //Natural: ASSIGN PSTA9612.LTTR-SLTTN-TXT := PSTA9612.FULL-NME
            //*  NEEDED IF CWF IS NOT USED
            //*  NEEDED IF CWF IS NOT USED
        }                                                                                                                                                                 //Natural: END-IF
        pdaPsta9612.getPsta9612_No_Updte_To_Mit_Ind().setValue(true);                                                                                                     //Natural: ASSIGN PSTA9612.NO-UPDTE-TO-MIT-IND := TRUE
        pdaPsta9612.getPsta9612_No_Updte_To_Efm_Ind().setValue(true);                                                                                                     //Natural: ASSIGN PSTA9612.NO-UPDTE-TO-EFM-IND := TRUE
        //*  CALL POST OPEN
        //* **
        //*    #ERROR-STRING := 'Post OPEN'
        DbsUtil.callnat(Pstn9612.class , getCurrentProcessState(), pdaPsta9610.getPsta9610(), pdaPsta9612.getPsta9612(), pdaCwfpda_M.getMsg_Info_Sub(),                   //Natural: CALLNAT 'PSTN9612' PSTA9610 PSTA9612 MSG-INFO-SUB PARM-EMPLOYEE-INFO PARM-UNIT-INFO
            parm_Employee_Info, parm_Unit_Info);
        if (condition(Global.isEscape())) return;
        //* **
        //*  CHECK FOR ERRORS IN POST OPEN.
        //* **
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE <> ' '
        {
            DbsUtil.callnat(Pstn9980.class , getCurrentProcessState(), pdaCwfpda_M.getMsg_Info_Sub());                                                                    //Natural: CALLNAT 'PSTN9980' MSG-INFO-SUB
            if (condition(Global.isEscape())) return;
            pdaPvca5000.getPvca5000_Pnd_Result_Code().setValue(4090);                                                                                                     //Natural: ASSIGN PVCA5000.#RESULT-CODE := 4090
            pdaPvca5000.getPvca5000_Pnd_Result_Msg().setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                 //Natural: ASSIGN PVCA5000.#RESULT-MSG := MSG-INFO-SUB.##MSG
            getReports().write(0, "=",pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg(),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code());                                       //Natural: WRITE '=' MSG-INFO-SUB.##MSG MSG-INFO-SUB.##RETURN-CODE
            if (Global.isEscape()) return;
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "MAIL DATA STORED    :",pdaPsta9610.getPsta9610_Pst_Rqst_Id(),NEWLINE,"=",pdaPsta9612.getPsta9612_Pckge_Cde());                         //Natural: WRITE 'MAIL DATA STORED    :' PSTA9610.PST-RQST-ID / '=' PSTA9612.PCKGE-CDE
            if (Global.isEscape()) return;
            pnd_Save_Rqst_Id.setValue(pdaPsta9610.getPsta9610_Pst_Rqst_Id());                                                                                             //Natural: ASSIGN #SAVE-RQST-ID := PSTA9610.PST-RQST-ID
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Initialize_Variables() throws Exception                                                                                                              //Natural: INITIALIZE-VARIABLES
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        //*  INC3952468
        //*  011910
        ldaPstl0495.getPstl0495_Data_Pstl0495_Data_Array().getValue("*").reset();                                                                                         //Natural: RESET PSTL0495-DATA.PSTL0495-DATA-ARRAY ( * )
        ldaPstl0495.getPstl0495_Data_Pstl0495_Rec_Id().setValue("AA");                                                                                                    //Natural: ASSIGN PSTL0495-REC-ID := 'AA'
        ldaPstl0495.getPstl0495_Data_Letter_Type().setValue(pnd_Input_Letter_Pnd_Input_Letter_Type);                                                                      //Natural: ASSIGN LETTER-TYPE := #INPUT-LETTER-TYPE
        ldaPstl0495.getPstl0495_Data_Decedent_Pin_Nbr().setValue(pnd_Input_Letter_Pnd_Input_Pin);                                                                         //Natural: ASSIGN DECEDENT-PIN-NBR := #INPUT-PIN
        ldaPstl0495.getPstl0495_Data_Decedent_Name().setValue(pnd_Input_Letter_Pnd_Input_Decendent_Name);                                                                 //Natural: ASSIGN DECEDENT-NAME := #INPUT-DECENDENT-NAME
        ldaPstl0495.getPstl0495_Data_Decedent_Date_Of_Birth().setValue(pdaPvca5000.getPvca5000_Pnd_A_Decedent_Date_Of_Birth());                                           //Natural: ASSIGN DECEDENT-DATE-OF-BIRTH := #A-DECEDENT-DATE-OF-BIRTH
        ldaPstl0495.getPstl0495_Data_Decedent_Date_Of_Death().setValue(pdaPvca5000.getPvca5000_Pnd_A_Decedent_Date_Of_Death());                                           //Natural: ASSIGN DECEDENT-DATE-OF-DEATH := #A-DECEDENT-DATE-OF-DEATH
        ldaPstl0495.getPstl0495_Data_Survivor_Task_Category().setValue("S");                                                                                              //Natural: ASSIGN SURVIVOR-TASK-CATEGORY := 'S'
        ldaPstl0495.getPstl0495_Data_Pvc_Delivery_Type().setValue("S");                                                                                                   //Natural: ASSIGN PVC-DELIVERY-TYPE := 'S'
    }
    private void sub_Initialize_Pi_Data_Fields() throws Exception                                                                                                         //Natural: INITIALIZE-PI-DATA-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************************
        ldaPstl0495.getPstl0495_Data_Pstl0495_Data_Array().getValue("*").reset();                                                                                         //Natural: RESET PSTL0495-DATA-ARRAY ( * )
        ldaPstl0495.getPstl0495_Data_Pstl0495_Rec_Id().setValue("PI");                                                                                                    //Natural: ASSIGN PSTL0495-REC-ID := 'PI'
        //* *PI-EXPORT-IND     := 'G'                       /* 022610 START
        //*  START DM 070213
        //*  IF #INPUT-TASK-ID = MASK ('T') OR = MASK ('T') /* 022411 START
        //*    PI-EXPORT-IND     := 'I'
        //*    PI-TASK-ID        := #INPUT-TASK-ID
        //*    PI-TASK-GUID      := ' '
        //*  ELSE                                           /* 022511 END
        //*    IF #INPUT-GUID = MASK ('T') OR = MASK ('T')  /* 040810
        //*      PI-EXPORT-IND     := 'I'
        //*      PI-TASK-ID        := #INPUT-TASK-ID        /* 022411
        //*      PI-TASK-ID        := #INPUT-GUID           /* 022411
        //*      PI-TASK-GUID      := ' '                   /* 040810 START
        //*    ELSE
        //*      PI-EXPORT-IND     := 'G'
        //*      PI-TASK-GUID      := #INPUT-GUID
        //*      PI-TASK-ID        := ' '
        //*    END-IF                                       /* 040810 END
        //*  END-IF                                         /* 022411
        //* *PI-TASK-GUID      := ' '                       /* 022610 END
        if (condition(pnd_Input_Letter_Pnd_Input_Guid.notEquals(" ")))                                                                                                    //Natural: IF #INPUT-GUID NE ' '
        {
            //*  20141230
            if (condition(pnd_Input_Letter_Pnd_Input_Guid.getSubstring(1,1).equals("T") || pnd_Input_Letter_Pnd_Input_Guid.getSubstring(1,1).equals("t")))                //Natural: IF SUBSTR ( #INPUT-GUID,1,1 ) = 'T' OR = 't'
            {
                ldaPstl0495.getPstl0495_Data_Pi_Task_Id().setValue(pnd_Input_Letter_Pnd_Input_Guid);                                                                      //Natural: ASSIGN PI-TASK-ID := #INPUT-GUID
                ldaPstl0495.getPstl0495_Data_Pi_Export_Ind().setValue("I");                                                                                               //Natural: ASSIGN PI-EXPORT-IND := 'I'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaPstl0495.getPstl0495_Data_Pi_Task_Guid().setValue(pnd_Input_Letter_Pnd_Input_Guid);                                                                    //Natural: ASSIGN PI-TASK-GUID := #INPUT-GUID
                ldaPstl0495.getPstl0495_Data_Pi_Export_Ind().setValue("G");                                                                                               //Natural: ASSIGN PI-EXPORT-IND := 'G'
            }                                                                                                                                                             //Natural: END-IF
            ldaPstl0495.getPstl0495_Data_Pi_Task_Type().setValue(pnd_Input_Letter_Pnd_Input_Task_Type);                                                                   //Natural: ASSIGN PI-TASK-TYPE := #INPUT-TASK-TYPE
            ldaPstl0495.getPstl0495_Data_Pi_Action_Step().setValue("Process");                                                                                            //Natural: ASSIGN PI-ACTION-STEP := 'Process'
            ldaPstl0495.getPstl0495_Data_Pi_Doc_Content().setValue("POST LETTER");                                                                                        //Natural: ASSIGN PI-DOC-CONTENT := 'POST LETTER'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Input_Letter_Pnd_Input_Task_Id.notEquals(" ")))                                                                                             //Natural: IF #INPUT-TASK-ID NE ' '
            {
                ldaPstl0495.getPstl0495_Data_Pi_Export_Ind().setValue("I");                                                                                               //Natural: ASSIGN PI-EXPORT-IND := 'I'
                ldaPstl0495.getPstl0495_Data_Pi_Task_Id().setValue(pnd_Input_Letter_Pnd_Input_Task_Id);                                                                   //Natural: ASSIGN PI-TASK-ID := #INPUT-TASK-ID
                ldaPstl0495.getPstl0495_Data_Pi_Task_Type().setValue(pnd_Input_Letter_Pnd_Input_Task_Type);                                                               //Natural: ASSIGN PI-TASK-TYPE := #INPUT-TASK-TYPE
                ldaPstl0495.getPstl0495_Data_Pi_Action_Step().setValue("Process");                                                                                        //Natural: ASSIGN PI-ACTION-STEP := 'Process'
                ldaPstl0495.getPstl0495_Data_Pi_Doc_Content().setValue("POST LETTER");                                                                                    //Natural: ASSIGN PI-DOC-CONTENT := 'POST LETTER'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaPstl0495.getPstl0495_Data_Pi_Pin_Type().setValue("P");                                                                                                 //Natural: ASSIGN PI-PIN-TYPE := 'P'
                //*  PINEXP
                ldaPstl0495.getPstl0495_Data_Pi_Pin_Npin_Ppg().reset();                                                                                                   //Natural: RESET PI-PIN-NPIN-PPG
                ldaPstl0495.getPstl0495_Data_Pi_Plan_Id().setValue(pnd_Input_Letter_Pnd_Input_Plan);                                                                      //Natural: ASSIGN PI-PLAN-ID := #INPUT-PLAN
                ldaPstl0495.getPstl0495_Data_Pi_Export_Ind().setValue("T");                                                                                               //Natural: ASSIGN PI-EXPORT-IND := 'T'
                ldaPstl0495.getPstl0495_Data_Pi_Task_Type().setValue("Confirm");                                                                                          //Natural: ASSIGN PI-TASK-TYPE := 'Confirm'
                ldaPstl0495.getPstl0495_Data_Pi_Task_Status().setValue("C");                                                                                              //Natural: ASSIGN PI-TASK-STATUS := 'C'
                ldaPstl0495.getPstl0495_Data_Pi_Action_Step().setValue("None");                                                                                           //Natural: ASSIGN PI-ACTION-STEP := 'None'
                ldaPstl0495.getPstl0495_Data_Pi_Tiaa_Full_Date().setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                         //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO PI-TIAA-FULL-DATE
                ldaPstl0495.getPstl0495_Data_Pi_Tiaa_Time().setValueEdited(Global.getTIMX(),new ReportEditMask("HH:MM:SS"));                                              //Natural: MOVE EDITED *TIMX ( EM = HH:MM:SS ) TO PI-TIAA-TIME
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  END DM 070213
        //*  INITIALIZE-PI-DATA-FIELDS
    }
    private void sub_Store_Record() throws Exception                                                                                                                      //Natural: STORE-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
        DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                    //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY PSTL0495-DATA
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
            ldaPstl0495.getSort_Key(), ldaPstl0495.getPstl0495_Data());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE <> ' '
        {
            DbsUtil.callnat(Pstn9980.class , getCurrentProcessState(), pdaCwfpda_M.getMsg_Info_Sub());                                                                    //Natural: CALLNAT 'PSTN9980' MSG-INFO-SUB
            if (condition(Global.isEscape())) return;
            pdaPvca5000.getPvca5000_Pnd_Result_Code().setValue(4091);                                                                                                     //Natural: ASSIGN PVCA5000.#RESULT-CODE := 4091
            pdaPvca5000.getPvca5000_Pnd_Result_Msg().setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                 //Natural: ASSIGN PVCA5000.#RESULT-MSG := MSG-INFO-SUB.##MSG
            getReports().write(0, "=",pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg(),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code(),"STORE");                               //Natural: WRITE '=' MSG-INFO-SUB.##MSG MSG-INFO-SUB.##RETURN-CODE 'STORE'
            if (Global.isEscape()) return;
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaPvca5000.getPvca5000_Pnd_Result_Code().setValue(0);                                                                                                        //Natural: ASSIGN PVCA5000.#RESULT-CODE := 0000
            pdaPvca5000.getPvca5000_Pnd_Result_Msg().setValue(pdaPsta9610.getPsta9610_Pst_Rqst_Id());                                                                     //Natural: ASSIGN PVCA5000.#RESULT-MSG := PSTA9610.PST-RQST-ID
            getReports().write(0, "MAIL DATA STORED    :",pdaPsta9610.getPsta9610_Pst_Rqst_Id(),NEWLINE,"=",pdaPsta9612.getPsta9612_Pckge_Cde());                         //Natural: WRITE 'MAIL DATA STORED    :' PSTA9610.PST-RQST-ID / '=' PSTA9612.PCKGE-CDE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  STORE-RECORD
    }
    private void sub_Post_Close() throws Exception                                                                                                                        //Natural: POST-CLOSE
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        //*  CALL POST CLOSE TO FINALISE THE REQUEST
        DbsUtil.callnat(Pstn9685.class , getCurrentProcessState(), pdaPsta9610.getPsta9610(), pdaPsta9612.getPsta9612(), pdaCwfpda_M.getMsg_Info_Sub(),                   //Natural: CALLNAT 'PSTN9685' PSTA9610 PSTA9612 MSG-INFO-SUB PARM-EMPLOYEE-INFO PARM-UNIT-INFO
            parm_Employee_Info, parm_Unit_Info);
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE <> ' '
        {
            pdaPvca5000.getPvca5000_Pnd_Result_Code().setValue(4090);                                                                                                     //Natural: ASSIGN PVCA5000.#RESULT-CODE := 4090
            pdaPvca5000.getPvca5000_Pnd_Result_Msg().setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                 //Natural: ASSIGN PVCA5000.#RESULT-MSG := MSG-INFO-SUB.##MSG
            getReports().write(0, "=",pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg(),pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code(),"CLOSE");                               //Natural: WRITE '=' MSG-INFO-SUB.##MSG MSG-INFO-SUB.##RETURN-CODE 'CLOSE'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaPvca5000.getPvca5000_Pnd_Result_Code().setValue(0);                                                                                                        //Natural: ASSIGN PVCA5000.#RESULT-CODE := 0000
            pdaPvca5000.getPvca5000_Pnd_Result_Msg().setValue(pdaPsta9610.getPsta9610_Pst_Rqst_Id());                                                                     //Natural: ASSIGN PVCA5000.#RESULT-MSG := PSTA9610.PST-RQST-ID
        }                                                                                                                                                                 //Natural: END-IF
        //* *IF PSTA9612.PCKGE-DLVRY-TYP EQ 'EML' AND #A-DELIVERY-TYPE EQ 'S'
        //* *  RESET PSTA9612.PCKGE-DLVRY-TYP
        //* *    PSTA9612.ALT-DLVRY-ADDR
        //* *END-IF
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        pdaPsta9513.getPsta9513_Mail_Id().setValue(pnd_Save_Rqst_Id);                                                                                                     //Natural: ASSIGN PSTA9513.MAIL-ID := #SAVE-RQST-ID
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("GET");                                                                                                               //Natural: ASSIGN CDAOBJ.#FUNCTION := 'GET'
        DbsUtil.callnat(Pstn9513.class , getCurrentProcessState(), pdaPsta9513.getPsta9513(), pdaPsta9513.getPsta9513_Id(), pdaPsta9514.getPsta9514(),                    //Natural: CALLNAT 'PSTN9513' PSTA9513 PSTA9513-ID PSTA9514 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
        if (condition(Global.isEscape())) return;
        if (condition(pdaPsta9513.getPsta9513_Pckge_Dlvry_Typ().equals("EML")))                                                                                           //Natural: IF PSTA9513.PCKGE-DLVRY-TYP EQ 'EML'
        {
            pdaPsta9513.getPsta9513_Mail_Id().setValue(pnd_Save_Rqst_Id);                                                                                                 //Natural: ASSIGN PSTA9513.MAIL-ID := #SAVE-RQST-ID
            pdaPsta9513.getPsta9513_Pckge_Dlvry_Typ().setValue("  ");                                                                                                     //Natural: ASSIGN PSTA9513.PCKGE-DLVRY-TYP := '  '
            pdaPsta9513.getPsta9513_Alt_Dlvry_Addr().setValue("           ");                                                                                             //Natural: ASSIGN PSTA9513.ALT-DLVRY-ADDR := '           '
            pdaCdaobj.getCdaobj_Pnd_Function().setValue("UPDATE");                                                                                                        //Natural: ASSIGN CDAOBJ.#FUNCTION := 'UPDATE'
            DbsUtil.callnat(Pstn9513.class , getCurrentProcessState(), pdaPsta9513.getPsta9513(), pdaPsta9513.getPsta9513_Id(), pdaPsta9514.getPsta9514(),                //Natural: CALLNAT 'PSTN9513' PSTA9513 PSTA9513-ID PSTA9514 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
                pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
            if (condition(Global.isEscape())) return;
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
        //*  POST-CLOSE
    }
    private void sub_Write_Report() throws Exception                                                                                                                      //Natural: WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(1, "Plan",                                                                                                                                   //Natural: DISPLAY ( 1 ) 'Plan' #INPUT-PLAN 'Part' #INPUT-PARTID 'Deceased Name' #INPUT-DECENDENT-NAME ( AL = 30 ) 'Bene Name' #INPUT-BENE-NAME ( AL = 30 ) 'GUID' #INPUT-GUID ( AL = 40 )
        		pnd_Input_Letter_Pnd_Input_Plan,"Part",
        		pnd_Input_Letter_Pnd_Input_Partid,"Deceased Name",
        		pnd_Input_Letter_Pnd_Input_Decendent_Name, new AlphanumericLength (30),"Bene Name",
        		pnd_Input_Letter_Pnd_Input_Bene_Name, new AlphanumericLength (30),"GUID",
        		pnd_Input_Letter_Pnd_Input_Guid, new AlphanumericLength (40));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        pdaPvca5000.getPvca5000_Pnd_Result_Msg().setValue(DbsUtil.compress("NATURAL ON ERROR", Global.getPROGRAM(), Global.getERROR_NR(), Global.getERROR_LINE()));       //Natural: COMPRESS 'NATURAL ON ERROR' *PROGRAM *ERROR-NR *ERROR-LINE INTO PVCA5000.#RESULT-MSG
        pdaPvca5000.getPvca5000_Pnd_Result_Code().setValue(Global.getERROR_LINE());                                                                                       //Natural: ASSIGN PVCA5000.#RESULT-CODE := *ERROR-LINE
        pnd_Response_Code.setValue(pdaPvca5000.getPvca5000_Pnd_Result_Code());                                                                                            //Natural: ASSIGN #RESPONSE-CODE := PVCA5000.#RESULT-CODE
        pnd_Response_Text.setValue(pdaPvca5000.getPvca5000_Pnd_Result_Msg());                                                                                             //Natural: ASSIGN #RESPONSE-TEXT := PVCA5000.#RESULT-MSG
        //*  TS 032105
        //*  CALLNAT  'PVCN4099'  /* UPDATE MIT
        //*    PVCA5000
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=132");

        getReports().setDisplayColumns(1, "Plan",
        		pnd_Input_Letter_Pnd_Input_Plan,"Part",
        		pnd_Input_Letter_Pnd_Input_Partid,"Deceased Name",
        		pnd_Input_Letter_Pnd_Input_Decendent_Name, new AlphanumericLength (30),"Bene Name",
        		pnd_Input_Letter_Pnd_Input_Bene_Name, new AlphanumericLength (30),"GUID",
        		pnd_Input_Letter_Pnd_Input_Guid, new AlphanumericLength (40));
    }
}
