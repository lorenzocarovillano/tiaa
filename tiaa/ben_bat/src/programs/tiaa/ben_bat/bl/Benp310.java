/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:14:57 PM
**        * FROM NATURAL PROGRAM : Benp310
************************************************************
**        * FILE NAME            : Benp310.java
**        * CLASS NAME           : Benp310
**        * INSTANCE NAME        : Benp310
************************************************************
***********************************************************************
* PROGRAM NAME : BENP310
* DESCRIPTION  : BENEFICIARY BATCH SYSTEM - COPY BENE DESIGNATION FOR
*                NEGATIVE ENROLLMENT / SAME AS CONTRACTS
* WRITTEN BY   : DENNIS DURAN
* DATE WRITTEN : 10/13/2017
***********************************************************************
*    DATE      USERID                   DESCRIPTION
* MM/DD/YYYY  XXXXXXXX  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
***********************************************************************

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benp310 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Acis;
    private DbsField pnd_Acis_Pnd_I_Plan;
    private DbsField pnd_Acis_Pnd_I_Fill1;
    private DbsField pnd_Acis_Pnd_I_Pin;
    private DbsField pnd_Acis_Pnd_I_Fill2;
    private DbsField pnd_Acis_Pnd_I_Tiaa_Src;
    private DbsField pnd_Acis_Pnd_I_Fill3;
    private DbsField pnd_Acis_Pnd_I_Cref_Src;
    private DbsField pnd_Acis_Pnd_I_Fill4;
    private DbsField pnd_Acis_Pnd_I_Tiaa_Tgt;
    private DbsField pnd_Acis_Pnd_I_Fill5;
    private DbsField pnd_Acis_Pnd_I_Cref_Tgt;
    private DbsField pnd_Acis_Pnd_I_Fill6;
    private DbsField pnd_Acis_Pnd_I_Eff_Date;

    private DataAccessProgramView vw_bc;
    private DbsField bc_Bc_Pin;
    private DbsField bc_Bc_Tiaa_Cntrct;
    private DbsField bc_Bc_Cref_Cntrct;
    private DbsField bc_Bc_Tiaa_Cref_Ind;
    private DbsField bc_Bc_Stat;
    private DbsField bc_Bc_Cntrct_Type;
    private DbsField bc_Bc_Rqst_Timestamp;
    private DbsField bc_Bc_Eff_Dte;
    private DbsField bc_Bc_Mos_Ind;
    private DbsField bc_Bc_Irvcbl_Ind;
    private DbsField bc_Bc_Pymnt_Chld_Dcsd_Ind;
    private DbsField bc_Bc_Exempt_Spouse_Rights;
    private DbsField bc_Bc_Spouse_Waived_Bnfts;
    private DbsField bc_Bc_Trust_Data_Fldr;
    private DbsField bc_Bc_Bene_Addr_Fldr;
    private DbsField bc_Bc_Co_Owner_Data_Fldr;
    private DbsField bc_Bc_Fldr_Min;
    private DbsField bc_Bc_Fldr_Srce_Id;
    private DbsField bc_Bc_Fldr_Log_Dte_Tme;
    private DbsField bc_Bc_Rcrd_Last_Updt_Dte;
    private DbsField bc_Bc_Rcrd_Last_Updt_Tme;
    private DbsField bc_Bc_Rcrd_Last_Updt_Userid;
    private DbsField bc_Bc_Last_Dsgntn_Srce;
    private DbsField bc_Bc_Last_Dsgntn_System;
    private DbsField bc_Bc_Last_Dsgntn_Userid;
    private DbsField bc_Bc_Last_Dsgntn_Dte;
    private DbsField bc_Bc_Last_Dsgntn_Tme;
    private DbsField bc_Bc_Last_Vrfy_Dte;
    private DbsField bc_Bc_Last_Vrfy_Tme;
    private DbsField bc_Bc_Last_Vrfy_Userid;
    private DbsField bc_Bc_Tiaa_Cref_Chng_Dte;
    private DbsField bc_Bc_Tiaa_Cref_Chng_Tme;
    private DbsField bc_Bc_Chng_Pwr_Atty;
    private DbsField bc_Bc_Mgrtn_Cnfrm_Stmnt_Prod_Dte;

    private DataAccessProgramView vw_bd;
    private DbsField bd_Bd_Pin;
    private DbsField bd_Bd_Tiaa_Cntrct;
    private DbsField bd_Bd_Cref_Cntrct;
    private DbsField bd_Bd_Tiaa_Cref_Ind;
    private DbsField bd_Bd_Stat;
    private DbsField bd_Bd_Seq_Nmbr;
    private DbsField bd_Bd_Rqst_Timestamp;
    private DbsField bd_Bd_Rcrd_Last_Updt_Dte;
    private DbsField bd_Bd_Rcrd_Last_Updt_Tme;
    private DbsField bd_Bd_Rcrd_Last_Updt_Userid;
    private DbsField bd_Bd_Bene_Type;
    private DbsField bd_Bd_Rltn_Cd;
    private DbsField bd_Bd_Bene_Name1;
    private DbsField bd_Bd_Bene_Name2;
    private DbsField bd_Bd_Rltn_Free_Txt;
    private DbsField bd_Bd_Dte_Birth_Trust;
    private DbsField bd_Bd_Ss_Cd;
    private DbsField bd_Bd_Ss_Nmbr;
    private DbsField bd_Bd_Perc_Share_Ind;
    private DbsField bd_Bd_Share_Perc;
    private DbsField bd_Bd_Share_Ntor;
    private DbsField bd_Bd_Share_Dtor;
    private DbsField bd_Bd_Irvcbl_Ind;
    private DbsField bd_Bd_Stlmnt_Rstrctn;
    private DbsField bd_Bd_Spcl_Txt1;
    private DbsField bd_Bd_Spcl_Txt2;
    private DbsField bd_Bd_Spcl_Txt3;
    private DbsField bd_Bd_Dflt_Estate;
    private DbsField bd_Bd_Mdo_Calc_Bene;
    private DbsField bd_Bd_Addr1;
    private DbsField bd_Bd_Addr2;
    private DbsField bd_Bd_Addr3_City;
    private DbsField bd_Bd_State;
    private DbsField bd_Bd_Zip;
    private DbsField bd_Bd_Phone;

    private DataAccessProgramView vw_bm;
    private DbsField bm_Bm_Pin;
    private DbsField bm_Bm_Tiaa_Cntrct;
    private DbsField bm_Bm_Cref_Cntrct;
    private DbsField bm_Bm_Tiaa_Cref_Ind;
    private DbsField bm_Bm_Stat;
    private DbsField bm_Bm_Rqst_Timestamp;
    private DbsField bm_Bm_Rcrd_Last_Updt_Dte;
    private DbsField bm_Bm_Rcrd_Last_Updt_Tme;
    private DbsField bm_Bm_Rcrd_Last_Updt_Userid;

    private DbsGroup bm_Bm_Mos_Txt_Group;
    private DbsField bm_Bm_Mos_Txt;

    private DataAccessProgramView vw_bca;
    private DbsField bca_Bc_Pin;
    private DbsField bca_Bc_Tiaa_Cntrct;
    private DbsField bca_Bc_Cref_Cntrct;
    private DbsField bca_Bc_Tiaa_Cref_Ind;
    private DbsField bca_Bc_Stat;
    private DbsField bca_Bc_Cntrct_Type;
    private DbsField bca_Bc_Rqst_Timestamp;
    private DbsField bca_Bc_Eff_Dte;
    private DbsField bca_Bc_Mos_Ind;
    private DbsField bca_Bc_Irvcbl_Ind;
    private DbsField bca_Bc_Pymnt_Chld_Dcsd_Ind;
    private DbsField bca_Bc_Exempt_Spouse_Rights;
    private DbsField bca_Bc_Spouse_Waived_Bnfts;
    private DbsField bca_Bc_Trust_Data_Fldr;
    private DbsField bca_Bc_Bene_Addr_Fldr;
    private DbsField bca_Bc_Co_Owner_Data_Fldr;
    private DbsField bca_Bc_Fldr_Min;
    private DbsField bca_Bc_Fldr_Srce_Id;
    private DbsField bca_Bc_Fldr_Log_Dte_Tme;
    private DbsField bca_Bc_Rcrd_Last_Updt_Dte;
    private DbsField bca_Bc_Rcrd_Last_Updt_Tme;
    private DbsField bca_Bc_Rcrd_Last_Updt_Userid;
    private DbsField bca_Bc_Last_Dsgntn_Srce;
    private DbsField bca_Bc_Last_Dsgntn_System;
    private DbsField bca_Bc_Last_Dsgntn_Userid;
    private DbsField bca_Bc_Last_Dsgntn_Dte;
    private DbsField bca_Bc_Last_Dsgntn_Tme;
    private DbsField bca_Bc_Chng_Pwr_Atty;
    private DbsField bca_Bc_Mgrtn_Cnfrm_Stmnt_Prod_Dte;

    private DataAccessProgramView vw_bda;
    private DbsField bda_Bd_Pin;
    private DbsField bda_Bd_Tiaa_Cntrct;
    private DbsField bda_Bd_Cref_Cntrct;
    private DbsField bda_Bd_Tiaa_Cref_Ind;
    private DbsField bda_Bd_Stat;
    private DbsField bda_Bd_Seq_Nmbr;
    private DbsField bda_Bd_Rqst_Timestamp;
    private DbsField bda_Bd_Rcrd_Last_Updt_Dte;
    private DbsField bda_Bd_Rcrd_Last_Updt_Tme;
    private DbsField bda_Bd_Rcrd_Last_Updt_Userid;
    private DbsField bda_Bd_Bene_Type;
    private DbsField bda_Bd_Rltn_Cd;
    private DbsField bda_Bd_Bene_Name1;
    private DbsField bda_Bd_Bene_Name2;
    private DbsField bda_Bd_Rltn_Free_Txt;
    private DbsField bda_Bd_Dte_Birth_Trust;
    private DbsField bda_Bd_Ss_Cd;
    private DbsField bda_Bd_Ss_Nmbr;
    private DbsField bda_Bd_Perc_Share_Ind;
    private DbsField bda_Bd_Share_Perc;
    private DbsField bda_Bd_Share_Ntor;
    private DbsField bda_Bd_Share_Dtor;
    private DbsField bda_Bd_Irvcbl_Ind;
    private DbsField bda_Bd_Stlmnt_Rstrctn;
    private DbsField bda_Bd_Spcl_Txt1;
    private DbsField bda_Bd_Spcl_Txt2;
    private DbsField bda_Bd_Spcl_Txt3;
    private DbsField bda_Bd_Dflt_Estate;
    private DbsField bda_Bd_Mdo_Calc_Bene;
    private DbsField bda_Bd_Addr1;
    private DbsField bda_Bd_Addr2;
    private DbsField bda_Bd_Addr3_City;
    private DbsField bda_Bd_State;
    private DbsField bda_Bd_Zip;
    private DbsField bda_Bd_Phone;

    private DataAccessProgramView vw_bma;
    private DbsField bma_Bm_Pin;
    private DbsField bma_Bm_Tiaa_Cntrct;
    private DbsField bma_Bm_Cref_Cntrct;
    private DbsField bma_Bm_Tiaa_Cref_Ind;
    private DbsField bma_Bm_Stat;
    private DbsField bma_Bm_Rqst_Timestamp;
    private DbsField bma_Bm_Rcrd_Last_Updt_Dte;
    private DbsField bma_Bm_Rcrd_Last_Updt_Tme;
    private DbsField bma_Bm_Rcrd_Last_Updt_Userid;

    private DbsGroup bma_Bm_Mos_Txt_Group;
    private DbsField bma_Bm_Mos_Txt;
    private DbsField pnd_Bc_Pin_Cntrct_Ind_Key;

    private DbsGroup pnd_Bc_Pin_Cntrct_Ind_Key__R_Field_1;
    private DbsField pnd_Bc_Pin_Cntrct_Ind_Key_Pnd_Bc_Pin;
    private DbsField pnd_Bc_Pin_Cntrct_Ind_Key_Pnd_Bc_Tiaa_Cntrct;
    private DbsField pnd_Bc_Pin_Cntrct_Ind_Key_Pnd_Bc_Tiaa_Cref_Ind;

    private DbsGroup pnd_Details;
    private DbsField pnd_Details_Pnd_Add_Bc;
    private DbsField pnd_Details_Pnd_Add_Bd;
    private DbsField pnd_Details_Pnd_Add_Bm;
    private DbsField pnd_Details_Pnd_Dlt;
    private DbsField pnd_Details_Pnd_Error;
    private DbsField pnd_Details_Pnd_Et;
    private DbsField pnd_Details_Pnd_Found_Ben_Src;
    private DbsField pnd_Details_Pnd_Found_Ben_Tgt;
    private DbsField pnd_Details_Pnd_Isn_Bc;
    private DbsField pnd_Details_Pnd_Isn_Bc_Dlt;
    private DbsField pnd_Details_Pnd_Isn_Bd;
    private DbsField pnd_Details_Pnd_Isn_Bm;
    private DbsField pnd_Details_Pnd_Isn_Same_Dflt;
    private DbsField pnd_Details_Pnd_Last_Date;
    private DbsField pnd_Details_Pnd_Msg;
    private DbsField pnd_Details_Pnd_Post_1999;
    private DbsField pnd_Details_Pnd_Rldt;

    private DbsGroup pnd_Details__R_Field_2;
    private DbsField pnd_Details_Pnd_Date;
    private DbsField pnd_Details_Pnd_Time;
    private DbsField pnd_Details_Pnd_Same_Dflt;
    private DbsField pnd_Details_Pnd_Seq;

    private DbsGroup pnd_Totals;
    private DbsField pnd_Totals_Pnd_Total_Acis;
    private DbsField pnd_Totals_Pnd_Total_Copied;
    private DbsField pnd_Totals_Pnd_Total_Default;
    private DbsField pnd_Totals_Pnd_Total_Error;
    private DbsField pnd_Totals_Pnd_Total_Exist;
    private DbsField pnd_Totals_Pnd_Total_Same_Dflt;
    private DbsField pls_Trace;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Acis = localVariables.newGroupInRecord("pnd_Acis", "#ACIS");
        pnd_Acis_Pnd_I_Plan = pnd_Acis.newFieldInGroup("pnd_Acis_Pnd_I_Plan", "#I-PLAN", FieldType.STRING, 6);
        pnd_Acis_Pnd_I_Fill1 = pnd_Acis.newFieldInGroup("pnd_Acis_Pnd_I_Fill1", "#I-FILL1", FieldType.STRING, 1);
        pnd_Acis_Pnd_I_Pin = pnd_Acis.newFieldInGroup("pnd_Acis_Pnd_I_Pin", "#I-PIN", FieldType.NUMERIC, 12);
        pnd_Acis_Pnd_I_Fill2 = pnd_Acis.newFieldInGroup("pnd_Acis_Pnd_I_Fill2", "#I-FILL2", FieldType.STRING, 1);
        pnd_Acis_Pnd_I_Tiaa_Src = pnd_Acis.newFieldInGroup("pnd_Acis_Pnd_I_Tiaa_Src", "#I-TIAA-SRC", FieldType.STRING, 8);
        pnd_Acis_Pnd_I_Fill3 = pnd_Acis.newFieldInGroup("pnd_Acis_Pnd_I_Fill3", "#I-FILL3", FieldType.STRING, 1);
        pnd_Acis_Pnd_I_Cref_Src = pnd_Acis.newFieldInGroup("pnd_Acis_Pnd_I_Cref_Src", "#I-CREF-SRC", FieldType.STRING, 8);
        pnd_Acis_Pnd_I_Fill4 = pnd_Acis.newFieldInGroup("pnd_Acis_Pnd_I_Fill4", "#I-FILL4", FieldType.STRING, 1);
        pnd_Acis_Pnd_I_Tiaa_Tgt = pnd_Acis.newFieldInGroup("pnd_Acis_Pnd_I_Tiaa_Tgt", "#I-TIAA-TGT", FieldType.STRING, 8);
        pnd_Acis_Pnd_I_Fill5 = pnd_Acis.newFieldInGroup("pnd_Acis_Pnd_I_Fill5", "#I-FILL5", FieldType.STRING, 1);
        pnd_Acis_Pnd_I_Cref_Tgt = pnd_Acis.newFieldInGroup("pnd_Acis_Pnd_I_Cref_Tgt", "#I-CREF-TGT", FieldType.STRING, 8);
        pnd_Acis_Pnd_I_Fill6 = pnd_Acis.newFieldInGroup("pnd_Acis_Pnd_I_Fill6", "#I-FILL6", FieldType.STRING, 1);
        pnd_Acis_Pnd_I_Eff_Date = pnd_Acis.newFieldInGroup("pnd_Acis_Pnd_I_Eff_Date", "#I-EFF-DATE", FieldType.STRING, 8);

        vw_bc = new DataAccessProgramView(new NameInfo("vw_bc", "BC"), "BENE_CONTRACT_12", "BENE_CONTRACT");
        bc_Bc_Pin = vw_bc.getRecord().newFieldInGroup("bc_Bc_Pin", "BC-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BC_PIN");
        bc_Bc_Pin.setDdmHeader("PIN");
        bc_Bc_Tiaa_Cntrct = vw_bc.getRecord().newFieldInGroup("bc_Bc_Tiaa_Cntrct", "BC-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BC_TIAA_CNTRCT");
        bc_Bc_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bc_Bc_Cref_Cntrct = vw_bc.getRecord().newFieldInGroup("bc_Bc_Cref_Cntrct", "BC-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BC_CREF_CNTRCT");
        bc_Bc_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bc_Bc_Tiaa_Cref_Ind = vw_bc.getRecord().newFieldInGroup("bc_Bc_Tiaa_Cref_Ind", "BC-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_TIAA_CREF_IND");
        bc_Bc_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bc_Bc_Stat = vw_bc.getRecord().newFieldInGroup("bc_Bc_Stat", "BC-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_STAT");
        bc_Bc_Stat.setDdmHeader("STATUS");
        bc_Bc_Cntrct_Type = vw_bc.getRecord().newFieldInGroup("bc_Bc_Cntrct_Type", "BC-CNTRCT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_CNTRCT_TYPE");
        bc_Bc_Cntrct_Type.setDdmHeader("CNTRCT/TYPE");
        bc_Bc_Rqst_Timestamp = vw_bc.getRecord().newFieldInGroup("bc_Bc_Rqst_Timestamp", "BC-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BC_RQST_TIMESTAMP");
        bc_Bc_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        bc_Bc_Eff_Dte = vw_bc.getRecord().newFieldInGroup("bc_Bc_Eff_Dte", "BC-EFF-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "BC_EFF_DTE");
        bc_Bc_Eff_Dte.setDdmHeader("EFFECTIVE/DATE");
        bc_Bc_Mos_Ind = vw_bc.getRecord().newFieldInGroup("bc_Bc_Mos_Ind", "BC-MOS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_MOS_IND");
        bc_Bc_Mos_Ind.setDdmHeader("MOS/IND");
        bc_Bc_Irvcbl_Ind = vw_bc.getRecord().newFieldInGroup("bc_Bc_Irvcbl_Ind", "BC-IRVCBL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_IRVCBL_IND");
        bc_Bc_Irvcbl_Ind.setDdmHeader("IRVCBL/IND");
        bc_Bc_Pymnt_Chld_Dcsd_Ind = vw_bc.getRecord().newFieldInGroup("bc_Bc_Pymnt_Chld_Dcsd_Ind", "BC-PYMNT-CHLD-DCSD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_PYMNT_CHLD_DCSD_IND");
        bc_Bc_Pymnt_Chld_Dcsd_Ind.setDdmHeader("PYMNT/CHILD/DCSD");
        bc_Bc_Exempt_Spouse_Rights = vw_bc.getRecord().newFieldInGroup("bc_Bc_Exempt_Spouse_Rights", "BC-EXEMPT-SPOUSE-RIGHTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_EXEMPT_SPOUSE_RIGHTS");
        bc_Bc_Exempt_Spouse_Rights.setDdmHeader("EXEMPT/SPOUSAL/RIGHTS");
        bc_Bc_Spouse_Waived_Bnfts = vw_bc.getRecord().newFieldInGroup("bc_Bc_Spouse_Waived_Bnfts", "BC-SPOUSE-WAIVED-BNFTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_SPOUSE_WAIVED_BNFTS");
        bc_Bc_Spouse_Waived_Bnfts.setDdmHeader("SPOUSE/WAIVED/BENEFITS");
        bc_Bc_Trust_Data_Fldr = vw_bc.getRecord().newFieldInGroup("bc_Bc_Trust_Data_Fldr", "BC-TRUST-DATA-FLDR", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_TRUST_DATA_FLDR");
        bc_Bc_Trust_Data_Fldr.setDdmHeader("TRUST/DATA/IN FLDR");
        bc_Bc_Bene_Addr_Fldr = vw_bc.getRecord().newFieldInGroup("bc_Bc_Bene_Addr_Fldr", "BC-BENE-ADDR-FLDR", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_BENE_ADDR_FLDR");
        bc_Bc_Bene_Addr_Fldr.setDdmHeader("BENE/ADDRESS/IN FLDR");
        bc_Bc_Co_Owner_Data_Fldr = vw_bc.getRecord().newFieldInGroup("bc_Bc_Co_Owner_Data_Fldr", "BC-CO-OWNER-DATA-FLDR", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_CO_OWNER_DATA_FLDR");
        bc_Bc_Co_Owner_Data_Fldr.setDdmHeader("CO-OWNER/DATA FLDR");
        bc_Bc_Fldr_Min = vw_bc.getRecord().newFieldInGroup("bc_Bc_Fldr_Min", "BC-FLDR-MIN", FieldType.STRING, 11, RepeatingFieldStrategy.None, "BC_FLDR_MIN");
        bc_Bc_Fldr_Min.setDdmHeader("FOLDER/MIN");
        bc_Bc_Fldr_Srce_Id = vw_bc.getRecord().newFieldInGroup("bc_Bc_Fldr_Srce_Id", "BC-FLDR-SRCE-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "BC_FLDR_SRCE_ID");
        bc_Bc_Fldr_Srce_Id.setDdmHeader("FOLDER/SOURCE/ID");
        bc_Bc_Fldr_Log_Dte_Tme = vw_bc.getRecord().newFieldInGroup("bc_Bc_Fldr_Log_Dte_Tme", "BC-FLDR-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BC_FLDR_LOG_DTE_TME");
        bc_Bc_Fldr_Log_Dte_Tme.setDdmHeader("FOLDER/LOG DATE/AND TIME");
        bc_Bc_Rcrd_Last_Updt_Dte = vw_bc.getRecord().newFieldInGroup("bc_Bc_Rcrd_Last_Updt_Dte", "BC-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_RCRD_LAST_UPDT_DTE");
        bc_Bc_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bc_Bc_Rcrd_Last_Updt_Tme = vw_bc.getRecord().newFieldInGroup("bc_Bc_Rcrd_Last_Updt_Tme", "BC-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_RCRD_LAST_UPDT_TME");
        bc_Bc_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bc_Bc_Rcrd_Last_Updt_Userid = vw_bc.getRecord().newFieldInGroup("bc_Bc_Rcrd_Last_Updt_Userid", "BC-RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "BC_RCRD_LAST_UPDT_USERID");
        bc_Bc_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        bc_Bc_Last_Dsgntn_Srce = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Dsgntn_Srce", "BC-LAST-DSGNTN-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_SRCE");
        bc_Bc_Last_Dsgntn_Srce.setDdmHeader("LAST/DSGNTN/SOURCE");
        bc_Bc_Last_Dsgntn_System = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Dsgntn_System", "BC-LAST-DSGNTN-SYSTEM", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_SYSTEM");
        bc_Bc_Last_Dsgntn_System.setDdmHeader("LAST/DSGNTN/SYSTEM");
        bc_Bc_Last_Dsgntn_Userid = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Dsgntn_Userid", "BC-LAST-DSGNTN-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_USERID");
        bc_Bc_Last_Dsgntn_Userid.setDdmHeader("LAST/DSGNTN/USERID");
        bc_Bc_Last_Dsgntn_Dte = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Dsgntn_Dte", "BC-LAST-DSGNTN-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_DTE");
        bc_Bc_Last_Dsgntn_Dte.setDdmHeader("LAST/DSGNTN/DATE");
        bc_Bc_Last_Dsgntn_Tme = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Dsgntn_Tme", "BC-LAST-DSGNTN-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_TME");
        bc_Bc_Last_Dsgntn_Tme.setDdmHeader("LAST/DSGNTN/TIME");
        bc_Bc_Last_Vrfy_Dte = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Vrfy_Dte", "BC-LAST-VRFY-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_VRFY_DTE");
        bc_Bc_Last_Vrfy_Dte.setDdmHeader("LAST/VRFY/DATE");
        bc_Bc_Last_Vrfy_Tme = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Vrfy_Tme", "BC-LAST-VRFY-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_LAST_VRFY_TME");
        bc_Bc_Last_Vrfy_Tme.setDdmHeader("LAST/VRFY/TIME");
        bc_Bc_Last_Vrfy_Userid = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Vrfy_Userid", "BC-LAST-VRFY-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_VRFY_USERID");
        bc_Bc_Last_Vrfy_Userid.setDdmHeader("LAST/VRFY/USERID");
        bc_Bc_Tiaa_Cref_Chng_Dte = vw_bc.getRecord().newFieldInGroup("bc_Bc_Tiaa_Cref_Chng_Dte", "BC-TIAA-CREF-CHNG-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_TIAA_CREF_CHNG_DTE");
        bc_Bc_Tiaa_Cref_Chng_Dte.setDdmHeader("TIAA CREF/CHANGE/DATE");
        bc_Bc_Tiaa_Cref_Chng_Tme = vw_bc.getRecord().newFieldInGroup("bc_Bc_Tiaa_Cref_Chng_Tme", "BC-TIAA-CREF-CHNG-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_TIAA_CREF_CHNG_TME");
        bc_Bc_Tiaa_Cref_Chng_Tme.setDdmHeader("TIAA CREF/CHANGE/TIME");
        bc_Bc_Chng_Pwr_Atty = vw_bc.getRecord().newFieldInGroup("bc_Bc_Chng_Pwr_Atty", "BC-CHNG-PWR-ATTY", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_CHNG_PWR_ATTY");
        bc_Bc_Chng_Pwr_Atty.setDdmHeader("CHANGE/POWER/ATTNY");
        bc_Bc_Mgrtn_Cnfrm_Stmnt_Prod_Dte = vw_bc.getRecord().newFieldInGroup("bc_Bc_Mgrtn_Cnfrm_Stmnt_Prod_Dte", "BC-MGRTN-CNFRM-STMNT-PROD-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "BC_MGRTN_CNFRM_STMNT_PROD_DTE");
        bc_Bc_Mgrtn_Cnfrm_Stmnt_Prod_Dte.setDdmHeader("MGRTN CONFIRM/STATEMENT/PROD DATE");
        registerRecord(vw_bc);

        vw_bd = new DataAccessProgramView(new NameInfo("vw_bd", "BD"), "BENE_DESIGNATION_12", "BENE_DESIGNATION");
        bd_Bd_Pin = vw_bd.getRecord().newFieldInGroup("bd_Bd_Pin", "BD-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BD_PIN");
        bd_Bd_Pin.setDdmHeader("PIN");
        bd_Bd_Tiaa_Cntrct = vw_bd.getRecord().newFieldInGroup("bd_Bd_Tiaa_Cntrct", "BD-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BD_TIAA_CNTRCT");
        bd_Bd_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bd_Bd_Cref_Cntrct = vw_bd.getRecord().newFieldInGroup("bd_Bd_Cref_Cntrct", "BD-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BD_CREF_CNTRCT");
        bd_Bd_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bd_Bd_Tiaa_Cref_Ind = vw_bd.getRecord().newFieldInGroup("bd_Bd_Tiaa_Cref_Ind", "BD-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_TIAA_CREF_IND");
        bd_Bd_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bd_Bd_Stat = vw_bd.getRecord().newFieldInGroup("bd_Bd_Stat", "BD-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_STAT");
        bd_Bd_Stat.setDdmHeader("STAT");
        bd_Bd_Seq_Nmbr = vw_bd.getRecord().newFieldInGroup("bd_Bd_Seq_Nmbr", "BD-SEQ-NMBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "BD_SEQ_NMBR");
        bd_Bd_Seq_Nmbr.setDdmHeader(" SEQ/NMBR");
        bd_Bd_Rqst_Timestamp = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rqst_Timestamp", "BD-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BD_RQST_TIMESTAMP");
        bd_Bd_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        bd_Bd_Rcrd_Last_Updt_Dte = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rcrd_Last_Updt_Dte", "BD-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BD_RCRD_LAST_UPDT_DTE");
        bd_Bd_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bd_Bd_Rcrd_Last_Updt_Tme = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rcrd_Last_Updt_Tme", "BD-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BD_RCRD_LAST_UPDT_TME");
        bd_Bd_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bd_Bd_Rcrd_Last_Updt_Userid = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rcrd_Last_Updt_Userid", "BD-RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "BD_RCRD_LAST_UPDT_USERID");
        bd_Bd_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        bd_Bd_Bene_Type = vw_bd.getRecord().newFieldInGroup("bd_Bd_Bene_Type", "BD-BENE-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_BENE_TYPE");
        bd_Bd_Bene_Type.setDdmHeader("BENE/TYPE");
        bd_Bd_Rltn_Cd = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rltn_Cd", "BD-RLTN-CD", FieldType.STRING, 2, RepeatingFieldStrategy.None, "BD_RLTN_CD");
        bd_Bd_Rltn_Cd.setDdmHeader("RELATION/CODE");
        bd_Bd_Bene_Name1 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Bene_Name1", "BD-BENE-NAME1", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BD_BENE_NAME1");
        bd_Bd_Bene_Name1.setDdmHeader("BENEFICIARY/NAME 1");
        bd_Bd_Bene_Name2 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Bene_Name2", "BD-BENE-NAME2", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BD_BENE_NAME2");
        bd_Bd_Bene_Name2.setDdmHeader("BENEFICIARY/NAME 2");
        bd_Bd_Rltn_Free_Txt = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rltn_Free_Txt", "BD-RLTN-FREE-TXT", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BD_RLTN_FREE_TXT");
        bd_Bd_Rltn_Free_Txt.setDdmHeader("OTHER/RELATION");
        bd_Bd_Dte_Birth_Trust = vw_bd.getRecord().newFieldInGroup("bd_Bd_Dte_Birth_Trust", "BD-DTE-BIRTH-TRUST", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BD_DTE_BIRTH_TRUST");
        bd_Bd_Dte_Birth_Trust.setDdmHeader("DOB/DOT");
        bd_Bd_Ss_Cd = vw_bd.getRecord().newFieldInGroup("bd_Bd_Ss_Cd", "BD-SS-CD", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_SS_CD");
        bd_Bd_Ss_Cd.setDdmHeader("SS/CODE");
        bd_Bd_Ss_Nmbr = vw_bd.getRecord().newFieldInGroup("bd_Bd_Ss_Nmbr", "BD-SS-NMBR", FieldType.STRING, 9, RepeatingFieldStrategy.None, "BD_SS_NMBR");
        bd_Bd_Ss_Nmbr.setDdmHeader("SS/NUMBER");
        bd_Bd_Perc_Share_Ind = vw_bd.getRecord().newFieldInGroup("bd_Bd_Perc_Share_Ind", "BD-PERC-SHARE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_PERC_SHARE_IND");
        bd_Bd_Perc_Share_Ind.setDdmHeader("PERCNT/SHARE/IND");
        bd_Bd_Share_Perc = vw_bd.getRecord().newFieldInGroup("bd_Bd_Share_Perc", "BD-SHARE-PERC", FieldType.NUMERIC, 5, 2, RepeatingFieldStrategy.None, 
            "BD_SHARE_PERC");
        bd_Bd_Share_Perc.setDdmHeader("SHARE/PERCENT");
        bd_Bd_Share_Ntor = vw_bd.getRecord().newFieldInGroup("bd_Bd_Share_Ntor", "BD-SHARE-NTOR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "BD_SHARE_NTOR");
        bd_Bd_Share_Ntor.setDdmHeader("SHARE/NUMERATOR");
        bd_Bd_Share_Dtor = vw_bd.getRecord().newFieldInGroup("bd_Bd_Share_Dtor", "BD-SHARE-DTOR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "BD_SHARE_DTOR");
        bd_Bd_Share_Dtor.setDdmHeader("SHARE/DENOMINATOR");
        bd_Bd_Irvcbl_Ind = vw_bd.getRecord().newFieldInGroup("bd_Bd_Irvcbl_Ind", "BD-IRVCBL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_IRVCBL_IND");
        bd_Bd_Irvcbl_Ind.setDdmHeader("IRVCBL/IND");
        bd_Bd_Stlmnt_Rstrctn = vw_bd.getRecord().newFieldInGroup("bd_Bd_Stlmnt_Rstrctn", "BD-STLMNT-RSTRCTN", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_STLMNT_RSTRCTN");
        bd_Bd_Stlmnt_Rstrctn.setDdmHeader("SETTLEMENT/RESTRICTION");
        bd_Bd_Spcl_Txt1 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Spcl_Txt1", "BD-SPCL-TXT1", FieldType.STRING, 72, RepeatingFieldStrategy.None, "BD_SPCL_TXT1");
        bd_Bd_Spcl_Txt1.setDdmHeader("SPECIAL TEXT 1");
        bd_Bd_Spcl_Txt2 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Spcl_Txt2", "BD-SPCL-TXT2", FieldType.STRING, 72, RepeatingFieldStrategy.None, "BD_SPCL_TXT2");
        bd_Bd_Spcl_Txt2.setDdmHeader("SPECIAL TEXT 2");
        bd_Bd_Spcl_Txt3 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Spcl_Txt3", "BD-SPCL-TXT3", FieldType.STRING, 72, RepeatingFieldStrategy.None, "BD_SPCL_TXT3");
        bd_Bd_Spcl_Txt3.setDdmHeader("SPECIAL TEXT 3");
        bd_Bd_Dflt_Estate = vw_bd.getRecord().newFieldInGroup("bd_Bd_Dflt_Estate", "BD-DFLT-ESTATE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_DFLT_ESTATE");
        bd_Bd_Dflt_Estate.setDdmHeader("DEFAULT/ESTATE");
        bd_Bd_Mdo_Calc_Bene = vw_bd.getRecord().newFieldInGroup("bd_Bd_Mdo_Calc_Bene", "BD-MDO-CALC-BENE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_MDO_CALC_BENE");
        bd_Bd_Mdo_Calc_Bene.setDdmHeader("MDO/CALC/BENE");
        bd_Bd_Addr1 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Addr1", "BD-ADDR1", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BD_ADDR1");
        bd_Bd_Addr2 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Addr2", "BD-ADDR2", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BD_ADDR2");
        bd_Bd_Addr3_City = vw_bd.getRecord().newFieldInGroup("bd_Bd_Addr3_City", "BD-ADDR3-CITY", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BD_ADDR3_CITY");
        bd_Bd_State = vw_bd.getRecord().newFieldInGroup("bd_Bd_State", "BD-STATE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "BD_STATE");
        bd_Bd_Zip = vw_bd.getRecord().newFieldInGroup("bd_Bd_Zip", "BD-ZIP", FieldType.STRING, 10, RepeatingFieldStrategy.None, "BD_ZIP");
        bd_Bd_Phone = vw_bd.getRecord().newFieldInGroup("bd_Bd_Phone", "BD-PHONE", FieldType.STRING, 20, RepeatingFieldStrategy.None, "BD_PHONE");
        registerRecord(vw_bd);

        vw_bm = new DataAccessProgramView(new NameInfo("vw_bm", "BM"), "BENE_MOS_12", "BENE_MOS", DdmPeriodicGroups.getInstance().getGroups("BENE_MOS_12"));
        bm_Bm_Pin = vw_bm.getRecord().newFieldInGroup("bm_Bm_Pin", "BM-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BM_PIN");
        bm_Bm_Pin.setDdmHeader("PIN");
        bm_Bm_Tiaa_Cntrct = vw_bm.getRecord().newFieldInGroup("bm_Bm_Tiaa_Cntrct", "BM-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BM_TIAA_CNTRCT");
        bm_Bm_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bm_Bm_Cref_Cntrct = vw_bm.getRecord().newFieldInGroup("bm_Bm_Cref_Cntrct", "BM-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BM_CREF_CNTRCT");
        bm_Bm_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bm_Bm_Tiaa_Cref_Ind = vw_bm.getRecord().newFieldInGroup("bm_Bm_Tiaa_Cref_Ind", "BM-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BM_TIAA_CREF_IND");
        bm_Bm_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bm_Bm_Stat = vw_bm.getRecord().newFieldInGroup("bm_Bm_Stat", "BM-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BM_STAT");
        bm_Bm_Stat.setDdmHeader("STAT");
        bm_Bm_Rqst_Timestamp = vw_bm.getRecord().newFieldInGroup("bm_Bm_Rqst_Timestamp", "BM-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BM_RQST_TIMESTAMP");
        bm_Bm_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        bm_Bm_Rcrd_Last_Updt_Dte = vw_bm.getRecord().newFieldInGroup("bm_Bm_Rcrd_Last_Updt_Dte", "BM-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BM_RCRD_LAST_UPDT_DTE");
        bm_Bm_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bm_Bm_Rcrd_Last_Updt_Tme = vw_bm.getRecord().newFieldInGroup("bm_Bm_Rcrd_Last_Updt_Tme", "BM-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BM_RCRD_LAST_UPDT_TME");
        bm_Bm_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bm_Bm_Rcrd_Last_Updt_Userid = vw_bm.getRecord().newFieldInGroup("bm_Bm_Rcrd_Last_Updt_Userid", "BM-RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "BM_RCRD_LAST_UPDT_USERID");
        bm_Bm_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");

        bm_Bm_Mos_Txt_Group = vw_bm.getRecord().newGroupInGroup("bm_Bm_Mos_Txt_Group", "BM-MOS-TXT-GROUP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "BENE_MOS_BM_MOS_TXT_GROUP");
        bm_Bm_Mos_Txt = bm_Bm_Mos_Txt_Group.newFieldArrayInGroup("bm_Bm_Mos_Txt", "BM-MOS-TXT", FieldType.STRING, 72, new DbsArrayController(1, 60) , 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "BM_MOS_TXT", "BENE_MOS_BM_MOS_TXT_GROUP");
        bm_Bm_Mos_Txt.setDdmHeader("MOS TEXT");
        registerRecord(vw_bm);

        vw_bca = new DataAccessProgramView(new NameInfo("vw_bca", "BCA"), "BENE_CONTRACT_12", "BENE_CONTRACT");
        bca_Bc_Pin = vw_bca.getRecord().newFieldInGroup("bca_Bc_Pin", "BC-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BC_PIN");
        bca_Bc_Pin.setDdmHeader("PIN");
        bca_Bc_Tiaa_Cntrct = vw_bca.getRecord().newFieldInGroup("bca_Bc_Tiaa_Cntrct", "BC-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BC_TIAA_CNTRCT");
        bca_Bc_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bca_Bc_Cref_Cntrct = vw_bca.getRecord().newFieldInGroup("bca_Bc_Cref_Cntrct", "BC-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BC_CREF_CNTRCT");
        bca_Bc_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bca_Bc_Tiaa_Cref_Ind = vw_bca.getRecord().newFieldInGroup("bca_Bc_Tiaa_Cref_Ind", "BC-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_TIAA_CREF_IND");
        bca_Bc_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bca_Bc_Stat = vw_bca.getRecord().newFieldInGroup("bca_Bc_Stat", "BC-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_STAT");
        bca_Bc_Stat.setDdmHeader("STATUS");
        bca_Bc_Cntrct_Type = vw_bca.getRecord().newFieldInGroup("bca_Bc_Cntrct_Type", "BC-CNTRCT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_CNTRCT_TYPE");
        bca_Bc_Cntrct_Type.setDdmHeader("CNTRCT/TYPE");
        bca_Bc_Rqst_Timestamp = vw_bca.getRecord().newFieldInGroup("bca_Bc_Rqst_Timestamp", "BC-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BC_RQST_TIMESTAMP");
        bca_Bc_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        bca_Bc_Eff_Dte = vw_bca.getRecord().newFieldInGroup("bca_Bc_Eff_Dte", "BC-EFF-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "BC_EFF_DTE");
        bca_Bc_Eff_Dte.setDdmHeader("EFFECTIVE/DATE");
        bca_Bc_Mos_Ind = vw_bca.getRecord().newFieldInGroup("bca_Bc_Mos_Ind", "BC-MOS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_MOS_IND");
        bca_Bc_Mos_Ind.setDdmHeader("MOS/IND");
        bca_Bc_Irvcbl_Ind = vw_bca.getRecord().newFieldInGroup("bca_Bc_Irvcbl_Ind", "BC-IRVCBL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_IRVCBL_IND");
        bca_Bc_Irvcbl_Ind.setDdmHeader("IRVCBL/IND");
        bca_Bc_Pymnt_Chld_Dcsd_Ind = vw_bca.getRecord().newFieldInGroup("bca_Bc_Pymnt_Chld_Dcsd_Ind", "BC-PYMNT-CHLD-DCSD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_PYMNT_CHLD_DCSD_IND");
        bca_Bc_Pymnt_Chld_Dcsd_Ind.setDdmHeader("PYMNT/CHILD/DCSD");
        bca_Bc_Exempt_Spouse_Rights = vw_bca.getRecord().newFieldInGroup("bca_Bc_Exempt_Spouse_Rights", "BC-EXEMPT-SPOUSE-RIGHTS", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "BC_EXEMPT_SPOUSE_RIGHTS");
        bca_Bc_Exempt_Spouse_Rights.setDdmHeader("EXEMPT/SPOUSAL/RIGHTS");
        bca_Bc_Spouse_Waived_Bnfts = vw_bca.getRecord().newFieldInGroup("bca_Bc_Spouse_Waived_Bnfts", "BC-SPOUSE-WAIVED-BNFTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_SPOUSE_WAIVED_BNFTS");
        bca_Bc_Spouse_Waived_Bnfts.setDdmHeader("SPOUSE/WAIVED/BENEFITS");
        bca_Bc_Trust_Data_Fldr = vw_bca.getRecord().newFieldInGroup("bca_Bc_Trust_Data_Fldr", "BC-TRUST-DATA-FLDR", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_TRUST_DATA_FLDR");
        bca_Bc_Trust_Data_Fldr.setDdmHeader("TRUST/DATA/IN FLDR");
        bca_Bc_Bene_Addr_Fldr = vw_bca.getRecord().newFieldInGroup("bca_Bc_Bene_Addr_Fldr", "BC-BENE-ADDR-FLDR", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_BENE_ADDR_FLDR");
        bca_Bc_Bene_Addr_Fldr.setDdmHeader("BENE/ADDRESS/IN FLDR");
        bca_Bc_Co_Owner_Data_Fldr = vw_bca.getRecord().newFieldInGroup("bca_Bc_Co_Owner_Data_Fldr", "BC-CO-OWNER-DATA-FLDR", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_CO_OWNER_DATA_FLDR");
        bca_Bc_Co_Owner_Data_Fldr.setDdmHeader("CO-OWNER/DATA FLDR");
        bca_Bc_Fldr_Min = vw_bca.getRecord().newFieldInGroup("bca_Bc_Fldr_Min", "BC-FLDR-MIN", FieldType.STRING, 11, RepeatingFieldStrategy.None, "BC_FLDR_MIN");
        bca_Bc_Fldr_Min.setDdmHeader("FOLDER/MIN");
        bca_Bc_Fldr_Srce_Id = vw_bca.getRecord().newFieldInGroup("bca_Bc_Fldr_Srce_Id", "BC-FLDR-SRCE-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "BC_FLDR_SRCE_ID");
        bca_Bc_Fldr_Srce_Id.setDdmHeader("FOLDER/SOURCE/ID");
        bca_Bc_Fldr_Log_Dte_Tme = vw_bca.getRecord().newFieldInGroup("bca_Bc_Fldr_Log_Dte_Tme", "BC-FLDR-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BC_FLDR_LOG_DTE_TME");
        bca_Bc_Fldr_Log_Dte_Tme.setDdmHeader("FOLDER/LOG DATE/AND TIME");
        bca_Bc_Rcrd_Last_Updt_Dte = vw_bca.getRecord().newFieldInGroup("bca_Bc_Rcrd_Last_Updt_Dte", "BC-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_RCRD_LAST_UPDT_DTE");
        bca_Bc_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bca_Bc_Rcrd_Last_Updt_Tme = vw_bca.getRecord().newFieldInGroup("bca_Bc_Rcrd_Last_Updt_Tme", "BC-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_RCRD_LAST_UPDT_TME");
        bca_Bc_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bca_Bc_Rcrd_Last_Updt_Userid = vw_bca.getRecord().newFieldInGroup("bca_Bc_Rcrd_Last_Updt_Userid", "BC-RCRD-LAST-UPDT-USERID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "BC_RCRD_LAST_UPDT_USERID");
        bca_Bc_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        bca_Bc_Last_Dsgntn_Srce = vw_bca.getRecord().newFieldInGroup("bca_Bc_Last_Dsgntn_Srce", "BC-LAST-DSGNTN-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_SRCE");
        bca_Bc_Last_Dsgntn_Srce.setDdmHeader("LAST/DSGNTN/SOURCE");
        bca_Bc_Last_Dsgntn_System = vw_bca.getRecord().newFieldInGroup("bca_Bc_Last_Dsgntn_System", "BC-LAST-DSGNTN-SYSTEM", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_SYSTEM");
        bca_Bc_Last_Dsgntn_System.setDdmHeader("LAST/DSGNTN/SYSTEM");
        bca_Bc_Last_Dsgntn_Userid = vw_bca.getRecord().newFieldInGroup("bca_Bc_Last_Dsgntn_Userid", "BC-LAST-DSGNTN-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_USERID");
        bca_Bc_Last_Dsgntn_Userid.setDdmHeader("LAST/DSGNTN/USERID");
        bca_Bc_Last_Dsgntn_Dte = vw_bca.getRecord().newFieldInGroup("bca_Bc_Last_Dsgntn_Dte", "BC-LAST-DSGNTN-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_DTE");
        bca_Bc_Last_Dsgntn_Dte.setDdmHeader("LAST/DSGNTN/DATE");
        bca_Bc_Last_Dsgntn_Tme = vw_bca.getRecord().newFieldInGroup("bca_Bc_Last_Dsgntn_Tme", "BC-LAST-DSGNTN-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_TME");
        bca_Bc_Last_Dsgntn_Tme.setDdmHeader("LAST/DSGNTN/TIME");
        bca_Bc_Chng_Pwr_Atty = vw_bca.getRecord().newFieldInGroup("bca_Bc_Chng_Pwr_Atty", "BC-CHNG-PWR-ATTY", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_CHNG_PWR_ATTY");
        bca_Bc_Chng_Pwr_Atty.setDdmHeader("CHANGE/POWER/ATTNY");
        bca_Bc_Mgrtn_Cnfrm_Stmnt_Prod_Dte = vw_bca.getRecord().newFieldInGroup("bca_Bc_Mgrtn_Cnfrm_Stmnt_Prod_Dte", "BC-MGRTN-CNFRM-STMNT-PROD-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "BC_MGRTN_CNFRM_STMNT_PROD_DTE");
        bca_Bc_Mgrtn_Cnfrm_Stmnt_Prod_Dte.setDdmHeader("MGRTN CONFIRM/STATEMENT/PROD DATE");
        registerRecord(vw_bca);

        vw_bda = new DataAccessProgramView(new NameInfo("vw_bda", "BDA"), "BENE_DESIGNATION_12", "BENE_DESIGNATION");
        bda_Bd_Pin = vw_bda.getRecord().newFieldInGroup("bda_Bd_Pin", "BD-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BD_PIN");
        bda_Bd_Pin.setDdmHeader("PIN");
        bda_Bd_Tiaa_Cntrct = vw_bda.getRecord().newFieldInGroup("bda_Bd_Tiaa_Cntrct", "BD-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BD_TIAA_CNTRCT");
        bda_Bd_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bda_Bd_Cref_Cntrct = vw_bda.getRecord().newFieldInGroup("bda_Bd_Cref_Cntrct", "BD-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BD_CREF_CNTRCT");
        bda_Bd_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bda_Bd_Tiaa_Cref_Ind = vw_bda.getRecord().newFieldInGroup("bda_Bd_Tiaa_Cref_Ind", "BD-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_TIAA_CREF_IND");
        bda_Bd_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bda_Bd_Stat = vw_bda.getRecord().newFieldInGroup("bda_Bd_Stat", "BD-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_STAT");
        bda_Bd_Stat.setDdmHeader("STAT");
        bda_Bd_Seq_Nmbr = vw_bda.getRecord().newFieldInGroup("bda_Bd_Seq_Nmbr", "BD-SEQ-NMBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "BD_SEQ_NMBR");
        bda_Bd_Seq_Nmbr.setDdmHeader(" SEQ/NMBR");
        bda_Bd_Rqst_Timestamp = vw_bda.getRecord().newFieldInGroup("bda_Bd_Rqst_Timestamp", "BD-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BD_RQST_TIMESTAMP");
        bda_Bd_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        bda_Bd_Rcrd_Last_Updt_Dte = vw_bda.getRecord().newFieldInGroup("bda_Bd_Rcrd_Last_Updt_Dte", "BD-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BD_RCRD_LAST_UPDT_DTE");
        bda_Bd_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bda_Bd_Rcrd_Last_Updt_Tme = vw_bda.getRecord().newFieldInGroup("bda_Bd_Rcrd_Last_Updt_Tme", "BD-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BD_RCRD_LAST_UPDT_TME");
        bda_Bd_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bda_Bd_Rcrd_Last_Updt_Userid = vw_bda.getRecord().newFieldInGroup("bda_Bd_Rcrd_Last_Updt_Userid", "BD-RCRD-LAST-UPDT-USERID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "BD_RCRD_LAST_UPDT_USERID");
        bda_Bd_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        bda_Bd_Bene_Type = vw_bda.getRecord().newFieldInGroup("bda_Bd_Bene_Type", "BD-BENE-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_BENE_TYPE");
        bda_Bd_Bene_Type.setDdmHeader("BENE/TYPE");
        bda_Bd_Rltn_Cd = vw_bda.getRecord().newFieldInGroup("bda_Bd_Rltn_Cd", "BD-RLTN-CD", FieldType.STRING, 2, RepeatingFieldStrategy.None, "BD_RLTN_CD");
        bda_Bd_Rltn_Cd.setDdmHeader("RELATION/CODE");
        bda_Bd_Bene_Name1 = vw_bda.getRecord().newFieldInGroup("bda_Bd_Bene_Name1", "BD-BENE-NAME1", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "BD_BENE_NAME1");
        bda_Bd_Bene_Name1.setDdmHeader("BENEFICIARY/NAME 1");
        bda_Bd_Bene_Name2 = vw_bda.getRecord().newFieldInGroup("bda_Bd_Bene_Name2", "BD-BENE-NAME2", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "BD_BENE_NAME2");
        bda_Bd_Bene_Name2.setDdmHeader("BENEFICIARY/NAME 2");
        bda_Bd_Rltn_Free_Txt = vw_bda.getRecord().newFieldInGroup("bda_Bd_Rltn_Free_Txt", "BD-RLTN-FREE-TXT", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BD_RLTN_FREE_TXT");
        bda_Bd_Rltn_Free_Txt.setDdmHeader("OTHER/RELATION");
        bda_Bd_Dte_Birth_Trust = vw_bda.getRecord().newFieldInGroup("bda_Bd_Dte_Birth_Trust", "BD-DTE-BIRTH-TRUST", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BD_DTE_BIRTH_TRUST");
        bda_Bd_Dte_Birth_Trust.setDdmHeader("DOB/DOT");
        bda_Bd_Ss_Cd = vw_bda.getRecord().newFieldInGroup("bda_Bd_Ss_Cd", "BD-SS-CD", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_SS_CD");
        bda_Bd_Ss_Cd.setDdmHeader("SS/CODE");
        bda_Bd_Ss_Nmbr = vw_bda.getRecord().newFieldInGroup("bda_Bd_Ss_Nmbr", "BD-SS-NMBR", FieldType.STRING, 9, RepeatingFieldStrategy.None, "BD_SS_NMBR");
        bda_Bd_Ss_Nmbr.setDdmHeader("SS/NUMBER");
        bda_Bd_Perc_Share_Ind = vw_bda.getRecord().newFieldInGroup("bda_Bd_Perc_Share_Ind", "BD-PERC-SHARE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_PERC_SHARE_IND");
        bda_Bd_Perc_Share_Ind.setDdmHeader("PERCNT/SHARE/IND");
        bda_Bd_Share_Perc = vw_bda.getRecord().newFieldInGroup("bda_Bd_Share_Perc", "BD-SHARE-PERC", FieldType.NUMERIC, 5, 2, RepeatingFieldStrategy.None, 
            "BD_SHARE_PERC");
        bda_Bd_Share_Perc.setDdmHeader("SHARE/PERCENT");
        bda_Bd_Share_Ntor = vw_bda.getRecord().newFieldInGroup("bda_Bd_Share_Ntor", "BD-SHARE-NTOR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "BD_SHARE_NTOR");
        bda_Bd_Share_Ntor.setDdmHeader("SHARE/NUMERATOR");
        bda_Bd_Share_Dtor = vw_bda.getRecord().newFieldInGroup("bda_Bd_Share_Dtor", "BD-SHARE-DTOR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "BD_SHARE_DTOR");
        bda_Bd_Share_Dtor.setDdmHeader("SHARE/DENOMINATOR");
        bda_Bd_Irvcbl_Ind = vw_bda.getRecord().newFieldInGroup("bda_Bd_Irvcbl_Ind", "BD-IRVCBL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_IRVCBL_IND");
        bda_Bd_Irvcbl_Ind.setDdmHeader("IRVCBL/IND");
        bda_Bd_Stlmnt_Rstrctn = vw_bda.getRecord().newFieldInGroup("bda_Bd_Stlmnt_Rstrctn", "BD-STLMNT-RSTRCTN", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_STLMNT_RSTRCTN");
        bda_Bd_Stlmnt_Rstrctn.setDdmHeader("SETTLEMENT/RESTRICTION");
        bda_Bd_Spcl_Txt1 = vw_bda.getRecord().newFieldInGroup("bda_Bd_Spcl_Txt1", "BD-SPCL-TXT1", FieldType.STRING, 72, RepeatingFieldStrategy.None, "BD_SPCL_TXT1");
        bda_Bd_Spcl_Txt1.setDdmHeader("SPECIAL TEXT 1");
        bda_Bd_Spcl_Txt2 = vw_bda.getRecord().newFieldInGroup("bda_Bd_Spcl_Txt2", "BD-SPCL-TXT2", FieldType.STRING, 72, RepeatingFieldStrategy.None, "BD_SPCL_TXT2");
        bda_Bd_Spcl_Txt2.setDdmHeader("SPECIAL TEXT 2");
        bda_Bd_Spcl_Txt3 = vw_bda.getRecord().newFieldInGroup("bda_Bd_Spcl_Txt3", "BD-SPCL-TXT3", FieldType.STRING, 72, RepeatingFieldStrategy.None, "BD_SPCL_TXT3");
        bda_Bd_Spcl_Txt3.setDdmHeader("SPECIAL TEXT 3");
        bda_Bd_Dflt_Estate = vw_bda.getRecord().newFieldInGroup("bda_Bd_Dflt_Estate", "BD-DFLT-ESTATE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_DFLT_ESTATE");
        bda_Bd_Dflt_Estate.setDdmHeader("DEFAULT/ESTATE");
        bda_Bd_Mdo_Calc_Bene = vw_bda.getRecord().newFieldInGroup("bda_Bd_Mdo_Calc_Bene", "BD-MDO-CALC-BENE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_MDO_CALC_BENE");
        bda_Bd_Mdo_Calc_Bene.setDdmHeader("MDO/CALC/BENE");
        bda_Bd_Addr1 = vw_bda.getRecord().newFieldInGroup("bda_Bd_Addr1", "BD-ADDR1", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BD_ADDR1");
        bda_Bd_Addr2 = vw_bda.getRecord().newFieldInGroup("bda_Bd_Addr2", "BD-ADDR2", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BD_ADDR2");
        bda_Bd_Addr3_City = vw_bda.getRecord().newFieldInGroup("bda_Bd_Addr3_City", "BD-ADDR3-CITY", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "BD_ADDR3_CITY");
        bda_Bd_State = vw_bda.getRecord().newFieldInGroup("bda_Bd_State", "BD-STATE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "BD_STATE");
        bda_Bd_Zip = vw_bda.getRecord().newFieldInGroup("bda_Bd_Zip", "BD-ZIP", FieldType.STRING, 10, RepeatingFieldStrategy.None, "BD_ZIP");
        bda_Bd_Phone = vw_bda.getRecord().newFieldInGroup("bda_Bd_Phone", "BD-PHONE", FieldType.STRING, 20, RepeatingFieldStrategy.None, "BD_PHONE");
        registerRecord(vw_bda);

        vw_bma = new DataAccessProgramView(new NameInfo("vw_bma", "BMA"), "BENE_MOS_12", "BENE_MOS", DdmPeriodicGroups.getInstance().getGroups("BENE_MOS_12"));
        bma_Bm_Pin = vw_bma.getRecord().newFieldInGroup("bma_Bm_Pin", "BM-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BM_PIN");
        bma_Bm_Pin.setDdmHeader("PIN");
        bma_Bm_Tiaa_Cntrct = vw_bma.getRecord().newFieldInGroup("bma_Bm_Tiaa_Cntrct", "BM-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BM_TIAA_CNTRCT");
        bma_Bm_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bma_Bm_Cref_Cntrct = vw_bma.getRecord().newFieldInGroup("bma_Bm_Cref_Cntrct", "BM-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BM_CREF_CNTRCT");
        bma_Bm_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bma_Bm_Tiaa_Cref_Ind = vw_bma.getRecord().newFieldInGroup("bma_Bm_Tiaa_Cref_Ind", "BM-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BM_TIAA_CREF_IND");
        bma_Bm_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bma_Bm_Stat = vw_bma.getRecord().newFieldInGroup("bma_Bm_Stat", "BM-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BM_STAT");
        bma_Bm_Stat.setDdmHeader("STAT");
        bma_Bm_Rqst_Timestamp = vw_bma.getRecord().newFieldInGroup("bma_Bm_Rqst_Timestamp", "BM-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BM_RQST_TIMESTAMP");
        bma_Bm_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        bma_Bm_Rcrd_Last_Updt_Dte = vw_bma.getRecord().newFieldInGroup("bma_Bm_Rcrd_Last_Updt_Dte", "BM-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BM_RCRD_LAST_UPDT_DTE");
        bma_Bm_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bma_Bm_Rcrd_Last_Updt_Tme = vw_bma.getRecord().newFieldInGroup("bma_Bm_Rcrd_Last_Updt_Tme", "BM-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BM_RCRD_LAST_UPDT_TME");
        bma_Bm_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bma_Bm_Rcrd_Last_Updt_Userid = vw_bma.getRecord().newFieldInGroup("bma_Bm_Rcrd_Last_Updt_Userid", "BM-RCRD-LAST-UPDT-USERID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "BM_RCRD_LAST_UPDT_USERID");
        bma_Bm_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");

        bma_Bm_Mos_Txt_Group = vw_bma.getRecord().newGroupInGroup("bma_Bm_Mos_Txt_Group", "BM-MOS-TXT-GROUP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "BENE_MOS_BM_MOS_TXT_GROUP");
        bma_Bm_Mos_Txt = bma_Bm_Mos_Txt_Group.newFieldArrayInGroup("bma_Bm_Mos_Txt", "BM-MOS-TXT", FieldType.STRING, 72, new DbsArrayController(1, 60) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "BM_MOS_TXT", "BENE_MOS_BM_MOS_TXT_GROUP");
        bma_Bm_Mos_Txt.setDdmHeader("MOS TEXT");
        registerRecord(vw_bma);

        pnd_Bc_Pin_Cntrct_Ind_Key = localVariables.newFieldInRecord("pnd_Bc_Pin_Cntrct_Ind_Key", "#BC-PIN-CNTRCT-IND-KEY", FieldType.STRING, 23);

        pnd_Bc_Pin_Cntrct_Ind_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Bc_Pin_Cntrct_Ind_Key__R_Field_1", "REDEFINE", pnd_Bc_Pin_Cntrct_Ind_Key);
        pnd_Bc_Pin_Cntrct_Ind_Key_Pnd_Bc_Pin = pnd_Bc_Pin_Cntrct_Ind_Key__R_Field_1.newFieldInGroup("pnd_Bc_Pin_Cntrct_Ind_Key_Pnd_Bc_Pin", "#BC-PIN", 
            FieldType.NUMERIC, 12);
        pnd_Bc_Pin_Cntrct_Ind_Key_Pnd_Bc_Tiaa_Cntrct = pnd_Bc_Pin_Cntrct_Ind_Key__R_Field_1.newFieldInGroup("pnd_Bc_Pin_Cntrct_Ind_Key_Pnd_Bc_Tiaa_Cntrct", 
            "#BC-TIAA-CNTRCT", FieldType.STRING, 10);
        pnd_Bc_Pin_Cntrct_Ind_Key_Pnd_Bc_Tiaa_Cref_Ind = pnd_Bc_Pin_Cntrct_Ind_Key__R_Field_1.newFieldInGroup("pnd_Bc_Pin_Cntrct_Ind_Key_Pnd_Bc_Tiaa_Cref_Ind", 
            "#BC-TIAA-CREF-IND", FieldType.STRING, 1);

        pnd_Details = localVariables.newGroupInRecord("pnd_Details", "#DETAILS");
        pnd_Details_Pnd_Add_Bc = pnd_Details.newFieldInGroup("pnd_Details_Pnd_Add_Bc", "#ADD-BC", FieldType.BOOLEAN, 1);
        pnd_Details_Pnd_Add_Bd = pnd_Details.newFieldInGroup("pnd_Details_Pnd_Add_Bd", "#ADD-BD", FieldType.BOOLEAN, 1);
        pnd_Details_Pnd_Add_Bm = pnd_Details.newFieldInGroup("pnd_Details_Pnd_Add_Bm", "#ADD-BM", FieldType.BOOLEAN, 1);
        pnd_Details_Pnd_Dlt = pnd_Details.newFieldInGroup("pnd_Details_Pnd_Dlt", "#DLT", FieldType.NUMERIC, 3);
        pnd_Details_Pnd_Error = pnd_Details.newFieldInGroup("pnd_Details_Pnd_Error", "#ERROR", FieldType.BOOLEAN, 1);
        pnd_Details_Pnd_Et = pnd_Details.newFieldInGroup("pnd_Details_Pnd_Et", "#ET", FieldType.BOOLEAN, 1);
        pnd_Details_Pnd_Found_Ben_Src = pnd_Details.newFieldInGroup("pnd_Details_Pnd_Found_Ben_Src", "#FOUND-BEN-SRC", FieldType.BOOLEAN, 1);
        pnd_Details_Pnd_Found_Ben_Tgt = pnd_Details.newFieldInGroup("pnd_Details_Pnd_Found_Ben_Tgt", "#FOUND-BEN-TGT", FieldType.BOOLEAN, 1);
        pnd_Details_Pnd_Isn_Bc = pnd_Details.newFieldInGroup("pnd_Details_Pnd_Isn_Bc", "#ISN-BC", FieldType.PACKED_DECIMAL, 10);
        pnd_Details_Pnd_Isn_Bc_Dlt = pnd_Details.newFieldInGroup("pnd_Details_Pnd_Isn_Bc_Dlt", "#ISN-BC-DLT", FieldType.PACKED_DECIMAL, 10);
        pnd_Details_Pnd_Isn_Bd = pnd_Details.newFieldArrayInGroup("pnd_Details_Pnd_Isn_Bd", "#ISN-BD", FieldType.PACKED_DECIMAL, 10, new DbsArrayController(1, 
            30));
        pnd_Details_Pnd_Isn_Bm = pnd_Details.newFieldInGroup("pnd_Details_Pnd_Isn_Bm", "#ISN-BM", FieldType.PACKED_DECIMAL, 10);
        pnd_Details_Pnd_Isn_Same_Dflt = pnd_Details.newFieldInGroup("pnd_Details_Pnd_Isn_Same_Dflt", "#ISN-SAME-DFLT", FieldType.PACKED_DECIMAL, 10);
        pnd_Details_Pnd_Last_Date = pnd_Details.newFieldInGroup("pnd_Details_Pnd_Last_Date", "#LAST-DATE", FieldType.STRING, 8);
        pnd_Details_Pnd_Msg = pnd_Details.newFieldInGroup("pnd_Details_Pnd_Msg", "#MSG", FieldType.STRING, 40);
        pnd_Details_Pnd_Post_1999 = pnd_Details.newFieldInGroup("pnd_Details_Pnd_Post_1999", "#POST-1999", FieldType.BOOLEAN, 1);
        pnd_Details_Pnd_Rldt = pnd_Details.newFieldInGroup("pnd_Details_Pnd_Rldt", "#RLDT", FieldType.STRING, 15);

        pnd_Details__R_Field_2 = pnd_Details.newGroupInGroup("pnd_Details__R_Field_2", "REDEFINE", pnd_Details_Pnd_Rldt);
        pnd_Details_Pnd_Date = pnd_Details__R_Field_2.newFieldInGroup("pnd_Details_Pnd_Date", "#DATE", FieldType.STRING, 8);
        pnd_Details_Pnd_Time = pnd_Details__R_Field_2.newFieldInGroup("pnd_Details_Pnd_Time", "#TIME", FieldType.STRING, 7);
        pnd_Details_Pnd_Same_Dflt = pnd_Details.newFieldInGroup("pnd_Details_Pnd_Same_Dflt", "#SAME-DFLT", FieldType.BOOLEAN, 1);
        pnd_Details_Pnd_Seq = pnd_Details.newFieldInGroup("pnd_Details_Pnd_Seq", "#SEQ", FieldType.NUMERIC, 3);

        pnd_Totals = localVariables.newGroupInRecord("pnd_Totals", "#TOTALS");
        pnd_Totals_Pnd_Total_Acis = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Acis", "#TOTAL-ACIS", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Copied = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Copied", "#TOTAL-COPIED", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Default = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Default", "#TOTAL-DEFAULT", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Error = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Error", "#TOTAL-ERROR", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Exist = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Exist", "#TOTAL-EXIST", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Same_Dflt = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Same_Dflt", "#TOTAL-SAME-DFLT", FieldType.PACKED_DECIMAL, 10);
        pls_Trace = WsIndependent.getInstance().newFieldInRecord("pls_Trace", "+TRACE", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_bc.reset();
        vw_bd.reset();
        vw_bm.reset();
        vw_bca.reset();
        vw_bda.reset();
        vw_bma.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Benp310() throws Exception
    {
        super("Benp310");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) LS = 133 PS = 60 SG = OFF ZP = ON;//Natural: FORMAT ( 1 ) LS = 133 PS = 60 SG = OFF ZP = ON
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #ACIS
        while (condition(getWorkFiles().read(1, pnd_Acis)))
        {
            pnd_Totals_Pnd_Total_Acis.nadd(1);                                                                                                                            //Natural: ADD 1 TO #TOTAL-ACIS
            pnd_Details.reset();                                                                                                                                          //Natural: RESET #DETAILS
                                                                                                                                                                          //Natural: PERFORM CHECK-BEN-PIN-CNTRCT-SRC-TGT
            sub_Check_Ben_Pin_Cntrct_Src_Tgt();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            short decideConditionsMet257 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #ERROR
            if (condition(pnd_Details_Pnd_Error.getBoolean()))
            {
                decideConditionsMet257++;
                                                                                                                                                                          //Natural: PERFORM PRINT-ERROR
                sub_Print_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN #FOUND-BEN-TGT AND NOT #SAME-DFLT
            else if (condition(pnd_Details_Pnd_Found_Ben_Tgt.getBoolean() && ! (pnd_Details_Pnd_Same_Dflt.getBoolean())))
            {
                decideConditionsMet257++;
                pnd_Totals_Pnd_Total_Exist.nadd(1);                                                                                                                       //Natural: ADD 1 TO #TOTAL-EXIST
            }                                                                                                                                                             //Natural: WHEN #FOUND-BEN-SRC AND ( NOT #FOUND-BEN-TGT OR #SAME-DFLT )
            else if (condition((pnd_Details_Pnd_Found_Ben_Src.getBoolean() && (! (pnd_Details_Pnd_Found_Ben_Tgt.getBoolean()) || pnd_Details_Pnd_Same_Dflt.getBoolean()))))
            {
                decideConditionsMet257++;
                                                                                                                                                                          //Natural: PERFORM COPY-BEN-SRC-TO-TGT
                sub_Copy_Ben_Src_To_Tgt();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN NOT #FOUND-BEN-SRC AND NOT #FOUND-BEN-TGT
            else if (condition(! ((pnd_Details_Pnd_Found_Ben_Src.getBoolean()) && ! (pnd_Details_Pnd_Found_Ben_Tgt.getBoolean()))))
            {
                decideConditionsMet257++;
                                                                                                                                                                          //Natural: PERFORM ADD-BENE-DEFAULT-DESIGNATION
                sub_Add_Bene_Default_Designation();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet257 > 0))
            {
                if (condition(pnd_Details_Pnd_Et.getBoolean()))                                                                                                           //Natural: IF #ET
                {
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getCurrentProcessState().getDbConv().dbRollback();                                                                                                    //Natural: BACKOUT TRANSACTION
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(! (pnd_Details_Pnd_Error.getBoolean())))                                                                                                        //Natural: IF NOT #ERROR
            {
                getReports().display(1, ReportOption.NOTITLE,"PIN",                                                                                                       //Natural: DISPLAY ( 1 ) NOTITLE 'PIN' #I-PIN 'TIAA Src' #I-TIAA-SRC 'CREF Src' #I-CREF-SRC 'TIAA Tgt' #I-TIAA-TGT 'CREF Tgt' #I-CREF-TGT 'Eff Date' #I-EFF-DATE 'Message' #MSG
                		pnd_Acis_Pnd_I_Pin,"TIAA Src",
                		pnd_Acis_Pnd_I_Tiaa_Src,"CREF Src",
                		pnd_Acis_Pnd_I_Cref_Src,"TIAA Tgt",
                		pnd_Acis_Pnd_I_Tiaa_Tgt,"CREF Tgt",
                		pnd_Acis_Pnd_I_Cref_Tgt,"Eff Date",
                		pnd_Acis_Pnd_I_Eff_Date,"Message",
                		pnd_Details_Pnd_Msg);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getReports().write(1, "Total ACIS Source/Target Contracts Read :",pnd_Totals_Pnd_Total_Acis, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));                               //Natural: WRITE ( 1 ) 'Total ACIS Source/Target Contracts Read :' #TOTAL-ACIS
        if (Global.isEscape()) return;
        getReports().write(1, "Total Target Contract not copied (error):",pnd_Totals_Pnd_Total_Error, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));                              //Natural: WRITE ( 1 ) 'Total Target Contract not copied (error):' #TOTAL-ERROR
        if (Global.isEscape()) return;
        getReports().write(1, "Total Target Contract already in BENE   :",pnd_Totals_Pnd_Total_Exist, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));                              //Natural: WRITE ( 1 ) 'Total Target Contract already in BENE   :' #TOTAL-EXIST
        if (Global.isEscape()) return;
        getReports().write(1, "Total Target Contract is SAME AS/Default:",pnd_Totals_Pnd_Total_Same_Dflt, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));                          //Natural: WRITE ( 1 ) 'Total Target Contract is SAME AS/Default:' #TOTAL-SAME-DFLT
        if (Global.isEscape()) return;
        getReports().write(1, "Total Source BENE Copied to Target      :",pnd_Totals_Pnd_Total_Copied, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));                             //Natural: WRITE ( 1 ) 'Total Source BENE Copied to Target      :' #TOTAL-COPIED
        if (Global.isEscape()) return;
        getReports().write(1, "Total No Source; Target Bene Defaulted  :",pnd_Totals_Pnd_Total_Default, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));                            //Natural: WRITE ( 1 ) 'Total No Source; Target Bene Defaulted  :' #TOTAL-DEFAULT
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-BENE-DEFAULT-DESIGNATION
        //*  IF #POST-1999
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-BEN-PIN-CNTRCT-SRC-TGT
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COPY-BEN-SRC-TO-TGT
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-ERROR
    }
    private void sub_Add_Bene_Default_Designation() throws Exception                                                                                                      //Natural: ADD-BENE-DEFAULT-DESIGNATION
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Details_Pnd_Date.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                             //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO #DATE
        pnd_Details_Pnd_Time.setValueEdited(Global.getTIMX(),new ReportEditMask("HHIISST"));                                                                              //Natural: MOVE EDITED *TIMX ( EM = HHIISST ) TO #TIME
        //*  BLANK=BOTH
        //*  CURRENT
        //*  DA
        //*  BENE-DESIGNATION
        //*  NO
        //*  NONE
        //*  NONE
        //*  NONE
        //*  INTERFACE
        //*  ACIS
        //*  NONE
        vw_bca.reset();                                                                                                                                                   //Natural: RESET BCA
        bca_Bc_Pin.setValue(pnd_Acis_Pnd_I_Pin);                                                                                                                          //Natural: ASSIGN BCA.BC-PIN := #I-PIN
        bca_Bc_Tiaa_Cntrct.setValue(pnd_Acis_Pnd_I_Tiaa_Tgt);                                                                                                             //Natural: ASSIGN BCA.BC-TIAA-CNTRCT := #I-TIAA-TGT
        bca_Bc_Cref_Cntrct.setValue(pnd_Acis_Pnd_I_Cref_Tgt);                                                                                                             //Natural: ASSIGN BCA.BC-CREF-CNTRCT := #I-CREF-TGT
        bca_Bc_Tiaa_Cref_Ind.setValue(" ");                                                                                                                               //Natural: ASSIGN BCA.BC-TIAA-CREF-IND := ' '
        bca_Bc_Stat.setValue("C");                                                                                                                                        //Natural: ASSIGN BCA.BC-STAT := 'C'
        bca_Bc_Cntrct_Type.setValue("D");                                                                                                                                 //Natural: ASSIGN BCA.BC-CNTRCT-TYPE := 'D'
        bca_Bc_Rqst_Timestamp.setValue(pnd_Details_Pnd_Rldt);                                                                                                             //Natural: ASSIGN BCA.BC-RQST-TIMESTAMP := #RLDT
        bca_Bc_Eff_Dte.setValue(pnd_Acis_Pnd_I_Eff_Date);                                                                                                                 //Natural: ASSIGN BCA.BC-EFF-DTE := #I-EFF-DATE
        bca_Bc_Mos_Ind.setValue("N");                                                                                                                                     //Natural: ASSIGN BCA.BC-MOS-IND := 'N'
        bca_Bc_Irvcbl_Ind.setValue("N");                                                                                                                                  //Natural: ASSIGN BCA.BC-IRVCBL-IND := 'N'
        bca_Bc_Pymnt_Chld_Dcsd_Ind.setValue(" ");                                                                                                                         //Natural: ASSIGN BCA.BC-PYMNT-CHLD-DCSD-IND := ' '
        bca_Bc_Exempt_Spouse_Rights.setValue(" ");                                                                                                                        //Natural: ASSIGN BCA.BC-EXEMPT-SPOUSE-RIGHTS := ' '
        bca_Bc_Spouse_Waived_Bnfts.setValue(" ");                                                                                                                         //Natural: ASSIGN BCA.BC-SPOUSE-WAIVED-BNFTS := ' '
        bca_Bc_Rcrd_Last_Updt_Dte.setValue(pnd_Details_Pnd_Date);                                                                                                         //Natural: ASSIGN BCA.BC-RCRD-LAST-UPDT-DTE := #DATE
        bca_Bc_Rcrd_Last_Updt_Tme.setValue(pnd_Details_Pnd_Time);                                                                                                         //Natural: ASSIGN BCA.BC-RCRD-LAST-UPDT-TME := #TIME
        bca_Bc_Rcrd_Last_Updt_Userid.setValue(Global.getPROGRAM());                                                                                                       //Natural: ASSIGN BCA.BC-RCRD-LAST-UPDT-USERID := *PROGRAM
        bca_Bc_Last_Dsgntn_Srce.setValue("I");                                                                                                                            //Natural: ASSIGN BCA.BC-LAST-DSGNTN-SRCE := 'I'
        bca_Bc_Last_Dsgntn_System.setValue("A");                                                                                                                          //Natural: ASSIGN BCA.BC-LAST-DSGNTN-SYSTEM := 'A'
        bca_Bc_Last_Dsgntn_Userid.setValue(Global.getPROGRAM());                                                                                                          //Natural: ASSIGN BCA.BC-LAST-DSGNTN-USERID := *PROGRAM
        bca_Bc_Last_Dsgntn_Dte.setValue(pnd_Details_Pnd_Date);                                                                                                            //Natural: ASSIGN BCA.BC-LAST-DSGNTN-DTE := #DATE
        bca_Bc_Last_Dsgntn_Tme.setValue(pnd_Details_Pnd_Time);                                                                                                            //Natural: ASSIGN BCA.BC-LAST-DSGNTN-TME := #TIME
        bca_Bc_Chng_Pwr_Atty.setValue(" ");                                                                                                                               //Natural: ASSIGN BCA.BC-CHNG-PWR-ATTY := ' '
        bca_Bc_Mgrtn_Cnfrm_Stmnt_Prod_Dte.setValue(pnd_Details_Pnd_Date);                                                                                                 //Natural: ASSIGN BCA.BC-MGRTN-CNFRM-STMNT-PROD-DTE := #DATE
        //*  BENE-CONTRACT FILE
        vw_bca.insertDBRow();                                                                                                                                             //Natural: STORE BCA
        pnd_Details_Pnd_Add_Bc.setValue(true);                                                                                                                            //Natural: ASSIGN #ADD-BC := TRUE
        //*  BLANK=BOTH
        //*  CURRENT
        //*  ONLY ONE
        //*  PRIMARY
        //*  OTHER
        //*  PERCENT
        //*  NO
        //*  NO
        vw_bda.reset();                                                                                                                                                   //Natural: RESET BDA
        bda_Bd_Pin.setValue(pnd_Acis_Pnd_I_Pin);                                                                                                                          //Natural: ASSIGN BDA.BD-PIN := #I-PIN
        bda_Bd_Tiaa_Cntrct.setValue(pnd_Acis_Pnd_I_Tiaa_Tgt);                                                                                                             //Natural: ASSIGN BDA.BD-TIAA-CNTRCT := #I-TIAA-TGT
        bda_Bd_Cref_Cntrct.setValue(pnd_Acis_Pnd_I_Cref_Tgt);                                                                                                             //Natural: ASSIGN BDA.BD-CREF-CNTRCT := #I-CREF-TGT
        bda_Bd_Tiaa_Cref_Ind.setValue(" ");                                                                                                                               //Natural: ASSIGN BDA.BD-TIAA-CREF-IND := ' '
        bda_Bd_Stat.setValue("C");                                                                                                                                        //Natural: ASSIGN BDA.BD-STAT := 'C'
        bda_Bd_Seq_Nmbr.setValue(1);                                                                                                                                      //Natural: ASSIGN BDA.BD-SEQ-NMBR := 001
        bda_Bd_Rqst_Timestamp.setValue(pnd_Details_Pnd_Rldt);                                                                                                             //Natural: ASSIGN BDA.BD-RQST-TIMESTAMP := #RLDT
        bda_Bd_Rcrd_Last_Updt_Dte.setValue(pnd_Details_Pnd_Date);                                                                                                         //Natural: ASSIGN BDA.BD-RCRD-LAST-UPDT-DTE := #DATE
        bda_Bd_Rcrd_Last_Updt_Tme.setValue(pnd_Details_Pnd_Time);                                                                                                         //Natural: ASSIGN BDA.BD-RCRD-LAST-UPDT-TME := #TIME
        bda_Bd_Rcrd_Last_Updt_Userid.setValue(Global.getPROGRAM());                                                                                                       //Natural: ASSIGN BDA.BD-RCRD-LAST-UPDT-USERID := *PROGRAM
        bda_Bd_Bene_Type.setValue("P");                                                                                                                                   //Natural: ASSIGN BDA.BD-BENE-TYPE := 'P'
        bda_Bd_Rltn_Cd.setValue("38");                                                                                                                                    //Natural: ASSIGN BDA.BD-RLTN-CD := '38'
        bda_Bd_Perc_Share_Ind.setValue("P");                                                                                                                              //Natural: ASSIGN BDA.BD-PERC-SHARE-IND := 'P'
        bda_Bd_Share_Perc.setValue(100);                                                                                                                                  //Natural: ASSIGN BDA.BD-SHARE-PERC := 100.00
        bda_Bd_Irvcbl_Ind.setValue("N");                                                                                                                                  //Natural: ASSIGN BDA.BD-IRVCBL-IND := 'N'
        bda_Bd_Stlmnt_Rstrctn.setValue("N");                                                                                                                              //Natural: ASSIGN BDA.BD-STLMNT-RSTRCTN := 'N'
        bda_Bd_Bene_Name1.setValue("Default to Product / Plan provision");                                                                                                //Natural: ASSIGN BDA.BD-BENE-NAME1 := 'Default to Product / Plan provision'
        bda_Bd_Bene_Name2.setValue("Please add your current intentions");                                                                                                 //Natural: ASSIGN BDA.BD-BENE-NAME2 := 'Please add your current intentions'
        //*  ELSE
        //*   BDA.BD-BENE-NAME1 := 'Please give us your current'
        //*   BDA.BD-BENE-NAME2 := 'intentions'
        //*  END-IF
        //*  BENE-DESIGNATION FILE
        vw_bda.insertDBRow();                                                                                                                                             //Natural: STORE BDA
        pnd_Details_Pnd_Add_Bd.setValue(true);                                                                                                                            //Natural: ASSIGN #ADD-BD := TRUE
        pnd_Details_Pnd_Et.setValue(true);                                                                                                                                //Natural: ASSIGN #ET := TRUE
        pnd_Totals_Pnd_Total_Default.nadd(1);                                                                                                                             //Natural: ADD 1 TO #TOTAL-DEFAULT
        pnd_Details_Pnd_Msg.setValue(DbsUtil.compress(pnd_Details_Pnd_Msg, bda_Bd_Bene_Name1));                                                                           //Natural: COMPRESS #MSG BDA.BD-BENE-NAME1 INTO #MSG
        //*  ADD-BENE-DEFAULT-DESIGNATION
    }
    private void sub_Check_Ben_Pin_Cntrct_Src_Tgt() throws Exception                                                                                                      //Natural: CHECK-BEN-PIN-CNTRCT-SRC-TGT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Bc_Pin_Cntrct_Ind_Key_Pnd_Bc_Pin.setValue(pnd_Acis_Pnd_I_Pin);                                                                                                //Natural: ASSIGN #BC-PIN := #I-PIN
        pnd_Bc_Pin_Cntrct_Ind_Key_Pnd_Bc_Tiaa_Cntrct.setValue(pnd_Acis_Pnd_I_Tiaa_Src);                                                                                   //Natural: ASSIGN #BC-TIAA-CNTRCT := #I-TIAA-SRC
        pnd_Bc_Pin_Cntrct_Ind_Key_Pnd_Bc_Tiaa_Cref_Ind.setValue(" ");                                                                                                     //Natural: ASSIGN #BC-TIAA-CREF-IND := ' '
        vw_bc.startDatabaseRead                                                                                                                                           //Natural: READ BC BY BC-PIN-CNTRCT-IND-KEY STARTING FROM #BC-PIN-CNTRCT-IND-KEY
        (
        "RBC1",
        new Wc[] { new Wc("BC_PIN_CNTRCT_IND_KEY", ">=", pnd_Bc_Pin_Cntrct_Ind_Key, WcType.BY) },
        new Oc[] { new Oc("BC_PIN_CNTRCT_IND_KEY", "ASC") }
        );
        RBC1:
        while (condition(vw_bc.readNextRow("RBC1")))
        {
            if (condition(bc_Bc_Pin.notEquals(pnd_Bc_Pin_Cntrct_Ind_Key_Pnd_Bc_Pin) || bc_Bc_Tiaa_Cntrct.notEquals(pnd_Bc_Pin_Cntrct_Ind_Key_Pnd_Bc_Tiaa_Cntrct)))        //Natural: IF BC.BC-PIN NE #BC-PIN OR BC.BC-TIAA-CNTRCT NE #BC-TIAA-CNTRCT
            {
                if (true) break RBC1;                                                                                                                                     //Natural: ESCAPE BOTTOM ( RBC1. )
            }                                                                                                                                                             //Natural: END-IF
            //*  NOT CURRENT
            if (condition(bc_Bc_Tiaa_Cref_Ind.notEquals(" ")))                                                                                                            //Natural: IF BC.BC-TIAA-CREF-IND NE ' '
            {
                pnd_Details_Pnd_Error.setValue(true);                                                                                                                     //Natural: ASSIGN #ERROR := TRUE
                pnd_Details_Pnd_Msg.setValue(DbsUtil.compress("Split Designation =", bc_Bc_Tiaa_Cref_Ind));                                                               //Natural: COMPRESS 'Split Designation =' BC-TIAA-CREF-IND INTO #MSG
                if (true) break RBC1;                                                                                                                                     //Natural: ESCAPE BOTTOM ( RBC1. )
            }                                                                                                                                                             //Natural: END-IF
            //*  NOT CURRENT
            if (condition(bc_Bc_Stat.notEquals("C")))                                                                                                                     //Natural: IF BC.BC-STAT NE 'C'
            {
                pnd_Details_Pnd_Error.setValue(true);                                                                                                                     //Natural: ASSIGN #ERROR := TRUE
                pnd_Details_Pnd_Msg.setValue("BENE Status is not Current");                                                                                               //Natural: ASSIGN #MSG := 'BENE Status is not Current'
                if (true) break RBC1;                                                                                                                                     //Natural: ESCAPE BOTTOM ( RBC1. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Bc_Pin_Cntrct_Ind_Key_Pnd_Bc_Tiaa_Cref_Ind.setValue(bc_Bc_Tiaa_Cref_Ind);                                                                                 //Natural: ASSIGN #BC-TIAA-CREF-IND := BC-TIAA-CREF-IND
            pnd_Details_Pnd_Found_Ben_Src.setValue(true);                                                                                                                 //Natural: ASSIGN #FOUND-BEN-SRC := TRUE
            pnd_Details_Pnd_Isn_Bc.setValue(vw_bc.getAstISN("RBC1"));                                                                                                     //Natural: ASSIGN #ISN-BC := *ISN ( RBC1. )
            if (condition(bc_Bc_Mos_Ind.equals("Y")))                                                                                                                     //Natural: IF BC.BC-MOS-IND = 'Y'
            {
                vw_bm.startDatabaseFind                                                                                                                                   //Natural: FIND BM WITH BM-PIN-CNTRCT-IND-KEY = #BC-PIN-CNTRCT-IND-KEY
                (
                "FBM1",
                new Wc[] { new Wc("BM_PIN_CNTRCT_IND_KEY", "=", pnd_Bc_Pin_Cntrct_Ind_Key, WcType.WITH) }
                );
                FBM1:
                while (condition(vw_bm.readNextRow("FBM1", true)))
                {
                    vw_bm.setIfNotFoundControlFlag(false);
                    if (condition(vw_bm.getAstCOUNTER().equals(0)))                                                                                                       //Natural: IF NO RECORDS FOUND
                    {
                        if (true) break FBM1;                                                                                                                             //Natural: ESCAPE BOTTOM ( FBM1. )
                    }                                                                                                                                                     //Natural: END-NOREC
                    pnd_Details_Pnd_Isn_Bm.setValue(vw_bm.getAstISN("FBM1"));                                                                                             //Natural: ASSIGN #ISN-BM := *ISN ( FBM1. )
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RBC1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RBC1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                vw_bd.startDatabaseFind                                                                                                                                   //Natural: FIND BD WITH BD-PIN-CNTRCT-IND-KEY = #BC-PIN-CNTRCT-IND-KEY
                (
                "FBD1",
                new Wc[] { new Wc("BD_PIN_CNTRCT_IND_KEY", "=", pnd_Bc_Pin_Cntrct_Ind_Key, WcType.WITH) }
                );
                FBD1:
                while (condition(vw_bd.readNextRow("FBD1", true)))
                {
                    vw_bd.setIfNotFoundControlFlag(false);
                    if (condition(vw_bd.getAstCOUNTER().equals(0)))                                                                                                       //Natural: IF NO RECORDS FOUND
                    {
                        if (true) break FBD1;                                                                                                                             //Natural: ESCAPE BOTTOM ( FBD1. )
                    }                                                                                                                                                     //Natural: END-NOREC
                    pnd_Details_Pnd_Seq.setValue(bd_Bd_Seq_Nmbr);                                                                                                         //Natural: ASSIGN #SEQ := BD.BD-SEQ-NMBR
                    pnd_Details_Pnd_Isn_Bd.getValue(pnd_Details_Pnd_Seq).setValue(vw_bd.getAstISN("FBD1"));                                                               //Natural: ASSIGN #ISN-BD ( #SEQ ) := *ISN ( FBD1. )
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RBC1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RBC1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(! (pnd_Details_Pnd_Found_Ben_Src.getBoolean())))                                                                                                    //Natural: IF NOT #FOUND-BEN-SRC
        {
            pnd_Details_Pnd_Msg.setValue(DbsUtil.compress(pnd_Details_Pnd_Msg, "No BEN TIAA Source;"));                                                                   //Natural: COMPRESS #MSG 'No BEN TIAA Source;' INTO #MSG
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Details_Pnd_Found_Ben_Src.getBoolean() && ! (pnd_Details_Pnd_Isn_Bd.getValue("*").greater(getZero())) && pnd_Details_Pnd_Isn_Bm.equals(getZero()))) //Natural: IF #FOUND-BEN-SRC AND ( NOT ( #ISN-BD ( * ) > 0 ) AND #ISN-BM = 0 )
        {
            pnd_Details_Pnd_Error.setValue(true);                                                                                                                         //Natural: ASSIGN #ERROR := TRUE
            pnd_Details_Pnd_Msg.setValue(DbsUtil.compress("Incomplete BENE Source; BC", pnd_Details_Pnd_Isn_Bc, "BM", pnd_Details_Pnd_Isn_Bm, "BD", pnd_Details_Pnd_Isn_Bd.getValue("*"))); //Natural: COMPRESS 'Incomplete BENE Source; BC' #ISN-BC 'BM' #ISN-BM 'BD' #ISN-BD ( * ) INTO #MSG
            pnd_Details_Pnd_Found_Ben_Src.reset();                                                                                                                        //Natural: RESET #FOUND-BEN-SRC #ISN-BC #ISN-BD ( * ) #ISN-BM
            pnd_Details_Pnd_Isn_Bc.reset();
            pnd_Details_Pnd_Isn_Bd.getValue("*").reset();
            pnd_Details_Pnd_Isn_Bm.reset();
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Details_Pnd_Last_Date.reset();                                                                                                                                //Natural: RESET #LAST-DATE
        pnd_Bc_Pin_Cntrct_Ind_Key_Pnd_Bc_Tiaa_Cntrct.setValue(pnd_Acis_Pnd_I_Tiaa_Tgt);                                                                                   //Natural: ASSIGN #BC-TIAA-CNTRCT := #I-TIAA-TGT
        pnd_Bc_Pin_Cntrct_Ind_Key_Pnd_Bc_Tiaa_Cref_Ind.setValue(" ");                                                                                                     //Natural: ASSIGN #BC-TIAA-CREF-IND := ' '
        vw_bc.startDatabaseRead                                                                                                                                           //Natural: READ BC BY BC-PIN-CNTRCT-IND-KEY STARTING FROM #BC-PIN-CNTRCT-IND-KEY
        (
        "RBC2",
        new Wc[] { new Wc("BC_PIN_CNTRCT_IND_KEY", ">=", pnd_Bc_Pin_Cntrct_Ind_Key, WcType.BY) },
        new Oc[] { new Oc("BC_PIN_CNTRCT_IND_KEY", "ASC") }
        );
        RBC2:
        while (condition(vw_bc.readNextRow("RBC2")))
        {
            if (condition(bc_Bc_Pin.notEquals(pnd_Bc_Pin_Cntrct_Ind_Key_Pnd_Bc_Pin) || bc_Bc_Tiaa_Cntrct.notEquals(pnd_Bc_Pin_Cntrct_Ind_Key_Pnd_Bc_Tiaa_Cntrct)))        //Natural: IF BC.BC-PIN NE #BC-PIN OR BC.BC-TIAA-CNTRCT NE #BC-TIAA-CNTRCT
            {
                if (true) break RBC2;                                                                                                                                     //Natural: ESCAPE BOTTOM ( RBC2. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Details_Pnd_Isn_Bc_Dlt.setValue(vw_bc.getAstISN("RBC2"));                                                                                                 //Natural: ASSIGN #ISN-BC-DLT := *ISN ( RBC2. )
            pnd_Details_Pnd_Found_Ben_Tgt.setValue(true);                                                                                                                 //Natural: ASSIGN #FOUND-BEN-TGT := TRUE
            vw_bd.startDatabaseRead                                                                                                                                       //Natural: READ BD BY BD-PIN-CNTRCT-IND-KEY STARTING FROM #BC-PIN-CNTRCT-IND-KEY
            (
            "RBD2",
            new Wc[] { new Wc("BD_PIN_CNTRCT_IND_KEY", ">=", pnd_Bc_Pin_Cntrct_Ind_Key, WcType.BY) },
            new Oc[] { new Oc("BD_PIN_CNTRCT_IND_KEY", "ASC") }
            );
            RBD2:
            while (condition(vw_bd.readNextRow("RBD2")))
            {
                if (condition(bd_Bd_Pin.notEquals(pnd_Bc_Pin_Cntrct_Ind_Key_Pnd_Bc_Pin) || bd_Bd_Tiaa_Cntrct.notEquals(pnd_Bc_Pin_Cntrct_Ind_Key_Pnd_Bc_Tiaa_Cntrct)      //Natural: IF BD.BD-PIN NE #BC-PIN OR BD.BD-TIAA-CNTRCT NE #BC-TIAA-CNTRCT OR BD.BD-TIAA-CREF-IND NE #BC-TIAA-CREF-IND
                    || bd_Bd_Tiaa_Cref_Ind.notEquals(pnd_Bc_Pin_Cntrct_Ind_Key_Pnd_Bc_Tiaa_Cref_Ind)))
                {
                    if (true) break RBD2;                                                                                                                                 //Natural: ESCAPE BOTTOM ( RBD2. )
                }                                                                                                                                                         //Natural: END-IF
                pnd_Details_Pnd_Last_Date.setValue(bc_Bc_Rcrd_Last_Updt_Dte);                                                                                             //Natural: ASSIGN #LAST-DATE := BC-RCRD-LAST-UPDT-DTE
                DbsUtil.examine(new ExamineSource(bd_Bd_Bene_Name1), new ExamineTranslate(TranslateOption.Upper));                                                        //Natural: EXAMINE BD-BENE-NAME1 TRANSLATE INTO UPPER
                DbsUtil.examine(new ExamineSource(bd_Bd_Bene_Name2), new ExamineTranslate(TranslateOption.Upper));                                                        //Natural: EXAMINE BD-BENE-NAME2 TRANSLATE INTO UPPER
                if (condition(((DbsUtil.maskMatches(bd_Bd_Bene_Name1,"'SAME AS'") || DbsUtil.maskMatches(bd_Bd_Bene_Name1,"'DEFAULT TO'")) || (bd_Bd_Bene_Name1.equals("ESTATE")  //Natural: IF BD-BENE-NAME1 = MASK ( 'SAME AS' ) OR = MASK ( 'DEFAULT TO' ) OR ( BD-BENE-NAME1 = 'ESTATE' AND BD-BENE-NAME2 = 'DEFAULT TO ESTATE' )
                    && bd_Bd_Bene_Name2.equals("DEFAULT TO ESTATE")))))
                {
                    pnd_Details_Pnd_Isn_Same_Dflt.setValue(vw_bd.getAstISN("RBD2"));                                                                                      //Natural: ASSIGN #ISN-SAME-DFLT := *ISN ( RBD2. )
                    pnd_Details_Pnd_Same_Dflt.setValue(true);                                                                                                             //Natural: ASSIGN #SAME-DFLT := TRUE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Details_Pnd_Same_Dflt.reset();                                                                                                                    //Natural: RESET #SAME-DFLT
                    //*      WRITE (0) BD.BD-PIN BD.BD-TIAA-CNTRCT BD.BD-BENE-NAME1
                    //*        BD.BD-RCRD-LAST-UPDT-DTE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RBC2"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RBC2"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Details_Pnd_Found_Ben_Tgt.getBoolean()))                                                                                                        //Natural: IF #FOUND-BEN-TGT
        {
            //*  DELETE SAME AS
            if (condition(pnd_Details_Pnd_Same_Dflt.getBoolean()))                                                                                                        //Natural: IF #SAME-DFLT
            {
                if (condition(pnd_Details_Pnd_Isn_Same_Dflt.greater(getZero())))                                                                                          //Natural: IF #ISN-SAME-DFLT > 0
                {
                    GBC:                                                                                                                                                  //Natural: GET BC #ISN-BC-DLT
                    vw_bc.readByID(pnd_Details_Pnd_Isn_Bc_Dlt.getLong(), "GBC");
                    vw_bc.deleteDBRow("GBC");                                                                                                                             //Natural: DELETE ( GBC. )
                    GBD:                                                                                                                                                  //Natural: GET BD #ISN-SAME-DFLT
                    vw_bd.readByID(pnd_Details_Pnd_Isn_Same_Dflt.getLong(), "GBD");
                    vw_bd.deleteDBRow("GBD");                                                                                                                             //Natural: DELETE ( GBD. )
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION
                    //*      BACKOUT TRANSACTION
                }                                                                                                                                                         //Natural: END-IF
                pnd_Details_Pnd_Found_Ben_Tgt.reset();                                                                                                                    //Natural: RESET #FOUND-BEN-TGT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Details_Pnd_Error.setValue(true);                                                                                                                     //Natural: ASSIGN #ERROR := TRUE
                pnd_Details_Pnd_Msg.setValue("Target BENE exists");                                                                                                       //Natural: ASSIGN #MSG := 'Target BENE exists'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  CHECK-BEN-PIN-CNTRCT-SRC-TGT
    }
    private void sub_Copy_Ben_Src_To_Tgt() throws Exception                                                                                                               //Natural: COPY-BEN-SRC-TO-TGT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Details_Pnd_Date.setValue(Global.getDATN());                                                                                                                  //Natural: ASSIGN #DATE := *DATN
        pnd_Details_Pnd_Time.setValue(Global.getTIMN());                                                                                                                  //Natural: ASSIGN #TIME := *TIMN
        GET01:                                                                                                                                                            //Natural: GET BC #ISN-BC
        vw_bc.readByID(pnd_Details_Pnd_Isn_Bc.getLong(), "GET01");
        vw_bca.reset();                                                                                                                                                   //Natural: RESET BCA
        //*  INTERFACE
        //*  ACIS
        vw_bca.setValuesByName(vw_bc);                                                                                                                                    //Natural: MOVE BY NAME BC TO BCA
        bca_Bc_Pin.setValue(pnd_Acis_Pnd_I_Pin);                                                                                                                          //Natural: ASSIGN BCA.BC-PIN := #I-PIN
        bca_Bc_Tiaa_Cntrct.setValue(pnd_Acis_Pnd_I_Tiaa_Tgt);                                                                                                             //Natural: ASSIGN BCA.BC-TIAA-CNTRCT := #I-TIAA-TGT
        bca_Bc_Cref_Cntrct.setValue(pnd_Acis_Pnd_I_Cref_Tgt);                                                                                                             //Natural: ASSIGN BCA.BC-CREF-CNTRCT := #I-CREF-TGT
        bca_Bc_Eff_Dte.setValue(pnd_Acis_Pnd_I_Eff_Date);                                                                                                                 //Natural: ASSIGN BCA.BC-EFF-DTE := #I-EFF-DATE
        bca_Bc_Rqst_Timestamp.setValue(pnd_Details_Pnd_Rldt);                                                                                                             //Natural: ASSIGN BCA.BC-RQST-TIMESTAMP := #RLDT
        bca_Bc_Rcrd_Last_Updt_Dte.setValue(pnd_Details_Pnd_Date);                                                                                                         //Natural: ASSIGN BCA.BC-RCRD-LAST-UPDT-DTE := #DATE
        bca_Bc_Rcrd_Last_Updt_Tme.setValue(pnd_Details_Pnd_Time);                                                                                                         //Natural: ASSIGN BCA.BC-RCRD-LAST-UPDT-TME := #TIME
        bca_Bc_Rcrd_Last_Updt_Userid.setValue(Global.getPROGRAM());                                                                                                       //Natural: ASSIGN BCA.BC-RCRD-LAST-UPDT-USERID := *PROGRAM
        bca_Bc_Last_Dsgntn_Srce.setValue("I");                                                                                                                            //Natural: ASSIGN BCA.BC-LAST-DSGNTN-SRCE := 'I'
        bca_Bc_Last_Dsgntn_System.setValue("A");                                                                                                                          //Natural: ASSIGN BCA.BC-LAST-DSGNTN-SYSTEM := 'A'
        bca_Bc_Last_Dsgntn_Userid.setValue(Global.getPROGRAM());                                                                                                          //Natural: ASSIGN BCA.BC-LAST-DSGNTN-USERID := *PROGRAM
        bca_Bc_Last_Dsgntn_Dte.setValue(pnd_Details_Pnd_Date);                                                                                                            //Natural: ASSIGN BCA.BC-LAST-DSGNTN-DTE := #DATE
        bca_Bc_Last_Dsgntn_Tme.setValue(pnd_Details_Pnd_Time);                                                                                                            //Natural: ASSIGN BCA.BC-LAST-DSGNTN-TME := #TIME
        bca_Bc_Mgrtn_Cnfrm_Stmnt_Prod_Dte.setValue(pnd_Details_Pnd_Date);                                                                                                 //Natural: ASSIGN BCA.BC-MGRTN-CNFRM-STMNT-PROD-DTE := #DATE
        //*  BENE-CONTRACT FILE
        vw_bca.insertDBRow();                                                                                                                                             //Natural: STORE BCA
        pnd_Details_Pnd_Add_Bc.setValue(true);                                                                                                                            //Natural: ASSIGN #ADD-BC := TRUE
        if (condition(bc_Bc_Mos_Ind.equals("Y") && pnd_Details_Pnd_Isn_Bm.notEquals(getZero())))                                                                          //Natural: IF BC.BC-MOS-IND = 'Y' AND #ISN-BM NE 0
        {
            GET02:                                                                                                                                                        //Natural: GET BM #ISN-BM
            vw_bm.readByID(pnd_Details_Pnd_Isn_Bm.getLong(), "GET02");
            vw_bma.reset();                                                                                                                                               //Natural: RESET BMA
            vw_bma.setValuesByName(vw_bm);                                                                                                                                //Natural: MOVE BY NAME BM TO BMA
            bma_Bm_Pin.setValue(pnd_Acis_Pnd_I_Pin);                                                                                                                      //Natural: ASSIGN BMA.BM-PIN := #I-PIN
            bma_Bm_Tiaa_Cntrct.setValue(pnd_Acis_Pnd_I_Tiaa_Tgt);                                                                                                         //Natural: ASSIGN BMA.BM-TIAA-CNTRCT := #I-TIAA-TGT
            bma_Bm_Cref_Cntrct.setValue(pnd_Acis_Pnd_I_Cref_Tgt);                                                                                                         //Natural: ASSIGN BMA.BM-CREF-CNTRCT := #I-CREF-TGT
            bma_Bm_Rqst_Timestamp.setValue(pnd_Details_Pnd_Rldt);                                                                                                         //Natural: ASSIGN BMA.BM-RQST-TIMESTAMP := #RLDT
            bma_Bm_Rcrd_Last_Updt_Dte.setValue(pnd_Details_Pnd_Date);                                                                                                     //Natural: ASSIGN BMA.BM-RCRD-LAST-UPDT-DTE := #DATE
            bma_Bm_Rcrd_Last_Updt_Tme.setValue(pnd_Details_Pnd_Time);                                                                                                     //Natural: ASSIGN BMA.BM-RCRD-LAST-UPDT-TME := #TIME
            bma_Bm_Rcrd_Last_Updt_Userid.setValue(Global.getPROGRAM());                                                                                                   //Natural: ASSIGN BMA.BM-RCRD-LAST-UPDT-USERID := *PROGRAM
            //*  BENE-MOS FILE
            vw_bma.insertDBRow();                                                                                                                                         //Natural: STORE BMA
            pnd_Details_Pnd_Add_Bm.setValue(true);                                                                                                                        //Natural: ASSIGN #ADD-BM := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            FOR01:                                                                                                                                                        //Natural: FOR #SEQ = 1 TO 30
            for (pnd_Details_Pnd_Seq.setValue(1); condition(pnd_Details_Pnd_Seq.lessOrEqual(30)); pnd_Details_Pnd_Seq.nadd(1))
            {
                if (condition(pnd_Details_Pnd_Isn_Bd.getValue(pnd_Details_Pnd_Seq).greater(getZero())))                                                                   //Natural: IF #ISN-BD ( #SEQ ) > 0
                {
                    GET03:                                                                                                                                                //Natural: GET BD #ISN-BD ( #SEQ )
                    vw_bd.readByID(pnd_Details_Pnd_Isn_Bd.getValue(pnd_Details_Pnd_Seq).getLong(), "GET03");
                    vw_bda.reset();                                                                                                                                       //Natural: RESET BDA
                    vw_bda.setValuesByName(vw_bd);                                                                                                                        //Natural: MOVE BY NAME BD TO BDA
                    bda_Bd_Pin.setValue(pnd_Acis_Pnd_I_Pin);                                                                                                              //Natural: ASSIGN BDA.BD-PIN := #I-PIN
                    bda_Bd_Tiaa_Cntrct.setValue(pnd_Acis_Pnd_I_Tiaa_Tgt);                                                                                                 //Natural: ASSIGN BDA.BD-TIAA-CNTRCT := #I-TIAA-TGT
                    bda_Bd_Cref_Cntrct.setValue(pnd_Acis_Pnd_I_Cref_Tgt);                                                                                                 //Natural: ASSIGN BDA.BD-CREF-CNTRCT := #I-CREF-TGT
                    bda_Bd_Rqst_Timestamp.setValue(pnd_Details_Pnd_Rldt);                                                                                                 //Natural: ASSIGN BDA.BD-RQST-TIMESTAMP := #RLDT
                    bda_Bd_Rcrd_Last_Updt_Dte.setValue(pnd_Details_Pnd_Date);                                                                                             //Natural: ASSIGN BDA.BD-RCRD-LAST-UPDT-DTE := #DATE
                    bda_Bd_Rcrd_Last_Updt_Tme.setValue(pnd_Details_Pnd_Time);                                                                                             //Natural: ASSIGN BDA.BD-RCRD-LAST-UPDT-TME := #TIME
                    bda_Bd_Rcrd_Last_Updt_Userid.setValue(Global.getPROGRAM());                                                                                           //Natural: ASSIGN BDA.BD-RCRD-LAST-UPDT-USERID := *PROGRAM
                    //*  BENE-DESIGNATION FILE
                    vw_bda.insertDBRow();                                                                                                                                 //Natural: STORE BDA
                    pnd_Details_Pnd_Add_Bd.setValue(true);                                                                                                                //Natural: ASSIGN #ADD-BD := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Details_Pnd_Add_Bc.getBoolean() && (pnd_Details_Pnd_Add_Bd.getBoolean() || pnd_Details_Pnd_Add_Bm.getBoolean())))                               //Natural: IF #ADD-BC AND ( #ADD-BD OR #ADD-BM )
        {
            pnd_Details_Pnd_Et.setValue(true);                                                                                                                            //Natural: ASSIGN #ET := TRUE
            pnd_Details_Pnd_Msg.setValue(DbsUtil.compress(pnd_Details_Pnd_Msg, "Source copied to Target;"));                                                              //Natural: COMPRESS #MSG 'Source copied to Target;' INTO #MSG
            pnd_Totals_Pnd_Total_Copied.nadd(1);                                                                                                                          //Natural: ADD 1 TO #TOTAL-COPIED
            if (condition(pnd_Details_Pnd_Same_Dflt.getBoolean()))                                                                                                        //Natural: IF #SAME-DFLT
            {
                pnd_Totals_Pnd_Total_Same_Dflt.nadd(1);                                                                                                                   //Natural: ADD 1 TO #TOTAL-SAME-DFLT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Details_Pnd_Et.reset();                                                                                                                                   //Natural: RESET #ET
            pnd_Details_Pnd_Msg.setValue(DbsUtil.compress(pnd_Details_Pnd_Msg, "BC", pnd_Details_Pnd_Isn_Bc, "BD", pnd_Details_Pnd_Isn_Bd.getValue(1),                    //Natural: COMPRESS #MSG 'BC' #ISN-BC 'BD' #ISN-BD ( 1 ) 'BM' #ISN-BM INTO #MSG
                "BM", pnd_Details_Pnd_Isn_Bm));
                                                                                                                                                                          //Natural: PERFORM PRINT-ERROR
            sub_Print_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  COPY-BEN-SRC-TO-TGT
    }
    private void sub_Print_Error() throws Exception                                                                                                                       //Natural: PRINT-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().display(0, ReportOption.NOTITLE,"Plan",                                                                                                              //Natural: DISPLAY ( 0 ) NOTITLE 'Plan' #I-PLAN 'PIN' #I-PIN 'TIAA Src' #I-TIAA-SRC 'CREF Src' #I-CREF-SRC 'TIAA Tgt' #I-TIAA-TGT 'CREF Tgt' #I-CREF-TGT 'Eff Date' #I-EFF-DATE 'Last Date' #LAST-DATE 'Message' #MSG
        		pnd_Acis_Pnd_I_Plan,"PIN",
        		pnd_Acis_Pnd_I_Pin,"TIAA Src",
        		pnd_Acis_Pnd_I_Tiaa_Src,"CREF Src",
        		pnd_Acis_Pnd_I_Cref_Src,"TIAA Tgt",
        		pnd_Acis_Pnd_I_Tiaa_Tgt,"CREF Tgt",
        		pnd_Acis_Pnd_I_Cref_Tgt,"Eff Date",
        		pnd_Acis_Pnd_I_Eff_Date,"Last Date",
        		pnd_Details_Pnd_Last_Date,"Message",
        		pnd_Details_Pnd_Msg);
        if (Global.isEscape()) return;
        pnd_Totals_Pnd_Total_Error.nadd(1);                                                                                                                               //Natural: ADD 1 TO #TOTAL-ERROR
        //*  COPY-BEN-SRC-TO-TGT
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=60 SG=OFF ZP=ON");
        Global.format(1, "LS=133 PS=60 SG=OFF ZP=ON");

        getReports().setDisplayColumns(1, ReportOption.NOTITLE,"PIN",
        		pnd_Acis_Pnd_I_Pin,"TIAA Src",
        		pnd_Acis_Pnd_I_Tiaa_Src,"CREF Src",
        		pnd_Acis_Pnd_I_Cref_Src,"TIAA Tgt",
        		pnd_Acis_Pnd_I_Tiaa_Tgt,"CREF Tgt",
        		pnd_Acis_Pnd_I_Cref_Tgt,"Eff Date",
        		pnd_Acis_Pnd_I_Eff_Date,"Message",
        		pnd_Details_Pnd_Msg);
        getReports().setDisplayColumns(0, ReportOption.NOTITLE,"Plan",
        		pnd_Acis_Pnd_I_Plan,"PIN",
        		pnd_Acis_Pnd_I_Pin,"TIAA Src",
        		pnd_Acis_Pnd_I_Tiaa_Src,"CREF Src",
        		pnd_Acis_Pnd_I_Cref_Src,"TIAA Tgt",
        		pnd_Acis_Pnd_I_Tiaa_Tgt,"CREF Tgt",
        		pnd_Acis_Pnd_I_Cref_Tgt,"Eff Date",
        		pnd_Acis_Pnd_I_Eff_Date,"Last Date",
        		pnd_Details_Pnd_Last_Date,"Message",
        		pnd_Details_Pnd_Msg);
    }
}
