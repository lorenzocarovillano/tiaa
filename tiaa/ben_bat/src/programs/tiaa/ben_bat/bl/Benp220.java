/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:14:29 PM
**        * FROM NATURAL PROGRAM : Benp220
************************************************************
**        * FILE NAME            : Benp220.java
**        * CLASS NAME           : Benp220
**        * INSTANCE NAME        : Benp220
************************************************************
************************************************************************
* PROGRAM NAME : BENP220
* DESCRIPTION  : BENEFICIARY BATCH SYSTEM - CALL POST FOR CONFIRMATION
*                          STATEMENTS (REG. MAIL) AND BENP220 ERROR REP.
* WRITTEN BY   : JULIAN WEBB
* DATE WRITTEN : 06/14/1999
************************************************************************
*    DATE      USERID                   DESCRIPTION
* 09/23/1999  EATONA    AMEND PROGRAM TO PRINT PAGE PER UNIT
* 12/28/1999  GREIFF    REJECT IF BA-VRFY-IND EQ '4'
*                       ADDED RQST CMPLTD DTE TO REPORT TRUNCATE NAME
* 12/10/2001  FONTOUR   REMOVE MDO-CALC-BENE FIELDS
* 03/12/2003  FONTOUR   REJECT BA-RQST-CMPLTD-DTE GREATER THAN CNTRL DTE
* 04/20/2005  O SOTTO   FIX PROD PROBLEM - PAGE CTR OVERFLOW.
* 11/28/2006  O SOTTO   FIX FOR PROD PROBLEM. SC 112806.
* 12/21/2006  O SOTTO   RECOMPILED FOR BENG200 CHANGES.
* 09/15/2010  DURAND    ADD #BENE-FIX-DTE TO GDA & PDA
* 10/22/2012  ALAGANI   RE-STOWED DUE TO #BENE-GENDER #BENE-ADDR1
*                       #BENE-ADDR2  #BENE-ADDR3-CITY #BENE-COUNTRY
*                       #BENE-STATE #BENE-ZIP #BENE-PHONE WERE ADDED
*                       TO BENA002
* 04/01/2017  DURAND    PIN EXPANSION N7 TO N12
* MM/DD/YYYY  XXXXXXXX  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
***********************************************************************

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benp220 extends BLNatBase
{
    // Data Areas
    private GdaBeng200 gdaBeng200;
    private PdaBena011 pdaBena011;
    private PdaBena002 pdaBena002;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cnct;
    private DbsField cnct_Bc_Cref_Cntrct;
    private DbsField cnct_Bc_Pin;
    private DbsField cnct_Bc_Stat;
    private DbsField cnct_Bc_Rqst_Timestamp;
    private DbsField cnct_Bc_Eff_Dte;
    private DbsField cnct_Bc_Pymnt_Chld_Dcsd_Ind;
    private DbsField cnct_Bc_Spouse_Waived_Bnfts;
    private DbsField cnct_Bc_Mos_Ind;
    private DbsField cnct_Bc_Irvcbl_Ind;
    private DbsField cnct_Bc_Tiaa_Cntrct;
    private DbsField cnct_Bc_Tiaa_Cref_Ind;
    private DbsField pnd_Bc_S_P_C_I_Key;

    private DbsGroup pnd_Bc_S_P_C_I_Key__R_Field_1;
    private DbsField pnd_Bc_S_P_C_I_Key_Pnd_Bc_Stat;
    private DbsField pnd_Bc_S_P_C_I_Key_Pnd_Bc_Pin;
    private DbsField pnd_Bc_S_P_C_I_Key_Pnd_Bc_Tiaa_Cntrct;
    private DbsField pnd_Bc_S_P_C_I_Key_Pnd_Bc_Tiaa_Cref_Ind;

    private DataAccessProgramView vw_pnd_Ben;
    private DbsField pnd_Ben_Bd_Pin;
    private DbsField pnd_Ben_Bd_Tiaa_Cntrct;
    private DbsField pnd_Ben_Bd_Cref_Cntrct;
    private DbsField pnd_Ben_Bd_Tiaa_Cref_Ind;
    private DbsField pnd_Ben_Bd_Stat;
    private DbsField pnd_Ben_Bd_Seq_Nmbr;
    private DbsField pnd_Ben_Bd_Rqst_Timestamp;
    private DbsField pnd_Ben_Bd_Bene_Type;
    private DbsField pnd_Ben_Bd_Rltn_Cd;
    private DbsField pnd_Ben_Bd_Bene_Name1;
    private DbsField pnd_Ben_Bd_Bene_Name2;
    private DbsField pnd_Ben_Bd_Rltn_Free_Txt;
    private DbsField pnd_Ben_Bd_Dte_Birth_Trust;
    private DbsField pnd_Ben_Bd_Ss_Cd;
    private DbsField pnd_Ben_Bd_Ss_Nmbr;
    private DbsField pnd_Ben_Bd_Perc_Share_Ind;
    private DbsField pnd_Ben_Bd_Share_Perc;
    private DbsField pnd_Ben_Bd_Share_Ntor;
    private DbsField pnd_Ben_Bd_Share_Dtor;
    private DbsField pnd_Ben_Bd_Irvcbl_Ind;
    private DbsField pnd_Ben_Bd_Stlmnt_Rstrctn;
    private DbsField pnd_Ben_Bd_Spcl_Txt1;
    private DbsField pnd_Ben_Bd_Spcl_Txt2;
    private DbsField pnd_Ben_Bd_Spcl_Txt3;
    private DbsField pnd_Ben_Bd_Dflt_Estate;
    private DbsField pnd_Bd_S_P_C_I_T_S;

    private DbsGroup pnd_Bd_S_P_C_I_T_S__R_Field_2;
    private DbsField pnd_Bd_S_P_C_I_T_S_Pnd_Bd_Stat;
    private DbsField pnd_Bd_S_P_C_I_T_S_Pnd_Bd_Pin;
    private DbsField pnd_Bd_S_P_C_I_T_S_Pnd_Bd_Tiaa_Cntrct;
    private DbsField pnd_Bd_S_P_C_I_T_S_Pnd_Bd_Tiaa_Cref_Ind;

    private DataAccessProgramView vw_pnd_Mos;
    private DbsField pnd_Mos_Bm_Pin;
    private DbsField pnd_Mos_Bm_Tiaa_Cntrct;
    private DbsField pnd_Mos_Bm_Tiaa_Cref_Ind;
    private DbsField pnd_Mos_Bm_Stat;
    private DbsField pnd_Mos_Bm_Rqst_Timestamp;

    private DbsGroup pnd_Mos_Bm_Mos_Txt_Group;
    private DbsField pnd_Mos_Bm_Mos_Txt;
    private DbsField pnd_Mos_Count_Castbm_Mos_Txt_Group;
    private DbsField pnd_Bm_S_P_C_I_Key;

    private DbsGroup pnd_Bm_S_P_C_I_Key__R_Field_3;
    private DbsField pnd_Bm_S_P_C_I_Key_Pnd_Bm_Stat;
    private DbsField pnd_Bm_S_P_C_I_Key_Pnd_Bm_Pin;
    private DbsField pnd_Bm_S_P_C_I_Key_Pnd_Bm_Tiaa_Cntrct;
    private DbsField pnd_Bm_S_P_C_I_Key_Pnd_Bm_Tiaa_Cref_Ind;

    private DataAccessProgramView vw_bt;
    private DbsField bt_Bt_Table_Id;
    private DbsField bt_Bt_Table_Key;
    private DbsField bt_Bt_Table_Text;
    private DbsField pnd_Bt_Table_Id_Key;
    private DbsField pnd_Error_Line;

    private DbsGroup pnd_Error_Line__R_Field_4;
    private DbsField pnd_Error_Line_Pnd_Error_Name;
    private DbsField pnd_Error_Line_Pnd_Error_Pin;
    private DbsField pnd_Error_Line_Pnd_Error_Dte;
    private DbsField pnd_Error_Line_Pnd_Err_Cntrct;
    private DbsField pnd_Error_Line_Pnd_Error_Wpid;
    private DbsField pnd_Error_Line_Pnd_Error_Wpid_Unit;
    private DbsField pnd_Error_Line_Pnd_Error_Msg;

    private DbsGroup pnd_Error_Line__R_Field_5;
    private DbsField pnd_Error_Line_Pnd_Error_Msg_Arr;
    private DbsField pnd_Old_Dte;
    private DbsField pnd_Error_Hold;

    private DbsGroup pnd_Error_Hold__R_Field_6;
    private DbsField pnd_Error_Hold_Pnd_Error_Hold_Arr;
    private DbsField pnd_Ind;
    private DbsField pnd_P;
    private DbsField pnd_N;
    private DbsField pnd_M;
    private DbsField pnd_K;
    private DbsField pnd_L;
    private DbsField pnd_Frm_Ind;
    private DbsField pnd_End_Ind;
    private DbsField pnd_Page_Number;
    private DbsField pnd_Line_Err;
    private DbsField pnd_A_Cnt;
    private DbsField pnd_Et_Cnt;
    private DbsField pnd_E_Cnt;
    private DbsField pnd_Cref_Only;
    private DbsField pnd_Error;
    private DbsField pnd_Cont_Found;
    private DbsField pnd_Isn;
    private DbsField pnd_Slctd_Tiaa;
    private DbsField pnd_Slctd_Cref;
    private DbsField pnd_Cont_Stat;
    private DbsField pnd_Alpha;

    private DbsGroup pnd_Alpha__R_Field_7;
    private DbsField pnd_Alpha_Pnd_Alpha_N;

    private DbsRecord internalLoopRecord;
    private DbsField s1Ba_Rqst_Cmpltd_DteOld;
    private DbsField s1Ba_Seq_NmbrOld;
    private DbsField s1Count_Castba_Slctd_Cntrct_GroupOld;
    private DbsField s1Act_Ba_Slctd_TiaaOld;
    private DbsField s1Act_Ba_Slctd_CrefOld;
    private DbsField s1Act_Ba_Wpid_UnitOld;
    private DbsField s1Act_Ba_WpidOld;
    private DbsField s1Act_Ba_NameOld;
    private DbsField s1Act_Ba_Owner_NameOld;
    private DbsField s1Ba_Rqst_TimestampOld;
    private DbsField s1Act_Ba_Lttr_InfoOld;
    private DbsField s1Act_Ba_Mail_AddrOld;
    private DbsField s1Act_Ba_Us_FrgnOld;
    private DbsField s1Act_Ba_Addrss_Postal_DataOld;
    private DbsField s1Act_Ba_Rcrd_Last_Updt_UseridOld;
    private DbsField s1Act_Ba_Diff_DsgntnOld;
    private DbsField s1Ba_PinOld;
    private DbsField s1Act_Ba_Lttr_OptnOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaBeng200 = GdaBeng200.getInstance(getCallnatLevel());
        registerRecord(gdaBeng200);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaBena011 = new PdaBena011(localVariables);
        pdaBena002 = new PdaBena002(localVariables);

        // Local Variables

        vw_cnct = new DataAccessProgramView(new NameInfo("vw_cnct", "CNCT"), "BENE_CONTRACT_12", "BENE_CONTRACT");
        cnct_Bc_Cref_Cntrct = vw_cnct.getRecord().newFieldInGroup("cnct_Bc_Cref_Cntrct", "BC-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BC_CREF_CNTRCT");
        cnct_Bc_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        cnct_Bc_Pin = vw_cnct.getRecord().newFieldInGroup("cnct_Bc_Pin", "BC-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BC_PIN");
        cnct_Bc_Pin.setDdmHeader("PIN");
        cnct_Bc_Stat = vw_cnct.getRecord().newFieldInGroup("cnct_Bc_Stat", "BC-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_STAT");
        cnct_Bc_Stat.setDdmHeader("STATUS");
        cnct_Bc_Rqst_Timestamp = vw_cnct.getRecord().newFieldInGroup("cnct_Bc_Rqst_Timestamp", "BC-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BC_RQST_TIMESTAMP");
        cnct_Bc_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        cnct_Bc_Eff_Dte = vw_cnct.getRecord().newFieldInGroup("cnct_Bc_Eff_Dte", "BC-EFF-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "BC_EFF_DTE");
        cnct_Bc_Eff_Dte.setDdmHeader("EFFECTIVE/DATE");
        cnct_Bc_Pymnt_Chld_Dcsd_Ind = vw_cnct.getRecord().newFieldInGroup("cnct_Bc_Pymnt_Chld_Dcsd_Ind", "BC-PYMNT-CHLD-DCSD-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "BC_PYMNT_CHLD_DCSD_IND");
        cnct_Bc_Pymnt_Chld_Dcsd_Ind.setDdmHeader("PYMNT/CHILD/DCSD");
        cnct_Bc_Spouse_Waived_Bnfts = vw_cnct.getRecord().newFieldInGroup("cnct_Bc_Spouse_Waived_Bnfts", "BC-SPOUSE-WAIVED-BNFTS", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "BC_SPOUSE_WAIVED_BNFTS");
        cnct_Bc_Spouse_Waived_Bnfts.setDdmHeader("SPOUSE/WAIVED/BENEFITS");
        cnct_Bc_Mos_Ind = vw_cnct.getRecord().newFieldInGroup("cnct_Bc_Mos_Ind", "BC-MOS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_MOS_IND");
        cnct_Bc_Mos_Ind.setDdmHeader("MOS/IND");
        cnct_Bc_Irvcbl_Ind = vw_cnct.getRecord().newFieldInGroup("cnct_Bc_Irvcbl_Ind", "BC-IRVCBL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_IRVCBL_IND");
        cnct_Bc_Irvcbl_Ind.setDdmHeader("IRVCBL/IND");
        cnct_Bc_Tiaa_Cntrct = vw_cnct.getRecord().newFieldInGroup("cnct_Bc_Tiaa_Cntrct", "BC-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BC_TIAA_CNTRCT");
        cnct_Bc_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        cnct_Bc_Tiaa_Cref_Ind = vw_cnct.getRecord().newFieldInGroup("cnct_Bc_Tiaa_Cref_Ind", "BC-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_TIAA_CREF_IND");
        cnct_Bc_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        registerRecord(vw_cnct);

        pnd_Bc_S_P_C_I_Key = localVariables.newFieldInRecord("pnd_Bc_S_P_C_I_Key", "#BC-S-P-C-I-KEY", FieldType.STRING, 24);

        pnd_Bc_S_P_C_I_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Bc_S_P_C_I_Key__R_Field_1", "REDEFINE", pnd_Bc_S_P_C_I_Key);
        pnd_Bc_S_P_C_I_Key_Pnd_Bc_Stat = pnd_Bc_S_P_C_I_Key__R_Field_1.newFieldInGroup("pnd_Bc_S_P_C_I_Key_Pnd_Bc_Stat", "#BC-STAT", FieldType.STRING, 
            1);
        pnd_Bc_S_P_C_I_Key_Pnd_Bc_Pin = pnd_Bc_S_P_C_I_Key__R_Field_1.newFieldInGroup("pnd_Bc_S_P_C_I_Key_Pnd_Bc_Pin", "#BC-PIN", FieldType.NUMERIC, 12);
        pnd_Bc_S_P_C_I_Key_Pnd_Bc_Tiaa_Cntrct = pnd_Bc_S_P_C_I_Key__R_Field_1.newFieldInGroup("pnd_Bc_S_P_C_I_Key_Pnd_Bc_Tiaa_Cntrct", "#BC-TIAA-CNTRCT", 
            FieldType.STRING, 10);
        pnd_Bc_S_P_C_I_Key_Pnd_Bc_Tiaa_Cref_Ind = pnd_Bc_S_P_C_I_Key__R_Field_1.newFieldInGroup("pnd_Bc_S_P_C_I_Key_Pnd_Bc_Tiaa_Cref_Ind", "#BC-TIAA-CREF-IND", 
            FieldType.STRING, 1);

        vw_pnd_Ben = new DataAccessProgramView(new NameInfo("vw_pnd_Ben", "#BEN"), "BENE_DESIGNATION_12", "BENE_DESIGNATION");
        pnd_Ben_Bd_Pin = vw_pnd_Ben.getRecord().newFieldInGroup("pnd_Ben_Bd_Pin", "BD-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BD_PIN");
        pnd_Ben_Bd_Pin.setDdmHeader("PIN");
        pnd_Ben_Bd_Tiaa_Cntrct = vw_pnd_Ben.getRecord().newFieldInGroup("pnd_Ben_Bd_Tiaa_Cntrct", "BD-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BD_TIAA_CNTRCT");
        pnd_Ben_Bd_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        pnd_Ben_Bd_Cref_Cntrct = vw_pnd_Ben.getRecord().newFieldInGroup("pnd_Ben_Bd_Cref_Cntrct", "BD-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BD_CREF_CNTRCT");
        pnd_Ben_Bd_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        pnd_Ben_Bd_Tiaa_Cref_Ind = vw_pnd_Ben.getRecord().newFieldInGroup("pnd_Ben_Bd_Tiaa_Cref_Ind", "BD-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_TIAA_CREF_IND");
        pnd_Ben_Bd_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        pnd_Ben_Bd_Stat = vw_pnd_Ben.getRecord().newFieldInGroup("pnd_Ben_Bd_Stat", "BD-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_STAT");
        pnd_Ben_Bd_Stat.setDdmHeader("STAT");
        pnd_Ben_Bd_Seq_Nmbr = vw_pnd_Ben.getRecord().newFieldInGroup("pnd_Ben_Bd_Seq_Nmbr", "BD-SEQ-NMBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "BD_SEQ_NMBR");
        pnd_Ben_Bd_Seq_Nmbr.setDdmHeader(" SEQ/NMBR");
        pnd_Ben_Bd_Rqst_Timestamp = vw_pnd_Ben.getRecord().newFieldInGroup("pnd_Ben_Bd_Rqst_Timestamp", "BD-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BD_RQST_TIMESTAMP");
        pnd_Ben_Bd_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        pnd_Ben_Bd_Bene_Type = vw_pnd_Ben.getRecord().newFieldInGroup("pnd_Ben_Bd_Bene_Type", "BD-BENE-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_BENE_TYPE");
        pnd_Ben_Bd_Bene_Type.setDdmHeader("BENE/TYPE");
        pnd_Ben_Bd_Rltn_Cd = vw_pnd_Ben.getRecord().newFieldInGroup("pnd_Ben_Bd_Rltn_Cd", "BD-RLTN-CD", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "BD_RLTN_CD");
        pnd_Ben_Bd_Rltn_Cd.setDdmHeader("RELATION/CODE");
        pnd_Ben_Bd_Bene_Name1 = vw_pnd_Ben.getRecord().newFieldInGroup("pnd_Ben_Bd_Bene_Name1", "BD-BENE-NAME1", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "BD_BENE_NAME1");
        pnd_Ben_Bd_Bene_Name1.setDdmHeader("BENEFICIARY/NAME 1");
        pnd_Ben_Bd_Bene_Name2 = vw_pnd_Ben.getRecord().newFieldInGroup("pnd_Ben_Bd_Bene_Name2", "BD-BENE-NAME2", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "BD_BENE_NAME2");
        pnd_Ben_Bd_Bene_Name2.setDdmHeader("BENEFICIARY/NAME 2");
        pnd_Ben_Bd_Rltn_Free_Txt = vw_pnd_Ben.getRecord().newFieldInGroup("pnd_Ben_Bd_Rltn_Free_Txt", "BD-RLTN-FREE-TXT", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BD_RLTN_FREE_TXT");
        pnd_Ben_Bd_Rltn_Free_Txt.setDdmHeader("OTHER/RELATION");
        pnd_Ben_Bd_Dte_Birth_Trust = vw_pnd_Ben.getRecord().newFieldInGroup("pnd_Ben_Bd_Dte_Birth_Trust", "BD-DTE-BIRTH-TRUST", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BD_DTE_BIRTH_TRUST");
        pnd_Ben_Bd_Dte_Birth_Trust.setDdmHeader("DOB/DOT");
        pnd_Ben_Bd_Ss_Cd = vw_pnd_Ben.getRecord().newFieldInGroup("pnd_Ben_Bd_Ss_Cd", "BD-SS-CD", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_SS_CD");
        pnd_Ben_Bd_Ss_Cd.setDdmHeader("SS/CODE");
        pnd_Ben_Bd_Ss_Nmbr = vw_pnd_Ben.getRecord().newFieldInGroup("pnd_Ben_Bd_Ss_Nmbr", "BD-SS-NMBR", FieldType.STRING, 9, RepeatingFieldStrategy.None, 
            "BD_SS_NMBR");
        pnd_Ben_Bd_Ss_Nmbr.setDdmHeader("SS/NUMBER");
        pnd_Ben_Bd_Perc_Share_Ind = vw_pnd_Ben.getRecord().newFieldInGroup("pnd_Ben_Bd_Perc_Share_Ind", "BD-PERC-SHARE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_PERC_SHARE_IND");
        pnd_Ben_Bd_Perc_Share_Ind.setDdmHeader("PERCNT/SHARE/IND");
        pnd_Ben_Bd_Share_Perc = vw_pnd_Ben.getRecord().newFieldInGroup("pnd_Ben_Bd_Share_Perc", "BD-SHARE-PERC", FieldType.NUMERIC, 5, 2, RepeatingFieldStrategy.None, 
            "BD_SHARE_PERC");
        pnd_Ben_Bd_Share_Perc.setDdmHeader("SHARE/PERCENT");
        pnd_Ben_Bd_Share_Ntor = vw_pnd_Ben.getRecord().newFieldInGroup("pnd_Ben_Bd_Share_Ntor", "BD-SHARE-NTOR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "BD_SHARE_NTOR");
        pnd_Ben_Bd_Share_Ntor.setDdmHeader("SHARE/NUMERATOR");
        pnd_Ben_Bd_Share_Dtor = vw_pnd_Ben.getRecord().newFieldInGroup("pnd_Ben_Bd_Share_Dtor", "BD-SHARE-DTOR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "BD_SHARE_DTOR");
        pnd_Ben_Bd_Share_Dtor.setDdmHeader("SHARE/DENOMINATOR");
        pnd_Ben_Bd_Irvcbl_Ind = vw_pnd_Ben.getRecord().newFieldInGroup("pnd_Ben_Bd_Irvcbl_Ind", "BD-IRVCBL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_IRVCBL_IND");
        pnd_Ben_Bd_Irvcbl_Ind.setDdmHeader("IRVCBL/IND");
        pnd_Ben_Bd_Stlmnt_Rstrctn = vw_pnd_Ben.getRecord().newFieldInGroup("pnd_Ben_Bd_Stlmnt_Rstrctn", "BD-STLMNT-RSTRCTN", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_STLMNT_RSTRCTN");
        pnd_Ben_Bd_Stlmnt_Rstrctn.setDdmHeader("SETTLEMENT/RESTRICTION");
        pnd_Ben_Bd_Spcl_Txt1 = vw_pnd_Ben.getRecord().newFieldInGroup("pnd_Ben_Bd_Spcl_Txt1", "BD-SPCL-TXT1", FieldType.STRING, 72, RepeatingFieldStrategy.None, 
            "BD_SPCL_TXT1");
        pnd_Ben_Bd_Spcl_Txt1.setDdmHeader("SPECIAL TEXT 1");
        pnd_Ben_Bd_Spcl_Txt2 = vw_pnd_Ben.getRecord().newFieldInGroup("pnd_Ben_Bd_Spcl_Txt2", "BD-SPCL-TXT2", FieldType.STRING, 72, RepeatingFieldStrategy.None, 
            "BD_SPCL_TXT2");
        pnd_Ben_Bd_Spcl_Txt2.setDdmHeader("SPECIAL TEXT 2");
        pnd_Ben_Bd_Spcl_Txt3 = vw_pnd_Ben.getRecord().newFieldInGroup("pnd_Ben_Bd_Spcl_Txt3", "BD-SPCL-TXT3", FieldType.STRING, 72, RepeatingFieldStrategy.None, 
            "BD_SPCL_TXT3");
        pnd_Ben_Bd_Spcl_Txt3.setDdmHeader("SPECIAL TEXT 3");
        pnd_Ben_Bd_Dflt_Estate = vw_pnd_Ben.getRecord().newFieldInGroup("pnd_Ben_Bd_Dflt_Estate", "BD-DFLT-ESTATE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_DFLT_ESTATE");
        pnd_Ben_Bd_Dflt_Estate.setDdmHeader("DEFAULT/ESTATE");
        registerRecord(vw_pnd_Ben);

        pnd_Bd_S_P_C_I_T_S = localVariables.newFieldInRecord("pnd_Bd_S_P_C_I_T_S", "#BD-S-P-C-I-T-S", FieldType.STRING, 24);

        pnd_Bd_S_P_C_I_T_S__R_Field_2 = localVariables.newGroupInRecord("pnd_Bd_S_P_C_I_T_S__R_Field_2", "REDEFINE", pnd_Bd_S_P_C_I_T_S);
        pnd_Bd_S_P_C_I_T_S_Pnd_Bd_Stat = pnd_Bd_S_P_C_I_T_S__R_Field_2.newFieldInGroup("pnd_Bd_S_P_C_I_T_S_Pnd_Bd_Stat", "#BD-STAT", FieldType.STRING, 
            1);
        pnd_Bd_S_P_C_I_T_S_Pnd_Bd_Pin = pnd_Bd_S_P_C_I_T_S__R_Field_2.newFieldInGroup("pnd_Bd_S_P_C_I_T_S_Pnd_Bd_Pin", "#BD-PIN", FieldType.NUMERIC, 12);
        pnd_Bd_S_P_C_I_T_S_Pnd_Bd_Tiaa_Cntrct = pnd_Bd_S_P_C_I_T_S__R_Field_2.newFieldInGroup("pnd_Bd_S_P_C_I_T_S_Pnd_Bd_Tiaa_Cntrct", "#BD-TIAA-CNTRCT", 
            FieldType.STRING, 10);
        pnd_Bd_S_P_C_I_T_S_Pnd_Bd_Tiaa_Cref_Ind = pnd_Bd_S_P_C_I_T_S__R_Field_2.newFieldInGroup("pnd_Bd_S_P_C_I_T_S_Pnd_Bd_Tiaa_Cref_Ind", "#BD-TIAA-CREF-IND", 
            FieldType.STRING, 1);

        vw_pnd_Mos = new DataAccessProgramView(new NameInfo("vw_pnd_Mos", "#MOS"), "BENE_MOS_12", "BENE_MOS", DdmPeriodicGroups.getInstance().getGroups("BENE_MOS_12"));
        pnd_Mos_Bm_Pin = vw_pnd_Mos.getRecord().newFieldInGroup("pnd_Mos_Bm_Pin", "BM-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BM_PIN");
        pnd_Mos_Bm_Pin.setDdmHeader("PIN");
        pnd_Mos_Bm_Tiaa_Cntrct = vw_pnd_Mos.getRecord().newFieldInGroup("pnd_Mos_Bm_Tiaa_Cntrct", "BM-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BM_TIAA_CNTRCT");
        pnd_Mos_Bm_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        pnd_Mos_Bm_Tiaa_Cref_Ind = vw_pnd_Mos.getRecord().newFieldInGroup("pnd_Mos_Bm_Tiaa_Cref_Ind", "BM-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BM_TIAA_CREF_IND");
        pnd_Mos_Bm_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        pnd_Mos_Bm_Stat = vw_pnd_Mos.getRecord().newFieldInGroup("pnd_Mos_Bm_Stat", "BM-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BM_STAT");
        pnd_Mos_Bm_Stat.setDdmHeader("STAT");
        pnd_Mos_Bm_Rqst_Timestamp = vw_pnd_Mos.getRecord().newFieldInGroup("pnd_Mos_Bm_Rqst_Timestamp", "BM-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BM_RQST_TIMESTAMP");
        pnd_Mos_Bm_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");

        pnd_Mos_Bm_Mos_Txt_Group = vw_pnd_Mos.getRecord().newGroupInGroup("pnd_Mos_Bm_Mos_Txt_Group", "BM-MOS-TXT-GROUP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "BENE_MOS_BM_MOS_TXT_GROUP");
        pnd_Mos_Bm_Mos_Txt = pnd_Mos_Bm_Mos_Txt_Group.newFieldArrayInGroup("pnd_Mos_Bm_Mos_Txt", "BM-MOS-TXT", FieldType.STRING, 72, new DbsArrayController(1, 
            60) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "BM_MOS_TXT", "BENE_MOS_BM_MOS_TXT_GROUP");
        pnd_Mos_Bm_Mos_Txt.setDdmHeader("MOS TEXT");
        pnd_Mos_Count_Castbm_Mos_Txt_Group = pnd_Mos_Bm_Mos_Txt_Group.newFieldInGroup("pnd_Mos_Count_Castbm_Mos_Txt_Group", "C*BM-MOS-TXT-GROUP", RepeatingFieldStrategy.CAsteriskVariable, 
            "BENE_MOS_BM_MOS_TXT_GROUP");
        registerRecord(vw_pnd_Mos);

        pnd_Bm_S_P_C_I_Key = localVariables.newFieldInRecord("pnd_Bm_S_P_C_I_Key", "#BM-S-P-C-I-KEY", FieldType.STRING, 24);

        pnd_Bm_S_P_C_I_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Bm_S_P_C_I_Key__R_Field_3", "REDEFINE", pnd_Bm_S_P_C_I_Key);
        pnd_Bm_S_P_C_I_Key_Pnd_Bm_Stat = pnd_Bm_S_P_C_I_Key__R_Field_3.newFieldInGroup("pnd_Bm_S_P_C_I_Key_Pnd_Bm_Stat", "#BM-STAT", FieldType.STRING, 
            1);
        pnd_Bm_S_P_C_I_Key_Pnd_Bm_Pin = pnd_Bm_S_P_C_I_Key__R_Field_3.newFieldInGroup("pnd_Bm_S_P_C_I_Key_Pnd_Bm_Pin", "#BM-PIN", FieldType.NUMERIC, 12);
        pnd_Bm_S_P_C_I_Key_Pnd_Bm_Tiaa_Cntrct = pnd_Bm_S_P_C_I_Key__R_Field_3.newFieldInGroup("pnd_Bm_S_P_C_I_Key_Pnd_Bm_Tiaa_Cntrct", "#BM-TIAA-CNTRCT", 
            FieldType.STRING, 10);
        pnd_Bm_S_P_C_I_Key_Pnd_Bm_Tiaa_Cref_Ind = pnd_Bm_S_P_C_I_Key__R_Field_3.newFieldInGroup("pnd_Bm_S_P_C_I_Key_Pnd_Bm_Tiaa_Cref_Ind", "#BM-TIAA-CREF-IND", 
            FieldType.STRING, 1);

        vw_bt = new DataAccessProgramView(new NameInfo("vw_bt", "BT"), "BENE_TABLE_FILE", "BENE_TABLE");
        bt_Bt_Table_Id = vw_bt.getRecord().newFieldInGroup("bt_Bt_Table_Id", "BT-TABLE-ID", FieldType.STRING, 2, RepeatingFieldStrategy.None, "BT_TABLE_ID");
        bt_Bt_Table_Id.setDdmHeader("TABLE/ID");
        bt_Bt_Table_Key = vw_bt.getRecord().newFieldInGroup("bt_Bt_Table_Key", "BT-TABLE-KEY", FieldType.STRING, 20, RepeatingFieldStrategy.None, "BT_TABLE_KEY");
        bt_Bt_Table_Key.setDdmHeader("TABLE/KEY");
        bt_Bt_Table_Text = vw_bt.getRecord().newFieldInGroup("bt_Bt_Table_Text", "BT-TABLE-TEXT", FieldType.STRING, 72, RepeatingFieldStrategy.None, "BT_TABLE_TEXT");
        bt_Bt_Table_Text.setDdmHeader("TABLE/TEXT");
        registerRecord(vw_bt);

        pnd_Bt_Table_Id_Key = localVariables.newFieldInRecord("pnd_Bt_Table_Id_Key", "#BT-TABLE-ID-KEY", FieldType.STRING, 22);
        pnd_Error_Line = localVariables.newFieldInRecord("pnd_Error_Line", "#ERROR-LINE", FieldType.STRING, 132);

        pnd_Error_Line__R_Field_4 = localVariables.newGroupInRecord("pnd_Error_Line__R_Field_4", "REDEFINE", pnd_Error_Line);
        pnd_Error_Line_Pnd_Error_Name = pnd_Error_Line__R_Field_4.newFieldInGroup("pnd_Error_Line_Pnd_Error_Name", "#ERROR-NAME", FieldType.STRING, 34);
        pnd_Error_Line_Pnd_Error_Pin = pnd_Error_Line__R_Field_4.newFieldInGroup("pnd_Error_Line_Pnd_Error_Pin", "#ERROR-PIN", FieldType.STRING, 13);
        pnd_Error_Line_Pnd_Error_Dte = pnd_Error_Line__R_Field_4.newFieldInGroup("pnd_Error_Line_Pnd_Error_Dte", "#ERROR-DTE", FieldType.STRING, 11);
        pnd_Error_Line_Pnd_Err_Cntrct = pnd_Error_Line__R_Field_4.newFieldInGroup("pnd_Error_Line_Pnd_Err_Cntrct", "#ERR-CNTRCT", FieldType.STRING, 11);
        pnd_Error_Line_Pnd_Error_Wpid = pnd_Error_Line__R_Field_4.newFieldInGroup("pnd_Error_Line_Pnd_Error_Wpid", "#ERROR-WPID", FieldType.STRING, 7);
        pnd_Error_Line_Pnd_Error_Wpid_Unit = pnd_Error_Line__R_Field_4.newFieldInGroup("pnd_Error_Line_Pnd_Error_Wpid_Unit", "#ERROR-WPID-UNIT", FieldType.STRING, 
            9);
        pnd_Error_Line_Pnd_Error_Msg = pnd_Error_Line__R_Field_4.newFieldInGroup("pnd_Error_Line_Pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 45);

        pnd_Error_Line__R_Field_5 = pnd_Error_Line__R_Field_4.newGroupInGroup("pnd_Error_Line__R_Field_5", "REDEFINE", pnd_Error_Line_Pnd_Error_Msg);
        pnd_Error_Line_Pnd_Error_Msg_Arr = pnd_Error_Line__R_Field_5.newFieldArrayInGroup("pnd_Error_Line_Pnd_Error_Msg_Arr", "#ERROR-MSG-ARR", FieldType.STRING, 
            1, new DbsArrayController(1, 45));
        pnd_Old_Dte = localVariables.newFieldInRecord("pnd_Old_Dte", "#OLD-DTE", FieldType.DATE);
        pnd_Error_Hold = localVariables.newFieldInRecord("pnd_Error_Hold", "#ERROR-HOLD", FieldType.STRING, 79);

        pnd_Error_Hold__R_Field_6 = localVariables.newGroupInRecord("pnd_Error_Hold__R_Field_6", "REDEFINE", pnd_Error_Hold);
        pnd_Error_Hold_Pnd_Error_Hold_Arr = pnd_Error_Hold__R_Field_6.newFieldArrayInGroup("pnd_Error_Hold_Pnd_Error_Hold_Arr", "#ERROR-HOLD-ARR", FieldType.STRING, 
            1, new DbsArrayController(1, 79));
        pnd_Ind = localVariables.newFieldInRecord("pnd_Ind", "#IND", FieldType.NUMERIC, 3);
        pnd_P = localVariables.newFieldInRecord("pnd_P", "#P", FieldType.NUMERIC, 2);
        pnd_N = localVariables.newFieldInRecord("pnd_N", "#N", FieldType.NUMERIC, 2);
        pnd_M = localVariables.newFieldInRecord("pnd_M", "#M", FieldType.NUMERIC, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 2);
        pnd_L = localVariables.newFieldInRecord("pnd_L", "#L", FieldType.NUMERIC, 2);
        pnd_Frm_Ind = localVariables.newFieldInRecord("pnd_Frm_Ind", "#FRM-IND", FieldType.NUMERIC, 2);
        pnd_End_Ind = localVariables.newFieldInRecord("pnd_End_Ind", "#END-IND", FieldType.NUMERIC, 2);
        pnd_Page_Number = localVariables.newFieldInRecord("pnd_Page_Number", "#PAGE-NUMBER", FieldType.NUMERIC, 5);
        pnd_Line_Err = localVariables.newFieldInRecord("pnd_Line_Err", "#LINE-ERR", FieldType.NUMERIC, 3);
        pnd_A_Cnt = localVariables.newFieldInRecord("pnd_A_Cnt", "#A-CNT", FieldType.NUMERIC, 6);
        pnd_Et_Cnt = localVariables.newFieldInRecord("pnd_Et_Cnt", "#ET-CNT", FieldType.NUMERIC, 6);
        pnd_E_Cnt = localVariables.newFieldInRecord("pnd_E_Cnt", "#E-CNT", FieldType.NUMERIC, 6);
        pnd_Cref_Only = localVariables.newFieldInRecord("pnd_Cref_Only", "#CREF-ONLY", FieldType.BOOLEAN, 1);
        pnd_Error = localVariables.newFieldInRecord("pnd_Error", "#ERROR", FieldType.BOOLEAN, 1);
        pnd_Cont_Found = localVariables.newFieldInRecord("pnd_Cont_Found", "#CONT-FOUND", FieldType.BOOLEAN, 1);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Slctd_Tiaa = localVariables.newFieldArrayInRecord("pnd_Slctd_Tiaa", "#SLCTD-TIAA", FieldType.STRING, 10, new DbsArrayController(1, 20));
        pnd_Slctd_Cref = localVariables.newFieldArrayInRecord("pnd_Slctd_Cref", "#SLCTD-CREF", FieldType.STRING, 10, new DbsArrayController(1, 20));
        pnd_Cont_Stat = localVariables.newFieldInRecord("pnd_Cont_Stat", "#CONT-STAT", FieldType.STRING, 1);
        pnd_Alpha = localVariables.newFieldInRecord("pnd_Alpha", "#ALPHA", FieldType.STRING, 7);

        pnd_Alpha__R_Field_7 = localVariables.newGroupInRecord("pnd_Alpha__R_Field_7", "REDEFINE", pnd_Alpha);
        pnd_Alpha_Pnd_Alpha_N = pnd_Alpha__R_Field_7.newFieldInGroup("pnd_Alpha_Pnd_Alpha_N", "#ALPHA-N", FieldType.NUMERIC, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        s1Ba_Rqst_Cmpltd_DteOld = internalLoopRecord.newFieldInRecord("S1_Ba_Rqst_Cmpltd_Dte_OLD", "Ba_Rqst_Cmpltd_Dte_OLD", FieldType.STRING, 8);
        s1Ba_Seq_NmbrOld = internalLoopRecord.newFieldInRecord("S1_Ba_Seq_Nmbr_OLD", "Ba_Seq_Nmbr_OLD", FieldType.NUMERIC, 2);
        s1Count_Castba_Slctd_Cntrct_GroupOld = internalLoopRecord.newFieldInRecord("S1_Count_Castba_Slctd_Cntrct_Group_OLD", "Count_Castba_Slctd_Cntrct_Group_OLD", 
            FieldType.NUMERIC, 3);
        s1Act_Ba_Slctd_TiaaOld = internalLoopRecord.newFieldArrayInRecord("S1_Act_Ba_Slctd_Tiaa_OLD", "Act_Ba_Slctd_Tiaa_OLD", FieldType.STRING, 10, new 
            DbsArrayController(1, 20));
        s1Act_Ba_Slctd_CrefOld = internalLoopRecord.newFieldArrayInRecord("S1_Act_Ba_Slctd_Cref_OLD", "Act_Ba_Slctd_Cref_OLD", FieldType.STRING, 10, new 
            DbsArrayController(1, 20));
        s1Act_Ba_Wpid_UnitOld = internalLoopRecord.newFieldInRecord("S1_Act_Ba_Wpid_Unit_OLD", "Act_Ba_Wpid_Unit_OLD", FieldType.STRING, 8);
        s1Act_Ba_WpidOld = internalLoopRecord.newFieldInRecord("S1_Act_Ba_Wpid_OLD", "Act_Ba_Wpid_OLD", FieldType.STRING, 6);
        s1Act_Ba_NameOld = internalLoopRecord.newFieldInRecord("S1_Act_Ba_Name_OLD", "Act_Ba_Name_OLD", FieldType.STRING, 35);
        s1Act_Ba_Owner_NameOld = internalLoopRecord.newFieldInRecord("S1_Act_Ba_Owner_Name_OLD", "Act_Ba_Owner_Name_OLD", FieldType.STRING, 35);
        s1Ba_Rqst_TimestampOld = internalLoopRecord.newFieldInRecord("S1_Ba_Rqst_Timestamp_OLD", "Ba_Rqst_Timestamp_OLD", FieldType.STRING, 15);
        s1Act_Ba_Lttr_InfoOld = internalLoopRecord.newFieldInRecord("S1_Act_Ba_Lttr_Info_OLD", "Act_Ba_Lttr_Info_OLD", FieldType.STRING, 10);
        s1Act_Ba_Mail_AddrOld = internalLoopRecord.newFieldArrayInRecord("S1_Act_Ba_Mail_Addr_OLD", "Act_Ba_Mail_Addr_OLD", FieldType.STRING, 35, new 
            DbsArrayController(1, 5));
        s1Act_Ba_Us_FrgnOld = internalLoopRecord.newFieldInRecord("S1_Act_Ba_Us_Frgn_OLD", "Act_Ba_Us_Frgn_OLD", FieldType.STRING, 1);
        s1Act_Ba_Addrss_Postal_DataOld = internalLoopRecord.newFieldInRecord("S1_Act_Ba_Addrss_Postal_Data_OLD", "Act_Ba_Addrss_Postal_Data_OLD", FieldType.STRING, 
            32);
        s1Act_Ba_Rcrd_Last_Updt_UseridOld = internalLoopRecord.newFieldInRecord("S1_Act_Ba_Rcrd_Last_Updt_Userid_OLD", "Act_Ba_Rcrd_Last_Updt_Userid_OLD", 
            FieldType.STRING, 8);
        s1Act_Ba_Diff_DsgntnOld = internalLoopRecord.newFieldInRecord("S1_Act_Ba_Diff_Dsgntn_OLD", "Act_Ba_Diff_Dsgntn_OLD", FieldType.STRING, 1);
        s1Ba_PinOld = internalLoopRecord.newFieldInRecord("S1_Ba_Pin_OLD", "Ba_Pin_OLD", FieldType.NUMERIC, 12);
        s1Act_Ba_Lttr_OptnOld = internalLoopRecord.newFieldInRecord("S1_Act_Ba_Lttr_Optn_OLD", "Act_Ba_Lttr_Optn_OLD", FieldType.STRING, 1);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cnct.reset();
        vw_pnd_Ben.reset();
        vw_pnd_Mos.reset();
        vw_bt.reset();
        internalLoopRecord.reset();

        localVariables.reset();
        pnd_Bc_S_P_C_I_Key.setInitialValue("C");
        pnd_Bd_S_P_C_I_T_S.setInitialValue("C");
        pnd_Bm_S_P_C_I_Key.setInitialValue("C");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Benp220() throws Exception
    {
        super("Benp220");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 60 LS = 133;//Natural: FORMAT ( 1 ) PS = 60 LS = 133
        //*  REMOVED BENE-CONTROL
                                                                                                                                                                          //Natural: PERFORM START-UP
        sub_Start_Up();
        if (condition(Global.isEscape())) {return;}
        pnd_Page_Number.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #PAGE-NUMBER
        pdaBena011.getBena011_Pnd_Max_Cntrct().setValue(gdaBeng200.getPnd_Pnd_Max_Cntrct());                                                                              //Natural: ASSIGN BENA011.#MAX-CNTRCT := ##MAX-CNTRCT
        //*  WITH BA-RQST-CMPLTD-DTE = #DATE-Y THRU #DATE-X
        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Benm220.class));                                                                               //Natural: WRITE ( 1 ) NOTITLE USING FORM 'BENM220 '
        //* ***********************************************************************
        //*  MAIN PROCESS
        //* ***********************************************************************
        //*  READ PHYS. TO GET ANYTHING WITHOUT A STATEMENT FROM WHENEVER.
        gdaBeng200.getVw_act().startDatabaseRead                                                                                                                          //Natural: READ ACT
        (
        "R1",
        new Oc[] { new Oc("ISN", "ASC") }
        );
        R1:
        while (condition(gdaBeng200.getVw_act().readNextRow("R1")))
        {
            //*  IE. UNCONVERTED PRE-UNIT
            //*  CHANGE RECORDS
            if (condition(gdaBeng200.getAct_Ba_Wpid_Unit().equals(" ")))                                                                                                  //Natural: IF ACT.BA-WPID-UNIT = ' '
            {
                gdaBeng200.getAct_Ba_Wpid_Unit().setValue("CTADME");                                                                                                      //Natural: ASSIGN ACT.BA-WPID-UNIT := 'CTADME'
            }                                                                                                                                                             //Natural: END-IF
            //*  VERIFIER UNDO
            if (condition(gdaBeng200.getAct_Ba_Vrfy_Ind().equals("4")))                                                                                                   //Natural: REJECT IF BA-VRFY-IND = '4'
            {
                continue;
            }
            //*  REJECT FUTURE DATES CF
            //*  REJECT FUTURE DATES CF
            //*  NO LETTER OPTION
            if (condition(!(gdaBeng200.getAct_Ba_Cnfrm_Stmnt_Prod_Dte().lessOrEqual(" ") && gdaBeng200.getAct_Ba_Rqst_Cmpltd_Dte().notEquals(" ") && gdaBeng200.getAct_Ba_Rqst_Cmpltd_Dte().lessOrEqual(gdaBeng200.getPnd_Date_X())  //Natural: ACCEPT IF BA-CNFRM-STMNT-PROD-DTE LE ' ' AND BA-RQST-CMPLTD-DTE NE ' ' AND BA-RQST-CMPLTD-DTE LE #DATE-X AND NOT BA-LTTR-OPTN = '1'
                && ! (gdaBeng200.getAct_Ba_Lttr_Optn().equals("1")))))
            {
                continue;
            }
            //* ** AND     BA-RQST-CMPLTD-DTE GT ' '
            pnd_A_Cnt.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #A-CNT
            //*  SEQ-NMBR SHOULD NEVER BE > 3
            getSort().writeSortInData(gdaBeng200.getAct_Ba_Pin(), gdaBeng200.getAct_Ba_Rqst_Timestamp(), gdaBeng200.getAct_Ba_Seq_Nmbr(), gdaBeng200.getAct_Ba_Wpid_Unit(),  //Natural: END-ALL
                gdaBeng200.getAct_Ba_Wpid(), gdaBeng200.getAct_Ba_Rqst_Cmpltd_Dte(), gdaBeng200.getAct_Count_Castba_Slctd_Cntrct_Group(), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(1), 
                gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(2), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(3), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(4), 
                gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(5), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(6), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(7), 
                gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(8), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(9), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(10), 
                gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(11), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(12), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(13), 
                gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(14), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(15), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(16), 
                gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(17), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(18), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(19), 
                gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(20), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(1), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(2), 
                gdaBeng200.getAct_Ba_Slctd_Cref().getValue(3), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(4), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(5), 
                gdaBeng200.getAct_Ba_Slctd_Cref().getValue(6), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(7), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(8), 
                gdaBeng200.getAct_Ba_Slctd_Cref().getValue(9), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(10), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(11), 
                gdaBeng200.getAct_Ba_Slctd_Cref().getValue(12), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(13), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(14), 
                gdaBeng200.getAct_Ba_Slctd_Cref().getValue(15), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(16), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(17), 
                gdaBeng200.getAct_Ba_Slctd_Cref().getValue(18), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(19), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(20), 
                gdaBeng200.getAct_Ba_Diff_Dsgntn(), gdaBeng200.getAct_Ba_Name(), gdaBeng200.getAct_Ba_Owner_Name(), gdaBeng200.getAct_Ba_Lttr_Info(), gdaBeng200.getAct_Ba_Lttr_Optn(), 
                gdaBeng200.getAct_Ba_Mail_Addr().getValue(1), gdaBeng200.getAct_Ba_Mail_Addr().getValue(2), gdaBeng200.getAct_Ba_Mail_Addr().getValue(3), 
                gdaBeng200.getAct_Ba_Mail_Addr().getValue(4), gdaBeng200.getAct_Ba_Mail_Addr().getValue(5), gdaBeng200.getAct_Ba_Addrss_Postal_Data(), gdaBeng200.getAct_Ba_Us_Frgn(), 
                gdaBeng200.getAct_Ba_Eff_Dte(), gdaBeng200.getAct_Ba_Rcrd_Last_Updt_Userid());
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getSort().sortData(gdaBeng200.getAct_Ba_Pin(), gdaBeng200.getAct_Ba_Rqst_Timestamp(), gdaBeng200.getAct_Ba_Seq_Nmbr());                                           //Natural: SORT BY ACT.BA-PIN ACT.BA-RQST-TIMESTAMP ACT.BA-SEQ-NMBR USING ACT.BA-WPID-UNIT ACT.BA-WPID ACT.BA-RQST-CMPLTD-DTE ACT.C*BA-SLCTD-CNTRCT-GROUP ACT.BA-SLCTD-TIAA ( 1:20 ) ACT.BA-SLCTD-CREF ( 1:20 ) ACT.BA-DIFF-DSGNTN ACT.BA-NAME ACT.BA-OWNER-NAME ACT.BA-LTTR-INFO ACT.BA-LTTR-OPTN ACT.BA-MAIL-ADDR ( 1:5 ) ACT.BA-ADDRSS-POSTAL-DATA ACT.BA-US-FRGN ACT.BA-EFF-DTE ACT.BA-RCRD-LAST-UPDT-USERID
        boolean endOfDataS1 = true;
        boolean firstS1 = true;
        S1:
        while (condition(getSort().readSortOutData(gdaBeng200.getAct_Ba_Pin(), gdaBeng200.getAct_Ba_Rqst_Timestamp(), gdaBeng200.getAct_Ba_Seq_Nmbr(), 
            gdaBeng200.getAct_Ba_Wpid_Unit(), gdaBeng200.getAct_Ba_Wpid(), gdaBeng200.getAct_Ba_Rqst_Cmpltd_Dte(), gdaBeng200.getAct_Count_Castba_Slctd_Cntrct_Group(), 
            gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(1), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(2), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(3), 
            gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(4), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(5), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(6), 
            gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(7), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(8), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(9), 
            gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(10), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(11), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(12), 
            gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(13), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(14), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(15), 
            gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(16), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(17), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(18), 
            gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(19), gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(20), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(1), 
            gdaBeng200.getAct_Ba_Slctd_Cref().getValue(2), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(3), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(4), 
            gdaBeng200.getAct_Ba_Slctd_Cref().getValue(5), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(6), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(7), 
            gdaBeng200.getAct_Ba_Slctd_Cref().getValue(8), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(9), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(10), 
            gdaBeng200.getAct_Ba_Slctd_Cref().getValue(11), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(12), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(13), 
            gdaBeng200.getAct_Ba_Slctd_Cref().getValue(14), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(15), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(16), 
            gdaBeng200.getAct_Ba_Slctd_Cref().getValue(17), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(18), gdaBeng200.getAct_Ba_Slctd_Cref().getValue(19), 
            gdaBeng200.getAct_Ba_Slctd_Cref().getValue(20), gdaBeng200.getAct_Ba_Diff_Dsgntn(), gdaBeng200.getAct_Ba_Name(), gdaBeng200.getAct_Ba_Owner_Name(), 
            gdaBeng200.getAct_Ba_Lttr_Info(), gdaBeng200.getAct_Ba_Lttr_Optn(), gdaBeng200.getAct_Ba_Mail_Addr().getValue(1), gdaBeng200.getAct_Ba_Mail_Addr().getValue(2), 
            gdaBeng200.getAct_Ba_Mail_Addr().getValue(3), gdaBeng200.getAct_Ba_Mail_Addr().getValue(4), gdaBeng200.getAct_Ba_Mail_Addr().getValue(5), gdaBeng200.getAct_Ba_Addrss_Postal_Data(), 
            gdaBeng200.getAct_Ba_Us_Frgn(), gdaBeng200.getAct_Ba_Eff_Dte(), gdaBeng200.getAct_Ba_Rcrd_Last_Updt_Userid())))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventS1(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataS1 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT BREAK OF ACT.BA-SEQ-NMBR
            //*                                                                                                                                                           //Natural: AT BREAK OF ACT.BA-RQST-TIMESTAMP
            //*  (S1.)
            s1Ba_Rqst_Cmpltd_DteOld.setValue(gdaBeng200.getAct_Ba_Rqst_Cmpltd_Dte());                                                                                     //Natural: END-SORT
            s1Ba_Seq_NmbrOld.setValue(gdaBeng200.getAct_Ba_Seq_Nmbr());
            s1Count_Castba_Slctd_Cntrct_GroupOld.setValue(gdaBeng200.getAct_Count_Castba_Slctd_Cntrct_Group());
            s1Act_Ba_Slctd_TiaaOld.getValue(1,":",20).setValue(gdaBeng200.getAct_Ba_Slctd_Tiaa().getValue(1,":",20));
            s1Act_Ba_Slctd_CrefOld.getValue(1,":",20).setValue(gdaBeng200.getAct_Ba_Slctd_Cref().getValue(1,":",20));
            s1Act_Ba_Wpid_UnitOld.setValue(gdaBeng200.getAct_Ba_Wpid_Unit());
            s1Act_Ba_WpidOld.setValue(gdaBeng200.getAct_Ba_Wpid());
            s1Act_Ba_NameOld.setValue(gdaBeng200.getAct_Ba_Name());
            s1Act_Ba_Owner_NameOld.setValue(gdaBeng200.getAct_Ba_Owner_Name());
            s1Ba_Rqst_TimestampOld.setValue(gdaBeng200.getAct_Ba_Rqst_Timestamp());
            s1Act_Ba_Lttr_InfoOld.setValue(gdaBeng200.getAct_Ba_Lttr_Info());
            s1Act_Ba_Mail_AddrOld.getValue(1,":",5).setValue(gdaBeng200.getAct_Ba_Mail_Addr().getValue(1,":",5));
            s1Act_Ba_Us_FrgnOld.setValue(gdaBeng200.getAct_Ba_Us_Frgn());
            s1Act_Ba_Addrss_Postal_DataOld.setValue(gdaBeng200.getAct_Ba_Addrss_Postal_Data());
            s1Act_Ba_Rcrd_Last_Updt_UseridOld.setValue(gdaBeng200.getAct_Ba_Rcrd_Last_Updt_Userid());
            s1Act_Ba_Diff_DsgntnOld.setValue(gdaBeng200.getAct_Ba_Diff_Dsgntn());
            s1Ba_PinOld.setValue(gdaBeng200.getAct_Ba_Pin());
            s1Act_Ba_Lttr_OptnOld.setValue(gdaBeng200.getAct_Ba_Lttr_Optn());
        }
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventS1(endOfDataS1);
        }
        endSort();
        if (condition(getReports().getAstLineCount(1).greater(58)))                                                                                                       //Natural: IF *LINE-COUNT ( 1 ) GT 58
        {
            if (condition(getReports().getAstLineCount(1).less(60)))                                                                                                      //Natural: IF *LINE-COUNT ( 1 ) LT 60
            {
                FOR01:                                                                                                                                                    //Natural: FOR #M *LINE-COUNT ( 1 ) 60
                for (pnd_M.setValue(getReports().getAstLineCount(1)); condition(pnd_M.lessOrEqual(60)); pnd_M.nadd(1))
                {
                    getReports().write(1, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 1 ) ' '
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            pnd_Page_Number.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #PAGE-NUMBER
            getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Benm220.class));                                                                           //Natural: WRITE ( 1 ) NOTITLE USING FORM 'BENM220 '
        }                                                                                                                                                                 //Natural: END-IF
        getReports().skip(1, 3);                                                                                                                                          //Natural: SKIP ( 1 ) 3
        getReports().write(1, ReportOption.NOTITLE,"                                        ***  END REPORT  ***");                                                       //Natural: WRITE ( 1 ) '                                        ***  END REPORT  ***'
        if (Global.isEscape()) return;
        getReports().write(0, "Number of Statements succesfully sent to POST     :",pnd_Et_Cnt);                                                                          //Natural: WRITE ( 0 ) 'Number of Statements succesfully sent to POST     :' #ET-CNT
        if (Global.isEscape()) return;
        getReports().write(0, "Number of Activity records accepted for processing:",pnd_A_Cnt);                                                                           //Natural: WRITE ( 0 ) 'Number of Activity records accepted for processing:' #A-CNT
        if (Global.isEscape()) return;
        getReports().write(0, "Number of errors written to error report          :",pnd_E_Cnt);                                                                           //Natural: WRITE ( 0 ) 'Number of errors written to error report          :' #E-CNT
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POP-REST-OF-A011
        //*      #CONT-STAT := 'P'
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-MOS/BENE-AND-POP-002
        //*  BENE-MOS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERR-WRITE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COUNT-AND-CHECK/FORMAT-ERROR
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: START-UP
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-OR-WRITE-TITLE
    }
    private void sub_Pop_Rest_Of_A011() throws Exception                                                                                                                  //Natural: POP-REST-OF-A011
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Cont_Found.reset();                                                                                                                                           //Natural: RESET #CONT-FOUND
        pdaBena011.getBena011_Pnd_Lttr_Option().setValue("2");                                                                                                            //Natural: ASSIGN BENA011.#LTTR-OPTION := '2'
        //*  BENA011 BENE-CONTRACT
        if (condition(pnd_Cref_Only.getBoolean()))                                                                                                                        //Natural: IF #CREF-ONLY
        {
            vw_cnct.startDatabaseRead                                                                                                                                     //Natural: READ CNCT BY BC-CREF-CNTRCT = BENA011.#CREF-CNTRCT ( 1 )
            (
            "RCC",
            new Wc[] { new Wc("BC_CREF_CNTRCT", ">=", pdaBena011.getBena011_Pnd_Cref_Cntrct().getValue(1), WcType.BY) },
            new Oc[] { new Oc("BC_CREF_CNTRCT", "ASC") }
            );
            RCC:
            while (condition(vw_cnct.readNextRow("RCC")))
            {
                if (condition(cnct_Bc_Cref_Cntrct.greater(pdaBena011.getBena011_Pnd_Cref_Cntrct().getValue(1))))                                                          //Natural: IF BC-CREF-CNTRCT GT BENA011.#CREF-CNTRCT ( 1 )
                {
                    if (true) break RCC;                                                                                                                                  //Natural: ESCAPE BOTTOM ( RCC. )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(cnct_Bc_Pin.notEquals(pnd_Bc_S_P_C_I_Key_Pnd_Bc_Pin) || ! (cnct_Bc_Stat.equals("C") || cnct_Bc_Stat.equals("P") || cnct_Bc_Stat.equals("X"))  //Natural: IF BC-PIN NE #BC-PIN OR NOT ( BC-STAT = 'C' OR = 'P' OR = 'X' ) OR BC-TIAA-CREF-IND NE 'C'
                    || cnct_Bc_Tiaa_Cref_Ind.notEquals("C")))
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pnd_Cont_Found.setValue(true);                                                                                                                            //Natural: ASSIGN #CONT-FOUND := TRUE
                if (condition(cnct_Bc_Stat.equals("P") || cnct_Bc_Stat.equals("X")))                                                                                      //Natural: IF BC-STAT = 'P' OR = 'X'
                {
                    pnd_Cont_Stat.setValue(cnct_Bc_Stat);                                                                                                                 //Natural: ASSIGN #CONT-STAT := BC-STAT
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Cont_Stat.setValue("C");                                                                                                                          //Natural: ASSIGN #CONT-STAT := 'C'
                    if (true) break RCC;                                                                                                                                  //Natural: ESCAPE BOTTOM ( RCC. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Bc_S_P_C_I_Key_Pnd_Bc_Stat.setValue("C");                                                                                                                 //Natural: ASSIGN #BC-STAT := 'C'
            vw_cnct.startDatabaseRead                                                                                                                                     //Natural: READ CNCT BY BC-STAT-PIN-CNTRCT-IND-KEY = #BC-S-P-C-I-KEY
            (
            "RC1",
            new Wc[] { new Wc("BC_STAT_PIN_CNTRCT_IND_KEY", ">=", pnd_Bc_S_P_C_I_Key, WcType.BY) },
            new Oc[] { new Oc("BC_STAT_PIN_CNTRCT_IND_KEY", "ASC") }
            );
            RC1:
            while (condition(vw_cnct.readNextRow("RC1")))
            {
                if (condition(cnct_Bc_Pin.greater(pnd_Bc_S_P_C_I_Key_Pnd_Bc_Pin) || cnct_Bc_Tiaa_Cntrct.greater(pnd_Bc_S_P_C_I_Key_Pnd_Bc_Tiaa_Cntrct)                    //Natural: IF BC-PIN GT #BC-PIN OR BC-TIAA-CNTRCT GT #BC-TIAA-CNTRCT OR BC-STAT NE 'C'
                    || cnct_Bc_Stat.notEquals("C")))
                {
                    //*        OR BC-TIAA-CREF-IND NE #BC-TIAA-CREF-IND
                    if (true) break RC1;                                                                                                                                  //Natural: ESCAPE BOTTOM ( RC1. )
                }                                                                                                                                                         //Natural: END-IF
                //*  ' ' OR 'T' OK, 'C' NO GOOD
                if (condition(cnct_Bc_Tiaa_Cref_Ind.equals("C")))                                                                                                         //Natural: IF BC-TIAA-CREF-IND = 'C'
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pnd_Cont_Found.setValue(true);                                                                                                                            //Natural: ASSIGN #CONT-FOUND := TRUE
                pnd_Cont_Stat.setValue("C");                                                                                                                              //Natural: ASSIGN #CONT-STAT := 'C'
                if (true) break RC1;                                                                                                                                      //Natural: ESCAPE BOTTOM ( RC1. )
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
            if (condition(! (pnd_Cont_Found.getBoolean())))                                                                                                               //Natural: IF NOT #CONT-FOUND
            {
                pnd_Bc_S_P_C_I_Key_Pnd_Bc_Stat.setValue("P");                                                                                                             //Natural: ASSIGN #BC-STAT := 'P'
                vw_cnct.startDatabaseRead                                                                                                                                 //Natural: READ CNCT BY BC-STAT-PIN-CNTRCT-IND-KEY = #BC-S-P-C-I-KEY
                (
                "RC2",
                new Wc[] { new Wc("BC_STAT_PIN_CNTRCT_IND_KEY", ">=", pnd_Bc_S_P_C_I_Key, WcType.BY) },
                new Oc[] { new Oc("BC_STAT_PIN_CNTRCT_IND_KEY", "ASC") }
                );
                RC2:
                while (condition(vw_cnct.readNextRow("RC2")))
                {
                    if (condition(cnct_Bc_Pin.greater(pnd_Bc_S_P_C_I_Key_Pnd_Bc_Pin) || cnct_Bc_Tiaa_Cntrct.greater(pnd_Bc_S_P_C_I_Key_Pnd_Bc_Tiaa_Cntrct)                //Natural: IF BC-PIN GT #BC-PIN OR BC-TIAA-CNTRCT GT #BC-TIAA-CNTRCT OR BC-STAT NE 'P'
                        || cnct_Bc_Stat.notEquals("P")))
                    {
                        //*          OR BC-TIAA-CREF-IND NE #BC-TIAA-CREF-IND
                        if (true) break RC2;                                                                                                                              //Natural: ESCAPE BOTTOM ( RC2. )
                    }                                                                                                                                                     //Natural: END-IF
                    //*  ' ' OR 'T' OK, 'C' NO GOOD
                    if (condition(cnct_Bc_Tiaa_Cref_Ind.equals("C")))                                                                                                     //Natural: IF BC-TIAA-CREF-IND = 'C'
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Cont_Found.setValue(true);                                                                                                                        //Natural: ASSIGN #CONT-FOUND := TRUE
                    pnd_Cont_Stat.setValue("P");                                                                                                                          //Natural: ASSIGN #CONT-STAT := 'P'
                    if (true) break RC2;                                                                                                                                  //Natural: ESCAPE BOTTOM ( RC2. )
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(! (pnd_Cont_Found.getBoolean())))                                                                                                               //Natural: IF NOT #CONT-FOUND
            {
                pnd_Bc_S_P_C_I_Key_Pnd_Bc_Stat.setValue("X");                                                                                                             //Natural: ASSIGN #BC-STAT := 'X'
                vw_cnct.startDatabaseRead                                                                                                                                 //Natural: READ CNCT BY BC-STAT-PIN-CNTRCT-IND-KEY = #BC-S-P-C-I-KEY
                (
                "RC3",
                new Wc[] { new Wc("BC_STAT_PIN_CNTRCT_IND_KEY", ">=", pnd_Bc_S_P_C_I_Key, WcType.BY) },
                new Oc[] { new Oc("BC_STAT_PIN_CNTRCT_IND_KEY", "ASC") }
                );
                RC3:
                while (condition(vw_cnct.readNextRow("RC3")))
                {
                    if (condition(cnct_Bc_Pin.greater(pnd_Bc_S_P_C_I_Key_Pnd_Bc_Pin) || cnct_Bc_Tiaa_Cntrct.greater(pnd_Bc_S_P_C_I_Key_Pnd_Bc_Tiaa_Cntrct)                //Natural: IF BC-PIN GT #BC-PIN OR BC-TIAA-CNTRCT GT #BC-TIAA-CNTRCT OR BC-STAT NE 'X'
                        || cnct_Bc_Stat.notEquals("X")))
                    {
                        //*          OR BC-TIAA-CREF-IND NE #BC-TIAA-CREF-IND
                        if (true) break RC3;                                                                                                                              //Natural: ESCAPE BOTTOM ( RC3. )
                    }                                                                                                                                                     //Natural: END-IF
                    //*  ' ' OR 'T' OK, 'C' NO GOOD
                    if (condition(cnct_Bc_Tiaa_Cref_Ind.equals("C")))                                                                                                     //Natural: IF BC-TIAA-CREF-IND = 'C'
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Cont_Found.setValue(true);                                                                                                                        //Natural: ASSIGN #CONT-FOUND := TRUE
                    pnd_Cont_Stat.setValue("X");                                                                                                                          //Natural: ASSIGN #CONT-STAT := 'X'
                    if (true) break RC3;                                                                                                                                  //Natural: ESCAPE BOTTOM ( RC3. )
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* JW7/6/01
        if (condition(((pnd_Cont_Found.getBoolean() && cnct_Bc_Eff_Dte.greater(" ")) && (cnct_Bc_Mos_Ind.equals("Y") || cnct_Bc_Mos_Ind.equals("N")))))                   //Natural: IF #CONT-FOUND AND CNCT.BC-EFF-DTE GT ' ' AND ( CNCT.BC-MOS-IND = 'Y' OR = 'N' )
        {
            pdaBena011.getBena011_Pnd_Eff_Dte().setValue(cnct_Bc_Eff_Dte);                                                                                                //Natural: ASSIGN BENA011.#EFF-DTE := CNCT.BC-EFF-DTE
            pdaBena011.getBena011_Pnd_Pymnt_Chld_Dcsd_Ind().setValue(cnct_Bc_Pymnt_Chld_Dcsd_Ind);                                                                        //Natural: ASSIGN BENA011.#PYMNT-CHLD-DCSD-IND := CNCT.BC-PYMNT-CHLD-DCSD-IND
            pdaBena011.getBena011_Pnd_Spouse_Waived_Bnfts().setValue(cnct_Bc_Spouse_Waived_Bnfts);                                                                        //Natural: ASSIGN BENA011.#SPOUSE-WAIVED-BNFTS := CNCT.BC-SPOUSE-WAIVED-BNFTS
            if (condition(cnct_Bc_Mos_Ind.equals("Y")))                                                                                                                   //Natural: IF CNCT.BC-MOS-IND = 'Y'
            {
                pdaBena011.getBena011_Pnd_Mos_Ind().setValue(cnct_Bc_Mos_Ind);                                                                                            //Natural: ASSIGN BENA011.#MOS-IND := CNCT.BC-MOS-IND
                pdaBena011.getBena011_Pnd_Mos_Irvcbl().setValue(cnct_Bc_Irvcbl_Ind);                                                                                      //Natural: ASSIGN BENA011.#MOS-IRVCBL := CNCT.BC-IRVCBL-IND
                pnd_Bm_S_P_C_I_Key_Pnd_Bm_Tiaa_Cntrct.setValue(cnct_Bc_Tiaa_Cntrct);                                                                                      //Natural: ASSIGN #BM-TIAA-CNTRCT := CNCT.BC-TIAA-CNTRCT
                pnd_Bm_S_P_C_I_Key_Pnd_Bm_Tiaa_Cref_Ind.setValue(cnct_Bc_Tiaa_Cref_Ind);                                                                                  //Natural: ASSIGN #BM-TIAA-CREF-IND := CNCT.BC-TIAA-CREF-IND
                pnd_Bm_S_P_C_I_Key_Pnd_Bm_Stat.setValue(pnd_Cont_Stat);                                                                                                   //Natural: ASSIGN #BM-STAT := #CONT-STAT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaBena011.getBena011_Pnd_Mos_Ind().setValue(cnct_Bc_Mos_Ind);                                                                                            //Natural: ASSIGN BENA011.#MOS-IND := CNCT.BC-MOS-IND
                pnd_Bd_S_P_C_I_T_S_Pnd_Bd_Tiaa_Cntrct.setValue(cnct_Bc_Tiaa_Cntrct);                                                                                      //Natural: ASSIGN #BD-TIAA-CNTRCT := CNCT.BC-TIAA-CNTRCT
                pnd_Bd_S_P_C_I_T_S_Pnd_Bd_Tiaa_Cref_Ind.setValue(cnct_Bc_Tiaa_Cref_Ind);                                                                                  //Natural: ASSIGN #BD-TIAA-CREF-IND := CNCT.BC-TIAA-CREF-IND
                pnd_Bd_S_P_C_I_T_S_Pnd_Bd_Stat.setValue(pnd_Cont_Stat);                                                                                                   //Natural: ASSIGN #BD-STAT := #CONT-STAT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Error.setValue(true);                                                                                                                                     //Natural: ASSIGN #ERROR := TRUE
            pdaBena011.getBena011_Pnd_Err_Ind().setValue("System error! No Bene-Contract rec. Contact Support");                                                          //Natural: ASSIGN BENA011.#ERR-IND := 'System error! No Bene-Contract rec. Contact Support'
            if (condition(cnct_Bc_Eff_Dte.lessOrEqual(" ")))                                                                                                              //Natural: IF CNCT.BC-EFF-DTE LE ' '
            {
                pdaBena011.getBena011_Pnd_Err_Ind().setValue("System error! Invalid effective date. Contact Support");                                                    //Natural: ASSIGN BENA011.#ERR-IND := 'System error! Invalid effective date. Contact Support'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(! (cnct_Bc_Mos_Ind.equals("Y") || cnct_Bc_Mos_Ind.equals("N"))))                                                                                //Natural: IF NOT ( CNCT.BC-MOS-IND = 'Y' OR = 'N' )
            {
                pdaBena011.getBena011_Pnd_Err_Ind().setValue("System error! Invalid MOS Indicator. Contact Support");                                                     //Natural: ASSIGN BENA011.#ERR-IND := 'System error! Invalid MOS Indicator. Contact Support'
            }                                                                                                                                                             //Natural: END-IF
            //*  MOVE '(This may happen if 2 or more Designations for Pin/'-
            //*    'Contract are being processed)' TO BENA011.#ERR-MESSAGE
        }                                                                                                                                                                 //Natural: END-IF
        //*  POP-REST-OF-A011
    }
    private void sub_Read_Mosfslash_Bene_And_Pop_002() throws Exception                                                                                                   //Natural: READ-MOS/BENE-AND-POP-002
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pdaBena011.getBena011_Pnd_Mos_Ind().equals("Y")))                                                                                                   //Natural: IF BENA011.#MOS-IND = 'Y'
        {
            vw_pnd_Mos.startDatabaseRead                                                                                                                                  //Natural: READ ( 1 ) #MOS BY BM-STAT-PIN-CNTRCT-IND-KEY = #BM-S-P-C-I-KEY
            (
            "RM",
            new Wc[] { new Wc("BM_STAT_PIN_CNTRCT_IND_KEY", ">=", pnd_Bm_S_P_C_I_Key, WcType.BY) },
            new Oc[] { new Oc("BM_STAT_PIN_CNTRCT_IND_KEY", "ASC") },
            1
            );
            RM:
            while (condition(vw_pnd_Mos.readNextRow("RM")))
            {
                if (condition(pnd_Mos_Bm_Pin.greater(pnd_Bm_S_P_C_I_Key_Pnd_Bm_Pin) || pnd_Mos_Bm_Tiaa_Cntrct.greater(pnd_Bm_S_P_C_I_Key_Pnd_Bm_Tiaa_Cntrct)              //Natural: IF BM-PIN GT #BM-PIN OR BM-TIAA-CNTRCT GT #BM-TIAA-CNTRCT OR BM-STAT NE #BM-STAT OR BM-TIAA-CREF-IND NE #BM-TIAA-CREF-IND
                    || pnd_Mos_Bm_Stat.notEquals(pnd_Bm_S_P_C_I_Key_Pnd_Bm_Stat) || pnd_Mos_Bm_Tiaa_Cref_Ind.notEquals(pnd_Bm_S_P_C_I_Key_Pnd_Bm_Tiaa_Cref_Ind)))
                {
                    if (true) break RM;                                                                                                                                   //Natural: ESCAPE BOTTOM ( RM. )
                }                                                                                                                                                         //Natural: END-IF
                pdaBena011.getBena011_Pnd_Mos_Txt().getValue("*").setValue(pnd_Mos_Bm_Mos_Txt.getValue("*"));                                                             //Natural: ASSIGN BENA011.#MOS-TXT ( * ) := #MOS.BM-MOS-TXT ( * )
                pdaBena011.getBena011_Pnd_C_Mos_Txt().setValue(pnd_Mos_Count_Castbm_Mos_Txt_Group);                                                                       //Natural: ASSIGN BENA011.#C-MOS-TXT := #MOS.C*BM-MOS-TXT-GROUP
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
            //*  1ST 11 LINES MAY BE EMPTY.
            //*  ONLINE EDIT ONLY FOR SOMETHING
            //*  EACH 12 LINES.
            if (condition(! (pdaBena011.getBena011_Pnd_Mos_Txt().getValue("*").notEquals(" "))))                                                                          //Natural: IF NOT BENA011.#MOS-TXT ( * ) NE ' '
            {
                pnd_Error.setValue(true);                                                                                                                                 //Natural: ASSIGN #ERROR := TRUE
                pdaBena011.getBena011_Pnd_Err_Ind().setValue("System error! No Bene-MOS rec. Contact Support");                                                           //Natural: ASSIGN BENA011.#ERR-IND := 'System error! No Bene-MOS rec. Contact Support'
            }                                                                                                                                                             //Natural: END-IF
            //*  REGULAR BENE-DESIGNATION
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_P.reset();                                                                                                                                                //Natural: RESET #P
            vw_pnd_Ben.startDatabaseRead                                                                                                                                  //Natural: READ #BEN BY BD-STAT-PIN-CNTRCT-IND-TYPE-SEQ = #BD-S-P-C-I-T-S
            (
            "RB",
            new Wc[] { new Wc("BD_STAT_PIN_CNTRCT_IND_TYPE_SEQ", ">=", pnd_Bd_S_P_C_I_T_S, WcType.BY) },
            new Oc[] { new Oc("BD_STAT_PIN_CNTRCT_IND_TYPE_SEQ", "ASC") }
            );
            RB:
            while (condition(vw_pnd_Ben.readNextRow("RB")))
            {
                if (condition(pnd_Ben_Bd_Pin.greater(pnd_Bd_S_P_C_I_T_S_Pnd_Bd_Pin) || pnd_Ben_Bd_Tiaa_Cntrct.greater(pnd_Bd_S_P_C_I_T_S_Pnd_Bd_Tiaa_Cntrct)              //Natural: IF BD-PIN GT #BD-PIN OR BD-TIAA-CNTRCT GT #BD-TIAA-CNTRCT OR BD-STAT GT #BD-STAT OR BD-TIAA-CREF-IND NE #BD-TIAA-CREF-IND
                    || pnd_Ben_Bd_Stat.greater(pnd_Bd_S_P_C_I_T_S_Pnd_Bd_Stat) || pnd_Ben_Bd_Tiaa_Cref_Ind.notEquals(pnd_Bd_S_P_C_I_T_S_Pnd_Bd_Tiaa_Cref_Ind)))
                {
                    if (true) break RB;                                                                                                                                   //Natural: ESCAPE BOTTOM ( RB. )
                }                                                                                                                                                         //Natural: END-IF
                pnd_P.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #P
                pdaBena002.getPnd_Bena002_Pnd_Bene_Type().getValue(pnd_P).setValue(pnd_Ben_Bd_Bene_Type);                                                                 //Natural: ASSIGN #BENA002.#BENE-TYPE ( #P ) := #BEN.BD-BENE-TYPE
                pdaBena002.getPnd_Bena002_Pnd_Bene_Name1().getValue(pnd_P).setValue(pnd_Ben_Bd_Bene_Name1);                                                               //Natural: ASSIGN #BENA002.#BENE-NAME1 ( #P ) := #BEN.BD-BENE-NAME1
                pdaBena002.getPnd_Bena002_Pnd_Bene_Name2().getValue(pnd_P).setValue(pnd_Ben_Bd_Bene_Name2);                                                               //Natural: ASSIGN #BENA002.#BENE-NAME2 ( #P ) := #BEN.BD-BENE-NAME2
                pdaBena002.getPnd_Bena002_Pnd_Rltn_Cd().getValue(pnd_P).setValue(pnd_Ben_Bd_Rltn_Cd);                                                                     //Natural: ASSIGN #BENA002.#RLTN-CD ( #P ) := #BEN.BD-RLTN-CD
                if (condition(pnd_Ben_Bd_Rltn_Free_Txt.equals(" ")))                                                                                                      //Natural: IF #BEN.BD-RLTN-FREE-TXT = ' '
                {
                    if (condition(pdaBena002.getPnd_Bena002_Pnd_Rltn_Cd().getValue(pnd_P).notEquals(" ")))                                                                //Natural: IF #BENA002.#RLTN-CD ( #P ) NE ' '
                    {
                        pnd_Bt_Table_Id_Key.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "RC", pdaBena002.getPnd_Bena002_Pnd_Rltn_Cd().getValue(pnd_P)));     //Natural: COMPRESS 'RC' #BENA002.#RLTN-CD ( #P ) INTO #BT-TABLE-ID-KEY LEAVING NO
                        vw_bt.startDatabaseFind                                                                                                                           //Natural: FIND ( 1 ) BT WITH BT-TABLE-ID-KEY = #BT-TABLE-ID-KEY
                        (
                        "FBT",
                        new Wc[] { new Wc("BT_TABLE_ID_KEY", "=", pnd_Bt_Table_Id_Key, WcType.WITH) },
                        1
                        );
                        FBT:
                        while (condition(vw_bt.readNextRow("FBT")))
                        {
                            vw_bt.setIfNotFoundControlFlag(false);
                            pdaBena002.getPnd_Bena002_Pnd_Rltn_Free_Txt().getValue(pnd_P).setValue(gdaBeng200.getTable_Bt_Table_Text());                                  //Natural: ASSIGN #BENA002.#RLTN-FREE-TXT ( #P ) := BT-TABLE-TEXT
                        }                                                                                                                                                 //Natural: END-FIND
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RB"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RB"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaBena002.getPnd_Bena002_Pnd_Rltn_Free_Txt().getValue(pnd_P).setValue(pnd_Ben_Bd_Rltn_Free_Txt);                                                     //Natural: ASSIGN #BENA002.#RLTN-FREE-TXT ( #P ) := #BEN.BD-RLTN-FREE-TXT
                }                                                                                                                                                         //Natural: END-IF
                pdaBena002.getPnd_Bena002_Pnd_Dte_Birth_Trust().getValue(pnd_P).setValue(pnd_Ben_Bd_Dte_Birth_Trust);                                                     //Natural: ASSIGN #BENA002.#DTE-BIRTH-TRUST ( #P ) := #BEN.BD-DTE-BIRTH-TRUST
                pdaBena002.getPnd_Bena002_Pnd_Ss_Nmbr().getValue(pnd_P).setValue(pnd_Ben_Bd_Ss_Nmbr);                                                                     //Natural: ASSIGN #BENA002.#SS-NMBR ( #P ) := #BEN.BD-SS-NMBR
                pdaBena002.getPnd_Bena002_Pnd_Ss_Cd().getValue(pnd_P).setValue(pnd_Ben_Bd_Ss_Cd);                                                                         //Natural: ASSIGN #BENA002.#SS-CD ( #P ) := #BEN.BD-SS-CD
                pdaBena002.getPnd_Bena002_Pnd_Bene_Irvcbl().getValue(pnd_P).setValue(pnd_Ben_Bd_Irvcbl_Ind);                                                              //Natural: ASSIGN #BENA002.#BENE-IRVCBL ( #P ) := #BEN.BD-IRVCBL-IND
                pdaBena002.getPnd_Bena002_Pnd_Stlmnt_Rstrctn().getValue(pnd_P).setValue(pnd_Ben_Bd_Stlmnt_Rstrctn);                                                       //Natural: ASSIGN #BENA002.#STLMNT-RSTRCTN ( #P ) := #BEN.BD-STLMNT-RSTRCTN
                pdaBena002.getPnd_Bena002_Pnd_Perc_Share_Ind().getValue(pnd_P).setValue(pnd_Ben_Bd_Perc_Share_Ind);                                                       //Natural: ASSIGN #BENA002.#PERC-SHARE-IND ( #P ) := #BEN.BD-PERC-SHARE-IND
                pdaBena002.getPnd_Bena002_Pnd_Share_Perc().getValue(pnd_P).setValue(pnd_Ben_Bd_Share_Perc);                                                               //Natural: ASSIGN #BENA002.#SHARE-PERC ( #P ) := #BEN.BD-SHARE-PERC
                pdaBena002.getPnd_Bena002_Pnd_Share_Ntor().getValue(pnd_P).setValue(pnd_Ben_Bd_Share_Ntor);                                                               //Natural: ASSIGN #BENA002.#SHARE-NTOR ( #P ) := #BEN.BD-SHARE-NTOR
                pdaBena002.getPnd_Bena002_Pnd_Share_Dtor().getValue(pnd_P).setValue(pnd_Ben_Bd_Share_Dtor);                                                               //Natural: ASSIGN #BENA002.#SHARE-DTOR ( #P ) := #BEN.BD-SHARE-DTOR
                pdaBena002.getPnd_Bena002_Pnd_Spcl_Txt1().getValue(pnd_P).setValue(pnd_Ben_Bd_Spcl_Txt1);                                                                 //Natural: ASSIGN #BENA002.#SPCL-TXT1 ( #P ) := #BEN.BD-SPCL-TXT1
                pdaBena002.getPnd_Bena002_Pnd_Spcl_Txt2().getValue(pnd_P).setValue(pnd_Ben_Bd_Spcl_Txt2);                                                                 //Natural: ASSIGN #BENA002.#SPCL-TXT2 ( #P ) := #BEN.BD-SPCL-TXT2
                pdaBena002.getPnd_Bena002_Pnd_Spcl_Txt3().getValue(pnd_P).setValue(pnd_Ben_Bd_Spcl_Txt3);                                                                 //Natural: ASSIGN #BENA002.#SPCL-TXT3 ( #P ) := #BEN.BD-SPCL-TXT3
                pdaBena002.getPnd_Bena002_Pnd_Dflt_To_Estate().getValue(pnd_P).setValue(pnd_Ben_Bd_Dflt_Estate);                                                          //Natural: ASSIGN #BENA002.#DFLT-TO-ESTATE ( #P ) := #BEN.BD-DFLT-ESTATE
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
            if (condition(pdaBena002.getPnd_Bena002_Pnd_Bene_Type().getValue(1).equals(" ")))                                                                             //Natural: IF #BENA002.#BENE-TYPE ( 1 ) = ' '
            {
                pnd_Error.setValue(true);                                                                                                                                 //Natural: ASSIGN #ERROR := TRUE
                pdaBena011.getBena011_Pnd_Err_Ind().setValue("System error! No Bene-Designation rec. Contact Support");                                                   //Natural: ASSIGN BENA011.#ERR-IND := 'System error! No Bene-Designation rec. Contact Support'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  READ-MOS/BENE-AND-POP-002
    }
    private void sub_Err_Write() throws Exception                                                                                                                         //Natural: ERR-WRITE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_E_Cnt.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #E-CNT
        pnd_Line_Err.reset();                                                                                                                                             //Natural: RESET #LINE-ERR #ERROR-HOLD #ERROR-LINE #K
        pnd_Error_Hold.reset();
        pnd_Error_Line.reset();
        pnd_K.reset();
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
                                                                                                                                                                          //Natural: PERFORM CHECK-OR-WRITE-TITLE
        sub_Check_Or_Write_Title();
        if (condition(Global.isEscape())) {return;}
        pnd_Error_Line_Pnd_Error_Name.setValue(pdaBena011.getBena011_Pnd_Nas_Name().getSubstring(1,33));                                                                  //Natural: MOVE SUBSTR ( BENA011.#NAS-NAME,1,33 ) TO #ERROR-NAME
        pnd_Error_Line_Pnd_Error_Dte.setValueEdited(pnd_Old_Dte,new ReportEditMask("MM/DD/YYYY"));                                                                        //Natural: MOVE EDITED #OLD-DTE ( EM = MM/DD/YYYY ) TO #ERROR-DTE
        //*  LITERAL FROM BENN013 SHOULD
        //*  FIT INTO 45
        pnd_Error_Line_Pnd_Error_Pin.setValueEdited(pdaBena011.getBena011_Pnd_Pin(),new ReportEditMask("999999999999"));                                                  //Natural: MOVE EDITED BENA011.#PIN ( EM = 999999999999 ) TO #ERROR-PIN
        pnd_Error_Line_Pnd_Error_Wpid.setValue(pdaBena011.getBena011_Pnd_Wpid());                                                                                         //Natural: ASSIGN #ERROR-WPID := BENA011.#WPID
        pnd_Error_Line_Pnd_Error_Wpid_Unit.setValue(pdaBena011.getBena011_Pnd_Wpid_Unit());                                                                               //Natural: ASSIGN #ERROR-WPID-UNIT := BENA011.#WPID-UNIT
        pnd_Error_Line_Pnd_Error_Msg.setValue(pdaBena011.getBena011_Pnd_Err_Ind());                                                                                       //Natural: ASSIGN #ERROR-MSG := BENA011.#ERR-IND
        FOR02:                                                                                                                                                            //Natural: FOR #IND 1 #MAX-CNTRCT
        for (pnd_Ind.setValue(1); condition(pnd_Ind.lessOrEqual(pdaBena011.getBena011_Pnd_Max_Cntrct())); pnd_Ind.nadd(1))
        {
            //*  NO MORE SELECTED CONTRACTS
            if (condition(! (pdaBena011.getBena011_Pnd_Slctd().getValue(pnd_Ind,":",pdaBena011.getBena011_Pnd_Max_Cntrct()).notEquals(" "))))                             //Natural: IF NOT #SLCTD ( #IND:#MAX-CNTRCT ) NE ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(getReports().getAstLineCount(1).greaterOrEqual(58)))                                                                                            //Natural: IF *LINE-COUNT ( 1 ) GE 58
            {
                                                                                                                                                                          //Natural: PERFORM CHECK-OR-WRITE-TITLE
                sub_Check_Or_Write_Title();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  TIAA SELECTED
            if (condition(pdaBena011.getBena011_Pnd_Tiaa_Cref_Ind().getValue(pnd_Ind).equals("T")))                                                                       //Natural: IF BENA011.#TIAA-CREF-IND ( #IND ) = 'T'
            {
                                                                                                                                                                          //Natural: PERFORM COUNT-AND-CHECK/FORMAT-ERROR
                sub_Count_And_Checkfslash_Format_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Error_Line_Pnd_Err_Cntrct.setValue(pdaBena011.getBena011_Pnd_Tiaa_Cntrct().getValue(pnd_Ind));                                                        //Natural: ASSIGN #ERR-CNTRCT := BENA011.#TIAA-CNTRCT ( #IND )
                getReports().write(1, ReportOption.NOTITLE,pnd_Error_Line);                                                                                               //Natural: WRITE ( 1 ) #ERROR-LINE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Error_Line.reset();                                                                                                                                   //Natural: RESET #ERROR-LINE
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  CREF SELECTED
            if (condition(pdaBena011.getBena011_Pnd_Tiaa_Cref_Ind().getValue(pnd_Ind).equals("C")))                                                                       //Natural: IF BENA011.#TIAA-CREF-IND ( #IND ) = 'C'
            {
                                                                                                                                                                          //Natural: PERFORM COUNT-AND-CHECK/FORMAT-ERROR
                sub_Count_And_Checkfslash_Format_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Error_Line_Pnd_Err_Cntrct.setValue(pdaBena011.getBena011_Pnd_Cref_Cntrct().getValue(pnd_Ind));                                                        //Natural: ASSIGN #ERR-CNTRCT := BENA011.#CREF-CNTRCT ( #IND )
                getReports().write(1, ReportOption.NOTITLE,pnd_Error_Line);                                                                                               //Natural: WRITE ( 1 ) #ERROR-LINE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Error_Line.reset();                                                                                                                                   //Natural: RESET #ERROR-LINE
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //* ** AT THIS POINT WE NEED BOTH CONTRACTS
                                                                                                                                                                          //Natural: PERFORM COUNT-AND-CHECK/FORMAT-ERROR
            sub_Count_And_Checkfslash_Format_Error();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Error_Line_Pnd_Err_Cntrct.setValue(pdaBena011.getBena011_Pnd_Tiaa_Cntrct().getValue(pnd_Ind));                                                            //Natural: ASSIGN #ERR-CNTRCT := BENA011.#TIAA-CNTRCT ( #IND )
            getReports().write(1, ReportOption.NOTITLE,pnd_Error_Line);                                                                                                   //Natural: WRITE ( 1 ) #ERROR-LINE
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Error_Line.reset();                                                                                                                                       //Natural: RESET #ERROR-LINE
                                                                                                                                                                          //Natural: PERFORM COUNT-AND-CHECK/FORMAT-ERROR
            sub_Count_And_Checkfslash_Format_Error();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Error_Line_Pnd_Err_Cntrct.setValue(pdaBena011.getBena011_Pnd_Cref_Cntrct().getValue(pnd_Ind));                                                            //Natural: ASSIGN #ERR-CNTRCT := BENA011.#CREF-CNTRCT ( #IND )
            getReports().write(1, ReportOption.NOTITLE,pnd_Error_Line);                                                                                                   //Natural: WRITE ( 1 ) #ERROR-LINE
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Error_Line.reset();                                                                                                                                       //Natural: RESET #ERROR-LINE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Line_Err.less(3)))                                                                                                                              //Natural: IF #LINE-ERR LT 3
        {
            //*  NO CONTRCAT, NO ERROR YET PRINTED
            if (condition(pnd_Line_Err.less(1)))                                                                                                                          //Natural: IF #LINE-ERR LT 1
            {
                                                                                                                                                                          //Natural: PERFORM COUNT-AND-CHECK/FORMAT-ERROR
                sub_Count_And_Checkfslash_Format_Error();
                if (condition(Global.isEscape())) {return;}
                getReports().write(1, ReportOption.NOTITLE,pnd_Error_Line);                                                                                               //Natural: WRITE ( 1 ) #ERROR-LINE
                if (Global.isEscape()) return;
                pnd_Error_Line.reset();                                                                                                                                   //Natural: RESET #ERROR-LINE
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM COUNT-AND-CHECK/FORMAT-ERROR
            sub_Count_And_Checkfslash_Format_Error();
            if (condition(Global.isEscape())) {return;}
            getReports().write(1, ReportOption.NOTITLE,pnd_Error_Line);                                                                                                   //Natural: WRITE ( 1 ) #ERROR-LINE
            if (Global.isEscape()) return;
            pnd_Error_Line.reset();                                                                                                                                       //Natural: RESET #ERROR-LINE
            if (condition(pnd_Line_Err.less(3)))                                                                                                                          //Natural: IF #LINE-ERR LT 3
            {
                                                                                                                                                                          //Natural: PERFORM COUNT-AND-CHECK/FORMAT-ERROR
                sub_Count_And_Checkfslash_Format_Error();
                if (condition(Global.isEscape())) {return;}
                if (condition(pnd_Error_Line.greater(" ")))                                                                                                               //Natural: IF #ERROR-LINE GT ' '
                {
                    getReports().write(1, ReportOption.NOTITLE,pnd_Error_Line);                                                                                           //Natural: WRITE ( 1 ) #ERROR-LINE
                    if (Global.isEscape()) return;
                    pnd_Error_Line.reset();                                                                                                                               //Natural: RESET #ERROR-LINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ERR-WRITE
    }
    private void sub_Count_And_Checkfslash_Format_Error() throws Exception                                                                                                //Natural: COUNT-AND-CHECK/FORMAT-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Line_Err.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #LINE-ERR
        if (condition(pnd_Line_Err.equals(2)))                                                                                                                            //Natural: IF #LINE-ERR = 2
        {
            pnd_Error_Hold.setValue(pdaBena011.getBena011_Pnd_Err_Message());                                                                                             //Natural: ASSIGN #ERROR-HOLD := BENA011.#ERR-MESSAGE
            if (condition(pnd_Error_Hold.greater(" ")))                                                                                                                   //Natural: IF #ERROR-HOLD GT ' '
            {
                FOR03:                                                                                                                                                    //Natural: FOR #K 45 1 STEP -1
                for (pnd_K.setValue(45); condition(pnd_K.greaterOrEqual(1)); pnd_K.nsubtract(1))
                {
                    if (condition(pnd_Error_Hold_Pnd_Error_Hold_Arr.getValue(pnd_K).equals(" ")))                                                                         //Natural: IF #ERROR-HOLD-ARR ( #K ) = ' '
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                pnd_L.compute(new ComputeParameters(false, pnd_L), DbsField.subtract(80,pnd_K));                                                                          //Natural: COMPUTE #L = 80 - #K
                pnd_Error_Line_Pnd_Error_Msg_Arr.getValue("*").reset();                                                                                                   //Natural: RESET #ERROR-MSG-ARR ( * )
                pnd_Error_Line_Pnd_Error_Msg_Arr.getValue(1,":",pnd_K).setValue(pnd_Error_Hold_Pnd_Error_Hold_Arr.getValue(1,":",pnd_K));                                 //Natural: ASSIGN #ERROR-MSG-ARR ( 1:#K ) := #ERROR-HOLD-ARR ( 1:#K )
                if (condition(pnd_Error_Hold_Pnd_Error_Hold_Arr.getValue(pnd_K,":",79).notEquals(" ")))                                                                   //Natural: IF #ERROR-HOLD-ARR ( #K:79 ) NE ' '
                {
                    pnd_Error_Hold_Pnd_Error_Hold_Arr.getValue(1,":",pnd_L).setValue(pnd_Error_Hold_Pnd_Error_Hold_Arr.getValue(pnd_K,":",79));                           //Natural: ASSIGN #ERROR-HOLD-ARR ( 1:#L ) := #ERROR-HOLD-ARR ( #K:79 )
                    pnd_Error_Hold_Pnd_Error_Hold_Arr.getValue(pnd_L.getDec().add(1),":",79).reset();                                                                     //Natural: RESET #ERROR-HOLD-ARR ( #L +1:79 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Error_Hold_Pnd_Error_Hold_Arr.getValue("*").reset();                                                                                              //Natural: RESET #ERROR-HOLD-ARR ( * )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Line_Err.equals(3) && pnd_Error_Hold.greater(" ")))                                                                                             //Natural: IF #LINE-ERR = 3 AND #ERROR-HOLD GT ' '
        {
            pnd_Error_Line_Pnd_Error_Msg.setValue(pnd_Error_Hold);                                                                                                        //Natural: ASSIGN #ERROR-MSG := #ERROR-HOLD
        }                                                                                                                                                                 //Natural: END-IF
        //*  COUNT-AND-CHECK/FORMAT-ERROR
    }
    private void sub_Start_Up() throws Exception                                                                                                                          //Natural: START-UP
    {
        if (BLNatReinput.isReinput()) return;

        gdaBeng200.getPnd_Pnd_Control_I().setValue(gdaBeng200.getPnd_Pnd_Reportx());                                                                                      //Natural: ASSIGN ##CONTROL-I := ##REPORTX
        gdaBeng200.getPnd_Pnd_Program().setValue(Global.getPROGRAM());                                                                                                    //Natural: ASSIGN ##PROGRAM := *PROGRAM
        gdaBeng200.getPnd_Date_X().setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                       //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO #DATE-X
        gdaBeng200.getPnd_Date_Xd().setValueEdited(Global.getDATX(),new ReportEditMask("MM/DD/YYYY"));                                                                    //Natural: MOVE EDITED *DATX ( EM = MM/DD/YYYY ) TO #DATE-XD
        //*  PERFORM BENS200-START-UP             /* REMOVED BENE-CONTROL
        //*  START-UP
    }
    private void sub_Check_Or_Write_Title() throws Exception                                                                                                              //Natural: CHECK-OR-WRITE-TITLE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(getReports().getAstLineCount(1).greater(53)))                                                                                                       //Natural: IF *LINE-COUNT ( 1 ) GT 53
        {
            if (condition(getReports().getAstLineCount(1).less(60)))                                                                                                      //Natural: IF *LINE-COUNT ( 1 ) LT 60
            {
                FOR04:                                                                                                                                                    //Natural: FOR #M *LINE-COUNT ( 1 ) 60
                for (pnd_M.setValue(getReports().getAstLineCount(1)); condition(pnd_M.lessOrEqual(60)); pnd_M.nadd(1))
                {
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            pnd_Page_Number.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #PAGE-NUMBER
            getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Benm220.class));                                                                           //Natural: WRITE ( 1 ) NOTITLE USING FORM 'BENM220 '
        }                                                                                                                                                                 //Natural: END-IF
        //*  CHECK-OR-WRITE-TITLE
    }

    //

    // Support Methods

    private void atBreakEventS1() throws Exception {atBreakEventS1(false);}
    private void atBreakEventS1(boolean endOfData) throws Exception
    {
        boolean gdaBeng200_getAct_Ba_Seq_NmbrIsBreak = gdaBeng200.getAct_Ba_Seq_Nmbr().isBreak(endOfData);
        boolean gdaBeng200_getAct_Ba_Rqst_TimestampIsBreak = gdaBeng200.getAct_Ba_Rqst_Timestamp().isBreak(endOfData);
        if (condition(gdaBeng200_getAct_Ba_Seq_NmbrIsBreak || gdaBeng200_getAct_Ba_Rqst_TimestampIsBreak))
        {
            pnd_Old_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),s1Ba_Rqst_Cmpltd_DteOld);                                                                           //Natural: MOVE EDITED OLD ( ACT.BA-RQST-CMPLTD-DTE ) TO #OLD-DTE ( EM = YYYYMMDD )
            pnd_N.setValue(s1Ba_Seq_NmbrOld);                                                                                                                             //Natural: ASSIGN #N := OLD ( ACT.BA-SEQ-NMBR )
            pnd_Frm_Ind.compute(new ComputeParameters(false, pnd_Frm_Ind), (pnd_N.subtract(1)).multiply(20));                                                             //Natural: COMPUTE #FRM-IND = ( #N - 1 ) * 20
            pnd_End_Ind.setValue(s1Count_Castba_Slctd_Cntrct_GroupOld);                                                                                                   //Natural: ASSIGN #END-IND := OLD ( ACT.C*BA-SLCTD-CNTRCT-GROUP )
            pnd_Slctd_Tiaa.getValue(1,":",20).setValue(s1Act_Ba_Slctd_TiaaOld.getValue(1,":",20));                                                                        //Natural: ASSIGN #SLCTD-TIAA ( 1:20 ) := OLD ( BA-SLCTD-TIAA ( 1:20 ) )
            pnd_Slctd_Cref.getValue(1,":",20).setValue(s1Act_Ba_Slctd_CrefOld.getValue(1,":",20));                                                                        //Natural: ASSIGN #SLCTD-CREF ( 1:20 ) := OLD ( BA-SLCTD-CREF ( 1:20 ) )
            FOR:                                                                                                                                                          //Natural: FOR #P = 1 TO #END-IND
            for (pnd_P.setValue(1); condition(pnd_P.lessOrEqual(pnd_End_Ind)); pnd_P.nadd(1))
            {
                if (condition(pnd_Slctd_Tiaa.getValue(pnd_P).greater(" ")))                                                                                               //Natural: IF #SLCTD-TIAA ( #P ) GT ' '
                {
                    pnd_Cref_Only.reset();                                                                                                                                //Natural: RESET #CREF-ONLY
                    pdaBena011.getBena011_Pnd_Tiaa_Cntrct().getValue(pnd_Frm_Ind.getDec().add(pnd_P)).setValue(pnd_Slctd_Tiaa.getValue(pnd_P));                           //Natural: ASSIGN BENA011.#TIAA-CNTRCT ( #FRM-IND + #P ) := #BC-TIAA-CNTRCT := #SLCTD-TIAA ( #P )
                    pnd_Bc_S_P_C_I_Key_Pnd_Bc_Tiaa_Cntrct.setValue(pnd_Slctd_Tiaa.getValue(pnd_P));
                    pdaBena011.getBena011_Pnd_Slctd().getValue(pnd_Frm_Ind.getDec().add(pnd_P)).setValue("S");                                                            //Natural: ASSIGN BENA011.#SLCTD ( #FRM-IND + #P ) := 'S'
                    if (condition(pnd_Slctd_Cref.getValue(pnd_P).lessOrEqual(" ")))                                                                                       //Natural: IF #SLCTD-CREF ( #P ) LE ' '
                    {
                        pdaBena011.getBena011_Pnd_Tiaa_Cref_Ind().getValue(pnd_Frm_Ind.getDec().add(pnd_P)).setValue("T");                                                //Natural: ASSIGN BENA011.#TIAA-CREF-IND ( #FRM-IND + #P ) := 'T'
                    }                                                                                                                                                     //Natural: END-IF
                    //*        #BC-TIAA-CREF-IND := BENA011.#TIAA-CREF-IND (#P)
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Slctd_Cref.getValue(pnd_P).greater(" ")))                                                                                               //Natural: IF #SLCTD-CREF ( #P ) GT ' '
                {
                    pdaBena011.getBena011_Pnd_Cref_Cntrct().getValue(pnd_Frm_Ind.getDec().add(pnd_P)).setValue(pnd_Slctd_Cref.getValue(pnd_P));                           //Natural: ASSIGN BENA011.#CREF-CNTRCT ( #FRM-IND + #P ) := #SLCTD-CREF ( #P )
                    pdaBena011.getBena011_Pnd_Slctd().getValue(pnd_Frm_Ind.getDec().add(pnd_P)).setValue("S");                                                            //Natural: ASSIGN BENA011.#SLCTD ( #FRM-IND + #P ) := 'S'
                    if (condition(pnd_Slctd_Tiaa.getValue(pnd_P).lessOrEqual(" ")))                                                                                       //Natural: IF #SLCTD-TIAA ( #P ) LE ' '
                    {
                        pdaBena011.getBena011_Pnd_Tiaa_Cref_Ind().getValue(pnd_Frm_Ind.getDec().add(pnd_P)).setValue("C");                                                //Natural: ASSIGN BENA011.#TIAA-CREF-IND ( #FRM-IND + #P ) := 'C'
                        if (condition(! (pdaBena011.getBena011_Pnd_Tiaa_Cntrct().getValue("*").notEquals(" "))))                                                          //Natural: IF NOT BENA011.#TIAA-CNTRCT ( * ) NE ' '
                        {
                            pnd_Cref_Only.setValue(true);                                                                                                                 //Natural: ASSIGN #CREF-ONLY := TRUE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape())) return;
            //*  UPDT BA-CNFRM-STMNT-PROD-DTE
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(gdaBeng200_getAct_Ba_Rqst_TimestampIsBreak))
        {
            //*   IF #CREF-ONLY
            pdaBena011.getBena011_Pnd_Sys_Dte().setValue(gdaBeng200.getPnd_Date_X());                                                                                     //Natural: ASSIGN BENA011.#SYS-DTE := #DATE-X
            pdaBena011.getBena011_Pnd_Wpid_Unit().setValue(s1Act_Ba_Wpid_UnitOld);                                                                                        //Natural: ASSIGN BENA011.#WPID-UNIT := OLD ( BA-WPID-UNIT )
            pdaBena011.getBena011_Pnd_Wpid().setValue(s1Act_Ba_WpidOld);                                                                                                  //Natural: ASSIGN BENA011.#WPID := OLD ( BA-WPID )
            pdaBena011.getBena011_Pnd_Nas_Name().setValue(s1Act_Ba_NameOld);                                                                                              //Natural: ASSIGN BENA011.#NAS-NAME := OLD ( BA-NAME )
            pdaBena011.getBena011_Pnd_Owner_Name().setValue(s1Act_Ba_Owner_NameOld);                                                                                      //Natural: ASSIGN BENA011.#OWNER-NAME := OLD ( BA-OWNER-NAME )
            pdaBena011.getBena011_Pnd_Rqst_Log_Dte_Tme().setValue(s1Ba_Rqst_TimestampOld);                                                                                //Natural: ASSIGN BENA011.#RQST-LOG-DTE-TME := OLD ( ACT.BA-RQST-TIMESTAMP )
            pdaBena011.getBena011_Pnd_Lttr_Info().setValue(s1Act_Ba_Lttr_InfoOld);                                                                                        //Natural: ASSIGN BENA011.#LTTR-INFO := OLD ( BA-LTTR-INFO )
            pdaBena011.getBena011_Pnd_Mail_Addr().getValue(1,":",5).setValue(s1Act_Ba_Mail_AddrOld.getValue(1,":",5));                                                    //Natural: ASSIGN BENA011.#MAIL-ADDR ( 1:5 ) := OLD ( BA-MAIL-ADDR ( 1:5 ) )
            pdaBena011.getBena011_Pnd_Us_Frgn().setValue(s1Act_Ba_Us_FrgnOld);                                                                                            //Natural: ASSIGN BENA011.#US-FRGN := OLD ( BA-US-FRGN )
            pdaBena011.getBena011_Pnd_Mail_Postal_Data().setValue(s1Act_Ba_Addrss_Postal_DataOld);                                                                        //Natural: ASSIGN BENA011.#MAIL-POSTAL-DATA := OLD ( BA-ADDRSS-POSTAL-DATA )
            pdaBena011.getBena011_Pnd_Userid().setValue(s1Act_Ba_Rcrd_Last_Updt_UseridOld);                                                                               //Natural: ASSIGN BENA011.#USERID := OLD ( BA-RCRD-LAST-UPDT-USERID )
            pdaBena011.getBena011_Pnd_Diff_Dsgntn().setValue(s1Act_Ba_Diff_DsgntnOld);                                                                                    //Natural: ASSIGN BENA011.#DIFF-DSGNTN := OLD ( BA-DIFF-DSGNTN )
            pdaBena011.getBena011_Pnd_Pin().setValue(s1Ba_PinOld);                                                                                                        //Natural: ASSIGN BENA011.#PIN := #BC-PIN := #BD-PIN := #BM-PIN := OLD ( ACT.BA-PIN )
            pnd_Bc_S_P_C_I_Key_Pnd_Bc_Pin.setValue(s1Ba_PinOld);
            pnd_Bd_S_P_C_I_T_S_Pnd_Bd_Pin.setValue(s1Ba_PinOld);
            pnd_Bm_S_P_C_I_Key_Pnd_Bm_Pin.setValue(s1Ba_PinOld);
            if (condition(s1Act_Ba_Lttr_OptnOld.notEquals("2")))                                                                                                          //Natural: IF OLD ( BA-LTTR-OPTN ) NE '2'
            {
                pnd_Error.setValue(true);                                                                                                                                 //Natural: ASSIGN #ERROR := TRUE
                pdaBena011.getBena011_Pnd_Err_Ind().setValue("Confirmation Statement not produced for Local Print,");                                                     //Natural: ASSIGN BENA011.#ERR-IND := 'Confirmation Statement not produced for Local Print,'
                pdaBena011.getBena011_Pnd_Err_Message().setValue("Overnight Mail or Fax request.");                                                                       //Natural: ASSIGN BENA011.#ERR-MESSAGE := 'Overnight Mail or Fax request.'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  INCLUDES READ OF CONTRACT FILE
                                                                                                                                                                          //Natural: PERFORM POP-REST-OF-A011
                sub_Pop_Rest_Of_A011();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            if (condition(! (pnd_Error.getBoolean())))                                                                                                                    //Natural: IF NOT #ERROR
            {
                                                                                                                                                                          //Natural: PERFORM READ-MOS/BENE-AND-POP-002
                sub_Read_Mosfslash_Bene_And_Pop_002();
                if (condition(Global.isEscape())) {return;}
                if (condition(! (pnd_Error.getBoolean())))                                                                                                                //Natural: IF NOT #ERROR
                {
                    DbsUtil.callnat(Benn013.class , getCurrentProcessState(), pdaBena011.getBena011(), pdaBena002.getPnd_Bena002());                                      //Natural: CALLNAT 'BENN013' BENA011 #BENA002
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaBena011.getBena011_Pnd_Err_Ind().notEquals(" ") || pnd_Error.getBoolean()))                                                                  //Natural: IF BENA011.#ERR-IND NE ' ' OR #ERROR
            {
                getCurrentProcessState().getDbConv().dbRollback();                                                                                                        //Natural: BACKOUT TRANSACTION
                                                                                                                                                                          //Natural: PERFORM ERR-WRITE
                sub_Err_Write();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                pnd_Et_Cnt.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #ET-CNT
            }                                                                                                                                                             //Natural: END-IF
            pdaBena011.getBena011().reset();                                                                                                                              //Natural: RESET BENA011 #BENA002 #CREF-ONLY #ERROR
            pdaBena002.getPnd_Bena002().reset();
            pnd_Cref_Only.reset();
            pnd_Error.reset();
            pdaBena011.getBena011_Pnd_Max_Cntrct().setValue(gdaBeng200.getPnd_Pnd_Max_Cntrct());                                                                          //Natural: ASSIGN BENA011.#MAX-CNTRCT := ##MAX-CNTRCT
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
        Global.format(1, "PS=60 LS=133");
    }
}
