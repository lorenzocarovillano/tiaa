/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:15:34 PM
**        * FROM NATURAL PROGRAM : Benp5000
************************************************************
**        * FILE NAME            : Benp5000.java
**        * CLASS NAME           : Benp5000
**        * INSTANCE NAME        : Benp5000
************************************************************

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benp5000 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Input;
    private DbsField pnd_Input_Pnd_Input_Rec;

    private DbsGroup pnd_Input__R_Field_1;
    private DbsField pnd_Input_Pnd_Plan_Id;
    private DbsField pnd_Input_Pnd_Third_Party_Ind;
    private DbsField pnd_Input_Pnd_Del_Or_Inst_Ind;
    private DbsField pnd_Input_Pnd_Spousal_Waiver;
    private DbsField pnd_Input_Pnd_Erisa_Ind;
    private DbsField pnd_Input_Pnd_Approval_Ind;
    private DbsField pnd_Inp_System_Date;

    private DbsGroup pnd_Inp_System_Date__R_Field_2;
    private DbsField pnd_Inp_System_Date_Pnd_System_Date;
    private DbsField pnd_Inp_System_Date_Pnd_Inp_Filler;

    private DataAccessProgramView vw_bene_Table_File;
    private DbsField bene_Table_File_Bt_Table_Id;
    private DbsField bene_Table_File_Bt_Table_Key;
    private DbsField bene_Table_File_Bt_Table_Text;
    private DbsField bene_Table_File_Bt_Rcrd_Last_Updt_Dte;
    private DbsField bene_Table_File_Bt_Rcrd_Last_Updt_Tme;
    private DbsField bene_Table_File_Bt_Rcrd_Last_Updt_Userid;
    private DbsField pnd_Bt_Table_Id_Key;

    private DbsGroup pnd_Bt_Table_Id_Key__R_Field_3;
    private DbsField pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Id;
    private DbsField pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Key;
    private DbsField pnd_I;
    private DbsField pnd_Table;
    private DbsField pnd_Record_Found;
    private DbsField pnd_At_Start;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Input = localVariables.newGroupInRecord("pnd_Input", "#INPUT");
        pnd_Input_Pnd_Input_Rec = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Input_Rec", "#INPUT-REC", FieldType.STRING, 250);

        pnd_Input__R_Field_1 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_1", "REDEFINE", pnd_Input_Pnd_Input_Rec);
        pnd_Input_Pnd_Plan_Id = pnd_Input__R_Field_1.newFieldInGroup("pnd_Input_Pnd_Plan_Id", "#PLAN-ID", FieldType.STRING, 6);
        pnd_Input_Pnd_Third_Party_Ind = pnd_Input__R_Field_1.newFieldInGroup("pnd_Input_Pnd_Third_Party_Ind", "#THIRD-PARTY-IND", FieldType.STRING, 1);
        pnd_Input_Pnd_Del_Or_Inst_Ind = pnd_Input__R_Field_1.newFieldInGroup("pnd_Input_Pnd_Del_Or_Inst_Ind", "#DEL-OR-INST-IND", FieldType.STRING, 1);
        pnd_Input_Pnd_Spousal_Waiver = pnd_Input__R_Field_1.newFieldInGroup("pnd_Input_Pnd_Spousal_Waiver", "#SPOUSAL-WAIVER", FieldType.STRING, 1);
        pnd_Input_Pnd_Erisa_Ind = pnd_Input__R_Field_1.newFieldInGroup("pnd_Input_Pnd_Erisa_Ind", "#ERISA-IND", FieldType.STRING, 1);
        pnd_Input_Pnd_Approval_Ind = pnd_Input__R_Field_1.newFieldInGroup("pnd_Input_Pnd_Approval_Ind", "#APPROVAL-IND", FieldType.STRING, 1);
        pnd_Inp_System_Date = localVariables.newFieldInRecord("pnd_Inp_System_Date", "#INP-SYSTEM-DATE", FieldType.STRING, 250);

        pnd_Inp_System_Date__R_Field_2 = localVariables.newGroupInRecord("pnd_Inp_System_Date__R_Field_2", "REDEFINE", pnd_Inp_System_Date);
        pnd_Inp_System_Date_Pnd_System_Date = pnd_Inp_System_Date__R_Field_2.newFieldInGroup("pnd_Inp_System_Date_Pnd_System_Date", "#SYSTEM-DATE", FieldType.STRING, 
            8);
        pnd_Inp_System_Date_Pnd_Inp_Filler = pnd_Inp_System_Date__R_Field_2.newFieldInGroup("pnd_Inp_System_Date_Pnd_Inp_Filler", "#INP-FILLER", FieldType.STRING, 
            242);

        vw_bene_Table_File = new DataAccessProgramView(new NameInfo("vw_bene_Table_File", "BENE-TABLE-FILE"), "BENE_TABLE_FILE", "BENE_TABLE");
        bene_Table_File_Bt_Table_Id = vw_bene_Table_File.getRecord().newFieldInGroup("bene_Table_File_Bt_Table_Id", "BT-TABLE-ID", FieldType.STRING, 2, 
            RepeatingFieldStrategy.None, "BT_TABLE_ID");
        bene_Table_File_Bt_Table_Id.setDdmHeader("TABLE/ID");
        bene_Table_File_Bt_Table_Key = vw_bene_Table_File.getRecord().newFieldInGroup("bene_Table_File_Bt_Table_Key", "BT-TABLE-KEY", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "BT_TABLE_KEY");
        bene_Table_File_Bt_Table_Key.setDdmHeader("TABLE/KEY");
        bene_Table_File_Bt_Table_Text = vw_bene_Table_File.getRecord().newFieldInGroup("bene_Table_File_Bt_Table_Text", "BT-TABLE-TEXT", FieldType.STRING, 
            72, RepeatingFieldStrategy.None, "BT_TABLE_TEXT");
        bene_Table_File_Bt_Table_Text.setDdmHeader("TABLE/TEXT");
        bene_Table_File_Bt_Rcrd_Last_Updt_Dte = vw_bene_Table_File.getRecord().newFieldInGroup("bene_Table_File_Bt_Rcrd_Last_Updt_Dte", "BT-RCRD-LAST-UPDT-DTE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "BT_RCRD_LAST_UPDT_DTE");
        bene_Table_File_Bt_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bene_Table_File_Bt_Rcrd_Last_Updt_Tme = vw_bene_Table_File.getRecord().newFieldInGroup("bene_Table_File_Bt_Rcrd_Last_Updt_Tme", "BT-RCRD-LAST-UPDT-TME", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "BT_RCRD_LAST_UPDT_TME");
        bene_Table_File_Bt_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bene_Table_File_Bt_Rcrd_Last_Updt_Userid = vw_bene_Table_File.getRecord().newFieldInGroup("bene_Table_File_Bt_Rcrd_Last_Updt_Userid", "BT-RCRD-LAST-UPDT-USERID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "BT_RCRD_LAST_UPDT_USERID");
        bene_Table_File_Bt_Rcrd_Last_Updt_Userid.setDdmHeader("RECRD LAST/UPDATE/USER ID");
        registerRecord(vw_bene_Table_File);

        pnd_Bt_Table_Id_Key = localVariables.newFieldInRecord("pnd_Bt_Table_Id_Key", "#BT-TABLE-ID-KEY", FieldType.STRING, 22);

        pnd_Bt_Table_Id_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Bt_Table_Id_Key__R_Field_3", "REDEFINE", pnd_Bt_Table_Id_Key);
        pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Id = pnd_Bt_Table_Id_Key__R_Field_3.newFieldInGroup("pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Id", "#BT-TABLE-ID", FieldType.STRING, 
            2);
        pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Key = pnd_Bt_Table_Id_Key__R_Field_3.newFieldInGroup("pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Key", "#BT-TABLE-KEY", 
            FieldType.STRING, 20);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 9);
        pnd_Table = localVariables.newFieldArrayInRecord("pnd_Table", "#TABLE", FieldType.STRING, 2, new DbsArrayController(1, 5));
        pnd_Record_Found = localVariables.newFieldInRecord("pnd_Record_Found", "#RECORD-FOUND", FieldType.BOOLEAN, 1);
        pnd_At_Start = localVariables.newFieldInRecord("pnd_At_Start", "#AT-START", FieldType.NUMERIC, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_bene_Table_File.reset();

        localVariables.reset();
        pnd_Table.getValue(1).setInitialValue("OA");
        pnd_Table.getValue(2).setInitialValue("OO");
        pnd_Table.getValue(3).setInitialValue("OM");
        pnd_Table.getValue(4).setInitialValue("OE");
        pnd_Table.getValue(5).setInitialValue("OS");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Benp5000() throws Exception
    {
        super("Benp5000");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        READWORK01:                                                                                                                                                       //Natural: FORMAT ( 1 ) LS = 132 PS = 60;//Natural: READ WORK FILE 1 #INPUT
        while (condition(getWorkFiles().read(1, pnd_Input)))
        {
            pnd_At_Start.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #AT-START
            if (condition(pnd_At_Start.equals(1)))                                                                                                                        //Natural: IF #AT-START EQ 1
            {
                pnd_Inp_System_Date.setValue(pnd_Input_Pnd_Input_Rec);                                                                                                    //Natural: ASSIGN #INP-SYSTEM-DATE := #INPUT-REC
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM DISPLAY-REPORT
            sub_Display_Report();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Record_Found.setValue(false);                                                                                                                             //Natural: ASSIGN #RECORD-FOUND := FALSE
                                                                                                                                                                          //Natural: PERFORM CHECK-TABLE-EXSIST
            sub_Check_Table_Exsist();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(! (pnd_Record_Found.getBoolean())))                                                                                                             //Natural: IF NOT #RECORD-FOUND
            {
                                                                                                                                                                          //Natural: PERFORM STORE-ALL-RECORDS
                sub_Store_All_Records();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-REPORT
        //* *********************************
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-TABLE-EXSIST
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STORE-ALL-RECORDS
        //* *************************************
        //* *********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-APPROVAL-IND
        //* *********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-DELAYED-VESTED-OR-INST-IND
        //* *********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-THIRD-PARTY-IND
        //* *********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-ERISA-IND
        //* *********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-SPOUSAL-WAIVER-IND
    }
    private void sub_Display_Report() throws Exception                                                                                                                    //Natural: DISPLAY-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(1, "DATE",                                                                                                                                   //Natural: DISPLAY ( 1 ) 'DATE' #SYSTEM-DATE 'PLAN' #PLAN-ID 'THIRD PARTY-IND' #THIRD-PARTY-IND 'VEST IND' #DEL-OR-INST-IND 'SPOUSAL WAIVER' #SPOUSAL-WAIVER 'ERISA IND' #ERISA-IND 'APPROVAL IND' #APPROVAL-IND
        		pnd_Inp_System_Date_Pnd_System_Date,"PLAN",
        		pnd_Input_Pnd_Plan_Id,"THIRD PARTY-IND",
        		pnd_Input_Pnd_Third_Party_Ind,"VEST IND",
        		pnd_Input_Pnd_Del_Or_Inst_Ind,"SPOUSAL WAIVER",
        		pnd_Input_Pnd_Spousal_Waiver,"ERISA IND",
        		pnd_Input_Pnd_Erisa_Ind,"APPROVAL IND",
        		pnd_Input_Pnd_Approval_Ind);
        if (Global.isEscape()) return;
    }
    private void sub_Check_Table_Exsist() throws Exception                                                                                                                //Natural: CHECK-TABLE-EXSIST
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        pnd_Bt_Table_Id_Key.reset();                                                                                                                                      //Natural: RESET #BT-TABLE-ID-KEY
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO 5
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(5)); pnd_I.nadd(1))
        {
            pnd_Record_Found.setValue(false);                                                                                                                             //Natural: ASSIGN #RECORD-FOUND := FALSE
            pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Id.setValue(pnd_Table.getValue(pnd_I));                                                                                      //Natural: ASSIGN #BT-TABLE-ID := #TABLE ( #I )
            pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Key.setValue(pnd_Input_Pnd_Plan_Id);                                                                                         //Natural: ASSIGN #BT-TABLE-KEY := #PLAN-ID
            vw_bene_Table_File.startDatabaseRead                                                                                                                          //Natural: READ BENE-TABLE-FILE BY BT-TABLE-ID-KEY EQ #BT-TABLE-ID-KEY
            (
            "READ1",
            new Wc[] { new Wc("BT_TABLE_ID_KEY", ">=", pnd_Bt_Table_Id_Key, WcType.BY) },
            new Oc[] { new Oc("BT_TABLE_ID_KEY", "ASC") }
            );
            READ1:
            while (condition(vw_bene_Table_File.readNextRow("READ1")))
            {
                if (condition(pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Id.notEquals(bene_Table_File_Bt_Table_Id)))                                                                //Natural: IF #BT-TABLE-ID NE BT-TABLE-ID
                {
                    if (true) break READ1;                                                                                                                                //Natural: ESCAPE BOTTOM ( READ1. )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Key.notEquals(bene_Table_File_Bt_Table_Key)))                                                              //Natural: IF #BT-TABLE-KEY NE BT-TABLE-KEY
                {
                    if (true) break READ1;                                                                                                                                //Natural: ESCAPE BOTTOM ( READ1. )
                }                                                                                                                                                         //Natural: END-IF
                pnd_Record_Found.setValue(true);                                                                                                                          //Natural: ASSIGN #RECORD-FOUND := TRUE
                if (condition(pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Id.equals("OA")))                                                                                          //Natural: IF #BT-TABLE-ID EQ 'OA'
                {
                                                                                                                                                                          //Natural: PERFORM UPDATE-APPROVAL-IND
                    sub_Update_Approval_Ind();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (true) break READ1;                                                                                                                                //Natural: ESCAPE BOTTOM ( READ1. )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Id.equals("OO")))                                                                                          //Natural: IF #BT-TABLE-ID EQ 'OO'
                {
                                                                                                                                                                          //Natural: PERFORM UPDATE-DELAYED-VESTED-OR-INST-IND
                    sub_Update_Delayed_Vested_Or_Inst_Ind();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (true) break READ1;                                                                                                                                //Natural: ESCAPE BOTTOM ( READ1. )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Id.equals("OM")))                                                                                          //Natural: IF #BT-TABLE-ID EQ 'OM'
                {
                                                                                                                                                                          //Natural: PERFORM UPDATE-THIRD-PARTY-IND
                    sub_Update_Third_Party_Ind();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (true) break READ1;                                                                                                                                //Natural: ESCAPE BOTTOM ( READ1. )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Id.equals("OE")))                                                                                          //Natural: IF #BT-TABLE-ID EQ 'OE'
                {
                                                                                                                                                                          //Natural: PERFORM UPDATE-ERISA-IND
                    sub_Update_Erisa_Ind();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (true) break READ1;                                                                                                                                //Natural: ESCAPE BOTTOM ( READ1. )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Id.equals("OS")))                                                                                          //Natural: IF #BT-TABLE-ID EQ 'OS'
                {
                                                                                                                                                                          //Natural: PERFORM UPDATE-SPOUSAL-WAIVER-IND
                    sub_Update_Spousal_Waiver_Ind();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (true) break READ1;                                                                                                                                //Natural: ESCAPE BOTTOM ( READ1. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Store_All_Records() throws Exception                                                                                                                 //Natural: STORE-ALL-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        bene_Table_File_Bt_Table_Id.setValue(pnd_Table.getValue(1));                                                                                                      //Natural: ASSIGN BT-TABLE-ID := #TABLE ( 1 )
        bene_Table_File_Bt_Table_Key.setValue(pnd_Input_Pnd_Plan_Id);                                                                                                     //Natural: ASSIGN BT-TABLE-KEY := #PLAN-ID
        bene_Table_File_Bt_Table_Text.setValue(pnd_Input_Pnd_Approval_Ind);                                                                                               //Natural: ASSIGN BT-TABLE-TEXT := #APPROVAL-IND
        bene_Table_File_Bt_Rcrd_Last_Updt_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                            //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO BT-RCRD-LAST-UPDT-DTE
        bene_Table_File_Bt_Rcrd_Last_Updt_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("HHMMSST"));                                                             //Natural: MOVE EDITED *TIMX ( EM = HHMMSST ) TO BT-RCRD-LAST-UPDT-TME
        bene_Table_File_Bt_Rcrd_Last_Updt_Userid.setValue(Global.getUSER());                                                                                              //Natural: ASSIGN BT-RCRD-LAST-UPDT-USERID := *USER
        vw_bene_Table_File.insertDBRow();                                                                                                                                 //Natural: STORE BENE-TABLE-FILE
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        bene_Table_File_Bt_Table_Id.setValue(pnd_Table.getValue(2));                                                                                                      //Natural: ASSIGN BT-TABLE-ID := #TABLE ( 2 )
        bene_Table_File_Bt_Table_Key.setValue(pnd_Input_Pnd_Plan_Id);                                                                                                     //Natural: ASSIGN BT-TABLE-KEY := #PLAN-ID
        bene_Table_File_Bt_Table_Text.setValue(pnd_Input_Pnd_Del_Or_Inst_Ind);                                                                                            //Natural: ASSIGN BT-TABLE-TEXT := #DEL-OR-INST-IND
        bene_Table_File_Bt_Rcrd_Last_Updt_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                            //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO BT-RCRD-LAST-UPDT-DTE
        bene_Table_File_Bt_Rcrd_Last_Updt_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("HHMMSST"));                                                             //Natural: MOVE EDITED *TIMX ( EM = HHMMSST ) TO BT-RCRD-LAST-UPDT-TME
        bene_Table_File_Bt_Rcrd_Last_Updt_Userid.setValue(Global.getUSER());                                                                                              //Natural: ASSIGN BT-RCRD-LAST-UPDT-USERID := *USER
        vw_bene_Table_File.insertDBRow();                                                                                                                                 //Natural: STORE BENE-TABLE-FILE
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        bene_Table_File_Bt_Table_Id.setValue(pnd_Table.getValue(3));                                                                                                      //Natural: ASSIGN BT-TABLE-ID := #TABLE ( 3 )
        bene_Table_File_Bt_Table_Key.setValue(pnd_Input_Pnd_Plan_Id);                                                                                                     //Natural: ASSIGN BT-TABLE-KEY := #PLAN-ID
        bene_Table_File_Bt_Table_Text.setValue(pnd_Input_Pnd_Third_Party_Ind);                                                                                            //Natural: ASSIGN BT-TABLE-TEXT := #THIRD-PARTY-IND
        bene_Table_File_Bt_Rcrd_Last_Updt_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                            //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO BT-RCRD-LAST-UPDT-DTE
        bene_Table_File_Bt_Rcrd_Last_Updt_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("HHMMSST"));                                                             //Natural: MOVE EDITED *TIMX ( EM = HHMMSST ) TO BT-RCRD-LAST-UPDT-TME
        bene_Table_File_Bt_Rcrd_Last_Updt_Userid.setValue(Global.getUSER());                                                                                              //Natural: ASSIGN BT-RCRD-LAST-UPDT-USERID := *USER
        vw_bene_Table_File.insertDBRow();                                                                                                                                 //Natural: STORE BENE-TABLE-FILE
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        bene_Table_File_Bt_Table_Id.setValue(pnd_Table.getValue(4));                                                                                                      //Natural: ASSIGN BT-TABLE-ID := #TABLE ( 4 )
        bene_Table_File_Bt_Table_Key.setValue(pnd_Input_Pnd_Plan_Id);                                                                                                     //Natural: ASSIGN BT-TABLE-KEY := #PLAN-ID
        bene_Table_File_Bt_Table_Text.setValue(pnd_Input_Pnd_Erisa_Ind);                                                                                                  //Natural: ASSIGN BT-TABLE-TEXT := #ERISA-IND
        bene_Table_File_Bt_Rcrd_Last_Updt_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                            //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO BT-RCRD-LAST-UPDT-DTE
        bene_Table_File_Bt_Rcrd_Last_Updt_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("HHMMSST"));                                                             //Natural: MOVE EDITED *TIMX ( EM = HHMMSST ) TO BT-RCRD-LAST-UPDT-TME
        bene_Table_File_Bt_Rcrd_Last_Updt_Userid.setValue(Global.getUSER());                                                                                              //Natural: ASSIGN BT-RCRD-LAST-UPDT-USERID := *USER
        vw_bene_Table_File.insertDBRow();                                                                                                                                 //Natural: STORE BENE-TABLE-FILE
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        bene_Table_File_Bt_Table_Id.setValue(pnd_Table.getValue(5));                                                                                                      //Natural: ASSIGN BT-TABLE-ID := #TABLE ( 5 )
        bene_Table_File_Bt_Table_Key.setValue(pnd_Input_Pnd_Plan_Id);                                                                                                     //Natural: ASSIGN BT-TABLE-KEY := #PLAN-ID
        bene_Table_File_Bt_Table_Text.setValue(pnd_Input_Pnd_Spousal_Waiver);                                                                                             //Natural: ASSIGN BT-TABLE-TEXT := #SPOUSAL-WAIVER
        bene_Table_File_Bt_Rcrd_Last_Updt_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                            //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO BT-RCRD-LAST-UPDT-DTE
        bene_Table_File_Bt_Rcrd_Last_Updt_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("HHMMSST"));                                                             //Natural: MOVE EDITED *TIMX ( EM = HHMMSST ) TO BT-RCRD-LAST-UPDT-TME
        bene_Table_File_Bt_Rcrd_Last_Updt_Userid.setValue(Global.getUSER());                                                                                              //Natural: ASSIGN BT-RCRD-LAST-UPDT-USERID := *USER
        vw_bene_Table_File.insertDBRow();                                                                                                                                 //Natural: STORE BENE-TABLE-FILE
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_Update_Approval_Ind() throws Exception                                                                                                               //Natural: UPDATE-APPROVAL-IND
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************
        GET1:                                                                                                                                                             //Natural: GET BENE-TABLE-FILE *ISN ( READ1. )
        vw_bene_Table_File.readByID(vw_bene_Table_File.getAstISN("READ1"), "GET1");
        bene_Table_File_Bt_Table_Text.setValue(pnd_Input_Pnd_Approval_Ind);                                                                                               //Natural: ASSIGN BT-TABLE-TEXT := #APPROVAL-IND
        bene_Table_File_Bt_Rcrd_Last_Updt_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                            //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO BT-RCRD-LAST-UPDT-DTE
        bene_Table_File_Bt_Rcrd_Last_Updt_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("HHMMSST"));                                                             //Natural: MOVE EDITED *TIMX ( EM = HHMMSST ) TO BT-RCRD-LAST-UPDT-TME
        bene_Table_File_Bt_Rcrd_Last_Updt_Userid.setValue(Global.getUSER());                                                                                              //Natural: ASSIGN BT-RCRD-LAST-UPDT-USERID := *USER
        vw_bene_Table_File.updateDBRow("GET1");                                                                                                                           //Natural: UPDATE ( GET1. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_Update_Delayed_Vested_Or_Inst_Ind() throws Exception                                                                                                 //Natural: UPDATE-DELAYED-VESTED-OR-INST-IND
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************
        GET2:                                                                                                                                                             //Natural: GET BENE-TABLE-FILE *ISN ( READ1. )
        vw_bene_Table_File.readByID(vw_bene_Table_File.getAstISN("READ1"), "GET2");
        bene_Table_File_Bt_Table_Text.setValue(pnd_Input_Pnd_Del_Or_Inst_Ind);                                                                                            //Natural: ASSIGN BT-TABLE-TEXT := #DEL-OR-INST-IND
        bene_Table_File_Bt_Rcrd_Last_Updt_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                            //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO BT-RCRD-LAST-UPDT-DTE
        bene_Table_File_Bt_Rcrd_Last_Updt_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("HHMMSST"));                                                             //Natural: MOVE EDITED *TIMX ( EM = HHMMSST ) TO BT-RCRD-LAST-UPDT-TME
        bene_Table_File_Bt_Rcrd_Last_Updt_Userid.setValue(Global.getUSER());                                                                                              //Natural: ASSIGN BT-RCRD-LAST-UPDT-USERID := *USER
        vw_bene_Table_File.updateDBRow("GET2");                                                                                                                           //Natural: UPDATE ( GET2. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_Update_Third_Party_Ind() throws Exception                                                                                                            //Natural: UPDATE-THIRD-PARTY-IND
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************
        GET3:                                                                                                                                                             //Natural: GET BENE-TABLE-FILE *ISN ( READ1. )
        vw_bene_Table_File.readByID(vw_bene_Table_File.getAstISN("READ1"), "GET3");
        bene_Table_File_Bt_Table_Text.setValue(pnd_Input_Pnd_Third_Party_Ind);                                                                                            //Natural: ASSIGN BT-TABLE-TEXT := #THIRD-PARTY-IND
        bene_Table_File_Bt_Rcrd_Last_Updt_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                            //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO BT-RCRD-LAST-UPDT-DTE
        bene_Table_File_Bt_Rcrd_Last_Updt_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("HHMMSST"));                                                             //Natural: MOVE EDITED *TIMX ( EM = HHMMSST ) TO BT-RCRD-LAST-UPDT-TME
        bene_Table_File_Bt_Rcrd_Last_Updt_Userid.setValue(Global.getUSER());                                                                                              //Natural: ASSIGN BT-RCRD-LAST-UPDT-USERID := *USER
        vw_bene_Table_File.updateDBRow("GET3");                                                                                                                           //Natural: UPDATE ( GET3. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_Update_Erisa_Ind() throws Exception                                                                                                                  //Natural: UPDATE-ERISA-IND
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************
        GET4:                                                                                                                                                             //Natural: GET BENE-TABLE-FILE *ISN ( READ1. )
        vw_bene_Table_File.readByID(vw_bene_Table_File.getAstISN("READ1"), "GET4");
        bene_Table_File_Bt_Table_Text.setValue(pnd_Input_Pnd_Erisa_Ind);                                                                                                  //Natural: ASSIGN BT-TABLE-TEXT := #ERISA-IND
        bene_Table_File_Bt_Rcrd_Last_Updt_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                            //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO BT-RCRD-LAST-UPDT-DTE
        bene_Table_File_Bt_Rcrd_Last_Updt_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("HHMMSST"));                                                             //Natural: MOVE EDITED *TIMX ( EM = HHMMSST ) TO BT-RCRD-LAST-UPDT-TME
        bene_Table_File_Bt_Rcrd_Last_Updt_Userid.setValue(Global.getUSER());                                                                                              //Natural: ASSIGN BT-RCRD-LAST-UPDT-USERID := *USER
        vw_bene_Table_File.updateDBRow("GET4");                                                                                                                           //Natural: UPDATE ( GET4. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_Update_Spousal_Waiver_Ind() throws Exception                                                                                                         //Natural: UPDATE-SPOUSAL-WAIVER-IND
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************
        GET5:                                                                                                                                                             //Natural: GET BENE-TABLE-FILE *ISN ( READ1. )
        vw_bene_Table_File.readByID(vw_bene_Table_File.getAstISN("READ1"), "GET5");
        bene_Table_File_Bt_Table_Text.setValue(pnd_Input_Pnd_Spousal_Waiver);                                                                                             //Natural: ASSIGN BT-TABLE-TEXT := #SPOUSAL-WAIVER
        bene_Table_File_Bt_Rcrd_Last_Updt_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                            //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO BT-RCRD-LAST-UPDT-DTE
        bene_Table_File_Bt_Rcrd_Last_Updt_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("HHMMSST"));                                                             //Natural: MOVE EDITED *TIMX ( EM = HHMMSST ) TO BT-RCRD-LAST-UPDT-TME
        bene_Table_File_Bt_Rcrd_Last_Updt_Userid.setValue(Global.getUSER());                                                                                              //Natural: ASSIGN BT-RCRD-LAST-UPDT-USERID := *USER
        vw_bene_Table_File.updateDBRow("GET5");                                                                                                                           //Natural: UPDATE ( GET5. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=60");

        getReports().setDisplayColumns(1, "DATE",
        		pnd_Inp_System_Date_Pnd_System_Date,"PLAN",
        		pnd_Input_Pnd_Plan_Id,"THIRD PARTY-IND",
        		pnd_Input_Pnd_Third_Party_Ind,"VEST IND",
        		pnd_Input_Pnd_Del_Or_Inst_Ind,"SPOUSAL WAIVER",
        		pnd_Input_Pnd_Spousal_Waiver,"ERISA IND",
        		pnd_Input_Pnd_Erisa_Ind,"APPROVAL IND",
        		pnd_Input_Pnd_Approval_Ind);
    }
}
