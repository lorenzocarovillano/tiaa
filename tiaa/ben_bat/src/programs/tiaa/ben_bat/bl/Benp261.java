/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:14:46 PM
**        * FROM NATURAL PROGRAM : Benp261
************************************************************
**        * FILE NAME            : Benp261.java
**        * CLASS NAME           : Benp261
**        * INSTANCE NAME        : Benp261
************************************************************
************************************************************************
* PROGRAM NAME : BENP261
* DESCRIPTION  : BENEFICIARY BATCH SYSTEM - MONTHLY STATISTIC REPORT
* WRITTEN BY   : K. GREIFF
* DATE WRITTEN : 11/1999
************************************************************************
*    DATE      USERID                   DESCRIPTION
* 11/10/2000  WEBBJ     ACIS CORRECTION. LAST-DSGNTN-SYSTEM = 'X'
* 07/13/2005  O SOTTO   CONVERSION PROJECT.  SC 071405.
* 12/07/2012  ALAGANI   CODED TO GENERATE NEW STATS REPORT.
* 04/01/2017  DURAND    PIN EXPANSION N7 TO N12
* 05/28/2019  DURAND    REMOVE COR FILE READ FOR DEAD & ALIVE PINS
************************************************************************

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benp261 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_bc;
    private DbsField bc_Bc_Pin;
    private DbsField bc_Bc_Tiaa_Cntrct;
    private DbsField bc_Bc_Cref_Cntrct;
    private DbsField bc_Bc_Tiaa_Cref_Ind;
    private DbsField bc_Bc_Stat;
    private DbsField bc_Bc_Cntrct_Type;
    private DbsField bc_Bc_Rqst_Timestamp;
    private DbsField bc_Bc_Eff_Dte;
    private DbsField bc_Bc_Mos_Ind;
    private DbsField bc_Bc_Irvcbl_Ind;
    private DbsField bc_Bc_Pymnt_Chld_Dcsd_Ind;
    private DbsField bc_Bc_Exempt_Spouse_Rights;
    private DbsField bc_Bc_Spouse_Waived_Bnfts;
    private DbsField bc_Bc_Trust_Data_Fldr;
    private DbsField bc_Bc_Bene_Addr_Fldr;
    private DbsField bc_Bc_Co_Owner_Data_Fldr;
    private DbsField bc_Bc_Fldr_Min;
    private DbsField bc_Bc_Fldr_Srce_Id;
    private DbsField bc_Bc_Fldr_Log_Dte_Tme;
    private DbsField bc_Bc_Rcrd_Last_Updt_Dte;
    private DbsField bc_Bc_Rcrd_Last_Updt_Tme;
    private DbsField bc_Bc_Rcrd_Last_Updt_Userid;
    private DbsField bc_Bc_Last_Dsgntn_Srce;
    private DbsField bc_Bc_Last_Dsgntn_System;
    private DbsField bc_Bc_Last_Dsgntn_Userid;
    private DbsField bc_Bc_Last_Dsgntn_Dte;
    private DbsField bc_Bc_Last_Dsgntn_Tme;
    private DbsField bc_Bc_Last_Vrfy_Dte;
    private DbsField bc_Bc_Last_Vrfy_Tme;
    private DbsField bc_Bc_Last_Vrfy_Userid;
    private DbsField bc_Bc_Tiaa_Cref_Chng_Dte;
    private DbsField bc_Bc_Tiaa_Cref_Chng_Tme;
    private DbsField bc_Bc_Chng_Pwr_Atty;
    private DbsField bc_Bc_Mgrtn_Dsgntn_Grp_Nbr;
    private DbsField bc_Bc_Mgrtn_Cnfrm_Stmnt_Prod_Dte;
    private DbsField bc_Bc_Excptn_Unit;

    private DataAccessProgramView vw_bd;
    private DbsField bd_Bd_Pin;
    private DbsField bd_Bd_Tiaa_Cntrct;
    private DbsField bd_Bd_Cref_Cntrct;
    private DbsField bd_Bd_Tiaa_Cref_Ind;
    private DbsField bd_Bd_Stat;
    private DbsField bd_Bd_Seq_Nmbr;
    private DbsField bd_Bd_Rqst_Timestamp;
    private DbsField bd_Bd_Rcrd_Last_Updt_Dte;
    private DbsField bd_Bd_Rcrd_Last_Updt_Tme;
    private DbsField bd_Bd_Rcrd_Last_Updt_Userid;
    private DbsField bd_Bd_Bene_Type;
    private DbsField bd_Bd_Rltn_Cd;

    private DbsGroup bd__R_Field_1;
    private DbsField bd_Bd_Rltn_Cd_N2;
    private DbsField bd_Bd_Bene_Name1;

    private DbsGroup bd__R_Field_2;
    private DbsField bd_Bd_Bene_Name_01_30;
    private DbsField bd_Bd_Bene_Name_31_35;

    private DbsGroup bd__R_Field_3;
    private DbsField bd_Bd_Bene_Name1_20;
    private DbsField bd_Bd_Bene_Name2;
    private DbsField bd_Bd_Rltn_Free_Txt;
    private DbsField bd_Bd_Dte_Birth_Trust;
    private DbsField bd_Bd_Ss_Cd;
    private DbsField bd_Bd_Ss_Nmbr;

    private DbsGroup bd__R_Field_4;
    private DbsField bd_Bd_Ss_Nmbr_N9;
    private DbsField bd_Bd_Perc_Share_Ind;
    private DbsField bd_Bd_Share_Perc;
    private DbsField bd_Bd_Share_Ntor;
    private DbsField bd_Bd_Share_Dtor;
    private DbsField bd_Bd_Irvcbl_Ind;
    private DbsField bd_Bd_Stlmnt_Rstrctn;
    private DbsField bd_Bd_Spcl_Txt1;
    private DbsField bd_Bd_Spcl_Txt2;
    private DbsField bd_Bd_Spcl_Txt3;
    private DbsField bd_Bd_Dflt_Estate;
    private DbsField bd_Bd_Addr1;
    private DbsField bd_Bd_Addr2;
    private DbsField bd_Bd_Addr3_City;
    private DbsField bd_Bd_State;
    private DbsField bd_Bd_Zip;
    private DbsField bd_Bd_Phone;
    private DbsField bd_Bd_Gender;
    private DbsField bd_Bd_Country;

    private DataAccessProgramView vw_bm;
    private DbsField bm_Bm_Pin;
    private DbsField bm_Bm_Tiaa_Cntrct;
    private DbsField bm_Bm_Cref_Cntrct;
    private DbsField bm_Bm_Tiaa_Cref_Ind;
    private DbsField bm_Bm_Stat;
    private DbsField bm_Bm_Rqst_Timestamp;
    private DbsField bm_Bm_Rcrd_Last_Updt_Dte;
    private DbsField bm_Bm_Rcrd_Last_Updt_Tme;
    private DbsField bm_Bm_Rcrd_Last_Updt_Userid;
    private DbsField bm_Count_Castbm_Mos_Txt_Group;

    private DbsGroup bm_Bm_Mos_Txt_Group;
    private DbsField bm_Bm_Mos_Txt;

    private DataAccessProgramView vw_bt;
    private DbsField bt_Bt_Table_Id;
    private DbsField bt_Bt_Table_Key;

    private DbsGroup bt__R_Field_5;
    private DbsField bt_Bt_Table_Key_N2;
    private DbsField bt_Bt_Table_Text;
    private DbsField pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq;

    private DbsGroup pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq__R_Field_6;
    private DbsField pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Stat;
    private DbsField pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Pin;
    private DbsField pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Tiaa_Cntrct;
    private DbsField pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Tiaa_Cref_Ind;
    private DbsField pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Bene_Type;
    private DbsField pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Seq_Nmbr;
    private DbsField pnd_Bm_Stat_Pin_Cntrct_Ind_Key;

    private DbsGroup pnd_Bm_Stat_Pin_Cntrct_Ind_Key__R_Field_7;
    private DbsField pnd_Bm_Stat_Pin_Cntrct_Ind_Key_Pnd_Bm_Stat;
    private DbsField pnd_Bm_Stat_Pin_Cntrct_Ind_Key_Pnd_Bm_Pin;
    private DbsField pnd_Bm_Stat_Pin_Cntrct_Ind_Key_Pnd_Bm_Tiaa_Cntrct;
    private DbsField pnd_Bm_Stat_Pin_Cntrct_Ind_Key_Pnd_Bm_Tiaa_Cref_Ind;
    private DbsField pnd_Bene;
    private DbsField pnd_Default;
    private DbsField pnd_Header;
    private DbsField pnd_I;
    private DbsField pnd_Last_Bene;
    private DbsField pnd_Last_Ins;
    private DbsField pnd_Last_Mit;
    private DbsField pnd_Last_Poa;
    private DbsField pnd_Last_Trust;
    private DbsField pnd_Line;
    private DbsField pnd_Primary;
    private DbsField pnd_Rltn;
    private DbsField pnd_Secondary;
    private DbsField pnd_X;

    private DbsGroup pnd_Grand;
    private DbsField pnd_Grand_Pnd_Total_Pin;
    private DbsField pnd_Grand_Pnd_Total_Dead;
    private DbsField pnd_Grand_Pnd_Total_Alive;
    private DbsField pnd_Grand_Pnd_Total_Mos_Txt;
    private DbsField pnd_Grand_Pnd_Total_Rltn_Code;
    private DbsField pnd_Grand_Pnd_Total_Bd_Bene;

    private DbsGroup pnd_Totals;
    private DbsField pnd_Totals_Pnd_Total_Bc;
    private DbsField pnd_Totals_Pnd_Total_Bc_Tc;
    private DbsField pnd_Totals_Pnd_Total_Bc_T;
    private DbsField pnd_Totals_Pnd_Total_Bc_C;
    private DbsField pnd_Totals_Pnd_Total_No_Eff;
    private DbsField pnd_Totals_Pnd_Total_Mos;
    private DbsField pnd_Totals_Pnd_Total_Reg_Bene;
    private DbsField pnd_Totals_Pnd_Total_Pymnt_A;
    private DbsField pnd_Totals_Pnd_Total_Pymnt_B;
    private DbsField pnd_Totals_Pnd_Total_Pymnt_C;
    private DbsField pnd_Totals_Pnd_Total_Pymnt_D;
    private DbsField pnd_Totals_Pnd_Total_Pymnt_E;
    private DbsField pnd_Totals_Pnd_Total_Pymnt_F;
    private DbsField pnd_Totals_Pnd_Total_Exempt;
    private DbsField pnd_Totals_Pnd_Total_Waive;
    private DbsField pnd_Totals_Pnd_Total_Fldr_T;
    private DbsField pnd_Totals_Pnd_Total_Fldr_B;
    private DbsField pnd_Totals_Pnd_Total_Fldr_I;
    private DbsField pnd_Totals_Pnd_Total_Fldr_Mit;
    private DbsField pnd_Totals_Pnd_Total_Poa;
    private DbsField pnd_Totals_Pnd_Total_Bm_Irvcbl;
    private DbsField pnd_Totals_Pnd_Total_Bm;
    private DbsField pnd_Totals_Pnd_Total_Bm_Tc;
    private DbsField pnd_Totals_Pnd_Total_Bm_T;
    private DbsField pnd_Totals_Pnd_Total_Bm_C;
    private DbsField pnd_Totals_Pnd_Total_Bd;
    private DbsField pnd_Totals_Pnd_Total_Bd_Tc;
    private DbsField pnd_Totals_Pnd_Total_Bd_T;
    private DbsField pnd_Totals_Pnd_Total_Bd_C;
    private DbsField pnd_Totals_Pnd_Total_Prmry_Only;
    private DbsField pnd_Totals_Pnd_Total_Dlft;
    private DbsField pnd_Totals_Pnd_Total_Prmry_Scndry;
    private DbsField pnd_Totals_Pnd_Total_Primary;
    private DbsField pnd_Totals_Pnd_Total_Secondary;
    private DbsField pnd_Totals_Pnd_Total_Name1;
    private DbsField pnd_Totals_Pnd_Total_Name2;
    private DbsField pnd_Totals_Pnd_Total_Name1_2;
    private DbsField pnd_Totals_Pnd_Total_No_Name1;
    private DbsField pnd_Totals_Pnd_Total_No_Name2;
    private DbsField pnd_Totals_Pnd_Total_No_Rltn;
    private DbsField pnd_Totals_Pnd_Total_Rltn;
    private DbsField pnd_Totals_Pnd_Total_Trust_Dot;
    private DbsField pnd_Totals_Pnd_Total_Trust_Tin;
    private DbsField pnd_Totals_Pnd_Total_Rltn_Txt;
    private DbsField pnd_Totals_Pnd_Total_Dob;
    private DbsField pnd_Totals_Pnd_Total_No_Dob;
    private DbsField pnd_Totals_Pnd_Total_Ss_S;
    private DbsField pnd_Totals_Pnd_Total_Ss_I;
    private DbsField pnd_Totals_Pnd_Total_Ss_T;
    private DbsField pnd_Totals_Pnd_Total_No_Ss_Cd;
    private DbsField pnd_Totals_Pnd_Total_No_Ss_Nmbr;
    private DbsField pnd_Totals_Pnd_Total_P;
    private DbsField pnd_Totals_Pnd_Total_100;
    private DbsField pnd_Totals_Pnd_Total_S;
    private DbsField pnd_Totals_Pnd_Total_Eq;
    private DbsField pnd_Totals_Pnd_Total_Bd_Irvcbl;
    private DbsField pnd_Totals_Pnd_Total_Rstct_L;
    private DbsField pnd_Totals_Pnd_Total_Rstct_N;
    private DbsField pnd_Totals_Pnd_Total_Rstct_R;
    private DbsField pnd_Totals_Pnd_Total_Rstct_X;
    private DbsField pnd_Totals_Pnd_Total_Addr1;
    private DbsField pnd_Totals_Pnd_Total_Addr2;
    private DbsField pnd_Totals_Pnd_Total_Addr3;
    private DbsField pnd_Totals_Pnd_Total_State;
    private DbsField pnd_Totals_Pnd_Total_Zip;
    private DbsField pnd_Totals_Pnd_Total_Country;
    private DbsField pnd_Totals_Pnd_Total_Gender;
    private DbsField pnd_Totals_Pnd_Total_Phone;
    private DbsField pnd_Totals_Pnd_Total_Spcl1;
    private DbsField pnd_Totals_Pnd_Total_Spcl2;
    private DbsField pnd_Totals_Pnd_Total_Spcl3;
    private DbsField pnd_Totals_Pnd_Total_Nm_Dob_Ssn;
    private DbsField pnd_Totals_Pnd_Total_Nm_Dob;
    private DbsField pnd_Totals_Pnd_Total_Nm_Ssn;
    private DbsField pnd_Totals_Pnd_Total_Nm;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_bc = new DataAccessProgramView(new NameInfo("vw_bc", "BC"), "BENE_CONTRACT_12", "BENE_CONTRACT");
        bc_Bc_Pin = vw_bc.getRecord().newFieldInGroup("bc_Bc_Pin", "BC-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BC_PIN");
        bc_Bc_Pin.setDdmHeader("PIN");
        bc_Bc_Tiaa_Cntrct = vw_bc.getRecord().newFieldInGroup("bc_Bc_Tiaa_Cntrct", "BC-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BC_TIAA_CNTRCT");
        bc_Bc_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bc_Bc_Cref_Cntrct = vw_bc.getRecord().newFieldInGroup("bc_Bc_Cref_Cntrct", "BC-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BC_CREF_CNTRCT");
        bc_Bc_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bc_Bc_Tiaa_Cref_Ind = vw_bc.getRecord().newFieldInGroup("bc_Bc_Tiaa_Cref_Ind", "BC-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_TIAA_CREF_IND");
        bc_Bc_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bc_Bc_Stat = vw_bc.getRecord().newFieldInGroup("bc_Bc_Stat", "BC-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_STAT");
        bc_Bc_Stat.setDdmHeader("STATUS");
        bc_Bc_Cntrct_Type = vw_bc.getRecord().newFieldInGroup("bc_Bc_Cntrct_Type", "BC-CNTRCT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_CNTRCT_TYPE");
        bc_Bc_Cntrct_Type.setDdmHeader("CNTRCT/TYPE");
        bc_Bc_Rqst_Timestamp = vw_bc.getRecord().newFieldInGroup("bc_Bc_Rqst_Timestamp", "BC-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BC_RQST_TIMESTAMP");
        bc_Bc_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        bc_Bc_Eff_Dte = vw_bc.getRecord().newFieldInGroup("bc_Bc_Eff_Dte", "BC-EFF-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "BC_EFF_DTE");
        bc_Bc_Eff_Dte.setDdmHeader("EFFECTIVE/DATE");
        bc_Bc_Mos_Ind = vw_bc.getRecord().newFieldInGroup("bc_Bc_Mos_Ind", "BC-MOS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_MOS_IND");
        bc_Bc_Mos_Ind.setDdmHeader("MOS/IND");
        bc_Bc_Irvcbl_Ind = vw_bc.getRecord().newFieldInGroup("bc_Bc_Irvcbl_Ind", "BC-IRVCBL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_IRVCBL_IND");
        bc_Bc_Irvcbl_Ind.setDdmHeader("IRVCBL/IND");
        bc_Bc_Pymnt_Chld_Dcsd_Ind = vw_bc.getRecord().newFieldInGroup("bc_Bc_Pymnt_Chld_Dcsd_Ind", "BC-PYMNT-CHLD-DCSD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_PYMNT_CHLD_DCSD_IND");
        bc_Bc_Pymnt_Chld_Dcsd_Ind.setDdmHeader("PYMNT/CHILD/DCSD");
        bc_Bc_Exempt_Spouse_Rights = vw_bc.getRecord().newFieldInGroup("bc_Bc_Exempt_Spouse_Rights", "BC-EXEMPT-SPOUSE-RIGHTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_EXEMPT_SPOUSE_RIGHTS");
        bc_Bc_Exempt_Spouse_Rights.setDdmHeader("EXEMPT/SPOUSAL/RIGHTS");
        bc_Bc_Spouse_Waived_Bnfts = vw_bc.getRecord().newFieldInGroup("bc_Bc_Spouse_Waived_Bnfts", "BC-SPOUSE-WAIVED-BNFTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_SPOUSE_WAIVED_BNFTS");
        bc_Bc_Spouse_Waived_Bnfts.setDdmHeader("SPOUSE/WAIVED/BENEFITS");
        bc_Bc_Trust_Data_Fldr = vw_bc.getRecord().newFieldInGroup("bc_Bc_Trust_Data_Fldr", "BC-TRUST-DATA-FLDR", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_TRUST_DATA_FLDR");
        bc_Bc_Trust_Data_Fldr.setDdmHeader("TRUST/DATA/IN FLDR");
        bc_Bc_Bene_Addr_Fldr = vw_bc.getRecord().newFieldInGroup("bc_Bc_Bene_Addr_Fldr", "BC-BENE-ADDR-FLDR", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_BENE_ADDR_FLDR");
        bc_Bc_Bene_Addr_Fldr.setDdmHeader("BENE/ADDRESS/IN FLDR");
        bc_Bc_Co_Owner_Data_Fldr = vw_bc.getRecord().newFieldInGroup("bc_Bc_Co_Owner_Data_Fldr", "BC-CO-OWNER-DATA-FLDR", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_CO_OWNER_DATA_FLDR");
        bc_Bc_Co_Owner_Data_Fldr.setDdmHeader("CO-OWNER/DATA FLDR");
        bc_Bc_Fldr_Min = vw_bc.getRecord().newFieldInGroup("bc_Bc_Fldr_Min", "BC-FLDR-MIN", FieldType.STRING, 11, RepeatingFieldStrategy.None, "BC_FLDR_MIN");
        bc_Bc_Fldr_Min.setDdmHeader("FOLDER/MIN");
        bc_Bc_Fldr_Srce_Id = vw_bc.getRecord().newFieldInGroup("bc_Bc_Fldr_Srce_Id", "BC-FLDR-SRCE-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "BC_FLDR_SRCE_ID");
        bc_Bc_Fldr_Srce_Id.setDdmHeader("FOLDER/SOURCE/ID");
        bc_Bc_Fldr_Log_Dte_Tme = vw_bc.getRecord().newFieldInGroup("bc_Bc_Fldr_Log_Dte_Tme", "BC-FLDR-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BC_FLDR_LOG_DTE_TME");
        bc_Bc_Fldr_Log_Dte_Tme.setDdmHeader("FOLDER/LOG DATE/AND TIME");
        bc_Bc_Rcrd_Last_Updt_Dte = vw_bc.getRecord().newFieldInGroup("bc_Bc_Rcrd_Last_Updt_Dte", "BC-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_RCRD_LAST_UPDT_DTE");
        bc_Bc_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bc_Bc_Rcrd_Last_Updt_Tme = vw_bc.getRecord().newFieldInGroup("bc_Bc_Rcrd_Last_Updt_Tme", "BC-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_RCRD_LAST_UPDT_TME");
        bc_Bc_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bc_Bc_Rcrd_Last_Updt_Userid = vw_bc.getRecord().newFieldInGroup("bc_Bc_Rcrd_Last_Updt_Userid", "BC-RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "BC_RCRD_LAST_UPDT_USERID");
        bc_Bc_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        bc_Bc_Last_Dsgntn_Srce = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Dsgntn_Srce", "BC-LAST-DSGNTN-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_SRCE");
        bc_Bc_Last_Dsgntn_Srce.setDdmHeader("LAST/DSGNTN/SOURCE");
        bc_Bc_Last_Dsgntn_System = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Dsgntn_System", "BC-LAST-DSGNTN-SYSTEM", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_SYSTEM");
        bc_Bc_Last_Dsgntn_System.setDdmHeader("LAST/DSGNTN/SYSTEM");
        bc_Bc_Last_Dsgntn_Userid = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Dsgntn_Userid", "BC-LAST-DSGNTN-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_USERID");
        bc_Bc_Last_Dsgntn_Userid.setDdmHeader("LAST/DSGNTN/USERID");
        bc_Bc_Last_Dsgntn_Dte = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Dsgntn_Dte", "BC-LAST-DSGNTN-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_DTE");
        bc_Bc_Last_Dsgntn_Dte.setDdmHeader("LAST/DSGNTN/DATE");
        bc_Bc_Last_Dsgntn_Tme = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Dsgntn_Tme", "BC-LAST-DSGNTN-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_TME");
        bc_Bc_Last_Dsgntn_Tme.setDdmHeader("LAST/DSGNTN/TIME");
        bc_Bc_Last_Vrfy_Dte = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Vrfy_Dte", "BC-LAST-VRFY-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_VRFY_DTE");
        bc_Bc_Last_Vrfy_Dte.setDdmHeader("LAST/VRFY/DATE");
        bc_Bc_Last_Vrfy_Tme = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Vrfy_Tme", "BC-LAST-VRFY-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_LAST_VRFY_TME");
        bc_Bc_Last_Vrfy_Tme.setDdmHeader("LAST/VRFY/TIME");
        bc_Bc_Last_Vrfy_Userid = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Vrfy_Userid", "BC-LAST-VRFY-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_VRFY_USERID");
        bc_Bc_Last_Vrfy_Userid.setDdmHeader("LAST/VRFY/USERID");
        bc_Bc_Tiaa_Cref_Chng_Dte = vw_bc.getRecord().newFieldInGroup("bc_Bc_Tiaa_Cref_Chng_Dte", "BC-TIAA-CREF-CHNG-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_TIAA_CREF_CHNG_DTE");
        bc_Bc_Tiaa_Cref_Chng_Dte.setDdmHeader("TIAA CREF/CHANGE/DATE");
        bc_Bc_Tiaa_Cref_Chng_Tme = vw_bc.getRecord().newFieldInGroup("bc_Bc_Tiaa_Cref_Chng_Tme", "BC-TIAA-CREF-CHNG-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_TIAA_CREF_CHNG_TME");
        bc_Bc_Tiaa_Cref_Chng_Tme.setDdmHeader("TIAA CREF/CHANGE/TIME");
        bc_Bc_Chng_Pwr_Atty = vw_bc.getRecord().newFieldInGroup("bc_Bc_Chng_Pwr_Atty", "BC-CHNG-PWR-ATTY", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_CHNG_PWR_ATTY");
        bc_Bc_Chng_Pwr_Atty.setDdmHeader("CHANGE/POWER/ATTNY");
        bc_Bc_Mgrtn_Dsgntn_Grp_Nbr = vw_bc.getRecord().newFieldInGroup("bc_Bc_Mgrtn_Dsgntn_Grp_Nbr", "BC-MGRTN-DSGNTN-GRP-NBR", FieldType.NUMERIC, 3, 
            RepeatingFieldStrategy.None, "BC_MGRTN_DSGNTN_GRP_NBR");
        bc_Bc_Mgrtn_Dsgntn_Grp_Nbr.setDdmHeader("MGRTN/DSGNTN/GRP NBR");
        bc_Bc_Mgrtn_Cnfrm_Stmnt_Prod_Dte = vw_bc.getRecord().newFieldInGroup("bc_Bc_Mgrtn_Cnfrm_Stmnt_Prod_Dte", "BC-MGRTN-CNFRM-STMNT-PROD-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "BC_MGRTN_CNFRM_STMNT_PROD_DTE");
        bc_Bc_Mgrtn_Cnfrm_Stmnt_Prod_Dte.setDdmHeader("MGRTN CONFIRM/STATEMENT/PROD DATE");
        bc_Bc_Excptn_Unit = vw_bc.getRecord().newFieldInGroup("bc_Bc_Excptn_Unit", "BC-EXCPTN-UNIT", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_EXCPTN_UNIT");
        bc_Bc_Excptn_Unit.setDdmHeader("MGRTN/EXCPTN/UNIT");
        registerRecord(vw_bc);

        vw_bd = new DataAccessProgramView(new NameInfo("vw_bd", "BD"), "BENE_DESIGNATION_12", "BENE_DESIGNATION");
        bd_Bd_Pin = vw_bd.getRecord().newFieldInGroup("bd_Bd_Pin", "BD-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BD_PIN");
        bd_Bd_Pin.setDdmHeader("PIN");
        bd_Bd_Tiaa_Cntrct = vw_bd.getRecord().newFieldInGroup("bd_Bd_Tiaa_Cntrct", "BD-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BD_TIAA_CNTRCT");
        bd_Bd_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bd_Bd_Cref_Cntrct = vw_bd.getRecord().newFieldInGroup("bd_Bd_Cref_Cntrct", "BD-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BD_CREF_CNTRCT");
        bd_Bd_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bd_Bd_Tiaa_Cref_Ind = vw_bd.getRecord().newFieldInGroup("bd_Bd_Tiaa_Cref_Ind", "BD-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_TIAA_CREF_IND");
        bd_Bd_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bd_Bd_Stat = vw_bd.getRecord().newFieldInGroup("bd_Bd_Stat", "BD-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_STAT");
        bd_Bd_Stat.setDdmHeader("STAT");
        bd_Bd_Seq_Nmbr = vw_bd.getRecord().newFieldInGroup("bd_Bd_Seq_Nmbr", "BD-SEQ-NMBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "BD_SEQ_NMBR");
        bd_Bd_Seq_Nmbr.setDdmHeader(" SEQ/NMBR");
        bd_Bd_Rqst_Timestamp = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rqst_Timestamp", "BD-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BD_RQST_TIMESTAMP");
        bd_Bd_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        bd_Bd_Rcrd_Last_Updt_Dte = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rcrd_Last_Updt_Dte", "BD-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BD_RCRD_LAST_UPDT_DTE");
        bd_Bd_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bd_Bd_Rcrd_Last_Updt_Tme = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rcrd_Last_Updt_Tme", "BD-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BD_RCRD_LAST_UPDT_TME");
        bd_Bd_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bd_Bd_Rcrd_Last_Updt_Userid = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rcrd_Last_Updt_Userid", "BD-RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "BD_RCRD_LAST_UPDT_USERID");
        bd_Bd_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        bd_Bd_Bene_Type = vw_bd.getRecord().newFieldInGroup("bd_Bd_Bene_Type", "BD-BENE-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_BENE_TYPE");
        bd_Bd_Bene_Type.setDdmHeader("BENE/TYPE");
        bd_Bd_Rltn_Cd = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rltn_Cd", "BD-RLTN-CD", FieldType.STRING, 2, RepeatingFieldStrategy.None, "BD_RLTN_CD");
        bd_Bd_Rltn_Cd.setDdmHeader("RELATION/CODE");

        bd__R_Field_1 = vw_bd.getRecord().newGroupInGroup("bd__R_Field_1", "REDEFINE", bd_Bd_Rltn_Cd);
        bd_Bd_Rltn_Cd_N2 = bd__R_Field_1.newFieldInGroup("bd_Bd_Rltn_Cd_N2", "BD-RLTN-CD-N2", FieldType.NUMERIC, 2);
        bd_Bd_Bene_Name1 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Bene_Name1", "BD-BENE-NAME1", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BD_BENE_NAME1");
        bd_Bd_Bene_Name1.setDdmHeader("BENEFICIARY/NAME 1");

        bd__R_Field_2 = vw_bd.getRecord().newGroupInGroup("bd__R_Field_2", "REDEFINE", bd_Bd_Bene_Name1);
        bd_Bd_Bene_Name_01_30 = bd__R_Field_2.newFieldInGroup("bd_Bd_Bene_Name_01_30", "BD-BENE-NAME-01-30", FieldType.STRING, 30);
        bd_Bd_Bene_Name_31_35 = bd__R_Field_2.newFieldInGroup("bd_Bd_Bene_Name_31_35", "BD-BENE-NAME-31-35", FieldType.STRING, 5);

        bd__R_Field_3 = vw_bd.getRecord().newGroupInGroup("bd__R_Field_3", "REDEFINE", bd_Bd_Bene_Name1);
        bd_Bd_Bene_Name1_20 = bd__R_Field_3.newFieldInGroup("bd_Bd_Bene_Name1_20", "BD-BENE-NAME1-20", FieldType.STRING, 20);
        bd_Bd_Bene_Name2 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Bene_Name2", "BD-BENE-NAME2", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BD_BENE_NAME2");
        bd_Bd_Bene_Name2.setDdmHeader("BENEFICIARY/NAME 2");
        bd_Bd_Rltn_Free_Txt = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rltn_Free_Txt", "BD-RLTN-FREE-TXT", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BD_RLTN_FREE_TXT");
        bd_Bd_Rltn_Free_Txt.setDdmHeader("OTHER/RELATION");
        bd_Bd_Dte_Birth_Trust = vw_bd.getRecord().newFieldInGroup("bd_Bd_Dte_Birth_Trust", "BD-DTE-BIRTH-TRUST", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BD_DTE_BIRTH_TRUST");
        bd_Bd_Dte_Birth_Trust.setDdmHeader("DOB/DOT");
        bd_Bd_Ss_Cd = vw_bd.getRecord().newFieldInGroup("bd_Bd_Ss_Cd", "BD-SS-CD", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_SS_CD");
        bd_Bd_Ss_Cd.setDdmHeader("SS/CODE");
        bd_Bd_Ss_Nmbr = vw_bd.getRecord().newFieldInGroup("bd_Bd_Ss_Nmbr", "BD-SS-NMBR", FieldType.STRING, 9, RepeatingFieldStrategy.None, "BD_SS_NMBR");
        bd_Bd_Ss_Nmbr.setDdmHeader("SS/NUMBER");

        bd__R_Field_4 = vw_bd.getRecord().newGroupInGroup("bd__R_Field_4", "REDEFINE", bd_Bd_Ss_Nmbr);
        bd_Bd_Ss_Nmbr_N9 = bd__R_Field_4.newFieldInGroup("bd_Bd_Ss_Nmbr_N9", "BD-SS-NMBR-N9", FieldType.NUMERIC, 9);
        bd_Bd_Perc_Share_Ind = vw_bd.getRecord().newFieldInGroup("bd_Bd_Perc_Share_Ind", "BD-PERC-SHARE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_PERC_SHARE_IND");
        bd_Bd_Perc_Share_Ind.setDdmHeader("PERCNT/SHARE/IND");
        bd_Bd_Share_Perc = vw_bd.getRecord().newFieldInGroup("bd_Bd_Share_Perc", "BD-SHARE-PERC", FieldType.NUMERIC, 5, 2, RepeatingFieldStrategy.None, 
            "BD_SHARE_PERC");
        bd_Bd_Share_Perc.setDdmHeader("SHARE/PERCENT");
        bd_Bd_Share_Ntor = vw_bd.getRecord().newFieldInGroup("bd_Bd_Share_Ntor", "BD-SHARE-NTOR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "BD_SHARE_NTOR");
        bd_Bd_Share_Ntor.setDdmHeader("SHARE/NUMERATOR");
        bd_Bd_Share_Dtor = vw_bd.getRecord().newFieldInGroup("bd_Bd_Share_Dtor", "BD-SHARE-DTOR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "BD_SHARE_DTOR");
        bd_Bd_Share_Dtor.setDdmHeader("SHARE/DENOMINATOR");
        bd_Bd_Irvcbl_Ind = vw_bd.getRecord().newFieldInGroup("bd_Bd_Irvcbl_Ind", "BD-IRVCBL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_IRVCBL_IND");
        bd_Bd_Irvcbl_Ind.setDdmHeader("IRVCBL/IND");
        bd_Bd_Stlmnt_Rstrctn = vw_bd.getRecord().newFieldInGroup("bd_Bd_Stlmnt_Rstrctn", "BD-STLMNT-RSTRCTN", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_STLMNT_RSTRCTN");
        bd_Bd_Stlmnt_Rstrctn.setDdmHeader("SETTLEMENT/RESTRICTION");
        bd_Bd_Spcl_Txt1 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Spcl_Txt1", "BD-SPCL-TXT1", FieldType.STRING, 72, RepeatingFieldStrategy.None, "BD_SPCL_TXT1");
        bd_Bd_Spcl_Txt1.setDdmHeader("SPECIAL TEXT 1");
        bd_Bd_Spcl_Txt2 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Spcl_Txt2", "BD-SPCL-TXT2", FieldType.STRING, 72, RepeatingFieldStrategy.None, "BD_SPCL_TXT2");
        bd_Bd_Spcl_Txt2.setDdmHeader("SPECIAL TEXT 2");
        bd_Bd_Spcl_Txt3 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Spcl_Txt3", "BD-SPCL-TXT3", FieldType.STRING, 72, RepeatingFieldStrategy.None, "BD_SPCL_TXT3");
        bd_Bd_Spcl_Txt3.setDdmHeader("SPECIAL TEXT 3");
        bd_Bd_Dflt_Estate = vw_bd.getRecord().newFieldInGroup("bd_Bd_Dflt_Estate", "BD-DFLT-ESTATE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_DFLT_ESTATE");
        bd_Bd_Dflt_Estate.setDdmHeader("DEFAULT/ESTATE");
        bd_Bd_Addr1 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Addr1", "BD-ADDR1", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BD_ADDR1");
        bd_Bd_Addr2 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Addr2", "BD-ADDR2", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BD_ADDR2");
        bd_Bd_Addr3_City = vw_bd.getRecord().newFieldInGroup("bd_Bd_Addr3_City", "BD-ADDR3-CITY", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BD_ADDR3_CITY");
        bd_Bd_State = vw_bd.getRecord().newFieldInGroup("bd_Bd_State", "BD-STATE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "BD_STATE");
        bd_Bd_Zip = vw_bd.getRecord().newFieldInGroup("bd_Bd_Zip", "BD-ZIP", FieldType.STRING, 10, RepeatingFieldStrategy.None, "BD_ZIP");
        bd_Bd_Phone = vw_bd.getRecord().newFieldInGroup("bd_Bd_Phone", "BD-PHONE", FieldType.STRING, 20, RepeatingFieldStrategy.None, "BD_PHONE");
        bd_Bd_Gender = vw_bd.getRecord().newFieldInGroup("bd_Bd_Gender", "BD-GENDER", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_GENDER");
        bd_Bd_Country = vw_bd.getRecord().newFieldInGroup("bd_Bd_Country", "BD-COUNTRY", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BD_COUNTRY");
        registerRecord(vw_bd);

        vw_bm = new DataAccessProgramView(new NameInfo("vw_bm", "BM"), "BENE_MOS_12", "BENE_MOS", DdmPeriodicGroups.getInstance().getGroups("BENE_MOS_12"));
        bm_Bm_Pin = vw_bm.getRecord().newFieldInGroup("bm_Bm_Pin", "BM-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BM_PIN");
        bm_Bm_Pin.setDdmHeader("PIN");
        bm_Bm_Tiaa_Cntrct = vw_bm.getRecord().newFieldInGroup("bm_Bm_Tiaa_Cntrct", "BM-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BM_TIAA_CNTRCT");
        bm_Bm_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bm_Bm_Cref_Cntrct = vw_bm.getRecord().newFieldInGroup("bm_Bm_Cref_Cntrct", "BM-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BM_CREF_CNTRCT");
        bm_Bm_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bm_Bm_Tiaa_Cref_Ind = vw_bm.getRecord().newFieldInGroup("bm_Bm_Tiaa_Cref_Ind", "BM-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BM_TIAA_CREF_IND");
        bm_Bm_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bm_Bm_Stat = vw_bm.getRecord().newFieldInGroup("bm_Bm_Stat", "BM-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BM_STAT");
        bm_Bm_Stat.setDdmHeader("STAT");
        bm_Bm_Rqst_Timestamp = vw_bm.getRecord().newFieldInGroup("bm_Bm_Rqst_Timestamp", "BM-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BM_RQST_TIMESTAMP");
        bm_Bm_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        bm_Bm_Rcrd_Last_Updt_Dte = vw_bm.getRecord().newFieldInGroup("bm_Bm_Rcrd_Last_Updt_Dte", "BM-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BM_RCRD_LAST_UPDT_DTE");
        bm_Bm_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bm_Bm_Rcrd_Last_Updt_Tme = vw_bm.getRecord().newFieldInGroup("bm_Bm_Rcrd_Last_Updt_Tme", "BM-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BM_RCRD_LAST_UPDT_TME");
        bm_Bm_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bm_Bm_Rcrd_Last_Updt_Userid = vw_bm.getRecord().newFieldInGroup("bm_Bm_Rcrd_Last_Updt_Userid", "BM-RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "BM_RCRD_LAST_UPDT_USERID");
        bm_Bm_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        bm_Count_Castbm_Mos_Txt_Group = vw_bm.getRecord().newFieldInGroup("bm_Count_Castbm_Mos_Txt_Group", "C*BM-MOS-TXT-GROUP", RepeatingFieldStrategy.CAsteriskVariable, 
            "BENE_MOS_BM_MOS_TXT_GROUP");

        bm_Bm_Mos_Txt_Group = vw_bm.getRecord().newGroupInGroup("bm_Bm_Mos_Txt_Group", "BM-MOS-TXT-GROUP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "BENE_MOS_BM_MOS_TXT_GROUP");
        bm_Bm_Mos_Txt = bm_Bm_Mos_Txt_Group.newFieldArrayInGroup("bm_Bm_Mos_Txt", "BM-MOS-TXT", FieldType.STRING, 72, new DbsArrayController(1, 60) , 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "BM_MOS_TXT", "BENE_MOS_BM_MOS_TXT_GROUP");
        bm_Bm_Mos_Txt.setDdmHeader("MOS TEXT");
        registerRecord(vw_bm);

        vw_bt = new DataAccessProgramView(new NameInfo("vw_bt", "BT"), "BENE_TABLE_FILE", "BENE_TABLE");
        bt_Bt_Table_Id = vw_bt.getRecord().newFieldInGroup("bt_Bt_Table_Id", "BT-TABLE-ID", FieldType.STRING, 2, RepeatingFieldStrategy.None, "BT_TABLE_ID");
        bt_Bt_Table_Id.setDdmHeader("TABLE/ID");
        bt_Bt_Table_Key = vw_bt.getRecord().newFieldInGroup("bt_Bt_Table_Key", "BT-TABLE-KEY", FieldType.STRING, 20, RepeatingFieldStrategy.None, "BT_TABLE_KEY");
        bt_Bt_Table_Key.setDdmHeader("TABLE/KEY");

        bt__R_Field_5 = vw_bt.getRecord().newGroupInGroup("bt__R_Field_5", "REDEFINE", bt_Bt_Table_Key);
        bt_Bt_Table_Key_N2 = bt__R_Field_5.newFieldInGroup("bt_Bt_Table_Key_N2", "BT-TABLE-KEY-N2", FieldType.NUMERIC, 2);
        bt_Bt_Table_Text = vw_bt.getRecord().newFieldInGroup("bt_Bt_Table_Text", "BT-TABLE-TEXT", FieldType.STRING, 72, RepeatingFieldStrategy.None, "BT_TABLE_TEXT");
        bt_Bt_Table_Text.setDdmHeader("TABLE/TEXT");
        registerRecord(vw_bt);

        pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq = localVariables.newFieldInRecord("pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq", "#BD-STAT-PIN-CNTRCT-IND-TYPE-SEQ", 
            FieldType.STRING, 28);

        pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq__R_Field_6 = localVariables.newGroupInRecord("pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq__R_Field_6", "REDEFINE", 
            pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq);
        pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Stat = pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq__R_Field_6.newFieldInGroup("pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Stat", 
            "#BD-STAT", FieldType.STRING, 1);
        pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Pin = pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq__R_Field_6.newFieldInGroup("pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Pin", 
            "#BD-PIN", FieldType.NUMERIC, 12);
        pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Tiaa_Cntrct = pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq__R_Field_6.newFieldInGroup("pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Tiaa_Cntrct", 
            "#BD-TIAA-CNTRCT", FieldType.STRING, 10);
        pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Tiaa_Cref_Ind = pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq__R_Field_6.newFieldInGroup("pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Tiaa_Cref_Ind", 
            "#BD-TIAA-CREF-IND", FieldType.STRING, 1);
        pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Bene_Type = pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq__R_Field_6.newFieldInGroup("pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Bene_Type", 
            "#BD-BENE-TYPE", FieldType.STRING, 1);
        pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Seq_Nmbr = pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq__R_Field_6.newFieldInGroup("pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Seq_Nmbr", 
            "#BD-SEQ-NMBR", FieldType.NUMERIC, 3);
        pnd_Bm_Stat_Pin_Cntrct_Ind_Key = localVariables.newFieldInRecord("pnd_Bm_Stat_Pin_Cntrct_Ind_Key", "#BM-STAT-PIN-CNTRCT-IND-KEY", FieldType.STRING, 
            24);

        pnd_Bm_Stat_Pin_Cntrct_Ind_Key__R_Field_7 = localVariables.newGroupInRecord("pnd_Bm_Stat_Pin_Cntrct_Ind_Key__R_Field_7", "REDEFINE", pnd_Bm_Stat_Pin_Cntrct_Ind_Key);
        pnd_Bm_Stat_Pin_Cntrct_Ind_Key_Pnd_Bm_Stat = pnd_Bm_Stat_Pin_Cntrct_Ind_Key__R_Field_7.newFieldInGroup("pnd_Bm_Stat_Pin_Cntrct_Ind_Key_Pnd_Bm_Stat", 
            "#BM-STAT", FieldType.STRING, 1);
        pnd_Bm_Stat_Pin_Cntrct_Ind_Key_Pnd_Bm_Pin = pnd_Bm_Stat_Pin_Cntrct_Ind_Key__R_Field_7.newFieldInGroup("pnd_Bm_Stat_Pin_Cntrct_Ind_Key_Pnd_Bm_Pin", 
            "#BM-PIN", FieldType.NUMERIC, 12);
        pnd_Bm_Stat_Pin_Cntrct_Ind_Key_Pnd_Bm_Tiaa_Cntrct = pnd_Bm_Stat_Pin_Cntrct_Ind_Key__R_Field_7.newFieldInGroup("pnd_Bm_Stat_Pin_Cntrct_Ind_Key_Pnd_Bm_Tiaa_Cntrct", 
            "#BM-TIAA-CNTRCT", FieldType.STRING, 10);
        pnd_Bm_Stat_Pin_Cntrct_Ind_Key_Pnd_Bm_Tiaa_Cref_Ind = pnd_Bm_Stat_Pin_Cntrct_Ind_Key__R_Field_7.newFieldInGroup("pnd_Bm_Stat_Pin_Cntrct_Ind_Key_Pnd_Bm_Tiaa_Cref_Ind", 
            "#BM-TIAA-CREF-IND", FieldType.STRING, 1);
        pnd_Bene = localVariables.newFieldInRecord("pnd_Bene", "#BENE", FieldType.PACKED_DECIMAL, 2);
        pnd_Default = localVariables.newFieldInRecord("pnd_Default", "#DEFAULT", FieldType.BOOLEAN, 1);
        pnd_Header = localVariables.newFieldInRecord("pnd_Header", "#HEADER", FieldType.STRING, 30);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Last_Bene = localVariables.newFieldInRecord("pnd_Last_Bene", "#LAST-BENE", FieldType.STRING, 8);
        pnd_Last_Ins = localVariables.newFieldInRecord("pnd_Last_Ins", "#LAST-INS", FieldType.STRING, 8);
        pnd_Last_Mit = localVariables.newFieldInRecord("pnd_Last_Mit", "#LAST-MIT", FieldType.STRING, 8);
        pnd_Last_Poa = localVariables.newFieldInRecord("pnd_Last_Poa", "#LAST-POA", FieldType.STRING, 8);
        pnd_Last_Trust = localVariables.newFieldInRecord("pnd_Last_Trust", "#LAST-TRUST", FieldType.STRING, 8);
        pnd_Line = localVariables.newFieldInRecord("pnd_Line", "#LINE", FieldType.STRING, 30);
        pnd_Primary = localVariables.newFieldInRecord("pnd_Primary", "#PRIMARY", FieldType.BOOLEAN, 1);
        pnd_Rltn = localVariables.newFieldArrayInRecord("pnd_Rltn", "#RLTN", FieldType.STRING, 15, new DbsArrayController(1, 99));
        pnd_Secondary = localVariables.newFieldInRecord("pnd_Secondary", "#SECONDARY", FieldType.BOOLEAN, 1);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.PACKED_DECIMAL, 3);

        pnd_Grand = localVariables.newGroupInRecord("pnd_Grand", "#GRAND");
        pnd_Grand_Pnd_Total_Pin = pnd_Grand.newFieldInGroup("pnd_Grand_Pnd_Total_Pin", "#TOTAL-PIN", FieldType.PACKED_DECIMAL, 10);
        pnd_Grand_Pnd_Total_Dead = pnd_Grand.newFieldInGroup("pnd_Grand_Pnd_Total_Dead", "#TOTAL-DEAD", FieldType.PACKED_DECIMAL, 10);
        pnd_Grand_Pnd_Total_Alive = pnd_Grand.newFieldInGroup("pnd_Grand_Pnd_Total_Alive", "#TOTAL-ALIVE", FieldType.PACKED_DECIMAL, 10);
        pnd_Grand_Pnd_Total_Mos_Txt = pnd_Grand.newFieldArrayInGroup("pnd_Grand_Pnd_Total_Mos_Txt", "#TOTAL-MOS-TXT", FieldType.PACKED_DECIMAL, 10, new 
            DbsArrayController(1, 60, 1, 4));
        pnd_Grand_Pnd_Total_Rltn_Code = pnd_Grand.newFieldArrayInGroup("pnd_Grand_Pnd_Total_Rltn_Code", "#TOTAL-RLTN-CODE", FieldType.PACKED_DECIMAL, 10, 
            new DbsArrayController(1, 99, 1, 4));
        pnd_Grand_Pnd_Total_Bd_Bene = pnd_Grand.newFieldArrayInGroup("pnd_Grand_Pnd_Total_Bd_Bene", "#TOTAL-BD-BENE", FieldType.PACKED_DECIMAL, 10, new 
            DbsArrayController(1, 30, 1, 4));

        pnd_Totals = localVariables.newGroupArrayInRecord("pnd_Totals", "#TOTALS", new DbsArrayController(1, 4));
        pnd_Totals_Pnd_Total_Bc = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Bc", "#TOTAL-BC", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Bc_Tc = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Bc_Tc", "#TOTAL-BC-TC", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Bc_T = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Bc_T", "#TOTAL-BC-T", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Bc_C = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Bc_C", "#TOTAL-BC-C", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_No_Eff = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_No_Eff", "#TOTAL-NO-EFF", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Mos = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Mos", "#TOTAL-MOS", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Reg_Bene = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Reg_Bene", "#TOTAL-REG-BENE", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Pymnt_A = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Pymnt_A", "#TOTAL-PYMNT-A", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Pymnt_B = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Pymnt_B", "#TOTAL-PYMNT-B", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Pymnt_C = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Pymnt_C", "#TOTAL-PYMNT-C", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Pymnt_D = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Pymnt_D", "#TOTAL-PYMNT-D", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Pymnt_E = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Pymnt_E", "#TOTAL-PYMNT-E", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Pymnt_F = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Pymnt_F", "#TOTAL-PYMNT-F", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Exempt = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Exempt", "#TOTAL-EXEMPT", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Waive = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Waive", "#TOTAL-WAIVE", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Fldr_T = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Fldr_T", "#TOTAL-FLDR-T", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Fldr_B = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Fldr_B", "#TOTAL-FLDR-B", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Fldr_I = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Fldr_I", "#TOTAL-FLDR-I", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Fldr_Mit = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Fldr_Mit", "#TOTAL-FLDR-MIT", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Poa = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Poa", "#TOTAL-POA", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Bm_Irvcbl = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Bm_Irvcbl", "#TOTAL-BM-IRVCBL", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Bm = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Bm", "#TOTAL-BM", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Bm_Tc = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Bm_Tc", "#TOTAL-BM-TC", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Bm_T = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Bm_T", "#TOTAL-BM-T", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Bm_C = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Bm_C", "#TOTAL-BM-C", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Bd = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Bd", "#TOTAL-BD", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Bd_Tc = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Bd_Tc", "#TOTAL-BD-TC", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Bd_T = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Bd_T", "#TOTAL-BD-T", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Bd_C = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Bd_C", "#TOTAL-BD-C", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Prmry_Only = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Prmry_Only", "#TOTAL-PRMRY-ONLY", FieldType.PACKED_DECIMAL, 
            10);
        pnd_Totals_Pnd_Total_Dlft = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Dlft", "#TOTAL-DLFT", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Prmry_Scndry = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Prmry_Scndry", "#TOTAL-PRMRY-SCNDRY", FieldType.PACKED_DECIMAL, 
            10);
        pnd_Totals_Pnd_Total_Primary = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Primary", "#TOTAL-PRIMARY", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Secondary = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Secondary", "#TOTAL-SECONDARY", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Name1 = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Name1", "#TOTAL-NAME1", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Name2 = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Name2", "#TOTAL-NAME2", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Name1_2 = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Name1_2", "#TOTAL-NAME1-2", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_No_Name1 = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_No_Name1", "#TOTAL-NO-NAME1", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_No_Name2 = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_No_Name2", "#TOTAL-NO-NAME2", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_No_Rltn = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_No_Rltn", "#TOTAL-NO-RLTN", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Rltn = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Rltn", "#TOTAL-RLTN", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Trust_Dot = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Trust_Dot", "#TOTAL-TRUST-DOT", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Trust_Tin = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Trust_Tin", "#TOTAL-TRUST-TIN", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Rltn_Txt = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Rltn_Txt", "#TOTAL-RLTN-TXT", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Dob = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Dob", "#TOTAL-DOB", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_No_Dob = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_No_Dob", "#TOTAL-NO-DOB", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Ss_S = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Ss_S", "#TOTAL-SS-S", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Ss_I = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Ss_I", "#TOTAL-SS-I", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Ss_T = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Ss_T", "#TOTAL-SS-T", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_No_Ss_Cd = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_No_Ss_Cd", "#TOTAL-NO-SS-CD", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_No_Ss_Nmbr = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_No_Ss_Nmbr", "#TOTAL-NO-SS-NMBR", FieldType.PACKED_DECIMAL, 
            10);
        pnd_Totals_Pnd_Total_P = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_P", "#TOTAL-P", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_100 = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_100", "#TOTAL-100", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_S = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_S", "#TOTAL-S", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Eq = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Eq", "#TOTAL-EQ", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Bd_Irvcbl = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Bd_Irvcbl", "#TOTAL-BD-IRVCBL", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Rstct_L = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Rstct_L", "#TOTAL-RSTCT-L", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Rstct_N = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Rstct_N", "#TOTAL-RSTCT-N", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Rstct_R = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Rstct_R", "#TOTAL-RSTCT-R", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Rstct_X = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Rstct_X", "#TOTAL-RSTCT-X", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Addr1 = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Addr1", "#TOTAL-ADDR1", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Addr2 = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Addr2", "#TOTAL-ADDR2", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Addr3 = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Addr3", "#TOTAL-ADDR3", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_State = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_State", "#TOTAL-STATE", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Zip = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Zip", "#TOTAL-ZIP", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Country = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Country", "#TOTAL-COUNTRY", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Gender = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Gender", "#TOTAL-GENDER", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Phone = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Phone", "#TOTAL-PHONE", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Spcl1 = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Spcl1", "#TOTAL-SPCL1", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Spcl2 = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Spcl2", "#TOTAL-SPCL2", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Spcl3 = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Spcl3", "#TOTAL-SPCL3", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Nm_Dob_Ssn = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Nm_Dob_Ssn", "#TOTAL-NM-DOB-SSN", FieldType.PACKED_DECIMAL, 
            10);
        pnd_Totals_Pnd_Total_Nm_Dob = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Nm_Dob", "#TOTAL-NM-DOB", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Nm_Ssn = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Nm_Ssn", "#TOTAL-NM-SSN", FieldType.PACKED_DECIMAL, 10);
        pnd_Totals_Pnd_Total_Nm = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Nm", "#TOTAL-NM", FieldType.PACKED_DECIMAL, 10);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_bc.reset();
        vw_bd.reset();
        vw_bm.reset();
        vw_bt.reset();

        localVariables.reset();
        pnd_Default.setInitialValue(false);
        pnd_Primary.setInitialValue(false);
        pnd_Secondary.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Benp261() throws Exception
    {
        super("Benp261");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 60 SG = OFF ZP = ON
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        pnd_Rltn.getValue("*").reset();                                                                                                                                   //Natural: RESET #RLTN ( * )
        vw_bt.startDatabaseRead                                                                                                                                           //Natural: READ BT BY BT-TABLE-ID-KEY STARTING FROM 'RC'
        (
        "READ01",
        new Wc[] { new Wc("BT_TABLE_ID_KEY", ">=", "RC", WcType.BY) },
        new Oc[] { new Oc("BT_TABLE_ID_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_bt.readNextRow("READ01")))
        {
            if (condition(bt_Bt_Table_Id.notEquals("RC")))                                                                                                                //Natural: IF BT-TABLE-ID NE 'RC'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_X.setValue(bt_Bt_Table_Key_N2);                                                                                                                           //Natural: ASSIGN #X := BT-TABLE-KEY-N2
            if (condition(pnd_X.notEquals(getZero())))                                                                                                                    //Natural: IF #X NE 0
            {
                pnd_Rltn.getValue(pnd_X).setValue(bt_Bt_Table_Text);                                                                                                      //Natural: ASSIGN #RLTN ( #X ) := BT-TABLE-TEXT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  MAIN PROCESS
        //* ***********************************************************************
        vw_bc.startDatabaseRead                                                                                                                                           //Natural: READ BC BY BC-PIN-CNTRCT-IND-KEY
        (
        "RBC",
        new Oc[] { new Oc("BC_PIN_CNTRCT_IND_KEY", "ASC") }
        );
        boolean endOfDataRbc = true;
        boolean firstRbc = true;
        RBC:
        while (condition(vw_bc.readNextRow("RBC")))
        {
            if (condition(vw_bc.getAstCOUNTER().greater(0)))
            {
                atBreakEventRbc();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataRbc = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT BREAK OF BC-PIN
            //*  PENDING AND CURRENT ONLY
            if (condition(!(bc_Bc_Stat.equals("C") || bc_Bc_Stat.equals("P"))))                                                                                           //Natural: ACCEPT IF BC-STAT = 'C' OR = 'P'
            {
                continue;
            }
            //*  SET INDEX FOR BUCKETS
            //*  DA
            //*  IA
            //*  INSURANCE
            short decideConditionsMet275 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF BC-CNTRCT-TYPE;//Natural: VALUE 'd','D'
            if (condition((bc_Bc_Cntrct_Type.equals("d") || bc_Bc_Cntrct_Type.equals("D"))))
            {
                decideConditionsMet275++;
                pnd_I.setValue(2);                                                                                                                                        //Natural: ASSIGN #I := 2
            }                                                                                                                                                             //Natural: VALUE 'i', 'I'
            else if (condition((bc_Bc_Cntrct_Type.equals("i") || bc_Bc_Cntrct_Type.equals("I"))))
            {
                decideConditionsMet275++;
                pnd_I.setValue(3);                                                                                                                                        //Natural: ASSIGN #I := 3
            }                                                                                                                                                             //Natural: VALUE 'n', 'N'
            else if (condition((bc_Bc_Cntrct_Type.equals("n") || bc_Bc_Cntrct_Type.equals("N"))))
            {
                decideConditionsMet275++;
                pnd_I.setValue(4);                                                                                                                                        //Natural: ASSIGN #I := 4
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_I.setValue(2);                                                                                                                                        //Natural: ASSIGN #I := 2
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Totals_Pnd_Total_Bc.getValue(pnd_I).nadd(1);                                                                                                              //Natural: ADD 1 TO #TOTAL-BC ( #I )
            short decideConditionsMet287 = 0;                                                                                                                             //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN BC-TIAA-CREF-IND = ' '
            if (condition(bc_Bc_Tiaa_Cref_Ind.equals(" ")))
            {
                decideConditionsMet287++;
                pnd_Totals_Pnd_Total_Bc_Tc.getValue(pnd_I).nadd(1);                                                                                                       //Natural: ADD 1 TO #TOTAL-BC-TC ( #I )
            }                                                                                                                                                             //Natural: WHEN BC-TIAA-CREF-IND = 't' OR = 'T'
            if (condition(bc_Bc_Tiaa_Cref_Ind.equals("t") || bc_Bc_Tiaa_Cref_Ind.equals("T")))
            {
                decideConditionsMet287++;
                pnd_Totals_Pnd_Total_Bc_T.getValue(pnd_I).nadd(1);                                                                                                        //Natural: ADD 1 TO #TOTAL-BC-T ( #I )
            }                                                                                                                                                             //Natural: WHEN BC-TIAA-CREF-IND = 'c' OR = 'C'
            if (condition(bc_Bc_Tiaa_Cref_Ind.equals("c") || bc_Bc_Tiaa_Cref_Ind.equals("C")))
            {
                decideConditionsMet287++;
                pnd_Totals_Pnd_Total_Bc_C.getValue(pnd_I).nadd(1);                                                                                                        //Natural: ADD 1 TO #TOTAL-BC-C ( #I )
            }                                                                                                                                                             //Natural: WHEN BC-EFF-DTE = ' ' OR = '00000000'
            if (condition(bc_Bc_Eff_Dte.equals(" ") || bc_Bc_Eff_Dte.equals("00000000")))
            {
                decideConditionsMet287++;
                pnd_Totals_Pnd_Total_No_Eff.getValue(pnd_I).nadd(1);                                                                                                      //Natural: ADD 1 TO #TOTAL-NO-EFF ( #I )
            }                                                                                                                                                             //Natural: WHEN BC-MOS-IND = 'y' OR = 'Y'
            if (condition(bc_Bc_Mos_Ind.equals("y") || bc_Bc_Mos_Ind.equals("Y")))
            {
                decideConditionsMet287++;
                pnd_Totals_Pnd_Total_Mos.getValue(pnd_I).nadd(1);                                                                                                         //Natural: ADD 1 TO #TOTAL-MOS ( #I )
            }                                                                                                                                                             //Natural: WHEN BC-MOS-IND = 'n' OR = 'N'
            if (condition(bc_Bc_Mos_Ind.equals("n") || bc_Bc_Mos_Ind.equals("N")))
            {
                decideConditionsMet287++;
                pnd_Totals_Pnd_Total_Reg_Bene.getValue(pnd_I).nadd(1);                                                                                                    //Natural: ADD 1 TO #TOTAL-REG-BENE ( #I )
            }                                                                                                                                                             //Natural: WHEN BC-IRVCBL-IND = 'y' OR = 'Y'
            if (condition(bc_Bc_Irvcbl_Ind.equals("y") || bc_Bc_Irvcbl_Ind.equals("Y")))
            {
                decideConditionsMet287++;
                pnd_Totals_Pnd_Total_Bm_Irvcbl.getValue(pnd_I).nadd(1);                                                                                                   //Natural: ADD 1 TO #TOTAL-BM-IRVCBL ( #I )
            }                                                                                                                                                             //Natural: WHEN BC-PYMNT-CHLD-DCSD-IND = 'a' OR = 'A'
            if (condition(bc_Bc_Pymnt_Chld_Dcsd_Ind.equals("a") || bc_Bc_Pymnt_Chld_Dcsd_Ind.equals("A")))
            {
                decideConditionsMet287++;
                pnd_Totals_Pnd_Total_Pymnt_A.getValue(pnd_I).nadd(1);                                                                                                     //Natural: ADD 1 TO #TOTAL-PYMNT-A ( #I )
            }                                                                                                                                                             //Natural: WHEN BC-PYMNT-CHLD-DCSD-IND = 'b' OR = 'B'
            if (condition(bc_Bc_Pymnt_Chld_Dcsd_Ind.equals("b") || bc_Bc_Pymnt_Chld_Dcsd_Ind.equals("B")))
            {
                decideConditionsMet287++;
                pnd_Totals_Pnd_Total_Pymnt_B.getValue(pnd_I).nadd(1);                                                                                                     //Natural: ADD 1 TO #TOTAL-PYMNT-B ( #I )
            }                                                                                                                                                             //Natural: WHEN BC-PYMNT-CHLD-DCSD-IND = 'c' OR = 'C'
            if (condition(bc_Bc_Pymnt_Chld_Dcsd_Ind.equals("c") || bc_Bc_Pymnt_Chld_Dcsd_Ind.equals("C")))
            {
                decideConditionsMet287++;
                pnd_Totals_Pnd_Total_Pymnt_C.getValue(pnd_I).nadd(1);                                                                                                     //Natural: ADD 1 TO #TOTAL-PYMNT-C ( #I )
            }                                                                                                                                                             //Natural: WHEN BC-PYMNT-CHLD-DCSD-IND = 'd' OR = 'D'
            if (condition(bc_Bc_Pymnt_Chld_Dcsd_Ind.equals("d") || bc_Bc_Pymnt_Chld_Dcsd_Ind.equals("D")))
            {
                decideConditionsMet287++;
                pnd_Totals_Pnd_Total_Pymnt_D.getValue(pnd_I).nadd(1);                                                                                                     //Natural: ADD 1 TO #TOTAL-PYMNT-D ( #I )
            }                                                                                                                                                             //Natural: WHEN BC-PYMNT-CHLD-DCSD-IND = 'e' OR = 'E'
            if (condition(bc_Bc_Pymnt_Chld_Dcsd_Ind.equals("e") || bc_Bc_Pymnt_Chld_Dcsd_Ind.equals("E")))
            {
                decideConditionsMet287++;
                pnd_Totals_Pnd_Total_Pymnt_E.getValue(pnd_I).nadd(1);                                                                                                     //Natural: ADD 1 TO #TOTAL-PYMNT-E ( #I )
            }                                                                                                                                                             //Natural: WHEN BC-PYMNT-CHLD-DCSD-IND = 'f' OR = 'F'
            if (condition(bc_Bc_Pymnt_Chld_Dcsd_Ind.equals("f") || bc_Bc_Pymnt_Chld_Dcsd_Ind.equals("F")))
            {
                decideConditionsMet287++;
                pnd_Totals_Pnd_Total_Pymnt_F.getValue(pnd_I).nadd(1);                                                                                                     //Natural: ADD 1 TO #TOTAL-PYMNT-F ( #I )
            }                                                                                                                                                             //Natural: WHEN BC-EXEMPT-SPOUSE-RIGHTS = 'y' OR = 'Y'
            if (condition(bc_Bc_Exempt_Spouse_Rights.equals("y") || bc_Bc_Exempt_Spouse_Rights.equals("Y")))
            {
                decideConditionsMet287++;
                pnd_Totals_Pnd_Total_Exempt.getValue(pnd_I).nadd(1);                                                                                                      //Natural: ADD 1 TO #TOTAL-EXEMPT ( #I )
            }                                                                                                                                                             //Natural: WHEN BC-SPOUSE-WAIVED-BNFTS = 'y' OR = 'Y'
            if (condition(bc_Bc_Spouse_Waived_Bnfts.equals("y") || bc_Bc_Spouse_Waived_Bnfts.equals("Y")))
            {
                decideConditionsMet287++;
                pnd_Totals_Pnd_Total_Waive.getValue(pnd_I).nadd(1);                                                                                                       //Natural: ADD 1 TO #TOTAL-WAIVE ( #I )
            }                                                                                                                                                             //Natural: WHEN BC-TRUST-DATA-FLDR NE ' '
            if (condition(bc_Bc_Trust_Data_Fldr.notEquals(" ")))
            {
                decideConditionsMet287++;
                pnd_Totals_Pnd_Total_Fldr_T.getValue(pnd_I).nadd(1);                                                                                                      //Natural: ADD 1 TO #TOTAL-FLDR-T ( #I )
                if (condition(bc_Bc_Rcrd_Last_Updt_Dte.greater(pnd_Last_Trust)))                                                                                          //Natural: IF BC-RCRD-LAST-UPDT-DTE > #LAST-TRUST
                {
                    pnd_Last_Trust.setValue(bc_Bc_Rcrd_Last_Updt_Dte);                                                                                                    //Natural: ASSIGN #LAST-TRUST := BC-RCRD-LAST-UPDT-DTE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN BC-BENE-ADDR-FLDR NE ' '
            if (condition(bc_Bc_Bene_Addr_Fldr.notEquals(" ")))
            {
                decideConditionsMet287++;
                pnd_Totals_Pnd_Total_Fldr_B.getValue(pnd_I).nadd(1);                                                                                                      //Natural: ADD 1 TO #TOTAL-FLDR-B ( #I )
                if (condition(bc_Bc_Rcrd_Last_Updt_Dte.greater(pnd_Last_Bene)))                                                                                           //Natural: IF BC-RCRD-LAST-UPDT-DTE > #LAST-BENE
                {
                    pnd_Last_Bene.setValue(bc_Bc_Rcrd_Last_Updt_Dte);                                                                                                     //Natural: ASSIGN #LAST-BENE := BC-RCRD-LAST-UPDT-DTE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN BC-CO-OWNER-DATA-FLDR NE ' '
            if (condition(bc_Bc_Co_Owner_Data_Fldr.notEquals(" ")))
            {
                decideConditionsMet287++;
                pnd_Totals_Pnd_Total_Fldr_I.getValue(pnd_I).nadd(1);                                                                                                      //Natural: ADD 1 TO #TOTAL-FLDR-I ( #I )
                if (condition(bc_Bc_Rcrd_Last_Updt_Dte.greater(pnd_Last_Ins)))                                                                                            //Natural: IF BC-RCRD-LAST-UPDT-DTE > #LAST-INS
                {
                    pnd_Last_Ins.setValue(bc_Bc_Rcrd_Last_Updt_Dte);                                                                                                      //Natural: ASSIGN #LAST-INS := BC-RCRD-LAST-UPDT-DTE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN BC-FLDR-MIN NE ' ' OR BC-FLDR-SRCE-ID NE ' ' OR BC-FLDR-LOG-DTE-TME NE ' '
            if (condition(bc_Bc_Fldr_Min.notEquals(" ") || bc_Bc_Fldr_Srce_Id.notEquals(" ") || bc_Bc_Fldr_Log_Dte_Tme.notEquals(" ")))
            {
                decideConditionsMet287++;
                pnd_Totals_Pnd_Total_Fldr_Mit.getValue(pnd_I).nadd(1);                                                                                                    //Natural: ADD 1 TO #TOTAL-FLDR-MIT ( #I )
                if (condition(bc_Bc_Rcrd_Last_Updt_Dte.greater(pnd_Last_Mit)))                                                                                            //Natural: IF BC-RCRD-LAST-UPDT-DTE > #LAST-MIT
                {
                    pnd_Last_Mit.setValue(bc_Bc_Rcrd_Last_Updt_Dte);                                                                                                      //Natural: ASSIGN #LAST-MIT := BC-RCRD-LAST-UPDT-DTE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN BC-CHNG-PWR-ATTY NE ' '
            if (condition(bc_Bc_Chng_Pwr_Atty.notEquals(" ")))
            {
                decideConditionsMet287++;
                pnd_Totals_Pnd_Total_Poa.getValue(pnd_I).nadd(1);                                                                                                         //Natural: ADD 1 TO #TOTAL-POA ( #I )
                if (condition(bc_Bc_Rcrd_Last_Updt_Dte.greater(pnd_Last_Poa)))                                                                                            //Natural: IF BC-RCRD-LAST-UPDT-DTE > #LAST-POA
                {
                    pnd_Last_Poa.setValue(bc_Bc_Rcrd_Last_Updt_Dte);                                                                                                      //Natural: ASSIGN #LAST-POA := BC-RCRD-LAST-UPDT-DTE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet287 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(bc_Bc_Mos_Ind.equals("y") || bc_Bc_Mos_Ind.equals("Y")))                                                                                        //Natural: IF BC-MOS-IND = 'y' OR = 'Y'
            {
                                                                                                                                                                          //Natural: PERFORM READ-BENE-MOS
                sub_Read_Bene_Mos();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RBC"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RBC"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM READ-BENE-DESIGNATION
                sub_Read_Bene_Designation();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RBC"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RBC"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (condition(vw_bc.getAstCOUNTER().greater(0)))
        {
            atBreakEventRbc(endOfDataRbc);
        }
        if (Global.isEscape()) return;
        //*  ADD DA, IA, INS BUCKETS TO TOTAL BUCKETS
        pnd_Totals_Pnd_Total_Bc.getValue(1).nadd(pnd_Totals_Pnd_Total_Bc.getValue(2,":",4));                                                                              //Natural: ADD #TOTAL-BC ( 2:4 ) TO #TOTAL-BC ( 1 )
        pnd_Totals_Pnd_Total_Bc_Tc.getValue(1).nadd(pnd_Totals_Pnd_Total_Bc_Tc.getValue(2,":",4));                                                                        //Natural: ADD #TOTAL-BC-TC ( 2:4 ) TO #TOTAL-BC-TC ( 1 )
        pnd_Totals_Pnd_Total_Bc_T.getValue(1).nadd(pnd_Totals_Pnd_Total_Bc_T.getValue(2,":",4));                                                                          //Natural: ADD #TOTAL-BC-T ( 2:4 ) TO #TOTAL-BC-T ( 1 )
        pnd_Totals_Pnd_Total_Bc_C.getValue(1).nadd(pnd_Totals_Pnd_Total_Bc_C.getValue(2,":",4));                                                                          //Natural: ADD #TOTAL-BC-C ( 2:4 ) TO #TOTAL-BC-C ( 1 )
        pnd_Totals_Pnd_Total_No_Eff.getValue(1).nadd(pnd_Totals_Pnd_Total_No_Eff.getValue(2,":",4));                                                                      //Natural: ADD #TOTAL-NO-EFF ( 2:4 ) TO #TOTAL-NO-EFF ( 1 )
        pnd_Totals_Pnd_Total_Mos.getValue(1).nadd(pnd_Totals_Pnd_Total_Mos.getValue(2,":",4));                                                                            //Natural: ADD #TOTAL-MOS ( 2:4 ) TO #TOTAL-MOS ( 1 )
        pnd_Totals_Pnd_Total_Reg_Bene.getValue(1).nadd(pnd_Totals_Pnd_Total_Reg_Bene.getValue(2,":",4));                                                                  //Natural: ADD #TOTAL-REG-BENE ( 2:4 ) TO #TOTAL-REG-BENE ( 1 )
        pnd_Totals_Pnd_Total_Pymnt_A.getValue(1).nadd(pnd_Totals_Pnd_Total_Pymnt_A.getValue(2,":",4));                                                                    //Natural: ADD #TOTAL-PYMNT-A ( 2:4 ) TO #TOTAL-PYMNT-A ( 1 )
        pnd_Totals_Pnd_Total_Pymnt_B.getValue(1).nadd(pnd_Totals_Pnd_Total_Pymnt_B.getValue(2,":",4));                                                                    //Natural: ADD #TOTAL-PYMNT-B ( 2:4 ) TO #TOTAL-PYMNT-B ( 1 )
        pnd_Totals_Pnd_Total_Pymnt_C.getValue(1).nadd(pnd_Totals_Pnd_Total_Pymnt_C.getValue(2,":",4));                                                                    //Natural: ADD #TOTAL-PYMNT-C ( 2:4 ) TO #TOTAL-PYMNT-C ( 1 )
        pnd_Totals_Pnd_Total_Pymnt_D.getValue(1).nadd(pnd_Totals_Pnd_Total_Pymnt_D.getValue(2,":",4));                                                                    //Natural: ADD #TOTAL-PYMNT-D ( 2:4 ) TO #TOTAL-PYMNT-D ( 1 )
        pnd_Totals_Pnd_Total_Pymnt_E.getValue(1).nadd(pnd_Totals_Pnd_Total_Pymnt_E.getValue(2,":",4));                                                                    //Natural: ADD #TOTAL-PYMNT-E ( 2:4 ) TO #TOTAL-PYMNT-E ( 1 )
        pnd_Totals_Pnd_Total_Pymnt_F.getValue(1).nadd(pnd_Totals_Pnd_Total_Pymnt_F.getValue(2,":",4));                                                                    //Natural: ADD #TOTAL-PYMNT-F ( 2:4 ) TO #TOTAL-PYMNT-F ( 1 )
        pnd_Totals_Pnd_Total_Exempt.getValue(1).nadd(pnd_Totals_Pnd_Total_Exempt.getValue(2,":",4));                                                                      //Natural: ADD #TOTAL-EXEMPT ( 2:4 ) TO #TOTAL-EXEMPT ( 1 )
        pnd_Totals_Pnd_Total_Waive.getValue(1).nadd(pnd_Totals_Pnd_Total_Waive.getValue(2,":",4));                                                                        //Natural: ADD #TOTAL-WAIVE ( 2:4 ) TO #TOTAL-WAIVE ( 1 )
        pnd_Totals_Pnd_Total_Fldr_T.getValue(1).nadd(pnd_Totals_Pnd_Total_Fldr_T.getValue(2,":",4));                                                                      //Natural: ADD #TOTAL-FLDR-T ( 2:4 ) TO #TOTAL-FLDR-T ( 1 )
        pnd_Totals_Pnd_Total_Fldr_B.getValue(1).nadd(pnd_Totals_Pnd_Total_Fldr_B.getValue(2,":",4));                                                                      //Natural: ADD #TOTAL-FLDR-B ( 2:4 ) TO #TOTAL-FLDR-B ( 1 )
        pnd_Totals_Pnd_Total_Fldr_I.getValue(1).nadd(pnd_Totals_Pnd_Total_Fldr_I.getValue(2,":",4));                                                                      //Natural: ADD #TOTAL-FLDR-I ( 2:4 ) TO #TOTAL-FLDR-I ( 1 )
        pnd_Totals_Pnd_Total_Fldr_Mit.getValue(1).nadd(pnd_Totals_Pnd_Total_Fldr_Mit.getValue(2,":",4));                                                                  //Natural: ADD #TOTAL-FLDR-MIT ( 2:4 ) TO #TOTAL-FLDR-MIT ( 1 )
        pnd_Totals_Pnd_Total_Poa.getValue(1).nadd(pnd_Totals_Pnd_Total_Poa.getValue(2,":",4));                                                                            //Natural: ADD #TOTAL-POA ( 2:4 ) TO #TOTAL-POA ( 1 )
        pnd_Totals_Pnd_Total_Bd.getValue(1).nadd(pnd_Totals_Pnd_Total_Bd.getValue(2,":",4));                                                                              //Natural: ADD #TOTAL-BD ( 2:4 ) TO #TOTAL-BD ( 1 )
        pnd_Totals_Pnd_Total_Bd_Tc.getValue(1).nadd(pnd_Totals_Pnd_Total_Bd_Tc.getValue(2,":",4));                                                                        //Natural: ADD #TOTAL-BD-TC ( 2:4 ) TO #TOTAL-BD-TC ( 1 )
        pnd_Totals_Pnd_Total_Bd_T.getValue(1).nadd(pnd_Totals_Pnd_Total_Bd_T.getValue(2,":",4));                                                                          //Natural: ADD #TOTAL-BD-T ( 2:4 ) TO #TOTAL-BD-T ( 1 )
        pnd_Totals_Pnd_Total_Bd_C.getValue(1).nadd(pnd_Totals_Pnd_Total_Bd_C.getValue(2,":",4));                                                                          //Natural: ADD #TOTAL-BD-C ( 2:4 ) TO #TOTAL-BD-C ( 1 )
        pnd_Totals_Pnd_Total_Dlft.getValue(1).nadd(pnd_Totals_Pnd_Total_Dlft.getValue(2,":",4));                                                                          //Natural: ADD #TOTAL-DLFT ( 2:4 ) TO #TOTAL-DLFT ( 1 )
        pnd_Totals_Pnd_Total_Primary.getValue(1).nadd(pnd_Totals_Pnd_Total_Primary.getValue(2,":",4));                                                                    //Natural: ADD #TOTAL-PRIMARY ( 2:4 ) TO #TOTAL-PRIMARY ( 1 )
        pnd_Totals_Pnd_Total_Prmry_Only.getValue(1).nadd(pnd_Totals_Pnd_Total_Prmry_Only.getValue(2,":",4));                                                              //Natural: ADD #TOTAL-PRMRY-ONLY ( 2:4 ) TO #TOTAL-PRMRY-ONLY ( 1 )
        pnd_Totals_Pnd_Total_Secondary.getValue(1).nadd(pnd_Totals_Pnd_Total_Secondary.getValue(2,":",4));                                                                //Natural: ADD #TOTAL-SECONDARY ( 2:4 ) TO #TOTAL-SECONDARY ( 1 )
        pnd_Totals_Pnd_Total_Prmry_Scndry.getValue(1).nadd(pnd_Totals_Pnd_Total_Prmry_Scndry.getValue(2,":",4));                                                          //Natural: ADD #TOTAL-PRMRY-SCNDRY ( 2:4 ) TO #TOTAL-PRMRY-SCNDRY ( 1 )
        pnd_Totals_Pnd_Total_Name1.getValue(1).nadd(pnd_Totals_Pnd_Total_Name1.getValue(2,":",4));                                                                        //Natural: ADD #TOTAL-NAME1 ( 2:4 ) TO #TOTAL-NAME1 ( 1 )
        pnd_Totals_Pnd_Total_Name2.getValue(1).nadd(pnd_Totals_Pnd_Total_Name2.getValue(2,":",4));                                                                        //Natural: ADD #TOTAL-NAME2 ( 2:4 ) TO #TOTAL-NAME2 ( 1 )
        pnd_Totals_Pnd_Total_Name1_2.getValue(1).nadd(pnd_Totals_Pnd_Total_Name1_2.getValue(2,":",4));                                                                    //Natural: ADD #TOTAL-NAME1-2 ( 2:4 ) TO #TOTAL-NAME1-2 ( 1 )
        pnd_Totals_Pnd_Total_No_Name1.getValue(1).nadd(pnd_Totals_Pnd_Total_No_Name1.getValue(2,":",4));                                                                  //Natural: ADD #TOTAL-NO-NAME1 ( 2:4 ) TO #TOTAL-NO-NAME1 ( 1 )
        pnd_Totals_Pnd_Total_No_Name2.getValue(1).nadd(pnd_Totals_Pnd_Total_No_Name2.getValue(2,":",4));                                                                  //Natural: ADD #TOTAL-NO-NAME2 ( 2:4 ) TO #TOTAL-NO-NAME2 ( 1 )
        pnd_Totals_Pnd_Total_No_Rltn.getValue(1).nadd(pnd_Totals_Pnd_Total_No_Rltn.getValue(2,":",4));                                                                    //Natural: ADD #TOTAL-NO-RLTN ( 2:4 ) TO #TOTAL-NO-RLTN ( 1 )
        pnd_Totals_Pnd_Total_Rltn.getValue(1).nadd(pnd_Totals_Pnd_Total_Rltn.getValue(2,":",4));                                                                          //Natural: ADD #TOTAL-RLTN ( 2:4 ) TO #TOTAL-RLTN ( 1 )
        pnd_Totals_Pnd_Total_Dob.getValue(1).nadd(pnd_Totals_Pnd_Total_Dob.getValue(2,":",4));                                                                            //Natural: ADD #TOTAL-DOB ( 2:4 ) TO #TOTAL-DOB ( 1 )
        pnd_Totals_Pnd_Total_No_Dob.getValue(1).nadd(pnd_Totals_Pnd_Total_No_Dob.getValue(2,":",4));                                                                      //Natural: ADD #TOTAL-NO-DOB ( 2:4 ) TO #TOTAL-NO-DOB ( 1 )
        pnd_Totals_Pnd_Total_Ss_S.getValue(1).nadd(pnd_Totals_Pnd_Total_Ss_S.getValue(2,":",4));                                                                          //Natural: ADD #TOTAL-SS-S ( 2:4 ) TO #TOTAL-SS-S ( 1 )
        pnd_Totals_Pnd_Total_Ss_I.getValue(1).nadd(pnd_Totals_Pnd_Total_Ss_I.getValue(2,":",4));                                                                          //Natural: ADD #TOTAL-SS-I ( 2:4 ) TO #TOTAL-SS-I ( 1 )
        pnd_Totals_Pnd_Total_Ss_T.getValue(1).nadd(pnd_Totals_Pnd_Total_Ss_T.getValue(2,":",4));                                                                          //Natural: ADD #TOTAL-SS-T ( 2:4 ) TO #TOTAL-SS-T ( 1 )
        pnd_Totals_Pnd_Total_No_Ss_Cd.getValue(1).nadd(pnd_Totals_Pnd_Total_No_Ss_Cd.getValue(2,":",4));                                                                  //Natural: ADD #TOTAL-NO-SS-CD ( 2:4 ) TO #TOTAL-NO-SS-CD ( 1 )
        pnd_Totals_Pnd_Total_No_Ss_Nmbr.getValue(1).nadd(pnd_Totals_Pnd_Total_No_Ss_Nmbr.getValue(2,":",4));                                                              //Natural: ADD #TOTAL-NO-SS-NMBR ( 2:4 ) TO #TOTAL-NO-SS-NMBR ( 1 )
        pnd_Totals_Pnd_Total_P.getValue(1).nadd(pnd_Totals_Pnd_Total_P.getValue(2,":",4));                                                                                //Natural: ADD #TOTAL-P ( 2:4 ) TO #TOTAL-P ( 1 )
        pnd_Totals_Pnd_Total_100.getValue(1).nadd(pnd_Totals_Pnd_Total_100.getValue(2,":",4));                                                                            //Natural: ADD #TOTAL-100 ( 2:4 ) TO #TOTAL-100 ( 1 )
        pnd_Totals_Pnd_Total_S.getValue(1).nadd(pnd_Totals_Pnd_Total_S.getValue(2,":",4));                                                                                //Natural: ADD #TOTAL-S ( 2:4 ) TO #TOTAL-S ( 1 )
        pnd_Totals_Pnd_Total_Eq.getValue(1).nadd(pnd_Totals_Pnd_Total_Eq.getValue(2,":",4));                                                                              //Natural: ADD #TOTAL-EQ ( 2:4 ) TO #TOTAL-EQ ( 1 )
        pnd_Totals_Pnd_Total_Bd_Irvcbl.getValue(1).nadd(pnd_Totals_Pnd_Total_Bd_Irvcbl.getValue(2,":",4));                                                                //Natural: ADD #TOTAL-BD-IRVCBL ( 2:4 ) TO #TOTAL-BD-IRVCBL ( 1 )
        pnd_Totals_Pnd_Total_Rstct_L.getValue(1).nadd(pnd_Totals_Pnd_Total_Rstct_L.getValue(2,":",4));                                                                    //Natural: ADD #TOTAL-RSTCT-L ( 2:4 ) TO #TOTAL-RSTCT-L ( 1 )
        pnd_Totals_Pnd_Total_Rstct_N.getValue(1).nadd(pnd_Totals_Pnd_Total_Rstct_N.getValue(2,":",4));                                                                    //Natural: ADD #TOTAL-RSTCT-N ( 2:4 ) TO #TOTAL-RSTCT-N ( 1 )
        pnd_Totals_Pnd_Total_Rstct_R.getValue(1).nadd(pnd_Totals_Pnd_Total_Rstct_R.getValue(2,":",4));                                                                    //Natural: ADD #TOTAL-RSTCT-R ( 2:4 ) TO #TOTAL-RSTCT-R ( 1 )
        pnd_Totals_Pnd_Total_Rstct_X.getValue(1).nadd(pnd_Totals_Pnd_Total_Rstct_X.getValue(2,":",4));                                                                    //Natural: ADD #TOTAL-RSTCT-X ( 2:4 ) TO #TOTAL-RSTCT-X ( 1 )
        pnd_Totals_Pnd_Total_Addr1.getValue(1).nadd(pnd_Totals_Pnd_Total_Addr1.getValue(2,":",4));                                                                        //Natural: ADD #TOTAL-ADDR1 ( 2:4 ) TO #TOTAL-ADDR1 ( 1 )
        pnd_Totals_Pnd_Total_Addr2.getValue(1).nadd(pnd_Totals_Pnd_Total_Addr2.getValue(2,":",4));                                                                        //Natural: ADD #TOTAL-ADDR2 ( 2:4 ) TO #TOTAL-ADDR2 ( 1 )
        pnd_Totals_Pnd_Total_Addr3.getValue(1).nadd(pnd_Totals_Pnd_Total_Addr3.getValue(2,":",4));                                                                        //Natural: ADD #TOTAL-ADDR3 ( 2:4 ) TO #TOTAL-ADDR3 ( 1 )
        pnd_Totals_Pnd_Total_State.getValue(1).nadd(pnd_Totals_Pnd_Total_State.getValue(2,":",4));                                                                        //Natural: ADD #TOTAL-STATE ( 2:4 ) TO #TOTAL-STATE ( 1 )
        pnd_Totals_Pnd_Total_Zip.getValue(1).nadd(pnd_Totals_Pnd_Total_Zip.getValue(2,":",4));                                                                            //Natural: ADD #TOTAL-ZIP ( 2:4 ) TO #TOTAL-ZIP ( 1 )
        pnd_Totals_Pnd_Total_Country.getValue(1).nadd(pnd_Totals_Pnd_Total_Country.getValue(2,":",4));                                                                    //Natural: ADD #TOTAL-COUNTRY ( 2:4 ) TO #TOTAL-COUNTRY ( 1 )
        pnd_Totals_Pnd_Total_Phone.getValue(1).nadd(pnd_Totals_Pnd_Total_Phone.getValue(2,":",4));                                                                        //Natural: ADD #TOTAL-PHONE ( 2:4 ) TO #TOTAL-PHONE ( 1 )
        pnd_Totals_Pnd_Total_Gender.getValue(1).nadd(pnd_Totals_Pnd_Total_Gender.getValue(2,":",4));                                                                      //Natural: ADD #TOTAL-GENDER ( 2:4 ) TO #TOTAL-GENDER ( 1 )
        pnd_Totals_Pnd_Total_Spcl1.getValue(1).nadd(pnd_Totals_Pnd_Total_Spcl1.getValue(2,":",4));                                                                        //Natural: ADD #TOTAL-SPCL1 ( 2:4 ) TO #TOTAL-SPCL1 ( 1 )
        pnd_Totals_Pnd_Total_Spcl2.getValue(1).nadd(pnd_Totals_Pnd_Total_Spcl2.getValue(2,":",4));                                                                        //Natural: ADD #TOTAL-SPCL2 ( 2:4 ) TO #TOTAL-SPCL2 ( 1 )
        pnd_Totals_Pnd_Total_Spcl3.getValue(1).nadd(pnd_Totals_Pnd_Total_Spcl3.getValue(2,":",4));                                                                        //Natural: ADD #TOTAL-SPCL3 ( 2:4 ) TO #TOTAL-SPCL3 ( 1 )
        pnd_Totals_Pnd_Total_Nm_Dob_Ssn.getValue(1).nadd(pnd_Totals_Pnd_Total_Nm_Dob_Ssn.getValue(2,":",4));                                                              //Natural: ADD #TOTAL-NM-DOB-SSN ( 2:4 ) TO #TOTAL-NM-DOB-SSN ( 1 )
        pnd_Totals_Pnd_Total_Nm_Dob.getValue(1).nadd(pnd_Totals_Pnd_Total_Nm_Dob.getValue(2,":",4));                                                                      //Natural: ADD #TOTAL-NM-DOB ( 2:4 ) TO #TOTAL-NM-DOB ( 1 )
        pnd_Totals_Pnd_Total_Nm_Ssn.getValue(1).nadd(pnd_Totals_Pnd_Total_Nm_Ssn.getValue(2,":",4));                                                                      //Natural: ADD #TOTAL-NM-SSN ( 2:4 ) TO #TOTAL-NM-SSN ( 1 )
        pnd_Totals_Pnd_Total_Nm.getValue(1).nadd(pnd_Totals_Pnd_Total_Nm.getValue(2,":",4));                                                                              //Natural: ADD #TOTAL-NM ( 2:4 ) TO #TOTAL-NM ( 1 )
        pnd_Totals_Pnd_Total_Bm.getValue(1).nadd(pnd_Totals_Pnd_Total_Bm.getValue(2,":",4));                                                                              //Natural: ADD #TOTAL-BM ( 2:4 ) TO #TOTAL-BM ( 1 )
        pnd_Totals_Pnd_Total_Bm_Irvcbl.getValue(1).nadd(pnd_Totals_Pnd_Total_Bm_Irvcbl.getValue(2,":",4));                                                                //Natural: ADD #TOTAL-BM-IRVCBL ( 2:4 ) TO #TOTAL-BM-IRVCBL ( 1 )
        pnd_Totals_Pnd_Total_Bm_Tc.getValue(1).nadd(pnd_Totals_Pnd_Total_Bm_Tc.getValue(2,":",4));                                                                        //Natural: ADD #TOTAL-BM-TC ( 2:4 ) TO #TOTAL-BM-TC ( 1 )
        pnd_Totals_Pnd_Total_Bm_T.getValue(1).nadd(pnd_Totals_Pnd_Total_Bm_T.getValue(2,":",4));                                                                          //Natural: ADD #TOTAL-BM-T ( 2:4 ) TO #TOTAL-BM-T ( 1 )
        pnd_Totals_Pnd_Total_Bm_C.getValue(1).nadd(pnd_Totals_Pnd_Total_Bm_C.getValue(2,":",4));                                                                          //Natural: ADD #TOTAL-BM-C ( 2:4 ) TO #TOTAL-BM-C ( 1 )
        pnd_Totals_Pnd_Total_Rltn_Txt.getValue(1).nadd(pnd_Totals_Pnd_Total_Rltn_Txt.getValue(2,":",4));                                                                  //Natural: ADD #TOTAL-RLTN-TXT ( 2:4 ) TO #TOTAL-RLTN-TXT ( 1 )
        FOR01:                                                                                                                                                            //Natural: FOR #X = 1 TO 30
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(30)); pnd_X.nadd(1))
        {
            pnd_Grand_Pnd_Total_Bd_Bene.getValue(pnd_X,1).nadd(pnd_Grand_Pnd_Total_Bd_Bene.getValue(pnd_X,2,":",4));                                                      //Natural: ADD #TOTAL-BD-BENE ( #X,2:4 ) TO #TOTAL-BD-BENE ( #X,1 )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR02:                                                                                                                                                            //Natural: FOR #X = 1 TO 60
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(60)); pnd_X.nadd(1))
        {
            pnd_Grand_Pnd_Total_Mos_Txt.getValue(pnd_X,1).nadd(pnd_Grand_Pnd_Total_Mos_Txt.getValue(pnd_X,2,":",4));                                                      //Natural: ADD #TOTAL-MOS-TXT ( #X,2:4 ) TO #TOTAL-MOS-TXT ( #X,1 )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR03:                                                                                                                                                            //Natural: FOR #X = 1 TO 99
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(99)); pnd_X.nadd(1))
        {
            pnd_Grand_Pnd_Total_Rltn_Code.getValue(pnd_X,1).nadd(pnd_Grand_Pnd_Total_Rltn_Code.getValue(pnd_X,2,":",4));                                                  //Natural: ADD #TOTAL-RLTN-CODE ( #X,2:4 ) TO #TOTAL-RLTN-CODE ( #X,1 )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
        sub_Write_Report();
        if (condition(Global.isEscape())) {return;}
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-BENE-DESIGNATION
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-BENE-MOS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT
    }
    private void sub_Read_Bene_Designation() throws Exception                                                                                                             //Natural: READ-BENE-DESIGNATION
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Primary.reset();                                                                                                                                              //Natural: RESET #PRIMARY #SECONDARY #DEFAULT #BENE
        pnd_Secondary.reset();
        pnd_Default.reset();
        pnd_Bene.reset();
        pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq.reset();                                                                                                                      //Natural: RESET #BD-STAT-PIN-CNTRCT-IND-TYPE-SEQ
        pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Stat.setValue(bc_Bc_Stat);                                                                                             //Natural: ASSIGN #BD-STAT := BC.BC-STAT
        pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Pin.setValue(bc_Bc_Pin);                                                                                               //Natural: ASSIGN #BD-PIN := BC.BC-PIN
        pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Tiaa_Cntrct.setValue(bc_Bc_Tiaa_Cntrct);                                                                               //Natural: ASSIGN #BD-TIAA-CNTRCT := BC.BC-TIAA-CNTRCT
        pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Tiaa_Cref_Ind.setValue(bc_Bc_Tiaa_Cref_Ind);                                                                           //Natural: ASSIGN #BD-TIAA-CREF-IND := BC.BC-TIAA-CREF-IND
        vw_bd.startDatabaseRead                                                                                                                                           //Natural: READ BD BY BD-STAT-PIN-CNTRCT-IND-TYPE-SEQ STARTING FROM #BD-STAT-PIN-CNTRCT-IND-TYPE-SEQ
        (
        "RBD",
        new Wc[] { new Wc("BD_STAT_PIN_CNTRCT_IND_TYPE_SEQ", ">=", pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq, WcType.BY) },
        new Oc[] { new Oc("BD_STAT_PIN_CNTRCT_IND_TYPE_SEQ", "ASC") }
        );
        RBD:
        while (condition(vw_bd.readNextRow("RBD")))
        {
            if (condition(bd_Bd_Stat.notEquals(pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Stat) || bd_Bd_Pin.notEquals(pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Pin)    //Natural: IF BD-STAT NE #BD-STAT OR BD-PIN NE #BD-PIN OR BD-TIAA-CNTRCT NE #BD-TIAA-CNTRCT OR BD-TIAA-CREF-IND NE #BD-TIAA-CREF-IND
                || bd_Bd_Tiaa_Cntrct.notEquals(pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Tiaa_Cntrct) || bd_Bd_Tiaa_Cref_Ind.notEquals(pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Tiaa_Cref_Ind)))
            {
                if (true) break RBD;                                                                                                                                      //Natural: ESCAPE BOTTOM ( RBD. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Totals_Pnd_Total_Bd.getValue(pnd_I).nadd(1);                                                                                                              //Natural: ADD 1 TO #TOTAL-BD ( #I )
            pnd_Bene.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #BENE
            short decideConditionsMet470 = 0;                                                                                                                             //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN BD-TIAA-CREF-IND = ' '
            if (condition(bd_Bd_Tiaa_Cref_Ind.equals(" ")))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_Bd_Tc.getValue(pnd_I).nadd(1);                                                                                                       //Natural: ADD 1 TO #TOTAL-BD-TC ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-TIAA-CREF-IND = 't' OR = 'T'
            if (condition(bd_Bd_Tiaa_Cref_Ind.equals("t") || bd_Bd_Tiaa_Cref_Ind.equals("T")))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_Bd_T.getValue(pnd_I).nadd(1);                                                                                                        //Natural: ADD 1 TO #TOTAL-BD-T ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-TIAA-CREF-IND = 'c' OR = 'C'
            if (condition(bd_Bd_Tiaa_Cref_Ind.equals("c") || bd_Bd_Tiaa_Cref_Ind.equals("C")))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_Bd_C.getValue(pnd_I).nadd(1);                                                                                                        //Natural: ADD 1 TO #TOTAL-BD-C ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-DFLT-ESTATE = 'y' OR = 'Y'
            if (condition(bd_Bd_Dflt_Estate.equals("y") || bd_Bd_Dflt_Estate.equals("Y")))
            {
                decideConditionsMet470++;
                pnd_Default.setValue(true);                                                                                                                               //Natural: ASSIGN #DEFAULT := TRUE
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: WHEN BD-BENE-TYPE = 'p' OR = 'P'
            if (condition(bd_Bd_Bene_Type.equals("p") || bd_Bd_Bene_Type.equals("P")))
            {
                decideConditionsMet470++;
                pnd_Primary.setValue(true);                                                                                                                               //Natural: ASSIGN #PRIMARY := TRUE
                pnd_Totals_Pnd_Total_Primary.getValue(pnd_I).nadd(1);                                                                                                     //Natural: ADD 1 TO #TOTAL-PRIMARY ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-BENE-TYPE = 's' OR = 'S' OR = 'c' OR = 'C'
            if (condition(bd_Bd_Bene_Type.equals("s") || bd_Bd_Bene_Type.equals("S") || bd_Bd_Bene_Type.equals("c") || bd_Bd_Bene_Type.equals("C")))
            {
                decideConditionsMet470++;
                pnd_Secondary.setValue(true);                                                                                                                             //Natural: ASSIGN #SECONDARY := TRUE
                pnd_Totals_Pnd_Total_Secondary.getValue(pnd_I).nadd(1);                                                                                                   //Natural: ADD 1 TO #TOTAL-SECONDARY ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-BENE-NAME1 NE ' '
            if (condition(bd_Bd_Bene_Name1.notEquals(" ")))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_Name1.getValue(pnd_I).nadd(1);                                                                                                       //Natural: ADD 1 TO #TOTAL-NAME1 ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-BENE-NAME2 NE ' '
            if (condition(bd_Bd_Bene_Name2.notEquals(" ")))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_Name2.getValue(pnd_I).nadd(1);                                                                                                       //Natural: ADD 1 TO #TOTAL-NAME2 ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-BENE-NAME1 NE ' ' AND BD-BENE-NAME2 NE ' '
            if (condition(bd_Bd_Bene_Name1.notEquals(" ") && bd_Bd_Bene_Name2.notEquals(" ")))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_Name1_2.getValue(pnd_I).nadd(1);                                                                                                     //Natural: ADD 1 TO #TOTAL-NAME1-2 ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-BENE-NAME1 = ' '
            if (condition(bd_Bd_Bene_Name1.equals(" ")))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_No_Name1.getValue(pnd_I).nadd(1);                                                                                                    //Natural: ADD 1 TO #TOTAL-NO-NAME1 ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-BENE-NAME2 = ' '
            if (condition(bd_Bd_Bene_Name2.equals(" ")))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_No_Name2.getValue(pnd_I).nadd(1);                                                                                                    //Natural: ADD 1 TO #TOTAL-NO-NAME2 ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-RLTN-CD = ' '
            if (condition(bd_Bd_Rltn_Cd.equals(" ")))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_No_Rltn.getValue(pnd_I).nadd(1);                                                                                                     //Natural: ADD 1 TO #TOTAL-NO-RLTN ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-RLTN-CD-N2 > 0
            if (condition(bd_Bd_Rltn_Cd_N2.greater(getZero())))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_Rltn.getValue(pnd_I).nadd(1);                                                                                                        //Natural: ADD 1 TO #TOTAL-RLTN ( #I )
                pnd_Grand_Pnd_Total_Rltn_Code.getValue(bd_Bd_Rltn_Cd_N2,pnd_I).nadd(1);                                                                                   //Natural: ADD 1 TO #TOTAL-RLTN-CODE ( BD-RLTN-CD-N2,#I )
            }                                                                                                                                                             //Natural: WHEN BD-RLTN-CD = '08'
            if (condition(bd_Bd_Rltn_Cd.equals("08")))
            {
                decideConditionsMet470++;
                if (condition(bd_Bd_Dte_Birth_Trust.notEquals(" ")))                                                                                                      //Natural: IF BD-DTE-BIRTH-TRUST NE ' '
                {
                    pnd_Totals_Pnd_Total_Trust_Dot.getValue(pnd_I).nadd(1);                                                                                               //Natural: ADD 1 TO #TOTAL-TRUST-DOT ( #I )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(((bd_Bd_Ss_Cd.equals("t") || bd_Bd_Ss_Cd.equals("T")) && ! (((bd_Bd_Ss_Nmbr.equals(" ") || bd_Bd_Ss_Nmbr.equals("000000000"))               //Natural: IF BD-SS-CD = 't' OR = 'T' AND NOT ( BD-SS-NMBR = ' ' OR = '000000000' OR = '999999999' )
                    || bd_Bd_Ss_Nmbr.equals("999999999"))))))
                {
                    pnd_Totals_Pnd_Total_Trust_Tin.getValue(pnd_I).nadd(1);                                                                                               //Natural: ADD 1 TO #TOTAL-TRUST-TIN ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN BD-RLTN-FREE-TXT NE ' '
            if (condition(bd_Bd_Rltn_Free_Txt.notEquals(" ")))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_Rltn_Txt.getValue(pnd_I).nadd(1);                                                                                                    //Natural: ADD 1 TO #TOTAL-RLTN-TXT ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-DTE-BIRTH-TRUST NE ' '
            if (condition(bd_Bd_Dte_Birth_Trust.notEquals(" ")))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_Dob.getValue(pnd_I).nadd(1);                                                                                                         //Natural: ADD 1 TO #TOTAL-DOB ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-DTE-BIRTH-TRUST = ' '
            if (condition(bd_Bd_Dte_Birth_Trust.equals(" ")))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_No_Dob.getValue(pnd_I).nadd(1);                                                                                                      //Natural: ADD 1 TO #TOTAL-NO-DOB ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-SS-CD = 's' OR = 'S'
            if (condition(bd_Bd_Ss_Cd.equals("s") || bd_Bd_Ss_Cd.equals("S")))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_Ss_S.getValue(pnd_I).nadd(1);                                                                                                        //Natural: ADD 1 TO #TOTAL-SS-S ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-SS-CD = 'i' OR = 'I'
            if (condition(bd_Bd_Ss_Cd.equals("i") || bd_Bd_Ss_Cd.equals("I")))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_Ss_I.getValue(pnd_I).nadd(1);                                                                                                        //Natural: ADD 1 TO #TOTAL-SS-I ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-SS-CD = 't' OR = 'T'
            if (condition(bd_Bd_Ss_Cd.equals("t") || bd_Bd_Ss_Cd.equals("T")))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_Ss_T.getValue(pnd_I).nadd(1);                                                                                                        //Natural: ADD 1 TO #TOTAL-SS-T ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-SS-CD = ' '
            if (condition(bd_Bd_Ss_Cd.equals(" ")))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_No_Ss_Cd.getValue(pnd_I).nadd(1);                                                                                                    //Natural: ADD 1 TO #TOTAL-NO-SS-CD ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-SS-NMBR = ' ' OR = '000000000' OR = '999999999'
            if (condition(bd_Bd_Ss_Nmbr.equals(" ") || bd_Bd_Ss_Nmbr.equals("000000000") || bd_Bd_Ss_Nmbr.equals("999999999")))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_No_Ss_Nmbr.getValue(pnd_I).nadd(1);                                                                                                  //Natural: ADD 1 TO #TOTAL-NO-SS-NMBR ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-PERC-SHARE-IND = 'p' OR = 'P' AND BD-SHARE-PERC > 0
            if (condition(((bd_Bd_Perc_Share_Ind.equals("p") || bd_Bd_Perc_Share_Ind.equals("P")) && bd_Bd_Share_Perc.greater(getZero()))))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_P.getValue(pnd_I).nadd(1);                                                                                                           //Natural: ADD 1 TO #TOTAL-P ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-SHARE-PERC = 100 AND BD-PERC-SHARE-IND = 'p' OR = 'P'
            if (condition((bd_Bd_Share_Perc.equals(100) && (bd_Bd_Perc_Share_Ind.equals("p") || bd_Bd_Perc_Share_Ind.equals("P")))))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_100.getValue(pnd_I).nadd(1);                                                                                                         //Natural: ADD 1 TO #TOTAL-100 ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-PERC-SHARE-IND = 's' OR = 'S' AND ( BD-SHARE-NTOR > 0 OR BD-SHARE-DTOR > 0 )
            if (condition(((bd_Bd_Perc_Share_Ind.equals("s") || bd_Bd_Perc_Share_Ind.equals("S")) && (bd_Bd_Share_Ntor.greater(getZero()) || bd_Bd_Share_Dtor.greater(getZero())))))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_S.getValue(pnd_I).nadd(1);                                                                                                           //Natural: ADD 1 TO #TOTAL-S ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-SHARE-NTOR = BD-SHARE-DTOR AND BD-PERC-SHARE-IND = 's' OR = 'S'
            if (condition((bd_Bd_Share_Ntor.equals(bd_Bd_Share_Dtor) && (bd_Bd_Perc_Share_Ind.equals("s") || bd_Bd_Perc_Share_Ind.equals("S")))))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_Eq.getValue(pnd_I).nadd(1);                                                                                                          //Natural: ADD 1 TO #TOTAL-EQ ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-IRVCBL-IND = 'y' OR = 'Y'
            if (condition(bd_Bd_Irvcbl_Ind.equals("y") || bd_Bd_Irvcbl_Ind.equals("Y")))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_Bd_Irvcbl.getValue(pnd_I).nadd(1);                                                                                                   //Natural: ADD 1 TO #TOTAL-BD-IRVCBL ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-STLMNT-RSTRCTN = 'l' OR = 'L'
            if (condition(bd_Bd_Stlmnt_Rstrctn.equals("l") || bd_Bd_Stlmnt_Rstrctn.equals("L")))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_Rstct_L.getValue(pnd_I).nadd(1);                                                                                                     //Natural: ADD 1 TO #TOTAL-RSTCT-L ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-STLMNT-RSTRCTN = 'n' OR = 'N'
            if (condition(bd_Bd_Stlmnt_Rstrctn.equals("n") || bd_Bd_Stlmnt_Rstrctn.equals("N")))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_Rstct_N.getValue(pnd_I).nadd(1);                                                                                                     //Natural: ADD 1 TO #TOTAL-RSTCT-N ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-STLMNT-RSTRCTN = 'r' OR = 'R'
            if (condition(bd_Bd_Stlmnt_Rstrctn.equals("r") || bd_Bd_Stlmnt_Rstrctn.equals("R")))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_Rstct_R.getValue(pnd_I).nadd(1);                                                                                                     //Natural: ADD 1 TO #TOTAL-RSTCT-R ( #I )
            }                                                                                                                                                             //Natural: WHEN NOT ( BD-STLMNT-RSTRCTN = 'l' OR = 'n' OR = 'r' OR = 'L' OR = 'N' OR = 'R' )
            if (condition(! ((bd_Bd_Stlmnt_Rstrctn.equals("l") || bd_Bd_Stlmnt_Rstrctn.equals("n") || bd_Bd_Stlmnt_Rstrctn.equals("r") || bd_Bd_Stlmnt_Rstrctn.equals("L") 
                || bd_Bd_Stlmnt_Rstrctn.equals("N") || bd_Bd_Stlmnt_Rstrctn.equals("R")))))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_Rstct_X.getValue(pnd_I).nadd(1);                                                                                                     //Natural: ADD 1 TO #TOTAL-RSTCT-X ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-ADDR1 NE ' '
            if (condition(bd_Bd_Addr1.notEquals(" ")))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_Addr1.getValue(pnd_I).nadd(1);                                                                                                       //Natural: ADD 1 TO #TOTAL-ADDR1 ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-ADDR2 NE ' '
            if (condition(bd_Bd_Addr2.notEquals(" ")))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_Addr2.getValue(pnd_I).nadd(1);                                                                                                       //Natural: ADD 1 TO #TOTAL-ADDR2 ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-ADDR3-CITY NE ' '
            if (condition(bd_Bd_Addr3_City.notEquals(" ")))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_Addr3.getValue(pnd_I).nadd(1);                                                                                                       //Natural: ADD 1 TO #TOTAL-ADDR3 ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-STATE NE ' '
            if (condition(bd_Bd_State.notEquals(" ")))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_State.getValue(pnd_I).nadd(1);                                                                                                       //Natural: ADD 1 TO #TOTAL-STATE ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-ZIP NE ' '
            if (condition(bd_Bd_Zip.notEquals(" ")))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_Zip.getValue(pnd_I).nadd(1);                                                                                                         //Natural: ADD 1 TO #TOTAL-ZIP ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-COUNTRY NE ' '
            if (condition(bd_Bd_Country.notEquals(" ")))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_Country.getValue(pnd_I).nadd(1);                                                                                                     //Natural: ADD 1 TO #TOTAL-COUNTRY ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-PHONE NE ' '
            if (condition(bd_Bd_Phone.notEquals(" ")))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_Phone.getValue(pnd_I).nadd(1);                                                                                                       //Natural: ADD 1 TO #TOTAL-PHONE ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-GENDER NE ' '
            if (condition(bd_Bd_Gender.notEquals(" ")))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_Gender.getValue(pnd_I).nadd(1);                                                                                                      //Natural: ADD 1 TO #TOTAL-GENDER ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-SPCL-TXT1 NE ' '
            if (condition(bd_Bd_Spcl_Txt1.notEquals(" ")))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_Spcl1.getValue(pnd_I).nadd(1);                                                                                                       //Natural: ADD 1 TO #TOTAL-SPCL1 ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-SPCL-TXT2 NE ' '
            if (condition(bd_Bd_Spcl_Txt2.notEquals(" ")))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_Spcl2.getValue(pnd_I).nadd(1);                                                                                                       //Natural: ADD 1 TO #TOTAL-SPCL2 ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-SPCL-TXT3 NE ' '
            if (condition(bd_Bd_Spcl_Txt3.notEquals(" ")))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_Spcl3.getValue(pnd_I).nadd(1);                                                                                                       //Natural: ADD 1 TO #TOTAL-SPCL3 ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-BENE-NAME1 NE ' ' AND BD-DTE-BIRTH-TRUST NE ' ' AND BD-SS-NMBR NE ' '
            if (condition(bd_Bd_Bene_Name1.notEquals(" ") && bd_Bd_Dte_Birth_Trust.notEquals(" ") && bd_Bd_Ss_Nmbr.notEquals(" ")))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_Nm_Dob_Ssn.getValue(pnd_I).nadd(1);                                                                                                  //Natural: ADD 1 TO #TOTAL-NM-DOB-SSN ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-BENE-NAME1 NE ' ' AND BD-DTE-BIRTH-TRUST NE ' ' AND BD-SS-NMBR = ' '
            if (condition(bd_Bd_Bene_Name1.notEquals(" ") && bd_Bd_Dte_Birth_Trust.notEquals(" ") && bd_Bd_Ss_Nmbr.equals(" ")))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_Nm_Dob.getValue(pnd_I).nadd(1);                                                                                                      //Natural: ADD 1 TO #TOTAL-NM-DOB ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-BENE-NAME1 NE ' ' AND BD-DTE-BIRTH-TRUST = ' ' AND BD-SS-NMBR NE ' '
            if (condition(bd_Bd_Bene_Name1.notEquals(" ") && bd_Bd_Dte_Birth_Trust.equals(" ") && bd_Bd_Ss_Nmbr.notEquals(" ")))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_Nm_Ssn.getValue(pnd_I).nadd(1);                                                                                                      //Natural: ADD 1 TO #TOTAL-NM-SSN ( #I )
            }                                                                                                                                                             //Natural: WHEN BD-BENE-NAME1 NE ' ' AND BD-DTE-BIRTH-TRUST = ' ' AND BD-SS-NMBR = ' '
            if (condition(bd_Bd_Bene_Name1.notEquals(" ") && bd_Bd_Dte_Birth_Trust.equals(" ") && bd_Bd_Ss_Nmbr.equals(" ")))
            {
                decideConditionsMet470++;
                pnd_Totals_Pnd_Total_Nm.getValue(pnd_I).nadd(1);                                                                                                          //Natural: ADD 1 TO #TOTAL-NM ( #I )
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet470 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  RDBD.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Bene.greater(getZero())))                                                                                                                       //Natural: IF #BENE > 0
        {
            pnd_Grand_Pnd_Total_Bd_Bene.getValue(pnd_Bene,pnd_I).nadd(1);                                                                                                 //Natural: ADD 1 TO #TOTAL-BD-BENE ( #BENE,#I )
            if (condition(pnd_Primary.getBoolean() && ! (pnd_Secondary.getBoolean())))                                                                                    //Natural: IF #PRIMARY AND NOT #SECONDARY
            {
                if (condition(pnd_Default.getBoolean()))                                                                                                                  //Natural: IF #DEFAULT
                {
                    pnd_Totals_Pnd_Total_Dlft.getValue(pnd_I).nadd(1);                                                                                                    //Natural: ADD 1 TO #TOTAL-DLFT ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Totals_Pnd_Total_Prmry_Only.getValue(pnd_I).nadd(1);                                                                                              //Natural: ADD 1 TO #TOTAL-PRMRY-ONLY ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Totals_Pnd_Total_Prmry_Scndry.getValue(pnd_I).nadd(1);                                                                                                //Natural: ADD 1 TO #TOTAL-PRMRY-SCNDRY ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  READ-BENE-DESIGNATION
    }
    private void sub_Read_Bene_Mos() throws Exception                                                                                                                     //Natural: READ-BENE-MOS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Bm_Stat_Pin_Cntrct_Ind_Key.reset();                                                                                                                           //Natural: RESET #BM-STAT-PIN-CNTRCT-IND-KEY
        pnd_Bm_Stat_Pin_Cntrct_Ind_Key_Pnd_Bm_Stat.setValue(bc_Bc_Stat);                                                                                                  //Natural: ASSIGN #BM-STAT := BC.BC-STAT
        pnd_Bm_Stat_Pin_Cntrct_Ind_Key_Pnd_Bm_Pin.setValue(bc_Bc_Pin);                                                                                                    //Natural: ASSIGN #BM-PIN := BC.BC-PIN
        pnd_Bm_Stat_Pin_Cntrct_Ind_Key_Pnd_Bm_Tiaa_Cntrct.setValue(bc_Bc_Tiaa_Cntrct);                                                                                    //Natural: ASSIGN #BM-TIAA-CNTRCT := BC.BC-TIAA-CNTRCT
        pnd_Bm_Stat_Pin_Cntrct_Ind_Key_Pnd_Bm_Tiaa_Cref_Ind.setValue(bc_Bc_Tiaa_Cref_Ind);                                                                                //Natural: ASSIGN #BM-TIAA-CREF-IND := BC.BC-TIAA-CREF-IND
        vw_bm.startDatabaseRead                                                                                                                                           //Natural: READ BM BY BM-STAT-PIN-CNTRCT-IND-KEY STARTING FROM #BM-STAT-PIN-CNTRCT-IND-KEY
        (
        "READ02",
        new Wc[] { new Wc("BM_STAT_PIN_CNTRCT_IND_KEY", ">=", pnd_Bm_Stat_Pin_Cntrct_Ind_Key, WcType.BY) },
        new Oc[] { new Oc("BM_STAT_PIN_CNTRCT_IND_KEY", "ASC") }
        );
        READ02:
        while (condition(vw_bm.readNextRow("READ02")))
        {
            if (condition(bm_Bm_Stat.notEquals(pnd_Bm_Stat_Pin_Cntrct_Ind_Key_Pnd_Bm_Stat) || bm_Bm_Pin.notEquals(pnd_Bm_Stat_Pin_Cntrct_Ind_Key_Pnd_Bm_Pin)              //Natural: IF BM-STAT NE #BM-STAT OR BM-PIN NE #BM-PIN OR BM-TIAA-CNTRCT NE #BM-TIAA-CNTRCT OR BM-TIAA-CREF-IND NE #BM-TIAA-CREF-IND
                || bm_Bm_Tiaa_Cntrct.notEquals(pnd_Bm_Stat_Pin_Cntrct_Ind_Key_Pnd_Bm_Tiaa_Cntrct) || bm_Bm_Tiaa_Cref_Ind.notEquals(pnd_Bm_Stat_Pin_Cntrct_Ind_Key_Pnd_Bm_Tiaa_Cref_Ind)))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Totals_Pnd_Total_Bm.getValue(pnd_I).nadd(1);                                                                                                              //Natural: ADD 1 TO #TOTAL-BM ( #I )
            short decideConditionsMet608 = 0;                                                                                                                             //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN BM-TIAA-CREF-IND = ' '
            if (condition(bm_Bm_Tiaa_Cref_Ind.equals(" ")))
            {
                decideConditionsMet608++;
                pnd_Totals_Pnd_Total_Bm_Tc.getValue(pnd_I).nadd(1);                                                                                                       //Natural: ADD 1 TO #TOTAL-BM-TC ( #I )
            }                                                                                                                                                             //Natural: WHEN BM-TIAA-CREF-IND = 't' OR = 'T'
            if (condition(bm_Bm_Tiaa_Cref_Ind.equals("t") || bm_Bm_Tiaa_Cref_Ind.equals("T")))
            {
                decideConditionsMet608++;
                pnd_Totals_Pnd_Total_Bm_T.getValue(pnd_I).nadd(1);                                                                                                        //Natural: ADD 1 TO #TOTAL-BM-T ( #I )
            }                                                                                                                                                             //Natural: WHEN BM-TIAA-CREF-IND = 'c' OR = 'C'
            if (condition(bm_Bm_Tiaa_Cref_Ind.equals("c") || bm_Bm_Tiaa_Cref_Ind.equals("C")))
            {
                decideConditionsMet608++;
                pnd_Totals_Pnd_Total_Bm_C.getValue(pnd_I).nadd(1);                                                                                                        //Natural: ADD 1 TO #TOTAL-BM-C ( #I )
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet608 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            FOR04:                                                                                                                                                        //Natural: FOR #X = 60 TO 1 STEP -1
            for (pnd_X.setValue(60); condition(pnd_X.greaterOrEqual(1)); pnd_X.nsubtract(1))
            {
                if (condition(bm_Bm_Mos_Txt.getValue(pnd_X).notEquals(" ")))                                                                                              //Natural: IF BM-MOS-TXT ( #X ) NE ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_X.greater(getZero())))                                                                                                                      //Natural: IF #X > 0
            {
                pnd_Grand_Pnd_Total_Mos_Txt.getValue(pnd_X,pnd_I).nadd(1);                                                                                                //Natural: ADD 1 TO #TOTAL-MOS-TXT ( #X,#I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  READ-BENE-MOS
    }
    private void sub_Write_Report() throws Exception                                                                                                                      //Natural: WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"Total PIN                                  :",pnd_Grand_Pnd_Total_Pin, new ReportEditMask                     //Natural: WRITE ( 1 ) NOTITLE / 'Total PIN                                  :' #TOTAL-PIN / 'Total PIN with Date of Death               :' #TOTAL-DEAD / 'Total PIN w/o  Date of Death               :' #TOTAL-ALIVE / / 'Total Bene-Contract                        :' #TOTAL-BC ( * ) / '      MOS-Ind = Y (Bene-MOS)               :' #TOTAL-MOS ( * ) / '      MOS-Ind = N (Bene-Designation)       :' #TOTAL-REG-BENE ( * ) / 'Total Designation (Both)                   :' #TOTAL-BC-TC ( * ) / '      T Split Designation                  :' #TOTAL-BC-T ( * ) / '      C Split Designation                  :' #TOTAL-BC-C ( * ) / 'Total NO Effective Date                    :' #TOTAL-NO-EFF ( * ) / 'Total Pymnt/Child Dcsd Bene All          =A:' #TOTAL-PYMNT-A ( * ) / '      Pymnt/Child Dcsd Been Primary      =B:' #TOTAL-PYMNT-B ( * ) / '      Pymnt/Child Dcsd Bene Secondary    =C:' #TOTAL-PYMNT-C ( * ) / '      Pymnt/Child Dcsd Child Mine All    =D:' #TOTAL-PYMNT-D ( * ) / '      Pymnt/Child Dcsd Child Mine Primary=E:' #TOTAL-PYMNT-E ( * ) / '      Pymnt/Child Dcsd Child Mine Secndry=F:' #TOTAL-PYMNT-F ( * ) / 'Total Exempt Spouse Rights                 :' #TOTAL-EXEMPT ( * ) / '      Spouse Waived Benefits               :' #TOTAL-WAIVE ( * ) / '      Folder - Trust                       :' #TOTAL-FLDR-T ( * ) '   Last Date:' #LAST-TRUST / '      Folder - Bene                        :' #TOTAL-FLDR-B ( * ) '   Last Date:' #LAST-BENE / '      Folder - Ins (Co-Owner)              :' #TOTAL-FLDR-I ( * ) '   Last Date:' #LAST-INS / '      Folder info in MIT                   :' #TOTAL-FLDR-MIT ( * ) '   Last Date:' #LAST-MIT / 'Total Change by Power of Attorney          :' #TOTAL-POA ( * ) '   Last Date:' #LAST-POA
            ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"Total PIN with Date of Death               :",pnd_Grand_Pnd_Total_Dead, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"Total PIN w/o  Date of Death               :",pnd_Grand_Pnd_Total_Alive, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,"Total Bene-Contract                        :",pnd_Totals_Pnd_Total_Bc.getValue("*"), new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      MOS-Ind = Y (Bene-MOS)               :",pnd_Totals_Pnd_Total_Mos.getValue("*"), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      MOS-Ind = N (Bene-Designation)       :",pnd_Totals_Pnd_Total_Reg_Bene.getValue("*"), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"Total Designation (Both)                   :",pnd_Totals_Pnd_Total_Bc_Tc.getValue("*"), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      T Split Designation                  :",pnd_Totals_Pnd_Total_Bc_T.getValue("*"), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      C Split Designation                  :",pnd_Totals_Pnd_Total_Bc_C.getValue("*"), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"Total NO Effective Date                    :",pnd_Totals_Pnd_Total_No_Eff.getValue("*"), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"Total Pymnt/Child Dcsd Bene All          =A:",pnd_Totals_Pnd_Total_Pymnt_A.getValue("*"), new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      Pymnt/Child Dcsd Been Primary      =B:",pnd_Totals_Pnd_Total_Pymnt_B.getValue("*"), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      Pymnt/Child Dcsd Bene Secondary    =C:",pnd_Totals_Pnd_Total_Pymnt_C.getValue("*"), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      Pymnt/Child Dcsd Child Mine All    =D:",pnd_Totals_Pnd_Total_Pymnt_D.getValue("*"), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      Pymnt/Child Dcsd Child Mine Primary=E:",pnd_Totals_Pnd_Total_Pymnt_E.getValue("*"), new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      Pymnt/Child Dcsd Child Mine Secndry=F:",pnd_Totals_Pnd_Total_Pymnt_F.getValue("*"), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"Total Exempt Spouse Rights                 :",pnd_Totals_Pnd_Total_Exempt.getValue("*"), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      Spouse Waived Benefits               :",pnd_Totals_Pnd_Total_Waive.getValue("*"), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      Folder - Trust                       :",pnd_Totals_Pnd_Total_Fldr_T.getValue("*"), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9"),"   Last Date:",pnd_Last_Trust,NEWLINE,"      Folder - Bene                        :",pnd_Totals_Pnd_Total_Fldr_B.getValue("*"), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),"   Last Date:",pnd_Last_Bene,NEWLINE,"      Folder - Ins (Co-Owner)              :",pnd_Totals_Pnd_Total_Fldr_I.getValue("*"), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),"   Last Date:",pnd_Last_Ins,NEWLINE,"      Folder info in MIT                   :",pnd_Totals_Pnd_Total_Fldr_Mit.getValue("*"), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),"   Last Date:",pnd_Last_Mit,NEWLINE,"Total Change by Power of Attorney          :",pnd_Totals_Pnd_Total_Poa.getValue("*"), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),"   Last Date:",pnd_Last_Poa);
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"Total Bene-Contract                        :",pnd_Totals_Pnd_Total_Bc.getValue("*"), new ReportEditMask       //Natural: WRITE ( 1 ) NOTITLE / 'Total Bene-Contract                        :' #TOTAL-BC ( * ) / '      MOS-Ind = Y (Bene-MOS)               :' #TOTAL-MOS ( * ) / 'Total Bene-MOS                             :' #TOTAL-BM ( * ) / 'Total Bene-MOS Irrevocable Ind=Y           :' #TOTAL-BM-IRVCBL ( * ) / 'Total Bene-MOS (Both)                      :' #TOTAL-BM-TC ( * ) / '      T Split Designation                  :' #TOTAL-BM-T ( * ) / '      C Split Designation                  :' #TOTAL-BM-C ( * )
            ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      MOS-Ind = Y (Bene-MOS)               :",pnd_Totals_Pnd_Total_Mos.getValue("*"), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"Total Bene-MOS                             :",pnd_Totals_Pnd_Total_Bm.getValue("*"), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"Total Bene-MOS Irrevocable Ind=Y           :",pnd_Totals_Pnd_Total_Bm_Irvcbl.getValue("*"), new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"Total Bene-MOS (Both)                      :",pnd_Totals_Pnd_Total_Bm_Tc.getValue("*"), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      T Split Designation                  :",pnd_Totals_Pnd_Total_Bm_T.getValue("*"), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      C Split Designation                  :",pnd_Totals_Pnd_Total_Bm_C.getValue("*"), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        FOR05:                                                                                                                                                            //Natural: FOR #X = 1 TO 60
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(60)); pnd_X.nadd(1))
        {
            if (condition(pnd_Grand_Pnd_Total_Mos_Txt.getValue(pnd_X,"*").greater(getZero())))                                                                            //Natural: IF #TOTAL-MOS-TXT ( #X,* ) > 0
            {
                getReports().write(1, ReportOption.NOTITLE,"      MOS with",pnd_X, new ReportEditMask ("Z9"),"Lines                    :",pnd_Grand_Pnd_Total_Mos_Txt.getValue(pnd_X,"*"),  //Natural: WRITE ( 1 ) '      MOS with' #X ( EM = Z9 ) 'Lines                    :' #TOTAL-MOS-TXT ( #X,* )
                    new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"Total Bene-Contract                        :",pnd_Totals_Pnd_Total_Bc.getValue("*"), new ReportEditMask       //Natural: WRITE ( 1 ) NOTITLE / 'Total Bene-Contract                        :' #TOTAL-BC ( * ) / 'Total Bene-Designation                     :' #TOTAL-BD ( * ) / '      MOS-Ind = N (Bene-Designation)       :' #TOTAL-REG-BENE ( * ) / 'Total Designation (Both)                   :' #TOTAL-BD-TC ( * ) / '      T Split Designation                  :' #TOTAL-BD-T ( * ) / '      C Split Designation                  :' #TOTAL-BD-C ( * ) / 'Total Bene Default To Estate               :' #TOTAL-DLFT ( * ) / 'Total Primary                              :' #TOTAL-PRIMARY ( * ) / '      - Primary ONLY                       :' #TOTAL-PRMRY-ONLY ( * ) / '      Secondary                            :' #TOTAL-SECONDARY ( * ) / '      Primary & Secondary                  :' #TOTAL-PRMRY-SCNDRY ( * ) / 'Total Bene with Name1                      :' #TOTAL-NAME1 ( * ) / '      Bene with Name2                      :' #TOTAL-NAME2 ( * ) / '      Bene with Name1 & Name2              :' #TOTAL-NAME1-2 ( * ) / '      Bene with No Name1                   :' #TOTAL-NO-NAME1 ( * ) / '      Bene with No Name2                   :' #TOTAL-NO-NAME2 ( * ) / '      Bene with NO Relationship Code       :' #TOTAL-NO-RLTN ( * ) / '      Bene with Relationship               :' #TOTAL-RLTN ( * ) / '      Bene with Relation free text         :' #TOTAL-RLTN-TXT ( * ) / '      Bene with Date of Birth or Trust     :' #TOTAL-DOB ( * ) / '      Bene with NO Date of Birth or Trust  :' #TOTAL-NO-DOB ( * ) / '      Bene SS Code = S(SSN)                :' #TOTAL-SS-S ( * ) / '           SS Code = I(Social Ins)         :' #TOTAL-SS-I ( * ) / '           SS Code = T(Tax ID)             :' #TOTAL-SS-T ( * ) / '      Bene with NO SS Code                 :' #TOTAL-NO-SS-CD ( * ) / '      Bene with NO SS Number               :' #TOTAL-NO-SS-NMBR ( * ) / '      Bene with Percentage                 :' #TOTAL-P ( * ) / '           Percentage = 100%               :' #TOTAL-100 ( * ) / '      Bene with Share                      :' #TOTAL-S ( * ) / '           Equal Shares                    :' #TOTAL-EQ ( * ) / '      Bene with Irrevocable Ind            :' #TOTAL-BD-IRVCBL ( * ) / '      Bene with Restriction = L(Lumpsum)   :' #TOTAL-RSTCT-L ( * ) / '           with Restriction = N(None)      :' #TOTAL-RSTCT-N ( * ) / '           with Restriction = R(Restricted):' #TOTAL-RSTCT-R ( * ) / '           with Restriction = Unknown      :' #TOTAL-RSTCT-X ( * ) / '      Bene with Address1                   :' #TOTAL-ADDR1 ( * ) / '           with Address2                   :' #TOTAL-ADDR2 ( * ) / '           with Address3/City              :' #TOTAL-ADDR3 ( * ) / '           with State/Province             :' #TOTAL-STATE ( * ) / '           with Zip Code                   :' #TOTAL-ZIP ( * ) / '           with Country                    :' #TOTAL-COUNTRY ( * ) / '           with Phone number               :' #TOTAL-PHONE ( * ) / '           with Gender                     :' #TOTAL-GENDER ( * ) / '      Bene with Special Text1              :' #TOTAL-SPCL1 ( * ) / '           with Special Text2              :' #TOTAL-SPCL2 ( * ) / '           with Special Text3              :' #TOTAL-SPCL3 ( * )
            ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"Total Bene-Designation                     :",pnd_Totals_Pnd_Total_Bd.getValue("*"), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      MOS-Ind = N (Bene-Designation)       :",pnd_Totals_Pnd_Total_Reg_Bene.getValue("*"), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"Total Designation (Both)                   :",pnd_Totals_Pnd_Total_Bd_Tc.getValue("*"), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      T Split Designation                  :",pnd_Totals_Pnd_Total_Bd_T.getValue("*"), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      C Split Designation                  :",pnd_Totals_Pnd_Total_Bd_C.getValue("*"), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"Total Bene Default To Estate               :",pnd_Totals_Pnd_Total_Dlft.getValue("*"), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"Total Primary                              :",pnd_Totals_Pnd_Total_Primary.getValue("*"), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      - Primary ONLY                       :",pnd_Totals_Pnd_Total_Prmry_Only.getValue("*"), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      Secondary                            :",pnd_Totals_Pnd_Total_Secondary.getValue("*"), new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      Primary & Secondary                  :",pnd_Totals_Pnd_Total_Prmry_Scndry.getValue("*"), new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"Total Bene with Name1                      :",pnd_Totals_Pnd_Total_Name1.getValue("*"), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      Bene with Name2                      :",pnd_Totals_Pnd_Total_Name2.getValue("*"), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      Bene with Name1 & Name2              :",pnd_Totals_Pnd_Total_Name1_2.getValue("*"), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      Bene with No Name1                   :",pnd_Totals_Pnd_Total_No_Name1.getValue("*"), new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      Bene with No Name2                   :",pnd_Totals_Pnd_Total_No_Name2.getValue("*"), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      Bene with NO Relationship Code       :",pnd_Totals_Pnd_Total_No_Rltn.getValue("*"), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      Bene with Relationship               :",pnd_Totals_Pnd_Total_Rltn.getValue("*"), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      Bene with Relation free text         :",pnd_Totals_Pnd_Total_Rltn_Txt.getValue("*"), new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      Bene with Date of Birth or Trust     :",pnd_Totals_Pnd_Total_Dob.getValue("*"), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      Bene with NO Date of Birth or Trust  :",pnd_Totals_Pnd_Total_No_Dob.getValue("*"), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      Bene SS Code = S(SSN)                :",pnd_Totals_Pnd_Total_Ss_S.getValue("*"), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"           SS Code = I(Social Ins)         :",pnd_Totals_Pnd_Total_Ss_I.getValue("*"), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"           SS Code = T(Tax ID)             :",pnd_Totals_Pnd_Total_Ss_T.getValue("*"), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      Bene with NO SS Code                 :",pnd_Totals_Pnd_Total_No_Ss_Cd.getValue("*"), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      Bene with NO SS Number               :",pnd_Totals_Pnd_Total_No_Ss_Nmbr.getValue("*"), new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      Bene with Percentage                 :",pnd_Totals_Pnd_Total_P.getValue("*"), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"           Percentage = 100%               :",pnd_Totals_Pnd_Total_100.getValue("*"), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      Bene with Share                      :",pnd_Totals_Pnd_Total_S.getValue("*"), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"           Equal Shares                    :",pnd_Totals_Pnd_Total_Eq.getValue("*"), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      Bene with Irrevocable Ind            :",pnd_Totals_Pnd_Total_Bd_Irvcbl.getValue("*"), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      Bene with Restriction = L(Lumpsum)   :",pnd_Totals_Pnd_Total_Rstct_L.getValue("*"), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"           with Restriction = N(None)      :",pnd_Totals_Pnd_Total_Rstct_N.getValue("*"), new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"           with Restriction = R(Restricted):",pnd_Totals_Pnd_Total_Rstct_R.getValue("*"), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"           with Restriction = Unknown      :",pnd_Totals_Pnd_Total_Rstct_X.getValue("*"), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      Bene with Address1                   :",pnd_Totals_Pnd_Total_Addr1.getValue("*"), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"           with Address2                   :",pnd_Totals_Pnd_Total_Addr2.getValue("*"), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"           with Address3/City              :",pnd_Totals_Pnd_Total_Addr3.getValue("*"), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"           with State/Province             :",pnd_Totals_Pnd_Total_State.getValue("*"), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"           with Zip Code                   :",pnd_Totals_Pnd_Total_Zip.getValue("*"), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"           with Country                    :",pnd_Totals_Pnd_Total_Country.getValue("*"), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"           with Phone number               :",pnd_Totals_Pnd_Total_Phone.getValue("*"), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"           with Gender                     :",pnd_Totals_Pnd_Total_Gender.getValue("*"), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      Bene with Special Text1              :",pnd_Totals_Pnd_Total_Spcl1.getValue("*"), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"           with Special Text2              :",pnd_Totals_Pnd_Total_Spcl2.getValue("*"), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"           with Special Text3              :",pnd_Totals_Pnd_Total_Spcl3.getValue("*"), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"Total Bene with Name + DOB + SSN           :",pnd_Totals_Pnd_Total_Nm_Dob_Ssn.getValue("*"),                  //Natural: WRITE ( 1 ) NOTITLE / 'Total Bene with Name + DOB + SSN           :' #TOTAL-NM-DOB-SSN ( * ) / '      Bene with Name + DOB (No SSN)        :' #TOTAL-NM-DOB ( * ) / '      Bene with Name + SSN (No DoB)        :' #TOTAL-NM-SSN ( * ) / '      Bene with Name (No SSN or DoB)       :' #TOTAL-NM ( * )
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      Bene with Name + DOB (No SSN)        :",pnd_Totals_Pnd_Total_Nm_Dob.getValue("*"), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      Bene with Name + SSN (No DoB)        :",pnd_Totals_Pnd_Total_Nm_Ssn.getValue("*"), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE,"      Bene with Name (No SSN or DoB)       :",pnd_Totals_Pnd_Total_Nm.getValue("*"), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        FOR06:                                                                                                                                                            //Natural: FOR #X = 1 TO 30
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(30)); pnd_X.nadd(1))
        {
            if (condition(pnd_Grand_Pnd_Total_Bd_Bene.getValue(pnd_X,"*").greater(getZero())))                                                                            //Natural: IF #TOTAL-BD-BENE ( #X,* ) > 0
            {
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(28),"Bene Count =",pnd_X, new ReportEditMask ("Z9"),":",pnd_Grand_Pnd_Total_Bd_Bene.getValue(pnd_X,"*"),  //Natural: WRITE ( 1 ) 28T 'Bene Count =' #X ( EM = Z9 ) ':' #TOTAL-BD-BENE ( #X,* )
                    new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"Relationship Code & Description Count      :");                                                               //Natural: WRITE ( 1 ) NOTITLE / 'Relationship Code & Description Count      :'
        if (Global.isEscape()) return;
        FOR07:                                                                                                                                                            //Natural: FOR #X = 1 TO 99
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(99)); pnd_X.nadd(1))
        {
            if (condition(pnd_Grand_Pnd_Total_Rltn_Code.getValue(pnd_X,"*").greater(getZero())))                                                                          //Natural: IF #TOTAL-RLTN-CODE ( #X,* ) > 0
            {
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),pnd_X, new ReportEditMask ("99"),pnd_Rltn.getValue(pnd_X),":",pnd_Grand_Pnd_Total_Rltn_Code.getValue(pnd_X,"*"),  //Natural: WRITE ( 1 ) 25T #X ( EM = 99 ) #RLTN ( #X ) ':' #TOTAL-RLTN-CODE ( #X,* )
                    new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  WRITE-REPORT
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(57),"Beneficiary System",new TabSetting(120),"Page:",getReports().getPageNumberDbs(1),  //Natural: WRITE ( 1 ) NOTITLE 001T *PROGRAM 057T 'Beneficiary System' 120T 'Page:' *PAGE-NUMBER ( 1 ) ( EM = ZZ,ZZ9 ) / 001T *DATX ( EM = MM/DD/YYYY ) 053T 'Monthly Statistics Report' 121T *TIMX ( EM = HH:II:SS' 'AP ) // 046T '        Total            DA            IA           INS' / 046T '------------- ------------- ------------- -------------'
                        new ReportEditMask ("ZZ,ZZ9"),NEWLINE,new TabSetting(1),Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new TabSetting(53),"Monthly Statistics Report",new 
                        TabSetting(121),Global.getTIMX(), new ReportEditMask ("HH:II:SS' 'AP"),NEWLINE,NEWLINE,new TabSetting(46),"        Total            DA            IA           INS",NEWLINE,new 
                        TabSetting(46),"------------- ------------- ------------- -------------");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventRbc() throws Exception {atBreakEventRbc(false);}
    private void atBreakEventRbc(boolean endOfData) throws Exception
    {
        boolean bc_Bc_PinIsBreak = bc_Bc_Pin.isBreak(endOfData);
        if (condition(bc_Bc_PinIsBreak))
        {
            pnd_Grand_Pnd_Total_Pin.nadd(1);                                                                                                                              //Natural: ADD 1 TO #TOTAL-PIN
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=60 SG=OFF ZP=ON");
    }
}
