/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:20:11 PM
**        * FROM NATURAL PROGRAM : Benp9800
************************************************************
**        * FILE NAME            : Benp9800.java
**        * CLASS NAME           : Benp9800
**        * INSTANCE NAME        : Benp9800
************************************************************
************************************************************************
* PROGRAM  : BENP9800
* SYSTEM   : BENEFICIARY-SYSTEM
* TITLE    : INTERFACE DRIVER
* GENERATED: JUNE 29, 1999
*          :
* HISTORY
* CHANGED ON NOV 05,99 BY WEBBJ
* > ADD CALL TO FCPA115 TO GET NEXT BUSINESS DATE FOR BENN9820 EDIT.
* CHANGED ON DEC 03,99 BY WEBBJ
* > ADD CALL TO BENN9834 TO LOAD TABLE FILE TO TABLES.
* CHANGED ON AUG 30,00 BY WEBBJ
* > UPDATE PIN FROM COR IF ITS ZERO - NEGATIVE ENROLLMENT
* CHANGED ON OCT 04 BY WEBBJ
* > ONLINE STATUS TO 'M' IF CORR REJECT
* CHANGED ON NOV 28 BY WEBBJ
* > FLDR-LOG-DTE-TME TRANSFERED TO BENA9801 VIA BENN9815
* CHANGED ON DEC 18 BY WEBBJ
* 'INTERFACE-AS-MOS' FLAG TRANSFER TO BENN9802
*
* 04/07/2004 SOTTO    CHANGED CALL FROM FCPN119 TO CPWN119.
* 05/07/2005 KAKADIA  CHANGE ERROR ROUTINE TO LOG CWF AND ADD FOLDER
*                     BENN9881 AND BENN9883
* 08/04/2005 KAKADIA  COMMENT OUT CALL TO MIT.
************************************************************************
*    DATE      USERID                   DESCRIPTION
* MM/DD/YYYY  XXXXXXXX  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
* 07/16/2018  DURAND    REPLACE CPWN119 W/ NECN4000 - GET NEXT-BUSI-DATE
* 04/01/2017  DURAND    PIN EXPANSION N7 TO N12
************************************************************************

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benp9800 extends BLNatBase
{
    // Data Areas
    private PdaBenpda_M pdaBenpda_M;
    private PdaBenpda_E pdaBenpda_E;
    private PdaBena9801 pdaBena9801;
    private PdaBena9811 pdaBena9811;
    private PdaBena9817 pdaBena9817;
    private PdaBena9812 pdaBena9812;
    private PdaBena9800 pdaBena9800;
    private PdaBena9820 pdaBena9820;
    private PdaBena9810 pdaBena9810;
    private PdaBena9802 pdaBena9802;
    private PdaBena9821 pdaBena9821;
    private PdaBena9850 pdaBena9850;
    private PdaBena9815 pdaBena9815;
    private PdaBena5220 pdaBena5220;
    private PdaBena9834 pdaBena9834;
    private PdaBena5236 pdaBena5236;
    private PdaNeca4000 pdaNeca4000;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_I1;
    private DbsField pnd_Second_Cntrct_Retry;
    private DbsField pnd_Second_Pin_Retry;
    private DbsField pnd_Next_Record;
    private DbsField pnd_Next_Busi_Date;

    private DbsGroup pnd_Next_Busi_Date__R_Field_1;
    private DbsField pnd_Next_Busi_Date_Pnd_Next_Busi_Date_N8;

    private DbsGroup bena9831;
    private DbsField bena9831_Pnd_Busi_Date;

    private DbsGroup bena9831__R_Field_2;
    private DbsField bena9831_Pnd_Busi_Date_N;
    private DbsField pnd_Et_Cnt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaBenpda_M = new PdaBenpda_M(localVariables);
        pdaBenpda_E = new PdaBenpda_E(localVariables);
        pdaBena9801 = new PdaBena9801(localVariables);
        pdaBena9811 = new PdaBena9811(localVariables);
        pdaBena9817 = new PdaBena9817(localVariables);
        pdaBena9812 = new PdaBena9812(localVariables);
        pdaBena9800 = new PdaBena9800(localVariables);
        pdaBena9820 = new PdaBena9820(localVariables);
        pdaBena9810 = new PdaBena9810(localVariables);
        pdaBena9802 = new PdaBena9802(localVariables);
        pdaBena9821 = new PdaBena9821(localVariables);
        pdaBena9850 = new PdaBena9850(localVariables);
        pdaBena9815 = new PdaBena9815(localVariables);
        pdaBena5220 = new PdaBena5220(localVariables);
        pdaBena9834 = new PdaBena9834(localVariables);
        pdaBena5236 = new PdaBena5236(localVariables);
        pdaNeca4000 = new PdaNeca4000(localVariables);

        // Local Variables
        pnd_I1 = localVariables.newFieldInRecord("pnd_I1", "#I1", FieldType.PACKED_DECIMAL, 3);
        pnd_Second_Cntrct_Retry = localVariables.newFieldInRecord("pnd_Second_Cntrct_Retry", "#SECOND-CNTRCT-RETRY", FieldType.BOOLEAN, 1);
        pnd_Second_Pin_Retry = localVariables.newFieldInRecord("pnd_Second_Pin_Retry", "#SECOND-PIN-RETRY", FieldType.BOOLEAN, 1);
        pnd_Next_Record = localVariables.newFieldInRecord("pnd_Next_Record", "#NEXT-RECORD", FieldType.BOOLEAN, 1);
        pnd_Next_Busi_Date = localVariables.newFieldInRecord("pnd_Next_Busi_Date", "#NEXT-BUSI-DATE", FieldType.STRING, 8);

        pnd_Next_Busi_Date__R_Field_1 = localVariables.newGroupInRecord("pnd_Next_Busi_Date__R_Field_1", "REDEFINE", pnd_Next_Busi_Date);
        pnd_Next_Busi_Date_Pnd_Next_Busi_Date_N8 = pnd_Next_Busi_Date__R_Field_1.newFieldInGroup("pnd_Next_Busi_Date_Pnd_Next_Busi_Date_N8", "#NEXT-BUSI-DATE-N8", 
            FieldType.NUMERIC, 8);

        bena9831 = localVariables.newGroupInRecord("bena9831", "BENA9831");
        bena9831_Pnd_Busi_Date = bena9831.newFieldInGroup("bena9831_Pnd_Busi_Date", "#BUSI-DATE", FieldType.STRING, 8);

        bena9831__R_Field_2 = bena9831.newGroupInGroup("bena9831__R_Field_2", "REDEFINE", bena9831_Pnd_Busi_Date);
        bena9831_Pnd_Busi_Date_N = bena9831__R_Field_2.newFieldInGroup("bena9831_Pnd_Busi_Date_N", "#BUSI-DATE-N", FieldType.NUMERIC, 8);
        pnd_Et_Cnt = localVariables.newFieldInRecord("pnd_Et_Cnt", "#ET-CNT", FieldType.PACKED_DECIMAL, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Benp9800() throws Exception
    {
        super("Benp9800");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("BENP9800", onError);
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ERROR-INFO-SUB.##LAST-PROGRAM = *PROGRAM
        MAIN_ROUTINE:                                                                                                                                                     //Natural: REPEAT
        while (condition(whileTrue))
        {
            //*   GET THE BUSINESS DATE FOR THIS RUN
                                                                                                                                                                          //Natural: PERFORM GET-BUSINESS-DATE
            sub_Get_Business_Date();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("MAIN_ROUTINE"))) break;
                else if (condition(Global.isEscapeBottomImmediate("MAIN_ROUTINE"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  NEXT BUS DATE NEEDED FOR EDIT IN N9820
                                                                                                                                                                          //Natural: PERFORM GET-NEXT-BUSINESS-DATE
            sub_Get_Next_Business_Date();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("MAIN_ROUTINE"))) break;
                else if (condition(Global.isEscapeBottomImmediate("MAIN_ROUTINE"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*   /* JW
            //*  ENTRIES FROM TABLE FILE NEEDED FOR REL CODE &
                                                                                                                                                                          //Natural: PERFORM LOAD-TABLE-FILE
            sub_Load_Table_File();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("MAIN_ROUTINE"))) break;
                else if (condition(Global.isEscapeBottomImmediate("MAIN_ROUTINE"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  /* ETC. LOAD HERE ONCE AND PASS AROUND VIA BENA9820. JW 12/3/99
            REPEAT01:                                                                                                                                                     //Natural: REPEAT
            while (condition(whileTrue))
            {
                //*  GET THE NEXT RECORD FOR PROCESSING
                                                                                                                                                                          //Natural: PERFORM GET-NEXT-RECORD
                sub_Get_Next_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "PROCESSING",pdaBena9801.getBena9801_Pin_Tiaa_Cntrct());                                                                            //Natural: WRITE 'PROCESSING' BENA9801.PIN-TIAA-CNTRCT
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*   MAKE SURE THIS RECORD IS ON MDM.  IF NOT, PUT IN 'retry' STATUS
                                                                                                                                                                          //Natural: PERFORM CHECK-MDM
                sub_Check_Mdm();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  FAILED CHECK-MDM...GET NEXT RECORD.
                if (condition(pnd_Next_Record.getBoolean()))                                                                                                              //Natural: IF #NEXT-RECORD
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*   PERFORM EDITS
                                                                                                                                                                          //Natural: PERFORM EDITS
                sub_Edits();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  FAILED EDITS...GET NEXT RECORD.
                if (condition(pnd_Next_Record.getBoolean()))                                                                                                              //Natural: IF #NEXT-RECORD
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*   THIS DESIGNATION IS READY TO BE MOVED INTO THE BENE SYSTEM.
                //*   IF THIS IS A CHANGE, WE MUST DELETE THE EXISTING BENE RECORDS AND
                //*     PUT THEM ON THE HISTORY FILE.
                if (condition(pdaBena9801.getBena9801_Chng_New_Issue_Ind().equals("C")))                                                                                  //Natural: IF BENA9801.CHNG-NEW-ISSUE-IND = 'C'
                {
                                                                                                                                                                          //Natural: PERFORM DELETE-EXISTING-BENE-RECORDS
                    sub_Delete_Existing_Bene_Records();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-TO-BENE-RECORDS
                sub_Write_To_Bene_Records();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*   THIS INTERFACE WAS A SUCCESS
                getReports().write(0, "SUCCESS",pdaBena9801.getBena9801_Pin_Tiaa_Cntrct(),pdaBena9801.getBena9801_Rcrd_Crtd_For_Bsnss_Dte());                             //Natural: WRITE 'SUCCESS' BENA9801.PIN-TIAA-CNTRCT BENA9801.RCRD-CRTD-FOR-BSNSS-DTE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pdaBena9811.getBena9811_Pnd_New_Intrfce_Stts().setValue("S");                                                                                             //Natural: ASSIGN BENA9811.#NEW-INTRFCE-STTS = 'S'
                                                                                                                                                                          //Natural: PERFORM UPDATE-STATUS
                sub_Update_Status();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Et_Cnt.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #ET-CNT
                if (condition(pnd_Et_Cnt.greater(50)))                                                                                                                    //Natural: IF #ET-CNT GT 50
                {
                    pnd_Et_Cnt.reset();                                                                                                                                   //Natural: RESET #ET-CNT
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-REPEAT
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("MAIN_ROUTINE"))) break;
                else if (condition(Global.isEscapeBottomImmediate("MAIN_ROUTINE"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  MAIN-ROUTINE.
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
            //* *******************************
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DONE-FOR-TODAY
            //*  MAIN-ROUTINE.
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //* *********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DELETE-EXISTING-BENE-RECORDS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-BUSINESS-DATE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-NEXT-BUSINESS-DATE
        //* ***************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-TABLE-FILE
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TO-ERROR-TABLE
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TO-BENE-RECORDS
        //* ***********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-IF-SECOND-CONTRACT-RETRY
        //* ******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-IF-SECOND-PIN-RETRY
        //* **************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-MDM
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-NEXT-RECORD
        //* ***********************************************************************
        //* *************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-PIN-FROM-COR
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-STATUS
        //* **********************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EDITS
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: HANDLE-ERROR
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TERMINATE-PROCESS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STORE-WORK-REQUEST
        //* *BENA5220.#WPID-UNIT       := 'BENE'
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Done_For_Today() throws Exception                                                                                                                    //Natural: DONE-FOR-TODAY
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        //*  MAIN-ROUTINE.
        Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom);                                                                                                  //Natural: ESCAPE BOTTOM
        if (true) return;
        //*  DONE-FOR-TODAY
    }
    private void sub_Delete_Existing_Bene_Records() throws Exception                                                                                                      //Natural: DELETE-EXISTING-BENE-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************
        pdaBena9815.getBena9815_Pnd_Pin_Tiaa_Cntrct().setValue(pdaBena9801.getBena9801_Pin_Tiaa_Cntrct());                                                                //Natural: ASSIGN BENA9815.#PIN-TIAA-CNTRCT = BENA9801.PIN-TIAA-CNTRCT
        pdaBena9815.getBena9815_Pnd_Tiaa_Cref_Ind().setValue(pdaBena9801.getBena9801_Tiaa_Cref_Ind());                                                                    //Natural: ASSIGN BENA9815.#TIAA-CREF-IND = BENA9801.TIAA-CREF-IND
        DbsUtil.callnat(Benn9815.class , getCurrentProcessState(), pdaBena9815.getBena9815(), pdaBenpda_M.getMsg_Info_Sub(), pdaBenpda_E.getError_Info_Sub());            //Natural: CALLNAT 'BENN9815' BENA9815 MSG-INFO-SUB ERROR-INFO-SUB
        if (condition(Global.isEscape())) return;
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ##LAST-PROGRAM = *PROGRAM
        //*  JW 11/28/00
        if (condition(pdaBena9801.getBena9801_Rqstng_Systm().equals("CORR")))                                                                                             //Natural: IF BENA9801.RQSTNG-SYSTM = 'CORR'
        {
            pdaBena9801.getBena9801_Fldr_Log_Dte_Tme().setValue(pdaBena9815.getBena9815_Pnd_Fldr_Log_Dte_Tme());                                                          //Natural: ASSIGN BENA9801.FLDR-LOG-DTE-TME := BENA9815.#FLDR-LOG-DTE-TME
        }                                                                                                                                                                 //Natural: END-IF
        //*  DELETE-EXISTING-BENE-RECORDS
    }
    private void sub_Get_Business_Date() throws Exception                                                                                                                 //Natural: GET-BUSINESS-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        DbsUtil.callnat(Benn9831.class , getCurrentProcessState(), bena9831_Pnd_Busi_Date, pdaBenpda_M.getMsg_Info_Sub(), pdaBenpda_E.getError_Info_Sub());               //Natural: CALLNAT 'BENN9831' BENA9831.#BUSI-DATE MSG-INFO-SUB ERROR-INFO-SUB
        if (condition(Global.isEscape())) return;
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ##LAST-PROGRAM := *PROGRAM
        //*  GET-BUSINESS-DATE
    }
    private void sub_Get_Next_Business_Date() throws Exception                                                                                                            //Natural: GET-NEXT-BUSINESS-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pdaNeca4000.getNeca4000().reset();                                                                                                                                //Natural: RESET NECA4000
        pdaNeca4000.getNeca4000_Function_Cde().setValue("CAL");                                                                                                           //Natural: ASSIGN NECA4000.FUNCTION-CDE := 'CAL'
        pdaNeca4000.getNeca4000_Cal_Key_For_Dte().setValue(bena9831_Pnd_Busi_Date_N);                                                                                     //Natural: ASSIGN NECA4000.CAL-KEY-FOR-DTE := #BUSI-DATE-N
        DbsUtil.callnat(Necn4000.class , getCurrentProcessState(), pdaNeca4000.getNeca4000());                                                                            //Natural: CALLNAT 'NECN4000' NECA4000
        if (condition(Global.isEscape())) return;
        pnd_Next_Busi_Date_Pnd_Next_Busi_Date_N8.setValue(pdaNeca4000.getNeca4000_Cal_Next_Business_Dte().getValue(1));                                                   //Natural: ASSIGN #NEXT-BUSI-DATE-N8 := NECA4000.CAL-NEXT-BUSINESS-DTE ( 1 )
        getReports().write(0, "GET-NEXT-BUSINESS-DATE BusiDate",bena9831_Pnd_Busi_Date,"NextBusiDate",pnd_Next_Busi_Date);                                                //Natural: WRITE 'GET-NEXT-BUSINESS-DATE BusiDate' #BUSI-DATE 'NextBusiDate' #NEXT-BUSI-DATE
        if (Global.isEscape()) return;
        //*  GET-NEXT-BUSINESS-DATE
    }
    private void sub_Load_Table_File() throws Exception                                                                                                                   //Natural: LOAD-TABLE-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************
        pdaBena9834.getBena9834().reset();                                                                                                                                //Natural: RESET BENA9834
        DbsUtil.callnat(Benn9834.class , getCurrentProcessState(), pdaBena9834.getBena9834(), pdaBenpda_M.getMsg_Info_Sub(), pdaBenpda_E.getError_Info_Sub());            //Natural: CALLNAT 'BENN9834' BENA9834 MSG-INFO-SUB ERROR-INFO-SUB
        if (condition(Global.isEscape())) return;
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ##LAST-PROGRAM = *PROGRAM
        //*  LOAD-TABLE-FILE
    }
    private void sub_Write_To_Error_Table() throws Exception                                                                                                              //Natural: WRITE-TO-ERROR-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        pdaBena9812.getBena9812_Pnd_Cntrct_Isn().setValue(pdaBena9801.getBena9801_Pnd_Cntrct_Isn());                                                                      //Natural: ASSIGN BENA9812.#CNTRCT-ISN = BENA9801.#CNTRCT-ISN
        pdaBena9812.getBena9812_Pnd_Pin_Tiaa_Cntrct().setValue(pdaBena9801.getBena9801_Pin_Tiaa_Cntrct());                                                                //Natural: ASSIGN BENA9812.#PIN-TIAA-CNTRCT = BENA9801.PIN-TIAA-CNTRCT
        pdaBena9812.getBena9812_Pnd_Tiaa_Cref_Ind().setValue(pdaBena9801.getBena9801_Tiaa_Cref_Ind());                                                                    //Natural: ASSIGN BENA9812.#TIAA-CREF-IND = BENA9801.TIAA-CREF-IND
        pdaBena9812.getBena9812_Pnd_Rcrd_Crtd_For_Bsnss_Dte().setValue(pdaBena9801.getBena9801_Rcrd_Crtd_For_Bsnss_Dte());                                                //Natural: ASSIGN BENA9812.#RCRD-CRTD-FOR-BSNSS-DTE = BENA9801.RCRD-CRTD-FOR-BSNSS-DTE
        pdaBena9812.getBena9812_Pnd_Intrfce_Stts().setValue(pdaBena9801.getBena9801_Intrfce_Stts());                                                                      //Natural: ASSIGN BENA9812.#INTRFCE-STTS = BENA9801.INTRFCE-STTS
        pdaBena9812.getBena9812_Pnd_Bsnss_Dte().setValue(bena9831_Pnd_Busi_Date);                                                                                         //Natural: ASSIGN BENA9812.#BSNSS-DTE = BENA9831.#BUSI-DATE
        DbsUtil.callnat(Benn9812.class , getCurrentProcessState(), pdaBena9812.getBena9812(), pdaBenpda_M.getMsg_Info_Sub(), pdaBenpda_E.getError_Info_Sub());            //Natural: CALLNAT 'BENN9812' BENA9812 MSG-INFO-SUB ERROR-INFO-SUB
        if (condition(Global.isEscape())) return;
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ##LAST-PROGRAM = *PROGRAM
        //*  WRITE-TO-ERROR-TABLE
    }
    private void sub_Write_To_Bene_Records() throws Exception                                                                                                             //Natural: WRITE-TO-BENE-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        getReports().write(0, "WRITING TO BENE RECORDS");                                                                                                                 //Natural: WRITE 'WRITING TO BENE RECORDS'
        if (Global.isEscape()) return;
        pdaBena9802.getBena9802_Pnd_Cntrct_Isn().setValue(pdaBena9801.getBena9801_Pnd_Cntrct_Isn());                                                                      //Natural: ASSIGN BENA9802.#CNTRCT-ISN = BENA9801.#CNTRCT-ISN
        pdaBena9802.getBena9802_Pnd_Mos_Isn().setValue(pdaBena9820.getBena9820_Pnd_Mos_Isn());                                                                            //Natural: ASSIGN BENA9802.#MOS-ISN = BENA9820.#MOS-ISN
        pdaBena9802.getBena9802_Pnd_Dsgntn_Isn().getValue(1,":",30).setValue(pdaBena9820.getBena9820_Pnd_Dsgntn_Isn().getValue(1,":",30));                                //Natural: ASSIGN BENA9802.#DSGNTN-ISN ( 1:30 ) = BENA9820.#DSGNTN-ISN ( 1:30 )
        pdaBena9802.getBena9802_Pnd_Dsgntn_Rltn_Cde().getValue(1,":",30).setValue(pdaBena9820.getBena9820_Pnd_Dsgntn_Rltn_Cde().getValue(1,":",30));                      //Natural: ASSIGN BENA9802.#DSGNTN-RLTN-CDE ( 1:30 ) = BENA9820.#DSGNTN-RLTN-CDE ( 1:30 )
        //* JW1218
        pdaBena9802.getBena9802_Pnd_Interface_As_Mos().setValue(pdaBena9820.getBena9820_Pnd_Interface_As_Mos().getBoolean());                                             //Natural: ASSIGN BENA9802.#INTERFACE-AS-MOS = BENA9820.#INTERFACE-AS-MOS
        pdaBena9802.getBena9802_Pnd_Bsnss_Dte().setValue(bena9831_Pnd_Busi_Date);                                                                                         //Natural: ASSIGN BENA9802.#BSNSS-DTE = BENA9831.#BUSI-DATE
        DbsUtil.callnat(Benn9802.class , getCurrentProcessState(), pdaBena9802.getBena9802(), pdaBena9801.getBena9801(), pdaBenpda_M.getMsg_Info_Sub(),                   //Natural: CALLNAT 'BENN9802' BENA9802 BENA9801 MSG-INFO-SUB ERROR-INFO-SUB
            pdaBenpda_E.getError_Info_Sub());
        if (condition(Global.isEscape())) return;
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ##LAST-PROGRAM = *PROGRAM
        //*  WRITE-TO-BENE-RECORDS
    }
    private void sub_Check_If_Second_Contract_Retry() throws Exception                                                                                                    //Natural: CHECK-IF-SECOND-CONTRACT-RETRY
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************
        pnd_I1.reset();                                                                                                                                                   //Natural: RESET #I1 #SECOND-CNTRCT-RETRY
        pnd_Second_Cntrct_Retry.reset();
        DbsUtil.examine(new ExamineSource(pdaBena9801.getBena9801_Error_Cde().getValue("*")), new ExamineSearch("R"), new ExamineGivingNumber(pnd_I1));                   //Natural: EXAMINE BENA9801.ERROR-CDE ( * ) FOR 'R' GIVING NUMBER #I1
        if (condition(pnd_I1.equals(2)))                                                                                                                                  //Natural: IF #I1 EQ 2
        {
            pnd_Second_Cntrct_Retry.setValue(true);                                                                                                                       //Natural: ASSIGN #SECOND-CNTRCT-RETRY = TRUE
        }                                                                                                                                                                 //Natural: END-IF
        //*  CHECK-IF-SECOND-CONTRACT-RETRY
    }
    private void sub_Check_If_Second_Pin_Retry() throws Exception                                                                                                         //Natural: CHECK-IF-SECOND-PIN-RETRY
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************
        pnd_I1.reset();                                                                                                                                                   //Natural: RESET #I1 #SECOND-PIN-RETRY
        pnd_Second_Pin_Retry.reset();
        DbsUtil.examine(new ExamineSource(pdaBena9801.getBena9801_Error_Cde().getValue("*")), new ExamineSearch("PR"), new ExamineGivingNumber(pnd_I1));                  //Natural: EXAMINE BENA9801.ERROR-CDE ( * ) FOR 'PR' GIVING NUMBER #I1
        if (condition(pnd_I1.equals(2)))                                                                                                                                  //Natural: IF #I1 EQ 2
        {
            pnd_Second_Pin_Retry.setValue(true);                                                                                                                          //Natural: ASSIGN #SECOND-PIN-RETRY = TRUE
        }                                                                                                                                                                 //Natural: END-IF
        //*  CHECK-IF-SECOND-PIN-RETRY
    }
    private void sub_Check_Mdm() throws Exception                                                                                                                         //Natural: CHECK-MDM
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************
        //*  BENA9801
        pnd_Next_Record.reset();                                                                                                                                          //Natural: RESET #NEXT-RECORD #CNTRCT-FOUND BENA9850.#CNTRCT-ISSUE-DTE
        pdaBena9850.getBena9850_Pnd_Cntrct_Found().reset();
        pdaBena9850.getBena9850_Pnd_Cntrct_Issue_Dte().reset();
        pdaBena9850.getBena9850_Pnd_Cntrct().setValue(pdaBena9801.getBena9801_Tiaa_Cntrct());                                                                             //Natural: ASSIGN BENA9850.#CNTRCT = BENA9801.TIAA-CNTRCT
        DbsUtil.callnat(Benn9850.class , getCurrentProcessState(), pdaBena9850.getBena9850(), pdaBenpda_M.getMsg_Info_Sub(), pdaBenpda_E.getError_Info_Sub());            //Natural: CALLNAT 'BENN9850' BENA9850 MSG-INFO-SUB ERROR-INFO-SUB
        if (condition(Global.isEscape())) return;
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ##LAST-PROGRAM = *PROGRAM
        //*  ACIS NEG. ENROLL. MAY SEND 0 PIN. JW 8/30
        if (condition(pdaBena9801.getBena9801_Pin().equals(getZero()) && pdaBena9850.getBena9850_Pnd_Pin_Found().getBoolean()))                                           //Natural: IF BENA9801.PIN = 0 AND BENA9850.#PIN-FOUND
        {
                                                                                                                                                                          //Natural: PERFORM UPDATE-PIN-FROM-COR
            sub_Update_Pin_From_Cor();
            if (condition(Global.isEscape())) {return;}
            //*  JW 8/30
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet1610 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN NOT BENA9850.#CNTRCT-FOUND
        if (condition(! ((pdaBena9850.getBena9850_Pnd_Cntrct_Found().getBoolean()))))
        {
            decideConditionsMet1610++;
            //*   IF THE CONTRACT IS NOT ON MDM, PUT IT IN RETRY STATUS
            //*     OR FAIL STATUS IF IT's been retried twice already!
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-SECOND-CONTRACT-RETRY
            sub_Check_If_Second_Contract_Retry();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Second_Cntrct_Retry.getBoolean()))                                                                                                          //Natural: IF #SECOND-CNTRCT-RETRY
            {
                pdaBena9812.getBena9812_Pnd_Error_Txt().setValue(DbsUtil.compress("MDM access tried for third time; Contract:", pdaBena9801.getBena9801_Tiaa_Cntrct(),    //Natural: COMPRESS 'MDM access tried for third time; Contract:' BENA9801.TIAA-CNTRCT 'not on MDM' INTO BENA9812.#ERROR-TXT
                    "not on MDM"));
                pdaBena9812.getBena9812_Pnd_Error_Pgm().setValue(pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program());                                                   //Natural: ASSIGN BENA9812.#ERROR-PGM = ERROR-INFO-SUB.##LAST-PROGRAM
                pdaBena9811.getBena9811_Pnd_New_Intrfce_Stts().setValue("F");                                                                                             //Natural: ASSIGN BENA9811.#NEW-INTRFCE-STTS = 'F'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaBena9812.getBena9812_Pnd_Error_Txt().setValue(DbsUtil.compress("Contract: ", pdaBena9801.getBena9801_Tiaa_Cntrct(), "not on MDM"));                    //Natural: COMPRESS 'Contract: ' BENA9801.TIAA-CNTRCT 'not on MDM' INTO BENA9812.#ERROR-TXT
                pdaBena9812.getBena9812_Pnd_Error_Pgm().setValue(pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program());                                                   //Natural: ASSIGN BENA9812.#ERROR-PGM = ERROR-INFO-SUB.##LAST-PROGRAM
                pdaBena9812.getBena9812_Pnd_Error_Cde().setValue("R");                                                                                                    //Natural: ASSIGN BENA9812.#ERROR-CDE = 'R'
                pdaBena9811.getBena9811_Pnd_New_Intrfce_Stts().setValue("R");                                                                                             //Natural: ASSIGN BENA9811.#NEW-INTRFCE-STTS = 'R'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NOT BENA9850.#PIN-FOUND
        else if (condition(! ((pdaBena9850.getBena9850_Pnd_Pin_Found().getBoolean()))))
        {
            decideConditionsMet1610++;
            //*   IF THE PIN IS NOT ON MDM, PUT IT IN RETRY STATUS
            //*     OR FAIL STATUS IF IT's been retried twice already!
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-SECOND-PIN-RETRY
            sub_Check_If_Second_Pin_Retry();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Second_Cntrct_Retry.getBoolean()))                                                                                                          //Natural: IF #SECOND-CNTRCT-RETRY
            {
                pdaBena9812.getBena9812_Pnd_Error_Txt().setValue(DbsUtil.compress("MDM access tried for third time; PIN:", pdaBena9801.getBena9801_Pin(),                 //Natural: COMPRESS 'MDM access tried for third time; PIN:' BENA9801.PIN 'not on MDM' INTO BENA9812.#ERROR-TXT
                    "not on MDM"));
                pdaBena9812.getBena9812_Pnd_Error_Pgm().setValue(pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program());                                                   //Natural: ASSIGN BENA9812.#ERROR-PGM = ERROR-INFO-SUB.##LAST-PROGRAM
                pdaBena9811.getBena9811_Pnd_New_Intrfce_Stts().setValue("F");                                                                                             //Natural: ASSIGN BENA9811.#NEW-INTRFCE-STTS = 'F'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaBena9812.getBena9812_Pnd_Error_Txt().setValue(DbsUtil.compress("PIN:  ", pdaBena9801.getBena9801_Pin(), "not on MDM"));                                //Natural: COMPRESS 'PIN:  ' BENA9801.PIN 'not on MDM' INTO BENA9812.#ERROR-TXT
                pdaBena9812.getBena9812_Pnd_Error_Pgm().setValue(pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program());                                                   //Natural: ASSIGN BENA9812.#ERROR-PGM = ERROR-INFO-SUB.##LAST-PROGRAM
                pdaBena9812.getBena9812_Pnd_Error_Cde().setValue("PR");                                                                                                   //Natural: ASSIGN BENA9812.#ERROR-CDE = 'PR'
                pdaBena9811.getBena9811_Pnd_New_Intrfce_Stts().setValue("R");                                                                                             //Natural: ASSIGN BENA9811.#NEW-INTRFCE-STTS = 'R'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet1610 > 0))
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-TO-ERROR-TABLE
            sub_Write_To_Error_Table();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM UPDATE-STATUS
            sub_Update_Status();
            if (condition(Global.isEscape())) {return;}
            //*  JW 9/11/00
            pnd_Next_Record.setValue(true);                                                                                                                               //Natural: ASSIGN #NEXT-RECORD = TRUE
            pnd_Et_Cnt.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #ET-CNT
            if (condition(pnd_Et_Cnt.greater(50)))                                                                                                                        //Natural: IF #ET-CNT GT 50
            {
                pnd_Et_Cnt.reset();                                                                                                                                       //Natural: RESET #ET-CNT
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  CHECK-MDM
    }
    private void sub_Get_Next_Record() throws Exception                                                                                                                   //Natural: GET-NEXT-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pdaBena9800.getBena9800_Pnd_Bsnss_Dte().setValue(bena9831_Pnd_Busi_Date);                                                                                         //Natural: ASSIGN BENA9800.#BSNSS-DTE := BENA9831.#BUSI-DATE
        //*  CONTAINS CONTRACT RECORD FIELDS
        DbsUtil.callnat(Benn9800.class , getCurrentProcessState(), pdaBena9800.getBena9800(), pdaBena9801.getBena9801(), pdaBenpda_M.getMsg_Info_Sub(),                   //Natural: CALLNAT 'BENN9800' BENA9800 BENA9801 MSG-INFO-SUB ERROR-INFO-SUB
            pdaBenpda_E.getError_Info_Sub());
        if (condition(Global.isEscape())) return;
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ##LAST-PROGRAM = *PROGRAM
        if (condition(pdaBena9800.getBena9800_Pnd_Done_For_Today().getBoolean()))                                                                                         //Natural: IF BENA9800.#DONE-FOR-TODAY
        {
            getReports().write(0, "DONE FOR TODAY");                                                                                                                      //Natural: WRITE 'DONE FOR TODAY'
            if (Global.isEscape()) return;
            getReports().eject(0, true);                                                                                                                                  //Natural: EJECT
                                                                                                                                                                          //Natural: PERFORM DONE-FOR-TODAY
            sub_Done_For_Today();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  GET-NEXT-RECORD
    }
    //*  JW 8/30
    private void sub_Update_Pin_From_Cor() throws Exception                                                                                                               //Natural: UPDATE-PIN-FROM-COR
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************
        pdaBena9817.getBena9817_Pnd_New_Pin().setValue(pdaBena9850.getBena9850_Pnd_Pin());                                                                                //Natural: MOVE BENA9850.#PIN TO BENA9817.#NEW-PIN
        pdaBena9817.getBena9817_Pnd_Intrfce_Stts().setValue(pdaBena9801.getBena9801_Intrfce_Stts());                                                                      //Natural: ASSIGN BENA9817.#INTRFCE-STTS = BENA9801.INTRFCE-STTS
        pdaBena9817.getBena9817_Pnd_Cntrct_Isn().setValue(pdaBena9801.getBena9801_Pnd_Cntrct_Isn());                                                                      //Natural: ASSIGN BENA9817.#CNTRCT-ISN = BENA9801.#CNTRCT-ISN
        pdaBena9817.getBena9817_Pnd_Pin_Tiaa_Cntrct().setValue(pdaBena9801.getBena9801_Pin_Tiaa_Cntrct());                                                                //Natural: ASSIGN BENA9817.#PIN-TIAA-CNTRCT = BENA9801.PIN-TIAA-CNTRCT
        pdaBena9817.getBena9817_Pnd_Tiaa_Cref_Ind().setValue(pdaBena9801.getBena9801_Tiaa_Cref_Ind());                                                                    //Natural: ASSIGN BENA9817.#TIAA-CREF-IND = BENA9801.TIAA-CREF-IND
        pdaBena9817.getBena9817_Pnd_Rcrd_Crtd_For_Bsnss_Dte().setValue(pdaBena9801.getBena9801_Rcrd_Crtd_For_Bsnss_Dte());                                                //Natural: ASSIGN BENA9817.#RCRD-CRTD-FOR-BSNSS-DTE = BENA9801.RCRD-CRTD-FOR-BSNSS-DTE
        pdaBena9817.getBena9817_Pnd_Mos_Ind().setValue(pdaBena9801.getBena9801_Mos_Ind());                                                                                //Natural: ASSIGN BENA9817.#MOS-IND = BENA9801.MOS-IND
        DbsUtil.callnat(Benn9817.class , getCurrentProcessState(), pdaBena9817.getBena9817(), pdaBenpda_M.getMsg_Info_Sub(), pdaBenpda_E.getError_Info_Sub());            //Natural: CALLNAT 'BENN9817' BENA9817 MSG-INFO-SUB ERROR-INFO-SUB
        if (condition(Global.isEscape())) return;
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ##LAST-PROGRAM = *PROGRAM
        pdaBena9801.getBena9801_Pin().setValue(pdaBena9850.getBena9850_Pnd_Pin());                                                                                        //Natural: MOVE BENA9850.#PIN TO BENA9801.PIN
        //*  UPDATE-PIN-FROM-COR JW 8/30
    }
    private void sub_Update_Status() throws Exception                                                                                                                     //Natural: UPDATE-STATUS
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        pdaBena9811.getBena9811_Pnd_Intrfce_Stts().setValue(pdaBena9801.getBena9801_Intrfce_Stts());                                                                      //Natural: ASSIGN BENA9811.#INTRFCE-STTS = BENA9801.INTRFCE-STTS
        pdaBena9811.getBena9811_Pnd_Cntrct_Isn().setValue(pdaBena9801.getBena9801_Pnd_Cntrct_Isn());                                                                      //Natural: ASSIGN BENA9811.#CNTRCT-ISN = BENA9801.#CNTRCT-ISN
        pdaBena9811.getBena9811_Pnd_Pin_Tiaa_Cntrct().setValue(pdaBena9801.getBena9801_Pin_Tiaa_Cntrct());                                                                //Natural: ASSIGN BENA9811.#PIN-TIAA-CNTRCT = BENA9801.PIN-TIAA-CNTRCT
        pdaBena9811.getBena9811_Pnd_Tiaa_Cref_Ind().setValue(pdaBena9801.getBena9801_Tiaa_Cref_Ind());                                                                    //Natural: ASSIGN BENA9811.#TIAA-CREF-IND = BENA9801.TIAA-CREF-IND
        pdaBena9811.getBena9811_Pnd_Rcrd_Crtd_For_Bsnss_Dte().setValue(pdaBena9801.getBena9801_Rcrd_Crtd_For_Bsnss_Dte());                                                //Natural: ASSIGN BENA9811.#RCRD-CRTD-FOR-BSNSS-DTE = BENA9801.RCRD-CRTD-FOR-BSNSS-DTE
        pdaBena9811.getBena9811_Pnd_Mos_Ind().setValue(pdaBena9801.getBena9801_Mos_Ind());                                                                                //Natural: ASSIGN BENA9811.#MOS-IND = BENA9801.MOS-IND
        pdaBena9811.getBena9811_Pnd_Bsnss_Dte().setValue(bena9831_Pnd_Busi_Date);                                                                                         //Natural: ASSIGN BENA9811.#BSNSS-DTE = BENA9831.#BUSI-DATE
        pdaBena9811.getBena9811_Pnd_Intrfce_Stts().setValue(pdaBena9801.getBena9801_Intrfce_Stts());                                                                      //Natural: ASSIGN BENA9811.#INTRFCE-STTS = BENA9801.INTRFCE-STTS
        DbsUtil.callnat(Benn9811.class , getCurrentProcessState(), pdaBena9811.getBena9811(), pdaBenpda_M.getMsg_Info_Sub(), pdaBenpda_E.getError_Info_Sub());            //Natural: CALLNAT 'BENN9811' BENA9811 MSG-INFO-SUB ERROR-INFO-SUB
        if (condition(Global.isEscape())) return;
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ##LAST-PROGRAM = *PROGRAM
        //*  UPDATE-STATUS
    }
    private void sub_Edits() throws Exception                                                                                                                             //Natural: EDITS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************
        pnd_Next_Record.reset();                                                                                                                                          //Natural: RESET #NEXT-RECORD BENA9821 BENA9820 ##RETURN-CODE
        pdaBena9821.getBena9821().reset();
        pdaBena9820.getBena9820().reset();
        pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().reset();
        //*   INTERFACE SPECIFIC EDITS
        DbsUtil.callnat(Benn9821.class , getCurrentProcessState(), pdaBena9801.getBena9801(), pdaBena9821.getBena9821(), pdaBenpda_M.getMsg_Info_Sub(),                   //Natural: CALLNAT 'BENN9821' BENA9801 BENA9821 MSG-INFO-SUB ERROR-INFO-SUB
            pdaBenpda_E.getError_Info_Sub());
        if (condition(Global.isEscape())) return;
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ##LAST-PROGRAM = *PROGRAM
        if (condition(pdaBena9821.getBena9821_Pnd_Edit_Failed().getBoolean()))                                                                                            //Natural: IF BENA9821.#EDIT-FAILED
        {
            pdaBena9812.getBena9812_Pnd_Error_Txt().setValue(pdaBena9821.getBena9821_Pnd_Error_Msg());                                                                    //Natural: ASSIGN BENA9812.#ERROR-TXT = BENA9821.#ERROR-MSG
            pdaBena9812.getBena9812_Pnd_Error_Pgm().setValue(pdaBena9821.getBena9821_Pnd_Error_Pgm());                                                                    //Natural: ASSIGN BENA9812.#ERROR-PGM = BENA9821.#ERROR-PGM
            pdaBena9812.getBena9812_Pnd_Error_Cde().setValue(pdaBena9821.getBena9821_Pnd_Error_Cde());                                                                    //Natural: ASSIGN BENA9812.#ERROR-CDE = BENA9821.#ERROR-CDE
                                                                                                                                                                          //Natural: PERFORM HANDLE-ERROR
            sub_Handle_Error();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("T")))                                                                                     //Natural: IF ##RETURN-CODE = 'T'
        {
                                                                                                                                                                          //Natural: PERFORM TERMINATE-PROCESS
            sub_Terminate_Process();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*   BENE SPECIFIC EDITS
        pdaBena9820.getBena9820_Pnd_Bsnss_Dte().setValue(bena9831_Pnd_Busi_Date);                                                                                         //Natural: ASSIGN BENA9820.#BSNSS-DTE = BENA9831.#BUSI-DATE
        pdaBena9820.getBena9820_Pnd_Next_Bsnss_Dte().setValue(pdaNeca4000.getNeca4000_Cal_Next_Business_Dte().getValue(1));                                               //Natural: ASSIGN BENA9820.#NEXT-BSNSS-DTE = NECA4000.CAL-NEXT-BUSINESS-DTE ( 1 )
        pdaBena9820.getBena9820_Pnd_Cntrct_Lob_Cde().setValue(pdaBena9850.getBena9850_Pnd_Cntrct_Lob_Cde());                                                              //Natural: ASSIGN BENA9820.#CNTRCT-LOB-CDE = BENA9850.#CNTRCT-LOB-CDE
        pdaBena9820.getBena9820_Pnd_Cntrct_Nmbr_Range_Cde().setValue(pdaBena9850.getBena9850_Pnd_Cntrct_Nmbr_Range_Cde());                                                //Natural: ASSIGN BENA9820.#CNTRCT-NMBR-RANGE-CDE = BENA9850.#CNTRCT-NMBR-RANGE-CDE
        pdaBena9820.getBena9820_Pnd_Cntrct_Spcl_Cnsdrtn_Cde().setValue(pdaBena9850.getBena9850_Pnd_Cntrct_Spcl_Cnsdrtn_Cde());                                            //Natural: ASSIGN BENA9820.#CNTRCT-SPCL-CNSDRTN-CDE = BENA9850.#CNTRCT-SPCL-CNSDRTN-CDE
        pdaBena9820.getBena9820_Pnd_Cref_Cntrct_Nmbr().setValue(pdaBena9850.getBena9850_Pnd_Cntrct_Cref_Nbr());                                                           //Natural: ASSIGN BENA9820.#CREF-CNTRCT-NMBR = BENA9850.#CNTRCT-CREF-NBR
        DbsUtil.callnat(Benn9820.class , getCurrentProcessState(), pdaBena9820.getBena9820(), pdaBena9801.getBena9801(), pdaBena9834.getBena9834(), pdaBenpda_M.getMsg_Info_Sub(),  //Natural: CALLNAT 'BENN9820' BENA9820 BENA9801 BENA9834 MSG-INFO-SUB ERROR-INFO-SUB
            pdaBenpda_E.getError_Info_Sub());
        if (condition(Global.isEscape())) return;
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ##LAST-PROGRAM = *PROGRAM
        if (condition(pdaBena9820.getBena9820_Pnd_Edit_Failed().getBoolean()))                                                                                            //Natural: IF BENA9820.#EDIT-FAILED
        {
            pdaBena9812.getBena9812_Pnd_Error_Txt().setValue(pdaBena9820.getBena9820_Pnd_Error_Msg());                                                                    //Natural: ASSIGN BENA9812.#ERROR-TXT = BENA9820.#ERROR-MSG
            pdaBena9812.getBena9812_Pnd_Error_Pgm().setValue(pdaBena9820.getBena9820_Pnd_Error_Pgm());                                                                    //Natural: ASSIGN BENA9812.#ERROR-PGM = BENA9820.#ERROR-PGM
            pdaBena9812.getBena9812_Pnd_Error_Cde().setValue(pdaBena9820.getBena9820_Pnd_Error_Cde());                                                                    //Natural: ASSIGN BENA9812.#ERROR-CDE = BENA9820.#ERROR-CDE
                                                                                                                                                                          //Natural: PERFORM HANDLE-ERROR
            sub_Handle_Error();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //* *  END TRANSACTION
        //*  EDITS
    }
    private void sub_Handle_Error() throws Exception                                                                                                                      //Natural: HANDLE-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        getReports().write(0, "**************************************************",NEWLINE,"ERROR FOR",pdaBena9801.getBena9801_Pin_Tiaa_Cntrct(),NEWLINE,                 //Natural: WRITE '**************************************************' / 'ERROR FOR' BENA9801.PIN-TIAA-CNTRCT / '=' BENA9812.#ERROR-TXT / '=' BENA9812.#ERROR-PGM / '=' BENA9812.#ERROR-CDE
            "=",pdaBena9812.getBena9812_Pnd_Error_Txt(),NEWLINE,"=",pdaBena9812.getBena9812_Pnd_Error_Pgm(),NEWLINE,"=",pdaBena9812.getBena9812_Pnd_Error_Cde());
        if (Global.isEscape()) return;
        getReports().write(0, "**************************************************");                                                                                      //Natural: WRITE '**************************************************'
        if (Global.isEscape()) return;
        //* **PERFORM  STORE-WORK-REQUEST /* HK COMMENTED OUT THE CALL 08/05/05
        pnd_Next_Record.setValue(true);                                                                                                                                   //Natural: ASSIGN #NEXT-RECORD = TRUE
                                                                                                                                                                          //Natural: PERFORM WRITE-TO-ERROR-TABLE
        sub_Write_To_Error_Table();
        if (condition(Global.isEscape())) {return;}
        pdaBena9811.getBena9811_Pnd_New_Intrfce_Stts().setValue("E");                                                                                                     //Natural: ASSIGN BENA9811.#NEW-INTRFCE-STTS = 'E'
                                                                                                                                                                          //Natural: PERFORM UPDATE-STATUS
        sub_Update_Status();
        if (condition(Global.isEscape())) {return;}
        //*  IF AN EDIT FAILED AND IT's an MDO change, update the BENE-FILES with
        //*   A STATUS OF 'M'
        if (condition((pdaBena9801.getBena9801_Rqstng_Systm().equals("CORR")) && pdaBena9801.getBena9801_Chng_New_Issue_Ind().equals("C")))                               //Natural: IF ( BENA9801.RQSTNG-SYSTM = 'CORR' ) AND BENA9801.CHNG-NEW-ISSUE-IND = 'C'
        {
            pdaBena9810.getBena9810_Pnd_Pin_Tiaa_Cntrct().setValue(pdaBena9801.getBena9801_Pin_Tiaa_Cntrct());                                                            //Natural: ASSIGN BENA9810.#PIN-TIAA-CNTRCT = BENA9801.PIN-TIAA-CNTRCT
            //*  ASSIGN BENA9810.#TIAA-CREF-IND   = BENA9801.TIAA-CREF-IND JW 11/7/00
            //*  ALL RECORDS (1 OR 2) UPDATED, REGARDLESS OF TIAA-CREF-IND (CORR ONLY)
            pdaBena9810.getBena9810_Pnd_Old_Stts().setValue("C");                                                                                                         //Natural: ASSIGN BENA9810.#OLD-STTS = 'C'
            pdaBena9810.getBena9810_Pnd_New_Stts().setValue("M");                                                                                                         //Natural: ASSIGN BENA9810.#NEW-STTS = 'M'
            DbsUtil.callnat(Benn9810.class , getCurrentProcessState(), pdaBena9810.getBena9810(), pdaBenpda_M.getMsg_Info_Sub(), pdaBenpda_E.getError_Info_Sub());        //Natural: CALLNAT 'BENN9810' BENA9810 MSG-INFO-SUB ERROR-INFO-SUB
            if (condition(Global.isEscape())) return;
            pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                           //Natural: ASSIGN ##LAST-PROGRAM = *PROGRAM
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Et_Cnt.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #ET-CNT
        if (condition(pnd_Et_Cnt.greater(50)))                                                                                                                            //Natural: IF #ET-CNT GT 50
        {
            pnd_Et_Cnt.reset();                                                                                                                                           //Natural: RESET #ET-CNT
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
        //*  HANDLE-ERROR
    }
    private void sub_Terminate_Process() throws Exception                                                                                                                 //Natural: TERMINATE-PROCESS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        getReports().write(0, "*************************************************************");                                                                           //Natural: WRITE '*************************************************************'
        if (Global.isEscape()) return;
        getReports().write(0, "*************************************************************");                                                                           //Natural: WRITE '*************************************************************'
        if (Global.isEscape()) return;
        getReports().write(0, "*                                                           *");                                                                           //Natural: WRITE '*                                                           *'
        if (Global.isEscape()) return;
        getReports().write(0, "*   E   R   R   O   R      O   C   C   U   R   R   E   D ! !*");                                                                           //Natural: WRITE '*   E   R   R   O   R      O   C   C   U   R   R   E   D ! !*'
        if (Global.isEscape()) return;
        getReports().write(0, "*                                                           *");                                                                           //Natural: WRITE '*                                                           *'
        if (Global.isEscape()) return;
        getReports().write(0, "*************************************************************");                                                                           //Natural: WRITE '*************************************************************'
        if (Global.isEscape()) return;
        getReports().write(0, "*************************************************************");                                                                           //Natural: WRITE '*************************************************************'
        if (Global.isEscape()) return;
        getReports().write(0, "*                                                           *");                                                                           //Natural: WRITE '*                                                           *'
        if (Global.isEscape()) return;
        getReports().write(0, "*",pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                                             //Natural: WRITE '*' ##MSG
        if (Global.isEscape()) return;
        getReports().write(0, "* PROGRAM......:",pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program());                                                                   //Natural: WRITE '* PROGRAM......:' ##LAST-PROGRAM
        if (Global.isEscape()) return;
        getReports().write(0, "* ERROR NBR....:",Global.getERROR_NR());                                                                                                   //Natural: WRITE '* ERROR NBR....:' *ERROR-NR
        if (Global.isEscape()) return;
        getReports().write(0, "* ERROR LINE...:",Global.getERROR_LINE());                                                                                                 //Natural: WRITE '* ERROR LINE...:' *ERROR-LINE
        if (Global.isEscape()) return;
        getReports().write(0, "*                                                           *");                                                                           //Natural: WRITE '*                                                           *'
        if (Global.isEscape()) return;
        getReports().write(0, "* CONTACT SYSTEMS!                                          *");                                                                           //Natural: WRITE '* CONTACT SYSTEMS!                                          *'
        if (Global.isEscape()) return;
        getReports().write(0, "*                                                           *");                                                                           //Natural: WRITE '*                                                           *'
        if (Global.isEscape()) return;
        getReports().write(0, "*************************************************************");                                                                           //Natural: WRITE '*************************************************************'
        if (Global.isEscape()) return;
        getReports().write(0, "*************************************************************");                                                                           //Natural: WRITE '*************************************************************'
        if (Global.isEscape()) return;
        DbsUtil.terminate(33);  if (true) return;                                                                                                                         //Natural: TERMINATE 33
        //*  TERMINATE-PROCESS
    }
    private void sub_Store_Work_Request() throws Exception                                                                                                                //Natural: STORE-WORK-REQUEST
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pdaBena5236.getBena5236().reset();                                                                                                                                //Natural: RESET BENA5236
        pdaBena5236.getBena5236_Pnd_Pin().setValue(pdaBena9850.getBena9850_Pnd_Pin());                                                                                    //Natural: ASSIGN BENA5236.#PIN := BENA9850.#PIN
        pdaBena5236.getBena5236_Pnd_Tiaa_Cntrct().setValue(pdaBena9850.getBena9850_Pnd_Cntrct());                                                                         //Natural: ASSIGN BENA5236.#TIAA-CNTRCT := BENA9850.#CNTRCT
        pdaBena5236.getBena5236_Pnd_Lob_Cde().setValue(pdaBena9850.getBena9850_Pnd_Cntrct_Lob_Cde());                                                                     //Natural: ASSIGN BENA5236.#LOB-CDE := BENA9850.#CNTRCT-LOB-CDE
        DbsUtil.callnat(Benn5236.class , getCurrentProcessState(), pdaBena5236.getBena5236(), pdaBenpda_M.getMsg_Info_Sub(), pdaBenpda_E.getError_Info_Sub());            //Natural: CALLNAT 'BENN5236' BENA5236 MSG-INFO-SUB ERROR-INFO-SUB
        if (condition(Global.isEscape())) return;
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ERROR-INFO-SUB.##LAST-PROGRAM = *PROGRAM
        //*  FIRST CALL STATUS = 0
        //*  JW 09/19
        pdaBena5220.getBena5220().reset();                                                                                                                                //Natural: RESET BENA5220
        pdaBena5220.getBena5220_Pnd_Wpid_Pin().setValue(pdaBena9850.getBena9850_Pnd_Pin());                                                                               //Natural: ASSIGN BENA5220.#WPID-PIN := BENA9850.#PIN
        pdaBena5220.getBena5220_Pnd_Wpid_System().setValue("BEN");                                                                                                        //Natural: ASSIGN BENA5220.#WPID-SYSTEM := 'BEN'
        pdaBena5220.getBena5220_Pnd_Et_Ind().setValue("N");                                                                                                               //Natural: ASSIGN BENA5220.#ET-IND := 'N'
        pdaBena5220.getBena5220_Pnd_Wpid_Status().setValue("0000");                                                                                                       //Natural: ASSIGN BENA5220.#WPID-STATUS := '0000'
        pdaBena5220.getBena5220_Pnd_Empl_Racf_Id().setValue("   ");                                                                                                       //Natural: ASSIGN BENA5220.#EMPL-RACF-ID := '   '
        pdaBena5220.getBena5220_Pnd_User_Id().setValue("BENEWEB");                                                                                                        //Natural: ASSIGN BENA5220.#USER-ID := 'BENEWEB'
        pdaBena5220.getBena5220_Pnd_Wpid().setValue(pdaBena5236.getBena5236_Pnd_Wpid());                                                                                  //Natural: ASSIGN BENA5220.#WPID := BENA5236.#WPID
        pdaBena5220.getBena5220_Pnd_Wpid_Unit().setValue("CTADMW");                                                                                                       //Natural: ASSIGN BENA5220.#WPID-UNIT := 'CTADMW'
        pdaBena5220.getBena5220_Pnd_Wpid_Contracts().getValue(1).setValue(pdaBena9850.getBena9850_Pnd_Cntrct());                                                          //Natural: ASSIGN BENA5220.#WPID-CONTRACTS ( 1 ) := BENA9850.#CNTRCT
        getReports().write(0, "going to call BENN9881 with BENA5220 PDA",NEWLINE,"=",pdaBena5220.getBena5220_Pnd_Wpid_Pin(),NEWLINE,"=",pdaBena5220.getBena5220_Pnd_Wpid_System(), //Natural: WRITE 'going to call BENN9881 with BENA5220 PDA' / '=' BENA5220.#WPID-PIN / '=' BENA5220.#WPID-SYSTEM / '=' BENA5220.#ET-IND / '=' BENA5220.#WPID-STATUS / '=' BENA5220.#EMPL-RACF-ID / '=' BENA5220.#USER-ID / '=' BENA5220.#WPID / '=' BENA5220.#WPID-UNIT / '=' BENA5220.#WPID-CONTRACTS ( * )
            NEWLINE,"=",pdaBena5220.getBena5220_Pnd_Et_Ind(),NEWLINE,"=",pdaBena5220.getBena5220_Pnd_Wpid_Status(),NEWLINE,"=",pdaBena5220.getBena5220_Pnd_Empl_Racf_Id(),
            NEWLINE,"=",pdaBena5220.getBena5220_Pnd_User_Id(),NEWLINE,"=",pdaBena5220.getBena5220_Pnd_Wpid(),NEWLINE,"=",pdaBena5220.getBena5220_Pnd_Wpid_Unit(),
            NEWLINE,"=",pdaBena5220.getBena5220_Pnd_Wpid_Contracts().getValue("*"));
        if (Global.isEscape()) return;
        DbsUtil.callnat(Benn9881.class , getCurrentProcessState(), pdaBena5220.getBena5220());                                                                            //Natural: CALLNAT 'BENN9881' BENA5220
        if (condition(Global.isEscape())) return;
        if (condition(pdaBena5220.getBena5220_Pnd_Error_Msg().notEquals(" ")))                                                                                            //Natural: IF BENA5220.#ERROR-MSG NE ' '
        {
            pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue("BENN5220");                                                                                    //Natural: ASSIGN ##LAST-PROGRAM := 'BENN5220'
            pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(pdaBena5220.getBena5220_Pnd_Error_Msg());                                                                  //Natural: ASSIGN ##MSG := BENA5220.#ERROR-MSG
            pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                              //Natural: ASSIGN ##RETURN-CODE := 'E'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Benn9883.class , getCurrentProcessState(), pdaBena9801.getBena9801(), pdaBena5220.getBena5220(), pdaBena9812.getBena9812());                      //Natural: CALLNAT 'BENN9883' BENA9801 BENA5220 BENA9812
        if (condition(Global.isEscape())) return;
        //*  CF 01/17/02
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ##LAST-PROGRAM = *PROGRAM
        //* *#OUTPUT-LOG-DATE-TIME     := BENA5220.#RETURN-LOG-DTE-TME
        //* *#OUTPUT-WPID              := BENA5220.#WPID
        //* *#OUTPUT-WPID-UNIT         := BENA5220.#WPID-UNIT
        //*  STORE-WORK-REQUEST
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
                                                                                                                                                                          //Natural: PERFORM TERMINATE-PROCESS
        sub_Terminate_Process();
        if (condition(Global.isEscape())) {return;}
    };                                                                                                                                                                    //Natural: END-ERROR
}
