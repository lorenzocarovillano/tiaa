/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:20:23 PM
**        * FROM NATURAL PROGRAM : Benp9830
************************************************************
**        * FILE NAME            : Benp9830.java
**        * CLASS NAME           : Benp9830
**        * INSTANCE NAME        : Benp9830
************************************************************
************************************************************************
* PROGRAM  : BENP9831
* SYSTEM   : BENEFICIARY-SYSTEM
* TITLE    : UPDATE THE ZZ INTERFACE DATE RECORD
* GENERATED: JUNE 29, 1999
*          : UPDATE THE ZZ INTERFACE DATE
************************************************************************
*    DATE      USERID                   DESCRIPTION
* MM/DD/YYYY  XXXXXXXX  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
* 07/16/2018  DURAND    REPLACE CPWN119 W/ NECN4000 - GET NEXT-BUSI-DATE
************************************************************************

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benp9830 extends BLNatBase
{
    // Data Areas
    private PdaNeca4000 pdaNeca4000;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_bene_Table_File;
    private DbsField bene_Table_File_Bt_Table_Id;
    private DbsField bene_Table_File_Bt_Table_Key;
    private DbsField bene_Table_File_Bt_Table_Text;
    private DbsField bene_Table_File_Bt_Rcrd_Last_Updt_Dte;
    private DbsField bene_Table_File_Bt_Rcrd_Last_Updt_Tme;
    private DbsField bene_Table_File_Bt_Rcrd_Last_Updt_Userid;
    private DbsField pnd_Bt_Table_Id_Key;

    private DbsGroup pnd_Bt_Table_Id_Key__R_Field_1;
    private DbsField pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Id;
    private DbsField pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Key;
    private DbsField pnd_Bsnss_Dte;

    private DbsGroup pnd_Bsnss_Dte__R_Field_2;
    private DbsField pnd_Bsnss_Dte_Pnd_Bsnss_Dte_N;
    private DbsField pnd_Next_Bsnss_Dte;

    private DbsGroup pnd_Next_Bsnss_Dte__R_Field_3;
    private DbsField pnd_Next_Bsnss_Dte_Pnd_Next_Bsnss_Dte_N8;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaNeca4000 = new PdaNeca4000(localVariables);

        // Local Variables

        vw_bene_Table_File = new DataAccessProgramView(new NameInfo("vw_bene_Table_File", "BENE-TABLE-FILE"), "BENE_TABLE_FILE", "BENE_TABLE");
        bene_Table_File_Bt_Table_Id = vw_bene_Table_File.getRecord().newFieldInGroup("bene_Table_File_Bt_Table_Id", "BT-TABLE-ID", FieldType.STRING, 2, 
            RepeatingFieldStrategy.None, "BT_TABLE_ID");
        bene_Table_File_Bt_Table_Id.setDdmHeader("TABLE/ID");
        bene_Table_File_Bt_Table_Key = vw_bene_Table_File.getRecord().newFieldInGroup("bene_Table_File_Bt_Table_Key", "BT-TABLE-KEY", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "BT_TABLE_KEY");
        bene_Table_File_Bt_Table_Key.setDdmHeader("TABLE/KEY");
        bene_Table_File_Bt_Table_Text = vw_bene_Table_File.getRecord().newFieldInGroup("bene_Table_File_Bt_Table_Text", "BT-TABLE-TEXT", FieldType.STRING, 
            72, RepeatingFieldStrategy.None, "BT_TABLE_TEXT");
        bene_Table_File_Bt_Table_Text.setDdmHeader("TABLE/TEXT");
        bene_Table_File_Bt_Rcrd_Last_Updt_Dte = vw_bene_Table_File.getRecord().newFieldInGroup("bene_Table_File_Bt_Rcrd_Last_Updt_Dte", "BT-RCRD-LAST-UPDT-DTE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "BT_RCRD_LAST_UPDT_DTE");
        bene_Table_File_Bt_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bene_Table_File_Bt_Rcrd_Last_Updt_Tme = vw_bene_Table_File.getRecord().newFieldInGroup("bene_Table_File_Bt_Rcrd_Last_Updt_Tme", "BT-RCRD-LAST-UPDT-TME", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "BT_RCRD_LAST_UPDT_TME");
        bene_Table_File_Bt_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bene_Table_File_Bt_Rcrd_Last_Updt_Userid = vw_bene_Table_File.getRecord().newFieldInGroup("bene_Table_File_Bt_Rcrd_Last_Updt_Userid", "BT-RCRD-LAST-UPDT-USERID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "BT_RCRD_LAST_UPDT_USERID");
        bene_Table_File_Bt_Rcrd_Last_Updt_Userid.setDdmHeader("RECRD LAST/UPDATE/USER ID");
        registerRecord(vw_bene_Table_File);

        pnd_Bt_Table_Id_Key = localVariables.newFieldInRecord("pnd_Bt_Table_Id_Key", "#BT-TABLE-ID-KEY", FieldType.STRING, 22);

        pnd_Bt_Table_Id_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Bt_Table_Id_Key__R_Field_1", "REDEFINE", pnd_Bt_Table_Id_Key);
        pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Id = pnd_Bt_Table_Id_Key__R_Field_1.newFieldInGroup("pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Id", "#BT-TABLE-ID", FieldType.STRING, 
            2);
        pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Key = pnd_Bt_Table_Id_Key__R_Field_1.newFieldInGroup("pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Key", "#BT-TABLE-KEY", 
            FieldType.STRING, 20);
        pnd_Bsnss_Dte = localVariables.newFieldInRecord("pnd_Bsnss_Dte", "#BSNSS-DTE", FieldType.STRING, 8);

        pnd_Bsnss_Dte__R_Field_2 = localVariables.newGroupInRecord("pnd_Bsnss_Dte__R_Field_2", "REDEFINE", pnd_Bsnss_Dte);
        pnd_Bsnss_Dte_Pnd_Bsnss_Dte_N = pnd_Bsnss_Dte__R_Field_2.newFieldInGroup("pnd_Bsnss_Dte_Pnd_Bsnss_Dte_N", "#BSNSS-DTE-N", FieldType.NUMERIC, 8);
        pnd_Next_Bsnss_Dte = localVariables.newFieldInRecord("pnd_Next_Bsnss_Dte", "#NEXT-BSNSS-DTE", FieldType.STRING, 8);

        pnd_Next_Bsnss_Dte__R_Field_3 = localVariables.newGroupInRecord("pnd_Next_Bsnss_Dte__R_Field_3", "REDEFINE", pnd_Next_Bsnss_Dte);
        pnd_Next_Bsnss_Dte_Pnd_Next_Bsnss_Dte_N8 = pnd_Next_Bsnss_Dte__R_Field_3.newFieldInGroup("pnd_Next_Bsnss_Dte_Pnd_Next_Bsnss_Dte_N8", "#NEXT-BSNSS-DTE-N8", 
            FieldType.NUMERIC, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_bene_Table_File.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Benp9830() throws Exception
    {
        super("Benp9830");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("BENP9830", onError);
        pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Id.setValue("ZZ");                                                                                                               //Natural: ASSIGN #BT-TABLE-ID := 'ZZ'
        pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Key.setValue("INTERFACE-DATE");                                                                                                  //Natural: ASSIGN #BT-TABLE-KEY := 'INTERFACE-DATE'
        vw_bene_Table_File.startDatabaseFind                                                                                                                              //Natural: FIND ( 1 ) BENE-TABLE-FILE WITH BT-TABLE-ID-KEY = #BT-TABLE-ID-KEY
        (
        "FBT",
        new Wc[] { new Wc("BT_TABLE_ID_KEY", "=", pnd_Bt_Table_Id_Key, WcType.WITH) },
        1
        );
        FBT:
        while (condition(vw_bene_Table_File.readNextRow("FBT", true)))
        {
            vw_bene_Table_File.setIfNotFoundControlFlag(false);
            if (condition(vw_bene_Table_File.getAstCOUNTER().equals(0)))                                                                                                  //Natural: IF NO RECORDS
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Bsnss_Dte.setValue(bene_Table_File_Bt_Table_Text);                                                                                                        //Natural: ASSIGN #BSNSS-DTE := BENE-TABLE-FILE.BT-TABLE-TEXT
                                                                                                                                                                          //Natural: PERFORM GET-NEXT-BUSINESS-DATE
            sub_Get_Next_Business_Date();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FBT"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FBT"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            bene_Table_File_Bt_Table_Text.setValue(pnd_Next_Bsnss_Dte);                                                                                                   //Natural: ASSIGN BT-TABLE-TEXT := #NEXT-BSNSS-DTE
            bene_Table_File_Bt_Rcrd_Last_Updt_Userid.setValue(Global.getPROGRAM());                                                                                       //Natural: ASSIGN BT-RCRD-LAST-UPDT-USERID := *PROGRAM
            bene_Table_File_Bt_Rcrd_Last_Updt_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("HHIISST"));                                                         //Natural: MOVE EDITED *TIMX ( EM = HHIISST ) TO BT-RCRD-LAST-UPDT-TME
            bene_Table_File_Bt_Rcrd_Last_Updt_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                        //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO BT-RCRD-LAST-UPDT-DTE
            vw_bene_Table_File.updateDBRow("FBT");                                                                                                                        //Natural: UPDATE ( FBT. )
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-NEXT-BUSINESS-DATE
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Get_Next_Business_Date() throws Exception                                                                                                            //Natural: GET-NEXT-BUSINESS-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pdaNeca4000.getNeca4000().reset();                                                                                                                                //Natural: RESET NECA4000 #NEXT-BSNSS-DTE
        pnd_Next_Bsnss_Dte.reset();
        pdaNeca4000.getNeca4000_Function_Cde().setValue("CAL");                                                                                                           //Natural: ASSIGN NECA4000.FUNCTION-CDE := 'CAL'
        pdaNeca4000.getNeca4000_Cal_Key_For_Dte().setValue(pnd_Bsnss_Dte_Pnd_Bsnss_Dte_N);                                                                                //Natural: ASSIGN NECA4000.CAL-KEY-FOR-DTE := #BSNSS-DTE-N
        DbsUtil.callnat(Necn4000.class , getCurrentProcessState(), pdaNeca4000.getNeca4000());                                                                            //Natural: CALLNAT 'NECN4000' NECA4000
        if (condition(Global.isEscape())) return;
        pnd_Next_Bsnss_Dte_Pnd_Next_Bsnss_Dte_N8.setValue(pdaNeca4000.getNeca4000_Cal_Next_Business_Dte().getValue(1));                                                   //Natural: ASSIGN #NEXT-BSNSS-DTE-N8 := NECA4000.CAL-NEXT-BUSINESS-DTE ( 1 )
        getReports().write(0, "GET-NEXT-BUSINESS-DATE BusiDate",pnd_Bsnss_Dte_Pnd_Bsnss_Dte_N,"NextBusiDate",pdaNeca4000.getNeca4000_Cal_Next_Business_Dte().getValue(1)); //Natural: WRITE 'GET-NEXT-BUSINESS-DATE BusiDate' #BSNSS-DTE-N 'NextBusiDate' NECA4000.CAL-NEXT-BUSINESS-DTE ( 1 )
        if (Global.isEscape()) return;
        //*  GET-NEXT-BUSINESS-DATE
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        getReports().write(0, "*************************************************************");                                                                           //Natural: WRITE '*************************************************************'
        getReports().write(0, "*                                                           *");                                                                           //Natural: WRITE '*                                                           *'
        getReports().write(0, "*   E   R   R   O   R      O   C   C   U   R   R   E   D ! !*");                                                                           //Natural: WRITE '*   E   R   R   O   R      O   C   C   U   R   R   E   D ! !*'
        getReports().write(0, "*                                                           *");                                                                           //Natural: WRITE '*                                                           *'
        getReports().write(0, "*************************************************************");                                                                           //Natural: WRITE '*************************************************************'
        getReports().write(0, "*                                                           *");                                                                           //Natural: WRITE '*                                                           *'
        getReports().write(0, "* PROGRAM......:",Global.getPROGRAM());                                                                                                    //Natural: WRITE '* PROGRAM......:' *PROGRAM
        getReports().write(0, "* ERROR NBR....:",Global.getERROR_NR());                                                                                                   //Natural: WRITE '* ERROR NBR....:' *ERROR-NR
        getReports().write(0, "* ERROR LINE...:",Global.getERROR_LINE());                                                                                                 //Natural: WRITE '* ERROR LINE...:' *ERROR-LINE
        getReports().write(0, "*                                                           *");                                                                           //Natural: WRITE '*                                                           *'
        getReports().write(0, "* CONTACT SYSTEMS!                                          *");                                                                           //Natural: WRITE '* CONTACT SYSTEMS!                                          *'
        getReports().write(0, "*                                                           *");                                                                           //Natural: WRITE '*                                                           *'
        getReports().write(0, "*************************************************************");                                                                           //Natural: WRITE '*************************************************************'
        DbsUtil.terminate(33);  if (true) return;                                                                                                                         //Natural: TERMINATE 33
    };                                                                                                                                                                    //Natural: END-ERROR
}
