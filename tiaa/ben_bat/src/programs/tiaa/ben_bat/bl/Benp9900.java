/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:20:29 PM
**        * FROM NATURAL PROGRAM : Benp9900
************************************************************
**        * FILE NAME            : Benp9900.java
**        * CLASS NAME           : Benp9900
**        * INSTANCE NAME        : Benp9900
************************************************************
************************************************************************
* PROGRAM  : BENP9900
* SYSTEM   : BENEFICIARY-SYSTEM
* TITLE    : INTERFACE REPORTING
* WRITTEN  : ADRIAN EATON
* DATE     : JULY 6, 1999
* NOTES    : JOB READS BENE-CONTRACT-INTERFACE FOR ALL RECORDS (ALL 4
*            STATII) PROCESSED SINCE LR-INTERFACE-REPORT. 3 DETAIL AND
*            ONE ACTIVITY SUMMARY REPORT ARE WRITTEN (PER DAY FOR WHICH
*            BUSINESS HAS BEEN PROCESSED).
************************************************************************
*    DATE      USERID                   DESCRIPTION
* MM/DD/YYYY  XXXXXXXX  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
* 04/01/2017  DURAND    PIN EXPANSION N7 TO N12
* 07/06/1999  EATONA
* 01/12/2000  WEBBJ     ADD RQSTNG-SYSTM TO SUMMARY REPORT
* 07/14/2000  DURAND    ADD TIAA-CREF-IND TO ALL REPORTS
* CHANGED ON AUG 21,00 BY WEBBJ => ADD PIN RETRY TO RETRY REPORT
* CHANGED ON SEP 11,00 BY WEBBJ => ADD SUBTOTALS TO SUMMARY REPORT
* CHANGED ON DEC 08,03 BY FONTOUR ==> EXPANDED ARRAY TO ALLOW MORE
*                         SYSTEMS TO BE REPORTED . PREVIOUS ARRAY 15 OC.
* CHANGED ON FEB 06,04 BY FONTOUR ==> PRINT TOTALS BY REPORTS
************************************************************************

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benp9900 extends BLNatBase
{
    // Data Areas
    private GdaBeng900 gdaBeng900;
    private PdaBena015 pdaBena015;
    private PdaBena008 pdaBena008;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_P;
    private DbsField pnd_P_Chk;
    private DbsField pnd_P_Max;
    private DbsField pnd_R_Max;
    private DbsField pnd_Bt_Table_Id_Key;

    private DbsGroup pnd_Bt_Table_Id_Key__R_Field_1;
    private DbsField pnd_Bt_Table_Id_Key_Pnd_Zz;
    private DbsField pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Key;
    private DbsField pnd_Status;
    private DbsField pnd_Interface_Date;
    private DbsField pnd_Lr_Interface_Report;
    private DbsField pnd_Interface_Date_W;
    private DbsField pnd_Lr_Interface_Report_W;
    private DbsField pnd_Lr_Interface_Report_Orig;
    private DbsField pnd_Run_Date;
    private DbsField pnd_Xxx_Date;
    private DbsField pnd_Run_Date_X;
    private DbsField pnd_Fldr;
    private DbsField pnd_Attempt;
    private DbsField pnd_Attempt_A;
    private DbsField pnd_Report;
    private DbsField pnd_Cmprint;
    private DbsField pnd_Headers;

    private DbsGroup pnd_Headers__R_Field_2;
    private DbsField pnd_Headers_Pnd_Header;
    private DbsField pnd_Program;
    private DbsField pnd_Same;
    private DbsField pnd_Pi;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_M;
    private DbsField pnd_T;
    private DbsField pnd_R;
    private DbsField pnd_Break_X;

    private DbsGroup pnd_Break_X__R_Field_3;
    private DbsField pnd_Break_X_Pnd_Intrfce_Stts;
    private DbsField pnd_Break_X_Pnd_Rcrd_Crtd_For_Bsnss_Dte;
    private DbsField pnd_On_A_Break;
    private DbsField pnd_Break;
    private DbsField pnd_Sort;
    private DbsField pnd_Count;
    private DbsField pnd_Datd;
    private DbsField pnd_Start_Date;
    private DbsField pnd_No_Data;
    private DbsField pnd_No_Data_Txt;
    private DbsField pnd_Eor;
    private DbsField pnd_Date_Min;
    private DbsField pnd_S_Max;
    private DbsField pnd_S_Tot;
    private DbsField pnd_S_Inx;
    private DbsField pnd_System;

    private DbsGroup pnd_System__R_Field_4;

    private DbsGroup pnd_System_Pnd_System_Tab;
    private DbsField pnd_System_Pnd_System_Intfce;
    private DbsField pnd_System_Pnd_System_Reqst;
    private DbsField pnd_D_Max;
    private DbsField pnd_D_I;
    private DbsField pnd_S_I;

    private DbsGroup pnd_Dates;
    private DbsField pnd_Dates_Pnd_Date;

    private DbsGroup pnd_Dates_Pnd_Systems;
    private DbsField pnd_Dates_Pnd_Numbers;
    private DbsField pnd_Numbers_P;
    private DbsField pnd_Intrfce_Super_1;

    private DbsGroup pnd_Intrfce_Super_1__R_Field_5;
    private DbsField pnd_Intrfce_Super_1_Intrfce_Stts;
    private DbsField pnd_Intrfce_Super_1_Last_Prcssd_Bsnss_Dte;
    private DbsField pnd_Intrfce_Super_1_Rcrd_Crtd_For_Bsnss_Dte;
    private DbsField pnd_Intrfce_Super_1_Pin_Tiaa_Cntrct;
    private DbsField pnd_Intrfce_Super_1_Tiaa_Cref_Ind;

    private DataAccessProgramView vw_intrfce;
    private DbsField intrfce_Intrfce_Stts;
    private DbsField intrfce_Rcrd_Crtd_For_Bsnss_Dte;
    private DbsField intrfce_Rcrd_Crtd_Dte;
    private DbsField intrfce_Rcrd_Crtd_Tme;
    private DbsField intrfce_Intrfcng_Systm;
    private DbsField intrfce_Rqstng_Systm;
    private DbsField intrfce_Rcrd_Last_Updt_Dte;
    private DbsField intrfce_Rcrd_Last_Updt_Tme;
    private DbsField intrfce_Rcrd_Last_Updt_Userid;
    private DbsField intrfce_Last_Prcssd_Bsnss_Dte;
    private DbsField intrfce_Intrfce_Cmpltd_Dte;
    private DbsField intrfce_Intrfce_Cmpltd_Tme;
    private DbsField intrfce_Dflt_To_Estate_Ind;
    private DbsField intrfce_Illgble_Ind;
    private DbsField intrfce_More_Than_Five_Benes_Ind;
    private DbsField intrfce_Intrfce_Mgrtn_Ind;
    private DbsField intrfce_More_Than_Thirty_Benes_Ind;
    private DbsField intrfce_Pin_Tiaa_Cntrct;

    private DbsGroup intrfce__R_Field_6;
    private DbsField intrfce_Pin;
    private DbsField intrfce_Count_Casterror_Table;

    private DbsGroup intrfce_Error_Table;
    private DbsField intrfce_Error_Txt;
    private DbsField intrfce_Error_Cde;
    private DbsField intrfce_Error_Dte;
    private DbsField intrfce_Error_Tme;
    private DbsField intrfce_Error_Pgm;
    private DbsField intrfce_Fldr_Log_Dte_Tme;
    private DbsField intrfce_Last_Dsgntn_Srce;
    private DbsField intrfce_Last_Dsgntn_System;
    private DbsField intrfce_Last_Dsgntn_Userid;
    private DbsField intrfce_Last_Dsgntn_Dte;
    private DbsField intrfce_Last_Dsgntn_Tme;
    private DbsField intrfce_Tiaa_Cref_Chng_Dte;
    private DbsField intrfce_Tiaa_Cref_Chng_Tme;
    private DbsField intrfce_Chng_Pwr_Atty;
    private DbsField intrfce_Cref_Cntrct;
    private DbsField intrfce_Tiaa_Cref_Ind;
    private DbsField intrfce_Cntrct_Type;
    private DbsField intrfce_Eff_Dte;
    private DbsField intrfce_Mos_Ind;
    private DbsField intrfce_Irrvcble_Ind;
    private DbsField intrfce_Pymnt_Chld_Dcsd_Ind;
    private DbsField intrfce_Exempt_Spouse_Rights;
    private DbsField intrfce_Spouse_Waived_Bnfts;
    private DbsField intrfce_Chng_New_Issue_Ind;
    private DbsField intrfce_Same_As_Ind;

    private DataAccessProgramView vw_table;
    private DbsField table_Bt_Table_Id;
    private DbsField table_Bt_Table_Key;

    private DbsGroup table__R_Field_7;
    private DbsField table_Pnd_Bt_Table_Key_1st8;
    private DbsField table_Bt_Table_Text;
    private DbsField table_Bt_Rcrd_Last_Updt_Dte;
    private DbsField table_Bt_Rcrd_Last_Updt_Tme;
    private DbsField table_Bt_Rcrd_Last_Updt_Userid;
    private DbsField pnd_System_Needs_Subttl;
    private DbsField pnd_Hold_System;
    private DbsField pnd_Sub_Total;
    private DbsField pnd_Report_3_Missing;
    private DbsField pnd_End_Of_Job;
    private DbsField pnd_Total_Error;
    private DbsField pnd_Total_Success;
    private DbsField pnd_Total_Retry;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaBeng900 = GdaBeng900.getInstance(getCallnatLevel());
        registerRecord(gdaBeng900);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaBena015 = new PdaBena015(localVariables);
        pdaBena008 = new PdaBena008(localVariables);

        // Local Variables
        pnd_P = localVariables.newFieldInRecord("pnd_P", "#P", FieldType.PACKED_DECIMAL, 3);
        pnd_P_Chk = localVariables.newFieldInRecord("pnd_P_Chk", "#P-CHK", FieldType.PACKED_DECIMAL, 3);
        pnd_P_Max = localVariables.newFieldInRecord("pnd_P_Max", "#P-MAX", FieldType.PACKED_DECIMAL, 3);
        pnd_R_Max = localVariables.newFieldInRecord("pnd_R_Max", "#R-MAX", FieldType.PACKED_DECIMAL, 3);
        pnd_Bt_Table_Id_Key = localVariables.newFieldInRecord("pnd_Bt_Table_Id_Key", "#BT-TABLE-ID-KEY", FieldType.STRING, 22);

        pnd_Bt_Table_Id_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Bt_Table_Id_Key__R_Field_1", "REDEFINE", pnd_Bt_Table_Id_Key);
        pnd_Bt_Table_Id_Key_Pnd_Zz = pnd_Bt_Table_Id_Key__R_Field_1.newFieldInGroup("pnd_Bt_Table_Id_Key_Pnd_Zz", "#ZZ", FieldType.STRING, 2);
        pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Key = pnd_Bt_Table_Id_Key__R_Field_1.newFieldInGroup("pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Key", "#BT-TABLE-KEY", 
            FieldType.STRING, 20);
        pnd_Status = localVariables.newFieldArrayInRecord("pnd_Status", "#STATUS", FieldType.STRING, 1, new DbsArrayController(1, 4));
        pnd_Interface_Date = localVariables.newFieldInRecord("pnd_Interface_Date", "#INTERFACE-DATE", FieldType.STRING, 8);
        pnd_Lr_Interface_Report = localVariables.newFieldInRecord("pnd_Lr_Interface_Report", "#LR-INTERFACE-REPORT", FieldType.STRING, 8);
        pnd_Interface_Date_W = localVariables.newFieldInRecord("pnd_Interface_Date_W", "#INTERFACE-DATE-W", FieldType.DATE);
        pnd_Lr_Interface_Report_W = localVariables.newFieldInRecord("pnd_Lr_Interface_Report_W", "#LR-INTERFACE-REPORT-W", FieldType.DATE);
        pnd_Lr_Interface_Report_Orig = localVariables.newFieldInRecord("pnd_Lr_Interface_Report_Orig", "#LR-INTERFACE-REPORT-ORIG", FieldType.DATE);
        pnd_Run_Date = localVariables.newFieldArrayInRecord("pnd_Run_Date", "#RUN-DATE", FieldType.STRING, 8, new DbsArrayController(1, 20));
        pnd_Xxx_Date = localVariables.newFieldArrayInRecord("pnd_Xxx_Date", "#XXX-DATE", FieldType.STRING, 8, new DbsArrayController(1, 20));
        pnd_Run_Date_X = localVariables.newFieldInRecord("pnd_Run_Date_X", "#RUN-DATE-X", FieldType.STRING, 8);
        pnd_Fldr = localVariables.newFieldInRecord("pnd_Fldr", "#FLDR", FieldType.BOOLEAN, 1);
        pnd_Attempt = localVariables.newFieldInRecord("pnd_Attempt", "#ATTEMPT", FieldType.STRING, 3);
        pnd_Attempt_A = localVariables.newFieldArrayInRecord("pnd_Attempt_A", "#ATTEMPT-A", FieldType.STRING, 3, new DbsArrayController(1, 3));
        pnd_Report = localVariables.newFieldInRecord("pnd_Report", "#REPORT", FieldType.STRING, 20);
        pnd_Cmprint = localVariables.newFieldArrayInRecord("pnd_Cmprint", "#CMPRINT", FieldType.STRING, 8, new DbsArrayController(1, 4));
        pnd_Headers = localVariables.newFieldArrayInRecord("pnd_Headers", "#HEADERS", FieldType.STRING, 34, new DbsArrayController(1, 5));

        pnd_Headers__R_Field_2 = localVariables.newGroupInRecord("pnd_Headers__R_Field_2", "REDEFINE", pnd_Headers);
        pnd_Headers_Pnd_Header = pnd_Headers__R_Field_2.newFieldInGroup("pnd_Headers_Pnd_Header", "#HEADER", FieldType.STRING, 34);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 10);
        pnd_Same = localVariables.newFieldInRecord("pnd_Same", "#SAME", FieldType.STRING, 5);
        pnd_Pi = localVariables.newFieldInRecord("pnd_Pi", "#PI", FieldType.PACKED_DECIMAL, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.PACKED_DECIMAL, 3);
        pnd_M = localVariables.newFieldInRecord("pnd_M", "#M", FieldType.PACKED_DECIMAL, 3);
        pnd_T = localVariables.newFieldInRecord("pnd_T", "#T", FieldType.PACKED_DECIMAL, 3);
        pnd_R = localVariables.newFieldInRecord("pnd_R", "#R", FieldType.PACKED_DECIMAL, 3);
        pnd_Break_X = localVariables.newFieldInRecord("pnd_Break_X", "#BREAK-X", FieldType.STRING, 9);

        pnd_Break_X__R_Field_3 = localVariables.newGroupInRecord("pnd_Break_X__R_Field_3", "REDEFINE", pnd_Break_X);
        pnd_Break_X_Pnd_Intrfce_Stts = pnd_Break_X__R_Field_3.newFieldInGroup("pnd_Break_X_Pnd_Intrfce_Stts", "#INTRFCE-STTS", FieldType.STRING, 1);
        pnd_Break_X_Pnd_Rcrd_Crtd_For_Bsnss_Dte = pnd_Break_X__R_Field_3.newFieldInGroup("pnd_Break_X_Pnd_Rcrd_Crtd_For_Bsnss_Dte", "#RCRD-CRTD-FOR-BSNSS-DTE", 
            FieldType.STRING, 8);
        pnd_On_A_Break = localVariables.newFieldInRecord("pnd_On_A_Break", "#ON-A-BREAK", FieldType.BOOLEAN, 1);
        pnd_Break = localVariables.newFieldInRecord("pnd_Break", "#BREAK", FieldType.STRING, 9);
        pnd_Sort = localVariables.newFieldInRecord("pnd_Sort", "#SORT", FieldType.NUMERIC, 1);
        pnd_Count = localVariables.newFieldArrayInRecord("pnd_Count", "#COUNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 4));
        pnd_Datd = localVariables.newFieldInRecord("pnd_Datd", "#DATD", FieldType.DATE);
        pnd_Start_Date = localVariables.newFieldInRecord("pnd_Start_Date", "#START-DATE", FieldType.STRING, 40);
        pnd_No_Data = localVariables.newFieldInRecord("pnd_No_Data", "#NO-DATA", FieldType.BOOLEAN, 1);
        pnd_No_Data_Txt = localVariables.newFieldInRecord("pnd_No_Data_Txt", "#NO-DATA-TXT", FieldType.STRING, 40);
        pnd_Eor = localVariables.newFieldInRecord("pnd_Eor", "#EOR", FieldType.STRING, 40);
        pnd_Date_Min = localVariables.newFieldInRecord("pnd_Date_Min", "#DATE-MIN", FieldType.STRING, 8);
        pnd_S_Max = localVariables.newFieldInRecord("pnd_S_Max", "#S-MAX", FieldType.PACKED_DECIMAL, 3);
        pnd_S_Tot = localVariables.newFieldInRecord("pnd_S_Tot", "#S-TOT", FieldType.PACKED_DECIMAL, 3);
        pnd_S_Inx = localVariables.newFieldInRecord("pnd_S_Inx", "#S-INX", FieldType.PACKED_DECIMAL, 3);
        pnd_System = localVariables.newFieldArrayInRecord("pnd_System", "#SYSTEM", FieldType.STRING, 20, new DbsArrayController(1, 50));

        pnd_System__R_Field_4 = localVariables.newGroupInRecord("pnd_System__R_Field_4", "REDEFINE", pnd_System);

        pnd_System_Pnd_System_Tab = pnd_System__R_Field_4.newGroupArrayInGroup("pnd_System_Pnd_System_Tab", "#SYSTEM-TAB", new DbsArrayController(1, 50));
        pnd_System_Pnd_System_Intfce = pnd_System_Pnd_System_Tab.newFieldInGroup("pnd_System_Pnd_System_Intfce", "#SYSTEM-INTFCE", FieldType.STRING, 11);
        pnd_System_Pnd_System_Reqst = pnd_System_Pnd_System_Tab.newFieldInGroup("pnd_System_Pnd_System_Reqst", "#SYSTEM-REQST", FieldType.STRING, 9);
        pnd_D_Max = localVariables.newFieldInRecord("pnd_D_Max", "#D-MAX", FieldType.PACKED_DECIMAL, 3);
        pnd_D_I = localVariables.newFieldInRecord("pnd_D_I", "#D-I", FieldType.PACKED_DECIMAL, 3);
        pnd_S_I = localVariables.newFieldInRecord("pnd_S_I", "#S-I", FieldType.PACKED_DECIMAL, 3);

        pnd_Dates = localVariables.newGroupArrayInRecord("pnd_Dates", "#DATES", new DbsArrayController(1, 32));
        pnd_Dates_Pnd_Date = pnd_Dates.newFieldInGroup("pnd_Dates_Pnd_Date", "#DATE", FieldType.STRING, 8);

        pnd_Dates_Pnd_Systems = pnd_Dates.newGroupArrayInGroup("pnd_Dates_Pnd_Systems", "#SYSTEMS", new DbsArrayController(1, 50));
        pnd_Dates_Pnd_Numbers = pnd_Dates_Pnd_Systems.newFieldArrayInGroup("pnd_Dates_Pnd_Numbers", "#NUMBERS", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            5));
        pnd_Numbers_P = localVariables.newFieldArrayInRecord("pnd_Numbers_P", "#NUMBERS-P", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 5));
        pnd_Intrfce_Super_1 = localVariables.newFieldInRecord("pnd_Intrfce_Super_1", "#INTRFCE-SUPER-1", FieldType.STRING, 40);

        pnd_Intrfce_Super_1__R_Field_5 = localVariables.newGroupInRecord("pnd_Intrfce_Super_1__R_Field_5", "REDEFINE", pnd_Intrfce_Super_1);
        pnd_Intrfce_Super_1_Intrfce_Stts = pnd_Intrfce_Super_1__R_Field_5.newFieldInGroup("pnd_Intrfce_Super_1_Intrfce_Stts", "INTRFCE-STTS", FieldType.STRING, 
            1);
        pnd_Intrfce_Super_1_Last_Prcssd_Bsnss_Dte = pnd_Intrfce_Super_1__R_Field_5.newFieldInGroup("pnd_Intrfce_Super_1_Last_Prcssd_Bsnss_Dte", "LAST-PRCSSD-BSNSS-DTE", 
            FieldType.STRING, 8);
        pnd_Intrfce_Super_1_Rcrd_Crtd_For_Bsnss_Dte = pnd_Intrfce_Super_1__R_Field_5.newFieldInGroup("pnd_Intrfce_Super_1_Rcrd_Crtd_For_Bsnss_Dte", "RCRD-CRTD-FOR-BSNSS-DTE", 
            FieldType.STRING, 8);
        pnd_Intrfce_Super_1_Pin_Tiaa_Cntrct = pnd_Intrfce_Super_1__R_Field_5.newFieldInGroup("pnd_Intrfce_Super_1_Pin_Tiaa_Cntrct", "PIN-TIAA-CNTRCT", 
            FieldType.STRING, 22);
        pnd_Intrfce_Super_1_Tiaa_Cref_Ind = pnd_Intrfce_Super_1__R_Field_5.newFieldInGroup("pnd_Intrfce_Super_1_Tiaa_Cref_Ind", "TIAA-CREF-IND", FieldType.STRING, 
            1);

        vw_intrfce = new DataAccessProgramView(new NameInfo("vw_intrfce", "INTRFCE"), "BENE_CONTRACT_INTERFACE_12", "BENE_CONT_INTFCE", DdmPeriodicGroups.getInstance().getGroups("BENE_CONTRACT_INTERFACE_12"));
        intrfce_Intrfce_Stts = vw_intrfce.getRecord().newFieldInGroup("intrfce_Intrfce_Stts", "INTRFCE-STTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "INTRFCE_STTS");
        intrfce_Intrfce_Stts.setDdmHeader("INTRFCE/STATUS");
        intrfce_Rcrd_Crtd_For_Bsnss_Dte = vw_intrfce.getRecord().newFieldInGroup("intrfce_Rcrd_Crtd_For_Bsnss_Dte", "RCRD-CRTD-FOR-BSNSS-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "RCRD_CRTD_FOR_BSNSS_DTE");
        intrfce_Rcrd_Crtd_For_Bsnss_Dte.setDdmHeader("RCRD CRTD/FOR/BSNSS DTE");
        intrfce_Rcrd_Crtd_Dte = vw_intrfce.getRecord().newFieldInGroup("intrfce_Rcrd_Crtd_Dte", "RCRD-CRTD-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_CRTD_DTE");
        intrfce_Rcrd_Crtd_Dte.setDdmHeader("RECORD/CREATED/DATE");
        intrfce_Rcrd_Crtd_Tme = vw_intrfce.getRecord().newFieldInGroup("intrfce_Rcrd_Crtd_Tme", "RCRD-CRTD-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "RCRD_CRTD_TME");
        intrfce_Rcrd_Crtd_Tme.setDdmHeader("RECORD/CREATED/TIME");
        intrfce_Intrfcng_Systm = vw_intrfce.getRecord().newFieldInGroup("intrfce_Intrfcng_Systm", "INTRFCNG-SYSTM", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "INTRFCNG_SYSTM");
        intrfce_Intrfcng_Systm.setDdmHeader("INTRFCNG/SYSTEM");
        intrfce_Rqstng_Systm = vw_intrfce.getRecord().newFieldInGroup("intrfce_Rqstng_Systm", "RQSTNG-SYSTM", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RQSTNG_SYSTM");
        intrfce_Rqstng_Systm.setDdmHeader("RQSTNG/SYSTEM");
        intrfce_Rcrd_Last_Updt_Dte = vw_intrfce.getRecord().newFieldInGroup("intrfce_Rcrd_Last_Updt_Dte", "RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_DTE");
        intrfce_Rcrd_Last_Updt_Dte.setDdmHeader("RECORD/LAST UPDT/DATE");
        intrfce_Rcrd_Last_Updt_Tme = vw_intrfce.getRecord().newFieldInGroup("intrfce_Rcrd_Last_Updt_Tme", "RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_TME");
        intrfce_Rcrd_Last_Updt_Tme.setDdmHeader("RECORD/LAST UPDT/TIME");
        intrfce_Rcrd_Last_Updt_Userid = vw_intrfce.getRecord().newFieldInGroup("intrfce_Rcrd_Last_Updt_Userid", "RCRD-LAST-UPDT-USERID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "RCRD_LAST_UPDT_USERID");
        intrfce_Rcrd_Last_Updt_Userid.setDdmHeader("RECORD/LAST UPDT/USER ID");
        intrfce_Last_Prcssd_Bsnss_Dte = vw_intrfce.getRecord().newFieldInGroup("intrfce_Last_Prcssd_Bsnss_Dte", "LAST-PRCSSD-BSNSS-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "LAST_PRCSSD_BSNSS_DTE");
        intrfce_Last_Prcssd_Bsnss_Dte.setDdmHeader("LAST PRCSSD/BUSINESS/DATE");
        intrfce_Intrfce_Cmpltd_Dte = vw_intrfce.getRecord().newFieldInGroup("intrfce_Intrfce_Cmpltd_Dte", "INTRFCE-CMPLTD-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "INTRFCE_CMPLTD_DTE");
        intrfce_Intrfce_Cmpltd_Dte.setDdmHeader("INTFCE/CMPLTD/DATE");
        intrfce_Intrfce_Cmpltd_Tme = vw_intrfce.getRecord().newFieldInGroup("intrfce_Intrfce_Cmpltd_Tme", "INTRFCE-CMPLTD-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "INTRFCE_CMPLTD_TME");
        intrfce_Intrfce_Cmpltd_Tme.setDdmHeader("INTRFCE/CMPLTD/TME");
        intrfce_Dflt_To_Estate_Ind = vw_intrfce.getRecord().newFieldInGroup("intrfce_Dflt_To_Estate_Ind", "DFLT-TO-ESTATE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "DFLT_TO_ESTATE_IND");
        intrfce_Dflt_To_Estate_Ind.setDdmHeader("DEFAULT/TO/ESTATE");
        intrfce_Illgble_Ind = vw_intrfce.getRecord().newFieldInGroup("intrfce_Illgble_Ind", "ILLGBLE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ILLGBLE_IND");
        intrfce_Illgble_Ind.setDdmHeader("ILLGBLE/IND");
        intrfce_More_Than_Five_Benes_Ind = vw_intrfce.getRecord().newFieldInGroup("intrfce_More_Than_Five_Benes_Ind", "MORE-THAN-FIVE-BENES-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "MORE_THAN_FIVE_BENES_IND");
        intrfce_More_Than_Five_Benes_Ind.setDdmHeader("MORE/THAN 5/BENES");
        intrfce_Intrfce_Mgrtn_Ind = vw_intrfce.getRecord().newFieldInGroup("intrfce_Intrfce_Mgrtn_Ind", "INTRFCE-MGRTN-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "INTRFCE_MGRTN_IND");
        intrfce_Intrfce_Mgrtn_Ind.setDdmHeader("INTRFCE/MGRTN/IND");
        intrfce_More_Than_Thirty_Benes_Ind = vw_intrfce.getRecord().newFieldInGroup("intrfce_More_Than_Thirty_Benes_Ind", "MORE-THAN-THIRTY-BENES-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "MORE_THAN_THIRTY_BENES_IND");
        intrfce_More_Than_Thirty_Benes_Ind.setDdmHeader("MORE/THAN 30/BENES");
        intrfce_Pin_Tiaa_Cntrct = vw_intrfce.getRecord().newFieldInGroup("intrfce_Pin_Tiaa_Cntrct", "PIN-TIAA-CNTRCT", FieldType.STRING, 22, RepeatingFieldStrategy.None, 
            "PIN_TIAA_CNTRCT");
        intrfce_Pin_Tiaa_Cntrct.setDdmHeader("PIN/TIAA/CONTRACT");

        intrfce__R_Field_6 = vw_intrfce.getRecord().newGroupInGroup("intrfce__R_Field_6", "REDEFINE", intrfce_Pin_Tiaa_Cntrct);
        intrfce_Pin = intrfce__R_Field_6.newFieldInGroup("intrfce_Pin", "PIN", FieldType.NUMERIC, 12);
        intrfce_Count_Casterror_Table = vw_intrfce.getRecord().newFieldInGroup("intrfce_Count_Casterror_Table", "C*ERROR-TABLE", RepeatingFieldStrategy.CAsteriskVariable, 
            "BENE_CONT_INTFCE_ERROR_TABLE");

        intrfce_Error_Table = vw_intrfce.getRecord().newGroupArrayInGroup("intrfce_Error_Table", "ERROR-TABLE", new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "BENE_CONT_INTFCE_ERROR_TABLE");
        intrfce_Error_Txt = intrfce_Error_Table.newFieldInGroup("intrfce_Error_Txt", "ERROR-TXT", FieldType.STRING, 72, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ERROR_TXT", "BENE_CONT_INTFCE_ERROR_TABLE");
        intrfce_Error_Txt.setDdmHeader("ERROR/TEXT");
        intrfce_Error_Cde = intrfce_Error_Table.newFieldInGroup("intrfce_Error_Cde", "ERROR-CDE", FieldType.STRING, 5, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ERROR_CDE", "BENE_CONT_INTFCE_ERROR_TABLE");
        intrfce_Error_Cde.setDdmHeader("ERROR/CODE");
        intrfce_Error_Dte = intrfce_Error_Table.newFieldInGroup("intrfce_Error_Dte", "ERROR-DTE", FieldType.STRING, 8, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ERROR_DTE", "BENE_CONT_INTFCE_ERROR_TABLE");
        intrfce_Error_Dte.setDdmHeader("ERROR/DATE");
        intrfce_Error_Tme = intrfce_Error_Table.newFieldInGroup("intrfce_Error_Tme", "ERROR-TME", FieldType.STRING, 7, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ERROR_TME", "BENE_CONT_INTFCE_ERROR_TABLE");
        intrfce_Error_Tme.setDdmHeader("ERROR/TIME");
        intrfce_Error_Pgm = intrfce_Error_Table.newFieldInGroup("intrfce_Error_Pgm", "ERROR-PGM", FieldType.STRING, 8, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ERROR_PGM", "BENE_CONT_INTFCE_ERROR_TABLE");
        intrfce_Fldr_Log_Dte_Tme = vw_intrfce.getRecord().newFieldInGroup("intrfce_Fldr_Log_Dte_Tme", "FLDR-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "FLDR_LOG_DTE_TME");
        intrfce_Fldr_Log_Dte_Tme.setDdmHeader("FOLDER/LOG DATE/TIME");
        intrfce_Last_Dsgntn_Srce = vw_intrfce.getRecord().newFieldInGroup("intrfce_Last_Dsgntn_Srce", "LAST-DSGNTN-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "LAST_DSGNTN_SRCE");
        intrfce_Last_Dsgntn_Srce.setDdmHeader("LAST/DSGNTN/SOURCE");
        intrfce_Last_Dsgntn_System = vw_intrfce.getRecord().newFieldInGroup("intrfce_Last_Dsgntn_System", "LAST-DSGNTN-SYSTEM", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "LAST_DSGNTN_SYSTEM");
        intrfce_Last_Dsgntn_System.setDdmHeader("LAST/DSGNTN/SYSTEM");
        intrfce_Last_Dsgntn_Userid = vw_intrfce.getRecord().newFieldInGroup("intrfce_Last_Dsgntn_Userid", "LAST-DSGNTN-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "LAST_DSGNTN_USERID");
        intrfce_Last_Dsgntn_Userid.setDdmHeader("LAST/DSGNTN/USERID");
        intrfce_Last_Dsgntn_Dte = vw_intrfce.getRecord().newFieldInGroup("intrfce_Last_Dsgntn_Dte", "LAST-DSGNTN-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "LAST_DSGNTN_DTE");
        intrfce_Last_Dsgntn_Dte.setDdmHeader("LAST/DSGNTN/DATE");
        intrfce_Last_Dsgntn_Tme = vw_intrfce.getRecord().newFieldInGroup("intrfce_Last_Dsgntn_Tme", "LAST-DSGNTN-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "LAST_DSGNTN_TME");
        intrfce_Last_Dsgntn_Tme.setDdmHeader("LAST/DSGNTN/TIME");
        intrfce_Tiaa_Cref_Chng_Dte = vw_intrfce.getRecord().newFieldInGroup("intrfce_Tiaa_Cref_Chng_Dte", "TIAA-CREF-CHNG-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIAA_CREF_CHNG_DTE");
        intrfce_Tiaa_Cref_Chng_Dte.setDdmHeader("TIAA CREF/CHANGE/DATE");
        intrfce_Tiaa_Cref_Chng_Tme = vw_intrfce.getRecord().newFieldInGroup("intrfce_Tiaa_Cref_Chng_Tme", "TIAA-CREF-CHNG-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "TIAA_CREF_CHNG_TME");
        intrfce_Tiaa_Cref_Chng_Tme.setDdmHeader("TIAA CREF/CHANGE/TIME");
        intrfce_Chng_Pwr_Atty = vw_intrfce.getRecord().newFieldInGroup("intrfce_Chng_Pwr_Atty", "CHNG-PWR-ATTY", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CHNG_PWR_ATTY");
        intrfce_Chng_Pwr_Atty.setDdmHeader("CHANGE/POWER/ATTNY");
        intrfce_Cref_Cntrct = vw_intrfce.getRecord().newFieldInGroup("intrfce_Cref_Cntrct", "CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CREF_CNTRCT");
        intrfce_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        intrfce_Tiaa_Cref_Ind = vw_intrfce.getRecord().newFieldInGroup("intrfce_Tiaa_Cref_Ind", "TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIAA_CREF_IND");
        intrfce_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        intrfce_Cntrct_Type = vw_intrfce.getRecord().newFieldInGroup("intrfce_Cntrct_Type", "CNTRCT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_TYPE");
        intrfce_Cntrct_Type.setDdmHeader("CNTRCT/TYPE");
        intrfce_Eff_Dte = vw_intrfce.getRecord().newFieldInGroup("intrfce_Eff_Dte", "EFF-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "EFF_DTE");
        intrfce_Eff_Dte.setDdmHeader("EFFECTIVE/DATE");
        intrfce_Mos_Ind = vw_intrfce.getRecord().newFieldInGroup("intrfce_Mos_Ind", "MOS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "MOS_IND");
        intrfce_Mos_Ind.setDdmHeader("MOS/IND");
        intrfce_Irrvcble_Ind = vw_intrfce.getRecord().newFieldInGroup("intrfce_Irrvcble_Ind", "IRRVCBLE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "IRRVCBLE_IND");
        intrfce_Irrvcble_Ind.setDdmHeader("IRVCBL/IND");
        intrfce_Pymnt_Chld_Dcsd_Ind = vw_intrfce.getRecord().newFieldInGroup("intrfce_Pymnt_Chld_Dcsd_Ind", "PYMNT-CHLD-DCSD-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "PYMNT_CHLD_DCSD_IND");
        intrfce_Pymnt_Chld_Dcsd_Ind.setDdmHeader("PYMNT/CHILD/DCSD");
        intrfce_Exempt_Spouse_Rights = vw_intrfce.getRecord().newFieldInGroup("intrfce_Exempt_Spouse_Rights", "EXEMPT-SPOUSE-RIGHTS", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "EXEMPT_SPOUSE_RIGHTS");
        intrfce_Exempt_Spouse_Rights.setDdmHeader("EXEMPT/SPOUSAL/RIGHTS");
        intrfce_Spouse_Waived_Bnfts = vw_intrfce.getRecord().newFieldInGroup("intrfce_Spouse_Waived_Bnfts", "SPOUSE-WAIVED-BNFTS", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "SPOUSE_WAIVED_BNFTS");
        intrfce_Spouse_Waived_Bnfts.setDdmHeader("SPOUSE/WAIVED/BENEFITS");
        intrfce_Chng_New_Issue_Ind = vw_intrfce.getRecord().newFieldInGroup("intrfce_Chng_New_Issue_Ind", "CHNG-NEW-ISSUE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CHNG_NEW_ISSUE_IND");
        intrfce_Chng_New_Issue_Ind.setDdmHeader("CHANGE/NEW ISSUE/IND");
        intrfce_Same_As_Ind = vw_intrfce.getRecord().newFieldInGroup("intrfce_Same_As_Ind", "SAME-AS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "SAME_AS_IND");
        registerRecord(vw_intrfce);

        vw_table = new DataAccessProgramView(new NameInfo("vw_table", "TABLE"), "BENE_TABLE_FILE", "BENE_TABLE");
        table_Bt_Table_Id = vw_table.getRecord().newFieldInGroup("table_Bt_Table_Id", "BT-TABLE-ID", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "BT_TABLE_ID");
        table_Bt_Table_Id.setDdmHeader("TABLE/ID");
        table_Bt_Table_Key = vw_table.getRecord().newFieldInGroup("table_Bt_Table_Key", "BT-TABLE-KEY", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "BT_TABLE_KEY");
        table_Bt_Table_Key.setDdmHeader("TABLE/KEY");

        table__R_Field_7 = vw_table.getRecord().newGroupInGroup("table__R_Field_7", "REDEFINE", table_Bt_Table_Key);
        table_Pnd_Bt_Table_Key_1st8 = table__R_Field_7.newFieldInGroup("table_Pnd_Bt_Table_Key_1st8", "#BT-TABLE-KEY-1ST8", FieldType.STRING, 8);
        table_Bt_Table_Text = vw_table.getRecord().newFieldInGroup("table_Bt_Table_Text", "BT-TABLE-TEXT", FieldType.STRING, 72, RepeatingFieldStrategy.None, 
            "BT_TABLE_TEXT");
        table_Bt_Table_Text.setDdmHeader("TABLE/TEXT");
        table_Bt_Rcrd_Last_Updt_Dte = vw_table.getRecord().newFieldInGroup("table_Bt_Rcrd_Last_Updt_Dte", "BT-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "BT_RCRD_LAST_UPDT_DTE");
        table_Bt_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        table_Bt_Rcrd_Last_Updt_Tme = vw_table.getRecord().newFieldInGroup("table_Bt_Rcrd_Last_Updt_Tme", "BT-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, 
            RepeatingFieldStrategy.None, "BT_RCRD_LAST_UPDT_TME");
        table_Bt_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        table_Bt_Rcrd_Last_Updt_Userid = vw_table.getRecord().newFieldInGroup("table_Bt_Rcrd_Last_Updt_Userid", "BT-RCRD-LAST-UPDT-USERID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "BT_RCRD_LAST_UPDT_USERID");
        table_Bt_Rcrd_Last_Updt_Userid.setDdmHeader("RECRD LAST/UPDATE/USER ID");
        registerRecord(vw_table);

        pnd_System_Needs_Subttl = localVariables.newFieldInRecord("pnd_System_Needs_Subttl", "#SYSTEM-NEEDS-SUBTTL", FieldType.BOOLEAN, 1);
        pnd_Hold_System = localVariables.newFieldInRecord("pnd_Hold_System", "#HOLD-SYSTEM", FieldType.STRING, 11);
        pnd_Sub_Total = localVariables.newFieldInRecord("pnd_Sub_Total", "#SUB-TOTAL", FieldType.STRING, 11);
        pnd_Report_3_Missing = localVariables.newFieldInRecord("pnd_Report_3_Missing", "#REPORT-3-MISSING", FieldType.BOOLEAN, 1);
        pnd_End_Of_Job = localVariables.newFieldInRecord("pnd_End_Of_Job", "#END-OF-JOB", FieldType.BOOLEAN, 1);
        pnd_Total_Error = localVariables.newFieldInRecord("pnd_Total_Error", "#TOTAL-ERROR", FieldType.PACKED_DECIMAL, 9);
        pnd_Total_Success = localVariables.newFieldInRecord("pnd_Total_Success", "#TOTAL-SUCCESS", FieldType.PACKED_DECIMAL, 9);
        pnd_Total_Retry = localVariables.newFieldInRecord("pnd_Total_Retry", "#TOTAL-RETRY", FieldType.PACKED_DECIMAL, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_intrfce.reset();
        vw_table.reset();

        localVariables.reset();
        pnd_P_Chk.setInitialValue(1);
        pnd_P_Max.setInitialValue(4);
        pnd_Bt_Table_Id_Key.setInitialValue("ZZ");
        pnd_Status.getValue(1).setInitialValue("E");
        pnd_Status.getValue(2).setInitialValue("F");
        pnd_Status.getValue(3).setInitialValue("R");
        pnd_Status.getValue(4).setInitialValue("S");
        pnd_Attempt_A.getValue(0 + 1).setInitialValue("H'859999'");
        pnd_Attempt_A.getValue(1 + 1).setInitialValue("H'F1A2A3'");
        pnd_Attempt_A.getValue(2 + 1).setInitialValue("H'F29584'");
        pnd_Report.setInitialValue("LR-INTERFACE-REPORT");
        pnd_Cmprint.getValue(1).setInitialValue("CMPRT01");
        pnd_Cmprint.getValue(2).setInitialValue("CMPRT02");
        pnd_Cmprint.getValue(3).setInitialValue("CMPRT03");
        pnd_Cmprint.getValue(4).setInitialValue("CMPRT04");
        pnd_Headers.getValue(0 + 1).setInitialValue(" ");
        pnd_Headers.getValue(1 + 1).setInitialValue("     Exceptions Detail Report");
        pnd_Headers.getValue(2 + 1).setInitialValue("        Retry Detail Report");
        pnd_Headers.getValue(3 + 1).setInitialValue("Successful Interface Detail Report");
        pnd_Headers.getValue(4 + 1).setInitialValue("     Interface Summary Report");
        pnd_Program.setInitialValue(Global.getPROGRAM());
        pnd_Same.setInitialValue("SAME");
        pnd_Start_Date.setInitialValue("Sent Business Date:");
        pnd_No_Data_Txt.setInitialValue(" No data extracted for this report");
        pnd_Eor.setInitialValue("***  E n d   o f   R e p o r t  ***");
        pnd_Date_Min.setInitialValue("99999999");
        pnd_S_Max.setInitialValue(50);
        pnd_D_Max.setInitialValue(31);
        pnd_Sub_Total.setInitialValue("  Sub Total");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Benp9900() throws Exception
    {
        super("Benp9900");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Benp9900|Main");
        getReports().atTopOfPage(atTopEventRpt0, 0);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt4, 4);
        setupReports();
        while(true)
        {
            try
            {
                //*                                                                                                                                                       //Natural: FORMAT LS = 133 PS = 56 SG = OFF ZP = OFF;//Natural: FORMAT ( 01 ) LS = 133 PS = 56 SG = OFF ZP = OFF;//Natural: FORMAT ( 04 ) LS = 134 PS = 56 SG = OFF ZP = OFF;//Natural: FORMAT ( 05 ) LS = 133 PS = 56 SG = OFF ZP = OFF
                getReports().write(0, ReportOption.NOTITLE,"=====>>>",Global.getPROGRAM(),"=",gdaBeng900.getDialog_Info_Pnd_Pnd_Command(), new AlphanumericLength         //Natural: WRITE '=====>>>' *PROGRAM '=' ##COMMAND ( AL = 20 )
                    (20));
                if (Global.isEscape()) return;
                //*  IE, FIRST TIME IN
                if (condition(gdaBeng900.getDialog_Info_Pnd_Pnd_Command().equals(" ")))                                                                                   //Natural: IF ##COMMAND = ' '
                {
                    //*  READ ZZ-DATES, SET UP #RUN-DATE, AND RE-FETCH
                                                                                                                                                                          //Natural: PERFORM START-UP
                    sub_Start_Up();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: ELSE
                else if (condition(INPUT_1))
                {
                    DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Run_Date.getValue("*"));                                                                       //Natural: INPUT #RUN-DATE ( * )
                }                                                                                                                                                         //Natural: END-IF
                pnd_Interface_Date.setValue(pnd_Run_Date.getValue(1));                                                                                                    //Natural: MOVE #RUN-DATE ( 01 ) TO #INTERFACE-DATE #LR-INTERFACE-REPORT
                pnd_Lr_Interface_Report.setValue(pnd_Run_Date.getValue(1));
                pnd_Datd.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Interface_Date);                                                                               //Natural: MOVE EDITED #INTERFACE-DATE TO #DATD ( EM = YYYYMMDD )
                pnd_Interface_Date_W.setValue(pnd_Datd);                                                                                                                  //Natural: MOVE #DATD TO #INTERFACE-DATE-W
                pnd_Datd.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Lr_Interface_Report);                                                                          //Natural: MOVE EDITED #LR-INTERFACE-REPORT TO #DATD ( EM = YYYYMMDD )
                pnd_Lr_Interface_Report_W.setValue(pnd_Datd);                                                                                                             //Natural: MOVE #DATD TO #LR-INTERFACE-REPORT-W
                                                                                                                                                                          //Natural: PERFORM SYSTEM-TABLE
                sub_System_Table();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                pnd_Intrfce_Super_1_Last_Prcssd_Bsnss_Dte.setValue(pnd_Lr_Interface_Report);                                                                              //Natural: MOVE #LR-INTERFACE-REPORT TO #INTRFCE-SUPER-1.LAST-PRCSSD-BSNSS-DTE
                MAIN_LOOP:                                                                                                                                                //Natural: FOR #K = 1 TO #P-MAX
                for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pnd_P_Max)); pnd_K.nadd(1))
                {
                    pnd_Intrfce_Super_1_Intrfce_Stts.setValue(pnd_Status.getValue(pnd_K));                                                                                //Natural: MOVE #STATUS ( #K ) TO #INTRFCE-SUPER-1.INTRFCE-STTS
                    vw_intrfce.startDatabaseRead                                                                                                                          //Natural: READ INTRFCE WITH INTRFCE-SUPER-1 = #INTRFCE-SUPER-1
                    (
                    "PND_PND_L1105",
                    new Wc[] { new Wc("INTRFCE_SUPER_1", ">=", pnd_Intrfce_Super_1, WcType.BY) },
                    new Oc[] { new Oc("INTRFCE_SUPER_1", "ASC") }
                    );
                    PND_PND_L1105:
                    while (condition(vw_intrfce.readNextRow("PND_PND_L1105")))
                    {
                        if (condition(intrfce_Intrfce_Stts.notEquals(pnd_Intrfce_Super_1_Intrfce_Stts) || intrfce_Last_Prcssd_Bsnss_Dte.notEquals(pnd_Lr_Interface_Report))) //Natural: IF INTRFCE.INTRFCE-STTS NE #INTRFCE-SUPER-1.INTRFCE-STTS OR INTRFCE.LAST-PRCSSD-BSNSS-DTE NE #LR-INTERFACE-REPORT
                        {
                            if (true) break PND_PND_L1105;                                                                                                                //Natural: ESCAPE BOTTOM ( ##L1105. )
                        }                                                                                                                                                 //Natural: END-IF
                        //* SHOULDN't
                        if (condition(! (intrfce_Intrfcng_Systm.equals(pnd_System_Pnd_System_Intfce.getValue(1,":",pnd_S_Inx)))))                                         //Natural: IF NOT INTRFCE.INTRFCNG-SYSTM = #SYSTEM-INTFCE ( 1:#S-INX )
                        {
                            //*  HAPPEN
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM SUMMARISE-DATA
                        sub_Summarise_Data();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("PND_PND_L1105"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1105"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        getWorkFiles().write(1, false, vw_intrfce);                                                                                                       //Natural: WRITE WORK FILE 1 INTRFCE
                        pnd_Count.getValue(pnd_K).nadd(1);                                                                                                                //Natural: ADD 1 TO #COUNT ( #K )
                    }                                                                                                                                                     //Natural: END-READ
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("MAIN_LOOP"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("MAIN_LOOP"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_Status.getValue(pnd_K).equals("S") && pnd_Count.getValue(pnd_K).equals(getZero())))                                                 //Natural: IF #STATUS ( #K ) = 'S' AND #COUNT ( #K ) = 0
                    {
                        pnd_Report_3_Missing.setValue(true);                                                                                                              //Natural: MOVE TRUE TO #REPORT-3-MISSING
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                //*  NO DATA FOR ANY REP EXTRCTD FOR DATE
                if (condition(! (pnd_Count.getValue("*").notEquals(getZero()))))                                                                                          //Natural: IF NOT #COUNT ( * ) NOT = 0
                {
                    pnd_No_Data.setValue(true);                                                                                                                           //Natural: MOVE TRUE TO #NO-DATA
                    FOR01:                                                                                                                                                //Natural: FOR #P = 1 TO #P-MAX
                    for (pnd_P.setValue(1); condition(pnd_P.lessOrEqual(pnd_P_Max)); pnd_P.nadd(1))
                    {
                                                                                                                                                                          //Natural: PERFORM WRITE-NO-DATA
                        sub_Write_No_Data();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-FOR
                    if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM END-OF-JOB
                    sub_End_Of_Job();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getReports().closePrinter(2);                                                                                                                         //Natural: CLOSE PRINTER ( 01 )
                                                                                                                                                                          //Natural: PERFORM UPDATE-LR-INTERFACE-REPORT
                    sub_Update_Lr_Interface_Report();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    //*  STOP JOB
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                READWORK01:                                                                                                                                               //Natural: READ WORK 1 INTRFCE
                while (condition(getWorkFiles().read(1, vw_intrfce)))
                {
                    pnd_Break.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, intrfce_Intrfce_Stts, intrfce_Rcrd_Crtd_For_Bsnss_Dte));                           //Natural: COMPRESS INTRFCE.INTRFCE-STTS INTRFCE.RCRD-CRTD-FOR-BSNSS-DTE INTO #BREAK LEAVING NO
                    pnd_I.setValue(intrfce_Count_Casterror_Table);                                                                                                        //Natural: MOVE C*INTRFCE.ERROR-TABLE TO #I
                    if (condition(! (pnd_I.equals(getZero())) && intrfce_Error_Cde.getValue(pnd_I).equals(pnd_Same)))                                                     //Natural: IF NOT #I = 0 AND INTRFCE.ERROR-CDE ( #I ) = #SAME
                    {
                        pnd_Sort.setValue(9);                                                                                                                             //Natural: MOVE 9 TO #SORT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Sort.setValue(1);                                                                                                                             //Natural: MOVE 1 TO #SORT
                    }                                                                                                                                                     //Natural: END-IF
                    getSort().writeSortInData(pnd_Break, pnd_Sort, intrfce_Pin_Tiaa_Cntrct, intrfce_Tiaa_Cref_Ind, intrfce_Intrfce_Stts, intrfce_Rcrd_Crtd_For_Bsnss_Dte, //Natural: END-ALL
                        intrfce_Rcrd_Crtd_Dte, intrfce_Rcrd_Crtd_Tme, intrfce_Intrfcng_Systm, intrfce_Rqstng_Systm, intrfce_Rcrd_Last_Updt_Dte, intrfce_Rcrd_Last_Updt_Tme, 
                        intrfce_Rcrd_Last_Updt_Userid, intrfce_Last_Prcssd_Bsnss_Dte, intrfce_Intrfce_Cmpltd_Dte, intrfce_Intrfce_Cmpltd_Tme, intrfce_Dflt_To_Estate_Ind, 
                        intrfce_Illgble_Ind, intrfce_More_Than_Five_Benes_Ind, intrfce_Intrfce_Mgrtn_Ind, intrfce_More_Than_Thirty_Benes_Ind, intrfce_Count_Casterror_Table, 
                        intrfce_Error_Txt.getValue(1), intrfce_Error_Txt.getValue(2), intrfce_Error_Txt.getValue(3), intrfce_Error_Txt.getValue(4), intrfce_Error_Txt.getValue(5), 
                        intrfce_Error_Txt.getValue(6), intrfce_Error_Txt.getValue(7), intrfce_Error_Txt.getValue(8), intrfce_Error_Txt.getValue(9), intrfce_Error_Txt.getValue(10), 
                        intrfce_Error_Cde.getValue(1), intrfce_Error_Cde.getValue(2), intrfce_Error_Cde.getValue(3), intrfce_Error_Cde.getValue(4), intrfce_Error_Cde.getValue(5), 
                        intrfce_Error_Cde.getValue(6), intrfce_Error_Cde.getValue(7), intrfce_Error_Cde.getValue(8), intrfce_Error_Cde.getValue(9), intrfce_Error_Cde.getValue(10), 
                        intrfce_Error_Dte.getValue(1), intrfce_Error_Dte.getValue(2), intrfce_Error_Dte.getValue(3), intrfce_Error_Dte.getValue(4), intrfce_Error_Dte.getValue(5), 
                        intrfce_Error_Dte.getValue(6), intrfce_Error_Dte.getValue(7), intrfce_Error_Dte.getValue(8), intrfce_Error_Dte.getValue(9), intrfce_Error_Dte.getValue(10), 
                        intrfce_Error_Tme.getValue(1), intrfce_Error_Tme.getValue(2), intrfce_Error_Tme.getValue(3), intrfce_Error_Tme.getValue(4), intrfce_Error_Tme.getValue(5), 
                        intrfce_Error_Tme.getValue(6), intrfce_Error_Tme.getValue(7), intrfce_Error_Tme.getValue(8), intrfce_Error_Tme.getValue(9), intrfce_Error_Tme.getValue(10), 
                        intrfce_Error_Pgm.getValue(1), intrfce_Error_Pgm.getValue(2), intrfce_Error_Pgm.getValue(3), intrfce_Error_Pgm.getValue(4), intrfce_Error_Pgm.getValue(5), 
                        intrfce_Error_Pgm.getValue(6), intrfce_Error_Pgm.getValue(7), intrfce_Error_Pgm.getValue(8), intrfce_Error_Pgm.getValue(9), intrfce_Error_Pgm.getValue(10), 
                        intrfce_Fldr_Log_Dte_Tme, intrfce_Last_Dsgntn_Srce, intrfce_Last_Dsgntn_System, intrfce_Last_Dsgntn_Userid, intrfce_Last_Dsgntn_Dte, 
                        intrfce_Last_Dsgntn_Tme, intrfce_Tiaa_Cref_Chng_Dte, intrfce_Tiaa_Cref_Chng_Tme, intrfce_Chng_Pwr_Atty, intrfce_Cref_Cntrct, intrfce_Cntrct_Type, 
                        intrfce_Eff_Dte, intrfce_Mos_Ind, intrfce_Irrvcble_Ind, intrfce_Pymnt_Chld_Dcsd_Ind, intrfce_Exempt_Spouse_Rights, intrfce_Spouse_Waived_Bnfts, 
                        intrfce_Chng_New_Issue_Ind, intrfce_Same_As_Ind);
                }                                                                                                                                                         //Natural: END-WORK
                READWORK01_Exit:
                if (Global.isEscape()) return;
                getSort().sortData(pnd_Break, pnd_Sort, intrfce_Pin_Tiaa_Cntrct, intrfce_Tiaa_Cref_Ind);                                                                  //Natural: SORT BY #BREAK #SORT INTRFCE.PIN-TIAA-CNTRCT INTRFCE.TIAA-CREF-IND USING INTRFCE.INTRFCE-STTS INTRFCE.RCRD-CRTD-FOR-BSNSS-DTE INTRFCE.RCRD-CRTD-DTE INTRFCE.RCRD-CRTD-TME INTRFCE.INTRFCNG-SYSTM INTRFCE.RQSTNG-SYSTM INTRFCE.RCRD-LAST-UPDT-DTE INTRFCE.RCRD-LAST-UPDT-TME INTRFCE.RCRD-LAST-UPDT-USERID INTRFCE.LAST-PRCSSD-BSNSS-DTE INTRFCE.INTRFCE-CMPLTD-DTE INTRFCE.INTRFCE-CMPLTD-TME INTRFCE.DFLT-TO-ESTATE-IND INTRFCE.ILLGBLE-IND INTRFCE.MORE-THAN-FIVE-BENES-IND INTRFCE.INTRFCE-MGRTN-IND INTRFCE.MORE-THAN-THIRTY-BENES-IND INTRFCE.C*ERROR-TABLE INTRFCE.ERROR-TXT ( * ) INTRFCE.ERROR-CDE ( * ) INTRFCE.ERROR-DTE ( * ) INTRFCE.ERROR-TME ( * ) INTRFCE.ERROR-PGM ( * ) INTRFCE.FLDR-LOG-DTE-TME INTRFCE.LAST-DSGNTN-SRCE INTRFCE.LAST-DSGNTN-SYSTEM INTRFCE.LAST-DSGNTN-USERID INTRFCE.LAST-DSGNTN-DTE INTRFCE.LAST-DSGNTN-TME INTRFCE.TIAA-CREF-CHNG-DTE INTRFCE.TIAA-CREF-CHNG-TME INTRFCE.CHNG-PWR-ATTY INTRFCE.CREF-CNTRCT INTRFCE.CNTRCT-TYPE INTRFCE.EFF-DTE INTRFCE.MOS-IND INTRFCE.IRRVCBLE-IND INTRFCE.PYMNT-CHLD-DCSD-IND INTRFCE.EXEMPT-SPOUSE-RIGHTS INTRFCE.SPOUSE-WAIVED-BNFTS INTRFCE.CHNG-NEW-ISSUE-IND INTRFCE.SAME-AS-IND
                boolean endOfDataPnd_Pnd_L1345 = true;
                boolean firstPnd_Pnd_L1345 = true;
                PND_PND_L1345:
                while (condition(getSort().readSortOutData(pnd_Break, pnd_Sort, intrfce_Pin_Tiaa_Cntrct, intrfce_Tiaa_Cref_Ind, intrfce_Intrfce_Stts, 
                    intrfce_Rcrd_Crtd_For_Bsnss_Dte, intrfce_Rcrd_Crtd_Dte, intrfce_Rcrd_Crtd_Tme, intrfce_Intrfcng_Systm, intrfce_Rqstng_Systm, intrfce_Rcrd_Last_Updt_Dte, 
                    intrfce_Rcrd_Last_Updt_Tme, intrfce_Rcrd_Last_Updt_Userid, intrfce_Last_Prcssd_Bsnss_Dte, intrfce_Intrfce_Cmpltd_Dte, intrfce_Intrfce_Cmpltd_Tme, 
                    intrfce_Dflt_To_Estate_Ind, intrfce_Illgble_Ind, intrfce_More_Than_Five_Benes_Ind, intrfce_Intrfce_Mgrtn_Ind, intrfce_More_Than_Thirty_Benes_Ind, 
                    intrfce_Count_Casterror_Table, intrfce_Error_Txt.getValue(1), intrfce_Error_Txt.getValue(2), intrfce_Error_Txt.getValue(3), intrfce_Error_Txt.getValue(4), 
                    intrfce_Error_Txt.getValue(5), intrfce_Error_Txt.getValue(6), intrfce_Error_Txt.getValue(7), intrfce_Error_Txt.getValue(8), intrfce_Error_Txt.getValue(9), 
                    intrfce_Error_Txt.getValue(10), intrfce_Error_Cde.getValue(1), intrfce_Error_Cde.getValue(2), intrfce_Error_Cde.getValue(3), intrfce_Error_Cde.getValue(4), 
                    intrfce_Error_Cde.getValue(5), intrfce_Error_Cde.getValue(6), intrfce_Error_Cde.getValue(7), intrfce_Error_Cde.getValue(8), intrfce_Error_Cde.getValue(9), 
                    intrfce_Error_Cde.getValue(10), intrfce_Error_Dte.getValue(1), intrfce_Error_Dte.getValue(2), intrfce_Error_Dte.getValue(3), intrfce_Error_Dte.getValue(4), 
                    intrfce_Error_Dte.getValue(5), intrfce_Error_Dte.getValue(6), intrfce_Error_Dte.getValue(7), intrfce_Error_Dte.getValue(8), intrfce_Error_Dte.getValue(9), 
                    intrfce_Error_Dte.getValue(10), intrfce_Error_Tme.getValue(1), intrfce_Error_Tme.getValue(2), intrfce_Error_Tme.getValue(3), intrfce_Error_Tme.getValue(4), 
                    intrfce_Error_Tme.getValue(5), intrfce_Error_Tme.getValue(6), intrfce_Error_Tme.getValue(7), intrfce_Error_Tme.getValue(8), intrfce_Error_Tme.getValue(9), 
                    intrfce_Error_Tme.getValue(10), intrfce_Error_Pgm.getValue(1), intrfce_Error_Pgm.getValue(2), intrfce_Error_Pgm.getValue(3), intrfce_Error_Pgm.getValue(4), 
                    intrfce_Error_Pgm.getValue(5), intrfce_Error_Pgm.getValue(6), intrfce_Error_Pgm.getValue(7), intrfce_Error_Pgm.getValue(8), intrfce_Error_Pgm.getValue(9), 
                    intrfce_Error_Pgm.getValue(10), intrfce_Fldr_Log_Dte_Tme, intrfce_Last_Dsgntn_Srce, intrfce_Last_Dsgntn_System, intrfce_Last_Dsgntn_Userid, 
                    intrfce_Last_Dsgntn_Dte, intrfce_Last_Dsgntn_Tme, intrfce_Tiaa_Cref_Chng_Dte, intrfce_Tiaa_Cref_Chng_Tme, intrfce_Chng_Pwr_Atty, intrfce_Cref_Cntrct, 
                    intrfce_Cntrct_Type, intrfce_Eff_Dte, intrfce_Mos_Ind, intrfce_Irrvcble_Ind, intrfce_Pymnt_Chld_Dcsd_Ind, intrfce_Exempt_Spouse_Rights, 
                    intrfce_Spouse_Waived_Bnfts, intrfce_Chng_New_Issue_Ind, intrfce_Same_As_Ind)))
                {
                    CheckAtStartofData412();

                    if (condition(getSort().getAstCOUNTER().greater(0)))
                    {
                        atBreakEventPnd_Pnd_L1345(false);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom()))
                                break;
                            else if (condition(Global.isEscapeBottomImmediate()))
                            {
                                endOfDataPnd_Pnd_L1345 = false;
                                break;
                            }
                            else if (condition(Global.isEscapeTop()))
                            continue;
                            else if (condition())
                            return;
                        }
                    }
                    //*                                                                                                                                                   //Natural: AT START OF DATA
                    //*                                                                                                                                                   //Natural: AT BREAK OF #BREAK
                    pnd_On_A_Break.reset();                                                                                                                               //Natural: RESET #ON-A-BREAK
                                                                                                                                                                          //Natural: PERFORM PRINT-REPORTS
                    sub_Print_Reports();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PND_PND_L1345"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1345"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    //* ***********************************************************************
                    //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: END-OF-REPORT
                    //* ***********************************************************************
                    //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-SUMMARY
                    //* ***********************************************************************
                    //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-SUMMARY-DETAIL
                    //* ***********************************************************************
                    //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-REPORTS
                    //* ***********************************************************************
                    //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: END-OF-JOB
                    //* ***********************************************************************
                    //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-BREAK-STUFF
                    //* ***********************************************************************
                    //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DATE-BREAK-STUFF
                    //* ***********************************************************************
                    //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-HEADERS
                    //* ***********************************************************************
                    //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-NO-DATA
                    //* ***********************************************************************
                    //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-CORRECT-REPORT
                    //*  (1275)
                }                                                                                                                                                         //Natural: END-SORT
                if (condition(getSort().getAstCOUNTER().greater(0)))
                {
                    atBreakEventPnd_Pnd_L1345(endOfDataPnd_Pnd_L1345);
                }
                endSort();
                pnd_End_Of_Job.setValue(true);                                                                                                                            //Natural: MOVE TRUE TO #END-OF-JOB
                                                                                                                                                                          //Natural: PERFORM END-OF-REPORT
                sub_End_Of_Report();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //* ***********************************************************************
                //* ***********************************************************************
                //* ***********************************************************************
                //* ***********************************************************************
                //* ***********************************************************************
                //* ***********************************************************************
                //* ***********************************************************************
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( 01 )
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( 04 )
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( 00 )
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-TOTALS
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_End_Of_Report() throws Exception                                                                                                                     //Natural: END-OF-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  IE, NOT REPORT 4
        if (condition(pnd_P.less(pnd_P_Max)))                                                                                                                             //Natural: IF #P LT #P-MAX
        {
            //*  CF 02/05/04
                                                                                                                                                                          //Natural: PERFORM PRINT-TOTALS
            sub_Print_Totals();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(45),pnd_Eor);                                                                       //Natural: WRITE ( 01 ) // 45T #EOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*                     /* FINALLY - PRINT SUMMARY REPORT AFTER REPT 3
        if (condition(((! ((pnd_P.greaterOrEqual(1) && pnd_P.lessOrEqual(2))) || (pnd_Report_3_Missing.getBoolean() && pnd_End_Of_Job.getBoolean())) &&                   //Natural: IF ( ( NOT #P = 1 THRU 2 ) OR ( #REPORT-3-MISSING AND #END-OF-JOB ) ) AND NOT #D-I = 0
            ! (pnd_D_I.equals(getZero())))))
        {
                                                                                                                                                                          //Natural: PERFORM PRINT-SUMMARY
            sub_Print_Summary();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  END-OF-REPORT
    }
    private void sub_Print_Summary() throws Exception                                                                                                                     //Natural: PRINT-SUMMARY
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*                         /* DATES MAY BE IN ODD ORDERS DEPENDING ON
        //*                         /* DIFFERENT STATII, SO DATES ARE 'sorted' HERE
        pnd_P.setValue(pnd_P_Max);                                                                                                                                        //Natural: MOVE #P-MAX TO #P
                                                                                                                                                                          //Natural: PERFORM DEFINE-PRINTER
        sub_Define_Printer();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        PND_PND_L1820:                                                                                                                                                    //Natural: REPEAT
        while (condition(whileTrue))
        {
            //*  IE, ALL DATES HAVE BEEN PRINTED
            if (condition(! (pnd_Dates_Pnd_Date.getValue("*").notEquals(" "))))                                                                                           //Natural: IF NOT #DATE ( * ) NOT = ' '
            {
                pnd_D_I.reset();                                                                                                                                          //Natural: RESET #D-I
                if (true) break PND_PND_L1820;                                                                                                                            //Natural: ESCAPE BOTTOM ( ##L1820. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Date_Min.resetInitial();                                                                                                                                  //Natural: RESET INITIAL #DATE-MIN
            FOR02:                                                                                                                                                        //Natural: FOR #I = 1 TO #D-MAX
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_D_Max)); pnd_I.nadd(1))
            {
                if (condition(pnd_Dates_Pnd_Date.getValue(pnd_I.getInt() + 1).less(pnd_Date_Min) && pnd_Dates_Pnd_Date.getValue(pnd_I.getInt() + 1).notEquals(" ")))      //Natural: IF #DATE ( #I ) LT #DATE-MIN AND #DATE ( #I ) NE ' '
                {
                    pnd_Date_Min.setValue(pnd_Dates_Pnd_Date.getValue(pnd_I.getInt() + 1));                                                                               //Natural: MOVE #DATE ( #I ) TO #DATE-MIN
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L1820"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1820"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  LOWEST DATE ON TABLE
            DbsUtil.examine(new ExamineSource(pnd_Dates_Pnd_Date.getValue("*")), new ExamineSearch(pnd_Date_Min), new ExamineGivingIndex(pnd_I));                         //Natural: EXAMINE #DATE ( * ) FOR #DATE-MIN INDEX #I
            pnd_Datd.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Dates_Pnd_Date.getValue(pnd_I.getInt() + 1));                                                      //Natural: MOVE EDITED #DATE ( #I ) TO #DATD ( EM = YYYYMMDD )
            if (condition(getReports().getAstLinesLeft(4).less(25)))                                                                                                      //Natural: NEWPAGE ( 04 ) LESS THAN 25 LINES LEFT
            {
                getReports().newPage(4);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PND_PND_L1820"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1820"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }
            getReports().print(4, NEWLINE,NEWLINE,NEWLINE,pnd_Start_Date,pnd_Datd,NEWLINE,NEWLINE);                                                                       //Natural: PRINT ( 04 ) /// #START-DATE #DATD //
            pnd_Dates_Pnd_Numbers.getValue(pnd_I.getInt() + 1,pnd_S_Tot,1).nadd(pnd_Dates_Pnd_Numbers.getValue(pnd_I.getInt() + 1,1,":",pnd_S_Inx,1));                    //Natural: ADD #NUMBERS ( #I,1:#S-INX,1 ) TO #NUMBERS ( #I,#S-TOT,1 )
            pnd_Dates_Pnd_Numbers.getValue(pnd_I.getInt() + 1,pnd_S_Tot,2).nadd(pnd_Dates_Pnd_Numbers.getValue(pnd_I.getInt() + 1,1,":",pnd_S_Inx,2));                    //Natural: ADD #NUMBERS ( #I,1:#S-INX,2 ) TO #NUMBERS ( #I,#S-TOT,2 )
            pnd_Dates_Pnd_Numbers.getValue(pnd_I.getInt() + 1,pnd_S_Tot,3).nadd(pnd_Dates_Pnd_Numbers.getValue(pnd_I.getInt() + 1,1,":",pnd_S_Inx,3));                    //Natural: ADD #NUMBERS ( #I,1:#S-INX,3 ) TO #NUMBERS ( #I,#S-TOT,3 )
            pnd_Dates_Pnd_Numbers.getValue(pnd_I.getInt() + 1,pnd_S_Tot,4).nadd(pnd_Dates_Pnd_Numbers.getValue(pnd_I.getInt() + 1,1,":",pnd_S_Inx,4));                    //Natural: ADD #NUMBERS ( #I,1:#S-INX,4 ) TO #NUMBERS ( #I,#S-TOT,4 )
            pnd_Dates_Pnd_Numbers.getValue(pnd_I.getInt() + 1,pnd_S_Tot,5).nadd(pnd_Dates_Pnd_Numbers.getValue(pnd_I.getInt() + 1,1,":",pnd_S_Inx,5));                    //Natural: ADD #NUMBERS ( #I,1:#S-INX,5 ) TO #NUMBERS ( #I,#S-TOT,5 )
            //*  GRAND TOTAL
            pnd_Dates_Pnd_Numbers.getValue(0 + 1,pnd_S_Tot,1).nadd(pnd_Dates_Pnd_Numbers.getValue(pnd_I.getInt() + 1,1,":",pnd_S_Inx,1));                                 //Natural: ADD #NUMBERS ( #I,1:#S-INX,1 ) TO #NUMBERS ( 0,#S-TOT,1 )
            pnd_Dates_Pnd_Numbers.getValue(0 + 1,pnd_S_Tot,2).nadd(pnd_Dates_Pnd_Numbers.getValue(pnd_I.getInt() + 1,1,":",pnd_S_Inx,2));                                 //Natural: ADD #NUMBERS ( #I,1:#S-INX,2 ) TO #NUMBERS ( 0,#S-TOT,2 )
            pnd_Dates_Pnd_Numbers.getValue(0 + 1,pnd_S_Tot,3).nadd(pnd_Dates_Pnd_Numbers.getValue(pnd_I.getInt() + 1,1,":",pnd_S_Inx,3));                                 //Natural: ADD #NUMBERS ( #I,1:#S-INX,3 ) TO #NUMBERS ( 0,#S-TOT,3 )
            pnd_Dates_Pnd_Numbers.getValue(0 + 1,pnd_S_Tot,4).nadd(pnd_Dates_Pnd_Numbers.getValue(pnd_I.getInt() + 1,1,":",pnd_S_Inx,4));                                 //Natural: ADD #NUMBERS ( #I,1:#S-INX,4 ) TO #NUMBERS ( 0,#S-TOT,4 )
            pnd_Dates_Pnd_Numbers.getValue(0 + 1,pnd_S_Tot,5).nadd(pnd_Dates_Pnd_Numbers.getValue(pnd_I.getInt() + 1,1,":",pnd_S_Inx,5));                                 //Natural: ADD #NUMBERS ( #I,1:#S-INX,5 ) TO #NUMBERS ( 0,#S-TOT,5 )
            FOR03:                                                                                                                                                        //Natural: FOR #S-I = 1 TO #R-MAX
            for (pnd_S_I.setValue(1); condition(pnd_S_I.lessOrEqual(pnd_R_Max)); pnd_S_I.nadd(1))
            {
                if (condition(pnd_S_I.equals(pnd_R_Max)))                                                                                                                 //Natural: IF #S-I = #R-MAX
                {
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-IF
                //*  JW 9/07/00
                if (condition(pnd_System_Pnd_System_Intfce.getValue(pnd_S_I).equals(pnd_Sub_Total)))                                                                      //Natural: IF #SYSTEM-INTFCE ( #S-I ) = #SUB-TOTAL
                {
                    pnd_M.compute(new ComputeParameters(false, pnd_M), pnd_S_I.subtract(1));                                                                              //Natural: COMPUTE #M = #S-I - 1
                    pnd_Hold_System.setValue(pnd_System_Pnd_System_Intfce.getValue(pnd_M));                                                                               //Natural: MOVE #SYSTEM-INTFCE ( #M ) TO #HOLD-SYSTEM
                    FOR04:                                                                                                                                                //Natural: FOR #M #M 1 STEP -1
                    for (pnd_M.setValue(pnd_M); condition(pnd_M.greaterOrEqual(1)); pnd_M.nsubtract(1))
                    {
                        if (condition(pnd_System_Pnd_System_Intfce.getValue(pnd_M).notEquals(pnd_Hold_System)))                                                           //Natural: IF #SYSTEM-INTFCE ( #M ) <> #HOLD-SYSTEM
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Dates_Pnd_Numbers.getValue(pnd_I.getInt() + 1,pnd_S_I,1).nadd(pnd_Dates_Pnd_Numbers.getValue(pnd_I.getInt() + 1,pnd_M,1));                    //Natural: ADD #NUMBERS ( #I,#M,1 ) TO #NUMBERS ( #I,#S-I,1 )
                        pnd_Dates_Pnd_Numbers.getValue(pnd_I.getInt() + 1,pnd_S_I,2).nadd(pnd_Dates_Pnd_Numbers.getValue(pnd_I.getInt() + 1,pnd_M,2));                    //Natural: ADD #NUMBERS ( #I,#M,2 ) TO #NUMBERS ( #I,#S-I,2 )
                        pnd_Dates_Pnd_Numbers.getValue(pnd_I.getInt() + 1,pnd_S_I,3).nadd(pnd_Dates_Pnd_Numbers.getValue(pnd_I.getInt() + 1,pnd_M,3));                    //Natural: ADD #NUMBERS ( #I,#M,3 ) TO #NUMBERS ( #I,#S-I,3 )
                        pnd_Dates_Pnd_Numbers.getValue(pnd_I.getInt() + 1,pnd_S_I,4).nadd(pnd_Dates_Pnd_Numbers.getValue(pnd_I.getInt() + 1,pnd_M,4));                    //Natural: ADD #NUMBERS ( #I,#M,4 ) TO #NUMBERS ( #I,#S-I,4 )
                        pnd_Dates_Pnd_Numbers.getValue(pnd_I.getInt() + 1,pnd_S_I,5).nadd(pnd_Dates_Pnd_Numbers.getValue(pnd_I.getInt() + 1,pnd_M,5));                    //Natural: ADD #NUMBERS ( #I,#M,5 ) TO #NUMBERS ( #I,#S-I,5 )
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  JW 9/07/00
                }                                                                                                                                                         //Natural: END-IF
                pnd_Numbers_P.getValue("*").setValue(pnd_Dates_Pnd_Numbers.getValue(pnd_I.getInt() + 1,pnd_S_I,"*"));                                                     //Natural: MOVE #NUMBERS ( #I,#S-I,* ) TO #NUMBERS-P ( * )
                                                                                                                                                                          //Natural: PERFORM PRINT-SUMMARY-DETAIL
                sub_Print_Summary_Detail();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L1820"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1820"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  WIPE IT OUT AND CHECK FOR NEXT <EST
            pnd_Dates_Pnd_Date.getValue(pnd_I.getInt() + 1).reset();                                                                                                      //Natural: RESET #DATE ( #I )
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        getReports().skip(4, 3);                                                                                                                                          //Natural: SKIP ( 04 ) 3
        if (condition(getReports().getAstLinesLeft(4).less(3)))                                                                                                           //Natural: NEWPAGE ( 04 ) IF LESS THAN 3 LINES LEFT
        {
            getReports().newPage(4);
            if (condition(Global.isEscape())){return;}
        }
        pnd_Numbers_P.getValue("*").setValue(pnd_Dates_Pnd_Numbers.getValue(0 + 1,pnd_S_Tot,"*"));                                                                        //Natural: MOVE #NUMBERS ( 0,#S-TOT,* ) TO #NUMBERS-P ( * )
        pnd_System.getValue(pnd_S_Tot).setValue("Grand Total");                                                                                                           //Natural: MOVE 'Grand Total' TO #SYSTEM ( #S-TOT )
        pnd_S_I.setValue(pnd_S_Tot);                                                                                                                                      //Natural: MOVE #S-TOT TO #S-I
                                                                                                                                                                          //Natural: PERFORM PRINT-SUMMARY-DETAIL
        sub_Print_Summary_Detail();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        getReports().write(4, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(45),pnd_Eor);                                                                           //Natural: WRITE ( 04 ) // 45T #EOR
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM UPDATE-LR-INTERFACE-REPORT
        sub_Update_Lr_Interface_Report();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        //*  PRINT-SUMMARY
    }
    private void sub_Print_Summary_Detail() throws Exception                                                                                                              //Natural: PRINT-SUMMARY-DETAIL
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  JW 1/12/00
        DbsUtil.examine(new ExamineSource(pnd_System_Pnd_System_Intfce.getValue(pnd_S_I)), new ExamineSearch("Total"), new ExamineGivingIndex(pnd_T));                    //Natural: EXAMINE #SYSTEM-INTFCE ( #S-I ) FOR 'Total' INDEX #T
        if (condition(! (pnd_T.equals(getZero()))))                                                                                                                       //Natural: IF NOT #T = 0
        {
            //*  JW 9/07/00
            if (condition(pnd_System_Pnd_System_Intfce.getValue(pnd_S_I).equals(pnd_Sub_Total)))                                                                          //Natural: IF #SYSTEM-INTFCE ( #S-I ) = #SUB-TOTAL
            {
                getReports().write(4, new ReportZeroPrint (true),new SignPosition (false),ReportOption.NOTITLE,new TabSetting(3),"_",new RepeatItem(130));                //Natural: WRITE ( 04 ) ( ZP = ON SG = OFF ) 3T '_' ( 130 )
                if (Global.isEscape()) return;
                //*  JW 9/07/00
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(4, new ReportZeroPrint (true),new SignPosition (false),ReportOption.NOTITLE,"_",new RepeatItem(132));                                  //Natural: WRITE ( 04 ) ( ZP = ON SG = OFF ) '_' ( 132 )
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  JW 1/12/00
        //*  JW 1/12/00
        getReports().display(4, new ReportZeroPrint (true),new SignPosition (false),"Interfacing/System",                                                                 //Natural: DISPLAY ( 04 ) ( ZP = ON SG = OFF ) 'Interfacing/System' #SYSTEM-INTFCE ( #S-I ) 'Requesting/System' #SYSTEM-REQST ( #S-I ) 40T 'Total Designations/Read from Interface File' #NUMBERS-P ( 01 ) ( EM = ����������Z ( 6 ) 9 ) 10X 'Total Designations/Written to Bene File' #NUMBERS-P ( 02 ) ( EM = ��������Z ( 6 ) 9 ) 10X 'Total/Errors' #NUMBERS-P ( 03 ) 10X 'To Be/Retried' #NUMBERS-P ( 04 )
        		pnd_System_Pnd_System_Intfce.getValue(pnd_S_I),"Requesting/System",
        		pnd_System_Pnd_System_Reqst.getValue(pnd_S_I),new TabSetting(40),"Total Designations/Read from Interface File",
        		pnd_Numbers_P.getValue(1), new ReportEditMask ("          ZZZZZZ9"),new ColumnSpacing(10),"Total Designations/Written to Bene File",
        		pnd_Numbers_P.getValue(2), new ReportEditMask ("        ZZZZZZ9"),new ColumnSpacing(10),"Total/Errors",
        		pnd_Numbers_P.getValue(3),new ColumnSpacing(10),"To Be/Retried",
        		pnd_Numbers_P.getValue(4));
        if (Global.isEscape()) return;
        //*  JW 9/07/00
        if (condition(pnd_System_Pnd_System_Intfce.getValue(pnd_S_I).equals(pnd_Sub_Total)))                                                                              //Natural: IF #SYSTEM-INTFCE ( #S-I ) = #SUB-TOTAL
        {
            getReports().write(4, new ReportZeroPrint (true),new SignPosition (false),ReportOption.NOTITLE,new TabSetting(133),"!");                                      //Natural: WRITE ( 04 ) ( ZP = ON SG = OFF ) 133T '!'
            if (Global.isEscape()) return;
            //*  JW 9/07/00
        }                                                                                                                                                                 //Natural: END-IF
        //*  PRINT-SUMMARY-DETAIL
    }
    private void sub_Print_Reports() throws Exception                                                                                                                     //Natural: PRINT-REPORTS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pdaBena008.getPnd_Bena008().reset();                                                                                                                              //Natural: RESET #BENA008
        pdaBena008.getPnd_Bena008_Pnd_Pin().setValue(intrfce_Pin);                                                                                                        //Natural: MOVE INTRFCE.PIN TO #BENA008.#PIN
        DbsUtil.callnat(Benn008.class , getCurrentProcessState(), pdaBena008.getPnd_Bena008());                                                                           //Natural: CALLNAT 'BENN008' #BENA008
        if (condition(Global.isEscape())) return;
        if (condition(! (pdaBena008.getPnd_Bena008_Pnd_Rtrn_Msg().equals(" "))))                                                                                          //Natural: IF NOT #BENA008.#RTRN-MSG = ' '
        {
            pdaBena008.getPnd_Bena008_Pnd_Nas_Name().setValue(pdaBena008.getPnd_Bena008_Pnd_Rtrn_Msg(), MoveOption.LeftJustified);                                        //Natural: MOVE LEFT #BENA008.#RTRN-MSG TO #NAS-NAME
        }                                                                                                                                                                 //Natural: END-IF
        pnd_I.setValue(intrfce_Count_Casterror_Table);                                                                                                                    //Natural: MOVE C*INTRFCE.ERROR-TABLE TO #I
        //*  EXCEPTION DETAIL REPORT
        short decideConditionsMet648 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #P;//Natural: VALUE 1
        if (condition((pnd_P.equals(1))))
        {
            decideConditionsMet648++;
            getReports().display(1, new ReportEmptyLineSuppression(true),ReportOption.NOHDR,intrfce_Pin_Tiaa_Cntrct,intrfce_Tiaa_Cref_Ind, new ReportEditMask             //Natural: DISPLAY ( 01 ) NOHDR ( ES = ON ) INTRFCE.PIN-TIAA-CNTRCT INTRFCE.TIAA-CREF-IND ( EM = X ) #NAS-NAME ( AL = 24 ) INTRFCE.INTRFCNG-SYSTM INTRFCE.RQSTNG-SYSTM INTRFCE.ERROR-TXT ( #I ) ( EM = X ( 64 ) )
                ("X"),pdaBena008.getPnd_Bena008_Pnd_Nas_Name(), new AlphanumericLength (24),intrfce_Intrfcng_Systm,intrfce_Rqstng_Systm,intrfce_Error_Txt.getValue(pnd_I), 
                new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"));
            if (Global.isEscape()) return;
            //*  RETRY DETAIL REPORT
            pnd_Total_Error.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #TOTAL-ERROR
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((pnd_P.equals(2))))
        {
            decideConditionsMet648++;
            //*  PIN OR COR
            short decideConditionsMet656 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE #I;//Natural: VALUE 1
            if (condition((pnd_I.equals(1))))
            {
                decideConditionsMet656++;
                //*  1ST FOR EITHER PIN OR COR
                //*  PIN OR COR
                pnd_Attempt.setValue(pnd_Attempt_A.getValue(pnd_I.getInt() + 1));                                                                                         //Natural: MOVE #ATTEMPT-A ( #I ) TO #ATTEMPT
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_I.equals(2))))
            {
                decideConditionsMet656++;
                if (condition(intrfce_Error_Cde.getValue(2).equals("R") || intrfce_Error_Cde.getValue(1).equals("PR")))                                                   //Natural: IF INTRFCE.ERROR-CDE ( 2 ) = 'R' OR INTRFCE.ERROR-CDE ( 1 ) = 'PR'
                {
                    //*  2ND FOR EITHER PIN OR COR
                    pnd_Attempt.setValue(pnd_Attempt_A.getValue(2 + 1));                                                                                                  //Natural: MOVE #ATTEMPT-A ( 2 ) TO #ATTEMPT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  MUST BE 1ST PIN RETRY
                    pnd_Attempt.setValue(pnd_Attempt_A.getValue(1 + 1));                                                                                                  //Natural: MOVE #ATTEMPT-A ( 1 ) TO #ATTEMPT
                }                                                                                                                                                         //Natural: END-IF
                //*  CAN ONLY BE PIN
                pnd_Total_Retry.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #TOTAL-RETRY
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((pnd_I.equals(3))))
            {
                decideConditionsMet656++;
                if (condition(intrfce_Error_Cde.getValue(2).equals("R") && intrfce_Error_Cde.getValue(1).equals("R")))                                                    //Natural: IF INTRFCE.ERROR-CDE ( 2 ) = 'R' AND INTRFCE.ERROR-CDE ( 1 ) = 'R'
                {
                    //*  MUST BE 1ST PIN RETRY
                    pnd_Attempt.setValue(pnd_Attempt_A.getValue(1 + 1));                                                                                                  //Natural: MOVE #ATTEMPT-A ( 1 ) TO #ATTEMPT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  2ND FOR PIN
                    pnd_Attempt.setValue(pnd_Attempt_A.getValue(2 + 1));                                                                                                  //Natural: MOVE #ATTEMPT-A ( 2 ) TO #ATTEMPT
                    //*  CAN ONLY BE PIN
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 4
            else if (condition((pnd_I.equals(4))))
            {
                decideConditionsMet656++;
                //*   2ND FOR PIN
                pnd_Attempt.setValue(pnd_Attempt_A.getValue(2 + 1));                                                                                                      //Natural: MOVE #ATTEMPT-A ( 2 ) TO #ATTEMPT
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                //* SHOULDN't happen - max 2 retries each
                pnd_Attempt.setValue("???");                                                                                                                              //Natural: MOVE '???' TO #ATTEMPT
                //*  JW 1/26/00 TO HERE
            }                                                                                                                                                             //Natural: END-DECIDE
            getReports().display(1, new ReportEmptyLineSuppression(true),ReportOption.NOTITLE,intrfce_Pin_Tiaa_Cntrct,intrfce_Tiaa_Cref_Ind, new ReportEditMask           //Natural: DISPLAY ( 01 ) NOTITLE ( ES = ON ) INTRFCE.PIN-TIAA-CNTRCT INTRFCE.TIAA-CREF-IND ( EM = X ) #NAS-NAME ( AL = 20 ) #ATTEMPT ( EM = XXX ) INTRFCE.INTRFCNG-SYSTM INTRFCE.RQSTNG-SYSTM INTRFCE.ERROR-TXT ( #I ) ( EM = X ( 64 ) )
                ("X"),pdaBena008.getPnd_Bena008_Pnd_Nas_Name(), new AlphanumericLength (20),pnd_Attempt, new ReportEditMask ("XXX"),intrfce_Intrfcng_Systm,intrfce_Rqstng_Systm,intrfce_Error_Txt.getValue(pnd_I), 
                new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"));
            if (Global.isEscape()) return;
            //*  SUCCESSFUL INTRFCE DETAIL REPORT
            pnd_Total_Error.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #TOTAL-ERROR
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((pnd_P.equals(3))))
        {
            decideConditionsMet648++;
            if (condition(! (intrfce_Fldr_Log_Dte_Tme.equals(" "))))                                                                                                      //Natural: IF NOT INTRFCE.FLDR-LOG-DTE-TME = ' '
            {
                pnd_Fldr.setValue(true);                                                                                                                                  //Natural: MOVE TRUE TO #FLDR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Fldr.reset();                                                                                                                                         //Natural: RESET #FLDR
            }                                                                                                                                                             //Natural: END-IF
            getReports().display(1, new ReportEmptyLineSuppression(true),ReportOption.NOTITLE,intrfce_Pin_Tiaa_Cntrct,intrfce_Tiaa_Cref_Ind, new ReportEditMask           //Natural: DISPLAY ( 01 ) NOTITLE ( ES = ON ) INTRFCE.PIN-TIAA-CNTRCT INTRFCE.TIAA-CREF-IND ( EM = X ) #NAS-NAME INTRFCE.INTRFCNG-SYSTM 4X INTRFCE.RQSTNG-SYSTM 10X #FLDR ( EM = ' '/'Y' )
                ("X"),pdaBena008.getPnd_Bena008_Pnd_Nas_Name(),intrfce_Intrfcng_Systm,new ColumnSpacing(4),intrfce_Rqstng_Systm,new ColumnSpacing(10),pnd_Fldr.getBoolean(), 
                new ReportEditMask ("' '/'Y'"));
            if (Global.isEscape()) return;
            //*  INTERFACE SUMMARY REPORT
            //*  SUMMARY PRINTS IN END-OF-REPR
            pnd_Total_Success.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #TOTAL-SUCCESS
        }                                                                                                                                                                 //Natural: VALUE 4
        else if (condition((pnd_P.equals(4))))
        {
            decideConditionsMet648++;
            ignore();
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 01 ) 1
        //*  PRINT-REPORTS
    }
    private void sub_End_Of_Job() throws Exception                                                                                                                        //Natural: END-OF-JOB
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
                                                                                                                                                                          //Natural: PERFORM END-OF-REPORT
        sub_End_Of_Report();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        //*  END-OF-JOB
    }
    private void sub_Report_Break_Stuff() throws Exception                                                                                                                //Natural: REPORT-BREAK-STUFF
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  IE, BUSY WRITING A REPORT
        if (condition(! (pnd_P.equals(getZero()))))                                                                                                                       //Natural: IF NOT #P = 0
        {
                                                                                                                                                                          //Natural: PERFORM END-OF-REPORT
            sub_End_Of_Report();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CHECK-CORRECT-REPORT
        sub_Check_Correct_Report();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM DEFINE-PRINTER
        sub_Define_Printer();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM DATE-BREAK-STUFF
        sub_Date_Break_Stuff();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        //*  REPORT-BREAK-STUFF
    }
    private void sub_Date_Break_Stuff() throws Exception                                                                                                                  //Natural: DATE-BREAK-STUFF
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  SUMMARY REPORT IS NOT PRINTED HERE
        if (condition(pnd_P.equals(4)))                                                                                                                                   //Natural: IF #P = 4
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet744 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #P;//Natural: VALUE 1
        if (condition((pnd_P.equals(1))))
        {
            decideConditionsMet744++;
            if (condition(getReports().getAstLinesLeft(1).less(12)))                                                                                                      //Natural: NEWPAGE ( 01 ) IF LESS THAN 12 LINES
            {
                getReports().newPage(1);
                if (condition(Global.isEscape())){return;}
            }
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Datd.setValueEdited(new ReportEditMask("YYYYMMDD"),intrfce_Rcrd_Crtd_For_Bsnss_Dte);                                                                          //Natural: MOVE EDITED INTRFCE.RCRD-CRTD-FOR-BSNSS-DTE TO #DATD ( EM = YYYYMMDD )
        pnd_Break_X_Pnd_Rcrd_Crtd_For_Bsnss_Dte.setValue(intrfce_Rcrd_Crtd_For_Bsnss_Dte);                                                                                //Natural: MOVE INTRFCE.RCRD-CRTD-FOR-BSNSS-DTE TO #BREAK-X.#RCRD-CRTD-FOR-BSNSS-DTE
        getReports().print(1, ReportOption.NOHDR,NEWLINE,NEWLINE,NEWLINE,pnd_Start_Date,pnd_Datd,NEWLINE,NEWLINE);                                                        //Natural: PRINT ( 01 ) NOHDR /// #START-DATE #DATD //
        getReports().print(0, pnd_Start_Date,pnd_Datd,"for report",pnd_P,pnd_Status.getValue(pnd_P));                                                                     //Natural: PRINT #START-DATE #DATD 'for report' #P #STATUS ( #P )
                                                                                                                                                                          //Natural: PERFORM WRITE-HEADERS
        sub_Write_Headers();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        //*  DATE-BREAK-STUFF
    }
    private void sub_Write_Headers() throws Exception                                                                                                                     //Natural: WRITE-HEADERS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_No_Data.getBoolean()))                                                                                                                          //Natural: IF #NO-DATA
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  EXCEPTION DETAIL REPORT
        short decideConditionsMet768 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #P;//Natural: VALUE 1
        if (condition((pnd_P.equals(1))))
        {
            decideConditionsMet768++;
            //*  RETRY DETAIL REPORT
            getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,"                      T                          Intrfcng Rquestng",NEWLINE,"   PIN       Contract C Name                      System   System ","Error Message",NEWLINE,"------------ -------- - ------------------------ -------- --------","-",new  //Natural: WRITE ( 01 ) NOHDR '                      T                          Intrfcng Rquestng' / '   PIN       Contract C Name                      System   System ' 'Error Message' / '------------ -------- - ------------------------ -------- --------' '-' ( 64 )
                RepeatItem(64));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((pnd_P.equals(2))))
        {
            decideConditionsMet768++;
            //*  SUCCESSFUL INTRFCE DETAIL REPORT
            getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,"                      T                          Intrfcng Rquestng",NEWLINE,"   PIN       Contract C Name                 Try  System   System ","Error Message",NEWLINE,"------------ -------- - -------------------- --- -------- --------","-",new  //Natural: WRITE ( 01 ) NOHDR '                      T                          Intrfcng Rquestng' / '   PIN       Contract C Name                 Try  System   System ' 'Error Message' / '------------ -------- - -------------------- --- -------- --------' '-' ( 64 )
                RepeatItem(64));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((pnd_P.equals(3))))
        {
            decideConditionsMet768++;
            //*  INTERFACE SUMMARY REPORT
            //*  WRITTEN BY MAP
            getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,"                                                            ","                        MIT Log  ", //Natural: WRITE ( 01 ) NOHDR '                                                            ' '                        MIT Log  ' / '                      T                                    ' 'Interfacing Requesting  Date/Time' / '   PIN       Contract C Name Associated with Contract      ' '   System     System    Available?' / '------------ -------- - -----------------------------------' '----------- ----------- ----------'
                NEWLINE,"                      T                                    ","Interfacing Requesting  Date/Time",NEWLINE,"   PIN       Contract C Name Associated with Contract      ",
                "   System     System    Available?",NEWLINE,"------------ -------- - -----------------------------------","----------- ----------- ----------");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: VALUE 4
        else if (condition((pnd_P.equals(4))))
        {
            decideConditionsMet768++;
            ignore();
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  WRITE-HEADERS
    }
    private void sub_Write_No_Data() throws Exception                                                                                                                     //Natural: WRITE-NO-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
                                                                                                                                                                          //Natural: PERFORM DEFINE-PRINTER
        sub_Define_Printer();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(45),pnd_No_Data_Txt);                                                           //Natural: WRITE ( 01 ) /// 45T #NO-DATA-TXT
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(45),pnd_Eor);                                                                   //Natural: WRITE ( 01 ) /// 45T #EOR
        if (Global.isEscape()) return;
        //*  WRITE-NO-DATA
    }
    private void sub_Check_Correct_Report() throws Exception                                                                                                              //Natural: CHECK-CORRECT-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_I.reset();                                                                                                                                                    //Natural: RESET #I
        short decideConditionsMet800 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF INTRFCE.INTRFCE-STTS;//Natural: VALUE 'E'
        if (condition((intrfce_Intrfce_Stts.equals("E"))))
        {
            decideConditionsMet800++;
            pnd_I.setValue(1);                                                                                                                                            //Natural: MOVE 1 TO #I
        }                                                                                                                                                                 //Natural: VALUE 'R'
        else if (condition((intrfce_Intrfce_Stts.equals("R"))))
        {
            decideConditionsMet800++;
            pnd_I.setValue(2);                                                                                                                                            //Natural: MOVE 2 TO #I
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((intrfce_Intrfce_Stts.equals("S"))))
        {
            decideConditionsMet800++;
            pnd_I.setValue(3);                                                                                                                                            //Natural: MOVE 3 TO #I
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_I.equals(getZero())))                                                                                                                           //Natural: IF #I = 0
        {
            getReports().write(0, ReportOption.NOTITLE,"Terrible error on status (",intrfce_Intrfce_Stts,")","assignment");                                               //Natural: WRITE 'Terrible error on status (' INTRFCE.INTRFCE-STTS ')' 'assignment'
            if (Global.isEscape()) return;
            DbsUtil.terminate(33);  if (true) return;                                                                                                                     //Natural: TERMINATE 33
        }                                                                                                                                                                 //Natural: END-IF
        //*  PAST END OF DATA
        if (condition(pnd_P_Chk.greater(pnd_P_Max)))                                                                                                                      //Natural: IF #P-CHK GT #P-MAX
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        PND_PND_L3375:                                                                                                                                                    //Natural: REPEAT
        while (condition(whileTrue))
        {
            //*  THIS IS A GOOD THING
            if (condition(pnd_P_Chk.equals(pnd_I)))                                                                                                                       //Natural: IF #P-CHK = #I
            {
                pnd_P_Chk.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #P-CHK
                pnd_P.setValue(pnd_I);                                                                                                                                    //Natural: MOVE #I TO #P
                if (true) break PND_PND_L3375;                                                                                                                            //Natural: ESCAPE BOTTOM ( ##L3375. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_P.setValue(pnd_P_Chk);                                                                                                                                    //Natural: MOVE #P-CHK TO #P
                                                                                                                                                                          //Natural: PERFORM WRITE-NO-DATA
            sub_Write_No_Data();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L3375"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L3375"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(Map.getDoInput())) {return;}
            pnd_P_Chk.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #P-CHK
            //*  PAST END OF DATA
            if (condition(pnd_P_Chk.greater(pnd_P_Max)))                                                                                                                  //Natural: IF #P-CHK GT #P-MAX
            {
                //*  ESCAPE TO END-WORK
                Global.setEscape(true);                                                                                                                                   //Natural: ESCAPE BOTTOM ( ##L1345. )
                Global.setEscapeCode(EscapeType.Bottom, "pnd_pnd_L1345");
                if (true) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  CHECK-CORRECT-REPORT
    }
    private void sub_Summarise_Data() throws Exception                                                                                                                    //Natural: SUMMARISE-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(! (pnd_D_I.equals(getZero()))))                                                                                                                     //Natural: IF NOT #D-I = 0
        {
            DbsUtil.examine(new ExamineSource(pnd_Dates_Pnd_Date.getValue(1 + 1,":",pnd_D_I.getInt() + 1)), new ExamineSearch(intrfce_Rcrd_Crtd_For_Bsnss_Dte),           //Natural: EXAMINE #DATE ( 1:#D-I ) FOR INTRFCE.RCRD-CRTD-FOR-BSNSS-DTE INDEX #I
                new ExamineGivingIndex(pnd_I));
            if (condition(pnd_I.equals(getZero())))                                                                                                                       //Natural: IF #I = 0
            {
                pnd_D_I.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #D-I
                pnd_I.setValue(pnd_D_I);                                                                                                                                  //Natural: MOVE #D-I TO #I
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_D_I.setValue(1);                                                                                                                                          //Natural: MOVE 1 TO #D-I #I
            pnd_I.setValue(1);
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_I.greater(pnd_D_Max) || pnd_D_I.greater(pnd_D_Max)))                                                                                            //Natural: IF #I > #D-MAX OR #D-I > #D-MAX
        {
            pnd_D_I.setValue(31);                                                                                                                                         //Natural: MOVE 31 TO #D-I #I
            pnd_I.setValue(31);
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Dates_Pnd_Date.getValue(pnd_I.getInt() + 1).setValue(intrfce_Rcrd_Crtd_For_Bsnss_Dte);                                                                        //Natural: MOVE INTRFCE.RCRD-CRTD-FOR-BSNSS-DTE TO #DATE ( #I )
        PND_PND_L3590:                                                                                                                                                    //Natural: FOR #J = 1 TO #S-INX
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_S_Inx)); pnd_J.nadd(1))
        {
            //*  JW 1/12/00
            //*  JW 1/12/00
            if (condition(pnd_System_Pnd_System_Intfce.getValue(pnd_J).equals(intrfce_Intrfcng_Systm) && pnd_System_Pnd_System_Reqst.getValue(pnd_J).equals(intrfce_Rqstng_Systm))) //Natural: IF #SYSTEM-INTFCE ( #J ) = INTRFCE.INTRFCNG-SYSTM AND #SYSTEM-REQST ( #J ) = INTRFCE.RQSTNG-SYSTM
            {
                if (true) break PND_PND_L3590;                                                                                                                            //Natural: ESCAPE BOTTOM ( ##L3590. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  THIS OUGHT TO BE PREVENTED BY INTERFACE SYSTEM
        if (condition(pnd_J.greater(pnd_S_Inx)))                                                                                                                          //Natural: IF #J GT #S-INX
        {
            getReports().write(0, ReportOption.NOTITLE,"Terrible problem with Intrfcng-system",intrfce_Intrfcng_Systm,"not found on table");                              //Natural: WRITE 'Terrible problem with Intrfcng-system' INTRFCE.INTRFCNG-SYSTM 'not found on table'
            if (Global.isEscape()) return;
            DbsUtil.terminate(33);  if (true) return;                                                                                                                     //Natural: TERMINATE 33
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet874 = 0;                                                                                                                                 //Natural: DECIDE ON EVERY VALUE OF INTRFCE.INTRFCE-STTS;//Natural: VALUE 'S'
        if (condition(intrfce_Intrfce_Stts.equals("S")))
        {
            decideConditionsMet874++;
            pnd_Dates_Pnd_Numbers.getValue(pnd_I.getInt() + 1,pnd_J,2).nadd(1);                                                                                           //Natural: ADD 1 TO #NUMBERS ( #I,#J,2 )
            pnd_Dates_Pnd_Numbers.getValue(pnd_I.getInt() + 1,pnd_J,1).nadd(1);                                                                                           //Natural: ADD 1 TO #NUMBERS ( #I,#J,1 )
        }                                                                                                                                                                 //Natural: VALUE 'E','F'
        if (condition((intrfce_Intrfce_Stts.equals("E")) || (intrfce_Intrfce_Stts.equals("F"))))
        {
            decideConditionsMet874++;
            pnd_Dates_Pnd_Numbers.getValue(pnd_I.getInt() + 1,pnd_J,3).nadd(1);                                                                                           //Natural: ADD 1 TO #NUMBERS ( #I,#J,3 )
            pnd_Dates_Pnd_Numbers.getValue(pnd_I.getInt() + 1,pnd_J,1).nadd(1);                                                                                           //Natural: ADD 1 TO #NUMBERS ( #I,#J,1 )
        }                                                                                                                                                                 //Natural: VALUE 'F'
        if (condition(intrfce_Intrfce_Stts.equals("F")))
        {
            decideConditionsMet874++;
            pnd_Dates_Pnd_Numbers.getValue(pnd_I.getInt() + 1,pnd_J,5).nadd(1);                                                                                           //Natural: ADD 1 TO #NUMBERS ( #I,#J,5 )
            //*  CHEAT FOR AT BREAK
            intrfce_Intrfce_Stts.setValue("E");                                                                                                                           //Natural: MOVE 'E' TO INTRFCE.INTRFCE-STTS
        }                                                                                                                                                                 //Natural: VALUE 'R'
        if (condition(intrfce_Intrfce_Stts.equals("R")))
        {
            decideConditionsMet874++;
            pnd_Dates_Pnd_Numbers.getValue(pnd_I.getInt() + 1,pnd_J,4).nadd(1);                                                                                           //Natural: ADD 1 TO #NUMBERS ( #I,#J,4 )
            pnd_Dates_Pnd_Numbers.getValue(pnd_I.getInt() + 1,pnd_J,1).nadd(1);                                                                                           //Natural: ADD 1 TO #NUMBERS ( #I,#J,1 )
        }                                                                                                                                                                 //Natural: NONE
        if (condition(decideConditionsMet874 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  SUMMARISE-DATA
    }
    private void sub_Define_Printer() throws Exception                                                                                                                    //Natural: DEFINE-PRINTER
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().closePrinter(2);                                                                                                                                     //Natural: CLOSE PRINTER ( 01 )
        short decideConditionsMet898 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #P;//Natural: VALUE 1
        if (condition((pnd_P.equals(1))))
        {
            decideConditionsMet898++;
            getReports().definePrinter(1, "REPORT1");                                                                                                                     //Natural: DEFINE PRINTER ( REPORT1 = 1 ) OUTPUT #CMPRINT ( #P )
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((pnd_P.equals(2))))
        {
            decideConditionsMet898++;
            getReports().definePrinter(1, "REPORT2");                                                                                                                     //Natural: DEFINE PRINTER ( REPORT2 = 1 ) OUTPUT #CMPRINT ( #P )
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((pnd_P.equals(3))))
        {
            decideConditionsMet898++;
            getReports().definePrinter(1, "REPORT3");                                                                                                                     //Natural: DEFINE PRINTER ( REPORT3 = 1 ) OUTPUT #CMPRINT ( #P )
        }                                                                                                                                                                 //Natural: VALUE 4
        else if (condition((pnd_P.equals(4))))
        {
            decideConditionsMet898++;
            //*  DATA FILLED SUMMARY USES WRITE (04)
            if (condition(pnd_No_Data.getBoolean()))                                                                                                                      //Natural: IF #NO-DATA
            {
                getReports().definePrinter(1, "REPORT4");                                                                                                                 //Natural: DEFINE PRINTER ( REPORT4 = 1 ) OUTPUT #CMPRINT ( #P )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            getReports().write(0, ReportOption.NOTITLE,"terrible printer problem >>>>> #P =",pnd_P, new ReportEditMask ("999"));                                          //Natural: WRITE 'terrible printer problem >>>>> #P =' #P ( EM = 999 )
            if (Global.isEscape()) return;
            DbsUtil.terminate(33);  if (true) return;                                                                                                                     //Natural: TERMINATE 33
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Headers_Pnd_Header.setValue(pnd_Headers.getValue(pnd_P.getInt() + 1));                                                                                        //Natural: MOVE #HEADERS ( #P ) TO #HEADER
        pnd_Program.resetInitial();                                                                                                                                       //Natural: RESET INITIAL #PROGRAM
        pnd_Program.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Program, "/", pnd_P));                                                                   //Natural: COMPRESS #PROGRAM '/' #P INTO #PROGRAM LEAVING NO
        //*  DEFINE-PRINTER
    }
    private void sub_Update_Lr_Interface_Report() throws Exception                                                                                                        //Natural: UPDATE-LR-INTERFACE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Bt_Table_Id_Key.resetInitial();                                                                                                                               //Natural: RESET INITIAL #BT-TABLE-ID-KEY
        pnd_Bt_Table_Id_Key_Pnd_Bt_Table_Key.setValue(pnd_Report);                                                                                                        //Natural: MOVE #REPORT TO #BT-TABLE-KEY
        vw_table.startDatabaseFind                                                                                                                                        //Natural: FIND TABLE WITH BT-TABLE-ID-KEY = #BT-TABLE-ID-KEY
        (
        "PND_PND_L3900",
        new Wc[] { new Wc("BT_TABLE_ID_KEY", "=", pnd_Bt_Table_Id_Key, WcType.WITH) }
        );
        PND_PND_L3900:
        while (condition(vw_table.readNextRow("PND_PND_L3900")))
        {
            vw_table.setIfNotFoundControlFlag(false);
            table_Bt_Table_Text.setValue(pnd_Interface_Date);                                                                                                             //Natural: MOVE #INTERFACE-DATE TO TABLE.BT-TABLE-TEXT
            table_Bt_Rcrd_Last_Updt_Dte.setValue(Global.getDATN());                                                                                                       //Natural: MOVE *DATN TO TABLE.BT-RCRD-LAST-UPDT-DTE
            table_Bt_Rcrd_Last_Updt_Tme.setValue(Global.getTIMN(), MoveOption.RightJustified);                                                                            //Natural: MOVE RIGHT *TIMN TO TABLE.BT-RCRD-LAST-UPDT-TME
            table_Bt_Rcrd_Last_Updt_Userid.setValue(Global.getINIT_USER());                                                                                               //Natural: MOVE *INIT-USER TO TABLE.BT-RCRD-LAST-UPDT-USERID
            vw_table.updateDBRow("PND_PND_L3900");                                                                                                                        //Natural: UPDATE ( ##L3900. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        pnd_Xxx_Date.getValue(1,":",19).setValue(pnd_Run_Date.getValue(2,":",20));                                                                                        //Natural: MOVE #RUN-DATE ( 2:20 ) TO #XXX-DATE ( 1:19 )
        pnd_Run_Date.getValue("*").setValue(pnd_Xxx_Date.getValue("*"));                                                                                                  //Natural: MOVE #XXX-DATE ( * ) TO #RUN-DATE ( * )
        pnd_Xxx_Date.getValue("*").reset();                                                                                                                               //Natural: RESET #XXX-DATE ( * ) #RUN-DATE ( 20 )
        pnd_Run_Date.getValue(20).reset();
        //*  IE, > 1 DATES TO REPORT
        if (condition(! (pnd_Run_Date.getValue(1).equals(" "))))                                                                                                          //Natural: IF NOT #RUN-DATE ( 1 ) = ' '
        {
            gdaBeng900.getDialog_Info_Pnd_Pnd_Command().setValue(pnd_Run_Date.getValue(1));                                                                               //Natural: MOVE #RUN-DATE ( 1 ) TO ##COMMAND
            Global.getSTACK().pushData(StackOption.TOP,pnd_Run_Date.getValue(1), pnd_Run_Date.getValue(2), pnd_Run_Date.getValue(3), pnd_Run_Date.getValue(4),            //Natural: STACK TOP DATA #RUN-DATE ( * )
                pnd_Run_Date.getValue(5), pnd_Run_Date.getValue(6), pnd_Run_Date.getValue(7), pnd_Run_Date.getValue(8), pnd_Run_Date.getValue(9), pnd_Run_Date.getValue(10), 
                pnd_Run_Date.getValue(11), pnd_Run_Date.getValue(12), pnd_Run_Date.getValue(13), pnd_Run_Date.getValue(14), pnd_Run_Date.getValue(15), pnd_Run_Date.getValue(16), 
                pnd_Run_Date.getValue(17), pnd_Run_Date.getValue(18), pnd_Run_Date.getValue(19), pnd_Run_Date.getValue(20));
            Global.setFetchProgram(DbsUtil.getBlType(gdaBeng900.getPass_Pnd_Pnd_Prev_Program()));                                                                         //Natural: FETCH ##PREV-PROGRAM
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  UPDATE-LR-INTERFACE-REPORT
    }
    private void sub_Run_Dates() throws Exception                                                                                                                         //Natural: RUN-DATES
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Xxx_Date.getValue("*").setValue(pnd_Run_Date.getValue("*"));                                                                                                  //Natural: MOVE #RUN-DATE ( * ) TO #XXX-DATE ( * )
        pnd_Run_Date.getValue("*").reset();                                                                                                                               //Natural: RESET #RUN-DATE ( * ) #I
        pnd_I.reset();
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_I.equals(pnd_R))) {break;}                                                                                                                  //Natural: UNTIL #I = #R
            //*  GET SMALLEST DATE
            pnd_I.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #I
            FOR05:                                                                                                                                                        //Natural: FOR #J = 1 TO #R
            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_R)); pnd_J.nadd(1))
            {
                if (condition(pnd_Run_Date_X.equals(" ") && pnd_Xxx_Date.getValue(pnd_J).notEquals(" ")))                                                                 //Natural: IF #RUN-DATE-X = ' ' AND #XXX-DATE ( #J ) NE ' '
                {
                    pnd_Run_Date_X.setValue(pnd_Xxx_Date.getValue(pnd_J));                                                                                                //Natural: MOVE #XXX-DATE ( #J ) TO #RUN-DATE-X
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Xxx_Date.getValue(pnd_J).less(pnd_Run_Date_X) && pnd_Xxx_Date.getValue(pnd_J).notEquals(" ")))                                          //Natural: IF #XXX-DATE ( #J ) LT #RUN-DATE-X AND #XXX-DATE ( #J ) NE ' '
                {
                    pnd_Run_Date_X.setValue(pnd_Xxx_Date.getValue(pnd_J));                                                                                                //Natural: MOVE #XXX-DATE ( #J ) TO #RUN-DATE-X
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            DbsUtil.examine(new ExamineSource(pnd_Xxx_Date.getValue("*")), new ExamineSearch(pnd_Run_Date_X), new ExamineReplace(" "));                                   //Natural: EXAMINE #XXX-DATE ( * ) FOR #RUN-DATE-X REPLACE ' '
            pnd_Run_Date.getValue(pnd_I).setValue(pnd_Run_Date_X);                                                                                                        //Natural: MOVE #RUN-DATE-X TO #RUN-DATE ( #I )
            pnd_Run_Date_X.reset();                                                                                                                                       //Natural: RESET #RUN-DATE-X
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        pnd_R.reset();                                                                                                                                                    //Natural: RESET #R
        PND_PND_L4160:                                                                                                                                                    //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(pnd_Run_Date.getValue(pnd_I).equals(" ")))                                                                                                      //Natural: IF #RUN-DATE ( #I ) = ' '
            {
                if (true) break PND_PND_L4160;                                                                                                                            //Natural: ESCAPE BOTTOM ( ##L4160. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_R.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #R
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        gdaBeng900.getMsg_Info_Pnd_Pnd_Msg().setValueEdited(pnd_Lr_Interface_Report_Orig,new ReportEditMask("MM/DD/YYYY"));                                               //Natural: MOVE EDITED #LR-INTERFACE-REPORT-ORIG ( EM = MM/DD/YYYY ) TO ##MSG
        pnd_Datd.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Run_Date.getValue(pnd_R));                                                                             //Natural: MOVE EDITED #RUN-DATE ( #R ) TO #DATD ( EM = YYYYMMDD )
        gdaBeng900.getPass_Pnd_Pnd_Header2().setValueEdited(pnd_Datd,new ReportEditMask("MM/DD/YYYY"));                                                                   //Natural: MOVE EDITED #DATD ( EM = MM/DD/YYYY ) TO ##HEADER2
        gdaBeng900.getPass_Pnd_Pnd_Header2().setValue(DbsUtil.compress("Run executed for dates:", gdaBeng900.getMsg_Info_Pnd_Pnd_Msg(), "to", gdaBeng900.getPass_Pnd_Pnd_Header2())); //Natural: COMPRESS 'Run executed for dates:' ##MSG 'to' ##HEADER2 TO ##HEADER2
        gdaBeng900.getMsg_Info_Pnd_Pnd_Msg().reset();                                                                                                                     //Natural: RESET ##MSG
        getReports().write(0, ReportOption.NOTITLE,"RANGE:",pnd_R,pnd_Run_Date.getValue("*"),NEWLINE,gdaBeng900.getPass_Pnd_Pnd_Header2());                               //Natural: WRITE 'RANGE:' #R #RUN-DATE ( * ) / ##HEADER2
        if (Global.isEscape()) return;
        //*  RUN-DATES
    }
    private void sub_System_Table() throws Exception                                                                                                                      //Natural: SYSTEM-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pdaBena015.getBena015().reset();                                                                                                                                  //Natural: RESET BENA015
        pdaBena015.getBena015_Bt_Table_Id().setValue("IS");                                                                                                               //Natural: MOVE 'IS' TO BENA015.BT-TABLE-ID
        pnd_S_Tot.compute(new ComputeParameters(false, pnd_S_Tot), pnd_S_Max.subtract(1));                                                                                //Natural: COMPUTE #S-TOT = #S-MAX - 1
        vw_table.startDatabaseRead                                                                                                                                        //Natural: READ TABLE WITH BT-TABLE-ID-KEY = BENA015.BT-TABLE-ID
        (
        "PND_PND_L4285",
        new Wc[] { new Wc("BT_TABLE_ID_KEY", ">=", pdaBena015.getBena015_Bt_Table_Id(), WcType.BY) },
        new Oc[] { new Oc("BT_TABLE_ID_KEY", "ASC") }
        );
        PND_PND_L4285:
        while (condition(vw_table.readNextRow("PND_PND_L4285")))
        {
            if (condition(! (pdaBena015.getBena015_Bt_Table_Id().equals(table_Bt_Table_Id))))                                                                             //Natural: IF NOT BENA015.BT-TABLE-ID = TABLE.BT-TABLE-ID
            {
                if (true) break PND_PND_L4285;                                                                                                                            //Natural: ESCAPE BOTTOM ( ##L4285. )
            }                                                                                                                                                             //Natural: END-IF
            //*  JW 9/07/00 HERE
                                                                                                                                                                          //Natural: PERFORM CHECK-TAB-OFLOW
            sub_Check_Tab_Oflow();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L4285"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L4285"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(Map.getDoInput())) {return;}
            if (condition(pnd_S_Inx.greater(1) && table_Pnd_Bt_Table_Key_1st8.equals(pnd_System_Pnd_System_Intfce.getValue(pnd_S_Inx))))                                  //Natural: IF #S-INX GT 1 AND TABLE.#BT-TABLE-KEY-1ST8 = #SYSTEM-INTFCE ( #S-INX )
            {
                pnd_System_Needs_Subttl.setValue(true);                                                                                                                   //Natural: MOVE TRUE TO #SYSTEM-NEEDS-SUBTTL
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_System_Needs_Subttl.getBoolean() && table_Pnd_Bt_Table_Key_1st8.notEquals(pnd_System_Pnd_System_Intfce.getValue(pnd_S_Inx))))               //Natural: IF #SYSTEM-NEEDS-SUBTTL AND TABLE.#BT-TABLE-KEY-1ST8 <> #SYSTEM-INTFCE ( #S-INX )
            {
                pnd_S_Inx.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #S-INX
                                                                                                                                                                          //Natural: PERFORM CHECK-TAB-OFLOW
                sub_Check_Tab_Oflow();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PND_PND_L4285"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PND_PND_L4285"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
                pnd_System.getValue(pnd_S_Inx).setValue(pnd_Sub_Total);                                                                                                   //Natural: MOVE #SUB-TOTAL TO #SYSTEM ( #S-INX )
                pnd_System_Needs_Subttl.reset();                                                                                                                          //Natural: RESET #SYSTEM-NEEDS-SUBTTL
                //*  JW 9/07/00 TO HERE
            }                                                                                                                                                             //Natural: END-IF
            pnd_S_Inx.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #S-INX
            pnd_System_Pnd_System_Intfce.getValue(pnd_S_Inx).setValue(table_Pnd_Bt_Table_Key_1st8);                                                                       //Natural: MOVE TABLE.#BT-TABLE-KEY-1ST8 TO #SYSTEM-INTFCE ( #S-INX )
            //*  JW 1/12/00
            pnd_System_Pnd_System_Reqst.getValue(pnd_S_Inx).setValue(table_Bt_Table_Text);                                                                                //Natural: MOVE TABLE.BT-TABLE-TEXT TO #SYSTEM-REQST ( #S-INX )
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  IE, NO TABLE ENTRIES
        if (condition(pnd_S_Inx.equals(getZero())))                                                                                                                       //Natural: IF #S-INX = 0
        {
            getReports().write(0, ReportOption.NOTITLE,"Terrible problem in reading IS-table -","No Entries found on Table");                                             //Natural: WRITE 'Terrible problem in reading IS-table -' 'No Entries found on Table'
            if (Global.isEscape()) return;
            DbsUtil.terminate(33);  if (true) return;                                                                                                                     //Natural: TERMINATE 33
        }                                                                                                                                                                 //Natural: END-IF
        //*  JW 9/07/00
        if (condition(pnd_System_Needs_Subttl.getBoolean()))                                                                                                              //Natural: IF #SYSTEM-NEEDS-SUBTTL
        {
                                                                                                                                                                          //Natural: PERFORM CHECK-TAB-OFLOW
            sub_Check_Tab_Oflow();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_S_Inx.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #S-INX
                                                                                                                                                                          //Natural: PERFORM CHECK-TAB-OFLOW
            sub_Check_Tab_Oflow();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_System.getValue(pnd_S_Inx).setValue(pnd_Sub_Total);                                                                                                       //Natural: MOVE #SUB-TOTAL TO #SYSTEM ( #S-INX )
            pnd_System_Needs_Subttl.reset();                                                                                                                              //Natural: RESET #SYSTEM-NEEDS-SUBTTL
            //*  JW 9/07/00
        }                                                                                                                                                                 //Natural: END-IF
        pnd_S_Tot.compute(new ComputeParameters(false, pnd_S_Tot), pnd_S_Inx.add(1));                                                                                     //Natural: COMPUTE #S-TOT = #S-INX + 1
        pnd_System.getValue(pnd_S_Tot).setValue("  Total");                                                                                                               //Natural: MOVE '  Total' TO #SYSTEM ( #S-TOT )
        //*  JW 1/12/00
        pnd_R_Max.setValue(pnd_S_Tot);                                                                                                                                    //Natural: MOVE #S-TOT TO #R-MAX
        //*  SYSTEM-TABLE
    }
    //*  JW 9/07/00
    private void sub_Check_Tab_Oflow() throws Exception                                                                                                                   //Natural: CHECK-TAB-OFLOW
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_S_Inx.equals(pnd_S_Tot)))                                                                                                                       //Natural: IF #S-INX = #S-TOT
        {
            getReports().write(0, ReportOption.NOTITLE,"IS-table read -","Max entries",pnd_S_Tot,"(totals included) exceeded");                                           //Natural: WRITE 'IS-table read -' 'Max entries' #S-TOT '(totals included) exceeded'
            if (Global.isEscape()) return;
            DbsUtil.terminate(33);  if (true) return;                                                                                                                     //Natural: TERMINATE 33
        }                                                                                                                                                                 //Natural: END-IF
        //*  CHECK-TAB-OFLOW
    }
    private void sub_Start_Up() throws Exception                                                                                                                          //Natural: START-UP
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        DbsUtil.callnat(Benn9831.class , getCurrentProcessState(), pnd_Interface_Date, gdaBeng900.getMsg_Info(), gdaBeng900.getError_Info());                             //Natural: CALLNAT 'BENN9831' #INTERFACE-DATE MSG-INFO ERROR-INFO
        if (condition(Global.isEscape())) return;
        if (condition(! (gdaBeng900.getMsg_Info_Pnd_Pnd_Msg().equals(" "))))                                                                                              //Natural: IF NOT ##MSG = ' '
        {
            getReports().write(0, ReportOption.NOTITLE,"*",new RepeatItem(79),gdaBeng900.getMsg_Info_Pnd_Pnd_Msg(),"*",new RepeatItem(79));                               //Natural: WRITE '*' ( 79 ) ##MSG '*' ( 79 )
            if (Global.isEscape()) return;
            DbsUtil.terminate(33);  if (true) return;                                                                                                                     //Natural: TERMINATE 33
        }                                                                                                                                                                 //Natural: END-IF
        pdaBena015.getBena015_Bt_Table_Id().setValue(pnd_Bt_Table_Id_Key_Pnd_Zz);                                                                                         //Natural: MOVE #ZZ TO BENA015.BT-TABLE-ID
        pdaBena015.getBena015_Bt_Table_Key().setValue(pnd_Report);                                                                                                        //Natural: MOVE #REPORT TO BENA015.BT-TABLE-KEY
        DbsUtil.callnat(Benn015.class , getCurrentProcessState(), pdaBena015.getBena015());                                                                               //Natural: CALLNAT 'BENN015' BENA015
        if (condition(Global.isEscape())) return;
        if (condition(DbsUtil.maskMatches(pdaBena015.getBena015_Bt_Table_Text(),"'~!@'")))                                                                                //Natural: IF BENA015.BT-TABLE-TEXT = MASK ( '~!@' )
        {
            getReports().write(0, ReportOption.NOTITLE,"*",new RepeatItem(79),"=",pdaBena015.getBena015_Bt_Table_Id(),pdaBena015.getBena015_Bt_Table_Key(),pdaBena015.getBena015_Bt_Table_Text(),pdaBena015.getBena015_Bt_Rcrd_Last_Updt_Dte(),pdaBena015.getBena015_Bt_Rcrd_Last_Updt_Tme(),pdaBena015.getBena015_Bt_Rcrd_Last_Updt_Userid(),"*",new  //Natural: WRITE '*' ( 79 ) '=' BENA015 '*' ( 79 )
                RepeatItem(79));
            if (Global.isEscape()) return;
            DbsUtil.terminate(33);  if (true) return;                                                                                                                     //Natural: TERMINATE 33
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Lr_Interface_Report.setValue(pdaBena015.getBena015_Bt_Table_Text());                                                                                          //Natural: MOVE BENA015.BT-TABLE-TEXT TO #LR-INTERFACE-REPORT
        pnd_Datd.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Lr_Interface_Report);                                                                                  //Natural: MOVE EDITED #LR-INTERFACE-REPORT TO #DATD ( EM = YYYYMMDD )
        //*  START READ FROM DAY AFTER LR-INTRFCE-REPRT
        pnd_Datd.nadd(1);                                                                                                                                                 //Natural: ADD 1 TO #DATD
        pnd_Lr_Interface_Report.setValueEdited(pnd_Datd,new ReportEditMask("YYYYMMDD"));                                                                                  //Natural: MOVE EDITED #DATD ( EM = YYYYMMDD ) TO #LR-INTERFACE-REPORT
        pnd_Lr_Interface_Report_Orig.setValue(pnd_Datd);                                                                                                                  //Natural: MOVE #DATD TO #LR-INTERFACE-REPORT-ORIG
        //*  -
        //* ** -
        //* **** -
        //* ** -
        //*  -
                                                                                                                                                                          //Natural: PERFORM SYSTEM-TABLE
        sub_System_Table();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        pnd_I.reset();                                                                                                                                                    //Natural: RESET #I
        pnd_Intrfce_Super_1_Last_Prcssd_Bsnss_Dte.setValue(pnd_Lr_Interface_Report);                                                                                      //Natural: MOVE #LR-INTERFACE-REPORT TO #INTRFCE-SUPER-1.LAST-PRCSSD-BSNSS-DTE
        FOR06:                                                                                                                                                            //Natural: FOR #K = 1 TO #P-MAX
        for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pnd_P_Max)); pnd_K.nadd(1))
        {
            pnd_Intrfce_Super_1_Intrfce_Stts.setValue(pnd_Status.getValue(pnd_K));                                                                                        //Natural: MOVE #STATUS ( #K ) TO #INTRFCE-SUPER-1.INTRFCE-STTS
            vw_intrfce.startDatabaseRead                                                                                                                                  //Natural: READ INTRFCE WITH INTRFCE-SUPER-1 = #INTRFCE-SUPER-1
            (
            "PND_PND_L4755",
            new Wc[] { new Wc("INTRFCE_SUPER_1", ">=", pnd_Intrfce_Super_1, WcType.BY) },
            new Oc[] { new Oc("INTRFCE_SUPER_1", "ASC") }
            );
            PND_PND_L4755:
            while (condition(vw_intrfce.readNextRow("PND_PND_L4755")))
            {
                if (condition(intrfce_Intrfce_Stts.notEquals(pnd_Intrfce_Super_1_Intrfce_Stts) || intrfce_Last_Prcssd_Bsnss_Dte.greater(pnd_Interface_Date)))             //Natural: IF INTRFCE.INTRFCE-STTS NE #INTRFCE-SUPER-1.INTRFCE-STTS OR INTRFCE.LAST-PRCSSD-BSNSS-DTE GT #INTERFACE-DATE
                {
                    if (true) break PND_PND_L4755;                                                                                                                        //Natural: ESCAPE BOTTOM ( ##L4755. )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(! (intrfce_Intrfcng_Systm.equals(pnd_System_Pnd_System_Intfce.getValue(1,":",pnd_S_Inx)))))                                                 //Natural: IF NOT INTRFCE.INTRFCNG-SYSTM = #SYSTEM-INTFCE ( 1:#S-INX )
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Run_Date.getValue(1).equals(" ")))                                                                                                      //Natural: IF #RUN-DATE ( 01 ) = ' '
                {
                    pnd_R.setValue(1);                                                                                                                                    //Natural: MOVE 1 TO #R
                    pnd_Run_Date.getValue(pnd_R).setValue(intrfce_Last_Prcssd_Bsnss_Dte);                                                                                 //Natural: MOVE INTRFCE.LAST-PRCSSD-BSNSS-DTE TO #RUN-DATE ( #R )
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                DbsUtil.examine(new ExamineSource(pnd_Run_Date.getValue(1,":",pnd_R)), new ExamineSearch(intrfce_Last_Prcssd_Bsnss_Dte), new ExamineGivingIndex(pnd_J));  //Natural: EXAMINE #RUN-DATE ( 1:#R ) FOR INTRFCE.LAST-PRCSSD-BSNSS-DTE INDEX #J
                if (condition(pnd_J.equals(getZero())))                                                                                                                   //Natural: IF #J = 0
                {
                    pnd_R.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #R
                    pnd_Run_Date.getValue(pnd_R).setValue(intrfce_Last_Prcssd_Bsnss_Dte);                                                                                 //Natural: MOVE INTRFCE.LAST-PRCSSD-BSNSS-DTE TO #RUN-DATE ( #R )
                }                                                                                                                                                         //Natural: END-IF
                //*  (4755)
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_R.notEquals(getZero())))                                                                                                                        //Natural: IF #R <> 0
        {
            DbsUtil.examine(new ExamineSource(pnd_Run_Date.getValue(1,":",pnd_R)), new ExamineSearch(pnd_Interface_Date), new ExamineGivingNumber(pnd_J));                //Natural: EXAMINE #RUN-DATE ( 1:#R ) FOR #INTERFACE-DATE NUMBER #J
        }                                                                                                                                                                 //Natural: END-IF
        //*  IE, NO DATA ON #INTERFACE-DATE
        //*  OR     /* DESPITE RANGE, NO DATA
        if (condition(pnd_J.equals(getZero()) || pnd_R.equals(getZero())))                                                                                                //Natural: IF #J = 0 OR #R = 0
        {
            pnd_R.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #R
            pnd_Run_Date.getValue(pnd_R).setValue(pnd_Interface_Date);                                                                                                    //Natural: MOVE #INTERFACE-DATE TO #RUN-DATE ( #R )
        }                                                                                                                                                                 //Natural: END-IF
        //*  THERE ARE > 1 PRCSSD-BSNSS-DTE's
        if (condition(pnd_R.greater(1)))                                                                                                                                  //Natural: IF #R GT 1
        {
                                                                                                                                                                          //Natural: PERFORM RUN-DATES
            sub_Run_Dates();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        gdaBeng900.getPass_Pnd_Pnd_Prev_Program().setValue(Global.getPROGRAM());                                                                                          //Natural: MOVE *PROGRAM TO ##PREV-PROGRAM
        gdaBeng900.getDialog_Info_Pnd_Pnd_Command().setValue(pnd_Run_Date.getValue(1));                                                                                   //Natural: MOVE #RUN-DATE ( 1 ) TO ##COMMAND
        Global.getSTACK().pushData(StackOption.TOP,pnd_Run_Date.getValue(1), pnd_Run_Date.getValue(2), pnd_Run_Date.getValue(3), pnd_Run_Date.getValue(4),                //Natural: STACK TOP DATA #RUN-DATE ( * )
            pnd_Run_Date.getValue(5), pnd_Run_Date.getValue(6), pnd_Run_Date.getValue(7), pnd_Run_Date.getValue(8), pnd_Run_Date.getValue(9), pnd_Run_Date.getValue(10), 
            pnd_Run_Date.getValue(11), pnd_Run_Date.getValue(12), pnd_Run_Date.getValue(13), pnd_Run_Date.getValue(14), pnd_Run_Date.getValue(15), pnd_Run_Date.getValue(16), 
            pnd_Run_Date.getValue(17), pnd_Run_Date.getValue(18), pnd_Run_Date.getValue(19), pnd_Run_Date.getValue(20));
        Global.setFetchProgram(DbsUtil.getBlType(gdaBeng900.getPass_Pnd_Pnd_Prev_Program()));                                                                             //Natural: FETCH ##PREV-PROGRAM
        if (condition(Global.isEscape())) return;
        //*  START-UP
    }
    //*  02/05/04 CF
    private void sub_Print_Totals() throws Exception                                                                                                                      //Natural: PRINT-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //*  =================
        if (condition(! (pnd_No_Data.getBoolean())))                                                                                                                      //Natural: IF NOT #NO-DATA
        {
            //*  EXCEPTIONS
            short decideConditionsMet1158 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF #P;//Natural: VALUE 1
            if (condition((pnd_P.equals(1))))
            {
                decideConditionsMet1158++;
                //*  RETRY
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(30),"*** Total of Error Records:",pnd_Total_Error, new ReportEditMask           //Natural: WRITE ( 01 ) // 30T '*** Total of Error Records:' #TOTAL-ERROR ( EM = ZZZ,ZZZ,Z99 )
                    ("ZZZ,ZZZ,Z99"));
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_P.equals(2))))
            {
                decideConditionsMet1158++;
                //*  EXCEPTIONS
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(30),"*** Total of Retry Records:",pnd_Total_Retry, new ReportEditMask           //Natural: WRITE ( 01 ) // 30T '*** Total of Retry Records:' #TOTAL-RETRY ( EM = ZZZ,ZZZ,Z99 )
                    ("ZZZ,ZZZ,Z99"));
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((pnd_P.equals(3))))
            {
                decideConditionsMet1158++;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(30),"*** Total of Successful Records:",pnd_Total_Success, new                   //Natural: WRITE ( 01 ) // 30T '*** Total of Successful Records:' #TOTAL-SUCCESS ( EM = ZZZ,ZZZ,Z99 )
                    ReportEditMask ("ZZZ,ZZZ,Z99"));
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        //*  =================
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Benm9900.class));                                              //Natural: WRITE ( 01 ) NOTITLE NOHDR USING FORM 'BENM9900'
                    if (condition(! (pnd_On_A_Break.getBoolean())))                                                                                                       //Natural: IF NOT #ON-A-BREAK
                    {
                        getReports().print(1, ReportOption.NOHDR,NEWLINE,NEWLINE,NEWLINE,pnd_Start_Date,pnd_Datd,NEWLINE,NEWLINE);                                        //Natural: PRINT ( 01 ) NOHDR /// #START-DATE #DATD //
                                                                                                                                                                          //Natural: PERFORM WRITE-HEADERS
                        sub_Write_Headers();
                        if (condition(Global.isEscape())) {return;}
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt4 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().getPageNumberDbs(1).reset();                                                                                                             //Natural: RESET *PAGE-NUMBER ( 01 )
                    getReports().write(4, ReportOption.NOTITLE, ReportOption.NOHDR, writeMapToStringOutput(Benm9900.class));                                              //Natural: WRITE ( 04 ) NOTITLE NOHDR USING FORM 'BENM9900'
                    getReports().skip(4, 3);                                                                                                                              //Natural: SKIP ( 04 ) 3
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt0 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(0, ReportOption.NOTITLE,ReportOption.NOHDR," ");                                                                                   //Natural: WRITE ( 00 ) NOTITLE NOHDR ' '
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventPnd_Pnd_L1345() throws Exception {atBreakEventPnd_Pnd_L1345(false);}
    private void atBreakEventPnd_Pnd_L1345(boolean endOfData) throws Exception
    {
        boolean pnd_BreakIsBreak = pnd_Break.isBreak(endOfData);
        if (condition(pnd_BreakIsBreak))
        {
            pnd_On_A_Break.setValue(true);                                                                                                                                //Natural: MOVE TRUE TO #ON-A-BREAK
            if (condition(pnd_Break.equals(pnd_Break_X)))                                                                                                                 //Natural: IF #BREAK = #BREAK-X
            {
                if (!endOfData)                                                                                                                                           //Natural: ESCAPE BOTTOM ( ##L1345. )
                {
                    Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom, "pnd_pnd_L1345");
                }
                if (condition(true)) return;
            }                                                                                                                                                             //Natural: END-IF
            //*  REPORT-BREAK
            if (condition(! (pnd_Break_X_Pnd_Intrfce_Stts.equals(intrfce_Intrfce_Stts))))                                                                                 //Natural: IF NOT #INTRFCE-STTS = INTRFCE.INTRFCE-STTS
            {
                                                                                                                                                                          //Natural: PERFORM REPORT-BREAK-STUFF
                sub_Report_Break_Stuff();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM DATE-BREAK-STUFF
                sub_Date_Break_Stuff();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            pnd_Break_X.setValue(pnd_Break);                                                                                                                              //Natural: MOVE #BREAK TO #BREAK-X
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=56 SG=OFF ZP=OFF");
        Global.format(1, "LS=133 PS=56 SG=OFF ZP=OFF");
        Global.format(4, "LS=134 PS=56 SG=OFF ZP=OFF");
        Global.format(5, "LS=133 PS=56 SG=OFF ZP=OFF");

        getReports().setDisplayColumns(4, new ReportZeroPrint (true),new SignPosition (false),"Interfacing/System",
        		pnd_System_Pnd_System_Intfce,"Requesting/System",
        		pnd_System_Pnd_System_Reqst,new TabSetting(40),"Total Designations/Read from Interface File",
        		pnd_Numbers_P, new ReportEditMask ("          ZZZZZZ9"),new ColumnSpacing(10),"Total Designations/Written to Bene File",
        		pnd_Numbers_P, new ReportEditMask ("        ZZZZZZ9"),new ColumnSpacing(10),"Total/Errors",
        		pnd_Numbers_P,new ColumnSpacing(10),"To Be/Retried",
        		pnd_Numbers_P);
        getReports().setDisplayColumns(1, new ReportEmptyLineSuppression(true),ReportOption.NOHDR,intrfce_Pin_Tiaa_Cntrct,intrfce_Tiaa_Cref_Ind, new ReportEditMask 
            ("X"),pdaBena008.getPnd_Bena008_Pnd_Nas_Name(), new AlphanumericLength (24),intrfce_Intrfcng_Systm,intrfce_Rqstng_Systm,intrfce_Error_Txt, new 
            ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"));
    }
    private void CheckAtStartofData412() throws Exception
    {
        if (condition(getSort().getAtStartOfData()))
        {
            pnd_On_A_Break.setValue(true);                                                                                                                                //Natural: MOVE TRUE TO #ON-A-BREAK
            //*  IE NO DATE
            pnd_Break_X.setValue(intrfce_Intrfce_Stts);                                                                                                                   //Natural: MOVE INTRFCE.INTRFCE-STTS TO #BREAK-X
                                                                                                                                                                          //Natural: PERFORM REPORT-BREAK-STUFF
            sub_Report_Break_Stuff();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-START
    }
}
