/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:20:25 PM
**        * FROM NATURAL PROGRAM : Benp9860
************************************************************
**        * FILE NAME            : Benp9860.java
**        * CLASS NAME           : Benp9860
**        * INSTANCE NAME        : Benp9860
************************************************************
************************************************************************
* PROGRAM  : BENP9860
* SYSTEM   : BENEFICIARY-SYSTEM
* TITLE    : INTERFACE DRIVER /* INTERFACE SAME AS
* GENERATED: OCTOBER,
*          :
*
*
* HISTORY ==> CLONED FROM BENP9800 - FONTOUR
* CHANGED ON JUNE 29,99 BY DEPAUL
* >
* CHANGED ON NOV 05,99 BY WEBBJ
* > ADD CALL TO FCPA115 TO GET NEXT BUSINESS DATE FOR BENN9820 EDIT.
* CHANGED ON DEC 03,99 BY WEBBJ
* > ADD CALL TO BENN9834 TO LOAD TABLE FILE TO TABLES.
* CHANGED ON AUG 30,00 BY WEBBJ
* > UPDATE PIN FROM COR IF ITS ZERO - NEGATIVE ENROLLMENT
* CHANGED ON OCT 04 BY WEBBJ
* > ONLINE STATUS TO 'M' IF CORR REJECT
* CHANGED ON NOV 28 BY WEBBJ
* > FLDR-LOG-DTE-TME TRANSFERED TO BENA9801 VIA BENN9815
* CHANGED ON DEC 18 BY WEBBJ
* 'INTERFACE-AS-MOS' FLAG TRANSFER TO BENN9802
* CHANGED ON MAY 15 BY WEBBJ
* EXTENSIVE CHANGES DUE TO WBBC INTERFACING MDOS.
* 01/21/04 - FIX READ AND ACCEPT MORE THAN ONE ERROR AND LOOK IF THERE's
*            MORE THAN ONE ERROR AND LOOK IF THERE's same as error. (CF)
************************************************************************
*    DATE      USERID                   DESCRIPTION
* MM/DD/YYYY  XXXXXXXX  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
* 04/01/2017  DURAND    PIN EXPANSION N7 TO N12
************************************************************************

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benp9860 extends BLNatBase
{
    // Data Areas
    private PdaBenpda_M pdaBenpda_M;
    private PdaBenpda_E pdaBenpda_E;
    private PdaBena9860 pdaBena9860;
    private PdaBena9861 pdaBena9861;
    private PdaBena9871 pdaBena9871;
    private PdaBena9867 pdaBena9867;
    private PdaBena9872 pdaBena9872;
    private PdaBena9873 pdaBena9873;
    private PdaBena9875 pdaBena9875;
    private PdaBena9877 pdaBena9877;
    private PdaBena9878 pdaBena9878;
    private PdaBena9879 pdaBena9879;
    private PdaBena9869 pdaBena9869;
    private PdaBena9870 pdaBena9870;
    private PdaBena9866 pdaBena9866;
    private PdaBena9865 pdaBena9865;
    private PdaBena9868 pdaBena9868;
    private PdaBena9864 pdaBena9864;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_bci;
    private DbsField bci_Intrfce_Stts;
    private DbsField bci_Rcrd_Crtd_For_Bsnss_Dte;
    private DbsField bci_Rcrd_Crtd_Dte;
    private DbsField bci_Rcrd_Crtd_Tme;
    private DbsField bci_Intrfcng_Systm;
    private DbsField bci_Rqstng_Systm;
    private DbsField bci_Rcrd_Last_Updt_Dte;
    private DbsField bci_Rcrd_Last_Updt_Tme;
    private DbsField bci_Rcrd_Last_Updt_Userid;
    private DbsField bci_Last_Prcssd_Bsnss_Dte;
    private DbsField bci_Intrfce_Cmpltd_Dte;
    private DbsField bci_Intrfce_Cmpltd_Tme;
    private DbsField bci_Dflt_To_Estate_Ind;
    private DbsField bci_Illgble_Ind;
    private DbsField bci_More_Than_Five_Benes_Ind;
    private DbsField bci_Intrfce_Mgrtn_Ind;
    private DbsField bci_More_Than_Thirty_Benes_Ind;
    private DbsField bci_Pin_Tiaa_Cntrct;
    private DbsField bci_Count_Casterror_Table;

    private DbsGroup bci_Error_Table;
    private DbsField bci_Error_Txt;
    private DbsField bci_Error_Cde;
    private DbsField bci_Error_Dte;
    private DbsField bci_Error_Tme;
    private DbsField bci_Error_Pgm;
    private DbsField bci_Fldr_Log_Dte_Tme;
    private DbsField bci_Last_Dsgntn_Srce;
    private DbsField bci_Last_Dsgntn_System;
    private DbsField bci_Last_Dsgntn_Userid;
    private DbsField bci_Last_Dsgntn_Dte;
    private DbsField bci_Last_Dsgntn_Tme;
    private DbsField bci_Tiaa_Cref_Chng_Dte;
    private DbsField bci_Tiaa_Cref_Chng_Tme;
    private DbsField bci_Chng_Pwr_Atty;
    private DbsField bci_Cref_Cntrct;
    private DbsField bci_Tiaa_Cref_Ind;
    private DbsField bci_Cntrct_Type;
    private DbsField bci_Eff_Dte;
    private DbsField bci_Mos_Ind;
    private DbsField bci_Irrvcble_Ind;
    private DbsField bci_Pymnt_Chld_Dcsd_Ind;
    private DbsField bci_Exempt_Spouse_Rights;
    private DbsField bci_Spouse_Waived_Bnfts;
    private DbsField bci_Chng_New_Issue_Ind;
    private DbsField bci_Same_As_Ind;
    private DbsField pnd_I1;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Intrfce_Super_3;

    private DbsGroup pnd_Intrfce_Super_3__R_Field_1;
    private DbsField pnd_Intrfce_Super_3_Pnd_Intrfce_Stts;
    private DbsField pnd_Intrfce_Super_3_Pnd_Filler_Super_3;
    private DbsField pnd_Second_Cntrct_Retry;
    private DbsField pnd_Second_Pin_Retry;
    private DbsField pnd_Next_Record;
    private DbsField pnd_Record_Rejected;
    private DbsField pnd_Sameas;
    private DbsField pnd_Et_Cnt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaBenpda_M = new PdaBenpda_M(localVariables);
        pdaBenpda_E = new PdaBenpda_E(localVariables);
        pdaBena9860 = new PdaBena9860(localVariables);
        pdaBena9861 = new PdaBena9861(localVariables);
        pdaBena9871 = new PdaBena9871(localVariables);
        pdaBena9867 = new PdaBena9867(localVariables);
        pdaBena9872 = new PdaBena9872(localVariables);
        pdaBena9873 = new PdaBena9873(localVariables);
        pdaBena9875 = new PdaBena9875(localVariables);
        pdaBena9877 = new PdaBena9877(localVariables);
        pdaBena9878 = new PdaBena9878(localVariables);
        pdaBena9879 = new PdaBena9879(localVariables);
        pdaBena9869 = new PdaBena9869(localVariables);
        pdaBena9870 = new PdaBena9870(localVariables);
        pdaBena9866 = new PdaBena9866(localVariables);
        pdaBena9865 = new PdaBena9865(localVariables);
        pdaBena9868 = new PdaBena9868(localVariables);
        pdaBena9864 = new PdaBena9864(localVariables);

        // Local Variables

        vw_bci = new DataAccessProgramView(new NameInfo("vw_bci", "BCI"), "BENE_CONTRACT_INTERFACE_12", "BENE_CONT_INTFCE", DdmPeriodicGroups.getInstance().getGroups("BENE_CONTRACT_INTERFACE_12"));
        bci_Intrfce_Stts = vw_bci.getRecord().newFieldInGroup("bci_Intrfce_Stts", "INTRFCE-STTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, "INTRFCE_STTS");
        bci_Intrfce_Stts.setDdmHeader("INTRFCE/STATUS");
        bci_Rcrd_Crtd_For_Bsnss_Dte = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Crtd_For_Bsnss_Dte", "RCRD-CRTD-FOR-BSNSS-DTE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "RCRD_CRTD_FOR_BSNSS_DTE");
        bci_Rcrd_Crtd_For_Bsnss_Dte.setDdmHeader("RCRD CRTD/FOR/BSNSS DTE");
        bci_Rcrd_Crtd_Dte = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Crtd_Dte", "RCRD-CRTD-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_CRTD_DTE");
        bci_Rcrd_Crtd_Dte.setDdmHeader("RECORD/CREATED/DATE");
        bci_Rcrd_Crtd_Tme = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Crtd_Tme", "RCRD-CRTD-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "RCRD_CRTD_TME");
        bci_Rcrd_Crtd_Tme.setDdmHeader("RECORD/CREATED/TIME");
        bci_Intrfcng_Systm = vw_bci.getRecord().newFieldInGroup("bci_Intrfcng_Systm", "INTRFCNG-SYSTM", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "INTRFCNG_SYSTM");
        bci_Intrfcng_Systm.setDdmHeader("INTRFCNG/SYSTEM");
        bci_Rqstng_Systm = vw_bci.getRecord().newFieldInGroup("bci_Rqstng_Systm", "RQSTNG-SYSTM", FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQSTNG_SYSTM");
        bci_Rqstng_Systm.setDdmHeader("RQSTNG/SYSTEM");
        bci_Rcrd_Last_Updt_Dte = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Last_Updt_Dte", "RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_DTE");
        bci_Rcrd_Last_Updt_Dte.setDdmHeader("RECORD/LAST UPDT/DATE");
        bci_Rcrd_Last_Updt_Tme = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Last_Updt_Tme", "RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_TME");
        bci_Rcrd_Last_Updt_Tme.setDdmHeader("RECORD/LAST UPDT/TIME");
        bci_Rcrd_Last_Updt_Userid = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Last_Updt_Userid", "RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_USERID");
        bci_Rcrd_Last_Updt_Userid.setDdmHeader("RECORD/LAST UPDT/USER ID");
        bci_Last_Prcssd_Bsnss_Dte = vw_bci.getRecord().newFieldInGroup("bci_Last_Prcssd_Bsnss_Dte", "LAST-PRCSSD-BSNSS-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "LAST_PRCSSD_BSNSS_DTE");
        bci_Last_Prcssd_Bsnss_Dte.setDdmHeader("LAST PRCSSD/BUSINESS/DATE");
        bci_Intrfce_Cmpltd_Dte = vw_bci.getRecord().newFieldInGroup("bci_Intrfce_Cmpltd_Dte", "INTRFCE-CMPLTD-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "INTRFCE_CMPLTD_DTE");
        bci_Intrfce_Cmpltd_Dte.setDdmHeader("INTFCE/CMPLTD/DATE");
        bci_Intrfce_Cmpltd_Tme = vw_bci.getRecord().newFieldInGroup("bci_Intrfce_Cmpltd_Tme", "INTRFCE-CMPLTD-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "INTRFCE_CMPLTD_TME");
        bci_Intrfce_Cmpltd_Tme.setDdmHeader("INTRFCE/CMPLTD/TME");
        bci_Dflt_To_Estate_Ind = vw_bci.getRecord().newFieldInGroup("bci_Dflt_To_Estate_Ind", "DFLT-TO-ESTATE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "DFLT_TO_ESTATE_IND");
        bci_Dflt_To_Estate_Ind.setDdmHeader("DEFAULT/TO/ESTATE");
        bci_Illgble_Ind = vw_bci.getRecord().newFieldInGroup("bci_Illgble_Ind", "ILLGBLE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "ILLGBLE_IND");
        bci_Illgble_Ind.setDdmHeader("ILLGBLE/IND");
        bci_More_Than_Five_Benes_Ind = vw_bci.getRecord().newFieldInGroup("bci_More_Than_Five_Benes_Ind", "MORE-THAN-FIVE-BENES-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "MORE_THAN_FIVE_BENES_IND");
        bci_More_Than_Five_Benes_Ind.setDdmHeader("MORE/THAN 5/BENES");
        bci_Intrfce_Mgrtn_Ind = vw_bci.getRecord().newFieldInGroup("bci_Intrfce_Mgrtn_Ind", "INTRFCE-MGRTN-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "INTRFCE_MGRTN_IND");
        bci_Intrfce_Mgrtn_Ind.setDdmHeader("INTRFCE/MGRTN/IND");
        bci_More_Than_Thirty_Benes_Ind = vw_bci.getRecord().newFieldInGroup("bci_More_Than_Thirty_Benes_Ind", "MORE-THAN-THIRTY-BENES-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "MORE_THAN_THIRTY_BENES_IND");
        bci_More_Than_Thirty_Benes_Ind.setDdmHeader("MORE/THAN 30/BENES");
        bci_Pin_Tiaa_Cntrct = vw_bci.getRecord().newFieldInGroup("bci_Pin_Tiaa_Cntrct", "PIN-TIAA-CNTRCT", FieldType.STRING, 22, RepeatingFieldStrategy.None, 
            "PIN_TIAA_CNTRCT");
        bci_Pin_Tiaa_Cntrct.setDdmHeader("PIN/TIAA/CONTRACT");
        bci_Count_Casterror_Table = vw_bci.getRecord().newFieldInGroup("bci_Count_Casterror_Table", "C*ERROR-TABLE", RepeatingFieldStrategy.CAsteriskVariable, 
            "BENE_CONT_INTFCE_ERROR_TABLE");

        bci_Error_Table = vw_bci.getRecord().newGroupInGroup("bci_Error_Table", "ERROR-TABLE", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Error_Txt = bci_Error_Table.newFieldArrayInGroup("bci_Error_Txt", "ERROR-TXT", FieldType.STRING, 72, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ERROR_TXT", "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Error_Txt.setDdmHeader("ERROR/TEXT");
        bci_Error_Cde = bci_Error_Table.newFieldArrayInGroup("bci_Error_Cde", "ERROR-CDE", FieldType.STRING, 5, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ERROR_CDE", "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Error_Cde.setDdmHeader("ERROR/CODE");
        bci_Error_Dte = bci_Error_Table.newFieldArrayInGroup("bci_Error_Dte", "ERROR-DTE", FieldType.STRING, 8, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ERROR_DTE", "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Error_Dte.setDdmHeader("ERROR/DATE");
        bci_Error_Tme = bci_Error_Table.newFieldArrayInGroup("bci_Error_Tme", "ERROR-TME", FieldType.STRING, 7, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ERROR_TME", "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Error_Tme.setDdmHeader("ERROR/TIME");
        bci_Error_Pgm = bci_Error_Table.newFieldArrayInGroup("bci_Error_Pgm", "ERROR-PGM", FieldType.STRING, 8, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ERROR_PGM", "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Fldr_Log_Dte_Tme = vw_bci.getRecord().newFieldInGroup("bci_Fldr_Log_Dte_Tme", "FLDR-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "FLDR_LOG_DTE_TME");
        bci_Fldr_Log_Dte_Tme.setDdmHeader("FOLDER/LOG DATE/TIME");
        bci_Last_Dsgntn_Srce = vw_bci.getRecord().newFieldInGroup("bci_Last_Dsgntn_Srce", "LAST-DSGNTN-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "LAST_DSGNTN_SRCE");
        bci_Last_Dsgntn_Srce.setDdmHeader("LAST/DSGNTN/SOURCE");
        bci_Last_Dsgntn_System = vw_bci.getRecord().newFieldInGroup("bci_Last_Dsgntn_System", "LAST-DSGNTN-SYSTEM", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "LAST_DSGNTN_SYSTEM");
        bci_Last_Dsgntn_System.setDdmHeader("LAST/DSGNTN/SYSTEM");
        bci_Last_Dsgntn_Userid = vw_bci.getRecord().newFieldInGroup("bci_Last_Dsgntn_Userid", "LAST-DSGNTN-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "LAST_DSGNTN_USERID");
        bci_Last_Dsgntn_Userid.setDdmHeader("LAST/DSGNTN/USERID");
        bci_Last_Dsgntn_Dte = vw_bci.getRecord().newFieldInGroup("bci_Last_Dsgntn_Dte", "LAST-DSGNTN-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "LAST_DSGNTN_DTE");
        bci_Last_Dsgntn_Dte.setDdmHeader("LAST/DSGNTN/DATE");
        bci_Last_Dsgntn_Tme = vw_bci.getRecord().newFieldInGroup("bci_Last_Dsgntn_Tme", "LAST-DSGNTN-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "LAST_DSGNTN_TME");
        bci_Last_Dsgntn_Tme.setDdmHeader("LAST/DSGNTN/TIME");
        bci_Tiaa_Cref_Chng_Dte = vw_bci.getRecord().newFieldInGroup("bci_Tiaa_Cref_Chng_Dte", "TIAA-CREF-CHNG-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIAA_CREF_CHNG_DTE");
        bci_Tiaa_Cref_Chng_Dte.setDdmHeader("TIAA CREF/CHANGE/DATE");
        bci_Tiaa_Cref_Chng_Tme = vw_bci.getRecord().newFieldInGroup("bci_Tiaa_Cref_Chng_Tme", "TIAA-CREF-CHNG-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "TIAA_CREF_CHNG_TME");
        bci_Tiaa_Cref_Chng_Tme.setDdmHeader("TIAA CREF/CHANGE/TIME");
        bci_Chng_Pwr_Atty = vw_bci.getRecord().newFieldInGroup("bci_Chng_Pwr_Atty", "CHNG-PWR-ATTY", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CHNG_PWR_ATTY");
        bci_Chng_Pwr_Atty.setDdmHeader("CHANGE/POWER/ATTNY");
        bci_Cref_Cntrct = vw_bci.getRecord().newFieldInGroup("bci_Cref_Cntrct", "CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT");
        bci_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bci_Tiaa_Cref_Ind = vw_bci.getRecord().newFieldInGroup("bci_Tiaa_Cref_Ind", "TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIAA_CREF_IND");
        bci_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bci_Cntrct_Type = vw_bci.getRecord().newFieldInGroup("bci_Cntrct_Type", "CNTRCT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_TYPE");
        bci_Cntrct_Type.setDdmHeader("CNTRCT/TYPE");
        bci_Eff_Dte = vw_bci.getRecord().newFieldInGroup("bci_Eff_Dte", "EFF-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "EFF_DTE");
        bci_Eff_Dte.setDdmHeader("EFFECTIVE/DATE");
        bci_Mos_Ind = vw_bci.getRecord().newFieldInGroup("bci_Mos_Ind", "MOS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "MOS_IND");
        bci_Mos_Ind.setDdmHeader("MOS/IND");
        bci_Irrvcble_Ind = vw_bci.getRecord().newFieldInGroup("bci_Irrvcble_Ind", "IRRVCBLE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "IRRVCBLE_IND");
        bci_Irrvcble_Ind.setDdmHeader("IRVCBL/IND");
        bci_Pymnt_Chld_Dcsd_Ind = vw_bci.getRecord().newFieldInGroup("bci_Pymnt_Chld_Dcsd_Ind", "PYMNT-CHLD-DCSD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PYMNT_CHLD_DCSD_IND");
        bci_Pymnt_Chld_Dcsd_Ind.setDdmHeader("PYMNT/CHILD/DCSD");
        bci_Exempt_Spouse_Rights = vw_bci.getRecord().newFieldInGroup("bci_Exempt_Spouse_Rights", "EXEMPT-SPOUSE-RIGHTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "EXEMPT_SPOUSE_RIGHTS");
        bci_Exempt_Spouse_Rights.setDdmHeader("EXEMPT/SPOUSAL/RIGHTS");
        bci_Spouse_Waived_Bnfts = vw_bci.getRecord().newFieldInGroup("bci_Spouse_Waived_Bnfts", "SPOUSE-WAIVED-BNFTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "SPOUSE_WAIVED_BNFTS");
        bci_Spouse_Waived_Bnfts.setDdmHeader("SPOUSE/WAIVED/BENEFITS");
        bci_Chng_New_Issue_Ind = vw_bci.getRecord().newFieldInGroup("bci_Chng_New_Issue_Ind", "CHNG-NEW-ISSUE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CHNG_NEW_ISSUE_IND");
        bci_Chng_New_Issue_Ind.setDdmHeader("CHANGE/NEW ISSUE/IND");
        bci_Same_As_Ind = vw_bci.getRecord().newFieldInGroup("bci_Same_As_Ind", "SAME-AS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "SAME_AS_IND");
        registerRecord(vw_bci);

        pnd_I1 = localVariables.newFieldInRecord("pnd_I1", "#I1", FieldType.PACKED_DECIMAL, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_Intrfce_Super_3 = localVariables.newFieldInRecord("pnd_Intrfce_Super_3", "#INTRFCE-SUPER-3", FieldType.STRING, 32);

        pnd_Intrfce_Super_3__R_Field_1 = localVariables.newGroupInRecord("pnd_Intrfce_Super_3__R_Field_1", "REDEFINE", pnd_Intrfce_Super_3);
        pnd_Intrfce_Super_3_Pnd_Intrfce_Stts = pnd_Intrfce_Super_3__R_Field_1.newFieldInGroup("pnd_Intrfce_Super_3_Pnd_Intrfce_Stts", "#INTRFCE-STTS", 
            FieldType.STRING, 1);
        pnd_Intrfce_Super_3_Pnd_Filler_Super_3 = pnd_Intrfce_Super_3__R_Field_1.newFieldInGroup("pnd_Intrfce_Super_3_Pnd_Filler_Super_3", "#FILLER-SUPER-3", 
            FieldType.STRING, 31);
        pnd_Second_Cntrct_Retry = localVariables.newFieldInRecord("pnd_Second_Cntrct_Retry", "#SECOND-CNTRCT-RETRY", FieldType.BOOLEAN, 1);
        pnd_Second_Pin_Retry = localVariables.newFieldInRecord("pnd_Second_Pin_Retry", "#SECOND-PIN-RETRY", FieldType.BOOLEAN, 1);
        pnd_Next_Record = localVariables.newFieldInRecord("pnd_Next_Record", "#NEXT-RECORD", FieldType.BOOLEAN, 1);
        pnd_Record_Rejected = localVariables.newFieldInRecord("pnd_Record_Rejected", "#RECORD-REJECTED", FieldType.BOOLEAN, 1);
        pnd_Sameas = localVariables.newFieldInRecord("pnd_Sameas", "#SAMEAS", FieldType.BOOLEAN, 1);
        pnd_Et_Cnt = localVariables.newFieldInRecord("pnd_Et_Cnt", "#ET-CNT", FieldType.PACKED_DECIMAL, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_bci.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Benp9860() throws Exception
    {
        super("Benp9860");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("BENP9860", onError);
        setupReports();
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: FORMAT LS = 133 PS = 60;//Natural: ASSIGN ERROR-INFO-SUB.##LAST-PROGRAM = *PROGRAM
        //*  READ JUST "E" ERRORED RECORDS.
        pnd_Intrfce_Super_3_Pnd_Intrfce_Stts.setValue("E");                                                                                                               //Natural: ASSIGN #INTRFCE-SUPER-3.#INTRFCE-STTS = 'E'
        MAIN_ROUTINE:                                                                                                                                                     //Natural: REPEAT
        while (condition(whileTrue))
        {
            //*   /* JW
            //*  DATE IS THE SAME AS INTERFACE REPORT
                                                                                                                                                                          //Natural: PERFORM GET-BUSINESS-DATE
            sub_Get_Business_Date();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("MAIN_ROUTINE"))) break;
                else if (condition(Global.isEscapeBottomImmediate("MAIN_ROUTINE"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "PROCESSING DATE",pdaBena9873.getBena9873_Pnd_Bsnss_Dte());                                                                             //Natural: WRITE 'PROCESSING DATE' BENA9873.#BSNSS-DTE
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("MAIN_ROUTINE"))) break;
                else if (condition(Global.isEscapeBottomImmediate("MAIN_ROUTINE"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ENTRIES FROM TABLE FILE NEEDED FOR REL CODE &
                                                                                                                                                                          //Natural: PERFORM LOAD-TABLE-FILE
            sub_Load_Table_File();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("MAIN_ROUTINE"))) break;
                else if (condition(Global.isEscapeBottomImmediate("MAIN_ROUTINE"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  /* ETC. LOAD HERE ONCE AND PASS AROUND VIA BENA9820. JW 12/3/99
            //*  READ THROUGHOUT THE FILE TO PICK SAME AS RECORDS. WE ARE NOT UPDATING
            //*  LAST-BUSINESS-DATE.
            vw_bci.startDatabaseRead                                                                                                                                      //Natural: READ BCI BY INTRFCE-SUPER-3 FROM #INTRFCE-SUPER-3
            (
            "READ_CONTRACT",
            new Wc[] { new Wc("INTRFCE_SUPER_3", ">=", pnd_Intrfce_Super_3, WcType.BY) },
            new Oc[] { new Oc("INTRFCE_SUPER_3", "ASC") }
            );
            READ_CONTRACT:
            while (condition(vw_bci.readNextRow("READ_CONTRACT")))
            {
                if (condition(bci_Intrfce_Stts.notEquals("E")))                                                                                                           //Natural: IF BCI.INTRFCE-STTS NE 'E'
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                    //*  CF 01/21/04
                }                                                                                                                                                         //Natural: END-IF
                //*   WE HAVE TO GET JUST THE SAME ERRORED RECORDS.
                pnd_Record_Rejected.setValue(false);                                                                                                                      //Natural: ASSIGN #RECORD-REJECTED := FALSE
                pnd_Sameas.setValue(false);                                                                                                                               //Natural: ASSIGN #SAMEAS := FALSE
                pnd_I.setValue(bci_Count_Casterror_Table);                                                                                                                //Natural: ASSIGN #I := C*BCI.ERROR-TABLE
                if (condition(bci_Error_Cde.getValue(pnd_I).equals("NSAME")))                                                                                             //Natural: IF BCI.ERROR-CDE ( #I ) EQ 'NSAME'
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                FOR01:                                                                                                                                                    //Natural: FOR #I 1 C*BCI.ERROR-TABLE
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(bci_Count_Casterror_Table)); pnd_I.nadd(1))
                {
                    //*  CF 01/21/04
                    //*  ERROR AFTER 1ST
                    if (condition(bci_Error_Cde.getValue(pnd_I).equals("SAME")))                                                                                          //Natural: IF BCI.ERROR-CDE ( #I ) = 'SAME'
                    {
                        pnd_Sameas.setValue(true);                                                                                                                        //Natural: ASSIGN #SAMEAS := TRUE
                        //*     IF #I <= C*BCI.ERROR-TABLE
                        //*        RESET #RECORD-REJECTED
                        //*        ESCAPE BOTTOM
                        //*     END-IF
                        FOR02:                                                                                                                                            //Natural: FOR #J #I C*BCI.ERROR-TABLE
                        for (pnd_J.setValue(pnd_I); condition(pnd_J.lessOrEqual(bci_Count_Casterror_Table)); pnd_J.nadd(1))
                        {
                            //*  ERROR AFTER 1ST
                            if (condition(pnd_I.lessOrEqual(bci_Count_Casterror_Table)))                                                                                  //Natural: IF #I <= C*BCI.ERROR-TABLE
                            {
                                if (condition(bci_Error_Cde.getValue(pnd_J).notEquals("SAME")))                                                                           //Natural: IF BCI.ERROR-CDE ( #J ) NE 'SAME'
                                {
                                    pnd_Record_Rejected.reset();                                                                                                          //Natural: RESET #RECORD-REJECTED
                                    if (condition(true)) break;                                                                                                           //Natural: ESCAPE BOTTOM
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-FOR
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*   ELSE                                                   /* CF 01/21/04
                        //*     #RECORD-REJECTED := TRUE
                        //*     ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_CONTRACT"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_CONTRACT"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  CF 01/21/04
                //*  CF 01/21/04
                if (condition(pnd_Record_Rejected.getBoolean() || ! (pnd_Sameas.getBoolean())))                                                                           //Natural: IF #RECORD-REJECTED OR NOT #SAMEAS
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(0, "PROCESSING",bci_Pin_Tiaa_Cntrct);                                                                                                  //Natural: WRITE 'PROCESSING' BCI.PIN-TIAA-CNTRCT
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_CONTRACT"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_CONTRACT"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*   MAKE SURE THIS RECORD IS ON COR.  IF NOT, STAYS AS SAME AS
                pdaBena9860.getBena9860().setValuesByName(vw_bci);                                                                                                        //Natural: MOVE BY NAME BCI TO BENA9860
                                                                                                                                                                          //Natural: PERFORM VERIFY-LATEST-RECORD
                sub_Verify_Latest_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_CONTRACT"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_CONTRACT"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  FAILED VERIFY-LATEST-RECORD...GET NEXT RECORD.
                if (condition(pnd_Next_Record.getBoolean()))                                                                                                              //Natural: IF #NEXT-RECORD
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*  CHECK IF PARTICIPANT HAS DATE OF DEATH
                                                                                                                                                                          //Natural: PERFORM CHECK-DOD
                sub_Check_Dod();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_CONTRACT"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_CONTRACT"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  FAILED CHECK-DOD...GET NEXT RECORD.
                if (condition(pnd_Next_Record.getBoolean()))                                                                                                              //Natural: IF #NEXT-RECORD
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CHECK-COR
                sub_Check_Cor();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_CONTRACT"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_CONTRACT"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  FAILED CHECK-COR...GET NEXT RECORD.
                if (condition(pnd_Next_Record.getBoolean()))                                                                                                              //Natural: IF #NEXT-RECORD
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*   PERFORM EDITS
                                                                                                                                                                          //Natural: PERFORM EDITS
                sub_Edits();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_CONTRACT"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_CONTRACT"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  FAILED EDITS...GET NEXT RECORD.
                if (condition(pnd_Next_Record.getBoolean()))                                                                                                              //Natural: IF #NEXT-RECORD
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*   THIS DESIGNATION IS READY TO BE MOVED INTO THE BENE SYSTEM.
                //*   IF THIS IS A CHANGE, WE MUST DELETE THE EXISTING BENE RECORDS AND
                //*     PUT THEM ON THE HISTORY FILE.
                if (condition(pdaBena9860.getBena9860_Chng_New_Issue_Ind().equals("C")))                                                                                  //Natural: IF BENA9860.CHNG-NEW-ISSUE-IND = 'C'
                {
                                                                                                                                                                          //Natural: PERFORM DELETE-EXISTING-BENE-RECORDS
                    sub_Delete_Existing_Bene_Records();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_CONTRACT"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_CONTRACT"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-TO-BENE-RECORDS
                sub_Write_To_Bene_Records();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_CONTRACT"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_CONTRACT"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  FAILED EDITS...GET NEXT RECORD.
                if (condition(pnd_Next_Record.getBoolean()))                                                                                                              //Natural: IF #NEXT-RECORD
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*   THIS INTERFACE WAS A SUCCESS
                getReports().write(0, "SUCCESS",pdaBena9860.getBena9860_Pin_Tiaa_Cntrct(),pdaBena9860.getBena9860_Rcrd_Crtd_For_Bsnss_Dte());                             //Natural: WRITE 'SUCCESS' BENA9860.PIN-TIAA-CNTRCT BENA9860.RCRD-CRTD-FOR-BSNSS-DTE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_CONTRACT"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_CONTRACT"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pdaBena9871.getBena9871_Pnd_New_Intrfce_Stts().setValue("S");                                                                                             //Natural: ASSIGN BENA9871.#NEW-INTRFCE-STTS = 'S'
                                                                                                                                                                          //Natural: PERFORM UPDATE-STATUS
                sub_Update_Status();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_CONTRACT"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_CONTRACT"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Et_Cnt.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #ET-CNT
                if (condition(pnd_Et_Cnt.greater(50)))                                                                                                                    //Natural: IF #ET-CNT GT 50
                {
                    pnd_Et_Cnt.reset();                                                                                                                                   //Natural: RESET #ET-CNT
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("MAIN_ROUTINE"))) break;
                else if (condition(Global.isEscapeBottomImmediate("MAIN_ROUTINE"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* *END-REPEAT
            //*  MAIN-ROUTINE.
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
            //* *******************************
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DONE-FOR-TODAY
            //*  MAIN-ROUTINE.
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //* *********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DELETE-EXISTING-BENE-RECORDS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-BUSINESS-DATE
        //* ***************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-TABLE-FILE
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TO-ERROR-TABLE
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TO-BENE-RECORDS
        //* **************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-DOD
        //* **************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-COR
        //* *************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: VERIFY-LATEST-RECORD
        //* *************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-PIN-FROM-COR
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-STATUS
        //* **********************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EDITS
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: HANDLE-ERROR
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TERMINATE-PROCESS
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Done_For_Today() throws Exception                                                                                                                    //Natural: DONE-FOR-TODAY
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        //*  MAIN-ROUTINE.
        Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom);                                                                                                  //Natural: ESCAPE BOTTOM
        if (true) return;
        //*  DONE-FOR-TODAY
    }
    private void sub_Delete_Existing_Bene_Records() throws Exception                                                                                                      //Natural: DELETE-EXISTING-BENE-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************
        //*  JW 5/18
        pdaBena9868.getBena9868().reset();                                                                                                                                //Natural: RESET BENA9868
        pdaBena9868.getBena9868_Pnd_Pin_Tiaa_Cntrct().setValue(pdaBena9860.getBena9860_Pin_Tiaa_Cntrct());                                                                //Natural: ASSIGN BENA9868.#PIN-TIAA-CNTRCT = BENA9860.PIN-TIAA-CNTRCT
        pdaBena9868.getBena9868_Pnd_Tiaa_Cref_Ind().setValue(pdaBena9860.getBena9860_Tiaa_Cref_Ind());                                                                    //Natural: ASSIGN BENA9868.#TIAA-CREF-IND = BENA9860.TIAA-CREF-IND
        DbsUtil.callnat(Benn9868.class , getCurrentProcessState(), pdaBena9868.getBena9868(), pdaBenpda_M.getMsg_Info_Sub(), pdaBenpda_E.getError_Info_Sub());            //Natural: CALLNAT 'BENN9868' BENA9868 MSG-INFO-SUB ERROR-INFO-SUB
        if (condition(Global.isEscape())) return;
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ##LAST-PROGRAM = *PROGRAM
        //*  JW 11/28/00
        if (condition(pdaBena9860.getBena9860_Rqstng_Systm().equals("CORR")))                                                                                             //Natural: IF BENA9860.RQSTNG-SYSTM = 'CORR'
        {
            pdaBena9860.getBena9860_Fldr_Log_Dte_Tme().setValue(pdaBena9868.getBena9868_Pnd_Fldr_Log_Dte_Tme());                                                          //Natural: ASSIGN BENA9860.FLDR-LOG-DTE-TME := BENA9868.#FLDR-LOG-DTE-TME
        }                                                                                                                                                                 //Natural: END-IF
        //*  DELETE-EXISTING-BENE-RECORDS
    }
    private void sub_Get_Business_Date() throws Exception                                                                                                                 //Natural: GET-BUSINESS-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        DbsUtil.callnat(Benn9873.class , getCurrentProcessState(), pdaBena9873.getBena9873(), pdaBenpda_M.getMsg_Info_Sub(), pdaBenpda_E.getError_Info_Sub());            //Natural: CALLNAT 'BENN9873' BENA9873 MSG-INFO-SUB ERROR-INFO-SUB
        if (condition(Global.isEscape())) return;
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ##LAST-PROGRAM = *PROGRAM
        //*  GET-BUSINESS-DATE
    }
    private void sub_Load_Table_File() throws Exception                                                                                                                   //Natural: LOAD-TABLE-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************
        pdaBena9864.getBena9864().reset();                                                                                                                                //Natural: RESET BENA9864
        DbsUtil.callnat(Benn9864.class , getCurrentProcessState(), pdaBena9864.getBena9864(), pdaBenpda_M.getMsg_Info_Sub(), pdaBenpda_E.getError_Info_Sub());            //Natural: CALLNAT 'BENN9864' BENA9864 MSG-INFO-SUB ERROR-INFO-SUB
        if (condition(Global.isEscape())) return;
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ##LAST-PROGRAM = *PROGRAM
        //*  LOAD-TABLE-FILE
    }
    private void sub_Write_To_Error_Table() throws Exception                                                                                                              //Natural: WRITE-TO-ERROR-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        pdaBena9872.getBena9872_Pnd_Cntrct_Isn().setValue(pdaBena9860.getBena9860_Pnd_Cntrct_Isn());                                                                      //Natural: ASSIGN BENA9872.#CNTRCT-ISN = BENA9860.#CNTRCT-ISN
        pdaBena9872.getBena9872_Pnd_Pin_Tiaa_Cntrct().setValue(pdaBena9860.getBena9860_Pin_Tiaa_Cntrct());                                                                //Natural: ASSIGN BENA9872.#PIN-TIAA-CNTRCT = BENA9860.PIN-TIAA-CNTRCT
        pdaBena9872.getBena9872_Pnd_Tiaa_Cref_Ind().setValue(pdaBena9860.getBena9860_Tiaa_Cref_Ind());                                                                    //Natural: ASSIGN BENA9872.#TIAA-CREF-IND = BENA9860.TIAA-CREF-IND
        pdaBena9872.getBena9872_Pnd_Intrfce_Stts().setValue(pdaBena9860.getBena9860_Intrfce_Stts());                                                                      //Natural: ASSIGN BENA9872.#INTRFCE-STTS = BENA9860.INTRFCE-STTS
        pdaBena9872.getBena9872_Pnd_Rcrd_Crtd_For_Bsnss_Dte().setValue(pdaBena9860.getBena9860_Rcrd_Crtd_For_Bsnss_Dte());                                                //Natural: ASSIGN BENA9872.#RCRD-CRTD-FOR-BSNSS-DTE = BENA9860.RCRD-CRTD-FOR-BSNSS-DTE
        pdaBena9872.getBena9872_Pnd_Bsnss_Dte().setValue(pdaBena9873.getBena9873_Pnd_Bsnss_Dte());                                                                        //Natural: ASSIGN BENA9872.#BSNSS-DTE = BENA9873.#BSNSS-DTE
        DbsUtil.callnat(Benn9872.class , getCurrentProcessState(), pdaBena9872.getBena9872(), pdaBenpda_M.getMsg_Info_Sub(), pdaBenpda_E.getError_Info_Sub());            //Natural: CALLNAT 'BENN9872' BENA9872 MSG-INFO-SUB ERROR-INFO-SUB
        if (condition(Global.isEscape())) return;
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ##LAST-PROGRAM = *PROGRAM
        //*  WRITE-TO-ERROR-TABLE
    }
    private void sub_Write_To_Bene_Records() throws Exception                                                                                                             //Natural: WRITE-TO-BENE-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        getReports().write(0, "WRITING TO BENE RECORDS");                                                                                                                 //Natural: WRITE 'WRITING TO BENE RECORDS'
        if (Global.isEscape()) return;
        DbsUtil.callnat(Benn9870.class , getCurrentProcessState(), pdaBena9878.getBena9878(), pdaBena9860.getBena9860(), pdaBena9873.getBena9873(), pdaBenpda_M.getMsg_Info_Sub(),  //Natural: CALLNAT 'BENN9870' BENA9878 BENA9860 BENA9873 MSG-INFO-SUB ERROR-INFO-SUB
            pdaBenpda_E.getError_Info_Sub());
        if (condition(Global.isEscape())) return;
        if (condition(pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().notEquals(" ")))                                                                                          //Natural: IF ##MSG NE ' '
        {
            pdaBena9872.getBena9872_Pnd_Error_Txt().setValue(pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                  //Natural: ASSIGN BENA9872.#ERROR-TXT := ##MSG
            pdaBena9872.getBena9872_Pnd_Error_Pgm().setValue(pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program());                                                       //Natural: ASSIGN BENA9872.#ERROR-PGM := ##LAST-PROGRAM
            pdaBena9872.getBena9872_Pnd_Error_Cde().setValue("NSAME");                                                                                                    //Natural: ASSIGN BENA9872.#ERROR-CDE := 'NSAME'
                                                                                                                                                                          //Natural: PERFORM HANDLE-ERROR
            sub_Handle_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ##LAST-PROGRAM = *PROGRAM
        //*  WRITE-TO-BENE-RECORDS
    }
    private void sub_Check_Dod() throws Exception                                                                                                                         //Natural: CHECK-DOD
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************
        pnd_Next_Record.reset();                                                                                                                                          //Natural: RESET #NEXT-RECORD BENA9861 ##RETURN-CODE
        pdaBena9861.getBena9861().reset();
        pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().reset();
        pdaBena9861.getBena9861_Pnd_Pin().setValue(pdaBena9860.getBena9860_Pin());                                                                                        //Natural: ASSIGN BENA9861.#PIN := BENA9860.PIN
        DbsUtil.callnat(Benn9861.class , getCurrentProcessState(), pdaBena9861.getBena9861(), pdaBenpda_M.getMsg_Info_Sub(), pdaBenpda_E.getError_Info_Sub());            //Natural: CALLNAT 'BENN9861' BENA9861 MSG-INFO-SUB ERROR-INFO-SUB
        if (condition(Global.isEscape())) return;
        if (condition(pdaBena9861.getBena9861_Pnd_Dte_Of_Dth_Found().getBoolean()))                                                                                       //Natural: IF BENA9861.#DTE-OF-DTH-FOUND
        {
            pdaBena9872.getBena9872_Pnd_Error_Txt().setValue(DbsUtil.compress("Date of Death found for PIN:", pdaBena9860.getBena9860_Pin()));                            //Natural: COMPRESS 'Date of Death found for PIN:' BENA9860.PIN INTO BENA9872.#ERROR-TXT
            pdaBena9872.getBena9872_Pnd_Error_Cde().setValue("NSAME");                                                                                                    //Natural: ASSIGN BENA9872.#ERROR-CDE := 'NSAME'
            pdaBena9872.getBena9872_Pnd_Error_Pgm().setValue(pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program());                                                       //Natural: ASSIGN BENA9872.#ERROR-PGM := ERROR-INFO-SUB.##LAST-PROGRAM
                                                                                                                                                                          //Natural: PERFORM HANDLE-ERROR
            sub_Handle_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  CHECK-DOD
    }
    private void sub_Check_Cor() throws Exception                                                                                                                         //Natural: CHECK-COR
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************
        //*  BENA9865
        pnd_Next_Record.reset();                                                                                                                                          //Natural: RESET #NEXT-RECORD #CNTRCT-FOUND BENA9865.#CNTRCT-ISSUE-DTE BENA9865.#PIN-FOUND
        pdaBena9865.getBena9865_Pnd_Cntrct_Found().reset();
        pdaBena9865.getBena9865_Pnd_Cntrct_Issue_Dte().reset();
        pdaBena9865.getBena9865_Pnd_Pin_Found().reset();
        pdaBena9865.getBena9865_Pnd_Cntrct().setValue(pdaBena9860.getBena9860_Tiaa_Cntrct());                                                                             //Natural: ASSIGN BENA9865.#CNTRCT = BENA9860.TIAA-CNTRCT
        DbsUtil.callnat(Benn9865.class , getCurrentProcessState(), pdaBena9865.getBena9865(), pdaBenpda_M.getMsg_Info_Sub(), pdaBenpda_E.getError_Info_Sub());            //Natural: CALLNAT 'BENN9865' BENA9865 MSG-INFO-SUB ERROR-INFO-SUB
        if (condition(Global.isEscape())) return;
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ##LAST-PROGRAM = *PROGRAM
        //*  ACIS NEG. ENROLL. MAY SEND 0 PIN. JW 8/30
        if (condition(pdaBena9860.getBena9860_Pin().equals(getZero()) && pdaBena9865.getBena9865_Pnd_Pin_Found().getBoolean()))                                           //Natural: IF BENA9860.PIN = 0 AND BENA9865.#PIN-FOUND
        {
                                                                                                                                                                          //Natural: PERFORM UPDATE-PIN-FROM-COR
            sub_Update_Pin_From_Cor();
            if (condition(Global.isEscape())) {return;}
            //*  JW 8/30
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet734 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN NOT BENA9865.#CNTRCT-FOUND
        if (condition(! ((pdaBena9865.getBena9865_Pnd_Cntrct_Found().getBoolean()))))
        {
            decideConditionsMet734++;
            pdaBena9872.getBena9872_Pnd_Error_Txt().setValue(DbsUtil.compress("Contract: ", pdaBena9860.getBena9860_Tiaa_Cntrct(), "not on COR"));                        //Natural: COMPRESS 'Contract: ' BENA9860.TIAA-CNTRCT 'not on COR' INTO BENA9872.#ERROR-TXT
            pdaBena9872.getBena9872_Pnd_Error_Pgm().setValue(pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program());                                                       //Natural: ASSIGN BENA9872.#ERROR-PGM = ERROR-INFO-SUB.##LAST-PROGRAM
            pdaBena9872.getBena9872_Pnd_Error_Cde().setValue("SAME");                                                                                                     //Natural: ASSIGN BENA9872.#ERROR-CDE = 'SAME'
        }                                                                                                                                                                 //Natural: WHEN NOT BENA9861.#PIN-FOUND
        if (condition(! ((pdaBena9861.getBena9861_Pnd_Pin_Found().getBoolean()))))
        {
            decideConditionsMet734++;
            pdaBena9872.getBena9872_Pnd_Error_Txt().setValue(DbsUtil.compress("PIN:  ", pdaBena9860.getBena9860_Pin(), "not on COR"));                                    //Natural: COMPRESS 'PIN:  ' BENA9860.PIN 'not on COR' INTO BENA9872.#ERROR-TXT
            pdaBena9872.getBena9872_Pnd_Error_Pgm().setValue(pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program());                                                       //Natural: ASSIGN BENA9872.#ERROR-PGM = ERROR-INFO-SUB.##LAST-PROGRAM
            pdaBena9872.getBena9872_Pnd_Error_Cde().setValue("NSAME");                                                                                                    //Natural: ASSIGN BENA9872.#ERROR-CDE = 'NSAME'
        }                                                                                                                                                                 //Natural: WHEN NOT BENA9865.#CNTRCT-IS-ACTIVE AND BENA9865.#CNTRCT-FOUND
        if (condition(! ((pdaBena9865.getBena9865_Pnd_Cntrct_Is_Active().getBoolean()) && pdaBena9865.getBena9865_Pnd_Cntrct_Found().getBoolean())))
        {
            decideConditionsMet734++;
            pdaBena9872.getBena9872_Pnd_Error_Txt().setValue(DbsUtil.compress("Contract", pdaBena9860.getBena9860_Tiaa_Cntrct(), "is terminated on COR"));                //Natural: COMPRESS 'Contract' BENA9860.TIAA-CNTRCT 'is terminated on COR' INTO BENA9872.#ERROR-TXT
            pdaBena9872.getBena9872_Pnd_Error_Pgm().setValue(pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program());                                                       //Natural: ASSIGN BENA9872.#ERROR-PGM = ERROR-INFO-SUB.##LAST-PROGRAM
            pdaBena9872.getBena9872_Pnd_Error_Cde().setValue("NSAME");                                                                                                    //Natural: ASSIGN BENA9872.#ERROR-CDE = 'NSAME'
        }                                                                                                                                                                 //Natural: WHEN BENA9865.#PIN NE BENA9860.PIN AND BENA9865.#PIN-FOUND
        if (condition(pdaBena9865.getBena9865_Pnd_Pin().notEquals(pdaBena9860.getBena9860_Pin()) && pdaBena9865.getBena9865_Pnd_Pin_Found().getBoolean()))
        {
            decideConditionsMet734++;
            pdaBena9872.getBena9872_Pnd_Error_Txt().setValue(DbsUtil.compress("PIN on Intrfce", pdaBena9860.getBena9860_Pin(), "is dif. from COR", pdaBena9865.getBena9865_Pnd_Pin())); //Natural: COMPRESS 'PIN on Intrfce' BENA9860.PIN 'is dif. from COR' BENA9865.#PIN INTO BENA9872.#ERROR-TXT
            pdaBena9872.getBena9872_Pnd_Error_Pgm().setValue(pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program());                                                       //Natural: ASSIGN BENA9872.#ERROR-PGM = ERROR-INFO-SUB.##LAST-PROGRAM
            pdaBena9872.getBena9872_Pnd_Error_Cde().setValue("NSAME");                                                                                                    //Natural: ASSIGN BENA9872.#ERROR-CDE = 'NSAME'
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet734 > 0))
        {
                                                                                                                                                                          //Natural: PERFORM HANDLE-ERROR
            sub_Handle_Error();
            if (condition(Global.isEscape())) {return;}
            pnd_Et_Cnt.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #ET-CNT
            if (condition(pnd_Et_Cnt.greater(50)))                                                                                                                        //Natural: IF #ET-CNT GT 50
            {
                pnd_Et_Cnt.reset();                                                                                                                                       //Natural: RESET #ET-CNT
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet734 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  CHECK-COR
    }
    private void sub_Verify_Latest_Record() throws Exception                                                                                                              //Natural: VERIFY-LATEST-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************
        //*  /* JUST FOR CORR AND MDO
        pnd_Next_Record.reset();                                                                                                                                          //Natural: RESET #NEXT-RECORD BENA9879 ##RETURN-CODE
        pdaBena9879.getBena9879().reset();
        pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().reset();
        if (condition(pdaBena9860.getBena9860_Intrfcng_Systm().equals("ACIS") || (pdaBena9860.getBena9860_Intrfcng_Systm().equals("MDO") && pdaBena9860.getBena9860_Rqstng_Systm().equals("MDO")))) //Natural: IF BENA9860.INTRFCNG-SYSTM = 'ACIS' OR ( BENA9860.INTRFCNG-SYSTM = 'MDO' AND BENA9860.RQSTNG-SYSTM = 'MDO' )
        {
            pdaBena9879.getBena9879_Pnd_Pin().setValue(pdaBena9860.getBena9860_Pin());                                                                                    //Natural: MOVE BENA9860.PIN TO BENA9879.#PIN
            pdaBena9879.getBena9879_Pnd_Tiaa_Cntrct().setValue(pdaBena9860.getBena9860_Tiaa_Cntrct());                                                                    //Natural: MOVE BENA9860.TIAA-CNTRCT TO BENA9879.#TIAA-CNTRCT
            pdaBena9879.getBena9879_Pnd_Tiaa_Cref_Ind().setValue(pdaBena9860.getBena9860_Tiaa_Cref_Ind());                                                                //Natural: MOVE BENA9860.TIAA-CREF-IND TO BENA9879.#TIAA-CREF-IND
            pdaBena9879.getBena9879_Pnd_Rcrd_Crtd_Dte().setValue(pdaBena9860.getBena9860_Rcrd_Crtd_Dte());                                                                //Natural: MOVE BENA9860.RCRD-CRTD-DTE TO BENA9879.#RCRD-CRTD-DTE
            pdaBena9879.getBena9879_Pnd_Rcrd_Crtd_Tme().setValue(pdaBena9860.getBena9860_Rcrd_Crtd_Tme());                                                                //Natural: MOVE BENA9860.RCRD-CRTD-TME TO BENA9879.#RCRD-CRTD-TME
            DbsUtil.callnat(Benn9879.class , getCurrentProcessState(), pdaBena9879.getBena9879(), pdaBenpda_M.getMsg_Info_Sub(), pdaBenpda_E.getError_Info_Sub());        //Natural: CALLNAT 'BENN9879' BENA9879 MSG-INFO-SUB ERROR-INFO-SUB
            if (condition(Global.isEscape())) return;
            if (condition(pdaBena9879.getBena9879_Pnd_Exists().getBoolean()))                                                                                             //Natural: IF BENA9879.#EXISTS
            {
                pdaBena9872.getBena9872_Pnd_Error_Txt().setValue(pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                              //Natural: ASSIGN BENA9872.#ERROR-TXT = ##MSG
                pdaBena9872.getBena9872_Pnd_Error_Pgm().setValue(pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program());                                                   //Natural: ASSIGN BENA9872.#ERROR-PGM = ##LAST-PROGRAM
                pdaBena9872.getBena9872_Pnd_Error_Cde().setValue("NSAME");                                                                                                //Natural: ASSIGN BENA9872.#ERROR-CDE = 'NSAME'
                                                                                                                                                                          //Natural: PERFORM HANDLE-ERROR
                sub_Handle_Error();
                if (condition(Global.isEscape())) {return;}
                pdaBena9871.getBena9871_Pnd_New_Intrfce_Stts().setValue("E");                                                                                             //Natural: ASSIGN BENA9871.#NEW-INTRFCE-STTS = 'E'
                //*     PERFORM UPDATE-STATUS
                //*     ASSIGN #NEXT-RECORD = TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  JW 8/30
    private void sub_Update_Pin_From_Cor() throws Exception                                                                                                               //Natural: UPDATE-PIN-FROM-COR
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************
        pdaBena9867.getBena9867_Pnd_New_Pin().setValue(pdaBena9865.getBena9865_Pnd_Pin());                                                                                //Natural: MOVE BENA9865.#PIN TO BENA9867.#NEW-PIN
        pdaBena9867.getBena9867_Pnd_Intrfce_Stts().setValue(pdaBena9860.getBena9860_Intrfce_Stts());                                                                      //Natural: ASSIGN BENA9867.#INTRFCE-STTS = BENA9860.INTRFCE-STTS
        pdaBena9867.getBena9867_Pnd_Cntrct_Isn().setValue(pdaBena9860.getBena9860_Pnd_Cntrct_Isn());                                                                      //Natural: ASSIGN BENA9867.#CNTRCT-ISN = BENA9860.#CNTRCT-ISN
        pdaBena9867.getBena9867_Pnd_Pin_Tiaa_Cntrct().setValue(pdaBena9860.getBena9860_Pin_Tiaa_Cntrct());                                                                //Natural: ASSIGN BENA9867.#PIN-TIAA-CNTRCT = BENA9860.PIN-TIAA-CNTRCT
        pdaBena9867.getBena9867_Pnd_Tiaa_Cref_Ind().setValue(pdaBena9860.getBena9860_Tiaa_Cref_Ind());                                                                    //Natural: ASSIGN BENA9867.#TIAA-CREF-IND = BENA9860.TIAA-CREF-IND
        pdaBena9867.getBena9867_Pnd_Rcrd_Crtd_For_Bsnss_Dte().setValue(pdaBena9860.getBena9860_Rcrd_Crtd_For_Bsnss_Dte());                                                //Natural: ASSIGN BENA9867.#RCRD-CRTD-FOR-BSNSS-DTE = BENA9860.RCRD-CRTD-FOR-BSNSS-DTE
        pdaBena9867.getBena9867_Pnd_Mos_Ind().setValue(pdaBena9860.getBena9860_Mos_Ind());                                                                                //Natural: ASSIGN BENA9867.#MOS-IND = BENA9860.MOS-IND
        DbsUtil.callnat(Benn9867.class , getCurrentProcessState(), pdaBena9867.getBena9867(), pdaBenpda_M.getMsg_Info_Sub(), pdaBenpda_E.getError_Info_Sub());            //Natural: CALLNAT 'BENN9867' BENA9867 MSG-INFO-SUB ERROR-INFO-SUB
        if (condition(Global.isEscape())) return;
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ##LAST-PROGRAM = *PROGRAM
        pdaBena9860.getBena9860_Pin().setValue(pdaBena9865.getBena9865_Pnd_Pin());                                                                                        //Natural: MOVE BENA9865.#PIN TO BENA9860.PIN
        //*  UPDATE-PIN-FROM-COR JW 8/30
    }
    private void sub_Update_Status() throws Exception                                                                                                                     //Natural: UPDATE-STATUS
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        pdaBena9871.getBena9871_Pnd_Intrfce_Stts().setValue(pdaBena9860.getBena9860_Intrfce_Stts());                                                                      //Natural: ASSIGN BENA9871.#INTRFCE-STTS = BENA9860.INTRFCE-STTS
        pdaBena9871.getBena9871_Pnd_Cntrct_Isn().setValue(pdaBena9860.getBena9860_Pnd_Cntrct_Isn());                                                                      //Natural: ASSIGN BENA9871.#CNTRCT-ISN = BENA9860.#CNTRCT-ISN
        pdaBena9871.getBena9871_Pnd_Pin_Tiaa_Cntrct().setValue(pdaBena9860.getBena9860_Pin_Tiaa_Cntrct());                                                                //Natural: ASSIGN BENA9871.#PIN-TIAA-CNTRCT = BENA9860.PIN-TIAA-CNTRCT
        pdaBena9871.getBena9871_Pnd_Tiaa_Cref_Ind().setValue(pdaBena9860.getBena9860_Tiaa_Cref_Ind());                                                                    //Natural: ASSIGN BENA9871.#TIAA-CREF-IND = BENA9860.TIAA-CREF-IND
        pdaBena9871.getBena9871_Pnd_Rcrd_Crtd_For_Bsnss_Dte().setValue(pdaBena9860.getBena9860_Rcrd_Crtd_For_Bsnss_Dte());                                                //Natural: ASSIGN BENA9871.#RCRD-CRTD-FOR-BSNSS-DTE = BENA9860.RCRD-CRTD-FOR-BSNSS-DTE
        pdaBena9871.getBena9871_Pnd_Mos_Ind().setValue(pdaBena9860.getBena9860_Mos_Ind());                                                                                //Natural: ASSIGN BENA9871.#MOS-IND = BENA9860.MOS-IND
        pdaBena9871.getBena9871_Pnd_Bsnss_Dte().setValue(pdaBena9873.getBena9873_Pnd_Bsnss_Dte());                                                                        //Natural: ASSIGN BENA9871.#BSNSS-DTE = BENA9873.#BSNSS-DTE
        pdaBena9871.getBena9871_Pnd_Intrfce_Stts().setValue(pdaBena9860.getBena9860_Intrfce_Stts());                                                                      //Natural: ASSIGN BENA9871.#INTRFCE-STTS = BENA9860.INTRFCE-STTS
        DbsUtil.callnat(Benn9871.class , getCurrentProcessState(), pdaBena9871.getBena9871(), pdaBenpda_M.getMsg_Info_Sub(), pdaBenpda_E.getError_Info_Sub());            //Natural: CALLNAT 'BENN9871' BENA9871 MSG-INFO-SUB ERROR-INFO-SUB
        if (condition(Global.isEscape())) return;
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ##LAST-PROGRAM = *PROGRAM
        //*  UPDATE-STATUS
    }
    private void sub_Edits() throws Exception                                                                                                                             //Natural: EDITS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************
        pnd_Next_Record.reset();                                                                                                                                          //Natural: RESET #NEXT-RECORD BENA9866 BENA9877 ##RETURN-CODE
        pdaBena9866.getBena9866().reset();
        pdaBena9877.getBena9877().reset();
        pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().reset();
        //*   INTERFACE SPECIFIC EDITS
        DbsUtil.callnat(Benn9866.class , getCurrentProcessState(), pdaBena9860.getBena9860(), pdaBena9866.getBena9866(), pdaBena9873.getBena9873(), pdaBenpda_M.getMsg_Info_Sub(),  //Natural: CALLNAT 'BENN9866' BENA9860 BENA9866 BENA9873 MSG-INFO-SUB ERROR-INFO-SUB
            pdaBenpda_E.getError_Info_Sub());
        if (condition(Global.isEscape())) return;
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ##LAST-PROGRAM = *PROGRAM
        if (condition(pdaBena9866.getBena9866_Pnd_Edit_Failed().getBoolean()))                                                                                            //Natural: IF BENA9866.#EDIT-FAILED
        {
            pdaBena9872.getBena9872_Pnd_Error_Txt().setValue(pdaBena9866.getBena9866_Pnd_Error_Msg());                                                                    //Natural: ASSIGN BENA9872.#ERROR-TXT = BENA9866.#ERROR-MSG
            pdaBena9872.getBena9872_Pnd_Error_Pgm().setValue(pdaBena9866.getBena9866_Pnd_Error_Pgm());                                                                    //Natural: ASSIGN BENA9872.#ERROR-PGM = BENA9866.#ERROR-PGM
            pdaBena9872.getBena9872_Pnd_Error_Cde().setValue(pdaBena9866.getBena9866_Pnd_Error_Cde());                                                                    //Natural: ASSIGN BENA9872.#ERROR-CDE = BENA9866.#ERROR-CDE
                                                                                                                                                                          //Natural: PERFORM HANDLE-ERROR
            sub_Handle_Error();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("T")))                                                                                     //Natural: IF ##RETURN-CODE = 'T'
        {
                                                                                                                                                                          //Natural: PERFORM TERMINATE-PROCESS
            sub_Terminate_Process();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*   BENE SPECIFIC EDITS
        pdaBena9877.getBena9877_Pnd_Bsnss_Dte().setValue(pdaBena9860.getBena9860_Rcrd_Crtd_For_Bsnss_Dte());                                                              //Natural: ASSIGN BENA9877.#BSNSS-DTE = BENA9860.RCRD-CRTD-FOR-BSNSS-DTE
        pdaBena9877.getBena9877_Pnd_Cntrct_Lob_Cde().setValue(pdaBena9865.getBena9865_Pnd_Cntrct_Lob_Cde());                                                              //Natural: ASSIGN BENA9877.#CNTRCT-LOB-CDE = BENA9865.#CNTRCT-LOB-CDE
        pdaBena9877.getBena9877_Pnd_Cntrct_Nmbr_Range_Cde().setValue(pdaBena9865.getBena9865_Pnd_Cntrct_Nmbr_Range_Cde());                                                //Natural: ASSIGN BENA9877.#CNTRCT-NMBR-RANGE-CDE = BENA9865.#CNTRCT-NMBR-RANGE-CDE
        pdaBena9877.getBena9877_Pnd_Cntrct_Spcl_Cnsdrtn_Cde().setValue(pdaBena9865.getBena9865_Pnd_Cntrct_Spcl_Cnsdrtn_Cde());                                            //Natural: ASSIGN BENA9877.#CNTRCT-SPCL-CNSDRTN-CDE = BENA9865.#CNTRCT-SPCL-CNSDRTN-CDE
        pdaBena9877.getBena9877_Pnd_Cref_Cntrct_Nmbr().setValue(pdaBena9865.getBena9865_Pnd_Cntrct_Cref_Nbr());                                                           //Natural: ASSIGN BENA9877.#CREF-CNTRCT-NMBR = BENA9865.#CNTRCT-CREF-NBR
        DbsUtil.callnat(Benn9877.class , getCurrentProcessState(), pdaBena9877.getBena9877(), pdaBena9878.getBena9878(), pdaBena9860.getBena9860(), pdaBena9864.getBena9864(),  //Natural: CALLNAT 'BENN9877' BENA9877 BENA9878 BENA9860 BENA9864 MSG-INFO-SUB ERROR-INFO-SUB
            pdaBenpda_M.getMsg_Info_Sub(), pdaBenpda_E.getError_Info_Sub());
        if (condition(Global.isEscape())) return;
        pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                               //Natural: ASSIGN ##LAST-PROGRAM = *PROGRAM
        if (condition(pdaBena9877.getBena9877_Pnd_Edit_Failed().getBoolean()))                                                                                            //Natural: IF BENA9877.#EDIT-FAILED
        {
            pdaBena9872.getBena9872_Pnd_Error_Txt().setValue(pdaBena9877.getBena9877_Pnd_Error_Msg());                                                                    //Natural: ASSIGN BENA9872.#ERROR-TXT = BENA9877.#ERROR-MSG
            pdaBena9872.getBena9872_Pnd_Error_Pgm().setValue(pdaBena9877.getBena9877_Pnd_Error_Pgm());                                                                    //Natural: ASSIGN BENA9872.#ERROR-PGM = BENA9877.#ERROR-PGM
            pdaBena9872.getBena9872_Pnd_Error_Cde().setValue(pdaBena9877.getBena9877_Pnd_Error_Cde());                                                                    //Natural: ASSIGN BENA9872.#ERROR-CDE = BENA9877.#ERROR-CDE
                                                                                                                                                                          //Natural: PERFORM HANDLE-ERROR
            sub_Handle_Error();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  EDITS
    }
    private void sub_Handle_Error() throws Exception                                                                                                                      //Natural: HANDLE-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        getReports().write(0, "**************************************************",NEWLINE,"ERROR FOR",pdaBena9860.getBena9860_Pin_Tiaa_Cntrct(),NEWLINE,                 //Natural: WRITE '**************************************************' / 'ERROR FOR' BENA9860.PIN-TIAA-CNTRCT / '=' BENA9872.#ERROR-TXT / '=' BENA9872.#ERROR-PGM / '=' BENA9872.#ERROR-CDE
            "=",pdaBena9872.getBena9872_Pnd_Error_Txt(),NEWLINE,"=",pdaBena9872.getBena9872_Pnd_Error_Pgm(),NEWLINE,"=",pdaBena9872.getBena9872_Pnd_Error_Cde());
        if (Global.isEscape()) return;
        getReports().write(0, "**************************************************");                                                                                      //Natural: WRITE '**************************************************'
        if (Global.isEscape()) return;
        pnd_Next_Record.setValue(true);                                                                                                                                   //Natural: ASSIGN #NEXT-RECORD = TRUE
                                                                                                                                                                          //Natural: PERFORM WRITE-TO-ERROR-TABLE
        sub_Write_To_Error_Table();
        if (condition(Global.isEscape())) {return;}
        pdaBena9871.getBena9871_Pnd_New_Intrfce_Stts().setValue("E");                                                                                                     //Natural: ASSIGN BENA9871.#NEW-INTRFCE-STTS = 'E'
                                                                                                                                                                          //Natural: PERFORM UPDATE-STATUS
        sub_Update_Status();
        if (condition(Global.isEscape())) {return;}
        //*  IF AN EDIT FAILED AND IT's an MDO change, update the BENE-FILES with
        //*   A STATUS OF 'M'
        //*  JW 10/04
        if (condition((pdaBena9860.getBena9860_Rqstng_Systm().equals("MDO") || pdaBena9860.getBena9860_Rqstng_Systm().equals("CORR")) && pdaBena9860.getBena9860_Chng_New_Issue_Ind().equals("C"))) //Natural: IF ( BENA9860.RQSTNG-SYSTM = 'MDO' OR BENA9860.RQSTNG-SYSTM = 'CORR' ) AND BENA9860.CHNG-NEW-ISSUE-IND = 'C'
        {
            pdaBena9869.getBena9869_Pnd_Pin_Tiaa_Cntrct().setValue(pdaBena9860.getBena9860_Pin_Tiaa_Cntrct());                                                            //Natural: ASSIGN BENA9869.#PIN-TIAA-CNTRCT = BENA9860.PIN-TIAA-CNTRCT
            //*  ASSIGN BENA9869.#TIAA-CREF-IND   = BENA9800.TIAA-CREF-IND JW 11/7/00
            //*  ALL RECORDS (1 OR 2) UPDATED, REGARDLESS OF TIAA-CREF-IND (CORR ONLY
            pdaBena9869.getBena9869_Pnd_Old_Stts().setValue("C");                                                                                                         //Natural: ASSIGN BENA9869.#OLD-STTS = 'C'
            pdaBena9869.getBena9869_Pnd_New_Stts().setValue("M");                                                                                                         //Natural: ASSIGN BENA9869.#NEW-STTS = 'M'
            DbsUtil.callnat(Benn9869.class , getCurrentProcessState(), pdaBena9869.getBena9869(), pdaBenpda_M.getMsg_Info_Sub(), pdaBenpda_E.getError_Info_Sub());        //Natural: CALLNAT 'BENN9869' BENA9869 MSG-INFO-SUB ERROR-INFO-SUB
            if (condition(Global.isEscape())) return;
            pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program().setValue(Global.getPROGRAM());                                                                           //Natural: ASSIGN ##LAST-PROGRAM = *PROGRAM
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Et_Cnt.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #ET-CNT
        if (condition(pnd_Et_Cnt.greater(50)))                                                                                                                            //Natural: IF #ET-CNT GT 50
        {
            pnd_Et_Cnt.reset();                                                                                                                                           //Natural: RESET #ET-CNT
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
        //*  HANDLE-ERROR
    }
    private void sub_Terminate_Process() throws Exception                                                                                                                 //Natural: TERMINATE-PROCESS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        getReports().write(0, "*************************************************************");                                                                           //Natural: WRITE '*************************************************************'
        if (Global.isEscape()) return;
        getReports().write(0, "*************************************************************");                                                                           //Natural: WRITE '*************************************************************'
        if (Global.isEscape()) return;
        getReports().write(0, "*                                                           *");                                                                           //Natural: WRITE '*                                                           *'
        if (Global.isEscape()) return;
        getReports().write(0, "*   E   R   R   O   R      O   C   C   U   R   R   E   D ! !*");                                                                           //Natural: WRITE '*   E   R   R   O   R      O   C   C   U   R   R   E   D ! !*'
        if (Global.isEscape()) return;
        getReports().write(0, "*                                                           *");                                                                           //Natural: WRITE '*                                                           *'
        if (Global.isEscape()) return;
        getReports().write(0, "*************************************************************");                                                                           //Natural: WRITE '*************************************************************'
        if (Global.isEscape()) return;
        getReports().write(0, "*************************************************************");                                                                           //Natural: WRITE '*************************************************************'
        if (Global.isEscape()) return;
        getReports().write(0, "*                                                           *");                                                                           //Natural: WRITE '*                                                           *'
        if (Global.isEscape()) return;
        getReports().write(0, "*",pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                                             //Natural: WRITE '*' ##MSG
        if (Global.isEscape()) return;
        getReports().write(0, "* PROGRAM......:",pdaBenpda_E.getError_Info_Sub_Pnd_Pnd_Last_Program());                                                                   //Natural: WRITE '* PROGRAM......:' ##LAST-PROGRAM
        if (Global.isEscape()) return;
        getReports().write(0, "* ERROR NBR....:",Global.getERROR_NR());                                                                                                   //Natural: WRITE '* ERROR NBR....:' *ERROR-NR
        if (Global.isEscape()) return;
        getReports().write(0, "* ERROR LINE...:",Global.getERROR_LINE());                                                                                                 //Natural: WRITE '* ERROR LINE...:' *ERROR-LINE
        if (Global.isEscape()) return;
        getReports().write(0, "*                                                           *");                                                                           //Natural: WRITE '*                                                           *'
        if (Global.isEscape()) return;
        getReports().write(0, "* CONTACT SYSTEMS!                                          *");                                                                           //Natural: WRITE '* CONTACT SYSTEMS!                                          *'
        if (Global.isEscape()) return;
        getReports().write(0, "*                                                           *");                                                                           //Natural: WRITE '*                                                           *'
        if (Global.isEscape()) return;
        getReports().write(0, "*************************************************************");                                                                           //Natural: WRITE '*************************************************************'
        if (Global.isEscape()) return;
        getReports().write(0, "*************************************************************");                                                                           //Natural: WRITE '*************************************************************'
        if (Global.isEscape()) return;
        DbsUtil.terminate(33);  if (true) return;                                                                                                                         //Natural: TERMINATE 33
        //*  TERMINATE-PROCESS
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
                                                                                                                                                                          //Natural: PERFORM TERMINATE-PROCESS
        sub_Terminate_Process();
        if (condition(Global.isEscape())) {return;}
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=60");
    }
}
