/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:20:39 PM
**        * FROM NATURAL PROGRAM : Benp9990
************************************************************
**        * FILE NAME            : Benp9990.java
**        * CLASS NAME           : Benp9990
**        * INSTANCE NAME        : Benp9990
************************************************************
************************************************************************
* PROGRAM NAME : BENP9990
* DESCRIPTION  : RESET PIN THRU BENT ID 'RP' KEY 'RESET PIN #n'
* WRITTEN BY   : DENNIS DURAN
* DATE WRITTEN : 11/02/2001
***************************************************************A********
* WE CHOSE THIS METHOD TO AVOID HAVING TO MOVE CODE CHANGE TO PRODUCTION
* EVERYTIME WE WISH TO DO PIN RESET.
*
* BENP9999 IS RUN BY JOB P9990BFR.
*
* TO USE THIS PROGRAM, USER MUST ENTER PIN, CONTRACT AND TIAA-CREF IND
* IN BENT WITH ID 'RP' KEY 'RESET PIN #n'.  THE PROGRAM WILL READ ALL
* RECORDS WITH 'RP' KEY AND UPDATE 'P'ENDING TO 'C'URRENT AND DELETE ALL
* 'R'EQUIRE VERIFICATION RECORDS.
*
************************************************************************
*    DATE      USERID                   DESCRIPTION
* MM/DD/YYYY  XXXXXXXX  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
* 04/01/2017  DURAND    PIN EXPANSION N7 TO N12
************************************************************************

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benp9990 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_ba;
    private DbsField ba_Ba_Pin;
    private DbsField ba_Ba_Rqst_Timestamp;
    private DbsField ba_Ba_Rqst_Cmpltd_Dte;
    private DbsField ba_Count_Castba_Slctd_Cntrct_Group;

    private DbsGroup ba_Ba_Slctd_Cntrct_Group;
    private DbsField ba_Ba_Slctd_Tiaa;
    private DbsField ba_Ba_Slctd_Cref;
    private DbsField ba_Ba_Lttr_Optn;
    private DbsField ba_Ba_Vrfy_Ind;
    private DbsField ba_Ba_Vrfy_Userid;
    private DbsField ba_Ba_Vrfy_Dte;
    private DbsField ba_Ba_Vrfy_Tme;
    private DbsField ba_Ba_Rcrd_Last_Updt_Dte;
    private DbsField ba_Ba_Rcrd_Last_Updt_Tme;
    private DbsField ba_Ba_Rcrd_Last_Updt_Userid;

    private DataAccessProgramView vw_bau;
    private DbsField bau_Ba_Rqst_Cmpltd_Dte;
    private DbsField bau_Ba_Lttr_Optn;
    private DbsField bau_Ba_Vrfy_Ind;
    private DbsField bau_Ba_Vrfy_Userid;
    private DbsField bau_Ba_Vrfy_Dte;
    private DbsField bau_Ba_Vrfy_Tme;
    private DbsField bau_Ba_Rcrd_Last_Updt_Dte;
    private DbsField bau_Ba_Rcrd_Last_Updt_Tme;
    private DbsField bau_Ba_Rcrd_Last_Updt_Userid;

    private DataAccessProgramView vw_bc;
    private DbsField bc_Bc_Pin;
    private DbsField bc_Bc_Tiaa_Cntrct;
    private DbsField bc_Bc_Cref_Cntrct;
    private DbsField bc_Bc_Tiaa_Cref_Ind;
    private DbsField bc_Bc_Stat;
    private DbsField bc_Bc_Mos_Ind;
    private DbsField bc_Bc_Last_Vrfy_Dte;
    private DbsField bc_Bc_Last_Vrfy_Tme;
    private DbsField bc_Bc_Last_Vrfy_Userid;
    private DbsField bc_Bc_Rcrd_Last_Updt_Dte;
    private DbsField bc_Bc_Rcrd_Last_Updt_Tme;
    private DbsField bc_Bc_Rcrd_Last_Updt_Userid;
    private DbsField bc_Bc_Rqst_Timestamp;

    private DataAccessProgramView vw_bcu;
    private DbsField bcu_Bc_Stat;
    private DbsField bcu_Bc_Last_Vrfy_Dte;
    private DbsField bcu_Bc_Last_Vrfy_Tme;
    private DbsField bcu_Bc_Last_Vrfy_Userid;
    private DbsField bcu_Bc_Rcrd_Last_Updt_Dte;
    private DbsField bcu_Bc_Rcrd_Last_Updt_Tme;
    private DbsField bcu_Bc_Rcrd_Last_Updt_Userid;

    private DataAccessProgramView vw_bd;
    private DbsField bd_Bd_Pin;
    private DbsField bd_Bd_Tiaa_Cntrct;
    private DbsField bd_Bd_Cref_Cntrct;
    private DbsField bd_Bd_Tiaa_Cref_Ind;
    private DbsField bd_Bd_Stat;
    private DbsField bd_Bd_Rcrd_Last_Updt_Dte;
    private DbsField bd_Bd_Rcrd_Last_Updt_Tme;
    private DbsField bd_Bd_Rcrd_Last_Updt_Userid;
    private DbsField bd_Bd_Rqst_Timestamp;

    private DataAccessProgramView vw_bdu;
    private DbsField bdu_Bd_Stat;
    private DbsField bdu_Bd_Rcrd_Last_Updt_Dte;
    private DbsField bdu_Bd_Rcrd_Last_Updt_Tme;
    private DbsField bdu_Bd_Rcrd_Last_Updt_Userid;
    private DbsField bdu_Bd_Rqst_Timestamp;

    private DataAccessProgramView vw_bm;
    private DbsField bm_Bm_Pin;
    private DbsField bm_Bm_Tiaa_Cntrct;
    private DbsField bm_Bm_Cref_Cntrct;
    private DbsField bm_Bm_Tiaa_Cref_Ind;
    private DbsField bm_Bm_Stat;
    private DbsField bm_Bm_Rcrd_Last_Updt_Dte;
    private DbsField bm_Bm_Rcrd_Last_Updt_Tme;
    private DbsField bm_Bm_Rcrd_Last_Updt_Userid;
    private DbsField bm_Bm_Rqst_Timestamp;

    private DataAccessProgramView vw_bmu;
    private DbsField bmu_Bm_Stat;
    private DbsField bmu_Bm_Rcrd_Last_Updt_Dte;
    private DbsField bmu_Bm_Rcrd_Last_Updt_Tme;
    private DbsField bmu_Bm_Rcrd_Last_Updt_Userid;
    private DbsField bmu_Bm_Rqst_Timestamp;

    private DataAccessProgramView vw_bt;
    private DbsField bt_Bt_Table_Id;
    private DbsField bt_Bt_Table_Key;
    private DbsField bt_Bt_Table_Text;

    private DbsGroup bt__R_Field_1;
    private DbsField bt_Bt_Pin;
    private DbsField bt_Bt_Filler_1;
    private DbsField bt_Bt_Tiaa;
    private DbsField bt_Bt_Filler_2;
    private DbsField bt_Bt_Tfslash_C_Ind;
    private DbsField bt_Bt_Rcrd_Last_Updt_Dte;
    private DbsField bt_Bt_Rcrd_Last_Updt_Tme;
    private DbsField bt_Bt_Rcrd_Last_Updt_Userid;
    private DbsField pnd_Bc_Pin_Cntrct_Ind_Key;

    private DbsGroup pnd_Bc_Pin_Cntrct_Ind_Key__R_Field_2;
    private DbsField pnd_Bc_Pin_Cntrct_Ind_Key_Pnd_Bc_Pin;
    private DbsField pnd_Bc_Pin_Cntrct_Ind_Key_Pnd_Bc_Cntrct;
    private DbsField pnd_Bc_Pin_Cntrct_Ind_Key_Pnd_Bc_Tiaa_Cref_Ind;
    private DbsField pnd_Ba_Pin_Rqst_Seq_Key;

    private DbsGroup pnd_Ba_Pin_Rqst_Seq_Key__R_Field_3;
    private DbsField pnd_Ba_Pin_Rqst_Seq_Key_Pnd_Ba_Pin;
    private DbsField pnd_Ba_Pin_Rqst_Seq_Key_Pnd_Ba_Rqst_Timestamp;
    private DbsField pnd_Ba_Pin_Rqst_Seq_Key_Pnd_Ba_Seq_Nmbr;
    private DbsField pnd_Bt_Isn;
    private DbsField pnd_I;
    private DbsField pnd_Pin_Reset;
    private DbsField pnd_Sys_Dte;
    private DbsField pnd_Timn;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_ba = new DataAccessProgramView(new NameInfo("vw_ba", "BA"), "BENE_ACTIVITY_12", "BENE_ACTIVITY", DdmPeriodicGroups.getInstance().getGroups("BENE_ACTIVITY_12"));
        ba_Ba_Pin = vw_ba.getRecord().newFieldInGroup("ba_Ba_Pin", "BA-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BA_PIN");
        ba_Ba_Pin.setDdmHeader("PIN");
        ba_Ba_Rqst_Timestamp = vw_ba.getRecord().newFieldInGroup("ba_Ba_Rqst_Timestamp", "BA-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BA_RQST_TIMESTAMP");
        ba_Ba_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        ba_Ba_Rqst_Cmpltd_Dte = vw_ba.getRecord().newFieldInGroup("ba_Ba_Rqst_Cmpltd_Dte", "BA-RQST-CMPLTD-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BA_RQST_CMPLTD_DTE");
        ba_Ba_Rqst_Cmpltd_Dte.setDdmHeader("RQST/COMPLETED/DATE");
        ba_Count_Castba_Slctd_Cntrct_Group = vw_ba.getRecord().newFieldInGroup("ba_Count_Castba_Slctd_Cntrct_Group", "C*BA-SLCTD-CNTRCT-GROUP", RepeatingFieldStrategy.CAsteriskVariable, 
            "BENE_ACTIVITY_BA_SLCTD_CNTRCT_GROUP");

        ba_Ba_Slctd_Cntrct_Group = vw_ba.getRecord().newGroupArrayInGroup("ba_Ba_Slctd_Cntrct_Group", "BA-SLCTD-CNTRCT-GROUP", new DbsArrayController(1, 
            20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "BENE_ACTIVITY_BA_SLCTD_CNTRCT_GROUP");
        ba_Ba_Slctd_Tiaa = ba_Ba_Slctd_Cntrct_Group.newFieldInGroup("ba_Ba_Slctd_Tiaa", "BA-SLCTD-TIAA", FieldType.STRING, 10, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "BA_SLCTD_TIAA", "BENE_ACTIVITY_BA_SLCTD_CNTRCT_GROUP");
        ba_Ba_Slctd_Tiaa.setDdmHeader("SELECTED/TIAA/CONTRACTS");
        ba_Ba_Slctd_Cref = ba_Ba_Slctd_Cntrct_Group.newFieldInGroup("ba_Ba_Slctd_Cref", "BA-SLCTD-CREF", FieldType.STRING, 10, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "BA_SLCTD_CREF", "BENE_ACTIVITY_BA_SLCTD_CNTRCT_GROUP");
        ba_Ba_Slctd_Cref.setDdmHeader("SELECTED/CREF/CONTRACTS");
        ba_Ba_Lttr_Optn = vw_ba.getRecord().newFieldInGroup("ba_Ba_Lttr_Optn", "BA-LTTR-OPTN", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BA_LTTR_OPTN");
        ba_Ba_Lttr_Optn.setDdmHeader("LETTER/OPTION");
        ba_Ba_Vrfy_Ind = vw_ba.getRecord().newFieldInGroup("ba_Ba_Vrfy_Ind", "BA-VRFY-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BA_VRFY_IND");
        ba_Ba_Vrfy_Ind.setDdmHeader("VRFY/IND");
        ba_Ba_Vrfy_Userid = vw_ba.getRecord().newFieldInGroup("ba_Ba_Vrfy_Userid", "BA-VRFY-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BA_VRFY_USERID");
        ba_Ba_Vrfy_Userid.setDdmHeader("VERIFY/USER ID");
        ba_Ba_Vrfy_Dte = vw_ba.getRecord().newFieldInGroup("ba_Ba_Vrfy_Dte", "BA-VRFY-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "BA_VRFY_DTE");
        ba_Ba_Vrfy_Dte.setDdmHeader("VERIFY/DATE");
        ba_Ba_Vrfy_Tme = vw_ba.getRecord().newFieldInGroup("ba_Ba_Vrfy_Tme", "BA-VRFY-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, "BA_VRFY_TME");
        ba_Ba_Vrfy_Tme.setDdmHeader("VERIFY/TIME");
        ba_Ba_Rcrd_Last_Updt_Dte = vw_ba.getRecord().newFieldInGroup("ba_Ba_Rcrd_Last_Updt_Dte", "BA-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BA_RCRD_LAST_UPDT_DTE");
        ba_Ba_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        ba_Ba_Rcrd_Last_Updt_Tme = vw_ba.getRecord().newFieldInGroup("ba_Ba_Rcrd_Last_Updt_Tme", "BA-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BA_RCRD_LAST_UPDT_TME");
        ba_Ba_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        ba_Ba_Rcrd_Last_Updt_Userid = vw_ba.getRecord().newFieldInGroup("ba_Ba_Rcrd_Last_Updt_Userid", "BA-RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "BA_RCRD_LAST_UPDT_USERID");
        ba_Ba_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        registerRecord(vw_ba);

        vw_bau = new DataAccessProgramView(new NameInfo("vw_bau", "BAU"), "BENE_ACTIVITY_12", "BENE_ACTIVITY");
        bau_Ba_Rqst_Cmpltd_Dte = vw_bau.getRecord().newFieldInGroup("bau_Ba_Rqst_Cmpltd_Dte", "BA-RQST-CMPLTD-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BA_RQST_CMPLTD_DTE");
        bau_Ba_Rqst_Cmpltd_Dte.setDdmHeader("RQST/COMPLETED/DATE");
        bau_Ba_Lttr_Optn = vw_bau.getRecord().newFieldInGroup("bau_Ba_Lttr_Optn", "BA-LTTR-OPTN", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BA_LTTR_OPTN");
        bau_Ba_Lttr_Optn.setDdmHeader("LETTER/OPTION");
        bau_Ba_Vrfy_Ind = vw_bau.getRecord().newFieldInGroup("bau_Ba_Vrfy_Ind", "BA-VRFY-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BA_VRFY_IND");
        bau_Ba_Vrfy_Ind.setDdmHeader("VRFY/IND");
        bau_Ba_Vrfy_Userid = vw_bau.getRecord().newFieldInGroup("bau_Ba_Vrfy_Userid", "BA-VRFY-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BA_VRFY_USERID");
        bau_Ba_Vrfy_Userid.setDdmHeader("VERIFY/USER ID");
        bau_Ba_Vrfy_Dte = vw_bau.getRecord().newFieldInGroup("bau_Ba_Vrfy_Dte", "BA-VRFY-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "BA_VRFY_DTE");
        bau_Ba_Vrfy_Dte.setDdmHeader("VERIFY/DATE");
        bau_Ba_Vrfy_Tme = vw_bau.getRecord().newFieldInGroup("bau_Ba_Vrfy_Tme", "BA-VRFY-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, "BA_VRFY_TME");
        bau_Ba_Vrfy_Tme.setDdmHeader("VERIFY/TIME");
        bau_Ba_Rcrd_Last_Updt_Dte = vw_bau.getRecord().newFieldInGroup("bau_Ba_Rcrd_Last_Updt_Dte", "BA-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BA_RCRD_LAST_UPDT_DTE");
        bau_Ba_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bau_Ba_Rcrd_Last_Updt_Tme = vw_bau.getRecord().newFieldInGroup("bau_Ba_Rcrd_Last_Updt_Tme", "BA-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BA_RCRD_LAST_UPDT_TME");
        bau_Ba_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bau_Ba_Rcrd_Last_Updt_Userid = vw_bau.getRecord().newFieldInGroup("bau_Ba_Rcrd_Last_Updt_Userid", "BA-RCRD-LAST-UPDT-USERID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "BA_RCRD_LAST_UPDT_USERID");
        bau_Ba_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        registerRecord(vw_bau);

        vw_bc = new DataAccessProgramView(new NameInfo("vw_bc", "BC"), "BENE_CONTRACT_12", "BENE_CONTRACT");
        bc_Bc_Pin = vw_bc.getRecord().newFieldInGroup("bc_Bc_Pin", "BC-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BC_PIN");
        bc_Bc_Pin.setDdmHeader("PIN");
        bc_Bc_Tiaa_Cntrct = vw_bc.getRecord().newFieldInGroup("bc_Bc_Tiaa_Cntrct", "BC-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BC_TIAA_CNTRCT");
        bc_Bc_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bc_Bc_Cref_Cntrct = vw_bc.getRecord().newFieldInGroup("bc_Bc_Cref_Cntrct", "BC-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BC_CREF_CNTRCT");
        bc_Bc_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bc_Bc_Tiaa_Cref_Ind = vw_bc.getRecord().newFieldInGroup("bc_Bc_Tiaa_Cref_Ind", "BC-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_TIAA_CREF_IND");
        bc_Bc_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bc_Bc_Stat = vw_bc.getRecord().newFieldInGroup("bc_Bc_Stat", "BC-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_STAT");
        bc_Bc_Stat.setDdmHeader("STATUS");
        bc_Bc_Mos_Ind = vw_bc.getRecord().newFieldInGroup("bc_Bc_Mos_Ind", "BC-MOS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_MOS_IND");
        bc_Bc_Mos_Ind.setDdmHeader("MOS/IND");
        bc_Bc_Last_Vrfy_Dte = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Vrfy_Dte", "BC-LAST-VRFY-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_VRFY_DTE");
        bc_Bc_Last_Vrfy_Dte.setDdmHeader("LAST/VRFY/DATE");
        bc_Bc_Last_Vrfy_Tme = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Vrfy_Tme", "BC-LAST-VRFY-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_LAST_VRFY_TME");
        bc_Bc_Last_Vrfy_Tme.setDdmHeader("LAST/VRFY/TIME");
        bc_Bc_Last_Vrfy_Userid = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Vrfy_Userid", "BC-LAST-VRFY-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_VRFY_USERID");
        bc_Bc_Last_Vrfy_Userid.setDdmHeader("LAST/VRFY/USERID");
        bc_Bc_Rcrd_Last_Updt_Dte = vw_bc.getRecord().newFieldInGroup("bc_Bc_Rcrd_Last_Updt_Dte", "BC-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_RCRD_LAST_UPDT_DTE");
        bc_Bc_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bc_Bc_Rcrd_Last_Updt_Tme = vw_bc.getRecord().newFieldInGroup("bc_Bc_Rcrd_Last_Updt_Tme", "BC-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_RCRD_LAST_UPDT_TME");
        bc_Bc_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bc_Bc_Rcrd_Last_Updt_Userid = vw_bc.getRecord().newFieldInGroup("bc_Bc_Rcrd_Last_Updt_Userid", "BC-RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "BC_RCRD_LAST_UPDT_USERID");
        bc_Bc_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        bc_Bc_Rqst_Timestamp = vw_bc.getRecord().newFieldInGroup("bc_Bc_Rqst_Timestamp", "BC-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BC_RQST_TIMESTAMP");
        bc_Bc_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        registerRecord(vw_bc);

        vw_bcu = new DataAccessProgramView(new NameInfo("vw_bcu", "BCU"), "BENE_CONTRACT_12", "BENE_CONTRACT");
        bcu_Bc_Stat = vw_bcu.getRecord().newFieldInGroup("bcu_Bc_Stat", "BC-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_STAT");
        bcu_Bc_Stat.setDdmHeader("STATUS");
        bcu_Bc_Last_Vrfy_Dte = vw_bcu.getRecord().newFieldInGroup("bcu_Bc_Last_Vrfy_Dte", "BC-LAST-VRFY-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_VRFY_DTE");
        bcu_Bc_Last_Vrfy_Dte.setDdmHeader("LAST/VRFY/DATE");
        bcu_Bc_Last_Vrfy_Tme = vw_bcu.getRecord().newFieldInGroup("bcu_Bc_Last_Vrfy_Tme", "BC-LAST-VRFY-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_LAST_VRFY_TME");
        bcu_Bc_Last_Vrfy_Tme.setDdmHeader("LAST/VRFY/TIME");
        bcu_Bc_Last_Vrfy_Userid = vw_bcu.getRecord().newFieldInGroup("bcu_Bc_Last_Vrfy_Userid", "BC-LAST-VRFY-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_VRFY_USERID");
        bcu_Bc_Last_Vrfy_Userid.setDdmHeader("LAST/VRFY/USERID");
        bcu_Bc_Rcrd_Last_Updt_Dte = vw_bcu.getRecord().newFieldInGroup("bcu_Bc_Rcrd_Last_Updt_Dte", "BC-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_RCRD_LAST_UPDT_DTE");
        bcu_Bc_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bcu_Bc_Rcrd_Last_Updt_Tme = vw_bcu.getRecord().newFieldInGroup("bcu_Bc_Rcrd_Last_Updt_Tme", "BC-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_RCRD_LAST_UPDT_TME");
        bcu_Bc_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bcu_Bc_Rcrd_Last_Updt_Userid = vw_bcu.getRecord().newFieldInGroup("bcu_Bc_Rcrd_Last_Updt_Userid", "BC-RCRD-LAST-UPDT-USERID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "BC_RCRD_LAST_UPDT_USERID");
        bcu_Bc_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        registerRecord(vw_bcu);

        vw_bd = new DataAccessProgramView(new NameInfo("vw_bd", "BD"), "BENE_DESIGNATION_12", "BENE_DESIGNATION");
        bd_Bd_Pin = vw_bd.getRecord().newFieldInGroup("bd_Bd_Pin", "BD-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BD_PIN");
        bd_Bd_Pin.setDdmHeader("PIN");
        bd_Bd_Tiaa_Cntrct = vw_bd.getRecord().newFieldInGroup("bd_Bd_Tiaa_Cntrct", "BD-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BD_TIAA_CNTRCT");
        bd_Bd_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bd_Bd_Cref_Cntrct = vw_bd.getRecord().newFieldInGroup("bd_Bd_Cref_Cntrct", "BD-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BD_CREF_CNTRCT");
        bd_Bd_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bd_Bd_Tiaa_Cref_Ind = vw_bd.getRecord().newFieldInGroup("bd_Bd_Tiaa_Cref_Ind", "BD-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_TIAA_CREF_IND");
        bd_Bd_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bd_Bd_Stat = vw_bd.getRecord().newFieldInGroup("bd_Bd_Stat", "BD-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_STAT");
        bd_Bd_Stat.setDdmHeader("STAT");
        bd_Bd_Rcrd_Last_Updt_Dte = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rcrd_Last_Updt_Dte", "BD-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BD_RCRD_LAST_UPDT_DTE");
        bd_Bd_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bd_Bd_Rcrd_Last_Updt_Tme = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rcrd_Last_Updt_Tme", "BD-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BD_RCRD_LAST_UPDT_TME");
        bd_Bd_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bd_Bd_Rcrd_Last_Updt_Userid = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rcrd_Last_Updt_Userid", "BD-RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "BD_RCRD_LAST_UPDT_USERID");
        bd_Bd_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        bd_Bd_Rqst_Timestamp = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rqst_Timestamp", "BD-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BD_RQST_TIMESTAMP");
        bd_Bd_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        registerRecord(vw_bd);

        vw_bdu = new DataAccessProgramView(new NameInfo("vw_bdu", "BDU"), "BENE_DESIGNATION_12", "BENE_DESIGNATION");
        bdu_Bd_Stat = vw_bdu.getRecord().newFieldInGroup("bdu_Bd_Stat", "BD-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_STAT");
        bdu_Bd_Stat.setDdmHeader("STAT");
        bdu_Bd_Rcrd_Last_Updt_Dte = vw_bdu.getRecord().newFieldInGroup("bdu_Bd_Rcrd_Last_Updt_Dte", "BD-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BD_RCRD_LAST_UPDT_DTE");
        bdu_Bd_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bdu_Bd_Rcrd_Last_Updt_Tme = vw_bdu.getRecord().newFieldInGroup("bdu_Bd_Rcrd_Last_Updt_Tme", "BD-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BD_RCRD_LAST_UPDT_TME");
        bdu_Bd_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bdu_Bd_Rcrd_Last_Updt_Userid = vw_bdu.getRecord().newFieldInGroup("bdu_Bd_Rcrd_Last_Updt_Userid", "BD-RCRD-LAST-UPDT-USERID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "BD_RCRD_LAST_UPDT_USERID");
        bdu_Bd_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        bdu_Bd_Rqst_Timestamp = vw_bdu.getRecord().newFieldInGroup("bdu_Bd_Rqst_Timestamp", "BD-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BD_RQST_TIMESTAMP");
        bdu_Bd_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        registerRecord(vw_bdu);

        vw_bm = new DataAccessProgramView(new NameInfo("vw_bm", "BM"), "BENE_MOS_12", "BENE_MOS");
        bm_Bm_Pin = vw_bm.getRecord().newFieldInGroup("bm_Bm_Pin", "BM-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BM_PIN");
        bm_Bm_Pin.setDdmHeader("PIN");
        bm_Bm_Tiaa_Cntrct = vw_bm.getRecord().newFieldInGroup("bm_Bm_Tiaa_Cntrct", "BM-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BM_TIAA_CNTRCT");
        bm_Bm_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bm_Bm_Cref_Cntrct = vw_bm.getRecord().newFieldInGroup("bm_Bm_Cref_Cntrct", "BM-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BM_CREF_CNTRCT");
        bm_Bm_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bm_Bm_Tiaa_Cref_Ind = vw_bm.getRecord().newFieldInGroup("bm_Bm_Tiaa_Cref_Ind", "BM-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BM_TIAA_CREF_IND");
        bm_Bm_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bm_Bm_Stat = vw_bm.getRecord().newFieldInGroup("bm_Bm_Stat", "BM-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BM_STAT");
        bm_Bm_Stat.setDdmHeader("STAT");
        bm_Bm_Rcrd_Last_Updt_Dte = vw_bm.getRecord().newFieldInGroup("bm_Bm_Rcrd_Last_Updt_Dte", "BM-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BM_RCRD_LAST_UPDT_DTE");
        bm_Bm_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bm_Bm_Rcrd_Last_Updt_Tme = vw_bm.getRecord().newFieldInGroup("bm_Bm_Rcrd_Last_Updt_Tme", "BM-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BM_RCRD_LAST_UPDT_TME");
        bm_Bm_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bm_Bm_Rcrd_Last_Updt_Userid = vw_bm.getRecord().newFieldInGroup("bm_Bm_Rcrd_Last_Updt_Userid", "BM-RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "BM_RCRD_LAST_UPDT_USERID");
        bm_Bm_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        bm_Bm_Rqst_Timestamp = vw_bm.getRecord().newFieldInGroup("bm_Bm_Rqst_Timestamp", "BM-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BM_RQST_TIMESTAMP");
        bm_Bm_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        registerRecord(vw_bm);

        vw_bmu = new DataAccessProgramView(new NameInfo("vw_bmu", "BMU"), "BENE_MOS_12", "BENE_MOS");
        bmu_Bm_Stat = vw_bmu.getRecord().newFieldInGroup("bmu_Bm_Stat", "BM-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BM_STAT");
        bmu_Bm_Stat.setDdmHeader("STAT");
        bmu_Bm_Rcrd_Last_Updt_Dte = vw_bmu.getRecord().newFieldInGroup("bmu_Bm_Rcrd_Last_Updt_Dte", "BM-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BM_RCRD_LAST_UPDT_DTE");
        bmu_Bm_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bmu_Bm_Rcrd_Last_Updt_Tme = vw_bmu.getRecord().newFieldInGroup("bmu_Bm_Rcrd_Last_Updt_Tme", "BM-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BM_RCRD_LAST_UPDT_TME");
        bmu_Bm_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bmu_Bm_Rcrd_Last_Updt_Userid = vw_bmu.getRecord().newFieldInGroup("bmu_Bm_Rcrd_Last_Updt_Userid", "BM-RCRD-LAST-UPDT-USERID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "BM_RCRD_LAST_UPDT_USERID");
        bmu_Bm_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        bmu_Bm_Rqst_Timestamp = vw_bmu.getRecord().newFieldInGroup("bmu_Bm_Rqst_Timestamp", "BM-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BM_RQST_TIMESTAMP");
        bmu_Bm_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        registerRecord(vw_bmu);

        vw_bt = new DataAccessProgramView(new NameInfo("vw_bt", "BT"), "BENE_TABLE_FILE", "BENE_TABLE");
        bt_Bt_Table_Id = vw_bt.getRecord().newFieldInGroup("bt_Bt_Table_Id", "BT-TABLE-ID", FieldType.STRING, 2, RepeatingFieldStrategy.None, "BT_TABLE_ID");
        bt_Bt_Table_Id.setDdmHeader("TABLE/ID");
        bt_Bt_Table_Key = vw_bt.getRecord().newFieldInGroup("bt_Bt_Table_Key", "BT-TABLE-KEY", FieldType.STRING, 20, RepeatingFieldStrategy.None, "BT_TABLE_KEY");
        bt_Bt_Table_Key.setDdmHeader("TABLE/KEY");
        bt_Bt_Table_Text = vw_bt.getRecord().newFieldInGroup("bt_Bt_Table_Text", "BT-TABLE-TEXT", FieldType.STRING, 72, RepeatingFieldStrategy.None, "BT_TABLE_TEXT");
        bt_Bt_Table_Text.setDdmHeader("TABLE/TEXT");

        bt__R_Field_1 = vw_bt.getRecord().newGroupInGroup("bt__R_Field_1", "REDEFINE", bt_Bt_Table_Text);
        bt_Bt_Pin = bt__R_Field_1.newFieldInGroup("bt_Bt_Pin", "BT-PIN", FieldType.NUMERIC, 12);
        bt_Bt_Filler_1 = bt__R_Field_1.newFieldInGroup("bt_Bt_Filler_1", "BT-FILLER-1", FieldType.STRING, 1);
        bt_Bt_Tiaa = bt__R_Field_1.newFieldInGroup("bt_Bt_Tiaa", "BT-TIAA", FieldType.STRING, 10);
        bt_Bt_Filler_2 = bt__R_Field_1.newFieldInGroup("bt_Bt_Filler_2", "BT-FILLER-2", FieldType.STRING, 1);
        bt_Bt_Tfslash_C_Ind = bt__R_Field_1.newFieldInGroup("bt_Bt_Tfslash_C_Ind", "BT-T/C-IND", FieldType.STRING, 1);
        bt_Bt_Rcrd_Last_Updt_Dte = vw_bt.getRecord().newFieldInGroup("bt_Bt_Rcrd_Last_Updt_Dte", "BT-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BT_RCRD_LAST_UPDT_DTE");
        bt_Bt_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bt_Bt_Rcrd_Last_Updt_Tme = vw_bt.getRecord().newFieldInGroup("bt_Bt_Rcrd_Last_Updt_Tme", "BT-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BT_RCRD_LAST_UPDT_TME");
        bt_Bt_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bt_Bt_Rcrd_Last_Updt_Userid = vw_bt.getRecord().newFieldInGroup("bt_Bt_Rcrd_Last_Updt_Userid", "BT-RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "BT_RCRD_LAST_UPDT_USERID");
        bt_Bt_Rcrd_Last_Updt_Userid.setDdmHeader("RECRD LAST/UPDATE/USER ID");
        registerRecord(vw_bt);

        pnd_Bc_Pin_Cntrct_Ind_Key = localVariables.newFieldInRecord("pnd_Bc_Pin_Cntrct_Ind_Key", "#BC-PIN-CNTRCT-IND-KEY", FieldType.STRING, 23);

        pnd_Bc_Pin_Cntrct_Ind_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Bc_Pin_Cntrct_Ind_Key__R_Field_2", "REDEFINE", pnd_Bc_Pin_Cntrct_Ind_Key);
        pnd_Bc_Pin_Cntrct_Ind_Key_Pnd_Bc_Pin = pnd_Bc_Pin_Cntrct_Ind_Key__R_Field_2.newFieldInGroup("pnd_Bc_Pin_Cntrct_Ind_Key_Pnd_Bc_Pin", "#BC-PIN", 
            FieldType.NUMERIC, 12);
        pnd_Bc_Pin_Cntrct_Ind_Key_Pnd_Bc_Cntrct = pnd_Bc_Pin_Cntrct_Ind_Key__R_Field_2.newFieldInGroup("pnd_Bc_Pin_Cntrct_Ind_Key_Pnd_Bc_Cntrct", "#BC-CNTRCT", 
            FieldType.STRING, 10);
        pnd_Bc_Pin_Cntrct_Ind_Key_Pnd_Bc_Tiaa_Cref_Ind = pnd_Bc_Pin_Cntrct_Ind_Key__R_Field_2.newFieldInGroup("pnd_Bc_Pin_Cntrct_Ind_Key_Pnd_Bc_Tiaa_Cref_Ind", 
            "#BC-TIAA-CREF-IND", FieldType.STRING, 1);
        pnd_Ba_Pin_Rqst_Seq_Key = localVariables.newFieldInRecord("pnd_Ba_Pin_Rqst_Seq_Key", "#BA-PIN-RQST-SEQ-KEY", FieldType.STRING, 29);

        pnd_Ba_Pin_Rqst_Seq_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Ba_Pin_Rqst_Seq_Key__R_Field_3", "REDEFINE", pnd_Ba_Pin_Rqst_Seq_Key);
        pnd_Ba_Pin_Rqst_Seq_Key_Pnd_Ba_Pin = pnd_Ba_Pin_Rqst_Seq_Key__R_Field_3.newFieldInGroup("pnd_Ba_Pin_Rqst_Seq_Key_Pnd_Ba_Pin", "#BA-PIN", FieldType.NUMERIC, 
            12);
        pnd_Ba_Pin_Rqst_Seq_Key_Pnd_Ba_Rqst_Timestamp = pnd_Ba_Pin_Rqst_Seq_Key__R_Field_3.newFieldInGroup("pnd_Ba_Pin_Rqst_Seq_Key_Pnd_Ba_Rqst_Timestamp", 
            "#BA-RQST-TIMESTAMP", FieldType.STRING, 15);
        pnd_Ba_Pin_Rqst_Seq_Key_Pnd_Ba_Seq_Nmbr = pnd_Ba_Pin_Rqst_Seq_Key__R_Field_3.newFieldInGroup("pnd_Ba_Pin_Rqst_Seq_Key_Pnd_Ba_Seq_Nmbr", "#BA-SEQ-NMBR", 
            FieldType.NUMERIC, 2);
        pnd_Bt_Isn = localVariables.newFieldInRecord("pnd_Bt_Isn", "#BT-ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_Pin_Reset = localVariables.newFieldInRecord("pnd_Pin_Reset", "#PIN-RESET", FieldType.BOOLEAN, 1);
        pnd_Sys_Dte = localVariables.newFieldInRecord("pnd_Sys_Dte", "#SYS-DTE", FieldType.STRING, 8);
        pnd_Timn = localVariables.newFieldInRecord("pnd_Timn", "#TIMN", FieldType.STRING, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ba.reset();
        vw_bau.reset();
        vw_bc.reset();
        vw_bcu.reset();
        vw_bd.reset();
        vw_bdu.reset();
        vw_bm.reset();
        vw_bmu.reset();
        vw_bt.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Benp9990() throws Exception
    {
        super("Benp9990");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT PS = 60 LS = 132
        vw_bt.startDatabaseRead                                                                                                                                           //Natural: READ BT BY BT-TABLE-ID = 'RP'
        (
        "RBT",
        new Wc[] { new Wc("BT_TABLE_ID", ">=", "RP", WcType.BY) },
        new Oc[] { new Oc("BT_TABLE_ID", "ASC") }
        );
        RBT:
        while (condition(vw_bt.readNextRow("RBT")))
        {
            if (condition(bt_Bt_Table_Id.notEquals("RP")))                                                                                                                //Natural: IF BT-TABLE-ID NE 'RP'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(bt_Bt_Table_Text.equals("000000000000,CONTRACT  ,B") || bt_Bt_Pin.equals(getZero()) || bt_Bt_Tiaa.equals("CONTRACT  ")))                        //Natural: IF BT-TABLE-TEXT = '000000000000,CONTRACT  ,B' OR BT-PIN = 0 OR BT-TIAA = 'CONTRACT  '
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
                //*  CONTRACT # TO BE CHANGED
            }                                                                                                                                                             //Natural: END-IF
            pnd_Bt_Isn.setValue(vw_bt.getAstISN("RBT"));                                                                                                                  //Natural: ASSIGN #BT-ISN := *ISN ( RBT. )
            pnd_Bc_Pin_Cntrct_Ind_Key_Pnd_Bc_Pin.setValue(bt_Bt_Pin);                                                                                                     //Natural: ASSIGN #BC-PIN := BT-PIN
            pnd_Bc_Pin_Cntrct_Ind_Key_Pnd_Bc_Cntrct.setValue(bt_Bt_Tiaa);                                                                                                 //Natural: ASSIGN #BC-CNTRCT := BT-TIAA
            //*  DON't forget if SPLIT dsng
            if (condition(bt_Bt_Tfslash_C_Ind.equals("B")))                                                                                                               //Natural: IF BT-T/C-IND = 'B'
            {
                pnd_Bc_Pin_Cntrct_Ind_Key_Pnd_Bc_Tiaa_Cref_Ind.setValue(" ");                                                                                             //Natural: ASSIGN #BC-TIAA-CREF-IND := ' '
                //*  'T' OR= 'C'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Bc_Pin_Cntrct_Ind_Key_Pnd_Bc_Tiaa_Cref_Ind.setValue(bt_Bt_Tfslash_C_Ind);                                                                             //Natural: ASSIGN #BC-TIAA-CREF-IND := BT-T/C-IND
            }                                                                                                                                                             //Natural: END-IF
            pnd_Pin_Reset.reset();                                                                                                                                        //Natural: RESET #PIN-RESET
            pnd_Timn.setValueEdited(Global.getTIMN(),new ReportEditMask("9999999"));                                                                                      //Natural: MOVE EDITED *TIMN ( EM = 9999999 ) TO #TIMN
            pnd_Sys_Dte.setValue(Global.getDATN());                                                                                                                       //Natural: ASSIGN #SYS-DTE := *DATN
                                                                                                                                                                          //Natural: PERFORM RESET-PIN-CONTRACT
            sub_Reset_Pin_Contract();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RBT"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RBT"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Pin_Reset.getBoolean()))                                                                                                                    //Natural: IF #PIN-RESET
            {
                G1:                                                                                                                                                       //Natural: GET BT #BT-ISN
                vw_bt.readByID(pnd_Bt_Isn.getLong(), "G1");
                bt_Bt_Table_Text.setValue("000000000000,CONTRACT  ,B");                                                                                                   //Natural: ASSIGN BT-TABLE-TEXT := '000000000000,CONTRACT  ,B'
                bt_Bt_Rcrd_Last_Updt_Dte.setValue(pnd_Timn);                                                                                                              //Natural: ASSIGN BT-RCRD-LAST-UPDT-DTE := #TIMN
                bt_Bt_Rcrd_Last_Updt_Tme.setValue(pnd_Sys_Dte);                                                                                                           //Natural: ASSIGN BT-RCRD-LAST-UPDT-TME := #SYS-DTE
                bt_Bt_Rcrd_Last_Updt_Userid.setValue(Global.getPROGRAM());                                                                                                //Natural: ASSIGN BT-RCRD-LAST-UPDT-USERID := *PROGRAM
                vw_bt.updateDBRow("G1");                                                                                                                                  //Natural: UPDATE ( G1. )
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                getReports().newPage(new ReportSpecification(0));                                                                                                         //Natural: NEWPAGE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RBT"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RBT"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, ReportOption.NOTITLE,NEWLINE,"Not Reset:",bt_Bt_Table_Text,NEWLINE);                                                                //Natural: WRITE NOTITLE / 'Not Reset:' BT-TABLE-TEXT /
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RBT"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RBT"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RESET-PIN-CONTRACT
        //* ***********************************************************************
    }
    private void sub_Reset_Pin_Contract() throws Exception                                                                                                                //Natural: RESET-PIN-CONTRACT
    {
        if (BLNatReinput.isReinput()) return;

        vw_bc.startDatabaseFind                                                                                                                                           //Natural: FIND BC WITH BC-PIN-CNTRCT-IND-KEY = #BC-PIN-CNTRCT-IND-KEY
        (
        "FBC",
        new Wc[] { new Wc("BC_PIN_CNTRCT_IND_KEY", "=", pnd_Bc_Pin_Cntrct_Ind_Key, WcType.WITH) }
        );
        FBC:
        while (condition(vw_bc.readNextRow("FBC")))
        {
            vw_bc.setIfNotFoundControlFlag(false);
            if (condition(bc_Bc_Pin.notEquals(pnd_Bc_Pin_Cntrct_Ind_Key_Pnd_Bc_Pin) || bc_Bc_Tiaa_Cntrct.notEquals(pnd_Bc_Pin_Cntrct_Ind_Key_Pnd_Bc_Cntrct)               //Natural: IF BC.BC-PIN NE #BC-PIN OR BC.BC-TIAA-CNTRCT NE #BC-CNTRCT OR BC.BC-TIAA-CREF-IND NE #BC-TIAA-CREF-IND
                || bc_Bc_Tiaa_Cref_Ind.notEquals(pnd_Bc_Pin_Cntrct_Ind_Key_Pnd_Bc_Tiaa_Cref_Ind)))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(bc_Bc_Stat.notEquals("P") && bc_Bc_Stat.notEquals("R")))                                                                                        //Natural: IF BC.BC-STAT NE 'P' AND BC.BC-STAT NE 'R'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            G2:                                                                                                                                                           //Natural: GET BCU *ISN ( FBC. )
            vw_bcu.readByID(vw_bc.getAstISN("FBC"), "G2");
            if (condition(bcu_Bc_Stat.equals("P")))                                                                                                                       //Natural: IF BCU.BC-STAT = 'P'
            {
                bcu_Bc_Stat.setValue("C");                                                                                                                                //Natural: ASSIGN BCU.BC-STAT := 'C'
                bcu_Bc_Last_Vrfy_Dte.setValue(pnd_Sys_Dte);                                                                                                               //Natural: ASSIGN BCU.BC-LAST-VRFY-DTE := #SYS-DTE
                bcu_Bc_Last_Vrfy_Tme.setValue(pnd_Timn);                                                                                                                  //Natural: ASSIGN BCU.BC-LAST-VRFY-TME := #TIMN
                bcu_Bc_Last_Vrfy_Userid.setValue(Global.getPROGRAM());                                                                                                    //Natural: ASSIGN BCU.BC-LAST-VRFY-USERID := *PROGRAM
                bcu_Bc_Rcrd_Last_Updt_Dte.setValue(pnd_Sys_Dte);                                                                                                          //Natural: ASSIGN BCU.BC-RCRD-LAST-UPDT-DTE := #SYS-DTE
                bcu_Bc_Rcrd_Last_Updt_Tme.setValue(pnd_Timn);                                                                                                             //Natural: ASSIGN BCU.BC-RCRD-LAST-UPDT-TME := #TIMN
                bcu_Bc_Rcrd_Last_Updt_Userid.setValue(Global.getPROGRAM());                                                                                               //Natural: ASSIGN BCU.BC-RCRD-LAST-UPDT-USERID := *PROGRAM
                getReports().write(0, ReportOption.NOTITLE,"Contract updated to");                                                                                        //Natural: WRITE NOTITLE 'Contract updated to'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FBC"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FBC"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, ReportOption.NOTITLE,"Old","=",bc_Bc_Pin);                                                                                          //Natural: WRITE 'Old' '=' BC.BC-PIN
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FBC"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FBC"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, ReportOption.NOTITLE,"Old","=",bc_Bc_Tiaa_Cntrct);                                                                                  //Natural: WRITE 'Old' '=' BC.BC-TIAA-CNTRCT
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FBC"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FBC"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, ReportOption.NOTITLE,"Old","=",bc_Bc_Tiaa_Cref_Ind);                                                                                //Natural: WRITE 'Old' '=' BC.BC-TIAA-CREF-IND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FBC"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FBC"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, ReportOption.NOTITLE,"Old","=",bc_Bc_Stat);                                                                                         //Natural: WRITE 'Old' '=' BC.BC-STAT
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FBC"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FBC"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, ReportOption.NOTITLE,"Old","=",bc_Bc_Rqst_Timestamp);                                                                               //Natural: WRITE 'Old' '=' BC.BC-RQST-TIMESTAMP
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FBC"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FBC"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, ReportOption.NOTITLE,"New","=",bcu_Bc_Stat);                                                                                        //Natural: WRITE 'New' '=' BCU.BC-STAT
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FBC"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FBC"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, ReportOption.NOTITLE,"New","=",bcu_Bc_Last_Vrfy_Dte);                                                                               //Natural: WRITE 'New' '=' BCU.BC-LAST-VRFY-DTE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FBC"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FBC"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, ReportOption.NOTITLE,"New","=",bcu_Bc_Last_Vrfy_Tme);                                                                               //Natural: WRITE 'New' '=' BCU.BC-LAST-VRFY-TME
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FBC"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FBC"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, ReportOption.NOTITLE,"New","=",bcu_Bc_Last_Vrfy_Userid);                                                                            //Natural: WRITE 'New' '=' BCU.BC-LAST-VRFY-USERID
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FBC"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FBC"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, ReportOption.NOTITLE,"New","=",bcu_Bc_Rcrd_Last_Updt_Dte);                                                                          //Natural: WRITE 'New' '=' BCU.BC-RCRD-LAST-UPDT-DTE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FBC"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FBC"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, ReportOption.NOTITLE,"New","=",bcu_Bc_Rcrd_Last_Updt_Tme);                                                                          //Natural: WRITE 'New' '=' BCU.BC-RCRD-LAST-UPDT-TME
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FBC"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FBC"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, ReportOption.NOTITLE,"New","=",bcu_Bc_Rcrd_Last_Updt_Userid,NEWLINE);                                                               //Natural: WRITE 'New' '=' BCU.BC-RCRD-LAST-UPDT-USERID /
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FBC"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FBC"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Pin_Reset.setValue(true);                                                                                                                             //Natural: ASSIGN #PIN-RESET := TRUE
                vw_bcu.updateDBRow("G2");                                                                                                                                 //Natural: UPDATE ( G2. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(bcu_Bc_Stat.equals("R")))                                                                                                                       //Natural: IF BCU.BC-STAT = 'R'
            {
                getReports().write(0, ReportOption.NOTITLE,"Contract deleted");                                                                                           //Natural: WRITE NOTITLE 'Contract deleted'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FBC"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FBC"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, ReportOption.NOTITLE,"=",bc_Bc_Pin);                                                                                                //Natural: WRITE '=' BC.BC-PIN
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FBC"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FBC"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, ReportOption.NOTITLE,"=",bc_Bc_Tiaa_Cntrct);                                                                                        //Natural: WRITE '=' BC.BC-TIAA-CNTRCT
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FBC"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FBC"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, ReportOption.NOTITLE,"=",bc_Bc_Tiaa_Cref_Ind);                                                                                      //Natural: WRITE '=' BC.BC-TIAA-CREF-IND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FBC"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FBC"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, ReportOption.NOTITLE,"=",bc_Bc_Stat);                                                                                               //Natural: WRITE '=' BC.BC-STAT
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FBC"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FBC"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, ReportOption.NOTITLE,"=",bc_Bc_Rqst_Timestamp,NEWLINE);                                                                             //Natural: WRITE '=' BC.BC-RQST-TIMESTAMP /
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FBC"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FBC"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Pin_Reset.setValue(true);                                                                                                                             //Natural: ASSIGN #PIN-RESET := TRUE
                vw_bcu.deleteDBRow("G2");                                                                                                                                 //Natural: DELETE ( G2. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(bc_Bc_Mos_Ind.equals("Y")))                                                                                                                     //Natural: IF BC.BC-MOS-IND = 'Y'
            {
                vw_bm.startDatabaseFind                                                                                                                                   //Natural: FIND BM WITH BM-PIN-CNTRCT-IND-KEY = #BC-PIN-CNTRCT-IND-KEY
                (
                "FBM",
                new Wc[] { new Wc("BM_PIN_CNTRCT_IND_KEY", "=", pnd_Bc_Pin_Cntrct_Ind_Key, WcType.WITH) }
                );
                FBM:
                while (condition(vw_bm.readNextRow("FBM")))
                {
                    vw_bm.setIfNotFoundControlFlag(false);
                    if (condition(bm_Bm_Rqst_Timestamp.notEquals(bc_Bc_Rqst_Timestamp) || bm_Bm_Stat.notEquals(bc_Bc_Stat)))                                              //Natural: IF BM.BM-RQST-TIMESTAMP NE BC.BC-RQST-TIMESTAMP OR BM.BM-STAT NE BC.BC-STAT
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    G3:                                                                                                                                                   //Natural: GET BMU *ISN ( FBM. )
                    vw_bmu.readByID(vw_bm.getAstISN("FBM"), "G3");
                    //*  CURRENT
                    if (condition(bmu_Bm_Stat.equals("P")))                                                                                                               //Natural: IF BMU.BM-STAT = 'P'
                    {
                        bmu_Bm_Stat.setValue("C");                                                                                                                        //Natural: ASSIGN BMU.BM-STAT := 'C'
                        bmu_Bm_Rcrd_Last_Updt_Dte.setValue(pnd_Sys_Dte);                                                                                                  //Natural: ASSIGN BMU.BM-RCRD-LAST-UPDT-DTE := #SYS-DTE
                        bmu_Bm_Rcrd_Last_Updt_Tme.setValue(pnd_Timn);                                                                                                     //Natural: ASSIGN BMU.BM-RCRD-LAST-UPDT-TME := #TIMN
                        bmu_Bm_Rcrd_Last_Updt_Userid.setValue(Global.getPROGRAM());                                                                                       //Natural: ASSIGN BMU.BM-RCRD-LAST-UPDT-USERID := *PROGRAM
                        getReports().write(0, ReportOption.NOTITLE,"Designation updated to");                                                                             //Natural: WRITE NOTITLE 'Designation updated to'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FBM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FBM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE,"Old","=",bm_Bm_Pin);                                                                                  //Natural: WRITE 'Old' '=' BM.BM-PIN
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FBM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FBM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE,"Old","=",bm_Bm_Tiaa_Cntrct);                                                                          //Natural: WRITE 'Old' '=' BM.BM-TIAA-CNTRCT
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FBM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FBM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE,"Old","=",bm_Bm_Tiaa_Cref_Ind);                                                                        //Natural: WRITE 'Old' '=' BM.BM-TIAA-CREF-IND
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FBM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FBM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE,"Old","=",bm_Bm_Stat);                                                                                 //Natural: WRITE 'Old' '=' BM.BM-STAT
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FBM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FBM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE,"Old","=",bm_Bm_Rqst_Timestamp,NEWLINE);                                                               //Natural: WRITE 'Old' '=' BM.BM-RQST-TIMESTAMP /
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FBM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FBM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE,"New","=",bmu_Bm_Stat);                                                                                //Natural: WRITE 'New' '=' BMU.BM-STAT
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FBM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FBM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE,"New","=",bmu_Bm_Rcrd_Last_Updt_Dte);                                                                  //Natural: WRITE 'New' '=' BMU.BM-RCRD-LAST-UPDT-DTE
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FBM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FBM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE,"New","=",bmu_Bm_Rcrd_Last_Updt_Tme);                                                                  //Natural: WRITE 'New' '=' BMU.BM-RCRD-LAST-UPDT-TME
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FBM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FBM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE,"New","=",bmu_Bm_Rcrd_Last_Updt_Userid,NEWLINE);                                                       //Natural: WRITE 'New' '=' BMU.BM-RCRD-LAST-UPDT-USERID /
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FBM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FBM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        vw_bmu.updateDBRow("G3");                                                                                                                         //Natural: UPDATE ( G3. )
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(bmu_Bm_Stat.equals("R")))                                                                                                               //Natural: IF BMU.BM-STAT = 'R'
                    {
                        getReports().write(0, ReportOption.NOTITLE,"MOS Designation deleted");                                                                            //Natural: WRITE NOTITLE 'MOS Designation deleted'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FBM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FBM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE,"=",bm_Bm_Pin);                                                                                        //Natural: WRITE '=' BM.BM-PIN
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FBM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FBM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE,"=",bm_Bm_Tiaa_Cntrct);                                                                                //Natural: WRITE '=' BM.BM-TIAA-CNTRCT
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FBM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FBM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE,"=",bm_Bm_Tiaa_Cref_Ind);                                                                              //Natural: WRITE '=' BM.BM-TIAA-CREF-IND
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FBM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FBM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE,"=",bm_Bm_Stat);                                                                                       //Natural: WRITE '=' BM.BM-STAT
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FBM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FBM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE,"=",bm_Bm_Rqst_Timestamp,NEWLINE);                                                                     //Natural: WRITE '=' BM.BM-RQST-TIMESTAMP /
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FBM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FBM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        vw_bmu.deleteDBRow("G3");                                                                                                                         //Natural: DELETE ( G3. )
                    }                                                                                                                                                     //Natural: END-IF
                    //*  FBM.
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FBC"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FBC"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                vw_bd.startDatabaseFind                                                                                                                                   //Natural: FIND BD WITH BD-PIN-CNTRCT-IND-KEY = #BC-PIN-CNTRCT-IND-KEY
                (
                "FBD",
                new Wc[] { new Wc("BD_PIN_CNTRCT_IND_KEY", "=", pnd_Bc_Pin_Cntrct_Ind_Key, WcType.WITH) }
                );
                FBD:
                while (condition(vw_bd.readNextRow("FBD")))
                {
                    vw_bd.setIfNotFoundControlFlag(false);
                    if (condition(bd_Bd_Stat.notEquals(bc_Bc_Stat) || bd_Bd_Rqst_Timestamp.notEquals(bc_Bc_Rqst_Timestamp)))                                              //Natural: IF BD.BD-STAT NE BC.BC-STAT OR BD.BD-RQST-TIMESTAMP NE BC.BC-RQST-TIMESTAMP
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    G4:                                                                                                                                                   //Natural: GET BDU *ISN ( FBD. )
                    vw_bdu.readByID(vw_bd.getAstISN("FBD"), "G4");
                    if (condition(bdu_Bd_Stat.equals("P")))                                                                                                               //Natural: IF BDU.BD-STAT = 'P'
                    {
                        bdu_Bd_Stat.setValue("C");                                                                                                                        //Natural: ASSIGN BDU.BD-STAT := 'C'
                        bdu_Bd_Rcrd_Last_Updt_Dte.setValue(pnd_Sys_Dte);                                                                                                  //Natural: ASSIGN BDU.BD-RCRD-LAST-UPDT-DTE := #SYS-DTE
                        bdu_Bd_Rcrd_Last_Updt_Tme.setValue(pnd_Timn);                                                                                                     //Natural: ASSIGN BDU.BD-RCRD-LAST-UPDT-TME := #TIMN
                        bdu_Bd_Rcrd_Last_Updt_Userid.setValue(Global.getPROGRAM());                                                                                       //Natural: ASSIGN BDU.BD-RCRD-LAST-UPDT-USERID := *PROGRAM
                        getReports().write(0, ReportOption.NOTITLE,"Designation updated to");                                                                             //Natural: WRITE NOTITLE 'Designation updated to'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FBD"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FBD"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE,"Old","=",bd_Bd_Pin);                                                                                  //Natural: WRITE 'Old' '=' BD.BD-PIN
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FBD"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FBD"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE,"Old","=",bd_Bd_Tiaa_Cntrct);                                                                          //Natural: WRITE 'Old' '=' BD.BD-TIAA-CNTRCT
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FBD"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FBD"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE,"Old","=",bd_Bd_Tiaa_Cref_Ind);                                                                        //Natural: WRITE 'Old' '=' BD.BD-TIAA-CREF-IND
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FBD"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FBD"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE,"Old","=",bd_Bd_Stat);                                                                                 //Natural: WRITE 'Old' '=' BD.BD-STAT
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FBD"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FBD"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE,"Old","=",bd_Bd_Rqst_Timestamp);                                                                       //Natural: WRITE 'Old' '=' BD.BD-RQST-TIMESTAMP
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FBD"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FBD"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE,"New","=",bdu_Bd_Stat);                                                                                //Natural: WRITE 'New' '=' BDU.BD-STAT
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FBD"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FBD"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE,"New","=",bdu_Bd_Rcrd_Last_Updt_Dte);                                                                  //Natural: WRITE 'New' '=' BDU.BD-RCRD-LAST-UPDT-DTE
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FBD"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FBD"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE,"New","=",bdu_Bd_Rcrd_Last_Updt_Tme);                                                                  //Natural: WRITE 'New' '=' BDU.BD-RCRD-LAST-UPDT-TME
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FBD"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FBD"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE,"New","=",bdu_Bd_Rcrd_Last_Updt_Userid,NEWLINE);                                                       //Natural: WRITE 'New' '=' BDU.BD-RCRD-LAST-UPDT-USERID /
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FBD"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FBD"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        vw_bdu.updateDBRow("G4");                                                                                                                         //Natural: UPDATE ( G4. )
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(bdu_Bd_Stat.equals("R")))                                                                                                               //Natural: IF BDU.BD-STAT = 'R'
                    {
                        getReports().write(0, ReportOption.NOTITLE,"Individual Bene Designation deleted");                                                                //Natural: WRITE NOTITLE 'Individual Bene Designation deleted'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FBD"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FBD"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE,"=",bd_Bd_Pin);                                                                                        //Natural: WRITE '=' BD.BD-PIN
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FBD"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FBD"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE,"=",bd_Bd_Tiaa_Cntrct);                                                                                //Natural: WRITE '=' BD.BD-TIAA-CNTRCT
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FBD"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FBD"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE,"=",bd_Bd_Tiaa_Cref_Ind);                                                                              //Natural: WRITE '=' BD.BD-TIAA-CREF-IND
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FBD"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FBD"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE,"=",bd_Bd_Stat);                                                                                       //Natural: WRITE '=' BD.BD-STAT
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FBD"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FBD"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE,"=",bd_Bd_Rqst_Timestamp,NEWLINE);                                                                     //Natural: WRITE '=' BD.BD-RQST-TIMESTAMP /
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FBD"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FBD"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        vw_bdu.deleteDBRow("G4");                                                                                                                         //Natural: DELETE ( G4. )
                    }                                                                                                                                                     //Natural: END-IF
                    //*  FBD.
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FBC"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FBC"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ba_Pin_Rqst_Seq_Key_Pnd_Ba_Pin.setValue(bc_Bc_Pin);                                                                                                       //Natural: ASSIGN #BA-PIN := BC-PIN
            pnd_Ba_Pin_Rqst_Seq_Key_Pnd_Ba_Rqst_Timestamp.setValue(bc_Bc_Rqst_Timestamp);                                                                                 //Natural: ASSIGN #BA-RQST-TIMESTAMP := BC-RQST-TIMESTAMP
            pnd_Ba_Pin_Rqst_Seq_Key_Pnd_Ba_Seq_Nmbr.setValue(1);                                                                                                          //Natural: ASSIGN #BA-SEQ-NMBR := 1
            vw_ba.startDatabaseFind                                                                                                                                       //Natural: FIND BA WITH BA-PIN-RQST-SEQ-KEY = #BA-PIN-RQST-SEQ-KEY
            (
            "FBA",
            new Wc[] { new Wc("BA_PIN_RQST_SEQ_KEY", "=", pnd_Ba_Pin_Rqst_Seq_Key, WcType.WITH) }
            );
            FBA:
            while (condition(vw_ba.readNextRow("FBA")))
            {
                vw_ba.setIfNotFoundControlFlag(false);
                //*  NO LETTER
                //*  VERIFY COMPLETED
                if (condition(ba_Ba_Vrfy_Ind.equals("1")))                                                                                                                //Natural: IF BA.BA-VRFY-IND = '1'
                {
                    G5:                                                                                                                                                   //Natural: GET BAU *ISN ( FBA. )
                    vw_bau.readByID(vw_ba.getAstISN("FBA"), "G5");
                    bau_Ba_Rqst_Cmpltd_Dte.setValue(pnd_Sys_Dte);                                                                                                         //Natural: ASSIGN BAU.BA-RQST-CMPLTD-DTE := #SYS-DTE
                    bau_Ba_Lttr_Optn.setValue("1");                                                                                                                       //Natural: ASSIGN BAU.BA-LTTR-OPTN := '1'
                    bau_Ba_Vrfy_Ind.setValue("3");                                                                                                                        //Natural: ASSIGN BAU.BA-VRFY-IND := '3'
                    bau_Ba_Vrfy_Userid.setValue(Global.getPROGRAM());                                                                                                     //Natural: ASSIGN BAU.BA-VRFY-USERID := *PROGRAM
                    bau_Ba_Vrfy_Dte.setValue(pnd_Sys_Dte);                                                                                                                //Natural: ASSIGN BAU.BA-VRFY-DTE := #SYS-DTE
                    bau_Ba_Vrfy_Tme.setValue(pnd_Timn);                                                                                                                   //Natural: ASSIGN BAU.BA-VRFY-TME := #TIMN
                    bau_Ba_Rcrd_Last_Updt_Dte.setValue(pnd_Sys_Dte);                                                                                                      //Natural: ASSIGN BAU.BA-RCRD-LAST-UPDT-DTE := #SYS-DTE
                    bau_Ba_Rcrd_Last_Updt_Tme.setValue(pnd_Timn);                                                                                                         //Natural: ASSIGN BAU.BA-RCRD-LAST-UPDT-TME := #TIMN
                    bau_Ba_Rcrd_Last_Updt_Userid.setValue(Global.getPROGRAM());                                                                                           //Natural: ASSIGN BAU.BA-RCRD-LAST-UPDT-USERID := *PROGRAM
                    getReports().write(0, ReportOption.NOTITLE,"Activity updated to");                                                                                    //Natural: WRITE NOTITLE 'Activity updated to'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FBA"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FBA"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, ReportOption.NOTITLE,"Old","=",ba_Ba_Pin);                                                                                      //Natural: WRITE 'Old' '=' BA.BA-PIN
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FBA"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FBA"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, ReportOption.NOTITLE,"Old","=",ba_Ba_Rqst_Timestamp);                                                                           //Natural: WRITE 'Old' '=' BA.BA-RQST-TIMESTAMP
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FBA"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FBA"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, ReportOption.NOTITLE,"New","=",bau_Ba_Rqst_Cmpltd_Dte);                                                                         //Natural: WRITE 'New' '=' BAU.BA-RQST-CMPLTD-DTE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FBA"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FBA"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, ReportOption.NOTITLE,"New","=",bau_Ba_Lttr_Optn);                                                                               //Natural: WRITE 'New' '=' BAU.BA-LTTR-OPTN
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FBA"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FBA"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, ReportOption.NOTITLE,"New","=",bau_Ba_Vrfy_Ind);                                                                                //Natural: WRITE 'New' '=' BAU.BA-VRFY-IND
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FBA"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FBA"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, ReportOption.NOTITLE,"New","=",bau_Ba_Vrfy_Userid);                                                                             //Natural: WRITE 'New' '=' BAU.BA-VRFY-USERID
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FBA"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FBA"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, ReportOption.NOTITLE,"New","=",bau_Ba_Vrfy_Dte);                                                                                //Natural: WRITE 'New' '=' BAU.BA-VRFY-DTE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FBA"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FBA"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, ReportOption.NOTITLE,"New","=",bau_Ba_Vrfy_Tme);                                                                                //Natural: WRITE 'New' '=' BAU.BA-VRFY-TME
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FBA"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FBA"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, ReportOption.NOTITLE,"New","=",bau_Ba_Rcrd_Last_Updt_Dte);                                                                      //Natural: WRITE 'New' '=' BAU.BA-RCRD-LAST-UPDT-DTE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FBA"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FBA"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, ReportOption.NOTITLE,"New","=",bau_Ba_Rcrd_Last_Updt_Tme);                                                                      //Natural: WRITE 'New' '=' BAU.BA-RCRD-LAST-UPDT-TME
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FBA"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FBA"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, ReportOption.NOTITLE,"New","=",bau_Ba_Rcrd_Last_Updt_Userid,NEWLINE);                                                           //Natural: WRITE 'New' '=' BAU.BA-RCRD-LAST-UPDT-USERID /
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FBA"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FBA"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    vw_bau.updateDBRow("G5");                                                                                                                             //Natural: UPDATE ( G5. )
                }                                                                                                                                                         //Natural: END-IF
                //*  FBA.
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FBC"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FBC"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  FBC.
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  RESET-PIN-CONTRACT
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=132");
    }
}
