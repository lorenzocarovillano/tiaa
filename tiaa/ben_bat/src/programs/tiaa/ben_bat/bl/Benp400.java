/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:15:05 PM
**        * FROM NATURAL PROGRAM : Benp400
************************************************************
**        * FILE NAME            : Benp400.java
**        * CLASS NAME           : Benp400
**        * INSTANCE NAME        : Benp400
************************************************************
************************************************************************
* PROGRAM NAME : BENP400
* DESCRIPTION  : BENEFICIARY SYSTEM - PLAN ANALYZER EXTRACT
*                PIN WITH BENES='Y'; WITH DEFAULT='N'
* WRITTEN BY   : DENNIS DURAN
* DATE WRITTEN : OCT 28, 2011
************************************************************************
*    DATE     USERID                   DESCRIPTION
* MM/DD/YYYY  XXXXXXXX  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
* 04/01/2017  DURAND    PIN EXPANSION N7 TO N12
************************************************************************

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benp400 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_bc;
    private DbsField bc_Bc_Pin;
    private DbsField bc_Bc_Tiaa_Cntrct;
    private DbsField bc_Bc_Cref_Cntrct;
    private DbsField bc_Bc_Tiaa_Cref_Ind;
    private DbsField bc_Bc_Stat;
    private DbsField bc_Bc_Cntrct_Type;
    private DbsField bc_Bc_Rqst_Timestamp;
    private DbsField bc_Bc_Eff_Dte;
    private DbsField bc_Bc_Mos_Ind;
    private DbsField bc_Bc_Irvcbl_Ind;
    private DbsField bc_Bc_Pymnt_Chld_Dcsd_Ind;
    private DbsField bc_Bc_Exempt_Spouse_Rights;
    private DbsField bc_Bc_Spouse_Waived_Bnfts;
    private DbsField bc_Bc_Trust_Data_Fldr;
    private DbsField bc_Bc_Bene_Addr_Fldr;
    private DbsField bc_Bc_Co_Owner_Data_Fldr;
    private DbsField bc_Bc_Fldr_Min;
    private DbsField bc_Bc_Fldr_Srce_Id;
    private DbsField bc_Bc_Fldr_Log_Dte_Tme;
    private DbsField bc_Bc_Rcrd_Last_Updt_Dte;

    private DbsGroup bc__R_Field_1;
    private DbsField bc_Bc_Rcrd_Last_Updt_Yyyy;
    private DbsField bc_Bc_Rcrd_Last_Updt_Mm;
    private DbsField bc_Bc_Rcrd_Last_Updt_Dd;
    private DbsField bc_Bc_Rcrd_Last_Updt_Tme;
    private DbsField bc_Bc_Rcrd_Last_Updt_Userid;
    private DbsField bc_Bc_Last_Dsgntn_Srce;
    private DbsField bc_Bc_Last_Dsgntn_System;
    private DbsField bc_Bc_Last_Dsgntn_Userid;
    private DbsField bc_Bc_Last_Dsgntn_Dte;
    private DbsField bc_Bc_Last_Dsgntn_Tme;
    private DbsField bc_Bc_Last_Vrfy_Dte;
    private DbsField bc_Bc_Last_Vrfy_Tme;
    private DbsField bc_Bc_Last_Vrfy_Userid;
    private DbsField bc_Bc_Tiaa_Cref_Chng_Dte;
    private DbsField bc_Bc_Tiaa_Cref_Chng_Tme;
    private DbsField bc_Bc_Chng_Pwr_Atty;
    private DbsField bc_Bc_Mgrtn_Dsgntn_Grp_Nbr;
    private DbsField bc_Bc_Mgrtn_Cnfrm_Stmnt_Prod_Dte;
    private DbsField bc_Bc_Excptn_Unit;

    private DataAccessProgramView vw_bd;
    private DbsField bd_Bd_Pin;
    private DbsField bd_Bd_Tiaa_Cntrct;
    private DbsField bd_Bd_Cref_Cntrct;
    private DbsField bd_Bd_Tiaa_Cref_Ind;
    private DbsField bd_Bd_Stat;
    private DbsField bd_Bd_Seq_Nmbr;
    private DbsField bd_Bd_Rqst_Timestamp;
    private DbsField bd_Bd_Rcrd_Last_Updt_Dte;

    private DbsGroup bd__R_Field_2;
    private DbsField bd_Bd_Rcrd_Last_Updt_Dte_Yyyy;
    private DbsField bd_Bd_Rcrd_Last_Updt_Dte_Mm;
    private DbsField bd_Bd_Rcrd_Last_Updt_Dte_Dd;
    private DbsField bd_Bd_Rcrd_Last_Updt_Tme;
    private DbsField bd_Bd_Rcrd_Last_Updt_Userid;
    private DbsField bd_Bd_Bene_Type;
    private DbsField bd_Bd_Rltn_Cd;

    private DbsGroup bd__R_Field_3;
    private DbsField bd_Bd_Rltn_Cd_N2;
    private DbsField bd_Bd_Bene_Name1;
    private DbsField bd_Bd_Bene_Name2;
    private DbsField bd_Bd_Rltn_Free_Txt;
    private DbsField bd_Bd_Dte_Birth_Trust;
    private DbsField bd_Bd_Ss_Cd;
    private DbsField bd_Bd_Ss_Nmbr;
    private DbsField bd_Bd_Perc_Share_Ind;
    private DbsField bd_Bd_Share_Perc;

    private DbsGroup bd__R_Field_4;
    private DbsField bd_Bd_Share_Whole;
    private DbsField bd_Bd_Share_Fraction;
    private DbsField bd_Bd_Share_Ntor;
    private DbsField bd_Bd_Share_Dtor;
    private DbsField bd_Bd_Irvcbl_Ind;
    private DbsField bd_Bd_Stlmnt_Rstrctn;
    private DbsField bd_Bd_Spcl_Txt1;
    private DbsField bd_Bd_Spcl_Txt2;
    private DbsField bd_Bd_Spcl_Txt3;
    private DbsField bd_Bd_Dflt_Estate;
    private DbsField bd_Bd_Mdo_Calc_Bene;
    private DbsField bd_Bd_Addr1;
    private DbsField bd_Bd_Addr2;
    private DbsField bd_Bd_Addr3_City;
    private DbsField bd_Bd_State;
    private DbsField bd_Bd_Zip;
    private DbsField bd_Bd_Phone;
    private DbsField bd_Bd_Gender;
    private DbsField bd_Bd_Country;

    private DbsGroup bd__R_Field_5;
    private DbsField bd_Bd_Iso_Cd;
    private DbsField bd_Bd_Non_Iso_Cd;

    private DataAccessProgramView vw_bm;
    private DbsField bm_Bm_Pin;
    private DbsField bm_Bm_Tiaa_Cntrct;
    private DbsField bm_Bm_Cref_Cntrct;
    private DbsField bm_Bm_Tiaa_Cref_Ind;
    private DbsField bm_Bm_Stat;
    private DbsField bm_Bm_Rqst_Timestamp;
    private DbsField bm_Bm_Rcrd_Last_Updt_Dte;
    private DbsField bm_Bm_Rcrd_Last_Updt_Tme;
    private DbsField bm_Bm_Rcrd_Last_Updt_Userid;
    private DbsField bm_Count_Castbm_Mos_Txt_Group;

    private DbsGroup bm_Bm_Mos_Txt_Group;
    private DbsField bm_Bm_Mos_Txt;
    private DbsField bm_Bm_Stat_Pin_Cntrct_Ind_Key;
    private DbsField bm_Bm_Pin_Tiaa_Ind_Cntrct_Key;
    private DbsField bm_Bm_Pin_Cref_Ind_Cntrct_Key;
    private DbsField bm_Bm_Pin_Cntrct_Ind_Key;
    private DbsField pnd_Bene_Key;

    private DbsGroup pnd_Bene_Key__R_Field_6;
    private DbsField pnd_Bene_Key_Pnd_Stat;
    private DbsField pnd_Bene_Key_Pnd_Bene_Key2;

    private DbsGroup pnd_Bene_Key__R_Field_7;
    private DbsField pnd_Bene_Key_Pnd_Pin;
    private DbsField pnd_Bene_Key_Pnd_Tiaa_Cntrct;
    private DbsField pnd_Bene_Key_Pnd_Tiaa_Cref_Ind;
    private DbsField pnd_Total_Bc;
    private DbsField pnd_Total_Bd;
    private DbsField pnd_Total_Bm;
    private DbsField pnd_Total_N;
    private DbsField pnd_Total_Y;
    private DbsField pnd_Total_Workfile;

    private DbsGroup pnd_Workfile;
    private DbsField pnd_Workfile_Pnd_Wk_Pin;
    private DbsField pnd_Workfile_Pnd_Fill1;
    private DbsField pnd_Workfile_Pnd_Wk_Cntrct;
    private DbsField pnd_Workfile_Pnd_Fill2;
    private DbsField pnd_Workfile_Pnd_Wk_Primary;
    private DbsField pnd_Workfile_Pnd_Fill3;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_bc = new DataAccessProgramView(new NameInfo("vw_bc", "BC"), "BENE_CONTRACT_12", "BENE_CONTRACT");
        bc_Bc_Pin = vw_bc.getRecord().newFieldInGroup("bc_Bc_Pin", "BC-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BC_PIN");
        bc_Bc_Pin.setDdmHeader("PIN");
        bc_Bc_Tiaa_Cntrct = vw_bc.getRecord().newFieldInGroup("bc_Bc_Tiaa_Cntrct", "BC-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BC_TIAA_CNTRCT");
        bc_Bc_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bc_Bc_Cref_Cntrct = vw_bc.getRecord().newFieldInGroup("bc_Bc_Cref_Cntrct", "BC-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BC_CREF_CNTRCT");
        bc_Bc_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bc_Bc_Tiaa_Cref_Ind = vw_bc.getRecord().newFieldInGroup("bc_Bc_Tiaa_Cref_Ind", "BC-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_TIAA_CREF_IND");
        bc_Bc_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bc_Bc_Stat = vw_bc.getRecord().newFieldInGroup("bc_Bc_Stat", "BC-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_STAT");
        bc_Bc_Stat.setDdmHeader("STATUS");
        bc_Bc_Cntrct_Type = vw_bc.getRecord().newFieldInGroup("bc_Bc_Cntrct_Type", "BC-CNTRCT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_CNTRCT_TYPE");
        bc_Bc_Cntrct_Type.setDdmHeader("CNTRCT/TYPE");
        bc_Bc_Rqst_Timestamp = vw_bc.getRecord().newFieldInGroup("bc_Bc_Rqst_Timestamp", "BC-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BC_RQST_TIMESTAMP");
        bc_Bc_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        bc_Bc_Eff_Dte = vw_bc.getRecord().newFieldInGroup("bc_Bc_Eff_Dte", "BC-EFF-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "BC_EFF_DTE");
        bc_Bc_Eff_Dte.setDdmHeader("EFFECTIVE/DATE");
        bc_Bc_Mos_Ind = vw_bc.getRecord().newFieldInGroup("bc_Bc_Mos_Ind", "BC-MOS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_MOS_IND");
        bc_Bc_Mos_Ind.setDdmHeader("MOS/IND");
        bc_Bc_Irvcbl_Ind = vw_bc.getRecord().newFieldInGroup("bc_Bc_Irvcbl_Ind", "BC-IRVCBL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_IRVCBL_IND");
        bc_Bc_Irvcbl_Ind.setDdmHeader("IRVCBL/IND");
        bc_Bc_Pymnt_Chld_Dcsd_Ind = vw_bc.getRecord().newFieldInGroup("bc_Bc_Pymnt_Chld_Dcsd_Ind", "BC-PYMNT-CHLD-DCSD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_PYMNT_CHLD_DCSD_IND");
        bc_Bc_Pymnt_Chld_Dcsd_Ind.setDdmHeader("PYMNT/CHILD/DCSD");
        bc_Bc_Exempt_Spouse_Rights = vw_bc.getRecord().newFieldInGroup("bc_Bc_Exempt_Spouse_Rights", "BC-EXEMPT-SPOUSE-RIGHTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_EXEMPT_SPOUSE_RIGHTS");
        bc_Bc_Exempt_Spouse_Rights.setDdmHeader("EXEMPT/SPOUSAL/RIGHTS");
        bc_Bc_Spouse_Waived_Bnfts = vw_bc.getRecord().newFieldInGroup("bc_Bc_Spouse_Waived_Bnfts", "BC-SPOUSE-WAIVED-BNFTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_SPOUSE_WAIVED_BNFTS");
        bc_Bc_Spouse_Waived_Bnfts.setDdmHeader("SPOUSE/WAIVED/BENEFITS");
        bc_Bc_Trust_Data_Fldr = vw_bc.getRecord().newFieldInGroup("bc_Bc_Trust_Data_Fldr", "BC-TRUST-DATA-FLDR", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_TRUST_DATA_FLDR");
        bc_Bc_Trust_Data_Fldr.setDdmHeader("TRUST/DATA/IN FLDR");
        bc_Bc_Bene_Addr_Fldr = vw_bc.getRecord().newFieldInGroup("bc_Bc_Bene_Addr_Fldr", "BC-BENE-ADDR-FLDR", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_BENE_ADDR_FLDR");
        bc_Bc_Bene_Addr_Fldr.setDdmHeader("BENE/ADDRESS/IN FLDR");
        bc_Bc_Co_Owner_Data_Fldr = vw_bc.getRecord().newFieldInGroup("bc_Bc_Co_Owner_Data_Fldr", "BC-CO-OWNER-DATA-FLDR", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_CO_OWNER_DATA_FLDR");
        bc_Bc_Co_Owner_Data_Fldr.setDdmHeader("CO-OWNER/DATA FLDR");
        bc_Bc_Fldr_Min = vw_bc.getRecord().newFieldInGroup("bc_Bc_Fldr_Min", "BC-FLDR-MIN", FieldType.STRING, 11, RepeatingFieldStrategy.None, "BC_FLDR_MIN");
        bc_Bc_Fldr_Min.setDdmHeader("FOLDER/MIN");
        bc_Bc_Fldr_Srce_Id = vw_bc.getRecord().newFieldInGroup("bc_Bc_Fldr_Srce_Id", "BC-FLDR-SRCE-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "BC_FLDR_SRCE_ID");
        bc_Bc_Fldr_Srce_Id.setDdmHeader("FOLDER/SOURCE/ID");
        bc_Bc_Fldr_Log_Dte_Tme = vw_bc.getRecord().newFieldInGroup("bc_Bc_Fldr_Log_Dte_Tme", "BC-FLDR-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BC_FLDR_LOG_DTE_TME");
        bc_Bc_Fldr_Log_Dte_Tme.setDdmHeader("FOLDER/LOG DATE/AND TIME");
        bc_Bc_Rcrd_Last_Updt_Dte = vw_bc.getRecord().newFieldInGroup("bc_Bc_Rcrd_Last_Updt_Dte", "BC-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_RCRD_LAST_UPDT_DTE");
        bc_Bc_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");

        bc__R_Field_1 = vw_bc.getRecord().newGroupInGroup("bc__R_Field_1", "REDEFINE", bc_Bc_Rcrd_Last_Updt_Dte);
        bc_Bc_Rcrd_Last_Updt_Yyyy = bc__R_Field_1.newFieldInGroup("bc_Bc_Rcrd_Last_Updt_Yyyy", "BC-RCRD-LAST-UPDT-YYYY", FieldType.NUMERIC, 4);
        bc_Bc_Rcrd_Last_Updt_Mm = bc__R_Field_1.newFieldInGroup("bc_Bc_Rcrd_Last_Updt_Mm", "BC-RCRD-LAST-UPDT-MM", FieldType.NUMERIC, 2);
        bc_Bc_Rcrd_Last_Updt_Dd = bc__R_Field_1.newFieldInGroup("bc_Bc_Rcrd_Last_Updt_Dd", "BC-RCRD-LAST-UPDT-DD", FieldType.NUMERIC, 2);
        bc_Bc_Rcrd_Last_Updt_Tme = vw_bc.getRecord().newFieldInGroup("bc_Bc_Rcrd_Last_Updt_Tme", "BC-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_RCRD_LAST_UPDT_TME");
        bc_Bc_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bc_Bc_Rcrd_Last_Updt_Userid = vw_bc.getRecord().newFieldInGroup("bc_Bc_Rcrd_Last_Updt_Userid", "BC-RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "BC_RCRD_LAST_UPDT_USERID");
        bc_Bc_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        bc_Bc_Last_Dsgntn_Srce = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Dsgntn_Srce", "BC-LAST-DSGNTN-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_SRCE");
        bc_Bc_Last_Dsgntn_Srce.setDdmHeader("LAST/DSGNTN/SOURCE");
        bc_Bc_Last_Dsgntn_System = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Dsgntn_System", "BC-LAST-DSGNTN-SYSTEM", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_SYSTEM");
        bc_Bc_Last_Dsgntn_System.setDdmHeader("LAST/DSGNTN/SYSTEM");
        bc_Bc_Last_Dsgntn_Userid = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Dsgntn_Userid", "BC-LAST-DSGNTN-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_USERID");
        bc_Bc_Last_Dsgntn_Userid.setDdmHeader("LAST/DSGNTN/USERID");
        bc_Bc_Last_Dsgntn_Dte = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Dsgntn_Dte", "BC-LAST-DSGNTN-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_DTE");
        bc_Bc_Last_Dsgntn_Dte.setDdmHeader("LAST/DSGNTN/DATE");
        bc_Bc_Last_Dsgntn_Tme = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Dsgntn_Tme", "BC-LAST-DSGNTN-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_TME");
        bc_Bc_Last_Dsgntn_Tme.setDdmHeader("LAST/DSGNTN/TIME");
        bc_Bc_Last_Vrfy_Dte = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Vrfy_Dte", "BC-LAST-VRFY-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_VRFY_DTE");
        bc_Bc_Last_Vrfy_Dte.setDdmHeader("LAST/VRFY/DATE");
        bc_Bc_Last_Vrfy_Tme = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Vrfy_Tme", "BC-LAST-VRFY-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_LAST_VRFY_TME");
        bc_Bc_Last_Vrfy_Tme.setDdmHeader("LAST/VRFY/TIME");
        bc_Bc_Last_Vrfy_Userid = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Vrfy_Userid", "BC-LAST-VRFY-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_VRFY_USERID");
        bc_Bc_Last_Vrfy_Userid.setDdmHeader("LAST/VRFY/USERID");
        bc_Bc_Tiaa_Cref_Chng_Dte = vw_bc.getRecord().newFieldInGroup("bc_Bc_Tiaa_Cref_Chng_Dte", "BC-TIAA-CREF-CHNG-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_TIAA_CREF_CHNG_DTE");
        bc_Bc_Tiaa_Cref_Chng_Dte.setDdmHeader("TIAA CREF/CHANGE/DATE");
        bc_Bc_Tiaa_Cref_Chng_Tme = vw_bc.getRecord().newFieldInGroup("bc_Bc_Tiaa_Cref_Chng_Tme", "BC-TIAA-CREF-CHNG-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BC_TIAA_CREF_CHNG_TME");
        bc_Bc_Tiaa_Cref_Chng_Tme.setDdmHeader("TIAA CREF/CHANGE/TIME");
        bc_Bc_Chng_Pwr_Atty = vw_bc.getRecord().newFieldInGroup("bc_Bc_Chng_Pwr_Atty", "BC-CHNG-PWR-ATTY", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_CHNG_PWR_ATTY");
        bc_Bc_Chng_Pwr_Atty.setDdmHeader("CHANGE/POWER/ATTNY");
        bc_Bc_Mgrtn_Dsgntn_Grp_Nbr = vw_bc.getRecord().newFieldInGroup("bc_Bc_Mgrtn_Dsgntn_Grp_Nbr", "BC-MGRTN-DSGNTN-GRP-NBR", FieldType.NUMERIC, 3, 
            RepeatingFieldStrategy.None, "BC_MGRTN_DSGNTN_GRP_NBR");
        bc_Bc_Mgrtn_Dsgntn_Grp_Nbr.setDdmHeader("MGRTN/DSGNTN/GRP NBR");
        bc_Bc_Mgrtn_Cnfrm_Stmnt_Prod_Dte = vw_bc.getRecord().newFieldInGroup("bc_Bc_Mgrtn_Cnfrm_Stmnt_Prod_Dte", "BC-MGRTN-CNFRM-STMNT-PROD-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "BC_MGRTN_CNFRM_STMNT_PROD_DTE");
        bc_Bc_Mgrtn_Cnfrm_Stmnt_Prod_Dte.setDdmHeader("MGRTN CONFIRM/STATEMENT/PROD DATE");
        bc_Bc_Excptn_Unit = vw_bc.getRecord().newFieldInGroup("bc_Bc_Excptn_Unit", "BC-EXCPTN-UNIT", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_EXCPTN_UNIT");
        bc_Bc_Excptn_Unit.setDdmHeader("MGRTN/EXCPTN/UNIT");
        registerRecord(vw_bc);

        vw_bd = new DataAccessProgramView(new NameInfo("vw_bd", "BD"), "BENE_DESIGNATION_12", "BENE_DESIGNATION");
        bd_Bd_Pin = vw_bd.getRecord().newFieldInGroup("bd_Bd_Pin", "BD-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BD_PIN");
        bd_Bd_Pin.setDdmHeader("PIN");
        bd_Bd_Tiaa_Cntrct = vw_bd.getRecord().newFieldInGroup("bd_Bd_Tiaa_Cntrct", "BD-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BD_TIAA_CNTRCT");
        bd_Bd_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bd_Bd_Cref_Cntrct = vw_bd.getRecord().newFieldInGroup("bd_Bd_Cref_Cntrct", "BD-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BD_CREF_CNTRCT");
        bd_Bd_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bd_Bd_Tiaa_Cref_Ind = vw_bd.getRecord().newFieldInGroup("bd_Bd_Tiaa_Cref_Ind", "BD-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_TIAA_CREF_IND");
        bd_Bd_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bd_Bd_Stat = vw_bd.getRecord().newFieldInGroup("bd_Bd_Stat", "BD-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_STAT");
        bd_Bd_Stat.setDdmHeader("STAT");
        bd_Bd_Seq_Nmbr = vw_bd.getRecord().newFieldInGroup("bd_Bd_Seq_Nmbr", "BD-SEQ-NMBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "BD_SEQ_NMBR");
        bd_Bd_Seq_Nmbr.setDdmHeader(" SEQ/NMBR");
        bd_Bd_Rqst_Timestamp = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rqst_Timestamp", "BD-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BD_RQST_TIMESTAMP");
        bd_Bd_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        bd_Bd_Rcrd_Last_Updt_Dte = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rcrd_Last_Updt_Dte", "BD-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BD_RCRD_LAST_UPDT_DTE");
        bd_Bd_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");

        bd__R_Field_2 = vw_bd.getRecord().newGroupInGroup("bd__R_Field_2", "REDEFINE", bd_Bd_Rcrd_Last_Updt_Dte);
        bd_Bd_Rcrd_Last_Updt_Dte_Yyyy = bd__R_Field_2.newFieldInGroup("bd_Bd_Rcrd_Last_Updt_Dte_Yyyy", "BD-RCRD-LAST-UPDT-DTE-YYYY", FieldType.STRING, 
            4);
        bd_Bd_Rcrd_Last_Updt_Dte_Mm = bd__R_Field_2.newFieldInGroup("bd_Bd_Rcrd_Last_Updt_Dte_Mm", "BD-RCRD-LAST-UPDT-DTE-MM", FieldType.STRING, 2);
        bd_Bd_Rcrd_Last_Updt_Dte_Dd = bd__R_Field_2.newFieldInGroup("bd_Bd_Rcrd_Last_Updt_Dte_Dd", "BD-RCRD-LAST-UPDT-DTE-DD", FieldType.STRING, 2);
        bd_Bd_Rcrd_Last_Updt_Tme = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rcrd_Last_Updt_Tme", "BD-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BD_RCRD_LAST_UPDT_TME");
        bd_Bd_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bd_Bd_Rcrd_Last_Updt_Userid = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rcrd_Last_Updt_Userid", "BD-RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "BD_RCRD_LAST_UPDT_USERID");
        bd_Bd_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        bd_Bd_Bene_Type = vw_bd.getRecord().newFieldInGroup("bd_Bd_Bene_Type", "BD-BENE-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_BENE_TYPE");
        bd_Bd_Bene_Type.setDdmHeader("BENE/TYPE");
        bd_Bd_Rltn_Cd = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rltn_Cd", "BD-RLTN-CD", FieldType.STRING, 2, RepeatingFieldStrategy.None, "BD_RLTN_CD");
        bd_Bd_Rltn_Cd.setDdmHeader("RELATION/CODE");

        bd__R_Field_3 = vw_bd.getRecord().newGroupInGroup("bd__R_Field_3", "REDEFINE", bd_Bd_Rltn_Cd);
        bd_Bd_Rltn_Cd_N2 = bd__R_Field_3.newFieldInGroup("bd_Bd_Rltn_Cd_N2", "BD-RLTN-CD-N2", FieldType.NUMERIC, 2);
        bd_Bd_Bene_Name1 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Bene_Name1", "BD-BENE-NAME1", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BD_BENE_NAME1");
        bd_Bd_Bene_Name1.setDdmHeader("BENEFICIARY/NAME 1");
        bd_Bd_Bene_Name2 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Bene_Name2", "BD-BENE-NAME2", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BD_BENE_NAME2");
        bd_Bd_Bene_Name2.setDdmHeader("BENEFICIARY/NAME 2");
        bd_Bd_Rltn_Free_Txt = vw_bd.getRecord().newFieldInGroup("bd_Bd_Rltn_Free_Txt", "BD-RLTN-FREE-TXT", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BD_RLTN_FREE_TXT");
        bd_Bd_Rltn_Free_Txt.setDdmHeader("OTHER/RELATION");
        bd_Bd_Dte_Birth_Trust = vw_bd.getRecord().newFieldInGroup("bd_Bd_Dte_Birth_Trust", "BD-DTE-BIRTH-TRUST", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BD_DTE_BIRTH_TRUST");
        bd_Bd_Dte_Birth_Trust.setDdmHeader("DOB/DOT");
        bd_Bd_Ss_Cd = vw_bd.getRecord().newFieldInGroup("bd_Bd_Ss_Cd", "BD-SS-CD", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_SS_CD");
        bd_Bd_Ss_Cd.setDdmHeader("SS/CODE");
        bd_Bd_Ss_Nmbr = vw_bd.getRecord().newFieldInGroup("bd_Bd_Ss_Nmbr", "BD-SS-NMBR", FieldType.STRING, 9, RepeatingFieldStrategy.None, "BD_SS_NMBR");
        bd_Bd_Ss_Nmbr.setDdmHeader("SS/NUMBER");
        bd_Bd_Perc_Share_Ind = vw_bd.getRecord().newFieldInGroup("bd_Bd_Perc_Share_Ind", "BD-PERC-SHARE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_PERC_SHARE_IND");
        bd_Bd_Perc_Share_Ind.setDdmHeader("PERCNT/SHARE/IND");
        bd_Bd_Share_Perc = vw_bd.getRecord().newFieldInGroup("bd_Bd_Share_Perc", "BD-SHARE-PERC", FieldType.NUMERIC, 5, 2, RepeatingFieldStrategy.None, 
            "BD_SHARE_PERC");
        bd_Bd_Share_Perc.setDdmHeader("SHARE/PERCENT");

        bd__R_Field_4 = vw_bd.getRecord().newGroupInGroup("bd__R_Field_4", "REDEFINE", bd_Bd_Share_Perc);
        bd_Bd_Share_Whole = bd__R_Field_4.newFieldInGroup("bd_Bd_Share_Whole", "BD-SHARE-WHOLE", FieldType.NUMERIC, 3);
        bd_Bd_Share_Fraction = bd__R_Field_4.newFieldInGroup("bd_Bd_Share_Fraction", "BD-SHARE-FRACTION", FieldType.NUMERIC, 2);
        bd_Bd_Share_Ntor = vw_bd.getRecord().newFieldInGroup("bd_Bd_Share_Ntor", "BD-SHARE-NTOR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "BD_SHARE_NTOR");
        bd_Bd_Share_Ntor.setDdmHeader("SHARE/NUMERATOR");
        bd_Bd_Share_Dtor = vw_bd.getRecord().newFieldInGroup("bd_Bd_Share_Dtor", "BD-SHARE-DTOR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "BD_SHARE_DTOR");
        bd_Bd_Share_Dtor.setDdmHeader("SHARE/DENOMINATOR");
        bd_Bd_Irvcbl_Ind = vw_bd.getRecord().newFieldInGroup("bd_Bd_Irvcbl_Ind", "BD-IRVCBL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_IRVCBL_IND");
        bd_Bd_Irvcbl_Ind.setDdmHeader("IRVCBL/IND");
        bd_Bd_Stlmnt_Rstrctn = vw_bd.getRecord().newFieldInGroup("bd_Bd_Stlmnt_Rstrctn", "BD-STLMNT-RSTRCTN", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_STLMNT_RSTRCTN");
        bd_Bd_Stlmnt_Rstrctn.setDdmHeader("SETTLEMENT/RESTRICTION");
        bd_Bd_Spcl_Txt1 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Spcl_Txt1", "BD-SPCL-TXT1", FieldType.STRING, 72, RepeatingFieldStrategy.None, "BD_SPCL_TXT1");
        bd_Bd_Spcl_Txt1.setDdmHeader("SPECIAL TEXT 1");
        bd_Bd_Spcl_Txt2 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Spcl_Txt2", "BD-SPCL-TXT2", FieldType.STRING, 72, RepeatingFieldStrategy.None, "BD_SPCL_TXT2");
        bd_Bd_Spcl_Txt2.setDdmHeader("SPECIAL TEXT 2");
        bd_Bd_Spcl_Txt3 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Spcl_Txt3", "BD-SPCL-TXT3", FieldType.STRING, 72, RepeatingFieldStrategy.None, "BD_SPCL_TXT3");
        bd_Bd_Spcl_Txt3.setDdmHeader("SPECIAL TEXT 3");
        bd_Bd_Dflt_Estate = vw_bd.getRecord().newFieldInGroup("bd_Bd_Dflt_Estate", "BD-DFLT-ESTATE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_DFLT_ESTATE");
        bd_Bd_Dflt_Estate.setDdmHeader("DEFAULT/ESTATE");
        bd_Bd_Mdo_Calc_Bene = vw_bd.getRecord().newFieldInGroup("bd_Bd_Mdo_Calc_Bene", "BD-MDO-CALC-BENE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_MDO_CALC_BENE");
        bd_Bd_Mdo_Calc_Bene.setDdmHeader("MDO/CALC/BENE");
        bd_Bd_Addr1 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Addr1", "BD-ADDR1", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BD_ADDR1");
        bd_Bd_Addr2 = vw_bd.getRecord().newFieldInGroup("bd_Bd_Addr2", "BD-ADDR2", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BD_ADDR2");
        bd_Bd_Addr3_City = vw_bd.getRecord().newFieldInGroup("bd_Bd_Addr3_City", "BD-ADDR3-CITY", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BD_ADDR3_CITY");
        bd_Bd_State = vw_bd.getRecord().newFieldInGroup("bd_Bd_State", "BD-STATE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "BD_STATE");
        bd_Bd_Zip = vw_bd.getRecord().newFieldInGroup("bd_Bd_Zip", "BD-ZIP", FieldType.STRING, 10, RepeatingFieldStrategy.None, "BD_ZIP");
        bd_Bd_Phone = vw_bd.getRecord().newFieldInGroup("bd_Bd_Phone", "BD-PHONE", FieldType.STRING, 20, RepeatingFieldStrategy.None, "BD_PHONE");
        bd_Bd_Gender = vw_bd.getRecord().newFieldInGroup("bd_Bd_Gender", "BD-GENDER", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_GENDER");
        bd_Bd_Country = vw_bd.getRecord().newFieldInGroup("bd_Bd_Country", "BD-COUNTRY", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BD_COUNTRY");

        bd__R_Field_5 = vw_bd.getRecord().newGroupInGroup("bd__R_Field_5", "REDEFINE", bd_Bd_Country);
        bd_Bd_Iso_Cd = bd__R_Field_5.newFieldInGroup("bd_Bd_Iso_Cd", "BD-ISO-CD", FieldType.STRING, 2);
        bd_Bd_Non_Iso_Cd = bd__R_Field_5.newFieldInGroup("bd_Bd_Non_Iso_Cd", "BD-NON-ISO-CD", FieldType.STRING, 33);
        registerRecord(vw_bd);

        vw_bm = new DataAccessProgramView(new NameInfo("vw_bm", "BM"), "BENE_MOS_12", "BENE_MOS", DdmPeriodicGroups.getInstance().getGroups("BENE_MOS_12"));
        bm_Bm_Pin = vw_bm.getRecord().newFieldInGroup("bm_Bm_Pin", "BM-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BM_PIN");
        bm_Bm_Pin.setDdmHeader("PIN");
        bm_Bm_Tiaa_Cntrct = vw_bm.getRecord().newFieldInGroup("bm_Bm_Tiaa_Cntrct", "BM-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BM_TIAA_CNTRCT");
        bm_Bm_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bm_Bm_Cref_Cntrct = vw_bm.getRecord().newFieldInGroup("bm_Bm_Cref_Cntrct", "BM-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BM_CREF_CNTRCT");
        bm_Bm_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bm_Bm_Tiaa_Cref_Ind = vw_bm.getRecord().newFieldInGroup("bm_Bm_Tiaa_Cref_Ind", "BM-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BM_TIAA_CREF_IND");
        bm_Bm_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bm_Bm_Stat = vw_bm.getRecord().newFieldInGroup("bm_Bm_Stat", "BM-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BM_STAT");
        bm_Bm_Stat.setDdmHeader("STAT");
        bm_Bm_Rqst_Timestamp = vw_bm.getRecord().newFieldInGroup("bm_Bm_Rqst_Timestamp", "BM-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BM_RQST_TIMESTAMP");
        bm_Bm_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        bm_Bm_Rcrd_Last_Updt_Dte = vw_bm.getRecord().newFieldInGroup("bm_Bm_Rcrd_Last_Updt_Dte", "BM-RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BM_RCRD_LAST_UPDT_DTE");
        bm_Bm_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bm_Bm_Rcrd_Last_Updt_Tme = vw_bm.getRecord().newFieldInGroup("bm_Bm_Rcrd_Last_Updt_Tme", "BM-RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "BM_RCRD_LAST_UPDT_TME");
        bm_Bm_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bm_Bm_Rcrd_Last_Updt_Userid = vw_bm.getRecord().newFieldInGroup("bm_Bm_Rcrd_Last_Updt_Userid", "BM-RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "BM_RCRD_LAST_UPDT_USERID");
        bm_Bm_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        bm_Count_Castbm_Mos_Txt_Group = vw_bm.getRecord().newFieldInGroup("bm_Count_Castbm_Mos_Txt_Group", "C*BM-MOS-TXT-GROUP", RepeatingFieldStrategy.CAsteriskVariable, 
            "BENE_MOS_BM_MOS_TXT_GROUP");

        bm_Bm_Mos_Txt_Group = vw_bm.getRecord().newGroupInGroup("bm_Bm_Mos_Txt_Group", "BM-MOS-TXT-GROUP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "BENE_MOS_BM_MOS_TXT_GROUP");
        bm_Bm_Mos_Txt = bm_Bm_Mos_Txt_Group.newFieldArrayInGroup("bm_Bm_Mos_Txt", "BM-MOS-TXT", FieldType.STRING, 72, new DbsArrayController(1, 60) , 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "BM_MOS_TXT", "BENE_MOS_BM_MOS_TXT_GROUP");
        bm_Bm_Mos_Txt.setDdmHeader("MOS TEXT");
        bm_Bm_Stat_Pin_Cntrct_Ind_Key = vw_bm.getRecord().newFieldInGroup("bm_Bm_Stat_Pin_Cntrct_Ind_Key", "BM-STAT-PIN-CNTRCT-IND-KEY", FieldType.STRING, 
            24, RepeatingFieldStrategy.None, "BM_STAT_PIN_CNTRCT_IND_KEY");
        bm_Bm_Stat_Pin_Cntrct_Ind_Key.setSuperDescriptor(true);
        bm_Bm_Pin_Tiaa_Ind_Cntrct_Key = vw_bm.getRecord().newFieldInGroup("bm_Bm_Pin_Tiaa_Ind_Cntrct_Key", "BM-PIN-TIAA-IND-CNTRCT-KEY", FieldType.STRING, 
            23, RepeatingFieldStrategy.None, "BM_PIN_TIAA_IND_CNTRCT_KEY");
        bm_Bm_Pin_Tiaa_Ind_Cntrct_Key.setSuperDescriptor(true);
        bm_Bm_Pin_Cref_Ind_Cntrct_Key = vw_bm.getRecord().newFieldInGroup("bm_Bm_Pin_Cref_Ind_Cntrct_Key", "BM-PIN-CREF-IND-CNTRCT-KEY", FieldType.STRING, 
            23, RepeatingFieldStrategy.None, "BM_PIN_CREF_IND_CNTRCT_KEY");
        bm_Bm_Pin_Cref_Ind_Cntrct_Key.setSuperDescriptor(true);
        bm_Bm_Pin_Cntrct_Ind_Key = vw_bm.getRecord().newFieldInGroup("bm_Bm_Pin_Cntrct_Ind_Key", "BM-PIN-CNTRCT-IND-KEY", FieldType.STRING, 23, RepeatingFieldStrategy.None, 
            "BM_PIN_CNTRCT_IND_KEY");
        bm_Bm_Pin_Cntrct_Ind_Key.setDdmHeader("BM-PIN-CNTRCT-IND-KEY");
        bm_Bm_Pin_Cntrct_Ind_Key.setSuperDescriptor(true);
        registerRecord(vw_bm);

        pnd_Bene_Key = localVariables.newFieldInRecord("pnd_Bene_Key", "#BENE-KEY", FieldType.STRING, 24);

        pnd_Bene_Key__R_Field_6 = localVariables.newGroupInRecord("pnd_Bene_Key__R_Field_6", "REDEFINE", pnd_Bene_Key);
        pnd_Bene_Key_Pnd_Stat = pnd_Bene_Key__R_Field_6.newFieldInGroup("pnd_Bene_Key_Pnd_Stat", "#STAT", FieldType.STRING, 1);
        pnd_Bene_Key_Pnd_Bene_Key2 = pnd_Bene_Key__R_Field_6.newFieldInGroup("pnd_Bene_Key_Pnd_Bene_Key2", "#BENE-KEY2", FieldType.STRING, 23);

        pnd_Bene_Key__R_Field_7 = pnd_Bene_Key__R_Field_6.newGroupInGroup("pnd_Bene_Key__R_Field_7", "REDEFINE", pnd_Bene_Key_Pnd_Bene_Key2);
        pnd_Bene_Key_Pnd_Pin = pnd_Bene_Key__R_Field_7.newFieldInGroup("pnd_Bene_Key_Pnd_Pin", "#PIN", FieldType.NUMERIC, 12);
        pnd_Bene_Key_Pnd_Tiaa_Cntrct = pnd_Bene_Key__R_Field_7.newFieldInGroup("pnd_Bene_Key_Pnd_Tiaa_Cntrct", "#TIAA-CNTRCT", FieldType.STRING, 10);
        pnd_Bene_Key_Pnd_Tiaa_Cref_Ind = pnd_Bene_Key__R_Field_7.newFieldInGroup("pnd_Bene_Key_Pnd_Tiaa_Cref_Ind", "#TIAA-CREF-IND", FieldType.STRING, 
            1);
        pnd_Total_Bc = localVariables.newFieldInRecord("pnd_Total_Bc", "#TOTAL-BC", FieldType.PACKED_DECIMAL, 10);
        pnd_Total_Bd = localVariables.newFieldInRecord("pnd_Total_Bd", "#TOTAL-BD", FieldType.PACKED_DECIMAL, 10);
        pnd_Total_Bm = localVariables.newFieldInRecord("pnd_Total_Bm", "#TOTAL-BM", FieldType.PACKED_DECIMAL, 10);
        pnd_Total_N = localVariables.newFieldInRecord("pnd_Total_N", "#TOTAL-N", FieldType.PACKED_DECIMAL, 10);
        pnd_Total_Y = localVariables.newFieldInRecord("pnd_Total_Y", "#TOTAL-Y", FieldType.PACKED_DECIMAL, 10);
        pnd_Total_Workfile = localVariables.newFieldInRecord("pnd_Total_Workfile", "#TOTAL-WORKFILE", FieldType.PACKED_DECIMAL, 10);

        pnd_Workfile = localVariables.newGroupInRecord("pnd_Workfile", "#WORKFILE");
        pnd_Workfile_Pnd_Wk_Pin = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_Wk_Pin", "#WK-PIN", FieldType.NUMERIC, 12);
        pnd_Workfile_Pnd_Fill1 = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_Fill1", "#FILL1", FieldType.STRING, 1);
        pnd_Workfile_Pnd_Wk_Cntrct = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_Wk_Cntrct", "#WK-CNTRCT", FieldType.STRING, 10);
        pnd_Workfile_Pnd_Fill2 = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_Fill2", "#FILL2", FieldType.STRING, 1);
        pnd_Workfile_Pnd_Wk_Primary = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_Wk_Primary", "#WK-PRIMARY", FieldType.STRING, 1);
        pnd_Workfile_Pnd_Fill3 = pnd_Workfile.newFieldInGroup("pnd_Workfile_Pnd_Fill3", "#FILL3", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_bc.reset();
        vw_bd.reset();
        vw_bm.reset();

        localVariables.reset();
        pnd_Workfile_Pnd_Fill1.setInitialValue("|");
        pnd_Workfile_Pnd_Fill2.setInitialValue("|");
        pnd_Workfile_Pnd_Wk_Primary.setInitialValue("N");
        pnd_Workfile_Pnd_Fill3.setInitialValue("|");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Benp400() throws Exception
    {
        super("Benp400");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        getReports().write(0, "Start Time:",Global.getTIMX(), new ReportEditMask ("HH:II:SS"));                                                                           //Natural: FORMAT ( 0 ) ZP = ON SG = OFF PS = 60 LS = 250;//Natural: WRITE ( 0 ) 'Start Time:' *TIMX ( EM = HH:II:SS )
        if (Global.isEscape()) return;
        vw_bc.startDatabaseRead                                                                                                                                           //Natural: READ BC BY BC-STAT-PIN-CNTRCT-IND-KEY
        (
        "READ01",
        new Oc[] { new Oc("BC_STAT_PIN_CNTRCT_IND_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_bc.readNextRow("READ01")))
        {
            if (condition(bc_Bc_Stat.equals("R")))                                                                                                                        //Natural: IF BC-STAT = 'R'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(((bc_Bc_Stat.equals("C") || bc_Bc_Stat.equals("P")) && (bc_Bc_Cntrct_Type.equals("D") || bc_Bc_Cntrct_Type.equals("I"))))))                   //Natural: ACCEPT IF BC-STAT = 'C' OR = 'P' AND BC-CNTRCT-TYPE = 'D' OR = 'I'
            {
                continue;
            }
            pnd_Total_Bc.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #TOTAL-BC
            pnd_Workfile.resetInitial();                                                                                                                                  //Natural: RESET INITIAL #WORKFILE
            pnd_Bene_Key_Pnd_Stat.setValue(bc_Bc_Stat);                                                                                                                   //Natural: ASSIGN #STAT := BC-STAT
            pnd_Bene_Key_Pnd_Pin.setValue(bc_Bc_Pin);                                                                                                                     //Natural: ASSIGN #PIN := #WK-PIN := BC-PIN
            pnd_Workfile_Pnd_Wk_Pin.setValue(bc_Bc_Pin);
            pnd_Bene_Key_Pnd_Tiaa_Cntrct.setValue(bc_Bc_Tiaa_Cntrct);                                                                                                     //Natural: ASSIGN #TIAA-CNTRCT := #WK-CNTRCT := BC-TIAA-CNTRCT
            pnd_Workfile_Pnd_Wk_Cntrct.setValue(bc_Bc_Tiaa_Cntrct);
            pnd_Bene_Key_Pnd_Tiaa_Cref_Ind.setValue(bc_Bc_Tiaa_Cref_Ind);                                                                                                 //Natural: ASSIGN #TIAA-CREF-IND := BC-TIAA-CREF-IND
            if (condition(bc_Bc_Mos_Ind.equals("Y")))                                                                                                                     //Natural: IF BC-MOS-IND = 'Y'
            {
                vw_bm.startDatabaseFind                                                                                                                                   //Natural: FIND ( 1 ) BM WITH BM-STAT-PIN-CNTRCT-IND-KEY = #BENE-KEY
                (
                "FIND01",
                new Wc[] { new Wc("BM_STAT_PIN_CNTRCT_IND_KEY", "=", pnd_Bene_Key, WcType.WITH) },
                1
                );
                FIND01:
                while (condition(vw_bm.readNextRow("FIND01", true)))
                {
                    vw_bm.setIfNotFoundControlFlag(false);
                    if (condition(vw_bm.getAstCOUNTER().equals(0)))                                                                                                       //Natural: IF NO RECORDS FOUND
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-NOREC
                    if (condition(!(bm_Bm_Stat.equals("C") || bm_Bm_Stat.equals("P"))))                                                                                   //Natural: ACCEPT IF BM-STAT = 'C' OR = 'P'
                    {
                        continue;
                    }
                    pnd_Workfile_Pnd_Wk_Primary.setValue("Y");                                                                                                            //Natural: ASSIGN #WK-PRIMARY := 'Y'
                    pnd_Total_Bm.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #TOTAL-BM
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                vw_bd.startDatabaseFind                                                                                                                                   //Natural: FIND BD WITH BD-PIN-CNTRCT-IND-KEY = #BENE-KEY2
                (
                "FIND02",
                new Wc[] { new Wc("BD_PIN_CNTRCT_IND_KEY", "=", pnd_Bene_Key_Pnd_Bene_Key2, WcType.WITH) }
                );
                FIND02:
                while (condition(vw_bd.readNextRow("FIND02", true)))
                {
                    vw_bd.setIfNotFoundControlFlag(false);
                    if (condition(vw_bd.getAstCOUNTER().equals(0)))                                                                                                       //Natural: IF NO RECORDS FOUND
                    {
                        pnd_Workfile_Pnd_Wk_Primary.setValue("N");                                                                                                        //Natural: ASSIGN #WK-PRIMARY := 'N'
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-NOREC
                    DbsUtil.examine(new ExamineSource(bd_Bd_Bene_Name1), new ExamineTranslate(TranslateOption.Upper));                                                    //Natural: EXAMINE BD-BENE-NAME1 TRANSLATE INTO UPPER
                    DbsUtil.examine(new ExamineSource(bd_Bd_Bene_Name2), new ExamineTranslate(TranslateOption.Upper));                                                    //Natural: EXAMINE BD-BENE-NAME2 TRANSLATE INTO UPPER
                    if (condition(((bd_Bd_Bene_Type.equals("P") && ((bd_Bd_Share_Perc.equals(100) || (bd_Bd_Share_Ntor.equals(1) && bd_Bd_Share_Dtor.equals(1)))          //Natural: IF ( BD-BENE-TYPE = 'P' AND ( BD-SHARE-PERC = 100 OR ( BD-SHARE-NTOR = 1 AND BD-SHARE-DTOR = 1 ) OR ( BD-SHARE-NTOR = 100 AND BD-SHARE-DTOR = 100 ) ) ) AND ( BD-DFLT-ESTATE = 'Y' OR ( BD-BENE-NAME1 = 'ESTATE' AND BD-BENE-NAME2 = 'DEFAULT TO ESTATE' ) OR ( BD-BENE-NAME1 = MASK ( 'DEFAULT TO ' ) OR = MASK ( 'SUBJECT TO ' ) OR = 'PLEASE ADD A BENEFICIARY' ) )
                        || (bd_Bd_Share_Ntor.equals(100) && bd_Bd_Share_Dtor.equals(100)))) && ((bd_Bd_Dflt_Estate.equals("Y") || (bd_Bd_Bene_Name1.equals("ESTATE") 
                        && bd_Bd_Bene_Name2.equals("DEFAULT TO ESTATE"))) || ((DbsUtil.maskMatches(bd_Bd_Bene_Name1,"'DEFAULT TO '") || DbsUtil.maskMatches(bd_Bd_Bene_Name1,"'SUBJECT TO '")) 
                        || bd_Bd_Bene_Name1.equals("PLEASE ADD A BENEFICIARY"))))))
                    {
                        pnd_Workfile_Pnd_Wk_Primary.setValue("N");                                                                                                        //Natural: ASSIGN #WK-PRIMARY := 'N'
                        getReports().write(0, "BC",Global.getTIMX(), new ReportEditMask ("HH:II:SS"),bc_Bc_Pin,bc_Bc_Tiaa_Cntrct,bc_Bc_Stat,bc_Bc_Cntrct_Type,            //Natural: WRITE ( 0 ) 'BC' *TIMX ( EM = HH:II:SS ) BC-PIN BC-TIAA-CNTRCT BC-STAT BC-CNTRCT-TYPE #WK-PRIMARY BD-BENE-NAME1
                            pnd_Workfile_Pnd_Wk_Primary,bd_Bd_Bene_Name1);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Workfile_Pnd_Wk_Primary.setValue("Y");                                                                                                        //Natural: ASSIGN #WK-PRIMARY := 'Y'
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Total_Bd.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #TOTAL-BD
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pnd_Total_Workfile.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #TOTAL-WORKFILE
            if (condition(pnd_Workfile_Pnd_Wk_Primary.equals("Y")))                                                                                                       //Natural: IF #WK-PRIMARY = 'Y'
            {
                pnd_Total_Y.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #TOTAL-Y
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Total_N.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #TOTAL-N
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(1, false, pnd_Workfile);                                                                                                                 //Natural: WRITE WORK FILE 1 #WORKFILE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(0, "  End Time:",Global.getTIMX(), new ReportEditMask ("HH:II:SS"));                                                                           //Natural: WRITE ( 0 ) '  End Time:' *TIMX ( EM = HH:II:SS )
        if (Global.isEscape()) return;
        getReports().write(0, "Total BC  :",pnd_Total_Bc, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));                                                                          //Natural: WRITE ( 0 ) 'Total BC  :' #TOTAL-BC
        if (Global.isEscape()) return;
        getReports().write(0, "      BD  :",pnd_Total_Bd, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));                                                                          //Natural: WRITE ( 0 ) '      BD  :' #TOTAL-BD
        if (Global.isEscape()) return;
        getReports().write(0, "      BM  :",pnd_Total_Bm, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));                                                                          //Natural: WRITE ( 0 ) '      BM  :' #TOTAL-BM
        if (Global.isEscape()) return;
        getReports().write(0, "Total Workfile:",pnd_Total_Workfile, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));                                                                //Natural: WRITE ( 0 ) 'Total Workfile:' #TOTAL-WORKFILE
        if (Global.isEscape()) return;
        getReports().write(0, "Total Y       :",pnd_Total_Y, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));                                                                       //Natural: WRITE ( 0 ) 'Total Y       :' #TOTAL-Y
        if (Global.isEscape()) return;
        getReports().write(0, "Total N       :",pnd_Total_N, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));                                                                       //Natural: WRITE ( 0 ) 'Total N       :' #TOTAL-N
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "ZP=ON SG=OFF PS=60 LS=250");
    }
}
