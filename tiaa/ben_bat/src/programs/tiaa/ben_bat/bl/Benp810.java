/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:15:38 PM
**        * FROM NATURAL PROGRAM : Benp810
************************************************************
**        * FILE NAME            : Benp810.java
**        * CLASS NAME           : Benp810
**        * INSTANCE NAME        : Benp810
************************************************************
************************************************************************
* PROGRAM : BENP810
* FUNCTION: READ VUL BENEFICIARY INFO FEED FROM MCCAMISH AND LOAD
*           TO INTERFACE FILES THRU BENN970.
* CREATED : JULY 2006
************************************************************************
*    DATE      USERID                   DESCRIPTION
* MM/DD/YYYY  XXXXXXXX  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
* 10/18/2019  DURAND    REMOVE PSGM003 & REPLACE W/ MDMN211A
* 04/01/2017  DURAND    PIN EXPANSION N7 TO N12
* 08/18/2006 O SOTTO PRODUCTION FIX. SC 081806.
* 09/06/2006 O SOTTO PRODUCTION FIX - THE INDEX WITHIN THE 090606 START
*                    AND 090606 END TAGS WERE CHANGED FROM #A TO #B.
* 03/16/2010 DURAND   ACCEPT N (NEW) OR C (CHANGE) REGARDLESS IF RECORD
*                     DOES NOT EXIST. NEW FIELDS AT END OF FILE LAYOUT.
* 08/16/2011 DURAND   CHECK IF BENE IS MORE THAN 30
* 03/07/2013 ALAGANI  CREATED NEW LDA BENA971
* 09/2015   KCD       REMOVE ACCESS TO COR-XREF-PH,COR-XREF-CNTRCT
*                     TAKE INFO FROM PRO COBOL
************************************************************************
*

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benp810 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private PdaBena971 pdaBena971;
    private PdaMdma101 pdaMdma101;
    private PdaMdma211 pdaMdma211;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_bc;
    private DbsField bc_Bc_Pin;
    private DbsField bc_Bc_Tiaa_Cntrct;
    private DbsField bc_Bc_Tiaa_Cref_Ind;
    private DbsField bc_Bc_Stat;

    private DataAccessProgramView vw_bmi;
    private DbsField bmi_Intrfce_Stts;
    private DbsField bmi_Rcrd_Crtd_For_Bsnss_Dte;
    private DbsField bmi_Tiaa_Cref_Ind;
    private DbsField bmi_Pin_Tiaa_Cntrct;
    private DbsField bmi_Rcrd_Crtd_Dte;
    private DbsField bmi_Rcrd_Crtd_Tme;
    private DbsField bmi_Rcrd_Last_Updt_Dte;
    private DbsField bmi_Rcrd_Last_Updt_Tme;
    private DbsField bmi_Rcrd_Last_Updt_Userid;

    private DbsGroup bmi_Mos_Txt_Group;
    private DbsField bmi_Mos_Txt;

    private DataAccessProgramView vw_bci;
    private DbsField bci_Intrfce_Stts;
    private DbsField bci_Rcrd_Crtd_For_Bsnss_Dte;
    private DbsField bci_Rcrd_Crtd_Dte;
    private DbsField bci_Rcrd_Crtd_Tme;
    private DbsField bci_Intrfcng_Systm;
    private DbsField bci_Rqstng_Systm;
    private DbsField bci_Rcrd_Last_Updt_Dte;
    private DbsField bci_Rcrd_Last_Updt_Tme;
    private DbsField bci_Rcrd_Last_Updt_Userid;
    private DbsField bci_Last_Prcssd_Bsnss_Dte;
    private DbsField bci_Intrfce_Cmpltd_Dte;
    private DbsField bci_Intrfce_Cmpltd_Tme;
    private DbsField bci_Dflt_To_Estate_Ind;
    private DbsField bci_Illgble_Ind;
    private DbsField bci_More_Than_Five_Benes_Ind;
    private DbsField bci_Intrfce_Mgrtn_Ind;
    private DbsField bci_More_Than_Thirty_Benes_Ind;
    private DbsField bci_Pin_Tiaa_Cntrct;

    private DbsGroup bci_Error_Table;
    private DbsField bci_Error_Txt;
    private DbsField bci_Error_Cde;
    private DbsField bci_Error_Dte;
    private DbsField bci_Error_Tme;
    private DbsField bci_Error_Pgm;
    private DbsField bci_Fldr_Log_Dte_Tme;
    private DbsField bci_Last_Dsgntn_Srce;
    private DbsField bci_Last_Dsgntn_System;
    private DbsField bci_Last_Dsgntn_Userid;
    private DbsField bci_Last_Dsgntn_Dte;
    private DbsField bci_Last_Dsgntn_Tme;
    private DbsField bci_Tiaa_Cref_Chng_Dte;
    private DbsField bci_Tiaa_Cref_Chng_Tme;
    private DbsField bci_Chng_Pwr_Atty;
    private DbsField bci_Cref_Cntrct;
    private DbsField bci_Tiaa_Cref_Ind;
    private DbsField bci_Cntrct_Type;
    private DbsField bci_Eff_Dte;
    private DbsField bci_Mos_Ind;
    private DbsField bci_Irrvcble_Ind;
    private DbsField bci_Pymnt_Chld_Dcsd_Ind;
    private DbsField bci_Exempt_Spouse_Rights;
    private DbsField bci_Spouse_Waived_Bnfts;
    private DbsField bci_Chng_New_Issue_Ind;
    private DbsField bci_Same_As_Ind;

    private DataAccessProgramView vw_bdi;
    private DbsField bdi_Intrfce_Stts;
    private DbsField bdi_Tiaa_Cref_Ind;
    private DbsField bdi_Seq_Nmbr;
    private DbsField bdi_Rcrd_Crtd_For_Bsnss_Dte;
    private DbsField bdi_Rcrd_Last_Updt_Dte;
    private DbsField bdi_Rcrd_Last_Updt_Tme;
    private DbsField bdi_Rcrd_Last_Updt_Userid;
    private DbsField bdi_Pin_Tiaa_Cntrct;
    private DbsField bdi_Bene_Type;
    private DbsField bdi_Rltn_Cde;
    private DbsField bdi_Bene_Name1;
    private DbsField bdi_Bene_Name2;
    private DbsField bdi_Rltn_Free_Txt;
    private DbsField bdi_Dte_Birth_Trust;
    private DbsField bdi_Ssn_Cde;
    private DbsField bdi_Ssn;
    private DbsField bdi_Prcnt_Frctn_Ind;
    private DbsField bdi_Share_Prcnt;
    private DbsField bdi_Share_Nmrtr;
    private DbsField bdi_Share_Dnmntr;
    private DbsField bdi_Irrvcble_Ind;
    private DbsField bdi_Sttlmnt_Rstrctn;
    private DbsField bdi_Spcl_Txt1;
    private DbsField bdi_Spcl_Txt2;
    private DbsField bdi_Spcl_Txt3;
    private DbsField bdi_Dflt_Estate;
    private DbsField bdi_Bene_Addr1;
    private DbsField bdi_Bene_Addr2;
    private DbsField bdi_Bene_Addr3_City;
    private DbsField bdi_Bene_State;
    private DbsField bdi_Bene_Zip;
    private DbsField bdi_Bene_Phone;
    private DbsField bdi_Bene_Gender;
    private DbsField bdi_Bene_Country;

    private DbsGroup pnd_Input;
    private DbsField pnd_Input_Pnd_Common_Data;

    private DbsGroup pnd_Input__R_Field_1;
    private DbsField pnd_Input_Pnd_Intrfce_Bsnss_Dte;
    private DbsField pnd_Input_Pnd_New_Issuefslash_Chng_Ind;
    private DbsField pnd_Input_Pnd_Tiaa_Cntrct;
    private DbsField pnd_Input_Pnd_Effctve_Dte;
    private DbsField pnd_Input_Pnd_Exempt_Spouse_Rights;
    private DbsField pnd_Input_Pnd_Spouse_Waived_Bnfts;
    private DbsField pnd_Input_Pnd_Bene_Data;

    private DbsGroup pnd_Input__R_Field_2;
    private DbsField pnd_Input_Pnd_Bene_Name1;
    private DbsField pnd_Input_Pnd_Bene_Name2;
    private DbsField pnd_Input_Pnd_Bene_Type;
    private DbsField pnd_Input_Pnd_Dte_Birth_Trust;
    private DbsField pnd_Input_Pnd_Rtln_Cde;
    private DbsField pnd_Input_Pnd_Ss_Cd;
    private DbsField pnd_Input_Pnd_Ss_Nbr;
    private DbsField pnd_Input_Pnd_Prctge;
    private DbsField pnd_Input_Pnd_Dflt_To_Estate;
    private DbsField pnd_Input_Pnd_Mos_Ind;
    private DbsField pnd_Input_Pnd_Irrvcbl_Ind;
    private DbsField pnd_Input_Pnd_Pymnt_Chld_Dcsd_Ind;
    private DbsField pnd_Input_Pnd_Address1;
    private DbsField pnd_Input_Pnd_Address2;
    private DbsField pnd_Input_Pnd_Address3;
    private DbsField pnd_Input_Pnd_Address4;
    private DbsField pnd_Input_Pnd_City;
    private DbsField pnd_Input_Pnd_State;
    private DbsField pnd_Input_Pnd_Zip;
    private DbsField pnd_Input_Pnd_Country;
    private DbsField pnd_Input_Pnd_Phone_Number;
    private DbsField pnd_Input_Pnd_Gender;
    private DbsField pnd_A;
    private DbsField pnd_B;
    private DbsField pnd_C;
    private DbsField pnd_Ss_Num;
    private DbsField pnd_Total_Interface;
    private DbsField pnd_Error_Cnt;
    private DbsField pnd_Input_Cnt;
    private DbsField pnd_Input_Cntrct;
    private DbsField pnd_Input_Ignore;
    private DbsField pnd_Total_Alloc_P;
    private DbsField pnd_Total_Alloc_C;
    private DbsField pnd_Error_Found;
    private DbsField pnd_Intrfce_Super;

    private DbsGroup pnd_Intrfce_Super__R_Field_3;
    private DbsField pnd_Intrfce_Super_Pnd_Int_Pin;
    private DbsField pnd_Intrfce_Super_Pnd_Int_Cntrct;
    private DbsField pnd_Intrfce_Super_Pnd_Int_Stts;
    private DbsField pnd_Existing;
    private DbsField pnd_Error_Cntrct;
    private DbsField pnd_Err_Msg;
    private DbsField pnd_Mdm_Found;
    private DbsField pnd_Payee_Table;
    private DbsField pnd_J;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaBena971 = new PdaBena971(localVariables);
        pdaMdma101 = new PdaMdma101(localVariables);
        pdaMdma211 = new PdaMdma211(localVariables);

        // Local Variables

        vw_bc = new DataAccessProgramView(new NameInfo("vw_bc", "BC"), "BENE_CONTRACT_12", "BENE_CONTRACT");
        bc_Bc_Pin = vw_bc.getRecord().newFieldInGroup("bc_Bc_Pin", "BC-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BC_PIN");
        bc_Bc_Pin.setDdmHeader("PIN");
        bc_Bc_Tiaa_Cntrct = vw_bc.getRecord().newFieldInGroup("bc_Bc_Tiaa_Cntrct", "BC-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BC_TIAA_CNTRCT");
        bc_Bc_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bc_Bc_Tiaa_Cref_Ind = vw_bc.getRecord().newFieldInGroup("bc_Bc_Tiaa_Cref_Ind", "BC-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_TIAA_CREF_IND");
        bc_Bc_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bc_Bc_Stat = vw_bc.getRecord().newFieldInGroup("bc_Bc_Stat", "BC-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_STAT");
        bc_Bc_Stat.setDdmHeader("STATUS");
        registerRecord(vw_bc);

        vw_bmi = new DataAccessProgramView(new NameInfo("vw_bmi", "BMI"), "BENE_MOS_INTERFACE_12", "BENE_MOS_INTFCE", DdmPeriodicGroups.getInstance().getGroups("BENE_MOS_INTERFACE_12"));
        bmi_Intrfce_Stts = vw_bmi.getRecord().newFieldInGroup("bmi_Intrfce_Stts", "INTRFCE-STTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, "INTRFCE_STTS");
        bmi_Intrfce_Stts.setDdmHeader("INTRFCE/STATUS");
        bmi_Rcrd_Crtd_For_Bsnss_Dte = vw_bmi.getRecord().newFieldInGroup("bmi_Rcrd_Crtd_For_Bsnss_Dte", "RCRD-CRTD-FOR-BSNSS-DTE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "RCRD_CRTD_FOR_BSNSS_DTE");
        bmi_Rcrd_Crtd_For_Bsnss_Dte.setDdmHeader("RCRD CRTD/FOR/BSNSS DTE");
        bmi_Tiaa_Cref_Ind = vw_bmi.getRecord().newFieldInGroup("bmi_Tiaa_Cref_Ind", "TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIAA_CREF_IND");
        bmi_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bmi_Pin_Tiaa_Cntrct = vw_bmi.getRecord().newFieldInGroup("bmi_Pin_Tiaa_Cntrct", "PIN-TIAA-CNTRCT", FieldType.STRING, 22, RepeatingFieldStrategy.None, 
            "PIN_TIAA_CNTRCT");
        bmi_Pin_Tiaa_Cntrct.setDdmHeader("PIN/TIAA/CONTRACT");
        bmi_Rcrd_Crtd_Dte = vw_bmi.getRecord().newFieldInGroup("bmi_Rcrd_Crtd_Dte", "RCRD-CRTD-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_CRTD_DTE");
        bmi_Rcrd_Crtd_Dte.setDdmHeader("RECORD/CREATED/DATE");
        bmi_Rcrd_Crtd_Tme = vw_bmi.getRecord().newFieldInGroup("bmi_Rcrd_Crtd_Tme", "RCRD-CRTD-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "RCRD_CRTD_TME");
        bmi_Rcrd_Crtd_Tme.setDdmHeader("RECORD/CREATED/TIME");
        bmi_Rcrd_Last_Updt_Dte = vw_bmi.getRecord().newFieldInGroup("bmi_Rcrd_Last_Updt_Dte", "RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_DTE");
        bmi_Rcrd_Last_Updt_Dte.setDdmHeader("RECORD/LAST UPDT/DATE");
        bmi_Rcrd_Last_Updt_Tme = vw_bmi.getRecord().newFieldInGroup("bmi_Rcrd_Last_Updt_Tme", "RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_TME");
        bmi_Rcrd_Last_Updt_Tme.setDdmHeader("RECORD/LAST UPDT/TIME");
        bmi_Rcrd_Last_Updt_Userid = vw_bmi.getRecord().newFieldInGroup("bmi_Rcrd_Last_Updt_Userid", "RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_USERID");
        bmi_Rcrd_Last_Updt_Userid.setDdmHeader("RECORD/LAST UPDT/USER ID");

        bmi_Mos_Txt_Group = vw_bmi.getRecord().newGroupInGroup("bmi_Mos_Txt_Group", "MOS-TXT-GROUP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "BENE_MOS_INTFCE_MOS_TXT_GROUP");
        bmi_Mos_Txt = bmi_Mos_Txt_Group.newFieldArrayInGroup("bmi_Mos_Txt", "MOS-TXT", FieldType.STRING, 72, new DbsArrayController(1, 60) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "MOS_TXT", "BENE_MOS_INTFCE_MOS_TXT_GROUP");
        bmi_Mos_Txt.setDdmHeader("MOS TEXT");
        registerRecord(vw_bmi);

        vw_bci = new DataAccessProgramView(new NameInfo("vw_bci", "BCI"), "BENE_CONTRACT_INTERFACE_12", "BENE_CONT_INTFCE", DdmPeriodicGroups.getInstance().getGroups("BENE_CONTRACT_INTERFACE_12"));
        bci_Intrfce_Stts = vw_bci.getRecord().newFieldInGroup("bci_Intrfce_Stts", "INTRFCE-STTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, "INTRFCE_STTS");
        bci_Intrfce_Stts.setDdmHeader("INTRFCE/STATUS");
        bci_Rcrd_Crtd_For_Bsnss_Dte = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Crtd_For_Bsnss_Dte", "RCRD-CRTD-FOR-BSNSS-DTE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "RCRD_CRTD_FOR_BSNSS_DTE");
        bci_Rcrd_Crtd_For_Bsnss_Dte.setDdmHeader("RCRD CRTD/FOR/BSNSS DTE");
        bci_Rcrd_Crtd_Dte = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Crtd_Dte", "RCRD-CRTD-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_CRTD_DTE");
        bci_Rcrd_Crtd_Dte.setDdmHeader("RECORD/CREATED/DATE");
        bci_Rcrd_Crtd_Tme = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Crtd_Tme", "RCRD-CRTD-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "RCRD_CRTD_TME");
        bci_Rcrd_Crtd_Tme.setDdmHeader("RECORD/CREATED/TIME");
        bci_Intrfcng_Systm = vw_bci.getRecord().newFieldInGroup("bci_Intrfcng_Systm", "INTRFCNG-SYSTM", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "INTRFCNG_SYSTM");
        bci_Intrfcng_Systm.setDdmHeader("INTRFCNG/SYSTEM");
        bci_Rqstng_Systm = vw_bci.getRecord().newFieldInGroup("bci_Rqstng_Systm", "RQSTNG-SYSTM", FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQSTNG_SYSTM");
        bci_Rqstng_Systm.setDdmHeader("RQSTNG/SYSTEM");
        bci_Rcrd_Last_Updt_Dte = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Last_Updt_Dte", "RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_DTE");
        bci_Rcrd_Last_Updt_Dte.setDdmHeader("RECORD/LAST UPDT/DATE");
        bci_Rcrd_Last_Updt_Tme = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Last_Updt_Tme", "RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_TME");
        bci_Rcrd_Last_Updt_Tme.setDdmHeader("RECORD/LAST UPDT/TIME");
        bci_Rcrd_Last_Updt_Userid = vw_bci.getRecord().newFieldInGroup("bci_Rcrd_Last_Updt_Userid", "RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_USERID");
        bci_Rcrd_Last_Updt_Userid.setDdmHeader("RECORD/LAST UPDT/USER ID");
        bci_Last_Prcssd_Bsnss_Dte = vw_bci.getRecord().newFieldInGroup("bci_Last_Prcssd_Bsnss_Dte", "LAST-PRCSSD-BSNSS-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "LAST_PRCSSD_BSNSS_DTE");
        bci_Last_Prcssd_Bsnss_Dte.setDdmHeader("LAST PRCSSD/BUSINESS/DATE");
        bci_Intrfce_Cmpltd_Dte = vw_bci.getRecord().newFieldInGroup("bci_Intrfce_Cmpltd_Dte", "INTRFCE-CMPLTD-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "INTRFCE_CMPLTD_DTE");
        bci_Intrfce_Cmpltd_Dte.setDdmHeader("INTFCE/CMPLTD/DATE");
        bci_Intrfce_Cmpltd_Tme = vw_bci.getRecord().newFieldInGroup("bci_Intrfce_Cmpltd_Tme", "INTRFCE-CMPLTD-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "INTRFCE_CMPLTD_TME");
        bci_Intrfce_Cmpltd_Tme.setDdmHeader("INTRFCE/CMPLTD/TME");
        bci_Dflt_To_Estate_Ind = vw_bci.getRecord().newFieldInGroup("bci_Dflt_To_Estate_Ind", "DFLT-TO-ESTATE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "DFLT_TO_ESTATE_IND");
        bci_Dflt_To_Estate_Ind.setDdmHeader("DEFAULT/TO/ESTATE");
        bci_Illgble_Ind = vw_bci.getRecord().newFieldInGroup("bci_Illgble_Ind", "ILLGBLE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "ILLGBLE_IND");
        bci_Illgble_Ind.setDdmHeader("ILLGBLE/IND");
        bci_More_Than_Five_Benes_Ind = vw_bci.getRecord().newFieldInGroup("bci_More_Than_Five_Benes_Ind", "MORE-THAN-FIVE-BENES-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "MORE_THAN_FIVE_BENES_IND");
        bci_More_Than_Five_Benes_Ind.setDdmHeader("MORE/THAN 5/BENES");
        bci_Intrfce_Mgrtn_Ind = vw_bci.getRecord().newFieldInGroup("bci_Intrfce_Mgrtn_Ind", "INTRFCE-MGRTN-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "INTRFCE_MGRTN_IND");
        bci_Intrfce_Mgrtn_Ind.setDdmHeader("INTRFCE/MGRTN/IND");
        bci_More_Than_Thirty_Benes_Ind = vw_bci.getRecord().newFieldInGroup("bci_More_Than_Thirty_Benes_Ind", "MORE-THAN-THIRTY-BENES-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "MORE_THAN_THIRTY_BENES_IND");
        bci_More_Than_Thirty_Benes_Ind.setDdmHeader("MORE/THAN 30/BENES");
        bci_Pin_Tiaa_Cntrct = vw_bci.getRecord().newFieldInGroup("bci_Pin_Tiaa_Cntrct", "PIN-TIAA-CNTRCT", FieldType.STRING, 22, RepeatingFieldStrategy.None, 
            "PIN_TIAA_CNTRCT");
        bci_Pin_Tiaa_Cntrct.setDdmHeader("PIN/TIAA/CONTRACT");

        bci_Error_Table = vw_bci.getRecord().newGroupInGroup("bci_Error_Table", "ERROR-TABLE", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Error_Txt = bci_Error_Table.newFieldArrayInGroup("bci_Error_Txt", "ERROR-TXT", FieldType.STRING, 72, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ERROR_TXT", "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Error_Txt.setDdmHeader("ERROR/TEXT");
        bci_Error_Cde = bci_Error_Table.newFieldArrayInGroup("bci_Error_Cde", "ERROR-CDE", FieldType.STRING, 5, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ERROR_CDE", "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Error_Cde.setDdmHeader("ERROR/CODE");
        bci_Error_Dte = bci_Error_Table.newFieldArrayInGroup("bci_Error_Dte", "ERROR-DTE", FieldType.STRING, 8, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ERROR_DTE", "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Error_Dte.setDdmHeader("ERROR/DATE");
        bci_Error_Tme = bci_Error_Table.newFieldArrayInGroup("bci_Error_Tme", "ERROR-TME", FieldType.STRING, 7, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ERROR_TME", "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Error_Tme.setDdmHeader("ERROR/TIME");
        bci_Error_Pgm = bci_Error_Table.newFieldArrayInGroup("bci_Error_Pgm", "ERROR-PGM", FieldType.STRING, 8, new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ERROR_PGM", "BENE_CONT_INTFCE_ERROR_TABLE");
        bci_Fldr_Log_Dte_Tme = vw_bci.getRecord().newFieldInGroup("bci_Fldr_Log_Dte_Tme", "FLDR-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "FLDR_LOG_DTE_TME");
        bci_Fldr_Log_Dte_Tme.setDdmHeader("FOLDER/LOG DATE/TIME");
        bci_Last_Dsgntn_Srce = vw_bci.getRecord().newFieldInGroup("bci_Last_Dsgntn_Srce", "LAST-DSGNTN-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "LAST_DSGNTN_SRCE");
        bci_Last_Dsgntn_Srce.setDdmHeader("LAST/DSGNTN/SOURCE");
        bci_Last_Dsgntn_System = vw_bci.getRecord().newFieldInGroup("bci_Last_Dsgntn_System", "LAST-DSGNTN-SYSTEM", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "LAST_DSGNTN_SYSTEM");
        bci_Last_Dsgntn_System.setDdmHeader("LAST/DSGNTN/SYSTEM");
        bci_Last_Dsgntn_Userid = vw_bci.getRecord().newFieldInGroup("bci_Last_Dsgntn_Userid", "LAST-DSGNTN-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "LAST_DSGNTN_USERID");
        bci_Last_Dsgntn_Userid.setDdmHeader("LAST/DSGNTN/USERID");
        bci_Last_Dsgntn_Dte = vw_bci.getRecord().newFieldInGroup("bci_Last_Dsgntn_Dte", "LAST-DSGNTN-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "LAST_DSGNTN_DTE");
        bci_Last_Dsgntn_Dte.setDdmHeader("LAST/DSGNTN/DATE");
        bci_Last_Dsgntn_Tme = vw_bci.getRecord().newFieldInGroup("bci_Last_Dsgntn_Tme", "LAST-DSGNTN-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "LAST_DSGNTN_TME");
        bci_Last_Dsgntn_Tme.setDdmHeader("LAST/DSGNTN/TIME");
        bci_Tiaa_Cref_Chng_Dte = vw_bci.getRecord().newFieldInGroup("bci_Tiaa_Cref_Chng_Dte", "TIAA-CREF-CHNG-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIAA_CREF_CHNG_DTE");
        bci_Tiaa_Cref_Chng_Dte.setDdmHeader("TIAA CREF/CHANGE/DATE");
        bci_Tiaa_Cref_Chng_Tme = vw_bci.getRecord().newFieldInGroup("bci_Tiaa_Cref_Chng_Tme", "TIAA-CREF-CHNG-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "TIAA_CREF_CHNG_TME");
        bci_Tiaa_Cref_Chng_Tme.setDdmHeader("TIAA CREF/CHANGE/TIME");
        bci_Chng_Pwr_Atty = vw_bci.getRecord().newFieldInGroup("bci_Chng_Pwr_Atty", "CHNG-PWR-ATTY", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CHNG_PWR_ATTY");
        bci_Chng_Pwr_Atty.setDdmHeader("CHANGE/POWER/ATTNY");
        bci_Cref_Cntrct = vw_bci.getRecord().newFieldInGroup("bci_Cref_Cntrct", "CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT");
        bci_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bci_Tiaa_Cref_Ind = vw_bci.getRecord().newFieldInGroup("bci_Tiaa_Cref_Ind", "TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIAA_CREF_IND");
        bci_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bci_Cntrct_Type = vw_bci.getRecord().newFieldInGroup("bci_Cntrct_Type", "CNTRCT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_TYPE");
        bci_Cntrct_Type.setDdmHeader("CNTRCT/TYPE");
        bci_Eff_Dte = vw_bci.getRecord().newFieldInGroup("bci_Eff_Dte", "EFF-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "EFF_DTE");
        bci_Eff_Dte.setDdmHeader("EFFECTIVE/DATE");
        bci_Mos_Ind = vw_bci.getRecord().newFieldInGroup("bci_Mos_Ind", "MOS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "MOS_IND");
        bci_Mos_Ind.setDdmHeader("MOS/IND");
        bci_Irrvcble_Ind = vw_bci.getRecord().newFieldInGroup("bci_Irrvcble_Ind", "IRRVCBLE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "IRRVCBLE_IND");
        bci_Irrvcble_Ind.setDdmHeader("IRVCBL/IND");
        bci_Pymnt_Chld_Dcsd_Ind = vw_bci.getRecord().newFieldInGroup("bci_Pymnt_Chld_Dcsd_Ind", "PYMNT-CHLD-DCSD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PYMNT_CHLD_DCSD_IND");
        bci_Pymnt_Chld_Dcsd_Ind.setDdmHeader("PYMNT/CHILD/DCSD");
        bci_Exempt_Spouse_Rights = vw_bci.getRecord().newFieldInGroup("bci_Exempt_Spouse_Rights", "EXEMPT-SPOUSE-RIGHTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "EXEMPT_SPOUSE_RIGHTS");
        bci_Exempt_Spouse_Rights.setDdmHeader("EXEMPT/SPOUSAL/RIGHTS");
        bci_Spouse_Waived_Bnfts = vw_bci.getRecord().newFieldInGroup("bci_Spouse_Waived_Bnfts", "SPOUSE-WAIVED-BNFTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "SPOUSE_WAIVED_BNFTS");
        bci_Spouse_Waived_Bnfts.setDdmHeader("SPOUSE/WAIVED/BENEFITS");
        bci_Chng_New_Issue_Ind = vw_bci.getRecord().newFieldInGroup("bci_Chng_New_Issue_Ind", "CHNG-NEW-ISSUE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CHNG_NEW_ISSUE_IND");
        bci_Chng_New_Issue_Ind.setDdmHeader("CHANGE/NEW ISSUE/IND");
        bci_Same_As_Ind = vw_bci.getRecord().newFieldInGroup("bci_Same_As_Ind", "SAME-AS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "SAME_AS_IND");
        registerRecord(vw_bci);

        vw_bdi = new DataAccessProgramView(new NameInfo("vw_bdi", "BDI"), "BENE_DESIGNATION_INTERFACE_12", "BENE_DSGN_INTFCE");
        bdi_Intrfce_Stts = vw_bdi.getRecord().newFieldInGroup("bdi_Intrfce_Stts", "INTRFCE-STTS", FieldType.STRING, 1, RepeatingFieldStrategy.None, "INTRFCE_STTS");
        bdi_Intrfce_Stts.setDdmHeader("INTRFCE/STATUS");
        bdi_Tiaa_Cref_Ind = vw_bdi.getRecord().newFieldInGroup("bdi_Tiaa_Cref_Ind", "TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIAA_CREF_IND");
        bdi_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bdi_Seq_Nmbr = vw_bdi.getRecord().newFieldInGroup("bdi_Seq_Nmbr", "SEQ-NMBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "SEQ_NMBR");
        bdi_Seq_Nmbr.setDdmHeader("SEQ/NMBR");
        bdi_Rcrd_Crtd_For_Bsnss_Dte = vw_bdi.getRecord().newFieldInGroup("bdi_Rcrd_Crtd_For_Bsnss_Dte", "RCRD-CRTD-FOR-BSNSS-DTE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "RCRD_CRTD_FOR_BSNSS_DTE");
        bdi_Rcrd_Crtd_For_Bsnss_Dte.setDdmHeader("RCRD CRTD/FOR/BSNSS DTE");
        bdi_Rcrd_Last_Updt_Dte = vw_bdi.getRecord().newFieldInGroup("bdi_Rcrd_Last_Updt_Dte", "RCRD-LAST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_DTE");
        bdi_Rcrd_Last_Updt_Dte.setDdmHeader("RECORD/LAST UPDT/DATE");
        bdi_Rcrd_Last_Updt_Tme = vw_bdi.getRecord().newFieldInGroup("bdi_Rcrd_Last_Updt_Tme", "RCRD-LAST-UPDT-TME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_TME");
        bdi_Rcrd_Last_Updt_Tme.setDdmHeader("RECORD/LAST UPDT/TIME");
        bdi_Rcrd_Last_Updt_Userid = vw_bdi.getRecord().newFieldInGroup("bdi_Rcrd_Last_Updt_Userid", "RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RCRD_LAST_UPDT_USERID");
        bdi_Rcrd_Last_Updt_Userid.setDdmHeader("RECORD/LAST UPDT/USER ID");
        bdi_Pin_Tiaa_Cntrct = vw_bdi.getRecord().newFieldInGroup("bdi_Pin_Tiaa_Cntrct", "PIN-TIAA-CNTRCT", FieldType.STRING, 22, RepeatingFieldStrategy.None, 
            "PIN_TIAA_CNTRCT");
        bdi_Pin_Tiaa_Cntrct.setDdmHeader("PIN/TIAA/CONTRACT");
        bdi_Bene_Type = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Type", "BENE-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BENE_TYPE");
        bdi_Bene_Type.setDdmHeader("BENE/TYPE");
        bdi_Rltn_Cde = vw_bdi.getRecord().newFieldInGroup("bdi_Rltn_Cde", "RLTN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "RLTN_CDE");
        bdi_Rltn_Cde.setDdmHeader("RELATION/CODE");
        bdi_Bene_Name1 = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Name1", "BENE-NAME1", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BENE_NAME1");
        bdi_Bene_Name1.setDdmHeader("BENEFICIARY/NAME 1");
        bdi_Bene_Name2 = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Name2", "BENE-NAME2", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BENE_NAME2");
        bdi_Bene_Name2.setDdmHeader("BENEFICIARY/NAME 2");
        bdi_Rltn_Free_Txt = vw_bdi.getRecord().newFieldInGroup("bdi_Rltn_Free_Txt", "RLTN-FREE-TXT", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "RLTN_FREE_TXT");
        bdi_Rltn_Free_Txt.setDdmHeader("OTHER/RELATION");
        bdi_Dte_Birth_Trust = vw_bdi.getRecord().newFieldInGroup("bdi_Dte_Birth_Trust", "DTE-BIRTH-TRUST", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "DTE_BIRTH_TRUST");
        bdi_Dte_Birth_Trust.setDdmHeader("DOB/DOT");
        bdi_Ssn_Cde = vw_bdi.getRecord().newFieldInGroup("bdi_Ssn_Cde", "SSN-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "SSN_CDE");
        bdi_Ssn_Cde.setDdmHeader("SSN/CODE");
        bdi_Ssn = vw_bdi.getRecord().newFieldInGroup("bdi_Ssn", "SSN", FieldType.STRING, 9, RepeatingFieldStrategy.None, "SSN");
        bdi_Ssn.setDdmHeader("SSN");
        bdi_Prcnt_Frctn_Ind = vw_bdi.getRecord().newFieldInGroup("bdi_Prcnt_Frctn_Ind", "PRCNT-FRCTN-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PRCNT_FRCTN_IND");
        bdi_Prcnt_Frctn_Ind.setDdmHeader("PRCNT/FRCTN/IND");
        bdi_Share_Prcnt = vw_bdi.getRecord().newFieldInGroup("bdi_Share_Prcnt", "SHARE-PRCNT", FieldType.NUMERIC, 5, 2, RepeatingFieldStrategy.None, "SHARE_PRCNT");
        bdi_Share_Prcnt.setDdmHeader("SHARE/PERCENT");
        bdi_Share_Nmrtr = vw_bdi.getRecord().newFieldInGroup("bdi_Share_Nmrtr", "SHARE-NMRTR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "SHARE_NMRTR");
        bdi_Share_Nmrtr.setDdmHeader("SHARE/NUMERATOR");
        bdi_Share_Dnmntr = vw_bdi.getRecord().newFieldInGroup("bdi_Share_Dnmntr", "SHARE-DNMNTR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "SHARE_DNMNTR");
        bdi_Share_Dnmntr.setDdmHeader("SHARE/DENOMINATOR");
        bdi_Irrvcble_Ind = vw_bdi.getRecord().newFieldInGroup("bdi_Irrvcble_Ind", "IRRVCBLE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "IRRVCBLE_IND");
        bdi_Irrvcble_Ind.setDdmHeader("IRVCBL/IND");
        bdi_Sttlmnt_Rstrctn = vw_bdi.getRecord().newFieldInGroup("bdi_Sttlmnt_Rstrctn", "STTLMNT-RSTRCTN", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "STTLMNT_RSTRCTN");
        bdi_Sttlmnt_Rstrctn.setDdmHeader("SETTLEMENT/RESTRICTION");
        bdi_Spcl_Txt1 = vw_bdi.getRecord().newFieldInGroup("bdi_Spcl_Txt1", "SPCL-TXT1", FieldType.STRING, 72, RepeatingFieldStrategy.None, "SPCL_TXT1");
        bdi_Spcl_Txt1.setDdmHeader("SPECIAL TEXT 1");
        bdi_Spcl_Txt2 = vw_bdi.getRecord().newFieldInGroup("bdi_Spcl_Txt2", "SPCL-TXT2", FieldType.STRING, 72, RepeatingFieldStrategy.None, "SPCL_TXT2");
        bdi_Spcl_Txt2.setDdmHeader("SPECIAL TEXT 2");
        bdi_Spcl_Txt3 = vw_bdi.getRecord().newFieldInGroup("bdi_Spcl_Txt3", "SPCL-TXT3", FieldType.STRING, 72, RepeatingFieldStrategy.None, "SPCL_TXT3");
        bdi_Spcl_Txt3.setDdmHeader("SPECIAL TEXT 3");
        bdi_Dflt_Estate = vw_bdi.getRecord().newFieldInGroup("bdi_Dflt_Estate", "DFLT-ESTATE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "DFLT_ESTATE");
        bdi_Dflt_Estate.setDdmHeader("DEFAULT/ESTATE");
        bdi_Bene_Addr1 = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Addr1", "BENE-ADDR1", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BENE_ADDR1");
        bdi_Bene_Addr2 = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Addr2", "BENE-ADDR2", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BENE_ADDR2");
        bdi_Bene_Addr3_City = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Addr3_City", "BENE-ADDR3-CITY", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "BENE_ADDR3_CITY");
        bdi_Bene_State = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_State", "BENE-STATE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "BENE_STATE");
        bdi_Bene_Zip = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Zip", "BENE-ZIP", FieldType.STRING, 10, RepeatingFieldStrategy.None, "BENE_ZIP");
        bdi_Bene_Phone = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Phone", "BENE-PHONE", FieldType.STRING, 20, RepeatingFieldStrategy.None, "BENE_PHONE");
        bdi_Bene_Gender = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Gender", "BENE-GENDER", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BENE_GENDER");
        bdi_Bene_Country = vw_bdi.getRecord().newFieldInGroup("bdi_Bene_Country", "BENE-COUNTRY", FieldType.STRING, 35, RepeatingFieldStrategy.None, "BENE_COUNTRY");
        registerRecord(vw_bdi);

        pnd_Input = localVariables.newGroupInRecord("pnd_Input", "#INPUT");
        pnd_Input_Pnd_Common_Data = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Common_Data", "#COMMON-DATA", FieldType.STRING, 30);

        pnd_Input__R_Field_1 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_1", "REDEFINE", pnd_Input_Pnd_Common_Data);
        pnd_Input_Pnd_Intrfce_Bsnss_Dte = pnd_Input__R_Field_1.newFieldInGroup("pnd_Input_Pnd_Intrfce_Bsnss_Dte", "#INTRFCE-BSNSS-DTE", FieldType.STRING, 
            8);
        pnd_Input_Pnd_New_Issuefslash_Chng_Ind = pnd_Input__R_Field_1.newFieldInGroup("pnd_Input_Pnd_New_Issuefslash_Chng_Ind", "#NEW-ISSUE/CHNG-IND", 
            FieldType.STRING, 1);
        pnd_Input_Pnd_Tiaa_Cntrct = pnd_Input__R_Field_1.newFieldInGroup("pnd_Input_Pnd_Tiaa_Cntrct", "#TIAA-CNTRCT", FieldType.STRING, 11);
        pnd_Input_Pnd_Effctve_Dte = pnd_Input__R_Field_1.newFieldInGroup("pnd_Input_Pnd_Effctve_Dte", "#EFFCTVE-DTE", FieldType.STRING, 8);
        pnd_Input_Pnd_Exempt_Spouse_Rights = pnd_Input__R_Field_1.newFieldInGroup("pnd_Input_Pnd_Exempt_Spouse_Rights", "#EXEMPT-SPOUSE-RIGHTS", FieldType.STRING, 
            1);
        pnd_Input_Pnd_Spouse_Waived_Bnfts = pnd_Input__R_Field_1.newFieldInGroup("pnd_Input_Pnd_Spouse_Waived_Bnfts", "#SPOUSE-WAIVED-BNFTS", FieldType.STRING, 
            1);
        pnd_Input_Pnd_Bene_Data = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Bene_Data", "#BENE-DATA", FieldType.STRING, 97);

        pnd_Input__R_Field_2 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_2", "REDEFINE", pnd_Input_Pnd_Bene_Data);
        pnd_Input_Pnd_Bene_Name1 = pnd_Input__R_Field_2.newFieldInGroup("pnd_Input_Pnd_Bene_Name1", "#BENE-NAME1", FieldType.STRING, 35);
        pnd_Input_Pnd_Bene_Name2 = pnd_Input__R_Field_2.newFieldInGroup("pnd_Input_Pnd_Bene_Name2", "#BENE-NAME2", FieldType.STRING, 35);
        pnd_Input_Pnd_Bene_Type = pnd_Input__R_Field_2.newFieldInGroup("pnd_Input_Pnd_Bene_Type", "#BENE-TYPE", FieldType.STRING, 1);
        pnd_Input_Pnd_Dte_Birth_Trust = pnd_Input__R_Field_2.newFieldInGroup("pnd_Input_Pnd_Dte_Birth_Trust", "#DTE-BIRTH-TRUST", FieldType.STRING, 8);
        pnd_Input_Pnd_Rtln_Cde = pnd_Input__R_Field_2.newFieldInGroup("pnd_Input_Pnd_Rtln_Cde", "#RTLN-CDE", FieldType.STRING, 2);
        pnd_Input_Pnd_Ss_Cd = pnd_Input__R_Field_2.newFieldInGroup("pnd_Input_Pnd_Ss_Cd", "#SS-CD", FieldType.STRING, 1);
        pnd_Input_Pnd_Ss_Nbr = pnd_Input__R_Field_2.newFieldInGroup("pnd_Input_Pnd_Ss_Nbr", "#SS-NBR", FieldType.STRING, 9);
        pnd_Input_Pnd_Prctge = pnd_Input__R_Field_2.newFieldInGroup("pnd_Input_Pnd_Prctge", "#PRCTGE", FieldType.STRING, 6);
        pnd_Input_Pnd_Dflt_To_Estate = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Dflt_To_Estate", "#DFLT-TO-ESTATE", FieldType.STRING, 1);
        pnd_Input_Pnd_Mos_Ind = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Mos_Ind", "#MOS-IND", FieldType.STRING, 1);
        pnd_Input_Pnd_Irrvcbl_Ind = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Irrvcbl_Ind", "#IRRVCBL-IND", FieldType.STRING, 1);
        pnd_Input_Pnd_Pymnt_Chld_Dcsd_Ind = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Pymnt_Chld_Dcsd_Ind", "#PYMNT-CHLD-DCSD-IND", FieldType.STRING, 1);
        pnd_Input_Pnd_Address1 = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Address1", "#ADDRESS1", FieldType.STRING, 35);
        pnd_Input_Pnd_Address2 = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Address2", "#ADDRESS2", FieldType.STRING, 35);
        pnd_Input_Pnd_Address3 = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Address3", "#ADDRESS3", FieldType.STRING, 35);
        pnd_Input_Pnd_Address4 = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Address4", "#ADDRESS4", FieldType.STRING, 35);
        pnd_Input_Pnd_City = pnd_Input.newFieldInGroup("pnd_Input_Pnd_City", "#CITY", FieldType.STRING, 35);
        pnd_Input_Pnd_State = pnd_Input.newFieldInGroup("pnd_Input_Pnd_State", "#STATE", FieldType.STRING, 2);
        pnd_Input_Pnd_Zip = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Zip", "#ZIP", FieldType.STRING, 10);
        pnd_Input_Pnd_Country = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Country", "#COUNTRY", FieldType.STRING, 35);
        pnd_Input_Pnd_Phone_Number = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Phone_Number", "#PHONE-NUMBER", FieldType.STRING, 20);
        pnd_Input_Pnd_Gender = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Gender", "#GENDER", FieldType.STRING, 1);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.PACKED_DECIMAL, 3);
        pnd_B = localVariables.newFieldInRecord("pnd_B", "#B", FieldType.PACKED_DECIMAL, 3);
        pnd_C = localVariables.newFieldInRecord("pnd_C", "#C", FieldType.PACKED_DECIMAL, 3);
        pnd_Ss_Num = localVariables.newFieldInRecord("pnd_Ss_Num", "#SS-NUM", FieldType.NUMERIC, 9);
        pnd_Total_Interface = localVariables.newFieldInRecord("pnd_Total_Interface", "#TOTAL-INTERFACE", FieldType.PACKED_DECIMAL, 10);
        pnd_Error_Cnt = localVariables.newFieldInRecord("pnd_Error_Cnt", "#ERROR-CNT", FieldType.PACKED_DECIMAL, 10);
        pnd_Input_Cnt = localVariables.newFieldInRecord("pnd_Input_Cnt", "#INPUT-CNT", FieldType.PACKED_DECIMAL, 10);
        pnd_Input_Cntrct = localVariables.newFieldInRecord("pnd_Input_Cntrct", "#INPUT-CNTRCT", FieldType.PACKED_DECIMAL, 10);
        pnd_Input_Ignore = localVariables.newFieldInRecord("pnd_Input_Ignore", "#INPUT-IGNORE", FieldType.PACKED_DECIMAL, 10);
        pnd_Total_Alloc_P = localVariables.newFieldInRecord("pnd_Total_Alloc_P", "#TOTAL-ALLOC-P", FieldType.NUMERIC, 5, 2);
        pnd_Total_Alloc_C = localVariables.newFieldInRecord("pnd_Total_Alloc_C", "#TOTAL-ALLOC-C", FieldType.NUMERIC, 5, 2);
        pnd_Error_Found = localVariables.newFieldInRecord("pnd_Error_Found", "#ERROR-FOUND", FieldType.BOOLEAN, 1);
        pnd_Intrfce_Super = localVariables.newFieldInRecord("pnd_Intrfce_Super", "#INTRFCE-SUPER", FieldType.STRING, 23);

        pnd_Intrfce_Super__R_Field_3 = localVariables.newGroupInRecord("pnd_Intrfce_Super__R_Field_3", "REDEFINE", pnd_Intrfce_Super);
        pnd_Intrfce_Super_Pnd_Int_Pin = pnd_Intrfce_Super__R_Field_3.newFieldInGroup("pnd_Intrfce_Super_Pnd_Int_Pin", "#INT-PIN", FieldType.NUMERIC, 12);
        pnd_Intrfce_Super_Pnd_Int_Cntrct = pnd_Intrfce_Super__R_Field_3.newFieldInGroup("pnd_Intrfce_Super_Pnd_Int_Cntrct", "#INT-CNTRCT", FieldType.STRING, 
            10);
        pnd_Intrfce_Super_Pnd_Int_Stts = pnd_Intrfce_Super__R_Field_3.newFieldInGroup("pnd_Intrfce_Super_Pnd_Int_Stts", "#INT-STTS", FieldType.STRING, 
            1);
        pnd_Existing = localVariables.newFieldInRecord("pnd_Existing", "#EXISTING", FieldType.BOOLEAN, 1);
        pnd_Error_Cntrct = localVariables.newFieldInRecord("pnd_Error_Cntrct", "#ERROR-CNTRCT", FieldType.STRING, 10);
        pnd_Err_Msg = localVariables.newFieldInRecord("pnd_Err_Msg", "#ERR-MSG", FieldType.STRING, 100);
        pnd_Mdm_Found = localVariables.newFieldInRecord("pnd_Mdm_Found", "#MDM-FOUND", FieldType.BOOLEAN, 1);
        pnd_Payee_Table = localVariables.newFieldArrayInRecord("pnd_Payee_Table", "#PAYEE-TABLE", FieldType.STRING, 2, new DbsArrayController(1, 20));
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_bc.reset();
        vw_bmi.reset();
        vw_bci.reset();
        vw_bdi.reset();

        localVariables.reset();
        pnd_Payee_Table.getValue(1).setInitialValue("00");
        pnd_Payee_Table.getValue(2).setInitialValue("01");
        pnd_Payee_Table.getValue(3).setInitialValue("10");
        pnd_Payee_Table.getValue(4).setInitialValue("11");
        pnd_Payee_Table.getValue(5).setInitialValue("12");
        pnd_Payee_Table.getValue(6).setInitialValue("13");
        pnd_Payee_Table.getValue(7).setInitialValue("14");
        pnd_Payee_Table.getValue(8).setInitialValue("15");
        pnd_Payee_Table.getValue(9).setInitialValue("19");
        pnd_Payee_Table.getValue(10).setInitialValue("23");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Benp810() throws Exception
    {
        super("Benp810");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 60 LS = 133 SG = OFF ZP = ON;//Natural: FORMAT ( 1 ) PS = 60 LS = 250 SG = OFF ZP = ON;//Natural: FORMAT ( 2 ) PS = 60 LS = 133 SG = OFF ZP = ON;//Natural: FORMAT ( 3 ) PS = 60 LS = 133 SG = OFF ZP = ON
        //*  OPEN MDM
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        pdaBena971.getBena971().reset();                                                                                                                                  //Natural: RESET BENA971
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #INPUT
        while (condition(getWorkFiles().read(1, pnd_Input)))
        {
            pnd_Input_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #INPUT-CNT
            if (condition(! (pnd_Input_Pnd_Gender.equals("M") || pnd_Input_Pnd_Gender.equals("F"))))                                                                      //Natural: IF NOT ( #INPUT.#GENDER = 'M' OR = 'F' )
            {
                pnd_Input_Pnd_Gender.reset();                                                                                                                             //Natural: RESET #INPUT.#GENDER
            }                                                                                                                                                             //Natural: END-IF
            if (condition(DbsUtil.maskMatches(pnd_Input_Pnd_Intrfce_Bsnss_Dte,"YYYYMMDD") && DbsUtil.maskMatches(pnd_Input_Pnd_Effctve_Dte,"YYYYMMDD")))                  //Natural: IF #INPUT.#INTRFCE-BSNSS-DTE = MASK ( YYYYMMDD ) AND #INPUT.#EFFCTVE-DTE = MASK ( YYYYMMDD )
            {
                getReports().display(1, ReportOption.NOTITLE,"Int dte",                                                                                                   //Natural: DISPLAY ( 1 ) NOTITLE 'Int dte' #INPUT.#INTRFCE-BSNSS-DTE 'Nc' #INPUT.#NEW-ISSUE/CHNG-IND 'Cntrct' #INPUT.#TIAA-CNTRCT 'Eff Dte' #INPUT.#EFFCTVE-DTE 'Xr' #INPUT.#EXEMPT-SPOUSE-RIGHTS 'Wb' #INPUT.#SPOUSE-WAIVED-BNFTS 'Name1' #INPUT.#BENE-NAME1 ( AL = 20 ) 'Name2' #INPUT.#BENE-NAME2 ( AL = 20 ) 'Pc' #INPUT.#BENE-TYPE 'Dob' #INPUT.#DTE-BIRTH-TRUST 'Rl' #INPUT.#RTLN-CDE 'Sc' #INPUT.#SS-CD 'Nbr' #INPUT.#SS-NBR 'Pct' #INPUT.#PRCTGE 'Dflt' #INPUT.#DFLT-TO-ESTATE 'Mos' #INPUT.#MOS-IND 'Vcb' #INPUT.#IRRVCBL-IND 'Pd' #INPUT.#PYMNT-CHLD-DCSD-IND
                		pnd_Input_Pnd_Intrfce_Bsnss_Dte,"Nc",
                		pnd_Input_Pnd_New_Issuefslash_Chng_Ind,"Cntrct",
                		pnd_Input_Pnd_Tiaa_Cntrct,"Eff Dte",
                		pnd_Input_Pnd_Effctve_Dte,"Xr",
                		pnd_Input_Pnd_Exempt_Spouse_Rights,"Wb",
                		pnd_Input_Pnd_Spouse_Waived_Bnfts,"Name1",
                		pnd_Input_Pnd_Bene_Name1, new AlphanumericLength (20),"Name2",
                		pnd_Input_Pnd_Bene_Name2, new AlphanumericLength (20),"Pc",
                		pnd_Input_Pnd_Bene_Type,"Dob",
                		pnd_Input_Pnd_Dte_Birth_Trust,"Rl",
                		pnd_Input_Pnd_Rtln_Cde,"Sc",
                		pnd_Input_Pnd_Ss_Cd,"Nbr",
                		pnd_Input_Pnd_Ss_Nbr,"Pct",
                		pnd_Input_Pnd_Prctge,"Dflt",
                		pnd_Input_Pnd_Dflt_To_Estate,"Mos",
                		pnd_Input_Pnd_Mos_Ind,"Vcb",
                		pnd_Input_Pnd_Irrvcbl_Ind,"Pd",
                		pnd_Input_Pnd_Pymnt_Chld_Dcsd_Ind);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Input_Pnd_Address1.notEquals(" ") || pnd_Input_Pnd_Address2.notEquals(" ") || pnd_Input_Pnd_Address3.notEquals(" ")))                   //Natural: IF #INPUT.#ADDRESS1 NE ' ' OR #INPUT.#ADDRESS2 NE ' ' OR #INPUT.#ADDRESS3 NE ' '
                {
                    getReports().write(1, ReportOption.NOTITLE,"Addr1:",pnd_Input_Pnd_Address1,"Addr2:",pnd_Input_Pnd_Address2,"Addr3:",pnd_Input_Pnd_Address3);          //Natural: WRITE ( 1 ) 'Addr1:' #INPUT.#ADDRESS1 'Addr2:' #INPUT.#ADDRESS2 'Addr3:' #INPUT.#ADDRESS3
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Input_Pnd_Address4.notEquals(" ") || pnd_Input_Pnd_Country.notEquals(" ")))                                                             //Natural: IF #INPUT.#ADDRESS4 NE ' ' OR #INPUT.#COUNTRY NE ' '
                {
                    getReports().write(1, ReportOption.NOTITLE,"Addr4:",pnd_Input_Pnd_Address4,"Country:",pnd_Input_Pnd_Country);                                         //Natural: WRITE ( 1 ) 'Addr4:' #INPUT.#ADDRESS4 'Country:' #INPUT.#COUNTRY
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Input_Pnd_City.notEquals(" ") || pnd_Input_Pnd_State.notEquals(" ") || pnd_Input_Pnd_Zip.notEquals(" ") || pnd_Input_Pnd_Phone_Number.notEquals(" ")  //Natural: IF #INPUT.#CITY NE ' ' OR #INPUT.#STATE NE ' ' OR #INPUT.#ZIP NE ' ' OR #INPUT.#PHONE-NUMBER NE ' ' OR #INPUT.#GENDER NE ' '
                    || pnd_Input_Pnd_Gender.notEquals(" ")))
                {
                    getReports().write(1, ReportOption.NOTITLE,"City:",pnd_Input_Pnd_City,"State:",pnd_Input_Pnd_State,"Zip:",pnd_Input_Pnd_Zip,"Phone:",                 //Natural: WRITE ( 1 ) 'City:' #INPUT.#CITY 'State:' #INPUT.#STATE 'Zip:' #INPUT.#ZIP 'Phone:' #INPUT.#PHONE-NUMBER 'Gender:' #INPUT.#GENDER
                        pnd_Input_Pnd_Phone_Number,"Gender:",pnd_Input_Pnd_Gender);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Input_Ignore.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #INPUT-IGNORE
                getReports().write(1, ReportOption.NOTITLE,"IGNORED:",pnd_Input_Pnd_Common_Data,pnd_Input_Pnd_Bene_Data,pnd_Input_Pnd_Dflt_To_Estate,pnd_Input_Pnd_Mos_Ind, //Natural: WRITE ( 1 ) 'IGNORED:' #INPUT
                    pnd_Input_Pnd_Irrvcbl_Ind,pnd_Input_Pnd_Pymnt_Chld_Dcsd_Ind,pnd_Input_Pnd_Address1,pnd_Input_Pnd_Address2,pnd_Input_Pnd_Address3,pnd_Input_Pnd_Address4,
                    pnd_Input_Pnd_City,pnd_Input_Pnd_State,pnd_Input_Pnd_Zip,pnd_Input_Pnd_Country,pnd_Input_Pnd_Phone_Number,pnd_Input_Pnd_Gender);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaBena971.getBena971_Pnd_Tiaa_Cntrct().notEquals(pnd_Input_Pnd_Tiaa_Cntrct)))                                                                  //Natural: IF BENA971.#TIAA-CNTRCT NE #INPUT.#TIAA-CNTRCT
            {
                pnd_Input_Cntrct.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #INPUT-CNTRCT
                //*  FIRST TIME
                if (condition(pdaBena971.getBena971_Pnd_Tiaa_Cntrct().equals(" ")))                                                                                       //Natural: IF BENA971.#TIAA-CNTRCT = ' '
                {
                                                                                                                                                                          //Natural: PERFORM MOVE-CONTRACT-FIELDS
                    sub_Move_Contract_Fields();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Mdm_Found.getBoolean()))                                                                                                            //Natural: IF #MDM-FOUND
                    {
                                                                                                                                                                          //Natural: PERFORM CALL-BENN971
                        sub_Call_Benn971();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    pdaBena971.getBena971().reset();                                                                                                                      //Natural: RESET BENA971 #A
                    pnd_A.reset();
                                                                                                                                                                          //Natural: PERFORM MOVE-CONTRACT-FIELDS
                    sub_Move_Contract_Fields();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(! (pnd_Mdm_Found.getBoolean())))                                                                                                                //Natural: IF NOT #MDM-FOUND
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_A.greaterOrEqual(30)))                                                                                                                  //Natural: IF #A >= 30
                {
                    pnd_Error_Found.setValue(true);                                                                                                                       //Natural: ASSIGN #ERROR-FOUND := TRUE
                    pnd_Error_Cntrct.setValue(pdaBena971.getBena971_Pnd_Tiaa_Cntrct());                                                                                   //Natural: ASSIGN #ERROR-CNTRCT := BENA971.#TIAA-CNTRCT
                    pnd_Err_Msg.setValue("MORE THAN 30 BENEFICIARIES");                                                                                                   //Natural: ASSIGN #ERR-MSG := 'MORE THAN 30 BENEFICIARIES'
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REPORT
                    sub_Write_Error_Report();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Error_Cnt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #ERROR-CNT
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pdaBena971.getBena971_Pnd_Nmbr_Of_Benes().nadd(1);                                                                                                        //Natural: ADD 1 TO BENA971.#NMBR-OF-BENES
                pnd_A.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #A
                pdaBena971.getBena971_Pnd_Bene_Name1().getValue(pnd_A).setValue(pnd_Input_Pnd_Bene_Name1);                                                                //Natural: ASSIGN BENA971.#BENE-NAME1 ( #A ) := #INPUT.#BENE-NAME1
                pdaBena971.getBena971_Pnd_Bene_Name2().getValue(pnd_A).setValue(pnd_Input_Pnd_Bene_Name2);                                                                //Natural: ASSIGN BENA971.#BENE-NAME2 ( #A ) := #INPUT.#BENE-NAME2
                pdaBena971.getBena971_Pnd_Bene_Type().getValue(pnd_A).setValue(pnd_Input_Pnd_Bene_Type);                                                                  //Natural: ASSIGN BENA971.#BENE-TYPE ( #A ) := #INPUT.#BENE-TYPE
                pdaBena971.getBena971_Pnd_Dte_Birth_Trust().getValue(pnd_A).setValue(pnd_Input_Pnd_Dte_Birth_Trust);                                                      //Natural: ASSIGN BENA971.#DTE-BIRTH-TRUST ( #A ) := #INPUT.#DTE-BIRTH-TRUST
                pdaBena971.getBena971_Pnd_Rtln_Cde().getValue(pnd_A).setValue(pnd_Input_Pnd_Rtln_Cde);                                                                    //Natural: ASSIGN BENA971.#RTLN-CDE ( #A ) := #INPUT.#RTLN-CDE
                pdaBena971.getBena971_Pnd_Ss_Cd().getValue(pnd_A).setValue(pnd_Input_Pnd_Ss_Cd);                                                                          //Natural: ASSIGN BENA971.#SS-CD ( #A ) := #INPUT.#SS-CD
                pdaBena971.getBena971_Pnd_Prctge_Frctn_Ind().getValue(pnd_A).setValue("P");                                                                               //Natural: ASSIGN BENA971.#PRCTGE-FRCTN-IND ( #A ) := 'P'
                pdaBena971.getBena971_Pnd_Ss_Nbr().getValue(pnd_A).setValue(pnd_Input_Pnd_Ss_Nbr);                                                                        //Natural: ASSIGN BENA971.#SS-NBR ( #A ) := #INPUT.#SS-NBR
                pdaBena971.getBena971_Pnd_Irrvcbl_Ind().getValue(pnd_A).setValue(pnd_Input_Pnd_Irrvcbl_Ind);                                                              //Natural: ASSIGN BENA971.#IRRVCBL-IND ( #A ) := #INPUT.#IRRVCBL-IND
                if (condition(DbsUtil.is(pnd_Input_Pnd_Prctge.getText(),"N6")))                                                                                           //Natural: IF #INPUT.#PRCTGE IS ( N6 )
                {
                    pdaBena971.getBena971_Pnd_Prctge().getValue(pnd_A).compute(new ComputeParameters(false, pdaBena971.getBena971_Pnd_Prctge().getValue(pnd_A)),          //Natural: ASSIGN BENA971.#PRCTGE ( #A ) := VAL ( #INPUT.#PRCTGE )
                        pnd_Input_Pnd_Prctge.val());
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*                                                    /* 03/07/2013 STARTS
            if (condition(pnd_Input_Pnd_Address3.equals(" ") && pnd_Input_Pnd_Address4.notEquals(" ")))                                                                   //Natural: IF #INPUT.#ADDRESS3 = ' ' AND #INPUT.#ADDRESS4 NE ' '
            {
                pnd_Input_Pnd_Address3.setValue(DbsUtil.compress(pnd_Input_Pnd_Address3, pnd_Input_Pnd_Address4));                                                        //Natural: COMPRESS #INPUT.#ADDRESS3 #INPUT.#ADDRESS4 INTO #INPUT.#ADDRESS3
                pnd_Input_Pnd_Address4.reset();                                                                                                                           //Natural: RESET #INPUT.#ADDRESS4
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Pnd_Address2.equals(" ") && pnd_Input_Pnd_Address3.notEquals(" ")))                                                                   //Natural: IF #INPUT.#ADDRESS2 = ' ' AND #INPUT.#ADDRESS3 NE ' '
            {
                pnd_Input_Pnd_Address2.setValue(DbsUtil.compress(pnd_Input_Pnd_Address2, pnd_Input_Pnd_Address3));                                                        //Natural: COMPRESS #INPUT.#ADDRESS2 #INPUT.#ADDRESS3 INTO #INPUT.#ADDRESS2
                pnd_Input_Pnd_Address3.reset();                                                                                                                           //Natural: RESET #INPUT.#ADDRESS3
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Pnd_Address1.notEquals(" ") && pnd_Input_Pnd_Address2.notEquals(" ") && pnd_Input_Pnd_Address3.notEquals(" ")))                       //Natural: IF #INPUT.#ADDRESS1 NE ' ' AND #INPUT.#ADDRESS2 NE ' ' AND #INPUT.#ADDRESS3 NE ' '
            {
                pnd_Input_Pnd_Address1.setValue(DbsUtil.compress(pnd_Input_Pnd_Address1, pnd_Input_Pnd_Address2));                                                        //Natural: COMPRESS #INPUT.#ADDRESS1 #INPUT.#ADDRESS2 INTO #INPUT.#ADDRESS1
                pnd_Input_Pnd_Address2.setValue(DbsUtil.compress(pnd_Input_Pnd_Address3, pnd_Input_Pnd_Address4));                                                        //Natural: COMPRESS #INPUT.#ADDRESS3 #INPUT.#ADDRESS4 INTO #INPUT.#ADDRESS2
            }                                                                                                                                                             //Natural: END-IF
            pdaBena971.getBena971_Pnd_Addr1().getValue(pnd_A).setValue(pnd_Input_Pnd_Address1);                                                                           //Natural: ASSIGN BENA971.#ADDR1 ( #A ) := #INPUT.#ADDRESS1
            pdaBena971.getBena971_Pnd_Addr2().getValue(pnd_A).setValue(pnd_Input_Pnd_Address2);                                                                           //Natural: ASSIGN BENA971.#ADDR2 ( #A ) := #INPUT.#ADDRESS2
            pdaBena971.getBena971_Pnd_Addr3_City().getValue(pnd_A).setValue(pnd_Input_Pnd_City);                                                                          //Natural: ASSIGN BENA971.#ADDR3-CITY ( #A ) := #INPUT.#CITY
            pdaBena971.getBena971_Pnd_State().getValue(pnd_A).setValue(pnd_Input_Pnd_State);                                                                              //Natural: ASSIGN BENA971.#STATE ( #A ) := #INPUT.#STATE
            pdaBena971.getBena971_Pnd_Zip().getValue(pnd_A).setValue(pnd_Input_Pnd_Zip);                                                                                  //Natural: ASSIGN BENA971.#ZIP ( #A ) := #INPUT.#ZIP
            pdaBena971.getBena971_Pnd_Country().getValue(pnd_A).setValue(pnd_Input_Pnd_Country);                                                                          //Natural: ASSIGN BENA971.#COUNTRY ( #A ) := #INPUT.#COUNTRY
            pdaBena971.getBena971_Pnd_Phone().getValue(pnd_A).setValue(pnd_Input_Pnd_Phone_Number);                                                                       //Natural: ASSIGN BENA971.#PHONE ( #A ) := #INPUT.#PHONE-NUMBER
            pdaBena971.getBena971_Pnd_Gender().getValue(pnd_A).setValue(pnd_Input_Pnd_Gender);                                                                            //Natural: ASSIGN BENA971.#GENDER ( #A ) := #INPUT.#GENDER
            //*                                                     /* 03/07/2013 ENDS
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_Mdm_Found.getBoolean()))                                                                                                                        //Natural: IF #MDM-FOUND
        {
                                                                                                                                                                          //Natural: PERFORM CALL-BENN971
            sub_Call_Benn971();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  CLOSE MDM
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        getReports().write(0, ReportOption.NOTITLE,NEWLINE);                                                                                                              //Natural: WRITE ( 0 ) NOTITLE /
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE,"Total Input Records         :",pnd_Input_Cnt, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));                                  //Natural: WRITE ( 0 ) 'Total Input Records         :' #INPUT-CNT
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE,"Total Input Records Ignored :",pnd_Input_Ignore, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));                               //Natural: WRITE ( 0 ) 'Total Input Records Ignored :' #INPUT-IGNORE
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE,"Total Input Contracts       :",pnd_Input_Cntrct, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));                               //Natural: WRITE ( 0 ) 'Total Input Contracts       :' #INPUT-CNTRCT
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE,"Total Interface Contracts   :",pnd_Total_Interface, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));                            //Natural: WRITE ( 0 ) 'Total Interface Contracts   :' #TOTAL-INTERFACE
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE,"Total Contracts With Error  :",pnd_Error_Cnt, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));                                  //Natural: WRITE ( 0 ) 'Total Contracts With Error  :' #ERROR-CNT
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Total Input Records         :",pnd_Input_Cnt, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));                                  //Natural: WRITE ( 1 ) 'Total Input Records         :' #INPUT-CNT
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Total Input Records Ignored :",pnd_Input_Ignore, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));                               //Natural: WRITE ( 1 ) 'Total Input Records Ignored :' #INPUT-IGNORE
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Total Input Contracts       :",pnd_Input_Cntrct, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));                               //Natural: WRITE ( 1 ) 'Total Input Contracts       :' #INPUT-CNTRCT
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Total Interface Contracts   :",pnd_Total_Interface, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));                            //Natural: WRITE ( 1 ) 'Total Interface Contracts   :' #TOTAL-INTERFACE
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Total Contracts With Error  :",pnd_Error_Cnt, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));                                  //Natural: WRITE ( 1 ) 'Total Contracts With Error  :' #ERROR-CNT
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"Total Interface Contracts   :",pnd_Total_Interface, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));                            //Natural: WRITE ( 2 ) 'Total Interface Contracts   :' #TOTAL-INTERFACE
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,"Total Contracts With Error  :",pnd_Error_Cnt, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));                                  //Natural: WRITE ( 3 ) 'Total Contracts With Error  :' #ERROR-CNT
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //* ***********************************************************************
        //* ***********************************************************************
        //* ***********************************************************************
        //* ***********************************************************************
        //* ***********************************************************************
        //* ***********************************************************************
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 3 )
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-MDMN211A
    }
    private void sub_Call_Benn971() throws Exception                                                                                                                      //Natural: CALL-BENN971
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
                                                                                                                                                                          //Natural: PERFORM EDIT-DATA
        sub_Edit_Data();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Error_Found.getBoolean()))                                                                                                                      //Natural: IF #ERROR-FOUND
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM DELETE-EXISTING-INTERFACE
        sub_Delete_Existing_Interface();
        if (condition(Global.isEscape())) {return;}
        DbsUtil.callnat(Benn971.class , getCurrentProcessState(), pdaBena971.getBena971());                                                                               //Natural: CALLNAT 'BENN971' BENA971
        if (condition(Global.isEscape())) return;
        if (condition(pdaBena971.getBena971_Pnd_Return_Code().equals("E")))                                                                                               //Natural: IF BENA971.#RETURN-CODE = 'E'
        {
            pnd_Error_Cntrct.setValue(pdaBena971.getBena971_Pnd_Tiaa_Cntrct());                                                                                           //Natural: ASSIGN #ERROR-CNTRCT := BENA971.#TIAA-CNTRCT
            pnd_Err_Msg.setValue(DbsUtil.compress("BENN970:", pdaBena971.getBena971_Pnd_Error_Msg()));                                                                    //Natural: COMPRESS 'BENN970:' BENA971.#ERROR-MSG INTO #ERR-MSG
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REPORT
            sub_Write_Error_Report();
            if (condition(Global.isEscape())) {return;}
            pnd_Error_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #ERROR-CNT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Total_Interface.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #TOTAL-INTERFACE
            getReports().display(2, ReportOption.NOTITLE,"Count ",                                                                                                        //Natural: DISPLAY ( 2 ) NOTITLE 'Count ' #TOTAL-INTERFACE 'PIN' BENA971.#PIN 'Cntrct' BENA971.#TIAA-CNTRCT 'Bene Name1' BENA971.#BENE-NAME1 ( 1:#A ) 'Bene Name2' BENA971.#BENE-NAME2 ( 1:#A ) 'Typ' BENA971.#BENE-TYPE ( 1:#A ) 'Dob' BENA971.#DTE-BIRTH-TRUST ( 1:#A ) 'Pct' BENA971.#PRCTGE ( 1:#A )
            		pnd_Total_Interface,"PIN",
            		pdaBena971.getBena971_Pnd_Pin(),"Cntrct",
            		pdaBena971.getBena971_Pnd_Tiaa_Cntrct(),"Bene Name1",
            		pdaBena971.getBena971_Pnd_Bene_Name1().getValue(1,":",pnd_A),"Bene Name2",
            		pdaBena971.getBena971_Pnd_Bene_Name2().getValue(1,":",pnd_A),"Typ",
            		pdaBena971.getBena971_Pnd_Bene_Type().getValue(1,":",pnd_A),"Dob",
            		pdaBena971.getBena971_Pnd_Dte_Birth_Trust().getValue(1,":",pnd_A),"Pct",
            		pdaBena971.getBena971_Pnd_Prctge().getValue(1,":",pnd_A));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  CALL-BENN971
    }
    private void sub_Check_If_Existing() throws Exception                                                                                                                 //Natural: CHECK-IF-EXISTING
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Existing.reset();                                                                                                                                             //Natural: RESET #EXISTING
        vw_bc.startDatabaseFind                                                                                                                                           //Natural: FIND ( 1 ) BC WITH BC-TIAA-CNTRCT = BENA971.#TIAA-CNTRCT
        (
        "FIND01",
        new Wc[] { new Wc("BC_TIAA_CNTRCT", "=", pdaBena971.getBena971_Pnd_Tiaa_Cntrct(), WcType.WITH) },
        1
        );
        FIND01:
        while (condition(vw_bc.readNextRow("FIND01", true)))
        {
            vw_bc.setIfNotFoundControlFlag(false);
            if (condition(vw_bc.getAstCOUNTER().equals(0)))                                                                                                               //Natural: IF NO RECORDS FOUND
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Existing.setValue(true);                                                                                                                                  //Natural: ASSIGN #EXISTING := TRUE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  CHECK-IF-EXISTING
    }
    private void sub_Delete_Existing_Interface() throws Exception                                                                                                         //Natural: DELETE-EXISTING-INTERFACE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Intrfce_Super_Pnd_Int_Cntrct.setValue(pdaBena971.getBena971_Pnd_Tiaa_Cntrct());                                                                               //Natural: ASSIGN #INT-CNTRCT := BENA971.#TIAA-CNTRCT
        vw_bci.startDatabaseRead                                                                                                                                          //Natural: READ BCI BY INTRFCE-SUPER-4 STARTING FROM #INTRFCE-SUPER
        (
        "READ02",
        new Wc[] { new Wc("INTRFCE_SUPER_4", ">=", pnd_Intrfce_Super, WcType.BY) },
        new Oc[] { new Oc("INTRFCE_SUPER_4", "ASC") }
        );
        READ02:
        while (condition(vw_bci.readNextRow("READ02")))
        {
            if (condition(bci_Pin_Tiaa_Cntrct.notEquals(pnd_Intrfce_Super.getSubstring(1,22))))                                                                           //Natural: IF BCI.PIN-TIAA-CNTRCT NE SUBSTR ( #INTRFCE-SUPER,1,22 )
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(0, ReportOption.NOTITLE,"DELETING RECORD FROM BENE CONTRACT INT:",bci_Pin_Tiaa_Cntrct,"WITH STATUS",bci_Intrfce_Stts);                     //Natural: WRITE ( 0 ) NOTITLE 'DELETING RECORD FROM BENE CONTRACT INT:' BCI.PIN-TIAA-CNTRCT 'WITH STATUS' BCI.INTRFCE-STTS
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            vw_bci.deleteDBRow("READ02");                                                                                                                                 //Natural: DELETE
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        vw_bdi.startDatabaseRead                                                                                                                                          //Natural: READ BDI BY INTRFCE-SUPER-5 STARTING FROM #INTRFCE-SUPER
        (
        "READ03",
        new Wc[] { new Wc("INTRFCE_SUPER_5", ">=", pnd_Intrfce_Super, WcType.BY) },
        new Oc[] { new Oc("INTRFCE_SUPER_5", "ASC") }
        );
        READ03:
        while (condition(vw_bdi.readNextRow("READ03")))
        {
            if (condition(bdi_Pin_Tiaa_Cntrct.notEquals(pnd_Intrfce_Super.getSubstring(1,22))))                                                                           //Natural: IF BDI.PIN-TIAA-CNTRCT NE SUBSTR ( #INTRFCE-SUPER,1,22 )
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(0, ReportOption.NOTITLE,"DELETING RECORD FROM BENE DESIGNTN INT:",bdi_Pin_Tiaa_Cntrct,"WITH STATUS",bdi_Intrfce_Stts);                     //Natural: WRITE ( 0 ) NOTITLE 'DELETING RECORD FROM BENE DESIGNTN INT:' BDI.PIN-TIAA-CNTRCT 'WITH STATUS' BDI.INTRFCE-STTS
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            vw_bdi.deleteDBRow("READ03");                                                                                                                                 //Natural: DELETE
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        vw_bmi.startDatabaseRead                                                                                                                                          //Natural: READ BMI BY INTRFCE-SUPER-4 STARTING FROM #INTRFCE-SUPER
        (
        "READ04",
        new Wc[] { new Wc("INTRFCE_SUPER_4", ">=", pnd_Intrfce_Super, WcType.BY) },
        new Oc[] { new Oc("INTRFCE_SUPER_4", "ASC") }
        );
        READ04:
        while (condition(vw_bmi.readNextRow("READ04")))
        {
            if (condition(bmi_Pin_Tiaa_Cntrct.notEquals(pnd_Intrfce_Super.getSubstring(1,22))))                                                                           //Natural: IF BMI.PIN-TIAA-CNTRCT NE SUBSTR ( #INTRFCE-SUPER,1,22 )
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(0, ReportOption.NOTITLE,"DELETING RECORD FROM BENE MOS INT:",bmi_Pin_Tiaa_Cntrct,"WITH STATUS",bmi_Intrfce_Stts);                          //Natural: WRITE ( 0 ) NOTITLE 'DELETING RECORD FROM BENE MOS INT:' BMI.PIN-TIAA-CNTRCT 'WITH STATUS' BMI.INTRFCE-STTS
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            vw_bmi.deleteDBRow("READ04");                                                                                                                                 //Natural: DELETE
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  DELETE-EXISTING-INTERFACE
    }
    private void sub_Edit_Data() throws Exception                                                                                                                         //Natural: EDIT-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Error_Found.reset();                                                                                                                                          //Natural: RESET #ERROR-FOUND
        //*  AT LEAST ONE SHOULD BE PRIMARY
        if (condition(pdaBena971.getBena971_Pnd_Bene_Type().getValue(1,":",pnd_A).equals("P")))                                                                           //Natural: IF BENA971.#BENE-TYPE ( 1:#A ) = 'P'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Error_Found.setValue(true);                                                                                                                               //Natural: ASSIGN #ERROR-FOUND := TRUE
            pnd_Error_Cntrct.setValue(pdaBena971.getBena971_Pnd_Tiaa_Cntrct());                                                                                           //Natural: ASSIGN #ERROR-CNTRCT := BENA971.#TIAA-CNTRCT
            pnd_Err_Msg.setValue("No Primary Beneficiary found");                                                                                                         //Natural: ASSIGN #ERR-MSG := 'No Primary Beneficiary found'
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REPORT
            sub_Write_Error_Report();
            if (condition(Global.isEscape())) {return;}
            pnd_Error_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #ERROR-CNT
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaBena971.getBena971_Pnd_New_Issuefslash_Chng_Ind().equals("N") || pdaBena971.getBena971_Pnd_New_Issuefslash_Chng_Ind().equals("C")))              //Natural: IF BENA971.#NEW-ISSUE/CHNG-IND = 'N' OR = 'C'
        {
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-EXISTING
            sub_Check_If_Existing();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Existing.getBoolean()))                                                                                                                     //Natural: IF #EXISTING
            {
                if (condition(pdaBena971.getBena971_Pnd_New_Issuefslash_Chng_Ind().equals("N")))                                                                          //Natural: IF BENA971.#NEW-ISSUE/CHNG-IND = 'N'
                {
                    pdaBena971.getBena971_Pnd_New_Issuefslash_Chng_Ind().setValue("C");                                                                                   //Natural: ASSIGN BENA971.#NEW-ISSUE/CHNG-IND := 'C'
                    //*      #ERROR-CNTRCT := BENA970.#TIAA-CNTRCT
                    //*      #ERR-MSG := 'DESIGNATION FOUND FOR A NEW DESIGNATION REQUEST'
                    //*      PERFORM WRITE-ERROR-REPORT
                    //*      ADD 1 TO #ERROR-CNT
                    //*      #ERROR-FOUND := TRUE
                    //*      ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaBena971.getBena971_Pnd_New_Issuefslash_Chng_Ind().equals("C")))                                                                          //Natural: IF BENA971.#NEW-ISSUE/CHNG-IND = 'C'
                {
                    pdaBena971.getBena971_Pnd_New_Issuefslash_Chng_Ind().setValue("N");                                                                                   //Natural: ASSIGN BENA971.#NEW-ISSUE/CHNG-IND := 'N'
                    //*      #ERROR-CNTRCT := BENL970A.#TIAA-CNTRCT
                    //*      COMPRESS 'REQUEST TO CHANGE DESIGNATION BUT NO'
                    //*        'EXISTING DESIGNATION' INTO #ERR-MSG
                    //*      PERFORM WRITE-ERROR-REPORT
                    //*      ADD 1 TO #ERROR-CNT
                    //*      #ERROR-FOUND := TRUE
                    //*      ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Error_Cntrct.setValue(pdaBena971.getBena971_Pnd_Tiaa_Cntrct());                                                                                           //Natural: ASSIGN #ERROR-CNTRCT := BENA971.#TIAA-CNTRCT
            pnd_Err_Msg.setValue("INVALID NEW ISSUE/CHNG IND");                                                                                                           //Natural: ASSIGN #ERR-MSG := 'INVALID NEW ISSUE/CHNG IND'
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REPORT
            sub_Write_Error_Report();
            if (condition(Global.isEscape())) {return;}
            pnd_Error_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #ERROR-CNT
            pnd_Error_Found.setValue(true);                                                                                                                               //Natural: ASSIGN #ERROR-FOUND := TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_A.greater(1) && pdaBena971.getBena971_Pnd_Prctge().getValue(1,":",pnd_A).equals(getZero())))                                                    //Natural: IF #A GT 1 AND BENA971.#PRCTGE ( 1:#A ) = 0
        {
            DbsUtil.examine(new ExamineSource(pdaBena971.getBena971_Pnd_Bene_Type().getValue("*")), new ExamineSearch("P"), new ExamineGivingNumber(pnd_B));              //Natural: EXAMINE BENA971.#BENE-TYPE ( * ) FOR 'P' GIVING NUMBER #B
            //*  MORE THAN ONE PRIMARY
            if (condition(pnd_B.greater(1)))                                                                                                                              //Natural: IF #B GT 1
            {
                FOR01:                                                                                                                                                    //Natural: FOR #C 1 #A
                for (pnd_C.setValue(1); condition(pnd_C.lessOrEqual(pnd_A)); pnd_C.nadd(1))
                {
                    if (condition(pdaBena971.getBena971_Pnd_Bene_Type().getValue(pnd_C).equals("P") && pdaBena971.getBena971_Pnd_Prctge().getValue(pnd_C).equals(getZero()))) //Natural: IF BENA971.#BENE-TYPE ( #C ) = 'P' AND BENA971.#PRCTGE ( #C ) = 0
                    {
                        pnd_Error_Cntrct.setValue(pdaBena971.getBena971_Pnd_Tiaa_Cntrct());                                                                               //Natural: ASSIGN #ERROR-CNTRCT := BENA971.#TIAA-CNTRCT
                        pnd_Err_Msg.setValue("ZERO ALLOCATION FOR PRIMARY BENEFICIARY");                                                                                  //Natural: ASSIGN #ERR-MSG := 'ZERO ALLOCATION FOR PRIMARY BENEFICIARY'
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REPORT
                        sub_Write_Error_Report();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Error_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #ERROR-CNT
                        pnd_Error_Found.setValue(true);                                                                                                                   //Natural: ASSIGN #ERROR-FOUND := TRUE
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.examine(new ExamineSource(pdaBena971.getBena971_Pnd_Bene_Type().getValue("*")), new ExamineSearch("C"), new ExamineGivingNumber(pnd_B));              //Natural: EXAMINE BENA971.#BENE-TYPE ( * ) FOR 'C' GIVING NUMBER #B
            //*  MORE THAN ONE CONTINGENT
            if (condition(pnd_B.greater(1)))                                                                                                                              //Natural: IF #B GT 1
            {
                FOR02:                                                                                                                                                    //Natural: FOR #C 1 #A
                for (pnd_C.setValue(1); condition(pnd_C.lessOrEqual(pnd_A)); pnd_C.nadd(1))
                {
                    if (condition(pdaBena971.getBena971_Pnd_Bene_Type().getValue(pnd_C).equals("C") && pdaBena971.getBena971_Pnd_Prctge().getValue(pnd_C).equals(getZero()))) //Natural: IF BENA971.#BENE-TYPE ( #C ) = 'C' AND BENA971.#PRCTGE ( #C ) = 0
                    {
                        pnd_Error_Cntrct.setValue(pdaBena971.getBena971_Pnd_Tiaa_Cntrct());                                                                               //Natural: ASSIGN #ERROR-CNTRCT := BENA971.#TIAA-CNTRCT
                        pnd_Err_Msg.setValue("ZERO ALLOCATION FOR CONTINGENT BENEFICIARY");                                                                               //Natural: ASSIGN #ERR-MSG := 'ZERO ALLOCATION FOR CONTINGENT BENEFICIARY'
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REPORT
                        sub_Write_Error_Report();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Error_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #ERROR-CNT
                        pnd_Error_Found.setValue(true);                                                                                                                   //Natural: ASSIGN #ERROR-FOUND := TRUE
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Total_Alloc_P.reset();                                                                                                                                        //Natural: RESET #TOTAL-ALLOC-P #TOTAL-ALLOC-C #SS-NUM
        pnd_Total_Alloc_C.reset();
        pnd_Ss_Num.reset();
        FOR03:                                                                                                                                                            //Natural: FOR #B 1 #A
        for (pnd_B.setValue(1); condition(pnd_B.lessOrEqual(pnd_A)); pnd_B.nadd(1))
        {
            if (condition(pdaBena971.getBena971_Pnd_Bene_Type().getValue(pnd_B).equals("P")))                                                                             //Natural: IF BENA971.#BENE-TYPE ( #B ) = 'P'
            {
                pnd_Total_Alloc_P.nadd(pdaBena971.getBena971_Pnd_Prctge().getValue(pnd_B));                                                                               //Natural: ADD BENA971.#PRCTGE ( #B ) TO #TOTAL-ALLOC-P
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Total_Alloc_C.nadd(pdaBena971.getBena971_Pnd_Prctge().getValue(pnd_B));                                                                               //Natural: ADD BENA971.#PRCTGE ( #B ) TO #TOTAL-ALLOC-C
            }                                                                                                                                                             //Natural: END-IF
            if (condition(DbsUtil.is(pdaBena971.getBena971_Pnd_Ss_Nbr().getValue(pnd_B).getText(),"N9")))                                                                 //Natural: IF BENA971.#SS-NBR ( #B ) IS ( N9 )
            {
                pnd_Ss_Num.compute(new ComputeParameters(false, pnd_Ss_Num), pdaBena971.getBena971_Pnd_Ss_Nbr().getValue(pnd_B).val());                                   //Natural: ASSIGN #SS-NUM := VAL ( BENA971.#SS-NBR ( #B ) )
            }                                                                                                                                                             //Natural: END-IF
            if (condition((pdaBena971.getBena971_Pnd_Ss_Cd().getValue(pnd_B).notEquals(" ") && ((pdaBena971.getBena971_Pnd_Ss_Cd().getValue(pnd_B).equals("I")            //Natural: IF BENA971.#SS-CD ( #B ) NE ' ' AND ( BENA971.#SS-CD ( #B ) = 'I' OR = 'S' OR = 'T' )
                || pdaBena971.getBena971_Pnd_Ss_Cd().getValue(pnd_B).equals("S")) || pdaBena971.getBena971_Pnd_Ss_Cd().getValue(pnd_B).equals("T")))))
            {
                if (condition(pnd_Ss_Num.equals(getZero())))                                                                                                              //Natural: IF #SS-NUM = 0
                {
                    pdaBena971.getBena971_Pnd_Ss_Nbr().getValue(pnd_B).setValue("999999999");                                                                             //Natural: ASSIGN BENA971.#SS-NBR ( #B ) := '999999999'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaBena971.getBena971_Pnd_Ss_Cd().getValue(pnd_B).equals(" ")))                                                                             //Natural: IF BENA971.#SS-CD ( #B ) = ' '
                {
                    pdaBena971.getBena971_Pnd_Ss_Cd().getValue(pnd_B).setValue("S");                                                                                      //Natural: ASSIGN BENA971.#SS-CD ( #B ) := 'S'
                    if (condition(pnd_Ss_Num.equals(getZero())))                                                                                                          //Natural: IF #SS-NUM = 0
                    {
                        pdaBena971.getBena971_Pnd_Ss_Nbr().getValue(pnd_B).setValue("999999999");                                                                         //Natural: ASSIGN BENA971.#SS-NBR ( #B ) := '999999999'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Error_Cntrct.setValue(pdaBena971.getBena971_Pnd_Tiaa_Cntrct());                                                                                   //Natural: ASSIGN #ERROR-CNTRCT := BENA971.#TIAA-CNTRCT
                    pnd_Err_Msg.setValue("INVALID SS CD");                                                                                                                //Natural: ASSIGN #ERR-MSG := 'INVALID SS CD'
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REPORT
                    sub_Write_Error_Report();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Error_Cnt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #ERROR-CNT
                    pnd_Error_Found.setValue(true);                                                                                                                       //Natural: ASSIGN #ERROR-FOUND := TRUE
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Total_Alloc_P.greater(100)))                                                                                                                    //Natural: IF #TOTAL-ALLOC-P GT 100
        {
            pnd_Error_Cntrct.setValue(pdaBena971.getBena971_Pnd_Tiaa_Cntrct());                                                                                           //Natural: ASSIGN #ERROR-CNTRCT := BENA971.#TIAA-CNTRCT
            pnd_Err_Msg.setValue("TOTAL ALLOCATION FOR PRIMARY BENEFICIARIES EXCEEDS 100%");                                                                              //Natural: ASSIGN #ERR-MSG := 'TOTAL ALLOCATION FOR PRIMARY BENEFICIARIES EXCEEDS 100%'
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REPORT
            sub_Write_Error_Report();
            if (condition(Global.isEscape())) {return;}
            pnd_Error_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #ERROR-CNT
            pnd_Error_Found.setValue(true);                                                                                                                               //Natural: ASSIGN #ERROR-FOUND := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Total_Alloc_C.greater(100)))                                                                                                                    //Natural: IF #TOTAL-ALLOC-C GT 100
        {
            pnd_Error_Cntrct.setValue(pdaBena971.getBena971_Pnd_Tiaa_Cntrct());                                                                                           //Natural: ASSIGN #ERROR-CNTRCT := BENA971.#TIAA-CNTRCT
            pnd_Err_Msg.setValue(DbsUtil.compress("TOTAL ALLOCATION FOR CONTINGENT BENEFICIARIES EXCEEDS", "100%"));                                                      //Natural: COMPRESS 'TOTAL ALLOCATION FOR CONTINGENT BENEFICIARIES EXCEEDS' '100%' INTO #ERR-MSG
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REPORT
            sub_Write_Error_Report();
            if (condition(Global.isEscape())) {return;}
            pnd_Error_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #ERROR-CNT
            pnd_Error_Found.setValue(true);                                                                                                                               //Natural: ASSIGN #ERROR-FOUND := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT-DATA
    }
    private void sub_Move_Contract_Fields() throws Exception                                                                                                              //Natural: MOVE-CONTRACT-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  PERFORM READ-PROCOBOL
                                                                                                                                                                          //Natural: PERFORM READ-MDMN211A
        sub_Read_Mdmn211a();
        if (condition(Global.isEscape())) {return;}
        if (condition(! (pnd_Mdm_Found.getBoolean())))                                                                                                                    //Natural: IF NOT #MDM-FOUND
        {
            pnd_Error_Cntrct.setValue(pnd_Input_Pnd_Tiaa_Cntrct);                                                                                                         //Natural: ASSIGN #ERROR-CNTRCT := #INPUT.#TIAA-CNTRCT
            pnd_Err_Msg.setValue(DbsUtil.compress("CONTRACT NOT FOUND IN MDM FOR PIN =", pdaMdma211.getPnd_Mdma211_Pnd_O_Pin_N12()));                                     //Natural: COMPRESS 'CONTRACT NOT FOUND IN MDM FOR PIN =' #MDMA211.#O-PIN-N12 INTO #ERR-MSG
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REPORT
            sub_Write_Error_Report();
            if (condition(Global.isEscape())) {return;}
            pnd_Error_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #ERROR-CNT
            pdaBena971.getBena971_Pnd_Tiaa_Cntrct().setValue(pnd_Input_Pnd_Tiaa_Cntrct);                                                                                  //Natural: ASSIGN BENA971.#TIAA-CNTRCT := #INPUT.#TIAA-CNTRCT
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
            //*  PH-UNIQUE-ID-NBR
            //*  INSURANCE HAS NO CREF #
        }                                                                                                                                                                 //Natural: END-IF
        pdaBena971.getBena971_Pnd_Intrfce_Bsnss_Dte().setValue(pnd_Input_Pnd_Intrfce_Bsnss_Dte);                                                                          //Natural: ASSIGN BENA971.#INTRFCE-BSNSS-DTE := #INPUT.#INTRFCE-BSNSS-DTE
        pdaBena971.getBena971_Pnd_Intrfcng_Systm().setValue("PA");                                                                                                        //Natural: ASSIGN BENA971.#INTRFCNG-SYSTM := 'PA'
        pdaBena971.getBena971_Pnd_Rqstng_System().setValue("PA");                                                                                                         //Natural: ASSIGN BENA971.#RQSTNG-SYSTEM := 'PA'
        pdaBena971.getBena971_Pnd_New_Issuefslash_Chng_Ind().setValue(pnd_Input_Pnd_New_Issuefslash_Chng_Ind);                                                            //Natural: ASSIGN BENA971.#NEW-ISSUE/CHNG-IND := #INPUT.#NEW-ISSUE/CHNG-IND
        pdaBena971.getBena971_Pnd_Intrfce_Mgrtn_Ind().setValue("I");                                                                                                      //Natural: ASSIGN BENA971.#INTRFCE-MGRTN-IND := 'I'
        pdaBena971.getBena971_Pnd_Pin().setValue(pnd_Intrfce_Super_Pnd_Int_Pin);                                                                                          //Natural: ASSIGN BENA971.#PIN := #INT-PIN
        pdaBena971.getBena971_Pnd_Tiaa_Cntrct().setValue(pnd_Input_Pnd_Tiaa_Cntrct);                                                                                      //Natural: ASSIGN BENA971.#TIAA-CNTRCT := #INPUT.#TIAA-CNTRCT
        pdaBena971.getBena971_Pnd_Cref_Cntrct().setValue(" ");                                                                                                            //Natural: ASSIGN BENA971.#CREF-CNTRCT := ' '
        pdaBena971.getBena971_Pnd_Cntrct_Type().setValue("N");                                                                                                            //Natural: ASSIGN BENA971.#CNTRCT-TYPE := 'N'
        pdaBena971.getBena971_Pnd_Effctve_Dte().setValue(pnd_Input_Pnd_Effctve_Dte);                                                                                      //Natural: ASSIGN BENA971.#EFFCTVE-DTE := #INPUT.#EFFCTVE-DTE
        pdaBena971.getBena971_Pnd_Exempt_Spouse_Rights().setValue(pnd_Input_Pnd_Exempt_Spouse_Rights);                                                                    //Natural: ASSIGN BENA971.#EXEMPT-SPOUSE-RIGHTS := #INPUT.#EXEMPT-SPOUSE-RIGHTS
        pdaBena971.getBena971_Pnd_Spouse_Waived_Bnfts().setValue(pnd_Input_Pnd_Spouse_Waived_Bnfts);                                                                      //Natural: ASSIGN BENA971.#SPOUSE-WAIVED-BNFTS := #INPUT.#SPOUSE-WAIVED-BNFTS
        pdaBena971.getBena971_Pnd_Pymnt_Chld_Dcsd_Ind().setValue(pnd_Input_Pnd_Pymnt_Chld_Dcsd_Ind);                                                                      //Natural: ASSIGN BENA971.#PYMNT-CHLD-DCSD-IND := #INPUT.#PYMNT-CHLD-DCSD-IND
        //*  MOVE-CONTRACT-FIELDS
    }
    private void sub_Write_Error_Report() throws Exception                                                                                                                //Natural: WRITE-ERROR-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().display(3, ReportOption.NOTITLE,"Contract",                                                                                                          //Natural: DISPLAY ( 3 ) NOTITLE 'Contract' #ERROR-CNTRCT 'Error Message' #ERR-MSG
        		pnd_Error_Cntrct,"Error Message",
        		pnd_Err_Msg);
        if (Global.isEscape()) return;
        //*  WRITE-ERROR-REPORT
    }
    private void sub_Read_Mdmn211a() throws Exception                                                                                                                     //Natural: READ-MDMN211A
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pdaMdma211.getPnd_Mdma211().reset();                                                                                                                              //Natural: RESET #MDMA211 #MDMA101
        pdaMdma101.getPnd_Mdma101().reset();
        pnd_Intrfce_Super.reset();                                                                                                                                        //Natural: RESET #INTRFCE-SUPER
        pnd_Mdm_Found.reset();                                                                                                                                            //Natural: RESET #MDM-FOUND
        pdaMdma211.getPnd_Mdma211_Pnd_I_Tiaa_Cref_Ind().setValue("T");                                                                                                    //Natural: ASSIGN #MDMA211.#I-TIAA-CREF-IND := 'T'
        pdaMdma211.getPnd_Mdma211_Pnd_I_Contract_Number().setValue(pnd_Input_Pnd_Tiaa_Cntrct);                                                                            //Natural: ASSIGN #MDMA211.#I-CONTRACT-NUMBER := #INPUT.#TIAA-CNTRCT
        pdaMdma211.getPnd_Mdma211_Pnd_I_Payee_Code_A2().setValue(" ");                                                                                                    //Natural: ASSIGN #MDMA211.#I-PAYEE-CODE-A2 := ' '
        pdaMdma211.getPnd_Mdma211_Pnd_I_Cntrct_Role_Fltr().setValue("ACTIVE");                                                                                            //Natural: ASSIGN #MDMA211.#I-CNTRCT-ROLE-FLTR := 'ACTIVE'
        DbsUtil.callnat(Mdmn211a.class , getCurrentProcessState(), pdaMdma211.getPnd_Mdma211());                                                                          //Natural: CALLNAT 'MDMN211A' #MDMA211
        if (condition(Global.isEscape())) return;
        if (condition(pdaMdma211.getPnd_Mdma211_Pnd_O_Return_Code().equals("0000") && pdaMdma211.getPnd_Mdma211_Pnd_O_Contract_Number().equals(pnd_Input_Pnd_Tiaa_Cntrct))) //Natural: IF #MDMA211.#O-RETURN-CODE = '0000' AND #MDMA211.#O-CONTRACT-NUMBER = #INPUT.#TIAA-CNTRCT
        {
            pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_N12().setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Pin_N12());                                                                //Natural: ASSIGN #MDMA101.#I-PIN-N12 := #MDMA211.#O-PIN-N12
            pdaMdma101.getPnd_Mdma101_Pnd_I_Party_Fltr().setValue("ACTIVE");                                                                                              //Natural: ASSIGN #MDMA101.#I-PARTY-FLTR := 'ACTIVE'
            DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                      //Natural: CALLNAT 'MDMN101A' #MDMA101
            if (condition(Global.isEscape())) return;
            if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code().equals("0000") && pdaMdma101.getPnd_Mdma101_Pnd_O_Pin_N12().equals(pdaMdma211.getPnd_Mdma211_Pnd_O_Pin_N12()))) //Natural: IF #MDMA101.#O-RETURN-CODE = '0000' AND #MDMA101.#O-PIN-N12 = #MDMA211.#O-PIN-N12
            {
                pnd_Intrfce_Super_Pnd_Int_Pin.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Pin_N12());                                                                        //Natural: ASSIGN #INT-PIN := #MDMA101.#O-PIN-N12
                pnd_Mdm_Found.setValue(true);                                                                                                                             //Natural: ASSIGN #MDM-FOUND := TRUE
                //*  ACTIVE PAYEE IN MDM FOR CONTRACT
                getReports().display(0, ReportOption.NOTITLE,"C#",                                                                                                        //Natural: DISPLAY ( 0 ) NOTITLE 'C#' #INPUT.#TIAA-CNTRCT 'LOB' #MDMA211.#O-LINE-OF-BUSINESS 'PLAT' #MDMA211.#O-PLAN-CODE 'PIN' #MDMA211.#O-PIN-N12 'PY' #MDMA211.#O-PAYEE-CODE 'SSN' #MDMA211.#O-SSN
                		pnd_Input_Pnd_Tiaa_Cntrct,"LOB",
                		pdaMdma211.getPnd_Mdma211_Pnd_O_Line_Of_Business(),"PLAT",
                		pdaMdma211.getPnd_Mdma211_Pnd_O_Plan_Code(),"PIN",
                		pdaMdma211.getPnd_Mdma211_Pnd_O_Pin_N12(),"PY",
                		pdaMdma211.getPnd_Mdma211_Pnd_O_Payee_Code(),"SSN",
                		pdaMdma211.getPnd_Mdma211_Pnd_O_Ssn());
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, ReportOption.NOTITLE,"MDM PIN NOT FOUND FOR CONTRACT =",pnd_Input_Pnd_Tiaa_Cntrct,"MDMA211 PIN =",pdaMdma211.getPnd_Mdma211_Pnd_O_Pin_N12()); //Natural: WRITE ( 0 ) 'MDM PIN NOT FOUND FOR CONTRACT =' #INPUT.#TIAA-CNTRCT 'MDMA211 PIN =' #MDMA211.#O-PIN-N12
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, ReportOption.NOTITLE,"MDM PIN NOT FOUND FOR CONTRACT =",pnd_Input_Pnd_Tiaa_Cntrct);                                                     //Natural: WRITE ( 0 ) 'MDM PIN NOT FOUND FOR CONTRACT =' #INPUT.#TIAA-CNTRCT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  READ-MDMN211A
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(30),"        McCamish Feed Record Dump         ",new  //Natural: WRITE ( 1 ) NOTITLE 001T *PROGRAM 030T '        McCamish Feed Record Dump         ' 120T 'Page:' *PAGE-NUMBER ( 1 ) /
                        TabSetting(120),"Page:",getReports().getPageNumberDbs(1),NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(30)," Successful McCamish Feed to Beneficiary  ",new  //Natural: WRITE ( 2 ) NOTITLE 001T *PROGRAM 030T ' Successful McCamish Feed to Beneficiary  ' 120T 'Page:' *PAGE-NUMBER ( 2 ) /
                        TabSetting(120),"Page:",getReports().getPageNumberDbs(2),NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(3, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(30),"        McCamish Feed Error Report        ",new  //Natural: WRITE ( 3 ) NOTITLE 001T *PROGRAM 030T '        McCamish Feed Error Report        ' 120T 'Page:' *PAGE-NUMBER ( 3 ) /
                        TabSetting(120),"Page:",getReports().getPageNumberDbs(3),NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133 SG=OFF ZP=ON");
        Global.format(1, "PS=60 LS=250 SG=OFF ZP=ON");
        Global.format(2, "PS=60 LS=133 SG=OFF ZP=ON");
        Global.format(3, "PS=60 LS=133 SG=OFF ZP=ON");

        getReports().setDisplayColumns(1, ReportOption.NOTITLE,"Int dte",
        		pnd_Input_Pnd_Intrfce_Bsnss_Dte,"Nc",
        		pnd_Input_Pnd_New_Issuefslash_Chng_Ind,"Cntrct",
        		pnd_Input_Pnd_Tiaa_Cntrct,"Eff Dte",
        		pnd_Input_Pnd_Effctve_Dte,"Xr",
        		pnd_Input_Pnd_Exempt_Spouse_Rights,"Wb",
        		pnd_Input_Pnd_Spouse_Waived_Bnfts,"Name1",
        		pnd_Input_Pnd_Bene_Name1, new AlphanumericLength (20),"Name2",
        		pnd_Input_Pnd_Bene_Name2, new AlphanumericLength (20),"Pc",
        		pnd_Input_Pnd_Bene_Type,"Dob",
        		pnd_Input_Pnd_Dte_Birth_Trust,"Rl",
        		pnd_Input_Pnd_Rtln_Cde,"Sc",
        		pnd_Input_Pnd_Ss_Cd,"Nbr",
        		pnd_Input_Pnd_Ss_Nbr,"Pct",
        		pnd_Input_Pnd_Prctge,"Dflt",
        		pnd_Input_Pnd_Dflt_To_Estate,"Mos",
        		pnd_Input_Pnd_Mos_Ind,"Vcb",
        		pnd_Input_Pnd_Irrvcbl_Ind,"Pd",
        		pnd_Input_Pnd_Pymnt_Chld_Dcsd_Ind);
        getReports().setDisplayColumns(2, ReportOption.NOTITLE,"Count ",
        		pnd_Total_Interface,"PIN",
        		pdaBena971.getBena971_Pnd_Pin(),"Cntrct",
        		pdaBena971.getBena971_Pnd_Tiaa_Cntrct(),"Bene Name1",
        		pdaBena971.getBena971_Pnd_Bene_Name1(),"Bene Name2",
        		pdaBena971.getBena971_Pnd_Bene_Name2(),"Typ",
        		pdaBena971.getBena971_Pnd_Bene_Type(),"Dob",
        		pdaBena971.getBena971_Pnd_Dte_Birth_Trust(),"Pct",
        		pdaBena971.getBena971_Pnd_Prctge());
        getReports().setDisplayColumns(3, ReportOption.NOTITLE,"Contract",
        		pnd_Error_Cntrct,"Error Message",
        		pnd_Err_Msg);
        getReports().setDisplayColumns(0, ReportOption.NOTITLE,"C#",
        		pnd_Input_Pnd_Tiaa_Cntrct,"LOB",
        		pdaMdma211.getPnd_Mdma211_Pnd_O_Line_Of_Business(),"PLAT",
        		pdaMdma211.getPnd_Mdma211_Pnd_O_Plan_Code(),"PIN",
        		pdaMdma211.getPnd_Mdma211_Pnd_O_Pin_N12(),"PY",
        		pdaMdma211.getPnd_Mdma211_Pnd_O_Payee_Code(),"SSN",
        		pdaMdma211.getPnd_Mdma211_Pnd_O_Ssn());
    }
}
