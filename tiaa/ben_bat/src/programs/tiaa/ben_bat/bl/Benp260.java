/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:14:43 PM
**        * FROM NATURAL PROGRAM : Benp260
************************************************************
**        * FILE NAME            : Benp260.java
**        * CLASS NAME           : Benp260
**        * INSTANCE NAME        : Benp260
************************************************************
************************************************************************
* PROGRAM NAME : BENP260
* DESCRIPTION  : BENEFICIARY BATCH SYSTEM - MONTHLY STATISTIC REPORT
* WRITTEN BY   : K. GREIFF
* DATE WRITTEN : 11/1999
************************************************************************
*    DATE      USERID                   DESCRIPTION
* 11/10/2000  WEBBJ     ACIS CORRECTION. LAST-DSGNTN-SYSTEM = 'X'
* 03/05/2001  FONTOUR   ADDED LINE W/ DATA ENTERED FROM WEB
* 05/31/2002  FONTOUR   REMOVE MDO WEB CODE FROM 1ST IMPLEMENTATION
* STATUS X AND Y: FORM MAIL-IN WEB -THIS FEATURE WON't be implemented,
* BUT THE PROGRAM IS VERIFYING IF THERE's a record  with status X or Y
* 11/29/2006  O SOTTO   RE-STOWED FOR BENG200 CHANGE.
* 04/01/2017  DURAND    PIN EXPANSION N7 TO N12
************************************************************************

************************************************************ */

package tiaa.ben_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Benp260 extends BLNatBase
{
    // Data Areas
    private PdaBena010 pdaBena010;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_bc;
    private DbsField bc_Bc_Pin;
    private DbsField bc_Bc_Tiaa_Cntrct;
    private DbsField bc_Bc_Tiaa_Cref_Ind;
    private DbsField bc_Bc_Stat;
    private DbsField bc_Bc_Cntrct_Type;
    private DbsField bc_Bc_Rqst_Timestamp;
    private DbsField bc_Bc_Mos_Ind;
    private DbsField bc_Bc_Irvcbl_Ind;
    private DbsField bc_Bc_Rcrd_Last_Updt_Userid;
    private DbsField bc_Bc_Last_Dsgntn_Srce;
    private DbsField bc_Bc_Last_Dsgntn_System;
    private DbsField bc_Bc_Last_Dsgntn_Userid;

    private DataAccessProgramView vw_bd;
    private DbsField bd_Bd_Pin;
    private DbsField bd_Bd_Tiaa_Cntrct;
    private DbsField bd_Bd_Tiaa_Cref_Ind;
    private DbsField bd_Bd_Stat;
    private DbsField bd_Bd_Bene_Type;
    private DbsField bd_Bd_Irvcbl_Ind;
    private DbsField bd_Bd_Dflt_Estate;

    private DataAccessProgramView vw_cor;
    private DbsField cor_Cntrct_Nbr;
    private DbsField cor_Cntrct_Lob_Cde;
    private DbsField cor_Ph_Unique_Id_Nbr;
    private DbsField cor_Cntrct_Spcl_Cnsdrtn_Cde;
    private DbsField pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq;

    private DbsGroup pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq__R_Field_1;
    private DbsField pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Stat;
    private DbsField pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Pin;
    private DbsField pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Tiaa_Cntrct;
    private DbsField pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Tiaa_Cref_Ind;
    private DbsField pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Bene_Type;
    private DbsField pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Seq_Nmbr;

    private DbsGroup pnd_Counters;
    private DbsField pnd_Counters_Pnd_Total_Contracts;
    private DbsField pnd_Counters_Pnd_Total_Bene;
    private DbsField pnd_Counters_Pnd_Total_Mos;
    private DbsField pnd_Counters_Pnd_Total_I_Bene;
    private DbsField pnd_Counters_Pnd_Total_I_Mos;
    private DbsField pnd_Counters_Pnd_Total_Bene_Sys;
    private DbsField pnd_Counters_Pnd_Total_Acis_Interface;
    private DbsField pnd_Counters_Pnd_Total_Cis_Interface;
    private DbsField pnd_Counters_Pnd_Total_Mdo_Interface;
    private DbsField pnd_Counters_Pnd_Total_Acis_Migrate;
    private DbsField pnd_Counters_Pnd_Total_Cis_Migrate;
    private DbsField pnd_Counters_Pnd_Total_Mdo_Migrate;
    private DbsField pnd_Counters_Pnd_Total_Ms_Migrate;
    private DbsField pnd_Counters_Pnd_Total_Web;
    private DbsField pnd_Counters_Pnd_Total_Dflt_Estate;
    private DbsField pnd_Counters_Pnd_Total_Pbene_Only;
    private DbsField pnd_Counters_Pnd_Total_P_C_Benes;
    private DbsField pnd_Counters_Pnd_Total_Pbene_1_2;
    private DbsField pnd_Counters_Pnd_Total_Pbene_3_5;
    private DbsField pnd_Counters_Pnd_Total_Pbene_6_10;
    private DbsField pnd_Counters_Pnd_Total_Pbene_Gt10;
    private DbsField pnd_Counters_Pnd_Total_Cbene_1_2;
    private DbsField pnd_Counters_Pnd_Total_Cbene_3_5;
    private DbsField pnd_Counters_Pnd_Total_Cbene_6_10;
    private DbsField pnd_Counters_Pnd_Total_Cbene_Gt10;
    private DbsField pnd_I;
    private DbsField pnd_Program;
    private DbsField pnd_Pbene_Count;
    private DbsField pnd_Cbene_Count;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaBena010 = new PdaBena010(localVariables);

        // Local Variables

        vw_bc = new DataAccessProgramView(new NameInfo("vw_bc", "BC"), "BENE_CONTRACT_12", "BENE_CONTRACT");
        bc_Bc_Pin = vw_bc.getRecord().newFieldInGroup("bc_Bc_Pin", "BC-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BC_PIN");
        bc_Bc_Pin.setDdmHeader("PIN");
        bc_Bc_Tiaa_Cntrct = vw_bc.getRecord().newFieldInGroup("bc_Bc_Tiaa_Cntrct", "BC-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BC_TIAA_CNTRCT");
        bc_Bc_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bc_Bc_Tiaa_Cref_Ind = vw_bc.getRecord().newFieldInGroup("bc_Bc_Tiaa_Cref_Ind", "BC-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_TIAA_CREF_IND");
        bc_Bc_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bc_Bc_Stat = vw_bc.getRecord().newFieldInGroup("bc_Bc_Stat", "BC-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_STAT");
        bc_Bc_Stat.setDdmHeader("STATUS");
        bc_Bc_Cntrct_Type = vw_bc.getRecord().newFieldInGroup("bc_Bc_Cntrct_Type", "BC-CNTRCT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_CNTRCT_TYPE");
        bc_Bc_Cntrct_Type.setDdmHeader("CNTRCT/TYPE");
        bc_Bc_Rqst_Timestamp = vw_bc.getRecord().newFieldInGroup("bc_Bc_Rqst_Timestamp", "BC-RQST-TIMESTAMP", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "BC_RQST_TIMESTAMP");
        bc_Bc_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        bc_Bc_Mos_Ind = vw_bc.getRecord().newFieldInGroup("bc_Bc_Mos_Ind", "BC-MOS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_MOS_IND");
        bc_Bc_Mos_Ind.setDdmHeader("MOS/IND");
        bc_Bc_Irvcbl_Ind = vw_bc.getRecord().newFieldInGroup("bc_Bc_Irvcbl_Ind", "BC-IRVCBL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_IRVCBL_IND");
        bc_Bc_Irvcbl_Ind.setDdmHeader("IRVCBL/IND");
        bc_Bc_Rcrd_Last_Updt_Userid = vw_bc.getRecord().newFieldInGroup("bc_Bc_Rcrd_Last_Updt_Userid", "BC-RCRD-LAST-UPDT-USERID", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "BC_RCRD_LAST_UPDT_USERID");
        bc_Bc_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        bc_Bc_Last_Dsgntn_Srce = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Dsgntn_Srce", "BC-LAST-DSGNTN-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_SRCE");
        bc_Bc_Last_Dsgntn_Srce.setDdmHeader("LAST/DSGNTN/SOURCE");
        bc_Bc_Last_Dsgntn_System = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Dsgntn_System", "BC-LAST-DSGNTN-SYSTEM", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_SYSTEM");
        bc_Bc_Last_Dsgntn_System.setDdmHeader("LAST/DSGNTN/SYSTEM");
        bc_Bc_Last_Dsgntn_Userid = vw_bc.getRecord().newFieldInGroup("bc_Bc_Last_Dsgntn_Userid", "BC-LAST-DSGNTN-USERID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_LAST_DSGNTN_USERID");
        bc_Bc_Last_Dsgntn_Userid.setDdmHeader("LAST/DSGNTN/USERID");
        registerRecord(vw_bc);

        vw_bd = new DataAccessProgramView(new NameInfo("vw_bd", "BD"), "BENE_DESIGNATION_12", "BENE_DESIGNATION");
        bd_Bd_Pin = vw_bd.getRecord().newFieldInGroup("bd_Bd_Pin", "BD-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BD_PIN");
        bd_Bd_Pin.setDdmHeader("PIN");
        bd_Bd_Tiaa_Cntrct = vw_bd.getRecord().newFieldInGroup("bd_Bd_Tiaa_Cntrct", "BD-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BD_TIAA_CNTRCT");
        bd_Bd_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bd_Bd_Tiaa_Cref_Ind = vw_bd.getRecord().newFieldInGroup("bd_Bd_Tiaa_Cref_Ind", "BD-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_TIAA_CREF_IND");
        bd_Bd_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bd_Bd_Stat = vw_bd.getRecord().newFieldInGroup("bd_Bd_Stat", "BD-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_STAT");
        bd_Bd_Stat.setDdmHeader("STAT");
        bd_Bd_Bene_Type = vw_bd.getRecord().newFieldInGroup("bd_Bd_Bene_Type", "BD-BENE-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_BENE_TYPE");
        bd_Bd_Bene_Type.setDdmHeader("BENE/TYPE");
        bd_Bd_Irvcbl_Ind = vw_bd.getRecord().newFieldInGroup("bd_Bd_Irvcbl_Ind", "BD-IRVCBL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_IRVCBL_IND");
        bd_Bd_Irvcbl_Ind.setDdmHeader("IRVCBL/IND");
        bd_Bd_Dflt_Estate = vw_bd.getRecord().newFieldInGroup("bd_Bd_Dflt_Estate", "BD-DFLT-ESTATE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_DFLT_ESTATE");
        bd_Bd_Dflt_Estate.setDdmHeader("DEFAULT/ESTATE");
        registerRecord(vw_bd);

        vw_cor = new DataAccessProgramView(new NameInfo("vw_cor", "COR"), "COR_XREF_CNTRCT", "COR_XREF_FILE");
        cor_Cntrct_Nbr = vw_cor.getRecord().newFieldInGroup("cor_Cntrct_Nbr", "CNTRCT-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_NBR");
        cor_Cntrct_Nbr.setDdmHeader("CONTRACT/NUMBER");
        cor_Cntrct_Lob_Cde = vw_cor.getRecord().newFieldInGroup("cor_Cntrct_Lob_Cde", "CNTRCT-LOB-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_LOB_CDE");
        cor_Cntrct_Lob_Cde.setDdmHeader("LINE/OF/BUSINESS");
        cor_Ph_Unique_Id_Nbr = vw_cor.getRecord().newFieldInGroup("cor_Ph_Unique_Id_Nbr", "PH-UNIQUE-ID-NBR", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, 
            "PH_UNIQUE_ID_NBR");
        cor_Ph_Unique_Id_Nbr.setDdmHeader("PIN");
        cor_Cntrct_Spcl_Cnsdrtn_Cde = vw_cor.getRecord().newFieldInGroup("cor_Cntrct_Spcl_Cnsdrtn_Cde", "CNTRCT-SPCL-CNSDRTN-CDE", FieldType.STRING, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_SPCL_CNSDRTN_CDE");
        cor_Cntrct_Spcl_Cnsdrtn_Cde.setDdmHeader("CONTRACT/SPECIAL/CONSIDERATION");
        registerRecord(vw_cor);

        pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq = localVariables.newFieldInRecord("pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq", "#BD-STAT-PIN-CNTRCT-IND-TYPE-SEQ", 
            FieldType.STRING, 28);

        pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq__R_Field_1 = localVariables.newGroupInRecord("pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq__R_Field_1", "REDEFINE", 
            pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq);
        pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Stat = pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq__R_Field_1.newFieldInGroup("pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Stat", 
            "#BD-STAT", FieldType.STRING, 1);
        pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Pin = pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq__R_Field_1.newFieldInGroup("pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Pin", 
            "#BD-PIN", FieldType.NUMERIC, 12);
        pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Tiaa_Cntrct = pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq__R_Field_1.newFieldInGroup("pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Tiaa_Cntrct", 
            "#BD-TIAA-CNTRCT", FieldType.STRING, 10);
        pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Tiaa_Cref_Ind = pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq__R_Field_1.newFieldInGroup("pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Tiaa_Cref_Ind", 
            "#BD-TIAA-CREF-IND", FieldType.STRING, 1);
        pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Bene_Type = pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq__R_Field_1.newFieldInGroup("pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Bene_Type", 
            "#BD-BENE-TYPE", FieldType.STRING, 1);
        pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Seq_Nmbr = pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq__R_Field_1.newFieldInGroup("pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Seq_Nmbr", 
            "#BD-SEQ-NMBR", FieldType.NUMERIC, 3);

        pnd_Counters = localVariables.newGroupArrayInRecord("pnd_Counters", "#COUNTERS", new DbsArrayController(1, 4));
        pnd_Counters_Pnd_Total_Contracts = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Total_Contracts", "#TOTAL-CONTRACTS", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Counters_Pnd_Total_Bene = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Total_Bene", "#TOTAL-BENE", FieldType.PACKED_DECIMAL, 9);
        pnd_Counters_Pnd_Total_Mos = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Total_Mos", "#TOTAL-MOS", FieldType.PACKED_DECIMAL, 9);
        pnd_Counters_Pnd_Total_I_Bene = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Total_I_Bene", "#TOTAL-I-BENE", FieldType.PACKED_DECIMAL, 9);
        pnd_Counters_Pnd_Total_I_Mos = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Total_I_Mos", "#TOTAL-I-MOS", FieldType.PACKED_DECIMAL, 9);
        pnd_Counters_Pnd_Total_Bene_Sys = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Total_Bene_Sys", "#TOTAL-BENE-SYS", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Counters_Pnd_Total_Acis_Interface = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Total_Acis_Interface", "#TOTAL-ACIS-INTERFACE", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Counters_Pnd_Total_Cis_Interface = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Total_Cis_Interface", "#TOTAL-CIS-INTERFACE", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Counters_Pnd_Total_Mdo_Interface = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Total_Mdo_Interface", "#TOTAL-MDO-INTERFACE", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Counters_Pnd_Total_Acis_Migrate = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Total_Acis_Migrate", "#TOTAL-ACIS-MIGRATE", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Counters_Pnd_Total_Cis_Migrate = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Total_Cis_Migrate", "#TOTAL-CIS-MIGRATE", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Counters_Pnd_Total_Mdo_Migrate = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Total_Mdo_Migrate", "#TOTAL-MDO-MIGRATE", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Counters_Pnd_Total_Ms_Migrate = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Total_Ms_Migrate", "#TOTAL-MS-MIGRATE", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Counters_Pnd_Total_Web = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Total_Web", "#TOTAL-WEB", FieldType.PACKED_DECIMAL, 9);
        pnd_Counters_Pnd_Total_Dflt_Estate = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Total_Dflt_Estate", "#TOTAL-DFLT-ESTATE", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Counters_Pnd_Total_Pbene_Only = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Total_Pbene_Only", "#TOTAL-PBENE-ONLY", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Counters_Pnd_Total_P_C_Benes = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Total_P_C_Benes", "#TOTAL-P-C-BENES", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Counters_Pnd_Total_Pbene_1_2 = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Total_Pbene_1_2", "#TOTAL-PBENE-1-2", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Counters_Pnd_Total_Pbene_3_5 = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Total_Pbene_3_5", "#TOTAL-PBENE-3-5", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Counters_Pnd_Total_Pbene_6_10 = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Total_Pbene_6_10", "#TOTAL-PBENE-6-10", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Counters_Pnd_Total_Pbene_Gt10 = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Total_Pbene_Gt10", "#TOTAL-PBENE-GT10", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Counters_Pnd_Total_Cbene_1_2 = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Total_Cbene_1_2", "#TOTAL-CBENE-1-2", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Counters_Pnd_Total_Cbene_3_5 = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Total_Cbene_3_5", "#TOTAL-CBENE-3-5", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Counters_Pnd_Total_Cbene_6_10 = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Total_Cbene_6_10", "#TOTAL-CBENE-6-10", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Counters_Pnd_Total_Cbene_Gt10 = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Total_Cbene_Gt10", "#TOTAL-CBENE-GT10", FieldType.PACKED_DECIMAL, 
            9);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Pbene_Count = localVariables.newFieldInRecord("pnd_Pbene_Count", "#PBENE-COUNT", FieldType.PACKED_DECIMAL, 2);
        pnd_Cbene_Count = localVariables.newFieldInRecord("pnd_Cbene_Count", "#CBENE-COUNT", FieldType.PACKED_DECIMAL, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_bc.reset();
        vw_bd.reset();
        vw_cor.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Benp260() throws Exception
    {
        super("Benp260");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* **FORMAT ( 1) LS=133 PS=57 SG=OFF ZP=OFF
        //*  READ PHYSICAL                                                                                                                                                //Natural: FORMAT ( 1 ) LS = 133 PS = 57 SG = OFF ZP = ON
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: MOVE *PROGRAM TO #PROGRAM
        //* ***********************************************************************
        //*  MAIN PROCESS
        //* ***********************************************************************
        vw_bc.startDatabaseRead                                                                                                                                           //Natural: READ BC
        (
        "RDBC",
        new Oc[] { new Oc("ISN", "ASC") }
        );
        RDBC:
        while (condition(vw_bc.readNextRow("RDBC")))
        {
            //*  PENDING AND CURRENT ONLY
            //*      BYPASS OTHERS
            //*      BYPASS OTHERS
            //*      BYPASS OTHERS
            if (condition(bc_Bc_Stat.notEquals("C") && bc_Bc_Stat.notEquals("M") && bc_Bc_Stat.notEquals("P") && bc_Bc_Stat.notEquals("X")))                              //Natural: IF BC-STAT NE 'C' AND BC-STAT NE 'M' AND BC-STAT NE 'P' AND BC-STAT NE 'X'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  SET INDEX FOR BUCKETS
            short decideConditionsMet186 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF BC-CNTRCT-TYPE;//Natural: VALUE 'D'
            if (condition((bc_Bc_Cntrct_Type.equals("D"))))
            {
                decideConditionsMet186++;
                pnd_I.setValue(2);                                                                                                                                        //Natural: MOVE 2 TO #I
            }                                                                                                                                                             //Natural: VALUE 'I'
            else if (condition((bc_Bc_Cntrct_Type.equals("I"))))
            {
                decideConditionsMet186++;
                pnd_I.setValue(3);                                                                                                                                        //Natural: MOVE 3 TO #I
            }                                                                                                                                                             //Natural: VALUE 'N'
            else if (condition((bc_Bc_Cntrct_Type.equals("N"))))
            {
                decideConditionsMet186++;
                pnd_I.setValue(4);                                                                                                                                        //Natural: MOVE 4 TO #I
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_I.setValue(2);                                                                                                                                        //Natural: MOVE 2 TO #I
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Counters_Pnd_Total_Contracts.getValue(pnd_I).nadd(1);                                                                                                     //Natural: ADD 1 TO #TOTAL-CONTRACTS ( #I )
            if (condition(bc_Bc_Mos_Ind.equals("Y")))                                                                                                                     //Natural: IF BC-MOS-IND EQ 'Y'
            {
                pnd_Counters_Pnd_Total_Mos.getValue(pnd_I).nadd(1);                                                                                                       //Natural: ADD 1 TO #TOTAL-MOS ( #I )
                if (condition(bc_Bc_Irvcbl_Ind.equals("Y")))                                                                                                              //Natural: IF BC-IRVCBL-IND EQ 'Y'
                {
                    pnd_Counters_Pnd_Total_I_Mos.getValue(pnd_I).nadd(1);                                                                                                 //Natural: ADD 1 TO #TOTAL-I-MOS ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Counters_Pnd_Total_Bene.getValue(pnd_I).nadd(1);                                                                                                      //Natural: ADD 1 TO #TOTAL-BENE ( #I )
                if (condition(bc_Bc_Irvcbl_Ind.equals("Y")))                                                                                                              //Natural: IF BC-IRVCBL-IND EQ 'Y'
                {
                    pnd_Counters_Pnd_Total_I_Bene.getValue(pnd_I).nadd(1);                                                                                                //Natural: ADD 1 TO #TOTAL-I-BENE ( #I )
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM READ-BENE-DESIGNATION
                sub_Read_Bene_Designation();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RDBC"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RDBC"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //* JW 11/10/00 ACIS CORRECTION
            if (condition(bc_Bc_Last_Dsgntn_System.equals("A") || bc_Bc_Last_Dsgntn_System.equals("X")))                                                                  //Natural: IF BC-LAST-DSGNTN-SYSTEM EQ 'A' OR EQ 'X'
            {
                if (condition(bc_Bc_Last_Dsgntn_Srce.equals("1") || bc_Bc_Last_Dsgntn_Srce.equals("2")))                                                                  //Natural: IF BC-LAST-DSGNTN-SRCE EQ '1' OR BC-LAST-DSGNTN-SRCE EQ '2'
                {
                    pnd_Counters_Pnd_Total_Acis_Migrate.getValue(pnd_I).nadd(1);                                                                                          //Natural: ADD 1 TO #TOTAL-ACIS-MIGRATE ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Counters_Pnd_Total_Acis_Interface.getValue(pnd_I).nadd(1);                                                                                        //Natural: ADD 1 TO #TOTAL-ACIS-INTERFACE ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(bc_Bc_Last_Dsgntn_System.equals("B")))                                                                                                          //Natural: IF BC-LAST-DSGNTN-SYSTEM EQ 'B'
            {
                pnd_Counters_Pnd_Total_Bene_Sys.getValue(pnd_I).nadd(1);                                                                                                  //Natural: ADD 1 TO #TOTAL-BENE-SYS ( #I )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(bc_Bc_Last_Dsgntn_System.equals("C")))                                                                                                          //Natural: IF BC-LAST-DSGNTN-SYSTEM EQ 'C'
            {
                if (condition(bc_Bc_Last_Dsgntn_Srce.equals("1") || bc_Bc_Last_Dsgntn_Srce.equals("2")))                                                                  //Natural: IF BC-LAST-DSGNTN-SRCE EQ '1' OR BC-LAST-DSGNTN-SRCE EQ '2'
                {
                    pnd_Counters_Pnd_Total_Cis_Migrate.getValue(pnd_I).nadd(1);                                                                                           //Natural: ADD 1 TO #TOTAL-CIS-MIGRATE ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Counters_Pnd_Total_Cis_Interface.getValue(pnd_I).nadd(1);                                                                                         //Natural: ADD 1 TO #TOTAL-CIS-INTERFACE ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(bc_Bc_Last_Dsgntn_System.equals("M")))                                                                                                          //Natural: IF BC-LAST-DSGNTN-SYSTEM EQ 'M'
            {
                if (condition(bc_Bc_Last_Dsgntn_Srce.equals("1") || bc_Bc_Last_Dsgntn_Srce.equals("2")))                                                                  //Natural: IF BC-LAST-DSGNTN-SRCE EQ '1' OR BC-LAST-DSGNTN-SRCE EQ '2'
                {
                    pnd_Counters_Pnd_Total_Mdo_Migrate.getValue(pnd_I).nadd(1);                                                                                           //Natural: ADD 1 TO #TOTAL-MDO-MIGRATE ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Counters_Pnd_Total_Mdo_Interface.getValue(pnd_I).nadd(1);                                                                                         //Natural: ADD 1 TO #TOTAL-MDO-INTERFACE ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(bc_Bc_Last_Dsgntn_System.equals("MS")))                                                                                                         //Natural: IF BC-LAST-DSGNTN-SYSTEM EQ 'MS'
            {
                pnd_Counters_Pnd_Total_Ms_Migrate.getValue(pnd_I).nadd(1);                                                                                                //Natural: ADD 1 TO #TOTAL-MS-MIGRATE ( #I )
            }                                                                                                                                                             //Natural: END-IF
            //*  CF
            if (condition(bc_Bc_Last_Dsgntn_Userid.equals("WEB-E") || bc_Bc_Last_Dsgntn_Userid.equals("WEB-F")))                                                          //Natural: IF BC-LAST-DSGNTN-USERID = 'WEB-E' OR = 'WEB-F'
            {
                pnd_Counters_Pnd_Total_Web.getValue(pnd_I).nadd(1);                                                                                                       //Natural: ADD 1 TO #TOTAL-WEB ( #I )
            }                                                                                                                                                             //Natural: END-IF
            //*  RDBC.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  ADD DA, IA, INS BUCKETS TO TOTAL BUCKETS
        pnd_Counters_Pnd_Total_Contracts.getValue(1).nadd(pnd_Counters_Pnd_Total_Contracts.getValue(2,":",4));                                                            //Natural: ADD #TOTAL-CONTRACTS ( 2:4 ) TO #TOTAL-CONTRACTS ( 1 )
        pnd_Counters_Pnd_Total_Mos.getValue(1).nadd(pnd_Counters_Pnd_Total_Mos.getValue(2,":",4));                                                                        //Natural: ADD #TOTAL-MOS ( 2:4 ) TO #TOTAL-MOS ( 1 )
        pnd_Counters_Pnd_Total_Bene.getValue(1).nadd(pnd_Counters_Pnd_Total_Bene.getValue(2,":",4));                                                                      //Natural: ADD #TOTAL-BENE ( 2:4 ) TO #TOTAL-BENE ( 1 )
        pnd_Counters_Pnd_Total_Pbene_Only.getValue(1).nadd(pnd_Counters_Pnd_Total_Pbene_Only.getValue(2,":",4));                                                          //Natural: ADD #TOTAL-PBENE-ONLY ( 2:4 ) TO #TOTAL-PBENE-ONLY ( 1 )
        pnd_Counters_Pnd_Total_P_C_Benes.getValue(1).nadd(pnd_Counters_Pnd_Total_P_C_Benes.getValue(2,":",4));                                                            //Natural: ADD #TOTAL-P-C-BENES ( 2:4 ) TO #TOTAL-P-C-BENES ( 1 )
        pnd_Counters_Pnd_Total_Pbene_1_2.getValue(1).nadd(pnd_Counters_Pnd_Total_Pbene_1_2.getValue(2,":",4));                                                            //Natural: ADD #TOTAL-PBENE-1-2 ( 2:4 ) TO #TOTAL-PBENE-1-2 ( 1 )
        pnd_Counters_Pnd_Total_Pbene_3_5.getValue(1).nadd(pnd_Counters_Pnd_Total_Pbene_3_5.getValue(2,":",4));                                                            //Natural: ADD #TOTAL-PBENE-3-5 ( 2:4 ) TO #TOTAL-PBENE-3-5 ( 1 )
        pnd_Counters_Pnd_Total_Pbene_6_10.getValue(1).nadd(pnd_Counters_Pnd_Total_Pbene_6_10.getValue(2,":",4));                                                          //Natural: ADD #TOTAL-PBENE-6-10 ( 2:4 ) TO #TOTAL-PBENE-6-10 ( 1 )
        pnd_Counters_Pnd_Total_Pbene_Gt10.getValue(1).nadd(pnd_Counters_Pnd_Total_Pbene_Gt10.getValue(2,":",4));                                                          //Natural: ADD #TOTAL-PBENE-GT10 ( 2:4 ) TO #TOTAL-PBENE-GT10 ( 1 )
        pnd_Counters_Pnd_Total_Cbene_1_2.getValue(1).nadd(pnd_Counters_Pnd_Total_Cbene_1_2.getValue(2,":",4));                                                            //Natural: ADD #TOTAL-CBENE-1-2 ( 2:4 ) TO #TOTAL-CBENE-1-2 ( 1 )
        pnd_Counters_Pnd_Total_Cbene_3_5.getValue(1).nadd(pnd_Counters_Pnd_Total_Cbene_3_5.getValue(2,":",4));                                                            //Natural: ADD #TOTAL-CBENE-3-5 ( 2:4 ) TO #TOTAL-CBENE-3-5 ( 1 )
        pnd_Counters_Pnd_Total_Cbene_6_10.getValue(1).nadd(pnd_Counters_Pnd_Total_Cbene_6_10.getValue(2,":",4));                                                          //Natural: ADD #TOTAL-CBENE-6-10 ( 2:4 ) TO #TOTAL-CBENE-6-10 ( 1 )
        pnd_Counters_Pnd_Total_Cbene_Gt10.getValue(1).nadd(pnd_Counters_Pnd_Total_Cbene_Gt10.getValue(2,":",4));                                                          //Natural: ADD #TOTAL-CBENE-GT10 ( 2:4 ) TO #TOTAL-CBENE-GT10 ( 1 )
        pnd_Counters_Pnd_Total_I_Mos.getValue(1).nadd(pnd_Counters_Pnd_Total_I_Mos.getValue(2,":",4));                                                                    //Natural: ADD #TOTAL-I-MOS ( 2:4 ) TO #TOTAL-I-MOS ( 1 )
        pnd_Counters_Pnd_Total_I_Bene.getValue(1).nadd(pnd_Counters_Pnd_Total_I_Bene.getValue(2,":",4));                                                                  //Natural: ADD #TOTAL-I-BENE ( 2:4 ) TO #TOTAL-I-BENE ( 1 )
        pnd_Counters_Pnd_Total_Dflt_Estate.getValue(1).nadd(pnd_Counters_Pnd_Total_Dflt_Estate.getValue(2,":",4));                                                        //Natural: ADD #TOTAL-DFLT-ESTATE ( 2:4 ) TO #TOTAL-DFLT-ESTATE ( 1 )
        pnd_Counters_Pnd_Total_Acis_Interface.getValue(1).nadd(pnd_Counters_Pnd_Total_Acis_Interface.getValue(2,":",4));                                                  //Natural: ADD #TOTAL-ACIS-INTERFACE ( 2:4 ) TO #TOTAL-ACIS-INTERFACE ( 1 )
        pnd_Counters_Pnd_Total_Bene_Sys.getValue(1).nadd(pnd_Counters_Pnd_Total_Bene_Sys.getValue(2,":",4));                                                              //Natural: ADD #TOTAL-BENE-SYS ( 2:4 ) TO #TOTAL-BENE-SYS ( 1 )
        pnd_Counters_Pnd_Total_Cis_Interface.getValue(1).nadd(pnd_Counters_Pnd_Total_Cis_Interface.getValue(2,":",4));                                                    //Natural: ADD #TOTAL-CIS-INTERFACE ( 2:4 ) TO #TOTAL-CIS-INTERFACE ( 1 )
        pnd_Counters_Pnd_Total_Mdo_Interface.getValue(1).nadd(pnd_Counters_Pnd_Total_Mdo_Interface.getValue(2,":",4));                                                    //Natural: ADD #TOTAL-MDO-INTERFACE ( 2:4 ) TO #TOTAL-MDO-INTERFACE ( 1 )
        pnd_Counters_Pnd_Total_Acis_Migrate.getValue(1).nadd(pnd_Counters_Pnd_Total_Acis_Migrate.getValue(2,":",4));                                                      //Natural: ADD #TOTAL-ACIS-MIGRATE ( 2:4 ) TO #TOTAL-ACIS-MIGRATE ( 1 )
        pnd_Counters_Pnd_Total_Cis_Migrate.getValue(1).nadd(pnd_Counters_Pnd_Total_Cis_Migrate.getValue(2,":",4));                                                        //Natural: ADD #TOTAL-CIS-MIGRATE ( 2:4 ) TO #TOTAL-CIS-MIGRATE ( 1 )
        pnd_Counters_Pnd_Total_Mdo_Migrate.getValue(1).nadd(pnd_Counters_Pnd_Total_Mdo_Migrate.getValue(2,":",4));                                                        //Natural: ADD #TOTAL-MDO-MIGRATE ( 2:4 ) TO #TOTAL-MDO-MIGRATE ( 1 )
        pnd_Counters_Pnd_Total_Ms_Migrate.getValue(1).nadd(pnd_Counters_Pnd_Total_Ms_Migrate.getValue(2,":",4));                                                          //Natural: ADD #TOTAL-MS-MIGRATE ( 2:4 ) TO #TOTAL-MS-MIGRATE ( 1 )
        //*  CF
        pnd_Counters_Pnd_Total_Web.getValue(1).nadd(pnd_Counters_Pnd_Total_Web.getValue(2,":",4));                                                                        //Natural: ADD #TOTAL-WEB ( 2:4 ) TO #TOTAL-WEB ( 1 )
        //* *D #TOTAL-P(2:4)             TO #TOTAL-P(1)
        //* *D #TOTAL-R(2:4)             TO #TOTAL-R(1)
        //* *
        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Benm260.class));                                                                               //Natural: WRITE ( 1 ) NOTITLE USING FORM 'BENM260'
        //* **
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-BENE-DESIGNATION
    }
    private void sub_Read_Bene_Designation() throws Exception                                                                                                             //Natural: READ-BENE-DESIGNATION
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq.reset();                                                                                                                      //Natural: RESET #BD-STAT-PIN-CNTRCT-IND-TYPE-SEQ #PBENE-COUNT #CBENE-COUNT
        pnd_Pbene_Count.reset();
        pnd_Cbene_Count.reset();
        pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Stat.setValue(bc_Bc_Stat);                                                                                             //Natural: MOVE BC.BC-STAT TO #BD-STAT
        pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Pin.setValue(bc_Bc_Pin);                                                                                               //Natural: MOVE BC.BC-PIN TO #BD-PIN
        pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Tiaa_Cntrct.setValue(bc_Bc_Tiaa_Cntrct);                                                                               //Natural: MOVE BC.BC-TIAA-CNTRCT TO #BD-TIAA-CNTRCT
        pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Tiaa_Cref_Ind.setValue(bc_Bc_Tiaa_Cref_Ind);                                                                           //Natural: MOVE BC.BC-TIAA-CREF-IND TO #BD-TIAA-CREF-IND
        vw_bd.startDatabaseRead                                                                                                                                           //Natural: READ BD BY BD-STAT-PIN-CNTRCT-IND-TYPE-SEQ FROM #BD-STAT-PIN-CNTRCT-IND-TYPE-SEQ
        (
        "RDBD",
        new Wc[] { new Wc("BD_STAT_PIN_CNTRCT_IND_TYPE_SEQ", ">=", pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq, WcType.BY) },
        new Oc[] { new Oc("BD_STAT_PIN_CNTRCT_IND_TYPE_SEQ", "ASC") }
        );
        RDBD:
        while (condition(vw_bd.readNextRow("RDBD")))
        {
            if (condition(bd_Bd_Stat.notEquals(pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Stat) || bd_Bd_Pin.notEquals(pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Pin)    //Natural: IF BD-STAT NE #BD-STAT OR BD-PIN NE #BD-PIN OR BD-TIAA-CNTRCT NE #BD-TIAA-CNTRCT OR BD-TIAA-CREF-IND NE #BD-TIAA-CREF-IND
                || bd_Bd_Tiaa_Cntrct.notEquals(pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Tiaa_Cntrct) || bd_Bd_Tiaa_Cref_Ind.notEquals(pnd_Bd_Stat_Pin_Cntrct_Ind_Type_Seq_Pnd_Bd_Tiaa_Cref_Ind)))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(bd_Bd_Bene_Type.equals("P")))                                                                                                                   //Natural: IF BD-BENE-TYPE EQ 'P'
            {
                pnd_Pbene_Count.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #PBENE-COUNT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Cbene_Count.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #CBENE-COUNT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(bd_Bd_Dflt_Estate.equals("Y")))                                                                                                                 //Natural: IF BD-DFLT-ESTATE EQ 'Y'
            {
                pnd_Counters_Pnd_Total_Dflt_Estate.getValue(pnd_I).nadd(1);                                                                                               //Natural: ADD 1 TO #TOTAL-DFLT-ESTATE ( #I )
            }                                                                                                                                                             //Natural: END-IF
            //*  RDBD.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Cbene_Count.lessOrEqual(getZero())))                                                                                                            //Natural: IF #CBENE-COUNT LE 0
        {
            pnd_Counters_Pnd_Total_Pbene_Only.getValue(pnd_I).nadd(1);                                                                                                    //Natural: ADD 1 TO #TOTAL-PBENE-ONLY ( #I )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Counters_Pnd_Total_P_C_Benes.getValue(pnd_I).nadd(1);                                                                                                     //Natural: ADD 1 TO #TOTAL-P-C-BENES ( #I )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Pbene_Count.lessOrEqual(getZero())))                                                                                                            //Natural: IF #PBENE-COUNT LE 0
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Pbene_Count.lessOrEqual(2)))                                                                                                                //Natural: IF #PBENE-COUNT LE 2
            {
                pnd_Counters_Pnd_Total_Pbene_1_2.getValue(pnd_I).nadd(1);                                                                                                 //Natural: ADD 1 TO #TOTAL-PBENE-1-2 ( #I )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Pbene_Count.lessOrEqual(5)))                                                                                                            //Natural: IF #PBENE-COUNT LE 5
                {
                    pnd_Counters_Pnd_Total_Pbene_3_5.getValue(pnd_I).nadd(1);                                                                                             //Natural: ADD 1 TO #TOTAL-PBENE-3-5 ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Pbene_Count.lessOrEqual(10)))                                                                                                       //Natural: IF #PBENE-COUNT LE 10
                    {
                        pnd_Counters_Pnd_Total_Pbene_6_10.getValue(pnd_I).nadd(1);                                                                                        //Natural: ADD 1 TO #TOTAL-PBENE-6-10 ( #I )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Counters_Pnd_Total_Pbene_Gt10.getValue(pnd_I).nadd(1);                                                                                        //Natural: ADD 1 TO #TOTAL-PBENE-GT10 ( #I )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Cbene_Count.lessOrEqual(getZero())))                                                                                                            //Natural: IF #CBENE-COUNT LE 0
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Cbene_Count.lessOrEqual(2)))                                                                                                                //Natural: IF #CBENE-COUNT LE 2
            {
                pnd_Counters_Pnd_Total_Cbene_1_2.getValue(pnd_I).nadd(1);                                                                                                 //Natural: ADD 1 TO #TOTAL-CBENE-1-2 ( #I )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Cbene_Count.lessOrEqual(5)))                                                                                                            //Natural: IF #CBENE-COUNT LE 5
                {
                    pnd_Counters_Pnd_Total_Cbene_3_5.getValue(pnd_I).nadd(1);                                                                                             //Natural: ADD 1 TO #TOTAL-CBENE-3-5 ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Cbene_Count.lessOrEqual(10)))                                                                                                       //Natural: IF #CBENE-COUNT LE 10
                    {
                        pnd_Counters_Pnd_Total_Cbene_6_10.getValue(pnd_I).nadd(1);                                                                                        //Natural: ADD 1 TO #TOTAL-CBENE-6-10 ( #I )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Counters_Pnd_Total_Cbene_Gt10.getValue(pnd_I).nadd(1);                                                                                        //Natural: ADD 1 TO #TOTAL-CBENE-GT10 ( #I )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  READ-BENE-DESIGNATION
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=57 SG=OFF ZP=ON");
    }
}
