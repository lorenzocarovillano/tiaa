//PRD120AX JOB  (P,66800,000,LEGB),'DCS - RDR2010J',
//         CLASS=6,
//         MSGCLASS=I,
//         MSGLEVEL=(1,1)
//* RDR2010J - STANDARD DELIVERY
//* CONNECT DIRECT IMAGE FILE WITH PI DATA TO IMAGE SERVER
//******************************************************************
//******************************************************************
//*
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*
//******************************************************************
//* EXECUTE PROC P1240PTX
//******************************************************************
//PRD120AX EXEC P1240PTX,COND=(0,NE),
//*
//******************************************************************
//*  CHANGE THIS PARAMETER BEFORE MOVING INTO PRODUCTION
//*  JOB NAME IS PREVIOUS JOB NAME!!!
//******************************************************************
//        JOBNM=PRD1201R,
//*
//******************************************************************
//*   TEST-SPECIFIC PARAMETERS; MUST BE CHANGED FOR PRODUCTION
//******************************************************************
//        HLQ1=PPT.COR.XML
