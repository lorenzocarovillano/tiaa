//P1041NID JOB (P,15000,000,OPSB),NEWISSU,CLASS=1,MSGCLASS=I
//*                                                                     
//*                                                                     
//BKUP010 EXEC PGM=IDCAMS
//SYSPRINT DD SYSOUT=*
//INPUT    DD DSN=PPM.ANN.P1040NID.LETTERS,DISP=SHR
//OUTPUT   DD DSN=PPDT.P1040NID.LETTERS(+1),
//            DISP=(,CATLG,DELETE),
//            UNIT=CARTV,
//            RECFM=LS,LRECL=6200,DCB=MODLDSCB
//SYSIN    DD DSN=PROD.PARMLIB(REPRO),DISP=SHR
