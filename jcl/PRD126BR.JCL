//PRD126BR JOB (P,13840,000,LEGS),'2016 CREF SHARE',
//         MSGCLASS=I,
//         CLASS=1,
//         REGION=9M
//*                                                                     
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB                              
//*                                                                     
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//JOBLIB   DD  DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//*                                                                     
//*****************************************************************     
//* READ DA REPORTING EXTRACT & SPLIT BY NUMBER OF CONTRACTS.           
//*****************************************************************     
//PRD126BR EXEC PRD1265C,                                               
//      PARMMEM=RD1265R1,                                               
//      DATA='PART2',                                                   
//      FILE1='FILEB1',
//      FILE2='FILEB2',                                                 
//      REPORT1='RD126BR1',
//      REPORT2='RD126BR2'
//*
