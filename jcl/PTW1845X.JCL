//PTW1845X JOB (P,14400,000,FINB),CAIMS,MSGCLASS=I,CLASS=6,
//   REGION=8M
//*                                                                     
//* SC - SOUTH CAROLINA                                                 
//*                                                                     
//* SINCE IN HAS 2 OUTPUT FILES, PROC PTW1800X TO COPY A SINGLE FILE    
//* TO DISK IS NOT APPLICABLE HERE.                                     
//* THE COPY FILE PROCESS IS INCORPORATED IN NDM PROCESS - DLNA1845.    
//*--------------------------------------------------------------------*
//* NDMS010 - TRANSMITS STATE MAG MEDIA TAPE FILES TO A SERVER.        *
//*           THE FILES WILL THEN BE TRANSMITTED TO THE STATE TAXATION *
//*           AUTHORITY.                                               *
//*           NDM UTILITY PROGRAM "DMBATCH".                           *
//*--------------------------------------------------------------------*
//*                                                                     
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//NDMS010  EXEC PGM=DMBATCH,REGION=4M,PARM=(YYSLYNN),COND=(0,NE)        
//DMNETMAP DD DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR                   
//DMPUBLIB DD DSN=PMVSPP.STERLING.PROCESS.LIB,DISP=SHR                  
//DMMSGFIL DD DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR                      
//DMPRINT  DD SYSOUT=*                                                  
//NDMCMDS  DD SYSOUT=*                                                  
//SYSUDUMP DD SYSOUT=U                                                  
//SYSIN    DD *                                                         
  SIGNON ESF=YES                                                        
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(DLNA1845) MAXDELAY=UNLIMITED       
  SIGNOFF                                                               
/*                                                                      
