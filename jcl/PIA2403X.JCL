//PIA2403X JOB (P,66810,000,LEGB),IA,MSGCLASS=I,                        
//             CLASS=6,REGION=8M                                        
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB                              
//**************************************************************        
//*  FILE SENT TO GIS                                          *        
//**************************************************************        
//NDMS010 EXEC PGM=DMBATCH,PARM=(YYSLYNN)                               
//DMNETMAP DD  DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR                  
//DMPUBLIB DD  DSN=PROD.NDM.NY.PROCESS.LIB,DISP=SHR                     
//DMMSGFIL DD  DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR                     
//DMPRINT   DD SYSOUT=*                                                 
//NDMCMDS   DD SYSOUT=*                                                 
//SYSUDUMP  DD SYSOUT=U                                                 
//SYSIN     DD *                                                        
  SIGNON ESF=YES                                                        
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(IA2403X1) MAXDELAY=UNLIMITED -     
         CASE=YES                                                       
  SIGNOFF                                                               
/*                                                                      
