//POP4010X JOB (C,19999,000,OPSB),'OP',CLASS=1,                         
//         MSGCLASS=I                                                   
//*                                                                     
//LIBSRCH  JCLLIB ORDER=(PROD.OAS.NY.PARMLIB)                           
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*                                                                     
//*MSYS // INCLUDE MEMBER=JOBLIBU                        
//JOBLIB   DD  DISP=SHR,DSN=PDBFPP.OMNIPAY.TIAA.BAT.LOADLIB             00011001
//         DD  DISP=SHR,DSN=PDBFPP.OMNIPAY.CURRENT.BAT.LOADLIB          00012001
//         DD  DISP=SHR,DSN=PDBFPP.OMNIPAY.TIAA.ONLINE.LOADLIB          00013001
//         DD  DISP=SHR,DSN=PDBFPP.OMNIPAY.CURRENT.ONLINE.LOADLIB
//         DD  DISP=SHR,DSN=PROD.BATCH.LOADLIB
//*                                                                     
//***********************************************************           
//** TRANSFER VOIDS FILE TO MAINFRAME AND DELETE IT       ***           
//***********************************************************           
//NDMS010  EXEC PGM=DMBATCH,PARM=(YYSLYNN)                              
//DMMSGFIL DD  DISP=SHR,DSN=PMVSSF.STERLING.NDM.MSG                     
//DMNETMAP DD  DISP=SHR,DSN=PMVSSF.STERLING.NDM.NETMAP                  
//DMPUBLIB DD  DISP=SHR,DSN=PROD.NDM.NY.PROCESS.LIB                     
//DMPRINT  DD  SYSOUT=*                                                 
//NDMCMDS   DD SYSOUT=*                                                 
//SYSIN    DD  *                                                        
  SIGNON ESF=YES                                                        
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(OP4010X1) MAXDELAY=UNLIMITED -     
         CASE=YES                                                       
  SIGNOFF                                                               
/*                                                                      
//***********************************************************           
//** ARCHIVE FILE FROM MAINFRAME                          ***           
//***********************************************************           
//NDMS020  EXEC PGM=DMBATCH,PARM=(YYSLYNN),COND=(0,LT)                  
//DMMSGFIL DD  DISP=SHR,DSN=PMVSSF.STERLING.NDM.MSG                     
//DMNETMAP DD  DISP=SHR,DSN=PMVSSF.STERLING.NDM.NETMAP                  
//DMPUBLIB DD  DISP=SHR,DSN=PROD.NDM.NY.PROCESS.LIB                     
//DMPRINT  DD  SYSOUT=*                                                 
//NDMCMDS   DD SYSOUT=*                                                 
//SYSIN    DD  *                                                        
  SIGNON ESF=YES                                                        
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(OP4010X2) MAXDELAY=UNLIMITED -     
         CASE=YES                                                       
  SIGNOFF                                                               
/*                                                                      
//***********************************************************           
//** TRANSFER MAINFRAME FILE TO CHECKFREE SERVER          ***           
//***********************************************************           
//NDMS030  EXEC PGM=DMBATCH,PARM=(YYSLYNN),COND=(0,LT)                  
//DMMSGFIL DD  DISP=SHR,DSN=PMVSSF.STERLING.NDM.MSG                     
//DMNETMAP DD  DISP=SHR,DSN=PMVSSF.STERLING.NDM.NETMAP                  
//DMPUBLIB DD  DISP=SHR,DSN=PROD.NDM.NY.PROCESS.LIB                     
//DMPRINT  DD  SYSOUT=*                                                 
//NDMCMDS   DD SYSOUT=*                                                 
//SYSIN    DD  *                                                        
  SIGNON ESF=YES                                                        
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(OP4010X3) MAXDELAY=UNLIMITED -     
         CASE=YES                                                       
  SIGNOFF                                                               
/*                                                                      
