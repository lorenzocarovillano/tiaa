//P1196CPZ JOB (P,67412,000,FINB),DR,CLASS=1,MSGCLASS=I,
//        MSGLEVEL=(1,1)
//LIBSRCH  JCLLIB ORDER=PROD.OAS.DN.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//DELT005 EXEC PGM=IEFBR14                                              
//DD1     DD DSN=PPDD.SCD.TRIGGER.NY.P1196CPZ,                          
//          DISP=(MOD,DELETE),UNIT=SYSDA,SPACE=(TRK,0),                 
//          RECFM=LS,LRECL=80,DCB=MODLDSCB
//COPY010 EXEC PGM=IEBGENER,COND=(0,NE,DELT005)                         
//SYSPRINT DD SYSOUT=*                                                  
//SYSIN  DD DUMMY                                                       
//SYSUT1 DD DSN=PPDD.CASCD.JOB.TRIGGER,DISP=SHR                         
//SYSUT2 DD DSN=PPDD.SCD.TRIGGER.NY.P1196CPZ,                           
//          DISP=(,CATLG,DELETE),UNIT=SYSDA,SPACE=(TRK,(1,1)),          
//          RECFM=LS,LRECL=80,DCB=MODLDSCB
//*************************************************************
