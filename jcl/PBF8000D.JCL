//PBF8000D JOB (P,66812,000,OPSS),'PBF8000D',MSGCLASS=I,CLASS=1
//*
//LIBSRCH  JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*
//JOBLIB  DD DSN=PROD.OMNIPLUS.USER.LOADLIB,DISP=SHR
//        DD DSN=PDBFPP.OMNIPLUS.CURRENT.BAT.LOADLIB,DISP=SHR
//        DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//        DD DSN=PROD.BATCH.LOADLIB,DISP=SHR
//**********************************************************************
//*   GLOBAL SUBSTITUTION VALUES (SETS) ARE WITHIN THE INCLUDE MEMBER
//**********************************************************************
//**********************************************************************
//*  CHG745576 - 04/09/2020 - NIKHIL - REMOVE EMER LOADLIB
//*********************************************************************
//PBF8000D EXEC PBF8000D
