//PCW4700X JOB (C,62100,127,LEGB),CAIMS,MSGCLASS=I,CLASS=6,             
//       REGION=8M                                                      
//LIBSRCH  JCLLIB ORDER=PROD.OAS.NY.PARMLIB                             
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*                                                                     
//NDMS010 EXEC PGM=DMBATCH,REGION=4M,PARM=(YYSLYNN)                     
//DMNETMAP DD DISP=SHR,DSN=PMVSSF.STERLING.NDM.NETMAP                   
//DMPUBLIB DD DISP=SHR,DSN=PMVSPP.STERLING.PROCESS.LIB                  
//DMMSGFIL DD DISP=SHR,DSN=PMVSSF.STERLING.NDM.MSG                      
//NDMCMDS  DD SYSOUT=*                                                  
//DMPRINT  DD SYSOUT=*                                                  
//SYSIN    DD  *                                                        
 SIGNON                                                                 
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(CW4700X1) MAXDELAY=UNLIMITED -      
 CASE=YES                                                               
 SIGNOFF                                                                
//                                                                      
