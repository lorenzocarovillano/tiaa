//PNI9600D JOB (P,15000,000,OPSS),NEWISSU,CLASS=1,MSGCLASS=I
//*
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*
//JOBLIB DD DSN=PROD.BATCH.LOADLIB,DISP=SHR
//       DD DSN=PROD.OMNIPLUS.USER.LOADLIB,DISP=SHR
//       DD DSN=PDBFPP.OMNIPLUS.CURRENT.BAT.LOADLIB,DISP=SHR  ISP=SHR
//       DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//*
//PNI9600D EXEC PNI9600D
