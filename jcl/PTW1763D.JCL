//PTW1763D JOB (P,14400,000,FINS),                                      
//            CPS-NPD,                                                  
//            MSGCLASS=I,                                               
//            CLASS=1,                                                  
//            REGION=8M                                                 
//*                                                                     
//*--------------------------------------------------------------------*
//*                                                                    *
//*     T I A A - C R E F   O P E N   P L A N   S O L U T I O N S      *
//*                                                                    *
//*                          *** PTW1763D ***                          *
//*                                                                    *
//*    TAXWARS MCCAMISH LIFE INSURANCE DAILY FEED (PREVIOUS YEAR)      *
//*                               UPDATE                               *
//*                                                                    *
//*--------------------------------------------------------------------*
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB                              
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//JOBLIB  DD DSN=PROD.BATCH.LOADLIB,DISP=SHR                            
//        DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR                 
//*-----------------------------------------------------------          
//PTW1763D EXEC PTW1760D,                                               
//         JOBNM='PTW1763D',                                            
//         JOBNM1='PTW1753D',                                           
//         JOBNM2='PTW1743D',                                           
//         REPID2=',TW1763D2',                                          
//         REPID3=',TW1763D3',                                          
//         REPID4=',TW1763D4',                                          
//         REPID5=',TW1763D5',                                          
//         REPID6=',TW1763D6',                                          
//         REPID7=',TW1763D7',                                          
//         REPID8=',TW1763D8',                                          
//         REPID9=',TW1763D9',                                          
//         REPIDA=',TW1763DA',                                          
//         REPIDB=',TW1763DB',                                          
//         REPIDC=',TW1763DC',                                          
//         REPIDD=',TW1763DD',                                          
//         REPIDE=',TW1763DE',                                          
//         REPIDF=',TW1763DF',                                          
//         REPIDG=',TW1763DG',                                          
//         REPIDH=',TW1763DH',                                          
//         REPIDJ=',TW1763DJ',                                          
//         REPIDK=',TW1763DK',                                          
//         REPIDL=',TW1763DL',                                          
//         REPIDM=',TW1763DM',                                          
//         REPIDN=',TW1763DN',                                          
//         REPIDO=',TW1763DO',                                          
//         REPIDP=',TW1763DP'                                           
//                                                                      
