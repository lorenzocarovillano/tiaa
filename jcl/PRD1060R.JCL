//PRD1060R JOB (P,13840,000,LEGS),'2007 TSRC MAIL',
//         MSGCLASS=I,
//         CLASS=1,
//         REGION=9M
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//JOBLIB   DD  DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//*
//*****************************************************************
//* READ DA REPORTING EXTRACT & SPLIT BY NUMBER OF CONTRACTS
//*****************************************************************
//PRD1060R EXEC PRD1060R,
//      PARMMEM=RD1060R1,
//      DATA='DATA1'
//*****************************************************************
//* REPORTS
//*****************************************************************
//EXTR020.CMPRT01 DD SYSOUT=(&REPT.,RD1060R1)
//EXTR020.CMPRT02 DD SYSOUT=(&REPT.,RD1060R2)
