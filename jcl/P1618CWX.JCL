//P1618CWX JOB (C,67412,000,LEGB),'NDM CWF-EXTR',MSGCLASS=I,CLASS=6
//*
//*********************************************************************
//*                     JOB DESCRIPTION                               *
//*-------------------------------------------------------------------*
//* THIS JOB TRANSMITS 6 CWF - NON-PARTICIPANT EXTRACT FILES,CREATED  *
//* FROM JOB P1617CWD, TO LINUX SERVER CHAPDA3ORCCP01.                *
//*********************************************************************
//*                     MODIFICATION LOG                              *
//*-------------------------------------------------------------------*
//* CHANGE REQUEST NUMBER : CHG319449                                 *
//* IMPLEMENTATION DATE : 11/22/2014                                  *
//* AUTHOR : COGNIZANT                                                *
//* VERSION :INITIAL VERSION.                                         *
//*          CREATED IN PROJECT - "Mainframe Oracle Retirement - ORP3"*
//*          (TC00002875)                                             *
//*********************************************************************
//*
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*
//********************************************************************
//* TRANSMITS PCWF.COR.P1617CWD.UNITACT.UPD.EXT FILE TO LINUX SERVER
//********************************************************************
//NDMS010  EXEC PGM=DMBATCH,REGION=4M,PARM=(YYSLYNN),TIME=1440,
//             COND=(0,NE)
//DMMSGFIL DD  DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR
//DMNETMAP DD  DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR
//DMPUBLIB DD  DSN=PMVSPP.STERLING.PROCESS,DISP=SHR
//DMPRINT  DD  SYSOUT=*
//NDMCMDS   DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=U
//SYSIN     DD *
  SIGNON
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(CWF000X1)         -
  &DSN1=PCWF.COR.P1617CWD.UNITACT.UPD.EXT              -
  &DSN2=npart.activity_unit_temp.upd                   -
  &OUTOPTS=:datatype=text:xlate=yes:strip.blanks=no:   -
  &ERRMSG='P1618CWX.NDMS010'                           -
  MAXDELAY=UNLIMITED CASE=YES
  SIGNOFF
/*
//*
//********************************************************************
//*TRANSMITS PCWF.COR.P1617CWD.EMPLACT.UPD.EXT FILE TO LINUX SERVER
//********************************************************************
//NDMS020  EXEC PGM=DMBATCH,REGION=4M,PARM=(YYSLYNN),TIME=1440,
//             COND=(0,NE)
//DMMSGFIL DD  DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR
//DMNETMAP DD  DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR
//DMPUBLIB DD  DSN=PMVSPP.STERLING.PROCESS,DISP=SHR
//DMPRINT  DD  SYSOUT=*
//NDMCMDS   DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=U
//SYSIN     DD *
  SIGNON
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(CWF000X1)         -
  &DSN1=PCWF.COR.P1617CWD.EMPLACT.UPD.EXT              -
  &DSN2=npart.activity_employee_temp.upd               -
  &OUTOPTS=:datatype=text:xlate=yes:strip.blanks=no:   -
  &ERRMSG='P1618CWX.NDMS020'                           -
  MAXDELAY=UNLIMITED CASE=YES
  SIGNOFF
/*
//*
//********************************************************************
//* TRANSMITS PCWF.COR.P1617CWD.STEPACT.UPD.EXT FILE TO LINUX SERVER
//********************************************************************
//NDMS030  EXEC PGM=DMBATCH,REGION=4M,PARM=(YYSLYNN),TIME=1440,
//             COND=(0,NE)
//DMMSGFIL DD  DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR
//DMNETMAP DD  DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR
//DMPUBLIB DD  DSN=PMVSPP.STERLING.PROCESS,DISP=SHR
//DMPRINT  DD  SYSOUT=*
//NDMCMDS   DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=U
//SYSIN     DD *
  SIGNON
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(CWF000X1)         -
  &DSN1=PCWF.COR.P1617CWD.STEPACT.UPD.EXT              -
  &DSN2=npart.activity_step_temp.upd                   -
  &OUTOPTS=:datatype=text:xlate=yes:strip.blanks=no:   -
  &ERRMSG='P1618CWX.NDMS030'                           -
  MAXDELAY=UNLIMITED CASE=YES
  SIGNOFF
/*
//*
//********************************************************************
//*TRANSMITS PCWF.COR.P1617CWD.EXTRACT.UPD.EXT FILE TO LINUX SERVER
//********************************************************************
//NDMS040  EXEC PGM=DMBATCH,REGION=4M,PARM=(YYSLYNN),TIME=1440,
//             COND=(0,NE)
//DMMSGFIL DD  DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR
//DMNETMAP DD  DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR
//DMPUBLIB DD  DSN=PMVSPP.STERLING.PROCESS,DISP=SHR
//DMPRINT  DD  SYSOUT=*
//NDMCMDS   DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=U
//SYSIN     DD *
  SIGNON
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(CWF000X1)         -
  &DSN1=PCWF.COR.P1617CWD.EXTRACT.UPD.EXT              -
  &DSN2=npart.activity_external_temp.upd               -
  &OUTOPTS=:datatype=text:xlate=yes:strip.blanks=no:   -
  &ERRMSG='P1618CWX.NDMS040'                           -
  MAXDELAY=UNLIMITED CASE=YES
  SIGNOFF
/*
//*
//********************************************************************
//*TRANSMITS PCWF.COR.P1617CWD.INTRACT.UPD.EXT FILE TO LINUX SERVER
//********************************************************************
//NDMS050  EXEC PGM=DMBATCH,REGION=4M,PARM=(YYSLYNN),TIME=1440,
//             COND=(0,NE)
//DMMSGFIL DD  DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR
//DMNETMAP DD  DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR
//DMPUBLIB DD  DSN=PMVSPP.STERLING.PROCESS,DISP=SHR
//DMPRINT  DD  SYSOUT=*
//NDMCMDS   DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=U
//SYSIN     DD *
  SIGNON
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(CWF000X1)         -
  &DSN1=PCWF.COR.P1617CWD.INTRACT.UPD.EXT              -
  &DSN2=npart.activity_internal_temp.upd               -
  &OUTOPTS=:datatype=text:xlate=yes:strip.blanks=no:   -
  &ERRMSG='P1618CWX.NDMS050'                           -
  MAXDELAY=UNLIMITED CASE=YES
  SIGNOFF
/*
//*
//********************************************************************
//* TRANSMITS PCWF.COR.P1617CWD.ENRTACT.UPD.EXT FILE TO LINUX SERVER
//********************************************************************
//NDMS060  EXEC PGM=DMBATCH,REGION=4M,PARM=(YYSLYNN),TIME=1440,
//             COND=(0,NE)
//DMMSGFIL DD  DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR
//DMNETMAP DD  DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR
//DMPUBLIB DD  DSN=PMVSPP.STERLING.PROCESS,DISP=SHR
//DMPRINT  DD  SYSOUT=*
//NDMCMDS   DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=U
//SYSIN     DD *
  SIGNON
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(CWF000X1)         -
  &DSN1=PCWF.COR.P1617CWD.ENRTACT.UPD.EXT              -
  &DSN2=npart.activity_enrte_temp.upd                  -
  &OUTOPTS=:datatype=text:xlate=yes:strip.blanks=no:   -
  &ERRMSG='P1618CWX.NDMS060'                           -
  MAXDELAY=UNLIMITED CASE=YES
  SIGNOFF
/*
//*
