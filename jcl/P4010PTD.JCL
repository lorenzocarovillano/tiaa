//P4010PTD JOB (P,66800,000,OPSS),'CCP - PSLOAN',
//         CLASS=1,
//         MSGCLASS=I
//* --------------------------------------------------------------------
//*                                                                     
//OUT1     OUTPUT CLASS=8
//OUT2     OUTPUT CLASS=A,DEST=TXFMML
//OUT3     OUTPUT CLASS=A,DEST=TXFSML
//*                                                                     
//* --------------------------------------------------------------------
//* PSLOAN  - LEGACY LOAN BILL STATEMENT - LOANS2
//*           JOB CREATES A TEXT FILE CONTAINING UP TO 12,000
//*           REQUESTS. FILE WILL THEN BE SENT TO CCP BY PPT401JX.
//* --------------------------------------------------------------------
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*
//JOBLIB   DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//       DD DSN=PROD.BATCH.LOADLIB,DISP=SHR
//*
//*********************************************************************
//P4010PTD EXEC PCC5000R,
//       PACKAGE=PSLOAN
//*
//*********************************************************************
