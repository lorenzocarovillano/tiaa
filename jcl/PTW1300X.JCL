//PTW1300X JOB (P,14400,000,FINB),CAIMS,MSGCLASS=I,CLASS=6,REGION=8M    
//*                                                                     
//* CA - CALIFORNIA QUARTERLY                                           
//*                                                                     
//*--------------------------------------------------------------------*
//* NDMS010 - TRANSMITS STATE MAG MEDIA TAPE FILE TO A SERVER.         *
//*           THE FILE MUST BE COPIED TO A CD OR DVD BY A DULY         *
//*           AUTHORIZED REPRESENTATIVE OF THE TAX BUSINESS UNIT,      *
//*           AND PREPARED FOR DELIVERY TO THE STATE TAXATION          *
//*           AUTHORITY VIA BONDED COURIER.                            *
//*           NDM UTILITY PROGRAM "DMBATCH".                           *
//*--------------------------------------------------------------------*
//*                                                                     
//NDMS010  EXEC PGM=DMBATCH,REGION=4M,PARM=(YYSLYNN),COND=(0,NE)        
//DMNETMAP DD DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR                   
//DMPUBLIB DD DSN=PMVSPP.STERLING.PROCESS.LIB,DISP=SHR                  
//DMMSGFIL DD DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR                      
//DMPRINT  DD SYSOUT=*                                                  
//NDMCMDS  DD SYSOUT=*                                                  
//SYSUDUMP DD SYSOUT=U                                                  
//SYSIN    DD *                                                         
  SIGNON ESF=YES                                                        
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(DLNA1300) MAXDELAY=UNLIMITED       
  SIGNOFF                                                               
/*                                                                      
