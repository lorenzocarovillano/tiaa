//PCW1615X JOB (C,67412,000,LEGB),'NDM CWF-EXTR',MSGCLASS=I,CLASS=6
//*
//*********************************************************************
//*                     JOB DESCRIPTION                               *
//*-------------------------------------------------------------------*
//* THIS JOB TRANSMITS 6 CWF-PARTICIPANT EXTRACT FILES,CREATED BY JOB *
//* P1615CWD, TO THE LINUX SERVER CHAPDA3ORCCP01.                     *
//*********************************************************************
//*                     MODIFICATION LOG                              *
//*-------------------------------------------------------------------*
//* CHANGE REQUEST NUMBER : CHG319449                                 *
//* IMPLEMENTATION DATE : 11/22/2014                                  *
//* AUTHOR : COGNIZANT                                                *
//* VERSION :INITIAL VERSION.                                         *
//*          CREATED IN PROJECT - "Mainframe Oracle Retirement - ORP3"*
//*          (TC00002875)                                             *
//*********************************************************************
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*
//********************************************************************
//* TRANSMITS PCWF.COR.P1615CWD.TOT04.CONTROL FILE TO LINUX SERVER
//********************************************************************
//NDMS010  EXEC PGM=DMBATCH,REGION=4M,PARM=(YYSLYNN),TIME=1440,
//             COND=(0,NE)
//DMMSGFIL DD  DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR
//DMNETMAP DD  DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR
//DMPUBLIB DD  DSN=PMVSPP.STERLING.PROCESS,DISP=SHR
//DMPRINT  DD  SYSOUT=*
//NDMCMDS   DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=U
//SYSIN     DD *
  SIGNON
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(CWF000X1)         -
  &DSN1=PCWF.COR.P1615CWD.TOT04.CONTROL                -
  &DSN2=part.control_totals_table                      -
  &OUTOPTS=:datatype=text:xlate=yes:strip.blanks=no:   -
  &ERRMSG='PCW1615X.NDMS010'                           -
  MAXDELAY=UNLIMITED CASE=YES
  SIGNOFF
/*
//*
//********************************************************************
//* TRANSMITS PCWF.COR.P1615CWD.WRKRQST.DEL.EXT FILE TO LINUX SERVER
//********************************************************************
//NDMS020  EXEC PGM=DMBATCH,REGION=4M,PARM=(YYSLYNN),TIME=1440,
//             COND=(0,NE)
//DMMSGFIL DD  DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR
//DMNETMAP DD  DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR
//DMPUBLIB DD  DSN=PMVSPP.STERLING.PROCESS,DISP=SHR
//DMPRINT  DD  SYSOUT=*
//NDMCMDS   DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=U
//SYSIN     DD *
  SIGNON
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(CWF000X1)         -
  &DSN1=PCWF.COR.P1615CWD.WRKRQST.DEL.EXT              -
  &DSN2=part.delete_merges_table.rpl                   -
  &OUTOPTS=:datatype=text:xlate=yes:strip.blanks=no:   -
  &ERRMSG='PCW1615X.NDMS020'                           -
  MAXDELAY=UNLIMITED CASE=YES
  SIGNOFF
/*
//*
//********************************************************************
//* TRANSMITS PCWF.COR.P1615CWD.WRKRQST.ADD.EXT FILE TO LINUX SERVER
//********************************************************************
//NDMS030  EXEC PGM=DMBATCH,REGION=4M,PARM=(YYSLYNN),TIME=1440,
//             COND=(0,NE)
//DMMSGFIL DD  DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR
//DMNETMAP DD  DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR
//DMPUBLIB DD  DSN=PMVSPP.STERLING.PROCESS,DISP=SHR
//DMPRINT  DD  SYSOUT=*
//NDMCMDS   DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=U
//SYSIN     DD *
  SIGNON
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(CWF000X1)         -
  &DSN1=PCWF.COR.P1615CWD.WRKRQST.ADD.EXT              -
  &DSN2=part.work_request.add                          -
  &OUTOPTS=:datatype=text:xlate=yes:strip.blanks=no:   -
  &ERRMSG='PCW1615X.NDMS030'                           -
  MAXDELAY=UNLIMITED CASE=YES
  SIGNOFF
/*
//*
//********************************************************************
//* TRANSMITS PCWF.COR.P1615CWD.WRKRQST.UPD.EXT FILE TO LINUX SERVER
//********************************************************************
//NDMS040  EXEC PGM=DMBATCH,REGION=4M,PARM=(YYSLYNN),TIME=1440,
//             COND=(0,NE)
//DMMSGFIL DD  DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR
//DMNETMAP DD  DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR
//DMPUBLIB DD  DSN=PMVSPP.STERLING.PROCESS,DISP=SHR
//DMPRINT  DD  SYSOUT=*
//NDMCMDS   DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=U
//SYSIN     DD *
  SIGNON
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(CWF000X1)         -
  &DSN1=PCWF.COR.P1615CWD.WRKRQST.UPD.EXT              -
  &DSN2=part.work_request_temp.upd                     -
  &OUTOPTS=:datatype=text:xlate=yes:strip.blanks=no:   -
  &ERRMSG='PCW1615X.NDMS040'                           -
  MAXDELAY=UNLIMITED CASE=YES
  SIGNOFF
/*
//*
//********************************************************************
//* TRANSMITS PCWF.COR.P1615CWD.CONTRCT.ADD.EXT FILE TO LINUX SERVER
//********************************************************************
//NDMS050  EXEC PGM=DMBATCH,REGION=4M,PARM=(YYSLYNN),TIME=1440,
//             COND=(0,NE)
//DMMSGFIL DD  DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR
//DMNETMAP DD  DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR
//DMPUBLIB DD  DSN=PMVSPP.STERLING.PROCESS,DISP=SHR
//DMPRINT  DD  SYSOUT=*
//NDMCMDS   DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=U
//SYSIN     DD *
  SIGNON
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(CWF000X1)         -
  &DSN1=PCWF.COR.P1615CWD.CONTRCT.ADD.EXT              -
  &DSN2=part.contract_number_add                       -
  &OUTOPTS=:datatype=text:xlate=yes:strip.blanks=no:   -
  &ERRMSG='PCW1615X.NDMS050'                           -
  MAXDELAY=UNLIMITED CASE=YES
  SIGNOFF
/*
//*
//********************************************************************
//* TRANSMITS PCWF.COR.P1615CWD.CONTRCT.UPD.EXT FILE TO LINUX SERVER
//********************************************************************
//NDMS060  EXEC PGM=DMBATCH,REGION=4M,PARM=(YYSLYNN),TIME=1440,
//             COND=(0,NE)
//DMMSGFIL DD  DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR
//DMNETMAP DD  DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR
//DMPUBLIB DD  DSN=PMVSPP.STERLING.PROCESS,DISP=SHR
//DMPRINT  DD  SYSOUT=*
//NDMCMDS   DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=U
//SYSIN     DD *
  SIGNON
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(CWF000X1)         -
  &DSN1=PCWF.COR.P1615CWD.CONTRCT.UPD.EXT              -
  &DSN2=part.contract_number_temp.upd                  -
  &OUTOPTS=:datatype=text:xlate=yes:strip.blanks=no:   -
  &ERRMSG='PCW1615X.NDMS060'                           -
  MAXDELAY=UNLIMITED CASE=YES
  SIGNOFF
/*
//*
