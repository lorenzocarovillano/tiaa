//PAN2016X JOB (P,66810,000,OPSB),'ADAS',MSGCLASS=I,
//             CLASS=6,REGION=8M,TIME=1440
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*
//* Copy PDA.ANN.PAN2015D.CTLS.EXTRCT(0) file to
//*          /pd1/resrc_disk1/DS_OLTP_CRM/source/CA
//*
//*********************************************************************
//*        DEALLOCATE TRIGGER FILE
//*********************************************************************
//DELT001  EXEC PGM=IEFBR14
//SYSPRINT DD SYSOUT=*
//TMPFL01  DD DSN=PPDD.PAN2016X.TRGFILE,
//            UNIT=SYSDA,
//            DISP=(MOD,DELETE,DELETE),
//            SPACE=(CYL,(0,0))
//*********************************************************************
//*        ALLOCATE WORK DATASET - FOR DUMMY TRG FILE
//*********************************************************************
//ALLOCAT2 EXEC PGM=IEFBR14,COND=(0,NE)
//SYSPRINT DD SYSOUT=*
//D1       DD DSN=PPDD.PAN2016X.TRGFILE,
//            UNIT=SYSDA,
//            DISP=(NEW,CATLG,DELETE),
//            SPACE=(TRK,(1,1),RLSE),
//            DCB=(DSORG=PS,RECFM=LS,LRECL=80)
//SYSIN    DD DUMMY
//*
//*********************************************************************
//*        SENDS EXTRACTED PARTICIPANT FILE TO MDM
//*********************************************************************
//STEP005 EXEC NDMPROC,COND=(0,NE)
//SYSIN     DD *
 SIGNON
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(AN2016D1)        -
 &INFILE=PDA.ANN.PAN2015D.CTLS.EXTRCT(0)             -
 &OUTPATH=/pd1/resrc_disk1/DS_OLTP_CRM/source/CA     -
 &OUTFILE=/ADAS_SBL                                  -
 CASE=YES MAXDELAY=UNLIMITED
 SIGNOFF
//*********************************************************************
//*        SENDS TRIGGER FILE TO MDM
//*********************************************************************
//STEP010 EXEC NDMPROC,COND=(0,NE)
//SYSIN     DD *
 SIGNON
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(AN2016D2)        -
 &INFILE=PPDD.PAN2016X.TRGFILE                       -
 &OUTPATH=/pd1/resrc_disk1/DS_OLTP_CRM/source/CA     -
 &OUTFILE=/ADAS_SBL                                  -
 CASE=YES MAXDELAY=UNLIMITED
 SIGNOFF
//*
//*
