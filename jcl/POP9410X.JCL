//POP9410X JOB (P,19999,000,OPSB),'OMNIPAY',MSGCLASS=I,CLASS=6,
//          REGION=8M
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//* ------------------------------------------------------------------*
//STEP005 EXEC NDMPROC
//SYSIN     DD *
 SIGNON
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(OP0118D1)                -
 CASE=YES MAXDELAY=UNLIMITED                                 -
 &EXTFIL=POMPY.ESPEXTR.PMTPCC(0)                             -
 &CNTFIL=POMPY.ESPEXTR.PMTPCC.CTL                            -
 &OUTPATH=/pd1/resrc_disk1/ods/source                        -
 &EXTRACT=/OMNIPAY_CC_P_EXT.                                 -
 &CONTROL=/OMNIPAY_CC_P_CONTROL.                             -
 &JOBNAME=POP9410X
  SIGNOFF
//*
//STEP010 EXEC NDMPROC
//SYSIN     DD *
 SIGNON
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(OP0118D1)                -
 CASE=YES MAXDELAY=UNLIMITED                                 -
 &EXTFIL=POMPY.ESPEXTR.PMTPCK(0)                             -
 &CNTFIL=POMPY.ESPEXTR.PMTPCK.CTL                            -
 &OUTPATH=/pd1/resrc_disk1/ods/source                        -
 &EXTRACT=/OMNIPAY_CK_P_EXT.                                 -
 &CONTROL=/OMNIPAY_CK_P_CONTROL.                             -
 &JOBNAME=POP9410X
  SIGNOFF
//*
//STEP015 EXEC NDMPROC
//SYSIN     DD *
 SIGNON
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(OP0118D1)                -
 CASE=YES MAXDELAY=UNLIMITED                                 -
 &EXTFIL=POMPY.ESPEXTR.PMTPCD(0)                             -
 &CNTFIL=POMPY.ESPEXTR.PMTPCD.CTL                            -
 &OUTPATH=/pd1/resrc_disk1/ods/source                        -
 &EXTRACT=/OMNIPAY_CD_P_EXT.                                 -
 &CONTROL=/OMNIPAY_CD_P_CONTROL.                             -
 &JOBNAME=POP9410X
  SIGNOFF
//*
//STEP020 EXEC NDMPROC
//SYSIN     DD *
 SIGNON
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(OP0118D1)                -
 CASE=YES MAXDELAY=UNLIMITED                                 -
 &EXTFIL=POMPY.ESPEXTR.PMTPDS(0)                             -
 &CNTFIL=POMPY.ESPEXTR.PMTPDS.CTL                            -
 &OUTPATH=/pd1/resrc_disk1/ods/source                        -
 &EXTRACT=/OMNIPAY_DS_P_EXT.                                 -
 &CONTROL=/OMNIPAY_DS_P_CONTROL.                             -
 &JOBNAME=POP9410X
  SIGNOFF
//*
//STEP025 EXEC NDMPROC
//SYSIN     DD *
 SIGNON
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(OP0118D1)                -
 CASE=YES MAXDELAY=UNLIMITED                                 -
 &EXTFIL=POMPY.ESPEXTR.PMTPDF(0)                             -
 &CNTFIL=POMPY.ESPEXTR.PMTPDF.CTL                            -
 &OUTPATH=/pd1/resrc_disk1/ods/source                        -
 &EXTRACT=/OMNIPAY_DF_P_EXT.                                 -
 &CONTROL=/OMNIPAY_DF_P_CONTROL.                             -
 &JOBNAME=POP9410X
  SIGNOFF
//*
//STEP030 EXEC NDMPROC
//SYSIN     DD *
 SIGNON
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(OP0118D1)                -
 CASE=YES MAXDELAY=UNLIMITED                                 -
 &EXTFIL=POMPY.ESPEXTR.PMTPAN.RECCC(0)                       -
 &CNTFIL=POMPY.ESPEXTR.PMTPAN.RECCC.CTL                      -
 &OUTPATH=/pd1/resrc_disk1/ods/source                        -
 &EXTRACT=/OMNIPAY_ANCC_P_EXT.                               -
 &CONTROL=/OMNIPAY_ANCC_P_CONTROL.                           -
 &JOBNAME=POP9410X
  SIGNOFF
//*
//STEP035 EXEC NDMPROC
//SYSIN     DD *
 SIGNON
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(OP0118D2)                -
 CASE=YES MAXDELAY=UNLIMITED                                 -
 &INFILE=POMPY.ESPEXTR.PMTPCC(0)                             -
 &OUTPATH=/pd5/resrc_disk1/DS_FSL/source/incremental/omni    -
 &OUTFILE=/OMNIPAY_CC_P_EXT.                                 -
 &JOBNAME=POP9410X
  SIGNOFF
//*
//STEP040 EXEC NDMPROC
//SYSIN     DD *
 SIGNON
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(OP0118D2)                -
 CASE=YES MAXDELAY=UNLIMITED                                 -
 &INFILE=POMPY.ESPEXTR.PMTPCK(0)                             -
 &OUTPATH=/pd5/resrc_disk1/DS_FSL/source/incremental/omni    -
 &OUTFILE=/OMNIPAY_CK_P_EXT.                                 -
 &JOBNAME=POP9410X
  SIGNOFF
//*
//STEP045 EXEC NDMPROC
//SYSIN     DD *
 SIGNON
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(OP0118D2)                -
 CASE=YES MAXDELAY=UNLIMITED                                 -
 &INFILE=POMPY.ESPEXTR.PMTPCD(0)                             -
 &OUTPATH=/pd5/resrc_disk1/DS_FSL/source/incremental/omni    -
 &OUTFILE=/OMNIPAY_CD_P_EXT.                                 -
 &JOBNAME=POP9410X
  SIGNOFF
//*
//STEP050 EXEC NDMPROC
//SYSIN     DD *
 SIGNON
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(OP0118D2)                -
 CASE=YES MAXDELAY=UNLIMITED                                 -
 &INFILE=POMPY.ESPEXTR.PMTPDS(0)                             -
 &OUTPATH=/pd5/resrc_disk1/DS_FSL/source/incremental/omni    -
 &OUTFILE=/OMNIPAY_DS_P_EXT.                                 -
 &JOBNAME=POP9410X
  SIGNOFF
//*
//STEP055 EXEC NDMPROC
//SYSIN     DD *
 SIGNON
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(OP0118D2)                -
 CASE=YES MAXDELAY=UNLIMITED                                 -
 &INFILE=POMPY.ESPEXTR.PMTPDF(0)                             -
 &OUTPATH=/pd5/resrc_disk1/DS_FSL/source/incremental/omni    -
 &OUTFILE=/OMNIPAY_DF_P_EXT.                                 -
 &JOBNAME=POP9410X
  SIGNOFF
//*
//STEP060 EXEC NDMPROC
//SYSIN     DD *
 SIGNON
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(OP0118D2)                -
 CASE=YES MAXDELAY=UNLIMITED                                 -
 &INFILE=POMPY.ESPEXTR.PMTPAN.RECCC(0)                       -
 &OUTPATH=/pd5/resrc_disk1/DS_FSL/source/incremental/omni    -
 &OUTFILE=/OMNIPAY_ANCC_P_EXT.                               -
 &JOBNAME=POP9410X
  SIGNOFF
//*
