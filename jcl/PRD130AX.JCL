//PRD130AX JOB (P,69999,000,OPSB),'NDM',
//         CLASS=6,MSGCLASS=I
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//******************************************************
//*****COPY CREF FILE FROM SHARED DRIVE*****************
//******************************************************
//NDMS010  EXEC PGM=DMBATCH,REGION=4M,PARM=(YYSLYNN),TIME=1440
//DMNETMAP DD  DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR
//DMPUBLIB DD  DSN=PMVSPP.STERLING.PROCESS,DISP=SHR
//DMMSGFIL DD  DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR
//DMPRINT  DD  SYSOUT=*
//SYSIN     DD *
  SIGNON ESF=YES
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(RD1301X1) MAXDELAY=UNLIMITED -
  &PATH1=RiderMailings                                     -
  &PATH2=Cref                                              -
  &PATH3=Out                                               -
  &Filename=Cref.txt                                       -
  &FILEOUT=PDA.ANN.RIDER.SHARE.DATA                        -
  &LENGTH=255                                              -
  CASE=YES
  SIGNOFF
/*
//*
