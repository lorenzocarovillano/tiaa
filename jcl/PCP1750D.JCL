//PCP1750D JOB (P,78000,000,FINS),'OMNIPAY XML',CLASS=1,                
//       MSGCLASS=I                                                     
//*                                                                     
//LIBSRCH  JCLLIB ORDER=(PROD.OAS.NY.PARMLIB)                           
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*--------------------------------------------------------------------*
//*      CONSOLIDATED PAYMENT SYSTEMS (CPS)                            *
//*      * PCP1860D  DAILY FOR GENERIC WARRANTS TO OMNIPRO             *
//*--------------------------------------------------------------------*
//*                                                                     
//JOBLIB   DD DSN=PROD.BATCH.LOADLIB,DISP=SHR                           
//         DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR                
//*                                                                     
//PCP1750D EXEC PCP1750D                                                
//*                                                                     
