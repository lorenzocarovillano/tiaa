//PAN9000X JOB (P,14000,000,OPSB),'ADAM',                               
//          CLASS=6,                                                    
//          MSGCLASS=I                                                  
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB                              
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//NDM0010  EXEC PGM=DMBATCH,REGION=4M,                                  
//        PARM=(YYSLYNN),                                               
//        COND=(0,NE)                                                   
//DMNETMAP DD DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR                   
//DMPUBLIB DD DSN=PMVSPP.STERLING.PROCESS.LIB,DISP=SHR                  
//DMMSGFIL DD DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR                      
//DMPRINT  DD SYSOUT=*                                                  
//NDMCMDS  DD  SYSOUT=*                                                 
//SYSUDUMP DD SYSOUT=U                                                  
//SYSIN    DD *                                                         
  SIGNON ESF=YES                                                        
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(DLNA9000) MAXDELAY=UNLIMITED -     
  CASE=YES                                                              
  SIGNOFF                                                               
//*                                                                     
//NDM0020  EXEC PGM=DMBATCH,REGION=4M,                                  
//        PARM=(YYSLYNN),                                               
//        COND=(0,NE)                                                   
//DMNETMAP DD DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR                   
//DMPUBLIB DD DSN=PMVSPP.STERLING.PROCESS.LIB,DISP=SHR                  
//DMMSGFIL DD DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR                      
//DMPRINT  DD SYSOUT=*                                                  
//NDMCMDS  DD  SYSOUT=*                                                 
//SYSUDUMP DD SYSOUT=U                                                  
//SYSIN    DD *                                                         
  SIGNON ESF=YES                                                        
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(DLNA9001) MAXDELAY=UNLIMITED -     
  CASE=YES                                                              
  SIGNOFF                                                               
//*                                                                     
//                                                                      
