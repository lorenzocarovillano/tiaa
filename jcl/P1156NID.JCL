//P1156NID JOB (P,67412,000,OPSB),                                      
//             WIL,                                                     
//             CLASS=2,                                                 
//             MSGCLASS=I,                                              
//             MSGLEVEL=(1,1)                                           
//**                                                                    
//OUTSAR OUTPUT JESDS=ALL,DEFAULT=YES,DEST=MVSJESD                      
//*N                                                                    
//**                                                                    
//*                                                                     
//*                                                                     
//**********************************************************************
//STEP01   EXEC PGM=IEBGENER                                            
//SYSPRINT DD  SYSOUT=*                                                 
//SYSUT1   DD  DSN=PDNT.ACIS.DOCUMERG.P1155NID.DN(0),                   
//             DISP=SHR                                                 
//SYSUT2   DD  SYSOUT=(F,,1155),                                        
//             DEST=U1700,                                              
//             BLKSIZE=3000,                                            
//             LRECL=155,                                               
//             RECFM=VBM                                                
//SYSIN    DD  DUMMY                                                    
//                                                                      
