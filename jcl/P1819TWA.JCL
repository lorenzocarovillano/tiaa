//P1819TWA JOB (P,14400,000,FINS),ORIG-STRPT,MSGCLASS=I,                
//        CLASS=1,REGION=8M                                             
//*                                                                     
//*                                                                     
//JOBLIB  DD DSN=PROD.BATCH.LOADLIB,DISP=SHR                            
//        DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR                 
//*                                                                     
//P1819TWA EXEC P180DTWA,STATE='KS',STNUM=P1819TW1                      
//FRMT030.CMPRT01 DD SYSOUT=(&REPT.,TW1819A3)                           
//FRMT030.CMPRT02 DD SYSOUT=(&REPT.,TW1819A4)                           
//FRMT030.CMPRT03 DD SYSOUT=(&REPT.,TW1819A5)                           
//FRMT030.CMPRT04 DD SYSOUT=(&REPT.,TW1819A6)                           
//FRMT030.CMPRT05 DD SYSOUT=(&REPT.,TW1819A7)                           
//*KANSAS                                                               
/*                                                                      
