//POP9190X JOB (P,19999,000,OPSB),'OMNIPAY',CLASS=6,                    
//         MSGCLASS=I                                                   
//* ------------------------------------------------------------------* 
//LIBSRCH  JCLLIB ORDER=PROD.OAS.NY.PARMLIB                             
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//* ------------------------------------------------------------------* 
//SORT010  EXEC PGM=SORT
//SYSOUT   DD  SYSOUT=*
//SYSPRINT DD  SYSOUT=*
//SYMNAMES DD DSN=PROD.PARMLIB(OP0370X2),DISP=SHR
//SORTIN   DD DSN=PROD.NDM.NY.PROCESS.LIB(OP0370D1),DISP=SHR
//SORTOUT  DD DSN=PROD.NDM.NY.PROCESS.LIB(OP0370D2),DISP=SHR
//SYSIN    DD DSN=PROD.PARMLIB(OP0370X3),DISP=SHR
//* ------------------------------------------------------------------*
//NDMS020 EXEC PGM=DMBATCH,REGION=4M,PARM=(YYSLYNN)                     
//DMNETMAP DD DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR                   
//DMPUBLIB DD DSN=PMVSPP.STERLING.PROCESS.LIB,DISP=SHR                  
//DMMSGFIL DD DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR                      
//DMPRINT   DD SYSOUT=*                                                 
//NDMCMDS   DD SYSOUT=*                                                 
//SYSUDUMP  DD SYSOUT=U                                                 
//SYSIN     DD *                                                        
 SIGNON                                                                 
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(OP0370D2) -                         
 MAXDELAY=UNLIMITED                           -                         
 CASE=YES                                     -                         
 &DSNI=POMPY.PMTP.LETTR60(0)                  -                         
 &OUTFIL=/ecs-app/ccp/event-processor/drop/034001- -
 &SUFFIX=xml                                                            
 SIGNOFF                                                                
