//POP0702D JOB (C,19999,000,OPSB),'OMNIPAY',CLASS=1,                    
//         MSGCLASS=I,PRTY=11,REGION=0M                                 
//*                                                                     
//LIBSRCH  JCLLIB ORDER=(PROD.OAS.NY.PARMLIB)                           
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*                                                                     
//*MSYS // INCLUDE MEMBER=JOBLIBU                        
//JOBLIB   DD  DISP=SHR,DSN=PDBFPP.OMNIPAY.TIAA.BAT.LOADLIB             00011001
//         DD  DISP=SHR,DSN=PDBFPP.OMNIPAY.CURRENT.BAT.LOADLIB          00012001
//         DD  DISP=SHR,DSN=PDBFPP.OMNIPAY.TIAA.ONLINE.LOADLIB          00013001
//         DD  DISP=SHR,DSN=PDBFPP.OMNIPAY.CURRENT.ONLINE.LOADLIB
//         DD  DISP=SHR,DSN=PROD.BATCH.LOADLIB
//*                                                                     
//*-----------------------------------------------------------------*   
//*  STEP TO MERGE ALL EOD CC EXTRACT INTO A SINGLE CC EXTR FILE        
//*------------------------------------------------------------------*  
//MERGECC EXEC TIAMRGCC,                                                
//   PRF='ED',                                                          
//   CURJN='POP0702D',                                                  
//   VFSET='PMT0'                                                       
//*------------------------------------------------------------------*  
//*  STEP TO OVERLAY ADDRESS FROM ADVICE RECS TO CHECK/EFT PYMNTS       
//*------------------------------------------------------------------*  
//*                                                                     
//CCXPREP EXEC TIACRPRP,                                                
//  CURJN='POP0702D',                                                   
//  CCJOBN='POP0702D',                                                  
//  GDG0='(+1)',                                                        
//  ADRSRP='OP0702D4',                                                  
//  AOVRLVS='OPADVSF0',                                                 
//  VFSET='PMT0',                                                       
//  PRF='ED'                                                            
//*                                                                     
//*-----------------------------------------------------------------*   
//*  STEP TO CREATE FILESET 0 CCP XML FEED - END OF DAY RUN             
//*------------------------------------------------------------------*  
//CHKFEED EXEC TIACRCCP,                                                
//  VFSET='PMT0',                                                       
//  GDG0='(+1)',                                                        
//  FREQ='D',                                                           
//  PRF='ED',                                                           
//  JNAM1='POP0702D',                                                   
//  CCJOBN='POP0702D',                                                  
//  TOTRPT='OP0702D1',                                                  
//  REJRPT='OP0702D2',                                                  
//  BYPRPT='OP0702D3',                                                  
//  PRCRPT='OP0702D5'                                                   
//*                                                                     
//CRCCPFD.APXMLCHK DD DUMMY                                             
//CRCCPFD.APXMLLPR DD DUMMY                                             
//CRCCPFD.APXMLNCK DD DUMMY                                             
//*                                                                     
//*------------------------------------------------------------------*  
//*  STEP TO CREATE CONTROL FILE OF XML FEED - EOD RUN - CHECK          
//*------------------------------------------------------------------*  
//CHKCTRL EXEC TIACRCTL,                                                
//  PJOBNM='POP0702D',                                                  
//  FT1='CHK',                                                          
//  VFSET='PMT0',                                                       
//  GDG0='(+1)'                                                         
//*                                                                     
//*------------------------------------------------------------------*  
//*  STEP TO CREATE CONTROL FILE OF XML FEED - EOD RUN - NON-CHECK      
//*------------------------------------------------------------------*  
//NCKCTRL EXEC TIACRCTL,                                                
//  PJOBNM='POP0702D',                                                  
//  FT1='NCK',                                                          
//  VFSET='PMT0',                                                       
//  GDG0='(+1)'                                                         
//*                                                                     
//*------------------------------------------------------------------*
//*  STEP TO CREATE PI.REFUND FILE FOR EXPAG TRANSMISSION
//*------------------------------------------------------------------*
//CREPIIRF EXEC TIAPIIRF,
//  VFSET='PMT0',
//  PRF='ED',
//  CCJOBN='POP0702D',
//  JNAM1='POP0702D',
//  GDG0='(+1)',
//  GDG1='(+1)'
//*
