//PCP2315X JOB (P,66231,000,FINB),CES,MSGCLASS=I,CLASS=6,REGION=8M      
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB                              
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//************************************************************          
//* COPY PAA.ANN.PDW1070D.DEDUCTNS.EXPAND TO PDFINWSQL01CHA  *          
//************************************************************          
//NDMS010 EXEC PGM=DMBATCH,REGION=4M,PARM=(YYSLYNN)                     
//DMNETMAP DD DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR                   
//DMPUBLIB DD DSN=PMVSPP.STERLING.PROCESS.LIB,DISP=SHR                  
//DMMSGFIL DD DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR                      
//DMPRINT   DD SYSOUT=*,OUTPUT=*.OUTVDR                                 
//NDMCMDS   DD SYSOUT=*,OUTPUT=*.OUTVDR                                 
//SYSUDUMP  DD SYSOUT=U,OUTPUT=*.OUTLOCL                                
//SYSIN     DD *                                                        
  SIGNON ESF=YES                                                        
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(CP2315M2) MAXDELAY=UNLIMITED       
  SIGNOFF                                                               
