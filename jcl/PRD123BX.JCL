//PRD123BX JOB (P,69999,000,OPSB),'NDM',
//         CLASS=6,MSGCLASS=I
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//******************************************************
//******************************************************
//***      SENDING EMPTY FILE TO DRIVE      ************
//******************************************************
//NDMS010  EXEC PGM=DMBATCH,REGION=4M,PARM=(YYSLYNN),TIME=1440
//DMNETMAP DD  DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR
//DMPUBLIB DD  DSN=PMVSPP.STERLING.PROCESS,DISP=SHR
//DMMSGFIL DD  DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR
//DMPRINT  DD  SYSOUT=*
//SYSIN     DD *
  SIGNON ESF=YES
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(RD1301X2) MAXDELAY=UNLIMITED -
  &PATH1=RiderMailings                                     -
  &PATH2=MDO                                               -
  &PATH3=Out                                               -
  &Filename=MDO.txt                                      -
  &FILEIN=POMPL.FA.ADHOCREQ.EMPTY                  -
  CASE=YES
  SIGNOFF
//******************************************************
//**********************************************************************
