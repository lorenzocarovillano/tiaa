//POP1400D JOB (P,19999,000,OPSB),'OMNIPAY',CLASS=1,                    
//         MSGCLASS=I                                                   
//*                                                                     
//LIBSRCH  JCLLIB ORDER=(PROD.OAS.NY.PARMLIB)                           
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*                                                                     
//*MSYS // INCLUDE MEMBER=JOBLIBU                        
//JOBLIB   DD  DISP=SHR,DSN=PDBFPP.OMNIPAY.TIAA.BAT.LOADLIB             00011001
//         DD  DISP=SHR,DSN=PDBFPP.OMNIPAY.CURRENT.BAT.LOADLIB          00012001
//         DD  DISP=SHR,DSN=PDBFPP.OMNIPAY.TIAA.ONLINE.LOADLIB          00013001
//         DD  DISP=SHR,DSN=PDBFPP.OMNIPAY.CURRENT.ONLINE.LOADLIB
//         DD  DISP=SHR,DSN=PROD.BATCH.LOADLIB
//*                                                                     
//********************************************************************* 
//* COPYRIGHT 2007 AVEKSA CORPORATION.  ALL RIGHTS RESERVED             
//********************************************************************* 
//* THIS JOB CREATES THE OMNIPAY IDENTITY AND ENTITLEMENT FILES         
//* FOR THE AVEKSA COMPLIANCE MANAGER AND SENDS THEM VIA FTP TO THE     
//* APPROPRIATE AGENT PLATFORM.                                         
//*                                                                     
//* FOR THIS PROC, PLEASE ENTER VALUES FOR THESE SUBSTITUTION VARIABLES 
//* - OQUAL:    THE HIGH LEVEL QUALIFER(S) FOR THE COLLECTOR DATASETS   
//* - TR83:     THE DSN FOR THE CURRENT VERSION OF THE OMNIPAY TR83     
//*             EXTRACT FILE                                            
//*                                                                     
//********************************************************************* 
//EXTR010  EXEC TIA1400,                                                
//         OQUAL='POMPY.PMT0',                                          
//         TR83='POMPY.PMT0.TR83EXT(0)'                                 
//*                                                                     
//*                                                                     
