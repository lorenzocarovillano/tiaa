//POP2014R JOB (P,19999,000,OPSB),'OMNIPAY',CLASS=1,MSGCLASS=I,         
//         REGION=0M                                                    
//*                                                                     
//*                                                                     
//********************************************************              
//*                                                                     
//*    OMNIPAY 2018 RELEASE                                             
//*    DEFINE FILES, DO ANY NEEDED INITIALIZATION                       
//*                                                                     
//********************************************************              
//*                                                                     
//LIBSRCH  JCLLIB ORDER=(PROD.OAS.NY.PARMLIB)                           
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*                                                                     
//*MSYS // INCLUDE MEMBER=JOBLIBU                        
//JOBLIB   DD  DISP=SHR,DSN=PDBFPP.OMNIPAY.TIAA.BAT.LOADLIB             00011001
//         DD  DISP=SHR,DSN=PDBFPP.OMNIPAY.CURRENT.BAT.LOADLIB          00012001
//         DD  DISP=SHR,DSN=PDBFPP.OMNIPAY.TIAA.ONLINE.LOADLIB          00013001
//         DD  DISP=SHR,DSN=PDBFPP.OMNIPAY.CURRENT.ONLINE.LOADLIB
//         DD  DISP=SHR,DSN=PROD.BATCH.LOADLIB
//*                                                                     
//   SET VFSET='PMTQ'                                                   
//   SET LPREFX='PROD.OMNIPAY.BAT'                                      
//*                                                                     
//*-----------------------------------------------------------------*   
//*  DELETE/DEFINE NEXT GEN VSAM MASTER FILES                           
//*-----------------------------------------------------------------*   
//DELDEF   EXEC  PGM=IDCAMS                                             
//*                                                                     
//*                                                                     
//SYSPRINT DD  SYSOUT=*                                                 
//SYSIN    DD  DSN=&LPREFX..CTL(DEF&VFSET),DISP=SHR                     
//*                                                                     
//*-----------------------------------------------------------------*   
//*  DELETE/DEFINE NEXT GEN VSAM ALT INDEX FILES                        
//*-----------------------------------------------------------------*   
//DELDEF2  EXEC  PGM=IDCAMS,COND=(0,NE)                                 
//*                                                                     
//SYSPRINT DD  SYSOUT=*                                                 
//SYSIN    DD  DSN=&LPREFX..CTL(AUX&VFSET),DISP=SHR                     
//*                                                                     
