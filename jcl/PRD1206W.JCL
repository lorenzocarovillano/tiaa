//PRD1206W JOB (P,66955,000,LEGS),'STABLE VALUE',
//         MSGCLASS=I,
//         CLASS=1,
//         REGION=9M
//**********************************************************************
//* MODIFICATION LOG:
//*---------------------------------------------------------------------
//* DATE       NAME            CHG#      DESCRIPTION
//*---------------------------------------------------------------------
//* 01/14/2016 Y. MONTESCLAROS CHG368042 INITIAL SET UP FOR AUTOMATION
//*                                      COPIED FROM PRD1206R
//* 02/05/2016 COGNIZANT       CHG375243 ADD THE BATCH LOAD LIBRARY TO
//*                                      THE JOBLIB
//**********************************************************************
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//JOBLIB   DD  DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//         DD  DSN=PROD.BATCH.LOADLIB,DISP=SHR
//*
//*****************************************************************
//* READ DA REPORTING EXTRACT & SPLIT BY NUMBER OF CONTRACTS
//*****************************************************************
//PRD1206W EXEC PRD1206W,
//      PARMMEM=RD1206R1
//*****************************************************************
//* REPORTS
//*****************************************************************
//EXTR015.OREPORT DD SYSOUT=(&REPT.,RD1206R2)
//EXTR020.CMPRT01 DD SYSOUT=(&REPT.,RD1206R1)
