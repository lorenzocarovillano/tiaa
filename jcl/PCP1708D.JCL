//PCP1708D JOB (C,69999,000,FINS),'CPS/IS CHECKS',CLASS=1,              
//       MSGCLASS=I,PRTY=12                             MOBIUS >60 BRK  
//*                                                                     
//LIBSRCH  JCLLIB ORDER=(PROD.OAS.NY.PARMLIB)                           
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*--------------------------------------------------------------------*
//*      O P E N   I N V E S T M E N T S   A R C H I T E C T U R E     *
//*                   *** PCP1708D ***                                 *
//*      CPS - CHECK STATEMENTS SPOOLING TO ZEROX  MICR PRINTER        *
//*      ZEROX PRODUCTION - CHECKS = DEST=TXLBCHK                      *
//*      ZEROX PRODUCTION - EFT    = DEST=TXLBEFT                      *
//*      ZEROX TEST         CHECKS = DEST=TTLBCHK                      *
//*      ZEROX TEST       - EFT    = DEST=TTLBEFT                      *
//*--------------------------------------------------------------------*
//JOBLIB   DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR         PROD   
//         DD DSN=PROD.BATCH.LOADLIB,DISP=SHR                    PROD   
//*                                                                     
//PCP1708D EXEC PCP1707D,           1708 SHARES 1707 PROC.              
//         PRTDFLT='TXLBCHK',       ZEROX  PRINTER DESTINATION          
//         FORMTYP='CPWOIAC',                                           
//         OUTCLS=S,                CHECK/EFT OUTPUT CLASS              
//         CHKEFT='CHK.WITHHOLD',   'EFT WITH HOLD' FOR 1706/1708/1711  
//         RPTID='CP1708D',         MOBIUS REPORT ID                    
//         GDG0='0'                 EXISTING GDG                        
//****                                                                  
