//POP0708X JOB (C,19999,000,OPSB),'OMNIPAY',CLASS=1,                    
//         MSGCLASS=I,PRTY=12,REGION=0M                                 
//*                                                                     
//LIBSRCH  JCLLIB ORDER=(PROD.OAS.NY.PARMLIB)                           
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*                                                                     
//*MSYS // INCLUDE MEMBER=JOBLIBU                        
//JOBLIB   DD  DISP=SHR,DSN=PDBFPP.OMNIPAY.TIAA.BAT.LOADLIB             00011001
//         DD  DISP=SHR,DSN=PDBFPP.OMNIPAY.CURRENT.BAT.LOADLIB          00012001
//         DD  DISP=SHR,DSN=PDBFPP.OMNIPAY.TIAA.ONLINE.LOADLIB          00013001
//         DD  DISP=SHR,DSN=PDBFPP.OMNIPAY.CURRENT.ONLINE.LOADLIB
//         DD  DISP=SHR,DSN=PROD.BATCH.LOADLIB
//*                                                                     
//*-----------------------------------------------------------------*   
//*  CCP *NON-CHECK* FEED XML TRANSMISSION JOB - *FS 0* *END OF DAY*    
//*------------------------------------------------------------------*  
//*                                                                     
//XMIT1   EXEC TIACRXMT,                                                
//*                                                                     
//    SS=002,               *** SUBSYSTEM FOR CCP FILE                  
//    FT1=NCK,              *** MAINFRAME LAST QUALIFIER CHK / NCK /LPR 
//    PJOBNM=POP0702D,      *** PREV JOB NAME                           
//    XMLT=XML,             ***  XML FOR DAILY, MXML - MONTHLY IA       
//    VFSET=PMT0,           ***  FILE                                   
//    CJ=POP0708X,          *** CURR JOB NAME                           
//*                                                                     
//    LIB1=PROD,            *** WMQFTE.PARMLIB PREFIX                   
//    HLQ1=POMPY            *** HIGH LEVEL QUALIFIER                    
//*                                                                     
