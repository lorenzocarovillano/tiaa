//POP0112X JOB (P,66231,000,OPSB),'OMNIPAY EFT PRD',                    
//        CLASS=6,MSGCLASS=I,                                           
//        REGION=1024K,TIME=1440                                        
//*                                                                     
//****************************************************                  
//*                                                                     
//*   POP0112X -- PURPOSE:  JPMC OUTBOUND TRANSMISSION                  
//*               TYPE:     ACH DEPOSITS (MULTIPLE BANK MODELS)         
//*                                                                     
//****************************************************                  
//*                                                                     
//***********************************************                       
//*                                                                     
//*   NDMS010  -- PURPOSE:  JPMC OUTBOUND TRANSMISSION                  
//*               TYPE:     ACH DEPOSITS (TRUST BANK MODEL)             
//*               FILE:     POMPY.PMT0.EFT.TRS(0)                       
//*                                                                     
//***********************************************                       
//NDMS010  EXEC PGM=DMBATCH,PARM=(NYSSNNN),COND=(0,LT)                  
//DMNETMAP DD DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR                   
//DMMSGFIL DD DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR                      
//DMPUBLIB DD DSN=PROD.NDM.NY.PROCESS.LIB,DISP=SHR                      
//DMPRINT  DD SYSOUT=*                                                  
//NDMCMDS  DD SYSOUT=*                                                  
//SYSUDUMP DD SYSOUT=U                                                  
//SYSIN    DD *                                                         
  SIGNON                                                                
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(OP0110D1)        -                 
  MAXDELAY=UNLIMITED                                  -
  CASE=YES                                            -                 
  &LOCDSN=POMPY.PMT0.EFT.TRS(0)                       -                 
  &RMTNODE=TC_INT_PDCD                                -                 
  &SIDIR="/mailbox/OmniPay/JPMC/"                     -                 
  &DTYPE=ACH                                          -                 
  &DFORMAT=NACHA4                                     -                 
  &RMTFN="OP0112D1.TXT"                                                 
  SIGNOFF                                                               
//*                                                                     
//***********************************************                       
//*                                                                     
//*   NDMS020  -- PURPOSE:  JPMC OUTBOUND TRANSMISSION                  
//*               TYPE:     ACH DEPOSITS (ANNUITY BANK MODEL)           
//*               FILE:     POMPY.PMT0.EFT.ANN(0)                       
//*                                                                     
//***********************************************                       
//NDMS020  EXEC PGM=DMBATCH,PARM=(NYSSNNN),COND=(0,LT)                  
//DMNETMAP DD DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR                   
//DMMSGFIL DD DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR                      
//DMPUBLIB DD DSN=PROD.NDM.NY.PROCESS.LIB,DISP=SHR                      
//DMPRINT  DD SYSOUT=*                                                  
//NDMCMDS  DD SYSOUT=*                                                  
//SYSUDUMP DD SYSOUT=U                                                  
//SYSIN    DD *                                                         
  SIGNON                                                                
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(OP0110D1)        -                 
  MAXDELAY=UNLIMITED                                  -
  CASE=YES                                            -                 
  &LOCDSN=POMPY.PMT0.EFT.ANN(0)                       -                 
  &RMTNODE=TC_INT_PDCD                                -                 
  &SIDIR="/mailbox/OmniPay/JPMC/"                     -                 
  &DTYPE=ACH                                          -                 
  &DFORMAT=NACHA15                                    -                 
  &RMTFN="OP0112D2.TXT"                                                 
  SIGNOFF                                                               
//*                                                                     
//***********************************************                       
//*                                                                     
//*   NDMS030  -- PURPOSE:  JPMC OUTBOUND TRANSMISSION                  
//*               TYPE:     ACH DEPOSITS (ANNUITY BANK MODEL)           
//*               FILE:     POMPY.PMT0.EFT.IRA(0)                       
//*                                                                     
//***********************************************                       
//NDMS030  EXEC PGM=DMBATCH,PARM=(NYSSNNN),COND=(0,LT)                  
//DMNETMAP DD DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR                   
//DMMSGFIL DD DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR                      
//DMPUBLIB DD DSN=PROD.NDM.NY.PROCESS.LIB,DISP=SHR                      
//DMPRINT  DD SYSOUT=*                                                  
//NDMCMDS  DD SYSOUT=*                                                  
//SYSUDUMP DD SYSOUT=U                                                  
//SYSIN    DD *                                                         
  SIGNON                                                                
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(OP0110D1)        -                 
  MAXDELAY=UNLIMITED                                  -
  CASE=YES                                            -                 
  &LOCDSN=POMPY.PMT0.EFT.IRA(0)                       -                 
  &RMTNODE=TC_INT_PDCD                                -                 
  &SIDIR="/mailbox/OmniPay/JPMC/"                     -                 
  &DTYPE=ACH                                          -                 
  &DFORMAT=NACHA10                                    -                 
  &RMTFN="OP0112D3.TXT"                                                 
  SIGNOFF                                                               
//*                                                                     
//***********************************************                       
//*                                                                     
//*   NDMS040  -- PURPOSE:  JPMC OUTBOUND TRANSMISSION                  
//*               TYPE:     ACH DEPOSITS (PA1 BANK MODEL)               
//*               FILE:     POMPY.PMT0.EFT5(0)                          
//*                                                                     
//***********************************************                       
//NDMS040  EXEC PGM=DMBATCH,PARM=(NYSSNNN),COND=(0,LT)                  
//DMNETMAP DD DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR                   
//DMMSGFIL DD DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR                      
//DMPUBLIB DD DSN=PROD.NDM.NY.PROCESS.LIB,DISP=SHR                      
//DMPRINT  DD SYSOUT=*                                                  
//NDMCMDS  DD SYSOUT=*                                                  
//SYSUDUMP DD SYSOUT=U                                                  
//SYSIN    DD *                                                         
  SIGNON                                                                
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(OP0110D1)        -                 
  MAXDELAY=UNLIMITED                                  -
  CASE=YES                                            -                 
  &LOCDSN=POMPY.PMT0.EFT5(0)                          -                 
  &RMTNODE=TC_INT_PDCD                                -                 
  &SIDIR="/mailbox/OmniPay/JPMC/"                     -                 
  &DTYPE=ACH                                          -                 
  &DFORMAT=NACHA11                                    -                 
  &RMTFN="OP0112D4.TXT"                                                 
  SIGNOFF                                                               
//*                                                                     
//***********************************************                       
//*                                                                     
//*   NDMS050  -- PURPOSE:  JPMC OUTBOUND TRANSMISSION                  
//*               TYPE:     ACH DEPOSITS (PA2 BANK MODEL)               
//*               FILE:     POMPY.PMT0.EFT6(0)                          
//*                                                                     
//***********************************************                       
//NDMS050  EXEC PGM=DMBATCH,PARM=(NYSSNNN),COND=(0,LT)                  
//DMNETMAP DD DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR                   
//DMMSGFIL DD DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR                      
//DMPUBLIB DD DSN=PROD.NDM.NY.PROCESS.LIB,DISP=SHR                      
//DMPRINT  DD SYSOUT=*                                                  
//NDMCMDS  DD SYSOUT=*                                                  
//SYSUDUMP DD SYSOUT=U                                                  
//SYSIN    DD *                                                         
  SIGNON                                                                
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(OP0110D1)        -                 
  MAXDELAY=UNLIMITED                                  -
  CASE=YES                                            -                 
  &LOCDSN=POMPY.PMT0.EFT6(0)                          -                 
  &RMTNODE=TC_INT_PDCD                                -                 
  &SIDIR="/mailbox/OmniPay/JPMC/"                     -                 
  &DTYPE=ACH                                          -                 
  &DFORMAT=NACHA12                                    -                 
  &RMTFN="OP0112D5.TXT"                                                 
  SIGNOFF                                                               
//*                                                                     
//***********************************************                       
//*                                                                     
//*   NDMS060  -- PURPOSE:  JPMC OUTBOUND TRANSMISSION                  
//*               TYPE:     ACH DEPOSITS (RHP BANK MODEL)               
//*               FILE:     POMPY.PMT0.EFT.RHP(0)                       
//*                                                                     
//***********************************************                       
//NDMS060  EXEC PGM=DMBATCH,PARM=(NYSSNNN),COND=(0,LT)                  
//DMNETMAP DD DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR                   
//DMMSGFIL DD DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR                      
//DMPUBLIB DD DSN=PROD.NDM.NY.PROCESS.LIB,DISP=SHR                      
//DMPRINT  DD SYSOUT=*                                                  
//NDMCMDS  DD SYSOUT=*                                                  
//SYSUDUMP DD SYSOUT=U                                                  
//SYSIN    DD *                                                         
  SIGNON                                                                
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(OP0110D1)        -                 
  MAXDELAY=UNLIMITED                                  -
  CASE=YES                                            -                 
  &LOCDSN=POMPY.PMT0.EFT.RHP(0)                       -                 
  &RMTNODE=TC_INT_PDCD                                -                 
  &SIDIR="/mailbox/OmniPay/JPMC/"                     -                 
  &DTYPE=ACH                                          -                 
  &DFORMAT=NACHA13                                    -                 
  &RMTFN="OP0112D6.TXT"                                                 
  SIGNOFF                                                               
//                                                                      
