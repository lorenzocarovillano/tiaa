//PTW2000A JOB (P,14400,000,FINS),CPS-NPD,MSGCLASS=I,                   
//        CLASS=1,REGION=8M                                             
//*                                                                     
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//JOBLIB  DD DSN=PROD.BATCH.LOADLIB,DISP=SHR
//        DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//*
//PTW2000A EXEC PTW2000A                                                
//**                                                                    
//**----------------------------------------------------------------**
//** SEND NOTIFICATION INDICATING TAX VENDOR FILE IS READY          **
//**                                                                **
//**----------------------------------------------------------------**
//NOTFY999 EXEC CAJUCMNY,COND=(0,NE)
//SYSPRINT DD  SYSOUT=*
//SYSIN    DD  *
MO ATTN: PTW2000A IS COMPLETE AND RRD-SUNY VENDOR CARTRIDGE IS READY.
/*
//*
