//PRD125BX JOB  (P,66800,000,LEGB),'DCS - RDR2014B',
//         CLASS=6,
//         MSGCLASS=I,
//         MSGLEVEL=(1,1)
//* RDR2014B - STANDARD DELIVERY
//* CONNECT DIRECT IMAGE FILE WITH PI DATA TO IMAGE SERVER
//******************************************************************
//******************************************************************
//*
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*
//******************************************************************
//* EXECUTE PROC P1240PTX
//******************************************************************
//PRD125BX EXEC P1240PTX,COND=(0,NE),
//*
//******************************************************************
//*  CHANGE THIS PARAMETER BEFORE MOVING INTO PRODUCTION
//*  JOB NAME IS PREVIOUS JOB NAME!!!
//******************************************************************
//        JOBNM=PRD1252R,
//*
//******************************************************************
//*   TEST-SPECIFIC PARAMETERS; MUST BE CHANGED FOR PRODUCTION
//******************************************************************
//        HLQ1=PPT.COR.XML
