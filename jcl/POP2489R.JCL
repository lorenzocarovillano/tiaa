//POP2489R JOB (C,19999,000,OPSB),'OMNIPAY',CLASS=1,REGION=0M,          
//         MSGCLASS=I
//*
//LIBSRCH  JCLLIB ORDER=(PROD.OAS.NY.PARMLIB)
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*
//JOBLIB  DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//        DD DSN=PROD.BATCH.LOADLIB,DISP=SHR
//*--------------------------------------------------------------------*
//*      CONSOLIDATED PAYMENT SYSTEMS (CPS)                            *
//*      * EXTRACT FCP-CONS-PAYMENT                                    *
//*      * ADABAS 003/196                                              *
//*--------------------------------------------------------------------*
//*
//EXTR010  EXEC PGM=NATB030,REGION=9M,PARM='SYS=NAT013D'
//DDCARD   DD DSN=PROD.PARMLIB(DBAPP003),DISP=SHR
//SYSPRINT DD SYSOUT=*
//DDPRINT  DD SYSOUT=*
//SYSOUT   DD SYSOUT=*
//SYSUDUMP DD SYSOUT=*
//CMPRINT  DD SYSOUT=*
//CMSYNIN  DD DSN=PROD.OMNIPAY.BAT.CTL(FCPPEXEC),DISP=SHR
//*
//* DATA OUTPUT FILES
//CMWKF01  DD DSN=PCPS.ANN.FCPP2489.CSDED.EXT,
//            DISP=(,CATLG,DELETE),
//            DCB=(MODLDSCB,RECFM=LS,LRECL=50),UNIT=SYSDA,
//            SPACE=(CYL,(100,200),RLSE),
//            DATACLAS=DCPSEXTC
//*
//SORT020  EXEC  PGM=SORT
//SYSPRINT DD  SYSOUT=*
//SYSOUT   DD  SYSOUT=*
//SORTIN   DD  DSN=PCPS.ANN.FCPP2489.CSDED.EXT,DISP=SHR
//SORTOUT  DD  DSN=PCPS.ANN.FCPP2489.CSDED.SRT,
//             DISP=(,CATLG,DELETE),
//             DCB=(MODLDSCB,RECFM=LS,LRECL=50),UNIT=SYSDA,
//             SPACE=(CYL,(100,200),RLSE),
//             DATACLAS=DCPSEXTC
//SYSIN    DD  DSN=PROD.OMNIPAY.BAT.CTL(FCPPSORT),DISP=SHR
//*--------------------------------------------------------------------
//DEFINE   EXEC PGM=IDCAMS
//SYSPRINT DD  SYSOUT=*
//SYSIN    DD  DSN=PROD.OMNIPAY.BAT.CTL(FCPPDELT),DISP=SHR
//*--------------------------------------------------------------------
//REPRO    EXEC PGM=IDCAMS,COND=(0,NE)
//SYSPRINT DD  SYSOUT=*
//INPUT    DD  DSN=PCPS.ANN.FCPP2489.CSDED.SRT,DISP=SHR
//OUTPUT   DD  DSN=PCPS.ANN.CPSEXT.CSDED,DISP=SHR
//SYSIN    DD  DSN=PROD.PARMLIB(REPRO),DISP=SHR
//*
