//PTW1654A JOB (P,14400,000,FINS),'TAX 1099-INT',MSGCLASS=I,
//         REGION=8M,CLASS=1
//*
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//JOBLIB     DD  DSN=PROD.BATCH.LOADLIB,DISP=SHR
//           DD  DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//*********************************************************************
//* NO ACTION BY CA-11 REQUIRED
//*********************************************************************
//RMS      EXEC U11RMS,TYPRUN='N'
//*********************************************************************
//*
//* 1099-INT FILE FOR MOBIUS MASS MIGRATION(TAX REFORM)
//*
//*********************************************************************
//STEP010  EXEC PTW1650A,
//         FORMTYPE='F1099I',
//         JOBNM='PTW1654A',
//         PARMNM1='P1654TW1',
//         REPT1='TW1654A',
//         ETID=',ETID=F1099I'
//*-----------------------------------------
