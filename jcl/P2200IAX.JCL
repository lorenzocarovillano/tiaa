//P2200IAX JOB (P,74200,120,LEGB),IA,MSGCLASS=I,CLASS=1                 
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB                              
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//COPY020  EXEC PGM=IEBGENER                                            
//SYSUT1   DD  DSN=PPDT.PNA.IA.FUND.DETAIL.FINAL(0),                    
//             DISP=OLD,UNIT=CARTV                                      
//SYSUT2   DD  DSN=PPDTDR.PNA.IA.FUND.DETAIL.FINAL(+1),                 
//             DISP=(,CATLG,DELETE),                                    
//             UNIT=CARTVDN,                                            
//             DCB=MODLDSCB,RECFM=VB                                    
//SYSIN    DD  DUMMY                                                    
//SYSPRINT DD  SYSOUT=*                                                 
//**********************************************************************
