//POP9742D JOB (C,19999,000,OPSB),'OMNIPAY',CLASS=1,                    
//         MSGCLASS=I,PRTY=11,REGION=0M                                 
//*                                                                     
//LIBSRCH  JCLLIB ORDER=(PROD.OAS.NY.PARMLIB)                           
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*                                                                     
//*MSYS // INCLUDE MEMBER=JOBLIBU                        
//JOBLIB   DD  DISP=SHR,DSN=PDBFPP.OMNIPAY.TIAA.BAT.LOADLIB             00011001
//         DD  DISP=SHR,DSN=PDBFPP.OMNIPAY.CURRENT.BAT.LOADLIB          00012001
//         DD  DISP=SHR,DSN=PDBFPP.OMNIPAY.TIAA.ONLINE.LOADLIB          00013001
//         DD  DISP=SHR,DSN=PDBFPP.OMNIPAY.CURRENT.ONLINE.LOADLIB
//         DD  DISP=SHR,DSN=PROD.BATCH.LOADLIB
//*                                                                     
//*------------------------------------------------------------------*  
//*  STEP TO OVERLAY ADDRESS FROM ADVICE RECS TO CHECK/EFT PYMNTS       
//*------------------------------------------------------------------*  
//CCXPREP EXEC TIACRPRP,                                                
//  VFSET='PMTP',                                                       
//  CURJN='POP9742D',                                                   
//  CCJOBN='POP9130D',                                                  
//  ADRSRP='OP9742D4',                                                  
//  AOVRLVS='OPADVSFP',                                                 
//  PRF='PRO'                                                           
//*                                                                     
//*-----------------------------------------------------------------*   
//*  STEP TO CREATE FILESET P CCP XML FEED - AFTERNOON RUN              
//*------------------------------------------------------------------*  
//CHKFEED EXEC TIACRCCP,                                                
//  GDG0='(+1)',                                                        
//  VFSET='PMTP',                                                       
//  FREQ='D',                                                           
//  PRF='PRO',                                                          
//  JNAM1='POP9742D',                                                   
//  CCJOBN='POP9742D',                                                  
//  TOTRPT='OP9742D1',                                                  
//  REJRPT='OP9742D2',                                                  
//  BYPRPT='OP9742D3',                                                  
//  PRCRPT='OP9742D5'                                                   
//*                                                                     
//CRCCPFD.PMT0XF   DD DUMMY                                             
//CRCCPFD.REFRSN   DD DUMMY                                             
//CRCCPFD.REFEXT   DD DUMMY                                             
//CRCCPFD.APXMLCHK DD DUMMY                                             
//CRCCPFD.APXMLLPR DD DUMMY                                             
//CRCCPFD.APXMLNCK DD DUMMY                                             
//*                                                                     
//*------------------------------------------------------------------*  
//*  STEP TO CREATE CONTROL FILE OF XML FEED - AFTNOON RUN - CHECK      
//*------------------------------------------------------------------*  
//CHKCTRL EXEC TIACRCTL,                                                
//  PJOBNM='POP9742D',                                                  
//  FT1='CHK',                                                          
//  VFSET='PMTP',                                                       
//  GDG0='(+1)'                                                         
//*                                                                     
//*------------------------------------------------------------------*  
//*  STEP TO CREATE CONTROL FILE OF XML FEED - AFTNOON RUN - NON-CHECK  
//*------------------------------------------------------------------*  
//NCKCTRL EXEC TIACRCTL,                                                
//  PJOBNM='POP9742D',                                                  
//  FT1='NCK',                                                          
//  VFSET='PMTP',                                                       
//  GDG0='(+1)'                                                         
//*                                                                     
