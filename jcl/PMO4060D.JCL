//PMO4060D JOB (P,15000,000,OPSB),
//             'PMO.MAYO NOTIFY',
//             CLASS=1,
//             MSGCLASS=I,
//             MSGLEVEL=(1,1)
//*
//LIBSRCH  JCLLIB ORDER=(PROD.OAS.NY.PARMLIB)
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//**
//**-----------------------------------------------------------------**
//** NOTIFY TO:                                                      **
//** MNOTI                                                           **
//**-----------------------------------------------------------------**
//SYMBOLS  SET C7EMLIB=PMVSSF.CA7.XSYSD1.CAIEADDR,
//             PARMLIB=PROD.PARMLIB
//*
//UTIL0010 EXEC PGM=CAL2EMLT
//SYSPRINT DD  SYSOUT=*
//EMAILLIB DD  DSN=&PARMLIB,
//             DISP=SHR
//EADDRLIB DD  DSN=&C7EMLIB,
//             DISP=SHR
//SYSIN    DD  *
ESERVER=SMTP.GLB.TIAA-CREF.ORG
EPORT=25
EFROM=TIAA-CREF_IT_JWM
EREPLY=CA-7@DO.NOT.REPLY
ETIMEOUT=10
TO=MNOTI
TXT=MO3010D1
