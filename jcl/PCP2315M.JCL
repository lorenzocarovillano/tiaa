//PCP2315M JOB (P,19999,000,FINS),'WAREHOUSE',CLASS=1,MSGCLASS=I
//****************************************************
//*            ** PCP2315M **                      ***
//*     UNPACK PIA.ANN.P2310CPM.DEDUCTNS           ***
//****************************************************
//*
//JOBLIB  DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//        DD DSN=PROD.BATCH.LOADLIB,DISP=SHR
//LIBSRCH  JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//* ----------------------------------------------------------------- *
//PCP2315M EXEC PCP2315M
//* ----------------------------------------------------------------- *
