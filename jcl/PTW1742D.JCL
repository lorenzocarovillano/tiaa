//PTW1742D JOB (P,14400,000,FINS),                                      
//            CPS-NPD,                                                  
//            MSGCLASS=I,                                               
//            CLASS=1,                                                  
//            REGION=8M                                                 
//*                                                                     
//*--------------------------------------------------------------------*
//*                                                                    *
//*     T I A A - C R E F   O P E N   P L A N   S O L U T I O N S      *
//*                                                                    *
//*                          *** PTW1742D ***                          *
//*                                                                    *
//*    TAXWARS MCCAMISH LIFE INSURANCE DAILY FEED (CURRENT YEAR)       *
//*                        CONTROL CARD SET-UP                         *
//*                                                                    *
//*--------------------------------------------------------------------*
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB                              
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//JOBLIB  DD DSN=PROD.BATCH.LOADLIB,DISP=SHR                            
//        DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR                 
//*---------------------------------------------------------------------
//PTW1742D EXEC PTW1740D,                                               
//         JOBNM1='PTW1742D',                                           
//         PGMPARM='TW1740D1',                                          
//         REPID1=',TW1742D1',                                          
//         REPID2=',TW1742D2'                                           
//                                                                      
