//PRD1265R JOB (P,13840,000,LEGS),'2016 CREF SHARE',
//         MSGCLASS=I,
//         CLASS=1,
//         REGION=9M
//*                                                                     
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*                                                                     
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//JOBLIB   DD  DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//*
//*****************************************************************
// SET HLQ8='PPT.COR'
// SET PARMLIB='PROD.PARMLIB'
//*****************************************************************     
//* JOB DESC: IDENTIFIED SAME RECORDS ARE PRESENT IN DEFFERENT 6        
//* PARTS FILE, CREATE A PART7 FILE FOR THOSE RECORDS AND PROCESS       
//* SEPARATELY.                                                         
//*****************************************************************     
//PRD1265R EXEC PRD1265R                                                
//*****************************************************************     
//* READ DA REPORTING EXTRACT & SPLIT BY NUMBER OF CONTRACTS
//*****************************************************************
//PRD1265C EXEC PRD1265C,                                               
//      PARMMEM=RD1265R1,
//      DATA='PART7',                                                   
//      FILE1=FILEG1,
//      FILE2=FILEG2,                                                   
//      REPORT1='RD126GR1',
//      REPORT2='RD126GR2'
//********************************************************************* 
//*          MERGE G1 AND OTHER MERGE PARTS FILE                      * 
//********************************************************************* 
//SORT050 EXEC PGM=SORT                                                 
//SYSOUT DD SYSOUT=*                                                    
//SORTIN DD DSN=&HLQ8..RIDER.SHARE.FILE.MERGE1,DISP=SHR                 
//       DD DSN=&HLQ8..RIDER.SHARE.FILEG1,DISP=SHR                      
//SORTOUT DD DSN=&HLQ8..RIDER.SHARE.FILE1,                              
//           DISP=(NEW,CATLG,DELETE),                                   
//           SPACE=(CYL,(100,100)),UNIT=SYSDA                           
//SYSIN DD DSN=&PARMLIB(ORD12654),DISP=SHR                              
