//PCPYRESJ JOB (T,67412,000,ITOB),                                      
//             'OA',                                                    
//             CLASS=U,                                                 
//             MSGCLASS=U                                               
//MJCLCPY  EXEC PGM=IEBCOPY                                             
//SYSPRINT DD  SYSOUT=*                                                 
//INDD     DD  DSN=PMIGROS.TEMP.DENA.LINKLIB,                           
//             DISP=SHR                                                 
//OUTDD    DD  DSN=SYS1.LINKLIB,UNIT=SYSDA,VOL=SER=MRESA1,              
//             DISP=SHR                                                 
//SYSUT3   DD  SPACE=(TRK,(5,2)),                                       
//             UNIT=SYSDA                                               
//SYSUT4   DD  SPACE=(TRK,(5,2)),                                       
//             UNIT=SYSDA                                               
//SYSIN    DD  *                                                        
 COPY INDD=((INDD,R)),OUTDD=OUTDD
//COPY020 EXEC PGM=IEBGENER
//SYSIN DD DUMMY
//SYSPRINT DD SYSOUT=*
//SYSUT1 DD DSN=PMIGROS.TEMP.DENA.SYNCH,DISP=SHR
//SYSUT2 DD DSN=SYS1.SYNCH,UNIT=SYSDA,VOL=SER=MRESA1,DISP=SHR
