//P1100TXM  JOB  (P,14400,000,FINS),IATX,MSGCLASS=I,CLASS=1
//*
//*
//*-------------------------------------------------------------------*
//* OCTOBER 18, 2001                                                  *
//* IA CUTOFF DATE - UPDATE PREVIOUS IA CUTOFF DATE AND POPULATE THE  *
//*                  NEW IA CUTOFF DATE.                              *
//*                  DBID =003 FILE = 112 (REFERENCE-TABLE)           *
//* JULY 02, 1993                                                     *
//* TAX ELECTION  - UPDATE THE CONTROL RECORD ON TAX-ELECTION FILE    *
//*                 TO REFLECT THE NEXT IA CHECK CYCLE DATE AFTER A   *
//*                 SUCCESSFUL COMPLETION OF THE MONTHLY IA UPDATE    *
//*                                                                   *
//*-------------------------------------------------------------------*
//P1100TXM  EXEC  P1100TXM
