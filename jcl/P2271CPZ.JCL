//P2271CPZ JOB (P,67412,000,FINB),DR,CLASS=1,MSGCLASS=I,
//        MSGLEVEL=(1,1)
//DELT005 EXEC PGM=IEFBR14                                              
//DD1     DD DSN=PPDD.SCD.TRIGGER.NY.P2271CPZ,                          
//          DISP=(MOD,DELETE),UNIT=SYSDA,SPACE=(TRK,0),                 
//          RECFM=LS,LRECL=80,DCB=MODLDSCB
//COPY010 EXEC PGM=IEBGENER,COND=(0,NE,DELT005)                         
//SYSPRINT DD SYSOUT=*                                                  
//SYSIN  DD DUMMY                                                       
//SYSUT1 DD DSN=PPDD.CASCD.JOB.TRIGGER,DISP=SHR                         
//SYSUT2 DD DSN=PPDD.SCD.TRIGGER.NY.P2271CPZ,                           
//          DISP=(,CATLG,DELETE),UNIT=SYSDA,SPACE=(TRK,(1,1)),          
//          RECFM=LS,LRECL=80,DCB=MODLDSCB
//*************************************************************
