//PTW1062M JOB (P,14400,000,FINS),IRA-CNTRRPT,MSGCLASS=I,CLASS=1,       
//             REGION=8M                                                
//*                                                                     
//*--------------------------------------------------------------------*
//*                                                                    *
//*     T I A A - C R E F   O P E N   P L A N   S O L U T I O N S      *
//*                                                                    *
//*                          *** PTW1062M ***                          *
//*                                                                    *
//*                   TAXWARS DATA WAREHOUSE EXTRACT                   *
//*                           CONTRIBUTIONS                            *
//*                 MONTHLY EXTRACT (CURRENT YEAR - 2)                 *
//*                                                                    *
//*--------------------------------------------------------------------*
//*                                                                     
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB                              
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//JOBLIB  DD DSN=PROD.BATCH.LOADLIB,DISP=SHR                            
//        DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR                 
//*                                                                     
//PTW1062M EXEC P1065TWD,                                               
//         JOBNM1='PTW1062M',                                           
//         TAXYEAR='TW1062M0'                                           
//*                                                                     
/*                                                                      
//                                                                      
