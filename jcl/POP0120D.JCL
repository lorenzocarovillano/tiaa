//POP0120D JOB (P,69999,000,OPSS),'CPS/OMNIPAY EXTRACT',CLASS=1,        
//       MSGCLASS=I                                     MOBIUS > 60DAYS 
//*                                                                     
//LIBSRCH  JCLLIB ORDER=(PROD.OAS.NY.PARMLIB)                           
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                PROD  
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*--------------------------------------------------------------------*
//*                                                                    *
//*            C P S   O M N I   L O A D                               *
//*                                                                    *
//*                 *** POP0120D ***                                   *
//*                                                                    *
//*            CPS - OMNIPAY LOAD PROCESS                              *
//*                                                                    *
//*--------------------------------------------------------------------*
//*                                                                     
//JOBLIB   DD DSN=PROD.BATCH.LOADLIB,DISP=SHR                           
//         DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR                
//*                                                                     
//POP0120D EXEC POP0120D            OMNIPAY LOAD PROCESS                
//*                                                                     
