//P1870TWA JOB (P,14400,000,FINS),'TAX MASS MAILING',MSGCLASS=I,        
//         REGION=8M,CLASS=1                                            
//*                                                                     
//*                                                                     
//JOBLIB     DD  DSN=PROD.BATCH.LOADLIB,DISP=SHR                        
//           DD  DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR             
//********************************************************************* 
//* NR4 - MOBIUS MASS MIGRATION - EMAIL                                 
//********************************************************************* 
//STEP010  EXEC P1550TWA,                                               
//         FORMTYPE='FNR4.E',                                           
//         FILENBR='1',                                                 
//         JOBNM='P1870TWA',                                            
//         PARMNM1='P1870TW2',                                          
//         ETID=',ETID=FNR4E'                                           
//*                                                                     
//*     REPORT NAME OVERRIDES                                           
//*                                                                     
//*                                                                     
//EXTR010.CMPRT01  DD  SYSOUT=(&REPT,TW1870A1)                          
//EXTR010.CMPRT02  DD  SYSOUT=(&REPT,TW1870A2)                          
//EXTR010.CMPRT03  DD  SYSOUT=(&REPT,TW1870A3)                          
//********************************************************************* 
//* NR4 - MOBIUS MASS MIGRATION - PAPER                                 
//********************************************************************* 
//STEP020  EXEC P1550TWA,                                               
//         FORMTYPE='FNR4',                                             
//         FILENBR='1',                                                 
//         JOBNM='P1870TWA',                                            
//         PARMNM1='P1870TW3',                                          
//         ETID=''                                                      
//*                                                                     
//*     REPORT NAME OVERRIDES                                           
//*                                                                     
//*                                                                     
//EXTR010.CMPRT01  DD  SYSOUT=(&REPT,TW1870B1)                          
//EXTR010.CMPRT02  DD  SYSOUT=(&REPT,TW1870B2)                          
//EXTR010.CMPRT03  DD  SYSOUT=(&REPT,TW1870B3)                          
