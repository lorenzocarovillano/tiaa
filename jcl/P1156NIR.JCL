//P1156NIR JOB (P,67412,000),                                           
//             WIL,                                                     
//             CLASS=1,                                                 
//             MSGCLASS=I,                                              
//             MSGLEVEL=(1,1)                                           
//**                                                                    
//OUTSAR OUTPUT JESDS=ALL,DEFAULT=YES,DEST=MVSJESD                      
//**********************************************************************
//STEP01   EXEC PGM=IEBGENER,                                           
//             COND=(0,LT)                                              
//SYSPRINT DD  SYSOUT=*                                                 
//SYSUT1   DD  DSN=PPDD.ASICS.DOCUMERG.P1155NIR.DN,                     
//             DISP=SHR                                                 
//SYSUT2   DD  SYSOUT=(F,,1155),                                        
//             DEST=U1700,                                              
//             BLKSIZE=3000,                                            
//             LRECL=155,                                               
//             RECFM=LSM                                                
//SYSIN    DD  DUMMY                                                    
//                                                                      
