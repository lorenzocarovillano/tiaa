//P1160IAX JOB (P,15000,000,LEGB),                                      
//         'NDM RUN-TASK',                                              
//         MSGCLASS=I,TIME=1440,                                        
//         CLASS=6                                                      
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB                              
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*-------#NDM----------------------------------------------------*     
//NDMS999 EXEC PGM=DMBATCH,REGION=4M,PARM=(YYSLYNN)                     
//DMMSGFIL  DD DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR                     
//DMNETMAP  DD DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR                  
//DMPUBLIB  DD DSN=PROD.NDM.NY.PROCESS.LIB,DISP=SHR                     
//DMPRINT   DD SYSOUT=*,OUTPUT=*.OUTVDR                                 
//SYSIN     DD *                                                        
  SIGNON ESF=YES                                                        
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(DATA1160) MAXDELAY=UNLIMITED       
  SIGNOFF                                                               
/*                                                                      
//                                                                      
