//PBF2040X JOB (C,62131,000,LEGB),'BEN',MSGCLASS=I,CLASS=6,
//          REGION=8M,TIME=1440
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//* --------------------------------------------------------------------
//* ENV   SiebelServer    Path
//* ----  --------------  --------------------------------------------
//* DEV   chadvdshn01     /dv/resrc_disk1/DS_PLN_DBOARD/source
//* ST2   chast2dshn01    /st2/resrc_disk1/DS_PLN_DBOARD/source
//* ST4   chast4dshn01    /st4/resrc_disk1/DS_PLN_DBOARD/source
//* PROD  CHAPDDSHN01     /pd1/resrc_disk1/DS_PLN_DBOARD/source
//* --------------------------------------------------------------------
//NDMS010  EXEC PGM=DMBATCH,REGION=1024K,PARM=(YYSLYNN),COND=(0,NE)
//DMNETMAP  DD  DISP=SHR,DSN=PMVSSF.STERLING.NDM.NETMAP
//DMPUBLIB  DD  DISP=SHR,DSN=PROD.NDM.NY.PROCESS.LIB
//DMMSGFIL  DD  DISP=SHR,DSN=PMVSSF.STERLING.NDM.MSG
//NDMCMDS   DD  SYSOUT=*
//DMPRINT   DD  SYSOUT=*
//SYSUDUMP  DD  SYSOUT=U
//SYSIN     DD  *
  SIGNON ESF=YES
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(BF2040X1)          -
  &INFILE=PNA.COR.PBF2040D.BENEPLAN.ANALYZER            -
  &OUTPATH=/pd1/resrc_disk1/DS_PLN_DBOARD/source        -
  &OUTFILE=/PBF2040D_BENE_Plan_Analyzer.txt             -
  CASE=YES MAXDELAY=UNLIMITED
  SIGNOFF
/*
