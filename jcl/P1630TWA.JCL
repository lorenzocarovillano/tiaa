//P1630TWA JOB (P,14400,000,FINS),5498IRSCOR,MSGCLASS=I,
//        CLASS=1,REGION=0M
//*                                                                     
//*                                                                     
//JOBLIB  DD DSN=PROD.BATCH.LOADLIB,DISP=SHR
//        DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//*
//P1630TWA EXEC P1630TWA
//**                                                                    
//**----------------------------------------------------------------**
//** SEND NOTIFICATION INDICATING TAX VENDOR FILE IS READY          **
//**                                                                **
//**----------------------------------------------------------------**
//NOTFY999 EXEC CAJUCMNY,COND=(0,NE)
//SYSPRINT DD  SYSOUT=*
//SYSIN    DD  *
MO ATTN: P1630TWA IS COMPLETE AND VENDOR CARTRIDGE IS READY.
/*
//*
