//PRD1220R JOB (P,66800,000,LEGS),'GROSS UP',
//         MSGCLASS=I,
//         CLASS=1,
//         REGION=9M
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//JOBLIB   DD  DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//*
//*****************************************************************
//* READ BI QUERY EXTRACT & SPLIT BY NUMBER OF CONTRACTS
//*****************************************************************
//PRD1220R EXEC PRD1220R,
//      PARMMEM=RD1220R1,
//      DATA='DATA1'
//*****************************************************************
//* REPORTS
//*****************************************************************
//EXTR020.CMPRT01 DD SYSOUT=(&REPT.,RD1220R1)
//EXTR020.CMPRT02 DD SYSOUT=(&REPT.,RD1220R2)
//EXTR020.CMPRT03 DD SYSOUT=(&REPT.,RD1220R3)
