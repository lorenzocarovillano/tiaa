//PRD1288R JOB (P,66955,000,LEGS),'RIDER GSRALOAN',
//         MSGCLASS=I,
//         CLASS=1
//*
//* RDR2013A - GRSA SAME SET UP AS DEFAULT BENEFICIARY
//*
//**********************************************************************
//* MODIFICATION LOG:
//*---------------------------------------------------------------------
//* DATE       NAME            CHG#      DESCRIPTION
//*---------------------------------------------------------------------
//* 10/10/2018                           COMBINE K AND L FILE INTO 1 AND
//*                                      USE PROC SAME AS DAFAULT BENE
//*                                      PCC2004R
//*
//**********************************************************************
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//JOBLIB   DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//        DD DSN=PROD.BATCH.LOADLIB,DISP=SHR
//*
//**********************************************************************
//*
// SET HLQ8='PPT.COR'
// SET PARMLIB='PROD.PARMLIB'
//*
//********************************************************************
//*        INITIALIZE DATASETS TO BE USED
//********************************************************************
//DELT001  EXEC PGM=IEFBR14
//SYSPRINT DD SYSOUT=*
//D1       DD DSN=&HLQ8..RIDER.GSRA.FILE1,
//            UNIT=SYSDA,
//            DISP=(MOD,DELETE,DELETE),
//            SPACE=(CYL,(0,0))
//*
//SORT001  EXEC PGM=SORT
//SORTIN   DD DUMMY,DCB=(LRECL=5000,RECFM=F)
//SORTOUT  DD DSN=&HLQ8..RDR2013A.ECSFILE.FB5000,DISP=OLD
//SYSOUT   DD SYSOUT=*
//SYSIN    DD DSN=&PARMLIB(SORTCOPY),DISP=SHR
//*
//SORT002  EXEC PGM=SORT
//SYSOUT   DD   SYSOUT=*
//SORTIN   DD   DSN=&HLQ8..RIDER.GSRA.FILEK1,DISP=SHR
//         DD   DSN=&HLQ8..RIDER.GSRA.FILEL1,DISP=SHR
//SORTOUT  DD   DSN=&HLQ8..RIDER.GSRA.FILE1,
//          DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//          UNIT=SYSDA,SPACE=(CYL,(20,5),RLSE)
//SYSIN    DD   DSN=&PARMLIB(RD1283R1),DISP=SHR
//*
//PRD1288R EXEC PCC2004R,
//       PARMMEM2=RD1283R2,
//       PARMMEM3=RD1283R3,
//       PARMMEM6=RD1283R4,
//       PACKAGE=RDR2013A,
//       DATATYPE=GSRA,
//       JOBNM=PRD1288R,
//       FILE=FILE1
//*
//EXTR010.CMPRT01 DD SYSOUT=(&REPT.,RD1283R1)
//EXTR010.CMPRT02 DD SYSOUT=&JCLO
//EXTR020.CMPRT01 DD SYSOUT=(&REPT.,RD1283R2)
//EXTR020.CMPRT02 DD SYSOUT=(&REPT.,RD1283R3)
//*
