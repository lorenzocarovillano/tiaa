//PRD1021X JOB (P,66800,000,OPSB),'RIDER REA',
//         CLASS=6,
//         MSGCLASS=I
//*
//* RDR2006N - CALIFORNIA REA ENDORSEMENTS
//*
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*
//* CONNECT DIRECT MAINFRAME FILE TO ECS SERVER
//**********************************************************************
//* EXECUTE PROC P1240PTX CONNECT:DIRECT
//**********************************************************************
//PRD1021X EXEC P1240PTX,COND=(0,NE),
//        JOBNM=PRD1021R
//*
