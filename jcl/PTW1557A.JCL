//PTW1557A JOB (P,14400,000,FINS),'TAX E-DELIVRY',MSGCLASS=I,           
//         REGION=8M,CLASS=1                                            
//*                                                                     
//JOBLIB     DD  DSN=PROD.BATCH.LOADLIB,DISP=SHR                        
//           DD  DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR             
//*                                                                     
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB                              
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*                                                                     
//********************************************************************* 
//* NO ACTION BY CA-11 REQUIRED                                         
//********************************************************************* 
//RMS      EXEC U11RMS,TYPRUN='N'                                       
//********************************************************************* 
//* 1099-R EMAIL FILE FOR MOBIUS MASS MIGRATION                         
//*                                                                     
//* THIS JOB CREATES 1099-R ECS FILES NUMBER 11 THROUGH 30              
//********************************************************************* 
//STEP010  EXEC P1550TWA,                                               
//         FORMTYPE='F1099R.E',                                         
//         FILENBR='11',                                                
//         JOBNM='PTW1557A',                                            
//         PARMNM1='P1550TW1',                                          
//         ETID=',ETID=F1099RE'                                         
//*                                                                     
//*                    <<< O V E R R I D E S >>>                        
//*                                                                     
//EXTR010.CMPRT01  DD  SYSOUT=(&REPT,TW1557AA)                          
//EXTR010.CMPRT02  DD  SYSOUT=(&REPT,TW1557AB)                          
//EXTR010.CMPRT03  DD  SYSOUT=(&REPT,TW1557AC)                          
//*                                                                     
//********************************************************************* 
//*                                                                     
//  IF RC < 3 THEN    * EXECUTE THIS STEP ONLY IF HIGHEST RC FROM *     
//*                   *  ANY PREVIOUS STEP WAS LESS THAN 3        *     
//*                                                                     
//STEP020  EXEC P1550TWA,COND=(4,LT),                                   
//         FORMTYPE='F1099R.E',                                         
//         FILENBR='12',                                                
//         JOBNM='PTW1557A',                                            
//         PARMNM1='P1550TW2',                                          
//         ETID=',ETID=F1099RE'                                         
//*                                                                     
//*                    <<< O V E R R I D E S >>>                        
//*                                                                     
//EXTR010.CMPRT01  DD  SYSOUT=(&REPT,TW1557AD)                          
//EXTR010.CMPRT02  DD  SYSOUT=(&REPT,TW1557AE)                          
//EXTR010.CMPRT03  DD  SYSOUT=(&REPT,TW1557AF)                          
//*                                                                     
// ENDIF          * TERMINATE IF STMT HERE *                            
//*                                                                     
//********************************************************************* 
//*                                                                     
//  IF RC < 3 THEN    * EXECUTE THIS STEP ONLY IF HIGHEST RC FROM *     
//*                   *  ANY PREVIOUS STEP WAS LESS THAN 3        *     
//*                                                                     
//STEP030  EXEC P1550TWA,COND=(4,LT),                                   
//         FORMTYPE='F1099R.E',                                         
//         FILENBR='13',                                                
//         JOBNM='PTW1557A',                                            
//         PARMNM1='P1550TW2',                                          
//         ETID=',ETID=F1099RE'                                         
//*                                                                     
//*                    <<< O V E R R I D E S >>>                        
//*                                                                     
//EXTR010.CMPRT01  DD  SYSOUT=(&REPT,TW1557AG)                          
//EXTR010.CMPRT02  DD  SYSOUT=(&REPT,TW1557AH)                          
//EXTR010.CMPRT03  DD  SYSOUT=(&REPT,TW1557AI)                          
//*                                                                     
// ENDIF          * TERMINATE IF STMT HERE *                            
//*                                                                     
//********************************************************************* 
//*                                                                     
//  IF RC < 3 THEN    * EXECUTE THIS STEP ONLY IF HIGHEST RC FROM *     
//*                   *  ANY PREVIOUS STEP WAS LESS THAN 3        *     
//*                                                                     
//STEP040  EXEC P1550TWA,COND=(4,LT),                                   
//         FORMTYPE='F1099R.E',                                         
//         FILENBR='14',                                                
//         JOBNM='PTW1557A',                                            
//         PARMNM1='P1550TW2',                                          
//         ETID=',ETID=F1099RE'                                         
//*                                                                     
//*                    <<< O V E R R I D E S >>>                        
//*                                                                     
//EXTR010.CMPRT01  DD  SYSOUT=(&REPT,TW1557AJ)                          
//EXTR010.CMPRT02  DD  SYSOUT=(&REPT,TW1557AK)                          
//EXTR010.CMPRT03  DD  SYSOUT=(&REPT,TW1557AL)                          
//*                                                                     
// ENDIF          * TERMINATE IF STMT HERE *                            
//*                                                                     
//********************************************************************* 
//*                                                                     
//  IF RC < 3 THEN    * EXECUTE THIS STEP ONLY IF HIGHEST RC FROM *     
//*                   *  ANY PREVIOUS STEP WAS LESS THAN 3        *     
//*                                                                     
//STEP050  EXEC P1550TWA,COND=(4,LT),                                   
//         FORMTYPE='F1099R.E',                                         
//         FILENBR='15',                                                
//         JOBNM='PTW1557A',                                            
//         PARMNM1='P1550TW2',                                          
//         ETID=',ETID=F1099RE'                                         
//*                                                                     
//*                    <<< O V E R R I D E S >>>                        
//*                                                                     
//EXTR010.CMPRT01  DD  SYSOUT=(&REPT,TW1557AM)                          
//EXTR010.CMPRT02  DD  SYSOUT=(&REPT,TW1557AN)                          
//EXTR010.CMPRT03  DD  SYSOUT=(&REPT,TW1557AO)                          
//*                                                                     
// ENDIF          * TERMINATE IF STMT HERE *                            
//*                                                                     
//********************************************************************* 
//*                                                                     
//  IF RC < 3 THEN    * EXECUTE THIS STEP ONLY IF HIGHEST RC FROM *     
//*                   *  ANY PREVIOUS STEP WAS LESS THAN 3        *     
//*                                                                     
//STEP060  EXEC P1550TWA,COND=(4,LT),                                   
//         FORMTYPE='F1099R.E',                                         
//         FILENBR='16',                                                
//         JOBNM='PTW1557A',                                            
//         PARMNM1='P1550TW2',                                          
//         ETID=',ETID=F1099RE'                                         
//*                                                                     
//*                    <<< O V E R R I D E S >>>                        
//*                                                                     
//EXTR010.CMPRT01  DD  SYSOUT=(&REPT,TW1557AP)                          
//EXTR010.CMPRT02  DD  SYSOUT=(&REPT,TW1557AQ)                          
//EXTR010.CMPRT03  DD  SYSOUT=(&REPT,TW1557AR)                          
//*                                                                     
// ENDIF          * TERMINATE IF STMT HERE *                            
//*                                                                     
//********************************************************************* 
//*                                                                     
//  IF RC < 3 THEN    * EXECUTE THIS STEP ONLY IF HIGHEST RC FROM *     
//*                   *  ANY PREVIOUS STEP WAS LESS THAN 3        *     
//*                                                                     
//STEP070  EXEC P1550TWA,COND=(4,LT),                                   
//         FORMTYPE='F1099R.E',                                         
//         FILENBR='17',                                                
//         JOBNM='PTW1557A',                                            
//         PARMNM1='P1550TW2',                                          
//         ETID=',ETID=F1099RE'                                         
//*                                                                     
//*                    <<< O V E R R I D E S >>>                        
//*                                                                     
//EXTR010.CMPRT01  DD  SYSOUT=(&REPT,TW1557AS)                          
//EXTR010.CMPRT02  DD  SYSOUT=(&REPT,TW1557AT)                          
//EXTR010.CMPRT03  DD  SYSOUT=(&REPT,TW1557AU)                          
//*                                                                     
// ENDIF          * TERMINATE IF STMT HERE *                            
//*                                                                     
//********************************************************************* 
//*                                                                     
//  IF RC < 3 THEN    * EXECUTE THIS STEP ONLY IF HIGHEST RC FROM *     
//*                   *  ANY PREVIOUS STEP WAS LESS THAN 3        *     
//*                                                                     
//STEP080  EXEC P1550TWA,COND=(4,LT),                                   
//         FORMTYPE='F1099R.E',                                         
//         FILENBR='18',                                                
//         JOBNM='PTW1557A',                                            
//         PARMNM1='P1550TW2',                                          
//         ETID=',ETID=F1099RE'                                         
//*                                                                     
//*                    <<< O V E R R I D E S >>>                        
//*                                                                     
//EXTR010.CMPRT01  DD  SYSOUT=(&REPT,TW1557AV)                          
//EXTR010.CMPRT02  DD  SYSOUT=(&REPT,TW1557AW)                          
//EXTR010.CMPRT03  DD  SYSOUT=(&REPT,TW1557AX)                          
//*                                                                     
// ENDIF          * TERMINATE IF STMT HERE *                            
//*                                                                     
//********************************************************************* 
//*                                                                     
//  IF RC < 3 THEN    * EXECUTE THIS STEP ONLY IF HIGHEST RC FROM *     
//*                   *  ANY PREVIOUS STEP WAS LESS THAN 3        *     
//*                                                                     
//STEP090  EXEC P1550TWA,COND=(4,LT),                                   
//         FORMTYPE='F1099R.E',                                         
//         FILENBR='19',                                                
//         JOBNM='PTW1557A',                                            
//         PARMNM1='P1550TW2',                                          
//         ETID=',ETID=F1099RE'                                         
//*                                                                     
//*                    <<< O V E R R I D E S >>>                        
//*                                                                     
//EXTR010.CMPRT01  DD  SYSOUT=(&REPT,TW1557AY)                          
//EXTR010.CMPRT02  DD  SYSOUT=(&REPT,TW1557AZ)                          
//EXTR010.CMPRT03  DD  SYSOUT=(&REPT,TW1557A0)                          
//*                                                                     
// ENDIF          * TERMINATE IF STMT HERE *                            
//*                                                                     
//********************************************************************* 
//*                                                                     
//  IF RC < 3 THEN    * EXECUTE THIS STEP ONLY IF HIGHEST RC FROM *     
//*                   *  ANY PREVIOUS STEP WAS LESS THAN 3        *     
//*                                                                     
//STEP100  EXEC P1550TWA,COND=(4,LT),                                   
//         FORMTYPE='F1099R.E',                                         
//         FILENBR='20',                                                
//         JOBNM='PTW1557A',                                            
//         PARMNM1='P1550TW2',                                          
//         ETID=',ETID=F1099RE'                                         
//*                                                                     
//*                    <<< O V E R R I D E S >>>                        
//*                                                                     
//EXTR010.CMPRT01  DD  SYSOUT=(&REPT,TW1557A1)                          
//EXTR010.CMPRT02  DD  SYSOUT=(&REPT,TW1557A2)                          
//EXTR010.CMPRT03  DD  SYSOUT=(&REPT,TW1557A3)                          
//*                                                                     
// ENDIF          * TERMINATE IF STMT HERE *                            
//*                                                                     
//********************************************************************* 
//*                                                                     
//  IF RC < 3 THEN    * EXECUTE THIS STEP ONLY IF HIGHEST RC FROM *     
//*                   *  ANY PREVIOUS STEP WAS LESS THAN 3        *     
//*                                                                     
//STEP110  EXEC P1550TWA,                                               
//         FORMTYPE='F1099R.E',                                         
//         FILENBR='21',                                                
//         JOBNM='PTW1557A',                                            
//         PARMNM1='P1550TW1',                                          
//         ETID=',ETID=F1099RE'                                         
//*                                                                     
//*                    <<< O V E R R I D E S >>>                        
//*                                                                     
//EXTR010.CMPRT01  DD  SYSOUT=(&REPT,TW1557A4)                          
//EXTR010.CMPRT02  DD  SYSOUT=(&REPT,TW1557A5)                          
//EXTR010.CMPRT03  DD  SYSOUT=(&REPT,TW1557A6)                          
//*                                                                     
//*                                                                     
// ENDIF          * TERMINATE IF STMT HERE *                            
//*                                                                     
//********************************************************************* 
//*                                                                     
//  IF RC < 3 THEN    * EXECUTE THIS STEP ONLY IF HIGHEST RC FROM *     
//*                   *  ANY PREVIOUS STEP WAS LESS THAN 3        *     
//*                                                                     
//STEP120  EXEC P1550TWA,COND=(4,LT),                                   
//         FORMTYPE='F1099R.E',                                         
//         FILENBR='22',                                                
//         JOBNM='PTW1557A',                                            
//         PARMNM1='P1550TW2',                                          
//         ETID=',ETID=F1099RE'                                         
//*                                                                     
//*                    <<< O V E R R I D E S >>>                        
//*                                                                     
//EXTR010.CMPRT01  DD  SYSOUT=(&REPT,TW1557A7)                          
//EXTR010.CMPRT02  DD  SYSOUT=(&REPT,TW1557A8)                          
//EXTR010.CMPRT03  DD  SYSOUT=(&REPT,TW1557A9)                          
//*                                                                     
// ENDIF          * TERMINATE IF STMT HERE *                            
//*                                                                     
//********************************************************************* 
//*                                                                     
//  IF RC < 3 THEN    * EXECUTE THIS STEP ONLY IF HIGHEST RC FROM *     
//*                   *  ANY PREVIOUS STEP WAS LESS THAN 3        *     
//*                                                                     
//STEP130  EXEC P1550TWA,COND=(4,LT),                                   
//         FORMTYPE='F1099R.E',                                         
//         FILENBR='23',                                                
//         JOBNM='PTW1557A',                                            
//         PARMNM1='P1550TW2',                                          
//         ETID=',ETID=F1099RE'                                         
//*                                                                     
//*                    <<< O V E R R I D E S >>>                        
//*                                                                     
//EXTR010.CMPRT01  DD  SYSOUT=(&REPT,TW155AA1)                          
//EXTR010.CMPRT02  DD  SYSOUT=(&REPT,TW155AA2)                          
//EXTR010.CMPRT03  DD  SYSOUT=(&REPT,TW155AA3)                          
//*                                                                     
// ENDIF          * TERMINATE IF STMT HERE *                            
//*                                                                     
//********************************************************************* 
//*                                                                     
//  IF RC < 3 THEN    * EXECUTE THIS STEP ONLY IF HIGHEST RC FROM *     
//*                   *  ANY PREVIOUS STEP WAS LESS THAN 3        *     
//*                                                                     
//STEP140  EXEC P1550TWA,COND=(4,LT),                                   
//         FORMTYPE='F1099R.E',                                         
//         FILENBR='24',                                                
//         JOBNM='PTW1557A',                                            
//         PARMNM1='P1550TW2',                                          
//         ETID=',ETID=F1099RE'                                         
//*                                                                     
//*                    <<< O V E R R I D E S >>>                        
//*                                                                     
//EXTR010.CMPRT01  DD  SYSOUT=(&REPT,TW155AA4)                          
//EXTR010.CMPRT02  DD  SYSOUT=(&REPT,TW155AA5)                          
//EXTR010.CMPRT03  DD  SYSOUT=(&REPT,TW155AA6)                          
//*                                                                     
// ENDIF          * TERMINATE IF STMT HERE *                            
//*                                                                     
//********************************************************************* 
//*                                                                     
//  IF RC < 3 THEN    * EXECUTE THIS STEP ONLY IF HIGHEST RC FROM *     
//*                   *  ANY PREVIOUS STEP WAS LESS THAN 3        *     
//*                                                                     
//STEP150  EXEC P1550TWA,COND=(4,LT),                                   
//         FORMTYPE='F1099R.E',                                         
//         FILENBR='25',                                                
//         JOBNM='PTW1557A',                                            
//         PARMNM1='P1550TW2',                                          
//         ETID=',ETID=F1099RE'                                         
//*                                                                     
//*                    <<< O V E R R I D E S >>>                        
//*                                                                     
//EXTR010.CMPRT01  DD  SYSOUT=(&REPT,TW155AA7)                          
//EXTR010.CMPRT02  DD  SYSOUT=(&REPT,TW155AA8)                          
//EXTR010.CMPRT03  DD  SYSOUT=(&REPT,TW155AA9)                          
//*                                                                     
// ENDIF          * TERMINATE IF STMT HERE *                            
//*                                                                     
//********************************************************************* 
//*                                                                     
//  IF RC < 3 THEN    * EXECUTE THIS STEP ONLY IF HIGHEST RC FROM *     
//*                   *  ANY PREVIOUS STEP WAS LESS THAN 3        *     
//*                                                                     
//STEP160  EXEC P1550TWA,COND=(4,LT),                                   
//         FORMTYPE='F1099R.E',                                         
//         FILENBR='26',                                                
//         JOBNM='PTW1557A',                                            
//         PARMNM1='P1550TW2',                                          
//         ETID=',ETID=F1099RE'                                         
//*                                                                     
//*                    <<< O V E R R I D E S >>>                        
//*                                                                     
//EXTR010.CMPRT01  DD  SYSOUT=(&REPT,TW155BA1)                          
//EXTR010.CMPRT02  DD  SYSOUT=(&REPT,TW155BA2)                          
//EXTR010.CMPRT03  DD  SYSOUT=(&REPT,TW155BA3)                          
//*                                                                     
// ENDIF          * TERMINATE IF STMT HERE *                            
//*                                                                     
//********************************************************************* 
//*                                                                     
//  IF RC < 3 THEN    * EXECUTE THIS STEP ONLY IF HIGHEST RC FROM *     
//*                   *  ANY PREVIOUS STEP WAS LESS THAN 3        *     
//*                                                                     
//STEP170  EXEC P1550TWA,COND=(4,LT),                                   
//         FORMTYPE='F1099R.E',                                         
//         FILENBR='27',                                                
//         JOBNM='PTW1557A',                                            
//         PARMNM1='P1550TW2',                                          
//         ETID=',ETID=F1099RE'                                         
//*                                                                     
//*                    <<< O V E R R I D E S >>>                        
//*                                                                     
//EXTR010.CMPRT01  DD  SYSOUT=(&REPT,TW155BA4)                          
//EXTR010.CMPRT02  DD  SYSOUT=(&REPT,TW155BA5)                          
//EXTR010.CMPRT03  DD  SYSOUT=(&REPT,TW155BA6)                          
//*                                                                     
// ENDIF          * TERMINATE IF STMT HERE *                            
//*                                                                     
//********************************************************************* 
//*                                                                     
//  IF RC < 3 THEN    * EXECUTE THIS STEP ONLY IF HIGHEST RC FROM *     
//*                   *  ANY PREVIOUS STEP WAS LESS THAN 3        *     
//*                                                                     
//STEP180  EXEC P1550TWA,COND=(4,LT),                                   
//         FORMTYPE='F1099R.E',                                         
//         FILENBR='28',                                                
//         JOBNM='PTW1557A',                                            
//         PARMNM1='P1550TW2',                                          
//         ETID=',ETID=F1099RE'                                         
//*                                                                     
//*                    <<< O V E R R I D E S >>>                        
//*                                                                     
//EXTR010.CMPRT01  DD  SYSOUT=(&REPT,TW155BA7)                          
//EXTR010.CMPRT02  DD  SYSOUT=(&REPT,TW155BA8)                          
//EXTR010.CMPRT03  DD  SYSOUT=(&REPT,TW155BA9)                          
//*                                                                     
// ENDIF          * TERMINATE IF STMT HERE *                            
//*                                                                     
//********************************************************************* 
//*                                                                     
//  IF RC < 3 THEN    * EXECUTE THIS STEP ONLY IF HIGHEST RC FROM *     
//*                   *  ANY PREVIOUS STEP WAS LESS THAN 3        *     
//*                                                                     
//STEP190  EXEC P1550TWA,COND=(4,LT),                                   
//         FORMTYPE='F1099R.E',                                         
//         FILENBR='29',                                                
//         JOBNM='PTW1557A',                                            
//         PARMNM1='P1550TW2',                                          
//         ETID=',ETID=F1099RE'                                         
//*                                                                     
//*                    <<< O V E R R I D E S >>>                        
//*                                                                     
//EXTR010.CMPRT01  DD  SYSOUT=(&REPT,TW155CA1)                          
//EXTR010.CMPRT02  DD  SYSOUT=(&REPT,TW155CA2)                          
//EXTR010.CMPRT03  DD  SYSOUT=(&REPT,TW155CA3)                          
//*                                                                     
// ENDIF          * TERMINATE IF STMT HERE *                            
//*                                                                     
//********************************************************************* 
//*                                                                     
//  IF RC < 3 THEN    * EXECUTE THIS STEP ONLY IF HIGHEST RC FROM *     
//*                   *  ANY PREVIOUS STEP WAS LESS THAN 3        *     
//*                                                                     
//STEP200  EXEC P1550TWA,COND=(4,LT),                                   
//         FORMTYPE='F1099R.E',                                         
//         FILENBR='30',                                                
//         JOBNM='PTW1557A',                                            
//         PARMNM1='P1550TW2',                                          
//         ETID=',ETID=F1099RE'                                         
//*                                                                     
//*                    <<< O V E R R I D E S >>>                        
//*                                                                     
//EXTR010.CMPRT01  DD  SYSOUT=(&REPT,TW155CA4)                          
//EXTR010.CMPRT02  DD  SYSOUT=(&REPT,TW155CA5)                          
//EXTR010.CMPRT03  DD  SYSOUT=(&REPT,TW155CA6)                          
//*                                                                     
// ENDIF          * TERMINATE IF STMT HERE *                            
//*                                                                     
//********************************************************************* 
