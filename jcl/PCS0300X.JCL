//PCS0300X JOB (P,14000,000,LEGB),ADR,MSGCLASS=I,CLASS=6,               
//   TIME=1440                                                          
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB                              
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*-----#NDM-- OIA FUND FILE -------------------------------------*     
//NDMS999 EXEC PGM=DMBATCH,REGION=4M,PARM=(YYSLYNN)                     
//DMMSGFIL  DD DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR                     
//DMNETMAP  DD DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR                  
//DMPUBLIB  DD DSN=PROD.NDM.NY.PROCESS.LIB,DISP=SHR                     
//DMPRINT   DD SYSOUT=*,OUTPUT=*.OUTVDR                                 
//SYSIN     DD *                                                        
  SIGNON ESF=YES                                                        
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(DADACS01) MAXDELAY=UNLIMITED       
  SIGNOFF                                                               
/*                                                                      
//*-----#NDM-- OIA RATES FILE ------------------------------------*     
//NDMST02 EXEC PGM=DMBATCH,REGION=4M,PARM=(YYSLYNN)                     
//DMMSGFIL  DD DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR                     
//DMNETMAP  DD DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR                  
//DMPUBLIB  DD DSN=PROD.NDM.NY.PROCESS.LIB,DISP=SHR                     
//DMPRINT   DD SYSOUT=*,OUTPUT=*.OUTVDR                                 
//SYSIN     DD *                                                        
  SIGNON ESF=YES                                                        
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(DADACS02) MAXDELAY=UNLIMITED       
  SIGNOFF                                                               
/*                                                                      
//*-----#NDM-- OIA FUND3 FILE -------------------------------------*    
//NDMST03 EXEC PGM=DMBATCH,REGION=4M,PARM=(YYSLYNN)                     
//DMMSGFIL  DD DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR                     
//DMNETMAP  DD DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR                  
//DMPUBLIB  DD DSN=PROD.NDM.NY.PROCESS.LIB,DISP=SHR                     
//DMPRINT   DD SYSOUT=*,OUTPUT=*.OUTVDR                                 
//SYSIN     DD *                                                        
  SIGNON ESF=YES                                                        
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(DADACS03) MAXDELAY=UNLIMITED       
  SIGNOFF                                                               
/*                                                                      
//                                                                      
