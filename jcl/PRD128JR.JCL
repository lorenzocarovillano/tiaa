//PRD128JR JOB (P,13840,000,LEGS),'GSRA LOAN',
//         MSGCLASS=I,
//         CLASS=1,
//         REGION=9M
//***     =======================================================
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//***     =======================================================
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//* ==================================================================
//JOBLIB    DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//          DD DSN=PROD.BATCH.LOADLIB,DISP=SHR
//**********************************************************************
//*** JOB DESC: READ 10TH SPLIT FILE AND PROCESS                      **
//**********************************************************************
//PRD1282R EXEC PROC=PRD1282R,
//      PARMMEM=RD1282R1,
//      DATA='PART10',
//      FILE1='FILEJ1',
//      REPORT1='RD128JR1'
//*
