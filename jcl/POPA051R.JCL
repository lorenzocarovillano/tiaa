//POPA051R JOB (P,19999,000,OPSB),'OMNIPAY',                            
//         CLASS=1,MSGCLASS=I                                           
//*                                                                     
//*                                                                     
//LIBSRCH  JCLLIB ORDER=(PROD.OAS.NY.PARMLIB)                           
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*                                                                     
//*   COPY FIRST FILE TO TEMP FILE                                      
//POPA051R EXEC TIAARCN1,                                               
//         VFSET='PMTA',                 <FILESET ID>                   
//         COMP='001'                                                   
