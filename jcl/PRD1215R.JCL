//PRD1215R JOB (P,66800,000,LEGB),'RIDER MEOW',
//         CLASS=1,
//         MSGCLASS=I
//*
//* RDR2011C - MEOW - E-MAIL
//*
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*
//*********************************************************************
//* LIST ALL LOAD LIBRARIES NEEDED BY THE PROC.
//*********************************************************************
//*
//JOBLIB   DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//*
//**********************************************************************
//* EXECUTE PROC P1245PTD
//**********************************************************************
//PRD1215R EXEC P1245PTD,COND=(4,LE),
//        JOBNM1=PRD1214R,
//        JOBNM2=PRD1215R
//*
