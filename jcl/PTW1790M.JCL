//PTW1790M JOB (P,14400,000,FINS),INS-DNLD,MSGCLASS=I,CLASS=1,
//           REGION=8M
//*
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*
//*--------------------------------------------------------------------*
//*                                                                    *
//*     T I A A - C R E F   T A X W A R S   A P P L I C A T I O N      *
//*                                                                    *
//*                          *** PTW1790M ***                          *
//*                                                                    *
//*                     TAXWARS INSURANCE DOWNLOAD                     *
//*                    DIVIDEND AND INTEREST EXTRACT                   *
//*                                                                    *
//*--------------------------------------------------------------------*
//*
//JOBLIB  DD DSN=PROD.BATCH.LOADLIB,DISP=SHR
//        DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//*
//PTW1790M EXEC PTW1790M
//
