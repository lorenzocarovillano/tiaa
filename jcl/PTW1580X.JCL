//PTW1580X JOB (P,14400,000,FINB),CAIMS,MSGCLASS=I,CLASS=6,             
//      REGION=8M                                                       
//* FORM 1042S                                                          
//*                                                                     
//NDMS020 EXEC PGM=DMBATCH,REGION=4M,PARM=(YYSLYNN),COND=(0,NE)         
//DMNETMAP DD DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR                   
//DMPUBLIB DD DSN=PMVSPP.STERLING.PROCESS.LIB,DISP=SHR                  
//DMMSGFIL DD DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR                      
//DMPRINT   DD SYSOUT=*                                                 
//NDMCMDS   DD SYSOUT=*                                                 
//SYSUDUMP  DD SYSOUT=U                                                 
//SYSIN     DD *                                                        
  SIGNON ESF=YES                                                        
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(DLNB1580) MAXDELAY=UNLIMITED -     
  CASE=YES                                                              
  SIGNOFF                                                               
/*                                                                      
//                                                                      
