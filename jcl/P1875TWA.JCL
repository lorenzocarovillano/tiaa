//P1875TWA JOB (P,14400,000,FINS),PR-MAG-RPT,MSGCLASS=I,                
//        CLASS=1,REGION=8M                                             
//*                                                                     
//*                                                                     
//JOBLIB  DD DSN=PROD.BATCH.LOADLIB,DISP=SHR
//        DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//*
//P1875TWA EXEC P1875TWA                                                
//*
//* PUERTO RICO 480.6 MAG MEDIA REPORTING.                              
//*
//* PROCEDURE TO CREATE A NO LABEL CART.
//* 1.  MUST CALL TAPE LIBRARY FOR NO LABEL CART VOL SER NUMBER.
//* 2.  O/A #5298 MUST STAGE JOB P1875TWA, PLACE VOL SER NUMBER INTO
//*        JOB CARD AND RUN JOB.
//**                                                                    
//**----------------------------------------------------------------**
//** SEND NOTIFICATION INDICATING TAX VENDOR FILE IS READY          **
//**                                                                **
//**----------------------------------------------------------------**
//NOTFY999 EXEC CAJUCMNY,COND=(0,NE)
//SYSPRINT DD  SYSOUT=*
//SYSIN    DD  *
MO ATTN: P1875TWA IS COMPLETE AND VENDOR CARTRIDGE IS READY.
/*
//*
