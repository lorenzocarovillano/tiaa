//PNI2050R JOB (P,66200,000,OPSB),                                      
//             'PRAP-SERVER',                                           
//             MSGCLASS=I,                                              
//             CLASS=6                                                  
//*                                                                     
//LIBSRCH  JCLLIB ORDER=PROD.OAS.NY.PARMLIB                             
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//**                                                                    
//**-----------------------------------------------------------------** 
//**  TRANSMIT INFOMATICA DATA TO DENVER FOR PICKUP                  ** 
//**-----------------------------------------------------------------** 
//NDMS020  EXEC PGM=DMBATCH,                                            
//             PARM=(YYSLYNN),                                          
//             TIME=1440                                                
//DMNETMAP DD  DSN=PMVSSF.STERLING.NDM.NETMAP,                          
//             DISP=SHR                                                 
//DMPUBLIB DD  DSN=PMVSPP.STERLING.PROCESS.LIB,                         
//             DISP=SHR                                                 
//DMMSGFIL DD  DSN=PMVSSF.STERLING.NDM.MSG,                             
//             DISP=SHR                                                 
//DMPRINT  DD  SYSOUT=*,                                                
//             OUTPUT=*.OUTVDR                                          
//NDMCMDS  DD  SYSOUT=*,                                                
//             OUTPUT=*.OUTVDR                                          
//SYSUDUMP DD  SYSOUT=U,                                                
//             OUTPUT=*.OUTLOCL                                         
//SYSIN    DD  *                                                        
  SIGNON ESF=YES                                                        
  SUBMIT DSN=PROD.NDM.DN.PROCESS.LIB(DADR2050) MAXDELAY=UNLIMITED       
  SIGNOFF                                                               
//                                                                      
