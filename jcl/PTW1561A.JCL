//PTW1561A JOB (P,14400,000,FINS),'TAX MMM SUNY',MSGCLASS=I,
//         REGION=8M,CLASS=1
//*
//*
//JOBLIB     DD  DSN=PROD.BATCH.LOADLIB,DISP=SHR
//           DD  DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//*********************************************************************
//* NO ACTION BY CA-11 REQUIRED
//*********************************************************************
//RMS      EXEC U11RMS,TYPRUN='N'
//*********************************************************************
//*       MOBIUS MASS MIGRATION AND DST CONVERSION OF TAX FORMS         
//*     THIS JOB CREATES 1099R WITH SUNY PAPER FORM FILES 1 AND 2       
//*********************************************************************
//STEP010  EXEC P1550TWA,
//         FORMTYPE='F1099RS',
//         FILENBR='1',
//         JOBNM='PTW1561A',
//         PARMNM1='P1561TW1',
//         ETID=''
//*                    <<< O V E R R I D E S >>>
//*
//EXTR010.CMPRT01  DD  SYSOUT=(&REPT,TW1561A1)
//EXTR010.CMPRT02  DD  SYSOUT=(&REPT,TW1561A2)
//EXTR010.CMPRT03  DD  SYSOUT=(&REPT,TW1561A3)
//*
//*********************************************************************
//*
//  IF RC < 3 THEN    * EXECUTE THIS STEP ONLY IF HIGHEST RC FROM *
//*                   *  ANY PREVIOUS STEP WAS LESS THAN 3        *
//*
//STEP020  EXEC P1550TWA,COND=(4,LT),                                   
//         FORMTYPE='F1099RS',
//         FILENBR='2',                                                 
//*        RFILENBR='2',                                                
//*        REFBCK=',,REF=*.STEP010.ECSTAPE.SYSUT2',                     
//         JOBNM='PTW1561A',                                            
//         PARMNM1='P1561TW1',                                          
//         ETID=''
//*                    <<< O V E R R I D E S >>>
//*
//EXTR010.CMPRT01  DD  SYSOUT=(&REPT,TW1561A4)
//EXTR010.CMPRT02  DD  SYSOUT=(&REPT,TW1561A5)
//EXTR010.CMPRT03  DD  SYSOUT=(&REPT,TW1561A6)
//*
//*
// ENDIF          * TERMINATE IF STMT HERE *
//*
