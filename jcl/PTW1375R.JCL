//PTW1375R JOB (P,14400,000,FINS),PAYMT-VSFRM,MSGCLASS=I,CLASS=2,       
//             REGION=8M                                                
//*                                                                     
//*--------------------------------------------------------------------*
//*                                                                    *
//*                          *** PTW1375R ***                          *
//*                                                                    *
//*                   TAXWARS W-2 CORRECTIONS REPORT                   *
//*                                                                    *
//*                ANNUAL ON-REQUEST (CURRENT YEAR - 1)                *
//*                                                                    *
//*--------------------------------------------------------------------*
//*                                                                     
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB                              
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//JOBLIB  DD DSN=PROD.BATCH.LOADLIB,DISP=SHR                            
//        DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR                 
//*                                                                     
//PTW1375R EXEC PTW1375R,                                               
//         JOBNM1='PTW1375R',                                           
//         JOBNM2='PTW1071D',                                           
//         REPTID='TW1375RA'                                            
//                                                                      
