//PCC3401M JOB (P,66800,000,OPSS),'NET CHANGE FILES-CCP',               
//         CLASS=1,TIME=1440,
//         MSGCLASS=I
//*
//LIBSRCH  JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*
//JOBLIB   DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//         DD DSN=PROD.BATCH.LOADLIB,DISP=SHR
//**                                                                    
//*********************************************************************
//** CREATES THE XML AND MQFTE CARDS FOR ECS PACKAGES                   
//*********************************************************************
//PNTLABLD EXEC PCC3000M,
//       APPNM='PNTLABLD',
//       SPC3='50,50',
//       SEQNO='S000001',
//       PARMSEQ='SEQ00001'
//**                                                                    
