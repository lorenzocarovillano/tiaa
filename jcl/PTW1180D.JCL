//PTW1180D JOB (P,14400,000,FINS),CPS-NPD,MSGCLASS=I,CLASS=1,           
//             REGION=8M                                                
//*                                                                     
//*--------------------------------------------------------------------*
//*                                                                    *
//*     T I A A - C R E F   O P E N   P L A N   S O L U T I O N S      *
//*                                                                    *
//*                          *** PTW1180D ***                          *
//*                                                                    *
//*                    TAXWARS NAVISYS DAILY FEED (CURRENT YEAR)       *
//*                               UPDATE                               *
//*                                                                    *
//*--------------------------------------------------------------------*
//*                                                                     
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB                              
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//JOBLIB  DD DSN=PROD.BATCH.LOADLIB,DISP=SHR                            
//        DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR                 
//PTW1180D EXEC PTW1180D,                                               
//         JOBNM='PTW1180D',                                            
//         JOBNM1='PTW1170D',                                           
//         JOBNM2='PTW1160D'                                            
//                                                                      
