//P1835TWA JOB (P,14400,000,FINS),ORIG-STRPT,MSGCLASS=I,                
//        CLASS=1,REGION=8M                                             
//*                                                                     
//*                                                                     
//JOBLIB  DD DSN=PROD.BATCH.LOADLIB,DISP=SHR                            
//        DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR                 
//*                                                                     
//P1835TWA EXEC P180BTWA,STATE='NY',STNUM=P1835TW1                      
//*NEW YORK                                                             
//**----------------------------------------------------------------**  
//** SEND NOTIFICATION INDICATING TAX VENDOR FILE IS READY          **  
//**                                                                **  
//**----------------------------------------------------------------**  
//NOTFY999 EXEC CAJUCMNY,COND=(0,NE)                                    
//SYSPRINT DD  SYSOUT=*                                                 
//SYSIN    DD  *                                                        
MO ATTN: P1835TWA IS COMPLETE AND VENDOR CARTRIDGE IS READY.            
/*                                                                      
//                                                                      
