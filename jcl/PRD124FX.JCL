//PRD124FX JOB  (P,66800,000,LEGB),'DCS - RDR2013A',
//         CLASS=2,
//         MSGCLASS=I,
//         MSGLEVEL=(1,1)
//*
//* RDR2013A - DEFAULT BENEFICIARY
//*
//**********************************************************************
//* MODIFICATION LOG:
//*---------------------------------------------------------------------
//* DATE       NAME            CHG#      DESCRIPTION
//*---------------------------------------------------------------------
//* 08/28/2016 J. AVE          CHG393292 REPLACE P1240PTX WITH PCC2652D
//*                                      (NDM TO MQFTE)
//*                                      CHANGE CLASS=2 PER D. BUDALL
//**********************************************************************
//*
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*
//******************************************************************
//* EXECUTE PROC P1240PTX
//******************************************************************
//* PRD124FX EXEC P1240PTX,COND=(0,NE),
//*         JOBNM=PRD1246R,
//*         HLQ1=PPT.COR.XML
//*
//MQFTE001 EXEC PCC2652D,
//       PACKAGE=RDR2013A
