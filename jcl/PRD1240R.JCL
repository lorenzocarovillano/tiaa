//PRD1240R JOB (P,66955,000,LEGS),'RIDER DOMA',
//         MSGCLASS=I,
//         CLASS=1,
//         REGION=9M
//*
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//JOBLIB   DD  DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//*
//*****************************************************************
//* READ DA REPORTING EXTRACT & SPLIT BY NUMBER OF CONTRACTS
//*****************************************************************
//PRD1240R EXEC PRD1240R,
//      PARMMEM=RD1240R1,
//      DATA='DATA1'
//*****************************************************************
//* REPORTS
//*****************************************************************
//EXTR020.CMPRT01 DD SYSOUT=(&REPT.,RD1240R1)
//EXTR020.CMPRT02 DD SYSOUT=(&REPT.,RD1240R2)
