//PCP4010X JOB (P,66231,000,FINB),'CPS',MSGCLASS=I,CLASS=6,TIME=1440,   
//     MSGLEVEL=(1,1),REGION=4M                                         
//LIBSRCH  JCLLIB ORDER=PROD.OAS.NY.PARMLIB                             
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//***********************************************************           
//** TRANSFER VOIDS FILE TO MAINFRAME AND DELETE IT       ***           
//***********************************************************           
//NDMS010  EXEC PGM=DMBATCH,PARM=(YYSLYNN)                              
//DMMSGFIL DD  DISP=SHR,DSN=PMVSSF.STERLING.NDM.MSG                     
//DMNETMAP DD  DISP=SHR,DSN=PMVSSF.STERLING.NDM.NETMAP                  
//DMPUBLIB DD  DISP=SHR,DSN=PROD.NDM.NY.PROCESS.LIB                     
//DMPRINT  DD  SYSOUT=*                                                 
//NDMCMDS   DD SYSOUT=*                                                 
//SYSIN    DD  *                                                        
  SIGNON ESF=YES                                                        
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(CP4010X1) MAXDELAY=UNLIMITED -     
         CASE=YES
  SIGNOFF                                                               
/*                                                                      
//***********************************************************           
//** ARCHIVE FILE FROM MAINFRAME                          ***           
//***********************************************************           
//NDMS020  EXEC PGM=DMBATCH,PARM=(YYSLYNN),COND=(0,LT)                  
//DMMSGFIL DD  DISP=SHR,DSN=PMVSSF.STERLING.NDM.MSG                     
//DMNETMAP DD  DISP=SHR,DSN=PMVSSF.STERLING.NDM.NETMAP                  
//DMPUBLIB DD  DISP=SHR,DSN=PROD.NDM.NY.PROCESS.LIB                     
//DMPRINT  DD  SYSOUT=*                                                 
//NDMCMDS   DD SYSOUT=*                                                 
//SYSIN    DD  *                                                        
  SIGNON ESF=YES                                                        
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(CP4010X2) MAXDELAY=UNLIMITED -     
         CASE=YES
  SIGNOFF                                                               
/*                                                                      
//***********************************************************           
//** TRANSFER MAINFRAME FILE TO CHECKFREE SERVER          ***           
//***********************************************************           
//NDMS030  EXEC PGM=DMBATCH,PARM=(YYSLYNN),COND=(0,LT)                  
//DMMSGFIL DD  DISP=SHR,DSN=PMVSSF.STERLING.NDM.MSG                     
//DMNETMAP DD  DISP=SHR,DSN=PMVSSF.STERLING.NDM.NETMAP                  
//DMPUBLIB DD  DISP=SHR,DSN=PROD.NDM.NY.PROCESS.LIB                     
//DMPRINT  DD  SYSOUT=*                                                 
//NDMCMDS   DD SYSOUT=*                                                 
//SYSIN    DD  *                                                        
  SIGNON ESF=YES                                                        
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(CP4010X3) MAXDELAY=UNLIMITED -     
         CASE=YES
  SIGNOFF                                                               
/*                                                                      
