//PCC2518D JOB (P,66800,000,OPSS),'CIS IA REPRINT-DCS',
//         CLASS=1,
//         MSGCLASS=I
//*
//LIBSRCH  JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*
//JOBLIB   DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//         DD DSN=PROD.BATCH.LOADLIB,DISP=SHR
//**
//*********************************************************************
//**      SPLIT INPUT FILE BY CIS MDO PROFILE ID
//*********************************************************************
//**                                                                    
//DSNBUILD EXEC PCC2510D,
//       PARMMEM3=PC2518D1,
//       FILETYP='CIS.REPRINT.IA',
//       SRCFILE='PPM.ANN.CIS.REPRINT.IA.DIALOG.DATA'                   
//**                                                                    
//*********************************************************************
//**      EXECUTE PROC PCC2511D FOR EACH POSSIBLE CIS IA FILE           
//*********************************************************************
//BLDXML01 EXEC PCC2511D,
//       APPNM='CISRIA45',
//       SPC3='10,10',
//       PACKAGE='PTCISIA',
//       FILETYP='CIS.REPRINT.IA',
//       PROFNUM=PROFIL01
//**                                                                    
//*********************************************************************
//BLDXML02 EXEC PCC2511D,
//       APPNM='CISRIA46',
//       SPC3='10,10',
//       PACKAGE='PTCISIA',
//       FILETYP='CIS.REPRINT.IA',
//       PROFNUM=PROFIL02
//**                                                                    
//*********************************************************************
//BLDXML03 EXEC PCC2511D,
//       APPNM='CISRIA47',
//       SPC3='10,10',
//       PACKAGE='PTCISIA',
//       FILETYP='CIS.REPRINT.IA',
//       PROFNUM=PROFIL03
//**                                                                    
//*********************************************************************
//BLDXML04 EXEC PCC2511D,
//       APPNM='CISRIA48',
//       SPC3='10,10',
//       PACKAGE='PTCISIA',
//       FILETYP='CIS.REPRINT.IA',
//       PROFNUM=PROFIL04
//**                                                                    
//*********************************************************************
//BLDXML05 EXEC PCC2511D,
//       APPNM='CISRIA49',
//       SPC3='10,10',
//       PACKAGE='PTCISIA',
//       FILETYP='CIS.REPRINT.IA',
//       PROFNUM=PROFIL05
//**                                                                    
//*********************************************************************
//BLDXML06 EXEC PCC2511D,
//       APPNM='CISRIA52',
//       SPC3='10,10',
//       PACKAGE='PTCISIA',
//       FILETYP='CIS.REPRINT.IA',
//       PROFNUM=PROFIL06
//**                                                                    
//*********************************************************************
//BLDXML07 EXEC PCC2511D,
//       APPNM='CISRIA53',
//       SPC3='10,10',
//       PACKAGE='PTCISIA',
//       FILETYP='CIS.REPRINT.IA',
//       PROFNUM=PROFIL07
//**                                                                    
//*********************************************************************
//BLDXML08 EXEC PCC2511D,
//       APPNM='CISRIA56',
//       SPC3='10,10',
//       PACKAGE='PTCISIA',
//       FILETYP='CIS.REPRINT.IA',
//       PROFNUM=PROFIL08
//**                                                                    
//*********************************************************************
//BLDXML09 EXEC PCC2511D,
//       APPNM='CISRIA57',
//       SPC3='10,10',
//       PACKAGE='PTCISIA',
//       FILETYP='CIS.REPRINT.IA',
//       PROFNUM=PROFIL09
//**                                                                    
//*********************************************************************
//*    BUILD CONNECT:DIRECT COMMANDS FOR ALL XML FILES
//*********************************************************************
//**
//NDMBUILD EXEC PCC2512D,
//       JOBNM=PCC2518D,
//       PARMMEM1=PC2518D2,
//       FILETYP='CIS.REPRINT.IA'
//*
