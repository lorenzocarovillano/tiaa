//P1585TWX JOB (P,66113,000,FINB),                                      
//             'NDM SEND',                                              
//             MSGCLASS=I,                                              
//             CLASS=6                                                  
//**                                                                    
//*-----------------------------------------------------------------*   
//DMBATCH  EXEC PGM=DMBATCH,                                            
//             PARM=(YYSLYNN),                                          
//             REGION=4M                                                
//DMNETMAP DD DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR                   
//DMPUBLIB DD DSN=PMVSPP.STERLING.PROCESS.LIB,DISP=SHR                  
//DMMSGFIL DD DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR                      
//DMPRINT  DD SYSOUT=*                                                  
//SYSIN    DD *                                                         
  SIGNON ESF=YES                                                        
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(DN1585TX) MAXDELAY=UNLIMITED       
  SIGNOFF                                                               
/*                                                                      
