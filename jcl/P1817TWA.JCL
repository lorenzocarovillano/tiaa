//P1817TWA JOB (P,14400,000,FINS),ORIG-STRPT,MSGCLASS=I,                
//        CLASS=1,REGION=8M                                             
//*                                                                     
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//JOBLIB  DD DSN=PROD.BATCH.LOADLIB,DISP=SHR
//        DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//*
//P1817TWA EXEC P180DTWA,STATE='IN',STNUM=P1817TW1                      
//FRMT030.CMPRT01 DD SYSOUT=(&REPT.,TW1817A3)                           
//FRMT030.CMPRT02 DD SYSOUT=(&REPT.,TW1817A4)                           
//FRMT030.CMPRT03 DD SYSOUT=(&REPT.,TW1817A5)                           
//FRMT030.CMPRT04 DD SYSOUT=(&REPT.,TW1817A6)                           
//FRMT030.CMPRT05 DD SYSOUT=(&REPT.,TW1817A7)                           
//*INDIANA                                                              
/*
