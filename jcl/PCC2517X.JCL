//PCC2517X JOB (P,66800,000),'DCS-CIS MDO REPRINT',
//         CLASS=6,
//         MSGCLASS=I
//* PTCISMDO - STANDARD DELIVERY
//* CONNECT DIRECT XML FILE(S) TO DCS SERVER
//*
//LIBSRCH  JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*
//******************************************************************
//* EXECUTE PROC P1240PTX
//******************************************************************
//PCC2517X EXEC P1240PTX,COND=(0,NE),
//        JOBNM=PCC2517D,
//        HLQ1=PPT.COR.XML
