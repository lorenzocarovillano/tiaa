//PCI2130X JOB (P,15000,000,OPSB),'DIALOG',CLASS=6,MSGCLASS=I
//*
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*
//*********************************************************************
//* NDM TRANSFER - CIS NEWISSUE MDO REPRINT                           *
//*********************************************************************
//NDMS010 EXEC PCI2030X,
//        HLQ1='PPM.ANN',
//        JOBNM='PCI2130D',
//        PKGTYP='RP.MDO',
//        DMNETMAP=PMVSSF.STERLING.NDM.NETMAP,
//        DMPUBLIB=PMVSPP.STERLING.PROCESS.LIB,
//        DMMSGFIL=PMVSSF.STERLING.NDM.MSG
//*
//*********************************************************************
//* NDM TRANSFER - CIS NEWISSUE OTHER (IA, IPRO, TPA) REPRINT         *
//*********************************************************************
//NDMS020 EXEC PCI2030X,COND=(0,NE),
//        HLQ1='PPM.ANN',
//        JOBNM='PCI2130D',
//        PKGTYP='RP.IA',
//        DMNETMAP=PMVSSF.STERLING.NDM.NETMAP,
//        DMPUBLIB=PMVSPP.STERLING.PROCESS.LIB,
//        DMMSGFIL=PMVSSF.STERLING.NDM.MSG
/*
//
