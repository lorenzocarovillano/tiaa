//PNI1072D JOB (P,15000,000,OPSS),NEWISSU,CLASS=1,MSGCLASS=I            
//*                                                                     
//JOBLIB DD DSN=PROD.BATCH.LOADLIB,DISP=SHR                             
//       DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR                  
//       DD DSN=CEE.SCEERUN.RTEREUS,DISP=SHR                            
//       DD DSN=PROD.CICS.USERPRD1.LOADLIB,DISP=SHR                     
//       DD DSN=PCICSPP.OSSD.LOADLIB,DISP=SHR                           
//*                                                                     
//********************************************************************* 
//* CREATE NDM PARM - REPRINT WELCOME                                 * 
//********************************************************************* 
//CREA010 EXEC PNI1171D,                                                
//        HLQ1='PNA.ANN',                                               
//        JOBNM='PNI1072D',                                             
//        PKGTYP='RWLC',                                                
//        PARMMEM2='NI1171D4'                                           
//*                                                                     
//********************************************************************* 
//* CREATE NDM PARM - REPRINT LEGAL                                   * 
//********************************************************************* 
//CREA020 EXEC PNI1171D,COND=(0,NE),                                    
//        HLQ1='PNA.ANN',                                               
//        JOBNM='PNI1072D',                                             
//        PKGTYP='RLGL',                                                
//        PARMMEM2='NI1171D5'                                           
//*                                                                     
//                                                                      
