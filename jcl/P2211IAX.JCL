//P2211IAX JOB (P,74200,120,LEGB),IA,MSGCLASS=I,CLASS=2                 
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB                              
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//COPY020  EXEC PGM=IEBGENER                                            
//SYSUT1   DD  DSN=PPDT.P2210IAD.SORT20.IATRANS(0),                     
//             DISP=OLD,UNIT=CARTV                                      
//SYSUT2   DD  DSN=PPDTDR.P2210IAD.SORT20.IATRANS(+1),                  
//             DISP=(,CATLG,DELETE),TRTCH=COMP,                         
//             UNIT=CARTVDN,                                            
//             DCB=MODLDSCB,RECFM=LS LRECL=346                          
//SYSIN    DD  DUMMY                                                    
//SYSPRINT DD  SYSOUT=*                                                 
//**********************************************************************
