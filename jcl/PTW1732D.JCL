//PTW1732D JOB (P,14400,000,FINS),                                      
//            CPS-NPD,                                                  
//            MSGCLASS=I,                                               
//            CLASS=1,                                                  
//            REGION=8M                                                 
//*                                                                     
//*--------------------------------------------------------------------*
//*                                                                    *
//*                         T I A A - C R E F                          *
//*                                                                    *
//*                          *** PTW1732D ***                          *
//*                                                                    *
//*         TAXWARS MCCAMISH DAILY LIFE INSURANCE FILE PROCESSING      *
//*                                                                    *
//*--------------------------------------------------------------------*
//JOBLIB DD DSN=PROD.BATCH.LOADLIB,DISP=SHR                             
//       DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR                  
//*                                                                     
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB                              
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*---------------------------------------------------------------------
//PTW1732D EXEC PTW1730D,                                               
//         JOBNM='PTW1732D',                                            
//         JOBNM1='PTW1742D',                                           
//         REPID1=',TW1732D1',                                          
//         REPID2=',TW1732D2',                                          
//         REPID3=',TW1732D3'                                           
//*                                                                     
//S010.CMWKF01 DD DSN=&HLQ0..&JOBNM..LIFE.FILE,DISP=SHR                 
//S040.SYSUT1  DD DSN=&HLQ0..&JOBNM..LIFE.FILE,DISP=SHR                 
//S040.SYSUT2  DD DSN=&HLQ0..&JOBNM..LIFE.DATA(&GDG1),                  
//            DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,                   
//            SPACE=(CYL,(&SPC1),RLSE),                                 
//            UNIT=SYSDA,                                               
//            DCB=MODLDSCB,RECFM=LS,LRECL=671                           
//S050.SYSUT1  DD DSN=&HLQ0..&JOBNM..LIFE.FILE,DISP=SHR                 
//S060.DELMF   DD  DSN=&HLQ0..&JOBNM..LIFE.FILE,                        
//             UNIT=SYSDA,SPACE=(TRK,0),                                
//             DISP=(MOD,DELETE,DELETE)                                 
