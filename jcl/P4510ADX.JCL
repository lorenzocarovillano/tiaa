//P4510ADX JOB (P,19999,000,FINB),'CORPGL',                             
//             MSGCLASS=I,                                              
//             CLASS=6                                                  
//*
//LIBSRCH  JCLLIB ORDER=PROD.OAS.NY.PARMLIB                             
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//**                                                                    
//*=====================================================================
//*  DATE        NAME           CHG#       DESCRIPTION
//*---------------------------------------------------------------------
//*  08/23/2019  SUSHOBHAN ROY  CHG612009  SEPARATE THE NDM AND MQFTE
//*                                        IN P4510ADX
//*=====================================================================
//NDMS020  EXEC PGM=DMBATCH,                                            
//             PARM=(YYSLYNN),                                          
//             REGION=4M                                                
//DMNETMAP DD  DSN=PMVSSF.STERLING.NDM.NETMAP,                          
//             DISP=SHR                                                 
//DMPUBLIB DD  DSN=PMVSPP.STERLING.PROCESS.LIB,                         
//             DISP=SHR                                                 
//DMMSGFIL DD  DSN=PMVSSF.STERLING.NDM.MSG,                             
//             DISP=SHR                                                 
//DMPRINT  DD  SYSOUT=*                                                 
//NDMCMDS  DD  SYSOUT=*                                                 
//SYSUDUMP DD  SYSOUT=U                                                 
//SYSIN    DD  *                                                        
  SIGNON ESF=YES                                                        
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(DN4510AA) MAXDELAY=UNLIMITED       
  SIGNOFF                                                               
//**                                                                    
