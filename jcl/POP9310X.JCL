//POP9310X JOB (P,66231,000,OPSB),'OMNIPAY EFT PRO',                    
//        CLASS=6,MSGCLASS=I,                                           
//        REGION=1024K,TIME=1440                                        
//*                                                                     
//****************************************************                  
//*                                                                     
//*   POP9310X -- PURPOSE:  JPMC OUTBOUND TRANSMISSION                  
//*               TYPE:     ACH DEPOSITS (PRO BANK MODEL/MID-DAY)       
//*               FILE:     POMPY.PMT0.EFTPRO(0)                        
//*                                                                     
//****************************************************                  
//NDMS010  EXEC PGM=DMBATCH,PARM=(NYSSNNN)                              
//DMNETMAP DD DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR                   
//DMMSGFIL DD DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR                      
//DMPUBLIB DD DSN=PROD.NDM.NY.PROCESS.LIB,DISP=SHR                      
//DMPRINT  DD SYSOUT=*                                                  
//NDMCMDS  DD SYSOUT=*                                                  
//SYSUDUMP DD SYSOUT=U                                                  
//SYSIN    DD *                                                         
  SIGNON                                                                
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(OP0110D1)        -                 
  MAXDELAY=UNLIMITED                                  -
  CASE=YES                                            -                 
  &LOCDSN=POMPY.PMTP.EFTPRO(0)                        -                 
  &RMTNODE=TC_INT_PDCD                                -                 
  &SIDIR="/mailbox/OmniPay/JPMC/"                     -                 
  &DTYPE=ACH                                          -                 
  &DFORMAT=NACHA14                                    -                 
  &RMTFN="OP9310D1.TXT"                                                 
  SIGNOFF                                                               
//                                                                      
