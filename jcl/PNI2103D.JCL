//PNI2103D JOB (P,14000,000,OPSB),'PNI',
//         CLASS=1,MSGCLASS=I
//*
//**********************************************************************
//* THIS JOB WILL DO THE EMPTY FILE CHECK FOR
//* PPM.ANN.MDM.ACIS.PARTIC.FILE(0)
//**********************************************************************
//*
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*
//JOBLIB  DD DSN=PROD.BATCH.LOADLIB,DISP=SHR
//*
// SET JCLO='*'
// SET LIB1='PROD'
//*
//*********************************************************************
//* VERIFY THAT THERE IS DATA TO PROCESS
//*********************************************************************
//VERY001  EXEC  PGM=IDCAMS
//SYSPRINT DD  SYSOUT=&JCLO
//DD1      DD  DSN=PPM.ANN.MDM.ACIS.PARTIC.FILE(0),DISP=SHR
//SYSIN    DD  DSN=&LIB1..PARMLIB(OMA012D2),DISP=SHR
//*
