//P1429CPX JOB (P,66231,000,FINB),CAIMS,MSGCLASS=I,CLASS=6,             
//  REGION=8M,TIME=1440                                                 
//NDMS020 EXEC PGM=DMBATCH,REGION=4M,PARM=(YYSLYNN)                     
//DMNETMAP DD DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR                   
//DMPUBLIB DD DSN=PMVSPP.STERLING.PROCESS.LIB,DISP=SHR                  
//DMMSGFIL DD DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR                      
//DMPRINT   DD SYSOUT=*                                                 
//NDMCMDS   DD SYSOUT=*                                                 
//SYSUDUMP  DD SYSOUT=U                                                 
//SYSIN     DD *                                                        
  SIGNON ESF=YES                                                        
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(DN1429CP) CASE=YES -
  MAXDELAY=UNLIMITED                                                    
  SIGNOFF                                                               
/*                                                                      
//NDMS030 EXEC PGM=DMBATCH,REGION=4M,PARM=(YYSLYNN),COND=EVEN           
//DMNETMAP DD DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR                   
//DMPUBLIB DD DSN=PMVSPP.STERLING.PROCESS.LIB,DISP=SHR                  
//DMMSGFIL DD DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR                      
//DMPRINT   DD SYSOUT=*                                                 
//NDMCMDS   DD SYSOUT=*                                                 
//SYSUDUMP  DD SYSOUT=U                                                 
//SYSIN     DD *                                                        
  SIGNON ESF=YES                                                        
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(DD1429CP)   -                      
  MAXDELAY=UNLIMITED                                                    
  SIGNOFF                                                               
/*                                                                      
//                                                                      
