//PRD1041R JOB (P,66800,000,LEGS),'2006 ACCS',
//         MSGCLASS=I,
//         CLASS=1
//*
//**********************************************************************
//* MODIFICATION LOG:
//*---------------------------------------------------------------------
//* DATE       NAME            CHG#      DESCRIPTION
//*---------------------------------------------------------------------
//* 08/27/2016 J. AVE          CHG393292 CHANGE PROC FROM PCC2001R TO
//*                                      PCC2004R
//*
//**********************************************************************
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//JOBLIB   DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//        DD DSN=PROD.BATCH.LOADLIB,DISP=SHR
//*
//**********************************************************************
//*
//PRD1041R EXEC PCC2004R,
//*       PARMMEM=RD1041R1,
//       PARMMEM2=RD1041R2,
//       PARMMEM3=RD1041R3,
//       PARMMEM6=RD1041R4,
//       PACKAGE=RDR2009D,
//       DATATYPE=Y2006.ACCESS,
//       JOBNM=PRD1041R,
//       FILE=FILE1
//*
//EXTR010.CMPRT01 DD SYSOUT=(&REPT.,RD1041R1)
//EXTR010.CMPRT02 DD SYSOUT=(&REPT.,RD1041R2)
//EXTR020.CMPRT01 DD SYSOUT=(&REPT.,RD1041R3)
//EXTR020.CMPRT02 DD SYSOUT=(&REPT.,RD1041R4)
//*
