//PTW1760D JOB (P,14400,000,FINS),                                      
//            CPS-NPD,                                                  
//            MSGCLASS=I,                                               
//            CLASS=1,                                                  
//            REGION=8M                                                 
//*                                                                     
//*--------------------------------------------------------------------*
//*                                                                    *
//*     T I A A - C R E F   O P E N   P L A N   S O L U T I O N S      *
//*                                                                    *
//*                          *** PTW1760D ***                          *
//*                                                                    *
//*                   TAXWARS MCCAMISH DAILY FEED (CURRENT YEAR)       *
//*                               UPDATE                               *
//*                                                                    *
//*--------------------------------------------------------------------*
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB                              
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//JOBLIB  DD DSN=PROD.BATCH.LOADLIB,DISP=SHR                            
//        DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR                 
//*-----------------------------------------------------------          
//PTW1760D EXEC PTW1760D,                                               
//         JOBNM='PTW1760D',                                            
//         JOBNM1='PTW1750D',                                           
//         JOBNM2='PTW1740D',                                           
//         REPID2=',TW1760D2',                                          
//         REPID3=',TW1760D3',                                          
//         REPID4=',TW1760D4',                                          
//         REPID5=',TW1760D5',                                          
//         REPID6=',TW1760D6',                                          
//         REPID7=',TW1760D7',                                          
//         REPID8=',TW1760D8',                                          
//         REPID9=',TW1760D9',                                          
//         REPIDA=',TW1760DA',                                          
//         REPIDB=',TW1760DB',                                          
//         REPIDC=',TW1760DC',                                          
//         REPIDD=',TW1760DD',                                          
//         REPIDE=',TW1760DE',                                          
//         REPIDF=',TW1760DF',                                          
//         REPIDG=',TW1760DG',                                          
//         REPIDH=',TW1760DH',                                          
//         REPIDJ=',TW1760DJ',                                          
//         REPIDK=',TW1760DK',                                          
//         REPIDL=',TW1760DL',                                          
//         REPIDM=',TW1760DM',                                          
//         REPIDN=',TW1760DN',                                          
//         REPIDO=',TW1760DO',                                          
//         REPIDP=',TW1760DP'                                           
//                                                                      
