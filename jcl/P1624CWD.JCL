//P1624CWD  JOB (P,66241,000,LEGS),'NCW-SMART-EXTRACT',MSGCLASS=I,
//         CLASS=1
//*
//********************************************************************
//*  THIS JOB READS NCW-MASTER FILE AND EXTRACT "SMART" WORK-LEADS
//*  RECORDS TO CREATE FILES FOR REPORTING PURPOSES.
//********************************************************************
//*
//LIBSRCH JCLLIB ORDER=(PROD.OAS.NY.PARMLIB,PROD.PROCLIB)               
//*
//*                                                                     
//*
//JOBLIB   DD  DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//*
//P1624CWD EXEC P1624CWD
//
