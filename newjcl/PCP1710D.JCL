//PCP1710D JOB (C,69999,000,FINS),'CPS/IS UPDATE',CLASS=1,              
//       MSGCLASS=I,PRTY=12                             MOBIUS > 60 DYS 
//*                                                                     
//LIBSRCH  JCLLIB ORDER=(PROD.OAS.NY.PARMLIB)                           
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*--------------------------------------------------------------------*
//*                                                                    *
//*              I N V E S T M E N T   S O L U T I O N S               *
//*                                                                    *
//*                           *** PCP1710D ***                         *
//*                                                                    *
//*      CPS - UPDATE DATA BASE PAYMENTS.                              *
//*                                                                    *
//*--------------------------------------------------------------------*
//JOBLIB   DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR                
//         DD DSN=PROD.BATCH.LOADLIB,DISP=SHR                           
//*                                                                     
//PCP1710D EXEC PCP1710D,                                               
//         RPTID='CP1710D',         MOBIUS REPORT ID'S                  
//         GDG0='0',                CURRENT GDG FILE                    
//         GDG1='+1'                NEW GDG FILE CREATED                
