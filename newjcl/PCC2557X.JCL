//PCC2557X JOB (P,66800,000,OPSS),'CCP - PTX1099R',
//         CLASS=1,
//         MSGCLASS=I
//* PTX1099R - E-DELIVERY
//* SEND XML FILE(S) TO CCP SERVER VIA MQFTE
//*
//LIBSRCH  JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*
//JOBLIB   DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//         DD DSN=PROD.BATCH.LOADLIB,DISP=SHR
//*
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*
//******************************************************************
//* EXECUTE PROC TO TRANSMIT XML FILE TO CCP
//*   FROM PACKAGE=E1099R16 TO PACKAGE=E1099R20
//******************************************************************
//*
//*
//MQFTE016 EXEC PCC3001D,
//       JOBNM='E1099R16',
//       FN='16'
//*
//MQFTE017 EXEC PCC3001D,
//       JOBNM='E1099R17',
//       FN='17'
//*
//MQFTE018 EXEC PCC3001D,
//       JOBNM='E1099R18',
//       FN='18'
//*
//MQFTE019 EXEC PCC3001D,
//       JOBNM='E1099R19',
//       FN='19'
//*
//MQFTE020 EXEC PCC3001D,
//       JOBNM='E1099R20',
//       FN='20'
//*
