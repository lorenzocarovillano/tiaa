//PNI3502M JOB (P,19999,000,OPSS),NI,CLASS=2,MSGCLASS=I                 
//**                                                                    
//LIBSRCH  JCLLIB ORDER=PROD.OAS.NY.PARMLIB                             
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//JOBLIB DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR                  
//       DD DSN=PROD.CICS.USERPRD1.LOADLIB,DISP=SHR                     
//*                                                                     
//PNI3502M EXEC PNI3502M                                                
//*****************************************************************     
//* REPORTS                                                             
//*****************************************************************     
//REPT030.CMPRT01 DD SYSOUT=(8,NI3502M1)                                
//REPT030.CMPRT02 DD SYSOUT=(8,NI3502M2)                                
//REPT030.CMPRT03 DD SYSOUT=(8,NI3502M3)                                
//REPT030.CMPRT04 DD SYSOUT=(8,NI3502M4)                                
//REPT030.CMPRT05 DD SYSOUT=(8,NI3502M5)                                
//REPT030.CMPRT06 DD SYSOUT=(8,NI3502M6)                                
//REPT030.CMPRT07 DD SYSOUT=(8,NI3502M7)                                
//REPT030.CMPRT08 DD SYSOUT=(8,NI3502M8)                                
//REPT030.CMPRT09 DD SYSOUT=(8,NI3502M9)                                
//REPT030.CMPRT10 DD SYSOUT=(8,NI3502MA)                                
//REPT030.CMPRT11 DD SYSOUT=(8,NI3502MB)                                
//REPT030.CMPRT12 DD SYSOUT=(8,NI3502MC)                                
//REPT030.CMPRT13 DD SYSOUT=(8,NI3502MD)                                
//REPT030.CMPRT14 DD SYSOUT=(8,NI3502ME)                                
//REPT030.CMPRT15 DD SYSOUT=(8,NI3502MF)                                
//REPT030.CMPRT16 DD SYSOUT=(8,NI3502MG)                                
//*                                                                     
