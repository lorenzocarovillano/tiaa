//PCI2130D JOB (P,67253,000,OPSS),'DIALOG',CLASS=1,MSGCLASS=I           
//*                                                                     
//LIBSRCH  JCLLIB ORDER=PROD.OAS.NY.PARMLIB                             
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*                                                                     
//JOBLIB DD DSN=PROD.BATCH.LOADLIB,DISP=SHR                             
//       DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR                  
//       DD DSN=CEE.SCEERUN.RTEREUS,DISP=SHR                            
//       DD DSN=PROD.CICS.USERPRD1.LOADLIB,DISP=SHR                     
//       DD DSN=PCICSPP.OSSD.LOADLIB,DISP=SHR                           
//*                                                                     
//********************************************************************* 
//* CREATE NDM PARM - REPRINT MDO                                     * 
//********************************************************************* 
//CREA010 EXEC PCI2030D,                                                
//        HLQ1='PPM.ANN',                                               
//        JOBNM='PCI2130D',                                             
//        PKGTYP='RP.MDO',                                              
//        PARMMEM2='CI2130D1'                                           
//*                                                                     
//********************************************************************* 
//* CREATE NDM PARM - REPRINT IA, IPRO, TPA                           * 
//********************************************************************* 
//CREA020 EXEC PCI2030D,COND=(0,NE),                                    
//        HLQ1='PPM.ANN',                                               
//        JOBNM='PCI2130D',                                             
//        PKGTYP='RP.IA',                                               
//        PARMMEM2='CI2130D2'                                           
//*                                                                     
//                                                                      
