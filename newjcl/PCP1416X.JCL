//PCP1416X JOB (P,66231,000,FINB),'CPS/WACHO VOIDS',CLASS=6,            
//       MSGCLASS=I                                                     
//*                                                                     
//NDMS010 EXEC NDMPROC                                                  
//SYSIN    DD  *                                        
 SIGNON                                                                 
  SUBMIT PROC=CP1421D1                              -
  &LOCDSN=PCPS.ANN.PCP1416D.WACHO.VOIDS1(0)         -
  &RMTNODE=TC_INT_PDCD                              -
  &SIDIR="/mailbox/Axletree/WELLSFARGO/ARP"         -
  &SIFN="PCPS.ANN.PCP1416D.WACHO.VOIDS1.ARP15"      -
  MAXDELAY=UNLIMITED                                -
  CASE=YES                                                              
 SIGNOFF                                                                
//NDMS020 EXEC NDMPROC                                                  
//SYSIN    DD  *                                        
 SIGNON                                                                 
  SUBMIT PROC=CP1421D1                              -
  &LOCDSN=PCPS.ANN.PCP1416D.WACHO.VOIDS2(0)         -
  &RMTNODE=TC_INT_PDCD                              -
  &SIDIR="/mailbox/Axletree/WELLSFARGO/ARP"         -
  &SIFN="PCPS.ANN.PCP1416D.WACHO.VOIDS2.ARP12"      -
  MAXDELAY=UNLIMITED                                -
  CASE=YES                                                              
 SIGNOFF                                                                
