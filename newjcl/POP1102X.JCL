//POP1102X JOB (P,19999,000,OPSB),'OMNIPAY',MSGCLASS=I,CLASS=1
//* ------------------------------------------------------------------*
//LIBSRCH  JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//* ------------------------------------------------------------------*
//*  CTH   TEXAS CHILD SUPPORT EXTRACT / DAILY                        *
//* ------------------------------------------------------------------*
//*
//S1 EXEC MQFTEBPX,
//   USRPARMS='PROD.WMQFTE.PARMLIB(OP1102D1)'
