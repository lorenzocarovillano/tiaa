//P2205CPX JOB (P,66231,000,FINB),POD,CLASS=6,MSGCLASS=I,
//  TIME=1440
//*-----#NDM------------------------------------------------------*     
//NDMS999 EXEC PGM=DMBATCH,REGION=4M,PARM=(YYSLYNN)                     
//DMMSGFIL  DD DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR                     
//DMNETMAP  DD DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR                  
//DMPUBLIB  DD DSN=PROD.NDM.NY.PROCESS.LIB,DISP=SHR                     
//DMPRINT   DD SYSOUT=*                                                 
//SYSIN     DD *                                                        
  SIGNON ESF=YES                                                        
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(DATA2205) MAXDELAY=UNLIMITED       
  SIGNOFF                                                               
/*                                                                      
//                                                                      
