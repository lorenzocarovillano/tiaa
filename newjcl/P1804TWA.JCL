//P1804TWA JOB (P,14400,000,FINS),ORIG-STRPT,MSGCLASS=I,                
//        CLASS=1,REGION=8M                                             
//*
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//JOBLIB  DD DSN=PROD.BATCH.LOADLIB,DISP=SHR
//        DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//*
//P1804TWA EXEC P180DTWA,STATE='AR',STNUM=P1804TW1                      
//FRMT030.CMPRT01 DD SYSOUT=(&REPT.,TW1804A3)                           
//FRMT030.CMPRT02 DD SYSOUT=(&REPT.,TW1804A4)                           
//FRMT030.CMPRT03 DD SYSOUT=(&REPT.,TW1804A5)                           
//FRMT030.CMPRT04 DD SYSOUT=(&REPT.,TW1804A6)                           
//FRMT030.CMPRT05 DD SYSOUT=(&REPT.,TW1804A7)                           
//*ARKANSAS                                                             
/*
