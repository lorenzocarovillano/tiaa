//PRD1221R JOB (P,66800,000,LEGS),'GROSS UP',
//         MSGCLASS=I,
//         CLASS=1
//*
//* RDR2011D - GROSS UP (PAPER AND EMAIL)
//*
//**********************************************************************
//* MODIFICATION LOG:
//*---------------------------------------------------------------------
//* DATE       NAME            CHG#      DESCRIPTION
//*---------------------------------------------------------------------
//* 08/27/2016 J. AVE          CHG393292 CHANGE PROC FROM PCC2001R TO
//*                                      PCC2004R
//*
//**********************************************************************
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//JOBLIB   DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//        DD DSN=PROD.BATCH.LOADLIB,DISP=SHR
//*
//**********************************************************************
//*
//PRD1221R EXEC PCC2004R,
//*       PARMMEM=RD1221R1,
//       PARMMEM2=RD1221R2,
//       PARMMEM3=RD1221R3,
//       PARMMEM6=RD1221R4,
//       PACKAGE=RDR2011D,
//       DATATYPE=GROSSUP,
//       JOBNM=PRD1221R,
//       FILE=FILE1
//*
//EXTR010.CMPRT01 DD SYSOUT=(&REPT.,RD1221R1)
//EXTR020.CMPRT01 DD SYSOUT=(&REPT.,RD1221R2)
//EXTR020.CMPRT02 DD SYSOUT=(&REPT.,RD1221R3)
//*
