//PCW1617X JOB (C,67412,000,LEGB),'NDM CWF-EXTR',MSGCLASS=I,CLASS=6
//*
//*********************************************************************
//*                     JOB DESCRIPTION                               *
//*-------------------------------------------------------------------*
//* THIS JOB TRANSMITS 6 CWF - NON-PARTICIPANT EXTRACT FILES,CREATED  *
//* FROM JOB P1617CWD, TO LINUX SERVER CHAPDA3ORCCP01.                *
//*********************************************************************
//*                     MODIFICATION LOG                              *
//*-------------------------------------------------------------------*
//* CHANGE REQUEST NUMBER : CHG319449                                 *
//* IMPLEMENTATION DATE : 11/22/2014                                  *
//* AUTHOR : COGNIZANT                                                *
//* VERSION :INITIAL VERSION.                                         *
//*          CREATED IN PROJECT - "Mainframe Oracle Retirement - ORP3"*
//*          (TC00002875)                                             *
//*********************************************************************
//*
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*
//********************************************************************
//* TRANSMITS PCWF.COR.P1617CWD.TOT.CONTROL.EXT FILE TO LINUX SERVER
//********************************************************************
//NDMS010  EXEC PGM=DMBATCH,REGION=4M,PARM=(YYSLYNN),TIME=1440,
//             COND=(0,NE)
//DMMSGFIL DD  DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR
//DMNETMAP DD  DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR
//DMPUBLIB DD  DSN=PMVSPP.STERLING.PROCESS,DISP=SHR
//DMPRINT  DD  SYSOUT=*
//NDMCMDS   DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=U
//SYSIN     DD *
  SIGNON
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(CWF000X1)         -
  &DSN1=PCWF.COR.P1617CWD.TOT.CONTROL.EXT              -
  &DSN2=npart.control_totals_table                     -
  &OUTOPTS=:datatype=text:xlate=yes:strip.blanks=no:   -
  &ERRMSG='PCW1617X.NDMS010'                           -
  MAXDELAY=UNLIMITED CASE=YES
  SIGNOFF
/*
//*
//********************************************************************
//* TRANSMITS PCWF.COR.P1617CWD.WRKRQST.ADD.EXT FILE TO LINUX SERVER
//********************************************************************
//NDMS020  EXEC PGM=DMBATCH,REGION=4M,PARM=(YYSLYNN),TIME=1440,
//             COND=(0,NE)
//DMMSGFIL DD  DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR
//DMNETMAP DD  DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR
//DMPUBLIB DD  DSN=PMVSPP.STERLING.PROCESS,DISP=SHR
//DMPRINT  DD  SYSOUT=*
//NDMCMDS   DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=U
//SYSIN     DD *
  SIGNON
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(CWF000X1)         -
  &DSN1=PCWF.COR.P1617CWD.WRKRQST.ADD.EXT              -
  &DSN2=npart.work_request.add                         -
  &OUTOPTS=:datatype=text:xlate=yes:strip.blanks=no:   -
  &ERRMSG='PCW1617X.NDMS020'                           -
  MAXDELAY=UNLIMITED CASE=YES
  SIGNOFF
/*
//*
//********************************************************************
//* TRANSMITS PCWF.COR.P1617CWD.WRKRQST.UPD.EXT FILE TO LINUX SERVER
//********************************************************************
//NDMS030  EXEC PGM=DMBATCH,REGION=4M,PARM=(YYSLYNN),TIME=1440,
//             COND=(0,NE)
//DMMSGFIL DD  DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR
//DMNETMAP DD  DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR
//DMPUBLIB DD  DSN=PMVSPP.STERLING.PROCESS,DISP=SHR
//DMPRINT  DD  SYSOUT=*
//NDMCMDS   DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=U
//SYSIN     DD *
  SIGNON
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(CWF000X1)         -
  &DSN1=PCWF.COR.P1617CWD.WRKRQST.UPD.EXT              -
  &DSN2=npart.work_request_temp.upd                    -
  &OUTOPTS=:datatype=text:xlate=yes:strip.blanks=no:   -
  &ERRMSG='PCW1617X.NDMS030'                           -
  MAXDELAY=UNLIMITED CASE=YES
  SIGNOFF
/*
//*
//********************************************************************
//* TRANSMITS PCWF.COR.P1617CWD.ADTNL.WR.ADD.EXT FILE TO LINUX SERVER
//********************************************************************
//NDMS040  EXEC PGM=DMBATCH,REGION=4M,PARM=(YYSLYNN),TIME=1440,
//             COND=(0,NE)
//DMMSGFIL DD  DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR
//DMNETMAP DD  DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR
//DMPUBLIB DD  DSN=PMVSPP.STERLING.PROCESS,DISP=SHR
//DMPRINT  DD  SYSOUT=*
//NDMCMDS   DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=U
//SYSIN     DD *
  SIGNON
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(CWF000X1)         -
  &DSN1=PCWF.COR.P1617CWD.ADTNL.WR.ADD.EXT             -
  &DSN2=npart.addl_work_request_info.add               -
  &OUTOPTS=:datatype=text:xlate=yes:strip.blanks=no:   -
  &ERRMSG='PCW1617X.NDMS040'                           -
  MAXDELAY=UNLIMITED CASE=YES
  SIGNOFF
/*
//*
//********************************************************************
//* TRANSMITS PCWF.COR.P1617CWD.ADTNL.WR.UPD.EXT TO LINUX SERVER.
//********************************************************************
//NDMS050  EXEC PGM=DMBATCH,REGION=4M,PARM=(YYSLYNN),TIME=1440,
//             COND=(0,NE)
//DMMSGFIL DD  DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR
//DMNETMAP DD  DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR
//DMPUBLIB DD  DSN=PMVSPP.STERLING.PROCESS,DISP=SHR
//DMPRINT  DD  SYSOUT=*
//NDMCMDS   DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=U
//SYSIN     DD *
  SIGNON
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(CWF000X1)         -
  &DSN1=PCWF.COR.P1617CWD.ADTNL.WR.UPD.EXT             -
  &DSN2=npart.addl_work_request_info_temp.upd          -
  &OUTOPTS=:datatype=text:xlate=yes:strip.blanks=no:   -
  &ERRMSG='PCW1617X.NDMS050'                           -
  MAXDELAY=UNLIMITED CASE=YES
  SIGNOFF
/*
//*
//********************************************************************
//* TRANSMITS PCWF.COR.P1617CWD.CSTATUS.ADD.EXT FILE TO LINUX SERVER
//********************************************************************
//NDMS060  EXEC PGM=DMBATCH,REGION=4M,PARM=(YYSLYNN),TIME=1440,
//             COND=(0,NE)
//DMMSGFIL DD  DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR
//DMNETMAP DD  DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR
//DMPUBLIB DD  DSN=PMVSPP.STERLING.PROCESS,DISP=SHR
//DMPRINT  DD  SYSOUT=*
//NDMCMDS   DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=U
//SYSIN     DD *
  SIGNON
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(CWF000X1)         -
  &DSN1=PCWF.COR.P1617CWD.CSTATUS.ADD.EXT              -
  &DSN2=npart.special_activity.add                     -
  &OUTOPTS=:datatype=text:xlate=yes:strip.blanks=no:   -
  &ERRMSG='PCW1617X.NDMS060'                           -
  MAXDELAY=UNLIMITED CASE=YES
  SIGNOFF
/*
//*
