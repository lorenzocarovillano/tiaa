//PCP1785D JOB (C,69999,000,FINS),'CPS/IS STOP',CLASS=1,                
//       MSGCLASS=I,PRTY=12                    MOBIUS                   
//*                                                                     
//LIBSRCH  JCLLIB ORDER=PROD.OAS.NY.PARMLIB                             
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*--------------------------------------------------------------------*
//*                                                                    *
//*              I N V E S T M E N T   S O L U T I O N S               *
//*                                                                    *
//*                           *** PCP1785D ***                         *
//*                                                                     
//*      CPOP750                                                        
//*                                                                     
//*                                                                    *
//*--------------------------------------------------------------------*
//JOBLIB   DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR                
//         DD DSN=PROD.BATCH.LOADLIB,DISP=SHR                           
//*                                                                     
//PCP1785D EXEC PCP1785D            HLQ FOR INPUT FILES CREATED IN PROC 
//*                                                                     
//                                                                      
