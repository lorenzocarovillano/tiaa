//PCP1862D JOB (P,78000,000,FINB),'CPS GW RN EXTRACT',CLASS=1,          
//       MSGCLASS=I                                                     
//*                                                                     
//LIBSRCH  JCLLIB ORDER=(PROD.OAS.NY.PARMLIB)                           
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*--------------------------------------------------------------------*
//*  CONSOLIDATED PAYMENT SYSTEMS (CPS)                                *
//*  * PCP1861D  COMBINE DAILY FILES FOR GENERIC WARRANTS TO OMNIPRO   *
//*--------------------------------------------------------------------*
//*                                                                     
//JOBLIB   DD DSN=PROD.BATCH.LOADLIB,DISP=SHR                           
//*                                                                     
//PCP1862D EXEC PCP1862D                                                
//*                                                                     
