//P5125NIX JOB (P,15000,113,OPSB),                                      
//             'ACIS.XMIT',                                             
//             CLASS=1,                                                 
//             MSGCLASS=I                                               
//**                                                                    
//OUTSAR OUTPUT JESDS=ALL,DEFAULT=YES,DEST=MVSJESD                      
//*N                                                                    
//**                                                                    
//NDMS010  EXEC PGM=DMBATCH,                                            
//             PARM=(YYSLYNN),                                          
//             REGION=4M                                                
//DMMSGFIL DD  DSN=PMVSSF.STERLING.NDM.MSG,                             
//             DISP=SHR                                                 
//DMNETMAP DD  DSN=PMVSSF.STERLING.NDM.NETMAP,                          
//             DISP=SHR                                                 
//DMPUBLIB DD  DSN=PROD.NDM.DN.PROCESS.LIB,                             
//             DISP=SHR                                                 
//DMPRINT  DD  SYSOUT=*                                                 
//SYSIN    DD  *                                                        
  SIGNON ESF=YES                                                        
  SUBMIT DSN=PROD.NDM.DN.PROCESS.LIB(ADDA5125)                          
  SIGNOFF                                                               
