//P1429CPT JOB (P,66412,000,FINB),TA,CLASS=1,MSGCLASS=I,
//      MSGLEVEL=(1,1)
//DELT005 EXEC PGM=IEFBR14                                              
//DD1     DD DSN=PPDD.SCD.TRIGGER.NY.P1429CPT,                          
//          DISP=(MOD,DELETE),UNIT=SYSDA,SPACE=(TRK,0),                 
//*//          RECFM=FB,LRECL=80,DCB=MODLDSCB
//          DCB=(RECFM=FB,LRECL=80)
//COPY010 EXEC PGM=IEBGENER,COND=(0,NE,DELT005)                         
//SYSPRINT DD SYSOUT=*                                                  
//SYSIN  DD DUMMY                                                       
//SYSUT1 DD DSN=PPDD.CASCD.JOB.TRIGGER,DISP=SHR                         
//SYSUT2 DD DSN=PPDD.SCD.TRIGGER.NY.P1429CPT,                           
//          DISP=(,CATLG,DELETE),UNIT=SYSDA,SPACE=(TRK,(1,1)),          
//*//          RECFM=FB,LRECL=80,DCB=MODLDSCB
//          DCB=(RECFM=FB,LRECL=80)
//*************************************************************
