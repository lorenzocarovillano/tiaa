//PCK6300D JOB (C,67253,000,OPSB),'OMNIPLUS',CLASS=2,
//           MSGCLASS=I,REGION=0M,PRTY=12
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//JOBLIB DD DSN=PROD.OMNIPLUS.USER.LOADLIB,DISP=SHR
//       DD DSN=PDBFPP.OMNIPLUS.CURRENT.BAT.LOADLIB,DISP=SHR
//       DD DSN=PROD.BATCH.LOADLIB,DISP=SHR
//**********************************************************************
//*   GENERATE OMNISCRIPT EXTRACT FOR CPS                           ****
//**********************************************************************
//*---------------------------------------------------------------------
//*   GLOBAL SUBSTITUTION VALUES (SETS) ARE WITHIN THE INCLUDE MEMBER
//*---------------------------------------------------------------------
//*MSYS // INCLUDE MEMBER=TCEDIT
//*-------------------------------------------------------------------
//* EDIT PROCESSING SYMBOLIC VALUES
//* REVISIONS:
//*  ADDED SET SVINFO=PDA.ONL.ANN FOR NEW STABLE VALUE FILE
//*                                     BK  2010/05/15
//*  ADDED SYMBOLIC FOR LARGE FLAT FILES
//*                                     PDL 2007/04/20
//*  ADDED SET DISKPRE='PPDD.F' FOR PERM FLAT FILES
//*                                     SDR 2006/07/31
//*  ADDED SET UTA490=PINT.ONL.ANN FOR NEW MDO FILE
//*                                     SDR 2006/07/25
//*  CHANGED VALUES IN CICRG01 AND CICRG02 TO REFLECT THE NEW CICS
//*         REGION NAMES                SDR 2006/06/21
//*  ADDED SET BKUPPRE='PPDT.F' FOR TAPE BACKUP DSN
//*         PER SMS CHANGED STANDARDS   SDR 2006/02/10
//*  ADDED WEB REGION 5                 SDR 2005/11/28
//*  ADDED WEB REGION 4                 EJT 2005/05/24
//*  ADDED IARTMG SYMBOLIC              AWG 2005/03/01
//*  UPDATED FOR CICSROUTER/WEB REGIONS AWG 2004/11/09
//*  ADDED TPARATE SYMBOLIC             AWG 2004/11/04
//*  CHANGED TXTLIB TO POINT TO OCRUNCLC LIBRARY
//*                                     AWG 2004/10/19
//*-------------------------------------------------------------------
// SET ENVIR='EDIT'               *** SYSTEM ENVIRONMENT MEMBER
// SET BASEENV='ENVEDT#'
// SET TYP='M'
// SET CICSRG01='A52COLF1'
// SET CICSRG02='A52COLF2'
// SET CICSRGW1='A52CPL4'
// SET CICSRGW2='A52CPL5'
// SET CICSRGW3='A52CPL6'
// SET CICSRGW4='ACPLW4'
// SET CICSRGW5='A52CPLW5'
// SET VFILPRE='POMPL.ONL.F'
// SET CTLLIBL='PROD.OMNIPLUS.CTLLIBL'
// SET CTLLIBC='PROD.OMNIPLUS.CTLLIBC'
// SET CTLLIBB='PROD.OMNIPLUS.CTLLIBB'
// SET TXTLIB='PROD.OMNIPLUS.OCRUNCLC'
// SET OCLIB='PROD.OMNIPLUS.OCRUNCLC'
// SET TCJOBS='PROD.OMNIPLUS.JOBS'
// SET FPREFIX='POMPL.F'
// SET VSECPRE='POMPL.ONL'
// SET VPRESYSM='POMPL.ONL'
// SET BKUPPRE='PPDT.F'
// SET DISKPRE='PPDD.F'
// SET OMNIPAY='POMPY'
// SET STN='POMPL'
// SET DATACLS='DCPSEXTC'
// SET WBLK='27998'
// SET VBLK='27998'
// SET RETPD='30'
// SET SYSROUT='*'
// SET COPYLBL='1'
// SET COPYPF='1'
// SET PRMS='S'
// SET PRMSTEP='  '                  *** PROCESSING STEP
// SET PRMEDIT='    '                *** GLOBAL UPDATE CONTROL
// SET PRMRDAT='        '            *** RUN DATE OVERRIDE YYYYMMDD
// SET PRMRTIM='    '                *** RUN TIME OVERRIDE HHMM
// SET PRMSEQ=' '                    *** SEQUENTIAL HISTORY UPDATE CNTL
// SET PRMRPT=' '                    *** PLAN1 MESSAGE SUPPRESSION
// SET PRMMSG=' '                    *** PASS MESSAGE SUPRESSION
// SET PRMHDG='          '           *** PASS REPORT HEADING UDF
// SET PRMFDGT=' '                   *** PROCESSING MULTI FILE DIGIT
// SET PRMPGRP='  '                  *** PROCESSING GROUP 01 - 20
//*********************************************************************
//******  FOLLOWING SYMBOLICS ARE FOR TIAA-CREF FILES               ***
//*********************************************************************
// SET  SYS105='PINT.ONL.ANN'          ** ANNU TIAA-CREF FACTORS
// SET  DARFACT='PDA.ONL.ANN'          ** ANNU DAR FACTORS
// SET  FNDINFO='PDA.ONL.ANN'          ** ANNU EXTERNAL FUND INFO
// SET  FNDRATE='PDA.ONL.ANN'          ** ANNU EXTERNAL RATES
// SET  TPARATE='PINT.ONL.ANN'         ** ANNU TPA RATES
// SET  IARTMG='PIA.ONL.ANN'           ** IPRO RATE MANAGER
// SET  DPIFACT='PDA.ONL.ANN'          ** DELAYED PAYMENT INTEREST
// SET  HLQ1ACIS='PNA.ANN.ACIS'        ** ACIS HIGH LEVEL QUALIFIER 1
// SET  UTA490='PINT.ONL.ANN'          ** MDO FILE
// SET  SVINFO='PDA.ONL.ANN'           ** SVSAA PLAN INFO VSAM FILE
//*--------------------------------------------------------------------
//* SRTSPCPR & SRTSPCSE UPDATED 09/22/04 AS AN EMERGENCY SEE SC#15140
//* THE FOLLOWING VALUES DEFINE THE SPACE ALLOCATION PARAMETERS
//* AND NUMBER OF DASD VOLUMES
//* FOR THE LONG-TERM GENERATION DATASETS FOR DEFERRED WORK,
//* REPORT STRINGS, SEQUENTIAL HISTORY, AND PAYMENT INTERFACE
//* FILES
//*--------------------------------------------------------------------
// SET SVDSPCUN='TRK'                *** SPACE ALLOCATION UNITS
// SET SVDSPCPR='5000'               *** PRIMARY ALLOCATION
// SET SVDSPCSE='2000'               *** SECONDARY ALLOCATION
// SET LRGSPCPR='1500'              *** LARGE FILE PRIMARY ALLOCATION
// SET LRGSPCPE='1500'              *** LARGE FILE SECONDARY ALLOCATION
// SET TMPSPCUN='CYL'                *** TEMPORARY SPACE ALLOC UNITS
// SET TMPSPCPR='100'               *** TEMPORARY PRIMARY ALLOCATION
// SET TMPSPCSE='100'               *** TEMPORARY SECONDARY ALLOCATION
// SET SRTSPCPR='2000'               *** SORTWORK PRIMARY ALLOCATION
// SET SRTSPCSE='500'                *** SORTWORK SECONDARY ALLOCATION
//*--------------------------------------------------------------------
//*  FOLLOWING ADDED TO FACILITATE ELIMINATION OF "SEGMENTED"
//*  GETACTIVE CARDS
//*--------------------------------------------------------------------
// SET GLOBL=''
// SET TCSTRM='26'
// SET JOB='PCK6300D'
// SET HLQ1='POMPL.F'
// SET FSET='A'
// SET MOBIUS='0,CK6300D1'
// SET SVDSPCPR='5000'
// SET SVDSPCSE='1000'
// SET TMPSPCPR='100'
// SET TMPSPCSE='25'
//*---------------------------------------------------------------------
//*  ALLOCATE TEMPORARY FILES
//*---------------------------------------------------------------------
//ALLOCATE EXEC PROC=ALLOCATE
//*
//*---------------------------------------------------------------------
//*  RUN CPS EXTRACT OMNISCRIPT
//*---------------------------------------------------------------------
//PCK6300D EXEC PROC=PCK0001D
//*
