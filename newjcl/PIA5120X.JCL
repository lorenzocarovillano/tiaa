//PIA5120X JOB (P,19999,000,LEGB),IA-MQFTE,MSGCLASS=I,CLASS=1      
//*MSYS     REGION=0M                                                        
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB                              
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*                                                                     
//*  Copy Survivor Central files to SC server. This is for the RSA      
//*  quarterly processing.                                              
//*                                                                     
//S1 EXEC MQFTEBPX,                                                     
//   USRPARMS='PROD.WMQFTE.PARMLIB(IA5120M1)'                           
//*                                                                     
//S2 EXEC MQFTEBPX,                                                     
//   USRPARMS='PROD.WMQFTE.PARMLIB(IA5120M2)'                           
//*                                                                     
//S3 EXEC MQFTEBPX,                                                     
//   USRPARMS='PROD.WMQFTE.PARMLIB(IA5120M3)'                           
//                                                                      
