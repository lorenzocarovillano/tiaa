//PIA2250X JOB (C,66810,000,OPSB),IA,MSGCLASS=I,
//             CLASS=6,REGION=0M,TIME=1440
//*
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*
//* IA - JOB WILL PERFORM NDM STEP TO SEND STATUS FILE UPDATE
//*      TO CTH FOR ITD ACCELERATED ANNUITIZATION
//*********************************************************************
//*        DEALLOCATE TRIGGER FILE
//*********************************************************************
//DELT001  EXEC PGM=IEFBR14
//SYSPRINT DD SYSOUT=*
//TMPFL01  DD DSN=PPDD.ANN.IACTH.TRG,
//            UNIT=SYSDA,
//            DISP=(MOD,DELETE,DELETE),
//            SPACE=(CYL,(0,0))
//*********************************************************************
//*        ALLOCATE WORK DATASET - FOR DUMMY TRG FILE
//*********************************************************************
//ALLOCAT2 EXEC PGM=IEFBR14,COND=(4,LE)
//SYSPRINT DD SYSOUT=*
//D1       DD DSN=PPDD.ANN.IACTH.TRG,
//            UNIT=SYSDA,
//            DISP=(NEW,CATLG,DELETE),
//            SPACE=(TRK,(1,1),RLSE),
//            DCB=(DSORG=PS,RECFM=FB,LRECL=80)
//SYSIN    DD DUMMY
//*
//*********************************************************************
//*
//STEP005 EXEC NDMPROC
//SYSIN     DD *
 SIGNON
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(IA2250X1)      -
 NEWNAME=AV2250D1                                  -
 &INFILE=PIA.ANN.IA.STATUS.UPDT(0)                 -
 &OUTPATH=/pd1/resrc_disk1/DS_ANNUITY/source       -
 &OUTFILE=/IA_ANNUITY_SETUP_STATUS.txt             -
 CASE=YES MAXDELAY=UNLIMITED
 SIGNOFF
//*
//*********************************************************************
//*
//STEP010 EXEC NDMPROC
//SYSIN     DD *
 SIGNON
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(IA2250X1)      -
 NEWNAME=AV2250D2                                  -
 &INFILE=PPDD.ANN.IACTH.TRG                        -
 &OUTPATH=/pd1/resrc_disk1/DS_ANNUITY/source       -
 &OUTFILE=/IA_ANNUITY_SETUP_STATUS.trg             -
 CASE=YES MAXDELAY=UNLIMITED
 SIGNOFF
//*
//*
