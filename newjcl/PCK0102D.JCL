//PCK0102D JOB (P,67253,000,OPSS),'OMNIPAY',CLASS=1,                    
//           MSGCLASS=I,REGION=0M                                       
//*                                                                     
//LIBSRCH  JCLLIB ORDER=(PROD.OAS.NY.PARMLIB)                           
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*MSYS // INCLUDE MEMBER=JOBLIBU                        
//JOBLIB   DD  DISP=SHR,DSN=PDBFPP.OMNIPAY.TIAA.BAT.LOADLIB             00011001
//         DD  DISP=SHR,DSN=PDBFPP.OMNIPAY.CURRENT.BAT.LOADLIB          00012001
//         DD  DISP=SHR,DSN=PDBFPP.OMNIPAY.TIAA.ONLINE.LOADLIB          00013001
//         DD  DISP=SHR,DSN=PDBFPP.OMNIPAY.CURRENT.ONLINE.LOADLIB
//         DD  DISP=SHR,DSN=PROD.BATCH.LOADLIB
//*                                                                     
//* --------------------------------------------------------------*     
//PCK0102D EXEC PCK0002D,                                               
//         DMPS='U',                                                    
//         JCLO='*',                                                    
//         HLQ0='0',                                                    
//         HLQ1='POMPL.F',                                              
//         HLQ2='PAA.ANN.OMNIPLUS',                                     
//         REGN1='5M',                                                  
//         FSET='A',                                                    
//         JOB='PCK0101D',                                              
//         JOB2='PCK0102D'                                              
