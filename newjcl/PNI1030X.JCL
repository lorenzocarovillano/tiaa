//PNI1030X JOB (P,66205,000,OPSB),'IRA-SUB PRNT WELCOME',               
//         MSGCLASS=I,CLASS=6                                           
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*
//NDMS010  EXEC PGM=DMBATCH,REGION=4M,PARM=(YYSLYNN),TIME=1440          
//DMNETMAP DD  DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR                  
//DMPUBLIB DD  DSN=PMVSPP.STERLING.PROCESS,DISP=SHR                     
//DMMSGFIL DD  DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR                     
//DMPRINT  DD  SYSOUT=*                                                 
//SYSIN    DD  *                                                        
  SIGNON ESF=YES                                                        
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(NI1030X1) -                        
  &HLQ0=PPM.ACIS.CIS                           -                        
  &GDG0=0                                      -                        
  MAXDELAY=UNLIMITED CASE=YES                                           
  SIGNOFF                                                               
/*
