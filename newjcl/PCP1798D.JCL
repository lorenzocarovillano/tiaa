//PCP1798D JOB (P,69999,000,FINS),'CPS/IS EOD RPTS',CLASS=1,            
//       MSGCLASS=I                                                     
//*                                                                     
//LIBSRCH  JCLLIB ORDER=(PROD.OAS.NY.PARMLIB)                           
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*--------------------------------------------------------------------*
//*                                                                    *
//*      O P E N   I N V E S T M E N T S   A R C H I T E C T U R E     *
//*                                                                    *
//*                           *** PCP1798D ***                         *
//*                                                                    *
//*      CPS - DAILY EXTRACT OF PAYMENTS USING PROCESS DATE             
//*                                                                    *
//*--------------------------------------------------------------------*
//*                                                                     
//JOBLIB   DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR                
//         DD DSN=PROD.BATCH.LOADLIB,DISP=SHR                           
//*                                                                     
//PCP1775D EXEC PCP1775D,                                               
//         CNTL='PCP17981',         DATE PARAMETER, FORMAT CCYYMMDD     
//         JOBNM='PCP1798D',        2ND HLQ FOR FILES CREATED IN PROC   
//         RPTID='CP1798D',         MOBIUS REPORT ID'S                  
//         PYMNT1='PAYMENT'         INPUT PAYMENT FILE                  
//*                                                                     
//PCP1731D EXEC PCP1731D,                                               
//         RUNPARM='DSN=PPDD.PCP1798D.RUNPARM',                         
//         JOBNM='PCP1798D',        2ND HLQ FOR FILES CREATED IN PROC   
//         JOBNM1='PCP1798D',       2ND HLQ FOR FILES FROM PRIOR PROCS  
//         JOBNM2='PCP1700D',       2ND HLQ FOR FILES FROM PRIOR PROCS  
//         JOBNM3='PCP1798D',       2ND HLQ FOR FILES FROM PRIOR PROCS  
//         RPTID='CP1798D',         MOBIUS REPORT ID'S                  
//         EXT1='EXT1',             TYPE OF EXTRACT                     
//         EXT2='EXT2',             TYPE OF EXTRACT                     
//         EXT3='EXT3',             TYPE OF EXTRACT                     
//         EXT4='EXT4',             TYPE OF EXTRACT                     
//         EXT5='EXT5',             TYPE OF EXTRACT                     
//         EXT7='EXT7',             TYPE OF EXTRACT 7                   
//         HDR1='HDR.ACCT',         INPUT HDR FILE                      
//         HDR2='HDR.OTHER',        INPUT HDR FILE                      
//         HDR3='HDR.ORGNACCT',     INPUT HDR FILE                      
//         HDR4='HDR.ORIGIN',       INPUT HDR FILE                      
//         HDR5='HDR.OOTHER',       ROLLOVER ORIGIN HEADERS             
//         PYMNT1=PAYMENT,          INPUT PAYMENT FILE                  
//         PYMNT2=PAYMENT,          INPUT PAYMENT FILE                  
//         PROG1='PCP1798N',        HEADER CREATION PGM                 
//         SORT1='PCP1798O',        REPORT SORT PARM                    
//         SORT2='PCP1798M',        REPORT SORT PARM                    
//         SORT3='PCP1798P',        REPORT SORT PARM                    
//         SORT4='PCP1798M'         REPORT SORT PARM, NO CSR            
//*                                                                     
//*                                                                     
//PCP1798D EXEC PCP1798D,                                               
//         JOBNM='PCP1798D',        2ND HLQ FOR FILES CREATED IN PROC   
//         JOBNM2='PCP1700D',       2ND HLQ FOR FILES FROM PRIOR PROCS  
//         PYMNT1='PAYMENT',        INPUT PAYMENT FILE                  
//         HDR4='HDR.ORIGIN',       ORIGIN HEADER FROM PCP1700D         
//         RPTID='CP1798D'          MOBIUS REPORT ID'S                  
