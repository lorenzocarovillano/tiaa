//PIA2201X JOB (P,66810,000,LEGB),                                      
//             IA,                                                      
//             TIME=1440,                                               
//             MSGCLASS=I,                                              
//             CLASS=6                                                  
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB                              
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//NDM0010  EXEC PGM=DMBATCH,REGION=4M,                                  
//        PARM=(YYSLYNN),                                               
//        COND=(0,NE)                                                   
//DMPRINT  DD  SYSOUT=*                                                 
//DMNETMAP DD  DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR                  
//DMPUBLIB DD  DSN=PROD.NDM.NY.PROCESS.LIB,DISP=SHR                     
//DMMSGFIL DD  DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR                     
//NDMCMDS  DD  SYSOUT=*                                                 
//SYSUDUMP DD SYSOUT=U                                                  
//SYSIN    DD *                                                         
  SIGNON ESF=YES                                                        
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(IA2201X) MAXDELAY=UNLIMITED   -    
         CASE=YES                                                       
  SIGNOFF                                                               
//*                                                                     
//                                                                      
