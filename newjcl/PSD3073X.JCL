//PSD3073X JOB (P,68412,000,LEGB),NDM,MSGCLASS=I,CLASS=6,REGION=8M,     
//             TIME=1440                                                
//NDMS1   EXEC NDMPROC
//SYSIN    DD  *                                        
 SIGNON
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(DADA3073)     -
 &INFILE=PPDT.CREF.IA.PROXY(0)                    -
 &OUTPATH=/pd1/resrc_disk1/DS_RMW_CONV/source/IAM -
 &OUTFILE=/ia_proxy.sf                            -
 CASE=YES MAXDELAY=UNLIMITED
 SIGNOFF
//NDMS2   EXEC NDMPROC,COND=(0,NE)
//SYSIN    DD  *                                        
 SIGNON
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(DADA3073)     -
 &INFILE=PPDT.TIAA.IA.RESERVE(0)                  -
 &OUTPATH=/pd1/resrc_disk1/DS_RMW_CONV/source/IAM -
 &OUTFILE=/ia_reserve.sf                          -
 CASE=YES MAXDELAY=UNLIMITED
 SIGNOFF
