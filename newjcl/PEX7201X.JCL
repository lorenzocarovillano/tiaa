//PEX7201X JOB (P,13870,000,OPSB),'EXT',MSGCLASS=I,CLASS=6,TIME=1440
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//* --------------------------------------------------------------------
//*
//* Transmit DATASTAGE Files
//*
//* --------------------------------------------------------------------
//NDMS010  EXEC PGM=DMBATCH,REGION=1024K,PARM=(YYSLYNN),COND=(0,NE)
//DMNETMAP  DD  DISP=SHR,DSN=PMVSSF.STERLING.NDM.NETMAP
//DMPUBLIB  DD  DISP=SHR,DSN=PROD.NDM.NY.PROCESS.LIB
//DMMSGFIL  DD  DISP=SHR,DSN=PMVSSF.STERLING.NDM.MSG
//NDMCMDS   DD  SYSOUT=*
//DMPRINT   DD  SYSOUT=*
//SYSUDUMP  DD  SYSOUT=U
//SYSIN     DD  *
  SIGNON ESF=YES
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(EX7201X1) MAXDELAY=UNLIMITED -
  CASE=YES
  SIGNOFF
/*
