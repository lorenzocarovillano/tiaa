//PCP1899D JOB (P,69999,000,FINS),'CPS/IS EOD',CLASS=1,                 
//         MSGCLASS=I                                MOBIUS > 60 DAYS   
//*                                                                     
//LIBSRCH  JCLLIB ORDER=(PROD.OAS.NY.PARMLIB)                           
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*--------------------------------------------------------------------*
//*                                                                    *
//*      O P E N   I N V E S T M E N T S   A R C H I T E C T U R E     *
//*                                                                    *
//*                           *** PCP1899D ***                         *
//*                                                                    *
//*                    CPS - END-OF-DAY PROCESSING.                    *
//*                                                                    *
//*--------------------------------------------------------------------*
//JOBLIB   DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR                
//         DD DSN=PROD.BATCH.LOADLIB,DISP=SHR                           
//*                                                                     
//PCP1899D EXEC PCP1899D                                                
//                                                                      
