//PRD127FX JOB (P,66800,000,LEGB),'RIDER TNG60',                        
//         CLASS=2,
//         MSGCLASS=I,
//         MSGLEVEL=(1,1)
//*
//* RDR2016F - ATRA RIDER (PAPER AND EMAIL)
//*
//*---------------------------------------------------------------------
//* DATE       NAME            CHG#      DESCRIPTION
//*---------------------------------------------------------------------
//* 08/28/2016 J. AVE          CHG393292 CHANGE CLASS=2 PER D. BUDALL
//*---------------------------------------------------------------------
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*
//*********************************************************************
//MQFTE001 EXEC PCC2652D,
//       PACKAGE=RDR2016F
//*
//*********************************************************************
