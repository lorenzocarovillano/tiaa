//PNI2104D JOB (P,14000,000,OPSS),'PNI',
//         CLASS=1,MSGCLASS=I
//*
//**********************************************************************
//* PROGRAM  : APPB151 - DEFAULT ENROLLMENT PROCESSING             (CNR)
//* FUNCTION : READS ACIS PRAP FILE AND EXTRACTS DEFAULT ENROLLMENT
//*            CONTRACTS RECORDS. THIS EXTRACTED FILE WILL SENT TO MDM
//*            FOR DEFAULT ENROLLMENT IND THAT WILL BE USED IN APPB150
//*            TO UPDATE THE DEFAULT ENROLLMENT ID IN THE PRAP FILE.
//**********************************************************************
//*
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*
//JOBLIB  DD DSN=PROD.BATCH.LOADLIB,DISP=SHR
//        DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//*
//PNI2104D EXEC PNI2104D
//                                                                      
