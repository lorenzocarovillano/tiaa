//PCI2120D JOB (P,15000,000,OPSS),NEWISSU,CLASS=1,MSGCLASS=I            
//*MSYS #SCC,COND=(1,LT,PCI2120D.VERY030)
//*MSYS #SCC,COND=(0,NE,*-PCI2120D.VERY030)
//*                                                                     
//OUT1 OUTPUT CLASS=8                                                   
//OUT2 OUTPUT CLASS=A,DEST=TXFMML                                       
//*                                                                     
//LIBSRCH  JCLLIB ORDER=PROD.OAS.NY.PARMLIB                             
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*                                                                     
//JOBLIB DD DSN=PROD.BATCH.LOADLIB,DISP=SHR                             
//       DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR                  
//       DD DSN=CEE.SCEERUN.RTEREUS,DISP=SHR                            
//       DD DSN=PROD.CICS.USERPRD1.LOADLIB,DISP=SHR                     
//       DD DSN=PCICSPP.OSSD.LOADLIB,DISP=SHR                           
//*                                                                     
//PCI2120D EXEC PCI2120D                                                
/*                                                                      
//                                                                      
