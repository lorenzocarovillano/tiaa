//PBF9991X JOB (P,16000,000,LEGB),'BENE',                               
//          CLASS=6,                                                    
//          MSGCLASS=I                                                  
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB                              
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//NDM0010  EXEC PGM=DMBATCH,REGION=1024K,                               
//        PARM=(YYSLYNN),                                               
//        COND=(0,NE)                                                   
//DMPRINT  DD  SYSOUT=*                                                 
//DMNETMAP DD  DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR                  
//DMPUBLIB DD  DSN=PROD.NDM.NY.PROCESS.LIB,DISP=SHR                     
//DMMSGFIL DD  DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR                     
//NDMCMDS  DD  SYSOUT=*                                                 
//SYSUDUMP DD SYSOUT=U                                                  
//SYSIN    DD *                                                         
  SIGNON ESF=YES                                                        
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(DLNA9991) MAXDELAY=UNLIMITED       
  SIGNOFF                                                               
//*                                                                     
//                                                                      
