//PIA2330X JOB (P,19999,000,LEGB),IA-MQFTE,MSGCLASS=I,CLASS=1          
//*MSYS     REGION=0M                                                        
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB                              
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*                                                                     
//*  Copy IA Transfer and trigger files to RMW.                         
//*                                                                     
//S1 EXEC MQFTEBPX,                                                     
//   USRPARMS='PROD.WMQFTE.PARMLIB(IA2330M2)'                           
//*                                                                     
//S2 EXEC MQFTEBPX,                                                     
//   USRPARMS='PROD.WMQFTE.PARMLIB(IA2330M3)'                           
//                                                                      
