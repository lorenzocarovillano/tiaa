//POP0128X JOB (P,66231,000,OPSB),'OMNIPAY POS PAY JPM',                
//         REGION=1024K,CLASS=6,                                        
//         MSGCLASS=I,TIME=1440                                         
//*                                                                     
//****************************************************                  
//*                                                                     
//*   POP0128X -- PURPOSE:  JPMC OUTBOUND TRANSMISSION                  
//*               TYPE:     POSITIVE PAY (MID-DAY)                      
//*               FILE:     POMPY.PMT0.POSJPMMD(0)                      
//*                                                                     
//****************************************************                  
//NDMS010  EXEC PGM=DMBATCH,PARM=(NYSSNNN)                              
//DMNETMAP DD DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR                   
//DMMSGFIL DD DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR                      
//DMPUBLIB DD DSN=PROD.NDM.NY.PROCESS.LIB,DISP=SHR                      
//DMPRINT  DD SYSOUT=*                                                  
//NDMCMDS  DD SYSOUT=*                                                  
//SYSUDUMP DD SYSOUT=U                                                  
//SYSIN    DD *                                                         
  SIGNON                                                                
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(OP0110D1)        -                 
  MAXDELAY=UNLIMITED                                  -
  CASE=YES                                            -                 
  &LOCDSN=POMPY.PMT0.POSJPMMD(0)                      -                 
  &RMTNODE=TC_INT_PDCD                                -                 
  &SIDIR="/mailbox/OmniPay/JPMC/"                     -                 
  &DTYPE=ARP                                          -                 
  &DFORMAT=POSPAY                                     -                 
  &RMTFN="OP0128D1.TXT"                                                 
  SIGNOFF                                                               
//                                                                      
