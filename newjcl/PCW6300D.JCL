//PCW6300D JOB (P,62100,127,LEGS),'CWF-LEGACY EXTRACT',                 
//             MSGCLASS=I,                                              
//             CLASS=1                                                  
//**                                                                    
//LIBSRCH  JCLLIB ORDER=PROD.OAS.NY.PARMLIB                             
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//JOBLIB   DD  DSN=PROD.BATCH.LOADLIB,DISP=SHR                          
//         DD  DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR               
//**                                                                    
//PCW6300D EXEC PCW6300D                                                
//                                                                      
