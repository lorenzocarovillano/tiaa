//PCP1100X JOB (P,78000,000,FINB),'NDM',CLASS=6,MSGCLASS=I              
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB                              
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*                                                                     
//NDM010 EXEC NDMPROC                                                   
//SYSIN     DD *                                                        
 SIGNON                                                                 
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(CP1100D1) MAXDELAY=UNLIMITED -      
 CASE=YES                                                               
 SIGNOFF                                                                
//NDM020 EXEC NDMPROC                                                   
//SYSIN     DD *                                                        
 SIGNON                                                                 
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(CP1100D2) MAXDELAY=UNLIMITED -      
 CASE=YES                                                               
 SIGNOFF                                                                
