//P2500IAX JOB (P,61802,121,LEGB),IA,MSGCLASS=I,CLASS=1                 
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB                              
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//COPY020  EXEC PGM=IEBGENER                                            
//SYSUT1   DD  DSN=PPDT.PIA.P2500IAM.PRENET(0),                         
//             DISP=OLD,UNIT=CARTV                                      
//SYSUT2   DD  DSN=PPDTDR.PIA.P2500IAM.PRENET(+1),                      
//             DISP=(,CATLG,DELETE),                                    
//             UNIT=CARTVDN,                                            
//*//             DCB=MODLDSCB,RECFM=FB,LRECL=5669                         
//             DCB=(MODLDSCB,RECFM=FB,LRECL=5669)
//SYSIN    DD  DUMMY                                                    
//SYSPRINT DD  SYSOUT=*                                                 
//**********************************************************************
