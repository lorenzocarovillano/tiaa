//PCP1797D JOB (P,78000,000,FINS),'CPS EXTRACT',CLASS=1,                
//       MSGCLASS=I                                                     
//*                                                                     
//LIBSRCH  JCLLIB ORDER=(PROD.OAS.NY.PARMLIB)                           
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                PROD  
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*--------------------------------------------------------------------*
//*      CONSOLIDATED PAYMENT SYSTEMS (CPS)                            *
//*      * PCP1797D                                                    *
//*      * EXTRACT OFAC FROM FCP-CONS-PAYMENT AND FCP-CONS-ADDR        *
//*      * ADABAS 003/196 AND 003/197 AND 003/112 NEEDED               *
//*--------------------------------------------------------------------*
//*                                                                     
//JOBLIB   DD DSN=PROD.BATCH.LOADLIB,DISP=SHR                           
//         DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR                
//*                                                                     
//PCP1797D EXEC PCP1797D                                                
//*                                                                     
