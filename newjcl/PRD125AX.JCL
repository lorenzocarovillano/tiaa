//PRD125AX JOB  (P,66800,000,LEGB),'DCS - RDR2014A',
//         CLASS=6,
//         MSGCLASS=I,
//         MSGLEVEL=(1,1)
//* RDR2014A - STANDARD DELIVERY
//* CONNECT DIRECT IMAGE FILE WITH PI DATA TO IMAGE SERVER
//******************************************************************
//******************************************************************
//*
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*
//******************************************************************
//* EXECUTE PROC P1240PTX
//******************************************************************
//PRD125AX EXEC P1240PTX,COND=(0,NE),
//*
//******************************************************************
//*  CHANGE THIS PARAMETER BEFORE MOVING INTO PRODUCTION
//*  JOB NAME IS PREVIOUS JOB NAME!!!
//******************************************************************
//        JOBNM=PRD1251R,
//*
//******************************************************************
//*   TEST-SPECIFIC PARAMETERS; MUST BE CHANGED FOR PRODUCTION
//******************************************************************
//        HLQ1=PPT.COR.XML
