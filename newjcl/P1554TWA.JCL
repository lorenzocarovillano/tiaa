//P1554TWA JOB (P,14400,000,FINS),'TAX MMM 1099INT',MSGCLASS=I,         
//         CLASS=1,REGION=8M                                            
//*                                                                     
//*                                                                     
//JOBLIB     DD  DSN=PROD.BATCH.LOADLIB,DISP=SHR                        
//           DD  DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR             
//********************************************************************* 
//* NO ACTION BY CA-11 REQUIRED                                         
//********************************************************************* 
//RMS      EXEC U11RMS,TYPRUN='N'                                       
//********************************************************************* 
//* 1099-INT MOBIUS MASS MIGRATION OF NON-EMAIL TAX FORMS               
//*                                                                     
//* THIS JOB CREATES 1099-INT ECS FILES NUMBER 1 THRU 3                 
//********************************************************************* 
//STEP010  EXEC P1550TWA,                                               
//         FORMTYPE='F1099INT',                                         
//         FILENBR='1',                                                 
//         JOBNM='P1554TWA',                                            
//         PARMNM1='P1554TW1',                                          
//         ETID=''                                                      
//********************************************************************* 
//*                                                                     
//  IF RC < 3 THEN    * EXECUTE THIS STEP ONLY IF HIGHEST RC FROM *     
//*                   *  ANY PREVIOUS STEP WAS LESS THAN 3        *     
//*                                                                     
//STEP020  EXEC P1550TWA,COND=(4,LT),                                   
//         FORMTYPE='F1099INT',                                         
//         FILENBR='2',                                                 
//         JOBNM='P1554TWA',                                            
//         PARMNM1='P1554TW1',                                          
//         ETID=''                                                      
//*                                                                     
// ENDIF          * TERMINATE IF STMT HERE *                            
//*                                                                     
//********************************************************************* 
//*                                                                     
//  IF RC < 3 THEN    * EXECUTE THIS STEP ONLY IF HIGHEST RC FROM *     
//*                   *  ANY PREVIOUS STEP WAS LESS THAN 3        *     
//*                                                                     
//STEP030  EXEC P1550TWA,COND=(4,LT),                                   
//         FORMTYPE='F1099INT',                                         
//         FILENBR='3',                                                 
//         JOBNM='P1554TWA',                                            
//         PARMNM1='P1554TW1',                                          
//         ETID=''                                                      
//*                                                                     
// ENDIF          * TERMINATE IF STMT HERE *                            
//*                                                                     
