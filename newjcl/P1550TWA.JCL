//P1550TWA JOB (P,14400,000,FINS),'TAX E-DELIVRY',MSGCLASS=I,           
//         REGION=8M,CLASS=1                                            
//*                                                                     
//*                                                                     
//JOBLIB     DD  DSN=PROD.BATCH.LOADLIB,DISP=SHR                        
//           DD  DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR             
//********************************************************************* 
//* NO ACTION BY CA-11 REQUIRED                                         
//********************************************************************* 
//RMS      EXEC U11RMS,TYPRUN='N'                                       
//********************************************************************* 
//* 1099-R EMAIL FILE FOR MOBIUS MASS MIGRATION                         
//*                                                                     
//* THIS JOB CREATES 1099-R ECS FILES NUMBER 1 THROUGH 10               
//********************************************************************* 
//STEP010  EXEC P1550TWA,                                               
//         FORMTYPE='F1099R.E',                                         
//         FILENBR='1',                                                 
//         JOBNM='P1550TWA',                                            
//         PARMNM1='P1550TW1',                                          
//         ETID=',ETID=F1099RE'                                         
//********************************************************************* 
//*                                                                     
//  IF RC < 3 THEN    * EXECUTE THIS STEP ONLY IF HIGHEST RC FROM *     
//*                   *  ANY PREVIOUS STEP WAS LESS THAN 3        *     
//*                                                                     
//STEP020  EXEC P1550TWA,COND=(4,LT),                                   
//         FORMTYPE='F1099R.E',                                         
//         FILENBR='2',                                                 
//         JOBNM='P1550TWA',                                            
//         PARMNM1='P1550TW2',                                          
//         ETID=',ETID=F1099RE'                                         
//*                                                                     
// ENDIF          * TERMINATE IF STMT HERE *                            
//*                                                                     
//********************************************************************* 
//*                                                                     
//  IF RC < 3 THEN    * EXECUTE THIS STEP ONLY IF HIGHEST RC FROM *     
//*                   *  ANY PREVIOUS STEP WAS LESS THAN 3        *     
//*                                                                     
//STEP030  EXEC P1550TWA,COND=(4,LT),                                   
//         FORMTYPE='F1099R.E',                                         
//         FILENBR='3',                                                 
//         JOBNM='P1550TWA',                                            
//         PARMNM1='P1550TW2',                                          
//         ETID=',ETID=F1099RE'                                         
//*                                                                     
// ENDIF          * TERMINATE IF STMT HERE *                            
//*                                                                     
//********************************************************************* 
//*                                                                     
//  IF RC < 3 THEN    * EXECUTE THIS STEP ONLY IF HIGHEST RC FROM *     
//*                   *  ANY PREVIOUS STEP WAS LESS THAN 3        *     
//*                                                                     
//STEP040  EXEC P1550TWA,COND=(4,LT),                                   
//         FORMTYPE='F1099R.E',                                         
//         FILENBR='4',                                                 
//         JOBNM='P1550TWA',                                            
//         PARMNM1='P1550TW2',                                          
//         ETID=',ETID=F1099RE'                                         
//*                                                                     
// ENDIF          * TERMINATE IF STMT HERE *                            
//*                                                                     
//********************************************************************* 
//*                                                                     
//  IF RC < 3 THEN    * EXECUTE THIS STEP ONLY IF HIGHEST RC FROM *     
//*                   *  ANY PREVIOUS STEP WAS LESS THAN 3        *     
//*                                                                     
//STEP050  EXEC P1550TWA,COND=(4,LT),                                   
//         FORMTYPE='F1099R.E',                                         
//         FILENBR='5',                                                 
//         JOBNM='P1550TWA',                                            
//         PARMNM1='P1550TW2',                                          
//         ETID=',ETID=F1099RE'                                         
//*                                                                     
// ENDIF          * TERMINATE IF STMT HERE *                            
//*                                                                     
//********************************************************************* 
//*                                                                     
//  IF RC < 3 THEN    * EXECUTE THIS STEP ONLY IF HIGHEST RC FROM *     
//*                   *  ANY PREVIOUS STEP WAS LESS THAN 3        *     
//*                                                                     
//STEP060  EXEC P1550TWA,COND=(4,LT),                                   
//         FORMTYPE='F1099R.E',                                         
//         FILENBR='6',                                                 
//         JOBNM='P1550TWA',                                            
//         PARMNM1='P1550TW2',                                          
//         ETID=',ETID=F1099RE'                                         
//*                                                                     
// ENDIF          * TERMINATE IF STMT HERE *                            
//*                                                                     
//********************************************************************* 
//*                                                                     
//  IF RC < 3 THEN    * EXECUTE THIS STEP ONLY IF HIGHEST RC FROM *     
//*                   *  ANY PREVIOUS STEP WAS LESS THAN 3        *     
//*                                                                     
//STEP070  EXEC P1550TWA,COND=(4,LT),                                   
//         FORMTYPE='F1099R.E',                                         
//         FILENBR='7',                                                 
//         JOBNM='P1550TWA',                                            
//         PARMNM1='P1550TW2',                                          
//         ETID=',ETID=F1099RE'                                         
//*                                                                     
// ENDIF          * TERMINATE IF STMT HERE *                            
//*                                                                     
//********************************************************************* 
//*                                                                     
//  IF RC < 3 THEN    * EXECUTE THIS STEP ONLY IF HIGHEST RC FROM *     
//*                   *  ANY PREVIOUS STEP WAS LESS THAN 3        *     
//*                                                                     
//STEP080  EXEC P1550TWA,COND=(4,LT),                                   
//         FORMTYPE='F1099R.E',                                         
//         FILENBR='8',                                                 
//         JOBNM='P1550TWA',                                            
//         PARMNM1='P1550TW2',                                          
//         ETID=',ETID=F1099RE'                                         
//*                                                                     
// ENDIF          * TERMINATE IF STMT HERE *                            
//*                                                                     
//********************************************************************* 
//*                                                                     
//  IF RC < 3 THEN    * EXECUTE THIS STEP ONLY IF HIGHEST RC FROM *     
//*                   *  ANY PREVIOUS STEP WAS LESS THAN 3        *     
//*                                                                     
//STEP090  EXEC P1550TWA,COND=(4,LT),                                   
//         FORMTYPE='F1099R.E',                                         
//         FILENBR='9',                                                 
//         JOBNM='P1550TWA',                                            
//         PARMNM1='P1550TW2',                                          
//         ETID=',ETID=F1099RE'                                         
//*                                                                     
// ENDIF          * TERMINATE IF STMT HERE *                            
//*                                                                     
//********************************************************************* 
//*                                                                     
//  IF RC < 3 THEN    * EXECUTE THIS STEP ONLY IF HIGHEST RC FROM *     
//*                   *  ANY PREVIOUS STEP WAS LESS THAN 3        *     
//*                                                                     
//STEP100  EXEC P1550TWA,COND=(4,LT),                                   
//         FORMTYPE='F1099R.E',                                         
//         FILENBR='10',                                                
//         JOBNM='P1550TWA',                                            
//         PARMNM1='P1550TW2',                                          
//         ETID=',ETID=F1099RE'                                         
//*                                                                     
// ENDIF          * TERMINATE IF STMT HERE *                            
//*                                                                     
//********************************************************************* 
