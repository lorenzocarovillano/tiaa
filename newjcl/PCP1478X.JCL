//PCP1478X JOB (C,66600,000,FINB),COAEXTRT,MSGCLASS=I,CLASS=6,          
//          REGION=8M                                                   
//LIBSRCH  JCLLIB ORDER=PROD.OAS.NY.PARMLIB                             
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*****************************************************************     
//* CP1478D1 WILL SEND ISA ACCOUNT EXTARCT TO THE MAINFRAME             
//*****************************************************************     
//*                                                                     
//NDMS010 EXEC PGM=DMBATCH,REGION=4M,PARM=(YYSLYNN)                     
//DMNETMAP DD DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR                   
//DMPUBLIB DD DSN=PMVSPP.STERLING.PROCESS.LIB,DISP=SHR                  
//DMMSGFIL DD DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR                      
//DMPRINT   DD SYSOUT=*                                                 
//NDMCMDS   DD SYSOUT=*                                                 
//SYSUDUMP  DD SYSOUT=U                                                 
//SYSIN     DD *                                                        
  SIGNON ESF=YES                                                        
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(CP1478D1) CASE=YES -               
  MAXDELAY=UNLIMITED                                                    
  SIGNOFF                                                               
//*                                                                     
//                                                                      
