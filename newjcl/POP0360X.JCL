//POP0360X JOB (P,78000,000,FINB),'NDM',CLASS=6,MSGCLASS=I
//* ------------------------------------------------------------------*
//LIBSRCH  JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//* ------------------------------------------------------------------*
//*  BROKERAGE REPORT                                                 *
//* ------------------------------------------------------------------*
//*
//********************************************************************
//*      SENDS FEED TO NDM SERVER 1
//********************************************************************
//STEP010 EXEC NDMPROC
//SYSIN  DD  *
 SIGNON
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(OP0360X1)                        -
 MAXDELAY=UNLIMITED CASE=YES                                         -
 &INFIL=POMPY.PMT0.BROKER.PAYMENTS(0)                                -
 &DIR=/pd1/resrc_disk1/DS_BK_FUNDS/source                            -
 &OUTFIL=/CTO_Omni_Deposit_File.txt
 SIGNOFF
/*
//********************************************************************
//*      SENDS FEED TO NDM SERVER 2
//********************************************************************
//STEP020 EXEC NDMPROC,COND=(0,NE)
//SYSIN  DD  *
 SIGNON
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(OP0360X2)                        -
 MAXDELAY=UNLIMITED CASE=YES
 SIGNOFF
/*
//*
//*********************************************************************
//*        SENDS TRIGGER FILE TO MDM
//*********************************************************************
//STEP030 EXEC NDMPROC,COND=(0,NE)
//SYSIN     DD *
 SIGNON
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(OP0360X3)        -
 CASE=YES                                            -
 MAXDELAY=UNLIMITED
 SIGNOFF
//*
