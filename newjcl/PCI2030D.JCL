//PCI2030D JOB (P,67253,000,OPSS),'DIALOG',CLASS=1,MSGCLASS=I           
//*                                                                     
//LIBSRCH  JCLLIB ORDER=PROD.OAS.NY.PARMLIB                             
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*                                                                     
//JOBLIB DD DSN=PROD.BATCH.LOADLIB,DISP=SHR                             
//       DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR                  
//       DD DSN=CEE.SCEERUN.RTEREUS,DISP=SHR                            
//       DD DSN=PROD.CICS.USERPRD1.LOADLIB,DISP=SHR                     
//       DD DSN=PCICSPP.OSSD.LOADLIB,DISP=SHR                           
//*                                                                     
//********************************************************************* 
//* CREATE NDM PARM - NEWISSUE MDO                                    * 
//********************************************************************* 
//CREA010 EXEC PCI2030D,                                                
//        HLQ1='PPM.ANN',                                               
//        JOBNM='PCI2030D',                                             
//        PKGTYP='NI.MDO',                                              
//        PARMMEM2='CI2030D2'                                           
//*                                                                     
//********************************************************************* 
//* CREATE NDM PARM - NEWISSUE OTHER (IA, IPRO, TPA)                  * 
//********************************************************************* 
//CREA020 EXEC PCI2030D,COND=(0,NE),                                    
//        HLQ1='PPM.ANN',                                               
//        JOBNM='PCI2030D',                                             
//        PKGTYP='NI.IA',                                               
//        PARMMEM2='CI2030D3'                                           
//*                                                                     
//                                                                      
