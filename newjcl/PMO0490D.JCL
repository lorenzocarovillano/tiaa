//PMO0490D JOB (P,15000,000,OPSB),                                      
//             PREM,
//             CLASS=1,
//             MSGCLASS=I
//*                                                                     
//*MSYS #SCC,COND=(000,NE,*-PMO0490D.COPY030)
//*MSYS #SCC,COND=(000,NE,*-PMO0490D.COPY060)
//*MSYS #SCC,COND=(000,NE,*-PMO0490D.COPY090)
//*MSYS #SCC,COND=(004,LT,PMO0490D.COPY030)
//*MSYS #SCC,COND=(004,LT,PMO0490D.COPY060)
//*MSYS #SCC,COND=(004,LT,PMO0490D.COPY090)
//*
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*
//JOBLIB DD DSN=PROD.BATCH.LOADLIB,DISP=SHR                             
//       DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR                  
//*                                                                     
//PMO0490D EXEC PMO0490D                                                
//
