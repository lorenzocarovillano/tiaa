//P1020BFD JOB (P,12355,000,LEGS),BFD,MSGCLASS=I,CLASS=2                
//*                                                                     
//LIBSRCH  JCLLIB ORDER=PROD.OAS.NY.PARMLIB                             
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//JOBLIB   DD DSN=PROD.BATCH.LOADLIB,DISP=SHR                           
//         DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR                
//*                                                                     
//P1020BFD EXEC P1020BFD                                                
