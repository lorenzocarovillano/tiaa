//PCP1053X JOB (P,78000,000,FINB),'NDM',CLASS=6,MSGCLASS=I              
//* ------------------------------------------------------------------* 
//LIBSRCH  JCLLIB ORDER=PROD.OAS.NY.PARMLIB                             
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//* ------------------------------------------------------------------* 
//*  ODS / CONSOLIDATED PAYMENT SYSTEM EXTRACT / On Request           * 
//*  for the given date range. Files from PCP1050R                    * 
//* ------------------------------------------------------------------* 
//NDM010 EXEC NDMPROC                                                   
//SYSIN     DD *                                                        
 SIGNON                                                                 
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(CP1050D1) MAXDELAY=UNLIMITED -      
 CASE=YES -                                                             
 &DSNI=PCPS.ANN.PCP1050R.ODS.PAYMENT(0)       -                         
 &OUTFIL=/pd1/resrc_disk1/ods/source/cps.payment.record.INS.extract.    
 SIGNOFF                                                                
//* ------------------                                                  
//NDM020 EXEC NDMPROC                                                   
//SYSIN     DD *                                                        
 SIGNON                                                                 
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(CP1050D1) MAXDELAY=UNLIMITED -      
 CASE=YES -                                                             
 &DSNI=PCPS.ANN.PCP1050R.ODS.FUNDS(0)    -                              
 &OUTFIL=/pd1/resrc_disk1/ods/source/cps.payment.record.dtl.extract.    
 SIGNOFF                                                                
//* ------------------                                                  
//NDM030 EXEC NDMPROC                                                   
//SYSIN     DD *                                                        
 SIGNON                                                                 
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(CP1050D1) MAXDELAY=UNLIMITED -      
 CASE=YES -                                                             
 &DSNI=PCPS.ANN.PCP1050R.DETAIL.CNTL      -                             
 &OUTFIL=/pd1/resrc_disk1/ods/source/cps.payment.record.dtl.control.    
 SIGNOFF                                                                
//* ------------------                                                  
//NDM040 EXEC NDMPROC                                                   
//SYSIN     DD *                                                        
 SIGNON                                                                 
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(CP1050D1) MAXDELAY=UNLIMITED -      
 CASE=YES -                                                             
 &DSNI=PCPS.ANN.PCP1050R.SUMMARY.CNTL       -                           
 &OUTFIL=/pd1/resrc_disk1/ods/source/cps.payment.record.INS.control.    
 SIGNOFF                                                                
//* ------------------                                                  
//NDM050 EXEC NDMPROC                                                   
//SYSIN     DD *                                                        
 SIGNON                                                                 
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(CP1050D1) MAXDELAY=UNLIMITED -      
 CASE=YES -                                                             
 &DSNI=PCPS.ANN.PCP1050R.USUMMARY.CNTL      -                           
 &OUTFIL=/pd1/resrc_disk1/ods/source/cps.payment.record.UPD.control.    
 SIGNOFF                                                                
//* ------------------                                                  
//NDM060 EXEC NDMPROC                                                   
//SYSIN     DD *                                                        
 SIGNON                                                                 
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(CP1050D1) MAXDELAY=UNLIMITED -      
 CASE=YES -                                                             
 &DSNI=PCPS.ANN.PCP1050R.UODS.PAYMENT(0)    -                           
 &OUTFIL=/pd1/resrc_disk1/ods/source/cps.payment.record.UPD.extract.    
 SIGNOFF                                                                
//* ------------------                                                  
//NDM070 EXEC NDMPROC                                                   
//SYSIN     DD *                                                        
 SIGNON                                                                 
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(CP1050D1) MAXDELAY=UNLIMITED -      
 CASE=YES -                                                             
 &DSNI=PCPS.ANN.PCP1050R.ODS.ANNOT(0)    -                              
 &OUTFIL=/pd1/resrc_disk1/ods/source/cps.payment.annot.INS.extract.     
 SIGNOFF                                                                
//* ------------------                                                  
//NDM080 EXEC NDMPROC                                                   
//SYSIN     DD *                                                        
 SIGNON                                                                 
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(CP1050D1) MAXDELAY=UNLIMITED -      
 CASE=YES -                                                             
 &DSNI=PCPS.ANN.PCP1050R.UODS.ANNOT(0)    -                             
 &OUTFIL=/pd1/resrc_disk1/ods/source/cps.payment.annot.UPD.extract.     
 SIGNOFF                                                                
//* ------------------                                                  
//NDM090 EXEC NDMPROC                                                   
//SYSIN     DD *                                                        
 SIGNON                                                                 
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(CP1050D1) MAXDELAY=UNLIMITED -      
 CASE=YES -                                                             
 &DSNI=PCPS.ANN.PCP1050R.ODS.ANNOT2(0)    -                             
 &OUTFIL=/pd1/resrc_disk1/ods/source/cps.payment.annot2.INS.extract.    
 SIGNOFF                                                                
//* ------------------                                                  
//NDM100 EXEC NDMPROC                                                   
//SYSIN     DD *                                                        
 SIGNON                                                                 
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(CP1050D1) MAXDELAY=UNLIMITED -      
 CASE=YES -                                                             
 &DSNI=PCPS.ANN.PCP1050R.UODS.ANNOT2(0)    -                            
 &OUTFIL=/pd1/resrc_disk1/ods/source/cps.payment.annot2.UPD.extract.    
 SIGNOFF                                                                
//* ------------------                                                  
//NDM110 EXEC NDMPROC                                                   
//SYSIN     DD *                                                        
 SIGNON                                                                 
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(CP1050D1) MAXDELAY=UNLIMITED -      
 CASE=YES -                                                             
 &DSNI=PCPS.ANN.PCP1050R.ODS.ADVDTL(0)    -                             
 &OUTFIL=/pd1/resrc_disk1/ods/source/cps.payment.record.advdtl.extract. 
 SIGNOFF                                                                
