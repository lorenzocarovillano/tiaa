#7UNI                                                                   
//PSD1500D   JOB (P,99999,000,OPSB),                                    
//         CA7,                                                         
//         CLASS=S,                                                     
//         MSGCLASS=I,                                                  
//         MSGLEVEL=(1,1)                                               
//* CA-AGENT SUBNISSION JCL                                             
//LIBSRCH  JCLLIB  ORDER=PROD.OAS.NY.PARMLIB                            
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*                                                                     
//CADRIV2  EXEC CA7TOUNI,                                               
//         PARM='_L27#,_L2SN'                                           
//SYSIN    DD  DISP=SHR,DSN=PMVSSF.CAI.SCHD.NY.XJCL.CA7(PSD1500D)       
//                                                                      
