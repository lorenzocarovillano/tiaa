//PRD1246R JOB (P,66955,000,LEGS),'RIDER DEFAULTB',
//         MSGCLASS=I,
//         CLASS=1
//*
//* RDR2013A - DEFAULT BENEFICIARY - PAPER AND EMAIL
//*
//**********************************************************************
//* MODIFICATION LOG:
//*---------------------------------------------------------------------
//* DATE       NAME            CHG#      DESCRIPTION
//*---------------------------------------------------------------------
//* 08/27/2016 J. AVE          CHG393292 CHANGE PROC FROM PCC2001R TO
//*                                      PCC2004R
//*
//**********************************************************************
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//JOBLIB   DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//        DD DSN=PROD.BATCH.LOADLIB,DISP=SHR
//*
//**********************************************************************
//*
//PRD1246R EXEC PCC2004R,
//*       PARMMEM=RD1246R1,
//       PARMMEM2=RD1246R2,
//       PARMMEM3=RD1246R3,
//       PARMMEM6=RD1246R4,
//       PACKAGE=RDR2013A,
//       DATATYPE=DFLTBENE,
//       JOBNM=PRD1246R,
//       FILE=FILE1
//*
//EXTR010.CMPRT01 DD SYSOUT=(&REPT.,RD1246R1)
//EXTR020.CMPRT01 DD SYSOUT=(&REPT.,RD1246R2)
//EXTR020.CMPRT02 DD SYSOUT=(&REPT.,RD1246R3)
//*
