//PAN2150X JOB (P,66810,000,LEGB),CES,MSGCLASS=I,CLASS=6,REGION=8M      
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB                              
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*****************************************************************     
//* COPY PAA.ANN.PDW1140D.ADAS.EXPAND TO PDFINWSQL01CHA           *     
//*****************************************************************     
//NDMS010 EXEC PGM=DMBATCH,REGION=4M,PARM=(YYSLYNN)                     
//DMNETMAP DD DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR                   
//DMPUBLIB DD DSN=PMVSPP.STERLING.PROCESS.LIB,DISP=SHR                  
//DMMSGFIL DD DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR                      
//DMPRINT   DD SYSOUT=*,OUTPUT=*.OUTVDR                                 
//NDMCMDS   DD SYSOUT=*,OUTPUT=*.OUTVDR                                 
//SYSUDUMP  DD SYSOUT=U,OUTPUT=*.OUTLOCL                                
//SYSIN     DD *                                                        
  SIGNON ESF=YES                                                        
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(DW1140D1) MAXDELAY=UNLIMITED       
  SIGNOFF                                                               
