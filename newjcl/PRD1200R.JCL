//PRD1200R JOB (P,66955,000,LEGS),'RIDER DOMA',
//         MSGCLASS=I,
//         CLASS=1,
//         REGION=9M
//*
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//JOBLIB   DD  DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//*
//*****************************************************************
//* READ DA REPORTING EXTRACT & SPLIT BY NUMBER OF CONTRACTS
//*****************************************************************
//PRD1200R EXEC PRD1200R,
//      PARMMEM=RD1200R1,
//      DATA='DATA1'
//*****************************************************************
//* REPORTS
//*****************************************************************
//EXTR020.CMPRT01 DD SYSOUT=(&REPT.,RD1200R1)
//EXTR020.CMPRT02 DD SYSOUT=(&REPT.,RD1200R2)
//EXTR020.CMPRT03 DD SYSOUT=(&REPT.,RD1200R3)
