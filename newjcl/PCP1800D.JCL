//PCP1800D JOB (P,69999,000,FINS),'CPS/VT RECEIVE',CLASS=1,             
//       MSGCLASS=I                                     MOBIUS > 60DAYS 
//*                                                                     
//LIBSRCH  JCLLIB ORDER=(PROD.OAS.NY.PARMLIB)                           
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                PROD  
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*--------------------------------------------------------------------*
//*                                                                    *
//*      O P E N   I N V E S T M E N T S   A R C H I T E C T U R E     *
//*                                                                    *
//*                           *** PCP1800D ***                         *
//*                                                                    *
//*            CPS - PROLINE PAYMENTS RECEIVE                          *
//*--------------------------------------------------------------------*
//*                                                                     
//JOBLIB   DD DSN=PROD.BATCH.LOADLIB,DISP=SHR                           
//         DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR                
//*                                                                     
//PCP1800D EXEC PCP1800D                                                
//*                                 RUN PCP1701R FOR A RERUN            
