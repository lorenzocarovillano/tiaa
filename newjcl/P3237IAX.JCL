//P3237IAX JOB (P,69999,000,LEGB),'NDM COMBINED MASTER',
//             MSGCLASS=I,
//             CLASS=6,TIME=1440
//**
//LIBSRCH  JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//**
//**------------------------------------------------------------------*
//** JEM: 09/09 TRANSFER THE MONTHLY COMBINED MASTER FILE FROM PROD1  *
//** TO DNPR.                                                         *
//**                                                                  *
//**------------------------------------------------------------------*
//**
//NDM3237  EXEC PGM=DMBATCH,
//             PARM=(YYSLYNN),
//             REGION=4M
//DMMSGFIL DD  DSN=PMVSSF.STERLING.NDM.MSG,
//             DISP=SHR
//DMNETMAP DD  DSN=PMVSSF.STERLING.NDM.NETMAP,
//             DISP=SHR
//DMPUBLIB DD  DSN=PMVSPP.STERLING.PROCESS.LIB,
//             DISP=SHR
//DMPRINT  DD  SYSOUT=*,
//             OUTPUT=*.OUTVDR
//SYSIN    DD  *
  SIGNON ESF=YES
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(P3237IA1) MAXDELAY=UNLIMITED
