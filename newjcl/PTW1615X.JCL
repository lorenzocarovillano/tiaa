//PTW1615X JOB (P,14400,000,FINB),'TAXWARS',MSGCLASS=I,
//             CLASS=6,REGION=8M,TIME=1440
//* -------------------------------------------------------------------
//*
//* Taxwars sends a file to MDM and have the name and address info
//*          updated
//*
//* -------------------------------------------------------------------
//LIBSRCH  JCLLIB ORDER=PROD.OAS.NY.PARMLIB                             
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*********************************************************************
//*        DEALLOCATE TRIGGER FILE
//*********************************************************************
//DELT001  EXEC PGM=IEFBR14
//SYSPRINT DD SYSOUT=*
//TMPFL01  DD DSN=PPDD.PTW1615X.TRGFILE,
//            UNIT=SYSDA,
//            DISP=(MOD,DELETE,DELETE),
//            SPACE=(CYL,(0,0))
//*********************************************************************
//*        ALLOCATE WORK DATASET - FOR DUMMY TRG FILE
//*********************************************************************
//ALLOCAT2 EXEC PGM=IEFBR14,COND=(4,LE)
//SYSPRINT DD SYSOUT=*
//D1       DD DSN=PPDD.PTW1615X.TRGFILE,
//            UNIT=SYSDA,
//            DISP=(NEW,CATLG,DELETE),
//            SPACE=(TRK,(1,1),RLSE),
//            DCB=(DSORG=PS,RECFM=FB,LRECL=80)
//SYSIN    DD DUMMY
//*
//*********************************************************************
//*        SENDS EXTRACTED PARTICIPANT FILE TO MDM
//*********************************************************************
//STEP005 EXEC NDMPROC,COND=(0,NE)
//SYSIN     DD *
 SIGNON
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(TW1615X1)        -
 &INFILE=PNPD.COR.TAX.PTW1615A.ESPEXTR               -
 &OUTPATH=/pd1/resrc_disk1/DS_FSL/source/retirement  -
 &OUTFILE=/Taxwars_PTW1615A_Extract                  -
 CASE=YES                                            -
 MAXDELAY=UNLIMITED
 SIGNOFF
//*********************************************************************
//*        SENDS TRIGGER FILE TO MDM
//*********************************************************************
//STEP010 EXEC NDMPROC,COND=(0,NE)
//SYSIN     DD *
 SIGNON
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(TW1615X2)        -
 &INFILE=PPDD.PTW1615X.TRGFILE                       -
 &OUTPATH=/pd1/resrc_disk1/DS_FSL/source/retirement  -
 &OUTFILE=/Taxwars_PTW1615A                          -
 CASE=YES                                            -
 MAXDELAY=UNLIMITED
 SIGNOFF
//*
//*
