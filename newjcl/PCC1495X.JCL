//PCC1495X JOB (P,66800,000,OPSS),'CCP-PTX5498F',
//         CLASS=1,
//         MSGCLASS=I
//* PTX5498F - STANDARD DELIVERY
//* SEND XML FILE(S) TO CCP SERVER VIA MQFTE
//*
//LIBSRCH  JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*
//JOBLIB   DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//         DD DSN=PROD.BATCH.LOADLIB,DISP=SHR
//*
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//******************************************************************
//* EXECUTE PROC TO TRANSMIT XML FILE TO CCP
//*     FROM PACKAGE=P5498F01 TO PACKAGE=P5498F05
//******************************************************************
//*
//MQFTE001 EXEC PCC3001D,
//       JOBNM='P5498F01',
//       FN='1'
//*
//MQFTE002 EXEC PCC3001D,
//       JOBNM='P5498F02',
//       FN='2'
//*
//MQFTE003 EXEC PCC3001D,
//       JOBNM='P5498F03',
//       FN='3'
//*
//MQFTE004 EXEC PCC3001D,
//       JOBNM='P5498F04',
//       FN='4'
//*
//MQFTE005 EXEC PCC3001D,
//       JOBNM='P5498F05',
//       FN='5'
//*
//*
