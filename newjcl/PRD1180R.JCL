//PRD1180R JOB (P,66955,000,LEGB),'2009 IA ACCESS',
//         MSGCLASS=I,
//         CLASS=1,
//         REGION=9M
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//JOBLIB   DD  DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//*
//*****************************************************************
//* READ IA EXTRACT & SPLIT BY NUMBER OF CONTRACTS
//*****************************************************************
//PRD1180R EXEC PRD1180R,
//      PARMMEM=RD1180R1,
//      DATA='DATA1'
//*****************************************************************
//* REPORTS
//*****************************************************************
//EXTR020.CMPRT01 DD SYSOUT=(&REPT.,RD1080R1)
