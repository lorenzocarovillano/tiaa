//P1615CWD  JOB (P,66241,000,LEGS),'CWF-EXTRACT',MSGCLASS=I,CLASS=2
//*
//********************************************************************
//*  THIS JOB READS CWF-MASTER FILE AND EXTRACT WORK-REQUEST AND
//*  ACTIVITY RECORDS TO CREATE FILES FOR REPORTING PURPOSES.
//********************************************************************
//*
//*                                                                     
//*
//*                                                                     
//*
//JOBLIB   DD  DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//*
//P1615CWD EXEC P1615CWD
//
