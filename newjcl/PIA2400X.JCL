//PIA2400X JOB (P,66810,000,LEGB),IA,MSGCLASS=I                    
//*MSYS             CLASS=6,REGION=0M                                        
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB                              
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//**************************************************************        
//*  DAILY NDM TO SERVER                                       *        
//**************************************************************        
//NDMS010 EXEC PGM=DMBATCH,PARM=(YYSLYNN)                               
//DMNETMAP DD  DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR                  
//DMPUBLIB DD  DSN=PROD.NDM.NY.PROCESS.LIB,DISP=SHR                     
//DMMSGFIL DD  DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR                     
//DMPRINT   DD SYSOUT=*                                                 
//NDMCMDS   DD SYSOUT=*                                                 
//SYSUDUMP  DD SYSOUT=U                                                 
//SYSIN     DD *                                                        
  SIGNON ESF=YES                                                        
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(IA2400X1) MAXDELAY=UNLIMITED -     
         CASE=YES                                                       
  SIGNOFF                                                               
/*                                                                      
//*-------------------------------------------------------------        
//*   MQFTE step to new server chapda3tcpcs01.cloud.tiaa-cref.org       
//*-------------------------------------------------------------        
//S1 EXEC MQFTEBPX,                                                     
//   USRPARMS='PROD.WMQFTE.PARMLIB(IA2400X2)'                           
/*                                                                      
