//POP0999R JOB (C,19999,000,OPSB),'OMNIPAY',CLASS=1,                    
//         MSGCLASS=I,REGION=0M                                         
//*                                                                     
//LIBSRCH  JCLLIB ORDER=(PROD.OAS.NY.PARMLIB)                           
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*                                                                     
//*MSYS // INCLUDE MEMBER=JOBLIBU                        
//JOBLIB   DD  DISP=SHR,DSN=PDBFPP.OMNIPAY.TIAA.BAT.LOADLIB             00011001
//         DD  DISP=SHR,DSN=PDBFPP.OMNIPAY.CURRENT.BAT.LOADLIB          00012001
//         DD  DISP=SHR,DSN=PDBFPP.OMNIPAY.TIAA.ONLINE.LOADLIB          00013001
//         DD  DISP=SHR,DSN=PDBFPP.OMNIPAY.CURRENT.ONLINE.LOADLIB
//         DD  DISP=SHR,DSN=PROD.BATCH.LOADLIB
//*                                                                     
//*------------------------------------------------------------------*  
//*  DEFINE AND REPRO POMPY.PMT0.RFNDRSCD                               
//*------------------------------------------------------------------*  
//*                                                                     
//RFNDRSCD  EXEC RFNDRSCD,                                              
//  SPREFX='POMPY',                                                     
//  VFSET='PMT0',                                                       
//  PARMLIB='PROD.OMNIPAY.BAT.CTL',                                     
//  SYSOUT='*'                                                          
//*                                                                     
