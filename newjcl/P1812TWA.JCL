//P1812TWA JOB (P,14400,000,FINS),ORIG-STRPT,MSGCLASS=I,                
//       CLASS=1,REGION=8M                                              
//*                                                                     
//*                                                                     
//JOBLIB  DD DSN=PROD.BATCH.LOADLIB,DISP=SHR
//        DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//*
//P1812TWA EXEC P180DTWA,STATE='GA',STNUM=P1812TW1                      
//EXTR010.CMPRT01 DD SYSOUT=(&REPT,TW1812A1)
//EXTR010.CMPRT04 DD SYSOUT=(&REPT,TW1812A2)
//FRMT030.CMPRT01 DD SYSOUT=(&REPT,TW1812A3)
//FRMT030.CMPRT02 DD SYSOUT=(&REPT,TW1812A4)
//FRMT030.CMPRT03 DD SYSOUT=(&REPT,TW1812A6)
//FRMT030.CMPRT04 DD SYSOUT=(&REPT,TW1812A7)
//FRMT030.CMPRT05 DD SYSOUT=(&REPT,TW1812A8)
//*GEORGIA                                                              
//**                                                                    
//**----------------------------------------------------------------**
//** SEND NOTIFICATION INDICATING TAX VENDOR FILE IS READY          **
//**                                                                **
//**----------------------------------------------------------------**
//NOTFY999 EXEC CAJUCMNY,COND=(0,NE)
//SYSPRINT DD  SYSOUT=*
//SYSIN    DD  *
MO ATTN: P1812TWA IS COMPLETE AND VENDOR CARTRIDGE IS READY.
/*
//*
