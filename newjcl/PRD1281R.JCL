//PRD1281R JOB (P,13840,000,LEGB),'GSRA LOAN',
//         MSGCLASS=I,
//         CLASS=1,
//         REGION=9M
//***      ===========================================================
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//***      ===========================================================
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//***      ===========================================================
//JOBLIB    DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//          DD DSN=PROD.BATCH.LOADLIB,DISP=SHR
//**********************************************************************
//*** JOB DESC: SPLIT THE RIDER MAILING FILE INTO FIRST 15 PARTS ******
//**********************************************************************
//PRD1281R EXEC PROC=PRD1281R
//*
