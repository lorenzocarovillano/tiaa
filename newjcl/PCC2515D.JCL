//PCC2515D JOB (P,66800,000,OPSS),'CIS MDO NEWISSUE-DCS',
//         CLASS=1,
//         MSGCLASS=I
//*
//LIBSRCH  JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*
//JOBLIB   DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//         DD DSN=PROD.BATCH.LOADLIB,DISP=SHR
//**                                                                    
//*********************************************************************
//**      SPLIT INPUT FILE BY CIS MDO PROFILE ID                        
//*********************************************************************
//*
//DSNBUILD EXEC PCC2510D,
//       PARMMEM3=PC2515D1,
//       FILETYP='CIS.NEWISSUE.MDO',
//       SRCFILE='PPM.ANN.CIS.NEWISSUE.MDO.DIALOG.DATA'                 
//**                                                                    
//*********************************************************************
//**      EXECUTE PROC PCC2511D FOR EACH POSSIBLE CIS FILE              
//*********************************************************************
//BLDXML01 EXEC PCC2511D,
//       APPNM='CISMDO30',
//       SPC3='10,10',
//       PACKAGE='PTCISMDO',
//       FILETYP='CIS.NEWISSUE.MDO',
//       PROFNUM=PROFIL01
//**                                                                    
//*********************************************************************
//BLDXML02 EXEC PCC2511D,
//       APPNM='CISMDO31',
//       SPC3='10,10',
//       PACKAGE='PTCISMDO',
//       FILETYP='CIS.NEWISSUE.MDO',
//       PROFNUM=PROFIL02
//**                                                                    
//*********************************************************************
//BLDXML03 EXEC PCC2511D,
//       APPNM='CISMDO32',
//       SPC3='10,10',
//       PACKAGE='PTCISMDO',
//       FILETYP='CIS.NEWISSUE.MDO',
//       PROFNUM=PROFIL03
//**                                                                    
//*********************************************************************
//BLDXML04 EXEC PCC2511D,
//       APPNM='CISMDO33',
//       SPC3='10,10',
//       PACKAGE='PTCISMDO',
//       FILETYP='CIS.NEWISSUE.MDO',
//       PROFNUM=PROFIL04
//**                                                                    
//*********************************************************************
//BLDXML05 EXEC PCC2511D,
//       APPNM='CISMDO34',
//       SPC3='10,10',
//       PACKAGE='PTCISMDO',
//       FILETYP='CIS.NEWISSUE.MDO',
//       PROFNUM=PROFIL05
//*********************************************************************
//*   BUILD CONNECT:DIRECT COPY COMMANDS FOR ALL XML FILES
//*********************************************************************
//NDMBUILD EXEC PCC2512D,
//       JOBNM=PCC2515D,
//       PARMMEM1=PC2515D2,
//       FILETYP='CIS.NEWISSUE.MDO'
//*
