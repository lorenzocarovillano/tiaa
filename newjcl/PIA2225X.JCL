//PIA2225X JOB (C,66810,000,OPSB),IA,MSGCLASS=I,
//             CLASS=6,REGION=8M,TIME=1440
//*
//* IA - JOB WILL PERFORM NDM STEPS TO SEND 5 FILES CREATED BY PIA2225M
//*      AND PIA2225D TO DATASTAGE FOLDERS.
//*
//*
//STEP005 EXEC NDMPROC
//SYSIN     DD *
 SIGNON
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(IA2225X1)                -
 NEWNAME=AV2225D1                                            -
 &INFILE=PIA.ANN.PIA2225D.IATRANS.HDRTRLR                    -
 &OUTPATH=/pd3/resrc_disk1/DS_FSL/source/retirement          -
 &OUTFILE=/iatrans_hdrtrlr                                   -
 CASE=YES MAXDELAY=UNLIMITED
 SIGNOFF
//*
//*
//STEP010 EXEC NDMPROC
//SYSIN     DD *
 SIGNON
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(IA2225X1)                -
 NEWNAME=AV2225D2                                            -
 &INFILE=PIA.ANN.PIA2225D.IATRANS.RECTYP10                   -
 &OUTPATH=/pd3/resrc_disk1/DS_FSL/source/retirement          -
 &OUTFILE=/iatrans_rectyp10                                  -
 CASE=YES MAXDELAY=UNLIMITED
 SIGNOFF
//*
//*
//STEP020 EXEC NDMPROC
//SYSIN     DD *
 SIGNON
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(IA2225X1)                -
 NEWNAME=AV2225D3                                            -
 &INFILE=PIA.ANN.PIA2225D.IATRANS.RECTYP20                   -
 &OUTPATH=/pd3/resrc_disk1/DS_FSL/source/retirement          -
 &OUTFILE=/iatrans_rectyp20                                  -
 CASE=YES MAXDELAY=UNLIMITED
 SIGNOFF
//*
//*
//STEP030 EXEC NDMPROC
//SYSIN     DD *
 SIGNON
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(IA2225X1)                -
 NEWNAME=AV2225D4                                            -
 &INFILE=PIA.ANN.PIA2225D.IATRANS.RECTYP30                   -
 &OUTPATH=/pd3/resrc_disk1/DS_FSL/source/retirement          -
 &OUTFILE=/iatrans_rectyp30                                  -
 CASE=YES MAXDELAY=UNLIMITED
 SIGNOFF
//*
//*
//STEP040 EXEC NDMPROC
//SYSIN     DD *
 SIGNON
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(IA2225X1)                -
 NEWNAME=AV2225D5                                            -
 &INFILE=PIA.ANN.PIA2225D.IATRANS.RECTYP40                   -
 &OUTPATH=/pd3/resrc_disk1/DS_FSL/source/retirement          -
 &OUTFILE=/iatrans_rectyp40                                  -
 CASE=YES MAXDELAY=UNLIMITED
 SIGNOFF
//*
//*
