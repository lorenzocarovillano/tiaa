//PMO3070X JOB (P,19999,000,OPSB),
//             'ANNACCTG',
//             CLASS=1,
//             MSGCLASS=I
//**
//LIBSRCH  JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//**
//**----------------------------------------------------------------**
//** FTP A PGP ENCRYPTED FILE FROM PROD1 TO FTPMRO.FIDELITY.COM     **
//**----------------------------------------------------------------**
//**
//TRAN0010 EXEC PGM=FTP
//SYSTCPD  DD  DSN=PNETSF.TCPIP.SEZAINST(TCPDATD1),
//             DISP=SHR
//SYSFTPD  DD  DSN=PNETSF.TCPIP.SEZAINST(FTSDATD1),
//             DISP=SHR
//SYSMDUMP DD  SYSOUT=*
//SYSPRINT DD  SYSOUT=*
//OUTPUT   DD  DSN=&&LOG,
//             DISP=(NEW,PASS,DELETE),
//             DCB=(LRECL=80,RECFM=FB),
//             SPACE=(TRK,(5,5),RLSE),
//             UNIT=SYSDA
//INPUT    DD  DSN=PROD.PARMLIB(MO3070D1),
//             DISP=SHR
//**
//**----------------------------------------------------------------**
//** COPY FTP LOG TO SYSOUT FOR ARCHIVAL                            **
//**----------------------------------------------------------------**
//COPY0020 EXEC PGM=IEBGENER
//SYSPRINT DD  SYSOUT=*
//SYSUT1   DD  DSN=&&LOG,
//             DISP=(OLD,PASS,DELETE)
//SYSUT2   DD  SYSOUT=*
//SYSIN    DD  DUMMY
//**----------------------------------------------------------------**
//** VERIFY THAT THE FTP TRANSMIT COMPLETED OK.  RC=8 INDICATES     **
//** BAD TRANSMIT. RC=99 INDICATES EMPTY LOG FILE.                  **
//**----------------------------------------------------------------**
//**
//REXX0030 EXEC PGM=IKJEFT01,
//             DYNAMNBR=75,
//             PARM=FTP$VRFY
//SYSPROC  DD  DSN=PROD.VOL.REXX.EXEC,
//             DISP=SHR
//SYSUADS  DD  DSN=SYS1.UADS,
//             DISP=SHR
//SYSLBC   DD  DSN=SYS1.BRODCAST,
//             DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTERM  DD  SYSOUT=*
//FTPLOG   DD  DSN=&&LOG,
//             DISP=(OLD,PASS,DELETE)
//SYSTSPRT DD  SYSOUT=*
//SYSIN    DD  DUMMY
//SYSTSIN  DD  DUMMY
//**
//**----------------------------------------------------------------**
//** CANCEL PMO3090D FROM THE QUEUE SINCE XMIT IS GOOD              **
//**----------------------------------------------------------------**
//CA7SV040 EXEC PGM=U7SVC,
//             COND=(0,NE),
//             PARM='CA7=CA71'
//CA7PRINT DD  SYSOUT=*
//ERRORS   DD  SYSOUT=*
//SYSUDUMP DD  SYSOUT=U
//CA7DATA  DD  *
CANCEL,JOB=PMO3090D,FORCE=YES
