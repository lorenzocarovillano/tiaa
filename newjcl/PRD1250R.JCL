//PRD1250R JOB (P,66955,000,LEGS),'TNG BRIDGE',
//         MSGCLASS=I,
//         CLASS=1,
//         REGION=9M
//*
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//JOBLIB   DD  DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//*
//*****************************************************************
//* READ DA REPORTING EXTRACT & SPLIT BY NUMBER OF CONTRACTS
//*****************************************************************
//PRD1250R EXEC PRD1250R,
//      PARMMEM=RD1250R1,
//      DATA='DATA1'
//*****************************************************************
//* REPORTS
//*****************************************************************
//EXTR020.CMPRT01 DD SYSOUT=(&REPT.,RD1250R1)
//EXTR020.CMPRT02 DD SYSOUT=(&REPT.,RD1250R2)
