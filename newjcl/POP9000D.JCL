//POP9000D JOB (C,19999,000,OPSB),'OMNIPAY',CLASS=1,
//         MSGCLASS=I
//*
//LIBSRCH  JCLLIB ORDER=(PROD.OAS.NY.PARMLIB)
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*
//*MSYS // INCLUDE MEMBER=JOBLIBU
//JOBLIB   DD  DISP=SHR,DSN=PDBFPP.OMNIPAY.TIAA.BAT.LOADLIB             00011001
//         DD  DISP=SHR,DSN=PDBFPP.OMNIPAY.CURRENT.BAT.LOADLIB          00012001
//         DD  DISP=SHR,DSN=PDBFPP.OMNIPAY.TIAA.ONLINE.LOADLIB          00013001
//         DD  DISP=SHR,DSN=PDBFPP.OMNIPAY.CURRENT.ONLINE.LOADLIB
//         DD  DISP=SHR,DSN=PROD.BATCH.LOADLIB
//*
//*-----------------------------------------------------------------*
//*  SET RUN DATE FOE ALL OMNIPAY JOBS
//*-----------------------------------------------------------------*
//STEP010  EXEC TIADATE,
//            SPREFX='POMPY',
//            VFSET='PMTP'
//*
//*-----------------------------------------------------------------*
//* OMNIPAY PROLINE PAYMENT PROCESS - GENERIC WARRANTS REFORMAT
//*-----------------------------------------------------------------*
//OGENW   EXEC TIAGENW
