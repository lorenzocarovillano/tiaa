//PMO0500D JOB (P,15000,000,OPSS),                                      
//             PREM,
//             CLASS=1,
//             MSGCLASS=I
//*MSYS #SCC,COND=(3,LT,PMO0500D.UPDT010)
//*MSYS #SCC,COND=(0,NE,*-PMO0500D.UPDT010)
//*                                                                     
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*
//JOBLIB DD DSN=PROD.BATCH.LOADLIB,DISP=SHR                             
//       DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR                  
//*-----------------------------------------------------------------*
//* THIS JOB UPDATES FIDELITY LEDGER IND AND REPORTS ON 301S & 801S *
//*-----------------------------------------------------------------*
//PMO0500D EXEC PMO0500D                                                
//
