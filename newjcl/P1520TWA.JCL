//P1520TWA JOB (P,14400,000,FINS),CPS-NPD,MSGCLASS=I,                   
//        CLASS=1,REGION=8M                                             
//*                                                                     
//*                                                                     
//JOBLIB  DD DSN=PROD.BATCH.LOADLIB,DISP=SHR
//        DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//*
//P1520TWA EXEC P1520TWA                                                
//**                                                                    
//**----------------------------------------------------------------**
//** SEND NOTIFICATION INDICATING TAX VENDOR FILE IS READY          **
//**                                                                **
//**----------------------------------------------------------------**
//NOTFY999 EXEC CAJUCMNY,COND=(0,NE)
//SYSPRINT DD  SYSOUT=*
//SYSIN    DD  *
MO ATTN: P1520TWA IS COMPLETE AND VENDOR CARTRIDGE IS READY.
/*
//*
