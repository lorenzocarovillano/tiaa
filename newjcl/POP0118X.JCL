//POP0118X JOB (P,19999,000,OPSB),'OMNIPAY',MSGCLASS=I,CLASS=6,
//          REGION=8M
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//* ------------------------------------------------------------------*
//STEP005 EXEC NDMPROC
//SYSIN     DD *
 SIGNON
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(OP0118D1)                -
 CASE=YES MAXDELAY=UNLIMITED                                 -
 &EXTFIL=POMPY.ESPEXTR.PMT0CC(0)                             -
 &CNTFIL=POMPY.ESPEXTR.PMT0CC.CTL                            -
 &OUTPATH=/pd1/resrc_disk1/ods/source                        -
 &EXTRACT=/OMNIPAY_CC_0_EXT.                                 -
 &CONTROL=/OMNIPAY_CC_0_CONTROL.                             -
 &JOBNAME=POP0118X
  SIGNOFF
//*
//STEP010 EXEC NDMPROC
//SYSIN     DD *
 SIGNON
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(OP0118D1)                -
 CASE=YES MAXDELAY=UNLIMITED                                 -
 &EXTFIL=POMPY.ESPEXTR.PMT0CK(0)                             -
 &CNTFIL=POMPY.ESPEXTR.PMT0CK.CTL                            -
 &OUTPATH=/pd1/resrc_disk1/ods/source                        -
 &EXTRACT=/OMNIPAY_CK_0_EXT.                                 -
 &CONTROL=/OMNIPAY_CK_0_CONTROL.                             -
 &JOBNAME=POP0118X
  SIGNOFF
//*
//STEP015 EXEC NDMPROC
//SYSIN     DD *
 SIGNON
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(OP0118D1)                -
 CASE=YES MAXDELAY=UNLIMITED                                 -
 &EXTFIL=POMPY.ESPEXTR.PMT0CD(0)                             -
 &CNTFIL=POMPY.ESPEXTR.PMT0CD.CTL                            -
 &OUTPATH=/pd1/resrc_disk1/ods/source                        -
 &EXTRACT=/OMNIPAY_CD_0_EXT.                                 -
 &CONTROL=/OMNIPAY_CD_0_CONTROL.                             -
 &JOBNAME=POP0118X
  SIGNOFF
//*
//STEP020 EXEC NDMPROC
//SYSIN     DD *
 SIGNON
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(OP0118D1)                -
 CASE=YES MAXDELAY=UNLIMITED                                 -
 &EXTFIL=POMPY.ESPEXTR.PMT0DS(0)                             -
 &CNTFIL=POMPY.ESPEXTR.PMT0DS.CTL                            -
 &OUTPATH=/pd1/resrc_disk1/ods/source                        -
 &EXTRACT=/OMNIPAY_DS_0_EXT.                                 -
 &CONTROL=/OMNIPAY_DS_0_CONTROL.                             -
 &JOBNAME=POP0118X
  SIGNOFF
//*
//STEP025 EXEC NDMPROC
//SYSIN     DD *
 SIGNON
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(OP0118D1)                -
 CASE=YES MAXDELAY=UNLIMITED                                 -
 &EXTFIL=POMPY.ESPEXTR.PMT0DF(0)                             -
 &CNTFIL=POMPY.ESPEXTR.PMT0DF.CTL                            -
 &OUTPATH=/pd1/resrc_disk1/ods/source                        -
 &EXTRACT=/OMNIPAY_DF_0_EXT.                                 -
 &CONTROL=/OMNIPAY_DF_0_CONTROL.                             -
 &JOBNAME=POP0118X
  SIGNOFF
//*
//STEP030 EXEC NDMPROC
//SYSIN     DD *
 SIGNON
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(OP0118D1)                -
 CASE=YES MAXDELAY=UNLIMITED                                 -
 &EXTFIL=POMPY.ESPEXTR.PMT0AN.RECCC(0)                       -
 &CNTFIL=POMPY.ESPEXTR.PMT0AN.RECCC.CTL                      -
 &OUTPATH=/pd1/resrc_disk1/ods/source                        -
 &EXTRACT=/OMNIPAY_ANCC_0_EXT.                               -
 &CONTROL=/OMNIPAY_ANCC_0_CONTROL.                           -
 &JOBNAME=POP0118X
  SIGNOFF
//*
//STEP035 EXEC NDMPROC
//SYSIN     DD *
 SIGNON
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(OP0118D2)                -
 CASE=YES MAXDELAY=UNLIMITED                                 -
 &INFILE=POMPY.ESPEXTR.PMT0CC(0)                             -
 &OUTPATH=/pd5/resrc_disk1/DS_FSL/source/incremental/omni    -
 &OUTFILE=/OMNIPAY_CC_0_EXT.                                 -
 &JOBNAME=POP0118X
  SIGNOFF
//*
//STEP040 EXEC NDMPROC
//SYSIN     DD *
 SIGNON
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(OP0118D2)                -
 CASE=YES MAXDELAY=UNLIMITED                                 -
 &INFILE=POMPY.ESPEXTR.PMT0CK(0)                             -
 &OUTPATH=/pd5/resrc_disk1/DS_FSL/source/incremental/omni    -
 &OUTFILE=/OMNIPAY_CK_0_EXT.                                 -
 &JOBNAME=POP0118X
  SIGNOFF
//*
//STEP045 EXEC NDMPROC
//SYSIN     DD *
 SIGNON
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(OP0118D2)                -
 CASE=YES MAXDELAY=UNLIMITED                                 -
 &INFILE=POMPY.ESPEXTR.PMT0CD(0)                             -
 &OUTPATH=/pd5/resrc_disk1/DS_FSL/source/incremental/omni    -
 &OUTFILE=/OMNIPAY_CD_0_EXT.                                 -
 &JOBNAME=POP0118X
  SIGNOFF
//*
//STEP050 EXEC NDMPROC
//SYSIN     DD *
 SIGNON
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(OP0118D2)                -
 CASE=YES MAXDELAY=UNLIMITED                                 -
 &INFILE=POMPY.ESPEXTR.PMT0DS(0)                             -
 &OUTPATH=/pd5/resrc_disk1/DS_FSL/source/incremental/omni    -
 &OUTFILE=/OMNIPAY_DS_0_EXT.                                 -
 &JOBNAME=POP0118X
  SIGNOFF
//*
//STEP055 EXEC NDMPROC
//SYSIN     DD *
 SIGNON
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(OP0118D2)                -
 CASE=YES MAXDELAY=UNLIMITED                                 -
 &INFILE=POMPY.ESPEXTR.PMT0DF(0)                             -
 &OUTPATH=/pd5/resrc_disk1/DS_FSL/source/incremental/omni    -
 &OUTFILE=/OMNIPAY_DF_0_EXT.                                 -
 &JOBNAME=POP0118X
  SIGNOFF
//*
//STEP060 EXEC NDMPROC
//SYSIN     DD *
 SIGNON
 SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(OP0118D2)                -
 CASE=YES MAXDELAY=UNLIMITED                                 -
 &INFILE=POMPY.ESPEXTR.PMT0AN.RECCC(0)                       -
 &OUTPATH=/pd5/resrc_disk1/DS_FSL/source/incremental/omni    -
 &OUTFILE=/OMNIPAY_ANCC_0_EXT.                               -
 &JOBNAME=POP0118X
  SIGNOFF
//*
