//PNI7800R JOB (P,66205,000,OPSS),'IRA-SUB CNTRCT ISSUE',
//         CLASS=G,MSGCLASS=I
//*
//*MSYS #SCC,COND=(1,LT,PNI7800R.EXTR020)
//*MSYS #SCC,COND=(1,LT,PNI7800R.EXTR030)
//*MSYS #SCC,COND=(0,NE,*-PNI7800R.EXTR020)
//*MSYS #SCC,COND=(0,NE,*-PNI7800R.EXTR030)
//*
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*
//JOBLIB DD DSN=PROD.BATCH.LOADLIB,DISP=SHR
//       DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//       DD DSN=PROD.CICS.USERPRD1.LOADLIB,DISP=SHR
//       DD DSN=PCICSPP.OSSD.LOADLIB,DISP=SHR
//*
// SET SEQ='7'
// SET RPTSEQ='G'
//*
//PNI7800R EXEC PNI0800R
