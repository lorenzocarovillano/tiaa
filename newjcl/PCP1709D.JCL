//PCP1709D JOB (C,69999,000,FINS),'CPS/IS EFT,STMNTS',CLASS=1,          
//       MSGCLASS=I,PRTY=12                             MOBIUS >60 BRK  
//*                                                                     
//LIBSRCH  JCLLIB ORDER=(PROD.OAS.NY.PARMLIB)                           
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*--------------------------------------------------------------------*
//*      O P E N   I N V E S T M E N T S   A R C H I T E C T U R E     *
//*                   *** PCP1709D ***                                 *
//*      CPS - EFT   STATEMENTS SPOOLING TO ZEROX  MICR PRINTER        *
//*      ZEROX PRODUCTION - CHECKS = DEST=TXLBCHK                      *
//*      ZEROX PRODUCTION - EFT    = DEST=TXLBEFT                      *
//*      ZEROX TEST         CHECKS = DEST=TTLBCHK                      *
//*      ZEROX TEST       - EFT    = DEST=TTLBEFT                      *
//*--------------------------------------------------------------------*
//JOBLIB   DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR         PROD   
//         DD DSN=PROD.BATCH.LOADLIB,DISP=SHR                    PROD   
//*                                                                     
//PCP1709D EXEC PCP1707D,                                               
//         PRTDFLT='TXLBEFT',       ZEROX  PRINTER DESTINATION          
//         FORMTYP='CPWOIAE',                                           
//         OUTCLS=S,                CHECK/EFT OUTPUT CLASS              
//         CHKEFT='EFT.WITHHOLD',   'EFT NO HOLD' FOR 1706/1708/1711    
//*                                 'EFT'   FOR 1707/1709/1712          
//         RPTID='CP1709D',         MOBIUS REPORT ID                    
//         GDG0='0'                 EXISTING GDG                        
