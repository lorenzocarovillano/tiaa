//POP3031R JOB (P,19999,000,OPSB),'OMNIPAY',                            
//         CLASS=1,MSGCLASS=I,                                          
//         REGION=0M                                                    
//*                                                                     
//*******************************************************************   
//*                                                                     
//*    OMNIPAY 2018 RELEASE                                             
//*    CREATE MX MASTER FILE FOR ARCHIVAL DATASETS                      
//*                                                                     
//*******************************************************************   
//LIBSRCH  JCLLIB ORDER=(PROD.OAS.NY.PARMLIB)                           
//*                                                                     
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*MSYS // INCLUDE MEMBER=JOBLIBU                        
//JOBLIB   DD  DISP=SHR,DSN=PDBFPP.OMNIPAY.TIAA.BAT.LOADLIB             00011001
//         DD  DISP=SHR,DSN=PDBFPP.OMNIPAY.CURRENT.BAT.LOADLIB          00012001
//         DD  DISP=SHR,DSN=PDBFPP.OMNIPAY.TIAA.ONLINE.LOADLIB          00013001
//         DD  DISP=SHR,DSN=PDBFPP.OMNIPAY.CURRENT.ONLINE.LOADLIB
//         DD  DISP=SHR,DSN=PROD.BATCH.LOADLIB
//*                                                                     
//*-----------------------------------------------------------------*   
//*  DEFINE OMNIPAY ARCHIVAL MX FILES                                   
//*-----------------------------------------------------------------*   
//DEFIN10  EXEC PGM=IDCAMS                                              
//SYSPRINT DD SYSOUT=*                                                  
//SYSIN    DD DSN=PROD.OMNIPAY.BAT.CTL(DEFXARC),DISP=SHR                
//*                                                                     
//*-----------------------------------------------------------------*   
//*  POPULATE DATA INTO ARCHIVAL FILE SETS                              
//*-----------------------------------------------------------------*   
//RSTR010  EXEC PGM=IDCAMS                                              
//SYSPRINT DD SYSOUT=*                                                  
//FILEA    DD DSN=POMPY.ONL.PMT0MX,DISP=SHR                             
//FILEB    DD DSN=POMPY.ONL.PMTAMX,DISP=SHR                             
//SYSIN    DD DSN=PROD.OMNIPAY.BAT.CTL(REPNRU),DISP=SHR                 
//*                                                                     
//RSTR020  EXEC PGM=IDCAMS                                              
//SYSPRINT DD SYSOUT=*                                                  
//FILEA    DD DSN=POMPY.ONL.PMTPMX,DISP=SHR                             
//FILEB    DD DSN=POMPY.ONL.PMTQMX,DISP=SHR                             
//SYSIN    DD DSN=PROD.OMNIPAY.BAT.CTL(REPNRU),DISP=SHR                 
//*                                                                     
