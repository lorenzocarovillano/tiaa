//PRD1231W JOB (P,66955,000,LEGS),'TIAA MDOACCS',
//         MSGCLASS=I,
//         CLASS=1
//*
//* RDR2012A - TIAA MDO ACCESS ENDORSEMENTS
//*
//**********************************************************************
//* MODIFICATION LOG:
//*---------------------------------------------------------------------
//* DATE       NAME            CHG#      DESCRIPTION
//*---------------------------------------------------------------------
//* 01/14/2016 Y. MONTESCLAROS CHG368042 INITIAL SET UP FOR AUTOMATION
//*                                      COPIED FROM PRD1231R
//* 08/27/2016 J. AVE          CHG393292 CHANGE PROC FROM PCC2001R TO
//*                                      PCC2004R
//*
//**********************************************************************
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//JOBLIB   DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//        DD DSN=PROD.BATCH.LOADLIB,DISP=SHR
//*
//**********************************************************************
//*
//PRD1231W EXEC PCC2004R,
//*       PARMMEM=RD1231R1,
//       PARMMEM2=RD1231R2,
//       PARMMEM3=RD1231R3,
//       PARMMEM6=RD1231R4,
//       PACKAGE=RDR2012A,
//       DATATYPE=MDOACCS,
//       JOBNM=PRD1231R,
//       FILE=FILE1
//*
//EXTR010.CMPRT01 DD SYSOUT=(&REPT.,RD1231R1)
//EXTR010.CMPRT02 DD SYSOUT=(&REPT.,RD1231R2)
//EXTR020.CMPRT01 DD SYSOUT=(&REPT.,RD1231R3)
//EXTR020.CMPRT02 DD SYSOUT=(&REPT.,RD1231R4)
//*
