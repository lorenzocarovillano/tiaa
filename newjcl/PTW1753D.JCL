//PTW1753D JOB (P,14400,000,FINS),                                      
//            CPS-NPD,                                                  
//            MSGCLASS=I,                                               
//            CLASS=1,                                                  
//            REGION=8M                                                 
//*                                                                     
//*--------------------------------------------------------------------*
//*                                                                    *
//*     T I A A - C R E F   O P E N   P L A N   S O L U T I O N S      *
//*                                                                    *
//*                          *** PTW1753D ***                          *
//*                                                                    *
//*    TAXWARS MCCAMISH LIFE INSURANCE DAILY FEED (PREVIOUS YEAR)      *
//*                        FLAT FILE PROCESSING                        *
//*                                                                    *
//*--------------------------------------------------------------------*
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB                              
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//JOBLIB  DD DSN=PROD.BATCH.LOADLIB,DISP=SHR                            
//        DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR                 
//*---------------------------------------------------------------------
//PTW1753D EXEC PTW1750D,                                               
//         YEAR='PREV',                                                 
//         JOBNM='PTW1753D',                                            
//         JOBNM1='PTW1743D',                                           
//         JOBNM2='PTW1732D',                                           
//         REPID1=',TW1753D1',                                          
//         REPID2=',TW1753D2',                                          
//         REPID3=',TW1753D3',                                          
//         REPID4=',TW1753D4',                                          
//         REPID5=',TW1753D5',                                          
//         REPID6=',TW1753D6',                                          
//         REPID7=',TW1753D7',                                          
//         REPID8=',TW1753D8',                                          
//         REPID9=',TW1753D9',                                          
//         REPIDA=',TW1753DA',                                          
//         REPIDB=',TW1753DB',                                          
//         REPIDC=',TW1753DC'                                           
//                                                                      
