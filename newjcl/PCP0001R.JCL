//PCP0001R JOB (P,69999,000,FINS),'CPS-X3019',
//         MSGCLASS=I,CLASS=1
//*
//LIBSRCH  JCLLIB ORDER=(PROD.OAS.NY.PARMLIB)
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//JOBLIB   DD DSN=PROD.BATCH.LOADLIB,DISP=SHR
//         DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//*-------------------------------------------------------------------* 
//* STEP FOR IA EFT FILE DATE CHANGE                                    
//*-------------------------------------------------------------------* 
//P2240CPR EXEC P2240CPR                                                
//*
//*-------------------------------------------------------------------* 
//DEL0010 EXEC PGM=IEFBR14                                              
//DELDD01 DD DSN=PPDD.CPS.BANKINFO.EXTRACT,                             
//           DISP=(MOD,DELETE,DELETE),                                  
//           DCB=(RECFM=FB,LRECL=172,BLKSIZE=0),                        
//           UNIT=SYSDA,SPACE=(CYL,(5,5),RLSE)                          
//*                                                                     
//EXTR010  EXEC PGM=NATB030,REGION=0M,COND=(0,NE),
//         PARM='SYS=NAT003P,IM=D'
//CMPRINT  DD SYSOUT=*                                                  
//SYSPRINT DD SYSOUT=*                                                  
//DDPRINT  DD SYSOUT=*                                                  
//SYSUDUMP DD SYSOUT=*                                                  
//SYSUT2   DD SYSOUT=*                                                  
//SYSOUT   DD SYSOUT=*                                                  
//CMPRT01  DD  SYSOUT=*                                                 
//CMPRT02  DD  SYSOUT=*                                                 
//CMWKF01  DD DSN=PPDD.CPS.BANKINFO.EXTRACT,                            
//            DISP=(,CATLG,DELETE),                                     
//            UNIT=SYSDA,                                               
//            DATACLAS=DCPSEXTC,                                        
//            RECFM=FB,LRECL=172,                                       
//            SPACE=(CYL,(5,2),RLSE)                                    
//CMSYNIN   DD DSN=PROD.NT2LOGON.PARMLIB(PANNSEC),DISP=SHR
//          DD DSN=PROD.PARMLIB(PCP0001R),DISP=SHR
//* CPOP899
//*-------------------------------------------------------------------* 
//* SORT020 SORT BY CONTRACT NUM AND REMOVES DUP                        
//*-------------------------------------------------------------------* 
//SORT030  EXEC PGM=SORT,COND=(0,NE)                                    
//SORTWK01 DD  UNIT=SYSDA,                                              
//             SPACE=(CYL,(128,100),RLSE)                               
//SORTWK02 DD  UNIT=SYSDA,                                              
//             SPACE=(CYL,(128,100),RLSE)                               
//SORTWK03 DD  UNIT=SYSDA,                                              
//             SPACE=(CYL,(128,100),RLSE)                               
//SORTWK04 DD  UNIT=SYSDA,                                              
//             SPACE=(CYL,(128,100),RLSE)                               
//SORTWK05 DD  UNIT=SYSDA,                                              
//             SPACE=(CYL,(128,100),RLSE)                               
//SORTIN   DD DSN=PPDD.CPS.BANKINFO.EXTRACT,DISP=SHR                    
//SORTOUT  DD DSN=PPDD.CPS.BANKINFO.EXTRACT.SORTED,                     
//            DISP=(,CATLG,DELETE),UNIT=SYSDA,                          
//            SPACE=(CYL,(128,100),RLSE),                               
//            DATACLAS=DCPSEXTC,                                        
//            DCB=(*.SORTIN)                                            
//SYSOUT   DD  SYSOUT=*                                                 
//SYSIN    DD  DSN=PROD.PARMLIB(POP0120I),DISP=SHR                      
//*                                                                     
