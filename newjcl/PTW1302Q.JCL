//PTW1302Q JOB (P,14400,000,FINS),NPD-TAX,MSGCLASS=I,                   
//         CLASS=1,REGION=0M                                            
//*                                                                     
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB                              
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*                                                                     
//JOBLIB   DD DSN=PROD.BATCH.LOADLIB,DISP=SHR                           
//         DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR                
//*MSYS // INCLUDE MEMBER=PJCLINCL                       
//       DD DISP=SHR,DSN=PDBFPP.ORACLE.UPGRADE.CMDLOAD
//*                                                                     
//*                                                                     
//PTW1302Q EXEC PTW1302Q                                                
