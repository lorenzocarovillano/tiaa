//P1556TWX JOB (P,14400,000,FINB),CAIMS,MSGCLASS=I,CLASS=6,             
//  REGION=8M                                                           
//NDMS010 EXEC PGM=DMBATCH,REGION=4M,PARM=(YYSLYNN)                     
//DMNETMAP DD DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR                   
//DMPUBLIB DD DSN=PMVSPP.STERLING.PROCESS.LIB,DISP=SHR                  
//DMMSGFIL DD DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR                      
//DMPRINT   DD SYSOUT=*                                                 
//NDMCMDS   DD SYSOUT=*                                                 
//SYSIN     DD *                                                        
  SIGNON ESF=YES                                                        
  SUBMIT DSN=PROD.NDM.NY.PROCESS.LIB(DN1556TX) MAXDELAY=UNLIMITED       
  SIGNOFF                                                               
/*                                                                      
//                                                                      
