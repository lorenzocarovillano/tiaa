//PCC2520M JOB (P,66800,000,OPSS),'NET CHANGE FILES-CCP',               
//         CLASS=1,TIME=1440,
//         MSGCLASS=I
//*
//LIBSRCH  JCLLIB ORDER=PROD.OAS.NY.PARMLIB
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*
//JOBLIB   DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//         DD DSN=PROD.BATCH.LOADLIB,DISP=SHR
//**                                                                    
//*********************************************************************
//**      PREPARE WORK FILES THAT WILL BE USED BY NEXT JOB STEPS        
//*********************************************************************
//*
//*********************************************************************
//*
//PNTFLATD EXEC PCC2520M,
//       APPNM='PNTFLATD',
//       PARMMEM3=PC2521M1,
//       NTCHGSPL='PNTFLATD',
//       SRCFILE='PPDD.P2515IAM.ECS.FLATS.DOMESTIC'                     
//**                                                                    
//*********************************************************************
//PNTFLATF EXEC PCC2520M,
//       APPNM='PNTFLATF',
//       PARMMEM3=PC2521M2,
//       NTCHGSPL='PNTFLATF',
//       SRCFILE='PPDD.P2515IAM.ECS.FLATS.FOREIGN'                      
//**                                                                    
//*********************************************************************
//PNTFOURS EXEC PCC2520M,
//       APPNM='PNTFOURS',
//       PARMMEM3=PC2521M3,
//       NTCHGSPL='PNTFOURS',
//       SRCFILE='PPDD.P2515IAM.ECS.NETC4'                              
//**                                                                    
//*********************************************************************
//PNTHOLD  EXEC PCC2520M,
//       APPNM='PNTHOLD',
//       PARMMEM3=PC2521M4,
//       NTCHGSPL='PNTHOLD',
//       SRCFILE='PPDD.P2515IAM.ECS.HOLD'                               
//**                                                                    
//*********************************************************************
//PNTLABLD EXEC PCC2520M,
//       APPNM='PNTLABLD',
//       PARMMEM3=PC2521M5,
//       NTCHGSPL='PNTLABLD',
//       SRCFILE='PPDT.P2515IAM.ECS.LABELS.DOMESTIC'                    
//**                                                                    
//*********************************************************************
//PNTLABLF EXEC PCC2520M,
//       APPNM='PNTLABLF',
//       PARMMEM3=PC2521M6,
//       NTCHGSPL='PNTLABLF',
//       SRCFILE='PPDD.P2515IAM.ECS.LABELS.FOREIGN'                     
//**                                                                    
//*********************************************************************
//PNTONES  EXEC PCC2520M,
//       APPNM='PNTONES',
//       PARMMEM3=PC2521M7,
//       NTCHGSPL='PNTONES',
//       SRCFILE='PPDD.P2515IAM.ECS.NETC1'                              
//**                                                                    
//*********************************************************************
//PNTTHREE EXEC PCC2520M,
//       APPNM='PNTTHREE',
//       PARMMEM3=PC2521M8,
//       NTCHGSPL='PNTTHREE',
//       SRCFILE='PPDD.P2515IAM.ECS.NETC3'                              
//**                                                                    
//*********************************************************************
//PNTTRIF  EXEC PCC2520M,
//       APPNM='PNTTRIF',
//       PARMMEM3=PC2521M9,
//       NTCHGSPL='PNTTRIF',
//       SRCFILE='PPDD.P2515IAM.ECS.TRI.FOREIGN'                        
//**                                                                    
//*********************************************************************
//PNTTWOS  EXEC PCC2520M,
//       APPNM='PNTTWOS',
//       PARMMEM3=PC2521M0,
//       NTCHGSPL='PNTTWOS',
//       SRCFILE='PPDD.P2515IAM.ECS.NETC2'                              
//**                                                                    
