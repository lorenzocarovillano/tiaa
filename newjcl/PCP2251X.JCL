//PCP2251X JOB (P,78000,000,FINB),XMLTRNSFR,MSGCLASS=I,CLASS=6          
//LIBSRCH JCLLIB ORDER=PROD.OAS.NY.PARMLIB                              
//*MSYS //OUTPUT1 INCLUDE MEMBER=OUTVDRD                    
//OUTVDR  OUTPUT DEST=MVSJESD,JESDS=ALL,DEFAULT=YES
//OUTLOCL OUTPUT DEST=LOCAL
//OUTDUMP OUTPUT DEST=LOCAL
//*                                                                     
//JOBLIB    DD DSN=PROD.OMNIPLUS.USER.LOADLIB,DISP=SHR                  
//          DD DSN=PROD.BATCH.LOADLIB,DISP=SHR                          
//          DD DSN=PDBFPP.OMNIPLUS.CURRENT.BAT.LOADLIB,DISP=SHR         
//*                                                                     
//* SEND OMNIPAY PAYMENTS TO CCP                                        
//*                                                                     
//  SET LIB1=PROD                     *** WMQFTE.PARMLIB PREFIX         
//  SET HLQ1=PCPS.ANN                 *** HIGH LEVEL QUALIFIER          
//  SET OR=AP                         *** ORIGIN OF PAYMENT             
//  SET FT1=CWH                       *** MAINFRAME LAST QUALIFIER      
//  SET SS=017                        *** MAINFRAME LAST QUALIFIER      
//  SET CJ=PCP2251X                   *** CURR JOB NAME                 
//  SET PJOBNM=PCP2298M               *** PREV JOB NAME                 
//  SET HL=&HLQ1..&PJOBNM             *** SOURCE FL HLQ IN FTE CTL PARM 
//*                                                                     
//STEP010  EXEC PCP2250X                                                
