/bin/awk '
BEGIN { FS = "=" }
{
  line[NR]= $0
  pcb=0
  segdummy=0
}
END {
 PGM=""
 PGM=ENVIRON["PGM"]
 APPLCTN=0
 for (nline = 1 ; nline <= NR ; nline++){
   if ( line[nline] ~ /^\/\/ .*DCB=/ &&  (line[nline] ~ /LRECL/ || line[nline] ~ /RECFM/ ) && ( line[nline] !~ /^\/\/ .*DCB=\(/ && line[nline] !~ /^\/\/ .*DCB=BUFNO/ )  ) {
      print "//*" line[nline]
      if ( index(line[nline],"DCB=") > index(line[nline],"RECFM") ){
        line[nline]=substr(line[nline],1,index(line[nline],"DCB=")-2) ")"
        gsub(/RECFM=/,"DCB=(RECFM=",line[nline])

      }else{
        gsub(/DCB=/,"DCB=(",line[nline])
        gsub(/ *$/,"",line[nline])
        if ( line[nline] ~ /,$/ ) {
          gsub(/,$/,"),",line[nline])
        }else{
          line[nline]=line[nline] ")"
        }
      }
   }
   print line[nline]
 }
}
'
