//PCP0604D PROC HLQ0='POMPY',       DASD HLQ
//         JOBNM='POP0604D',        2ND HLQ FOR FILES CREATED
//         REGN1='9M',              REGION SIZE
//         GDG='0',                 NEW GDG FILE CREATED
//         RPTID='CP0604D',         MOBIUS
//         REPT1='8',               MOBIUS
//         REPT00='*',              REPORTS
//         JCLO00=*,
//         NATVERS='NATB030',       NATURAL VERSION
//         NAT='NAT003P',           NATURAL PROFILE
//         PARMLIB='PARMLIB',         LLQ COMMON PARM LIBRARY
//         NATLOG='NT2LOGON.PARMLIB', 2 LVL QUAL NATURAL LOGON LIBRARY
//         NATMEM='PANNSEC',          NATURAL LOGON MEMBER
//         LIB1='PROD',
//         DBID='003'
//*
//*--------------------------------------------------------------------*
//*                                                                    *
//* PROCEDURE:  PCP0604D                           DATE:    09/10/2008 *
//*                                                                    *
//*         CONSOLIDATED PAYMENT SYSTEM / INVESTMENT SOLUTIONS         *
//*                                                                    *
//*                COPY JPMORGAN CHASE (NOC) NOTICE OF CHANGE          *
//*   JPMORGAN ACCOUNTS
//*         009440001        TECHSRPT        TECHRET                   *
//*         926405900        TIDASRPT        TIDARET                   *
//*         005092201        TAAASRPT        TAAARET                   *
//*         9785079087       TIAASRPT        TIAARET                   *
//*                                                                    *
//*
//*  009440001 TECHRET   APTDTI53 / UPTDTT14.NDM.OUTPUT.TIAA.PPTDTI53(+1
//*                                 PCPS.ANN.NOC.RPT01.CHASEIN
//*
//*  926405900 TIDARET   APTDTI55 / UPTDTT14.NDM.OUTPUT.TIAA.PPTDTI55(+1
//*                                 PCPS.ANN.NOC.RPT02.CHASEIN
//*
//*  005092201 TAAARET   APTDTI57 / UPTDTT14.NDM.OUTPUT.TIAA.PPTDTI57(+1
//*                                 PCPS.ANN.NOC.RPT02.CHASEIN
//*
//*  9785079087 TIAARET  APTDTI59 / UPTDTT14.NDM.OUTPUT.TIAA.PPTDTI59(+1
//*                                 PCPS.ANN.NOC.RPT02.CHASEIN
//*
//*  STEP    PROGRAM   PARMS                   COMMENTS                *
//* -------  -------  --------  -------------------------------------- *
//* COPY010  IEBGENER           COPY FILE TO DASD  NOC   - RET 1       *
//* COPY020  IEBGENER           COPY FILE TO DASD  NOC   - RET 2       *
//* COPY030  IEBGENER           COPY FILE TO DASD  NOC   - RET 3       *
//* COPY050  IEBGENER           COPY FILE TO MOBIUS NOC   - MOBIUS 1   *
//* COPY060  IEBGENER           COPY FILE TO MOBIUS NOC   - MOBIUS 2   *
//* COPY070  IEBGENER           COPY FILE TO MOBIUS NOC   - MOBIUS 3   *
//* DELT080  IEFBR14            DELETE THE TEMPORARY DSNS FFROM BANK   *
//* UTIL090  COBOL    PSGN4004  READ EXTERNALISATION TABLE             *
//* PRNT100  NATURAL  FCPP604   CREATE SINGLE NOC SHEETS FROM REPORTS) *
//* COPY110  IEBGENER   COPY FILE TO MOBIUS  (ZERO DOLLAR CHANGE - NOC)*
//* COPY120  IEBGENER   COPY FILE TO MOBIUS  (REASON CODE SINGLE SHEET *
//* UPDT130  NATURAL  FCPP604A  ANNOTATE AND REJECT EFT ON DATABASE    *
//*                                                                    *
//* MODIFICATION LOG:                                                  *
//* C358410  MUKHERR    1) EXTRACT GENERATED FROM EXTERNALISATION TABLE*
//*                        USING PRO-COBOL PGM PSGN4004                *
//*                     2) NATURAL PGM FCPP604 READS THE FILE TO       *
//*                        POPULATE WPID                               *
//* C608620  VIKRAM     1) REMOVED THE USE OF FILE 04 FROM  CHASE      *
//* 01/15/2020 CTS     - CHANGES FOR CPS SUNSET                        *
//*                     FOLLOWING STEPS ARE REMOVED. SIMILAR FUNCTIONS *
//*                     WILL BE REPLICATED IN OMNIPAY JOB <********>   *
//*                     COPY010, COPY020, COPY030, COPY040,            *
//*                     COPY050, COPY060, COPY070, COPY080,            *
//*                     DELT090, UTIL095, PRNT100, COPY110, COPY120    *
//*--------------------------------------------------------------------*
//* UPDT130  NATURAL  FCPP604A ANNOTATE AND REJCT EFT ON DATABASE
//*--------------------------------------------------------------------*
//UPDT130  EXEC PGM=&NATVERS,REGION=&REGN1,
//         PARM='IM=D,SYS=&NAT'
//DDCARD   DD DSN=&LIB1..&PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT  DD SYSOUT=&JCLO00,OUTPUT=*.OUTVDR
//CMPRINT  DD SYSOUT=&REPT00,OUTPUT=*.OUTVDR
//CMPRT02  DD SYSOUT=(&REPT1,&RPTID.C),
//         DCB=(RECFM=FBA,LRECL=133)
//CMPRT03  DD SYSOUT=(&REPT1,&RPTID.D),
//          DCB=(RECFM=FBA,LRECL=133)
//CMWKF01  DD DSN=&HLQ0..&JOBNM..NOC.RPT01.CH(&GDG),DISP=SHR
//         DD DSN=&HLQ0..&JOBNM..NOC.RPT02.CH(&GDG),DISP=SHR
//         DD DSN=&HLQ0..&JOBNM..NOC.RPT03.CH(&GDG),DISP=SHR
//**       DD DSN=&HLQ0..&JOBNM..NOC.RPT04.CHASE(&GDG),DISP=SHR
//CMSYNIN  DD DSN=&LIB1..&NATLOG(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..&PARMLIB(PCP0604B),DISP=SHR
//*
