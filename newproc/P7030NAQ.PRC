//P7030NAQ PROC DBID='015',
//         DMPS='U',
//         GDG0='+0',
//         GDG1='+1',
//         HLQ0='PNA.COR',
//         JCLO='*',
//         REPT='8',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         NAT='NAT015P',
//         NATMEM='PCRPSEC',
//         REGN1='9M',
//         SPC1='50',
//         SPC2='20'
//***********************************************************
//* MODIFICATION HISTORY
//* 08/08/2005 DURAND   ADD REGN=128M & SYSPRT1 TO FINALIST STEP
//**********************************************************************
//*   IEFBR14:  DELETES FILES
//***************************************************************
//DELT010 EXEC PGM=IEFBR14
//DD1      DD  DSN=&HLQ0..BATCH.RESCH.LETTER.INPUT,
//             DISP=(MOD,CATLG),UNIT=SYSDA,
//             SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//             DCB=(RECFM=FB,LRECL=785,BLKSIZE=27475)
//*
//************************************************************          
//*   NASB666:  EXTRACTS LETTERS RECORDS                                
//**********************************************************            
//EXTR030 EXEC PGM=NATB030,REGION=&REGN1,                               
//        PARM='SYS=&NAT'                                               
//SYSPRINT DD  SYSOUT=&JCLO                                             
//SYSOUT   DD  SYSOUT=&JCLO                                             
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//DDPRINT  DD  SYSOUT=&JCLO                                             
//CMPRINT  DD  SYSOUT=(&REPT,NA7030Q2)                                  
//SYSUDUMP DD  SYSOUT=&DMPS                                             
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR            
//         DD  DSN=&LIB1..PARMLIB(P7030NA2),DISP=SHR                    
//CMWKF01  DD DSN=&HLQ0..BATCH.RESCH.ACCURINT.VALID(&GDG0),DISP=SHR
//CMWKF03  DD DSN=&HLQ0..BATCH.RESCH.LETTER.INPUT,DISP=SHR              
//CMPRT01  DD SYSOUT=(&REPT,NA7030Q1)
//CMPRT02  DD SYSOUT=(&REPT,NA7030Q2)
//CMPRT03  DD SYSOUT=(&REPT,NA7030Q3)
//**************************************************************        
//*   NASB667:  LETTERS                                                 
//********************************************************              
//EXTR050 EXEC PGM=NATB030,REGION=&REGN1,                               
//        PARM='SYS=&NAT'                                               
//SYSPRINT DD  SYSOUT=&JCLO                                             
//SYSOUT   DD  SYSOUT=&JCLO                                             
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//DDPRINT  DD  SYSOUT=&JCLO                                             
//CMPRINT  DD  SYSOUT=(&REPT,NA7030Q4)                                  
//SYSUDUMP DD  SYSOUT=&DMPS                                             
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR            
//         DD  DSN=&LIB1..PARMLIB(P7030NA3),DISP=SHR                    
//CMWKF01  DD DSN=&HLQ0..BATCH.RESCH.LETTER.INPUT,DISP=SHR              
//CMWKF02  DD DSN=&HLQ0..BATCH.RESCH.LETTERS.OUTPUT(&GDG1),             
//             DISP=(,CATLG,DELETE),UNIT=SYSDA,
//             SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//             DCB=(MODLDSCB,LRECL=80,BLKSIZE=27920,RECFM=FB)
