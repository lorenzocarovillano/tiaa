//P5160IAM PROC DBID='003',
//         DMPS='U',
//         GDG0='+0',
//         HLQ0='PIA.ANN.IA',
//         JCLO='*',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='5M',
//         REPT='8'
//*
//* ----------------------------------------------------------------- * 
//* PROCEDURE:  P5160IAM                 DATE:     4/01/96            * 
//* ----------------------------------------------------------------- * 
//* PROGRAMS EXECUTED: IAAP365 (NATURAL)                                
//* ----------------------------------------------------------------- * 
//* APPLY AUTO COMBINE CHECK TRANSACTIONS TO IAIQ DATABASE FILE       *
//* ----------------------------------------------------------------- * 
//*                                                                     
//* MODIFICATION HISTORY
//* 04/12/02 - OIA JCL STANDARDS - PHIL STEINHAUSER
//*                                                                     
//**********************************************************************
//*
//EXTR010 EXEC PGM=NATB030,REGION=&REGN1,
//        PARM='SYS=&NAT'
//SYSPRINT  DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//DDPRINT   DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRINT   DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRT01   DD SYSOUT=(&REPT,IA5160M1)          AUTO COMBINE TRANS REPT
//SYSUDUMP  DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSABOUT  DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSDBOUT  DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSOUT    DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMWKF01   DD DSN=&HLQ0..IMDANNP.T46(&GDG0),DISP=OLD,UNIT=SYSDA        
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(P5160IA1),DISP=SHR          IAAP365
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
