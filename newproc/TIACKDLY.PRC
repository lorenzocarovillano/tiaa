//TIACKDLY PROC VPREFX='POMPY.ONL',                                     
//         LPREFX='PROD.OMNIPAY.BAT',                                   
//         SPREFX='POMPY',                                              
//         VFSET='PMT0',                                                
//         INQDSN=INQHP,                       * TRX FILE W/DATES       
//         DEFCTL=DEFCKDLY,                    * DEL/DEF CARDS          
//         EXTTYPE=,                           * EXTRACT TYPE           
//         CKTYPE='.DAILY',                    * MINI-CK TYPE           
//         REGN1='0M',                                                  
//         SYSROUT='*',                                                 
//         GDG0='(+0)',                                                 
//         SPC1='50,50'                                                 
//*                                                                     
//* PROCEDURE:  TIACKDLY                                                
//*          :  OMNIPAY                                                 
//*          :  EXTRACT SUBSET OF CHECK MASTER FILE FOR DAILY           
//*             REPORTING PROCESSES                                     
//*          :  CREATED : 02/2013 - LIEDBERG                            
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
//* RERUN    : ALL STEPS RERUNNABLE                                     
//* RESTART  : FROM ANY STEP                                            
//* OUTPUT   : POMPY.PMT0.PMT0CK.EXTRACT                                
//*                                                                     
//* USAGE    : EXTRACT SUBSET OF CHECK MASTER FILE FOR DAILY            
//*          : REPORTING PROCESSES                                      
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
//*                                                                     
//* MODIFICATION HISTORY                                                
//*    2013/02/28   LIEDBERG       CREATION                             
//*    2014/03/21   C308169        SYMBOLIC PARAMETERS ADDED TO MAKE IT
//*                                GENERIC FOR MIDDAY JOBS              
//*-----------------------------------------------------------------*
//*UNAL01 UNALLOCATE ANY EXISTING WORK FILES
//*-----------------------------------------------------------------*
//UNAL01   EXEC PGM=IEFBR14
//EXTFILE  DD DSN=&SPREFX..&VFSET..PMT0CK.EXTRACT&EXTTYPE,              
//            DISP=(MOD,DELETE),UNIT=SYSDA,
//            SPACE=(TRK,(1,1),RLSE)
//********************************************************************* 
//* EXTR02  - READ CURRENT DAY'S TRANSACTIONS TO OBTAIN DATE            
//*           RANGE;  CREATE EXTRACT FILE WITH CK RECORDS               
//*           FOR THIS DATE RANGE                                       
//*******************************************************************   
//EXTR02  EXEC PGM=TIACKDLY,REGION=&REGN1,COND=(0,NE)                   
//SYSUDUMP DD SYSOUT=U
//SYSPRINT DD SYSOUT=&SYSROUT
//SYSOUT   DD SYSOUT=&SYSROUT
//*                                                                     
//CTLLIB    DD DSN=&SPREFX..CDTE&VFSET,DISP=SHR                         
//PMT0CK    DD DISP=SHR,DSN=&VPREFX..&VFSET.CK,AMP=('ACCBIAS=SO')       
//EDTTRANS  DD DISP=SHR,DSN=&SPREFX..&VFSET..&INQDSN&GDG0               
//EXTFILE   DD DSN=&SPREFX..&VFSET..PMT0CK.EXTRACT&EXTTYPE,             
//             DISP=(NEW,CATLG,DELETE),UNIT=SYSDA,                      
//             DATACLAS=DCPSEXTC,                                       
//             SPACE=(CYL,(&SPC1),RLSE)                                 
//*                                                                     
//*******************************************************************   
//* DELD03  - DEL/DEF THE MINI-CK MASTER FILE                           
//*******************************************************************   
//DELD03   EXEC PGM=IDCAMS                                              
//SYSPRINT DD  SYSOUT=*                                                 
//SYSIN    DD DSN=&LPREFX..CTL(&DEFCTL),DISP=SHR                        
//*                                                                     
//*******************************************************************   
//* COPY04  - COPY EXTRACT FILE WITH TODAY'S CK RECORDS INTO            
//*           MINI-CK MASTER FILE                                       
//*******************************************************************   
//COPY04   EXEC PGM=SORT                                                
//SYSOUT   DD SYSOUT=*                                                  
//SORTIN   DD DSN=&SPREFX..&VFSET..PMT0CK.EXTRACT&EXTTYPE,              
//         DISP=SHR                                                     
//SORTOUT  DD  DSN=&VPREFX..&VFSET.CK&CKTYPE,                           
//         DISP=SHR                                                     
//SYSIN    DD  DSN=&LPREFX..CTL(SORTCPY),DISP=SHR                       
