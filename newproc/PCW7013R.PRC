//PCW7013R PROC DBID=045,                                               
//         DMPS='U',                                                    
//         JCLO='*',                                                    
//         LIB1='PROD',                                                 
//         LIB2='PROD.NT2LOGON',                                        
//         NAT='NAT045P',                                               
//         NATMEM='PANNSEC',                                            
//         REGN1='5M'                                                   
//**********************************************************************
//*        CWF PIN EXPANSION - ADHOC DATA FIX                           
//**********************************************************************
//UPDT010  EXEC PGM=NATB030,REGION=&REGN1,                              
//         PARM='SYS=&NAT'                                              
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                   
//SYSPRINT DD SYSOUT=&JCLO                                              
//SYSOUT   DD SYSOUT=&JCLO                                              
//SYSUDUMP DD SYSOUT=&DMPS                                              
//DDPRINT  DD SYSOUT=&JCLO                                              
//CMPRINT  DD SYSOUT=&JCLO                                              
//CMSYNIN  DD DSN=&LIB2..PARMLIB(&NATMEM),DISP=SHR                      
//         DD DSN=&LIB1..PARMLIB(CW700D13),DISP=SHR                     
//*                                                                     
