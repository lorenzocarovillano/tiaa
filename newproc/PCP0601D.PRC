//PCP0601D   PROC SYSOUT='*'
//*       REPORT='8',                                                   
//*       SYSMSGS='U'                                                   
//***************************************************************
//* COPY THE POSITIVE PAY CLEARED CHECK JPMORGAN
//***************************************************************
//COPY010  EXEC PGM=IEBGENER
//SYSPRINT DD SYSOUT=&SYSOUT
//SYSUT1   DD  DSN=PPDD.CPS.ANN.CLRD.CHECKS.CHASE,DISP=SHR
//SYSUT2   DD  DSN=PMS.ANN.PCP0601D.CLRD.CHKS.CHASE(+1),
//         DISP=(,CATLG,DELETE),
//         UNIT=SYSDA,SPACE=(CYL,(20,5))
//**       RECFM=FB,LRECL=67,BLKSIZE=100
//SYSIN    DD DUMMY
//*************************************************************
//* DELETE THE PPDD DSN                                      **
//*************************************************************
//DELT020  EXEC PGM=IEFBR14,COND=(0,NE)
//SYSPRINT DD  SYSOUT=&SYSOUT
//DD01     DD  DSN=PPDD.CPS.ANN.CLRD.CHECKS.CHASE,
//          DISP=(OLD,DELETE,DELETE)
