//* ------------------------------------------                          
//PNI9600D PROC DBID=045,                                               
//       NAT=NAT003P,                                                   
//       NATLIB=PANNSEC,                                                
//       LIB1=PROD,                                                     
//       LIB2=PROD.NT2LOGON,                                            
//       LIB3=PROD,                                                     
//       HLQ1=POMPL.FA,                                                 
//       HLQ2=PPDD.ANN,                                                 
//       HLQ3=PPM.ANN,                                                  
//       SPC1=50,                                                       
//       SPC2=10,                                                       
//       SPC3='5,2',                                                    
//       JCLO='*',                                                      
//       DMPS=U,                                                        
//       REPT=8,                                                        
//       REGN1=4M,                                                      
//       GDG=0,                                                         
//       GDG2=+1                                                        
//*                                                                     
//*********************************************************************
//* MODIFIED  DESCRIPTION                                             *
//* --------  ----------------------------------------------------    *
//* 22/08/14  ADDED NEW SEGMENTED FILES FOR OMNI EXPANSION PROJECT    *
//* CCID - 351129  09/01/2015   RECORD LENGTH OF CMWKF02 FROM 120 TO  *
//*                             160 FOR BENE IN THE PLAN              *
//*********************************************************************
//* --------------------------------------------------------------------
//* DELT010  DELETES FILES CREATED PREVIOUSLY IN STEP 'SORT020'         
//* --------------------------------------------------------------------
//DELT010 EXEC PGM=IEFBR14                                              
//DD01    DD DSN=&HLQ2..SCIB9600.SORTOUT,                               
//           DISP=(MOD,DELETE),                                         
//           UNIT=SYSDA,SPACE=(CYL,0)                                   
//*                                                                     
//DD02    DD DSN=&HLQ2..SCIB9600.SORTCAI,                               
//           DISP=(MOD,DELETE),                                         
//           UNIT=SYSDA,SPACE=(CYL,0)                                   
//*                                                                     
//* --------------------------------------------------------------------
//* SORT020 - THIS STEP WILL EXTRACT ALL THE T051, T850, T851,          
//* T813 (PH665) AND T801 CHG (PH665) FROM THE OMNIPLUS BASEEXT FILE    
//* --------------------------------------------------------------------
//SORT020  EXEC PGM=SORT,REGION=9M,COND=(0,NE)                          
//SYSOUT   DD SYSOUT=*                                                  
//SORTIN   DD  DSN=&HLQ1..SEG01.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG02.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG03.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG04.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG05.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG06.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG07.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG08.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG09.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG10.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG11.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG12.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG13.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG14.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG15.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG16.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG17.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG18.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG19.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG20.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG21.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG22.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG23.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG24.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG25.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG26.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG27.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG28.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG29.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG30.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG31.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG32.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..MDAY.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..P4QT.BASEEXT(&GDG),DISP=SHR
//SORTOUT  DD DSN=&HLQ2..SCIB9600.SORTOUT,                              
//            DISP=(NEW,CATLG,DELETE),                                  
//            UNIT=SYSDA,DCB=*.SORTIN,                                  
//            DATACLAS=DCPSEXTC,                                        
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE)                            
//SYSIN    DD DSN=&LIB3..PARMLIB(NI9600D1),DISP=SHR                     
//*                                                                     
//* --------------------------------------------------------------------
//* SORT025 - THIS STEP WILL EXTRACT ALL T813 THAT UPDATES PH813        
//* REMOVE DUP RECORDS FOR MULTIPLE PH OCCURENCES SINCE MDM UPDATES     
//* AT THE CLIENT/PIN LEVEL. DUPLICATE BASED ON SSN AND CLIENT ID.      
//* --------------------------------------------------------------------
//SORT025  EXEC PGM=SORT,REGION=9M,COND=(0,NE)                          
//SYSOUT   DD SYSOUT=*                                                  
//SORTIN   DD  DSN=&HLQ1..SEG01.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG02.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG03.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG04.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG05.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG06.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG07.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG08.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG09.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG10.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG11.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG12.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG13.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG14.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG15.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG16.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG17.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG18.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG19.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG20.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG21.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG22.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG23.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG24.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG25.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG26.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG27.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG28.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG29.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG30.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG31.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..SEG32.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..MDAY.BASEEXT(&GDG),DISP=SHR
//         DD  DSN=&HLQ1..P4QT.BASEEXT(&GDG),DISP=SHR
//SORTOUT  DD DSN=&HLQ2..SCIB9600.SORTCAI,                              
//            DISP=(NEW,CATLG,DELETE),                                  
//            UNIT=SYSDA,DCB=*.SORTIN,                                  
//            DATACLAS=DCPSEXTC,                                        
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE)                            
//SYSIN    DD DSN=&LIB3..PARMLIB(NI9600D3),DISP=SHR                     
//*                                                                     
//* ------------------------------------------------------------------- 
//* THIS JOB WILL EXECUTE SCIB9600 NATURAL PROGRAM TO READ A FILE OF    
//* ALL THE T051, T850, T851, T813 (PH665) AND T801 CHG (PH665)         
//* TRANSACTIONS DONE IN OMNIPLUS.  THE PROGRAM WILL UPDATE THE ACIS    
//* FILES AND WILL ALSO CREATE THE MDM INTERFACE FILE.                  
//* ------------------------------------------------------------------- 
//EXTR030  EXEC PGM=NATB030,COND=(1,LT,SORT020),REGION=&REGN1,          
//  PARM='OBJIN=N,IM=D,SYS=&NAT,MADIO=0,MT=0,INTENS=1,OPRB=(.ALL)'      
//*                                                                     
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//SYSPRINT DD  SYSOUT=&JCLO                                             
//DDPRINT  DD  SYSOUT=&JCLO                                             
//SYSOUT   DD  SYSOUT=&JCLO                                             
//SYSUDUMP DD  SYSOUT=&DMPS                                             
//SYSABOUT DD  SYSOUT=&DMPS                                             
//CMPRINT  DD  SYSOUT=&JCLO                                             
//CMWKF01  DD  DSN=&HLQ2..SCIB9600.SORTOUT,UNIT=SYSDA,DISP=SHR          
//CMWKF02  DD  DSN=&HLQ3..PNI9600D.MDM.INTFILE(&GDG2),                  
//             DISP=(,CATLG,CATLG),DATACLAS=DCPSEXTC,                   
//             UNIT=SYSDA,                                              
//             SPACE=(CYL,(&SPC3),RLSE),                                
//             DCB=(RECFM=FB,LRECL=160,BLKSIZE=0)                       
//CMPRT01  DD  SYSOUT=(&REPT,NI9600D1)                                  
//CMPRT02  DD  SYSOUT=(&REPT,NI9600D2)                                  
//CMSYNIN  DD  DSN=&LIB2..PARMLIB(&NATLIB),DISP=SHR                     
//         DD  DSN=&LIB3..PARMLIB(NI9600D2),DISP=SHR                    
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                     
//EXTR040  EXEC PGM=PSG1250,COND=(0,LT),REGION=&REGN1                   
//*                                                                     
//SYSPRINT DD  SYSOUT=&JCLO                                             
//SYSOUT   DD  SYSOUT=&JCLO                                             
//SYSUDUMP DD  SYSOUT=&DMPS                                             
//SYSABOUT DD  SYSOUT=&DMPS                                             
//SYS010   DD  DSN=&HLQ2..SCIB9600.SORTOUT,UNIT=SYSDA,DISP=SHR          
//         DD  DSN=&HLQ2..SCIB9600.SORTCAI,UNIT=SYSDA,DISP=SHR          
//SYS011   DD  DSN=&HLQ3..PNI9600D.MDM.INTFILE(&GDG2),                  
//             DISP=(MOD,CATLG,CATLG),DATACLAS=DCPSEXTC,                
//             UNIT=SYSDA,                                              
//             SPACE=(CYL,(&SPC3),RLSE),                                
//             DCB=(RECFM=FB,LRECL=160,BLKSIZE=0)                       
//REJTRPRT  DD SYSOUT=(&REPT,NI9600D3)
//ACCTRPRT  DD SYSOUT=(&REPT,NI9600D4)
//*                                                                     
