//P3020CPM PROC SYSOUT='*',
//*       REPORT='8',                                                   
//        SYSMSGS='U'                                                   
//*--------------------------------------------------------------------
//*
//* FCPP925 : DELETES ALL ARCHIVED PAYMENT RECORDS FROM
//*           DATA BASE "003" FILE "196" FCP-CONS-PYMNT.
//*           THIS JOB IS TRIGGERED BY THE COMPLETION OF
//*           JOB "P3010CPM" IN "DENVER".
//*--------------------------------------------------------------------
//*
//*--------------------------------------------------------------------
//*   R E S T A R T  NOTE:
//*   ~~~~~~~~~~~~~~~~~~~
//*
//* THIS JOB IS COMPLETELY RESTARTABLE / RERUNABLE.
//*
//*--------------------------------------------------------------------
//DELT010 EXEC PGM=NATB030,REGION=9M,
//        PARM='SYS=NAT003P'
//STEPLIB   DD DSN=PROD.BATCH.LOADLIB,DISP=SHR
//          DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//SYSOUT    DD SYSOUT=&SYSOUT
//SYSUDUMP  DD SYSOUT=&SYSMSGS
//SYSPRINT  DD SYSOUT=&SYSOUT
//DDPRINT   DD SYSOUT=&SYSOUT
//DDCARD    DD DSN=PROD.PARMLIB(DBAPP003),DISP=SHR
//CMPRINT   DD SYSOUT=&SYSOUT
//CMPRT01   DD SYSOUT=&SYSOUT
//CMPRT02   DD SYSOUT=&SYSOUT
//CMPRT03   DD SYSOUT=&SYSOUT
//CMPRT15   DD SYSOUT=&SYSOUT
//CMWKF01   DD DSN=PCPS.ANN.P3000CPM.CPS.FILE196.DELETE.ISNS,DISP=SHR
//CMSYNIN   DD DSN=PROD.NT2LOGON.PARMLIB(PANNSEC),DISP=SHR
//          DD DSN=PROD.PARMLIB(P3020CPA),DISP=SHR
