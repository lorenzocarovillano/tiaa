//PCP2255R PROC DBID='003',
//         DMPS='U',
//         HLQ0='PIA.ANN',
//         JCLO='*',
//         REGN1='9M',
//         JOBNM='PCP2255R',
//         JOBNM2='P2270CPM',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC'
//*
//* ------------------------------------------------------------------*
//* PROCEDURE:  PCP2255R                 DATE:  04/09/2012            *
//* ------------------------------------------------------------------*
//* EXTR010 - PRODUCE A POSITIVE PAY TEXT FILE FOR BANK TRANSMISSION. *
//*           THIS JOB IS AN INTERIM SOLUTION UNTIL WELLS FARGO
//*           CORRECTS ITS HANDLING OF FUTURE DATED PAYMENTS          *
//*           NATURAL PROGRAM "FCPP821R".                             *
//********************************************************************* 
//*
//* EXTR010 - PRODUCE A POSITIVE PAY FILE FOR BANK TRANSMISSION.      *
//*           NATURAL PROGRAM "FCPP821".                              *
//*           THIS PROGRAM CONTAINS AN INTERNAL SORT.                 *
//*           THE OUTPUT FILE IS SORTED BY PAYMENT CHECK NUMBER.      *
//*
//*-------------------------------------------------------------------*
//*
//EXTR010 EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//        PARM='IM=D,SYS=&NAT'
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSPRINT DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//SYSOUT   DD SYSOUT=&JCLO
//DDPRINT  DD SYSOUT=&JCLO
//CMPRINT  DD SYSOUT=&JCLO
//CMWKF01  DD DSN=&HLQ0..&JOBNM2..POSPAY.S478,
//            DISP=SHR,BUFNO=25
//CMWKF02  DD DSN=&HLQ0..&JOBNM..POSPAY.TEXT,DISP=SHR,BUFNO=25
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(CP2255R1),DISP=SHR
