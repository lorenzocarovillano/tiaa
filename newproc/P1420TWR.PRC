//P1420TWR PROC                                                         
//*------------*                                                        
//* EXTRACTS EXPIRED DATES (FORM 1001)                                  
//* FROM PARTICIPANT DATABASE DB003/095                                 
//* PRINTS PARTICIPANTS WITH EXPIRED DATES (FORM 1001)                  
//EXTR010  EXEC PGM=NATB030,REGION=8M,                                  
//         PARM='SYS=NAT003P'                                           
//DDCARD   DD  DSN=PROD.PARMLIB(DBAPP003),DISP=SHR                      
//STEPLIB  DD  DSN=PROD.BATCH.LOADLIB,DISP=SHR                          
//         DD  DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR               
//CMWKF01  DD  DSN=PPDD.TAX.P1420TWR.PART.F1001.EXT,UNIT=SYSDA,         
//             DISP=(NEW,CATLG,DELETE),                                 
//             SPACE=(CYL,(4,2),RLSE),                                  
//*//             DCB=RECFM=FB,LRECL=53                                    
//             DCB=(RECFM=FB,LRECL=53)
//SYSPRINT DD  SYSOUT=*                                                 
//DDPRINT  DD  SYSOUT=*                                                 
//SYSOUT   DD  SYSOUT=*                                                 
//SYSUDUMP DD  SYSOUT=U                                                 
//CMPRINT  DD  SYSOUT=*                                                 
//CMPRT01  DD  SYSOUT=8                                                 
//CMPRT02  DD  SYSOUT=8                                                 
//CMSYNIN  DD  DSN=PROD.NT2LOGON.PARMLIB(PANNSEC),DISP=SHR              
//         DD  DSN=PROD.PARMLIB(P1420TW1),DISP=SHR                      
//         DD  DSN=PROD.PARMLIB(FINPARM),DISP=SHR                       
//*------------*                                                        
//* SORT EXTRACT BY TIN                                                 
//*------------*                                                        
//SORT020  EXEC PGM=SORT,REGION=8M,COND=(0,NE)                          
//STEPLIB  DD  DSN=PROD.BATCH.LOADLIB,DISP=SHR                          
//         DD  DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR               
//SORTWK01 DD  UNIT=SYSDA,SPACE=(CYL,(10,5),RLSE)                       
//SORTWK02 DD  UNIT=SYSDA,SPACE=(CYL,(10,5),RLSE)                       
//SORTWK03 DD  UNIT=SYSDA,SPACE=(CYL,(10,5),RLSE)                       
//SORTIN   DD  DSN=PPDD.TAX.P1420TWR.PART.F1001.EXT,DISP=SHR            
//SORTOUT  DD  DSN=PPDD.TAX.P1420TWR.PART.F1001.EXT.SORTED,             
//             DISP=(NEW,CATLG,DELETE),                                 
//             UNIT=SYSDA,                                              
//             SPACE=(CYL,(4,2),RLSE),                                  
//*//             DCB=RECFM=FB,LRECL=53                                    
//             DCB=(RECFM=FB,LRECL=53)
//SYSIN    DD  DSN=PROD.PARMLIB(P1420TW2),DISP=SHR                      
//*------------*                                                        
//* CREATES A REPORT OF PARTICIPANTS EXTRACTED                          
//*------------*                                                        
//REPT030  EXEC PGM=NATB030,REGION=8M,COND=(0,NE),                      
//         PARM='SYS=NAT003P'                                           
//STEPLIB  DD  DSN=PROD.BATCH.LOADLIB,DISP=SHR                          
//         DD  DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR               
//DDCARD   DD  DSN=PROD.PARMLIB(DBAPP003),DISP=SHR                      
//CMWKF01  DD  DSN=PPDD.TAX.P1420TWR.PART.F1001.EXT.SORTED,DISP=SHR     
//SYSPRINT DD  SYSOUT=*                                                 
//DDPRINT  DD  SYSOUT=*                                                 
//SYSOUT   DD  SYSOUT=*                                                 
//SYSUDUMP DD  SYSOUT=U                                                 
//CMPRINT  DD  SYSOUT=*                                                 
//CMPRT01  DD  SYSOUT=8                                                 
//CMPRT02  DD  SYSOUT=8                                                 
//CMSYNIN  DD  DSN=PROD.NT2LOGON.PARMLIB(PANNSEC),DISP=SHR              
//         DD  DSN=PROD.PARMLIB(P1420TW3),DISP=SHR                      
//         DD  DSN=PROD.PARMLIB(FINPARM),DISP=SHR                       
