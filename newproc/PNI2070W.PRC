//PNI2070W PROC DBID=003,                                               
//         LIB1='PROD',                                                 
//         LIB2='PROD',                                                 
//         HLQ1='PPM.ANN',                                              
//         HLQ8='PPDD.ANN',                                             
//         GDG0='+0',                                                   
//         GDG1='+1',                                                   
//         NAT='NAT003P',                                               
//         NATMEM='PANNSEC',                                            
//         REGN1='9M',                                                  
//         SPC1='15,5',                                                 
//         DMPS='*',                                                    
//         JCLO='*'                                                     
//*                                                                     
//**********************************************************************
//* WEEKLY CONTRACT RANGE USAGE REPORTS                                *
//**********************************************************************
//*                                                                     
//**********************************************************************
//* DELETE WORK DATASETS                                               *
//**********************************************************************
//DELT010  EXEC PGM=IEFBR14,REGION=&REGN1                               
//DELETE1  DD DSN=&HLQ8..PNI2070W.DA.CURR.RANGE.EXT,                    
//            DISP=(MOD,DELETE,DELETE),                                 
//            DCB=(RECFM=FB,LRECL=80,BLKSIZE=0),                        
//            UNIT=(SYSDA,5),SPACE=(TRK,(&SPC1),RLSE)                   
//DELETE2  DD DSN=&HLQ8..PNI2070W.IA.CURR.RANGE.EXT,                    
//            DISP=(MOD,DELETE,DELETE),                                 
//            DCB=(RECFM=FB,LRECL=80,BLKSIZE=0),                        
//            UNIT=(SYSDA,5),SPACE=(TRK,(&SPC1),RLSE)                   
//DELETE3  DD DSN=&HLQ8..PNI2070W.DA.CURR.RANGE.SRT,                    
//            DISP=(MOD,DELETE,DELETE),                                 
//            DCB=(RECFM=FB,LRECL=80,BLKSIZE=0),                        
//            UNIT=(SYSDA,5),SPACE=(TRK,(&SPC1),RLSE)                   
//DELETE4  DD DSN=&HLQ8..PNI2070W.IA.CURR.RANGE.SRT,                    
//            DISP=(MOD,DELETE,DELETE),                                 
//            DCB=(RECFM=FB,LRECL=80,BLKSIZE=0),                        
//            UNIT=(SYSDA,5),SPACE=(TRK,(&SPC1),RLSE)                   
//DELETE5  DD DSN=&HLQ8..PNI2070W.DA.RANGE.USAGE,                       
//            DISP=(MOD,DELETE,DELETE),                                 
//            DCB=(RECFM=FB,LRECL=35,BLKSIZE=0),                        
//            UNIT=(SYSDA,5),SPACE=(TRK,(&SPC1),RLSE)                   
//DELETE6  DD DSN=&HLQ8..PNI2070W.IA.RANGE.USAGE,                       
//            DISP=(MOD,DELETE,DELETE),                                 
//            DCB=(RECFM=FB,LRECL=35,BLKSIZE=0),                        
//            UNIT=(SYSDA,5),SPACE=(TRK,(&SPC1),RLSE)                   
//*                                                                     
//**********************************************************************
//* NATURAL PROGRAM: APPB2070                                          *
//* WRITE IA/DA CNTR RANGE INFORMATION TO WORK FILES                   *
//**********************************************************************
//EXTR020  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),                  
//   PARM='SYS=&NAT,UDB=&DBID,OBJIN=N'                                  
//DDCARD   DD DSN=&LIB2..PARMLIB(DBAPP&DBID),DISP=SHR                   
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR             
//         DD DSN=&LIB1..PARMLIB(NI2070W1),DISP=SHR                     
//CMWKF01  DD DSN=&HLQ8..PNI2070W.DA.CURR.RANGE.EXT,                    
//            DISP=(NEW,CATLG,DELETE),                                  
//            DCB=(RECFM=FB,LRECL=80,BLKSIZE=0),                        
//            UNIT=(SYSDA,5),SPACE=(TRK,(&SPC1),RLSE),                  
//            DATACLAS=DCPSEXTC                                         
//CMWKF02  DD DSN=&HLQ8..PNI2070W.IA.CURR.RANGE.EXT,                    
//            DISP=(NEW,CATLG,DELETE),                                  
//            DCB=(RECFM=FB,LRECL=80,BLKSIZE=0),                        
//            UNIT=(SYSDA,5),SPACE=(TRK,(&SPC1),RLSE),                  
//            DATACLAS=DCPSEXTC                                         
//CMPRINT  DD SYSOUT=&JCLO                                              
//SYSPRINT DD SYSOUT=&JCLO                                              
//SYSOUT   DD SYSOUT=&JCLO                                              
//DDPRINT  DD SYSOUT=&DMPS                                              
//SYSUDUMP DD SYSOUT=U                                                  
//*                                                                     
//**********************************************************************
//* SORT DA CNTR RANGE EXTRACT FILE BY PRODUCT CODE                    *
//**********************************************************************
//SORT030  EXEC PGM=SORT,REGION=&REGN1,COND=(0,NE)                      
//SORTIN   DD DSN=&HLQ8..PNI2070W.DA.CURR.RANGE.EXT,DISP=SHR            
//SORTOUT  DD DSN=&HLQ8..PNI2070W.DA.CURR.RANGE.SRT,                    
//            DISP=(NEW,CATLG,DELETE),                                  
//            DCB=(RECFM=FB,LRECL=80,BLKSIZE=0),                        
//            UNIT=(SYSDA,5),SPACE=(TRK,(&SPC1),RLSE),                  
//            DATACLAS=DCPSEXTC                                         
//SYSIN    DD DSN=&LIB1..PARMLIB(NI2070W2),DISP=SHR                     
//SYSOUT   DD SYSOUT=&JCLO                                              
//*                                                                     
//**********************************************************************
//* SORT IA CNTR RANGE EXTRACT FILE BY PRODUCT CODE                    *
//**********************************************************************
//SORT040  EXEC PGM=SORT,REGION=&REGN1,COND=(0,NE)                      
//SORTIN   DD DSN=&HLQ8..PNI2070W.IA.CURR.RANGE.EXT,DISP=SHR            
//SORTOUT  DD DSN=&HLQ8..PNI2070W.IA.CURR.RANGE.SRT,                    
//            DISP=(NEW,CATLG,DELETE),                                  
//            DCB=(RECFM=FB,LRECL=80,BLKSIZE=0),                        
//            UNIT=(SYSDA,5),SPACE=(TRK,(&SPC1),RLSE),                  
//            DATACLAS=DCPSEXTC                                         
//SYSIN    DD DSN=&LIB1..PARMLIB(NI2070W2),DISP=SHR                     
//SYSOUT   DD SYSOUT=&JCLO                                              
//*                                                                     
//********************************************************************* 
//* NATURAL PROGRAM: APPB2071                                         * 
//* COMPARE CURRENT & PREVIOUS DA CONTRACT RANGE                      * 
//* PRINT REPORT OF DIFFERENCE                                        * 
//********************************************************************* 
//REPT050  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),                  
//   PARM='SYS=&NAT,UDB=&DBID,OBJIN=N'                                  
//DDCARD   DD DSN=&LIB2..PARMLIB(DBAPP&DBID),DISP=SHR                   
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR             
//         DD DSN=&LIB1..PARMLIB(NI2070W3),DISP=SHR                     
//CMWKF01  DD DSN=&HLQ1..PNI2070W.DA.PREV.RANGE.SRT(&GDG0),DISP=SHR     
//CMWKF02  DD DSN=&HLQ8..PNI2070W.DA.CURR.RANGE.SRT,DISP=SHR            
//CMWKF03  DD DSN=&HLQ8..PNI2070W.DA.RANGE.USAGE,                       
//            DISP=(NEW,CATLG,DELETE),                                  
//            DCB=(RECFM=FB,LRECL=35,BLKSIZE=0),                        
//            UNIT=(SYSDA,5),SPACE=(TRK,(&SPC1),RLSE),                  
//            DATACLAS=DCPSEXTC                                         
//CMPRINT  DD SYSOUT=&JCLO                                              
//CMPRT01  DD SYSOUT=&JCLO                                              
//CMPRT02  DD SYSOUT=&JCLO                                              
//CMPRT03  DD SYSOUT=&JCLO                                              
//CMPRT04  DD SYSOUT=&JCLO                                              
//CMPRT05  DD SYSOUT=&JCLO                                              
//SYSPRINT DD SYSOUT=&JCLO                                              
//SYSOUT   DD SYSOUT=&JCLO                                              
//DDPRINT  DD SYSOUT=&DMPS                                              
//SYSUDUMP DD SYSOUT=U                                                  
//*                                                                     
//**********************************************************************
//* NATURAL PROGRAM: APPB2071                                          *
//* COMPARE CURRENT & PREVIOUS IA CONTRACT RANGE                       *
//* PRINT REPORT OF DIFFERENCE                                         *
//**********************************************************************
//REPT060  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),                  
//   PARM='SYS=&NAT,UDB=&DBID,OBJIN=N'                                  
//DDCARD   DD DSN=&LIB2..PARMLIB(DBAPP&DBID),DISP=SHR                   
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR             
//         DD DSN=&LIB1..PARMLIB(NI2070W3),DISP=SHR                     
//CMWKF01  DD DSN=&HLQ1..PNI2070W.IA.PREV.RANGE.SRT(&GDG0),DISP=SHR     
//CMWKF02  DD DSN=&HLQ8..PNI2070W.IA.CURR.RANGE.SRT,DISP=SHR            
//CMWKF03  DD DSN=&HLQ8..PNI2070W.IA.RANGE.USAGE,                       
//            DISP=(NEW,CATLG,DELETE),                                  
//            DCB=(RECFM=FB,LRECL=35,BLKSIZE=0),                        
//            UNIT=(SYSDA,5),SPACE=(TRK,(&SPC1),RLSE),                  
//            DATACLAS=DCPSEXTC                                         
//CMPRINT  DD SYSOUT=&JCLO                                              
//CMPRT01  DD SYSOUT=&JCLO                                              
//CMPRT02  DD SYSOUT=&JCLO                                              
//CMPRT03  DD SYSOUT=&JCLO                                              
//CMPRT04  DD SYSOUT=&JCLO                                              
//CMPRT05  DD SYSOUT=&JCLO                                              
//SYSPRINT DD SYSOUT=&JCLO                                              
//SYSOUT   DD SYSOUT=&JCLO                                              
//DDPRINT  DD SYSOUT=&DMPS                                              
//SYSUDUMP DD SYSOUT=U                                                  
//*                                                                     
//**********************************************************************
//* COPY DA CURRENT WEEKLY EXTRACT TO PREVIOUS WEEKLY EXTRACT GDG      *
//* NOTE: IF THIS STEP ABENDS, RESTART THE JOB FROM THIS STEP          *
//**********************************************************************
//COPY070  EXEC PGM=IEBGENER,REGION=&REGN1,COND=(0,NE)                  
//SYSUT1   DD DSN=&HLQ8..PNI2070W.DA.CURR.RANGE.SRT,DISP=SHR            
//SYSUT2   DD DSN=&HLQ1..PNI2070W.DA.PREV.RANGE.SRT(&GDG1),             
//            DISP=(NEW,CATLG,DELETE),                                  
//            DCB=(RECFM=FB,LRECL=80,BLKSIZE=0),                        
//            UNIT=(SYSDA,5),SPACE=(TRK,(&SPC1),RLSE),                  
//            DATACLAS=DCPSEXTC                                         
//SYSIN    DD DUMMY                                                     
//SYSPRINT DD SYSOUT=&DMPS                                              
//*                                                                     
//**********************************************************************
//* COPY IA CURRENT WEEKLY EXTRACT TO PREVIOUS WEEKLY EXTRACT GDG      *
//* NOTE: IF THIS STEP ABENDS, RESTART THE JOB FROM THIS STEP          *
//**********************************************************************
//COPY080  EXEC PGM=IEBGENER,REGION=&REGN1,COND=(0,NE)                  
//SYSUT1   DD DSN=&HLQ8..PNI2070W.IA.CURR.RANGE.SRT,DISP=SHR            
//SYSUT2   DD DSN=&HLQ1..PNI2070W.IA.PREV.RANGE.SRT(&GDG1),             
//            DISP=(NEW,CATLG,DELETE),                                  
//            DCB=(RECFM=FB,LRECL=80,BLKSIZE=0),                        
//            UNIT=(SYSDA,5),SPACE=(TRK,(&SPC1),RLSE),                  
//            DATACLAS=DCPSEXTC                                         
//SYSIN    DD DUMMY                                                     
//SYSPRINT DD SYSOUT=&DMPS                                              
//*                                                                     
//**********************************************************************
//* COPY DA CONTRACT RANGE USAGE TO MONTHLY USAGE ACCUM FILE           *
//* THE DA ACCUM FILE WILL ACCUMULATE THE WEEKLY USAGE FOR THE MONTH   *
//* AND USED IN THE MONTLY JOB AND CLEARED OUT EACH MONTH              *
//* NOTE: IF THIS STEP ABENDS, RESTART THE JOB FROM THIS STEP          *
//**********************************************************************
//COPY090  EXEC PGM=IEBGENER,REGION=&REGN1,COND=(0,NE)                  
//SYSUT1   DD DSN=&HLQ8..PNI2070W.DA.RANGE.USAGE,DISP=SHR               
//SYSUT2   DD DSN=&HLQ1..PNI3040M.DA.USAGE.ACCUM,                       
//            DISP=(MOD,KEEP,KEEP),                                     
//            DCB=(RECFM=FB,LRECL=35,BLKSIZE=0),                        
//            UNIT=(SYSDA,5),SPACE=(TRK,(&SPC1),RLSE)                   
//SYSIN    DD DUMMY                                                     
//SYSPRINT DD SYSOUT=&DMPS                                              
//*                                                                     
//**********************************************************************
//* COPY IA CONTRACT RANGE USAGE TO MONTHLY USAGE ACCUM FILE           *
//* THE IA ACCUM FILE WILL ACCUMULATE THE WEEKLY USAGE FOR THE MONTH   *
//* AND USED IN THE MONTLY JOB AND CLEARED OUT EACH MONTH              *
//* NOTE: IF THIS STEP ABENDS, RESTART THE JOB FROM THIS STEP          *
//**********************************************************************
//COPY100  EXEC PGM=IEBGENER,REGION=&REGN1,COND=(0,NE)                  
//SYSUT1   DD DSN=&HLQ8..PNI2070W.IA.RANGE.USAGE,DISP=SHR               
//SYSUT2   DD DSN=&HLQ1..PNI3040M.IA.USAGE.ACCUM,                       
//            DISP=(MOD,KEEP,KEEP),                                     
//            DCB=(RECFM=FB,LRECL=35,BLKSIZE=0),                        
//            UNIT=(SYSDA,5),SPACE=(TRK,(&SPC1),RLSE)                   
//SYSIN    DD DUMMY                                                     
//SYSPRINT DD SYSOUT=&DMPS                                              
