//P1615CWD PROC DBID='045',
//         DMPS='U',
//         HLQ0='PCWF.COR',
//         JCLO='*',
//         JOBNM='P1615CWD',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         NAT='NAT045P',
//         NATMEM='PANNSEC',
//         REGN1='9M',
//         REGN2='5M',
//         SPC1='5,5',
//         SPC2='50,50'
//*
//**********************************************************************
//* THIS PROC EXTRACTS INFORMATION FROM MIT FOR REPORTING PURPOSES     *
//* ==============================================================     *
//* STEP1 INIT010 INITIALIZE ALL PARTICIPATING FILES                   *
//*                                                                    *
//* STEP2 EXTR020 EXTRACT REQUEST-LOG-DATE-TIME FOR THE "WINDOW"       *
//*       PROVIDED BY CONTROL RECORD AND CREATE WORK-FILE WHICH        *
//*       CONTAINS RQST-LOG-DTE-TME FIELD.                             *
//*                                                                    *
//* STEP3 SORT030 EXTERNALLY SORT INPUT FILE FROM PREVIOUS STEP WITH   *
//*       MERGE OPTION TO ELIMINATE DUPLICATES AND CREATE SORTED OUPUT *
//*       WORK-FILE.                                                   *
//*                                                                    *
//* STEP4 EXTR040 READ WORK-FILE FROM PREVIOUS STEP AND READ MIT RECS. *
//*       BY HISTORY KEY TO CREATE OUTPUT FILES:                       *
//*          1. WORK-REQUEST-ADD   (LRCL = 833)                        *
//*          2. WORK-REQUEST-UPD   (LRCL = 833)                        *
//*          3. UNIT-ACT-ADD       (LRCL = 140)                        *
//*          4. UNIT-ACT-UPD       (LRCL = 140)                        *
//*          5. EMPL-ACT-ADD       (LRCL = 140)                        *
//*          6. EMPL-ACT-UPD       (LRCL = 140)                        *
//*          7. STEP-ACT-ADD       (LRCL = 140)                        *
//*          8. STEP-ACT-UPD       (LRCL = 140)                        *
//*          9. INTR-ACT-ADD       (LRCL = 140)                        *
//*         10. INTR-ACT-UPD       (LRCL = 140)                        *
//*         11. EXTR-ACT-ADD       (LRCL = 140)                        *
//*         12. EXTR-ACT-UPD       (LRCL = 140)                        *
//*         13. ENRT-ACT-ADD       (LRCL = 140)                        *
//*         14. ENRT-ACT-UPD       (LRCL = 140)                        *
//*         15. CONTRACT-ADD       (LRCL = 040)                        *
//*         16. CONTRACT-UPD       (LRCL = 040)                        *
//*         17. C-STATUS-ADD       (LRCL = 121)                        *
//*         18. WORK-REQUEST-DEL   (LRCL = 028)                        *
//*         19. TOTAL              (LRCL = 317)                        *
//*         20. ADDITIONAL-WR-ADD  (LRCL = 033)                        *
//*         21. ADDITIONAL-WR-UPD  (LRCL = 033)                        *
//*         22. SMART-TLC-TABLE    (LRCL = 046)                        *
//*                                                                    *
//* STEP5  EXTR050   PGM=CWFB4020      UNIT-CODE TABLE                 *
//* STEP6  EXTR060   PGM=CWFB4021      ACTION RQST TABLE               *
//* STEP7  EXTR070   PGM=CWFB4022      LOB/CO/PRODUCT TABLE            *
//* STEP8  EXTR080   PGM=CWFB4023      MAJOR BUSINESS TABLE            *
//* STEP9  EXTR090   PGM=CWFB4024      SPECIFIC BUSINESS TABLE         *
//* STEP10 EXTR100   PGM=CWFB4025      EMPLOYEE SKILLS TABLE           *
//* STEP11 EXTR110   PGM=CWFB4026      UNIT STATUS TABLE               *
//* STEP12 EXTR120   PGM=CWFB4027      UNIT WORK TABLE                 *
//* STEP13 EXTR130   PGM=CWFB4028      STEP SEQUENCE TABLE             *
//* STEP14 EXTR140   PGM=CWFB4029      WORK PROCESS TABLE              *
//* STEP15 EXTR150   PGM=CWFB4030      ROUTE SEQUENCE TABLE            *
//* STEP16 EXTR160   PGM=CWFB4031      EMPLOYEE POST TABLE             *
//* STEP17 EXTR170   PGM=CWFB4033      DIVISION TABLE                  *
//* STEP18 EXTR180   PGM=CWFB4034      WORK GROUP TABLE                *
//* STEP19 EXTR190   PGM=CWFB4035      DOCUMENT TYPE TABLE             *
//* STEP20 EXTR200   PGM=CWFB4036      IMAGE ENABLED WORKSTATION TBL   *
//*                                                                     
//**********************************************************************
//*                                                                     
//* MODIFICATION HISTORY
//* 04/16/02 - OIA JCL STANDARDS - PHIL STEINHAUSER
//*                                                                     
//**********************************************************************
//*    INITIALIZE WORK FILES                                           *
//**********************************************************************
//INIT010  EXEC PGM=IDCAMS
//DD1      DD  DUMMY
//DD2      DD DSN=&HLQ0..&JOBNM..RLDT.UNSORT.EXT,
//         DISP=OLD
//*
//DD3      DD  DUMMY
//DD4      DD DSN=&HLQ0..&JOBNM..RLDT.SORTED.EXT,
//         DISP=OLD
//*
//DD5      DD  DUMMY
//DD6      DD DSN=&HLQ0..&JOBNM..WRKRQST.ADD.EXT,
//         DISP=OLD
//*
//DD7      DD  DUMMY
//DD8      DD DSN=&HLQ0..&JOBNM..WRKRQST.UPD.EXT,
//         DISP=OLD
//*
//DD9      DD  DUMMY
//DD10     DD DSN=&HLQ0..&JOBNM..WRKRQST.DEL.EXT,
//         DISP=OLD
//*
//DD11     DD  DUMMY
//DD12     DD DSN=&HLQ0..&JOBNM..UNITACT.ADD.EXT,
//         DISP=OLD
//*
//DD13     DD  DUMMY
//DD14     DD DSN=&HLQ0..&JOBNM..UNITACT.UPD.EXT,
//         DISP=OLD
//*
//DD15     DD  DUMMY
//DD16     DD DSN=&HLQ0..&JOBNM..EMPLACT.ADD.EXT,
//         DISP=OLD
//*
//DD17     DD  DUMMY
//DD18     DD DSN=&HLQ0..&JOBNM..EMPLACT.UPD.EXT,
//         DISP=OLD
//*
//DD19     DD  DUMMY
//DD20     DD DSN=&HLQ0..&JOBNM..STEPACT.ADD.EXT,
//         DISP=OLD
//*
//DD21     DD  DUMMY
//DD22     DD DSN=&HLQ0..&JOBNM..STEPACT.UPD.EXT,
//         DISP=OLD
//*
//DD23     DD  DUMMY
//DD24     DD DSN=&HLQ0..&JOBNM..INTRACT.ADD.EXT,
//         DISP=OLD
//*
//DD25     DD  DUMMY
//DD26     DD DSN=&HLQ0..&JOBNM..INTRACT.UPD.EXT,
//         DISP=OLD
//*
//DD27     DD  DUMMY
//DD28     DD DSN=&HLQ0..&JOBNM..EXTRACT.ADD.EXT,
//         DISP=OLD
//*
//DD29     DD  DUMMY
//DD30     DD DSN=&HLQ0..&JOBNM..EXTRACT.UPD.EXT,
//         DISP=OLD
//*
//DD31     DD  DUMMY
//DD32     DD DSN=&HLQ0..&JOBNM..ENRTACT.ADD.EXT,
//         DISP=OLD
//*
//DD33     DD  DUMMY
//DD34     DD DSN=&HLQ0..&JOBNM..ENRTACT.UPD.EXT,
//         DISP=OLD
//*
//DD35     DD  DUMMY
//DD36     DD DSN=&HLQ0..&JOBNM..CONTRCT.ADD.EXT,
//         DISP=OLD
//*
//DD37     DD  DUMMY
//DD38     DD DSN=&HLQ0..&JOBNM..CONTRCT.UPD.EXT,
//         DISP=OLD
//*
//DD39     DD  DUMMY
//DD40     DD DSN=&HLQ0..&JOBNM..CSTATUS.ADD.EXT,
//         DISP=OLD
//*
//DD41     DD  DUMMY
//DD42     DD DSN=&HLQ0..&JOBNM..TOT01.CONTROL,
//         DISP=OLD
//*
//DD43     DD  DUMMY
//DD44     DD DSN=&HLQ0..&JOBNM..TOT02.CONTROL,
//         DISP=OLD
//*
//DD45     DD  DUMMY
//DD46     DD DSN=&HLQ0..&JOBNM..TOT03.CONTROL,
//         DISP=OLD
//*
//DD47     DD  DUMMY
//DD48     DD DSN=&HLQ0..&JOBNM..TOT04.CONTROL,
//         DISP=OLD
//*
//DD49     DD  DUMMY
//DD50     DD DSN=&HLQ0..&JOBNM..ADTNL.WR.ADD.EXT,
//         DISP=OLD
//*
//DD51     DD  DUMMY
//DD52     DD DSN=&HLQ0..&JOBNM..ADTNL.WR.UPD.EXT,
//         DISP=OLD
//*
//DD53     DD  DUMMY
//DD54     DD DSN=&HLQ0..&JOBNM..RELATION.TBL,
//         DISP=OLD
//*
//DD55     DD  DUMMY
//DD56     DD DSN=&HLQ0..&JOBNM..EFMCABNT.TBL,
//         DISP=OLD
//*
//DD57     DD  DUMMY
//DD58     DD DSN=&HLQ0..&JOBNM..WPID.IND.TBL,
//         DISP=OLD
//*
//DD59     DD  DUMMY
//DD60     DD DSN=&HLQ0..&JOBNM..SMART.RLDT.UNSORT,
//         DISP=OLD
//*
//DD61     DD  DUMMY
//DD62     DD DSN=&HLQ0..&JOBNM..SMART.TLC.UNSORT,
//         DISP=OLD
//*
//DD63     DD  DUMMY
//DD64     DD DSN=&HLQ0..&JOBNM..SMART.TLC.SORTED,
//         DISP=OLD
//*
//SYSPRINT DD  SYSOUT=&JCLO
//SYSIN    DD  DSN=&LIB1..PARMLIB(P1615CWA),DISP=SHR
//* --------------------------------------------------------------------
//* STEP2 EXTR020                                                      *
//* --------------------------------------------------------------------
//* CWFB4810 - READ MIT FILE AND COLLECTS REQUEST-LOG-DTE-TME WHICH
//*            FALLS INTO "WINDOW" PROVIDED BY CONTROL RECORD.
//*            (ONE - FOR REGULAR PART. AND ONE FOR "SMART" PART.)
//* --------------------------------------------------------------------
//EXTR020  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//         PARM='SYS=&NAT'
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSPRINT DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMPRT01  DD  SYSOUT=&JCLO
//CMWKF01  DD  DSN=&HLQ0..&JOBNM..RLDT.UNSORT.EXT,
//             DISP=SHR,
//             UNIT=SYSDA,
//             SPACE=(CYL,(&SPC1),RLSE),
//             RECFM=FB,LRECL=15
//CMWKF02  DD  DSN=&HLQ0..&JOBNM..SMART.RLDT.UNSORT,
//             DISP=SHR,
//             UNIT=SYSDA,
//             SPACE=(CYL,(&SPC1),RLSE),
//             RECFM=FB,LRECL=15
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P1615CWB),DISP=SHR
//*
//* --------------------------------------------------------------------
//* STEP3 SORT003                                                      *
//* --------------------------------------------------------------------
//* SORT     - READ SORTED WORK FILE (REQUEST-LOG-DTE-TME) & READS MIT *
//*            TO CREATE WR & ACTIVITY FILES FOR SQL SERVER.           *
//* --------------------------------------------------------------------
//SORT030  EXEC PGM=SORT,REGION=&REGN1,COND=(0,NE)
//SYSOUT   DD  SYSOUT=&JCLO
//SYSPRINT DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//SORTWK01 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC2))
//SORTWK02 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC2))
//SORTWK03 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC2))
//SORTIN   DD  DSN=&HLQ0..&JOBNM..RLDT.UNSORT.EXT,
//             DISP=SHR,
//             UNIT=SYSDA,
//             RECFM=FB,LRECL=15
//SORTOUT  DD  DSN=&HLQ0..&JOBNM..RLDT.SORTED.EXT,
//             DISP=SHR,
//             UNIT=SYSDA,
//             SPACE=(CYL,(&SPC1),RLSE),
//             RECFM=FB,LRECL=15
//SYSIN    DD  DSN=&LIB1..PARMLIB(P1615CWC),DISP=SHR
//*
//* --------------------------------------------------------------------
//* STEP4 EXTR040                                                      *
//* --------------------------------------------------------------------
//* CWFB4815 - READ SORTED WORK FILE (REQUEST-LOG-DTE-TME) & READS MIT *
//*            TO CREATE WR & ACTIVITY FILES FOR SQL SERVER.           *
//* --------------------------------------------------------------------
//EXTR040  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//         PARM='SYS=&NAT'
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSPRINT DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMPRT01  DD  SYSOUT=&JCLO
//CMWKF01  DD  DSN=&HLQ0..&JOBNM..RLDT.SORTED.EXT,DISP=SHR
//CMWKF02  DD  DSN=&HLQ0..&JOBNM..WRKRQST.ADD.EXT,DISP=SHR
//CMWKF03  DD  DSN=&HLQ0..&JOBNM..WRKRQST.UPD.EXT,DISP=SHR
//CMWKF04  DD  DSN=&HLQ0..&JOBNM..WRKRQST.DEL.EXT,DISP=SHR
//CMWKF05  DD  DSN=&HLQ0..&JOBNM..UNITACT.ADD.EXT,DISP=SHR
//CMWKF06  DD  DSN=&HLQ0..&JOBNM..UNITACT.UPD.EXT,DISP=SHR
//CMWKF07  DD  DSN=&HLQ0..&JOBNM..EMPLACT.ADD.EXT,DISP=SHR
//CMWKF08  DD  DSN=&HLQ0..&JOBNM..EMPLACT.UPD.EXT,DISP=SHR
//CMWKF09  DD  DSN=&HLQ0..&JOBNM..STEPACT.ADD.EXT,DISP=SHR
//CMWKF10  DD  DSN=&HLQ0..&JOBNM..STEPACT.UPD.EXT,DISP=SHR
//CMWKF11  DD  DSN=&HLQ0..&JOBNM..INTRACT.ADD.EXT,DISP=SHR
//CMWKF12  DD  DSN=&HLQ0..&JOBNM..INTRACT.UPD.EXT,DISP=SHR
//CMWKF13  DD  DSN=&HLQ0..&JOBNM..EXTRACT.ADD.EXT,DISP=SHR
//CMWKF14  DD  DSN=&HLQ0..&JOBNM..EXTRACT.UPD.EXT,DISP=SHR
//CMWKF15  DD  DSN=&HLQ0..&JOBNM..ENRTACT.ADD.EXT,DISP=SHR
//CMWKF16  DD  DSN=&HLQ0..&JOBNM..ENRTACT.UPD.EXT,DISP=SHR
//CMWKF17  DD  DSN=&HLQ0..&JOBNM..CONTRCT.ADD.EXT,DISP=SHR
//CMWKF18  DD  DSN=&HLQ0..&JOBNM..CONTRCT.UPD.EXT,DISP=SHR
//CMWKF19  DD  DSN=&HLQ0..&JOBNM..CSTATUS.ADD.EXT,DISP=SHR
//CMWKF20  DD  DSN=&HLQ0..&JOBNM..TOT01.CONTROL,DISP=SHR
//CMWKF21  DD  DSN=&HLQ0..&JOBNM..ADTNL.WR.ADD.EXT,DISP=SHR
//CMWKF22  DD  DSN=&HLQ0..&JOBNM..ADTNL.WR.UPD.EXT,DISP=SHR
//CMWKF23  DD  DSN=&HLQ0..&JOBNM..SMART.TLC.UNSORT,DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P1615CWD),DISP=SHR
//* --------------------------------------------------------------------
//* STEP5  EXTR050                                                     *
//* --------------------------------------------------------------------
//* CWFB4020 - PRODUCE WORK FILE FROM UNIT-CODE TABLE TO BE DOWNLOADED
//* --------------------------------------------------------------------
//EXTR050  EXEC PGM=NATB030,REGION=&REGN2,COND=(0,NE),
//         PARM='SYS=&NAT'
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSPRINT DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMPRT01  DD  SYSOUT=&JCLO
//CMPRT02  DD  SYSOUT=&JCLO
//CMPRT03  DD  SYSOUT=&JCLO
//CMWKF01  DD DSN=&HLQ0..&JOBNM..NDM.UNIT.TBL,
//          DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P1615CWE),DISP=SHR
//* --------------------------------------------------------------------
//* STEP6  EXTR060                                                     *
//* --------------------------------------------------------------------
//* CWFB4021 - PRODUCE WORK FILE FROM ACTION RQSTD TABLE TO BE DOWNLOADE
//* --------------------------------------------------------------------
//EXTR060  EXEC PGM=NATB030,REGION=&REGN2,COND=(0,NE),
//         PARM='SYS=&NAT'
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSPRINT DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMPRT01  DD  SYSOUT=&JCLO
//CMWKF01  DD DSN=&HLQ0..&JOBNM..NDM.ACTN.TBL,
//          DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P1615CWF),DISP=SHR
//* --------------------------------------------------------------------
//* STEP7  EXTR070                                                     *
//* --------------------------------------------------------------------
//* CWFB4022 - PRODUCE WORK FILE FROM LOB/CO/PRODUCT TABLE
//* --------------------------------------------------------------------
//EXTR070  EXEC PGM=NATB030,REGION=&REGN2,COND=(0,NE),
//         PARM='SYS=&NAT'
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSPRINT DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMPRT01  DD  SYSOUT=&JCLO
//CMWKF01  DD DSN=&HLQ0..&JOBNM..NDM.LOB.TBL,
//          DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P1615CWG),DISP=SHR
//* --------------------------------------------------------------------
//* STEP8  EXTR080                                                     *
//* --------------------------------------------------------------------
//* CWFB4023 - PRODUCE WORK FILE FROM  MAJOR BUSINESS TABLE
//* --------------------------------------------------------------------
//EXTR080  EXEC PGM=NATB030,REGION=&REGN2,COND=(0,NE),
//         PARM='SYS=&NAT'
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSPRINT DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMPRT01  DD  SYSOUT=&JCLO
//CMWKF01  DD DSN=&HLQ0..&JOBNM..NDM.MJBP.TBL,
//          DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P1615CWH),DISP=SHR
//* --------------------------------------------------------------------
//* STEP9  EXTR090                                                     *
//* --------------------------------------------------------------------
//* CWFB4024 - PRODUCE WORK FILE FROM SPECIFIC BUSINESS TABLE
//* --------------------------------------------------------------------
//EXTR090  EXEC PGM=NATB030,REGION=&REGN2,COND=(0,NE),
//         PARM='SYS=&NAT'
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSPRINT DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMPRT01  DD  SYSOUT=&JCLO
//CMWKF01  DD DSN=&HLQ0..&JOBNM..NDM.SPBP.TBL,
//          DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P1615CWI),DISP=SHR
//* --------------------------------------------------------------------
//* STEP10 EXTR100                                                     *
//* --------------------------------------------------------------------
//* CWFB4025 - PRODUCE WORK FILE FROM EMPLOYEE SKILLS TABLE
//* --------------------------------------------------------------------
//EXTR100  EXEC PGM=NATB030,REGION=&REGN2,COND=(0,NE),
//         PARM='SYS=&NAT'
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSPRINT DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMPRT01  DD  SYSOUT=&JCLO
//CMWKF01  DD DSN=&HLQ0..&JOBNM..NDM.EMSK.TBL,
//          DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P1615CWJ),DISP=SHR
//* --------------------------------------------------------------------
//* STEP11 EXTR110                                                     *
//* --------------------------------------------------------------------
//* CWFB4026 - PRODUCE WORK FILE FROM UNIT STATUS TABLE
//* --------------------------------------------------------------------
//EXTR110  EXEC PGM=NATB030,REGION=&REGN2,COND=(0,NE),
//         PARM='SYS=&NAT'
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSPRINT DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMPRT01  DD  SYSOUT=&JCLO
//CMWKF01  DD DSN=&HLQ0..&JOBNM..NDM.UNST.TBL,
//          DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P1615CWK),DISP=SHR
//* --------------------------------------------------------------------
//* STEP12 EXTR120                                                     *
//* --------------------------------------------------------------------
//* CWFB4027 - PRODUCE WORK FILE FROM UNIT WORK TABLE
//* --------------------------------------------------------------------
//EXTR120  EXEC PGM=NATB030,REGION=&REGN2,COND=(0,NE),
//         PARM='SYS=&NAT'
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSPRINT DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMPRT01  DD  SYSOUT=&JCLO
//CMWKF01  DD DSN=&HLQ0..&JOBNM..NDM.UNWK.TBL,
//          DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P1615CWM),DISP=SHR
//* --------------------------------------------------------------------
//* STEP13 EXTR130                                                     *
//* --------------------------------------------------------------------
//* CWFB4028 - PRODUCE WORK FILE FROM STEP SEQUENCE TABLE
//* --------------------------------------------------------------------
//EXTR130  EXEC PGM=NATB030,REGION=&REGN2,COND=(0,NE),
//         PARM='SYS=&NAT'
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSPRINT DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMPRT01  DD  SYSOUT=&JCLO
//CMWKF01  DD DSN=&HLQ0..&JOBNM..NDM.STSQ.TBL,
//          DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P1615CWN),DISP=SHR
//* --------------------------------------------------------------------
//* STEP14 EXTR140                                                     *
//* --------------------------------------------------------------------
//* CWFB4029 - PRODUCE WORK FILE FROM WORK PROCESS ID TABLE
//*          - PRODUCE WPID INDICARS TABLE
//* --------------------------------------------------------------------
//EXTR140  EXEC PGM=NATB030,REGION=&REGN2,COND=(0,NE),
//         PARM='SYS=&NAT'
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSPRINT DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMPRT01  DD  SYSOUT=&JCLO
//CMWKF01  DD DSN=&HLQ0..&JOBNM..NDM.WPID.TBL,DISP=SHR
//CMWKF02  DD DSN=&HLQ0..&JOBNM..WPID.IND.TBL,DISP=SHR
//CMWKF03  DD DSN=&HLQ0..&JOBNM..TOT01.CONTROL,DISP=SHR
//CMWKF04  DD DSN=&HLQ0..&JOBNM..TOT02.CONTROL,DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P1615CWO),DISP=SHR
//* --------------------------------------------------------------------
//* STEP15 EXTR150                                                     *
//* --------------------------------------------------------------------
//* CWFB4030 - PRODUCE WORK FILE FROM ROUT SEQUENCE TABLE
//* --------------------------------------------------------------------
//EXTR150  EXEC PGM=NATB030,REGION=&REGN2,COND=(0,NE),
//         PARM='SYS=&NAT'
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSPRINT DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMPRT01  DD  SYSOUT=&JCLO
//CMWKF01  DD DSN=&HLQ0..&JOBNM..NDM.ROUT.TBL,
//          DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P1615CWP),DISP=SHR
//* --------------------------------------------------------------------
//* STEP16 EXTR160                                                     *
//* --------------------------------------------------------------------
//* CWFB4031 - PRODUCE WORK FILE FROM EMPLOYEE POST TABLE
//* --------------------------------------------------------------------
//EXTR160  EXEC PGM=NATB030,REGION=&REGN2,COND=(0,NE),
//         PARM='SYS=&NAT'
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSPRINT DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMPRT01  DD  SYSOUT=&JCLO
//CMWKF01  DD DSN=&HLQ0..&JOBNM..NDM.EMPL.TBL,
//          DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P1615CWQ),DISP=SHR
//* --------------------------------------------------------------------
//* STEP17 EXTR170                                                     *
//* --------------------------------------------------------------------
//* CWFB4033 - PRODUCE WORK FILE FROM DIVISION  TABLE
//* --------------------------------------------------------------------
//EXTR170  EXEC PGM=NATB030,REGION=&REGN2,COND=(0,NE),
//         PARM='SYS=&NAT'
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSPRINT DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMPRT01  DD  SYSOUT=&JCLO
//CMWKF01  DD DSN=&HLQ0..&JOBNM..NDM.DIV.TBL,DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P1615CWR),DISP=SHR
//* --------------------------------------------------------------------
//* STEP18 EXTR180                                                     *
//* --------------------------------------------------------------------
//* CWFB4034 - PRODUCE WORK FILE FROM WORK GROUP TABLE
//* --------------------------------------------------------------------
//EXTR180  EXEC PGM=NATB030,REGION=&REGN2,COND=(0,NE),
//         PARM='SYS=&NAT'
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSPRINT DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMPRT01  DD  SYSOUT=&JCLO
//CMWKF01  DD  DSN=&HLQ0..&JOBNM..NDM.WKGRP.TBL,DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P1615CWS),DISP=SHR
//* --------------------------------------------------------------------
//* STEP19 EXTR190                                                     *
//* --------------------------------------------------------------------
//* CWFB4035 - PRODUCE WORK FILE FROM DOCUMENT TYPE TABLE
//* --------------------------------------------------------------------
//EXTR190  EXEC PGM=NATB030,REGION=&REGN2,COND=(0,NE),
//         PARM='SYS=&NAT'
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSPRINT DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMPRT01  DD  SYSOUT=&JCLO
//CMPRT02  DD  SYSOUT=&JCLO
//CMWKF01  DD  DSN=&HLQ0..&JOBNM..NDM.DOC.TBL,DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P1615CWT),DISP=SHR
//* --------------------------------------------------------------------
//* STEP20 EXTR200                                                     *
//* --------------------------------------------------------------------
//* CWFB4036 - PRODUCE WORK FILE FROM IMAGE ENABLED WORKSTATIONS TABLE
//* --------------------------------------------------------------------
//EXTR200  EXEC PGM=NATB030,REGION=&REGN2,COND=(0,NE),
//         PARM='SYS=&NAT'
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSPRINT DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMPRT01  DD  SYSOUT=&JCLO
//CMWKF01  DD  DSN=&HLQ0..&JOBNM..NDM.IMG.TBL,DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P1615CWU),DISP=SHR
//*
//* --------------------------------------------------------------------
//* STEP21 EXTR210                                                     *
//* --------------------------------------------------------------------
//* CWFB4817 - PRODUCE RELATIONSHIP TABLE (EVERY TIME EXTRACT ALL RECS)*
//* --------------------------------------------------------------------
//EXTR210  EXEC PGM=NATB030,REGION=&REGN2,COND=(0,NE),
//         PARM='SYS=&NAT'
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSPRINT DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMPRT01  DD  SYSOUT=&JCLO
//CMWKF01  DD DSN=&HLQ0..&JOBNM..RELATION.TBL,DISP=SHR
//CMWKF02  DD DSN=&HLQ0..&JOBNM..TOT02.CONTROL,DISP=SHR
//CMWKF03  DD DSN=&HLQ0..&JOBNM..TOT03.CONTROL,DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P1615CWV),DISP=SHR
//*
//* --------------------------------------------------------------------
//* STEP22 EXTR220                                                     *
//* --------------------------------------------------------------------
//* CWFB4818 - PRODUCE CABINET INFO TABLE (EVERY TIME EXTRACT ALL RECS)*
//* --------------------------------------------------------------------
//EXTR220  EXEC PGM=NATB030,REGION=&REGN2,COND=(0,NE),
//         PARM='SYS=&NAT'
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSPRINT DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMPRT01  DD  SYSOUT=&JCLO
//CMWKF01  DD  DSN=&HLQ0..&JOBNM..EFMCABNT.TBL,DISP=SHR
//CMWKF02  DD  DSN=&HLQ0..&JOBNM..TOT03.CONTROL,DISP=SHR
//CMWKF03  DD  DSN=&HLQ0..&JOBNM..TOT04.CONTROL,DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P1615CWW),DISP=SHR
//*                                                                     
//* --------------------------------------------------------------------
//* STEP230 SORT230                                                   *
//* --------------------------------------------------------------------
//* SORT     - SORTS UNSORTED TLC FILE FOR SUBSEQUENT ORACLE LOAD.     *
//* --------------------------------------------------------------------
//SORT230  EXEC PGM=SORT,REGION=&REGN1,COND=(0,NE)
//SYSOUT   DD  SYSOUT=&JCLO
//SYSPRINT DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//SORTWK01 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC2))
//SORTWK02 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC2))
//SORTWK03 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC2))
//SORTIN   DD  DSN=&HLQ0..&JOBNM..SMART.TLC.UNSORT,DISP=SHR
//SORTOUT  DD  DSN=&HLQ0..&JOBNM..SMART.TLC.SORTED,DISP=SHR
//SYSIN    DD  DSN=&LIB1..PARMLIB(P1615CWX),DISP=SHR
