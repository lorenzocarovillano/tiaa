//P1030TXM  PROC DBID=003
//*--------------------------------------------------------*
//*           ON REQUEST JOB
//*--------------------------------------------------------*
//REPT010  EXEC PGM=NATB030,PARM='SYS=NAT003P'
//STEPLIB   DD  DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//          DD  DSN=PROD.BATCH.LOADLIB,DISP=SHR
//SYSPRINT  DD  SYSOUT=*
//DDPRINT   DD  SYSOUT=*
//CMPRINT   DD  SYSOUT=*
//CMPRT01   DD  SYSOUT=8
//CMPRT02   DD  SYSOUT=8
//CMPRT03   DD  SYSOUT=8
//CMPRT04   DD  SYSOUT=8
//SYSUDUMP  DD  SYSOUT=U
//SYSABOUT  DD  SYSOUT=U
//SYSDBOUT  DD  SYSOUT=U
//SYSOUT    DD  SYSOUT=*
//DDCARD    DD  DSN=PROD.PARMLIB(DBAPP&DBID),DISP=SHR
//CMSYNIN   DD  DSN=PROD.NT2LOGON.PARMLIB(PANNSEC),DISP=SHR
//          DD  DSN=PROD.PARMLIB(P1030TX1),DISP=SHR
