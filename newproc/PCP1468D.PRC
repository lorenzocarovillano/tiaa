//PCP1468D PROC DBID=003,
//        REPT='8',                                                     
//        JCLO='*',                                                     
//        DMPS='U',                                                     
//        GDG0='+0',                                                    
//        GDG1='+1',                                                    
//        HLQ0='PAA.ONL.ANN',                                           
//        HLQ1='PIA.ANN',                                               
//        CJOBNM=PCP1468D,                                              
//        PJOBNM1=PAN2005D,                                             
//        PJOBNM2=PCP1415D,                                             
//        NAT=NAT003P,
//        NATMEM=PANNSEC,
//        DATACLS='DCPSEXTC',
//        LIB1=PROD,
//        LIB2=PROD,
//        SPC1='30,10',
//        SPC2='25,10',
//        REGN1=0M
//*
//*********************************************************************
//* PROCEDURE : PCP1468D
//*********************************************************************
//DLTE000  EXEC PGM=IEFBR14
//DD1      DD DISP=(MOD,DELETE,DELETE),UNIT=SYSDA,SPACE=(TRK,(1)),
//         DSN=&HLQ1..LEDGERNZ.DAILY.SORT
//DD2      DD DISP=(MOD,DELETE,DELETE),UNIT=SYSDA,SPACE=(TRK,(1)),
//         DSN=&HLQ1..&CJOBNM..LDPAYNZ.SORT
//*********************************************************************
//*   STORE FUTURE DATED LEDGERS IN GDG
//*********************************************************************
//STEP001  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//            PARM=('SYS=&NAT',
//            'OPRB=(.DBID=253,FNR=68,I,A)')
//SYSPRINT  DD SYSOUT=&JCLO
//SYSUDUMP  DD SYSOUT=&DMPS
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT   DD SYSOUT=&JCLO
//CMPRINT   DD SYSOUT=&JCLO
//CMWKF01   DD DISP=SHR,DSN=&HLQ1..&PJOBNM1..LEDGERNZ.DAILY(&GDG0)
//          DD DISP=SHR,DSN=&HLQ1..&PJOBNM2..LGRNZ.REV.DAILY(&GDG0)
//          DD DISP=SHR,DSN=&HLQ1..&CJOBNM..LEDGERNZ.FUTR(&GDG0)
//CMWKF02   DD DSN=&HLQ1..&CJOBNM..LEDGERNZ.FUTR(&GDG1),
//             LRECL=100,RECFM=FB,BUFNO=25,
//             DISP=(NEW,CATLG,DELETE),DATACLAS=&DATACLS,
//             SPACE=(CYL,(&SPC2),RLSE),UNIT=SYSDA
//CMWKF03   DD DSN=&HLQ1..&CJOBNM..LEDGERNZ.CURR(&GDG1),
//             LRECL=100,RECFM=FB,BUFNO=25,
//             DISP=(NEW,CATLG,DELETE),DATACLAS=&DATACLS,
//             SPACE=(CYL,(&SPC2),RLSE),UNIT=SYSDA
//CMWKF04   DD DSN=&HLQ1..&CJOBNM..LEDGER.CNTL.FILE(&GDG0),DISP=SHR
//CMWKF05   DD DSN=&HLQ1..&CJOBNM..LEDGER.CNTL.FILE(&GDG1),
//             LRECL=80,RECFM=FB,BUFNO=25,
//             DISP=(NEW,CATLG,DELETE),DATACLAS=&DATACLS,
//             SPACE=(CYL,(&SPC2),RLSE),UNIT=SYSDA
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(P1469CPD),DISP=SHR
//          DD DSN=&LIB2..CNTL.PARMLIB(P1465CP1),DISP=SHR
//* FCPPFUPB
//*****************************************************************
//* REMOVE EMPTY FILES IF STEP001 ABENDS
//* THIS STEP WILL ONLY EXECUTE IF STEP001 HAVE RC=92
//*****************************************************************
//STEP002  EXEC PGM=IEFBR14,COND=(92,GT,STEP001)
//SYSPRINT DD SYSOUT=*
//SYSIN DD DUMMY
//DD1    DD DSN=&HLQ1..&CJOBNM..LEDGERNZ.FUTR(&GDG1),
//          DISP=(MOD,DELETE,DELETE)
//DD2    DD DSN=&HLQ1..&CJOBNM..LEDGERNZ.CURR(&GDG1),
//          DISP=(MOD,DELETE,DELETE)
//********************************************************************
//SORT010  EXEC PGM=SORT,REGION=&REGN1,COND=(0,NE)
//SYSPRINT DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//SORTIN   DD  DISP=SHR,DSN=&HLQ1..&CJOBNM..LEDGERNZ.CURR(&GDG1)
//*SORTIN   DD  DISP=SHR,DSN=&HLQ1..&PJOBNM1..LEDGERNZ.DAILY(&GDG0)
//*         DD  DISP=SHR,DSN=&HLQ1..&PJOBNM2..LGRNZ.REV.DAILY(&GDG0)
//         DD  DISP=SHR,DSN=&HLQ1..P1465CPD.LEDGERNZ.DAILY(&GDG0)
//SORTOUT  DD  DSN=&HLQ1..LEDGERNZ.DAILY(&GDG1),
//             DISP=(,CATLG,DELETE),UNIT=SYSDA,
//             SPACE=(CYL,(100,50),RLSE),
//             DCB=(LRECL=100,RECFM=FB)
//SYSIN    DD DSN=&LIB1..PARMLIB(P1468CPX),DISP=SHR
//*
//*********************************************************************
//*
//SORT020  EXEC PGM=SORT,REGION=&REGN1,COND=(0,NE)
//SYSPRINT DD SYSOUT=&JCLO
//SYSOUT   DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//SORTIN   DD DISP=SHR,DSN=&HLQ1..&CJOBNM..LEDGERNZ.CURR(&GDG1)
//*SORTIN   DD DISP=SHR,DSN=&HLQ1..&PJOBNM1..LEDGERNZ.DAILY(&GDG0)
//*         DD DISP=SHR,DSN=&HLQ1..&PJOBNM2..LGRNZ.REV.DAILY(&GDG0)
//SORTOUT  DD DSN=&HLQ1..LEDGERNZ.DAILY.SORT,
//         DISP=(NEW,CATLG,DELETE),DATACLAS=&DATACLS,
//         UNIT=SYSDA,DCB=(RECFM=FB,LRECL=100,BLKSIZE=0),
//         SPACE=(CYL,(100,50),RLSE)
//SYSIN    DD DSN=&LIB1..PARMLIB(P1468CP1),DISP=SHR
//*
//*********************************************************************
//*
//EXTR030  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//         PARM='SYS=&NAT'
//SYSPRINT DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//DDPRINT  DD SYSOUT=&JCLO
//CMPRINT  DD SYSOUT=&JCLO
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMPRT01  DD SYSOUT=(&REPT,CP1468D1)
//CMWKF01  DD DSN=&HLQ1..LEDGERNZ.DAILY.SORT,DISP=SHR
//CMSYNIN  DD DSN=&LIB1..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(P1468CP2),DISP=SHR
//         DD DSN=&LIB1..CNTL.PARMLIB(P1465CP1),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(P1468CP3),DISP=SHR
//* FCPP120W FOR NZ
//*********************************************************************
//*   STORE FUTURE DATED LEDGERS IN GDG
//*********************************************************************
//STEP035  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//            PARM=('SYS=&NAT',
//            'OPRB=(.DBID=253,FNR=68,I,A)')
//SYSPRINT  DD SYSOUT=&JCLO
//SYSUDUMP  DD SYSOUT=&DMPS
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT   DD SYSOUT=&JCLO
//CMPRINT   DD SYSOUT=&JCLO
//CMWKF01   DD DISP=SHR,DSN=&HLQ1..&PJOBNM1..LDPAYNZ.DAILY(&GDG0)
//          DD DISP=SHR,DSN=&HLQ1..&PJOBNM2..LDPYRNZ.DAILY(&GDG0)
//          DD DISP=SHR,DSN=&HLQ1..&CJOBNM..LDPAYNZ.FUTR(&GDG0)
//CMWKF02   DD DSN=&HLQ1..&CJOBNM..LDPAYNZ.FUTR(&GDG1),
//             LRECL=154,RECFM=FB,BUFNO=25,
//             DISP=(NEW,CATLG,DELETE),DATACLAS=&DATACLS,
//             SPACE=(CYL,(&SPC2),RLSE),UNIT=SYSDA
//CMWKF03   DD DSN=&HLQ1..&CJOBNM..LDPAYNZ.CURR(&GDG1),
//             LRECL=154,RECFM=FB,BUFNO=25,
//             DISP=(NEW,CATLG,DELETE),DATACLAS=&DATACLS,
//             SPACE=(CYL,(&SPC2),RLSE),UNIT=SYSDA
//CMWKF04   DD DISP=SHR,DSN=&HLQ1..&CJOBNM..LEDPAY.CNTL.FILE(&GDG0)
//CMWKF05   DD DSN=&HLQ1..&CJOBNM..LEDPAY.CNTL.FILE(&GDG1),
//             LRECL=80,RECFM=FB,BUFNO=25,
//             DISP=(NEW,CATLG,DELETE),DATACLAS=&DATACLS,
//             SPACE=(CYL,(&SPC2),RLSE),UNIT=SYSDA
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(P1469CPC),DISP=SHR
//          DD DSN=&LIB2..CNTL.PARMLIB(P1465CP1),DISP=SHR
//* FCPPFUPA
//*****************************************************************
//* REMOVE EMPTY FILES IF STEP001 ABENDS
//* THIS STEP WILL ONLY EXECUTE IF STEP035 HAVE RC=92
//*****************************************************************
//STEP036  EXEC PGM=IEFBR14,COND=((92,EQ,STEP001),(92,GT,STEP035))
//SYSPRINT DD SYSOUT=*
//SYSIN DD DUMMY
//DD1    DD DSN=&HLQ1..&CJOBNM..LDPAYNZ.FUTR(&GDG1),
//          DISP=(MOD,DELETE,DELETE)
//DD2    DD DSN=&HLQ1..&CJOBNM..LDPAYNZ.CURR(&GDG1),
//          DISP=(MOD,DELETE,DELETE)
//********************************************************************
//*********************************************************************
//* SORT040 - SORT "NZ" & LEDGER EXTRACT FOR DETAIL AND
//*           SUMMARY REPORTS.
//*********************************************************************
//SORT040  EXEC PGM=SORT,REGION=&REGN1,COND=(0,NE)
//SYSOUT    DD SYSOUT=&JCLO
//SORTWK01  DD UNIT=SYSDA,SPACE=(CYL,(&SPC1))
//SORTWK02  DD UNIT=SYSDA,SPACE=(CYL,(&SPC1))
//SORTWK03  DD UNIT=SYSDA,SPACE=(CYL,(&SPC1))
//SORTWK04  DD UNIT=SYSDA,SPACE=(CYL,(&SPC1))
//SYSPRINT  DD SYSOUT=&JCLO
//SORTIN   DD  DISP=SHR,DSN=&HLQ1..&CJOBNM..LDPAYNZ.CURR(+1)
//*SORTIN   DD  DISP=SHR,DSN=&HLQ1..&PJOBNM1..LDPAYNZ.DAILY(&GDG0)
//*         DD  DISP=SHR,DSN=&HLQ1..&PJOBNM2..LDPYRNZ.DAILY(&GDG0)
//*
//SORTOUT   DD DSN=&HLQ1..&CJOBNM..LDPAYNZ.SORT,
//             LRECL=154,RECFM=FB,BUFNO=25,
//             DISP=(NEW,CATLG,DELETE),DATACLAS=&DATACLS,
//             SPACE=(CYL,(&SPC2),RLSE),UNIT=SYSDA
//SYSIN     DD DSN=&LIB1..PARMLIB(P1465CPB),DISP=SHR
//*
//*********************************************************************
//* REPT050 - PRODUCE "NZ" DETAIL AND SUMMARY LEDGER REPORTS
//*           NATURAL PROGRAM "FCPP121".
//*********************************************************************
//REPT050  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//            PARM=('SYS=&NAT',
//            'OPRB=(.DBID=253,FNR=68,I,A)')
//SYSPRINT  DD SYSOUT=&JCLO
//SYSUDUMP  DD SYSOUT=&DMPS
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT   DD SYSOUT=&JCLO
//ISA       DD DSN=&HLQ0..ABC.ABCTBL.V026,DISP=SHR
//CMPRINT   DD SYSOUT=&JCLO
//CMPRT01   DD SYSOUT=(&REPT,CP1468D2)
//CMPRT02   DD SYSOUT=(&REPT,CP1468D3)
//CMWKF01   DD DSN=&HLQ1..&CJOBNM..LDPAYNZ.SORT,DISP=SHR,BUFNO=25
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(P1465CPC),DISP=SHR
//* FCPP121
//*********************************************************************
