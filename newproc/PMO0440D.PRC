//PMO0440D PROC DBID='008',
//         DMPS='U',
//         HLQ0='PPM.ANN.MAYO',    CORPORATE DASD GROUP
//         HLQ8='PPDD.ANN.MAYO',   CORPORATE DASD GROUP
//         JCLO='*',
//         JOBNM='PMO0440D',       JOBNAME FOR CURRENT JOB
//         JOBNM1='PMO0250D',      JOBNAME THAT CREATES FIDELITY FEED
//         LIB1='PROD',            PARMLIB
//         LIB2='PROD',            NT2LOGON.PARMLIB
//         LIB3='PROD',
//         NAT='NAT008P',
//         FSET=M,
//         TCSTRM=02,
//         NATMEM='PANNSEC',
//         REGN1='0M',
//         REPT='8',
//         SPC1='5',
//         SPC2='5'
//*
//*===================================================================*
//*
//*            PROCESS FIDELITY FEED
//*
//*-------------------------------------------------------------------*
//*
//* PROCEDURE: PMO0440D
//*
//* PROGRAMS:  SMYP200
//*
//* FUNCTION:
//*         PROGRAM READS WORK FILE PASSED IN HOUSE FROM FIDELITY;
//*         CHECKS/VALIDATES THE DATA AND PRODUCES 2 REPORTS.
//*         1. EXCEPTION REPORT - BASIC DATA VALIDATIONS
//*         2. COR AND FIDELITY DATA COMPARISON OF REQUIRED DATA FOR
//*            CONTRACT ISSUANCE.
//*
//*                                                                   *
//*   FILE FIDELITY-MASTER-FILE 008/046                               *
//*   FIDELITY-TRANSACTION-FILE 008/052                               *
//*   FIDELITY-CONTRACT-FILE    008/058                               *
//*                                                                   *
//*                                                                   *
//* PROGRAM UPDATES/ADDS MASTER FILE RECORDS; ADDS NEW TRANSACTION    *
//* RECORDS; WILL CALL ACIS IF NO CONTRACTS EXIST FOR A PH            *
//* RECORDS CHANGES IN MIT FOR DOB, GENDER, ADDRESS AND NAME CHANGES  *
//*                                                                   *
//* ----------------------------------------------------------------- *
//* DELETES
//* ----------------------------------------------------------------- *
//*
//DELETE  EXEC PGM=IEFBR14
//*
//SYSOUT  DD SYSOUT=*
//*
//DD1     DD DSN=&HLQ8..&JOBNM..COMPARE.FILE1,
//        DISP=(MOD,DELETE),UNIT=SYSDA,SPACE=(TRK,0)
//*
//DD2     DD DSN=&HLQ8..&JOBNM..COMPARE.FILE2,
//        DISP=(MOD,DELETE),UNIT=SYSDA,SPACE=(TRK,0)
//*
//DD3     DD DSN=&HLQ0..&JOBNM..REPT040.UPLOAD,
//        DISP=(MOD,DELETE),UNIT=SYSDA,SPACE=(TRK,0)
//*
//DD4     DD DSN=&HLQ8..&JOBNM..REPT040.OUTPUT1,
//        DISP=(MOD,DELETE),UNIT=SYSDA,SPACE=(TRK,0)
//*
//DD5     DD DSN=&HLQ8..&JOBNM..REPT040.CNTRL,
//        DISP=(MOD,DELETE),UNIT=SYSDA,SPACE=(TRK,0)
//*
//DD6     DD DSN=&HLQ0..&JOBNM..SORT050.CNTRL.OUT,
//        DISP=(MOD,DELETE),UNIT=SYSDA,SPACE=(TRK,0)
//*
//DD7     DD DSN=&HLQ8..&JOBNM..SORT055.CNTRL.II,
//        DISP=(MOD,DELETE),UNIT=SYSDA,SPACE=(TRK,0)
//*
//DD8     DD DSN=&HLQ0..&JOBNM..REPT040.OVRFLO,
//        DISP=(MOD,DELETE),UNIT=SYSDA,SPACE=(TRK,0)
//*
//DD9     DD DSN=&HLQ0..&JOBNM..REPT070.TOTALS,
//        DISP=(MOD,DELETE),UNIT=SYSDA,SPACE=(TRK,0)
//*
//********************************************************************
//*  EXECUTES THE PROGRAM SMYP200 (MAYOJOB0)                          *
//*********************************************************************
//*
//*
//* RESTART:
//*
//*  THIS IS REPORTING STEP. A FAILURE MAY BE RESTARTED FROM
//*  THE FAILING STEP.
//*
//**********************************************************************
//*                 FIDELITY FILE ACIS PREPROCESS                     *
//*-------------------------------------------------------------------*
//*                                                                   *
//REPT010 EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//        PARM='SYS=&NAT'
//SYSOUT   DD SYSOUT=&JCLO
//SYSPRINT DD SYSOUT=&JCLO
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT  DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//CMPRINT  DD SYSOUT=&JCLO
//CMPRT01  DD SYSOUT=(&REPT,MO0440D1)
//CMPRT02  DD SYSOUT=(&REPT,MO0440D2)
//CMWKF01  DD DSN=&HLQ0..&JOBNM1..UPLOAD,                               
//         DISP=SHR                                                     
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB3..PARMLIB(MO0440D1),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//*********************************************************************
//* SORT015 SORTS THE INPUT FILE FROM FIDELITY TO SUM DUPICATE TRANS  *
//*********************************************************************
//*
//*
//* RESTART:
//*
//*  A FAILURE MAY BE RESTARTED FROM THIS STEP.
//*
//*
//*********************************************************************
//SORT015  EXEC PGM=SORT
//SYSABOUT DD SYSOUT=U
//SYSOUT   DD SYSOUT=*
//SYSUDUMP DD SYSOUT=U
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,(100,50),RLSE)
//SORTWK02 DD UNIT=SYSDA,SPACE=(CYL,(100,50),RLSE)
//SORTIN   DD DSN=&HLQ0..&JOBNM1..UPLOAD,DISP=SHR
//SORTOUT  DD DSN=&HLQ8..&JOBNM..UPLOAD.SORTED,
//            DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//            UNIT=SYSDA
//SYSIN    DD DSN=&LIB3..PARMLIB(MO0440DA),DISP=SHR
//*
//*================================================================*
//* COPY FIDELITY INPUT FILE
//*================================================================*
//COPY020  EXEC PGM=IEBGENER
//SYSIN    DD  DUMMY
//SYSPRINT DD  SYSOUT=*
//SYSUT1   DD  DSN=&HLQ8..&JOBNM..UPLOAD.SORTED,
//             DISP=SHR
//SYSUT2   DD  DSN=&HLQ8..&JOBNM..COMPARE.FILE1,
//             DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//*//             DCB=RECFM=FB,
//             DCB=(RECFM=FB),
//             SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//             UNIT=SYSDA
//*================================================================*
//* COPY FIDELITY INPUT FILE AGAIN
//*================================================================*
//COPY030  EXEC PGM=IEBGENER
//SYSIN    DD  DUMMY
//SYSPRINT DD  SYSOUT=*
//SYSUT1   DD  DSN=&HLQ8..&JOBNM..UPLOAD.SORTED,
//             DISP=SHR
//SYSUT2   DD  DSN=&HLQ8..&JOBNM..COMPARE.FILE2,
//             DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//*//             DCB=RECFM=FB,
//             DCB=(RECFM=FB),
//             SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//             UNIT=SYSDA
//*================================================================****
//* SMYP320 PROGRAM READS WORK FILE PASSED IN HOUSE FROM FIDELITY;
//*         PEFORMS VARIOUS AUDITS ON INPUT FILE.  CREATES EXCEPTION
//*         REPORT OR TERMINATES JOB DEPENDING ON ERROR (IF ANY).
//*
//* CMWKF03  DD DSN=&HLQ0..&JOBNM..REPT040.UPLOAD
//*          THIS DATASET IS A REBUILD OF THE FEED SENT BY FIDELITY
//*          AND WILL BE USED THROUGHOUT THE REST OF PROCESSING STREAM.
//*
//* 04/25/2014 - B. NEWSOM - ADDED NEW WORKFILE 7 CONTAINING FUNDS THAT
//*                          WILL BE LOADED TO AN ARRAY AND USED FOR
//*                          TRANSLATION. (CREA)
//*================================================================****
//REPT040 EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//        PARM=('SYS=&NAT,OPRB=(.ALL)',
//        'RCA=(PDA0410,PDA0420,PDA0450,PDA0451,PDA0550,PDA0551)')
//*MSYS // INCLUDE MEMBER=DDAH&TCSTRM.MM        AHST DD STATEMENT
//*MSYS // INCLUDE MEMBER=DDMU&TCSTRM.MM        MUPD DD STATEMENTS
//*MSYS // INCLUDE MEMBER=DDBR&TCSTRM.MM        BRAR DD STATEMENT
//*MSYS // INCLUDE MEMBER=DDEX&TCSTRM.MM        EXTK DD STATEMENT
//*MSYS // INCLUDE MEMBER=DDFU&TCSTRM.MM        FUND DD STATEMENT
//*MSYS // INCLUDE MEMBER=DDPA&TCSTRM.MM        PART DD STATEMENT
//*MSYS // INCLUDE MEMBER=DDPL&TCSTRM.MM        PLAN
//SYSOUT   DD SYSOUT=&JCLO
//SYSPRINT DD SYSOUT=&JCLO
//SYSINIT  DD  DSN=&CTLLIBL(&ENVIR.&FSET.&TCSTRM),DISP=SHR
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT  DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//VSECU     DD DSN=&VSECPRE..MSTR.SECU,DISP=SHR
//VVIOL     DD DSN=&VSECPRE..MSTR.VIOL,DISP=SHR
//*
//*REL580
//*
//*VSYSM   DD  DSN=&VPRESYSM..MSTR.SYS,DISP=SHR
//VSYSMX   DD  DSN=&VPRESYSM..MSTR.SYS,DISP=SHR
//VSYSM    DD  SUBSYS=(BLSR,'DDNAME=VSYSMX,SHRPOOL=5',
//        'BUFNI=25,BUFND=50,HBUFND=200,HBUFNI=100,DEFERW=YES,MSG=I')
//CMPRINT  DD SYSOUT=&JCLO
//CMPRT01  DD SYSOUT=(&REPT,MO0440D9)                                   
//CMWKF01  DD DSN=&HLQ8..&JOBNM..COMPARE.FILE1,DISP=SHR                 
//CMWKF02  DD DSN=&HLQ8..&JOBNM..COMPARE.FILE2,DISP=SHR                 
//CMWKF03  DD DSN=&HLQ0..&JOBNM..REPT040.UPLOAD,
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//            UNIT=SYSDA,DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//*//            DCB=RECFM=FB
//            DCB=(RECFM=FB)
//CMWKF04  DD DSN=&HLQ8..&JOBNM..REPT040.OUTPUT1,
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//            UNIT=SYSDA,DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//*//            DCB=RECFM=FB
//            DCB=(RECFM=FB)
//CMWKF05  DD DSN=&HLQ8..&JOBNM..REPT040.CNTRL,
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//            UNIT=SYSDA,DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//*//            DCB=RECFM=FB
//            DCB=(RECFM=FB)
//CMWKF06  DD DSN=&HLQ0..&JOBNM..REPT040.OVRFLO,
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//            UNIT=SYSDA,DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//*//            DCB=RECFM=FB
//            DCB=(RECFM=FB)
//CMWKF07  DD DSN=&HLQ0..FUND.TRANSLTE.FILE,DISP=SHR
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB3..PARMLIB(MO0440D2),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//*********************************************************************
//* TEST045 RETURN CODE TEST TO ALLOW REPORT PROCESSING TO COMPLETE  *
//*********************************************************************
//*
//*
//* RESTART:
//*
//*  A FAILURE MAY BE RESTARTED FROM THIS STEP.
//*
//*
//*********************************************************************
//TEST045  IF (REPT040.RC=0 | REPT040.RC=5 | REPT040.RC=9
//      | REPT040.RC=13 | REPT040.RC=17)  THEN
//*********************************************************************
//* SORT050 SORTS THE RESULTS OF STEP REPT040                         *
//*********************************************************************
//*
//*
//* RESTART:
//*
//*  A FAILURE MAY BE RESTARTED FROM THIS STEP.
//*
//*
//*********************************************************************
//SORT050  EXEC PGM=SORT
//SYSABOUT DD SYSOUT=U
//SYSOUT   DD SYSOUT=*
//SYSUDUMP DD SYSOUT=U
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,(100,50),RLSE)
//SORTWK02 DD UNIT=SYSDA,SPACE=(CYL,(100,50),RLSE)
//SORTIN   DD DSN=&HLQ8..&JOBNM..REPT040.CNTRL,DISP=SHR
//SORTOUT  DD DSN=&HLQ8..&JOBNM..SORT050.CNTRL.OUT,
//            DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//            UNIT=SYSDA
//SYSIN    DD DSN=&LIB3..PARMLIB(MO0440D4),DISP=SHR
//*
//*********************************************************************
//* SORT055 SORTS THE RESULTS OF STEP REPT040                         *
//*********************************************************************
//*
//*
//* RESTART:
//*
//*  A FAILURE MAY BE RESTARTED FROM THIS STEP.
//*
//*
//*********************************************************************
//SORT055  EXEC PGM=SORT
//SYSABOUT DD SYSOUT=U
//SYSOUT   DD SYSOUT=*
//SYSUDUMP DD SYSOUT=U
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,(100,50),RLSE)
//SORTWK02 DD UNIT=SYSDA,SPACE=(CYL,(100,50),RLSE)
//SORTIN   DD DSN=&HLQ8..&JOBNM..REPT040.CNTRL,DISP=SHR
//SORTOUT  DD DSN=&HLQ8..&JOBNM..SORT050.CNTRL.II,
//            DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//            UNIT=SYSDA
//SYSIN    DD DSN=&LIB3..PARMLIB(MO0440D8),DISP=SHR
//*================================================================****
//* SMYP3203 PROGRAM READS EXCEPTION REPORT FROM STEP REPT040 AND
//*          PRODUCES A REPORT.
//*
//*================================================================****
//REPT060 EXEC PGM=NATB030,REGION=&REGN1,
//        PARM='SYS=&NAT'
//SYSOUT   DD SYSOUT=&JCLO
//SYSPRINT DD SYSOUT=&JCLO
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT  DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//CMPRINT  DD SYSOUT=&JCLO
//CMPRT01  DD SYSOUT=(&REPT,MO0440D3)
//CMPRT02  DD SYSOUT=(&REPT,MO0440D4)
//CMPRT03  DD SYSOUT=(&REPT,MO0440D5)
//CMPRT04  DD SYSOUT=(&REPT,MO0440D6)
//CMWKF01  DD DSN=&HLQ8..&JOBNM..REPT040.OUTPUT1,
//            DISP=SHR
//CMWKF02  DD DSN=&HLQ8..&JOBNM..SORT050.CNTRL.OUT,
//            DISP=SHR
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB3..PARMLIB(MO0440D3),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//*================================================================****
//* SMYP3204 PROGRAMS READ FILE CREATED IN SORT050 AND REPORTS ON
//* TRANSACTIONS.
//*
//*================================================================****
//REPT070 EXEC PGM=NATB030,REGION=&REGN1,
//        PARM='SYS=&NAT'
//SYSOUT   DD SYSOUT=&JCLO
//SYSPRINT DD SYSOUT=&JCLO
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT  DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//CMPRINT  DD SYSOUT=&JCLO
//CMPRT04  DD SYSOUT=(&REPT,MO0440D7)
//CMPRT05  DD SYSOUT=(&REPT,MO0440D8)
//CMWKF01  DD DSN=&HLQ8..&JOBNM..SORT050.CNTRL.OUT,
//            DISP=SHR
//CMWKF02  DD DSN=&HLQ8..&JOBNM..SORT050.CNTRL.II,
//            DISP=SHR
//CMWKF03  DD DSN=&HLQ0..&JOBNM..REPT070.TOTALS,
//            DISP=(,CATLG,DELETE),
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//            UNIT=SYSDA
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB3..PARMLIB(MO0440D5),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//ENDTEST  ENDIF            END OF RETURN CODE PROCESSING
//**
//**----------------------------------------------------------------**
//** COMPLETE PMO0440D BY EXEC THE PMO0445D JOB TO CONTIUE STREAM   **
//** IF THERE IS A A RC=0 OR RC=5 OR RC=17 THEN CONTINUE THE STREAM **
//** AND SEND ERROR MESSAGE TO THE MAYO DISTRIBUTION LIST           **
//**----------------------------------------------------------------**
//TEST075  IF (REPT040.RC=0 OR REPT040.RC=5 OR REPT040.RC=17) THEN
//**----------------------------------------------------------------**
//** COMPLETE PMO0440D-01 MAYO00 ST=50                              **
//**----------------------------------------------------------------**
//CASCH080 EXEC PGM=U7SVC,
//             PARM='CA7=CA71'
//CA7PRINT DD  SYSOUT=*
//ERRORS   DD  SYSOUT=*
//SYSUDUMP DD  SYSOUT=U
//CA7DATA  DD  DSN=&LIB3..PARMLIB(MO0440D6),
//             DISP=SHR
//ENDTEST2 ENDIF
//TEST085  IF (REPT040.RC=5) THEN
//**----------------------------------------------------------------**
//** RUN XMO0440A TO NOTIFY USERS OF PROBLEMS                       **
//**----------------------------------------------------------------**
//CASCH090 EXEC PGM=U7SVC,
//             PARM='CA7=CA71'
//CA7PRINT DD  SYSOUT=*
//ERRORS   DD  SYSOUT=*
//SYSUDUMP DD  SYSOUT=U
//CA7DATA  DD  DSN=&LIB3..PARMLIB(MO0440D7),
//             DISP=SHR
//ENDTEST3 ENDIF
//TEST095  IF (REPT040.RC=9) THEN
//**----------------------------------------------------------------**
//** RUN XMO0440B TO NOTIFY USERS OF PROBLEMS                       **
//**----------------------------------------------------------------**
//CASCH100 EXEC PGM=U7SVC,
//             PARM='CA7=CA71'
//CA7PRINT DD  SYSOUT=*
//ERRORS   DD  SYSOUT=*
//SYSUDUMP DD  SYSOUT=U
//CA7DATA  DD  DSN=&LIB3..PARMLIB(MO0440DB),
//             DISP=SHR
//ENDTEST4 ENDIF
//TEST105  IF (REPT040.RC=17) THEN
//**----------------------------------------------------------------**
//** RUN XMO0440D TO NOTIFY USERS OF PROBLEMS                       **
//**----------------------------------------------------------------**
//CASCH110 EXEC PGM=U7SVC,
//             PARM='CA7=CA71'
//CA7PRINT DD  SYSOUT=*
//ERRORS   DD  SYSOUT=*
//SYSUDUMP DD  SYSOUT=U
//CA7DATA  DD  DSN=&LIB3..PARMLIB(MO0440DD),
//             DISP=SHR
//ENDTEST5 ENDIF
