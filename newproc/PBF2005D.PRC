//PBF2005D PROC DBID=003,
//         DMPS='U',
//         GDG0='0',
//         HLQ1='PDS.ANN',
//         JCLO='*',
//         JOBNM='PBF2005D',
//         LIB1='PROD',
//*  FOLLOWING 4 STATEMENTS FOR PRO COBOL
//      INCLUDE='PSTPINCL',             INCLUDE-ORACLE INTERFACE PROD
//      LOGMEM1='UP354SEC',             USER/PASSWORD - ORACLE CONNECT
//      LIBPART1='PROD',                PROD SECURITY LIB FOR ORACLE
//      LIBPART2='ORALOGON.NY.PARMLIB', PRODSSECURITY LIB FOR ORACLE
//         LIB2='PROD.NT2LOGON',
//         LIB3='PROD',
//         NAT='NAT015P',
//         NATMEM='PANNSEC',
//         REGN1='9M',
//         REPT='8'
//**********************************************************************
//* THIS IS A PROC TO WRITE BENEFICIARY INTERFACE RECORDS FOR VUL       
//* CONTRACTS FROM MCCAMISH.                                            
//* - THIS JOB RUNS DAILY                                               
//* - PROGRAMS EXECUTED: BENP810
//**********************************************************************
//EXTR010  EXEC PGM=NATB030,REGION=&REGN1,                              
//         PARM='SYS=&NAT'                                              
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                   
//CMWKF01  DD DSN=&HLQ1..&JOBNM..MCCAMISH.BENE(&GDG0),
//            DISP=OLD
//*  FOLLOWING 2 STATEMENTS FOR PRO COBOL                               
//*MSYS // INCLUDE MEMBER=&INCLUDE                                              
//SECFILE  DD  DSN=&LIBPART1..&LIBPART2.(&LOGMEM1.),DISP=SHR            
//SYSPRINT DD SYSOUT=&JCLO                                              
//SYSOUT   DD SYSOUT=&JCLO                                              
//SYSUDUMP DD SYSOUT=&DMPS                                              
//DDPRINT  DD SYSOUT=&JCLO                                              
//CMPRINT  DD SYSOUT=&JCLO                                              
//CMPRT01  DD SYSOUT=(&REPT,BF2005D1)
//CMPRT02  DD SYSOUT=(&REPT,BF2005D2)
//CMPRT03  DD SYSOUT=(&REPT,BF2005D3)
//CMSYNIN  DD DSN=&LIB2..PARMLIB(&NATMEM),DISP=SHR                      
//         DD DSN=&LIB3..PARMLIB(BF2005D1),DISP=SHR                     
//*
