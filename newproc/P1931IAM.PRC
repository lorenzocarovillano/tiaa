//P1931IAM  PROC
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
//*                                                                   *
//* PROCEDURE:  P1931IAM                 DATE:  06/17/98              *
//*                                                                   *
//* ------------------------------------------------------------------*
//*                                                                   *
//* PROGRAMS EXECUTED: IEBGENER                                       *
//*                                                                   *
//* ------------------------------------------------------------------*
//*------------------------------------------------------------------*
//COPY010  EXEC PGM=IEBGENER,COND=(4,LT)                                
//SYSPRINT DD  SYSOUT=*,OUTPUT=*.OUTVDR                                 
//SYSIN    DD  DUMMY                                                    
//SYSUT1    DD DSN=PIA.ANN.CPS.CNTL.FILE(0),
//             DISP=SHR                                                 
//SYSUT2    DD DSN=PPDT.PNA.IA.CPS.CNTL.FILE.BU(+1),        OUTPUT
//             DISP=(,CATLG,DELETE),
//             UNIT=CARTV,
//*//             DCB=MODLDSCB,RECFM=FB
//             DCB=(MODLDSCB,RECFM=FB)
//*------------------------------------------------------------------*
//COPY020  EXEC PGM=IEBGENER                                            
//SYSPRINT DD  SYSOUT=*,OUTPUT=*.OUTVDR                                 
//SYSIN    DD  DUMMY                                                    
//SYSUT1    DD DSN=PPDT.PNA.IA.CPS.FILE(0),            INPUT
//             DISP=(OLD),UNIT=CARTV                                    
//SYSUT2    DD DSN=PPDT.PNA.IA.CPS.FILE.BU(+1),            OUTPUT
//             DISP=(,CATLG,DELETE),
//             UNIT=CARTV,
//*//             DCB=MODLDSCB,RECFM=FB
//             DCB=(MODLDSCB,RECFM=FB)
