//PCP1797D PROC LIB1='PROD',        COMMON LIBRARY HLQ                  
//         REPT00=*,                MOBIUS REPORT ID                    
//         HLQ0='PCPS.ANN',         DASD HLQ                            
//         JOBNM='PCP1797D',        2ND HLQ FOR FILES CREATED IN PROC   
//         UNIT1=SYSDA,             UNIT PARM FOR DASD OUTPUT           
//         SPC1=50,                                                     
//         SPC2=10,                                                     
//         DMPS='U',                DUMP OUTPUT                         
//         JCLO00=*,                JOB OUTPUT                          
//         DBID=003,                DATABASE ID                         
//         REGN1='9M',              REGION SIZE                         
//         NATVERS=NATB030,         NATURAL VERSION                     
//         NAT=NAT003P,             NATURAL PROFILE                     
//         PARMLIB=PARMLIB,         LLQ COMMON PARM LIBRARY             
//         NATLOG=NT2LOGON.PARMLIB, 2 LVL QUAL NATURAL LOGON LIBRARY    
//         NATMEM=PANNSEC           NATURAL LOGON MEMBER                
//*                                                                     
//*--------------------------------------------------------------------*
//*                                                                    *
//* PROCEDURE:  PCP1797D                           DATE:    10/15/2002 *
//*                                                                    *
//*           CONSOLIDATED PAYMENT SYSTEM / OPEN INVESTMENTS           *
//*                                                                    *
//*                       END-OF-DAY PROCESSING                        *
//*                                                                    *
//*  STEP    PROGRAM   PARMS                  COMMENTS                 *
//* -------  -------  --------  -------------------------------------- *
//* EXTR010  CPOP999  PCP1797A  OFAC EXTRACTS                          *
//*                                                                    *
//*--------------------------------------------------------------------*
//* EXTR010  - EXTRACT OFAC PAYMENTS                                    
//*            NATURAL PROGRAM CPOP999                                 *
//***----------------------------------------------------------------***
//EXTR010  EXEC PGM=&NATVERS,REGION=&REGN1,COND=(0,NE),                 
//         PARM='IM=D,SYS=&NAT'                                         
//SYSOUT   DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL                             
//SYSPRINT DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL                             
//SYSUDUMP DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL                             
//DDCARD   DD DSN=&LIB1..&PARMLIB(DBAPP&DBID),DISP=SHR                  
//DDPRINT  DD SYSOUT=&JCLO00,OUTPUT=*.OUTLOCL                           
//CMPRINT  DD SYSOUT=&REPT00,OUTPUT=*.OUTVDR                            
//CMPRT01  DD DSN=&HLQ0..&JOBNM..PAYMENT.OFACPRT,                       
//            DISP=(,CATLG,DELETE),LRECL=132,RECFM=FB,                  
//            SPACE=(CYL,(2,1),RLSE),UNIT=SYSDA,                        
//            DATACLAS=DCPSEXTC                                         
//CMPRT05  DD DSN=&HLQ0..&JOBNM..PAYMENT.CNTL,                          
//            DISP=(,CATLG,DELETE),LRECL=80,RECFM=FB,                   
//            SPACE=(TRK,(1,1),RLSE),UNIT=SYSDA,                        
//            DATACLAS=DCPSEXTC                                         
//CMWKF01  DD DSN=&HLQ0..&JOBNM..PAYMENT.OFAC1,                         
//            DISP=(,CATLG,DELETE),LRECL=400,RECFM=FB,                  
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),UNIT=&UNIT1,               
//            DATACLAS=DCPSEXTC                                         
//CMWKF02  DD DSN=&HLQ0..&JOBNM..PAYMENT.OFAC2,                         
//            DISP=(,CATLG,DELETE),LRECL=400,RECFM=FB,                  
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),UNIT=&UNIT1,               
//            DATACLAS=DCPSEXTC                                         
//CMWKF03  DD DSN=&HLQ0..&JOBNM..PAYMENT.OFAC3,                         
//            DISP=(,CATLG,DELETE),LRECL=400,RECFM=FB,                  
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),UNIT=&UNIT1,               
//            DATACLAS=DCPSEXTC                                         
//CMWKF04  DD DSN=&HLQ0..&JOBNM..PAYMENT.OFAC4,                         
//            DISP=(,CATLG,DELETE),LRECL=400,RECFM=FB,                  
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),UNIT=&UNIT1,               
//            DATACLAS=DCPSEXTC                                         
//CMWKF05  DD DSN=&HLQ0..&JOBNM..PAYMENT.OFAC5,                         
//            DISP=(,CATLG,DELETE),LRECL=400,RECFM=FB,                  
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),UNIT=&UNIT1,               
//            DATACLAS=DCPSEXTC                                         
//CMWKF06  DD DSN=&HLQ0..&JOBNM..PAYMENT.OFAC6,                         
//            DISP=(,CATLG,DELETE),LRECL=400,RECFM=FB,                  
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),UNIT=&UNIT1,               
//            DATACLAS=DCPSEXTC                                         
//CMWKF07  DD DSN=&HLQ0..&JOBNM..PAYMENT.OFAC7,                         
//            DISP=(,CATLG,DELETE),LRECL=400,RECFM=FB,                  
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),UNIT=&UNIT1,               
//            DATACLAS=DCPSEXTC                                         
//CMWKF08  DD DSN=&HLQ0..&JOBNM..PAYMENT.OFAC8,                         
//            DISP=(,CATLG,DELETE),LRECL=400,RECFM=FB,                  
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),UNIT=&UNIT1,               
//            DATACLAS=DCPSEXTC                                         
//CMWKF09  DD DSN=&HLQ0..&JOBNM..PAYMENT.OFAC9,                         
//            DISP=(,CATLG,DELETE),LRECL=400,RECFM=FB,                  
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),UNIT=&UNIT1,               
//            DATACLAS=DCPSEXTC                                         
//CMWKF10  DD DSN=&HLQ0..&JOBNM..PAYMENT.OFAC10,                        
//            DISP=(,CATLG,DELETE),LRECL=400,RECFM=FB,                  
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),UNIT=&UNIT1,               
//            DATACLAS=DCPSEXTC                                         
//CMWKF11  DD DSN=&HLQ0..&JOBNM..PAYMENT.OFAC11,                        
//            DISP=(,CATLG,DELETE),LRECL=400,RECFM=FB,                  
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),UNIT=&UNIT1,               
//            DATACLAS=DCPSEXTC                                         
//CMWKF12  DD DSN=&HLQ0..&JOBNM..PAYMENT.OFAC12,                        
//            DISP=(,CATLG,DELETE),LRECL=400,RECFM=FB,                  
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),UNIT=&UNIT1,               
//            DATACLAS=DCPSEXTC                                         
//CMWKF13  DD DSN=&HLQ0..&JOBNM..PAYMENT.OFAC13,                        
//            DISP=(,CATLG,DELETE),LRECL=400,RECFM=FB,                  
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),UNIT=&UNIT1,               
//            DATACLAS=DCPSEXTC                                         
//CMWKF14  DD DSN=&HLQ0..&JOBNM..PAYMENT.OFAC14,                        
//            DISP=(,CATLG,DELETE),LRECL=9000,RECFM=FB,                 
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),UNIT=&UNIT1,               
//            DATACLAS=DCPSEXTC                                         
//CMWKF15  DD DSN=&HLQ0..&JOBNM..PAYMENT.OFAC15,                        
//            DISP=(,CATLG,DELETE),LRECL=9000,RECFM=FB,                 
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),UNIT=&UNIT1,               
//            DATACLAS=DCPSEXTC                                         
//CMWKF16  DD DSN=&HLQ0..&JOBNM..PAYMENT.OFAC16,                        
//            DISP=(,CATLG,DELETE),LRECL=9000,RECFM=FB,                 
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),UNIT=&UNIT1,               
//            DATACLAS=DCPSEXTC                                         
//CMWKF17  DD DSN=&HLQ0..&JOBNM..PAYMENT.OFAC17,                        
//            DISP=(,CATLG,DELETE),LRECL=9000,RECFM=FB,                 
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),UNIT=&UNIT1,               
//            DATACLAS=DCPSEXTC                                         
//CMSYNIN  DD DSN=&LIB1..&NATLOG(&NATMEM),DISP=SHR                      
//         DD DSN=&LIB1..&PARMLIB(CP1797DA),DISP=SHR                    
//*                                                                     
