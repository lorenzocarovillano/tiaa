//P5080IAM PROC DBID='003',
//         DMPS='U',
//         GDG1='+1',
//         HLQ0='PIA.ANN',
//         HLQ2='PNPD.COR.TAX',
//         HLQ8='PPDD.TAX',
//         JCLO='*',
//         JOBNM2='P1260TWM',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//*        LIB3='PROD',             CNTL.PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='5M',
//         REPT='8',
//         UNIT1='SYSDA'
//*
//*--------------------------------------------------------*
//* PROCEDURE: P5080IAM                  DATE: 10/24/96
//*--------------------------------------------------------*
//*
//* PROGRAMS EXECUTED: NT2B030 (TWRP5080)
//*                    NT2B030 (IAAP312)
//*                    NT2B030 (IAAP390)
//*                                                                     
//*--------------------------------------------------------*
//*                                                                     
//* MODIFICATION HISTORY
//* 04/11/02 - OIA JCL STANDARDS - PHIL STEINHAUSER
//* 09/08/07 - REVISED FOR DATE PARM AUTOMATION.        A. YOUNG
//*                                                                     
//**********************************************************************
//*
//*--------------------------------------------------*
//CREA005  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//         PARM='SYS=&NAT'
//SYSPRINT DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT  DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//SYSOUT   DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRINT  DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRT01  DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMWKF01  DD  DSN=&HLQ2..&JOBNM2..AP.CONTROL,
//             DISP=SHR
//*            DISP=(OLD,KEEP,KEEP)
//CMWKF02  DD  DSN=&HLQ2..P5080IAM.DATES.PREVIOUS,
//             DISP=SHR
//*            DISP=(OLD,KEEP,KEEP)
//CMWKF03  DD  DSN=&HLQ8..P5080IAM.DATES.CURRENT,
//             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             LRECL=80,RECFM=FB,
//             SPACE=(TRK,(1,1),RLSE),UNIT=&UNIT1
//SYSUDUMP DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSABOUT DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//SYSDBOUT DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P5080IA0),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//*
//*--------------------------------------------------*
//EXTR010  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//         PARM='SYS=&NAT'
//SYSPRINT DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT  DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//SYSOUT   DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRINT  DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRT01  DD  SYSOUT=(&REPT,IA5080M1)
//CMWKF01  DD  DSN=&HLQ0..IAADMIN.TAXMAINT(&GDG1),
//             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             UNIT=&UNIT1,
//*//             DCB=MODLDSCB,RECFM=FB,LRECL=457,
//             DCB=(MODLDSCB,RECFM=FB,LRECL=457),
//             SPACE=(CYL,(50,25),RLSE)
//*CMWKF02  DD  DSN=&LIB3..CNTL.PARMLIB(P5080IA2),DISP=SHR
//CMWKF02  DD  DSN=&HLQ8..P5080IAM.DATES.CURRENT,
//             DISP=SHR
//SYSUDUMP DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSABOUT DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//SYSDBOUT DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P5080IA1),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//*
//*--------------------------------------------------*
//EXTR020  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//         PARM='SYS=&NAT'
//SYSPRINT DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT  DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//SYSOUT   DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRINT  DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//*CMWKF02  DD  DSN=&LIB3..CNTL.PARMLIB(P5080IA2),DISP=SHR
//CMWKF02  DD  DSN=&HLQ8..P5080IAM.DATES.CURRENT,
//             DISP=SHR
//CMPRT01  DD  SYSOUT=(&REPT,IA5080M2)
//CMPRT02  DD  SYSOUT=(&REPT,IA5080M3)
//SYSUDUMP DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSABOUT DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//SYSDBOUT DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P5080IA3),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
