//P5200IAA PROC DBID='003',
//         DMPS='U',
//         GDG1='+1',
//         HLQ0='PIA.ANN',
//         HLQ1='PIA.ANN.TPA',
//         HLQ2='PIA.ONL.ANN',
//         HLQ3='PDA.ONL.ANN',
//         HLQ7A='PPDT',
//         HLQ7B='PPDT.TPA',
//         JCLO='*',
//         JOBNM='P5200IAA',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='5M',
//         REPT='8',
//         SPC1='1',
//         SPC2='1',
//         SPC3='300',
//         SPC4='50',
//         SPC5='3',
//         SPC6='2',
//         UNIT1='CARTV',
//         UNIT2='CARTN'
//*
//* ----------------------------------------------------------------- * 
//* PROCEDURE:  P5200IAA                 DATE:    11/25/96            * 
//*                                                                   * 
//* MODIFICATION HISTORY                                              *
//* 11/06    ADDED IARTMG VSAM FILE AND BUFFER OVERRIDE TO MAKE VSAM  * 
//*          ACCESS MORE EFFICIENT                                    * 
//*                                                                   * 
//* 10/03    REMOVE LRECL FROM OUTPUT FILES                           * 
//*                                                                   *
//* 12/01    ADDED TWO NEW FILES CMWKF04 & CMWKF05                    * 
//*          ADDED TWO NEW COPY STEPS                                 *
//*          CHANGED LRECL FROM 1000 TO 1002 FOR FOLLOWING            * 
//*          DSN=PPDT.IAIQ.JAN.REVAL.TRANS(+1), IAIQ FILE             *
//*                                                                   *
//* 11/98    CHANGED LRECL FOR DSN(PPDT.IAIQ.ACTUARY.JAN.REVAL)       * 
//*          FROM LRECL=2462 TO LRECL=2464 IN STEP EXTR010 & COPY030  *
//*                                                                     
//* 04/12/02 - OIA JCL STANDARDS - PHIL STEINHAUSER
//*                                                                     
//* 08/06/10 - ADDED HLQ2 AND IARNDVX AND IARNDV. - O SOTTO
//* 11/09/10 - ADDED WORK FILE 6 FOR CALC METHOD AND VSAM RIUMTF WITH
//*          - BUFFERING.                         - J TINIO
//* 12/06/17 - ADDED NATURAL PARM
//**********************************************************************
//* ----------------------------------------------------------------- * 
//* PROGRAMS EXECUTED: IAAP395 (NATURAL)                                
//* ----------------------------------------------------------------- * 
//* SPECIAL FUNCTION JANUARY DIVIDEND RATE CHANGE FOR IAIQ FUND RECORDS
//*         3 OUTPUT FILES ARE CREATED
//*      1) CMWKF01 - RATE TRANS FILE INPUT TO JOB P5210IAA TO APPLY
//*                   RATE CHANGES TO IA FUND RECORDS
//*      2) CMWKF02 - ACTUARIAL FILE USED BY ACTUARY FOR AUDIT
//*                   CONTROL
//*      3) CMWKF03 - GRADED CONTRACTS FILE USED IN NET CHANGE
//*                   JOB P2520IAM JANUARY PROCESSING ONLY
//*      4) CMWKF04 - FEED TO TPA REINVESTMENT
//*
//*      5) CMWKF05 - FEED TO TPA REINVESTMENT
//*
//*      6) CMWKF06 - CALCULATION METHOD
//* ----------------------------------------------------------------- *
//EXTR010 EXEC PGM=NATB030,REGION=&REGN1,
//        PARM=('SYS=&NAT,IM=D,LT=999999999','OPRB=(.ALL)')             
//SYSPRINT  DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//DDPRINT   DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRINT   DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRT01   DD SYSOUT=(&REPT,IA5200A1)
//SYSUDUMP  DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSABOUT  DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSDBOUT  DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSOUT    DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//*                                                                     
//IARTMGX  DD  DSN=&HLQ2..IARATE.MANAGER.V470,DISP=SHR                  
//IARTMG   DD  SUBSYS=(BLSR,'DDNAME=IARTMGX,SHRPOOL=5',                 
//        'BUFNI=50,BUFND=25,HBUFND=100,HBUFNI=200,DEFERW=YES,MSG=I')   
//IARNDVX  DD  DSN=&HLQ2..RENEWAL.DIVIDEND.V472,DISP=SHR
//IARNDV   DD  SUBSYS=(BLSR,'DDNAME=IARNDVX,SHRPOOL=5',
//        'BUFNI=50,BUFND=25,HBUFND=100,HBUFNI=200,DEFERW=YES,MSG=I')
//*                                                                     
//RIUMTFX  DD  DSN=&HLQ3..MORT.ULTTABLF.V446,DISP=SHR
//RIUMTF   DD  SUBSYS=(BLSR,'DDNAME=RIUMTFX,SHRPOOL=5',
//        'BUFNI=50,BUFND=25,HBUFND=100,HBUFNI=200,DEFERW=YES,MSG=I')
//*                                                                     
//CMWKF01  DD  DSN=&HLQ7A..IAIQ.JAN.REVAL.TRANS(&GDG1), IAIQ FILE
//             DISP=(NEW,CATLG,DELETE),
//             UNIT=&UNIT1,TRTCH=COMP,                                  
//             DCB=(MODLDSCB,RECFM=FB)
//****         DCB=(MODLDSCB,RECFM=FB,LRECL=1002)
//*                                                                     
//CMWKF02  DD  DSN=&HLQ7A..IAIQ.ACTUARY.JAN.REVAL(&GDG1), ACTUARY
//             DISP=(NEW,CATLG,DELETE),                     FILE
//             UNIT=&UNIT1,TRTCH=COMP,                                  
//             DCB=(MODLDSCB,RECFM=FB)
//****         DCB=(MODLDSCB,RECFM=FB,LRECL=2464)
//*                                                                     
//CMWKF03   DD DSN=&HLQ0..GRADED.CNTRCTS(&GDG1),    GRD USED BY NETCH   
//             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,               
//             UNIT=SYSDA,SPACE=(CYL,(&SPC5,&SPC6)),                    
//             DCB=(MODLDSCB,RECFM=FB,LRECL=030)                        
//*                                                                     
//CMWKF04   DD DSN=&HLQ1..&JOBNM..DETAIL.RENEWAL,   USED BY TPA RINV    
//             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,               
//             UNIT=SYSDA,SPACE=(CYL,(&SPC3,&SPC4),RLSE),
//             DCB=(MODLDSCB,RECFM=FB)
//****         RECFM=FB,LRECL=1300,                                     
//*                                                                     
//CMWKF05   DD DSN=&HLQ1..&JOBNM..TOTAL.RENEWAL,    USED BY TPA RINV    
//             DISP=(NEW,CATLG,DELETE),                                 
//             RECFM=FB,LRECL=080,DATACLAS=DCPSEXTC,                    
//             UNIT=SYSDA,SPACE=(TRK,(&SPC5,&SPC6))
//*                                                                     
//CMWKF06   DD DSN=&HLQ0..ACTUARY.JAN.REVAL.CALCMTHD,     CALC METHOD   
//             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,               
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1,&SPC2),RLSE),               
//             DCB=(MODLDSCB,RECFM=FB)                                  
//*                                                                     
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(P5200IA1),DISP=SHR          IAAP395
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//* ----------------------------------------------------------------- * 
//COPY020  EXEC PGM=IEBGENER,COND=(0,NE)                                
//SYSABOUT DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL                            
//SYSUDUMP DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL                            
//SYSPRINT DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL                            
//SYSIN    DD  DUMMY                                                    
//SYSUT1   DD  DSN=&HLQ7A..IAIQ.JAN.REVAL.TRANS(&GDG1),
//             DISP=OLD,UNIT=&UNIT1
//SYSUT2   DD  DSN=&HLQ7A..IAIQ.JAN.REVAL.TRANS.VR(&GDG1),              
//             UNIT=&UNIT2,TRTCH=COMP,                                  
//             DISP=(NEW,CATLG,DELETE),                                 
//             DCB=(MODLDSCB,RECFM=FB)                                  
//****         DCB=(MODLDSCB,RECFM=FB,LRECL=1002)                       
//* ----------------------------------------------------------------- * 
//COPY030  EXEC PGM=IEBGENER,COND=(0,NE)                                
//SYSABOUT DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL                            
//SYSUDUMP DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL                            
//SYSPRINT DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL                            
//SYSIN    DD  DUMMY                                                    
//SYSUT1   DD  DSN=&HLQ7A..IAIQ.ACTUARY.JAN.REVAL(&GDG1),
//             DISP=OLD,UNIT=&UNIT1
//SYSUT2   DD  DSN=&HLQ7A..IAIQ.ACTUARY.JAN.REVAL.BU(&GDG1),       OP   
//             UNIT=&UNIT1,TRTCH=COMP,                                  
//             DISP=(NEW,CATLG,DELETE),                                 
//             DCB=(MODLDSCB,RECFM=FB)                                  
//****         DCB=(MODLDSCB,RECFM=FB,LRECL=2464)                       
//* ----------------------------------------------------------------- * 
//COPY040  EXEC PGM=IEBGENER,COND=(0,NE)                                
//SYSABOUT DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL                            
//SYSUDUMP DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL                            
//SYSPRINT DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL                            
//SYSIN    DD  DUMMY                                                    
//SYSUT1   DD  DSN=&HLQ0..GRADED.CNTRCTS(&GDG1),DISP=SHR
//SYSUT2   DD  DSN=&HLQ7A..&JOBNM..GRADED.CNTRCTS.VR(&GDG1),            
//             UNIT=&UNIT2,TRTCH=COMP,                                  
//             DISP=(NEW,CATLG,DELETE),                                 
//             DCB=(MODLDSCB,RECFM=FB,LRECL=30)                         
//* ----------------------------------------------------------------- * 
//COPY050  EXEC PGM=IEBGENER,COND=(0,NE)                                
//SYSABOUT DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL                            
//SYSUDUMP DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL                            
//SYSPRINT DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL                            
//SYSIN    DD  DUMMY                                                    
//SYSUT1   DD  DSN=&HLQ1..&JOBNM..DETAIL.RENEWAL,DISP=SHR
//SYSUT2   DD  DSN=&HLQ7B..&JOBNM..DETAIL.RENEWAL.JAN.BU,               
//             UNIT=&UNIT1,TRTCH=COMP,                                  
//             DISP=(NEW,CATLG,DELETE),                                 
//             DCB=(MODLDSCB,RECFM=FB)                                  
//****         DCB=(MODLDSCB,RECFM=FB,LRECL=1300)                       
//* ----------------------------------------------------------------- * 
//COPY060  EXEC PGM=IEBGENER,COND=(0,NE)                                
//SYSABOUT DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL                            
//SYSUDUMP DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL                            
//SYSPRINT DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL                            
//SYSIN    DD  DUMMY                                                    
//SYSUT1   DD  DSN=&HLQ1..&JOBNM..TOTAL.RENEWAL,DISP=SHR
//SYSUT2   DD  DSN=&HLQ7B..&JOBNM..TOTAL.RENEWAL.JAN.BU,                
//             UNIT=&UNIT1,TRTCH=COMP,                                  
//             DISP=(NEW,CATLG,DELETE),                                 
//             DCB=(MODLDSCB,RECFM=FB,LRECL=80)                         
