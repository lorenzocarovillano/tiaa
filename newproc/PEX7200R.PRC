//PEX7200R PROC DBID='003',
//         REGN1='4M',
//         DMPS='U',
//         JCLO='*',
//         LIB2='PROD',
//         HLQ2='PEX',
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         PARM1='EX7200R1',
//         JOBNAME='PEX7200R'
//*
//**********************************************************************
//*                                                                    *
//* PROCEDURE        :  PEX7200R                                       *
//*                  LOAD UPDATES TO EXTERNALIZATION USING DATA        *
//*                  COMING FROM INFORMATICA SERVER USING NDM FEED     *
//* PROGRAMS EXECUTED:                                                 *
//*                  UPDT010 - NAT PROG (UPDATE EXTERNALIZATION FILES  *
//**********************************************************************
//*
//UPDT010  EXEC PGM=NATB030,REGION=&REGN1,
//         PARM='SYS=&NAT'
//SYSOUT   DD  SYSOUT=&JCLO
//SYSABOUT DD  SYSOUT=&DMPS
//SYSUDUMP DD  SYSOUT=&DMPS
//DDCARD   DD  DSN=PROD.PARMLIB(DBAPP&DBID),DISP=SHR
//CMPRINT  DD  SYSOUT=&JCLO
//* ----------------------------------------------------------
//*  DATASET FROM NDM PEX7100R
//CMWKF01  DD  DSN=&HLQ2..COR.&JOBNAME..NDM010.FND,DISP=SHR
//         DD  DSN=&HLQ2..COR.&JOBNAME..NDM010.ACL,DISP=SHR
//         DD  DSN=&HLQ2..COR.&JOBNAME..NDM010.CFN,DISP=SHR
//         DD  DSN=&HLQ2..COR.&JOBNAME..NDM010.FDS,DISP=SHR
//         DD  DSN=&HLQ2..COR.&JOBNAME..NDM010.FST,DISP=SHR
//         DD  DSN=&HLQ2..COR.&JOBNAME..NDM010.RTE,DISP=SHR
//* ----------------------------------------------------------
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB2..PARMLIB(&PARM1),DISP=SHR
//         DD  DSN=&LIB2..PARMLIB(FINPARM),DISP=SHR
//************ END OF PROC PCS0300D *************
