//P1422CPD PROC JCLO='*',                                               
//          HLQ0='PMS.ANN',                                             
//          JOBNM1=P1420CPD,                                            
//          GDG0='0',                                                   
//          GDG1='+1',                                                  
//          SPC1='5,2'                                                  
//*                                                                     
//* MODIFICATION HISTORY                                                
//* 04/30/02 - OIA JCL STANDARDS                                        
//* 03/31/06 - PAYEE MATCH. MODIFY DCB WACHO POS PAY LRECL=150          
//*                                                                     
//**********************************************************************
//COPY010 EXEC PGM=IEBGENER                                             
//SYSUT1   DD DSN=&HLQ0..&JOBNM1..POSPAY(&GDG0),DISP=SHR                
//SYSUT2   DD DSN=&HLQ0..RUN2.POSPAY(&GDG1),                            
//            DISP=(,CATLG,DELETE),                                     
//            UNIT=SYSDA,                                               
//            SPACE=(CYL,(&SPC1),RLSE),                                 
//*//            DCB=MODLDSCB,RECFM=FB,LRECL=150                           
//            DCB=(MODLDSCB,RECFM=FB,LRECL=150)
//*           DCB=MODLDSCB,RECFM=FB,LRECL=80                            
//SYSPRINT DD SYSOUT=&JCLO                                              
//SYSIN    DD DUMMY                                                     
