//P5170IAM PROC DBID='003',
//         DBID1='015',
//         DMPS='U',
//         GDG1='+1',
//         HLQ0='PIA.ANN',
//***      HLQ1='PINT.ONL.ANN',
//         HLQ8='PPDD',
//         JCLO='*',
//         HLQ7='PPDT.PNA.IA',
//         UNIT1='CARTV',
//         GDG0='+0',
//         JOBNM='P5170IAM',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         LIB3='PROD',             CNTL.PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='0M',
//         REPT='8',
//         SPC1='50',
//         SPC2='5',
//         SPC3='10',
//         SPC4='5'
//*
//* ----------------------------------------------------------------- *
//* PROCEDURE:  P5170IAM                 DATE:    05/01/96            *
//* ----------------------------------------------------------------- *
//*                                                                   *
//* PROGRAMS EXECUTED:  NT2B030 (IAAP380)                             *
//*                             (IAAP381)                             *
//*                              PIA3029                              *
//* ----------------------------------------------------------------- *
//*                                                                     
//* MODIFICATION HISTORY
//* 12/00/01 - ADDED NEW STEP EXTR015                          *
//* 04/11/02 - OIA JCL STANDARDS - PHIL STEINHAUSER
//* 05/11/17 - REMOVED LRECL'S - OS                                     
//**********************************************************************
//*
//EXTR010 EXEC PGM=NATB030,REGION=&REGN1,
//         PARM='SYS=&NAT,WH=ON,ETID=P5150IAM'
//SYSPRINT DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//DDPRINT  DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRINT  DD  SYSOUT=(&REPT,IA5170M1)
//SYSOUT   DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRT01  DD  SYSOUT=(&REPT,IA5170M2)
//*
//CMWKF01  DD  DSN=&HLQ8..&JOBNM..EXTR010,
//             DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//*//             DCB=RECFM=FB
//             DCB=(RECFM=FB)
//*            DCB=(RECFM=FB,LRECL=0080)
//*
//SYSUDUMP DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSABOUT DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSDBOUT DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P5170IA1),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//******************************************************************
//EXTR015 EXEC PGM=NATB030,REGION=&REGN1,
//         PARM='SYS=&NAT,WH=ON,ETID=P5170IAM'
//SYSPRINT DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//DDPRINT  DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRINT  DD  SYSOUT=(&REPT,IA5170M7)
//SYSOUT   DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRT01  DD  SYSOUT=(&REPT,IA5170M8)
//*
//CMWKF01  DD  DSN=&HLQ8..&JOBNM..EXTR010,DISP=OLD
//CMWKF02  DD  DSN=&HLQ8..&JOBNM..EXTR015.LEGAL,
//             DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//*//             DCB=RECFM=FB
//             DCB=(RECFM=FB)
//*            DCB=(RECFM=FB,LRECL=0080)
//*
//SYSUDUMP DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSABOUT DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSDBOUT DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P5170IA2),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//* ----------------------------------------------------------------- *
//* PROGRAMS EXECUTED: PIA3029                                        *
//* ----------------------------------------------------------------- *
//EXTR020  EXEC PGM=PIA3029,COND=(0,NE),REGION=&REGN1
//SYSABOUT DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSOUT   DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//SYSDBOUT DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSUDUMP DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//IAMASTER DD  DSN=&HLQ8..&JOBNM..EXTR010,DISP=OLD
//*
//SYS081   DD  DSN=&HLQ8..&JOBNM..EXTR015.LEGAL,DISP=SHR
//*
//SYS082   DD  DSN=&HLQ0..IAIQ.LEGAL.EXTRACT(&GDG1),
//             UNIT=SYSDA,SPACE=(CYL,(&SPC3,&SPC4)),
//             DISP=(,CATLG,DELETE),
//             DCB=(MODLDSCB,RECFM=FB)
//*            DCB=(MODLDSCB,RECFM=FB,LRECL=155)
//SYS012   DD  DSN=&HLQ7..NAME.CORR.MDM(&GDG0),                CORR ETL
//             DISP=OLD,UNIT=&UNIT1
//SYSIN    DD  DSN=&LIB3..CNTL.PARMLIB(IAMCKDT),DISP=SHR          INPUT
//DATECARD DD  DSN=&LIB1..PARMLIB(CURRENT),DISP=SHR               INPUT
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID1),DISP=SHR
//*** IAA991   DD  DSN=&HLQ1..IALEGAL.ADDRESS.V175,DISP=SHR
//PRT006   DD  SYSOUT=(&REPT,IA5170M3)
//PRT007   DD  SYSOUT=(&REPT,IA5170M4)
//PRT008   DD  SYSOUT=(&REPT,IA5170M5)
//PRT009   DD  SYSOUT=(&REPT,IA5170M6)
