//P9997ANR PROC DBID=003
//*********************************************************************
//* PROCEDURE:  P9997ANR                 DATE:     01/11/00
//* PROGRAMS EXECUTED: NAZP997
//*-------------------------------------------------------------------*
//*
//* D A T E     PROGRAMMER / EXT      A  C  T  I  O  N
//*
//* 01/11/00    JOHN VLISMAS   X5530 DELETE A REQUEST OR PART OF A
//*                                   REQUEST FROM ADAM
//*                                   (FILES 160 DB45)
//*
//***************************************************************
//UPDT010  EXEC PGM=NATB030,REGION=4M,
//   PARM='SYS=NAT003P'
//STEPLIB  DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//SYSPRINT DD SYSOUT=*
//SYSABOUT DD SYSOUT=U
//SYSUDUMP DD SYSOUT=U
//DDCARD   DD DSN=PROD.PARMLIB(DBAPP&DBID),DISP=SHR
//CMWKF01  DD DSN=PROD.PARMLIB(P9997AN1),DISP=SHR
//SYSOUT   DD SYSOUT=*
//DDPRINT  DD SYSOUT=*
//CMPRINT  DD SYSOUT=*
//CMPRT01  DD SYSOUT=(8,AN9997R1)
//CMSYNIN  DD DSN=PROD.NT2LOGON.PARMLIB(PANNSEC),DISP=SHR
//         DD DSN=PROD.PARMLIB(P9997AN2),DISP=SHR
//         DD DSN=PROD.PARMLIB(FINPARM),DISP=SHR
