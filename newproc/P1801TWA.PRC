//P1801TWA PROC DBID=003,                                               
//         DMPS='U',                                                    
//         GDG1='+1',                                                   
//         HLQ7='PPDT.TAX',                                             
//         HLQ8='PPDD.TAX',                                             
//         JCLO='*',                                                    
//         LIB1='PROD',              PARMLIB                            
//         LIB2='PROD',              NT2LOGON.PARMLIB                   
//         NAT='NAT003P',                                               
//         NATMEM='PANNSEC',                                            
//         REGN1='8M',                                                  
//         REPT='8',                                                    
//         SPC1='50,10',                                                
//         SPC2='15,5',                                                 
//         SPC3='50,10',                                                
//         UNIT1='CARTV'                                                
//*--------------------------------------------------------------------*
//*  OIA STANDARDS  11/26/02 - J. VIOLA                                 
//*                                                                     
//* PROCEDURE:  P1801TWA TAX & WITHHOLDING REPORTING                   *
//*                                                                    *
//*             ORIGINAL PAPER STATES REPORTING                        *
//*                                                                    *
//*             CREATES ANNUAL TAX REPORTS AND FOR PAPER STATES        *
//*             REPORTING.                                             *
//*                                                                    *
//*--------------------------------------------------------------------*
//* EXTR010 - PROGRAM CREATES A DISK EXTRACT OF THE SPECIFIED PAPER     
//*           STATE FROM THE TAX FORMS DATA BASE FILE.                  
//*           &STNUM = STATE PARM (I.E. P1823TW1, P1841TW1...)          
//*           &STATE = STATE NAME (I.E. AL, MA, OH, TX, FL, CA...)      
//*           NATURAL PROGRAM "TWRP3600".                               
//*--------------------------------------------------------------------*
//EXTR010 EXEC PGM=NATB030,TIME=1400,REGION=&REGN1,                     
//        PARM='SYS=&NAT'                                               
//SYSPRINT  DD SYSOUT=&JCLO                                             
//DDPRINT   DD SYSOUT=&JCLO                                             
//CMPRINT   DD SYSOUT=&JCLO                                             
//CMPRT01   DD SYSOUT=&REPT,                                            
//             RECFM=FBA,LRECL=133,BLKSIZE=11571                        
//CMPRT04   DD SYSOUT=&REPT,                                            
//             RECFM=FBA,LRECL=133,BLKSIZE=11571                        
//SYSUDUMP  DD SYSOUT=&DMPS                                             
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//CMWKF01   DD DSN=&HLQ8..EXTR010.&STATE..OUTPUT,                       
//             DISP=(,CATLG,DELETE),                                    
//             UNIT=SYSDA,                                              
//             RECFM=FB,LRECL=570,                                      
//             SPACE=(CYL,(&SPC1),RLSE)                                 
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR            
//          DD DSN=&LIB1..PARMLIB(P1801TWA),DISP=SHR                    
//          DD DSN=&LIB1..PARMLIB(&STNUM),DISP=SHR                      
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                     
//* TWRP3600                                                            
//* 1099  2000N20   (PARM FOR THE STATE OF KENTUCKY)                    
//*---------------------------------------------------------------------
//* SORT020 - SORTS EXTRACTED ORIGINAL PAPER STATE REPORTING DATA BY    
//*           COMPANY CODE, AND SYS ERROR CODE.                         
//*---------------------------------------------------------------------
//SORT020 EXEC PGM=SORT,REGION=&REGN1,COND=(0,NE)                       
//SYSOUT    DD SYSOUT=&JCLO                                             
//SYSPRINT  DD SYSOUT=&JCLO                                             
//SORTWK01  DD UNIT=SYSDA,SPACE=(CYL,(&SPC2),RLSE)                      
//SORTWK02  DD UNIT=SYSDA,SPACE=(CYL,(&SPC2),RLSE)                      
//SORTWK03  DD UNIT=SYSDA,SPACE=(CYL,(&SPC2),RLSE)                      
//SORTWK04  DD UNIT=SYSDA,SPACE=(CYL,(&SPC2),RLSE)                      
//SORTWK05  DD UNIT=SYSDA,SPACE=(CYL,(&SPC2),RLSE)                      
//SORTWK06  DD UNIT=SYSDA,SPACE=(CYL,(&SPC2),RLSE)                      
//SORTWK07  DD UNIT=SYSDA,SPACE=(CYL,(&SPC2),RLSE)                      
//SORTIN    DD DSN=&HLQ8..EXTR010.&STATE..OUTPUT,                       
//             DISP=(OLD,DELETE,KEEP)                                   
//SORTOUT   DD DSN=&HLQ8..SORT020.&STATE..OUTPUT.SRTED,                 
//             DISP=(,CATLG,DELETE),                                    
//             UNIT=SYSDA,                                              
//             RECFM=FB,LRECL=570,                                      
//             SPACE=(CYL,(&SPC3),RLSE)                                 
//SYSIN     DD DSN=&LIB1..PARMLIB(P1801TWB),DISP=SHR                    
//* SORT FIELDS=(5,1,CH,A,352,1,CH,A,7,20,CH,A)                         
//*                                                                     
//*   COMPANY CODE                                                      
//*   SORT INDICATOR                                                    
//*   TIRF-TIN                                                          
//*   TIRF-CONTRACT-NBR                                                 
//*   TIRF-PAYEE-CDE                                                    
//*---------------------------------------------------------------------
//* FRMT030 - PROGRAM READS EXTRACT OF SPECIFIED PAPER STATE FROM       
//*           THE TAX FORMS DATA BASE FILE.  SELECTS RECORDS TO BE      
//*           REPORTED, AND PRODUCES FOUR REPORTS.                      
//*           NATURAL PROGRAM "TWRP3615".                               
//*---------------------------------------------------------------------
//FRMT030 EXEC PGM=NATB030,TIME=1400,REGION=&REGN1,COND=(0,NE),         
//        PARM='SYS=&NAT'                                               
//SYSPRINT  DD SYSOUT=&JCLO                                             
//DDPRINT   DD SYSOUT=&JCLO                                             
//CMPRINT   DD SYSOUT=&JCLO                                             
//CMPRT01   DD SYSOUT=&REPT,                                            
//             RECFM=FBA,LRECL=133,BLKSIZE=11571                        
//CMPRT02   DD SYSOUT=&REPT,                                            
//             RECFM=FBA,LRECL=133,BLKSIZE=11571                        
//CMPRT03   DD SYSOUT=&REPT,                                            
//             RECFM=FBA,LRECL=133,BLKSIZE=11571                        
//CMPRT04   DD SYSOUT=&REPT,                                            
//             RECFM=FBA,LRECL=133,BLKSIZE=11571                        
//SYSUDUMP  DD SYSOUT=&DMPS                                             
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//CMWKF01   DD DSN=&HLQ8..SORT020.&STATE..OUTPUT.SRTED,                 
//             DISP=SHR                                                 
//CMWKF02   DD DSN=&HLQ8..FRMT030.&STATE..PRINT,                        
//*//             DCB=RECFM=FB,LRECL=80,                                   
//             DCB=(RECFM=FB,LRECL=80),
//             SPACE=(CYL,(&SPC2),RLSE),                                
//             DISP=(NEW,CATLG,DELETE),UNIT=SYSDA                       
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR            
//          DD DSN=&LIB1..PARMLIB(P1801TWC),DISP=SHR                    
//          DD DSN=&LIB1..PARMLIB(&STNUM),DISP=SHR                      
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                     
//* TWRP3605                                                            
//********************************************************************* 
//* PRNT035  READS INPUT FILE AND GENERATES HARDCOPY FORMS TO LOCAL   * 
//*          PRINTER                                                  * 
//********************************************************************* 
//PRNT035 EXEC PGM=NATB030,TIME=1400,REGION=5M,                         
//        PARM='SYS=NAT003P'                                            
//*STEPLIB   DD DSN=PROD.BATCH.LOADLIB,DISP=SHR                         
//*          DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR              
//SYSPRINT  DD SYSOUT=*                                                 
//SYSOUT    DD SYSOUT=*                                                 
//SYSUDUMP  DD SYSOUT=U                                                 
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//*DDCARD    DD DSN=PROD.PARMLIB(DBAPP&DBID),DISP=SHR                   
//DDPRINT   DD SYSOUT=*                                                 
//CMPRINT   DD SYSOUT=*                                                 
//CMWKF01   DD DSN=&HLQ8..FRMT030.&STATE..PRINT,DISP=SHR                
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR            
//          DD DSN=&LIB1..PARMLIB(P1801TWD),DISP=SHR                    
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                     
//* TWRP3528                                                            
//*---------------------------------------------------------------------
//* COPY040 - STEP CREATES A COPY OF THE PAPER STATE ORIGINAL FORMS     
//*           DATA FILE EXTRACTED IN STEP "EXTR010" IN THIS JOB.        
//*---------------------------------------------------------------------
//COPY040 EXEC PGM=IEBGENER,COND=(0,NE)                                 
//SYSPRINT  DD SYSOUT=&JCLO                                             
//SYSUT1    DD DSN=&HLQ8..SORT020.&STATE..OUTPUT.SRTED,DISP=SHR         
//SYSUT2    DD DSN=&HLQ7..COPY040.&STATE..BKUP(&GDG1),                  
//             DISP=(,CATLG,DELETE),UNIT=&UNIT1,                        
//*//             DCB=MODLDSCB,RECFM=FB,LRECL=570                          
//             DCB=(MODLDSCB,RECFM=FB,LRECL=570)
//SYSIN     DD DUMMY                                                    
//*---------------------------------------------------------------------
