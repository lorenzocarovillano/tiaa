//PTW1190D PROC DBID='003',
//         DMPS='U',
//         HLQ0='PNPD.COR.TAX',
//         JCLO='*',
//         JOBNM=,
//         PGMPARM=,
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD.NT2LOGON',    NT2LOGON PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='8M',
//         REPID1=',TW1190D1',
//         REPID2=',TW1190D2',
//         REPID3=',TW1190D3',
//         REPT='8'
//*
//*---------------------------------------------------------------------
//*                    EW - DATE SETIP
//*---------------------------------------------------------------------
//* MODIFICATION HISTORY
//*                                                                     
//*                                                                     
//**********************************************************************
//* EXTR010 - CREATES A CONTROL RECORD EXTRACT FOR SOURCE CODE "EW",
//*           USING LAST BUSINESS DAY'S EXECUTION CONTROL RECORD
//*           EXTRACT FOR INPUT.
//*           NATURAL PROGRAM "TWRP0600".
//*---------------------------------------------------------------------
//EXTR010  EXEC PGM=NATB030,REGION=&REGN1,
//         PARM='SYS=&NAT'
//SYSOUT    DD SYSOUT=&JCLO
//SYSUDUMP  DD SYSOUT=&DMPS
//SYSPRINT  DD SYSOUT=&JCLO
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT   DD SYSOUT=&JCLO
//CMPRINT   DD SYSOUT=&JCLO
//CMPRT01   DD SYSOUT=(&REPT&REPID1)
//CMPRT02   DD SYSOUT=(&REPT&REPID2)
//CMPRT03   DD SYSOUT=(&REPT&REPID3)
//CMWKF01   DD DSN=&HLQ0..&JOBNM..CONTROL.EXT,DISP=SHR
//CMWKF02   DD DSN=&HLQ0..&JOBNM..CONTROL.EXT,DISP=SHR
//CMSYNIN   DD DSN=&LIB2..PARMLIB(&NATMEM),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(&PGMPARM),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//* TWRP0600
