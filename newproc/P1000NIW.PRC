//P1000NIW PROC DBID=003,
//         DMPS='U',
//         JCLO='*',
//         LIB1='PROD',
//         LIB2='PROD',
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REPT='8',
//         REGN1='5M'
//* ----------------------------------------------------------------- *
//* PROCEDURE:  P1000NID                 DATE:    01/16/91            *
//*             REMOVE ALL STEPS EXCEPT REPT100                       *
//* ----------------------------------------------------------------- *
//* PROGRAMS EXECUTED: APPB319                                        *
//* ----------------------------------------------------------------- *
//*********************************************************************
//REPT100  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//   PARM='SYS=&NAT'
//DATECARD DD  DSN=&LIB1..PARMLIB(CURRENT),DISP=SHR
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSPRINT DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMPRT01  DD  SYSOUT=(&REPT,NI1000WA)
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P1000NIB),DISP=SHR
