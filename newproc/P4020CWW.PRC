//P4020CWW PROC DBID='045',
//         DMPS='U',
//         JCLO=*,
//         LIB1='PROD',
//         LIB2='PROD',
//         NAT='NAT045P',
//         NATMEM='PANNSEC',
//         REGN1='5M',
//         REPT='8'
//**********************************************************************
//* DELETE AUTOMATIC TRANSACTION INITIATION FULFILLMENT RECORDS (192)
//**********************************************************************
//REPT010  EXEC PGM=NATB030,REGION=&REGN1,
//         PARM='SYS=&NAT'
//SYSPRINT DD  SYSOUT=&JCLO
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSOUT   DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//CMPRT01  DD  SYSOUT=(&REPT,CW4020W1)
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P4020CW2),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
