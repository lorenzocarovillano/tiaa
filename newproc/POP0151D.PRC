//POP0151D PROC EMBDY='PROD.PARMLIB(OP0151D1)'
//* *******************************************************************
//*  SEND EMAIL TO PROD SUPPORT ON-CALL WHEN OPS NDM'S ARE COMPLETE
//* *******************************************************************
//*----------------------------------------------------------------
//* PREPARE AN EMAIL TO SEND TO PROD SUPPORT ON-CALL
//*----------------------------------------------------------------
//STEP050 EXEC PGM=IRXJCL,
//       PARM='PARMSUB REPID=POP0151D EMBDY=&EMBDY'
//TEMPLATE DD DSN=PROD.PARMLIB(OP0151D2),DISP=SHR
//SUBSTED DD DSN=&&SUBSTED,DISP=(,PASS),SPACE=(TRK,(1,2))
//SYSTSIN DD DUMMY
//SYSTSPRT DD SYSOUT=*
//SYSEXEC DD DSN=PROD.REXXLIB,DISP=SHR
//*---------------------------------------------------------------------
//*       SEND EMAIL NOTIFICATION
//*---------------------------------------------------------------------
//STEP060 EXEC PGM=IKJEFT01,DYNAMNBR=50,REGION=4M
//SYSTSPRT DD SYSOUT=*
//SYSUDUMP DD SYSOUT=*
//SYSPROC DD DSN=PMVSPP.ISPF.CLISTLIB,DISP=SHR
//SYSTSIN DD DSN=&&SUBSTED,DISP=(OLD,DELETE)
//***********************************************************
