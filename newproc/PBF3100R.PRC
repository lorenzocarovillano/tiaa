//PBF3100R PROC DBID=003,
//         DMPS='U',
//         JCLO='*',
//         GDG0='0',
//         HLQ1='PNA.ANN',
//         LIB1='PROD',
//         LIB2='PROD.NT2LOGON',
//         NAT='NAT015P',
//         NATMEM='PANNSEC',
//         REGN1='9M',
//         REPT='8'
//**********************************************************************
//* THIS IS COPY BENE CONTRACT DESIGNATION FROM SOURCE TO TARGET        
//*      CONTRACT LIST THAT CAME FROM ACIS JOB PNI9010R                 
//* - THIS JOB RUNS ON-REQUEST                                          
//* - THIS JOB IS RESTARTABLE                                           
//* - PROGRAMS EXECUTED: BENP310
//**********************************************************************
//UPDT010  EXEC PGM=NATB030,REGION=&REGN1,                              
//         PARM='SYS=&NAT'                                              
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                   
//SYSPRINT DD SYSOUT=&JCLO                                              
//SYSOUT   DD SYSOUT=&JCLO                                              
//SYSUDUMP DD SYSOUT=&DMPS                                              
//DDPRINT  DD SYSOUT=&JCLO                                              
//CMPRINT  DD SYSOUT=&JCLO                                              
//CMPRT01  DD SYSOUT=(&REPT,BF3100R1)                                   
//CMWKF01  DD DSN=&HLQ1..ACIS.IRASUB.BENEDATA(&GDG0),                   
//         DISP=SHR                                                     
//CMSYNIN  DD DSN=&LIB2..PARMLIB(&NATMEM),DISP=SHR                      
//         DD DSN=&LIB1..PARMLIB(BF3100R1),DISP=SHR                     
