//PNI2075W PROC DBID=003,                                               
//         LIB1='PROD',                                                 
//         LIB2='PROD',                                                 
//         NATMEM='PANNSEC',                                            
//         NAT='NAT003P',                                               
//         REGN1='9M',                                                  
//         DMPS='*',                                                    
//         JCLO='*'                                                     
//*                                                                     
//**********************************************************************
//* WEEKLY JOB - ADDS NEXT CONTRACT RANGES TO THE ILOG FILES           *
//* EXECUTES NATURAL PROGRAM APPB2075                                  *
//**********************************************************************
//UPDT010  EXEC PGM=NATB030,REGION=&REGN1,                              
//   PARM='SYS=&NAT,UDB=&DBID,OBJIN=N'                                  
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR             
//         DD DSN=&LIB1..PARMLIB(NI2075W1),DISP=SHR                     
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                   
//CMPRT01  DD SYSOUT=&JCLO                                              
//CMPRT02  DD SYSOUT=&JCLO                                              
//CMPRT03  DD SYSOUT=&JCLO                                              
//CMPRT04  DD SYSOUT=&JCLO                                              
//CMPRT05  DD SYSOUT=&JCLO                                              
//SYSPRINT DD SYSOUT=&JCLO                                              
//SYSOUT   DD SYSOUT=&JCLO                                              
//CMPRINT  DD SYSOUT=&JCLO                                              
//DDPRINT  DD SYSOUT=&DMPS                                              
//SYSUDUMP DD SYSOUT=&DMPS                                              
