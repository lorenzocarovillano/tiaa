//PRD1200R  PROC REGN1='8M',
//          PARMMEM='XXXXXXXX',
//          DATA='XXXXX',
//          NATLOGON='PANNSEC',
//          NAT='NATB030',
//          SYS='NAT045P',
//          DBID=045,
//          LIB1='PROD',
//          HLQ1='PDA.ANN',
//          HLQ8='PPT.COR',
//          SPC1=50,
//          SPC2=10,
//          JCLO=*,
//          DMPS=U,
//          REPT=8
//********************************************************************
//*        INITIALIZE DATASETS TO BE USED
//********************************************************************
//DELT010  EXEC PGM=IEFBR14
//SYSPRINT DD SYSOUT=*
//D1       DD DSN=&HLQ8..RIDER.DOMA.FILE1,
//            UNIT=SYSDA,
//            DISP=(MOD,DELETE,DELETE),
//            SPACE=(CYL,(0,0))
//D2       DD DSN=&HLQ8..RIDER.DOMA.FILE2,
//            UNIT=SYSDA,
//            DISP=(MOD,DELETE,DELETE),
//            SPACE=(CYL,(0,0))
//D3       DD DSN=&HLQ8..RIDER.DOMA.FILE3,
//            UNIT=SYSDA,
//            DISP=(MOD,DELETE,DELETE),
//            SPACE=(CYL,(0,0))
//D4       DD DSN=&HLQ8..RIDER.DOMA.FILE4,
//            UNIT=SYSDA,
//            DISP=(MOD,DELETE,DELETE),
//            SPACE=(CYL,(0,0))
//D5       DD DSN=&HLQ8..RIDER.DOMA.FILE5,
//            UNIT=SYSDA,
//            DISP=(MOD,DELETE,DELETE),
//            SPACE=(CYL,(0,0))
//D6       DD DSN=&HLQ8..RIDER.DOMA.FILE6,
//            UNIT=SYSDA,
//            DISP=(MOD,DELETE,DELETE),
//            SPACE=(CYL,(0,0))
//D7       DD DSN=&HLQ8..RIDER.DOMA.FILE7,
//            UNIT=SYSDA,
//            DISP=(MOD,DELETE,DELETE),
//            SPACE=(CYL,(0,0))
//D8       DD DSN=&HLQ8..RIDER.DOMA.FILE8,
//            UNIT=SYSDA,
//            DISP=(MOD,DELETE,DELETE),
//            SPACE=(CYL,(0,0))
//*********************************************************************
//*        RIDP550 - READ DA REPORTING ACCESS EXTRACT
//*********************************************************************
//EXTR020  EXEC PGM=&NAT,COND=(0,NE),REGION=&REGN1,
//            PARM='SYS=&SYS,UDB=&DBID,WH=ON'
//SYSPRINT DD  SYSOUT=&JCLO
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//CMPRT01  DD  SYSOUT=&REPT
//CMPRT02  DD  SYSOUT=&REPT
//CMPRT03  DD  SYSOUT=&REPT
//CMWKF01  DD  DSN=&HLQ1..RIDER.DOMA.SORTED.&DATA,DISP=SHR
//CMWKF02  DD  DSN=&HLQ8..RIDER.DOMA.FILE1,
//             DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//             DCB=(RECFM=FB,LRECL=980)
//CMWKF03  DD  DSN=&HLQ8..RIDER.DOMA.FILE2,
//             DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//             DCB=(RECFM=FB,LRECL=980)
//CMWKF04  DD  DSN=&HLQ8..RIDER.DOMA.FILE3,
//             DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//             DCB=(RECFM=FB,LRECL=980)
//CMWKF05  DD  DSN=&HLQ8..RIDER.DOMA.FILE4,
//             DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//             DCB=(RECFM=FB,LRECL=980)
//CMWKF06  DD  DSN=&HLQ8..RIDER.DOMA.FILE5,
//             DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//             DCB=(RECFM=FB,LRECL=980)
//CMWKF07  DD  DSN=&HLQ8..RIDER.DOMA.FILE6,
//             DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//             DCB=(RECFM=FB,LRECL=980)
//CMWKF08  DD  DSN=&HLQ8..RIDER.DOMA.FILE7,
//             DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//             DCB=(RECFM=FB,LRECL=980)
//CMWKF09  DD  DSN=&HLQ8..RIDER.DOMA.FILE8,
//             DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//             DCB=(RECFM=FB,LRECL=980)
//SYSUDUMP DD  SYSOUT=&DMPS
//SYSOUT   DD  SYSOUT=&JCLO
//CMSYNIN  DD  DSN=&LIB1..NT2LOGON.PARMLIB(&NATLOGON),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(&PARMMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
