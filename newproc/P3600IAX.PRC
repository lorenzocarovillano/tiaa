//P3600IAX PROC NDMPARM='(YYSLYNN)',
//       YYYYMM='YYYYMM',
//        MFDSN='PIA.ANN.IA.P3600IAR(0)',
//        PCDSN='TIAAPENS-NY-EDP-MORT',
//     CNTLPARM='PROD.CNTL.PARMLIB',                                    
//      CNTLNDM='IA3600XP',                                             
//     DMPUBLIB='PROD.NDM.NY.PROCESS.LIB',
//     DMNETMAP='PMVSSF.STERLING.NDM.NETMAP',
//     DMMSGFIL='PMVSSF.STERLING.NDM.MSG',
//     REXXLIB1='PROD.REXXLIB',                                         
//     REXXLIB2='PROD.REXXLIB'                                          
//**********************************************************************
//* GENERATE NDM CONTROL CARDS
//*---------------------------------------------------------------------
//GENER010 EXEC PGM=IKJEFT01,PARM='%IA3600X1 &MFDSN &PCDSN &YYYYMM'
//*IN
//SYSTSIN  DD DUMMY
//*OUT
//NDM      DD DISP=SHR,DSN=&CNTLPARM(&CNTLNDM)
//SYSTSPRT DD SYSOUT=*
//*OTHER
//SYSEXEC  DD DISP=SHR,DSN=&REXXLIB1
//         DD DISP=SHR,DSN=&REXXLIB2
//*---------------------------------------------------------------------
//* SPOOL NDM CONTROL CARDS                                             
//*---------------------------------------------------------------------
//SPOOL020 EXEC PGM=SORT,COND=EVEN
//SYSOUT   DD SYSOUT=*
//SYSIN    DD DISP=SHR,DSN=PROD.PARMLIB(SORTCOPY)
//SORTIN   DD DISP=SHR,DSN=&CNTLPARM(&CNTLNDM)
//SORTOUT  DD SYSOUT=*
//*---------------------------------------------------------------------
//* INVOKE NDM
//*---------------------------------------------------------------------
//DMBATC30 EXEC PGM=DMBATCH,PARM=&NDMPARM,COND=(0,LT)
//DMNETMAP  DD DISP=SHR,DSN=&DMNETMAP
//DMPUBLIB  DD DISP=SHR,DSN=&DMPUBLIB
//DMMSGFIL  DD DISP=SHR,DSN=&DMMSGFIL
//DMPRINT   DD SYSOUT=*
//NDMCMDS   DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=*
//SYSIN     DD DISP=SHR,DSN=&CNTLPARM(&CNTLNDM)
//**********************************************************************
