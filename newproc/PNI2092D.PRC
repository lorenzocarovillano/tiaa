//PNI2092D PROC DBID=003,                                               
//         DMPS='U',                                                    
//         JCLO='*',                                                    
//         LIB1='PROD',                                                 
//         LIB2='PROD',                                                 
//         NAT='NAT003P',                                               
//         NATMEM='PANNSEC',                                            
//         HLQ1='PPM.ANN',
//         REGN1='0M',                                                  
//         SPC2='20,20',                                                
//         REPT='8',                                                    
//         GDG1='+1'                                                    
//*                                                                     
//*********************************************************************
//* PROCEDURE:                                                        *
//* ACIS JOB WILL CREATE AN AUDIT INTERFACE FILE FOR EXP-AG           *
//* THE JOB WILL CONTAIN AUDIT INFORMATION WHEN PLAN, SUBPLAN, SSN    *
//* AND DIV-SUB INFORMATION IS CHANGED DIRECTLY IN ACIS PRAP FILE     *
//** --------------------------------------------------------------- **
//* PROGRAMS EXECUTED: APPB112                                        *
//*********************************************************************
//* MODIFICATION                                                      *
//* ----------------------------------------------------------------- *
//*
//*********************************************************************
//UPDT010  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//   PARM='SYS=&NAT,UDB=&DBID,OBJIN=N'
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSPRINT DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&DMPS
//SYSOUT   DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMPRINT  DD  SYSOUT=&JCLO
//CMPRT01  DD  SYSOUT=(&REPT,NI2092D1)
//CMWKF01  DD  DSN=&HLQ1..PNI2092D.ACIS2EXP.DAILY(&GDG1),
//             DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,DATACLAS=DCPSEXTC,
//             SPACE=(CYL,(&SPC2),RLSE),
//             DCB=(RECFM=FB,LRECL=80)
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(NI2092D1),DISP=SHR
//*********************************************************************
