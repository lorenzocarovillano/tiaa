//P5050IAM PROC DBID='003',
//         DMPS='U',
//         GDG1='+1',
//         HLQ7='PPDT.PNA',
//         HLQ0='PIA.ANN',
//         JCLO='*',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='5M',
//         REPT='8',
//         UNIT1='CARTV'
//*
//* ----------------------------------------------------------------- *
//* PROCEDURE:  P5050IAM                 DATE:    04/10/96            *
//* ----------------------------------------------------------------- *
//* PROGRAMS EXECUTED:  NT2B030 (IAAP302)                             *
//* ----------------------------------------------------------------- *
//*                                                                     
//* MODIFICATION HISTORY
//* 04/11/02 - OIA JCL STANDARDS - PHIL STEINHAUSER
//* 05/2017  - PIN EXPANSION - REMOVED LRECL IN CMWKF02  OS             
//**********************************************************************
//*
//EXTR010 EXEC PGM=NATB030,REGION=&REGN1,
//        PARM='SYS=&NAT'
//SYSPRINT DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//DDPRINT  DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRINT  DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//SYSOUT   DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRT01  DD  SYSOUT=(&REPT,IA5050M1)
//CMPRT02  DD  SYSOUT=(&REPT,IA5050M2)
//CMWKF01  DD  DSN=&HLQ7..IAIQ.MASTER.FUND.DETAIL(&GDG1),
//             DISP=(NEW,CATLG,DELETE),                                 
//             DCB=(MODLDSCB,RECFM=VB),UNIT=&UNIT1                      
//CMWKF02  DD  DSN=&HLQ0..PNA.IAIQ.MASTER.FUND.SUMRY(&GDG1),
//             DISP=(NEW,CATLG,DELETE),                                 
//             UNIT=SYSDA,SPACE=(CYL,(550,110),RLSE),
//*            DCB=MODLDSCB,RECFM=FB,LRECL=323
//*//             DCB=MODLDSCB,RECFM=FB
//             DCB=(MODLDSCB,RECFM=FB)
//SYSUDUMP DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSABOUT DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSDBOUT DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPF&DBID),DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P5050IA1),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
