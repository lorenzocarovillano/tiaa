//P2280IAM PROC DBID='003',
//         DMPS='U',
//         HLQ0='PPDT',
//         JCLO='*',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='5M',
//         REPT='8'
//*
//* ----------------------------------------------------------------- *
//* PROCEDURE:  P2280IAM                 DATE:    03/15/96            *
//* ----------------------------------------------------------------- *
//* PROGRAMS EXECUTED:  NT2B030 (IAAP710)                             *
//* ----------------------------------------------------------------- *
//* MODIFICATION HISTORY
//* 04/08/02 - OIA JCL STANDARDS - PHIL STEINHAUSER
//* 11/15/13 - SAG CPU MONITORING- JUN TINIO
//*            ADDED CMWKF01 = PPDT.PNA.IAIQ.IATRANS.FINAL(0)           
//**********************************************************************
//*
//EXTR010  EXEC PGM=NATB030,REGION=&REGN1,
//        PARM='SYS=&NAT'
//SYSPRINT DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//DDPRINT  DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//SYSOUT   DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRINT  DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRT01  DD  SYSOUT=(&REPT,IA2280M1)
//CMPRT02  DD  SYSOUT=(&REPT,IA2280M2)
//CMPRT03  DD  SYSOUT=(&REPT,IA2280M3)
//CMWKF01  DD  DSN=&HLQ0..PNA.IAIQ.IATRANS.FINAL(0),DISP=SHR
//SYSUDUMP DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSABOUT DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSDBOUT DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P2280IA1),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
