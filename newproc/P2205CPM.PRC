//P2205CPM PROC GDG0='+0',
//         GDG1='+1',
//         HLQ0='PIA.ANN',
//         HLQ7='PPDT',
//         JCLO='*',
//         JOBNM='P2205CPM',
//         UNIT1='CARTV'
//*
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
//* PROCEDURE:  P2205CPM                 DATE:  02/29/96              *
//*-------------------------------------------------------------------*
//* BACKUP GROSS-TO-NET (GTN) IA CPS FILE
//*-------------------------------------------------------------------
//*                                                                     
//* MODIFICATION HISTORY
//* 04/19/02 - OIA JCL STANDARDS - PHIL STEINHAUSER
//*                                                                     
//**********************************************************************
//* COPY010 - COPY GTN FILE TO CART
//*******************************************************************
//*                                                                     
//COPY010  EXEC PGM=IEBGENER                                            
//SYSPRINT DD  SYSOUT=&JCLO
//SYSIN    DD  DUMMY                                                    
//SYSUT1   DD  DSN=&HLQ0..IAADMIN.GTN.S461(&GDG0),DISP=SHR              
//SYSUT2   DD  DSN=&HLQ7..&JOBNM..IAADMIN(&GDG1),UNIT=&UNIT1,           
//             DISP=(,CATLG,DELETE),                                    
//*//             DCB=MODLDSCB,RECFM=VB,LRECL=5250                         
//             DCB=(MODLDSCB,RECFM=VB,LRECL=5250)
