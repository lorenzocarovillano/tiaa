//P1765TWR PROC DBID='003',
//         DMPS='U',
//         HLQ0='PNPD.COR.TAX',
//         HLQ8='PPDD.TAX',
//         JCLO='*',
//         JOBNM1='P1765TWR',
//         JOBNM2='P1770TWR',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='8M',
//         REPT='8',
//         SPC1='1,0'
//*
//*---------------------------------------------------------------------
//*                                                                     
//* MODIFICATION HISTORY
//* 05/16/02 - OIA JCL STANDARDS - PHIL STEINHAUSER
//*                                                                     
//**********************************************************************
//* EXTR010 - CREATES A TEMPORARY UPDATED CONTROL RECORD EXTRACT FOR
//*           SOURCE CODE "GS", USING LAST YEAR'S EXECUTION CONTROL
//*           RECORD EXTRACT AS INPUT TO P1770TWR.
//*           NATURAL PROGRAM "TWRP0600".
//*---------------------------------------------------------------------
//EXTR010  EXEC PGM=NATB030,REGION=&REGN1,
//         PARM='SYS=&NAT'
//SYSOUT    DD SYSOUT=&JCLO
//SYSUDUMP  DD SYSOUT=&DMPS
//SYSPRINT  DD SYSOUT=&JCLO
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT   DD SYSOUT=&JCLO
//CMPRINT   DD SYSOUT=&JCLO
//CMPRT01   DD SYSOUT=&REPT
//CMPRT02   DD SYSOUT=&REPT
//CMPRT03   DD SYSOUT=&REPT
//CMWKF01   DD DSN=&HLQ0..&JOBNM2..CONTROL.EXT,DISP=SHR
//CMWKF02   DD DSN=&HLQ8..&JOBNM1..CONTROL.TEMP,
//             DISP=(NEW,CATLG,DELETE),
//             SPACE=(TRK,(&SPC1),RLSE),UNIT=SYSDA,
//*//             DCB=RECFM=FB,LRECL=80
//             DCB=(RECFM=FB,LRECL=80)
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(P1765TW1),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//* TWRP0600
//*---------------------------------------------------------------------
//* COPY020 - CREATES THE CONTROL DATE CARD RECORD FOR THE LOAD
//*           DEFAULTS ("GS") LOAD JOB "P1770TWR".
//*           UTILITY PROGRAM "IEBGENER".
//*---------------------------------------------------------------------
//COPY020  EXEC PGM=IEBGENER,COND=(0,NE)
//SYSPRINT  DD SYSOUT=&JCLO
//SYSIN     DD DUMMY
//SYSUT1    DD DSN=&HLQ8..&JOBNM1..CONTROL.TEMP,
//             DISP=SHR
//SYSUT2    DD DSN=&HLQ0..&JOBNM2..CONTROL.EXT,
//             DISP=SHR
