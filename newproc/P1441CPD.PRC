//P1441CPD PROC DBID=003,                                               
//        REPT='8',                                                     
//        JCLO='*',                                                     
//        DMPS='U',                                                     
//        NAT=NAT003P,                                                  
//        NATMEM=PANNSEC,                                               
//        LIB1=PROD,                                                    
//        LIB2=PROD,                                                    
//        LIB3=PROD,                                                    
//        JOBNM=P1441CPD,                                               
//        HLQ8=PPDD,                                                    
//        SPC1='35,15',                                                 
//        SPC2='50,30',                                                 
//        SPC3='10,5',                                                  
//        SPC4='30,10',                                                 
//        SPC5='7,3',                                                   
//        REGN1=9M                                                      
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
//*                                                                     
//* PROCEDURE:  P1441CPD                                                
//*                                                                     
//*-------------------------------------------------------------------- 
//* EXTR010 - EXTRACT "NZ" CPS PAYMENT DATA FOR END-OF-DAY              
//*           PROCESSING BASED ON SPECIFIED INTERFACE DATE.             
//*           NATURAL PROGRAM "FCPP120".                                
//* EXTR025 - PROCESS "NZ" FILE AND BUILD SORT-KEYS                     
//*            NATURAL PROGRAM FCPP182A                                 
//* SORT026 - SORT MERGED PAYMENT FILE                                  
//* REPT030 - "NZ" END-OF-DAY PAYMENT REGISTER.                         
//*           NATURAL PROGRAM "FCPP182".                                
//* SORT070 - SORT "NZ" FILE FOR TREASURY DIVISION EOD PAYMENT          
//*           REGISTER.                                                 
//* REPT080 - "NZ" TREASURY DIVISION END OF DAY PAYMENT REGISTER.       
//*           NATURAL PROGRAM "FCPP885".                                
//*-------------------------------------------------------------------- 
//*                                                                     
//* MODIFICATION HISTORY                                                
//* 04/30/02 - OIA JCL STANDARDS                                        
//*                                                                     
//**********************************************************************
//*                                                                     
//* EXTR010 - EXTRACT "NZ" CPS PAYMENT DATA FOR END-OF-DAY              
//*           PROCESSING BASED ON SPECIFIED INTERFACE DATE.             
//*           NATURAL PROGRAM "FCPP120".                                
//*-------------------------------------------------------------------- 
//EXTR010  EXEC PGM=NATB030,REGION=&REGN1,                              
//             PARM='SYS=&NAT'                                          
//SYSPRINT  DD SYSOUT=&JCLO                                             
//SYSUDUMP  DD SYSOUT=&DMPS                                             
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//DDPRINT   DD SYSOUT=&JCLO                                             
//CMPRINT   DD SYSOUT=&JCLO                                             
//CMPRT01   DD SYSOUT=&REPT                                             
//CMWKF03   DD DSN=&HLQ8..&JOBNM..PAYMENT.NZ,                           
//             LRECL=6200,RECFM=VB,BUFNO=25,                            
//             DISP=(NEW,CATLG,DELETE),                                 
//             SPACE=(CYL,(&SPC1),RLSE),UNIT=SYSDA                      
//*                                                                     
//CMWKF05   DD DUMMY,LRECL=6200,RECFM=VB                                
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR            
//          DD DSN=&LIB1..PARMLIB(P1441CPA),DISP=SHR                    
//          DD DSN=&LIB3..CNTL.PARMLIB(P1441CP1),DISP=SHR               
//* FCPP120                                                             
//* NZ PAYMENT                                                          
//* 19971201  - INTERFACE DATE                                          
//*-------------------------------------------------------------------- 
//*  EXTR025 - PROCESS "NZ" FILE AND BUILD SORT-KEYS                    
//*            NATURAL PROGRAM FCPP182A                                 
//*-------------------------------------------------------------------- 
//EXTR025  EXEC PGM=NATB030,REGION=&REGN1,                              
//             PARM='SYS=&NAT'                                          
//SYSPRINT  DD SYSOUT=&JCLO                                             
//SYSUDUMP  DD SYSOUT=&DMPS                                             
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//DDPRINT   DD SYSOUT=&JCLO                                             
//CMPRINT   DD SYSOUT=&JCLO                                             
//CMPRT01   DD SYSOUT=&REPT                                             
//CMWKF01   DD DSN=&HLQ8..&JOBNM..PAYMENT.NZ,DISP=SHR                   
//*                                                                     
//CMWKF02   DD DSN=&HLQ8..&JOBNM..NZ.PYMNT.MERGE,                       
//             LRECL=6014,RECFM=VB,BUFNO=15,                            
//             DISP=(NEW,CATLG,DELETE),                                 
//             SPACE=(CYL,(&SPC2),RLSE),UNIT=SYSDA                      
//CMWKF03   DD DSN=&HLQ8..&JOBNM..NZ.PYMNT.HEADER,                      
//             LRECL=6014,RECFM=VB,BUFNO=15,                            
//             DISP=(NEW,CATLG,DELETE),                                 
//             SPACE=(CYL,(&SPC3),RLSE),UNIT=SYSDA                      
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR            
//          DD DSN=&LIB1..PARMLIB(P1441CPL),DISP=SHR                    
//* FCPP182A                                                            
//*-------------------------------------------------------------------- 
//*  SORT026 - SORT MERGED PAYMENT FILE                                 
//*-------------------------------------------------------------------- 
//SORT026  EXEC PGM=SORT,REGION=&REGN1,COND=(0,NE)                      
//SYSOUT    DD SYSOUT=&JCLO                                             
//SORTWK01  DD UNIT=SYSDA,SPACE=(CYL,(&SPC4))                           
//SORTWK02  DD UNIT=SYSDA,SPACE=(CYL,(&SPC4))                           
//SORTWK03  DD UNIT=SYSDA,SPACE=(CYL,(&SPC4))                           
//SORTWK04  DD UNIT=SYSDA,SPACE=(CYL,(&SPC4))                           
//SYSPRINT  DD SYSOUT=&JCLO                                             
//SORTIN    DD DSN=&HLQ8..&JOBNM..NZ.PYMNT.MERGE,DISP=SHR               
//          DD DSN=&HLQ8..&JOBNM..NZ.PYMNT.HEADER,DISP=SHR              
//SORTOUT   DD DSN=&HLQ8..&JOBNM..NZ.PYMNT.MERGE.SORT,                  
//             LRECL=6014,RECFM=VB,BUFNO=15,                            
//             DISP=(NEW,CATLG,DELETE),                                 
//             SPACE=(CYL,(&SPC2),RLSE),UNIT=SYSDA                      
//SYSIN     DD DSN=&LIB1..PARMLIB(P1441CPM),DISP=SHR                    
//*                                                                     
//* SORT FIELDS=(  5,10,CH,A,  SORT-KEY                        1+4,10   
//*              186,2,CH,A,   CNTRCT-TYPE-CDE               182+4,2    
//*              359,4,PD,A,   PYMNT-CHECK-DTE               355+4,4    
//*               15,10,CH,A,  CNTRCT-PPCN-NBR                11+4,10   
//*               25,4,CH,A,   CNTRCT-PAYEE-CDE               21+4,4    
//*               37,9,CH,A)   PYMNT-PRCSS-SEQ-NBR            33+4,9    
//*                                                                     
//*-------------------------------------------------------------------- 
//* REPT030 - "NZ" END-OF-DAY PAYMENT REGISTER.                         
//*           NATURAL PROGRAM "FCPP182".                                
//*-------------------------------------------------------------------- 
//REPT030  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),                  
//             PARM='SYS=&NAT'                                          
//SYSPRINT  DD SYSOUT=&JCLO                                             
//SYSUDUMP  DD SYSOUT=&DMPS                                             
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//DDPRINT   DD SYSOUT=&JCLO                                             
//CMPRINT   DD SYSOUT=&JCLO                                             
//CMPRT01   DD SYSOUT=&REPT                                             
//CMPRT02   DD SYSOUT=&REPT                                             
//CMWKF01   DD DSN=&HLQ8..&JOBNM..NZ.PYMNT.MERGE.SORT,DISP=SHR          
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR            
//          DD DSN=&LIB1..PARMLIB(P1441CPC),DISP=SHR                    
//* FCPP182                                                             
//* END-OF-DAY                                                          
//*-------------------------------------------------------------------- 
//* SORT070 - SORT FOR "NZ" TREASURY DIVISION PAYMENT REGISTER.         
//*-------------------------------------------------------------------- 
//SORT070  EXEC PGM=SORT,REGION=&REGN1,COND=(0,NE)                      
//SYSOUT    DD SYSOUT=&JCLO                                             
//SORTWK01  DD UNIT=SYSDA,SPACE=(CYL,(&SPC4))                           
//SORTWK02  DD UNIT=SYSDA,SPACE=(CYL,(&SPC4))                           
//SORTWK03  DD UNIT=SYSDA,SPACE=(CYL,(&SPC4))                           
//SORTWK04  DD UNIT=SYSDA,SPACE=(CYL,(&SPC4))                           
//SYSPRINT  DD SYSOUT=&JCLO                                             
//SORTIN    DD DSN=&HLQ8..&JOBNM..PAYMENT.NZ,DISP=SHR                   
//*                                                                     
//SORTOF1   DD DSN=&HLQ8..&JOBNM..PAYMENT.NZ.SORT,                      
//             LRECL=6200,RECFM=VB,BUFNO=25,                            
//             DISP=(NEW,CATLG,DELETE),                                 
//             SPACE=(CYL,(&SPC1),RLSE),UNIT=SYSDA                      
//*                                                                     
//SORTOF2   DD DSN=&HLQ8..&JOBNM..PAYMENT.NZ.SORT.INTROLL,              
//             LRECL=6200,RECFM=VB,BUFNO=25,                            
//             DISP=(NEW,CATLG,DELETE),                                 
//             SPACE=(CYL,(&SPC5),RLSE),UNIT=SYSDA                      
//SYSIN     DD DSN=&LIB1..PARMLIB(P1441CPG),DISP=SHR                    
//* SORT FIELDS=(336,1,CH,A,376,7,CH,A,369,7,CH,A)                      
//* INCLUDE COND=(34,2,CH,EQ,C'01')                                     
//* OUTFIL FILES=1,                                                     
//* INCLUDE=(217,2,CH,EQ,C'  ',AND,340,1,CH,NE,C'8')                    
//* OUTFIL FILES=2,                                                     
//* INCLUDE=(217,2,CH,EQ,C'  ',AND,340,1,CH,EQ,C'8')                    
//*                                                                     
//* PYMNT-FTRE-IND                       332+4,1                        
//* PYMNT-CHECK-NBR                      365+4,7                        
//* PYMNT-CHECK-SCRTY-NBR                372+4,7                        
//* PYMNT-INSTMT-NBR                      30+4,2                        
//*-------------------------------------------------------------------- 
//* REPT080 - "NZ" TREASURY DIVISION END-OF-DAY PAYMENT REGISTER.       
//*           NATURAL PROGRAM "FCPP885".                                
//*-------------------------------------------------------------------- 
//REPT080  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),                  
//             PARM='SYS=&NAT'                                          
//SYSPRINT  DD SYSOUT=&JCLO                                             
//SYSUDUMP  DD SYSOUT=&DMPS                                             
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//DDPRINT   DD SYSOUT=&JCLO                                             
//CMPRINT   DD SYSOUT=&JCLO                                             
//CMPRT01   DD SYSOUT=&REPT                                             
//CMPRT02   DD SYSOUT=&REPT                                             
//CMWKF01   DD DSN=&HLQ8..&JOBNM..PAYMENT.NZ.SORT,DISP=SHR              
//          DD DSN=&HLQ8..&JOBNM..PAYMENT.NZ.SORT.INTROLL,DISP=SHR      
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR            
//          DD DSN=&LIB1..PARMLIB(P1441CPH),DISP=SHR                    
//* FCPP885                                                             
//* NZ  END-OF-DAY                                                      
