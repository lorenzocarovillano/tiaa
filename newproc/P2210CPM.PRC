//P2210CPM PROC DBID='003',
//         DMPS='U',
//         GDG0='+0',
//         GDG1='+1',
//         HLQ0='PIA.ANN',
//         HLQ1='PIA.ANN.CPS',
//         HLQVS='PCPS',
//*        HLQ7A='PPDT',
//*        HLQ7B='PPDT.ANN',
//         HLQ8='PPDD',
//         JCLO='*',
//         JOBNM='P2210CPM',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='9M',
//         REGN2='32M',
//         REPT='8',
//         SPC1='200,50',
//         SPC2='800,75',
//         SPC3='75,25',
//         SPC4='50,55',
//         SPC5='154,50'
//*        SPC6='900,50',
//*        UNIT1='CARTV'
//*
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
//*                                                                   *
//* PROCEDURE:  P2210CPM                 DATE:  02/29/96              *
//*                                                                   *
//*********************************************************************
//*
//* CPS IA GENERATE FINANCIAL FILES
//*
//* SORT010 - SORT THE GROSS-TO-NET'ED IA PAYMENT FILE
//*           PYMNT-PRCSS-SEQ-NBR WITHIN CNTRCT-CMBN-NBR
//* EXTR020 - READ IAR PAYMENT FILE AFTER GTN AND SEPARATE IT INTO
//*           MULTIPLE FILES. GENERATE LEDGERS AND CREATE ANNOTATIONS.
//*           REJECT ERRORS INTO ERROR FILE.
//*           NATURAL PROGRAM  FCPP801
//*           USES PARM:     99999
//*           WRITE NEW RECON FILE WK 12
//* SORT030 - SORT PAYMENT FOR COMBINED CONTRACT WITHIN DATE
//*
//* REPT040 - CALCULATE CHECK FIELDS
//*           NATURAL PROGRAM FCPP802
//* SORT050 - SORT THE "CHECK FILE WITH EXTRA FIELDS"
//*           INTO STATEMENT PRODUCING SEQUENCE
//* COPY060 - COPY THE TAX FILE FROM THIS PROCESS AS
//*           THE NEXT GENERATION FILE FOR TAX SYSTEM
//* COPY070 - COPY THE SORTED "CHECKS WITH EXTRA FIELDS"
//*           TO.......
//*                                                                     
//**********************************************************************
//*
//* MODIFICATION HISTORY
//* 04/19/02 - OIA JCL STANDARDS - PHIL STEINHAUSER
//*                                                                     
//* 01/15/2020 - CTS     - CPS SUNSET
//*******************************************************************
//DELT000  EXEC PGM=IEFBR14
//DD01 DD  DSN=&HLQ0..&JOBNM..LEDGER.EXTRCT,
//  SPACE=(TRK,(1,1),RLSE),DISP=(MOD,DELETE,DELETE)
//DD02 DD  DSN=&HLQ0..&JOBNM..LEDGER.EXTRCT.SRTD,
//  SPACE=(TRK,(1,1),RLSE),DISP=(MOD,DELETE,DELETE)
//DD03 DD  DSN=&HLQ0..&JOBNM..LEDGER.EXTRCT.DUPS,
//  SPACE=(TRK,(1,1),RLSE),DISP=(MOD,DELETE,DELETE)
//DD04 DD  DSN=&HLQ0..&JOBNM..LEDGER.BACKUP,
//  SPACE=(TRK,(1,1),RLSE),DISP=(MOD,DELETE,DELETE)
//*******************************************************************
//* SORT010 - SORT THE GROSS-TO-NET'ED IA PAYMENT FILE
//*           PYMNT-PRCSS-SEQ-NBR WITHIN CNTRCT-CMBN-NBR
//*******************************************************************
//SORT010 EXEC PGM=SORT,COND=(0,NE),REGION=&REGN1
//SYSOUT   DD  SYSOUT=&JCLO
//SYSPRINT DD  SYSOUT=&JCLO
//SORTWK01 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC1)),DCB=BUFNO=10
//SORTWK02 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC1)),DCB=BUFNO=10
//SORTWK03 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC1)),DCB=BUFNO=10
//SORTWK04 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC1)),DCB=BUFNO=10
//SORTWK05 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC1)),DCB=BUFNO=10
//SORTWK06 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC1)),DCB=BUFNO=10
//SORTIN   DD  DSN=&HLQ0..IAADMIN.GTN.S462(&GDG0),
//             DISP=SHR,DCB=BUFNO=20
//SORTOUT  DD  DSN=&HLQ8..&JOBNM..GTN,                         OUTPUT
//             DISP=(,CATLG,DELETE),
//             UNIT=(SYSDA,3),SPACE=(CYL,(&SPC2),RLSE),
//             DCB=(LRECL=5300,RECFM=VB,BUFNO=25)
//SYSIN    DD  DSN=&LIB1..PARMLIB(P2210CP1),DISP=SHR
//* SORT FIELDS=(29,14,CH,A,68,9,CH,D)
//* CNTRCT-CMBN-NBR           25+4,14
//* PYMNT-PRCSS-SEQ-NBR       64+4,9
//********************************************************************
//* EXTR020 - READ IAR PAYMENT FILE AFTER GTN AND SEPARATE IT INTO
//*           MULTIPLE FILES. GENERATE LEDGERS AND CREATE ANNOTATIONS.
//*           REJECT ERRORS INTO ERROR FILE.
//*           NATURAL PROGRAM  FCPP801
//*           USES PARM:     99999
//********************************************************************
//EXTR020 EXEC PGM=NATB030,COND=(0,NE),REGION=&REGN2,
//            PARM=('SYS=&NAT,UDB=003,OBJIN=N,IM=D')
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSOUT   DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//SYSPRINT DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//CMPRT15  DD  SYSOUT=&REPT
//CMWKF01  DD  DSN=&HLQ8..&JOBNM..GTN,DISP=SHR,DCB=BUFNO=15
//CMWKF02  DD  DSN=&HLQ0..&JOBNM..REJECT.ERRORS.S464,DISP=SHR
//*CMWKF03  DD  DSN=&HLQ0..&JOBNM..TAX.S465,DISP=SHR
//CMWKF04  DD  DSN=&HLQ0..&JOBNM..PENDS.S487,DISP=SHR
//CMWKF05  DD  DSN=&HLQ0..&JOBNM..EFT.S466,DISP=SHR
//CMWKF06  DD  DSN=&HLQ0..&JOBNM..CHECKS.S488,DISP=SHR
//CMWKF07  DD  DSN=&HLQ0..&JOBNM..COLLEGE.DVDND.S467,DISP=SHR
//*CMWKF08  DD  DSN=&HLQ0..&JOBNM..ANNOT.S468,DISP=SHR
//CMWKF09  DD  DSN=&HLQ0..&JOBNM..LEDGER.S469,DISP=SHR
//*CMWKF10  DD  DSN=&HLQ0..&JOBNM..GLOBAL.S470,DISP=SHR
//CMWKF11  DD  DSN=&HLQ0..&JOBNM..BEFORE.CHECK.S471,DISP=SHR
//CMWKF12  DD  DSN=&HLQ0..&JOBNM..LEDGER2.MONTHLY(&GDG1),
//             DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             UNIT=SYSDA,SPACE=(CYL,(&SPC2),RLSE),
//             DCB=(LRECL=100,RECFM=FB,BUFNO=25)
//CMWKF13  DD  DSN=&HLQ0..&JOBNM..GLOBAL.S470,DISP=SHR
//CMWKF14  DD  DSN=&HLQ1..CNTL.FILE(&GDG0),
//             DISP=SHR
//CMWKF15  DD  DSN=&HLQ0..&JOBNM..CONTROLS.S472,DISP=OLD
//*CMWKF16  DD  DSN=&HLQ0..&JOBNM..TEXAS.CHILD.OUT(&GDG1),
//*            DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//*            UNIT=SYSDA,SPACE=(CYL,(&SPC2),RLSE),
//*            DCB=(LRECL=200,RECFM=FB,BUFNO=25)
//*            DCB=(LRECL=205,RECFM=FB,BUFNO=25)        PIN EXPANSION
//CMWKF17  DD  DSN=&HLQ0..&JOBNM..LEDGER.EXTRCT,
//             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             UNIT=SYSDA,SPACE=(CYL,(&SPC2),RLSE),
//             DCB=(LRECL=4034,RECFM=FB,BUFNO=25)
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P2210CP2),DISP=SHR
//* FCPP801 99999
//*********************************************************************
//* SORT030 - SORT PAYMENT FOR COMBINED CONTRACT WITHIN DATE
//*
//*********************************************************************
//SORT030 EXEC PGM=SORT,COND=(0,NE),REGION=&REGN1
//SYSOUT   DD  SYSOUT=&JCLO
//SYSPRINT DD  SYSOUT=&JCLO
//SORTWK01 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC3)),DCB=BUFNO=10
//SORTWK02 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC3)),DCB=BUFNO=10
//SORTWK03 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC3)),DCB=BUFNO=10
//SORTIN   DD  DSN=&HLQ0..&JOBNM..CHECKS.S488,
//             DISP=SHR,DCB=BUFNO=24
//SORTOUT  DD  DSN=&HLQ8..&JOBNM..SRTCHK,
//             DISP=(NEW,CATLG,DELETE),
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE),
//             DCB=(LRECL=4800,RECFM=VB,BUFNO=25)
//SYSIN    DD  DSN=&LIB1..PARMLIB(P2210CP3),DISP=SHR
//* SORT FIELDS=(94,2,CH,A,110,8,CH,A,79,14,CH,A,9,1,CH,D,118,9,CH,A)
//* CNTRCT-ORGN-CDE           90+4,2
//* CNTRCT-INVRSE-DTE        106+4,8
//* CNTRCT-CMBN-NBR           75+4,14
//* CNTRCT-COMPANY-CDE         5+4,1
//* PYMNT-PRCSS-SEQ-NBR      114+4,9
//*********************************************************************
//* REPT040 - CALCULATE CHECK FIELDS
//*           NATURAL PROGRAM FCPP802
//*********************************************************************
//REPT040 EXEC PGM=NATB030,COND=(0,NE),REGION=&REGN1,
//            PARM='SYS=&NAT,UDB=003,OBJIN=N,IM=D'
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSOUT   DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//SYSPRINT DD  SYSOUT=&JCLO
//SORTWK01 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC4)),DCB=BUFNO=10
//SORTWK02 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC4)),DCB=BUFNO=10
//SORTWK03 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC4)),DCB=BUFNO=10
//SORTWK04 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC4)),DCB=BUFNO=10
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//CMWKF01  DD  DSN=&HLQ8..&JOBNM..SRTCHK,
//             DISP=SHR,DCB=BUFNO=24
//CMWKF02  DD  DSN=&HLQ8..&JOBNM..CHECKS.EXTRA.FIELDS,
//             DISP=(,CATLG,DELETE),UNIT=SYSDA,
//             SPACE=(CYL,(&SPC1),RLSE),
//             LRECL=4875,RECFM=VB
//CMWKF03  DD  DSN=&HLQ8..&JOBNM..CHECKS.NEW.FIELDS,
//             DISP=(,CATLG,DELETE),UNIT=SYSDA,
//             SPACE=(CYL,(&SPC1),RLSE),
//             LRECL=4746,RECFM=FB
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P2210CP4),DISP=SHR
//* FCPP802
//*********************************************************************
//* SORT050 - SORT THE "CHECK FILE WITH EXTRA FIELDS"
//*           INTO STATEMENT PRODUCING SEQUENCE
//*********************************************************************
//SORT050 EXEC PGM=SORT,COND=(0,NE),REGION=&REGN1
//SYSOUT   DD  SYSOUT=&JCLO
//SYSPRINT DD  SYSOUT=&JCLO
//SORTWK01 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC4)),DCB=BUFNO=10
//SORTWK02 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC4)),DCB=BUFNO=10
//SORTWK03 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC4)),DCB=BUFNO=10
//SORTWK04 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC4)),DCB=BUFNO=10
//SORTIN   DD  DSN=&HLQ8..&JOBNM..CHECKS.EXTRA.FIELDS,
//             DISP=SHR,DCB=BUFNO=24
//SORTOUT  DD  DSN=&HLQ0..&JOBNM..SRTDXTRA(&GDG1),
//             DISP=(,CATLG,DELETE),
//*//             DCB=MODLDSCB,LRECL=4875,RECFM=VB,
//             DCB=(MODLDSCB,LRECL=4875,RECFM=VB),
//             UNIT=SYSDA,
//             SPACE=(CYL,(&SPC5),RLSE)
//SYSIN    DD  DSN=&LIB1..PARMLIB(P2210CP5),DISP=SHR
//* SORT FIELDS=(5,4,CH,A,10,2,PD,A,94,2,CH,A,110,8,CH,A,79,14,CH,A,
//*              9,1,CH,D,118,9,CH,A)
//* CNTRCT-HOLD-CDE            1+4,4
//* PYMNT-TOTAL-PAGES          6+4,2
//* CNTRCT-ORGN-CDE           90+4,2
//* CNTRCT-INVRSE-DTE        106+4,8
//* CNTRCT-CMBN-NBR           75+4,14
//* CNTRCT-COMPANY-CDE         5+4,1     DESC
//* PYMNT-PRCSS-SEQ-NBR      118+4,9
//*********************************************************************
//* COPY060 - COPY THE TAX FILE FROM THIS PROCESS AS
//*           THE NEXT GENERATION FILE FOR TAX SYSTEM
//*********************************************************************
//*COPY060 EXEC PGM=ICEGENER,COND=(0,NE)
//*SYSPRINT DD  SYSOUT=&JCLO
//*SYSIN    DD  DUMMY
//*SYSUT1   DD  DSN=&HLQ0..&JOBNM..TAX.S465,DISP=SHR
//*SYSUT2   DD  DSN=&HLQ0..&JOBNM..TAX(&GDG1),
//*             DISP=(NEW,CATLG,DELETE),
//*             UNIT=(SYSDA,5),SPACE=(CYL,(&SPC6),RLSE),
//*             DCB=MODLDSCB,RECFM=VB,LRECL=5250
//*********************************************************************
//* COPY070 - COPY THE SORTED "CHECKS WITH EXTRA FIELDS"
//*           TO.......
//*********************************************************************
//*COPY070  EXEC PGM=ICEGENER,COND=(0,NE)
//*SYSPRINT DD  SYSOUT=&JCLO
//*SYSIN    DD  DUMMY
//*SYSUT1   DD  DSN=&HLQ0..&JOBNM..SRTDXTRA(&GDG1),
//*             DISP=SHR
//*SYSUT2   DD  DSN=&HLQ7A..&JOBNM..SRTDVR(&GDG1),
//*             DISP=(,CATLG,DELETE),
//*             UNIT=&UNIT1,
//*             DCB=MODLDSCB,LRECL=4875,RECFM=VB
//*********************************************************************
//*COPY075 EXEC PGM=ICEGENER,COND=(0,NE)
//*SYSPRINT DD  SYSOUT=&JCLO
//*SYSIN    DD  DUMMY
//*SYSUT1   DD  DSN=&HLQ0..&JOBNM..TAX(&GDG1),DISP=SHR
//*SYSUT2   DD  DSN=&HLQ7B..&JOBNM..TAX(&GDG1),
//*             DISP=(NEW,CATLG,DELETE),UNIT=&UNIT1,
//*             DCB=MODLDSCB,RECFM=VB,LRECL=5250
//*******************************************************************
//* RPRO090 - COPY THE EXISTING REVERSAL LEDGER VSAM DATA.
//*******************************************************************
//RPRO090   EXEC PGM=IDCAMS,COND=(0,NE)
//SYSPRINT  DD SYSOUT=&JCLO
//INPUT     DD DISP=SHR,DSN=&HLQVS..LEDGER.REV.VSAM.BATCH
//OUTPUT    DD DSN=&HLQ0..&JOBNM..LEDGER.BACKUP,
//             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             UNIT=SYSDA,SPACE=(CYL,(&SPC2),RLSE),
//             DCB=(LRECL=4034,RECFM=FB,BUFNO=25)
//SYSIN     DD DSN=PROD.PARMLIB(REPRO),DISP=SHR
//*******************************************************************
//* ICTL100 - SORT THE LEDGER EXTRACT FILE ON VSAM KEY
//*           WORK FILE 17 OUT OF FCPP801 (DD-CMWKF17,STEP-EXTR020)
//*           ADD EXISTING DATA FROM VSAM (DD-OUTPUT,STPE-RPRO090)
//*******************************************************************
//ICTL100 EXEC PGM=ICETOOL,COND=(0,NE),REGION=&REGN1
//SYSOUT   DD  SYSOUT=&JCLO
//SYSPRINT DD  SYSOUT=&JCLO
//TOOLMSG  DD  SYSOUT=&JCLO
//DFSMSG   DD  SYSOUT=&JCLO
//SYSABOUT DD  SYSOUT=&DMPS
//SYSDBOUT DD  SYSOUT=&DMPS
//SYSUDUMP DD  SYSOUT=&DMPS
//SORTWK01 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC1)),DCB=BUFNO=10
//SORTWK02 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC1)),DCB=BUFNO=10
//SORTWK03 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC1)),DCB=BUFNO=10
//SORTWK04 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC1)),DCB=BUFNO=10
//SORTWK05 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC1)),DCB=BUFNO=10
//SORTWK06 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC1)),DCB=BUFNO=10
//SORTIN   DD  DSN=&HLQ0..&JOBNM..LEDGER.BACKUP,
//             DISP=SHR,DCB=BUFNO=20
//         DD  DSN=&HLQ0..&JOBNM..LEDGER.EXTRCT,
//             DISP=SHR,DCB=BUFNO=20
//SORTOUT  DD  DSN=&HLQ0..&JOBNM..LEDGER.EXTRCT.SRTD,
//             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             UNIT=SYSDA,SPACE=(CYL,(&SPC2),RLSE),
//             DCB=(LRECL=4034,RECFM=FB,BUFNO=25)
//SORTXSUM DD  DSN=&HLQ0..&JOBNM..LEDGER.EXTRCT.DUPS,
//             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             UNIT=SYSDA,SPACE=(CYL,(&SPC2),RLSE),
//             DCB=(LRECL=4034,RECFM=FB,BUFNO=25)
//TOOLIN   DD  DSN=&LIB1..PARMLIB(P2210CP9),DISP=SHR
//*---------------------------------------------------------------
//* SORT FILE ON BELOW FIELDS, AND REMOVE DUPLICATES TO 'SORTXSUM'
//*---------------------------------------------------------------
//*FIELD NAME                           FORMAT       START  LENGTH
//*---------------------------------------------------------------
//*LGR-CNTRCT-PPCN-NBR                 C    10           1      10
//*LGR-PAYEE-CODE                      C     4          11       4
//*LGR-CNTRCT-ORGN-CDE                 C     2          15       2
//*LGR-PYMNT-CHECK-DTE                 C     8          17       8
//*LGR-PYMNT-PRCSS-SEQ-NBR             Z     9          25       9
//*******************************************************************
//* DELETE AND DEFINE THE S462 VSAM FILE
//*******************************************************************
//DEFINE   EXEC  PGM=IDCAMS,COND=(0,NE)
//SYSPRINT  DD  SYSOUT=&JCLO
//SYSIN     DD  DSN=&LIB1..PARMLIB(P2210CPR),DISP=SHR
//*
//*******************************************************************
//* RPRU110 - INSERT REVERSAL LEDGER VSAM DATA BACK TO VSAM
//*           ALONG WITH IA MONTHLY PROCESSED DATA.
//*******************************************************************
//RPRU110   EXEC PGM=IDCAMS,COND=(0,NE)
//SYSPRINT  DD SYSOUT=&JCLO
//INPUT     DD DISP=SHR,DSN=&HLQ0..&JOBNM..LEDGER.EXTRCT.SRTD
//OUTPUT    DD DISP=SHR,DSN=&HLQVS..LEDGER.REV.VSAM.BATCH
//SYSIN     DD DSN=PROD.PARMLIB(REPRO),DISP=SHR
//*
//************************END OF PROC********************************
