//P0600CPD  PROC SYSOUT='*'
//*       REPORT='8',                                                   
//*       SYSMSGS='U'                                                   
//***************************************************************
//* COPY THE POSITIVE PAY CLEARED CHECK UPLOAD INTO A CORP DSN  *
//***************************************************************
//COPY010  EXEC PGM=IEBGENER
//SYSPRINT DD SYSOUT=&SYSOUT
//SYSUT1   DD  DSN=PPDD.CPS.ANN.CLEARED.CHECKS,DISP=SHR
//SYSUT2   DD  DSN=PMS.ANN.P0600CPD.CLEARED.CHECKS(+1),
//         DISP=(,CATLG,DELETE),
//         UNIT=SYSDA,SPACE=(CYL,(20,5))
//**       RECFM=FB,LRECL=80
//SYSIN    DD DUMMY
//*************************************************************
//* DELETE THE PPDD DSN                                      **
//*************************************************************
//DELT020  EXEC PGM=IEFBR14,COND=(0,NE)
//SYSPRINT DD  SYSOUT=&SYSOUT
//DD01     DD  DSN=PPDD.CPS.ANN.CLEARED.CHECKS,
//         DISP=(OLD,DELETE,DELETE)
