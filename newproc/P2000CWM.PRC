//P2000CWM  PROC REGN1=9M,
//          NAT=NAT045P,
//          NATMEM=PANNSEC,
//          DBID=045,
//          JOBNM='P2000CWM',
//          HLQ8='PPDD',
//          LIB1=PROD,
//          LIB2=PROD,
//          DMPS='U',
//          REPT='8',
//          JCLO=*,
//          SPC1='150,90',
//          SPC2='10,10',
//          SPC3='10,10',
//          SPC4='10,10'
//**********************************************************************
//* STEP ID      : EXTR010                                             *
//* DESCRIPTION  : EXTRACT RECORDS FOR FOLLOW UP OF EXT. PENDED CASES  *
//* PROGRAM NAME : CWFB3215                                            *
//* ADABAS FILES : DB045 - FILE 181 CWF-MASTER-INDEX-VIEW              *
//* ADABAS FILES : DB003 - FILE 160 PH-INFORMATION-FILE (PIF)          *
//* DATASETS     : CMWKF01 DSN=PPDD.P2000CWM.EXTR010                   *
//* REMARKS      : DDCARD MEMBER DPAPF045 INVOKES PREFETCH             *
//* -------------------------------------------------------------------*
//* CHANGES      :                                                     *
//*1/19/95 DELETED EXTR010.CMPRT01 DD STATEMENT AS PER J. PATINGO. A.A.*
//*3/04/96 ADD STEP REPT040 - TURNAROUND PROCESSING REPORT FOR P. JENSEN
//*7/06/98 REMOVE STEP REPT040 - REPT PRODUCED IN P2010CWM NOW JVH
//* -------------------------------------------------------------------*
//EXTR010  EXEC PGM=NATB030,REGION=&REGN1,
//         PARM='SYS=&NAT'
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPF&DBID),DISP=SHR
//SYSPRINT DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMWKF01  DD  DSN=&HLQ8..&JOBNM..EXTR010,
//             DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,
//             SPACE=(CYL,(&SPC1),RLSE),
//             RECFM=FB,LRECL=256
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P2000CWA),DISP=SHR
//**********************************************************************
//* STEP ID      : REPT020                                             *
//* DESCRIPTION  : PRINTS REPORT FOR FOLLOW UP OF EXT. PENDED CASES    *
//* PROGRAM NAME : CWFB3216 (SORTED BY UNIT)                           *
//* ADABAS FILE  : DB045 - FILE 182 CWF-WP-WORK-PRCSS-ID               *
//*              : DB045 - FILE 184 CWF-ORG-UNIT-TBL                   *
//*              : DB045 - FILE 184 CWF-ORG-EMPL-TBL                   *
//* DATASETS     : CMWKF01 DSN=PPDD.P2000CWM.EXTR010
//* -------------------------------------------------------------------*
//* CHANGES      :                                                     *
//* -------------------------------------------------------------------*
//REPT020  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//         PARM='SYS=&NAT'
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSPRINT DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMPRT01  DD  SYSOUT=(&REPT,CW2000M1)
//CMWKF01  DD  DSN=&HLQ8..&JOBNM..EXTR010,DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P2000CWB),DISP=SHR
//**********************************************************************
//* STEP ID      : REPT025                                             *
//* DESCRIPTION  : PRINTS REPORT FOR FOLLOW UP OF EXT. PENDED CASES    *
//* PROGRAM NAME : CWFB3216 (SORTED BY UNIT/EMPLOYEE W/IN UNIT)        *
//* ADABAS FILE  : DB045 - FILE 182 CWF-WP-WORK-PRCSS-ID               *
//*              : DB045 - FILE 184 CWF-ORG-UNIT-TBL                   *
//*              : DB045 - FILE 184 CWF-ORG-EMPL-TBL                   *
//* DATASETS     : CMWKF01 DSN=PPDD.P2000CWM.EXTR010
//* -------------------------------------------------------------------*
//* CHANGES      :                                                     *
//* -------------------------------------------------------------------*
//REPT025  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//         PARM='SYS=&NAT'
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSPRINT DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMPRT01  DD  SYSOUT=(&REPT,CW2000M2)
//CMWKF01  DD  DSN=&HLQ8..&JOBNM..EXTR010,DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P2000CWC),DISP=SHR
//**********************************************************************
//* STEP        : REPT030
//* PARM        : P2000CWD
//* PROGRAM     : CWFB3803
//* DESCRIPTION : THIS PRODUCES THE MONTHLY
//*             : "PREMIUM DIVISION REPORT OF PARTICIPANT CLOSED CASES
//*             :          INTERNALLY/EXTERNALLY GENERATED"
//*             : FOR
//*             : UNITS ACADM, AIIVC, ALOAN, APC, PRADJ, PRREC, STATS,
//*             :       AND TRSFR
//*             :
//* NOTES       : DDCARD MEMBER DBAPF045 INVOKES ADABAS PREFETCH
//*---------------------------------------------------------------------
//REPT030  EXEC PGM=NATB030,REGION=&REGN1,
//         PARM='SYS=&NAT'
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPF&DBID),DISP=SHR
//SYSPRINT DD SYSOUT=&JCLO
//SYSOUT   DD SYSOUT=&JCLO
//DDPRINT  DD SYSOUT=&JCLO
//CMPRINT  DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//CMPRT01  DD SYSOUT=(&REPT,CW2000M3)
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(P2000CWD),DISP=SHR
//**********************************************************************
//* STEP        : REPT050
//* PARM        : P2000CWF
//* PROGRAM     : CWFB5560
//* DESCRIPTION : THIS PRODUCES THE MONTHLY
//*             : REQUEST ARRIVED VIA FAX, EMAIL AND INTERNET
//*             :
//* NOTES       : DDCARD MEMBER DBAPF045 INVOKES ADABAS PREFETCH
//*---------------------------------------------------------------------
//REPT050  EXEC PGM=NATB030,REGION=&REGN1,
//         PARM='SYS=&NAT'
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPF&DBID),DISP=SHR
//SYSPRINT DD SYSOUT=&JCLO
//SYSOUT   DD SYSOUT=&JCLO
//DDPRINT  DD SYSOUT=&JCLO
//CMPRINT  DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//CMPRT01  DD SYSOUT=(&REPT,CW2000M5)
//CMPRT02  DD SYSOUT=(&REPT,CW2000M6)
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(P2000CWF),DISP=SHR
//**********************************************************************
//* STEP        : REPT060
//* PARM        : P2000CWG
//* PROGRAM     : CWFB5580
//* DESCRIPTION : THIS PRODUCES THE MONTHLY
//*             : WPID STATISTICS
//*---------------------------------------------------------------------
//REPT060  EXEC PGM=NATB030,REGION=&REGN1,
//         PARM='SYS=&NAT'
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSPRINT DD SYSOUT=&JCLO
//SYSOUT   DD SYSOUT=&JCLO
//DDPRINT  DD SYSOUT=&JCLO
//CMPRINT  DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//CMPRT01  DD SYSOUT=(&REPT,CW2000M7)
//CMPRT02  DD SYSOUT=(&REPT,CW2000M8)
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(P2000CWG),DISP=SHR
//**********************************************************************
//* STEP        : REPT070
//* PARM        : P2000CWH
//* PROGRAM     : ICWB3410
//* DESCRIPTION : THIS PRODUCES THE MONTHLY REPORT
//*             : OF REQUEST ARRIVED VIA FAX
//*             : INSTITUTIONAL CORPORATE WORKFLOW
//*---------------------------------------------------------------------
//REPT070  EXEC PGM=NATB030,REGION=&REGN1,COND=EVEN,
//         PARM='SYS=&NAT'
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPF&DBID),DISP=SHR
//SYSPRINT DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMPRT01  DD  SYSOUT=(&REPT,CW2000M9)
//CMPRT02  DD  SYSOUT=(&REPT,CW2000MA)
//SORTWK01 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC2))
//SORTWK02 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC3))
//SORTWK03 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC4))
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P2000CWH),DISP=SHR
