//PNI1270D PROC DBID=003,
//         HLQ1='PPM.ANN',
//         HLQ6='PNA.ANN',
//         HLQ8='PPDD.PNI1270D',
//         JCLO='*',
//         DMPS='*',
//         LIB1='PROD',
//         REGN1='9M',
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         GDG1='+1',                                                   
//         SPC1='10,5',
//         SPC2='10,5',
//         INCLUDE=PSTPINCL,             INCLUD - ORACLE INTERFACE PROD
//         CNTL1=UP076SEC,
//         ORALOG=ORALOGON.NY.PARMLIB
//*====================================================================*
//*                         NARRATIVE                                  *
//*====================================================================*
//*                 DIALOG WELCOME PACKAGE FEED                        *
//*====================================================================*
//* THIS JOB READS THE DIALOGUE EXTRACT FILE FROM P1110NID AND THE     *
//* ROUTING AND SHORT NAME RULES TO PRODUCE A FILE WHICH IS SENT TO    *
//* ETL FOR WELCOME PACKAGES.                                          *
//*--------------------------------------------------------------------*
//* MODIFICATION HISTORY                                               *
//*--------------------------------------------------------------------*
//* LAEVEY     7/14/2013 ADDED STEPS FRMT074 AND SORT075 TO INCLUDE    *
//*                      BUNDLE INFORMATION AS PART OF IRA SUBSTITUTION*
//*====================================================================*
//* ALOC005: PRE-ALLOCATE OUTPUT FILES                                 *
//*====================================================================*
//ALOC005  EXEC PGM=IEFBR14,REGION=8M
//ALOCLGL  DD DSN=&HLQ6..ACIS.WELCOME.DIALOG.DATA,
//             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             DCB=(RECFM=VB,BLKSIZE=23476),
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE)
//*====================================================================*
//* CHECK FOR EMPTY FILE                                               *
//*====================================================================*
//VERY010  EXEC PGM=IDCAMS
//SYSPRINT DD DUMMY
//DDIN     DD DSN=&HLQ1..ACIS.WELCOME.EXTRACT,DISP=SHR
//SYSIN    DD  DSN=&LIB1..PARMLIB(NI1270D1),DISP=SHR
//*====================================================================*
//* SUM THE WELCOME PLAN INFO FILE DOWN TO PLAN LEVEL                  *
//*====================================================================*
//SORT020  EXEC PGM=SORT,REGION=&REGN1,COND=(0,NE)
//SYSOUT   DD SYSOUT=&JCLO
//SORTIN   DD DSN=&HLQ1..ACIS.PLANINFO.WELCOME.SORT,DISP=SHR
//SORTOUT  DD DSN=&HLQ1..ACIS.PLANINFO.WELCOME.SUM,
//            DISP=(,CATLG,DELETE),
//            UNIT=SYSDA,
//            DATACLAS=DCPSEXTC,
//            RECFM=FB,
//            SPACE=(CYL,(&SPC2),RLSE)
//SYSIN    DD DSN=&LIB1..PARMLIB(NI1270D2),DISP=SHR
//*===================================================================*
//* PROGRAM APPB1150                                                  *
//*                                                                   *
//* WRITE THE WELCOME PLAN INFO DATA TO APP-TABLE-ENTRY TABLE: 100/PL *
//*===================================================================*
//UPDT030  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//   PARM='SYS=&NAT'
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE)
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMWKF01  DD DSN=&HLQ1..ACIS.PLANINFO.WELCOME.SUM,DISP=SHR
//SYSPRINT DD SYSOUT=&JCLO
//DDPRINT  DD SYSOUT=&JCLO
//SYSOUT   DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//CMPRINT  DD SYSOUT=&JCLO
//CMSYNIN  DD DSN=&LIB1..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(NI1270D3),DISP=SHR
//*====================================================================*
//* SORT SUM TO ELIMINATE DUPLICATE CONTRACTS                          *
//*====================================================================*
//SORT040  EXEC PGM=SORT,COND=(0,NE)
//SORTIN   DD DSN=&HLQ1..ACIS.WELCOME.EXTRACT,
//             DISP=SHR
//SORTOUT  DD DSN=&HLQ8..WELCEXTR.DIAG.SORT,
//             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//*//             DCB=RECFM=FB,
//             DCB=(RECFM=FB),
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE)
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE)
//SYSABOUT DD  SYSOUT=*
//SYSOUT   DD  SYSOUT=*
//SYSUDUMP DD  SYSOUT=*
//SYSIN    DD  DSN=&LIB1..PARMLIB(NI1270D4),DISP=SHR
//*====================================================================*
//* PROGRAM APPB1155                                                   *
//*                                                                    *
//* FRMT045: EXPAND WELCOME EXTRACT FILE FORMAT SAME AS LEGAL          *
//*====================================================================*
//FRMT045  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//   PARM='SYS=&NAT'
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//*----->  BEGIN ORACLE ITEMS    <-------
//*MSYS //    INCLUDE MEMBER=&INCLUDE
//ACISFLE  DD DSN=&LIB1..&ORALOG(&CNTL1),
//         DISP=SHR
//*----->  END ORACLE ITEMS    <-------
//CMWKF01  DD DSN=&HLQ8..WELCEXTR.DIAG.SORT,DISP=SHR
//CMWKF02  DD DSN=&HLQ8..WELCEXTR.DIAG.SORTNEW,
//            DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//*//            DCB=RECFM=FB,
//            DCB=(RECFM=FB),
//            UNIT=SYSDA,SPACE=(CYL,(&SPC2),RLSE)
//SYSPRINT DD SYSOUT=*
//DDPRINT  DD SYSOUT=*
//SYSOUT   DD SYSOUT=*
//SYSUDUMP DD SYSOUT=&JCLO
//CMPRINT  DD SYSOUT=*
//CMSYNIN  DD DSN=&LIB1..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(NI1270DA),DISP=SHR
//*====================================================================*
//* PROGRAM PPP1006                                                    *
//*                                                                    *
//* ADD THE JOBNAME AND SHORT NAME TO THE EXTRACT FILE                 *
//*====================================================================*
//FRMT050  EXEC PGM=PPP1006,REGION=&REGN1,COND=(0,NE)
//SYSOUT   DD SYSOUT=&JCLO
//SYSABOUT DD SYSOUT=&JCLO,
//            RECFM=FBM
//SYSUDUMP DD SYSOUT=&JCLO,
//            RECFM=FBM
//SYSPRINT DD SYSOUT=&JCLO,
//            RECFM=FBM
//NROLDATA DD DSN=&HLQ8..WELCEXTR.DIAG.SORTNEW,DISP=SHR
//CPDATA   DD DSN=&HLQ6..ACIS.DIAG.RULES,DISP=SHR
//TEMPNROL DD DSN=&HLQ8..WELCEXTR.DIAG.PPP1006,
//             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//*//             DCB=RECFM=FB,
//             DCB=(RECFM=FB),
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE)
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE)
//JOBRPT   DD SYSOUT=(,NI1270D1),OUTPUT=(*.OUT1,*.OUT2),
//*//            DCB=RECFM=FBM
//            DCB=(RECFM=FBM)
//*====================================================================*
//* PROGRAM APPB1151                                                   *
//*                                                                    *
//* ADD THE RECORD KEY TO THE EXTRACT FILE                             *
//*====================================================================*
//FRMT060  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//   PARM='SYS=&NAT'
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMWKF01  DD DSN=&HLQ8..WELCEXTR.DIAG.PPP1006,DISP=SHR
//CMWKF02  DD DSN=&HLQ8..WELCEXTR.DIAG.EXTRWHDR,
//             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//*//             DCB=RECFM=FB,
//             DCB=(RECFM=FB),
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE)
//SYSPRINT DD SYSOUT=*
//DDPRINT  DD SYSOUT=*
//SYSOUT   DD SYSOUT=*
//SYSUDUMP DD SYSOUT=&JCLO
//CMPRINT  DD SYSOUT=*
//CMSYNIN  DD DSN=&LIB1..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(NI1270D5),DISP=SHR
//*====================================================================*
//* SORT EXTRACT FILE BY THE RECORD KEY                                *
//*====================================================================*
//SORT070  EXEC PGM=SORT,COND=(0,NE)
//SORTIN   DD DSN=&HLQ8..WELCEXTR.DIAG.EXTRWHDR,
//             DISP=SHR
//SORTOUT  DD DSN=&HLQ8..WELCEXTR.DIAG.SORTWHDR,
//             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//*//             DCB=RECFM=FB,
//             DCB=(RECFM=FB),
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE)
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE)
//SYSABOUT DD  SYSOUT=*
//SYSOUT   DD  SYSOUT=*
//SYSUDUMP DD  SYSOUT=*
//SYSIN    DD  DSN=&LIB1..PARMLIB(NI1270D6),DISP=SHR
//*====================================================================*
//* PROGRAM APPB1158                                                  *
//*                                                                    *
//* ADD BUNDLE INFORMATION TO THE EXTRACT FILE                         *
//*====================================================================*
//FRMT074  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//   PARM='SYS=&NAT'
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMWKF01  DD DSN=&HLQ8..WELCEXTR.DIAG.SORTWHDR,DISP=SHR
//CMWKF02  DD DSN=&HLQ8..WELCEXTR.DIAG.FRMTBNDL,
//             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//*//             DCB=RECFM=FB,
//             DCB=(RECFM=FB),
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE)
//SYSPRINT DD SYSOUT=*
//DDPRINT  DD SYSOUT=*
//SYSOUT   DD SYSOUT=*
//SYSUDUMP DD SYSOUT=&JCLO
//CMPRINT  DD SYSOUT=*
//CMSYNIN  DD DSN=&LIB1..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(NI1270D8),DISP=SHR
//*====================================================================*
//* SORT EXTRACT FILE BY THE RECORD KEY                                *
//*====================================================================*
//SORT075  EXEC PGM=SORT,COND=(0,NE)
//SORTIN   DD DSN=&HLQ8..WELCEXTR.DIAG.FRMTBNDL,
//             DISP=SHR
//SORTOUT  DD DSN=&HLQ8..WELCEXTR.DIAG.SORTWBND,
//             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//*//             DCB=RECFM=FB,
//             DCB=(RECFM=FB),
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE)
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE)
//SYSABOUT DD  SYSOUT=*
//SYSOUT   DD  SYSOUT=*
//SYSUDUMP DD  SYSOUT=*
//SYSIN    DD  DSN=&LIB1..PARMLIB(NI1270D9),DISP=SHR
//*====================================================================*
//* PROGRAM APPB1152                                                   *
//*                                                                    *
//* CREATE DIALOG FEED FILE                                            *
//*====================================================================*
//FRMT080  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//   PARM='SYS=&NAT'
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMWKF01  DD DSN=&HLQ8..WELCEXTR.DIAG.SORTWBND,DISP=SHR
//CMWKF02  DD DSN=&HLQ6..ACIS.WELCOME.DIALOG.DATA,
//             DISP=SHR
//SYSPRINT DD SYSOUT=*
//DDPRINT  DD SYSOUT=*
//SYSOUT   DD SYSOUT=*
//SYSUDUMP DD SYSOUT=&JCLO
//CMPRINT  DD SYSOUT=*
//CMSYNIN  DD DSN=&LIB1..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(NI1270D7),DISP=SHR
//*===================================================================*
//* COPY THE DIALOG FEED FILE TO A BACKUP GDG                         *
//*===================================================================*
//COPY090  EXEC PGM=IEBGENER
//SYSPRINT DD  SYSOUT=&JCLO
//SYSUT1   DD  DSN=&HLQ6..ACIS.WELCOME.DIALOG.DATA,DISP=SHR
//SYSUT2   DD  DSN=&HLQ6..ACIS.WELCOME.DIALOG.BU(&GDG1),
//             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//*//             DCB=RECFM=VB,
//             DCB=(RECFM=VB),
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE)
//SYSIN    DD  DUMMY
