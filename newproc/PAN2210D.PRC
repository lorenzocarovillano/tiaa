//PAN2210D PROC DBID=003,
//         DMPS='U',
//         JCLO='*',
//         LIB1='PROD',
//         LIB2='PROD',
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REPT='8',
//         REPTID1='AD2210D1',
//         REPTID2='AD2210D2',
//         REPTID3='AD2210D3',
//         REPTID4='AD2210D4',
//         REPTID5='AD2210D5',
//         REPTID6='AD2210D6',
//         PARMMEM1='AN2210D1',
//         PARMMEM2='AN2210D2',
//         REGN1='4M'
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
//* PROCEDURE:  PAN2210D                 DATE:    04/29/04            *
//* PROGRAMS EXECUTED:                                                *
//*   STEP NAME    PROGRAM NAME                                       *
//* -------------  ------------                                       *
//*   UPDT010      ADSP590                                            *
//*   UPDT020      ADSP595                                            *
//*                                                                   *
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
//* FILES ACCESSED:
//* DB 003: 026, 028, 163, 164
//* DB 045: 108, 109, 110, 111, 112, 113, 114, 160,
//*         181, 182, 183, 184, 185, 186, 187
//* FILES UPDATED IN STEP UPDT010:
//*   DB 045: 113, 185
//* FILES UPDATED IN STEP UPDT020:
//*   DB 003: 028, 160
//*********************************************************************
//* UPDT010 - ADSP590 WILL BE EXECUTED IN THIS STEP WHICH
//*                   SETS INTERFACE TO POST
//*********************************************************************
//UPDT010 EXEC PGM=NATB030,REGION=&REGN1,
//      PARM='SYS=&NAT,OPRB=(.DBID=253,FNR=192,I,A)'                    
//SYSABOUT DD SYSOUT=&DMPS
//SYSDBOUT DD SYSOUT=&DMPS                                              
//SYSUDUMP DD SYSOUT=&DMPS
//SYSOUT   DD SYSOUT=&JCLO
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMPRINT  DD SYSOUT=(&REPT,&REPTID1)                                   
//CMPRT01  DD SYSOUT=(&REPT,&REPTID2)                                   
//CMPRT02  DD SYSOUT=(&REPT,&REPTID3)                                   
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB2..PARMLIB(&PARMMEM1),DISP=SHR
//         DD DSN=&LIB2..PARMLIB(FINPARM),DISP=SHR
//*********************************************************************
//* UPDT020 - ADSP595 WILL BE EXECUTED IN THIS STEP WHICH
//*                   UPDATES ADS-CNTL (ADS-FNL-PRM-CNTRL-DTE)
//*********************************************************************
//UPDT020 EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//      PARM='SYS=&NAT'                                                 
//SYSABOUT DD SYSOUT=&DMPS
//SYSDBOUT DD SYSOUT=&DMPS                                              
//SYSUDUMP DD SYSOUT=&DMPS
//SYSOUT   DD SYSOUT=&JCLO
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMPRINT  DD SYSOUT=(&REPT,&REPTID4)                                   
//CMPRT01  DD SYSOUT=(&REPT,&REPTID5)                                   
//CMPRT02  DD SYSOUT=(&REPT,&REPTID6)                                   
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB2..PARMLIB(&PARMMEM2),DISP=SHR
//         DD DSN=&LIB2..PARMLIB(FINPARM),DISP=SHR
