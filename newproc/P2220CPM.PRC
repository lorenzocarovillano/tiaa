//P2220CPM PROC DBID='003',
//         DMPS='U',
//         GDG0='0',
//         GDG1='+1',
//         HLQ0='PIA.ANN',
//         HLQ8='PPDD',
//         JCLO='*',
//         JOBNM1='P2210CPM',
//         JOBNM2='P2220CPM',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='9M',
//         REPT='8',
//         SPC1='15,5',
//         SPC2='5,1',
//         SPC3='100,400'
//*
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
//*
//* PROCEDURE:  P2220CPM                 DATE:  11/13/97
//*
//********************************************************************
//*
//* CPS IA GENERATE FINANCIAL FILES
//*
//* SORT010 - SORT GLOBAL PAY FILE INTO PAYMENT SEQUENCE
//* REPT020 - CALCULATE STATEMENTS FIELDS
//*           NATURAL PROGRAM FCPP802
//* SORT030 - SORT GLOBAL PAY CHECK FILE
//* REPT040 - PRINT GLOBAL PAY STATEMENTS
//*           NATURAL PROGRAM:  FCPP803
//*
//**********************************************************************
//*                                                                     
//* MODIFICATION HISTORY
//* 04/19/02 - OIA JCL STANDARDS - PHIL STEINHAUSER
//* 01/15/20 - CPS SUNSET        - CTS
//*                                                                     
//*******************************************************************
//* SORT010 - SORT GLOBAL PAY FILE INTO PAYMENT SEQUENCE
//*******************************************************************
//SORT010 EXEC PGM=SORT,COND=(0,NE),REGION=&REGN1
//SYSOUT   DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,(&SPC1)),BUFNO=10
//SORTWK02 DD UNIT=SYSDA,SPACE=(CYL,(&SPC1)),BUFNO=10
//SORTWK03 DD UNIT=SYSDA,SPACE=(CYL,(&SPC1)),BUFNO=10
//SORTWK04 DD UNIT=SYSDA,SPACE=(CYL,(&SPC1)),BUFNO=10
//SYSPRINT DD SYSOUT=&JCLO
//SORTIN   DD DSN=&HLQ0..&JOBNM1..GLOBAL.S470,                INPUT
//            DISP=SHR,BUFNO=24
//SORTOUT  DD DSN=&HLQ8..&JOBNM2..GLOBAL1,                    OUTPUT
//            DISP=(NEW,CATLG,DELETE),
//            UNIT=SYSDA,SPACE=(CYL,(&SPC2),RLSE),
//            DCB=(LRECL=4800,RECFM=VB,BUFNO=25)
//SYSIN    DD DSN=PROD.PARMLIB(P2220CP1),DISP=SHR
//* SORT FIELDS=(94,2,CH,A,110,8,CH,A,79,14,CH,A,9,1,CH,D,118,9,CH,A)
//* CNTRCT-ORGN-CDE           90+4,2
//* CNTRCT-INVRSE-DTE        106+4,8
//* CNTRCT-CMBN-NBR           75+4,14
//* CNTRCT-COMPANY-CDE         5+4,1
//* PYMNT-PRCSS-SEQ-NBR      114+4,9
//*******************************************************************
//* REPT020 - CALCULATE STATEMENTS FIELDS
//*           USES PROGRAM:  FCPP802
//*******************************************************************
//REPT020 EXEC PGM=NATB030,COND=(0,NE),REGION=&REGN1,
//            PARM='SYS=&NAT,UDB=003,OBJIN=N,IM=D'
//DDPRINT  DD SYSOUT=&JCLO
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSOUT   DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//SYSPRINT DD SYSOUT=&JCLO
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,(&SPC1)),BUFNO=10
//SORTWK02 DD UNIT=SYSDA,SPACE=(CYL,(&SPC1)),BUFNO=10
//SORTWK03 DD UNIT=SYSDA,SPACE=(CYL,(&SPC1)),BUFNO=10
//SORTWK04 DD UNIT=SYSDA,SPACE=(CYL,(&SPC1)),BUFNO=10
//CMPRINT  DD SYSOUT=&JCLO
//CMWKF01  DD DSN=&HLQ8..&JOBNM2..GLOBAL1,                    INPUT
//            DISP=SHR,BUFNO=24
//CMWKF02  DD DSN=&HLQ8..&JOBNM2..GLOBAL2,                    OUTPUT
//            DISP=(NEW,CATLG,DELETE),
//            UNIT=SYSDA,SPACE=(CYL,(&SPC2),RLSE),
//            DCB=(LRECL=4875,RECFM=VB,BUFNO=25)
//CMWKF03  DD DUMMY,SPACE=(CYL,(1,1),RLSE)                    OUTPUT
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(P2220CP2),DISP=SHR
//* FCPP802
//*******************************************************************
//* SORT030 - SORT GLOBAL PAY CHECK FILE
//*******************************************************************
//SORT030 EXEC PGM=SORT,COND=(0,NE),REGION=&REGN1
//SYSOUT   DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,(&SPC1)),BUFNO=10
//SORTWK02 DD UNIT=SYSDA,SPACE=(CYL,(&SPC1)),BUFNO=10
//SORTWK03 DD UNIT=SYSDA,SPACE=(CYL,(&SPC1)),BUFNO=10
//SORTWK04 DD UNIT=SYSDA,SPACE=(CYL,(&SPC1)),BUFNO=10
//SYSPRINT DD SYSOUT=&JCLO
//SORTIN   DD DSN=&HLQ8..&JOBNM2..GLOBAL2,                    INPUT
//            DISP=SHR,BUFNO=24
//SORTOUT  DD DSN=&HLQ8..&JOBNM2..GLOBAL3,                    OUTPUT
//            DISP=(NEW,CATLG,DELETE),
//            UNIT=SYSDA,SPACE=(CYL,(&SPC2),RLSE),
//            DCB=(LRECL=4875,RECFM=VB,BUFNO=25)
//SYSIN    DD DSN=&LIB1..PARMLIB(P2220CP3),DISP=SHR
//* SORT FIELDS=(5,4,CH,A,10,2,PD,A,94,2,CH,A,110,8,CH,A,79,14,CH,A,
//*              9,1,CH,D,118,9,CH,A)
//* CNTRCT-HOLD-CDE            1+4,4
//* PYMNT-TOTAL-PAGES          6+4,2
//* CNTRCT-ORGN-CDE           90+4,2
//* CNTRCT-INVRSE-DTE        106+4,8
//* CNTRCT-CMBN-NBR           75+4,14
//* CNTRCT-COMPANY-CDE         5+4,1     DESC
//* PYMNT-PRCSS-SEQ-NBR      114+4,9
//*******************************************************************
//* REPT040 - PRINT GLOBAL PAY STATEMENTS
//*           NATURAL PROGRAM:  FCPP803
//*******************************************************************
//REPT040 EXEC PGM=NATB030,COND=(0,NE),REGION=&REGN1,
//            PARM='SYS=&NAT,UDB=003,OBJIN=N,IM=D'
//DDPRINT  DD SYSOUT=&JCLO
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSOUT   DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//SYSPRINT DD SYSOUT=&JCLO
//CMPRINT  DD SYSOUT=&JCLO
//CMPRT01  DD DUMMY,DCB=BLKSIZE=27984
//CMPRT15  DD SYSOUT=&REPT
//CMWKF01  DD DSN=&HLQ8..&JOBNM2..GLOBAL3,                     INPUT
//            DISP=SHR,BUFNO=24
//CMWKF08  DD DUMMY,DCB=BLKSIZE=27885
//*MWKF08  DD SYSOUT=F,DCB=(RECFM=FBSA,LRECL=143,BUFNO=35)
//CMWKF09  DD DSN=&HLQ0..&JOBNM2..GLOBAL4.S473,DISP=SHR        OUTPUT
//CMWKF10  DD  DSN=&HLQ0..IAADMIN.GTN.CANTX(&GDG0),DISP=SHR
//CMWKF11  DD DSN=&HLQ8..&JOBNM2..GLOBAL,
//            DISP=(NEW,CATLG,DELETE),
//            UNIT=SYSDA,SPACE=(CYL,(&SPC2),RLSE),
//            DCB=(LRECL=4746,RECFM=FB,BUFNO=25)
//CMWKF15  DD DSN=&HLQ0..&JOBNM1..CONTROLS.S472,DISP=SHR       INPUT
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(P2220CP4),DISP=SHR
//* FCPP803 GLOBAL
//********************************************************************
//* SORT050 - CONVERT VB FILE TO FB
//********************************************************************
//SORT050 EXEC PGM=SORT,COND=(0,NE)
//*
//SYSPRINT DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//SORTIN   DD  DISP=SHR,DSN=&HLQ0..&JOBNM1..EFT.S466
//SORTOUT  DD  DSN=&HLQ8..&JOBNM1..EFT.S466.FBOUT,
//             DISP=(NEW,CATLG,DELETE),
//             UNIT=SYSDA,SPACE=(CYL,(&SPC3),RLSE),
//             DCB=(LRECL=4746,RECFM=FB)
//SYSIN    DD DSN=&LIB1..PARMLIB(P2220CP5),DISP=SHR
//*
//*********************************************************************
//*       ADDING SORT STEP TO MERGE EFT,CHECKS AND GLOBAL FILES
//*       BACKUP DATA FOR DE80 MONTHLY CONVERTER JOB
//*********************************************************************
//SORT060 EXEC PGM=SORT,COND=(0,NE)
//*
//SYSPRINT DD SYSOUT=&JCLO
//SYSOUT   DD SYSOUT=&JCLO
//SORTIN   DD DISP=(OLD,DELETE,KEEP),DSN=&HLQ8..&JOBNM1..EFT.S466.FBOUT
//         DD DISP=SHR,DSN=&HLQ8..&JOBNM1..CHECKS.NEW.FIELDS
//         DD DISP=SHR,DSN=&HLQ8..&JOBNM2..GLOBAL
//SORTOUT  DD DSN=&HLQ0..&JOBNM1..DE80.FEED(&GDG1),
//            DISP=(NEW,CATLG,DELETE),UNIT=SYSDA,
//            SPACE=(CYL,(&SPC3),RLSE),
//            DCB=(LRECL=4746,RECFM=FB)
//SYSIN    DD DSN=&LIB1..PARMLIB(SORTCOPY),DISP=SHR
//*
