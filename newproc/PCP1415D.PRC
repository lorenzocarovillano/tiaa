//PCP1415D PROC JNAME='PCP1415D',                                       
//         HLQ1='POMPY',                                                
//         HLQ2='PCPS',                                                 
//         HLQ5='PIA',                                                  
//         VFSET='PMTP',
//         JCLO='*',                                                    
//         REGN1='0M',                                                  
//         REGN2='9M',                                                  
//         GDG0='0',
//         GDG1='+1',                                                   
//         LIB1='PROD',                                                 
//         LIB2='PROD',                                                 
//         NATMEM='PANNSEC',                                            
//         NAT='NAT003P',                                               
//         DMPS='U',                                                    
//         DBID='003',                                                  
//         SPC1='100,50'                                                
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
//* PROCEDURE:  PCP1415D                                                
//*          :  CREATE A LEDGER EXTRACT FILE FROM STOP/CANCEL FILE AND  
//*             LEDGER VSAM.                                            
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
//********************************************************************* 
//* EXTRT010- CREATE A FLAT  FILE BY PROCESSING STOP & CANCEL FILE      
//*           AND LEDGER VSAM.                                          
//*******************************************************************   
//EXTRT010 EXEC PGM=TIAALEDR,REGION=&REGN1                              
//SYSOUT    DD SYSOUT=&JCLO                                             
//SYSPRINT  DD SYSOUT=&JCLO                                             
//SYSUDUMP  DD SYSOUT=&DMPS
//STOPCNFL  DD DSN=&HLQ1..&VFSET..STOPCAN.FILE(&GDG0),DISP=SHR          
//LEDGERFL  DD DSN=&HLQ2..LEDGER.REV.VSAM.BATCH,DISP=SHR                
//OUTFILE   DD DSN=&HLQ5..ANN.&JNAME..OUTFILE(&GDG1),                   
//             DISP=(NEW,CATLG,DELETE),UNIT=SYSDA,                      
//             DATACLAS=DCPSEXTC,DCB=(RECFM=FB,LRECL=4034,BLKSIZE=0),   
//             SPACE=(CYL,(&SPC1),RLSE)                                 
//REJFILE   DD DSN=&HLQ5..ANN.&JNAME..REJFILE(&GDG1),                   
//             DISP=(NEW,CATLG,DELETE),UNIT=SYSDA,                      
//             DATACLAS=DCPSEXTC,DCB=(RECFM=FB,LRECL=80,BLKSIZE=0),     
//             SPACE=(CYL,(&SPC1),RLSE)                                 
//*                                                                     
//********************************************************************* 
//* EXTRT020- CREATE A FILE EXTRACT IN THE LAYOUT OF #LEDGER-EXT-2.     
//********************************************************************* 
//EXTRT020 EXEC PGM=NATB030,REGION=&REGN2,COND=(0,NE),                  
//      PARM='SYS=&NAT'
//SYSABOUT DD  SYSOUT=&DMPS
//SYSDBOUT DD  SYSOUT=&DMPS
//SYSOUT   DD  SYSOUT=&JCLO
//SYSPRINT DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//DDCARD   DD  DSN=&LIB2..PARMLIB(DBAPP&DBID),DISP=SHR
//CMWKF01   DD DSN=&HLQ5..ANN.&JNAME..OUTFILE(&GDG1),DISP=SHR           
//CMWKF02   DD DSN=&HLQ5..ANN.&JNAME..LEDGER.EXTRACT(&GDG1),            
//             DISP=(NEW,CATLG,DELETE),UNIT=SYSDA,                      
//             DATACLAS=DCPSEXTC,DCB=(RECFM=FB,LRECL=100,BLKSIZE=0),    
//             SPACE=(CYL,(&SPC1),RLSE)                                 
//CMWKF03   DD DSN=&HLQ5..ANN.&JNAME..LDPAYRV.EXT(&GDG1),               
//             DISP=(NEW,CATLG,DELETE),UNIT=SYSDA,                      
//             DATACLAS=DCPSEXTC,DCB=(RECFM=FB,LRECL=154,BLKSIZE=0),    
//             SPACE=(CYL,(&SPC1),RLSE)                                 
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P1415CP1),DISP=SHR
//**********************************************************************
//* SORT030-STEP TO SPLIT LEDGER FILE FOR NZ AND EXCEPT NZ ORIGIN CODES
//**********************************************************************
//SORT030  EXEC PGM=SORT,REGION=&REGN1,COND=(0,NE)
//SYSOUT   DD SYSOUT=&JCLO
//SYSPRINT DD SYSOUT=&JCLO
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,(&SPC1))
//SORTWK02 DD UNIT=SYSDA,SPACE=(CYL,(&SPC1))
//SORTWK03 DD UNIT=SYSDA,SPACE=(CYL,(&SPC1))
//SORTIN   DD DSN=&HLQ5..ANN.&JNAME..LEDGER.EXTRACT(&GDG1),DISP=SHR
//SORTOF1  DD DSN=&HLQ5..ANN.&JNAME..LGRNZ.REV.DAILY(&GDG1),
//             DISP=(NEW,CATLG,DELETE),UNIT=SYSDA,
//             DATACLAS=DCPSEXTC,DCB=(RECFM=FB,LRECL=100,BLKSIZE=0),
//             SPACE=(CYL,(&SPC1),RLSE)
//SORTOF2  DD DSN=&HLQ5..ANN.&JNAME..LGRAL.REV.DAILY(&GDG1),
//             DISP=(NEW,CATLG,DELETE),UNIT=SYSDA,
//             DATACLAS=DCPSEXTC,DCB=(RECFM=FB,LRECL=100,BLKSIZE=0),
//             SPACE=(CYL,(&SPC1),RLSE)
//SYSIN    DD DSN=&LIB1..PARMLIB(P1415CP2),DISP=SHR
//**********************************************************************
//* SORT040-STEP TO SPLIT PYMNT RVRSL FILE FOR NZ AND NON-NZ ORGN CODES
//**********************************************************************
//SORT040  EXEC PGM=SORT,REGION=&REGN1,COND=(0,NE)
//SYSOUT   DD SYSOUT=&JCLO
//SYSPRINT DD SYSOUT=&JCLO
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,(&SPC1))
//SORTWK02 DD UNIT=SYSDA,SPACE=(CYL,(&SPC1))
//SORTWK03 DD UNIT=SYSDA,SPACE=(CYL,(&SPC1))
//SORTIN   DD DSN=&HLQ5..ANN.&JNAME..LDPAYRV.EXT(&GDG1),DISP=SHR
//SORTOF1  DD DSN=&HLQ5..ANN.&JNAME..LDPYRNZ.DAILY(&GDG1),
//             DISP=(NEW,CATLG,DELETE),UNIT=SYSDA,
//             DATACLAS=DCPSEXTC,DCB=(RECFM=FB,LRECL=154,BLKSIZE=0),
//             SPACE=(CYL,(&SPC1),RLSE)
//SORTOF2  DD DSN=&HLQ5..ANN.&JNAME..LDPYRAL.DAILY(&GDG1),
//             DISP=(NEW,CATLG,DELETE),UNIT=SYSDA,
//             DATACLAS=DCPSEXTC,DCB=(RECFM=FB,LRECL=154,BLKSIZE=0),
//             SPACE=(CYL,(&SPC1),RLSE)
//SYSIN    DD DSN=&LIB1..PARMLIB(P1415CP2),DISP=SHR
