//P5020CWD PROC DBID='045',
//         DMPS='U',
//         GDG1='+1',
//         HLQ0='PCWF.COR',
//         JCLO='*',
//         LIB1='PROD',
//         LIB2='PROD',
//         NAT='NAT045P',
//         NATMEM='PANNSEC',
//         REGN1='5120K',
//         REPT='8',
//         SPC1='10,2'
//**********************************************************************
//* P5020CWD - CWF MUTUAL FUND PACKAGE PRINTING
//*
//*        SCHEDULE TO BE RUN DAILY (AFTER P5010CWD-03)
//*
//*        1) THIS JOB CONTAINS 2 STEPS: (REPT010-CWFB6800)
//*                                      (UPDT020-CWFB6810)
//*
//*           1ST STEP IS A REPORT - CONTAINS LAN AND POR REPORT TYPES
//*                                  VIA VPS. ALSO WILL GENERATE IMAGING
//*                                  ID ADDRESS TO CMWKF02 AND WRITE
//*                                  DATA TO CMWKF01.
//*
//*           2ND STEP IS A UPDATE - CAN BE EXECUTED ONLY AFTER THE
//*                                  1ST STEP WAS SUCCESSFULLY DONE.
//*
//*        2) THIS JOB NEEDS TWO WORK FILES(CMWKF01 & CMWKF02).
//*
//*        3) OUTPUT WILL BE ARCHIVED TO ARM. REPRINTS WILL ONLY BE
//*           GENERATED ON ONE-SIDED LANDSCAPE AS PER G.HONG (5/97).
//*
//**********************************************************************
//REPT010  EXEC PGM=NATB030,REGION=&REGN1,
//         PARM='SYS=&NAT'
//SYSPRINT DD  SYSOUT=&JCLO
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSOUT   DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//CMPRT01  DD  SYSOUT=(&REPT,CW5020D1)
//CMPRT02  DD  SYSOUT=(&REPT,CW5020D2)
//CMPRT03  DD  SYSOUT=(&REPT,CW5020D3)
//CMPRT04  DD  SYSOUT=(&REPT,CW5020D4)
//CMPRT05  DD  SYSOUT=(&REPT,CW5020D5)
//CMWKF01  DD  DSN=&HLQ0..CWFMF(&GDG1),DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,
//             SPACE=(TRK,(&SPC1),RLSE),
//             RECFM=FB,LRECL=85
//CMWKF02  DD  DSN=&HLQ0..BFDS.IMAGE.P5020NDM,DISP=(OLD)
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P5020CW1),DISP=SHR
//**********************************************************************
//UPDT020  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//         PARM='SYS=&NAT'
//SYSPRINT DD  SYSOUT=&JCLO
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSOUT   DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//CMPRT01  DD  SYSOUT=(&REPT,CW5020D6)
//CMWKF01  DD  DSN=&HLQ0..CWFMF(&GDG1),DISP=(SHR)
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P5020CW2),DISP=SHR
