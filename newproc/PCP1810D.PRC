//PCP1810D PROC LIB1='PROD',        COMMON LIBRARY HLQ                  
//         HLQ0='PCPS.ANN',         INPUT FILE                          
//         JOBNM='PCP1805D',        INPUT FILE                          
//         JOBNM1='PCP1800D',       INPUT FILE                          
//         JCLO00=*,                JOB OUTPUT                          
//         REPT00=*,                REPORTS                             
//         REPT01=8,                REPORTS                             
//         RPTID='CP1810D',         REPORT                              
//         DBID=003,                DATABASE ID                         
//         REGN1='9M',              REGION SIZE                         
//         GDG0='+0',               INPUT FROM OMNIPAY                  
//         NATVERS=NATB030,         NATURAL VERSION                     
//         NAT=NAT003P,             NATURAL PROFILE                     
//         PARMLIB=PARMLIB,         LLQ COMMON PARM LIBRARY             
//         NATLOG=NT2LOGON.PARMLIB, 2 LVL QUAL NATURAL LOGON LIBRARY    
//         NATMEM=PANNSEC           NATURAL LOGON MEMBER                
//*                                                                     
//*--------------------------------------------------------------------*
//*                                                                    *
//* PROCEDURE:  PCP1705D                           DATE:    10/15/2002 *
//*                                                                    *
//*           CONSOLIDATED PAYMENT SYSTEM / OPEN INVESTMENTS           *
//*                                                                    *
//*               CREATE XML DOCUMENT TO PRINT PAYMENTS                *
//*                                                                    *
//*  STEP    PROGRAM   PARMS                   COMMENTS                *
//* -------  -------  --------  -------------------------------------- *
//* LOAD010  CPVP610  PCP1810A                                          
//*                                                                    *
//*--------------------------------------------------------------------*
//*--------------------------------------------------------------------*
//* CREA020  CPVP600 - CREATE XML DOCUMENTS FOR CHECK PRINTING          
//*--------------------------------------------------------------------*
//*                                                                     
//LOAD010  EXEC PGM=&NATVERS,REGION=&REGN1,COND=(0,NE),                 
//         PARM='IM=D,SYS=&NAT'                                         
//DDCARD   DD DSN=&LIB1..&PARMLIB(DBAPP&DBID),DISP=SHR                  
//DDPRINT  DD SYSOUT=&JCLO00,OUTPUT=*.OUTVDR                            
//CMPRINT  DD SYSOUT=&REPT00,OUTPUT=*.OUTVDR                            
//CMPRT01  DD SYSOUT=(&REPT01,&RPTID.1),                                
//         DCB=(RECFM=VB,LRECL=136),OUTPUT=*.OUTVDR                     
//CMPRT10  DD SYSOUT=(&REPT01,&RPTID.2),                                
//         DCB=(RECFM=VB,LRECL=136),OUTPUT=*.OUTVDR                     
//CMWKF01  DD DSN=&HLQ0..&JOBNM1..PAYMENTS(&GDG0),DISP=OLD              
//CMWKF02  DD DSN=&HLQ0..&JOBNM..CHKDRI,DISP=OLD                        
//CMSYNIN  DD DSN=&LIB1..&NATLOG(&NATMEM),DISP=SHR                      
//         DD DSN=&LIB1..&PARMLIB(PCP1810A),DISP=SHR                    
//* CPVP610                                                             
//*                                                                     
