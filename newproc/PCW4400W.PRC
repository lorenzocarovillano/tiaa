//PCW4400W PROC DBID=045,
//       NATVERS='NATB030',
//       REGN1='8M',
//       NAT='NAT003P',                                                 
//       DMPS='U',
//       JCLO='*',
//       LIB1='PROD',
//       LIB2='PROD',
//       NATMEM='PANNSEC',                                              
//       HLQ1='PCWF.COR',
//       HLQ2='PPDD',
//       SPC1='5,5',
//       GDG1='+1',
//       REPT='8'
//*-------------------------------------------------------------------*
//* PCW4400W : Extract and produce reports of CWF & EWS administrative
//*            activities every week
//*
//* CWFB8610 - Extracts information from CWF-ORG-EMPL-TBL4 (045/184)
//* SORT     - By system/activity date/racfid/unit
//* CWFB8611 - Produces reports: 1) CWF admin activities; 2) EWS
//* ----------------------------------------------------------------- *
//*
//EXTR010  EXEC PGM=&NATVERS,REGION=&REGN1,
//            PARM='SYS=&NAT'
//SYSUDUMP DD SYSOUT=&DMPS
//SYSPRINT DD SYSOUT=&JCLO
//DDPRINT  DD SYSOUT=&JCLO
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMPRINT  DD SYSOUT=&JCLO
//CMWKF01  DD DSN=&HLQ2..PCW4400W.EXTR010.SECURITY.LOG,
//            UNIT=SYSDA,DISP=(,CATLG,DELETE),
//            SPACE=(CYL,(&SPC1),RLSE),
//            DCB=(RECFM=FB,LRECL=300,BLKSIZE=27900)
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(CW4400WA),DISP=SHR
//* CWFB8610
//*------------------------------------------------------------------ *
//*  Sort
//*------------------------------------------------------------------ *
//SORT020  EXEC PGM=SORT,REGION=0K,COND=(0,NE)
//SYSOUT   DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//SYSPRINT DD SYSOUT=&JCLO
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,(&SPC1)),DCB=BUFNO=10
//SORTWK02 DD UNIT=SYSDA,SPACE=(CYL,(&SPC1)),DCB=BUFNO=10
//SORTWK03 DD UNIT=SYSDA,SPACE=(CYL,(&SPC1)),DCB=BUFNO=10
//SORTWK04 DD UNIT=SYSDA,SPACE=(CYL,(&SPC1)),DCB=BUFNO=10
//SORTIN   DD DSN=&HLQ2..PCW4400W.EXTR010.SECURITY.LOG,DISP=SHR
//SORTOUT  DD DSN=&HLQ1..PCW4400W.SECURITY(&GDG1),
//            UNIT=SYSDA,DISP=(,CATLG,DELETE),
//            SPACE=(CYL,(&SPC1),RLSE),
//            DCB=(RECFM=FB,LRECL=300,BLKSIZE=27900)
//SYSIN    DD DSN=&LIB1..PARMLIB(CW4400WB),DISP=SHR
//* SORT FIELDS=(1,3,CH,A,4,8,CH,A,12,8,CH,A,20,8,CH,A)
//*
//REPT030   EXEC PGM=&NATVERS,REGION=&REGN1,COND=(0,NE),
//             PARM='SYS=&NAT'
//SYSUDUMP  DD SYSOUT=&JCLO
//SYSPRINT  DD SYSOUT=&JCLO
//DDPRINT   DD SYSOUT=&JCLO
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMWKF01   DD DSN=&HLQ1..PCW4400W.SECURITY(&GDG1),DISP=SHR
//CMPRINT   DD SYSOUT=&JCLO
//CMPRT01   DD SYSOUT=(&REPT,CW4400W1)
//CMPRT02   DD SYSOUT=(&REPT,CW4400W2)
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(CW4400WC),DISP=SHR
//* CWFB8611
