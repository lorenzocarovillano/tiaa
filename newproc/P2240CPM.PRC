//P2240CPM PROC DBID='003',
//         DMPS='U',
//         GDG1='+1',
//         HLQ0='PIA.ANN',
//         HLQ7='PPDT.IAR.EFT',
//         HLQ8='PPDD',
//         JCLO='*',
//         JOBNM1='P2240CPM',
//         JOBNM2='P2310CPM',
//         JOBNM3='P2230CPM',
//         JOBNM4='P2210CPM',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='9M',
//         REPT='8',
//         SPC1='95,25',
//         UNIT1='CARTN'
//*
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
//*                                                                   *
//* PROCEDURE:  P2240CPM                 DATE:  02/29/96              *
//*                                                                   *
//*********************************************************************
//*
//* CPS IA   EFT                                                      *
//*
//* EXTR005 - EXTRACT MUTUAL FUND DEDUCTION
//*           NATURAL PROGRAM FCPP804A
//* EXTR010 - EXTRACT EFT PAYMENTS FROM THE UPDATED NONCHK FILE
//*           NATURAL PROGRAM FCPP816
//* EXTR020 - CREATE AN EFT TRANSMISSION FILE FROM THE EFT EXTRACT
//*           NATURAL PROGRAM:  FCPP817
//* COPY030 - BACKUP THE EFT TRANSMISSION FILE USING IEBGENER
//*********************************************************************
//*                                                                     
//* MODIFICATION HISTORY
//* 04/22/02 - OIA JCL STANDARDS - PHIL STEINHAUSER
//*                                                                     
//*********************************************************************
//*
//* EXTR005 - EXTRACT MUTUAL FUND DEDUCTION
//*           NATURAL PROGRAM FCPP804A
//*********************************************************************
//EXTR005 EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//             PARM='IM=D,SYS=&NAT'
//SYSPRINT DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//DDPRINT  DD SYSOUT=&JCLO
//CMPRINT  DD SYSOUT=&JCLO
//CMPRT01  DD SYSOUT=&REPT
//CMWKF01  DD DSN=&HLQ0..&JOBNM2..DEDUCTNS,                   INPUT
//            DISP=SHR
//CMWKF02  DD DSN=&HLQ8..&JOBNM1..MUTUAL.FUND,                OUTPUT
//            DISP=(NEW,CATLG,DELETE),
//            UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE),
//            LRECL=3000,RECFM=VB
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(P2240CP3),DISP=SHR
//* FCPP804A
//*********************************************************************
//*
//* EXTR010 - EXTRACT EFT PAYMENTS FROM THE UPDATED NONCHK FILE
//*           NATURAL PROGRAM FCPP816
//*********************************************************************
//EXTR010 EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//             PARM='IM=D,SYS=&NAT'
//SYSPRINT DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//DDPRINT  DD SYSOUT=&JCLO
//CMPRINT  DD SYSOUT=&JCLO
//CMPRT01  DD SYSOUT=&REPT
//CMPRT15  DD SYSOUT=&REPT CONTROL REPORT
//CMWKF01  DD DSN=&HLQ0..&JOBNM3..NONCHK.UPDATED.S474,          INPUT
//            DISP=SHR,DCB=BUFNO=25
//         DD DSN=&HLQ8..&JOBNM1..MUTUAL.FUND,DISP=SHR         INPUT
//CMWKF02  DD DSN=&HLQ8..&JOBNM1..EFTXTR,                      OUTPUT
//            DISP=(NEW,CATLG,DELETE),
//            UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE),
//            DCB=(LRECL=200,RECFM=FB,BUFNO=25)
//CMWKF15  DD DSN=&HLQ0..&JOBNM4..CONTROLS.S472,DISP=SHR        INPUT
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(P2240CP1),DISP=SHR
//* FCPP816
//*********************************************************************
//* EXTR020 - CREATE AN EFT TRANSMISSION FILE FROM THE EFT EXTRACT
//*           NATURAL PROGRAM:  FCPP817
//*********************************************************************
//EXTR020 EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//        PARM='IM=D,SYS=&NAT'
//SYSUDUMP DD SYSOUT=&DMPS
//SYSPRINT DD SYSOUT=&JCLO
//DDPRINT  DD SYSOUT=&JCLO
//CMPRINT  DD SYSOUT=&JCLO
//CMPRT01  DD SYSOUT=&REPT
//CMPRT15  DD SYSOUT=&REPT CONTROL REPORT
//CMPRT02  DD SYSOUT=&REPT
//CMWKF01  DD DSN=&HLQ8..&JOBNM1..EFTXTR,DISP=SHR,DCB=BUFNO=30 INPUT
//CMWKF04  DD DSN=&HLQ0..IAR.EFT.XTRCT.TRNSMSN.S475,           OUTPUT
//            DISP=SHR,BUFNO=27
//CMWKF15  DD DSN=&HLQ0..&JOBNM4..CONTROLS.S472,DISP=SHR
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(P2240CP2),DISP=SHR
//* FCPP817
//*********************************************************************
//* COPY030 - BACKUP THE EFT TRANSMISSION FILE USING IEBGENER
//*********************************************************************
//COPY030  EXEC PGM=IEBGENER,COND=(4,LT),REGION=&REGN1
//SYSABOUT DD SYSOUT=&DMPS
//SYSUDUMP DD SYSOUT=&DMPS
//SYSPRINT DD SYSOUT=&DMPS
//SYSIN    DD DUMMY
//SYSUT1   DD DSN=&HLQ0..IAR.EFT.XTRCT.TRNSMSN.S475,
//            DISP=SHR,BUFNO=27
//SYSUT2   DD DSN=&HLQ7..XTRCT.TRNSMSN.S475.VR(&GDG1),
//            DISP=(NEW,CATLG,DELETE),UNIT=&UNIT1,
//*//            DCB=MODLDSCB,RECFM=FB,LRECL=94
//            DCB=(MODLDSCB,RECFM=FB,LRECL=94)
