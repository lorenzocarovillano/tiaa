//P9983CWD PROC DBID='045',                                             
//         DMPS='U',                                                    
//         HLQ0='PCWF.COR',                                             
//         HLQ8='PPDD',                                                 
//         JCLO='*',                                                    
//         LIB1='PROD',             PARMLIB                             
//         LIB2='PROD',             NT2LOGON.PARMLIB                    
//         NAT='NAT045P',                                               
//         NATMEM='PANNSEC',                                            
//         REGN1='5M',                                                  
//         SPC1='50',                                                   
//         SPC2='10'                                                    
//*                                                                     
//**********************************************************************
//* CWF-READ MIT AND PURGE RECORDS BASED ON ISN LIST OF RANGES          
//**********************************************************************
//* SORT010  - SORT INPUT FILE WITH ISNS (PARM=P9983CW1)                
//* I/P CREATED BY NDM PROCESS ADDA9982  (P9982CWX FROM MVSDNPR)        
//* UPDT020  - PURGE ISN RANGES          (PARM=P9983CW2)                
//**********************************************************************
//SORT010  EXEC PGM=SORT                                                
//SYSOUT   DD  SYSOUT=&JCLO                                             
//SYSUDUMP DD  SYSOUT=&DMPS                                             
//SORTIN   DD  DSN=&HLQ0..MIH.ISN.RANGES,DISP=SHR                       
//SORTOUT  DD  DSN=&HLQ8..MIH.ISN.SORT,UNIT=SYSDA,                      
//             DISP=(NEW,CATLG,DELETE),                                 
//             SPACE=(CYL,(&SPC1,&SPC2),RLSE),                          
//             DSORG=PS,RECFM=FB,LRECL=19                               
//SYSIN    DD  DSN=&LIB1..PARMLIB(P9983CW1),DISP=SHR                    
//**********************************************************************
//UPDT020  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),                  
//         PARM='SYS=&NAT'                                              
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//SYSPRINT DD  SYSOUT=&JCLO                                             
//SYSOUT   DD  SYSOUT=&JCLO                                             
//DDPRINT  DD  SYSOUT=&JCLO                                             
//CMPRINT  DD  SYSOUT=&JCLO                                             
//SYSUDUMP DD  SYSOUT=&DMPS                                             
//CMPRT01  DD  SYSOUT=&JCLO                                             
//CMWKF01  DD  DSN=&HLQ8..MIH.ISN.SORT,DISP=SHR                         
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR            
//         DD  DSN=&LIB1..PARMLIB(P9983CW2),DISP=SHR                    
