//PNI1030D PROC JCLO='*',                                               
//         HLQ1='PPM.CIS',
//         HLQ2='PPM.ACIS',
//         HLQ3='PPM.ACIS.CIS',
//         LIB1='PROD',
//         SPC1='50,15',
//         GDG0=(0),
//         GDG1=(+1)
//** -----------------------------------------------------------------**
//* MODIFICATION HISTORY                                               *
//** ---------------------------------------------------------------- **
//BENCONT EXEC PGM=IEBGENER
//SYSUT1   DD DISP=SHR,DSN=&HLQ1..EXTR010.BENCONT&GDG0
//         DD DISP=SHR,DSN=&HLQ1..UPDT030.BENCONT&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR02.BENCONT&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR03.BENCONT&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR04.BENCONT&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR05.BENCONT&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR06.BENCONT&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR07.BENCONT&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR08.BENCONT&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR09.BENCONT&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR10.BENCONT&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR11.BENCONT&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR12.BENCONT&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR13.BENCONT&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR14.BENCONT&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR15.BENCONT&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR16.BENCONT&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR17.BENCONT&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR18.BENCONT&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR19.BENCONT&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR20.BENCONT&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR21.BENCONT&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR22.BENCONT&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR23.BENCONT&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR24.BENCONT&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR25.BENCONT&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR26.BENCONT&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR27.BENCONT&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR28.BENCONT&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR29.BENCONT&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR30.BENCONT&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR31.BENCONT&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR32.BENCONT&GDG0
//         DD DISP=SHR,DSN=&HLQ2..UPDT126.BENCONT&GDG0
//SYSUT2   DD DSN=&HLQ3..BENE.CONT.FILE&GDG1,
//            DISP=(,CATLG,DELETE),
//            SPACE=(CYL,(&SPC1),RLSE),
//            DCB=*.SYSUT1
//SYSPRINT DD SYSOUT=&JCLO
//SYSIN    DD DUMMY
//*
//BENMOS  EXEC PGM=IEBGENER
//SYSUT1   DD DISP=SHR,DSN=&HLQ1..UPDT030.BENMOS&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR02.BENMOS&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR03.BENMOS&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR04.BENMOS&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR05.BENMOS&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR06.BENMOS&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR07.BENMOS&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR08.BENMOS&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR09.BENMOS&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR10.BENMOS&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR11.BENMOS&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR12.BENMOS&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR13.BENMOS&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR14.BENMOS&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR15.BENMOS&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR16.BENMOS&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR17.BENMOS&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR18.BENMOS&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR19.BENMOS&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR20.BENMOS&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR21.BENMOS&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR22.BENMOS&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR23.BENMOS&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR24.BENMOS&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR25.BENMOS&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR26.BENMOS&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR27.BENMOS&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR28.BENMOS&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR29.BENMOS&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR30.BENMOS&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR31.BENMOS&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR32.BENMOS&GDG0
//         DD DISP=SHR,DSN=&HLQ2..UPDT126.BENMOS&GDG0
//SYSUT2   DD DSN=&HLQ3..BENE.MOS.FILE&GDG1,
//            DISP=(,CATLG,DELETE),
//            SPACE=(CYL,(&SPC1),RLSE),
//            DCB=*.SYSUT1
//SYSPRINT DD SYSOUT=&JCLO
//SYSIN    DD DUMMY
//*
//BENDEST EXEC PGM=IEBGENER
//SYSUT1   DD DISP=SHR,DSN=&HLQ1..EXTR010.BENDEST&GDG0
//         DD DISP=SHR,DSN=&HLQ1..UPDT030.BENDEST&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR02.BENDEST&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR03.BENDEST&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR04.BENDEST&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR05.BENDEST&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR06.BENDEST&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR07.BENDEST&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR08.BENDEST&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR09.BENDEST&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR10.BENDEST&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR11.BENDEST&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR12.BENDEST&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR13.BENDEST&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR14.BENDEST&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR15.BENDEST&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR16.BENDEST&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR17.BENDEST&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR18.BENDEST&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR19.BENDEST&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR20.BENDEST&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR21.BENDEST&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR22.BENDEST&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR23.BENDEST&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR24.BENDEST&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR25.BENDEST&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR26.BENDEST&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR27.BENDEST&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR28.BENDEST&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR29.BENDEST&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR30.BENDEST&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR31.BENDEST&GDG0
//         DD DISP=SHR,DSN=&HLQ2..EXTR32.BENDEST&GDG0
//         DD DISP=SHR,DSN=&HLQ2..UPDT126.BENDEST&GDG0
//SYSUT2   DD DSN=&HLQ3..BENE.DEST.FILE&GDG1,
//            DISP=(,CATLG,DELETE),
//            SPACE=(CYL,(&SPC1),RLSE),
//            DCB=*.SYSUT1
//SYSPRINT DD SYSOUT=&JCLO
//SYSIN    DD DUMMY
//*********************************************************************
//*    USE SYNCSORT TO REMOVE BLANK RECORDS FROM CONTRACT FILE
//*********************************************************************
//OMITCONT EXEC PGM=SORT                                                
//SYSOUT   DD  SYSOUT=*                                                 
//SORTWK01 DD  SPACE=(CYL,(20,20),RLSE),                                
//             UNIT=SYSDA                                               
//SORTWK02 DD  SPACE=(CYL,(20,20),RLSE),                                
//             UNIT=SYSDA                                               
//*
//SORTIN  DD DSN=&HLQ3..BENE.CONT.FILE&GDG1,DISP=SHR                    
//SORTOUT DD DSN=&HLQ3..BENE.CONT.FILE&GDG1,DISP=SHR                    
//SYSIN   DD DSN=&LIB1..PARMLIB(NI1030D1),DISP=SHR                      
//*
//*********************************************************************
//*    USE SYNCSORT TO REMOVE BLANK RECORDS FROM MOS FILE
//*********************************************************************
//OMITMOS  EXEC PGM=SORT                                                
//SYSOUT   DD  SYSOUT=*                                                 
//SORTWK01 DD  SPACE=(CYL,(20,20),RLSE),                                
//             UNIT=SYSDA                                               
//SORTWK02 DD  SPACE=(CYL,(20,20),RLSE),                                
//             UNIT=SYSDA                                               
//*
//SORTIN  DD DSN=&HLQ3..BENE.MOS.FILE&GDG1,DISP=SHR                     
//SORTOUT DD DSN=&HLQ3..BENE.MOS.FILE&GDG1,DISP=SHR                     
//SYSIN   DD DSN=&LIB1..PARMLIB(NI1030D1),DISP=SHR                      
//*
//*********************************************************************
//*    USE SYNCSORT TO REMOVE BLANK RECORDS FROM DEST FILE
//*********************************************************************
//OMITDEST EXEC PGM=SORT                                                
//SYSOUT   DD  SYSOUT=*                                                 
//SORTWK01 DD  SPACE=(CYL,(20,20),RLSE),                                
//             UNIT=SYSDA                                               
//SORTWK02 DD  SPACE=(CYL,(20,20),RLSE),                                
//             UNIT=SYSDA                                               
//*
//SORTIN  DD DSN=&HLQ3..BENE.DEST.FILE&GDG1,DISP=SHR                    
//SORTOUT DD DSN=&HLQ3..BENE.DEST.FILE&GDG1,DISP=SHR                    
//SYSIN   DD DSN=&LIB1..PARMLIB(NI1030D1),DISP=SHR                      
//*
