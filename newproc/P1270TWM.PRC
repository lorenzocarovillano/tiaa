//P1270TWM PROC DBID='003',
//         DMPS='U',
//         GDG1='+1',
//         HLQ0='PNPD.COR.TAX',
//         HLQ7='PPDT.TAX',
//         HLQ8='PPDD.TAX',
//         JCLO='*',
//         JOBNM='P1270TWM',
//         JOBNM2='P1260TWM',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='8M',
//         REPT='8',
//         SPC1='450,10',
//         SPC2='15,5',
//         SPC3='1,1',
//         SPC4='50,10'
//*
//*---------------------------------------------------------------------
//* PRNT005 - PRINTS TRANSACTION MISSING FIELDS REPORTS FROM
//*           SEQUENTIAL INPUT.
//*           NATURAL PROGRAM "TWRP0690".
//* EXTR010 - EDIT FEEDER SYSTEMS EXTRACT.                              
//*           NATURAL PROGRAM "TWRP0700".
//* SORT020 - SORT COMBINED ACCEPTED AND CITED (DURING EDITS) RECORDS
//*           EXTRACT BY SOURCE CODE, COMPANY CODE, PAYMENT TYPE,
//*           SETTLEMENT TYPE, AND REFERENCE-NUMBER.
//* EDIT030 - MORE EDITS OF FEEDER SYSTEMS EXTRACT.                     
//*           NATURAL PROGRAM "TWRP0705".
//* EXTR040 - EDITS, REFORMATES AND CREATES TRANSACTION FILE TO LOAD
//*           THE TAX DATABASES.
//*           NATURAL PROGRAM "TWRP0665".
//* LOAD050 - THE PROGRAM ASSIGNS UNIQUE NUMBER TO EACH TRANSACTION
//*           AT THE END.  THE UNIQUE NUMBER COUNTER IS STORED ON THE
//*           CONTROL FILE (003/098).  FEEDER TABLE NO. 5 ORIGIN "XX".
//*           NATURAL PROGRAM "TWRP0800".
//*---------------------------------------------------------------------
//*                                                                     
//* MODIFICATION HISTORY
//* 05/09/02 - OIA JCL STANDARDS - PHIL STEINHAUSER
//* 10/14/04 : A. YOUNG - MODIFY STEP EXTR010 DD CMWKF03 FROM &HLQ8
//*                       TO &HLQ0, AND CHANGE DISP TO SHR.
//*                                                                     
//**********************************************************************
//* PRNT005 - PRINTS TRANSACTION MISSING FIELDS REPORTS FROM
//*           SEQUENTIAL INPUT.
//*           NATURAL PROGRAM "TWRP0690".
//*---------------------------------------------------------------------
//PRNT005  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),                  
//         PARM='SYS=&NAT'                                              
//SYSOUT    DD SYSOUT=&JCLO
//SYSUDUMP  DD SYSOUT=&DMPS
//SYSPRINT  DD SYSOUT=&JCLO
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT   DD SYSOUT=&JCLO
//CMPRINT   DD SYSOUT=&JCLO
//CMPRT01   DD SYSOUT=&REPT
//CMPRT02   DD SYSOUT=&REPT
//CMPRT03   DD SYSOUT=&REPT
//CMPRT04   DD SYSOUT=&REPT
//CMPRT05   DD SYSOUT=&REPT
//CMWKF01   DD DSN=&HLQ7..&JOBNM2..SORT040.AP.EXT,DISP=OLD              
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR            
//          DD DSN=&LIB1..PARMLIB(P1270TW6),DISP=SHR                    
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                     
//* TWRP0690
//*---------------------------------------------------------------------
//* EXTR010 - EDIT FEEDER SYSTEMS EXTRACT.                              
//*           NATURAL PROGRAM "TWRP0700".
//*---------------------------------------------------------------------
//EXTR010  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),                  
//         PARM='SYS=&NAT'                                              
//SYSOUT    DD SYSOUT=&JCLO                                             
//SYSUDUMP  DD SYSOUT=&DMPS                                             
//SYSPRINT  DD SYSOUT=&JCLO                                             
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//DDPRINT   DD SYSOUT=&JCLO                                             
//CMPRINT   DD SYSOUT=&JCLO                                             
//CMPRT01   DD SYSOUT=&REPT                                             
//CMPRT02   DD SYSOUT=&REPT                                             
//CMPRT03   DD SYSOUT=&REPT                                             
//CMPRT04   DD SYSOUT=&REPT                                             
//CMPRT05   DD SYSOUT=&REPT                                             
//CMWKF01   DD DSN=&HLQ7..&JOBNM2..SORT040.AP.EXT,DISP=OLD              
//CMWKF02   DD DSN=&HLQ8..&JOBNM..EXTR010.ACCEPTED.RECS,                
//             DISP=(NEW,CATLG,DELETE),UNIT=SYSDA,                      
//             SPACE=(CYL,(&SPC1),RLSE),                                
//*//             DCB=RECFM=FB,LRECL=770                                   
//             DCB=(RECFM=FB,LRECL=770)
//CMWKF03   DD DSN=&HLQ0..&JOBNM..EXTR010.REJECTED.RECS,                
//             DISP=SHR                                                 
//*            SPACE=(CYL,(&SPC2),RLSE),                                
//*            DCB=RECFM=FB,LRECL=650                                   
//CMWKF04   DD DSN=&HLQ8..&JOBNM..EXTR010.CITED.RECS,                   
//             DISP=(NEW,CATLG,DELETE),UNIT=SYSDA,                      
//             SPACE=(CYL,(&SPC2),RLSE),                                
//*//             DCB=RECFM=FB,LRECL=770                                   
//             DCB=(RECFM=FB,LRECL=770)
//CMWKF05   DD DSN=&HLQ0..&JOBNM2..EXTR050.AP.SUM,DISP=SHR              
//CMWKF06   DD DSN=&HLQ8..&JOBNM..EXTR010.ACCEPTED.SUM.RECS,            
//             DISP=(NEW,CATLG,DELETE),UNIT=SYSDA,                      
//             SPACE=(CYL,(&SPC3),RLSE),                                
//*//             DCB=RECFM=FB,LRECL=650                                   
//             DCB=(RECFM=FB,LRECL=650)
//CMWKF07   DD DSN=&HLQ8..&JOBNM..EXTR010.CITED.SUM.RECS,               
//             DISP=(NEW,CATLG,DELETE),UNIT=SYSDA,                      
//             SPACE=(CYL,(&SPC3),RLSE),                                
//*//             DCB=RECFM=FB,LRECL=650                                   
//             DCB=(RECFM=FB,LRECL=650)
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR            
//          DD DSN=&LIB1..PARMLIB(P1270TW1),DISP=SHR                    
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                     
//* TWRP0700
//*---------------------------------------------------------------------
//* SORT020 - SORT COMBINED ACCEPTED AND CITED (DURING EDITS) RECORDS
//*           EXTRACT BY SOURCE CODE, COMPANY CODE, PAYMENT TYPE,
//*           SETTLEMENT TYPE, AND REFERENCE-NUMBER.
//*---------------------------------------------------------------------
//SORT020  EXEC PGM=SORT,REGION=&REGN1,COND=(0,NE)
//SYSOUT    DD SYSOUT=&JCLO
//SYSUDUMP  DD SYSOUT=&DMPS
//SORTWK01  DD UNIT=SYSDA,SPACE=(CYL,(&SPC4),RLSE)
//SORTWK02  DD UNIT=SYSDA,SPACE=(CYL,(&SPC4),RLSE)
//SORTWK03  DD UNIT=SYSDA,SPACE=(CYL,(&SPC4),RLSE)
//SORTWK04  DD UNIT=SYSDA,SPACE=(CYL,(&SPC4),RLSE)
//SYSPRINT  DD SYSOUT=&JCLO
//SORTIN    DD DSN=&HLQ8..&JOBNM..EXTR010.ACCEPTED.RECS,                
//             DISP=(OLD,DELETE,KEEP)
//          DD DSN=&HLQ8..&JOBNM..EXTR010.CITED.RECS,                   
//             DISP=(OLD,DELETE,KEEP)
//SORTOUT   DD DSN=&HLQ8..&JOBNM..SORT020.ACCRECS.SRTED,                
//             DISP=(NEW,CATLG,DELETE),UNIT=SYSDA,
//             SPACE=(CYL,(&SPC1),RLSE),
//*//             DCB=RECFM=FB,LRECL=770
//             DCB=(RECFM=FB,LRECL=770)
//SYSIN     DD DSN=&LIB1..PARMLIB(P1270TW2),DISP=SHR
//* SORT FIELDS=(1,1,CH,A,2,2,CH,A,40,2,CH,A)                           
//*---------------------------------------------------------------------
//* EDIT030 - MORE EDITS OF FEEDER SYSTEMS EXTRACT.                     
//*           NATURAL PROGRAM "TWRP0705".
//*---------------------------------------------------------------------
//EDIT030  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),                  
//         PARM='SYS=&NAT'                                              
//SYSOUT    DD SYSOUT=&JCLO
//SYSUDUMP  DD SYSOUT=&DMPS
//SYSPRINT  DD SYSOUT=&JCLO
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT   DD SYSOUT=&JCLO
//CMPRINT   DD SYSOUT=&JCLO
//CMPRT01   DD SYSOUT=&REPT
//CMWKF01   DD DSN=&HLQ8..&JOBNM..SORT020.ACCRECS.SRTED,DISP=SHR        
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR            
//          DD DSN=&LIB1..PARMLIB(P1270TW3),DISP=SHR                    
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                     
//*---------------------------------------------------------------------
//* EXTR040 - EDITS, REFORMATES AND CREATES TRANSACTION FILE TO LOAD
//*           THE TAX DATABASES.
//*           NATURAL PROGRAM "TWRP0665".
//*---------------------------------------------------------------------
//EXTR040  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),                  
//         PARM='SYS=&NAT'                                              
//SYSOUT    DD SYSOUT=&JCLO
//SYSUDUMP  DD SYSOUT=&DMPS
//SYSPRINT  DD SYSOUT=&JCLO
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT   DD SYSOUT=&JCLO
//CMPRINT   DD SYSOUT=&JCLO
//CMPRT01   DD SYSOUT=&REPT
//CMPRT02   DD SYSOUT=&REPT
//CMPRT03   DD SYSOUT=&REPT
//CMWKF01   DD DSN=&HLQ8..&JOBNM..SORT020.ACCRECS.SRTED,                
//             DISP=SHR
//CMWKF02   DD DSN=&HLQ0..&JOBNM2..AP.CONTROL,DISP=SHR
//CMWKF03   DD DSN=&HLQ8..&JOBNM..EXTR040,
//             DISP=(NEW,CATLG,DELETE),
//             LRECL=650,RECFM=FB,
//             SPACE=(CYL,(&SPC3),RLSE),UNIT=SYSDA
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR            
//         DD  DSN=&LIB1..PARMLIB(P1270TW4),DISP=SHR                    
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                     
//* TWRP0665
//*---------------------------------------------------------------------
//* LOAD050 - THE PROGRAM ASSIGNS UNIQUE NUMBER TO EACH TRANSACTION
//*           AT THE END.  THE UNIQUE NUMBER COUNTER IS STORED ON THE
//*           CONTROL FILE (003/098).  FEEDER TABLE NO. 5 ORIGIN "XX".
//*           NATURAL PROGRAM "TWRP0800".
//*---------------------------------------------------------------------
//LOAD050  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),                  
//         PARM='SYS=&NAT'                                              
//SYSPRINT  DD SYSOUT=&JCLO
//SYSUDUMP  DD SYSOUT=&DMPS
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT   DD SYSOUT=&JCLO
//CMPRINT   DD SYSOUT=&JCLO
//CMPRT01   DD SYSOUT=&REPT
//CMPRT02   DD SYSOUT=&REPT
//CMWKF01   DD DSN=&HLQ8..&JOBNM..SORT020.ACCRECS.SRTED,                
//             DISP=(OLD,DELETE,KEEP)
//CMWKF02   DD DSN=&HLQ0..&JOBNM..LOAD050.ACREC(&GDG1),                 
//             DISP=(NEW,CATLG,DELETE),
//             UNIT=SYSDA,SPACE=(CYL,(475,95),RLSE),
//             DCB=(MODLDSCB,LRECL=800,RECFM=FB)
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR            
//          DD DSN=&LIB1..PARMLIB(P1270TW5),DISP=SHR                    
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                     
//* TWRP0800
