//PCP0606D PROC HLQ0='PCPS.ANN',    DASD HLQ
//         HLQ8='PCPS.ANN',         HLQ
//         JOBNM='PCP0606D',        2ND HLQ FOR FILES CREATED
//         DMPS='U',
//         DEST0J='MVSPROD1',       DESTINATION
//         REGN1='9M',              REGION SIZE
//         GDG='+1',                NEW GDG FILE CREATED
//         SPC1='20',               FILE1 - PRIMARY ALLOCATION
//         SPC2='5',                FILE1 - SECONDARY ALLOCATION
//         RPTID='CP0606D',         MOBIUS
//         REPT1='8'                MOBIUS
//*
//*--------------------------------------------------------------------*
//*                                                                    *
//* PROCEDURE:  PCP0606D                           DATE:    09/10/2008 *
//*                                                                    *
//*         CONSOLIDATED PAYMENT SYSTEM / INVESTMENT SOLUTIONS         *
//*                                                                    *
//*               COPY  NACHA RETERN FILE TO GENERATION DATASETS        
//*   JPMORGAN ACCOUNTS
//*         009440001        TECHSRPT        TECHRET                   *
//*         926405900        TIDASRPT        TIDARET                   *
//*         005092201        TAAASRPT        TAAARET                   *
//*         9785079087       TIAASRPT        TIAARET                   *
//*                                                                    *
//*
//*  009440001 TECHSRPT  APTDTI52 / UPTDTT14.NDM.OUTPUT.TIAA.PPTDTI52(+1
//*                                 PCPS.ANN.NACHA.RETURN01.CHASEIN
//*
//*  926405900 TIDASRPT  APTDTI54 / UPTDTT14.NDM.OUTPUT.TIAA.PPTDTI54(+1
//*                                 PCPS.ANN.NACHA.RETURN02.CHASEIN
//*
//*  005092201 TAAASRPT  APTDTI56 / UPTDTT14.NDM.OUTPUT.TIAA.PPTDTI56(+1
//*                                 PCPS.ANN.NACHA.RETURN03.CHASEIN
//*
//*  9785079087 TIAASRPT APTDTI58 / UPTDTT14.NDM.OUTPUT.TIAA.PPTDTI58(+1
//*                                 PCPS.ANN.NACHA.RETURN04.CHASEIN
//*
//*  STEP    PROGRAM   PARMS                   COMMENTS                *
//* -------  -------  --------  -------------------------------------- *
//* COPY010  IEBGENER           COPY FILE TO DASD  NACHA - RET 1       *
//* COPY020  IEBGENER           COPY FILE TO DASD  NACHA - RET 2       *
//* COPY030  IEBGENER           COPY FILE TO DASD  NACHA - RET 3       *
//* COPY040  IEBGENER           COPY FILE TO DASD  NACHA - RET 4       *
//* COPY050  IEBGENER           COPY FILE TO MOBIUS NACHA - MOBIUS 1   *
//* COPY060  IEBGENER           COPY FILE TO MOBIUS NACHA - MOBIUS 2   *
//* COPY070  IEBGENER           COPY FILE TO MOBIUS NACHA - MOBIUS 3   *
//* COPY080  IEBGENER           COPY FILE TO MOBIUS NACHA - MOBIUS 4   *
//* DELT090  IEFBR14            DELETE THE TEMPORARY DSNS FFROM BANK   *
//*                                                                    *
//*--------------------------------------------------------------------*
//* COPY010  -  COPY FILE TO DASD  NACHA - RET 1                       *
//*             IEBGENER                                               *
//*--------------------------------------------------------------------*
//COPY010 EXEC PGM=IEBGENER,REGION=&REGN1,COND=(0,NE)
//SYSOUT   DD SYSOUT=&DMPS,DEST=&DEST0J,OUTPUT=*.OUTLOCL
//SYSPRINT DD SYSOUT=&DMPS,DEST=&DEST0J,OUTPUT=*.OUTLOCL
//SYSUT1   DD  DSN=&HLQ8..NACHA.RETURN01.CHASEIN,DISP=SHR
//SYSUT2   DD  DSN=&HLQ0..&JOBNM..NACHA.RET01.CHASE(&GDG),
//         DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//         UNIT=SYSDA,SPACE=(CYL,(&SPC1,&SPC2))
//**       RECFM=FB,LRECL=132
//SYSIN    DD DUMMY
//*
//*--------------------------------------------------------------------*
//* COPY020  -  COPY FILE TO DASD  NACHA - RET 2                       *
//*             IEBGENER                                               *
//*--------------------------------------------------------------------*
//COPY020 EXEC PGM=IEBGENER,REGION=&REGN1,COND=(0,NE)
//SYSOUT   DD SYSOUT=&DMPS,DEST=&DEST0J,OUTPUT=*.OUTLOCL
//SYSPRINT DD SYSOUT=&DMPS,DEST=&DEST0J,OUTPUT=*.OUTLOCL
//SYSUT1   DD  DSN=&HLQ8..NACHA.RETURN02.CHASEIN,DISP=SHR
//SYSUT2   DD  DSN=&HLQ0..&JOBNM..NACHA.RET02.CHASE(&GDG),
//         DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//         UNIT=SYSDA,SPACE=(CYL,(&SPC1,&SPC2))
//**       RECFM=FB,LRECL=132
//SYSIN    DD DUMMY
//*
//*--------------------------------------------------------------------*
//* COPY030  -  COPY FILE TO DASD  NACHA - RET 3                       *
//*             IEBGENER                                               *
//*--------------------------------------------------------------------*
//COPY030 EXEC PGM=IEBGENER,REGION=&REGN1,COND=(0,NE)
//SYSOUT   DD SYSOUT=&DMPS,DEST=&DEST0J,OUTPUT=*.OUTLOCL
//SYSPRINT DD SYSOUT=&DMPS,DEST=&DEST0J,OUTPUT=*.OUTLOCL
//SYSUT1   DD  DSN=&HLQ8..NACHA.RETURN03.CHASEIN,DISP=SHR
//SYSUT2   DD  DSN=&HLQ0..&JOBNM..NACHA.RET03.CHASE(&GDG),
//         DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//         UNIT=SYSDA,SPACE=(CYL,(&SPC1,&SPC2))
//**       RECFM=FB,LRECL=132
//SYSIN    DD DUMMY
//*
//*--------------------------------------------------------------------*
//* COPY040  -  COPY FILE TO DASD  NACHA - RET 2                       *
//*             IEBGENER                                               *
//*--------------------------------------------------------------------*
//COPY040 EXEC PGM=IEBGENER,REGION=&REGN1,COND=(0,NE)
//SYSOUT   DD SYSOUT=&DMPS,DEST=&DEST0J,OUTPUT=*.OUTLOCL
//SYSPRINT DD SYSOUT=&DMPS,DEST=&DEST0J,OUTPUT=*.OUTLOCL
//SYSUT1   DD  DSN=&HLQ8..NACHA.RETURN04.CHASEIN,DISP=SHR
//SYSUT2   DD  DSN=&HLQ0..&JOBNM..NACHA.RET04.CHASE(&GDG),
//         DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//         UNIT=SYSDA,SPACE=(CYL,(&SPC1,&SPC2))
//**       RECFM=FB,LRECL=132
//SYSIN    DD DUMMY
//*
//*--------------------------------------------------------------------*
//* COPY 050    COPY FILE TO MOBIUS NACHA FILE 1                       *
//*             IEBGENER                                               *
//*--------------------------------------------------------------------*
//COPY050 EXEC PGM=IEBGENER,REGION=&REGN1,COND=(0,NE)
//SYSOUT   DD SYSOUT=&DMPS,DEST=&DEST0J,OUTPUT=*.OUTLOCL
//SYSPRINT DD SYSOUT=&DMPS,DEST=&DEST0J,OUTPUT=*.OUTLOCL
//SYSUT1   DD DSN=&HLQ8..NACHA.RETURN01.CHASEIN,DISP=SHR
//SYSUT2   DD SYSOUT=(&REPT1,&RPTID.1)
//SYSIN    DD DUMMY
//*
//*--------------------------------------------------------------------*
//* COPY 060    COPY FILE TO MOBIUS NACHA FILE 2                       *
//*             IEBGENER                                               *
//*--------------------------------------------------------------------*
//COPY060 EXEC PGM=IEBGENER,REGION=&REGN1,COND=(0,NE)
//SYSOUT   DD SYSOUT=&DMPS,DEST=&DEST0J,OUTPUT=*.OUTLOCL
//SYSPRINT DD SYSOUT=&DMPS,DEST=&DEST0J,OUTPUT=*.OUTLOCL
//SYSUT1   DD DSN=&HLQ8..NACHA.RETURN02.CHASEIN,DISP=SHR
//SYSUT2   DD SYSOUT=(&REPT1,&RPTID.2)
//SYSIN    DD DUMMY
//*
//*--------------------------------------------------------------------*
//* COPY 070    COPY FILE TO MOBIUS NACHA FILE 3                       *
//*             IEBGENER                                               *
//*--------------------------------------------------------------------*
//COPY070 EXEC PGM=IEBGENER,REGION=&REGN1,COND=(0,NE)
//SYSOUT   DD SYSOUT=&DMPS,DEST=&DEST0J,OUTPUT=*.OUTLOCL
//SYSPRINT DD SYSOUT=&DMPS,DEST=&DEST0J,OUTPUT=*.OUTLOCL
//SYSUT1   DD DSN=&HLQ8..NACHA.RETURN03.CHASEIN,DISP=SHR
//SYSUT2   DD SYSOUT=(&REPT1,&RPTID.3)
//SYSIN    DD DUMMY
//*
//*--------------------------------------------------------------------*
//* COPY 080    COPY FILE TO MOBIUS NACHA FILE 4                       *
//*             IEBGENER                                               *
//*--------------------------------------------------------------------*
//COPY080 EXEC PGM=IEBGENER,REGION=&REGN1,COND=(0,NE)
//SYSOUT   DD SYSOUT=&DMPS,DEST=&DEST0J,OUTPUT=*.OUTLOCL
//SYSPRINT DD SYSOUT=&DMPS,DEST=&DEST0J,OUTPUT=*.OUTLOCL
//SYSUT1   DD DSN=&HLQ8..NACHA.RETURN04.CHASEIN,DISP=SHR
//SYSUT2   DD SYSOUT=(&REPT1,&RPTID.4)
//SYSIN    DD DUMMY
//*
//*--------------------------------------------------------------------*
//* DELT090  -  DELETE THE TEMPORARY DSN'S FROM THE BANK SO TOMORROWS  *
//*             FILES CAN BE REALLOCATED BY THE BANK
//*             IEFBR14                                                *
//*--------------------------------------------------------------------*
//DELT090  EXEC PGM=IEFBR14,COND=(0,NE)
//SYSPRINT DD SYSOUT=&DMPS,DEST=&DEST0J,OUTPUT=*.OUTLOCL
//DD01     DD  DSN=&HLQ8..NACHA.RETURN01.CHASEIN,
//         DISP=(OLD,DELETE,DELETE)
//DD02     DD  DSN=&HLQ8..NACHA.RETURN02.CHASEIN,
//         DISP=(OLD,DELETE,DELETE)
//DD03     DD  DSN=&HLQ8..NACHA.RETURN03.CHASEIN,
//         DISP=(OLD,DELETE,DELETE)
//DD04     DD  DSN=&HLQ8..NACHA.RETURN04.CHASEIN,
//         DISP=(OLD,DELETE,DELETE)
