//PCW5910D PROC DBID=045,                                               
//             HLQ1='PCWF.ANN',                                         
//             JCLO='*',                                                
//             DMPS='*',                                                
//             LIB1='PROD',           NT2LOGON.PARMLIB                  
//             LIB2='PROD',           PROD.PARMLIB                      
//             NAT='NAT045P',                                           
//             NATMEM='PANNSEC',                                        
//             REGN1='9M'                                               
//**------------------------------------------------------------------* 
//* PROCEDURE:  PCW5910D                                                
//*                                                                     
//*   A003208 = SGC-PLAN-COMPONENTS-CONV-HIST-FL                        
//*                                                                     
//********************************************************************* 
//* PROGRAM :  CWFB5900 NATURAL                                         
//* FUNCTION:  READS THE OMNI BANKING MODEL EXTRACT FILE AND POPULATE   
//*            BANKING-MODEL-PPG IN CWF SYSTEM CONTROL TABLE            
//**------------------------------------------------------------------* 
//UPDT010  EXEC PGM=NATB030,                                            
//             COND=(0,LT),                                             
//             PARM='SYS=&NAT,WH=ON',                                   
//             REGION=&REGN1                                            
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//SYSPRINT DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR                             
//DDPRINT  DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR                             
//SYSOUT   DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR                             
//SYSUDUMP DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL                            
//CMPRINT  DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR                             
//CMPRT01  DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR                             
//CMWKF01  DD  DSN=&HLQ1..PCW5900D.OMNI.EXTRACT,                        
//             DISP=SHR                                                 
//CMSYNIN  DD  DSN=&LIB1..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR            
//         DD  DSN=&LIB2..PARMLIB(CW5910D1),DISP=SHR                    
