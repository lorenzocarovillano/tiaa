//PNI9250D PROC DBID='043',
//         DMPS='U',
//         JCLO='*',
//         HLQ2='PPM.ANN',
//         HLQ3='POMPL',
//         REPT='8',
//         GDG0='0',
//         GDG1='+1',
//         LIB2='PROD',
//         LIB3DSN='PROD.PARMLIB',
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         SPC1='50,30'
//*
//**********************************************************************
//*                                                                    *
//* PROCEDURE        :  PNI9250D                                       *
//* ACCEPTS REMITTANCE ENROLLMENT TRANSACTIONS AND BUILDS T813 CARDS   *
//* THAT WILL USED BY SCIB9251.                                        *
//**********************************************************************
//* MODIFICATIONS:
//**********************************************************************
//**------------------------------------------------------------------**
//* PROGRAM SCIB9250                                                   *
//*                                                                    *
//* THIS STEP WILL ISSUE THE CONTRACTS AND CREATE T813 DATA FILE THAT  *
//* WILL BE USED TO CREATE T813 TRANSACTIONS.                          *
//**------------------------------------------------------------------**
//EXTR010  EXEC PGM=NATB030,COND=(0,NE),
//         PARM='SYS=&NAT'
//SYSOUT   DD  SYSOUT=&JCLO
//SYSABOUT DD  SYSOUT=&DMPS
//SYSUDUMP DD  SYSOUT=&DMPS
//DDCARD   DD  DSN=PROD.PARMLIB(DBAPP&DBID),DISP=SHR
//CMPRINT  DD  SYSOUT=&JCLO
//CMWKF01  DD  DSN=&HLQ2..RM.PRE.ACIS.DATA(&GDG0),DISP=SHR
//CMWKF02  DD  DSN=&HLQ2..ACIS.DSP.CNTRCT(&GDG1),
//             DATACLAS=DCPSEXTC,
//             DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,
//             SPACE=(CYL,(&SPC1),RLSE),
//             RECFM=FB
//CMPRT01  DD SYSOUT=(&REPT,NI9250D1)
//CMPRT02  DD SYSOUT=(&REPT,NI9250D2)
//CMPRT03  DD SYSOUT=(&REPT,NI9250D3)
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB3DSN(NI9250D1),DISP=SHR
//         DD  DSN=&LIB2..PARMLIB(FINPARM),DISP=SHR
//*
//**------------------------------------------------------------------**
//* PROGRAM SORT                                                       *
//*                                                                    *
//* THIS STEP WILL SORT THE CONTRACTS DATA CREATED BY SCIB9250 IN      *
//* PLAN AND SUBPLAN ORDER.                                            *
//**------------------------------------------------------------------**
//COND020  IF EXTR010.RC < 4  THEN
//SORT020 EXEC PGM=SORT
//SORTIN  DD DSN=&HLQ2..ACIS.DSP.CNTRCT(&GDG1),DISP=SHR
//SORTOUT DD DSN=&HLQ2..ACIS.DSP.CNTRCT.SRT(&GDG1),
//             DATACLAS=DCPSEXTC,
//             DISP=(NEW,CATLG,DELETE),
//             UNIT=SYSDA,
//             SPACE=(CYL,(&SPC1),RLSE),
//             RECFM=FB
//SYSOUT   DD SYSOUT=&JCLO
//SYSPRINT DD SYSOUT=&JCLO
//SYSIN    DD DSN=&LIB3DSN(NI9250D2),DISP=SHR
//SYSUDUMP DD SYSOUT=&DMPS
//*
//**------------------------------------------------------------------**
//* PROGRAM SCIB9251                                                   *
//*                                                                    *
//* THIS STEP CREATES THE T813 TRANSACTIONS THAT WILL BE LOADED INTO   *
//* THE OMNI VTRAN FILE.                                               *
//**------------------------------------------------------------------**
//EXTR020  EXEC PGM=NATB030,REGION=0M,                                  
//         PARM='SYS=&NAT'
//CMWKF01  DD  DSN=&HLQ2..ACIS.DSP.CNTRCT.SRT(&GDG1),DISP=SHR
//CMWKF02  DD  DSN=&HLQ3..F&FSET..VT01.T813(&GDG1),
//             DATACLAS=DCPSEXTC,
//             DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,
//             SPACE=(CYL,(&SPC1),RLSE),
//             RECFM=FB
//DDCARD   DD  DSN=&LIB2..PARMLIB(DBAPP003),DISP=SHR
//SYSPRINT DD  SYSOUT=*
//DDPRINT  DD  SYSOUT=*
//CMPRINT  DD  SYSOUT=*                                                 
//*
//CMPRT01  DD SYSOUT=(&REPT,NI9250D4)
//CMPRT02  DD SYSOUT=(&REPT,NI9250D5)
//CMPRT03  DD SYSOUT=(&REPT,NI9250D6)
//*
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB3DSN(NI9250D3),DISP=SHR                          
//         DD  DSN=&LIB2..PARMLIB(FINPARM),DISP=SHR
//LOCKFILE DD  DUMMY
//   ENDIF
