//P1000TWD PROC DBID='003',
//         DMPS='U',
//         HLQ0='PNPD.COR.TAX',
//         JCLO='*',
//         JOBNM='P1000TWD',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='8M',
//         REPT='8'
//*
//*---------------------------------------------------------------------
//*                                                                     
//* MODIFICATION HISTORY
//* 05/16/02 - OIA JCL STANDARDS - PHIL STEINHAUSER
//*                                                                     
//**********************************************************************
//* EXTR010 - CREATES A CONTROL RECORD EXTRACT FOR THE DAILY "CPS"
//*           TAX LOAD, USING LAST BUSINESS DAY'S EXECUTION CONTROL
//*           RECORDS EXTRACT FOR INPUT.  DAILY "CPS" SOURCE CODES
//*           ARE "DC", "DS", "MS", "NZ", AND "SS".
//*           NATURAL PROGRAM "TWRP0600".
//*---------------------------------------------------------------------
//EXTR010  EXEC PGM=NATB030,REGION=&REGN1,
//         PARM='SYS=&NAT'
//SYSOUT    DD SYSOUT=&JCLO
//SYSUDUMP  DD SYSOUT=&DMPS
//SYSPRINT  DD SYSOUT=&JCLO
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT   DD SYSOUT=&JCLO
//CMPRINT   DD SYSOUT=&JCLO
//CMPRT01   DD SYSOUT=&REPT
//CMPRT02   DD SYSOUT=&REPT
//CMPRT03   DD SYSOUT=&REPT
//CMWKF01   DD DSN=&HLQ0..&JOBNM..CONTROL.EXT,DISP=SHR
//CMWKF02   DD DSN=&HLQ0..&JOBNM..CONTROL.EXT,DISP=SHR
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(P1000TW1),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//* TWRP0600
