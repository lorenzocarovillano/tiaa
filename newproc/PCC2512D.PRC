//*********************************************************************
//*  PCC2512D  -   PROC TO BUILD NDM COMMANDS FOR SENDING FILES TO DCS
//*********************************************************************
//PCC2512D PROC JOBNM='XXXXXXXX',
//         REGN1=8M,
//         NATLOGON=PANNSEC,
//         NAT=NATB030,
//         SYS=NAT045P,
//         DBID=045,
//         LIB1=PROD,
//         HLQ8=PPT.COR,
//         JCLO=*,
//         DMPS=U,
//         PARMMEM1=PC2515D2,
//         PARMMEM2=PC2510D3
//*
//*
//*********************************************************************
//*        DEALLOCATE WORK DATASET FOR NDM COMMAND PARMS
//*********************************************************************
//DEALLOC1 EXEC PGM=IEFBR14
//SYSPRINT DD  SYSOUT=&JCLO
//TMPFL01  DD  DSN=&HLQ8..XML.&JOBNM..CREA010,
//             UNIT=SYSDA,SPACE=(0,(0)),DISP=(MOD,DELETE)
//SYSIN    DD  DUMMY
//*
//*
//*********************************************************************
//*    PCCB9557 - EXTRACT E-MAIL DATA / CREATE ORACLE CONTROL RECORD
//*               CREATE DIRECT:CONNECT CONTROL CARDS FOR COPY TO DCS
//*********************************************************************
//CREA010  EXEC PGM=&NAT,REGION=&REGN1,
//         PARM='SYS=&SYS,WH=ON'
//SYSPRINT DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMWKF01  DD  DSN=&HLQ8..XML.&JOBNM..CREA010,
//             UNIT=SYSDA,
//             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             SPACE=(CYL,(1,1),RLSE),
//             DCB=(DSORG=PS,RECFM=F,LRECL=120)
//CMWKF02  DD  DSN=&LIB1..PARMLIB(&PARMMEM2),DISP=SHR
//CMWKF03  DD  DSN=&HLQ8..&FILETYP..ECSFILE.SORTNSUM,DISP=SHR
//CMWKF04  DD  DSN=&LIB1..PARMLIB(POST2DCS),DISP=SHR
//CMSYNIN  DD  DSN=&LIB1..NT2LOGON.PARMLIB(&NATLOGON),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(PC2510D4),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(&PARMMEM1),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//*
