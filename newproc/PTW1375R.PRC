//PTW1375R PROC DBID='003',
//         DMPS='U',
//         GDG1='+1',
//         HLQ0='PNPD.COR.TAX',
//         HLQ8='PPDD.TAX',
//         JCLO='*',
//         JOBNM1=,
//         JOBNM2=,
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='9M',
//         REPT='8',
//         REPTID=' ',
//         SPC1='50,10'
//*
//*---------------------------------------------------------------------
//*                                                                     
//* MODIFICATION HISTORY
//*                                                                     
//* 05/25/10 - CREATED FOR W-2 CORRECTIONS REPORTING              -JRR- 
//*---------------------------------------------------------------------
//* EXTR010  - CREATES FF1 (CMWKF03) CONTAINING W2 PAYMENT RECORDS
//*            NATURAL PROGRAM "TWRP5707".
//*---------------------------------------------------------------------
//EXTR010  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//            PARM='SYS=&NAT'
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSOUT   DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//SYSPRINT DD SYSOUT=&JCLO
//DDPRINT  DD SYSOUT=&JCLO
//CMPRINT  DD SYSOUT=&JCLO
//CMWKF01  DD DSN=&HLQ0..&JOBNM2..CREA010.CTRL,DISP=SHR
//CMWKF02  DD DSN=&HLQ0..&JOBNM2..EXTR020.FF2,DISP=SHR
//CMWKF03  DD DSN=&HLQ0..&JOBNM1..EXTR010.FF1(&GDG1),
//            DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//            DCB=(LRECL=300,RECFM=FB),
//            SPACE=(CYL,(&SPC1),RLSE),UNIT=SYSDA
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(TW1375R1),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//* TWRP5707
//*---------------------------------------------------------------------
//* SORT020  - SORT EXTRACT FILE FOR W2 CORRECTIONS REPORTING
//*            COMPANY, RESIDENCY CODE, TAX-ID
//*---------------------------------------------------------------------
//SORT020   EXEC PGM=SORT,REGION=&REGN1,COND=(0,NE)
//SYSUDUMP  DD SYSOUT=&DMPS
//SYSOUT    DD SYSOUT=&JCLO
//SORTWK01  DD UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE)
//SORTWK02  DD UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE)
//SORTWK03  DD UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE)
//SORTWK04  DD UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE)
//SYSPRINT  DD SYSOUT=&JCLO
//SORTIN    DD DSN=&HLQ0..&JOBNM1..EXTR010.FF1(&GDG1),DISP=SHR
//SORTOUT   DD DSN=&HLQ8..&JOBNM1..EXTR010.FF1.SORTED,
//             DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             UNIT=SYSDA,
//             SPACE=(CYL,(&SPC1),RLSE),
//             LRECL=300,RECFM=FB
//SYSIN     DD DSN=&LIB1..PARMLIB(TW1375R2),DISP=SHR
//* SORT FIELDS=(23,1,CH,A,263,2,CH,A,6,10,CH,A)
//*---------------------------------------------------------------------
//* PRNT030  - PRINTS W2 CORRECTIONS REPORT
//*            TAX-ID WITHIN STATE WITHIN COMPANY
//*            NATURAL PROGRAM "TWRP5720".
//*---------------------------------------------------------------------
//PRNT030  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//            PARM='SYS=&NAT'
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSOUT   DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//SYSPRINT DD SYSOUT=&JCLO
//DDPRINT  DD SYSOUT=&JCLO
//CMPRINT  DD SYSOUT=&JCLO
//CMPRT01  DD SYSOUT=(&REPT,&REPTID)
//CMWKF01  DD DSN=&HLQ8..&JOBNM1..EXTR010.FF1.SORTED,DISP=SHR
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(TW1375R3),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//* TWRP5720
//*
