//PCP0691D PROC DBID='003',
//         DMPS='U',
//         GDG1='+1',
//         HLQ0='PIA.ANN',
//         JCLO='*',
//         JOBNM='PCP0691D',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='9M',
//         REPT='8',
//         SPC1='300',
//         SPC2='100',
//         UNIT1=SYSDA
//*
//*-------------------------------------------------------------------*
//* PROCEDURE:  PCP0691D                 DATE:  04/04/2006            *
//* ------------------------------------------------------------------*
//* EXTR010 - THIS STEP WILL EXTRACTS ALL PAYMENTS MADE FOR GIVEN     *
//*           ACCOUNTING DATE FROM PAYMENT-FILE -196                  *
//*           FOR ALL ORIGIN CODES                                    *
//*           NATURAL PROGRAM "FCPP691".                              *
//**********************************************************************
//*
//EXTR010 EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//        PARM='IM=D,SYS=&NAT'
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSPRINT DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//SYSOUT   DD SYSOUT=&JCLO
//DDPRINT  DD SYSOUT=&JCLO
//CMPRINT  DD SYSOUT=&JCLO
//CMPRT01  DD SYSOUT=(&REPT,CP0691D1)
//CMWKF01  DD DSN=&HLQ0..&JOBNM..CFREE.DAILY.EXT(&GDG1),
//            DISP=(,CATLG,DELETE),UNIT=&UNIT1,DATACLAS=DCPSEXTC,       
//            DCB=(MODLDSCB,RECFM=FB,LRECL=220),                        
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE)
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(CP0691D1),DISP=SHR
//* FCPP691
