//P1125IAM PROC GDG0='+0',                                              
//         GDG1='+1',                                                   
//         HLQ0='PIA.ANN',                                              
//         HLQ7='PPDT.PNA.IA',                                          
//         JCLO='*',                                                    
//         UNIT1='CARTV'                                                
//*                                                                     
//* ----------------------------------------------------------------- * 
//*                                                                     
//* PROCEDURE:  P1125IAM                 DATE:    07/10/94            * 
//*                                                                     
//* MODIFICATION HISTORY                                                
//* 04/03/02 - OIA JCL STANDARDS - PHIL STEINHAUSER                     
//*                                                                     
//* ----------------------------------------------------------------- * 
//* PROGRAMS EXECUTED: IEBGENER                                         
//* ----------------------------------------------------------------- * 
//*                                                                     
//COPY030  EXEC PGM=IEBGENER                                            
//SYSPRINT DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR                              
//SYSIN    DD DUMMY                                                     
//SYSUT1   DD DSN=&HLQ0..IMDANNP.T08.MNTHEND(&GDG0),                    
//            DISP=SHR                                                  
//SYSUT2   DD DSN=&HLQ7..IMDANNP.T08.MNTHEND.BU(&GDG1),                 
//            UNIT=&UNIT1,                                              
//            DISP=(,CATLG,DELETE),                                     
//            RECFM=FB                                                  
//*           DCB=MODLDSCB,RECFM=FB,LRECL=100,BLKSIZE=32700             
//********************************************************************* 
//COPY050  EXEC PGM=IEBGENER,COND=(0,NE)                                
//SYSPRINT DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR                              
//SYSIN    DD DUMMY                                                     
//SYSUT1   DD DSN=&HLQ0..IMDANNP.T24.MNTHEND(&GDG0),                    
//            DISP=SHR                                                  
//SYSUT2   DD DSN=&HLQ7..IMDANNP.T24.MNTHEND.BU(&GDG1),                 
//            UNIT=&UNIT1,                                              
//            DISP=(,CATLG,DELETE),                                     
//            RECFM=FB                                                  
//*           DCB=MODLDSCB,RECFM=FB,LRECL=120,BLKSIZE=32760             
