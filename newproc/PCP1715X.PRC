//PCP1715X PROC HLQ1=PROD,HLQ2=PPDD                                     
//S1 EXEC  PGM=IDCAMS                                                   
//SYSPRINT DD DUMMY                                                     
//SYSIN    DD DSN=&HLQ1..PARMLIB(CP1715D1),DISP=SHR                     
//S2       EXEC PGM=IKJEFT01,REGION=0M,COND=(0,NE),                     
//         PARM='CP1715R1'                                              
//SYSEXEC  DD  DISP=SHR,DSN=&HLQ1..REXXLIB                              
//SYSTSPRT DD  SYSOUT=*                                                 
//SYSOUT   DD  SYSOUT=*                                                 
//SYSTSIN  DD  DUMMY                                                    
//SYSUT1   DD DSN=&HLQ2..CP1715D0,DISP=SHR                              
//SYSUT2   DD DSN=&HLQ2..CP1715D1,DISP=(,CATLG),SPACE=(CYL,1)           
//SYSUT3   DD DSN=&HLQ2..CP1715D2,DISP=(,CATLG),SPACE=(CYL,1)           
//SYSUT4   DD DSN=&HLQ2..CP1715D3,DISP=(,CATLG),SPACE=(CYL,1)           
