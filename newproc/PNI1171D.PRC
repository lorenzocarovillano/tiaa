//PNI1171D PROC DBID=003,                                               
//         LIB1='PROD',                                                 
//         LIB2='PROD',                                                 
//         HLQ1='PNA.ANN',                                              
//         JOBNM='XXXXXXXX',                                            
//         PKGTYP='XXXX',                                               
//         NAT='NAT003P',                                               
//         NATMEM='PANNSEC',                                            
//         PARMMEM2='XXXXXXXX',                                         
//         REGN1='8M',                                                  
//         JCLO='*',                                                    
//         DMPS='U'                                                     
//*                                                                     
//********************************************************************* 
//* CREATE CONNECT:DIRECT CONTROL CARDS                               * 
//* RUN NATURAL PROGRAM APPB1171                                      * 
//********************************************************************* 
//CREA010  EXEC PGM=NATB030,REGION=&REGN1,                              
//   PARM='SYS=&NAT,UDB=&DBID,OBJIN=N,WH=ON'                            
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR             
//         DD DSN=&LIB1..PARMLIB(NI1171D1),DISP=SHR                     
//         DD DSN=&LIB1..PARMLIB(&PARMMEM2),DISP=SHR                    
//         DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                      
//DDCARD   DD DSN=&LIB2..PARMLIB(DBAPP&DBID),DISP=SHR                   
//CMWKF01  DD DSN=&HLQ1..&JOBNM..&PKGTYP,DISP=OLD                       
//CMPRINT  DD SYSOUT=&JCLO                                              
//SYSPRINT DD SYSOUT=&JCLO                                              
//SYSOUT   DD SYSOUT=&JCLO                                              
//DDPRINT  DD SYSOUT=&JCLO                                              
//SYSUDUMP DD SYSOUT=&DMPS                                              
