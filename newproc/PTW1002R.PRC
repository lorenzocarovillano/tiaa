//PTW1002R PROC DBID='003',                                             
//         DMPS='U',                                                    
//         JCLO='*',                                                    
//         LIB1='PROD',             PARMLIB                             
//         LIB2='PROD',             NT2LOGON.PARMLIB                    
//         NAT='NAT003P',                                               
//         NATMEM='PANNSEC',                                            
//         REGN1='8M'                                                   
//*                                                                     
//****************************************************************      
//* UPDT010 - Updates TAXWARS Table #4 with 1042-S form Control         
//*           Numbers for the New Tax Year.                             
//*           Tax Year and Control Numbers are entered into PARM        
//*           which is READ by the Natural Program TWRP8162.            
//*---------------------------------------------------------------------
//UPDT010  EXEC PGM=NATB030,REGION=&REGN1,                              
//         PARM='SYS=&NAT'                                              
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//SYSPRINT DD  SYSOUT=&JCLO                                             
//DDPRINT  DD  SYSOUT=&JCLO                                             
//SYSOUT   DD  SYSOUT=&JCLO                                             
//SYSUDUMP DD  SYSOUT=&DMPS                                             
//CMPRINT  DD  SYSOUT=&JCLO                                             
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR            
//         DD  DSN=&LIB1..PARMLIB(T1002TW1),DISP=SHR                    
//         DD  DSN=&LIB1..PARMLIB(T1002TW2),DISP=SHR                    
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                     
//* TWRP8162                                                            
