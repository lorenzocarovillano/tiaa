//P1700CWD PROC                                                         
//**********************************************************************
//*          THIS JOB RUNS MIT BATCH ARCHIVING PROCESS                **
//**********************************************************************
//* THIS STEP CREATES CONTROL RECORD FOR "WINDOW" AND READS DB=145 AND *
//* ARCHIVE WORK REQUESTS AND ACTIVITIES TO DB=050                     *
//* PROGRAM MIHP0310 - STARTS ARCHIVING PROCESS                        *
//**********************************************************************
//ARCH0010 EXEC PGM=NATB030,                                            
//             COND=(0,NE),                                             
//             PARM='SYS=NAT045P',                                      
//             REGION=8M                                                
//STEPLIB  DD  DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,                       
//             DISP=SHR                                                 
//DDCARD   DD  DSN=PROD.DN.PARMLIB(DBAPP045),                           
//             DISP=SHR                                                 
//ROCDB045 DD  DUMMY                                                    
//SYSPRINT DD  SYSOUT=*                                                 
//SYSOUT   DD  SYSOUT=*                                                 
//DDPRINT  DD  SYSOUT=*                                                 
//CMPRINT  DD  SYSOUT=*                                                 
//SYSUDUMP DD  SYSOUT=U                                                 
//CMPRT01  DD  SYSOUT=*                                                 
//CMSYNIN  DD  DSN=PROD.NT2LOGON.PARMLIB(PANNSEC),                      
//             DISP=SHR                                                 
//        DD   DSN=PROD.DN.PARMLIB(P1700CWA),                           
//             DISP=SHR                                                 
//**                                                                    
