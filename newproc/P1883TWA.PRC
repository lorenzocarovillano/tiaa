//P1883TWA PROC DBID='003',
//         DMPS='U',
//         JCLO='*',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         HLQ1='PNPD.COR.TAX',
//         GDG1='+1',
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='8M',
//         REPT='8'
//*
//*********************************************************************
//* PROCEDURE:  P1883TWA                                              *
//*                                                                   *
//*                        TAX REPORTING                              *
//*                                                                   *
//* PUERTO RICO CORRECTION REPORTING                                  *
//*                                                                   *
//*********************************************************************
//*                                                                     
//* MODIFICATION HISTORY
//* 11/14/02 - OIA JCL STANDARDS - PHIL STEINHAUSER
//*                                                                     
//*********************************************************************
//*                                                                     
//* EXTR010 FORMAT AND CREATE 480.6 PUERTO RICO CORRECTION REPORT.
//*                                                                   *
//* 003/097  TAX FORM FILE  R/W
//*                                                                   *
//*********************************************************************
//EXTR010 EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//        PARM='SYS=&NAT'
//SYSPRINT  DD SYSOUT=&JCLO
//DDPRINT   DD SYSOUT=&JCLO
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMPRINT   DD SYSOUT=&JCLO
//CMPRT01   DD SYSOUT=&REPT
//CMPRT02   DD SYSOUT=&REPT
//CMPRT03   DD SYSOUT=&REPT
//CMPRT04   DD SYSOUT=&REPT
//CMPRT05   DD SYSOUT=&REPT
//CMPRT06   DD SYSOUT=&REPT
//CMPRT07   DD SYSOUT=&REPT
//CMPRT08   DD SYSOUT=&REPT
//CMPRT09   DD SYSOUT=&REPT
//CMPRT10   DD SYSOUT=&REPT
//CMPRT11   DD SYSOUT=&REPT
//CMPRT12   DD SYSOUT=&REPT
//CMWKF01  DD DSN=&HLQ1..P1883TWA.IRS4807C.ORG(&GDG1),
//            DISP=(NEW,CATLG,DELETE),
//            SPACE=(TRK,(1,1),RLSE),UNIT=SYSDA,
//            DCB=(LRECL=2500,RECFM=FB)
//CMWKF02  DD DSN=&HLQ1..P1883TWA.IRS4807C.COR(&GDG1),
//            DISP=(NEW,CATLG,DELETE),
//            SPACE=(TRK,(1,1),RLSE),UNIT=SYSDA,
//            DCB=(LRECL=2500,RECFM=FB)
//SYSUDUMP  DD SYSOUT=&DMPS
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(P1883TW1),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//* TWRP3530
