//P1010NID PROC DBID=003,
//         DMPS='U',
//         JCLO='*',
//         LIB1='PROD',
//         LIB2='PROD',
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='5M',
//         SPC1='5,5',
//         GDG1='+1',
//         GDG0='+0',
//         HLQ1='PNA.ANN',
//         REPT=8,
//         REPTID1='NI1010D1',
//         REPTID2='NI1010D2'
//*********************************************************************
//* THIS PROGRAM WRITES DELETED CONTRACTS FROM PRAP TO THE ACIS       *
//* REPRINT FILE.                                                     *
//*                                                                   *
//*********************************************************************
//* PROCEDURE:  P1010NID                 DATE:    06/26/96            *
//*                                                                   *
//* CHANGED 5/21/2010 - DEVELBISS - REMOVE OBSOLETE STEPS             *
//*********************************************************************
//* PROGRAMS EXECUTED: APPB1080 - COPY DELETED PRAP RECS. TO REPRINTFL*
//*********************************************************************
//UPDT040  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//   PARM='SYS=&NAT'
//SORTWK01 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE)
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSPRINT DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMPRINT  DD  SYSOUT=&JCLO
//CMWKF01  DD  DSN=&HLQ1..ACIS.DELETE.CNTRLFLE(&GDG0),DISP=SHR
//CMWKF02  DD  DSN=&HLQ1..ACIS.DELETE.CNTRLFLE(&GDG1),
//            DISP=(NEW,CATLG,DELETE),
//            UNIT=SYSDA,
//            DATACLAS=DCPSEXTC,
//            RECFM=FB,LRECL=50,
//            SPACE=(CYL,(&SPC1),RLSE)
//CMPRT01  DD  SYSOUT=(&REPT,&REPTID1)
//CMPRT02  DD  SYSOUT=(&REPT,&REPTID2)
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P1010NI4),DISP=SHR
