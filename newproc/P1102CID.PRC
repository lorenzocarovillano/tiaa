//P1102CID PROC
//ALOC010 EXEC PGM=IEBGENER
//SYSPRINT DD SYSOUT=*
//SYSIN    DD DUMMY
//SYSUT1   DD DSN=PNA.ANN.CIS.COND.RULE.S642,DISP=SHR
//SYSUT2   DD  DSN=TMIGR.ANN.CIS.COND.RULE,
//             DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,
//             LRECL=700,RECFM=FB,BLKSIZE=21000,
//             SPACE=(CYL,(5,2))
