//PCW4140R PROC DBID=045,
//       NATVERS='NATB030',
//       REGN1='8M',
//       NAT='NAT003P',                                                 
//       DMPS='U',
//       JCLO='*',
//       LIB1='PROD',
//       NATMEM='PANNSEC',                                              
//       REPT='8'
//*-------------------------------------------------------------------*
//* On-demand proc to close MIT work requests that have been open in
//* the target units for more than 90 days. Target units and WPID's
//* are identified on a CWF support table. Another table gives the
//* date range of the requests to be closed, and a read limit.
//* A report lists the PIN, Log Date/Time and Received Date/Time of
//* the closed requests.
//* ----------------------------------------------------------------- *
//UPDT010  EXEC PGM=&NATVERS,REGION=&REGN1,
//            PARM='SYS=&NAT'
//UCC11NR  DD DUMMY
//SYSUDUMP DD SYSOUT=&DMPS
//SYSPRINT DD SYSOUT=&JCLO
//DDPRINT  DD SYSOUT=&JCLO
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMPRINT  DD SYSOUT=&JCLO
//CMPRT01  DD SYSOUT=(&REPT,CW4140R1)
//CMSYNIN  DD DSN=&LIB1..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(CW4140RA),DISP=SHR
