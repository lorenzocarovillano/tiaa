//P2350CPM PROC DBID='003',
//         DMPS='U',
//         HLQ0='PIA.ANN',
//         HLQ8='PPDD',
//         JCLO='*',
//         JOBNM='P2350CPM',
//         JOBNM1='P2340CPM',
//         JOBNM2='P2210CPM',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='8M',
//         REPT='8',
//         SPC1='3,20',
//         SPC2='1,1'
//*
//*-------------------------------------------------------------------*
//* PROCEDURE:  P2350CPM                 DATE:  02/29/96              *
//*                                                                     
//* MODIFICATION HISTORY
//* 04/24/02 - OIA JCL STANDARDS - JOHN VIOLA
//*                                                                     
//**********************************************************************
//*-------------------------------------------------------------------*
//* UPDT010 - ADD "AP" LEDGER RECORDS TO ADABAS FILE 198 -            *
//*           "FCP-CONS-LEDGER"                                       *
//*           !!! IMPORTANT: FOR RESTARTABILITY THE NATURAL "ETID"    *
//*           !!! PARAMETER "LGAPUPDT" HAS TO BE SPECIFIED.           *
//*           NATURAL PROGRAM FCPP819                                 *
//*-------------------------------------------------------------------*
//* SORT020 - SORTS THE ANNOTATION FROM "P2210CPM" BY : CONTRACT      *
//*           ORIGIN CODE / CONTRACT PPCN NUMBER / CONTRACT INVERSE   *
//*           DATE / PAYMENT PROCESS SEQUENCE NUMBER.                 *
//* SORT BY :                                                         *
//* CNTRCT-ORGN-CDE     (DESCENDING)  1,2                             *
//* CNTRCT-PPCN-NBR                   3,10                            *
//* CNTRCT-INVRSE-DTE                13,8                             *
//* PYMNT-PRCSS-SEQ-NBR              21,9                             *
//*-------------------------------------------------------------------*
//* UPDT030 - ADD "AP" ANNOTATION RECORDS TO ADABAS FILE 198 -        *
//*           "FCP-CONS-LEDGER"                                       *
//*           !!! IMPORTANT: FOR RESTARTABILITY THE NATURAL "ETID"    *
//*           !!! PARAMETER "ANAPUPDT" HAS TO BE SPECIFIED.           *
//*           NATURAL PROGRAM FCPP828                                 *
//*-------------------------------------------------------------------*
//*
//*-------------------------------------------------------------------*
//* UPDT010 - ADD "AP" LEDGER RECORDS TO ADABAS FILE 198 -            *
//*           "FCP-CONS-LEDGER"                                       *
//*           !!! IMPORTANT: FOR RESTARTABILITY THE NATURAL "ETID"    *
//*           !!! PARAMETER "LGAPUPDT" HAS TO BE SPECIFIED.           *
//*           NATURAL PROGRAM FCPP819                                 *
//*-------------------------------------------------------------------*
//UPDT010 EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//        PARM='IM=D,SYS=&NAT,ETID=LGAPUPDT'
//SYSPRINT DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//CMPRT15  DD  SYSOUT=&REPT
//CMWKF01  DD  DSN=&HLQ0..&JOBNM1..AP.LEDGER.SORT.S479,DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P2350CP1),DISP=SHR
//* FCPP819
//*-------------------------------------------------------------------*
//* SORT020 - SORTS THE ANNOTATION FROM "P2210CPM" BY : CONTRACT      *
//*           ORIGIN CODE / CONTRACT PPCN NUMBER / CONTRACT INVERSE   *
//*           DATE / PAYMENT PROCESS SEQUENCE NUMBER.                 *
//* SORT BY :                                                         *
//* CNTRCT-ORGN-CDE     (DESCENDING)  1,2                             *
//* CNTRCT-PPCN-NBR                   3,10                            *
//* CNTRCT-INVRSE-DTE                13,8                             *
//* PYMNT-PRCSS-SEQ-NBR              21,9                             *
//*-------------------------------------------------------------------*
//SORT020  EXEC PGM=SORT,COND=(0,NE)
//SYSOUT    DD SYSOUT=&JCLO
//SYSUDUMP  DD SYSOUT=&DMPS
//SORTWK01  DD UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE,,ROUND),BUFNO=15
//SORTWK02  DD UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE,,ROUND),BUFNO=15
//SORTWK03  DD UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE,,ROUND),BUFNO=15
//SYSPRINT  DD SYSOUT=&JCLO
//SORTIN    DD DSN=&HLQ0..&JOBNM2..ANNOT.S468,DISP=SHR,BUFNO=23
//SORTOUT   DD DSN=&HLQ8..&JOBNM..ANNOT.SORT,
//             SPACE=(CYL,(&SPC2),RLSE),DISP=(NEW,CATLG,DELETE),
//             UNIT=SYSDA,DCB=(RECFM=FB,LRECL=36,BUFNO=25)
//SYSIN     DD DSN=&LIB1..PARMLIB(P2350CP2),DISP=SHR
//* SORT FIELDS=(1,2,CH,D,3,27,CH,A)
//* SORT BY :
//* CNTRCT-ORGN-CDE     (DESCENDING)  1,2
//* CNTRCT-PPCN-NBR                   3,10
//* CNTRCT-INVRSE-DTE                13,8
//* PYMNT-PRCSS-SEQ-NBR              21,9
//*-------------------------------------------------------------------*
//* UPDT030 - ADD "AP" ANNOTATION RECORDS TO ADABAS FILE 198 -        *
//*           "FCP-CONS-LEDGER"                                       *
//*           !!! IMPORTANT: FOR RESTARTABILITY THE NATURAL "ETID"    *
//*           !!! PARAMETER "ANAPUPDT" HAS TO BE SPECIFIED.           *
//*           NATURAL PROGRAM FCPP828                                 *
//*-------------------------------------------------------------------*
//UPDT030 EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//        PARM='IM=D,SYS=&NAT,ETID=ANAPUPDT'
//SYSPRINT DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT  DD SYSOUT=&JCLO
//CMPRINT  DD SYSOUT=&JCLO
//CMPRT15  DD SYSOUT=&REPT
//CMWKF01  DD DSN=&HLQ8..&JOBNM..ANNOT.SORT,DISP=SHR,BUFNO=25
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(P2350CP3),DISP=SHR
//* FCPP828
