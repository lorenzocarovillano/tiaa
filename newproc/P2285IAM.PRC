//P2285IAM PROC DBID='003',
//         DMPS='U',
//         GDG0='+0',
//         HLQ0='PIA.ANN',
//         JCLO='*',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='5M',
//         REPT='8'
//*                                                                     
//**********************************************************************
//*                                                                     
//* MODIFICATION HISTORY
//* 04/11/02 - OIA JCL STANDARDS - PHIL STEINHAUSER
//*                                                                     
//**********************************************************************
//*
//REPT010 EXEC PGM=NATB030,REGION=&REGN1,
//    PARM='SYS=&NAT,MADIO=0,MT=0,INTENS=1'
//SYSUDUMP DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMWKF01  DD DSN=&HLQ0..IATRANS.FINAL(&GDG0),DISP=SHR
//SYSOUT   DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//SYSABOUT DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSDBOUT DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//CMPRINT  DD SYSOUT=(&REPT,IA2285M1)
//CMPRT    DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRT01  DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRT02  DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR                              
//CMPRT03  DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR                              
//CMPRT04  DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR                              
//CMPRT05  DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR                              
//SYSPRINT DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//DDPRINT  DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P2285IA1),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
