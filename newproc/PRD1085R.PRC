//PRD1085R  PROC REGN1='8M',
//          PARMMEM='XXXXXXXX',
//          DATA='XXXXX',
//          NATLOGON='PANNSEC',
//          NAT='NATB030',
//          SYS='NAT045P',
//          DBID=045,
//          LIB1='PROD',
//          HLQ1='PDA.ANN',
//          HLQ8='PPT.COR',
//          SPC1=50,
//          SPC2=10,
//          JCLO=*,
//          DMPS=U,
//          REPT=8
//********************************************************************
//*        INITIALIZE DATASETS TO BE USED
//********************************************************************
//DELT010  EXEC PGM=IEFBR14
//SYSPRINT DD SYSOUT=*
//D1       DD DSN=&HLQ8..RIDER.CREFRA.FILE1,
//            UNIT=SYSDA,
//            DISP=(MOD,DELETE,DELETE),
//            SPACE=(CYL,(0,0))
//D2       DD DSN=&HLQ8..RIDER.CREFRA.FILE2,
//            UNIT=SYSDA,
//            DISP=(MOD,DELETE,DELETE),
//            SPACE=(CYL,(0,0))
//*********************************************************************
//*        RIDP500 - READ DA REPORTING ACCESS EXTRACT
//*********************************************************************
//EXTR020  EXEC PGM=&NAT,COND=(0,NE),REGION=&REGN1,
//            PARM='SYS=&SYS,UDB=&DBID,WH=ON'
//SYSPRINT DD  SYSOUT=&JCLO
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//CMPRT01  DD  SYSOUT=&REPT
//CMPRT02  DD  SYSOUT=&REPT
//CMPRT03  DD  SYSOUT=&REPT
//CMWKF01  DD  DSN=&HLQ1..RIDER.CREFRA.&DATA,DISP=SHR
//CMWKF02  DD  DSN=&HLQ8..RIDER.CREFRA.FILE1,
//             DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//             DCB=(RECFM=FB,LRECL=500)
//CMWKF03  DD  DSN=&HLQ8..RIDER.CREFRA.FILE2,
//             DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//             DCB=(RECFM=FB,LRECL=500)
//SYSUDUMP DD  SYSOUT=&DMPS
//SYSOUT   DD  SYSOUT=&JCLO
//CMSYNIN  DD  DSN=&LIB1..NT2LOGON.PARMLIB(&NATLOGON),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(&PARMMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
