//PNI7900R PROC DBID=003,
//         DMPS='U',
//         JCLO='*',
//         LIB1='PROD',
//         LIB2='PROD',
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='5M',
//         HLQ1='PNA.ANN',
//         REPT='8'
//*********************************************************************
//* PROCEDURE:  PNI7900R                 DATE:    01/16/2013          *
//*                                                                   *
//*********************************************************************
//* PROGRAMS EXECUTED: APPB1090 - LOGICALLY DELETE PRAP RECS.         *
//*********************************************************************
//UPDT010  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//   PARM='SYS=&NAT'
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSPRINT DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMPRINT  DD  SYSOUT=&JCLO
//CMPRT01  DD SYSOUT=(&REPT,NI7900R1)
//CMPRT02  DD SYSOUT=(&REPT,NI7900R1)
//CMPRT03  DD SYSOUT=(&REPT,NI7900R1)
//CMWKF01  DD  DSN=&HLQ1..PNI7900R.INPUT,DISP=SHR
//CMWKF02  DD  DSN=&HLQ1..PNI9700R.DELETE.CODE,DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(NI7900R1),DISP=SHR
