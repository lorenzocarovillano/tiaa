//P1065TWD PROC DBID='003',                                             
//         DMPS='U',                                                    
//         DWF='FB',                DW RECFM                            
//         DWL='325',               DW LRECL                            
//         GDG1='+1',                                                   
//         HLQ0='PNPD.COR.TAX',                                         
//         HLQ8='PPDD.TAX',                                             
//         JCLO='*',                                                    
//         JOBNM1=,                                                     
//         JOBNM2='P1000TWD',                                           
//         LIB1='PROD',             PARMLIB                             
//         LIB2='PROD',             NT2LOGON.PARMLIB                    
//         NAT='NAT003P',                                               
//         NATMEM='PANNSEC',                                            
//         REGN1='8M',                                                  
//         REPT='8',                                                    
//         SPC1='100,100',                                              
//         SPC2='1',                                                    
//         SPC3='50,50',                                                
//         SPC4='250,250',                                              
//         SPC5='400,600',                                              
//         TAXYEAR=''                                                   
//*                                                                     
//*-------------------------------------------------------------------* 
//*           IRA-CONTRIBUTION RECORDS BY TAX YEAR                    * 
//*-------------------------------------------------------------------* 
//*                                                                     
//********************************************************************* 
//* CREA005  - CREATES PARM FOR RECONCILIATION REPORTS                  
//*            NATURAL PROGRAM "TWRP5500".                              
//********************************************************************* 
//CREA005  EXEC PGM=NATB030,REGION=&REGN1,                              
//            PARM='SYS=&NAT'                                           
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                   
//SYSOUT   DD SYSOUT=&JCLO                                              
//SYSUDUMP DD SYSOUT=&DMPS                                              
//SYSPRINT DD SYSOUT=&JCLO                                              
//DDPRINT  DD SYSOUT=&JCLO                                              
//CMPRINT  DD SYSOUT=&JCLO                                              
//CMPRT01  DD SYSOUT=&REPT                                              
//CMWKF01  DD DSN=&LIB1..PARMLIB(&TAXYEAR),DISP=SHR                     
//CMWKF02  DD DSN=&HLQ0..&JOBNM1..CREA005.CTRL.YTD,    DW YTD           
//            DISP=SHR                                                  
//CMWKF03  DD DSN=&HLQ0..&JOBNM1..CREA005.CTRL.DLY,    DW DAILY         
//            DISP=SHR                                                  
//CMWKF04  DD DSN=&HLQ0..&JOBNM2..CONTROL.EXT,DISP=SHR                  
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR             
//         DD DSN=&LIB1..PARMLIB(P1065TW6),DISP=SHR                     
//         DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                      
//* TWRP5500                                                            
//********************************************************************* 
//* EXTR010 - CREATES FLAT-FILE1 CONTAINING EXTRACTED IRA-CONTRIBUTION  
//*           RECORDS BY TAX YEAR                                       
//********************************************************************* 
//EXTR010  EXEC PGM=NATB030,REGION=&REGN1,                              
//            PARM='SYS=&NAT'                                           
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                   
//SYSOUT   DD SYSOUT=&JCLO                                              
//SYSPRINT DD SYSOUT=&JCLO                                              
//DDPRINT  DD SYSOUT=&JCLO                                              
//CMPRINT  DD SYSOUT=&JCLO                                              
//CMPRT01  DD SYSOUT=&REPT                                              
//CMWKF01  DD DSN=&HLQ0..&JOBNM1..CREA005.CTRL.DLY,DISP=SHR             
//CMWKF02  DD DSN=&HLQ8..&JOBNM1..EXTR010.FF1,                          
//           DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,                    
//*          DCB=(LRECL=97,RECFM=FB),                                   
//           DCB=(LRECL=107,RECFM=FB),                                  
//           SPACE=(CYL,(&SPC1),RLSE),                                  
//           UNIT=SYSDA                                                 
//CMWKF03  DD DSN=&HLQ8..&JOBNM1..EXTR010.FF2,                          
//           DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,                    
//           DCB=(LRECL=327,RECFM=FB),                                  
//           SPACE=(CYL,&SPC2,RLSE),UNIT=SYSDA                          
//CMWKF04  DD DSN=&HLQ0..&JOBNM1..EXTR010.DWD(&GDG1),  DW DAILY         
//            DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,                   
//            DCB=(LRECL=&DWL,RECFM=&DWF),                              
//            SPACE=(CYL,(&SPC4),RLSE),UNIT=SYSDA                       
//CMWKF05  DD DSN=&HLQ0..&JOBNM1..EXTR010.DWY(&GDG1),  DW YTD           
//            DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,                   
//            DCB=(LRECL=&DWL,RECFM=&DWF),                              
//            SPACE=(CYL,(&SPC5),RLSE),UNIT=SYSDA                       
//SYSUDUMP DD SYSOUT=&DMPS                                              
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR             
//         DD DSN=&LIB1..PARMLIB(P1065TW1),DISP=SHR                     
//         DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                      
//* TWRP1900                                                            
//*******************************************************************   
//* SORT020 - SORTS EXTRACTED CONTRIBUTION RECORDS BY COMPANY,          
//*           ORIG/UPD SOURCE CODE AND STATUS                           
//*******************************************************************   
//SORT020  EXEC PGM=SORT,REGION=&REGN1,COND=(0,NE)                      
//SYSOUT   DD SYSOUT=&JCLO                                              
//SYSUDUMP DD SYSOUT=&DMPS                                              
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,(&SPC3),RLSE)                       
//SORTWK02 DD UNIT=SYSDA,SPACE=(CYL,(&SPC3),RLSE)                       
//SORTWK03 DD UNIT=SYSDA,SPACE=(CYL,(&SPC3),RLSE)                       
//SORTWK04 DD UNIT=SYSDA,SPACE=(CYL,(&SPC3),RLSE)                       
//SORTIN   DD DSN=&HLQ8..&JOBNM1..EXTR010.FF1,                          
//            DISP=SHR                                                  
//SORTOUT  DD DSN=&HLQ8..&JOBNM1..SORT020.FF1.SRTED,                    
//            DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,                   
//            DCB=(LRECL=97,RECFM=FB),                                  
//            SPACE=(CYL,(&SPC1),RLSE),                                 
//            UNIT=SYSDA                                                
//SYSIN    DD DSN=&LIB1..PARMLIB(P1065TW2),DISP=SHR                     
//********************************************************************* 
//* PRNT030 - PRINTS IRA5498 CONTRIBUTION FILE   (DAILY REPORT)         
//********************************************************************* 
//PRNT030  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),                  
//            PARM='SYS=&NAT'                                           
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                   
//SYSPRINT DD SYSOUT=&JCLO                                              
//SYSOUT   DD SYSOUT=&JCLO                                              
//DDPRINT  DD SYSOUT=&JCLO                                              
//CMPRINT  DD SYSOUT=&JCLO                                              
//CMPRT01  DD SYSOUT=&REPT                                              
//*CMWKF01  DD DSN=&HLQ8..&JOBNM1..EXTR010.FF2,DISP=SHR                 
//CMWKF01  DD DSN=&HLQ0..&JOBNM1..CREA005.CTRL.DLY,                     
//            DISP=SHR                                                  
//CMWKF02  DD DSN=&HLQ8..&JOBNM1..SORT020.FF1.SRTED,DISP=SHR            
//SYSUDUMP DD SYSOUT=&DMPS                                              
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR             
//         DD DSN=&LIB1..PARMLIB(P1065TW3),DISP=SHR                     
//         DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                      
//*******************************************************************   
//* PRNT040 - PRINTS IRA5498 DAILY ONLINE MAINTENANCE DETAIL REPORT     
//*******************************************************************   
//PRNT040  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),                  
//            PARM='SYS=&NAT'                                           
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                   
//SYSPRINT DD SYSOUT=&JCLO                                              
//SYSOUT   DD SYSOUT=&JCLO                                              
//DDPRINT  DD SYSOUT=&JCLO                                              
//CMPRINT  DD SYSOUT=&JCLO                                              
//CMPRT01  DD SYSOUT=&REPT                                              
//*CMWKF01  DD DSN=&HLQ8..&JOBNM1..EXTR010.FF2,DISP=SHR                 
//CMWKF01  DD DSN=&HLQ0..&JOBNM1..CREA005.CTRL.DLY,                     
//            DISP=SHR                                                  
//CMWKF02  DD DSN=&HLQ8..&JOBNM1..SORT020.FF1.SRTED,DISP=SHR            
//SYSUDUMP DD SYSOUT=&DMPS                                              
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR             
//         DD DSN=&LIB1..PARMLIB(P1065TW4),DISP=SHR                     
//         DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                      
//****************************************************************      
//* PRNT050 - PRINTS IRA5498 CONTRIBUTION FILE   (YTD REPORT)           
//****************************************************************      
//PRNT050  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),                  
//            PARM='SYS=&NAT'                                           
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                   
//SYSPRINT DD SYSOUT=&JCLO                                              
//SYSOUT   DD SYSOUT=&JCLO                                              
//DDPRINT  DD SYSOUT=&JCLO                                              
//CMPRINT  DD SYSOUT=&JCLO                                              
//CMPRT01  DD SYSOUT=&REPT                                              
//CMPRT02  DD SYSOUT=&REPT                                              
//*CMWKF01  DD DSN=&HLQ8..&JOBNM1..EXTR010.FF2,                         
//CMWKF01  DD DSN=&HLQ0..&JOBNM1..CREA005.CTRL.DLY,                     
//            DISP=SHR                                                  
//CMWKF02  DD DSN=&HLQ8..&JOBNM1..SORT020.FF1.SRTED,                    
//            DISP=SHR                                                  
//SYSUDUMP DD SYSOUT=&DMPS                                              
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR             
//         DD DSN=&LIB1..PARMLIB(P1065TW5),DISP=SHR                     
//         DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                      
