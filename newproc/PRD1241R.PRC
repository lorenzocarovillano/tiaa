//PRD1241R PROC REGN1='8M',                                             
//         PARMMEM='XXXXXXXX',                                          
//         PARMMEM2='XXXXXXXX',                                         
//         NATLOGON='PANNSEC',                                          
//         NAT='NATB030',                                               
//         SYS='NAT045P',                                               
//         DBID='045',                                                  
//         LIB1='PROD',                                                 
//         HLQ8='PPT.COR',                                              
//         SPC1=500,                                                    
//         SPC2=10,                                                     
//         JCLO=*,                                                      
//         DMPS=U,                                                      
//         REPT=8                                                       
//* ******************************************************************* 
//VERY001  EXEC PGM=IDCAMS,COND=(3,LE)                                  
//SYSPRINT DD  SYSOUT=&JCLO                                             
//DD1      DD  DSN=&HLQ8..RIDER.DOMA.&FILE,DISP=SHR                     
//SYSIN    DD  DSN=&LIB1..PARMLIB(CHECKRCD),DISP=SHR                    
//*                                                                     
//* ******************************************************************* 
//* ******************************************************************* 
//*   PROCEED WITH THE NEXT STEPS ONLY WHEN THERE ARE REQUEST EXTRACTS  
//* ******************************************************************* 
//* ******************************************************************* 
//*                                                                     
// IF (VERY001.RC NE 1) THEN                                            
//*****************************************************************     
//*   DEALLOCATE WORK FILE                                              
//*****************************************************************     
//DELT005  EXEC PGM=IEFBR14                                             
//SYSPRINT DD SYSOUT=&JCLO                                              
//D1       DD DSN=&HLQ8..RIDER.DOMA.POST.DATA,                          
//            UNIT=SYSDA,                                               
//            DISP=(MOD,DELETE,DELETE),                                 
//            SPACE=(CYL,(0,0))                                         
//D2       DD DSN=&HLQ8..&JOBNM..ECSFILE,                               
//            UNIT=SYSDA,                                               
//            DISP=(MOD,DELETE,DELETE),                                 
//            SPACE=(CYL,(0,0))                                         
//D3       DD DSN=&HLQ8..&JOBNM..CREA010,                               
//            UNIT=SYSDA,                                               
//            DISP=(MOD,DELETE,DELETE),                                 
//            SPACE=(CYL,(0,0))                                         
//********************************************************************* 
//*   RIDP562 GET POST DATA - NAME AND ADDRESSES (ALSO READS COR)       
//********************************************************************* 
//EXTR010  EXEC PGM=&NAT,COND=(0,NE),REGION=&REGN1,                     
//            PARM='SYS=&SYS,UDB=&DBID,WH=ON'                           
//SYSPRINT DD  SYSOUT=&JCLO                                             
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//DDPRINT  DD  SYSOUT=&JCLO                                             
//CMPRINT  DD  SYSOUT=&JCLO                                             
//SYSUDUMP DD  SYSOUT=&DMPS                                             
//SYSOUT   DD  SYSOUT=&JCLO                                             
//CMPRT01  DD  SYSOUT=&REPT                                             
//*                                                                     
//CMWKF01  DD  DSN=&HLQ8..RIDER.DOMA.&FILE,                             
//             DISP=SHR                                                 
//CMWKF02  DD  DSN=&HLQ8..RIDER.DOMA.POST.DATA,                         
//             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,               
//             UNIT=SYSDA,                                              
//             SPACE=(CYL,(&SPC1,&SPC2),RLSE),                          
//             DCB=(RECFM=FB,LRECL=900)                                 
//CMSYNIN  DD  DSN=&LIB1..NT2LOGON.PARMLIB(&NATLOGON),DISP=SHR          
//         DD  DSN=&LIB1..PARMLIB(RD1241R3),DISP=SHR                    
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                     
//********************************************************************* 
//*  RIDB404 - CREATES FILE(S) CONTAINING COMPUSET DATA                 
//********************************************************************* 
//EXTR020  EXEC PGM=&NAT,COND=(0,NE),REGION=&REGN1,                     
//            PARM='SYS=&SYS,UDB=&DBID,WH=ON'                           
//SYSPRINT DD  SYSOUT=&JCLO                                             
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//DDPRINT  DD  SYSOUT=&JCLO                                             
//CMPRINT  DD  SYSOUT=&JCLO                                             
//SYSUDUMP DD  SYSOUT=&DMPS                                             
//SYSOUT   DD  SYSOUT=&JCLO                                             
//CMPRT01  DD  SYSOUT=&REPT                                             
//CMPRT02  DD  SYSOUT=&REPT                                             
//CMWKF01  DD  DSN=&HLQ8..RIDER.DOMA.POST.DATA,DISP=SHR                 
//CMWKF02  DD  DSN=&HLQ8..&JOBNM..ECSFILE,                              
//             UNIT=SYSDA,                                              
//             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,               
//             SPACE=(CYL,(&SPC1,&SPC2),RLSE),                          
//             DCB=(DSORG=PS,RECFM=FB,LRECL=800)                        
//*                                                                     
//********************************************************************* 
//*        PARMLIB MEMBERS MUST BE IN THIS ORDER. DO NOT CHANGE !!!!!!! 
//********************************************************************* 
//CMSYNIN  DD  DSN=&LIB1..NT2LOGON.PARMLIB(&NATLOGON),DISP=SHR          
//         DD  DSN=&LIB1..PARMLIB(&PARMMEM2),DISP=SHR                   
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                     
//*                                                                     
//********************************************************************* 
//*    PSTB9556 - EXTRACT E-MAIL DATA / CREATE ORACLE CONTROL RECORD    
//*               CREATE DIRECT:CONNECT CONTROL CARDS                   
//********************************************************************* 
//CREA060  EXEC PGM=&NAT,REGION=&REGN1,                                 
//         PARM='SYS=&SYS,WH=ON'                                        
//SYSPRINT DD  SYSOUT=&JCLO                                             
//SYSOUT   DD  SYSOUT=&JCLO                                             
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//DDPRINT  DD  SYSOUT=&JCLO                                             
//CMPRINT  DD  SYSOUT=&JCLO                                             
//SYSUDUMP DD  SYSOUT=&DMPS                                             
//CMWKF01  DD  DSN=&HLQ8..&JOBNM..CREA010,                              
//             UNIT=SYSDA,                                              
//             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,               
//             SPACE=(CYL,(1,1),RLSE),                                  
//             DCB=(DSORG=PS,RECFM=F,LRECL=120)                         
//CMSYNIN  DD  DSN=&LIB1..NT2LOGON.PARMLIB(&NATLOGON),DISP=SHR          
//         DD  DSN=&LIB1..PARMLIB(PT2000D3),DISP=SHR                    
//         DD  DSN=&LIB1..PARMLIB(&PARMMEM),DISP=SHR                    
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                     
//*                                                                     
// ENDIF                                                                
