//PPTCOPYD PROC APPNM='XXXXXXXX',                                       
//         HLQ8=PPT.COR,                                                
//         DMPS='U',                                                    
//         JCLO='*',                                                    
//         SPC3='10,10'                                                 
//*                                                                     
//*---------------------------------------------------------------
//BACKUP   EXEC PGM=IEBGENER,REGION=0M                                  
//*                                                                     
//SYSUT1   DD  DSN=&HLQ8..&APPNM..XMLOUT.FILE,DISP=SHR                  
//*                                                                     
//SYSUT2   DD  DSN=&HLQ8..&APPNM..XMLOUT.BACKUP(+1),                    
//             DISP=(NEW,CATLG,DELETE),                                 
//             UNIT=SYSDA,DATACLAS=DCPSEXTC,                            
//             SPACE=(CYL,(&SPC3),RLSE)                                 
//*                                                                     
//SYSIN    DD  *                                                        
//SYSOUT   DD  SYSOUT=&JCLO                                             
//SYSUDUMP DD  SYSOUT=&DMPS                                             
//SYSPRINT DD  SYSOUT=&JCLO                                             
//*                                                                     
