//PMO0500D PROC DBID='008',
//         DMPS='U',
//         HLQ0='PPM.ANN.MAYO',    CORPORATE DASD
//         HLQ1='POMPL',           OMNIPLUS BASEVENT FILE
//         JOBNM1='PMO0440D',      RECREATION OF FIDELITY UPLOAD
//         JOBNM2='PMO0500D',
//         JCLO='*',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         LIB3='PROD',             FOR TESTING CRS
//         NAT='NAT008P',
//         NATMEM='PANNSEC',
//         REGN1='0M',
//         FSET='FM',
//         SEG='01',
//         REPT='8'
//*
//*
//*===================================================================*
//*
//*            PROCESS OMNIPLUS OCFILE
//*
//*-------------------------------------------------------------------*
//*
//* PROCEDURE: PMO0500D
//*
//* PROGRAMS:  SMYP500
//*
//* FUNCTION:
//*
//* PROCESSING FOR FIDELITY'S MAYO CLINIC DATA                        *
//* INPUT IS THE OMNIPLUS OCFILE CREATED BY T301 END                  *
//*                                                                   *
//*   FILE FIDELITY-CONTROL-FILE 008/046                              *
//*                                                                   *
//* PROGRAM UPDATES THE LEDGER INDICATOR ON THE FIDELITY-             *
//* TRANSACTION FILE                                                  *
//*                                                                   *
//*********************************************************************
//* DELETE  EXECUTES THE PROGRAM IEFBR14                              *
//*********************************************************************
//*                                                                   *
//DELETE  EXEC PGM=IEFBR14
//*
//SYSOUT  DD SYSOUT=*
//*
//DD1     DD DSN=&HLQ0..&JOBNM2..UPDT010.TOTALS,
//        DISP=(MOD,DELETE),UNIT=SYSDA,SPACE=(TRK,0)
//*
//*********************************************************************
//* STEP001 EXECUTES THE PROGRAM SMYP500                              *
//*********************************************************************
//* RESTART:
//*
//*  THIS IS AN UPDATE STEP. A FAILURE MAY BE RESTARTED FROM THIS
//*  FAILING STEP.
//*
//**********************************************************************
//*                 FIDELITY FILE -                                   *
//*-------------------------------------------------------------------*
//* 04/25/2014 - B. NEWSOM - ADDED NEW WORKFILE 4 CONTAINING FUNDS THAT
//*                          WILL BE LOADED TO AN ARRAY AND USED FOR
//*                          TRANSLATION. (CREA)
//**********************************************************************
//*
//UPDT010 EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//        PARM='SYS=&NAT,OPRB=(.ALL)'
//SYSOUT   DD SYSOUT=&JCLO
//SYSPRINT DD SYSOUT=&JCLO
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT  DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//CMPRINT  DD SYSOUT=&JCLO
//CMPRT01  DD SYSOUT=(&REPT,MO0500D1)
//CMPRT02  DD SYSOUT=(&REPT,MO0500D2)
//CMPRT03  DD SYSOUT=(&REPT,MO0500D3)
//CMPRT04  DD SYSOUT=(&REPT,MO0500D4)
//CMPRT05  DD SYSOUT=(&REPT,MO0500D5)
//CMWKF01  DD DSN=&HLQ0..&JOBNM1..REPT040.UPLOAD,                       
//         DISP=SHR                                                     
//CMWKF02  DD DSN=&HLQ1..&FSET..SEG&SEG..BASEVENT(0),                   
//         DISP=SHR                                                     
//CMWKF03  DD DSN=&HLQ0..&JOBNM2..UPDT010.TOTALS,
//            DISP=(,CATLG,DELETE),
//            SPACE=(TRK,(1,1),RLSE),
//            UNIT=SYSDA,
//*//            DCB=RECFM=FB
//            DCB=(RECFM=FB)
//CMWKF04  DD DSN=&HLQ0..FUND.TRANSLTE.FILE,DISP=SHR
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB3..PARMLIB(MO0500D1),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//*********************************************************************
//*      AUTO EMAIL FUNCTION IF ERROR REPORTS NEED ATTENTION
//*********************************************************************
//*
//* RESTART:
//* EMAIL STEP. IF STEP FAILS, MAY RESTART AT THIS 'JOBSTEP.PROCSTEP'
//*                                                                   *
//*********************************************************************
//ERR1FILE  IF (UPDT010.RC EQ 1 OR UPDT010.RC EQ 3) THEN
//EMAL020  EXEC  PGM=CAJUCMD0
//SYSPRINT DD  SYSOUT=*
//$$SCD1   DD  DUMMY        >>>> PROD <<<<
//SYSIN    DD  DSN=&LIB1..PARMLIB(MO0500D2),DISP=SHR
//       ENDIF       >>>> END ERR1FILE <<<<
//*********************************************************************
//*
//* RESTART:
//* EMAIL STEP. IF STEP FAILS, MAY RESTART AT THIS 'JOBSTEP.PROCSTEP'
//*                                                                   *
//*********************************************************************
//ERR2FILE  IF (UPDT010.RC EQ 2 OR UPDT010.RC EQ 3) THEN
//EMAL030  EXEC  PGM=CAJUCMD0
//SYSPRINT DD  SYSOUT=*
//$$SCD1   DD  DUMMY        >>>> PROD <<<<
//SYSIN    DD  DSN=&LIB1..PARMLIB(MO0500D3),DISP=SHR
//       ENDIF       >>>> END ERR2FILE <<<<
