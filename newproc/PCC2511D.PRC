//*********************************************************************
//*        PCC2511D  -   PROC FOR GENERATING XML - CIS PACKAGES
//*********************************************************************
//PCC2511D PROC APPNM='XXXXXXXX',
//         LIB1=PROD,
//         HLQD='PROD',                                                 
//         HLQ8=PPT.COR,
//         SPC3='10,10',
//         JCLO=*,
//         PACKAGE='XXXXXXXX',
//         FILETYP='XXX.XXXXXXXX',
//         PROFNUM='PROFIL01'
//*
//*
//*********************************************************************
//*        DEALLOCATE WORK DATASETS FOR XMLFILE BUILD
//*********************************************************************
//DEALLOC2 EXEC PGM=IEFBR14
//SYSPRINT DD  SYSOUT=&JCLO
//TMPFL01  DD  DSN=&HLQ8..&APPNM..TRGFILE,
//             UNIT=SYSDA,SPACE=(0,(0)),DISP=(MOD,DELETE)
//TMPFL02  DD  DSN=&HLQ8..&APPNM..SCHEMA.FILE,
//             UNIT=SYSDA,SPACE=(0,(0)),DISP=(MOD,DELETE)
//TMPFL03  DD  DSN=&HLQ8..&APPNM..XMLOUT.FILE,
//             UNIT=SYSDA,SPACE=(0,(0)),DISP=(MOD,DELETE)
//TMPFL04  DD  DSN=&HLQ8..&APPNM..ECSFILE.FB5000,
//             UNIT=SYSDA,SPACE=(0,(0)),DISP=(MOD,DELETE)
//SYSIN    DD  DUMMY
//*
//*********************************************************************
//*        ALLOCATE WORK DATASET - FOR DUMMY TRG FILE
//*********************************************************************
//ALLOCAT2 EXEC PGM=IEFBR14,COND=(0,NE)
//SYSPRINT DD  SYSOUT=&JCLO
//D1       DD  DSN=&HLQ8..&APPNM..TRGFILE,
//             UNIT=SYSDA,
//             DISP=(NEW,CATLG,DELETE),
//             SPACE=(TRK,(1,1),RLSE),
//             DCB=(DSORG=PS,RECFM=FB,LRECL=80)
//SYSIN    DD  DUMMY
//*
//*                                                                     
//PCC0000  EXEC PGM=PCC0000,PARM='&PACKAGE',REGION=0M                   
//PACKAGE  DD  DSN=&LIB1..PARMLIB(POST2DCS),DISP=SHR
//TEMPLATE DD  DSN=&HLQD..DATADICT(MASTER),DISP=SHR
//INF5000  DD  DSN=&HLQ8..&FILETYP..ECSFILE.&PROFNUM,DISP=SHR
//OUTF5000 DD  DSN=&HLQ8..&APPNM..ECSFILE.FB5000,
//             DISP=(NEW,CATLG,DELETE),
//             SPACE=(CYL,(&SPC3),RLSE),
//             UNIT=SYSDA
//SCHEMA   DD  DSN=&HLQ8..&APPNM..SCHEMA.FILE,
//             DISP=(NEW,CATLG,DELETE),
//             SPACE=(CYL,(&SPC3),RLSE),
//             UNIT=SYSDA
//SYSOUT   DD  SYSOUT=&JCLO                                             
//SYSPRINT DD  SYSOUT=&JCLO                                             
//SYSIN    DD  DUMMY                                                    
//*
//PCC1000  EXEC PGM=PCC1000,PARM='&PACKAGE',REGION=0M                   
//PACKAGE  DD  DSN=&LIB1..PARMLIB(POST2DCS),DISP=SHR
//DICTINRY DD  DSN=&HLQ8..&APPNM..SCHEMA.FILE,DISP=SHR
//INF5000  DD  DSN=&HLQ8..&APPNM..ECSFILE.FB5000,DISP=SHR
//XMLOUT   DD  DSN=&HLQ8..&APPNM..XMLOUT.FILE,
//             DISP=(NEW,CATLG,DELETE),
//             SPACE=(CYL,(&SPC3),RLSE),
//             UNIT=SYSDA
//SYSOUT   DD  SYSOUT=&JCLO                                             
//SYSPRINT DD  SYSOUT=&JCLO                                             
//SYSIN    DD  DUMMY                                                    
//*
//*
