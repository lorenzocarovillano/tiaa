//PMO0480D PROC DBID='008',
//         DMPS='U',
//         HLQ0='PPM.ANN.MAYO',
//         HLQ8='PPDD.ANN.MAYO',
//         JCLO='*',
//         JOBNM='PMO0480D',       CURRENT JOBNAME
//         JOBNM1='PMO0440D',      FIELITY FEED JOBNAME
//         JOBNM2='PMO0477D',      JOBNAME OF PRIOR STEP INPUT
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',
//         LIB3='PROD',             NT2LOGON.PARMLIB
//         NAT='NAT008P',
//         NATMEM='PANNSEC',
//         REPT='8',
//         REGN1='5M',
//         SPC1='20',
//         SPC2='10'
//*
//* ********************************************************************
//* MAINTENANCE:                                                        
//*                                                                     
//*    DATE        WHOM          REASON FOR CHANGE                      
//*    07/30/20   SUSHOBHAN ROY  CHANGED THE NATUTRAL PROGRAM SMYP343   
//*                              TO COBOL MODULE PSG0992 FOR EXTR005    
//*===================================================================*
//*
//*            PROCESS FIDELITY EARNINGS TRANSACTIONS
//*
//*-------------------------------------------------------------------*
//*
//* PROCEDURE: PMO0480D (REPLACES P0470PMD)
//*
//* PROGRAMS:  PSG0992/SMYP344/SMYP352/SMYP408
//*
//* FUNCTION:
//*
//*                                                                   *
//*                                                                   *
//* JOB BUILDS THE MAYO EARNINGS FILE TO BE SENT TO FIDELITY.         *
//*                                                                   *
//**********************************************************************
//* ----------------------------------------------------------------- * 
//* DELETES                                                             
//* ----------------------------------------------------------------- * 
//*                                                                     
//DELETE  EXEC PGM=IEFBR14                                              
//*                                                                     
//SYSOUT  DD SYSOUT=*                                                   
//*                                                                     
//DD1     DD DSN=&HLQ8..&JOBNM..SORT020.EARNOUT,                        
//        DISP=(MOD,DELETE),UNIT=SYSDA,SPACE=(TRK,0)                    
//*                                                                     
//DD2     DD DSN=&HLQ0..&JOBNM..EXTR030.SENDFILE,                       
//        DISP=(MOD,DELETE),UNIT=SYSDA,SPACE=(TRK,0)                    
//*                                                                     
//DD3     DD DSN=&HLQ0..&JOBNM..REPT040.EARNREPT,
//        DISP=(MOD,DELETE),UNIT=SYSDA,SPACE=(TRK,0)
//*                                                                     
//DD4     DD DSN=&HLQ8..&JOBNM..EXTR010.EARNOUT,
//        DISP=(MOD,DELETE),UNIT=SYSDA,SPACE=(TRK,0)
//*
//DD5     DD DSN=&HLQ0..&JOBNM2..EXTR010.EARN,
//        DISP=(MOD,DELETE),UNIT=SYSDA,SPACE=(TRK,0)
//*
//DD6     DD DSN=&HLQ0..&JOBNM2..EXTR010.SORTED,
//        DISP=(MOD,DELETE),UNIT=SYSDA,SPACE=(TRK,0)
//*
//DD7     DD DSN=&HLQ0..&JOBNM1..REPT040.SORTED,
//        DISP=(MOD,DELETE),UNIT=SYSDA,SPACE=(TRK,0)
//*
//DD8     DD DSN=&HLQ0..FIDELITY.T301.EXTRACT.SORT,
//        DISP=(MOD,DELETE),UNIT=SYSDA,SPACE=(TRK,0)
//*
//DD9     DD DSN=&HLQ0..&JOBNM..REPT040.TOTALS,
//        DISP=(MOD,DELETE),UNIT=SYSDA,SPACE=(TRK,0)
//*
//*********************************************************************
//* RESTART:
//*
//*  THIS IS AN EXTRACT STEP. A FAILURE MAY BE RESTARTED FROM
//*  THE FAILING STEP.
//*
//*                                                                   *
//*********************************************************************
//EXTR005 EXEC PGM=PSG0992,REGION=&REGN1,COND=(0,NE)
//SYSOUT   DD SYSOUT=&JCLO
//SYSPRINT DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//FIDPPG   DD DSN=&HLQ0..FIDLTY.PPG.VSAM,DISP=SHR
//EARNIN   DD DSN=&HLQ0..&JOBNM2..EXTR010.EARNOUT,DISP=SHR              
//EARNOUT  DD DSN=&HLQ0..&JOBNM2..EXTR010.EARN,
//          DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//          DCB=(RECFM=FB),
//          SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//          UNIT=SYSDA
//*
//*********************************************************************
//* SORT007 SORTS THE RESULTS OF STEP EXTR005.CMWKF02                 *
//*********************************************************************
//*
//* RESTART:
//*
//*  THIS IS A SORT STEP. A FAILURE MAY BE RESTARTED FROM
//*  THE FAILING STEP.
//*
//*********************************************************************
//SORT007  EXEC PGM=SORT,COND=(0,NE)
//SYSABOUT DD SYSOUT=U
//SYSOUT   DD SYSOUT=*
//SYSUDUMP DD SYSOUT=U
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,(10,5),RLSE)
//SORTWK02 DD UNIT=SYSDA,SPACE=(CYL,(10,5),RLSE)
//SORTIN   DD DSN=&HLQ0..&JOBNM2..EXTR010.EARN,DISP=SHR
//SORTOUT  DD DSN=&HLQ0..&JOBNM2..EXTR010.SORTED,
//          DISP=(,CATLG,DELETE),
//          SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//          DCB=(RECFM=FB),
//          UNIT=SYSDA
//SYSIN    DD DSN=&LIB3..PARMLIB(MO0480D6),DISP=SHR
//*
//*********************************************************************
//* SORT008 SORTS THE FIDELITY FILE FROM PMO0440D
//*********************************************************************
//*
//* RESTART:
//*
//*  THIS IS A SORT STEP. A FAILURE MAY BE RESTARTED FROM
//*  THE FAILING STEP.
//*
//*********************************************************************
//*
//SORT008  EXEC PGM=SORT,COND=(0,NE)
//SYSABOUT DD SYSOUT=U
//SYSOUT   DD SYSOUT=*
//SYSUDUMP DD SYSOUT=U
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,(10,5),RLSE)
//SORTWK02 DD UNIT=SYSDA,SPACE=(CYL,(10,5),RLSE)
//SORTIN   DD DSN=&HLQ0..&JOBNM1..REPT040.UPLOAD,DISP=SHR
//SORTOUT  DD DSN=&HLQ0..&JOBNM1..REPT040.SORTED,
//          DISP=(,CATLG,DELETE),
//          SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//          DCB=(RECFM=FB),
//          UNIT=SYSDA
//SYSIN    DD DSN=&LIB3..PARMLIB(MO0480D8),DISP=SHR
//*
//*********************************************************************
//* UPDT010 EXECUTES THE PROGRAM SMYP344                              *
//*********************************************************************
//*
//*
//* RESTART:
//*
//*  THIS IS AN UPDATE STEP. A FAILURE MAY NOT BE RESTARTED FROM
//*  THE FAILING STEP.
//*
//*                                                                   *
//*********************************************************************
//UPDT010 EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//        PARM='SYS=&NAT,OPRB=(.ALL)'
//SYSOUT   DD SYSOUT=&JCLO
//SYSPRINT DD SYSOUT=&JCLO
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT  DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//CMPRINT  DD SYSOUT=&JCLO
//CMPRT01  DD SYSOUT=(&REPT,MO0480D2)
//CMWKF01  DD DSN=&HLQ0..&JOBNM1..REPT040.UPLOAD,DISP=SHR               
//CMWKF02  DD DSN=&HLQ0..&JOBNM2..EXTR010.SORTED,DISP=SHR
//CMWKF03  DD DSN=&HLQ8..&JOBNM..EXTR010.EARNOUT,
//          DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//          DCB=(RECFM=FB),
//          SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//          UNIT=SYSDA
//CMWKF04  DD DSN=&HLQ0..ROUNDING.T301.EXTRACT(+01),
//          DISP=(,CATLG,DELETE),
//          DCB=(RECFM=FB),
//          SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//          UNIT=SYSDA
//CMWKF05  DD DSN=&HLQ0..&JOBNM1..REPT040.SORTED,DISP=SHR               
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB3..PARMLIB(MO0480D1),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//*********************************************************************
//* SORT015 SORTS THE ROUNDING CORRECTION FILE FROM PREVIOUS STEP
//*********************************************************************
//*
//* RESTART:
//*
//*  THIS IS A SORT STEP. A FAILURE MAY BE RESTARTED FROM
//*  THE FAILING STEP.
//*
//*********************************************************************
//*
//SORT015  EXEC PGM=SORT,COND=(2,LT)
//SYSABOUT DD SYSOUT=U
//SYSOUT   DD SYSOUT=*
//SYSUDUMP DD SYSOUT=U
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,(10,5),RLSE)
//SORTWK02 DD UNIT=SYSDA,SPACE=(CYL,(10,5),RLSE)
//SORTIN   DD DSN=&HLQ0..ROUNDING.T301.EXTRACT(+01),DISP=SHR
//SORTOUT  DD DSN=&HLQ0..FIDELITY.T301.EXTRACT.SORT,
//          DISP=(,CATLG,DELETE),
//          SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//          DCB=(RECFM=FB),
//          UNIT=SYSDA
//SYSIN    DD DSN=&LIB3..PARMLIB(MO0480D9),DISP=SHR
//*
//*********************************************************************
//*********************************************************************
//* SORT020 SORTS THE RESULTS OF STEP EXTR010                         *
//*********************************************************************
//*
//*
//* RESTART:
//*
//*  THIS IS A SORT STEP. A FAILURE MAY BE RESTARTED FROM
//*  THE FAILING STEP.
//*
//*                                                                   *
//*********************************************************************
//SORT020  EXEC PGM=SORT,COND=(2,LT)
//SYSABOUT DD SYSOUT=U
//SYSOUT   DD SYSOUT=*
//SYSUDUMP DD SYSOUT=U
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,(100,50),RLSE)
//SORTWK02 DD UNIT=SYSDA,SPACE=(CYL,(100,50),RLSE)
//SORTIN   DD DSN=&HLQ8..&JOBNM..EXTR010.EARNOUT,DISP=SHR
//SORTOUT  DD DSN=&HLQ8..&JOBNM..SORT020.EARNOUT,                       
//          DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//          SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//          DCB=(RECFM=FB),
//          UNIT=SYSDA
//SYSIN    DD DSN=&LIB3..PARMLIB(MO0480D2),DISP=SHR
//*                                                                   *
//*********************************************************************
//* EXTR030 BUILDS THE MAYO FIDEILITY EARNINGS FILE FOR TRANSMISSION  *
//*********************************************************************
//*
//*
//* RESTART:
//*
//*  THIS IS AN EXTRACT STEP. A FAILURE MAY BE RESTARTED FROM
//*  THIS STEP.
//*
//*                                                                   *
//*********************************************************************
//EXTR030 EXEC PGM=NATB030,REGION=&REGN1,COND=(2,LT),
//        PARM='SYS=&NAT,OPRB=(.ALL)'
//SYSOUT   DD SYSOUT=&JCLO
//SYSPRINT DD SYSOUT=&JCLO
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT  DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//CMPRINT  DD SYSOUT=&JCLO
//CMWKF01  DD DSN=&HLQ0..&JOBNM1..REPT040.UPLOAD,DISP=SHR               
//CMWKF02  DD DSN=&HLQ8..&JOBNM..SORT020.EARNOUT,DISP=SHR               
//CMWKF03  DD DSN=&HLQ0..&JOBNM..EXTR030.SENDFILE,
//            DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//            UNIT=SYSDA,
//*//            DCB=RECFM=FB
//            DCB=(RECFM=FB)
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB3..PARMLIB(MO0480D3),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//*********************************************************************
//* REPT040 EXECUTES THE PROGRAM SMYP408                              *
//*********************************************************************
//*
//*
//* RESTART:
//*
//*  THIS IS A REPORTING STEP. A FAILURE MAY BE RESTARTED FROM
//*  THE FAILING STEP.
//*
//*                                                                   *
//*********************************************************************
//REPT040 EXEC PGM=NATB030,REGION=&REGN1,COND=(2,LT),
//        PARM='SYS=&NAT,OPRB=(.ALL)'
//SYSOUT   DD SYSOUT=&JCLO
//SYSPRINT DD SYSOUT=&JCLO
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT  DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//CMPRINT  DD SYSOUT=&JCLO
//CMPRT01  DD SYSOUT=(&REPT,MO0480D1)
//CMWKF01  DD DSN=&HLQ0..&JOBNM..EXTR030.SENDFILE,DISP=SHR
//CMWKF02  DD DSN=&HLQ0..&JOBNM..REPT040.EARNREPT,
//            DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//            UNIT=SYSDA,
//*//            DCB=RECFM=FB
//            DCB=(RECFM=FB)
//CMWKF03  DD DSN=&HLQ0..&JOBNM..REPT040.TOTALS,
//            DISP=(,CATLG,DELETE),
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//            UNIT=SYSDA,
//*//            DCB=RECFM=FB
//            DCB=(RECFM=FB)
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB3..PARMLIB(MO0480D4),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//*********************************************************************
//*      AUTO EMAIL FUNCTION IF ERROR REPORT NEEDS ATTENTION
//*********************************************************************
//*
//* RESTART:
//*
//* EMAIL STEP. IF STEP FAILS, MAY RESTART AT THIS 'JOBSTEP.PROCSTEP'
//*                                                                   *
//*********************************************************************
//ERR1FILE  IF (UPDT010.RC EQ 2) THEN
//EMAL050  EXEC  PGM=CAJUCMD0
//SYSPRINT DD  SYSOUT=*
//$$SCD1   DD  DUMMY        >>>> PROD <<<<
//SYSIN    DD  DSN=&LIB1..PARMLIB(MO0480D7),DISP=SHR
//       ENDIF       >>>> END ERR1FILE <<<<
//*********************************************************************
