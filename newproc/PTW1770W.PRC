//PTW1170W PROC DBID='003',                                             
//         DMPS='U',                                                    
//         HLQ0='PNPD.TAX',                                             
//         HLQ8='PPDD.TAX',                                             
//         JCLO='*',                                                    
//         GDG0='0',                                                    
//         JOBNM='PTW1770D',                                            
//         LIB1='PROD',             PARMLIB                             
//         LIB2='PROD.NT2LOGON',    NT2LOGON.PARMLIB                    
//         NAT='NAT003P',                                               
//         NATMEM='PANNSEC',                                            
//         REGN1='8M',                                                  
//         REPT='8',                                                    
//         REPID1=',TW1770W1',                                          
//         REPID2=',TW1770W2',                                          
//         REPID3=',TW1770W3',                                          
//         SPC2='10,2'                                                  
//*                                                                     
//**********************************************************************
//*                                                                     
//* MODIFICATION HISTORY                                                
//* --------------------                                                
//* 99/99/99 : X. XXXXXXXXXXXXX - XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX.
//**********************************************************************
//*   STEP    PROGRAM                     DESCRIPTION                   
//* --------  --------  ------------------------------------------------
//* SORT010   SORT      SORTS EDW PLAN FILE BY PLAN NUMBER              
//*                     FILTER OUT DUPLICATES                           
//* UPDT020   TWRP0470                                                  
//* SORT030   SORT      SORTS EDW SUBPLAN FILE BY SUBPLAN NUMBER        
//* UPDT040   TWRP0471                                                  
//* SORT050   SORT      SORTS EDW CLIENT ID BY CLIENT ID                
//*                     FILTER OUT DUPLICATES                           
//* UPDT060   TWRP0472                                                  
//*                                                                     
//**********************************************************************
//* SORT010 - SORTS EDW PLAN FILE BY PLAN NUMBER                        
//*           FILTER OUT DUPLICATES                                     
//*-----------------------------------------------------------------    
//SORT010 EXEC PGM=SORT,REGION=&REGN1,COND=(0,NE)                       
//SYSOUT    DD SYSOUT=&JCLO                                             
//SYSPRINT  DD SYSOUT=&JCLO                                             
//SORTWK01  DD UNIT=SYSDA,SPACE=(CYL,(20),RLSE)                         
//SORTWK02  DD UNIT=SYSDA,SPACE=(CYL,(20),RLSE)                         
//SORTWK03  DD UNIT=SYSDA,SPACE=(CYL,(20),RLSE)                         
//SORTIN    DD DSN=&HLQ0..EDW.PLAN.FILE(&GDG0),DISP=SHR                 
//SORTOUT   DD DSN=&HLQ8..&JOBNM..EDW.PLAN.FILE.SRTD,                   
//             DISP=(,CATLG,DELETE),                                    
//             UNIT=SYSDA,DATACLAS=DCPSEXTC,                            
//             RECFM=FB,LRECL=200,                                      
//             SPACE=(CYL,(&SPC2),RLSE)                                 
//SYSIN     DD DSN=&LIB1..PARMLIB(TW1770W3),DISP=SHR                    
//*                                                                     
//* SORT FIELDS=(1,6,CH,A)    PLAN NUMBER                               
//* SUM  FIELDS=NONE                                                    
//*                                                                     
//*-----------------------------------------------------------------    
//UPDT020  EXEC PGM=NATB030,REGION=8M,COND=(0,NE),                      
//          PARM='SYS=&NAT'                                             
//*-----------------------------------------------------------------    
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//SYSOUT    DD SYSOUT=&JCLO                                             
//SYSUDUMP  DD SYSOUT=&DMPS                                             
//SYSPRINT  DD SYSOUT=&JCLO                                             
//DDPRINT   DD SYSOUT=&JCLO                                             
//CMPRINT   DD SYSOUT=&JCLO                                             
//CMPRT01  DD SYSOUT=(&REPT&REPID1)                                     
//CMWKF01   DD DSN=&HLQ8..&JOBNM..EDW.PLAN.FILE.SRTD,                   
//             DISP=SHR                                                 
//*                                                                     
//CMSYNIN   DD DSN=&LIB2..PARMLIB(&NATMEM),DISP=SHR                     
//          DD DSN=&LIB1..PARMLIB(TW1770W1),DISP=SHR                    
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                     
//*  TWRP0470                                                           
//*                                                                     
//*-----------------------------------------------------------------    
//*           FILTER OUT DUPLICATES                                     
//*-----------------------------------------------------------------    
//SORT030 EXEC PGM=SORT,REGION=&REGN1,COND=(0,NE)                       
//*-----------------------------------------------------------------    
//SYSOUT    DD SYSOUT=&JCLO                                             
//SYSPRINT  DD SYSOUT=&JCLO                                             
//SORTWK01  DD UNIT=SYSDA,SPACE=(CYL,(25),RLSE)                         
//SORTWK02  DD UNIT=SYSDA,SPACE=(CYL,(25),RLSE)                         
//SORTWK03  DD UNIT=SYSDA,SPACE=(CYL,(25),RLSE)                         
//SORTWK04  DD UNIT=SYSDA,SPACE=(CYL,(25),RLSE)                         
//SORTIN    DD DSN=&HLQ0..EDW.SUBPLAN.FILE(&GDG0),                      
//             DISP=SHR                                                 
//SORTOUT   DD DSN=&HLQ8..&JOBNM..EDW.SUBPLAN.FILE.SRTD,                
//             DISP=(,CATLG,DELETE),                                    
//             UNIT=SYSDA,DATACLAS=DCPSEXTC,                            
//             RECFM=FB,LRECL=200,                                      
//             SPACE=(CYL,(&SPC2),RLSE)                                 
//SYSIN     DD DSN=&LIB1..PARMLIB(TW1770W3),DISP=SHR                    
//*                                                                     
//* SORT FIELDS=(1,6,CH,A)    PLAN NUMBER                               
//* SUM  FIELDS=NONE                                                    
//*-----------------------------------------------------------------    
//UPDT040  EXEC PGM=NATB030,REGION=8M,COND=(0,NE),                      
//          PARM='SYS=&NAT'                                             
//*-----------------------------------------------------------------    
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                   
//SYSOUT    DD SYSOUT=&JCLO                                             
//SYSUDUMP  DD SYSOUT=&DMPS                                             
//SYSPRINT  DD SYSOUT=&JCLO                                             
//DDPRINT   DD SYSOUT=&JCLO                                             
//CMPRINT   DD SYSOUT=&JCLO                                             
//CMPRT01  DD SYSOUT=(&REPT&REPID2)                                     
//CMWKF01  DD DSN=&HLQ8..&JOBNM..EDW.SUBPLAN.FILE.SRTD,                 
//             DISP=SHR                                                 
//*                                                                     
//CMSYNIN  DD DSN=&LIB2..PARMLIB(&NATMEM),DISP=SHR                      
//         DD DSN=&LIB1..PARMLIB(TW1770W2),DISP=SHR                     
//         DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                      
//*  TWRP0471                                                           
//*-----------------------------------------------------------------    
//*           FILTER OUT DUPLICATES                                     
//*-----------------------------------------------------------------    
//SORT050 EXEC PGM=SORT,REGION=&REGN1,COND=(0,NE)                       
//*-----------------------------------------------------------------    
//SYSOUT    DD SYSOUT=&JCLO                                             
//SYSPRINT  DD SYSOUT=&JCLO                                             
//SORTWK01  DD UNIT=SYSDA,SPACE=(CYL,(25),RLSE)                         
//SORTWK02  DD UNIT=SYSDA,SPACE=(CYL,(25),RLSE)                         
//SORTWK03  DD UNIT=SYSDA,SPACE=(CYL,(25),RLSE)                         
//SORTWK04  DD UNIT=SYSDA,SPACE=(CYL,(25),RLSE)                         
//SORTIN    DD DSN=&HLQ0..EDW.CLIENTID.FILE(&GDG0),                     
//             DISP=SHR                                                 
//SORTOUT   DD DSN=&HLQ8..&JOBNM..EDW.CLIENTID.FILE.SRTD,               
//             DISP=(,CATLG,DELETE),                                    
//             UNIT=SYSDA,DATACLAS=DCPSEXTC,                            
//             RECFM=FB,LRECL=66,                                       
//             SPACE=(CYL,(&SPC2),RLSE)                                 
//SYSIN     DD DSN=&LIB1..PARMLIB(TW1770W4),DISP=SHR                    
//*                                                                     
//* SORT FIELDS=(1,6,CH,A)    CLIENT ID                                 
//* SUM  FIELDS=NONE                                                    
//*-----------------------------------------------------------------    
//UPDT060  EXEC PGM=NATB030,REGION=8M,COND=(0,NE),                      
//          PARM='SYS=&NAT'                                             
//*-----------------------------------------------------------------    
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                   
//SYSOUT    DD SYSOUT=&JCLO                                             
//SYSUDUMP  DD SYSOUT=&DMPS                                             
//SYSPRINT  DD SYSOUT=&JCLO                                             
//DDPRINT   DD SYSOUT=&JCLO                                             
//CMPRINT   DD SYSOUT=&JCLO                                             
//CMPRT01  DD SYSOUT=(&REPT&REPID3)                                     
//CMWKF01  DD DSN=&HLQ8..&JOBNM..EDW.CLIENTID.FILE.SRTD,                
//             DISP=SHR                                                 
//*                                                                     
//CMSYNIN  DD DSN=&LIB2..PARMLIB(&NATMEM),DISP=SHR                      
//         DD DSN=&LIB1..PARMLIB(TW1770W5),DISP=SHR                     
//         DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                      
