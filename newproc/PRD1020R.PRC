//PRD1020R  PROC REGN1='8M',
//          PARMMEM='XXXXXXXX',
//          DATA='XXXXX',
//          NATLOGON='PANNSEC',
//          NAT='NATB030',
//          SYS='NAT045P',
//          DBID=045,
//          LIB1='PROD',
//          HLQ1='PDA.ANN',
//          HLQ8='PPT.COR',
//          SPC1=50,
//          SPC2=10,
//          JCLO=*,
//          DMPS=U,
//          REPT=8
//********************************************************************
//*        INITIALIZE DATASETS TO BE USED
//********************************************************************
//DELT010  EXEC PGM=IEFBR14
//SYSPRINT DD SYSOUT=*
//D1       DD DSN=&HLQ8..RIDER.Y2006.REA.FILE1,
//            UNIT=SYSDA,
//            DISP=(MOD,DELETE,DELETE),
//            SPACE=(CYL,(0,0))
//D2       DD DSN=&HLQ8..RIDER.Y2006.REA.FILE2,
//            UNIT=SYSDA,
//            DISP=(MOD,DELETE,DELETE),
//            SPACE=(CYL,(0,0))
//D3       DD DSN=&HLQ8..RIDER.Y2006.REA.FILE3,
//            UNIT=SYSDA,
//            DISP=(MOD,DELETE,DELETE),
//            SPACE=(CYL,(0,0))
//D4       DD DSN=&HLQ8..RIDER.Y2006.REA.FILE4,
//            UNIT=SYSDA,
//            DISP=(MOD,DELETE,DELETE),
//            SPACE=(CYL,(0,0))
//*********************************************************************
//*        RIDP360 - READ DA REPORTING REA EXTRACT
//*********************************************************************
//EXTR020  EXEC PGM=&NAT,COND=(0,NE),REGION=&REGN1,
//            PARM='SYS=&SYS,UDB=&DBID,WH=ON'
//SYSPRINT DD  SYSOUT=&JCLO
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//CMPRT01  DD  SYSOUT=&REPT
//CMWKF01  DD  DSN=&HLQ1..RIDER.Y2006.REA.&DATA,DISP=SHR
//CMWKF02  DD  DSN=&HLQ8..RIDER.Y2006.REA.FILE1,
//             DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//             DCB=(RECFM=FB,LRECL=400)
//CMWKF03  DD  DSN=&HLQ8..RIDER.Y2006.REA.FILE2,
//             DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//             DCB=(RECFM=FB,LRECL=400)
//CMWKF04  DD  DSN=&HLQ8..RIDER.Y2006.REA.FILE3,
//             DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//             DCB=(RECFM=FB,LRECL=400)
//CMWKF05  DD  DSN=&HLQ8..RIDER.Y2006.REA.FILE4,
//             DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//             DCB=(RECFM=FB,LRECL=400)
//SYSUDUMP DD  SYSOUT=&DMPS
//SYSOUT   DD  SYSOUT=&JCLO
//CMSYNIN  DD  DSN=&LIB1..NT2LOGON.PARMLIB(&NATLOGON),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(&PARMMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
