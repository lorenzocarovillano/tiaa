//P2030BFD PROC DBID=015,
//         DMPS='U',
//         JCLO='*',
//         LIB1='PROD',
//         LIB2='PROD.NT2LOGON',
//         NAT='NAT015P',
//         NATMEM='PANNSEC',
//         REGN1='5M'
//**********************************************************************
//*        BENP9830 - BENEFICIARY INTERFACE DATE UPDATE                 
//**********************************************************************
//REPT010  EXEC PGM=NATB030,REGION=&REGN1,                              
//         PARM='SYS=&NAT'                                              
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                   
//SYSPRINT DD SYSOUT=&JCLO                                              
//SYSOUT   DD SYSOUT=&JCLO                                              
//SYSUDUMP DD SYSOUT=&DMPS                                              
//DDPRINT  DD SYSOUT=&JCLO                                              
//CMPRINT  DD SYSOUT=&JCLO                                              
//CMSYNIN  DD DSN=&LIB2..PARMLIB(&NATMEM),DISP=SHR                      
//         DD DSN=&LIB1..PARMLIB(P2030BF1),DISP=SHR                     
//**********************************************************************
