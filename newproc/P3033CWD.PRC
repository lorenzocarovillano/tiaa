//P3033CWD PROC DBID='045'
//**********************************************************************
//* DESCRIPTION  : PRODUCE MIN/WPID DOWNLOAD FILE                      *
//* PROGRAM NAME : ICWB3100                                            *
//* ADABAS FILES : DB045/186/208/82/   DB46/42                         *
//* REMARKS      :                                                     *
//* -------------------------------------------------------------------*
//* CHANGES      :                                                     *
//*                                                                    *
//**********************************************************************
//**********************************************************************
//*    INITIALIZE WORK FILE                                            *
//**********************************************************************
//INIT010  EXEC PGM=IDCAMS
//DD1      DD DUMMY
//DD2      DD DSN=PCWF.COR.P3033CWD.MIN.WPID.DWNLD,
//         DISP=OLD
//*
//SYSPRINT DD  SYSOUT=*
//SYSIN    DD  DSN=PROD.PARMLIB(P3033CW1),DISP=SHR
//*
//**********************************************************************
//EXTR010  EXEC PGM=NATB030,REGION=8M,COND=(0,NE),
//         PARM='SYS=NAT045P'
//DDCARD   DD  DSN=PROD.PARMLIB(DBAPP&DBID),DISP=SHR
//SYSOUT   DD  SYSOUT=*
//SYSPRINT DD  SYSOUT=*
//DDPRINT  DD  SYSOUT=*
//CMPRINT  DD  SYSOUT=*
//SYSUDUMP DD  SYSOUT=*
//CMPRT01  DD  SYSOUT=(8,CW3033D1)
//CMWKF01  DD  DSN=PCWF.COR.P3033CWD.MIN.WPID.DWNLD,
//             DISP=SHR,
//             UNIT=SYSDA,
//             SPACE=(CYL,(5,5),RLSE),
//             RECFM=FB,LRECL=114,BLKSIZE=27930
//CMSYNIN  DD  DSN=PROD.NT2LOGON.PARMLIB(PANNSEC),DISP=SHR
//         DD  DSN=PROD.PARMLIB(P3033CW2),DISP=SHR
//*
