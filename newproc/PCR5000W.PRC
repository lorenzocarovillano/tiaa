//PCR5000W PROC DBID='005',
//         DMPS='U',
//         JCLO='*',
//         HLQ0='PCR',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         NAT='NAT005P',
//         NATMEM='PANNSEC',
//         REGN1='5M'
//**********************************************************************
//* CORP850 - READS SECURITY FILE AND EXTRACT ALL RECORDS
//**********************************************************************
//EXTR010  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//         PARM='OBJIN=N,SYS=&NAT'
//SYSPRINT DD SYSOUT=&JCLO
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSOUT   DD SYSOUT=&JCLO
//DDPRINT  DD SYSOUT=&JCLO
//CMPRINT  DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//CMWKF01  DD DSN=&HLQ0..COR.ENTITLE.EXTRACT,DISP=SHR
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(CR5000W1),DISP=SHR
