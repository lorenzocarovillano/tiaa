//P2241CPM PROC HLQ0='PDS',         DASD HLQ
//         HLQ8='PDS.ANN',          HLQ
//         JOBNM='P2238CPM',
//         REGN1='9M',              REGION SIZE
//         GDG='+1',                NEW GDG FILE CREATED
//         SPC1='20',               FILE1 - PRIMARY ALLOCATION
//         SPC2='5',                FILE1 - SECONDARY ALLOCATION
//         RPTID='CP2238M',         MOBIUS
//         REPT1='8'                MOBIUS
//*
//*--------------------------------------------------------------------*
//*                                                                    *
//* PROCEDURE:  P2241CPM                                          2016 *
//*          :  CONSOLIDATED PAYMENT SYSTEMS                           *
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
//*  STEP    PROGRAM   PARMS                   COMMENTS                *
//* -------  -------  --------  -------------------------------------- *
//* COPY010  IEBGENER           COPY CHECK FILE TO GDG                 *
//* COPY020  IEBGENER           COPY CHECK FILE TO MOBIUS              *
//* DELT030  IEFBR14            DELETE THE TEMPORARY FILE  FROM BANK   *
//*                                                                    *
//*--------------------------------------------------------------------*
//* COPY010  -  COPY CHECK FILE TO GDG                                 *
//*             IEBGENER                                               *
//*--------------------------------------------------------------------*
//COPY010 EXEC PGM=IEBGENER,REGION=&REGN1,COND=(0,NE)
//SYSOUT   DD SYSOUT=*
//SYSPRINT DD SYSOUT=*
//SYSUT1   DD  DSN=&HLQ0..&JOBNM..NY.FTP.CITI.CHECK1,DISP=SHR
//SYSUT2   DD  DSN=&HLQ8..GLOBAL.PAY.CITIBANK.POC(&GDG),
//         DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//         UNIT=SYSDA,SPACE=(CYL,(&SPC1,&SPC2))
//SYSIN    DD DUMMY
//*
//*--------------------------------------------------------------------*
//* COPY020     COPY CHECK FILE TO MOBIUS                              *
//*             IEBGENER                                               *
//*--------------------------------------------------------------------*
//COPY020 EXEC PGM=IEBGENER,REGION=&REGN1,COND=(0,NE)
//SYSOUT   DD SYSOUT=*
//SYSPRINT DD SYSOUT=*
//SYSUT1   DD DSN=&HLQ0..&JOBNM..NY.FTP.CITI.CHECK1,DISP=SHR
//SYSUT2   DD SYSOUT=(&REPT1,&RPTID.1)
//SYSIN    DD DUMMY
//*
//*--------------------------------------------------------------------*
//* DELT030  -  DELETE THE TEMPORARY FILES FROM THE BANK SO TOMORROW   *
//*             FILES CAN BE REALLOCATED BY THE BANK                   *
//*             IEFBR14                                                *
//*--------------------------------------------------------------------*
//DELT030  EXEC PGM=IEFBR14,COND=(0,NE)
//SYSPRINT DD SYSOUT=*
//DD01     DD  DSN=&HLQ0..&JOBNM..NY.FTP.CITI.CHECK1,
//         DISP=(OLD,DELETE,DELETE)
