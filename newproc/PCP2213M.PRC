//PCP2213M PROC HLQ0='PIA.ANN',     DATASET HLQ                         
//         JOBNM='P2210CPM',        JOBNAME                             
//         JCLO00=*,                JOB OUTPUT                          
//         REGN1='9M'               REGION SIZE                         
//*                                                                     
//*--------------------------------------------------------------------*
//*                                                                    *
//* PROCEDURE:  PCP2213M                           DATE:    07/21/2017 *
//*                                                                    *
//*           CONSOLIDATED PAYMENT SYSTEM / OPEN INVESTMENTS           *
//*                                                                    *
//*               CREATE A COPY OF PENDS.S487 FILE                     *
//*--------------------------------------------------------------------*
//*--------------------------------------------------------------------*
//* COPY010  -  COPY FILE TO DASD  NACHA - RET 1                       *
//*             IEBGENER                                               *
//*--------------------------------------------------------------------*
//COPY010 EXEC PGM=IEBGENER,REGION=&REGN1,COND=(0,NE)                   
//SYSOUT   DD SYSOUT=&JCLO00                                            
//SYSPRINT DD SYSOUT=&JCLO00                                            
//SYSUT1   DD  DSN=&HLQ0..&JOBNM..PENDS.S487,DISP=SHR                   
//SYSUT2   DD  DSN=&HLQ0..&JOBNM..PENDS.S487PE,DISP=SHR                 
//**       RECFM=FB,LRECL=132                                           
//SYSIN    DD DUMMY                                                     
//*                                                                     
