//PCW9125D PROC DBID=045,
//       NATVERS='NATB030',
//       REGN1='8M',
//       NAT='NAT003P',                                                 
//       DMPS='U',
//       JCLO='*',
//       LIB1='PROD',
//       NATMEM='PANNSEC',                                              
//       REPT='8'
//*-------------------------------------------------------------------*
//* Berwyn Report automation - IA Interface module
//* ----------------------------------------------------------------- *
//UPDT010  EXEC PGM=&NATVERS,REGION=&REGN1,
//            PARM='SYS=&NAT'
//UCC11NR  DD DUMMY
//SYSUDUMP DD SYSOUT=&DMPS
//SYSPRINT DD SYSOUT=&JCLO
//DDPRINT  DD SYSOUT=&JCLO
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMPRINT  DD SYSOUT=&JCLO
//CMPRT01  DD SYSOUT=(&REPT,CW9125D1)
//CMPRT02  DD SYSOUT=(&REPT,CW9125D2)
//CMSYNIN  DD DSN=&LIB1..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(CW9125DA),DISP=SHR
