//P2041NID PROC GDG0='0',
//         GDG1='+1',
//         HLQ1='PPM.ANN',
//         HLQ2='POMPL.FA',
//         DMPS='U',
//         JCLO='*',
//         JOBNMI='P2040NID',
//         JOBNMO='P2041NID',
//         LIB='PROD.PARMLIB',
//         REPT='8',
//         SPC1='5,10'
//*                                                                     
//********************************************************************* 
//* PROCEDURE: P2041NID                             DATE: 01/16/15    * 
//********************************************************************* 
//* ----------------------------------------------------------------- * 
//* PROGRAMS EXECUTED : PSG1550 PSG1551 PSG1552                       * 
//* ----------------------------------------------------------------- * 
//* 01/16/14 OEC DB2    - INTRODUCED P2041NID AS PARTOF OEC DB2 CONV  *
//*                     - SPLIT P2040NID INTO P2040NID & P2041NID     *
//*                     - WHERE P2040NID WOULD EXECUTE THE NATURAL    *
//*                     - STEPS AND P2041NID WOULD EXECUTE THE        *
//*                     - NON NATURAL STEPS                           *
//********************************************************************* 
//*---------------------------------------------------------------------
//*       PARM SUBSTITUTION FOR SYSTSIN CARDS
//*---------------------------------------------------------------------
//PARM010 EXEC PARMSUB,COND=(4,LT)
//*MSYS        PARM='PARMSUB DB2S=&DB2S DBPL=&DBPL DBPGM=PSG1550
//*MSYS              PARMS=&FSET'
//TEMPLATE DD  DSN=&LIB(DB2CALLB),
//             DISP=SHR
//******************************************************************
//* PROGRAM: GET PLAN/PART ID USING CONTRACT NUMBER AND TAG TO EACH
//* RECORD AND CREATE WORK EXTRACT FILE
//******************************************************************
//EXTR020 EXEC PGM=IKJEFT1B,REGION=0M
//SYSOUT   DD  SYSOUT=&JCLO
//SYSDBOUT DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//SYSTSIN  DD  DSN=&&SUBSTED,DISP=(OLD,DELETE)
//SYSTSPRT DD  SYSOUT=&JCLO
//EXTFILE  DD DSN=&HLQ1..&JOBNMI..REPLREG.EXTRACT(&GDG0),DISP=SHR
//OUTFILE  DD  DSN=&HLQ1..&JOBNMO..WORK.EXTRACT(&GDG1),
//             DISP=(NEW,CATLG,DELETE),
//             UNIT=SYSDA,DATACLAS=&DATACLS,
//             DCB=(LRECL=1069,RECFM=FB,BLKSIZE=0),
//             SPACE=(TRK,(&SPC1),RLSE)
//******************************************************************
//* SORT: SORT WORK EXTRACT BY PLAN/PART ID
//******************************************************************
//SORT030  EXEC PGM=SORT,COND=(0,LT)
//SORTIN   DD  DSN=&HLQ1..&JOBNMO..WORK.EXTRACT(&GDG1),DISP=SHR
//SORTOUT  DD  DSN=&HLQ1..&JOBNMO..WORK.EXTRACT.SRT(&GDG1),
//             DISP=(NEW,CATLG,DELETE),                                 
//             UNIT=SYSDA,DATACLAS=DCPSEXTC,
//             DCB=(RECFM=FB,LRECL=1069,BLKSIZE=0),
//             SPACE=(CYL,(&SPC1),RLSE)
//SYSOUT   DD  SYSOUT=&JCLO
//SYSIN    DD  DSN=&LIB(NI2041D1),DISP=SHR
//******************************************************************
//* SORT: GET ONLY T801 AND T813 RECORDS FROM DEFWK FILE AND CREATE
//* TWO SEPERATE FILES
//******************************************************************
//SORT040  EXEC PGM=SORT,COND=(0,LT)
//SORTIN   DD  DSN=&HLQ2..DEFWK(&GDG0),DISP=SHR
//SORTOF1  DD  DSN=&&DEFWK801,
//             DISP=(NEW,PASS),                                         
//             UNIT=SYSDA,DATACLAS=DCPSEXTC,
//             DCB=(RECFM=FB,LRECL=48,BLKSIZE=0),
//             SPACE=(CYL,(&SPC1),RLSE)
//SORTOF2  DD  DSN=&&DEFWK813,
//             DISP=(NEW,PASS),                                         
//             UNIT=SYSDA,DATACLAS=DCPSEXTC,
//             DCB=(RECFM=FB,LRECL=48,BLKSIZE=0),
//             SPACE=(CYL,(&SPC1),RLSE)
//SYSOUT   DD  SYSOUT=&JCLO
//SYSIN    DD  DSN=&LIB(NI2041D2),DISP=SHR
//*----------------------------------------------------------------*
//* SORT: GET THE RECORDS THAT ARE MATCHING WITH WORK EXTRACT FROM
//* DEFWK801 FILE
//*----------------------------------------------------------------*
//SORT050  EXEC PGM=SORT,COND=(0,LT)
//IN01     DD  DSN=&&DEFWK801,DISP=(OLD,PASS)                           
//IN02     DD  DSN=&HLQ1..&JOBNMO..WORK.EXTRACT.SRT(&GDG1),DISP=SHR
//SORTOUT  DD  DSN=&HLQ1..&JOBNMO..DEFWK801.MATCH(&GDG1),               
//             DISP=(NEW,CATLG,DELETE),                                 
//             UNIT=SYSDA,DATACLAS=DCPSEXTC,
//             DCB=(RECFM=FB,LRECL=48,BLKSIZE=0),
//             SPACE=(CYL,(&SPC1),RLSE)
//SYSIN    DD  DSN=&LIB(NI2041D3),DISP=SHR
//SYSOUT   DD  SYSOUT=&JCLO
//SYSABOUT DD  SYSOUT=&JCLO
//SYSDBOUT DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//*----------------------------------------------------------------*
//* SORT: GET THE RECORDS THAT ARE MATCHING WITH WORK EXTRACT FROM
//* DEFWK813 FILE
//*----------------------------------------------------------------*
//SORT060  EXEC PGM=SORT,COND=(0,LT)                                    
//IN01     DD  DSN=&&DEFWK813,DISP=(OLD,PASS)                           
//IN02     DD  DSN=&HLQ1..&JOBNMO..WORK.EXTRACT.SRT(&GDG1),DISP=SHR
//SORTOUT  DD  DSN=&HLQ1..&JOBNMO..DEFWK813.MATCH(&GDG1),               
//             DISP=(NEW,CATLG,DELETE),                                 
//             UNIT=SYSDA,DATACLAS=DCPSEXTC,
//             DCB=(RECFM=FB,LRECL=48,BLKSIZE=0),
//             SPACE=(CYL,(&SPC1),RLSE)
//SYSIN    DD  DSN=&LIB(NI2041D3),DISP=SHR
//SYSOUT   DD  SYSOUT=&JCLO
//SYSABOUT DD  SYSOUT=&JCLO
//SYSDBOUT DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//*---------------------------------------------------------------------
//*       PARM SUBSTITUTION FOR SYSTSIN CARDS
//*---------------------------------------------------------------------
//PARM070 EXEC PARMSUB,COND=(4,LT)
//*MSYS        PARM='PARMSUB DB2S=&DB2S DBPL=&DBPL DBPGM=PSG1551
//*MSYS              PARMS=&FSET'
//TEMPLATE DD  DSN=&LIB(DB2CALLB),
//             DISP=SHR
//******************************************************************
//* PROGRAM: ADD THE 3 OMNI FIELDS AT END OF THE REPLACEMENT REG EXTR
//* AT END OF THE REPLACEMENT REG EXTR FILE USING T801 DEFWK FILE
//* ALSO UPDATE IF REPLACEMENT REG FIELDS ARE CHANGING THROUGH T813'S
//******************************************************************
//EXTR080 EXEC   PGM=IKJEFT1B,REGION=0M
//SYSOUT   DD  SYSOUT=&JCLO
//SYSDBOUT DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//SYSTSIN  DD  DSN=&&SUBSTED,DISP=(OLD,DELETE)
//SYSTSPRT DD  SYSOUT=&JCLO
//*
//EXTFILE  DD DSN=&HLQ1..&JOBNMO..WORK.EXTRACT.SRT(&GDG1),DISP=SHR
//T801FILE DD DSN=&HLQ1..&JOBNMO..DEFWK801.MATCH(&GDG1),DISP=SHR
//T813FILE DD DSN=&HLQ1..&JOBNMO..DEFWK813.MATCH(&GDG1),DISP=SHR
//*
//OUTFILE  DD  DSN=&HLQ1..&JOBNMO..REPLREG.EXTR.UPD(&GDG1),
//             DISP=(NEW,CATLG,CATLG),
//             UNIT=SYSDA,DATACLAS=&DATACLS,
//             DCB=(LRECL=1075,RECFM=FB,BLKSIZE=0),
//             SPACE=(TRK,(&SPC1),RLSE)
//EXCRPT   DD  DSN=&HLQ1..&JOBNMO..EXCEPT.FILE(&GDG1),
//             DISP=(NEW,CATLG,DELETE),                                 
//             UNIT=SYSDA,DATACLAS=&DATACLS,
//             DCB=(RECFM=FB,LRECL=118,BLKSIZE=0),
//             SPACE=(CYL,(&SPC1),RLSE)
//*----------------------------------------------------------------*
//* SORT: GET THE RECORDS THAT ARE NOT MATCHING WITH WORK EXTRACT
//* FROM DEFWK801 FILE
//*----------------------------------------------------------------*
//SORT090  EXEC PGM=SORT                                                
//IN01     DD  DSN=&&DEFWK801,DISP=(OLD,PASS)                           
//IN02     DD  DSN=&HLQ1..&JOBNMO..WORK.EXTRACT.SRT(&GDG1),DISP=SHR
//SORTOUT  DD  DSN=&HLQ1..&JOBNMO..DEFWK801.NOMATCH(&GDG1),             
//             DISP=(NEW,CATLG,DELETE),                                 
//             UNIT=SYSDA,DATACLAS=DCPSEXTC,
//             DCB=(RECFM=FB,LRECL=48,BLKSIZE=0),
//             SPACE=(CYL,(&SPC1),RLSE)
//SYSIN    DD  DSN=&LIB(NI2041D4),DISP=SHR
//SYSOUT   DD  SYSOUT=&JCLO
//SYSABOUT DD  SYSOUT=&JCLO
//SYSDBOUT DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//*----------------------------------------------------------------*
//* SORT: GET THE RECORDS THAT ARE NOT MATCHING WITH WORK EXTRACT
//* FROM DEFWK813 FILE
//*----------------------------------------------------------------*
//SORT100  EXEC PGM=SORT                                                
//IN01     DD  DSN=&&DEFWK813,DISP=(OLD,PASS)                           
//IN02     DD  DSN=&HLQ1..&JOBNMO..WORK.EXTRACT.SRT(&GDG1),DISP=SHR
//SORTOUT  DD  DSN=&HLQ1..&JOBNMO..DEFWK813.NOMATCH(&GDG1),             
//             DISP=(NEW,CATLG,DELETE),                                 
//             UNIT=SYSDA,DATACLAS=DCPSEXTC,
//             DCB=(RECFM=FB,LRECL=48,BLKSIZE=0),
//             SPACE=(CYL,(&SPC1),RLSE)
//SYSIN    DD  DSN=&LIB(NI2041D4),DISP=SHR
//SYSOUT   DD  SYSOUT=&JCLO
//SYSABOUT DD  SYSOUT=&JCLO
//SYSDBOUT DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//*----------------------------------------------------------------*
//* SORT: MERGE BOTH THE 801 AND 813 FILES
//*----------------------------------------------------------------*
//SORT110  EXEC PGM=SORT,COND=(0,LT)
//SORTIN   DD  DSN=&HLQ1..&JOBNMO..DEFWK801.NOMATCH(&GDG1),DISP=SHR
//         DD  DSN=&HLQ1..&JOBNMO..DEFWK813.NOMATCH(&GDG1),DISP=SHR
//SORTOUT  DD  DSN=&HLQ1..&JOBNMO..DEFWKFL.NOMATCH(&GDG1),
//             DISP=(NEW,CATLG,DELETE),                                 
//             UNIT=SYSDA,DATACLAS=DCPSEXTC,
//             DCB=(RECFM=FB,LRECL=48,BLKSIZE=0),
//             SPACE=(CYL,(&SPC1),RLSE)
//SYSOUT   DD  SYSOUT=&JCLO
//SYSIN    DD  DSN=&LIB(NI2041D1),DISP=SHR
//*---------------------------------------------------------------------
//*       PARM SUBSTITUTION FOR SYSTSIN CARDS
//*---------------------------------------------------------------------
//PARM120 EXEC PARMSUB,COND=(4,LT)
//*MSYS        PARM='PARMSUB DB2S=&DB2S DBPL=&DBPL DBPGM=PSG1552
//*MSYS              PARMS=&FSET'
//TEMPLATE DD  DSN=&LIB(DB2CALLB),
//             DISP=SHR
//******************************************************************
//*****************************************************************
//* PROGRAM:  GET THE REQUIRED REPLACEMENT REG FILEDS FROM OMNI
//* AND APPEND THE REOCRDS AT END OF THE REPLCEMENT REG EXTRACT FILE
//* IF ANY OF THE REPLACEMENT REGISTER RELATED FILEDS ARE MODIFYING
//* THROUGH T801'S OR T813'S.
//*****************************************************************
//EXTR130 EXEC   PGM=IKJEFT1B,REGION=0M
//SYSOUT   DD  SYSOUT=&JCLO
//SYSDBOUT DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//SYSTSIN  DD  DSN=&&SUBSTED,DISP=(OLD,DELETE)
//SYSTSPRT DD  SYSOUT=&JCLO
//*
//DEFWKFL  DD DSN=&HLQ1..&JOBNMO..DEFWKFL.NOMATCH(&GDG1),DISP=SHR
//REGFILE  DD  DSN=&HLQ1..&JOBNMO..REPLREG.EXTR.UPD(&GDG1),
//             DISP=(MOD,CATLG,CATLG),
//             UNIT=SYSDA,DATACLAS=&DATACLS,
//             DCB=(LRECL=1075,RECFM=FB,BLKSIZE=0),
//             SPACE=(TRK,(&SPC1),RLSE)
//EXCRPT   DD  DSN=&HLQ1..&JOBNMO..EXCEPT.FILE(&GDG1),
//             DISP=(MOD,CATLG,CATLG),                                  
//             UNIT=SYSDA,DATACLAS=&DATACLS,
//             DCB=(RECFM=FB,LRECL=118,BLKSIZE=0),
//             SPACE=(CYL,(&SPC1),RLSE)
//*----------------------------------------------------------------*
//* IEBGENER : COPY THE EXCEPTION REPORT TO MOBIUS
//*----------------------------------------------------------------*
//COPY140  EXEC PGM=IEBGENER,COND=(0,NE)
//SYSPRINT DD  SYSOUT=&JCLO
//SYSIN    DD  DUMMY
//SYSUT1   DD  DSN=&HLQ1..&JOBNMO..EXCEPT.FILE(&GDG1),DISP=SHR
//SYSUT2   DD  SYSOUT=(&REPT,NI2041D3)
//*----------------------------------------------------------------*
//*                      END-OF-JOB
//*----------------------------------------------------------------*
