//P5210IAA PROC DBID='003',
//         DMPS='U',
//         GDG0='+0',
//         HLQ7='PPDT',
//         JCLO='*',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='5M',
//         REPT='8',
//         UNIT1='CARTV'
//*
//* ----------------------------------------------------------------- * 
//* PROCEDURE:  P5210IAA                 DATE:    11/25/96            * 
//* ----------------------------------------------------------------- * 
//* PROGRAMS EXECUTED: IAAP396 (NATURAL)                                
//* ----------------------------------------------------------------- * 
//* APPLY DIVIDEND RATE CHANGES ON TRANS TO IA FUND RECORDS
//*         UPDATE IAA-TIAA-FUND-RCRD FILE 201
//* ----------------------------------------------------------------- * 
//*                                                                     
//* MODIFICATION HISTORY
//* 04/12/02 - OIA JCL STANDARDS - PHIL STEINHAUSER
//*                                                                     
//**********************************************************************
//*
//UPDT010 EXEC PGM=NATB030,REGION=&REGN1,
//        PARM='SYS=&NAT'
//SYSPRINT  DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//DDPRINT   DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRINT   DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRT01   DD SYSOUT=(&REPT,IA5210A1)
//SYSUDUMP  DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSABOUT  DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSDBOUT  DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSOUT    DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//*                                                                     
//CMWKF01  DD  DSN=&HLQ7..IAIQ.JAN.REVAL.TRANS(&GDG0), TRANS RATE
//             DISP=SHR,                                  FILE
//             UNIT=&UNIT1                                              
//*                                                                     
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(P5210IA1),DISP=SHR          IAAP340
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
