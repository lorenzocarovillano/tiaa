//P2000IAM PROC DBID='003',
//         DMPS='U',
//         HLQ8='PPDD',
//         JCLO='*',
//         JOBNM='P2000IAM',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='5M',
//         REPT='8',
//         SPC1='5',
//         SPC2='5'
//*
//* ----------------------------------------------------------------- *
//* PROCEDURE:  P2000IAM                 DATE:    11/12/94            *
//* ----------------------------------------------------------------- *
//* PROGRAMS EXECUTED:
//*
//*                     NT2B030 (IAAP950)                             *
//*                                                                     
//* MODIFICATION HISTORY
//* 04/03/02 - OIA JCL STANDARDS - PHIL STEINHAUSER
//*                                                                     
//* ----------------------------------------------------------------- *
//*                                                                     
//REPT040 EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//        PARM='SYS=&NAT'
//SYSPRINT DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//DDPRINT  DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//SYSOUT   DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRINT  DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRT01  DD  SYSOUT=(&REPT,IA2000M9)
//CMPRT02  DD  SYSOUT=(&REPT,IA2000MA)
//CMPRT03  DD  SYSOUT=(&REPT,IA2000MB)
//CMWKF01  DD  DSN=&HLQ8..&JOBNM..IAAP950.IAADATA,
//             DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1,&SPC2)),
//             RECFM=FB
//*            DCB=(RECFM=FB,LRECL=86,BLKSIZE=8600)
//SYSUDUMP DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P2000IA4),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
