//PAN2600D PROC DBID=045,
//         DMPS='U',
//         JCLO='*',
//         HLQ1='PDA.ANN',
//         LIB1='PROD',
//         NAT='NAT045P',
//         NATMEM='PANNSEC',
//         REPT='8',
//         REPTID1='AN2600D1',
//         REPTID2='AN2600D2',
//         PARMMEM1='AN2600D1',
//         REGN1='5M'
//*********************************************************************
//* PROCEDURE:  PAN2600D                 DATE:    04/29/04
//* PROGRAMS EXECUTED:
//*   STEP NAME    PROGRAM NAME
//* -------------  ------------
//*   REPT010      ADSP6009
//*
//*  MODIFICATIONS:
//*
//*********************************************************************
//REPT010 EXEC PGM=NATB030,REGION=&REGN1,TIME=1400,
//             PARM='SYS=&NAT,OBJIN=N'
//CMPRINT  DD  SYSOUT=&JCLO
//CMPRT01  DD  SYSOUT=(&REPT,&REPTID1)
//CMPRT02  DD  SYSOUT=(&REPT,&REPTID2)
//CMWKF01  DD  DSN=&HLQ1..PAN2600D.CIS.EXTRCT(+0),DISP=SHR
//SYSPRINT DD  SYSOUT=&DMPS
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT  DD  SYSOUT=&DMPS
//SYSUDUMP DD  SYSOUT=&DMPS
//SYSOUT   DD  SYSOUT=&JCLO
//CMSYNIN  DD  DSN=&LIB1..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(&PARMMEM1),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
