//PIA5120M PROC DBID='003',
//         DMPS='U',
//         HLQ0='PIA.ANN',
//         JCLO='*',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         SPC1='10',
//         SPC2='5',
//         REGN1='5M',
//         REPT='8'
//*
//* ----------------------------------------------------------------- * 
//* PROCEDURE:  PIA5120M                 DATE:    09/17/2014          * 
//* ----------------------------------------------------------------- * 
//* PROGRAMS EXECUTED: IAAPRSA (NATURAL)                                
//* ----------------------------------------------------------------- * 
//* GENERATE FILE FEEDS TO RMW FROM AN INPUT FILE                     *
//* INPUT:  DSN=PIA.ANN.IA.RMW.DEATH.IN                                 
//* OUTPUT: DSN=PIA.ANN.IA.RMW.DEATH.OUT -- CREATE INITIAL EMPTY FILES
//*         DSN=PIA.ANN.IA.RMW.CNTRL     -- TO BE OVERWRITTEN EACH TIME
//*         DSN=PIA.ANN.IA.RMW.TRG       -- THE JOB RUNS.
//* ----------------------------------------------------------------- * 
//*                                                                     
//* MODIFICATION HISTORY
//* 09/17/14 - JUN TINIO
//* 05/11/17 - ADD IEFBR14 TO DELETE CMWKF02 IN EXTR010 AND CHANGE THE  
//*            DISP TO NEW PER JUN T.   OS
//**********************************************************************
//DELT001  EXEC PGM=IEFBR14
//DD1      DD DSN=&HLQ0..IA.RMW.DEATH.OUT,
//            DISP=(MOD,DELETE),
//            UNIT=SYSDA,
//            SPACE=(TRK,0)
//*
//EXTR010   EXEC PGM=NATB030,REGION=&REGN1,
//          PARM='SYS=&NAT'
//SYSPRINT  DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//DDPRINT   DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRINT   DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRT01   DD SYSOUT=(&REPT,IA5120M1)
//SYSUDUMP  DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSABOUT  DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSDBOUT  DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSOUT    DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMWKF01   DD DSN=&HLQ0..IA.RMW.DEATH.IN,DISP=SHR
//CMWKF02   DD DSN=&HLQ0..IA.RMW.DEATH.OUT,
//             DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//             UNIT=SYSDA,DCB=MODLDSCB,
//             RECFM=FB
//*
//CMWKF03   DD DSN=&HLQ0..IA.RMW.DEATH.CNTRL,DISP=SHR
//CMWKF04   DD DSN=&HLQ0..IA.RMW.DEATH.TRG,DISP=SHR
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(IA5120M1),DISP=SHR          IAAP350
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
