//PTW1150W PROC DBID='003',
//         DMPS='U',
//         JOBNM='PTW1150W',
//         HLQ0='PNPD.COR.TAX',
//         JCLO='*',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD.NT2LOGON',    NT2LOGON PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='8M'
//*
//*---------------------------------------------------------------------
//*
//* MODIFICATION HISTORY
//* --------------------
//*
//*---------------------------------------------------------------------
//*   STEP    PROGRAM                     DESCRIPTION
//* --------  --------  ------------------------------------------------
//* EXTR010   TWRP0931  - EXTRACT PARTICIPANT DATA
//*---------------------------------------------------------------------
//EXTR010  EXEC PGM=NATB030,REGION=&REGN1,PARM='SYS=&NAT'
//SYSOUT    DD SYSOUT=&JCLO
//SYSUDUMP  DD SYSOUT=&DMPS
//SYSPRINT  DD SYSOUT=&JCLO
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT   DD SYSOUT=&JCLO
//CMPRINT   DD SYSOUT=&JCLO
//CMWKF01  DD DSN=&HLQ0..&JOBNM..PART.EXT,DISP=SHR
//*           DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//*           SPACE=(CYL,(&SPC1),RLSE),
//*           UNIT=SYSDA,
//*           DCB=MODLDSCB,RECFM=FB,LRECL=329
//*
//CMSYNIN   DD DSN=&LIB2..PARMLIB(&NATMEM),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(TW1150D0),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//* TWRP0931
//* FIN
//*---------------------------------------------------------------------
