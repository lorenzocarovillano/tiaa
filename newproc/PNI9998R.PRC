//PNI9998R PROC DMPS='U',                                               
//         JCLO='*',
//         HLQ1='PPM.ANN',
//         HLQ2='PPDD',
//         LIB1='PROD',
//         LIB2='PROD',
//         SPC1='25,10',
//         SPC2='15,5',
//         SPC3='5,5',
//         NAT='NAT003P',
//         NATMEM='PANNSEC'
//* ----------------------------------------------------------------- * 
//* PROGRAMS EXECUTED: SCIB9998  SCIB9999                             * 
//********************************************************************* 
//*********************************************************************
//SORT010  EXEC PGM=SORT,COND=(0,NE)
//SYSABOUT DD  SYSOUT=&DMPS
//SYSOUT   DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//SORTWK01 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC3),RLSE)
//SORTIN   DD  DSN=&HLQ1..ACIS.PPG.FILE,DISP=SHR
//SORTOUT  DD  DSN=&HLQ2..PPG.FILE.SORT,
//             DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,
//             SPACE=(CYL,(&SPC2),RLSE),
//             DCB=(RECFM=FB,LRECL=20)
//SYSIN    DD  DSN=&LIB1..PARMLIB(NI9998R1),DISP=SHR
//********************************************************************
//FRMT020  EXEC PGM=NATB030,REGION=0M,COND=(0,NE),
//         PARM='SYS=&NAT'
//CMWKF01  DD  DSN=&HLQ2..PPG.FILE.SORT,
//             DISP=SHR
//CMWKF02  DD  DSN=&HLQ2..ACIS.ORG.CODES,
//             DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,
//             SPACE=(CYL,(&SPC1),RLSE),
//             DCB=(RECFM=FB,LRECL=20)
//SYSOUT   DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMPRINT  DD  SYSOUT=&JCLO
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(NI9998R2),DISP=SHR
//*********************************************************************
//SORT030  EXEC PGM=SORT,COND=(0,NE)
//SYSABOUT DD  SYSOUT=&DMPS
//SYSOUT   DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//SORTWK01 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC3),RLSE)
//SORTIN   DD  DSN=&HLQ2..ACIS.ORG.CODES,DISP=SHR
//SORTOUT  DD  DSN=&HLQ2..ACIS.ORG.CODES.SORT,
//             DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,
//             SPACE=(CYL,(&SPC2),RLSE),
//             DCB=(RECFM=FB,LRECL=20)
//SYSIN    DD  DSN=&LIB1..PARMLIB(NI9998R3),DISP=SHR
//*********************************************************************
//UPDT040  EXEC PGM=NATB030,REGION=0M,COND=(0,NE),                      
//         PARM='SYS=&NAT'                                              
//CMWKF01  DD  DSN=&HLQ2..ACIS.ORG.CODES.SORT,
//             DISP=SHR
//SYSOUT   DD  SYSOUT=&JCLO                                             
//SYSUDUMP DD  SYSOUT=&DMPS                                             
//CMPRINT  DD  SYSOUT=&JCLO                                             
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR            
//         DD  DSN=&LIB1..PARMLIB(NI9998R4),DISP=SHR                    
