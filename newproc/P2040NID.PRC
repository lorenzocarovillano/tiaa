//P2040NID PROC DBID='003',
//         GDG1='+1',
//         HLQ1='PPM.ANN',
//         HLQ8='PPDD',
//         DMPS='U',
//         JCLO='*',
//         JOBNM='P2040NID',
//         LIB1='PROD',
//         LIB2='PROD.PARMLIB',
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='5M',
//         REPT='8',
//         SPC1='5,10',
//         SPC2='5,1'
//*                                                                     
//********************************************************************* 
//* ----------------------------------------------------------------- * 
//* PROCEDURE: P2040NID                             DATE: 08/12/03    * 
//* ----------------------------------------------------------------- * 
//* PROGRAMS EXECUTED : APPB4100 APPB4101 APPB160                     * 
//* ----------------------------------------------------------------- * 
//* MODIFICATION HISTORY                                              *
//* 07/18/06 J.CAMPBELL - ADD APPB160 FOR AUTO REPLACEMENT REGISTER   *
//* 08/20/13 KISHORE    - REDESINGED THE OMNI LOGIC                   *
//* 01/16/14 OEC DB2    - SPLIT P2040NID INTO P2040NID & P2041NID     *
//*                     - AS PART OF OEC DB2 CONVERSION WHERE         *
//*                     - P2040NID WOULD EXECUTE THE NATURAL STEPS    *
//*                     - AND P2041NID WOULD EXECUTE THE NON NATURAL  *
//*                     - STEPS.                                      *
//* 12/15/18 GENTRY     - ADDED METRICS OF CUST AND ACCT FILES        *
//*                     - C489623.                                    *
//********************************************************************* 
//EXTR010  EXEC PGM=NATB030,REGION=&REGN1,
//         PARM='SYS=&NAT,UDB=&DBID,OBJIN=N'
//DDCARD   DD  DSN=&LIB2(DBAPP&DBID),DISP=SHR
//SYSPRINT DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMPRINT  DD  SYSOUT=&JCLO
//CMWKF01  DD  DSN=&HLQ1..&JOBNM..EXTRACT.CUST.CIP(&GDG1),
//             DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,DATACLAS=DCPSEXTC,
//             SPACE=(CYL,(&SPC1),RLSE),
//             DCB=(RECFM=FB,LRECL=500,BLKSIZE=0)
//CMWKF02  DD  DSN=&HLQ1..&JOBNM..EXTRACT.ACCT.CIP(&GDG1),
//             DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,DATACLAS=DCPSEXTC,
//             SPACE=(CYL,(&SPC1),RLSE),
//             DCB=(RECFM=FB,LRECL=250,BLKSIZE=0)
//CMWKF03  DD  DSN=&HLQ8..&JOBNM..EXTRACT.REPORT,
//             DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,DATACLAS=DCPSEXTC,
//             SPACE=(CYL,(&SPC1),RLSE),
//             DCB=(RECFM=FB,LRECL=71,BLKSIZE=0)
//CMPRT01  DD  SYSOUT=&JCLO
//CMPRT02  DD  SYSOUT=&JCLO
//CMSYNIN  DD  DSN=&LIB1..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB2(P2040NI1),DISP=SHR
//*********************************************************************
//REPT020  EXEC PGM=NATB030,REGION=&REGN1,
//   PARM='SYS=&NAT'
//SORTWK01 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC2),RLSE)
//DDCARD   DD  DSN=&LIB2(DBAPP&DBID),DISP=SHR
//SYSPRINT DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMPRINT  DD  SYSOUT=&JCLO
//CMWKF01  DD  DSN=&HLQ8..&JOBNM..EXTRACT.REPORT,
//             DISP=OLD
//CMPRT01  DD  SYSOUT=(&REPT,NI2040D1)
//CMSYNIN  DD  DSN=&LIB1..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB2(P2040NI2),DISP=SHR
//*********************************************************************
//EXTR030  EXEC PGM=NATB030,REGION=&REGN1,
//         PARM='SYS=&NAT,UDB=&DBID,OBJIN=N'
//SORTWK01 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC2),RLSE)
//DDCARD   DD  DSN=&LIB2(DBAPP&DBID),DISP=SHR
//SYSPRINT DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMPRINT  DD  SYSOUT=&JCLO
//CMWKF01  DD  DSN=&HLQ1..&JOBNM..REPLREG.EXTRACT(&GDG1),
//             DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,
//             SPACE=(CYL,(&SPC1),RLSE),
//             DATACLAS=DCPSEXTC,
//             DCB=(RECFM=FB,LRECL=1046,BLKSIZE=0)
//CMPRT01  DD  SYSOUT=(&REPT,NI2040D2)
//CMSYNIN  DD  DSN=&LIB1..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB2(P2040NI3),DISP=SHR
//******************************************************************
//* COUNT RECORDS IN CUST FILE                                     *
//******************************************************************
//SORT040  EXEC PGM=SORT                                                
//SYSOUT   DD  SYSOUT=&JCLO
//SORTIN   DD  DSN=&HLQ1..&JOBNM..EXTRACT.CUST.CIP(&GDG1),              
//             DISP=SHR
//SORTOUT  DD  DSN=&HLQ1..&JOBNM..EXTRACT.CNTR.CIP(&GDG1),              
//         DISP=(,CATLG,DELETE),
//         DATACLAS=DCPSEXTC,
//         UNIT=SYSDA,SPACE=(CYL,(&SPC2),RLSE),
//         DCB=(RECFM=FB,LRECL=80)
//SYSIN    DD  DSN=&LIB1..PARMLIB(P2040NI4),DISP=SHR                    
//******************************************************************
//* COUNT RECORDS IN ACCT FILE                                     *
//******************************************************************
//SORT050  EXEC PGM=SORT                                                
//SYSOUT   DD  SYSOUT=&JCLO
//SORTIN   DD  DSN=&HLQ1..&JOBNM..EXTRACT.ACCT.CIP(&GDG1),              
//             DISP=SHR
//SORTOUT  DD  DSN=&HLQ1..&JOBNM..EXTRACT.CNTR.CIP(&GDG1),              
//             DISP=MOD
//SYSIN    DD  DSN=&LIB1..PARMLIB(P2040NI5),DISP=SHR                    
