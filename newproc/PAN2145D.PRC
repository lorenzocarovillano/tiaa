//PAN2145D PROC DBID=003,
//         DMPS='U',
//         HLQ8='PPDD',
//         JCLO='*',
//         LIB1='PROD',
//         LIB2='PROD',
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         PRGNM1='ADSP730',
//         PRGNM2='ADSP740',
//         REPT='8',
//         REPTID1='AN2145D1',
//         REPTID2='AN2145D2',
//         REPTID3='AN2145D3',
//         REPTID4='AN2145D4',
//         REPTID5='AN2145D5',
//         REPTID6='AN2145D6',
//         PARMMEM1='AN2145D1',
//         PARMMEM2='AN2145D2',
//         PARMMEM3='AN2145D3',
//         PARMMEM4='AN2145D4',
//         PARMMEM5='AN2145D5',
//         PARMMEM6='AN2145D6',
//         PARMMEM7='AN2145D7',
//         REGN1='4M',
//         SPC1='200,100',
//         SPC2='50,10'
//*********************************************************************
//* PROCEDURE:  PAN2145D                 DATE:    04/23/04
//* PROGRAMS EXECUTED:
//*   STEP NAME    PROGRAM NAME                                       *
//* -------------  ------------                                       *
//*   UNDO010      IEFBR14                                            *
//*   REPT020      ADSP730                                            *
//*   SORT030      SORT                                               *
//*   REPT040      ADSP1000, ADSP1005
//*   REPT050      ADSP740
//*   SORT060      SORT
//*   REPT070      ADSP1010 ADSP1030 ADSP1060
//*   UPDT080      ADSP745                                            *
//*********************************************************************
//* UNDO010 WILL DELETE THE TEMPORARY DATASETS USED BY THIS JOB
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
//UNDO010 EXEC PGM=IEFBR14,COND=(0,NE)
//WKFILE1  DD DSN=&HLQ8..&PRGNM1..TRANS.TEMP1,
//         DISP=(MOD,DELETE),SPACE=(CYL,(&SPC1),RLSE),UNIT=SYSDA
//WKFILE2  DD DSN=&HLQ8..&PRGNM2..TRANS.TEMP1,
//         DISP=(MOD,DELETE),SPACE=(CYL,(&SPC1),RLSE),UNIT=SYSDA
//WKFILE3  DD DSN=&HLQ8..&PRGNM1..EXTRACT.FILE.TEMP1,
//         DISP=(MOD,DELETE),SPACE=(CYL,(&SPC1),RLSE),UNIT=SYSDA
//WKFILE4  DD DSN=&HLQ8..&PRGNM2..EXTRACT.FILE.TEMP1,
//         DISP=(MOD,DELETE),SPACE=(CYL,(&SPC1),RLSE),UNIT=SYSDA
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
//* REPT020 - ADSP730 WILL BE  EXECUTED IN THIS STEP.
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
//REPT020  EXEC PGM=NATB030,REGION=&REGN1,
//    PARM='SYS=&NAT'
//SYSPRINT DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SORTWK01 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC2),RLSE)
//CMWKF01  DD  DSN=&HLQ8..&PRGNM1..TRANS.TEMP1,
//             DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,
//             SPACE=(CYL,(&SPC1),RLSE),
//*//             DCB=RECFM=FB
//             DCB=(RECFM=FB)
//SYSOUT   DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//CMPRT01  DD  SYSOUT=(&REPT,&REPTID1)
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB2..PARMLIB(&PARMMEM1),DISP=SHR
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
//* SORT030  WILL READ PPDD.ADSP730.TRANS.TEMP1  SORT THE DATA AND
//*          WRITE SORTED DATA ON PPDD.ADSP730.EXTRACT.FILE.TEMP1.
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
//SORT030  EXEC PGM=SORT,COND=(0,NE)
//SORTIN   DD  DSN=&HLQ8..&PRGNM1..TRANS.TEMP1,DISP=SHR
//SORTWK01 DD  UNIT=SYSDA,
//             SPACE=(CYL,(&SPC2),RLSE)
//SORTOUT  DD  DSN=&HLQ8..&PRGNM1..EXTRACT.FILE.TEMP1,
//             DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,
//*//             DCB=RECFM=FB,
//             DCB=(RECFM=FB),
//             SPACE=(CYL,(&SPC1),RLSE)
//SYSOUT   DD  SYSOUT=&JCLO
//SYSIN    DD  DSN=&LIB2..PARMLIB(&PARMMEM2),DISP=SHR
//* SORT CONTROL CARD:
//* SORT BY ADC-LST-ACTVTY-DTE, ADP-EFFCTV-DTE
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
//*  PROGRAM ADSP1000 ADSP1005 WILL BE EXECUTED IN THIS STEP.
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
//REPT040  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//    PARM='SYS=&NAT'
//SYSPRINT DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SORTWK01 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC2),RLSE)
//CMWKF01  DD  DSN=&HLQ8..&PRGNM1..EXTRACT.FILE.TEMP1,DISP=SHR
//SYSOUT   DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//CMPRT01  DD  SYSOUT=(&REPT,&REPTID2)
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB2..PARMLIB(&PARMMEM3),DISP=SHR
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
//* PROGRAM ADSP740 WILL BE EXECUTED IN THIS STEP
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
//REPT050  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//    PARM='SYS=&NAT'
//SYSPRINT DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SORTWK01 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC2),RLSE)
//CMWKF01  DD  DSN=&HLQ8..&PRGNM2..TRANS.TEMP1,
//             DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,
//             SPACE=(CYL,(&SPC1),RLSE),
//*//             DCB=RECFM=FB
//             DCB=(RECFM=FB)
//SYSOUT   DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//CMPRT01  DD  SYSOUT=(&REPT,&REPTID3)
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB2..PARMLIB(&PARMMEM4),DISP=SHR
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
//* SORT060  - WILL READ PPDD.ADSP740.TRANS.TEMP1 - SORT THE DATA AND
//*          - WRITE SORTED DATA ON PPDD.ADSP740.EXTRACT.FILE1.
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
//SORT060  EXEC PGM=SORT,COND=(0,NE)
//SORTIN   DD  DSN=&HLQ8..&PRGNM2..TRANS.TEMP1,DISP=SHR
//SORTWK01 DD  UNIT=SYSDA,
//             SPACE=(CYL,(&SPC1),RLSE)
//SORTOUT  DD  DSN=&HLQ8..&PRGNM2..EXTRACT.FILE.TEMP1,
//             DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,
//*//             DCB=RECFM=FB,
//             DCB=(RECFM=FB),
//             SPACE=(CYL,(&SPC1),RLSE)
//SYSOUT   DD  SYSOUT=&JCLO
//SYSIN    DD  DSN=&LIB2..PARMLIB(&PARMMEM5),DISP=SHR
//* SORT CONTROL CARD:
//* SORT BY ADI-LST-ACTVTY-DTE, ADI-FRST-PYMNT-DUE-DTE
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
//* REPT070  WILL READ PPDD.ADSP740.EXTRACT.FILE.TEMP1   AND EXECUTE
//*          PROGRAM ADSP1010 ADSP1030 ADSP1060
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
//REPT070  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//    PARM='SYS=&NAT'
//SYSPRINT DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SORTWK01 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC2),RLSE)
//CMWKF01  DD  DSN=&HLQ8..&PRGNM2..EXTRACT.FILE.TEMP1,DISP=SHR
//SYSOUT   DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//CMPRT01  DD  SYSOUT=(&REPT,&REPTID4)
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB2..PARMLIB(&PARMMEM6),DISP=SHR
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
//* UPDT080    ADSP745 WILL BE EXECUTED IN THIS STEP AND WILL UPDATE    
//*            ADS-FNL-PRM-ACCUM-DTE ON ADS-CONTROL-RECORD
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
//UPDT080  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//    PARM='SYS=&NAT'
//SYSPRINT DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSOUT   DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//CMPRT02  DD  SYSOUT=(&REPT,&REPTID5)
//CMPRT03  DD  SYSOUT=(&REPT,&REPTID6)
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB2..PARMLIB(&PARMMEM7),DISP=SHR
