//*-------------------------------------------------------------------- 
//P3050CPM PROC DBID=003,                 ADABAS DB ENVIRONMENT         
//        APPHLQ=PPDT.P3050CPM.CPS,       PROJ TAPE HI-LEVEL QUAL       
//        NLOGLIB=PROD.NT2LOGON.PARMLIB,  NAT2 LOGON STMT PARM LIB      
//        NATLOGCC=PANNSEC,               NAT2 LOGON STMT MBR NAME      
//        NATPROF=NAT003P,                NAT2 PROFILE (SYS=)           
//        PARMLIB=PROD.PARMLIB,           PARM STMT LIBRARY             
//        SYSOUT='*',                     SYSOUT= OPERAND FOR MSGS      
//        SYSMSGS='U'                     SYSOUT= OPERAND FOR DEBUG     
//*                                                                     
//*-------------------------------------------------------------------- 
//*THIS JOB IS RESTARTABLE FROM ANY STEP/ RERUNABLE FROM 1ST STEP       
//*-------------------------------------------------------------------- 
//*  REMOVES AGED RECORDS (PURGE) FROM 003/189 FCP-CONS-PDAFILE         
//*  READS "FCP-CONS-PDAFILE" FILE RECORDS AND DELETES PAYMENTS         
//*  WITH PAYMENT DATES OLDER THEN A DATE STORED ON REFERENCE FILE      
//*  REFERENCE TABLE "CPARM" - "P3050CPM". THIS PARAMETER IS MAINTAINED 
//*  BY CONSOLIDATED PAYMENT SYSTEMS AND WILL BE SET ORIGINALLY         
//*  TO 6 (MONTHS). A MINIMUM RETENTION OF 3 MONTHS                     
//*  IS USED AS A DEFAULT (FCPP926 - OVERRIDE) IN THE EVENT             
//*  THE TABLE ENTRY IS UN-AVAILIABLE OR INCORRECTLY SET.               
//*  RECORDS SELECTED FOR DELETION ARE WRITTEN TO A GDG CARTRIDGE       
//*  WHICH CAPTURES ALL DATA ELEMENTS FROM                              
//*  "FCP-CONS-PDAFILE" (DB/FILE 003/189).                              
//*-------------------------------------------------------------------- 
//*  NATURAL PROGRAM: "FCPP926".                                        
//*-------------------------------------------------------------------- 
//DELT010  EXEC PGM=NATB030,REGION=9M,COND=(0,NE),                      
//             PARM='SYS=&NATPROF'                                      
//SYSOUT   DD  SYSOUT=&SYSOUT                                           
//SYSPRINT DD  SYSOUT=&SYSOUT                                           
//SYSUDUMP DD  SYSOUT=&SYSMSGS                                          
//DDPRINT  DD  SYSOUT=&SYSOUT                                           
//DDCARD   DD  DSN=&PARMLIB(DBAPP&DBID),DISP=SHR                        
//         DD  DSN=&PARMLIB(P3000CP1),DISP=SHR                          
//CMPRINT  DD  SYSOUT=&SYSOUT                                           
//CMPRT01  DD  SYSOUT=&SYSOUT                                           
//CMWKF01  DD  DSN=&APPHLQ..FLE189.ARC.DATA(+1),                        
//             DISP=(,CATLG,DELETE),                                    
//*            DCB=(MODLDSCB,LRECL=10479,RECFM=FB),                     
//             DCB=(MODLDSCB,LRECL=10484,RECFM=FB),      PIN EXP        
//             UNIT=CARTV,BUFNO=50,                                     
//             VOL=(,,,99)                                              
//CMWKF02  DD  DSN=&APPHLQ..FLE189.ARC.ISNS(+1),                        
//             DISP=(,CATLG,DELETE),                                    
//             DCB=(MODLDSCB,LRECL=4,RECFM=FB),                         
//             UNIT=CARTV,BUFNO=25,                                     
//             VOL=(,,,99)                                              
//CMSYNIN  DD  DSN=&NLOGLIB(&NATLOGCC),DISP=SHR                         
//         DD  DSN=&PARMLIB(P3050CPA),DISP=SHR                          
//*FCPP926                                                              
