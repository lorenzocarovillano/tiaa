//PCP1710D PROC LIB1='PROD',        COMMON LIBRARY HLQ
//         HLQ0='PCPS.ANN',         CORPORATE DASD HLQ
//         JOBNM='PCP1710D',        2ND HLQ FOR FILES CREATED IN PROC
//         JOBNM1='PCP1725D',       2ND HLQ FOR FILES FROM PRIOR PROCS
//         JOBNM2='PCP1700D',       2ND HLQ FOR FILES FROM PRIOR PROCS
//         UNIT1=SYSDA,             UNIT PARM FOR DASD OUTPUT
//         RPTID='CP1710D',         MOBIUS REPORT ID'S
//         DMPS=*,                  DUMP OUTPUT
//         JCLO00=*,                JOB OUTPUT
//         REPT00=*,                REPORTS
//         REPT01=8,                REPORTS
//         SPC1=25,                 FILE1 - PRIMARY SPACE ALLOCATION
//         SPC2=50,                 FILE1 - SECONDARY SPACE ALLOCATION
//         DBID=003,                DATABASE ID
//         REGN1='5M',              REGION SIZE
//         GDG0='0',                CURRENT GDG FILE
//         GDG1='+1',               NEW GDG FILE CREATED
//         NATVERS=NATB030,         NATURAL VERSION
//         NAT=NAT003P,             NATURAL PROFILE
//         PARMLIB=PARMLIB,         LLQ COMMON PARM LIBRARY
//         NATLOG=NT2LOGON.PARMLIB, 2 LVL QUAL NATURAL LOGON LIBRARY
//         NATMEM=PANNSEC           NATURAL LOGON MEMBER
//*
//*--------------------------------------------------------------------*
//*                                                                    *
//* PROCEDURE:  PCP1710D                           DATE:    10/15/2002 *
//*                                                                    *
//*           CONSOLIDATED PAYMENT SYSTEM / OPEN INVESTMENTS           *
//*                                                                    *
//*                     UPDATE DATA BASE PAYMENTS                      *
//*                                                                    *
//*  STEP    PROGRAM   PARMS                  COMMENTS                 *
//* -------  -------  --------  -------------------------------------- *
//* UPDT010  CPOP160  PCP17101  UPDATE DATA BASE PAYMENTS USING        *
//*                             PAYMENT FILE FROM PCP1705D, AND        *
//*                             OTHER, CAN / STOP FILES FROM PCP1700D  *
//* CREA020  IEBGENER           COPY ALL PAYMENTS TO GDG DATA SET      *
//*                                                                    *
//*--------------------------------------------------------------------*
//* UPDT010  -  UPDATE DATA BASE PAYMENTS USING PAYMENT FILE FROM      *
//*             PCP1705D; AND CAN / STOP, OTHER FILES FROM PCP1700D    *
//*             NATURAL PROGRAM CPOP160                                *
//*------------------------------------------------------------------***
//*
//UPDT010  EXEC PGM=&NATVERS,REGION=&REGN1,COND=(0,NE),
//         PARM='IM=D,SYS=&NAT'
//DDCARD   DD DSN=&LIB1..&PARMLIB(DBAPP&DBID),DISP=SHR
//SYSOUT   DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSPRINT DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSUDUMP DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//DDPRINT  DD SYSOUT=&JCLO00,OUTPUT=*.OUTVDR
//CMPRINT  DD SYSOUT=&REPT00,OUTPUT=*.OUTVDR
//CMPRT01  DD SYSOUT=(&REPT01,&RPTID.1),
//         DCB=(RECFM=VB,LRECL=136),OUTPUT=*.OUTVDR
//CMPRT10  DD SYSOUT=(&REPT01,&RPTID.2),
//         DCB=(RECFM=VB,LRECL=136),OUTPUT=*.OUTVDR
//CMPRT15  DD SYSOUT=&REPT00,OUTPUT=*.OUTVDR,
//         DCB=(RECFM=VB,LRECL=136)
//CMWKF01  DD DSN=&HLQ0..&JOBNM1..EXTRACT(&GDG0),DISP=SHR
//CMWKF02  DD DSN=&HLQ0..&JOBNM2..OTHER(&GDG0),DISP=SHR
//CMWKF03  DD DSN=&HLQ0..&JOBNM..PAYMENT.ALL,
//            DISP=SHR
//CMSYNIN  DD DSN=&LIB1..&NATLOG(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..&PARMLIB(PCP17101),DISP=SHR
//*
//*------------------------------------------------------------------***
//* CREA020 - COPY ALL PAYMENTS TO GENERATION DATA SET                 *
//*------------------------------------------------------------------***
//*
//CREA020 EXEC PGM=IEBGENER,REGION=&REGN1,COND=(0,NE)
//SYSOUT   DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSPRINT DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSUT1   DD DSN=&HLQ0..&JOBNM..PAYMENT.ALL,DISP=SHR
//SYSUT2   DD DSN=&HLQ0..&JOBNM..PYMNTS(&GDG1),
//            DISP=(,CATLG,DELETE),
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),UNIT=&UNIT1
//SYSIN    DD DUMMY
//*
