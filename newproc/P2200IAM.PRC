//P2200IAM PROC DBID='003',
//         DMPS='U',
//         GDG1='+1',
//         HLQ0='PIA.ANN',
//         JCLO='*',
//         JOBNM='P2200IAM',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='9M',
//         REPT='8',
//         HLQDR=PPDT          DO NOT CHANGE HLQ FOR D/R USE
//*
//* ----------------------------------------------------------------- *
//* PROCEDURE:  P2200IAM                 DATE:    01/22/96            *
//* ----------------------------------------------------------------- *
//* PROGRAMS EXECUTED:  NT2B030 (IAAP300)                             *
//*
//* ----------------------------------------------------------------- *
//*                                                                     
//* MODIFICATION HISTORY
//* 04/05/02 - OIA JCL STANDARDS - PHIL STEINHAUSER
//* 04/05/03 - CHANGED FOLLOWING LRECL
//*            RECFM=FB,LRECL=315 TO LRECL=346
//*                                                                     
//* 10/19/06 - CHANGED FOLLOWING LRECL                                  
//*            RECFM=FB,LRECL=346 TO LRECL=354                          
//* 05/30/08 - REMOVED LRECL FOR ROTH 403(B) / 401(K)                   
//*                                                                     
//**********************************************************************
//*
//EXTR010 EXEC PGM=NATB030,REGION=&REGN1,
//        PARM='SYS=&NAT'
//SYSPRINT DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//DDPRINT  DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRINT  DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//SYSOUT   DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRT01  DD  SYSOUT=(&REPT,IA2200M1)
//CMPRT02  DD  SYSOUT=(&REPT,IA2200M2)
//CMWKF01  DD  DSN=&HLQDR..PNA.IA.FUND.DETAIL.FINAL(&GDG1),
//             DISP=(NEW,CATLG,DELETE),                                 
//             DCB=(MODLDSCB,RECFM=VB),UNIT=CARTV                       
//CMWKF02  DD  DSN=&HLQ0..&JOBNM..IAAP300.IATRNS.FNL(&GDG1),
//             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,               
//             UNIT=SYSDA,SPACE=(CYL,(535,110),RLSE),
//*//             DCB=MODLDSCB,RECFM=FB
//             DCB=(MODLDSCB,RECFM=FB)
//*            DCB=MODLDSCB,RECFM=FB,LRECL=354  REMOVED LRECL 5/08
//*            DCB=MODLDSCB,RECFM=FB,LRECL=346
//*            RECFM=FB,LRECL=315
//SYSUDUMP DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSABOUT DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSDBOUT DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPF&DBID),DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P2200IA1),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
