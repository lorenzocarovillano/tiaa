//PTW1003R PROC DBID='003',
//         DMPS='U',
//         JCLO='*',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='8M',
//         REPID1=',TW1003R1',
//         REPT='8'
//*
//*---------------------------------------------------------------------
//*                                                                     
//* Modification History
//* 01/30/18 - Cognizant- Initial Version
//*                                                                     
//**********************************************************************
//* UPDT010 - Update the Moore Hold Indicator after the 1042-S forms
//*           ROLLUP to reduce the E-DELIVERY and PAPER Bypass cases
//*           Natural Program "TWRP5800".
//*---------------------------------------------------------------------
//UPDT010  EXEC PGM=NATB030,REGION=&REGN1,
//         PARM='SYS=&NAT'
//SYSOUT    DD SYSOUT=&JCLO
//SYSUDUMP  DD SYSOUT=&DMPS
//SYSPRINT  DD SYSOUT=&JCLO
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT   DD SYSOUT=&JCLO
//CMPRINT   DD SYSOUT=&JCLO
//CMPRT01   DD SYSOUT=(&REPT&REPID1)
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(T1003TW1),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(T1003TW2),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//* TWRP5800
