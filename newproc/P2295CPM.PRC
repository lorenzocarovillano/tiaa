//P2295CPM PROC DBID='003',
//         DMPS='U',
//         GDG0='+0',
//         HLQ0='PIA.ANN',
//         JCLO='*',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='9M',
//         REPT='8'
//*
//*-------------------------------------------------------------------*
//* PROCEDURE:  P2295CPM                 DATE:  05/15/96              *
//* ------------------------------------------------------------------*
//* UPDT010 - UPDATES ADABAS NAME & ADDRESS FILE (197) WITH "AP"      *
//*           DATA.  TO RESTART THE "ETID" NATURAL SESSION PARAMETER  *
//*           HAS TO BE SPECIFIED.  IT HAS TO BE "ADAPUPDT".          *
//*           NATURAL PROGRAM "FCPP829".                              *
//*-------------------------------------------------------------------*
//*                                                                     
//* MODIFICATION HISTORY
//* 04/23/02 - OIA JCL STANDARDS - PHIL STEINHAUSER
//*                                                                     
//**********************************************************************
//*
//UPDT010 EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//        PARM='IM=D,SYS=&NAT,ETID=ADAPUPDT'
//SYSPRINT DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//DDPRINT  DD SYSOUT=&JCLO
//CMPRINT  DD SYSOUT=&JCLO
//CMPRT15  DD SYSOUT=&REPT
//CMWKF01  DD DSN=&HLQ0..TX197S(&GDG0),DISP=SHR,
//            BUFNO=50,UNIT=SYSDA
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(P2295CP1),DISP=SHR
//* P2295CPM
