//PNA2030M PROC DBID='015',                                             
//         DMPS='U',                                                    
//*        HLQ1='PNA',                                                  
//         JCLO='*',                                                    
//         LIB1='PROD',             PARMLIB                             
//         LIB2='PROD',             NT2LOGON.PARMLIB                    
//         NAT='NAT015P',                                               
//         NATMEM='PCRPSEC',                                            
//         REPT='8',                                                    
//         REGN1='128M'                                                 
//**********************************************************************
//* MODIFICATION HISTORY                                                
//**********************************************************************
//*   NASB988:  PRINT ADDRESS CHANGES FOR W0570000 THRU W0589999        
//**********************************************************************
//REPT010  EXEC PGM=NATB030,REGION=&REGN1,                              
//         PARM='SYS=&NAT'                                              
//SYSPRINT DD SYSOUT=&JCLO                                              
//SYSOUT   DD SYSOUT=&JCLO                                              
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                   
//DDPRINT  DD SYSOUT=&JCLO                                              
//CMPRINT  DD SYSOUT=&JCLO                                              
//CMPRT01  DD SYSOUT=(&REPT,NA2030M1)                                   
//SYSUDUMP DD SYSOUT=&DMPS                                              
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR             
//         DD DSN=&LIB1..PARMLIB(NA2030M1),DISP=SHR                     
