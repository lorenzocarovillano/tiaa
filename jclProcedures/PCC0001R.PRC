//*********************************************************************
//*  PCC0001R  -   MODIFY CURRENT PROCESSING ENVIRONMENT SETTING
//*********************************************************************
//PCC0001R PROC REGN1=8M,
//         NATLOGON=PANNSEC,
//         NAT=NATB030,
//         SYS=NAT045P,
//         DBID=045,
//         LIB1=PROD,
//         JCLO=*,
//         DMPS=U,
//         PARMMEM1=PC0001PD
//*
//*
//*********************************************************************
//*    PCCB0001 - EXTRACT E-MAIL DATA / CREATE ORACLE CONTROL RECORD
//*               CREATE DIRECT:CONNECT CONTROL CARDS FOR COPY TO DCS
//*********************************************************************
//UPDTENV  EXEC PGM=&NAT,REGION=&REGN1,
//         PARM='SYS=&SYS,WH=ON'
//SYSPRINT DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMSYNIN  DD  DSN=&LIB1..NT2LOGON.PARMLIB(&NATLOGON),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(PC0001R1),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(&PARMMEM1),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//*
