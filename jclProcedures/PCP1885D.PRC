//PCP1885D PROC LIB1='PROD',        COMMON LIBRARY HLQ
//         RPTID='CP1885D',         MOBIUS REPORT ID'S
//         REPT00=*,                REPORTS
//         REPT01=8,                REPORTS
//         DBID=003,                DATABASE ID
//         GDG0=0,                  EXISTING GDG
//         HLQ1=POMPY,              DASD HLQ
//         REGN1='5M',              REGION SIZE
//         NATVERS=NATB030,         NATURAL VERSION
//         NAT=NAT003P,             NATURAL PROFILE
//         PARMLIB=PARMLIB,         LLQ COMMON PARM LIBRARY
//         NATLOG=NT2LOGON.PARMLIB, 2 LVL QUAL NATURAL LOGON LIBRARY
//         NATMEM=PANNSEC           NATURAL LOGON MEMBER
//*--------------------------------------------------------------------*
//*                                                                    *
//* PROCEDURE:  PCP1885D                           DATE:    05/12/2011 *
//*                                                                    *
//*           CONSOLIDATED PAYMENT SYSTEM / OPEN INVESTMENTS           *
//*                                                                    *
//*           CPS - STATISTICAL REPORTS (PROLINE)                      *
//*                                                                    *
//*  STEP    PROGRAM   PARMS                  COMMENTS                 *
//* -------  -------  --------  -------------------------------------- *
//*                                                                    *
//* UPDATE10 CPVP750   UPDATE PAYMENT FILE FOR STOP OR CANCEL STATUS   *
//*                                                                    *
//*
//UPDATE10 EXEC PGM=&NATVERS,REGION=&REGN1,COND=(0,NE),
//             PARM='SYS=&NAT'
//DDCARD   DD  DISP=SHR,DSN=&LIB1..&PARMLIB(DBAPP&DBID)
//DDPRINT  DD SYSOUT=&REPT00,OUTPUT=*.OUTVDR
//CMPRT01  DD SYSOUT=(&REPT01,&RPTID.2),
//         DCB=(RECFM=VB,LRECL=137),OUTPUT=*.OUTVDR
//CMPRINT  DD SYSOUT=(&REPT01,&RPTID.1),
//         DCB=(RECFM=VB,LRECL=137),OUTPUT=*.OUTVDR
//CMWKF01  DD  DISP=SHR,
//             DSN=&HLQ1..PMTP.DELCPS(&GDG0)
//CMSYNIN  DD  DISP=SHR,DSN=&LIB1..&NATLOG(&NATMEM)
//         DD  DISP=SHR,DSN=&LIB1..&PARMLIB(PCP1885A)
//*
//* *********************************                                  *
//* UPDT020  CPVP760   UPDATE LEDGER FILE FOR REVERSALS OF SWING ACCTS *
//* (THIS STEP WAS DELETED ON 10/2015 AS PART OF COR/NAS SUNSET)
//*
