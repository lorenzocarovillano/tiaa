//P2360CPM PROC DBID='003',
//         DMPS='U',
//         GDG1='+1',
//         HLQ0='PIA.ANN',
//         HLQ7='PPDT',
//         HLQ8='PPDD',
//         JCLO='*',
//         JOBNM='P2360CPM',
//         JOBNM1='P2210CPM',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='9M',
//         REPT='8',
//         SPC1='500,50',
//         SPC2='800,75',
//         SPC3='100,50',
//         SPC4='400,90',
//         SPC5='50,50',
//         UNIT1='CARTV'
//*
//*-------------------------------------------------------------------*
//* PROCEDURE:  P2360CPM                 DATE:  02/29/96              *
//*-------------------------------------------------------------------*
//* MODIFICATION HISTORY
//* 04/24/02 - OIA JCL STANDARDS - JOHN VIOLA
//*                                                                     
//**********************************************************************
//* SORT010 - SORT THE "IA" TAX FILE BY PAYMENT PROCESS SEQUENCE      *
//*           NUMBER.                                                 *
//*-------------------------------------------------------------------*
//* FRMT020 - REFORMATS 'CPS' FORMAT TO TAX REPORTING FORMAT.         *
//*           INPUT PARM REFLECTS THE ORIGIN CODE:                    *
//*           AP   - NEW IA.                                          *
//*           DCSS - DEATH CLAIMS, SINGLE SUM.                        *
//*           DS   - DAILY SETTLEMENTS.                               *
//*           MS   - MONTHLY SETTLEMENTS.                             *
//*           NATURAL PROGRAM FCPP396.                                *
//*-------------------------------------------------------------------*
//* FRMT030 - CREATES A "CPS" TAX INTERFACE FILE TO THE "NPD" SYSTEM. *
//*           NATURAL PROGRAM FCPP397.                                *
//*-------------------------------------------------------------------*
//* SORT040 - SORTS THE "AP" TAX REPORTING FORMAT FILE CREATED        *
//*           EARLIER IN STEP "FRMT020" BY : COMPANY (DESCENDING) /   *
//*           ORIGIN / PEND / CREF # / PPCN # / SEQ #.                *
//*           SELECTS U.S. CITIZENS & RESIDENT ALIENS ONLY.           *
//*-------------------------------------------------------------------*
//* REPT050 - PRODUCES REPORTS ABOUT U.S. CITIZENS & RESIDENT ALIENS. *
//*           REP 1 - THE FEDERAL TAX WITHOLDING REGISTER.            *
//*                   (EVERY INSTALLMENT FOR "DC" SETTLEMENT).        *
//*           REP 2 - ALL, U.S. CITIZENS AND RESIDENT ALIENS.         *
//*                   (PER PAYEE FOR "DC" SETTLEMENT).                *
//*           REP 3 - TAX DATA INTERFACE SUMMARY CONTROL REPORT.      *
//*           NATURAL PROGRAM FCPP393                                 *
//*-------------------------------------------------------------------*
//* SORT060 - SORTS THE "AP" TAX REPORTING FORMAT FILE CREATED        *
//*           EARLIER IN STEP "FRMT020" BY : COMPANY (DESCENDING) /   *
//*           ORIGIN / PEND / CREF # / PPCN # / SEQ #.                *
//*           SELECTS NON RESIDENT ALIENS ONLY.                       *
//*-------------------------------------------------------------------*
//* REPT070 - PRODUCES REPORTS OF FEDERAL TAXES FOR NON RESIDENT      *
//*           ALIENS.                                                 *
//*           REP 1 - TAX WITHOLDING REGISTER - NON RESIDENT ALIENS.  *
//*           REP 2 - TAX WITHOLDING REGISTER - NON RESIDENT ALIENS   *
//*                   (DETAIL LISTING).                               *
//*           NATURAL PROGRAM FCPP394.                                *
//*-------------------------------------------------------------------*
//* SORT080 - SORTS THE "AP" TAX REPORTING FORMAT FILE CREATED        *
//*           EARLIER IN STEP "FRMT020" BY : COMPANY (DESCENDING) /   *
//*           ORIGIN / PEND / CREF # / PPCN # / SEQ #.                *
//*           SELECTS U.S. CITIZENS & RESIDENT ALIENS WITH STATE OR   *
//*           LOCAL TAXES DUE (GREATER THAN ZERO).                    *
//*-------------------------------------------------------------------*
//* REPT090 - PRODUCES THE STATE / LOCALITY TAX WITHOLDING REGISTER   *
//*           FOR U.S. CITIZENS AND RESIDENT ALIENS.                  *
//*           REP 1 - STATE / LOCALITY TAX WITHOLDING REGISTER.       *
//*           NATURAL PROGRAM FCPP395.                                *
//*-------------------------------------------------------------------*
//* SORT100 - SORTES THE "AP" TAX INTERFACE FILE TO THE "NPD" SYSTEM  *
//*           BY : SOCIAL SECURITY / COMPANY CODE (DESCENDING) /      *
//*           PPCN/CREF NUMBER.  SEPARATES THE DATA INTO 5 DIFFERENT  *
//*           "GDG" DATA SETS :                                       *
//*  FILE 1 - SOCIAL SECURITY NUMBERS "000-00-0000" TO "111-19-9999"  *
//*  FILE 2 -   "       "        "    "111-20-0000" TO "199-99-9999"  *
//*  FILE 3 -   "       "        "    "200-00-0000" TO "333-49-9999"  *
//*  FILE 4 -   "       "        "    "333-50-0000" TO "459-99-9999"  *
//*  FILE 5 -   "       "        "    "460-00-0000" TO "999-99-9999"  *
//*-------------------------------------------------------------------*
//*
//*-------------------------------------------------------------------*
//* SORT010 - SORT THE "AP" TAX FILE BY PAYMENT PROCESS SEQUENCE      *
//*           NUMBER.                                                 *
//*-------------------------------------------------------------------*
//SORT010 EXEC PGM=SORT,REGION=&REGN1
//SYSOUT   DD SYSOUT=&JCLO
//SYSDUMP  DD SYSOUT=&DMPS
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE,,ROUND),BUFNO=15
//SORTWK02 DD UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE,,ROUND),BUFNO=15
//SORTWK03 DD UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE,,ROUND),BUFNO=15
//SORTWK04 DD UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE,,ROUND),BUFNO=15
//SORTWK05 DD UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE,,ROUND),BUFNO=15
//SORTWK06 DD UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE,,ROUND),BUFNO=15
//SYSPRINT DD SYSOUT=&JCLO
//SORTIN   DD DSN=&HLQ0..&JOBNM1..TAX.S465,DISP=SHR,UNIT=SYSDA
//SORTOUT  DD DSN=&HLQ8..&JOBNM..IAREXT,UNIT=(SYSDA,3),
//            SPACE=(CYL,(&SPC2),RLSE),DISP=(NEW,CATLG,DELETE),
//            DCB=(RECFM=VB,LRECL=5250)
//SYSIN    DD DSN=&LIB1..PARMLIB(P2360CP1),DISP=SHR
//* SORT FIELDS=(68,9,CH,A)
//* OPTION DSPSIZE=0,HIPRMAX=0
//* KEY START IN COLUMN 64 + 4 BYTES (FOR VARIABLE LENGTH RECORDS) = 68
//* IT IS 9 CHARACTERS LONG.  THE FIELD NAME IS "PYMNT-PRCSS-SEQ-NBR".
//*
//*-------------------------------------------------------------------*
//* FRMT020 - REFORMATS "CPS" FORMAT TO TAX REPORTING FORMAT.         *
//*           INPUT PARM REFLECTS THE ORIGIN CODE:                    *
//*           AP   - NEW IA.                                          *
//*           DCSS - DEATH CLAIMS, SINGLE SUM.                        *
//*           DS   - DAILY SETTLEMENTS.                               *
//*           MS   - MONTHLY SETTLEMENTS.                             *
//*           NATURAL PROGRAM FCPP396.                                *
//*-------------------------------------------------------------------*
//FRMT020 EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//            PARM='SYS=&NAT'
//SYSOUT   DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//SYSPRINT DD SYSOUT=&JCLO
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT  DD SYSOUT=&JCLO
//CMPRINT  DD SYSOUT=&JCLO
//CMPRT01  DD SYSOUT=&REPT
//CMPRT02  DD SYSOUT=&REPT
//CMPRT03  DD SYSOUT=&REPT
//CMPRT15  DD SYSOUT=&REPT
//CMWKF01  DD DSN=&HLQ8..&JOBNM..IAREXT,DISP=SHR
//CMWKF02  DD DSN=&HLQ7..&JOBNM..IARTAX(&GDG1),
//            DISP=(NEW,CATLG,DELETE),UNIT=&UNIT1,
//*           DCB=(MODLDSCB,RECFM=LS,LRECL=874)
//            DCB=(MODLDSCB,RECFM=LS,LRECL=879)
//CMWKF15  DD DSN=&HLQ0..&JOBNM1..CONTROLS.S472,DISP=SHR,UNIT=SYSDA
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(P2360CP2),DISP=SHR
//* FCPP396 AP
//*-------------------------------------------------------------------*
//* FRMT030 - CREATES A "CPS" TAX INTERFACE FILE TO THE "NPD" SYSTEM. *
//*           NATURAL PROGRAM FCPP397                                 *
//*-------------------------------------------------------------------*
//FRMT030 EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//            PARM='SYS=&NAT'
//SYSOUT   DD SYSOUT=&JCLO
//SYSPRINT DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT  DD SYSOUT=&JCLO
//CMPRINT  DD SYSOUT=&JCLO
//CMPRT01  DD SYSOUT=&REPT
//CMPRT02  DD SYSOUT=&REPT
//CMWKF01  DD DSN=&HLQ7..&JOBNM..IARTAX(&GDG1),DISP=SHR,UNIT=&UNIT1
//CMWKF02  DD DSN=&HLQ7..&JOBNM..IARNPD(&GDG1),
//            UNIT=&UNIT1,DISP=(NEW,CATLG,DELETE),
//            DCB=(MODLDSCB,RECFM=LS,LRECL=450)
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(P2360CP3),DISP=SHR
//* FCPP397
//*-------------------------------------------------------------------*
//* SORT040 - SORTS THE "AP" TAX REPORTING FORMAT FILE CREATED        *
//*           EARLIER IN STEP "FRMT020" BY : COMPANY (DESCENDING) /   *
//*           ORIGIN / PEND / CREF # / PPCN # / SEQ #.                *
//*           SELECTS U.S. CITIZENS & RESIDENT ALIENS ONLY.           *
//*-------------------------------------------------------------------*
//SORT040 EXEC PGM=SORT,REGION=&REGN1,COND=(0,NE)
//SYSOUT   DD SYSOUT=&JCLO
//SYSPRINT DD SYSOUT=&JCLO
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,(&SPC3),RLSE,,ROUND),BUFNO=15
//SORTWK02 DD UNIT=SYSDA,SPACE=(CYL,(&SPC3),RLSE,,ROUND),BUFNO=15
//SORTWK03 DD UNIT=SYSDA,SPACE=(CYL,(&SPC3),RLSE,,ROUND),BUFNO=15
//SORTWK04 DD UNIT=SYSDA,SPACE=(CYL,(&SPC3),RLSE,,ROUND),BUFNO=15
//SORTWK05 DD UNIT=SYSDA,SPACE=(CYL,(&SPC3),RLSE,,ROUND),BUFNO=15
//SORTIN   DD DSN=&HLQ7..&JOBNM..IARTAX(&GDG1),DISP=SHR,UNIT=&UNIT1
//SORTOUT  DD DSN=&HLQ8..&JOBNM..SORTEX1,UNIT=SYSDA,
//            SPACE=(CYL,(&SPC4),RLSE),DISP=(NEW,CATLG,DELETE),
//*           DCB=(RECFM=LS,LRECL=874)
//            DCB=(RECFM=LS,LRECL=879)
//SYSIN    DD DSN=&LIB1..PARMLIB(P2360CP4),DISP=SHR
//* SORT FIELDS=(824,1,CH,D,26,2,CH,A,817,1,CH,A,63,10,CH,A,2,10,CH,A,
//*              117,9,CH,A)
//* INCLUDE COND=(29,1,CH,NE,C'N')  U.S. CITIZENS & RESIDENT ALIENS
//* OPTION DSPSIZE=0,HIPRMAX=0
//*
//* TIAA-CREF-TCLIFE-IND (DESCENDING) 824,1
//* CNTRCT-ORGN-CDE                  26,2
//* PYMNT-SUSPEND-CDE               817,1
//* CNTRCT-CREF-NBR                  63,10
//* CNTRCT-PPCN-NBR                   2,10
//* PYMNT-PRCSS-SEQ-NBR             117,9
//*
//* PYMNT-PAYEE-TX-ELCT-TRGGR        29,1
//*-------------------------------------------------------------------*
//* REPT050 - PRODUCES REPORTS ABOUT U.S. CITIZENS & RESIDENT ALIENS. *
//*           REP 1 - THE FEDERAL TAX WITHOLDING REGISTER.            *
//*                   (EVERY INSTALLMENT FOR "DC" SETTLEMENT).        *
//*           REP 2 - ALL, U.S. CITIZENS AND RESIDENT ALIENS.         *
//*                   (PER PAYEE FOR "DC" SETTLEMENT).                *
//*           REP 3 - TAX DATA INTERFACE SUMMARY CONTROL REPORT.      *
//*           NATURAL PROGRAM FCPP393                                 *
//*-------------------------------------------------------------------*
//REPT050 EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//       PARM='SYS=&NAT'
//SYSPRINT DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT  DD SYSOUT=&JCLO
//CMPRINT  DD SYSOUT=&JCLO
//CMPRT01  DD SYSOUT=&REPT
//CMPRT02  DD SYSOUT=&REPT
//CMPRT03  DD SYSOUT=&REPT
//CMWKF01  DD DSN=&HLQ8..&JOBNM..SORTEX1,DISP=SHR,UNIT=SYSDA
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(P2360CP5),DISP=SHR
//* FCPP393
//*-------------------------------------------------------------------*
//* SORT060 - SORTS THE "AP" TAX REPORTING FORMAT FILE CREATED        *
//*           EARLIER IN STEP "FRMT020" BY : COMPANY (DESCENDING) /   *
//*           ORIGIN / PEND / CREF # / PPCN # / SEQ #.                *
//*           SELECTS NON RESIDENT ALIENS ONLY.                       *
//*-------------------------------------------------------------------*
//SORT060 EXEC PGM=SORT,REGION=&REGN1,COND=(0,NE)
//SYSOUT   DD SYSOUT=&JCLO
//SYSDUMP  DD SYSOUT=&DMPS
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,(&SPC5),RLSE,,ROUND),BUFNO=15
//SORTWK02 DD UNIT=SYSDA,SPACE=(CYL,(&SPC5),RLSE,,ROUND),BUFNO=15
//SORTWK03 DD UNIT=SYSDA,SPACE=(CYL,(&SPC5),RLSE,,ROUND),BUFNO=15
//SORTWK04 DD UNIT=SYSDA,SPACE=(CYL,(&SPC5),RLSE,,ROUND),BUFNO=15
//SORTWK05 DD UNIT=SYSDA,SPACE=(CYL,(&SPC5),RLSE,,ROUND),BUFNO=15
//SYSPRINT DD SYSOUT=&JCLO
//SORTIN   DD DSN=&HLQ7..&JOBNM..IARTAX(&GDG1),DISP=SHR,UNIT=&UNIT1
//SORTOUT  DD DSN=&HLQ8..&JOBNM..SORTEX2,UNIT=SYSDA,
//            SPACE=(CYL,(&SPC3),RLSE),DISP=(NEW,CATLG,DELETE),
//*           DCB=(RECFM=LS,LRECL=874)
//            DCB=(RECFM=LS,LRECL=879)
//SYSIN    DD DSN=&LIB1..PARMLIB(P2360CP6),DISP=SHR
//* SORT FIELDS=(824,1,CH,D,26,2,CH,A,817,1,CH,A,63,10,CH,A,2,10,CH,A,
//*              117,9,CH,A)
//* INCLUDE COND=(29,1,CH,EQ,C'N')  SELECT ALIENS BUT NON RESIDENT
//* OPTION DSPSIZE=0,HIPRMAX=0
//*
//* TIAA-CREF-TCLIFE-IND (DESCENDING) 824,1
//* CNTRCT-ORGN-CDE                    26,2
//* PYMNT-SUSPEND-CDE                 817,1
//* CNTRCT-CREF-NBR                    63,10
//* CNTRCT-PPCN-NBR                     2,10
//* PYMNT-PRCSS-SEQ-NBR               117,9
//*
//* PYMNT-PAYEE-TX-ELCT-TRGGR          29,1
//*-------------------------------------------------------------------*
//* REPT070 - PRODUCES REPORTS OF FEDERAL TAXES FOR NON RESIDENT      *
//*           ALIENS.                                                 *
//*           REP 1 - TAX WITHOLDING REGISTER - NON RESIDENT ALIENS.  *
//*           REP 2 - TAX WITHOLDING REGISTER - NON RESIDENT ALIENS   *
//*                   (DETAIL LISTING).                               *
//*           NATURAL PROGRAM FCPP394.                                *
//*-------------------------------------------------------------------*
//REPT070 EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//       PARM='SYS=&NAT'
//SYSPRINT DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT  DD SYSOUT=&JCLO
//CMPRINT  DD SYSOUT=&JCLO
//CMPRT01  DD SYSOUT=&REPT
//CMPRT02  DD SYSOUT=&REPT
//CMWKF01  DD DSN=&HLQ8..&JOBNM..SORTEX2,DISP=SHR,UNIT=SYSDA
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(P2360CP7),DISP=SHR
//* FCPP394
//*-------------------------------------------------------------------*
//* SORT080 - SORTS THE "AP" TAX REPORTING FORMAT FILE CREATED        *
//*           EARLIER IN STEP "FRMT020" BY : COMPANY (DESCENDING) /   *
//*           ORIGIN / PEND / CREF # / PPCN # / SEQ #.                *
//*           SELECTS U.S. CITIZENS & RESIDENT ALIENS WITH STATE OR   *
//*           LOCAL TAXES DUE (GREATER THAN ZERO).                    *
//*-------------------------------------------------------------------*
//SORT080 EXEC PGM=SORT,REGION=&REGN1,COND=(0,NE)
//SYSOUT   DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&JCLO
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,(&SPC5),RLSE,,ROUND),BUFNO=15
//SORTWK02 DD UNIT=SYSDA,SPACE=(CYL,(&SPC5),RLSE,,ROUND),BUFNO=15
//SORTWK03 DD UNIT=SYSDA,SPACE=(CYL,(&SPC5),RLSE,,ROUND),BUFNO=15
//SORTWK04 DD UNIT=SYSDA,SPACE=(CYL,(&SPC5),RLSE,,ROUND),BUFNO=15
//SORTWK05 DD UNIT=SYSDA,SPACE=(CYL,(&SPC5),RLSE,,ROUND),BUFNO=15
//SYSPRINT DD SYSOUT=&JCLO
//SORTIN   DD DSN=&HLQ7..&JOBNM..IARTAX(&GDG1),DISP=SHR,UNIT=&UNIT1
//SORTOUT  DD DSN=&HLQ8..&JOBNM..SORTEX3,UNIT=SYSDA,
//            SPACE=(CYL,(&SPC3),RLSE),DISP=(NEW,CATLG,DELETE),
//*           DCB=(RECFM=LS,LRECL=874)
//            DCB=(RECFM=LS,LRECL=879)
//SYSIN    DD DSN=&LIB1..PARMLIB(P2360CP8),DISP=SHR
//* SORT FIELDS=(824,1,CH,D,26,2,CH,A,100,2,CH,A,817,1,CH,A,63,10,CH,A,
//*              2,10,CH,A,117,9,CH,A)
//* INCLUDE COND=(29,1,CH,NE,C'N',AND,865,5,PD,GT,0,OR,870,5,PD,GT,0)
//* SELECT U.S. CITIZENS & RESIDENT ALIENS WITH STATE OR LOCAL TAXES
//* GREATER THAN ZERO.
//* OPTION DSPSIZE=0,HIPRMAX=0
//*
//* TIAA-CREF-TCLIFE-IND (DESCENDING) 824,1
//* CNTRCT-ORGN-CDE                    26,2
//* PYMNT-SUSPEND-CDE                 817,1
//* CNTRCT-CREF-NBR                    63,10
//* CNTRCT-PPCN-NBR                     2,10
//* PYMNT-PRCSS-SEQ-NBR               117,9
//*
//* PYMNT-PAYEE-TX-ELCT-TRGGR          29,1
//* INV-ACCT-STATE-TAX-AMT            865,5
//* INV-ACCT-LOCAL-TAX-AMT            870,5
//*-------------------------------------------------------------------*
//* REPT090 - PRODUCES THE STATE / LOCALITY TAX WITHOLDING REGISTER   *
//*           FOR U.S. CITIZENS AND RESIDENT ALIENS.                  *
//*           REP 1 - STATE / LOCALITY TAX WITHOLDING REGISTER.       *
//*           NATURAL PROGRAM FCPP395.                                *
//*-------------------------------------------------------------------*
//REPT090 EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//       PARM='SYS=&NAT'
//SYSPRINT DD SYSOUT=&JCLO
//SYSOUT   DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT  DD SYSOUT=&JCLO
//CMPRINT  DD SYSOUT=&JCLO
//CMPRT01  DD SYSOUT=&REPT
//CMWKF01  DD DSN=&HLQ8..&JOBNM..SORTEX3,DISP=SHR,UNIT=SYSDA
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(P2360CP9),DISP=SHR
//* FCPP395
//*-------------------------------------------------------------------*
//* SORT100 - SORTES THE "AP" TAX INTERFACE FILE TO THE "NPD" SYSTEM  *
//*           BY : SOCIAL SECURITY / COMPANY CODE (DESCENDING) /      *
//*           PPCN/CREF NUMBER.  SEPARATES THE DATA INTO 5 DIFFERENT  *
//*           "GDG" DATA SETS :                                       *
//*       1 - SOCIAL SECURITY NUMBERS "000-00-0000" TO "111-19-9999"  *
//*       2 -   "       "        "    "111-20-0000" TO "199-99-9999"  *
//*       3 -   "       "        "    "200-00-0000" TO "333-49-9999"  *
//*       4 -   "       "        "    "333-50-0000" TO "459-99-9999"  *
//*       5 -   "       "        "    "460-00-0000" TO "999-99-9999"  *
//*-------------------------------------------------------------------*
//SORT100 EXEC PGM=SORT,REGION=&REGN1,COND=(0,NE)
//SYSOUT    DD SYSOUT=&JCLO
//SYSUDUMP  DD SYSOUT=&JCLO
//SORTWK01  DD UNIT=SYSDA,SPACE=(CYL,(&SPC5),RLSE,,ROUND),BUFNO=15
//SORTWK02  DD UNIT=SYSDA,SPACE=(CYL,(&SPC5),RLSE,,ROUND),BUFNO=15
//SORTWK03  DD UNIT=SYSDA,SPACE=(CYL,(&SPC5),RLSE,,ROUND),BUFNO=15
//SORTWK04  DD UNIT=SYSDA,SPACE=(CYL,(&SPC5),RLSE,,ROUND),BUFNO=15
//SORTWK05  DD UNIT=SYSDA,SPACE=(CYL,(&SPC5),RLSE,,ROUND),BUFNO=15
//SORTWK06  DD UNIT=SYSDA,SPACE=(CYL,(&SPC5),RLSE,,ROUND),BUFNO=15
//SYSPRINT  DD SYSOUT=&JCLO
//SORTIN    DD DSN=&HLQ7..&JOBNM..IARNPD(&GDG1),DISP=SHR,UNIT=&UNIT1
//SORTOF1   DD DSN=&HLQ7..&JOBNM..IARNPD.SORTED.SEQ1(&GDG1),
//             UNIT=&UNIT1,
//             DISP=(NEW,CATLG,DELETE),
//             DCB=(MODLDSCB,RECFM=LS,LRECL=450)
//SORTOF2   DD DSN=&HLQ7..&JOBNM..IARNPD.SORTED.SEQ2(&GDG1),
//             UNIT=&UNIT1,
//             DISP=(NEW,CATLG,DELETE),
//             DCB=(MODLDSCB,RECFM=LS,LRECL=450)
//SORTOF3   DD DSN=&HLQ7..&JOBNM..IARNPD.SORTED.SEQ3(&GDG1),
//             UNIT=&UNIT1,
//             DISP=(NEW,CATLG,DELETE),
//             DCB=(MODLDSCB,RECFM=LS,LRECL=450)
//SORTOF4   DD DSN=&HLQ7..&JOBNM..IARNPD.SORTED.SEQ4(&GDG1),
//             UNIT=&UNIT1,
//             DISP=(NEW,CATLG,DELETE),
//             DCB=(MODLDSCB,RECFM=LS,LRECL=450)
//SORTOF5   DD DSN=&HLQ7..&JOBNM..IARNPD.SORTED.SEQ5(&GDG1),
//             UNIT=&UNIT1,
//             DISP=(NEW,CATLG,DELETE),
//             DCB=(MODLDSCB,RECFM=LS,LRECL=450)
//SYSIN     DD DSN=&LIB1..PARMLIB(P2360CPA),DISP=SHR
//* SORT FIELDS=(13,9,CH,A,1,1,CH,D,2,8,CH,A)
//* OUTFIL FILES=1,
//* INCLUDE=(13,9,CH,GE,C'000000000',AND,13,9,CH,LE,C'111199999')
//* OUTFIL FILES=2,
//* INCLUDE=(13,9,CH,GE,C'111200000',AND,13,9,CH,LE,C'199999999')
//* OUTFIL FILES=3,
//* INCLUDE=(13,9,CH,GE,C'200000000',AND,13,9,CH,LE,C'333499999')
//* OUTFIL FILES=4,
//* INCLUDE=(13,9,CH,GE,C'333500000',AND,13,9,CH,LE,C'459999999')
//* OUTFIL FILES=5,
//* INCLUDE=(13,9,CH,GE,C'460000000',AND,13,9,CH,LE,C'999999999')
//* OPTION DSPSIZE=0,HIPRMAX=0
//*
//* SORTED BY :
//* SOCIAL SECURITY NUMBER                13,9,CH,A
//* COMPANY CODE            (DESCENDING)   1,1,CH,D
//* PPCN/CREF NUMBER                       2,8,CH,A
//*-------------------------------------------------------------------*
