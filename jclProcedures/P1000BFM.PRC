//P1000BFM PROC DBID=015,
//         DMPS='U',
//         JCLO='*',
//         LIB1='PROD',
//         LIB2='PROD.NT2LOGON',
//         NAT='NAT015P',
//         NATMEM='PANNSEC',
//         REGN1='5M',
//         REPT='8'
//**********************************************************************
//*        BENP260 - PRODUCES BENEFICIARY SYSTEM STATISTICAL REPORT     
//* - THIS JOB RUNS ON THE LAST BUSINESS DAY OF THE MONTH AFTER         
//*   P2010BFD DAILY INTERFACE JOB RUNS.                                
//* - THIS IS A READ ONLY JOB NO UPDATE                                 
//* - DATABASE 015                                                      
//* - FILES 038 & 031                                                   
//* - THIS JOB IS RESTARTABLE                                           
//**********************************************************************
//REPT010  EXEC PGM=NATB030,REGION=&REGN1,                              
//         PARM='SYS=&NAT'                                              
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                   
//SYSPRINT DD SYSOUT=&JCLO                                              
//SYSOUT   DD SYSOUT=&JCLO                                              
//SYSUDUMP DD SYSOUT=&DMPS                                              
//DDPRINT  DD SYSOUT=&JCLO                                              
//CMPRINT  DD SYSOUT=&JCLO                                              
//CMPRT01  DD SYSOUT=(&REPT,BF1000M1)                                   
//CMSYNIN  DD DSN=&LIB2..PARMLIB(&NATMEM),DISP=SHR                      
//         DD DSN=&LIB1..PARMLIB(P1000BF3),DISP=SHR                     
//*
//**********************************************************************
//* BENP261 - PRODUCE BENEFICIARY SYSTEM STATISTICAL REPORT (ADDITIONAL)
//* - THIS IS A READ ONLY JOB NO UPDATE                                 
//* - DATABASE 015                                                      
//* - FILE 038                                                          
//* - THIS JOB IS RESTARTABLE                                           
//**********************************************************************
//REPT020  EXEC PGM=NATB030,REGION=&REGN1,                              
//         PARM='SYS=&NAT'                                              
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                   
//SYSPRINT DD SYSOUT=&JCLO                                              
//SYSOUT   DD SYSOUT=&JCLO                                              
//SYSUDUMP DD SYSOUT=&DMPS                                              
//DDPRINT  DD SYSOUT=&JCLO                                              
//CMPRINT  DD SYSOUT=&JCLO                                              
//CMPRT01  DD SYSOUT=(&REPT,BF1000M2)                                   
//CMSYNIN  DD DSN=&LIB2..PARMLIB(&NATMEM),DISP=SHR                      
//         DD DSN=&LIB1..PARMLIB(P1000BF4),DISP=SHR                     
//         DD DSN=PROD.PARMLIB(P1000BF4),DISP=SHR                       
//**********************************************************************
