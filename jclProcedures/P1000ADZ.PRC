//P1000ADZ PROC
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
//*                                                                   *
//* PROCEDURE:  P1000ADZ                 DATE:    11/08/12            *
//* PROGRAMS EXECUTED:  IDCAMS
//*                                                                   *
//*********************************************************************
//IDCAMS  EXEC PGM=IDCAMS,REGION=4096K
//SYSPRINT DD  SYSOUT=A
//SYSIN    DD  *

  DELETE  PINT.ANN.PROJ.FACTORS.V165

  DEFINE CLUSTER (                                          -
               NAME(PINT.ANN.PROJ.FACTORS.V165)    -
               RECORDSIZE(3100 3100)                          -
               RECORDS(50 10)                               -
               KEYS(3 1)                                   -
               REUSE                                        -
               INDEXED )                                    -
         DATA (                                             -
               NAME(PINT.ANN.PROJ.FACTORS.V165.DATA  ))     -
         INDEX (                                             -
               NAME(PINT.ANN.PROJ.FACTORS.V165.INDEX   ))
