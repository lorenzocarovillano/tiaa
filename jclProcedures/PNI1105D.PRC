//PNI1105D PROC DMPS='U',                                               
//         JCLO='*',
//         HLQ1='PPM.ANN',
//         HLQ2='PNA.ANN.ACIS',
//         LIB1='PROD',
//         LIB2='PROD.NT2LOGON',
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         FSET=A,                   <-- OMNI FILE SET
//         TCSTRM=01,                <--
//         SPC1='25,5',
//         GDG1='+1',
//         REPT='8'
//** --------------------------------------------------------------- ** 
//* PROCEDURE:  THIS PROC IS USED TO PROCESS BULK ENROLLMENTS         * 
//*             AND CREATE T801 TRANSACTIONS FOR ILLINOIS SMP.        * 
//*             THE T801 TRANSACTIONS ARE THEN LOADED TO VTRAN BY     * 
//*             ROSTER JOB PPM3580S.                                  * 
//** -----------------------------------------------------------------* 
//* MODIFICATION:                                                     * 
//* 06/20/14  L.SHU - CHANGES FOR CREF REA PROJECTS            (CREA) * 
//*                   ADDED CMWKF03 TO HOLD THE ILLINOIS FUNDS        * 
//* ----------------------------------------------------------------- * 
//* PROGRAMS EXECUTED: SCIB9310 SCIB9300                              * 
//** --------------------------------------------------------------- ** 
//*********************************************************************
//**            RESET BATCH-RESTART RECORD                           **
//*********************************************************************
//RSET010  EXEC PGM=NATB030,REGION=0M,COND=(0,NE),
//   PARM='SYS=&NAT'
//SYSOUT   DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMPRINT  DD  SYSOUT=&JCLO
//CMSYNIN  DD  DSN=&LIB2..PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(NI1105D1),DISP=SHR
//*********************************************************************
//** CLEAR INTERMEDIATE DATASET USED TO CREATE T801 CARDS            **
//*********************************************************************
//CLR0020  EXEC PGM=IEBGENER,COND=(0,NE)
//SYSPRINT DD  SYSOUT=&JCLO
//SYSIN    DD  DUMMY
//SYSUT1   DD  DCB=&HLQ2..OMNI.ILL.PRE.CARDS,
//             DISP=SHR,
//             DSN=NULLFILE
//SYSUT2   DD  DISP=SHR,
//             DSN=&HLQ2..OMNI.ILL.PRE.CARDS
//*****************************************************************
//UPDT030  EXEC PGM=NATB030,REGION=0M,COND=(0,NE),                      
//   PARM='SYS=&NAT'                                                    
//*MSYS // INCLUDE MEMBER=DDTR&TCSTRM.M     TRAN DD STATEMENTS
//*MSYS // INCLUDE MEMBER=DDPL&TCSTRM.M     PLAN DD STATEMENTS
//*MSYS // INCLUDE MEMBER=DDPA&TCSTRM.M     PART DD STATEMENTS
//*MSYS // INCLUDE MEMBER=DDFU&TCSTRM.M     FUND DD STATEMENTS
//*MSYS // INCLUDE MEMBER=DDAH&TCSTRM.M     AHST DD STATEMENTS
//*MSYS // INCLUDE MEMBER=DDMU&TCSTRM.M     MUPD DD STATEMENTS
//*MSYS // INCLUDE MEMBER=DDEX&TCSTRM.M     EXTK DD STATEMENTS
//VSECU     DD DSN=&VSECPRE..MSTR.SECU,DISP=SHR
//VVIOL     DD DSN=&VSECPRE..MSTR.VIOL,DISP=SHR
//VSYSMX    DD  DSN=&VPRESYSM..MSTR.SYS,DISP=SHR
//VSYSM      DD  SUBSYS=(BLSR,'DDNAME=VSYSMX,SHRPOOL=5',
//          'BUFNI=25,BUFND=50,HBUFND=200,HBUFNI=100,DEFERW=YES,MSG=I')
//CMWKF01  DD  DSN=&HLQ1..ILL.ORP.ISSUE.UPLOAD,
//             DISP=SHR
//CMWKF02  DD  DSN=&HLQ2..OMNI.ILL.PRE.CARDS,DISP=MOD
//CMWKF03  DD  DSN=&HLQ2..ILL.FUNDS.FILE,DISP=SHR
//SYSOUT   DD  SYSOUT=&JCLO                                             
//SYSUDUMP DD  SYSOUT=&DMPS                                             
//SYSINIT  DD  DSN=&CTLLIBL(&ENVIR.&FSET.&TCSTRM),DISP=SHR
//CMPRINT  DD  SYSOUT=&JCLO                                             
//CMPRT01  DD  SYSOUT=(&REPT,NI1105D1)                                  
//CMPRT02  DD  SYSOUT=(&REPT,NI1105D2)                                  
//CMPRT03  DD  SYSOUT=(&REPT,NI1105D3)                                  
//CMSYNIN  DD  DSN=&LIB2..PARMLIB(&NATMEM),DISP=SHR                     
//         DD  DSN=&LIB1..PARMLIB(NI1105D2),DISP=SHR                    
//*********************************************************************
//* COPY DATASET TO GDG
//*********************************************************************
//COPY040 EXEC  PGM=IEBGENER,COND=(1,LT)
//SYSPRINT DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//SYSUT1   DD  DSN=&HLQ2..OMNI.ILL.PRE.CARDS,DISP=SHR
//SYSUT2   DD  DSN=&HLQ2..OMNI.RMT.ILL.CARD(&GDG1),
//             DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,
//             DATACLAS=DCPSEXTC,
//             SPACE=(CYL,(&SPC1),RLSE),
//             DCB=(RECFM=LS,LRECL=80,BLKSIZE=0)
//SYSIN     DD  DUMMY
//*
//**-----------------------------------------------------------------**
