//P1465TWR PROC DBID='003',
//         DMPS='U',
//         JCLO='*',
//         LIB1='PROD',
//         LIB2='PROD',
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REPT='8'
//*
//*************************************************************
//*
//* MODIFICATION HISTORY
//* 02/19/03 - NEW JOB TO PRODUCE MULTI-IRC ROLLOVERS REPORT    R. MA
//*
//*************************************************************
//*
//* EXTR010 - EXTRACTS PAYMENT RECORDS WITH MULTI IRC ROLLOVERS AND
//*           WRITES A REPORT.
//*           PROGRAM : TWRP5465
//*
//*--------------------------------------------------------------------*
//*
//EXTR010  EXEC PGM=NATB030,COND=(0,NE),
//         PARM=('OBJIN=N,IM=D,SYS=&NAT')
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSPRINT DD SYSOUT=&JCLO
//DDPRINT  DD SYSOUT=&JCLO
//SYSOUT   DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//CMPRINT  DD SYSOUT=&JCLO
//CMPRT01  DD SYSOUT=&REPT
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(P1465TW1),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//* TWRP5465
//* 2003   BLANK = CURRENT YEAR - 1
