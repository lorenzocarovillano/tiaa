//PNI1900D PROC DBID=003,                                               
//         DMPS='U',                                                    
//         GDG0='0',                                                    
//         GDG1='+1',                                                   
//         HLQ1='PPM.ANN',                                              
//*----->  BEGIN ORACLE ITEMS    <-------
//         LIB3='PROD',              <-- PROD SECURITY LIB FOR ORACLE
//         ORALOG=ORALOGON.NY.PARMLIB,   ORACLE SECURITY LIBRARY
//         CNTL3=UP003SEC,               USER/PASSWORD - ORACLE CONNECT
//         INCLUDE=PSTPINCL,             INCLUDE ORACLE INTERFACE PROD
//*----->  END ORACLE ITEMS    <-------
//*        HLQ2='PPDD.ANN',    /* COMMENTED OUT FOR TEST ONLY
//         JCLO='*',                                                    
//         LIB1='PROD',                                                 
//         LIB2='PROD',                                                 
//         NAT='NAT003P',                                               
//         NATMEM='PANNSEC',                                            
//         NATPGM='NI1900D1',                                           
//         NATPGM2='NI1900D3',                                          
//         REGN1='0M',                                                  
//         REPT='8',                                                    
//         SPC2='5,2'                                                   
//*        FSET=A,                   <-- OMNI FILE SET
//*        TCSTRM=01                 <--
//*********************************************************************
//* MODIFICATION                                                      *
//* 12/2014 MONTESCLAROS YURI C332636 REPLACE UP303SEC W/ UP003SEC    *
//*********************************************************************
//*      C R E A T E   R H S P EXTRACT AND GENERATE POST DATA         *
//*********************************************************************
//* PROGRAMS EXECUTED: APPB165  SORT APPB166
//*********************************************************************
//UPDT010  EXEC PGM=NATB030,REGION=&REGN1,
//   PARM='SYS=&NAT,UDB=&DBID,OBJIN=N'
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMWKF01  DD DSN=&HLQ1..RHSP.APPB165.EXTRACT(&GDG1),
//            DATACLAS=DCPSEXTC,UNIT=SYSDA,
//            DISP=(,CATLG,DELETE),SPACE=(CYL,(&SPC2),RLSE),
//            DCB=(RECFM=VB,LRECL=7200,BLKSIZE=0)
//CMWKF02  DD DSN=&HLQ1..RHSP.APPB165.EXTRACT(&GDG0),DISP=OLD
//SYSPRINT DD SYSOUT=&JCLO
//DDPRINT  DD SYSOUT=&JCLO
//SYSOUT   DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//CMPRINT  DD SYSOUT=&JCLO
//CMPRT01  DD SYSOUT=(&REPT,NI1900D1)
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(&NATPGM),DISP=SHR
//* SORT BY PLAN
//**********************************************************************
//SORT025  EXEC PGM=SORT,REGION=&REGN1,COND=(0,NE)
//SYSOUT   DD SYSOUT=&JCLO
//SORTIN   DD DSN=&HLQ1..RHSP.APPB165.EXTRACT(&GDG1),
//            DISP=(OLD,PASS,DELETE)
//SORTOUT  DD DSN=&&EXTRACT,
//            DISP=(,PASS,),
//            DATACLAS=DCPSEXTC,
//            UNIT=SYSDA,
//            RECFM=VB,LRECL=9590,
//            SPACE=(CYL,(&SPC2),RLSE)
//SYSIN    DD DSN=&LIB1..PARMLIB(NI1900D2),DISP=SHR
//*********************************************************************
//UPDT030  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//   PARM='SYS=&NAT,UDB=&DBID,OBJIN=N'
//*----->  BEGIN ORACLE ITEMS    <-------
//*MSYS //        INCLUDE MEMBER=&INCLUDE
//ACISFLE   DD DSN=&LIB3..&ORALOG(&CNTL3),DISP=SHR
//*----->  END ORACLE ITEMS    <-------
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMWKF01  DD DSN=&&EXTRACT,DISP=(OLD,DELETE,DELETE)
//SYSPRINT DD SYSOUT=&JCLO
//DDPRINT  DD SYSOUT=&JCLO
//SYSOUT   DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//CMPRINT  DD SYSOUT=&JCLO
//CMPRT01  DD SYSOUT=&JCLO
//CMPRT02  DD SYSOUT=(&REPT,NI1900D2)
//CMPRT03  DD SYSOUT=(&REPT,NI1900D3)
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(&NATPGM2),DISP=SHR
//* VSECU     DD  DSN=&VSECPRE..MSTR.SECU,DISP=SHR
//* VVIOL     DD  DSN=&VSECPRE..MSTR.VIOL,DISP=SHR
//* VSYSM     DD  DSN=&VPRESYSM..MSTR.SYS,DISP=SHR
//* SYSINIT   DD  DSN=&CTLLIBL(&ENVIR.&FSET.&TCSTRM),DISP=SHR
//* SYS105    DD  DSN=&HLQ2..TIAACREF.FACTORS.V168,
//*         DISP=SHR
//*********************************************************************
