//P2160IAD PROC DBID='003',
//         DMPS='U',
//         HLQ0='PIA.ONL.ANN',
//         HLQ1='PDA.ONL.ANN',
//         HLQ8='PPDD',
//         JCLO='*',
//         JOBNM='P2160IAD',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='5M',
//         REPT='8',
//         SPC1='10',
//         SPC2='05'
//*
//* ----------------------------------------------------------------- *
//* PROCEDURE:  P2160IAD                 DATE:    03/22/98            *
//* ----------------------------------------------------------------- *
//* PROGRAMS EXECUTED: IATP100S SORT IATP100                          *
//* ----------------------------------------------------------------- *
//* THIS JOB IS FOR ETNAT REPORTING GIVING ESTIMINATE OF TOTAL ASSET
//* TRANSFER AMOUNT                                                   *
//*                                                                   *
//*   READS ADABAS FILE 171 DBID=45                                   *
//*                                                                     
//* ----------------------------------------------------------------- *
//*                                                                     
//*    PROGRAMS
//*    IATP100S- CREATE EXTRACT FILE USED BY REPORTING PROGRAM
//*    IATP100 - IA ETNAT TOTAL ASSET TRANSFER AMOUNT REPORT
//*                                                                     
//* ----------------------------------------------------------------- *
//*                                                                     
//* MODIFICATION HISTORY
//* 09/29/99   CHANGED DCB IN STEP SORT010
//* 08/12/98   ADDED  OPRB PARAMETER
//* 04/04/02 - OIA JCL STANDARDS - PHIL STEINHAUSER
//* 07/15/10 - SVSAA/PCPOP PROJECT - ADDED IARNDV IN REPT020 - OSOTTO
//*                                                                     
//*******************************************************
//*                                                                     
//EXTR005 EXEC PGM=NATB030,REGION=&REGN1,
//        PARM='SYS=&NAT'
//SYSPRINT  DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//DDPRINT   DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRINT   DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRT01   DD SYSOUT=(&REPT,IA2160D1)
//SYSOUT    DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//SYSUDUMP  DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSABOUT  DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSDBOUT  DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//CMWKF01   DD DSN=&HLQ8..&JOBNM..DAILY.EXTR1,DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//             DCB=(RECFM=LS)
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(P2160IA1),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//*************************************************************
//SORT010  EXEC PGM=SORT,COND=(0,NE)
//SORTIN    DD DSN=&HLQ8..&JOBNM..DAILY.EXTR1,DISP=SHR
//SORTOUT   DD DSN=&HLQ8..&JOBNM..DAILY.TRANSFER,
//             DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,
//             SPACE=(TRK,(&SPC1,&SPC2),RLSE),
//             DCB=(MODLDSCB,RECFM=LS)
//*            DCB=(MODLDSCB,LRECL=737,RECFM=LS)
//SORTWK01  DD UNIT=SYSDA,SPACE=(CYL,(&SPC1,&SPC2),RLSE)
//SORTWK02  DD UNIT=SYSDA,SPACE=(CYL,(&SPC1,&SPC2),RLSE)
//SYSOUT    DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//SYSIN     DD DSN=&LIB1..PARMLIB(P2160IA2),DISP=SHR
//********************************************************************
//REPT020  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//        PARM=('SYS=&NAT','OPRB=(.ALL)')
//SYSPRINT  DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//DDPRINT   DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRINT   DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRT01   DD SYSOUT=(&REPT,IA2160D2)
//CMPRT02   DD SYSOUT=(&REPT,IA2160D3)
//CMPRT03   DD SYSOUT=(&REPT,IA2160D4)
//SYSOUT    DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//SYSUDUMP  DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSABOUT  DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSDBOUT  DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//DIAUVS    DD DSN=&HLQ0..DAILYAUV.V471,DISP=SHR
//IARTMG    DD DSN=&HLQ0..IARATE.MANAGER.V470,DISP=SHR
//IARNDV    DD DSN=&HLQ0..RENEWAL.DIVIDEND.V472,DISP=SHR
//RIUMTF    DD DSN=&HLQ1..MORT.ULTTABLF.V446,DISP=SHR
//CMWKF01   DD DSN=&HLQ8..&JOBNM..DAILY.TRANSFER,DISP=SHR
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(P2160IA3),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
