//P9982CWD PROC                                                         
//**********************************************************************
//*          CWF-READ MIT AND CREATE LIST OF ISN TO BE PURGED           
//**********************************************************************
//* INIT010 - INITIALIZE PCWF FILE                                      
//* EXTR020 - EXTRACTS/CREATES LIST OF ISNS TO BE PURGED (PARM=P9982CW1)
//* UPDT030 - UPDATES RUN CONTROL RECORD                                
//*         - INDICATES SUCCESSFUL RUN (PARM=P9982CW3)                  
//*                                                                     
//* 8/7/96 CHANGED PROD.NT2LOGON.PARMLIB(PANNSEC) TO (DENANN).          
//*        IN EXTR020 AND UPDT030.                               A.A.   
//*                                                                     
//* 9/17/96  UPDT30 WAS ELEMINATED DUE TO CHANGES OF PURGING            
//* MASTER INDEX FILE AS PER L EPSTEIN.                          A.J    
//* CORRESPONDING  PARMLIB MEMBER (P9982CW3) WAS DELETED.               
//**********************************************************************
//INIT010  EXEC PGM=IEBGENER                                            
//SYSPRINT DD SYSOUT=*                                                  
//SYSUT1   DD DUMMY,DCB=(DSORG=PS,RECFM=LS,LRECL=19,BLKSIZE=1900)       
//SYSUT2   DD  DSN=PCWF.COR.MIH.ISN.RANGES,DISP=OLD,                    
//             DCB=(DSORG=PS,RECFM=LS,LRECL=19)                         
//SYSIN    DD DUMMY                                                     
//**********************************************************************
//EXTR020  EXEC PGM=NATB030,COND=(0,NE),                                
//         PARM='SYS=NAT045P'                                           
//STEPLIB  DD  DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR               
//DDCARD   DD  DSN=PROD.DN.PARMLIB(DBAPF145),DISP=SHR                   
//ROCDB045 DD  DUMMY                                                    
//SYSPRINT DD  SYSOUT=*                                                 
//SYSOUT   DD  SYSOUT=*                                                 
//DDPRINT  DD  SYSOUT=*                                                 
//CMPRINT  DD  SYSOUT=*                                                 
//SYSUDUMP DD  SYSOUT=U                                                 
//CMPRT01  DD  SYSOUT=*                                                 
//CMWKF01  DD  DSN=PCWF.COR.MIH.ISN.RANGES,                             
//             DISP=OLD,                                                
//             SPACE=(CYL,(5,3),RLSE),                                  
//             DCB=(DSORG=PS,RECFM=LS,LRECL=19)                         
//CMSYNIN  DD  DSN=PROD.NT2LOGON.PARMLIB(DENANN),DISP=SHR               
//         DD  DSN=PROD.DN.PARMLIB(P9982CW1),DISP=SHR                   
//**********************************************************************
