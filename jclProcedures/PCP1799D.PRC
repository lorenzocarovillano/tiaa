//PCP1799D PROC LIB1='PROD',        COMMON LIBRARY HLQ                  
//         RPTID='CP1799D',         MOBIUS REPORT ID                    
//         DMPS=*,                  DUMP OUTPUT                         
//         JCLO00=*,                JOB OUTPUT                          
//         REPT00=*,                REPORTS                             
//         REPT01=8,                REPORTS                             
//         DBID=003,                DATABASE ID                         
//         REGN1='9M',              REGION SIZE                         
//         NATVERS=NATB030,         NATURAL VERSION                     
//         NAT=NAT003P,             NATURAL PROFILE                     
//         PARMLIB=PARMLIB,         LLQ COMMON PARM LIBRARY             
//         NATLOG=NT2LOGON.PARMLIB, 2 LVL QUAL NATURAL LOGON LIBRARY    
//         NATMEM=PANNSEC           NATURAL LOGON MEMBER                
//*                                                                     
//*--------------------------------------------------------------------*
//*                                                                    *
//* PROCEDURE:  PCP1799D                           DATE:    10/15/2002 *
//*                                                                    *
//*           CONSOLIDATED PAYMENT SYSTEM / OPEN INVESTMENTS           *
//*                                                                    *
//*                       END-OF-DAY PROCESSING                        *
//*                                                                    *
//*  STEP    PROGRAM   PARMS                  COMMENTS                 *
//* -------  -------  --------  -------------------------------------- *
//* UPDT010  CPOP135  PCP1735A  UPDATE CNTW TABLE FOR END-OF-DAY RUN   *
//*                                                                    *
//*--------------------------------------------------------------------*
//*                                                                     
//***----------------------------------------------------------------***
//* UPDT010  - UPDATE CNTW TABLE FOR END-OF-DAY RUN                    *
//*            NATURAL PROGRAM CPOP135                                 *
//***----------------------------------------------------------------***
//UPDT010  EXEC PGM=&NATVERS,REGION=&REGN1,COND=(0,NE),                 
//         PARM='IM=D,SYS=&NAT'                                         
//SYSPRINT DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL                             
//SYSUDUMP DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL                             
//DDCARD   DD DSN=&LIB1..&PARMLIB(DBAPP&DBID),DISP=SHR                  
//DDPRINT  DD SYSOUT=&JCLO00,OUTPUT=*.OUTLOCL                           
//CMPRINT  DD SYSOUT=&REPT00,OUTPUT=*.OUTVDR                            
//CMPRT10  DD SYSOUT=(&REPT01,&RPTID.1),                                
//         DCB=(RECFM=VB,LRECL=136),OUTPUT=*.OUTVDR                     
//CMSYNIN  DD DSN=&LIB1..&NATLOG(&NATMEM),DISP=SHR                      
//         DD DSN=&LIB1..&PARMLIB(PCP1770A),DISP=SHR                    
