//P9100NID PROC DMPS='U',                                               
//         JCLO='*',
//         HLQ1='POMPL',
//         HLQ2='PNA.ANN.ACIS',
//         LIB1='PROD',
//         LIB2='PROD.NT2LOGON',
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         SPC1='25,10',
//         FSET='FA',
//         REPT='8'
//** --------------------------------------------------------------- ** 
//* PROCEDURE:  THIS PROC IS USED TO UPDATE THE PRAP RECORDS WITH     * 
//*             MATCH AND RELEASE STATUS.                             * 
//* ----------------------------------------------------------------- * 
//* PROGRAMS EXECUTED: SORT  SCIB9100                                 * 
//** --------------------------------------------------------------- ** 
//*********************************************************************
//SORT010  EXEC PGM=SORT,COND=(0,NE)
//SYSABOUT DD  SYSOUT=&DMPS
//SYSOUT   DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//SORTWK01 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE)
//SORTIN   DD  DSN=&HLQ1..&FSET..BASEVENT(0),DISP=SHR
//SORTOUT  DD  DSN=&HLQ2..OMNI.CONTRIBS.FILE.EXTRACT,
//             DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,
//             SPACE=(CYL,(&SPC1),RLSE),
//             DCB=(RECFM=LS,LRECL=82)
//SYSIN    DD  DSN=&LIB1..PARMLIB(NI9100D2),DISP=SHR
//*********************************************************************
//UPDT010  EXEC PGM=NATB030,REGION=0M,COND=(0,NE),                      
//   PARM='SYS=&NAT'                                                    
//CMWKF01  DD  DSN=&HLQ2..OMNI.CONTRIBS.FILE.EXTRACT,
//             DISP=SHR
//SYSOUT   DD  SYSOUT=&JCLO                                             
//SYSUDUMP DD  SYSOUT=&DMPS                                             
//CMPRINT  DD  SYSOUT=&JCLO                                             
//CMPRT01  DD  SYSOUT=(&REPT,NI9100D1)                                  
//CMPRT02  DD  SYSOUT=(&REPT,NI9100D2)                                  
//CMPRT03  DD  SYSOUT=(&REPT,NI9100D3)                                  
//CMSYNIN  DD  DSN=&LIB2..PARMLIB(&NATMEM),DISP=SHR                     
//         DD  DSN=&LIB1..PARMLIB(NI9100D1),DISP=SHR                    
//**-----------------------------------------------------------------**
