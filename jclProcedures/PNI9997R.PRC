//PNI9997R PROC DBID=003,                                               
//         NATMEM='PANNSEC',                                            
//         NAT='NAT003P',                                               
//         LIB1='PROD',                                                 
//         LIB2='PROD',                                                 
//         HLQ0='PNA.ANN',                                              
//         NATPGM='NATPGM1',                                            
//         HLQ1='PPDD',                                                 
//         WORK1='WORK.FILE1',                                          
//         REGN1='4M',                                                  
//         DMPS='U',                                                    
//         JCLO='*'                                                     
//*                                                                     
//********************************************************************* 
//*      A C I S   A D H O C   J O B                                  * 
//********************************************************************* 
//* EXECUTES ADHOC PROGRAMS FOR ACIS. STEP PARMS AND FILES ARE        * 
//* CHANGED AS NEEDED FOR THE ADHOC PROGRAMS.                         * 
//* ----------------------------------------------------------------- * 
//* CREATED: 09/30/2005 - J BERGHEISER                                * 
//* ----------------------------------------------------------------- * 
//* MODIFICATION                                                      * 
//********************************************************************* 
//* ASSIGN BLANKS TO AAT-NA-CODE ON ANNTY-ACTVTY-ACCPTD-APPLCTN         
//* SC35226 - RUN ADHOC PROGRAM TO CORRECT OMNI TPO & IPRO AMOUNTS      
//* SC47366 - ADD SYMBOLICS FOR WORK FILE                               
//********************************************************************* 
//*                                                                     
//UPDT010  EXEC PGM=NATB030,REGION=&REGN1,                              
//   PARM='SYS=&NAT,UDB=&DBID,OBJIN=N'                                  
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                   
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR             
//         DD DSN=&HLQ0..PNI9997R.&NATPGM,DISP=SHR                      
//CMWKF01  DD DSN=&HLQ1..PNI9997R.&WORK1,DISP=SHR                       
//CMWKF02  DD DUMMY                                                     
//CMWKF03  DD DUMMY                                                     
//CMWKF04  DD DUMMY                                                     
//CMWKF05  DD DUMMY                                                     
//CMPRT01  DD SYSOUT=&JCLO                                              
//CMPRT02  DD SYSOUT=&JCLO                                              
//CMPRT03  DD SYSOUT=&JCLO                                              
//CMPRT04  DD SYSOUT=&JCLO                                              
//CMPRT05  DD SYSOUT=&JCLO                                              
//SYSPRINT DD SYSOUT=&JCLO                                              
//DDPRINT  DD SYSOUT=&JCLO                                              
//SYSOUT   DD SYSOUT=&JCLO                                              
//CMPRINT  DD SYSOUT=&JCLO                                              
//SYSUDUMP DD SYSOUT=&DMPS                                              
