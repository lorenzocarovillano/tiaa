//PMO0467D PROC DBID='008',
//         DMPS='U',
//         HLQ0='PPM.ANN.MAYO',
//         HLQ8='PPDD.ANN.MAYO',
//         JCLO='*',
//         JOBNM='PMO0467D',       CURRENT JOBNAME
//         JOBNM1='PMO0440D',      JOBNAME OF FIDELITY FEED
//         LIB1='PROD',            PARMLIB
//         LIB2='PROD',            NT2LOGON.PARMLIB
//         LIB3='PROD',            FOR TESTING CRS
//         NAT='NAT008P',
//         NATMEM='PANNSEC',
//         REGN1='5M',
//         REPT='8',
//         SPC1='10',
//         SPC2='5'
//*
//*===================================================================*
//*
//*        FIDELITY PREMIUM AND NON PREMIUM POST-UPDATE REPORTING
//*
//*-------------------------------------------------------------------*
//*
//* PROCEDURE: PMO0467D (RUNS CONCURRENTLY WITH PMO0465D)
//*
//* PROGRAMS:  SEE STEPS
//*
//* FUNCTION:
//*
//* PROCESSING FOR FIDELITY'S MAYO CLINIC DATA                        *
//* INPUT IS THE FIDELITY TRANSACTION FILE - ADABAS                   *
//* OUTPUT IS A SEQUENTIOAL FLAT FILE OF NONPREMIUM TRANSACTIONS      *
//*                                                                   *
//*   FIDELITY-TRANSACTION-FILE 008/052                               *
//*                                                                   *
//*                                                                   *
//* PROGRAM READS MAYO TRANSACTION FILE AND CREATES A SEQUENTIAL FEED *
//* TO SUBSEQUENT STEPS.                                              *
//*                                                                   *
//**********************************************************************
//* ----------------------------------------------------------------- *
//* DELETES
//* ----------------------------------------------------------------- *
//*
//DELETE  EXEC PGM=IEFBR14
//*
//SYSOUT  DD SYSOUT=*
//*
//DD1     DD DSN=&HLQ8..&JOBNM..EXTR010.PREMOUT,
//        DISP=(MOD,DELETE),UNIT=SYSDA,SPACE=(TRK,0)
//*
//DD2     DD DSN=&HLQ8..&JOBNM..SORT020.PREMOUT,
//        DISP=(MOD,DELETE),UNIT=SYSDA,SPACE=(TRK,0)
//*
//DD3     DD DSN=&HLQ8..&JOBNM..EXTR030.PREMOUT,
//        DISP=(MOD,DELETE),UNIT=SYSDA,SPACE=(TRK,0)
//*
//DD4     DD DSN=&HLQ8..&JOBNM..EXTR040.PREMOUT,
//        DISP=(MOD,DELETE),UNIT=SYSDA,SPACE=(TRK,0)
//*
//DD5     DD DSN=&HLQ8..&JOBNM..SORT050.PREMOUT,
//        DISP=(MOD,DELETE),UNIT=SYSDA,SPACE=(TRK,0)
//*
//DD6     DD DSN=&HLQ0..&JOBNM..REPT060.TOTALS,
//        DISP=(MOD,DELETE),UNIT=SYSDA,SPACE=(TRK,0)
//*
//DD7     DD DSN=&HLQ0..&JOBNM..EXTR010.TOTALS,
//        DISP=(MOD,DELETE),UNIT=SYSDA,SPACE=(TRK,0)
//*********************************************************************
//* EXTR010  RUNS SMYP425  - PREMIUM EXTRACT                         *
//*********************************************************************
//*
//* RESTART:
//*
//* EXTRACT STEP. IF STEP FAILS, MAY RESTART AT THIS 'JOBSTEP.PROCSTEP'
//*
//*********************************************************************
//EXTR010 EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//        PARM='SYS=&NAT,OPRB=(.ALL)'
//SYSOUT   DD SYSOUT=&JCLO
//SYSPRINT DD SYSOUT=&JCLO
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT  DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//CMPRINT  DD SYSOUT=&JCLO
//CMWKF01  DD DSN=&HLQ0..&JOBNM1..REPT040.UPLOAD,DISP=SHR
//CMWKF02  DD DSN=&HLQ8..&JOBNM..EXTR010.PREMOUT,
//            DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//            UNIT=SYSDA,
//            DCB=RECFM=LS
//CMWKF03  DD DSN=&HLQ0..&JOBNM..EXTR010.TOTALS,
//            DISP=(,CATLG,DELETE),
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//            UNIT=SYSDA
//CMPRT01  DD SYSOUT=(&REPT,MO0467D1)
//CMPRT02  DD SYSOUT=(&REPT,MO0467D2)
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB3..PARMLIB(MO0467D1),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//*********************************************************************
//* SORT020 EXECUTES SORT ON EXTR010 DATASET                          *
//*********************************************************************
//*
//* RESTART:
//*
//* SORT    STEP. IF STEP FAILS, MAY RESTART AT THIS 'JOBSTEP.PROCSTEP'
//*
//*********************************************************************
//SORT020  EXEC PGM=SORT,COND=(2,LT),PARM='EQUALS'
//SYSABOUT DD SYSOUT=U
//SYSOUT   DD SYSOUT=*
//SYSUDUMP DD SYSOUT=&DMPS
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,(50,50),RLSE)
//SORTWK02 DD UNIT=SYSDA,SPACE=(CYL,(50,50),RLSE)
//SORTIN   DD DSN=&HLQ8..&JOBNM..EXTR010.PREMOUT,DISP=SHR
//SORTOUT  DD DSN=&HLQ8..&JOBNM..SORT020.PREMOUT,
//            DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//            UNIT=SYSDA
//SYSIN    DD DSN=&LIB3..PARMLIB(MO0467D6),DISP=SHR
//*********************************************************************
//* REPT030 EXECUTES THE PROGRAM SMYP427                              *
//*********************************************************************
//*
//* RESTART:
//*
//* REPORT  STEP. IF STEP FAILS, MAY RESTART AT THIS 'JOBSTEP.PROCSTEP'
//*
//*********************************************************************
//REPT030 EXEC PGM=NATB030,REGION=&REGN1,COND=(2,LT),
//        PARM='SYS=&NAT'
//SYSOUT   DD SYSOUT=&JCLO
//SYSPRINT DD SYSOUT=&JCLO
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT  DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//CMPRINT  DD SYSOUT=&JCLO
//CMPRT01  DD SYSOUT=(&REPT,MO0467D3)
//CMWKF01  DD DSN=&HLQ8..&JOBNM..SORT020.PREMOUT,DISP=SHR
//CMWKF02  DD DSN=&HLQ0..&JOBNM1..REPT040.UPLOAD,DISP=SHR
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB3..PARMLIB(MO0467D7),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//*********************************************************************
//* EXTR040 RUNS SMYP434 - PREMIUM SUMMARY REPORTING STARTS HERE      *
//*********************************************************************
//*
//* RESTART:
//*
//* EXTRACT STEP. IF STEP FAILS, MAY RESTART AT THIS 'JOBSTEP.PROCSTEP'
//*
//*********************************************************************
//EXTR040 EXEC PGM=NATB030,REGION=&REGN1,COND=(2,LT),
//        PARM='SYS=&NAT'
//SYSOUT   DD SYSOUT=&JCLO
//SYSPRINT DD SYSOUT=&JCLO
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT  DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//CMPRINT  DD SYSOUT=&JCLO
//CMWKF01  DD DSN=&HLQ8..&JOBNM..EXTR010.PREMOUT,DISP=SHR
//CMWKF02  DD DSN=&HLQ8..&JOBNM..EXTR040.PREMOUT,
//            DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//            UNIT=SYSDA,
//            DCB=RECFM=LS
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB3..PARMLIB(MO0467D8),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//*********************************************************************
//* SORT050 EXECUTES SORT ON EXTR040 DATASET                          *
//*********************************************************************
//*
//* RESTART:
//*
//* SORT    STEP. IF STEP FAILS, MAY RESTART AT THIS 'JOBSTEP.PROCSTEP'
//*
//*********************************************************************
//SORT050  EXEC PGM=SORT,COND=(2,LT),PARM='EQUALS'
//SYSABOUT DD SYSOUT=U
//SYSOUT   DD SYSOUT=*
//SYSUDUMP DD SYSOUT=&DMPS
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,(50,50),RLSE)
//SORTWK02 DD UNIT=SYSDA,SPACE=(CYL,(50,50),RLSE)
//SORTIN   DD DSN=&HLQ8..&JOBNM..EXTR040.PREMOUT,DISP=SHR
//SORTOUT  DD DSN=&HLQ8..&JOBNM..SORT050.PREMOUT,
//            DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//            UNIT=SYSDA
//SYSIN    DD DSN=&LIB3..PARMLIB(MO0467DB),DISP=SHR
//*
//*********************************************************************
//* REPT060 EXECUTES THE PROGRAM SMYP431                              *
//*********************************************************************
//*
//* RESTART:
//*
//* REPORT  STEP. IF STEP FAILS, MAY RESTART AT THIS 'JOBSTEP.PROCSTEP'
//*
//*********************************************************************
//REPT060 EXEC PGM=NATB030,REGION=&REGN1,COND=(2,LT),
//        PARM='SYS=&NAT'
//SYSOUT   DD SYSOUT=&JCLO
//SYSPRINT DD SYSOUT=&JCLO
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT  DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//CMPRINT  DD SYSOUT=&JCLO
//CMPRT01  DD SYSOUT=(&REPT,MO0467D4)
//CMWKF01  DD DSN=&HLQ8..&JOBNM..SORT050.PREMOUT,DISP=SHR
//CMWKF02  DD DSN=&HLQ0..&JOBNM1..REPT040.UPLOAD,DISP=SHR
//CMWKF03  DD DSN=&HLQ0..&JOBNM..REPT060.TOTALS,
//            DISP=(,CATLG,DELETE),
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//            UNIT=SYSDA
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB3..PARMLIB(MO0467DC),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//*********************************************************************
//*      AUTO EMAIL FUNCTION IF ERROR REPORT NEEDS ATTENTION
//*********************************************************************
//*
//* RESTART:
//*
//* EMAIL STEP. IF STEP FAILS, MAY RESTART AT THIS 'JOBSTEP.PROCSTEP'
//*                                                                   *
//*********************************************************************
//ERR1FILE  IF (EXTR010.RC EQ 2) THEN
//EMAL070  EXEC  PGM=CAJUCMD0
//SYSPRINT DD  SYSOUT=*
//$$SCD1   DD  DUMMY        >>>> PROD <<<<
//SYSIN    DD  DSN=&LIB1..PARMLIB(MO0467DA),DISP=SHR
//       ENDIF       >>>> END ERR1FILE <<<<
