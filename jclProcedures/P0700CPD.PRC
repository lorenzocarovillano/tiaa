//P0700CPD  PROC DBID=003,
//        REPORT='8',                                                   
//        SYSOUT='*',                                                   
//        SYSMSGS='U'                                                   
//*********************************************************************
//* PROCEDURE:  P0700CPD                  01/2004                     *
//*********************************************************************
//* UPDT010  UPDATE TO CPS PAYMENT FILE - WACHOVIA PAID CHECK FILE
//*          CPWP730 REPLACES OLD PROGRAM FCPP902 FOR INVESTMENT      *
//*          SOLUTIONS AS OF 01/26/2004 DATA CHANGE FROM THE BANK
//*********************************************************************
//UPDT010 EXEC PGM=NATB030,REGION=8M,
//        PARM='SYS=NAT003P'
//STEPLIB   DD DSN=PROD.BATCH.LOADLIB,DISP=SHR
//          DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//DDCARD    DD DSN=PROD.PARMLIB(DBAPP&DBID),DISP=SHR
//SYSPRINT  DD SYSOUT=&SYSOUT
//SYSOUT    DD SYSOUT=&SYSOUT
//SYSABOUT  DD SYSOUT=&SYSMSGS
//SYSDBOUT  DD SYSOUT=&SYSMSGS
//DDPRINT   DD SYSOUT=&SYSOUT
//CMPRINT   DD SYSOUT=&SYSOUT
//CMPRT01   DD SYSOUT=&REPORT
//CMPRT02   DD SYSOUT=&REPORT
//SYSUDUMP  DD SYSOUT=&SYSMSGS
//CMWKF01   DD DSN=PMS.ANN.P0600CPD.CLEARED.CHECKS(0),DISP=SHR
//CMSYNIN   DD DSN=PROD.NT2LOGON.PARMLIB(PANNSEC),DISP=SHR
//          DD DSN=PROD.PARMLIB(P0700CPA),DISP=SHR
//*
//* CPWP730
//*
