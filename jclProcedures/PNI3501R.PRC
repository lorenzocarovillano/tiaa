//PNI3501R PROC DBID=003,
//         DMPS='U',
//         JCLO='*',
//         LIB1='PROD',
//         LIB2='PROD',
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='5M',
//         SPC1='200,100',
//         GDG1='+1',
//         HLQ1='PNA.ANN'
//*********************************************************************
//* PROGRAM EXECUTED: APPB550 - DELETES RECORD FRON RECON FILE GIVEN  *
//*                   A CONTROL DATE -(USED FOR RESTART PROCESSING)   *
//*********************************************************************
//UPDT010  EXEC PGM=NATB030,REGION=&REGN1,
//   PARM='SYS=&NAT'
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSPRINT DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMPRINT  DD  SYSOUT=&JCLO
//CMPRT01  DD  SYSOUT=&JCLO
//CMWKF01  DD  DSN=&HLQ1..ACIS.RECON.CNTRLFLE,DISP=SHR
//CMWKF02  DD  DSN=&HLQ1..ACIS.RECON.DLTDFILE(&GDG1),
//            DISP=(NEW,CATLG,DELETE),
//            UNIT=SYSDA,
//            DATACLAS=DCPSEXTC,
//            RECFM=LS,LRECL=200,
//            SPACE=(CYL,(&SPC1),RLSE)
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(NI3501R1),DISP=SHR
