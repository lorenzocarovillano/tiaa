//TIARSNCD PROC RGN1='0M',
//        SPREFX='POMPY',
//        VPREFX='POMPY',
//        VFSET='PMT0',
//        DATACLS='DCPSEXTC',
//        GDG1='(+1)',
//        PARMLIB='PROD.PARMLIB',
//        LPREFX='PROD.OMNIPAY.BAT', <<LIBRARY FILE PREFIX
//        SPC1='500,500',
//        SYSOUT='*'
//*
//*---------------------------------------------------------------------
//*   BACKUP        POMPY.PMT0.RFNDRSCD
//*---------------------------------------------------------------------
//BACKUP1  EXEC PGM=IDCAMS
//SYSPRINT DD SYSOUT=&SYSOUT
//INPUT    DD DISP=SHR,DSN=&VPREFX..&VFSET..RFNDRSCD
//OUTPUT   DD DSN=&SPREFX..&VFSET..RFNDRSCD.BKP&GDG1,
//            DISP=(NEW,CATLG,DELETE),SPACE=(CYL,(&SPC1),RLSE),
//            UNIT=SYSDA,DATACLAS=&DATACLS,
//            DCB=(LRECL=650,BLKSIZE=0,DSORG=PS,RECFM=LS)
//*
//SYSIN DD DISP=SHR,DSN=&PARMLIB(REPRO)
//*
//*---------------------------------------------------------------------
//*   DELETE DEFINE POMPY.PMT0.RFNDRSCD
//*---------------------------------------------------------------------
//DEFINE2  EXEC PGM=IDCAMS,COND=(0,NE),REGION=&RGN1
//SYSPRINT DD SYSOUT=&SYSOUT
//*
//SYSIN DD DISP=SHR,DSN=&LPREFX..CTL(OPREFCD)
//*
//*---------------------------------------------------------------------
//*   REPRO DATA TO POMPY.PMT0.RFNDRSCD
//*   INPUT FILE MUST BE PROVIDED FROM JOB
//*---------------------------------------------------------------------
//REPRO3   EXEC PGM=IDCAMS,COND=(0,NE),REGION=&RGN1
//SYSPRINT DD SYSOUT=&SYSOUT
//*
//INPUT    DD DUMMY,SPACE=(TRK,(1,1))
//OUTPUT   DD DISP=OLD,DSN=&VPREFX..&VFSET..RFNDRSCD
//*
//SYSIN DD DISP=SHR,DSN=&PARMLIB(REPRO)
//*
