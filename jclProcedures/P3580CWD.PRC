//P3580CWD PROC DMPS='U',
//         JCLO=*,
//         LIB1='PROD',
//         LIB2='PROD',
//         NAT='NAT046P',
//         NATMEM='PANNSEC',
//         REGN1='5M',
//         REPT='8'
//*******************************************************************
//* SELECT 'NCW-NPIR' (030) RECORDS BY 'CWF-SUPPORT-TBL' DATE
//*   P3580CW1='NPRP6000'
//*******************************************************************
//UPDT010  EXEC PGM=NATB030,REGION=&REGN1,
//         PARM='SYS=&NAT'
//SYSPRINT DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMPRT01  DD  SYSOUT=(&REPT,CW3580D1)
//CMPRT02  DD  SYSOUT=(&REPT,CW3580D2)
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P3580CW1),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
