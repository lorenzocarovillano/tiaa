//P2150IAD PROC DBID='003',
//         DMPS='U',
//         HLQ0='PDA.ONL.ANN',
//         HLQ1='PINT.ONL.ANN',
//         JCLO='*',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='5M',
//         REPT='8'
//*
//*********************************************************************
//*
//* PROCEDURE:  P2150IAD                 DATE:    12/30/97            *
//*
//* ----------------------------------------------------------------- *
//*
//* PROGRAMS EXECUTED: IATP850 (NATURAL)                              *
//*                                                                   *
//* ----------------------------------------------------------------- *
//* THIS JOB CREATES REMINDER LETTERS FOR TRANSFERS THRU POST         *
//*                                                                   *
//*   READS ADABAS FILE 171 DBID=45                                   *
//*                                                                   *
//* ----------------------------------------------------------------- *
//*                                                                   *
//* MODIFICATION HISTORY
//* 04/04/02 - OIA JCL STANDARDS - PHIL STEINHAUSER
//*                                                                     
//*******************************************************
//*                                                                     
//REPT005 EXEC PGM=NATB030,REGION=&REGN1,
//        PARM='SYS=&NAT'
//SYSPRINT  DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//DDPRINT   DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRINT   DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRT01   DD SYSOUT=(&REPT,IA2150D1)
//SYSOUT    DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//SYSUDUMP  DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSABOUT  DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSDBOUT  DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//DAFILE    DD DSN=&HLQ0..DASRA.MASTER.FILE.V360,DISP=SHR
//DFACT     DD DSN=&HLQ1..TIAACREF.FACTORS.V168,DISP=SHR
//ISEIAFL   DD DSN=&HLQ1..IA.MASTER.V166,DISP=SHR
//IAA770    DD DSN=&HLQ1..TPARATE.FILE.V169,DISP=SHR
//DARFACT   DD DSN=&HLQ0..DAR.FACTORS.FILE.V431,DISP=SHR
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(P2150IA1),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
