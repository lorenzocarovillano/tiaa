//P4085IAA PROC DBID='003',
//         DMPS='U',
//         GDG0='+0',
//         HLQ0='PIA.ANN',
//         JCLO='*',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='5M',
//         REPT='8'
//*
//* ----------------------------------------------------------------- *
//* PROCEDURE:  P4085IAA                 DATE:    03/01/98            *
//* ----------------------------------------------------------------- *
//* PROGRAMS EXECUTED:  NT2B030 (IAAP386)                             *
//*                                                                   *
//*                     UPDATE HISTORICAL RATE FILE (DB003 - 205)     *
//* ----------------------------------------------------------------- *
//*                                                                     
//* MODIFICATION HISTORY
//* 04/11/02 - OIA JCL STANDARDS - PHIL STEINHAUSER
//*                                                                     
//**********************************************************************
//*
//UPDT010 EXEC PGM=NATB030,REGION=&REGN1,
//        PARM='SYS=&NAT'
//SYSPRINT DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//DDPRINT  DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRINT  DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//SYSOUT   DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRT01  DD SYSOUT=(&REPT,IA4085A1)
//CMPRT02  DD SYSOUT=(&REPT,IA4085A2)
//CMWKF01  DD DSN=&HLQ0..APRIL.RATES(&GDG0),DISP=SHR
//SYSUDUMP DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSABOUT DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSDBOUT DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(P4085IA1),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
