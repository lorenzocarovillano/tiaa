//POP0311D PROC SPREFX='POMPY',                    * WORK FILE PREFIX   
//            LPREFX='PROD.OMNIPAY',               * CTL LIB PREFIX     
//            VFSET='PMT0'                         * FILESET ID         
//*-----------------------------------------------------------------    
//*                                                                     
//*  MACCAMISH FILE TRANSMISSION                                        
//*                                                                     
//*-----------------------------------------------------------------    
//UNAL010  EXEC PGM=IEFBR14,COND=(0,LT)                                 
//DD1      DD DSN=&SPREFX..&VFSET..MACAMISH.CYCDATE,                    
//         DISP=(MOD,DELETE),                                           
//         UNIT=SYSDA,SPACE=(TRK,0)                                     
//*                                                                     
//*-----------------------------------------------------------------    
//*                                                                     
//*  CREATE CYCLE DATE FOR FILENAME TO BE TRANSMITTED TO MACCAMISH      
//*                                                                     
//*-----------------------------------------------------------------    
//EXTR020  EXEC PGM=SORT,COND=(0,LT)                                    
//SYSOUT   DD SYSOUT=*                                                  
//SORTIN   DD DSN=&SPREFX..CDTE&VFSET,DISP=SHR                          
//SORTOUT  DD DSN=&SPREFX..&VFSET..MACAMISH.CYCDATE,                    
//         DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,                   
//         UNIT=SYSDA,SPACE=(TRK,(1,1),RLSE)                            
//SYSIN    DD DSN=&LPREFX..BAT.CTL(OP0311D1),DISP=SHR                   
//*                                                                     
//*-----------------------------------------------------------------    
//*                                                                     
//*  TRANSMIT MACCAMISH FILE                                            
//*                                                                     
//*-----------------------------------------------------------------    
//NDMS030  EXEC PGM=DMBATCH,PARM=(NYSSNNN),COND=(0,LT)                  
//DMNETMAP DD DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR                   
//DMMSGFIL DD DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR                      
//DMPUBLIB DD DSN=PROD.NDM.NY.PROCESS.LIB,DISP=SHR                      
//DMPRINT  DD SYSOUT=*                                                  
//NDMCMDS  DD SYSOUT=*                                                  
//SYSUDUMP DD SYSOUT=U                                                  
//SYSIN    DD DSN=&LPREFX..BAT.CTL(OP0311D2),DISP=SHR                   
//         DD DSN=&SPREFX..&VFSET..MACAMISH.CYCDATE,DISP=SHR            
//         DD DSN=&LPREFX..BAT.CTL(OP0311D4),DISP=SHR                   
//*                                                                     
//*-----------------------------------------------------------------    
//*                                                                     
//*  TRANSMIT TRIGGER FILE                                              
//*                                                                     
//*-----------------------------------------------------------------    
//NDMS040  EXEC PGM=DMBATCH,PARM=(NYSSNNN),COND=(0,LT)                  
//DMNETMAP DD DSN=PMVSSF.STERLING.NDM.NETMAP,DISP=SHR                   
//DMMSGFIL DD DSN=PMVSSF.STERLING.NDM.MSG,DISP=SHR                      
//DMPUBLIB DD DSN=PROD.NDM.NY.PROCESS.LIB,DISP=SHR                      
//DMPRINT  DD SYSOUT=*                                                  
//NDMCMDS  DD SYSOUT=*                                                  
//SYSUDUMP DD SYSOUT=U                                                  
//SYSIN    DD DSN=&LPREFX..BAT.CTL(OP0311D5),DISP=SHR                   
