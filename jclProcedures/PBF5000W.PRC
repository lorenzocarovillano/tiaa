//PBF5000W PROC DMPS='U',
//         JCLO='*',
//         FSET='A'
//*
//*---------------------------------------------------------------------
//*       PARM SUBSTITUTION FOR SYSTSIN CARD (WITH PARMS)
//*---------------------------------------------------------------------
//PARM005 EXEC PARMSUB,
//        PARM='PARMSUB DB2S=&DB2S DBPL=&DBPL DBPGM=PSG0852            +
//              PARMS=&FSET'
//TEMPLATE DD  DSN=&PARMLIB(DB2CALLB),
//             DISP=SHR
//*-------------------------------------------------------------------
//* SXBF0001 SCRIPT CONVERTED TO COBOL DB2 MODULE AS A PART OF
//* OMNI SIMPLIFICATION PROJECT.
//*-------------------------------------------------------------------
//EXTR010  EXEC PGM=IKJEFT01,
//             REGION=0M,COND=(4,LT)
//SYSOUT   DD  SYSOUT=&JCLO
//SYSDBOUT DD  SYSOUT=&DMPS
//SYSABEND DD  SYSOUT=&DMPS
//SYSUDUMP DD  SYSOUT=&DMPS
//SYSTSPRT DD  SYSOUT=&JCLO
//SYSTSIN  DD  DSN=&&SUBSTED,DISP=(OLD,DELETE)
//OUTFILE DD  DSN=&FPREFIX.&FSET..BFEXT(+1),
//            DISP=(NEW,CATLG),DATACLAS=DCPSEXTC,UNIT=SYSDA,
//            SPACE=(TRK,(5000,2000),RLSE),
//            DCB=(RECFM=LS,LRECL=200,BLKSIZE=0)
//P491TDT  DD DSN=&FPREFIX.&FSET..PTSA006D.TRADE.DATE,DISP=SHR
