//P2200IAD PROC DBID='003',
//         DMPS='U',
//         GDG1='+1',
//         HLQ7='PPDT.PNA',
//         HLQ8='PPDD',
//         JCLO='*',
//         JOBNM='P2200IAD',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='9M',
//         REPT='8',
//         SPC1='50',
//         SPC2='20',
//         SPC3='1300',
//         SPC4='50'
//*
//* ----------------------------------------------------------------- *
//* PROCEDURE:  P2200IAD                 DATE:    01/22/96            *
//* ----------------------------------------------------------------- *
//* PROGRAMS EXECUTED:  NT2B030 (IAAP300)                             *
//*
//* ----------------------------------------------------------------- *
//*                                                                     
//* MODIFICATION HISTORY
//* 30/29/96    ADDED TRTCH=COMP                            P MOFFETT*
//* 04/05/02 - OIA JCL STANDARDS - PHIL STEINHAUSER
//* 04/05/03 - CHANGED FOLLOWING LRECL
//*            RECFM=LS,LRECL=315 TO LRECL=346
//*                                                                     
//* 10/19/06 - CHANGED FOLLOWING LRECL                                  
//*            RECFM=LS,LRECL=346 TO LRECL=354                          
//*                                                                     
//* 05/30/08 - REMOVED LRECL - ROTH 403(B) / 401(K)                     
//**********************************************************************
//*
//EXTR010 EXEC PGM=NATB030,REGION=&REGN1,
//        PARM='SYS=&NAT'
//SYSPRINT DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//DDPRINT  DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRINT  DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//SYSOUT   DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//SORTWK01 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC1,&SPC2),RLSE)
//SORTWK02 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC1,&SPC2),RLSE)
//SORTWK03 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC1,&SPC2),RLSE)
//SORTWK04 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC1,&SPC2),RLSE)
//SORTWK05 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC1,&SPC2),RLSE)
//CMPRT01  DD  SYSOUT=(&REPT,IA2200D1)
//CMPRT02  DD  SYSOUT=(&REPT,IA2200D2)
//CMWKF01  DD  DSN=&HLQ7..IA.FUND.DETAIL(&GDG1),TRTCH=COMP,
//             DISP=(NEW,CATLG,DELETE),VOL=(,,,99),                     
//             DCB=(MODLDSCB,RECFM=VB),UNIT=CARTV                       
//CMWKF02  DD  DSN=&HLQ8..&JOBNM..IAAP300.IATRANS,
//             DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             UNIT=SYSDA,SPACE=(CYL,(&SPC3,&SPC4),RLSE),
//             RECFM=LS
//*            RECFM=LS,LRECL=354  REMOVED LRECL FOR ROTH 5/08
//*            RECFM=LS,LRECL=346
//*            RECFM=LS,LRECL=315
//SYSUDUMP DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSABOUT DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSDBOUT DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPF&DBID),DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P2200IA1),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
