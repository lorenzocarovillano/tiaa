//PBF3300R PROC DBID=015,
//         DMPS='U',
//         JCLO='*',
//         LIB1='PROD',
//         LIB2='PROD.NT2LOGON',
//         NAT='NAT015P',
//         NATMEM='PANNSEC',
//         REGN1='9M',
//         REPT='8'
//**********************************************************************
//* THIS IS A PROC TO ADD DEFAULT BENE DESIGNATION PRE & POST 1999      
//* - THIS JOB READS WORKFILE 1 WITH PIN & TIAA CONTRACT NUMBER         
//* - THIS JOB RUNS ON-REQUEST                                          
//* - THIS JOB IS RESTARTABLE                                           
//* - PROGRAMS EXECUTED: BENP330
//**********************************************************************
//UPDT010  EXEC PGM=NATB030,REGION=&REGN1,                              
//         PARM='SYS=&NAT'                                              
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                   
//SYSPRINT DD SYSOUT=&JCLO                                              
//SYSOUT   DD SYSOUT=&JCLO                                              
//SYSUDUMP DD SYSOUT=&DMPS                                              
//DDPRINT  DD SYSOUT=&JCLO                                              
//CMPRINT  DD SYSOUT=&JCLO                                              
//CMPRT01  DD SYSOUT=(&REPT,BF3300R1)                                   
//CMWKF01  DD DUMMY
//CMSYNIN  DD DSN=&LIB2..PARMLIB(&NATMEM),DISP=SHR                      
//         DD DSN=&LIB1..PARMLIB(BF3300R1),DISP=SHR                     
