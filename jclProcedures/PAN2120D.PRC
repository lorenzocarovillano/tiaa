//PAN2120D PROC  DBID=003,
//         DMPS='U',
//*        GDG1='+1',
//         HLQ1='PAA.ANNZ',
//         HLQ8='PPDD',
//         JCLO='*',
//         LIB1='PROD',
//         LIB2='PROD',
//         LRECL='27587',
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         JOBNM='PAN2120D',
//         REGN1='4M',
//         REPT='8',
//         REPTID1='AN2120D1',
//         REPTID2='AN2120D2',
//         PARMMEM1='AN2120D1',
//         PARMMEM2='AN2120D2',
//         PARMMEM3='AN2120D3',
//         SPC1='9',
//         SPC2='1'
//*********************************************************************
//* PROCEDURE:  PAN2120D                 DATE:    04/21/04            *
//* PROGRAMS EXECUTED:                                                *
//*   STEP NAME    PROGRAM NAME                                       *
//* -------------  ------------                                       *
//*   UNDO010      IEFBR14
//*   REPT020      ADSP750                                            *
//*   SORT030      SORT                                               *
//*   SORT035      SORT     (SURVIVOR ONLY RECORDS)                   *
//*   UPDT040      ADSP773                                            *
//*********************************************************************
//* HISTORY OF CHANGES:
//*
//* 01/02/2013 O. SOTTO REMOVED SORTWKXX AND CHANGED SPACE ALLOCATIONS
//*                     TO RESOLVE SB37.
//*********************************************************************
//* UNDO010 WILL DELETE THE TEMPORARY DATASETS USED BY THIS JOB
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
//UNDO010 EXEC PGM=IEFBR14,COND=(0,NE)
//WKFILE1  DD DSN=&HLQ8..&JOBNM..TRANS.TEMP1,
//         DISP=(MOD,DELETE),SPACE=(CYL,(&SPC1),RLSE),UNIT=SYSDA
//WKFILE2  DD DSN=&HLQ8..&JOBNM..TRANS.TEMP2,
//         DISP=(MOD,DELETE),SPACE=(CYL,(&SPC1),RLSE),UNIT=SYSDA
//WKFILE3  DD DSN=&HLQ8..&JOBNM..TRANS.HIST,
//         DISP=(MOD,DELETE),SPACE=(CYL,(&SPC1),RLSE),UNIT=SYSDA
//WKFILE4  DD DSN=&HLQ1..&JOBNM..TRANS.HIST2,
//         DISP=(MOD,DELETE),SPACE=(CYL,(&SPC1),RLSE),UNIT=SYSDA
//*-------------------------------------------------------------------*
//* REPT020 WILL READ ADS-PRTCPNT, ADS-CNTRCT, ADS-IA-RSLT            * 
//*         AND SELECT ALL COMPLETED CASES (CLOSE STATUS) AND CREATE A*
//*         WORK FILE WITH SELECTED DATA.                             *
//*-------------------------------------------------------------------*
//REPT020  EXEC PGM=NATB030,REGION=&REGN1,
//    PARM='SYS=&NAT'
//SYSPRINT DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//DDCARD   DD  DSN=&LIB2..PARMLIB(DBAPP&DBID),DISP=SHR
//*SORTWK01 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC2),RLSE)
//CMWKF01  DD  DSN=&HLQ8..&JOBNM..TRANS.TEMP1,
//             DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,
//             SPACE=(&LRECL,(&SPC1,&SPC2),RLSE),
//             AVGREC=K,DATACLAS=DCPSEXTC,
//             DCB=RECFM=VB
//CMWKF02  DD  DSN=&HLQ8..&JOBNM..TRANS.TEMP2,
//             DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,
//             SPACE=(&LRECL,(&SPC1,&SPC2),RLSE),
//             AVGREC=K,DATACLAS=DCPSEXTC,
//             DCB=RECFM=VB
//SYSOUT   DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//CMPRT01  DD  SYSOUT=(&REPT,&REPTID1)
//CMSYNIN  DD  DSN=&LIB1..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB2..PARMLIB(&PARMMEM1),DISP=SHR
//*-------------------------------------------------------------------* 
//* SORT030 WILL READ THE WORK FILE PPDD.P1120OAD.TRANS.TEMP1,
//*         AND WRITE TO EXTRACT-FILE.  THIS IS FOR THE NON-TPA AND
//*         NON-IPRO REQUESTS
//*-------------------------------------------------------------------*
//SORT030  EXEC PGM=SORT,COND=(0,NE)
//SORTIN   DD  DSN=&HLQ8..&JOBNM..TRANS.TEMP1,DISP=SHR
//*SORTWK01 DD  UNIT=SYSDA,
//*             SPACE=(CYL,(&SPC1),RLSE)
//*SORTOUT  DD  DSN=&HLQ1..TRANS.HIST(&GDG1),
//SORTOUT  DD  DSN=&HLQ8..&JOBNM..TRANS.HIST,
//             DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,
//             DCB=RECFM=VB,
//             SPACE=(&LRECL,(&SPC1,&SPC2),RLSE),
//             AVGREC=K,DATACLAS=DCPSEXTC
//SYSOUT   DD  SYSOUT=&JCLO
//SYSIN    DD  DSN=&LIB2..PARMLIB(&PARMMEM2),DISP=SHR
//* SORT CONTROL CARD
//* SORT BY RQST-ID, #REC-TYPE
//*-------------------------------------------------------------------*
//* SORT035 WILL READ THE WORK FILE PPDD.P1120OAD.TRANS.TEMP2,
//*         AND WRITE TO EXTRACT-FILE.  THIS IS FOR THE NON-TPA AND
//*         NON-IPRO REQUESTS (SURVIVOR RECORDS ONLY)
//*-------------------------------------------------------------------*
//SORT035  EXEC PGM=SORT,COND=(0,NE)
//SORTIN   DD  DSN=&HLQ8..&JOBNM..TRANS.TEMP2,DISP=SHR
//*SORTWK01 DD  UNIT=SYSDA,
//*             SPACE=(CYL,(&SPC1),RLSE)
//*SORTOUT  DD  DSN=&HLQ1..TRANS.HIST(&GDG1),
//SORTOUT  DD  DSN=&HLQ1..&JOBNM..TRANS.HIST2,
//             DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,
//             DCB=RECFM=VB,
//             SPACE=(&LRECL,(&SPC1,&SPC2),RLSE),
//             AVGREC=K,DATACLAS=DCPSEXTC
//SYSOUT   DD  SYSOUT=&JCLO
//SYSIN    DD  DSN=&LIB2..PARMLIB(&PARMMEM2),DISP=SHR
//* SORT CONTROL CARD
//* SORT BY RQST-ID, #REC-TYPE
//*-------------------------------------------------------------------*
//* UPDT040 WILL EXEC ADSP773 AND UPDATE ADS-TRN-HSTRY-EXTRCT-RPT-DTE
//*         ON ADS-CONTROL RECORD, IF ABOVE STEPS RAN SUCCESSFULL.
//*-------------------------------------------------------------------*
//UPDT040  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//    PARM='SYS=&NAT'
//SYSPRINT DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//DDCARD   DD  DSN=&LIB2..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSOUT   DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//CMPRT01  DD  SYSOUT=(&REPT,&REPTID2)
//CMSYNIN  DD  DSN=&LIB1..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB2..PARMLIB(&PARMMEM3),DISP=SHR
