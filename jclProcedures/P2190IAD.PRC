//P2190IAD PROC DBID='003',
//         DMPS='U',
//         JCLO='*',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='5M',
//         REPT='8'
//*
//* ----------------------------------------------------------------- *
//* PROCEDURE:  P2190IAD                 DATE:    03/01/98            *
//* ----------------------------------------------------------------- *
//* PROGRAMS EXECUTED:  NT2B030 (IATP402)                             *
//*                                                                   *
//*                     IA TRANSFER POST PROCESSING                   *
//*                                                                     
//* ----------------------------------------------------------------- *
//*                                                                     
//* MODIFICATION HISTORY
//* 04/05/02 - OIA JCL STANDARDS - PHIL STEINHAUSER
//*                                                                     
//**********************************************************************
//*
//UPDT010 EXEC PGM=NATB030,REGION=&REGN1,
//        PARM='SYS=&NAT'
//SYSPRINT DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//DDPRINT  DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRINT  DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//SYSOUT   DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRT01  DD SYSOUT=(&REPT,IA2190D1)
//CMPRT02  DD SYSOUT=(&REPT,IA2190D2)
//CMPRT03  DD SYSOUT=(&REPT,IA2190D3)
//SYSUDUMP DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSABOUT DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSDBOUT DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(P2190IA1),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
