//P1430TWR PROC                                                         
//*------------*                                                        
//* DELETES TAX TABLES FOR THE SPECIFIC TAX YEAR FOUND IN PARM.         
//*------------*                                                        
//COPY010  EXEC PGM=NATB030,REGION=8M,                                  
//         PARM='SYS=NAT003P'                                           
//STEPLIB  DD  DSN=PROD.BATCH.LOADLIB,DISP=SHR                          
//         DD  DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR               
//DDCARD   DD  DSN=PROD.PARMLIB(DBAPP003),DISP=SHR                      
//SYSPRINT DD  SYSOUT=*                                                 
//DDPRINT  DD  SYSOUT=*                                                 
//SYSOUT   DD  SYSOUT=*                                                 
//SYSUDUMP DD  SYSOUT=U                                                 
//CMPRINT  DD  SYSOUT=*                                                 
//CMPRT01  DD  SYSOUT=8                                                 
//CMPRT02  DD  SYSOUT=8                                                 
//CMSYNIN  DD  DSN=PROD.NT2LOGON.PARMLIB(PANNSEC),DISP=SHR              
//         DD  DSN=PROD.PARMLIB(P1430TW1),DISP=SHR                      
//         DD  DSN=PROD.PARMLIB(FINPARM),DISP=SHR                       
