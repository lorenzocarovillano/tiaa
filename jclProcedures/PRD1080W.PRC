//PRD1080W  PROC REGN1='8M',
//          PARMMEM='XXXXXXXX',
//          GDG0='0',
//          GDG1='+1',
//          NATLOGON='PANNSEC',
//          NAT='NATB030',
//          SYS='NAT045P',
//          DBID=045,
//          LIB1='PROD',
//          HLQ1='PDA.ANN',
//          HLQ8='PPT.COR',
//          SPC1=50,
//          SPC2=10,
//          JCLO=*,
//          DMPS=U,
//          REPT=8
//********************************************************************
//* MODIFICATION LOG:
//*-------------------------------------------------------------------
//* DATE       NAME            CHG#      DESCRIPTION
//*-------------------------------------------------------------------
//* 01/14/2016 Y. MONTESCLAROS CHG368042 ADDED THE FOLLOWING STEPS:
//*                                      VERY001, SORT005, EXTR015,
//*                                      PARM025, MAIL030, DELT035,
//*                                      CREA040, PARM045 AND MAIL050
//* 06/05/2017 L. SHU          CHG422718 INCREASE LRECL BY 5 BYTES FOR
//*                                      PIN EXPANSION       /* PINEXP
//********************************************************************
//VERY001  EXEC PGM=IDCAMS
//SYSPRINT DD  SYSOUT=&JCLO
//DD1      DD  DSN=&HLQ1..RIDER.CASHOUT(&GDG0),DISP=SHR
//SYSIN    DD  DSN=&LIB1..PARMLIB(CHECKRCD),DISP=SHR
//*
//********************************************************************
//*   PROCEED WITH THE NEXT STEPS ONLY WHEN THERE ARE REQUEST EXTRACTS
//********************************************************************
//*
// IF (VERY001.RC NE 1) THEN
//********************************************************************
//*        COPY ALL GDG INTO A COMBINED INPUT FILE
//********************************************************************
//SORT005  EXEC PGM=SORT,PARM='EQUALS',
//             COND=(0,NE),REGION=&REGN1
//SYSOUT   DD  SYSOUT=&JCLO
//SORTWK01 DD  SPACE=(CYL,(&SPC1,&SPC2),RLSE),UNIT=SYSDA
//SORTWK02 DD  SPACE=(CYL,(&SPC1,&SPC2),RLSE),UNIT=SYSDA
//SORTWK03 DD  SPACE=(CYL,(&SPC1,&SPC2),RLSE),UNIT=SYSDA
//SORTWK04 DD  SPACE=(CYL,(&SPC1,&SPC2),RLSE),UNIT=SYSDA
//SORTWK05 DD  SPACE=(CYL,(&SPC1,&SPC2),RLSE),UNIT=SYSDA
//SORTIN   DD  DSN=&HLQ1..RIDER.CASHOUT,DISP=SHR
//SORTOUT  DD  DSN=&HLQ1..RIDER.CASHOUT.CMBND(&GDG1),
//             DISP=(NEW,CATLG,DELETE),
//             UNIT=SYSDA,DATACLAS=DCPSEXTC,
//             SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//             DCB=(RECFM=LS,LRECL=166,DSORG=PS)           /* PINEXP
//SYSIN    DD  DSN=&LIB1..PARMLIB(RD1080R6),DISP=SHR
//********************************************************************
//*        INITIALIZE DATASETS TO BE USED
//********************************************************************
//DELT010  EXEC PGM=IEFBR14,COND=(0,NE)
//SYSPRINT DD SYSOUT=*
//D1       DD DSN=&HLQ8..RIDER.CASHOUT.FILE1,
//            UNIT=SYSDA,
//            DISP=(MOD,DELETE,DELETE),
//            SPACE=(CYL,(0,0))
//D2       DD DSN=&HLQ8..RIDER.CASHOUT.FILE2,
//            UNIT=SYSDA,
//            DISP=(MOD,DELETE,DELETE),
//            SPACE=(CYL,(0,0))
//*********************************************************************
//*        PCC2600 - EDIT CHECK AND FORMAT FORCED CASHOUT DATA
//*********************************************************************
//EXTR015  EXEC PGM=PCC2600,COND=(0,NE),
//            PARM='CASH'
//SYSPRINT DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//OREPORT  DD  SYSOUT=&REPT
//INRIDER  DD  DSN=&HLQ1..RIDER.CASHOUT.CMBND(&GDG1),DISP=SHR
//OUTRIDER DD  DSN=&HLQ1..RIDER.CASHOUT.FRMTD(&GDG1),
//             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//             DCB=(RECFM=LS,LRECL=175,DSORG=PS)           /* PINEXP
//*********************************************************************
//*        RIDP450 - READ DA REPORTING ACCESS EXTRACT
//*********************************************************************
//EXTR020  EXEC PGM=&NAT,COND=(0,NE),REGION=&REGN1,
//            PARM='SYS=&SYS,UDB=&DBID,WH=ON'
//SYSPRINT DD  SYSOUT=&JCLO
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//CMPRT01  DD  SYSOUT=&REPT
//CMPRT02  DD  SYSOUT=&REPT
//CMPRT03  DD  SYSOUT=&REPT
//CMWKF01  DD  DSN=&HLQ1..RIDER.CASHOUT.FRMTD(&GDG1),DISP=SHR
//CMWKF02  DD  DSN=&HLQ8..RIDER.CASHOUT.FILE1,
//             DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//             DCB=(RECFM=LS,LRECL=500)
//CMWKF03  DD  DSN=&HLQ8..RIDER.CASHOUT.FILE2,
//             DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//             DCB=(RECFM=LS,LRECL=500)
//SYSUDUMP DD  SYSOUT=&DMPS
//SYSOUT   DD  SYSOUT=&JCLO
//CMSYNIN  DD  DSN=&LIB1..NT2LOGON.PARMLIB(&NATLOGON),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(&PARMMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//********************************************************************
//*        SUBSTITUTE EMAIL FILE PREFIX IN SYSTSIN
//********************************************************************
//PARM025  EXEC PGM=IRXJCL,COND=(0,NE),
//            PARM='PARMSUB LIB1=&LIB1'
//TEMPLATE DD DSN=&LIB1..PARMLIB(RD1080R2),DISP=SHR
//SUBSTED  DD DSN=&&SUBSTED,DISP=(,PASS),SPACE=(TRK,(1,2))
//SYSTSIN  DD DUMMY
//SYSTSPRT DD SYSOUT=&JCLO
//SYSEXEC  DD DSN=PROD.REXXLIB,DISP=SHR
//********************************************************************
//*        SEND EMAIL NOTIFICATION - PRD1080W HAS RAN
//********************************************************************
//MAIL030  EXEC PGM=IKJEFT01,COND=(0,NE),DYNAMNBR=50,REGION=4M
//SYSTSPRT DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//SYSPROC  DD DSN=PMVSPP.ISPF.CLISTLIB,DISP=SHR
//SYSTSIN  DD DSN=&&SUBSTED,DISP=(OLD,DELETE)
//********************************************************************
//*        DELETE ALL GDG THAT HAVE BEEN COMBINED/PROCESSED
//********************************************************************
//DELT035  EXEC PGM=IEFBR14,COND=(0,NE)
//SYSPRINT DD SYSOUT=*
//D1       DD DSN=&HLQ1..RIDER.CASHOUT,
//            UNIT=SYSDA,
//            DISP=(MOD,DELETE,DELETE),
//            SPACE=(CYL,(0,0))
//********************************************************************
//*        CREATE AN EMPTY FILE FOR NEXT RUN
//********************************************************************
//CREA040  EXEC PGM=IEFBR14,COND=(0,NE)
//SYSPRINT DD  SYSOUT=&JCLO
//D1       DD  DSN=&HLQ1..RIDER.CASHOUT(&GDG1),
//             DISP=(NEW,CATLG,DELETE),
//             UNIT=SYSDA,DATACLAS=DCPSEXTC,
//             SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//             DCB=(RECFM=LS,LRECL=166,DSORG=PS)           /* PINEXP
//SYSIN    DD  DUMMY
//*
// ELSE
//********************************************************************
//*        SUBSTITUTE EMAIL FILE PREFIX IN SYSTSIN
//********************************************************************
//PARM045  EXEC PGM=IRXJCL,
//            PARM='PARMSUB LIB1=&LIB1'
//TEMPLATE DD DSN=&LIB1..PARMLIB(RD1080R4),DISP=SHR
//SUBSTED  DD DSN=&&SUBSTED,DISP=(,PASS),SPACE=(TRK,(1,2))
//SYSTSIN  DD DUMMY
//SYSTSPRT DD SYSOUT=&JCLO
//SYSEXEC  DD DSN=PROD.REXXLIB,DISP=SHR
//********************************************************************
//*        SEND EMAIL NOTIFICATION - NO DATA TO PROCESS
//********************************************************************
//MAIL050  EXEC PGM=IKJEFT01,DYNAMNBR=50,REGION=4M
//SYSTSPRT DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//SYSPROC  DD DSN=PMVSPP.ISPF.CLISTLIB,DISP=SHR
//SYSTSIN  DD DSN=&&SUBSTED,DISP=(OLD,DELETE)
//*
// ENDIF             >>>> END VERY001.RC IF <<<<
