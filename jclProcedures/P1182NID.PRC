//P1182NID PROC JCLO='*',
//         HLQ1='PNA.ANN',
//         HLQ2='TMIGR.ANN',
//         SPC1='10,5'
//*****************************************************************
//ALOC020 EXEC PGM=IEBGENER
//SYSPRINT DD SYSOUT=&JCLO
//SYSIN    DD  DUMMY
//SYSUT1   DD DSN=&HLQ1..ACIS.COND.S249,DISP=SHR
//SYSUT2   DD  DSN=&HLQ2..ACIS.COND.S247,
//         DISP=(,CATLG,DELETE),
//         UNIT=SYSDA,DATACLAS=DCPSEXTC,
//         LRECL=306,RECFM=LS,BLKSIZE=0,
//         SPACE=(CYL,(&SPC1))
