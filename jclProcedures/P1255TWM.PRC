//P1255TWM  PROC DBID=003
//*--------------------------------------------------------*
//*                       AP MAINTENANCE                   *
//*--------------------------------------------------------*
//*                          HISTORY                       *
//*                          -------                       *
//*                                                        *
//* 12/19/07 : A. YOUNG - ADDED STEP COPY030 FOR DATE PARM *
//*                       AUTOMATION.                      *
//*                                                        *
//*--------------------------------------------------------*
//*      SORT TAX MAINTENANCE INTERFACE FROM P5080IAM      *
//*--------------------------------------------------------*
//SORT010   EXEC PGM=SORT,REGION=8M
//SYSUDUMP  DD SYSOUT=U
//SYSOUT    DD SYSOUT=*
//SYSPRINT  DD SYSOUT=*
//SORTWK01  DD UNIT=SYSDA,SPACE=(TRK,(75,25),RLSE)
//SORTWK02  DD UNIT=SYSDA,SPACE=(TRK,(75,25),RLSE)
//SORTWK03  DD UNIT=SYSDA,SPACE=(TRK,(75,25),RLSE)
//SORTWK04  DD UNIT=SYSDA,SPACE=(TRK,(75,25),RLSE)
//SORTWK05  DD UNIT=SYSDA,SPACE=(TRK,(75,25),RLSE)
//SORTWK06  DD UNIT=SYSDA,SPACE=(TRK,(75,25),RLSE)
//SORTWK07  DD UNIT=SYSDA,SPACE=(TRK,(75,25),RLSE)
//SORTIN    DD DSN=PIA.ANN.IAADMIN.TAXMAINT(0),
//             DISP=SHR
//SORTOUT   DD DSN=PNPD.COR.TAX.P1255TWM.APTRANS.SRTED(+1),
//             DISP=(NEW,CATLG,DELETE),
//             UNIT=SYSDA,
//             SPACE=(CYL,(1,1),RLSE),
//             DCB=(MODLDSCB,RECFM=LS,LRECL=457)
//SYSIN     DD DSN=PROD.PARMLIB(P1255TW1),DISP=SHR
//*--------------------------------------------------------*
//* UPDT020  AUTOMATED NPD TAX MAINTENANCE.                *
//*          COPY OF NATURAL PROGRAM TAXP6000              *
//*--------------------------------------------------------*
//UPDT020  EXEC PGM=NATB030,REGION=8M,COND=(0,NE),
//         PARM='SYS=NAT003P'
//STEPLIB   DD DSN=PROD.BATCH.LOADLIB,DISP=SHR
//          DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//DDCARD    DD DSN=PROD.PARMLIB(DBAPP&DBID),DISP=SHR
//SYSPRINT  DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=U
//SYSOUT    DD SYSOUT=*
//DDPRINT   DD SYSOUT=*
//SORTWK01  DD UNIT=SYSDA,SPACE=(TRK,(25,5),RLSE)
//SORTWK02  DD UNIT=SYSDA,SPACE=(TRK,(25,5),RLSE)
//SORTWK03  DD UNIT=SYSDA,SPACE=(TRK,(25,5),RLSE)
//SORTWK04  DD UNIT=SYSDA,SPACE=(TRK,(25,5),RLSE)
//SORTWK05  DD UNIT=SYSDA,SPACE=(TRK,(25,5),RLSE)
//SORTWK06  DD UNIT=SYSDA,SPACE=(TRK,(25,5),RLSE)
//SORTWK07  DD UNIT=SYSDA,SPACE=(TRK,(25,5),RLSE)
//CMPRINT   DD SYSOUT=*
//CMPRT01   DD SYSOUT=8
//CMPRT02   DD SYSOUT=8
//CMPRT03   DD SYSOUT=8
//CMPRT04   DD SYSOUT=8
//CMPRT05   DD SYSOUT=8
//CMPRT06   DD SYSOUT=8
//CMPRT07   DD SYSOUT=8
//CMPRT08   DD SYSOUT=8
//CMWKF01   DD DSN=PNPD.COR.TAX.P1255TWM.APTRANS.SRTED(+1),
//             DISP=SHR
//* CMWKF02 = TEMP SORT WORK
//CMWKF02   DD DSN=PPDD.TAXMNTAP,
//             DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,
//             SPACE=(TRK,(25,10),RLSE),
//             DCB=(RECFM=LS,LRECL=66)
//CMSYNIN   DD DSN=PROD.NT2LOGON.PARMLIB(PANNSEC),DISP=SHR
//          DD DSN=PROD.PARMLIB(P1255TW2),DISP=SHR
//          DD DSN=PROD.PARMLIB(FINPARM),DISP=SHR
//*
//*--------------------------------------------------------*
//* COPY030  COPY CURRENT DATES TO PREVIOUS FOR NEXT RUN.  *
//*--------------------------------------------------------*
//*
//COPY030  EXEC PGM=IEBGENER,COND=(0,NE)
//SYSABOUT DD SYSOUT=*
//SYSUDUMP DD SYSOUT=*
//SYSPRINT DD SYSOUT=*
//SYSIN    DD DUMMY
//SYSUT1   DD DSN=PPDD.TAX.P5080IAM.DATES.CURRENT,
//            DISP=SHR
//SYSUT2   DD DSN=PNPD.COR.TAX.P5080IAM.DATES.PREVIOUS,
//            DISP=SHR
//*
