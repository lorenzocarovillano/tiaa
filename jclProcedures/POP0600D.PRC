//POP0600D  PROC SYSOUT='*',
//         HLQ0='POMPY',            HLQ0
//         HLQ8='PMS',              HLQ8
//         JOBNM='P0600CPD'         2ND HLQ FOR FILES CREATED
//*       REPORT='8',                                                   
//*       SYSMSGS='U'                                                   
//***************************************************************
//* COPY THE POSITIVE PAY CLEARED CHECK UPLOAD INTO A CORP DSN  *
//***************************************************************
//COPY010  EXEC PGM=IEBGENER
//SYSPRINT DD SYSOUT=&SYSOUT
//SYSUT1   DD  DSN=&HLQ0..ANN.CLEARED.CHECKS,DISP=SHR
//SYSUT2   DD  DSN=&HLQ8..ANN.&JOBNM..CLEARED.CHECKS(+1),
//         DISP=(,CATLG,DELETE),
//         UNIT=SYSDA,SPACE=(CYL,(20,5))
//**       RECFM=LS,LRECL=80
//SYSIN    DD DUMMY
//*************************************************************
//* DELETE THE POMPY DSN                                     **
//*************************************************************
//DELT020  EXEC PGM=IEFBR14,COND=(0,NE)
//SYSPRINT DD  SYSOUT=&SYSOUT
//DD01     DD  DSN=&HLQ0..ANN.CLEARED.CHECKS,
//         DISP=(OLD,DELETE,DELETE)
