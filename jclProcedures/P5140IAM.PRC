//P5140IAM PROC DBID='003',
//         DMPS='U',
//         GDG0='+0',
//         HLQ0='PIA.ANN.IA',
//         JCLO='*',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='5M',
//         REPT='8'
//*
//* ----------------------------------------------------------------- * 
//* PROCEDURE:  P5140IAM                 DATE:     3/25/96            * 
//* ----------------------------------------------------------------- * 
//* PROGRAMS EXECUTED: IAAP345 (NATURAL)                                
//* ----------------------------------------------------------------- * 
//* APPLY   AUTO GENERATED TRANSACTIONS 006(TERMINATIONS),700(IVC)    *
//*         & 900(DEDUCTIONS) TO IAIQ DATABASE FILES                  *
//* ----------------------------------------------------------------- * 
//*                                                                     
//* MODIFICATION HISTORY
//* 04/11/02 - OIA JCL STANDARDS - PHIL STEINHAUSER
//*                                                                     
//**********************************************************************
//*
//EXTR010 EXEC PGM=NATB030,REGION=&REGN1,
//        PARM='SYS=&NAT'
//SYSPRINT  DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//DDPRINT   DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRINT   DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRT01   DD SYSOUT=(&REPT,IA5140M1)          AUTO GEN TRANS REPT
//CMPRT02   DD SYSOUT=(&REPT,IA5140M2)          CONTROL REC CHANGES REP
//SYSUDUMP  DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSABOUT  DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSDBOUT  DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSOUT    DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMWKF01   DD DSN=&HLQ0..AUTOGEN.TRANS(&GDG0),DISP=OLD,UNIT=SYSDA      
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(P5140IA1),DISP=SHR          IAAP345
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
