//PNI8900R PROC INDSN='POMPL.FA.POMA906R.ACIS.INTRFACE',
//         HLQ0='PNA.ANN',
//         GDG0='0',
//         GDG1='+1',
//         SEQ1='1',
//         SEQ2='2',
//         SEQ3='3',
//         SEQ4='4',
//         SEQ5='5',
//         SEQ6='6',
//         SEQ7='7',
//         SEQ8='8',
//         SEQ9='9',
//         SEQ0='0',
//         PRM='100',
//         SEC='100',
//         LIB1='PROD'
//*====================================================================*
//*                         NARRATIVE                                  *
//*====================================================================*
//*             SPLIT PROCESS FOR IRA SUBSTITUTION                     *
//*====================================================================*
//* THIS PROC WILL SPLIT THE IRA SUBSITUTION TRANSACTION FILE INTO 10  *
//* FILES TO ALLOW 10 CONCURRENT PROCESSES.                            *
//*--------------------------------------------------------------------*
//* MODIFICATION HISTORY                                               *
//*====================================================================*
//* PROGRAMS EXECUTED: SORT                                            *
//*====================================================================*
//SPLIT010  EXEC  PGM=SORT
//SYSOUT DD SYSOUT=*
//SORTIN DD DSN=&INDSN(&GDG0),DISP=SHR
//OUT1   DD DSN=&HLQ0..FILE&SEQ1..OMNI.INTRFACE(&GDG1),
//            DISP=(NEW,CATLG,DELETE),
//            SPACE=(CYL,(&PRM,&SEC),RLSE),
//            DATACLAS=DCPSEXTC,
//            DCB=*.SORTIN,
//            UNIT=SYSDA
//OUT2   DD DSN=&HLQ0..FILE&SEQ2..OMNI.INTRFACE(&GDG1),
//            DISP=(NEW,CATLG,DELETE),
//            SPACE=(CYL,(&PRM,&SEC),RLSE),
//            DATACLAS=DCPSEXTC,
//            DCB=*.SORTIN,
//            UNIT=SYSDA
//OUT3   DD DSN=&HLQ0..FILE&SEQ3..OMNI.INTRFACE(&GDG1),
//            DISP=(NEW,CATLG,DELETE),
//            SPACE=(CYL,(&PRM,&SEC),RLSE),
//            DATACLAS=DCPSEXTC,
//            DCB=*.SORTIN,
//            UNIT=SYSDA
//OUT4   DD DSN=&HLQ0..FILE&SEQ4..OMNI.INTRFACE(&GDG1),
//            DISP=(NEW,CATLG,DELETE),
//            SPACE=(CYL,(&PRM,&SEC),RLSE),
//            DATACLAS=DCPSEXTC,
//            DCB=*.SORTIN,
//            UNIT=SYSDA
//OUT5   DD DSN=&HLQ0..FILE&SEQ5..OMNI.INTRFACE(&GDG1),
//            DISP=(NEW,CATLG,DELETE),
//            SPACE=(CYL,(&PRM,&SEC),RLSE),
//            DATACLAS=DCPSEXTC,
//            DCB=*.SORTIN,
//            UNIT=SYSDA
//OUT6   DD DSN=&HLQ0..FILE&SEQ6..OMNI.INTRFACE(&GDG1),
//            DISP=(NEW,CATLG,DELETE),
//            SPACE=(CYL,(&PRM,&SEC),RLSE),
//            DATACLAS=DCPSEXTC,
//            DCB=*.SORTIN,
//            UNIT=SYSDA
//OUT7   DD DSN=&HLQ0..FILE&SEQ7..OMNI.INTRFACE(&GDG1),
//            DISP=(NEW,CATLG,DELETE),
//            SPACE=(CYL,(&PRM,&SEC),RLSE),
//            DATACLAS=DCPSEXTC,
//            DCB=*.SORTIN,
//            UNIT=SYSDA
//OUT8   DD DSN=&HLQ0..FILE&SEQ8..OMNI.INTRFACE(&GDG1),
//            DISP=(NEW,CATLG,DELETE),
//            SPACE=(CYL,(&PRM,&SEC),RLSE),
//            DATACLAS=DCPSEXTC,
//            DCB=*.SORTIN,
//            UNIT=SYSDA
//OUT9   DD DSN=&HLQ0..FILE&SEQ9..OMNI.INTRFACE(&GDG1),
//            DISP=(NEW,CATLG,DELETE),
//            SPACE=(CYL,(&PRM,&SEC),RLSE),
//            DATACLAS=DCPSEXTC,
//            DCB=*.SORTIN,
//            UNIT=SYSDA
//OUT0   DD DSN=&HLQ0..FILE&SEQ0..OMNI.INTRFACE(&GDG1),
//            DISP=(NEW,CATLG,DELETE),
//            SPACE=(CYL,(&PRM,&SEC),RLSE),
//            DATACLAS=DCPSEXTC,
//            DCB=*.SORTIN,
//            UNIT=SYSDA
//SYSIN  DD DSN=&LIB1..PARMLIB(NI8900R1),DISP=SHR
