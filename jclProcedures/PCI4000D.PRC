//PCI4000D PROC DBID='045',                                             
//         DMPS='U',                                                    
//         JCLO='*',                                                    
//         LIB1='PROD',                                                 
//         LIB2='PROD',                                                 
//         HLQ1='POMPL.FA',                                             
//         HLQ2='PPDD.ANN.OMNIPLUS',                                    
//         HLQ3='PPM.ANN.CIS',                                          
//         NAT='NAT003P',            NATURAL PROFILE                    
//         NATMEM='PANNSEC',                                            
//         REGN1='5M',                                                  
//         REPT='8',                 REPORT OUTPUT                      
//         REPTID1='CI4000D1',       REPORT ID                          
//         REPTID2='CI4000D2',       REPORT ID                          
//         REPTID3='CI4000D3',       REPORT ID                          
//         REPTID4='CI4000D4',       REPORT ID                          
//         PARM1='CI4000D1',                                            
//         PARM2='CI4000D2'                                             
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *     
//* PROCEDURE:  PCI4000D                                                
//* DESCRIPTION : OMNI TO CIS INTERFACE PROGRAM                         
//* PROGRAMS :                                                          
//*             SGDB4000 (VIA ADABAS/NATURAL)                           
//*                                                                     
//* FREQUENCY:  DAILY TRANSFER CONFIRMATION STATEMENT UPDATES POST      
//*                                                                     
//*  ADABAS FILES USED IN THIS PROC:                                    
//*  ==============================                                     
//*   A045    = POST FILES                                              
//* A003220 = EXTERNALIZATION FILE                                      
//*                                                                     
//* MODIFICATIONS                                                       
//* =================================================================   
//* 9/10/2008 K.GATES - ADDED ADDITIONAL REPORTS TO UPDT010 STEP        
//*                   - ADDED DATACLAS=DCPSEXTC                         
//* 06/2014  B.NEWSOM - ADDED TICKER SYMBOLS TRANSLATE FILE   (CREA)    
//* =================================================================   
//*                                                                     
//DELT001 EXEC PGM=IEFBR14                                              
//DD1     DD DSN=&HLQ2..TPA,                                            
//           DISP=(MOD,DELETE),                                         
//           UNIT=SYSDA,                                                
//           SPACE=(TRK,0)                                              
//**********************************************************************
//* SORT010 STEP: SORTS THE INPUT FILE BY JOB NAME TO BE USED AS INPUT  
//*          IN STEP FRMT020                                            
//**********************************************************************
//SORT010  EXEC PGM=SORT,REGION=&REGN1,COND=(0,NE)                      
//SYSOUT   DD SYSOUT=&JCLO                                              
//DDCARD   DD DSN=&LIB2..PARMLIB(DBAPP&DBID),DISP=SHR                   
//SORTIN   DD DSN=&HLQ1..BASEEXT(0),DISP=SHR                            
//SORTOF1  DD DSN=&HLQ2..TPA,                                           
//            DISP=(NEW,CATLG,DELETE),                                  
//            UNIT=SYSDA,DATACLAS=DCPSEXTC,                             
//            RECFM=VB,LRECL=30004,                                     
//            SPACE=(20,5,RLSE)                                         
//SYSIN    DD DSN=&LIB2..PARMLIB(&PARM2),DISP=SHR                       
//*****************************************************************     
//* PROGRAM :  SGDB4000                                                 
//* FUNCTION : THIS PROGRAM GENERATES UPDATES TO CIS FILE FOR NEW       
//*            TPA ISSUED BY OMNI STATION                               
//****************************************************************      
//UPDT010  EXEC PGM=NATB030,COND=(0,LT),                                
//             PARM=('SYS=&NAT,OPRB=(.ALL)'),                           
//             REGION=&REGN1                                            
//DDCARD   DD  DSN=&LIB2..PARMLIB(DBAPP&DBID),DISP=SHR                  
//SYSPRINT DD  SYSOUT=&JCLO                                             
//DDPRINT  DD  SYSOUT=&JCLO                                             
//SYSOUT   DD  SYSOUT=&JCLO                                             
//SYSDUMP  DD  SYSOUT=&DMPS                                             
//CMPRINT  DD  SYSOUT=&JCLO                                             
//CMWKF01  DD  DSN=&HLQ2..TPA,DISP=SHR                                  
//CMWKF02  DD  DSN=&HLQ3..FUND.TRANSLTE.FILE,DISP=SHR         (CREA)    
//CMPRT01  DD  SYSOUT=(&REPT,&REPTID1)                                  
//CMPRT02  DD  SYSOUT=(&REPT,&REPTID2)                                  
//CMPRT03  DD  SYSOUT=(&REPT,&REPTID3)                                  
//CMPRT04  DD  SYSOUT=(&REPT,&REPTID4)                                  
//*-------------------CONTROL CARD------------------------------------  
//CMSYNIN  DD  DSN=&LIB1..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR            
//         DD  DSN=&LIB2..PARMLIB(&PARM1),DISP=SHR       * SGDB4000     
