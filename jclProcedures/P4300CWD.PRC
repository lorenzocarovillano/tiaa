//P4300CWD PROC DBID='045',
//         DMPS='U',
//         JCLO='*',
//         LIB1='PROD',
//         LIB2='PROD',
//         NAT='NAT045P',
//         NATMEM='PANNSEC',
//         REGN1='5M'
//**********************************************************************
//* INITIAL LOADING OF ENHANCED WORK LISTS TO THE SCRATCHPAD
//* 05/10/1999 - FELICJU
//**********************************************************************
//EXTR010  EXEC PGM=NATB030,REGION=&REGN1,
//     PARM='SYS=&NAT'
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPF&DBID),DISP=SHR
//SYSPRINT DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMPRT01  DD  SYSOUT=&JCLO
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P4300CW1),DISP=SHR
