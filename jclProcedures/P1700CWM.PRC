//P1700CWM PROC                                                         
//* --------------------------------------------------------------------
//INIT005  EXEC PGM=IDCAMS                                              
//DD1      DD  DUMMY                                                    
//DD2      DD  DSN=PCWFL.COR.P1700CWM.WPID.S640,                        
//           DISP=OLD                                                   
//DD3      DD  DUMMY                                                    
//DD4      DD  DSN=PCWFL.COR.P1700CWM.REPT.S641,                        
//           DISP=OLD                                                   
//SYSPRINT DD  SYSOUT=*                                                 
//SYSIN    DD  DSN=PROD.DN.PARMLIB(P1700CWD),DISP=SHR                   
//* --------------------------------------------------------------------
//* CWFB4950 - CREATE WORK FILE OF DETAIL/CONTROL RECORDS TO BE READ    
//*            INTO CWFB4955                                            
//* --------------------------------------------------------------------
//EXTR010  EXEC PGM=NT2B030,REGION=5M,COND=(0,NE),                      
//         PARM='SYS=NT2050P'                                           
//STEPLIB  DD  DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR               
//DDCARD   DD  DSN=PROD.DN.PARMLIB(DBAPP050),DISP=SHR                   
//SYSPRINT DD  SYSOUT=*                                                 
//SYSOUT   DD  SYSOUT=*                                                 
//DDPRINT  DD  SYSOUT=*                                                 
//CMPRINT  DD  SYSOUT=*                                                 
//SYSUDUMP DD  SYSOUT=U                                                 
//CMPRT01  DD  SYSOUT=*                                                 
//CMWKF01  DD  DSN=PCWFL.COR.P1700CWM.WPID.S640,DISP=SHR                
//CMSYNIN  DD  DSN=PROD.NT2LOGON.PARMLIB(PANNSEC),DISP=SHR              
//         DD  DSN=PROD.DN.PARMLIB(P1700CWA),DISP=SHR                   
//* --------------------------------------------------------------------
//* CWFB4955 - READ DETAIL/CONTROL RECORDS AND CREATE REPORT FILE       
//*            SORTED BY WPID/REQUESTS COMPLETED                        
//* --------------------------------------------------------------------
//EXTR020  EXEC PGM=NT2B030,REGION=5M,COND=(0,NE),                      
//         PARM='SYS=NT2050P'                                           
//STEPLIB  DD  DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR               
//         DD  DSN=PROD.BATCH.LOADLIB,DISP=SHR                          
//DDCARD   DD  DSN=PROD.DN.PARMLIB(DBAPP050),DISP=SHR                   
//SYSPRINT DD  SYSOUT=*                                                 
//SYSOUT   DD  SYSOUT=*                                                 
//DDPRINT  DD  SYSOUT=*                                                 
//CMPRINT  DD  SYSOUT=*                                                 
//SYSUDUMP DD  SYSOUT=U                                                 
//CMPRT01  DD  SYSOUT=*                                                 
//CMWKF01  DD  DSN=PCWFL.COR.P1700CWM.WPID.S640,                        
//          DISP=SHR                                                    
//*                                                                     
//CMWKF02  DD  DSN=PCWFL.COR.P1700CWM.REPT.S641,                        
//          DISP=SHR                                                    
//CMSYNIN  DD  DSN=PROD.NT2LOGON.PARMLIB(PANNSEC),DISP=SHR              
//         DD  DSN=PROD.DN.PARMLIB(P1700CWB),DISP=SHR                   
//* --------------------------------------------------------------------
//* CWFB4960 - READ REPORT FILE/SORT/FORMAT AND PRINT REPORT            
//* --------------------------------------------------------------------
//REPT030  EXEC PGM=NT2B030,REGION=5M,COND=(0,NE),                      
//         PARM='SYS=NT2050P'                                           
//STEPLIB  DD  DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR               
//DDCARD   DD  DSN=PROD.DN.PARMLIB(DBAPP050),DISP=SHR                   
//ROCDB045 DD DUMMY                                                     
//SYSPRINT DD  SYSOUT=*                                                 
//SYSOUT   DD  SYSOUT=*                                                 
//DDPRINT  DD  SYSOUT=*                                                 
//CMPRINT  DD  SYSOUT=*                                                 
//SYSUDUMP DD  SYSOUT=U                                                 
//CMPRT01  DD  SYSOUT=(8,CW1700M1)                                      
//CMWKF01  DD  DSN=PCWFL.COR.P1700CWM.REPT.S641,                        
//          DISP=SHR                                                    
//CMSYNIN  DD  DSN=PROD.NT2LOGON.PARMLIB(PANNSEC),DISP=SHR              
//         DD  DSN=PROD.DN.PARMLIB(P1700CWC),DISP=SHR                   
