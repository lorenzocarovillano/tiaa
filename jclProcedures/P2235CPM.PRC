//P2235CPM PROC DBID='003',
//         DMPS='U',
//         GDG1='+1',
//         HLQ0='PIA.ANN',
//         HLQ1='PINT.ONL.ANN',
//         HLQ7='PPDT',
//         JCLO='*',
//         JOBNM1='P2235CPM',
//         JOBNM2='P2220CPM',
//         JOBNM3='P2210CPM',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='8M',
//         REPT='8',
//         REPTID3=CP2235M3,
//         SPC1='1,5',
//         UNIT1='CARTN'
//*
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
//*                                                                   *
//* PROCEDURE:  P2235CPM                 DATE:  04/18/96              *
//*                                                                   *
//*********************************************************************
//*
//* CPS AP   GLOBAL PAY FILE CREATION                                 *
//*
//* EXTR010 - CREATE A POSITIVE PAY FILE TO BE SEND TO THE BANK
//*           THE INPUT FILE IS THE PROCESSED GLOBAL PAY FILE
//*           NATURAL PROGRAM  FCPP831
//* COPY020 - BACKUP THE POSITIVE PAY FILE TO A CART
//*******************************************************************
//*                                                                     
//* MODIFICATION HISTORY
//* 04/19/02 - OIA JCL STANDARDS - PHIL STEINHAUSER
//*                                                                     
//*******************************************************************
//* EXTR010 - CREATE A POSITIVE PAY FILE TO BE SEND TO THE BANK
//*           THE INPUT FILE IS THE PROCESSED GLOBAL PAY FILE
//*           NATURAL PROGRAM  FCPP831
//**********************************************************************
//*                                                                     
//EXTR010 EXEC PGM=NATB030,REGION=&REGN1,
//    PARM='SYS=&NAT,UDB=013,OPRB=(.DBID=253,FNR=233,I,A)'
//SYSABOUT DD SYSOUT=&DMPS
//SYSDBOUT DD SYSOUT=&DMPS
//SYSUDUMP DD SYSOUT=&DMPS
//SYSOUT   DD SYSOUT=&JCLO
//SYSPRINT DD SYSOUT=&JCLO
//CMPRINT  DD SYSOUT=&JCLO
//CMPRT01  DD SYSOUT=&REPT
//CMPRT02  DD SYSOUT=&REPT
//CMPRT15  DD SYSOUT=&REPT
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT  DD SYSOUT=&JCLO
//IAA991   DD DSN=&HLQ1..IALEGAL.ADDRESS.V175,DISP=SHR
//CMWKF01  DD DSN=&HLQ0..&JOBNM2..GLOBAL4.S473,DISP=SHR
//CMWKF02  DD DSN=&HLQ0..GLOBAL.PAY.TRNSMSN(&GDG1),
//            DISP=(NEW,CATLG,DELETE),UNIT=SYSDA,
//            DCB=MODLDSCB,RECFM=LS,LRECL=80,
//            SPACE=(CYL,(&SPC1),RLSE)
//CMWKF03  DD DSN=&HLQ0..GLOBAL.PAY.NEWEXCEL.FILE,
//            DISP=OLD
//CMWKF15  DD DSN=&HLQ0..&JOBNM3..CONTROLS.S472,DISP=SHR
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(P2235CP1),DISP=SHR
//* FCPP831                                                             
//         DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//*******************************************************************
//* COPY020 - BACKUP THE POSITIVE PAY FILE TO A CART
//*******************************************************************
//COPY020  EXEC PGM=IEBGENER,COND=(4,LT)
//SYSABOUT DD SYSOUT=&DMPS
//SYSUDUMP DD SYSOUT=&DMPS
//SYSPRINT DD SYSOUT=&DMPS
//SYSUT1   DD DSN=&HLQ0..GLOBAL.PAY.TRNSMSN(&GDG1),
//            DISP=SHR,UNIT=SYSDA
//SYSUT2   DD DSN=&HLQ7..&JOBNM1..GLOBAL.PAY.TRNSMSN.VR(&GDG1),
//            DISP=(NEW,CATLG,DELETE),UNIT=&UNIT1,
//            DCB=MODLDSCB,RECFM=LS,LRECL=80
//SYSIN    DD DUMMY
//*--------------------------------------------------------------------*
//* SORT030  SORT NEW GLOBAL PAY WORLD LINK FILE
//*--------------------------------------------------------------------*
//*
//SORT030  EXEC PGM=SORT,REGION=&REGN1,COND=(0,NE)
//SYSOUT   DD SYSOUT=&DMPS
//SORTIN   DD DSN=&HLQ0..GLOBAL.PAY.NEWEXCEL.FILE,DISP=SHR
//SORTOUT  DD SYSOUT=(&REPT,&REPTID3)
//SYSIN    DD DSN=&LIB1..PARMLIB(&REPTID3),DISP=SHR
//*
//* SORT FIELDS=(295,03,A,       : CURRENCY CODE
//*              241,10,A,       : COUNTRY NAME
//*              086,10,A),      : CONTRACT
//*              FORMAT=CH
//*
