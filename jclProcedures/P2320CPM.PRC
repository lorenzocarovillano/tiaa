//P2320CPM PROC DBID='003',
//         DMPS='U',
//         HLQ0='PIA.ANN',
//         HLQ7='PPDT',
//         HLQ8='PPDD',
//         JCLO='*',
//         JOBNM='P2320CPM',
//*        JOBNM2='P2230CPM',
//         JOBNM3='P2260CPM',
//         JOBNM4='P2210CPM',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='8M',
//         REPT='8',
//         RPTID='CP2320MG',
//         SPC1='200,50',
//         SPC2='850,10',
//         SPC3='300,50',
//         SPC4='5,1',
//         UNIT1='CARTV'
//*                                                                     
//*-------------------------------------------------------------------*
//* PROCEDURE:  P2320CPM                 DATE:  02/29/96              *
//*-------------------------------------------------------------------*
//* SORT010 - COMBINES THE PAYMENT FILES FROM "P2230CPM" & "P2260CPM" *
//*           AND SORTS THEM BY : COMBINE NUMBER (ASCENDING) / PPCN   *
//*           NUMBER (DESCENDING).                                    *
//* SORT BY:                                                          *
//* CNTRCT-CMBN-NBR           25,14                                   *
//* CNTRCT-PPCN-NBR           42,10                                   *
//*-------------------------------------------------------------------*
//* REPT020 - GENERATES AFTER CHECKS PRINTING REPORTS (PAYMENT        *
//*           REGISTERS).                                             *
//*   RPT 1 - PAYMENT REGISTER OF ALL PROCESSED CONTRACTS REPORT.     *
//*   RPT 2 - TOTAL PAYMENTS BY FUND REPORT.                          *
//*   RPT 3 - GLOBAL PAY REGISTER REPORT.                             *
//*   RPT 4 - "IRS" LIENS REPORT.                                     *
//*   RPT 5 - ANNUITY PAYMENTS REPORT.                                *
//*           NATURAL PROGRAM FCPP811.                                *
//*-------------------------------------------------------------------*
//* COPY023 - GENER THE PAYMENT REGISTER OF ALL PROCESSED CONTRACTS   *
//*           REPORT TO CARTRIDGE FOR MICROFILM.                      *
//*           UTILITY PROGRAM "IEBGENER".                             *
//*-------------------------------------------------------------------*
//* COPY026 - GENER THE PAYMENT REGISTER OF ALL PROCESSED CONTRACTS   *
//*           REPORT TO "ARMD" (SYSOUT=&REPT).
//*           UTILITY PROGRAM "IEBGENER".                             *
//*-------------------------------------------------------------------*
//* REPT030 - GENERATES REPORTS OF ANNUITY PAYMENTS WITHOUT TAX       *
//*           OR VOLUNTARY WITHOLDINGS.                               *
//*   PRT 1 - ANNUITY PAYMENTS, TAXES NOT WITHHELD REPORT.            *
//*   PRT 2 - ANNUITY PAYMENTS, VOLUNTARY DEDUCTIONS NOT TAKEN.       *
//*           NATURAL PROGRAM FCPP812                                 *
//*-------------------------------------------------------------------*
//* SORT040 - COMBINES THE PAYMENT FILES FROM "P2230CPM" & "P2260CPM" *
//*           SELECTS PAYMENTS WITH CONTRACT ANNUITY INSURANCE TYPE   *
//*           CODE "G" FOR GROUP ANNUITY, AND SORTS THEM BY :         *
//*           PPCN NUMBER / PAYEE CODE                                *
//* SORT BY:                                                          *
//* CNTRCT-PPCN-NBR           42,10                                   *
//* CNTRCT-PAYEE-CDE          52,4                                    *
//*-------------------------------------------------------------------*
//* SPLIT045 - SPLIT THE FILE WHICH IS GENERATED FROM PREVIOUS SORT040 *
//*           STEP FOR HARVARD GROUP ANNUITY ONLY.                    *
//*           NATURAL PROGRAM FCPP855                                 *
//*-------------------------------------------------------------------*
//* REPT050 - GENERATES A REPORT OF ALL GROUP ANNUITY PAYMENT         *
//*           REGISTER CREATED IN THE PREVIOUS SORT STEP "SORT040".   *
//*   PRT 1 - ANNUITY PAYMENTS, GROUP ANNUITY PAYMENT REGISTER REPORT.*
//*           NATURAL PROGRAM FCPP813                                 *
//*-------------------------------------------------------------------*
//* REPT055 - GENERATES A REPORT OF ALL HARVARD GROUP ANNUITY PAYMENT *
//*           REGISTER CREATED IN THE PREVIOUS SPLIT STEP "SPLIT045.  *
//*   PRT 1 - ANNUITY PAYMENTS, GROUP ANNUITY PAYMENT REGISTER REPORT.*
//*           NATURAL PROGRAM FCPP856                                 *
//*-------------------------------------------------------------------*
//* SORT060 - SORTS THE NEGATIVE NET INCOME DATA FROM "P2210CPM" BY : *
//*           COMBINE NUMBER / PPCN NUMBER.                           *
//* SORT BY:                                                          *
//* CNTRCT-CMBN-NBR                25,14                              *
//* CNTRCT-PPCN-NBR                42,10                              *
//*-------------------------------------------------------------------*
//* REPT070 - GENERATES A REPORT OF ALL NEGATIVE NET INCOME PAYMENTS  *
//*           PRODUCED FROM THE PREVIOUS SORT STEP "SORT060".         *
//*   PRT 1 - ANNUITY PAYMENTS, NEGATIVE NET INCOME REPORT.           *
//*           NATURAL PROGRAM FCPP832                                 *
//*-------------------------------------------------------------------*
//* SORT080 - SORTS THE NEGATIVE NET INCOME DATA FROM "P2210CPM" BY : *
//*           COMBINE NUMBER / PPCN NUMBER.                           *
//* SORT BY:                                                          *
//* CNTRCT-PPCN-NBR                52,12                              *
//*-------------------------------------------------------------------*
//* REPT090 - GENERATES A REPORT OF ALL NEGATIVE NET INCOME PAYMENTS  *
//*           PRODUCED FROM THE PREVIOUS SORT STEP "SORT080".         *
//*   PRT 1 - ANNUITY PMTS, NEGATIVE NET INCOME RPT FOR UPENN ONLY
//*           NATURAL PROGRAM FCPP814                                 *
//*                                                                     
//*-------------------------------------------------------------------*
//* MODIFICATION HISTORY
//* 04/23/02 - OIA JCL STANDARDS - PHIL STEINHAUSER
//* 04/01/04 - STEP COPY023 DD SYSUT2 - CHANGED CART TO CARTDN
//* 02/21/06 - PAYEE-MATCH-PROJECT - RAMANA ANNE
//*            USED NEW DATASET WHICH CREATED IN P2260CPM JOB
//*            CHANGED SORT CRITERIA IN P2320CP1,P2320CP4               
//* 03/28/08 - ADDED STPS 080, 090 FOR UPENN GRP ANNTY RPT FCPP814.     
//*
//**********************************************************************
//* SORT010 - COMBINES THE PAYMENT FILES FROM "P2230CPM" & "P2260CPM" *
//*           AND SORTS THEM BY : COMBINE NUMBER (ASCENDING) / PPCN   *
//*           NUMBER (DESCENDING).                                    *
//* SORT BY:                                                          *
//* CNTRCT-CMBN-NBR           25,14                                   *
//* CNTRCT-PPCN-NBR           42,10                                   *
//*-------------------------------------------------------------------*
//SORT010  EXEC PGM=SORT,COND=(0,NE)
//SYSOUT    DD SYSOUT=&JCLO
//SORTWK01  DD UNIT=SYSDA,SPACE=(CYL,(&SPC1))
//SORTWK02  DD UNIT=SYSDA,SPACE=(CYL,(&SPC1))
//SORTWK03  DD UNIT=SYSDA,SPACE=(CYL,(&SPC1))
//SORTWK04  DD UNIT=SYSDA,SPACE=(CYL,(&SPC1))
//SORTWK05  DD UNIT=SYSDA,SPACE=(CYL,(&SPC1))
//SORTWK06  DD UNIT=SYSDA,SPACE=(CYL,(&SPC1))
//SORTIN    DD DSN=&HLQ0..&JOBNM3..CHECKS.NO.EXTRA.FIELDS.S477,
//             DISP=SHR
//          DD DSN=&HLQ0..&JOBNM3..NONCHK.UPDATED.S474E,
//             DISP=SHR
//SORTOUT   DD DSN=&HLQ8..&JOBNM..SORTED.PYMNTS,
//             SPACE=(CYL,(&SPC2),RLSE),
//             DISP=(NEW,CATLG,DELETE),
//             UNIT=(SYSDA,2)
//SYSIN     DD DSN=&LIB1..PARMLIB(P2320CP1),DISP=SHR
//* SORT FIELDS=(29,14,CH,A,46,10,CH,D)
//* SORT FIELDS=(39,14,CH,A,56,10,CH,D) - NEW
//*
//* SORT BY:
//* CNTRCT-CMBN-NBR                25,14
//* CNTRCT-PPCN-NBR  (DESCENDING)  42,10
//*
//*-------------------------------------------------------------------*
//* REPT020 - GENERATES AFTER CHECKS PRINTING REPORTS (PAYMENT        *
//*           REGISTERS).                                             *
//*   RPT 1 - PAYMENT REGISTER OF ALL PROCESSED CONTRACTS REPORT.     *
//*   RPT 2 - TOTAL PAYMENTS BY FUND REPORT.                          *
//*   RPT 3 - GLOBAL PAY REGISTER REPORT.                             *
//*   RPT 4 - "IRS" LIENS REPORT.                                     *
//*   RPT 5 - ANNUITY PAYMENTS REPORT.                                *
//*           NATURAL PROGRAM FCPP811.                                *
//*-------------------------------------------------------------------*
//REPT020  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//        PARM='SYS=&NAT'
//SYSUDUMP  DD SYSOUT=&DMPS
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMPRINT   DD SYSOUT=&JCLO
//CMPRT01   DD DSN=&HLQ8..&JOBNM..PYMNT.REG,
//             SPACE=(CYL,(&SPC2),RLSE),DISP=(NEW,CATLG,DELETE),
//             UNIT=SYSDA,RECFM=LS,LRECL=133
//CMPRT02   DD SYSOUT=&REPT
//CMPRT03   DD SYSOUT=&REPT
//CMPRT04   DD SYSOUT=&REPT
//CMPRT05   DD SYSOUT=&REPT
//CMPRT06   DD SYSOUT=&REPT
//CMPRT07   DD SYSOUT=&REPT
//CMWKF01   DD DSN=&HLQ8..&JOBNM..SORTED.PYMNTS,DISP=SHR
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(P2320CP2),DISP=SHR
//* FCPP811
//*-------------------------------------------------------------------*
//* COPY023 - GENER THE PAYMENT REGISTER OF ALL PROCESSED CONTRACTS   *
//*           REPORT TO CARTRIDGE FOR MICROFILM.                      *
//*           UTILITY PROGRAM "IEBGENER".                             *
//*-------------------------------------------------------------------*
//COPY023  EXEC PGM=IEBGENER,COND=(0,NE)
//SYSPRINT  DD SYSOUT=&JCLO
//SYSIN     DD DUMMY
//SYSUT1    DD DSN=&HLQ8..&JOBNM..PYMNT.REG,DISP=SHR
//SYSUT2    DD DSN=&HLQ7..MICROFLM.PYMNT.REG,
//             DISP=(NEW,CATLG,DELETE),
//             UNIT=&UNIT1,
//             RECFM=LS,LRECL=133
//*-------------------------------------------------------------------*
//* COPY026 - GENER THE PAYMENT REGISTER OF ALL PROCESSED CONTRACTS   *
//*           REPORT TO "ARMD" (SYSOUT=&REPT).
//*           UTILITY PROGRAM "IEBGENER".                             *
//*-------------------------------------------------------------------*
//COPY026  EXEC PGM=IEBGENER,COND=(0,NE)
//SYSPRINT  DD  SYSOUT=&JCLO
//SYSIN     DD  DUMMY
//SYSUT1    DD  DSN=&HLQ8..&JOBNM..PYMNT.REG,DISP=SHR
//SYSUT2    DD  SYSOUT=&REPT
//*
//*-------------------------------------------------------------------*
//* REPT030 - GENERATES REPORTS OF ANNUITY PAYMENTS WITHOUT TAX       *
//*           OR VOLUNTARY WITHOLDINGS.                               *
//*   PRT 1 - ANNUITY PAYMENTS, TAXES NOT WITHHELD REPORT.            *
//*   PRT 2 - ANNUITY PAYMENTS, VOLUNTARY DEDUCTIONS NOT TAKEN.       *
//*           NATURAL PROGRAM FCPP812                                 *
//*-------------------------------------------------------------------*
//REPT030  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//        PARM='SYS=&NAT'
//SYSOUT    DD SYSOUT=&JCLO
//SYSUDUMP  DD SYSOUT=&DMPS
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMPRINT   DD SYSOUT=&JCLO
//CMPRT01   DD SYSOUT=&REPT
//CMPRT02   DD SYSOUT=&REPT
//SORTWK01  DD UNIT=SYSDA,SPACE=(CYL,(&SPC2))
//SORTWK02  DD UNIT=SYSDA,SPACE=(CYL,(&SPC2))
//SORTWK03  DD UNIT=SYSDA,SPACE=(CYL,(&SPC2))
//SORTWK04  DD UNIT=SYSDA,SPACE=(CYL,(&SPC2))
//SORTWK05  DD UNIT=SYSDA,SPACE=(CYL,(&SPC2))
//SORTWK06  DD UNIT=SYSDA,SPACE=(CYL,(&SPC2))
//CMWKF01   DD DSN=&HLQ0..&JOBNM4..CONTROLS.S472,DISP=SHR
//CMWKF02   DD DSN=&HLQ0..&JOBNM4..REJECT.ERRORS.S464,DISP=SHR
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(P2320CP3),DISP=SHR
//* FCPP812
//*-------------------------------------------------------------------*
//* SORT040 - COMBINES THE PAYMENT FILES FROM "P2230CPM" & "P2260CPM" *
//*           SELECTS PAYMENTS WITH CONTRACT ANNUITY INSURANCE TYPE   *
//*           CODE "G" FOR GROUP ANNUITY, AND SORTS THEM BY :         *
//*           PPCN NUMBER / PAYEE CODE                                *
//* SORT BY:                                                          *
//* CNTRCT-PPCN-NBR           42,10                                   *
//* CNTRCT-PAYEE-CDE          52,4                                    *
//*-------------------------------------------------------------------*
//SORT040  EXEC PGM=SORT,COND=(0,NE)
//SYSOUT    DD SYSOUT=&JCLO
//SYSUDUMP  DD SYSOUT=&DMPS
//SORTWK01  DD UNIT=SYSDA,SPACE=(CYL,(&SPC1))
//SORTWK02  DD UNIT=SYSDA,SPACE=(CYL,(&SPC1))
//SORTWK03  DD UNIT=SYSDA,SPACE=(CYL,(&SPC1))
//SORTWK04  DD UNIT=SYSDA,SPACE=(CYL,(&SPC1))
//SORTWK05  DD UNIT=SYSDA,SPACE=(CYL,(&SPC1))
//SORTWK06  DD UNIT=SYSDA,SPACE=(CYL,(&SPC1))
//SORTIN    DD DSN=&HLQ0..&JOBNM3..CHECKS.NO.EXTRA.FIELDS.S477,
//             DISP=SHR
//          DD DSN=&HLQ0..&JOBNM3..NONCHK.UPDATED.S474E,
//             DISP=SHR
//SORTOUT   DD DSN=&HLQ8..&JOBNM..GRP.ANNTY.PYMNT.REG,                  
//             SPACE=(CYL,(&SPC3),RLSE),DISP=(NEW,CATLG,DELETE),
//             UNIT=(SYSDA,2)
//SYSIN     DD DSN=&LIB1..PARMLIB(P2320CP4),DISP=SHR
//* INCLUDE COND=(941,1,CH,EQ,C'G')  CNTRCT-ANNTY-INS-TYPE = 'G'
//* INCLUDE COND=(951,1,CH,EQ,C'G')  CNTRCT-ANNTY-INS-TYPE = 'G'
//* INCLUDE COND=(956,1,CH,EQ,C'G')  CNTRCT-ANNTY-INS-TYPE = 'G'
//* SORT FIELDS=(56,10,CH,A,66,4,CH,A) - NEW
//* SORT FIELDS=(46,10,CH,A,56,4,CH,A)
//*
//* SORT BY:
//* CNTRCT-PPCN-NBR           42,10
//* CNTRCT-PAYEE-CDE          52,4
//*-------------------------------------------------------------------*
//* SPLIT045 - SPLIT THE FILE WHICH IS GENERATED FROM PREVIOUS SORT040 *
//*           STEP FOR HARVARD GROUP ANNUITY ONLY.                    *
//*-------------------------------------------------------------------*
//SPLIT045 EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//        PARM='SYS=&NAT'
//SYSUDUMP  DD SYSOUT=&DMPS
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMPRINT   DD SYSOUT=&JCLO
//CMWKF01   DD DSN=&HLQ8..&JOBNM..GRP.ANNTY.PYMNT.REG,DISP=SHR
//CMWKF02   DD DSN=&HLQ8..&JOBNM..HARVDGRP.ANTY.PYMNT.REG,
//             SPACE=(CYL,(&SPC3),RLSE),DISP=(NEW,CATLG,DELETE),
//             DCB=MODLDSCB,RECFM=VB,LRECL=4875,
//             UNIT=(SYSDA,2)
//CMWKF03   DD DSN=&HLQ8..&JOBNM..HARVDGRP.ANTY.PYMNT.TEMP1,
//             SPACE=(CYL,(&SPC3),RLSE),DISP=(NEW,CATLG,DELETE),
//             DCB=MODLDSCB,RECFM=VB,LRECL=4875,
//             UNIT=(SYSDA,2)
//CMWKF04   DD DSN=&HLQ8..&JOBNM..HARVDGRP.ANTY.PYMNT.TEMP2,
//             SPACE=(CYL,(&SPC3),RLSE),DISP=(NEW,CATLG,DELETE),
//             DCB=MODLDSCB,RECFM=VB,LRECL=4875,
//             UNIT=(SYSDA,2)
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(P2320CP8),DISP=SHR
//* FCPP855
//*-------------------------------------------------------------------*
//* REPT050 - GENERATES A REPORT OF ALL GROUP ANNUITY PAYMENT         *
//*           REGISTER CREATED IN THE PREVIOUS SORT STEP "SORT040".   *
//*   PRT 1 - ANNUITY PAYMENTS, GROUP ANNUITY PAYMENT REGISTER REPORT.*
//*           NATURAL PROGRAM FCPP813                                 *
//*-------------------------------------------------------------------*
//REPT050  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//        PARM='SYS=&NAT'
//SYSUDUMP  DD  SYSOUT=&DMPS
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMPRINT   DD  SYSOUT=&JCLO
//CMPRT01   DD  SYSOUT=&REPT
//CMPRT02   DD  SYSOUT=&REPT                 NEW 120607 LCW
//CMWKF01   DD  DSN=&HLQ8..&JOBNM..GRP.ANNTY.PYMNT.REG,DISP=SHR         
//CMSYNIN   DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD  DSN=&LIB1..PARMLIB(P2320CP5),DISP=SHR
//* FCPP813
//*
//*-------------------------------------------------------------------*
//* SORT060 - SORTS THE NEGATIVE NET INCOME DATA FROM "P2210CPM" BY : *
//*           COMBINE NUMBER / PPCN NUMBER.                           *
//* SORT BY:                                                          *
//* CNTRCT-CMBN-NBR                25,14                              *
//* CNTRCT-PPCN-NBR                42,10                              *
//*-------------------------------------------------------------------*
//* REPT055 - GENERATES A REPORT OF ALL HARVARD GROUP ANNUITY PAYMENT
//*           REGISTER CREATED IN THE PREVIOUS SPLIT STEP "SPLIT045"  *
//*   PRT 1 - ANNUITY PAYMENTS, GROUP ANNUITY PAYMENT REGISTER REPORT *
//*           FOR HARVARD NATURAL PROGRAM FCPP856        *
//*-------------------------------------------------------------------*
//REPT055  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//        PARM='SYS=&NAT'
//SYSUDUMP  DD  SYSOUT=&DMPS
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMPRINT   DD  SYSOUT=&JCLO
//CMPRT01   DD  SYSOUT=(8,CP2320MD)
//CMWKF01   DD  DSN=&HLQ8..&JOBNM..HARVDGRP.ANTY.PYMNT.REG,DISP=SHR
//CMSYNIN   DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD  DSN=&LIB1..PARMLIB(P2320CP9),DISP=SHR
//* FCPP856
//*
//*-------------------------------------------------------------------*
//SORT060  EXEC PGM=SORT,COND=(0,NE)
//SYSOUT    DD SYSOUT=&JCLO
//SORTWK01  DD UNIT=SYSDA,SPACE=(CYL,(&SPC2))
//SORTWK02  DD UNIT=SYSDA,SPACE=(CYL,(&SPC2))
//SORTWK03  DD UNIT=SYSDA,SPACE=(CYL,(&SPC2))
//SORTWK04  DD UNIT=SYSDA,SPACE=(CYL,(&SPC2))
//SORTWK05  DD UNIT=SYSDA,SPACE=(CYL,(&SPC2))
//SORTWK06  DD UNIT=SYSDA,SPACE=(CYL,(&SPC2))
//SORTIN    DD DSN=&HLQ0..&JOBNM4..REJECT.ERRORS.S464,DISP=SHR
//SORTOUT   DD DSN=&HLQ8..&JOBNM..NEGATIVE.NET.SORT,
//             SPACE=(CYL,(&SPC4),RLSE),
//             DISP=(NEW,CATLG,DELETE),
//             UNIT=SYSDA
//SYSIN     DD DSN=&LIB1..PARMLIB(P2320CP6),DISP=SHR
//* SORT FIELDS=(29,14,CH,A,46,10,CH,A)
//* SORT BY:
//* CNTRCT-CMBN-NBR                25,14
//* CNTRCT-PPCN-NBR                42,10
//*-------------------------------------------------------------------*
//* REPT070 - GENERATES A REPORT OF ALL NEGATIVE NET INCOME PAYMENTS  *
//*           PRODUCED FROM THE PREVIOUS SORT STEP "SORT060".         *
//*   PRT 1 - ANNUITY PAYMENTS, NEGATIVE NET INCOME REPORT.           *
//*           NATURAL PROGRAM FCPP832                                 *
//*-------------------------------------------------------------------*
//REPT070  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//        PARM='SYS=&NAT'
//SYSUDUMP  DD  SYSOUT=&DMPS
//DDCARD    DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMPRINT   DD  SYSOUT=&JCLO
//CMPRT01   DD  SYSOUT=&REPT
//CMWKF01   DD  DSN=&HLQ0..&JOBNM4..CONTROLS.S472,DISP=SHR
//CMWKF02   DD  DSN=&HLQ8..&JOBNM..NEGATIVE.NET.SORT,DISP=SHR
//CMSYNIN   DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD  DSN=&LIB1..PARMLIB(P2320CP7),DISP=SHR
//* FCPP832
//*                                                                     
//********************************************************************
//* SORT080 - SORTS GRP.ANNTY.PYMNT.REG FILE FOR FEEDING TO REPT090,
//*           WHICH CREATES GRP ANNTY REPORT FOR UPENN ONLY.
//* -----------------------------------------------------------------
//SORT080  EXEC PGM=SORT,COND=(0,NE)
//SYSOUT    DD SYSOUT=&JCLO
//SYSUDUMP  DD SYSOUT=&DMPS
//SORTWK01  DD UNIT=SYSDA,SPACE=(CYL,(&SPC1))
//SORTWK02  DD UNIT=SYSDA,SPACE=(CYL,(&SPC1))
//SORTWK03  DD UNIT=SYSDA,SPACE=(CYL,(&SPC1))
//SORTWK04  DD UNIT=SYSDA,SPACE=(CYL,(&SPC1))
//SORTWK05  DD UNIT=SYSDA,SPACE=(CYL,(&SPC1))
//SORTWK06  DD UNIT=SYSDA,SPACE=(CYL,(&SPC1))
//*
//SORTIN    DD DSN=&HLQ8..&JOBNM..GRP.ANNTY.PYMNT.REG,
//             DISP=SHR
//SORTOUT   DD DSN=&HLQ8..&JOBNM..GRP.ANNTY.PYMNT.REG.SORT,
//             SPACE=(CYL,(&SPC3),RLSE),DISP=(NEW,CATLG,DELETE),
//             UNIT=(SYSDA,2)
//SYSIN     DD DSN=&LIB1..PARMLIB(P2320CPA),DISP=SHR
//*  SORT FIELDS=(52,12,CH,A)
//*
//********************************************************************
//* REPT090 - GENERATES A REPORT OF ALL GROUP ANNUITY PMT REGISTER
//*           CREATED IN THE PREV SORT STEP "SORT080".
//*   PRT 1 - ANNUITY PAYMENTS, GROUP ANNUITY PMT REG RPT FOR UPENN.
//*           NATURAL PROGRAM FCPP814
//* -----------------------------------------------------------------
//REPT090  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//        PARM='SYS=&NAT'
//SYSUDUMP  DD  SYSOUT=&DMPS
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMPRINT   DD  SYSOUT=&JCLO
//CMPRT01   DD  SYSOUT=(&REPT,&RPTID)
//CMWKF01   DD  DSN=&HLQ8..&JOBNM..GRP.ANNTY.PYMNT.REG.SORT,DISP=SHR
//CMSYNIN   DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD  DSN=&LIB1..PARMLIB(P2320CPB),DISP=SHR
//* FCPP814
//* -----------------------------------------------------------------
//*                 - END OF PROC P2320CPM -                            
