//P1100CWD PROC DBID='045',
//         DMPS='U',
//         JCLO='*',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         NAT='NAT045P',
//         NATMEM='PANNSEC',
//         REGN1='5M',
//         REPT='8'
//*
//**********************************************************************
//*
//* CWFB3014 - PRODUCE WORK REQUESTS CURRENTLY ACTIVE IN UNIT REPORT
//*
//**********************************************************************
//*
//* MODIFICATION HISTORY
//* 07/15/94 - CHANGED STEPLIB AS PER DONNA BUDALL
//* 04/15/02 - OIA JCL STANDARDS - PHIL STEINHAUSER
//*                                                                     
//**********************************************************************
//*
//REPT010  EXEC PGM=NATB030,REGION=&REGN1,
//         PARM='SYS=&NAT'
//SYSPRINT DD  SYSOUT=&JCLO
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSOUT   DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//CMPRT01  DD  SYSOUT=(&REPT,CW1100D1)
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P1100CW1),DISP=SHR
