//P1930IAM PROC DBID='003',
//         DMPS='U',
//         GDG0='+0',
//         GDG1='+1',
//         HLQ0='PIA.ANN',
//         HLQ1='PIA.ONL.ANN',
//***      HLQ2='PINT.ONL.ANN',
//         HLQ7='PPDT.PNA.IA',
//         HLQ8A='PPDD',
//         HLQ8B='PPDD.PNA.IA',
//         JCLO='*',
//         JOBNM='P1930IAM',
//         LIB1='PROD',                    PARMLIB
//         LIB2='PROD',                    NT2LOGON.PARMLIB
//         LIB3='PROD',                    CNTL.PARMLIB
//         INCLUDE='PSTPINCL',            INCLUDE-ORACLE INTERFACE PROD
//         LIBPART2='ORALOGON.NY.PARMLIB', PRODSSECURITY LIB FOR ORACLE
//         LOGMEM1='UP354SEC',             USER/PWD - ORACLE CONNECT    
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='0M',
//         REPT='8',
//         SPC1='2',
//         SPC2='1',
//         SPC3='5',
//         SPC4='1',
//         SPC5='6',
//         SPC6='2',
//         SPC7='550',
//         SPC8='50',
//         SPC9='100',
//         SPC10='100',
//         UNIT1='CARTV',
//         HLQDR=PPDT               DO NOT CHANGE HLQ FOR D/R USE
//*
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
//*                                                                   *
//* PROCEDURE:  P1930IAM                 DATE:  03/14/96              *
//*                                                                   *
//* ------------------------------------------------------------------*
//*                                                                   *
//* PROGRAMS EXECUTED: NT2B030 (FCPP150)                              *
//*                    NT2B030 (IAAP700)                              *
//*                    NT2B030 (IAAP701)                              *
//*                    NT2B030 (IAAP702)                              *
//* ------------------------------------------------------------------*
//* CHANGE HISTORY
//* 03/29/96   ADDED TRTCH=COMP                            P MOFFETT *
//* 03/20/98   ADDED DDNAME-->DIAUVS  TO (STEP010 & STEP030)         *
//*            ADDED DDNAME-->CMWKF09 TO STEP010 NEW PARM
//* 04/20/98   CHANGED LRECL=2343 TO 3583
//* 06/17/98   REMOVED COPYS STEPS TO P1931IAM
//* 08/12/98   ADDED  OPRB PARAMETER
//* 12/03/01   REMOVED LRECL PARMATER IN STEP EXTR010 & SORT020
//*                    RECFM=LS  ,LRECL=2479<-OLD 2481<-NEW RECSIZE
//*                    RECFM=LS  ,LRECL=2479<-OLD 2481<-NEW RECSIZE
//* 04/03/02 - OIA JCL STANDARDS - PHIL STEINHAUSER
//* 01/27/09 - ADDED EXTRACT OF TIAA ACCESS FOR OMNI TRADE - J. TINIO
//* 05/11/17 - REMOVED LRECL'S - OS                                     
//*********************************************************************
//*                                                                     
//EXTR005 EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),                   
//        PARM='SYS=&NAT'                                               
//SYSPRINT DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR                             
//DDPRINT  DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR                             
//CMPRINT  DD  SYSOUT=(&REPT,IA1930M4)                                  
//CMPRT01  DD  SYSOUT=(&REPT,IA1930M5)                                  
//CMPRT02  DD  SYSOUT=(&REPT,IA1930M6)                                  
//CMWKF01  DD  DSN=&HLQ8A..&JOBNM..AUDIT.HOLD,                          
//             UNIT=SYSDA,DISP=(NEW,CATLG,DELETE),                      
//             SPACE=(CYL,(&SPC1,&SPC2),RLSE),DATACLAS=DCPSEXTC,        
//             DCB=RECFM=LS                                             
//*            DCB=(RECFM=LS,LRECL=80)                                  
//CMWKF02  DD  DSN=&LIB3..CNTL.PARMLIB(IAMCKDT),DISP=SHR                
//SYSUDUMP DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL                            
//SYSABOUT DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL                            
//SYSDBOUT DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL                            
//SYSOUT   DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR                             
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR            
//         DD  DSN=&LIB1..PARMLIB(P1320IA1),DISP=SHR                    
//*------------------------------------------------------------------*  
//SORT007  EXEC PGM=SORT,COND=(0,NE)                                    
//SYSABOUT DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL                            
//SYSOUT   DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL                            
//SYSUDUMP DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL                            
//SORTWK01 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC3,&SPC4))                     
//SORTWK02 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC3,&SPC4))                     
//SORTWK03 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC3,&SPC4))                     
//SORTIN   DD  DSN=&HLQ8A..&JOBNM..AUDIT.HOLD,DISP=OLD                  
//SORTOUT  DD  DSN=&HLQ8A..&JOBNM..AUDIT.HOLD.SRTD,                     
//             DISP=(,CATLG,DELETE),UNIT=SYSDA,                         
//             SPACE=(CYL,(&SPC3,&SPC4),RLSE),DATACLAS=DCPSEXTC,        
//             DCB=RECFM=LS                                             
//*            DCB=(RECFM=LS,LRECL=80)                                  
//SYSIN    DD  DSN=&LIB1..PARMLIB(P1930IA4),DISP=SHR                    
//*------------------------------------------------------------------*  
//EXTR010 EXEC PGM=NATB030,REGION=&REGN1,
//        PARM=('SYS=&NAT',
//       'OPRB=(.DBID=253,FNR=043,I,A)'),COND=(0,NE)
//SYSPRINT  DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//*MSYS //     INCLUDE MEMBER=&INCLUDE
//SECFILE  DD  DSN=&LIB1..&LIBPART2.(&LOGMEM1.),DISP=SHR
//DDPRINT   DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRINT   DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRT01   DD SYSOUT=(&REPT,IA1930M1)
//CMWKF01   DD DSN=&HLQ0..IATRANS.FINAL(&GDG0),
//             DISP=SHR
//CMWKF02   DD DSN=&HLQ7..MNTHLY.COR.EXTRACT(&GDG0),
//             DISP=SHR,UNIT=&UNIT1
//CMWKF03   DD DSN=&HLQ0..NAMEADD(&GDG0),
//             DISP=SHR,UNIT=SYSDA
//CMWKF04   DD DSN=&HLQ7..CPS.TEMP(&GDG1),            OUTPUT
//             DISP=(,CATLG,DELETE),
//             UNIT=&UNIT1,
//             DCB=MODLDSCB,RECFM=LS
//CMWKF05   DD DSN=&HLQ8B..CPS.CNTL.TEMP,             OUTPUT
//             DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//             RECFM=LS
//*            RECFM=LS,LRECL=3583
//CMWKF07   DD DSN=&HLQ8B..CPS.TEMP6,                 OUTPUT
//             DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,SPACE=(CYL,(&SPC7,&SPC8),RLSE),
//             RECFM=LS   ,LRECL=2479 2481<-NEW
//CMWKF06  DD  DSN=&HLQ8A..&JOBNM..AUDIT.HOLD.SRTD,                     
//             DISP=SHR
//*
//CMWKF09  DD  DSN=&LIB1..PARMLIB(IAPARMP),DISP=SHR                     
//*
//SYSUDUMP  DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSABOUT  DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//SYSDBOUT  DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//SYSOUT    DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//DIAUVS    DD DSN=&HLQ1..DAILYAUV.V471,DISP=SHR
//*
//*** IAA470    DD DSN=&HLQ2..CREFFANN.UNITVALS.V164,DISP=SHR
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPF&DBID),DISP=SHR
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(P1930IA1),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//*------------------------------------------------------------------*
//SORT020  EXEC PGM=SORT,REGION=8M,COND=(0,NE)
//SYSABOUT DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSDBOUT DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSOUT   DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//SORTIN   DD DSN=&HLQ8B..CPS.TEMP6,DISP=SHR
//SORTOUT  DD DSN=&HLQ8B..TEMP6,
//             DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             UNIT=SYSDA,SPACE=(CYL,(&SPC7,&SPC8),RLSE),
//             RECFM=LS    ,LRECL=2479 2481<-NEW
//SORTWK01 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC9,&SPC10))                    
//SORTWK02 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC9,&SPC10))                    
//SORTWK03 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC9,&SPC10))                    
//SORTWK04 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC9,&SPC10))                    
//SORTWK05 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC9,&SPC10))                    
//SORTWK06 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC9,&SPC10))                    
//SORTWK07 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC9,&SPC10))                    
//SYSIN    DD  DSN=&LIB1..PARMLIB(P1930IA6),DISP=SHR                    
//*------------------------------------------------------------------*
//SORT030  EXEC PGM=SORT,REGION=&REGN1,COND=(0,NE)
//SYSABOUT DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSDBOUT DD  SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSOUT   DD  SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//SORTIN01 DD DSN=&HLQ7..CPS.TEMP(&GDG1),DISP=OLD,UNIT=&UNIT1
//SORTIN02 DD DSN=&HLQ8B..TEMP6,DISP=SHR
//SORTOUT  DD DSN=&HLQ7..CPS.FILE.SRTD(&GDG1),
//             DISP=(,CATLG,DELETE),
//             UNIT=&UNIT1,
//             DCB=MODLDSCB,RECFM=LS
//SORTWK01 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC9,&SPC10))                    
//SORTWK02 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC9,&SPC10))                    
//SORTWK03 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC9,&SPC10))                    
//SORTWK04 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC9,&SPC10))                    
//SORTWK05 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC9,&SPC10))                    
//SORTWK06 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC9,&SPC10))                    
//SORTWK07 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC9,&SPC10))                    
//SYSIN    DD  DSN=&LIB1..PARMLIB(P1930IA7),DISP=SHR                    
//*------------------------------------------------------------------*
//EXTR030 EXEC PGM=NATB030,REGION=&REGN1,
//        PARM='SYS=&NAT',COND=(0,NE)
//SYSPRINT  DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//DDPRINT   DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRINT   DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRT01   DD SYSOUT=(&REPT,IA1930M2)
//CMPRT02   DD SYSOUT=(&REPT,IA1930M7)
//CMWKF01   DD DSN=&HLQ7..CPS.FILE.SRTD(&GDG1),DISP=OLD,
//             UNIT=&UNIT1
//CMWKF02   DD DSN=&HLQDR..PNA.IA.CPS.FILE(&GDG1),              OUTPUT
//             DISP=(,CATLG,DELETE),
//             UNIT=&UNIT1,
//             DCB=MODLDSCB,RECFM=LS
//SYSUDUMP  DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSABOUT  DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//SYSDBOUT  DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//SYSOUT    DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//*
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPF&DBID),DISP=SHR
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(P1930IA2),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//*------------------------------------------------------------------*
//REPT040 EXEC PGM=NATB030,REGION=&REGN1,
//        PARM='SYS=&NAT',COND=(0,NE)
//SYSPRINT  DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//DDPRINT   DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRINT   DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRT01   DD SYSOUT=(&REPT,IA1930M3)
//CMPRT02   DD SYSOUT=(&REPT,IA1930M8)
//CMWKF01   DD DSN=&HLQDR..PNA.IA.CPS.FILE(&GDG1),              INPUT
//             DISP=OLD,UNIT=&UNIT1
//CMWKF02   DD DSN=&HLQ0..CPS.CNTL.FILE(&GDG1),
//             DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             UNIT=SYSDA,SPACE=(TRK,(&SPC5,&SPC6)),
//             DCB=MODLDSCB,RECFM=LS
//*            DCB=MODLDSCB,RECFM=LS,LRECL=3583
//CMWKF03   DD DSN=&HLQ8B..CPS.CNTL.TEMP,DISP=SHR       OUTPUT
//CMWKF04   DD DSN=&HLQ0..IA.OMNI.MNTHLY(&GDG1),        OMNI TRADE
//             DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             UNIT=SYSDA,SPACE=(CYL,(&SPC8,&SPC5)),
//             DCB=MODLDSCB,RECFM=LS
//SYSUDUMP  DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSABOUT  DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//SYSDBOUT  DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//SYSOUT    DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPF&DBID),DISP=SHR
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(P1930IA3),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
