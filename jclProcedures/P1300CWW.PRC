//P1300CWW PROC DBID='045',                                             
//         DMPS='U',                                                    
//         JCLO='*',                                                    
//         LIB1='PROD',             PARMLIB                             
//         LIB2='PROD',             NT2LOGON.PARMLIB                    
//         NAT='NAT045P',                                               
//         NATMEM='PANNSEC',                                            
//         REGN1='5M',                                                  
//         REPT='8'                                                     
//*                                                                     
//* --------------------------------------------------------------------
//*                                                                     
//* CWFB3905 - CREATE TURNAROUND REPORT FOR NARD SHAW                   
//*                                                                     
//* --------------------------------------------------------------------
//*                                                                     
//* MODIFICATION HISTORY                                                
//* 04/15/02 - OIA JCL STANDARDS - PHIL STEINHAUSER                     
//*                                                                     
//**********************************************************************
//*                                                                     
//REPT010  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),                  
//         PARM='SYS=&NAT'                                              
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//SYSPRINT DD  SYSOUT=&JCLO                                             
//SYSOUT   DD  SYSOUT=&JCLO                                             
//DDPRINT  DD  SYSOUT=&JCLO                                             
//CMPRINT  DD  SYSOUT=&JCLO                                             
//SYSUDUMP DD  SYSOUT=&DMPS                                             
//CMPRT01  DD  SYSOUT=(&REPT,CW1300W1)                                  
//CMPRT02  DD  SYSOUT=&DMPS                                             
//CMPRT03  DD  SYSOUT=&DMPS                                             
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR            
//         DD  DSN=&LIB1..PARMLIB(P1300CWA),DISP=SHR                    
