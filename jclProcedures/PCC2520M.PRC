//*********************************************************************
//*        PCC2025M  -   PROC FOR PRE-PROCESSING NET CHANGE FILES
//*********************************************************************
//PCC2025M PROC LIB1=PROD,
//         REGN1=0M,
//         NATLOGON=PANNSEC,
//         NAT=NATB030,
//         SYS=NAT045P,
//         DBID=045,
//         HLQD=PROD,
//         HLQ8=PPT.COR,
//         DMPS=U,
//         JCLO=*,
//         APPNM='PNTXXXXX',
//         PARMMEM1=PC2520M1,
//         PARMMEM2=PC2520M2,
//         PARMMEM3=PC2521M1,
//         NTCHGSPL='',
//         SRCFILE='TEMP.JOBNAME.SRCEFILE'
//*
//*********************************************************************
//*        USE SYNCSORT TO SELECT NET CHANGE DATADICT ENTRIES
//*********************************************************************
//**                                                                    
//SORT01   EXEC PGM=SORT                                                
//SYSOUT   DD  SYSOUT=&JCLO                                             
//SORTWK01 DD  SPACE=(CYL,(2,2),RLSE),                                  
//             UNIT=SYSDA                                               
//*
//SORTIN  DD DSN=&HLQD..DATADICT(MASTER),DISP=SHR                       
//SORTOUT DD DSN=&HLQ8..&APPNM..POST.DATADICT.SORT,                     
//     DISP=(,CATLG,DELETE),SPACE=(CYL,(1,1),RLSE),
//     DCB=RECFM=LS
//SYSIN   DD  DSN=&LIB1..PARMLIB(&PARMMEM1),DISP=SHR                    
//*
//*********************************************************************
//*    PCCB2520 - PREPARE NET CHANGE FILE BY ADDING MI RECORD
//*********************************************************************
//*
//PREPF010 EXEC PGM=&NAT,REGION=&REGN1,
//         PARM='SYS=&SYS,WH=ON'
//SYSPRINT DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMWKF01  DD  DSN=&SRCFILE,DISP=OLD
//CMWKF02  DD  DSN=&HLQ8..&APPNM..ECS.FILE,
//             UNIT=SYSDA,
//             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             SPACE=(CYL,(5,5),RLSE),
//             DCB=(DSORG=PS,RECFM=LS,LRECL=800)
//CMWKF03  DD  DSN=&HLQ8..&APPNM..POST.DATADICT.SORT,DISP=SHR
//CMWKF04  DD  DSN=&HLQ8..&APPNM..POST.DATADICT,
//             UNIT=SYSDA,
//             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             SPACE=(CYL,(1,1),RLSE),
//             DCB=(DSORG=PS,RECFM=LS,LRECL=300)
//CMWKF05  DD  DSN=&HLQ8..&APPNM..APPREC,
//             UNIT=SYSDA,
//             DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             SPACE=(CYL,(1,1),RLSE),
//             DCB=(DSORG=PS,RECFM=LS,LRECL=20)
//CMSYNIN  DD  DSN=&LIB1..NT2LOGON.PARMLIB(&NATLOGON),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(&PARMMEM2),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(&PARMMEM3),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//*
//SELECTMI EXEC PGM=SORT
//SYSOUT   DD  SYSOUT=*
//SORTIN   DD DSN=&HLQ8..&APPNM..ECS.FILE,
//         DISP=SHR
//SORTOUT DD DSN=&HLQ8..&APPNM..NETCHNG.SELECTMI,
//             DISP=(NEW,CATLG,DELETE),
//             SPACE=(CYL,(50,30),RLSE),
//             RECFM=LS,
//             UNIT=SYSDA
//SYSIN DD DSN=&LIB1..PARMLIB(PC3000X1),DISP=SHR
//*
//CHECK001 EXEC PGM=SORT,PARM='NULLOUT=RC4'
//SORTIN  DD DSN=&HLQ8..&APPNM..NETCHNG.SELECTMI,DISP=OLD
//SORTOUT DD DUMMY
//SYSOUT  DD SYSOUT=*
//SYSIN   DD DSN=&LIB1..PARMLIB(SORTCOPY),DISP=SHR
//*
//  IF CHECK001.RC = 0 THEN
//*********************************************************************
//*    PCC3000  - SPLITS NET CHANGE FILE INTO SMALLER FILES BASED ON
//*               THE PARM FILE. THIS PROGRAM USES DYNAMIC FILE
//*               ALLOCATION AND WILL CREATE THE FILES REQUIRED AT THE
//*               TIME OF EXECUTION.
//*********************************************************************
//*
//PCC3000  EXEC PGM=PCC3000,REGION=&REGN1,
//   PARM='&HLQ8..&APPNM..ECS.FILE'
//SYSOUT DD SYSOUT=*
//SYSIN    DD DSN=&LIB1..PARMLIB(&NTCHGSPL),DISP=SHR
//  ENDIF
//*
