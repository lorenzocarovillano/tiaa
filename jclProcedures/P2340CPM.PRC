//P2340CPM PROC DMPS='U',
//         HLQ0='PIA.ANN',
//         JCLO='*',
//         JOBNM='P2340CPM',
//         JOBNM1='P2210CPM',
//         LIB1='PROD',             PARMLIB
//         SPC1='50,50',
//         SPC2='5,1'
//*
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
//*                                                                   *
//* PROCEDURE:  P2340CPM                 DATE:  02/29/96              *
//*                                                                   *
//*********************************************************************
//*                                                                   *
//* CPS IA                                                            *
//*                                                                   *
//* SORT010 - SORT AND SUMMARIZE LEDGER FILE
//*                                                                   *
//* SORT020 - SORT AND SUMMARIZE SUMMARIZED-LEDGER FILE FOR             
//*           REFORMATING AND LOADING                                   
//*
//*                                                                   *
//*********************************************************************
//* SORT010 - SORT AND SUMMARIZE LEDGER FILE
//*********************************************************************
//SORT010  EXEC PGM=SORT,COND=(0,NE)
//SYSOUT   DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//SYSPRINT DD  SYSOUT=&JCLO
//SORTWK01 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC1)),DCB=BUFNO=10
//SORTWK02 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC1)),DCB=BUFNO=10
//SORTWK03 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC1)),DCB=BUFNO=10
//SORTWK04 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC1)),DCB=BUFNO=10
//SORTIN   DD  DSN=&HLQ0..&JOBNM1..LEDGER.S469,
//             DISP=SHR,DCB=BUFNO=25
//SORTOUT  DD  DSN=&HLQ0..&JOBNM..AP.LEDGER.SORT.S479,
//             DISP=OLD,DCB=BUFNO=27
//SYSIN    DD  DSN=&LIB1..PARMLIB(P2340CP1),DISP=SHR
//* SORT FIELDS=(1,2,CH,D,33,4,PD,A,20,4,CH,A,78,1,CH,A,53,2,CH,A,   *
//*           77,1,CH,D,55,15,CH,A)
//* SUM  FIELDS=(70,7,PD)
//*
//* SORT FIELDS:                                                        
//* CNTRCT-ORGN-CDE            1,2   DESC
//* PYMNT-CHECK-DTE           33,4
//* CNTRCT-ANNTY-INS-TYPE     20,1  (20,4)=20,21,22,23
//* CNTRCT-ANNTY-TYPE-CDE     21,1
//* CNTRCT-INSURANCE-OPTION   22,1
//* CNTRCT-LIFE-CONTINGENCY   23,1
//* CNTRCT-COMPANY-CDE        78,1
//* INV-ACCT-CDE              53,2
//* INV-ACCT-LEDGR-IND(DR/CR) 77,1   DESC
//* INV-ACCT-LEDGR-NBR        55,15
//* SUM
//* INV-ACCT-LEDGR-AMT        70,7
//*********************************************************************
//* SORT020 - SORT AND SUMMARIZE SUMMARIZED-LEDGER FILE FOR             
//*           REFORMATING AND LOADING                                   
//*********************************************************************
//SORT020  EXEC PGM=SORT,COND=(0,NE)
//SYSOUT   DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//SYSPRINT DD  SYSOUT=&JCLO
//SORTWK01 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC2)),DCB=BUFNO=10
//SORTWK02 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC2)),DCB=BUFNO=10
//SORTWK03 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC2)),DCB=BUFNO=10
//SORTWK04 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC2)),DCB=BUFNO=10
//SORTWK05 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC2)),DCB=BUFNO=10
//SORTWK06 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC2)),DCB=BUFNO=10
//SORTWK07 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC2)),DCB=BUFNO=10
//SORTIN   DD  DSN=&HLQ0..&JOBNM..AP.LEDGER.SORT.S479,DISP=SHR
//SORTOUT  DD  DSN=&HLQ0..&JOBNM..AP.LEDGER.S480,
//             DISP=OLD
//SYSIN    DD  DSN=&LIB1..PARMLIB(P2340CP2),DISP=SHR
//* SORT FIELDS=(1,2,CH,D,33,4,PD,A,20,1,CH,A,55,15,CH,A,77,1,CH,A)
//* SUM  FIELDS=(70,7,PD)
//*
//* CNTRCT-ORGN-CDE            1,2   DESC
//* PYMNT-CHECK-DTE           33,4
//* CNTRCT-ANNTY-INS-TYPE     20,1
//* INV-ACCT-LEDGR-NBR        55,15
//* INV-ACCT-LEDGR-IND(DR/CR) 77,1
//* SUM
//* INV-ACCT-LEDGR-AMT        70,7
