//PCP1480X PROC
//*
//*--------------------------------------------------------------------
//*--- CREATE FILES IF DO NOT EXIST
//*------------------------------------------------------------------
//PREP010      EXEC  PGM=IEFBR14
//DD1      DD  DSN=&HLQ1..&PJOBNM..FTEPARM.&FT1,
//             DISP=(MOD,CATLG),DATACLAS=DCPSEXTC,UNIT=SYSDA,
//             RECFM=LS,LRECL=200,SPACE=(TRK,(5,5),RLSE)
//DD2      DD  DSN=&HLQ1..&PJOBNM..&OR.TRG.&FT1,
//             DISP=(MOD,CATLG),SPACE=(80,1),UNIT=SYSDA,
//             RECFM=LS,LRECL=80,BLKSIZE=0,DATACLAS=DCPSEXTC
//*
//*--------------------------------------------------------------------
//*--- GENERATE CURRENT TIMESTAMP
//*------------------------------------------------------------------
//TSGEN   EXEC PGM=IRXJCL,
//        PARM='CURRTS SHORT'
//TSTAMP   DD DSN=&&TSTAMP,DISP=(,PASS)
//SYSTSPRT DD SYSOUT=*
//SYSEXEC  DD DSN=PROD.REXXLIB,DISP=SHR
//*
//*--------------------------------------------------------------------
//*--- PREPARE MQFTE PARM
//*------------------------------------------------------------------
//PARM020 EXEC PGM=IRXJCL,
// PARM='PARMSTS HL=&HL OR=&OR FT1=&FT1 G0=(0) SS=&SS FT=xml CJ=&CJ'
//*PARM='PARMSTS HL=&HL OR=&OR FT1=&FT1 G0=(0) SA=&SA DA=&DA SS=&SS FT=
//*            xml,CJ=&CJ'
//SYSEXEC  DD  DSN=PROD.REXXLIB,DISP=SHR
//SYSTSPRT DD  SYSOUT=*
//TEMPLATE DD  DSN=&LIB1..WMQFTE.PARMLIB(CP1480X1),DISP=SHR
//TSTAMP   DD  DSN=&&TSTAMP,DISP=(OLD,PASS)
//SUBSTED  DD  DSN=&HLQ1..&PJOBNM..FTEPARM.&FT1,
//             DISP=OLD
//*
//*--------------------------------------------------------------------
//*--- SEND CSV DATA
//*------------------------------------------------------------------
//SEND020 EXEC MQFTEBPX,
//   USRPARMS=&HLQ1..&PJOBNM..FTEPARM.&FT1
//*
//*--------------------------------------------------------------------
//*--- PREPARE MQFTE PARM
//*------------------------------------------------------------------
//PARM030 EXEC PGM=IRXJCL,
// PARM='PARMSTS HL=&HL OR=&OR FT1=&FT1 G0=(0) SS=&SS FT=ctrl CJ=&CJ'
//*
//SYSEXEC  DD  DSN=PROD.REXXLIB,DISP=SHR
//SYSTSPRT DD  SYSOUT=*
//TEMPLATE DD  DSN=&LIB1..WMQFTE.PARMLIB(CP1480X1),DISP=SHR
//TSTAMP   DD  DSN=&&TSTAMP,DISP=(OLD,PASS)
//SUBSTED  DD  DSN=&HLQ1..&PJOBNM..FTEPARM.&FT1,
//             DISP=OLD
//*
//*--------------------------------------------------------------------
//*--- SEND TRIGGER FILE
//*------------------------------------------------------------------
//SEND030 EXEC MQFTEBPX,
//   USRPARMS=&HLQ1..&PJOBNM..FTEPARM.&FT1
// IF RC=256 THEN
//*
//*--------------------------------------------------------------------
//*--- PREPARE MQFTE PARM
//*------------------------------------------------------------------
//PARM040 EXEC PGM=IRXJCL,
// PARM='PARMSTS HL=&HL OR=&OR FT1=&FT1 G0= SS=&SS FT=trg CJ=&CJ'
//*            &CJ'
//*PARM='PARMSTS HL=&HL OR= FT1=trg G0= SA=&SA DA=&DA SS=&SS FT=trg CJ=
//*            &CJ'
//SYSEXEC  DD  DSN=PROD.REXXLIB,DISP=SHR
//SYSTSPRT DD  SYSOUT=*
//TEMPLATE DD  DSN=&LIB1..WMQFTE.PARMLIB(CP1480X1),DISP=SHR
//TSTAMP   DD  DSN=&&TSTAMP,DISP=(OLD,PASS)
//SUBSTED  DD  DSN=&HLQ1..&PJOBNM..FTEPARM.&FT1,
//             DISP=OLD
//*
//*--------------------------------------------------------------------
//*--- SEND TRIGGER FILE
//*------------------------------------------------------------------
//SEND040 EXEC MQFTEBPX,
//   USRPARMS=&HLQ1..&PJOBNM..FTEPARM.&FT1
//* ENDIF
// ENDIF
// PEND
