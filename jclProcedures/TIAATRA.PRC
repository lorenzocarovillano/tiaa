//TIAATRA  PROC VPREFX='VXX',                    <VSAM FILE PREFIX
//            SPREFX='XX',                       <WORK FILE PREFIX
//            VFSET='PMT0',                      <FILESET ID
//            ADATE='CURR',
//            SYSROUT='*'                        <SYSOUT CLASS
//*-----------------------------------------------------------------*
//*  PROCESS CHECK FILE FOR ATRA RECON
//*-----------------------------------------------------------------*
//ATRAREC  EXEC PGM=TIAATRA,PARM='&ADATE.'
//SYSOUT   DD SYSOUT=&SYSROUT.
//SYSUDUMP DD SYSOUT=U
//SYSPRINT DD SYSOUT=&SYSROUT.
//ABNLTERM DD SYSOUT=&SYSROUT.
//*
//PMT0PA   DD DISP=SHR,DSN=&VPREFX..&VFSET.PA
//PMT0CKX  DD  DISP=SHR,DSN=&VPREFX..&VFSET.CK
//PMT0CK   DD  SUBSYS=(BLSR,'DDNAME=PMT0CKX,SHRPOOL=6',
//   'BUFND=250,BUFNI=250,HBUFND=400,HBUFNI=400')
//PMT0CK3  DD DISP=SHR,DSN=&VPREFX..&VFSET.CKA3,
//         AMP=('ACCBIAS=SO')
//PMT0DSX  DD DISP=SHR,DSN=&VPREFX..&VFSET.DS
//PMT0DS   DD  SUBSYS=(BLSR,'DDNAME=PMT0DSX,SHRPOOL=7',
//   'BUFND=250,BUFNI=250,HBUFND=400,HBUFNI=400')
//PMT0MF   DD DISP=SHR,DSN=&VPREFX..&VFSET.MF
//*
//CTLLIB   DD DISP=SHR,DSN=&SPREFX..CDTE&VFSET.
//*
//RECON    DD DISP=SHR,DSN=&SPREFX..&VFSET..ATRAWK
//*
//*
//*
//*
