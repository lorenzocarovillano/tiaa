//PTW1801A PROC DBID=003,
//         DMPS='U',
//         GDG1='+1',
//         JCLO='*',
//         HLQ6='PPDT.TAX',
//         HLQ7='PNPD.TAX',
//         HLQ8='PPDD.TAX',
//         JOBNM='PTW1801A',
//         LIB1='PROD',          PARMLIB
//         LIB2='PROD',          NT2.PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='8M',
//         REPT='8',
//         REPID=',TW1801A9',
//         SPC1='10,10',
//         SPC4='50,10',
//         STATE='AL',
//         UNIT2='CARTN'
//*--------------------------------------------------------------------*
//* PROCEDURE:  THIS IS A SPECIAL PROC FOR ALABAMA                     *
//*             TO REFORMAT THE STANDARD IRS FORMAT STATE REPORTING    *
//*             TO THEIR SPECIAL FORMAT                                *
//*             IT SPLITS THE OUTPUT FROM PROC P180ATWA INTO TWO FILES *
//*             TIAA AND TRUST AND BACKS THEM UP                       *
//*---------------------------------------------------------------------
//* SORT003 - SORTS THE STATE'S CREATED ORIGINAL REPORTING DATA BY
//*           COMPANY CODE, AND SSN
//*---------------------------------------------------------------------
//SORT003 EXEC PGM=SORT,COND=(0,NE)
//SYSOUT    DD SYSOUT=&JCLO
//SYSPRINT  DD SYSOUT=&JCLO
//SORTWK01  DD UNIT=SYSDA,SPACE=(CYL,(&SPC4),RLSE)
//SORTWK02  DD UNIT=SYSDA,SPACE=(CYL,(&SPC4),RLSE)
//SORTWK03  DD UNIT=SYSDA,SPACE=(CYL,(&SPC4),RLSE)
//SORTIN    DD DSN=&HLQ8..FRMT030.&STATE..IRS.RECS,
//             DISP=SHR
//SORTOUT   DD DSN=&HLQ8..SORT003.&STATE..IRS.SRT,
//             DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,
//             RECFM=LS,LRECL=1100,
//             SPACE=(CYL,(&SPC4),RLSE)
//SYSIN     DD DSN=&LIB1..PARMLIB(TW1801A2),DISP=SHR
//* SORT FIELDS=(751,2,CH,A)
//*
//* 751-1 COMPANY CODE
//* 012-9 SSN
//*---------------------------------------------------------------------
//* CREA005 - PROGRAM PRODUCES STATE'S ORIGINAL REPORTING MAGNETIC
//*           MEDIA FILE FOR MAGNETIC MEDIA STATE REPORTING.
//*           NATURAL PROGRAM "TWRP3620".
//*---------------------------------------------------------------------
//CREA005 EXEC PGM=NATB030,TIME=1400,REGION=&REGN1,COND=(0,NE),
//        PARM='SYS=&NAT'
//SYSPRINT  DD SYSOUT=&JCLO
//DDPRINT   DD SYSOUT=&JCLO
//CMPRINT   DD SYSOUT=&JCLO
//CMPRT05   DD SYSOUT=&REPT,
//             RECFM=LS,LRECL=133,BLKSIZE=11571
//SYSUDUMP  DD SYSOUT=&DMPS
//CMWKF01   DD DSN=&HLQ8..SORT003.&STATE..IRS.SRT,
//             DISP=SHR
//CMWKF02   DD DSN=&HLQ8..CREA005.&STATE..DATA,
//             DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,
//             RECFM=LS,LRECL=1100,
//             SPACE=(CYL,(&SPC4),RLSE)
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(P180ATWD),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//* TWRP3620
//*--------------------------------------------------------------------*
//* EXTR010 - PROGRAM REFORMATS IRS FORMAT TO SPECIAL ALABAMA
//*           FORMAT AND SPLITS TIAA AND TRUST RECORDS
//*           NATURAL PROGRAM "TWRP3646".
//*--------------------------------------------------------------------*
//EXTR010 EXEC PGM=NATB030,TIME=1400,REGION=&REGN1,
//        PARM='SYS=&NAT'
//SYSPRINT  DD SYSOUT=&JCLO
//DDPRINT   DD SYSOUT=&JCLO
//CMPRINT   DD SYSOUT=&JCLO
//CMPRT01   DD SYSOUT=(&REPT&REPID),
//             RECFM=LS,LRECL=133,BLKSIZE=11571
//SYSUDUMP  DD SYSOUT=&DMPS
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//** CMWKF01   DD DSN=&HLQ8..CREA060.&STATE..DATA,DISP=SHR
//CMWKF01   DD DSN=&HLQ8..CREA005.&STATE..DATA,DISP=SHR
//CMWKF02   DD DSN=&HLQ8..EXTR010.&STATE..DATA.T,
//             DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,
//             RECFM=LS,LRECL=750,
//             SPACE=(CYL,(&SPC1),RLSE)
//CMWKF03   DD DSN=&HLQ8..EXTR010.&STATE..DATA.X,
//             DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,
//             RECFM=LS,LRECL=750,
//             SPACE=(CYL,(&SPC1),RLSE)
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(TW1801A1),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//*---------------------------------------------------------------------
//* SORT020 - STEP CREATES THE 1ST ORIGINAL REPORTING FILE OF TIAA
//*           TO SEND TO THE STATE TAXATION AUTHORITY.
//*---------------------------------------------------------------------
//SORT020 EXEC PGM=SORT,COND=(0,NE),REGION=&REGN1
//SYSOUT    DD SYSOUT=&JCLO
//SYSPRINT  DD SYSOUT=&JCLO
//SORTWK01  DD UNIT=SYSDA,SPACE=(CYL,(&SPC4),RLSE)
//SORTWK02  DD UNIT=SYSDA,SPACE=(CYL,(&SPC4),RLSE)
//SORTWK03  DD UNIT=SYSDA,SPACE=(CYL,(&SPC4),RLSE)
//SORTIN    DD DSN=&HLQ8..EXTR010.&STATE..DATA.T,
//             DISP=SHR
//SORTOUT   DD DSN=&HLQ7..PTW1801A.&STATE..ORGT(&GDG1),
//             DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             SPACE=(CYL,(&SPC1)),UNIT=SYSDA,
//             DCB=(MODLDSCB,LRECL=750,RECFM=LS)
//SYSIN     DD DSN=&LIB1..PARMLIB(P180ATWE),DISP=SHR
//* SORT FIELDS=COPY
//*
//*---------------------------------------------------------------------
//* SORT030 - STEP CREATES THE 2ND ORIGINAL REPORTING FILE OF TRUST
//*           TO SEND TO THE STATE TAXATION AUTHORITY.
//*---------------------------------------------------------------------
//SORT030 EXEC PGM=SORT,COND=(0,NE),REGION=&REGN1
//SYSOUT    DD SYSOUT=&JCLO
//SYSPRINT  DD SYSOUT=&JCLO
//SORTWK01  DD UNIT=SYSDA,SPACE=(CYL,(&SPC4),RLSE)
//SORTWK02  DD UNIT=SYSDA,SPACE=(CYL,(&SPC4),RLSE)
//SORTWK03  DD UNIT=SYSDA,SPACE=(CYL,(&SPC4),RLSE)
//SORTIN    DD DSN=&HLQ8..EXTR010.&STATE..DATA.X,
//             DISP=SHR
//SORTOUT   DD DSN=&HLQ7..PTW1801A.&STATE..ORGX(&GDG1),
//             DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             SPACE=(CYL,(&SPC1)),UNIT=SYSDA,
//             DCB=(MODLDSCB,LRECL=750,RECFM=LS)
//SYSIN     DD DSN=&LIB1..PARMLIB(P180ATWE),DISP=SHR
//* SORT FIELDS=COPY
//*
//*---------------------------------------------------------------------
//* COPY040 - STEP CREATES A COPY OF THE STATE ORIGINAL REPORTING
//*           FILES FOR VR (VITAL RECORDS) TO BE STORED IN IRON
//*           MOUNTAINS. (COMBINES 2 FILES INTO 1)
//*---------------------------------------------------------------------
//COPY040 EXEC PGM=IEBGENER,COND=(0,NE)
//SYSPRINT  DD SYSOUT=&JCLO
//SYSUT1    DD DSN=&HLQ8..EXTR010.&STATE..DATA.T,DISP=SHR
//          DD DSN=&HLQ8..EXTR010.&STATE..DATA.X,DISP=SHR
//SYSUT2    DD DSN=&HLQ6..&JOBNM..COPY070.&STATE..VR(&GDG1),
//             DISP=(,CATLG,DELETE),
//             UNIT=&UNIT2,
//             DCB=MODLDSCB,RECFM=LS,LRECL=750
//SYSIN     DD DUMMY
//*
