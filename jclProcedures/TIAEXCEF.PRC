//TIAEXCEF PROC RGN1='0M',
//        SPREFX='POMPY',
//*       SPREFX='TPFX',
//        VFSET='PMTP',
//        DATACLS='DCPSEXTC',
//        PRF='IAM',
//        GDG1='(+1)',
//        GDG0='(0)',
//        CURJN='POP9744M',
//        PRJBN='POP9712M',
//        PARMLIB='PROD.PARMLIB',
//        DMPS='U',
//        SPC1='500,500',
//        SYSOUT='*'
//*
//*--------------------------------------------------------------------
//*--- DELETE INTERMEDIATE FILES IF THEY EXIST
//*------------------------------------------------------------------
//* DD1= POMPY.POP9744M.PMTP.SEQ.CCEXTR
//DELT001   EXEC  PGM=IEFBR14
//DD1      DD  DSN=&SPREFX..&CURJN..&VFSET..SEQ.CCEXTR,
//             DISP=(MOD,DELETE,DELETE),
//             SPACE=(TRK,(1,1),RLSE),
//             UNIT=SYSDA,DATACLAS=&DATACLS,
//             DCB=(LRECL=10010,BLKSIZE=0,DSORG=PS,RECFM=LS)
//* DD2= POMPY.POP9744M.PMTP.EFTLIST
//DD2      DD  DSN=&SPREFX..&CURJN..&VFSET..EFTLIST,
//             DISP=(MOD,DELETE,DELETE),
//             SPACE=(TRK,(1,1),RLSE),
//             UNIT=SYSDA,DATACLAS=&DATACLS,
//             DCB=(LRECL=11,BLKSIZE=0,DSORG=PS,RECFM=LS)
//* DD3= POMPY.POP9744M.PMTP.EFTSQ.CCEXTR
//DD3      DD  DSN=&SPREFX..&CURJN..&VFSET..EFTSQ.CCEXTR,
//             DISP=(MOD,DELETE,DELETE),
//             SPACE=(TRK,(1,1),RLSE),
//             UNIT=SYSDA,DATACLAS=&DATACLS,
//             DCB=(LRECL=10010,BLKSIZE=0,DSORG=PS,RECFM=LS)
//* DD4= POMPY.POP9744M.PMTP.PRTSQ.CCEXTR
//DD4      DD  DSN=&SPREFX..&CURJN..&VFSET..PRTSQ.CCEXTR,
//             DISP=(MOD,DELETE,DELETE),
//             SPACE=(TRK,(1,1),RLSE),
//             UNIT=SYSDA,DATACLAS=&DATACLS,
//             DCB=(LRECL=10010,BLKSIZE=0,DSORG=PS,RECFM=LS)
//*
//*---------------------------------------------------------------------
//*   SORT001 EXECUTES A SORT TO ADD A 2 BYTE SPACE FOLLOWED BY AN
//*   8 DIGIT SEQ NUMBER TO THE END OF EACH CCEXTR FILE RECORD
//*---------------------------------------------------------------------
//*
//SORT001  EXEC PGM=SORT,COND=(0,NE),REGION=&RGN1
//SYSOUT   DD SYSOUT=&SYSOUT
//SYSUDUMP DD SYSOUT=&DMPS
//SYSABEND DD SYSOUT=&DMPS
//SYSDBOUT DD  SYSOUT=*
//*
//SORTWK01 DD  DSN=&&SORTWK01,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK02 DD  DSN=&&SORTWK02,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK03 DD  DSN=&&SORTWK03,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK04 DD  DSN=&&SORTWK04,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK05 DD  DSN=&&SORTWK05,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK06 DD  DSN=&&SORTWK06,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK07 DD  DSN=&&SORTWK07,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK08 DD  DSN=&&SORTWK08,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK09 DD  DSN=&&SORTWK09,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK10 DD  DSN=&&SORTWK10,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK11 DD  DSN=&&SORTWK11,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK12 DD  DSN=&&SORTWK12,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK13 DD  DSN=&&SORTWK13,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK14 DD  DSN=&&SORTWK14,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK15 DD  DSN=&&SORTWK15,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK16 DD  DSN=&&SORTWK16,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//*
//SORTIN   DD DISP=SHR,DSN=&SPREFX..&PRJBN..&VFSET..&PRF..CCEXTR&GDG0.
//SORTOUT  DD DSN=&SPREFX..&CURJN..&VFSET..SEQ.CCEXTR,
//            DISP=(NEW,CATLG,DELETE),UNIT=SYSDA,
//            SPACE=(CYL,(&SPC1),RLSE),DATACLAS=&DATACLS,
//            DCB=(LRECL=10010,BLKSIZE=0,DSORG=PS,RECFM=LS)
//SYSIN    DD  DISP=SHR,DSN=&PARMLIB(OP9744M1)
//*
//*---------------------------------------------------------------------
//*   SORT002 EXECUTES A SORT TO EXTRACT ALL EF* FORM ID CHECK NUMBERS
//*   THAT HAVE ADVICE PRINT = N AND CREATE A CHECK NUM LIST
//*---------------------------------------------------------------------
//*
//SORT002  EXEC PGM=SORT,COND=(0,NE),REGION=&RGN1
//SYSOUT   DD SYSOUT=&SYSOUT
//SYSUDUMP DD SYSOUT=&DMPS
//SYSABEND DD SYSOUT=&DMPS
//SYSDBOUT DD  SYSOUT=*
//*
//SORTIN   DD DISP=SHR,DSN=&SPREFX..&CURJN..&VFSET..SEQ.CCEXTR
//SORTWK01 DD  DSN=&&SORTWK17,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK02 DD  DSN=&&SORTWK18,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK03 DD  DSN=&&SORTWK19,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK04 DD  DSN=&&SORTWK20,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK05 DD  DSN=&&SORTWK21,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK06 DD  DSN=&&SORTWK22,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK07 DD  DSN=&&SORTWK23,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK08 DD  DSN=&&SORTWK24,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK09 DD  DSN=&&SORTWK25,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK10 DD  DSN=&&SORTWK26,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK11 DD  DSN=&&SORTWK27,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK12 DD  DSN=&&SORTWK28,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK13 DD  DSN=&&SORTWK29,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK14 DD  DSN=&&SORTWK30,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK15 DD  DSN=&&SORTWK31,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK16 DD  DSN=&&SORTWK32,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTOUT  DD DSN=&SPREFX..&CURJN..&VFSET..EFTLIST,
//            DISP=(NEW,CATLG,DELETE),UNIT=SYSDA,
//            SPACE=(CYL,(&SPC1),RLSE),DATACLAS=&DATACLS,
//            DCB=(LRECL=11,BLKSIZE=0,DSORG=PS,RECFM=LS)
//SYSIN DD DISP=SHR,DSN=&PARMLIB(OP9744M2)
//*
//*---------------------------------------------------------------------
//*   SORT003 EXECUTES A SORT TO DO UNPAIRED JOIN OF THE EFTLIST FILE
//*   AND SEQUENCED CCEXTR FILES. MATCHED RECORDS ARE WRITTEN IN EFT
//*   CCEXTR FILE TO BE EXCLUDED FOR CHECK FEED CREATION. CCEXTR UNMATCH
//*   -ED RECORDS ARE RECORDS TO BE INCLUDED IN CHECK FEED XML PROCESS
//*---------------------------------------------------------------------
//*
//SORT003 EXEC PGM=SORT,COND=(0,NE),REGION=&RGN1
//SYSOUT   DD SYSOUT=&SYSOUT
//SYSUDUMP DD SYSOUT=&DMPS
//SYSABEND DD SYSOUT=&DMPS
//SYSDBOUT DD  SYSOUT=*
//*
//SORTWK01 DD  DSN=&&SORTWK33,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK02 DD  DSN=&&SORTWK34,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK03 DD  DSN=&&SORTWK35,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK04 DD  DSN=&&SORTWK36,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK05 DD  DSN=&&SORTWK37,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK06 DD  DSN=&&SORTWK38,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK07 DD  DSN=&&SORTWK39,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK08 DD  DSN=&&SORTWK40,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK09 DD  DSN=&&SORTWK41,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK10 DD  DSN=&&SORTWK42,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK11 DD  DSN=&&SORTWK43,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK12 DD  DSN=&&SORTWK44,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK13 DD  DSN=&&SORTWK45,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK14 DD  DSN=&&SORTWK46,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK15 DD  DSN=&&SORTWK47,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//SORTWK16 DD  DSN=&&SORTWK48,DISP=(NEW,PASS),
//             UNIT=(SYSDA,20),SPACE=(TRK,(600,1800))
//*
//SORTJNF1 DD DISP=SHR,DSN=&SPREFX..&CURJN..&VFSET..EFTLIST
//SORTJNF2 DD DISP=SHR,DSN=&SPREFX..&CURJN..&VFSET..SEQ.CCEXTR
//EFTCC    DD DSN=&SPREFX..&CURJN..&VFSET..EFTSQ.CCEXTR,
//            DISP=(NEW,CATLG,DELETE),
//            SPACE=(CYL,(&SPC1),RLSE),
//            UNIT=SYSDA,DATACLAS=&DATACLS,
//            DCB=(LRECL=10010,BLKSIZE=0,DSORG=PS,RECFM=LS)
//PRTCC  DD DSN=&SPREFX..&CURJN..&VFSET..PRTSQ.CCEXTR,
//            DISP=(NEW,CATLG,DELETE),
//            SPACE=(CYL,(&SPC1),RLSE),
//            UNIT=SYSDA,DATACLAS=&DATACLS,
//            DCB=(LRECL=10010,BLKSIZE=0,DSORG=PS,RECFM=LS)
//SYSIN DD DISP=SHR,DSN=&PARMLIB(OP9744M3)
//*
//*---------------------------------------------------------------------
//*   SORT004 EXECUTES A SORT TO SORT CCEXTR FILE FOR CHECK FEED ON
//*   SEQUENCE NUMBER AND OUTRECS TO REMOVE SEQUENCE NUMBER THUS
//*   RESTORING ORIGINAL CCEXTR LAYOUT. THIS FILE IS FED TO CCP
//*   CHECK FEED XML PROCESS .
//*---------------------------------------------------------------------
//*
//SORT004 EXEC PGM=SORT,COND=(0,NE),REGION=&RGN1
//SYSOUT   DD SYSOUT=&SYSOUT
//SYSUDUMP DD SYSOUT=&DMPS
//SYSABEND DD SYSOUT=&DMPS
//SYSDBOUT DD  SYSOUT=*
//*
//SORTIN   DD DISP=SHR,DSN=&SPREFX..&CURJN..&VFSET..PRTSQ.CCEXTR
//SORTOUT  DD DSN=&SPREFX..&CURJN..&VFSET..&PRF..CCEXTR&GDG1.,
//            DISP=(NEW,CATLG,DELETE),
//            SPACE=(CYL,(&SPC1),RLSE),
//            UNIT=SYSDA,DATACLAS=&DATACLS,
//            DCB=(LRECL=10000,BLKSIZE=0,DSORG=PS,RECFM=LS)
//*
//SYSIN DD DISP=SHR,DSN=&PARMLIB(OP9744M4)
//*
//*---------------------------------------------------------------------
//*   SORT005 EXECUTES A SORT TO SORT CCEXTR FILE WITH EXCLUDED RECS ON
//*   SEQUENCE NUMBER AND OUTRECS TO REMOVE SEQUENCE NUMBER THUS
//*   RESTORING ORIGINAL CCEXTR LAYOUT. THIS FILE CONTAINS LIST OF ALL
//*   EF* RECRODS WITH PRINT=N FROM CCEXTR (CC,CK,CD)
//*---------------------------------------------------------------------
//*
//SORT005 EXEC PGM=SORT,COND=(0,NE),REGION=&RGN1
//SYSOUT   DD SYSOUT=&SYSOUT
//SYSUDUMP DD SYSOUT=&DMPS
//SYSABEND DD SYSOUT=&DMPS
//SYSDBOUT DD  SYSOUT=*
//*
//SORTIN   DD DISP=SHR,DSN=&SPREFX..&CURJN..&VFSET..EFTSQ.CCEXTR
//SORTOUT  DD DSN=&SPREFX..&CURJN..&VFSET..IAMEF.CCEXTR&GDG1.,
//            DISP=(NEW,CATLG,DELETE),
//            SPACE=(CYL,(&SPC1),RLSE),
//            UNIT=SYSDA,DATACLAS=&DATACLS,
//            DCB=(LRECL=10000,BLKSIZE=0,DSORG=PS,RECFM=LS)
//*
//SYSIN DD DISP=SHR,DSN=&PARMLIB(OP9744M4)
//*
