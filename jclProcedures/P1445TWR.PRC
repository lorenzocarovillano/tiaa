//P1445TWR PROC                                                         
//*------------*                                                        
//* EXTRACTS NAME AND ADDRESSES FROM N/A DATABASE (015/165)             
//* FOR ALL ACTIVE PARTICIPANTS                                         
//* PRINTS NAME AND ADDRESS REPORT.                                     
//*------------*                                                        
//EXTR010  EXEC PGM=NATB030,REGION=8M,                                  
//         PARM='SYS=NAT003P'                                           
//STEPLIB  DD  DSN=PROD.BATCH.LOADLIB,DISP=SHR                          
//         DD  DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR               
//DDCARD   DD  DSN=PROD.PARMLIB(DBAPP003),DISP=SHR                      
//CMWKF01  DD  DSN=PPDD.TAX.P1445TWR.PART.NADDR.EXT,UNIT=SYSDA,         
//             DISP=(NEW,CATLG,DELETE),                                 
//             SPACE=(CYL,(16,6),RLSE),                                 
//             DCB=RECFM=LS,LRECL=262                                   
//SYSPRINT DD  SYSOUT=*                                                 
//DDPRINT  DD  SYSOUT=*                                                 
//SYSOUT   DD  SYSOUT=*                                                 
//SYSUDUMP DD  SYSOUT=U                                                 
//CMPRINT  DD  SYSOUT=*                                                 
//CMPRT01  DD  SYSOUT=8                                                 
//CMPRT02  DD  SYSOUT=8                                                 
//CMSYNIN  DD  DSN=PROD.NT2LOGON.PARMLIB(PANNSEC),DISP=SHR              
//         DD  DSN=PROD.PARMLIB(P1445TW1),DISP=SHR                      
//         DD  DSN=PROD.PARMLIB(FINPARM),DISP=SHR                       
//*------------*                                                        
//* SORT EXTRACT BY COUNTRY CODE AND PIN.                               
//*------------*                                                        
//SORT020 EXEC PGM=SORT,REGION=8M,COND=(0,NE)                           
//SYSOUT   DD  SYSOUT=*                                                 
//SYSUDUMP DD  SYSOUT=U                                                 
//SYSPRINT DD  SYSOUT=*                                                 
//SORTWK01 DD  UNIT=SYSDA,SPACE=(CYL,(10,5),RLSE)                       
//SORTWK02 DD  UNIT=SYSDA,SPACE=(CYL,(10,5),RLSE)                       
//SORTWK03 DD  UNIT=SYSDA,SPACE=(CYL,(10,5),RLSE)                       
//SORTIN   DD  DSN=PPDD.TAX.P1445TWR.PART.NADDR.EXT,DISP=SHR            
//SORTOUT  DD  DSN=PPDD.TAX.P1445TWR.PART.NADDR.EXT.SORTED,             
//             DISP=(NEW,CATLG,DELETE),                                 
//             UNIT=SYSDA,                                              
//             SPACE=(CYL,(16,6),RLSE),                                 
//             DCB=RECFM=LS,LRECL=262                                   
//SYSIN    DD  DSN=PROD.PARMLIB(P1445TW2),DISP=SHR                      
//*------------*                                                        
//* CREATES A REPORT OF PARTICIPANTS NAME AND ADDRESSES.                
//*------------*                                                        
//REPT030  EXEC PGM=NATB030,REGION=8M,COND=(0,NE),                      
//         PARM='SYS=NAT003P'                                           
//STEPLIB  DD  DSN=PROD.BATCH.LOADLIB,DISP=SHR                          
//         DD  DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR               
//DDCARD   DD  DSN=PROD.PARMLIB(DBAPP003),DISP=SHR                      
//CMWKF01  DD  DSN=PPDD.TAX.P1445TWR.PART.NADDR.EXT.SORTED,DISP=SHR     
//SYSPRINT DD  SYSOUT=*                                                 
//DDPRINT  DD  SYSOUT=*                                                 
//SYSOUT   DD  SYSOUT=*                                                 
//SYSUDUMP DD  SYSOUT=U                                                 
//CMPRINT  DD  SYSOUT=*                                                 
//CMPRT01  DD  SYSOUT=8                                                 
//CMPRT02  DD  SYSOUT=8                                                 
//CMSYNIN  DD  DSN=PROD.NT2LOGON.PARMLIB(PANNSEC),DISP=SHR              
//         DD  DSN=PROD.PARMLIB(P1445TW3),DISP=SHR                      
//         DD  DSN=PROD.PARMLIB(FINPARM),DISP=SHR                       
