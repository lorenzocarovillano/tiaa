//TIAXCEL  PROC SPREFX='POMPY',
//            VPREFX='POMPY.ONL',
//            LPREFX='PROD.OMNIPAY.BAT',
//            VFSET='PMTP',
//            GENC='(+0)',              * CURRENT GENERATION            
//            SPC='5,2',                                                
//            REPT='8',
//            SYSROUT='*'
//*-----------------------------------------------------------------*
//*  PROCESS SPREADSHEET PAYMENTS
//*-----------------------------------------------------------------*
//*-----------------------------------------------------------------
//*   UNALLOCATE ANY EXISTING WORK FILES
//*-----------------------------------------------------------------
//UNAL01   EXEC PGM=IEFBR14,
//         COND=(12,LT)
//*
//WKFILE   DD DSN=&SPREFX..&VFSET..XCEL.WK,
//            DISP=(MOD,DELETE),
//            SPACE=(TRK,(1,1),RLSE)
//*
//*-----------------------------------------------------------------*
//*    ALLOCATE WORK FILES                                              
//*-----------------------------------------------------------------*
//ALLOC02  EXEC PGM=IEFBR14
//*
//WKFILE   DD DSN=&SPREFX..&VFSET..XCEL.WK,
//            DCB=(RECFM=LS,LRECL=2496,BLKSIZE=0),
//            DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//            SPACE=(CYL,(&SPC),RLSE),                                  
//            UNIT=SYSDA
//*
//*-----------------------------------------------------------------*
//*  REPRO NEW FILE
//*-----------------------------------------------------------------*
//SORT03   EXEC PGM=SORT
//SYSOUT   DD SYSOUT=&SYSROUT
//SYSUDUMP DD SYSOUT=U
//*
//SORTIN   DD  DISP=SHR,DSN=&SPREFX..&VFSET..MANPMTS&GENC               
//SORTOUT  DD  DISP=SHR,DSN=&SPREFX..&VFSET..XCEL.WK
//*
//SYSIN    DD DISP=SHR,DSN=&LPREFX..CTL(SORTVB)
//*                                                                     
//*-----------------------------------------------------------------*   
//*   CREATE MANUAL PAYMENT DE80'S                                      
//*-----------------------------------------------------------------*   
//XCEL04   EXEC PGM=TIAXCEL                                             
//SYSOUT   DD SYSOUT=&SYSROUT.
//SYSUDUMP DD SYSOUT=U
//SYSPRINT DD SYSOUT=&SYSROUT.
//ABNLTERM DD SYSOUT=&SYSROUT.
//*
//CTLLIB   DD DISP=SHR,DSN=&SPREFX..CDTE&VFSET.
//*
//XCELIN   DD DISP=SHR,DSN=&SPREFX..&VFSET..XCEL.WK
//DE80OT   DD DISP=MOD,DSN=&SPREFX..&VFSET..DE80XCEL(+0)
//PMT0IF   DD DISP=SHR,DSN=&VPREFX..&VFSET.IF
//PMT0PA   DD DISP=SHR,DSN=&VPREFX..&VFSET.PA                           
//*
//RPTOUT   DD SYSOUT=(&REPT.,OP9040D1),
// DCB=(RECFM=LS,LRECL=133,BLKSIZE=1330)
//*
