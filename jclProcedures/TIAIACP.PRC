//TIAIACP  PROC SPREFX='POMPY',
//            VPREFX='POMPY.ONL',                                       
//            LPREFX='PROD.OMNIPAY.BAT',
//            VFSET='PMTP',
//            INGEN='(+0)',                                             
//            REPT='8',
//            SYSROUT='*'
//*-----------------------------------------------------------------*
//*  PROCESS IA COMMUNITY PROPERTY PAYMENTS
//*-----------------------------------------------------------------*
//*-----------------------------------------------------------------
//*   UNALLOCATE ANY EXISTING WORK FILES
//*-----------------------------------------------------------------
//UNAL01   EXEC PGM=IEFBR14,
//         COND=(12,LT)
//*
//WKFILE   DD DSN=&SPREFX..&VFSET..IACP.WK,
//            DISP=(MOD,DELETE),
//            SPACE=(TRK,(1,1),RLSE)
//*
//*-----------------------------------------------------------------*
//*ALLOCATE ALLOCATE WORK FILES
//*-----------------------------------------------------------------*
//ALLOC02  EXEC PGM=IEFBR14
//*
//WKFILE   DD DSN=&SPREFX..&VFSET..IACP.WK,
//            DCB=(RECFM=LS,LRECL=1708,BLKSIZE=0),
//            DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//            SPACE=(CYL,(5,2),RLSE),
//            UNIT=SYSDA
//*
//*-----------------------------------------------------------------*
//*  REPRO NEW FILE
//*-----------------------------------------------------------------*
//SORT03   EXEC PGM=SORT
//SYSOUT   DD SYSOUT=&SYSROUT
//SYSUDUMP DD SYSOUT=U
//*
//SORTIN   DD  DISP=SHR,DSN=&SPREFX..&VFSET..IACP&INGEN                 
//SORTOUT  DD  DISP=SHR,DSN=&SPREFX..&VFSET..IACP.WK
//*
//SYSIN    DD DISP=SHR,DSN=&LPREFX..CTL(SORTCPY)
//*
//*-----------------------------------------------------------------*   
//*  CREATE DE80 CARDS                                                  
//*-----------------------------------------------------------------*   
//IACP04   EXEC PGM=TIAIACP
//SYSOUT   DD SYSOUT=&SYSROUT.
//SYSUDUMP DD SYSOUT=U
//SYSPRINT DD SYSOUT=&SYSROUT.
//ABNLTERM DD SYSOUT=&SYSROUT.
//*
//CTLLIB   DD DISP=SHR,DSN=&SPREFX..CDTE&VFSET.
//*
//IACPIN   DD DISP=SHR,DSN=&SPREFX..&VFSET..IACP.WK
//PMT0PA   DD DISP=SHR,DSN=&VPREFX..&VFSET.PA                           
//DE80OT   DD DISP=MOD,DSN=&SPREFX..&VFSET..DE80IACP(+0)
//*
//RPTOUT   DD SYSOUT=(&REPT.,OP9010M1),
// DCB=(RECFM=LS,LRECL=133,BLKSIZE=1330)
//*
//*
