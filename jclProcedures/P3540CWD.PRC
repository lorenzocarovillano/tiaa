//P3540CWD PROC DBID='046',                                             
//         DMPS='*',                                                    
//         JCLO='*',                                                    
//         HLQ0='PCWF',                                                 
//         LIB1='PROD',                                                 
//         LIB2='PROD',                                                 
//         NAT='NAT046P',                                               
//         NATMEM='PANNSEC',                                            
//         REGN1='5M',                                                  
//         SPC1='150,25',                                               
//         DATACLS=DCSPCR                                               
//**********************************************************************
//EXTR010  EXEC PGM=NATB030,REGION=&REGN1,                              
//      PARM='SYS=&NAT'                                                 
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                   
//SYSOUT   DD SYSOUT=&JCLO                                              
//SYSPRINT DD SYSOUT=&JCLO                                              
//CMWKF01  DD  DSN=&HLQ0..COR.ICWF.MASTER.EXTRACT(+1),                  
//             DISP=(,CATLG,DELETE),                                    
//             UNIT=SYSDA,                                              
//             SPACE=(CYL,(&SPC1),RLSE),                                
//             DATACLAS=&DATACLS,                                       
//             DCB=MODLDSCB,RECFM=LS,LRECL=94                           
//CMPRINT  DD SYSOUT=&JCLO                                              
//DDPRINT  DD SYSOUT=&DMPS                                              
//SYSUDUMP DD SYSOUT=&DMPS                                              
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR             
//         DD DSN=&LIB1..PARMLIB(P3540CW1),DISP=SHR                     
//         DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                      
