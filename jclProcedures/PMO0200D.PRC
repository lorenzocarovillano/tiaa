//PMO0200D PROC                                                         
//**                                                                    
//**----------------------------------------------------------------**  
//** QUERY THE FIDELITY SERVER DIRECTORY FOR THE EDT FILE.           *  
//**                                                                **  
//**----------------------------------------------------------------**  
//**                                                                    
//TRAN0010 EXEC PGM=FTP                                                 
//SYSTCPD  DD  DSN=PNETSF.TCPIP.SEZAINST(TCPDATD1),                     
//             DISP=SHR                                                 
//SYSFTPD  DD  DSN=PNETSF.TCPIP.SEZAINST(FTSDATD1),                     
//             DISP=SHR                                                 
//SYSMDUMP DD  SYSOUT=*                                                 
//SYSPRINT DD  SYSOUT=*                                                 
//OUTPUT   DD  DSN=PPDD.PMO0200D.FTP.LOG,                               
//             DISP=(NEW,CATLG,DELETE),                                 
//             DCB=(LRECL=120,RECFM=LS),                                
//             SPACE=(TRK,(5,5),RLSE),                                  
//             DATACLAS=DCPSEXTC,                                       
//             UNIT=SYSDA                                               
//INPUT    DD  DSN=PROD.PARMLIB(MO0200D1),                              
//             DISP=SHR                                                 
//**                                                                    
//**----------------------------------------------------------------**  
//** VERIFY THAT THE FTP TRANSMIT COMPLETED OK.  RC=8 INDICATES     **  
//** BAD TRANSMIT. RC=99 INDICATES EMPTY LOG FILE.                  **  
//**----------------------------------------------------------------**  
//**                                                                    
//REXX0020 EXEC PGM=IKJEFT01,                                           
//             DYNAMNBR=75,                                             
//             PARM=FTP$VRFY                                            
//SYSPROC  DD  DSN=PROD.VOL.REXX.EXEC,                                  
//             DISP=SHR                                                 
//SYSPRINT DD  SYSOUT=*                                                 
//SYSTERM  DD  SYSOUT=*                                                 
//FTPLOG   DD  DSN=PPDD.PMO0200D.FTP.LOG,                               
//             DISP=SHR                                                 
//SYSTSPRT DD  SYSOUT=*                                                 
//SYSIN    DD  DUMMY                                                    
//SYSTSIN  DD  DUMMY                                                    
//CA11NR   DD  DUMMY                          NOT RESTARTABLE           
//**                                                                    
//**----------------------------------------------------------------**  
//** CHECK DIRECTORY LISTING FOR odc04635.bal.asc FILE.  IF PRESENT **  
//** RC=0.  IF BAD DATE OR NO FILE, RC=8.                           **  
//**----------------------------------------------------------------**  
//**                                                                    
//REXX0030 EXEC PGM=IKJEFT01,                                           
//             COND=(0,NE),                                             
//             DYNAMNBR=30,                                             
//             PARM='FTP$DIR1 odca4635.bal.asc' <- lower case           
//SYSPROC  DD  DSN=PROD.VOL.REXX.EXEC,                                  
//             DISP=SHR                                                 
//FTPLOG   DD  DSN=PPDD.PMO0200D.FTP.LOG,                               
//             DISP=SHR                                                 
//SYSTSPRT DD  SYSOUT=*                                                 
//SYSTSIN  DD  DUMMY                                                    
//CA11NR   DD  DUMMY                          NOT RESTARTABLE           
//         PEND                                                         
