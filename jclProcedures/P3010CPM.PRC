//P3010CPM  PROC
//*--------------------------------------------------------------------
//*
//* FCPP923 : ADD RECORDS TO ADABAS 070 ARCHIVE FILE 196 FCP-ARCH-PYMNT
//**********************************************************************
//*
//**********************************************************************
//*   R E S T A R T  NOTE:
//*   ~~~~~~~~~~~~~~~~~~~
//*
//* THIS JOB IS COMPLETELY RESTARTABLE / RERUNABLE.
//*
//**********************************************************************
//UPDT010 EXEC PGM=NATB030,REGION=9M,
//        PARM='IM=D,SYS=NAT003P'
//STEPLIB   DD DSN=PROD.BATCH.LOADLIB,DISP=SHR
//          DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//SYSOUT    DD SYSOUT=*
//OUTDUMP   OUTPUT DEST=LOCAL
//SYSUDUMP  DD SYSOUT=U
//SYSPRINT  DD SYSOUT=*
//DDPRINT   DD SYSOUT=*
//DDCARD    DD DSN=PROD.DN.PARMLIB(DBAPP070),DISP=SHR
//CMPRINT   DD SYSOUT=*
//CMPRT01   DD SYSOUT=*
//CMPRT02   DD SYSOUT=*
//CMPRT03   DD SYSOUT=*
//CMPRT15   DD SYSOUT=*
//CMWKF01   DD DSN=PDNT.P3000CPM.CPS.FILE196.DATA(0),
//             DISP=OLD,
//             UNIT=CART,
//             TRTCH=COMP,
//             VOL=(,,,99),
//             BUFNO=25
//CMSYNIN   DD DSN=PROD.NT2LOGON.PARMLIB(PANNSEC),DISP=SHR
//          DD DSN=PROD.DN.PARMLIB(P3010CPA),DISP=SHR
