//P1160IAM PROC DBID='003',
//         DMPS='U',
//         GDG0='+0',
//         GDG1='+1',
//         HLQ0='PIA.ANN',
//         HLQ1='PINT.ONL.ANN',
//         HLQ7='PPDT.PNA.IA',
//         UNIT1='CARTV',
//         JCLO='*',
//         JOBNM='P1120IAM',
//         LIB1='PROD',                    PARMLIB
//         LIB2='PROD',                    NT2LOGON.PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='0M',
//         REPT='8',
//         SPC1='200',
//         SPC2='150'
//*
//* ----------------------------------------------------------------- * 
//* PROCEDURE:  P1160IAM                 DATE:    10/10/95            * 
//* ----------------------------------------------------------------- * 
//* PROGRAMS EXECUTED: ITPP0160 (NATURAL) SORT IADP170 (NATURAL)      * 
//* ----------------------------------------------------------------- * 
//* EXTRACT OF IA TPA & IPRO CONTRACTS OFF MONTH END IA MASTER        *
//*                                                                   *
//* MODIFICATION HISTORY
//* 01/19/2001   CHANGED LRECL FROM 118 TO 668 FOR OUTPUT FILE IN     *
//*              STEP EXTR030                                         *
//* 02/22/2001   CHANGED LRECL FROM 668 TO 818 FOR OUTPUT FILE IN     *
//*              STEP EXTR030                                         *
//* 10/29/2001   REMOVE STEP EXTR010 & STEP SORT020                   *
//*              STEP EXTR030 & STEP SORT020 OLD QTRLY PROCESSING     *
//*              NO LONGER VALID READS OLD IAFL                       *
//*              CHANGE CMWKF01 TO READ NEW FILE FOR CHECK-DATE       *
//* 04/03/2002   OIA JCL STANDARDS - PHIL STEINHAUSER
//*                                                                     
//* 10/22/2002   CHANGED LRECL DD CMWKF02 FROM 818 TO 1299
//* 10/2015      COR NAAD DECOMM
//*                                                                     
//*------------------------------------------------------------------ * 
//* EXTRACT OF IA TPA, IPRO & P/I CONTRACTS OFF MONTH END IA MASTER   *
//*            FOR NEW QTRLY PROCESSING                               *
//*------------------------------------------------------------------ *
//*                                                                     
//EXTR030 EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//        PARM='SYS=&NAT'
//SYSPRINT  DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//DDPRINT   DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRINT   DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRT01   DD SYSOUT=(&REPT,IA1160M2)
//SYSUDUMP  DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSABOUT  DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSDBOUT  DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//SYSOUT    DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//*CMWKF01   DD DSN=&LIB3..CNTL.PARMLIB(IAMCKDT),DISP=SHR CHECK DATE    
//*                                                                     
//CMWKF01   DD DSN=&HLQ0..&JOBNM..IADP165.CHECK.DATE,DISP=SHR CHK-DTE   
//CMWKF03   DD DSN=&HLQ0..IMDANNP.T08.MNTHEND(&GDG0),  FROM P1120IAM    
//             DISP=SHR,UNIT=SYSDA                                      
//CMWKF04   DD DSN=&HLQ7..COR.EXTRACT(&GDG0),
//             DISP=SHR,
//             UNIT=&UNIT1
//*                                                                     
//CMWKF02   DD DSN=&HLQ0..IMDANNP.IA.EXTRACT.TOQCOT(&GDG1), TO QTRLY    
//             DISP=(,CATLG,DELETE),                                    
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1,&SPC2),RLSE),               
//             RECFM=LS                                                 
//*            RECFM=LS,LRECL=1299                                      
//*            RECFM=LS,LRECL=818                                       
//IAA770    DD DSN=&HLQ1..TPARATE.FILE.V169,DISP=SHR
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(P1160IA3),DISP=SHR          IADP170
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
