//PCP2248M PROC DMPS='U',                                               
//         HLQ0='PNETSF',                                               
//         JCLO='*',                                                    
//         LIB1='PROD'              PARMLIB                             
//*                                                                     
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
//* PROCEDURE:  PCP2248M                                                
//*          :  CONSOLIDATED PAYMENT SYSTEMS                            
//*          :  DELETE CITIBANK WORLDLINK GLOBAL PAYMENT FILE           
//*          :  FROM THE SERVER AFTER THE FIRST OF THE MONTH            
//*                                                                     
//*   NEW YORK EXTERNAL SERVER MUST BE ACTIVE ftp1e.ops.tiaa-cref.org   
//*                                                                     
//*   THE FILE BEING DELETED IS PLACED ON THE SERVER BY JOB P2237CPM.   
//*                                                                     
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
//* RERUN    : ALL STEPS RERUNNABLE                                     
//*          : SERVER -  ftp1e.ops.tiaa-cref.org   (NEWYORK)            
//*          : DIRECTORY   /citibank/outbound/globalpay/payment1.txt    
//*                                                                     
//* RESTART  : FROM TOP                                                 
//*          : SERVER -  ftp1e.ops.tiaa-cref.org   (NEWYORK)            
//*          : DIRECTORY   /citibank/outbound/globalpay/payment1.txt    
//*          : CITIBANK WORLDLINK GLOBAL PAYMENT FILE                   
//*                                                                     
//*                                                                     
//*******************************************************************   
//*                                                                     
//* MODIFICATION HISTORY                                                
//* 08/30/2012 - PROC INITIALLY ADDED BY MINDY PATTERSON                
//*******************************************************************   
//* FTP0010 - FILE TRANSFER PROTOCOL (FTP)                              
//***************************************                               
//*                                                                     
//FTP0010 EXEC PGM=FTP,PARM='(EXIT=8)'                                  
//SYSTCPD  DD DSN=&HLQ0..TCPIP.SEZAINST(TCPDATD1),DISP=SHR              
//SYSFTPD  DD DSN=&HLQ0..TCPIP.SEZAINST(FTSDATD1),DISP=SHR              
//SYSPRINT DD SYSOUT=&JCLO                                              
//SYSOUT   DD SYSOUT=&JCLO                                              
//SYSUDUMP DD SYSOUT=&DMPS                                              
//OUTPUT   DD SYSOUT=&JCLO                                              
//INPUT    DD DSN=&LIB1..PARMLIB(PCP2248A),DISP=SHR                     
