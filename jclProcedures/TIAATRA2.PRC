//TIAATRA2 PROC SPREFX='VXX',                    <VSAM FILE PREFIX
//            LPREFX='XX',                       <LIBRARY PREFIX
//            PLSFILE='XX',                      <FILE ID
//            TAXFILE='XX',                      <FILE ID
//            VFSET='PMT0',                      <FILESET ID
//            SYSROUT='*'                        <SYSOUT CLASS
//*-----------------------------------------------------------------*
//*  SORT AND CONSOLIDATE ATRA RECON FEED
//*-----------------------------------------------------------------*
//ATRAST1  EXEC PGM=SORT,
//         COND=(12,LT)
//SYSOUT   DD SYSOUT=&SYSROUT
//SYSUDUMP DD SYSOUT=U
//*
//SORTIN   DD DISP=SHR,DSN=&SPREFX..&VFSET..ATRAWK
//*
//SORTOUT  DD DISP=SHR,DSN=&SPREFX..&VFSET..ATRAWK2
//*
//SYSIN    DD DISP=SHR,DSN=&LPREFX..CTL(CKRMSRT)
//*
//ATRACON1 EXEC PGM=TIAATRA2
//SYSOUT   DD SYSOUT=&SYSROUT.
//SYSUDUMP DD SYSOUT=U
//SYSPRINT DD SYSOUT=&SYSROUT.
//ABNLTERM DD SYSOUT=&SYSROUT.
//*
//ATRAIN   DD DISP=SHR,DSN=&SPREFX..&VFSET..ATRAWK2
//ATRAOT   DD DISP=SHR,DSN=&SPREFX..&VFSET..ATRA(+1)
//*
//ATRAST2  EXEC PGM=SORT,
//         COND=(12,LT)
//SYSOUT   DD SYSOUT=&SYSROUT
//SYSUDUMP DD SYSOUT=U
//*
//SORTIN   DD DISP=SHR,DSN=&PLSFILE.
//*
//SORTOUT  DD DISP=SHR,DSN=&SPREFX..&VFSET..ATRAPL2
//*
//SYSIN    DD DISP=SHR,DSN=&LPREFX..CTL(CKRMSRT)
//*
//ATRACON2 EXEC PGM=TIAATRA2
//SYSOUT   DD SYSOUT=&SYSROUT.
//SYSUDUMP DD SYSOUT=U
//SYSPRINT DD SYSOUT=&SYSROUT.
//ABNLTERM DD SYSOUT=&SYSROUT.
//*
//ATRAIN   DD DISP=SHR,DSN=&SPREFX..&VFSET..ATRAPL2
//ATRAOT   DD DISP=SHR,DSN=&SPREFX..&VFSET..ATRAPL(+1)
//*
//ATRAST3  EXEC PGM=SORT,
//         COND=(12,LT)
//SYSOUT   DD SYSOUT=&SYSROUT
//SYSUDUMP DD SYSOUT=U
//*
//SORTIN   DD DISP=SHR,DSN=&TAXFILE.
//*
//SORTOUT  DD DISP=SHR,DSN=&SPREFX..&VFSET..ATRATX2
//*
//SYSIN    DD DISP=SHR,DSN=&LPREFX..CTL(CKRMSRT)
//*
//ATRACON3 EXEC PGM=TIAATRA2
//SYSOUT   DD SYSOUT=&SYSROUT.
//SYSUDUMP DD SYSOUT=U
//SYSPRINT DD SYSOUT=&SYSROUT.
//ABNLTERM DD SYSOUT=&SYSROUT.
//*
//ATRAIN   DD DISP=SHR,DSN=&SPREFX..&VFSET..ATRATX2
//ATRAOT   DD DISP=SHR,DSN=&SPREFX..&VFSET..ATRATX(+1)
//*
//*
//*
