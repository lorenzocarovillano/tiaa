//PNI2060D PROC  DBID=003,
//         DMPS='U',
//*        GDG0='0',
//         GDG1='+1',
//         HLQ1='PPM.ANN',
//         HLQ2='POPS',
//         HLQ3='PPDD',
//         JCLO='*',
//         LIB1='PROD',
//         LIB2='PROD',
//         LIB3='PROD',
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='0M',
//         REPT='8',
//         SPC1='40,10'
//*
//********************************************************************
//*  UPDATE ACIS-REPRINT-FL WITH PLAN/SUBPLAN FROM CONVERSION FILE
//********************************************************************
//** BACKUP INPUT FILE TO GDG
//********************************************************************
//*********************************************************************
//COPY001  EXEC PGM=IDCAMS
//SYSPRINT DD  SYSOUT=&JCLO
//INPUT    DD  DSN=&HLQ2..CONV.PCV0400R.ACIS.EXTRACT,DISP=SHR
//OUTPUT   DD  DSN=&HLQ1..CNTR2060.CONV.ORIG(&GDG1),
//             DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,
//             DCB=MODLDSCB,RECFM=LS,LRECL=80,
//             SPACE=(CYL,(&SPC1),RLSE)
//SYSIN    DD DSN=&LIB1..PARMLIB(REPRO),DISP=SHR
//********************************************************************
//** SORT SUM THE FILE TO GET RID OF DUPLICATE RECORDS
//**   SORT FIELDS=(1,80,CH,A)
//**   SUM FIELDS=(NONE)
//********************************************************************
//SORT001  EXEC PGM=SORT,COND=(0,NE)
//SYSABOUT DD  SYSOUT=&DMPS
//SYSOUT   DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//SORTWK01 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC1),RLSE)
//SORTIN   DD  DSN=&HLQ2..CONV.PCV0400R.ACIS.EXTRACT,DISP=SHR
//SORTOUT  DD  DSN=&HLQ3..CNTR2060.CONV.SORTSM,
//            DISP=(,CATLG,DELETE),SPACE=(CYL,(&SPC1),RLSE),
//            DCB=(RECFM=LS,LRECL=80)
//SYSIN    DD  DSN=&LIB1..PARMLIB(NI2060D1),DISP=SHR
//***************************************************************
//* APPB410
//***************************************************************
//REPT010  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//   PARM='SYS=&NAT,UDB=&DBID,OBJIN=N'
//DDCARD   DD DSN=&LIB3..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSPRINT DD SYSOUT=&JCLO
//DDPRINT  DD SYSOUT=&JCLO
//SYSOUT   DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//CMWKF01  DD DSN=&HLQ3..CNTR2060.CONV.SORTSM,DISP=SHR
//CMWKF02  DD DSN=&HLQ2..CONV.PCV0400R.ACIS.EXTRACT,DISP=SHR
//CMPRINT  DD SYSOUT=(&REPT,NI2060D1)
//CMPRT01  DD SYSOUT=(&REPT,NI2060D2)
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(NI2060D2),DISP=SHR
//***************************************************************
