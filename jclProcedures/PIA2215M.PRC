//*********************************************************************
//*                              I A
//*                REFORMAT DATA IN IA FILE THAT GETS
//*                CREATED SEMI-MONTHLY AND SPLIT DATA
//*                INTO 5 OUTPUT FILES BASED ON RECORD TYPE.
//*********************************************************************
//PIA2215M PROC JOBNM=PIA2215M,
//         REGN1=8M,
//         NATLOGON=PANNSEC,
//         NAT=NATB030,
//         SYS=NAT045P,
//         DBID=045,
//         LIB1=PROD,
//         HLQ0=PIA.ANN,
//         GDG1=0,
//         SPC1=30,
//         SPC2=20,
//         JCLO=*,
//         DMPS=U,
//         PARMMEM=IA2215M1
//*
//*********************************************************************
//*        IAAP399A - REFORMAT DATA FROM SEMI-MONTHLY IATRANS FILE AND
//*                   SPLIT DATA AMONG 5 OUTPUT FILES
//*********************************************************************
//SPLIT010 EXEC PGM=&NAT,COND=EVEN,REGION=&REGN1,
//         PARM='SYS=&SYS,WH=ON'
//SYSPRINT DD  SYSOUT=&JCLO
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//CMPRT01  DD  SYSOUT=&JCLO
//CMWKF01 DD DSN=&HLQ0..&JOBNM..IATRANS.RECTYP10,DISP=(,CATLG,DELETE),  
//        SPACE=(CYL,(&SPC1,&SPC2),RLSE),UNIT=SYSDA,DCB=RECFM=LS        
//CMWKF02 DD DSN=&HLQ0..&JOBNM..IATRANS.RECTYP20,DISP=(,CATLG,DELETE),  
//        SPACE=(CYL,(&SPC1,&SPC2),RLSE),UNIT=SYSDA,DCB=RECFM=LS        
//CMWKF03 DD DSN=&HLQ0..&JOBNM..IATRANS.RECTYP30,DISP=(,CATLG,DELETE),  
//        SPACE=(CYL,(&SPC1,&SPC2),RLSE),UNIT=SYSDA,DCB=RECFM=LS        
//CMWKF04 DD DSN=&HLQ0..&JOBNM..IATRANS.RECTYP40,DISP=(,CATLG,DELETE),  
//        SPACE=(CYL,(&SPC1,&SPC2),RLSE),UNIT=SYSDA,DCB=RECFM=LS        
//CMWKF05 DD DSN=&HLQ0..&JOBNM..IATRANS.HDRTRLR,DISP=(,CATLG,DELETE),   
//        SPACE=(CYL,(1,1),RLSE),UNIT=SYSDA,DCB=RECFM=LS                
//CMWKF06  DD  DSN=PIA.ANN.SIEBEL.IAIQ.IATRANS.FINAL(&GDG1),DISP=SHR    
//SYSUDUMP DD  SYSOUT=&DMPS
//SYSOUT   DD  SYSOUT=&JCLO
//CMSYNIN  DD  DSN=&LIB1..NT2LOGON.PARMLIB(&NATLOGON),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(&PARMMEM),DISP=SHR
//*
//*  END OF PROC
