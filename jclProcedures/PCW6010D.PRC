//PCW6010D PROC DBID='045',
//         DMPS='U',
//         HLQ0='PCWF.COR',
//         JCLO='*',
//         LIB1='PROD',
//         LIB2='PROD',
//         NAT='NAT045P',
//         NATMEM='PANNSEC',
//         REGN1='5120K',
//         SPC1='10,2'
//**********************************************************************
//* PCW6010D - MIT TABLE EWS DATA EXTRACT FOR AVEKSA PROJECT
//*
//*        SCHEDULE TO BE RUN DAILY
//*
//*        1) THIS JOB CONTAINS 1 STEP: (REPT010-CWFBEWS9)
//*
//*           THIS STEP IS A REPORT- GENERATE EXTRACT DATA AND WRITE
//*                                  DATA TO CMWKF01.
//*
//*        2) THIS JOB NEEDS ONE WORK FILES(CMWKF01).
//*
//*        3) OUTPUT WILL BE ARCHIVED TO ARM.
//*
//**********************************************************************
//REPT010  EXEC PGM=NATB030,REGION=&REGN1,
//         PARM='SYS=&NAT'
//SYSPRINT DD  SYSOUT=&JCLO
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSOUT   DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//CMWKF01  DD  DSN=&HLQ0..MITEWS.P6010NDM,DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,
//             DATACLAS=DCPSEXTC,
//             SPACE=(CYL,(&SPC1),RLSE),
//             RECFM=LS,LRECL=300
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(CW6010D1),DISP=SHR
