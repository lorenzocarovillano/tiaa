//P1020NID PROC DBID=003,                                               
//         DMPS='U',
//         GDG1='+1',
//         HLQ1='PPM.ANN',
//         HLQ2='PPM.ACIS',
//         HLQ3='PPDD.ANN',
//         HLQ6='PINT.ONL.ANN',
//         JCLO='*',
//         LIB1='PROD',
//         LIB2='PROD',
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='5M',
//         REPT='8',
//         SPC1='20,5',
//         SPC2='5,5',
//         SPC3='300,50',
//         SPC4='5,2',
//         SPC6='5,5'
//*        UNIT1='CARTV'
//* ----------------------------------------------------------------- * 
//* PROCEDURE:  P1020NID                 DATE:    09/12/92            * 
//* 09/12/92 CHANGE STEP BKUP135 FROM FAVER TO IEBGNER                * 
//* 09/17/92 CHANGE SYSOUT TO * FROM (P,,PUN3)                        * 
//* 05/03/93 CHANGE UPDT100 AND UPDT120 TO NATURAL PROGRAMS           * 
//* 09/10/93 NAT2/V224 UPGRADE (OPRB DBID & FNR'S ADDED FROM PARM)      
//* 09/10/93 SYSOUT UPDATED FOR SAR CONVERSION      J. CASSIDY          
//* 09/13/93 CHANGED ALL REF CICSPROD TO &LIB1..CICS                    
//* 10/12/93 - CHANGED NATURAL 2 STEP TO 4 MEG                          
//* 09/28/94- CONVERTED FOR A.B.R. OUTPUT                 M. AYERS    * 
//* 05/  /95- UPDATED FOR COR IMPLEMETATION R.SHTROMBERG/K.LOGALLO    * 
//* 11/01/95- COMMENTED OUT IDCUT1 & IDCUT2 (DEFN190)    P.MOFFETT    * 
//* 12/14/95- CONDITIONED OUT STEP COPY127.              A.A.         * 
//* 30/29/96    ADDED TRTCH=COMP                            P MOFFETT *
//* 02/12/99-ADDED A WORKFILE IN STEP UPDT120               L SHU     *
//*         -ADDED TWO NEW STEP : SORT001 AND RPT125                  *
//* 06/12/07-COMMENTED OUT S252 BACKUP STEP                K. GATES   *
//* 05/07/09 REMOVED OBSOLETE BKUP010 STEP                 DEVELBISS  *
//* 04/07/10 ADD STEP110 TO GET GUARANTEED INDEXED RATE    RAISA      *
//* ----------------------------------------------------------------- * 
//* PROGRAMS EXECUTED: IEFBR14  FAVER    APPB210  SORT     IEFBR14    * 
//*                    APPB220  SORT     APPB221                      * 
//*                             SORT     IDCAMS   IEBGENER IEBGENER   * 
//*                    IEBGENER IEBGENER IDCAMS   IDCAMS   IDCAMS     * 
//********************************************************************* 
//*UPDT100  EXEC PGM=NATB030,REGION=&REGN3,COND=(0,NE),                 
//*   PARM=('SYS=&NAT,UDB=&DBID,OBJIN=N',                               
//*   'OPRB=(.DBID=253,FNR=202,I,A)')                                   
//*SORTWK01 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC2),RLSE)                     
//*DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                 
//**INSTDAT  DD  DSN=&HLQB..INSTITUT.DATA.MASTER.V437,DISP=SHR          
//*PMA990   DD  DSN=&HLQ0..ACCEPTED.APPL.V107,DISP=SHR                  
//*SYSPRINT DD  SYSOUT=&JCLO                                            
//*DDPRINT  DD  SYSOUT=&DMPS                                            
//*SYSOUT   DD  SYSOUT=&JCLO                                            
//*SYSUDUMP DD  SYSOUT=&DMPS                                            
//*CMPRINT  DD  SYSOUT=&JCLO                                            
//*CMPRT01  DD  SYSOUT=(&REPT,NI1020D1)                                 
//*CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR           
//*         DD  DSN=&LIB1..PARMLIB(P1020NI2),DISP=SHR                   
//*         DD  DSN=&LIB3..CNTL.PARMLIB(P1020DAT),DISP=SHR              
//********************************************************************* 
//* SCIB9008 UPDATE PRAP FILE WITH RATE FOR GUAR INDEXED PRODUCTS       
//********************************************************************* 
//UPDT110  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),                  
//   PARM='SYS=&NAT,UDB=&DBID,OBJIN=N,OPRB=(.ALL)'                      
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//SYSPRINT DD  SYSOUT=&JCLO                                             
//DDPRINT  DD  SYSOUT=&DMPS                                             
//SYSOUT   DD  SYSOUT=&JCLO                                             
//SYSUDUMP DD  SYSOUT=&DMPS                                             
//CMPRINT  DD  SYSOUT=&JCLO                                             
//DFACT    DD  DSN=&HLQ6..TIAACREF.FACTORS.V168,DISP=SHR                
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR            
//         DD  DSN=&LIB1..PARMLIB(P1020N19),DISP=SHR                    
//********************************************************************* 
//UPDT120  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),                  
//   PARM='SYS=&NAT,UDB=&DBID,OBJIN=N'                                  
//*  PARM='OBJIN=N,IM=D,SYS=&NAT,MADIO=0,MT=0,INTENS=1,UDB=&DBID'       
//SORTWK01 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC2),RLSE)                      
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//SYSPRINT DD  SYSOUT=&JCLO                                             
//DDPRINT  DD  SYSOUT=&DMPS                                             
//SYSOUT   DD  SYSOUT=&JCLO                                             
//SYSUDUMP DD  SYSOUT=&DMPS                                             
//CMPRINT  DD  SYSOUT=&JCLO                                             
//CMWKF01  DD  DSN=&HLQ1..NEWISSUE.BENE.S252,DISP=MOD                   
//CMWKF02  DD  DSN=&HLQ1..NEWISSUE.T600.NYDC,DISP=SHR                   
//CMWKF03  DD  DSN=&HLQ1..NI.NEWISSU.T12(&GDG1),                        
//             DISP=(,CATLG,DELETE),
//             UNIT=(SYSDA,5),DATACLAS=DCPSEXTC,
//             SPACE=(CYL,(&SPC3),RLSE),
//             DCB=(MODLDSCB,RECFM=LS,LRECL=4000)
//CMWKF05  DD  DSN=&HLQ3..NEWISSUE.RPT220,                              
//             DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,DATACLAS=DCPSEXTC,
//             SPACE=(CYL,(&SPC4),RLSE),
//             DCB=(RECFM=LS,LRECL=50,BLKSIZE=5000)
//CMPRT01  DD  SYSOUT=(&REPT,NI1020D2)                                  
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR            
//         DD  DSN=&LIB1..PARMLIB(P1020NI4),DISP=SHR                    
//********************************************************************
//SORT122  EXEC PGM=SORT,COND=(0,NE)
//SYSABOUT DD  SYSOUT=&DMPS
//SYSOUT   DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//SORTWK01 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC2),RLSE)
//SORTIN   DD  DSN=&HLQ3..NEWISSUE.RPT220,DISP=OLD
//SORTOUT  DD  DSN=&HLQ3..NEWISSUE.SORTED.RPT220,
//             DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,DATACLAS=DCPSEXTC,
//             SPACE=(CYL,(&SPC4),RLSE),
//             DCB=(RECFM=LS,LRECL=50,BLKSIZE=5000)
//SYSIN    DD  DSN=&LIB1..PARMLIB(P1020N15),DISP=SHR
//********************************************************************* 
//REPT125  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//   PARM='SYS=&NAT,UDB=&DBID,OBJIN=N'
//SORTWK01 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC2),RLSE)
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSPRINT DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&DMPS
//SYSOUT   DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMPRINT  DD  SYSOUT=&JCLO
//CMWKF01  DD  DSN=&HLQ3..NEWISSUE.SORTED.RPT220,DISP=OLD
//CMPRT01  DD  SYSOUT=(&REPT,NI1020D5)
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P1020N16),DISP=SHR
//********************************************************************* 
//UPDT126  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//   PARM='SYS=&NAT,UDB=&DBID,OBJIN=N'
//SORTWK01 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC2),RLSE)
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSPRINT DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&DMPS
//SYSOUT   DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMPRINT  DD  SYSOUT=&JCLO
//CMPRT01  DD  SYSOUT=(&REPT,NI1020D6)
//CMPRT02  DD  SYSOUT=(&REPT,NI1020D7)
//CMPRT03  DD  SYSOUT=(&REPT,NI1020D8)
//CMWKF10  DD DSN=&HLQ2..UPDT126.BENCONT(&GDG1),
//            DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//            UNIT=SYSDA,
//            SPACE=(CYL,(&SPC1),RLSE),
//            DCB=(RECFM=LS,LRECL=219,BLKSIZE=0)
//CMWKF11  DD DSN=&HLQ2..UPDT126.BENMOS(&GDG1),
//            DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//            UNIT=SYSDA,
//            SPACE=(CYL,(&SPC1),RLSE),
//            DCB=(RECFM=LS,LRECL=4462,BLKSIZE=0)
//CMWKF12  DD DSN=&HLQ2..UPDT126.BENDEST(&GDG1),
//            DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//            UNIT=SYSDA,
//            SPACE=(CYL,(&SPC1),RLSE),
//            DCB=(RECFM=LS,LRECL=622,BLKSIZE=0)
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P1020N17),DISP=SHR
//********************************************************************
//*COPY127  EXEC PGM=IDCAMS,COND=(0,LE)                                 
//*SYSPRINT DD  SYSOUT=&JCLO                                            
//*FILEIN   DD  DSN=&HLQ1..NI.NEWISSU.T12(&GDG1),DISP=SHR               
//*DISKOUT  DD  DSN=&HLQ4..POLHOLD.XREFPREM.V101,DISP=SHR               
//*SYSIN    DD  DSN=&LIB1..PARMLIB(P1020NI5),DISP=SHR                   
//********************************************************************* 
//COPY130  EXEC PGM=IEBGENER,COND=(0,NE)                                
//SYSPRINT DD  SYSOUT=&DMPS                                             
//SYSIN    DD  DUMMY                                                    
//SYSUT1   DD  DSN=&HLQ1..NI.NEWISSU.T12(&GDG1),DISP=SHR                
//SYSUT2   DD  DSN=&HLQ1..NI.NEWISSU.T14(&GDG1),                        
//             DISP=(,CATLG,DELETE),
//             UNIT=(SYSDA,4),DATACLAS=DCPSEXTC,
//             SPACE=(CYL,(&SPC3),RLSE),
//             DCB=(MODLDSCB,RECFM=LS,LRECL=4000)
//********************************************************************* 
//*BKUP135  EXEC PGM=IEBGENER,COND=(0,NE)                               
//*SYSPRINT DD  SYSOUT=&DMPS                                            
//*SYSIN    DD  DUMMY                                                   
//*SYSUT1   DD  DSN=&HLQ1..NEWISSUE.BENE.S252,DISP=SHR                  
//*SYSUT2   DD  DSN=&HLQ7..P1020NID.S252.BU(&GDG1),                     
//*             DISP=(,CATLG,DELETE),                                   
//*             UNIT=&UNIT1,                                            
//*             DCB=(MODLDSCB)                                          
//********************************************************************* 
//COPY150  EXEC PGM=IEBGENER,COND=(4,LT)                                
//SYSPRINT DD  SYSOUT=&DMPS                                             
//SYSIN    DD  DUMMY                                                    
//SYSUT1   DD  DSN=&HLQ1..NEWISSUE.T600.NYDC,DISP=SHR                   
//SYSUT2   DD  DSN=&HLQ1..NI.NEWISSU.T05(&GDG1),                        
//             DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,DATACLAS=DCPSEXTC,
//             SPACE=(CYL,(&SPC6),RLSE),
//             DCB=(MODLDSCB,RECFM=LS,LRECL=4000)
//********************************************************************* 
//*DEFN170  EXEC PGM=IDCAMS,COND=(0,NE)                                 
//*SYSABOUT DD  SYSOUT=&DMPS                                            
//*SYSUDUMP DD  SYSOUT=&DMPS                                            
//*SYSOUT   DD  SYSOUT=&JCLO                                            
//*SYSPRINT DD  SYSOUT=&REPT                                            
//*SYSIN    DD  DSN=&LIB1..PARMLIB(P1020N11),DISP=SHR                   
//********************************************************************* 
//*DEFN180  EXEC PGM=IDCAMS,COND=(0,NE)                                 
//*SYSABOUT DD  SYSOUT=&DMPS                                            
//*SYSUDUMP DD  SYSOUT=&DMPS                                            
//*SYSOUT   DD  SYSOUT=&JCLO                                            
//*SYSPRINT DD  SYSOUT=&REPT                                            
//*SYSIN    DD  DSN=&LIB1..PARMLIB(P1020N12),DISP=SHR                   
//********************************************************************* 
//*DEFN190  EXEC PGM=IDCAMS,COND=(0,NE),REGION=&REGN1                   
//*SYSABOUT DD  SYSOUT=&DMPS                                            
//*SYSUDUMP DD  SYSOUT=&DMPS                                            
//*SYSOUT   DD  SYSOUT=&JCLO                                            
//*SYSPRINT DD  SYSOUT=&JCLO                                            
//*BASEDD   DD  DSN=&HLQ5..SAS.ACTIVITY.V103,DISP=OLD                   
//*AIXDD    DD  DSN=&HLQ0..SAS.ACT.V108.AIX,DISP=OLD                    
//*SYSIN    DD  DSN=&LIB1..PARMLIB(P1020N13),DISP=SHR                   
//*********************************************************************
//REPT200  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//   PARM='SYS=&NAT,UDB=&DBID,OBJIN=N'
//SORTWK01 DD  UNIT=SYSDA,SPACE=(CYL,(&SPC2),RLSE)
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSPRINT DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&DMPS
//SYSOUT   DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMPRINT  DD  SYSOUT=&JCLO
//CMWKF01  DD  DSN=&HLQ1..NEWISSUE.T600.NYDC,DISP=SHR
//CMPRT01  DD  SYSOUT=(&REPT,NI1020D9)
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P1020N18),DISP=SHR
//********************************************************************
