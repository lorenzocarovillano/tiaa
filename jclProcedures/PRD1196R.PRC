//*********************************************************************
//*                            P O S T
//*     PROCESSING OF MOBIUS INDEX FILE FOR E-MAIL DATA EXTRACTION
//*           DATASETS CREATION OF ORACLE CONTROL RECORD AND
//*                    DIRECT:CONNECT CONTROL CARDS
//*********************************************************************
//PRD1196R PROC JOBNM='XXXXXXXX',
//         PJOBNM='XXXXXXXX',
//         REGN1=8M,
//         NATLOGON=PANNSEC,
//         NAT=NATB030,
//         SYS=NAT045P,
//         DBID=045,
//         LIB1=PROD,
//         HLQ1=PPT.COR,
//         HLQ9=PMVSSF,
//         SPC1=20,
//         SPC2=100,
//         SPC3=1,
//         SPC4=1,
//         JCLO=*,
//         DMPS=U,
//         MEDIATYP='XXXXX',
//         PARMMEM1=P1240PT2,
//         PARMMEM2='XXXXXXXX'
//*
//*********************************************************************
//*        DEALLOCATE WORK DATASET
//*********************************************************************
//DELT002  EXEC PGM=IEFBR14
//SYSPRINT DD  SYSOUT=&JCLO
//TMPFL01  DD  DSN=&HLQ1..&JOBNM..EMAIL.DATA,
//             UNIT=SYSDA,
//             DISP=(MOD,DELETE,DELETE),
//             SPACE=(CYL,(0,0))
//TMPFL02  DD  DSN=&HLQ1..&JOBNM..EMAIL.CONTROL.RECORD,
//             UNIT=SYSDA,
//             DISP=(MOD,DELETE,DELETE),
//             SPACE=(CYL,(0,0))
//TMPFL03  DD  DSN=&HLQ1..&JOBNM..CREA010,
//             UNIT=SYSDA,
//             DISP=(MOD,DELETE,DELETE),
//             SPACE=(CYL,(0,0))
//*********************************************************************
//*     ALLOCATE DATASETS - E-MAIL EXTRACT, ORACLE CONTROL RECORD AND
//*                         DIRECT:CONNECT CONTROL CARDS
//*********************************************************************
//ALLC004  EXEC PGM=IEFBR14,COND=(0,NE)
//SYSPRINT DD  SYSOUT=&JCLO
//D1       DD  DSN=&HLQ1..&JOBNM..EMAIL.DATA,
//             UNIT=SYSDA,DATACLAS=DCPSEXTC,
//             DISP=(NEW,CATLG,DELETE),
//             SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//             DCB=(DSORG=PS,RECFM=VB,LRECL=300)
//SYSIN    DD  DUMMY
//*
//ALLC006  EXEC PGM=IEFBR14,COND=(0,NE)
//SYSPRINT DD  SYSOUT=&JCLO
//D1       DD  DSN=&HLQ1..&JOBNM..EMAIL.CONTROL.RECORD,
//             UNIT=SYSDA,DATACLAS=DCPSEXTC,
//             DISP=(NEW,CATLG,DELETE),
//             SPACE=(CYL,(&SPC3,&SPC4),RLSE),
//             DCB=(DSORG=PS,RECFM=LS,LRECL=223)
//SYSIN    DD  DUMMY
//*
//ALLC008  EXEC PGM=IEFBR14,COND=(0,NE)
//SYSPRINT DD  SYSOUT=&JCLO
//D1       DD  DSN=&HLQ1..&JOBNM..CREA010,
//             UNIT=SYSDA,DATACLAS=DCPSEXTC,
//             DISP=(NEW,CATLG,DELETE),
//             SPACE=(CYL,(1,1),RLSE),
//             DCB=(DSORG=PS,RECFM=F,LRECL=80)
//SYSIN    DD  DUMMY
//*
//*********************************************************************
//*    RIDP556  - EXTRACT E-MAIL DATA / CREATE ORACLE CONTROL RECORD
//*               CREATE DIRECT:CONNECT CONTROL CARDS
//*********************************************************************
//CREA010  EXEC PGM=&NAT,COND=(0,NE),REGION=&REGN1,
//         PARM='SYS=&SYS,WH=ON'
//SYSPRINT DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//CMWKF01  DD  DSN=&HLQ9..VDR.WEB.TOPFMT.&PJOBNM..&MEDIATYP,
//             DISP=SHR
//CMWKF02  DD  DSN=&HLQ1..&JOBNM..EMAIL.DATA,
//             DISP=SHR
//CMWKF03  DD  DSN=&HLQ1..&JOBNM..EMAIL.CONTROL.RECORD,
//             DISP=SHR
//CMWKF04  DD  DSN=&HLQ1..&JOBNM..CREA010,
//             DISP=SHR
//CMSYNIN  DD  DSN=&LIB1..NT2LOGON.PARMLIB(&NATLOGON),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(RD1196R2),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(&PARMMEM1),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(&PARMMEM2),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//*
