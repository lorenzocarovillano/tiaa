//PCP1060D PROC DBID=003,                                               
//         DMPS='U',                                                    
//         HLQ0='PCPS.ANN',         DASD HLQ
//         HLQ8='PPDD',             DASD HLQ TEMP
//         JOBNM='PCP1060D',        2ND HLQ FOR FILES CREATED IN PROC
//         UNIT1=SYSDA,             UNIT PARM FOR DASD OUTPUT
//         JCLO='*',                                                    
//         GDG1='+1',                                                   
//         LIB1='PROD',                                                 
//         LIB2='PROD',                                                 
//         NAT='NAT003P',                                               
//         NATMEM='PANNSEC',                                            
//         NATPGM1='CP1060D1',                                          
//         NATPGM2='CP1060D2',                                          
//         NATPGM3='CP1060D5',                                          
//         PARMMEM1='CP1060D3',                                         
//         PARMMEM2='CP1060D4',                                         
//         REGN1='9M',                                                  
//         SPC1=1285,
//         SPC2=128,
//         SPC3=100
//* -------------------------------------------------------------------
//* --  DELETE NEW DATASETS F.ENDAYA 2013/11/21              ----------
//DELT005  EXEC PGM=IEFBR14
//DD1      DD DSN=&HLQ0..&JOBNM..FCPCNFUL,DISP=(MOD,DELETE),
//            SPACE=(TRK,(1,1),RLSE)
//DD2      DD DSN=&HLQ0..&JOBNM..CPSFUND,DISP=(MOD,DELETE),
//            SPACE=(TRK,(1,1),RLSE)
//* -------------------------------------------------------------------
//* PROGRAMS EXECUTED: CPUP1060 EXTRACTS BY ORGN STAT AND INRVSE DATE
//* -------------------------------------------------------------------
//EXTR010  EXEC PGM=NATB030,REGION=&REGN1,PARM='SYS=&NAT'
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMWKF01  DD DSN=&HLQ0..&JOBNM..PAYMENT.EXT(&GDG1),
//            DISP=(,CATLG,DELETE),
//            DCB=(MODLDSCB,RECFM=LS,LRECL=100),UNIT=&UNIT1,
//            SPACE=(CYL,(&SPC2,&SPC3),RLSE),
//            DATACLAS=DCPSEXTC
//SYSPRINT DD SYSOUT=&JCLO
//DDPRINT  DD SYSOUT=&JCLO
//SYSOUT   DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//CMPRINT  DD SYSOUT=&JCLO
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(&NATPGM1),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(&PARMMEM2),DISP=SHR
//*-------------------------------------------------------------------*
//* SORT020 SORT BY CONTRACT NUM AND REMOVES DUP
//*-------------------------------------------------------------------*
//SORT020  EXEC PGM=SORT,COND=(0,NE)
//SORTWK01 DD  UNIT=&UNIT1,
//             SPACE=(CYL,(&SPC2,&SPC3),RLSE)
//SORTWK02 DD  UNIT=&UNIT1,
//             SPACE=(CYL,(&SPC2,&SPC3),RLSE)
//SORTWK03 DD  UNIT=&UNIT1,
//             SPACE=(CYL,(&SPC2,&SPC3),RLSE)
//SORTWK04 DD  UNIT=&UNIT1,
//             SPACE=(CYL,(&SPC2,&SPC3),RLSE)
//SORTWK05 DD  UNIT=&UNIT1,
//             SPACE=(CYL,(&SPC2,&SPC3),RLSE)
//SORTIN   DD DSN=&HLQ0..&JOBNM..PAYMENT.EXT(&GDG1),DISP=SHR
//SORTOUT  DD DSN=&HLQ8..&JOBNM..PAYMENT.SORTED,
//            DISP=(,CATLG,DELETE),UNIT=&UNIT1,
//            SPACE=(CYL,(&SPC2,&SPC3),RLSE),
//            DATACLAS=DCPSEXTC,
//            DCB=(*.SORTIN)
//SYSOUT   DD  SYSOUT=&JCLO
//SYSIN    DD  DSN=&LIB1..PARMLIB(&PARMMEM1),DISP=SHR
//* -------------------------------------------------------------------
//* PROGRAMS EXECUTED: CPUP1051 EXTRACTS BY ORGN STAT AND INRVSE DATE
//* -------------------------------------------------------------------
//EXTR030  EXEC PGM=NATB030,REGION=&REGN1,PARM='SYS=&NAT'
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMWKF01  DD DSN=&HLQ8..&JOBNM..PAYMENT.SORTED,DISP=SHR
//CMWKF02  DD DSN=&HLQ0..&JOBNM..PAYMENT.EXTRACT(&GDG1),
//            DISP=(,CATLG,DELETE),
//            DCB=(MODLDSCB,RECFM=LS,LRECL=298),UNIT=&UNIT1,
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//            DATACLAS=DCPSEXTC
//CMWKF03  DD DSN=&HLQ0..&JOBNM..PAYMENT.CNTL(&GDG1),
//            DISP=(,CATLG,DELETE),
//            DCB=(MODLDSCB,RECFM=LS,LRECL=141),UNIT=&UNIT1,
//            SPACE=(CYL,(&SPC2,&SPC3),RLSE),
//            DATACLAS=DCPSEXTC
//CMWKF04  DD DSN=&HLQ0..&JOBNM..DETAIL.CNTL,DISP=SHR
//CMWKF05  DD DSN=&HLQ0..&JOBNM..SUMMARY.CNTL,DISP=SHR
//CMWKF06  DD DSN=&HLQ0..&JOBNM..ODS.PAYMENT(&GDG1),
//            DISP=(,CATLG,DELETE),
//            DCB=(MODLDSCB,RECFM=LS,LRECL=1700),UNIT=&UNIT1,
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//            DATACLAS=DCPSEXTC
//CMWKF07  DD DSN=&HLQ0..&JOBNM..ODS.FUNDS(&GDG1),
//            DISP=(,CATLG,DELETE),
//            DCB=(MODLDSCB,RECFM=LS,LRECL=200),UNIT=&UNIT1,
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//            DATACLAS=DCPSEXTC
//CMWKF08  DD DSN=&HLQ0..&JOBNM..ODS.ADVDTL(&GDG1),
//            DISP=(,CATLG,DELETE),
//            DCB=(MODLDSCB,RECFM=LS,LRECL=200),UNIT=&UNIT1,
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//            DATACLAS=DCPSEXTC
//CMWKF15  DD DSN=&HLQ0..&JOBNM..ODS.ANNOT(&GDG1),
//            DISP=(,CATLG,DELETE),
//            DCB=(MODLDSCB,RECFM=LS,LRECL=900),UNIT=&UNIT1,
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//            DATACLAS=DCPSEXTC
//CMWKF16  DD DSN=&HLQ0..&JOBNM..ODS.ANNOT2(&GDG1),
//            DISP=(,CATLG,DELETE),
//            DCB=(MODLDSCB,RECFM=LS,LRECL=200),UNIT=&UNIT1,
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//            DATACLAS=DCPSEXTC
//SYSPRINT DD SYSOUT=&JCLO
//DDPRINT  DD SYSOUT=&JCLO
//SYSOUT   DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//CMPRINT  DD SYSOUT=&JCLO
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(&NATPGM2),DISP=SHR
//*        DD  DSN=&LIB1..PARMLIB(&PARMMEM2),DISP=SHR
//* -------------------------------------------------------------------
//* PROGRAMS EXECUTED: CPUP1052 EXTRACTS CSR FOR ODS
//* -------------------------------------------------------------------
//EXTR040  EXEC PGM=NATB030,REGION=&REGN1,PARM='SYS=&NAT'
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMWKF04  DD DSN=&HLQ0..&JOBNM..UDETAIL.CNTL,DISP=SHR
//CMWKF05  DD DSN=&HLQ0..&JOBNM..USUMMARY.CNTL,DISP=SHR
//CMWKF06  DD DSN=&HLQ0..&JOBNM..UODS.PAYMENT(&GDG1),
//            DISP=(,CATLG,DELETE),
//            DCB=(MODLDSCB,RECFM=LS,LRECL=1700),UNIT=&UNIT1,
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//            DATACLAS=DCPSEXTC
//CMWKF07  DD DSN=&HLQ0..&JOBNM..UODS.FUNDS(&GDG1),
//            DISP=(,CATLG,DELETE),
//            DCB=(MODLDSCB,RECFM=LS,LRECL=200),UNIT=&UNIT1,
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//            DATACLAS=DCPSEXTC
//CMWKF10  DD  DSN=&HLQ0..&JOBNM..FCPCNFUL,
//             DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//             DATACLAS=DCPSEXTC,
//             DCB=(RECFM=LS,LRECL=58,DSORG=PS)
//CMWKF11  DD  DSN=&HLQ0..&JOBNM..CPSFUND,
//             DISP=(,CATLG,DELETE),
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//             DATACLAS=DCPSEXTC,
//             DCB=(RECFM=LS,LRECL=154,DSORG=PS)
//CMWKF15  DD DSN=&HLQ0..&JOBNM..UODS.ANNOT(&GDG1),
//            DISP=(,CATLG,DELETE),
//            DCB=(MODLDSCB,RECFM=LS,LRECL=900),UNIT=&UNIT1,
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//            DATACLAS=DCPSEXTC
//CMWKF16  DD DSN=&HLQ0..&JOBNM..UODS.ANNOT2(&GDG1),
//            DISP=(,CATLG,DELETE),
//            DCB=(MODLDSCB,RECFM=LS,LRECL=200),UNIT=&UNIT1,
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//            DATACLAS=DCPSEXTC
//SYSPRINT DD SYSOUT=&JCLO
//DDPRINT  DD SYSOUT=&JCLO
//SYSOUT   DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//CMPRINT  DD SYSOUT=&JCLO
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(&NATPGM3),DISP=SHR
