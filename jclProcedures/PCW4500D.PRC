//PCW4500D PROC DBID=045,
//       NATVERS='NATB030',
//       REGN1='8M',
//       NAT='NAT003P',
//       DMPS='U',
//       JCLO='*',
//       LIB1='PROD',
//       LIB2='PROD',
//       NATMEM='PANNSEC',
//       HLQ1='PPDD',
//       HLQ2='PCWF.COR',
//       SPC1='5,5'
//*-------------------------------------------------------------------*
//* Extract data from CWF-MASTER-INDEX-VIEW (DB 45,File 181) and
//* produce Daily 'Pending/Aging ATA & Life Insurance Disbursement
//* Transactions Reports'
//*------------------------------------------------------------------ *
//* CWFB8640 - Pending/Aging ATA & Life Insurance Disbursement
//*            Transactions
//*------------------------------------------------------------------ *
//EXTR010  EXEC PGM=&NATVERS,REGION=&REGN1,
//            PARM='SYS=&NAT'
//SYSUDUMP DD SYSOUT=&DMPS
//SYSPRINT DD SYSOUT=&JCLO
//DDPRINT  DD SYSOUT=&JCLO
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMPRINT  DD SYSOUT=&JCLO
//CMWKF01  DD DSN=&HLQ1..PCW4500D.EXTR010.ATAPENDG.EXTRACT,
//            UNIT=SYSDA,DISP=(,CATLG,DELETE),
//            DATACLAS=DCPSEXTC,
//            SPACE=(CYL,(&SPC1),RLSE),
//            DCB=(RECFM=LS,LRECL=300)
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(CW4500DA),DISP=SHR
//*------------------------------------------------------------------ *
//* SORT by TIAA-RCVD-DTE
//* SORT FIELDS=(146,8,CH,A)
//*------------------------------------------------------------------ *
//SORT020  EXEC PGM=SORT,REGION=0K,COND=(0,NE)
//SYSOUT   DD SYSOUT=&JCLO
//SYSUDUMP DD SYSOUT=&DMPS
//SYSPRINT DD SYSOUT=&JCLO
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,(&SPC1)),DCB=BUFNO=10
//SORTWK02 DD UNIT=SYSDA,SPACE=(CYL,(&SPC1)),DCB=BUFNO=10
//SORTWK03 DD UNIT=SYSDA,SPACE=(CYL,(&SPC1)),DCB=BUFNO=10
//SORTWK04 DD UNIT=SYSDA,SPACE=(CYL,(&SPC1)),DCB=BUFNO=10
//SORTIN   DD DSN=&HLQ1..PCW4500D.EXTR010.ATAPENDG.EXTRACT,DISP=SHR
//SORTOUT  DD DSN=&HLQ1..PCW4500D.SORT020.ATAPENDG.SRTD,
//            UNIT=SYSDA,DISP=(,CATLG,DELETE),
//            DATACLAS=DCPSEXTC,
//            SPACE=(CYL,(&SPC1),RLSE),
//            DCB=(RECFM=LS,LRECL=300)
//SYSIN    DD DSN=&LIB1..PARMLIB(CW4500DB),DISP=SHR
//*------------------------------------------------------------------ *
//* CWFB8641 - Pending/Aging ATA & Life Insurance Disbursement
//*            Transactions
//*------------------------------------------------------------------ *
//REPT030  EXEC PGM=&NATVERS,REGION=&REGN1,
//            PARM='SYS=&NAT'
//SYSUDUMP DD SYSOUT=&DMPS
//SYSPRINT DD SYSOUT=&JCLO
//DDPRINT  DD SYSOUT=&JCLO
//DDCARD   DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//CMPRINT  DD SYSOUT=&JCLO
//CMWKF01  DD DSN=&HLQ1..PCW4500D.SORT020.ATAPENDG.SRTD,DISP=SHR
//CMWKF02  DD DSN=&HLQ2..PCW4500D.REPT030.ATAPENDG.REPORT,
//            UNIT=SYSDA,DISP=(,CATLG,DELETE),
//            DATACLAS=DCPSEXTC,
//            SPACE=(CYL,(&SPC1),RLSE),
//            DCB=(RECFM=LS,LRECL=200)
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(CW4500DC),DISP=SHR
//*------------------------------------------------------------------ *
//* IDCAMS   - Backup the report into a GDG
//*------------------------------------------------------------------ *
//COPY040 EXEC PGM=IDCAMS
//SYSPRINT  DD SYSOUT=*
//INDD      DD DSN=&HLQ2..PCW4500D.REPT030.ATAPENDG.REPORT,DISP=SHR
//OUDD      DD DSN=&HLQ2..PCW4500D.ATAPENDG.RPT(+1),
//             DISP=(NEW,CATLG,DELETE),UNIT=SYSDA,
//             SPACE=(CYL,(&SPC1),RLSE),
//             DATACLAS=DCPSEXTC,
//             RECFM=LS,LRECL=200
//SYSIN     DD DSN=&LIB1..PARMLIB(CW4500DD),DISP=SHR
//*------------------------------------------------------------------ *
