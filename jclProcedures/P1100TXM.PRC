//P1100TXM PROC DBID=003
//* P1100TXM PROC RUNTYPE=A
//*********************************************************************
//*                                                                   *
//* PROCEDURE:  P1100TXM                 DATE: 10/18/2001             *
//*                                                                   *
//* ------------------------------------------------------------------*
//* OCTOBER 18, 2001                                                  *
//* IA CUTOFF DATE - UPDATE PREVIOUS IA CUTOFF DATE AND POPULATE THE  *
//*                  NEW IA CUTOOF DATE.                              *
//*                  DBID =003 FILE = 112 (REFERENCE-TABLE)           *
//* JULY 01, 1992                                                     *
//* TAX ELECTION  - UPDATE THE CONTROL RECORD ON TAX-ELECTION FILE    *
//*                 TO REFLECTED THE NEXT IA CHECK CYCLE DATE         *
//* ------------------------------------------------------------------*
//* PROGRAMS EXECUTED: TXWP2900 (REPLACED ELCP5010)                   *
//*********************************************************************
//* 10/10/01 - SYSOUT UPDATED FOR SAR CONVERSION          J. CASSIDY  *
//* 09/15/93 - SYSOUT UPDATED FOR SAR CONVERSION          J. CASSIDY  *
//*********************************************************************
//* TXWP2900 (REPLACED ELCP5010) DBID=003  FILE=112 (REFERENCE-TABLE)
//*           PARMLIB P1100TX1 = ELCP5010
//*           PARMLIB P1100TX2 = TXWP2900 (NEW) REPLACING P1100TX1.
//*********************************************************************
//UPDT010 EXEC PGM=NATB030,REGION=5M,
//        PARM='SYS=NAT003P'
//*       PARM='IM=D,SYS=NAT003P'
//STEPLIB   DD DSN=PDBFPP.ADABAS.CURRENT.LOADLIB,DISP=SHR
//          DD DSN=PROD.BATCH.LOADLIB,DISP=SHR
//SYSPRINT  DD SYSOUT=*
//DDPRINT   DD SYSOUT=*
//CMPRINT   DD SYSOUT=*
//CMPRT01   DD SYSOUT=8,
//             DCB=(RECFM=LS,LRECL=133)
//SYSUDUMP  DD SYSOUT=*
//DDCARD    DD DSN=PROD.PARMLIB(DBAPP&DBID),DISP=SHR
//CMSYNIN   DD DSN=PROD.NT2LOGON.PARMLIB(PANNSEC),DISP=SHR
//          DD DSN=PROD.PARMLIB(P1100TX2),DISP=SHR
//          DD DSN=PROD.PARMLIB(FINPARM),DISP=SHR
