//PIA2196D PROC DBID='003',
//         DMPS='U',
//         JCLO='*',
//         LIB1='PROD',             PARMLIB
//         LIB2='PROD',             NT2LOGON.PARMLIB
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REGN1='8M',
//         REPT='8',
//         HLQ0='PPDD',
//         SPC1='50',
//         SPC2='50'
//*
//********************************************************************
//* PROCEDURE:  PIA2196D             DATE:  05/15/2007
//*
//* 1) READS IAA-XFR-AUDIT FILE AND EXTRACTS ALL TRANSFERS TO CREF
//*    FOR BLUESKY. THE OUTPUT FILE PPDD.ANN.IA.BLUESKY WILL BE INPUT
//*    TO PIA2196X TO NDM TO BLUESKY SERVER.
//*                                                                     
//**********************************************************************
//*                                                                     
//* MODIFICATION HISTORY
//* 05/15/07 - ORIGINAL CODE - JUN TINIO
//* 06/2017  - REMOVE LRECL                                             
//********************************************************************
//*
//*******************************************************
//EXTR010  EXEC PGM=NATB030,COND=(0,NE),REGION=&REGN1,
//         PARM='SYS=&NAT'
//SYSPRINT DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//DDCARD   DD DSN=&LIB2..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSOUT   DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//DDPRINT  DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRINT  DD SYSOUT=&JCLO,OUTPUT=*.OUTVDR
//CMPRT01  DD SYSOUT=(&REPT,IA2196D1)
//CMWKF01  DD  DSN=&HLQ0..ANN.IA.BLUESKY,
//             DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//             UNIT=SYSDA,SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//*            RECFM=LS,LRECL=80
//             RECFM=LS
//SYSUDUMP DD SYSOUT=&DMPS,OUTPUT=*.OUTLOCL
//CMSYNIN  DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(IA2196D1),DISP=SHR
