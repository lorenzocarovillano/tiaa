//PCP0605M PROC SYSOUT='*',        COMMON LIBRARY HLQ
//         HLQ8='PCPS.ANN.',    TEMPORARY FILES PASSED
//         HLQ1='PCPS.ANN.',         FILES
//         UNIT1=SYSDA,             UNIT PARM FOR DASD OUTPUT
//         RPTID='CP0605M1',         MOBIUS REPORT ID
//         REPT0=8,                REPORTS
//         SPC1=10,                 FILE1 - PRIMARY SPACE ALLOCATION
//         SPC2=02,                 FILE1 - SECONDARY SPACE ALLOCATION
//         GDG1='+1'                NEW GENERATION DATASET CREATED
//***************************************************************
//* COPY THE MONTHLY OPEN CHECKS FILE WACHOVIA TO MOBIUS
//***************************************************************
//COPY005  EXEC PGM=IEBGENER
//SYSPRINT DD SYSOUT=&SYSOUT
//SYSUT1   DD DSN=&HLQ8.GLOBAL.ACKCITI,DISP=SHR
//SYSUT2   DD SYSOUT=(&REPT0,&RPTID),OUTPUT=*.OUTVDR
//SYSIN    DD DUMMY
//***************************************************************
//* COPY THE MONTHLY OPEN CHECKS FILE WACHOVIA
//***************************************************************
//COPY010  EXEC PGM=IEBGENER
//SYSPRINT DD SYSOUT=&SYSOUT
//SYSUT1   DD  DSN=&HLQ8.GLOBAL.ACKCITI,DISP=SHR
//SYSUT2   DD  DSN=&HLQ1.GLOBAL.CITI.ACK(&GDG1),
//         DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,
//         UNIT=&UNIT1,SPACE=(CYL,(&SPC1,&SPC2))
//SYSIN    DD DUMMY
//*************************************************************
//* DELETE THE PPDD DSN                                      **
//*************************************************************
//DELT020  EXEC PGM=IEFBR14,COND=(0,NE)
//SYSPRINT DD  SYSOUT=&SYSOUT
//DD01     DD  DSN=&HLQ8.GLOBAL.ACKCITI,
//         DISP=(MOD,DELETE,DELETE),UNIT=&UNIT1,SPACE=(&SPC1,&SPC2)
