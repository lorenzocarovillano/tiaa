//PTW1710M PROC DBID='003',                                             
//         DMPS='U',                                                    
//         GDG1='+1',                                                   
//         HLQ0='PNPD.COR.TAX',     CORPORATE DATASET                   
//         HLQ8='PPDD.TAX',                                             
//         JCLO='*',                                                    
//         JOBNM='PTW1710M',                                            
//         JOBNM1='PTW1705M',                                           
//         LIB1='PROD',             PARMLIB                             
//         LIB2='PROD',             NT2LOGON.PARMLIB                    
//         NAT='NAT003P',                                               
//         NATMEM='PANNSEC',                                            
//         REGN1='8M',                                                  
//         REPID1='TW1710M1',                                           
//         REPID2='TW1710M2',                                           
//         REPID3='TW1710M3',                                           
//         REPID4='TW1710M4',                                           
//         REPID5='TW1710M5',                                           
//         REPID6='TW1710M6',                                           
//         REPID7='TW1710M7',                                           
//         REPID8='TW1710M8',                                           
//         REPID9='TW1710M9',                                           
//         REPIDA='TW1710MA',                                           
//         REPIDB='TW1710MB',                                           
//         REPIDC='TW1710MC',                                           
//         REPT='8',                                                    
//         SPC1='50,50',                                                
//         SPC2='15,5',                                                 
//         SPC3='50,10',                                                
//         SPC4='100,50'                                                
//*                                                                     
//**********************************************************************
//*                                                                     
//* MODIFICATION HISTORY                                                
//* --------------------                                                
//* 99/99/99 : X. XXXXXXXXXXXXX - XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX.
//**********************************************************************
//*   STEP    PROGRAM                     DESCRIPTION                   
//* --------  --------  ------------------------------------------------
//* SORT003   SORT      SPLIT INPUT FILE INTO DETAIL AND CONTROL FILES. 
//* PRNT005   TWRP0690  PRINTS TRANSACTION MISSING FIELDS REPORTS FROM  
//*                     SEQUENTIAL INPUT.                               
//* EXTR010   TWRP0702  EDIT FEEDER SYSTEMS EXTRACT.                    
//* SORT020   SORT      SORT COMBINED ACCEPTED AND CITED (DURING EDITS) 
//*                     RECORDS EXTRACT BY SOURCE CODE, COMPANY CODE,   
//*                     PAYMENT TYPE, SETTLEMENT TYPE, AND REFERENCE-   
//*                     NUMBER.                                         
//* EDIT030   TWRP0705  MORE EDITS OF FEEDER SYSTEMS EXTRACT.           
//* EXTR040   TWRP0665  EDITS, REFORMATS AND CREATES TRANSACTION FILE   
//*                     TO LOAD THE TAX DATABASES.                      
//* LOAD050   TWRP0800  THE PROGRAM ASSIGNS UNIQUE NUMBER TO EACH       
//*                     TRANSACTION AT THE END.  THE UNIQUE NUMBER      
//*                     COUNTER IS STORED ON THE CONTROL FILE (003/098).
//*                     FEEDER TABLE NO. 5 ORIGIN "XX".                 
//**********************************************************************
//*                                                                     
//*--------------------------------------------------------------------*
//* SORT003 - SPLIT INPUT FILE INTO DETAIL AND CONTROL FILES.           
//*--------------------------------------------------------------------*
//SORT003  EXEC PGM=SORT,REGION=&REGN1,COND=(0,NE)                      
//SYSOUT    DD SYSOUT=&JCLO                                             
//SYSPRINT  DD SYSOUT=&JCLO                                             
//SYSUDUMP  DD SYSOUT=&DMPS                                             
//SORTWK01  DD UNIT=SYSDA,SPACE=(CYL,(&SPC3),RLSE)                      
//SORTWK02  DD UNIT=SYSDA,SPACE=(CYL,(&SPC3),RLSE)                      
//SORTWK03  DD UNIT=SYSDA,SPACE=(CYL,(&SPC3),RLSE)                      
//SORTWK04  DD UNIT=SYSDA,SPACE=(CYL,(&SPC3),RLSE)                      
//SORTWK05  DD UNIT=SYSDA,SPACE=(CYL,(&SPC3),RLSE)                      
//SORTWK06  DD UNIT=SYSDA,SPACE=(CYL,(&SPC3),RLSE)                      
//SORTWK07  DD UNIT=SYSDA,SPACE=(CYL,(&SPC3),RLSE)                      
//SORTIN    DD DSN=&HLQ0..&JOBNM1..VUL.REFORMAT,                        
//             DISP=SHR                                                 
//          DD DSN=&HLQ0..&JOBNM1..VUL.REFORMAT.SUMM,                   
//             DISP=SHR                                                 
//SORTOF1   DD DSN=&HLQ0..&JOBNM..SORT003.DETAIL,                       
//             DISP=SHR                                                 
//SORTOF2   DD DSN=&HLQ0..&JOBNM..SORT003.CONTROL,DISP=SHR              
//SYSIN     DD DSN=&LIB1..PARMLIB(TW1710M1),DISP=SHR                    
//*  SORT FIELDS=(2,2,CH,A,1,1,CH,A,14,11,CH,A,4,10,CH,A)               
//*  OUTFIL FILES=1,                                                    
//*  INCLUDE=(1,1,CH,NE,C'Z')                                           
//*  OUTFIL FILES=2,                                                    
//*  INCLUDE=(1,1,CH,EQ,C'Z')                                           
//*                                                                     
//*  COMPANY-CDE        1,1                                             
//*  SOURCE             2,2                                             
//*  TAX-ID-TYPE       14,1                                             
//*  TAX-ID            15,10                                            
//*  CNTRCT-NBR         4,8                                             
//*  PAYEE-CDE         12,2                                             
//*                                                                     
//*--------------------------------------------------------------------*
//* PRNT005 - PRINTS TRANSACTION MISSING FIELDS REPORTS FROM            
//*           SEQUENTIAL INPUT.                                         
//*           NATURAL PROGRAM 'TWRP0690'.                               
//*--------------------------------------------------------------------*
//PRNT005  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),                  
//         PARM='SYS=&NAT'                                              
//SYSOUT    DD SYSOUT=&JCLO                                             
//SYSUDUMP  DD SYSOUT=&DMPS                                             
//SYSPRINT  DD SYSOUT=&JCLO                                             
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//DDPRINT   DD SYSOUT=&JCLO                                             
//CMPRINT   DD SYSOUT=&JCLO                                             
//CMPRT01   DD SYSOUT=(&REPT,&REPID1)                                   
//CMPRT02   DD SYSOUT=(&REPT,&REPID2)                                   
//CMPRT03   DD SYSOUT=(&REPT,&REPID3)                                   
//CMPRT04   DD SYSOUT=(&REPT,&REPID4)                                   
//CMPRT05   DD SYSOUT=(&REPT,&REPID5)                                   
//CMWKF01   DD DSN=&HLQ0..&JOBNM..SORT003.DETAIL,                       
//             DISP=SHR                                                 
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR            
//          DD DSN=&LIB1..PARMLIB(TW1710M2),DISP=SHR                    
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                     
//* TWRP0690                                                            
//*--------------------------------------------------------------------*
//* EXTR010 - EDIT FEEDER SYSTEMS EXTRACT.                              
//*           NATURAL PROGRAM 'TWRP0702' - COPY OF 'TWRP0700'.          
//*--------------------------------------------------------------------*
//EXTR010  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),                  
//          PARM='SYS=&NAT'                                             
//SYSOUT    DD SYSOUT=&JCLO                                             
//SYSUDUMP  DD SYSOUT=&DMPS                                             
//SYSPRINT  DD SYSOUT=&JCLO                                             
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//DDPRINT   DD SYSOUT=&JCLO                                             
//CMPRINT   DD SYSOUT=&JCLO                                             
//*CMPRT01   DD SYSOUT=&REPT                                            
//CMPRT02   DD SYSOUT=(&REPT,&REPID6)                                   
//CMPRT03   DD SYSOUT=(&REPT,&REPID7)                                   
//CMPRT04   DD SYSOUT=(&REPT,&REPID8)                                   
//CMPRT05   DD SYSOUT=(&REPT,&REPID9)                                   
//CMPRT06   DD SYSOUT=(&REPT,&REPIDA)                                   
//CMPRT07   DD SYSOUT=(&REPT,&REPIDB)                                   
//CMWKF01   DD DSN=&HLQ0..&JOBNM..SORT003.DETAIL,                       
//             DISP=SHR                                                 
//CMWKF02   DD DSN=&HLQ8..&JOBNM..EXTR010.ACCEPT.RECS,UNIT=SYSDA,       
//             DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,                  
//             SPACE=(CYL,(&SPC1),RLSE),                                
//             DCB=(RECFM=LS,LRECL=770)                                 
//CMWKF03   DD DSN=&HLQ8..&JOBNM..EXTR010.REJECT.RECS,UNIT=SYSDA,       
//             DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,                  
//             SPACE=(CYL,(&SPC2),RLSE),                                
//             DCB=RECFM=LS,LRECL=650                                   
//CMWKF04   DD DSN=&HLQ8..&JOBNM..EXTR010.CITED.RECS,UNIT=SYSDA,        
//             DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,                  
//             SPACE=(CYL,(&SPC2),RLSE),                                
//             DCB=(RECFM=LS,LRECL=770)                                 
//*                                                                     
//CMWKF05   DD DSN=&HLQ0..&JOBNM..SORT003.CONTROL,DISP=SHR              
//CMWKF06   DD DSN=&HLQ8..&JOBNM..EXTR010.ACCEPT.SUM.RECS,              
//             DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,                  
//             UNIT=SYSDA,                                              
//             SPACE=(CYL,(&SPC2),RLSE),                                
//             DCB=RECFM=LS,LRECL=650                                   
//CMWKF07   DD DSN=&HLQ8..&JOBNM..EXTR010.CITED.SUM.RECS,               
//             DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,                  
//             UNIT=SYSDA,                                              
//             SPACE=(CYL,(&SPC2),RLSE),                                
//             DCB=RECFM=LS,LRECL=650                                   
//CMWKF08   DD DSN=&HLQ0..&JOBNM1..CONTROL.EXT,DISP=SHR                 
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR            
//          DD DSN=&LIB1..PARMLIB(TW1710M3),DISP=SHR                    
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                     
//* TWRP0702                                                            
//*--------------------------------------------------------------------*
//* SORT020 - SORT COMBINED ACCEPTED AND CITED (DURING EDITS) RECORDS   
//*           EXTRACT BY SOURCE CODE, COMPANY CODE, PAYMENT TYPE,       
//*           SETTLEMENT TYPE, AND REFERENCE-NUMBER.                    
//*--------------------------------------------------------------------*
//SORT020  EXEC PGM=SORT,REGION=&REGN1,COND=(0,NE)                      
//SYSOUT    DD SYSOUT=&JCLO                                             
//SYSUDUMP  DD SYSOUT=&DMPS                                             
//SORTWK01  DD UNIT=SYSDA,SPACE=(CYL,(&SPC3),RLSE)                      
//SORTWK02  DD UNIT=SYSDA,SPACE=(CYL,(&SPC3),RLSE)                      
//SORTWK03  DD UNIT=SYSDA,SPACE=(CYL,(&SPC3),RLSE)                      
//SORTWK04  DD UNIT=SYSDA,SPACE=(CYL,(&SPC3),RLSE)                      
//SYSPRINT  DD SYSOUT=&JCLO                                             
//SORTIN    DD DSN=&HLQ8..&JOBNM..EXTR010.ACCEPT.RECS,                  
//             DISP=(OLD,KEEP,DELETE)                                   
//          DD DSN=&HLQ8..&JOBNM..EXTR010.CITED.RECS,                   
//             DISP=(OLD,KEEP,DELETE)                                   
//SORTOUT   DD DSN=&HLQ8..&JOBNM..SORT020.ACCRECS.SRTED,                
//             DISP=(,CATLG,DELETE),UNIT=SYSDA,                         
//             SPACE=(CYL,(&SPC1),RLSE),DATACLAS=DCPSEXTC,              
//             DCB=RECFM=LS,LRECL=770                                   
//SYSIN     DD DSN=&LIB1..PARMLIB(TW1710M4),DISP=SHR                    
//* SORT FIELDS=(1,1,CH,A,2,2,CH,A,40,2,CH,A)                           
//*--------------------------------------------------------------------*
//* EDIT030 - MORE EDITS OF FEEDER SYSTEMS EXTRACT.                     
//*           NATURAL PROGRAM "TWRP0705".                               
//*--------------------------------------------------------------------*
//EDIT030  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),                  
//         PARM='SYS=&NAT'                                              
//SYSOUT    DD SYSOUT=&JCLO                                             
//SYSUDUMP  DD SYSOUT=&DMPS                                             
//SYSPRINT  DD SYSOUT=&JCLO                                             
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//DDPRINT   DD SYSOUT=&JCLO                                             
//CMPRINT   DD SYSOUT=&JCLO                                             
//CMPRT01   DD SYSOUT=(&REPT,&REPIDC)                                   
//CMWKF01   DD DSN=&HLQ8..&JOBNM..SORT020.ACCRECS.SRTED,DISP=SHR        
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR            
//          DD DSN=&LIB1..PARMLIB(TW1710M5),DISP=SHR                    
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                     
//* TWRP0705                                                            
//*--------------------------------------------------------------------*
//* EXTR040 - EDITS, REFORMATS AND CREATES TRANSACTION FILE TO LOAD     
//*           THE TAX DATABASES.                                        
//*           NATURAL PROGRAM "TWRP0665".                               
//*--------------------------------------------------------------------*
//EXTR040  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),                  
//         PARM='SYS=&NAT'                                              
//SYSOUT    DD SYSOUT=&JCLO                                             
//SYSUDUMP  DD SYSOUT=&DMPS                                             
//SYSPRINT  DD SYSOUT=&JCLO                                             
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//DDPRINT   DD SYSOUT=&JCLO                                             
//CMPRINT   DD SYSOUT=&JCLO                                             
//CMWKF01   DD DSN=&HLQ8..&JOBNM..SORT020.ACCRECS.SRTED,                
//             DISP=SHR                                                 
//CMWKF02   DD DSN=&HLQ0..&JOBNM1..CONTROL.EXT,DISP=SHR                 
//CMWKF03   DD DSN=&HLQ0..&JOBNM..EXTR040,DISP=SHR                      
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR            
//         DD  DSN=&LIB1..PARMLIB(TW1710M6),DISP=SHR                    
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                     
//* TWRP0665                                                            
//*--------------------------------------------------------------------*
//* LOAD050 - THE PROGRAM ASSIGNS UNIQUE NUMBER TO EACH TRANSACTION     
//*           AT THE END.  THE UNIQUE NUMBER COUNTER IS STORED ON THE   
//*           CONTROL FILE (003/098).  FEEDER TABLE NO. 5 ORIGIN "XX".  
//*           NATURAL PROGRAM "TWRP0800".                               
//*--------------------------------------------------------------------*
//LOAD050  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),                  
//         PARM='SYS=&NAT'                                              
//SYSPRINT  DD SYSOUT=&JCLO                                             
//SYSUDUMP  DD SYSOUT=&DMPS                                             
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//DDPRINT   DD SYSOUT=&JCLO                                             
//CMPRINT   DD SYSOUT=&JCLO                                             
//CMWKF01   DD DSN=&HLQ8..&JOBNM..SORT020.ACCRECS.SRTED,                
//             DISP=(OLD,KEEP,DELETE)                                   
//CMWKF02   DD DSN=&HLQ0..&JOBNM..LOAD050.ACREC(&GDG1),                 
//             DISP=(,CATLG,DELETE),DATACLAS=DCPSEXTC,                  
//             UNIT=SYSDA,SPACE=(CYL,(&SPC4),RLSE),                     
//             DCB=(MODLDSCB,LRECL=800,RECFM=LS)                        
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR            
//          DD DSN=&LIB1..PARMLIB(TW1710M7),DISP=SHR                    
//          DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                     
//* TWRP0800                                                            
