//PCP1470D PROC LIB1='PROD',        COMMON LIBRARY HLQ                  
//         VPREFX='POMPY.ONL',      WORK FILE PREFIX
//         VFSET='PMTP',            FILESET ID (VSAM)
//         HLQ0='PIA.ANN',          IA FILE                             
//         JCLO00=*,                JOB OUTPUT                          
//         SYSROUT='*',             SYSOUT                              
//         REPT00=*,                REPORTS                             
//         REPT01=8,                REPORTS                             
//         RPTID='CP1470D',         REPORT                              
//         DBID=003,                DATABASE ID                         
//         REGN1='9M',              REGION SIZE                         
//         GDG0='+0',               INPUT FROM OMNIPAY                  
//         GDG1='+1',               INPUT TO NATURAL PROGRAM            
//         UNIT1=SYSDA,             UNIT PARM FOR DASD OUTPUT           
//         SPC1='50',               PRMRY  SPACE ALLOCATION FOR OUTPUT
//         SPC2='75',               SCNDRY SPACE ALLOCATION FOR OUTPUT
//         NATVERS=NATB030,         NATURAL VERSION                     
//         NAT=NAT003P,             NATURAL PROFILE                     
//         PARMLIB=PARMLIB,         LLQ COMMON PARM LIBRARY             
//         NATLOG=NT2LOGON.PARMLIB, 2 LVL QUAL NATURAL LOGON LIBRARY    
//         NATMEM=PANNSEC           NATURAL LOGON MEMBER                
//*
//*--------------------------------------------------------------------*
//*                                                                    *
//* PROCEDURE:  PCP1470D                           DATE:    09/30/2015 *
//*                                                                    *
//*           CONSOLIDATED PAYMENT SYSTEM / OPEN INVESTMENTS           *
//*                                                                    *
//*                      PROLINE REPLACEMENT PAYMENTS                  *
//*                                                                    *
//*  STEP    PROGRAM   PARMS                   COMMENTS                *
//* -------  -------  --------  -------------------------------------- *
//* REPT010  FCPP95O            PRODUCE REPORT FOR NFP & FFP           *
//*                                                                    *
//*--------------------------------------------------------------------*
//EXTNFP   EXEC PGM=TIANFP
//SYSOUT   DD SYSOUT=&SYSROUT
//SYSUDUMP DD SYSOUT=U
//SYSPRINT DD SYSOUT=&SYSROUT
//ABNLTERM DD SYSOUT=&SYSROUT
//*
//PMT0CK   DD DISP=SHR,DSN=&VPREFX..&VFSET.CK,
//            AMP=('ACCBIAS=SO')
//PMT0CK8  DD DISP=SHR,DSN=&VPREFX..&VFSET.CKA8,
//            AMP=('ACCBIAS=DO')
//PMT0AN   DD DISP=SHR,DSN=&VPREFX..&VFSET.AN
//INPNFP   DD DSN=&HLQ0..P2090IAD.NFP.FFP.FILE(&GDG0),DISP=SHR
//OUTNFP   DD DSN=&HLQ0..PCP1470D.NFP.FFP.OUT(&GDG1),
//            DISP=(,CATLG,DELETE),
//            DCB=(RECFM=LS,LRECL=191),UNIT=&UNIT1,
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE),
//            DATACLAS=DCPSEXTC
//*
//REPT010  EXEC PGM=&NATVERS,REGION=&REGN1,COND=(0,NE),                 
//         PARM='IM=D,SYS=&NAT'                                         
//DDCARD   DD DSN=&LIB1..&PARMLIB(DBAPP&DBID),DISP=SHR                  
//DDPRINT  DD SYSOUT=&JCLO00,OUTPUT=*.OUTVDR                            
//CMPRINT  DD SYSOUT=&REPT00,OUTPUT=*.OUTVDR                            
//CMPRT01  DD SYSOUT=(&REPT01,&RPTID.1),                                
//         DCB=(RECFM=VB,LRECL=255),OUTPUT=*.OUTVDR                     
//CMWKF01  DD DSN=&HLQ0..PCP1470D.NFP.FFP.OUT(&GDG1),DISP=SHR           
//CMSYNIN  DD DSN=&LIB1..&NATLOG(&NATMEM),DISP=SHR                      
//         DD DSN=&LIB1..&PARMLIB(PCP1470A),DISP=SHR                    
//*
