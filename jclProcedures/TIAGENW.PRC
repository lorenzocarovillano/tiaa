//TIAGENW  PROC SPREFX='POMPY',                                         
//            CPSPREFX='PCPS.ANN',                                      
//            VPREFX='POMPY.ONL',                                       
//            VFSET='PMTP',
//            GENC='(+0)',              * CURRENT GENERATION            
//            REPT='8',
//            SYSROUT='*'
//*-----------------------------------------------------------------*
//*  PROCESS GENERIC WARRANTS PAYMENTS
//*-----------------------------------------------------------------*
//GENW01   EXEC PGM=TIAGENW                                             
//SYSOUT   DD SYSOUT=&SYSROUT.
//SYSUDUMP DD SYSOUT=U
//SYSPRINT DD SYSOUT=&SYSROUT.
//ABNLTERM DD SYSOUT=&SYSROUT.
//*
//CTLLIB   DD DISP=SHR,DSN=&SPREFX..CDTE&VFSET.
//*
//GENWIN   DD DISP=SHR,DSN=&CPSPREFX..PCP1860D.GW.OMNIPRO&GENC          
//DE80OT   DD DISP=MOD,DSN=&SPREFX..&VFSET..DE80GENW(+0)
//PMT0PG   DD DISP=SHR,DSN=&VPREFX..&VFSET.PG
//*
//RPTOUT   DD SYSOUT=(&REPT.,OP9000D1),
// DCB=(RECFM=LS,LRECL=133,BLKSIZE=1330)
//*
