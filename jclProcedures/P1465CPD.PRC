//P1465CPD PROC DBID=003,
//        REPT='8',                                                     
//        JCLO='*',                                                     
//        DMPS='U',                                                     
//        GDG1='+1',                                                    
//        NAT=NAT003P,
//        NATMEM=PANNSEC,
//        LIB1=PROD,
//        LIB2=PROD,
//        LIB3=PROD,
//        JOBNM=P1465CPD,
//        DATACLAS=DCPSEXTC,
//        HLQ0=PAA.ONL.ANN,
//        HLQ1=PIA.ANN,
//        HLQ8=PPDD,
//        SPC1='30,10',
//        SPC2='25,10',
//        SPC3='35,10',
//        REGN1=9M
//*
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
//*
//* PROCEDURE:  P1465CPD
//*
//*--------------------------------------------------------------------
//* EXTR010 - EXTRACT "NZ"  CPS LEDGER DATA FOR END-OF-DAY
//*           PROCESSING BASED ON SPECIFIED INTERFACE DATE.
//*           NATURAL PROGRAM "FCPP120".
//* SORT020 - SORT "NZ" AND LEDGER EXTRACT FOR DETAIL AND
//*           SUMMARY REPORTS
//* REPT030 - PRODUCE "NZ" DETAIL AND SUMMARY LEDGER REPORTS
//*           NATURAL PROGRAM "FCPP121".
//* REPT040 - CREATE A REPORT ON DAILY PAYMENT STATISTICS
//*           NATURAL PROGRAM "FCPP662".
//* EXTR050 - EXTRACT CPS RECON LEDGER DATA,
//*           PROCESSING BASED ON SPECIFIED INTERFACE DATE.
//*           NATURAL PROGRAM "FCPP120Y".
//*--------------------------------------------------------------------
//* MODIFICATION HISTORY
//* 04/30/02 - OIA JCL STANDARDS
//* 11/08/10 - RS0 ADDED STEP EXTR050                                   
//**********************************************************************
//*
//* EXTR010 - EXTRACT "NZ"  CPS LEDGER DATA FOR END-OF-DAY
//*           PROCESSING BASED ON SPECIFIED INTERFACE DATE.
//*           NATURAL PROGRAM "FCPP120".
//*--------------------------------------------------------------------
//EXTR010  EXEC PGM=NATB030,REGION=&REGN1,
//             PARM='SYS=&NAT'
//SYSPRINT  DD SYSOUT=&JCLO
//SYSUDUMP  DD SYSOUT=&DMPS
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT   DD SYSOUT=&JCLO
//CMPRINT   DD SYSOUT=&JCLO
//CMPRT01   DD SYSOUT=&REPT
//CMWKF01   DD DSN=&HLQ8..&JOBNM..LEDGER.EXT,
//             LRECL=154,RECFM=LS,BUFNO=25,
//             DISP=(NEW,CATLG,DELETE),DATACLAS=&DATACLAS,
//             SPACE=(CYL,(&SPC3),RLSE),UNIT=SYSDA
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(P1465CPA),DISP=SHR
//          DD DSN=&LIB3..CNTL.PARMLIB(P1465CP1),DISP=SHR
//* FCPP120
//* NZ LEDGER
//*--------------------------------------------------------------------
//* SORT020 - SORT "NZ" & LEDGER EXTRACT FOR DETAIL AND
//*           SUMMARY REPORTS.
//*--------------------------------------------------------------------
//SORT020  EXEC PGM=SORT,REGION=&REGN1,COND=(0,NE)
//SYSOUT    DD SYSOUT=&JCLO
//SORTWK01  DD UNIT=SYSDA,SPACE=(CYL,(&SPC1))
//SORTWK02  DD UNIT=SYSDA,SPACE=(CYL,(&SPC1))
//SORTWK03  DD UNIT=SYSDA,SPACE=(CYL,(&SPC1))
//SORTWK04  DD UNIT=SYSDA,SPACE=(CYL,(&SPC1))
//SYSPRINT  DD SYSOUT=&JCLO
//SORTIN    DD DSN=&HLQ8..&JOBNM..LEDGER.EXT,DISP=SHR,BUFNO=24
//*
//SORTOUT   DD DSN=&HLQ8..&JOBNM..LEDGER.EXT.SORT,
//             LRECL=154,RECFM=LS,BUFNO=25,
//             DISP=(NEW,CATLG,DELETE),DATACLAS=&DATACLAS,
//             SPACE=(CYL,(&SPC2),RLSE),UNIT=SYSDA
//SYSIN     DD DSN=&LIB1..PARMLIB(P1465CPB),DISP=SHR
//*
//*--------------------------------------------------------------------
//* REPT030 - PRODUCE "NZ" DETAIL AND SUMMARY LEDGER REPORTS
//*           NATURAL PROGRAM "FCPP121".
//*--------------------------------------------------------------------
//REPT030  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//            PARM=('SYS=&NAT',
//            'OPRB=(.DBID=253,FNR=68,I,A)')
//SYSPRINT  DD SYSOUT=&JCLO
//SYSUDUMP  DD SYSOUT=&DMPS
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT   DD SYSOUT=&JCLO
//ISA       DD DSN=&HLQ0..ABC.ABCTBL.V026,DISP=SHR
//CMPRINT   DD SYSOUT=&JCLO
//CMPRT01   DD SYSOUT=&REPT
//CMPRT02   DD SYSOUT=&REPT
//CMWKF01   DD DSN=&HLQ8..&JOBNM..LEDGER.EXT.SORT,DISP=SHR,BUFNO=25
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(P1465CPC),DISP=SHR
//* FCPP121
//*--------------------------------------------------------------------
//* REPT040 - CREATE A REPORT ON DAILY PAYMENT STATICTICS
//*           NATURAL PROGRAM  "FCPP662".
//*--------------------------------------------------------------------
//REPT040 EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//    PARM='SYS=&NAT'
//SYSABOUT DD  SYSOUT=&DMPS
//SYSDBOUT DD  SYSOUT=&DMPS
//SYSUDUMP DD  SYSOUT=&DMPS
//SYSOUT   DD  SYSOUT=&JCLO
//SYSPRINT DD  SYSOUT=&JCLO
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//CMPRT01  DD  SYSOUT=&REPT
//CMPRT02  DD  SYSOUT=&REPT
//CMSYNIN  DD  DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(P1465CPE),DISP=SHR
//* FCPP662
//*****************************************************************
//* EXTR050 - EXTRACT CPS LEDGER DATA FOR NZ PAYMENTS ONLY
//*           PROCESSING BASED ON SPECIFIED INTERFACE DATE.
//*           NATURAL PROGRAM "FCPP120Y".
//*--------------------------------------------------------------------
//EXTR050  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//             PARM='SYS=&NAT'
//SYSPRINT  DD SYSOUT=&JCLO
//SYSUDUMP  DD SYSOUT=&DMPS
//DDCARD    DD DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//DDPRINT   DD SYSOUT=&JCLO
//CMPRINT   DD SYSOUT=&JCLO
//CMPRT01   DD SYSOUT=(&REPT,CP1465D5)
//CMWKF01   DD DSN=&HLQ1..&JOBNM..LEDGERNZ.DAILY(&GDG1),
//             LRECL=100,RECFM=LS,BUFNO=25,
//             DISP=(NEW,CATLG,DELETE),DATACLAS=&DATACLAS,
//             SPACE=(CYL,(&SPC3),RLSE),UNIT=SYSDA
//CMSYNIN   DD DSN=&LIB2..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(P1465CPF),DISP=SHR
//          DD DSN=&LIB3..CNTL.PARMLIB(P1465CP1),DISP=SHR
//          DD DSN=&LIB1..PARMLIB(P1465CP2),DISP=SHR
//* FCPP120Y FOR NZ ONLY
//*********************************************************************
