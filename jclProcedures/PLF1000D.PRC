//PLF1000D PROC DBID=045,
//      INCLUDE='PSTPINCL',      INCLUDE-ORACLE INTERFACE PROD
//      NAT=NAT003P,
//      NATLIB=PANNSEC,
//      LIB1=PROD,
//      LIB2=PROD.NT2LOGON,
//      PASSW=UP003SEC,                      ORACLE/ACISFLE
//      INCLUD2=PSTPINCL,                    ORACLE/ACISFLE
//      HLQ1=POMPL.FA,
//      HLQ2=PPDD.ANN,
//      SPC1=50,
//      SPC2=20,
//      JCLO='*',
//      DMPS=U,
//      REPT=8,
//      REGN1=4M,
//      GDG=0
//*                                                                     
//*********************************************************************
//* MODIFICATION                                                      *
//* 12/2014 MONTESCLAROS,YURI C332636 REPLACE UP303SEC W/ UP003SEC    *
//* 05/2016 JAMIE CRUZ - UNCOMMENT JCL FOR CREATING PTRKTPCD REQUESTS *
//*                    - AS THIS PACKAGE IS NOW ACTIVATED IN CCP.     *
//*********************************************************************
//* --------------------------------------------------------------------
//* DELT000  DELETES FILES CREATED PREVIOUSLY IN STEP 'SORT005'
//* --------------------------------------------------------------------
//DELT000 EXEC PGM=IEFBR14
//DD01    DD DSN=&HLQ2..INPUT.LTFP0010,
//           DISP=(MOD,DELETE),
//           UNIT=SYSDA,SPACE=(TRK,0)
//DD02    DD DSN=&HLQ2..INPUT.LTFP0100,
//           DISP=(MOD,DELETE),
//           UNIT=SYSDA,SPACE=(TRK,0)
//DD03    DD DSN=&HLQ2..INPUT.LTFP0200,
//           DISP=(MOD,DELETE),
//           UNIT=SYSDA,SPACE=(TRK,0)
//DD04    DD DSN=&HLQ2..INPUT.LTFP0300,
//           DISP=(MOD,DELETE),
//           UNIT=SYSDA,SPACE=(TRK,0)
//DD05    DD DSN=&HLQ2..INPUT.LTFP0310,
//           DISP=(MOD,DELETE),
//           UNIT=SYSDA,SPACE=(TRK,0)
//DD06    DD DSN=&HLQ2..INPUT.LTFP0400,
//           DISP=(MOD,DELETE),
//           UNIT=SYSDA,SPACE=(TRK,0)
//DD07    DD DSN=&HLQ2..INPUT.LTFP0500,
//           DISP=(MOD,DELETE),
//           UNIT=SYSDA,SPACE=(TRK,0)
//DD08    DD DSN=&HLQ2..INPUT.IOLP0500,
//           DISP=(MOD,DELETE),
//           UNIT=SYSDA,SPACE=(TRK,0)
//DD09    DD DSN=&HLQ2..INPUT.IOLP0510,
//           DISP=(MOD,DELETE),
//           UNIT=SYSDA,SPACE=(TRK,0)
//DD10    DD DSN=&HLQ2..INPUT.LTFP0120,
//           DISP=(MOD,DELETE),
//           UNIT=SYSDA,SPACE=(TRK,0)
//DD12    DD DSN=&HLQ2..INPUT.LTFP0110,
//           DISP=(MOD,DELETE),
//           UNIT=SYSDA,SPACE=(TRK,0)
//DD13    DD DSN=&HLQ2..INPUT.LTFP0020,
//           DISP=(MOD,DELETE),
//           UNIT=SYSDA,SPACE=(TRK,0)
//*                                                                     
//* --------------------------------------------------------------------
//* SORT005  SPLITS THE OMNI EXTRACT FILE INTO 10 INPUT FILES FOR
//*          PRINTING VARIOUS LUMPSUM/TPA/IPRO LETTERS.                 
//*          REALLY A 'COPY'; NEEDS NO SORT WORK AREAS.                 
//* --------------------------------------------------------------------
//SORT005  EXEC PGM=SORT,REGION=6M,COND=(0,NE)
//SYSOUT   DD SYSOUT=*
//SORTIN   DD DSN=&HLQ1..BASEEXT(&GDG),DISP=SHR
//**          RECFM=VB,LRECL=30004
//SORTOF01 DD DSN=&HLQ2..INPUT.LTFP0010,
//            DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//            UNIT=SYSDA,RECFM=VB,LRECL=30004,
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE)
//SORTOF02 DD DSN=&HLQ2..INPUT.LTFP0100,
//            DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//            UNIT=SYSDA,RECFM=VB,LRECL=30004,
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE)
//SORTOF03 DD DSN=&HLQ2..INPUT.LTFP0200,
//            DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//            UNIT=SYSDA,RECFM=VB,LRECL=30004,
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE)
//SORTOF04 DD DSN=&HLQ2..INPUT.LTFP0300,
//            DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//            UNIT=SYSDA,RECFM=VB,LRECL=30004,
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE)
//SORTOF05 DD DSN=&HLQ2..INPUT.LTFP0310,
//            DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//            UNIT=SYSDA,RECFM=VB,LRECL=30004,
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE)
//SORTOF06 DD DSN=&HLQ2..INPUT.LTFP0400,
//            DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//            UNIT=SYSDA,RECFM=VB,LRECL=30004,
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE)
//SORTOF07 DD DSN=&HLQ2..INPUT.LTFP0500,
//            DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//            UNIT=SYSDA,RECFM=VB,LRECL=30004,
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE)
//SORTOF08 DD DSN=&HLQ2..INPUT.IOLP0500,
//            DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//            UNIT=SYSDA,RECFM=VB,LRECL=30004,
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE)
//SORTOF09 DD DSN=&HLQ2..INPUT.IOLP0510,
//            DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//            UNIT=SYSDA,RECFM=VB,LRECL=30004,
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE)
//SORTOF10 DD DSN=&HLQ2..INPUT.LTFP0120,
//            DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//            UNIT=SYSDA,RECFM=VB,LRECL=30004,
//            SPACE=(CYL,(&SPC1,&SPC2),RLSE)
//SORTOF11 DD DUMMY
//*SORTOF11 DD DSN=&HLQ2..INPUT.LTFP0040,
//*            DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//*            UNIT=SYSDA,RECFM=VB,LRECL=30004,
//*            SPACE=(TRK,(&SPC1,&SPC2),RLSE)
//SORTOF12 DD DSN=&HLQ2..INPUT.LTFP0110,
//            DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//            UNIT=SYSDA,RECFM=VB,LRECL=30004,
//            SPACE=(TRK,(&SPC1,&SPC2),RLSE)
//SORTOF13 DD DSN=&HLQ2..INPUT.LTFP0020,
//            DISP=(NEW,CATLG,DELETE),DATACLAS=DCPSEXTC,
//            UNIT=SYSDA,RECFM=VB,LRECL=30004,
//            SPACE=(TRK,(&SPC1,&SPC2),RLSE)
//SYSIN    DD DSN=&LIB1..PARMLIB(LF1000DC),DISP=SHR                     
//*
//* -------------------------------------------------------------------
//* LTFP0010/PSTL9845  PFRKLSM/LUMP SUM WITHDRAWALS                     
//* -------------------------------------------------------------------
//REPT010  EXEC PGM=NATB030,COND=(1,LT,SORT005),                        
//   REGION=0M,                                  ORACLE PSG9061
//   PARM='OBJIN=N,IM=D,SYS=&NAT,MADIO=0,MT=0,INTENS=1,OPRB=(.ALL)'
//*                                                                     
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//SYSPRINT DD  SYSOUT=&JCLO                                             
//DDPRINT  DD  SYSOUT=&JCLO                                             
//SYSOUT   DD  SYSOUT=&JCLO                                             
//SYSUDUMP DD  SYSOUT=&DMPS                                             
//SYSABOUT DD  SYSOUT=&DMPS                                             
//CMPRINT  DD  SYSOUT=&JCLO                                             
//CMWKF01  DD  DSN=&HLQ2..INPUT.LTFP0010,UNIT=SYSDA,DISP=SHR            
//*********************************************************************
//*** LF1000DT CONTAINS A LIST OF PLANS THAT WILL ALWAYS HAVE THE
//*** EMPLOYER AUTHORIZATION PAGE PRINTED.
//*** WHEN ADDING PLANS SORT THE 'LF1000DT'. PLAN 999999 SHOULD ALWAYS
//*** BE THE LAST PLAN.
//*********************************************************************
//CMWKF02  DD  DSN=&LIB1..PARMLIB(LF1000DT),DISP=SHR
//*********************************************************************
//CMPRT01  DD  SYSOUT=(&REPT,LF1000D1)                                  
//*
//*MSYS // INCLUDE MEMBER=&INCLUD2                       ORACLE PSG9061
//ACISFLE  DD  DSN=&LIB1..ORALOGON.DN.PARMLIB(&PASSW),DISP=SHR
//*
//*MSYS //     INCLUDE MEMBER=&INCLUDE
//SECFILE  DD  DSN=&LIB1..ORALOGON.NY.PARMLIB(EXTSEC1),DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..PARMLIB(&NATLIB),DISP=SHR                     
//         DD  DSN=&LIB1..PARMLIB(LF1000D1),DISP=SHR                    
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                     
//*
//* -------------------------------------------------------------------
//* LTFP0100/PSTL6775  PTRKSNPC/SWAT PAYMENTS - STOPPING/STOPPED        
//* -------------------------------------------------------------------
//REPT020  EXEC PGM=NATB030,COND=(1,LT,SORT005),REGION=&REGN1,          
//   PARM='OBJIN=N,IM=D,SYS=&NAT,MADIO=0,MT=0,INTENS=1,OPRB=(.ALL)'
//*                                                                     
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//SYSPRINT DD  SYSOUT=&JCLO                                             
//DDPRINT  DD  SYSOUT=&JCLO                                             
//SYSOUT   DD  SYSOUT=&JCLO                                             
//SYSUDUMP DD  SYSOUT=&DMPS                                             
//SYSABOUT DD  SYSOUT=&DMPS                                             
//CMPRINT  DD  SYSOUT=&JCLO                                             
//CMWKF01  DD  DSN=&HLQ2..INPUT.LTFP0100,UNIT=SYSDA,DISP=SHR            
//CMPRT01  DD  SYSOUT=(&REPT,LF1000D2)                                  
//*MSYS //     INCLUDE MEMBER=&INCLUDE
//SECFILE  DD  DSN=&LIB1..ORALOGON.NY.PARMLIB(EXTSEC1),DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..PARMLIB(&NATLIB),DISP=SHR                     
//         DD  DSN=&LIB1..PARMLIB(LF1000D2),DISP=SHR                    
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                     
//*
//* -------------------------------------------------------------------
//* LTFN0200/PSTL6745  PTRKACFS/CONFIRM LS-TPA-IPRO EXTERNAL ROLLOVERS  
//* LTFN0201/PSTL6705  PTRKTPCD/CONFIRM LS-TPA-IPRO INTERNAL ROLLOVERS  
//* LTFN0201/PSTL6705  PTRKTPCM/TPA REINVESTMENTS                       
//* -------------------------------------------------------------------
//REPT030  EXEC PGM=NATB030,COND=(1,LT,SORT005),REGION=&REGN1,          
//   PARM='OBJIN=N,IM=D,SYS=&NAT,MADIO=0,MT=0,INTENS=1,OPRB=(.ALL)'
//*                                                                     
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//SYSPRINT DD  SYSOUT=&JCLO                                             
//DDPRINT  DD  SYSOUT=&JCLO                                             
//SYSOUT   DD  SYSOUT=&JCLO                                             
//SYSUDUMP DD  SYSOUT=&DMPS                                             
//SYSABOUT DD  SYSOUT=&DMPS                                             
//CMPRINT  DD  SYSOUT=&JCLO                                             
//CMWKF01  DD  DSN=&HLQ2..INPUT.LTFP0200,UNIT=SYSDA,DISP=SHR            
//CMPRT01  DD  SYSOUT=(&REPT,LF1000D3)                                  
//*MSYS //     INCLUDE MEMBER=&INCLUDE
//SECFILE  DD  DSN=&LIB1..ORALOGON.NY.PARMLIB(EXTSEC1),DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..PARMLIB(&NATLIB),DISP=SHR                     
//         DD  DSN=&LIB1..PARMLIB(LF1000D3),DISP=SHR                    
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                     
//*
//* -------------------------------------------------------------------
//* LTFP0300/PSTL6715  PTRKTPAD/TPA INITIAL ACKNOWLEDGMENTS             
//* -------------------------------------------------------------------
//REPT040  EXEC PGM=NATB030,COND=(1,LT,SORT005),REGION=&REGN1,          
//   PARM='OBJIN=N,IM=D,SYS=&NAT,MADIO=0,MT=0,INTENS=1,OPRB=(.ALL)'
//*                                                                     
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//SYSPRINT DD  SYSOUT=&JCLO                                             
//DDPRINT  DD  SYSOUT=&JCLO                                             
//SYSOUT   DD  SYSOUT=&JCLO                                             
//SYSUDUMP DD  SYSOUT=&DMPS                                             
//SYSABOUT DD  SYSOUT=&DMPS                                             
//CMPRINT  DD  SYSOUT=&JCLO                                             
//CMWKF01  DD  DSN=&HLQ2..INPUT.LTFP0300,UNIT=SYSDA,DISP=SHR            
//CMPRT01  DD  SYSOUT=(&REPT,LF1000D4)                                  
//*MSYS //     INCLUDE MEMBER=&INCLUDE
//SECFILE  DD  DSN=&LIB1..ORALOGON.NY.PARMLIB(EXTSEC1),DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..PARMLIB(&NATLIB),DISP=SHR                     
//         DD  DSN=&LIB1..PARMLIB(LF1000D4),DISP=SHR                    
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                     
//*
//* -------------------------------------------------------------------
//* LTFP0310/PSTL6705  PTRKTPCD/TPA MAINTENANCE ACKNOWLEDGMENTS         
//* -------------------------------------------------------------------
//REPT050  EXEC PGM=NATB030,COND=(1,LT,SORT005),REGION=&REGN1,          
//   PARM='OBJIN=N,IM=D,SYS=&NAT,MADIO=0,MT=0,INTENS=1,OPRB=(.ALL)'
//*                                                                     
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//SYSPRINT DD  SYSOUT=&JCLO                                             
//DDPRINT  DD  SYSOUT=&JCLO                                             
//SYSOUT   DD  SYSOUT=&JCLO                                             
//SYSUDUMP DD  SYSOUT=&DMPS                                             
//SYSABOUT DD  SYSOUT=&DMPS                                             
//CMPRINT  DD  SYSOUT=&JCLO                                             
//CMWKF01  DD  DSN=&HLQ2..INPUT.LTFP0310,UNIT=SYSDA,DISP=SHR            
//CMPRT01  DD  SYSOUT=(&REPT,LF1000D5)                                  
//*MSYS //     INCLUDE MEMBER=&INCLUDE
//SECFILE  DD  DSN=&LIB1..ORALOGON.NY.PARMLIB(EXTSEC1),DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..PARMLIB(&NATLIB),DISP=SHR                     
//         DD  DSN=&LIB1..PARMLIB(LF1000D5),DISP=SHR                    
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                     
//*
//* -------------------------------------------------------------------
//* LTFP0400/PSTL6725  PTRKTPPD/TPA-IPRO RESETTLEMENTS - ADDL CONTRIB   
//* -------------------------------------------------------------------
//REPT060  EXEC PGM=NATB030,COND=(1,LT,SORT005),REGION=&REGN1,          
//   PARM='OBJIN=N,IM=D,SYS=&NAT,MADIO=0,MT=0,INTENS=1,OPRB=(.ALL)'
//*                                                                     
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//SYSPRINT DD  SYSOUT=&JCLO                                             
//DDPRINT  DD  SYSOUT=&JCLO                                             
//SYSOUT   DD  SYSOUT=&JCLO                                             
//SYSUDUMP DD  SYSOUT=&DMPS                                             
//SYSABOUT DD  SYSOUT=&DMPS                                             
//CMPRINT  DD  SYSOUT=&JCLO                                             
//CMWKF01  DD  DSN=&HLQ2..INPUT.LTFP0400,UNIT=SYSDA,DISP=SHR            
//CMPRT01  DD  SYSOUT=(&REPT,LF1000D6)                                  
//*MSYS //     INCLUDE MEMBER=&INCLUDE
//SECFILE  DD  DSN=&LIB1..ORALOGON.NY.PARMLIB(EXTSEC1),DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..PARMLIB(&NATLIB),DISP=SHR                     
//         DD  DSN=&LIB1..PARMLIB(LF1000D6),DISP=SHR                    
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                     
//*
//* ===================================================================
//* LTFP0500/PSTL6815  PTRKLSHW/LUMP SUM HARDSHIP WITHDRAWALS           
//* -------------------------------------------------------------------
//REPT070  EXEC PGM=NATB030,COND=(1,LT,SORT005),                        
//   REGION=0M,                                  ORACLE PSG9061
//   PARM='OBJIN=N,IM=D,SYS=&NAT,MADIO=0,MT=0,INTENS=1,OPRB=(.ALL)'
//*                                                                     
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//SYSPRINT DD  SYSOUT=&JCLO                                             
//DDPRINT  DD  SYSOUT=&JCLO                                             
//SYSOUT   DD  SYSOUT=&JCLO                                             
//SYSUDUMP DD  SYSOUT=&DMPS                                             
//SYSABOUT DD  SYSOUT=&DMPS                                             
//CMPRINT  DD  SYSOUT=&JCLO                                             
//CMWKF01  DD  DSN=&HLQ2..INPUT.LTFP0500,UNIT=SYSDA,DISP=SHR            
//CMPRT01  DD  SYSOUT=(&REPT,LF1000D7)                                  
//*
//*MSYS // INCLUDE MEMBER=&INCLUD2                       ORACLE PSG9061
//ACISFLE  DD  DSN=&LIB1..ORALOGON.DN.PARMLIB(&PASSW),DISP=SHR
//*
//*MSYS //     INCLUDE MEMBER=&INCLUDE
//SECFILE  DD  DSN=&LIB1..ORALOGON.NY.PARMLIB(EXTSEC1),DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..PARMLIB(&NATLIB),DISP=SHR                     
//         DD  DSN=&LIB1..PARMLIB(LF1000D7),DISP=SHR                    
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                     
//*
//* -------------------------------------------------------------------
//* IOLP0500/PSTL6715  PTRKTPAD/IPRO INITIAL ACKNOWLEDGMENTS            
//* -------------------------------------------------------------------
//REPT080  EXEC PGM=NATB030,COND=(1,LT,SORT005),REGION=&REGN1,          
//   PARM='OBJIN=N,IM=D,SYS=&NAT,MADIO=0,MT=0,INTENS=1,OPRB=(.ALL)'
//*                                                                     
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//SYSPRINT DD  SYSOUT=&JCLO                                             
//DDPRINT  DD  SYSOUT=&JCLO                                             
//SYSOUT   DD  SYSOUT=&JCLO                                             
//SYSUDUMP DD  SYSOUT=&DMPS                                             
//SYSABOUT DD  SYSOUT=&DMPS                                             
//CMPRINT  DD  SYSOUT=&JCLO                                             
//CMWKF01  DD  DSN=&HLQ2..INPUT.IOLP0500,UNIT=SYSDA,DISP=SHR            
//CMPRT01  DD  SYSOUT=(&REPT,LF1000D0)                                  
//*MSYS //     INCLUDE MEMBER=&INCLUDE
//SECFILE  DD  DSN=&LIB1..ORALOGON.NY.PARMLIB(EXTSEC1),DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..PARMLIB(&NATLIB),DISP=SHR                     
//         DD  DSN=&LIB1..PARMLIB(LF1000D0),DISP=SHR                    
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                     
//*
//* -------------------------------------------------------------------
//* IOLP0510/PSTL6845  PTRKIPMT/IPRO MAINTENANCE ACKNOWLEDGMENTS        
//* -------------------------------------------------------------------
//REPT090  EXEC PGM=NATB030,COND=(1,LT,SORT005),REGION=&REGN1,          
//   PARM='OBJIN=N,IM=D,SYS=&NAT,MADIO=0,MT=0,INTENS=1,OPRB=(.ALL)'
//*                                                                     
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//SYSPRINT DD  SYSOUT=&JCLO                                             
//DDPRINT  DD  SYSOUT=&JCLO                                             
//SYSOUT   DD  SYSOUT=&JCLO                                             
//SYSUDUMP DD  SYSOUT=&DMPS                                             
//SYSABOUT DD  SYSOUT=&DMPS                                             
//CMPRINT  DD  SYSOUT=&JCLO                                             
//CMWKF01  DD  DSN=&HLQ2..INPUT.IOLP0510,UNIT=SYSDA,DISP=SHR            
//CMPRT01  DD  SYSOUT=(&REPT,LF1000DA)                                  
//*MSYS //     INCLUDE MEMBER=&INCLUDE
//SECFILE  DD  DSN=&LIB1..ORALOGON.NY.PARMLIB(EXTSEC1),DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..PARMLIB(&NATLIB),DISP=SHR                     
//         DD  DSN=&LIB1..PARMLIB(LF1000DA),DISP=SHR                    
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                     
//*
//* -------------------------------------------------------------------
//* LTFP0120/PSTL0155  PTRKICAK/ICAP INITIAL ACKNOWLEDGMENTS            
//* -------------------------------------------------------------------
//REPT100  EXEC PGM=NATB030,COND=(1,LT,SORT005),REGION=&REGN1,          
//   PARM='OBJIN=N,IM=D,SYS=&NAT,MADIO=0,MT=0,INTENS=1,OPRB=(.ALL)'
//*                                                                     
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR                  
//SYSPRINT DD  SYSOUT=&JCLO                                             
//DDPRINT  DD  SYSOUT=&JCLO                                             
//SYSOUT   DD  SYSOUT=&JCLO                                             
//SYSUDUMP DD  SYSOUT=&DMPS                                             
//SYSABOUT DD  SYSOUT=&DMPS                                             
//CMPRINT  DD  SYSOUT=&JCLO                                             
//CMWKF01  DD  DSN=&HLQ2..INPUT.LTFP0120,UNIT=SYSDA,DISP=SHR            
//CMPRT01  DD  SYSOUT=(&REPT,LF1000DB)                                  
//*MSYS //     INCLUDE MEMBER=&INCLUDE
//SECFILE  DD  DSN=&LIB1..ORALOGON.NY.PARMLIB(EXTSEC1),DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..PARMLIB(&NATLIB),DISP=SHR                     
//         DD  DSN=&LIB1..PARMLIB(LF1000DB),DISP=SHR                    
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR                     
//*
//* -------------------------------------------------------------------
//* LTFP0110/PSTL0015  PTRKSWTP/SWAT CONFIRMATION
//* -------------------------------------------------------------------
//REPT120  EXEC PGM=NATB030,COND=(1,LT,SORT005),REGION=&REGN1,
//   PARM='OBJIN=N,IM=D,SYS=&NAT,MADIO=0,MT=0,INTENS=1,OPRB=(.ALL)'
//*
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSPRINT DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//SYSABOUT DD  SYSOUT=&DMPS
//CMPRINT  DD  SYSOUT=&JCLO
//CMWKF01  DD  DSN=&HLQ2..INPUT.LTFP0110,UNIT=SYSDA,DISP=SHR
//CMPRT01  DD  SYSOUT=(&REPT,LF1000D9)
//*MSYS //     INCLUDE MEMBER=&INCLUDE
//SECFILE  DD  DSN=&LIB1..ORALOGON.NY.PARMLIB(EXTSEC1),DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..PARMLIB(&NATLIB),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(LF1000D9),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//*
//* -------------------------------------------------------------------
//* LTFP0020/PSTL0135  PTRKGRAS/GRA SURRENDER FEE CONFIRMATION
//* -------------------------------------------------------------------
//REPT130  EXEC PGM=NATB030,COND=(1,LT,SORT005),REGION=&REGN1,
//   PARM='OBJIN=N,IM=D,SYS=&NAT,MADIO=0,MT=0,INTENS=1,OPRB=(.ALL)'
//*
//DDCARD   DD  DSN=&LIB1..PARMLIB(DBAPP&DBID),DISP=SHR
//SYSPRINT DD  SYSOUT=&JCLO
//DDPRINT  DD  SYSOUT=&JCLO
//SYSOUT   DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//SYSABOUT DD  SYSOUT=&DMPS
//CMPRINT  DD  SYSOUT=&JCLO
//CMWKF01  DD  DSN=&HLQ2..INPUT.LTFP0020,UNIT=SYSDA,DISP=SHR
//CMPRT01  DD  SYSOUT=(&REPT,LF1000DD)
//*MSYS //     INCLUDE MEMBER=&INCLUDE
//SECFILE  DD  DSN=&LIB1..ORALOGON.NY.PARMLIB(EXTSEC1),DISP=SHR
//CMSYNIN  DD  DSN=&LIB2..PARMLIB(&NATLIB),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(LF1000DD),DISP=SHR
//         DD  DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//*
