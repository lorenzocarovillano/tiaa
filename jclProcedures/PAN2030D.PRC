//PAN2030D PROC DBID='045',
//         DBID2='003',
//         DMPS='U',
//         HLQ8='PPDD',
//         JCLO='*',
//         JOBNM='PAN2030D',
//         LIB1='PROD',
//         LIB2='PROD',
//         NAT='NAT003P',
//         NATMEM='PANNSEC',
//         REPT='8',
//         REPTID1='AN2030D1',
//         REPTID2='AN2030D2',
//         REPTID3='AN2030D3',
//*        REPTID4='AN2030D4',
//*        REPTID5='AN2030D5',
//         PARMMEM1='AN2030D1',
//         PARMMEM2='AN2030D2',
//         PARMMEM3='AN2030D3',
//*        PARMMEM4='AN2030D4',
//         REGN1='4M',
//         SPC1='50,30'
//*                                                                   *
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
//*                                                                   *
//* PROCEDURE:  PAN2030D                 DATE:    04/21/04            *
//* PROGRAMS EXECUTED:                                                *
//*   STEP NAME    PROGRAM NAME                                       *
//* -------------  ------------                                       *
//*   UNDO010      IEFBR14                                            *
//*   REPT020      ADSP930                                            *
//*   REPT030      ADSP931                                            *
//*   REPT040      ADSP937                                            *
//*   UPDT050      ADSP936                                            *
//*                                                                   *
//*                                                                   *
//* ADABAS FILES ACCESSED:                                            *
//* DB 045, FILES 228 (ADS PARTICIPANT) AND 229 (ADS CONTRACT)        *
//* DB 003, FILE 231 (CONTROL RECORDS)                                *
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
//* STEP REPT020 EXTRACTS DATA AND WRITES WORKFILE PPDD.PAN2030D.TEMP1*
//*   READ BY ADSP931 AND ADSP932 (STEP REPT030), WHICH REPORT ON     *
//*   DELAYED AND INCOMPLETE ADAS SETTLEMENTS BY UNIT.                *
//* STEP REPT040 REPORT ON DELAYED AND INCOMPLETE ADAS SETTLEMENTS    *
//*              FOR SURVIVOR RECORDS ONLY                            *
//* STEP UPDT050 (PROGRAM ADSP936) UPDATES THE CONTROL RECORD.        *
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
//* MODIFICATION HISTORY                                              *
//* 04/21/04 - CREATED FOR ADAS BATCH REPORT                          *
//* 04/13/10 - CREATED FOR ADAS BATCH REPORT - SURVIVOR ONLY          *
//*                                                                   * 
//********************************************************************* 
//*
//UNDO010 EXEC PGM=IEFBR14,COND=(0,NE)
//WKFILE1  DD DSN=&HLQ8..&JOBNM..TEMP1,
//         DISP=(MOD,DELETE),SPACE=(CYL,(&SPC1),RLSE),UNIT=SYSDA
//*WKFILE2  DD DSN=&HLQ8..&JOBNM..TEMP2,
//*         DISP=(MOD,DELETE),SPACE=(CYL,(&SPC1),RLSE),UNIT=SYSDA
//***************************************************************
//REPT020 EXEC PGM=NATB030,
//             PARM='SYS=&NAT',
//             REGION=&REGN1
//SYSABOUT DD SYSOUT=&DMPS
//SYSUDUMP DD SYSOUT=&DMPS
//SYSOUT   DD SYSOUT=&JCLO
//DDCARD   DD DSN=&LIB2..PARMLIB(DBAPP&DBID),DISP=SHR
//CMWKF01  DD DSN=&HLQ8..&JOBNM..TEMP1,
//            DISP=(,CATLG,DELETE),
//            UNIT=SYSDA,
//            SPACE=(CYL,(&SPC1),RLSE),
//            RECFM=LS
//*           LRECL=66,RECFM=LS
//*CMWKF02  DD DSN=&HLQ8..&JOBNM..TEMP2,
//*            DISP=(,CATLG,DELETE),
//*            UNIT=SYSDA,
//*            SPACE=(CYL,(&SPC1),RLSE),
//*            RECFM=LS
//*           LRECL=73,RECFM=LS
//CMPRINT  DD SYSOUT=&JCLO
//CMPRT01  DD SYSOUT=(&REPT,&REPTID1)
//CMSYNIN  DD DSN=&LIB1..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(&PARMMEM1),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//***************************************************************
//REPT030  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//    PARM='SYS=&NAT'
//SYSPRINT DD  SYSOUT=&JCLO
//DDCARD   DD  DSN=&LIB2..PARMLIB(DBAPP&DBID),DISP=SHR
//CMWKF01  DD  DSN=&HLQ8..&JOBNM..TEMP1,DISP=SHR
//SORTWK01 DD  UNIT=SYSDA,
//             SPACE=(CYL,(&SPC1),RLSE)
//SYSOUT   DD  SYSOUT=&JCLO
//SYSUDUMP DD  SYSOUT=&DMPS
//DDPRINT  DD  SYSOUT=&JCLO
//CMPRINT  DD  SYSOUT=&JCLO
//CMPRT01  DD  SYSOUT=(&REPT,&REPTID2)
//CMPRT02  DD  SYSOUT=(&REPT,&REPTID3)
//CMSYNIN  DD DSN=&LIB1..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(&PARMMEM2),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//***************************************************************
//*REPT040  EXEC PGM=NATB030,REGION=&REGN1,COND=(0,NE),
//*    PARM='SYS=&NAT'
//*SYSPRINT DD  SYSOUT=&JCLO
//*DDCARD   DD  DSN=&LIB2..PARMLIB(DBAPP&DBID),DISP=SHR
//*CMWKF01  DD  DSN=&HLQ8..&JOBNM..TEMP2,DISP=SHR
//*SORTWK01 DD  UNIT=SYSDA,
//*             SPACE=(CYL,(&SPC1),RLSE)
//*SYSOUT   DD  SYSOUT=&JCLO
//*SYSUDUMP DD  SYSOUT=&DMPS
//*DDPRINT  DD  SYSOUT=&JCLO
//*CMPRINT  DD  SYSOUT=&JCLO
//*CMPRT01  DD  SYSOUT=(&REPT,&REPTID4)
//* CMPRT02  DD  SYSOUT=(&REPT,&REPTID5)
//*CMSYNIN  DD DSN=&LIB1..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//*         DD DSN=&LIB1..PARMLIB(&PARMMEM4),DISP=SHR
//*         DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
//***************************************************************
//UPDT050 EXEC PGM=NATB030,
//             PARM='SYS=&NAT',
//             REGION=&REGN1,COND=(0,NE)
//SYSABOUT DD SYSOUT=&DMPS
//SYSUDUMP DD SYSOUT=&DMPS
//SYSOUT   DD SYSOUT=&JCLO
//DDCARD   DD DSN=&LIB2..PARMLIB(DBAPP&DBID2),DISP=SHR
//CMPRINT  DD SYSOUT=&JCLO
//CMPRT01  DD SYSOUT=&JCLO
//CMSYNIN  DD DSN=&LIB1..NT2LOGON.PARMLIB(&NATMEM),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(&PARMMEM3),DISP=SHR
//         DD DSN=&LIB1..PARMLIB(FINPARM),DISP=SHR
