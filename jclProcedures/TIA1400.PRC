//TIA1400  PROC  OQUAL='POMPY.PMT0',                                    
//         TR83='POMPY.PMT0.TR83EXT(0)',                                
//         LPREFX='PROD.OMNIPAY.BAT'                                    
//********************************************************************* 
//* STEP 1: PARSE TR83 EXTRACT FILE TO PRODUCE IDENTITY AND             
//* ENTITLEMENT CSV FILES                                               
//********************************************************************* 
//REXX1    EXEC PGM=IRXJCL,REGION=2M,                                   
//         PARM='AVRXOP01'                                              
//SYSTSPRT DD SYSOUT=*                                                  
//SYSEXEC  DD DSN=&LPREFX..CTL,DISP=SHR                                 
//INDDTR83 DD DSN=&TR83,DISP=SHR                                        
//OUTDDUSR DD DSN=&OQUAL..USR,                                          
//         DISP=SHR                                                     
//OUTDDENT DD DSN=&OQUAL..ENT,                                          
//         DISP=SHR                                                     
//*                                                                     
//*      PEND                                                           
//*                                                                     
